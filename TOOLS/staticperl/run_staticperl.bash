#!/bin/bash                                       
# Script for creating static perl executables using staticperl
# http://search.cpan.org/~mlehmann/App-Staticperl-1.43/bin/staticperl

### Adjust this path
STPERL=/user/plasserf/programs/perl/App-Staticperl-1.44/bin/staticperl
###                                                                   

### Make a new perl interpreter
# Standard modules
#   some of these have to be installed with
#   staticperl instcpan <module>
# Note: $COLUMBUS/CPAN/Curses.pm has to be deleted, since a different version
#   of Curses is given here
MPACK="-MCwd -Mlib -MShell -MCPAN::Shell -MFile::Copy -MFile::Glob -MFile::Basename -MConfig -MCurses -MTerm::Cap -MPOSIX"
#MPACK="-MCwd -Mlib -MShell -MCPAN::Shell -MFile::Copy -MFile::Glob -MConfig -MTerm::Cap"

echo $STPERL mkperl $MPACK
eval $STPERL mkperl $MPACK
mv -v perl colperl

### Optionally, also link with the Columbus modules.
# But it is probably better to read these from the actual .pm files
ADD=""
for PM in $COLUMBUS/perlscripts/*.pm
do
   ADD="${ADD} --add \"$PM ${PM##*/}\" "
done
ADD="${ADD} --add \"$COLUMBUS/../TOOLS/CPAN/perlmenu.v4.0/menuutil.pl menuutil.pl\" "

echo $STPERL mkperl $MPACK $ADD
eval $STPERL mkperl $MPACK $ADD
mv -v perl colperl.complete

### After this, adjust the interpreter in the perl files                                                                                                                                   
#export PERL=$COLUMBUS/colperl   
#cp -v colperl $PERL                                                                                                                   
## or:
#export PERL=colperl
#cp -v colperl ~/bin/colperl # copy it somewhere into the path

#cd $COLUMBUS                                                                                                                                 
#$PERL $COLUMBUS/perlshebang *.pl gaunder ../filepp colinp runc prepinp DtoSblas StoDblas perlscripts/*.pl                                    
#rm -v $COLUMBUS/CPAN/Curses.pm

### Or make the individual applications
#$STPERL mkapp runc --boot $COLUMBUS/runc $MPACK
#$STPERL mkapp colinp --boot $COLUMBUS/colinp $MPACK
