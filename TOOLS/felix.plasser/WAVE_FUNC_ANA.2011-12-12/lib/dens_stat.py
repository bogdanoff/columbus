#!/usr/bin/python
import os
import numpy
import orb_compare

try:
    import matplotlib
    matplotlib.use('Agg')
    import pylab
except:
    print "pylab/matplotlib not installed - plotting not possible"

# interfaces to Columbus and Turbomole

class dens_ana:
    """
    Class for density matrix analysis.
    """
    def __init__(self,mo_file,rdMOargs={},plot=0):
        
        self.Ddict = {} # dictionary with all density matrices
        self.Omega_dict = {} # dictionary with the charge transfer numbers
        self.partition = None
        self.plot=plot
        
        self.prtD_list = []
        self.prtTD_list = []
        self.state_list = []
        
        self.mos = orb_compare.MO_set(file=mo_file)
        self.mos.read_molden_file(get_GTO=True,**rdMOargs)
        #self.mos.compute_inverse(strict=True)
        self.mos.compute_inverse(strict=False)
        self.num_mo = len(self.mos.mo_mat)
        self.num_bas = len(self.mos.mo_mat[0])
        #print 'number of MOs: %i, number of basis functions: %i'%(self.num_mo, self.num_bas)
        
    # print routines provide quick interface to the calculation routines below
        
    def print_allD(self,not_list=['Dasy','Dsym']):
        """
        Print out all density matrices.
        """
        for typ,Dmat in self.Ddict.iteritems():
            if not typ in not_list:
                print typ
                print Dmat
            
    def print_all_sq(self,not_list=['Dasy','Dsym']):
        for typ,Dmat in self.Ddict.iteritems():
            if not typ in not_list:
                print typ
                print self.sumsquares(Dmat) 

    def print_all_S_part(self,*args,**kwargs):
        """
        Print the partitioning for the transition density.
        """
        for typ in self.prtTD_list:
            print 'Sum S: ',typ
            (sum_all,sum_part) = self.sum_S_part(typ,*args,**kwargs)
            print sum_all
            if sum_part!=None:
                print sum_part
                if self.plot >= 1:
                    pylab.figure()
                    pylab.pcolor(sum_part,cmap=pylab.get_cmap(name='Greys'))
                    pylab.axis('off')
                    pylab.savefig('pcolor_%s.png'%typ, orientation='landscape',dpi=50)
                    
		if self.plot == 2:
                    pylab.figure()
                    abs_max, phase_part = self.Om_phases(self.Ddict[typ])                    
                    
                    plot_arr = phase_part * sum_part
                    #print plot_arr                    
                    
                    pylab.pcolor(plot_arr,cmap=pylab.get_cmap(name='RdBu'),vmin=-abs_max,vmax=abs_max)
                    #pylab.axis('off')
                    pylab.savefig('pcolor2_%s.png'%typ, orientation='landscape',dpi=50)		   
                    
    def print_bond_orders(self,*args,**kwargs):
        """
        Print the bond orders.
        """
        # as opposed to Omega, the bond orders should not be divided by two
        #  c.f. Eqs (13) and (14) in JCTC, DOI:10.1021/ct300307c
        for typ in self.prtD_list:
            print 'bond order: ',typ
            (sum_all,sum_part) = self.sum_S_part(typ,*args,**kwargs)
            print 2. * sum_all
            if sum_part!=None:
                print 2. * sum_part
                if self.plot:
                    pylab.figure()
                    
                    # plot only the off-diagonal parts
                    num_part = len(self.partition)
                    plot_part = numpy.zeros([num_part,num_part])
                    for i in xrange(num_part):
                        for j in xrange(i+1,num_part):
                            plot_part[i,j] = sum_part[i,j]
                            plot_part[j,i] = sum_part[i,j]
                            
                    pylab.pcolor(plot_part,cmap=pylab.get_cmap(name='Greys'))
                    #pylab.axis('off')
                    pylab.savefig('pcolor_%s.png'%typ, orientation='landscape',dpi=50)                    
                
    def print_all_mullpop(self,*args,**kwargs):
        # this has to be divided by a factor of two to be consistent
        for typ in self.prtD_list:
            print 'mullpop: ',typ
            (sum_all,sum_part) = self.mullpop(self.Ddict[typ],*args,**kwargs)
            print sum_all
            if sum_part!=None:
                print sum_part
                
    def print_all_NTO(self,mincoeff=0.2,jmolout='NTOs.jmol',*args,**kwargs):
        """
        Print informatin about natural transition orbitals
        """
        if jmolout != '':
            jmfile = open(jmolout,'w')
            jmfile.write('background white\nmo fill\nmo cutoff 0.04\n')
            
        for typ in self.prtTD_list:
            print '\nNTOs: ',typ
            (U,s,Vh) = self.NTO(self.Ddict[typ],*args,**kwargs)
            # s contains actually the square roots of what is considered the singular values
            
            sum_SVD = sum(sval**2 for sval in s)
            PR_SVD = sum_SVD**2 / sum(sval**4 for sval in s)
            
            print 'sum_SVD: % .6f, PR_NTO % .3f'%(sum_SVD,PR_SVD)
            Uh = numpy.transpose(U)
            for i, sval in enumerate(s):
                if sval < 0.1:
                    break
                else:
                    outstr = 'lam=% .3f(%3.1f%%):'%(sval**2,100.*sval**2/sum_SVD)
                    #jmolI = 'jmol (in. orb.): mo ['
                    #jmolF = 'jmol (fin.orb.): mo ['
                    jmolI = 'mo ['
                    jmolF = 'mo ['
                    # this has to be sorted according to absolute values in descending order
                    for occind in (-abs(Uh[i])**2).argsort():
                        occ = Uh[i][occind]
                        if abs(occ) < mincoeff:
                            break
                        else:
                            outstr+=' %+.2f*%5s'%(occ,self.mos.syms[occind])
                            jmolI += ' %.3f %i'%(occ,occind+1)
                            #outstr+='%i(% .2f),'%(occind+1,occ)
                    outstr+=' -> '
                    
                    for virtind in (-abs(Vh[i])**2).argsort():
                        virt = Vh[i][virtind]
                        if abs(virt) < mincoeff:
                            break
                        else:
                            outstr+=' %+.2f*%5s'%(virt,self.mos.syms[virtind])        
                            jmolF +=' %.3f %i'%(virt,virtind+1)
                    print outstr
                    if jmolout != '' and sval**2./sum_SVD > 0.1:
                        jmfile.write(jmolI + ']\n')
                        jmfile.write('write image png "NTO%s_%io_%.2f.png"\n'%(typ,i+1,sval**2./sum_SVD))
                        jmfile.write(jmolF + ']\n')
                        jmfile.write('write image png "NTO%s_%iv_%.2f.png"\n'%(typ,i+1,sval**2./sum_SVD))
            if jmolout != '':
                jmfile.write('\n')
                
        if jmolout != '':
            jmfile.close()
                
    def print_summ(self,order=1,prop_list = ['POS','PR','CT','COH','CTnt'],*SPargs,**SPkwargs):
        """
        Print a global compact summary.
        order=-1 - order according to energy
        order=1  - order according to symmetry
        order=0  - print both
        """       
        print_list = []        
        
        #exct = open('exc_type.out','w')
        print ' Summary'
        
        outstr0 = ' st.   exc.(eV) osc. Om  '
        for prop in prop_list:
	  outstr0 += '   %4s'%prop
        outstr0 += '   config., coeff.'
        
        if order >= 0:
            print outstr0
            print len(outstr0)*'-'
        #print self.state_list
        for ist,state in enumerate(self.state_list):
            state['name'] = self.prtTD_list[ist]
            (sum_all,sum_part) = self.sum_S_part(self.prtTD_list[ist],*SPargs,**SPkwargs)
            state['POSi'] = sum((A+1) * sum(sum_part[A,B] for B in xrange(self.num_lists)) for A in xrange(self.num_lists))  / sum_all
            state['POSf'] = sum((B+1) * sum(sum_part[A,B] for A in xrange(self.num_lists)) for B in xrange(self.num_lists))  / sum_all
            state['POS'] = float((state['POSi'] + state['POSf']) / 2.)
            
            PR_I = sum_all**2. / sum((sum (sum_part[A,B] for B in xrange(self.num_lists))**2 for A in xrange(self.num_lists)))
            PR_F = sum_all**2. / sum((sum (sum_part[A,B] for A in xrange(self.num_lists))**2 for B in xrange(self.num_lists)))
            state['PR'] = float((PR_I + PR_F) / 2.)
            
            state['COH']  = (sum_all**2. / state['PR']) / sum(sum(sum_part[A,B]**2. for B in xrange(self.num_lists)) for A in xrange(self.num_lists))
            
            CTtmp = 0.
            #net_CT = 0.
            state['SEP'] = 0.
            state['SEP2'] = 0.
            for A in xrange(self.num_lists):
                for B in xrange(A+1,self.num_lists):
                    #if A!=B: # always??
                    CTtmp     += sum_part[A,B] + sum_part[B,A]
                    #net_CT += sum_part[A,B] - sum_part[B,A]
                    state['SEP']    += abs(A-B) * (sum_part[A,B] + sum_part[B,A])
                    state['SEP2']   += (A-B)**2.* (sum_part[A,B] + sum_part[B,A])                    
            state['CT'] = 1./sum_all * CTtmp
            #net_CT = 1./sum_all * net_CT
            state['SEP'] = 1./sum_all * state['SEP']
            state['SEP2'] = 1./sum_all * (state['SEP2']**0.5)
            state['CTnt'] = state['POSf'] - state['POSi']
            
            # specific for transition metal complexes
            #  the transition metal is assumed to be fragment 1
            state['MC'] = sum_part[0,0] / sum_all
            
            state['LC'] = 0.
            state['MLCT'] = 0.
            state['LMCT'] = 0.
            state['LLCT'] = 0.
            for A in xrange(1,self.num_lists):
	        state['LC'] += sum_part[A,A]
	        state['MLCT'] += sum_part[0,A]
	        state['LMCT'] += sum_part[A,0]
		for B in xrange(A+1,self.num_lists):
		  state['LLCT'] += sum_part[A,B] + sum_part[B,A]
		
            state['LC'] = state['LC'] / sum_all
            state['MLCT'] = state['MLCT'] / sum_all
            state['LMCT'] = state['LMCT'] / sum_all
            state['LLCT'] = state['LLCT'] / sum_all
            
            # net_CT and net_CT2 are the same if only two fragments are considered
            
            outstr='%6s '%state['name']
            if 'exc_en' in state:
                outstr+='% 5.3f'%state['exc_en']
            else:
                outstr+='   -   '
            if 'osc_str' in state:
                outstr+=' % 5.3f'%state['osc_str']
            else:
                outstr+='  -   '
            outstr+=' %4.3f'%sum_all
            
            for prop in prop_list:
	      outstr+=' % 4.3f'%state[prop]
	    
	    outstr+='  '
            numconf=0
            for conf in state['char']:
                try:
                    outstr+='%s->%s(%.3f)'%(conf['occ'],conf['virt'],conf['coeff'])
                except KeyError:
                    outstr+='%s->%s(%.3f)'%(conf['occ'],conf['virt'],conf['weight'])
                numconf+=1
                if numconf == 3:
                    break
                
            if order>=0: print outstr
            if order<=0: print_list.append([state['exc_en'],outstr])
            #exct.write('%3i % .6f % .6f % .6f % .6f % .6f\n'%(state['state_ind'],sum_all,state['POS'], state['PR'], state['CT'],net_CT2))
            
        #exct.close()
        
        if order <=0:
            print 'Sorted by excitation energy'
            print outstr0
            print len(outstr0)*'-'
            print_list.sort()
            for en,outstr in print_list:
                print outstr
                
    def sumsquares(self,matrix):
        """
        Sum over the squares in the matrix. Optionally partioning can be performed.
        """
        if self.partition==None:
            sum=0
            for i in xrange(len(matrix)):
                for j in xrange(len(matrix[0])):
                    sum += matrix[i,j]**2
                    
        else:
            num_part = len(self.partition)
            sum = numpy.zeros([num_part,num_part])
            for A,Abas in enumerate(self.partition):
                for B,Bbas in enumerate(self.partition):
                    for i in Abas:
                        for j in Bbas:
                            sum[A,B] += matrix[i,j]**2
                    
        return sum
                        
    def sum_S_part(self,typ,use_S=True):
        """
        Partion using the S matrix.
        If use_S=False, then S is implicitely constructed from the MO-coefficients.
        """
        if typ in self.Omega_dict:
	  return self.Omega_dict[typ]
	else:        
	  D = self.Ddict[typ]
	  if use_S:
	      DS = numpy.dot(D, self.S)
	      mat2 = numpy.dot(numpy.transpose(D.conj()), self.S)
		  
	  else:
	      # transpose because the coefficients are written along lines here
	      temp = numpy.dot(numpy.transpose(self.mos.mo_mat), D)
	      DS = numpy.dot(temp, numpy.transpose(self.mos.inv_mo_mat))
	      
		  # mat2 is (SD)^H
	      temp = numpy.dot(numpy.transpose(self.mos.mo_mat), numpy.transpose(D.conj()))
	      mat2 = numpy.dot(temp, numpy.transpose(self.mos.inv_mo_mat))
	      
	  sum_all=0
	  for i in xrange(self.num_bas):
	      for j in xrange(self.num_bas):
		  #divide this by two to comply with the definition in the paper
		  sum_all += DS[i,j]*mat2[j,i]/2.
		      
	  if self.partition!=None:
	      num_part = len(self.partition)
	      sum_part = numpy.zeros([num_part,num_part])
	      for A,Abas in enumerate(self.partition):
		  for B,Bbas in enumerate(self.partition):
		      for i in Abas:
			  for j in Bbas:
			      #sum_part[A,B] += DS[i,j]*mat2[j,i]
			      #divide this by two to comply with the definition in the paper
			      sum_part[A,B] += DS[i,j]*mat2[j,i]/2.
	  else:
	      sum_part = None
	
	  self.Omega_dict[typ] = [sum_all, sum_part]
	  return sum_all, sum_part
                        
    def mullpop(self,D,use_S=True):
        """
        Compute the Mulliken population.
        """
        # The DS matrix is constructed explicitely. For critical cases this is somewhat inefficient
        #  considering that only the trace is needed. But it is easier to implement like this.
        #  in principle one matrix multiplication could be left out and the trace constructed directly from the two matrices.
        
        if use_S:
            DS = numpy.dot(D, self.S)
        else:
            try:
                temp = numpy.dot(numpy.transpose(self.mos.mo_mat), D)
                DS = numpy.dot(temp, numpy.transpose(self.mos.inv_mo_mat))
            except ValueError:
                print 'Size of MO matrix: %i x %i'%(len(self.mos.mo_mat),len(self.mos.mo_mat[0]))
                print 'Size of D: %i x %i\n'%(len(D),len(D[0]))
                raise
            
    
        sum_all=0
        for i in xrange(self.num_bas):
                sum_all += DS[i,i]
                
        if self.partition!=None:
            num_part = len(self.partition)
            sum_part = numpy.zeros([num_part])
            for A,Abas in enumerate(self.partition):
                for i in Abas:
                        sum_part[A] += DS[i,i]
        else:
            sum_part=None
                            
        return sum_all,sum_part

    def Om_phases(self, D):
	"""
	Compute phases for plotting.
	"""
	temp = numpy.dot(numpy.transpose(self.mos.mo_mat), D)
	DS = numpy.dot(temp, numpy.transpose(self.mos.inv_mo_mat))
        
        abs_max = 0
	if self.partition!=None:
	      num_part = len(self.partition)
	      phases = numpy.zeros([num_part,num_part])
	      #phaseinv = numpy.zeros([num_part,num_part])
	      for A,Abas in enumerate(self.partition):
		  for B,Bbas in enumerate(self.partition):
		      tmpsum = 0.
		      for i in Abas:
			  for j in Bbas:
			      abs_max = max(abs_max, abs(DS[i,j]))
			      tmpsum += DS[i,j]
		      phases[A,B] = numpy.sign(tmpsum)
		      #phaseinv[A,B] = numpy.sign(tmpsum)
	else:
	      phases = None
	      
	return abs_max, phases
        
    def NTO(self,D):
        """
        Compute the natural transition orbitals.
        """
        (U,s,Vh) = numpy.linalg.svd(D)
            
        return U,s,Vh
      
    def find_exciton_pairs(self, PR_thres=1.4, tol=0.3, mx_search=2, min_orb=2):
        """
        Routine for finding exciton pairs.
        Some heuristics are applied for determining, which states are paired.
           PR_thres: minimum PR for a state to be considered
           tol: tolerances to consider an excitonic pair
           mx_search: maximum number of higher states searched for each state
              this goes after the pre-screening
           min_orb: agreement in how many orbitals
        """
        
        diabo = open('diab.out','w')
        
        COH_thres = 1.3
        
        PR_tol = tol
        CT_tol = tol
        COH_tol = tol
        POS_tol = tol/2.
        
        print "\nSearching for excitonic pairs on neighbouring chromophores ..."
        ind_list = []
        for ist,state in enumerate(self.state_list):
            # consider only states with at least some delocalization and discard states with significant coherent contributions
            if state['PR'] >= PR_thres and state['COH'] <= COH_thres:
                ind_list.append([state['exc_en'],ist])
        
        
        ind_list.sort()
        
        while(len(ind_list) > 0):
            state1 = self.state_list[ind_list.pop(0)[1]]
            match = False
            for ii,ist2 in enumerate(ind_list[:mx_search]):
                state2 = self.state_list[ist2[1]]
                
                if abs(state1['PR'] - state2['PR']) > PR_tol: continue
                if abs(state1['CT'] - state2['CT']) > CT_tol: continue
                
                # the POS value is essential, since it is used for diabatization
                offset = numpy.floor((state1['POS'] + state2['POS'])/2.)
                if min(state1['POS'],state2['POS']) < offset: continue
                if max(state1['POS'],state2['POS']) > offset+1: continue
                if abs((state1['POS']-offset) + (state2['POS']-offset) - 1.) > POS_tol: continue
                
                # check if PR is too small for the POS value
                
                # check if there is a correspondance in at least one occ. and virt. orbital each
                #   the result depends on how many configurations are considered
                #   check this: not all the orbitals have to be present
                occ1 = [conf['occ'] for conf in state1['char'][:3]]
                occ2 = [conf['occ'] for conf in state2['char'][:3]]
                virt1 = [conf['virt'] for conf in state1['char'][:3]]
                virt2 = [conf['virt'] for conf in state2['char'][:3]]
                
                orb_ct = 0
                
                if occ1[0] in occ2: orb_ct += 1
                if occ2[0] in occ1: orb_ct += 1
                if virt1[0] in virt2: orb_ct += 1
                if virt2[0] in virt1: orb_ct += 1
                
                if orb_ct >= min_orb:
                # if all tests passed, the states are an excitonic pair
                    del ind_list[ii]
                    match = True
                    break
                
            if match:
                # state1 and state2 are a pair of excitons
                print "Excitonic pair %s - %s"%(state1['name'], state2['name'])
                gap = state2['exc_en'] - state1['exc_en']
                offset = numpy.floor((state1['POS'] + state2['POS'])/2.)
                eta1 = numpy.arccos(numpy.sqrt(state1['POS'] - offset))
                eta2 = numpy.arccos(numpy.sqrt(state2['POS'] - offset))
                
                #print gap, offset, eta1, eta2
                
                H12_1 = gap * numpy.cos(eta1) * numpy.sin(eta1)
                H12_2 = gap * numpy.cos(eta2) * numpy.sin(eta2)
                
                H12_m = (H12_1 + H12_2) / 2.
                H12_e = (H12_1 - H12_2) / 2.
                
                #H12_sqrt = gap * numpy.sqrt(numpy.cos(eta1)*numpy.sin(eta1)*numpy.cos(eta2)*numpy.sin(eta2))
                
                #H12_cos = gap * numpy.cos(eta2) * numpy.cos(eta1)
                #H12_sin = gap * numpy.sin(eta2) * numpy.sin(eta1)
                
                print '  gap: %.3f eV, Hif: %.4f +/- %.4f eV'%(gap, H12_m, H12_e)
                
                site_A = state1['exc_en']*(numpy.cos(eta1)**2.+numpy.sin(eta2)**2.)/2. + state2['exc_en']*(numpy.sin(eta1)**2.+numpy.cos(eta2)**2.)/2.
                site_B = state2['exc_en']*(numpy.cos(eta1)**2.+numpy.sin(eta2)**2.)/2. + state1['exc_en']*(numpy.sin(eta1)**2.+numpy.cos(eta2)**2.)/2.
                #print state1
                #print state2
                #print H12_sqrt, site_A, site_B, site_A + site_B, state1['exc_en'] + state2['exc_en']
                print '  site energies: %.3f eV, %.3f eV'%(site_A, site_B)
                diabo.write('% .6f % .6f % .6f % .6f\n'%(H12_m, H12_e, site_A, site_B))
                
        diabo.close()
        
### I/O routines start here
                
    def rd_dens_col(self,method,tmin_list,wdir='WORK'):
        """
        Read the density and transition density matrices from Columbus iwfmt.x output
        tmin_list is a nested list that contains the density information in transmomin style.
        e.g. tmin_list = [[1,1,1,1],[1,1,1,2],...]
        """
        
        self.state_list = []
        for trans in tmin_list:
            (drt1,st1,drt2,st2)=trans
            if (drt1==drt2) and (st1==st2):
                print "State density of DRT #%i, State #%i"%(drt1,st1)
                DN = 'D%i_%i'%(drt1,st1)
                if method=='mcscf':
                    fname='%s/mcsd1fl.drt%i.st%02i.iwfmt'%(wdir,drt1,st1)
                elif method=='mrci':
                    fname='%s/cid1fl.drt%i.state%i.iwfmt'%(wdir,drt1,st1)
                self.read_iwfmt(file=fname,DN=DN,strict=True)
                self.prtD_list.append(DN)
            else:
                self.state_list.append({})
                self.state_list[-1]['state_ind'] = st2
                self.state_list[-1]['irrep'] = 'D%i'%drt2
                self.state_list[-1]['char'] = []
                print "Transition density DRT #%i, State #%i to DRT #%i, State #%i"%(drt1,st1,drt2,st2)
                DN = 'TD%i-%i'%(st1,st2)
                if method=='mcscf':
                    self.read_iwfmt(file='%s/mcsd1fl.drt%i.st%02i-st%02i.iwfmt'%(wdir,drt1,st1,st2),DN=DN,strict=True)
                    self.read_iwfmt(file='%s/mcad1fl.drt%i.st%02i-st%02i.iwfmt'%(wdir,drt1,st1,st2),DN=DN,strict=True)
                elif method=='mrci':
                    self.read_iwfmt(file='%s/cid1trfl.FROMdrt%i.state%iTOdrt%i.state%i.iwfmt'%(wdir,drt1,st1,drt2,st2),DN=DN,strict=True)
                self.prtTD_list.append(DN)
            #Dsym = self.read_iwfmt(file='mcsd1fl'+file,sym=1)
            #Dasy = self.read_iwfmt(file='mcad1fl'+file,sym=-1)
            #self.Ddict[file] = Dsym + Dasy
            #self.prtTD_list.append(file)

    def read_iwfmt(self,file, DN, sym=1, strict=False):
        """
        Read output from iwfmt.x for a 1-particle density file.
        """
	# add an offset for frozen core orbitals
        if not DN in self.Ddict:
            self.Ddict[DN] = numpy.zeros((self.num_mo,self.num_mo))
        
        try:
            in_data=False
            for line in open(file,'r').readlines():
                if ' 0.000000000000E+00' in line:
                    if len(line.split()) == 1:
                        words = last_line.split()
                        (num,lab1,ibvtyp,itypea,itypeb,ifmt,last,nipv) = [int(word) for word in words]
                        if itypea==0 and itypeb==7:
                            print ' Reading symmetric density ...'
                            in_data=True
                            sym = 1
                        elif itypea==2 and itypeb==9:
                            print ' Reading antisymmetric density ...'
                            in_data=True
                            sym = -1
                        else:
                            print 'Warning: this section does not contain a density'
                            print 'itypea: %i, itypeb: %i'%(itypea,itypeb)
                            print last_line
                            in_data=False
                elif '    ' in line:
                    #print 'Skipping:', line
                    pass
                elif in_data:
                    words = line.split()
                    
                    val = float(words[0])
                    i = int(words[1])-1
                    j = int(words[2])-1
                    #i = self.mo_map.index(int(words[1]))
                    #j = self.mo_map.index(int(words[2]))
                    
#                    print i,j,val
                    self.Ddict[DN][j,i] += val
                    
                    # the following has to be added!
                    if i != j:
                        self.Ddict[DN][i,j] += sym*val
                    
                last_line = line
                
        except IOError:
            print "IOError, file skipped:", file
            if strict:
                print " *** Please run write_den.bash!\n"
                raise
                
    def rd_tden_ricc2(self,method='ricc2',rfile='ricc2.out'):
        """
        Read the approximate MO transition density matrix from the configurations in a Turbomole output.
        """
        self.state_list = orb_compare.ret_conf_tm(in_file=rfile, method=method)
        
        for state in self.state_list:
            DN = '%i%s'%(state['state_ind'],state['irrep'])
            self.prtTD_list.append(DN)
            #print DN
            self.Ddict[DN] = numpy.zeros((self.num_mo,self.num_mo))
            
            for conf in state['char']:
                # look for the orbital in the MO file. This makes sure that the procedure works even when the MOs are reordered because of symmetry.
                iocc  = self.mos.syms.index(conf['occ'])
                ivirt = self.mos.syms.index(conf['virt'])
                # is the prefactor sqrt(2) correct?
                #   (it is cancelled out in the normalization anyway)
                self.Ddict[DN][iocc,ivirt] = 2.**.5 * conf['coeff']
                #self.Ddict[DN][conf['i_occ']-1,conf['i_virt']-1] = conf['coeff']
                
            #print self.Ddict[DN]
            
        #print state_list
                
    def rd_tden_tddft(self,method='tddft',rfile='escf.out'):
        """
        Read the TDDFT response function from a Turbomole calculation.
        Currently this works only for c1 symmetry (symmetry treatment would need to consider the different blocks).
        """
        self.state_list = orb_compare.ret_conf_tm(in_file=rfile, method=method)
        
        #print self.mos.syms
        #print self.mos.occs
        #exit(0)

        nocc={}
        nvirt={}

        for state in self.state_list:
            DN = '%i%s'%(state['state_ind'],state['irrep'])
            self.prtTD_list.append(DN)
            #print DN
            self.Ddict[DN] = numpy.zeros((self.num_mo,self.num_mo))

            occmap  = []
            virtmap = []
            for iorb, sym in enumerate(self.mos.syms):
                if state['irrep'] in sym:
                    occ = self.mos.occs[iorb]
                    if occ == 2.:
                        occmap.append(iorb)
                    elif occ == 0.:
                        virtmap.append(iorb)
                    else:
                        print " Error: invalid occupation!", occ
                        exit(5)

            nocc=len(occmap)
            nvirt=len(virtmap)

            print "\n Considering state: %s"%DN
            print "  Number of occupied orbitals:", nocc
            print "  Number of virtual orbitals:", nvirt
            print "  Tensor space dimension:", nocc*nvirt

	    if os.path.exists('sing_%s'%state['irrep']):
	      readf = 'sing_%s'%state['irrep']
	      print '  Reading information of singlet calculation from file %s'%readf
	    elif os.path.exists('trip_%s'%state['irrep']):
	      readf = 'trip_%s'%state['irrep']
	      print '  Reading information of triplet calculation from file %s'%readf
	    else:
	      print 'No file with information about the excited state (sing_a, trip_a, ...) found!'
	      exit(7)

            curr_state = 0
            for line in open(readf):
                if 'tensor space dimension' in line:
                    words = line.split()
                    space_dim = int(words[-1])
                    assert(nocc*nvirt == space_dim)
                elif 'eigenvalue' in line:
                    words = line.split()
                    curr_state = float(words[0])
                    iiocc  = 0
                    iivirt = 0
                elif curr_state == state['state_ind']:
                    words = [line[0+20*i:20+20*i] for i in xrange(4)]                    
                    for word in words:
                        self.Ddict[DN][occmap[iiocc],virtmap[iivirt]] = float(word.replace('D','E'))

                        iivirt+=1
                        if iivirt == nvirt:
                            iivirt = 0
                            iiocc += 1
                            if iiocc == nocc:
                                curr_state = 0
                                break
                elif curr_state > state['state_ind']: break

            #print DN
            #print self.Ddict[DN]

    def rd_casidawf(self,nocc=0,ncore=0,ndisc=0):
        """
        Read the output from cis_casida converted to a text file by bin2matrix.
        """
        # It has to be verified that all parts of the code are compatible with the complex coefficients
        self.state_list = orb_compare.ret_conf_tm(in_file="grad.out", method="TDDFT")
        #print self.state_list
        try:
            cawf = open("casidawf.txt",'r')
        except IOError:
            print "\n *** Cannot open casidawf.txt! ***"
            print " Did you run: "
            print "    bin2matrix casidawf > casidawf.txt\n"
            raise

        line=cawf.next().split()
        nst = int(line[0])
        dim = int(line[1])
        
        print "Casida wave function: %i states, number of entries: %i"%(nst,dim)
        
        gs = cawf.next() # discard ground state
        
        for ist in xrange(nst-1):
            state = self.state_list[ist]
            DN = '%i%s'%(state['state_ind'],state['irrep'])
            self.prtTD_list.append(DN)
            #print DN
            self.Ddict[DN] = numpy.zeros((self.num_mo,self.num_mo),dtype=numpy.complex128)
            
            coeff_temp = [float(coeff) for coeff in cawf.next().split()]
            
            # read in the complex coefficients from the CI-vector into the density matrix
            ic = 1
            try:
                for iocc in xrange(ncore,nocc):
                    #for ivirt in xrange(nocc,self.num_mo - ndisc - self.mos.num_extra_d):
                    for ivirt in xrange(nocc,self.num_mo - ndisc):
                        self.Ddict[DN][iocc,ivirt] = numpy.complex(coeff_temp[ic],coeff_temp[ic+1])
                        ic+=2
            except TypeError:
                print "Please specify ncore, ndisc and nocc in dens_ana.in!\n"
                raise
                    
            print "density matrix read in, %i entries considered"%ic
            if not ic==dim:
                print "  Inconsistency regarding the dimensions!"
                print "  Please recheck nocc, ncore, ndisc in dens_ana.in"
                exit(11)
            
    def rd_dens_no(self):
        """
        Read a density by considering a diagonal density according to the
        orbital occupations in the specified MO file.
        """
        self.prtD_list.append('NO')
        self.Ddict['NO'] = numpy.diag(self.mos.occs)
        
        self.prtD_list.append('NO_unpaired n -> min(n, 2-n)')
        self.Ddict['NO_unpaired n -> min(n, 2-n)'] = numpy.diag([min(occ, 2.-occ) for occ in self.mos.occs])
		
        self.prtD_list.append('NO_unpaired n -> n^2 (2-n)^2')
        self.Ddict['NO_unpaired n -> n^2 (2-n)^2'] = numpy.diag([occ*occ*(2-occ)*(2-occ) for occ in self.mos.occs])

    def make_lin_comb(self, lc_list):
        """
        Make linear combinations of density matrices.
        """
        for lc_ind,lc in enumerate(lc_list):
            LC='%iLC'%lc_ind
            self.Ddict[LC] = numpy.zeros((self.num_mo,self.num_mo))
            self.prtTD_list.append(LC)
            self.state_list.append({})
            self.state_list[-1]['state_ind'] = lc_ind
            self.state_list[-1]['irrep'] = 'LC'
            self.state_list[-1]['char'] = []
            self.state_list[-1]['exc_en'] = 50. + lc_ind

            for coeff,DN in lc:
                self.Ddict[LC] += coeff*self.Ddict[DN]
            
    def getS(self,gtype,Sfile='S.iwfmt',):
        """
        Read the overlap matrix.
        gtype: diag  - assume unit matrix
               iwfmt - read iwfmt file
               C     - compute from MO-coefficients
        """
        if gtype=='UM':
            self.S = numpy.diag(numpy.ones(self.num_mo))
        elif gtype=='iwfmt':
            self.S = self.read_iwfmt(file=Sfile, sym=1)
        elif gtype=='C':
            # note that mos.mo_mat = C^T in the usual notation
            self.S = numpy.dot(self.mos.inv_mo_mat,numpy.transpose(self.mos.inv_mo_mat))
            
        else:
            print "getS: gtype %s not supported!"%gtype
            exit(2)
        
        #self.Sinv = numpy.linalg.inv(self.S)
        #print self.Sinv
        
        #print 'dot:'
        #print numpy.dot(self.S, self.Sinv)
        
    def read_dens(self,rtype,tm_kwargs={},col_kwargs={},casida_kwargs={}):
        """
        Read a set of symmetric and anti-symmetric (transition) density matrices from Columbus iwfmt.x output
            or construct from Turbomole.
        """
        if rtype=="mcscf" or rtype=='mrci':
            self.rd_dens_col(method=rtype,**col_kwargs)
        elif rtype=="NO":
            self.rd_dens_no()
        elif rtype=="ricc2": 
            self.rd_tden_ricc2(method=rtype,**tm_kwargs)
        elif rtype=="tddft":
            self.rd_tden_tddft(method=rtype,**tm_kwargs)
        elif rtype=="casidawf":
            self.rd_casidawf(**casida_kwargs)
        else:
            print 'read_dens: rtype %s not implemented!'%rtype
            exit(5)
        
    def MO_AO_trans(self):
        """
        Transform all density matrices from the MO basis to the AO basis.
        """        
        for N in self.Ddict:
            temp = numpy.dot(numpy.transpose(self.mos.mo_mat), self.Ddict[N])
            self.Ddict[N] = numpy.dot(temp, self.mos.mo_mat)
        
    def get_partition(self,at_lists):
        """
        Partition the AOs according to centers. Additionally the MO-coefficients are read in.
        """
        self.num_lists = len(at_lists)
        
        self.partition=[[] for i in xrange(self.num_lists)]

        for i,bas_type in enumerate(self.mos.bas_types):
            #print i,bas_type
            at = bas_type['at_ind']
            for j,at_list in enumerate(at_lists):
                if at in at_list:
                    self.partition[j].append(i)
                    
        #print self.partition
         
