#!/usr/bin/python

# script to run density analysis
# for Turbomole:
#   delete the $freeze section out of tm2molden to get all orbitals
#   sed -i "/implicit core/d" control

# read in the configurations, yielding the transition density

import os, subprocess
import dens_stat,orb_compare,struc_linalg

inpfile='dens_ana.in'
plot = 0
print_NTO = True
print_Omega = False
print_mullpop = True
find_excitons = False
prt_order = -1
add_6d = False
rfile = None
tmin_list = []
at_lists = None
lc_list = []
ncore = 0
ndisc = 0
nocc = None
print_bond_order = False
prop_list = ['POS','PR','CT','COH','CTnt']

try:
    execfile(inpfile)
    
    rdMOargs={'add_6d':add_6d}
    
    tm_kwargs={'rfile':rfile}
    col_kwargs={'tmin_list':tmin_list}
    casida_kwargs={'nocc':nocc,'ncore':ncore,'ndisc':ndisc}
        
except IOError: # create default input file
    print 'Inputfile %s not present. It will be created now.'%inpfile
    print ' Please check the information and run this script again.\n'
    
    inpf = open(inpfile,'w')
    if os.path.exists('coord'):
        coorfile='coord'
        coortype='tmol'
        inpf.write("mo_file='molden.input'\n")
        #inpf.write("add_6d=True\n") no longer necessary
        if os.path.exists('auxbasis'):
	  inpf.write("rtype='ricc2'\n")
	  inpf.write("rfile='ricc2.out'\n")
	else:
	  inpf.write("rtype='tddft'\n")
	  inpf.write("rfile='escf.out'\n")	  
        inpf.write("prt_order=-1\n")
    elif os.path.exists('geom'):
        coorfile='geom'
        coortype='col'
        inpf.write("rtype='mrci'\n")
        inpf.write("prt_order=1\n")
        
        tmin_list = []
        if os.path.exists('WORK/mcdenin'):
            print 'Found mcdenin'
            mcd = open('mcdenin','r')
            for line in mcd:
                words = line.split()
                if len(words) == 4: tmin_list.append([int(word) for word in words])
            print 'tmin_list = ', tmin_list
    else:
        print 'Did not find geometry file coord or geom, cannot provide default input.'
        exit(1)
        
    inpf.write("print_NTO=True\n")
    inpf.write("plot=0\n")
    inpf.write("prop_list = ['POS','PR','CT','COH','CTnt']\n")
    inpf.write("#prop_list=['POSi','POSf','PR','CT','MC','LC','MLCT','LMCT','LLCT']")
        
    # read in a File geom.mol with custom bond information
    if os.path.exists('geom.mol'):
        print 'Found geom.mol. This file will be considered for bond information.'
        coorfile = 'geom.mol'
        coortype = 'mol'
    
    print 'Trying to create default partition at_lists from file %s'%coorfile
    print ' check and copy to %s'%inpfile
    struc = struc_linalg.structure()
    struc.read_file(file_path=coorfile, file_type=coortype)

    at_lists = struc.ret_partition()
    orb_compare.check_at_lists(at_lists,prt_lvl=2)
    
    inpf.close()
else: # run the program
    if not os.path.exists(mo_file):
        print 'MO input file %s not found'%mo_file
        #if rtype=='RICC2':
        #    print 'Creating MO File for Turbomole'
        #    print '   Erasing frozen core section ...'
        #    subprocess.Popen('ls') #/bin/sed -i "/implicit core/d" control')
        #    print '   Calling tm2molden ...'
        #    subprocess.Popen('tm2molden')
        
        
    densa = dens_stat.dens_ana(mo_file=mo_file,rdMOargs=rdMOargs,plot=plot)
    densa.read_dens(rtype=rtype,tm_kwargs=tm_kwargs,col_kwargs=col_kwargs,casida_kwargs=casida_kwargs)
    
    if lc_list!=[]:
        densa.make_lin_comb(lc_list)

    if not at_lists==None:
        densa.get_partition(at_lists)
    
    if print_mullpop: # this only works if state densities are available
        densa.print_all_mullpop(use_S=False)
        
    if print_Omega or (plot > 0): densa.print_all_S_part(use_S=False)
    
    if print_bond_order: densa.print_bond_orders(use_S=False)
    
    print
    densa.print_summ(order=prt_order, prop_list=prop_list,use_S=False)
    print
    
    if find_excitons: densa.find_exciton_pairs()
    
    if print_NTO: densa.print_all_NTO()
