#!/bin/bash

# reduced version where the transformation happens in the python script

rm -v WORK/*iwfmt
#export COLUMBUS=/home3/plasserf/programs/Columbus/Devel_2/C70_beta.3/Columbus

cd WORK
echo -e "aoints\n 1\n" |$COLUMBUS/iwfmt.x > aoints.iwfmt 2> err.ignore

for file in *d1fl.* *d1trfl.*
do
    echo $file
#    ln -s $file modens
#    $COLUMBUS/tran.x -m 200000000
#    unlink modens
#    cp aodens $file.ao
    echo -e "$file\n 1\n" |$COLUMBUS/iwfmt.x > $file.iwfmt 2> err.ignore
#    echo -e "$file.ao\n" |$COLUMBUS/iwfmt.x > $file.ao.iwfmt
done
