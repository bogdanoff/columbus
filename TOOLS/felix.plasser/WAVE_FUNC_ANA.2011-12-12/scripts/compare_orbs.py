#!/usr/bin/python

print"""
===================================================================================
version 1.0
author: Felix Plasser
usage: Read and compare orbitals in molden format
syntax: compare_orbs.py <st_ind> <end_ind> <ref_file> <file1> [<file2> ...]

Orbitals can be named by using the "Sym=" tags. If the file does not contain those tags, they can be put there with:
   compare_orbs.py put_sym <in_file> <out_file>
   
   naming the orbitals may be done using sed, example:
       sed -i 's/_64_A__/CnN/' molden_mo_mc.named
====================================================================================
"""

import orb_compare
import os,sys

at_lists=[]
if os.path.exists('compare_orbs.in'):
    execfile('compare_orbs.in')

if len(sys.argv) >= 1+4:        
    orb_compare.orb_compare(int(sys.argv[1]),int(sys.argv[2]),sys.argv[3],sys.argv[4:],at_lists=at_lists)
elif len(sys.argv) == 1+3 and sys.argv[1] == 'put_sym':
    orb_compare.put_sym(sys.argv[2],sys.argv[3])
else:
    print 'syntax: orb_compare.py <st_ind> <end_ind> <ref_file> <file1> [<file2> ...]'
    print '   (or: orb_compoare.py put_sym <in_file> <out_file>)'
    sys.exit()
