#!/usr/bin/perl
#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/




@blas_cfunc = qw(cdotc cdotu);
@blas_dfunc = qw(dasum dcabs1 ddot dnrm2 dzasum dznrm2);
@blas_ifunc = qw(icamax idamax isamax izamax );
@blas_lfunc = qw(lsame);
@blas_sfunc = qw(sasum scasum scnrm2  sdot  snrm2);
@blas_zfunc = qw(zdotc zdotu);

@blas_csub =  qw(caxpy ccopy cgbmv cgemm cgemv cgerc cgeru chbmv chemm chemv cher2
                 cher2k cher cherk chpmv chpr2 chpr crotg cscal csscal cswap csymm
                 csyr2k csyrk ctbmv ctbsv ctpmv ctpsv ctrmm ctrmv ctrsm ctrsv);
@blas_dsub = qw(daxpy dcopy dgbmv dgemm dgemv dger drot drotg dsbmv dscal dspmv dspr2
                dspr dswap dsymm dsymv dsyr2 dsyr2k dsyr dsyrk dtbmv dtbsv dtpmv dtpsv
                dtrmm dtrmv dtrsm dtrsv);
@blas_ssub = qw(saxpy scopy sgbmv sgemm sgemv sger srot srotg ssbmv sscal sspmv sspr2
                sspr sswap ssymm ssymv ssyr2 ssyr2k ssyr ssyrk stbmv stbsv stpmv stpsv
                strmm strmv strsm strsv xerbla);
@blas_zsub = qw(zaxpy zcopy zdscal zgbmv zgemm zgemv zgerc zgeru zhbmv zhemm zhemv zher2
                zher2k zher zherk zhpmv zhpr2 zhpr zrotg zscal zswap zsymm zsyr2k zsyrk
                ztbmv ztbsv ztpmv ztpsv ztrmm ztrmv ztrsm ztrsv);

 $allfiles=join(' ',@ARGV);

 foreach $file (@ARGV) 
 { print "DOUBLE blasconversion for file $file \n";
   if (! -f $file ){ die "could not find $file\n";}
   open FIN,"<$file";
   @findata = <FIN>;
   close FIN;
   foreach $k (@blas_dsub, @blas_dfunc, @blas_ifunc, @blas_lfunc) 

#

    { for ($i=0; $i<=$#findata; $i++) { if (! grep(/^(\*@|\#|[cC]|\*\w)/,$findata[$i]))
                                             { $findata[$i]=~s/\b$k\b/${k}_wr/ig;}
                                  #     else { print $findata[$i];}
                                          }}

   open FIN,">$file";
     $i=0;
    while (1) 
      { if ($#findata == -1) {last;}
        $i++;
        $tmp = shift (@findata );
        if (length($tmp)>73 && (! grep(/^(\*@|\#|[cC]|\*\w)/,$tmp))) 
                                   {printf "%s :  %6d  ** too long\n",$file,$i;
                              $ll = length($tmp); $fc=substr($tmp,0,1); 
                              while ($ll)
                               { print "tmp: $tmp";
                                 $tmp1 = substr $tmp,0,68;
                                 print "tmp1:$tmp1\n";
                                 print FIN "$tmp1\n"; @tmp = split(//,$tmp,69); 
                                 $tmp=pop @tmp; 
                                 $ll=length($tmp);
                                 $tmp="$fc     + " . $tmp;
                                }}
         else { print FIN $tmp;}
      }

   close FIN;
 }
  exit;

  





