!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
c   mcpc2.f  Part 2 of 2.
      subroutine driver( core, lcore, mem1, ifirst )
c
      integer lcore
      real*8 core(lcore)
c
      integer  atebyt,  forbyt
      external atebyt,  forbyt
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer    namomx
      parameter( namomx=100 )
c
      integer idim,cpt(5),nlist
c
      nlist = 6
c
      idim = lcore/(forbyt(namomx)+forbyt(2)+atebyt(1))
c
c   1:csf,2:icsf,3:invcsf,4:cs
      cpt(1) = 1
      cpt(2) = cpt(1)+atebyt(idim)
      cpt(3) = cpt(2)+forbyt(idim)
      cpt(4) = cpt(3)+forbyt(idim)
      cpt(5) = cpt(4)+forbyt(namomx*idim)
c
      if(cpt(5)-1 .gt. lcore)then
        write(nlist,1000)cpt(5),lcore
1000  format("requested mem./available mem.: ",i10," / ",i10)
         call bummer('mcpc: allocation error.  lcore=',lcore,faterr)
      endif
c
      call mcpc_run(core(cpt(1)),core(cpt(2)),core(cpt(3)),
     & core(cpt(4)),idim)
c
      return
      end
c
c****************************************************************
c
      subroutine mcpc_run(csf,icsf,invcsf,cs,idim)
c
c this program prints the csfs according to either csf number or
c indexed csf number based on wave function contribution.
c
c mcscf coefficients are read from the restart file.
c csf step vectors are read from the mdrtfile.
c
c  version log:
c  28-oct-96 state averaging partially included.  -hl
c  28-sep-91 version=2 mcdrt support. minor cleanup. -rls
c  11-aug-89 alliant,sun,titan port with cmdc changes. -rls
c  14-jul-88 unicos version (rls).
c  13-apr-88 bummer() added (rls).
c  02-sep-87 indexed csf vector print option added (rls).
c  31-aug-87 optional print of vector, mos and nos added (dhr).
c  03-aug-87 cmdc version for vax, fps, cray (rls).
c  24-jul-87 prtdet() added along with other minor changes (rls).
c  23-jul-87 srtiad() added (ron shepard).
c  21-jul-87 original version written by daniel robertson at fsu.
c
c  things-to-do in this program:
c  * generate step-vectors on-the-fly from the drt instead of reading
c    them from mcdrtfl in order to eliminate the cs(*) array.
c
      implicit none 
      logical       qdet
      integer             m2
      common /opt1/ qdet, m2
c
c
      integer    namomx
      parameter( namomx=100 )
      integer idim
c
      real*8 csf(idim)
      integer icsf(idim), invcsf(idim), modrt(namomx), syml(namomx)
c
      integer    nflmax
      parameter( nflmax=20 )
      integer         iunits
      common /cfiles/ iunits(nflmax)
      integer      nlist
      equivalence (nlist,iunits(1))
      integer      nin
      equivalence (nin,iunits(2))
      integer      nres
      equivalence (nres,iunits(3))
      character*60    flname
      common /cfname/ flname(nflmax)
c
      integer       nsym, nbpsy,    nmpsy
      common /csym/ nsym, nbpsy(8), nmpsy(8)
c
      character      slabel*4,  cstep*1,    cdet*1
      common /charc/ slabel(8), cstep(0:3), cdet(0:3)
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      character*8 program,version
      parameter (program='MCPC')
      parameter (version='5.5   ')
c
c
c     # note:  dimension of cs needs not be as large as
c     # idim*namomx but should be
c     # large enough to hold the case vectors for all the csfs.
c
      integer cs(namomx*idim)
c
      integer ndot, nact, ncsf, nrow, nwalk, smult, ldrtb, i, j
c
c     ##  parameter section
c     #  maximal No. of different DRTs
      integer maxnst
      parameter (maxnst=8)
c
c     #  maximal No. of states in one DRT
      integer mxavst
      parameter (mxavst = 50)
c
c     #  maximal total number of states
      integer maxstat
      parameter (maxstat = maxnst*mxavst)
c
c     ## common section
c
      integer navst,nst,navst_max
      real*8 heig,wavst,norm
      common /avstat/ wavst(maxnst,mxavst), heig(maxnst,mxavst),
     & navst(maxnst),nst,navst_max
c
c
      integer nrow_f,ncsf_f,nwalk_f,ssym_f
      common/drtf/ nrow_f(maxnst),ncsf_f(maxnst),
     & nwalk_f(maxnst),ssym_f(maxnst)
c
c     ## variable section
c
      integer ist,icount,idrt,iseq
      integer cptcs(maxnst),cptcsf(maxnst)
      integer ndrt(maxnst)
      equivalence (ndrt,iunits(4))
c
c     # set default unit numbers and file names.
c
      nlist = 6
      nin   = 5
      nres  = 20
      do ist=1,maxnst
         ndrt(ist)  = 30-1+ist
      enddo
c
      flname(1) = 'mcpcls'
      flname(2) = 'mcpcin'
      flname(3) = 'restart'
      call trnfln( 3, flname )
c
c   # write out the program header information
c
      call headwr(nlist,program,version)
      write(nlist,
     & '(/'' original author: Daniel Robertson, FSU'')')
      write(nlist,
     & '('' later revisions: Ron Shepard, ANL;'')')
      write(nlist,
     & '(''                  Michal Dallos, University Vienna''//)')
c
      call who2c( 'mcpc', nlist )
c
c     # read only the restart header in order to know the number of
c     # states
c
      write(nlist,
     & '(//,3x,6(1h*),2x,''File header section'',2x,6(1h*)/)')
c
      call rdvec( csf,.true.,idim )
c
c     # define the names of the required DRT files
c
      if (nst.eq.1) then
         flname(4) = 'mcdrtfl'
         call trnfln( 1, flname(4) )
      else
         do ist = 1,nst
c           # note, for multiple drt files, no translation is done.
            write(flname(4+ist-1),'(a8,i1)')'mcdrtfl.',ist
         enddo
      endif
c
c     #  set the counter for the fields csf,cs
c
      cptcs(1) = 1
      cptcsf(1) = 1
      do ist=1,nst
         cptcsf(ist+1) = cptcsf(ist) + ncsf_f(ist)*navst(ist)
      enddo
c
c     # open the required DRT file(s)
c
c     # use preconnected units 5 (stdin) and 6 (stdout). -rls
c
      qdet = .false.
      m2   = 0
      call ibummr( nlist )
c
      write(nlist,
     & '(//,3x,6(1h*),2x,''DRT info section'',2x,6(1h*))')
      do ist = 1,nst
      open(unit=ndrt(ist),file=flname(4+ist-1),status='old')
      write(nlist,'(/'' Informations for the DRT no. '',i2)')ist
c
      call rdhdrt(
     & ndot,   nact,   nsym,   ncsf,
     & nrow,   nwalk,  smult,  ldrtb,
     & csf ,   ndrt(ist),ist    )
c
      if ( ncsf .gt. idim ) then
         write(nlist,*)'ncsf,idim=',ncsf,idim
         call bummer('mcpc: ncsf=',ncsf,faterr)
      elseif ( ldrtb .gt. idim ) then
         write(nlist,*)'ldrtb,idim=',ldrtb,idim
         call bummer('mcpc: ldrtb=',ldrtb,faterr)
      elseif ( nact*ncsf .gt.namomx*idim  ) then
         write(nlist,*)'nact*ncsf, namomx*idim',nact*ncsf, namomx*idim
         call bummer('mcpc: nact*ncsf=',nact*ncsf,faterr)
      elseif ( nact .gt. namomx ) then
         write(nlist,*)'nact,namomx',nact,namomx
         call bummer('mcpc: nact=',nact,faterr)
      endif
c
c     #  set the counter for the fields csf
c
         cptcs(ist+1) = cptcs(ist) + nact*ncsf_f(ist)
c
      write(nlist,
     & '(/3x,''***  Informations from the DRT number: '',i3,/)')
     & ist
c
      call rdstep(
     & cs(cptcs(ist)),     modrt,  syml,   ndot,
     & nact,   ncsf,   nrow,   nwalk,
     & csf,    ldrtb,  slabel, ndrt(ist) )
c
      close( unit=ndrt(ist) )
c
      enddo ! ist ---> nst
c
      call rdvec( csf,.false.,idim )
c
c     # sort the csf coefficients into decreasing order of magnitude.
c     # on return, icsf(i) gives the i'th most important contribution.
c
      icount = 1
      do ist=1,nst
         do j=1,navst(ist)
            do  i = 1, ncsf_f(ist)
               icsf(icount +i-1) = i
            enddo ! i
c
            call srtiad( ncsf_f(ist), csf(icount), icsf(icount) )
c
c     # compute the inverse csf mapping vector.
            do i = 1, ncsf_f(ist)
               invcsf( icount+ icsf(i) -1) = i
            enddo
            icount = icount + ncsf_f(ist)
         enddo !j
      enddo ! ist
c
      if (nst.eq.1) then
      write(nlist,6053)nst
      else
      write(nlist,6054)nst
      endif
6053  format(/' MCSCF calculation performmed for ',1x,i2,1x,'symmetry.')
6054  format(/' MCSCF calculation performmed for ',1x,i2,1x,
     & 'symmetries.')

      write(nlist,'(/,1x,"State averaging:")')
      write(nlist,6056)
6056  format(' No,  ssym, navst, wavst')
      do ist=1,nst
      write(nlist,'(i3,3x,a3,3x,i3,3x,8(f6.4,1x))')
     & ist,slabel(ssym_f(ist)),navst(ist),
     & (wavst(ist,i),i=1, navst(ist))
      enddo
c
c    #  select the DRT of interest
900   idrt = 1
      call inpi(idrt,'Input the DRT No of interest:')
      if ((idrt.gt.nst).or.(idrt.le.0)) go to 900
c
c    #  select the state of the DRT of interest
      write(nlist,
     & '(/"In the DRT No.:",1x,i1,1x,"there are",1x,i2,1x,"states.")')
     & idrt,navst(idrt)
910   iseq = 1
      call inpi(iseq,'Which one to take?')
      if ((iseq.gt.navst(idrt)).or.(iseq.le.0)) go to 910
c
c     #  now comes the print section
c     write(nlist,6200) navst(idrt),slabel(idrt)
      write(nlist,6200) iseq,slabel(idrt)
6200  format(/1x,
     & 'The CSFs for the state No',1x,i2,1x,'of the symmetry',1x,a3,1x,
     & ' will be printed'/,1x,
     & 'according to the following print options :')
c
5     continue
      write(nlist,6210)
6210  format(/
     & ' 1) print csf info by sorted index number.'/
     & ' 2) print csf info by contribution threshold.'/
     & ' 3) print csf info by csf number.'/
     & ' 4) set additional print options.'/
     & ' 5) print the entire sorted csf vector.'/
     & ' 6) print the entire csf vector.'/
     & ' 7) print the mcscf molecular orbitals.'/
     & ' 8) print the mcscf natural orbitals and occupation numbers.'/
     & ' 9) export wave function files for cioverlap (all states).'/
     & ' 0) end.' )
c
      j = 0
      call inpi(j,'input menu number')
c
      if ( j .eq. 0 ) then
         call bummer('normal termination',0,3)
         stop 'end of mcpc'
      elseif ( j .eq. 1 ) then
         call prsidx(csf(cptcsf(idrt)+(iseq-1)*ncsf_f(idrt)),
     &    icsf(cptcsf(idrt)+(iseq-1)*ncsf_f(idrt)),cs(cptcs(idrt)),
     &    modrt,syml,nact,ncsf_f(idrt))
      elseif ( j .eq. 2 ) then
         call prtol(csf(cptcsf(idrt)+(iseq-1)*ncsf_f(idrt)),
     &    icsf(cptcsf(idrt)+(iseq-1)*ncsf_f(idrt)),cs(cptcs(idrt)),
     &    modrt,syml,nact,ncsf_f(idrt))
      elseif ( j .eq. 3 ) then
         call prnum(csf(cptcsf(idrt)+(iseq-1)*ncsf_f(idrt)),
     &    cs(cptcs(idrt)),modrt,syml,nact,ncsf_f(idrt))
      elseif ( j .eq. 4 ) then
         call setopt(smult)
      elseif ( j .eq. 5 ) then
         call privec(csf(cptcsf(idrt)+(iseq-1)*ncsf_f(idrt)),
     &    icsf(cptcsf(idrt)+(iseq-1)*ncsf_f(idrt)),ncsf_f(idrt))
      elseif ( j .eq. 6 ) then
         call prvec(csf(cptcsf(idrt)+(iseq-1)*ncsf_f(idrt)),
     &    ncsf_f(idrt))
      elseif ( j .eq. 7 ) then
         call prorb( 0 )
      elseif ( j .eq. 8 ) then
         call prorb( 1 )
cjp
      elseif ( j .eq. 9 ) then
         call prnum2(csf(cptcsf(idrt)),
     &    cs(cptcs(idrt)),modrt,syml,nact,
     &ncsf_f(idrt),navst(idrt),smult)
cjp
      else
         write(nlist,*) 'enter a valid menu number?'
      endif
      go to 5
c
      end
c deck block data
      block data
       implicit none
      character      slabel*4,  cstep*1,    cdet*1
      common /charc/ slabel(8), cstep(0:3), cdet(0:3)
c
      data slabel /
     & 'sym1', 'sym2', 'sym3', 'sym4',
     & 'sym5', 'sym6', 'sym7', 'sym8'   /
c
      data cstep / '0', '1', '2', '3' /
      data cdet  / '.', '+', '-', '#' /
c
      end
c deck prtol
      subroutine prtol(
     & csf,    icsf,   cs,     modrt,
     & syml,   nact,   ncsf )
c
c prints csf info according to a given threshold contribution.
c
       implicit none 
       integer    nflmax
      parameter( nflmax=20 )
      integer         iunits
      common /cfiles/ iunits(nflmax)
      integer      nlist
      equivalence (nlist,iunits(1))
c
      integer nact, ncsf
      real*8 csf(ncsf)
      integer icsf(ncsf), cs(nact,ncsf), modrt(nact), syml(nact)
c
      integer i, id
      real*8 tol
c
      write(nlist,'(1x,a)')
     & 'csfs will be printed based on coefficient magnitudes.'
c
1     continue
      tol = (0)
      call inpf(tol,'input the coefficient threshold (end with 0.)')
c
      if ( tol .eq. (0) ) then
         return
      elseif ( (tol.lt.(0)) .or. (tol.gt.(1)) ) then
         write(nlist,*) 'input a number between 0.0 and 1.0'
      else
         call prcsfh( nact, syml, modrt )
         do 2 i = 1, ncsf
            id = icsf(i)
            if ( abs(csf(id)) .lt. tol ) go to 3
            call prtcsf( id, csf(id), cs(1,id), nact )
2        continue
3        continue
      endif
      go to 1
c
      end
c deck prsidx
      subroutine prsidx( csf, icsf, cs, modrt, syml, nact, ncsf )
c
c  prints csf info according to their sorted index.
c
       implicit none
      integer    nflmax
      parameter( nflmax=20 )
      integer         iunits
      common /cfiles/ iunits(nflmax)
      integer      nlist
      equivalence (nlist,iunits(1))
c
      integer nact, ncsf
      real*8 csf(ncsf)
      integer icsf(ncsf),modrt(nact),cs(nact,ncsf),syml(nact)
c
      integer istart, iend, i, id
      integer iarray(2)
c
      write(nlist,'(1x,a)')
     & 'csfs will be printed by sorted index ranges.'
c
10    iarray(1)=0
      iarray(2)=0
      call inpiv(2,iarray,'input istart,iend (end with 0,0)')
      istart = iarray(1)
      iend   = iarray(2)
      if (istart .eq. 0 ) then
         return
      elseif ( (istart.gt.ncsf) .or. (istart.lt.0) ) then
         write(nlist,*)' 0 < istart <', ncsf
      else
         call prcsfh( nact, syml, modrt )
         do 15 i = istart, max( istart, min( iend, ncsf) )
            id = icsf(i)
            call prtcsf( id, csf(id), cs(1,id), nact )
15       continue
      endif
      go to 10
c
      end
      subroutine prnum( csf, cs, modrt, syml, nact, ncsf )
c
c  prints csf info according to csf index.
c
       implicit none
      integer    nflmax
      parameter( nflmax=20 )
      integer         iunits
      common /cfiles/ iunits(nflmax)
      integer      nlist
      equivalence (nlist,iunits(1))
c
      integer nact, ncsf
      real*8 csf(ncsf)
      integer modrt(nact), cs(nact,ncsf), syml(nact)
c
      integer istart, iend, i
      integer iarray(2)
c
      write(nlist,'(1x,a)') 'csfs will be printed by index ranges.'
c
10    iarray(1) = 0
      iarray(2) = 0
      call inpiv(2,iarray,'input istart,iend (end with 0,0)')
      istart = iarray(1)
      iend   = iarray(2)
      if (istart .eq. 0 ) then
         return
      elseif ( (istart.gt.ncsf) .or. (istart.lt.0) ) then
         write(nlist,*)' 0 < istart <', ncsf
      else
         call prcsfh( nact, syml, modrt )
         do 15 i = istart, max( istart, min( iend, ncsf ) )
            call prtcsf( i, csf(i), cs(1,i), nact )
15       continue
      endif
      go to 10
c
      end
cjp
      subroutine prnum2(csf,cs,modrt,syml,nact,ncsf,nstates,smult)
c
c  prints csf info according to csf index.
c
       implicit none
      integer smult
      integer    nflmax
      parameter( nflmax=20 )
      integer         iunits
      common /cfiles/ iunits(nflmax)
      logical       qdet
      integer             m2
      common /opt1/ qdet, m2
      integer      nlist
      equivalence (nlist,iunits(1))
c
      integer nact, ncsf
      real*8 csf(ncsf)
      integer modrt(nact), cs(nact,ncsf), syml(nact)
c
      integer  i,j
c
      integer    namomx
      parameter( namomx=100 )
      integer reclunit
      parameter (reclunit=1)

      integer nstates,myroot,eivecfile,slaterfile,jirec,nelec,orbnum
      common/jiri/myroot,eivecfile,slaterfile,jirec,nelec,orbnum(namomx)
      integer*4 nstates4,jirec4


      qdet=.true.
      m2=smult-1

      eivecfile=98
      slaterfile=99
      jirec=0

      open(file='eivectors',unit=eivecfile,access='direct',
     & form='unformatted',recl=8/reclunit)

c compute number of active electrons from the step vector of first csf and open slaterfile
      nelec=0
      do i=1,nact
       nelec = nelec + (cs(i,1)+1)/2
      enddo
      write(6,*)' number of active electrons = ',nelec
      write(6,*)' 2*Ms = ',m2

      open(file='slaterfile',unit=slaterfile,access='direct',
     &      form='unformatted',recl=2*nelec/reclunit)


c copy orbital list to common block to be available in deeper routines
      do i=1,nact
       orbnum(i)=modrt(i)
      enddo


      do myroot=1,nstates
      write(nlist,*)
      write(nlist,*) '-------------------------------------------------'
      write(nlist,*) 'Processing state ',myroot
      write(nlist,*)

c
c     call prcsfh( nact, syml, modrt )
      do  i = 1,ncsf
            call prtcsf( i, csf(i+(myroot-1)*ncsf), cs(1,i), nact )
      enddo
      if(myroot.eq.1) then
      close(slaterfile)
      open(file='eivectorshead',unit=slaterfile,access='direct',
     & form='unformatted',recl=8/reclunit)
      nstates4 = nstates
      jirec4=jirec
      write(slaterfile,rec=1) nstates4,jirec4
      close(slaterfile)
      endif

      enddo

      stop 'all done'
c
      end
cjp
c deck prcsfh
      subroutine prcsfh( nact, syml, modrt )
c
c  print the header info for subsequent prtcsf() calls.
c
c  28-sep-91 written by ron shepard.
c
       implicit none
      integer    nflmax
      parameter( nflmax=20 )
      integer         iunits
      common /cfiles/ iunits(nflmax)
      integer      nlist
      equivalence (nlist,iunits(1))
c
      character      slabel*4,  cstep*1,    cdet*1
      common /charc/ slabel(8), cstep(0:3), cdet(0:3)
c
      integer nact
      integer syml(nact), modrt(nact)
c
      integer i
c
      write(nlist,
     & '(/1x,"List of active orbitals:"/,12(i3,a4))')
     &  (modrt(i),slabel(syml(i)),i=1,nact)
      write(nlist,6200)
c
      return
6100  format(1x,a,(10i5))
6110  format(1x,a,(10(1x,a4)))
6200  format(/3x,'csf', 7x, 'coeff', 7x, 'coeff**2', 4x, 'step(*)'/
     & 2x, 5('-'),  2x,12('-'), 2x,12('-'), 2x,12('-') )
      end
c deck prtcsf
      subroutine prtcsf( id, csf, cs, nact )
c
c  print the info for csf number id.
c
c  28-sep-91 prtcsf/prcsfh split. -rls
c
       implicit none
      integer    nflmax
      parameter( nflmax=20 )
      integer         iunits
      common /cfiles/ iunits(nflmax)
      integer      nlist
      equivalence (nlist,iunits(1))
c
      logical       qdet
      integer             m2
      common /opt1/ qdet, m2
c
      integer id, nact
      real*8 csf
      integer cs(nact)
c
      integer i
c
      character      slabel*4,  cstep*1,    cdet*1
      common /charc/ slabel(8), cstep(0:3), cdet(0:3)
c
      write(nlist,6100) id, csf, csf**2, (cstep(cs(i)),i=1,nact)
c
      if ( qdet ) call prtdet( nact, m2, cs, csf )
c
      return
6100  format(1x,i6,2f14.10,(2x,100a1))
      end
c deck inpi
      subroutine inpi( j, txt )
c
c  inputs an integer
c
       implicit none
      integer    nflmax
      parameter( nflmax=20 )
      integer         iunits
      common /cfiles/ iunits(nflmax)
      integer      nlist
      equivalence (nlist,iunits(1))
      integer      nin
      equivalence (nin,iunits(2))
c
      integer j
      character*(*) txt
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer numerr, ios
c
      numerr = 0
10    write(nlist,6000) txt, j
      read(nin,*,iostat=ios) j
      if ( ios .lt. 0 ) then
         call bummer('inpi: ios=',ios,faterr)
      elseif ( ios .gt. 0 ) then
         numerr = numerr + 1
         if ( numerr .ge. 10 ) call bummer(
     &    'inpi: numerr=',numerr,faterr)
         write(nlist,*) 'input an integer'
         go to 10
      else
         return
      endif
*@ifdef format
6000  format(/1x,a,' [',i3,']:',$)
*@else
*6000  format(/1x,a,' [',i3,']:')
*@endif
      end
c deck inpiv
      subroutine inpiv( n, j, txt )
c
c  inputs an integer array
c
       implicit none
      integer    nflmax
      parameter( nflmax=20 )
      integer         iunits
      common /cfiles/ iunits(nflmax)
      integer      nlist
      equivalence (nlist,iunits(1))
      integer      nin
      equivalence (nin,iunits(2))
c
      integer n
      character*(*) txt
      integer j(n)
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer numerr, ios
c
      numerr = 0
10    write(nlist,6000) txt
      read(nin,*,iostat=ios) j
      if ( ios .lt. 0 ) then
         call bummer('inpiv: ios=',ios,faterr)
      elseif ( ios .gt. 0 ) then
         numerr = numerr + 1
         if ( numerr .ge. 10 ) call bummer(
     &    'inpiv: numerr=', numerr, faterr )
         write(nlist,*)'input an integer array'
         go to 10
      else
         return
      endif
c
*@ifdef format
6000  format(/1x,a,':',$)
*@else
*6000  format(/1x,a,':')
*@endif
      end
c deck inpf
      subroutine inpf( f, txt )
c
c  read in a floating point number.
c
       implicit none
      integer    nflmax
      parameter( nflmax=20 )
      integer         iunits
      common /cfiles/ iunits(nflmax)
      integer      nlist
      equivalence (nlist,iunits(1))
      integer      nin
      equivalence (nin,iunits(2))
c
      character*(*) txt
      real*8 f
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer numerr, ios
c
      numerr = 0
10    write(nlist,6000) txt, f
      read(nin,*,iostat=ios) f
      if ( ios .lt. 0 ) then
         call bummer('inpf: ios=',ios,faterr)
      elseif ( ios .gt. 0 ) then
         numerr = numerr + 1
         if ( numerr .ge. 10 ) call bummer(
     &    'inpf: numerr=', numerr, faterr )
         write(nlist,*)'input a floating point number'
         go to 10
      else
         return
      endif
c
*@ifdef format
6000  format(/1x,a,' [',f7.4,']:',$)
*@else
*6000  format(/1x,a,' [',f7.4,']:')
*@endif
      end
c deck inpcv
      subroutine inpcv( cj, n, txt )
c
c  inputs a character array
c
       implicit none
      integer    nflmax
      parameter( nflmax=20 )
      integer         iunits
      common /cfiles/ iunits(nflmax)
      integer      nlist
      equivalence (nlist,iunits(1))
      integer      nin
      equivalence (nin,iunits(2))
c
      integer n
      character*(*) cj(n)
      character*(*) txt
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer numerr, ios
c
      numerr = 0
10    write(nlist,6000) txt, cj
      read(nin,*,iostat=ios) cj
      if ( ios .lt. 0 ) then
         call bummer('inpcv: ios=',ios,faterr)
      elseif ( ios .gt. 0 ) then
         numerr = numerr + 1
         if ( numerr .ge. 10 ) call bummer(
     &    'inpcv: numerr=',numerr,faterr)
         write(nlist,*) 'input a character array, using list-directed '
     &    // 'input, of length', n
         go to 10
      else
         return
      endif
6000  format(/1x,a
     & /' (use list-directed input) original values:'/(1x,a))
      end
c deck rdvec
      subroutine rdvec( csf,iflag,idim )
c
c  read the restart file created by program mcscf, return the
c  csf coefficients
c
       implicit none
      integer    nflmax
      parameter( nflmax=20 )
      integer         iunits
      common /cfiles/ iunits(nflmax)
      integer      nlist
      equivalence (nlist,iunits(1))
      integer      nin
      equivalence (nin,iunits(2))
      integer      nres
      equivalence (nres,iunits(3))
c
      character*60    flname
      common /cfname/ flname(nflmax)
c
      real*8 csf(*)
c
      integer ntitle, ios, nsym, ninf, ntol, nmot, nbmt, i,
     & nread, ncsf
      logical qfskip, qconvg
      integer nmpsy(8), nbpsy(8)
      character*80 title(20)
      character*8 label
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      data qfskip / .true. /
c
c     ##  parameter section
c     #  maximal No. of different DRTs
      integer maxnst
      parameter (maxnst=8)
c
c     #  maximal No. of states in one DRT
      integer mxavst
      parameter (mxavst = 50)
c
c     #  maximal total number of states
      integer maxstat
      parameter (maxstat = maxnst*mxavst)
c
c     ## common section
c
      integer navst,nst,navst_max
      real*8 heig,wavst,norm
      common /avstat/ wavst(maxnst,mxavst), heig(maxnst,mxavst),
     & navst(maxnst),nst,navst_max
c
      integer nrow_f,ncsf_f,nwalk_f,ssym_f
      common/drtf/ nrow_f(maxnst),ncsf_f(maxnst),
     & nwalk_f(maxnst),ssym_f(maxnst)
c
c
      logical iflag
      integer j,maxnavst,ncsftot,ist,idim
c
      open(unit=nres,file=flname(3),form='unformatted',status='old')
c
c     # get the header info from the restart file.
c
      ntitle = 0
      rewind nres
      read(nres,iostat=ios)
     &   nsym,ninf,ntol,nmot,nbmt,nst,
     &  (ncsf_f(i),i=1,nst),qconvg,ntitle,
     &  nmpsy,nbpsy,(title(i),i=1,ntitle),
     &  (navst(i),i=1,nst),maxnavst,
     &  ((wavst(i,j),i=1,nst),j=1,maxnavst),
     &  ((heig(i,j),i=1,nst),j=1,maxnavst)
c
      if ( ios .ne. 0 ) call bummer('rdvec 1: ios=',ios,faterr)
c
      if (iflag) then
      write(nlist,'(1x,''Headers form the restart file:'')')
         do i=1,ntitle
         write(nlist,'(4x,a)')title(i)
         enddo
      endif
c
c     # return after header read
      if (iflag) return
c
c     # read and print the convergence information.
c
      call rdlv( nres, ninf, nread, csf, 'coninf', qfskip )
      if ( ninf .ne. nread ) then
          write(nlist,*)
     &    '*** error *** rdvec: incorrect number of elements'
          call bummer('rdvec 2: ninf=',ninf,faterr)
      endif
c
c
      write(nlist,
     & '(//,3x,6(1h*),''  MCSCF convergence information:  '',6(1h*)/)')
      if ( qconvg ) then
         write(nlist,6100)
     &    'MCSCF convergence criteria were satisfied.'
      else
         write(nlist,6100)
     &    'MCSCF convergence criteria were not satisfied.'
      endif
      write(nlist,6030) (csf(i), i=1,ninf)
6030  format(
     & /' mcscf energy= ',f16.10,4x,'nuclear repulsion= ',f16.10
     & /' demc=         ',f16.10,4x,'wnorm=             ',f16.10
     & /' knorm=        ',f16.10,4x,'apxde=             ',f16.10
     & /(1x,e16.8))
c
c     # search restart file for label and get the csf coefficients.
c
      ncsftot = 0
      do ist=1,nst
         do i=1,navst(ist)
            ncsftot = ncsftot+ncsf_f(ist)
         enddo
      enddo
      if (ncsftot.gt.idim)
     & call bummer('rdvec 3: Not enough memory allocated',
     & idim,faterr)
c
      label='hvec'
      call rdlv( nres, ncsftot, ncsf, csf, label, qfskip )
c
c
      close(unit=nres)
c
6100  format(1x,a)
      return
      end
cdek rdlv
      subroutine rdlv( iunit, n, na, a, alab, qfskip )
c
c  search unit i for label alab and read the corresponding vector.
c
c  iunit  = unit number.
c  n      = expected vector length.
c  na     = actual vector length.  na=-1 for unsatisfied search.
c  a(*)   = if(na.le.n) the vector is returned.
c  alab   = character label used to locate the correct record.
c           first 8 characters only are used.
c  qfskip = .true. if first record should be skipped.
c
       implicit none
      integer    nflmax
      parameter( nflmax=20 )
      integer         iunits
      common /cfiles/ iunits(nflmax)
      integer      nlist
      equivalence (nlist,iunits(1))
c
      integer iunit, n, na
      logical qfskip
      real*8 a(n)
      character*(*) alab
c
      character*8 label, inlab
c
c     # convert input label to upper case for subsequent comparisons.
c
      inlab=alab
      call allcap( inlab )
c
c     # search for the correct label.
c
      na=0
      rewind iunit
      if ( qfskip ) read(iunit)
10    read(iunit,end=20) label, na
      call allcap( label )
      if ( inlab .ne. label ) then
         read(iunit)
         go to 10
      endif
c
c     # correct label.  check length.
c
      if ( na .ne. n ) write(nlist,6010) alab, na, n
6010  format(/' rdlv: alab=',a,' na,n=',2i10)
      if ( na .le. n ) call seqr( iunit, na, a )
      return
c
c     # unsatisfied search for alab.
20    write(nlist,6020) alab
6020  format(/' rdlv: record not found. alab=',a)
      na = -1
      return
      end
      subroutine seqr( iunit, n, a )
c
c     # sequential read of a(*) from unit iunit.
c
       implicit none 
       integer iunit, n
      real*8 a(n)
c
      read(iunit) a
c
      return
      end
c deck rdhdrt
      subroutine rdhdrt(
     & ndot,   nact,   nsym,   ncsf,
     & nrow,   nwalk,  smult,  lenbuf,
     & buf,    ndrt,   ist )
c
c  read the header information from the drt file.
c
c  28-sep-91 vrsion=2 support added. -rls
c  written by ron shepard.
c
       implicit none
      integer    nflmax
      parameter( nflmax=20 )
      integer         iunits
      common /cfiles/ iunits(nflmax)
      integer      nlist
      equivalence (nlist,iunits(1))
      integer      ndrt
c
      integer ndot, nact, nsym, nrow, smult, nwalk, ncsf, lenbuf
      integer buf(50)
c
      integer vrsion, nhdint, ierr, nelt, nela,ist
      character*80 title
c
c     ##  parameter section
c     #  maximal No. of different DRTs
      integer maxnst
      parameter (maxnst=8)
c
      integer nrow_f,ncsf_f,nwalk_f,ssym_f
      common/drtf/ nrow_f(maxnst),ncsf_f(maxnst),
     & nwalk_f(maxnst),ssym_f(maxnst)
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      character      slabel*4,  cstep*1,    cdet*1
      common /charc/ slabel(8), cstep(0:3), cdet(0:3)
c
c
      rewind ndrt
c
      read(ndrt, '(a)', iostat=ierr) title
      if ( ierr .ne. 0 ) then
         call bummer('rdhdrt: title ierr=',ierr,faterr)
      endif

      write(nlist,'(1x,''Header form the DRT file: ''/,4x,a)')title
c
c     # first header record.
      call rddbl( ndrt, 3, buf, 3 )
c
      vrsion = buf(1)
      lenbuf = buf(2)
      nhdint = buf(3)
c
      if ( vrsion .ne. 2 ) then
         write(nlist,*)
     &    '*** warning: rdhead: drt version .ne. 2 *** vrsion=',vrsion
      endif
c
c     # second header record.
      call rddbl( ndrt, lenbuf, buf, nhdint )
c
      ndot  = buf(1)
      nact  = buf(2)
      nsym  = buf(3)
      nrow  = buf(4)
      ssym_f(ist)  = buf(5)
      smult = buf(6)
      nelt  = buf(7)
      nela  = buf(8)
      nwalk = buf(9)
      ncsf  = buf(10)
c
      write(nlist,'(1x,"Molecular symmetry group:",t30,a4)')
     & slabel(ssym_f(ist))
      write(nlist,'(1x,"Total number of electrons:",t29,i4)')nelt
      write(nlist,'(1x,"Spin multiplicity:",t30,i3)')smult
      write(nlist,'(1x,"Number of active orbitals:",t30,i3)')nact
      write(nlist,'(1x,"Number of active electrons:",t30,i3)')nela
      write(nlist,'(1x,"Total number of CSFs:",t26,i7)')ncsf
c
cmd   write(nlist,6100)
cmd  & ndot, nact, nsym,  nrow, ssym_f(ist), smult,
cmd  & nelt, nela, nwalk, ncsf, lenbuf
6100  format(/
     & ' ndot=  ',i6,4x,'nact=  ',i6,4x,'nsym=  ',i6,4x,'nrow=  ',i6/
     & ' ssym=  ',i6,4x,'smult= ',i6,4x,'nelt=  ',i6,4x,'nela=  ',i6/
     & ' nwalk= ',i6,4x,'ncsf=  ',i6,4x,'lenbuf=',i6)
c
      return
      end
c deck rdstep
      subroutine rdstep(
     & cs,     modrt,    syml,   ndot,
     & nact,   ncsf,   nrow,   nwalk,
     & buf,    lenbuf, slabel, ndrt )
c
c  read the orbital indices and step vectors from the drt file.
c
c  28-sep-91 vrsion=2 support added. -rls
c
       implicit none
      integer    nflmax
      parameter( nflmax=20 )
      integer         iunits
      common /cfiles/ iunits(nflmax)
      integer      nlist
      equivalence (nlist,iunits(1))
      integer      ndrt
c
      integer       nsym, nbpsy,    nmpsy
      common /csym/ nsym, nbpsy(8), nmpsy(8)
c
      integer ndot, nact, ncsf, nrow, nwalk, lenbuf
      integer buf(lenbuf), modrt(nact), syml(nact)
      integer cs(nact,ncsf)
      character*4 slabel(8)
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer i, j, ierr, nlevel
      integer*4 buf32(8)
      character*20 cfmt
c
      nlevel = nact + 1
c
      write(nlist,*)
c
c     # slabel(*)...
      call rddbl( ndrt, lenbuf, buf32, nsym )
      do 10 i = 1, nsym
         write( slabel(i), fmt='(a4)' ) buf32(i)
 10    continue
      write(nlist,'(1x,"Symmetry orbital summary:")')
      write(nlist,'(1x,"Symm.blocks:",t20,8(2x,i2,2x))')(i,i=1,nsym)
      write(nlist,'(1x,"Symm.labels:",t20,8(2x,a4))')
     & (slabel(i),i=1,nsym)
c
      if ( ndot .gt. 0 ) then
c        # doub(*)...
         call rddbl( ndrt, lenbuf, buf(ndot+1), ndot )
c        # symd(*)...
         call rddbl( ndrt, lenbuf, buf, ndot )
         write(nlist,
     & '(/1x,"List of doubly occupied orbitals:"/,12(i3,a4))')
     &  (buf(ndot+i),slabel(buf(i)),i=1,ndot)
      endif
c
c     # modrt(*)...
      if ( nact .gt. 0 ) then
      call rddbl( ndrt, lenbuf, modrt, nact )
c
c     # syml(*)...
      call rddbl( ndrt, lenbuf, syml, nact )
      write(nlist,
     & '(/1x,"List of active orbitals:"/,12(i3,a4))')
     & (modrt(i),slabel(syml(i)),i=1,nact)
      endif
c
c     # nj(*)...
      call rddbl( ndrt, lenbuf, buf, -nlevel )
c
c     # njskp(*)...
      call rddbl( ndrt, lenbuf, buf, -nlevel )
c
c     # a(*)...
      call rddbl( ndrt, lenbuf, buf, -nrow )
c
c     # b(*)...
      call rddbl( ndrt, lenbuf, buf, -nrow )
c
c     # l(*)...
      call rddbl( ndrt, lenbuf, buf, -4*nrow )
c
c     # y(*,*,*)...
      call rddbl( ndrt, lenbuf, buf, -4*nrow*nsym )
c
c     # xbar(*,*)...
      call rddbl( ndrt, lenbuf, buf, -nrow*nsym )
c
c     # xp(*,*)...
      call rddbl( ndrt, lenbuf, buf, -nrow*nsym )
c
c     # z(*,*)...
      call rddbl( ndrt, lenbuf, buf, -nrow*nsym )

c     # limvec(*)...
      call rddbl( ndrt, lenbuf, buf, -nwalk )
c
c     # r(*)...
      call rddbl( ndrt, lenbuf, buf, -nwalk )
c
c     # step(*,*)...
      if ( nact.gt.0 ) then
      read(ndrt,'(a)',iostat=ierr) cfmt
      if ( ierr .ne. 0 ) then
         write(nlist,*) 'rddrt: error in reading step vectors '
     &    // 'from the drt file.'
         write(nlist,*) 'if necessary, regenerate the drt file with '
     &    // 'the step-vector option enabled.'
ctm should be fatal but mcdrt bug
ctm      call bummer('rddrt: ierr=', ierr, faterr )
         call bummer('normal termination',0,3)
         stop
      endif
c
      do 70 i = 1, ncsf
         read(ndrt,cfmt) (cs(j,i), j=1,nact)
70    continue
      endif
c
      close(unit=ndrt)
      return
6100  format(1x,a,(10i5))
6110  format(1x,a,(10(1x,a4)))
      end
c deck setopt
      subroutine setopt( smult )
c
c  set additional printing options.
c
       implicit none
      integer    nflmax
      parameter( nflmax=20 )
      integer         iunits
      common /cfiles/ iunits(nflmax)
      integer      nlist
      equivalence (nlist,iunits(1))
c
      logical       qdet
      integer             m2
      common /opt1/ qdet, m2
c
      character      slabel*4,  cstep*1,    cdet*1
      common /charc/ slabel(8), cstep(0:3), cdet(0:3)
c
      integer smult
c
      integer j
      real*8 dms
c
      real*8     half
      parameter( half=.5d0 )
c
1     write(nlist,6010)
6010  format(/' set additional printing options:'//
     & ' 1) print determinants.'/
     & ' 2) suppress printing of determinants.'/
     & ' 3) reset cstep(*) array.'/
     & ' 4) reset cdet(*) array.'/
     & ' 0) end')
c
      j = 0
      call inpi(j,'input print option')
      if ( j .eq. 0 ) then
          return
      elseif ( j .eq. 1 ) then
          qdet = .true.
          write(nlist,6100)
     &    'determinant representations of csfs will be printed'
2         write(nlist,6100) 'smult=(2S+1)=', smult
          dms = (smult - 1) * half
          call inpf(dms,'input ms value for the determinants')
          m2 = nint(2*dms)
          if ( abs(m2)+1 .gt. smult ) then
              write(nlist,*) 'ms too large'
              go to 2
          endif
          if ( mod(m2,2) .eq. mod(smult,2) ) then
              write(nlist,*) 'incorrect ms'
              go to 2
          endif
      elseif ( j .eq. 2 ) then
          qdet=.false.
          write(nlist,6100)
     &    'determinant representations of csfs will not be printed'
       elseif ( j .eq. 3 ) then
          call inpcv( cstep, 4, 'resetting the cstep*1(0:3) array' )
          write(nlist,6110) cstep
       elseif ( j .eq. 4 ) then
          call inpcv( cdet, 4, 'resetting the cstep*1(0:3) array' )
          write(nlist,6120) cdet
      else
          write(nlist,*)'enter a valid menu number?'
      endif
      go to 1
c
6100  format(1x,a,i4)
6110  format(/' new values of cstep(*):'
     & /'  step=0123'
     & /' cstep=',4a1)
6120  format(/' new values of cdet(*):'
     & /'  occ=0123  [empty, alpha, beta, double]'
     & /' cdet=',4a1)
      end
c deck prtdet
cjp
      subroutine prtdet( nact, ms2, d, cicoeff )
cjp
c
c  construct and print the determinant representation of a csf.
c
c  28-sep-91 minor cleanup. -rls
c  1982 (approximately) written by ron shepard.
c
       implicit none
      integer    nflmax
      parameter( nflmax=20 )
      integer         iunits
      common /cfiles/ iunits(nflmax)
      integer      nlist
      equivalence (nlist,iunits(1))
cjp
      real *8 cicoeff
      integer namomx
      parameter( namomx=100 )
      integer nstates,myroot,eivecfile,slaterfile,jirec,nelec,orbnum
      common/jiri/myroot,eivecfile,slaterfile,jirec,nelec,orbnum(namomx)
      integer*2 slater
      dimension slater(1024)
cjp
c
      character      slabel*4,  cstep*1,    cdet*1
      common /charc/ slabel(8), cstep(0:3), cdet(0:3)
c
      integer nact, ms2
      integer d(*)


c
c     # construct determinants corresponding to nopen open shells,
c     # nalpha alpha als, and nclosd closed shell orbitals.
c     # this is done by placing the alpha electrons into nopen locations
c     # in all possible ways.  for each determinant, determine its
c     # projection onto the csf.
c
      real*8 coeff, xnorm
c
      integer i, nopen, nclosd, btot, k, dk, nalpha, idet, ndet, low,
     & sign, m2, num, denom
c
      integer open(namomx), closed(namomx)
      integer rdet(namomx), det(namomx), b(namomx)
      integer alpha(namomx)
      integer db(0:3)
      integer mz2(2)
      integer del(2)
      integer d1f(2)
      integer d2f(2)
      integer m1p1(0:1)
c
      real*8     zero,     one,     tol
      parameter( zero=0d0, one=1d0, tol=1d-10 )
c
      intrinsic mod, sqrt, abs
c
      integer m1pow,j
c
      data db   /  0, 1,-1, 0 /
      data mz2  /  1,-1 /
      data del  /  1, 0 /
      data d1f  /  1,-1 /
      data d2f  / -1, 1 /
      data m1p1 /  1,-1 /
c
c     #  m1pow(i) = (-1)**i
      m1pow(j) = m1p1( mod( j, 2 ) )
c
c     # initialize determinant info.

cjp debug
c     write(6,*)' step vector ',(d(i),i=1,nact)
c
      do 10 i = 1, nact
         rdet(i) = 0
10    continue
c
      nopen  = 0
      nclosd = 0
      btot   = 0
c
      do 20 k = 1, nact
         dk = d(k)
         btot = btot + db(dk)
         b(k)= btot
         if ( dk .eq. 1 .or. dk .eq. 2 ) then
            nopen = nopen + 1
            open(nopen) = k
            rdet(k) = 2
         elseif ( dk .eq. 3 ) then
            nclosd = nclosd+1
            closed(nclosd) = k
            rdet(k) = 3
         endif
20    continue
c
      nalpha = (ms2 + nopen) / 2
c
c     # det(i)=0  if orbital i is empty.
c     # det(i)=1  if alpha al i is occupied.
c     # det(i)=2  if beta al i is occupied.
c     # det(i)=3  if orbital i is doubly occupied.
c
c     # set up for the first determinant.
c
      xnorm = zero
      alpha(nalpha+1) = nopen
      idet = 0
      ndet = 0
      low = nalpha + 1
c
c     # loop over determinants.
c
100   continue
      ndet = ndet + 1
      alpha(low) = alpha(low) + 1
c
      do 110 i = 1, (low-1)
         alpha(i) = i
110   continue
c
      do 115 i = 1, nact
         det(i) = rdet(i)
115   continue
c
      do 120 i = 1, nalpha
         det(open(alpha(i))) = 1
120   continue
c
c     # calculate the coefficient.
c
      coeff = one
      sign  = +1
      m2    = 0
      do 4, k = 1, nact
         dk = d(k)
         if ( dk .eq. 1 ) then
c
c           # alpha al: factor=sqrt( (b+2*m)/(2*b) )
c           # beta  al: factor=sqrt( (b-2*m)/(2*b) )
c
            m2  = m2 + mz2(det(k))
            num = (b(k)+d1f(det(k))*m2)
            if ( num .eq. 0 ) go to 5
            denom = 2 * b(k)
            coeff = (coeff*num) / denom
c
         elseif ( dk .eq. 2 ) then
c
c           # alpha al:
c           #          factor=(-1)**(b+1) * sqrt( (b+2-2*m)/(2*(b+2)) )
c           # beta  al:
c           #          factor=(-1)**(b)   * sqrt( (b+2+2*m)/(2*(b+2)) )
c
            m2  = m2 + mz2(det(k))
            num = (b(k) + 2 + d2f(det(k))*m2)
            if ( num .eq. 0 ) go to 5
            denom = 2 * (b(k) + 2)
            sign  = sign * m1pow( b(k) + del(det(k)) )
cjp debug
c       write(6,*)'SIGN contribution ',k,b(k),det(k),del(det(k)),
c    &       m1pow( b(k) + del(det(k)) )
            coeff = (coeff*num) / denom
c
         elseif ( dk .eq. 3 ) then
c
c           # factor=(-1)**(b)
c
            sign = sign * m1pow( b(k) )
cjp debug
c         write(6,*)'sign contribution ',k, b(k),m1pow( b(k) )
c
         endif
cjp debug
c        write(6,*)' coeff now = ',k,coeff,sign
4     continue
c
      idet  = idet + 1
      xnorm = xnorm + coeff
      coeff = sqrt(coeff) * sign
      write(nlist,6060) ndet, coeff, (cdet(det(i)),i=1,nact)
cjp
      j=1
      do i=1,nact
        if(det(i).eq.1) then
        slater(j)=orbnum(i)
        j=j+1
        endif
        if(det(i).eq.2) then
        slater(j)= -orbnum(i)
        j=j+1
        endif
        if(det(i).eq.3) then
        slater(j)=orbnum(i)
        slater(j+1)= -orbnum(i)
        j=j+2
        endif
      enddo
      jirec=jirec+1
      if(myroot.eq.1) then
c         write(6,*) 'SLATER ',jirec,' : ',(slater(j),j=1,nelec)
          write(slaterfile,rec=jirec) (slater(j),j=1,nelec)
      endif
      write(eivecfile,rec=jirec) coeff*cicoeff
cjp
c
5     continue
c
c     # finished with idet.  find next determinant.
c
      do 130 i = 1, nalpha
         low =i
         if ( alpha(i) .lt. (alpha(i+1)-1) ) go to 100
130   continue
c
c     # exit from loop means all through.
c
      write(nlist,6050) idet, ndet
6050  format(23x,' idet=',i3,' ndet=',i3)
      if ( abs(one-xnorm) .gt. tol ) write(nlist,*)'error in xnorm'
c
      return
6060  format(21x,i3,':',f10.6,(2x,50a1))
      end
c deck prnorb
      subroutine prorb( orbtyp )
c
c  read the restart file created by program mcscf for orbitals
c  then print the orbitals.
c
c  arguments: orbtyp=0 print the morb orbitals.
c                   =1 print the natural orbitals and occupations.
c
c  28-sep-92 prorb() and prnorb() merged. -rls
c
       implicit none
      integer    nflmax
      parameter( nflmax=20 )
      integer         iunits
      common /cfiles/ iunits(nflmax)
      integer      nlist
      equivalence (nlist,iunits(1))
      integer      nres
      equivalence (nres,iunits(3))
c
      character*60    flname
      common /cfname/ flname(nflmax)
c
      integer       nsym, nbpsy,    nmpsy
      common /csym/ nsym, nbpsy(8), nmpsy(8)
c
c     # dummy...
      integer orbtyp
c
c     # local...
      integer b0, m0, bmpt, nb, nm, isym, ntitle, ios, ninf,
     & ntol, nmot, nbmt, i, na,j,maxnavst
      logical qfskip, qconvg
      character*80 title(20)
      character*8 label
c
      integer    nbfmx
      parameter( nbfmx=510 )
c
      real*8 c(nbfmx*nbfmx), occ(nbfmx)
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      data qfskip /.true./
c
c     ##  parameter section
c     #  maximal No. of different DRTs
      integer maxnst
      parameter (maxnst=8)
c
c     #  maximal No. of states in one DRT
      integer mxavst
      parameter (mxavst = 50)
c
c     #  maximal total number of states
      integer maxstat
      parameter (maxstat = maxnst*mxavst)
c
c     ## common section
c
      integer navst,nst,navst_max
      real*8 heig,wavst,norm
      common /avstat/ wavst(maxnst,mxavst), heig(maxnst,mxavst),
     & navst(maxnst),nst,navst_max
c
      integer nrow_f,ncsf_f,nwalk_f,ssym_f
      common/drtf/ nrow_f(maxnst),ncsf_f(maxnst),
     & nwalk_f(maxnst),ssym_f(maxnst)
c
      open(unit=nres,file=flname(3),form='unformatted',status='old')
c
c     # get the header info from the restart file.
c
      ntitle = 0
      rewind nres
      read(nres,iostat=ios)
     &   nsym,ninf,ntol,nmot,nbmt,nst,
     &  (ncsf_f(i),i=1,nst),qconvg,ntitle,
     &  nmpsy,nbpsy,(title(i),i=1,ntitle),
     &  (navst(i),i=1,nst),maxnavst,
     &  ((wavst(i,j),i=1,nst),j=1,maxnavst),
     &  ((heig(i,j),i=1,nst),j=1,maxnavst)
c
      if ( ios. ne. 0 ) call bummer('prnorb: header ios=',ios,faterr)
c
      if ( orbtyp .eq. 0 ) then
c
c        # search restart file for label and get the orbitals.
c
         label = 'morb'
         call rdlv( nres, nbmt, na, c, label, qfskip )
c
c        # write out the orbitals and occupations
c
         if ( na .eq. nbmt ) then
c           # when orbital labels are available, switch to
c           # prblkc().
            call prblks('mcscf orbitals of the final iteration',
     &       c, nsym, nbpsy, nmpsy, ' bfn', '  mo', 1, nlist )
         endif
      elseif ( orbtyp .eq. 1 ) then
c
c        # search the restart file for label and get the
c        # natural orbital occupations.
c
         label = 'nocc'
         call rdlv(nres,nmot,na,occ,label,qfskip)
c
c        # search restart file for label and get the orbitals.
c
         label = 'norb'
         call rdlv(nres,nbmt,na,c,label,qfskip)
c
c        # write out the orbitals and occupations
c
         if ( na .eq. nbmt ) then
            b0 = 0
            m0 = 0
            bmpt = 1
            do 100 isym = 1, nsym
               nb = nbpsy(isym)
               nm = nmpsy(isym)
               if ( nb*nm .ne. 0 ) then
                  write(nlist,6200) isym
c                 # when orbital labels are available, switch
c                 # to pvblkc().
                  call prvblk(
     &             c(bmpt), occ(m0+1), nb,     nb,
     &             nm,      b0,        m0,     ' bfn',
     &             '  no',  ' occ',    1,      nlist )
               endif
               b0 = b0 + nb
               m0 = m0 + nm
               bmpt = bmpt + nb*nm
100         continue
         endif
      else
         write(nlist,*) 'unknown orbtyp=', orbtyp
      endif
c
      close(unit=nres)
c
      return
6200  format(/t10, 'natural orbitals, block', i2 )
      end
c deck prvec
      subroutine prvec( csf, ncsf )
c
c  print entire mcscf vector by csf number.
c
       implicit none
      integer    nflmax
      parameter( nflmax=20 )
      integer         iunits
      common /cfiles/ iunits(nflmax)
      integer      nlist
      equivalence (nlist,iunits(1))
c
      integer ncsf
      real*8 csf(ncsf)
c
      integer i, i1, i2
c
      write(nlist,6000)
      do 100 i1 = 1, ncsf, 100
         i2 = min( ncsf, i1+100-1 )
         write(nlist,6100) i1, i2, (csf(i),  i=i1,i2 )
100   continue
c
      return
6000  format(/' print the entire csf vector.')
6100  format(4x,i6,':',i6 / (10(1x,f7.4)))
      end
c deck privec
      subroutine privec( csf, icsf, ncsf )
c
c  print the entire mcscf vector in sorted order.
c
       implicit none
      integer    nflmax
      parameter( nflmax=20 )
      integer         iunits
      common /cfiles/ iunits(nflmax)
      integer      nlist
      equivalence (nlist,iunits(1))
c
      integer ncsf
      integer icsf(ncsf)
      real*8 csf(ncsf)
c
      integer i, i1, i2
c
      write(nlist,6000)
      do 100 i1 = 1, ncsf, 50
         i2 = min( ncsf, i1+50-1 )
         write(nlist,6100) i1, i2, (icsf(i),csf(icsf(i)), i=i1,i2 )
100   continue
c
      return
6000  format(/
     & ' print the entire sorted csf vector. (icsf(i):csf(icsf(i)))')
6100  format(4x,'icsf(', i6, '):icsf(', i6, ')'
     & /(1x,5(i7,':',f7.4)))
      end
      subroutine  wtlabel(slabel,nsym,fmt,buf)
      implicit none
      integer nsym
      integer i
      character*4 slabel(nsym),buf(nsym)
       character*(*) fmt
      do 10 i = 1, nsym
         write( slabel(i), '(a4)' ) buf(i)
10    continue
      return
      end

