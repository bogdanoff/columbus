!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      PROGRAM addfield
       implicit none 
       integer tmin0,tminit,tmprt,tmrein,tmclr,tmsusp,tmresm,evn1
      data tmin0/0/,tminit/1/,tmprt/2/,tmrein/3/,tmclr/4/,tmsusp/5/,
     &tmresm/6/,evn1/1/
c
      integer aoints, aoprop
      integer nmomx, maxsym
      integer ndim
      integer n1maxx, l1recx, nipv, maxelm
      parameter (nmomx = 510, maxsym = 8)
      parameter (aoints=11, aoprop=12 )
      parameter (ndim = nmomx*(nmomx+1)/2)
      parameter (n1maxx = 3272, l1recx = 4096)
      parameter (nipv = 2)
      integer ntot, nntot, n1max, nskp(maxsym), nnskp(maxsym),
     &        symb(nmomx)
      integer ntitle, nsym, nbft, ninfo, nenrgy, nmap, ierr
      integer nbpsy(maxsym), info(5), idummy(20), ietype(10)
      integer faterr
      parameter (faterr = 2)
      real*8 zero, thresh
      parameter (zero = 0d0, thresh = 1.0D-12)
      real*8 energy(10)
      character*80 title(20), head
      character*8 bfnlab(nmomx), dummy(nmomx)
      character*4 slabel(maxsym)
      real*8 values(n1maxx), buffer(l1recx)
      integer labels(2,n1maxx)
      integer itypea, itypeb, num, iretbv, last, ifmt, ibvtyp,
     &        ibitv, nrec
      parameter (iretbv=0)
      real*8 fcore, b(3), bp(3), field, fac
      real*8 v(ndim), a(ndim,3) , dx(ndim), dy(ndim), dz(ndim)
      integer i, j, ij, isym, jsym, ia, iint, nmat, n
      integer    msame,     nmsame,     nomore
      parameter (msame = 0, nmsame = 1, nomore = 2)
      integer nndxf
      integer imtype(10)
      real*8 mapv(10*nmomx)


      integer prnopt
      real*8  fstrength,forient(10)
      real*8 enuc,ecorr
      common /input/ fstrength,forient,
     .                enuc,ecorr,prnopt

c
      nndxf(i) = (i*(i-1))/2
c
      call timer(' ',tmin0,evn1,6)
      open (unit=aoints, file='aoints', status='old',
     &     access='sequential', form= 'unformatted')
      open (unit=aoprop, file='aoints.field', status='unknown',
     &     access='sequential', form= 'unformatted')
      open (unit=6,file='ffieldls', status='unknown')

      write(6,10)
 10   format(20x,'Finite Field Program'//
     .        1x,'This program writes additional generic 1-e',
     .           ' hamiltonian contributions to the 1-e'/
     .        1x,' integral file '//
     .        1x,' H1(i,j)=sum F*N(type)*integral(type,i,j)'/
     .        1x,'  where F = field strength '/
     .        1x,'        N = weight of integral type '/
     .        1x,'        sum  N(type)*N(type) == 1'//)


      call headwr(6,'Finite Field','5.4.0.2 ')

      call readin
c
c     # read aoints header
c
      call sifrh1(
     &     aoints, ntitle, nsym, nbft,
     &     ninfo, nenrgy, nmap, ierr)
      if (ierr. ne. 0) then
        call bummer ('sifrh1, ierr=', ierr, faterr)
ctm
      if (nenrgy.gt.10) then
        call bummer ('sifrh1, nenrgy.gt.10',nenrgy,faterr)
       endif
      if (ninfo.gt.5) then
        call bummer ('sifrh1, ninfo.gt.5',ninfo,faterr)
       endif
      if (ntitle.gt.20) then
        call bummer ('sifrh1, ntitle.gt.20',ntitle,faterr)
       endif
      if (nmap.gt.10) then
        call bummer ('sifrh1, nmap.gt.10',ntitle,faterr)
       endif

      endif
ctm don't read map vectors
      call sifrh2(
     &     aoints, ntitle, nsym, nbft,
     &     ninfo, nenrgy, nmap, title,
     &     nbpsy, slabel, info, bfnlab,
     &     ietype, energy, imtype,mapv ,
     &     ierr)
      if (ierr. ne. 0) then
        call bummer ('sifrh2, ierr=', ierr, faterr)
      endif
      n1max = info(3)
c
c    # compute index
c
      ntot = 0
      nntot = 0
      do isym = 1, nsym
        nskp(isym) = ntot
        nnskp(isym) = nntot
        ntot = ntot + nbpsy(isym)
        nntot = nntot + nndxf(nbpsy(isym) + 1)
        do j = (nskp(isym)+1), ntot
          symb(j) = isym
        end do
      end do
      nmat = nntot
c
c     # initialize v and a  matrices
c
c     nmat = nnskp(nsym)
      call wzero(nmat, v, 1)
      call wzero(nmat, a, 1)
c
c     # write header of aoprop
c
      call sifwh(
     &  aoprop, ntitle, nsym, nbft,
     &  ninfo, nenrgy, nmap, title,
     &  nbpsy, slabel, info, bfnlab,
     &  ietype, energy, imtype, mapv,
     &  ierr)
      if (ierr. ne. 0) then
        call bummer ('sifwh, ierr=', ierr, faterr)
      endif
      last = msame
c
c     # read in aoints
c     # looping over buffer of aoints
c
 100  continue

c     now read all one electron records into the a matrix,
c     analyse whether they are multipole moment integrals
c     and if so add them accordingly to v matrix

      if (last .eq. nomore) goto 101
        call sifrd1(
     &     aoints, info, nipv, iretbv,
     &     buffer, num, last, itypea,
     &     itypeb, ibvtyp, values,
     &     labels, fcore, ibitv, ierr)
      if (ierr. ne. 0) then
        call bummer ('sifrd1, ierr=', ierr, faterr)
      endif

c
c      write the matrices to aoprop  but for the last one
c      change last=nomore to last=nmsame in order to be
c      able to add a further 1e record
c

        if  (itypea.eq.1) then
c
c   construct v(*,*) on the fly:
c   v(i) = fstrength*forient(j)*array(i, itypeb=j-1)
c
c         write(*,*) 'itypea,itypeb=',itypea,itypeb
c         write(*,*) forient
          if (abs(forient(itypeb+1)).gt.1.d-8) then
c         add appropriate integral

          do iint = 1, num
            i = max(labels(1,iint), labels(2,iint))
            j = min(labels(1,iint), labels(2,iint))
            isym = symb(i)
            jsym = symb(j)
            if (isym.eq.jsym) then
              ij = nnskp(isym) + nndxf(i-nskp(isym)) + (j-nskp(isym))
              v(ij) = v(ij)+values(iint)*fstrength*forient(itypeb+1)
c             write(*,*) 'adding ',ij,'of type',itypeb
            else
       call bummer('cannot add non-totally symmetric integral',itypeb,
     &              faterr)
            endif
          end do
          endif
         endif
       if (last.eq.nomore) then
       call sifew1(aoprop, info, nipv, num, nmsame, itypea,
     &  itypeb, ibvtyp, values, labels, fcore, ibitv, buffer,
     &  nrec, ierr)
       else
       call sifew1(aoprop, info, nipv, num, last, itypea,
     &  itypeb,  ibvtyp, values, labels, fcore, ibitv, buffer,
     &  nrec, ierr)
       endif

        if (ierr. ne. 0) then
            call bummer ('sifew1: writing error', ierr, faterr)
       endif
        goto 100
101   continue

c
c    # write v matrice to aoprop (itypea=0, itypeb=6  == H1 )
c
      iint = 0
      ij = 0
      nrec = 0
      itypea = 0
      itypeb = 6
      ntot = 0
      do isym = 1, nsym
        ntot = ntot + nbpsy(isym)
        do i = (nskp(isym)+1), ntot
          do j = (nskp(isym)+1), i
            ij = ij + 1
            if (dabs(v(ij)).gt.thresh) then
              iint = iint + 1
              values(iint) = v(ij)
              labels(1,iint) = i
              labels(2,iint) = j
c
c     # if iint larger than buffer size dump to file
c
              if (iint.eq.n1max) then
c        last = nmsame
                last = msame
                num = iint
                nrec = nrec + 1
                call sifew1(aoprop, info, nipv, num, last, itypea,
     &            itypeb,  0, values, labels, 0.0d0, 0, buffer,
     &            nrec, ierr)
               if (ierr. ne. 0) then
                call bummer ('sifew1: v buffer, ierr=', ierr, faterr)
               endif
               iint = 0
              endif
            endif
            end do
          end do
        end do
c
c     # dump the rest to file
c
      last = nomore
      num = iint
      nrec = nrec+1
      call sifew1(aoprop, info, nipv, num, last, itypea,
     &  itypeb,  0, values, labels, 0.0d0, 0, buffer,
     &  nrec, ierr)
      if (ierr. ne. 0) then
        call bummer ('sifew1: v final, ierr=', ierr, faterr)
      endif

      close (unit=aoints, status= 'delete')
      close (unit=aoprop, status= 'keep')
      call timer('finite field required time',tmclr,evn1,6)
      call bummer('normal termination',0,3)
      stop
      end

       subroutine readin
       implicit none 
       real*8 x,y,z
       real*8 xx,xy,xz,yy,yz,zz

       integer prnopt
       real*8  fstrength,forient(10)
       real*8 enuc,ecorr
       common /input/ fstrength,forient
     .               ,enuc,ecorr,prnopt
       namelist /input/  prnopt,fstrength,x,y,z,
     .                   xx,xy,xz,yy,yz,zz

       integer ierr
       integer i
       character*2 atom
       real*8 charge,ax,ay,az,norm,fnu(3),nucdip(3)


       open (unit=5,file='fieldin',status='old',form='formatted')

      write(6,6010)
6010  format(' echo of the input for program ffield:')
      call echoin( 5, 6, ierr )
      if ( ierr .ne. 0 ) then
         call bummer(' datain: from echoin, ierr=',ierr,2)
      endif
c
      rewind 5
       prnopt=0
       fstrength=0.0d0
       x=0.0d0
       y=0.0d0
       z=0.0d0
       xx=0.0d0
       xy=0.0d0
       xz=0.0d0
       yy=0.0d0
       yz=0.0d0
       zz=0.0d0
       call wzero(9,forient,1)

       read (5,input,end=4)
  4    continue
       close(5)

       if ( abs(x).gt.1.0d-8) forient(1)=x
       if ( abs(y).gt.1.0d-8) forient(2)=y
       if ( abs(z).gt.1.0d-8) forient(3)=z
       if ( abs(xx).gt.1.0d-8) forient(4)=xx
       if ( abs(xy).gt.1.0d-8) forient(5)=xy
       if ( abs(xz).gt.1.0d-8) forient(6)=xz
       if ( abs(yy).gt.1.0d-8) forient(7)=yy
       if ( abs(yz).gt.1.0d-8) forient(8)=yz
       if ( abs(zz).gt.1.0d-8) forient(9)=zz

       norm=0.0d0
       do i=1,9
       norm=norm+forient(i)*forient(i)
       enddo
       norm=1.0d0/sqrt(norm)
       do i=1,9
         forient(i)=forient(i)*norm
       enddo
       write(*,1001) fstrength
       write(*,1002) (forient(i),i=1,3)
       write(*,1003) (forient(i),i=4,9)
1000   format (1x,a2,2x,f5.1,3f14.8)
1001   format (1x,'field strength:',f6.3)
1002   format (1x,'N(x):',f6.3,/1x,'N(y):',f6.3,/1x,'N(z):',f6.3//)
1003   format (1x,'N(xx):',f6.3,/1x,'N(xy):',f6.3,/1x,'N(xz):',f6.3/
     .         1x,'N(yy):',f6.3,/1x,'N(yz):',f6.3,/1x,'N(zz):',f6.3)

       open(unit=5,file='geom',status='old',form='formatted')
  8    read(unit=5,fmt=1000,end=10)
     .   atom,charge,ax,ay,az
       write(*,1100) atom,charge,ax,ay,az
1100   format (1x,a2,1x,'charge=',f6.3,'x=',f8.4,'y=',f8.4,'z=',f8.4)
        nucdip(1) = nucdip(1) + charge*ax
        nucdip(2) = nucdip(2) + charge*ay
        nucdip(3) = nucdip(3) + charge*az
       goto 8
  10   continue
       write(*,1101) nucdip
1101   format(1x,'Nuclear dipole moment:'/
     .        20x,'<x>=',f8.4 /
     .        20x,'<y>=',f8.4 /
     .        20x,'<z>=',f8.4 /)

       do i=1,3
        fnu(i) = fstrength*forient(i)*nucdip(i)
       enddo
       ecorr =  fnu(1) + fnu(2) + fnu(3)
       close(5)
       return
       end


