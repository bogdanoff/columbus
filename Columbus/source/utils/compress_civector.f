!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/

       program comprcivec
       implicit none
       integer mxbuf
       integer,parameter :: llenbuf=20000
       integer, parameter :: ndrt=8,nci=9,nccmpr=10
       real*8, allocatable :: buf(:,:) 
       integer buf2(128)
       integer ierr,lxyzir(3),vrsion,nhdint,lenbuf,hmult
       integer ncsft,bsize,ii,offset,outoffset,nsym,blen,num,nc
       logical lexist,spnorb,spnodd
       character*80 title
       real*8 accuracy
       integer  iii,iz
       write(6,*) ' ***********************************************'
       write(6,*) ' Compression/Decompression of the CI vector file'
       write(6,*) ' ***********************************************'


c
c      find out whether cidrtfl is present
c
       inquire(file='cidrtfl',exist=lexist)
       if (lexist) then
        open(file='cidrtfl',unit=ndrt,form='formatted')
        READ (ndrt,'(a)',iostat=ierr) title
        IF (ierr/=0) CALL bummer('could not read title, ierr=',ierr,2)
        READ (ndrt,'(2L4,4I4)',iostat=ierr) spnorb, spnodd,lxyzir,hmult
        IF (ierr/=0) CALL bummer('SOCI parameters err=',ierr,2)
        CALL rddbl(ndrt,128,buf2,3)
!
        vrsion = buf2(1)
        lenbuf = buf2(2)
        nhdint = buf2(3)
!
        IF (vrsion/=6) THEN
          CALL bummer('NEED DRT VERSION 6',vrsion,2)
        END IF

        CALL rddbl(ndrt,128,buf2,nhdint)
        nsym = buf2(6)
        ncsft = buf2(16) 
        close(ndrt)
       else
         write(6,*) ' could not find cidrtfl file '
         write(6,*) ' enter CI dimension manually '
         read(5,*) ncsft
       endif
        write(6,'(a,i10)') 'CI dimension (ncsft)=',ncsft

       inquire(file='civfl',exist=lexist)
        if (.not. lexist) then
        call bummer('cannot find file civfl',0,2)
        endif
       
        write(6,*) 'desired accuracy (Delta E)' 
        read(5,*) accuracy
        write(6,*) 'buffer size: mxbuf'
        read(5,*) mxbuf 
        mxbuf=max(10000000,mxbuf)
c
c       Accuracy (delta c_i) approx sqrt(Delta E/|sum_i(H_ii -E)|)
c                            approx sqrt(Delta E*fudge factor)
c
        accuracy=sqrt(accuracy*1.d-3)
        write(6,*) ' accuracy (Delta c)=',accuracy
        
         bsize=min(mxbuf/2,ncsft)
         allocate(buf(bsize,2))
        call getputvec('civflcmpr',buf(1,2),1,1,'new')
        call getputvec('civflcmpr',buf(1,2),1,1,'close')
        buf(1,2)=bsize
        CALL getputvec('civflcmpr',buf(1,2),1,1,'openwt')
        
         outoffset=2 
         do ii=1,ncsft,bsize
           blen=min(bsize,ncsft-ii+1)
           offset=ii
           write(6,'(a,i10,a,i10)') 'reading range:',
     .                              offset,'-',offset+blen-1
           CALL getputvec('civfl',buf(1,1),blen,offset,'openrd')
             iz=0
            do iii=1,blen
              if (abs(buf(iii,1)).lt.accuracy) then 
                  iz=iz+1
                  buf(iii,1)=0.0d0
              endif
            enddo
            write(6,*) iz,' initial zeros' 
           CALL sif_compress(buf(1,1),blen,buf(1,2),blen,nc) 
           call sif_endofstream(buf(1,2),blen, nc)
         write(6,*) 'adding eor marker'
           nc=(nc-1)/64+1
           write(6,'(a,i10,a,i10)') 'compressed to:',nc
           CALL getputvec('civflcmpr',buf(1,2),nc,outoffset,'openwt')
           write(6,'(a,i10,a,i10)') 'written to:',outoffset,'-',
     .                            outoffset+nc-1
           outoffset=outoffset+nc
         enddo

       write(6,*) 'CI vector size reduced to ',
     .        outoffset,'(',outoffset*100.0d0/dble(ncsft),' %)'

       end





      SUBROUTINE sif_endofstream (stream, slen, ioff)
      IMPLICIT NONE
      INTEGER*8 slen,ioff,ntot
      INTEGER*8 stream(slen)
      INTEGER*8 ioffb,ioffw,rem1,rem2,itmp
!    total number of valid bits 1+6+7+bitsmantissa
!    counted from the left
      ioffw=(ioff-1)/64+1
      ioffb=ioff-(ioffw-1)*64
c     write(0,'(a,3(2x,i8))') 'eofstream (w,b,len)=',ioffw,ioffb,6
!    remainder in current word 64-ioffb+1
      rem1=64-ioffb+1
!    mark endofstream by bitpattern 111011 
      ntot=6
      itmp=59
      stream(ioffw)=ior(stream(ioffw),ishft(itmp,rem1-ntot))
      rem2=max(0,ntot-rem1)
      IF (rem2.gt.0) then 
        stream(ioffw+1)=ior(stream(ioffw+1),ishft(itmp,64-rem2))
       
c       write(0,'(a,b64)') 'itmp ', itmp 
c       write(0,'(a,b64)') 'stream(ioffw)=',stream(ioffw)
c       write(0,'(a,b64)') 'stream(ioffw+1)=',stream(ioffw+1)
c       write(0,*) ' endofstream: two words rem1,rem2=',rem1,rem2
      endif
      ioff=ioff+ntot
      ioffw=(ioff-1)/64+1
      ioffb=ioff-(ioffw-1)*64
c     write(0,'(a,3(2x,i8))') 'eofstream tot (w,b)=',ioffw,ioffb
c     WRITE (6,*) 'used ',ntot,' bits; total usage=',ioff
      RETURN
      END
       SUBROUTINE sif_getfactor (din,n,ioff)
      IMPLICIT NONE
      INTEGER*8 din(n),expt,mantissa,sign
      INTEGER*8 two11,two,two52,two7
      PARAMETER  (two11=2**11-1,two=1,two52=2**52-1,two7=2**7-1)
      INTEGER*8 two6,two3
      PARAMETER  (two6=2**6-1,two3=7)
      INTEGER*8 exptn,bitsmantissa,mantissanew,expnew,ntot,ioff
      LOGICAL zero
      INTEGER n,i

      ioff=0
      do i=1,n
      expt=iand(ishft(din(i),-52),two11)
      mantissa=iand(din(i),two52)
      exptn=expt-1023_8
      bitsmantissa=(40_8+exptn)
!    total number of valid bits 1+6+7+bitsmantissa
!    counted from the left
      IF (bitsmantissa.gt.0.and.expt.ne.0) THEN
           ntot=14+bitsmantissa
      ELSE
!    mark zeros by 3 leading bits  set to 1
           ntot=4
      END IF
      ioff=ioff+ntot
      enddo
!    add end-of-stream marker
      ioff=ioff+6
!    return in dword entieties
      ioff=(ioff-1)/64+1
      RETURN
      END
      SUBROUTINE sif_decompress (dout, n, stream, slen)
      IMPLICIT NONE
      INTEGER*8 n,dtmp,expt,mantissa,sign
      INTEGER*8 dout(n)
      INTEGER*8 two11,two,two52,two7
      PARAMETER  (two11=2**11-1,two=1,two52=2**52-1,two7=2**7-1)
      INTEGER*8 two6
      PARAMETER  (two6=2**6-1)
      INTEGER*8 exptn,bitsmantissa,mantissanew,expnew
      LOGICAL zero
      INTEGER*8 slen,stream(slen),ioff,ioffw,ioffb,ntot,rem1,rem2
      INTEGER*8 icnt,iseven,itmp,expnt

      ioff=1
      ioffw=1
      ioffb=1
      icnt=0
! 
!     get first 7 bits = sign+bitsmantissa
! 
   10 rem1=64-ioffb+1
!     write(6,*) 'current offset:',(ioffw-1)*64+ioffb
c     write(0,'(a,2i8)') ' decompress w,b=',ioffw,ioffb
      IF (rem1.ge.7) THEN
!     all in one word
           iseven=iand(ishft(stream(ioffw),-(64-ioffb-7+1)),two7)
      ELSE
           iseven=iand(stream(ioffw),2**rem1-1)
           iseven=ishft(iseven,7-rem1)
!      contribution of second word
           itmp=iand(ishft(stream(ioffw+1),-(64-(7-rem1))),2**(7-rem1)-
     &      1)
           iseven=ior(iseven,itmp)
c          write(0,'(a,b64)') 'stream(ioffw)=',stream(ioffw)
c          write(0,'(a,b64)') 'stream(ioffw+1)=',stream(ioffw+1)
c          write(0,'(a,b10)') 'iseven=',iseven
      END IF
! 
!     end of stream marker: iseven=111011X
!     two7=1111111
! 
      IF (iand(iseven,two7-1).eq.118) THEN
c          WRITE (6,*) 'found eos marker at word=',ioffw,'bit=',ioffb
           if (icnt.ne.n) then
           write (0,'(a,b64)') 'found eos marker',iseven
           write(0,*) 'incorrect icnt',icnt,n
            stop 999
           endif
           GO TO 20
      END IF
! 
!     zero marker : iseven=1111000
! 
      IF (iand(iseven,120).eq.120) THEN
           icnt=icnt+1
           ioffb=ioffb+4
           IF (ioffb.gt.64) THEN
                ioffb=ioffb-64
                ioffw=ioffw+1
           END IF
!     write a floating point zero 
!     (positive zero = sign, exponent and mantisse set to 0) 
           dout(icnt)=0
           GO TO 10
      ELSE
! 
!     this is a valid number
! 
           sign=ishft(iseven,-6)
           bitsmantissa=iand(iseven,two6)
           ntot=14+bitsmantissa

! 
!     get the ntot bits
! 
           rem1=64-ioffb+1
c          write(0,*) 'rem1=',rem1,rem1.ge.ntot
           IF (rem1.ge.ntot) THEN
! 
!      all from one word
! 
                itmp=iand(ishft(stream(ioffw),ntot-rem1),2**ntot-1)
                ioffb=ioffb+ntot
                if (ioffb.gt.64) then
                 ioffb=ioffb-64
                 ioffw=ioffw+1 
                endif
           ELSE
! 
!      collect from two words
! 
                itmp=iand(stream(ioffw),2**rem1-1)
                itmp=ishft(itmp,ntot-rem1)
                ioffw=ioffw+1
                ioffb=1
                itmp=ior(itmp,iand(ishft(stream(ioffw),-(64-(ntot-rem1))
     &           ),2**(ntot-rem1)-1))

                ioffb=ioffb+ntot-rem1
           END IF
           expnt=iand(ishft(itmp,-bitsmantissa),two7)
           mantissa=iand(itmp,2**bitsmantissa-1)
           dtmp=ishft(sign,63)
           expnt=expnt-63+1023
           dtmp=ior(dtmp,ishft(iand(expnt,two11),52))
           mantissa=ishft(mantissa,52-bitsmantissa)
           dtmp=ior(dtmp,mantissa)
           icnt=icnt+1
           dout(icnt)=dtmp
           if (icnt.gt.n) then
           write(0,*) 'incorrect icnt',icnt,n
            stop 998
           endif
      END IF
          GO TO 10

   20 CONTINUE
      RETURN
      END
      SUBROUTINE sif_compress (din,n,stream, slen, ioff)
      IMPLICIT NONE
      integer*8 n,ival
      INTEGER*8 din(n),dout,expt,mantissa,sign
      INTEGER*8 two11,two,two52,two7
      PARAMETER  (two11=2**11-1,two=1,two52=2**52-1,two7=2**7-1)
      INTEGER*8 two6,two4
      PARAMETER  (two6=2**6-1,two4=15)
      INTEGER*8 exptn,bitsmantissa,mantissanew,expnew
      LOGICAL zero
      INTEGER*8 slen,stream(slen),ioff,ioffw,ioffb,ntot,rem1,rem2
      real*8 accuracy
      integer zeroin,zeroout

c
c     # (-1)**SIGN * 2**(exp-exp_bias) * 1.Mantissa
c     1.d-12  = 2**(-40)
c     1.d+12  = 2**(+40)
c      => Bereich = 80     ... 7bits : Bereich 127
c     1.d-12  = 2**(-40)
c     8.d+09  = 2**(+23)
c      => Bereich = 64     ... 6bits : Bereich 63

c
c   # here we use a fixed scheme, originally defined for integrals
c
c
c
      stream(1:slen)=0
      ioff=1
      zeroin=0
      zeroout=0
      do ival=1,n
      expt=iand(ishft(din(ival),-52),two11)
      mantissa=iand(din(ival),two52)
      sign=iand(ishft(din(ival),-63),two)
      exptn=expt-1023_8
c     2**63 = 9.2 10**18
c     if (abs(exptn).gt.63)  stop 'exptn too large'
c     default mantissa= 52bit
c     there are only 64-14 bits available for the mantissa
c     so error < 1.d-12 for exptn <= 10
c     otherwise the absolute error increases
c     with a constant relative error of 2**(-50)= 9.d-16
c     instead of the relative error of DP 2**(-52) = 2.2 d-16
      bitsmantissa=min(50_8,(40_8+exptn))
!    total number of valid bits 1+6+7+bitsmantissa
!    counted from the left
      ioffw=ishft(ioff-1,-6)
      ioffb=ioff-ishft(ioffw,6)
      ioffw = ioffw+1
!    remainder in current word 64-ioffb+1
      rem1=64-ioffb+1
      IF (bitsmantissa.gt.0.and.expt.ne.0) THEN
           mantissanew=ishft(mantissa,bitsmantissa-52)
           expnew=iand(exptn+63,two7)
           expnew=ibclr(expnew,63)
           ntot=14+bitsmantissa
c   packed to the right
           dout=ior(ior(ior(ishft(sign,ntot-1),ishft(iand(bitsmantissa,
     &      two6),ntot-7)),ishft(expnew,ntot-14)),mantissanew)
c          WRITE (6,10) din,expt,exptn,mantissa,sign,bitsmantissa,
c    &      mantissanew,expnew,dout
   10      FORMAT ('DIN=',b64,/'E=',b23,'EN=',i10,'M=',b52,'S=',b2,/'BIT
     &SM=',i6,' MN=',b40,'Enew=',b8,/'DOUT=',b64)
!    add to current word
           stream(ioffw)=ior(stream(ioffw),ishft(dout,rem1-ntot))
!    remainder for next word
           rem2=max(0,ntot-rem1)
           IF (rem2.gt.0) then
              stream(ioffw+1)=ior(stream(ioffw+1),ishft(dout,64-rem2))
           ENDIF
      ELSE
          zeroout=zeroout+1
!    mark zeros by 4 leading bits  set to 1
           ntot=4
           stream(ioffw)=ior(stream(ioffw),ishft(two4,rem1-ntot))
           rem2=max(0,ntot-rem1)
           IF (rem2.gt.0) then
             stream(ioffw+1)=ior(stream(ioffw+1),ishft(two4,64-rem2))
           ENDIF
      END IF
!     write(0,'(a,3(2x,i8),a,i8)') 'compress (w,b,len)=',
!    .     ioffw,ioffb,ntot,' ival=',ival
      ioff=ioff+ntot
      enddo
       write(6,*) ' zeroout=',zeroout
      RETURN
      END
