!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      implicit real*8 (a-h,o-z)

      real*8, dimension(:,:), allocatable :: x
      real*8, dimension(:)  , allocatable :: aM,Z
      character(2), dimension(:), allocatable :: S

      Nat = 0
      IOStatus = 0
      open(unit=9,file='geom',status='old',iostat=ist)
      if (ist > 0) STOP "Prepare geom file first!"

      do while (IOStatus == 0)
        read(9,*,IOSTAT=IOStatus)
        if (IOStatus /= 0) EXIT
        Nat=Nat+1
      enddo
      close(9)

      allocate (x(Nat,3),STAT=istat)
      if (istat /= 0)  STOP "*** Not enough memory ***"
      allocate (aM(Nat),Z(Nat),S(Nat),STAT=istat2)
      if (istat2 /= 0) STOP "*** Not enough memory ***"

       open(unit=9,file='geom',status='unknown')
       open(unit=10,file='coord',status='unknown')

       nunit=2

!      Read geometry:
       do i=1,Nat
       read(9,*) S(i),Z(i),(x(i,j),j=1,3),aM(i)
       enddo

!      Write Turbomole geometry:
       write(10,105) '$coord'
       do i=1,nat
        if (nunit .eq. 2) then
        write(10,110) (X(i,j),j=1,3),S(i)
        else
        write(10,110) (X(i,j)*0.529177D0,j=1,3),S(i)
        endif
       enddo 
       write(10,120) '$user-defined bonds'
       write(10,130) '$end'

  105 format(A6)
  110 format(3F20.14,A7)
  120 format(A19)
  130 format(A4)
      end 
