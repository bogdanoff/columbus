!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      program rotatemo
       implicit none
c  rotates MOs pairwise by some angle theta         
c
c  namelist input
c
c    &input
c     mocoef           mocoefficient input file name
c     rotated          mocoefficient output file name
c     mopair(ipair,1) = irrep 
c     mopair(ipair,2) = bfn1  
c     mopair(ipair,3) = bfn2   
c     angle(ipair)=angle
c    &end
c
c     e.g.  rotate basis functions 2 and 3 in irrep 2 by 5 degrees
c
c      mopair(1,1)=2,
c      mopair(1,2)=2,
c      mopair(1,3)=3,
c      mopair(1,4)=5,
c

      integer mxtitle, ntitle, filerr, syserr, mocoefin, mocoefout
      integer maxbfn,ierr
      parameter (mxtitle=20, maxbfn=510)
      character*80 titles(mxtitle),afmt
      character*4  labels(8)
      integer off(8),off2(8),npair,ipair,isym,ibfn1,ibfn2,n
      integer i,j,offsetocc,offsetc, offset1,offset2
      integer occlen,nsym,nbfpsy(8),nmopsy(8),clen
      real*8 c(maxbfn*maxbfn),scratch(maxbfn*maxbfn)
      real*8 occnew(maxbfn),occ(maxbfn),angle(10),v(maxbfn,2)
      character*80 mocoef, rotated
      integer mopair(10,3)
      logical nooccvec
      namelist /input/ mopair,mocoef,rotated ,angle 
c
      integer sym(maxbfn,8)
      integer left(maxbfn),right(maxbfn)

      open(6,file='rotatemols',status='unknown')

      write(6,*) '===== Program rotatemo ====='
      call izero_wr(maxbfn*8,sym,1)
      call izero_wr(10*4,mopair,1)
      mocoef='x'
      rotated='x'

      open(unit=10,file='rotatemoin',status='old')
      call echoin(10,6,ierr)
      rewind(10)
      read(10,input,end=100)
 100  continue
      close(10)

      if ( mocoef .eq. 'x' )
     . call bummer('enter original MO coefficient file name ',0,2)
      if ( rotated .eq. 'x' )
     . call bummer('enter rotated MO coefficient file name ',0,2)

       npair=0
       do i=1,10
         if (mopair(i,1).eq.0) exit
         npair=npair+1
       enddo
       
       write(6,*) 'found ',npair, ' MO pair definitions'

       do i=1,npair
        write(6,55) i,mopair(i,1:3),angle(i)
 55    format( 'MO-pair #',i3,' irrep=',i3,' bfn1=',i3,' bnf2=',i3,
     .     ' angle=',f8.4)
       enddo
 
      write(*,*) 'opening old mocoefficient file:',mocoef
      open(unit=11,file=mocoef,status='old')
      clen=maxbfn*maxbfn
      call moread(11,10,filerr,syserr,mxtitle,ntitle,titles,
     . afmt,nsym,nbfpsy,nmopsy,labels,clen,c)
      write(*,*) 'Titles from mocoef file:'
      write(*,*) (titles(i),i=1,ntitle)
      write(*,900) nsym
      write(*,901) (nbfpsy(i),i=1,nsym)
      write(*,902) (nmopsy(i),i=1,nsym)

 900  format(' number of irreducible representations:',i4)
 901  format(' number of basis functions per irrep:',8i4)
 902  format(' number of molecular orbitals per irrep:',8i4)
 903  format(' irrep ',i3,' MO ', i3, 'incorrect value:',i4)

      write(*,*) ' reading mocoefficients ... '
      call moread(11,20,filerr,syserr,mxtitle,ntitle,titles,
     . afmt,nsym,nbfpsy,nmopsy,labels,clen,c)

      call prblks('Original MO coefficients',c,nsym,nbfpsy,nmopsy,
     .            'SAO','MO',1,6)
      close(11)
       
       off(1)=0
       off2(1)=0
        do i=1,nsym-1
         off(i+1)=off(i)+nmopsy(i)
         off2(i+1)=off2(i)+nmopsy(i)*nmopsy(i)
        enddo

       do i=1,npair
         isym=mopair(i,1)
         ibfn1=mopair(i,2) 
         ibfn2=mopair(i,3) 
         offset1=off2(isym)+(ibfn1-1)*nmopsy(isym)+1
         offset2=off2(isym)+(ibfn2-1)*nmopsy(isym)+1
         call dcopy_wr(nmopsy(isym),c(offset1),1,v(1,1),1)
         call dcopy_wr(nmopsy(isym),c(offset2),1,v(1,2),1)

         call rotate(v,maxbfn,nmopsy(isym),angle(i))

         call dcopy_wr(nmopsy(isym),v(1,1),1,c(offset1),1)
         call dcopy_wr(nmopsy(isym),v(1,2),1,c(offset2),1)
       
       enddo
 
       write(*,*) 'writing rotated MOs to ',rotated   

       open(unit=12,file=rotated,status='unknown')
       call mowrit(12,10,filerr,syserr,ntitle,titles,afmt,
     .  nsym,nbfpsy,nmopsy,labels,c)
       write(*,*) '  writing MOs ... '
       call mowrit(12,20,filerr,syserr,ntitle,titles,afmt,
     .  nsym,nbfpsy,nmopsy,labels,c)

      call prblks('Rotated MO coefficients',c,nsym,nbfpsy,
     .            nmopsy,'SAO','MO',1,6)

       close(12)
       close(6)
       stop
       end


        subroutine rotate (v,maxbfn,n,angle)
        integer n,maxbfn
        real*8 angle,v(maxbfn,2)
        real*8 tmp1,tmp2
        real*8 degreetograd
c
c     rad = (angle/180)*Pi
c
        data degreetograd/0.017453292d0/

        do i=1,n
           tmp1=cos(angle*degreetograd) * v(i,1) +
     .          sin(angle*degreetograd) * v(i,2)
  
           tmp2=-sin(angle*degreetograd) * v(i,1) +
     .          cos(angle*degreetograd) * v(i,2)
        
          v(i,1)=tmp1
          v(i,2)=tmp2
        enddo
        return
        end




