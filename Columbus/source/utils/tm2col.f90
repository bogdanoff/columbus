!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
!     tm2col program converts the 'coord' geometry input file from
!     TURBOMOLE to 'geom' input file of COLUMBUS.
!     Author: M. Barbatti, Aug. 2005. Updated May 2007.
!
      implicit real*8 (a-h,o-z)

      real*8, dimension(:,:), allocatable :: x
      character(2), dimension(:), allocatable :: S
      character(2) Sb
      character(4) A

     ! open(unit= 8, file='tm2col.log',status='unknown')

!     read number of atoms
      Nat = 0
      is0 = 0

      open(unit= 7, file='coord',status='unknown')
      read(7,*) A
      read(7,*) A
      do while (index(A,"$") == 0)
      read(7,*) A
      Nat = Nat + 1
      enddo
      close(7)

     ! write(8,'(A16,I3,/)') 'Number of atoms:', Nat

!     Allocate
      allocate (x(Nat,3),STAT=istat)
      if (istat /= 0)  STOP "*** Not enough memory ***"
      allocate (S(Nat),STAT=istat2)
      if (istat2 /= 0) STOP "*** Not enough memory ***"

!     Read Turbomole geometry:
      open(unit= 7, file='coord',status='unknown')
      read(7,*) 
      do i=1,Nat
      read(7,*) (x(i,j),j=1,3),S(i)
     ! write(8,'(3(F20.14,2x),A5)')  (x(i,j),j=1,3),S(i)
      enddo
      close(7)

!     Write Columbus geometry:
      open(unit=9,file='geom',status='unknown')
      do i=1,Nat
      Sb=S(i)
      call atom(Sb,Z,aM)
      write(9,'(1x,a2,2x,f5.1,4f14.8)') S(i),Z,(x(i,j),j=1,3),aM
      enddo 

      contains
!===============================================================================
! John Appleyard, 21 June 2005.
! This file: http://ftp.aset.psu.edu/pub/ger/fortran/hdk/ulcaseJA.f90
!
      function Translate(String,InTable,OutTable)
      character(*),intent(in) :: String,InTable,OutTable
      character(Len(String)) :: Translate
      Integer :: i , p , l

      Translate = String
      l = min(len(InTable),len(OutTable))
      do p = 1 , len(String)
         i = index(InTable(1:l),String(p:p))
         if ( i>0 ) Translate(p:p) = OutTable(i:i)
      enddo
      end function Translate
!=================================================================
      function UpperCase(string)
      character(*),intent(in) :: string
      character(len(string)) :: UpperCase
      UpperCase = Translate(string,'abcdefghijklmnopqrstuvwxyz' &
                                ,'ABCDEFGHIJKLMNOPQRSTUVWXYZ')
      end function UpperCase
!=================================================================
      function LowerCase(string)
      character(*),intent(in) :: string
      character(len(string)) :: LowerCase
      LowerCase = Translate(string,'ABCDEFGHIJKLMNOPQRSTUVWXYZ' &
                                ,'abcdefghijklmnopqrstuvwxyz')
      end function LowerCase
!=================================================================

! ...............................

      subroutine atom(Sb,Z,aM)
      implicit real*8 (a-h,o-z)
      character(2) :: Sb

      Sb=trim(Sb)
      Sb=LowerCase(Sb)
! great coding: 103 comparisons for each element - even for h (tm)

        if (Sb == 'x') then
          Z  = 0
          aM = 0.0
        endif
        if (Sb == 'h') then
          Z  = 1
          aM = 1.007825037d0
        endif
        if (Sb == 'he') then
          Z  = 2
          aM = 4.00260325d0
        endif
        if (Sb == 'li') then
          Z  = 3
          aM = 7.0160045d0
        endif
        if (Sb == 'be') then
          Z  = 4
          aM = 9.0121825d0
        endif
        if (Sb == 'b') then
          Z  = 5
          aM = 11.0093053d0
        endif
        if (Sb == 'c') then
          Z  = 6
          aM = 12.0
        endif
        if (Sb == 'n') then
          Z  = 7
          aM = 14.003074008d0
        endif
        if (Sb == 'o') then
          Z  = 8
          aM = 15.99491464d0
        endif
        if (Sb == 'f') then
          Z  = 9
          aM = 18.99840325d0
        endif
        if (Sb == 'ne') then
          Z  = 10
          aM = 19.9924391d0
        endif
        if (Sb == 'na') then
          Z  = 11
          aM = 22.9897697d0
        endif
        if (Sb == 'mg') then
          Z  = 12
          aM = 23.9850450d0
        endif
        if (Sb == 'al') then
          Z  = 13
          aM = 26.9815413d0
        endif
        if (Sb == 'si') then
          Z  = 14
          aM = 27.9769284d0
        endif
        if (Sb == 'p') then
          Z  = 15
          aM = 30.9737634d0
        endif
        if (Sb == 's') then
          Z  = 16
          aM = 31.9720718d0
        endif
        if (Sb == 'cl') then
          Z  = 17
          aM = 34.968852729d0
        endif
        if (Sb == 'ar') then
          Z  = 18
          aM = 39.9623831d0
        endif
        if (Sb == 'k') then
          Z  = 19
          aM = 38.9637079d0
        endif
        if (Sb == 'ca') then
          Z  = 20
          aM = 39.9625907d0
        endif
        if (Sb == 'sc') then
          Z  = 21
          aM = 44.9559136d0
        endif
        if (Sb == 'ti') then
          Z  = 22
          aM = 47.9479467d0
        endif
        if (Sb == 'v') then
          Z  = 23
          aM = 50.9439625d0
        endif
        if (Sb == 'cr') then
          Z  = 24
          aM = 51.9405097d0
        endif
        if (Sb == 'mn') then
          Z  = 25
          aM = 54.9380463d0
        endif
        if (Sb == 'fe') then
          Z  = 26
          aM = 55.9349393d0
        endif
        if (Sb == 'co') then
          Z  = 27
          aM = 58.9331978d0
        endif
        if (Sb == 'ni') then
          Z  = 28
          aM = 57.9353471d0
        endif
        if (Sb == 'cu') then
          Z  = 29
          aM = 62.9295992d0
        endif
        if (Sb == 'zn') then
          Z  = 30
          aM = 63.9291454d0
        endif
        if (Sb == 'ga') then
          Z  = 31
          aM = 68.9255809d0
        endif
        if (Sb == 'ge') then
          Z  = 32
          aM = 73.9211788d0
        endif
        if (Sb == 'as') then
          Z  = 33
          aM = 74.9215955d0
        endif
        if (Sb == 'se') then
          Z  = 34
          aM = 79.9165205d0
        endif
        if (Sb == 'br') then
          Z  = 35
          aM = 78.9183361d0
        endif
        if (Sb == 'kr') then
          Z  = 36
          aM = 83.80
        endif
        if (Sb == 'rb') then
          Z  = 37
          aM = 85.4678
        endif
        if (Sb == 'sr') then
          Z  = 38
          aM = 87.62
        endif
        if (Sb == 'y') then
          Z  = 39
          aM = 88.9059
        endif
        if (Sb == 'zr') then
          Z  = 40
          aM = 91.22
        endif
        if (Sb == 'nb') then
          Z  = 41
          aM = 92.9064
        endif
        if (Sb == 'mo') then
          Z  = 42
          aM = 95.94
        endif
        if (Sb == 'tc') then
          Z  = 43
          aM = 98
        endif
        if (Sb == 'ru') then
          Z  = 44
          aM = 101.07
        endif
        if (Sb == 'rh') then
          Z  = 45
          aM = 102.9055
        endif
        if (Sb == 'pd') then
          Z  = 46
          aM = 106.4
        endif
        if (Sb == 'ag') then
          Z  = 47
          aM = 107.868
        endif
        if (Sb == 'cd') then
          Z  = 48
          aM = 112.41
        endif
        if (Sb == 'in') then
          Z  = 49
          aM = 114.82
        endif
        if (Sb == 'sn') then
          Z  = 50
          aM = 118.69
        endif
        if (Sb == 'sb') then
          Z  = 51
          aM = 121.75
        endif
        if (Sb == 'te') then
          Z  = 52
          aM = 127.60
        endif
        if (Sb == 'i') then
          Z  = 53
          aM = 126.9045
        endif
        if (Sb == 'xe') then
          Z  = 54
          aM = 131.30
        endif
        if (Sb == 'cs') then
          Z  = 55
          aM = 132.9054
        endif
        if (Sb == 'ba') then
          Z  = 56
          aM = 137.33
        endif
        if (Sb == 'la') then
          Z  = 57
          aM = 138.9055
        endif
        if (Sb == 'ce') then
          Z  = 58
          aM = 140.12
        endif
        if (Sb == 'pr') then
          Z  = 59
          aM = 140.9077
        endif
        if (Sb == 'nd') then
          Z  = 60
          aM = 144.24
        endif
        if (Sb == 'pm') then
          Z  = 61
          aM = 145
        endif
        if (Sb == 'sm') then
          Z  = 62
          aM = 150.4
        endif
        if (Sb == 'eu') then
          Z  = 63
          aM = 151.96
        endif
        if (Sb == 'gd') then
          Z  = 64
          aM = 157.25
        endif
        if (Sb == 'tb') then
          Z  = 65
          aM = 158.9254
        endif
        if (Sb == 'dy') then
          Z  = 66
          aM = 162.50
        endif
        if (Sb == 'ho') then
          Z  = 67
          aM = 164.9304
        endif
        if (Sb == 'er') then
          Z  = 68
          aM = 167.26
        endif
        if (Sb == 'tm') then
          Z  = 69
          aM = 168.9342
        endif
        if (Sb == 'yb') then
          Z  = 70
          aM = 173.04
        endif
        if (Sb == 'lu') then
          Z  = 71
          aM = 174.967
        endif
        if (Sb == 'hf') then
          Z  = 72
          aM = 178.49
        endif
        if (Sb == 'ta') then
          Z  = 73
          aM = 180.9479
        endif
        if (Sb == 'w') then
          Z  = 74
          aM = 183.85
        endif
        if (Sb == 're') then
          Z  = 75
          aM = 186.207
        endif
        if (Sb == 'os') then
          Z  = 76
          aM = 190.2
        endif
        if (Sb == 'ir') then
          Z  = 77
          aM = 192.22
        endif
        if (Sb == 'pt') then
          Z  = 78
          aM = 195.09
        endif
        if (Sb == 'au') then
          Z  = 79
          aM = 196.9665
        endif
        if (Sb == 'hg') then
          Z  = 80
          aM = 200.59
        endif
        if (Sb == 'tl') then
          Z  = 81
          aM = 204.37
        endif
        if (Sb == 'pb') then
          Z  = 82
          aM = 207.2
        endif
        if (Sb == 'bi') then
          Z  = 83
          aM = 208.9804
        endif
        if (Sb == 'po') then
          Z  = 84
          aM = 209
        endif
        if (Sb == 'at') then
          Z  = 85
          aM = 210
        endif
        if (Sb == 'rn') then
          Z  = 86
          aM = 222
        endif
        if (Sb == 'fr') then
          Z  = 87
          aM = 223
        endif
        if (Sb == 'ra') then
          Z  = 88
          aM = 226.0254
        endif
        if (Sb == 'ac') then
          Z  = 89
          aM = 227.0278
        endif
        if (Sb == 'th') then
          Z  = 90
          aM = 232.0381
        endif
        if (Sb == 'pa') then
          Z  = 91
          aM = 231.0359
        endif
        if (Sb == 'u') then
          Z  = 92
          aM = 238.029
        endif
        if (Sb == 'np') then
          Z  = 93
          aM = 237.0482
        endif
        if (Sb == 'pu') then
          Z  = 94
          aM = 244
        endif
        if (Sb == 'am') then
          Z  = 95
          aM = 243
        endif
        if (Sb == 'cm') then
          Z  = 96
          aM = 247
        endif
        if (Sb == 'bk') then
          Z  = 97
          aM = 247
        endif
        if (Sb == 'cf') then
          Z  = 98
          aM = 251
        endif
        if (Sb == 'es') then
          Z  = 99
          aM = 254
        endif
        if (Sb == 'fm') then
          Z  = 100
          aM = 257
        endif
        if (Sb == 'md') then
          Z  = 101
          aM = 258
        endif
        if (Sb == 'no') then
          Z  = 102
          aM = 259
        endif
        if (Sb == 'lr') then
          Z  = 103
          aM = 260
        endif
      end subroutine

      end program
