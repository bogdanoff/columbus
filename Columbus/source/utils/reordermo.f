!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      program reordermo
       implicit none
c  reorders the MOs from the mo coefficient file
c  this should enable moving diffuse MOs far into the
c  VO space, in order to improve mcscf convergence
c
c  namelist input
c
c    &input
c     mocoef           mocoefficient input file name
c     reordered        mocoefficient output file name
c     newmo(old basis function number,irrep) = new basis function number
c    &end
c
c     e.g.  exchange basis functions 2 and 3 in irrep 2
c
c      newmo(2,2)=3,
c      newmo(3,2)=2,
c      newmo(3,2)=-1   removes orbital three of irrep 2
c

      integer mxtitle, ntitle, filerr, syserr, mocoefin, mocoefout
      integer maxbfn,ierr
      parameter (mxtitle=20, maxbfn=710)
      character*80 titles(mxtitle),afmt
      character*4  labels(8)
      integer i,j,offsetocc,offsetc
      integer occlen,nsym,nbfpsy(8),nmopsy(8),clen
      real*8 c(maxbfn*maxbfn),scratch(maxbfn*maxbfn)
      real*8 occnew(maxbfn),occ(maxbfn)
      character*80 mocoef,reordered
      integer newmo(maxbfn,8),redmo,offsetoccnew,offsetcnew
      integer nmopsynew(8),moinqu
      logical nooccvec
      namelist /input/ newmo,mocoef,reordered
c
c      $input
c        newmo(bfn,irrep#) = newbfn
c      $end
c
c
      integer sym(maxbfn,8)
      integer left(maxbfn),right(maxbfn)

      open(6,file='reordermols',status='unknown')

      write(6,*) '===== Program reordermo ====='
      call izero_wr(maxbfn*8,newmo,1)
      call izero_wr(maxbfn*8,sym,1)
      mocoef='x'
      reordered='x'

      open(unit=10,file='reordermoin',status='old',err=400)
      goto 401
      
400   write(6,500)
500   format('Please create a reordermoin file, example:'//
     & " --- reordermoin --- "/
     & "&input"/"mocoef='mocoef'"/"reordered='mocoef.new'"/
     & "newmo(2,2)=3,"/"newmo(3,2)=2,"/"&end"/"------"/ )
      call bummer('reordermoin missing, see reordermols
     & for a template',0,2)
     
401   call echoin(10,6,ierr)
      rewind(10)
      read(10,input,end=100)
 100  continue
      close(10)

      if ( mocoef .eq. 'x' )
     . call bummer('enter original MO coefficient file name ',0,2)
      if ( reordered .eq. 'x' )
     . call bummer('enter reordered MO coefficient file name ',0,2)

      ierr=0
      do i=1,8
       call izero_wr(maxbfn,left,1)
       call izero_wr(maxbfn,right,1)
c
c input  newmo(2,1) = 1
c        newmo(2,1) = 2    -> the last entry will be read, only
c

c  # accumulate information from the input data

       do j=1,maxbfn
        if (newmo(j,i).gt.0) then
             left(j)=1
          if ( right(newmo(j,i)).eq.0 ) then
             right(newmo(j,i))=1
          else
             write(6,1001) j,i,newmo(j,i)
             ierr=ierr+1
          endif
1001     format(1x,'mapping error newmo(',i2,',',i2,')=',i2)
        endif
       enddo

c  # complete newmo arrays by setting default mapping newmo(j,i)=j

       do j=1,maxbfn
        if (newmo(j,i).eq.0) then
           left(j) = 1
          if ( right(j).eq.0 ) then
             right(j)=1
          else
             write(6,1002) j,i,newmo(j,i)
1002     format(1x,'implicit mapping error newmo(',i2,',',i2,')=',i2)
             ierr=ierr+1
          endif
         newmo(j,i)=j
        endif
       enddo



       do j=1,maxbfn
        if (left(j).ne.right(j)) then
          call bummer('incomplete permutation for drt # ',i,0)
         ierr=ierr+1
        endif
       enddo
c     if (ierr.gt.0) then
c        write(*,1002) (left(j),j=1,maxbfn)
c        write(*,1003) (right(j),j=1,maxbfn)
c002   format('left:',50(30i2/))
c003   format('right:',50(30i2/))
c     endif
      enddo

      if (ierr.gt.0) then
1100   format(' mapping error: '
     . /15x,'the new mo number on the right hand side appears',
     . /15x,'several times for one DRT.')
1101   format(' implicit mapping error: '
     . /15x,'there was no complete permutation defined, hence',
     . /15x,'collision with the implicit mapping, i.e. newmo(j,i)=j')

       write(6,1100)
       write(6,1101)
       call bummer('input errors!',ierr,2)
       endif


      write(*,*) 'opening old mocoefficient file:',mocoef
      open(unit=11,file=mocoef,status='old')
      clen=maxbfn*maxbfn
      call moread(11,10,filerr,syserr,mxtitle,ntitle,titles,
     . afmt,nsym,nbfpsy,nmopsy,labels,clen,c)

      write(*,*) 'Titles from mocoef file:'
      write(*,*) (titles(i),i=1,ntitle)
      write(*,900) nsym
      write(*,901) (nbfpsy(i),i=1,nsym)
      write(*,902) (nmopsy(i),i=1,nsym)

 900  format(' number of irreducible representations:',i4)
 901  format(' number of basis functions per irrep:',8i4)
 902  format(' number of molecular orbitals per irrep:',8i4)
 903  format(' irrep ',i3,' MO ', i3, 'incorrect value:',i4)


      ierr=0
      occlen=0
      clen=0
      do i=1,nsym
       clen=clen+nmopsy(i)*nbfpsy(i)
       occlen=occlen+nmopsy(i)
       do j=1,nmopsy(i)
       if (newmo(j,i).eq.0 .or. newmo(j,i).gt.nmopsy(i)) then
          write(*,903) i,j,newmo(j,i)
          ierr=ierr+1
       endif
       enddo
      enddo
       if (ierr.ne.0) call bummer ('inconsistent input, ierr=',ierr,2)

      write(*,*) ' reading mocoefficients ... '
      call moread(11,20,filerr,syserr,mxtitle,ntitle,titles,
     . afmt,nsym,nbfpsy,nmopsy,labels,clen,c)

      call prblks('Original MO coefficients',c,nsym,nbfpsy,nmopsy,
     .            'SAO','MO',1,6)

      filerr=moinqu(11,40) 
      if (filerr.ne.0)  then
          nooccvec=.true.
          else
          nooccvec=.false.
          write(*,*) 'reading occupation numbers ... '
          call moread(11,40,filerr,syserr,mxtitle,ntitle,titles,
     .         afmt,nsym,nbfpsy,nmopsy,labels,occlen,occ)
      endif
      close(11)


      offsetocc=1
      offsetoccnew=1
      offsetc  =1
      offsetcnew  =1
      do i=1,nsym
      call wzero(nbfpsy(i)*nmopsy(i),scratch(offsetcnew),1)
      call dcopy_wr(nbfpsy(i)*nmopsy(i),c(offsetc),1,
     .   scratch(offsetcnew),1)
      if (nooccvec) then
      call wzero(nmopsy(i),occnew(offsetoccnew),1)
      else
      call dcopy_wr(nmopsy(i),occ(offsetocc),1,occnew(offsetoccnew),1)
      endif
      write(*,*) '===========  reordering irrep ',i,'============='
        call reorder(newmo(1,i),c(offsetc),occ(offsetocc),nbfpsy(i),
     .  nmopsy(i),scratch(offsetcnew),occnew(offsetoccnew),redmo)
       offsetc=offsetc+nmopsy(i)*nbfpsy(i)
       offsetcnew=offsetcnew+redmo*nbfpsy(i)
       offsetocc=offsetocc+nmopsy(i)
       offsetoccnew=offsetoccnew+redmo
       nmopsynew(i)=redmo
      enddo

       write(*,*) 'writing reordered MOs to ',reordered

       open(unit=12,file=reordered,status='unknown')
       call mowrit(12,10,filerr,syserr,ntitle,titles,afmt,
     .  nsym,nbfpsy,nmopsynew,labels,scratch)
       write(*,*) '  writing MOs ... '
       call mowrit(12,20,filerr,syserr,ntitle,titles,afmt,
     .  nsym,nbfpsy,nmopsynew,labels,scratch)

      call prblks('Reordered MO coefficients',scratch,nsym,nbfpsy,
     .            nmopsynew,'SAO','MO',1,6)

       if (.not.nooccvec) then
       write(*,*) '  writing occupation numbers ... '
       call mowrit(12,40,filerr,syserr,ntitle,titles,afmt,
     .  nsym,nbfpsy,nmopsynew,labels,occnew)
       endif

       close(12)
       close(6)
       call bummer('normal termination',0,3)
       stop
       end



       subroutine reorder( sym,c,occ,nbf,nmo,cnew,occnew,redmo)
        implicit none 
        integer i,nbf,nmo,sym(*),redmo
       real*8 c(nbf,nmo), occ(nmo),cnew(nbf,nmo)
       real*8 tmp,occnew(nmo)

       write(*,*) 'mapping vector: ', (sym(i),i=1,nmo)
        redmo=nmo 
        do i=1,nmo
         if (i.ne.sym(i) .and. sym(i).ne.-1 ) then
          write(*,*) '    copying mo',i,' to position ',sym(i)
          occnew(sym(i))=occ(i)
          call dcopy_wr(nbf,c(1,i),1,cnew(1,sym(i)),1)
         elseif (sym(i).eq.-1) then
            redmo=redmo-1
         endif
        enddo
        return
        end






