!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      program shavittgraph 
       implicit none
      include "../colib/getlcl.h"

c  allocate space and call the driver routine.
c
c  ifirst = first usable space in the the real*8 work array a(*).
c  lcore  = length of workspace array in working precision.
c  a(*)   = workspace array. a(ifirst : ifirst+lcore-1) is useable.
c
      integer  lcore
c
c     #  a(*) should be declared below within mdc blocks.
c
c     # local...
c     # lcored = default value for lcore.
c     # ierr   = error return code.
c
      integer lcored, ierr
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
c
c     # use standard f90 memory allocation.
c
      parameter ( lcored = 2500000 )
      real*8, allocatable :: a(:)
c
      call getlcl( lcored, lcore, ierr )
      if ( ierr .ne. 0 ) then
         call bummer('cipc: from getlcl: ierr=',ierr,faterr)
      endif
c
      allocate( a(lcore),stat=ierr )
      if ( ierr .ne. 0 ) then
         call bummer('cipc: allocate failed ierr=',ierr,faterr)
      endif

c
c     # lcore,  and ifirst should now be defined.
c     # the values of lcore,  and ifirst are passed to
c     # driver() to be printed.
c     # since these arguments are passed by expression, they
c     # must not be modified within driver().
c
      call driver( a, lcore  )
c
c     # return from driver() implies successful execution.
c
      call bummer('normal termination',0,3)
      stop 'end of shavittgraph'
      end

      subroutine driver( core, lencor )
c
c this program prints the csfs according to either csf number or
c indexed csf number based on wave function contribution.
c
c ci coefficients are read from the sequential output ci file.
c csf step vectors are generated from the cidrtfl.
c
c  version log:
c  28-sep-91 written by ron shepard.  based in part on program mcpc.
c
       implicit none
c     # nrowmx = the maximum number of rows in the drt.
c     # nimomx = the maximum number of internal orbitals.
c     # nmotmx = the maximum number of orbitals total.
c
      integer    nrowmx,        nimomx,    nmotmx
      parameter( nrowmx=2**10-1, nimomx=128, nmotmx=510 )
c
c.....include 'cdrt.h'
      integer          l,                             syml,
     & modrt,          mapl,            level,           mapd
      common /cdrt/    l(0:3,nrowmx,3), syml(nimomx),
     & modrt(nimomx),  mapl(2,nmotmx),  level(nmotmx),   mapd(2,nimomx)
c.....end of cdrt
c
      integer    nflmax
      parameter( nflmax=20 )
      integer         iunits
      common /cfiles/ iunits(nflmax)
      integer      nlist
      equivalence (nlist,iunits(1))
      integer      nin
      equivalence (nin,iunits(2))
      integer      ngnu
      equivalence (ngnu,iunits(3))
      integer      ndrt
      equivalence (ndrt,iunits(4))
      integer      nweights
      equivalence (nweights,iunits(6))
      integer      nwbin   
      equivalence (nwbin,iunits(7))
      character*60    flname
      common /cfname/ flname(nflmax)
c
      character      slabel*4,  cstep*1,    cdet*1
      common /charc/ slabel(8), cstep(0:3), cdet(0:3)
c
      integer       mult,      nsym, ssym
      common /csym/ mult(8,8), nsym, ssym
c
c.....include 'extc.h'
      integer
     & nmpsy,       ncsfv,    nexw,      aboff,      nvalw,
     & ivcsf1,    valw0,    nmot,     nimot,     nemot,
     & nfct,      nvalwt,   nzwalk,   nabtot
      common /extc/
     & nmpsy(8),   ncsfv(4), nexw(8,4), aboff(8,4), nvalw(4),
     & ivcsf1(4), valw0(4), nmot,     nimot,     nemot,      nfct,
     & nvalwt,    nzwalk,   nabtot
c......end of extc
c
c     # dummy.
      integer lencor
      real*8 core(lencor)
c
c     # local.
      integer  buf, nwlkmx, nfvt, lenbuf,
     & nvwxy, ref,   itotal, 
     & nrow,i,
     & nwalk, smult, irow,j, nvalwz,arcweight(50),nodeweight(50),
     . intweight(50)
       integer ncsft,linput(9)
c
      integer    nengmx,    ntitmx,    ninfmx
      parameter( nengmx=30, ntitmx=30, ninfmx=30 )
c
      integer mu(1), nj(nrowmx), njskp(1), a(nrowmx), b(nrowmx)
      integer level2(nrowmx)
      integer xbar(4)
      character*80 title(ntitmx)
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer    toindx,   to01
      parameter( toindx=0, to01=1 )

      integer  iwfiles    
      integer, parameter :: maxfiles=50
      character*20  suffix(maxfiles)

c
      integer  forbyt, atebyt
      external forbyt, atebyt
c
c     # set default unit numbers and file names.
c
      nlist  = 6
      nin    = 5
      ndrt   = 21
      nweights = 23
      nwbin    =24
      ngnu     =25
c
      flname(1) = 'shavittls'
      flname(2) = 'shavittin'
      flname(3) = 'gnuplot.in'
      flname(4) = 'cidrtfl'
      flname(6) = 'fweights'
      flname(7) = 'fweights.bin'
c
c
c     # open the input and listing files, if necessary...

      open(file=flname(2),unit=nin,form='formatted')
c
c     # use preconnected units 5 (stdin) and 6 (stdout).
c
      call ibummr( nlist )
c
      write(nlist,6000)
6000  format(/' program shavitt      '//
     & ' print the weighted DRT graph'//
     & ' written by: Thomas Mueller & Ron Shepard '//
     & ' version date: 16-jun-09')
c
      call who2c( 'shavitt', nlist )
      call headwr(nlist,'SHAVITT','5.9     ')
c
      write (nlist,6040) lencor
6040  format(/' workspace allocation parameters: lencor=',i10)


c
c    first line reference file
c
       iwfiles=1
 149   read(nin,*,end=150) suffix(iwfiles)
       iwfiles=iwfiles+1
       goto 149
 150   continue
       iwfiles=iwfiles-1

       write(6,*) 'reference:',
     .      flname(7)(1:len_trim(flname(7)))
     .      //suffix(1)(1:len_trim(suffix(1)))
       do i=2,iwfiles
       write(6,*)
     .     '  further:',flname(7)(1:len_trim(flname(7)))
     .       //suffix(i)(1:len_trim(suffix(i)))
       enddo

c
c     # get the header info from the drt file.
c
      open(unit=ndrt,file=flname(4),status='old')
c
c     # core(*)=> buf
c
      buf = 1
c
      nwlkmx = lencor

      call rdhdrt(
     & title,  nmot,      nimot,  nfct,
     & nfvt,   nrow,      nsym,   ssym,
     & xbar,   nvalw,     ncsft,  lenbuf,
     & nwalk,  nimomx,    nmotmx, nrowmx,
     & nwlkmx, core(buf), nlist,  ndrt )
c
      if ( nfct .gt. nimomx ) then
         call bummer('from rdhdrt(), (nfct-nimomx)=',
     &    (nfct-nimomx), faterr )
      endif
c
      nzwalk = xbar(1)
      nvalwz = nvalw(1)
      nvwxy  = nvalw(2) + nvalw(3) + nvalw(4)
      nvalwt = nvalwz + nvwxy
c
c     # core(*)=> ref(nzwalk), iwsym(nvalwt), limvec(nwalk), buf(lenbuf)
c
      ref    = 1
c     iwsym  = ref    + forbyt( nzwalk )
c     limvec = iwsym  + forbyt( nvalwt )
c     buf    = limvec + forbyt( nwalk )
      buf    = ref + forbyt( nzwalk )
c
      itotal = buf + forbyt( lenbuf ) -1
      if ( itotal .gt. lencor ) then
         call bummer('for rddrt() call, (itotal-lencor)=',
     &    (itotal-lencor), faterr )
      endif
c
      call rddrt(
     & nmot,      nimot,      nsym,        nrow,
     & nwalk,     lenbuf,     level,       nmpsy,
     & modrt,      mu,          syml(1),
     & nj,        njskp,      a,           b,  
     & l,                       
     & nrowmx,    nvalwz,     nvwxy,    
     & slabel,      smult,
     & core(buf) )

c
c     at each level n the relation holds
c     a(n)+b(n)+c(n)=n
c     2*a(n)+b(n) = N(n)   
c     S(n)=b(n)/2 
c
c    # a(*) number of doubly occupied orbitals
c    # b(*) number of singly occupied orbitals
c    # c(*) number of empty orbitals
c
c     # compute c(nrow) 
       irow=0
       do i=0,nimot
         do j=1,nj(i+1)
           irow=irow+1
            level2(irow)=i
         enddo
        enddo   
c
      close( unit=ndrt )
c
c     intweight=buf 
      do i=1,iwfiles
       nodeweight(i)=buf
       arcweight(i)=nodeweight(i)+atebyt(nrow)
       buf      =arcweight(i) + atebyt(4*nrow)
       if (buf.gt.lencor) call bummer('insufficient memory',lencor,2)
c
c   # read unformatted version of weights file
c   # there may be several files to be combined 
c   # start with a single one and add namelist input later
c
       open(
     . file=flname(7)(1:len_trim(flname(7)))//
     .      suffix(i)(1:len_trim(suffix(i))),
     .      unit=iunits(7),form='unformatted')
       read (iunits(7),err=1100) linput(1:9)
       write(6,*) 'operating on root=',linput(1) 
       if (linput(2).ne. ncsft) 
     .     call bummer('linput(2).ne.ncsft',linput(2),2)
       if (linput(3).ne. nvalwt) 
     .     call bummer('linput(3).ne.nvalwt',linput(3),2)
       if (linput(4).ne. nrow) 
     .     call bummer('linput(4).ne.nrow',linput(4),2)
       if (linput(5).ne. nimot) 
     .     call bummer('linput(4).ne.nimot',linput(5),2)
       if (linput(6).ne. nvalw(1)) 
     .     call bummer('linput(6).ne.nvalw(1)',linput(6),2)
       if (linput(7).ne. nvalw(2)) 
     .     call bummer('linput(7).ne.nvalw(2)',linput(7),2)
       if (linput(8).ne. nvalw(3)) 
     .     call bummer('linput(8).ne.nvalw(3)',linput(8),2)
       if (linput(9).ne. nvalw(4)) 
     .     call bummer('linput(9).ne.nvalw(4)',linput(9),2)
c      read(iunits(7),err=1101) core(intweight:intweight+nvalwt-1)
       read(iunits(7),err=1101)
       read(iunits(7),err=1102) core(nodeweight(i):nodeweight(i)+nrow-1)
       read(iunits(7),err=1103) core(arcweight(i):arcweight(i)+4*nrow-1)
       close(iunits(7))
      enddo

      open(file='shavittdrt_0.0001',unit=17)
      call create_newdrt(nimot,nrow,a,b,level2,
     .  core(nodeweight(1)),core(arcweight(1)),1.d-4,17)
      close(17)
      open(file='shavittdrt_0.0005',unit=17)
      call create_newdrt(nimot,nrow,a,b,level2,
     .  core(nodeweight(1)),core(arcweight(1)),5.d-4,17)
      close(17)
      open(file='shavittdrt_0.001',unit=17)
      call create_newdrt(nimot,nrow,a,b,level2,
     .  core(nodeweight(1)),core(arcweight(1)),1.d-3,17)
      close(17)
      open(file='shavittdrt_0.005',unit=17)
      call create_newdrt(nimot,nrow,a,b,level2,
     .  core(nodeweight(1)),core(arcweight(1)),5.d-3,17)
      close(17)

      open(file='gnuplot.reference',unit=iunits(3),form='formatted')
      call print_graph_density(nimot,nrow,a,b,level2,
     .  core(nodeweight(1)),core(arcweight(1)),iunits(3),'reference')
      close (iunits(3))

      open(file='pstricks.reference',unit=iunits(3),form='formatted')
      call print_graph_density_pstricks(nimot,nrow,a,b,level2,
     .  core(nodeweight(1)),core(arcweight(1)),iunits(3),'reference')
      close (iunits(3))


      do i=2,iwfiles
       open(file='gnuplot'//suffix(i)(1:len_trim(suffix(i))),
     .      unit=iunits(3),form='formatted')
       call daxpy_wr(nrow,-1.0d0,core(nodeweight(1)),1,
     .               core(nodeweight(i)),1)
       call daxpy_wr(4*nrow,-1.0d0,core(arcweight(1)),1,
     .               core(arcweight(i)),1)
      call print_graph_density(nimot,nrow,a,b,level2,
     .  core(nodeweight(i)),core(arcweight(i)),iunits(3),suffix(i))
      close (iunits(3))
      enddo
      return
1100  write(6,1000)
        stop 1100
1101  write(6,1001)
        stop 1101
1102  write(6,1002)
        stop 1102
1103  write(6,1003)
        stop 1103
1000  format('could not read header line')
1001  format('could not read intweight')
1002  format('could not read nodeweight')
1003  format('could not read arcweight')

      end



      subroutine print_graph_density
     .    (nimot,nrow, a,b,level2,node_density, arc_density, gnuplot,
     .    suffix)

c      print the arc and node density to the output file.
c      and write a gnuplot file 

      implicit none
      integer    nrowmx,        nimomx,    nmotmx
      parameter( nrowmx=2**10-1, nimomx=128, nmotmx=510 )
      character*(*) suffix
c
c.....include 'cdrt.h'
      integer          l,                             syml,
     & modrt,          mapl,            level,           mapd
      common /cdrt/    l(0:3,nrowmx,3),  syml(nimomx),
     & modrt(nimomx),  mapl(2,nmotmx),  level(nmotmx),   mapd(2,nimomx)
c.....end of cdrt

      integer:: lprint,nimot,nrow,gnuplot,level2(nrow)
      real*8 :: node_density(nrow), arc_density(0:3,nrow)  
      integer :: a(nrow),b(nrow),idrt

      integer  :: irow, istep, a_head, b_head, b_max, jrow, color
      real*8 :: b_inc, ratio, x_min, x_max, y_min, y_max, R_max_x
      real*8 :: R_max_y, scale_x, scale_y
      real*8 :: d_max, sqrt_d_max, d_small, arc_small, temp

      type xy_coord
         real :: x, y
      end type xy_coord
      type(xy_coord) :: coord(0:nrow)  
c eventual coordinates for the graph nodes.

      integer, parameter :: fill_negative = 1 
      integer, parameter :: fill_neutral = 2 
      integer, parameter :: fill_positive = 3 
      integer, parameter :: line_negative = 1
      integer, parameter :: line_neutral = 2
      integer, parameter :: line_positive = 3

      real*8, parameter :: page_factor = 10.0d0  / 7.5d0    

      real*8, parameter :: thresh = 1.d-4 

      character(len=1), parameter :: backslash='\\ ' 
      character(*), parameter :: afmt='(8a)'

         ! assume the output file has already been opened and correctly positioned.

         ! the output produced by this code may be used as
         !   %gnuplot gnuplot.data >gnuplot.ps
         ! or
         !   %gnuplot gnuplot.data | ps2pdf >gnuplot.pdf
         ! or
         !   %gnuplot <gnuplot.data
         ! depending on the terminal type.

         a_head = a(nrow)
         b_head = b(nrow)
         b_max  = maxval(b(1:nrow))
         
         if ( b_max <= 3 ) then  ! compute a reasonable graphical coordinate offset for b.
            b_inc = 0.25d0
         else
            b_inc = 0.75d0 / b_max
         endif

         R_max_x = 0.5d0 * b_inc  ! maximum circle radius in the x direction.
         R_max_y = 1.0d0 / 3.0d0 ! maximum circle radius in the y direction.

         ! write out the general gnuplot information.
         
         write(gnuplot,afmt) 
     .       '################## NEXT PLOT ##################### '
         write(gnuplot,afmt) 
     .       'set terminal postscript solid enhanced color'  ! color postscript.
         write(gnuplot,afmt) '#set terminal pdf enhanced'  ! pdf works, but it is not as flexible as postscript.

         write(gnuplot,afmt) 
     .        "set output 'testplot"//suffix(1:len_trim(suffix))//".ps'"
         write(gnuplot,afmt) 'unset border'
         write(gnuplot,afmt) 'set xtics 1'
         write(gnuplot,afmt) 'set ytics 1'
         write(gnuplot,afmt) 'set ytics nomirror'
         write(gnuplot,afmt) 'set xlabel "a"'
         write(gnuplot,afmt) 'set ylabel "level"'
         
         x_min = -R_max_x 
         x_max = a_head + b_inc * (b_head + 1) + R_max_x
         y_min = -R_max_y * page_factor
         y_max = (nimot) + R_max_y * page_factor
         write(gnuplot,'(a,f8.4,a,f8.4,a)') 
     .       'set xrange [', x_max, ':', x_min, ']'
         write(gnuplot,'(a,f8.4,a,f8.4,a)') 
     .       'set yrange [', y_min, ':', y_max, ']'
         
         ratio = (y_max-y_min) / (x_max-x_min)  ! the coordinate space is spread to fill the page.
         write(gnuplot,'(a,f8.4)') '#set size ratio ', ratio
         write(gnuplot,'(a)') 'set size 1,1'

         ! compute the coordinates for each node.
         
         coord(0)%x = a_head + b_inc * b_head
         coord(0)%y = 0.0d0
         do irow = 1, nrow
            coord(irow)%x = a(irow) + b_inc * b(irow)
            coord(irow)%y = level2(irow)
         enddo
         
         ! label the nodes and draw all of the arcs in the shavitt graph.
         
c        arc_small = thresh * maxval(abs(arc_density))
         arc_small = thresh 
         do irow = 1, nrow
            write(gnuplot,'(a,i4,a,f8.4,",",f8.4,a)') 
     .  'set label "', irow, '" at ', coord(irow), ' font "Courier,4"'
           do istep = 0, 3
             do idrt = 1,3 
             jrow = l(istep,irow,idrt)
             if ( jrow .ne. 0 ) then          ! draw the connection between the nodes.
                if ( arc_density(istep,irow) .ge. arc_small ) then
                   color = line_positive
                elseif ( arc_density(istep,irow) .le. -arc_small ) then
                   color = line_negative
                else  ! the arc density is too small, select a neutral color.
                   color = line_neutral
                endif
             write(gnuplot,'(a,f8.4,",",f8.4,a,f8.4,",",f8.4,a,i8,a)') 
     . 'set arrow from ', coord(irow), ' to ', coord(jrow), 
     .  ' nohead lt ', 
     .  color, ' lw 1'
             endif
             enddo 
            enddo
         enddo

         ! determine the x and y scale factors that result in true circles of maximum radius.
         ! for tall graphs, the y/x scale factor is large, and the limiting circle radius is determined
         ! by the requirement that the circles do not overlap vertically.  for short graphs with large b values,
         ! the limiting circle radius is determined by the requirement that the circles do not overlap horizontally.

c        d_max  = maxval(abs(node_density))
         d_max  = 1.0d0 
         if ( d_max .eq. 0.0d0 ) d_max = 1.0d0  ! reset to avoid infinities.
         sqrt_d_max = sqrt(d_max)
         d_small = thresh * d_max

         temp = ratio * page_factor  ! y/x ratio drawing scale factor.
         scale_x = min( R_max_y/temp, R_max_x) / sqrt_d_max
         scale_y = scale_x * temp
         
         write(gnuplot,'(a,es9.3,a,f8.4,a,f8.4,a)') 
     .    'set label "=+', d_max,'" at ', 
     .     coord(0)%x-sqrt_d_max*scale_x, ',', coord(0)%y, 
     .     ' left front font "Courier,14"'

         write(gnuplot,afmt) 'set parametric'
         write(gnuplot,afmt) 'unset key'
         write(gnuplot,afmt) 'plot [t=-pi:pi] ', backslash

         temp  = sqrt_d_max
         color = fill_positive
         write(gnuplot,fmt='(3x,4(f8.4,a),i4,2a)') 
     .          scale_x*temp, '*cos(t)+', coord(0)%x, ',', 
     .          scale_y*temp, '*sin(t)+', coord(0)%y,
     .         ' with filledcurve  closed lt ', color, ',', backslash

         do irow = 1, nrow
            temp = sqrt(abs(node_density(irow)))
            if ( node_density(irow) .ge. d_small ) then
               color = fill_positive
            elseif ( node_density(irow) .le. -d_small ) then
               color = fill_negative
            else  ! the node density is too small, select a neutral color.
               color = fill_neutral
            endif
            write(gnuplot,advance='no',fmt='(3x,4(f8.4,a),i4)')  
     .           scale_x*temp, '*cos(t)+', coord(irow)%x, ',',  
     .           scale_y*temp, '*sin(t)+', coord(irow)%y,
     .           ' with filledcurve closed lt ', color
            if ( irow .lt. nrow ) then
               write(gnuplot,afmt) ',', backslash
            endif
         enddo
         

      return

      end 
   
      subroutine rdhdrt(
     & title,  nmot,   niot,   nfct,
     & nfvt,   nrow,   nsym,   ssym,
     & xbar,   nvalw,  ncsft,  lenbuf,
     & nwalk,  niomx,  nmomx,  nrowmx,
     & nwlkmx, buf,    nlist,  ndrt )
c
c  read the header info from the drt file.
c
       implicit none
      integer nmot, niot, nfct, nfvt, nrow, nsym, ssym, ncsft,
     & lenbuf, nwalk, niomx, nmomx, nrowmx, nwlkmx, nlist, ndrt
      integer xbar(4), nvalw(4), buf(*)
      character*80 title
c
      integer nhdint, vrsion, ierr, nvalwt
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
*@ifdef spinorbit
      logical spnorb, spnodd
      integer lxyzir(3)
*@endif

c
c     # title card...
c
      rewind ndrt
      read(ndrt,'(a)',iostat=ierr)title
      if(ierr.ne.0)call bummer('rdhead: title ierr=',ierr,faterr)
*@ifdef spinorbit
      read(ndrt,'(2L4,3I4)',iostat=ierr)spnorb, spnodd, lxyzir
      if(ierr.ne.0)call bummer('rdhead: spnorb, ierr=',ierr,faterr)
      if (spnorb) then
        call bummer('cannot cope with SO-CI',0,0)
        call bummer('normal termination',0,3)
        stop
      endif
*@endif
c
      write(nlist,6010) title
c
c     # number of integer drt parameters, buffer length, and version.
c
      call rddbl( ndrt, 3, buf, 3 )
c
      vrsion = buf(1)
      lenbuf = buf(2)
      nhdint = buf(3)
c
*@ifdef spinorbit
      if ( vrsion .ne. 6 ) then
       call bummer(' drt version 6 required',vrsion,2)
      endif
*@else
*      if ( vrsion .ne. 3 ) then
*         write(nlist,*)
*     &    '*** warning: rdhdrt: drt version .ne. 3 *** vrsion=',vrsion
*      endif
*@endif
c
c     # header info...
c
      call rddbl( ndrt, lenbuf, buf, nhdint )
c
      nmot     = buf(1)
      niot     = buf(2)
      nfct     = buf(3)
      nfvt     = buf(4)
      nrow     = buf(5)
      nsym     = buf(6)
      ssym     = buf(7)
      xbar(1)  = buf(8)
      xbar(2)  = buf(9)
      xbar(3)  = buf(10)
      xbar(4)  = buf(11)
      nvalw(1) = buf(12)
      nvalw(2) = buf(13)
      nvalw(3) = buf(14)
      nvalw(4) = buf(15)
      ncsft    = buf(16)
*@ifdef spinorbit
      spnorb  = buf(17) .eq. 1
      spnodd  = buf(18) .eq. 1
      lxyzir(1) = buf(19)
      lxyzir(2) = buf(20)
      lxyzir(3) = buf(21)
      if (spnorb) then
         call bummer('cannot cope with SO-CI',0,0)
         call bummer('normal termination',0,3)
         stop
      endif
*@endif
c
      nwalk  = xbar(1)  + xbar(2)  + xbar(3)  + xbar(4)
      nvalwt = nvalw(1) + nvalw(2) + nvalw(3) + nvalw(4)
c
      write(nlist,6020) nmot, niot, nfct, nfvt, nrow, nsym, ssym, lenbuf
*@ifdef spinorbit
     & , spnorb, spnodd, lxyzir
*@endif
      write(nlist,6100)'nwalk,xbar', nwalk, xbar
      write(nlist,6100)'nvalwt,nvalw', nvalwt, nvalw
      write(nlist,6100)'ncsft', ncsft
c
      if ( niot .gt. niomx ) then
         write(nlist,6900)'niot,niomx', niot, niomx
         call bummer('rdhead: niomx=',niomx,faterr)
      endif
      if ( nmot .gt. nmomx ) then
         write(nlist,6900)'nmot,nmomx', nmot, nmomx
         call bummer('rdhead: nmomx=',nmomx,faterr)
      endif
      if ( nrow .gt. nrowmx ) then
         write(nlist,6900)'nrow,nrowmx', nrow, nrowmx
         call bummer('rdhead: nrowmx=',nrowmx,faterr)
      endif
c
      return
c
6010  format(/' drt header information:'/1x,a)
6020  format(
     & ' nmot  =',i6,' niot  =',i6,' nfct  =',i6,' nfvt  =',i6/
     & ' nrow  =',i6,' nsym  =',i6,' ssym  =',i6,' lenbuf=',i6:/
     & ' spnorb=',l6,' spnodd=',l6,' lxyzir(1:3)=',3i2)
6100  format(1x,a,':',t15,5i9)
6900  format(/' error: ',a,3i10)
      end

      subroutine rddrt(
     & nmot,    niot,    nsym,   nrow,
     & nwalk,   lenbuf,  map,    nmpsy,
     &  modrt,   mu,     syml,
     & nj,      njskp,   a,      b, 
     & l,                
     & nrowmx,  nvalwz,  nvwxy,  
     &      slabel, smult,
     & buf )
c
c  read the drt arrays from the drt file.
c  title and dimension information have already been read.
c  the file is assumed to be positioned correctly for the
c  next records.
c  vectors are buffered in records of length lenbuf.
c
       implicit none
      integer nmot, niot, nsym, nrow, nwalk, lenbuf, nrowmx,
     & nvalwz, nvwxy,  smult
      integer map(nmot)
      integer nmpsy(nsym)
      integer modrt(niot)
      integer syml(niot)
      integer nj(nrow)
      integer njskp(nrow)
      integer mu(niot)
      integer a(nrow)
      integer b(nrow)
      integer l(0:3,nrowmx,3)
      integer buf(lenbuf)
      character*4 slabel(8)
c
      integer    nflmax
      parameter( nflmax=20 )
      integer         iunits
      common /cfiles/ iunits(nflmax)
      integer      nlist
      equivalence (nlist,iunits(1))
      integer      nin
      equivalence (nin,iunits(2))
      integer      ciuvfl
      equivalence (ciuvfl,iunits(3))
      integer      ndrt
      equivalence (ndrt,iunits(4))
c
c     # local:
      integer nlevel, i, ntot
      integer nsm(8)
      integer*4 buf32(8)
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)

      integer       mult,      nnsym, ssym
      common /csym/ mult(8,8), nnsym, ssym

c
      nlevel = niot + 1
c
c     # slabel(*)...
      call rddbl( ndrt, lenbuf, buf32, nsym )
      do 10 i = 1, nsym
         write( slabel(i), fmt='(a4)' ) buf32(i)
 10    continue
c
c     # map(*)...
      call rddbl( ndrt, lenbuf, map, nmot )
      write(nlist,6100)'map(*)=',map
c
c     # mu(*)...
      call rddbl( ndrt, lenbuf, buf, niot )
      write(nlist,6100)'mu(*)=',(buf(i),i=1,niot)
c
c     # nmpsy(*)...
      call rddbl( ndrt, lenbuf, nmpsy, nsym )
c
c     # nexo(*)...
      call rddbl( ndrt, lenbuf, buf, -nsym )
c
c     # nviwsm(*)...
      call rddbl( ndrt, lenbuf, buf, -4*nsym )
c
c     # ncsfsm(*)...
      call rddbl( ndrt, lenbuf, buf, -4*nsym )
c
c     # modrt(*)...
      call rddbl( ndrt, lenbuf, modrt(1), niot )
c
c     # syml(*)...
      call rddbl( ndrt, lenbuf, syml, niot )

c     # change from global orbital to symmetry orbital numbering...
      ntot = 0
      do 20 i = 1, 8
         nsm(i) = ntot
         ntot   = ntot + nmpsy(i)
20    continue
      do 30 i = 1, niot
         modrt(i) = modrt(i) - nsm( syml(i) )
30    continue
c
      write(nlist,6100)'syml(*) =',(syml(i),i=1,niot)
      write(nlist,6100)'rmo(*)=',(modrt(i),i=1,niot)
c
c     # nj(*)...
      call rddbl( ndrt, lenbuf, nj, nlevel )
c
c     # njskp(*)...
      call rddbl( ndrt, lenbuf, buf, -nlevel )
c
c     # a(*)...
      call rddbl( ndrt, lenbuf,a, nrow )
c
c     # b(*)...
      call rddbl( ndrt, lenbuf,b, nrow )
c     # compute smult=b(nrow)+1
      smult = b(nrow)+1
c
c     # l(*,*,*)...
      do 40 i = 1, 3
         call rddbl( ndrt, lenbuf, l(0,1,i), 4*nrow )
40    continue
c
      return
6100  format(1x,a,(t12,20i3))
      end


      subroutine print_graph_density_pstricks
     .    (nimot,nrow, a,b,level2,node_density, arc_density,pstricks,
     .    suffix)

c      print the arc and node density to the output file.
c      and write a gnuplot file 

      implicit none
      integer    nrowmx,        nimomx,    nmotmx
      parameter( nrowmx=2**10-1, nimomx=128, nmotmx=510 )
      character*(*) suffix
c
c.....include 'cdrt.h'
      integer          l,                             syml,
     & modrt,          mapl,            level,           mapd
      common /cdrt/    l(0:3,nrowmx,3),  syml(nimomx),
     & modrt(nimomx),  mapl(2,nmotmx),  level(nmotmx),   mapd(2,nimomx)
c.....end of cdrt

      integer:: nimot,nrow,pstricks,level2(nrow),lprint
      real*8 :: node_density(nrow), arc_density(0:3,nrow)  
      integer :: a(nrow),b(nrow),idrt

      integer  :: irow, istep, a_head, b_head, b_max, jrow, color
      real*8 :: b_inc, ratio, x_min, x_max, y_min, y_max, R_max_x
      real*8 :: R_max_y, scale_x, scale_y
      real*8 :: d_max, sqrt_d_max, d_small, arc_small, temp

      type xy_coord
         real :: x, y
      end type xy_coord
      type(xy_coord) :: coord(0:nrow)  
c eventual coordinates for the graph nodes.

      integer, parameter :: fill_negative = 1 
      integer, parameter :: fill_neutral = 2 
      integer, parameter :: fill_positive = 3 
      integer, parameter :: line_negative = 1
      integer, parameter :: line_neutral = 2
      integer, parameter :: line_positive = 3

      real*8, parameter :: page_factor = 10.0d0  / 7.5d0    

      real*8, parameter :: thresh = 1.d-4 

      character(len=1), parameter :: backslash='\\ ' 
      character(*), parameter :: afmt='(8a)'

      integer jj,i1,spalte,icase,izeile,i,j,ispalte
      real, allocatable :: radarray(:,:)
      integer, allocatable :: idxarray(:,:),colarray(:,:)
       real :: rowsep,colsep 
      character*5  ccolour(0:3),clabel,labelrow(510)
      integer  nlabelrow(510),nccolour(0:3)

      data ccolour /'yello','red  ','black  ','blue'/

      data nccolour /5,3,5,4/


         ! assume the output file has already been opened and correctly positioned.

         ! the output produced by this code may be used as
         !   %gnuplot gnuplot.data >gnuplot.ps
         ! or
         !   %gnuplot gnuplot.data | ps2pdf >gnuplot.pdf
         ! or
         !   %gnuplot <gnuplot.data
         ! depending on the terminal type.

         a_head = a(nrow)
         b_head = b(nrow)
         b_max  = maxval(b(1:nrow))
         
         if ( b_max <= 3 ) then  ! compute a reasonable graphical coordinate offset for b.
            b_inc = 0.25d0
         else
            b_inc = 0.75d0 / b_max
         endif

         R_max_x = 0.5d0 * b_inc  ! maximum circle radius in the x direction.
         R_max_y = 1.0d0 / 3.0d0 ! maximum circle radius in the y direction.
         d_small=1.d-4
         arc_small=1.d-4

c
c     idxarray(zeile,spalte)= irow/b/a 
c     radarray(zeile,spalte)= radius
c     colcarray(zeile,spalte)= color
c
         allocate(idxarray(nimot+4,a_head*(b_max+1)+b_head+2))
         allocate(radarray(nimot+4,a_head*(b_max+1)+b_head+2))
         allocate(colarray(nimot+4,a_head*(b_max+1)+b_head+2))
         idxarray=-99
         radarray=0.0d0
         colarray=0


c     row 1: b=2S+1
 
c     row 2: level, b values
      spalte=1
      do i=a_head, 0, -1
       if (i.eq.a_head) then 
           do j=b_head,0, -1
             spalte=spalte+1
             idxarray(2,spalte)=j
c            write(0,'(a,i3,a,i3,a,i3)') 
c    .           'A: a=',i,' b=',j,' spalte=',spalte
           enddo
       else
         do j= b_max, 0, -1 
           spalte=spalte+1
            idxarray(2,spalte)=j
c            write(0,'(a,i3,a,i3,a,i3)') 
c    .           'B: a=',i,' b=',j,' spalte=',spalte
         enddo
       endif
c      if (spalte.gt.a_head*(b_max+1)+b_head+1) then
c        write(0,*) 'head=',i,'spalte=',spalte 
c      endif
c      spalte=spalte+1
       enddo
       

c     row 4+nimot   a labels

      spalte=1
      do i=a_head, 0, -1
       if (i.eq.a_head) then 
           do j=b_head,0, -1
             spalte=spalte+1
           enddo
             idxarray(4+nimot,spalte)=i
       else
         do j= b_max, 0, -1 
           spalte=spalte+1
         enddo
            idxarray(4+nimot,spalte)=i
       endif
c      spalte=spalte+1
c      if (spalte.gt.a_head*(b_max+1)+b_head+1) stop 'spaltezugrossb'
       enddo
  
        
c    row 3 to 3+nimot 

        do irow=1,nrow
            temp = sqrt(abs(node_density(irow)))
            if ( node_density(irow) .ge. d_small ) then
               color = fill_positive
            elseif ( node_density(irow) .le. -d_small ) then
               color = fill_negative
            else  ! the node density is too small, select a neutral color.
               color = fill_neutral
!              write(0,'(a,i4,a,3(i4,2x))') 
!    .       ' DRTROW #',irow,' can be removed:',
!    .          level2(irow),a(irow),b(irow) 
            endif
!          write(0,'(a,i4,i4,a,i4,i6)') 
!    .    'irow,level2=',irow,level2(irow),
!    .    ' zeile,spalte=',nimot+3-level2(irow),
!    .                     spalte-a(irow)*(b_max+1)-b(irow)-1
          if (nimot+3-level2(irow).le.0) stop 'le0'
          if (nimot+3-level2(irow).gt.nimot+4) stop 'genimot4'
        idxarray(nimot+3-level2(irow),
     .     spalte-a(irow)*(b_max+1)-b(irow)) =irow
        radarray(nimot+3-level2(irow),
     .     spalte-a(irow)*(b_max+1)-b(irow)) =temp
        colarray(nimot+3-level2(irow),
     .     spalte-a(irow)*(b_max+1)-b(irow)) =color
        idxarray(nimot+3-level2(irow),1)=level2(irow)
        enddo 

c
c    write pstricks section
c
c     

         ! write out the general gnuplot information.
         rowsep=0.4
         colsep=1.0
         write(pstricks,'(a)') '\documentclass[10pt,a4paper]{article}'         
         write(pstricks,'(a)')
     .            '\textheight=21truecm \textwidth=15.5truecm '
         write(pstricks,'(a)') '\parindent=0pt'                               
         write(pstricks,'(a)') '\usepackage{pstricks,pst-node}'               
         write(pstricks,'(a)') '\begin{document}'
         write(pstricks,'(a,f6.2,a,f6.2,a)') 
     .            '\begin{pspicture}[showgrid=false](0,0)(',
     .            nimot+3*colsep,',', spalte*rowsep,')'               

        write(pstricks,*)'\rput{-90}(0,0){{'
c
c  write nodes
c
         write(pstricks,201) 
  201    format('\newcommand{\putrow}[6]
     . {\cnode[linecolor=#5](#1,#2){#6}{#3}
     . \rput{90}(#1,#2){\tiny #4}}')
         write(pstricks,211) 
  211    format('\newcommand{\putlevel}[3]
     . { \rput{90}(#1,#2){\textcolor{blue}{\small #3}}}')
          do irow=3,nimot+3
            if (idxarray(irow,1).lt.10) then 
               write(pstricks,212) (irow-2)*colsep,rowsep,
     .                    idxarray(irow,1)
            else
               write(pstricks,213) (irow-2)*colsep,rowsep,
     .                    idxarray(irow,1)
            endif 
 212   format('\putlevel{',f5.2,'}{',f5.2,'}{',i1,'}')
 213   format('\putlevel{',f5.2,'}{',f5.2,'}{',i2,'}')
            do ispalte=2,spalte
            if (idxarray(irow,ispalte).eq.-99) cycle 
            write(clabel,202) idxarray(irow,ispalte) 
  202    format('L',i3)   
            j=0
            do i=1,len_trim(clabel)
               if (clabel(i:i).ne.' ') then
                  j=j+1 
                  labelrow(idxarray(irow,ispalte))(j:j)=clabel(i:i)
               endif
            enddo 
            nlabelrow(idxarray(irow,ispalte))=j
           
  203    format('\putrow{',f5.2,'}{',f5.2,'}{',a,'}{',i1,'}{black}{',
     .       f5.2,'}')
  204    format('\putrow{',f5.2,'}{',f5.2,'}{',a,'}{',i2,'}{black}{',
     .       f5.2,'}')
          if (idxarray(irow,ispalte).lt.10) then 
             write(pstricks,203) (irow-2)*colsep,ispalte*rowsep,
     .           labelrow(idxarray(irow,ispalte))(1:j),
     .           idxarray(irow,ispalte),0.5*rowsep
          else 
             write(pstricks,204) (irow-2)*colsep,ispalte*rowsep,
     .           labelrow(idxarray(irow,ispalte))(1:j),
     .           idxarray(irow,ispalte),0.5*rowsep
          endif 
          enddo
          enddo  

c
c  write a and b labels
c  a: idxarray(4+nimot,spalte)=a
c  b: idxarray(2,spalte)=b
c
         write(pstricks,221) 
         write(pstricks,231) 
  221    format('\newcommand{\puta}[3]
     . { \rput{90}(#1,#2){\textcolor{red}{\small #3}}}')
  231    format('\newcommand{\putb}[3]
     . { \rput{90}(#1,#2){\textcolor{green}{\small #3}}}')

         do ispalte=2,spalte
          write(pstricks,222) 0.5*colsep,ispalte*rowsep,
     .         idxarray(2,ispalte)
  222     format('\putb{',f5.2,'}{',f5.2,'}{',i1,'}')
          if (idxarray(2,ispalte).eq.0) then
           write(pstricks,223) (nimot+1.5)*colsep,
     .            ispalte*rowsep,idxarray(nimot+4,ispalte)
  223     format('\puta{',f5.2,'}{',f5.2,'}{',i1,'}')
          endif
         enddo 

         write(pstricks,224) 0.0, spalte*rowsep*0.5,' b = 2S '
         write(pstricks,234) (nimot+2)*colsep,
     .         spalte*rowsep*0.5,' a = n$_{docc}$'
  234    format('\rput{90}(',f5.2,',',f5.2,'){\textcolor{red}{',a,'}}')
  224    format('\rput{90}(',f5.2,',',f5.2,')
     .      {\textcolor{green}{',a,'}}')
         
         write(pstricks,235) (nimot+2)*colsep*0.5, 0.0,'orbital level'
  235    format('\rput{180}(',f5.2,',',f5.2,')
     .          {\textcolor{blue}{',a,'}}')

c
c   arcs
c
         do irow = 1, nrow
           do istep = 0, 3
             do idrt = 1,3
             jrow = l(istep,irow,idrt)
             if ( jrow .ne. 0 ) then          ! draw the connection between the nmdes.
               write(pstricks,205) 
     .         labelrow(irow)(1:nlabelrow(irow)),
     .         labelrow(jrow)(1:nlabelrow(jrow)) 
  205    format('\ncline{',a,'}{',a,'}')
             endif
             enddo
            enddo
         enddo
        write(pstricks,*) '}}'
        write(pstricks,'(a)') '\end{pspicture}'
        write(pstricks,'(a)') '\newpage '
        write(pstricks,'(a,f6.2,a,f6.2,a)') 
     .            '\begin{pspicture}[showgrid=false](0,0)(',
     .            nimot+3*colsep,',', spalte*rowsep,')'               

        write(pstricks,*)'\rput{-90}(0,0){{'

         write(pstricks,211)
         write(pstricks,301)
c 301    format('\newcommand{\putcrow}[4]
c    . {\psdot[dotsize=#3,fillcolor=#4](#1,#2)}')
  301    format('\newcommand{\putcrow}[5]
     . {\Cnode[linecolor=#4,fillcolor=#4,fillstyle=solid,radius=#3]
     .   (#1,#2){#5}}')
          do irow=3,nimot+3
            if (idxarray(irow,1).lt.10) then
               write(pstricks,212) (irow-2.0)*colsep,rowsep,
     .                                idxarray(irow,1)
            else
               write(pstricks,213) (irow-2.0)*colsep,rowsep,
     .                                idxarray(irow,1)
            endif
            do ispalte=2,spalte
            if (idxarray(irow,ispalte).eq.-99) cycle
              j=(colarray(irow,ispalte))
              jj=idxarray(irow,ispalte)
  303    format('\putcrow{',f5.2,'}{',f5.2,'}{',f5.2,'}{',a,'}{',a,'}')
             if (j.ne.fill_neutral) then 
             write(pstricks,303) (irow-2)*colsep,ispalte*rowsep,
     .           radarray(irow,ispalte)*rowsep*0.5,
     .           ccolour(j)(1:nccolour(j)),
     .           labelrow(jj)(1:nlabelrow(jj))
             else
             write(pstricks,303) (irow-2)*colsep,ispalte*rowsep,
     .           rowsep*0.25,
     .           ccolour(j)(1:nccolour(j)),
     .           labelrow(jj)(1:nlabelrow(jj))
             endif
          enddo
          enddo
         write(pstricks,221)
         write(pstricks,231)

         do ispalte=2,spalte
          write(pstricks,222) 0.5*colsep,ispalte*rowsep,
     .         idxarray(2,ispalte)
          if (idxarray(2,ispalte).eq.0) then
           write(pstricks,223) (nimot+1.5)*colsep,
     .            ispalte*rowsep,idxarray(nimot+4,ispalte)
          endif
         enddo

         write(pstricks,224) 0.0, spalte*rowsep*0.5,' b = 2S '
         write(pstricks,234) (nimot+2)*colsep,
     .         spalte*rowsep*0.5,' a = n$_{docc}$'
         write(pstricks,235) (nimot+2)*colsep*0.5, 0.0,'orbital level'

c
c   arcs
c
         do irow = 1, nrow
           do istep = 0, 3
             do idrt = 1,3
             jrow = l(istep,irow,idrt)
             if ( jrow .ne. 0 ) then          ! draw the connection between the nmdes.
               write(pstricks,205)
     .         labelrow(irow)(1:nlabelrow(irow)),
     .         labelrow(jrow)(1:nlabelrow(jrow))
             endif
             enddo
            enddo
         enddo

         write(pstricks,*) '}}'
         write(pstricks,'(a)') '\end{pspicture}'





         ! determine the x and y scale factors that result in true circles of maximum radius.
         ! for tall graphs, the y/x scale factor is large, and the limiting circle radius is determined
         ! by the requirement that the circles do not overlap vertically.  for short graphs with large b values,
         ! the limiting circle radius is determined by the requirement that the circles do not overlap horizontally.
         ! label the nodes and draw all of the arcs in the shavitt graph.
         
         arc_small = thresh 
         do icase=1,3 
         if (icase.eq.1) write(pstricks,206) 
         if (icase.eq.2) write(pstricks,207) 
         if (icase.eq.3) write(pstricks,208) 
 206     format('\psset{linecolor=blue,linewidth=0.5pt}')
 207     format('\psset{linecolor=red,linewidth=0.5pt}')
 208     format('\psset{linecolor=black,linestyle=dotted,
     .         linewidth=0.5pt}')

         do irow = 1, nrow
c           write(gnuplot,'(a,i4,a,f8.4,",",f8.4,a)') 
c    .  'set label "', irow, '" at ', coord(irow), ' font "Courier,4"'
           do istep = 0, 3
             do idrt = 1,3 
             jrow = l(istep,irow,idrt)
             if ( jrow .ne. 0 ) then          ! draw the connection between the nodes.
                if (icase.eq.1) then 
                  if ( arc_density(istep,irow) .ge. arc_small ) then
                     color = line_positive
                  endif
                elseif (icase.eq.2) then 
                  if ( arc_density(istep,irow) .le. -arc_small ) then
                   color = line_negative
                  endif 
                else  ! the arc density is too small, select a neutral color.
                 if ( abs(arc_density(istep,irow)) .le. arc_small ) then
                   color = line_neutral
                  endif
                endif
c            write(pstricks,205) 
c    .           coord(irow)%y,coord(irow)%x,coord(jrow)%y,coord(jrow)%x 
  209    format('\ncline{',i2,',',i2,'}{',i2,','i2,'}') 
             endif
             enddo 
            enddo
         enddo
         enddo

          write(pstricks,'(a)') '\end{document}'
          deallocate(idxarray,colarray,radarray)


      return

      end 

      subroutine create_newdrt
     .    (nimot,nrow, a,b,level2,node_density, arc_density, 
     .     thresh,drtunit)

c      print the arc and node density to the output file.
c      and write a gnuplot file 

      implicit none
      integer    nrowmx,        nimomx,    nmotmx
      parameter( nrowmx=2**10-1, nimomx=128, nmotmx=510 )
c
c.....include 'cdrt.h'
      integer          l,                             syml,
     & modrt,          mapl,            level,           mapd
      common /cdrt/    l(0:3,nrowmx,3),  syml(nimomx),
     & modrt(nimomx),  mapl(2,nmotmx),  level(nmotmx),   mapd(2,nimomx)
c.....end of cdrt

      integer:: nimot,nrow,pstricks,level2(nrow),lprint
      real*8 :: node_density(nrow), arc_density(0:3,nrow)
      integer :: a(nrow),b(nrow),idrt,drtunit

      integer  :: irow, istep, a_head, b_head, b_max, jrow, color

      type xy_coord
         real :: x, y
      end type xy_coord
      type(xy_coord) :: coord(0:nrow)
c eventual coordinates for the graph nodes.

      real*8  thresh 
  25  format(a,4i5)
      write(drtunit,'(a,f12.6)') 'analyzing DRT with thresh=',thresh
      write(*,*) 'irow:    a    b   d(node)    '//
     .' istep   d(arc)  '//
     .' istep   d(arc)   istep   d(arc)   istep   d(arc) '
      do irow = 1, nrow
         write(*,'(i4," : ",2(i4,1x),es11.4)', advance='no')
     .           irow, a(irow), b(irow),  node_density(irow)
          if (abs(node_density(irow)).lt.thresh) then
             write(drtunit,'(a,4i5)') 'VER',level2(irow),a(irow),b(irow)
          endif 
         do istep = 0, 3
            if ( l(istep,irow,1) .ne. 0 .or.
     .           l(istep,irow,2) .ne. 0 .or.
     .           l(istep,irow,3) .ne. 0 ) then
          write(*,'(1x,i4,f12.4)',advance='no')
     .       istep, arc_density(istep,irow)

           if (abs(arc_density(istep,irow)).lt.thresh) then
             write(drtunit,25) 'ARC',level2(irow),a(irow),b(irow),istep
c            write(*,'(a,4i5)') 'deleting ARC',irow,level2(irow),a(irow),b(irow),istep
           endif


            else
            write(*,'(1x,4x,12x)',advance='no')
            endif
         enddo
         write(*,'(1x)')
      enddo
       return
       end

