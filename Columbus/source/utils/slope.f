!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
c      -------------------------------------
c                      SLOPE
c      -------------------------------------
c
c      This program orthogonalizes nonadiabatic coupling 
c      vectors and computes the parameters for the
c      linear approximation of the adiabatic energies
c      close to a conical intersection.
c
c      Author: Mario Barbatti, 2005.
c
c       ---------------
c       Input
c       ---------------
c
c       * slope.inp: namelist slope.
c
c   Parameter      Default     Description
c   ---------      -------     -----------
c       crit     = [1]         0            - Orthogonalize g and h. (a.u.)
c                              1            - Do not orthogonalize g and h
c                              0 < crit < 1 - Orthogonalize g and h if g*h > crit 
c       ort_type = [Y]         S  - Schimidt orhogonalization. g is kept fixed and h is rotated.
c                              Y  - Yarkony orthogonalization [Yarkony, 2000].
c       geomf    = [A]         Input file with the geometry of the conical intersection (Columbus format)
c       grad1f   = [B]         Gradient file for the first state  (free format)
c       grad2f   = [C]         Gradient file for the second state (free format)
c       coupf    = [D]         Nonadiabatic coupling vector file  (free format)
c       de       = [1]         Energy gap (a.u.) between the states (free format)
c
c       A:  GEOMS/geom.min
c       B:  GRADIENTS/cartgrd.drt1.state1.sp
c       C:  GRADIENTS/cartgrd.drt1.state2.sp
c       D:  GRADIENTS/artgrd.nad.drt1.state1.drt1.state2.sp
c
c      Example of file slope.inp:
c      (Orthogonalize 6-atoms molecule if g*h > 0.001 au. Use Yarkony method.)
c
c &slope
c   crit     = 0.001,
c   ort_type = Y,
c /end slope
c
c       * Geometry file
c
c       * First-state gradient file
c
c       * Second-state gradient file
c
c       * Nonadiabatic coupling vectors file
c
c
c       -------------------------------------
c       Implementation in the COLUMBUS system
c       -------------------------------------
c
c       For each iteraction of MXS search, slope is called in runc
c       by subroutine "orthogonalize".
c       slope.inp file is automaticaly created with the following parameters:
c       crit = 0
c       ort_type = Y 
c
c       ---------------
c       Output
c       ---------------
c
c       * slopels: results of the analysis.
c
c       * moldenort.freq: molden format file.
c                                                    JCP 120(2004) 7330  
c         Vibration 1: g1 first-state gradient        = h(II)
c         Vibration 2: g2 second-state gradient       = h(JJ)
c         Vibration 3: s = g1 + g2 (original)         = s(IJ)
c         Vibration 4: g = g1 - g2 (original)         = g(IJ)
c         Vibration 5: h (original)                   = h(IJ)
c         Vibration 6: g (orthogonal)  
c         Vibration 7: h (orthogonal)
c
c       -------------------
c       Current limitations
c       -------------------
c
c       Maximum number of atoms = 100.
c
c       The othogonalization procedure is valid only to states with the 
c       same multiplicity.
c
c       ---------------
c       References
c       ---------------
c
c     Linear approximation: D. R. Yarkony, J. Chem. Phys. 114, 2601 (2001).
c     Orhogonalization:     D. R. Yarkony, J. Chem. Phys. 112, 2111 (2000).
c     Implementation:       M. Barbatti, A. J. A. Aquino and H. Lischka, 
c                           J. Phys. Chem. A 109, 5168 (2005).
c       
c
c   
      implicit real*8 (a-h,o-z)

      character*2   SYMB,S(100),ort_type
      character*120 geomf,grad1f,grad2f,coupf

      dimension g1(100,3),g2(100,3),h(100,3),dg(100,3),sg(100,3),AZ(100)
      dimension gunit(100,3),hunit(100,3)
      dimension hort(100,3) ,hortunit(100,3)
      dimension gort(100,3) ,gortunit(100,3)
      dimension X(100,3)

       common NAT

c      Namelist ..................................................
       namelist /slope/ crit, ort_type, geomf, grad1f, grad2f,coupf,de
       crit     = 0.001
       ort_type = 'Y'
       geomf    = 'GEOMS/geom'
       grad1f   = 'GRADIENTS/cartgrd.drt1.state1.sp'
       grad2f   = 'GRADIENTS/cartgrd.drt1.state2.sp'
       coupf    = 'GRADIENTS/cartgrd.nad.drt1.state1.drt1.state2.sp'
       de       = 1.D0

       open(UNIT=10,file='slope.inp',     status='unknown')
       read(10,NML=slope) 
       if (dabs(de) .lt. 1D-7) then
         de = -1.D-5
       endif

c      Write initial output
       open(UNIT=12,file='slopels',status='unknown')
       write(12,*) '      -------------------------------------'
       write(12,*) '                    SLOPE                  '
       write(12,*) '        Orthogonalization and analysis of  '
       write(12,*) '          nonadiabatic coupling vectors    '
       write(12,*) '      -------------------------------------'
       write(12,*) '      Author: M. Barbatti, 2005.          '
       write(12,*) ' '

       write(12,*) 'References:'
       write(12,*) '-----------'
       write(12,*) 'Linear approximation: D.R. Yarkony, J. Chem. Phys. 
     #114, 2601 (2001).'
       write(12,*) 'Orhogonalization:     D.R. Yarkony, J. Chem. Phys. 
     #112, 2111 (2000).'
       write(12,*) 'Implementation:       M. Barbatti, A.J.A. Aquino and
     # H. Lischka, J. Phys. Chem. A 109, 5168 (2005).'
       write(12,*) ' '

       write(12,*) 'Input parameters:'
       write(12,*) '-----------------'
       write(12,'(A33,F7.4)') ' Criterium for orthog.     CRIT =',crit
       write(12,'(A34,A1)')   ' Orthog. Method        ORT_TYPE = ',
     #ort_type
       write(12,'(A34,A40)')  ' Gradient state 1        GRAD1F = ',
     #grad1f
       write(12,'(A34,A40)')  ' Gradient state 2        GRAD2F = ',
     #grad2f
       write(12,'(A34,A40)')  ' Nonadiab. coupling       COUPF = ',coupf
       write(12,*) ' '
c      ..........................................................

       open(UNIT=13,file=geomf,           status='unknown')
       open(UNIT=14,file=grad2f,          status='unknown')
       open(UNIT=15,file=grad1f,          status='unknown')
       open(UNIT=16,file=coupf,           status='unknown')

       do k1=1,100
       do k2=1,3
       g1(k1,k2)=0
       g2(k1,k2)=0
       h(k1,k2)=0
       enddo
       enddo

c      Read cartesian coordinates (a.u.)

       nr = 0
       do while (ist. eq. 0)
       nr = nr + 1
       read(13,*,IOSTAT=ist) S(nr),AZ(nr),(X(nr,j),j=1,4) 
       enddo
       NAT = nr - 1

c      Read gradient 1 (a.u.)

       write(12,*) 'Gradient 1 (a.u.)'
       do i1=1,NAT
       read(14,*)  (g1(i1,j1), j1=1,3)
       write(12,'(3F10.5)') (g1(i1,j1), j1=1,3)
       enddo
       write(12,*) ' '

c      Read gradient 2 (a.u.)

       write(12,*) 'Gradient 2 (a.u.)'
       do i2=1,NAT
       read(15,*) (g2(i2,j2), j2=1,3)
       write(12,'(3F10.5)') (g2(i2,j2), j2=1,3)
       enddo
       write(12,*) ' '

c      Read nonadiabatic coupling vector (a.u.)

       write(12,*) 'Nonadiabatic coupling vector (a.u.)'
       do ih=1,NAT
       read(16,*) (h(ih,jh), jh=1,3)
       write(12,'(3F10.5)') (h(ih,jh), jh=1,3)
       enddo
       write(12,*) ' '

c      Sum and difference gradient vectors

       do i=1,NAT
       do j=1,3
       sg(i,j)=(g2(i,j)+g1(i,j))/2.     
       dg(i,j)=(g2(i,j)-g1(i,j))/2.
       enddo
       enddo

       write(12,*) 'Sum gradient'
       do i=1,NAT
       write(12,'(3F10.5)') (sg(i,j), j=1,3)
       enddo
       write(12,*) ' '

       write(12,*) 'Difference gradient'
       do i=1,NAT
       write(12,'(3F10.5)') (dg(i,j), j=1,3)
       enddo
       write(12,*) ' '

c      Scalar products

       call scalar(dg,dg,q)
       sdg=dsqrt(q)
       call scalar(h,h,q)
       sh=dsqrt(q)
       call scalar(sg,sg,q)
       ssg=dsqrt(q)

c      Orthogonalization

       do k1=1,NAT
       do k2=1,3
       gunit(k1,k2)=dg(k1,k2)/sdg
       hunit(k1,k2)=h(k1,k2)/sh
       enddo
       enddo

       call scalar(hunit,gunit,q)
       shgunit=q
       call scalar(gunit,gunit,q)
       sgunit=dsqrt(q)
       call scalar(hunit,hunit,q)
       shunit=dsqrt(q)

c      test: g*h=0?
       call scalar(dg,h,q)
       prod0=q

       icrit=int(crit)

       if ((dabs(prod0) .le. crit) .or. (icrit .eq. 1) ) then ! if g*h <= crit or crit = 1, do nothing

       do k1=1,NAT
       do k2=1,3
       gort(k1,k2)=dg(k1,k2)
       hort(k1,k2)= h(k1,k2)
       enddo
       enddo
       ind_ort=0       

       endif

       if ((dabs(prod0) .gt. crit) .or. (icrit .eq. 0) ) then ! if g*h > crit or crit = 1, orthogonalize

       if ((ort_type .eq. 'S') .or. (ort_type .eq. 's')) then ! if ort_type = S, Schimidt orthog.

       call schmidt(dg,h,sh,shgunit,gunit,gort,hort)
       ind_ort=1       

       endif

       if ((ort_type .eq. 'Y') .or. (ort_type .eq. 'y')) then ! if ort_type = Y, Yarkony orthog.

       call yarkony(dg,h,gort,hort)
       ind_ort=2

       endif

       endif

c      Write orthogonalized vectors

       write(12,*) "Othogonalized g vector"
       do i=1,NAT
       write(12,'(3F10.5)') (gort(i,j),j=1,3)
       enddo
       write(12,*) " "

       write(12,*) "Othogonalized h vector"
       do i=1,NAT
       write(12,'(3F10.5)') (hort(i,j),j=1,3)
       enddo
       write(12,*) " "


c      test: gort*hort=0?
       call scalar(gort,gort,q)
       sgort=dsqrt(q)
       call scalar(hort,hort,q)
       short=dsqrt(q)
       call scalar(gort,hort,q)
       prod=q

       do k1=1,NAT
       do k2=1,3
       gortunit(k1,k2)=gort(k1,k2)/sgort
       hortunit(k1,k2)=hort(k1,k2)/short
       enddo
       enddo

c      Yarkony parameters (Yarkony, JCP 114, 2601 (2001).)
c      1. sx and sy:
       call scalar(sg,gortunit,q)
       sx=q
       call scalar(sg,hortunit,q)
       sy=q

c      2. dgh
       dgh=dsqrt(sgort**2+short**2)

c      3. Delta_gh
       delta=(sgort**2-short**2)/dgh**2

c      Write results
       call writepar(ind_ort,prod0,crit,prod,icrit,sdg,sh,ssg,
     #sgort,short,dgh,delta,sx,sy,dg,h,gort,hort,S,X,AZ,g1,g2,sg,de)

       call bummer('normal termination',0,3)

      end

      subroutine writepar(ind_ort,prod0,crit,prod,icrit,sdg,sh,ssg,
     #sgort,short,dgh,delta,sx,sy,dg,h,gort,hort,S,X,AZ,g1,g2,sg,de)
c     write
      implicit real*8 (a-h,o-z)
      character*2   SYMB,S(100),ort_type
      character*120 geomf,grad1f,grad2f,coupf
      dimension g1(100,3),g2(100,3),h(100,3),dg(100,3),sg(100,3),AZ(100)
      dimension gunit(100,3),hunit(100,3)
      dimension hort(100,3) ,hortunit(100,3)
      dimension gort(100,3) ,gortunit(100,3)
      dimension X(100,3)
      common NAT

       write(12,*) 'Norm and orthog. information:'
       write(12,*) '-----------------------------'

       write(12,*)  'g=(g2-g1)/2; s=(g2+g1)/2'
       write(12,89) ' g*h    = ',prod0, '. Original value.'

       if (ind_ort .eq. 0) then
       if (icrit .eq. 1) then
       write(12,'(A35)') ' g and h will not be orthogonalized.'
       else
       write(12,91) '|g*h|   < ', crit, '. g and h will not be orthog.'
       endif
       endif

       if (ind_ort .eq. 1) then
       write(12,90) ' g*hort = ' , prod,'. New h obtained via Schimidt o
     #rthogonalization'
       endif

       if (ind_ort .eq. 2) then
       write(12,92) ' g*hort = '  ,prod,'. New g and h obtained via Yark
     #ony orthogonaliz.'
       endif

       write(12,*) ' '
       write(12,*) 'Frobenius norm of g, h and s vectors (hartree/a0): '
       write(12,'(A10,F8.4,A18,F8.4)') ' |g|    = ',sdg, 
     #'       |gort|   = ',sgort
       write(12,'(A10,F8.4,A18,F8.4)') ' |h|    = ',sh, 
     #'       |hort|   = ',short
       write(12,'(A10,F8.4)')  ' |s|    = ',ssg

   88  format(A10,   I8,A29)
   89  format(A10, F8.4,A17)
   90  format(A10, F8.4,A47)
   91  format(A10, F8.4,A29)
   92  format(A10, F8.4,A51)

       write(12,*) ' '
       write(12,*) 'Parameters analysis:'
       write(12,*) '--------------------'
       write(12,*) 'The linear adiabatic energies in the unscaled-orthog
     #onal x-y space are:'
       write(12,*) 'Ea = sx*x+sy*y -/+dgh*[(x**2+y**2)/2+Delta_gh*(x**2-
     #y**2)/2]**(1/2)'
       write(12,*) ' '
       write(12,*) 'Atomic units: '
       write(12,120) ' Inclination: d_gh (hartree/a0) =   ',dgh
       write(12,120) ' Cylindrical: Delta_gh          =   ',delta
       write(12,120) ' Tilt g:      s_x (hartree/a0)  =   ',sx
       write(12,120) ' Tilt h:      s_y (hartree/a0)  =   ',sy
       write(12,*) ' '
       write(12,*) 'eV,Angstrom units: '
c      1 hartree/a0 = 51.42208240 eV/Angstrom
       conv=51.42208240
       write(12,121) ' Inclination: d_gh (eV/A) =         ',dgh*conv
       write(12,121) ' Cylindrical: Delta_gh    =         ',delta
       write(12,121) ' Tilt g:      s_x (eV/A)  =         ',sx*conv
       write(12,121) ' Tilt h:      s_y (eV/A)  =         ',sy*conv

       write(12,*) ' '
       write(12,*) 'sigma_x=s_x/d_gh; sigma_y=s_y/d_gh'
       write(12,122) ' Tilt g:      sigma_x     =         ',sx/dgh
       write(12,122) ' Tilt h:      sigma_y     =         ',sy/dgh

c      MAPLE (Atomic units)
       write(12,*) ' '
       write(12,*) 'MAPLE output'
       write(12,*) '------------'
       write(12,*) 'Atomic units '
       write(12,'(A6,F8.5,A1)') ' dgh:=',  dgh,  ';' 
       write(12,'(A8,F8.5,A1)') ' Delta:=',delta,';' 
       write(12,'(A5,F8.5,A1)') ' sx:=',   sx,   ';' 
       write(12,'(A5,F8.5,A1)') ' sy:=',   sy,   ';'
       write(12,*) 'Esup:=sx*x+sy*y+dgh*((x^2+y^2)/2+Delta*(x^2-y^2)/2)^
     #(1/2);' 
       write(12,*) 'Einf:=sx*x+sy*y-dgh*((x^2+y^2)/2+Delta*(x^2-y^2)/2)^
     #(1/2);'

c      MAPLE (eV, Angstrom units)
       write(12,*) ' '
       write(12,*) 'eV, Angstrom units '
       write(12,'(A6,F6.2,A1)') ' dgh:=',  dgh*conv,  ';' 
       write(12,'(A8,F6.2,A1)') ' Delta:=',delta,';' 
       write(12,'(A5,F6.2,A1)') ' sx:=',   sx*conv,   ';' 
       write(12,'(A5,F6.2,A1)') ' sy:=',   sy*conv,   ';'
       write(12,*) 'Esup:=sx*x+sy*y+dgh*((x^2+y^2)/2+Delta*(x^2-y^2)/2)^
     #(1/2);' 
       write(12,*) 'Einf:=sx*x+sy*y-dgh*((x^2+y^2)/2+Delta*(x^2-y^2)/2)^
     #(1/2);'

c      MOLDEN
       write(12,*) ' '
       write(12,*) 'MOLDEN output'
       write(12,*) '-------------'
       write(12,*) 'Written to molden.nad (arbitrary value of freq.)
     #:'
       write(12,*) 'Vibration 1: g1'
       write(12,*) 'Vibration 2: g2'
       write(12,*) 'Vibration 3: s '
       write(12,*) 'Vibration 4: g (original)'
       write(12,*) 'Vibration 5: h (original)'
       write(12,*) 'Vibration 6: g (orthogonal)'
       write(12,*) 'Vibration 7: h (orthogonal)'
       call writemolden(S,AZ,X,g1,g2,sg,dg,h,gort,hort,de)

  120  format(A36,F8.5)
  121  format(A36,F8.2)
  122  format(A36,F8.3)

      endsubroutine

      subroutine schmidt(dg,h,sh,shgunit,gunit,gort,hort)
c     Schmidt orthogonalization
      implicit real*8 (a-h,o-z)
      dimension v1(100,3),v2(100,3)
      dimension g1(100,3),g2(100,3),h(100,3),dg(100,3),sg(100,3)
      dimension hort(100,3),gunit(100,3),hunit(100,3),hortunit(100,3)
      dimension gort(100,3)
      common NAT

       do k1=1,NAT
       do k2=1,3
       gort(k1,k2)=dg(k1,k2)
       hort(k1,k2)=(h(k1,k2)-sh*shgunit*gunit(k1,k2))/
     #dsqrt(1.0-shgunit**2)
       enddo
       enddo

      endsubroutine

      subroutine yarkony(g,h,gort,hort)
c     Yarkony orthogonalization.
c     This equations are sligth different in JCP 112, 2111 (2000) and
c     at Conical Intersections, Ed. by Domcke et al., 2004. Here, one use the
c     most recent one.
      implicit real*8 (a-h,o-z)
      dimension h(100,3),g(100,3)
      dimension hort(100,3),gort(100,3)
      common NAT

       call scalar( g, g, q)
       g2 =q
       call scalar( h, h, q)
       h2 =q
       call scalar( g, h, q)
       gh =q

       ratio      = 2.D0*gh/(h2-g2)
       beta       = 0.5D0*datan(ratio)

       do k1=1,NAT
       do k2=1,3
c
c      Han and Yarkony, in Quantum Dynamics at Conical Intersections, 2004
c       gort(k1,k2)=-g(k1,k2)*dcos(beta)+ h(k1,k2)*dsin(beta)
c       hort(k1,k2)= g(k1,k2)*dsin(beta)+ h(k1,k2)*dcos(beta)
c
c      Yarkony, JCP 112, 211 (2000)
        gort(k1,k2)= g(k1,k2)*dcos(beta)- h(k1,k2)*dsin(beta)
        hort(k1,k2)= g(k1,k2)*dsin(beta)+ h(k1,k2)*dcos(beta)
       enddo
       enddo

c      write(12,*) '------------------------'
c      write(12,*) ' beta (rad) = ', beta
c      write(12,*) ' tan(2*beta)= ', dtan(2.D0*beta)
c      write(12,*) ' cos(beta)  = ', dcos(beta)
c      write(12,*) '------------------------'
c      write(12,*) ' '

      endsubroutine

      subroutine scalar(v1,v2,q)
c     Scalar product
      implicit real*8 (a-h,o-z)
      dimension v1(100,3),v2(100,3)
      common NAT

       sum1=0.0D0

       do k1=1,NAT
       do k2=1,3
       sum1=sum1+v1(k1,k2)*v2(k1,k2)
       enddo
       enddo

       q = sum1

      endsubroutine

      subroutine writemolden(S,AZ,X,g1,g2,sg,dg,h,gort,hort,de)
c     write molden file
      implicit real*8 (a-h,o-z)
      character*2 S(100)
      dimension v1(100,3),v2(100,3)
      dimension g1(100,3),g2(100,3),h(100,3),dg(100,3),sg(100,3),AZ(100)
      dimension hort(100,3),gunit(100,3),hunit(100,3),hortunit(100,3)
      dimension X(100,3)
      dimension gort(100,3)
      common NAT
       open(UNIT=17,file='moldenort.freq',status='unknown')
       open(UNIT=18,file='nadvecort',status='unknown')
       open(UNIT=19,file='grdvecort',status='unknown')
       
c [Molden Format]
c [FREQ]
c  1000.0
c  1000.0
c [FR-COORD]
c  C  -1.34941  -0.05385  -0.01218 6.00
c  ...
c [FR-NORM-COORD]
c vibration   1
c  -0.03218  -0.00197  -0.00013
c  ...

       write(17,151) ' [Molden Format]'
       write(17,152) ' [FREQ]'
       do i=1,7
       write(17,153) 1000.0*i
       enddo 
       write(17,154) ' [FR-COORD]'
       do i=1,NAT
       write(17,155) S(i),(X(i,j),j=1,3),AZ(i)
       enddo
       write(17,156) ' [FR-NORM-COORD]'
       write(17,157) ' vibration   1'
       do i=1,NAT
       write(17,158) (g1(i,j)*10.0,  j=1,3)
       enddo
       write(17,157) ' vibration   2'
       do i=1,NAT
       write(17,158) (g2(i,j)*10.0,  j=1,3)
       enddo
       write(17,157) ' vibration   3'
       do i=1,NAT
       write(17,158) (sg(i,j)*10.0,  j=1,3)
       enddo
       write(17,157) ' vibration   4'
       do i=1,NAT
       write(17,158) (dg(i,j)*10.0,  j=1,3)
       enddo
       write(17,157) ' vibration   5'
       do i=1,NAT
       write(17,158) ( h(i,j)/de,  j=1,3)
       enddo
       write(17,157) ' vibration   6'
       do i=1,NAT
       write(17,158) (gort(i,j)*10.0,j=1,3)
       enddo
       write(17,157) ' vibration   7'
       do i=1,NAT
       write(17,158) (hort(i,j)/de,j=1,3)
       enddo
       do i=1,NAT
       write(18,159) (hort(i,j),j=1,3)
       enddo
       do i=1,NAT
       write(19,159) (gort(i,j),j=1,3)
       enddo
       write(12,*) "Wrinting Molden with DE = ", de
 
  151  format(A16)
  152  format(A7)
  153  format(F8.1)
  154  format(A11)
  155  format(A3,3F10.5,F5.2)
  156  format(A16)
  157  format(A14)
  158  format(3F14.5)
  159  format(3E15.6)

      endsubroutine
