!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/

      program phaseci

      IMPLICIT NONE
      include "../colib/getlcl.h"

c
c   This program allows to copy all valid ci vectors (as defined in the
c   ci info file civout) from a given ci (or sigma) vector file.
c   Namelist type input
c
c
c    input file:   cutciin
c    listing file: cutcils
c
c   vectorin : name of the input vector file
c   vectorout: name of the output vector file
c   infoin   : name of the input vector info file
c   infoout  : name of the output vector info file
c   lvlprt   : printlevel (>=0)
c   delsrc   : if set to 1 the input vector and input vector info
c              files are deleted after the copying operation
c
c
c*********************************************************************
c
c  allocate space and call the driver routine.
c
c  ifirst = first usable space in the the real*8 work array a(*).
c  lcore  = length of workspace array in working precision.
c  mem1   = additional machine-dependent memory allocation variable.
c  a(*)   = workspace array. a(ifirst : ifirst+lcore-1) is useable.
c
      integer ifirst, lcore
c
c     # mem1 and a(*) should be declared below within mdc blocks.
c
c     # local...
c     # lcored = default value for lcore.
c     # ierr   = error return code.
c
      integer lcored, ierr
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
c     # sets up any communications necessary for parallel execution,
c     # and does nothing in the sequential case.

c
c
c
c     # use standard f90 memory allocation.
c
      integer mem1
      parameter ( lcored = 2 500 000 )
      real*8, allocatable :: a(:)
c
      call getlcl( lcored, lcore, ierr )
      if ( ierr .ne. 0 ) then
         call bummer('argos: from getlcl: ierr=',ierr,faterr)
      endif
c
      allocate( a(lcore) )
      ifirst = 1
      mem1   = 0
c     # lcore, mem1, and ifirst should now be defined.
c     # the values of lcore, mem1, and ifirst are passed to
c     # driver() to be printed.
c     # since these arguments are passed by expression, they
c     # must not be modified within driver().
c
c
      call driver( a(ifirst), (lcore), (mem1), (ifirst) )
c
c
c     # return from driver() implies successful execution.
c
c     # message-passing cleanup: stub if not in parallel
c
      call bummer('normal termination',0,3)
      stop 'end of phaseci'
      end
c
c
c***************************************************************
c
      subroutine driver( core, lcore, mem1, ifirst)
c
      implicit none 
c
c  ##  parameter & common block section
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      real*8     zero,        half
      parameter (zero=0.0d+00,half=0.5d+00)
c
      integer maxvec
      parameter (maxvec=16)
c
      integer lenrec
      parameter (lenrec=8*4096)
c
      integer   nfilmx
      parameter(nfilmx=55)
      integer         nunits
      common /cfiles/ nunits(nfilmx)
      character*60    fname
      common /cfname/ fname(nfilmx)
      integer phout,nlist,civfl,civfl_rest
      equivalence (nunits(1),nlist)
      equivalence (nunits(3),phout)
      equivalence(nunits(7),civfl)
      equivalence(nunits(8),civfl_rest)
c
      INTEGER             NREC35,      NREC36, ssym1,ssym2
      COMMON / INF4   /   NREC35,      NREC36, ssym1,ssym2
c
      integer          nroot1,nroot2,lvlprt,drt1,drt2
      common /indata/  nroot1,nroot2,lvlprt,drt1,drt2
c
      integer cicnt(2),cicsf(200,2)
      common  /phase/cicnt,cicsf
c
c  ##  integer section
c
      integer bperacc
      integer cpt(10),cipossition
      integer fildrt1,fndrt1
      integer i,inroot1,inroot2,iroot,isweep,ifirst,intorb
      integer lcore
      integer mem1
      integer ncsf(2),nel1st,nel,nroot,nmot,nvalz(2),nvalwk,nfct,
     &        nmsym,nvalx,nvaly,nintpt,nvalw,neli
      integer outroot
      integer pthz,pthy,pthx,pthw,phasefactor
      integer recpervec,roots(maxvec)
      integer vecspace
c
c  ##  real*8 sectionc
c
      real*8 core(*)
      real*8 overlap1,overlap2
c
c  ##  external section
c
      integer  atebyt
      external atebyt
c
c------------------------------------------------------------------
c
c     open the listing files.
c
      open( unit = nlist, file = fname(1), status = 'unknown')
      open( unit = phout, file= fname(3), status = 'unknown')

      call who2c( 'phaseci', nlist )
      call datain
c     # open the necessary info containing files
c
c
      call ibummr( nlist )
c
*@ifdef direct
*      call headwr(nlist,'phaseci','1.0     ')
*@else
*@endif
c
      write(nlist,100)
      write(nlist,101)
      write(nlist,102)
      write(nlist,103)
      write(nlist,104)
      write(nlist,105)
      write(nlist,200)
100   format
     & (/' phaseci.x determines the relative position and phase')
101   format
     & (' of 2 CI vectors in civfl_restart.drtX and civfl.drtX files.')
102   format
     & (' This information is used to:')
103   format
     & ('  1. Assign the correct relative sign to the non-adiabatic ',
     & 'coupling term elements')
104   format
     & ('  2. Identify the correct CI states in case of changes of ',
     & 'states in'/4x,' conical intersection searches.')
105   format
     & (' Author: Michal Dallos, University of Vienna, Austria')
200   format(1x,72('-'))
c
      write(nlist,*)
      write(nlist,*)'  Reading file: ',fname(11)
      call tapind (
     &  ssym1,      nmot,      nmsym,    core,
     &  nunits(11), fname(11), pthz,     pthy,
     &  pthx,       pthw,      nintpt,   intorb,
     &  nvalwk,     nvalz(1),  nvaly,    nvalw,
     &  nvalx,      nfct,      neli,     ncsf(1),
     &  lcore)
c
      write(nlist,*)
      write(nlist,*)'  Reading file: ',fname(12)
      call tapind (
     & ssym2,      nmot,      nmsym,    core,
     & nunits(12), fname(12), pthz,     pthy,
     & pthx,       pthw,      nintpt,   intorb,
     & nvalwk,     nvalz(2),  nvaly,    nvalw,
     & nvalx,      nfct,      neli,     ncsf(2),
     & lcore)
c
c  #  1:cir_drt1_st1,2:cir_drt2_st2,3:ci_drt1_st1,4:ci2st2,5:buffer
      cpt(1)=1
      cpt(2)=cpt(1)+atebyt(nvalz(1))
      cpt(3)=cpt(2)+atebyt(nvalz(2))
      cpt(4)=cpt(3)+atebyt(nvalz(1))
      cpt(5)=cpt(4)+atebyt(nvalz(2))
      cpt(6)=cpt(5)+atebyt(lenrec)
      call wzero(2*(nvalz(1)+nvalz(2)),core(cpt(1)),1)
c
c  #  get CI coefficients for the restart CI vector
      write(nlist,'(/''  Getting phase for restart CI vector'')')
c  # state1 (restart CI)
c     call openda(nunits(9),fname(9),lenrec,'keep','random')
      write(nlist,*)
      write(nlist,*)'  Reading file: ',fname(9)
      call getciphase(
     & nunits(9),  core(cpt(5)),   lenrec,   nroot1,
     & 1,          core(cpt(1)),   nvalz(1), ncsf(1),
     .  fname(9))
c     call closda(nunits(9),'keep',4)
c  # state2 (restart CI)
c     call openda(nunits(10),fname(10),lenrec,'keep','random')
      write(nlist,*)
      write(nlist,*)'  Reading file: ',fname(10)
      call getciphase(
     & nunits(10),  core(cpt(5)),   lenrec,    nroot2,
     & 2,           core(cpt(2)),   nvalz(2),  ncsf(2),
     . fname(10) )
c     call closda(nunits(10),'keep',4)
c
c  #  get CI coefficients for the current CI vector
      write(nlist,'(/''  Getting phase for current CI vector'')')
c  # state1 (current CI)
c     call openda(nunits(7),fname(7),lenrec,'keep','random')
      write(nlist,*)
      write(nlist,*)'  Reading file: ',fname(7)
      call getciphase(
     & nunits(7),   core(cpt(5)),  lenrec,    nroot1,
     & 1,           core(cpt(3)),  nvalz(1),  ncsf(1),
     . fname(7))
c     call closda(nunits(7),'keep',4)
c  # state2 (current CI)
c     call openda(nunits(8),fname(8),lenrec,'keep','random')
      write(nlist,*)
      write(nlist,*)'  Reading file: ',fname(8)
      call getciphase(
     & nunits(8),   core(cpt(5)),  lenrec,    nroot2,
     & 2,           core(cpt(4)),  nvalz(2),  ncsf(2),
     . fname(8))
c     call closda(nunits(8),'keep',4)
c
c   # Check if the CI vectors changed placeses
c  #  1:cir_drt1_st1,2:cir_drt2_st2,3:ci_drt1_st1,4:ci2st2,5:scr
      cpt(6)=cpt(5)+atebyt(nvalz)
c
      call vec_possition(
     & core(cpt(1)),  core(cpt(2)),  core(cpt(3)),  core(cpt(4)),
     & core(cpt(5)),  nvalz,         cipossition)
       write(phout,'(i2,2x,''/CI vector order'')')cipossition
c
      call calcphase(
     & core(cpt(1)),  core(cpt(2)),  core(cpt(3)),  core(cpt(4)),
     & core(cpt(5)),  nvalz,         cipossition,   phasefactor)
      write(phout,'(i2,2x,''/CI phase factor'')')phasefactor
c
      close (nlist)
      close (phout)
c
      return
      end
c
c*********************************************************************++
c
      subroutine vec_possition(
     & cir_drt1_st1,   cir_drt2_st2,   ci_drt1_st1,   ci_drt2_st2,
     & scr,            nvalz,          cipossition)
c
       implicit none
c  ##  parameter and common block section
c
      real*8     zero,        half
      parameter (zero=0.0d+00,half=0.5d+00)
c
      integer         nroot1,nroot2,lvlprt,drt1,drt2
      common /indata/ nroot1,nroot2,lvlprt,drt1,drt2
c
      integer   nfilmx
      parameter(nfilmx=55)
      integer         nunits
      common /cfiles/ nunits(nfilmx)
      integer nlist
      equivalence (nunits(1),nlist)
c
c  ##  integer section
c
      integer cipossition
      integer i
      integer nvalz
c
c  ##  real*8 section
c
      real*8 cir_drt1_st1(nvalz),cir_drt2_st2(nvalz),
     &       ci_drt1_st1(nvalz),ci_drt2_st2(nvalz)
      real*8 overlap1,overlap2
      real*8 scr(nvalz)
c
c-----------------------------------------------------------------------
c
      overlap1=zero
      overlap2=zero
      do i=1,nvalz
      overlap1=overlap1+(cir_drt1_st1(i)*ci_drt1_st1(i))
      overlap2=overlap2+(cir_drt2_st2(i)*ci_drt2_st2(i))
      enddo ! end of: do i=1,nvalz
      overlap1=dabs(overlap1)
      overlap2=dabs(overlap2)
c
      write(nlist,
     & '(/''   Checking for changes in CI vector position:''/)')
      write(nlist,'(/''   current CI   restart CI   Overlap:'')')
      write(nlist,'('' ---------------------------------------'')')
      write(nlist,'(8x,i2,9x,i2,5x,f10.5)')nroot1,nroot1,overlap1
      write(nlist,'(8x,i2,9x,i2,5x,f10.5)')nroot2,nroot2,overlap2
      write(nlist,
     &'(/,2x,''2 CI roots have:'')')
      if (overlap1.gt.half) then
       write(nlist,
     &  '(2x,''the same position in restart and current CI vector'')')
       cipossition = 1
      else
       write(nlist,
     &  '(2x,''changed position in restart and current CI vector'')')
       call dcopy(nvalz,ci_drt1_st1,1,scr,1)
       call dcopy(nvalz,ci_drt2_st2,1,ci_drt1_st1,1)
       call dcopy(nvalz,scr,1,ci_drt2_st2,1)
       cipossition = -1
      endif
c
      return
      end
c
c*********************************************************************++
c
      subroutine calcphase(
     & cir_drt1_st1,  cir_drt2_st2,  ci_drt1_st1,  ci_drt2_st2,
     & scr,           nvalz,         cipossition,  phasefactor)
c
       implicit none
c
c  ##  parameter & common block section
c
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      real*8     zero,        one,        mone
      parameter (zero=0.0d+00,one=1.0d+00,mone=-1.0d+00)
c
      integer ii,maxref,nref,iref
      parameter (maxref=8*4096)
      common /reflist/nref(2),iref(maxref,2)
c
      integer   nfilmx
      parameter(nfilmx=55)
      integer         nunits
      common /cfiles/ nunits(nfilmx)
      integer nlist
      equivalence (nunits(1),nlist)
c
c
      integer cicnt(2),cicsf(200,2)
      common  /phase/cicnt,cicsf
c
c  ##  integer section
c
      integer i
      integer j
      integer k
      integer k1,k2,icf,n1,n2
      integer nvalz
      integer cipossition
      integer phasefactor
c
c  ##  real*8 section
c
      real*8 cir_drt1_st1(*),cir_drt2_st2(*),
     &       ci_drt1_st1(*),ci_drt2_st2(*)
      real*8 scr(*),sng(2),sngtmp,maxvalue
c
c   sng(1)= state1:  determine whether current and restart ci has the sa
c   sng(2)= state2:  determine whether current and restart ci has the sa
c
c------------------------------------------------------------------
c   ciscf(k,m) = number of csf k of state m. MB, 28jul05
c
c ................
c   This block selects the csf with the largest coefficient at the
c   reference ci vector. MB 28jul05
c
      k1 = 1
      maxvalue = dabs(cir_drt1_st1(cicsf(k1,1)))
      do icf = 2,cicnt(1)
        if (dabs(cir_drt1_st1(cicsf(icf,1))) .gt. maxvalue) then
        maxvalue = dabs(cir_drt1_st1(cicsf(icf,1)))
        k1 = icf
        endif
      enddo
c
      k2 = 1
      maxvalue = dabs(cir_drt2_st2(cicsf(k2,2)))
      do icf = 2,cicnt(2)
        if (dabs(cir_drt2_st2(cicsf(icf,2))) .gt. maxvalue) then
        maxvalue = dabs(cir_drt2_st2(cicsf(icf,2)))
        k2 = icf
        endif
      enddo
c
      sng(1)=
     & sign(one,ci_drt1_st1(cicsf(k1,1))*cir_drt1_st1(cicsf(k1,1)))
      write(nlist,*) ' '
      write(nlist,*) '---------------   STATE 1   -----------------'
      write(nlist,*) 'CFS used in the phase determination:'
      write(nlist,*) 'CSF number:',cicsf(k1,1)
      write(nlist,*) 'Reference, current CI coef:',
     &      cir_drt1_st1(cicsf(k1,1)),ci_drt1_st1(cicsf(k1,1))
      write(nlist,*) 'Signal:',sng(1)
      write(nlist,*) '---------------------------------------------'
      write(nlist,*) ' '
        do i= 1,cicnt(1)
        sngtmp=
     &   sign(one,ci_drt1_st1(cicsf(i,1))*cir_drt1_st1(cicsf(i,1)))
          if (sng(1).ne.sngtmp) then
           call bummer  ('CI1: inconsistent sign',0,wrnerr)
           write(nlist,*)'CSF number:',cicsf(i,1)
           write(nlist,*)'Reference, current CI coef:',
     &      cir_drt1_st1(cicsf(i,1)),ci_drt1_st1(cicsf(i,1))
           write(nlist,*)'sng(1),sngtmp:',sng(1),sngtmp
           write(nlist,*) ' '
          endif
        enddo
c
      sng(2)=
     & sign(one,ci_drt2_st2(cicsf(k2,2))*cir_drt2_st2(cicsf(k2,2)))
      write(nlist,*) ' '
      write(nlist,*) '---------------   STATE 2   -----------------'
      write(nlist,*) 'CFS used in the phase determination:'
      write(nlist,*) 'CSF number:',cicsf(k2,2)
      write(nlist,*) 'Reference, current CI coef:',
     &      cir_drt2_st2(cicsf(k2,2)),ci_drt2_st2(cicsf(k2,2))
      write(nlist,*) 'Signal:',sng(2)
      write(nlist,*) '---------------------------------------------'
      write(nlist,*) ' '
        do i= 1,cicnt(2)
        sngtmp=
     &   sign(one,ci_drt2_st2(cicsf(i,2))*cir_drt2_st2(cicsf(i,2)))
          if (sng(2).ne.sngtmp) then
           call bummer ('CI2: inconsistent sign',0,wrnerr)
           write(nlist,*)'CSF number:',cicsf(i,2)
           write(nlist,*)'Reference, current CI coef:',
     &      cir_drt2_st2(cicsf(i,2)),ci_drt2_st2(cicsf(i,2))
           write(nlist,*)'sng(2),sngtmp:',sng(2),sngtmp
           write(nlist,*) ' '
          endif
        enddo
c
      if (sng(1).eq.sng(2)) then
       phasefactor=1
      else
       phasefactor=-1
      endif
      write(nlist,100)
 100  format(//' Determination of phase factor: '/)
      do i=1,2
       if (sng(i).eq.1) then
        write(nlist,*)
     &   'SAME phase of current and reference CI wave function',
     &   ' for state', i
       else
        write(nlist,*)
     &   'OPPOSITE phase of current and reference CI wave function',
     &   ' for state', i
       endif
      enddo
101   format(/' --> Relative phase factor: ', i2 //)
      write(nlist,101) phasefactor
      return
      end
c
c*********************************************************************++
c
      block data xx
      implicit logical(a-z)
c da file record parameters
C     Common variables
C
      INTEGER             vecrln,indxln, ssym1,ssym2
      COMMON / INF4   /   vecrln,indxln, ssym1,ssym2
C

      integer   nfilmx
      parameter(nfilmx=55)
      integer         nunits
      common /cfiles/ nunits(nfilmx)
      character*60    fname
      common /cfname/ fname(nfilmx)

c------------------------------------------------------------------
c     # nlist = standard listing file.
      integer      nlist
      equivalence (nlist,nunits(1))
      data nlist/6/, fname(1)/'phasecils'/
c------------------------------------------------------------------
c     # tape5 = user input file.
      integer      tape5
      equivalence (tape5,nunits(2))
      data tape5/5/, fname(2)/'trnciin'/
c------------------------------------------------------------------
c     # phout  = summary listing file.
      integer      phout
      equivalence (phout,nunits(3))
      data phout/13/, fname(3)/'ciphase'/
c------------------------------------------------------------------
c include(data.common)
      integer maxsym
      parameter (maxsym=8)
      integer symtab,sym12,sym21,nbas,lmda,lmdb,nmbpr,strtbw,strtbx
      integer blst1e,nmsym,mnbas,cnfx,cnfw,scrlen
      common/data/symtab(maxsym,maxsym),sym12(maxsym),sym21(maxsym),
     + nbas(maxsym),lmda(maxsym,maxsym),lmdb(maxsym,maxsym),
     + nmbpr(maxsym),strtbw(maxsym,maxsym),strtbx(maxsym,maxsym),
     + blst1e(maxsym),nmsym,mnbas,cnfx(maxsym),cnfw(maxsym),scrlen
c
      data symtab/
     & 1,2,3,4,5,6,7,8,
     & 2,1,4,3,6,5,8,7,
     & 3,4,1,2,7,8,5,6,
     & 4,3,2,1,8,7,6,5,
     & 5,6,7,8,1,2,3,4,
     & 6,5,8,7,2,1,4,3,
     & 7,8,5,6,3,4,1,2,
     & 8,7,6,5,4,3,2,1/
c
      data sym12/1,2,3,4,5,6,7,8/

c     # da record lengths
c     # record lengths for ci created direct acc. files
c     #
c     # indxln : da record length for index file
c     # vecrln : da record length for vector files
c
c     # should be tuned for your machine....
c

*@if defined vax ||  ( ibm && vector)
*C      # vax maximum da record length is 4095
*       data indxln/4095/, vecrln/4095/
*@else
       data indxln/4096/, vecrln/32768/
*@endif

       end
c
c*****************************************************+++
c
      subroutine datain
c
c  read user input.
c
       implicit none
      integer   nfilmx
      parameter(nfilmx=55)
      integer         nunits
      common /cfiles/ nunits(nfilmx)
      integer      nlist
      equivalence (nlist,nunits(1))
      integer      tape5
      equivalence (tape5,nunits(2))
c
      character*60    fname
      common /cfname/ fname(nfilmx)
c
      character*60 fnamex
      equivalence (fname(nfilmx),fnamex)
c
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)

       integer ierr,i,j
c
      integer         nroot1,nroot2,lvlprt,drt1,drt2
      common /indata/ nroot1,nroot2,lvlprt,drt1,drt2
c
*@ifndef nonamelist
      namelist /input/ lvlprt, nroot1,nroot2,drt1,drt2
*@endif
c
      ierr = 0
c
c
      open( unit = tape5, file = fname(2), status = 'old')
c
c     read and echo standard input until eof is reached.
c
      write(nlist,6010)
6010  format(' echo of the input for program phaseci (file:trnciin)')
      call echoin( tape5, nlist, ierr )
      if ( ierr .ne. 0 ) then
         call bummer(' datain: from echoin, ierr=',ierr,faterr)
      endif
c
      rewind tape5

c defaults
       nroot1=0
       nroot2=0
       lvlprt=0
       drt1=0
       drt2=0
c
c     # read namelist input and keep processing if eof is reached.
c
c
*@ifdef nonamelist
*@elif defined  nml_required
*      read( tape5, nml=input, end=3 )
*@else
      read( tape5, input, end=3 )
*@endif
c
3     continue
c

      if (ierr.ne.0)
     . call bummer('input file errors, exiting',ierr,2)
c
      write(nlist,'(/3x,''State to be considered in calculation:'')')
      write(nlist,'(17x,''DRT    State'')')
      write(nlist,'(15x,''--------------'')')
      write(nlist,'(1x,''CI vector1:'',6x,i1,7x,i1)')drt1,nroot1
      write(nlist,'(1x,''CI vector2:'',6x,i1,7x,i1/)')drt2,nroot2
      write(nlist,6020)
c
c     # filev1 = bra vector file.
      nunits(7)=7
      write(fname(7),'(a9,i1)') 'civfl.drt',drt1
c------------------------------------------------------------------
c     # filev2 = ket vector file.
      nunits(8)=8
      write(fname(8),'(a9,i1)') 'civfl.drt',drt2
c------------------------------------------------------------------
c     # filevr1 = bra drt file.
      nunits(9)=9
      write(fname(9),'(a17,i1)') 'civfl_restart.drt',drt1
c------------------------------------------------------------------
c     # filevr2 = ket drt file.
      nunits(10)=10
      write(fname(10),'(a17,i1)') 'civfl_restart.drt',drt2
c------------------------------------------------------------------
c     # fildrt1 = bra drt file.
      nunits(11)=11
      write(fname(11),'(a8,i1)') 'cidrtfl.',drt1
c------------------------------------------------------------------
c     # fildrt2 = ket drt file.
      nunits(12)=12
      write(fname(12),'(a8,i1)') 'cidrtfl.',drt2
c------------------------------------------------------------------
c
      write(nlist,*)'units and filenames:'
c
      do 110 i = 1, (nfilmx - 1)
         if ( nunits(i) .eq. 0 ) goto 110
111      continue
         write(nlist,6050) i, nunits(i), fname(i)
110   continue
6050  format(1x,i4,': (',i2,')',4x,a)
6051  format(1x,i4,': (',i2,')',4x,a,t35,a)
c
      write(nlist,6020)
      close(unit=tape5)
c
      return
6020  format(1x,72('-'))
      end
c
c****************************************************************
c
      subroutine getciphase (
     & unit,   buffer,   lenrec,  iroot,
     & ivec,   civec,    nvalz,   ncsf,fname)
c
       implicit none  
c     unit = file to read ci vector
c     buffer    = ci buffer
c     iroot     = root to read
c     lenrec    = record length
c
c  ##  common block section
c
      character*(*) fname
      integer   nfilmx
      parameter(nfilmx=55)
      integer         nunits
      common /cfiles/ nunits(nfilmx)
      integer      nlist
      equivalence (nlist,nunits(1))
c
      real*8 civec(*)
      integer cicnt(2),cicsf(200,2)
      common  /phase/cicnt,cicsf
c
c  ##  integer section
c
      integer blkcsf
      integer iroot,i,icsf,ivec
      integer lenrec
      integer ncsf,nvalz
      integer refptr
      integer unit
      integer*8 offset
c
c  ##  real*8 section
c
      real*8 buffer(lenrec)
c
c----------------------------------------------------------------------
        write(nlist,'(1x,a)')
     &   'Dominant configurations in CI vector'
        refptr=0
        cicnt(ivec)=0
          do icsf=1,nvalz,lenrec
          blkcsf=min(lenrec,(nvalz-icsf+1))
c         call getvec(unit,lenrec,iroot,ncsf,buffer,icsf,blkcsf)
          offset=(iroot-1)*ncsf+icsf
          call getputvec(fname(1:len_trim(fname)),buffer,blkcsf,
     .      offset,'openrd')

             do i=1,blkcsf
              if (abs(buffer(i)).gt.0.05) then
               cicnt(ivec)=cicnt(ivec)+1
               cicsf(cicnt(ivec),ivec)=(icsf-1)*lenrec+i
               civec((icsf-1)*lenrec+i)=buffer(i)
               write(nlist,*) 'civec( csf# =',(icsf-1)*lenrec+i,
     &          ' )=',civec((icsf-1)*lenrec+i)
              endif
             enddo ! end of: do i=1,blkcsf
          enddo ! end of: do icsf=1,nvalz,lenrec
c
        return
        end
c
c***********************************************************************
c
      subroutine tapind(
     & ssym,    nmot,   nmsym,   core,
     & fildrt,  fndrt,  pthz,    pthy,
     & pthx,    pthw,   nintpt,  intorb,
     & nvalwk,  nvalz,  nvaly,   nvalw,
     & nvalx,   nfct,   neli,    ncsf,
     & totspc)
c
       implicit none
c  ##  parameter & common block section
c
      integer mxinor
      parameter (mxinor=128)
c
      integer nmomx
      parameter (nmomx=510)
c
      integer lrow
      parameter (lrow= 1023 )
c
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer   nfilmx
      parameter(nfilmx=55)
      integer         nunits
      common /cfiles/ nunits(nfilmx)
      integer nlist
      equivalence (nunits(1),nlist)
c
c  ##  integer section
c
      integer fildrt
      integer intorb,ios
      integer lenbuf
      integer nmot, nmsym, nvalw,nvalwk,nvalx,nvaly,nvalz,nintpt,nfct,
     &        neli,nrow,nfvt,nsym,nvwalk(4),ncsf,nwalk
      integer pthx,pthy,pthw,pthz
      integer ssym
      integer totspc
      integer xbar(4)
c
c  ##  real*8 section
c
      real*8 core(*)
c
c  ##  character section
c
      character*60 fndrt
      character*80 title
c
c----------------------------------------------------------------------
c
c  bummer error types.
c                             memory for dynamic allocation
c                             location of indx array
c                             location of indsym array
c
      open( unit = fildrt, file = fndrt, status = 'old' ,
     +       form = 'formatted' ,iostat=ios)
      if (ios.ne.0)
     . call bummer ('can not open unit ',fildrt,2)
c
      title='xxxxxxxxxxxxxxxxxxxxxxxxxx'
      call rdhdrtn(
     & title,          nmot,       intorb, nfct,
     & nfvt,           nrow,       nsym,   ssym,
     & xbar,           nvwalk,     ncsf,  lenbuf,
     & nwalk,          mxinor,     nmomx,  lrow,
     & core,           nlist,      fildrt )
c
      pthz=xbar(1)
      pthy=xbar(2)
      pthx=xbar(3)
      pthw=xbar(4)
      nvalz=nvwalk(1)
      nvaly=nvwalk(2)
      nvalx=nvwalk(3)
      nvalw=nvwalk(4)
      nintpt=pthz+pthy+pthx+pthw
      nvalwk=nvalz+nvaly+nvalx+nvalw
      nmsym=nsym
c
      close (unit = fildrt)
c
      return
      end
c
c**************************************************************
c
      subroutine rdhdrtn(
     & title,  nmot,   niot,   nfct,
     & nfvt,   nrow,   nsym,   ssym,
     & xbar,   nvalw,  ncsft,  lenbuf,
     & nwalk,  niomx,  nmomx,  nrowmx,
     & buf,    nlist,  ndrt )
c
c  read the header info from the drt file.
c
c
      implicit none 

C    VARIABLE DECLARATIONS FOR subroutine rdhdrtn
C
C     Parameter variables
C
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
C
C     Argument variables
C
      CHARACTER*80        TITLE
C
      INTEGER             BUF(*),      LENBUF,      NCSFT,       NDRT
      INTEGER             NFCT,        NFVT,        NIOMX,       NIOT
      INTEGER             NLIST,       NMOMX,       NMOT,        NROW
      INTEGER             NROWMX,      NSYM,        NVALW(4),    NWALK
      INTEGER             SSYM,        XBAR(4)
C
C     Local variables
C
      INTEGER             IERR,        NHDINT,      NVALWT,      VRSION
*@ifdef spinorbit
      logical spnorb, spnodd
      integer lxyzir(3)
*@endif

C
C    END OF VARIABLE DECLARATIONS FOR subroutine rdhdrtn


c     # bummer error types.
c     # title card...
      rewind ndrt
      ierr=0
      read(ndrt,'(a)',iostat=ierr)title
      if(ierr.ne.0)call bummer('rdhead: title ierr=',ierr,faterr)
*@ifdef spinorbit
      read(ndrt,'(2L4,3I4)',iostat=ierr)spnorb, spnodd, lxyzir
      if(ierr.ne.0)call bummer('rdhead: spnorb, ierr=',ierr,faterr)
      if(spnorb) call bummer('rdhead: no SO-CI calcs',0,faterr)
*@endif


c     write(nlist,6010)title
c
c     # number of integer drt parameters, buffer length, and version.
c
      call rddbl( ndrt, 3, buf, 3 )
c
      vrsion = buf(1)
      lenbuf = buf(2)
      nhdint = buf(3)
c
*@ifdef spinorbit
      if ( vrsion .ne. 6 ) then
       call bummer('drt version 6 required',vrsion,2)
      endif
*@else
*      if ( vrsion .ne. 3 ) then
*         write(nlist,*)
*     &    '*** warning: rdhdrt: drt version .ne. 3 *** vrsion=',vrsion
*      endif
*@endif
c
c     # header info...
c
      call rddbl( ndrt, lenbuf, buf, nhdint )
c
      nmot     = buf(1)
      niot     = buf(2)
      nfct     = buf(3)
      nfvt     = buf(4)
      nrow     = buf(5)
      nsym     = buf(6)
      ssym     = buf(7)
      xbar(1)  = buf(8)
      xbar(2)  = buf(9)
      xbar(3)  = buf(10)
      xbar(4)  = buf(11)
      nvalw(1) = buf(12)
      nvalw(2) = buf(13)
      nvalw(3) = buf(14)
      nvalw(4) = buf(15)
      ncsft    = buf(16)
c
      nwalk  = xbar(1)  + xbar(2)  + xbar(3)  + xbar(4)
      nvalwt = nvalw(1) + nvalw(2) + nvalw(3) + nvalw(4)
c
      write(nlist,6020)nmot,niot,nfct,nfvt,nrow,nsym,ssym,lenbuf
      write(nlist,6100)'nwalk,xbar',nwalk,xbar
      write(nlist,6100)'nvalwt,nvalw',nvalwt,nvalw
      write(nlist,6100)'ncsft',ncsft
c
      if(niot.gt.niomx)then
         write(nlist,6900)'niot,niomx',niot,niomx
         call bummer('rdhead: niomx=',niomx,faterr)
      endif
      if(nmot.gt.nmomx)then
         write(nlist,6900)'nmot,nmomx',nmot,nmomx
         call bummer('rdhead: nmomx=',nmomx,faterr)
      endif
      if(nrow.gt.nrowmx)then
         write(nlist,6900)'nrow,nrowmx',nrow,nrowmx
         call bummer('rdhead: nrowmx=',nrowmx,faterr)
      endif
c
      return
c
6010  format(/' drt header information:'/1x,a)
6020  format(
     & ' nmot  =',i6,' niot  =',i6,' nfct  =',i6,' nfvt  =',i6/
     & ' nrow  =',i6,' nsym  =',i6,' ssym  =',i6,' lenbuf=',i6)
6100  format(1x,a,':',t15,5i9)
6900  format(/' error: ',a,3i10)
      end
