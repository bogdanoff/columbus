!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
       program densav

c     Based on a set of at most 10
c     NO coefficient files an averaged one-electron density
c     is calculated and diagonalized to produce a new
c     set of state-averaged NOs
c
c     input  file : densavin
c     output file : densavls     listing file
c                   nocoef.new   new averaged NOs
c
c     namelist input:
c
c                    nentry: number of NO coefficient files to
c                            average
c                    fnames(i) : filename of NO coefficient file # i
c                    factor(i) : (unnormalized) weight of NO set # i
c
c--------------------------------------------------------------------

c
c

         implicit none 
         integer nmax
        parameter ( nmax = 20)
        character*80 fmoc,fn,fnames(nmax)
        real*8  factor(nmax)
        integer nentry,nomode,nlmo
        common /datain/ factor,fnames,fmoc,nentry,nomode,nlmo

        integer nmomx
        parameter (nmomx=510)
        real*8 nocoef(nmomx*nmomx),occ(nmomx)
     .   ,newdens(nmomx*nmomx),stdmo(nmomx,nmomx)
     .   ,newnos(nmomx*nmomx),newocc(nmomx)
     .   ,invstdmo(nmomx*nmomx)

        integer i       

       call readinput

       call wzero(nmomx*nmomx,newdens,1)

       call readmo(20,fmoc,stdmo,occ,1,1.0d0)

       call calcinv(stdmo,invstdmo)

       do i=1,nentry

        call readmo(20,fnames(i),nocoef,occ,0,factor(i))

        call calcaodens(newdens,nocoef, occ,invstdmo)

       enddo

        call calcnos (newdens,newnos,newocc,stdmo,nomode,nlmo)

        fn='nocoef.new'
        call writemo(21,fn,newnos,newocc)

        call bummer('normal termination',0,3)
        stop       
        end


        subroutine readinput

         implicit none 
         integer nmax
        parameter ( nmax = 20)
        character*80 fmoc,fnames(nmax)
        real*8  f2,factor(nmax)
        integer nentry,nomode,nlmo
        namelist /input/factor,fnames,fmoc,nentry,nomode,nlmo
        common /datain/ factor,fnames,fmoc,nentry,nomode,nlmo

        integer ierr,i, donorm
        integer faterr
        logical ex
        parameter (faterr=2)

        open (unit=5,file='densavin',status='old')
        open (unit=6,file='densavls',status='unknown')
        call echoin (5,6,ierr)
      if ( ierr .ne. 0 ) then
         call bummer(' datain: from echoin, ierr=',ierr,faterr)
      endif
c
      rewind 5
          do i=1,nmax
            fnames(i)="x"
            factor(i)=0.0d0
            fmoc="Y"
            nentry=0
          enddo
          nomode=0
          nlmo=0

         read (5,input,end=3)

   3     continue

         close(5)
c        renormalization
         donorm = 1
         f2=0.0d0
         do i=1,nentry
           f2=factor(i)+f2
          if (fnames(i).eq."x")
     .      call bummer('missing NO coefficient file name',i,2)
          if (abs(factor(i)).lt.1.d-6)
     .      call bummer('missing weight factor',i,2)
         inquire(file=fnames(i), exist=ex)
          if (.not.ex)
     .      call bummer('NO coefficient file does not exist ',i,2)
          if (factor(i).lt.0.0) then
             write(6,*) 'factor(i) < 0,
     . no normalization will be performed. i=',i
            donorm = 0
          endif
         enddo

         if (donorm.eq.1) then
          do i=1,nentry
            factor(i) = factor(i)/f2
          enddo
         endif

         if (nomode.gt.0) then
           write(6,*) 'NO construction mode=',nomode
           write(6,*) 'MOs to be retained per irrep=',nlmo
         else
           fmoc=fnames(1)
         endif 


         write(6,*) 'Standard MO basis file :',fmoc
         write(6,*) 'NO coefficient file   averaging factor '
         do i=1,nentry
          write(6,'(a20,3x,f6.3)') fnames(i)(1:20), factor(i)
         enddo
         return
         end



         subroutine readmo(unitno,fname,nocoef,occ,noocc,fac)
          implicit none 
          integer nmomx
         parameter ( nmomx=510)
         real*8 nocoef(nmomx*nmomx),occ(nmomx),fac
         character*80 fname
         integer unitno,noocc
         integer mxtitle
         parameter (mxtitle = 20)
         character*80 titles(mxtitle)
         integer filerr,syserr,ntitle
         integer nsymmx
         parameter (nsymmx = 8)
         integer nsym,nbfpsy(nsymmx),nmopsy(nsymmx)
         character*4 labels(nmomx)
         character*80 afmt

         integer snsym,snbfpsy(nsymmx),snmopsy(nsymmx)

         common /check/ snsym,snbfpsy,snmopsy,labels

          integer i,ntot
         logical first
         data first /.true./
         save first,ntot


c  header information
         open (unit=unitno,file=fname,status='old',err=99)
         call moread(unitno,10,filerr,syserr,mxtitle,ntitle,
     .    titles,afmt,nsym,nbfpsy,nmopsy,labels,nmomx*nmomx,
     .     nocoef )

          ntot=0
          if (first) then
           snsym=nsym
           do i=1,nsym
             snbfpsy(i)=nbfpsy(i)
             snmopsy(i)=nmopsy(i)
             ntot=ntot+nmopsy(i)
           enddo
          else
           if (snsym.ne.nsym)
     .      call bummer ('readmo: snsym.ne.nsym',snsym,2)
           do i=1,nsym
             if (snbfpsy(i).ne.nbfpsy(i))
     .       call bummer ('readmo: snbfpsy(i).ne.nbfpsy(i),i=',i,2)
             if (snmopsy(i).ne.nmopsy(i))
     .       call bummer ('readmo: snmopsy(i).ne.nmopsy(i),i=',i,2)
           enddo
          endif

c mo coefficients
         call moread(unitno,20,filerr,syserr,mxtitle,ntitle,
     .    titles,afmt,nsym,nbfpsy,nmopsy,labels,nmomx*nmomx,
     .     nocoef )


c occupation numbers
         if (noocc.eq.1) return
         call moread(unitno,40,filerr,syserr,mxtitle,ntitle,
     .    titles,afmt,nsym,nbfpsy,nmopsy,labels,nmomx,
     .     occ )


 101    format(10i6)

         do i=1,ntitle
         write(6,*) titles(i)
         enddo
         write(6,*) 'scaling occ(*) by ',fac
         do i = 1,ntot
          occ(i)=occ(i)*fac
         enddo
         write(6,*) '-----------------------------------------'
c        call prblks(fname,nocoef,nsym,nmopsy,
c    .    nmopsy,'ao  ','no  ',3,6)
         return
  99     continue
         call bummer ('couldnt open nocoef file',0,2)
         return
         end


         subroutine calcaodens(newdens,nocoef, occ,
     .       standardmos)
          implicit none 
          integer nmomx
         parameter ( nmomx=510)
         real*8 nocoef(nmomx*nmomx),occ(nmomx)
         real*8 newdens(nmomx*nmomx)
         real*8 aodens(nmomx*nmomx),standardmos(nmomx*nmomx)

         integer nsymmx
         parameter (nsymmx=8)
         integer i,j,k,l,offset,off2

         integer nsym,nbfpsy(nsymmx),nmopsy(nsymmx)
         character*4 labels(nmomx)

          real*8 elnum
         common /check/ nsym,nbfpsy,nmopsy,labels

c offset: offset in nocoef and aodens
c off2  : offset in occ

         offset=1
         off2=0
         elnum=0
         call wzero(nmomx*nmomx,aodens ,1)
         do i=1,nsym
           do k=1,nmopsy(i)
            elnum=elnum+occ(off2+k)
           enddo
           write(6,*) 'elnum=',elnum

c  transform NO coefficients to standard MO basis
c   C(MO,NO) = sum(AO) CINV(AO,MO)*C(AO,NO)

       call mxmal(standardmos(offset),nocoef(offset),aodens(offset),
     .        nmopsy(i) )
       call dcopy_wr(nmopsy(i)*nmopsy(i),aodens(offset),
     .        1,nocoef(offset),1)

c   transform NO density into standard MO basis
c   DMO(i,j) = sumkl C(MO,NO)*DNO * CT(MO,NO)
c
           call mxmxmt(nocoef(offset),occ(off2+1),
     .    newdens(offset),nmopsy(i))


           offset=offset+nmopsy(i)*nmopsy(i)
           off2=off2+nmopsy(i)
          enddo
*@ifdef debug
*         call prblks('newdens',newdens,nsym,nmopsy,
*     .    nmopsy,'no  ','ao  ',3,6)
**
*         call prblks('nocoef',nocoef,nsym,nmopsy,
*     .    nmopsy,'no  ','mo  ',3,6)
*@endif

          return
          end

        subroutine calcnos (newdens,newnos,newocc,standmo,nomode,nlmo)
         implicit none 
         integer nmomx
        parameter (nmomx=510)
        real*8 newdens(nmomx*nmomx),newnos(nmomx*nmomx),newocc(nmomx)
        real*8 scr(nmomx*(nmomx+1)/2)
        real*8 standmo(nmomx*nmomx)
        real*8 finalmo(nmomx*nmomx)
        integer nomode,nlmo


         integer ij,i,j,k,l,offset,off2,kl,imd
         integer nsymmx
         real*8 elnum,trace
         parameter (nsymmx = 8)


         integer nsym,nbfpsy(nsymmx),nmopsy(nsymmx)
         character*4 labels(nmomx)

         common /check/ nsym,nbfpsy,nmopsy,labels

c offset: offset in nocoef and aodens
c off2  : offset in occ

         offset=0
         off2=0
          elnum=0.0d0
          trace=0.0d0
         write(6,'(/,''   Occupation numbers''/)')
         do i=1,nsym

c copy density block into lower triangular form

           ij=0
           kl=0
           do k=1,nmopsy(i)
            do l=1,k
             ij=ij+1
             kl=kl+1
             scr(ij)=newdens(offset+kl)
            enddo
             trace=trace+newdens(offset+kl)
             kl=kl+nmopsy(i)-k
           enddo
           if (nomode.eq.1) then
c  remove offdiagonal lmo,lmo block = elements 1:nlmo*(nlmo+1)/2
            call wzero(nlmo*(nlmo+1)/2,scr,1)
c add diagonal lmo,lmo elements
             kl=0
            do k=1,nlmo
              kl=kl+k
c additional term to ensure proper ordering
              scr(kl)=2.0d0 -dble(k)*0.0001d0
            enddo
c remove offdiagonal lmo,vo block 
            do k=nlmo+1,nmopsy(i)
               do l=1,nlmo
                 kl=kl+1
                 scr(kl)=0.0d0
               enddo
               kl=kl+(k-nlmo)
            enddo 
           endif 

c set newnos to diagonal matrix

           call wzero(nmopsy(i)*nmopsy(i),newnos(offset+1),1)
           ij=0
           do k=1,nmopsy(i)
           ij=ij+k
            newnos(offset+ij)=1.0d0
           ij=ij+nmopsy(i)-k
           enddo


c diagonalize it

*@ifdef debug
*         call prblks('erduw newn',newnos(offset+1),1,nmopsy(i),
*     .    nmopsy(i),'no  ','ao  ',3,6)
*         call plblks('erduw,scr',scr,1,nmopsy(i),'no  ',
*     .   3,6)
*@endif
           call erduw(scr,newnos(offset+1),nmopsy(i),1.d-14)

c readout occupation numbers

            ij=0
            do k=1,nmopsy(i)
             ij=ij+k
             newocc(off2+k)=scr(ij)
            enddo
               

c sort nos in ascending order

       call bubble(newnos(offset+1),newocc(off2+1),nmopsy(i),
     .      nmopsy(i) )
cmd
c  ##  write out the occupation
c        write(6,'(/,''  Symmetry: '',i2)')i
c        write(6,'(5(f12.7,3x))')(newocc(off2+imd),imd=1,nmopsy(i))

c  fix odd correction term
            if (nomode.eq.1) then
              do k=1,nlmo
                   newocc(off2+k)=2.0d0
              enddo
            endif

           write(6,'(a,i0)') 'NOs in MO basis of irrep',i 
           call prvblk(newnos(offset+1),newocc(off2+1),
     .      nmopsy(i),nmopsy(i),nmopsy(i),0,0,' mo ',' no ','occ',1,6)
cmd
c back transformation into ao basis
*@ifdef debug
*            do k=1,nmopsy(i)
*              elnum=elnum+newocc(off2+k)
*              write(*,*) 'newocc(',off2+k,')=',newocc(off2+k)
*            enddo
*            write(*,*) 'trace=',trace
*            write(*,*) 'elnum=',elnum
*@endif

       call mxmal(standmo(offset+1),newnos(offset+1),
     .  finalmo(offset+1),nmopsy(i) )
        call dcopy_wr(nmopsy(i)*nmopsy(i),finalmo(offset+1),1,
     .      newnos(offset+1),1)
        off2=off2+nmopsy(i)
        offset=offset+nmopsy(i)*nmopsy(i)
           enddo
           return
           end


        subroutine writemo(unitno,fname,newnos,newocc)
         implicit none 
         integer nmomx
        parameter (nmomx = 510)
        integer unitno
        character*80 fname
        real*8 newnos(nmomx*nmomx),newocc(nmomx)
        integer nmax
        parameter ( nmax = 20)
        character*80 fmoc,fnames(nmax)
        real*8  factor(nmax)
        integer i
        integer nentry,nomode,nlmo
        common /datain/ factor,fnames,fmoc,nentry,nomode,nlmo

         integer mxtitle
         parameter (mxtitle = 20)
         character*80 titles(mxtitle)
         integer filerr,syserr,ntitle
         integer nsymmx
         parameter (nsymmx = 8)
         integer nsym,nbfpsy(nsymmx),nmopsy(nsymmx)
         character*4 labels(nmomx)
         character*80 fmt

         integer snsym,snbfpsy(nsymmx),snmopsy(nsymmx)

         common /check/ snsym,snbfpsy,snmopsy,labels

          fmt = '(3d25.15)'
          ntitle=min(mxtitle,1+nentry)
          write(titles(1),*)'NO COEFFICIENTS FROM ',nentry,
     .    ' AVERAGED DENSITIES'
          do i=1,min(mxtitle-1,nentry)
           write(titles(i+1),*)
     .  'taken from ',fnames(i)(1:30),'with factor ',
     .      factor(i)
          enddo

         open(unit=unitno,file=fname,status='unknown')
         call mowrit(unitno,10,filerr,syserr,ntitle,titles,
     .  fmt,snsym,snmopsy,snmopsy,labels,newnos)

         call mowrit(unitno,20,filerr,syserr,ntitle,titles,
     .  fmt,snsym,snmopsy,snmopsy,labels,newnos)


         call mowrit(unitno,40,filerr,syserr,ntitle,titles,
     .  fmt,snsym,snmopsy,snmopsy,labels,newocc)

         call prblks(6,newnos,snsym,snmopsy,
     .    snmopsy,'ao  ','no  ',3,6)



         close(unitno)

         return
         end



      subroutine bubble ( c, e, n, m )
c
c  sort n energies and n vectors (of length m)
c  decreasing order
c
      implicit real*8 (a-h,o-z)
c
      dimension c(*), e(*)
c
      if ( n .eq. 1 ) return
      ipt = 0
c
      do 40 i= 1, n-1
      low = i
      elow = e( i )
c
      do 10 j= i+1 , n
      if ( e(j) .lt. elow ) go to 10
      low = j
      elow = e( j )
   10 continue
c
      if ( low .eq. i ) go to 30
c
c  swap low vector and energy
c
      tmp = e( i )
      e( i ) = e( low )
      e( low ) = tmp
      jpt = ( low-1 ) * m
      do 20 k= 1, m
      tmp = c( ipt+k )
      c( ipt+k ) = c( jpt+k )
      c( jpt+k ) = tmp
   20 continue
c
   30 continue
      ipt = ipt + m
   40 continue
c
      return
      end



      subroutine erduw(a,b,na,epslon)
c taken mainly from version 4, aug., 1971, of jacscf, u. of wa.
c   matrix diagonalization by the jacobi method.
c     a = real symmetric matrix to be diagonalized. it is stored
c       by columns with all sub-diagonal elements omitted, so a(i,j) is
c       stored as a((j*(j-1))/2+i).
c     b = matrix to be multiplied by the matrix of eigenvectors.
c     na = dimension of the matrices.
c      epslon is the convergence criterion for off-diagonal elements.
c     modified nov 82 r.a.
c     programm keeps track of non-diagonal norm and adjusts thresh
c     dynamically. the eventually quadratic convergence of jacobi is
c     exploited to reduce thresh faster at the end.
c
      implicit real*8  (a-h,o-z)
      dimension a(1), b(1)
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      data a0, a1s2 /0.0d0, 0.5d0/
c
      if(na.eq.1) return
      loopc=64
      sumnd=a0
      sum=a0
      ij=1
      do 24 i=1,na
      do 16 j=1,i
      term=a(ij)*a(ij)
      sum=sum+term
      if(i.eq.j) go to 16
      sumnd=sumnd+term
   16 ij=ij+1
   24 continue
      thrshg= sqrt((sum+sumnd)/na)*epslon
      small=sumnd*epslon*na*na
   32 if (sumnd.lt.small) go to 35
      thresh= sqrt(sumnd+sumnd)/na
      go to 37
   35 thresh=thresh*0.03d0
   37 thresh=dmax1(thresh,thrshg)
      n=0
      ij=2
      jj=1
      do 112 j=2,na
      jj=jj+j
      jm1=j-1
      ii=0
      do 104 i=1,jm1
      ii=ii+i
      if( abs(a(ij)).lt.thresh) go to 104
      n=n+1
      sumnd=sumnd-a(ij)*a(ij)
      sum=a1s2*(a(jj)+a(ii))
      term=a1s2*(a(jj)-a(ii))
      amax= sign( sqrt(term*term+a(ij)*a(ij)),term)
      c= sqrt((amax+term)/(amax+amax))
      s=a(ij)/(c*(amax+amax))
      a(ii)=sum-amax
      a(jj)=sum+amax
      a(ij)=a0
      im1=i-1
      if(im1) 40,56,40
   40 ki=ii-i
      kj=jj-j
      do 48 k=1,im1
      ki=ki+1
      kj=kj+1
      term=c*a(ki)-s*a(kj)
      a(kj)=s*a(ki)+c*a(kj)
   48 a(ki)=term
   56 if(jm1.eq.i) go to 72
      ip1=i+1
      ik=ii+i
      kj=ij
      do 64 k=ip1,jm1
      kj=kj+1
      term=c*a(ik)-s*a(kj)
      a(kj)=s*a(ik)+c*a(kj)
      a(ik)=term
   64 ik=ik+k
   72 if(j.eq.na) go to 88
      jp1=j+1
      ik=jj+i
      jk=jj+j
      do 80 k=jp1,na
      term=c*a(ik)-s*a(jk)
      a(jk)=s*a(ik)+c*a(jk)
      a(ik)=term
      ik=ik+k
   80 jk=jk+k
   88 ki=im1*na
      kj=jm1*na
      do 96 k=1,na
      ki=ki+1
      kj=kj+1
      term=c*b(ki)-s*b(kj)
      b(kj)=s*b(ki)+c*b(kj)
   96 b(ki)=term
  104 ij=ij+1
  112 ij=ij+1
      loopc=loopc-1
      if(loopc) 120,1024,120
  120 if(thresh.le.thrshg.and.n.eq.0) return
      go to 32
 1024 write (*,2048)
      call bummer('erduw: loopc=',loopc,faterr)
 2048 format(12h error erduw)
      return
      end



      subroutine mxmal (A,B,C,dim )
       implicit none 
       integer i,j,k,dim
      real*8 A(dim,dim),b(dim,dim),c(dim,dim)

      do i=1,dim
       do j=1,dim
        do k=1,dim
          c(i,j)=c(i,j)+a(i,k)*b(k,j)
        enddo
       enddo
      enddo
      return
      end



      subroutine mxmxmt(A,B,C,dim)
       implicit none 
       integer i,j,k,dim
      real*8 tr,A(dim,dim),b(dim),c(dim,dim)

      do i=1,dim
       do j=1,dim
        do k=1,dim
          c(i,j)=c(i,j)+a(i,k)*b(k)*a(j,k)
        enddo
       enddo
      enddo
      return
      end


       subroutine calcinv (matrix,invmatrix)
       implicit none 
       integer nmomx
       parameter (nmomx=510)

       real*8 matrix(nmomx*nmomx),invmatrix(nmomx*nmomx)
         integer nsymmx
         parameter (nsymmx = 8)
         integer naux
        parameter (naux=2000)
        real*8 aux(naux)


       integer nsym,nbfpsy(nsymmx),nmopsy(nsymmx)
       character*4 labels(nmomx)

       common /check/ nsym,nbfpsy,nmopsy, labels


        integer i,off,ierr,ipiv(nmomx)

       off=1

       do i=1,nsym

        call dcopy_wr(nmopsy(i)*nmopsy(i),matrix(off),1,
     .   invmatrix(off),1)

       call dgetrf_wr(nmopsy(i),nmopsy(i),invmatrix(off),nmopsy(i),
     .       ipiv, ierr)
        if (ierr.ne.0)
     .  call bummer('dgetrf, ierr=',ierr,2)
          ierr=0
        call dgetri_wr(nmopsy(i),invmatrix(off),nmopsy(i),ipiv,aux,
     .    naux,ierr)
        if (ierr.ne.0)
     .  call bummer('dgetri, ierr=',ierr,2)

        off=off+nmopsy(i)*nmopsy(i)
       enddo

         call prblks('stdmo',matrix,nsym,nmopsy,
     .    nmopsy,'no  ','ao  ',3,6)
         call prblks('invstdmo',invmatrix,nsym,nmopsy,
     .    nmopsy,'no  ','ao  ',3,6)
       return
       end

