!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      program sumcivec

      IMPLICIT NONE
      include "../colib/getlcl.h"

c
c   This program allows to copy all valid ci vectors (as defined in the
c   ci info file civout) from a given ci (or sigma) vector file.
c   Namelist type input
c
c
c    input file:   cutciin
c    listing file: cutcils
c
c   vectorin : name of the input vector file
c   vectorout: name of the output vector file
c   infoin   : name of the input vector info file
c   infoout  : name of the output vector info file
c   lvlprt   : printlevel (>=0)
c   delsrc   : if set to 1 the input vector and input vector info
c              files are deleted after the copying operation
c
c
c*********************************************************************
c
c  allocate space and call the driver routine.
c
c  ifirst = first usable space in the the real*8 work array a(*).
c  lcore  = length of workspace array in working precision.
c  mem1   = additional machine-dependent memory allocation variable.
c  a(*)   = workspace array. a(ifirst : ifirst+lcore-1) is useable.
c
      integer ifirst, lcore
c
c     # mem1 and a(*) should be declared below within mdc blocks.
c
c     # local...
c     # lcored = default value for lcore.
c     # ierr   = error return code.
c
      integer lcored, ierr
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
c     # sets up any communications necessary for parallel execution,
c     # and does nothing in the sequential case.
c

c
*@ifdef pointer
*c
*c     # pointer syntax supported
*c     # mem1 is a pointer to the beginning of a(*).
*c
*      parameter ( lcored = 2 500 000 )
**
*@ifdef bit64
**
*@ifdef declmalloc
*      integer*8 malloc,mallocarg
*      external malloc
*@ifdef declmem 
*     integer*8  mem1
*@endif
*@endif
**
*@else
**
*@ifdef decmalloc
*      integer*4 malloc,mallocarg
*      external malloc
*@ifdef declmem
*      integer  mem1
*@endif
*@endif
*@endif 
*      real*8 a(2)
*      pointer (mem1, a)
*c
*      call getlcl( lcored, lcore, ierr )
*      if ( ierr .ne. 0 ) then
*         call bummer('argos: from getlcl: ierr=',ierr,faterr)
*      endif
*c
*@ifdef unicos
*c     # unicos specific memory allocation
*c
*      call hpalloc( mem1, lcore, ierr, 1 )
*      ifirst = 1
*@elif defined sp4  
*      mallocarg=8*lcore
*      mem1 = malloc( %val(mallocarg) )
*@else
*      mem1 = malloc( 8*lcore)
*@endif
*      if ( mem1 .eq. 0 ) then
*         call bummer('argos: from malloc: mem1=',mem1,faterr)
*      endif
*      ifirst =  1
*@elif defined f90
c
c     # use standard f90 memory allocation.
c
      integer mem1
      parameter ( lcored = 2 500 000 )
      real*8, allocatable :: a(:)
c
      call getlcl( lcored, lcore, ierr )
      if ( ierr .ne. 0 ) then
         call bummer('argos: from getlcl: ierr=',ierr,faterr)
      endif
c
      allocate( a(lcore) )
      ifirst = 1
      mem1   = 0
*@elif defined usefalloc
*c
*c     # generic  memory allocation using () interface.
*c     # see also $COLUMBUS/source/colib/.c
*c
*      integer mem1, offset
*      parameter ( lcored = 2 500 000 )
*      real*8 a(1)
*c
*      call getlcl( lcored, lcore, ierr )
*      if ( ierr .ne. 0 ) then
*         call bummer('argos: from getlcl: ierr=',ierr,faterr)
*      endif
*c
*c     # on return from (), a(offset+1:offset+lcore) is useable.
*      call falloc( lcore, 8, 0, a, mem1, offset )
*      if ( mem1 .eq. 0 ) then
*         call bummer('argos: from : mem1=',mem1,faterr)
*      endif
*      ifirst = offset + 1
*@else
*c
*c     # standard fortran workspace allocation.
*c     # mem1 is not used.  use blank common to reduce executable size.
*c
*      integer mem1
*      parameter ( lcored = 2 500 000 )
*      real*8    a
*      common // a( lcored )
*c
*      call getlcl( lcored, lcore, ierr )
*      if ( ierr .ne. 0 ) then
*         call bummer('argos: from getlcl: ierr=',ierr,faterr)
*      elseif ( lcore .gt. lcored ) then
*c        # make sure lcore is consistent.  this check should be
*c        # redundant with logic within getlcl(), but is included
*c        # here just to be safe.
*         call bummer('argos: (lcore-lcored)=',
*     &    (lcore-lcored), faterr )
*      endif
*c
*      mem1   = 0
*      ifirst = 1
*@endif

c
c     # lcore, mem1, and ifirst should now be defined.
c     # the values of lcore, mem1, and ifirst are passed to
c     # driver() to be printed.
c     # since these arguments are passed by expression, they
c     # must not be modified within driver().
c
c
      call driver( a(ifirst), (lcore), (mem1), (ifirst) )
c
c
c     # return from driver() implies successful execution.
c
c     # message-passing cleanup: stub if not in parallel
c
      call bummer('normal termination',0,3)
      stop 'end of sumcivec'
      end



      subroutine driver( core, lcore, mem1, ifirst)
c
      implicit none 
c
c  ##  parameter & common block section
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer maxvec
      parameter (maxvec=16)
c
      integer   nfilmx
      parameter(nfilmx=55)
      integer         nunits
      common /cfiles/ nunits(nfilmx)
      character*60    fname
      common /cfname/ fname(nfilmx)
      integer nslist,nlist,civfl,civfln
      equivalence (nunits(1),nlist)
      equivalence (nunits(3),nslist)
      equivalence(nunits(7),civfl)
      equivalence(nunits(8),civfln)
c
      integer delsrc,lvlprt,vector1,vector2
      character*60 vectorin,vectorout,infoin,infoout
      common /indata/  delsrc,  lvlprt,  vectorin,  vectorout,
     &                 infoin,  infoout, vector1,   vector2
c
c  ##  integer section
c
      integer bperacc
      integer cpt(3)
      integer i,inroot1,inroot2,iroot,isweep,ifirst
      integer lcore
      integer mem1
      integer ncsf,nel1st,nel,nroot
      integer outroot
      integer recpervec,roots(maxvec)
      integer vecspace
c
c  ##  real*8 sectionc
c
      real*8 core(*)
c
c  ##  external section
c
      integer atebyt
      external atebyt
c
c------------------------------------------------------------------
c
c     open the listing files.
c
      open( unit = nlist, file = fname(1), status = 'unknown')
      open( unit = nslist, file= fname(3), status = 'unknown')

      call who2c( 'sumcivec', nlist )
      call datain
c     # open the necessary info containing files
c
      call cpcivout(nroot,roots,ncsf,delsrc)

c
      call ibummr( nlist )
c
*@ifdef direct
*      call headwr(nlist,'sumcivec','1.0     ')
*@else
      call headwr(nlist,'sumcivec','1.0      ')
*@endif
c

      call openda(civfl,fname(7),8*4096,'keep','random')
      call openda(civfln,fname(8),8*4096,'keep','random')


      vecspace=lcore/2
      if (vecspace.lt.8*4096)
     .   call bummer('not enough memory',lcore,faterr)
      bperacc= vecspace/(8*4096)
      recpervec = (ncsf-1)/(8*4096) +1

      write(nlist,*) 'Core space of', lcore,' words sufficient for '
     . ,bperacc,' records'
      write(nlist,*) ncsf,' configurations = ', recpervec,
     . ' records per vector'
      write(nlist,*) 'Writing ',1,' vectors to new vector file'
c
c    memory allocation:
c    1:vec1,2:vec2,
      cpt(1)=1
      cpt(2)=cpt(1)+atebyt(vecspace)
      cpt(3)=cpt(2)+atebyt(vecspace)
c
c  # check the validity of the CI vector
      if ((vector1.gt.nroot).or.(vector1.le.0))
     & call bummer ('wrong vector1 value',vector1,faterr)
      if ((vector2.gt.nroot).or.(vector2.le.0))
     & call bummer ('wrong vector2 value',vector2,faterr)
c
      outroot=1
      inroot1=vector1
      inroot2=vector2
c
      nel1st=1
      write(nlist,501) inroot1,inroot2
501   format(/1x,'Summing vectors: ',i2,' + ',i2,
     . ' and putting to possition 1 of the output file')
        do isweep = 1, (recpervec-1)/bperacc
        nel=bperacc*8*4096
        write(nlist,100) inroot1,inroot2,isweep,nel1st,nel
100     format('Processing: root1(in)=',i3,'  root1(in)=',i3,
     &   '  isweep=',i3,' nel1st=',i10,2x,'nel=',i10)
        call getvec(civfl,8*4096,inroot1,ncsf,core(cpt(1)),nel1st,nel)
         if (lvlprt.ge.1) then
          write(nlist,*)'CI vector ',inroot1
          call writex(core(cpt(1)),nel,nlist)
         endif
        call getvec(civfl,8*4096,inroot2,ncsf,core(cpt(2)),nel1st,nel)
         if (lvlprt.ge.1) then
          write(nlist,*)'CI vector ',inroot2
          call writex(core(cpt(2)),nel,nlist)
         endif
c   sum ci1 + ci2
        call daxpy_wr(nel,1.0d+00,core(cpt(1)),1,core(cpt(2)),1)
         if (lvlprt.ge.1) then
          write(nlist,*)'SUM of vectors:',inroot1,' + ',inroot2
          call writex(core(cpt(2)),nel,nlist)
         endif
c  write out the result
        call putvec(civfln,8*4096,outroot,ncsf,core(2),nel1st,nel,0)
        nel1st=nel1st+nel
        enddo ! end of: do isweep = 1, (recpervec-1)/bperacc
      nel=ncsf-nel1st+1
      write(nlist,101) inroot1,inroot2,nel1st,nel
101   format('final sweep: root(in)=',i3,2x,'root(in)=',i3,2x,
     . 'nel1st=',i10,2x,'nel=',i10)
      call getvec(civfl,8*4096,inroot1,ncsf,core(cpt(1)),nel1st,nel)
       if (lvlprt.ge.1) then
        write(nlist,*)'CI vector 1',inroot1
        call writex(core(cpt(1)),nel,nlist)
       endif
      call getvec(civfl,8*4096,inroot2,ncsf,core(cpt(2)),nel1st,nel)
       if (lvlprt.ge.1) then
        write(nlist,*)'CI vector',inroot2
        call writex(core(cpt(2)),nel,nlist)
       endif
c   sum ci1 + ci2
      call daxpy_wr(nel,1.0d+00,core(cpt(1)),1,core(cpt(2)),1)
c  write out the result
       if (lvlprt.ge.1) then
        write(nlist,*)'SUM of vectors:',inroot1,' + ',inroot2
        call writex(core(cpt(2)),nel,nlist)
       endif
      call putvec(civfln,8*4096,outroot,ncsf,core(cpt(2)),nel1st,nel,0)
c
      if (delsrc.eq.1) then
       call closda(civfl,'delete',1)
      else
       call closda(civfl,'nodel',1)
      endif
      call closda(civfln,'nodel',1)
      call bummer('normal termination',0,3)
      return
      end



      block data xx
      implicit logical(a-z)
c da file record parameters
C     Common variables
C
      INTEGER             vecrln,indxln, ssym1,ssym2
C
      COMMON / INF4   /   vecrln,indxln, ssym1,ssym2
C

      integer   nfilmx
      parameter(nfilmx=55)
      integer         nunits
      common /cfiles/ nunits(nfilmx)
      character*60    fname
      common /cfname/ fname(nfilmx)

c------------------------------------------------------------------
c     # nlist = standard listing file.
      integer      nlist
      equivalence (nlist,nunits(1))
      data nlist/6/, fname(1)/'sumcils'/
c------------------------------------------------------------------
c     # tape5 = user input file.
      integer      tape5
      equivalence (tape5,nunits(2))
      data tape5/5/, fname(2)/'sumciin'/
c------------------------------------------------------------------
c     # nslist = summary listing file.
      integer      nslist
      equivalence (nslist,nunits(3))
      data nslist/7/, fname(3)/'sumcism'/
c------------------------------------------------------------------
c     # filev1 = input vector file.
      integer      filev1
      equivalence (filev1,nunits(7))
      data filev1/10/
c------------------------------------------------------------------
c     # filev2 = output vector file.
      integer      filev2
      equivalence (filev2,nunits(8))
      data filev2/11/
c------------------------------------------------------------------
c     # civout1   input info file
      integer     civout1
      equivalence (civout1,nunits(16))
      data civout1/20/
c------------------------------------------------------------------
c     # civout2   output info file
      integer     civout2
      equivalence (civout2,nunits(17))
      data civout2/21/
c------------------------------------------------------------------
c include(data.common)
      integer maxsym
      parameter (maxsym=8)
      integer symtab,sym12,sym21,nbas,lmda,lmdb,nmbpr,strtbw,strtbx
      integer blst1e,nmsym,mnbas,cnfx,cnfw,scrlen
      common/data/symtab(maxsym,maxsym),sym12(maxsym),sym21(maxsym),
     + nbas(maxsym),lmda(maxsym,maxsym),lmdb(maxsym,maxsym),
     + nmbpr(maxsym),strtbw(maxsym,maxsym),strtbx(maxsym,maxsym),
     + blst1e(maxsym),nmsym,mnbas,cnfx(maxsym),cnfw(maxsym),scrlen
c
      data symtab/
     & 1,2,3,4,5,6,7,8,
     & 2,1,4,3,6,5,8,7,
     & 3,4,1,2,7,8,5,6,
     & 4,3,2,1,8,7,6,5,
     & 5,6,7,8,1,2,3,4,
     & 6,5,8,7,2,1,4,3,
     & 7,8,5,6,3,4,1,2,
     & 8,7,6,5,4,3,2,1/
c
      data sym12/1,2,3,4,5,6,7,8/

c     # da record lengths
c     # record lengths for ci created direct acc. files
c     #
c     # indxln : da record length for index file
c     # vecrln : da record length for vector files
c
c     # should be tuned for your machine....
c
*@if defined ibm && defined vector
*c      # vax maximum da record length is 4095
*       data indxln/4095/, vecrln/4095/
*@else
       data indxln/4096/, vecrln/32768/
*@endif

       end


      subroutine datain
c
c  read user input.
c
      implicit logical(a-z)
c
      integer   nfilmx
      parameter(nfilmx=55)
      integer         nunits
      common /cfiles/ nunits(nfilmx)
      integer      nlist
      equivalence (nlist,nunits(1))
      integer      tape5
      equivalence (tape5,nunits(2))
c
      character*60    fname
      common /cfname/ fname(nfilmx)
c
      character*60 fnamex
      equivalence (fname(nfilmx),fnamex)
c

c
      integer delsrc,lvlprt,vector1,vector2
      character*60 vectorin,vectorout,infoin,infoout
      common /indata/  delsrc,  lvlprt,  vectorin,  vectorout,
     &                 infoin,  infoout, vector1,   vector2
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)

       integer ierr,i,j
c
*@ifndef nonamelist
      namelist /input/ lvlprt,vectorin,vectorout,infoin,infoout
     .                 ,delsrc,vector1,vector2
*@endif
c
      ierr = 0
c
c
      open( unit = tape5, file = fname(2), status = 'unknown')
c
c     read and echo standard input until eof is reached.
c
      write(nlist,6010)
6010  format(' echo of the input for program transci:')
      call echoin( tape5, nlist, ierr )
      if ( ierr .ne. 0 ) then
         call bummer(' datain: from echoin, ierr=',ierr,faterr)
      endif
c
      rewind tape5

c defaults
       lvlprt=0
       vectorin='x'
       vectorout='x'
       infoin='x'
       infoout='x'
       delsrc =0
c
c     # read namelist input and keep processing if eof is reached.
c
c
*@ifdef nonamelist
*@elif defined  nml_required
*      read( tape5, nml=input, end=3 )
*@else
      read( tape5, input, end=3 )
*@endif
c
3     continue
c
      if ( vectorin.eq.'x') then
       call bummer('enter filename for input vector file (vectorin)',
     .               0,0)
      ierr=ierr+1
      endif

      if ( vectorout.eq.'x') then
       call bummer('enter filename for output vector file (vectorout)',
     .               0,0)
      ierr=ierr+1
      endif

      if ( infoin.eq.'x') then
       call bummer('enter filename for input info file (infoin)',
     .               0,0)
      ierr=ierr+1
      endif

      if ( infoout.eq.'x') then
       call bummer('enter filename for output info file (vectorout)',
     .               0,0)
      ierr=ierr+1
      endif

      if (ierr.ne.0)
     . call bummer('input file errors, exiting',ierr,2)

      fname(7) = vectorin
      fname(8) = vectorout
      fname(16)= infoin
      fname(17)= infoout



      write(nlist,*)'units and filenames:'
c
      do 110 i = 1, (nfilmx - 1)
         if ( nunits(i) .eq. 0 ) goto 110
111      continue
         write(nlist,6050) i, nunits(i), fname(i)
110   continue
6050  format(1x,i4,': (',i2,')',4x,a)
6051  format(1x,i4,': (',i2,')',4x,a,t35,a)
c
      write(nlist,6020)
      close(unit=tape5)
c
      return
6020  format(1x,72('-'))
      end



      subroutine cpcivout (nroot,roots,ncsf,delsrc)

c     copies and modifies the
c     vector info file written by ciudg
c
      implicit none 
c
c     output:
c       nroot   = number of valid roots on civout
c       roots(i)= contains the old position of ith valid vector
c                 on civout
c----------------------------------------------------
C
C  ##  parameter & common block section
C
      INTEGER             MXNSEG
      PARAMETER           (MXNSEG = 20)
      INTEGER             CURVER
      PARAMETER           (CURVER = 1)
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
      INTEGER             MXTITL
      PARAMETER           (MXTITL = 10)
      INTEGER             MXINFO
      PARAMETER           (MXINFO = 10)
      INTEGER             MXNRGY
      PARAMETER           (MXNRGY = 10)
      integer maxvec
      parameter (maxvec=16)
C
      integer nunits,nfilmx
      parameter(nfilmx=55)
      character*60 fname
      common/cfname/fname(nfilmx)
      common/cfiles/nunits(nfilmx)
      integer nlist, civoutin,civoutout
      equivalence (nunits(1),nlist)
      equivalence (nunits(16),civoutin)
      equivalence (nunits(17),civoutout)
c
c ## arguments

       integer nroot,ncsf,delsrc
       integer roots(maxvec)



c  ##  integer section
c
      integer cist, civcst
      integer i, indxst, iseg , ietype(mxnrgy), info(mxinfo),
     &        idum1, idum2
      integer lenci
      integer mxread, mxsegl
      integer ninfo, nenrgy
      integer nsegw, ntitle
      integer segci, segel
      integer temp
      integer versn,blksiz,iroot
c
c  ##  real*8 section
c
      REAL*8              energy(mxnrgy)
c
c  ##  character section
c
      CHARACTER*80        TITLE(MXTITL)
c
c-------------------------------------------------------------

      open( unit = civoutin, file = fname(16), status = 'unknown' ,
     +       form = 'unformatted' )
      rewind(civoutin)
c
      open( unit = civoutout, file = fname(17), status = 'unknown' ,
     +       form = 'unformatted' )
c

      call izero(maxvec,roots,1)
c
      nroot=0
      iroot=0
 90   read (civoutin,end=99) versn,blksiz,ncsf
      iroot=iroot+1
      if (iroot.eq.1)write(civoutout) versn,blksiz,ncsf
      if ( versn .ne. curver ) then
         call bummer('rdvhd:  wrong version number', versn, faterr)
      endif
c
c     # general information
      read (civoutin) ninfo, nenrgy, ntitle
      if(iroot.eq.1)write (civoutout) ninfo, nenrgy, ntitle
c
c     info(1) : maximum overlap with reference vector # info(1)
c     info(2) : corresponding ci vector is record # info(2)
c     info(3) : method CI(0),AQCC(3),AQCC-LRT(30)
c     info(4) : if set to one this is the last record of civout
c     info(5) : overlap of reference vector and ci vector in percent
c
      mxread = min (ninfo, mxinfo)
      read (civoutin) (info(i), i=1,mxread)

      write(nlist,*) 'copying information from info file: '
      write(nlist,*) 'old CI record: ',info(2),' new CI record:',
     .                nroot+1
      nroot=nroot+1
      roots(nroot)=info(2)
      info(2)=1
      temp = info(4)
      info(4)=1
      info(4)=temp
      if (iroot.eq.1)write (civoutout) (info(i), i=1,mxread)

c     # read titles:
      mxread = min (ntitle,mxtitl)
      read (civoutin) (title(i), i=1,mxread)
      if (iroot.eq.1)write(civoutout) (title(i), i=1,mxread)
c
c     # read energies
      mxread = min(nenrgy,mxnrgy)
      read(civoutin) (ietype(i),i=1,mxread),(energy(i),i=1,mxread)
      if (iroot.eq.1)write(civoutout)
     & (ietype(i),i=1,mxread),(energy(i),i=1,mxread)

      write(nlist,15)
15    format(20('==='))
      write(nlist,*) 'ci vector:',info(2),' itype=',info(3),
     .       ' reference vector',info(1)
      write(nlist,*) 'titles:'
      do i=1, min(ntitle,mxtitl)
       write(nlist,*) title(i)
      enddo
      write(nlist,15)
      read(civoutin)
      read(civoutin)
      read(civoutin)
       if (iroot.eq.1) then
        write(civoutout)
        write(civoutout)
        write(civoutout)
       endif
      if (info(4).eq.0) goto 90
 99   continue
      write(nlist,500) (roots(i),i=1,nroot)
      write(nlist,501)
500   format(1x,40('==')/2x,'root mapping:',16i4/)
501   format(1x,40('=='))
      if (delsrc.eq.1) then
       close(civoutin,status='delete')
      else
      close(civoutin)
      endif
      close(civoutout)
      return
      end
c deck writex
      subroutine writex( a, n,nlist )
c writex:
c this routine writes out a formatted real*8 vector
c a : the vector to be listed
c n : the number of elements to list
c
c  10-dec-90 iwritx() entry added for unit initialization. -rls
c
      real*8 a(*)
      integer n,m, iunit
c
      integer i,j, nlist
c
      m=1
      do j =1, (n/100)
      write(nlist,6011)m,m+99
      write(nlist,6010) (a(i),i=m,m+99)
      m=m+100
      enddo
      write(nlist,6011)m,n
      write(nlist,6010) (a(i),i=m,n)
      return
c
      entry iwritx( iunit )
      nlist = iunit
c
      return
6010  format(1x,10f10.6)
6011  format(1x, 4('----------'),'elements ',i4,' to ', i4,
     .           4('----------'))
      end
