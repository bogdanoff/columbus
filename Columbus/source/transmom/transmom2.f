!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
c**************************************************
c**************************************************
c
c   TRANSITION DIPOLE MOMENT CALCULATION
c     (based on MCSCF wavefunction)
c
c**************************************************
c**************************************************
c
      subroutine driver( core, lcore, mem1, ifirst )
c
      implicit integer(a-z)
c
      real*8 core(lcore)
c
      common/cfiles/iunits(25)
      equivalence (iunits(1),nlist)
      equivalence (iunits(2),nin)
      equivalence (iunits(4),ndrt1)
      equivalence (iunits(5),ndrt2)
      equivalence (iunits(6),ndft)
      equivalence (iunits(7),nft)
      equivalence (iunits(9),trsci)
      equivalence (iunits(10),nsfile)
      equivalence (iunits(11),ndiagf)
      equivalence (iunits(12),nrestp)
      equivalence (iunits(19),mos)
      equivalence (iunits(21),aoints)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nbpsy(8),nsb(8),nnsb(8),n2sb(8)
      equivalence (nxy(1,1),nbpsy(1))
      equivalence (nxy(1,2),nsb(1))
      equivalence (nxy(1,3),nnsb(1))
      equivalence (nxy(1,4),n2sb(1))
      equivalence (nxtot(1),nbft)
      equivalence (nxtot(2),nntb)
      equivalence (nxtot(3),n2tb)
      integer nmpsy(8),nsm(8),nnsm(8),n2sm(8)
      equivalence (nxy(1,5),nmpsy(1))
      equivalence (nxy(1,6),nsm(1))
      equivalence (nxy(1,7),nnsm(1))
      equivalence (nxy(1,8),n2sm(1))
      equivalence (nxtot(4),nmot)
      equivalence (nxtot(5),nntm)
      equivalence (nxtot(6),n2tm)
      integer ndpsy(8),nsd(8),nnsd(8),n2sd(8)
      equivalence (nxy(1,9),ndpsy(1))
      equivalence (nxy(1,10),nsd(1))
      equivalence (nxy(1,11),nnsd(1))
      equivalence (nxy(1,12),n2sd(1))
      equivalence (nxtot(7),ndot)
      equivalence (nxtot(8),nntd)
      equivalence (nxtot(9),n2td)
      integer napsy(8),nsa(8),nnsa(8),n2sa(8)
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxy(1,14),nsa(1))
      equivalence (nxy(1,15),nnsa(1))
      equivalence (nxy(1,16),n2sa(1))
      equivalence (nxtot(10),nact)
      equivalence (nxtot(11),nnta)
      equivalence (nxtot(12),n2ta)
      integer nvpsy(8),nsv(8),nnsv(8),n2sv(8)
      equivalence (nxy(1,17),nvpsy(1))
      equivalence (nxy(1,18),nsv(1))
      equivalence (nxy(1,19),nnsv(1))
      equivalence (nxy(1,20),n2sv(1))
      equivalence (nxtot(13),nvrt)
      equivalence (nxtot(14),nntv)
      equivalence (nxtot(15),n2tv)
      integer tsymdd(8),tsmdd2(8),tsymad(8),tsymaa(8)
      integer tsymvd(8),tsymva(8),tsymvv(8),tsmvv2(8)
      equivalence (nxy(1,21),tsymdd(1))
      equivalence (nxy(1,22),tsmdd2(1))
      equivalence (nxy(1,23),tsymad(1))
      equivalence (nxy(1,24),tsymaa(1))
      equivalence (nxy(1,25),tsymvd(1))
      equivalence (nxy(1,26),tsymva(1))
      equivalence (nxy(1,27),tsymvv(1))
      equivalence (nxy(1,28),tsmvv2(1))
      equivalence (tsymad(1),nadt)
      equivalence (tsymvd(1),nvdt)
      equivalence (tsymva(1),nvat)
      integer nsad(8),nsvd(8),nsva(8)
      equivalence (nxy(1,29),nsad(1))
      equivalence (nxy(1,30),nsvd(1))
      equivalence (nxy(1,31),nsva(1))
      integer nsbm(8)
      equivalence (nxy(1,32),nsbm(1))
      equivalence (nxtot(16),nbmt)
      equivalence (nxtot(17),ndimd)
      equivalence (nxtot(18),ndima)
      equivalence (nxtot(19),ndimv)
      integer irb1(8),irb2(8)
      equivalence (nxy(1,33),irb1(1))
      equivalence (nxy(1,34),irb2(1))
      integer irm1(8),irm2(8)
      equivalence (nxy(1,35),irm1(1))
      equivalence (nxy(1,36),irm2(1))
      integer ird1(8),ird2(8)
      equivalence (nxy(1,37),ird1(1))
      equivalence (nxy(1,38),ird2(1))
      integer ira1(8),ira2(8)
      equivalence (nxy(1,39),ira1(1))
      equivalence (nxy(1,40),ira2(1))
      integer irv1(8),irv2(8)
      equivalence (nxy(1,41),irv1(1))
      equivalence (nxy(1,42),irv2(1))
c
      parameter(nmotx=1023)
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx),symb(nmotx),symm(nmotx),symx(nmotx)
      integer invx(nmotx),orbidx(nmotx),orbtyp(nmotx),fcimsk(nmotx)
      equivalence (iorbx(1,1),nndx(1))
      equivalence (iorbx(1,2),symb(1))
      equivalence (iorbx(1,3),symm(1))
      equivalence (iorbx(1,4),symx(1))
      equivalence (iorbx(1,5),invx(1))
      equivalence (iorbx(1,6),orbidx(1))
      equivalence (iorbx(1,7),orbtyp(1))
      equivalence (iorbx(1,8),fcimsk(1))
c
      common/caddb/addpt(24),numint(12),bukpt(12),intoff(12),szh(15)
      equivalence (bukpt(12),nbuk)
c
c  lenbft=length of the formula tape buffers.
      data lenbft/2047/
c
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
c
      parameter(lcinf=6,lctol=4)
      real*8 coninf
      common/cconi/coninf(lcinf)
      real*8 emc,demc,wnorm,knorm,apxde,repnuc
      equivalence (coninf(1),emc)
      equivalence (coninf(2),repnuc)
      equivalence (coninf(3),demc)
      equivalence (coninf(4),wnorm)
      equivalence (coninf(5),knorm)
      equivalence (coninf(6),apxde)
c
      integer occl
      common /ocupy/ occl(nmotx)
c
      integer  atebyt,  forbyt
      external atebyt,  forbyt
c
c  local variables:
c
      logical qcoupl,qind(2),qconvg
      character*16 convrg
      character*80 title(20)
      integer cpt(50)
      real*8 ecore0,emcold
      integer mapmm(nmotx)
      character*60 fname
c
c  bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      real*8    zero,    two
      parameter(zero=0d0,two=2d0)
cmd
c
      integer symoff
      common /offset/ symoff(36)
c
      integer ssym1,ssym2,d1dim
      common /ssym/ ssym1,ssym2,d1dim
      integer step(0:nmotx)
c
c     variables for sif
      integer ntitxx, ninfao, nenrgy, nmap,idummy,infoao(10)
      integer    ntitmx,itypea,itypeb
      integer mapin(nmotx),symb2(nmotx),kntin(36)
      parameter (ntitmx=30)
      character*4    slabel(8)
      character*8    aolab(nmotx)
      real*8 score,tcore,vcore
c
      integer   nengmx
      parameter(nengmx=20)
      integer ietype(nengmx)
      real*8 energy(nengmx)

      integer btypes(0:2)
c
      real*8   sifsce
      external sifsce
c
      logical first,idebug
      character*60 fdft,foft

c
c    variables for restart header read
      integer ibuf,fbuf(8)
      character*80 chbuf(50)
      logical lbuf
c
      parameter (maxnst=8)
      parameter (mxavst = 50)
      parameter (maxstat = maxnst*mxavst)
      real*8 wavst,heig
      common /cidim/ ncsf_f(maxstat),navst(maxnst),nst,
     &  heig(maxnst,mxavst),wavst(maxnst,mxavst)
c
      real*8 trans(3),del_au,del_ev,diplen
      real*8 erg,freq,freq2,omega,f,f2,fact,b,a,pi
c
      real*8 one,three,eight,small,planck,clight,bohr,
     & echarg,ev,debye,cm_1
      parameter ( one=1.0D+00,
     *           three=3.0D+00, eight=8.0D+00, small=1.0D-08,
     *           planck=6.626176D-27, clight=2.9979925D+10,
     *           bohr=5.291771D-09, echarg=4.803242D-10,
     *           ev=27.212D+00, debye=2.541766D+00,
     &           cm_1=219474.7d+00)

cmd
c
c
c    open user input and output files
c
      fname = 'transin'
      call trnfln( 1, fname )
      open(unit=nin,file=fname,status='unknown')
c
      fname = 'transls'
      call trnfln( 1, fname )
      open(unit=nlist,file=fname,status='unknown')
c
c     write out title to user output
c
      write(nlist,9040)
9040  format(/,/,/,10x,
     & "TRANSITION DIPOLE MOMENT CALCULATION PROGRAM",/)
      write(nlist,'(15x,"written by: Michal Dallos")')
      write(nlist,'(15x,"Institute for Theoretical Chemistry")')
      write(nlist,'(15x,"Vienna, Austria",/,/)')
      call headwr(nlist,'TRANSMOM','5.5   ')
      call ibummr(nlist)
      call timer(' ',0,event1,nlist)
c
      write(nlist,6020) lcore, mem1, ifirst
6020  format(/' workspace allocation information:',
     &  ' lcore=',i10,' mem1=',i10,' ifirst=',i10)

c
c  read header information from the integral file:
c   1:buffer
c
      cpt(1)=1
c
c     open the integral file in sif format
c
      fname = 'aoints'
      call trnfln( 1, fname )
      open(unit=aoints,file=fname,status='old',form='unformatted')
c
c     read the integral file header
c
      call sifrh1(aoints,ntitxx,nsym,nbft,ninfao,nenrgy,nmap,ierr)
c
      if ( ierr .ne. 0 ) then
         call bummer('trmain: from sifrh1, ierr=',ierr,faterr)
      elseif ( ntitxx .gt. ntitmx ) then
         call bummer('trmain: from sifrh1, ntitao=',ntitxx,faterr)
      elseif ( nbft .gt. nmotx ) then
         call bummer('trmain: from sifrh1, nbft=',nbft,faterr)
      elseif ( ninfao .gt. 10 ) then
         call bummer('trmain: from sifrh1, ninfao=',ninfao,faterr)
      elseif ( nenrgy .gt. nengmx ) then
         call bummer('trmain: from sifrh1, nenrgy=',nenrgy,faterr)
      endif
c
c
c     # ignore any ao map vectors.
      nmap=0
c
c     # make space for the ao titles.
      ittl0 = 0
      ittl0 = min( ittl0, ntitmx-ntitxx )
c
      call sifrh2(
     & aoints, ntitxx, nsym,   nbft,
     & ninfao, nenrgy, nmap,   title(ittl0+1),
     & nbpsy,  slabel, infoao, aolab,
     & ietype, energy, idummy, idummy,
     & ierr   )
c
      if ( ierr .ne. 0 ) then
         call bummer('trmain: from sifrh2, ierr=',ierr,faterr)
      endif
c
      repnuc = sifsce( nenrgy, energy, ietype )
c
      ntitle=ntitxx
      write(nlist,6120)(title(i),i=1,ntitle)
      write(nlist,6130)repnuc,nbft,nsym,(i,nbpsy(i),i=1,nsym)
6120  format(/,/' INTEGRAL FILE HEADER INFORMATION:'/(1x,a))
6130  format(' repulsion energy:',1pe25.15,' hartree.'/
     &  ' total number of basis functions:',i4/
     &  ' total number of irreps:',i2/' (isym:nbpsy)',8(i3,':',i3))
c
c    read user input and close user input
c
      call readin(idebug)
      close(unit=nin)
c
c
c    open mcscf restart file
c
      fname = 'restart'
      call trnfln( 1, fname )
      open(unit=nrestp,file=fname,status='unknown',
     &  form='unformatted')
c
c    read header from mcscf restart file
c
      read(nrestp)
     & ibuf,ibuf,ibuf,ibuf,ibuf,nst,
     & (ncsf_f(i),i=1,nst),lbuf,ntitle2,
     & fbuf,fbuf,(chbuf(i),i=1,ntitle2),
     &  (navst(i),i=1,nst),maxnavst,
     &  ((wavst(i,j),i=1,nst),j=1,maxnavst),
     &  ((heig(i,j),i=1,nst),j=1,maxnavst)
c
c    #   check the No. of different states do not exceedes maximum
      if (nst .gt. maxnst) call bummer(
     & 'Number of DRT exceedes maximum = ',maxnst,faterr)
c
c    #   check the No. of states in one DRT do not exceedes maximum
      if (maxnavst .gt. mxavst) call bummer(
     & 'Number of states in one DRT exceedes maximum = ',maxnavst,
     & faterr)
c
c    write the header informations to the user output
c
      write(nlist,'(//,7x,"CI INFORMATIONS",/)')
      write(nlist,'(3x,"Number of different sets of CI vectors:",i3)')
     &  nst
      write(nlist,'(4x,"(defined through different DRT)"))')
      write(nlist,9030)
9030  format(/,3x,"CI set",3x,"No of CI vectors",3x,
     & "No of csf",/)
      do ist=1,nst
      write(nlist,'(4x,i2,10x,i3,13x,i6)')ist,navst(ist),ncsf_f(ist)
      enddo
c
c    write the available state energies to the user output
c
      icount = 1
      do ist = 1,nst
      write(nlist,'(/,3x,"Energies from DRT No.:",i3,/)')ist
         do imd=1,navst(ist)
         write(nlist,'(3x,i3,3x,f15.8)')imd,heig(ist,imd)
         enddo
      enddo
c
c   read the input for CI vectors
c
      fname = 'transci'
      call trnfln( 1, fname )
      open(unit=trsci,file=fname,status='unknown',
     &  form='formatted')
c
      read(trsci,*)iciset1
      if (iciset1.gt.maxnst) call bummer(
     & 'Set number exceeds the set dimenstion =',maxnst,faterr)
      if (navst(iciset1).le.0)
     & call bummer('not existing CI vector set No ',ciset1,faterr)
      read(trsci,*)iorder1
      if ((iorder1.gt.navst(iciset1)).or.(iorder1.le.0))
     & call bummer('not existing CI vector No ',iorder1,faterr)
c
      read(trsci,*)iciset2
      if (iciset2.gt.maxnst) call bummer(
     & 'Set number exceeds the set dimenstion =',maxnst,faterr)
      if (navst(iciset2).le.0)
     & call bummer('not existing CI vector set No ',ciset2,faterr)
      read(trsci,*)iorder2
      if ((iorder2.gt.navst(iciset2)).or.(iorder2.le.0))
     & call bummer('not existing CI vector No ',iorder2,faterr)
c
      close(trsci)
c
c    test for identical ci vectorts
c
cdd   if ((iciset1.eq.iciset2).and.(iorder1.eq.iorder2)) then
c     call bummer(
c    & 'Transition moment between identical states has no sense!',
c    & 0,faterr)
cdd   endif
c
c    test the existence of the needed DRT files and open them
c
      if ((iciset1.eq.1).and.(nst.eq.1)) then
         fname = 'mcdrtfl'
      elseif(iciset1.ge.10) then
         write(fname,'(a8,i2)')'mcdrtfl.',iciset1
      else
         write(fname,'(a8,i1)')'mcdrtfl.',iciset1
      endif
      inquire(file=fname,exist= lbuf)
      if (.not.lbuf) call bummer(
     & 'DRT file do not exist for the co set No ',iciset1,faterr)
      call trnfln( 1, fname )
      open(unit=ndrt1,file=fname,status='old')
c
      if (iciset1.ne.iciset2) then
         if(iciset2.ge.10) then
            write(fname,'(a8,i2)')'mcdrtfl.',iciset2
         else
            write(fname,'(a8,i1)')'mcdrtfl.',iciset2
         endif
         inquire(file=fname,exist= lbuf)
         if (.not.lbuf) call bummer(
     &    'DRT file do not exist for the ci set No ',iciset1,faterr)
         call trnfln( 1, fname )
         open(unit=ndrt2,file=fname,status='old')
      endif
c
c  read the drt information from the drtfile:
c
      ntitle=ntitle+1
      flags(9) = .true.
c
c     core(*)==>1:buf
c
      write(nlist,'(/,"***   DRT table No. 1   ***")')
      call rdhdrt(
     & title(ntitle), ndoub,  nact,   nrow,
     & ssym1,          smult1,  nelt,   nela,
     & nwalk,         ncsf1,   lenbuf, nmotx,
     & core(cpt(1)),ndrt1)
c
c     # ind(*) is referenced only if necessary.
      qind(1) = nwalk .ne. ncsf1
c
c  allocate space for arrays:
c   1:xbar1,2:xp1,3:z1,4:r1,5:ind1,6:modrt1,7:syml1,8:a1,9:b1,10:l1,
c   11:y1,12:nj1,13:njs1,14:doub1,15:buf
c
      nlevel=nact+1
      nnact=nact*(nact+1)/2
      cpt(2)=cpt(1)+forbyt(nsym*nrow)           ! xbar1
      cpt(3)=cpt(2)+forbyt(nsym*nrow)           ! xp1
      cpt(4)=cpt(3)+forbyt(nsym*nrow)           ! z1
      cpt(5)=cpt(4)+forbyt(nwalk)               ! r1
      cpt(6)=cpt(5)+forbyt(nwalk)               ! ind1
      cpt(7)=cpt(6)+forbyt(nlevel)              ! modrt
      cpt(8)=cpt(7)+forbyt(nlevel)              ! syml1
      cpt(9)=cpt(8)+forbyt(nrow)                ! a1
      cpt(10)=cpt(9)+forbyt(nrow)               ! b1
      cpt(11)=cpt(10)+forbyt(4*nrow)            ! l1
      cpt(12)=cpt(11)+forbyt(4*nsym*nrow)       ! y1
      cpt(13)=cpt(12)+forbyt(nlevel)            ! nj1
      cpt(14)=cpt(13)+forbyt(nlevel)            ! njs1
      cpt(15)=cpt(14)+forbyt(ndoub)             ! doubl1
c
      if (cpt(15).gt.lcore) then
      write(nlist,'("required/avalable",i10,"/",i10)')cpt(15),lcore
      call bummer('main 1: not enough memory allocated',lcore,faterr)
      endif
c
      do j=1,nmot
      orbtyp(j) = 3
      enddo
      call rddrt(ndoub,nrow,nwalk,ncsf1,lenbuf,
     &  core(cpt(15)),core(cpt(14)),core(cpt(6)),core(cpt(7)),
     &  core(cpt(12)),core(cpt(13)),core(cpt(8)),core(cpt(9)),
     &  core(cpt(10)),core(cpt(11)),core(cpt(1)),core(cpt(2)),
     &  core(cpt(3)),core(cpt(5)),core(cpt(4)),occl,flags(9),ndrt1)
c
c   check the consistence of the DRT input
c
      if (ncsf1.ne.ncsf_f(iciset1)) then
      write(nlist,*)' DRT error'
      write(nlist,
     & '(3x,"No of csf for vector 1 from restart file is",i3)')
     & ncsf_f(iciset1)
      write(nlist,
     & '(3x,"No of csf for vector 1 from DRT file is",i3)')ncsf1
      call bummer('Inconsistent DRT input for CI vector ',1,faterr)
      endif
c
      if (iciset1.eq.iciset2) then
      ndrt2 = ndrt1
      rewind ndrt1
      endif
      write(nlist,'(/,"***   DRT table No. 2   ***")')
      call rdhdrt(
     & title(ntitle), ndoub,  nact,   nrow2,
     & ssym2,          smult2,  nelt,   nela,
     & nwalk2,         ncsf2,   lenbuf, nmotx,
     & core(cpt(15)),ndrt2)
c
c     # ind(*) is referenced only if necessary.
      qind(2) = nwalk2 .ne. ncsf2
c
c  allocate space for arrays:
c   1:xbar1,2:xp1,3:z1,4:r1,5:ind1,6:modrt1,7:syml1,8:a1,9:b1,10:l1,
c   11:y1,12:nj1,13:njs1,14:doub1,15:xbar2,16:xp1,17:z2,18:r2,
c   19:ind2,20:modrt2,21:syml2,22:a2,23:b2,24:l2,25:y2,26:nj2,
c   27:njs2,28:doub2,29:buf
c
      cpt(16)=cpt(15)+forbyt(nsym*nrow2)        ! xbar2
      cpt(17)=cpt(16)+forbyt(nsym*nrow2)        ! xp2
      cpt(18)=cpt(17)+forbyt(nsym*nrow2)        ! z2
      cpt(19)=cpt(18)+forbyt(nwalk2)             ! r2
      cpt(20)=cpt(19)+forbyt(nwalk2)             ! ind2
      cpt(21)=cpt(20)+forbyt(nlevel)            ! modrt2
      cpt(22)=cpt(21)+forbyt(nlevel)            ! syml2
      cpt(23)=cpt(22)+forbyt(nrow2)             ! a2
      cpt(24)=cpt(23)+forbyt(nrow2)             ! b2
      cpt(25)=cpt(24)+forbyt(4*nrow2)           ! l2
      cpt(26)=cpt(25)+forbyt(4*nsym*nrow2)      ! y2
      cpt(27)=cpt(26)+forbyt(nlevel)            ! nj2
      cpt(28)=cpt(27)+forbyt(nlevel)            ! njs2
      cpt(29)=cpt(28)+forbyt(ndoub)             ! doub2
c
      if (cpt(29).gt.lcore) then
      write(nlist,'("required/avalable",i10,"/",i10)')cpt(29),lcore
      call bummer('main 2: not enough memory allocated',lcore,faterr)
      endif
c
      do j=1,nmot
      orbtyp(j) = 3
      enddo
      call rddrt(ndoub,nrow2,nwalk2,ncsf2,lenbuf,
     &  core(cpt(29)),core(cpt(28)),core(cpt(20)),core(cpt(21)),
     &  core(cpt(26)),core(cpt(27)),core(cpt(22)),core(cpt(23)),
     &  core(cpt(24)),core(cpt(25)),core(cpt(15)),core(cpt(16)),
     &  core(cpt(17)),core(cpt(19)),core(cpt(18)),occl,flags(9),ndrt2)
c
c   check the consistence of the DRT input
c
      if (ncsf2.ne.ncsf_f(iciset2)) then
      write(nlist,*)' DRT error'
      write(nlist,
     & '(3x,"No of csf for vector 2 from restart file is",i3)')
     &  ncsf_f(iciset2)
      write(nlist,9020)ncsf2
9020  format(3x,
     & "No of csf for vector 2 from DRT file is",i3)
      call bummer('Incosistent DRT input for CI vector ',2,faterr)
      endif

      close(unit=ndrt1)
      if (iciset1.ne.iciset2) close(unit=ndrt2)
c
      call timer('initialization',2,event1,nlist)
c
      call add1(core(cpt(6)),nelmt)
      if (ssym1.eq.ssym2) then
      d1dim = nnta
      else
      d1dim = nelmt
      endif
cdd
c     d1 dimension for unpacked density matrix
      d1dim = nbft*nbft
cdd
c
c    write out the CI vector information to user output
c
      write(nlist,9010)
9010  format(//,3x,
     & "The transition moment will be calculatel between ",
     & "the following CI states:")
      write(nlist,9000)
9000  format(/,3x,
     & "CI vector No",3x,"space symmetry",3x,"set No",4x,
     & "sequence No",4x,"energy (a.u.)"/)
      write(nlist,'(6x,"1",14x,a4,10x,i3,10x,i3,7x,f15.8)')
     &  slabel(ssym1),iciset1,iorder1,heig(iciset1,iorder1)
      write(nlist,'(6x,"2",14x,a4,10x,i3,10x,i3,7x,f15.8,//)')
     &  slabel(ssym2),iciset2,iorder2,heig(iciset2,iorder2)
c
c    calculate the dimenstion of the whole CI vector
c
      hdim = 0
      do ist = 1,nst
      hdim = hdim + ncsf_f(ist)*navst(ist)
      enddo
c
c    read hvec from restart file
c
      call rdlv(nrestp,hdim,hdim,core(cpt(29)),'hvec',.true.)
c
c    set the counter for two CI vectors
c
      ipoint1 = 0
      do ist = 1,iciset1-1
      ipoint1 = ipoint1+ncsf_f(ist)*navst(ist)
      enddo
      ipoint1 = ipoint1 +ncsf_f(iciset1)*(iorder1-1)
c
      ipoint2 = 0
      do ist = 1,iciset2-1
      ipoint2 = ipoint2+ncsf_f(ist)*navst(ist)
      enddo
      ipoint2 = ipoint2 +ncsf_f(iciset2)*(iorder2-1)
c
c   write out CI vectors to user output
c
      if (flags(1)) then
      call prblkt('CI vector No 1',core(cpt(29)+ipoint1),ncsf1,ncsf1,1,
     & 'csf ',' ',1,nlist)
      call prblkt('CI vector No 2',core(cpt(29)+ipoint2),ncsf2,ncsf2,1,
     & 'csf ',' ',1,nlist)
      endif
c
c  allocate space for arrays:
c   1:xbar1,2:xp1,3:z1,4:r1,5:ind1,6:modrt1,7:syml1,8:a1,9:b1,10:l1,
c   11:y1,12:nj1,13:njs1,14:doub1,15:xbar2,16:xp1,17:z2,18:r2,
c   19:ind2,20:modrt2,21:syml2,22:a2,23:b2,24:l2,25:y2,26:nj2,
c   27:njs2,28:doub2,29:hvec,30:d1,31:tden1,32:tden2,33:ftbuf
c
      cpt(30) = cpt(29) + atebyt(hdim)
      cpt(31) = cpt(30) + atebyt(d1dim)
      cpt(32) = cpt(31) + atebyt(ncsf1)
      cpt(33) = cpt(32) + atebyt(ncsf2)
      cpt(34) = cpt(33) + atebyt(lenbft)
c
      if (cpt(34).gt.lcore) then
      write(nlist,'("required/avalable",i10,"/",i10)')cpt(34),lcore
      call bummer('main 3: not enough memory allocated',lcore,faterr)
      endif
c
      fdft = 'mcdftfl'
      foft = 'mcoftfl'
      istep = 1
c
      call wzero(atebyt(d1dim),core(cpt(30)),1)
500   continue
c
      call trnfln( 1, fdft )
      open(unit=ndft,file=fdft,status='old',form='unformatted')
c
      call trnfln( 1, foft )
      open(unit=nft,file=foft,status='old',form='unformatted')
c
c
c    calculate the transition density matrix
c
      call rdft(core(cpt(33)),lenbft,core(cpt(30)),
     &  core(cpt(29)+ipoint1),core(cpt(29)+ipoint2),ncsf1,ncsf2,
     &  core(cpt(1)),core(cpt(2)),core(cpt(3)),core(cpt(4)),
     &  core(cpt(16)),core(cpt(17)),core(cpt(18)),
     &  core(cpt(5)),core(cpt(6)),qind(1),core(cpt(31)),
     &  core(cpt(32)),core(cpt(7)),core(cpt(21)),core(cpt(10)),
     &  core(cpt(24)),istep,nwalk,nwalk2)
c
499   close(ndft)
      close(nft)
      istep = istep + 1

c    #  exchange bra and ket vector
      if ((ssym1.ne.ssym2).and.(istep.eq.2)) then
      fdft = 'mcdftfl.2'
      foft = 'mcoftfl.2'
      go to 500
      endif

c
c
 650  if (flags(3))
     & call prblkt('d1 matrix in MO',core(cpt(30)),nbft,nbft,nbft,
     & ' mo',' mo',1,nlist)
c
c  allocate space for arrays:
c   1:xbar1,2:xp1,3:z1,4:r1,5:ind1,6:modrt1,7:syml1,8:a1,9:b1,10:l1,
c   11:y1,12:nj1,13:njs1,14:doub1,15:xbar2,16:xp1,17:z2,18:r2,
c   19:ind2,20:modrt2,21:syml2,22:a2,23:b2,24:l2,25:y2,26:nj2,
c   27:njs2,28:doub2,29:hvec,30:d1,31:mocoef,
c
      cpt(32) = cpt(31) + nbmt
c
      if (cpt(32).gt.lcore) then
      write(nlist,'("required/avalable",i10,"/",i10)')cpt(32),lcore
      call bummer('main 4: not enough memory allocated',lcore,faterr)
      endif
c
c     read in the currect MOs
c
c     flags(6) = .true.   - read MOs from 'mocoef' file
c     flags(6) = .false.  - read MOs from 'restart' file
c
      call read_mo(core(cpt(31)))
c
      if (flags(5)) then
      call prblks('initial orbital coefficients',
     + core(cpt(31)),nsym,nbpsy,nmpsy,' bfn','  mo',1,nlist)
      endif
c
c    transforme the d1 matric to the AO basis
c
c   temporary: 32:cbuf,33:bufmat
      cpt(33) = cpt(32) + nbft*nbft
      cpt(34) = cpt(33) + nbft*nbft
c
      if (cpt(34).gt.lcore) then
      write(nlist,'("required/avalable",i10,"/",i10)')cpt(34),lcore
      call bummer('main 5: not enough memory allocated',lcore,faterr)
      endif
c
      call trf_d1(core(cpt(31)),core(cpt(30)),core(cpt(32)),
     & core(cpt(33)),idebug)
c
      if (flags(4)) then
      call prblkt('d1 in AO',core(cpt(30)),nbft,nbft,nbft,'bft','bft',
     & 1,nlist)
      endif
c
c  allocate space for arrays:
c   1:xbar1,2:xp1,3:z1,4:r1,5:ind1,6:modrt1,7:syml1,8:a1,9:b1,10:l1,
c   11:y1,12:nj1,13:njs1,14:doub1,15:xbar2,16:xp1,17:z2,18:r2,
c   19:ind2,20:modrt2,21:syml2,22:a2,23:b2,24:l2,25:y2,26:nj2,
c   27:njs2,28:doub2,29:hvec,30:d1,31:mocoef,32:r,33:buffer,
c   34:values,35:labels,36:fcore
c
      l1rec = infoao(2)
      n1max = infoao(3)

      lda = nbft*(nbft+1)/2
      cpt(33) = cpt(32) + atebyt(3*lda)
      cpt(34) = cpt(33) + atebyt(l1rec)
      cpt(35) = cpt(34) + atebyt(n1max)
      cpt(36) = cpt(35) + forbyt(2*n1max)
      cpt(37) = cpt(36) + forbyt(lda)
c
      if (cpt(37).gt.lcore) then
      write(nlist,'("required/avalable",i10,"/",i10)')cpt(37),lcore
      call bummer('main 6: not enough memory allocated',lcore,faterr)
      endif
c
      itype = 1
      btypmx = 2
      btypes(0) = 1
      btypes(1) = 2
      btypes(2) = 3
      do i=1,nmotx
      mapin(i) = i
      enddo
      call wzero(atebyt(lda),core(cpt(36)),1)
      call wzero(atebyt(3*lda),core(cpt(32)),1)
      call mdsifr1n(aoints,infoao,itype,btypmx,btypes,core(cpt(33)),
     & core(cpt(34)),core(cpt(35)),nsym,nbpsy,symoff,mapin,lda,
     & core(cpt(32)),core(cpt(36)),symb2,kntin,lasta,lastb,last,nrec,
     & ierr)
c
ctm
      if (ierr .eq.-4) then
       call bummer('missing property integrals on ao-integral file',
     .              ierr,0)
      endif
      if (ierr .ne. 0) then
      call bummer(
     & 'main: invalid integral file for r marix elements, ierr = ',
     & ierr,faterr)
      endif
c
c    write the x,y,z matrix to the user output
c
      if (flags(2)) call prt_xyz(core(cpt(32)),lda)
c
c    calculate Transition density moment
c        X = tr(x.d1)
c        Y = tr(y.d1)
c        Z = tr(z.d1)
c
c
      call transi(core(cpt(32)),core(cpt(30)),lda,trans)
c
c    write out the final results
c

      write(nlist,'(//,80("*"),///,12x,"***   FINAL RESULTS   ***",/)')
      write(nlist,'(//,26x,"State No. 1",12x,"State No. 2")')
      write(nlist,'(/,3x,"Space symmetry:",11x,a4,19x,a4))')
     & slabel(ssym1),slabel(ssym2)
      write(nlist,'(3x,"Multiplicity:",14x,i3,20x,i3)')smult1,smult2
      write(nlist,'(3x,"No. of CSFs:",12x,i6,17x,i6)')
     &  ncsf_f(iciset1),ncsf_f(iciset2)
      write(nlist,'(3x,"State energies:",4x,f15.8,8x,f15.8,2x,"a.u.")')
     & heig(iciset1,iorder1),heig(iciset2,iorder2)
      del_au = heig(iciset2,iorder2)-heig(iciset1,iorder1)
c    #   1 a.u. = 27.211396 eV
      del_ev = del_au*ev
      write(nlist,'(/,3x,"Transition energy:",3x,f12.8,2x,"a.u.")')
     & del_au
      write(nlist,'(3x,"Transition energy:",3x,f6.2,8x,"eV")')del_ev
c
      erg = echarg * echarg / bohr
      freq = del_au * erg / planck
      freq2 =  del_au * cm_1
c
      write(nlist,'(3x,"Transition frequency:",2x,1pe11.5,1x,"1/sec")')
     &  freq
      write(nlist,'(3x,"Transition frequency:",2x,1pe11.5,1x,"cm-1")')
     &  freq2
c
      write(nlist,'(//,7x,"Transition moment components:")')
      write(nlist,'(/,9x,"x",12x,"y",12x,"z",/)')
      write(nlist,'(3(3x,f10.6),2x,"e*bohr")')trans
c
      diplen = 0.0d+00
      do i=1,3
         diplen = diplen + trans(i) * trans(i)
      enddo
      diplen = sqrt(diplen)
c
      write(nlist,'(//,3x,"Tr.dipole length:",3x,f10.6,4x,"e*bohr")')
     & diplen
c
      omega = freq / clight

      f = two * del_au * diplen * diplen/three
      f2 = 3.0381d-06 * freq2 * diplen * diplen
      fact = (debye * 1.0D-18 / planck)
      pi = acos(-one)
      fact = eight * pi * pi * pi * fact * fact
      fact = fact/(three*clight)
      b = fact * diplen * diplen
      a = eight * pi * del_au * erg * omega * omega * b

      write(nlist,'(3x,"Oscillator strength:",1x,f9.6)')f
c     write(nlist,'(3x,"Oscillator strength :",1x,f9.6)')f2
      write(nlist,'(3x,"Einstein A coef.:",3x,1pe12.4,2x,"1/sec")')a
      write(nlist,'(3x,"Einstein B coef.:",3x,1pe12.4,2x,"sec/g",///)')b



      end
C***********************************************************************
C***********************************************************************
      block data
c
      implicit integer(a-z)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
c
      common/cfiles/iunits(25)
      equivalence (iunits(1),nlist)
      equivalence (iunits(2),nin)
      equivalence (iunits(4),ndrt1)
      equivalence (iunits(5),ndrt2)
      equivalence (iunits(6),ndft)
      equivalence (iunits(7),nft)
      equivalence (iunits(9),trsci)
      equivalence (iunits(10),nsfile)
      equivalence (iunits(11),ndiagf)
      equivalence (iunits(12),nrestp)
      equivalence (iunits(19),mos)
      equivalence (iunits(21),aoints)
c
c  standard 8-by-8 symmetry multiplication table.
c
      data mult/
     & 1,2,3,4,5,6,7,8,
     & 2,1,4,3,6,5,8,7,
     & 3,4,1,2,7,8,5,6,
     & 4,3,2,1,8,7,6,5,
     & 5,6,7,8,1,2,3,4,
     & 6,5,8,7,2,1,4,3,
     & 7,8,5,6,3,4,1,2,
     & 8,7,6,5,4,3,2,1/
c
c  file unit numbers.
c
      data nlist  /60/
      data nin    /50/
      data ndrt1  /11/
      data ndrt2  /12/
      data ndft   /13/
      data nft    /14/
      data trsci  /4/
      data nrestp /17/
      data mos    /56/
      data aoints /58/
c
      end
      subroutine readin(idebugx)
c
c  read in user information.
c
c  17-jan-90 wnorm equivalence fixed. -rls/eas
c  written by ron shepard.
c
cmb   reading in number of states to average over (navst) and their
cmb   weights (wavst).

      implicit integer(a-z)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nbpsy(8),nmpsy(8)
      equivalence (nxy(1,1),nbpsy(1))
      equivalence (nxy(1,5),nmpsy(1))
      equivalence (nxtot(4),nmot)
c
      common/cfiles/iunits(25)
      equivalence (iunits(1),nlist)
      equivalence (iunits(2),nin)
c
c  use iorbx(*,i) for i = 1 to 6 as local scratch space.
c  orbtyp(*) and fcimsk(*) are the only arrays used elswhere.
c
      parameter(nmotx=1023)
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer orbtyp(nmotx),fcimsk(nmotx)
      equivalence (iorbx(1,7),orbtyp(1))
      equivalence (iorbx(1,8),fcimsk(1))
c
      integer npath(nmotx)
      equivalence (iorbx(1,1),npath(1))
      integer fciorb(2,nmotx)
      equivalence (iorbx(1,2),fciorb(1,1))
      integer fcore(nmotx)
      equivalence (iorbx(1,4),fcore(1))
      integer fvrt(nmotx)
      equivalence (iorbx(1,5),fvrt(1))
c
      real*8 coninf
      common/cconi/coninf(6)
      real*8 wnorm
      equivalence (coninf(4),wnorm)
c
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
c
      logical idebug

ctm
      logical idebugx
ctm
c
c
*@ifndef nonamelist
      namelist/input/flags,idebug,npath
*@endif
c
c  bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer fcorex(0:5),         fvrtx(0:5)
      data    fcorex/5,5,5,0,5,5/, fvrtx/5,5,5,4,5,5/
c
c**********************************************************************
c
      write(nlist,6010)
6010  format(/' USER INPUT INFORMATION:'/)
c
c  set up default values
c
      do 100 i=1,nflag
         flags(i)=.false.
100   continue
c
      idebug = .false.
c
c  initialize npath(*),fciorb(*,*),fcore(*),
c  fvrt(*),orbtyp(*), and fcimsk(*).
c
      do 122 j=1,8
         do 120 i=1,nmotx
            iorbx(i,j)=0
120      continue
122   continue
c
c  initialize nmpsy(*) to include frozen-core, inactive, active,
c  virtual, and frozen virtual orbitals.
c
      do 140 i=1,8
         nmpsy(i)=nbpsy(i)
140   continue
c
c**********************************************************************
c
c
c  read and echo the user input...
c
      write(nlist,'(1x,a)' ) '======== echo of the mcscf input ========'
      call echoin( nin, nlist, ierr )
c
      if ( ierr .ne. 0 ) then
         call bummer('readin: from echoin, ierr=',ierr,faterr)
      endif
c
      rewind nin
c
c  read namelist input...
c
*@ifdef nonamelist
*C    Cray-T3D unterstuetzt keinen Namelist-Input
*C    listenorientierte Eingabe nach folgendem Muster
*C     , , , , , / NPATH
*C     , , ,     / NSTATE, NITER
*C     , ,  ,    / TOL
*C     , ,       / NMITER, NVRSMX
*C     , ,       / NCOL, NCOUPL
*C     , ,       / NMPSY
*C     , ,       / FCIORB : 1. Paar
*C     , ,       / FCIORB : 2. Paar etc.   bis EOF
*        read(nin,*) (npath(i),i=1,nflag)
*        read(nin,*) nstate,niter
*        read(nin,*) (tol(i),i=1,10)
*        read(nin,*) nmiter, nvrsmx
*        read(nin,*) ncol, ncoupl
*        read(nin,*) (nmpsy(i),i=1,nsym)
*        i=1
*  198   read(nin,*,end=200) fciorb(1,i),fciorb(2,i)
*        i=i+1
*        goto 198
*@elif defined  nml_required
*      read( nin, nml=input, end=200 )
*@else
      read( nin, input, end=200 )
*@endif
200   continue
c
ctm
         idebugx=idebug
ctm
c**********************************************************************
c
c  process and print out the input data just read.
c
      do 230 i=1,nmotx
         if(npath(i))210,231,220
210      flags(-npath(i))=.false.
         go to 230
220      flags(npath(i))=.true.
230   continue
231   continue
c
c  check nmpsy(*).
c
      do 240 i=1,nsym
         if(nmpsy(i).gt.nbpsy(i))then
            write(nlist,*)'readin: nmpsy(*) error.'
            call bummer('readin: nmpsy(*) error.',0,faterr)
         endif
240   continue
c
c  initialize orbtyp(*)=3 (virtual orbital).
c  set orbtyp(*)=0 for frozen core orbitals.
c               =4 for frozen virtual orbitals.
c
      nmot=0
      do 250 i=1,nsym
         nmot=nmot+nmpsy(i)
250   continue
      do 260 i=1,nmot
         orbtyp(i)=3
260   continue
c
      nfcore=0
      do 270 i=1,nmotx
         j=fcore(i)
         if(j.le.0)go to 271
         nfcore=nfcore+1
         orbtyp(j)=fcorex(orbtyp(j))
270   continue
271   continue
c
      nfvrt=0
      do 280 i=1,nmotx
         j=fvrt(i)
         if(j.le.0)go to 281
         nfvrt=nfvrt+1
         orbtyp(j)=fvrtx(orbtyp(j))
280   continue
281   continue
cmd   write(nlist,6020)'nfcore=',nfcore
      if(nfcore.gt.0)write(nlist,6020)'fcore(*)=',(fcore(i),i=1,nfcore)
cmd   write(nlist,6020)'nfvrt=',nfvrt
      if(nfcore.gt.0)write(nlist,6020)'fvrt(*)=',(fvrt(i),i=1,nfvrt)
6020  format(1x,a,(1x,20i4))
      if(nfcore.ne.0 .or. nfvrt.ne.0)then
         write(nlist,*)'fcore(*) and fvrt(*) are not allowed'
         call bummer('readin: nfcore nfvrt error',0,faterr)
      endif
c
c
c  flags(1) must be .true., otherwise print out option list and stop.
c
      if(flags(1)) write(nlist,6201)
6201  format(/'  1:  print CI vector of the transition states.'/)
c
      if(flags(2)) write(nlist,6202)
6202  format(/'  2:  print R matrix elements.'/)
c
      if(flags(3)) write(nlist,6303)
6303  format(/'  3:  print transition density matrix in MO basis'/)
c
      if(flags(4)) write(nlist,6304)
6304  format(/'  4:  print transition density matrix in AO basis'/)
c
      if(flags(5)) write(nlist,6305)
6305  format(/'  5:  print initial MO coeficients'/)
c
      if(flags(6)) then
        write(nlist,6306)
6306    format(/'  6:   read MO coeficients from mocoef file'/)
      else
        write(nlist,6307)
6307    format(/' -6:   read MO coeficients, MORBN, from restart file'/)
      endif
c
      return
      end

