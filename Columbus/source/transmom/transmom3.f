!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/



      subroutine rdft(ftbuf,lenbft,
     +  d1,hvect1,hvect2,
     +  ncsf1,ncsf2,xbar,xp1,z1,r1,xp2,z2,r2,ind,modrt,qind,
     +  tden1,tden2,syml1,syml2,l1,l2,istep,nwalk,nwalk2)
c
c  read the unitary group formula tape and construct
c  the one- and two- particle symmetric density and
c  transition density matrix elements over the active orbitals.
c  an index driven algorithm is used to facilitate the construction
c  and use of the density matrix elements.  the required blocks of
c  the c matrix are constructed as the transition density matrix
c  elements are available. density matrix elements of the form
c  <mc| e |mc> are stored in d1(*) and d2(*).
c
c  the formula tape is packed with both integer indexing
c  information and the working precision loop values according to
c  the following scheme (see program mft2 for details):
c
c==================================================
c  diagonal ft:
c  code values, loop type, generator products
c
c   0  -- new record --
c   1  -- new level --
c
c   2       11ab        eiill, eilli
c   3       11a         eiill
c   4       11b         eilil
c
c   5       14ab        half*ellll, ell
c   6       14a         half*ellll
c   7       14b         ell
c--------------------------------------------------
c  off-diagonal ft:
c  code,    loop type, generator products
c
c   0  -- new record --
c   1  -- new level --
c
c   2           12abc      iiil, illl, il
c   3           2a'b'      ijlk, iklj
c   4           1ab        ijkl, ilkj
c   5           3ab        ikjl, iljk
c   6           4ab        iikl, ilki
c               8ab        ijll, illj
c   7           6a'b'      ikkl, ilkk
c   8           12ac       iiil, il
c   9           12bc       illl, il
c  10           1a         ijkl
c               2a'        ijlk
c               6a'        ikkl
c               7'         iklk
c  11           4a         iikl
c               8a         ijll
c  12           2b'        iklj
c               3a         ikjl
c               4b         ilki
c               4b'        ikli
c               5          ilik
c               8b         illj
c               10         iljl
c  13           11b        illi
c               13         half*ilil
c  14           1b         ilkj
c               3b         iljk
c  15           6b'        ilkk
c               12c        il
c==================================================
c  symmetry ft version: 18-oct-84, ron shepard.
c
      implicit integer(a-z)
c
      common/cfiles/iunits(25)
      equivalence (iunits(1),nlist)
      equivalence (iunits(6),ndft)
      equivalence (iunits(7),nft)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nsa(8),nnsa(8),napsy(8)
      equivalence (nxy(1,14),nsa(1))
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxy(1,15),nnsa(1))
      integer nbft
      equivalence (nxtot(1),nbft)
      equivalence (nxtot(10),nact)
      equivalence (nxtot(11),nnta)

c
      common/caddb/addpt(24),numint(12),bukpt(12),intoff(12),szh(15)
c
      parameter(nmotx=1023)
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx),symm(nmotx),orbidx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
      equivalence (iorbx(1,3),symm(1))
      equivalence (iorbx(1,6),orbidx(1))
c
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
cmd
      integer ssym1,ssym2,d1dim
      common /ssym/ ssym1,ssym2,d1dim
c
      integer syml1(0:nmotx),syml2(0:nmotx),l1(0:3,*),l2(0:3,*)
c
      real*8 dens
c
      integer istep
cmd
c
c  dummy arrays:
c
      real*8 ftbuf(lenbft),hvect1(ncsf1),hvect2(ncsf2)
      real*8 tden1(ncsf1),tden2(ncsf2)
      real*8 d1(nbft,nbft)
      integer xbar(nsym,*)
      integer xp1(nsym,*),z1(nsym,*),r1(*)
      integer xp2(nsym,*),z2(nsym,*),r2(*)
      integer ind(*),modrt(0:*)
      logical qind
c
c  local:
c
      integer ism(8),ism2(8),yb(8),yk(8),spindens
      real*8 val(3)
      logical qd1
      real*8 ddot_wr
c
      if(flags(3))call timer(' ',1,event,nlist)
c
c     for different state space symmetries omit diagonal elements
c     calculation
c
      if (ssym1.ne.ssym2) go to 2000
c
c  diagonal elements.
c
c     write(*,*)'head,tail,val,icode-1'
      rewind ndft
      kntft=1
      nw=0
      read(ndft)ftbuf
c
      do 1700 lmo=1,nact
      ll=orbidx(modrt(lmo))
      labsind = modrt(lmo)
      lsym=symm(modrt(lmo))
      lldx=nndx(ll+1)
c
      do 1600 imo=1,lmo
      ii=orbidx(modrt(imo))
      iidx=nndx(ii+1)
      lidx=nndx(max(ii,ll))+min(ii,ll)
      call wzero(ncsf1,tden1,1)
      call wzero(ncsf2,tden2,1)
c
1000  continue
      kntft=kntft+nw
      call decodf(nv,nsv,symw,head,tail,icode,val,ism,yb,yk,
     & nw,ftbuf(kntft),ism2,max(nwalk,nwalk2),spindens)
c
c
      igo=min(icode,2)+1
      go to (1100,1400,1200),igo
c
1100  continue
c
c     new record.
c
      kntft=1
      nw=0
      read(ndft)ftbuf
      go to 1000
c
1200  continue
c
c     write(*,*)'diag   ',head,tail,val,icode-1
      call sdmatd(symw,nsv,ism,yb,
     +  xbar(1,head),xp1(1,tail),z1(1,tail),r1,ind,
     +  val,icode,hvect1,tden1,ncsf1,ncsf2,qind)
      go to 1000
c
1400  continue
c
c  change level.  process density contributions at the present level.
c
      if(imo.ne.lmo)go to 1600
c
1500  continue
c  i.eq.l case
      lt=ll-nsa(lsym)
      llt=nnsa(lsym)+nndx(lt+1)
cmd   d1(llt)=ddot_wr(ncsf2,tden1,1,hvect2,1)
      d1(labsind,labsind)=ddot_wr(ncsf2,tden1,1,hvect2,1)
c
1600  continue
1700  continue
c
c  off-diagonal loop contributions.
c
2000  rewind nft
      kntft=1
      nw=0
      read(nft)ftbuf
c
      dens = 0.0d+00
c
c     do 2740 lmo=2,nact
c     ll=orbidx(modrt(lmo))
c     lsym=symm(modrt(lmo))
c     lldx=nndx(ll+1)
c
c     do 2730 kmo=1,lmo
c     kk=orbidx(modrt(kmo))
c     ksym=symm(modrt(kmo))
c     klsym=mult(lsym,ksym)
c     lkdx=nndx(max(ll,kk))+min(ll,kk)
c
c     do 2720 jmo=1,kmo
c     jj=orbidx(modrt(jmo))
c     jsym=symm(modrt(jmo))
c     jklsym=mult(jsym,klsym)
c     ljdx=nndx(max(ll,jj))+min(ll,jj)
c     kjdx=nndx(max(kk,jj))+min(kk,jj)
c
c     do 2710 imo=1,jmo
c     if(imo.eq.kmo)go to 2710
c     isym=symm(modrt(imo))
c     if(isym.ne.jklsym)go to 2710
c     ii=orbidx(modrt(imo))
c     lidx=nndx(max(ll,ii))+min(ll,ii)
c     kidx=nndx(max(kk,ii))+min(kk,ii)
c     jidx=nndx(max(jj,ii))+min(jj,ii)
c     iidx=nndx(ii+1)
c     call wzero(ncsf,tden,1)
c     qd1 = .false.
cdd
      do 2740 lmo=2,nact
      ll=orbidx(modrt(lmo))
      labsind = modrt(lmo)
      lsym=symm(modrt(lmo))
      lldx=nndx(ll+1)
c
      do 2720 imo=1,lmo-1
      ii=orbidx(modrt(imo))
      iabsind = modrt(imo)
      isym=symm(modrt(imo))
      if (mult(lsym,isym).ne.mult(ssym1,ssym2)) go to 2720
      call wzero(ncsf1,tden1,1)
      call wzero(ncsf2,tden2,1)
      qd1 = .false.
cdd
2100  continue
      kntft=kntft+nw
      call decodf(nv,nsv,symw,head,tail,icode,val,ism,yb,yk,
     & nw,ftbuf(kntft),ism2,max(nwalk,nwalk2),spindens)
c
      igo=min(icode,2)+1
      go to (2200,2500,2300),igo
2200  continue
c
c  new record
c
      kntft=1
      nw=0
      read(nft)ftbuf
      go to 2100
c
2300  continue
c
c     write(*,*)'offdiag',head,tail,val,icode-1
      call sdmat(symw,nsv,ism,ism2,yb,yk,
     +  xbar,xp1(1,tail),z1(1,tail),r1,
     +  xp2(1,tail),z2(1,tail),r2,ind,
     +  val,icode,hvect1,hvect2,tden1,tden2,ncsf1,ncsf2,qd1,qind,
     +  head,tail,syml1,syml2,l1,l2,dens,istep)
      go to 2100
c
2500  continue
c
c  change level.  process density contributions.
c
c     if(jmo.ne.lmo)go to 2710
c
2600  continue
c
c ll,ii,ii,ii; ll,ll,ll,ii; ll,ii  special case.
c
      if(qd1)then
         lt=ll-nsa(lsym)
         it=ii-nsa(lsym)
         lit=nnsa(lsym)+nndx(max(lt,it))+min(lt,it)
cmd         d1(lit)=d1(lit)+ddot_wr(ncsf2,tden2,1,hvect2,1)
            d1(labsind,iabsind) =d1(labsind,iabsind) + dens
            d1(iabsind,labsind) =d1(iabsind,labsind) + dens
            dens = 0.0d+00
      endif
c
c
2710  continue
2720  continue
2730  continue
2740  continue
c
      return
      end

      subroutine sdmat(symw,nsv,ism,ism2,yb,yk,xbar,xp1,z1,r1,
     +  xp2,z2,r2,ind,
     +  val,icode,hvect1,hvect2,tden1,tden2,ncsf1,ncsf2,qd1,qind,
     +  head,tail,syml1,syml2,l1,l2,dens,istep)
c
c  construct the symmetric one- and two-particle transition density
c  matrix element contributions for off-diagonal loops.
c
c  tden(n,ij)  = (1/2)*<mc| eij + eji |n>
c  tden(n,ijkl)= (1/4)*<mc| eijkl + ejikl + eijlk + ejilk |n>
c
c  *note* with this definition of the tden(*) matrix elements, the dot
c  product of tden(*) with hvect(*) gives a d*(*) matrix element of
c  the form <mc| e |mc>.
c
c  the loop values are in val(*), some of which have factors included
c  to facilitate their use in the hamiltonian matrix.
c
c  qind version: 31-may-85
c  symmetry ft version: 18-oct-84
c  programmed by ron shepard.
c
      implicit integer(a-z)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
c
      integer ism(nsv),ism2(nsv),yb(nsv),yk(nsv)
      integer xbar(nsym,*),xp1(nsym),z1(nsym)
      real*8 val(3),hvect1(ncsf1),hvect2(ncsf2)
      real*8 tden1(ncsf1),tden2(ncsf2)
      integer r1(*),ind(*)
      integer xp2(nsym),z2(nsym),r2(*)
      logical qind,qd1
      real*8 half,fourth,t
      data half/0.5d+00/
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
cmd
      integer ssym1,ssym2,d1dim
      common /ssym/ ssym1,ssym2,d1dim
c
      integer nmotx
      parameter(nmotx=1023)
c
      integer syml1(0:nmotx),syml2(0:nmotx),l1(0:3,*),l2(0:3,*)
      integer step1(0:nmotx),step2(0:nmotx)
c
      integer istep
c
      real*8 dens
c
      integer braind,ketind,ihelp
cmd
c
c  account for factors and assign the go to variable.
c
      iop=icode-1
cdd   go to(2, 3, 4, 5, 6, 7,8,9,10,11,12,13,14,15),iop
      go to(2,50,50,50,50,50,8,9,50,50,50,50,50,15),iop
c
2     continue
c  iiil, illl, il
c     assign 1123 to igo
      t=half*val(3)
      qd1 = .true.
      go to 50
c
8     continue
c  iiil, il
c     assign 1013 to igo
      t=half*val(2)
      qd1 = .true.
      go to 50
c
9     continue
c  illl, il
c     assign 1023 to igo
      t=half*val(2)
      qd1 = .true.
      go to 50
c
15    continue
c  ilkk
c  il
c     assign 1003 to igo
      t=half*val(1)
      qd1 = .true.
c
50    continue
c
      do 1300 isv=1,nsv
cmd
c    # warning
c    # yb,(yk) was already coded through ism,(ism2) in
c    # the subroutine encodf, so it is already in proper order
c
      ytb=yb(isv)
      ytk=yk(isv)
c
c    #  normal FT construction on different DRTs
      if (istep.eq.1) then
      tsym_b=ism(isv)
      tsym_k=ism2(isv)
      xt1=xp1(tsym_b)
      xt2=xp2(tsym_k)
      zt1=z1(tsym_b)
      zt2=z2(tsym_k)
c
      hsym=mult(tsym_b,symw)
cmd   xbh=xbar(hsym)
      xbh=xbar(hsym,head)
c
c    #  exchange bra and ket vector
      elseif(istep.eq.2) then

c    # symetry of walk from the 2-nd DRT
      tsym_b=ism(isv)

c    # symetry of walk from the 1-st DRT
      tsym_k=ism2(isv)

c    # bra walk is from DRT2
      xt1=xp1(tsym_k)
      zt1=z1(tsym_k)

c    # ket walk is from DRT1
      xt2=xp2(tsym_b)
      zt2=z2(tsym_b)

c    # note: xbar .eq. xbar2
      hsym=mult(tsym_b,symw)
      xbh=xbar(hsym,head)
c
      endif
c
      if(qind)then
      call bummer('csf selection case not implemented',0,faterr)
c
c  csf selection case.  ind(*) vector is required.
c
c     do 1200 j=1,xt
c     b=ytb+r1(zt+j)
c     k=ytk+r1(zt+j)
c     bmax=ytb+r1(zt+j)+xbh
c100   continue
c     b=b+1
c     k=k+1
c110   continue
c     if(b.gt.bmax)go to 1200
c     ib=ind(b)
c     ik=ind(k)
c     db=min(ib,ik)
c     if(db.gt.0)go to igo
c    +  ,(100,100,1003,100,1013,1023,1123)
c     b=b-db
c     k=k-db
c     go to 110
c
c1003  continue
c     tden(ib,1)=tden(ib,1)+hvect(ik)*t
c     tden(ik,1)=tden(ik,1)+hvect(ib)*t
c     go to 100
c
c
c1013  continue
c     tden(ib,1)=tden(ib,1)+hvect(ik)*t
c     tden(ik,1)=tden(ik,1)+hvect(ib)*t
c      go to 100
c
1023  continue
c     tden(ib,1)=tden(ib,3)+hvect(ik)*t
c     tden(ik,1)=tden(ik,1)+hvect(ib)*t
c     go to 100
c
c1123  continue
c     tden(ib,1)=tden(ib,1)+hvect(ik)*t
c     tden(ik,1)=tden(ik,1)+hvect(ib)*t
c     go to 100
c
c1200  continue
c
      else
c
c  no csf selection.  ind(i)=i for all i.
c
c
      if(qd1)then
cdd
      if (istep.eq.3) go to 550
c
      xtbra = 0
      xtket = 0
      last1 = 0
      last2 = 0
c
995   continue
      if (istep.eq.1) then
      call walkbra(syml1,l1,xbar,ssym1,tail,tsym_b,xtbra,last1,step1,
     & nlev1,xt1)
      else
c    #  bra walk from DRT2
c    #  ket walk from DRT1
      call walkbra(syml2,l2,xbar,ssym2,tail,tsym_b,xtbra,last1,step1,
     & nlev1,xt2)
      endif
c
996   continue
      if (istep.eq.1) then
      call walkket(syml2,l2,xbar,ssym2,tail,tsym_k,xtket,last2,step2,
     & nlev2,xt2)
      else
c    #  bra walk from DRT2
c    #  ket walk from DRT1
      call walkket(syml1,l1,xbar,ssym1,tail,tsym_k,xtket,last2,step2,
     & nlev2,xt1)
      endif
c
      nt = 0
      do i = 1,nlev1
        nt = nt + step1(i) - step2(i)
      enddo
      if (nt.eq.0) then
         if (tail.eq.1) then
           j1 = 1
           j2 = 1
         else
           j1= xtbra
           j2= xtket
         endif

      if (istep.eq.2) then
      ib=ytk+r1(zt1+j1)
      ik=ytb+r2(zt2+j2)
      else
      ib=ytb+r1(zt1+j1)
      ik=ytk+r2(zt2+j2)
      endif
c
c      do 2210 i=1,xbh
c2210  tden1(ik+i)=tden1(ik+i)+hvect1(ib+i)*t
c      do 2220 i=1,xbh
c2220  tden2(ib+i)=tden2(ib+i)+hvect2(ik+i)*t
c
       do i=1,xbh
       braind = ib+i
       ketind = ik+i
c
       if ((braind.le.ncsf1).and.(ketind.le.ncsf2)) then
         dens = dens + (hvect1(ib+i)*hvect2(ik+i))*t
       else
         write(6,*) 'braind,ketind=',braind,ketind,
     .             'ncsf1,2=',ncsf1,ncsf2
         stop 'braind,ketind exceeds ncsf1,ncsf2 dimension'
       endif
c
       if (ssym1.eq.ssym2) then
         dens = dens + (hvect1(ik+i)*hvect2(ib+i))*t
       endif
c
       enddo
c
2100    if(last1.eq.0) go to 995

      elseif ((nt.ne.0).and.(last2.eq.0)) then
      go to 996
      endif
      go to 1290
c
 550  continue
      ib=ytb+r1(zt1+j1)
      ik=ytk+r2(zt2+j2)
c
       do i=1,xbh
       braind = ib+i
       ketind = ik+i
         if ((braind.le.ncsf1).and.(ketind.le.ncsf2)) then
         dens = dens + (hvect1(ib+i)*hvect2(ik+i))*t
         endif
c        if ((braind.le.ncsf2).and.(ketind.le.ncsf1).and.
c    &   (braind.ne.ketind)) then
         if (ssym1.eq.ssym2) then
         dens = dens + (hvect1(ik+i)*hvect2(ib+i))*t
         endif
       enddo

      go to 1290
c

c     xt1 = j1
c     xt2 = j2
c
cdd
cdd   do 1230 j=1,xt
c     ib=ytb+r1(zt+j)
cdd   ik=ytk+r1(zt+j)
c1225  do 1230 j1=1,xt1
c     do 1230 j2=1,xt2
c     ib=ytb+r1(zt1+j1)
c     ik=ytk+r2(zt2+j2)
c      do 1210 i=1,xbh
c1210  tden1(ik+i)=tden1(ik+i)+hvect1(ib+i)*t
c      do 1220 i=1,xbh
c1220  tden1(ib+i)=tden1(ib+i)+hvect1(ik+i)*t
c
c     do 1210 i=1,xbh
c1210  tden1(ik+i)=tden1(ik+i)+hvect1(ib+i)*t
c     do 1220 i=1,xbh
c1220  tden2(ib+i)=tden2(ib+i)+hvect2(ik+i)*t
c
c1230  continue
      endif
c

1290  endif
c
c
1300  continue
c
      return
      end
c**************************************************************
c
      subroutine walkbra(syml,l,xbar,ssym,toplew,topsym,ncsf1,
     &  last,step,nlev,xp)
c
c**************************************************************
      implicit none 
      integer nmotx
      parameter(nmotx=1023)
c
      integer  mxy,mult,nsym,nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nact
      equivalence (nxtot(10),nact)

      integer step(0:nmotx),row(0:nmotx),wsym(0:nmotx)
      integer l(0:3,*),arcsym(0:3,0:nmotx),syml(0:nmotx)
      integer xbar(nsym,*)

      integer rown,rowc,symn,symc,ssym,istep,i,iwalk
      integer nlev,ncsf,nxy,clev
      integer toplew,topsym,ncsf1,last
      integer xp
      save




      if (ncsf1.eq.0) then
      do 30 i=0,nact
         step(i)=4
30    continue
c
      do 190 i = 1, nact
         arcsym(1,i-1) = syml(i)
         arcsym(2,i-1) = syml(i)
         arcsym(0,i-1) = 1
         arcsym(3,i-1) = 1
190   continue
c
c        # bottom level.
c
         row(0)  = 1
         clev    = 0
         wsym(0) = ssym
         endif
         go to 1100
c
c        # decrement the current level.
c
1040     continue
         step(clev) = 4
         if ( clev .eq. 0 ) go to 1180
         clev = clev - 1
c
c        # next step at the current level.
c
1100     continue
         istep = step(clev) - 1
         if ( istep .lt. 0 ) go to 1040
         step(clev) = istep
         rowc = row(clev)
         rown = l(istep,rowc)
         if(rown.eq.0)go to 1100
         symc = wsym(clev)
         symn = mult( arcsym(istep,clev), symc )
         if ( xbar(symn,rown) .eq. 0 ) go to 1100
c
         nlev = clev + 1
         row(nlev) = rown
         wsym(nlev) = symn
cdd
         if ((toplew.eq.rown).and.(topsym.eq.symn)) then
         ncsf1 = ncsf1 + 1
         if (xp.eq.ncsf1) last = 1
         return
         endif
cdd
         if ( nlev .eq. nact ) go to 1150
c
c        # increment to next level.
c
         clev = nlev
         go to 1100
c
c        # examine the current walk.
c
1150     continue
c
         ncsf = ncsf + 1
         go to 1100
c        # exit from walk generation.
1180     continue
         last = 1
      return
      end

c**************************************************************
c
      subroutine walkket(syml,l,xbar,ssym,toplew,topsym,ncsf1,
     & last,step,nlev,xp)
c
c**************************************************************
      implicit none 
      integer nmotx
      parameter(nmotx=1023)
c
      integer  mxy,mult,nsym,nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nact
      equivalence (nxtot(10),nact)

      integer step(0:nmotx),row(0:nmotx),wsym(0:nmotx)
      integer l(0:3,*),arcsym(0:3,0:nmotx),syml(0:nmotx)
      integer xbar(nsym,*)

      integer rown,rowc,symn,symc,ssym,istep,i,iwalk
      integer nlev,ncsf,nxy,clev
      integer toplew,topsym,ncsf1,last
      integer xp
      save




      if (ncsf1.eq.0) then
      do 30 i=0,nact
         step(i)=4
30    continue
c
      do 190 i = 1, nact
         arcsym(1,i-1) = syml(i)
         arcsym(2,i-1) = syml(i)
         arcsym(0,i-1) = 1
         arcsym(3,i-1) = 1
190   continue
c
c        # bottom level.
c
         row(0)  = 1
         clev    = 0
         wsym(0) = ssym
         endif
         go to 1100
c
c        # decrement the current level.
c
1040     continue
         step(clev) = 4
         if ( clev .eq. 0 ) go to 1180
         clev = clev - 1
c
c        # next step at the current level.
c
1100     continue
         istep = step(clev) - 1
         if ( istep .lt. 0 ) go to 1040
         step(clev) = istep
         rowc = row(clev)
         rown = l(istep,rowc)
         if(rown.eq.0)go to 1100
         symc = wsym(clev)
         symn = mult( arcsym(istep,clev), symc )
         if ( xbar(symn,rown) .eq. 0 ) go to 1100
c
         nlev = clev + 1
         row(nlev) = rown
         wsym(nlev) = symn
cdd
         if ((toplew.eq.rown).and.(topsym.eq.symn)) then
         ncsf1 = ncsf1 + 1
         if (xp.eq.ncsf1) last = 1
         return
         endif
cdd
         if ( nlev .eq. nact ) go to 1150
c
c        # increment to next level.
c
         clev = nlev
         go to 1100
c
c        # examine the current walk.
c
1150     continue
c
         ncsf = ncsf + 1
         go to 1100
c        # exit from walk generation.
1180     continue
         last = 1
      return
      end
c**************************************************************
      subroutine sdmatd(symw,nsv,ism,yb,xbar,xp,z,r,ind,
     +  val,icode,hvect,tden,ncsf1,ncsf2,qind)
c
c  construct the symmetric one- and two-particle transition density
c  matrix element contributions for diagonal loops.
c
c  tden(n,ii)  =<mc| eii   |n>
c  tden(n,iill)=<mc| eiill |n>
c  tden(n,ilil)=(1/2)* <mc| eilli |n>
c
c  *note* with this definition of the tden(*) matrix elements, the dot
c  product of tden(*) with hvect(*) gives a d*(*) matrix element of
c  the form <mc| e |mc>.
c
c  the loop values are in val(*), some of which have factors included
c  to facilitate their use in the hamiltonian matrix.
c
c  qind version: 31-may-85
c  symmetry ft version: 18-oct-84
c  programmed by ron shepard.
c
      implicit integer(a-z)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
c
      integer ism(nsv),yb(nsv)
      integer xbar(nsym),xp(nsym),z(nsym)
      real*8 val(3),hvect(ncsf1),tden(ncsf1+ncsf2)
      integer r(*),ind(*)
      logical qind,qd1
      real*8 t,half,two
c
c  account for factors and assign the go-to variable.
c
      iop=icode-1
cdd   go to( 2, 3, 4,5, 6,7),iop
      go to(50,50,50,5,50,7),iop
c
5     continue
c  ll
      t=val(2)
c     assign 1012 to igo
      qd1 = .true.
      go to 50
7     continue
c  ll
      t = val(1)
c     assign 1002 to igo
      qd1 = .true.
c
50    continue
c
      do 1300 isv=1,nsv
      ytb=yb(isv)
      tsym=ism(isv)
      xt=xp(tsym)
      zt=z(tsym)
      hsym=mult(tsym,symw)
      xbh=xbar(hsym)
c
      if(qind)then
      stop 'csf selection case'
c
c  csf selection case.  ind(*) is required.
c
c     do 1200 j=1,xt
c     b=ytb+r(zt+j)
c     bmax=ytb+r(zt+j)+xbh
c100   continue
c     b=b+1
c110   continue
c      if(b.gt.bmax)go to 1200
c      ib=ind(b)
c      if(ib.gt.0)go to igo
c     +  ,(100,1002,1012)
c      b=b-ib
c      go to 110
c
c1002  continue
cdd   tden(ib,1)=tden(ib,1)+hvect(ib)*t
c      go to 100
c1012  continue
cdd   tden(ib,1)=tden(ib,1)+hvect(ib)*t
c      go to 100
c
1200  continue
c
      else
c
c  no csf selection.  ind(i)=i for all i.
c
      if(qd1)then
      do 1230 j=1,xt
      ib=ytb+r(zt+j)
      do 1210 i=1,xbh
1210  tden(ib+i)=tden(ib+i)+hvect(ib+i)*t
1230  continue
      endif
c
      endif
c
1300  continue
c
      return
      end



      subroutine rdhdrt(
     & title,  ndot,   nact,   nrow,
     & ssym,   smult,  nelt,   nela,
     & nwalk,  ncsf,   lenbuf, namomx,
     & buf,ndrt)
c
c  read the header information from the drt file.
c
c  28-sep-91 vrsion=2 support added. -rls
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/cfiles/iunits(25)
      equivalence (iunits(1),nlist)
      equivalence (iunits(9),nslist)
c
      integer iist
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
c
      integer ndot, nact, nrow, ssym, smult, nelt, nela, nwalk, ncsf,
     & lenbuf, namomx
      integer buf(50)
      character*80 title
cmd
      character*4 slabel(20)
cmd
c
      integer vrsion, nhdint, ierr
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      rewind ndrt
c
      read(ndrt, '(a)', iostat=ierr) title
      if ( ierr .ne. 0 ) then
         call bummer('rdhdrt: title ierr=',ierr,faterr)
      endif

      write(nlist,6010)title
      write(nslist,6010)title
6010  format(/,/' DRT HEADER INFORMATION:',3x,a,/)
c
c     # first header record.
      call rddbl( ndrt, 3, buf, 3 )
c
      vrsion = buf(1)
      lenbuf = buf(2)
      nhdint = buf(3)
c
      if ( vrsion .ne. 2 ) then
         write(nlist,*)
     &    '*** warning: rdhead: drt version .ne. 2 *** vrsion=',vrsion
      endif
c
c     # second header record.
      call rddbl( ndrt, lenbuf, buf, nhdint )
c
      ndot  = buf(1)
      nact  = buf(2)
      nsymd = buf(3)
      nrow  = buf(4)
      ssym  = buf(5)
      smult = buf(6)
      nelt  = buf(7)
      nela  = buf(8)
      nwalk = buf(9)
      ncsf  = buf(10)
c
      write(nlist,6100)
     & ndot, nact, nsymd, nrow, ssym, smult,
     & nelt, nela, nwalk, ncsf, lenbuf
      write(nslist,6100)
     & ndot, nact, nsymd, nrow, ssym, smult,
     & nelt, nela, nwalk, ncsf, lenbuf
6100  format(
     & ' ndot=  ',i6,4x,'nact=  ',i6,4x,'nsymd= ',i6,4x,'nrow=  ',i6/
     & ' ssym=  ',i6,4x,'smult= ',i6,4x,'nelt=  ',i6,4x,'nela=  ',i6/
     & ' nwalk= ',i6,4x,'ncsf=  ',i6,4x,'lenbuf=',i6)
c
cmd
      read(ndrt,7000)(slabel(i),i=1,nsym)
      backspace ndrt
      backspace ndrt
cmd
      if ( nact .gt. namomx ) then
         write(nlist,6900)'nact,namomx', nact, namomx
         call bummer('rdhead: nact=',nact,faterr)
      elseif ( nsym .ne. nsymd ) then
         write(nlist,6900)'nsym,nsymd', nsym, nsymd
         call bummer('rdhead: nsymd=',nsymd,faterr)
      endif
c
      return
6900  format(/' error: ',a,2i10)
7000  format(/,20a4)
      end
      subroutine rddrt(
     & ndot,   nrow,   nwalk,  ncsf,
     & lenbuf, buf,    doub,   modrt,
     & syml,   nj,     njskp,  a,
     & b,      l,      y,      xbar,
     & xp,     z,      limvec, r,
     & occl,   drtprt, ndrt)
c
c  read drt arrays.  header info has already been read.  drt file
c  should be positioned correctly to begin reading drt arrays and
c  pointer vectors.
c
c  24-oct-91 buf(*) used to buffer doub(*) and symd(*). -rls
c  28-sep-91 vrsion=2 support added. -rls
c  written by ron shepard.
c
       implicit none
      integer   nmotx
      parameter(nmotx=1023)
      integer      iorbx,         ix0
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer orbtyp(nmotx)
      equivalence (iorbx(1,7),orbtyp(1))
c
      integer      nxy,      mult,     nsym,nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nmpsy(8), nsm(8)
      equivalence (nxy(1,5),nmpsy(1))
      equivalence (nxy(1,6),nsm(1))
      integer               nmot
      equivalence (nxtot(4),nmot)
      integer                nact
      equivalence (nxtot(10),nact)
c
      integer       iunits
      common/cfiles/iunits(25)
      integer                nlist
      equivalence (iunits(1),nlist)
c
      integer iist
c
      integer ndot, nrow, nwalk, ncsf, lenbuf, ndrt
      integer buf(lenbuf)
      integer doub(*)
      integer modrt(0:nact), occl(*)
      integer nj(0:nact), njskp(0:nact), syml(0:nact)
      integer a(nrow), b(nrow), l(0:3,nrow)
      integer y(nsym,0:3,nrow), xbar(nsym,nrow)
      integer xp(nsym,nrow), z(nsym,nrow)
      integer limvec(nwalk), r(nwalk)
      logical drtprt
c
c     # bummer error types.
      integer    wrnerr,   nfterr,   faterr
      parameter( wrnerr=0, nfterr=1, faterr=2)
c
      integer nlevel, b0, i, itemp, ierr, ndotx, nactx, li, j
c
      integer levprt(2)
      integer nx(0:1,0:1),actx(0:5),doubx(0:5),ntypex(0:5)
      data actx/5,5,5,2,5,5/, doubx/0,5,5,1,5,5/
c
c     # nsm(*) has not yet been set...
c     # hack in a fix here.
      itemp = 0
      do 10 i = 1, nsym
         nsm(i) = itemp
         itemp = itemp + nmpsy(i)
10    continue
c
c     # read in the drt arrays.
c
      nlevel = nact + 1
c
c     # slabel(*)...
      call rddbl( ndrt, lenbuf, buf, -nsym )
c
      if ( ndot .gt. 0 ) then
c
c        # doub(*)...
c        # use buf(b0+1:b0+ndot) for scratch.
         b0 = 2 * ndot + 1
         call rddbl( ndrt, lenbuf, buf(b0+1), ndot )
c
c        # symd(*)...
c        # use buf(b0+ndot+1:b0+2*ndot+1) for scratch.
         call rddbl( ndrt, lenbuf, buf(b0+ndot+1), ndot )
c
c        # move into buf(1:2*ndot+1) and convert into doub(*).
         do 20 i = 1, ndot
            buf( 2*(i-1) + 1 ) = buf(b0 + ndot + i)
            buf( 2*(i-1) + 2 ) = buf(b0 + i)
20       continue
         buf( b0 ) = 0
         call symcvt( nsym, 2, buf, nmpsy, nsm, ndotx, 1, doub, ierr )
         if ( ierr .ne. 0 ) then
            call bummer('rddrt: from doub symcvt, ierr=', ierr, faterr )
         elseif ( ndot .ne. ndotx ) then
            call bummer('rddrt: from symcvt, ndotx=', ndotx, faterr )
         endif
      endif
c
c     # modrt(*)...
c     # use a(*) for scratch.
      call rddbl( ndrt, lenbuf, a, nact )
c
c     # syml(*)...
      syml(0)=0
      call rddbl( ndrt, lenbuf, syml(1), nact )
c
c     # move into buf(*) and convert into modrt(*).
      do 30 i = 1, nact
         buf( 2*(i-1) + 1 ) = syml(i)
         buf( 2*(i-1) + 2 ) = a(i)
30    continue
      buf( 2*nact + 1 ) = 0
      modrt(0) = 0
      call symcvt( nsym, 2, buf, nmpsy, nsm, nactx, 1, modrt(1), ierr )
      if ( ierr .ne. 0 ) then
         call bummer('rddrt: from modrt symcvt, ierr=', ierr, faterr )
      elseif ( nact .ne. nactx ) then
         call bummer('rddrt: from symcvt, nactx=', nactx, faterr )
      endif
c
c     # nj(*)...
      call rddbl( ndrt, lenbuf, nj, nlevel )
c
c     # njskp(*)...
      call rddbl( ndrt, lenbuf, njskp, nlevel )
c
c     # a(*)...
      call rddbl( ndrt, lenbuf, a, nrow )
c
c     # b(*)...
      call rddbl( ndrt, lenbuf, b, nrow )
c
c     # l(*)...
      call rddbl( ndrt, lenbuf, l, 4*nrow )
c
c     # y(*,*,*)...
      call rddbl( ndrt, lenbuf, y, 4*nrow*nsym )
c
c     # xbar(*,*)...
      call rddbl( ndrt, lenbuf, xbar, nrow*nsym )
c
c     # xp(*,*)...
      call rddbl( ndrt, lenbuf, xp, nrow*nsym )
c
c     # z(*,*)...
      call rddbl( ndrt, lenbuf, z, nrow*nsym )
c
c     # limvec(*)...
      call rddbl( ndrt, lenbuf, limvec, nwalk )
c
c     # r(*)...
      call rddbl( ndrt, lenbuf, r, nwalk )
c
      write(nlist,6070) 'modrt(*)', (modrt(i), i=1,nact )
c
      if ( ndot .ne. 0 )
     & write(nlist,6070) 'doub(*)', (doub(i), i=1,ndot )
6070  format(/1x,a,(t10,10i4,10x,10i4))
c
      if(.not.drtprt) then
c        # print out the entire drt structure...
         levprt(1)=0
         levprt(2)=nact
         call prtdrt(levprt,nact,nrow,nsym,
     &    nj,njskp,modrt,syml,a,b,l,xbar,y,xp,z)
c
c        # ...and the reverse index array.
         write(nlist,6080) r
      endif
6080  format(/' r(*)',5(t8,10i5,10x,10i5/))
c
c     # change limvec(*) into skip/map form.
c     # on input: 0=deleted walk, 1=retained walk.
c     # on output: i>0 retained csf number i, i<0 then skip -i walks to
c     #            the next retained walk.  e.g.
c     # input:  1  0  1  1  0  0  1  1  1  0  0  0  1 ...
c     # output: 1 -1  2  3 -2 -1  4  5  6 -3 -2 -1  7 ...
c
      nx(0,0)=0
      nx(1,0)=1
      nx(0,1)=ncsf+1
      nx(1,1)=0
      do 2400 i = nwalk, 1, -1
         li=limvec(i)
         nx(0,0)=nx(li,0)-1
         nx(0,1)=nx(0,1)-li
         limvec(i)=nx(0,li)
2400  continue
c
      if ( nx(0,1) .ne. 1 ) then
         write(nlist,*)'rddrt: limvec(*) error'
         call bummer('rddrt: nx(0,1)=',nx(0,1),faterr)
      endif
c
      if ( .not. drtprt ) then
         write(nlist,6090) limvec
      endif
6090  format(/' ind(*)',5(t8,10i5,10x,10i5/))
c
c     # adjust orbtyp(*) for the active orbitals just read in.
c
      do 3100 i=1,nact
         j=modrt(i)
         occl(j)=i
         orbtyp(j)=actx(orbtyp(j))
3100  continue
c
c     # adjust orbtyp(*) for the inactive orbitals.
c
      do 3200 i=1,ndot
         j=doub(i)
         orbtyp(j)=doubx(orbtyp(j))
3200  continue
c
c     # orbtyp(*) is in its final form.  check for consistency.
c     # 0 = frozen core orbital.
c     # 1 = inactive orbital.
c     # 2 = active orbital.
c     # 3 = virtual orbital.
c     # 4 = frozen virtual orbital.
c     # 5 = error.
c
c-    write(nlist,6200)'orbtyp(*)',(orbtyp(i),i=1,nmot)
6200  format(1x,a/(5(1x,10i1)))
      do 3300 i=0,5
         ntypex(i)=0
3300  continue
      do 3400 i=1,nmot
         ntypex(orbtyp(i))=ntypex(orbtyp(i))+1
3400  continue
c-    write(nlist,6110)(i,ntypex(i),i=0,5)
6110  format(/' type:ntype',6(i8,':',i3))
c
      if(   (ntypex(5) .ne. 0)
     & .or. (ntypex(0)+ntypex(1) .ne. ndot)
     & .or. (ntypex(2) .ne. nact)           )then
         write(nlist,*)'rddrt: orbtyp(*) error.'
         call bummer('rddrt: orbtyp(*) error.',0,faterr)
      endif
c
      return
      end

      subroutine prtdrt(levprt,nact,nrow,nsym,
     +  nj,njskp,modrt,syml,a,b,l,xbar,y,xp,z)
c
c  print drt information from levels levprt(1) to levprt(2).
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/cfiles/iunits(25)
      equivalence (iunits(1),nlist)
c
      integer nj(0:nact),njskp(0:nact),modrt(0:nact),syml(0:nact)
      integer l(0:3,nrow),xbar(nsym,nrow),y(nsym,0:3,nrow)
      integer a(nrow),b(nrow)
      integer xp(nsym,nrow),z(nsym,nrow)
      integer levprt(2)
c
6010  format(/' level',i3,' through level',i3,' of the drt:'//
     +  1x,'row',' lev',' a',' b','  mo',' syml',
     +  '  l0 ',' l1 ',' l2 ',' l3 ',
     +  'isym',' xbar ', '  y0  ','  y1  ','  y2  ','  xp  ','   z')
6030  format(/1x,i3,i4,i2,i2,i4,i4,i5,3i4,i4,6i6)
6040  format(1x,36x,i4,6i6)
6050  format(1x,36('.'))
c
c  restrict array references to a meaningful range.
c
      l1=min(levprt(1),levprt(2))
      l2=max(levprt(1),levprt(2))
      l1=min(max(l1,0),nact)
      l2=min(max(l2,0),nact)
c
      write(nlist,6010)l1,l2
c
      do 300 ilev=l2,l1,-1
      ir1=njskp(ilev)+1
      ir2=njskp(ilev)+nj(ilev)
c
      do 200 irow=ir1,ir2
c
      isym=1
      write(nlist,6030)irow,ilev,a(irow),b(irow),
     +  modrt(ilev),syml(ilev),(l(i,irow),i=0,3),
     +  isym,xbar(isym,irow),(y(isym,i,irow),i=0,2),
     +  xp(isym,irow),z(isym,irow)
c
      if(nsym.eq.1)go to 200
      do 100 isym=2,nsym
      write(nlist,6040)
     +  isym,xbar(isym,irow),(y(isym,i,irow),i=0,2),
     +  xp(isym,irow),z(isym,irow)
100   continue
c
200   continue
c
      write(nlist,6050)
300   continue
c
      return
      end

