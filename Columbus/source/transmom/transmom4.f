!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      subroutine rdhint(nlist,nslist,ntape,
     & ntitle,title,repnuc,nbft,nsym,nbpsy,ierr)
c
c  read the header info from the labeled integral file.
c
c  input:
c  nlist = listing file.
c  nslist = optional summary listing file. ignored if < 0.
c  ntape = integral file.
c
c  output:
c  ntitle,title,repnuc,nbft,nsym,nbpsy = integral file info.
c  ierr = iostat for the read statement.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      real*8 repnuc
      character*80 title(*)
      integer nbpsy(8)
c
      real*8    one
      parameter(one=1d0)
c
      do 10 i=1,8
10    nbpsy(i)=0
c
      ntitle=1
      title(1)='no integral file'
      repnuc=-one
      nbft=0
      nsym=8
c
      rewind ntape
c
      read(ntape,iostat=ierr)ntitle,(title(i),i=1,ntitle),repnuc,
     & nbft,nsym,(nbpsy(i),i=1,nsym)
c
      return
      end
      subroutine rdlv(i,n,na,a,alab,qfskip)
c
c  search unit i for label alab and read the corresponding vector.
c
c  i   = unit number.
c  n   = expected vector length.
c  na  = actual vector length.  na=-1 for unsatisfied search.
c  a(*)= if(na.le.n) the vector is returned.
c  alab= character label used to locate the correct record.
c  qfskip= .true. if first record should be skipped.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/cfiles/iunits(25)
      equivalence (iunits(1),nlist)
c
      logical qfskip
      real*8 a(n)
      character*(*) alab
c
      character*8 label
c
c  search for the correct label.
c
      na=0
      rewind i
      if(qfskip)read(i)
10    read(i,end=20)label,na
      if(alab.ne.label)then
         read(i)
         go to 10
      endif
c
c  ...correct label.  check length.
c
      if(na.ne.n)write(nlist,6010)alab,na,n
6010  format(/' rdlv: alab=',a,' na,n=',2i10)
      if(na.le.n)call seqr(i,na,a)
      return
c
c  ...unsatisfied search for alab.
20    write(nlist,6020)alab
6020  format(/' rdlv: record not found. alab=',a)
      na=-1
      return
      end
C*******************************************************************
      subroutine seqr(iunit,n,a)
c
c  sequential read of a(*) from unit iunit.
c
      integer iunit,n
      real*8 a(n)
      read(iunit)a
      return
      end
c
      subroutine add1(modrt,nelmt)
c
c  calculate some of the arrays in /csymb/.
c  assign symmetry labels to basis functions and orbitals.
c  calculate integer pointers for addressing arrays.
c
c  input required:
c  nbpsy(*)  =number of basis functions of each irrep.
c  nmpsy(*)  =number of orbitals of each irrep.
c  orbtyp(*) =orbital type for each orbital (fc=0,d=1,a=2,v=3,fv=4).
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/cfiles/iunits(25)
      equivalence (iunits(1),nlist)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nbpsy(8),nsb(8),nnsb(8),n2sb(8)
      equivalence (nxy(1,1),nbpsy(1))
      equivalence (nxy(1,2),nsb(1))
      equivalence (nxy(1,3),nnsb(1))
      equivalence (nxy(1,4),n2sb(1))
      equivalence (nxtot(1),nbft)
      equivalence (nxtot(2),nntb)
      equivalence (nxtot(3),n2tb)
      integer nmpsy(8),nsm(8),nnsm(8),n2sm(8)
      equivalence (nxy(1,5),nmpsy(1))
      equivalence (nxy(1,6),nsm(1))
      equivalence (nxy(1,7),nnsm(1))
      equivalence (nxy(1,8),n2sm(1))
      equivalence (nxtot(4),nmot)
      equivalence (nxtot(5),nntm)
      equivalence (nxtot(6),n2tm)
      integer ndpsy(8),nsd(8),nnsd(8),n2sd(8)
      equivalence (nxy(1,9),ndpsy(1))
      equivalence (nxy(1,10),nsd(1))
      equivalence (nxy(1,11),nnsd(1))
      equivalence (nxy(1,12),n2sd(1))
      equivalence (nxtot(7),ndot)
      equivalence (nxtot(8),nntd)
      equivalence (nxtot(9),n2td)
      integer napsy(8),nsa(8),nnsa(8),n2sa(8)
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxy(1,14),nsa(1))
      equivalence (nxy(1,15),nnsa(1))
      equivalence (nxy(1,16),n2sa(1))
      equivalence (nxtot(10),nact)
      equivalence (nxtot(11),nnta)
      equivalence (nxtot(12),n2ta)
      integer nvpsy(8),nsv(8),nnsv(8),n2sv(8)
      equivalence (nxy(1,17),nvpsy(1))
      equivalence (nxy(1,18),nsv(1))
      equivalence (nxy(1,19),nnsv(1))
      equivalence (nxy(1,20),n2sv(1))
      equivalence (nxtot(13),nvrt)
      equivalence (nxtot(14),nntv)
      equivalence (nxtot(15),n2tv)
      integer nsad(8),nsvd(8),nsva(8)
      equivalence (nxy(1,29),nsad(1))
      equivalence (nxy(1,30),nsvd(1))
      equivalence (nxy(1,31),nsva(1))
      integer nsbm(8)
      equivalence (nxy(1,32),nsbm(1))
      equivalence (nxtot(16),nbmt)
      equivalence (nxtot(17),ndimd)
      equivalence (nxtot(18),ndima)
      equivalence (nxtot(19),ndimv)
      integer irb1(8),irb2(8)
      equivalence (nxy(1,33),irb1(1))
      equivalence (nxy(1,34),irb2(1))
      integer irm1(8),irm2(8)
      equivalence (nxy(1,35),irm1(1))
      equivalence (nxy(1,36),irm2(1))
      integer ird1(8),ird2(8)
      equivalence (nxy(1,37),ird1(1))
      equivalence (nxy(1,38),ird2(1))
      integer ira1(8),ira2(8)
      equivalence (nxy(1,39),ira1(1))
      equivalence (nxy(1,40),ira2(1))
      integer irv1(8),irv2(8)
      equivalence (nxy(1,41),irv1(1))
      equivalence (nxy(1,42),irv2(1))
c
      common/caddb/addpt(24),numint(12),bukpt(12),intoff(12),szh(15)
c
      parameter(nmotx=1023)
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer symb(nmotx),symm(nmotx),symx(nmotx),invx(nmotx)
      integer orbidx(nmotx),orbtyp(nmotx),nndx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
      equivalence (iorbx(1,2),symb(1))
      equivalence (iorbx(1,3),symm(1))
      equivalence (iorbx(1,4),symx(1))
      equivalence (iorbx(1,5),invx(1))
      equivalence (iorbx(1,6),orbidx(1))
      equivalence (iorbx(1,7),orbtyp(1))
      equivalence (ix0(1),ix0d),(ix0(2),ix0a),(ix0(3),ix0v)
c
      integer ntype(3)
cmd
      integer modrt(0:*)
      integer nelmt
      integer ssym1,ssym2,d1dim
      common /ssym/ ssym1,ssym2,d1dim
      integer symoff
      common /offset/ symoff(36)
cmd
c
c  initialize nndx(*)
c
      nndx(1)=0
      do 10 i=2,nmotx
10    nndx(i)=nndx(i-1)+(i-1)
c
c  calculate ndpsy(*), napsy(*), and nvpsy(*) from orbtyp(*).
c
      i2=0
      do 30 isym=1,8
      ndpsy(isym)=0
      napsy(isym)=0
      nvpsy(isym)=0
      i1=i2+1
      i2=i2+nmpsy(isym)
      do 20 i=i1,i2
      t=orbtyp(i)
      if(t.eq.1)then
         ndpsy(isym)=ndpsy(isym)+1
      elseif(t.eq.2)then
         napsy(isym)=napsy(isym)+1
      elseif(t.eq.3)then
         nvpsy(isym)=nvpsy(isym)+1
      endif
20    continue
30    continue
c
c  fill in arrays in /csymb/.
c
      nbft=0
      nntb=0
      n2tb=0
      nmot=0
      nntm=0
      n2tm=0
      ndot=0
      nntd=0
      n2td=0
      nact=0
      nnta=0
      n2ta=0
      nvrt=0
      nntv=0
      n2tv=0
c
      do 50 i=1,8
c
      nb=nbpsy(i)
      nm=nmpsy(i)
      nd=ndpsy(i)
      na=napsy(i)
      nv=nvpsy(i)
c
      nsb(i)=nbft
      nsm(i)=nmot
      nsd(i)=ndot
      nsa(i)=nact
      nsv(i)=nvrt
      nbft=nbft+nb
      nmot=nmot+nm
      ndot=ndot+nd
      nact=nact+na
      nvrt=nvrt+nv
c
      irb1(i)=nsb(i)+1
      irm1(i)=nsm(i)+1
      ird1(i)=nsd(i)+1
      ira1(i)=nsa(i)+1
      irv1(i)=nsv(i)+1
c
      irb2(i)=nbft
      irm2(i)=nmot
      ird2(i)=ndot
      ira2(i)=nact
      irv2(i)=nvrt
c
      nnsb(i)=nntb
      nnsm(i)=nntm
      nnsd(i)=nntd
      nnsa(i)=nnta
      nnsv(i)=nntv
      nntb=nntb+nndx(nb+1)
      nntm=nntm+nndx(nm+1)
      nntd=nntd+nndx(nd+1)
      nnta=nnta+nndx(na+1)
      nntv=nntv+nndx(nv+1)
c
      n2sb(i)=n2tb
      n2sm(i)=n2tm
      n2sd(i)=n2td
      n2sa(i)=n2ta
      n2sv(i)=n2tv
      n2tb=n2tb+nb**2
      n2tm=n2tm+nm**2
      n2td=n2td+nd**2
      n2ta=n2ta+na**2
      n2tv=n2tv+nv**2
50    continue
c
c  calculate offsets for ad(*),vd(*), and va(*) totally
c  symmetric arrays and orbital coefficients.
c
      nad1=0
      nvd1=0
      nva1=0
      nbmt=0
      do 60 i=1,8
      nsad(i)=nad1
      nsvd(i)=nvd1
      nsva(i)=nva1
      nsbm(i)=nbmt
      nad1=nad1+napsy(i)*ndpsy(i)
      nvd1=nvd1+nvpsy(i)*ndpsy(i)
      nva1=nva1+nvpsy(i)*napsy(i)
      nbmt=nbmt+nbpsy(i)*nmpsy(i)
60    continue
c
c  allocate space for addressing arrays.
c
      ndd=nndx(ndot+1)
      ndd2=ndot*ndot
      nad=nact*ndot
      naa=nndx(nact+1)
      naa2=nact*nact
      nvd=nvrt*ndot
      nva=nvrt*nact
      nvv=nndx(nvrt+1)
      nvv2=nvrt*nvrt
c
c  add**(*) arrays
c
      nptot=1
c  1:dd
      addpt(1)=nptot
      nptot=nptot+ndd
c  2:dd2
      addpt(2)=nptot
      nptot=nptot+ndd2
c  3:ad
      addpt(3)=nptot
      nptot=nptot+nad
c  4:aa
      addpt(4)=nptot
      nptot=nptot+naa
c  5:vd
      addpt(5)=nptot
      nptot=nptot+nvd
c  6:va
      addpt(6)=nptot
      nptot=nptot+nva
c  7:vv
      addpt(7)=nptot
      nptot=nptot+nvv
c  8:vv2
      addpt(8)=nptot
      nptot=nptot+nvv2
c  9:vv3
      addpt(9)=nptot
      nptot=nptot+nvv2
c
c  off*(*) arrays.
c
c  10:off1(pq)
      addpt(10)=nptot
      nptot=nptot+naa
c  11:off2(pq)
      addpt(11)=nptot
      if(nad.ne.0)nptot=nptot+naa
c  12:off3(pq)
      addpt(12)=nptot
      if(nvd.ne.0)nptot=nptot+naa
c  13:off4(q,p)
      addpt(13)=nptot
      if(nvd.ne.0)nptot=nptot+naa2
c  14:off5(pq)
      addpt(14)=nptot
      if(nva.ne.0)nptot=nptot+naa
c  15:off6(pq)
      addpt(15)=nptot
      if(ndd.ne.0)nptot=nptot+naa
c  16:off7(pq)
      addpt(16)=nptot
      if(nvv.ne.0)nptot=nptot+naa
c  17:off8(q,p)
      addpt(17)=nptot
      if(nvv2.ne.0)nptot=nptot+naa
c  18:off9(ai)
      addpt(18)=nptot
      if(nva.ne.0)nptot=nptot+nvd1
c  19:off10(ai)
      addpt(19)=nptot
      if(nad.ne.0)nptot=nptot+nvd1
c  20:off11(ij)
      addpt(20)=nptot
      if(nvv2.ne.0)nptot=nptot+ndd
c  21:off12(pq)
      addpt(21)=nptot
      if(nvv2.ne.0)nptot=nptot+naa
c  22:off13(pq)
      addpt(22)=nptot
      if(ndd2.ne.0)nptot=nptot+naa
c  23:off14(p,q)
      addpt(23)=nptot
      if(nvd.ne.0)nptot=nptot+naa2
c  24:total space required.
      nptot=nptot-1
      addpt(24)=nptot
c
c-    write(nlist,6010)nptot,addpt
c-6010  format(/' space required for addressing arrays:',i8/(1x,8i8))
c
c  assign symmetry labels to basis functions.
c
      do 120 i=1,8
      do 110 j=irb1(i),irb2(i)
      symb(j)=i
110   continue
120   continue
c-    write(nlist,6020)'symb:',(symb(i),i=1,nbft)
c-6020  format(1x,a,(5(1x,10i1)))
c
c  calculate orbital type offsets, ix0*.
c
      ix0d=0
      ix0a=ix0d+ndot
      ix0v=ix0a+nact
c
c  assign symmetry labels to orbitals.
c
      do 140 i=1,8
      do 130 j=ird1(i),ird2(i)
      symx(ix0d+j)=i
130   continue
140   continue
c-    write(nlist,6020)'symd:',(symx(ix0d+i),i=1,ndot)
c
      do 160 i=1,8
      do 150 j=ira1(i),ira2(i)
      symx(ix0a+j)=i
150   continue
160   continue
c-    write(nlist,6020)'syma:',(symx(ix0a+i),i=1,nact)
c
      do 180 i=1,8
      do 170 j=irv1(i),irv2(i)
      symx(ix0v+j)=i
170   continue
180   continue
c-    write(nlist,6020)'symv:',(symx(ix0v+i),i=1,nvrt)
c
      do 200 i=1,8
      do 190 j=irm1(i),irm2(i)
      symm(j)=i
190   continue
200   continue
c-    write(nlist,6020)'symm:',(symm(i),i=1,nmot)
c
c     Calculation of the total size of 1-e density matrix
c      for the case of different space symetry
c
      nelmt = 0
      do lmo=2,nact
      lsym = symm(modrt(lmo))
         do imo=1,lmo-1
         isym = symm(modrt(imo))
            if (mult(lsym,isym).eq.mult(ssym1,ssym2)) then
            nelmt = nelmt + 1
            endif
         enddo
      enddo
c
c     Calculation of offset for the sif routines
c  symoff(*) = symmetry block offsets to use for itypea=1 or itypea=2
c              arrays .  the blocks are offset by
c              symoff( nndxf(isym) + jsym ).
      call izero_wr(36,symoff,1)
      symoff(1) = 1
      icount = 0
      ind = 1
      do isym = 1,nsym
         do jsym = 1,isym
         symoff(ind) = icount
         if (isym.eq.jsym) then
         icount = icount + nbpsy(isym)*(nbpsy(isym)+1)/2
         else
         icount = icount + nbpsy(isym)*nbpsy(jsym)
         endif
         ind = ind + 1
         enddo ! jsym -> isym
      enddo ! isym -> nsym
c
c  calculate orbital mapping and inverse mapping arrays
c  for  inactive, active and virtual orbitals.
c
      ntype(1)=0
      ntype(2)=0
      ntype(3)=0
      do 220 i=1,nmot
      orbidx(i)=0
      t=orbtyp(i)
      if(t.ge.1 .and. t.le.3)then
         ntype(t)=ntype(t)+1
         orbidx(i)=ntype(t)
         ir=i-nsm(symm(i))
         invx(ix0(t)+ntype(t))=ir
      endif
220   continue
c-    write(nlist,6020)'orbtyp:',(orbtyp(i),i=1,nmot)
c-    write(nlist,6030)'orbidx:',(orbidx(i),i=1,nmot)
c-    write(nlist,6030)'invd:',(invx(ix0d+i),i=1,ndot)
c-    write(nlist,6030)'inva:',(invx(ix0a+i),i=1,nact)
c-    write(nlist,6030)'invv:',(invx(ix0v+i),i=1,nvrt)
c-6030  format(1x,a,(5(1x,10i3,3x,10i3/)))
c-    write(nlist,*)' ntype(*)=',ntype
c
c  set diminsions parameters.
c
      ndimd=max(1,ndot)
      ndima=max(1,nact)
      ndimv=max(1,nvrt)
      return
      end

      subroutine read_mo(c)
c
c     read in the MOs from different sources
c
       implicit none
      integer nxy,mult,nsym,nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nbpsy(8),nmpsy(8),nbmt
      equivalence (nxy(1,1),nbpsy(1))
      equivalence (nxy(1,5),nmpsy(1))
      equivalence (nxtot(16),nbmt)
c
      integer nflag
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
c
      integer iunits
      common/cfiles/iunits(25)
      integer nlist,nrestp,mos
      equivalence (iunits(1),nlist)
      equivalence (iunits(12),nrestp)
      equivalence (iunits(19),mos)
c  bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer filerr, syserr, ntit, nsize,i
      character*80 titles(10), afmt
      character*4 labels(8)
c
      integer mosrc,iostat,ierr
      real*8 c(nbmt)

      character*60 fname
c
      if (flags(6)) then

         fname = 'mocoef'
         call trnfln( 1, fname )
         open(unit=mos,file=fname,status='old',
     +    access='sequential',form='formatted',iostat=ierr)
c
         if ( ierr .ne. 0 ) then
            write(nlist,6010) 'inorbc: fname= ' // fname
            call bummer('inorbc: from open, ierr=', ierr, faterr )
        endif
c
         nsize=0
         do i=1,8
           nsize=nsize+nbpsy(i)**2
         enddo
         call moread(mos,10,filerr,syserr,10,ntit,titles,afmt,
     &        nsym,nbpsy,nmpsy,labels,nsize,c)
         call moread(mos,20,filerr,syserr,10,ntit,titles,afmt,
     &        nsym,nbpsy,nmpsy,labels,nsize,c)
c
          close(mos)

      else

         rewind nrestp
         call rdlv(nrestp,nbmt,nbmt,c,'morbn',.true.)

      endif

6010  format(/1x,a)
      return
      end
c*****************************************************************
      subroutine prt_xyz(x,lda)

       implicit none
      integer nxy,mult,nsym,nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nbft
      equivalence (nxtot(1),nbft)
c
      integer iunits
      common/cfiles/iunits(25)
      integer nlist
      equivalence (iunits(1),nlist)
c
      integer lda,i
      real*8 x(lda,3)

      character*30 title(3)


      title(1) = 'X component R matrix'
      title(2) = 'Y component R matrix'
      title(3) = 'Z component R matrix'

      do i=1,3
      call plblks(title(i),x(1,i),1,nbft,' ao',1,nlist)
      enddo
      return
      end
c****************************************************************
c deck sifr1n
      subroutine mdsifr1n(
     & aoints,  info,    itypea,  btypmx,
     & btypes,  buffer,  values,  labels,
     & nsym,    nbpsy,   symoff,  mapin,
     & lda,     array,   fcore,   symb,
     & kntin,   lasta,   lastb,   last,
     & nrec,    ierr )
c
c  read an arbitrary number of 1-e integral arrays.
c
c  this is a basic, no-frills, routine to read several 1-e integral
c  arrays of the same generic type with a single pass through the
c  1-e integral records.  see routines sifr1x() and sifrsh() for
c  examples of the use of this routine.
c
c  input:
c  aoints = input file unit number.
c  info(*) = info array for this file.
c  itypea = generic integral type to be read.
c         >= 0  read the entire set of 1-e records, and leave the
c               file positioned after the last one.
c         = -1, then read the next array, of any type, from the
c               current file position and return.  The file is
c               left positioned at the beginning of the next array.
c  btypmx = maximum value of itypeb to consider.
c  btypes(0:btypmx) = vector of typeb specific arrays to be read.
c                 btypes(i) <= 0       # ignore this array type.
c                 bytpes(i) =  iarray  # accumulate into array(*,iarray)
c                 if itypea=-1, then iarray=1 always and btypes(*) is
c                 not referenced.
c  buffer(1:l1rec) = record buffer.
c  values(1:n1max) = value buffer.
c  labels(1:2,1:n1max) = orbital label buffer.
c  nsym = number of symmetry blocks.
c  nbpsy(*) = number of basis functions per symmetry block.
c  symoff(*) = symmetry block offsets to use for itypea=1 or itypea=2
c              arrays .  the blocks are offset by
c              symoff( nndxf(isym) + jsym ).
c              itypea=0 arrays are addressed by nnskp(*) defined below.
c  mapin(*) = input_ao-to-ao mapping vector.
c  lda  = leading dimension of array(*,*). ( lda > 0 )
c         for fully dense arrays of itypea=1 or itypea=2, lda should be
c         at least (nbft*(nbft+1))/2.  the calling program is
c         responsible for ensuring that lda and symoff(*) are
c         consistent with the arrays that are being read.
c  array(1:lda,1:*) = initial values of the array values.
c               contributions from the 1-e records are accumulated into
c               this array.  The second dimension is determined by the
c               maximum value of the btypes(*) vector elements.
c  fcore(*) = initial values of frozen core energy contributions.
c             fcore(iarray) is associated with array(*,iarray).
c
c  output:
c  array(*,*) = updated array elements.
c  fcore(*) = updated frozen core values.
c  symb(1:nbft) = symmetry index of each basis function
c  kntin( 1 : (nsym*(nsym+1))/2 ) = number of elements read in each
c             symmetry block, referenced as ( nndxf(isym) + jsym ).
c             this array is to allow the calling program to verify that
c             the input arrays have the expected symmetry blocking of
c             elements.
c  lasta, lastb = last itypea and itypeb array values that were
c                   read and used.
c  nrec = total number of 1-e records processed.
c  ierr = error return code.
c       =  0     for normal return.
c       = -1     eof was found on aoints.
c       = -2     unsatisfied search.
c       = -3     symmetry blocking errors were detected.
c       >  0     iostat error while reading aoints.
c
c  24-apr-92 info(*) declaration fixed. -rls
c  08-oct-90 (columbus day) 1-e fcore change. -rls
c  04-oct-90 sifskh() call added. -rls
c  26-jul-89 written by ron shepard.
c
       implicit none
      integer   nipv,   msame,   nmsame,   nomore,   iretbv
      parameter(nipv=2, msame=0, nmsame=1, nomore=2, iretbv=0)
c
      integer  aoints, itypea, btypmx, nsym,   lda,
     & lasta,  lastb,  last,   nrec,   ierr
      integer  info(*),        kntin(*),       nbpsy(nsym),
     & btypes(0:btypmx),       symoff(*),      mapin(*),       symb(*),
     & labels(nipv,*)
      real*8   buffer(*),      values(*),      array(lda,*),   fcore(*)
c
c     # local...
      integer  i,      j,      akeep,  bkeep,  ntot,   nntot,  isym,
     & ifmt,   itypbx, itypax, num,    iarray, ilab,
     & jlab,   jsym,   ijsym,  ij,     nerror, ibvtyp
      integer  nskp(8),        nnskp(8),       idummy(1)
      logical  check(msame:nomore)
      real*8   fcorex
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer nndxf
      nndxf(i) = (i * (i - 1)) / 2
c
      ierr = 0
c
c     # set up nskp(*), nnskp(*), symb(*), and kntin(*)..
c
      ntot  = 0
      nntot = 0
      do 20 isym = 1, nsym
         nskp(isym)  = ntot
         nnskp(isym) = nntot
         ntot        = ntot  + nbpsy(isym)
         nntot       = nntot + nndxf( nbpsy(isym) + 1 )
         do 10 j = (nskp(isym)+1), ntot
            symb(j) = isym
10       continue
20    continue
c
c     # initialize the output symmetry block counter.
      call izero_wr( nndxf(nsym+1), kntin, 1 )
c
c     # position the integral file.
c
      if ( itypea .ge. 0 ) then
         call sifskh( aoints, ierr )
         if ( ierr .ne. 0 ) return
      endif
c
c     # if ( itypea .ge. 0 ) then
c     #    do while ( (last .eq. msame) .or. (last .eq. nmsame) )
c     # else
c     #    do while ( last .eq. msame )
c     # endif
c
      if ( itypea .ge. 0 ) then
         check(msame)  = .true.
         check(nmsame) = .true.
         check(nomore) = .false.
      else
         check(msame)  = .true.
         check(nmsame) = .false.
         check(nomore) = .false.
      endif
c
c     # read the 1-e integral records and accumulate the appropriate
c     # contributions.
c
      nrec  = 0
      akeep = -1
      bkeep = -1
c
      last = msame
100   continue
      if ( check(last) ) then
         call sifrd1(
     &    aoints, info,   nipv,   iretbv,
     &    buffer, num,    last,   itypax,
     &    itypbx, ibvtyp, values,
     &    labels, fcorex, idummy, ierr )
         if ( ierr .ne. 0 ) return
c
         nrec = nrec + 1
c
c        # set iarray to point into array(*,iarray).
c
         if ( itypea .eq. -1 ) then
c           # single array mode. accumulate into array(*,1) always.
            iarray = 1
         elseif ( itypea .eq. itypax ) then
c           # correct generic array type.
c           # mask btypmx with btypes(*) to see if this array should
c           # be accumulated somewhere.
            if ( itypbx .le. btypmx) then
               iarray = btypes(itypbx)
            else
               iarray = -1
            endif
         else
c           # ignore this array.
            iarray = -1
         endif
c
         if ( iarray .gt. 0 ) then
c
c           # a valid array has been found.
            akeep = itypax
            bkeep = itypbx
c
c           # accumulate the values from this record.
c
c           # map the input orbital labels.
            do 120 i = 1, num
               labels(1,i) = mapin( labels(1,i) )
               labels(2,i) = mapin( labels(2,i) )
120         continue
c
            if ( itypax .eq. 0 ) then
c
c              # symmetric matrix, totally symmetric operator.
c
c              # nnskp(1:nsym) offsets are referenced.
c              # diagonal symmetry blocks are returned lower-triangular
c              # packed by rows, asub( nndxf(i) + j ), where asub(*)
c              # begins at nnskp( isym ).
c
               do 140 i = 1, num
                  ilab = max( labels(1,i), labels(2,i) )
                  jlab = min( labels(1,i), labels(2,i) )
                  isym = symb(ilab)
                  jsym = symb(jlab)
                  ijsym = nndxf(isym) + jsym
                  kntin( ijsym ) = kntin( ijsym ) + 1
                  if ( isym .eq. jsym ) then
c                    # only valid array(*) elements are referenced.
                     ij   = nnskp(isym) +
     &                nndxf( ilab - nskp(isym) ) + jlab - nskp(isym)
                     array(ij,iarray) = array(ij,iarray) + values(i)
                  endif
140            continue
            elseif ( itypax .eq. 1 ) then
c
c              # symmetric matrix, nontotally symmetric operator.
c
c              # symoff(1: nndxf(nsym+1) ) is referenced.
c              # diagonal symmetry blocks of array(*,iarray) are
c              # returned lower-triangular packed.
c              # off-diagonal symmetry blocks are returned as
c              # rectangular arrays, asub(1:nbpsy(isym), 1:nbpsy(jsym)),
c              # where asub(*) begins at symoff( nndxf(isym) + jsym ).
c
               do 160 i = 1, num
                  ilab  = max( labels(1,i), labels(2,i) )
                  jlab  = min( labels(1,i), labels(2,i) )
                  isym  = symb(ilab)
                  jsym  = symb(jlab)
                  ijsym = nndxf(isym) + jsym
                  kntin( ijsym ) = kntin( ijsym ) + 1
                  if ( isym .eq. jsym ) then
c                    # diagonal blocks are stored lower-triangle packed.
                     ij = symoff(ijsym) +
     &                nndxf( ilab - nskp(isym) ) + jlab - nskp(isym)
                  else
                     ij = symoff(ijsym) +
     &                ( jlab - nskp(jsym) - 1 ) * nbpsy(isym) +
     &                ilab - nskp(isym)
                  endif
cdd               array(ij,iarray) = array(ij,iarray) + values(i)
                  array(ilab*(ilab-1)/2+jlab,iarray) =
     &             array(ilab*(ilab-1)/2+jlab,iarray) + values(i)
cdd
160            continue
            elseif ( itypax .eq. 2 ) then
c
c              # antisymmetric matrix, nontotally symmetric operator.
c
c              # symoff(1: nndxf(nsym+1) ) is referenced.
c              # diagonal symmetry blocks of array(*,iarray) are
c              # returned lower-triangular packed.
c              # off-diagonal symmetry blocks are returned as
c              # rectangular arrays, asub(1:nbpsy(isym), 1:nbpsy(jsym)),
c              # where asub(*) begins at symoff( nndxf(isym) + jsym ).
c
c              # note that antisymmetric arrays are stored the same way
c              # as symmetric arrays.  In particular, the diagonal
c              # elements, which are zero, are explicitly stored.
c
               do 180 i = 1, num
                  ilab  = labels(1,i)
                  jlab  = labels(2,i)
                  if ( ilab .lt. jlab ) then
                     ilab      = labels(2,i)
                     jlab      = labels(1,i)
c                    # note the sign change upon transposition.
                     values(i) = -values(i)
                  endif
                  isym  = symb(ilab)
                  jsym  = symb(jlab)
                  ijsym = nndxf(isym) + jsym
                  kntin( ijsym ) = kntin( ijsym ) + 1
                  if ( isym .eq. jsym ) then
c                    # diagonal blocks are stored lower-triangle packed.
                     ij = symoff(ijsym) +
     &                nndxf( ilab - nskp(isym) ) + jlab - nskp(isym)
                  else
                     ij = symoff(ijsym) +
     &                ( jlab - nskp(jsym) - 1 ) * nbpsy(isym) +
     &                ilab - nskp(isym)
                  endif
                  array(ij,iarray) = array(ij,iarray) + values(i)
180            continue
            endif
            if ( last .ne. msame ) then
c              # last record of this array.
c              # accumulate the core contribution.
               fcore(iarray) = fcore(iarray) + fcorex
            endif
         endif
         go to 100
      endif
c
      lasta = akeep
      lastb = bkeep
      if ( akeep .eq. -1 ) then
c        # unsatisfied search.
         ierr = -2
      elseif ( akeep .eq. 0 ) then
c        # if the processed array was a totally symmetric operator,
c        # then check for nonzero off-diagonal symmetry block
c        # elements.  if found, then either the integral file is in
c        # error, or there is an inconsistency in mapin(*).
         nerror = 0
         do 240 isym = 2, nsym
            do 220 jsym = 1, (isym-1)
               nerror = nerror + kntin( nndxf(isym) + jsym )
220         continue
240      continue
c        # reset ierr if symmetry errors were detected.
         if ( nerror .ne. 0 )then
            ierr = -3
            return
         endif
      endif
c
      return
      end
C*********************************************************************
C
      subroutine trf_d1(c,d1,cbuf,bufmat,idebug)
C
C
       implicit none
c     commons
c
      integer nxy,mult,nsym,nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nbpsy(8),nbft,nbmt
      equivalence (nxy(1,1),nbpsy(1))
      equivalence (nxtot(1),nbft)
      equivalence (nxtot(16),nbmt)
c
      integer iunits
      common/cfiles/iunits(25)
      integer nlist
      equivalence (iunits(1),nlist)
c
      integer i,ind,icount,isym
      integer j
c
      real*8 c(nbmt),d1(nbft,nbft),cbuf(nbft,nbft),bufmat(nbft,nbft)
c
      logical idebug
c
      call wzero(nbft*nbft,cbuf,1)
      ind = 0
      icount = 1
      do isym=1,nsym
         do i=1,nbpsy(isym)
            do j= 1,nbpsy(isym)
            cbuf(ind+j,ind+i) = c(icount)
            icount = icount + 1
            enddo ! j --> nbpsy(isym)
         enddo ! i --> nbspy(isym)
      ind = ind + nbpsy(isym)
      enddo ! isym --> nsym
c
      if (idebug) then
      call prblkt('big c matrix',cbuf,nbft,nbft,nbft,'bft',' mo',
     & 1,nlist)
      endif
c
      call mxmt(d1,nbft,cbuf,nbft,bufmat,nbft)
c
      call mxm(cbuf,nbft,bufmat,nbft,d1,nbft)
c
c    # temporary write out symmetry blocked density matrix
c
c     open(unit=200,file='trdens',status='unknown')
c     do i=1,nbft
c do j=1,i
c write(200,*) d1(i,j)
c enddo
c     enddo
c     close(200)

c
      return
      end
C*********************************************************************
C
      subroutine transi(r,d1,lda,trans)
C
C
       implicit none
c     commons
c
      integer nxy,mult,nsym,nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nbft
      equivalence (nxtot(1),nbft)
c
      integer iunits
      common/cfiles/iunits(25)
      integer nlist
      equivalence (iunits(1),nlist)
c
      integer nflag
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
c
      integer case
      integer i, ind
      integer j
      integer lda
c
      real*8 r(lda,*),d1(nbft,nbft),trans(3)

c


      call wzero(3,trans,1)
      do case = 1,3
         do i=1,nbft
            do j=1,i
               if (i.eq.j) then
               ind = i*(i+1)/2
               trans(case) = trans(case) + r(ind,case)*d1(i,i)
               else
               ind = i*(i-1)/2 + j
               trans(case) = trans(case) +
     &            r(ind,case)*(d1(i,j)+d1(j,i))
               endif
            enddo ! j --> j
         enddo ! i --> nbft
      enddo ! case --> 3

      return
      end
C*********************************************************************
C*********************************************************************
