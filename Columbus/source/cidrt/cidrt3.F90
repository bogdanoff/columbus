!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
!
!  pointer tables for segment construction and evaluation.
!
!  mu-dependent entries added 7-nov-86 (rls).
!  written 7-sep-84 by ron shepard.
!

      subroutine istack
!
!  initialize necessary stack information for loop construction.
!
       use cidrt_data
       implicit none 
!====>Begin Module ISTACK                 File cidrt3.f                 
!---->Makedcls Options: All variables                                   
!
!     Parameter variables
!
!      INTEGER             NIMOMX
!      PARAMETER           (NIMOMX = 128)
!
      REAL*8              ZERO
      PARAMETER           (ZERO = 0D0)
!
!     Local variables
!
      INTEGER             I
!
!====>End Module   ISTACK                 File cidrt3.f                 
!
      do 100 i = 0, ci_para%nimomx
         cstack%extbk(i) = 0
         cstack%v(1,i)   = zero
         cstack%v(2,i)   = zero
         cstack%v(3,i)   = zero
         cstack%yb(i)    = 0
         cstack%yk(i)    = 0
100   continue
!
      return
      end
      subroutine intab
!
!  initialize tables for segment value calculations.
!
!  for details of segment evaluation and the auxiliary functions,
!  see:
!        i. shavitt, in "the unitary group for the evaluation of
!        electronic energy matrix elements", j. hinze, ed.
!        (springer-verlag, berlin, 1981) p. 51. and i. shavitt,
!        "new methods in computational quantum chemistry and
!        their application on modern super-computers" (annual
!        report to the national aeronautics and space administration),
!        battelle columbus laboratories, june 1979.
!
!  mu-dependent segment evaluation entries added 7-nov-86 (rls).
!  this version written 01-aug-84 by ron shepard.
!
       use cidrt_data
       implicit none 
!====>Begin Module INTAB                  File cidrt3.f                 
!---->Makedcls Options: All variables                                   
!
!     Statement functions
!
      REAL*8              RX,          XA,          XB,          XC
      REAL*8              XD,          XF,          XR
!
!     Parameter variables
!
!      INTEGER             NTAB
!      PARAMETER           (NTAB = 70)
!      INTEGER             MAXB
!      PARAMETER           (MAXB = 19)
!
      REAL*8              ZERO
      PARAMETER           (ZERO = 0D0)
      REAL*8              ONE
      PARAMETER           (ONE = 1D0)
      REAL*8              TWO
      PARAMETER           (TWO = 2D0)
      REAL*8              HALF
      PARAMETER           (HALF = .5D0)
!
!     Local variables
!
      INTEGER             B,           I,           P,           Q
      INTEGER             R,           S
!
      REAL*8              SQRT2,       T
!
!====>End Module   INTAB                  File cidrt3.f                 
!
!  table choice is dictated by the requirement that all segment
!  values are to be calculated with array references only and
!  with no conditional tests or goto's required.  this requires
!  an over-complete set of tables to allow for sign changes and
!  constant factors.
!
!**********************************************************************
!
!  table entry definitions:
!
!   1:0             2:1             3:-1            4:2
!   5:-2            6:sqrt2         7:-t            8:1/b
!   9:-sqrt2/b     10:1/(b+1)      11:-1/(b+1)     12:-1/(b+2)
!  13:-sqrt2/(b+2) 14:a(-1,0)      15:-a(-1,0)     16:a(1,0)
!  17:-a(1,0)      18:a(0,1)       19:-a(0,1)      20:a(2,1)
!  21:-a(2,1)      22:a(1,2)       23:-a(1,2)      24:a(3,2)
!  25:-a(3,2)      26:b(-1,0)      27:b(0,1)       28:-b(0,2)
!  29:b(1,2)       30:b(2,3)       31:c(0)         32:-c(0)
!  33:c(1)         34:c(2)         35:-c(2)        36:d(-1)
!  37:d(0)         38:-d(0)        39:d(1)         40:-d(1)
!  41:d(2)         42:ta(-1,0)     43:-ta(-1,0)    44:ta(1,0)
!  45:ta(2,0)      46:-ta(2,0)     47:-ta(-1,1)    48:ta(0,1)
!  49:ta(2,1)      50:-ta(2,1)     51:ta(3,1)      52:ta(0,2)
!  53:-ta(0,2)     54:ta(1,2)      55:ta(3,2)      56:-ta(3,2)
!         added for mu-dependent segment evaluation
!  57:1/2          58:-1/2         59:-1/b         60:1/(b+2)
!  61:(b+1)/b      62:(b+1)/(b+2)  63:(2b+1)/2b    64:(2b+3)/(2b+4)
!  65:1/2*a(1,0)   66:1/2*a(0,1)   67:1/2*a(2,1)   68:1/2*a(1,2)
!  69:1/2*c(0)     70:1/2*c(2)
!
!**********************************************************************
!
!
!     # auxiliary functions:
!
!     # rx() is a precision-independent integer-to-real conversion
!     # function. -rls
      rx(p)         = (p)
      xa(p,q,b)     = sqrt(rx(b+p) / rx(b+q))
      xb(p,q,b)     = sqrt(two / rx((b+p) * (b+q)))
      xc(p,b)       = sqrt(rx((b+p-1) * (b+p+1))) / rx(b+p)
      xd(p,b)       = sqrt(rx((b+p-1) * (b+p+2)) / rx((b+p) * (b+p+1)))
      xr(p,b)       = one / rx(b+p)
      xf(p,q,r,s,b) = rx(p*b+q) / rx(r*b+s)
!
      sqrt2 = sqrt(two)
      t     = one / sqrt2
!
      do 110 b = 0, ci_para%maxb
         do 100 i = 1, ci_para%ntab
            CTAB%tab(i,b) = zero
100      continue
110   continue
!
      do 200 b = 0, ci_para%maxb
         CTAB%tab( 1,b) = zero
         CTAB%tab( 2,b) = +one
         ctab%tab( 3,b) = -one
         ctab%tab( 4,b) = +two
         ctab%tab( 5,b) = -two
         ctab%tab( 6,b) = +sqrt2
         ctab%tab( 7,b) = -t
         ctab%tab(10,b) = +xr(1,b)
         ctab%tab(11,b) = -xr(1,b)
         ctab%tab(12,b) = -xr(2,b)
         ctab%tab(13,b) = -sqrt2*xr(2,b)
         ctab%tab(18,b) = +xa(0,1,b)
         ctab%tab(19,b) = -xa(0,1,b)
         ctab%tab(20,b) = +xa(2,1,b)
         ctab%tab(21,b) = -xa(2,1,b)
         ctab%tab(22,b) = +xa(1,2,b)
         ctab%tab(23,b) = -xa(1,2,b)
         ctab%tab(24,b) = +xa(3,2,b)
         ctab%tab(25,b) = -xa(3,2,b)
         ctab%tab(29,b) = +xb(1,2,b)
         ctab%tab(30,b) = +xb(2,3,b)
         ctab%tab(34,b) = +xc(2,b)
         ctab%tab(35,b) = -xc(2,b)
         ctab%tab(39,b) = +xd(1,b)
         ctab%tab(40,b) = -xd(1,b)
         ctab%tab(41,b) = +xd(2,b)
         ctab%tab(48,b) = +t * xa(0,1,b)
         ctab%tab(49,b) = +t * xa(2,1,b)
         ctab%tab(50,b) = -t * xa(2,1,b)
         ctab%tab(51,b) = +t * xa(3,1,b)
         ctab%tab(52,b) = +t * xa(0,2,b)
         ctab%tab(53,b) = -t * xa(0,2,b)
         ctab%tab(54,b) = +t * xa(1,2,b)
         ctab%tab(55,b) = +t * xa(3,2,b)
         ctab%tab(56,b) = -t * xa(3,2,b)
         ctab%tab(57,b) = +half
         ctab%tab(58,b) = -half
         ctab%tab(60,b) = +xr(2,b)
         ctab%tab(62,b) = +xf(1,1,1,2,b)
         ctab%tab(64,b) = +xf(2,3,2,4,b)
         ctab%tab(66,b) = +half * xa(0,1,b)
         ctab%tab(67,b) = +half * xa(2,1,b)
         ctab%tab(68,b) = +half * xa(1,2,b)
         ctab%tab(70,b) = +half * xc(2,b)
200   continue
!
      do 300 b = 1, ci_para%maxb
         ctab%tab( 8,b) = +xr(0,b)
         ctab%tab( 9,b) = -sqrt2 * xr(0,b)
         ctab%tab(14,b) = +xa(-1,0,b)
         ctab%tab(15,b) = -xa(-1,0,b)
         ctab%tab(16,b) = +xa(1,0,b)
         ctab%tab(17,b) = -xa(1,0,b)
         ctab%tab(27,b) = +xb(0,1,b)
         ctab%tab(28,b) = -xb(0,2,b)
         ctab%tab(31,b) = +xc(0,b)
         ctab%tab(32,b) = -xc(0,b)
         ctab%tab(33,b) = +xc(1,b)
         ctab%tab(37,b) = +xd(0,b)
         ctab%tab(38,b) = -xd(0,b)
         ctab%tab(42,b) = +t * xa(-1,0,b)
         ctab%tab(43,b) = -t * xa(-1,0,b)
         ctab%tab(44,b) = +t * xa(1,0,b)
         ctab%tab(45,b) = +t * xa(2,0,b)
         ctab%tab(46,b) = -t * xa(2,0,b)
         ctab%tab(47,b) = -t * xa(-1,1,b)
         ctab%tab(59,b) = -xr(0,b)
         ctab%tab(61,b) = +xf(1,1,1,0,b)
         ctab%tab(63,b) = +xf(2,1,2,0,b)
         ctab%tab(65,b) = +half * xa(1,0,b)
         ctab%tab(69,b) = +half * xc(0,b)
300   continue
!
      do 400 b = 2, ci_para%maxb
         ctab%tab(26,b) = +xb(-1,0,b)
         ctab%tab(36,b) = +xd(-1,b)
400   continue
!
      return
      end
      subroutine intd( limvec, ref )
!
!  determine the diagonal interacting csfs.  these are reference csfs
!  of the correct symmetry.
!
!  input:
!  l(*),y(*),xbar(*) = chaining indices.
!  ref(*) = references in skip vector form.
!  wsym(0) = state symmetry.
!
!  output:
!  limvec(*) = updated in 0/1 form.
!
!  06-jun-89  written by ron shepard.
!
       use cidrt_data
       implicit none 
!====>Begin Module INTD                   File cidrt3.f                 
!---->Makedcls Options: All variables                                   
!
!     Parameter variables
!
!
!     Argument variables
!
      INTEGER             LIMVEC(*),   REF(*)
!
!     Local variables
!
      INTEGER             CLEV,        I,           ISTEP,       NLEV
      INTEGER             ROWC,        ROWN
!
!====>End Module   INTD                   File cidrt3.f                 
!
!     # the current version of the calling program only allows
!     # references of the correct symmetry if the interacting space
!     # limitation is imposed.  this routine could be simplified
!     # accordingly.  however, the following code handles the more
!     # general cases, which may be added in the future,  and should be
!     # used unless there are compelling reasons to do otherwise. -rls
!
      do 20 i = 0, cli%niot
         clim%step(i) = 4
20    continue
!
      clev    = 0
      cstack%yb(0)   = 0
      cwsym%wsym(0) = cwsym%ssym
!
!     # begin walk generation...
!
100   continue
!     # decrement the step at the current level.
      nlev = clev+1
      istep = clim%step(clev)-1
      if ( istep .lt. 0 ) then
!        # decrement the current level, and check if done.
         clim%step(clev) = 4
         if ( clev .eq. 0 ) goto 200
         clev = clev-1
         go to 100
      endif
      clim%step(clev) = istep
      rowc = clim%row(clev)
      rown = cdrt%l(istep,rowc,1)
      if ( rown .eq. 0 ) go to 100
      cstack%yb(nlev) = cstack%yb(clev)+cdrt%y(istep,rowc,1)
!     # can the next reference be reached from here?
      if ( ref(cstack%yb(nlev)+1) .ge. cdrt%xbar(rown,1) ) go to 100
      clim%row(nlev) = rown
      cwsym%wsym(nlev) = cmulti%mult( cwsym%wsym(clev),   & 
     &     cdrt%arcsym(istep,clev) )
      if ( nlev .lt. cli%niot ) then
!        # increment to the next level.
         clev = nlev
         go to 100
      else
!        # complete reference walk has been generated.  check
!        # the symmetry and update limvec(*).
         if ( cwsym%wsym(cli%niot) .eq. 1 ) then
            limvec(cstack%yb(cli%niot)+1) = 1
         endif
         clim%step(clev) = 4
         clev = clev-1
         go to 100
      endif
!
!     # all done.
200   continue
!
      return
      end
      subroutine intern2( limvec, ref )
!
!  construct the 2-internal interacting loops.
!
!     code   loop type     description
!     -----  ---------     -------------
!       0  -- new level --
!       1  -- new record --
!
!       2       3b          (xz)
!       3       3a          (wz)
!               10          (wz)
!       4       1ab         (yy)
!               8ab         (yy +diag)
!       5       1b          (yy)
!               8b          (yy +diag)
!       6       2b'         (yy)
!       7       1ab         (xx)
!               8ab         (xx +diag)
!       8       1b          (xx)
!               8b          (xx +diag)
!       9       2b'         (xx)
!      10       1b          (wx)
!               8b          (wx)
!      11       2b'         (wx)
!      12       1a          (ww)
!               8a          (ww +diag)
!
       use cidrt_data
       implicit none 
!====>Begin Module INTERN2                File cidrt3.f                 
!---->Makedcls Options: All variables                                   
!
!     Parameter variables
!
!
!     Argument variables
!
      INTEGER             LIMVEC(*),   REF(*)
!
!     Local variables
!
      INTEGER             KMO,         LMO,         QPRT
!
!====>End Module   INTERN2                File cidrt3.f                 
!
      cli%nintl = 2
      cli%mo(0) = 0
      qprt = 1
!
      do 200 lmo = 1, cli%niot
         do 100 kmo = 1, (lmo-1)
!            if(qprt.ge.5)write(*,6010)lmo,kmo
!***********************************************************************
!           i<l loop types:
!***********************************************************************
            cli%nmo = 2
            cli%mo(1) = kmo
            cli%mo(2) = lmo
!
!           # xz loop 3b:
!
            cli%numv = 1
            call loop( 3, 1, cst%st3, cdb%db3(1,0,2), 8, 4,   & 
     &            limvec, ref )
!
!           # wz loop 3a:
!
            cli%numv = 1
            call loop( 4, 1, cst%st3, cdb%db3, 8, 4, limvec, ref )
100      continue
!***********************************************************************
!        i=l loop types:
!***********************************************************************
!         if(qprt.ge.5)write(*,6010)lmo,lmo
         cli%nmo = 1
         cli%mo(1) = lmo
!
!        # wz loop 10:
!
         cli%numv = 1
         call loop( 4, 1, cst%st10, cdb%db10, 6, 4, limvec, ref )
200   continue
!
      return
6010  format(' intern2:  lmo,kmo = ',2i4)
      end
      subroutine intern3( limvec, ref )
!
!  construct the 3-internal interacting loops.
!
!  code  loop type     description
!  ----  ---------     ----------------
!
!   0  -- new level --
!   1  -- new record --
!   2  -- new vertex --
!
!   3       1ab         (yz,xy,wy)
!           6ab         (yz,xy,wy)
!           8ab         (yz,xy,wy)
!   4       2a'b'       (yz,xy,wy)
!   5       3ab         (yz,xy,wy)
!   6       12bc        (yz,xy,wy)
!   7   (not used)
!   8       2b'         (yz,xy,wy)
!           7'          (yz,xy,wy)
!           8b          (yz,xy,wy)
!           10          (yz,xy,wy)
!   9       1b          (yz,xy,wy)
!           6b          (yz,xy,wy)
!
       use cidrt_data
       implicit none 
!====>Begin Module INTERN3                File cidrt3.f                 
!---->Makedcls Options: All variables                                   
!
!     Parameter variables
!
!
!     Argument variables
!
      INTEGER             LIMVEC(*),   REF(*)
!
!     Local variables
!
      INTEGER             BTAIL,       BTAILX(3),   JMO,         KMO
      INTEGER             KTAIL,       KTAILX(3),   LMO,         QPRT
      INTEGER             VER
!
!====>End Module   INTERN3                File cidrt3.f                 
      data    btailx/2,3,4/, ktailx/1,2,2/
!
      cli%nintl = 3
      cli%mo(0) = 0
      qprt  = 1
      ver   = 1
!
      do 400 lmo = 1, cli%niot
!
         do 200 kmo = 1, (lmo-1)
!
            do 100 jmo = 1, (kmo-1)
!**********************************************************************
!              j<k<l
!**********************************************************************
!               if(qprt.ge.5)write(*,6010)lmo,kmo,jmo
               cli%nmo = 3
               cli%mo(1) = jmo
               cli%mo(2) = kmo
               cli%mo(3) = lmo
!
               btail = btailx(ver)
               ktail = ktailx(ver)
!
!              # 1ab loops:
!
               cli%numv = 2
               call loop( btail, ktail, cst%st1, cdb%db1, 8, 2,   & 
     &        limvec, ref )
!
!              # 2a'b' loops:
!
               cli%numv = 2
               call loop( btail, ktail, cst%st2, cdb%db2, 8, 2,   & 
     &        limvec, ref )
!
!              # 3ab loops:
!
               cli%numv = 2
               call loop( btail, ktail, cst%st3, cdb%db3, 8, 2,   & 
     &        limvec, ref )
100         continue
!**********************************************************************
!           j=k<l
!**********************************************************************
!            if(qprt.ge.5)write(*,6010)lmo,kmo,kmo
            cli%nmo = 2
            cli%mo(1) = kmo
            cli%mo(2) = lmo
!
!           # from ft: do 190 ver=1,3
            btail = btailx(ver)
            ktail = ktailx(ver)
!
!           # 6ab loops:
!
            cli%numv = 2
            call loop( btail, ktail, cst%st6, cdb%db6, 6, 2,   & 
     &     limvec, ref )
!
!           # 7' loops:
!
            cli%numv = 1
            call loop( btail, ktail, cst%st7, cdb%db7, 6, 2,   & 
     &     limvec, ref )
190         continue
200      continue
         do 300 jmo = 1, (lmo-1)
!**********************************************************************
!           j<k=l
!**********************************************************************
!            if(qprt.ge.5)write(*,6010)lmo,lmo,jmo
            cli%nmo = 2
            cli%mo(1) = jmo
            cli%mo(2) = lmo
!
            btail = btailx(ver)
            ktail = ktailx(ver)
!
!           # 8ab loops:
!
            cli%numv = 2
            call loop( btail, ktail, cst%st8, cdb%db8, 6, 2,   & 
     &      limvec, ref )
!
!           # 10 loops:
!
            cli%numv = 1
            call loop( btail, ktail, cst%st10, cdb%db10, 6, 2,   & 
     &     limvec, ref )
!
300      continue
!**********************************************************************
!        j=k=l
!**********************************************************************
!         if(qprt.ge.5)write(*,6010)lmo,lmo,lmo
         cli%nmo = 1
         cli%mo(1) = lmo
!
         btail = btailx(ver)
         ktail = ktailx(ver)
!
!        # 12bc loops:
!
         cli%numv = 2
         call loop( btail, ktail, cst%st12, cdb%db12(1,0,2), 4, 2,   & 
     &   limvec, ref )
!
400   continue
!
      return
6010  format(' intern3:  lmo,kmo,jmo = ',3i4)
      end
      subroutine intern4( limvec, ref )
!
!  construct the 4-internal interacting loops.
!
!  code  loop type      description
!  ----  ---------      ------------
!
!   1  -- new record --
!   2  -- new vertex --
!   3  -- new level --
!
!   4       1ab         (z,y,x,w)
!           4ab         (z,y,x,w)
!           6ab         (z,y,x,w)
!           8ab         (z,y,x,w)
!           12ac        (z,y,x,w)
!   5       2a'b'       (z,y,x,w)
!   6       3ab         (z,y,x,w)
!           12bc        (z,y,x,w)
!   7                   (z,y,x,w)
!   8       2b'         (z,y,x,w)
!           7'          (z,y,x,w)
!           10          (z,y,x,w)
!           11b         (z,y,x,w)
!           13          (z,y,x,w)
!   9       1b          (z,y,x,w)
!           4b          (z,y,x,w)
!           4b'         (z,y,x,w)
!           5           (z,y,x,w)
!           6b          (z,y,x,w)
!           8b          (z,y,x,w)
!           12c         (z,y,x,w)
!  10       12abc       (z,y,x,w)
!
       use cidrt_data
       implicit none 
!====>Begin Module INTERN4                File cidrt3.f                 
!---->Makedcls Options: All variables                                   
!
!     Parameter variables
!
!     Argument variables
!
      INTEGER             LIMVEC(*),   REF(*)
!
!     Local variables
!
      INTEGER             ILEV,        IMO,         JKLSYM,      JLEV
      INTEGER             JMO,         KLEV,        KLSYM,       KMO
      INTEGER             LLEV,        LMO,         LSYM,        QPRT
      INTEGER             TAIL
!
!====>End Module   INTERN4                File cidrt3.f                 
      cli%nintl = 4
      cli%mo(0) = 0
      qprt  = 1
      tail  = 1
!
      do 800 lmo = 2, cli%niot
         llev = lmo-1
         lsym = cdrt%arcsym(1,llev)
!
         do 400 kmo = 1, (lmo-1)
            klev = kmo-1
            klsym = cmulti%mult(cdrt%arcsym(1,klev),lsym)
            do 200 jmo = 1, (kmo-1)
               jlev = jmo-1
               jklsym = cmulti%mult(cdrt%arcsym(1,jlev),klsym)
               do 100 imo = 1, (jmo-1)
                  ilev = imo-1
                  if(cdrt%arcsym(1,ilev).ne.jklsym)go to 100
!***********************************************************
!                 i<j<k<l loops:
!***********************************************************
!                  if(qprt.ge.5)write(*,6010)lmo,kmo,jmo,imo
                  cli%nmo = 4
                  cli%mo(1) = imo
                  cli%mo(2) = jmo
                  cli%mo(3) = kmo
                  cli%mo(4) = lmo
!
!                 # 1ab loops:
!
                  cli%numv = 2
                  call loop( tail, tail, cst%st1, cdb%db1, 8, 0,   & 
     &             limvec, ref )
!
!                 # 2a'b' loops (bra-ket interchange from 2ab):
!
                  cli%numv = 2
                  call loop( tail, tail, cst%st2, cdb%db2, 8, 0, limvec,  & 
     &               ref )
!
!                 # 3ab loops:
!
                  cli%numv = 2
                  call loop( tail, tail, cst%st3, cdb%db3, 8, 0, limvec,  & 
     &             ref )
100            continue
               imo = jmo
               if(klsym.ne.1)go to 200
!***********************************************************
!              i=j<k<l loops:
!***********************************************************
!               if(qprt.ge.5)write(*,6010)lmo,kmo,jmo,imo
               cli%nmo = 3
               cli%mo(1) = jmo
               cli%mo(2) = kmo
               cli%mo(3) = lmo
!
!              # 4ab loops:
!
               cli%numv = 2
               call loop( tail, tail, cst%st4, cdb%db4, 6, 0, limvec,  & 
     &           ref )
!
!              # 4b' loops (bra-ket interchange from 4b):
!
               cli%numv = 1
               call loop( tail, tail, cst%st4p, cdb%db4p, 6, 0, limvec,   & 
     &         ref )
!
!              # 5 loops:
!
               cli%numv = 1
               call loop( tail, tail, cst%st5, cdb%db5, 6, 0, limvec,   & 
     &          ref )
!
!              # from ft: if(tail.lt.4)call wcode(2)
200         continue
            jmo = kmo
            do 300 imo = 1, (jmo-1)
               ilev = imo-1
               if(cdrt%arcsym(1,ilev).ne.lsym)go to 300
!***********************************************************
!              i<j=k<l loops:
!***********************************************************
!               if(qprt.ge.5)write(*,6010)lmo,kmo,jmo,imo
               cli%nmo = 3
               cli%mo(1) = imo
               cli%mo(2) = kmo
               cli%mo(3) = lmo
!
!              # 6ab loops:
!
               cli%numv = 2
               call loop( tail, tail, cst%st6, cdb%db6, 6, 0, limvec,   & 
     &          ref )
!
!              # 7' loops (bra-ket interchange from 7):
!
               cli%numv = 1
               call loop( tail, tail, cst%st7, cdb%db7, 6, 0, limvec,   & 
     &          ref )
300         continue
            imo = jmo
!***********************************************************
!           i=j=k<l loops (deferred until i<j=k=l )
!***********************************************************
400      continue
         kmo = lmo
         do 600 jmo = 1, (kmo-1)
            jlev = jmo-1
            jklsym = cdrt%arcsym(1,jlev)
            do 500 imo = 1, (jmo-1)
               ilev = imo-1
               if(cdrt%arcsym(1,ilev).ne.jklsym)go to 500
!***********************************************************
!              i<j<k=l loops:
!***********************************************************
!               if(qprt.ge.5)write(*,6010)lmo,kmo,jmo,imo
               cli%nmo = 3
               cli%mo(1) = imo
               cli%mo(2) = jmo
               cli%mo(3) = lmo
!
!              # 8ab loops:
!
               cli%numv = 2
               call loop( tail, tail, cst%st8, cdb%db8, 6, 0, limvec,   & 
     &         ref )
!
!              # 10 loops:
!
               cli%numv = 1
               call loop( tail, tail, cst%st10, cdb%db10, 6, 0, limvec,   & 
     &         ref )
500         continue
            imo = jmo
!***********************************************************
!           i=j<k=l loops:
!***********************************************************
!            if(qprt.ge.5)write(*,6010)lmo,kmo,jmo,imo
            cli%nmo = 2
            cli%mo(1) = jmo
            cli%mo(2) = lmo
!
!           # 11b loops:
!
            cli%numv = 1
            call loop(tail, tail, cst%st11, cdb%db11(1,0,2), 4, 0,   & 
     &      limvec, ref)
!
!           # 13 loops:
!
            cli%numv = 1
            call loop( tail, tail, cst%st13, cdb%db13, 4, 0, limvec,   & 
     &       ref )
600      continue
         jmo = kmo
         do 700 imo = 1, (jmo-1)
            ilev = imo-1
            if(cdrt%arcsym(1,ilev).ne.lsym)go to 700
!***********************************************************
!           i<j=k=l loops:
!***********************************************************
!            if(qprt.ge.5)write(*,6010)lmo,kmo,jmo,imo
            cli%nmo = 2
            cli%mo(1) = imo
            cli%mo(2) = lmo
!
!           # 12abc loops:
!
            cli%numv = 3
            call loop( tail, tail, cst%st12, cdb%db12, 4, 0, limvec,   & 
     &      ref )
700      continue
         imo = jmo
!***********************************************************
!        i=j=k=l loops (none for off-diagonal contributions):
!***********************************************************
800   continue
!
      return
6010  format(' intern4:  lmo,kmo,jmo,imo = ',4i4)
      end
      subroutine loop( btail, ktail, stype, db0, nran, ir0,  & 
     & limvec, ref )
!
!  construct interacting loops and accumulate
!  products of segment values.
!
!  input:
!  btail=    bra tail of the loops to be constructed.
!  ktail=    ket tail of the loops to be constructed.
!  stype(*)= segment extension type of the loops.
!  db0(*)=   delb offsets for the vn(*) segment values.
!  nran=     number of ranges for this loop template.
!  ir0=      range offset for this loop.
!  /cpt/     segment value pointer tables.
!  /cex/     segment type extension tables.
!  /ctab/    segment value tables.
!  /cli/     loop information.
!
!  06-jun-89  symmetry checking added. -rls
!  05-jun-89  skip-vector ref(*) version written. -rls
!  written 07-sep-84 by ron shepard.
!
       use cidrt_data
       implicit none 
!====>Begin Module LOOP                   File cidrt3.f                 
!---->Makedcls Options: All variables                                   
!
!     Parameter variables
!
      REAL*8              ZERO
      PARAMETER           (ZERO = 0D0)
      REAL*8              ONE
      PARAMETER           (ONE = 1D0)
      REAL*8              SMALL
      PARAMETER           (SMALL = 1.D-14)
!
!     Argument variables
!
      INTEGER             BTAIL,       DB0(NRAN,0:2,*),          IR0
      INTEGER             KTAIL,       LIMVEC(*),   NRAN,        REF(*)
      INTEGER             STYPE(NRAN)
!
!     Local variables
!
      INTEGER             BBRA,        BCASE,       BKET,        BROWC
      INTEGER             BROWN,       BVER,        CLEV,        DELB
      INTEGER             EXT,         I,           IGONV,       IL
      INTEGER             IMU,         IR,          KCASE,       KROWC
      INTEGER             KROWN,       KVER,        NEXR,        NLEV
      INTEGER             R,           STR
!
      LOGICAL             QBCHK(-4:4)
!
!====>End Module   LOOP                   File cidrt3.f                 
      data qbchk/.true.,.true.,.false.,.false.,.false.,  & 
     &  .false.,.false.,.true.,.true./
!
      bver = min(btail,3)
      kver = min(ktail,3)
!
!     # assign a range (ir1 to nran) for each level of
!     # the current loop type.
!
      cli%hilev = cli%mo(cli%nmo)
!
      ir = ir0
      do 120 i = 1,cli%nmo
         ir = ir+1
         do 110 il = cli%mo(i-1),cli%mo(i)-2
            cstack%range(il) = ir
110      continue
         ir = ir+1
         cstack%range(cli%mo(i)-1) = ir
120   continue
!
!     # initialize the stack arrays for loop construction.
!
      clev    = 0
      cstack%yb(0)   = 0
      cstack%yk(0)   = 0
      cstack%brow(0) = btail
      cstack%krow(0) = ktail
      cstack%qeq(0)  = btail .eq. ktail
      cwsym%wsym(0) = cwsym%ssym
      cstack%v(1,0)  = one
      cstack%v(2,0)  = one
      cstack%v(3,0)  = one
!
!     # one, two, or three segment value products are accumulated.
!
!
!     # begin loop construction:
!
      go to 1100
!
1000  continue
!
!     # decrement the current level, and check if done.
!
      cstack%extbk(clev) = 0
      clev = clev-1
      if ( clev .lt. 0 ) return
!
1100  continue
!
!     # new level:
!
      if ( clev .eq. cli%hilev ) then
         if ( .not. cstack%qeq(cli%hilev) ) then
!           # generate interacting upper walks.
            call intspc( limvec, ref )
         endif
         go to 1000
      endif
!
      nlev = clev+1
      r    = cstack%range(clev)
      str  = stype(r)
      nexr = cex%nex(str)
1200  continue
!
!     # new extension:
!
      ext = cstack%extbk(clev)+1
      if ( ext .gt. nexr ) go to 1000
      cstack%extbk(clev)  = ext
      bcase        = cex%bcasex(ext,str)
      kcase        = cex%kcasex(ext,str)
      clim%brcase(clev) = bcase
      clim%ktcase(clev) = kcase
!
!     # check for extension validity.
!
!     # canonical walk check:
!
      if ( cstack%qeq(clev) .and. (bcase .lt. kcase) ) go to 1200
      cstack%qeq(nlev) = cstack%qeq(clev).and.(bcase.eq.kcase)
!
!     # individual segment check:
!
      browc = cstack%brow(clev)
      brown = cdrt%l(bcase,browc,bver)
      if ( brown .eq. 0 ) go to 1200
      krowc = cstack%krow(clev)
      krown = cdrt%l(kcase,krowc,kver)
      if ( krown .eq. 0 ) go to 1200
      cstack%brow(nlev) = brown
      cstack%krow(nlev) = krown
!
!     # check b values of the bra and ket walks.
!
      bbra = cdrt%b(brown)
      bket = cdrt%b(krown)
      delb = bket-bbra
!
!     # the following check is equivalent to:
!     # if ( abs(delb) .gt. 2 ) go to 1200
!
      if ( qbchk(delb) ) go to 1200
!
      cstack%yb(nlev) = cstack%yb(clev)+cdrt%y(bcase,browc,bver)
      cstack%yk(nlev) = cstack%yk(clev)+cdrt%y(kcase,krowc,kver)
      if ( bver .eq. 1 ) then
!        # z-z loop.  check both bra and ket.
         if (  & 
     &    ref(cstack%yb(nlev)+1) .ge. cdrt%xbar(brown,1) .and.  & 
     &    ref(cstack%yk(nlev)+1) .ge. cdrt%xbar(krown,1)      )   & 
     &    goto 1200
      else
!        # just the ket check.
         if ( ref(cstack%yk(nlev)+1) .ge. cdrt%xbar(krown,1) )   & 
     & goto 1200
      endif
      cwsym%wsym(nlev) = cmulti%mult( cwsym%wsym(clev),   & 
     & cdrt%arcsym(bcase,clev) )
!
!     # accumulate the appropriate number of products:
!
      imu = cdrt%mu(nlev)
      if (cli%numv .eq.1) then 
      cstack%v(1,nlev) = cstack%v(1,clev)*ctab%tab(pt(bcase,kcase,  & 
     & db0(r,imu,1)+delb),bket)
      if ( abs(cstack%v(1,nlev)) .le. small ) go to 1200
      elseif (cli%numv.eq.2) then 
      cstack%v(1,nlev) = cstack%v(1,clev)*ctab%tab(pt(bcase,kcase,  & 
     & db0(r,imu,1)+delb),bket)
      cstack%v(2,nlev) = cstack%v(2,clev)*ctab%tab(pt(bcase,kcase,  & 
     & db0(r,imu,2)+delb),bket)
      if (  & 
     & abs(cstack%v(1,nlev)) .le. small  .and.  & 
     & abs(cstack%v(2,nlev)) .le. small       ) go to 1200
      else
      cstack%v(1,nlev) = cstack%v(1,clev)*ctab%tab(pt(bcase,kcase,  & 
     & db0(r,imu,1)+delb),bket)
      cstack%v(2,nlev) = cstack%v(2,clev)*ctab%tab(pt(bcase,kcase,  & 
     & db0(r,imu,2)+delb),bket)
      cstack%v(3,nlev) = cstack%v(3,clev)*ctab%tab(pt(bcase,kcase,  & 
     & db0(r,imu,3)+delb),bket)
      if (  & 
     & abs(cstack%v(1,nlev)) .le. small  .and.  & 
     & abs(cstack%v(2,nlev)) .le. small  .and.  & 
     & abs(cstack%v(3,nlev)) .le. small       )go to 1200
      endif 
!
!     # made it past all the checks, this is a
!     # valid segment extension, increment to the next level:
!
      clev = nlev
      go to 1100
!
      end
      subroutine intspc( limvec, ref )
!
!  determine the interacting walks from a given loop.
!  for a given loop all valid extensions are constructed.
!
!  on return, interacting walks are indicated by limvec(iwalk)=1
!
!  06-jun-89  symmetry checking added. -rls
!  05-jun-89  skip-vector ref(*) version. -rls
!
       use cidrt_data
       implicit none 
!====>Begin Module INTSPC                 File cidrt3.f                 
!---->Makedcls Options: All variables                                   
!
!     Parameter variables
!
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
!
!     Argument variables
!
      INTEGER             LIMVEC(*),   REF(*)
!
!     Local variables
!
      INTEGER             BRA,         BROWN,       BVER,        CLEV
      INTEGER             I,           ISTEP,       ISYM,        KET
      INTEGER             KROWN,       KVER,        NLEV,        ROWC
!
!====>End Module   INTSPC                 File cidrt3.f                 
!
      brown = 0
      krown = 0
      rowc  = 0
!
      bver = min( cstack%brow(0), 3 )
      kver = min( cstack%krow(0), 3 )
!
      do 10 i = cli%hilev, cli%niot
         clim%step(i) = 4
10    continue
!
!     # compute upper walks starting from brow(hilev) which include
!     # at least one reference csf for either the bra or ket.
!
      clev = cli%hilev
!
!     # check levels to make sure there are upper walks.
!
      if ( cli%hilev .eq. cli%niot ) go to 500
!
      clim%row(clev) = cstack%brow(cli%hilev)
!
100   continue
!     # next step at the current level.
      nlev = clev+1
      istep = clim%step(clev)-1
      if ( istep .lt. 0 ) then
!        # decrement current level.
         clim%step(clev) = 4
         if ( clev .eq. cli%hilev ) return
         clev = clev-1
         go to 100
      endif
      clim%step(clev) = istep
      rowc = clim%row(clev)
      brown = cdrt%l(istep,rowc,bver)
      if ( brown .eq. 0 ) go to 100
      krown = cdrt%l(istep,rowc,kver)
      if ( krown .eq. 0 ) go to 100
      if ( brown .ne. krown ) then
         call bummer('intspc: l(*,*,*) error.',0,faterr)
      endif
      clim%row(nlev) = krown
      cstack%yb(nlev) = cstack%yb(clev)+cdrt%y(istep,rowc,bver)
      cstack%yk(nlev) = cstack%yk(clev)+cdrt%y(istep,rowc,kver)
!
!     # check for possible interacting extensions from this row.
      if ( bver .ne. 1 ) then
         if ( ref(cstack%yk(nlev)+1) .ge. cdrt%xbar(krown,1) )   & 
     & go to 100
      else
!        # z-z loop; check both bra and ket for possibilities.
         if (  & 
     &    ref(cstack%yb(nlev)+1) .ge. cdrt%xbar(brown,1) .and.  & 
     &    ref(cstack%yk(nlev)+1) .ge. cdrt%xbar(krown,1)      )   & 
     & go to 100
      endif
!
      cwsym%wsym(nlev) = cmulti%mult( cwsym%wsym(clev),   & 
     & cdrt%arcsym(istep,clev) )
      if ( nlev .lt. cli%niot ) then
!        # increment to next level
         clev = nlev
         go to 100
      endif
500   continue
!
!     # check for reference interaction...
!
      isym = cwsym%wsym(cli%niot)
      ket = cstack%yk(cli%niot)+1
      if ( ket .gt. cdrt%xbar(1,1) ) then
         call bummer('intspc: ket too large, ket=',ket,faterr)
      endif
      bra = cstack%yb(cli%niot)+1
      if ( ref(ket) .eq. 0 ) then
         if ( clim%nexw(isym,bver) .ne. 0 ) limvec(bra) = 1
      endif
      if ( bver .eq. 1 ) then
         if ( ref(bra) .eq. 0 ) then
            if ( clim%nexw(isym,1) .ne. 0 ) limvec(ket) = 1
         endif
      endif
      go to 100
!
      end
      subroutine print_revision3(name,nlist)
      implicit none
      integer nlist
      character*12 name
      character*70 string
  50  format('* ',a,t12,a,t40,a,t68,' *')
      write(string,50) name(1:len_trim(name)) // '3.F90', &
     &   '$Revision: 1.1.6.2 $','$Date: 2013/04/11 14:37:29 $'
      call substitute(string)
      write(nlist,'(a)') string
      return
      end

