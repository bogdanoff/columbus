!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
module optimizer_input
  use dlf_parameter_module, only: rk
  implicit none
  type optinput_type
    integer :: natoms, nframes, opttype, maxcyc, hessupd, inithess
    integer :: printstep, trustr, lvlprt, fprt, ictyp, state, nstat
    integer :: state_l, state_u, mxs_type, restart, ndump, freeze_tot 
    integer :: ncons
!    for more freezing options freeze_at is now two dimensional array 
!    integer, dimension(:), allocatable :: freeze_at 
     integer, dimension(:,:), allocatable :: freeze_at, icons 
    real(rk) :: maxstp, tolgrad, tolenerg, scalingf, delta_in
    character*20 :: filename
  end type optinput_type
  type(optinput_type), save :: myinput
end module

!
!!######################################################################
!

subroutine read_input()
  use optimizer_input
  implicit none
  integer :: sindex
  character*120 :: line, varname, value
  character*16, parameter :: inp_fn='opt_internal.inp'
  logical :: exists
  integer :: f_status, io_inp, io_geom, freeunit, i, j
#include "common_params.i"
#include "errors.i"
  INQUIRE(file='geom.xyz',exist=exists)
  IF(exists)THEN
    io_geom=freeunit()
    OPEN(UNIT=io_geom, FILE='geom.xyz', STATUS='old', IOSTAT=f_status)
    IF(f_status .NE. 0 )THEN
    print*,"DEBUGMR iostat is ",f_status
      CALL fatal(err_opfl,'geom.xyz','opening geom.xyz')
    END IF
  ELSE
    CALL fatal(err_nofl, 'geom.xyz','file open')
  END IF
  line=''
  READ(io_geom,'(A120)',IOSTAT=f_status) line
  line=trim(adjustl(line))
  sindex=0
  sindex=INDEX(line,' ')
  IF(sindex.ne.0)THEN
    line=trim(adjustl(line(1:sindex-1)))
  END IF
  read(line,'(I10)') myinput%natoms
  PRINT*,"DEBUGMR: natoms is: ",myinput%natoms
  CLOSE(io_geom)

  INQUIRE(file=inp_fn,exist=exists)
  IF(exists)THEN
    io_inp=freeunit()
    OPEN(UNIT=io_inp, FILE=inp_fn, STATUS='old', IOSTAT=f_status)
    IF(f_status .NE. 0 )THEN
      CALL fatal(err_opfl,inp_fn,'opening myinput')
    END IF
  ELSE
    CALL fatal(err_nofl, inp_fn,'file open')
  END IF
  DO
    line=''
    READ(io_inp,'(A120)',IOSTAT=f_status) line
    IF(f_status .ne. 0) THEN
      EXIT
    END IF
    sindex=0
    varname=''
    value=''
    sindex=INDEX(line,'#')
    IF(sindex.ne.0)THEN
      line=line(1:sindex-1)
      sindex=0
    END IF
    sindex=INDEX(line,'=')
    IF(sindex.ne.0)THEN
      varname=trim(adjustl(line(1:sindex-1)))
      call lc(varname)
      value=trim(adjustl(line(sindex+1:)))
      call lc(value)
      sindex=0
    ELSE
      varname=trim(adjustl(line))
      call lc(varname)
      value='blank'
    END IF
    IF(trim(varname)=='nframes')THEN
      read(value,'(I10)') myinput%nframes
    ELSEIF(trim(varname)=='optimizer')THEN
!     don't set if ln-mxs-search is used
      IF(myinput%opttype.ne.40)THEN
        IF(trim(value) == 'sd')THEN
          myinput%opttype=0
        ELSEIF(trim(value) == 'cg')THEN
          myinput%opttype=2
        ELSEIF(trim(value) == 'l-bfgs')THEN
          myinput%opttype=3
        ELSEIF(trim(value) == 'p-rfo')THEN
          myinput%opttype=10
        ELSEIF(trim(value) == 'thermal')THEN
          myinput%opttype=11
        ELSEIF(trim(value) == 'nr')THEN
          myinput%opttype=20
        ELSEIF(trim(value) == 'ln')THEN
          myinput%opttype=40
        ELSE
          write(0,'("Cannot understand optimizer type ",A)') trim(value)
          STOP 1
        END IF
      ELSE
        write(6,*) "ignoring optimizer input, using Lagrange-Newton"
      END IF
    ELSEIF(trim(varname)=='freeze')THEN
!      allocate(myinput%freeze_at(myinput%natoms + 1))
      allocate(myinput%freeze_at(myinput%natoms,2))
!      read(value, *) myinput%freeze_tot, (myinput%freeze_at(i), &
!                     i=1, myinput%freeze_tot)
      read(value, *) myinput%freeze_tot,((myinput%freeze_at(i,j), j=1, 2), &
                     i=1, myinput%freeze_tot)
!      print *, "myinput%freeze_tot ",myinput%freeze_tot,&
!      " myinput%freeze_at(i)", (myinput%freeze_at(i),i=1, myinput%freeze_tot)
      print *, "myinput%freeze_tot ",myinput%freeze_tot,&
      "myinput%freeze_at(i,j)", ((myinput%freeze_at(i,j),j=1, 2), i=1, myinput%freeze_tot)
      ELSEIF(trim(varname)=='constraints')THEN
      read(value, *) myinput%ncons
       ELSEIF(trim(varname)=='constraints1')THEN
      allocate(myinput%icons(5,myinput%ncons))
      read(value, *)((myinput%icons(i,j), i=1, 5), j=1, myinput%ncons)
      print *, "myinput%ncons ",myinput%ncons,&
      "myinput%icons(i,j)", ((myinput%icons(i,j), i=1, 5), j=1, myinput%ncons)
    ELSEIF(trim(varname)=='delta')THEN
      read(value,'(F10.7)') myinput%delta_in !Hessian displacement
    ELSEIF(trim(varname)=='maxcycle')THEN
      read(value,'(I10)') myinput%maxcyc
    ELSEIF(trim(varname)=='hessian update')THEN
      IF(trim(value) == 'calc')THEN
        myinput%hessupd=0
      ELSEIF(trim(value) == 'powell')THEN
        myinput%hessupd=1
      ELSEIF(trim(value) == 'bofill')THEN
        myinput%hessupd=2
      ELSEIF(trim(value) == 'bfgs')THEN
        myinput%hessupd=3
      ELSE
        write(0,'("Cannot understand hessian update type ",A)') trim(value)
        STOP 1
      END IF
    ELSEIF(trim(varname)=='initial hessian')THEN
      IF(trim(value) == 'calc')THEN
        myinput%inithess=0
      ELSEIF(trim(value) == 'one point diff')THEN
        myinput%inithess=1
      ELSEIF(trim(value) == 'two point diff')THEN
        myinput%inithess=2
      ELSEIF(trim(value) == 'diagonal')THEN
        myinput%inithess=3
      ELSEIF(trim(value) == 'identity')THEN
        myinput%inithess=4
      ELSE
        write(0,'("Cannot understand initial hessian type ",A)') trim(value)
        STOP 1
      END IF
    ELSEIF(trim(varname)=='printstep')THEN
      read(value,'(I10)') myinput%printstep
    ELSEIF(trim(varname)=='trust radius')THEN
      IF(trim(value)=='scale')THEN
        myinput%trustr=0
      ELSEIF(trim(value)=='energy based')THEN
        myinput%trustr=1
      ELSEIF(trim(value)=='gradient based')THEN
        myinput%trustr=2
      ELSE
        write(0,'("Cannot understand trust radius type ",A)') trim(value)
        STOP 1
      END IF
    ELSEIF(trim(varname)=='print level')THEN
      read(value,'(I10)') myinput%lvlprt
    ELSEIF(trim(varname)=='file print level')THEN
      read(value,'(I10)') myinput%fprt
    ELSEIF(trim(varname)=='internal coordinates')THEN
      IF(trim(value) == 'cartesian')THEN
        myinput%ictyp=0
      ELSEIF(trim(value) == 'dlc')THEN
        myinput%ictyp=3
      ELSEIF(trim(value) == 'hdlc')THEN
        myinput%ictyp=1
      ELSE
        write(0,'("Cannot understand internal coord type ",A)') trim(value)
        STOP 1
      END IF
      IF(myinput%mxs_type.eq.3)THEN
        myinput%ictyp = 10+mod(myinput%ictyp,10);
      END IF
    ELSEIF(trim(varname)=='max stepsize')THEN
      read(value,'(F10.7)') myinput%maxstp
    ELSEIF(trim(varname)=='grad tolerance')THEN
      read(value,'(F10.7)') myinput%tolgrad
    ELSEIF(trim(varname)=='energy tolerance')THEN
      read(value,'(F10.7)') myinput%tolenerg
    ELSEIF(trim(varname)=='scaling factor')THEN
      read(value,'(F10.7)') myinput%scalingf
    ELSEIF(trim(varname)=='state')THEN
      read(value,'(I10)') myinput%state
      myinput%nstat = myinput%state
    ELSEIF(trim(varname)=='lower state')THEN
      read(value,'(I10)') myinput%state_l
    ELSEIF(trim(varname)=='upper state')THEN
      read(value,'(I10)') myinput%state_u
    ELSEIF(trim(varname)=='mxs_search')THEN
      IF(trim(value)=='penalty function')THEN
        myinput%mxs_type=1
      ELSEIF(trim(value)=='gradient projection')THEN
        myinput%mxs_type=2
      ELSEIF(trim(value)=='lagrange newton')THEN
        myinput%mxs_type=3
      ELSEIF(trim(value)=='none')THEN
        myinput%mxs_type=0
      ELSE
        write(0,'("Cannot understand mxs search type ",A)') trim(value)
      END IF
    ELSEIF(trim(varname)=='ndump')THEN
      read(value,'(I10)') myinput%ndump
    ELSEIF(trim(varname)=='restart')THEN
      myinput%restart=1
    ELSEIF(trim(varname)=='program')THEN
      write(0,*) trim(value)," used"
    ELSEIF(trim(varname)=='memory')THEN
      write(0,*) trim(value),"MB used for Columbus"
    ELSE
      IF(varname /= '')THEN
        write(0,*) "Cannot understand input"
        write(0,'(A,A3,A)') trim(varname)//" = "//trim(value)
        STOP 1
      END IF
    END IF
  END DO
end subroutine

!---------------------------------------------------------------------

SUBROUTINE set_defaults()
  use optimizer_input
  implicit none
!   type optinput_type
!     integer :: natoms, nframes, opttype, maxcyc, hessupd, inithess
!     integer :: printstep, trustr, lvlprt, fprt, ictyp, state, nstat
!     integer :: state_l, state_u, mxs_type, restart, ndump
!     real(rk) :: maxstp, tolgrad, tolenerg, scalingf
!     character*20 :: filename
!   end type optinput_type
! integers
  myinput%natoms=-1       !mark invalid input
  myinput%nframes=0       !for normal optimization
  myinput%opttype=0       !steepest descend
  myinput%maxcyc=100
  myinput%hessupd=2       !Bofill
  myinput%inithess=4      !unit matrix
  myinput%printstep=1
  myinput%trustr=0        !simple scaling as default
  myinput%lvlprt=2
  myinput%fprt=2
  myinput%ictyp=0         !Carthesians
  myinput%state=1
  myinput%nstat=1
  myinput%state_u=2
  myinput%state_l=1
  myinput%mxs_type=0      !no mxs search
  myinput%restart=0
  myinput%ndump=10
! reals
  myinput%maxstp=0.5d0
  myinput%tolgrad=-1      !mark missing input->will be set 450*tolenerg
  myinput%tolenerg=1.0D-6
  myinput%scalingf=1.0
! character
  myinput%filename='geom.xyz' !for NX optimization in XYZ
END SUBROUTINE

!---------------------------------------------------------------------

SUBROUTINE sanity_check()
  use optimizer_input
  implicit none
  IF(myinput%natoms < 0)THEN
    STOP "You have to set the number of atoms"
  END IF
  IF(myinput%tolgrad < 0)THEN
    myinput%tolgrad = 450*myinput%tolenerg
  END IF
  IF(myinput%mxs_type.gt.0)THEN
    IF(myinput%state_l.ge.myinput%state_u)THEN
      STOP "For mxs search you have to give a lower and an upper state"
    END IF
    IF(myinput%state_l.lt.1)THEN
      STOP "The first state is the lowest possible (lstate<1)"
    END IF
    IF(myinput%mxs_type.eq.2)THEN
      IF(myinput%trustr.eq.1)THEN
        STOP "gradient based trust radius impossible with gradient projection"
      END IF
    END IF
    IF(myinput%mxs_type.eq.3)THEN
!     for langrange newton set internal coordinate type
!     to y1X and optimizer to lagrange newton
      IF(((myinput%ictyp-(myinput%ictyp/100))/10).ne.1)THEN
        myinput%ictyp = 10+mod(myinput%ictyp,10);
        write(6,*) 'Using Langrang-Newton internal coordinates'
      END IF
      myinput%opttype=40
      IF(myinput%inithess.eq.0)THEN
        STOP "initial hessian cannot be calc with Lagrange-Newton"
      END IF
    END IF
  END IF
  IF(myinput%state.lt.1)THEN
    STOP "The first state is the lowest possible (state<1)"
  END IF
  IF(myinput%nstat.lt.myinput%state)THEN
    STOP "number of states is lower than state to optimize"
  END IF
END SUBROUTINE

!---------------------------------------------------------------------

SUBROUTINE lc(string)
  character (LEN=*) :: string
  integer :: capdiff, lclim, uclim, lslim, sav, i
  lclim=IACHAR('A')
  uclim=IACHAR('Z')
  lslim=IACHAR('a')
  capdiff=lclim-lslim
  DO i=1,len(string)
    sav=IACHAR(string(i:i))
    IF((sav.ge.lclim).and.(sav.le.uclim))THEN
      string(i:i)=ACHAR(sav-capdiff)
    END IF
  END DO
END SUBROUTINE
