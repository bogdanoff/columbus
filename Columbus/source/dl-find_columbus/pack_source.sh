#!/bin/bash
#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/

make clean
make mrproper

BASEDIR=` pwd|sed 's/\//\\\\\//g' `
sed "s/$BASEDIR/>>INSTALLDIR<</g" optimize.pl > opter.plsrc
# rm -f optimize.pl
sed "s/$BASEDIR/>>INSTALLDIR<</g" compute_col.pl > compcol.plsrc
# rm -f compute_col.pl
sed "s/$BASEDIR/>>INSTALLDIR<</g" geom2xyz > g2xyz.plsrc
# rm -f geom2xyz
sed "s/$BASEDIR/>>INSTALLDIR<</g" xyz2geom > xyz2g.plsrc
# rm -f xyz2geom
