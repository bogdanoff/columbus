#!/bin/bash
#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/

make clean
make all

BASEDIR=` pwd|sed 's/\//\\\\\//g' `
sed "s/>>INSTALLDIR<</$BASEDIR/g" opter.plsrc > optimize.pl
sed "s/>>INSTALLDIR<</$BASEDIR/g" compcol.plsrc > compute_col.pl
sed "s/>>INSTALLDIR<</$BASEDIR/g" g2xyz.plsrc > geom2xyz
sed "s/>>INSTALLDIR<</$BASEDIR/g" xyz2g.plsrc > xyz2geom
chmod 755 optimize.pl compute_col.pl geom2xyz xyz2geom
