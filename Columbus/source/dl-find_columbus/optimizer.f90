!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
program main

use dlf_parameter_module
use optimizer_input

implicit none

! this is the file that does the actual interface from Columbus to
! DL-find.
! I have my own module for reading the input file. This is in
! optimizer_module.f90. Whenever something is changed in the input file
! this has to be adapted!

integer :: nvar ! number of variables
                !  3*nat
integer :: nvar2! number of variables
                !  in the second array (coords2)
  ! nvar2= nframe*nat*3 + nweight + nmass + n_po_scaling
  !    nframe*nat*3    entries of coordinates of nframe structures
  !    nweight         entries of weights (nat or 0)
  !    nmass           entries of atomic masses (nat or 0)
  !    n_po_scaling    entries of radii scaling factors in the parallel
  !                    optimization (0 [meaning all radii set to the base
  !                    value], or a pre-known nivar)
integer :: nsp  ! number of values in the integer
                !  array spec
  ! nsp= nat + nz + 5*ncons + 2*nconn
  !    nat     entries of freezing/residue number
  !    nz      entries of nuclear charges (same order as coords)
  !    5*ncons entries of constraints (typ, atom1,atom2, atom3, atom4)
  !    2*nconn entries of connections (atom1 atom2)

integer :: mstr ! 1 if this task is the master of
                ! a parallel run, 0 otherwise
! logical     :: ext
! integer     :: io_geom, io_grad, filestat
#include "common_params.i"
#include "errors.i"

mstr = 1
! **********************************************************************
print*,"       Begin of master program     "
print*,"-----------------------------------------"

! these routines are in nxopt_module
call set_defaults()
call read_input()
!   type optinput_type
!     integer :: natoms, nframes, opttype, maxcyc, hessupd, inithess
!     integer :: printstep, trustr, lvlprt, fprt, ictyp, state, nstat
!     integer :: state_l, state_u, mxs_type, restart, ndump
!     real(rk) :: maxstp, tolgrad, tolenerg, scalingf, delta_in
!     real(rk) :: freeze, constraints
!     character*20 :: filename
!   end type optinput_type
if(myinput%lvlprt >=2)then
  write(6,*) "----------------------------"
  write(6,*) "input parameters"
  write(6,'(A10," = ",I6)') "restart",myinput%restart
  write(6,'(A10," = ",I6)') "natoms",myinput%natoms
  write(6,'(A10," = ",I6)') "nframes",myinput%nframes
  write(6,'(A10," = ",I3)') "opttype",myinput%opttype
  write(6,'(A10," = ",E12.4)') "delta",myinput%delta_in
  write(6,'(A10," = ",E12.4)') "maxstp",myinput%maxstp
  write(6,'(A10," = ",E12.4)') "tolgrad",myinput%tolgrad
  write(6,'(A10," = ",E12.4)') "tolenerg",myinput%tolenerg
  write(6,'(A10," = ",I6)') "maxcyc",myinput%maxcyc
  write(6,'(A10," = ",I3)') "hessupd",myinput%hessupd
  write(6,'(A10," = ",I3)') "inithess",myinput%inithess
  write(6,'(A10," = ",I6)') "printstep",myinput%printstep
  write(6,'(A10," = ",I6)') "trustr",myinput%trustr
  write(6,'(A10," = ",E12.4)') "scalingf",myinput%scalingf
  write(6,'(A10," = ",I6)') "lvlprt",myinput%lvlprt
  write(6,'(A10," = ",I6)') "ictyp",myinput%ictyp
  write(6,'(A10," = ",I6)') "mxs_type",myinput%mxs_type
  write(6,'(A10," = ",I6)') "ncons",myinput%ncons
  write(6,*) "----------------------------"
end if
call sanity_check()

nvar = 3*myinput%natoms
nvar2 = 3*myinput%natoms*myinput%nframes
nsp = myinput%natoms+5*myinput%ncons
call dl_find(nvar,nvar2,nsp,mstr)

end program main

!
!!######################################################################
!

subroutine dlf_get_params(nvar,nvar2,nspec,coords,coords2,spec,ierr, &
    tolerance,printl,maxcycle,maxene,tatoms,icoord, &
    iopt,iline,maxstep,scalestep,lbfgs_mem,nimage,nebk,dump,restart,&
    nz,ncons,nconn,update,maxupd,delta,soft,inithessian,carthessian,tsrel, &
    maxrot,tolrot,nframe,nmass,nweight,timestep,fric0,fricfac,fricp, &
    imultistate, state_i,state_j,pf_c1,pf_c2,gp_c3,gp_c4,ln_t1,ln_t2, &
    printf,tolerance_e,distort,massweight,minstep,maxdump,task,temperature, &
    po_pop_size,po_radius,po_contraction,po_tolerance_r,po_tolerance_g, &
    po_distribution,po_maxcycle,po_init_pop_size,po_reset,po_mutation_rate, &
    po_death_rate,po_scalefac,po_nsave,ntasks,tdlf_farm,n_po_scaling)

  use dlf_parameter_module
  use optimizer_input

  implicit none

  integer  ,intent(in)    :: nvar ! 3*nat
  integer  ,intent(in)    :: nvar2 ! nframe*nat*3 + nweight + nmass + n_po_scaling
  integer  ,intent(in)    :: nspec ! nat + nz + 5*ncons + 2*nconn
  real(rk) ,intent(inout) :: coords(nvar) ! start coordinates
  real(rk) ,intent(inout) :: coords2(nvar2) ! a real array that can be used
                                            ! depending on the calculation
                                            ! e.g. a second set of coordinates,
                                            ! mass weights, masses, ...
  integer  ,intent(inout) :: spec(nspec) ! specifications like fragment or frozen
  integer  ,intent(out)   :: ierr   ! ierr=0 on success
  real(rk) ,intent(inout) :: tolerance ! main convergence criterion (Max grad comp.)
  integer  ,intent(inout) :: printl ! how verbosely to write info to stdout
  integer  ,intent(inout) :: maxcycle ! maximum number of cycles
  integer  ,intent(inout) :: maxene ! maximum number of E&G evaluations
  integer  ,intent(inout) :: tatoms ! 1 if input contains atoms, something else, if
                                    ! input ontains sequential variables (as the
                                    ! 2D-potentials
  integer  ,intent(inout) :: icoord ! type of internal coordinates
                                    ! icoord%10 =
                                    !  0 .. Cartesians
                                    !  1 .. HDLC internals
                                    !  2 .. HDLC TC
                                    !  3 .. DLC internals
                                    !  4 .. DLC TC
                                    ! icoord-(icoord%10)=
                                    !   10 Lagrange-Newtonconical intersection search
                                    !      not implented yet!
                                    !  100 NEB with endpoints free
                                    !  110 NEB with endpoints moving only
                                    !      perpendicular to their tangent
                                    !      direction
                                    !  120 NEB with frozen endpoints
                                    !  130 NEB with endpoints free, initialization
                                    !      in coordinates X, optimization in
                                    !      carthessian
                                    !  140 -> 130, endpoints moving only perpen
                                    !      -dicular to thier tangent direction
                                    !  150 -> 130, endpoints frozen
                                    !  200 Dimer method, translation and rotation
                                    !      of the dimer are covered by the
                                    !      optimiser (iopt!)
                                    !  210 Dimer method, Rotation of the dimer is
                                    !      done by a line search within the dimer
                                    !  220 Dimer method. Rotation of the dimer is
                                    !      done by a line search, one calculation
                                    !      replaced by interpolation
  integer  ,intent(inout) :: iopt   ! type of optimisation algorithm
                                    !  0 Steepest descent
                                    !  1 Conjugate gradient (Polak-Ribiere)
                                    !    *don't use*
                                    !  2 Conjugate gradient (Polak-Ribiere)
                                    !  3 L-BFGS
                                    ! 10 P-RFO
                                    ! 11 calculate Hessian and do thermal analysis
                                    !    *don't use*
                                    ! 20 Newton-Raphson
                                    ! 30 Damped dynamics
                                    ! 51 Random (stochastic) search
                                    ! 52 Genetic algorithm
  integer  ,intent(inout) :: iline  ! type of line search or trust radius
                                    ! 0 simple scaling of proposed step
                                    ! 1 Trust radius based on energy (L-BFGS reco)
                                    ! 2 Trust radius based on gradient (CG reco)
                                    ! 3 Hard-core line search
                                    !   *don't use*
  real(rk) ,intent(inout) :: maxstep ! maximum length of the step in internals
  real(rk) ,intent(inout) :: scalestep ! constant factor with which to scale the
                                       ! step
  integer  ,intent(inout) :: lbfgs_mem ! number of steps in L-BFGS memory
  integer  ,intent(inout) :: nimage ! Number of images (e.g. in NEB)
  real(rk) ,intent(inout) :: nebk ! force constant for NEB
  integer  ,intent(inout) :: dump ! after how many E&G calculations to dump a
                                  !   checkpoint file?
  integer  ,intent(inout) :: restart ! restart mode: 0 new, 1 read dump file ...
  integer  ,intent(inout) :: nz ! number of entries of nuclear charges
                                ! (same order as coords)
  integer  ,intent(inout) :: ncons ! number of constraints
  integer  ,intent(inout) :: nconn ! number of user provided connections
  integer  ,intent(inout) :: update ! Hessian update scheme
                                    ! 0 no update, always recalculate
                                    ! 1 Powell update
                                    ! 2 Bofill update
                                    ! 3 BFGS update
  integer  ,intent(inout) :: maxupd ! Maximum number of Hessian updates
  real(rk) ,intent(inout) :: delta ! Delta-x in finite-difference Hessian
  real(rk) ,intent(inout) :: soft ! Abs(eigval(hess)) < soft -> ignored in P-RFO
  integer  ,intent(inout) :: inithessian ! Option for method of calculating the
                                         ! initial Hessian
                                         ! 0 Calculate externally using
                                         !   dlf_get_hessian
                                         ! 1 Build by one point finit difference
                                         !   of gradient
                                         ! 2 Build by two point finit difference
                                         !   of gradint
                                         ! 3 Build diagonal Hessian with one
                                         !   point finite difference
                                         ! 4 Use identity matrix
  integer  ,intent(inout) :: carthessian ! Hessian update in cartesians?
                                         ! 0 .. no ?
                                         ! 1 .. yes ?
                                         !   seems not to be used
  integer  ,intent(inout) :: tsrel ! Transition vector I/O absolute or relative?
                                   ! 1 .. yes
                                   ! 0 .. no
  integer  ,intent(inout) :: maxrot ! maximum number of rotations in each DIMER step
  real(rk) ,intent(inout) :: tolrot ! angle tolerance for rotation (deg) in DIMER
  integer  ,intent(inout) :: nframe ! ?? number of frames
  integer  ,intent(inout) :: nmass ! number of entries of atomic masses (nat or 0)
  integer  ,intent(inout) :: nweight ! number of entries of weights (nat or 0)
  real(rk) ,intent(inout) :: timestep ! for damped dynamics
  real(rk) ,intent(inout) :: fric0 ! start friction (damped dynamics)
  real(rk) ,intent(inout) :: fricfac ! factor to reduce friction (<1) whenever
                                        !   the energy is decreasing
  real(rk) ,intent(inout) :: fricp ! friction to use whenever energy increasing
  integer  ,intent(inout) :: imultistate ! type of multistate calculation
                                         ! 0 none
                                         ! 1 Conical intersection optimization
                                         !   (penalty funct)
                                         ! 2 Conical intersection optimization
                                         !   (gradient projection)
                                         ! (3 Conical intersection optimization
                                         !    (Lagrange-Newton) - not implemented)
  integer  ,intent(inout) :: state_i ! lower state
  integer  ,intent(inout) :: state_j ! upper state
  real(rk) ,intent(inout) :: pf_c1 ! penalty function parameter (aka alpha)
  real(rk) ,intent(inout) :: pf_c2 ! penalty function parameter (aka beta)
  real(rk) ,intent(inout) :: gp_c3 ! gradient projection parameter (aka alpha0)
  real(rk) ,intent(inout) :: gp_c4 ! gradient projection parameter (aka alpha1)
  real(rk) ,intent(inout) :: ln_t1 ! Lagrange-Newton orthogonalisation ON
                                   !   threshold
  real(rk) ,intent(inout) :: ln_t2 ! Lagrange-Newton orthogonalisation OFF
                                   !   threshold
  integer  ,intent(inout) :: printf ! how verbosely files should be written
  real(rk) ,intent(inout) :: tolerance_e ! convergence criterion on energy change
  real(rk) ,intent(inout) :: distort ! shift start structure along coords2 (+ or -)
  integer  ,intent(inout) :: massweight ! use mass-weighted coordinates
                                        ! 0 .. no
                                        ! 1 .. yes
  real(rk) ,intent(inout) :: minstep ! Hessian is not updated if step < minstep
  integer  ,intent(inout) :: maxdump ! do only dump restart file after the at most
                                     !   maxdump E&G evaluations
  integer  ,intent(inout) :: task ! number of taks for the task manager
  real(rk) ,intent(inout) :: temperature ! temperature for thermal analysis
  integer  ,intent(inout) :: po_pop_size ! sample population size
  real(rk) ,intent(inout) :: po_radius ! base value for the sample radii
  real(rk) ,intent(inout) :: po_contraction ! factor by which the search radius
                                            ! decreases between search cycles.
                                            ! Cycle = 1 energy eval for each member
                                            ! of the sample population.
  real(rk) ,intent(inout) :: po_tolerance_r ! base value for the radii tolerances
  real(rk) ,intent(inout) :: po_tolerance_g ! convergence criterion: max abs
                                            ! component of g
  integer  ,intent(inout) :: po_distribution ! type of distribn of sample points
                                             ! in space
  integer  ,intent(inout) :: po_maxcycle ! maximum number of cycles
  integer  ,intent(inout) :: po_init_pop_size ! size of initial population
  integer  ,intent(inout) :: po_reset ! number of cycles before population resetting
  real(rk) ,intent(inout) :: po_mutation_rate  ! Fraction of the total number of
                                               ! coordinates in the population to
                                               ! be mutated (randomly shifted) per
                                               ! cycle
  real(rk) ,intent(inout) :: po_death_rate ! Fraction of the population to be
                                           ! replaced by offspring per cycle
  real(rk) ,intent(inout) :: po_scalefac ! Multiplying factor for the absolute
                                         ! gradient vector in the force_bias
                                         ! stoch. search scheme
  integer  ,intent(inout) :: po_nsave ! number of low-energy minima to store
  integer  ,intent(inout) :: ntasks ! number of taskfarms
  integer  ,intent(inout) :: tdlf_farm ! something for mpi ...
  integer  ,intent(inout) :: n_po_scaling ! number of entries of radii scaling
                                          ! factors in the parallel optimization (0
                                          ! [meaning all radii set to the base
                                          ! value], or a pre-known nivar)
  logical     :: ext
  integer     :: io_geom, io_grad, nat, filestat
  integer     :: i, atom, j, k
  character*2 :: symbl
  logical :: exists
  integer :: f_status, io_inp, freeunit
#include "common_params.i"
#include "errors.i"

  ierr = 0
! **********************************************************************

  if(myinput%lvlprt >= 2)then
    print*,"dlf_get_params called"
  end if

  INQUIRE(file=myinput%filename,exist=ext)
  IF(ext)THEN
    io_geom=freeunit()
    OPEN(UNIT=io_geom, FILE=trim(adjustl(myinput%filename)), STATUS='old', IOSTAT=filestat)
    IF(filestat .NE. 0 )THEN
    print*,"DEBUGMR 288 iostat is ",filestat
      CALL fatal(err_opfl,trim(adjustl(myinput%filename)),'opening')
    END IF
  ELSE
    CALL fatal(err_nofl,trim(adjustl(myinput%filename)),'file open')
  END IF
  ! this is for XYZ-files
  READ(io_geom,*) nat
  IF(nat .ne. myinput%natoms) THEN
    STOP "Numbers of atoms don't match in XYZ and input files"
  END IF
  READ(io_geom,*)
  if(myinput%lvlprt >= 2)then
    write(6,*) 'reading coordinates:'
  end if
  DO atom=1,myinput%natoms
    READ(io_geom,*) symbl,(coords(i),i=3*atom-2,3*atom)
    if (myinput%lvlprt >= 2) then
      write(6,'(1X,A2,1X,3(F14.8))') symbl,(coords(i),i=3*atom-2,3*atom)
    end if
  END DO
  CLOSE(io_geom)
! coords are converted from angstrom to bohr
   DO atom=1,myinput%natoms
    DO i=3*atom-2,3*atom
    coords(i)=coords(i)*1.8897259890D0
    ENDDO
    ENDDO
!   coords2 = coords
  coords2(:) = 0

  print *, "myinput%ncons", myinput%ncons

  do i = 1, myinput%natoms+5*myinput%ncons
  spec(i) = 1
  enddo
!  spec(:) = (/-1,0,0,0,0,0,0,0,0,0,0,0,0,0,-1/)
!   spec = (/0,0,1/)
!   spec(:) = myinput%freeze_at(:)
    do i = 1, myinput%freeze_tot
      spec(myinput%freeze_at(i,1)) = myinput%freeze_at(i,2)
    enddo
    
   j=0
   do k = 1, myinput%ncons
   do i = 1, 5
   j = j + 1
   spec(myinput%natoms+j) = myinput%icons(i,k)
   enddo
   enddo

  print *,"spec(:)",spec
  restart = myinput%restart
  icoord = myinput%ictyp
  iopt = myinput%opttype
  delta = myinput%delta_in
  maxstep = myinput%maxstp
  maxcycle = myinput%maxcyc
  tolerance=myinput%tolgrad
  tolerance_e=myinput%tolenerg
  ntasks = 1
  nframe = myinput%nframes
  nmass = 0
  nweight = 0
  n_po_scaling = 0
  inithessian = myinput%inithess
  update = myinput%hessupd
  carthessian = 0
  printl = myinput%lvlprt
  scalestep = myinput%scalingf
  iline = myinput%trustr
  dump = myinput%ndump
  restart = myinput%restart
  imultistate = myinput%mxs_type
  ncons = myinput%ncons

end subroutine dlf_get_params

!----------------------------------------------------------------------

subroutine dlf_get_gradient(nvar,coords,energy,gradient,iimage,status)

  use dlf_parameter_module
  use optimizer_input
#ifdef INTEL
  use IFPORT
#endif

  implicit none

  integer   ,intent(in)    :: nvar
  real(rk)  ,intent(in)    :: coords(nvar)
  real(rk)  ,intent(out)   :: energy
  real(rk)  ,intent(out)   :: gradient(nvar)
  integer   ,intent(in)    :: iimage
  integer   ,intent(out)   :: status

#include "common_params.i"
#include "errors.i"
  logical     :: ext
  integer     :: io_geom, io_grd, io_en, nat, filestat, progstat
  integer     :: i, atom, check
  integer :: freeunit
  real(rk)   , allocatable :: epot(:)
  character*2, allocatable :: symbl(:)
  character*120 :: commandstring
  ALLOCATE(symbl(1:myinput%natoms),STAT=check)
  IF(check .NE. iZERO)THEN
    CALL fatal(err_oom,'symbl', 'allocation')
  END IF
  ALLOCATE(epot(1:myinput%nstat),STAT=check)
  IF(check .NE. iZERO)THEN
    CALL fatal(err_oom,'epot', 'allocation')
  END IF

  INQUIRE(file=myinput%filename,exist=ext)
  IF(ext)THEN
    io_geom=freeunit()
    OPEN(UNIT=io_geom, FILE=trim(adjustl(myinput%filename)), STATUS='old', IOSTAT=filestat)
    IF(filestat .NE. 0 )THEN
      CALL fatal(err_opfl,trim(adjustl(myinput%filename)),'opening')
    END IF
  ELSE
    CALL fatal(err_nofl,trim(adjustl(myinput%filename)),'file open')
  END IF
  ! this is for XYZ-files
  READ(io_geom,*) nat
  IF(nat .ne. myinput%natoms) THEN
    STOP "Numbers of atoms don't match in XYZ and input files"
  END IF
  READ(io_geom,*)
  DO atom=1,myinput%natoms
    READ(io_geom,*) symbl(atom)
  END DO
  CLOSE(io_geom)

  OPEN(UNIT=io_geom, FILE=trim(adjustl(myinput%filename)), STATUS='replace', IOSTAT=filestat)
  IF(filestat .NE. 0 )THEN
    CALL fatal(err_opfl,trim(adjustl(myinput%filename)),'opening')
  END IF
  write(io_geom,'(I6)') myinput%natoms
  write(io_geom,*)
  DO atom=1,myinput%natoms
! write(io_geom,'(1X,A2,1X,3(F14.8))') symbl(atom),(coords(i),i=3*atom-2,3*atom)
! geometry (coords) is written in bohr to anstrom
  write(io_geom,'(1X,A2,1X,3(F14.8))') symbl(atom),(coords(i)*0.529177249d0,i=3*atom-2,3*atom)
  END DO
  CLOSE(io_geom)
  energy = 0.0d0
  gradient(:) = 0.0d0
  status = 0
  commandstring="./compute_gradient.pl "//trim(adjustl(myinput%filename))
  if(myinput%lvlprt >= 4)then
    print*,"issuing command: ",commandstring
  end if
  progstat=system(trim(adjustl(commandstring)))
  IF(progstat .NE. 0 )THEN
    CALL fatal(0,'gradient calc failed','dlf_get_gradient')
  END IF
  INQUIRE(file='energy',exist=ext)
  IF(ext)THEN
    io_en=freeunit()
    OPEN(UNIT=io_en, FILE='energy', STATUS='old', IOSTAT=filestat)
    IF(filestat .NE. 0 )THEN
      CALL fatal(err_opfl,'energy','opening energy')
    END IF
  ELSE
    CALL fatal(err_nofl, 'energy','file open')
  END IF
  DO i=1,myinput%nstat
    READ(io_en,*) epot(i)
  END DO
  CLOSE(io_en)
  energy=epot(myinput%state)

  INQUIRE(file='gradient',exist=ext)
  IF(ext)THEN
    io_grd=freeunit()
    OPEN(UNIT=io_grd, FILE='gradient', STATUS='old', IOSTAT=filestat)
    IF(filestat .NE. 0 )THEN
      CALL fatal(err_opfl,'gradient','opening gradient')
    END IF
  ELSE
    CALL fatal(err_nofl, 'gradient','file open')
  END IF
  DO atom=1,myinput%natoms
    READ(io_grd,*) (gradient(i),i=3*atom-2,3*atom)
  END DO
  if(myinput%lvlprt >= 4)then
    write(6,'("giving gradient to optimizer:")')
    DO atom=1,myinput%natoms
      write(6,'(3(F14.8))') (gradient(i),i=3*atom-2,3*atom)
    END DO
    write(6,'("energy is")')
    write(6,'(F14.8)') energy
  end if
  CLOSE(io_grd)

end subroutine dlf_get_gradient

!----------------------------------------------------------------------

subroutine dlf_get_hessian(nvar,coords,hessian,status)

  use dlf_parameter_module
  use optimizer_input

  implicit none
  integer   ,intent(in)    :: nvar
  real(rk)  ,intent(in)    :: coords(nvar)
  real(rk)  ,intent(inout) :: hessian(nvar,nvar)
  integer   ,intent(out)   :: status
  integer :: i,j
  integer :: freeunit
#include "common_params.i"
#include "errors.i"
! **********************************************************************
  ! only a dummy routine here giving a unit matrix
hessian(:,:) = 0.0d0
do i=1,nvar,1
  hessian(i,i) = 1.0d0
end do
if(myinput%lvlprt >= 6)then
!  write(6,*) "computed hessian"
    DO i=1,nvar,1
    DO j=1,nvar,1
      IF(j.eq.nvar)THEN
        WRITE(6,'(1X,F12.8)',ADVANCE='yes') hessian(i,j)
      ELSE
        WRITE(6,'(1X,F12.8)',ADVANCE='no') hessian(i,j)
      END IF
    END DO
  END DO
end if
status = 0
end subroutine dlf_get_hessian

!----------------------------------------------------------------------

subroutine dlf_put_coords(nvar,mode,energy,coords,iam)

  use dlf_parameter_module
  use optimizer_input
  use dlf_stat, only: stat
  implicit none

  integer   ,intent(in)    :: nvar
  integer   ,intent(in)    :: mode
  integer   ,intent(in)    :: iam
  real(rk)  ,intent(in)    :: energy
  real(rk)  ,intent(in)    :: coords(nvar)

#include "common_params.i"
#include "errors.i"
  logical     :: ext
  integer     :: io_geom, io_en, nat, filestat, check
  integer     :: i, atom, nlength
  integer :: freeunit
  character*2, allocatable :: symbl(:)
  character*20 :: outfile
  ALLOCATE(symbl(1:myinput%natoms),STAT=check)
  IF(check .NE. iZERO)THEN
    CALL fatal(err_oom,'symbl', 'allocation')
  END IF

  INQUIRE(file=myinput%filename,exist=ext)
  IF(ext)THEN
    io_geom=freeunit()
    OPEN(UNIT=io_geom, FILE=trim(adjustl(myinput%filename)), STATUS='old', IOSTAT=filestat)
    IF(filestat .NE. 0 )THEN
      CALL fatal(err_opfl,trim(adjustl(myinput%filename)),'opening')
    END IF
  ELSE
    CALL fatal(err_nofl,trim(adjustl(myinput%filename)),'file open')
  END IF
  ! this is for XYZ-files
  READ(io_geom,*) nat
  IF(nat .ne. myinput%natoms) THEN
    STOP "Numbers of atoms don't match in XYZ and input files"
  END IF
  READ(io_geom,*)
  DO atom=1,myinput%natoms
    READ(io_geom,*) symbl(atom)
  END DO
  CLOSE(io_geom)

  if((mod(stat%ccycle,myinput%printstep).eq.0))then
    OPEN(UNIT=io_geom, FILE='findpath.xyz', POSITION='append', IOSTAT=filestat)
    IF(filestat .NE. 0 )THEN
      CALL fatal(err_opfl,'findpath.xyz','opening')
    END IF
    write(io_geom,'(I6)') myinput%natoms
    write(io_geom,'("cycle:",I8)') stat%ccycle
    if(myinput%lvlprt >= 4) then
      write(6,*) 'Write geometry: ',stat%ccycle
    end if
    DO atom=1,myinput%natoms
!      write(io_geom,'(1X,A2,1X,3(F14.8))') symbl(atom),(coords(i),i=3*atom-2,3*atom)
! geometry (coords) is written from bohr to angstrom
   write(io_geom,'(1X,A2,1X,3(F14.8))') symbl(atom),(coords(i)*0.5291772490d0,i=3*atom-2,3*atom)
      if(myinput%lvlprt >= 4) then
!        write(6,'(1X,A2,1X,3(F14.8))') symbl(atom),(coords(i),i=3*atom-2,3*atom)
! geometry (coords) is written from bohr to angstrom
    write(6,'(1X,A2,1X,3(F14.8))') symbl(atom),(coords(i)*0.5291772490d0,i=3*atom-2,3*atom)
      end if
    END DO
    CLOSE(io_geom)

    myinput%filename=TRIM(ADJUSTL(myinput%filename))
    nlength=LEN_TRIM(ADJUSTL(myinput%filename))-4
    write(outfile,'(A,I5.5,A4)') myinput%filename(1:nlength), stat%ccycle, ".xyz"
    outfile=adjustl(outfile)
    print*,"DEBUGMR: writing geometry to ",outfile
!    OPEN(UNIT=io_geom, FILE=trim(outfile), STATUS='new', IOSTAT=filestat)
     OPEN(UNIT=io_geom, FILE=trim(outfile), STATUS='unknown', IOSTAT=filestat)
    IF(filestat .NE. 0 )THEN
      CALL fatal(err_opfl,trim(outfile),'opening')
    END IF
    write(io_geom,'(I6)') myinput%natoms
    write(io_geom,'("cycle:",I8)') stat%ccycle
    DO atom=1,myinput%natoms
!      write(io_geom,'(1X,A2,1X,3(F14.8))') symbl(atom),(coords(i),i=3*atom-2,3*atom)
! geometry (coords) is written from bohr to angstrom
  write(io_geom,'(1X,A2,1X,3(F14.8))') symbl(atom),(coords(i)*0.529177249d0,i=3*atom-2,3*atom)
    END DO
    CLOSE(io_geom)
  end if

  io_en=freeunit()
  OPEN(UNIT=io_en, FILE='finden.dat', POSITION='append', IOSTAT=filestat)
  IF(filestat .NE. 0 )THEN
    CALL fatal(err_opfl,'finden.dat','opening')
  END IF
  write(io_en,'(E20.8)') energy
  CLOSE(io_en)

end subroutine dlf_put_coords

!----------------------------------------------------------------------

subroutine dlf_error()
  implicit none

  print*,"dlf_error called!"
  STOP 1;
! **********************************************************************
  ! only a dummy routine here.

end subroutine dlf_error

!----------------------------------------------------------------------

subroutine dlf_update()
  implicit none

  print*,"dlf_update called!"
! **********************************************************************
  ! only a dummy routine here.

end subroutine dlf_update
!----------------------------------------------------------------------
subroutine dlf_get_multistate_gradients(nvar,coords,energy,gradient,coupling,needcoupling,iimage,status)

  use dlf_parameter_module
  use optimizer_input
#ifdef INTEL
  use IFPORT
#endif

  implicit none

  integer   ,intent(in)    :: nvar
  real(rk)  ,intent(in)    :: coords(nvar)
  real(rk)  ,intent(out)   :: coupling(nvar)
  real(rk)  ,intent(out)   :: energy(2)
  real(rk)  ,intent(out)   :: gradient(nvar,2)
  integer   ,intent(in)    :: needcoupling
  integer   ,intent(in)    :: iimage
  integer   ,intent(out)   :: status

#include "common_params.i"
#include "errors.i"
  logical     :: ext
  integer     :: io_geom, io_grd, io_en, io_nad, nat, filestat, progstat
  integer     :: i, atom, check, sti, stj
  integer :: freeunit
  real(rk)   , allocatable :: epot(:)
  character*2, allocatable :: symbl(:)
  character*120 :: commandstring
  ALLOCATE(symbl(1:myinput%natoms),STAT=check)
  IF(check .NE. iZERO)THEN
    CALL fatal(err_oom,'symbl', 'allocation')
  END IF
  ALLOCATE(epot(1:myinput%nstat),STAT=check)
  IF(check .NE. iZERO)THEN
    CALL fatal(err_oom,'epot', 'allocation')
  END IF

  INQUIRE(file=myinput%filename,exist=ext)
  IF(ext)THEN
    io_geom=freeunit()
    OPEN(UNIT=io_geom, FILE=trim(adjustl(myinput%filename)), STATUS='old', IOSTAT=filestat)
    IF(filestat .NE. 0 )THEN
    print*,"DEBUGMR iostat is ",filestat
      CALL fatal(err_opfl,trim(adjustl(myinput%filename)),'opening')
    END IF
  ELSE
    CALL fatal(err_nofl,trim(adjustl(myinput%filename)),'file open')
  END IF
  ! this is for XYZ-files
  READ(io_geom,*) nat
  IF(nat .ne. myinput%natoms) THEN
    STOP "Numbers of atoms don't match in XYZ and input files"
  END IF
  READ(io_geom,*)
  DO atom=1,myinput%natoms
    READ(io_geom,*) symbl(atom)
  END DO
  CLOSE(io_geom)

  OPEN(UNIT=io_geom, FILE=trim(adjustl(myinput%filename)), STATUS='replace', IOSTAT=filestat)
  IF(filestat .NE. 0 )THEN
    CALL fatal(err_opfl,trim(adjustl(myinput%filename)),'opening')
  END IF
  write(io_geom,'(I6)') myinput%natoms
  write(io_geom,*)
  DO atom=1,myinput%natoms
    write(io_geom,'(1X,A2,1X,3(F14.8))') symbl(atom),(coords(i),i=3*atom-2,3*atom)
  END DO
  CLOSE(io_geom)
  energy = 0.0d0
  gradient(:,:) = 0.0d0
  coupling(:)= 0.0d0
  status = 0
  commandstring="./compute_gradient.pl "//trim(adjustl(myinput%filename))
  if(myinput%lvlprt >= 4)then
    print*,"issuing command: ",commandstring
  end if
  progstat=system(trim(adjustl(commandstring)))
  IF(progstat .NE. 0 )THEN
    CALL fatal(0,'gradient calc failed','dlf_get_multistate_gradient')
  END IF
  INQUIRE(file='energy',exist=ext)
  IF(ext)THEN
    io_en=freeunit()
    OPEN(UNIT=io_en, FILE='energy', STATUS='old', IOSTAT=filestat)
    IF(filestat .NE. 0 )THEN
      CALL fatal(err_opfl,'energy','opening energy')
    END IF
  ELSE
    CALL fatal(err_nofl, 'energy','file open')
  END IF
  DO i=1,myinput%nstat
    READ(io_en,*) epot(i)
  END DO
  CLOSE(io_en)
  energy(myinput%state_l)=epot(myinput%state_l)
  energy(myinput%state_u)=epot(myinput%state_u)

  INQUIRE(file='gradient.all',exist=ext)
  IF(ext)THEN
    io_grd=freeunit()
    OPEN(UNIT=io_grd, FILE='gradient.all', STATUS='old', IOSTAT=filestat)
    IF(filestat .NE. 0 )THEN
      CALL fatal(err_opfl,'gradient.all','opening gradient')
    END IF
  ELSE
    CALL fatal(err_nofl, 'gradient.all','file open')
  END IF
  DO sti=1,myinput%nstat
    DO atom=1,myinput%natoms
      IF(sti.eq.myinput%state_l)THEN
        READ(io_grd,*) (gradient(i,1),i=3*atom-2,3*atom)
      ELSEIF(sti.eq.myinput%state_u)THEN
        READ(io_grd,*) (gradient(i,2),i=3*atom-2,3*atom)
      ELSE
        READ(io_grd,*)
      END IF
    END DO
  END DO
  if(myinput%lvlprt >= 4)then
    write(6,'("giving gradient to optimizer:")')
    DO sti=1,2
      DO atom=1,myinput%natoms
        write(6,'(3(F14.8))') (gradient(i,sti),i=3*atom-2,3*atom)
      END DO
      write(6,'(42A)') ('-',i=1,42)
    END DO
    write(6,'("energy is")')
    write(6,'(F14.8)') energy
  end if
  CLOSE(io_grd)

  IF(needcoupling.gt.0)THEN
    INQUIRE(file='nad_couplings',exist=ext)
    IF(ext)THEN
      io_nad=freeunit()
      OPEN(UNIT=io_nad, FILE='nad_couplings', STATUS='old', IOSTAT=filestat)
      IF(filestat .NE. 0 )THEN
        CALL fatal(err_opfl,'nad_couplings','opening gradient')
      END IF
    ELSE
      CALL fatal(err_nofl, 'nad_couplings','file open')
    END IF
    DO stj=2,myinput%nstat
      DO sti=1,stj-1
        DO atom=1,myinput%natoms
          IF((sti.eq.myinput%state_l).and.(stj.eq.myinput%state_u))THEN
            READ(io_nad,*) (coupling(i),i=3*atom-2,3*atom)
          ELSE
            READ(io_nad,*)
          END IF
        END DO
      END DO
    END DO
    IF(myinput%lvlprt >= 4)THEN
      write(6,'("giving nad_coupling to optimizer:")')
      DO atom=1,myinput%natoms
        write(6,'(3(F14.8))') (coupling(i),i=3*atom-2,3*atom)
      END DO
    END IF
    CLOSE(io_nad)
  END IF

  status=0

end subroutine dlf_get_multistate_gradients

!----------------------------------------------------------------------
