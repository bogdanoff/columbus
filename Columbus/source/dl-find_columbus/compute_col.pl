#!/usr/bin/perl -w
#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/

#
use strict;
use warnings;
use Cwd;
use lib("/scratch/matruc/dl-find_columbus_ver2.0.1alpha");
use optim_lib;
my (
  $coldir,       $file,       $mdle,   $BASED,        $lvprt,
  $nat,          $state,      $drt,    $coremem,
  $mc,           
    $debug,  $energyf,      $cgrd,
  @line,         $keyw,       $val,    $istep,        $retval,
  @gradient,     $i,          @g,      $energy,       
                          
        $found
);

$debug = 0;

my $installdir    = "/scratch/matruc/dl-find_columbus_ver2.0.1alpha";
my $autoaJ        = 4.359748;
my $BohrtoA       = 0.5291772108;
my $aupBohrtoaJpA = $autoaJ / $BohrtoA;

$coldir = $ENV{"COLUMBUS"};

$file    = shift(@ARGV);
$mdle    = "gradcalc\n";
$BASED   = &getcwd();
$energyf = "LISTINGS/energy";
$cgrd    = "WORK/cartgrd";
$mc      = "WORK/mcscfsm";

# delete gdiis and abacus keywords from control.run
$found=&searchkeyword("control.run","gdiis");
if($found)
{
  &deletekeyword("control.run","control.run","gdiis");
  print_STDOUT("delted gdiis keyword from control.run\n");
}
$found=&searchkeyword("control.run","abacus");
if($found)
{
  &deletekeyword("control.run","control.run","abacus");
  print_STDOUT("delted abacus keyword from control.run\n");
}


# read input - set some defaults
$coremem = 1024;    # 1024MB
$lvprt   = 1;

open( INP, "opt_internal.inp" ) or die "cannot read opt_internal.inp\n$!\n";
while (<INP>)
{
  chomp;
  s/^\s+//;
  s/\s+=/=/;
  s/=\s+/=/;
  if( (not /^$/) and (not /^#/) )
  {
    ( $keyw, $val ) = split /=/;
    if ( lc($keyw) eq 'memory' )
    {
      $coremem = $val;
    }
    elsif ( lc($keyw) eq 'print level' )
    {
      $lvprt = $val;
      if($lvprt > 4) {$debug = 1;}
    }
  }
}
close(INP);

# check if there is only one gradient set in transmomin
# at the moment no NAD couplings and no MXS search

if ( -f "transmomin" )
{
  open( TMIN, "transmomin" ) or die "cannot read transmomin\n$!\n";
  <TMIN>;
  $_ = <TMIN>;
  chomp;
  s/^\s+//;
  @line = split /\s+/;
  if ( $line[0] != $line[2] )
  {
    die "only geometry optimization allowed, no NAD\n";
  }

  if ( $line[1] != $line[3] )
  {
    die "only geometry optimization allowed, no NAD\n";
  }

  $_ = <TMIN>;
  if ( defined($_) ) {
    die "you can only specify one gradient in transmomin\n";
  }
} ## end if ( -f "transmomin" )

# number of atoms is set automatically from geom
$nat = &number_of_atoms();
if ( $nat == 0 ) { die "cannot count atoms in geom\n"; }

# I use a separate file for counting the cycles
# not sure if runc messes with the curr_iter
# curr_iter is overwritten - hopefully this will make Columbus
# write the geoms correctly numbered to GEOM/
$istep = 0;
if ( -f "cycle" )
{
  open( CYC, "cycle" ) or die "cannot read cycle\n$!\n";
  $_ = <CYC>;
  chomp;
  s/\s+//g;
  $istep = $_;
  close CYC;
  $istep++;
}

open( CYC, ">cycle" ) or die "cannot write cycle\n$!\n";
print CYC "$istep\n";
close CYC;
open( CYC, ">curr_iter" ) or die "cannot write iter\n$!\n";
print CYC "$istep\n";
close CYC;

if($debug)
{
  system("cat $file >> $file.hist");
}

$retval = callprog( $installdir, "xyz2geom $file", $mdle );
if ( $retval != 0 )
{
  die "$mdle is dying now\n";
}

print STDOUT "------------------------\nenergy and gradient calculation\n";
if($debug)
{
  system("cat geom >> geom.hist; echo '' >> geom.hist");
}

# Output from runc is on STDOUT and in runls
$retval = callprog( $coldir, "runc -m $coremem|tee runls", $mdle );

print STDOUT "------------------------\nEnd of COLUMBUS calculation\n";

if ( $retval != 0 )
{
  die "$mdle is dying now\n";
}

print "\n $mdle //// ENERG \n";
read_write_energy();

print "\n $mdle //// GRAD \n";
read_write_grad();

# print "\n $mdle //// NONAD \n";
# if($state_u > $state_l)
# {
#   read_write_nonad();
# }

if($debug)
{
  system("cat grad >> grad.hist; echo '' >> grad.hist");
}
open( GRD, "grad" ) or die "cannot read grad\n";
open( GRAD, ">gradient" ) or die "cannot write gradient\n$!\n";
if($debug)
{
  print STDOUT "gradient (aJ/Ang):\n";
}
while (<GRD>)
{
  chomp;
  s/^\s+//;
  s/D/E/g;
  @line        = split /\s+/;
  $gradient[0] = $line[0] * $aupBohrtoaJpA;
  $gradient[1] = $line[1] * $aupBohrtoaJpA;
  $gradient[2] = $line[2] * $aupBohrtoaJpA;

# simple test - set the gradient ZERO - nothing should happen
#   $gradient[0] = $line[0] * $aupBohrtoaJpA*0;
#   $gradient[1] = $line[1] * $aupBohrtoaJpA*0;
#   $gradient[2] = $line[2] * $aupBohrtoaJpA*0;
  printf GRAD "% 14.8f  % 14.8f  % 14.8f\n", $gradient[0], $gradient[1],
    $gradient[2];
  if($debug)
  {
    printf STDOUT "G: % 14.8f  % 14.8f  % 14.8f\n", $gradient[0], $gradient[1],
    $gradient[2];
  }
}
close GRD;
close GRAD;

if ($debug)
{
  system("cp GRADIENTS/cartgrd.all DEBUG/cartgrd.$istep");
  system("cp LISTINGS/energy DEBUG/energy.$istep");
}

# use the previous mocoef
# delete scf keyword
if(-f "MOCOEFS/mocoef_mc.sp")
{
  system("cp MOCOEFS/mocoef_mc.sp ./mocoef");
  print_STDOUT("Using mocoef from previous step\n");
  $found=&searchkeyword("control.run","scf");
  if($found)
  {
    &deletekeyword("control.run","control.run","scf");
    print_STDOUT("Deleted scf keyword from control.run\n");
  }
}

#-----------------------------------------------------------------------

sub read_write_grad {

# Read and write gradient file
# Get gradient from COLUMBUS WORK/cartgrd
  if ( $debug ) { print_STDOUT("Gradient (a.u.): \n"); }
  if ( !-s $cgrd ) { die "ERROR: file $cgrd does not exist or has zero size."; }
  open( CGD, $cgrd )   or die "Cannot open $cgrd!";
  open( GD,  ">grad" ) or die "Cannot open grad!";
  $i = 0;
  while (<CGD>)
  {
    print GD $_;
    if ( $debug )
    {
      chomp;
      $_ =~ s/^\s*//;
      $_ =~ s/\s*$//;
      $_ =~ s/d/E/gi;
      @g = split( /\s+/, $_ );
      printf_STDOUT( "%16.7E %16.7E %16.7E\n", @g );
    }
  }
  close(CGD);
  close(GD);
} ## end sub read_write_grad

#
#-----------------------------------------------------------------------------------
#
sub read_write_energy {

# Read and write energies
# Get energies from COLUMBUS output LISTING/energy
# This should be the same for all methods
  open( EN,   $energyf )  or die "cannot read $energyf\n$!\n";
  open( ENRG, ">energy" ) or die "cannot write energy\n$!\n";
  print STDOUT "energy:\n";
  while (<EN>)
  {
    chomp;
    s/\s+//g;
    printf_STDOUT( "% 14.8f a.u.\n", $_ );
    $energy = $_ * $autoaJ;
    printf ENRG "% 14.8f\n", $energy;
    printf_STDOUT( "% 14.8f aJ\n", $energy );
  }
  close EN;
  close ENRG;

  print STDOUT "---------------------------------\n";

#  columbus_info();
} ## end sub read_write_energy
