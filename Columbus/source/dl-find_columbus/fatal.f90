!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
SUBROUTINE fatal(errn, text, where)
  include 'errors.i'
  include 'common_params.i'
  INTEGER :: errn
  CHARACTER(LEN=*) :: where, text
  WRITE(0,'(A33,I3)') "encountered fatal error number: ",errn
  SELECT CASE (errn)
    CASE (err_nounit)
      WRITE(0,*) "No I/O-unit free in ",where
    CASE (err_oom)
      WRITE(0,*) "Out of Memory while allocating ",text," in ",where
    CASE (err_deal)
      WRITE(0,*) "deallocation failed for ",text," in ",where
    CASE (err_nofl)
      WRITE(0,*) "file not found: ",text," in ",where
    CASE (err_opfl)
      WRITE(0,*) "could not open file: ",text," in ",where
    CASE DEFAULT
      WRITE(0,*) "fatal error: ",text," in ",where
  END SELECT
  STOP
END SUBROUTINE
