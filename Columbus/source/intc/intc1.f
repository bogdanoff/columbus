!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      program driveINTC
cversion 5.0
c  this is just a driver program
c  for all notes see the main subroutine INTC
c
c  version log:
c  09-nov-01 filename added to unit 17 open statement. -rls
c
      implicit real*8 (a-h,o-z)
      parameter (maxat=200,maxelm=36)
      parameter(nkey=8)
c      maxat- maximum number of atoms;maxelm- first 36 elements handled
c       note: this is just a driver program,
c       basic dimensions are set in subr intc
      dimension xyz(3,maxat),ian(maxat),xian(maxat)
      character*80 title
      character*2 name,elem(maxelm)
      character*4 keyw(nkey),code
      dimension x(maxat),y(maxat),z(maxat),name(maxat)
      data elem /'H ','HE','LI','BE','B ','C ','N ','O ','F ','NE',
     1 'NA','MG','AL','SI','P ','S ','CL','AR',
     2 'K ','CA','SC','TI','V ','CR','MN','FE','CO','NI','CU',
     3 'ZN','GA','GE','AS','SE','BR','KR'/
      data keyw /'B631','B421','NOFD','BLOW','RADI',
     1 'BOND','HALL','HSUB'/
c      these are the keywords that can be read by INTC (not this
c      driver main pr.) as options;they are needed here only to
c      recognize the end of coordinates.
c FG   ipun will be the punch file for the coordinates generated
c      iptyp will contain a description of the coordinates generated
c matruc 2007/01/17:
c most f90 compilers do not allow to REWIND or BACKSPACE STDIN (unit=5)
c intcin is now read directly
      inp=8
      open(unit=inp,file='intcin',status='unknown',form='formatted')
      ipun=7
c PSZ    open(unit=7,status='unknown')
      open(unit=ipun,file='intcfl',status='unknown',form='formatted')
      iptyp=17
      open(unit=iptyp,file='icoordtyp', status='unknown')
c
      natom=0
      read(inp,100) title
100   format(a80)
      write(*,100) title
      write(ipun,100) title
      if(title(1:5).eq.'TEXAS') then
        do 50 i=1,maxat
          read(inp,210,end=1000) code
210       format(a4)
          do 55 k=1,nkey
c   there may be option cards (for INTC) after the cartesians, these
c  indicate then the end of input for driver main
            if(code.eq.keyw(k)) then
             backspace (inp)
             goto 1000
            endif
 55       continue
             backspace (inp)
          read(inp,200,end=1000) name(i),xian(i),x(i),y(i),z(i)
 200      format(2x,a2,6x,4f10.5)
        natom=natom+1
        ian(i)=xian(i)+0.1
 50     continue
      else
c         if(title(1:5)... other input formats can be added here later
c
c       The next part converts PC-model eth (ExTended Huckel)
c       files to TEXAS input files
c       they will be rearrenged to put hydrogens to the end
        read(inp,*) nh,nc
        natom=nh+nc
        do 60 i=1,natom
          if(i.le.nh) then
            inew=i+nc
          else
            inew=i-nh
          endif
          read(inp,300) x(inew),y(inew),z(inew),name(inew)
 300      format(3f15.5,5x,a2)
          do 65 ii=1,18
           if (name(inew).eq.elem(ii)) then
               ian(inew)=ii
               go to 65
           end if
  65      continue
  60    continue
      end if
      do 70 i=1,natom
        write (ipun,400) name(i),ian(i),x(i),y(i),z(i)
  70  continue
 400    format('N=',a2,6x,i2,'.',7x,3F10.3)
1000  continue
      do 80 i=1,natom
        xyz(1,i)=x(i)
        xyz(2,i)=y(i)
        xyz(3,i)=z(i)
  80  continue
C temp
C         print *, 'natom=', natom
c
      call intc(xyz,ian,natom,inp,ipun,iptyp,ncg)
      call bummer('normal termination',0,3)
      STOP 'end of intc'
      end
c
