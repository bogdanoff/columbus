!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
ccidrt part=3 of 4.  interacting space routines.
cversion=4.1 last modified: 24-nov-90
cversion=5.0
c deck block data
      block data
c
c  pointer tables for segment construction and evaluation.
c
c  mu-dependent entries added 7-nov-86 (rls).
c  written 7-sep-84 by ron shepard.
c
       implicit none
c     # s32=sqrt(3./2.) s34=sqrt(3./4.)
c
      real*8    zero,     one,     half,     halfm,      onem
      parameter(zero=0d0, one=1d0, half=5d-1,halfm=-5d-1,onem=-1d0)
      real*8    s32
      parameter(s32=1.2247448713915890490986420373529458130097385d0)
      real*8    s34
      parameter(s34=.86602540378443864676372317075293616107654866d0)
c
      integer        mult
      common /cmult/ mult(8,8)
c
c     # nst= the number of segment value types.
c
      integer   nst,    nst5
      parameter(nst=40, nst5=5*nst)
      integer      pt
      common /cpt/ pt(0:3,0:3,nst5)
c
      integer      nex,       bcasex,         kcasex
      common /cex/ nex(-2:3), bcasex(6,-2:3), kcasex(6,-2:3)
c
      integer                                          vtpt
      real*8          vtran,        vcoeff,    oldval
      common /cvtran/ vtran(2,2,5), vcoeff(4), oldval, vtpt
c
      integer
     & st1,     st2,     st3,     st4,     st4p,     st5,
     & st6,     st7,     st8,     st10,    st11d,    st11,
     & st12,    st13,    st14
      common /cst/
     & st1(8),  st2(8),  st3(8),  st4(6),  st4p(6),  st5(6),
     & st6(6),  st7(6),  st8(6),  st10(6), st11d(4), st11(4),
     & st12(4), st13(4), st14(2)

      integer
     & db1,           db2,           db3,           db4,
     & db4p,          db5,           db6,           db7,
     & db8,           db10,          db11,          db12,
     & db13,          db14
      common /cdb/
     & db1(8,0:2,2),  db2(8,0:2,2),  db3(8,0:2,2),  db4(6,0:2,2),
     & db4p(6,0:2),   db5(6,0:2),    db6(6,0:2,2),  db7(6,0:2),
     & db8(6,0:2,2),  db10(6,0:2),   db11(4,0:2,2), db12(4,0:2,3),
     & db13(4,0:2),   db14(2,0:2,2)
c
c     # the following parameter list is the location of the
c     # delb=0 pointer matrix for the various segment types.
c
      integer  w00,    rt0,    lb0,    rb0,    lt0,    r0,     l0,
     & wrt00,  wrb00,  ww00,   wr00,   rtrt00, rbrb00, rtrb00, rrt00,
     & rrt10,  rrb00,  rrb10,  rr00,   rr10,   rtlb0,  rtlt10, rblb10,
     & rtl10,  rlb10,  rlt10,  rl00,   rl10,   w10,    w20,    ww10,
     & ww20,   wrt10,  wrt20,  wrb10,  wrb20,  wr10,   wr20,   rtrb10,
     & rtrb20
c
      parameter(w00=3)
      parameter(rt0=8)
      parameter(lb0=13)
      parameter(rb0=18)
      parameter(lt0=23)
      parameter(r0=28)
      parameter(l0=33)
      parameter(wrt00=38)
      parameter(wrb00=43)
      parameter(ww00=48)
      parameter(wr00=53)
      parameter(rtrt00=58)
      parameter(rbrb00=63)
      parameter(rtrb00=68)
      parameter(rrt00=73)
      parameter(rrt10=78)
      parameter(rrb00=83)
      parameter(rrb10=88)
      parameter(rr00=93)
      parameter(rr10=98)
      parameter(rtlb0=103)
      parameter(rtlt10=108)
      parameter(rblb10=113)
      parameter(rtl10=118)
      parameter(rlb10=123)
      parameter(rlt10=128)
      parameter(rl00=133)
      parameter(rl10=138)
      parameter(w10=143)
      parameter(w20=148)
      parameter(ww10=153)
      parameter(ww20=158)
      parameter(wrt10=163)
      parameter(wrt20=168)
      parameter(wrb10=173)
      parameter(wrb20=178)
      parameter(wr10=183)
      parameter(wr20=188)
      parameter(rtrb10=193)
      parameter(rtrb20=198)
c
c     # equivalence names are used only locally to this routine
c     # for the purpose of documentation.
c
      integer w0(0:3,0:3,-2:2)
      equivalence (w0(0,0,0),pt(0,0,w00))
c
      integer rt(0:3,0:3,-2:2)
      equivalence (rt(0,0,0),pt(0,0,rt0))
c
      integer lb(0:3,0:3,-2:2)
      equivalence (lb(0,0,0),pt(0,0,lb0))
c
      integer rb(0:3,0:3,-2:2)
      equivalence (rb(0,0,0),pt(0,0,rb0))
c
      integer lt(0:3,0:3,-2:2)
      equivalence (lt(0,0,0),pt(0,0,lt0))
c
      integer r(0:3,0:3,-2:2)
      equivalence (r(0,0,0),pt(0,0,r0))
c
      integer l(0:3,0:3,-2:2)
      equivalence (l(0,0,0),pt(0,0,l0))
c
      integer wrt0(0:3,0:3,-2:2)
      equivalence (wrt0(0,0,0),pt(0,0,wrt00))
c
      integer wrb0(0:3,0:3,-2:2)
      equivalence (wrb0(0,0,0),pt(0,0,wrb00))
c
      integer ww0(0:3,0:3,-2:2)
      equivalence (ww0(0,0,0),pt(0,0,ww00))
c
      integer wr0(0:3,0:3,-2:2)
      equivalence (wr0(0,0,0),pt(0,0,wr00))
c
      integer rtrt0(0:3,0:3,-2:2)
      equivalence (rtrt0(0,0,0),pt(0,0,rtrt00))
c
      integer rbrb0(0:3,0:3,-2:2)
      equivalence (rbrb0(0,0,0),pt(0,0,rbrb00))
c
      integer rtrb0(0:3,0:3,-2:2)
      equivalence (rtrb0(0,0,0),pt(0,0,rtrb00))
c
      integer rrt0(0:3,0:3,-2:2)
      equivalence (rrt0(0,0,0),pt(0,0,rrt00))
c
      integer rrt1(0:3,0:3,-2:2)
      equivalence (rrt1(0,0,0),pt(0,0,rrt10))
c
      integer rrb0(0:3,0:3,-2:2)
      equivalence (rrb0(0,0,0),pt(0,0,rrb00))
c
      integer rrb1(0:3,0:3,-2:2)
      equivalence (rrb1(0,0,0),pt(0,0,rrb10))
c
      integer rr0(0:3,0:3,-2:2)
      equivalence (rr0(0,0,0),pt(0,0,rr00))
c
      integer rr1(0:3,0:3,-2:2)
      equivalence (rr1(0,0,0),pt(0,0,rr10))
c
      integer rtlb(0:3,0:3,-2:2)
      equivalence (rtlb(0,0,0),pt(0,0,rtlb0))
c
      integer rtlt1(0:3,0:3,-2:2)
      equivalence (rtlt1(0,0,0),pt(0,0,rtlt10))
c
      integer rblb1(0:3,0:3,-2:2)
      equivalence (rblb1(0,0,0),pt(0,0,rblb10))
c
      integer rtl1(0:3,0:3,-2:2)
      equivalence (rtl1(0,0,0),pt(0,0,rtl10))
c
      integer rlb1(0:3,0:3,-2:2)
      equivalence (rlb1(0,0,0),pt(0,0,rlb10))
c
      integer rlt1(0:3,0:3,-2:2)
      equivalence (rlt1(0,0,0),pt(0,0,rlt10))
c
      integer rl0(0:3,0:3,-2:2)
      equivalence (rl0(0,0,0),pt(0,0,rl00))
c
      integer rl1(0:3,0:3,-2:2)
      equivalence (rl1(0,0,0),pt(0,0,rl10))
c
      integer w1(0:3,0:3,-2:2)
      equivalence (w1(0,0,0),pt(0,0,w10))
c
      integer w2(0:3,0:3,-2:2)
      equivalence (w2(0,0,0),pt(0,0,w20))
c
      integer ww1(0:3,0:3,-2:2)
      equivalence (ww1(0,0,0),pt(0,0,ww10))
c
      integer ww2(0:3,0:3,-2:2)
      equivalence (ww2(0,0,0),pt(0,0,ww20))
c
      integer wrt1(0:3,0:3,-2:2)
      equivalence (wrt1(0,0,0),pt(0,0,wrt10))
c
      integer wrt2(0:3,0:3,-2:2)
      equivalence (wrt2(0,0,0),pt(0,0,wrt20))
c
      integer wrb1(0:3,0:3,-2:2)
      equivalence (wrb1(0,0,0),pt(0,0,wrb10))
c
      integer wrb2(0:3,0:3,-2:2)
      equivalence (wrb2(0,0,0),pt(0,0,wrb20))
c
      integer wr1(0:3,0:3,-2:2)
      equivalence (wr1(0,0,0),pt(0,0,wr10))
c
      integer wr2(0:3,0:3,-2:2)
      equivalence (wr2(0,0,0),pt(0,0,wr20))
c
      integer rtrb1(0:3,0:3,-2:2)
      equivalence (rtrb1(0,0,0),pt(0,0,rtrb10))
c
      integer rtrb2(0:3,0:3,-2:2)
      equivalence (rtrb2(0,0,0),pt(0,0,rtrb20))
c
c     # for each loop type, assign the segment extension type pointers
c     # and the delb offsets for each range.
c
c     # loop type 1:
      data  st1, db1
     & /  3,     1,     0,    -1,     0,     1,     0,    -1,
     & rl00,   rb0,    r0,   rt0,  rl00,   rb0,    r0,   rt0,
     & rl00,   rb0,    r0,   rt0,  rl00,   rb0,    r0,   rt0,
     & rl00,   rb0,    r0,   rt0,  rl00,   rb0,    r0,   rt0,
     & rl00,   rb0,    r0, rlb10,  rl10, rlt10,    r0,   rt0,
     & rl00,   rb0,    r0, rlb10,  rl10, rlt10,    r0,   rt0,
     & rl00,   rb0,    r0, rlb10,  rl10, rlt10,    r0,   rt0/
c
c     # loop type 2:
      data  st2, db2
     & /  3,     1,     0,    -1,     0,    -1,     0,     1,
     & rl00,   rb0,    r0,   rt0,  rl00,   lb0,    l0,   lt0,
     & rl00,   rb0,    r0,   rt0,  rl00,   lb0,    l0,   lt0,
     & rl00,   rb0,    r0,   rt0,  rl00,   lb0,    l0,   lt0,
     & rl00,   rb0,    r0, rlb10,  rl10, rtl10,    l0,   lt0,
     & rl00,   rb0,    r0, rlb10,  rl10, rtl10,    l0,   lt0,
     & rl00,   rb0,    r0, rlb10,  rl10, rtl10,    l0,   lt0/
c
c     # loop type 3:
      data  st3, db3
     & /  3,     1,     0,     1,     0,    -1,     0,    -1,
     & rl00,   rb0,    r0, rrb00,  rr00, rrt00,    r0,   rt0,
     & rl00,   rb0,    r0, rrb00,  rr00, rrt00,    r0,   rt0,
     & rl00,   rb0,    r0, rrb00,  rr00, rrt00,    r0,   rt0,
     & rl00,   rb0,    r0, rrb10,  rr10, rrt10,    r0,   rt0,
     & rl00,   rb0,    r0, rrb10,  rr10, rrt10,    r0,   rt0,
     & rl00,   rb0,    r0, rrb10,  rr10, rrt10,    r0,   rt0/
c
c     # loop type 4:
      data  st4, db4
     & /  3,     0,     0,     1,     0,    -1,
     & rl00,   w00,  rl00,   rb0,    r0,   rt0,
     & rl00,   w10,  rl00,   rb0,    r0,   rt0,
     & rl00,   w20,  rl00,   rb0,    r0,   rt0,
     & rl00,rblb10,  rl10, rlt10,    r0,   rt0,
     & rl00,rblb10,  rl10, rlt10,    r0,   rt0,
     & rl00,rblb10,  rl10, rlt10,    r0,   rt0/
c
c     # loop type 4':
      data st4p, db4p
     & /  3,      0,     0,    -1,     0,     1,
     & rl00, rblb10,  rl10, rtl10,    l0,   lt0,
     & rl00, rblb10,  rl10, rtl10,    l0,   lt0,
     & rl00, rblb10,  rl10, rtl10,    l0,   lt0/
c
c     # loop type 5:
      data  st5, db5
     & /  3,      2,     0,    -1,     0,    -1,
     & rl00, rbrb00,  rr00, rrt00,    r0,   rt0,
     & rl00, rbrb00,  rr00, rrt00,    r0,   rt0,
     & rl00, rbrb00,  rr00, rrt00,    r0,   rt0/
c
c     # loop type 6:
      data  st6, db6
     & /  3,     1,     0,     0,     0,    -1,
     & rl00,   rb0,    r0,  wr00,    r0,   rt0,
     & rl00,   rb0,    r0,  wr10,    r0,   rt0,
     & rl00,   rb0,    r0,  wr20,    r0,   rt0,
     & rl00,   rb0,    r0,rtrb00,    r0,   rt0,
     & rl00,   rb0,    r0,rtrb10,    r0,   rt0,
     & rl00,   rb0,    r0,rtrb20,    r0,   rt0/
c
c     # loop type 7:
      data  st7, db7
     & /  3,     1,     0,    -2,     0,     1,
     & rl00,   rb0,    r0, rtlb0,    l0,   lt0,
     & rl00,   rb0,    r0, rtlb0,    l0,   lt0,
     & rl00,   rb0,    r0, rtlb0,    l0,   lt0/
c
c     # loop type 8:
      data  st8, db8
     & /  3,     1,     0,    -1,     0,      0,
     & rl00,   rb0,    r0,   rt0,  rl00,    w00,
     & rl00,   rb0,    r0,   rt0,  rl00,    w10,
     & rl00,   rb0,    r0,   rt0,  rl00,    w20,
     & rl00,   rb0,    r0, rlb10,  rl10, rtlt10,
     & rl00,   rb0,    r0, rlb10,  rl10, rtlt10,
     & rl00,   rb0,    r0, rlb10,  rl10, rtlt10/
c
c     # loop type 10:
      data st10, db10
     & /  3,     1,     0,     1,     0,     -2,
     & rl00,   rb0,    r0, rrb00,  rr00, rtrt00,
     & rl00,   rb0,    r0, rrb00,  rr00, rtrt00,
     & rl00,   rb0,    r0, rrb00,  rr00, rtrt00/
c
c     # loop type 11d, 11:
      data st11d, st11, db11
     & /  3,      3,      3,      3,
     &    3,      0,      0,      0,
     & rl00,    w00,   rl00,    w00,
     & rl00,    w10,   rl00,    w10,
     & rl00,    w20,   rl00,    w20,
     & rl00, rblb10,   rl10, rtlt10,
     & rl00, rblb10,   rl10, rtlt10,
     & rl00, rblb10,   rl10, rtlt10/
c
c     # loop type 12:
      data st12, db12
     & /  3,     1,     0,    -1,
     & rl00, wrb00,    r0,   rt0,
     & rl00, wrb10,    r0,   rt0,
     & rl00, wrb20,    r0,   rt0,
     & rl00,   rb0,    r0, wrt00,
     & rl00,   rb0,    r0, wrt10,
     & rl00,   rb0,    r0, wrt20,
     & rl00,   rb0,    r0,   rt0,
     & rl00,   rb0,    r0,   rt0,
     & rl00,   rb0,    r0,   rt0/
c
c     # loop type 13:
      data st13, db13
     & /  3,      2,     0,     -2,
     & rl00, rbrb00,  rr00, rtrt00,
     & rl00, rbrb00,  rr00, rtrt00,
     & rl00, rbrb00,  rr00, rtrt00/
c
c     # loop type 14:
      data st14, db14
     & /  3,     3,
     & rl00,  ww00,
     & rl00,  ww10,
     & rl00,  ww20,
     & rl00,   w00,
     & rl00,   w10,
     & rl00,   w20/
c
c     # segment values are calculated as:
c     #
c     #        tab( type(bcase,kcase,delb), bket)
c     #
c     # where the entries in tab(*,*) are defined in the table
c     # initialization routine.  type(*,*,*) is equivalenced to the
c     # appropriate  location of pt(*,*,*) and pt(*,*,*) is actually
c     # referenced with the corresponding offset added to delb.  in
c     # this version, this offset is determined from the range offset
c     # and from the reference occupation, imu, of the orbital.
c     #
c     # e.g. tab( pt(bcase,kcase,db0(r,imu)+delb), bket)
c
c**********************************************************
c     # w segment value pointers (assuming mu(*)=0):
      data w0/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 2, 1, 1,  1, 1, 2, 1,  1, 1, 1, 4,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r-top segment value pointers:
      data rt/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  2 ,1 ,1 ,1,  2, 1, 1, 1,  1,18,20, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # l-bottom segment value pointers:
      data lb/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  2, 1, 1, 1,  1,20, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  2, 1, 1, 1,  1, 1, 1, 1,  1, 1,18, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r-bottom segment value pointers:
      data rb/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 2, 1, 1,  1, 1, 1, 1,  1, 1, 1,22,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 2, 1,  1, 1, 1,16,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # l-top segment value pointers:
      data lt/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 2, 2, 1,  1, 1, 1,18,  1, 1, 1,20,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r segment value pointers:
      data r/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 2, 1, 1, 1,  1, 3, 1, 1,  1,12,34, 1,  1, 1, 1, 3,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 2, 1, 1, 1,  1,31, 8, 1,  1, 1, 3, 1,  1, 1, 1, 3,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # l segment value pointers:
      data l/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 2, 1, 1, 1,  1,33, 1, 1,  1,10, 3, 1,  1, 1, 1, 3,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 2, 1, 1, 1,  1, 3,11, 1,  1, 1,33, 1,  1, 1, 1, 3,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # wr-top segment value pointers (assuming mu(*)=0):
      data wrt0/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1,18,20, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # wr-bottom segment value pointers (assuming mu(*)=0):
      data wrb0/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1,22,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1,16,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # ww segment value pointers (assuming mu(*)=0):
      data ww0/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 4,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # wr segment value pointers (assuming mu(*)=0):
      data wr0/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 3, 1, 1,  1,12,34, 1,  1, 1, 1, 5,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1,31, 8, 1,  1, 1, 3, 1,  1, 1, 1, 5,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r-top-r-top, x=0 segment value pointers:
      data rtrt0/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  6, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r-bottom-r-bottom,x=0  segment value pointers:
      data rbrb0/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 6,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r-top-r-bottom segment value pointers (assuming mu(*)=0):
      data rtrb0/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 2, 1, 1,  1, 2, 1, 1,  1, 1, 1, 2,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 2, 1,  1, 1, 2, 1,  1, 1, 1, 2,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # rr-top, x=0 segment value pointers:
      data rrt0/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1, 54, 1, 1, 1,  1, 7, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1, 44, 1, 1, 1,  1, 1, 1, 1,  1, 1, 7, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # rr-top, x=1 segment value pointers:
      data rrt1/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  2, 1, 1, 1, 55, 1, 1, 1,  1,53,24, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1, 43, 1, 1, 1,  2, 1, 1, 1,  1,14,45, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # rr-bottom, x=0 segment value pointers:
      data rrb0/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1,48,49, 1,  1, 1, 1, 7,  1, 1, 1, 7,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # rr-bottom, x=1 segment value pointers:
      data rrb1/
     & 1, 3, 1, 1,  1, 1, 1, 1,  1, 1, 1,23,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1,50,48, 1,  1, 1, 1,46,  1, 1, 1,52,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 3, 1,  1, 1, 1,17,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # rr, x=0 segment value pointers:
      data rr0/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 2, 1, 1, 1,  1, 3, 1, 1,  1, 1, 3, 1,  1, 1, 1, 2,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # rr, x=1 segment value pointers:
      data rr1/
     & 2, 1, 1, 1,  1, 2, 1, 1,  1,30,41, 1,  1, 1, 1, 2,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 2, 1, 1, 1,  1,38,27, 1,  1,29,40, 1,  1, 1, 1, 2,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 2, 1, 1, 1,  1,36,26, 1,  1, 1, 2, 1,  1, 1, 1, 2/
c**********************************************************
c     # r-top-l-bottom segment value pointers:
      data rtlb/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1, 20, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1, 18, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r-top-l-top, x=1 segment value pointers:
      data rtlt1/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1,47, 2, 1,  1, 2,51, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r-bottom-l-bottom, x=1 segment value pointers:
      data rblb1/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1,34, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1,45, 1, 1,  1, 1,53, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1,31, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r-top-l, x=1 segment value pointers:
      data rtl1/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  2, 1, 1, 1, 51, 1, 1, 1,  1,48,21, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1, 47, 1, 1, 1,  2, 1, 1, 1,  1,19,50, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r-l-bottom, x=1 segment value pointers:
      data rlb1/
     & 1, 1, 1, 1,  1, 1, 1, 1, 34, 1, 1, 1,  1,25, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1, 45, 1, 1, 1, 53, 1, 1, 1,  1,50,48, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1, 31, 1, 1, 1,  1, 1, 1, 1,  1, 1,15, 1/
c**********************************************************
c     # r-l-top, x=1 segment value pointers:
      data rlt1/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1,53, 2, 1,  1, 1, 1,23,  1, 1, 1,56,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 2,45, 1,  1, 1, 1,42,  1, 1, 1,17,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # rl, x=0 segment value pointers:
      data rl0/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 2, 1, 1, 1,  1, 2, 1, 1,  1, 1, 2, 1,  1, 1, 1, 2,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # rl, x=1 segment value pointers:
      data rl1/
     & 2, 1, 1, 1,  1,35, 1, 1,  1,13,35, 1,  1, 1, 1, 2,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 2, 1, 1, 1,  1,37,28, 1,  1,28,39, 1,  1, 1, 1, 2,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 2, 1, 1, 1,  1,32, 9, 1,  1, 1,32, 1,  1, 1, 1, 2/
c**********************************************************
c     # w segment value pointers (assuming mu(*)=1):
      data w1/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 3, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 2,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # w segment value pointers (assuming mu(*)=2):
      data w2/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 5, 1, 1, 1,  1, 3, 1, 1,  1, 1, 3, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # ww segment value pointers (assuming mu(*)=1):
      data ww1/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 2, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 2,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # ww segment value pointers (assuming mu(*)=2):
      data ww2/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 4, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # wr-top segment value pointers (assuming mu(*)=1):
      data wrt1/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1, 58, 1, 1, 1, 58, 1, 1, 1,  1,66,67, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # wr-top segment value pointers (assuming mu(*)=2):
      data wrt2/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  3, 1, 1, 1,  3, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # wr-bottom segment value pointers (assuming mu(*)=1):
      data wrb1/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1,58, 1, 1,  1, 1, 1, 1,  1, 1, 1,68,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1,58, 1,  1, 1, 1,65,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # wr-bottom segment value pointers (assuming mu(*)=2):
      data wrb2/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 3, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 3, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # wr segment value pointers (assuming mu(*)=1):
      data wr1/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 3, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 3,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 3, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 3,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # wr segment value pointers (assuming mu(*)=2):
      data wr2/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 5, 1, 1, 1,  1, 2, 1, 1,  1,60,35, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 5, 1, 1, 1,  1,32,59, 1,  1, 1, 2, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r-top-r-bottomsegment value pointers (assuming mu(*)=1):
      data rtrb1/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 57, 1, 1, 1,  1,57, 1, 1,  1,64,70, 1,  1, 1, 1,57,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 57, 1, 1, 1,  1,69,63, 1,  1, 1,57, 1,  1, 1, 1,57,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c**********************************************************
c     # r-top-r-bottom segment value pointers (assuming mu(*)=2):
      data rtrb2/
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 2, 1, 1, 1,  1, 1, 1, 1,  1,62,34, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 2, 1, 1, 1,  1,31,61, 1,  1, 1, 1, 1,  1, 1, 1, 1,
     & 1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1,  1, 1, 1, 1/
c
c     # segment extension tables:
c     # casex(*,n) for n=-2 to 2 are extensions for occupation changes
c     #                     of n.
c     #                n=3 is a special case for diagonal extensions.
c
      data nex/ 1, 4, 6, 4, 1, 4/
*@ifdef reference
*C    # debug versions to agree with ancient ft program. -rls
*     data bcasex/
*     &   0, 0, 0, 0, 0, 0,
*     &   0, 0, 2, 1, 0, 0,
*     &   0, 1, 1, 2, 2, 3,
*     &   1, 2, 3, 3, 0, 0,
*     &   3, 0, 0, 0, 0, 0,
*     &   0, 1, 2, 3, 0, 0/
*     data kcasex/
*     &   3, 0, 0, 0, 0, 0,
*     &   1, 2, 3, 3, 0, 0,
*     &   0, 1, 2, 1, 2, 3,
*     &   0, 0, 1, 2, 0, 0,
*     &   0, 0, 0, 0, 0, 0,
*     &   0, 1, 2, 3, 0, 0/
*@else
      data bcasex/
     & 0, 0, 0, 0, 0, 0,
     & 2, 1, 0, 0, 0, 0,
     & 3, 2, 2, 1, 1, 0,
     & 3, 3, 2, 1, 0, 0,
     & 3, 0, 0, 0, 0, 0,
     & 3, 2, 1, 0, 0, 0/
      data kcasex/
     & 3, 0, 0, 0, 0, 0,
     & 3, 3, 2, 1, 0, 0,
     & 3, 2, 1, 2, 1, 0,
     & 2, 1, 0, 0, 0, 0,
     & 0, 0, 0, 0, 0, 0,
     & 3, 2, 1, 0, 0, 0/
*@endif
c
c     # irrep multiplication table.
c
      data mult/
     & 1,2,3,4,5,6,7,8,
     & 2,1,4,3,6,5,8,7,
     & 3,4,1,2,7,8,5,6,
     & 4,3,2,1,8,7,6,5,
     & 5,6,7,8,1,2,3,4,
     & 6,5,8,7,2,1,4,3,
     & 7,8,5,6,3,4,1,2,
     & 8,7,6,5,4,3,2,1/
c
c     # loop-value to ft-value transformation matrices.
c
      data vtran /
     & one,   zero,  zero,   one,
     & one,   zero,  halfm,  one,
     & one,   onem,  one,    one,
     & half,  zero,  zero,   one,
     & one,   zero,  halfm,  s32 /
c
      data vcoeff /
     & one,   half,  s32,    s34 /
c
      end
c deck istack
      subroutine istack
c
c  initialize necessary stack information for loop construction.
c
       implicit none 
C====>Begin Module ISTACK                 File cidrtms3.f               
C---->Makedcls Options: All variables                                   
C
C     Parameter variables
C
      INTEGER             NIMOMX
      PARAMETER           (NIMOMX = 128)
C
      REAL*8              ZERO
      PARAMETER           (ZERO = 0D0)
C
C     Local variables
C
      INTEGER             I
C
      INCLUDE 'cstack.inc'
C====>End Module   ISTACK                 File cidrtms3.f               
c
      do 100 i = 0, nimomx
         extbk(i) = 0
         v(1,i)   = zero
         v(2,i)   = zero
         v(3,i)   = zero
         yb(i)    = 0
         yk(i)    = 0
100   continue
c
      return
      end
c deck intab
      subroutine intab
c
c  initialize tables for segment value calculations.
c
c  for details of segment evaluation and the auxiliary functions,
c  see:
c        i. shavitt, in "the unitary group for the evaluation of
c        electronic energy matrix elements", j. hinze, ed.
c        (springer-verlag, berlin, 1981) p. 51. and i. shavitt,
c        "new methods in computational quantum chemistry and
c        their application on modern super-computers" (annual
c        report to the national aeronautics and space administration),
c        battelle columbus laboratories, june 1979.
c
c  mu-dependent segment evaluation entries added 7-nov-86 (rls).
c  this version written 01-aug-84 by ron shepard.
c
       implicit none 
C====>Begin Module INTAB                  File cidrtms3.f               
C---->Makedcls Options: All variables                                   
C
C     Statement functions
C
      REAL*8              RX,          XA,          XB,          XC
      REAL*8              XD,          XF,          XR
C
C     Parameter variables
C
      INTEGER             NTAB
      PARAMETER           (NTAB = 70)
      INTEGER             MAXB
      PARAMETER           (MAXB = 19)
C
      REAL*8              ZERO
      PARAMETER           (ZERO = 0D0)
      REAL*8              ONE
      PARAMETER           (ONE = 1D0)
      REAL*8              TWO
      PARAMETER           (TWO = 2D0)
      REAL*8              HALF
      PARAMETER           (HALF = .5D0)
C
C     Local variables
C
      INTEGER             B,           I,           P,           Q
      INTEGER             R,           S
C
      REAL*8              SQRT2,       T
C
      INCLUDE 'ctab.inc'
C====>End Module   INTAB                  File cidrtms3.f               
c
c  table choice is dictated by the requirement that all segment
c  values are to be calculated with array references only and
c  with no conditional tests or goto's required.  this requires
c  an over-complete set of tables to allow for sign changes and
c  constant factors.
c
c**********************************************************************
c
c  table entry definitions:
c
c   1:0             2:1             3:-1            4:2
c   5:-2            6:sqrt2         7:-t            8:1/b
c   9:-sqrt2/b     10:1/(b+1)      11:-1/(b+1)     12:-1/(b+2)
c  13:-sqrt2/(b+2) 14:a(-1,0)      15:-a(-1,0)     16:a(1,0)
c  17:-a(1,0)      18:a(0,1)       19:-a(0,1)      20:a(2,1)
c  21:-a(2,1)      22:a(1,2)       23:-a(1,2)      24:a(3,2)
c  25:-a(3,2)      26:b(-1,0)      27:b(0,1)       28:-b(0,2)
c  29:b(1,2)       30:b(2,3)       31:c(0)         32:-c(0)
c  33:c(1)         34:c(2)         35:-c(2)        36:d(-1)
c  37:d(0)         38:-d(0)        39:d(1)         40:-d(1)
c  41:d(2)         42:ta(-1,0)     43:-ta(-1,0)    44:ta(1,0)
c  45:ta(2,0)      46:-ta(2,0)     47:-ta(-1,1)    48:ta(0,1)
c  49:ta(2,1)      50:-ta(2,1)     51:ta(3,1)      52:ta(0,2)
c  53:-ta(0,2)     54:ta(1,2)      55:ta(3,2)      56:-ta(3,2)
c         added for mu-dependent segment evaluation
c  57:1/2          58:-1/2         59:-1/b         60:1/(b+2)
c  61:(b+1)/b      62:(b+1)/(b+2)  63:(2b+1)/2b    64:(2b+3)/(2b+4)
c  65:1/2*a(1,0)   66:1/2*a(0,1)   67:1/2*a(2,1)   68:1/2*a(1,2)
c  69:1/2*c(0)     70:1/2*c(2)
c
c**********************************************************************
c
c     # auxiliary functions:
c
c     # rx() is a precision-independent integer-to-real conversion
c     # function. -rls
c
c
      rx(p)         = (p)
      xa(p,q,b)     = sqrt(rx(b+p) / rx(b+q))
      xb(p,q,b)     = sqrt(two / rx((b+p) * (b+q)))
      xc(p,b)       = sqrt(rx((b+p-1) * (b+p+1))) / rx(b+p)
      xd(p,b)       = sqrt(rx((b+p-1) * (b+p+2)) / rx((b+p) * (b+p+1)))
      xr(p,b)       = one / rx(b+p)
      xf(p,q,r,s,b) = rx(p*b+q) / rx(r*b+s)
c
      sqrt2 = sqrt(two)
      t     = one / sqrt2
c
      do 110 b = 0, maxb
         do 100 i = 1, ntab
            tab(i,b) = zero
100      continue
110   continue
c
      do 200 b = 0, maxb
         tab( 1,b) = zero
         tab( 2,b) = +one
         tab( 3,b) = -one
         tab( 4,b) = +two
         tab( 5,b) = -two
         tab( 6,b) = +sqrt2
         tab( 7,b) = -t
         tab(10,b) = +xr(1,b)
         tab(11,b) = -xr(1,b)
         tab(12,b) = -xr(2,b)
         tab(13,b) = -sqrt2*xr(2,b)
         tab(18,b) = +xa(0,1,b)
         tab(19,b) = -xa(0,1,b)
         tab(20,b) = +xa(2,1,b)
         tab(21,b) = -xa(2,1,b)
         tab(22,b) = +xa(1,2,b)
         tab(23,b) = -xa(1,2,b)
         tab(24,b) = +xa(3,2,b)
         tab(25,b) = -xa(3,2,b)
         tab(29,b) = +xb(1,2,b)
         tab(30,b) = +xb(2,3,b)
         tab(34,b) = +xc(2,b)
         tab(35,b) = -xc(2,b)
         tab(39,b) = +xd(1,b)
         tab(40,b) = -xd(1,b)
         tab(41,b) = +xd(2,b)
         tab(48,b) = +t * xa(0,1,b)
         tab(49,b) = +t * xa(2,1,b)
         tab(50,b) = -t * xa(2,1,b)
         tab(51,b) = +t * xa(3,1,b)
         tab(52,b) = +t * xa(0,2,b)
         tab(53,b) = -t * xa(0,2,b)
         tab(54,b) = +t * xa(1,2,b)
         tab(55,b) = +t * xa(3,2,b)
         tab(56,b) = -t * xa(3,2,b)
         tab(57,b) = +half
         tab(58,b) = -half
         tab(60,b) = +xr(2,b)
         tab(62,b) = +xf(1,1,1,2,b)
         tab(64,b) = +xf(2,3,2,4,b)
         tab(66,b) = +half * xa(0,1,b)
         tab(67,b) = +half * xa(2,1,b)
         tab(68,b) = +half * xa(1,2,b)
         tab(70,b) = +half * xc(2,b)
200   continue
c
      do 300 b = 1, maxb
         tab( 8,b) = +xr(0,b)
         tab( 9,b) = -sqrt2 * xr(0,b)
         tab(14,b) = +xa(-1,0,b)
         tab(15,b) = -xa(-1,0,b)
         tab(16,b) = +xa(1,0,b)
         tab(17,b) = -xa(1,0,b)
         tab(27,b) = +xb(0,1,b)
         tab(28,b) = -xb(0,2,b)
         tab(31,b) = +xc(0,b)
         tab(32,b) = -xc(0,b)
         tab(33,b) = +xc(1,b)
         tab(37,b) = +xd(0,b)
         tab(38,b) = -xd(0,b)
         tab(42,b) = +t * xa(-1,0,b)
         tab(43,b) = -t * xa(-1,0,b)
         tab(44,b) = +t * xa(1,0,b)
         tab(45,b) = +t * xa(2,0,b)
         tab(46,b) = -t * xa(2,0,b)
         tab(47,b) = -t * xa(-1,1,b)
         tab(59,b) = -xr(0,b)
         tab(61,b) = +xf(1,1,1,0,b)
         tab(63,b) = +xf(2,1,2,0,b)
         tab(65,b) = +half * xa(1,0,b)
         tab(69,b) = +half * xc(0,b)
300   continue
c
      do 400 b = 2, maxb
         tab(26,b) = +xb(-1,0,b)
         tab(36,b) = +xd(-1,b)
400   continue
c
      return
      end
c deck intd
      subroutine intd( limvec, ref ,ndrts)
c
c  determine the diagonal interacting csfs.  these are reference csfs
c  of the correct symmetry.
c
c  input:
c  l(*),y(*),xbar(*) = chaining indices.
c  ref(*) = references in skip vector form.
c  wsym(0) = state symmetry.
c  ndrts   = DRT set
c
c  output:
c  limvec(*) = updated in 0/1 form.
c
c  06-jun-89  written by ron shepard.
c
       implicit none 
C====>Begin Module INTD                   File cidrtms3.f               
C---->Makedcls Options: All variables                                   
C
C     Parameter variables
C
      INTEGER             NIMOMX
      PARAMETER           (NIMOMX = 128)
      INTEGER             NROWMX
      PARAMETER           (NROWMX = 2**10-1)
      INTEGER             NMXDRT
      PARAMETER           (NMXDRT = 8)
C
C     Argument variables
C
      INTEGER             LIMVEC(*),   NDRTS,       REF(*)
C
C     Local variables
C
      INTEGER             CLEV,        I,           ISTEP,       NLEV
      INTEGER             ROWC,        ROWN
C
C
      INCLUDE 'cdrt.inc' 
      INCLUDE 'cli.inc'
      INCLUDE 'clim.inc'
      INCLUDE 'cmult.inc'
      INCLUDE 'cstack.inc'
      INCLUDE 'cwsym.inc'
C====>End Module   INTD                   File cidrtms3.f               
c
c     # the current version of the calling program only allows
c     # references of the correct symmetry if the interacting space
c     # limitation is imposed.  this routine could be simplified
c     # accordingly.  however, the following code handles the more
c     # general cases, which may be added in the future,  and should be
c     # used unless there are compelling reasons to do otherwise. -rls
c
      do 20 i = 0, niot
         step(i) = 4
20    continue
c
      clev    = 0
      yb(0)   = 0
      row(0)  = 1 
      wsym(0) = ssym(ndrts)
c
c     # begin walk generation...
c
100   continue
c     # decrement the step at the current level.
      nlev = clev+1
      istep = step(clev)-1
      if ( istep .lt. 0 ) then
c        # decrement the current level, and check if done.
         step(clev) = 4
         if ( clev .eq. 0 ) goto 200
         clev = clev-1
         go to 100
      endif
      step(clev) = istep
      rowc = row(clev)
      rown = l(istep,rowc,1)
      if ( rown .eq. 0 ) go to 100
      yb(nlev) = yb(clev)+y(istep,rowc,1)
c     # can the next reference be reached from here?
      if ( ref(yb(nlev)+1) .ge. xbar(rown,1) ) go to 100
      row(nlev) = rown
      wsym(nlev) = mult( wsym(clev), arcsym(istep,clev) )
      if ( nlev .lt. niot ) then
c        # increment to the next level.
         clev = nlev
         go to 100
      else
c        # complete reference walk has been generated.  check
c        # the symmetry and update limvec(*).
         if ( wsym(niot) .eq. 1 ) then
            limvec(yb(niot)+1) = 1
         endif
         step(clev) = 4
         clev = clev-1
         go to 100
      endif
c
c     # all done.
200   continue
c
      return
      end
c deck intern2
      subroutine intern2( limvec, ref,ndrts )
c
c  construct the 2-internal interacting loops.
c
c     code   loop type     description
c     -----  ---------     -------------
c       0  -- new level --
c       1  -- new record --
c
c       2       3b          (xz)
c       3       3a          (wz)
c               10          (wz)
c       4       1ab         (yy)
c               8ab         (yy +diag)
c       5       1b          (yy)
c               8b          (yy +diag)
c       6       2b'         (yy)
c       7       1ab         (xx)
c               8ab         (xx +diag)
c       8       1b          (xx)
c               8b          (xx +diag)
c       9       2b'         (xx)
c      10       1b          (wx)
c               8b          (wx)
c      11       2b'         (wx)
c      12       1a          (ww)
c               8a          (ww +diag)
c
       implicit none 
C====>Begin Module INTERN2                File cidrtms3.f               
C---->Makedcls Options: All variables                                   
C
C     Parameter variables
C
      INTEGER             NIMOMX
      PARAMETER           (NIMOMX = 128)
      INTEGER             NROWMX
      PARAMETER           (NROWMX = 2**10-1)
C
C     Argument variables
C
      INTEGER             LIMVEC(*),   NDRTS,       REF(*)
C
C     Local variables
C
      INTEGER             KMO,         LMO,         QPRT
C
      INCLUDE 'cdb.inc'
      INCLUDE 'cli.inc'
      INCLUDE 'cst.inc'
C====>End Module   INTERN2                File cidrtms3.f               
c
      nintl = 2
      mo(0) = 0
      qprt = 1
c
      do 200 lmo = 1, niot
         do 100 kmo = 1, (lmo-1)
c            if(qprt.ge.5)write(*,6010)lmo,kmo
c***********************************************************************
c           i<l loop types:
c***********************************************************************
            nmo = 2
            mo(1) = kmo
            mo(2) = lmo
c
c           # xz loop 3b:
c
            numv = 1
            call loop( 3, 1, st3, db3(1,0,2), 8, 4, limvec, ref,ndrts)
c
c           # wz loop 3a:
c
            numv = 1
            call loop( 4, 1, st3, db3, 8, 4, limvec, ref,ndrts )
100      continue
c***********************************************************************
c        i=l loop types:
c***********************************************************************
c         if(qprt.ge.5)write(*,6010)lmo,lmo
         nmo = 1
         mo(1) = lmo
c
c        # wz loop 10:
c
         numv = 1
         call loop( 4, 1, st10, db10, 6, 4, limvec, ref ,ndrts)
200   continue
c
      return
6010  format(' intern2:  lmo,kmo = ',2i4)
      end
c deck intern3
      subroutine intern3( limvec, ref ,ndrts)
c
c  construct the 3-internal interacting loops.
c
c  code  loop type     description
c  ----  ---------     ----------------
c
c   0  -- new level --
c   1  -- new record --
c   2  -- new vertex --
c
c   3       1ab         (yz,xy,wy)
c           6ab         (yz,xy,wy)
c           8ab         (yz,xy,wy)
c   4       2a'b'       (yz,xy,wy)
c   5       3ab         (yz,xy,wy)
c   6       12bc        (yz,xy,wy)
c   7   (not used)
c   8       2b'         (yz,xy,wy)
c           7'          (yz,xy,wy)
c           8b          (yz,xy,wy)
c           10          (yz,xy,wy)
c   9       1b          (yz,xy,wy)
c           6b          (yz,xy,wy)
c
       implicit none 
C====>Begin Module INTERN3                File cidrtms3.f               
C---->Makedcls Options: All variables                                   
C
C     Parameter variables
C
      INTEGER             NIMOMX
      PARAMETER           (NIMOMX = 128)
      INTEGER             NROWMX
      PARAMETER           (NROWMX = 2**10-1)
C
C     Argument variables
C
      INTEGER             LIMVEC(*),   NDRTS,       REF(*)
C
C     Local variables
C
      INTEGER             BTAIL,       BTAILX(3),   JMO,         KMO
      INTEGER             KTAIL,       KTAILX(3),   LMO,         QPRT
      INTEGER             VER
C
      INCLUDE 'cdb.inc'
      INCLUDE 'cli.inc'
      INCLUDE 'cst.inc'
C====>End Module   INTERN3                File cidrtms3.f               
      data    btailx/2,3,4/, ktailx/1,2,2/
c
      nintl = 3
      mo(0) = 0
      qprt  = 1
      ver   = 1
c
      do 400 lmo = 1, niot
c
         do 200 kmo = 1, (lmo-1)
c
            do 100 jmo = 1, (kmo-1)
c**********************************************************************
c              j<k<l
c**********************************************************************
c               if(qprt.ge.5)write(*,6010)lmo,kmo,jmo
               nmo = 3
               mo(1) = jmo
               mo(2) = kmo
               mo(3) = lmo
c
               btail = btailx(ver)
               ktail = ktailx(ver)
c
c              # 1ab loops:
c
               numv = 2
         call loop( btail, ktail, st1, db1, 8, 2, limvec, ref,ndrts )
c
c              # 2a'b' loops:
c
               numv = 2
         call loop( btail, ktail, st2, db2, 8, 2, limvec, ref,ndrts )
c
c              # 3ab loops:
c
               numv = 2
         call loop( btail, ktail, st3, db3, 8, 2, limvec, ref,ndrts )
100         continue
c**********************************************************************
c           j=k<l
c**********************************************************************
c            if(qprt.ge.5)write(*,6010)lmo,kmo,kmo
            nmo = 2
            mo(1) = kmo
            mo(2) = lmo
c
c           # from ft: do 190 ver=1,3
            btail = btailx(ver)
            ktail = ktailx(ver)
c
c           # 6ab loops:
c
            numv = 2
        call loop( btail, ktail, st6, db6, 6, 2, limvec, ref,ndrts )
c
c           # 7' loops:
c
            numv = 1
         call loop( btail, ktail, st7, db7, 6, 2, limvec, ref,ndrts)
190         continue
200      continue
         do 300 jmo = 1, (lmo-1)
c**********************************************************************
c           j<k=l
c**********************************************************************
c            if(qprt.ge.5)write(*,6010)lmo,lmo,jmo
            nmo = 2
            mo(1) = jmo
            mo(2) = lmo
c
            btail = btailx(ver)
            ktail = ktailx(ver)
c
c           # 8ab loops:
c
            numv = 2
        call loop( btail, ktail, st8, db8, 6, 2, limvec, ref,ndrts )
c
c           # 10 loops:
c
            numv = 1
        call loop( btail, ktail, st10, db10, 6, 2, limvec, ref,ndrts )
c
300      continue
c**********************************************************************
c        j=k=l
c**********************************************************************
c         if(qprt.ge.5)write(*,6010)lmo,lmo,lmo
         nmo = 1
         mo(1) = lmo
c
         btail = btailx(ver)
         ktail = ktailx(ver)
c
c        # 12bc loops:
c
         numv = 2
      call loop( btail, ktail, st12, db12(1,0,2), 4, 2, limvec,
     .    ref,ndrts )
c
400   continue
c
      return
6010  format(' intern3:  lmo,kmo,jmo = ',3i4)
      end
c deck intern4
      subroutine intern4( limvec, ref, ndrts )
c
c  construct the 4-internal interacting loops.
c
c  code  loop type      description
c  ----  ---------      ------------
c
c   1  -- new record --
c   2  -- new vertex --
c   3  -- new level --
c
c   4       1ab         (z,y,x,w)
c           4ab         (z,y,x,w)
c           6ab         (z,y,x,w)
c           8ab         (z,y,x,w)
c           12ac        (z,y,x,w)
c   5       2a'b'       (z,y,x,w)
c   6       3ab         (z,y,x,w)
c           12bc        (z,y,x,w)
c   7                   (z,y,x,w)
c   8       2b'         (z,y,x,w)
c           7'          (z,y,x,w)
c           10          (z,y,x,w)
c           11b         (z,y,x,w)
c           13          (z,y,x,w)
c   9       1b          (z,y,x,w)
c           4b          (z,y,x,w)
c           4b'         (z,y,x,w)
c           5           (z,y,x,w)
c           6b          (z,y,x,w)
c           8b          (z,y,x,w)
c           12c         (z,y,x,w)
c  10       12abc       (z,y,x,w)
c
       implicit none 
C====>Begin Module INTERN4                File cidrtms3.f               
C---->Makedcls Options: All variables                                   
C
C     Parameter variables
C
      INTEGER             NIMOMX
      PARAMETER           (NIMOMX = 128)
      INTEGER             NROWMX
      PARAMETER           (NROWMX = 2**10-1)
      INTEGER             NMXDRT
      PARAMETER           (NMXDRT = 8)
C
C     Argument variables
C
      INTEGER             LIMVEC(*),   NDRTS,       REF(*)
C
C     Local variables
C
      INTEGER             ILEV,        IMO,         JKLSYM,      JLEV
      INTEGER             JMO,         KLEV,        KLSYM,       KMO
      INTEGER             LLEV,        LMO,         LSYM,        QPRT
      INTEGER             TAIL
C
      INCLUDE 'cdb.inc'
      INCLUDE 'cdrt.inc'
      INCLUDE 'cli.inc'
      INCLUDE 'cmult.inc'
      INCLUDE 'cst.inc'
C====>End Module   INTERN4                File cidrtms3.f               
c
      nintl = 4
      mo(0) = 0
      qprt  = 1
      tail  = 1
c
      do 800 lmo = 2, niot
         llev = lmo-1
         lsym = arcsym(1,llev)
c
         do 400 kmo = 1, (lmo-1)
            klev = kmo-1
            klsym = mult(arcsym(1,klev),lsym)
            do 200 jmo = 1, (kmo-1)
               jlev = jmo-1
               jklsym = mult(arcsym(1,jlev),klsym)
               do 100 imo = 1, (jmo-1)
                  ilev = imo-1
                  if(arcsym(1,ilev).ne.jklsym)go to 100
c***********************************************************
c                 i<j<k<l loops:
c***********************************************************
c                  if(qprt.ge.5)write(*,6010)lmo,kmo,jmo,imo
                  nmo = 4
                  mo(1) = imo
                  mo(2) = jmo
                  mo(3) = kmo
                  mo(4) = lmo
c
c                 # 1ab loops:
c
                  numv = 2
             call loop( tail, tail, st1, db1, 8, 0, limvec, ref,ndrts )
c
c                 # 2a'b' loops (bra-ket interchange from 2ab):
c
                  numv = 2
             call loop( tail, tail, st2, db2, 8, 0, limvec, ref,ndrts )
c
c                 # 3ab loops:
c
                  numv = 2
             call loop( tail, tail, st3, db3, 8, 0, limvec, ref,ndrts )
100            continue
               imo = jmo
               if(klsym.ne.1)go to 200
c***********************************************************
c              i=j<k<l loops:
c***********************************************************
c               if(qprt.ge.5)write(*,6010)lmo,kmo,jmo,imo
               nmo = 3
               mo(1) = jmo
               mo(2) = kmo
               mo(3) = lmo
c
c              # 4ab loops:
c
               numv = 2
         call loop( tail, tail, st4, db4, 6, 0, limvec, ref,ndrts )
c
c              # 4b' loops (bra-ket interchange from 4b):
c
               numv = 1
         call loop( tail, tail, st4p, db4p, 6, 0, limvec, ref,ndrts )
c
c              # 5 loops:
c
               numv = 1
         call loop( tail, tail, st5, db5, 6, 0, limvec, ref,ndrts )
c
c              # from ft: if(tail.lt.4)call wcode(2)
200         continue
            jmo = kmo
            do 300 imo = 1, (jmo-1)
               ilev = imo-1
               if(arcsym(1,ilev).ne.lsym)go to 300
c***********************************************************
c              i<j=k<l loops:
c***********************************************************
c               if(qprt.ge.5)write(*,6010)lmo,kmo,jmo,imo
               nmo = 3
               mo(1) = imo
               mo(2) = kmo
               mo(3) = lmo
c
c              # 6ab loops:
c
               numv = 2
         call loop( tail, tail, st6, db6, 6, 0, limvec, ref,ndrts )
c
c              # 7' loops (bra-ket interchange from 7):
c
               numv = 1
         call loop( tail, tail, st7, db7, 6, 0, limvec, ref,ndrts )
300         continue
            imo = jmo
c***********************************************************
c           i=j=k<l loops (deferred until i<j=k=l )
c***********************************************************
400      continue
         kmo = lmo
         do 600 jmo = 1, (kmo-1)
            jlev = jmo-1
            jklsym = arcsym(1,jlev)
            do 500 imo = 1, (jmo-1)
               ilev = imo-1
               if(arcsym(1,ilev).ne.jklsym)go to 500
c***********************************************************
c              i<j<k=l loops:
c***********************************************************
c               if(qprt.ge.5)write(*,6010)lmo,kmo,jmo,imo
               nmo = 3
               mo(1) = imo
               mo(2) = jmo
               mo(3) = lmo
c
c              # 8ab loops:
c
               numv = 2
          call loop( tail, tail, st8, db8, 6, 0, limvec, ref,ndrts )
c
c              # 10 loops:
c
               numv = 1
          call loop( tail, tail, st10, db10, 6, 0, limvec, ref,ndrts )
500         continue
            imo = jmo
c***********************************************************
c           i=j<k=l loops:
c***********************************************************
c            if(qprt.ge.5)write(*,6010)lmo,kmo,jmo,imo
            nmo = 2
            mo(1) = jmo
            mo(2) = lmo
c
c           # 11b loops:
c
            numv = 1
       call loop(tail, tail, st11, db11(1,0,2), 4, 0, limvec,ref,
     .           ndrts)
c
c           # 13 loops:
c
            numv = 1
       call loop( tail, tail, st13, db13, 4, 0, limvec, ref,ndrts)
600      continue
         jmo = kmo
         do 700 imo = 1, (jmo-1)
            ilev = imo-1
            if(arcsym(1,ilev).ne.lsym)go to 700
c***********************************************************
c           i<j=k=l loops:
c***********************************************************
c            if(qprt.ge.5)write(*,6010)lmo,kmo,jmo,imo
            nmo = 2
            mo(1) = imo
            mo(2) = lmo
c
c           # 12abc loops:
c
            numv = 3
      call loop( tail, tail, st12, db12, 4, 0, limvec, ref,ndrts)
700      continue
         imo = jmo
c***********************************************************
c        i=j=k=l loops (none for off-diagonal contributions):
c***********************************************************
800   continue
c
      return
6010  format(' intern4:  lmo,kmo,jmo,imo = ',4i4)
      end
c deck loop
      subroutine loop( btail, ktail, stype, db0, nran, ir0,
     & limvec, ref ,ndrts)
c
c  construct interacting loops and accumulate
c  products of segment values.
c
c  input:
c  btail=    bra tail of the loops to be constructed.
c  ktail=    ket tail of the loops to be constructed.
c  stype(*)= segment extension type of the loops.
c  db0(*)=   delb offsets for the vn(*) segment values.
c  nran=     number of ranges for this loop template.
c  ir0=      range offset for this loop.
c  /cpt/     segment value pointer tables.
c  /cex/     segment type extension tables.
c  /ctab/    segment value tables.
c  /cli/     loop information.
c
c  06-jun-89  symmetry checking added. -rls
c  05-jun-89  skip-vector ref(*) version written. -rls
c  written 07-sep-84 by ron shepard.
c
       implicit none 
C====>Begin Module LOOP                   File cidrtms3.f               
C---->Makedcls Options: All variables                                   
C
C     Parameter variables
C
      INTEGER             NIMOMX
      PARAMETER           (NIMOMX = 128)
      INTEGER             NROWMX
      PARAMETER           (NROWMX = 2**10-1)
      INTEGER             NST
      PARAMETER           (NST = 40)
      INTEGER             NST5
      PARAMETER           (NST5 = 5*NST)
      INTEGER             NTAB
      PARAMETER           (NTAB = 70)
      INTEGER             MAXB
      PARAMETER           (MAXB = 19)
      INTEGER             NMXDRT
      PARAMETER           (NMXDRT = 8)
C
      REAL*8              ZERO
      PARAMETER           (ZERO = 0D0)
      REAL*8              ONE
      PARAMETER           (ONE = 1D0)
      REAL*8              SMALL
      PARAMETER           (SMALL = 1.D-14)
C
C     Argument variables
C
      INTEGER             BTAIL,       DB0(NRAN,0:2,*),          IR0
      INTEGER             KTAIL,       LIMVEC(*),   NDRTS,       NRAN
      INTEGER             REF(*),      STYPE(NRAN)
C
C     Local variables
C
      INTEGER             BBRA,        BCASE,       BKET,        BROWC
      INTEGER             BROWN,       BVER,        CLEV,        DELB
      INTEGER             EXT,         I,           IGONV,       IL
      INTEGER             IMU,         IR,          KCASE,       KROWC
      INTEGER             KROWN,       KVER,        NEXR,        NLEV
      INTEGER             R,           STR
C
      LOGICAL             QBCHK(-4:4)
C
C
C     Common variables
      INCLUDE 'cdrt.inc'
      INCLUDE 'cex.inc'
      INCLUDE 'cli.inc'
      INCLUDE 'clim.inc'
      INCLUDE 'cmult.inc'
      INCLUDE 'cpt.inc'
      INCLUDE 'cstack.inc'
      INCLUDE 'ctab.inc'
      INCLUDE 'cwsym.inc'
C====>End Module   LOOP                   File cidrtms3.f               
c
      data qbchk/.true.,.true.,.false.,.false.,.false.,
     &  .false.,.false.,.true.,.true./
c
      bver = min(btail,3)
      kver = min(ktail,3)
c
c     # assign a range (ir1 to nran) for each level of
c     # the current loop type.
c
      hilev = mo(nmo)
c
      ir = ir0
      do 120 i = 1,nmo
         ir = ir+1
         do 110 il = mo(i-1),mo(i)-2
            range(il) = ir
110      continue
         ir = ir+1
         range(mo(i)-1) = ir
120   continue
c
c     # initialize the stack arrays for loop construction.
c
      clev    = 0
      yb(0)   = 0
      yk(0)   = 0
      brow(0) = btail
      krow(0) = ktail
      qeq(0)  = btail .eq. ktail
      wsym(0) = ssym(ndrts)
      v(1,0)  = one
      v(2,0)  = one
      v(3,0)  = one
c
c     # one, two, or three segment value products are accumulated.
c
c     # begin loop construction:
c
      go to 1100
c
1000  continue
c
c     # decrement the current level, and check if done.
c
      extbk(clev) = 0
      clev = clev-1
      if ( clev .lt. 0 ) return
c
1100  continue
c
c     # new level:
c
      if ( clev .eq. hilev ) then
         if ( .not. qeq(hilev) ) then
c           # generate interacting upper walks.
            call intspc( limvec, ref )
         endif
         go to 1000
      endif
c
      nlev = clev+1
      r    = range(clev)
      str  = stype(r)
      nexr = nex(str)
1200  continue
c
c     # new extension:
c
      ext = extbk(clev)+1
      if ( ext .gt. nexr ) go to 1000
      extbk(clev)  = ext
      bcase        = bcasex(ext,str)
      kcase        = kcasex(ext,str)
      brcase(clev) = bcase
      ktcase(clev) = kcase
c
c     # check for extension validity.
c
c     # canonical walk check:
c
      if ( qeq(clev) .and. (bcase .lt. kcase) ) go to 1200
      qeq(nlev) = qeq(clev).and.(bcase.eq.kcase)
c
c     # individual segment check:
c
      browc = brow(clev)
      brown = l(bcase,browc,bver)
      if ( brown .eq. 0 ) go to 1200
      krowc = krow(clev)
      krown = l(kcase,krowc,kver)
      if ( krown .eq. 0 ) go to 1200
      brow(nlev) = brown
      krow(nlev) = krown
c
c     # check b values of the bra and ket walks.
c
      bbra = b(brown)
      bket = b(krown)
      delb = bket-bbra
c
c     # the following check is equivalent to:
c     # if ( abs(delb) .gt. 2 ) go to 1200
c
      if ( qbchk(delb) ) go to 1200
c
      yb(nlev) = yb(clev)+y(bcase,browc,bver)
      yk(nlev) = yk(clev)+y(kcase,krowc,kver)
      if ( bver .eq. 1 ) then
c        # z-z loop.  check both bra and ket.
         if (
     &    ref(yb(nlev)+1) .ge. xbar(brown,1) .and.
     &    ref(yk(nlev)+1) .ge. xbar(krown,1)      ) goto 1200
      else
c        # just the ket check.
         if ( ref(yk(nlev)+1) .ge. xbar(krown,1) ) goto 1200
      endif
      wsym(nlev) = mult( wsym(clev), arcsym(bcase,clev) )
c
c     # accumulate the appropriate number of products:
c
      imu = mu(nlev,ndrts)
      if (numv.eq.1) then 
      v(1,nlev) = v(1,clev)*tab(pt(bcase,kcase,db0(r,imu,1)+delb),bket)
      if ( abs(v(1,nlev)) .le. small ) go to 1200
      elseif (numv.eq.2) then 
      v(1,nlev) = v(1,clev)*tab(pt(bcase,kcase,db0(r,imu,1)+delb),bket)
      v(2,nlev) = v(2,clev)*tab(pt(bcase,kcase,db0(r,imu,2)+delb),bket)
      if (
     & abs(v(1,nlev)) .le. small  .and.
     & abs(v(2,nlev)) .le. small       ) go to 1200
      else
      v(1,nlev) = v(1,clev)*tab(pt(bcase,kcase,db0(r,imu,1)+delb),bket)
      v(2,nlev) = v(2,clev)*tab(pt(bcase,kcase,db0(r,imu,2)+delb),bket)
      v(3,nlev) = v(3,clev)*tab(pt(bcase,kcase,db0(r,imu,3)+delb),bket)
      if (
     & abs(v(1,nlev)) .le. small  .and.
     & abs(v(2,nlev)) .le. small  .and.
     & abs(v(3,nlev)) .le. small       )go to 1200
      endif 
c
c     # made it past all the checks, this is a
c     # valid segment extension, increment to the next level:
c
      clev = nlev
      go to 1100
c
      end
c deck intspc
      subroutine intspc( limvec, ref )
c
c  determine the interacting walks from a given loop.
c  for a given loop all valid extensions are constructed.
c
c  on return, interacting walks are indicated by limvec(iwalk)=1
c
c  06-jun-89  symmetry checking added. -rls
c  05-jun-89  skip-vector ref(*) version. -rls
c
       implicit none 
C====>Begin Module INTSPC                 File cidrtms3.f               
C---->Makedcls Options: All variables                                   
C
C     Parameter variables
C
      INTEGER             NIMOMX
      PARAMETER           (NIMOMX = 128)
      INTEGER             NROWMX
      PARAMETER           (NROWMX = 2**10-1)
      INTEGER             NMXDRT
      PARAMETER           (NMXDRT = 8)
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
C
C     Argument variables
C
      INTEGER             LIMVEC(*),   REF(*)
C
C     Local variables
C
      INTEGER             BRA,         BROWN,       BVER,        CLEV
      INTEGER             I,           ISTEP,       ISYM,        KET
      INTEGER             KROWN,       KVER,        NLEV,        ROWC
C
C
C     Common variables
C
      INTEGER             ARCSYM(0:3,0:NIMOMX),     B(NROWMX)
      INTEGER             L(0:3,NROWMX,3),          MU(NIMOMX)
      INTEGER             NJ(0:NIMOMX),             NJSKP(0:NIMOMX)
      INTEGER             XBAR(NROWMX,3),           Y(0:3,NROWMX,3)
C
      COMMON / CDRT   /   B,           L,           Y,           XBAR
      COMMON / CDRT   /   ARCSYM,      NJ,          NJSKP,       MU
C
      INCLUDE 'cli.inc'
      INCLUDE 'clim.inc'
      INCLUDE 'cmult.inc'
      INCLUDE 'cstack.inc'
      INCLUDE 'cwsym.inc'
C====>End Module   INTSPC                 File cidrtms3.f               
c
      brown = 0
      krown = 0
      rowc  = 0
c
      bver = min( brow(0), 3 )
      kver = min( krow(0), 3 )
c
      do 10 i = hilev, niot
         step(i) = 4
10    continue
c
c     # compute upper walks starting from brow(hilev) which include
c     # at least one reference csf for either the bra or ket.
c
      clev = hilev
c
c     # check levels to make sure there are upper walks.
c
      if ( hilev .eq. niot ) go to 500
c
      row(clev) = brow(hilev)
c
100   continue
c     # next step at the current level.
      nlev = clev+1
      istep = step(clev)-1
      if ( istep .lt. 0 ) then
c        # decrement current level.
         step(clev) = 4
         if ( clev .eq. hilev ) return
         clev = clev-1
         go to 100
      endif
      step(clev) = istep
      rowc = row(clev)
      brown = l(istep,rowc,bver)
      if ( brown .eq. 0 ) go to 100
      krown = l(istep,rowc,kver)
      if ( krown .eq. 0 ) go to 100
      if ( brown .ne. krown ) then
         call bummer('intspc: l(*,*,*) error.',0,faterr)
      endif
      row(nlev) = krown
      yb(nlev) = yb(clev)+y(istep,rowc,bver)
      yk(nlev) = yk(clev)+y(istep,rowc,kver)
c
c     # check for possible interacting extensions from this row.
      if ( bver .ne. 1 ) then
         if ( ref(yk(nlev)+1) .ge. xbar(krown,1) ) go to 100
      else
c        # z-z loop; check both bra and ket for possibilities.
         if (
     &    ref(yb(nlev)+1) .ge. xbar(brown,1) .and.
     &    ref(yk(nlev)+1) .ge. xbar(krown,1)      ) go to 100
      endif
c
      wsym(nlev) = mult( wsym(clev), arcsym(istep,clev) )
      if ( nlev .lt. niot ) then
c        # increment to next level
         clev = nlev
         go to 100
      endif
500   continue
c
c     # check for reference interaction...
c
      isym = wsym(niot)
      ket = yk(niot)+1
      if ( ket .gt. xbar(1,1) ) then
         call bummer('intspc: ket too large, ket=',ket,faterr)
      endif
      bra = yb(niot)+1
      if ( ref(ket) .eq. 0 ) then
         if ( nexw(isym,bver) .ne. 0 ) limvec(bra) = 1
      endif
      if ( bver .eq. 1 ) then
         if ( ref(bra) .eq. 0 ) then
            if ( nexw(isym,1) .ne. 0 ) limvec(ket) = 1
         endif
      endif
      go to 100
c
      end
      subroutine print_revision3(name,nlist)
      implicit none
      integer nlist
      character*12 name
      character*70 string
  50  format('* ',a,t12,a,t40,a,t68,' *')
      write(string,50) name(1:len_trim(name)) // '3.f',
     .   '$Revision: 2.5.6.1 $','$Date: 2013/04/11 14:37:29 $'
      call substitute(string)
      write(nlist,'(a)') string
      return
      end

