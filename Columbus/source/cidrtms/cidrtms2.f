!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/

      subroutine driver( core, lencor, mem1, ifirst )
c
c  drt construction, reference csf selection, and internal walk
c        selection for the columbus mrsdci program "ciudg".
c                  written by ron shepard.
c
c   three-index drt version with generalized interactive space
c     limitation written by hans lischka and peter g. szalay
c
c***********************************************************************
c
c   the following computer programs contain work performed
c   partially or completely by the argonne national laboratory
c   theoretical chemistry group under the auspices of the office
c   of basic energy sciences, division of chemical sciences,
c   u.s. department of energy, under contract w-31-109-eng-38.
c
c   these programs may not be (re)distributed without the
c   written consent of the argonne theoretical chemistry group.
c
c   since these programs are under development, correct results
c   are not guaranteed.
c
c***********************************************************************
c
       implicit none 
C====>Begin Module DRIVER                 File cidrtms2.f               
C---->Makedcls Options: All variables                                   
C
C     External functions
C
      EXTERNAL            FORBYT,      JOIN
C
      INTEGER             FORBYT,      JOIN
C
C     Parameter variables
C
      INTEGER             NROWMX
      PARAMETER           (NROWMX = 2**10-1)
      INTEGER             NMXDRT
      PARAMETER           (NMXDRT = 8)
      INTEGER             LENBUF
      PARAMETER           (LENBUF = 1600)
      INTEGER             NMOMX
      PARAMETER           (NMOMX = 1023)
      INTEGER             NIMOMX
      PARAMETER           (NIMOMX = 128)
      INTEGER             TMIN0
      PARAMETER           (TMIN0 = 0)
      INTEGER             TMINIT
      PARAMETER           (TMINIT = 1)
      INTEGER             TMPRT
      PARAMETER           (TMPRT = 2)
      INTEGER             TMREIN
      PARAMETER           (TMREIN = 3)
      INTEGER             TMCLR
      PARAMETER           (TMCLR = 4)
      INTEGER             TMSUSP
      PARAMETER           (TMSUSP = 5)
      INTEGER             TMRESM
      PARAMETER           (TMRESM = 6)
      INTEGER             YES
      PARAMETER           (YES = 1)
      INTEGER             NO
      PARAMETER           (NO = 0)
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
      INTEGER             TOINDX
      PARAMETER           (TOINDX = 0)
      INTEGER             TO01
      PARAMETER           (TO01 = 1)
      INTEGER             MULTMX
      PARAMETER           (MULTMX = 19)
      INTEGER             NMULMX
      PARAMETER           (NMULMX = 9)
      INTEGER             MAXCD
      PARAMETER           (MAXCD = 40)
*@ifdef int64
      INTEGER             WORDLEN
      PARAMETER           (WORDLEN = 64)
*@else
*      INTEGER             WORDLEN
*      PARAMETER           (WORDLEN = 32)
*@endif
C
C     Argument variables
C
      INTEGER             IFIRST,      LENCOR,      MEM1
C
      REAL*8              CORE(LENCOR)
C
C     Local variables
C
      CHARACTER           CHRPRN
      CHARACTER*80        FNAME,       TITLE
C
      INTEGER             BMAX2(NIMOMX),            BMIN2(NIMOMX)
      INTEGER             I,           ICD(MAXCD),  ICHAR,       IERR
      INTEGER             II,          IRFN,        IRFNJ,       ITIME
      INTEGER             J,           LEVPRT(2),   NCSFT(NMXDRT)
      INTEGER             NDOT2,       NDRTS,       NREFMX,      NREM
      INTEGER             NREMX,       NROW,        NUMBEROFDRTS
      INTEGER             NUMREP,      NVALWT(NMXDRT),           NWALK
      INTEGER             NWALKO,      OCCMAX2(NIMOMX)
      INTEGER             OCCMIN2(NIMOMX),          SMASK2(NIMOMX)
      INTEGER             TOPREF
C
      LOGICAL             INIT,        TRUE,        YNTOT
C
      INCLUDE 'cdrt.inc'
      INCLUDE 'cfiles.inc'
      INCLUDE 'cli.inc'
      INCLUDE 'clim.inc'
      INCLUDE 'cmult.inc'
      INCLUDE 'cstack.inc'
      INCLUDE 'cwsym.inc'
      INCLUDE 'input1.inc'
      INCLUDE 'input2.inc'
      INCLUDE 'input3.inc'
      INCLUDE 'input4.inc'
      INCLUDE 'so.inc'
C====>End Module   DRIVER                 File cidrtms2.f               
c
c
c     # maximum number of distinct rows...
c     integer   nrowmx
c     parameter(nrowmx=2**10-1)
c
c     # maximum number of DRTs
c     integer nmxdrt
c     parameter (nmxdrt=8)

c
c     # drt output buffer length...
c     integer   lenbuf
c     parameter(lenbuf=1600)
c
c     # maximum number of molecular orbitals...
c     integer   nmomx
c     parameter(nmomx=1023)
c
c     # maximum number of internal orbitals...
c     integer   nimomx
c     parameter(nimomx=51)

c
c     # /cli/, /cdrt/ and /cstack/ are used in the computation of the
c     # interacting space.
c
c----------------------------------------------------------------------
c
c     # /cwsym/ and /clim/ hold some additional information used
c     # by limint().
c
c----------------------------------------------------------------------
c
      data chrprn / ' ' /
c
      nin    = 5
      nout   = 6
      nlist  = 6
      ndrt   = 50
c
      call izero_wr( 8, nmpsy, 1 )
      call izero_wr( 8, nmskp, 1 )
      call izero_wr( 8, nexo,  1 )
c
      call ibummr( nlist )
      call timer( ' ', tmin0, itime, nlist )
c
      write(nlist,'(a)') chrprn
      call progheader(nlist)
c
      write(nlist,6040) lencor, mem1, ifirst
c
c
c     # initialize some scalars.
      nwalk  = 0
      ndp1   = 0
      call izero_wr(nmxdrt,nref,1)
      call izero_wr(nmxdrt,nvalwt,1)
      nrem   = 0
      nkey  = 21
c
      spnorb = .false.
      spnodd = .false.
c


c
c     # open the expanded keystroke files.
c

c
c      Scheme:
c               1. read in number of DRTs
c               2. read in input common to all DRTs
c               3. For all DRTs
c                       read DRT specific input
c                  endfor
c               4. Construct the 1-DRT subject
c                  restrictions applied (occmin,occmax ... )
c                  are set so that a superset of all configurations
c                  spaces is guaranteed
c               5. construct the chaining information for the 1-DRT
c               6. For all DRTs
c                       construct the references index vector
c                       apply excitation limit to each reference
c                       mark all passed vertices and arcs that are
c                       used by any of the excitations applied to any
c                       DRT (superset of all CSFs)
c                  endfor
c               7. Remove all unmarked vertices and arcs from the 1-DRT
c                  and create the 3-DRT
c               8. for all DRTs
c                     convert reference index vector to new indexing sch
c                  endfor
c               9. calculate the superset of all references, apply
c                  the generalized interacting space restrictions
c                  recalculate the l-values and rebuild chaining indices
c              10. for all DRTs
c                    convert reference index to new indexing scheme
c                    create index vector (limint) for this DRT and
c                      apply generalized interacting space restrictions
c                    apply manual CSF selections for this DRT
c                    calculate walk symmetry
c                    write the DRT to file cidrtfl.#
c                   endfor
c


      write(nout,*)
     & 'expanded "keystrokes" are being written to files:'
      fname='cidrtky'
      open(unit=nkey+1,file=fname,status='unknown')
      write(*,*)'                      ',fname


c    ------------------------------------------------------------
c
c     Read in the number of DRTs
c

101   continue
      numberofdrts = 0
      write(nlist,*)
       nkl=1
       nk1=1
      call ini(numberofdrts,'number of DRTs to be constructed',*101)
      if (numberofdrts.gt.nmxdrt) then
        write(nlist,*) ' no more than ',nmxdrt, ' DRTs.'
        goto 101
      endif

c    ------------------------------------------------------------




      write(nout,*)
     & 'DRT infos are being written to files:'
      do 1000  ndrts=1,numberofdrts
        if(ndrts.lt.10) then
          write(fname,996) ndrts
        else
          write(fname,997) ndrts
        endif
        open(unit=ndrt+ndrts,file=fname,status='unknown')
        write(nout,*) fname
1000  continue

 996  format('cidrtfl.',i1)
 997  format('cidrtfl.',i2)

c    ------------------------------------------------------------
c
c    Read input common to all DRTs
c


      write(*,*) 'starting with the input common to all drts ... '

      nk1=1
c     nkl=numberofdrts
      nkl=1

      call wcomm(3,1)

      call commoninp

c    ------------------------------------------------------------
c
c     Read DRT specific input
c

      do 1100  ndrts=1,numberofdrts
        call wcomm(1,ndrts)
        nk1=1
        nkl=1
        call specificinp(ndrts)
        call wcomm(2,ndrts)
1100  continue

c    ------------------------------------------------------------

      call wcomm(3,1)

541   continue
      write(nlist,*)
      ichar = no
      call inyn(ichar,
     & 'impose generalized interacting space restrictions?',*541)
c
c     # the following change allows more general use of limint(). -rls
c     # old:     qgis=(ichar.eq.yes).and.(ndot.gt.0)
c
      qgis = (ichar .eq. yes)
      if(qgis)then
         write(nlist,*) 'generalized interacting space restrictions'
     &    //' will be imposed.'
         if(exmax.ne.2)then
            write(nlist,6100)'inconsistent input.'
     &       //' gis restrictions with exmax.ne.2',exmax
            goto 541
         endif
      else
         write(nlist,*) 'generalized interacting space restrictions'
     &    //' will not be imposed.'
      endif

c
      icd(1) = 1

c
c     # initialize the spin function irrep and multp arrays
c
c      write(*, *)'multp', multp
c      write(*, *)'spnir'
c      write(*,"(19i3)")spnir
      if(spnodd)then
         hmult = hmult + 1
         smult = hmult
         neli = neli + 1
         niot = niot + 1
      endif
      call inisp
c
c
c
c
c  Should this feature still be here??
c

 570  continue
      nremx = ( (lencor+1-icd(1))/3 ) * (2 / forbyt(2))
      call vrem( nremx, core(icd(1)), nrem, *570 )
      icd(2) = icd(1) + forbyt( 3*nrem )


c -------------------------------------------------------------
c      construct the 1-DRT
c
c
c     # 1-index drt must encompass the superset of all DRTs
c     # hence, - produce a general occmin,occmax bmin,bmax,
c     # smask vector set from the information for all DRTs
c
       do i=1,niot
        occmin2(i)=occmin(i,1)
        occmax2(i)=occmax(i,1)
        bmin2(i) = bmin(i,1)
        bmax2(i) = bmax(i,1)
        smask2(i) = smask(i,1)
       enddo
        ndot2=ndot(1)
       do j=2,numberofdrts
       do i=1,niot
         occmin2(i)=min(occmin2(i),occmin(i,j))
         occmax2(i)=max(occmax2(i),occmax(i,j))
         bmin2(i) = min(bmin2(i),bmin(i,j))
         bmax2(i) = max(bmax2(i),bmax(i,j))
         smask2(i)= join(smask(i,j),smask2(i))
       enddo
         ndot2=min(ndot(j),ndot2)
       enddo

 620   continue

c
c  # prepare a suitable DRT covering all states
c

      call condrt(
     & a,      b,             l,       nj,
     & njskp,  niot,          neli,    smult,
     & nrem,   core(icd(1)),  occmin2,  occmax2,
     & bmin2,   bmax2,          ndot2,    qgis,
     & smask2,  nrow,          nrowmx,  spnorb,
     & multp,  nmul,          spnodd )
c
c     # delete selected arcs and vertices for noninteracting csfs.
c     # hope this is ok ...  tm
c
      if ( qgis )  then
        call gisarc( a, b, l, nj, njskp, ndot2, nrow, nrowmx )
      endif
c
c     # manually remove arcs.
c
640   continue
c     # note: should this operation be applied instead (or also)
c     #       to the 3-index drt?  I think it should. -rls

c
c  # temporarily disable this option
c  # as this affects ALL DRTs and hence it is rather difficult
c  # to be sure about what one is doing - may be allow it
c  # restricted to the level zero ...
c

       call remarc( a, b, l, nj, njskp, niot, nrow, *620 )


c
c     # for the resulting 1-index drt, construct chaining information.
c
      call chain( niot, nj, njskp, l, y, xbar, nrow, nrowmx, nwalk, 1,
     & spnorb, multp, nmul, spnodd )
c

c -------------------------------------------------------------

      if (2+numberofdrts+5 .gt.maxcd)
     .  call bummer('increase maxcd to at least ',
     .  2+numberofdrts+5,2)

c
c    core memory:
c    icd(1) ... icd(ndrts)   compressed reference vectors for all DRTs
c    topref = icd(ndrts+1)   rdiff  (qxlim)
c    topref+1 = icd(ndrts+2) rstep  (qxlim)
c    topref+2 = icd(irfn)      scratch space for converting ref vectors
c    topref+3 = icd(irfnj)     uncompressed joined reference vector
c    topref+4 =  other temp stuff

      do ndrts=1,numberofdrts
      icd(1+ndrts)=icd(ndrts)+forbyt(((xbar(1,1)-1)/wordlen)+1)
      enddo
      topref=1+numberofdrts
c
      if ( (icd(topref)-1) .gt. lencor )
     .   call bummer('core memory, need at least',icd(topref)-1,2)

      call printadr(topref,icd)
c
660   continue
c     levprt(1) = 0
      levprt(1) = 1
      levprt(2) = niot
      call iniv(levprt,2,
     & 'input the range of drt levels to print (l1,l2)',*640)
      write(nout,6200)'levprt(*)',levprt
      if(levprt(1).ge.0) then
         call prcdrt(
     &    levprt, niot,   nrow,   nrowmx,
     &    nsym,   nj,     njskp,  modrt,
     &    syml,   slabel, a,      b,
     &    l,      y,      xbar,   chrprn )
      endif
c
c
c     # make three identical copies of the drt chaining arrays.
c     # this is the initial 3-index drt, but the indexing scheme
c     # is the same as the original 1-index drt.
c
      call lcopy( nrow, nrowmx, y, xbar, l )


c
c   # allow individual reference space selection for
c   # each drt

      init=.true.
      do 1200 ndrts=1,numberofdrts
        call wcomm(1,ndrts)

661     continue
        write(*,*) ' INPUT FOR DRT # ',ndrts

        nrefmx = xbar(1,1)

c
c       create the reference space
c       store the reference index vector in compressed form
c

        call selctr(
     &    niot,   nrow,   nrowmx,        neli,
     &    nwalk,  nref(ndrts), nrefmx,  ssym(ndrts),
     &    nsym,   a,      b,             l,
     &    y,      xbar,   slabel,        modrt,
     &    step,   row,    ytot,          arcsym,
     &    wsym,   bminr,  bmaxr,         occmnr,
     &    occmxr, smaskr, core(icd(ndrts)),  mult,
     &    qgis, syml,nmskp,  *661, spnorb, spnodd )
c
c     # input reference occupations mu(*).
c
 720   continue
c


        if(ndot(ndrts).gt.0)call iset(ndot(ndrts),2,mu(1,ndrts),1)
        if(nact(ndrts).gt.0)call izero_wr(nact(ndrts),mu(ndp1,ndrts),1)
c
         write(nlist,*)
         call iniv(mu(1,ndrts),
     .     niot,'input the reference occupations, mu(*)',*660)
          write(nlist,*) 'reference occupations:'
         write(nlist,6200)'mu(*)=',(mu(i,ndrts),i=1,niot)
         write(nlist,*)
         do 730 i = 1,niot
             if(mu(i,ndrts).lt.0 .or. mu(i,ndrts).gt.2)then
                 write(nlist,*) 'mu(*) must be 0,1, or 2'
                 go to 720
             endif
730       continue

c           core(topref):rstep(*),core(topref+1):rdiff(*)
c           generate reference information
c           we just keep the information for the current DRT
c
            icd(topref+1) = icd(topref) +
     .                forbyt( nref(ndrts)*(niot+1) )
            icd(topref+2) = icd(topref+1) +
     .                forbyt( nref(ndrts)*(niot+1) )
c
c
c           uncompress reference vector
c           calculate rstep and rdiff vectors
c           recompress reference vector
c
            irfn=topref+2
            icd(irfn+1)=icd(irfn)+forbyt(xbar(1,1))
            call printadr(irfn+1,icd)
            if ( (icd(irfn+1)-1) .gt. lencor )
     .   call bummer('core memory, need at least',icd(irfn+1),2)

            call uncmpref(core(icd(ndrts)),core(icd(irfn)),xbar(1,1))
            call scref(
     &         niot,   nrow,   nrowmx, nref(ndrts),
     &         l,      y,      xbar,   core(icd(irfn)),
     &         ytot,   step,   row,    core(icd(topref)),
     &         spnorb, b,      spnodd )
            call cmpref(core(icd(ndrts)),core(icd(irfn)),xbar(1,1))

c
c        apply excitation selection and create a list of
c        valid arcs for the 1-DRT
c
c        # core=>icd(ndrts):ref(*)
c        #       icd(topref):rstep(*),icd(topref+1):rdiff(*)


c        qxlim must be always true
         if ( .not. qxlim) call bummer ('qxlim=.false.',0,2)

         call exlimwnew(
     &    niot,   nrow,          nsym,
     &    nwalk,  nref(ndrts),  ssym(ndrts), qxlim,
     &    exmax,  l,        y,
     &    xbar,   row,    ytot,          step,
     &    wsym,   arcsym,   core(icd(topref)), core(icd(topref+1)),
     &    mult,   nexw,  ncsft(ndrts),    ncsfsm(1,1,ndrts),
     &    nvalw(1,ndrts),  nviwsm(1,1,ndrts),slabel ,init, spnorb,
     &    spnir,  b, multmx,     spnodd )
          init=.false.

         call wcomm(2,ndrts)
1200     continue


        call wcomm(3,ndrts)

c       first, copy old drt indexing arrays

        call drtcpy(nrow,nrowmx,y,xbar,l,yold,xbold,lold)

c       generate the pruned 3-DRT where all invalid arcs and
c       invalid vertices are removed based on the analysis of
c       exlimwnew before, i.e. calculate new l array,
c       recalculate chaining indices
c
        write(nout,*) 'Generating 3-DRT at exlimw level ... '

        call newl(nlist,niot,nrow,yold,lold,xbold,step,ytot,yntot,
     .            row,l)

c       calculate new y(*) and xbar(*)

        nwalko = nwalk
        call chain( niot, nj, njskp, l, y, xbar, nrow, nrowmx, nwalk,3,
     &       spnorb, multp, nmul, spnodd )
        write(nout,6200)'levprt(*)',levprt
        if ( levprt(1) .ge. 0 ) then
            call pr3drt(
     &             levprt, niot,   nrow,   nrowmx,
     &             nsym,   nj,     njskp,  modrt,
     &             syml,   slabel, a,      b,
     &             l,      y,      xbar,   chrprn )
        endif

       init=.true.

c     from now on the uncompressed reference vector at most xbar(1,1)
c     elements long - much less than the old value of xbold(1,1)
c     so reassign some of the icd pointers

       irfn=topref+2
       icd(irfn+1)=icd(irfn)+forbyt(xbar(1,1))
       irfnj=irfn+1
       icd(irfnj+1)=icd(irfnj)+forbyt(xbar(1,1))
       call printadr(irfnj+1,icd)
c
       do 1300 ndrts=1,numberofdrts

        call wcomm(1,ndrts)
        write(nout,*) 'Converting reference vector for DRT #',ndrts
c
c       core(icd(ndrts): ref compressed index
c       core(icd(irfn)): refnew  as CSF indices
c

        call refnew(
     .   niot, xbold(1,1), core(icd(ndrts)), nref(ndrts),
     .   core(icd(irfn)),
     .   nrow, y, l, yold, xbold, lold, step, ytot, ktcase,
     .   row, spnorb, b, spnodd )
c
c       core(icd(ndrts)) refnew converted into compressed index form
c

        call indx01r(nlist,nref(ndrts),
     .     xbar(1,1),core(icd(ndrts)),core(icd(irfn)),to01)

c
c #     calculate joined set of reference vectors
c       in core(icd(irfnj)) as uncompressed index vector
c

        call joinref(core(icd(ndrts)),
     .      core(icd(irfnj)),xbar(1,1),init)

        init=.false.

        call wcomm(2,ndrts)
1300   continue

        call wcomm(3,ndrts)

      if  (qgis)then

         icd(irfnj+2)=icd(irfnj+1)+forbyt(nwalk)
         if ( (icd(irfnj+2)-1) .gt. lencor)
     .   call bummer('core memory, need at least',icd(irfnj+2),2)

         call printadr(irfnj+1,icd)

c
c # the next step determines the JOINED interacting space over
c # all DRTs in attempt to minimize the number of chaining
c # indices and to reduce size of the 3-DRT
c # For high spin cases this step may be efficient.
c

c  joined reference vector : icd(irfnj)
c  limint vector : core(icd(irfnj+1))

         call limintnew( core(icd(irfnj+1)),core(icd(irfnj)),
     .             nvalw(1,numberofdrts),numberofdrts,.true.)

         write(nout,*) 'JOINED number of valid walks:',
     .    'nvalw(*,',numberofdrts,')='
     .    ,nvalw(1,numberofdrts),nvalw(2,numberofdrts),
     .     nvalw(3,numberofdrts),nvalw(4,numberofdrts)

c        # save old drt indexing arrays

         call drtcpy(nrow,nrowmx,y,xbar,l,yold,xbold,lold)

c     # for the current limvec(*), prune the l(*) chaining indices.
c     # ktcase(*) is used for scratch.
c
          call lprune(
     &         nlist,          niot,   nwalk,   core(icd(irfnj+1)),
     &         core(icd(irfnj)),   nrow,   nrowmx,  yold,
     &         lold,           xbold,  step,    ytot,
     &         ktcase,         row,    l,       spnorb,
     &         b,              spnorb )

c
c
c     # using the new l(*), recompute the rest of the 3-index chaining
c     # indices.
c     => this defines the final indexing scheme. <=
c
      nwalko = nwalk
       call chain( niot, nj, njskp, l, y, xbar, nrow, nrowmx, nwalk, 3,
     &         spnorb, multp, nmul, spnodd )
      write(nout,6200)'levprt(*)',levprt
      if ( levprt(1) .ge. 0 ) then
         call pr3drt(
     &    levprt, niot,   nrow,   nrowmx,
     &    nsym,   nj,     njskp,  modrt,
     &    syml,   slabel, a,      b,
     &    l,      y,      xbar,   chrprn )
      endif

      do 1400  ndrts=1,numberofdrts
         call wcomm(1,ndrts)

c
c        recompute the reference vector with new chaining indices
c        and overwrite the old one in core(icd(ndrts))
c

        call refnew(
     .   niot, xbold(1,1), core(icd(ndrts)), nref(ndrts),
     .   core(icd(irfn)),
     .   nrow, y, l, yold, xbold, lold, step, ytot, ktcase,
     .   row, spnorb, b, spnodd )
c
      call indx01r(
     & nlist, nref(ndrts), xbar(1,1), core(icd(ndrts)),
     .      core(icd(irfn)), to01 )

c
c     recompute limvec for this particular DRT applying
c     generalized interactings space restrictions
c     limint: core(icd(irfnj+1))
ctm2008  for the construction of the individual index 
ctm2008  vectors, we must not use the joined reference
ctm2008  index vector
ctm2008  individ. refindex : core(icd(irfn)) 
ctm2008    .. in 01 notation, compressed: core(icd(ndrts))
ctm2008    .. in 01 uncompressed: core(icd(irfnj))


      call uncmpref(core(icd(ndrts)),core(icd(irfnj)),xbar(1,1))
      call limintnew( core(icd(irfnj+1)),core(icd(irfnj)),
     .      nvalw(1,ndrts),ndrts,.false.)

c
c     # what remains to be done, is
c     #       o  manual walk selection
c     #       o  symmetry vector calculation
c     #       o  write the individual drt files
c
c
c     # based on the current chaining indices, perform any manual
c     # mrsdci walk selection...
c
710    continue
      call manlim(
     & niot,   nrow,  nrowmx,         nsym,
     & nwalk,  ssym(ndrts),  core(icd(irfnj+1)),  l,
     & y,      xbar,  arcsym,         step,
     & mult,   nexw,  nvalw(1,ndrts),          *710 )

c    since this step should not largely simplify the drt
c    we do not recompute chaining indices

c     # calculate the symmetry of the valid walks.
c     # csym(i)=mult( walk_symmetry_i, ssym)
c     # use limvec(*) and nexw(*) to calculate the final ncsft.
c     # note:  remove z-walk csym(*) entries later. -rls
c
      nvalwt(ndrts)=nvalw(1,ndrts)+nvalw(2,ndrts)+nvalw(3,ndrts)
     .   +nvalw(4,ndrts)
         icd(irfnj+3)=icd(irfnj+2)+forbyt(nvalwt(ndrts))
         if ( (icd(irfnj+3)-1) .gt. lencor)
     .   call bummer('core memory, need at least',icd(irfnj+3),2)

         call printadr(irfnj+3,icd)
      call symwlk(
     & niot,   nsym,          nrow,  nrowmx,
     & nwalk,  l,             y,     row,
     & step,   core(icd(irfnj+2)), wsym,   arcsym,
     & ytot,   xbar,          slabel, ssym(ndrts),
     & mult,   core(icd(irfnj+1)), nexw,   ncsft(ndrts),
     & nviwsm(1,1,ndrts), ncsfsm(1,1,ndrts),  spnorb, b,
     & spnir,  multmx,       spnodd)
      write(nlist,*) 'Core memory requirements:',
     .  icd(irfnj+2),' DWords'
c
740   continue
c
        nk1=1
        nkl=1
        write(nlist,*)
        call inchst(title,'input a title card','cidrt_title',
     .   *740 )
        write(nlist,*) 'title card for DRT # ',ndrts
        write(nlist,*) title

        write(nout,*)'drt file is being written... for DRT #',ndrts
c
         icd(irfnj+4) = icd(irfnj+3) + forbyt( lenbuf )
c
         if ( (icd(irfnj+4)-1) .gt. lencor )
     .   call bummer('core memory, need at least',icd(irfnj+4),2)
c

         call printadr(irfnj+4,icd)

          nk1=ndrts
         call wrthd(
     &    ndrt+nk1,  title,         nmot,  niot,
     &    nfct,  nfvt,          nrow,  nsym,
     &    ssym(ndrts),  nrowmx,        xbar,  nvalw(1,ndrts),
     &    ncsft(ndrts), core(icd(irfnj+3)),  lenbuf ,
     &    spnorb,spnodd, lxyzir,hmult )
c
c        # note: may eliminate z-walk csym(*) computation later. -rls.
c
      call uncmpref(core(icd(ndrts)),core(icd(irfn)),xbar(1,1))
         call wrtdrt(
     &    nmot,         niot,         nsym,   nrow,
     &    nrowmx,       nwalk,        lenbuf, core(icd(irfnj+3)),
     &    map,          nmpsy,        slabel, nexo,
     &    nviwsm(1,1,ndrts),ncsfsm(1,1,ndrts),
     .         mu(1,ndrts),     modrt,
     &    syml,         nj,           njskp,  a,
     &    b,            l,            y,      xbar,
     &    core(icd(irfn)), core(icd(irfnj+2)), nvalw(1,ndrts),
     .      core(icd(irfnj+1)) )
c
      close(unit=ndrt+nk1)
c
      call wcomm(2,ndrts)

 1400  continue

       else
c
c    selection through excitation limitation only
c
c    for each DRT calculate the index vector and possibly
c    apply manual CSF selection
c
        do 1500  ndrts=1,numberofdrts
         nk1=1
         nkl=1

         call wcomm(1,ndrts)

c
c   # this needs all of the rstep  vectors ...
c
         if(qxlim)then
            icd(topref+1) = icd(topref) +
     .                forbyt( nref(ndrts)*(niot+1) )
            icd(topref+2) = icd(topref+1) +
     .                forbyt( nref(ndrts)*(niot+1) )
c
            irfn=topref+2
            icd(irfn+1)=icd(irfn)+forbyt(xbar(1,1))
            call printadr(irfn+1,icd)
            if ( (icd(irfn+1)-1) .gt. lencor )
     .   call bummer('core memory, need at least',icd(irfn+1),2)

            call uncmpref(core(icd(ndrts)),core(icd(irfn)),xbar(1,1))

            call scref(
     &       niot,   nrow,   nrowmx, nref(ndrts),
     &       l,      y,      xbar,   core(icd(irfn)),
     &       ytot,   step,   row,    core(icd(topref)),
     &       spnorb, b,      spnodd)
            call cmpref(core(icd(ndrts)),core(icd(irfn)),xbar(1,1))
         else
c           # rstep(*) and rdiff(*) are not referenced.
            icd(topref+1) = icd(topref)
            icd(topref+2) = icd(topref)
         endif
         irfn=topref+2
         icd(irfn+1)=icd(topref+2)+forbyt(nwalk)
         if ( (icd(irfn+1)-1) .gt. lencor)
     .     call bummer ('core memory: need at least',icd(irfn+1),2)

c
c       icd(irfn) : limvec
c       icd(topref): rstep
c       icd(topref+1): rdiff
c
c
        call exlimw (
     .        niot,nrow, nrowmx, nsym,nwalk,nref(ndrts),ssym(ndrts),
     .        qxlim,
     .        exmax,core(icd(irfn)),l,y,xbar, row,ytot, step,
     .        wsym,arcsym,core(icd(topref)),core(icd(topref+1)),mult,
     .        nexw,ncsft(ndrts),ncsfsm(1,1,ndrts),
     .        nvalw(1,ndrts),nviwsm(1,1,ndrts),slabel,        spnorb,
     &        spnir,  b, multmx,     spnodd )
c
c     # what remains to be done, is
c     #       o  manual walk selection
c     #       o  symmetry vector calculation
c     #       o  write the individual drt files
c

c
c     # based on the current chaining indices, perform any manual
c     # mrsdci walk selection...
c
711   continue
      call manlim(
     & niot,   nrow,  nrowmx,         nsym,
     & nwalk,  ssym(ndrts),  core(icd(irfn)),   l,
     & y,      xbar,  arcsym,         step,
     & mult,   nexw,  nvalw(1,ndrts),          *711 )

c    since this step should not largely simplify the drt
c    we do not recompute chaining indices

c     # calculate the symmetry of the valid walks.
c     # csym(i)=mult( walk_symmetry_i, ssym)
c     # use limvec(*) and nexw(*) to calculate the final ncsft.
c     # note:  remove z-walk csym(*) entries later. -rls
c
        nvalwt(ndrts)=nvalw(1,ndrts)+nvalw(2,ndrts)+nvalw(3,ndrts)
     .   +nvalw(4,ndrts)
         icd(irfn+2)=icd(irfn+1)+forbyt(nvalwt(ndrts))
         if ( (icd(irfn+2)-1) .gt. lencor)
     .     call bummer ('core memory: need at least ',icd(irfnj+2),2)
c
c       icd(irfn) : limvec
c       icd(irfn+1) : csym
c       icd(irfn+2) : buffer
c       icd(irfn+3) : decompressed reference vector
c

      call symwlk(
     & niot,   nsym,          nrow,  nrowmx,
     & nwalk,  l,             y,     row,
     & step,   core(icd(irfn+1)), wsym,   arcsym,
     & ytot,   xbar,          slabel, ssym(ndrts),
     & mult,   core(icd(irfn)), nexw,   ncsft(ndrts),
     & nviwsm(1,1,ndrts), ncsfsm(1,1,ndrts),  spnorb, b,
     & spnir,  multmx,       spnodd)
c
         write(nout,*)'drt file is being written...'
c        # write header info, drt, and indexing arrays to the drt file.
c
         icd(irfn+3) = icd(irfn+2) + forbyt( lenbuf )
         icd(irfn+4) = icd(irfn+3) + forbyt( xbar(1,1) )
c
         if ( (icd(irfn+4)-1) .gt. lencor )
     .     call bummer ('core memory: need at least ',icd(irfn+3),2)
c

         call printadr(irfn+3,icd)
c
        nk1=1
        nkl=1
 741    continue
        write(nlist,*)
        call inchst(title,'input a title card','cidrt_title',
     .   *741 )
        write(nlist,*) 'title card for DRT # ',ndrts
        write(nlist,*) title

         write(nout,*)'drt file is being written... for DRT #',ndrts

          nk1=ndrts
          call wrthd(
     &         ndrt+nk1,  title,         nmot,  niot,
     &         nfct,  nfvt,          nrow,  nsym,
     &         ssym(ndrts),  nrowmx,        xbar,  nvalw(1,ndrts),
     &         ncsft(ndrts), core(icd(irfn+2)),  lenbuf,
     &         spnorb,spnodd, lxyzir,hmult )
c
c        # note: may eliminate z-walk csym(*) computation later. -rls.
c
         call uncmpref(core(icd(ndrts)),core(icd(irfn+3)),xbar(1,1))
         call wrtdrt(
     &    nmot,         niot,         nsym,   nrow,
     &    nrowmx,       nwalk,        lenbuf, core(icd(irfn+2)),
     &    map,          nmpsy,        slabel, nexo,
     &    nviwsm(1,1,ndrts), ncsfsm(1,1,ndrts),
     .          mu(1,ndrts),     modrt,
     &    syml,         nj,           njskp,  a,
     &    b,            l,            y,      xbar,
     &    core(icd(irfn+3)), core(icd(irfn+1)), nvalw(1,ndrts),
     .      core(icd(irfn)) )
c
      close(unit=ndrt+nk1)
      call wcomm(2,ndrts)
 1500 continue

      endif

c
      call timer( 'cidrt required', tmclr, itime, nlist )
c
c     # all done.
c
      return
c
6020  format(1x,'spin multiplicity, smult',t38,': ',i3,4x,a)
6030  format(1x,a/(8(i4,':',i4.4)))
6040  format(/' workspace allocation parameters: lencor=',i10,
     & ' mem1=',i10,' ifirst=',i10)
6060  format(1x,a)
6100  format(1x,a,t38,': ',(1x,9i4))
6105  format(1x,a,t38,': ',a4,a,i5)
6200  format(1x,a,(t17,15i4))
6201  format(1x,a,(t17,15a4))
6210  format(1x,a,/1x,8(:'(',i2,', ',a4,') '))
6220  format(1x,a,i2,a)
6300  format(/' core(*) allocation error at ',a,', lencor=',i8,
     & ' icd(*)=',/(1x,10i8))
      end
      subroutine commoninp
c
       implicit none 
C====>Begin Module COMMONINP              File cidrtms2.f               
C---->Makedcls Options: All variables                                   
C
C     External functions
C
      EXTERNAL            FORBYT
C
      INTEGER             FORBYT
C
C     Parameter variables
C
      INTEGER             NROWMX
      PARAMETER           (NROWMX = 2**10-1)
      INTEGER             NMXDRT
      PARAMETER           (NMXDRT = 8)
      INTEGER             LENBUF
      PARAMETER           (LENBUF = 1600)
      INTEGER             NMOMX
      PARAMETER           (NMOMX = 1023)
      INTEGER             NIMOMX
      PARAMETER           (NIMOMX = 128)
      INTEGER             YES
      PARAMETER           (YES = 1)
      INTEGER             NO
      PARAMETER           (NO = 0)
      INTEGER             NSOMX
      PARAMETER           (NSOMX = NIMOMX+1)
      INTEGER             MULTMX
      PARAMETER           (MULTMX = 19)
      INTEGER             NMULMX
      PARAMETER           (NMULMX = 9)
C
C     Local variables
C
      CHARACTER*8         CHAR8,       CMULT(0:8)
      CHARACTER*4         INLAB
C
      INTEGER             FCORB(NIMOMX),            FVORB(NIMOMX)
      INTEGER             I,           ICHAR,       IERR,        IPT
      INTEGER             ISYM,        J,           JKSYM,       JSYM
      INTEGER             KSYM,        LEVPRT(2),   NCSFT(NMXDRT)
      INTEGER             NELF,        NELT,        NEXT,        NI
      INTEGER             NJK,         NREF,        NREM,        NUMJ
      INTEGER             NUMREP,      NVALWT(NMXDRT),           NWALK
      INTEGER             SYMORB(2,NSOMX)
C
      INCLUDE 'cdrt.inc'
      INCLUDE 'cfiles.inc'
      INCLUDE 'cli.inc'
      INCLUDE 'clim.inc'
      INCLUDE 'cmult.inc'
      INCLUDE 'cstack.inc'
      INCLUDE 'cwsym.inc'
      INCLUDE 'input2.inc'
      INCLUDE 'input3.inc'
      INCLUDE 'input4.inc'
      INCLUDE 'so.inc'
C====>End Module   COMMONINP              File cidrtms2.f               
c
      data cmult /
     & 'bad-tet', 'singlet', 'doublet', 'triplet', 'quartet',
     & 'quintet', 'sextet',  'septet',  'big-tet'/

c     # initialize some scalars.
      nwalk  = 0
      ndp1   = 0
      nref   = 0
      call izero_wr(nmxdrt,nvalwt,1)
      nrem   = 0
c
c
      spnorb = .false.
      spnodd = .false.
c
c     # start reading user input.
c
c***********************************************************************
c     molecule description input:
c***********************************************************************
c
c
97    continue
      ichar=no
      call inyn(ichar,
     & 'Spin-Orbit CI Calculation?',*97)
      if ( ichar .eq. yes) then
         write(nlist,*) 'Spin-Orbit CI Calculation'
      else
         write(nlist,*) 'Spin-Free Calculation'
         goto 100
      endif
c
99    continue
      hmult = 0
      write(nlist,*)
      call ini(hmult, 'input the highest multiplicity', *99)
      spnorb = .true.
      smult = hmult
c
c
      if(mod(smult, 2) .eq. 0)then
         spnodd = .true.
      endif
      write(*,*)spnodd
c
c
98    continue
      call izero_wr(3,lxyzir,1)
      call iniv(lxyzir,3,'input lxyzir(*)', *98)
      goto 110
c
c
100   continue
      smult = 0
      write(nlist,*)
      call ini(smult,'input the spin multiplicity',*100)
      numrep = 0
      char8 = cmult(max(0,min(smult,8)))
      write(nlist,6020)smult,char8
      if(smult.le.0)then
         write(nlist,*) '0 < smult'
         go to 100
      endif
c
110   continue
      nelt = 0
      call ini(
     .       nelt,'input the total number of electrons',*100)
      write(nlist,6100)'total number of electrons, nelt',nelt
      if(mod(nelt,2).eq.mod(smult,2))then
         write(nlist,6100)'inconsistent data, nelt,smult',nelt,smult
         go to 110
      elseif(nelt.le.0)then
         write(nlist,*) '0 < nelt'
         go to 110
      elseif(nelt.lt.smult-1)then
         write(nlist,*) 'smult-1 <= nelt'
         go to 110
      endif
c
120   continue
      nsym = 0
      call ini(
     .     nsym,'input the number of irreps (1:8)',*110)
      write(nlist,6100)'point group dimension, nsym',nsym
      if(nsym.le.0 .or. nsym.gt.8)then
         write(nlist,*) '0 < nsym <= 8'
         go to 120
      endif
c
c     # enter symmetry labels  -- eas
123   continue
      ichar = no
      call inyn(ichar,'enter symmetry labels:',*120)
      slabel(0) = ' '
      do 124 i = 1,nsym
         write (slabel(i),'(i4)') i
124   continue
      if ( ichar .eq. no ) then
         write(nlist,6060) 'no symmetry labels entered: defaults taken'
      else
         write(nout,6220) 'enter', nsym, ' labels (a4):'
         do 126 i = 1, nsym
            call inchst(inlab,'enter symmetry label',slabel(i),*123)
            slabel(i) = inlab
126      continue
      endif
      write(nout,6210) 'symmetry labels: (symmetry, slabel)',
     & (i,slabel(i),i = 1,nsym)
130   continue
      call izero_wr(nsym,nmpsy,1)
      call iniv(nmpsy,nsym,'input nmpsy(*)',*120)
      write(nout,6200)'nmpsy(*)=',(nmpsy(i),i = 1,nsym)
c
      nmot = 0
      do 138 isym = 1, nsym
         nmskp(isym) = nmot
         nmot = nmot + nmpsy(isym)
138   continue
      write(nlist,*)
      write(nlist,6060)'  symmetry block summary'
      write(nlist,6200)'block(*)=',(i,i=1,nsym)
      write(nlist,6201)'slabel(*)=',(slabel(i),i=1,nsym)
      write(nlist,6200)'nmpsy(*)=',(nmpsy(i),i=1,nsym)
      write(nlist,*)
      write(nlist,6100)'total molecular orbitals',nmot
      if(nmot.gt.nmomx)then
         write(nlist,6100)'nmot,nmomx',nmot,nmomx
         go to 130
      endif
c
c***********************************************************************
c     orbital input:
c***********************************************************************
c
150   continue
      write(nlist,*)
      call inso(symorb,nsomx,nfct,
     & 'input the frozen core orbitals (sym(i),rmo(i),i=1,nfct)',*100)
      write(nout,6100)'total frozen core orbitals, nfct',nfct
      if (nfct.le.0) then
         write(nlist,*) 'no frozen core orbitals entered'
      endif
      if(nfct.gt.nimomx)then
         write(nout,6100)'nfct,nimomx',nfct,nimomx
         go to 150
      endif
c
      if(nfct.gt.0)then
c
         call symcvt( nsym, 2, symorb, nmpsy, nmskp,
     &    nfct, 1, fcorb, ierr )
         write(nlist,*)
         write(nlist,6200)'fcorb(*)=',(fcorb(i),i=1,nfct)
c
         do 158 i = 1,nfct
            if(fcorb(i).le.0 .or. fcorb(i).gt.nmot)then
               write(nlist,*) '0 < fcorb(i) <= nmot'
               go to 150
            endif
            syml(i) = symorb(1,i)
            do 156 j = 1,i-1
               if(fcorb(j).eq.fcorb(i))then
                  write(nlist,6100)
     &             'error in frozen core orbitals. i,j,fcorb(i)',
     &             i,j,fcorb(i)
                  go to 150
               endif
156         continue
158      continue
         write(nlist,6201)'slabel(*)=',(slabel(syml(i)),i=1,nfct)
c
      endif
c
      nelf = 2*nfct
      neli = nelt-nelf
      write(nlist,*)
      write(nlist,6100)'number of frozen core orbitals',nfct
      write(nlist,6100)'number of frozen core electrons',nelf
      write(nlist,6100)'number of internal electrons',neli
      if(neli.le.0)then
         write(nlist,*) '0 < neli'
         go to 150
      endif
c
250   continue
      write(nlist,*)
      call inso(symorb,nsomx,nfvt,'input the frozen'
     & //' virtual orbitals (sym(i),rmo(i),i=1,nfvt)',*150)
      write(nout,6100)'total frozen virtual orbitals, nfvt',nfvt
      if (nfvt.le.0) then
         write(nlist,'(/a)') ' no frozen virtual orbitals entered'
      endif
      if(nfvt.gt.nimomx)then
         write(nlist,6100)'nfvt,nimomx',nfvt,nimomx
         go to 250
      endif
c
      if(nfvt.gt.0)then
c
         call symcvt( nsym, 2, symorb, nmpsy, nmskp,
     &    nfvt, 1, fvorb, ierr )
         write(nlist,*)
         write(nout,6200)'fvorb(*)=',(fvorb(i),i=1,nfvt)
c
         do 258 i = 1,nfvt
            if(fvorb(i).le.0 .or. fvorb(i).gt.nmot)then
               write(nlist,*) '0 < fvorb(i) <= nmot'
               go to 250
            endif
            syml(i) = symorb(1,i)
            do 254 j = 1,i-1
               if(fvorb(j).eq.fvorb(i))then
                  write(nlist,6100)
     &             'error in frozen virtual orbitals. i,j,fvorb(i)',
     &             i,j,fvorb(i)
                  go to 250
               endif
254         continue
            do 256 j = 1,nfct
               if(fvorb(i).eq.fcorb(j))then
                  write(nlist,6100)'error in frozen virtual orbitals.'
     &             //' i,j,fvorb(i),fcorb(j)',i,j,fvorb(i),fcorb(j)
                  go to 250
               endif
256         continue
258      continue
         write(nlist,6201)'slabel(*)=',(slabel(syml(i)),i=1,nfvt)
c
      endif
c
280   continue
      write(nlist,*)
      modrt(0) = 0
      syml(0) = 0
      call inso(symorb,nsomx,niot,
     & 'input the internal orbitals (sym(i),rmo(i),i=1,niot)',*250)
      write(nout,6100)'niot',niot
      if (niot.le.0) then
         write(nlist,*) 'no internal orbitals entered'
      endif
c
c     # subsequent program steps require at least
c     # two internal orbitals...
c
      if(niot.lt.2)then
         write(nlist,*) '2 <= niot'
         go to 280
      elseif(niot.gt.(nmot-nfct-nfvt))then
         write(nlist,6100)'inconsistent input, niot,nmot,nfct,nfvt',
     &    niot,nmot,nfct,nfvt
         go to 280
      endif
      call symcvt( nsym, 2, symorb, nmpsy, nmskp,
     & niot, 1, modrt(1), ierr )
      write(nlist,*)
      write(nlist,6200)'modrt(*)=',(modrt(i),i=1,niot)
c
      do 288 i = 1,niot
         if(modrt(i).le.0 .or. modrt(i).gt.nmot)then
            write(nlist,*) '0 < modrt(i) <= nmot'
            go to 280
         endif
         syml(i) = symorb(1,i)
         do 282 j = 1,i-1
            if(modrt(i).eq.modrt(j))then
               write(nlist,6100)
     &          'error in internal orbital. i,j,modrt(i)',i,j,modrt(i)
               go to 280
            endif
282      continue
         do 284 j = 1,nfct
            if(modrt(i).eq.fcorb(j))then
               write(nlist,6100)
     &          'error in internal orbital. i,j,modrt(i),fcorb(j)',
     &          i,j,modrt(i),fcorb(j)
               go to 280
            endif
284      continue
         do 286 j = 1,nfvt
            if(modrt(i).eq.fvorb(j))then
               write(nlist,6100)
     &          'error in internal orbital. i,j,modrt(i),fvorb(j)',
     &          i,j,modrt(i),fvorb(j)
               go to 280
            endif
286      continue
288   continue
      write(nlist,6201)'slabel(*)=',(slabel(syml(i)),i=1,niot)
c
      next = nmot-nfct-nfvt-niot
      write(nlist,*)
      write(nlist,6100)'total number of orbitals',nmot
      write(nlist,6100)'number of frozen core orbitals',nfct
      write(nlist,6100)'number of frozen virtual orbitals',nfvt
      write(nlist,6100)'number of internal orbitals',niot
      write(nlist,6100)'number of external orbitals',next
c
      do 290 i = 1, niot
         arcsym(0,i-1) = 1
         arcsym(3,i-1) = 1
         arcsym(1,i-1) = syml(i)
         arcsym(2,i-1) = syml(i)
290   continue
c
c
      if(spnodd)then
         syml(niot + 1) = 1
         arcsym(0, niot) = 1
         arcsym(3, niot) = 1
         arcsym(1, niot) = syml(niot + 1)
         arcsym(2, niot) = syml(niot + 1)
      endif
c
c
c     # calculate map(*). -1:frozen core, 0:frozen virtual.
c
      do 350 i = 1, nmot
         map(i) = -2
350   continue
c
      do 360 i = 1, nfct
         map(fcorb(i)) = -1
360   continue
c
      do 370 i = 1, nfvt
         map(fvorb(i)) = 0
370   continue
c
      do 380 i = 1, niot
         map(modrt(i)) = next + i
380   continue
c
      do 390 i = 1, 8
         nexo(i) = 0
390   continue
      ipt = 0
      i = 0
      do 402 isym = 1, 8
         do 400 ni = 1, nmpsy(isym)
            i = i + 1
            if ( map(i) .eq. -2 ) then
               ipt = ipt + 1
               map(i) = ipt
               nexo(isym) = nexo(isym) + 1
            endif
400      continue
402   continue
c
      write(nlist,*)
      write(nlist,*) 'orbital-to-level mapping vector'
      write(nlist,6200)'map(*)=',(map(i),i=1,nmot)
c
c     # calculate the number of external walks of each symmetry
c     # for each vertex.
c
      call izero_wr(4*8,nexw,1)
c
c     # z-walks (1 effective walk of symmetry 1)...
      nexw(1,1) = 1
c
c     # y-walks...
      do 420 i = 1,8
         nexw(i,2) = nexo(i)
420   continue
c
c     # w,x -walks...
      do 440 jsym = 1,8
         numj = nexo(jsym)
         nexw(1,3) = nexw(1,3)+(numj*(numj-1))/2
         nexw(1,4) = nexw(1,4)+(numj*(numj+1))/2
         do 430 ksym = 1,(jsym-1)
            jksym = mult(ksym,jsym)
            njk = numj*nexo(ksym)
            nexw(jksym,3) = nexw(jksym,3)+njk
            nexw(jksym,4) = nexw(jksym,4)+njk
430      continue
440   continue

       return

6020  format(1x,'spin multiplicity, smult',t38,': ',i3,4x,a)
6030  format(1x,a/(8(i4,':',i4.4)))
6040  format(/' workspace allocation parameters: lencor=',i10,
     & ' mem1=',i10,' ifirst=',i10)
6060  format(1x,a)
6100  format(1x,a,t38,': ',(1x,9i4))
6105  format(1x,a,t38,': ',a4,a,i5)
6200  format(1x,a,(t17,15i4))
6201  format(1x,a,(t17,15a4))
6210  format(1x,a,/1x,8(:'(',i2,', ',a4,') '))
6220  format(1x,a,i2,a)
6300  format(/' core(*) allocation error at ',a,', lencor=',i8,
     & ' icd(*)=',/(1x,10i8))
       end

      subroutine specificinp(ndrts)
       implicit none 
C====>Begin Module SPECIFICINP            File cidrtms2.f               
C---->Makedcls Options: All variables                                   
C
C     External functions
C
      EXTERNAL            FORBYT
C
      INTEGER             FORBYT
C
C     Parameter variables
C
      INTEGER             NROWMX
      PARAMETER           (NROWMX = 2**10-1)
      INTEGER             NMXDRT
      PARAMETER           (NMXDRT = 8)
      INTEGER             LENBUF
      PARAMETER           (LENBUF = 1600)
      INTEGER             NMOMX
      PARAMETER           (NMOMX = 1023)
      INTEGER             NIMOMX
      PARAMETER           (NIMOMX = 128)
      INTEGER             YES
      PARAMETER           (YES = 1)
      INTEGER             NO
      PARAMETER           (NO = 0)
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
      INTEGER             MULTMX
      PARAMETER           (MULTMX = 19)
      INTEGER             NMULMX
      PARAMETER           (NMULMX = 9)
C
C     Argument variables
C
      INTEGER             NDRTS
C
C     Local variables
C
      INTEGER             I,           ICHAR,       J
      INTEGER             LEVPRT(2),   NELA,        NELD,        NUMJ
      INTEGER             NUMREP
C
      INCLUDE 'cfiles.inc'
      INCLUDE 'cli.inc'
      INCLUDE 'cwsym.inc'
      INCLUDE 'input1.inc'
      INCLUDE 'input2.inc'
      INCLUDE 'input3.inc'
      INCLUDE 'input4.inc'
      INCLUDE 'so.inc'
C====>End Module   SPECIFICINP            File cidrtms2.f               
c
      write(*,99)
99    format(20('===='))
      write(*,*) ' Input for DRT number ',ndrts

140   continue
      ssym(ndrts) = 0
      call ini(ssym(ndrts),
     & 'input the molecular spatial symmetry (irrep 1:nsym)',*140)
      if(ssym(ndrts).le.0 .or. ssym(ndrts).gt.nsym)then
         write(nlist,*) '0 < ssym <= nsym'
         go to 140
      endif
      write(nlist,6105)'state spatial symmetry label',
     .      slabel(ssym(ndrts))


c***********************************************************************
c     reference drt input:
c***********************************************************************
c
460   continue
      ndot(ndrts) = 0
      write(nlist,*)
      call ini(ndot(ndrts),
     & 'input the number of ref-csf doubly-occupied orbitals',*460)
      write(nout,6100)'(ref) doubly-occupied orbitals',ndot(ndrts)
      if(ndot(ndrts).lt.0)then
         write(nlist,*) '0 <= ndot(ndrts)'
         go to 460
      endif
      neld = 2*ndot(ndrts)
      nela = neli-neld
      nact(ndrts) = niot-ndot(ndrts)
      if(neld.gt.neli)then
         write(nlist,6100)'neld <= neli',neld,neli
         go to 460
      endif
      do 470 i = 1,ndot(ndrts)
         smaskr(i,ndrts) = 1000
         bminr(i,ndrts) = 0
         bmaxr(i,ndrts) = 0
         occmnr(i,ndrts) = 2*i
         occmxr(i,ndrts) = 2*i
470   continue
c
      if(nact(ndrts).gt.0)then
c
         write(nlist,*)
         write(nlist,6100)'no. of internal orbitals',niot
         write(nlist,6100)'no. of doubly-occ. (ref) orbitals',
     .                     ndot(ndrts)
         write(nlist,6100)'no. active (ref) orbitals',nact(ndrts)
         write(nlist,6100)'no. of active electrons',nela
c
480      continue
c
c        # initialize the active levels.  doubly occupied levels are not
c        # included in the count...
c
         ndp1 = ndot(ndrts)+1
         call izero_wr(nact(ndrts),occmnr(ndp1,ndrts),1)
         call iset(nact(ndrts),nela,occmxr(ndp1,ndrts),1)
         write(nlist,*)
         call inivp(modrt(ndp1),occmnr(ndp1,ndrts),nact(ndrts),
     &    'input the active-orbital, active-electron occmnr(*)',*460)
         call inivp(modrt(ndp1),occmxr(ndp1,ndrts),nact(ndrts),
     &    'input the active-orbital, active-electron occmxr(*)',*480)
c
         write(nlist,*)
         write(nout,6200)'actmo(*) =',(modrt(i),i=ndp1,niot)
         write(nout,6200)'occmnr(*)=',(occmnr(i,ndrts),i=ndp1,niot)
         write(nout,6200)'occmxr(*)=',(occmxr(i,ndrts),i=ndp1,niot)
c
         do 486 i = ndp1,niot
            if(occmnr(i,ndrts).gt.nela)then
               write(nlist,6100)'occmnr(i,ndrts) <= nela, i',i
               go to 480
            endif
            if(occmnr(i,ndrts).gt.occmxr(i,ndrts))then
            write(nlist,6100)'occmnr(i,ndrts) <= occmxr(i,ndrts), i',i
               go to 480
            endif
486      continue
         if(occmxr(niot,ndrts).lt.nela)then
            write(nlist,*) 'nela <= occmxr(niot,ndrts)'
            go to 480
         endif
c
c        # add back in the doubly-occupied electrons...
c
         do 488 i = ndp1,niot
            occmnr(i,ndrts) = occmnr(i,ndrts)+neld
            occmxr(i,ndrts) = occmxr(i,ndrts)+neld
488      continue
c
      endif
c
      write(nout,6060)'reference csf cumulative electron occupations:'
      write(nout,6200)'modrt(*)=',(modrt(i),i=1,niot)
      write(nout,6200)'occmnr(*)=',(occmnr(i,ndrts),i=1,niot)
      write(nout,6200)'occmxr(*)=',(occmxr(i,ndrts),i=1,niot)
c
c
      if(spnodd)then
         occmnr(niot + 1,ndrts) = occmnr(niot,ndrts) + 1
         occmxr(niot + 1,ndrts) = occmxr(niot,ndrts) + 1
      endif
c
c
      if(nact(ndrts).gt.0)then
490      continue
         call izero_wr(nact(ndrts),bminr(ndp1,ndrts),1)
         call iset(nact(ndrts),neli,bmaxr(ndp1,ndrts),1)
         write(nlist,*)
         call inivp(modrt(ndp1),bminr(ndp1,ndrts),nact(ndrts),
     &    'input the active-orbital bminr(*)',*460)
         call inivp(modrt(ndp1),bmaxr(ndp1,ndrts),nact(ndrts),
     &    'input the active-orbital bmaxr(*)',*490)
      endif
c
      write(nout,6060)'reference csf b-value constraints:'
      write(nout,6200)'modrt(*)=',(modrt(i),i=1,niot)
      write(nout,6200)'bminr(*)=',(bminr(i,ndrts),i=1,niot)
      write(nout,6200)'bmaxr(*)=',(bmaxr(i,ndrts),i=1,niot)
c
c
      if(spnorb)then
         bminr(niot+1,ndrts) = 0
         bmaxr(niot+1,ndrts) = neli + 1
      endif
c
c
      if(nact(ndrts).gt.0)then
         call iset(nact(ndrts),1111,smaskr(ndp1,ndrts),1)
         call inivp(modrt(ndp1),smaskr(ndp1,ndrts),nact(ndrts),
     &    'input the active orbital smaskr(*)',*460)
      endif
      write(nout,6030)'modrt:smaskr=',
     .             (modrt(i),smaskr(i,ndrts),i=1,niot)
c
c
      if(spnodd)then
         smaskr(niot+1,ndrts) = 0110
      endif
c
c
c***********************************************************************
c     mrsdci drt input:
c***********************************************************************
c
540   continue
c     exmax = 2
      if (neli.le.2) then
         exmax=neli
      else
         exmax =2
      endif
      write(nlist,*)
      call ini(exmax,'input the maximum excitation '
     & //'level from the reference csfs',*460)
      write(nlist,6100)'maximum excitation from ref. csfs: ',exmax
      if(exmax.lt.0)then
         write(nlist,*) '0 <= exmax'
         go to 540
      endif
c
c     # qxlim = .true. if reference step vectors are to be saved.
c
c     # qxlim must be true always
c     # hence exmax = neli  if neli <=2
c                           else  exmax= 2  (tm)
c
c     qxlim = exmax .lt. neli
      qxlim = .true.
c
550   continue
c
c     # adjust occmin(*), occmax(*) for excitations of level exmax
c     # in the internal space.
c
      do 554 i = 1,niot
         occmin(i,ndrts) = max(occmnr(i,ndrts)-exmax,0)
         occmax(i,ndrts) = min(occmxr(i,ndrts)+exmax,neli)
554   continue
c
      write(nlist,6100)'number of internal electrons: ',neli
      write(nlist,*)
      call inivp(modrt(1),occmin(1,ndrts),niot,
     & 'input the internal-orbital mrsdci occmin(*)',*540)
c
      call inivp(modrt(1),occmax(1,ndrts),niot,
     & 'input the internal-orbital mrsdci occmax(*)',*550)
c
      write(nout,6060)'mrsdci csf cumulative electron occupations:'
      write(nout,6200)'modrt(*)=',(modrt(i),i=1,niot)
      write(nout,6200)'occmin(*)=',(occmin(i,ndrts),i=1,niot)
      write(nout,6200)'occmax(*)=',(occmax(i,ndrts),i=1,niot)
c
c
      if(spnodd)then
         occmin(niot+1,ndrts) = occmin(niot,ndrts) + 1
         occmax(niot+1,ndrts) = occmax(niot,ndrts) + 1
      endif
c
c
      do 566 i = 1,niot
         if(occmin(i,ndrts).gt.neli)then
            write(nlist,6100)'occmin(i) <= neli, i',i
            go to 550
         endif
         if(occmin(i,ndrts).gt.occmax(i,ndrts))then
            write(nlist,6100)'occmin(i) <= occmax(i), i',i
            go to 550
         endif
566   continue
      if(occmax(niot,ndrts).lt.neli)then
         write(nlist,*) 'neli <= occmax(niot)'
         go to 550
      endif
c
570   continue
      call izero_wr(niot,bmin(1,ndrts),1)
      write(nlist,*)
      call inivp(modrt(1),bmin(1,ndrts),niot,
     & 'input the internal-orbital mrsdci bmin(*)',*550)
c
      call iset(niot,neli,bmax(1,ndrts),1)
      call inivp(modrt(1),bmax(1,ndrts),niot,
     & 'input the internal-orbital mrsdci bmax(*)',*570)
c
      write(nout,6060)'mrsdci b-value constraints:'
      write(nout,6200)'modrt(*)=',(modrt(i),i=1,niot)
      write(nout,6200)'bmin(*)=',(bmin(i,ndrts),i=1,niot)
      write(nout,6200)'bmax(*)=',(bmax(i,ndrts),i=1,niot)
c
c
      if(spnodd)then
         bmin(niot+1,ndrts) = 0
         bmax(niot+1,ndrts) = neli + 1
      endif
c
c
      write(nlist,*)
      call iset(niot,1111,smask(1,ndrts),1)
      call inivp(modrt(1),smask(1,ndrts),niot,
     & 'input the internal-orbital smask(*)',*570)
      write(nout,6030)'modrt:smask=',(modrt(i),smask(i,ndrts),i=1,niot)
      write(nlist,*)
c
c
      if(spnodd)then
         smask(niot+1,ndrts) = 0110
      endif
c
c
c     # write summary table
      write(nlist,6060)'internal orbital summary:'
      write(nlist,6200)'block(*)=',(syml(i),i=1,niot)
      write(nlist,6201)'slabel(*)=',(slabel(syml(i)),i=1,niot)
      write(nlist,6200)'rmo(*)=',((modrt(i)-nmskp(syml(i))),i=1,niot)
      write(nlist,6200)'modrt(*)=',(modrt(i),i=1,niot)
      write(nlist,*)
      write(nlist,6060)'reference csf info:'
      write(nlist,6200)'occmnr(*)=',(occmnr(i,ndrts),i=1,niot)
      write(nlist,6200)'occmxr(*)=',(occmxr(i,ndrts),i=1,niot)
      write(nlist,*)
      write(nlist,6200)'bminr(*)=',(bminr(i,ndrts),i=1,niot)
      write(nlist,6200)'bmaxr(*)=',(bmaxr(i,ndrts),i=1,niot)
      write(nlist,*)
      write(nlist,*)
      write(nlist,6060)'mrsdci csf info:'
      write(nlist,6200)'occmin(*)=',(occmin(i,ndrts),i=1,niot)
      write(nlist,6200)'occmax(*)=',(occmax(i,ndrts),i=1,niot)
      write(nlist,*)
      write(nlist,6200)'bmin(*)=',(bmin(i,ndrts),i=1,niot)
      write(nlist,6200)'bmax(*)=',(bmax(i,ndrts),i=1,niot)
      write(nlist,*)
c
600   continue
c
c  note:  vrem() call should be closer to condrt() call and remove(*)
c         should be treated as a temporary array which is deallocated
c         after condrt().  make it so, number one, the next time the
c         input to this program changes.
c
c     # store distinct rows to be removed during drt construction...
c     # core=>1:remove(*),2:
c
c
c***********************************************************************
c     csf input:
c***********************************************************************
      return
c
6020  format(1x,'spin multiplicity, smult',t38,': ',i3,4x,a)
6030  format(1x,a/(8(i4,':',i4.4)))
6040  format(/' workspace allocation parameters: lencor=',i10,
     & ' mem1=',i10,' ifirst=',i10)
6060  format(1x,a)
6100  format(1x,a,t38,': ',(1x,9i4))
6105  format(1x,a,t38,': ',a4,a,i5)
6200  format(1x,a,(t17,15i4))
6201  format(1x,a,(t17,15a4))
6210  format(1x,a,/1x,8(:'(',i2,', ',a4,') '))
6220  format(1x,a,i2,a)
6300  format(/' core(*) allocation error at ',a,', lencor=',i8,
     & ' icd(*)=',/(1x,10i8))
      end

      subroutine vrem( nremx, remove, nrem, * )
c
c  store distinct rows to be excluded during drt construction.
c
       implicit none 
C====>Begin Module VREM                   File cidrtms2.f               
C---->Makedcls Options: All variables                                   
C
C     Argument variables
C
      INTEGER             NREM,        NREMX,       REMOVE(3,NREMX)
C
C     Local variables
C
      INTEGER             AROW,        BROW,        ITEMP(3),    LEVEL
C
      INCLUDE 'cfiles.inc'
C====>End Module   VREM                   File cidrtms2.f               

c
      write(nlist,6010)
      write(nout,6011)
6010  format(/' a priori removal of distinct rows:'/)
6011  format(' input the level, a, and b values for the vertices '/
     & ' to be removed (-1/ to end).'/)
c
      nrem = 0
100   continue
c     # sun fortran is braindamaged, so don't use equivalences. -rls
      itemp(1) = -1
      itemp(2) = 0
      itemp(3) = 0
      call iniv(itemp,3,'input level, a, and b (-1/ to end)',*1000)
      level = itemp(1)
      arow = itemp(2)
      brow = itemp(3)
      if(level.lt.0)then
         if(nrem.eq.0) then
            write (nlist,*) 'no vertices marked for removal'
         else
            write(nlist,*) 'number of marked vertices : ',nrem
         endif
         return
      elseif(level.eq.0)then
         write(nlist,*) 'level=0 vertices cannot be removed.'
         go to 100
      else
         nrem = nrem+1
         if(nrem.gt.nremx)then
            write(nlist,*) 'too many vertices to be removed. nremx=',
     &       nremx
            go to 1000
         else
            remove(1,nrem) = level
            remove(2,nrem) = arow
            remove(3,nrem) = brow
            write(nlist,6020)nrem,level,arow,brow
            go to 100
         endif
      endif
1000  return 1
6020  format(1x,i4,': level=',i4,' a=',i4,' b=',i4,
     &  ' marked for removal.')
      end

      subroutine prcdrt(
     & levprt,  niot,    nrow,   nrowmx,
     & nsym,    nj,      njskp,  modrt,
     & syml,    slabel,  a,      b,
     & l,       y,       xbar,   chrprn  )
c
c  print drt information from levels levprt(1) to levprt(2).
c
c  24-nov-90 chrprn added. -rls
c
       implicit none 
C====>Begin Module PRCDRT                 File cidrtms2.f               
C---->Makedcls Options: All variables                                   
C
C     Argument variables
C
      CHARACTER           CHRPRN
      CHARACTER*4         SLABEL(0:8)
C
      INTEGER             A(NROW),     B(NROW),     L(0:3,NROWMX)
      INTEGER             LEVPRT(2),   MODRT(0:NIOT),            NIOT
      INTEGER             NJ(0:NIOT),  NJSKP(0:NIOT),            NROW
      INTEGER             NROWMX,      NSYM,        SYML(0:NIOT)
      INTEGER             XBAR(NROW),  Y(0:3,NROW)
C
C     Local variables
C
      INTEGER             I,           ILEV,        IR1,         IR2
      INTEGER             IROW,        L1,          L2
C
      INCLUDE 'cfiles.inc'
C====>End Module   PRCDRT                 File cidrtms2.f               
c
c     # restrict array references to a meaningful range.
c
      l1 = min(levprt(1),levprt(2))
      l2 = max(levprt(1),levprt(2))
      l1 = min(max(l1,0),niot)
      l2 = min(max(l2,0),niot)
c
c     # chrprn is either ' ' or '1', depending on whether the program
c     # has been compiled for batch mode.  -rls
c
      write(nlist,6060) chrprn
c
      write(nlist,6010)l1,l2
c
      do 300 ilev = l2, l1, -1
          ir1 = njskp(ilev)+1
          ir2 = njskp(ilev)+nj(ilev)
          do 200 irow = ir1,ir2
              write(nlist,6030)irow,ilev,a(irow),b(irow),
     &          modrt(ilev),slabel(syml(ilev)),(l(i,irow),i=0,3),
     &          (y(i,irow),i=0,2),xbar(irow)
200       continue
          write(nlist,6050)
300   continue
      write(nlist,6060) chrprn
c
      return
6010  format(' level',i3,' through level',i3,' of the drt:'//
     &  1x,'row',' lev',' a',' b','  mo',' syml',
     &  '  l0','  l1','  l2','  l3',
     &  '     y0 ','     y1 ','     y2 ','   xbar '/)
6030  format(1x,i3,i4,i2,i2,i4,a5,4i4,4i8)
6050  format(1x,36('.')/)
6060  format(a)
      end

      subroutine pr3drt(
     & levprt,  niot,    nrow,   nrowmx,
     & nsym,    nj,      njskp,  modrt,
     & syml,    slabel,  a,      b,
     & l,       y,       xbar,   chrprn )
c
c  call the prcdrt routine for the three sets of drt chaining arrays.
c
c  24-nov-90 chrprn added to remove some mdc blocks. -rls
c
       implicit none 
C====>Begin Module PR3DRT                 File cidrtms2.f               
C---->Makedcls Options: All variables                                   
C
C     Argument variables
C
      CHARACTER           CHRPRN
      CHARACTER*4         SLABEL(0:8)
C
      INTEGER             A(NROW),     B(NROW),     L(0:3,NROWMX,3)
      INTEGER             LEVPRT(2),   MODRT(0:NIOT),            NIOT
      INTEGER             NJ(0:NIOT),  NJSKP(0:NIOT),            NROW
      INTEGER             NROWMX,      NSYM,        SYML(0:NIOT)
      INTEGER             XBAR(NROWMX,3),           Y(0:3,NROWMX,3)
C
C     Local variables
C
      CHARACTER*2         WXYZ(3)
C
      INTEGER             I
C
      INCLUDE 'cfiles.inc'
C====>End Module   PR3DRT                 File cidrtms2.f               

      data wxyz/' z',' y','xw'/
c
      do 10 i = 1, 3
         write(nlist,6010) wxyz(i)
         call prcdrt(
     &    levprt,    niot,      nrow,      nrowmx,
     &    nsym,      nj,        njskp,     modrt,
     &    syml,      slabel,    a,         b,
     &    l(0,1,i),  y(0,1,i),  xbar(1,i), chrprn )
10    continue
      return
6010  format(/t25,'the ',a,' drt chaining indices:')
      end

      subroutine prtcsf(
     & niot,   modrt,  syml,    nsym,
     & nmpsy,  nmskp,  slabel,  nref,
     & refi  )
c
c  print the reference csfs.
c
c  15-apr-89  nmskp(*), format changes, and minor cleanup. -rls
c  written by eric stahlberg.
c
       implicit none 
C====>Begin Module PRTCSF                 File cidrtms2.f               
C---->Makedcls Options: All variables                                   
C
C     Argument variables
C
      CHARACTER*4         SLABEL(0:8)
C
      INTEGER             MODRT(0:NIOT),            NIOT
      INTEGER             NMPSY(NSYM), NMSKP(NSYM), NREF,        NSYM
      INTEGER             REFI(NREF,NIOT),          SYML(0:NIOT)
C
C     Local variables
C
      CHARACTER*80        CFMT
      CHARACTER*4         LBLTP(0:3)
      CHARACTER*2         STPLEN
C
      INTEGER             I,           J,           K
C
      INCLUDE 'cfiles.inc'
C====>End Module   PRTCSF                 File cidrtms2.f               

      data lbltp /'    ','  + ','   -','  +-'/
c
      if (niot.gt.24) then
c        # fix this later...
         write (nlist,*) 'error: prtcsf: too many orbitals to print:'
     &    //' will not fit.'
         return
      endif
c
c     build the output format...
c                      this only allows 99 references: fix this later.
      write (stplen,'(i2)') niot
      cfmt = '(1x,i2,1x,'//stplen//'i1,(t29,12a4))'
c
c     print the header info...
c
*@ifdef batchmode
*      write(nlist,'(a)')'1'
*@endif
      write(nlist,6000)
      write(nlist,6030) (slabel(syml(i)),i=1,niot)
      write(nlist,6020) (modrt(i)-nmskp(syml(i)),i=1,niot)
      write(nlist,6010) (modrt(i),i=1,niot)
      write(nlist,6040)
c
c     begin processing step vectors
c
      do 200 i = 1,nref
         write(nlist,cfmt)i,(refi(i,k),k=1,niot),
     &                      (lbltp(refi(i,j)),j=1,niot)
 200  continue
      return
 6000 format(/t25,'***** reference csfs *****'/)
 6010 format(1x,'modrt(*)  =',(t29,12i4))
 6020 format(1x,'rmo(*)    =',(t29,12i4))
 6030 format(1x,'slabel(*) =',(t29,12a4))
 6040 format(/1x,'ref.',t10,'step vector',
     & t29,'character representation'/)
      end

      subroutine wrthd(
     & ndrt,  title,  nmot,  niot,
     & nfct,  nfvt,   nrow,  nsym,
     & ssym,  nrowmx, xbar,  nvalw,
     & ncsft, buf,    lenbuf,
     & spnorb,spnodd, lxyzir,hmult)
       implicit none 
C====>Begin Module WRTHD                  File cidrtms2.f               
C---->Makedcls Options: All variables                                   
C
C     Parameter variables
C
      INTEGER             NMXDRT
      PARAMETER           (NMXDRT = 8)
      INTEGER             NHDINT
      PARAMETER           (NHDINT = 16)
      INTEGER             VRSION
      PARAMETER           (VRSION = 6)
C
C     Argument variables
C
      CHARACTER*(*)       TITLE
C
      INTEGER             BUF(LENBUF), HMULT,       LENBUF
      INTEGER             LXYZIR(3),   NCSFT,       NDRT,        NFCT
      INTEGER             NFVT,        NIOT,        NMOT,        NROW
      INTEGER             NROWMX,      NSYM,        NVALW(4),    SSYM
      INTEGER             XBAR(NROWMX,3)
C
      LOGICAL             SPNODD,      SPNORB
C
C====>End Module   WRTHD                  File cidrtms2.f               
c
c  write header info to the drt file.
c
c
c     # cidrtfl version history:
c     # 6: deleted csym(*) 
c     # 5: compressed index vectors
c     # 3: 24-nov-90 y(*,*,1:3),l(*,*,1:3), xbar(*,1:3) change,
c     #              ncsfv(*) removed, ncsft added. -rls
c     # 2: 03-oct-88 slabel(*) added. -eas
c     # 1: 10-jan-88 3-index drt. -psz/hl
c     # 0: 20-jul-84 drt arrays buffered. -rls
c
c     # title card...
c
      rewind ndrt
      write(ndrt,'(a)')title
      write(ndrt,'(2L4,4I4)')spnorb, spnodd, lxyzir,hmult
c
c     # write out the number of header integers...
c
      write(6,*)'CI version:',vrsion
      buf(1) = vrsion
      buf(2) = lenbuf
      buf(3) = nhdint
      call wrtdbl(lenbuf,buf,3,' ','cidrt')
c
c     # put the integers into the buffer and write it out...
c
      buf( 1) = nmot
      buf( 2) = niot
      buf( 3) = nfct
      buf( 4) = nfvt
      buf( 5) = nrow
      buf( 6) = nsym
      buf( 7) = ssym
      buf( 8) = xbar(1,1)
      buf( 9) = xbar(2,2)
      buf(10) = xbar(3,3)
      buf(11) = xbar(4,3)
      buf(12) = nvalw(1)
      buf(13) = nvalw(2)
      buf(14) = nvalw(3)
      buf(15) = nvalw(4)
      buf(16) = ncsft
c
      call wrtdbl( lenbuf, buf, nhdint, ' ', 'info' )
c
      return
      end

      subroutine wrtdrt(
     & nmot,   niot,   nsym,   nrow,
     & nrowmx, nwalk,  lenbuf, buf,
     & map,    nmpsy,  slabel, nexo,
     & nviwsm, ncsfsm, mu,     modrt,
     & syml,   nj,     njskp,  a,
     & b,      l,      y,      xbar,
     & ref,    csym,   nvalw,  limvec )
c
c  write the drt arrays to the drt file.
c  title and dimension information have already been written.
c  the file is assumed to be positioned correctly for the next
c  records.  all vectors are buffered in records of length lenbuf.
c
c  see wrthd() for the ci drt file version number history.
c
c  version:
c  25-nov-90 y(*) and xbar(*) pieces written separately.
c            nviwsm(*) and ncsfsm(*) written out. -rls
c
c  written by ron shepard.
c
       implicit none 
C====>Begin Module WRTDRT                 File cidrtms2.f               
C---->Makedcls Options: All variables                                   
C
      real*8    DBLE
C
C     Argument variables
C
      CHARACTER*4         SLABEL(0:8)
C
      INTEGER             A(NROW),     B(NROW),     BUF(LENBUF), CSYM(*)
      INTEGER             L(0:3,NROWMX,3),          LENBUF
      INTEGER             LIMVEC(NWALK),            MAP(NMOT)
      INTEGER             MODRT(0:NIOT),            MU(NIOT)
      INTEGER             NCSFSM(4,NSYM),           NEXO(NSYM),  NIOT
      INTEGER             NJ(0:NIOT),  NJSKP(0:NIOT),            NMOT
      INTEGER             NMPSY(NSYM), NROW,        NROWMX,      NSYM
      INTEGER             NVALW(4),    NVIWSM(4,NSYM),           NWALK
      INTEGER             REF(*),      SYML(0:NIOT)
      INTEGER             XBAR(NROWMX,3),           Y(0:3,NROWMX,3)
C
C     Local variables
C
      INTEGER             CPOS,        CURPOS,      CURVAL,      I
      INTEGER             IFAC,        LIMVECINFO(3),            NLEVEL
      INTEGER             NVWXY, maxval 
      INTEGER*4           BUF32(8)
C
*@ifdef g95
*      include 'cfiles.inc'
*@endif
C====>End Module   WRTDRT                 File cidrtms2.f               
c
      nlevel = niot + 1
      nvwxy  = nvalw(2) + nvalw(3) + nvalw(4)
c
c     # slabel(*)...
      do 10 i = 1, nsym
         read(slabel(i),'(a4)') buf32(i)
10    continue
      call wrtdbl( lenbuf, buf32, nsym, '(8a4)', 'slabel' )
c
c     # map(*)...
      call wrtdbl( lenbuf, map, nmot, ' ', 'map' )
c
c     # mu(*)...
      call wrtdbl( lenbuf, mu, niot, ' ', 'mu' )
c
c     # nmpsy(*)...
      call wrtdbl( lenbuf, nmpsy, nsym, ' ', 'nmpsy' )
c
c     # nexo(*)...
      call wrtdbl( lenbuf, nexo, nsym, ' ', 'nexo' )
c
c     # nviwsm(*)...
      call wrtdbl( lenbuf, nviwsm, 4*nsym, ' ', 'nviwsm' )
c
c     # ncsfsm(*)...
      call wrtdbl( lenbuf, ncsfsm, 4*nsym, ' ', 'ncsfsm' )
c
c     # modrt(1:niot)...
      call wrtdbl( lenbuf, modrt(1), niot, ' ', 'modrt' )
c
c     # syml(1:niot)...
      call wrtdbl( lenbuf, syml(1), niot, ' ', 'syml' )
c
c     # nj(*)...
      call wrtdbl( lenbuf, nj, nlevel, ' ', 'nj' )
c
c     # njskp(*)...
      call wrtdbl( lenbuf, njskp, nlevel, ' ', 'njskp' )
c
c     # a(*)...
      call wrtdbl( lenbuf, a, nrow, ' ', 'a' )
c
c     # b(*)...
      call wrtdbl( lenbuf, b, nrow, ' ', 'b' )
c
c     # l(0:3,1:nrow,1:3)...
      do 20 i = 1, 3
         call wrtdbl( lenbuf, l(0,1,i), 4*nrow, ' ', 'l' )
20    continue
c
c     # y(1:3,1:nrow,1:3)...
      do 30 i = 1, 3
         call wrtdbl( lenbuf, y(0,1,i), 4*nrow, ' ', 'y' )
30    continue
c
c     # xbar(1:nrow,1:3)...
      do 40 i = 1, 3
         call wrtdbl( lenbuf, xbar(1,i), nrow, ' ', 'xbar' )
40    continue

c
c     # 0/1 limvec(*) for all walks...
c     compress limvec
c     #  use length encoding for limvec right now
c     #  negative number: n consecutive invalid walks
c     #  positive number: n consecutive valid walks
c     #  0 : end of limvec
      call cmprlimvec(limvec,nwalk,cpos,ifac,maxval)
      limvecinfo(1)=ifac
      limvecinfo(2)=cpos
      limvecinfo(3)=maxval

      write(6,970) nwalk
970   format('initial index vector length:',i10)
      write(6,971) cpos,100d0- dble(cpos)*100.d0/dble(nwalk)
971   format('compressed index vector length:',i10,
     .       'reduction: ',f6.2,'%')
      call wrtdbl( lenbuf, limvecinfo,3,' ','limvecinfo')
      call wrtdbl( lenbuf, limvec, cpos, 'noblank', 'limvec' )
c
c     # csym(*) for valid wxy walks...
c     call wrtdbl( lenbuf, csym(nvalw(1)+1), nvwxy, '(80i1)', 'csym' )
c
c     # 0/1 ref(*) for z-walks...
c     compress limvec
c     #  use length encoding for limvec right now
c     #  negative number: n consecutive invalid walks
c     #  positive number: n consecutive valid walks
      call cmprlimvec(ref,xbar(1,1),cpos,ifac,maxval)
      limvecinfo(1)=ifac
      limvecinfo(2)=cpos
      limvecinfo(3)=maxval
      write(6,972) xbar(1,1)
972   format('initial ref vector length:',i10)
      write(6,973) cpos,100d0- dble(cpos)*100.d0/dble(xbar(1,1))
973   format('compressed ref vector length:',i10,
     .       'reduction: ',f6.2,'%')

      call wrtdbl( lenbuf, limvecinfo, 3, ' ', 'refinfo' )
      call wrtdbl( lenbuf, ref, cpos, 'noblank', 'ref' )
      return
      end

      subroutine remarc( a, b, l, nj, njskp, niot, nrow, * )
c
c  remove arcs manually from the 1-index drt.
c
c  17-oct-91 ntot increment moved. -rls/g.gawboy
c
       implicit none 
C====>Begin Module REMARC                 File cidrtms2.f               
C---->Makedcls Options: All variables                                   
C
C     Argument variables
C
      INTEGER             A(NROW),     B(NROW),     L(0:3,NROW), NIOT
      INTEGER             NJ(0:NIOT),  NJSKP(0:NIOT),            NROW
C
C     Local variables
C
      INTEGER             AROW,        BROW,        ILEV,        IR1
      INTEGER             IR2,         ISTEP,       ITEMP(4),    LT
      INTEGER             NREM,        NTOT,        ROW
C
      INCLUDE 'cfiles.inc'
C====>End Module   REMARC                 File cidrtms2.f               
c
      write(nlist,6010)
      write(nout,6011)
c
      row = 0
c
      nrem = 0
      ntot = 0
100   continue
c
      itemp(1) = -1
      itemp(2) = 0
      itemp(3) = 0
      itemp(4) = 0
c
      call iniv(itemp, 4,
     &  'input the level, a, b, and step (-1/ to end)', *9000 )
c
      ilev  = itemp(1)
      arow  = itemp(2)
      brow  = itemp(3)
      istep = itemp(4)
c
      if ( ilev .ge. 0 ) then
c
         ntot  = ntot + 1
c
         if ( (istep .lt. 0) .or. (istep .gt. 3) ) then
c           # invalid step.
            write(nlist,6100)'invalid step:', ilev, arow, brow, istep
            goto 100
         elseif ( ilev .le. niot ) then
c
c           # check to make sure the row exists.
c           # (ilev is the bottom of the arc)
c
            ir1 = njskp(ilev)+1
            ir2 = njskp(ilev)+nj(ilev)
            do 400 row = ir1,ir2
               if ( (arow .eq. a(row)) .and. (brow .eq. b(row)) ) then
                  lt = l(istep,row)
                  if ( lt .eq. 0 ) then
                     write(nlist,6100)
     &                'arc not found', ilev, arow, brow, istep
                  else
                     l(istep,row) = 0
                     nrem         = nrem + 1
                     write(nlist,6100)
     &                'arc removed', ilev, arow, brow, istep, nrem
                  endif
                  go to 100
               endif
400         continue
         endif
         write(nlist,6100) 'vertex not found', ilev, arow, brow
         go to 100
      endif
c
      write(nlist,6020) nrem, ntot
c
      return
c
9000  return 1
c
6010  format(/' manual arc removal step:'/)
6011  format(/' input the level, a, b, and step values '
     & /' for the arcs to be removed (-1/ to end).'/)
6100  format(1x,a,': level=',i3,', a=',i3,', b=',i3:,
     & ', step=',i2:,', nrem=',i3)
6020  format(' remarc:',i4,' arcs removed out of',i4,' specified.')
      end
c*deck chain
      subroutine chain(
     & niot,   nj,     njskp,  l,
     & y,      xbar,   nrow,   nrowmx,
     & nwalk,  drttyp, spnorb, multp,
     & nmul,   spnodd )
       implicit none 
C====>Begin Module CHAIN                  File cidrtms2.f               
C---->Makedcls Options: All variables                                   
C
C     Argument variables
C
      INTEGER             DRTTYP,      L(0:3,NROWMX,3)
      INTEGER             MULTP(NMUL), NIOT,        NJ(0:NIOT)
      INTEGER             NJSKP(0:NIOT),            NMUL,        NROW
      INTEGER             NROWMX,      NWALK,       XBAR(NROWMX,3)
      INTEGER             Y(0:3,NROWMX,3)
C
      LOGICAL             SPNODD,      SPNORB
C
C     Local variables
C
      INTEGER             I,           ILEV,        IMUL,        IR1
      INTEGER             IR2,         IROW,        ISTEP,       LT
      INTEGER             NLEV,        VER,         VERS(4)
C
      INCLUDE 'cfiles.inc'
C====>End Module   CHAIN                  File cidrtms2.f               
c
c  calculate the chaining information for the drt.
c  reverse lexical ordering is used.
c
c
c     # drttyp = 1 for 1-index drt,
c     #        = 3 for 3-index drt.
c
      do 10 i = 1, 4
         vers(i) = min(i,drttyp)
10    continue
c
      do 1000 ver = 1, vers(4)
c
c        # initialize the top row.
c
c
         if(spnorb)then
            do 1001 imul = 1, nj(niot)
               irow = njskp(niot)+imul
               xbar(irow,ver) = multp(imul)
               y(0,irow,ver)  = 0
               y(1,irow,ver)  = 0
               y(2,irow,ver)  = 0
               y(3,irow,ver)  = 0
1001        continue
c
         else
            irow = njskp(niot)+1
            xbar(irow,ver) = 1
            y(0,irow,ver)  = 0
            y(1,irow,ver)  = 0
            y(2,irow,ver)  = 0
            y(3,irow,ver)  = 0
         endif
c
c
c        # loop over all the lower rows.
c
         do 600 nlev = niot, 1, -1
            ilev = nlev-1
c
            ir1 = njskp(ilev)+1
            ir2 = njskp(ilev)+nj(ilev)
            do 400 irow = ir1, ir2
c
c              # y(3,*,*) = 0  for all rows; included only for indexing.
c
               y(3,irow,ver) = 0
c
c              # loop over the remaining step numbers.
c
               do 200 istep = 2, 0, -1
                  lt = l(istep+1,irow,ver)
                  if ( lt .ne. 0 ) then
                     y(istep,irow,ver) = y(istep+1,irow,ver)
     &                + xbar(lt,ver)
                  else
                     y(istep,irow,ver) = y(istep+1,irow,ver)
                  endif
200            continue
c
               lt = l(0,irow,ver)
               if(lt.ne.0)then
                  xbar(irow,ver) = y(0,irow,ver)+xbar(lt,ver)
               else
                  xbar(irow,ver) = y(0,irow,ver)
               endif
c
400         continue
600      continue
1000  continue
c
c     # offset the arcs passing through w,x, and y so that each path
c     # is unique.
c
      nwalk = xbar(1,1)
      do 800 ver = 2, 4
         do 700 istep = 0, 3
            y(istep,ver,vers(ver)) = y(istep,ver,vers(ver))+nwalk
700      continue
         nwalk = nwalk+xbar(ver,vers(ver))
800   continue
c
      write(nlist,6010)(xbar(i,vers(i)),i=1,4),nwalk
6010  format(/' xbarz=',i8
     &  /' xbary=',i8
     &  /' xbarx=',i8
     &  /' xbarw=',i8
     &  /8x,8('-')
     &  /' nwalk=',i8)
c
      return
      end
      subroutine condrt(
     & a,      b,      l,      nj,
     & njskp,  niot,   neli,   smult,
     & nrem,   remove, occmin, occmax,
     & bmin,   bmax,   ndot,   qgis,
     & smask,  nrow,   nrowmx, spnorb,
     & multp,  nmul,   spnodd )
       implicit none 
C====>Begin Module CONDRT                 File cidrtms2.f               
C---->Makedcls Options: All variables                                   
C
C     External functions
C
      EXTERNAL            VFAIL
C
      LOGICAL             VFAIL
C
C     Parameter variables
C
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
C
C     Argument variables
C
      INTEGER             A(*),        B(*),        BMAX(NIOT)
      INTEGER             BMIN(NIOT),  L(0:3,*),    MULTP(NMUL), NDOT
      INTEGER             NELI,        NIOT,        NJ(0:NIOT)
      INTEGER             NJSKP(0:NIOT),            NMUL,        NREM
      INTEGER             NROW,        NROWMX,      OCCMAX(NIOT)
      INTEGER             OCCMIN(NIOT),             REMOVE(3,*)
      INTEGER             SMASK(NIOT), SMULT
C
      LOGICAL             QGIS,        SPNODD,      SPNORB
C
C     Local variables
C
      INTEGER             AC,          ANEXT,       BC,          BNEXT
      INTEGER             CLEV,        DELA(0:3),   DELB(0:3),   INR
      INTEGER             INR1,        INR2,        IR1,         IR2
      INTEGER             IROW,        ISTEP,       NLEV
C
      INCLUDE 'cfiles.inc'
C====>End Module   CONDRT                 File cidrtms2.f               
c
c  subroutine condrt constructs the chaining indices for the distinct
c  row table subject to any imposed restrictions.
c  because only general occupation restrictions are imposed at this
c  point, the 1-index drt is constructed.
c
      data dela/0,0,1,1/,  delb/0,1,-1,0/
c
c     # initialize the w, x, y, and z vertices at the zeroth level.
c
c     # z vertex...
      a(1)   = 0
      b(1)   = 0
      l(0,1) = 0
      l(1,1) = 0
      l(2,1) = 0
      l(3,1) = 0
c     # y vertex...
      a(2)   = 0
      b(2)   = 1
      l(0,2) = 0
      l(1,2) = 0
      l(2,2) = 0
      l(3,2) = 0
c     # x vertex...
      a(3)   = 0
      b(3)   = 2
      l(0,3) = 0
      l(1,3) = 0
      l(2,3) = 0
      l(3,3) = 0
c     # w vertex...
      a(4)   = 1
      b(4)   = 0
      l(0,4) = 0
      l(1,4) = 0
      l(2,4) = 0
      l(3,4) = 0
c
      nrow     = 4
      nj(0)    = 4
      njskp(0) = 0
c
c     # loop over the remaining levels.
c
      do 600 nlev = 1,niot
         clev = nlev-1
         if(nj(clev).eq.0)
     &    call bummer('condrt: nj=0 for clev=',clev,faterr)
         njskp(nlev) = njskp(clev) + nj(clev)
         nj(nlev) = 0
c
c        # loop over the rows of the current level.
c
         ir1 = njskp(clev)+1
         ir2 = njskp(clev)+nj(clev)
         do 500 irow = ir1, ir2
            ac = a(irow)
            bc = b(irow)
c
c           # loop over the four steps from the current row, irow.
c
            do 400 istep = 3, 0, -1
c              # smask(*) is used to manually eliminate arcs.
               if(mod(smask(nlev)/10**(istep),10).eq.0)go to 400
c
               anext = ac+dela(istep)
               bnext = bc+delb(istep)
c      write(*,*) 'nlev,nj(nlev),irow,istep=',nlev,nj(nlev),irow,istep
               if(vfail(niot,neli,smult,anext,bnext,
     &              nlev,occmin,occmax,bmin,bmax,
     &              nrem,remove,ndot,qgis,spnorb,multp,
     &              nmul,spnodd))go to 400
c      write(*,*) ' ... successful'
c
c              # check to see if this new row already exists in the drt.
c
               inr1 = njskp(nlev)+1
               inr2 = njskp(nlev)+nj(nlev)
               do 100 inr = inr1,inr2
                  if(anext.eq.a(inr) .and. bnext.eq.b(inr))then
c                    # this row has already been added.
c                    # adjust the chaining indices.
                     l(istep,irow) = inr
                     go to 400
                  endif
100            continue
c
c              # this is a new row.  append this new row to the drt.
c
               nrow = nrow+1
               if(nrow.gt.nrowmx)then
                  write(nlist,6020)nrow,nrowmx
                  call bummer('condrt: nlev=',nlev,faterr)
               endif
               l(0,nrow) = 0
               l(1,nrow) = 0
               l(2,nrow) = 0
               l(3,nrow) = 0
               nj(nlev) = nj(nlev)+1
               a(nrow) = anext
               b(nrow) = bnext
               l(istep,irow) = nrow
400         continue
500      continue
600   continue
c
      write(nlist,6030)nrow
      return
c
6020  format(/' condrt: error, nrow,nrowmx=',2i5)
6030  format(/' number of rows in the drt :',i4)
      end

      subroutine gisarc(
     & a,      b,      l,      nj,
     & njskp,  ndot,   nrow,   nrowmx )
c
c  this routine removes noninteracting walks from the csf expansion
c  by deleting particular arcs.  the modified arcs contribute only
c  to zero segment values when combined with the doubly occupied
c  reference csf arcs.
c
c  in most cases, this does not remove all noninteracting csfs but
c  a reduction of the number of internal walks in the drt is
c  achieved, which simplifies any further restrictions to be
c  made later.
c
c  these arc deletions are consistent with the generalization of the
c  hartree-fock interacting space to mrsdci wave functions.
c
c  only the lowest 'ndot' levels of the drt are modified.
c
c  written by ron shepard.
c
       implicit none 
C====>Begin Module GISARC                 File cidrtms2.f               
C---->Makedcls Options: All variables                                   
C
C     Argument variables
C
      INTEGER             A(NROW),     B(NROW),     L(0:3,NROWMX)
      INTEGER             NDOT,        NJ(0:NROW),  NJSKP(0:NROW)
      INTEGER             NROW,        NROWMX
C
C     Local variables
C
      INTEGER             AROW,        CLEV,        IR1,         IR2
      INTEGER             NLEV,        NREM,        ROW,         TYPE
C
      INCLUDE 'cfiles.inc'
C====>End Module   GISARC                 File cidrtms2.f               
      nrem = 0
      do 300 nlev = 1,ndot
         clev = nlev-1
c
c        #          segment         bstep  kstep  delb  bket
c        # type=1  r-top-r-top,       0      3      -2    0
c        # type=2  r-bottom-r-bottom, 3      0       2    2
c
         do 200 type = 1,2
            arow = nlev-type
            if(arow.lt.0)go to 200
c
            ir1 = njskp(clev)+1
            ir2 = njskp(clev)+nj(clev)
            do 100 row = ir1,ir2
               if ( (a(row) .eq. arow) .and. (b(row) .eq. 2) .and.
     &          (l(0,row) .ne. 0) )then
                  nrem = nrem+1
                  l(0,row) = 0
               endif
100         continue
200      continue
300   continue
c
      write(nlist,6010)nrem
6010  format(1x,i5,' arcs removed due to generalized interacting',
     & ' space restrictions.')
      return
      end
c*deck symwlk
      subroutine symwlk(
     & niot,   nsym,   nrow,   nrowmx,
     & nwalk,  l,      y,      row,
     & step,   csym,   wsym,   arcsym,
     & ytot,   xbar,   slabel, ssym,
     & mult,   limvec, nexw,   ncsft,
     & nviwsm, ncsfsm, spnorb, b,
     & spnir,  multmx, spnodd  )
       implicit none 
C====>Begin Module SYMWLK                 File cidrtms2.f               
C---->Makedcls Options: All variables                                   
C
C     Parameter variables
C
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
      INTEGER             TOSKP
      PARAMETER           (TOSKP = 0)
      INTEGER             TO01
      PARAMETER           (TO01 = 1)
C
C     Argument variables
C
      CHARACTER*4         SLABEL(0:8)
C
      INTEGER             ARCSYM(0:3,0:NIOT),       B(*),        CSYM(*)
      INTEGER             L(0:3,NROWMX,3),          LIMVEC(*)
      INTEGER             MULT(8,8),   MULTMX,      NCSFSM(4,NSYM)
      INTEGER             NCSFT,       NEXW(8,4),   NIOT,        NROW
      INTEGER             NROWMX,      NSYM,        NVIWSM(4,NSYM)
      INTEGER             NWALK,       ROW(0:NIOT), SPNIR(MULTMX,MULTMX)
      INTEGER             SSYM,        STEP(0:NIOT)
      INTEGER             WSYM(0:NIOT),             XBAR(NROWMX,3)
      INTEGER             Y(0:3,NROWMX,3),          YTOT(0:NIOT)
C
      LOGICAL             SPNODD,      SPNORB
C
C     Local variables
C
      CHARACTER           WXYZ(4)
C
      INTEGER             CLEV,        I,           IMUL,        ISSYM
      INTEGER             ISTEP,       ISYM,        MUL,         NLEV
      INTEGER             NUMV1,       NVALWT,      ROWC,        ROWN
      INTEGER             VER,         VER1,        VERS(4)
C
      INCLUDE 'cfiles.inc'
C====>End Module   SYMWLK                 File cidrtms2.f               
c
c  this routine calculates the symmetry of all the valid internal
c  walks and computes csf counts.
c
c  output:
c  csym(*) = mult( internal_walk_symmetry, ssym ) for
c            the valid z,y,x,w-walks.
c  ncsft = total number of csfs for this orbital basis and drt.
c  nviwsm(1:4,1:nsym) = number of valid internal walks of each symmetry
c                       through each vertex.
c  ncsfsm(1:4,1:nsym) = number of csfs passing through each vertex
c                       indexed by the internal symmetry.
c
c  07-jun-89  skip-vector based walk generation. rls
c
c
      data vers/1,2,3,3/
      data wxyz/'z','y','x','w'/
c
      write(nlist,'(/1x,a)')'beginning the final csym(*) computation...'
c
c     # change limvec(*) to skip-vector form.
c
      numv1 = 0
      call skpx01( nwalk, limvec, toskp, numv1 )
      nvalwt = 0
c
      call izero_wr( 4*nsym, nviwsm, 1 )
c
c     # loop over z, y, x, and w vertices.
c
      do 200 ver = 1, 4
         ver1 = vers(ver)
         clev = 0
         row(0) = ver
         ytot(0) = 0
         wsym(0) = 1
c
         do 50 i = 0, niot
            step(i) = 4
50       continue
c
100      continue
         nlev = clev+1
         istep = step(clev)-1
         if(istep.lt.0)then
c           # decrement the current level.
            step(clev) = 4
            if(clev.eq.0)go to 180
            clev = clev-1
            goto 100
         endif
         step(clev) = istep
         rowc = row(clev)
         rown = l(istep,rowc,ver1)
         if(rown.eq.0)go to 100
         row(nlev) = rown
         ytot(nlev) = ytot(clev)+y(istep,rowc,ver1)
         if( limvec( ytot(nlev)+1 ) .ge. xbar(rown,ver1) )goto 100
         wsym(nlev) = mult( arcsym(istep,clev), wsym(clev) )
         if(nlev.lt.niot)then
c           # increment to the next level.
            clev = nlev
            go to 100
         endif
c        # a complete valid walk has been generated.
c
         mul = 1
         if(spnorb)mul = b(rown) + 1
         do 179 imul = 1, mul
               isym  = mult(wsym(nlev), spnir(imul, mul))
               issym = mult(isym,ssym)
               if ( nexw(issym,ver) .ne. 0 ) then
                  nvalwt = nvalwt+1
                  csym(nvalwt) = issym
                  nviwsm(ver,isym) = nviwsm(ver,isym) + 1
               endif
179      continue
c
         go to 100
c
180      continue
c        # finished with walk generation for this vertex.
200   continue
c
c     # check the valid upper walk count...
      if(numv1.ne.nvalwt)call bummer('symwlk: numv1-nvalwt=',
     & numv1-nvalwt,faterr)
c
      call pscsf(
     & nsym,   slabel, ssym,   nviwsm,
     & mult,   nexw,   ncsft,  ncsfsm )
c
c     # change limvec(*) back to 0/1 form.
c
      call skpx01( nwalk, limvec, to01, numv1 )
      if(numv1.ne.nvalwt)call bummer('symwlk: numv1-nvalwt=',
     & numv1-nvalwt,faterr)
c
      return
c
6100  format(1x,a,':',(8i8))
      end
      subroutine pscsf(
     & nsym,   slabel, ssym,   nviwsm,
     & mult,   nexw,   ncsft,  ncsfsm )
c
c  compute and print symmetry related csf info.
c
c  input:
c  nsym = number of symmetry blocks.
c  slabel(*) = symmetry labels.
c  ssym = state spatial symmetry.
c  nviwsm(1:4,1:nsym) = ctot(iver,isym) is the number of upper walks
c                       of symmetry isym from vertex iver.
c  mult(*) = irrep multiplication table.
c  nexw(*) = nexw(isym,iver) is the number of external walks of
c            symmetry isym from vertex iver.
c
c  output:
c  ncsft   = total number of csfs for this orbital basis and drt.
c  ncsfsm(1:4,1:nsym) = ncsfsm(iver,isym) is the number of csfs
c                       with internal walk symmetry of isym
c                       through vertex iver.
c
       implicit none 
C====>Begin Module PSCSF                  File cidrtms2.f               
C---->Makedcls Options: All variables                                   
C
C     Argument variables
C
      CHARACTER*4         SLABEL(0:NSYM)
C
      INTEGER             MULT(8,8),   NCSFSM(4,8), NCSFT
      INTEGER             NEXW(8,4),   NSYM,        NVIWSM(4,8), SSYM
C
C     Local variables
C
      CHARACTER           WXYZ(4)
C
      INTEGER             I,           ISSYM,       ISYM,        IVER
      INTEGER             NCSFV(4)
C
      INCLUDE 'cfiles.inc'
C====>End Module   PSCSF                  File cidrtms2.f               
      data wxyz/'z','y','x','w'/
c
      write(nlist,6010)
     & ' number of valid internal walks of each symmetry:',
     & (slabel(i),i=1,nsym)
      write(nlist,6020)('----',i=1,nsym)
      do 10 iver=1,4
         write(nlist,6030)wxyz(iver),(nviwsm(iver,i),i=1,nsym)
10    continue
c
      if ( nviwsm(1,ssym) .eq. 0 ) then
         write(nlist,*)
     &    '*** warning: no z-walks of the correct symmetry. ***'
      endif
c
c     # compute csf counts...
c
      do 30 iver = 1, 4
         ncsfv(iver) = 0
         do 20 isym = 1, nsym
            issym = mult( isym, ssym )
            if( (nexw(issym,iver) .eq. 0)
     &       .and. (nviwsm(iver,isym) .ne. 0) ) then
               write(nlist,*) 'nviwsm(*) error: '
     &          //wxyz(iver)//'-vertex, isym=',isym
            endif
            ncsfsm(iver,isym) = nexw(issym,iver) * nviwsm(iver,isym)
            ncsfv(iver) = ncsfv(iver) + ncsfsm(iver,isym)
20       continue
30    continue
c
      write(nlist,6010)'csfs grouped by internal walk symmetry:',
     & (slabel(i),i=1,nsym)
      write(nlist,6020)('----',i=1,nsym)
      do 40 iver = 1,4
         write(nlist,6030)wxyz(iver),(ncsfsm(iver,i),i=1,nsym)
40    continue
c
      write(nlist,6040) (wxyz(iver), ncsfv(iver), iver = 1, 4)
      ncsft = ncsfv(1) + ncsfv(2) + ncsfv(3) + ncsfv(4)
      write(nlist,6050) ncsft
c
      return
6010  format(/1x,a//2x,8(4x,a4))
6020  format(2x,8(4x,a4))
6030  format(1x,a1,8i8)
6040  format(/' total csf counts:'/(1x,a,'-vertex:',i9))
6050  format(11x,8('-')/' total:',i12)
      end

      subroutine selctr(
     & niot,   nrow,   nrowmx, neli,
     & nwalk,  nref,   nrfmx,  ssym,
     & nsym,   a,      b,      l,
     & y,      xbar,   slabel, modrt,
     & step,   row,    ytot,   arcsym,
     & wsym,   bminr,  bmaxr,  occmnr,
     & occmxr, smaskr, ref,    mult,
     & qgis,  syml,nmskp, *,   spnorb, spnodd )
       implicit none 
C====>Begin Module SELCTR                 File cidrtms2.f               
C---->Makedcls Options: All variables                                   
C
C     External functions
C
      EXTERNAL            GETREF
C
      INTEGER             GETREF
C
C     Parameter variables
C
      INTEGER             NGPMX
      PARAMETER           (NGPMX = 50)
      INTEGER             GLDIM
      PARAMETER           (GLDIM = 1000)
      INTEGER             NDEFMX
      PARAMETER           (NDEFMX = 20)
      INTEGER             NPRFMX
      PARAMETER           (NPRFMX = 256)
      INTEGER             PRFDIM
      PARAMETER           (PRFDIM = 1000)
      INTEGER             YES
      PARAMETER           (YES = 1)
      INTEGER             NO
      PARAMETER           (NO = 0)
C
C     Argument variables
C
      CHARACTER*4         SLABEL(0:8)
C
      INTEGER             A(NROW),     ARCSYM(0:3,0:NIOT),       B(NROW)
      INTEGER             BMAXR(NIOT), BMINR(NIOT), L(0:3,NROWMX)
      INTEGER             MODRT(0:NIOT),            MULT(8,8),   NELI
      INTEGER             NIOT,        NMSKP(8),    NREF,        NRFMX
      INTEGER             NROW,        NROWMX,      NSYM,        NWALK
      INTEGER             OCCMNR(NIOT),             OCCMXR(NIOT)
      INTEGER             REF(*),      ROW(0:NIOT), SMASKR(NIOT)
      INTEGER             SSYM,        STEP(0:NIOT)
      INTEGER             SYML(0:NIOT),             WSYM(0:NIOT)
      INTEGER             XBAR(NROW),  Y(0:3,NROW), YTOT(0:NIOT)
C
      LOGICAL             QGIS,        SPNODD,      SPNORB
C
C     Local variables
C
      INTEGER             BN,          CHAR,        CLEV,        I
      INTEGER             IMUL,        ISTEP,       ISYM,        IWALK
      INTEGER             MUL,         NARS,        NLEV,        NZWALK
      INTEGER             OCCN,        ROWC,        ROWN,        RSYM(8)
      INTEGER             WLKSYM
C
      LOGICAL             QGROUP,      QINDIV,      QREF,        QREJCT
      LOGICAL             QRSYM(8)
C
      INCLUDE 'cfiles.inc'
      INCLUDE 'cgroup.inc'
      INCLUDE 'cref.inc'
C====>End Module   SELCTR                 File cidrtms2.f               
c
c  control the various reference-csf selection schemes available.
c  reference-csfs must be z-walks.
c
c  08-jun-89 qgis check added. -rls
c  07-may-89 symmetry selection fixed. -rls
c
c
6100  format(1x,a,':',(8i8))
c
      bn = 0
      qrejct = .false.
      rowc = 0
      rown = 0
      char = 0
      nars = 0
      occn = 0
c
      call izero_wr(ndefmx,ngroup,1)
      nref = 0
      nzwalk = xbar(1)
100   write(nlist,6010)nzwalk
6010  format(/' reference-csf selection step 1:'
     & /' total number of z-walks in the drt, nzwalk=',i8/)
c
      do 110 i = 1,8
         rsym(i) = 0
         qrsym(i) = .false.
110   continue
      rsym(1) = ssym
c
      call iniv(rsym,nsym,
     & 'input the list of allowed reference symmetries',*9000)
c
      nars = 0
      do 120 i = 1,nsym
         if(rsym(i).eq.0)then
            go to 121
         elseif(rsym(i).lt.0 .or. rsym(i).gt.nsym)then
            write(nlist,*) '0 < rsym(i) <= nsym'
            go to 100
         else
            nars = nars+1
            qrsym(mult(rsym(i),ssym)) = .true.
         endif
120   continue
121   continue
      write(nlist,6101)'allowed reference symmetries:',
     & (rsym(i),i=1,nars)
6101  format(1x,a,t40,8i5)
      write(nlist,6102)'allowed reference symmetry labels:',
     & (slabel(rsym(i)),i=1,nars)
6102  format(1x,a,t40,8a5)
c
      if(.not.qrsym(1))then
         write(nlist,*) 'symmetry ',ssym,' must be allowed.'
         go to 100
      endif
      if(qgis.and.(nars.gt.1))then
         write(nlist,*) 'gis and nars>1 is not consistent.'
         go to 100
      endif
c
200   continue
c  *** this input should come before the allowed-symmetry input.
c      fix this the next time the input is changed. -rls ***
c
      char = no
      call inyn( char, 'keep all of the z-walks as references?', *100 )
c
      if ( char .eq. yes ) then
c        # keep all z-walks.
         if ( nzwalk .gt. nrfmx ) then
            write(nlist,6100)'nzwalk,nrfmx',nzwalk,nrfmx
            go to 100
         endif
         nref = nzwalk
         write(nlist,*)
     &    'all z-walks of all symmetries are initially retained.'
         do 210 i = 1,nzwalk
c     can be done in a more efficient way!
            call setref(ref,i)
210      continue
         go to 2000
      endif
c
300   continue
c
c     # initialize walks as invalid...
c
      write(nlist,*) 'all z-walks are initially deleted.'
      nref = 0
      do 310 i = 1,nzwalk
c     can be done in a more efficient way!
            call unsetref(ref,i)
310   continue
c
      write(nlist,*)
      char = yes
      call inyn(char,
     & 'generate walks while applying reference drt restrictions?',*100)
      if(char.eq.no)then
         write(nlist,*) 'walks will not be generated.'
         go to 2000
      endif
c
      write(nlist,*) 'reference drt restrictions will be imposed'
     & //' on the z-walks.'
      write(nlist,*)
      char = no
      call inyn(char,
     & 'impose additional orbital-group occupation restrictions?',*300)
      qgroup = char.eq.yes
c
c     # get group information.
c
      if(qgroup)call groupi(niot,modrt,syml,slabel(1),nmskp,*300)
      qgroup = qgroup.and.(ndef.gt.0)
      if(qgroup)write(nlist,*)
     & 'orbital-group restrictions will be imposed.'
c
400   continue
      write(nlist,*)
      char = no
      call inyn(char,
     & 'apply primary reference occupation restrictions?',*300)
      qref = char.eq.yes
c
c     # get primary reference information.
c
      if(qref)call prefi(niot,modrt,neli,*300)
      qref = qref.and.(npref.gt.0)
      if(qref)write(nlist,*)
     & 'primary reference restrictions will be imposed.'
c
500   continue
      write(nlist,*)
      char = no
      call inyn(char,'manually select individual walks?',*300)
      qindiv = char.eq.yes
      if(qindiv)write(nlist,*) 'references will be manually selected.'
c
c     # initialize walks as invalid.  loop over all walks and determine
c     # the valid ones.  walks are determined in lexical order.
c     # reference restrictions, occmnr(*),occmxr(*),bminr(*),bmaxr(*),
c     # and smaskr(*), are applied during the walk generation.
c
      do 1020 i = 0,niot
         step(i) = 4
1020  continue
c
c     # bottom level.
c
      clev = 0
      row(0) = 1
      ytot(0) = 0
      wsym(0) = 1
c
1100  continue
      nlev = clev+1
      istep = step(clev)-1
      if(istep.lt.0)then
c        # decrement the current level.
         step(clev) = 4
         if(clev.eq.0)go to 1180
         clev = clev-1
         go to 1100
      endif
      step(clev) = istep
      rowc = row(clev)
      rown = l(istep,rowc)
      if(rown.eq.0)go to 1100
      if( mod(smaskr(nlev)/(10**istep),10).eq.0)go to 1100
      bn = b(rown)
      if(bn.lt.bminr(nlev))go to 1100
      if(bn.gt.bmaxr(nlev))go to 1100
      occn = 2*a(rown)+bn
      if(occn.lt.occmnr(nlev))go to 1100
      if(occn.gt.occmxr(nlev))go to 1100
c
      row(nlev) = rown
      ytot(nlev) = ytot(clev)+y(istep,rowc)
      wsym(nlev) = mult(arcsym(istep,clev),wsym(clev))

      if(nlev.lt.niot)then
c        # increment to the next level.
         clev = nlev
         go to 1100
      endif
c
c     # complete walk has been generated.
c
c
      iwalk = ytot(niot)
c

      isym = wsym(nlev)
      wlksym = mult(isym,ssym)
c
c     # check the current walk against the various selection modes.
c
      if(.not.qrsym(wlksym))go to 1100
c
      if(qgroup)then
         call group(step,qrejct)
         if(qrejct)go to 1100
      endif
c
      if(qref)then
         call prfchk(step,niot,qrejct)
         if(qrejct)go to 1100
      endif
c
      if(qindiv)then
         call indiv(iwalk,isym,step,niot,qrejct,*300)
         if(qrejct)go to 1100
      endif
c
c     # the walk passes all the tests.
c
      mul = 1
      if(spnorb)mul = b(rown)+1
      do 119 imul = 1, mul
         iwalk=iwalk+1
c         ref(iwalk) = +1
         call setref(ref, iwalk)
         nref = nref+1
119   continue
c
cspnorbit
      if(nref.gt.nrfmx)then
         write(nlist,6100)'nref,nrfmx',nref,nrfmx
         go to 300
      endif
      go to 1100
c
c     # exit from walk generation.
1180  continue
c
c**********************************************************************
c  step 1 complete. begin step-vector csf selection.
c**********************************************************************
c
2000  continue
      ytot(0) = 0
      row(0) = 1
      wsym(0) = 1
      write(nlist,6040) nref, nzwalk
      write(nout,6041)
6040  format(/' step 1 reference csf selection complete.'
     & /1x,i8,' csfs initially selected from',i8,' total walks.'
     & //' beginning step-vector based selection.')
6041  format(
     & ' enter [internal_orbital_step_vector/disposition] pairs:'/)
c
2010  continue
      step(0) = -1
      call inivp( modrt(1), step, niot,
     & 'enter internal orbital step vector, (-1/ to end)',*100)
      if ( step(0) .lt. 0 ) go to 3000
      do 2040 nlev = 1, niot
         clev = nlev - 1
         istep = step(clev)
         if ( (istep .lt. 0) .or. (istep .gt. 3) ) then
            write(nlist,*) '0 <= step <= 3'
            go to 2010
         endif
         rowc = row(clev)
         rown = l(istep,rowc)
         if ( rown .eq. 0 ) then
            write(nlist,*) 'not a valid walk'
            go to 2010
         endif
         row(nlev) = rown
         ytot(nlev) = ytot(clev) + y(istep,rowc)
         wsym(nlev) = mult(arcsym(istep,clev),wsym(clev))
2040  continue
      iwalk = ytot(niot) + 1
      isym = wsym(niot)
      wlksym = mult(isym,ssym)
      if ( .not. qrsym(wlksym) ) then
         write(nlist,6100)'incorrect symmetry: walk,isym',iwalk,isym
         go to 2010
      endif
      call indiv( iwalk, isym, step, niot, qrejct, *2010 )
      if ( qrejct ) then
         if ( getref(ref,iwalk) .eq. 0 ) then
            write(nlist,6100)'walk already deleted',iwalk
         else
c           ref(iwalk) = 0
            call unsetref(ref,iwalk)
            nref = nref - 1
            write(nlist,6100)'deleted: walk,isym',iwalk,isym
         endif
      else
         if ( getref(ref,iwalk) .eq. 0 ) then
c           ref(iwalk) = +1
            call setref(ref,iwalk)
            nref = nref + 1
            write(nlist,6100)'added: walk,isym', iwalk, isym
         else
            write(nlist,6100)'walk already retained', iwalk
         endif
      endif
      go to 2010
c
c**********************************************************************
c  step 2 complete. begin numerical walk-number based selection.
c**********************************************************************
c
3000  continue
      write(nlist,6030)nref,nzwalk
      write(nout,6031)
6030  format(/' step 2 reference csf selection complete.'
     & /1x,i8,' csfs currently selected from',i8,' total walks.'
     & //' beginning numerical walk based selection.')
6031  format(' enter positive walk numbers to add walks,'
     & /' negative walk numbers to delete walks, and zero to end:'/)
c
3010  continue
      iwalk = 0
      call ini( iwalk, 'input reference walk number (0 to end)', *100 )
c
      if ( iwalk .lt. 0 ) then
c        # delete walk...
         iwalk = -iwalk
         if ( iwalk .gt. nzwalk ) then
            write(nlist,6100)'walk too big',iwalk
         elseif ( getref(ref,iwalk) .eq. 0 ) then
            write(nlist,6100)'walk already deleted',iwalk
         else
            call unsetref(ref,iwalk)
            nref = nref - 1
            call gstepc(iwalk, 1, niot, l,
     &       y, arcsym, mult, step, isym )
            write(nlist,6200)(step(i),i=0,niot-1)
            write(nlist,6100)'deleted: walk,isym', iwalk, isym
         endif
         go to 3010
      elseif ( iwalk .gt. 0 ) then
c        # add walk...
         if ( iwalk .gt. nzwalk ) then
            write(nlist,6100)'walk too big', iwalk
         elseif ( getref(ref,iwalk) .eq. 0 ) then
            call gstepc(iwalk, 1, niot, l,
     &       y, arcsym, mult, step, isym )
            write(nlist,6200) (step(i),i=0,niot-1)
            wlksym = mult(isym,ssym)
            if ( qrsym(wlksym) ) then
               call setref(ref,iwalk)
               nref = nref + 1
               write(nlist,6100)'added: walk,isym', iwalk, isym
            else
               write(nlist,6100)'incorrect symmetry: walk,isym',
     &          iwalk, isym
            endif
         else
            write(nlist,6100)'walk already retained', iwalk
         endif
         go to 3010
      endif
c
c     # finished.
      write(nlist,6090)nref,nzwalk
6090  format(/' numerical walk-number based selection complete.'
     & /1x,i8,' reference csfs selected from',i8,' total z-walks.')
cdeb
c     do i=1,nzwalk/32
c      write(*,*) 'ref(',i,')=',ref(i)
c     enddo
c     call uncmpref(ref,ref(nzwalk+1),nzwalk)
cdeb
      if ( nref .eq. 0 ) then
         write(nlist,*) 'selctr: nref must be nonzero'
         go to 3000
      endif
c
      return
c
9000  return 1
6200  format(' step:',(1x,50i1))
      end

      subroutine gstepc(
     & iwalk,   iver,   niot,   l,
     & y,       arcsym, mult,   step,
     & isym )
c
c  generate the step vector and walk symmetry for a given
c  internal walk index.
c
c  08-jun-89 iver added for non-z-walks. -rls
c  07-may-89 written by ron shepard.
c
       implicit none 
C====>Begin Module GSTEPC                 File cidrtms2.f               
C---->Makedcls Options: All variables                                   
C
C     Parameter variables
C
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
C
C     Argument variables
C
      INTEGER             ARCSYM(0:3,0:NIOT),       ISYM,        IVER
      INTEGER             IWALK,       L(0:3,*),    MULT(8,8),   NIOT
      INTEGER             STEP(0:NIOT),             Y(0:3,*)
C
C     Local variables
C
      INTEGER             CLEV,        ISTEP,       ISTEPT,      NLEV
      INTEGER             ROWC,        ROWN,        YTEMP,       YTOT
C
C====>End Module   GSTEPC                 File cidrtms2.f               
c
      rown = 0
      istep = 0
      ytemp = 0
c
      ytot = iwalk - 1
      rowc = iver
      isym = 1
      do 20 nlev = 1, niot
         clev = nlev - 1
         do 10 istept = 0, 3
            istep = istept
            rown = l(istep,rowc)
            if ( rown .eq. 0 ) go to 10
            ytemp = ytot - y(istep,rowc)
            if ( ytemp .ge. 0 ) go to 11
10       continue
c        # ...no valid exit from the loop.
         call bummer('gstepc: ytemp=',ytemp,faterr)
11       continue
         ytot = ytemp
         rowc = rown
         step(clev) = istep
         isym = mult( arcsym(istep,clev), isym )
20    continue
      if ( ytot .ne. 0 ) call bummer('gstepc: ytot=',ytot,faterr)
      return
      end
c*deck refnew
      subroutine refnew(
     & niot, xbarz, ref,  nref,
     & refn, nrow,  y,    l,
     & yold, xbold, lold, step,
     & ytot, yntot, row,  spnorb,
     & b,    spnodd )
       implicit none 
C====>Begin Module REFNEW                 File cidrtms2.f               
C---->Makedcls Options: All variables                                   
C
C     External functions
C
      EXTERNAL            GETIREF
C
      INTEGER             GETIREF
C
C     Parameter variables
C
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
      INTEGER             TOSKP
      PARAMETER           (TOSKP = 0)
      INTEGER             TO01
      PARAMETER           (TO01 = 1)
C
C     Argument variables
C
      INTEGER             XBARZ
      INTEGER             B(*),        L(0:3,NROW), LOLD(0:3,NROW)
      INTEGER             NIOT,        NREF,        NROW
      INTEGER             REF(XBARZ),  REFN(NREF),  ROW(0:NIOT)
      INTEGER             STEP(0:NIOT)
      INTEGER             XBOLD(NROW), Y(0:3,NROW), YNTOT(0:NIOT)
      INTEGER             YOLD(0:3,NROW),           YTOT(0:NIOT)
C
      LOGICAL             SPNODD,      SPNORB
C
C     Local variables
C
      INTEGER             CLEV,        I,           IMUL,        IREF
      INTEGER             ISTEP,       IWALK,       MUL,         NLEV
      INTEGER             NUMV1,       ROWC,        ROWN
C
      INCLUDE 'cfiles.inc'
C====>End Module   REFNEW                 File cidrtms2.f               
c
c  recompute reference csf indices with the new chaining arrays.
c
c  input:
c  niot = total number of internal orbitals.
c  xbarz = total number of z walks using old indexing arrays.
c  ref(1:xbarz) = 0/1 representation using old indexing arrays.
c  nref = number of references.
c  nrow = number of distinct rows in the drt arrays.
c  y(*),l(*) = new drt chaining indices.
c  yold(*),xbold(*),lold(*) = old drt chaining indices.
c  ytot(*),step(*),yntot(*),row(*) = temp walk generation arrays.
c
c  output:
c  refn(1:nref) = new reference csf indices.
c
c  2-jun-89 written by ron shepard.
c
c
      write(nlist,'(/1x,a)')
     & 'beginning the reference csf index recomputation...'
      write(nlist,6010)
c
c     # change ref(*) to skip vector form.
c
      numv1 = 0
ct    call skpx01( xbarz, ref, toskp, numv1 )
ct    if(nref.ne.numv1)call bummer('refnew: 0 nref-numv1=',
ct   & (nref-numv1),faterr)
c
      iref     = 0
      clev     = 0
      row(0)   = 1
      ytot(0)  = 0
      yntot(0) = 0
c
      do 20 i = 0, niot
         step(i) = 4
20    continue
c
c     # begin walk generation...
c
100   continue
c     # decrement the step at the current level.
      nlev = clev+1
      istep = step(clev)-1
      if(istep.lt.0)then
c        # decrement the current level, and check if done.
         step(clev) = 4
         if(clev.eq.0)goto 200
         clev = clev-1
         go to 100
      endif
      step(clev) = istep
      rowc = row(clev)
      rown = lold(istep,rowc)
c     # valid row in the old drt?
      if(rown.eq.0)go to 100
      ytot(nlev) = ytot(clev)+yold(istep,rowc)
c     # can the next reference be reached from here?
      if( xbold(rown) .le. getiref(xbarz,ref,ytot(nlev)+1) )go to 100
c     # valid row in the new drt?
ctm ???
c     if(l(istep,rowc).eq.0)then
c        call bummer('refnew: error rowc=',rowc,faterr)
c     endif
ctm
      row(nlev) = rown
      yntot(nlev) = yntot(clev)+y(istep,rowc)
      if(nlev.lt.niot)then
c        # increment to the next level.
         clev = nlev
         go to 100
      else
c        # complete reference walk has been generated.
c        # complete reference walk has been generated.
c
c
         mul = 1
         iwalk = yntot(nlev) + 1
         if( spnorb )mul = b(rown) + 1
         do 179 imul = 1, mul
            iref = iref+1
            refn(iref) = iwalk
            write(nlist,6020)iref,iwalk,(step(i),i=0,(niot-1))
            iwalk = iwalk + 1
179      continue
         goto 100
c
cspinorb
      endif
200   continue
c     # all done, check the reference csf count.
      if(iref.ne.nref)call bummer('refnew: nref-iref=',
     & (nref-iref),faterr)
c
c     # change ref(*) back.
c
ct    call skpx01( xbarz, ref, to01, numv1 )
ct    if(nref.ne.numv1)call bummer('refnew: 1 nref-numv1=',
ct   & (nref-numv1),faterr)
c
      return
6010  format(/1x,'    iref   iwalk',t20,'step-vector'/
     &        1x,'  ------  ------',t20,12('-'))
6020  format(1x,2i8,(t20,50i1))
      end

      subroutine limnew(
     & niot,   nwalko, limvec, nvalwt,
     & nvalw,  limn,   nrow,   nrowmx,
     & y,      l,      yold,   xbold,
     & lold,   step,   ytot,   yntot,
     & row  )
c
c  recompute valid upper walk indices with the new chaining arrays.
c
c  input:
c  niot = total number of internal orbitals.
c  nwalko = total number of walks using old indexing arrays.
c  limvec(1:nwalko) = 0/1 representation using old indexing arrays.
c  nvalwt = number of valid upper walks.
c  nvalw(*) = valid upper walks for z,y,x,w-vertices.
c  nrow,nrowmx = drt array dimensions.
c  y(*),l(*) = new drt chaining arrays.
c  yold(*),xbold(*),lold(*) = old drt chaining arrays.
c  ytot(*),step(*),yntot(*),row(*) = temp walk generation arrays.
c
c  output:
c  limn(1:nvalwt) = new valid upper walk indices.
c
c  2-jun-89 written by ron shepard.
c
       implicit none 
C====>Begin Module LIMNEW                 File cidrtms2.f               
C---->Makedcls Options: All variables                                   
C
C     Parameter variables
C
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
      INTEGER             TOSKP
      PARAMETER           (TOSKP = 0)
      INTEGER             TO01
      PARAMETER           (TO01 = 1)
C
C     Argument variables
C
      INTEGER             L(0:3,NROWMX,3),          LIMN(NVALWT)
      INTEGER             LIMVEC(NWALKO),           LOLD(0:3,NROWMX,3)
      INTEGER             NIOT,        NROW,        NROWMX
      INTEGER             NVALW(4),    NVALWT,      NWALKO
      INTEGER             ROW(0:NIOT), STEP(0:NIOT)
      INTEGER             XBOLD(NROWMX,3),          Y(0:3,NROWMX,3)
      INTEGER             YNTOT(0:NIOT),            YOLD(0:3,NROWMX,3)
      INTEGER             YTOT(0:NIOT)
C
C     Local variables
C
      INTEGER             CLEV,        I,           ISTEP,       IWALK
      INTEGER             IWALKN,      NLEV,        NUMV1,       NVALWV
      INTEGER             ROWC,        ROWN,        VER,         VER1
      INTEGER             VERS(4)
C
      INCLUDE 'cfiles.inc'
C====>End Module   LIMNEW                 File cidrtms2.f               
      data vers/1,2,3,3/
c
      write(nlist,'(/1x,a)')
     & 'beginning the valid upper walk index recomputation...'
c
c     # change limvec(*) to skip vector form.
c
      numv1 = 0
      call skpx01( nwalko, limvec, toskp, numv1 )
      if(nvalwt.ne.numv1)call bummer('limnew: 0 nvalwt-numv1 = ',
     & (nvalwt-numv1),faterr)
c
      do 20 i=0,niot
         step(i) = 4
20    continue
c
c     # loop over z,y,x,w-vertices...
c
      iwalkn = 0
      do 200 ver = 1,4
         ver1 = vers(ver)
         nvalwv = 0
         clev = 0
         row(0) = ver
         ytot(0) = 0
c
c        # begin walk generation...
c
100      continue
c        # decrement the step at the current level.
         nlev = clev+1
         istep = step(clev)-1
         if(istep.lt.0)then
c           # decrement the current level.
            step(clev) = 4
            if(clev.eq.0)goto 180
            clev = clev-1
            go to 100
         endif
         step(clev) = istep
         rowc = row(clev)
         rown = lold(istep,rowc,ver1)
c        # valid row in the old drt?
         if(rown.eq.0)go to 100
         ytot(nlev) = ytot(clev)+yold(istep,rowc,ver1)
c        # can the next valid walk be reached from here?
         if( limvec(ytot(nlev)+1) .ge. xbold(rown,ver1) )go to 100
c        # valid row in the new drt?
         if(l(istep,rowc,ver1).eq.0)then
            call bummer('limnew: error rowc=',rowc,faterr)
         endif
         row(nlev) = rown
         yntot(nlev) = yntot(clev)+y(istep,rowc,ver1)
         if(nlev.lt.niot)then
c           # increment to the next level.
            clev = nlev
            go to 100
         else
c           # complete walk has been generated.
            nvalwv = nvalwv+1
            iwalkn = iwalkn+1
            iwalk = yntot(nlev)+1
            limn(iwalkn) = iwalk
            go to 100
         endif
180      continue
c        # all done with this vertex.  check valid walk count.
         if(nvalwv.ne.nvalw(ver))call bummer(
     &    'limnew: nvalw(ver)-nvalwv=',(nvalw(ver)-nvalwv),faterr)
200   continue
c     # all done, check valid walk count.
      if(iwalkn.ne.nvalwt)call bummer(
     & 'limnew: nvalwt-iwalkn=',(nvalwt-iwalkn),faterr)
c
c     # change limvec(*) back.
c
      call skpx01( nwalko, limvec, to01, numv1 )
      if(nvalwt.ne.numv1)call bummer('limnew: 1 nvalwt-numv1=',
     & (nvalwt-numv1),faterr)
c
      return
      end

      subroutine drtcpy( nrow, nrowmx, y, xbar, l, yold, xbold, lold )
c
c  copy the drt chaining arrays.
c
       implicit none 
C====>Begin Module DRTCPY                 File cidrtms2.f               
C---->Makedcls Options: All variables                                   
C
C     Argument variables
C
      INTEGER             L(0:3,NROWMX,3),          LOLD(0:3,NROWMX,3)
      INTEGER             NROW,        NROWMX,      XBAR(NROWMX,3)
      INTEGER             XBOLD(NROWMX,3),          Y(0:3,NROWMX,3)
      INTEGER             YOLD(0:3,NROWMX,3)
C
C     Local variables
C
      INTEGER             I
C
C====>End Module   DRTCPY                 File cidrtms2.f               
c
      do 10 i = 1, 3
         call icopy_wr( 4*nrow, y(0,1,i),  1, yold(0,1,i), 1 )
         call icopy_wr( nrow,   xbar(1,i), 1, xbold(1,i),  1 )
         call icopy_wr( 4*nrow, l(0,1,i),  1, lold(0,1,i), 1 )
10    continue
c
      return
      end

c*deck lprune
      subroutine lprunenew(
     & nlist,  niot,  nwalk,
     & ref,    nrow,  y,
     & l,      xbar,  step,   ytot,
     & yntot,  row,   lnew,  spnorb,
     & b,      spnodd  )
       implicit none 
C====>Begin Module LPRUNENEW              File cidrtms2.f               
C---->Makedcls Options: All variables                                   
C
C     Parameter variables
C
      INTEGER             NROWMX
      PARAMETER           (NROWMX = 2**10-1)
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
      INTEGER             TOSKP
      PARAMETER           (TOSKP = 0)
      INTEGER             TO01
      PARAMETER           (TO01 = 1)
C
C     Argument variables
C
      INTEGER             B(*),        L(0:3,NROWMX,3)
      INTEGER             LNEW(0:3,NROWMX,3),       NIOT,        NLIST
      INTEGER             NROW,        NWALK,       REF(*)
      INTEGER             ROW(0:NIOT), STEP(0:NIOT)
      INTEGER             XBAR(NROWMX,3),           Y(0:3,NROWMX,3)
      INTEGER             YNTOT(0:NIOT),            YTOT(0:NIOT)
C
      LOGICAL             SPNODD,      SPNORB
C
C     Local variables
C
      CHARACTER*2         CDRT(3)
C
      INTEGER             CLEV,        I,           IMUL,        IROW
      INTEGER             ISTEP,       IWALK,       MUL,         NLEV
      INTEGER             NPRUNE,      NREF,        NUMV1,       NVALWT
      INTEGER             ROWC,        ROWN,        VER,         VER1
C
      INCLUDE 'special.inc'
C====>End Module   LPRUNENEW              File cidrtms2.f               
c
c  prune l(*,*,*) according to limvec(*) so that it results in
c  the smallest drt and the most compact indexing scheme.
c
c  input:
c  niot = total number of internal orbitals.
c  nwalk = total number of walks using the current indexing scheme.
c  ref(1:xbar(1,1)) = 0/1 representation (current indexing scheme).
c  nrow = number of distinct rows in the drt arrays.
c  nrowmx = maximum number of rows.
c  y(*),xbar(*),l(*) = current drt chaining indices.
c  ytot(*),step(*),yntot(*),row(*) = temp walk generation arrays.
c
c  output:
c  lnew(*) = pruned chaining array.
c
c  31-jul-89 ref(*) checks added for z-walks. -rls
c  05-jun-89 written by ron shepard.
c
      data cdrt/' z',' y','wx'/
c
      rown = 0
      numv1 = 0
      rowc = 0
c
c     # initialize lnew(*)...
      call izero_wr(12*nrowmx,lnew,1)
c
c     # change limvec(*) and ref(*) to skip vector form.
c
c     call skpx01( nwalk,     limvec, toskp, numv1 )
      call skpx01( xbar(1,1), ref,    toskp, nref  )
c
c     # loop over z-, y-, x-, and w-vertex walks...
c
      nvalwt = 0
      do 200 ver = 1,4
c
c        # begin walk generation for this vertex...
c
         ver1 = min(3,ver)
         clev = 0
         row(0) = ver
         ytot(0) = 0
c
         do 20 i = 0,niot
            step(i) = 4
20       continue
c
100      continue
c        # decrement the step at the current level.
         nlev = clev+1
         istep = step(clev)-1
         if(istep.lt.0)then
c           # decrement the current level.
            step(clev) = 4
            if(clev.eq.0)goto 200
            clev = clev-1
            go to 100
         endif
         step(clev) = istep
         rowc = row(clev)
         rown = l(istep,rowc,ver1)
         if(rown.eq.0)go to 100
         ytot(nlev) = ytot(clev)+y(istep,rowc,ver1)
c        if(ver.eq.1)then
c           # z-walk; check both limvec(*) and ref(*).
ct          if( ( limvec(ytot(nlev)+1) .ge. xbar(rown,ver1) )
ct   &       .and. ( ref(ytot(nlev)+1) .ge. xbar(rown,ver1) ))go to 100
c        else
c           # w,x, or y walk.  just check limvec(*).
c           if( limvec(ytot(nlev)+1) .ge. xbar(rown,ver1) )go to 100
c        endif
           if (valrow(rowc,ver1).eq.0) goto 100
           if (valarc(istep,rowc,ver1).eq.0) goto 100
c        # this step is used by at least one valid or reference walk.
         lnew(istep,rowc,ver1) = rown
         row(nlev) = rown
         if(nlev.lt.niot)then
c           # increment to the next level.
            clev = nlev
            go to 100
         else
ct          if(limvec(ytot(nlev)+1) .eq. 0 )then
c              # complete valid walk has been generated.
c              nvalwt = nvalwt+1
c           endif
            step(clev) = 4
            clev = clev-1
            go to 100
         endif
200   continue
c
      write(nlist,'(/1x,a,i8,a,i8)')
     & 'lprune: l(*,*,*) pruned with nwalk=',nwalk,' nvalwt=',nvalwt
c
c     # compute the number of pruned arcs.
      do 230 ver1 = 1,3
         nprune = 0
         do 220 irow = 1,nrow
            do 210 istep = 0,3
               if(lnew(istep,irow,ver1).ne.l(istep,irow,ver1))
     &          nprune = nprune+1
210         continue
220      continue
         write(nlist,'(1x,a,i6)') 'lprune: ' // cdrt(ver1) //
     &    '-drt, nprune=', nprune
230   continue
c
c     # change limvec(*) and ref(*) back to 0/1 form...
c
c     call skpx01( nwalk, limvec, to01, numv1 )
c     if(numv1.ne.nvalwt)call bummer('lprune: nvalwt-numv1=',
c    & (nvalwt-numv1),faterr)
      call skpx01( xbar(1,1), ref, to01, nref )
c
      return
      end

      subroutine groupi( niot, modrt,syml,slabel,nmskp, * )
c
c  read in group occupation information.
c
       implicit none 
C====>Begin Module GROUPI                 File cidrtms2.f               
C---->Makedcls Options: All variables                                   
C
C     Parameter variables
C
      INTEGER             NGPMX
      PARAMETER           (NGPMX = 50)
      INTEGER             GLDIM
      PARAMETER           (GLDIM = 1000)
      INTEGER             NDEFMX
      PARAMETER           (NDEFMX = 20)
C
C     Argument variables
C
      CHARACTER*4         SLABEL(*)
C
      INTEGER             MODRT(0:NIOT),            NIOT
      INTEGER             NMSKP(8),    SYML(0:NIOT)
C
C     Local variables
C
      INTEGER             I,           I0,          I1,          I2
      INTEGER             I2X,         IDEF,        IG,          IMO
      INTEGER             J,           JMO,         MONMB(GLDIM)
      INTEGER             MOSYM(GLDIM),             NMOPG
C
      INCLUDE 'cfiles.inc'
      INCLUDE 'cgroup.inc'
C====>End Module   GROUPI                 File cidrtms2.f               
6100  format(1x,a,':',i8)
c
      i2 = 0
c
100   continue
      write(nlist,*) 'orbitals are selected if they satisfy any'
     . //' of the orbital group definitions'
      write(nlist,*) 'within each orbital group occupation '
     .// 'specification.'
      write(nlist,*) 'each reference csf must satisfy all group'
     &  //' occupation restrictions.'
      write(nlist,6100) 'maximum number of orbital group definitions',
     . ndefmx
      write(nlist,6100) 'maximum number of orbital groups',ngpmx
      write(nlist,6100) 'maximum total number of orbitals allowed',gldim
      write(nlist,*) 'note that orbitals are specified by level.'
      write(nlist,*)
      ndef=0
 890  call ini(ndef,'input the number of orbital group definitions'
     .  ,*9000)
      if ( (ndef.le.0) .or. (ndef.gt.ndefmx)) then
        write(nlist,*) '0 < ndef < ndefmx'
        goto 890
      endif
      do idef=1,ndef
 891  ngroup(idef)=0
      call ini(ngroup(idef),'input the number of orbital groups',*9000)
      write(nlist,6100)'ngroup',ngroup(idef)
      if(ngroup(idef).lt.0 .or. ngroup(idef).gt.ngpmx)then
          write(nlist,*) '0 < ngroup <= ngpmx'
          go to 891
      endif
      write(nlist,*) 'for each group, input the number of orbitals'
     &  //' in the group, the minimum group'
      write(nlist,*) 'occupation, the maximum group occupation, and the'
     &  //'  list of levels in the group.'
      write(nlist,*)
      write(nlist,6060) 'modrt(*)',(modrt(i),i=1,niot)
      write(nlist,6060) 'level:  ',(i,i=1,niot)
6060  format(1x,a,(1x,20i3))
      write(nlist,*)
c
      i2=0
      do 180 ig=1,ngroup(idef)
          i1=i2+1
110       continue
          write(nlist,6100)'specification for group',ig
          gpinfo(1,ig,idef)=0
          gpinfo(2,ig,idef)=0
          gpinfo(3,ig,idef)=0
          call iniv(gpinfo(1,ig,idef),3,'input nmopg,gmin,gmax',*100)
          nmopg=gpinfo(1,ig,idef)
          if(nmopg.le.0)then
              write(nlist,*) '0 < nmopg'
              go to 110
          endif
          if(nmopg.gt.niot)then
              write(nlist,*) 'nmopg <= niot'
              go to 110
          endif
          if(gpinfo(2,ig,idef).lt.0)then
              write(nlist,*) '0 <= gmin'
              go to 110
          endif
          if(gpinfo(3,ig,idef).lt.gpinfo(2,ig,idef))then
              write(nlist,*) 'gmin <= gmax'
              go to 110
          endif
          i2x=i2+nmopg
          if(i2x.gt.gldim)then
              write(nlist,6100)'total orbitals',i2x
              write(nlist,*) 'total orbitals <= gldim'
              go to 110
          endif
140       continue
          call izero_wr(nmopg,glevel(i1,idef),1)
          call iniv(glevel(i1,idef),nmopg,'glevel(*)',*110)
          do 160 imo=i1,i2x
              if(glevel(imo,idef).le.0 .or.
     .                  glevel(imo,idef).gt.niot)then
                  write(nlist,*) '0 < level <= niot'
                  go to 140
              endif
              do 150 jmo=i1,(imo-1)
                  if(glevel(jmo,idef).eq.glevel(imo,idef))then
                      write(nlist,*) 'duplicate levels'
                      go to 140
                  endif
150           continue
160       continue
          i2=i2x
180   continue
c
      enddo ! idef
      do idef=1,ndef
      write(nlist,6071) idef
      write(nlist,6070)
c6070  format(/'  g nmopg gmin gmax glevel(*)')
6070  format(/' group minocc maxocc orbitals ')
6071  format(/'  Orbital group definition set #',i2)
6081  format(4x,i2,i5,i5,(t20,20(i2,a3)))
      i0=0
      do 200 ig=1,ngroup(idef)
          nmopg=gpinfo(1,ig,idef)
c         write(nlist,6080)
c    &      ig,(gpinfo(j,ig,idef),j=2,3),(glevel(i0+i,idef),i=1,nmopg)
           do i=1,nmopg
            mosym(i)=syml(glevel(i0+i,idef))
            monmb(i)=modrt(glevel(i0+i,idef))-nmskp(mosym(i))
           enddo
          write(nlist,6081)
     &     ig,(gpinfo(j,ig,idef),j=2,3),
     &         ( monmb(i),slabel(mosym(i))(2:4),i=1,nmopg)
          i0=i0+nmopg
200   continue
      enddo
      return
c
9000  return 1
c
      end

      subroutine group( step, qrejct )
c
c  determine if the csf specified in step(*) passes the
c  group occupation restrictions.
c  csf must pass all of the group restrictions in order to qualify.
c
       implicit none 
C====>Begin Module GROUP                  File cidrtms2.f               
C---->Makedcls Options: All variables                                   
C
C     Parameter variables
C
      INTEGER             NGPMX
      PARAMETER           (NGPMX = 50)
      INTEGER             GLDIM
      PARAMETER           (GLDIM = 1000)
      INTEGER             NDEFMX
      PARAMETER           (NDEFMX = 20)
C
C     Argument variables
C
      INTEGER             STEP(*)
C
      LOGICAL             QREJCT
C
C     Local variables
C
      INTEGER             I,           I1,          I2,          IDEF
      INTEGER             IG,          NELI,        NMOPG,       NOK
      INTEGER             STEPOC(0:3)
C
      INCLUDE 'cgroup.inc'
C====>End Module   GROUP                  File cidrtms2.f               
      data stepoc/0,1,1,2/
c
      qrejct = .true.
      nok=0
      do idef=1,ndef
      i2 = 0
      do 120 ig = 1, ngroup(idef)
          nmopg = gpinfo(1,ig,idef)
          i1 = i2 + 1
          i2 = i2 + nmopg
          neli = 0
          do 110 i = i1, i2
              neli = neli + stepoc( step(glevel(i,idef)) )
110       continue
          if ( neli .lt. gpinfo(2,ig,idef) ) goto 140
          if ( neli .gt. gpinfo(3,ig,idef) ) goto 140
120   continue
         nok=nok*10+1
140      continue
        enddo ! idef

        if (nok.gt.0) then
c
c     # ...on loop exit, csf is ok.
c
      qrejct = .false.
        endif
      return
      end

      subroutine prefi( niot, modrt, neli, * )
c
c  read in the primary reference occupation information.
c
       implicit none 
C====>Begin Module PREFI                  File cidrtms2.f               
C---->Makedcls Options: All variables                                   
C
C     Parameter variables
C
      INTEGER             NPRFMX
      PARAMETER           (NPRFMX = 256)
      INTEGER             PRFDIM
      PARAMETER           (PRFDIM = 1000)
C
C     Argument variables
C
      INTEGER             MODRT(0:NIOT),            NELI,        NIOT
C
C     Local variables
C
      INTEGER             EXLEVX,      I,           I0,          I1
      INTEGER             I2,          IREF,        NELIX,       REFMAX
C
      INCLUDE 'cfiles.inc'
      INCLUDE 'cref.inc'
C====>End Module   PREFI                  File cidrtms2.f               
c
      i2 = 0
c
100   continue
      refmax=min(prfdim/niot,nprfmx)
      write(nlist,*) 'primary reference specification.'
      write(nlist,*) 'references must satisfy requirements'
     &  //' from at least one primary reference.'
      write(nlist,6100)
     & 'maximum number of primary reference occupations, refmax',refmax
6100  format(/1x,a,':',4i8)
c
      write(nlist,*)
      npref=0
      call ini(npref,
     &  'input the number of primary reference occupations',*9000)
      write(nlist,6100)'number of reference occupations',npref
      if(npref.lt.0 .or. npref.gt.refmax)then
          write(nlist,*) '0 < npref <= refmax'
          go to 100
      endif
      if(npref.eq.0)return
c
      write(nout,*)'occupations are entered as 0=empty,'
     &  //' 1=singly_occupied, 2=doubly_occupied.'
c
200   continue
      write(nout,6100)'neli',neli
      i2=0
      do 240 iref=1,npref
          i1=i2+1
          i2=i2+niot
210       write(nlist,6100)
     &      'occupation specification for primary reference',iref
          call iset(niot,-1,procc(i1),1)
          call inivp(modrt(1),procc(i1),niot,'input procc(*)',*100)
          nelix=0
          do 220 i=i1,i2
              if(procc(i).lt.0 .or. procc(i).gt.2)then
                  write(nlist,*) '0 < procc(i) <= 2'
                  go to 210
              endif
              nelix=nelix+procc(i)
220       continue
          if(neli.ne.nelix)then
              write(nlist,6100)'neli,nelix',neli,nelix
              go to 210
          endif
          exlevx=-1
          call ini(exlevx,'input the excitation level from this primary'
     &      //' reference',*210)
          if(exlevx.lt.0)then
              write(nlist,*) '0 <= exlev'
              go to 210
          endif
          exlev(iref)=exlevx
240   continue
c
      i0=0
      do 320 iref=1,npref
          write(nout,6060)iref,exlev(iref),(procc(i0+i),i=1,niot)
          i0=i0+niot
320   continue
6060  format(' iref=',i3,' exlev=',i3,' occ(*)=',(1x,50i1))
c
      return
c
9000  return 1
c
      end

      subroutine prfchk( step, niot, qrejct )
c
c  compare a reference csf to the primary reference occupations.
c  the csf must satisfy at least one reference comparison to qualify.
c
       implicit none 
C====>Begin Module PRFCHK                 File cidrtms2.f               
C---->Makedcls Options: All variables                                   
C
C     Parameter variables
C
      INTEGER             NPRFMX
      PARAMETER           (NPRFMX = 256)
      INTEGER             PRFDIM
      PARAMETER           (PRFDIM = 1000)
C
C     Argument variables
C
      INTEGER             NIOT,        STEP(NIOT)
C
      LOGICAL             QREJCT
C
C     Local variables
C
      INTEGER             DIFF(0:2,0:3),            EX2,         EXR2
      INTEGER             I,           I0,          IREF
C
      INCLUDE 'cref.inc'
C====>End Module   PRFCHK                 File cidrtms2.f               
      data diff/0,1,2, 1,0,1, 1,0,1, 2,1,0/
c
      qrejct = .false.
      i0 = 0
      do 120 iref = 1, npref
          ex2  = 0
          exr2 = 2 * exlev(iref)
          do 110 i = 1, niot
              ex2 = ex2 + diff( procc(i0+i), step(i) )
110       continue
          if ( ex2 .le. exr2 ) return
          i0 = i0 + niot
120   continue
c
c     # ...on loop exit, maximum excitation level was exceeded for
c     #    all reference occupations.
c
      qrejct = .true.
c
      return
      end

      subroutine indiv( iwalk, isym, step, niot, qrejct, * )
c
c  individual csf selection.
c
       implicit none 
C====>Begin Module INDIV                  File cidrtms2.f               
C---->Makedcls Options: All variables                                   
C
C     Parameter variables
C
      INTEGER             YES
      PARAMETER           (YES = 1)
      INTEGER             NO
      PARAMETER           (NO = 0)
C
C     Argument variables
C
      INTEGER             ISYM,        IWALK,       NIOT
      INTEGER             STEP(NIOT)
C
      LOGICAL             QREJCT
C
C     Local variables
C
      INTEGER             CHAR
C
      INCLUDE 'cfiles.inc'
C====>End Module   INDIV                  File cidrtms2.f               
c
      write(nout,6010)iwalk,isym,step
6010  format(' walk:',i8,' isym=',i1,' step(*)=',(1x,50i1))
      char=yes
      call inyn(char,'keep?',*9000)
      qrejct=char.eq.no
c
      return
c
9000  return 1
c
      end
      subroutine exlimwnew(
     & niot,  nrow,   nsym,
     & nwalk, nref,   ssym,   qxlim,
     & exmax, l,      y,
     & xbar,  row,    ytot,   step,
     & wsym,  arcsym, rstep,  rdiff,
     & mult,  nexw,   ncsft,  ncsfsm,
     & nvalw, nviwsm, slabel,init, spnorb,
     & spnir, b, multmx,      spnodd)
       implicit none 
C====>Begin Module EXLIMWNEW              File cidrtms2.f               
C---->Makedcls Options: All variables                                   
C
C     Parameter variables
C
      INTEGER             NROWMX
      PARAMETER           (NROWMX = 2**10-1)
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
C
C     Argument variables
C
      CHARACTER*4         SLABEL(0:NSYM)
C
      INTEGER             ARCSYM(0:3,0:NIOT),       B(*),        EXMAX
      INTEGER             L(0:3,NROWMX,3),          MULT(8,8),   MULTMX
      INTEGER             NCSFSM(4,NSYM),           NCSFT
      INTEGER             NEXW(8,4),   NIOT,        NREF,        NROW
      INTEGER             NSYM,        NVALW(4),    NVIWSM(4,NSYM)
      INTEGER             NWALK,       RDIFF(NREF,0:NIOT)
      INTEGER             ROW(0:NIOT), RSTEP(NREF,NIOT)
      INTEGER             SPNIR(MULTMX,MULTMX),     SSYM
      INTEGER             STEP(0:NIOT),             WSYM(0:NIOT)
      INTEGER             XBAR(NROWMX,3),           Y(0:3,NROWMX,3)
      INTEGER             YTOT(0:NIOT)
C
      LOGICAL             INIT,        QXLIM,       SPNODD,      SPNORB
C
C     Local variables
C
      INTEGER             CLEV,        DIFF(0:3,0:3),            EXMAX2
      INTEGER             I,           IISTEP,      IMUL,        IREF
      INTEGER             IROW,        ISSYM,       ISTEP,       ISYM
      INTEGER             IVER,        IW,          IW1,         IW2
      INTEGER             IWALK,       IWRITTEN,    MUL,         NLEV
      INTEGER             NREM(4),     RDMIN,       ROWC,        ROWN
      INTEGER             VDIFF(4),    VER,         VER1,        VERS(4)
C
      INCLUDE 'cfiles.inc'
      INCLUDE 'special.inc'
C====>End Module   EXLIMWNEW              File cidrtms2.f               
c
c  perform excitation-level based walk selection.
c  internal walks with no external extensions due to symmetry
c  are also deleted.
c
c  input:
c  qxlim = .true. if excitation levels are to be computed.
c        = .false. if symmetry checking only is to be done.
c  rstep(*) = step vectors of the reference csfs.
c  rdiff(*) = workspace for walk occupation differences.  rstep(*)
c             and rdiff(*) are referenced only if qxlim=.true.
c
c  output:
c  ncsft = total number of csfs for this orbital basis and drt.
c  ncsfsm(1:4,1:nsym) = number of csfs through w,x,y,z vertices indexed
c                       by internal walk symmetry.
c  nvalw(1:4) = number of valid internal walks through each vertex.
c  nviwsm(1:4,1:nsym) = number of valid internal walks of each symmetry
c                       through each vertex.
c
c  07-jun-89 3-index drt version. -rls
c
ctm
c      0: corresponds to the 1-DRT
c      1: corresponds to the z-DRT
c      2: corresponds to the y-DRT
c      3: corresponds to the xw-DRT

      data diff/0,1,1,2, 1,0,0,1, 1,0,0,1, 2,1,1,0/
      data vdiff/0,1,2,2/
      data vers/1,2,3,3/
c
      write(nlist,'(/1x,a)')
     & 'exlimw: beginning excitation-based walk selection...'
c
c     # initialize all the walks as valid.
c
c
c     # nvalw(*)        = the number of valid walks for z,y,x,w.
c     # nrem(iver)      = the number of walks removed for vertex iver.
c
      call izero_wr( 4,      nrem,   1 )
      call izero_wr( 4*nsym, nviwsm, 1 )
c
      exmax2 = 2 * exmax
c
c     # construct all the internal walks.
c     # as the walks are constructed, accumulate the occupation
c     # differences in the array rdiff(*,*).  when a partial walk
c     # exceeds exmax2 for all the references, mark all the internal
c     # walks from the partial walk head as invalid.
c
ctm
      if (init) then
      call izero_wr(nrowmx*3,valrow,1)
      call izero_wr(nrowmx*3*4,valarc,1)
      endif
ctm

      do 50 i=0,niot
         step(i)=4
50    continue
c
      do 200 ver=1,4
c
         if(qxlim)then
            do 60 iref=1,nref
               rdiff(iref,0)=vdiff(ver)
60          continue
         endif
c
         ver1=vers(ver)
         clev=0
         row(0)=ver
         wsym(0)=1
         iwritten=0
100      continue
         nlev=clev+1
         istep=step(clev)-1
         if(istep.lt.0)then
c           # decrement the current level.
            step(clev)=4
            if(clev.eq.0)go to 180
            clev=clev-1
            iwritten=min(iwritten,clev)
            go to 100
         endif
         step(clev)=istep
         rowc=row(clev)
         rown=l(istep,rowc,ver1)
         if(rown.eq.0)go to 100

         row(nlev)=rown
         wsym(nlev)=mult(wsym(clev),arcsym(istep,clev))
c
c        # compare occupations with reference csfs.
c
         if(qxlim)then
            rdmin=exmax2+1
            do 110 iref=1,nref
               rdiff(iref,nlev)=rdiff(iref,clev)+
     &          diff(rstep(iref,nlev),istep)
               rdmin=min(rdiff(iref,nlev),rdmin)
110         continue
            if(rdmin.gt.exmax2) go to 100
         endif
c
         if(nlev.eq.niot)then
c           # complete internal walk has been generated.
c           # check for a valid external extension.
c           # a valid walk is either of correct symmetry
c           # or a valid reference (rdmin.eq.0)
            mul = 1
            spnir(1,1)=1
            iwalk = ytot(nlev) + 1
177         continue
            if(spnorb)mul = b(rown) + 1
            do 179 imul = 1, mul
               isym  = mult(wsym(nlev),spnir(imul, mul))
               issym = mult(isym,ssym)
               if ( nexw(issym,ver).ne.0
     &              .or. rdmin.eq.0) then
                  nviwsm(ver,isym) = nviwsm(ver,isym) + 1
                  do i = iwritten, nlev-1
                     irow = row(i)
                     iistep = step(i)
                     valrow(irow,ver1)=1
                     valarc(iistep,irow,ver1)=1
                  enddo
                  valrow(row(nlev),ver1)=1
                  iwritten=nlev
               endif
 179        continue
czzhang??            step(clev)=4
czzhang??            clev=clev-1
            iwritten=min(iwritten,clev)
            go to 100
         else
c           # increment to next level.
            clev=nlev
            go to 100
         endif
c
c        # finished with walk generation for this vertex.
180      continue
c     # next vertex.
200   continue
c
c     # compute nvalw(*)...
c
      do 220 iver = 1, 4
         nvalw(iver) = 0
         do 210 isym = 1, nsym
            nvalw(iver) = nvalw(iver) + nviwsm(iver,isym)
210      continue
220   continue
c
c     # compute and print csf statistics...
c
      call pscsf(
     & nsym,   slabel, ssym,   nviwsm,
     & mult,   nexw,   ncsft,  ncsfsm )
c
ctm
c     write(*,*) 'Number of internal walks = ', iwalk

      return
      end

      subroutine manlim(
     & niot,   nrow,   nrowmx, nsym,
     & nwalk,  ssym,   limvec, l,
     & y,      xbar,   arcsym, step,
     & mult,   nexw,   nvalw,  *  )
c
c  perform final manual mrsdci walk selection.
c
c  input:
c  niot,nrow,nrowmx = drt array dimensions.
c  nwalk = total number of walks.
c  ssym = state spatial symmetry.
c  limvec(*) = initial 0/1 limvec(*).
c  l(*),y(*),arcsym(*) = drt arrays.
c  step(*) = workspace for storing step-vectors.
c  mult(*) = irrep multiplication table.
c  nexw(*) = number of external walks of each symmetry for
c            the w,x,y,z-vertices.
c  nvalw(*) = initial number of valid walks for the w,x,y,z-vertices.
c
c  output:
c  limvec(*) = modified 0/1 limvec(*).
c  nvalw(*)= modified nvalw(*).
c
       implicit none 
C====>Begin Module MANLIM                 File cidrtms2.f               
C---->Makedcls Options: All variables                                   
C
C     Parameter variables
C
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
C
C     Argument variables
C
      INTEGER             ARCSYM(0:3,0:NIOT),       L(0:3,NROWMX,3)
      INTEGER             LIMVEC(NWALK),            MULT(8,8)
      INTEGER             NEXW(8,4),   NIOT,        NROW,        NROWMX
      INTEGER             NSYM,        NVALW(4),    NWALK,       SSYM
      INTEGER             STEP(0:NIOT),             XBAR(NROWMX,3)
      INTEGER             Y(0:3,NROWMX,3)
C
C     Local variables
C
      CHARACTER           WXYZ(4)
C
      INTEGER             I,           ISSYM,       ISYM,        IWALK
      INTEGER             NVALWT,      NWADD,       NWREM,       VER
      INTEGER             VERS(4),     VERS1,       W0(4)
C
      INCLUDE 'cfiles.inc'
C====>End Module   MANLIM                 File cidrtms2.f               
      data vers/1,2,3,3/
c
c     # compute walk offsets...
c
      w0(1) = 0
      w0(2) = xbar(1,1)
      w0(3) = xbar(2,2) + w0(2)
      w0(4) = xbar(3,3) + w0(3)
c
      write(nlist,6010)
      nvalwt=nvalw(1)+nvalw(2)+nvalw(3)+nvalw(4)
      write(nlist,6020)nvalw,nvalwt
      write(nout,6030)
c
      nwadd=0
      nwrem=0
100   continue
      iwalk=0
      call ini(iwalk,'input mrsdci walk number (0 to end)',*9000)
c
      if(iwalk.lt.0)then
c        # delete walk...
         iwalk=-iwalk
         if(iwalk.gt.nwalk)then
            write(nlist,6100)'walk too big',iwalk
         else if(limvec(iwalk).eq.0)then
            write(nlist,6100)'walk already deleted',iwalk
         else
            if(iwalk.gt.w0(3))then
               if(iwalk.gt.w0(4))then
                  ver=4
               else
                  ver=3
               endif
            else
               if(iwalk.gt.w0(2))then
                  ver=2
               else
                  ver=1
               endif
            endif
            nwrem=nwrem+1
            nvalw(ver)=nvalw(ver)-1
            vers1=vers(ver)
            call gstepc( iwalk, ver, niot, l(0,1,vers1),
     &       y(0,1,vers1), arcsym, mult, step, isym )
            limvec(iwalk)=0
            write(nlist,6040)'removed:',wxyz(ver),iwalk,isym,
     &       (step(i),i=1,(niot-1))
         endif
         go to 100
      elseif(iwalk.gt.0)then
c        # add walk...
         if(iwalk.gt.nwalk)then
            write(nlist,6100)'walk too big',iwalk
         else if(limvec(iwalk).eq.1)then
            write(nlist,6100)'walk already added',iwalk
         else
            if(iwalk.gt.w0(3))then
               if(iwalk.gt.w0(4))then
                  ver=4
               else
                  ver=3
               endif
            else
               if(iwalk.gt.w0(2))then
                  ver=2
               else
                  ver=1
               endif
            endif
               vers1=vers(ver)
               call gstepc( iwalk, ver, niot, l(0,1,vers1),
     &          y(0,1,vers1), arcsym, mult, step, isym )
               issym=mult(isym,ssym)
            if(nexw(issym,ver).eq.0)then
               write(nlist,6100)'no external extension. iwalk,isym',
     &          iwalk,isym
            else
               nwadd=nwadd+1
               nvalw(ver)=nvalw(ver)+1
               limvec(iwalk)=1
               write(nlist,6040)'added:',wxyz(ver),iwalk,isym,
     &          (step(i),i=1,(niot-1))
            endif
         endif
         go to 100
      endif
c
c     # finished.
c
      write(nlist,6050)nwadd,nwrem
      nvalwt=nvalw(1)+nvalw(2)+nvalw(3)+nvalw(4)
      write(nlist,6020)nvalw,nvalwt

      return
c
9000  return 1
c
6010  format(/' final mrsdci walk selection step:')
6020  format(/' nvalw(*)=',4i8,' nvalwt=',i8/)
6030  format(' enter positive',
     & ' walk numbers to add walks,'
     & /' negative walk numbers to delete walks, and zero to end.'/)
6040  format(1x,a,t9,a1,'-walk=',i8,' isym=',i1,
     & ' step(*)=',(t40,50i1))
6050  format(/' end of manual mrsdci walk selection.'/
     & ' number added=',i4,' number removed=',i4)
6100  format(1x,a,': ',(3i8))
      end
      subroutine scref(
     & niot,   nrow,   nrowmx,  nref,
     & l,      y,      xbar,    ref,
     & ytot,   step,   row,     rstep,
     & spnorb, b,      spnodd  )
       implicit none 
C====>Begin Module SCREF                  File cidrtms2.f               
C---->Makedcls Options: All variables                                   
C
C     Parameter variables
C
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
      INTEGER             TOSKP
      PARAMETER           (TOSKP = 0)
      INTEGER             TO01
      PARAMETER           (TO01 = 1)
C
C     Argument variables
C
      INTEGER             B(*),        L(0:3,NROWMX),            NIOT
      INTEGER             NREF,        NROW,        NROWMX,      REF(*)
      INTEGER             ROW(0:NIOT), RSTEP(NREF,NIOT)
      INTEGER             STEP(0:NIOT),             XBAR(NROW)
      INTEGER             Y(0:3,NROW), YTOT(0:NIOT)
C
      LOGICAL             SPNODD,      SPNORB
C
C     Local variables
C
      INTEGER             CLEV,        I,           IMUL,        IREF
      INTEGER             ISTEP,       MUL,         NLEV,        NUMV1
      INTEGER             ROWC,        ROWN
C
      INCLUDE 'cfiles.inc'
C====>End Module   SCREF                  File cidrtms2.f               
c
c  save the step vectors for the reference csfs.
c
c
c     # change ref(*) to skip vector form.
c
      call skpx01( xbar(1), ref, toskp, numv1 )
c
c     # generate all the reference csfs.
c
      do 50 i=0,niot
         step(i) = 4
50    continue
c
      iref    = 0
      clev    = 0
      row(0)  = 1
      ytot(0) = 0
c
100   continue
      nlev = clev + 1
      istep = step(clev) - 1
      if ( istep .lt. 0 ) then
c        # decrement the current level, and check if done.
         step(clev) = 4
         if ( clev .eq. 0 ) go to 180
         clev = clev - 1
         go to 100
      endif
      step(clev) = istep
      rowc = row(clev)
      rown = l(istep,rowc)
      if(rown.eq.0)go to 100
      row(nlev) = rown
      ytot(nlev) = ytot(clev)+y(istep,rowc)
      if(ref(ytot(nlev)+1) .ge. xbar(rown) )go to 100
c
      if(nlev.lt.niot)then
c        # increment to next level.
         clev = nlev
         go to 100
      else
c        # reference csf.
         mul = 1
         if(spnorb) mul = b(rown) + 1
         do 119 imul = 1, mul
            iref = iref + 1
            do 120 i = 1, niot
               rstep(iref,i) = step(i-1)
 120        continue
            write(nlist,6010)iref,(step(i),i=0,(niot-1))
119      continue
czzhang?         step(clev) = 4
czzhang?         clev = clev - 1
         go to 100
      endif
c
c     # finished with walk generation.
180   continue
c
      if(iref.ne.nref)then
          write(nlist,6100)'iref,nref',iref,nref
          call bummer('scref: iref=',iref,faterr)
      endif
      write(nout,6100)'number of step vectors saved',iref
c
*@ifdef debug
*      do 190 iref=1,nref
*         write(nout,6010)iref,(rstep(iref,i),i=1,niot)
*190   continue
*@endif
c
c     # change ref(*) back to 0/1 form.
c
      call skpx01( xbar(1), ref, to01, numv1 )
c
      return
c
6010  format(1x,i6,':',(1x,80i1))
6100  format(1x,a,': ',(6i6))
      end

      subroutine lcopy( nrow, nrowmx, y, xbar, l )
c
c  make three identical copies of the drt chaining arrays.
c
       implicit none 
C====>Begin Module LCOPY                  File cidrtms2.f               
C---->Makedcls Options: All variables                                   
C
C     Argument variables
C
      INTEGER             L(0:3,NROWMX,3),          NROW,        NROWMX
      INTEGER             XBAR(NROWMX,3),           Y(0:3,NROWMX,3)
C
C     Local variables
C
      INTEGER             I,           ILEV,        VER
C
C====>End Module   LCOPY                  File cidrtms2.f               
c
      do 100 ver = 2, 3
         do 110 ilev = 1, nrow
            xbar(ilev,ver) = xbar(ilev,1)
            do 120 i = 0, 3
               l(i,ilev,ver) = l(i,ilev,1)
               y(i,ilev,ver) = y(i,ilev,1)
120         continue
110      continue
100   continue
c
      return
      end

      subroutine limintnew( limvec, ref, nvalw ,ndrts,joined)
c
c  this routine  determines the limvec(*) by constructing
c  the set of reference-space-interacting loops.
c  limint and the related subroutines are a modified copy of
c  the program ciuft.
c
c  24-nov-90 /cwsym/,/cstack/,/cdb/,/cst/,/cdrt/ added.  corresponding
c            variables were removed from the argument lists. this is to
c            restore some symmetry between this program and ciuft. -rls
c  06-jun-89 nexw(*) and symmetry selection added. -rls
c  05-jun-89 limvec(*)-based version written. efficiency is improved
c            by using skip-vector form of ref(*). -rls
c  10-jan-88 ln(*,*,*)-based version written. -hl/pz
c
       implicit none 
C====>Begin Module LIMINTNEW              File cidrtms2.f               
C---->Makedcls Options: All variables                                   
C
C     Parameter variables
C
      INTEGER             NROWMX
      PARAMETER           (NROWMX = 2**10-1)
      INTEGER             NIMOMX
      PARAMETER           (NIMOMX = 128)
      INTEGER             NMXDRT
      PARAMETER           (NMXDRT = 8)
      INTEGER             TMINIT
      PARAMETER           (TMINIT = 1)
      INTEGER             TMPRT
      PARAMETER           (TMPRT = 2)
      INTEGER             TMREIN
      PARAMETER           (TMREIN = 3)
      INTEGER             TMCLR
      PARAMETER           (TMCLR = 4)
      INTEGER             TMSUSP
      PARAMETER           (TMSUSP = 5)
      INTEGER             TMRESM
      PARAMETER           (TMRESM = 6)
      INTEGER             TOSKP
      PARAMETER           (TOSKP = 0)
      INTEGER             TO01
      PARAMETER           (TO01 = 1)
C
C     Argument variables
C
      INTEGER             LIMVEC(*),   NDRTS,       NVALW(4),    REF(*)
C
      LOGICAL             JOINED
C
C     Local variables
C
      INTEGER             CNT,         I,           ITIME,       ND
      INTEGER             NUMV1,       NUMW,        VER,         VER1
      INTEGER             W1,          W2
C
      INCLUDE 'cdrt.inc'
      INCLUDE 'cfiles.inc'
C====>End Module   LIMINTNEW              File cidrtms2.f               
c
      call timer( ' ', tminit, itime, nlist )
c
c     # initialize segment value arrays.
c
      call intab
c
c     # initialize necessary stack arrays.
c
      call istack
c
c     # change ref(*) to skip vector form.
c
c
c     # initialize limvec(*) = 0
c     # interacting walks are reset as they are determined.
c
      numw = xbar(1,1) + xbar(2,2) + xbar(3,3) + xbar(4,3)
       call izero_wr(numw,limvec,1)
c
c     # diagonal selection.  this is equivalent to picking out reference
c     # csfs of the correct symmetry.
c
       cnt=0
      do i=1,xbar(1,1)
       if (ref(i).eq.1) cnt=cnt+1
      enddo
      write(*,*) 'limintnew: ', cnt ,'references found.'
      call skpx01( xbar(1,1), ref, toskp, numv1 )

      if (joined ) then
       do nd=1,ndrts
      write(nlist,*) 'joined interacting space determination:'
      write(nlist,*) 'checking diagonal loops... for drt',nd
      call intd( limvec, ref ,nd)
c
c     # construct the 2-internal loops...
c
      write(nlist,*) 'checking 2-internal loops...'
      call intern2( limvec, ref,nd )
c
c     # construct the 3-internal loops...
c
      write(nlist,*) 'checking 3-internal loops...'
      call intern3( limvec, ref,nd )
c
c     # construct the 4-internal loops...
c
      write(nlist,*) 'checking 4-internal loops...'
      call intern4( limvec, ref,nd )
       enddo
      else
      write(nlist,*) 'interacting space determination:'
      write(nlist,*) 'checking diagonal loops...'
      call intd( limvec, ref ,ndrts)
c
c     # construct the 2-internal loops...
c
      write(nlist,*) 'checking 2-internal loops...'
      call intern2( limvec, ref,ndrts )
c
c     # construct the 3-internal loops...
c
      write(nlist,*) 'checking 3-internal loops...'
      call intern3( limvec, ref,ndrts )
c
c     # construct the 4-internal loops...
c
      write(nlist,*) 'checking 4-internal loops...'
      call intern4( limvec, ref,ndrts )
       endif
c
c     # change ref(*) back to 0/1 form...
c
      call skpx01( xbar(1,1), ref, to01, numv1 )
c
c     # compute the number of valid walks for each z,y,x,w-vertex.
c     # limvec(*) has been computed in 0/1 form.
c
      w2 = 0
      do 20 ver = 1, 4
         nvalw(ver) = 0
         ver1 = min(ver,3)
         w1 = w2 + 1
         w2 = w2 + xbar(ver,ver1)
         do 10 i = w1, w2
            nvalw(ver) = nvalw(ver) + limvec(i)
10       continue
20    continue
c
      call timer( 'limint() required', tmclr, itime, nlist )
c
      return
      end

      subroutine newl(
     & nlist,  niot,
     & nrow,  y,
     & l,      xbar,  step,   ytot,
     & yntot,  row,   lnew )
c
c  input:
c  niot = total number of internal orbitals.
c  nrow = number of distinct rows in the drt arrays.
c  nrowmx = maximum number of rows.
c  y(*),xbar(*),l(*) = current drt chaining indices.
c  ytot(*),step(*),yntot(*),row(*) = temp walk generation arrays.
c
c  output:
c  lnew(*) = new chaining array.
c
c  31-jul-89 ref(*) checks added for z-walks. -rls
c  05-jun-89 written by ron shepard.
c
       implicit none 
C====>Begin Module NEWL                   File cidrtms2.f               
C---->Makedcls Options: All variables                                   
C
C     Parameter variables
C
      INTEGER             NROWMX
      PARAMETER           (NROWMX = 2**10-1)
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
      INTEGER             TOSKP
      PARAMETER           (TOSKP = 0)
      INTEGER             TO01
      PARAMETER           (TO01 = 1)
C
C     Argument variables
C
      INTEGER             L(0:3,NROWMX,3),          LNEW(0:3,NROWMX,3)
      INTEGER             NIOT,        NLIST,       NROW
      INTEGER             ROW(0:NIOT), STEP(0:NIOT)
      INTEGER             XBAR(NROWMX,3),           Y(0:3,NROWMX,3)
      INTEGER             YNTOT(0:NIOT),            YTOT(0:NIOT)
C
C     Local variables
C
      CHARACTER*2         CDRT(3)
C
      INTEGER             CLEV,        I,           IROW,        ISTEP
      INTEGER             NLEV,        NPRUNE,      NREF,        NUMV1
      INTEGER             NVALWT,      NWALK,       ROWC,        ROWN
      INTEGER             VER,         VER1
C
      INCLUDE 'special.inc'
C====>End Module   NEWL                   File cidrtms2.f               
      data cdrt/' z',' y','wx'/
c
      rown = 0
      numv1 = 0
      rowc = 0
c
c     # initialize lnew(*)...
      call izero_wr(12*nrowmx,lnew,1)
c
c     # loop over z-, y-, x-, and w-vertex walks...
c
      nvalwt = 0
      do 200 ver = 1,4
c
c        # begin walk generation for this vertex...
c
         ver1 = min(3,ver)
         clev = 0
         row(0) = ver
         ytot(0) = 0
c
         do 20 i = 0,niot
            step(i) = 4
20       continue
c
100      continue
c        # decrement the step at the current level.
         nlev = clev+1
         istep = step(clev)-1
         if(istep.lt.0)then
c           # decrement the current level.
            step(clev) = 4
            if(clev.eq.0)goto 200
            clev = clev-1
            go to 100
         endif
         step(clev) = istep
         rowc = row(clev)
         rown = l(istep,rowc,ver1)
         if(rown.eq.0)go to 100
         ytot(nlev) = ytot(clev)+y(istep,rowc,ver1)
         if (valrow(rowc,ver1).eq.0) goto 100
         if (valarc(istep,rowc,ver1).eq.0) goto 100
         lnew(istep,rowc,ver1) = rown
         row(nlev) = rown
         if(nlev.lt.niot)then
c           # increment to the next level.
            clev = nlev
            go to 100
         else
czzhang??            step(clev) = 4
czzhang??            clev = clev-1
            go to 100
         endif
200   continue
c
      return
      end


      subroutine exlimw(
     & niot,  nrow,   nrowmx, nsym,
     & nwalk, nref,   ssym,   qxlim,
     & exmax, limvec, l,      y,
     & xbar,  row,    ytot,   step,
     & wsym,  arcsym, rstep,  rdiff,
     & mult,  nexw,   ncsft,  ncsfsm,
     & nvalw, nviwsm, slabel, spnorb,
     & spnir, b,      multmx, spnodd )
       implicit none 
C====>Begin Module EXLIMW                 File cidrtms2.f               
C---->Makedcls Options: All variables                                   
C
C     Parameter variables
C
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
C
C     Argument variables
C
      CHARACTER*4         SLABEL(0:NSYM)
C
      INTEGER             ARCSYM(0:3,0:NIOT),       B(*),        EXMAX
      INTEGER             L(0:3,NROWMX,3),          LIMVEC(NWALK)
      INTEGER             MULT(8,8),   MULTMX,      NCSFSM(4,NSYM)
      INTEGER             NCSFT,       NEXW(8,4),   NIOT,        NREF
      INTEGER             NROW,        NROWMX,      NSYM
      INTEGER             NVALW(4),    NVIWSM(4,NSYM),           NWALK
      INTEGER             RDIFF(NREF,0:NIOT),       ROW(0:NIOT)
      INTEGER             RSTEP(NREF,NIOT),         SPNIR(MULTMX,MULTMX)
      INTEGER             SSYM,        STEP(0:NIOT)
      INTEGER             WSYM(0:NIOT),             XBAR(NROWMX,3)
      INTEGER             Y(0:3,NROWMX,3),          YTOT(0:NIOT)
C
      LOGICAL             QXLIM,       SPNODD,      SPNORB
C
C     Local variables
C
      INTEGER             CLEV,        DIFF(0:3,0:3),            EXMAX2
      INTEGER             I,           IMUL,        IREF,        ISSYM
      INTEGER             ISTEP,       ISYM,        IVER,        IW
      INTEGER             IW1,         IW2,         IWALK,       MUL
      INTEGER             NLEV,        NOOFWALKS,   NREM(4),     RDMIN
      INTEGER             ROWC,        ROWN,        VDIFF(4),    VER
      INTEGER             VER1,        VERS(4)
C
      INCLUDE 'cfiles.inc'
C====>End Module   EXLIMW                 File cidrtms2.f               
c
c  perform excitation-level based walk selection.
c  internal walks with no external extensions due to symmetry
c  are also deleted.
c
c  input:
c  qxlim = .true. if excitation levels are to be computed.
c        = .false. if symmetry checking only is to be done.
c  rstep(*) = step vectors of the reference csfs.
c  rdiff(*) = workspace for walk occupation differences.  rstep(*)
c             and rdiff(*) are referenced only if qxlim=.true.
c
c  output:
c  ncsft = total number of csfs for this orbital basis and drt.
c  ncsfsm(1:4,1:nsym) = number of csfs through w,x,y,z vertices indexed
c                       by internal walk symmetry.
c  nvalw(1:4) = number of valid internal walks through each vertex.
c  nviwsm(1:4,1:nsym) = number of valid internal walks of each symmetry
c                       through each vertex.
c
c  07-jun-89 3-index drt version. -rls
c
c
      data diff/0,1,1,2, 1,0,0,1, 1,0,0,1, 2,1,1,0/
      data vdiff/0,1,2,2/
      data vers/1,2,3,3/
c
      write(nlist,'(/1x,a)')
     & 'exlimw: beginning excitation-based walk selection...'
c
c     # initialize all the walks as valid.
c
       noofwalks=0
      call iset(nwalk,1,limvec,1)
c     write(*,*) 'Anfang limvec(*)'
c     write(*,'(20i3)') (limvec(i),i=1,100)
c
c     # nvalw(*)        = the number of valid walks for z,y,x,w.
c     # nrem(iver)      = the number of walks removed for vertex iver.
c
      call izero_wr( 4,      nrem,   1 )
      call izero_wr( 4*nsym, nviwsm, 1 )
c
      exmax2 = 2 * exmax
c
c     # construct all the internal walks.
c     # as the walks are constructed, accumulate the occupation
c     # differences in the array rdiff(*,*).  when a partial walk
c     # exceeds exmax2 for all the references, mark all the internal
c     # walks from the partial walk head as invalid.
c
      do 50 i=0,niot
         step(i)=4
50    continue
c
      do 200 ver=1,4
c
         if(qxlim)then
            do 60 iref=1,nref
               rdiff(iref,0)=vdiff(ver)
60          continue
         endif
c
         ver1=vers(ver)
         clev=0
         row(0)=ver
         ytot(0)=0
         wsym(0)=1
cok   write(*,*) 'limvec(*), vertex',ver1
cok   write(*,'(20i3)') (limvec(i),i=1,100)
100      continue
         nlev=clev+1
         istep=step(clev)-1
         if(istep.lt.0)then
c           # decrement the current level.
            step(clev)=4
            if(clev.eq.0)go to 180
            clev=clev-1
            go to 100
         endif
         step(clev)=istep
         rowc=row(clev)
         rown=l(istep,rowc,ver1)
c     write(*,*) 'limvec(*), clev',clev,' istep=',istep,'rown=',rown
c     write(*,'(20i3)') (limvec(i),i=1,20)
         if(rown.eq.0)go to 100
         row(nlev)=rown
         ytot(nlev)=ytot(clev)+y(istep,rowc,ver1)
         wsym(nlev)=mult(wsym(clev),arcsym(istep,clev))
c
c        # compare occupations with reference csfs.
c
         if(qxlim)then
            rdmin=exmax2+1
            do 110 iref=1,nref
               rdiff(iref,nlev)=rdiff(iref,clev)+
     &          diff(rstep(iref,nlev),istep)
c             write(*,*) 'rdiff(iref,nlev)=',rdiff(iref,nlev)
c             write(*,*) 'rstep(iref,nlev)=',rstep(iref,nlev)
c             write(*,*) 'diff(rstep(iref,nlev),istep)=',
c    .              diff(rstep(iref,nlev),istep)
               rdmin=min(rdiff(iref,nlev),rdmin)
110         continue
            if(rdmin.gt.exmax2)then
c              # mark all internal walks from the next row as invalid.
               iw1=ytot(nlev)+1
               iw2=ytot(nlev)+xbar(rown,ver1)
               do 120 iw=iw1,iw2
                  nrem(ver)=nrem(ver)+limvec(iw)
c                 write(*,*) 'eliminate iwalk=',iw
                  limvec(iw)=0
120            continue
               go to 100
            endif
         endif
c
         if(nlev.eq.niot)then
c           # complete internal walk has been generated.
c           # check for a valid external extension.
            mul = 1
            spnir(1,1)=1
            iwalk = ytot(nlev) + 1
            if( spnorb )mul = b(rown) + 1
            do 179 imul = 1, mul
               isym  = mult(wsym(nlev), spnir(imul, mul))
               issym = mult(isym,ssym)
c               write(nlist,6020)imul,mul,(step(i),i=0,(niot-1))
c 6020          format(1x,2i8,(t20,50i1))
               if ( nexw(issym,ver) .ne. 0 ) then
                  nviwsm(ver,isym) = nviwsm(ver,isym) + 1
                  noofwalks=noofwalks+1
               else
                  nrem(ver) = nrem(ver) + limvec(iwalk)
                  limvec(iwalk) = 0
               endif
               iwalk = iwalk + 1
179         continue
            goto 100
         else
c           # increment to next level.
            clev=nlev
            go to 100
         endif
c
c        # finished with walk generation for this vertex.
180      continue
c     # next vertex.
c     write(*,*) 'limvec(*), vertex',ver1
c     write(*,*) 'noofwalks=',noofwalks
c     write(*,'(20i3)') (limvec(i),i=1,100)
200   continue
c
c     # compute nvalw(*)...
c
      do 220 iver = 1, 4
         nvalw(iver) = 0
         do 210 isym = 1, nsym
            nvalw(iver) = nvalw(iver) + nviwsm(iver,isym)
210      continue
220   continue
c
c     # compute and print csf statistics...
c
      call pscsf(
     & nsym,   slabel, ssym,   nviwsm,
     & mult,   nexw,   ncsft,  ncsfsm )

c
      return
      end

      subroutine lprune(
     & nlist,  niot,  nwalk,  limvec,
     & ref,    nrow,  nrowmx, y,
     & l,      xbar,  step,   ytot,
     & yntot,  row,   lnew,  spnorb,
     & b,      spnodd )
       implicit none 
C====>Begin Module LPRUNE                 File cidrtms2.f               
C---->Makedcls Options: All variables                                   
C
C     Parameter variables
C
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
      INTEGER             TOSKP
      PARAMETER           (TOSKP = 0)
      INTEGER             TO01
      PARAMETER           (TO01 = 1)
C
C     Argument variables
C
      INTEGER             B(*),        L(0:3,NROWMX,3)
      INTEGER             LIMVEC(NWALK),            LNEW(0:3,NROWMX,3)
      INTEGER             NIOT,        NLIST,       NROW,        NROWMX
      INTEGER             NWALK,       REF(*),      ROW(0:NIOT)
      INTEGER             STEP(0:NIOT),             XBAR(NROWMX,3)
      INTEGER             Y(0:3,NROWMX,3),          YNTOT(0:NIOT)
      INTEGER             YTOT(0:NIOT)
C
      LOGICAL             SPNODD,      SPNORB
C
C     Local variables
C
      CHARACTER*2         CDRT(3)
C
      INTEGER             CLEV,        I,           IMUL,        IROW
      INTEGER             ISTEP,       IWALK,       MUL,         NLEV
      INTEGER             NPRUNE,      NREF,        NUMV1,       NVALWT
      INTEGER             ROWC,        ROWN,        VER,         VER1
C
C====>End Module   LPRUNE                 File cidrtms2.f               
c
c  prune l(*,*,*) according to limvec(*) so that it results in
c  the smallest drt and the most compact indexing scheme.
c
c  input:
c  niot = total number of internal orbitals.
c  nwalk = total number of walks using the current indexing scheme.
c  limvec(1:nwalk) = 0/1 representation (current indexing scheme).
c  ref(1:xbar(1,1)) = 0/1 representation (current indexing scheme).
c  nrow = number of distinct rows in the drt arrays.
c  nrowmx = maximum number of rows.
c  y(*),xbar(*),l(*) = current drt chaining indices.
c  ytot(*),step(*),yntot(*),row(*) = temp walk generation arrays.
c
c  output:
c  lnew(*) = pruned chaining array.
c
c  31-jul-89 ref(*) checks added for z-walks. -rls
c  05-jun-89 written by ron shepard.
c
      data cdrt/' z',' y','wx'/
c
      rown = 0
      numv1 = 0
      rowc = 0
c
c     # initialize lnew(*)...
      call izero_wr(12*nrowmx,lnew,1)
c
c     # change limvec(*) and ref(*) to skip vector form.
c
      call skpx01( nwalk,     limvec, toskp, numv1 )
      call skpx01( xbar(1,1), ref,    toskp, nref  )
c
c     # loop over z-, y-, x-, and w-vertex walks...
c
      nvalwt = 0
      do 200 ver = 1,4
c
c        # begin walk generation for this vertex...
c
         ver1 = min(3,ver)
         clev = 0
         row(0) = ver
         ytot(0) = 0
c
         do 20 i = 0,niot
            step(i) = 4
20       continue
c
100      continue
c        # decrement the step at the current level.
         nlev = clev+1
         istep = step(clev)-1
         if(istep.lt.0)then
c           # decrement the current level.
            step(clev) = 4
            if(clev.eq.0)goto 200
            clev = clev-1
            go to 100
         endif
         step(clev) = istep
         rowc = row(clev)
         rown = l(istep,rowc,ver1)
         if(rown.eq.0)go to 100
         ytot(nlev) = ytot(clev)+y(istep,rowc,ver1)
         if(ver.eq.1)then
c           # z-walk; check both limvec(*) and ref(*).
            if( ( limvec(ytot(nlev)+1) .ge. xbar(rown,ver1) )
     &       .and. ( ref(ytot(nlev)+1) .ge. xbar(rown,ver1) ))go to 100
         else
c           # w,x, or y walk.  just check limvec(*).
            if( limvec(ytot(nlev)+1) .ge. xbar(rown,ver1) )go to 100
         endif
c        # this step is used by at least one valid or reference walk.
         lnew(istep,rowc,ver1) = rown
         row(nlev) = rown
         if(nlev.lt.niot)then
c           # increment to the next level.
            clev = nlev
            go to 100
         else
c
            mul = 1
            iwalk = ytot(nlev) + 1
            if( spnorb )mul = b(rown) + 1
            do 179 imul = 1, mul
               if(limvec(iwalk) .eq. 0 )then
c                 # complete valid walk has been generated.
                  nvalwt = nvalwt+1
               endif
               iwalk = iwalk + 1
179         continue
            go to 100
         endif
c
200   continue
c
      write(nlist,'(/1x,a,i8,a,i8)')
     & 'lprune: l(*,*,*) pruned with nwalk=',nwalk,' nvalwt=',nvalwt
c
c     # compute the number of pruned arcs.
      do 230 ver1 = 1,3
         nprune = 0
         do 220 irow = 1,nrow
            do 210 istep = 0,3
               if(lnew(istep,irow,ver1).ne.l(istep,irow,ver1))
     &          nprune = nprune+1
210         continue
220      continue
         write(nlist,'(1x,a,i6)') 'lprune: ' // cdrt(ver1) //
     &    '-drt, nprune=', nprune
230   continue
c
c     # change limvec(*) and ref(*) back to 0/1 form...
c
      call skpx01( nwalk, limvec, to01, numv1 )
      if(numv1.ne.nvalwt)call bummer('lprune: nvalwt-numv1=',
     & (nvalwt-numv1),faterr)
      call skpx01( xbar(1,1), ref, to01, nref )
c
      return
      end

      logical function vfail(
     & niot,   nel,    smult,   ai,
     & bi,     ilev,   occmin,  occmax,
     & bmin,   bmax,   nrem,    remove,
     & ndot,   qgis,   spnorb,  multp,
     & nmul,   spnodd   )
       implicit none 
C====>Begin Module VFAIL                  File cidrtms2.f               
C---->Makedcls Options: All variables                                   
C
C     Argument variables
C
      INTEGER             AI,          BI,          BMAX(*),     BMIN(*)
      INTEGER             ILEV,        MULTP(NMUL), NDOT,        NEL
      INTEGER             NIOT,        NMUL,        NREM
      INTEGER             OCCMAX(*),   OCCMIN(*),   REMOVE(3,*), SMULT
C
      LOGICAL             QGIS,        SPNODD,      SPNORB
C
C     Local variables
C
      INTEGER             BLEFT,       IBLEFT,      ILEFT,       IMULT
      INTEGER             IREM,        NELEFT,      OCC
C
C====>End Module   VFAIL                  File cidrtms2.f               
c
c  function vfail determines if a possible vertex is allowed.
c  .true.  indicates that the vertex is not allowed (i.e. fails).
c  .false. indicates that the vertex is acceptable.
c
      vfail = .true.
c     # check b range.
c     occ = 2*ai+bi
c     write(*,*) 'bcheck:',bi,bmin(ilev),bmax(ilev),
c    .           'occchk:',occ,occmin(ilev),occmax(ilev)
      if(bi.lt.bmin(ilev))return
      if(bi.gt.bmax(ilev))return
c     # check occupation range.
      occ = 2*ai+bi
      if(occ .lt. occmin(ilev)) return
      if(occ .gt. occmax(ilev)) return
      neleft = nel-occ
c
      ileft = niot-ilev
      if(spnorb)then
         do 200 imult=1, smult, 2
            bleft = (imult-1) -bi
             ibleft = abs(bleft)
c     # see if enough electrons are left to go to this multiplicity,
             if(neleft.lt.ibleft)goto 200
c     # enough electrons are left, see if enough orbital are left
             if(2*ileft-neleft .lt. ibleft)then
                goto 200
              else
c     # valid vertex by spin coupling consideration
                goto 99
              endif
 200      continue
         return
      else
         bleft = (smult-1) -bi
         ibleft = abs(bleft)
c     # check if enough electrons are left to get to smult.
         if(neleft .lt. ibleft) return
         ileft = niot-ilev
c     # check if enough orbitals are left to get to smult.
         if(2*ileft-neleft .lt. ibleft)return
      endif
c
 99   continue
c
c
c     # check against the list of manually-specified vertices.
      do 100 irem = 1,nrem
          if(ilev.ne.remove(1,irem))go to 100
          if(ai.ne.remove(2,irem))go to 100
          if(bi.ne.remove(3,irem))go to 100
c         # match found.
          return
100   continue
c
c     # remove delta_b>2 and delta_n>2 vertices.
c      write(*,*) 'qgis:',qgis,ilev.le.ndot,
c    .    abs(occ-2*ilev).gt.2, bi.gt.2
      if ( qgis .and. (ilev .le. ndot) .and.
     &  (abs(occ-2*ilev).gt.2  .or. bi.gt.2) )return
c
c     # passed all the tests, this vertex should be included.
      vfail =  .false.
c
      return
      end

        subroutine joinref(ref,joined,n,init)
       implicit none 
C====>Begin Module JOINREF                File cidrtms2.f               
C---->Makedcls Options: All variables                                   
C
C     External functions
C
      EXTERNAL            GETREF
C
      INTEGER             GETREF
C
C     Parameter variables
C
*@ifdef int64
      INTEGER             WORDLEN
      PARAMETER           (WORDLEN = 64)
*@else
*      INTEGER             WORDLEN
*      PARAMETER           (WORDLEN = 32)
*@endif
C
C     Argument variables
C
      INTEGER             JOINED(*),   N,           REF(*)
C
      LOGICAL             INIT
C
C     Local variables
C
      INTEGER             I
C
C====>End Module   JOINREF                File cidrtms2.f               

        if (init) call izero_wr((n-1)/wordlen+1,joined,1)

        do i=1,n
          if (getref(ref,i).eq.1) joined(i)=1
        enddo
        return
        end

        subroutine wcomm(icode,idrt)
c       icode = 1  start   2 end   3 all
       implicit none 
C====>Begin Module WCOMM                  File cidrtms2.f               
C---->Makedcls Options: All variables                                   
C
C     Argument variables
C
      INTEGER             ICODE,       IDRT
C
C====>End Module   WCOMM                  File cidrtms2.f               


 200    format(10('=='),' START DRT# ',i3,10('=='))
 201    format(10('**'),' END   DRT# ',i3,10('**'))
 202    format(10('**'),' ALL   DRTs ','   ',10('**'))

        if (icode.eq.1) write(*,200) idrt
        if (icode.eq.2) write(*,201) idrt
        if (icode.eq.3) write(*,202)
        return
        end

        subroutine printadr(n,iadr)
       implicit none 
C====>Begin Module PRINTADR               File cidrtms2.f               
C---->Makedcls Options: All variables                                   
C
C     Argument variables
C
      INTEGER             IADR(N),     N
C
C     Local variables
C
      INTEGER             I
C
C====>End Module   PRINTADR               File cidrtms2.f               

        write(*,199)
        write(*,200)
        write(*,201)(iadr(i),i=1,n)
        write(*,199)
 199    format(18('----'))
 200    format('Core address array: ')
 201    format(5(2x,5(i12,2x)/))
        return
        end

        integer function join ( step,jstep)
       implicit none 
C====>Begin Module JOIN                   File cidrtms2.f               
C---->Makedcls Options: All variables                                   
C
C     Argument variables
C
      INTEGER             JSTEP,       STEP
C
C     Local variables
C
      INTEGER             BIT1,        BIT2,        BIT3,        BIT4
      INTEGER             JBIT1,       JBIT2,       JBIT3,       JBIT4
      INTEGER             TMP
C
C====>End Module   JOIN                   File cidrtms2.f               

        bit1= step / 1000
        tmp = mod (step,1000)
        bit2 = tmp / 100
        tmp = mod (tmp,100)
        bit3 = tmp /10
        bit4 = mod (tmp,10)

        jbit1= jstep / 1000
        tmp = mod (jstep,1000)
        jbit2 = tmp / 100
        tmp = mod (tmp,100)
        jbit3 = tmp /10
        jbit4 = mod (tmp,10)

        jbit1=max(bit1,jbit1)
        jbit2=max(bit2,jbit2)
        jbit3=max(bit3,jbit3)
        jbit4=max(bit4,jbit4)


        join =(((jbit1*10+jbit2)*10)+jbit3)*10+jbit4
c        write(*,*) 'JOIN: ',step,'#',jstep,'=',join
        return
        end

         subroutine setref(iref,iwalk)
       implicit none 
C====>Begin Module SETREF                 File cidrtms2.f               
C---->Makedcls Options: All variables                                   
C
      INTEGER             IOR,         ISHFT
C
C     Parameter variables
C
*@ifdef int64
      INTEGER             WORDLEN
      PARAMETER           (WORDLEN = 64)
*@else
*      INTEGER             WORDLEN
*      PARAMETER           (WORDLEN = 32)
*@endif
C
C     Argument variables
C
      INTEGER             IREF(*),     IWALK
C
C     Local variables
C
      INTEGER             BITPOS,      NEL,         WORDPOS
C
C====>End Module   SETREF                 File cidrtms2.f               

c        set the reference bit at bit iwalk counted from the beginning
c        to one


         wordpos = ((iwalk -1 ) / wordlen) +1
         bitpos  = iwalk-1- (wordpos-1)*wordlen

         iref(wordpos)=ior(iref(wordpos),ishft(1,bitpos))

         return
         end

         subroutine unsetref(iref,iwalk)
       implicit none 
C====>Begin Module UNSETREF               File cidrtms2.f               
C---->Makedcls Options: All variables                                   
C
      INTEGER             IAND,        IEOR,        ISHFT
C
C     Parameter variables
C
*@ifdef int64
      INTEGER             WORDLEN
      PARAMETER           (WORDLEN = 64)
*@else
*      INTEGER             WORDLEN
*      PARAMETER           (WORDLEN = 32)
*@endif
C
C     Argument variables
C
      INTEGER             IREF(*),     IWALK
C
C     Local variables
C
      INTEGER             BITPOS,      TMP,         WORDPOS
C
C====>End Module   UNSETREF               File cidrtms2.f               

c        set the reference bit at bit iwalk counted from the beginning
c        to zero


         wordpos = ((iwalk -1 ) / wordlen) +1
         bitpos  = iwalk-1- (wordpos-1)*wordlen

         tmp=ishft(iand(iref(wordpos),ishft(1,bitpos)),-bitpos)
         if (tmp.eq.1)
     .   iref(wordpos)=ieor(iref(wordpos),ishft(1,bitpos))

         return
         end

          subroutine uncmpref(iref,uref,nref)
       implicit none 
C====>Begin Module UNCMPREF               File cidrtms2.f               
C---->Makedcls Options: All variables                                   
C
      INTEGER             IAND,        ISHFT
C
C     Parameter variables
C
*@ifdef int64
      INTEGER             WORDLEN
      PARAMETER           (WORDLEN = 64)
*@else
*      INTEGER             WORDLEN
*      PARAMETER           (WORDLEN = 32)
*@endif
C
C     Argument variables
C
      INTEGER             IREF(*),     NREF,        UREF(NREF)
C
C     Local variables
C
      INTEGER             BITPOS,      CNT,         I,           J
      INTEGER             WORDBUF,     WORDPOS
C
C====>End Module   UNCMPREF               File cidrtms2.f               


           wordpos = ((nref-1) /wordlen)+1
           bitpos  = nref- (wordpos-1)*wordlen

            cnt=0
            do i=1,wordpos-1
             wordbuf=iref(i)
              do j=1,wordlen
                cnt=cnt+1
                uref(cnt)=ishft(iand(wordbuf,ishft(1,j-1)),-j+1)
              enddo
             enddo
             wordbuf=iref(wordpos)
             do j=1,bitpos
             cnt=cnt+1
                uref(cnt)=ishft(iand(wordbuf,ishft(1,j-1)),-j+1)
             enddo
            return
            end

          subroutine cmpref(iref,uref,nref)
       implicit none 
C====>Begin Module CMPREF                 File cidrtms2.f               
C---->Makedcls Options: All variables                                   
C
      INTEGER             IOR,         ISHFT
C
C     Parameter variables
C
*@ifdef int64
      INTEGER             WORDLEN
      PARAMETER           (WORDLEN = 64)
*@else
*      INTEGER             WORDLEN
*      PARAMETER           (WORDLEN = 32)
*@endif
C
C     Argument variables
C
      INTEGER             IREF(*),     NREF,        UREF(*)
C
C     Local variables
C
      INTEGER             BITPOS,      CNT,         I,           J
      INTEGER             TMP,         WORDBUF,     WORDPOS
C
C====>End Module   CMPREF                 File cidrtms2.f               

           wordpos = ((nref-1) /wordlen)+1
           bitpos  = nref- (wordpos-1)*wordlen

            cnt=0
            do i=1,wordpos-1
             wordbuf=0
              do j=1,wordlen
                cnt=cnt+1
                wordbuf=ior(wordbuf,ishft(uref(cnt),j-1))
              enddo
             iref(i)=wordbuf
            enddo
             wordbuf=0
             do j=1,bitpos
             cnt=cnt+1
                wordbuf=ior(wordbuf,ishft(uref(cnt),j-1))
             enddo
             iref(wordpos)=wordbuf
            return
            end

         integer function  getref(iref,iwalk)
       implicit none 
C====>Begin Module GETREF                 File cidrtms2.f               
C---->Makedcls Options: All variables                                   
C
      INTEGER             IAND,        ISHFT
C
C     Parameter variables
C
*@ifdef int64
      INTEGER             WORDLEN
      PARAMETER           (WORDLEN = 64)
*@else
*      INTEGER             WORDLEN
*      PARAMETER           (WORDLEN = 32)
*@endif
C
C     Argument variables
C
      INTEGER             IREF(*),     IWALK
C
C     Local variables
C
      INTEGER             BITPOS,      WORDPOS
C
C====>End Module   GETREF                 File cidrtms2.f               

c        return the reference bit at bit iwalk counted from the beginnin


         wordpos = ((iwalk -1 ) / wordlen) +1
         bitpos  = iwalk-1- (wordpos-1)*wordlen

         getref=ishft(iand(iref(wordpos),ishft(1,bitpos)),-bitpos)

         return
         end

         integer function  getiref(ilen,iref,iwalk)
       implicit none 
C====>Begin Module GETIREF                File cidrtms2.f               
C---->Makedcls Options: All variables                                   
C
      INTEGER             IAND,        ISHFT
C
C     Parameter variables
C
*@ifdef int64
      INTEGER             WORDLEN
      PARAMETER           (WORDLEN = 64)
*@else
*      INTEGER             WORDLEN
*      PARAMETER           (WORDLEN = 32)
*@endif
C
C     Argument variables
C
      INTEGER             ILEN,        IREF(*),     IWALK
C
C     Local variables
C
      INTEGER             BITPOS,      I,           ITMP,        TMP
      INTEGER             WORDPOS
C
C====>End Module   GETIREF                File cidrtms2.f               

c        return the reference bit at bit iwalk counted from the beginnin
c        valid =0 invalid=1

         itmp=0
         do i=iwalk,ilen
         wordpos = ((i -1 ) / wordlen) +1
         bitpos  = i-1- (wordpos-1)*wordlen

         tmp=ishft(iand(iref(wordpos),ishft(1,bitpos)),-bitpos)
         itmp = itmp+mod(tmp+1,2)
         if (tmp.eq.1) goto 100
         enddo
100      getiref = itmp
c        write(0,*) 'getiref returns: ',itmp
         return
         end






      subroutine indx01r( nlist, numi, len01, vec01, indxv, icode )
c
c  transform between the index representation of a vector and the
c  0/1 representation.
c  indxv(*) = 1,2,  4,    7
c  vec01(*) = 1,1,0,1,0,0,1
c
c  arguments:
c  numi = number of index-representation entries.
c  len01 = length of the 0/1 representation vector.
c  vec01(1:len01) = 0/1 representation.
c  indxv(1:numi) = index vector representation.
c  icode = 0  convert from vec01(*) to indxv(*),
c        = 1  convert from indxv(*) to vec01(*).
c
c
       implicit none 
C====>Begin Module INDX01R                File cidrtms2.f               
C---->Makedcls Options: All variables                                   
C
C     Parameter variables
C
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
      INTEGER             TOINDX
      PARAMETER           (TOINDX = 0)
      INTEGER             TO01
      PARAMETER           (TO01 = 1)
*@ifdef int64
      INTEGER             WORDLEN
      PARAMETER           (WORDLEN = 64)
*@else
*      INTEGER             WORDLEN
*      PARAMETER           (WORDLEN = 32)
*@endif

C     Argument variables
C
      INTEGER             ICODE,       INDXV(NUMI), LEN01,       NLIST
      INTEGER             NUMI,        VEC01(LEN01)
C
C     Local variables
C
      INTEGER             I,           ICNT
C
C====>End Module   INDX01R                File cidrtms2.f               

c
c
      if ( icode .eq. toindx ) then
          call bummer('invalid option',0,2)
      else
c        # indxv(*) to vec01(*).
         do 20 i = 1,  ( (len01-1)/wordlen) +1
            vec01(i) = 0
20       continue
         do 30 icnt = 1, numi
c           vec01(indxv(icnt)) = 1
            call setref(vec01,indxv(icnt))
30       continue
         write(nlist,'(1x,a,i6,a)')'indx01:',
     &    numi,' elements set in vec01(*)'
      endif
      return
      end
*@ifdef spinorbit
c*deck inisp
      subroutine inisp
       implicit none 
C====>Begin Module INISP                  File cidrtms2.f               
C---->Makedcls Options: All variables                                   
C
C     Parameter variables
C
      INTEGER             MULTMX
      PARAMETER           (MULTMX = 19)
      INTEGER             NMULMX
      PARAMETER           (NMULMX = 9)
C
C     Local variables
C
      INTEGER             I,           IMULT,       J,           MS
C
      INCLUDE 'so.inc'
C====>End Module   INISP                  File cidrtms2.f               

c
c     # initialize the spin function irrep and multp arrays
c
c
      nmul = 0
      do 5 j = 1, hmult, 2
         nmul = nmul +1
         multp(nmul) = j
5     continue

      do 10 i = 1, multmx
         do 11 j = 1, multmx
         spnir(i, j) = 0
11       continue
10    continue
c
c     # spin function irreps(integer spin)
c
c
c     # spin = 0, 2, 4, ...(even); multiplicity = 1, 5, 9, ...
c
c      write(*, *)' hmult', hmult
      do 6 imult = 1, multmx, 4
         spnir(1, imult) = 1
         do 7 ms = 2, imult, 4
            spnir(ms,   imult) = lxyzir(1)
            spnir(ms+1, imult) = lxyzir(2)
            spnir(ms+2, imult) = lxyzir(3)
            spnir(ms+3, imult) = 1
7        continue
6     continue

c
c     # spin = 1, 3, 5, ...(odd) ; multiplicity = 3, 7, 11, ...
c
      do 8 imult = 3, multmx, 4
         spnir(1, imult) = lxyzir(3)
         spnir(2, imult) = lxyzir(2)
         spnir(3, imult) = lxyzir(1)
         do 9 ms = 4, imult, 4
            spnir(ms,   imult) = 1
            spnir(ms+1, imult) = lxyzir(3)
            spnir(ms+2, imult) = lxyzir(2)
            spnir(ms+3, imult) = lxyzir(1)
9        continue
8     continue
c      write(*,*)'lxyzir', lxyzir
c      write(*,*)'spnir'
c      write(*,"(19i3)")spnir
      return
      end

*@endif


      subroutine print_revision2(name,nlist)
      implicit none
      integer nlist
      character*12 name
      character*70 string
  50  format('* ',a,t12,a,t40,a,t68,' *')
      write(string,50) name(1:len_trim(name)) // '2.f',
     .   '$Revision: 2.17.6.1 $','$Date: 2013/04/11 14:37:29 $'
      call substitute(string)
      write(nlist,'(a)') string
      return
      end

       subroutine progheader(nlist)
       implicit none
       integer nlist
       character*70 str
      write(nlist,6010)
6010  format(' program cidrtms 7.0  ',  
     & //' distinct row table construction, reference csf',  
     & ' selection, and internal'  
     & /' walk selection for multireference single-',  
     & ' and double-excitation'  
     & /'configuration interaction.'  
     & //' references:',t15,'r. shepard, i. shavitt, r. m. pitzer, d.',  
     & ' c. comeau, m. pepper'  
     & /t19,'h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and',  
     & /t19,'j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).'   
     & /t15,'h. lischka, r. shepard, f. b. brown, and i. shavitt,'  
     & /t19,'int. j. quantum chem. symp. 15, 91 (1981).'  
     & //' based on the initial version by  Ron Shepard',  
     & //' extended for spin-orbit CI calculations ( Russ Pitzer, OSU)',   
     & //' and large active spaces (Thomas Müller, FZ(21 Juelich)',
     & //' multi-DRT version (multi-headed DRTs)                 ')  
c
c     # print out the name and address of the local programmer
c     # responsible for maintaining this program.
c
      call who2c( 'CIDRTMS', nlist )
 20   format(5('****'),'*** File revision status: ***',5('****'))
      write(nlist,20)
      call print_revision1('cidrtms     ',nlist)
      call print_revision2('cidrtms     ',nlist)
      call print_revision3('cidrtms     ',nlist)
      call print_revision4('cidrtms     ',nlist)
 21   format(17('****'))
      write(nlist,21)
      end

      subroutine substitute(str)
      implicit none
      character*70 str
      integer i
       do i=1, len_trim(str)
         if (str(i:i).eq.'$') str(i:i)=' '
       enddo
      return
      end


