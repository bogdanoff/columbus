!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
C
C     Common variables
C
      INTEGER             BROW(0:NIMOMX),           EXTBK(0:NIMOMX)
      INTEGER             KROW(0:NIMOMX),           RANGE(0:NIMOMX)
      INTEGER             YB(0:NIMOMX),             YK(0:NIMOMX)
C
      LOGICAL             QEQ(0:NIMOMX)
C
      REAL*8              V(3,0:NIMOMX)
C
      COMMON / CSTACK /   V,           RANGE,       EXTBK,       QEQ
      COMMON / CSTACK /   BROW,        KROW,        YB,          YK
C
