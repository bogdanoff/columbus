!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
C
C     Common variables
C
      INTEGER             BMAX(NIMOMX,NMXDRT),      BMAXR(NIMOMX,NMXDRT)
      INTEGER             BMIN(NIMOMX,NMXDRT),      BMINR(NIMOMX,NMXDRT)
      INTEGER             NACT(NMXDRT),             NDOT(NMXDRT)
      INTEGER             NREF(NMXDRT)
      INTEGER             OCCMAX(NIMOMX,NMXDRT)
      INTEGER             OCCMIN(NIMOMX,NMXDRT)
      INTEGER             OCCMNR(NIMOMX,NMXDRT)
      INTEGER             OCCMXR(NIMOMX,NMXDRT),    SMASK(NIMOMX,NMXDRT)
      INTEGER             SMASKR(NIMOMX,NMXDRT)
C
      COMMON / INPUT1 /   OCCMNR,      OCCMXR,      BMINR,       BMAXR
      COMMON / INPUT1 /   SMASKR,      OCCMIN,      OCCMAX,      BMIN
      COMMON / INPUT1 /   BMAX,        SMASK,       NREF,        NDOT
      COMMON / INPUT1 /   NACT
C
