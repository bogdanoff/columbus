!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
C
C     Common variables
C
      CHARACTER*4         SLABEL(0:8)
C
      INTEGER             EXMAX,       NCSFSM(4,8,NMXDRT),       NELI
      INTEGER             NEXO(8),     NMPSY(8),    NMSKP(8)
      INTEGER             NVALW(4,NMXDRT),          NVIWSM(4,8,NMXDRT)
      INTEGER             SMULT
C
      LOGICAL             QGIS,        QSTEPW,      QXLIM
C
      COMMON / INPUT3 /   SMULT,       EXMAX,       NVALW,       NVIWSM
      COMMON / INPUT3 /   NCSFSM,      NMPSY,       NMSKP,       NEXO
      COMMON / INPUT3 /   QXLIM,       QGIS,        QSTEPW,      NELI
      COMMON / INPUT3 /   SLABEL
C
