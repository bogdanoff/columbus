!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      program makscf
c
c  Prepares the input file 'scfin' for an SCF calculation
c  autor: Michal Dallos, University Vienna
c
c  version log:
c  09-nov-01 nkey changed to 13 because of hardwired unit numbers
c            in the get*() subroutines. output changed to nout. -rls
c
       implicit none
c     #  common section
c
      integer         nin, nout, nlist, idum, nkey, ninffl
      common /cfiles/ nin, nout, nlist, idum, nkey, ninffl
c
c     #  parameter section
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
c     # maximum number of molecular orbitals...
      integer   nmotx
      parameter(nmotx=1023)
c
      character*(*) program, version
      parameter (program='MAKSCF')
      parameter (version='5.5.1')
c
      character*4 molsym,slabel(0:8)
c
      integer   yes,  no
      parameter(yes=1,no=0)
c
c     #  character section
c
      character*130 fname
      character*80 title
c
c     #  integer section
c
      integer alpha(nmotx)
      integer beta(nmotx)
      integer ichar,i,isym,iprct,iscf,i2con,inflg,ivect,ipunch,iprint,
     & idebug,isave,itmax,isto,micmx,iauto,idfix,itprep,itcon,iall,
     & ivo,igrad,idmat,irnd,ichar2,icount,itest
      integer j
      integer fdoc(8),fosh(8),foccup(nmotx),fsymosh(nmotx)
      integer hspin
      integer msh
      integer nsym,nmpsy(8),nmot,nosh,nsp,nsymo
c
c     #  real section
c
      real*8 eshst
      real*8 ttr,tmr,tnr
c
c     # initialize some variables
c
      call izero_wr(8,fosh,1)
      call izero_wr(8,fdoc,1)
      call izero_wr(nmotx,foccup,1)
      call izero_wr(nmotx,fsymosh,1)
      call izero_wr(nmotx,alpha,1)
      call izero_wr(nmotx,beta,1)
      nin    = 5
      nlist  = 6
      nout   = 20
      nkey   = 13
      ninffl = 21
c
c     #  open the required files
c
      fname = 'infofl'
      call trnfln(1,fname)
      open(unit=ninffl,file=fname,status='unknown')
c
c     # nout used for the output file instead of nkey, which is the
c     # keystroke log file. 09-nov-01 -rls
      fname = 'scfin'
      call trnfln(1,fname)
      open(unit=nout,file=fname,status='unknown')
c
c     # note: the keystroke log file now works for some cases, but
c     # not all input is correctly echoed. -rls
      fname = 'makscfky'
      call trnfln(1,fname)
      open(unit=nkey,file=fname,status='unknown')
c
      call headwr(nlist, program, version)
c
      read(ninffl,'(a4)')molsym
      read(ninffl,*)
      read(ninffl,'(i3)') nsym
      read(ninffl,'(8(2x,a3))') (slabel(i),i=1,nsym)
c
c     # consider switching the 2nd and 3rd records in this file so
c     # that the following rewind can be avoided. -rls
      rewind ninffl
      read(ninffl,*)
      call izero_wr(nsym,nmpsy,1)
      read(ninffl,'(8i5)') (nmpsy(i),i=1,nsym)
c
      nmot = 0
      do isym = 1, nsym
         nmot = nmot + nmpsy(isym)
      enddo
c
      write(nlist,'(/,1x,"Symmetry orbital summary:")')
      write(nlist,'(/1x,"Molecular symmetry group:",2x,a4)')molsym
      write(nlist,'(1x,"Symm.blocks:",t20,8(2x,i2,2x))')(i,i=1,nsym)
      write(nlist,'(1x,"Symm.labels:",t20,8(2x,a4))')
     & (slabel(i),i=1,nsym)
      write(nlist,
     & '(1x,"Number of basis"/,2x,"functions:",t20,8(2x,i3,1x))')
     & (nmpsy(i),i=1,nsym)
      if(nmot.gt.nmotx)then
         write(nlist,
     &   '(1x,"Total number of basis functions exceedes the maximum!")')
         write(nlist,'(1x,"Actual number of basis functions:",1x,i3)')
     &    nmot
         write(nlist,'(1x,"Maximal number of basis functions:",1x,i3)')
     &    nmotx
         call bummer('nmot = ',nmotx,2)
      endif
c
      ichar = 1
      write(nlist,*)
      call getyn('Do you want a closed shell calculation ?',ichar)
c
1100  write(nlist,*)
      call iniv(fdoc,nsym,
     & 'Input the no. of doubly occupied orbitals for each irrep, DOCC',
     & 1, *1100)
      do isym=1,nsym
         if ((fdoc(isym) .lt. 0) .or. (fdoc(isym) .gt. nmpsy(isym)))
     &    go to 1100
      enddo
c
      if (ichar.eq.no) then
1200     write(nlist,*)
         call iniv(fosh,nsym,
     &    'Input the no. of open shell orbitals for each irrep, OPSH',
     &    1, *1200)
         do isym=1,nsym
            if ((fosh(isym) .lt. 0) .or. (fosh(isym) .gt. nmpsy(isym)))
     &       go to 1200
         enddo
      endif
c
      write(nlist,'(//1x,"The orbital occupation is:")')
      write(nlist,'(/,6x,8(a3,1x))')(slabel(isym),isym=1,nsym)
      write(nlist,'(1x,"DOCC",2x,8(i3,1x))')(fdoc(isym),isym=1,nsym)
      write(nlist,'(1x,"OPSH",2x,8(i3,1x))')(fosh(isym),isym=1,nsym)
c
c     #  consistecy check
c
      do isym=1,nsym
         if ((fdoc(isym)+fosh(isym)).gt.nmpsy(isym))then
         write(nlist,'("Input ERROR!")')
         write(nlist,
     &    '(''You specified '',i3,'' DOCC + OPSH orbitals in the '//
     &    'irrep '',a3,'' but there are only '',i3,'' orbitals.'')')
     &    fdoc(isym)+fosh(isym),slabel(isym),nmpsy(isym)
        endif
      enddo
c
      ichar = yes
      write(nlist,*)
      call getyn('Is this correct?',ichar)
      if (ichar .eq. no) go to 1100
c
c     #  analyze the open shells
c
      call izero_wr(nmotx,fsymosh,1)
      call izero_wr(nmotx,alpha,1)
      call izero_wr(nmotx,beta,1)
      nosh = 0
      icount = 1
      do isym = 1, nsym
ctm
ctm incorrect behaviour for open shells not belonging to the first irrep
ctm
         if (fosh(isym) .gt. 0) then
            nosh = nosh + fosh(isym)
            fsymosh(icount) = isym
            icount = icount + 1
         endif

      enddo
      nsymo = 0
      do isym=1,nsym
         if (fosh(isym) .gt. 0) nsymo = nsymo + 1
      enddo
c
      if (nosh .eq. 1) then
         write(nlist,
     &    '(/1x,"You specified an doublet with the unpaired electron in"
     &    ,1x,i2,1x,a3,1x,"orbital.")')
     &    fdoc(fsymosh(1))+fosh(fsymosh(1)), slabel(fsymosh(1))
         alpha(1) = 0
         beta(1) = -1
c
      elseif (nosh .gt. 1) then
         itest = yes
         do isym=1,nsym
            if (fosh(isym).gt.1) itest = no
         enddo
c
         if (itest.eq.yes) then
            write(nlist,
     &       '(/'' There are '',i3,'' open shell orbitals.'')')
     &       nosh
            hspin = 1
            write(nlist,*)
            call getyn('Would you like the high spin case?',hspin)
            if (hspin.eq.yes) then
c              #  high spin case
               icount = 1
               do i=1,nosh
                  do j=1,i
                  beta(icount) = -1
                  icount = icount + 1
                  enddo
               enddo
               write(nlist,'(1x,"You specified a triplet.")')
               go to 1300
            elseif ((hspin.eq.no).and.(nosh.eq.2)) then
c              #  open shell singlet
               beta(1) = -1
               beta(2) = 3
               beta(3) = -1
               write(nlist,
     &          '(1x,"You specified an open shell singlet.")')
               go to 1300
            endif
         endif
c
1250     write(nlist,
     &    '(/'' Input the Roothaan alpha and beta values.'')')
         icount = 1
         nsymo = 0
         do isym=1,nsym
         if (fosh(isym).gt.0) nsymo = nsymo + 1
         enddo
         do i=1,nsymo
            do j=1,i
            write(nlist,
     &       '('' Input alpha('',i2,'')   beta('',i2,'') '//
     &       'pair:'')')i,j
            read(nin,*)alpha(icount),beta(icount)
            icount = icount + 1
            enddo
         enddo
      endif
1300  continue
c
c  ##
c
c   recommended values are ...
c     iprct=-8         ! diis flag
      iprct=-15         ! diis flag
      iscf=9           ! criterion for SCF convergence
      i2con=0          ! TCSCF flag
      inflg=-1         ! source of start vectors
      ivect=0          ! print flag for MO coefficients on output
      ipunch=8         ! unit no. for file "mocoef"
      iprint=0         ! printing of integral values
      idebug=0         ! printing of additional information
      isave=0          ! use results from previous state treated
                       ! in same run?
      itmax=40         ! max. no. of iterations
      isto=20          ! normalization threshold
      micmx=1          ! no. of microiterations per iteration
      iauto=1          ! automatic orbital ordering
      idfix=0          ! damping factors variable of fixed
      itprep=0         ! unit no. for storage of additional data
      itcon=0          ! unit no. for reading of additional data
      iall=-1          ! all vectors in input and (ordered) in
                       ! output
      ivo=0            ! IVO calculation?
      igrad=11         ! nit no. for writing the SCF gradient
                       ! input (formatted file "vectgrd")
      idmat=0          ! disabled
      irnd=1           ! supermatrix not needed
      ttr = 0.9
      tmr = 0.1
      tnr = 0.05
c
      ichar = 0
      write(nlist,*)
      call getyn(
     & 'Would you like to change the default program parameters?',
     & ichar)
c
c     ##  Optionaly change some values
c
      if (ichar .eq. yes) then
c        # number of iterations
         write(nlist,*)
         call GETINT2('Input the number of SCF iterations.',itmax,'40')
c        # scf convergence criterion
         call GETINT2('Input the SCF convergence criterion.',iscf,'9')
c
c        # damping parameters
         ichar2 = 0
         call getyn(
     &    'Would you like to change the energy damping parameters  ?',
     &    ichar2)
         if (ichar2 .eq. yes) then
            write(nlist,
     &       '('' The energy damping parameters are: dampmax, '//
     &       'dampmin, dampdel'')')
            write(nlist,
     &       '('' dampmax  - maximal weight of the old fock operator.'''
     &       //
     &       '/'' dampmin  - minimal weight of the old fock operator.'''
     &       //
     &       '/'' dampdel  - value, by which the damping parameter '//
     &       'is decreased'',/,''every iteration until dampmin is '//
     &       'reached.''/)')
            call getreal('Input the dampmax value',ttr,'0.9')
            call getreal('Input the dampmin value',tmr,'0.1')
            call getreal('Input the dampdel value',tnr,'0.05')
         endif
c        #  source of mo coeficients
1500     write(nlist,
     &    '(/'' Source of the initial MO coeficients:''/'//
     &    ''' 1. Generate by diagonalizing the H matrix.''/'//
     &    ''' 2. Read from the mocoef file.''/)')
         call GETINT2('Input option:',itest,'1')
         if (itest.eq.2) then
            inflg = 8
         elseif((itest.lt.1).or.(itest.gt.2)) then
            go to 1500
         endif
c
      endif
c
c     # why was getchr() used here instead of getitle()?
c     # Should the title really be converted to upper case?
c      write(nlist,'(/" Input title:")')
c      call getchr (' ',title,' ')
      title = 'Default SCF Title'
      call getitle(' Input a title:', title )
c
c     # write the output file (scfin).
c
      write(nout,'(a80)') title
      write(nout,'(21i4)')
     & iprct,  iscf,   i2con,  inflg, ivect,
     & ipunch, iprint, idebug, isave, itmax,
     & isto,   micmx,  iauto,  idfix, itprep,
     & itcon,  iall,   ivo,    igrad, idmat,
     & irnd
      write(nout,'(8i4)')(fdoc(isym)+fosh(isym),isym=1,nsym)
      do isym=1,nsym
         do i=1,fdoc(isym)
            foccup(i) = 2
         enddo
         do i=fdoc(isym)+1,fdoc(isym)+fosh(isym)
            foccup(i) = 1
         enddo
         if (fdoc(isym)+fosh(isym).ne.0) write(nout,'(20i4)')
     &    (foccup(i),i=1,fdoc(isym)+fosh(isym))
      enddo
      eshst = 0.0
      msh = 0
      nsp = 0
c
c     #  Roothaan parameters
c
      if (nosh .eq. 1) then
         write(nout,'(2(i6,1x,i3))')alpha(1),1,beta(1),1
      elseif (nosh.gt.1) then
         icount = 1
         do i=1,nsymo
            do j=1,i
               write(nout,'(2(i6,1x,i3))')alpha(icount),1,beta(icount),1
               icount = icount + 1
            enddo
         enddo
      endif
c
c     #  write out the rest
c
      write(nout,'(4f10.5,3i5)')ttr,tmr,tnr,eshst,msh,nsp
      write(nout,'(1x,"(4f18.12)")')
      write(nout,'(i4)')1
c
      call bummer('normal termination',0,3)
c
      stop 'end of makscf'
      end

      subroutine iniv( j, n, text, iprint, * )
c
c  read an integer array j(1:n) with prompt from text.
c
c  if (iprint .eq. 1), then echo to the keystroke log file.
c
       implicit none
c     integer         nin, nout, nlist, ndrt, nkey(8),ninffl
      integer         nin, nout, nlist, ndrt, nkey, ninffl
      common /cfiles/ nin, nout, nlist, ndrt, nkey, ninffl
c
      character*(*) text
      integer n,iprint
      integer j(n)
c
      integer   qok,   qup,   qinerr,   qexerr,   qend
      parameter(qok=0, qup=1, qinerr=2, qexerr=3, qend=-1)
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer numerr, iret, ios, iwerr
c
      iwerr = 0
      iret = 0
c
      numerr=0
100   continue
      write(nlist,6010) text
6010  format(1x,a,':')
c
      call qrdiv( nin, n, j, iret, ios )
c
      if(iret.eq.qok)then
c        if (iprint.eq.1) call wrtivf(nkey(1),j,n,' ',' ',0,iwerr)
         if (iprint.eq.1) call wrtivf(nkey,j,n,' ',' ',0,iwerr)
         if(iwerr.ne.0)
     &    call bummer('iniv: from wrtivf, iwerr=',iwerr,faterr)
         return
      elseif(iret.eq.qup)then
c        if (iprint.eq.1) write(nkey(1),*)' ^'
         if (iprint.eq.1) write(nkey,*)' ^'
         return 1
      elseif(iret.eq.qend)then
         call bummer('iniv: eof on input',0,faterr)
      elseif(iret.eq.qinerr)then
         write(nlist,*)'internal error, ios=',ios
         numerr=numerr+1
         if(numerr.gt.5)call bummer('iniv: numerr=',numerr,faterr)
         go to 100
      elseif(iret.eq.qexerr)then
         write(nlist,*)'external error, iostat=',ios
         numerr=numerr+1
         if(numerr.gt.5)call bummer('iniv: numerr=',numerr,faterr)
         go to 100
      else
         call bummer('iniv: unknown code, iret=',iret,faterr)
      endif
c
      end

      subroutine qrdiv( nunit, ivlen, iv, iret, ierr )
c
c  simulated list-directed read of an integer vector.
c
c  input:
c  nunit = fortran unit number.
c  ivlen = integer vector length.
c  iv(*) = integer vector.
c
c  output:
c  iv(*) = modified vector.
c  iret  = integer return code.  includes normal return, up-arrow,
c          internal input error, external input error, and
c          end-of-file.
c  ierr  = integer error code.  defined only with internal or
c          external errors.
c
c  written by ron shepard 1-oct-87.
c
       implicit none
      integer nunit, ivlen, iv(ivlen), iret, ierr
c
c     # output return codes...
      integer   qok,   qup,   qinerr,   qexerr,   qend
      parameter(qok=0, qup=1, qinerr=2, qexerr=3, qend=-1)
c
      integer l1, l2, ivpt, l1n, ircode, starpt, repeat, ival, i1, i
      integer   linlen
      parameter(linlen=80)
      character*(linlen) linbuf
c
c     # internal return codes...
      integer   irok,   ireof,    ireor,    irup
      parameter(irok=0, ireof=-1, ireor=-2, irup=-3)
c
      integer  ichtoi
      external ichtoi
c
*@ifdef reference
*C
*C     # all-fortran code included for reference.
*C     # only external errors are detected. '^' is not detected.
*C
*      read(nunit,*,iostat=ierr)iv
*      if(ierr.lt.0)then
*          iret=qend
*      elseif(ierr.eq.0)then
*          iret=qok
*      elseif(ierr.gt.0)then
*          iret=qexerr
*      endif
*      return
*@endif
c
      ival = 0
      l2 = 0
      repeat = 0
      ircode = 0
c
      ivpt=0
      l1n=linlen+1
100   continue
      l1=l1n
      if(ivpt.ge.ivlen)then
         iret=qok
         return
      endif
      call gtoken(nunit,linbuf,linlen,l1,l2,l1n,ircode)
      if(ircode.eq.irok)then
c
c        # interpret token.
c        # l1 points to the first character of the token.
c        # l2 points to the last character of the token.
c        # l1n points to the first character after the token.
c        # token possibilities are: [ nnn [ * [ nnn ] ] ].
c
         if(l1.gt.l2)then
            ivpt=ivpt+1
            go to 100
         else
            starpt=index(linbuf(l1:l2),'*')
            if(starpt.eq.0)then
               ivpt=ivpt+1
               iv(ivpt)=ichtoi(linbuf(l1:l2),ierr)
               if(ierr.ne.0)then
                  iret=qinerr
                  return
               endif
               go to 100
            elseif(starpt.eq.1)then
               ierr=1
               iret=qinerr
               return
            else
               repeat=ichtoi(linbuf(l1:l1+starpt-2),ierr)
               if(ierr.ne.0)then
                  iret=qinerr
                  return
               elseif(repeat.le.0)then
                  ierr=1
                  iret=qinerr
                  return
               endif
               if(starpt.eq.l2-l1+1)then
                  ivpt=ivpt+repeat
                  go to 100
               else
                  ival=ichtoi(linbuf(l1+starpt:l2),ierr)
                  if(ierr.ne.0)then
                     iret=qinerr
                     return
                  endif
                  i1=ivpt+1
                  ivpt=min(ivpt+repeat,ivlen)
                  do 110 i=i1,ivpt
                     iv(i)=ival
110               continue
                  go to 100
               endif
            endif
         endif
      elseif(ircode.eq.ireof)then
         iret=qend
         return
      elseif(ircode.eq.ireor)then
         iret=qok
         return
      elseif(ircode.eq.irup)then
         iret=qup
         return
      else
         iret=qexerr
         ierr=ircode
         return
      endif
c
      end


      subroutine wrtivf( unit, ia, lena, cfmt, string, flag, ierr )
c
c  write a formatted integer vector.  optionally determine the format
c  that gives the minimum number of 80 column records.  formats are
c  determined assuming no carriage control characters in column 1.
c
c  29-nov-90 string added. -rls
c  08-sep-90 ierr initialized for lena=0 cases. -rls
c  cfmt option added 23-sep-87. -rls
c  ierr added 21-sep-87. -rls
c  modified by ron shepard, 8/22/84.
c  written by ray bair, anl, 8/22/84.
c
c  input:
c  unit = fortran unit number.
c  ia(*)= integer array of numbers to output.
c  lena  = array length. may be zero.
c  cfmt = output format.
c         if cfmt=' ', then determine a suitable format.
c  flag = format flag.  only the last digit, mod(flag,10) is
c                       used in this version.
c       = *0  =>  do not write out the format.
c       = *n  =>  write out the format and string.
c
c  output:
c  ierr = 0 for normal return
c
       implicit none
      integer lena, ia(*), flag, unit, ierr
      character*(*) cfmt, string
c
      intrinsic len
c
      integer amina, amaxa, ifpt, i, imin, imax
      character*6 ifmt(2:21)
      data ifmt/'(40i2)','(26i3)','(20i4)','(16i5)','(13i6)','(11i7)',
     * '(10i8)','(8i9)','(8i10)','(7i11)','(6i12)','(6i13)','(5i14)',
     * '(5i15)','(5i16)','(4i17)','(4i18)','(4i19)','(4i20)','(3i26)'/
c
      ierr = 0
      if( cfmt .ne. ' ' )then
c        # optionally write out the format...
         if ( mod( flag, 10 ) .ne. 0 ) then
            if ( len(cfmt) .gt. 20 ) then
c              # drt formats must be no longer than 20 characters.
               ierr = -2
               return
            endif
            write(unit,6010,iostat=ierr,err=6) cfmt, string
         endif
c        # write the array using the dummy argument input format.
         if ( lena .gt. 0 ) then
            call wrtiv( unit, ia, lena, cfmt, ierr)
            if ( ierr .ne. 0 ) return
         endif
      else
c        # find the best format...
         if( lena .le. 0 ) then
            ifpt = 2
         else
            amina = 0
            amaxa = 0
c           # find largest and smallest element.
            do 1 i = 1, lena
               amina = min( ia(i), amina )
               amaxa = max( ia(i), amaxa )
1           continue
c           # find log(10)+2 of smallest element, allow for sign.
            imin = 2
            if( amina .eq. 0 ) go to 3
            i = 3
            amina = abs(amina)
            do 2 imin = i, 20
               if( amina .lt. 10 ) go to 3
               amina = amina / 10
2           continue
            imin = 21
c           # find log(10)+2 of largest element.
3           i = 2
            do 4 imax = i, 20
               if( amaxa.lt.10 ) go to 5
               amaxa = amaxa/10
4           continue
            imax = 21
5           ifpt = max( imin,imax )
         endif
c        # optionally write the format.
         if( mod( flag, 10 ) .ne. 0 ) then
            write(unit,6010,iostat=ierr,err=6) ifmt(ifpt), string
         endif
c        # write the array.
         if ( lena .gt. 0 ) then
            call wrtiv( unit, ia, lena, ifmt(ifpt), ierr )
            if ( ierr .ne. 0 ) return
         endif
      endif
c
6     return
6010  format(a,t21,a)
      end

      subroutine wrtiv( iunit, ia, len, cfmt, ierr )
c
c  write an integer vector to unit iunit.
c
c  input:
c  iunit = fortran unit.
c  ia(*) = integer vector.
c  len   = vector length.  must be > 0.
c  cfmt  = character format.
c
c  output:
c  ierr  = 0 for normal return.
c
       implicit none
      integer iunit, len, ierr
      integer ia(len)
      character*(*) cfmt
c
      write(iunit,cfmt,iostat=ierr) ia
c
      return
      end

      subroutine gtoken( nunit, linbuf, linlen, l1, l2, l1n, ircode )
c
c  get the next token from linbuf.  if linbuf is exhausted, read a
c  new buffer line from nunit.  tokens are delimited by spaces,
c  commas, end-of-lines, up-arrows, and slashes.
c
c  input:
c  nunit  = input unit.
c  linbuf = character line buffer.
c  linlen = buffer length.
c  l1     = initial position in linbuf at which to begin the search.
c
c  output:
c  linbuf = possibly modified.
c  l1     = first position of the token.
c  l2     = last position of the token.
c  l1n    = initial position of the next token.
c  ircode = return code.  codes include internal end-of-file,
c           end-of-record, special up-arrow,  normal returns and
c           external error codes.
c
c  written by ron shepard 1-oct-87.
c
       implicit none
      integer nunit, linlen, l1, l2, l1n, ircode
      character*(*) linbuf
c
      integer i, lx, ierr, match
      character*1 char
c
c     # internal return codes...
      integer   irok,   ireof,    ireor,    irup
      parameter(irok=0, ireof=-1, ireor=-2, irup=-3)
c
      integer  ifnch, ifchl
      external ifnch, ifchl
c
      ierr  = 0
      match = 0
c
100   continue
      if(l1.gt.linlen)then
         l1=1
         read(nunit,'(a)',iostat=ierr)linbuf
         if(ierr.lt.0)then
            ircode=ireof
            return
         elseif(ierr.gt.0)then
c           # return external error code.
            ircode=ierr
            return
         endif
      endif
c
c     # get the initial character...
      i=ifnch(linbuf(l1:),' ')
      if(i.eq.0)then
         l1=linlen+1
         go to 100
      endif
      l1=l1+i-1
c
c     # check for special characters...
      char=linbuf(l1:l1)
      if(char.eq.'/')then
         ircode=ireor
         return
      elseif(char.eq.'^')then
         ircode=irup
         return
      endif
c
c     # get the final character position...
      i = ifchl(linbuf(l1:),' ,/^',match)
      if(match.eq.0)then
         l2=linlen
         l1n=l2+1
      elseif(match.eq.1)then
         l2=l1+i-2
         lx=l2+ifnch(linbuf(l2+1:),' ')
         if(lx.eq.l2)then
            l1n=linlen+1
         elseif(linbuf(lx:lx).eq.',')then
            l1n=lx+1
         else
            l1n=lx
         endif
      elseif(match.eq.2)then
         l2=l1+i-2
         l1n=l2+2
      elseif(match.eq.3 .or. match.eq.4)then
         l2=l1+i-2
         l1n=l2+1
      endif
      ircode=irok
      return
      end


      integer function ichtoi( char, ierr )
c
c  convert packed character string with optional sign to
c  an integer value.
c
c  ierr = 0 for normal return.
c       = location of nondigit character for error.
c
c  this version does not use an internal read in order to return
c  a useful ierr.  -rls
c
       implicit none 
       character*(*) char
      integer ierr
      logical negate
c
      integer i1, i, j
      character*10 cdigts
      parameter(cdigts='0123456789')
c
      if ( char(1:1) .eq. '+' ) then
         i1 = 2
         negate = .false.
      elseif ( char(1:1) .eq. '-' ) then
         i1 = 2
         negate = .true.
      else
         i1 = 1
         negate = .false.
      endif
      ierr   = 0
      ichtoi = 0
      do 100 i = i1, len(char)
         j = index(cdigts,char(i:i))
         if ( j .eq. 0 ) then
            ierr = i
            return
         else
            ichtoi = ichtoi * 10 + (j-1)
         endif
100   continue
      if ( negate ) ichtoi = -ichtoi
      return
      end
