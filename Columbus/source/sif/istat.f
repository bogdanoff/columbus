!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
cistat.f
cistat part=1 of 1.  integral file statistics program
cversion=1.0 last modified: 08-oct-90
cversion 5.0
      program istat
c
c  read an integral file and compute various statistics.
c
c  08-oct-90 (columbus day) 1-e fcore change. -rls
c  06-oct-90 fsplit=2 capability added. -rls
c  11-aug-89 siftyp() call added. -rls
c  08-aug-89 SIFS version. -rls
c  written by ron shepard.
c
      implicit integer(a-z)
c
      integer   lenbuf,       n1eabf,       n2eabf,       nbfmxp
      parameter(lenbuf=32768, n1eabf=30000, n2eabf=30000, nbfmxp=510)
c
c     # bitvector length must be rounded up to a multiple of 64.
      integer   lenbv
      parameter(lenbv=((n2eabf+63)/64)*64)
c
      character*80 title(20)
      integer nbpsy(8)
      real*8 buffer(lenbuf)
      integer ilab1(2,n1eabf),ilab2(4,n2eabf)
      equivalence (ilab1,ilab2)
      real*8 val1(n1eabf),val2(n2eabf)
      equivalence (val1,val2)
      integer ibitv(lenbv)
      integer ietype(20),info(10)
      integer imtype(1),map(1)
      real*8 energy(20)
      character*4 slabel(8)
      character*8 bfnlab(nbfmxp)
      character*60 fname
c
      integer nin, nlist, aoints, ntitle, ierr, nmap, nenrgy,
     & ninfo, nbft, nsym, i, idummy
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      real*8   sifsce
      external sifsce
c
      nin   = 5
      nlist = 6
      aoints= 10
c
*@ifdef future
*C  open nlist here if necessary.
*@else
c  assume preconnected unit.
*@endif
c
      call ibummr(nlist)
c
      write(nlist,6010)
6010  format(/' program "istat" 1.0b3'
     & //' written by ron shepard'
     & /' version date: 08-oct-90')
c
      write(nlist,*) 'Enter filename1'
      read(5,*) fname
c     fname = 'aoints'
      call trnfln( 1, fname )
      open(unit=aoints,file=fname,form='unformatted',status='old')
c
c     # read the header info.
c
      call sifrh1( aoints, ntitle, nsym, nbft,
     & ninfo, nenrgy, nmap, ierr )
c
      if(ierr.ne.0)then
         call bummer('istat: ierr=',ierr,faterr)
      elseif(ntitle.gt.20)then
         call bummer('istat: ntitle=',ntitle,faterr)
      elseif(nbft.gt.nbfmxp)then
         call bummer('istat: nbft=',nbft,faterr)
      elseif(ninfo.gt.10)then
         call bummer('istat: ninfo=',ninfo,faterr)
      elseif(nenrgy.gt.10)then
         call bummer('istat: nenrgy=',nenrgy,faterr)
      endif
c
c     # ignore map vectors, if any...
      nmap = 0
      call sifrh2( aoints, ntitle, nsym, nbft,
     & ninfo, nenrgy, nmap, title,
     & nbpsy, slabel, info, bfnlab,
     & ietype, energy, imtype, map,    ierr )
      if ( ierr.ne.0 ) call bummer('istat: ierr=',ierr,faterr)
c
      write(nlist,6020)(title(i),i=1,ntitle)
6020  format(/' file header information:'/(1x,a))
c
      write(nlist,6030)
6030  format(/' core energies:')
      call sifpre( nlist, nenrgy, energy, ietype )
c
      write(nlist,6032) sifsce(nenrgy, energy, ietype )
6032  format(/' total core energy =',1pe20.12)
c
      write(nlist,6040) nsym, nbft
6040  format(/' nsym =',i2,' nbft=',i4/)
      write(nlist,6050)'symmetry  =',(i,i=1,nsym)
      write(nlist,6060)'slabel(*) =',(slabel(i),i=1,nsym)
      write(nlist,6050)'nbpsy(*)  =',(nbpsy(i),i=1,nsym)
6050  format(1x,a,t15,8i5)
6060  format(1x,a,t15,8(1x,a4))
      write(nlist,6070)(info(i),i=1,ninfo)
6070  format(/' info(*)=',10i6)
      write(nlist,6080)(i,bfnlab(i),i=1,nbft)
6080  format(/' i:bfnlab(i) ='/ (5(i4,':',a8)) )
c
c     # check record length parameters...
c
      if(info(1).ne.1 .and. info(1).ne.2 )then
         call bummer('istat: fsplit=',info(1),faterr)
      elseif(info(2).gt.lenbuf)then
         call bummer('istat: l1rec=',info(2),faterr)
      elseif(info(3).gt.n1eabf)then
         call bummer('istat: n1max=',info(3),faterr)
      elseif(info(4).gt.lenbuf)then
         call bummer('istat: l2rec=',info(4),faterr)
      elseif(info(5).gt.n2eabf)then
         call bummer('istat: n2max=',info(5),faterr)
      endif
c
c     # check the 1-e integrals and compute statistics.
c
      call chk1e( nlist, aoints, info, buffer, ilab1, val1 )
c
c     # open the 2-e file.
      write(nlist,*) 'Enter filename2'
      read(5,*) fname
c     fname = 'aoints2'
      call trnfln( 1, fname )
      call sifo2f( aoints, aoints, fname, info, idummy, ierr )
      if ( ierr .ne. 0 ) then
         call bummer('istat: from sifo2f, ierr=',ierr,faterr)
      endif
c
c     # check the 2-e integrals.
c
      call chk2e( nlist, aoints, info, buffer, ilab2, val2, ibitv )
c
      call sifc2f( aoints, info, ierr )
      if ( ierr .ne. 0 ) then
         call bummer('istat: from sifc2f, ierr=',ierr,faterr)
      endif
      close (unit = aoints)
c
*@ifdef unix
c     # this call is to keep the stop statement from writing to the
c     # listing file on braindamaged  machines.
c       call exit(0)
cfp the bummer is important since the program is included in runc
*@endif
c
      call bummer('normal termination',0,3)
      stop
      end
c deck chk1e
      subroutine chk1e( nlist, ntape, info, buf, ilab, val )
c
c  read the 1-e integrals and write out statistics.
c
c  11-aug-89 siftyp() call added. -rls
c  08-aug-89 SIFS version. -rls
c  written by ron shepard.
c
      implicit integer(a-z)
c
      integer   nipv,   msame,   nmsame
      parameter(nipv=2, msame=0, nmsame=1)
c
      integer nlist, ntape
      real*8 buf(*),val(*)
      integer info(*),ilab(nipv,*)
c
      integer nrec, ntot, ierr, ibvtyp, ifmt, itypea, itypeb, last, num,
     & i, p, q
      integer   iretbv
      parameter(iretbv=0)
      integer ibitv(1)
c
      real*8 fcore
      real*8 sum(5,3), x, x2, w1, w2, denom
c
      character*8 chrtyp
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      real*8    one
      parameter(one=1d0)
c
100   continue
         call wzero( 5*3, sum, 1 )
         nrec = 0
         ntot = 0
200      continue
            call sifrd1( ntape, info, nipv, iretbv,
     &       buf, num, last, itypea,
     &       itypeb, ibvtyp, val,
     &       ilab, fcore, ibitv, ierr )
            if ( ierr.ne.0 ) call bummer(
     &       'istat: sifrd1 ierr=',ierr,faterr)
            nrec = nrec+1
            ntot = ntot+num
            do 300 i = 1,num
               p = ilab(1,i)
               q = ilab(2,i)
               x = val(i)
               x2 = x*x
               denom = one+x2
               w1 = p+q
               w2 = abs(p-q)
               sum(1,1) = sum(1,1)+x
               sum(2,1) = sum(2,1)+abs(x)
               sum(3,1) = sum(3,1)+x2
               sum(4,1) = sum(4,1)+x/denom
               sum(5,1) = sum(5,1)+x2/denom
               sum(1,2) = sum(1,2)+w1*x
               sum(2,2) = sum(2,2)+w1*abs(x)
               sum(3,2) = sum(3,2)+w1*x2
               sum(4,2) = sum(4,2)+w1*x/denom
               sum(5,2) = sum(5,2)+w1*x2/denom
               sum(1,3) = sum(1,3)+w2*x
               sum(2,3) = sum(2,3)+w2*abs(x)
               sum(3,3) = sum(3,3)+w2*x2
               sum(4,3) = sum(4,3)+w2*x/denom
               sum(5,3) = sum(5,3)+w2*x2/denom
300         continue
         if ( last .eq. msame ) go to 200
c
         call siftyp( itypea, itypeb, chrtyp )
         write(nlist,'(/t20,a,/t16,a,1pe15.7,a,i10,a,i7)')
     &    '=== statistics for '//chrtyp//' integrals ===',
     &    'fcore=', fcore, ' ntot=', ntot, ' nrec=', nrec
         write(nlist,'(1x,1p,5e15.7)') sum
c
c     # check for more 1-e integrals...
c
      if(last.eq.nmsame)go to 100
c
      return
      end
c deck chk2e
      subroutine chk2e( nlist, ntape, info, buf, ilab, val, ibitv )
c
c  read the 2-e integrals and write out statistics.
c
c  09-dec-90 ibitv(*) added.
c  11-aug-89 siftyp() call added. -rls
c  08-aug-89 SIFS version. -rls
c  written by ron shepard.
c
      implicit integer(a-z)
c
      integer   nipv,   msame
      parameter(nipv=4, msame=0)
c
      integer nlist, ntape
      real*8 buf(*),val(*)
      integer info(*), ilab(nipv,*), ibitv(*)
c
      integer   iretbv
      parameter(iretbv=-1)
c
      integer reqnum, ierr, nrec, ntot, num, ibvtyp,
     & ifmt, itypea, itypeb, last, i, p, q, r, s, ibtsum
c
      real*8 sum(5,3), x, x2, w1, w2, denom
c
      character*8 chrtyp
c
      real*8    one
      parameter(one=1d0)
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
c     # this routine is written to use asynchronous I/O.  This is not
c     # particularly important in this program for efficiency, but the
c     # structure of this code may be used as a template for other
c     # electronic structure codes.  -rls
c
c     # initiate the read of the first record.
      call sifr2( ntape, 0, info, buf, reqnum, ierr )
      if ( ierr.ne.0 ) call bummer('istat: sifr2 ierr=',ierr,faterr)
c
      call wzero( 5*3, sum, 1 )
      ibtsum = 0
      nrec   = 0
      ntot   = 0
100   continue
c
c        # make sure the buffer is ready.
         call sif2w8( ntape, info, reqnum, ierr )
         if ( ierr.ne.0 ) call bummer(
     &    'istat: sif2w8 ierr=',ierr,faterr)
c
c        # decode the record, reading any bit-vector.
         call sifd2( info, nipv, iretbv, buf,
     &    num, last, itypea, itypeb,
     &    ibvtyp, val, ilab,
     &    ibitv, ierr )
         if ( ierr .ne. 0 ) then
            call bummer('istat: from sifd2, ierr=',ierr,faterr)
         endif
         if ( last .eq. msame ) then
c           # initiate the next read while the current buffer is
c           # being processed.
            call sifr2( ntape, 0, info, buf, reqnum, ierr )
            if ( ierr.ne.0 ) call bummer(
     &       'istat: sif2w8 ierr=',ierr,faterr)
         endif
         nrec = nrec + 1
         ntot = ntot + num
         do 200 i = 1, num
            p = ilab(1,i)
            q = ilab(2,i)
            r = ilab(3,i)
            s = ilab(4,i)
            x = val(i)
            x2 = x*x
            denom = one+x2
            w1 = p+q+r+s
            w2 = abs(abs(p-q)-abs(r-s))
            sum(1,1) = sum(1,1)+x
            sum(2,1) = sum(2,1)+abs(x)
            sum(3,1) = sum(3,1)+x2
            sum(4,1) = sum(4,1)+x/denom
            sum(5,1) = sum(5,1)+x2/denom
            sum(1,2) = sum(1,2)+w1*x
            sum(2,2) = sum(2,2)+w1*abs(x)
            sum(3,2) = sum(3,2)+w1*x2
            sum(4,2) = sum(4,2)+w1*x/denom
            sum(5,2) = sum(5,2)+w1*x2/denom
            sum(1,3) = sum(1,3)+w2*x
            sum(2,3) = sum(2,3)+w2*abs(x)
            sum(3,3) = sum(3,3)+w2*x2
            sum(4,3) = sum(4,3)+w2*x/denom
            sum(5,3) = sum(5,3)+w2*x2/denom
200      continue
         if ( ibvtyp .ne. 0 ) then
c           # compute the sum of the bit-vector elements.
            do 220 i = 1, num
               ibtsum = ibtsum + ibitv(i)
220         continue
         endif
      if ( last .eq. msame ) go to 100
c
      call siftyp( itypea, itypeb, chrtyp )
      write(nlist,'(/t20,a/t18,a,i10,a,i7,a,i10)')
     & '=== statistics for '//chrtyp//' integrals ===',
     & ' ntot=', ntot,' nrec=',nrec,' ibtsum=',ibtsum
      write(nlist,'(1x,1p,5e15.7)') sum
c
      return
      end
