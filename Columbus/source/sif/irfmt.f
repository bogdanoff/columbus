!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
cirfmt.f
cirfmt part=1 of 1.  formatted integral file utility program
cversion=1.0b2 last modified: 04-may-92
cversion 5.0
c
      program irfmt
c
c  read a formatted version of an SIFS integral file and write
c  a standard unformatted version.
c
c  version 1.0b2 04-may-92
c
c  version history:
c  04-may-92 updated based on latest sifs library routines. -rls
c  21-oct-90 written by ron shepard, based on program iwfmt.
c
       implicit none
      integer   nbfmxp,     nengmx,    nmapmx
      parameter(nbfmxp=510, nengmx=20, nmapmx=10)
c
      character*80 title(20)
      integer nbpsy(8)
      integer ietype(nengmx), info(10)
      integer imtype(nmapmx), map(nbfmxp*nmapmx)
      real*8 energy(nengmx)
      character*4 slabel(8)
      character*8 bfnlab(nbfmxp)
      character*60 fname
c
      integer   lencor
      parameter(lencor=20000)
      real*8 core(lencor)
c
      integer aoints, buf, i, ibitv, idummy, ierr, ilab, itotal,
     & lenbuf, nbft, nenrgy, nin, ninfo, nlist, nmap, nmax, nsym,
     & ntitle, val
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer  forbyt, atebyt
      external forbyt, atebyt
c
      nin   = 5
      nlist = 6
      aoints= 10
c
*@ifdef future
*C     # open nin and nlist here if necessary.
*@else
c     # assume preconnected units.
*@endif
c
      call ibummr(nlist)
c
c     # open the unformatted output file.
      fname = 'aoints'
      call trnfln( 1, fname )
      open(unit=aoints,file=fname,form='unformatted',status='unknown')
c
c     # read the header info.
c
      read(nin,*,iostat=ierr) ntitle, nsym, nbft, ninfo, nenrgy, nmap
c
      if ( ierr .ne. 0 ) then
         call bummer('irfmt: ierr=',ierr,faterr)
      elseif ( ntitle .gt. 20 ) then
         call bummer('irfmt: ntitle=',ntitle,faterr)
      elseif ( nbft .gt. nbfmxp ) then
         call bummer('irfmt: nbft=',nbft,faterr)
      elseif ( ninfo .gt. 10 ) then
         call bummer('irfmt: ninfo=',ninfo,faterr)
      elseif ( nenrgy .gt. nengmx ) then
         call bummer('irfmt: nenrgy=',nenrgy,faterr)
      elseif ( nmap .gt. nmapmx ) then
         call bummer('irfmt: nmap=',nmap,faterr)
      endif
c
c     # note: some of the following read statements assume fortran
c     # carriage control was used when writing the records.
c
      read(nin,6020)(title(i),i=1,ntitle)
6020  format(1x,a)
c
      read(nin,*) (nbpsy(i),i=1,nsym)
c
      read(nin,6030) (slabel(i),i=1,nsym)
6030  format(1x,8a5)
c
      read(nin,*) (info(i),i=1,ninfo)
c
      read(nin,6040) (bfnlab(i),i=1,nbft)
6040  format(1x,8a10)
c
      read(nin,*) (ietype(i),i=1,nenrgy)
c
      read(nin,*) (energy(i),i=1,nenrgy)
6050  format(1x,1p4e20.12)
c
      if ( nmap .gt. 0 ) then
         read(nin,*) ( imtype(i), i = 1, nmap )
         read(nin,*) ( map(i), i = 1, nmap*nbft )
      endif
c
      if ( (info(1) .ne. 1) .and. (info(1) .ne. 2) ) then
         call bummer('irfmt: fsplit=',info(1),faterr)
      endif
c
      call sifwh(
     & aoints, ntitle, nsym,   nbft,
     & ninfo,  nenrgy, nmap,   title,
     & nbpsy,  slabel, info,   bfnlab,
     & ietype, energy, imtype, map,
     & ierr )
      if ( ierr.ne.0 ) call bummer('from sifwh(), ierr=',ierr,faterr)
c
c     # transfer the 1-e integrals.
c     # core=>1:buffer,2:ilab1,3:val1,4:ibitv
c
      lenbuf = info(2)
      nmax   = info(3)
c
      buf    = 1
      ilab   = buf   + atebyt( lenbuf )
      val    = ilab  + forbyt( 2*nmax )
      ibitv  = val   + atebyt( nmax )
      itotal = ibitv + forbyt( ((nmax+63)/64)*64 ) -1
      if ( itotal .gt. lencor ) then
         call bummer( 'irfmt: out1e itotal=',itotal,faterr)
      endif
c
      call out1e(
     & nin,        aoints,    info,        core(buf),
     & core(ilab), core(val), core(ibitv) )
c
c     # open the output 2-e file.
      fname = 'aoints2'
      call trnfln( 1, fname )
      call sifo2f( aoints, aoints, fname, info, idummy, ierr )
      if ( ierr .ne. 0 ) then
         call bummer('irfmt: from sifo2f, ierr=',ierr,faterr)
      endif
c
c     # transfer the 2-e integrals.
c     # core=>1:buffer,2:ilab1,3:val1,4:ibitv
c
      lenbuf = info(4)
      nmax   = info(5)
c
      buf    = 1
      ilab   = buf   + atebyt( lenbuf )
      val    = ilab  + forbyt( 4*nmax )
      ibitv  = val   + atebyt( nmax )
      itotal = ibitv + forbyt( ((nmax+63)/64)*64 ) -1
      if ( itotal .gt. lencor ) then
         call bummer( 'irfmt: out2e itotal=',itotal,faterr)
      endif
c
      call out2e(
     & nin,        aoints,    info,       core(buf),
     & core(ilab), core(val), core(ibitv) )
c
      call sifc2f( aoints, info, ierr )
      if ( ierr .ne. 0 ) then
         call bummer('irfmt: from sifc2f, ierr=',ierr,faterr)
      endif
      close (unit = aoints)
c
      stop
      end
c deck out1e
      subroutine out1e(
     & nin,    ntape,  info,   buf,
     & ilab,   val,    ibitv )
c
c  read in formatted 1-e integral records and write out
c  unformatted SIFS integral records.
c
c  21-oct-90 written by ron shepard.
c
       implicit none
      integer   msame,   nmsame,   nomore
      parameter(msame=0, nmsame=1, nomore=2)
c
      integer nin, ntape
      real*8 buf(*), val(*)
      integer info(*), ilab(*), ibitv(*)
c
      integer last, ibvtyp, ierr, nipv, ifmt, itypea, itypeb, nrec, num
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer    iretbv
      parameter( iretbv=-1 )
c
      real*8 fcore
c
c     # note: this version assumes that the input and output
c     # record length parameters are identical.  if necessary,
c     # this routine will be generalized in the future to
c     # allow different input and output record length and
c     # record format parameters. -rls
c
      nrec = 0
      last = msame
200   if ( last .ne. nomore ) then
c
c        # formatted read.
         call siffr1(
     &    nin,    info,   nipv,   num,
     &    last,   itypea, itypeb, 
     &    ibvtyp, val,    ilab,   fcore,
     &    ibitv,  ierr )
c
         if ( ierr.ne.0 ) call bummer(
     &    'out1e(), from siffr1(), ierr=',ierr,faterr)
c
c        # unformatted write.
         call sifew1(
     &    ntape,  info,   nipv,   num,
     &    last,   itypea, itypeb, 
     &    ibvtyp, val,    ilab,   fcore,
     &    ibitv,  buf,    nrec,   ierr )
c
         if ( ierr.ne.0 ) call bummer(
     &    'out1e(), from sifew1(), ierr=',ierr,faterr)
c
         goto 200
      endif
c
      return
      end
c deck out2e
      subroutine out2e(
     & nin,    ntape,  info,   buf,
     & ilab,   val,    ibitv )
c
c  read in formatted 2-e integral records and write out
c  unformatted SIFS integral records.
c
c  21-oct-90 written by ron shepard.
c
       implicit none
      integer   msame,   nmsame,   nomore
      parameter(msame=0, nmsame=1, nomore=2)
c
      integer nin, ntape
      real*8 buf(*), val(*)
      integer info(*), ilab(*), ibitv(*)
c
      integer   iretbv
      parameter(iretbv=-1)
c
      integer    iwait
      parameter( iwait=1 )
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer ibvtyp, ierr, ifmt, itypea, itypeb, last, nipv,
     & num, nrec, reqnum
c
c     # note: this version assumes that the input and output
c     # record length parameters are identical.  if necessary,
c     # this routine will be generalized in the future to
c     # allow different input and output record length and
c     # record format parameters. -rls
c
      nrec = 0
      last = msame
200   if ( last .ne. nomore ) then
c
c        # formatted read.
         call siffr2(
     &    nin,    info,   nipv,   num,
     &    last,   itypea, itypeb, 
     &    ibvtyp, val,    ilab,   ibitv,
     &    ierr )
c
         if ( ierr .ne. 0 ) call bummer(
     &    'out2e(), from siffr2(), ierr=', ierr, faterr )
c
c        # unformatted write.
         call sifew2(
     &    ntape,  info,   nipv,   num,
     &    last,   itypea, itypeb, 
     &    ibvtyp, val,    ilab,   ibitv,
     &    buf,    iwait,  nrec,   reqnum,
     &    ierr )
c
         if ( ierr .ne. 0 ) call bummer(
     &    'out2e(), from siffw2(), ierr=', ierr, faterr )
c
         goto 200
      endif
c
      return
      end
