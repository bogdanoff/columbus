!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
cmofmt.f
cmofmt part=1 of 1.  binary-to-formatted orbital coefficient translation
cversion=2.1 last modified: 28-oct-96
cversion 5.0
      program mofmt
c
c  read the restart file created by program mcscf, search for the
c  appropriate labeled mo coefficients, and write them to the formatted
c  output file.
c
c  written by ron shepard.
c
       implicit none
cvp   integer         nlist, nin, nfres, nfout
cvp   common /cfiles/ nlist, nin, nfres, nfout
      integer         nlist, nin, nfres, nfout, nfout2
      common /cfiles/ nlist, nin, nfres, nfout, nfout2
      logical qfskip, qconvg
      integer nmpsy(8), nbpsy(8)
c
      integer    nbfmx,     ntitmx
      parameter( nbfmx=511, ntitmx=20 )
c
      real*8 c(nbfmx*nbfmx)
ca
      real*8 c1(nbfmx*nbfmx), c2(nbfmx)
      character*80 title(ntitmx)
      character*40 cfmt
      character*8 label
cvp   character*60 rfile,mfile
      character*60 rfile,mfile,nfile
c
      integer ntitle, ierr, nsym, ninf, ntol, nmot, nbmt, nst, ncsf,
     & i, nread
ca
      integer nprev, j, k
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
cvp
c  --- mowrit
      integer filerr, syserr
      integer nsize
      character*4 labels(8)
c      data labels/'SYM1','SYM2','SYM3','SYM4','SYM5','SYM6',
c     . 'SYM7','SYM8'/
cfp: if the correct labels are not read in, use at least labels
c     that are short enough to be displayed in Molekel
      data labels/'S1  ','S2  ','S3  ','S4  ','S5  ','S6  ',
     . 'S7  ','S8  '/

c
      data qfskip/.true./
cmd
      integer maxstat
      parameter (maxstat = 400)
      integer ncsf_f(maxstat)
cmd
c
      nin=5
      nlist=6
      nfres=20
      nfout=21
      nfout2=22
c
      call ibummr(nlist)
c
*@ifdef crayctss
*      call link("unit5=tty,unit6=unit5//")
*@elif defined  fps164
*      open(unit=nin,status='unknown',file='mofmtin')
*      open(unit=nlist,form='formatted',status='new',
*     &  file='mofmtls,sblock=1024,blocks=2,size=128',recl=133)
*@else
c     # use preconnected nin and nlist.
c     # on the vax, this is for005 (sys$input)and for006 (sys$output).
c     # on  machines, this is stdin and stdout.
*@endif
c
      write(nlist,6010)
6010  format(' program mofmt 2.1a3'/
     & ' read a set of labeled mo coefficients from an mcscf',
     & ' restart'/
     & ' file and write them to a formatted output file.'//
     & ' version date: 28-sep-91'//
     & ' programmed by: ron shepard'/)
c
      call who2c( 'mofmt', nlist )
c
c     # version log:
c     # 28-sep-91 minor cleanup for 4.1.1.0a2 distribution. -rls
c     # 27-jul-89 port to sun, alliant, titan and minor mods. -rls
c     # 15-jul-88 unicos version (rls).
c     # 12-apr-88 allcap() added for case independence, bummer() (rls).
c     # 22-mar-86 program mof2 with vax, cray, and fps mdc blocks.
c
c     # get file names:
c
      call inchst(rfile,'restart',
     & 'input a restart file name.')
      call inchst(mfile,'mocoef_mc',
     & 'input an mo coefficient file name')
c     call inchst(nfile,'nocoef_mc',
cvp  & 'input an mo coefficient file name')
      rfile='restart'
      mfile='mocoef_mc'
      nfile='nocoef_mc'
c
      call trnfln( 1, rfile )
      open(unit=nfres,file=rfile,form='unformatted',status='old')
c
      call trnfln( 1, mfile )
*@ifdef vax
*C     # must use nonstandard parameter to get the correct file type.
*      open(unit=nfout,file=mfile,form='formatted',status='new',
*     & carriagecontrol='list')
*@else
      open(unit=nfout,file=mfile,form='formatted',status='unknown')
      call trnfln( 1, nfile )
      open(unit=nfout2,file=nfile,form='formatted',status='unknown')
*@endif
      inquire(unit=nfres,name=rfile)
      inquire(unit=nfout,name=mfile)
      inquire(unit=nfout2,name=nfile)
c
      write(nlist,6100) '    restart file: ' // rfile
      write(nlist,6100) 'coefficient file: ' // mfile
c
c     # get the header info from the restart file.
c
      ntitle=0
      rewind nfres
      read(nfres,iostat=ierr)
     & nsym,   ninf,   ntol,   nmot,
     & nbmt,   nst, (ncsf_f(i),i=1,nst),   qconvg, ntitle,
     & nmpsy,  nbpsy,  (title(i),i=1,ntitle)
cmd
c     temporary omitting of state averaging
      ncsf = ncsf_f(1)
cmd
c
      if ( ierr .ne. 0 ) call bummer('mofmt: from input file, iostat=',
     & ierr,faterr)
c
      write(nlist,6100) 'restart file header info:',
     & (title(i),i=1,ntitle)
c
      write(nlist,6020) nsym, ninf, ntol, nmot, nbmt, ncsf, ntitle
6020  format(/' nsym=',i2,' ninf=',i3,' ntol=',i2,' nmot=',i3,
     & ' nbmt=',i6,' ncsf=',i8,' ntitle=',i2)
      write(nlist,6110) 'nmpsy(*)=', (nmpsy(i),i=1,nsym)
      write(nlist,6110) 'nbpsy(*)=', (nbpsy(i),i=1,nsym)
6110  format(1x,a,8i4)
c
      if ( qconvg ) then
         write(nlist,6100)'mcscf convergence criteria were satisfied.'
      else
         write(nlist,6100)
     &    'mcscf convergence criteria were not satisfied.'
      endif
c
      if ( nmot .gt. nbfmx ) then
         write(nlist,*)'nmot,nbfmx=',nmot,nbfmx
         call bummer('invalid nbfmx',nbfmx,faterr)
      endif
c
c     # read and print the convergence information.
c
      call rdlv( nfres, ninf, nread, c, 'coninf', qfskip )
      if ( ninf .ne. nread ) then
         call bummer('mofmt: incorrect ninf=',ninf,faterr)
      endif
c
      write(nlist,6030) (c(i), i=1,ninf )
6030  format(/' convergence info:'/(1x,1p8e16.8))
c
c     # get a label for the orbital coefficients.
c
      call inchst(label,'morb',
     & 'input an mo coefficient label.')
      write(nlist,6100)'mo coefficient label: ' // label
c
c     # search the restart file for label and get the orbitals.
c
      call rdlv( nfres, nbmt, nread, c1, label, qfskip )
      if ( nread .ne. nbmt ) then
         call bummer('mofmt: incorrect nread=', nread,faterr)
      endif
c
c     # get the output format.
c
      call inchst( cfmt, '(1p3d25.15)',
     & 'input a format for writing the orbitals.')
c
      write(nlist,6100)'output format: ' // cfmt
c     # write the orbital coefficients to the output file.
c
c     call wrtmof( cfmt, nsym, nmpsy, nbpsy, c1, c2 )
c
cvp for mocoef_mc file
c
      if ( ntitle .lt. ntitmx ) then
         ntitle = ntitle + 1
         title(ntitle)(1:32) = 'mofmt: formatted orbitals label='
         title(ntitle)(33:40) = label(1:8)
         call siftdy( title(ntitle)(41:80) )
         write(nlist,6100) title(ntitle)
      endif
      call mowrit(nfout,10,filerr,syserr,ntitle,title,cfmt,nsym,
     &     nbpsy,nmpsy,labels,c1)
      call mowrit(nfout,20,filerr,syserr,ntitle,title,cfmt,nsym,
     &     nbpsy,nmpsy,labels,c1)

      open (unit=80,file='mocoef_mc.lumorb')
      call wrtmof_molcas(80,
     .       nsym,nmpsy,nbpsy,c1,'mos','mocoef_mc')
c
c
c     # search the restart file for norb and occupation
c
c     # clear up c1 to be used again
c
      
      nsize=0
      do i=1,nsym
        nsize=nsize+nbpsy(i)*nmpsy(i)
      enddo
      call wzero( nsize,c1,1 )
cvp   call wzero( nbpsy*nmpsy,c1,1 )
cvp   call inchst(label,'norb',
cvp  & 'input an no coefficient label.')
c
cvp for nocoef_mc file
c
      label='norb'
      if ( ntitle .lt. ntitmx ) then
         title(ntitle)(1:32) = 'mofmt: formatted orbitals label='
         title(ntitle)(33:40) = label(1:8)
         call siftdy( title(ntitle)(41:80) )
         write(nlist,6100) title(ntitle)
      endif
      write(nlist,6100)'no coefficient label: ' // label
      call rdlv( nfres, nbmt, nread, c1, label, qfskip )
      if ( nread .ne. nbmt ) then
         call bummer('mofmt: incorrect nread=', nread,faterr)
      endif
      label='nocc'
      call rdlv( nfres, nmot, nread, c2, label, qfskip )
      if ( nread .ne. nmot ) then
         call bummer('mofmt: incorrect nread=', nread,faterr)
      endif
cvp     write(6,*) (c2(i),i=1,nmot)
cvp     write(6,*)
      do i=1,nsym
         nprev=0
         do j=1,i-1
            nprev=nprev+nmpsy(j)
         enddo
         write(6,*) nmpsy(i), nprev
         write (6,'(6f12.9)')  (c2(k),k=nprev+1,nprev+nmpsy(i))
      enddo
      call mowrit(nfout2,10,filerr,syserr,ntitle,title,cfmt,nsym,
     &     nbpsy,nmpsy,labels,c1)
      call mowrit(nfout2,20,filerr,syserr,ntitle,title,cfmt,nsym,
     &     nbpsy,nmpsy,labels,c1)
      call mowrit(nfout2,40,filerr,syserr,ntitle,title,cfmt,nsym,
     &     nbpsy,nmpsy,labels,c2)
8010  format(a)

      call wrtmof_molcas(80,
     .       nsym,nmpsy,nbpsy,c2,'occ','mocoef_mc')
      close (unit=80)
      open (unit=80,file='nocoef_mc.lumorb')
      call wrtmof_molcas(80,
     .       nsym,nmpsy,nbpsy,c1,'mos','nocoef_mc')
      call wrtmof_molcas(80,
     .       nsym,nmpsy,nbpsy,c2,'occ','nocoef_mc')
      close (unit=80)
c
c
      call bummer('normal termination',0,3)
      stop 'end of mofmt'
6100  format(1x,a)
      end
      subroutine rdlv( iunit, n, na, a, alab, qfskip )
c
c  search unit i for label alab and read the corresponding vector.
c
c  iunit  = unit number.
c  n      = expected vector length.
c  na     = actual vector length.  na=-1 for unsatisfied search.
c  a(*)   = if(na.le.n) the vector is returned.
c  alab   = character label used to locate the correct record.
c           first 8 characters only are used.
c  qfskip = .true. if first record should be skipped.
c
       implicit none
      integer         nlist, nin, nfres, nfout, nfout2
      common /cfiles/ nlist, nin, nfres, nfout, nfout2
c
c
      integer iunit, n, na
      logical qfskip
      real*8 a(n)
      character*(*) alab
c
      character*8 label, inlab
c
c     # convert input label to upper case for subsequent comparisons.
c
      inlab=alab
      call allcap( inlab )
c
c     # search for the correct label.
c
      na=0
      rewind iunit
      if ( qfskip ) read(iunit)
10    read(iunit,end=20) label, na
      call allcap( label )
      if ( inlab .ne. label ) then
         read(iunit)
         go to 10
      endif
c
c     # correct label.  check length.
c
      if ( na .ne. n ) write(nlist,6010) alab, na, n
6010  format(/' rdlv: alab=',a,' na,n=',2i10)
      if ( na .le. n ) call seqr( iunit, na, a )
      return
c
c     # unsatisfied search for alab.
20    write(nlist,6020) alab
6020  format(/' rdlv: record not found. alab=',a)
      na = -1
      return
      end
      subroutine inchst( chst, dchst, prompt )
c
c  read a character string.
c  if blank or eof then return default, dchst.
c
       implicit none
      integer         nlist, nin, nfres, nfout, nfout2
      common /cfiles/ nlist, nin, nfres, nfout, nfout2
c
      character*(*) chst, dchst, prompt
c
      integer nerr
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      nerr = -1
100   write(nlist,6010) prompt, dchst
6010  format(/1x,a,' default=',a)
      nerr = nerr + 1
      if ( nerr .ge. 10 ) then
         write(nlist,*)'inchst: *** error ***'
         call bummer('inchst: nerr=',nerr,faterr)
      endif
      chst = dchst
      read(nin,'(a)',err=100,end=500) chst
c
c     # return default if blank.
c
      if ( chst .eq. ' ' ) chst = dchst
      return
c
500   continue
c
c     # return default if end of file.
c
      chst = dchst
      return
c
      end
      subroutine seqr( iunit, n, a )
c
c     # sequential read of a(*) from unit iunit.
c
       implicit none 
       integer iunit, n
      real*8 a(n)
c
      read(iunit) a
c
      return
      end
