!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/

      program hernew
c  ********************************************************************
c  *   written by Hoechtl Peter, University of Vienna, May. 14. 1996  *
c  ********************************************************************
c  input files:
c               daltaoin
c               geom.unik
c  output file:
c               daltaoin.new
c
c  this program copies the coordinates for the atoms from
c  the geom-file in the daltaoin.new file. This new file remains
c  the same as the daltaoin, only the coordinates are changed.
c  The format is taken from hermit.doc.(and after some experiments
c  with an daltaoin  file)
      integer natmax
      parameter (natmax=255)
      character*72 title(3),dummy
      character*6  intgrl
      character*4  name
      character*3  symop(3)
      character*1  flag,onechar
c  -----------------------------
c  declarations of the variables
c  -----------------------------
      dimension ngrp(12),cont(8)
      integer ord(natmax)
      integer atomnr,geomnr,i,j,k,l,m,notyp,nsymop,nprim,ncont
      integer inp,out,geominp,maxl,number
      real*8  thr,charge,exp,cont
c  ----------------------------------
c  declarations of variables for geom
c  ----------------------------------
      character*2 atombez(natmax)
      real*8 gx(natmax),gy(natmax),gz(natmax)
c  ---------------------
c  set fileunitnumbers
c
c  inp     ... daltaoin
c  out     ... daltaoin.new
c  geominp ... geom.unik
c  ---------------------
      inp=5
      out=7
      geominp=8
c  ----------------------------------
c  read in coordinates from geom file
c  ----------------------------------
      atomnr=0
      open(unit=geominp,file='geom.unik',status='old')
c  ----------------------------------------------------
c  the informations of the first two lines are not used
c  ----------------------------------------------------
   1  format(a)
      read(geominp,1) dummy
      read(geominp,1) dummy
      do 270 i=1,natmax
        read(geominp,280,end=290) ord(i),gx(i),gy(i),gz(i)
 280  format (i5,3f14.8)
 270  continue
 290  i=i-1
      geomnr=i
      atomnr=0
      close(geominp)
c  -------------------------------
c  open the input and output files
c  -------------------------------
      open(unit=inp,file='daltaoin',status='old')
      open(unit=out,file='daltaoin.new',status='unknown')
c  -----------------------
c  read in the word INTGRL
c  -----------------------
      read(inp,5) intgrl
      write(out,5) intgrl
   5  format(a6)
c  ---------------------------------
c  read in two arbitrary title lines
c  ---------------------------------
      read(inp,1) title(1)
      read(inp,1) title(2)
      write(out,1) title(1)
      write(out,1) title(2)
c  ----------------------------------------
c  onechar ... one character, s = spherical AOs, c = cartesian aos
c  nontyp  ... number of atom types
c  nsymop  ... number of symmetry generators
c  isymop  ... symmetry generators
c  thr     ... threshold when neglecting the final integrals
c  ----------------------------------------
      read(inp,10) onechar,nontyp,nsymop,(symop(i),i=1,3),thr
      write(out,10) onechar,nontyp,nsymop,(symop(i),i=1,3),thr
  10  format(a1,i4,i5,3a3,d10.2)
c  ----------------------
c  loop over all atom types
c  ------------------------
      do 200 m=1,nontyp
c  ----------------------------------------------------------------
c  charge  ... charge on this atom type
c  number  ... number of symmetry distinct atoms of this type
c  maxl    ... maximum angular quantum number used in basis for
c              this atom type (s=1 etc.)
c  ngrp(i) ... number of groups of generally contracted function of
c              angular quantum number i
c  ----------------------------------------------------------------
        read(inp,20) charge,number,maxl,(ngrp(i),i=1,maxl)
        write(out,20) charge,number,maxl,(ngrp(i),i=1,maxl)
  20  format(6x,f4.1,14i5)
c  --------------------------------------------------
c  loop over all symmetry distinct atoms of each type
c  --------------------------------------------------
        do 190 k=1,number
c  -----------------------------------------------------------
c  name  ... Atom name
c  x,y,z ... coordinates of the atom in angstroem
c  flag  ... if flag is * then line is read in as (a4,3f20.15)
c            is flag a blank then read in line as (a4,3f10.5)
c  -----------------------------------------------------------
          read(inp,'(71x,a1)') flag
          backspace(inp)
          atomnr=atomnr+1
          if (flag.eq.'*') then
            read(inp,30) name,x,y,z,flag
            write(out,30) name,gx(atomnr),gy(atomnr),gz(atomnr),flag
          else
            read(inp,40) name,x,y,z,flag
            write(out,40) name,gx(atomnr),gy(atomnr),gz(atomnr)
          endif
  30  format(a4,3f20.15,7x,a1)
  40  format(a4,3f10.5)
190     continue
c  ---------------------------------------------
c  loop over all quantum number till the maximum
c  ---------------------------------------------
        do 170 k=1,maxl
c  ----------------------------------------------------------
c  loop over groups of gen. con. function of quantum number k
c  ----------------------------------------------------------
          do 160 l=1,ngrp(k)
c  ---------------------------------------------------------------
c  nprim ... number of primitive gaussians in this group, for this
c            angular moment
c  ncont ... number of contracted gaussians in this group
c  ---------------------------------------------------------------
            read(inp,50) onechar,nprim,ncont
            write(out,50) onechar,nprim,ncont
  50  format(a,2i4)
c  ----------------------------------------
c  loop over all primitives in one gaussian
c  ----------------------------------------
            do 180 j=1,nprim
              read(inp,*) exp, (cont(i),i=1,ncont)
              if (ncont.gt.3) then
                write(out,60) exp, (cont(i),i=1,3)
                write(out,61) (cont(i),i=4,ncont)
              else
                write(out,60) exp, (cont(i),i=1,ncont)
              endif
  60  format(f20.8,3f19.8)
  61  format(20x,3f19.8)
 180        continue
 160      continue
 170    continue
c
 200  continue
      close(inp)
      close(out)
      call bummer('normal termination',0,3)
      stop 'end of hernew'
      END

