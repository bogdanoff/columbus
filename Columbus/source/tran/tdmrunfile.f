!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
c===============================================================================
      program main
      use molcaswrapper
      implicit none

      call link_it
      call getenvinit
      call fioinit
      call NameRun('RUNFILE')
      call tdmrunfile

      call bummer('normal termination',0,3)
      stop 'end of tdmrunfile'
      end
c===============================================================================
      subroutine tdmrunfile
c  reads aodens and write the one-electron density matrix to molcas RUNFILE
      implicit none
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer ntitmx,maxsym,nengmx,ninfmx
      parameter(ntitmx=30,maxsym=8,nengmx=20,ninfmx=10)
c
      integer natom,nmotx
      parameter(natom=50,nmotx=1023)
c
      real*8 zero,two,halfm,half
      parameter (zero=0.0d+00,two=2.0d+00,halfm=-0.5d+00,half=0.5d+00)
c
      integer lcore
      parameter (lcore=nmotx*(nmotx+1)/2+3*(2*16))
c
c  ##  common block section
c
c  ##  integer section
c
      integer cietyp(nengmx),cpt(10)
      integer denfl,d1dim,denoff
      integer ierr,idummy,info(ninfmx),iorder,i,isym,itypea,
     &        itypeb,ijsym,icount,ind1,ind2,iind,imd,ipair
      integer jsym,jj,jcount,j,jind
      integer kntin(maxsym*(maxsym+1)/2)
      integer lenm1e,lenm2e,lasta,lastb,ll,lmda(maxsym,maxsym),
     &        lmdb(maxsym,maxsym)
      integer map(nmotx)
      integer ntitle, nsym,nmot,ninfo,nenrgy,nmap,nmpsy(maxsym),
     &        ndim,nndx(nmotx),n1embf,n2embf,nnmt,nrec,last,nm,
     &        nucdep,nmbpr(maxsym)
      integer offset(maxsym)
      integer ssym(nmotx),symoff(maxsym*(maxsym+1)/2)
      integer totsym
c
c  ##  real*8 section
c
      real*8 d1_buf(nmotx)
      real*8 core(lcore)
      real*8 fcore
      real*8 cienrg(nengmx)
c
c  ##  character section
c
      character*4 slabel(maxsym)
      character*8 cidlab(nmotx)
      character*80 title(ntitmx)
c
c  ##  logical setion
c
      logical lprint
c
c  ##  external setion
c
      integer forbyt, atebyt
      real*8 ddot_wr
      external forbyt, atebyt,ddot_wr
      logical lexist
c
      integer nndxf
      nndxf(i)= i*(i-1)/2
c
c-------------------------------------------------------------
c
c  initialize nndx(*), nmpsy(*), map(*), symoff(*)
c
      nndx(1)=0
      do i=2,nmotx
      nndx(i)=nndx(i-1)+(i-1)
      enddo ! end of: do i=2,nmotx
      do i=1,nmotx
      map(i)=i
      enddo ! end of: do i=1,nmotx
      call izero_wr(maxsym*(maxsym+1)/2,symoff,1)
      call izero_wr(maxsym,nmpsy,1)
c
      denfl=10
      inquire(file='aodens',exist=lexist)

      if (.not. lexist) then
        call bummer('missing aodens in tdmrunfile, ierr=',ierr,faterr) 
      endif 

      open(unit=denfl,file='aodens',status='old',
     & form='unformatted')
c
      call sifrh1(
     & denfl, ntitle, nsym, nmot,
     & ninfo, nenrgy, nmap, ierr)
       if (ierr.ne.0) call bummer(
     & '1:sifrh1:error in d1tr reading, ierr=',ierr,faterr)

c
c     #ignore map
      nmap = 0
      call sifrh2(
     & denfl,   ntitle,  nsym,    nmot,
     & ninfo,   nenrgy,  nmap,    title,
     & nmpsy,   slabel,  info,    cidlab,
     & cietyp,  cienrg,  idummy,  idummy,
     & ierr)
       if (ierr.ne.0) call bummer(
     & '2:sifrh2:error in d1tr reading, ierr=',ierr,faterr)

c
c  #  determine the symmetry pairs for the given symmetry group
      call symdet(nsym,nmpsy,lmda,lmdb,nmbpr)
c
c  #  initialize offset(*)
      nnmt=0
      do isym=1,maxsym
      offset(isym)=0
      nm=nmpsy(isym)
      nnmt = nnmt + nndx(nm+1)
      enddo ! end of: do isym=1,maxsym
      offset(1)=1
      do isym=2,nsym
      offset(isym)=offset(isym-1)+nmpsy(isym-1)
      enddo
c
c  #  initialize symoff(*),d1dim
      d1dim=0
      do isym=1,nsym
        do jsym=1,isym-1
        ijsym=nndxf(isym)+jsym
        symoff(ijsym)=d1dim
        d1dim=d1dim+nmpsy(isym)*nmpsy(jsym)
        enddo ! end of: do jsym=1,isym-1
      ijsym=nndxf(isym)+isym
      symoff(ijsym)=d1dim
      d1dim=d1dim+nmpsy(isym)*(nmpsy(isym)+1)/2
      enddo
c
      lenm1e = info(2)
      n1embf = info(3)
      lenm2e = info(4)
      n2embf = info(5)
c   # 1:d1tr_ao,2:buf,3:val,4:ilab,5:scr
      cpt(1)=1
      cpt(2)=cpt(1)+atebyt(d1dim)
      cpt(3)=cpt(2)+atebyt(lenm1e)
      cpt(4)=cpt(3)+atebyt(n1embf)
      cpt(5)=cpt(4)+forbyt(2*n1embf)
      if (cpt(5).gt.lcore) call bummer(
     & "Not enough memory allocated",cpt(5),faterr)
c
      itypea=2
      itypeb=9
      call wzero(d1dim,core(cpt(1)),1)
      call sifr1x(
     & denfl,        info,         itypea, itypeb,
     & nsym,         nmpsy,        symoff, core(cpt(2)),
     & core(cpt(3)), core(cpt(4)), map,    ssym,
     & nnmt,         core(cpt(1)), fcore,  kntin,
     & lasta,        lastb,        last,   nrec,
     & ierr)
       if (ierr.ne.0) call bummer(
     & '3:sifr1x:error in d1tr reading, ierr=',ierr,faterr)
c
c  ##  print out the 1-el antisymmetric transition density matrix
c     debug print
      lprint=.false.
      lprint=.true. 
      totsym=1
       if (lprint) then
       do ipair=1,nmbpr(totsym)
       isym=lmda(totsym,ipair)
       jsym=lmdb(totsym,ipair)
        if (isym.ne.jsym) then
         ijsym=nndxf(isym)+jsym
         i=symoff(ijsym)
           write(*,*)' offdiagonal density for symmetry',isym,jsym
           call prblkc('d1 offdiagonal block',
     &      core(cpt(1)+i),nmpsy(isym),nmpsy(isym),nmpsy(jsym),
     &      cidlab(offset(isym)),cidlab(offset(jsym)),1,6)
        else
         ijsym=nndxf(isym)+isym
         i=symoff(ijsym)
           write(*,*)' diagonal density for symmetry',isym,jsym
           call plblkc('d1_diagonal block',
     &      core(cpt(1)+i),nmpsy(isym),cidlab(offset(isym)),1,6)
          endif! end of:if (lprint) then
       enddo ! end of: do i=1,nmbpr(ssym)
       endif
c
      close(denfl)

      call put_darray('D1ao-',core(cpt(1)),d1dim)
c
 1010 format (3d15.6)

      return
      end
c
c***************************************************************
c
      subroutine symdet(nsym,nmpsy,lmda,lmdb,nmbpr)
c
      implicit none
c
      integer maxsym
      parameter(maxsym=8)
c
      integer i, is, ijs, ijsym,ix
      integer j, js
      integer lmda(maxsym,maxsym),lmdb(maxsym,maxsym)
      integer mult(maxsym,maxsym)
      integer nmbpr(maxsym),nmbj,nsym,nmpsy(maxsym)
      integer sym12(maxsym),sym21(maxsym)
c
      data sym12/1,2,3,4,5,6,7,8/
      data mult/
     & 1,2,3,4,5,6,7,8,
     & 2,1,4,3,6,5,8,7,
     & 3,4,1,2,7,8,5,6,
     & 4,3,2,1,8,7,6,5,
     & 5,6,7,8,1,2,3,4,
     & 6,5,8,7,2,1,4,3,
     & 7,8,5,6,3,4,1,2,
     & 8,7,6,5,4,3,2,1/
c
c------------------------------------------------------------
c
      call izero_wr(maxsym*maxsym,lmda,1)
      call izero_wr(maxsym*maxsym,lmdb,1)
      call izero_wr(maxsym,nmbpr,1)
c
      do i=1,nsym
      ix=sym12(i)
      sym21(ix)=i
      enddo ! end of: do i=1,nsym
c
      do i=1,nsym
        is=sym12(i)
        nmbj=0
         do j=1,nsym
         if(nmpsy(j).eq.0)go to 210
         js=sym12(j)
         ijs=mult(is,js)
         ijsym=sym21(ijs)
         if(nmpsy(ijsym).eq.0)go to 210
         if(ijsym.gt.j)go to 210
         nmbj=nmbj+1
         lmdb(i,nmbj)=j
         lmda(i,nmbj)=ijsym
  210    continue
         enddo ! end of: do j=1,nsym
      enddo ! end of: do i=1,nsym
c
      do i=1,nsym
         do j=1,nsym
          if (lmdb(i,j).gt.0) then
           if (nmpsy(lmdb(i,j))*nmpsy(lmda(i,j)).gt.0) then
           nmbpr(i)=nmbpr(i)+1
           endif
          endif
        enddo! end of: do j=1,nsym
      enddo! end of: do i=1,nsym
c
      write(*,*)' lmda,lmdb'
      do i=1,nsym
      write(*,30000)(lmdb(i,j),lmda(i,j),j=1,nsym)
      enddo ! end of: do i=1,nsym
      write(*,*)' nmbpr'
      write(*,30030)(nmbpr(i),i=1,nsym)
c
30000 format(1x,8(2i4,2x))
30030 format(1x,20i6)

      return
      end
c================================================================================
