#!/bin/bash
#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/

 for i in  0 1 2 3 4 
 do
  echo " &input " > tranin
  echo "  nextint=$i " >> tranin
  echo " &end   " >> tranin
  $COLUMBUS/tran.x 
  $COLUMBUS/istatmo.x <<EOF > istatmols.next$i
moints
moints
4 1
0
EOF

 done


