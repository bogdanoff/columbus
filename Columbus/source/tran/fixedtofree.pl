#!/usr/bin/perl
#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/


 $fin=shift @ARGV;

 open FIN,"<$fin" || die "could not open $fin";


 $saveline=<FIN>; 
 chop $saveline;
 while (<FIN>) 
 {  s/^[^0-9 \t\n\r\f]/!/;
   chop;
   if (/^     \S/) {$saveline=$saveline . " & "; s/^     \S/     & /;} 
   $currline=$_;
   print $saveline, "\n";
   $saveline=$currline;
 }

 print FOUT $saveline," \n";
 close FOUT;
 close FIN ;
 exit;
 
