Pseudo Basis(1s,1p,1d):  Hay and Wadt, JCP 82, 270 (1985). ncore=68
   3
   3  1  1
        0.3553000D+00      -0.1029813D+01
        0.2437000D+00       0.1076634D+01
        0.5830000D-01       0.8249345D+00
   3  2  1
        0.5100000D+00      -0.6002610D-01
        0.9800000D-01       0.4270969D+00
        0.2900000D-01       0.6860066D+00
   3  3  1
        0.1183000D+01       0.4003032D+00
        0.4492000D+00       0.5004758D+00
        0.1463000D+00       0.3036704D+00
Pseudo Basis(2s,2p,2d):  Hay and Wadt, JCP 82, 270 (1985). ncore=68
   6
   2  1  1
        0.3553000D+00      -0.4198072D+01
        0.2437000D+00       0.4388938D+01
   1  1  1
        0.5830000D-01       0.1000000D+01
   2  2  1
        0.5100000D+00      -0.1490279D+00
        0.9800000D-01       0.1060362D+01
   1  2  1
        0.2900000D-01       0.1000000D+01
   2  3  1
        0.1183000D+01       0.4852204D+00
        0.4492000D+00       0.6066428D+00
   1  3  1
        0.1463000D+00       0.1000000D+01
Pseudo Basis(2s,2p,1d):  Hay and Wadt, JCP 82, 299 (1985). ncore=60
   5
   3  1  1
        0.2222000D+01      -0.1653804D+01
        0.1496000D+01       0.2067030D+01
        0.4774000D+00       0.4232017D+00
   5  1  1
        0.2222000D+01       0.6928780D+00
        0.1496000D+01      -0.9551167D+00
        0.4774000D+00      -0.4879650D+00
        0.2437000D+00       0.5463999D+00
        0.5830000D-01       0.8604824D+00
   3  2  1
        0.2518000D+01      -0.4177345D+00
        0.1460000D+01       0.9434947D+00
        0.4923000D+00       0.4672976D+00
   3  2  1
        0.5100000D+00      -0.6002610D-01
        0.9800000D-01       0.4270969D+00
        0.2900000D-01       0.6860066D+00
   3  3  1
        0.1183000D+01       0.3943319D+00
        0.4492000D+00       0.4954809D+00
        0.1463000D+00       0.3175559D+00
Pseudo Basis(3s,3p,2d):  Hay and Wadt, JCP 82, 299 (1985). ncore=60
   8
   3  1  1
        0.2222000D+01      -0.1653804D+01
        0.1496000D+01       0.2067030D+01
        0.4774000D+00       0.4232017D+00
   4  1  1
        0.2222000D+01       0.1604697D+01
        0.1496000D+01      -0.2212039D+01
        0.4774000D+00      -0.1130121D+01
        0.2437000D+00       0.1265455D+01
   1  1  1
        0.5830000D-01       0.1000000D+01
   3  2  1
        0.2518000D+01      -0.4177345D+00
        0.1460000D+01       0.9434947D+00
        0.4923000D+00       0.4672976D+00
   2  2  1
        0.5100000D+00      -0.1490279D+00
        0.9800000D-01       0.1060362D+01
   1  2  1
        0.2900000D-01       0.1000000D+01
   2  3  1
        0.1183000D+01       0.4838470D+00
        0.4492000D+00       0.6079573D+00
   1  3  1
        0.1463000D+00       0.1000000D+01
