!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
ciargos2.f
cversion 5.8
c deck defso
      subroutine defso (
     & blkshft,  blshco, chi,      ctrans,
     & hiso,     icomp,  icsu,     ictu,
     & ideg,     igcs,   ihatmbas, ihiatm,
     & ihigcs,   ihiord, ihisym,   ihsamep,
     & ihsend,   iirep,  ipiv,     iprd,
     & iproj,    isamep, isend,    ishi,
     & isocoef,  lmnp1,  ltrans,   mcons,
     & mxang,    mxang1, mxang2,   mxang3,
     & mxao,     mxaov,  mxatm,    mxatmsym,
     & mxconatm, mxcons, mxhirep,  mxlorep,
     & mxorder,  mxso,   mxsoset,  nc,
     & nchi,     nchi1,  nf,       ngcs,
     & nhigcs,   nhirep, nhsamep,  nirep,
     & nop,      nsamep, nsymtp,   poly1,
     & poly2,    projao, soaox,    soaoy,
     & soao1,    soao1s, sox,      sqtol,
     & transao)
c
c  define LSOs and (if applicable) HSOs for hisym atom type ishi.
c
      implicit real*8 (a-h,o-z)
      parameter (thr8=1.0d-8)
      real*8 blkshft(mxaov,mxaov),blshco(nop),chi(mxhirep,mxorder),
     & ctrans(3,3,nop),hiso(mxaov,mxaov,mxsoset),poly1(mxang2),
     & poly2(mxang2),projao(mxaov,mxaov,mxhirep),soaox(mxaov,mxaov),
     & soaoy(mxaov,mxaov+1),soao1(mxaov,mxaov),soao1s(mxaov,mxaov),
     & sox(mxaov),transao(mxaov,mxaov)
      integer icomp(mxang3,mxang2,mxang),icsu(mxsoset),ictu(mxsoset),
     & ideg(nhirep),igcs(mxconatm,mxatm),ihatmbas(mxatm),ihiatm(mxatm),
     & ihigcs(mxconatm,mxatm),ihiord(mxaov,mxsoset),ihsamep(mxatm),
     & ihsend(mxatm,nop),iirep(mxso,mxsoset),ipiv(mxaov),
     & iprd(3,mxang1,mxang3),iproj(mxang2,mxatmsym,mxatmsym,mxlorep),
     & isamep(mxatm,mxatm),isend(mxatmsym,mxlorep,mxatm),
     & isocoef(mxao,mxso,mxsoset),lmnp1(mxcons),
     & ltrans(mxlorep+3,nirep),mcons(mxconatm,mxatm),nc(mxatm),
     & nf(mxatm),nsamep(mxatm),nsymtp(mxsoset)
c
c     # bummer error type.
      integer   faterr
      parameter(faterr=2)
c
      do icon=1,nf(ishi)
c        # define LSOs wherever possible
         jcon=mcons(icon,ishi)
         iam=lmnp1(jcon)
         iam2=iam*(iam+1)/2
         do is=ihatmbas(ishi)+1,ihatmbas(ishi)+nchi
            if (igcs(icon,is).le.0.and.iam2*nc(is).le.mxao) then
c              # try to find an existing set of suitable SO coefficients
               do jsamep=1,nsamep(is) ! loop over atoms with same permut
                  js=isamep(jsamep,is)
                  jshi=ihiatm(js)
                  do icon1=1,nf(jshi) ! loop over sets on other atom
                     jcon1=mcons(icon1,jshi)
                     if (lmnp1(jcon1) .eq. iam .and.
     &                igcs(icon1,js) .gt. 0) then
                        igcs(icon,is)=igcs(icon1,js)
                        goto 74
                     endif
                  enddo
               enddo
c              # form new SO transformation matrix
               if (ngcs.lt.mxsoset) then
                  ngcs=ngcs+1
                  igcs(icon,is)=ngcs
                  ictu(ngcs)=nc(is)*iam2
                  icsu(ngcs)=ictu(ngcs)
                  do j=1,icsu(ngcs)
                     do i=1,ictu(ngcs)
                        isocoef(i,j,ngcs)=0
                     enddo
                  enddo
                  do iatm=0,nc(is)-1
                     do jatm=0,nc(is)-1
                        ix=iand(iatm,jatm)
                        jx=1
                        if (ix .eq. 1 .or. ix .eq. 2 .or. ix .eq. 4
     &                   .or. ix .eq. 7) jx=-1
                        do j=1,iam2
                           isocoef(iatm*iam2+j,jatm*iam2+j,ngcs)=jx
                        enddo
                     enddo
                  enddo
c                 # determine the symmetries of the new LSOs
                  call loprj (
     &             iam,icomp,iproj,isend(1,1,is),ltrans,mxang,
     &             mxang2,mxang3,mxatmsym,mxlorep,nc(is),nirep)
                  do iso=1,icsu(ngcs)
                     iirep(iso,ngcs)=ifrep(iam2,iproj,
     &                isocoef(1,iso,ngcs),mxang2,mxatmsym,nc(is),nirep)
                     if (iirep(iso,ngcs).eq.0) then
                        write (6,*) 'An SO does not belong to an irrep!'
                        write (6,*)
     &                   'Contact the author of this software.'
                        call bummer ('exiting ...',0,faterr)
                     endif
                  enddo
               endif
            endif
74          continue
         enddo
c        # define HSOs wherever possible
         if (ihigcs(icon,ishi).le.0) then
            do is=ihatmbas(ishi)+1,ihatmbas(ishi)+nchi
               if (igcs(icon,is).le.0) goto 88 ! can't define HSOs
            enddo
c           # try to find an existing suitable set of HSO coefficients
            do jhsamep=1,nhsamep
               jshi=ihsamep(jhsamep)
               do icon1=1,nf(jshi)
                  if (ihigcs(icon1,jshi).gt.0) then
                     do is1=1,nchi
                        is=is1+ihatmbas(ishi)
                        js=is1+ihatmbas(jshi)
                        if (igcs(icon,is).ne.igcs(icon1,js)) goto 81
                     enddo
                     ihigcs(icon,ishi)=ihigcs(icon1,jshi) ! found one
                     goto 88
                  endif
81                continue
               enddo
            enddo
c           # convert LSO coefficients to real*8 and take the sqrt
            isobas=0
            iaobas=0
            nhiao=iam2*nchi1
            call wzero (mxaov*mxaov,soaox,1)
            call wzero (mxaov*mxaov,soao1,1)
            do i=1,nhiao
               soao1(i,i)=1     ! identity matrix
            enddo
            do is=ihatmbas(ishi)+1,ihatmbas(ishi)+nchi
               jgcs=igcs(icon,is)
               do iso=1,icsu(jgcs)
                  do iao=1,ictu(jgcs)
                     x=(isocoef(iao,iso,jgcs))
                     y=sign(sqrt(abs(x)),x)
                     soaox(iao+iaobas,iso+isobas)=y
                     soaoy(iao+iaobas,iso+isobas)=y ! soaoy is scratch
                  enddo
                  do iao=1,iaobas
                     soaoy(iao,iso+isobas)=0
                  enddo
               enddo
c              # invert the SOAO matrix
               ison=isobas+icsu(jgcs)
               iaon=iaobas+ictu(jgcs)
               call invert (iaobas,iaon,iflag,isobas,ison,mxaov,soaoy,
     &          soao1,soao1s)
               if (iflag.ne.0) then
                  ihigcs(icon,ishi)=-1
                  goto 88
               endif
               isobas=ison
               iaobas=iaon
            enddo
            ihigcs(icon,ishi)=0
            if (ihisym.ne.0.and.nhigcs.lt.mxsoset) then
               call frmhso (
     &          blkshft,            blshco, chi,     ctrans,
     &          iam,                icomp,  ideg,    iflag,
     &          ihiord(1,nhigcs+1), ihsend, ipiv,    iprd,
     &          mxang,              mxang1, mxang2,  mxang3,
     &          mxaov,              mxatm,  mxhirep, nchi1,
     &          nhirep,             ison,   nop,     poly1,
     &          poly2,              projao, soaox,   soaoy,
     &          soao1,              sox,    sqtol,   transao)
               if (iflag.eq.0) then
                  nhigcs=nhigcs+1
                  ihigcs(icon,ishi)=nhigcs
                  nsymtp(nhigcs)=ison
c                 # square magnitudes of HSOs and normalize the
c                 # smallest nonzero component to 1
                  do j=1,ison
                     small=1000
                     do k=1,ison
                        x=soaoy(k,j)
                        if (abs(x).gt.thr8) then
                           hiso(k,j,nhigcs)=x*abs(x)
                           small=min(small,x*x)
                        else
                           hiso(k,j,nhigcs)=0
                        endif
                     enddo
                     do k=1,ison
                        hiso(k,j,nhigcs)=hiso(k,j,nhigcs)/small
                     enddo
                  enddo
               endif
            endif
         endif
88       continue
      enddo
      return
      end
c deck edtecp
      subroutine edtecp (
     & am,     ccr,   chg,    cls,
     & ishi,   izcr,  lcru,   libdir,
     & llsu,   mcrs,  mtype,  mxang,
     & mxang3, mxatm, mxbfcr, mxcrs,
     & nbfcr,  nbfls, ncr,    ncrs,
     & nls,    nshi,  zcr,    zls)
c
c  drive menu for editing pseudopotentials
c
      implicit real*8 (a-h,o-z)
      real*8 ccr(mxbfcr,0:mxang3,mxcrs),chg(mxatm),
     & cls(mxbfcr,mxang,mxcrs),zcr(mxbfcr,0:mxang3,mxcrs),
     & zls(mxbfcr,mxang,mxcrs)
      integer izcr(mxcrs),mcrs(mxatm),lcru(mxcrs),llsu(mxcrs),
     & nbfcr(0:mxang3,mxcrs),nbfls(mxang,mxcrs),
     & ncr(mxbfcr,0:mxang3,mxcrs),nls(mxbfcr,mxang,mxcrs)
      character am(10)
      character*3 mtype(mxatm)
      character*8 option
      character*16 cnum
      character*72 title
      character*130 libdir
c
      integer clen
      character*130 libfile, newlib
      logical fopen, fexist
c
      logical  compabb
      external compabb
c
      integer  fstrlen
      external fstrlen
c
      write (6,*)
224   call getint ('For which atom do you want to edit the '//
     & 'pseudopotential? <return to menu>',ishi,'$')
      if (ishi.eq.1000000) return
      if (ishi.le.0.or.ishi.gt.nshi) goto 224
359   continue
c     # generate an ecp filename from libdir and the atomic symbol.
      llibd   = fstrlen( libdir )
      clen    = fstrlen( mtype(ishi) )
      libfile = libdir(1:llibd) // mtype(ishi)(1:clen) // '.ecp'
360   continue
      write (6,*)
      if (mcrs(ishi).eq.0) then
         write (6,*) 'This atom currently has no pseudopotential.'
      else
         write (6,*) 'The pseudopotential for this atom contains'
         write (6,*) 'angular momentum terms up to ',
     &    am(lcru(mcrs(ishi))+1)
      endif
      write (6,*)
      write (6,*) 'Enter one of the following options:'
      if (mcrs(ishi).le.0) then
         write (6,*)   '  LIBRARY:  add ',
     &    'pseudopotential to this atom from a library'
         if (nshi.gt.1) then
            write (6,*) '  OTHER:    add pseudopotential ',
     &       'from another atom in this molecule'
         endif
         write (6,*)   '  KEYBOARD: add pseudopotential from keyboard'
      else
         write (6,*)   '  EXPORT    pseudopotential on this atom to ',
     &    'new or existing library'
         write (6,*)   '  VIEW      pseudopotential on this atom'
         write (6,*)   '  DELETE    pseudopotential on this atom'
      endif
      write (6,*)     '  PROCEED'
      write (6,*)     '  QUIT'
      write (6,*)
361   continue
      if (mcrs(ishi).eq.0) then
         call getchr ('Which?',option,'LIBRARY')
      else
         call getchr ('Which?',option,'PROCEED')
      endif
      if (compabb(option,'LIBRARY',1).and.mcrs(ishi).eq.0) then
c        # present list of titles in library
358      continue
         write (6,*)
         write (6,*) 'Pseudopotential library ',libfile
         nbas=0
         inquire (unit=2,opened=fopen)
         if (.not.fopen) then
            inquire (file=libfile,exist=fexist)
            if (.not.fexist) goto 349
            open (unit=2, file=libfile, status='old', err=349)
         else
            rewind (unit=2)
         endif
371      continue
         read (2,'(a)',err=349,end=348) title
         if (title.eq.' ') goto 371
         nbas=nbas+1
         write (6,"(i3,':  ',a)") nbas,title
         read (2,*,err=349,end=349) lcru1,llsu1
         do l=0,lcru1
            read (2,*,end=349,err=349) nbfcrx
            if (nbfcrx.gt.mxbfcr) goto 349
            do k=1,nbfcrx
               read (2,*,end=349,err=349) idum,xdum,ydum
            enddo
         enddo
         do l=1,llsu1
            read (2,*,end=349,err=349) nbfcrx
            if (nbfcrx.gt.mxbfcr) goto 349
            do k=1,nbfcrx
               read (2,*,end=349,err=349) idum,xdum,ydum
            enddo
         enddo
         if (lcru1.gt.0) read (2,*,end=349,err=349) idum
         goto 371
349      continue
         nbas=0
348      continue
         if (nbas.gt.0) then
            write (6,"(i3,':  ',a)") 0,'Use other library'
            write (6,*)
345         call getint ('Which one? <return to menu>',ibas,'$')
            if (ibas.eq.1000000) goto 360
            if (ibas.lt.0.or.ibas.gt.nbas) goto 345
         else
            write (6,*)
            write (6,*) '       This library file is'
            write (6,*) '*** EMPTY, ABSENT, or DEFECTIVE ***'
            write (6,*)
            write (6,*) 'The default directory for basis set'
            write (6,*) 'libraries may be incorrect.  To override it,'
            write (6,*) 'set the environment variable COL_BASISLIBDIR'
            write (6,*) 'to the correct directory name.'
            write (6,*)
            ibas=0              ! force choice of other library
         endif
         if (ibas.eq.0) then    ! user wants other library
            inquire (unit=2,opened=fopen)
            if (fopen) close (unit=2)
            libfile=' '
            call getitle (
     &       'Enter new library filename: <return to menu>',libfile)
            if (libfile.eq.' ') goto 359
            goto 358
         endif
c        # reread library up to desired core potential
         rewind (unit=2)
         do jbas=1,ibas-1
            read (2,*)
            read (2,*) lcru1,llsu1
            do l=0,lcru1
               read (2,*) nbfcrx
               do k=1,nbfcrx
                  read (2,*)
               enddo
            enddo
            do l=1,llsu1
               read (2,*) nbfcrx
               do k=1,nbfcrx
                  read (2,*)
               enddo
            enddo
            if (lcru1.gt.0) read (2,*) idum
         enddo
         ncrs=ncrs+1
c        # read in desired core potential
ctm falls es sich um eine ano-Basis handelt frage die Kontraktion ab
         read (2,*)             ! skip title
         read (2,*) lcru(ncrs),llsu(ncrs)
         write (6,*)
         do l=0,lcru(ncrs)
            read (2,*) nbfcr(l,ncrs)
            do k=1,nbfcr(l,ncrs)
               read (2,*) ncr(k,l,ncrs),zcr(k,l,ncrs),ccr(k,l,ncrs)
            enddo
         enddo
cmd      do l=1,llsu1
         do l=1,llsu(ncrs)
            read (2,*) nbfls(l,ncrs)
            do k=1,nbfls(l,ncrs)
               read (2,*) nls(k,l,ncrs),zls(k,l,ncrs),cls(k,l,ncrs)
            enddo
         enddo
         if (lcru(ncrs).gt.0) then
            read (2,*) izcr(ncrs)
         else
            izcr(ncrs)=0
         endif
         mcrs(ishi)=ncrs
         goto 360
      elseif (compabb(option,'OTHER',1).and.nshi.gt.1.and.
     &    mcrs(ishi).eq.0) then
         write (6,*)
         write (6,*) 'The other atoms in the molecule are:'
         do jshi=1,nshi
            if (jshi.ne.ishi) write (6,"(i2,':   ',a,
     &       '   nuclear charge =',f4.0)") jshi,mtype(jshi),chg(jshi)
         enddo
         write (6,*)
265      call getint ('Which one do you want a pseudopotential '//
     &    'from? <return to menu>',jshi,'$')
         if (jshi.eq.1000000) goto 360
         if (jshi.le.0.or.jshi.gt.nshi.or.jshi.eq.ishi) goto 265
         if (mcrs(jshi).le.0) then
            write (6,*) 'There is no pseudopotential on that atom.'
            goto 360
         endif
         mcrs(ishi)=mcrs(jshi)
         write (6,*) 'Pseudopotential added from other atom'
         goto 360
      elseif (compabb(option,'KEYBOARD',1).and.mcrs(ishi).eq.0) then
         write (6,*)
381      write (6,*) 'What is the highest angular momentum in ',
     &    'the new pseudopotential?'
         call getint ('(1=S, etc.) <return to menu>',lcru(ncrs+1),'$')
         if (lcru(ncrs+1).eq.1000000) goto 360
         if (lcru(ncrs+1).le.0) goto 381
         ncrs=ncrs+1
         llsu(ncrs)=0
         lcru(ncrs)=lcru(ncrs)-1
         do l=0,lcru(ncrs)
383         continue
            if (l.eq.0) then
               call getint ('How many '//am(lcru(ncrs)+1)//
     &          ' terms are in the pseudopotential expansion?',
     &          nbfcr(l,ncrs),' ')
            else
               call getint ('How many '//am(l)//'-'//am(lcru(ncrs)+1)//
     &          ' terms are in the pseudopotential expansion?',
     &          nbfcr(l,ncrs),' ')
            endif
            if (nbfcr(l,ncrs).lt.0) goto 383
            if (nbfcr(l,ncrs).gt.mxbfcr) then
               write (6,*) 'That exceeds the maximum allowed.'
               goto 383
            endif
            do k=1,nbfcr(l,ncrs)
               write (6,*) 'Enter N, zeta, and C for term #',k
               read (5,*) ncr(k,l,ncrs),zcr(k,l,ncrs),ccr(k,l,ncrs)
            enddo
         enddo
         if (lcru(ncrs).gt.0) then
            if (izcr(ncrs).gt.0) then
               write (cnum(1:2),'(i2)') izcr(ncrs)
            else
               cnum=' '
            endif
            call getint ('Enter Zcore for this pseudopotential:',
     &       izcr(ncrs),cnum)
         else
            izcr(ncrs)=0
         endif
         mcrs(ishi)=ncrs
         goto 360
      elseif (compabb(option,'EXPORT',1).and.mcrs(ishi).gt.0) then
         newlib=' '
         call getitle ('Enter library name: <cancel export>',newlib)
         if (newlib.eq.' ') goto 360
         title=' '
         call getitle ('Enter a title for the new pseudopotential.',
     &    title)
         open (unit=3,file=newlib,status='unknown')
c        # position the file to write after the last record.
         call skpend( 3, 1 )
         icrs=mcrs(ishi)
         write (3,*) title
         write (3,*) lcru(icrs),llsu(icrs)
         do l=0,lcru(icrs)
            write (3,*) nbfcr(l,icrs)
            do k=1,nbfcr(l,icrs)
               write (3,*) ncr(k,l,icrs),zcr(k,l,icrs),ccr(k,l,icrs)
            enddo
         enddo
         do l=1,llsu(icrs)
            do k=1,nbfls(l,icrs)
               write (3,*) nls(k,l,icrs),zls(k,l,icrs),cls(k,l,icrs)
            enddo
         enddo
         if (lcru(icrs).gt.0) write (3,*) izcr(icrs)
         close (unit=3)
         goto 360
      elseif (compabb(option,'VIEW',1).and.mcrs(ishi).gt.0) then
         icrs=mcrs(ishi)
         write (6,*)
         if (lcru(icrs).gt.0) write (6,*) 'Pseudopotential:'
         do l=0,lcru(icrs)
            if (l.eq.0) then
               write (6,*) am(lcru(icrs)+1),' terms:'
            else
               write (6,*) am(l),'-',am(lcru(icrs)+1),' terms:'
            endif
            do k=1,nbfcr(l,icrs)
               write (6,*) ncr(k,l,icrs),zcr(k,l,icrs),ccr(k,l,icrs)
            enddo
         enddo
         if (lcru(icrs).gt.0) write (6,*) 'Core electrons:',izcr(icrs)
         write (6,*)
         if (llsu(icrs).gt.0) write (6,*) 'LS coupling:'
         do l=1,llsu(icrs)
            write (6,*) am(l+1),' terms:'
            do k=1,nbfls(l,icrs)
               write (6,*) nls(k,l,icrs),zls(k,l,icrs),cls(k,l,icrs)
            enddo
         enddo
         goto 360
      elseif (compabb(option,'DELETE',1).and.mcrs(ishi).gt.0) then
c        # remove pseudopotential from this atom
         icrs=mcrs(ishi)
         mcrs(ishi)=0
         call rmecp (
     &    ccr,   cls,   icrs,   izcr,
     &    lcru,  llsu,  mxang,  mxang3,
     &    mcrs,  mxatm, mxbfcr, mxcrs,
     &    nbfcr, nbfls, ncr,    ncrs,
     &    nls,   nshi,  zcr,    zls)
         goto 360
      elseif (compabb(option,'QUIT',1)) then
         write (6,*) 'Bye!'
         call bummer('normal termination',0,3)
         stop
      elseif (.not.compabb(option,'PROCEED',1)) then
         goto 361
      endif
      inquire (unit=2,opened=fopen)
      if (fopen) close (unit=2)
      return
      end
c deck edtgcs
      subroutine edtgcs (
     & am,       chg,     eta,      hiso,
     & iconu,    icsu,    ictu,     igcs,
     & ihatmbas, ihiatm,  ihigcs,   ihiord,
     & iirep,    ishi,    isocoef,  jgcsl,
     & libdir,   lmnp1,   mcons,    mtype,
     & mxang,    mxao,    mxaov,    mxatm,
     & mxconatm, mxcons,  mxconset, mxprim,
     & mxso,     mxsoset, nchi,     ncons,
     & nf,       nfl,     ngcs,     nrcr,
     & ns,       nshi,    nsymtp,   zet)
c
c  drive menu for editing gaussian basis sets
c
      implicit real*8 (a-h,o-z)
      real*8 chg(mxatm),eta(mxconset,mxprim,mxcons),
     & hiso(mxaov,mxaov,mxsoset),zet(mxprim,mxcons)
      integer iconu(mxcons),icsu(mxsoset),ictu(mxsoset),
     & igcs(mxconatm,mxatm),ihatmbas(mxatm),ihiatm(mxatm),
     & ihigcs(mxconatm,mxatm),ihiord(mxaov,mxsoset),
     & iirep(mxso,mxsoset),isocoef(mxao,mxso,mxsoset),jgcsl(mxatm),
     & lmnp1(mxcons),mcons(mxconatm,mxatm),nchi(mxatm),nf(mxatm),
     & nfl(mxang),nrcr(mxcons),nsymtp(mxsoset)
      character am(10)
      character*3 mtype(mxatm)
      character*8 option
      character*130 libdir
c
      integer clen
      character*130 libfile, newlib
      character*72 title
      logical fopen, fexist
c
      logical  compabb
      external compabb
c
      integer  fstrlen
      external fstrlen
c
229   continue
c     # generate an AO basis filename from libdir and the atomic symbol.
      llibd   = fstrlen( libdir )
      clen    = fstrlen( mtype(ishi) )
      libfile = libdir(1:llibd) // mtype(ishi)(1:clen) // '.bas'
c
      call izero_wr (mxang,nfl,1)
      do icon=1,nf(ishi)
         jcon=mcons(icon,ishi)
         iam=lmnp1(jcon)
         nfl(iam)=nfl(iam)+nrcr(jcon)
      enddo
230   continue
      write (6,*)
      if (nf(ishi).eq.0) then
         write (6,*) 'This atom currently has no basis functions.'
      else
         write (6,*) 'The basis set for this atom now consists of:'
         do iam=1,mxang
            if (nfl(iam).gt.0) write (6,'(i3,1x,a1)') nfl(iam),am(iam)
         enddo
      endif
      write (6,*)
      write (6,*)   'Enter one of the following options:'
      write (6,*)   '  LIBRARY:  add basis functions to this atom ',
     & 'from a library'
      if (nshi.gt.1) then
         write (6,*) '  OTHER:    add basis functions from ',
     &    'another atom in this molecule'
      endif
      write (6,*)   '  KEYBOARD: add basis functions from the keyboard'
      if (nf(ishi).gt.0) then
         write (6,*) '  EXPORT    basis on this atom to new or ',
     &    'existing library'
         write (6,*) '  VIEW      basis set on this atom'
         write (6,*) '  DELETE    a contraction set on this atom'
      endif
      write (6,*)   '  PROCEED'
      write (6,*)   '  QUIT'
      write (6,*)
231   continue
      if (nf(ishi).eq.0) then
         call getchr ('Which?',option,'LIBRARY')
      else
         call getchr ('Which?',option,'PROCEED')
      endif
      if (compabb(option,'LIBRARY',1)) then
c        # present list of titles in library
240      continue
         write (6,*)
         llibd = fstrlen(libfile)
         write (6,*) 'Basis set library:', libfile(1:llibd)
         nbas=0
         inquire (unit=2,opened=fopen)
         if (.not.fopen) then
            inquire (file=libfile,exist=fexist)
            if (.not.fexist) goto 249
            open (unit=2, file=libfile, status='old', err=249)
         else
            rewind (unit=2)
         endif
241      continue
         read (2,'(a)',err=249,end=248) title
         if (title.eq.' ') goto 241
         nbas=nbas+1
         write (6,"(i3,':  ',a)") nbas,title
         read (2,*,err=249,end=249) nf1
         do if1=1,nf1
            read (2,*,end=249,err=249) jconu,idum,jdum
            if (jconu.gt.mxprim.or.jdum.gt.mxconset) goto 249
            do k=1,jconu
               read (2,*,end=249,err=249) xdum,(ydum,i=1,jdum)
            enddo
         enddo
         goto 241
249      continue
         nbas=0
248      continue
         if (nbas.gt.0) then
            write (6,"(i3,':  ',a)") 0,'Use other library'
            write (6,*)
245         call getint ('Which one? <return to menu>',ibas,'$')
            if (ibas.eq.1000000) goto 230
            if (ibas.lt.0.or.ibas.gt.nbas) goto 245
         else
            write (6,*)
            write (6,*) '       This library file is'
            write (6,*) '*** EMPTY, ABSENT, or DEFECTIVE ***'
            write (6,*)
            write (6,*) 'The default directory for basis set'
            write (6,*) 'libraries may be incorrect.  To override it,'
            write (6,*) 'set the environment variable COL_BASISLIBDIR'
            write (6,*) 'to the correct directory name.'
            write (6,*)
            ibas=0
         endif
         if (ibas.eq.0) then
            inquire (unit=2,opened=fopen)
            if (fopen) close (unit=2)
            libfile=' '
            call getitle (
     &       'Enter new library filename: <return to menu>',libfile)
            if (libfile.eq.' ') goto 229
            goto 240
         endif
c        # reread library up to desired basis set
         rewind (unit=2)
         do jbas=1,ibas-1
            read (2,*)
            read (2,*) nf1
            do if1=1,nf1
               read (2,*) jconu
               do k=1,jconu
                  read (2,*)
               enddo
            enddo
         enddo
c        # read in desired basis set
ctm
c        # Abfrage ano
         read (2,'(a)') title
         itm = index( title, 'ANO' )
ctm
         read (2,*) nf1
         write (6,*)
         if (ncons+nf1.gt.mxcons) then
            write (6,*) 'The maximum allowed # of contraction sets'
            write (6,*) 'has been exceeded.  New functions not added.'
            goto 230
         endif
         if (nf(ishi)+nf1.gt.mxconatm) then
            write (6,*) 'You have too many contraction sets'
            write (6,*) 'on this atom.  New functions not added.'
            goto 230
         endif
         do icon=1,nf1
            jcon=icon+ncons
            read (2,*) iconu(jcon),lmnp1(jcon),nrcr(jcon)
            write (6,*) am(lmnp1(jcon)),' orbital:'
ctm
            if (itm.gt.0) then
1001           continue
c              # lower bound changed from 1 to 0. 17-oct-01 -rls
               write(6,*) '>>ANO-type basis: select number of ',
     &          'contractions (min. 0, max.',nrcr(jcon),')'
               read(5,*,err=1001) itm2
               if (itm2.gt.nrcr(jcon) .or. itm2.lt.0) then
                  write(6,*) 'invalid: no more than ', nrcr(jcon),
     &             'may be selected! '
                  goto 1001
               endif
               nrcr(jcon)=itm2
            endif
c
ctm
c
            do k=1,iconu(jcon)
               read (2,*) zet(k,jcon),(eta(j,k,jcon),j=1,nrcr(jcon))
               write (6,'(f15.6,5f13.7/(11x,5f13.7))') zet(k,jcon),
     &          (eta(j,k,jcon),j=1,nrcr(jcon))
            enddo
c
            mcons(nf(ishi)+icon,ishi)=jcon
            do is=ihatmbas(ishi)+1,ihatmbas(ishi)+nchi(ishi)
               igcs(nf(ishi)+icon,is)=0 ! initialize undefined LSO set
            enddo
            ihigcs(nf(ishi)+icon,ishi)=0 ! initialize undefine HSO set
         enddo
         write (6,*)
         iyn=1
         call getyn ('Is this correct?',iyn)
         if (iyn.eq.0) goto 230
         nf(ishi)=nf(ishi)+nf1
         do if1=ncons+1,ncons+nf1
            nfl(lmnp1(if1))=nfl(lmnp1(if1))+nrcr(if1)
         enddo
         ncons=ncons+nf1
         goto 230
      elseif (compabb(option,'OTHER',1).and.nshi.gt.1) then
         write (6,*)
         write (6,*) 'The other atoms in the molecule are:'
         do jshi=1,nshi
            if (jshi.ne.ishi) write (6,"(i2,':   ',a3,
     &       '   nuclear charge =',f4.0)") jshi,mtype(jshi),chg(jshi)
         enddo
         write (6,*)
250      call getint ('Which one do you want basis functions from? '//
     &    '<return to menu>',jshi,'$')
         if (jshi.eq.1000000) goto 230
         if (jshi.le.0.or.jshi.gt.nshi.or.jshi.eq.ishi) goto 250
         if (nf(jshi).le.0) then
            write (6,*) 'There are no basis functions on that atom.'
            goto 230
         endif
         if (nf(jshi)+nf(ishi).gt.mxconatm) then
            write (6,*) 'You have too many contraction sets on'
            write (6,*) 'this atom.  Basis not added from other atom.'
            goto 230
         endif
         do if1=1,nf(jshi)
            icon=mcons(if1,jshi)
            nf(ishi)=nf(ishi)+1
            mcons(nf(ishi),ishi)=icon
            nfl(lmnp1(icon))=nfl(lmnp1(icon))+nrcr(icon)
            do is=ihatmbas(ishi)+1,ihatmbas(ishi)+nchi(ishi)
               igcs(nf(ishi),is)=0 ! initialize undefined LSO set
            enddo
            ihigcs(nf(ishi),ishi)=0 ! initialize undefined HSO set
         enddo
         write (6,*) nf(jshi),' contraction sets added from other atom'
         goto 230
      elseif (compabb(option,'KEYBOARD',1)) then
260      continue
         if (ncons.lt.mxcons.and.nf(ishi).lt.mxconatm) then
            write (6,*)
261         write (6,*)
     &       'How many primitives are in the next contraction set?'
            call getint ('<return to menu>',iconu(ncons+1),'$')
            if (iconu(ncons+1).eq.1000000) goto 230
            if (iconu(ncons+1).le.0) goto 261
            if (iconu(ncons+1).gt.mxprim) then
               write (6,*) 'That exceeds the maximum allowed.'
               goto 261
            endif
            ncons=ncons+1
262         call getint ('What is the angular momentum? (1=S, etc.)',
     &       lmnp1(ncons),' ')
            if (lmnp1(ncons).le.0) goto 262
            if (lmnp1(ncons).gt.mxang) then
               write (6,*) 'Angular momentum greater than ',am(mxang),
     &          ' is not allowed.'
               goto 262
            endif
263         call getint ('How many CGTFs are there?',nrcr(ncons),' ')
            if (nrcr(ncons).le.0) goto 263
            if (nrcr(ncons).gt.mxconset) then
               write (6,*) 'That exceeds the maximum allowed.'
               goto 263
            endif
            do j=1,iconu(ncons)
               write (6,*) 'Enter the exponent and contraction'
               write (6,*) 'coefficients for primitive ',j
               read (5,*) zet(j,ncons),(eta(k,j,ncons),k=1,nrcr(ncons))
               write(13,*) zet(j,ncons),(eta(k,j,ncons),k=1,nrcr(ncons))
            enddo
            iyn=1
            call getyn ('Is that contraction set correct?',iyn)
            if (iyn.eq.0) then
               ncons=ncons-1
            else
               nf(ishi)=nf(ishi)+1
               mcons(nf(ishi),ishi)=ncons
               do is=ihatmbas(ishi)+1,ihatmbas(ishi)+nchi(ishi)
                  igcs(nf(ishi),is)=0 ! initialize undefined LSO set
               enddo
               ihigcs(nf(ishi),ishi)=0 ! initialize undefined HSO set
               nfl(lmnp1(ncons))=nfl(lmnp1(ncons))+nrcr(ncons)
            endif
            goto 260
         else
            write (6,*) 'The system is filled to capacity with.'
            write (6,*) 'contraction sets.  No more can be added.'
         endif
         goto 230
      elseif (compabb(option,'EXPORT',1).and.nf(ishi).gt.0) then
         newlib=' '
         call getitle ('Enter library name: <cancel export>',newlib)
         if (newlib.eq.' ') goto 230
         title=' '
         call getitle ('Enter a title for the new basis set.',title)
         open (unit=3,file=newlib,status='unknown')
c        # position the file to write after the last record.
         call skpend( 3, 1 )
         write (3,*) title
         write (3,*) nf(ishi)
         do icon=1,nf(ishi)
            jcon=mcons(icon,ishi)
            write (3,*) iconu(jcon),lmnp1(jcon),nrcr(jcon)
            do j=1,iconu(jcon)
               write (3,*) zet(j,jcon),(eta(k,j,jcon),k=1,nrcr(jcon))
            enddo
         enddo
         close (unit=3)
      elseif (compabb(option,'VIEW',1).and.nf(ishi).gt.0) then
         do jcon=1,nf(ishi)
            icon=mcons(jcon,ishi)
            write (6,"(' Contraction set',i3,'  (',a1,' orbitals):')")
     &       jcon,am(lmnp1(icon))
            do k=1,iconu(icon)
               write (6,'(f15.6,5f13.7/(11x,5f13.7))') zet(k,jcon),
     &          (eta(j,k,jcon),j=1,nrcr(jcon))
            enddo
         enddo
         goto 230
      elseif (compabb(option,'DELETE',1).and.nf(ishi).gt.0) then
         write (6,*) 'There are currently ',nf(ishi),
     &    ' contraction sets on this atom.'
290      call getint ('Which one will be deleted? <return to menu>',
     &    ic,'$')
         if (ic.eq.1000000) goto 230
         if (ic.le.0.or.ic.gt.nf(ishi)) goto 290
         icon=mcons(ic,ishi)
         nfl(lmnp1(icon))=nfl(lmnp1(icon))-nrcr(icon)
c        # remove contraction set from this atom
         nf(ishi)=nf(ishi)-1
         do jc=ic,nf(ishi)
            mcons(jc,ishi)=mcons(jc+1,ishi)
         enddo
         call rmcon (eta,icon,iconu,lmnp1,mcons,mxatm,mxconatm,
     &    mxcons,mxconset,mxprim,ncons,nf,nrcr,nshi,zet)
c        # remove corresponding LSO sets
         do is=ihatmbas(ishi)+1,ihatmbas(ishi)+nchi(ishi)
            jgcsl(is)=igcs(ic,is)
            do jc=ic,nf(ishi)
               igcs(jc,is)=igcs(jc+1,is)
            enddo
         enddo
         do is=ihatmbas(ishi)+1,ihatmbas(ishi)+nchi(ishi)
            call rmlso (icsu,ictu,igcs,ihiatm,iirep,isocoef,jgcsl(is),
     &       mxao,mxatm,mxconatm,mxso,mxsoset,nf,ngcs,ns)
         enddo
c        # remove corresponding HSO sets
         jhigcs=ihigcs(ic,ishi)
         do jc=ic,nf(ishi)
            ihigcs(jc,ishi)=ihigcs(jc+1,ishi)
         enddo
         call rmhso (hiso,ihigcs,ihiord,jhigcs,mxaov,mxatm,
     &    mxconatm,mxsoset,nf,nhigcs,nshi,nsymtp)
         goto 230
      elseif (compabb(option,'QUIT',1)) then
         inquire (unit=2,opened=fopen)
         if (fopen) close (unit=2)
         write (6,*) 'Bye!'
         call bummer('normal termination',0,3)
         stop
      elseif (.not.compabb(option,'PROCEED',1)) then
         goto 231
      endif
      inquire (unit=2,opened=fopen)
      if (fopen) close (unit=2)
      return
      end
c deck edtsoc
      subroutine edtsoc (
     & am,       blkshft,  blshco,  chi,
     & ctrans,   hilabl,   hiptgrp, hiso,
     & icomp,    icsu,     ictu,    ideg,
     & igcs,     ihatmbas, ihiatm,  ihigcs,
     & ihiord,   ihisym,   ihsamep, ihsend,
     & iirep,    iperm,    ipiv,    iprd,
     & iproj,    irepofhi, isamep,  isend,
     & ishi,     isize,    islist,  isocoef,
     & isox,     lco,      lmnp1,   lmnv,
     & loatmbas, lolabl,   loptgrp, ltrans,
     & mcons,    mtype,    mxang,   mxang1,
     & mxang2,   mxang3,   mxang4,  mxang5,
     & mxao,     mxaov,    mxatm,   mxatmsym,
     & mxconatm, mxcons,   mxhiblk, mxhirep,
     & mxlorep,  mxorder,  mxso,    mxsoset,
     & nc,       nchi,     nchi1,   nf,
     & ngcs,     nhigcs,   nhirep,  nhsamep,
     & nirep,    nop,      ns,      nsamep,
     & nshi,     nsymtp,   poly1,   poly2,
     & projao,   soaox,    soaoy,   soao1,
     & soao1s,   sox,      sqtol,   transao,
     & xyz)
c
c  drive menu for coarse editing of SOs
c
      implicit real*8 (a-h,o-z)
      real*8 blkshft(mxaov,mxaov),blshco(mxorder),chi(mxhirep,mxorder),
     & ctrans(3,3,mxorder),hiso(mxaov,mxaov,mxsoset),poly1(mxang2),
     & poly2(mxang2),projao(mxaov,mxaov,mxhirep),soaox(mxaov,mxaov),
     & soaoy(mxaov,mxaov+1),soao1(mxaov,mxaov),soao1s(mxaov,mxaov),
     & sox(mxaov),transao(mxaov,mxaov),xyz(3,mxatm+1,mxatm)
      integer icomp(mxang3,mxang2,mxang),icsu(mxsoset),ictu(mxsoset),
     & ideg(mxhirep),igcs(mxconatm,mxatm),ihatmbas(mxatm),
     & ihiatm(mxatm),ihigcs(mxconatm,mxatm),ihiord(mxaov,mxsoset),
     & ihsamep(mxatm),ihsend(mxatm,mxorder,mxatm),iirep(mxso,mxsoset),
     & iperm(mxang2,mxang),ipiv(mxaov),iprd(3,mxang1,mxang3),
     & iproj(mxang2,mxatmsym,mxatmsym,mxlorep),irepofhi(mxhiblk),
     & isamep(mxatm,mxatm),isend(mxatmsym,mxlorep,mxatm),
     & isize(mxhirep),islist(mxconatm),isocoef(mxao,mxso,mxsoset),
     & isox(mxao),lco(mxang2,mxang4,mxang),lmnp1(mxcons),
     & lmnv(3,mxang5),loatmbas(mxatm),ltrans(mxlorep+3,nirep),
     & mcons(mxconatm,mxatm),nc(mxatm),nchi(mxatm),nchi1(mxatm),
     & nf(mxatm),nsamep(mxatm),nsymtp(mxsoset)
      character am(10)
      character*3 hilabl(mxhirep),mtype(mxatm),loptgrp,lolabl(mxlorep)
      character*4 hiptgrp
      character*8 option
      character*72 msg
c
      logical  compabb
      external compabb
c
c     # bummer error type.
      integer   faterr
      parameter(faterr=2)
c
      call fsamep (
     & ihatmbas(ishi), ihsamep, ihsend, isamep,
     & isend,          ishi,    mxatm,  mxatmsym,
     & mxlorep,        mxorder, nc,     nchi(ishi),
     & nchi1,          nhsamep, nirep,  nop,
     & ns,             nsamep,  nshi)
301   continue
      call defso (
     & blkshft,          blshco,      chi,      ctrans,
     & hiso,             icomp,       icsu,     ictu,
     & ideg,             igcs,        ihatmbas, ihiatm,
     & ihigcs,           ihiord,      ihisym,   ihsamep,
     & ihsend(1,1,ishi), iirep,       ipiv,     iprd,
     & iproj,            isamep,      isend,    ishi,
     & isocoef,          lmnp1,       ltrans,   mcons,
     & mxang,            mxang1,      mxang2,   mxang3,
     & mxao,             mxaov,       mxatm,    mxatmsym,
     & mxconatm,         mxcons,      mxhirep,  mxlorep,
     & mxorder,          mxso,        mxsoset,  nc,
     & nchi(ishi),       nchi1(ishi), nf,       ngcs,
     & nhigcs,           nhirep,      nhsamep,  nirep,
     & nop,              nsamep,      nsymtp,   poly1,
     & poly2,            projao,      soaox,    soaoy,
     & soao1,            soao1s,      sox,      sqtol,
     & transao)
302   continue
      write (6,*)
      write (6,*) 'The sets of symmetry orbitals on this atom are:'
      do icon=1,nf(ishi)
         jcon=mcons(icon,ishi)
         iam=lmnp1(jcon)
         msg='Insufficient memory'
         do is=ihatmbas(ishi)+1,ihatmbas(ishi)+nchi(ishi)
            if (igcs(icon,is).le.0) goto 309
         enddo
         msg='Linearly dependent'
         if (ihigcs(icon,ishi).lt.0) goto 309
         msg='Incompatible with HISYM'
         if (ihigcs(icon,ishi).gt.0.or.ihisym.eq.0) msg='OK'
309      continue
c===> Liste der SOs
c===> sollte nun voreingestellt sein auf define z fuer alle SOs
         write (6,"(i3,':   ',a1,4x,a)") icon,am(iam),msg(1:32)
      enddo
      write (6,*)
      write (6,*) 'Enter one of the following options:'
      write (6,*) '  DEFINE prepackaged SOs ',
     & '(includes well-defined angular momentum)'
      write (6,*) '  VIEW   a set of SOs'
      write (6,*) '  EDIT   a set of SOs'
      write (6,*) '  PROCEED'
      write (6,*) '  QUIT'
      write (6,*)
310   continue
      call getchr ('Which?',option,'PROCEED')
      if (compabb(option,'DEFINE',1)) then
         write (6,*)
350      call getint ('For which contraction set do you want to '//
     &    'define SOs? <return to menu>',icon,'$')
         if (icon.eq.1000000) goto 302
         if (icon.le.0.or.icon.gt.nf(ishi)) goto 350
         jcon=mcons(icon,ishi)
         iam=lmnp1(jcon)
         iam2=iam*(iam+1)/2
         write (6,*)
         write (6,*) 'This is a set of ',am(iam),' orbitals.'
         write (6,*)
     &    'You can define these orbitals as one of the following:'
         write (6,*) '  X:      Well-defined L2 and Lx'
         write (6,*) '  Y:      Well-defined L2 and Ly'
         write (6,*) '  Z:      Well-defined L2 and Lz'
         write (6,*) '  CART:   Cartesian SOs'
         write (6,*)
351      continue
*@ifdef f90
         write (6,'(a)',advance='no') ' Which? <return to menu> '
*@else
*         write (6,1900) 'Which? <return to menu> '
*@ifdef format
*1900     format (' ',a,$)
*@else
* 1900    format (' ',a)
*@endif
*@endif
         read (5,'(a)') option
         write(13,'(a)') option
         if (option.eq.' ') goto 302
         call strupp (option)
         if (.not.compabb(option,'CART',1).and.option.ne.'X'.and.
     &    option.ne.'Y'.and.option.ne.'Z') goto 351
         do is=ihatmbas(ishi)+1,ihatmbas(ishi)+nchi(ishi)
            jgcs=igcs(icon,is)
            write (6,*)
            write (6,*) 'Defining SOs for atom(s) at:'
            do iatm=loatmbas(is)+1,loatmbas(is)+nc(is)
               write (6,*) (xyz(j,iatm,ishi),j=1,3)
            enddo
            call sameso (
     &       hiso,   iam2,     icon,   icsu,
     &       ictu,   igcs,     ihiatm, ihigcs,
     &       ihiord, iirep,    is,     isocoef,
     &       jgcs,   loatmbas, mxao,   mxaov,
     &       mxatm,  mxconatm, mxso,   mxsoset,
     &       nc,     nf,       ngcs,   nhigcs,
     &       ns,     nshi,     nsymtp, xyz)
            if (jgcs.le.0) goto 352
            if (compabb(option,'CART',1)) then
               ictu(jgcs)=nc(is)*iam2
               icsu(jgcs)=ictu(jgcs)
               do j=1,icsu(jgcs)
                  do i=1,ictu(jgcs)
                     isocoef(i,j,jgcs)=0
                  enddo
               enddo
               do iatm=0,nc(is)-1
                  do jatm=0,nc(is)-1
                     ix=iand(iatm,jatm)
                     jx=1
                     if (ix.eq.1.or.ix.eq.2.or.ix.eq.4.or.ix.eq.7) jx=-1
                     do j=1,iam2
                        isocoef(iatm*iam2+j,jatm*iam2+j,jgcs)=jx
                     enddo
                  enddo
               enddo
            elseif (option.eq.'X') then
               icsu(jgcs)=(iam*2-1)*nc(is)
               do iatm=0,nc(is)-1
                  do jatm=0,nc(is)-1
                     ix=iand(iatm,jatm)
                     jx=1
                     if (ix.eq.1.or.ix.eq.2.or.ix.eq.4.or.ix.eq.7) jx=-1
                     do iso=1,iam*2-1
                        do j=1,iam2
                           isocoef(iatm*iam2+j,jatm*(2*iam-1)+iso,jgcs)=
     &                      jx*lco(iperm(iperm(j,iam),iam),iso,iam)
                        enddo
                     enddo
                  enddo
               enddo
            elseif (option.eq.'Y') then
               icsu(jgcs)=(iam*2-1)*nc(is)
               do iatm=0,nc(is)-1
                  do jatm=0,nc(is)-1
                     ix=iand(iatm,jatm)
                     jx=1
                     if (ix.eq.1.or.ix.eq.2.or.ix.eq.4.or.ix.eq.7) jx=-1
                     do iso=1,iam*2-1
                        do j=1,iam2
                           isocoef(iatm*iam2+j,jatm*(2*iam-1)+iso,jgcs)=
     &                      jx*lco(iperm(j,iam),iso,iam)
                        enddo
                     enddo
                  enddo
               enddo
            elseif (option.eq.'Z') then
               icsu(jgcs)=(iam*2-1)*nc(is)
               do iatm=0,nc(is)-1
                  do jatm=0,nc(is)-1
                     ix=iand(iatm,jatm)
                     jx=1
                     if (ix.eq.1.or.ix.eq.2.or.ix.eq.4.or.ix.eq.7) jx=-1
                     do iso=1,iam*2-1
                        do j=1,iam2
                           isocoef(iatm*iam2+j,jatm*(2*iam-1)+iso,jgcs)=
     &                      jx*lco(j,iso,iam)
                        enddo
                     enddo
                  enddo
               enddo
            endif
c           # determine the symmetries of the new SOs
            call loprj (
     &       iam,      icomp,   iproj,  isend(1,1,is),
     &       ltrans,   mxang,   mxang2, mxang3,
     &       mxatmsym, mxlorep, nc(is), nirep)
            do iso=1,icsu(jgcs)
               iirep(iso,jgcs)=ifrep(iam2,iproj,isocoef(1,iso,jgcs),
     &          mxang2,mxatmsym,nc(is),nirep)
               if (iirep(iso,jgcs).eq.0) then
                  write (6,*) 'An SO does not belong to an irrep!'
                  write (6,*) 'Contact the author of this software.'
                  call bummer ('exiting ...',0,faterr)
               endif
            enddo
352         continue
         enddo
         goto 301
      elseif (compabb(option,'VIEW',1)) then
         write (6,*)
         write (6,*) 'SOs are displayed with their magnitudes squared.'
         write (6,*) '(i. e. -4 is really -2)'
         write (6,*)
320      continue
         call getint ('Which set of SOs do you want to see? '//
     &    '<return to menu>',icon,'$')
         if (icon.eq.1000000) goto 302
         if (icon.le.0.or.icon.gt.nf(ishi)) goto 320
         jcon=mcons(icon,ishi)
         iam=lmnp1(jcon)
         iam2=iam*(iam+1)/2
         ndxl=(iam2*(iam-1))/3+1
         ndxu=(iam2*(iam+2))/3
         do is=ihatmbas(ishi)+1,ihatmbas(ishi)+nchi(ishi)
            jgcs=igcs(icon,is)
            write (6,*)
            write (6,*) 'For atoms at coordinates'
            do iatm=loatmbas(is)+1,loatmbas(is)+nc(is)
               write (6,*) (xyz(j,iatm,ishi),j=1,3)
            enddo
            write (6,*) 'The SO coefficients are:'
            write (6,*) '(x^l y^m z^n denoted by lmn)'
c         write (6,'(t14,12a5)') ((poly(i,iam),i=1,iam2),jat=1,nc(is))
c         write (6,"(t14,12('(',3i1,')'))")
            write (6,"(t14,12(2x,3i1))")
     &       (((lmnv(k,ndx),k=1,3),ndx=ndxl,ndxu),jat=1,nc(is))
            do jso=1,icsu(jgcs)
               write (6,"(i3,':  (',a3,')',(t14,12i5))") jso,
     &          lolabl(iirep(jso,jgcs)),
     &          (isocoef(i,jso,jgcs),i=1,ictu(jgcs))
            enddo
         enddo
c        # determine symmetries of SOs in hisym point group, if possible
         jhigcs=ihigcs(icon,ishi)
         if (jhigcs.gt.0) then
            write (6,*)
            call izero_wr (nhirep,isize,1)
            write (6,*) 'In ',hiptgrp,' these SOs transform as:'
            do iblk=1,nsymtp(jhigcs)
               jhirep=irepofhi(ihiord(iblk,jhigcs))
               isize(jhirep)=isize(jhirep)+1
            enddo
            do ihirep=1,nhirep
               if (isize(ihirep).gt.0) write (6,'(i7,1x,a3)')
     &          isize(ihirep)/ideg(ihirep),hilabl(ihirep)
            enddo
         elseif (jhigcs.lt.0) then
            write (6,*)
            write (6,*) 'These SOs are linearly dependent.'
         elseif (ihisym.ne.0) then
            write (6,*)
            write (6,*) 'These SOs are unstable in ',hiptgrp
         endif
         goto 302
      elseif (compabb(option,'EDIT',1)) then
         write (6,*)
330      continue
         call getint ('Which set of SOs do you want to edit? '//
     &    '<return to menu>',icon,'$')
         if (icon.eq.1000000) goto 302
         if (icon.le.0.or.icon.gt.nf(ishi)) goto 330
         write (6,*)
         write (6,*) 'SOs are displayed and entered with their ',
     &    'magnitudes squared.'
         write (6,*) '(i. e. -4 is really -2)'
         call edtsof (
     &    hiso,     icomp,    icon,     icsu,
     &    ictu,     igcs,     ihatmbas, ihiatm,
     &    ihigcs,   ihiord,   iirep,    iproj,
     &    isamep,   isend,    ishi,     islist,
     &    isocoef,  isox,     lmnp1,    lmnv,
     &    loatmbas, lolabl,   loptgrp,  ltrans,
     &    mcons,    mtype,    mxang,    mxang2,
     &    mxang3,   mxang5,   mxao,     mxaov,
     &    mxatm,    mxatmsym, mxconatm, mxcons,
     &    mxlorep,  mxso,     mxsoset,  nc,
     &    nchi,     nf,       ngcs,     nhigcs,
     &    nirep,    ns,       nsamep,   nshi,
     &    nsymtp,   xyz)
         goto 301
      elseif (compabb(option,'QUIT',1)) then
         write (6,*) 'Bye!'
         call bummer('normal termination',0,3)
         stop
      elseif (.not.compabb(option,'PROCEED',1)) then
         goto 310
      endif
      return
      end
c deck edtsof
      subroutine edtsof (
     & hiso,     icomp,    icon,     icsu,
     & ictu,     igcs,     ihatmbas, ihiatm,
     & ihigcs,   ihiord,   iirep,    iproj,
     & isamep,   isend,    ishi,     islist,
     & isocoef,  isox,     lmnp1,    lmnv,
     & loatmbas, lolabl,   loptgrp,  ltrans,
     & mcons,    mtype,    mxang,    mxang2,
     & mxang3,   mxang5,   mxao,     mxaov,
     & mxatm,    mxatmsym, mxconatm, mxcons,
     & mxlorep,  mxso,     mxsoset,  nc,
     & nchi,     nf,       ngcs,     nhigcs,
     & nirep,    ns,       nsamep,   nshi,
     & nsymtp,   xyz)
c
c  drive menu for fine editing of the SOs
c
      implicit real*8 (a-h,o-z)
      real*8 hiso(mxaov,mxaov,mxsoset),xyz(3,mxatm+1,mxatm)
      integer icomp(mxang3,mxang2,mxang),icsu(mxsoset),ictu(mxsoset),
     & igcs(mxconatm,mxatm),ihatmbas(mxatm),ihiatm(mxatm),
     & ihigcs(mxconatm,mxatm),ihiord(mxaov,mxsoset),
     & iirep(mxso,mxsoset),iproj(mxang2,mxatmsym,mxatmsym,mxlorep),
     & isamep(mxatm,mxatm),isend(mxatmsym,mxatm,mxlorep),
     & islist(mxconatm),isocoef(mxao,mxso,mxsoset),isox(mxao),
     & lmnp1(mxcons),lmnv(3,mxang5),loatmbas(mxatm),
     & ltrans(mxlorep+3,mxlorep),mcons(mxconatm,mxatm),nc(mxatm),
     & nchi(mxatm),nf(mxatm),nsamep(mxatm),nsymtp(mxsoset)
      character*3 mtype(mxatm),loptgrp,lolabl(mxlorep)
      character*8 option
c
      logical  compabb
      external compabb
c
c     # bummer error type.
      integer   faterr
      parameter(faterr=2)
c
      jcon=mcons(icon,ishi)
      iam=lmnp1(jcon)
      iam2=iam*(iam+1)/2
      ndxl=(iam2*(iam-1))/3+1
      ndxu=(iam2*(iam+2))/3
      do is=ihatmbas(ishi)+1,ihatmbas(ishi)+nchi(ishi) ! loop over losym
         call loprj (
     &    iam,      icomp,   iproj,  isend(1,1,is),
     &    ltrans,   mxang,   mxang2, mxang3,
     &    mxatmsym, mxlorep, nc(is), nirep)
         jgcs=igcs(icon,is)
         write (6,*)
         write (6,*) 'The current set of SOs has',ictu(jgcs),
     &    ' AOs and applies to atom(s) at:'
         do iatm=loatmbas(is)+1,loatmbas(is)+nc(is)
            write (6,*) (xyz(j,iatm,ishi),j=1,3)
         enddo
         call sameso (
     &    hiso,   iam2,     icon,    icsu,
     &    ictu,   igcs,     ihiatm,  ihigcs,
     &    ihiord, iirep,    is,      isocoef,
     &    jgcs,   loatmbas, mxao,    mxaov,
     &    mxatm,  mxconatm, mxso,    mxsoset,
     &    nc,     nf,       ngcs,    nhigcs,
     &    ns,     nshi,     nsymtp,  xyz)
         if (jgcs.le.0) goto 340
332      continue
         write (6,*)
         write (6,*) 'The SOs transform (in ',loptgrp,') as:'
         write (6,'(15(2x,a3))') (lolabl(iirep(j,jgcs)),j=1,icsu(jgcs))
333      continue
         write (6,*)
         write (6,*) 'Enter one of the following options:'
         if (icsu(jgcs).lt.ictu(jgcs)) write (6,*) '  ADD    a new SO'
         if (icsu(jgcs).gt.0) then
            write (6,*) '  CHANGE an SO'
            write (6,*) '  VIEW   the set of SOs'
            write (6,*) '  DELETE an SO'
         endif
         if (ngcs.gt.0) write (6,*)
     &    '  OTHER: use the SOs on another contraction set'
         write (6,*) '  PROCEED'
         write (6,*) '  QUIT'
         write (6,*)
334      call getchr ('Which one?',option,'PROCEED')
         if (compabb(option,'ADD',1).and.icsu(jgcs).lt.ictu(jgcs)) then
            icsu(jgcs)=icsu(jgcs)+1
*@ifdef f90
            write (6,'(a)',advance='no')
     &       ' Enter the coefficients of the new SO: '
*@else
*            write (6,1900) 'Enter the coefficients of the new SO: '
*@ifdef format
*1900        format (' ',a,$)
*@else
* 1900      format (' ',a)
*@endif
*@endif
            read (5,*) (isocoef(i,icsu(jgcs),jgcs),i=1,ictu(jgcs))
            jrep=ifrep(iam2,iproj,isocoef(1,icsu(jgcs),jgcs),
     &       mxang2,mxatmsym,nc(is),nirep)
            iirep(icsu(jgcs),jgcs)=jrep
            if (jrep.eq.0) then
               write (6,*) 'That SO doesn''t belong to an irrep !!!'
               icsu(jgcs)=icsu(jgcs)-1
            endif
            goto 332
         elseif (compabb(option,'CHANGE',1).and.icsu(jgcs).gt.0) then
335         call getint ('Which SO will be changed? <return to menu>',
     &       jso,'$')
            if (jso.eq.1000000) goto 332
            if (jso.le.0.or.jso.gt.icsu(jgcs)) goto 335
            write (6,'(a)') ' Enter the coefficients of the new SO: '
            read (5,*) (isox(i),i=1,ictu(jgcs))
c           # symmetry of new SO
            jrep=ifrep(iam2,iproj,isox,mxang2,mxatmsym,nc(is),nirep)
            if (jrep.ne.0) then
               do j=1,ictu(jgcs)
                  isocoef(j,jso,jgcs)=isox(j)
               enddo
               iirep(jso,jgcs)=jrep
            else
               write (6,*) 'That SO doesn''t belong to an irrep !!!'
            endif
            goto 332
         elseif (compabb(option,'VIEW',1).and.icsu(jgcs).gt.0) then
            write (6,*)
            write (6,*) 'The SO coefficients are:'
            write (6,*) '(x^l y^m z^n denoted by lmn)'
c         write (6,'(t14,12a5)') ((poly(i,iam),i=1,iam2),jat=1,nc(is))
c         write (6,"(t14,12('(',3i1,')'))")
            write (6,"(t14,12(2x,3i1))")
     &       (((lmnv(k,ndx),k=1,3),ndx=ndxl,ndxu),jat=1,nc(is))
            do jso=1,icsu(jgcs)
               write (6,"(i3,':  (',a3,')',(t14,12i5))") jso,
     &          lolabl(iirep(jso,jgcs)),
     &          (isocoef(i,jso,jgcs),i=1,ictu(jgcs))
            enddo
            goto 333
         elseif (compabb(option,'DELETE',1).and.icsu(jgcs).gt.0) then
336         call getint ('Which SO will be deleted? <return to menu>',
     &       jso,'$')
            if (jso.eq.1000000) goto 332
            if (jso.le.0.or.jso.gt.icsu(jgcs)) goto 336
            icsu(jgcs)=icsu(jgcs)-1
            do kso=jso,icsu(jgcs)
               do j=1,ictu(jgcs)
                  isocoef(j,kso,jgcs)=isocoef(j,kso+1,jgcs)
               enddo
               iirep(kso,jgcs)=iirep(kso+1,jgcs)
            enddo
            goto 332
         elseif (compabb(option,'OTHER',1)) then
c  find the other contraction sets that could be used here
            write (6,*)
            write (6,*) 'The atoms that may have SO'
     &       // ' sets that can be used here are:'
            write (6,*) ' 1:   This atom'
            do jsamep=2,nsamep(is)
               js=isamep(jsamep,is)
               jshi=ihiatm(js)
               write (6,"(i3,':   ',a3,' atom(s) at coordinates:')")
     &          jsamep,mtype(jshi)
               do iatm=loatmbas(js)+1,loatmbas(js)+nc(js)
                  write (6,*) '       ',(xyz(j,iatm,jshi),j=1,3)
               enddo
            enddo
            write (6,*)
341         call getint ('Which one do you want SOs from? '//
     &       '<return to menu>',jsamep,'$')
            if (jsamep.eq.1000000) goto 332
            if (jsamep.le.0.or.jsamep.gt.nsamep(is)) goto 341
            js=isamep(jsamep,is)
            jshi=ihiatm(js)
            write (6,*)
            write (6,*) 'Possible SO sets on that atom are:'
            nslist=0
            do jcn=1,nf(jshi)
               jcn1=mcons(jcn,jshi)
               if (lmnp1(jcn1).eq.iam) then
                  kgcs=igcs(jcn,js)
                  if (kgcs.eq.jgcs) goto 337
                  do jslist=1,nslist
                     if (islist(jslist).eq.kgcs) goto 337
                  enddo
                  nslist=nslist+1
                  islist(nslist)=kgcs
                  write (6,"(i3,':',(t7,12(2x,a3)))") nslist,
     &             (lolabl(iirep(j,kgcs)),j=1,icsu(kgcs))
               endif
337            continue
            enddo
            write (6,*)
            if (nslist.le.0) then ! other atom has no SO sets for this a
               write (6,*) '   *** NONE ***'
               goto 332
            endif
338         call getint ('Which one do you want? <return to menu>',
     &       jslist,'$')
            if (jslist.eq.1000000) goto 332
            if (jslist.le.0.or.jslist.gt.nslist) goto 338
            kgcs=islist(jslist)
            if (ictu(jgcs).ne.ictu(kgcs)) then
               write (6,*) 'Unequal SO set sizes.'
               write (6,*) 'Somethin'' ''bout that BUUUUUUUUGS me!'
               write (6,*) 'Contact the author of this software.'
               call bummer ('exiting ...',0,faterr)
            endif
            write (6,*)
            iyn=1
            write (6,*) 'Do you want further changes on that set'
            call getyn ('to affect the other contraction set(s)?',iyn)
            if (iyn.ne.0) then
               igcs(icon,is)=kgcs
               call rmlso (
     &          icsu,  ictu,     igcs, ihiatm,
     &          iirep, isocoef,  jgcs, mxao,
     &          mxatm, mxconatm, mxso, mxsoset,
     &          nf,    ngcs,     ns)
               jgcs=igcs(icon,is)
c              # undefine all HSO sets that depend on this LSO set
               do js=1,ns
                  jshi=ihiatm(js)
                  do jcn=1,nf(jshi)
                     if (igcs(jcn,js).eq.jgcs) then
                        jhigcs=ihigcs(jcn,jshi)
                        ihigcs(jcn,jshi)=0
                        call rmhso (
     &                   hiso,  ihigcs, ihiord,   jhigcs,
     &                   mxaov, mxatm,  mxconatm, mxsoset,
     &                   nf,    nhigcs, nshi,     nsymtp)
                     endif
                  enddo
               enddo
            else
               icsu(jgcs)=icsu(kgcs)
               do iso=1,icsu(kgcs)
                  iirep(iso,jgcs)=iirep(iso,kgcs)
                  do iao=1,ictu(kgcs)
                     isocoef(iao,iso,jgcs)=isocoef(iao,iso,kgcs)
                  enddo
               enddo
            endif
            goto 332
         elseif (compabb(option,'QUIT',1)) then
            write (6,*) 'Bye!'
            call bummer('normal termination',0,3)
            stop
         elseif (.not.compabb(option,'PROCEED',1)) then
            goto 334
         endif
340      continue
      enddo
      return
      end
c deck fhperm
      subroutine fhperm (ctrans,ihsend,mxatm,natm,nop,xyz)
c
c  find atom permutations due to hisym operations.  Atom coordinates
c  pass in through xyz, transformation matrices in ctrans.  Result
c  returned in ihsend.
c
      implicit real*8 (a-h,o-z)
      real*8 ctrans(3,3,nop),xyz(3,natm),xyz1(3)
      integer ihsend(mxatm,nop)
c
      do iop=1,nop              ! loop over operations
         do iatm=1,natm
            do i=1,3
               a=0
               do j=1,3
                  a=a+ctrans(i,j,iop)*xyz(j,iatm)
               enddo
               xyz1(i)=a
            enddo
c         # this will be the square of the distance of the
c         # atom closest to where iatm gets sent by iop
            close=10000
            do jatm=1,natm
               distsq=sqdif(xyz1,xyz(1,jatm),3)
               if (distsq.lt.close) then
                  close=distsq
                  iclat=jatm    ! closest atom so far
               endif
            enddo
            ihsend(iatm,iop)=iclat
         enddo
      enddo
      return
      end
c deck flperm
      subroutine flperm (isend,ltrans,mxatmsym,mxlorep,natm,nirep,xyz)
c
c  find atom permutations due to losym operations.  Atom coordinates
c  pass in through xyz, diagonal elements of transformation matrices in
c  ltrans.  Result passed out through isend.
c
      implicit real*8 (a-h,o-z)
      real*8 xyz(3,natm),xyz1(3)
      integer isend(mxatmsym,nirep),ltrans(mxlorep+3,nirep)
c
      do iopl=1,nirep
         do iatm=1,natm
            do i=1,3
               xyz1(i)=ltrans(i,iopl)*xyz(i,iatm)
            enddo
c           # this will be the square of the distance of the
c           # atom closest to where iat gets sent by iopl
            close=10000
            do jatm=1,natm
               distsq=sqdif(xyz1,xyz(1,jatm),3)
               if (distsq.lt.close) then
                  close=distsq
                  iclat=jatm    ! closest atom so far
               endif
            enddo
            isend(iatm,iopl)=iclat
         enddo
      enddo
      return
      end
c deck frmhso
      subroutine frmhso (
     & blkshft, blshco, chi,     ctrans,
     & iam,     icomp,  ideg,    iflag,
     & ihiord,  ihsend, ipiv,    iprd,
     & mxang,   mxang1, mxang2,  mxang3,
     & mxaov,   mxatm,  mxhirep, nchi1,
     & nhirep,  nhiso,  nop,     poly1,
     & poly2,   projao, soaox,   soaoy,
     & soao1,   sox,    sqtol,   transao)
c
c  form HSOs from combined SOAO matrix soaox.  soao1 is its inverse as
c  determined by invert subroutine.  ihsend is the hisym atom
c  permutation.  Resulting HSO-LSO matrix is returned in soaoy.
c
      implicit real*8 (a-h,o-z)
      parameter (thr4=1.0d-4)
      real*8 blkshft(mxaov,mxaov),blshco(nop),chi(mxhirep,nop),
     & ctrans(3,3,nop),poly1(mxang2),poly2(mxang2),
     & projao(mxaov,mxaov,mxhirep),soaox(mxaov,mxaov),
     & soaoy(mxaov,mxaov+1),soao1(mxaov,mxaov),sox(mxaov),
     & transao(mxaov,mxaov)
      integer icomp(mxang3,mxang2,mxang),ideg(nhirep),ihiord(nhiso),
     & ihsend(mxatm,nop),ipiv(mxaov),iprd(3,mxang1,mxang3)
c
c     # bummer error type.
      integer   faterr
      parameter(faterr=2)
c
      call wzero (mxaov*mxaov*nhirep,projao,1)
      call wzero (mxaov*mxaov,blkshft,1)
      iam2=iam*(iam+1)/2
      nhiao=nchi1*iam2
      iflag=0
      do iop=1,nop              ! loop over operators
c        # prepare the AO transformation matrix for iop for the
c        # current hisym at set (with permutations specified by
c        # ihsend) and contraction set (angular momentum
c        # specified by iam)
         call wzero (mxaov*mxaov,transao,1)
         do iatm=1,nchi1
            iatoff=(iatm-1)*iam2
            jatm=ihsend(iatm,iop)
            jatoff=(jatm-1)*iam2
            do iao=1,iam2
               poly1(1)=1
               do ifact=1,iam-1
                  call polymult (iprd,mxang1,mxang3,ifact,
     &             ctrans(1,icomp(ifact,iao,iam),iop),poly1,poly2)
                  do j=1,(ifact+1)*(ifact+2)/2
                     poly1(j)=poly2(j)
                  enddo
               enddo
               do j=1,iam2
                  transao(j+jatoff,iao+iatoff)=poly1(j)
               enddo
            enddo
         enddo
c        # add contribution of this matrix to the projection operator
         do ihirep=1,nhirep
            x=chi(ihirep,iop)/nop
            do i=1,nhiao
               do j=1,nhiao
                  projao(j,i,ihirep)=projao(j,i,ihirep)+transao(j,i)*x
               enddo
            enddo
         enddo
c        # ... and to the block shift operator
         do i=1,nhiao
            do j=1,nhiao
               blkshft(j,i)=blkshft(j,i)+transao(j,i)*blshco(iop)
            enddo
         enddo
      enddo
c     # transform the block shift operator to the LSO basis
      do iso=1,nhiso
         do iao=1,nhiao
            x=0
            do j=1,nhiao
               x=x+blkshft(iao,j)*soaox(j,iso)
            enddo
            sox(iao)=x
         enddo
         do iao=1,nhiao
            x=0
            do j=1,nhiao
               x=x+soao1(iao,j)*sox(j)
            enddo
            soaoy(iao,iso)=x
         enddo
         sumsq=0
         do i=nhiso+1,nhiao
            sumsq=sumsq+soaoy(i,iso)**2
         enddo
         if (sumsq.gt.sqtol) then
            iflag=1
            goto 78
         endif
      enddo
      do i=1,nhiso
         do j=1,nhiso
            blkshft(j,i)=soaoy(j,i)
         enddo
      enddo
c     # project the SOs onto the hisym irreps
      istblk=0
      ihiblk=0
      do ihirep=1,nhirep      ! loop over hisym irreps
         nlipso=0      ! # of linearly independent projected SO's so far
         ihiblk=ihiblk+1
         do iso=1,nhiso
            newcol=istblk+nlipso+1
            do i=1,nhiao
               x=0
               do j=1,nhiao
                  x=x+projao(i,j,ihirep)*soaox(j,iso)
               enddo
               sox(i)=x
            enddo
c           # transform to LSO basis
            do i=1,nhiao
               x=0
               do j=1,nhiao
                  x=x+soao1(i,j)*sox(j)
               enddo
               soaoy(i,newcol)=x
            enddo
c           # check if LSO was projected out of the basis set
            sumsq=0
            do i=nhiso+1,nhiao
               sumsq=sumsq+soaoy(i,newcol)**2
            enddo
            if (sumsq.gt.sqtol) then
               iflag=1
               goto 78
            endif
c           # check if new HSO is linearly indep. with previous ones
            do ihso=istblk+1,istblk+nlipso ! loop over previous HSO's
               jpiv=ipiv(ihso)
               a=soaoy(jpiv,newcol)/soaoy(jpiv,ihso)
               do ilso=1,nhiso
                  soaoy(ilso,newcol)=soaoy(ilso,newcol)-
     &             a*soaoy(ilso,ihso)
               enddo
            enddo
c           # was the HSO zeroed?  If not, find new pivot point
            alarge=0
            do ilso=1,nhiso
               if (abs(soaoy(ilso,newcol)).gt.alarge) then
                  alarge=abs(soaoy(ilso,newcol))
                  ipiv(newcol)=ilso
               endif
            enddo
            if (alarge.gt.thr4) then
               nlipso=nlipso+1  ! new HSO
               ihiord(newcol)=ihiblk
            endif
         enddo
         istblk=istblk+nlipso
c        # now use the block shift operator to find the HSO's of
c        # the other block, if any, of ihirep
         do iblk=2,ideg(ihirep)
            ihiblk=ihiblk+1
            do ihso=1,nlipso
               newcol=istblk+ihso
               ioldcol=newcol-nlipso
               do i=1,nhiso
                  x=0
                  do j=1,nhiso
                     x=x+blkshft(i,j)*soaoy(j,ioldcol)
                  enddo
                  soaoy(i,newcol)=x
               enddo
               ihiord(newcol)=ihiblk
            enddo
            istblk=istblk+nlipso
         enddo
      enddo
      if (istblk.ne.nhiso) then
         write (6,*) 'Somethin'' ''bout that BUUUUUUUUGS me!!!'
         write (6,*) '(i. e. contact the author of this software)'
         call bummer ('exiting ...',0,faterr)
      endif
78    continue
      return
      end
c deck fsamep
      subroutine fsamep (
     & ihatmbas, ihsamep, ihsend, isamep,
     & isend,    ishi,    mxatm,  mxatmsym,
     & mxlorep,  mxorder, nc,     nchi,
     & nchi1,    nhsamep, nirep,  nop,
     & ns,       nsamep,  nshi)
c
      integer ihsamep(mxatm),ihsend(mxatm,mxorder,mxatm),
     & isamep(mxatm,mxatm),isend(mxatmsym,mxlorep,mxatm),nc(mxatm),
     & nchi1(mxatm),nsamep(mxatm)
c
c  determine which other kinds of atoms have the same permutations
c  as this one (ishi) in the hisym point group.  Result returned
c  in ihsamep.
c
      nhsamep=1
      ihsamep(1)=ishi      ! this atom appears first in its own list
      do jshi=1,nshi
         if (nchi1(jshi).ne.nchi1(ishi).or.ishi.eq.jshi) goto 71
         do iop=1,nop
            do iatm=1,nchi1(ishi)
               if (ihsend(iatm,iop,ishi) .ne.
     &          ihsend(iatm,iop,jshi)) goto 71
            enddo
         enddo
         nhsamep=nhsamep+1
         ihsamep(nhsamep)=jshi
71       continue
      enddo
      do is=ihatmbas+1,ihatmbas+nchi
c        # determine which other kinds of atoms have the same
c        # permutations as this one (is) in the losym point group.
c        # Result returned in isamep
         nsamep(is)=1
         isamep(1,is)=is   ! this atom appears first in its own list
         do js=1,ns
            if (nc(js).ne.nc(is).or.is.eq.js) goto 73
            do iopl=1,nirep
               do iatm=1,nc(is)
                  if (isend(iatm,iopl,is) .ne.
     &             isend(iatm,iopl,js)) goto 73
               enddo
            enddo
            nsamep(is)=nsamep(is)+1
            isamep(nsamep(is),is)=js
73          continue
         enddo
      enddo
      return
      end
c deck genptgrp
      subroutine genptgrp (
     & gen,    ideg,   ihoff,   issqd,
     & multed, mxgen,  mxhirep, mxorder,
     & ngen,   nhirep, nop,     sqtol,
     & trans)
c
c  generate hisym point group operations
c
      implicit real*8 (a-h,o-z)
      real*8 gen(mxorder,mxgen),trans(mxorder,mxorder+1)
      integer ideg(mxhirep),ihoff(mxhirep),multed(mxgen)
c
c     # bummer error type.
      integer   faterr
      parameter(faterr=2)
c
c     # insert the identity as the 1st symmetry operator
      call wzero (mxorder,trans,1)
      do ihirep=1,nhirep
         jdeg=ideg(ihirep)
         do j=1,jdeg
c           # diagonal element of the identity operator
            trans(ihoff(ihirep)+(j-1)*(jdeg+1),1)=1
         enddo
      enddo
c
      nop=1
      call izero_wr (ngen,multed,1)
      do 3550 igen=1,ngen
c        # igen is the number of generators of the point group to
c        # use for now.  Only when the subgroup generated by the
c        # first ngen generators is found will the next generator
c        # be tried.  This technique permits the feature of
c        # detecting redundant generators.  It also tends to
c        # produce the group elements in a more logical order.
100      continue
         if (multed(igen).lt.nop) then
c           # loop over generators to be tried
c           # loop over all group elements not already multiplied by
c           # iop, including those not known at the start of the loop
            do 3560 iop=igen,1,-1
               jop=multed(iop)
110            continue
               if (jop.lt.nop) then
                  jop=jop+1
c                 # multiply iop by jop
c                 # check if nop+1 is already known
                  call opmul (ideg,issqd,mxhirep,nhirep,gen(1,iop),
     &             trans(1,jop),trans(1,nop+1))
                  kop=0
120               continue
                  kop=kop+1
                  sumsq=sqdif(trans(1,kop),trans(1,nop+1),issqd)
                  if (sumsq/issqd.gt.sqtol) goto 120
                  nop=max(nop,kop)
                  if (nop.gt.mxorder) then
                     write (6,*)
     &                'Generation of the point group operators'
                     write (6,*) 'is running away at generator',igen
                     write (6,*) 'Contact the author of this software.'
                     close (unit=1)
                     call bummer ('exiting ...',0,faterr)
                  endif
                  goto 110
               endif
               multed(iop)=jop
3560        enddo
            goto 100
         endif
3550  enddo
      return
      end
c deck getatm
      subroutine getatm (
     & atmasi,   chgi,    ctrans, fconvert,
     & loatb,    ltrans,  mtype,  mxatm,
     & mxatmsym, mxlorep, nc,     nchi,
     & nchi1,    nirep,   nop,    sqtol,
     & xyz,      xyz1)
c
c  prompt user for atomic number, label, and coordinates of atom and
c  determine coordinates of all symmetry-related atoms in the point
c  group.
c  added dummy atom with zero charge, gsk, july 1996
c
      implicit real*8 (a-h,o-z)
      parameter (nelmt=109)
      real*8 atmasi,chgi,ctrans(3,3,nop),fconvert,xyz(3,mxatm),xyz1(3)
      integer loatb(mxatm),ltrans(mxlorep+3,nirep),nc(mxatmsym)
      character*3 mtype,yn
      character*2 symbol(0:nelmt)
      real*8 amsdef(0:nelmt-1)
      data symbol /'X ',
     & 'H ','He','Li','Be','B ','C ','N ','O ','F ','Ne',
     & 'Na','Mg','Al','Si','P ','S ','Cl','Ar','K ','Ca',
     & 'Sc','Ti','V ','Cr','Mn','Fe','Co','Ni','Cu','Zn',
     & 'Ga','Ge','As','Se','Br','Kr','Rb','Sr','Y ','Zr',
     & 'Nb','Mo','Tc','Ru','Rh','Pd','Ag','Cd','In','Sn',
     & 'Sb','Te','I ','Xe','Cs','Ba','La','Ce','Pr','Nd',
     & 'Pm','Sm','Eu','Gd','Tb','Dy','Ho','Er','Tm','Yb',
     & 'Lu','Hf','Ta','W ','Re','Os','Ir','Pt','Au','Hg',
     & 'Tl','Pb','Bi','Po','At','Rn','Fr','Ra','Ac','Th',
     & 'Pa','U ','Np','Pu','Am','Cm','Bk','Cf','Es','Fm',
     & 'Md','No','Lr','Rf','Db','Sg','Bh','Hs','Mt'/
ctm  for the heavier elements it is not clear to which
ctm  isotopes the masses should refer
      data amsdef/0.0d0, 1.007825037d0,4.00260325d0,
     $ 7.0160045d0,9.0121825d0,11.0093053d0,12.d0,
     $ 14.003074008d0,15.99491464d0,18.99840325d0,
     $ 19.9924391d0,
     $ 22.9897697d0,23.9850450d0,26.9815413d0,
     $ 27.9769284d0,30.9737634d0,31.9720718d0,
     $ 34.968852729d0,39.9623831d0,
     $ 38.9637079d0,39.9625907d0,
     $ 44.9559136d0,47.9479467d0,50.9439625d0,
     $ 51.9405097d0,54.9380463d0,55.9349393d0,
     $ 58.9331978d0,57.9353471d0,62.9295992d0,
     $ 63.9291454d0,
     $ 68.9255809d0,73.9211788d0,74.9215955d0,
     $ 79.9165205d0,78.9183361d0,73*0.0d0/
30    continue
      write (6,*)
      if (chgi.lt.1000.and.chgi.gt.-100) then
         write (yn,'(i3)') nint(chgi) ! temporary use of yn
         call getint ('Enter the atomic number for this atom:',ichg,yn)
      else
         call getint ('Enter the atomic number for this atom:',ichg,' ')
      endif
      if (mtype.ne.' ') then
         yn=mtype               ! temporary use of yn
         write (6,204) yn
         read (5,'(a3)') mtype
         write(13,'(a3)') mtype
         if (mtype.eq.' ') mtype=yn
      elseif (ichg.ge.0.and.ichg.le.nelmt) then
*@ifdef f90
         write (6,204,advance='no') symbol(ichg)
204      format (' Enter the label for this atom: <',a,'>')
*@else
*         write (6,204) symbol(ichg)
*@ifdef format
*204      format (' Enter the label for this atom: <',a,'> ',$)
*@else
*  204    format (' Enter the label for this atom: <',a,'>')
*@endif
*@endif
         read (5,'(a3)') mtype
         write(13,'(a3)') mtype
         if (mtype.eq.' ') mtype=symbol(ichg)
      else
23       continue
*@ifdef f90
         write (6,'(a)',advance='no') ' Enter the label for this atom:'
*@else
*         write (6,203)
*@ifdef format
*203      format (' Enter the label for this atom: ',$)
*@else
*  203    format (' Enter the label for this atom:')
*@endif
*@endif
         read (5,'(a3)') mtype
         write(13,'(a3)') mtype
         if (mtype.eq.' ') goto 23
      endif
      chgi=ichg
      atmasi=amsdef(ichg)
      write (6,*)
      write (6,*) 'Enter the Cartesian coordinates ',
     & 'for one of these atoms: '
*@ifdef f90
      write (6,'(a)',advance='no') '   --> '
*@else
*      write (6,1900) '  --> '
*@ifdef format
*1900  format (' ',a,$)
*@else
* 1900 format (' ',a)
*@endif
*@endif
      read (5,*) xyz1
      do i=1,3
         xyz1(i)=xyz1(i)*fconvert
      enddo
      write(13,*) xyz1
      write (6,*)
      write (6,*) 'The atoms of this type are at coordinates :'
      call symatm (ctrans,fconvert,1,loatb,ltrans,mxatm,
     & mxatmsym,mxlorep,nc,nchi,nchi1,nirep,nop,sqtol,xyz,xyz1,*30)
      write (6,*)
      iyn=1
      call getyn ('Is this correct?',iyn)
      if (iyn.eq.0) goto 30
      return
      end
c deck ifrep
      function ifrep(iam2,iproj,isox,mxang2,mxatmsym,natm,nirep)
c
c  return losym irrep of so isox using projection operators iproj
c  0 is returned if not well-defined
c
      integer iproj(mxang2,mxatmsym,mxatmsym,nirep),isox(iam2,natm)
c
      ifrep=0
      do irep=1,nirep           ! loop over losym irreps
c        # project isox onto irep
         do iatm=1,natm
            do i=1,iam2
               iy=0
               do jatm=1,natm
                  iy=iy+iproj(i,iatm,jatm,irep)*isox(i,jatm)
               enddo
               if (iy.ne.0) then ! the component for AO i, atom jatm rem
                  if (ifrep.ne.0) then ! another irrep was also non0. oo
                     ifrep=0
                     goto 90
                  endif
c                 # no previous irrep was nonvanishing.
c                 # It's this one, if any.
                  ifrep=irep
                  goto 80
               endif
            enddo
         enddo
80       continue
      enddo
90    continue
      return
      end
c deck invert
      subroutine invert (iaobas,iaon,iflag,isobas,ison,mxaov,soaoy,
     & soao1,soao1s)
c
c  "invert" a piece of the soaoy matrix from iaobas+1,isobas+1 to
c  ison,ison.  Return result in the appropriate part of soao1.  iflag is
c  returned nonzero if the matrix is singular, in which case the
c  original soao1 is returned intact.
c
      implicit real*8 (a-h,o-z)
      parameter (thr4=1.0d-4)
      real*8 soaoy(mxaov,mxaov+1),soao1(mxaov,mxaov),soao1s(mxaov,mxaov)
c
      iflag=0
c     # save soao1 in case some SOs are linearly dependent
      do j=1,iaon
         do i=1,iaon
            soao1s(i,j)=soao1(i,j)
         enddo
      enddo
c     # do the inversion
      do iso=isobas+1,ison ! loop over diagonal elements (pivot point)
c        # zero the elements below pivot point
         do iao=iso+1,iaon      ! loop over rows
            a=soaoy(iso,iso)
            b=soaoy(iao,iso)
            r=sqrt(a*a+b*b)
            if (r.gt.0) then
c              # transform rows iao and iso so that
c              # soaoy(iao,iso) vanishes
               a=a/r
               b=b/r
               do k=iso,ison
                  x=soaoy(iso,k)
                  y=soaoy(iao,k)
                  soaoy(iso,k)=x*a+y*b
                  soaoy(iao,k)=y*a-x*b
               enddo
               do k=1,iaon
                  x=soao1(iso,k)
                  y=soao1(iao,k)
                  soao1(iso,k)=x*a+y*b
                  soao1(iao,k)=y*a-x*b
               enddo
            endif
         enddo
         if (abs(soaoy(iso,iso)).lt.thr4) then
            iflag=1
            do j=1,iaon
               do i=1,iaon
                  soao1(i,j)=soao1s(i,j) ! recover the previous soao1
               enddo
            enddo
            return
         endif
c        # zero elements above pivot point.
         do iao=1,iso-1
c           # transform rows iao and iso so that soaoy(iao,iso) vanishes
            a=soaoy(iao,iso)/soaoy(iso,iso)
            do k=iso,ison
               soaoy(iao,k)=soaoy(iao,k)-a*soaoy(iso,k)
            enddo
            do k=1,iaon
               soao1(iao,k)=soao1(iao,k)-a*soao1(iso,k)
            enddo
         enddo
c        # normalize pivot points to 1.
         a=1/soaoy(iso,iso)
         do k=iso,ison
            soaoy(iso,k)=soaoy(iso,k)*a
         enddo
         do k=1,iaon
            soao1(iso,k)=soao1(iso,k)*a
         enddo
      enddo
      return
      end
c deck locate
      subroutine locate(input,token,ierror)
c
c  search through input file for token to locate input for program.
c  ierror is set to 0 if no errors, 1 if any error occurs.
c
      character*10 token,token1,line
c
      token1=token
      call strupp (token1)
      rewind (unit=input,err=99)
1     continue
      read (unit=input,fmt='(a10)',end=99,err=99) line
      call strupp (line)
      if (line.ne.token1) goto 1
      ierror=0
      return
99    continue
      ierror=1
      return
      end
c deck loprj
      subroutine loprj (
     & iam,      icomp,   iproj,  isend,
     & ltrans,   mxang,   mxang2, mxang3,
     & mxatmsym, mxlorep, nc,     nirep)
c
c  form losym projection operators iproj.  isend(iatm,iopl) is where
c  atom iatm gets sent by losym operation iopl.  ltrans is the diagonal
c  elements of the transformation matrices of the losym point group in
c  x, y, and z coordiantes.  iam is the angular momentum to use.
c
      implicit real*8 (a-h,o-z)
      integer icomp(mxang3,mxang2,mxang),
     & iproj(mxang2,mxatmsym,mxatmsym,nirep),isend(mxatmsym,mxlorep),
     & ltrans(mxlorep+3,mxlorep)
c
      iam2=iam*(iam+1)/2
      do irep=1,nirep
         do iatm=1,nc
            do jatm=1,nc
               do j=1,iam2
                  iproj(j,jatm,iatm,irep)=0
               enddo
            enddo
         enddo
      enddo
      do iopl=1,nirep
         do iatm=1,nc
            jatm=isend(iatm,iopl)
            do i=1,iam2
               jx=1
               do ifact=1,iam-1
                  jx=jx*ltrans(icomp(ifact,i,iam),iopl)
               enddo
               do irep=1,nirep
                  iproj(i,jatm,iatm,irep)=iproj(i,jatm,iatm,irep)+
     &             jx*ltrans(irep+3,iopl)
               enddo
            enddo
         enddo
      enddo
      return
      end
c deck opmul
      subroutine opmul (ideg,issqd,mxhirep,nhirep,op1,op2,op3)
c
c  multiply group operation transformation matrices op1 and op2 to get
c  op3
c
      implicit real*8 (a-h,o-z)
      real*8 op1(issqd+9),op2(issqd+9),op3(issqd+9)
      integer ideg(mxhirep)
c
      ioff=1
      do ihirep=1,nhirep
         jdeg=ideg(ihirep)
c       call matmul (op1(ioff),op2(ioff),op3(ioff),
c    &               jdeg,jdeg,jdeg,jdeg,jdeg,jdeg)
         call wzero(jdeg*jdeg,op3(ioff),1)
         call gmxm(op1(ioff),jdeg,op2(ioff),jdeg,op3(ioff),jdeg,1.0d0)
         ioff=ioff+jdeg*jdeg
      enddo
      return
      end
c deck polymult
      subroutine polymult (iprd,mxang1,mxang3,n,x1,xn,yn)
c
c  multiply 1st degree polynomial x1 by (n-1)th degree polynomial xn to
c  nth degree polynomial yn
c
      implicit real*8 (a-h,o-z)
      real*8 x1(3),xn(n*(n+1)/2),yn((n+1)*(n+2)/2)
      integer iprd(3,mxang1,mxang3)
c
      call wzero (((n+1)*(n+2))/2,yn,1)
      do i1=1,3
         do in=1,(n*(n+1))/2
            ip=iprd(i1,in,n)
            yn(ip)=yn(ip)+x1(i1)*xn(in)
         enddo
      enddo
      return
      end
c deck rdtrans
      subroutine rdtrans (jdeg,nf,trans)
c
c  read an irrep transformation matrix from point group library
c
      implicit real*8 (a-h,o-z)
      real*8 trans(jdeg,jdeg)
      integer num(9)
      character*2 trnc(9)
      parameter (quar=0.25d0, half=0.5d0, one=1.0d0, two=2.0d0,
     & three=3.0d0, four=4.0d0, five=5.0d0, eight=8.0d0)
c
c     # bummer error type.
      integer faterr
      parameter(faterr=2)
c
      tau=(sqrt(five)+one)/two
      do i=1,jdeg
         read (nf,'(9(1x,i2,a2))') (num(j),trnc(j),j=1,jdeg)
         do j=1,jdeg
            if (trnc(j).eq.'  ') then
               x=one
            elseif (trnc(j).eq.'/2') then
               x=half
            elseif (trnc(j).eq.'/4') then
               x=quar
            elseif (trnc(j).eq.'R2') then
               x=sqrt(two)/two
            elseif (trnc(j).eq.'R3') then
               x=sqrt(three)/four
            elseif (trnc(j).eq.'R5') then
               x=sqrt(five)/four
            elseif (trnc(j).eq.'C5') then
               x=(tau-one)/two
            elseif (trnc(j).eq.'CF') then
               x=-tau/two
            elseif (trnc(j).eq.'S5') then
               x=sqrt(two+tau)/two
            elseif (trnc(j).eq.'SF') then
               x=sqrt(three-tau)/two
            elseif (trnc(j).eq.'T1') then
               x=tau*sqrt(three)/four
            elseif (trnc(j).eq.'T2') then
               x=(tau+one)/four
            elseif (trnc(j).eq.'TI') then
               x=sqrt(three)*(tau-one)/eight
            elseif (trnc(j).eq.'TT') then
               x=(two-tau)/four
            elseif (trnc(j).eq.'TD') then
               x=(three*tau-one)/eight
            else
               write (6,*) 'Error in transformation matrix file!'
               write (6,*) 'Contact the author of this software.'
               close (unit=1)
               call bummer ('exiting ...',0,faterr)
            endif
            trans(i,j)=x*num(j)
         enddo
      enddo
      return
      end
c deck rmcon
      subroutine rmcon (
     & eta,      icon,   iconu,    lmnp1,
     & mcons,    mxatm,  mxconatm, mxcons,
     & mxconset, mxprim, ncons,    nf,
     & nrcr,     nshi,   zet)
c
c  If there are no refernces to contraction set icon, remove it.
c
      implicit real*8 (a-h,o-z)
      real*8 eta(mxconset,mxprim,mxcons),zet(mxprim,mxcons)
      integer iconu(mxcons),lmnp1(mxcons),mcons(mxconatm,mxatm),
     & nf(mxatm),nrcr(mxcons)
c     # check if this contraction set is used anywhere else
      do jshi=1,nshi
         do jc=1,nf(jshi)
            if (mcons(jc,jshi).eq.icon) goto 291
         enddo
      enddo
c     # change contraction set pointers on all atoms wherever needed
      do jshi=1,nshi
         do jc=1,nf(jshi)
            if (mcons(jc,jshi).gt.icon) mcons(jc,jshi)=mcons(jc,jshi)-1
         enddo
      enddo
c     # remove contraction set from list
      ncons=ncons-1
      do jcon=icon,ncons
         iconu(jcon)=iconu(jcon+1)
         lmnp1(jcon)=lmnp1(jcon+1)
         nrcr(jcon)=nrcr(jcon+1)
         do k=1,iconu(jcon)
            zet(k,jcon)=zet(k,jcon+1)
            do j=1,nrcr(jcon)
               eta(j,k,jcon)=eta(j,k,jcon+1)
            enddo
         enddo
      enddo
291   continue
      return
      end
c deck rmecp
      subroutine rmecp (
     & ccr,   cls,   icrs,   izcr,
     & lcru,  llsu,  mxang,  mxang3,
     & mcrs,  mxatm, mxbfcr, mxcrs,
     & nbfcr, nbfls, ncr,    ncrs,
     & nls,   nshi,  zcr,    zls)
c
c  If there are no references to core potential icrs, remove it.
c
      implicit real*8 (a-h,o-z)
      real*8 ccr(mxbfcr,0:mxang3,mxcrs),cls(mxbfcr,mxang,mxcrs),
     & zcr(mxbfcr,0:mxang3,mxcrs),zls(mxbfcr,mxang,mxcrs)
      integer izcr(mxcrs),lcru(mxcrs),llsu(mxcrs),mcrs(mxatm),
     & nbfcr(0:mxang3,mxcrs),nbfls(mxang,mxcrs),
     & ncr(mxbfcr,0:mxang3,mxcrs),nls(mxbfcr,mxang,mxcrs)
c
c  check if this pseudopotential used anywhere else
c
      do jshi=1,nshi
         if (mcrs(jshi).eq.icrs) goto 290
      enddo
c     # change pseudopotential pointers on all atoms wherever needed
      do jshi=1,nshi
         if (mcrs(jshi).gt.icrs) mcrs(jshi)=mcrs(jshi)-1
      enddo
c     # remove pseudopotential from list
      ncrs=ncrs-1
      do jcrs=icrs,ncrs
         lcru(jcrs)=lcru(jcrs+1)
         do l=0,lcru(jcrs)
            nbfcr(l,jcrs)=nbfcr(l,jcrs+1)
            do k=1,nbfcr(l,jcrs)
               ncr(k,l,jcrs)=ncr(k,l,jcrs+1)
               zcr(k,l,jcrs)=zcr(k,l,jcrs+1)
               ccr(k,l,jcrs)=ccr(k,l,jcrs+1)
            enddo
         enddo
         izcr(jcrs)=izcr(jcrs+1)
         llsu(jcrs)=llsu(jcrs+1)
         do l=1,llsu(jcrs)
            nbfls(l,jcrs)=nbfls(l,jcrs+1)
            do k=1,nbfls(l,jcrs)
               nls(k,l,jcrs)=nls(k,l,jcrs+1)
               zls(k,l,jcrs)=zls(k,l,jcrs+1)
               cls(k,l,jcrs)=cls(k,l,jcrs+1)
            enddo
         enddo
      enddo
290   continue
      return
      end
c deck rmhso
      subroutine rmhso (
     & hiso,  ihigcs, ihiord,   jhigcs,
     & mxaov, mxatm,  mxconatm, mxsoset,
     & nf,    nhigcs, nshi,     nsymtp)
c
c  Check if HSO set jhigcs is unused.  If so, remove it.
c
      implicit real*8 (a-h,o-z)
      real*8 hiso(mxaov,mxaov,mxsoset)
      integer ihigcs(mxconatm,mxatm),ihiord(mxaov,mxsoset),nf(mxatm),
     & nsymtp(mxsoset)
c     # check if this HSO set is used anywhere else
      if (jhigcs.le.0) goto 69
      do jshi=1,nshi
         do jc=1,nf(jshi)
            if (ihigcs(jc,jshi).eq.jhigcs) goto 69
         enddo
      enddo
c     # change HSO set pointers on all atoms wherever needed
      do jshi=1,nshi
         do jc=1,nf(jshi)
            if (ihigcs(jc,jshi).gt.jhigcs)
     &       ihigcs(jc,jshi)=ihigcs(jc,jshi)-1
         enddo
      enddo
c     # remove HSO set from list
      nhigcs=nhigcs-1
      do khigcs=jhigcs,nhigcs
         nsymtp(khigcs)=nsymtp(khigcs+1)
         do ihso=1,nsymtp(khigcs)
            ihiord(ihso,khigcs)=ihiord(ihso,khigcs+1)
            do ilso=1,nsymtp(khigcs)
               hiso(ihso,ilso,khigcs)=hiso(ihso,ilso,khigcs+1)
            enddo
         enddo
      enddo
69    continue
      return
      end
c deck rmlso
      subroutine rmlso (
     & icsu,  ictu,     igcs, ihiatm,
     & iirep, isocoef,  jgcs, mxao,
     & mxatm, mxconatm, mxso, mxsoset,
     & nf,    ngcs,     ns)
c
c  If there are no references to LSO set jgcs, remove it.
c
      integer icsu(mxsoset),ictu(mxsoset),igcs(mxconatm,mxatm),
     & ihiatm(mxatm),iirep(mxso,mxsoset),isocoef(mxao,mxso,mxsoset),
     & nf(mxatm)
c     # check if this set of LSOs is used elsewhere
      if (jgcs.le.0) goto 70
      do js=1,ns
         do jc=1,nf(ihiatm(js))
            if (igcs(jc,js).eq.jgcs) goto 70
         enddo
      enddo
c     # change LSO set pointers wherever needed
      do js=1,ns
         do jc=1,nf(ihiatm(js))
            if (igcs(jc,js).gt.jgcs) igcs(jc,js)=igcs(jc,js)-1
         enddo
      enddo
c     # remove LSO set from list
      ngcs=ngcs-1
      do kgcs=jgcs,ngcs
         icsu(kgcs)=icsu(kgcs+1)
         ictu(kgcs)=ictu(kgcs+1)
         do iso=1,icsu(kgcs)
            iirep(iso,kgcs)=iirep(iso,kgcs+1)
            do iao=1,ictu(kgcs)
               isocoef(iao,iso,kgcs)=isocoef(iao,iso,kgcs+1)
            enddo
         enddo
      enddo
70    continue
      return
      end
c deck sameso
      subroutine sameso (
     & hiso,   iam2,     icon,   icsu,
     & ictu,   igcs,     ihiatm, ihigcs,
     & ihiord, iirep,    is,     isocoef,
     & jgcs,   loatmbas, mxao,   mxaov,
     & mxatm,  mxconatm, mxso,   mxsoset,
     & nc,     nf,       ngcs,   nhigcs,
     & ns,     nshi,     nsymtp, xyz)
c
c  check if SO set jgcs is used any place other than losym atom type is,
c  contraction set icon.  If jgcs is 0, allocate memory for new SO set.
c
      implicit real*8 (a-h,o-z)
      real*8 hiso(mxaov,mxaov,mxsoset),xyz(3,mxatm+1,mxatm)
      integer icsu(mxsoset),ictu(mxsoset),igcs(mxconatm,mxatm),
     & ihiatm(mxatm),ihigcs(mxconatm,mxatm),ihiord(mxaov,mxsoset),
     & iirep(mxso,mxsoset),isocoef(mxao,mxso,mxsoset),loatmbas(mxatm),
     & nc(mxatm),nf(mxatm),nsymtp(mxsoset)
c
      iaffect=0
      if (jgcs.gt.0) then
         do js=1,ns             ! loop over losym atoms
            jshi=ihiatm(js)
            do jcn=1,nf(jshi)   ! loop over contraction sets
               if (igcs(jcn,js) .eq. jgcs .and.
     &          (jcn .ne. icon .or. is .ne. js)) then
c                 # Another reference to this SO set has been found.
                  if (iaffect.eq.0) then
                     iaffect=1
                  endif
                  if (is.ne.js) then
                     do iatm=loatmbas(js)+1,loatmbas(js)+nc(js)
                        write (6,*) (xyz(j,iatm,jshi),j=1,3)
                     enddo
                  endif
               endif
            enddo
         enddo
      elseif (ngcs.lt.mxsoset.and.iam2*nc(is).le.mxao) then
c        # allocate new SO
         ngcs=ngcs+1
         jgcs=ngcs
         igcs(icon,is)=jgcs
         icsu(jgcs)=0
         ictu(jgcs)=nc(is)*iam2
      else
         write (6,*) 'There is no memory for more SOs.'
      endif
      if (iaffect.ne.0) then    ! There is another reference to SO set j
         if (ngcs.lt.mxsoset) then
c           # inquire if user wants any changes to SO set jgcs
c           # to affect all refere
            write (6,*)
            iyn=1
ca
ca          write (6,*)
ca     &      'Do you want the changes on this set of SOs to apply'
ca          call getyn ('to the other contraction set(s) as well?',iyn)
ca
ca block the option for the time being, so to apply on any case for
ca descending point groups, ie. force constant calculation.
ca
ctm       iyn=0
ca
            if (iyn.eq.0) then  ! User says no.  Allocate new SO set.
               ngcs=ngcs+1
               icsu(ngcs)=icsu(jgcs)
               ictu(ngcs)=ictu(jgcs)
c              # copy old SO set to new SO set
               do iso=1,icsu(jgcs)
                  iirep(iso,ngcs)=iirep(iso,jgcs)
                  do iao=1,ictu(jgcs)
                     isocoef(iao,iso,ngcs)=isocoef(iao,iso,jgcs)
                  enddo
               enddo
               jgcs=ngcs
               igcs(icon,is)=jgcs ! set up pointer to new SO set.
               iaffect=0
            endif
         else
            write (6,*) 'Warning: There is no more room for more'
            write (6,*) 'sets of SO coefficients.  All changes will'
            write (6,*) 'apply to the contraction set(s) above.'
         endif
      endif
c     # undefine all HSO sets that depend on this LSO set
      do js=1,ns
         jshi=ihiatm(js)
         do jcn=1,nf(jshi)
            if (igcs(jcn,js).eq.jgcs) then
               jhigcs=ihigcs(jcn,jshi)
               ihigcs(jcn,jshi)=0
               call rmhso (
     &          hiso,  ihigcs, ihiord,   jhigcs,
     &          mxaov, mxatm,  mxconatm, mxsoset,
     &          nf,    nhigcs, nshi,     nsymtp)
            endif
         enddo
      enddo
      return
      end
c deck sqdif
      function sqdif( a, b, n )
c     # returns the sum of the squares of the differences between
c     # the elements of the vectors a(:) and b(:)
       implicit none 
       integer n
      real*8 a(n), b(n), sqdif
      integer i
      real*8 zero
      parameter(zero=0d0)
      sqdif = zero
      do i = 1, n
         sqdif = sqdif + (a(i) - b(i))**2
      enddo
      return
      end
c deck symatm
      subroutine symatm (
     & ctrans,  fconvert, iflag,    loatb,
     & ltrans,  mxatm,    mxatmsym, mxlorep,
     & nc,      nchi,     nchi1,    nirep,
     & nop,     sqtol,    xyz,      xyz1,
     & *)
c
c  Determine coordinates of all symmetry-related atoms of an atom at
c  coordinates xyz1 in the point group.
c
c  The strategy for generating new locations is that every time a new
c  location is found from a hisym operation, stop and operate on that
c  location with all losym operations.  This guarantees that each set
c  of losym-related coordinates is contiguous.
c
      implicit real*8 (a-h,o-z)
      real*8 ctrans(3,3,nop),xyz(3,mxatm),xyz1(3)
      integer loatb(mxatm),ltrans(mxlorep+3,nirep),nc(mxatmsym)
c
      nchi=0
      nchi1=0
      do iop=1,nop              ! loop over hisym operations
c        # operate on original location with hisym operation
         do i=1,3
            a=0
            do j=1,3
               a=a+ctrans(i,j,iop)*xyz1(j)
            enddo
            xyz(i,nchi1+1)=a
         enddo
         iatm=0
c        # check if newly generated location is already occupied
40       continue
         iatm=iatm+1
         sumsq=sqdif(xyz(1,iatm),xyz(1,nchi1+1),3)
         if (sumsq.gt.sqtol) goto 40
         if (iatm.gt.nchi1) then ! empty.  new atom.
            nchi=nchi+1
            nc(nchi)=1
            loatb(nchi)=nchi1
            nchi1=iatm
            if (nchi1.gt.mxatm) then
               write (6,*) 'The point group geometry gives rise to more'
               write (6,*) 'symmetry-related atoms of this type than'
               write (6,*) 'ARGOS can handle!'
               return 1
            endif
            if (iflag.ne.0) write (6,*) (xyz(j,nchi1)/fconvert,j=1,3)
            do iopl=2,nirep     ! loop over losym operations
c              # operate on atom loatb(nchi)+1 with losym operation
               do j=1,3
                  xyz(j,nchi1+1)=xyz(j,loatb(nchi)+1)*ltrans(j,iopl)
               enddo
               iatm=loatb(nchi)
c              # check if newly generated location is already occupied
38             continue
               iatm=iatm+1
               sumsq=sqdif(xyz(1,iatm),xyz(1,nchi1+1),3)
               if (sumsq.gt.sqtol) goto 38
               if (iatm.gt.nchi1) then ! empty.  new atom.
                  nchi1=iatm
                  nc(nchi)=nc(nchi)+1
                  if (nchi1.gt.mxatm.or.nc(nchi).gt.mxatmsym) then
                     write (6,*)
     &                'The point group geometry gives rise to'
                     write (6,*) 'more symmetry-related atoms of this'
                     write (6,*) 'type than ARGOS can handle!'
                     return 1
                  endif
                  if (iflag .ne. 0)
     &             write (6,*) (xyz(j,nchi1)/fconvert,j=1,3)
               endif
            enddo
         endif
      enddo
      return
      end
c deck defsph
      subroutine defsph (
     & blkshft, blshco,  chi,      ctrans,
     & hiso,    icomp,   icsu,     ictu,
     & ideg,    igcs,    ihatmbas, ihiatm,
     & ihigcs,  ihiord,  ihisym,   ihsamep,
     & ihsend,  iirep,   ipiv,     iprd,
     & iproj,   isamep,  isend,    ishi,
     & isocoef, lco,     lmnp1,    loatmbas,
     & ltrans,  mcons,   mxang,    mxang1,
     & mxang2,  mxang3,  mxang4,   mxao,
     & mxaov,   mxatm,   mxatmsym, mxconatm,
     & mxcons,  mxhirep, mxlorep,  mxorder,
     & mxso,    mxsoset, nc,       nchi,
     & nchi1,   nf,      ngcs,     nhigcs,
     & nhirep,  nhsamep, nirep,    nop,
     & ns,      nsamep,  nshi,     nsymtp,
     & poly1,   poly2,   projao,   soaox,
     & soaoy,   soao1,   soao1s,   sox,
     & sqtol,   transao, xyz)
c
c  converting CAOs into SAOs with well-defined l2 and lz
c
      implicit real*8 (a-h,o-z)
      real*8 blkshft(mxaov,mxaov),blshco(mxorder),chi(mxhirep,mxorder),
     & ctrans(3,3,mxorder),hiso(mxaov,mxaov,mxsoset),poly1(mxang2),
     & poly2(mxang2),projao(mxaov,mxaov,mxhirep),soaox(mxaov,mxaov),
     & soaoy(mxaov,mxaov+1),soao1(mxaov,mxaov),soao1s(mxaov,mxaov),
     & sox(mxaov),transao(mxaov,mxaov),xyz(3,mxatm+1,mxatm)
      integer icomp(mxang3,mxang2,mxang),icsu(mxsoset),ictu(mxsoset),
     & ideg(mxhirep),igcs(mxconatm,mxatm),ihatmbas(mxatm),
     & ihiatm(mxatm),ihigcs(mxconatm,mxatm),ihiord(mxaov,mxsoset),
     & ihsamep(mxatm),ihsend(mxatm,mxorder,mxatm),iirep(mxso,mxsoset),
     & ipiv(mxaov),iprd(3,mxang1,mxang3),
     & iproj(mxang2,mxatmsym,mxatmsym,mxlorep),isamep(mxatm,mxatm),
     & isend(mxatmsym,mxlorep,mxatm),isocoef(mxao,mxso,mxsoset),
     & lco(mxang2,mxang4,mxang),lmnp1(mxcons),loatmbas(mxatm),
     & ltrans(mxlorep+3,nirep),mcons(mxconatm,mxatm),nc(mxatm),
     & nchi(mxatm),nchi1(mxatm),nf(mxatm),nsamep(mxatm),nsymtp(mxsoset)
      character*8 option
      character*72 msg
c
c     # bummer error type.
      integer   faterr
      parameter(faterr=2)
c
c     # determine which other kinds of atoms have the same permutations
c     # as this one (ishi) in the hisym point group.  Result returned
c     # in ihsamep.
c
      call fsamep (
     & ihatmbas(ishi), ihsamep, ihsend, isamep,
     & isend,          ishi,    mxatm,  mxatmsym,
     & mxlorep,        mxorder, nc,     nchi(ishi),
     & nchi1,          nhsamep, nirep,  nop,
     & ns,             nsamep,  nshi)
301   continue
c
c     # define LSOs and (if applicable) HSOs for hisym atom type ishi.
c
      call defso (
     & blkshft,          blshco,      chi,      ctrans,
     & hiso,             icomp,       icsu,     ictu,
     & ideg,             igcs,        ihatmbas, ihiatm,
     & ihigcs,           ihiord,      ihisym,   ihsamep,
     & ihsend(1,1,ishi), iirep,       ipiv,     iprd,
     & iproj,            isamep,      isend,    ishi,
     & isocoef,          lmnp1,       ltrans,   mcons,
     & mxang,            mxang1,      mxang2,   mxang3,
     & mxao,             mxaov,       mxatm,    mxatmsym,
     & mxconatm,         mxcons,      mxhirep,  mxlorep,
     & mxorder,          mxso,        mxsoset,  nc,
     & nchi(ishi),       nchi1(ishi), nf,       ngcs,
     & nhigcs,           nhirep,      nhsamep,  nirep,
     & nop,              nsamep,      nsymtp,   poly1,
     & poly2,            projao,      soaox,    soaoy,
     & soao1,            soao1s,      sox,      sqtol,
     & transao)
c
302   continue
      do icon=1,nf(ishi)
         jcon=mcons(icon,ishi)
         msg='Insufficient memory'
         do is=ihatmbas(ishi)+1,ihatmbas(ishi)+nchi(ishi)
            if (igcs(icon,is).le.0) goto 309
         enddo
         msg='Linearly dependent'
         if (ihigcs(icon,ishi).lt.0) goto 309
         msg='Incompatible with HISYM'
         if (ihigcs(icon,ishi).gt.0.or.ihisym.eq.0) msg='OK'
309      continue
      enddo
310   continue
      option='DEFINE'
      icon=0
350   icon=icon+1
c
      if (icon.le.0.or.icon.gt.nf(ishi)) goto 3500
c
      jcon=mcons(icon,ishi)
      iam=lmnp1(jcon)
      iam2=iam*(iam+1)/2
      option='Z'
      do is=ihatmbas(ishi)+1,ihatmbas(ishi)+nchi(ishi)
         jgcs=igcs(icon,is)
         write (6,*)
c
c        # check if SO set jgcs is used any place other than losym atom
c        # type is contraction set icon.  If jgcs is 0, allocate memory
c        # for new SO set.
c
         call sameso (
     &    hiso,   iam2,     icon,   icsu,
     &    ictu,   igcs,     ihiatm, ihigcs,
     &    ihiord, iirep,    is,     isocoef,
     &    jgcs,   loatmbas, mxao,   mxaov,
     &    mxatm,  mxconatm, mxso,   mxsoset,
     &    nc,     nf,       ngcs,   nhigcs,
     &    ns,     nshi,     nsymtp, xyz)
         if (jgcs.le.0) goto 352
         if (option.eq.'Z') then
            icsu(jgcs)=(iam*2-1)*nc(is)
            do iatm=0,nc(is)-1
               do jatm=0,nc(is)-1
                  ix=iand(iatm,jatm)
                  jx=1
                  if (ix.eq.1.or.ix.eq.2.or.ix.eq.4.or.ix.eq.7) jx=-1
                  do iso=1,iam*2-1
                     do j=1,iam2
                        isocoef(iatm*iam2+j,jatm*(2*iam-1)+iso,jgcs)=
     &                   jx*lco(j,iso,iam)
                     enddo
                  enddo
               enddo
            enddo
         endif
c        # determine the symmetries of the new SOs
         call loprj (
     &    iam,      icomp,   iproj,  isend(1,1,is),
     &    ltrans,   mxang,   mxang2, mxang3,
     &    mxatmsym, mxlorep, nc(is), nirep)
         do iso=1,icsu(jgcs)
            iirep(iso,jgcs)=ifrep(iam2,iproj,isocoef(1,iso,jgcs),
     &       mxang2,mxatmsym,nc(is),nirep)
            if (iirep(iso,jgcs).eq.0) then
               write (6,*) 'An SO does not belong to an irrep!'
               write (6,*) 'Contact the author of this software.'
               call bummer ('exiting ...',0,faterr)
            endif
         enddo
352      continue
      enddo
      goto 350
3500  continue
      call defso (
     & blkshft,          blshco,      chi,      ctrans,
     & hiso,             icomp,       icsu,     ictu,
     & ideg,             igcs,        ihatmbas, ihiatm,
     & ihigcs,           ihiord,      ihisym,   ihsamep,
     & ihsend(1,1,ishi), iirep,       ipiv,     iprd,
     & iproj,            isamep,      isend,    ishi,
     & isocoef,          lmnp1,       ltrans,   mcons,
     & mxang,            mxang1,      mxang2,   mxang3,
     & mxao,             mxaov,       mxatm,    mxatmsym,
     & mxconatm,         mxcons,      mxhirep,  mxlorep,
     & mxorder,          mxso,        mxsoset,  nc,
     & nchi(ishi),       nchi1(ishi), nf,       ngcs,
     & nhigcs,           nhirep,      nhsamep,  nirep,
     & nop,              nsamep,      nsymtp,   poly1,
     & poly2,            projao,      soaox,    soaoy,
     & soao1,            soao1s,      sox,      sqtol,
     & transao)
c
      return
      end
