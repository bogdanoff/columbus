!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
module diis_mod

   ! shared data and subprograms for diis interpolation.
   ! module to compute diis solutions.

   ! 11-sep-2006 written by ron shepard.

   use precision_defs

   type diis_info
      integer :: frequency            ! perform diis interpolation this often.
      integer :: num_max              ! the maximum number of vectors in the interpolation space.
      integer :: num_vector           ! the number of vectors in the space.
      integer :: last                 ! index of the last vector added.

      real(wp) :: lambda              ! |r|^2 from the last solution.

      real(wp), allocatable :: E(:,:) ! error vectors.
      real(wp), allocatable :: X(:,:) ! coordinate vectors.
      real(wp), allocatable :: c(:)   ! least-squares coefficients.
   end type diis_info

   private :: diis_normal

contains
   subroutine diis_init( diis, nrow, num_max, frequency )

      ! allocate the arrays associated with a diis data structure and
      ! initialize the scalar variables.

      ! 11-sep-2006 written by ron shepard.

      use precision_defs
      implicit none
      type(diis_info), intent(out) :: diis
      integer, intent(in) :: nrow
      integer, intent(in) :: num_max
      integer, intent(in) :: frequency

      allocate( diis%c(num_max), diis%E(nrow,num_max), diis%X(nrow,num_max) )
      diis%frequency  = frequency
      diis%num_max    = num_max
      diis%num_vector = 0
      diis%last       = 0
      diis%lambda     = -1.0_wp
      return
   end subroutine diis_init

   subroutine diis_close( diis )

      ! deallocate any arrays associated with the diis structure.
      ! this should be redundant with f2003 (and with f95+allocatableTR), but it
      ! is included here for f90 implementations that use pointers.

      ! 11-sep-2006 written by ron shepard.

      use precision_defs
      implicit none
      type(diis_info), intent(inout) :: diis

      deallocate( diis%c, diis%E, diis%X )

      return
   end subroutine diis_close

   subroutine diis_interpolate( diis, result, lambda, ierr )

      ! solve the least squares equation and compute the interpolated solution.

      ! 11-sep-2006 written by ron shepard.

      use precision_defs
      implicit none
      type(diis_info), intent(inout) :: diis
      real(wp), intent(out) :: result(:)  ! interpolated output vector.
      real(wp), intent(out), optional :: lambda     ! |r|^2 from the least-squares error.
      integer, intent(out), optional :: ierr

      integer :: n

      if ( present( lambda ) ) then
         lambda = -1.0_wp
      endif

      ! solve the least-squares equation.

      n = diis%num_vector
      call diis_normal( diis%E(:,1:n), diis%c(1:n), diis%lambda, ierr )
      if ( present( ierr ) ) then
         if ( ierr .ne. 0 ) return
      endif

      ! interpolate the solution.
      result = matmul( diis%X(:,1:n), diis%c(1:n) )

      if ( present( lambda ) ) then
         lambda = diis%lambda
      endif

      return
   end subroutine diis_interpolate

   subroutine diis_add_vector( diis, x, e )

      ! add a new coordinate vector and error vector pair to the diis structure.

      ! 11-sep-2006 written by ron shepard.

      use precision_defs
      implicit none
      type(diis_info), intent(inout) :: diis
      real(wp), intent(in) :: x(:) ! new coordinate vector.
      real(wp), intent(in) :: e(:) ! new error vector.

      diis%num_vector = min( diis%num_vector + 1, diis%num_max )  ! total number of vectors in the space.
      diis%last = mod( diis%last, diis%num_max ) + 1   ! circular buffer index.
      diis%E(:,diis%last) = e(:)
      diis%X(:,diis%last) = x(:)

      return
   end subroutine diis_add_vector

   subroutine diis_normal( E, c, lambda, ierr )

      use precision_defs
      use bummer_mod

      implicit none

      real(wp), intent(in)  :: E(:,:)  ! (m,n)  n error vectors of length m.
      real(wp), intent(out) :: c(:)    ! c(1:n) output coefficients.
      real(wp), intent(out) :: lambda  ! diis lagrange multiplier; lambda=cT.B.c=|r|^2
      integer, intent(out), optional :: ierr

      ! solve the traditional formulation in terms of normal equations.
      !    ( B  -u) (c)      = ( 0)
      !    (-u^T 0) (lambda)   (-1)
      ! with u=(/1,1,1...1/).

      integer :: n, np1, info, i, j, ij
      integer :: ipiv( size(c) + 1 )
      real(wp) :: z( size(c) + 1 )
      real(wp) :: B( ((size(c) + 1) * (size(c) + 2)) / 2 )

      if( present(ierr) ) ierr = 0
      c(:) = 0.0_wp
      lambda = -1.0_wp

      n   = size(c)

      if ( n .ne. size(E,dim=2) ) then
         if( present(ierr) ) then
            ierr = -1
            return
         endif
         call bummer('diis: E.c is inconsistent. size(c)-size(E,dim=2)',n-size(E,dim=2),fatal_err)
      endif

      if ( n .eq. 1 ) then
         c(1) = 1.0_wp  ! the solution is determined entirely by the constraint. 
         lambda = dot_product(E(:,1),E(:,1))
      else
         np1 = n + 1
         
         ij = 0
         do i = 1, n
            do j = 1, i
               ij = ij + 1
               B(ij) = dot_product( E(:,i), E(:,j) )
            enddo
         enddo
         do i = 1, n
            ij = ij + 1
            B(ij) = -1.0_wp
         enddo
         ij = ij + 1
         B(ij) = 0.0_wp
         if ( ij .ne. size(B) ) call bummer('diis: ij incorrect', ij-size(B), fatal_err)
         
         ! set z(:) to the right hand side vector.
         z(:)   = 0.0_wp
         z(n+1) = -1.0_wp

         ! this is an indefinite linear equation system.
         call dspsv( 'U', np1, 1, B, ipiv, z, np1, info )
         if ( info .ne. 0 ) then
            if( present(ierr) ) then
               ierr = info
               return
            endif
            call bummer('diis_normal() info=',info,fatal_err)
         endif
         
         c(:)   = z(1:n)
         lambda = z(n+1)
      endif

      return
   end subroutine diis_normal
end module diis_mod
