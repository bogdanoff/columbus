!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      subroutine strlow( string )
c
c  change all upper case characters in string to lower
c
      character*(*)     string
c
c  the ascii and ebcdic versions exists, not to make the routine faster,
c  but to make it less case sensitive
c
      integer           istr
c
#if defined(FORTRAN)
c     # case-dependent standard fortran version.
c     # warning: case dependent code
      integer          indx
      character*26     lower
      character*26     upperc
      data lower  / 'abcdefghijklmnopqrstuvwxyz' /
      data upperc / 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' /
c
      do 100 istr = 1, len(string)
         indx = index(upperc, string(istr:istr) )
         if ( indx .ne. 0 )
     $        string(istr:istr) = lower(indx:indx)
 100  continue
#elif defined(EBCDIC)
c     # ebcdic version.
c     # this code assumes that ichar() returns ebcdic values.
      integer           ich
      integer           uldiff
      parameter ( uldiff = -64 )
      integer   sets
      parameter ( sets = 3 )
      integer   low   ( sets )
      integer   high  ( sets )
      data low  / 193, 209, 226 /
      data high / 201, 217, 233 /
c
c
      do 100 istr = 1, len(string)
         ich = ichar( string(istr:istr) )
         if ( ( low(1) .le. ich  .and.  ich .le. high(1) ) .or.
     $        ( low(2) .le. ich  .and.  ich .le. high(2) ) .or.
     $        ( low(3) .le. ich  .and.  ich .le. high(3) ) )
     $        string(istr:istr) = char( ich + uldiff )
 100  continue
#else 
c     # ascii version.
c     # this code assumes that ichar() returns ascii values.
      integer           ich
      integer uppa, uppz, uldiff
      parameter ( uppa = 65 )
      parameter ( uppz = 90 )
      parameter ( uldiff = 32 )
c
      do 100 istr = 1, len(string)
         ich = ichar( string(istr:istr) )
         if ( uppa .le. ich  .and.  ich .le. uppz )
     $        string(istr:istr) = char( ich + uldiff )
 100  continue
#endif 
      return
      end
