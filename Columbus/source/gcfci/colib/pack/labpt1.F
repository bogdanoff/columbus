!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      subroutine labpt1(word,lab,ipos)
c
c  this routine puts the integer label 'lab' into the 'ipos' position
c  of the working precision word 'word'.
c
      implicit integer(a-z)
#if defined(INT64)
      integer iw16(4),word
      integer m16
      parameter(m16=2**16-1)
#  if defined(T3E64) || defined(CRAY)
      integer dpack4
      dpack4(i1,i2,i3,i4)=
     +  or(shiftl(or(shiftl(i1,16),i2),32),or(shiftl(i3,16),i4))
c
      iw16(1)=shiftr(word,48)
      iw16(2)=and(shiftr(word,32),m16)
      iw16(3)=and(shiftr(word,16),m16)
      iw16(4)=and(word,m16)
      iw16(ipos)=and(lab,m16)
      word=dpack4(iw16(1),iw16(2),iw16(3),iw16(4))
#  elif defined(DECALPHA) || defined(SGIPOWER)
      integer dpack4
      dpack4(i1,i2,i3,i4)=
     +  ior(ishft(ior(ishft(i1,16),i2),32),ior(ishft(i3,16),i4))
c
      iw16(1)=ishft(word,-48)
      iw16(2)=iand(ishft(word,-32),m16)
      iw16(3)=iand(ishft(word,-16),m16)
      iw16(4)=iand(word,m16)
      iw16(ipos)=iand(lab,m16)
      word=dpack4(iw16(1),iw16(2),iw16(3),iw16(4))
#  else 
       call bummer ('labpt1 not implemented',0,2)
#  endif 
#else 
      real*8 word
      integer*2 iw16(4)
      real*8 dpword
      equivalence (iw16(1),dpword)
      dpword=word
      iw16(ipos)=lab
      word=dpword
#endif 
      return
      end
