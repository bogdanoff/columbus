!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      subroutine gmtxm(a,natr, b,nbr, c,ncc, alpha)
c
c  to compute c = c + alpha*( a(transpose) * b )
c
c  input:
c  a(*),b(*) = input matrices.
c  natr = number of rows in a(transopose)(*) and c(*).
c  nbr = number of columns of a(transpose)(*) and rows of b(*).
c  ncc = number of columns of b(*) and c(*).
c        all dimensions must be > 0.
c  alpha = matrix product scale factor.
c
c  output:
c  c(*) = c(*) + alpha*( a(t) * b ) updated  matrix.
c
c  10-nov-89 written by ron shepard.
c
      implicit none
      integer natr, nbr, ncc
      real*8 a(nbr,natr), b(nbr,ncc), c(natr,ncc), alpha
c
      integer i, j, k
      real*8 sum, bkj
      real*8    zero,     one
      parameter(zero=0d0, one=1d0)
c
      real*8   ddot
      external ddot
c
#if defined(FUJITSU)
c     # 14-may-92 fujitsu code of Ross Nobes and Roger Edberg
c     #           merged. -rls
      integer    maxvl
      parameter( maxvl = 512 )
      integer imax, istart, kextra
      real*8  b0, b1, b2, b3
      kextra = mod( nbr, 4 )
c
         do 50 istart = 1, natr, maxvl
            imax = min( istart+maxvl-1, natr )
            do 20 k = 1, kextra
               do 20 j = 1, ncc
                  b0 = b(k,j) * alpha
*vocl loop,repeat(maxvl)
                  do 20 i = istart, imax
                     c(i,j) = c(i,j) + b0 * a(k,i)
20          continue
           do 30 k = kextra+1, nbr, 4
              do 30 j = 1, ncc
                 b0 = b(k,j)   * alpha
                 b1 = b(k+1,j) * alpha
                 b2 = b(k+2,j) * alpha
                 b3 = b(k+3,j) * alpha
*vocl loop,repeat(maxvl)
                 do 30 i = istart, imax
                    c(i,j) = c(i,j) + b0 * a(k,i)
     &                              + b1 * a(k+1,i)
     &                              + b2 * a(k+2,i)
     &                              + b3 * a(k+3,i)
30         continue
50       continue
c
#elif defined(BLAS3) || defined(CRAY) || (defined(IBM) && defined(VECTOR)) || defined(ESSL)
c     # interface to the library routine.
      call dgemm('t','n',natr,ncc,nbr,
     & alpha, a,nbr, b,nbr, one, c,natr)
#else 
c
c  all fortran version.
c
c  note: *** this code should not be modified ***
c  use dot-product loop ordering for sequential memory accesses.
c  sparsness is not exploited.  none of the saxpy inner-loop orderings
c  result in sequential memory access.  if saxpy loops are essential
c  for some machine, then the matrices should be partitioned, and
c  intermediate results accumulated into a sequentially accessed
c  intermediate array and periodically dumped to c(*). -rls
c
      do 30 j = 1, ncc
         do 20 i = 1, natr
            sum = zero
            do 10 k = 1, nbr
               sum = sum + a(k,i) * b(k,j)
10          continue
            c(i,j) = c(i,j) + alpha * sum
20       continue
30    continue
#endif 
      return
      end
