!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      subroutine  icopy(n,ivx,incx,ivy,incy)
c
c  blas-style integer vector copy.
c
      implicit integer(a-z)
      integer ivx(*),ivy(*)
      integer i,incx,incy,ix,iy,n
c
      if(n.le.0)return
      if(incx.eq.1.and.incy.eq.1)then
c
c        code for both increments equal to 1
c
         do 30 i = 1,n
            ivy(i) = ivx(i)
30       continue
c
      else
c
c        code for unequal increments or equal increments
c          not equal to 1
c
         ix = 1
         iy = 1
         if(incx.lt.0)ix = (-n+1)*incx + 1
         if(incy.lt.0)iy = (-n+1)*incy + 1
         do 10 i = 1, n
            ivy(iy) = ivx(ix)
            ix = ix + incx
            iy = iy + incy
10       continue
c
      endif
c
      return
      end
