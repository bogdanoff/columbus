!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      subroutine ortmgs( nur, nuc, u, isflg, s, su, tfact, ierr )
c
c  use the modified schmidt procedure to produce vectors that
c  are orthonormal with respect to a metric matrix.
c
c  input:
c  nur  =  dimension of s(*) and number of rows in u(*).
c  nuc  =  number of vectors (columns) in u(*).
c  u(*) =  initial input vectors.
c  isflg = metric matrix flag.
c        = 0 don't use s(*) or su(*). assumed metric is a unit matrix.
c        = 1 use s(*) and su(*) to determine vector overlaps.
c  s(*) =  basis overlap matrix (metric) stored lower-triangular packed.
c
c  output:
c  s(*) =  unmodified metric matrix.
c  u(*) =  modified vectors such that u(transpose) * s * u = 1 (the
c          unit matrix of dimension nuc).  the transformation matrix t,
c          where u(new)=u(old)*t, is upper triangular and is not stored
c          explicitly.
c  tfact = measure of the nonorthonormality of the input vectors.
c          sqrt( sum(i) norm(t(i)-1)**2 ) where t(i) is an elementary
c          transformation matrix and t = t(1)*t(2)*...*t(i)*...
c  ierr  = 0 for normal return.
c         -1 for error in input dimensions.
c         -2 for isflg error.
c          k if the k-th vector could not be normalized.
c
c  31-oct-90 isflg added, orthos()/ortho() combined. -rls
c  12-jun-87 tfact computation added. -rls
c  01-mar-83 inline s*u replaced with spmxv() call. -rls
c  04-jul-78 written by ron shepard.
c
      implicit integer(a-z)
c
      integer nur, nuc, isflg, ierr
      real*8 u(nur,nuc), s(*), su(nur), tfact
c
      integer j, k
      real*8 term, tnorm
      real*8     zero,    one
      parameter (zero=0d0,one=1d0)
c
      real*8   ddot, dnrm2
      external ddot, dnrm2
c
      ierr=0
      tfact=zero
c
c     # check input:
c
      if ( nuc .le. 0    .or.    nuc .gt. nur ) then
         ierr=-1
         return
      endif
c
c     # sucessively normalize each vector, then subtract that normalized
c     # component from each of the remaining updated vectors.
c     #
c     # note that the classical grahm-schmidt method subtracts the
c     # components of the updated vector with each original vector.
c     # this is am important distinction with finite precision. -rls
c
      if ( isflg .eq. 1 ) then
c
c        # use s(*) such that on return, transpose(u) * s * u = 1
c
         do 140 k = 1, nuc
            call wzero( nur, su, 1 )
c
c           # form the k'th column of s*u.
c
            call spmxv( s, u(1,k), su, nur )
c
            term = ddot( nur,   u(1,k), 1,   su, 1 )
c
            if ( term .le. zero ) then
               ierr=k
               return
            endif
c
            tnorm = one / sqrt(term)
c
c           # scale the k'th vector to unit norm.
c
            call dscal( nur, tnorm, u(1,k), 1 )
c
            tfact = tfact + (tnorm-one)**2
c
c           # calculate overlap of this k'th vector with each successive
c           # j'th vector and subtract this component of the k'th vector
c           # from the j'th vector.  the dot products are scaled by
c           # tnorm since u(*,k) was scaled but not su(*).
c
            do 120 j = (k+1), nuc
               term = -tnorm * ddot( nur, u(1,j), 1, su, 1 )
               if ( term .ne. zero )
     &          call daxpy( nur, term, u(1,k), 1, u(1,j), 1 )
               tfact = tfact + term**2
120         continue
140      continue
c
      elseif ( isflg .eq. 0 ) then
c
c        # assume s(*,*) = 1. upon return transpose(u) * u = 1
c        # s(*) and su(*) are not referenced.
c
         do 240 k = 1, nuc
c
c           # normalize u(*,k).
c
            term = dnrm2( nur, u(1,k), 1 )
c
            if ( term .le. zero ) then
               ierr=k
               return
            endif
c
            tnorm = one / term
c
c           # scale the k'th vector to unit norm.
c
            call dscal( nur, tnorm, u(1,k), 1 )
c
            tfact = tfact + (tnorm-one)**2
c
c           # calculate overlap of this k'th vector with each successive
c           # j'th vector and subtract this component of the k'th vector
c           # from the j'th vector.
c
            do 220 j = (k+1), nuc
               term = -ddot( nur, u(1,j), 1, u(1,k), 1 )
               if ( term .ne. zero )
     &          call daxpy( nur, term, u(1,k), 1, u(1,j), 1 )
               tfact = tfact + term**2
220         continue
240      continue
c
      else
         ierr = -2
         return
      endif
c
      tfact = sqrt( tfact )
c
      return
      end
