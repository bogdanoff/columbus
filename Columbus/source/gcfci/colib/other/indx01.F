!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      subroutine indx01( nlist, numi, len01, vec01, indxv, icode )
c
c  transform between the index representation of a vector and the
c  0/1 representation.
c  indxv(*) = 1,2,  4,    7
c  vec01(*) = 1,1,0,1,0,0,1
c
c  arguments:
c  numi = number of index-representation entries.
c  len01 = length of the 0/1 representation vector.
c  vec01(1:len01) = 0/1 representation.
c  indxv(1:numi) = index vector representation.
c  icode = 0  convert from vec01(*) to indxv(*),
c        = 1  convert from indxv(*) to vec01(*).
c
c  05-jun-89 written by ron shepard.
c
      implicit none
c
      integer nlist, len01, numi, icode
      integer vec01(len01),indxv(numi)
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer    toindx,   to01
      parameter( toindx=0, to01=1 )
c
      integer i, icnt
c
      if ( icode .eq. toindx ) then
c        # vec01(*) to indxv(*).
         icnt = 0
         do 10 i = 1, len01
            if ( vec01(i) .eq. 1 ) then
               icnt = icnt + 1
               indxv(icnt) = i
            endif
10       continue
         if ( icnt .ne. numi )
     &    call bummer('indx01: (numi-icnt)=',(numi-icnt),faterr)
         write(nlist,'(/1x,a,i6,a)')'indx01:',
     &    numi,' indices saved in indxv(*)'
      else
c        # indxv(*) to vec01(*).
         do 20 i = 1, len01
            vec01(i) = 0
20       continue
         do 30 icnt = 1, numi
            vec01(indxv(icnt)) = 1
30       continue
      endif
      return
      end
