!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      subroutine mulpop_map(
     & bfnl,   imtype,   map,     mnl,
     & ms,     nmap,     nd_at,   mtype,
     & nbft,   nsym,     nlist)

c  extract mnl(*) and ms(*) from map(*,*).
c  ##  OUTPUT:
c       # nd_at = number of symmetry-distinct centers
c       # mtype(i) = character center-label for each distinct center
c       # nd_at = number of symmtery distinct itypes of atoms
c
c  10-dec-01 modified and included into colib by m.dallos
c  25-may-91 written by russ pitzer.
c
      implicit none
c
c  ## parameter & common block section
c
      integer mnltyp,mstyp
      parameter (mnltyp = 4,mstyp = 3)
      logical debugging
#if defined(DEBUG)
      parameter ( debugging = .true. )
#else
      parameter ( debugging = .false. )
#endif
c
c  ##  arguments
c
      character*8 bfnl(*)
      integer imtype(*)
      integer nmap, nd_at, nsym, nbft, nlist
      integer map(nbft,*),mnl(nbft),ms(nbft)
      character*3 mtype(*)
c
c  ##  local
c
      integer i,imnl,ims, isym, iat
      character*8 tmpchr
c
c  ## external
c 
      logical streq
      external streq
c
c------------------------------------------------------------------
c
      imnl = 0
      ims = 0
      do i = 1,nmap
        if (imtype(i).eq.mnltyp) then
          imnl = i
        else if (imtype(i).eq.mstyp) then
          ims = i
        endif
      enddo

c     # extract the bfntyp(*) array from map(*,*).
      if (imnl.eq.0) then
c       # default: set everything to an s-type function.
        call iset (nbft,1,mnl,1)
      else
        call icopy (nbft,map(1,imnl),1,mnl,1)
      endif

c     # extract the bfn-to-center array from map(*,*).
      if (ims.eq.0) then
c       # default: set everything to one center.
        call iset (nbft,1,ms,1)
      else
        call icopy (nbft,map(1,ims),1,ms,1)
      endif

c     # set nd_at = number of symmetry-distinct centers
c     # set mtype(i) = character center-label for each distinct center
      nd_at = 0
      do isym = 1,nbft
        nd_at = max( nd_at, ms(isym) )
        mtype( ms(isym) ) = bfnl(isym)(4:6)//'_'
      enddo

c     # write out mtype and the map vectors ms(*) and mnl(*):
      if (debugging) then

        write(nlist,"('mulpop_map:')")

        write(nlist,*)'mtype:',(mtype(isym),isym=1,nd_at)

        write(nlist,"('input orbital labels, i:bfnlab(i)=')")
        write(nlist,"(6(i4,':',a8))") (i,bfnl(i),i=1,nbft)

        write(nlist,"('bfn_to_center map(*), i:map(i)')")
        write(nlist,"(10(i4,':',i3))") (i,ms(i),i=1,nbft)

        write(nlist,"('bfn_to_orbital_type map(*), i:map(i)')")
        write(nlist,"(10(i4,':',i3))") (i,mnl(i),i=1,nbft)
        write(nlist,*)' Number of symmetry-distinct centers:',nd_at

      endif

      return
      end
