!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      subroutine putvec(
     & unit,   lenrec, logrec, loglen,
     & vector, start,  length, icode  )
c
c     putvec : subroutine to place word addressable information
c              residing on direct access file
c
c     written by: eric stahlberg (in consultation with ron shepard)
c     date:       december 1990
c     version:    1.2
c
c
      implicit integer(a-z)
c
      integer unit,logrec,loglen,start,length,icode,lenrec
      real*8 vector(*)
c
c     unit  : unit number
c     logrec: logical record number
c     loglen: logical record length
c     vector: data
c     start : start of data in logical record
c     length: length of record in logical data
c     icode : initialization code (defined below)
c             icode=0 : no initialization
c             icode=1 : initialize record with breakpoints
c                       listed in common esio90
c             icode=2 : fully initialize the entire record
c
c     local variables
c
      integer ncodev,irecd,ibrk,ntime,rcode(4),precd
      real*8 data(8*4096)
c
c     # common information
c
c     # positions of breaks when writing data in offset form
c     #  - used for icode = 2 only
c
      integer breaks,nbreak,maxbrk
      parameter (maxbrk=100)
      common /esio90/ nbreak, breaks(maxbrk)
      save   /esio90/
c
c     # bummer error types
      integer    wrnerr,   nfterr,   faterr
      parameter (wrnerr=0, nfterr=1, faterr=2)
c
c     accounting information
c
      integer    maxvec
      parameter (maxvec=500)
      integer   vtotal,    codev(maxvec),   times(maxvec)
      save      vtotal,    codev,           times
      data      vtotal/0/, codev/maxvec*0/, times/maxvec*0/
c
c
#if defined(DEBUG)
      write(*,*) 'putvec:unit,lenrec,logrec,loglen,start,'
     .  ,'length,icode ', unit,lenrec,logrec,loglen,start,length,
     . icode
#endif 
      if ((unit.le.0).or.(unit.ge.100)) then
         call bummer('putvec: bad unit number', unit, faterr )
      endif
c
c     # search initialized vector list for current vector
c
      ncodev = logrec*100 + unit
      precd = 0
      do 10 irecd=1,vtotal
         if (codev(irecd).eq.ncodev) then
            precd = irecd
            goto 15
         endif
10    continue
c
      if (precd.eq.0) then
         vtotal = vtotal + 1
         precd  = vtotal
         codev(precd) = ncodev
         times(precd) = 0
      endif
      if (vtotal.gt.maxvec) then
         call bummer('putvec: max number vectors exceeded',
     &    maxvec, faterr )
      endif
15    continue
      times(precd) = times(precd) + 1
      ntime = times(precd)
c
c     write(0,*) 'putvec: icode,ntime=',icode,ntime
      if ( ntime .eq. 1 ) then
c        # first time for this vector.  initialize if necessary.
         if ( icode .eq. 0 ) then
c           # best case: no initialiation requested
         elseif ( icode .eq. 1 ) then
c           # initialize break records only
            do 100 ibrk = 1, nbreak
               call writvc(
     &          unit,   lenrec, data,   breaks(ibrk),
     &          1,      logrec, loglen, 0,
     &          rcode  )
100         continue
         elseif ( icode .eq. 2 ) then
            call wzero(8*4096,data,1)
c           # initialize every record
            do 200 ibrk = 1, loglen, lenrec
               call writvc(
     &          unit,   lenrec, data,   ibrk,
     &          min(lenrec,8*4096,loglen+1-ibrk),logrec, loglen, 0,
     &          rcode  )
200         continue
         else
            call bummer('putvec: invalid icode ', icode, faterr )
         endif
      endif
c
c     write(0,*) 'putvec: call writvc with 3'
      call writvc(
     & unit,   lenrec, vector, start,
     & length, logrec, loglen, 3,
     & rcode  )
c
      return
      end
