!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      subroutine plblkc( title, z, nr, labr, ifmt, nlist )

c  print a lower-triangular packed matrix with row labels.
c  this version prints eight columns at a time with three formats.
c  parameter ncol and parameter formats should be modified to print
c  a different number of columns or to use different formats.
c
c  input:
c  title   = optional title.  only printed if (title.ne.' ').
c  z(*)    = matrix to be printed.
c  nr      = row and column dimension.
c  labr(*) = character*8 row and column labels.
c  ifmt    = format type (1:f, 2:e, 3:g).
c  nlist   = output unit nubmer.
c
c  13-sep-05 parameter format version. -rls
c  18-oct-90 plblk() modified. title,labr(*) change. -rls
c  format statement assignment version 5-jun-87 (rls).
c  ron shepard 17-may-84.

      implicit none

      integer    ncol
      parameter (ncol=8)

      character(*), parameter :: cfmtt = '(/10x,a)'
      character(*), parameter :: cfmth = '(/8x,8(6x,a8,1x))'

      character(*), parameter :: cfmt(3) = (/
     & '(1x,a8,8f15.8)   ',
     & '(1x,a8,1p,8e15.6)',
     & '(1x,a8,1p,8g15.6)' /)

      integer nr, ifmt, nlist
      character*(*) title, labr(*)
      real*8 z(*)

      integer fmtz, jlast, jstrt, j, ij0, i, j2, jt
      real*8    zero
      parameter(zero=0d0)

      fmtz = min(max(1,ifmt),3) ! clip

      if ( title .ne. ' ' ) write(nlist,cfmtt) title

      jlast = 0
      do jstrt = 1, nr, ncol
         jlast = min( nr, jlast+ncol )

         write(nlist,cfmth) ( labr(j), j = jstrt, jlast )

         ij0 = (jstrt*(jstrt-1))/2
         do i = jstrt, nr
            j2 = min( i, jlast )

            ! print the row if a nonzero element is found.

            do j = jstrt, j2
               if ( z(ij0+j) .ne. zero ) then
                  write(nlist,cfmt(fmtz)) 
     &             labr(i), ( z(ij0+jt), jt=jstrt,j2)
                  exit
               endif
            enddo
            ij0 = ij0 + i
         enddo
      enddo

      return
      end
