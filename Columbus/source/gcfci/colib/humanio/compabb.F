!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      FUNCTION COMPABB(ANSWER,REFRNC,MINLEN)
C  test if ANSWER equals REFRNC to as many characters as are in ANSWER
C  ANSWER must be at lest MINLEN characters long
      LOGICAL COMPABB
      CHARACTER*(*) ANSWER,REFRNC
c     intrinsic len_trim
c     integer   len_trim
      external fstrlen
      integer  fstrlen
c Temporarily use our fstrlen instead of the F90 len_trim intrinsic
c because the f90 compiler cannot be used on SunOS platforms due to
c the declaration of pointer mem1 as an integer.
c See the bug reports; SRB March 26, 2005.

c     I = len_trim(ANSWER)
      I = fstrlen(ANSWER)
      IF (I.GT.0) THEN
        COMPABB=(I.GE.MINLEN).AND.(ANSWER(1:I).EQ.REFRNC(1:I))
      ELSE
        COMPABB=(I.GE.MINLEN)
      ENDIF
      RETURN
      END
