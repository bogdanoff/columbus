!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      subroutine sifskh( aoints, ierr )
c
c  skip over the header records and position the file at the
c  beginning of the 1-e integral records.
c
c  output: ierr = error return code.  0 for normal return.
c
c  20-sep-90 written by ron shepard.
c
      implicit none
c
      integer aoints, ierr
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      rewind aoints
      read( aoints, iostat=ierr )
      if ( ierr .ne. 0 ) return
c
      read( aoints, iostat=ierr )
      return
c
      end
