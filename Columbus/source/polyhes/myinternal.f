!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      PROGRAM INTERNAL
C
      IMPLICIT REAL*8 (A-H,O-Z)
      integer ncor
      parameter (ncor=1500000)
      real*8 RS8(ncor)
      integer IRS8(ncor)
      REAL*4 XT0,XU0,XS0,XT1,XU1,XS1,XT,XU,XS
      CHARACTER*(24) FDATE
      DATA IFAIL/0/
C
      CALL VMAIN(RS8,IRS8,NCOR)

      call exit2(0)
      END
      SUBROUTINE VMAIN(A,IA,NCOR)
      IMPLICIT INTEGER (A-Z)
      DIMENSION A(NCOR),IA(NCOR)
C
      open(74,file='geom',form='formatted')
      REWIND 74
      CALL NUMATM(74,0,NATOMS)
      IF(NATOMS.GT.30)STOP ' NATOMS > 30 '
C
      MFIXDIR=30
      MATOMS=NATOMS
      M3=NATOMS*3
      N3=MATOMS*3
      NDIM=N3+MFIXDIR+5
      ND1=NDIM*(NDIM+2)
      N31=max((N3+1)*N3,nd1)
      N3S=max(N3*N3,n31)
      NPREC=INTOWP(1)
      WRITE(6,1001)MATOMS,N3,N31,N3S,NPREC
      NACME=  1
      MACME=  nacme+m3*NPREC
      LACME=  MACME+m3*NPREC
      SCR=    LACME+m3*NPREC*(mfixdir+5)
      HESSS=  SCR+ND1*NPREC
      EGRADH= HESSS+ND1/2*NPREC
      GGRADH= EGRADH+N31*2*NPREC
      XGRADH= GGRADH+N31*2*NPREC
      HESS=   XGRADH+N31*2*NPREC
      GRAD=   HESS+ND1*NPREC
      EVAL=   GRAD+NDIM*NPREC
      ROOT=   EVAL+N3*NPREC
      VECT=   ROOT+N3*NPREC
      RVECT=  VECT+N3S*NPREC
      BVECT=  RVECT+N3*3*NPREC
      SCR1=   BVECT+N3S*NPREC
      SCR2=   SCR1+ND1*NPREC
      TVECT=  SCR2+ND1*NPREC
      VVECT=  TVECT+N31*NPREC
      LAMBDA= VVECT+N31*NPREC
      KAMBDA= LAMBDA+MFIXDIR*2*NPREC
      ICHAR=  KAMBDA+MFIXDIR
      ISYMTR= ICHAR + N3*11*NPREC
      ITOP=   ISYMTR+ N3S*NPREC
C
      WRITE(6,1000)NACME,SCR,HESSS,EGRADH,GGRADH,XGRADH,HESS,GRAD,
     1             EVAL,ROOT,VECT,MACME,LACME,RVECT,BVECT,SCR1,SCR2,
     2             TVECT,VVECT,LAMBDA,KAMBDA,ICHAR,ISYMTR,ITOP
      IF(ITOP.GT.NCOR)STOP 'INSUFFICIENT MEMORY IN POLYHES'
      CALL POLYHSD(IA(NACME),IA(NACME),IA(NACME)
     1      ,IA(SCR),IA(HESSS),IA(EGRADH),IA(GGRADH),IA(XGRADH)
     2      ,IA(HESS),IA(GRAD),IA(EVAL),IA(ROOT),IA(VECT)
     3      ,IA(MACME),IA(MACME),IA(LACME),IA(LACME),IA(RVECT)
     4      ,IA(BVECT),IA(SCR1),IA(SCR2),IA(TVECT),IA(VVECT)
     5      ,IA(LAMBDA),IA(KAMBDA),IA(ICHAR),IA(ISYMTR)
     6      ,N3,M3,MATOMS,MFIXDIR)
      RETURN
 1000 FORMAT(/2X,'NACME SCR HESSS EGRADH GGRADH XGRADH HESS GRAD',/8I6,
     1       /2X,'EVAL ROOT VECT MACME LACME RVECT BVECT   SCR1',/8I6,
     2       /2X,'SCR2 TVECT VVECT LAMBDA KAMBDA ICHAR ISYMTR ITOP',
     3       /8I6)
 1001 FORMAT(/2X,'MEMORY LOCATION PARAMETERS:MATOMS=',I3,' N3=',I5,
     1      ' N31=',I5,' N3S=',I5,' NPREC=',I1)
      END
      SUBROUTINE POLYHSD(NACME,RNACME,drygrad
     1      ,SCR,HESSS,EGRADH,GGRADH,XGRADH
     2      ,HESS,GRAD,EVAL,ROOT,VECT
     3      ,MACME,RMACME,LACME,RLACME,RVECT
     4      ,BVECT,SCR1,SCR2,TVECT,VVECT,LAMBDA,KLASS
     5      ,CHAR,SYMTRN
     6      ,N3,M3,MATOMS,MFIXDIR)
      IMPLICIT REAL*8(A-H,O-Z)
      CHARACTER*6 VERB
      CHARACTER*19 KMES(3)
      character*72 filen
      LOGICAL SAME
      character*3 name(100)
      real*8 mass(100),charge(100)
      INTEGER SIMULT,ACROSS,GMFILE,RESTART,ACCEL,SADDLE,
     1        ATOM(30),STATE1,STATE2,KILLER(90),CDIF,ORIGIN
     2       ,FIXDIR(6,30),KLASS(MFIXDIR),ACTUAL
     3      ,idrtv(3),statev(3),ipairs(3,5),state3
C       EQUIVALENT (NACME,RNACME)  (MACME,RMACME)  (LACME,RLACME)
      REAL*8 GM(3,30),DISP(90),DAMPV(90),xgradr(100)
     1      ,SCR(2),HESSS(2),EGRADH(N3,2),GGRADH(N3,2),dipolr(50)
     2      ,HESS(2),GRAD(N3),EVAL(N3),ROOT(N3),VECT(2)
     3      ,NORM,MACME(3,MATOMS),RMACME(N3),LAMBDA(MFIXDIR,2)
     4      ,RVECT(2),BVECT(2),SCR1(2),SCR2(2),VALDIR(30)
     5      ,TVECT(2),VVECT(2),NACME(3,MATOMS),RNACME(N3)
     6      ,LACME(3,MATOMS),RLACME(N3),XGRADH(N3,2)
     7      ,SYMTRN(M3,M3),CHAR(M3,11) ,drygrad(m3,3)
      equivalence(idrtv(1),idrt1),(idrtv(2),idrt2),(idrtv(3),idrt3)
      equivalence(statev(1),state1),(statev(2),state2),
     x           (statev(3),state3)
      DIMENSION GRAD1(3),GRAD2(3),GRAD3(3), GRD(3,3),ipflag(5)
      EQUIVALENCE(GRD(1,1),GRAD1),(GRD(1,2),GRAD2),(GRD(1,3),GRAD3)
      COMMON/VIBORD/IVBORD(90),IVBORD2(90)
      COMMON/DIATAT/NDIAT(30)
      COMMON/CHESS/NORR
      common/pscent/maxnew,psc(30,10),dpsc(30,10),ipsc(30,10)
C         NEWTON-RAPHSON CONVERGENCE CRITERIA
      DATA CONV/1.0D-06/,MAXIT/20/,NSFIG/5/,ipflag/5*0/
      data ipairs/2,2,2, 1,2,3, 1,3,3, 2,3,3, 3,3,2/
C
      DATA FIXDIR/180*0/,VALDIR/30*0.D0/
      DATA NCALC/1/,NACOUT/2/,SIMULT/0/,INTC/1/,IORG/2/
      DATA IPFLG/-1/,IDBG/-1/,LINEQ/0/
      DATA GMFILE/1/,RESTART/0/,ACCEL/0/,DAMP/0.0D+0/
      DATA IDISP/0/,ZEIG/5.D-6/,SCALE/1.D0/
     1     ,EIGROT/1.0D-3/,TOLROT/1.0D0/
      DATA N32/32/,IHESS/0/,SADDLE/0/,ZERO/0.D0/,MHESS/1/
      DATA ICIU1,ICIU2/31,41/
      DATA KZ/3/,CDIF/0/,IHESEQ1/0/,KSCALE/0/
      DATA KMES/' ENTIRE ','ENTIRE - GEOMETRIC', ' ENERGY PART OF '/
      DATA GRD/9*1/, NPROP/4/
C
C      FIXDIR       FIXDIR(1,*)=I FIXDIR(2,*)=J - R(I,J) FIXED AT VALDIR
C                   FIXDIR(1,*)=I FIXDIR(2,*)=J  FIXDIR(3,*)=K -
C                   ANGLE(I,J,K ) FIXED AT VALDIR(I)
C                   FIXDIR(I,J,K,0,0,L) ANGLE(I,J,K,L)
C                   TO INTRODUCE THIS OPTION WITH THE HESSIAN USER MUST
C                   REGENERATE WORKING HESSIAN USING RESTART
C
C                      FILE USAGE
C      I70       PROPERTY UNIT                  70
C      I73       DEPENDENT INPUT UNIT           73
C      I74       CURRENT GEOMETRY FILE          74
C      I75       NEXT GEOMETRY FILE             75
C      I76       SYMMETRY EQUIVALENT CENTER     76
C      I77       SEC NEXT GEOMETRY              77
C      I99       TEMPORARY UNIT                 99
C      N32       PROGRESS FILE                  32
C
C      ICIU1     ALCHEMY CI HEADER FILE         31
C      ICIU2     ALCHEMY CI HEADER FILE         41
C      ITAP12    GUGA CI FILE                   12
C      ITAP13    GUGA CI FILE                   13
C      ITAP10    SAXE TAPE 10    E(A)           10
C      ITAP11    SAXE TAPE 11    G(A)           11
C      ITAP14    SAXE TAPE 14    H(A)           14
C
C         UNITS
      ITAP10=10
      ITAP11=11
      ITAP14=14
      INP=5
      IOUT=6
      I70=70
      I73=73
      I74=74
      I75=75
      I76=76
      I77=77
      I99=99
C
      motion=3
      open(n32,file='progress',form='formatted')
      PI=ACOS(-1.D0)
      NORR=1
C
      WRITE(IOUT,1001)
      REWIND I74
      CALL NUMATM(I74,0,NATOMS)
      ij=0
      DO 2 I=1,NATOMS
      atom(i)=i
      do 2 j=1,3
      ij=ij+1
      DAMPV(Ij)=ZERO
    2 IVBORD(IJ)=IJ
C
C
      mstate=2
      open(i73,file='transmomin',form='formatted')
      REWIND I73
      READ(I73,*,End=920)
      READ(I73,*,End=920)(idrtv(k),statev(k),k=1,2)
      READ(I73,*,end=923)idrtv(3),statev(3),idrtv(3),statev(3)
      mstate=3
 923  continue
      close(i73)
c
      same= idrt1.eq.idrt2
      NCGNT=1
      if(same .and. (state1.eq.state2))then
      mstate=1
      write(iout,1074)statev(1)
C
      else
      if(same)then
      ncgnt=(mstate+2)*(mstate-1)/2
      WRITE(IOUT,1044)mstate,'    ',(statev(k),k=1,mstate)
      endif
      if(.not.same)then
      WRITE(IOUT,1044)mstate,'non',(statev(k),k=1,mstate)
      ncgnt=ncgnt-1
      endif
C
      endif
      ncgnt1=ncgnt+1
      ndo=1
      if(ncgnt.le.1)ndo=0
      if(ncgnt.gt.2)ndo=3
C
C
      WRITE(IOUT,1041)MOTION
      NTDEGF=NATOMS*3
      NDEGF=NATOMS*3-3
      NDEGFI=NDEGF-3
      NROT=  NDEGF-NDEGFI

      NINTI=    NDEGFI+NFIXDIR+NCGNT
      NINT=     NINTI+NROT
      NDEGFL=   NDEGF+NCGNT
      NDEGFIL=  NDEGFI+NCGNT
C
      NINT2=    NINT*(NINT+1)/2
      NINTI2=   NINTI*(NINTI+1)/2
      NDEGFL2=  NDEGFL*(NDEGFL+1)/2
      NDEGFIL2= NDEGFIL*(NDEGFIL+1)/2
      NDEGFI2=  NDEGFI*(NDEGFI+1)/2
      NDEGF2=   NDEGF*(NDEGF+1)/2
      WRITE(IOUT,1043)NDEGF,NROT,NFIXDIR,NINTI,NINT
c
c
      DO 7 I=1,NATOMS
    7 NDIAT(I)=ATOM(I)
C
      rewind i74
      call readcolg(i74,gm,NATOMS,iout,name,mass,charge)
      WRITE(IOUT,1012)(I,MASS(I),I=1,NATOMS)
      WRITE(IOUT,1021)(J,(GM(I,J),I=1,3),J=1,NATOMS)
      CALL DIST(NATOMS,GM,IOUT)
      CALL FANGL(NATOMS,GM,IOUT)
C
      WRITE(IOUT,1031)(NDIAT(I),I=1,NATOMS)
      CALL IIDOF(NATOMS,MOTION,NDIAT,KILLER,KZ,IOUT,ORIGIN)
C         BUILD THE ROTATION VECTOR
      CALL MROTV(NATOMS,ORIGIN,RVECT,GM,KZ,KILLER)
C
C          ENERGY  GRADIENTS
C
      call filenamer(4,idrt1,state1,idrt1,state1,filen,iout)
      open(itap10,file=filen)
      read(itap10,*)(rnacme(k),k=1,natoms*3)
      close(itap10)
      CALL IDOF(NATOMS,rnacme,SCR,KILLER)
      CALL CHKR(NDEGF,rnacme,RVECT,IOUT,NROT,TOLROT,1)
      if(ncgnt.gt.0)then
      do 41 mcon=1,ncgnt
      ist1=statev(ipairs(1,mcon))
      ist2=statev(ipairs(2,mcon))
      ihd=ipairs(3,mcon)
      idr1=idrtv(ipairs(1,mcon))
      idr2=idrtv(ipairs(2,mcon))
      call filenamer(3+ihd,idr1,ist1,idr2,ist2,filen,iout)
      open(itap10,file=filen)
      read(itap10,*)(drygrad(k,mcon+1),k=1,natoms*3)
      close(itap10)
      CALL IDOF(NATOMS,drygrad(1,mcon+1),SCR,KILLER)
      CALL CHKR(NDEGF,drygrad(1,mcon+1),RVECT,IOUT,NROT,TOLROT,1)
   41 continue
c        energies to  energy differences
      do 48 k=1,natoms*3
      if(ncgnt.gt.2)drygrad(k,6)=drygrad(k,6)-drygrad(k,2)
      drygrad(k,2)=drygrad(k,2)-rnacme(k)
   48 continue
      endif

C         MAKE AN ON INTERNAL BASIS
      LOOPON=0
  415 CONTINUE
      CALL MVIBV(NATOMS,NDEGFI,NDEGF,TVECT,VVECT,I76,KILLER)
C         O.N. INTERNAL MOTION
      CALL IBASIS(NROT,RVECT,VVECT,NDEGF,BVECT,SCR,IOUT,IPFLG,IFAIL)
      IF(IFAIL.EQ.0)GO TO 416
      LOOPON=LOOPON+1
      IF(LOOPON.GT.NROT)GO TO 910
      GO TO 415
  416 CONTINUE
C
C reorder vector
C      call neworder(rvect,ntdegf,ndegf)
C
C   find things to do
c   transform gradients
c      SUBROUTINE EBTC(A,B,C,NI,NK,NJ)
c      DIMENSION A(NI,NJ),B(NK,NI),C(NK,NJ)

      write(iout,*)'  transforming gradients '
      call filenamer(4,idrt1,state1,idrt1,state1,filen,iout)
      open(itap10,file=filen)
      read(itap10,*)(rnacme(k),k=1,3*natoms)
      close(itap10)
      CALL IDOF(NATOMS,NACME,SCR,KILLER)
      CALL EBTC(SCR,BVECT,nacme,ndegfi,ndegf,1)
      call filenamer(1,idrt1,state1,idrt1,state1,filen,iout)
      open(itap10,file=filen)
      write(itap10,1080)(scr(k),k=1,ndegfi)
      close(itap10)
C
      if(ncgnt.gt.0)then
      do 51 mcon=1,ncgnt
      ist1=statev(ipairs(1,mcon))
      ist2=statev(ipairs(2,mcon))
      ihd=ipairs(3,mcon)
      idr1=idrtv(ipairs(1,mcon))
      idr2=idrtv(ipairs(2,mcon))
      call filenamer(3+ihd,idr1,ist1,idr2,ist2,filen,iout)
      open(itap10,file=filen)
      read(itap10,*)(rnacme(k),k=1,natoms*3)
      close(itap10)
      CALL IDOF(NATOMS,NACME,SCR,KILLER)
      CALL EBTC(SCR,BVECT,nacme,ndegfi,ndegf,1)
      call filenamer(ihd,idr1,ist1,idr2,ist2,filen,iout)
      open(itap10,file=filen)
      write(itap10,1080)(scr(k),k=1,ndegfi)
      close(itap10)
   51 continue
      endif

C
c        process constraints
       nfixdir=0
       open(i75,file='cicgrad',form='unformatted')
       rewind i75
       read(i75,end=281)nfixdir,(klass(k),k=1,nfixdir)
       if(nfixdir.eq.0)go to 281
       write(iout,*)'  transforming  k '
       write(iout,1069)nfixdir,(klass(k),k=1,nfixdir)
       close(i75)
       open(i75,file='cartcgrd',form='formatted')
       do 280 i=1,nfixdir
       read(i75,*)(drygrad(k,i),k=1,3*natoms)
  280  continue
       close(I75)
       open(i75,file='intcgrd',form='formatted')
       do 282 i=1,nfixdir
       CALL IDOF(NATOMS,drygrad(1,i),SCR,KILLER)
       CALL EBTC(SCR,BVECT,drygrad(1,i),ndegfi,ndegf,1)
       write(i75,1080)(scr(k),k=1,ndegfi)
  282  continue
       close(I75)
  281  CONTINUE

c
c        process geometry
      open(itap10,file='displace')
      read(itap10,1080,end=300)(disp(i),i=1,ndegfi)
      CALL TBAK(ndegf,ndegfi,BVECT,DISP,SCR,0)
      write(iout,1037)(disp(i),i=1,ndegf)
      CALL INTMO(NATOMS,GM,DISP,KILLER)
      open (i75,file='geom.new')
      call writcolg(i75,gm,NATOMS,iout,name,mass,charge)
      close (i75)
      CALL DIST(NATOMS,GM,IOUT)
      CALL FANGL(NATOMS,GM,IOUT)
 300  continue
C
C
      RETURN
  900 CONTINUE
      WRITE(IOUT,1019)NCIU
      CALL EXIT(100)
  910 CONTINUE
      WRITE(IOUTU,1042)LOOPON
      CALL EXIT(100)
  920 WRITE(IOUTU,*)'BAD NAMELIST FILE=',I73
      CALL EXIT(100)
  921 WRITE(IOUTU,*)'BAD NAMELIST FILE=',INP
      CALL EXIT(100)
 1000 FORMAT(5X,'NO NAMELIST INPUT- DEFAULT MASSES USED ' )
 1001 FORMAT(/15X,'SEARCH ALGORITHM FOR MINIMUM ENERGY CROSSING ' )
 1002 FORMAT(/5X,' CURRENT GEOMETRY ',/(1X,I2,1X,3F15.8) )
 1003 FORMAT(/5X,'RAW ',A6,' GRAD',/7X,'X',14X,'Y',14X,'Z'/(1X,3F15.9))
 1010 FORMAT(' POINTERS TO GEOMETRY AND NACMES:',2I6 )
 1011 FORMAT('  HESSIAN CONSTRUCTION MODE: NEW DIRECTION=',I3)
 1012 FORMAT(' MASS COMBINATION:',5(' M(',I2,')=',F12.6),
     1       (/20X,5(' M(',I2,')=',F12.6) ))
 1015 FORMAT(5X,'DISPLACEMENT VECTOR GENERATED FROM H ! D> = -G> ' )
 1016 FORMAT('  CI-ENERGIES FROM UNIT ',I4,
     1       /5X,' E1=',E20.12,' E2=',E20.12,' E2-E1=',E20.12)
 1017 FORMAT(/5X,'DEL-E SCALED GRADIENTS',/7X,'X',14X,'Y',/(1X,2E16.9) )
 1018 FORMAT(/5X,'EIGENVALUES AND EIGENVECTORS OF HESSIAN' )
 1019 FORMAT(/5X,'CANT READ CI ENERGY FROM HEADER:UNIT=',I3)
 1020 FORMAT(/5X,'SCALE FACTOR FOR GRADIENT',E15.8,5X,' NORM=',E15.8,
     1       /5X,' UNSCALED',' GRADIENTS ',(/1X,5E15.8))
 1021 FORMAT(5X,'CURRENT GEOMETRY:',/(2X,I3,2X,3F12.6) )
 1022 FORMAT(5X,'**** CURRENT GEOMETRY TAKEN FROM UNIT:',I4 )
 1023 FORMAT(5X,'**** NO CURRENT GEOMETRY FILE ****' )
 1024 FORMAT(/5X,'**** NO GRADIENT POINTER ON TAPE=',I2,' ******' )
 1025 FORMAT(/5X,'ITERATION STATUS  ',A20)
 1026 FORMAT('  CI-ENERGIES FROM UNITS ',2I4,' E2-E1=',E20.12)
 1027 FORMAT(5X,'RESTART SEARCH FROM PROGRESS FILE=',I3)
 1029 FORMAT(5X,' NO INPUT FOUND ON INPUT UNIT=',I5)
 1030 FORMAT(5X,' NO INPUT AT ALL ON UNITS=',2I5,' ABORT' )
 1031 FORMAT(5X,'ATOM MAP=',10I3)
 1032 FORMAT(5X,'SOLUTION IN INTERNAL BASIS:',/,(2X,6F12.6))
 1033 FORMAT(5X,'LAGRANGE MULIPLIER:',A3,'=',30F12.6)
 1034 FORMAT(5X,'RESTART HESSIAN CONSTRUCT FROM PROGRESS FILE='
     1     ,I3,' IDISP=',I3)
 1035 FORMAT(5X,'STATE ENERGIES: ECI=',2E20.12,'  ENUC=',E20.12)
 1036 FORMAT(/,' ROOT=',I3,' EVAL=',E15.8,
     1       /,' EVECT=',6F12.6,/,(7X,6F12.6))
 1037 FORMAT(5X,'NUCLEAR DISPLACEMENTS:',/,(2X,6F12.6))
 1038 FORMAT(5X,'**** UNIT HESSIAN USED ****' )
 1039 FORMAT(5X,' DAMPING VECTOR:',/,(2X,6E12.5))
 1040 FORMAT(5X,' NORM OF GGRAD-OC=',E15.8,' IC=',E15.8)
 1041 FORMAT(5X,' COORDINATES IN ', I2,' DIMENSIONS SEARCHED')
 1042 FORMAT(5X,'SCHMO FAILURE IN IBASIS-MVIBV',I3)
 1043 FORMAT(5X,'DIMENSIONS:NDEGF=',I3,' NROT=',I3,' NFIX=',I3,' IVAR=',
     1       I3,' IVAR+ROT=',I3)
 1044 FORMAT(/15X,i2,' STATE ENERGY MINIMIZED ',a3,
     1      ' CONICAL INTERSECTION', /23X,' STATES:', 3I5)
 1045 FORMAT(5X,'CONSTRAINT CONDITION ',I2,' R(',I2,',',I2,
     X       ')=     ',F12.6)
 1046 FORMAT(5X,'CONSTRAINT CONDITION ',I2,' ANG(',I2,',',I2,
     X       ',',I2,')=',F12.6)
 1047 FORMAT(5X,'CONSTRAINT CONDITION ',I2,' R(',I2,',',I2,
     X       ')=         R(',I2,',',I2,')')
 1048 FORMAT(5X,'CONSTRAINT CONDITION ',I2,' ANG(',I2,',',I2,
     X       ',',I2,')=ANG(',I2,',',I2,',',I2,')')
 1049 FORMAT(3X,A8,1X,A8)
 1050 FORMAT(5X,'NORM OF RHS: ORIG BASIS ',E15.8,' INT BASIS ',E15.8)
 1051 FORMAT(5X,'GRADIENT: ORIGINAL BASIS',(/5X,5E15.8))
 1052 FORMAT(15X,'GRADIENT DECOMPOSITION')
 1053 FORMAT(3X,'I=',I2,' E=',F11.5,' G=',F11.5,' L1=',F11.5,
     X                  ' H=',F11.5,' L2=',F11.5)
 1054 FORMAT(/5X,'CURRENT GRADIENT FROM NEW LM - NORM=',
     X       E15.8,' OLD NORM=',E15.8,' CONV=',E15.8,(/5X,6E12.5))
 1055 FORMAT(/5X,'ITERATION STATUS  HALTED - NEW GRADIENT CRITERION' )
 1056 FORMAT(/15X,'RE-ANALYZE GRADIENT')
 1057 FORMAT(/15X,'SCALE RHS METHOD: ',A20,' GRADIENT')
 1058 FORMAT(/15X,'CANONICAL COORDINATES',/13X,'r',12X,'R',12X,'GMA',
     X       /3X,'g   ',3F12.6,/3X,'h   ',3F12.6,/3X,'seam',3F12.6)
 1059 FORMAT(/15X,'JACOBI ANALYSIS OF G-H PLANE ')
 1060 FORMAT(/15X,' ANALYSIS OF G-H PLANE ',/5X,
     X      ' NORMH=  ',F10.5, ' NORMG/2= ',F10.5,' ANG=   ',F9.3,
     X       /5X,' DEL=  ', F7.3,5X,' gxh/2=',E15.6, /5X,
     X      ' NORM-AV=', F10.5,' PERCENT-GH= ',F8.3,
     X      ' C-AVG-G=',F10.5,' C-AVG-H=',F10.5,/)
 1061  format(3x,'Linear equation matrix:',/(5x,4f10.5,3x,f12.7))
 1062  FORMAT(5X,'CONSTRAINT CONDITION ',I2,' ANG(',I2,',',I2,
     X       ',',I2,',',i2,' )= ',f12.6)
 1069 FORMAT(5X,' KLASS VECTOR:',/' NDIM=',i3,' KLASS=',(10i3))
 1074 FORMAT(/15X,'  ENERGY MINIMIZation STATE:', I5)
 1080 format(d15.8)
      END
      SUBROUTINE ANGL(GM,II,JJ,KK,ANG,CA,R13,R12,R23)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION GM(3,2)
      DATA ZERO/0.D0/,TWO/2.D0/,PI/3.141592654D0/
C         LAW OF COSINES
      R13=ZERO
      R12=ZERO
      R23=ZERO
      DO 10 K=1,3
      R12=R12+(GM(K,II)-GM(K,JJ))**2
      R23=R23+(GM(K,JJ)-GM(K,KK))**2
      R13=R13+(GM(K,II)-GM(K,KK))**2
   10 CONTINUE
      CA=-(R13-R12-R23)/(TWO*DSQRT(R12*R23))
      ANG=DACOS(CA)*180.D0/PI
      R12=DSQRT(R12)
      R23=DSQRT(R23)
      R13=DSQRT(R13)
      RETURN
      END
      SUBROUTINE CHKI(NDIM,S,RHS,LHS,WMAT,IOUT)
      IMPLICIT REAL*8(A-H,O-Z)
      real*8 LHS
      DIMENSION RHS(NDIM),LHS(NDIM),WMAT(NDIM,NDIM),S(2)
      IX=0
      DO 10 I=1,NDIM
      DO 10 J=1,I
      IX=IX+1
      WMAT(I,J)=S(IX)
      WMAT(J,I)=S(IX)
   10 CONTINUE
      DO 20 I=1,NDIM
      DP=DOT(WMAT(1,I),LHS,NDIM)
      IF(DABS(DP-RHS(I)).LT.1.0D-10)GO TO 20
      WRITE(IOUT,1000)I,DP,RHS(I)
   20 CONTINUE
      RETURN
 1000 FORMAT(5X,'CHKI FAILURE: ROW=',I2,' HX=',E15.8,' RHS=',E15.8)
      END
      SUBROUTINE CHKJ(NDIM,RHS,LHS,WMAT,IOUT)
      IMPLICIT REAL*8(A-H,O-Z)
      real*8 LHS
      DIMENSION RHS(NDIM),LHS(NDIM),WMAT(NDIM,NDIM),S(2)
      DO 20 I=1,NDIM
      dp=0.
      do 21 j=1,ndim
   21 DP=DP+WMAT(i,J)*LHS(J)
      IF(DABS(DP-RHS(I)).LT.1.0D-10)GO TO 20
      WRITE(IOUT,1000)I,DP,RHS(I)
   20 CONTINUE
      RETURN
 1000 FORMAT(5X,'CHKI FAILURE: ROW=',I2,' HX=',E15.8,' RHS=',E15.8)
      END
      SUBROUTINE BCOM(MASS,B)
      IMPLICIT REAL*8(A-H,O-Z)
      REAL*8 MASS(3),B(3,3),MD,MT
      EQUIVALENCE(N1,NDIAT(1)),(N2,NDIAT(2)),(N3,NDIAT(3))
      COMMON/DIATAT/NDIAT(30)
      MD=MASS(N2)+MASS(N3)
C         INVERSE TRANSFORMATION FROM ATOM CENTERED COORDINATES
C         TO INTERNAL COORDINATES RELATIVE TO
C         FRAME AT COM OF TRIATOM
      MT=MD+MASS(N1)
      B(N1,1)=MD/MT
      B(N2,1)=-MASS(N1)/MT
      B(N3,1)=-MASS(N1)/MT
      B(N1,2)=0.D0
      B(N2,2)=-MASS(N3)/MD
      B(N3,2)= MASS(N2)/MD
      B(N1,3)=1.D0
      B(N2,3)=1.D0
      B(N3,3)=1.D0
      RETURN
      END
      SUBROUTINE CHKONB(NDIM,WMAT,IOUT)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION WMAT(NDIM,NDIM)
      DATA ZERO,ONE/0.D0,1.D0/
      DO 10 I=1,NDIM
      TEST=ZERO
      DO 10 J=1,I
      IF(J.EQ.I)TEST=ONE
      DP=DOT(WMAT(1,I),WMAT(1,J),NDIM)
      IF(DABS(TEST-DP).LT.1.0D-08)GO TO 10
      WRITE(IOUT,1000)I,J,DP
   10 CONTINUE
      RETURN
 1000 FORMAT(5X,'CHKONB FAILURE: (',I2,' ! ',I2,')=',F15.8)
      END
      SUBROUTINE CHKR(N,A,V,IOUT,NV,TOL,JFAIL)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION A(2),V(N,NV)
C
      DP1=DSQRT(DOT(A,A,N))
      DO 100 I=1,NV
      IFAIL=I
      DP2=DSQRT(DOT(V(1,I),V(1,I),N))
      DP=DOT(A,V(1,I),N)
      WRITE(IOUT,1001)JFAIL,I,DP,DP1,DP2
      IF(DP.GT.TOL)GO TO 900
  100 CONTINUE
      RETURN
  900 CONTINUE
      WRITE(IOUT,1002)
      WRITE(IOUT,1000)JFAIL,IFAIL,TOL,DP,DP1,DP2
      WRITE(IOUT,1003)'ROT ',(V(K,IFAIL),K=1,N)
      WRITE(IOUT,1003)'GRAD',(A(K),K=1,N)
 1000 FORMAT(5X,'ROTATION VECTOR FAILURE:  TEST=',I1,
     1          ' DIREC=',I1,' TOLERANCE=',E15.8,/5X,
     2  ' DP=',E15.8,' NORMG=', E15.8,' NORMR=',E15.8)
 1001 FORMAT(5X,'TEST=',I2,' DIRECTION=',I2,' DP=',E15.8,' NORMG=',
     X       E15.8,' NORMR=',E15.8)
 1002 FORMAT(15X,'BAD ROTATION VECTOR OR GRADIENT:')
 1003 FORMAT(2X,A4,/,(5F12.6))
      return
      END
      SUBROUTINE CMPRS(NATOMS,HESS,II,IOUT)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION HESS(2)
      N3=NATOMS*3
      WRITE(IOUT,1000)'ORGINAL',N3
      CALL MATOUT(HESS,N3,N3,N3,N3,IOUT)
      IX=0
      IY=0
      DO 10 I=1,N3
      IF((I-1)/3+1.NE.II)GO TO 11
      IY=IY+N3
      GO TO 10
   11 CONTINUE
      DO 20 J=1,N3
      IY=IY+1
      IF((J-1)/3+1.EQ.II)GO TO 20
      IX=IX+1
      HESS(IX)=HESS(IY)
   20 CONTINUE
   10 CONTINUE
      NDO=N3-3
      WRITE(IOUT,1000)'NDEF',NDO
      CALL MATOUT(HESS,NDO,NDO,NDO,NDO,IOUT)
C
      IX=0
      DO 100 I=1,NDO
      DO 100 J=1,I
      IX=IX+1
      IJ=(I-1)*NDO+J
      HESS(IX)=HESS(IJ)
  100 CONTINUE
      RETURN
 1000 FORMAT(/5X,'SQUARE ', A8,' HESSIAN FROM CMPRS: DIMENSION =',I3)
      END
      subroutine  gDIH(C1,C2,C3,C4,ii,kk,grad)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION C1(3),C2(3),C3(3),C4(3)
     1          ,RGM(3,4),GM(3,4),RM1(3,3)
      DATA ZERO/0.D0/,ONE/1.D0/,PI/3.141592654D0/
      data h/0.001/
      do 10 i=1,3
      gm(i,1)=c1(i)
      gm(i,2)=c2(i)
      gm(i,3)=c3(i)
      gm(i,4)=c4(i)
 10   continue
      gm(kk,ii)=gm(kk,ii)+h
      call dihed1(gm(1,1),gm(1,2),gm(1,3),gm(1,4),dihp)
      gm(kk,ii)=gm(kk,ii)-h*2.d0
      call dihed1(gm(1,1),gm(1,2),gm(1,3),gm(1,4),dihm)
      grad=(dihp-dihm)/(2*h)
      return
      end

      SUBROUTINE DIHEDx(C1,C2,C3,C4,DIH)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION C1(3),C2(3),C3(3),C4(3)
     1          ,RGM(3,4),GM(3,4),RM1(3,3)
      DATA ZERO/0.D0/,ONE/1.D0/,PI/3.141592654D0/
      R=ZERO
      DO 10 I=1,3
      GM(I,1)=C1(I)-C2(I)
      GM(I,2)=ZERO
      GM(I,3)=C3(I)-C2(I)
      GM(I,4)=C4(I)-C2(I)
      R=R+GM(I,3)**2
   10 CONTINUE
      R=SQRT(R)
      THETA=ACOS(GM(3,3)/R)
      if(abs(gm(1,3)).gt.1.0e-08)then
      PHI=ATAN(GM(2,3)/GM(1,3))
      if(gm(1,3).lt.0)phi=phi+pi
      else
      phi=pi/2.d0
      if(gm(2,3).lt.0)phi=-phi
      endif
      DO 11 I=1,2
      RM1(3,I)=ZERO
  11  RM1(I,3)=ZERO
      RM1(3,3)=ONE
      RM1(1,1)=COS(PHI)
      RM1(2,2)=COS(PHI)
      RM1(1,2)=SIN(PHI)
      RM1(2,1)=-SIN(PHI)
      DO 15 I=1,4
   15 CALL EBC(RGM(1,I),RM1,GM(1,I),3,3,1)
      DO 21 I=1,3
      RM1(2,I)=ZERO
   21 RM1(I,2)=ZERO
      RM1(2,2)=ONE
      RM1(1,1)=COS(THETA)
      RM1(3,3)=COS(THETA)
      RM1(1,3)=-SIN(THETA)
      RM1(3,1)= SIN(THETA)
      DO 25 I=1,4
   25 CALL EBC(GM(1,I),RM1,RGM(1,I),3,3,1)
      if(abs(gm(1,3))/r.gt.1.0e-08)go to 900
      if(abs(gm(2,3))/r.gt.1.0e-08)go to 900
      A1=ATAN(GM(2,1)/GM(1,1))
      if(gm(1,1).lt.0)a1=a1+pi
      A4=ATAN(GM(2,4)/GM(1,4))
      if(gm(1,4).lt.0)a4=a4+pi
      DIH=(A4-A1)/PI*180.
      RETURN
  900 continue
      WRITE(6,1000)gm(1,3),gm(2,3),gm(3,3),theta/pi*180.,phi/pi*180.
      DIH=-999
 1000 format('DIHEDRAL ANGLE ALGORITHM ERROR',/5x,5f12.6)
      END
      SUBROUTINE DIHED1(C1,C2,C3,C4,DIH)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION C1(3),C2(3),C3(3),C4(3)
     1          ,RGM(3,4),GM(3,4),RM1(3,3)
      DATA ZERO/0.D0/,ONE/1.D0/,PI/3.141592654D0/
      R=ZERO
      rl1=0
      rl2=0
      dot=0
      DO 10 I=1,3
      GM(I,1)=C1(I)-C2(I)
      GM(I,2)=ZERO
      GM(I,3)=C3(I)-C2(I)
      GM(I,4)=C4(I)-C2(I)
      dot=dot+gm(i,1)*(gm(i,4)-gm(i,3))
      rl1=rl1+gm(i,1)**2
      rl2=rl2+(gm(i,4)-gm(i,3))**2
      R=R+GM(I,3)**2
   10 CONTINUE
      R=SQRT(R)
      THETA=ACOS(GM(3,3)/R)
      if(abs(gm(1,3)).gt.1.0e-08)then
      PHI=ATAN(GM(2,3)/GM(1,3))
      if(gm(1,3).lt.0)phi=phi+pi
      else
      phi=pi/2.d0
      if(gm(2,3).lt.0)phi=-phi
      endif
      DO 11 I=1,2
      RM1(3,I)=ZERO
  11  RM1(I,3)=ZERO
      RM1(3,3)=ONE
      RM1(1,1)=COS(PHI)
      RM1(2,2)=COS(PHI)
      RM1(1,2)=SIN(PHI)
      RM1(2,1)=-SIN(PHI)
      DO 15 I=1,4
   15 CALL EBC(RGM(1,I),RM1,GM(1,I),3,3,1)
      DO 21 I=1,3
      RM1(2,I)=ZERO
   21 RM1(I,2)=ZERO
      RM1(2,2)=ONE
      RM1(1,1)=COS(THETA)
      RM1(3,3)=COS(THETA)
      RM1(1,3)=-SIN(THETA)
      RM1(3,1)= SIN(THETA)
      DO 25 I=1,4
   25 CALL EBC(GM(1,I),RM1,RGM(1,I),3,3,1)
      if(abs(gm(1,3))/r.gt.1.0e-08)go to 900
      if(abs(gm(2,3))/r.gt.1.0e-08)go to 900
      do 31 i=3,3
      rl1=rl1-gm(i,1)**2
      rl2=rl2-(gm(i,4)-gm(i,3))**2
      dot=dot-gm(i,1)*(gm(i,4)-gm(i,3))
   31 continue
c  (-) for consistency with other dih method
C      dih=-dot/sqrt(rl1*rl2)
c8/1/01      dih=-(dot/sqrt(rl1))/sqrt(rl2)
      dih=0.
      if(abs(dot).gt.1.0e-07)dih=-dot/sqrt(rl1*rl2)
      RETURN
  900 continue
      WRITE(6,1000)gm(1,3),gm(2,3),gm(3,3),theta/pi*180.,phi/pi*180.
      DIH=-999
 1000 format('DIHEDRAL ANGLE ALGORITHM ERROR',/5x,5f12.6)
      END
      SUBROUTINE DIHED(C1,C2,C3,C4,DIH)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION C1(3),C2(3),C3(3),C4(3)
     1          ,RGM(3,4),GM(3,4),RM1(3,3)
      DATA ZERO/0.D0/,ONE/1.D0/,PI/3.141592654D0/
      R=ZERO
      rl1=0
      rl2=0
      dot=0
      DO 10 I=1,3
      GM(I,1)=C1(I)-C2(I)
      GM(I,2)=ZERO
      GM(I,3)=C3(I)-C2(I)
      GM(I,4)=C4(I)-C2(I)
      dot=dot+gm(i,1)*(gm(i,4)-gm(i,3))
      rl1=rl1+gm(i,1)**2
      rl2=rl2+(gm(i,4)-gm(i,3))**2
      R=R+GM(I,3)**2
   10 CONTINUE
      R=SQRT(R)
      THETA=ACOS(GM(3,3)/R)
      if(abs(gm(1,3)).gt.1.0e-08)then
      PHI=ATAN(GM(2,3)/GM(1,3))
      if(gm(1,3).lt.0)phi=phi+pi
      else
      phi=pi/2.d0
      if(gm(2,3).lt.0)phi=-phi
      endif
      DO 11 I=1,2
      RM1(3,I)=ZERO
  11  RM1(I,3)=ZERO
      RM1(3,3)=ONE
      RM1(1,1)=COS(PHI)
      RM1(2,2)=COS(PHI)
      RM1(1,2)=SIN(PHI)
      RM1(2,1)=-SIN(PHI)
      DO 15 I=1,4
   15 CALL EBC(RGM(1,I),RM1,GM(1,I),3,3,1)
      DO 21 I=1,3
      RM1(2,I)=ZERO
   21 RM1(I,2)=ZERO
      RM1(2,2)=ONE
      RM1(1,1)=COS(THETA)
      RM1(3,3)=COS(THETA)
      RM1(1,3)=-SIN(THETA)
      RM1(3,1)= SIN(THETA)
      DO 25 I=1,4
   25 CALL EBC(GM(1,I),RM1,RGM(1,I),3,3,1)
      if(abs(gm(1,3))/r.gt.1.0e-08)go to 900
      if(abs(gm(2,3))/r.gt.1.0e-08)go to 900
      do 31 i=3,3
      rl1=rl1-gm(i,1)**2
      rl2=rl2-(gm(i,4)-gm(i,3))**2
      dot=dot-gm(i,1)*(gm(i,4)-gm(i,3))
   31 continue
c  (-) for consistency with other dih method
C      dih=-dot/sqrt(rl1*rl2)
      if(abs(dot).gt.1.0e-07)then
      dih=-dot/sqrt(rl1*rl2)
      dih=180.d0/pi*acos(dih)
      else
      dih=90.
      endif
      RETURN
  900 continue
      WRITE(6,1000)gm(1,3),gm(2,3),gm(3,3),theta/pi*180.,phi/pi*180.
      DIH=-999
 1000 format('DIHEDRAL ANGLE ALGORITHM ERROR',/5x,5f12.6)
      END

      SUBROUTINE DIST(NATOMS,GM,IOUT)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION GM(3,NATOMS)
      DATA TOA/0.52917715D0/
      DIJ(I,J)=DSQRT((GM(1,I)-GM(1,J))**2+(GM(2,I)-GM(2,J))**2+
     1         (GM(3,I)-GM(3,J))**2)
      WRITE(IOUT,1001)'AU      '
      DO 10 I=2,NATOMS
      WRITE(IOUT,1000)I,(DIJ(I,J),J=1,I-1)
   10 CONTINUE
      WRITE(IOUT,1001)'ANGSTROM'
      DO 20 I=2,NATOMS
      WRITE(IOUT,1000)I,(DIJ(I,J)*TOA,J=1,I-1)
   20 CONTINUE
      RETURN
 1000 FORMAT(2X,I3,2X,6F12.6,/,(7X,6F12.6))
 1001 FORMAT(/5X,'INTERNUCLEAR DISTANCES IN ',A8)
      END
C
      SUBROUTINE FANGL(NATOMS,GM,IOUT)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION GM(3,2)
      WRITE(IOUT,1000)
      DO 10 I=1,NATOMS-2
      DO 11 J=I+1,NATOMS-1
      DO 11 K=J+1,NATOMS
      CALL ANGL(GM,I,J,K,ANG1,W,X,Y,Z)
      CALL ANGL(GM,J,K,I,ANG2,W,X,Y,Z)
      CALL ANGL(GM,K,I,J,ANG3,W,X,Y,Z)
      WRITE(IOUT,1001)I,J,K,ANG1, J,K,I,ANG2, K,I,J,ANG3
   11 CONTINUE
   10 CONTINUE
      RETURN
 1000 FORMAT(/5X,'INTERNUCLEAR ANGLES IN DEGREES',/)
 1001 FORMAT(2X,3(3(I2,1X),F6.1,2X))
      END
      SUBROUTINE FDANGL(NATOMS,GM,IOUT)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION GM(3,2)
      WRITE(IOUT,1000)
      IEVE=0
      DO 10 I=1,NATOMS-3
      DO 11 J=I+1,NATOMS-2
      DO 11 K=J+1,NATOMS-1
      DO 11 L=K+1,NATOMS
      I0=I
      J0=J
      K0=K
      L0=L
      CALL DIHED(GM(1,I),GM(1,J),GM(1,K),GM(1,L),DIH)
      IEVE=IEVE+1
      IF(IAND(IEVE,1).NE.0)THEN
      I1=I
      J1=J
      K1=K
      L1=L
      DIH1=DIH
      GO TO 11
      ENDIF
      WRITE(IOUT,1001)I1,J1,K1,L1,DIH1,I,J,K,L,DIH
   11 CONTINUE
      IF(IAND(IEVE,1).NE.0)WRITE(IOUT,1001)I0,J0,K0,L0,DIH
   10 CONTINUE
      RETURN
 1000 FORMAT(/5X,'INTERNUCLEAR DIHEDRAL IN DEGREES',/)
 1001 FORMAT(2X,3(4(I2,1X),F6.1,2X))
      END
      SUBROUTINE GAUSS(B,NMIX)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION B(1)
      DATA ZERO/1.0D-12/
      IND(I)=(I-1)*NMIX1

C           GAUSSIAN ELIMINATION
      NMIX1=NMIX+1
C
C           BEGIN THE ELIMINATION
C
      DO 140 I=1,NMIX
      M=IND(I)
      IF(DABS(B(M+I)).LT.ZERO)GO TO 70
      FX=1./B(M+I)
      GO TO 95
  70  CONTINUE
      IF(I.EQ.NMIX)GO TO 1000
      I1=I+1
C           PIVOT SECTION
      DO 90 J=I1,NMIX
      INJ=IND(J)
      IF(DABS(B(INJ+I)).LT.ZERO)GO TO 90
      FX=1./B(INJ+I)
      DO 85 L=I,NMIX1
      TEMP=B(INJ+L)
      B(INJ+L)=B(M+L)
      B(M+L)=TEMP
  85  CONTINUE
      GO TO 95
  90  CONTINUE
      GO TO 1000
  95  CONTINUE
      DO 100 J=I,NMIX1
      B(M+J)=B(M+J)*FX
 100  CONTINUE
C         END PIVOT
      DO 130 J=1,NMIX
      IF(I.EQ.J)GO TO 130
      L=IND(J)
      Y=B(L+I)
      DO 120 K=I,NMIX1
      B(L+K)=B(L+K)-Y*B(M+K)
 120  CONTINUE
 130  CONTINUE
 140  CONTINUE
C
C       MOVE THE SOLUTION THE TO BEGINNING OF CORE
C
      DO 150 I=1,NMIX
      B(I)=B(IND(I)+NMIX1)
 150  CONTINUE
      RETURN
1000  CONTINUE
      WRITE(6,1010)
1010  FORMAT('  ABORT GAUSS  SINGULAR MATRIX ')
      STOP
      END
      SUBROUTINE IBASIS(NR,R,V,N,BF,SCR,IOUT,IPFLG,IFAIL)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION R(N,NR),SCR(2),BF(2),V(N,2)
      COMMON/VIBORD/IVBORD1(90),IVBORD2(90)
C      NF=N+Nfixdir
      nfixdir=0
      NF=N
      NDO=NF**2
      DO 5 I=1,NDO
    5 SCR(I)=0.D0
      IJ=0
      IF(NR.EQ.0)GO TO 11
      DO 10 I=1,NR
      DO 20 J=1,N
      SCR(IJ+J)=R(J,I)
   20 CONTINUE
      IJ=IJ+NF
   10 CONTINUE
   11 CONTINUE
C         LAGRANGE MULTIPLIER slots not used
      IF(NFIXDIR.NE.0)THEN
      DO 27 KFIXDIR=1,NFIXDIR
      SCR(IJ+N+KFIXDIR)=1.0
      IJ=IJ+NF
   27 CONTINUE
      ENDIF
C         VIBRATION SLOTS
      NDO=N-NR
      DO 40 I=1,NDO
      DO 30 J=1,N
      SCR(IJ+J)=V(J,I)
   30 CONTINUE
      IJ=IJ+NF
   40 CONTINUE
C
      IF(IPFLG.LE.0)GO TO 46
      IHI=0
      WRITE(IOUT,1002)
      DO 45 I=1,NF
      LOW=IHI+1
      IHI=IHI+NF
      WRITE(IOUT,1000)I,(SCR(K),K=LOW,IHI)
   45 CONTINUE
   46 CONTINUE
C
      CALL SCHMO(NF,SCR,IOUT,IFAIL)
C
      IF(IFAIL.EQ.0)GO TO 70
      IF(IFAIL.LE.(NR+NFIXDIR))GO TO 900
      IPUT=IFAIL-(NR+NFIXDIR)
      WRITE(IOUT,1003)IVBORD2(IPUT),IVBORD1(IVBORD2(IPUT))
      IVBORD1(IVBORD2(IPUT))=-1
      RETURN
   70 CONTINUE
      WRITE(IOUT,1004)(IVBORD2(I),IVBORD1(IVBORD2(I)),I=1,N-NR)
C
      IX=0
      IY=NR*NF
      NDO=NF-NR
      DO 50 I=1,NDO+1
      DO 50 J=1,NF
      IX=IX+1
      IY=IY+1
   50 BF(IX)=SCR(IY)
C
      IHI=0
      WRITE(IOUT,1001)
      DO 60 I=1,NDO
      LOW=IHI+1
      IHI=IHI+NF
      WRITE(IOUT,1000)I,(BF(K),K=LOW,IHI)
   60 CONTINUE
      RETURN
 900  CONTINUE
      WRITE(IOUT,1003)IFAILED
      STOP ' SCHMO FAILURE IN IBASIS '
 1000 FORMAT(2X,I3,(8F10.6))
 1001 FORMAT(' ON - BASIS FOR HESSIAN ')
 1002 FORMAT(' ORIGINAL - BASIS FOR HESSIAN ')
 1003 FORMAT(5X,'SCHMO FAILURE IN IBASIS:BASIS FUNCTION=',2I3)
CDRY I2 FORMAT FAILED 3/25/92 IN 1004
C    VALUES LOOK OK - SCARY
 1004 FORMAT(/5X,'INTERNAL STATE BASIS',/,(5(F3.0,' (',F3.0,') ') ))
C 1004 FORMAT(/5X,'INTERNAL STATE BASIS',/,5(I2,' (',I2,') ') )
       END
      SUBROUTINE INTMO(NATOMS,GM,DISP,KILLER)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION GM(2),DISP(2),KILLER(2)
      NDO=NATOMS*3
      IX=0
      DO 10 I=1,NDO
      IF(KILLER(I).EQ.1)GO TO 10
      IX=IX+1
      GM(I)=GM(I)+DISP(IX)
   10 CONTINUE
      RETURN
      END
      SUBROUTINE IIDOF(NATOMS,MOTION,NDIAT,KILLER,Z,IOUT,IONE)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION KILLER(2),NDIAT(2),A(2),SCR(2)
      INTEGER Z
C         ELIMINATE FORMAL DOF (1,2,3)
C         AND Z-AXIS IF CS SYMMETRY
C         OR X-Y MOTION FOR LINEAR SYSTEM
      DO 10 I=1,NATOMS
      IF(NDIAT(I).EQ.1)IONE=I
      DO 10 J=1,3
      IJ=(I-1)*3+J
      IPUT=0
      IF((MOTION.EQ.2).AND.(J.EQ.Z))IPUT=1
      IF((MOTION.EQ.1).AND.(J.NE.Z))IPUT=1
   10 KILLER(IJ)=IPUT
      DO 11 I=1,3
      IPUT=(IONE-1)*3+I
   11 KILLER(IPUT)=1
C
      WRITE(IOUT,1000)(KILLER(I),I=1,NATOMS*3)
      RETURN
C
      ENTRY IDOF(NATOMS,A,SCR,KILLER)
C
      F=-1.D0/DFLOAT(NATOMS)
      DO 200 I=1,3*NATOMS
  200 SCR(I)=0.D0
C
      DO 230 L=1,3
      DO 220 I=1,NATOMS
      IXYZ=(I-1)*3+L
      SCR(IXYZ)=SCR(IXYZ)+A(IXYZ)
      DO 220 J=1,NATOMS
      JXYZ=(J-1)*3+L
      SCR(JXYZ)=SCR(JXYZ)+A(IXYZ)*F
  220 CONTINUE
  230 CONTINUE
C
      IX=0
      DO 210 I=1,NATOMS*3
      IF(KILLER(I).EQ.1)GO TO 210
      IX=IX+1
      A(IX)=SCR(I)
  210 CONTINUE
      RETURN
 1000 FORMAT(5X,'CARTESIAN TO INTERNAL DOF MAPPING',/(10(2X,3I2)))
      END
      subroutine mkpscent(natoms,maxnew,psc,dpsc,ipsc,mass,iout)
      IMPLICIT REAL*8(A-H,O-Z)
      dimension psc(30,2),dpsc(30,2),ipsc(30,2)
      real*8 mass(2),masst
      write(iout,1001)
      do 20 i=1,maxnew
      masst=0
      do 30 j=1,natoms
   30 psc(j,i)=0.d0
      do 31 j=1,natoms
      jj=ipsc(j,i)
      if(jj.eq.0)go to 31
      if(jj.gt.0)then
      cf=dpsc(j,i)
      else
      cf=mass(iabs(jj))
      masst=masst+cf
      endif
      psc(iabs(jj),i)=cf
   31 continue
      if(masst.lt.0.01)masst=1
      do 32 j=1,natoms
   32 psc(j,i)=psc(j,i)/masst
      write(iout,1000)i,(j,psc(j,i),j=1,natoms)
   20 continue
      return
 1000 format(2x,i2,6(1x,i2,f9.5))
 1001 format(/15x,'PSEUDO CENTERS')
      end

      SUBROUTINE MROTV(NATOMS,IONE,V,GM,KZ,KILLER)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION GM(2),V(2),ICROSS(3,3),LOOP(3),KILLER(2)
      DATA ICROSS/0,-3,2, 3,0,-1, 2,-1,0/
      LOOP(1)=KZ
      IX=1
      DO 1 I=1,3
      IF(KZ.EQ.I)GO TO 1
      IX=IX+1
      LOOP(IX)=I
    1 CONTINUE
      IX=0
      DO 20 JJ=1,3
      J=LOOP(JJ)
      IK=0
      DO 10 I=1,NATOMS
      DO 10 K=1,3
      IK=IK+1
      IF(KILLER(IK).EQ.1)GO TO 10
      IX=IX+1
      KK=ICROSS(K,J)
      V(IX)=0.D0
      IF(KK)8,10,9
    9 V(IX)=GM((I-1)*3+KK)-GM((IONE-1)*3+KK)
      GO TO 10
    8 V(IX)=-(GM((I-1)*3-KK)-GM((IONE-1)*3-KK))
   10 CONTINUE
   20 CONTINUE
      RETURN
      END
      SUBROUTINE MVIBV(NATOM,NVIB,NDEGF,V,VR,I76,KILLER)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION VR(2),KILLER(2),JSEC(50),V(2)
      COMMON/VIBORD/IVBORD(90),IVBORD2(90)
      MASK=2*16-1
C
      m76=0
      natoms=natom
  500 CONTINUE
      NATOM3=NATOMS*3
C
      NDO=0
      MVIB=0
      IK=0
      DO 10 I=1,NATOMS
C         SYMMETRY EQUIVALENT CENTER INSTRUCTIONS
      IF(M76.NE.0)READ(I76,1001)NDO
      IF(NDO.NE.0)READ(I76,1002)(JSEC(J),J=1,NDO)
C
      DO 10 K=1,3
      IK=IK+1
      IF(KILLER(IK).EQ.1)GO TO 10
      MVIB=MVIB+1
      ISKIP=(MVIB-1)*NATOM3
      IX=(I-1)*3+K + ISKIP
      V(IX)=1.0
C
      IF(NDO.EQ.0)GO TO 50
      DO 40 J=1,NDO
      NCENT=IAND(MASK,JSEC(J))
      PHASE=1.D0
      IF(NCENT.LT.I)PHASE=-1.D0
      IP=IAND(1,ISHFT(JSEC(J),-(16+K-1)))
      IX=(NCENT-1)*3+K+ISKIP
      V(IX)=PHASE
      IF(IP.NE.0)V(IX)=-PHASE
   40 CONTINUE
   50 CONTINUE
C
   10 CONTINUE
C
  100 CONTINUE
      IX=0
      LVIB=0
      DO 110 KVIB=1,NDEGF
      IVIB=IVBORD(KVIB)
      IF(IVIB.LE.0)GO TO 110
      LVIB=LVIB+1
      ISKIP=(IVIB-1)*NATOM3
      IVBORD2(LVIB)=KVIB
      DO 120 IK=1,NATOM3
      IF(KILLER(IK).EQ.1)GO TO 120
      IX=IX+1
      VR(IX)=V(IK+ISKIP)
  120 CONTINUE
      IF(LVIB.EQ.NVIB)GO TO 111
  110 CONTINUE
  111 CONTINUE
      IF(LVIB.NE.NVIB)GO TO 910
      RETURN
  900 CONTINUE
      WRITE(IOUT,1000)I76,NATOM,NATOMS
      STOP ' ERROR IN VIBV '
  910 CONTINUE
      WRITE(IOUT,1003)LVIB,NVIB
      STOP ' ERROR MAKING VIB '
 1000 FORMAT(5X,'UNIT=',I3,' ERROR NATOM=',I3,' NATOMS=',I3)
 1001 FORMAT(16I4)
 1002 FORMAT(8(1X,Z8))
 1003 FORMAT(5X,'ERROR MAKING VIB BASIS: HAVE=',I3,' NEED=',I3)
      END
      SUBROUTINE RBB(N,BETA,A,B,AN,BN)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION A(N),B(N),AN(N),BN(N)
      C2B=COS(2*BETA)
      S2B=SIN(2*BETA)
      DO 10 I=1,N
      AN(I)=C2B*A(I)-S2B*B(I)
 10   BN(I)=S2B*A(I)+C2B*B(I)
      RETURN
      END
      SUBROUTINE RBASIS(NR,R,N,SCR,ISCR,BF,IOUT,IPFLG)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION R(N,NR),SCR(2),BF(2),ISCR(2,2)
C         ISCR(1,*) LIST OF AVAILABLE BF
C         ISCR(2,*) CURRENT BF ( NR BF NOT COUNTED )
      DO 2 I=1,N
    2 ISCR(1,I)=I
      LOOP=0
  500 CONTINUE
      NDO=N**2
      DO 5 I=1,NDO
    5 SCR(I)=0.D0
      IJ=0
      IF(NR.EQ.0)GO TO 11
      DO 10 I=1,NR
      DO 10 J=1,N
      IJ=IJ+1
      SCR(IJ)=R(J,I)
   10 CONTINUE
   11 CONTINUE
      IOFF=NR*N
      KOFF=0
      DO 40 I=1,N
      IF(ISCR(1,I).LE.0)GO TO 40
      SCR(ISCR(1,I)+IOFF)=1.D0
      KOFF=KOFF+1
      ISCR(2,KOFF)=I
      IOFF=IOFF+N
      IF(KOFF.EQ.(N-NR))GO TO 41
   40 CONTINUE
   41 CONTINUE
      NDO=N-NR
C
      IF(IPFLG.LE.0)GO TO 46
      IHI=0
      WRITE(IOUT,1002)
      DO 45 I=1,N
      LOW=IHI+1
      IHI=IHI+N
      WRITE(IOUT,1000)I,(SCR(K),K=LOW,IHI)
   45 CONTINUE
   46 CONTINUE
C
      CALL SCHMO(N,SCR,IOUT,IFAIL)
      IF(IFAIL.EQ.0)GO TO 47
      IF(IFAIL.LE.NR)GO TO 900
      IPUT=IFAIL-NR
      ISCR(1,ISCR(2,IPUT))=-1
      LOOP=LOOP+1
      WRITE(IOUT,1004)LOOP,ISCR(2,IPUT)
      IF(LOOP.GT.NR)GO TO 900
      GO TO 500
   47 CONTINUE
      IX=0
      IY=NR*N
      DO 50 I=1,NDO
      DO 50 J=1,N
      IX=IX+1
      IY=IY+1
   50 BF(IX)=SCR(IY)
C
      IHI=0
      WRITE(IOUT,1001)
      DO 60 I=1,N-NR
      LOW=IHI+1
      IHI=IHI+N
      WRITE(IOUT,1000)I,(BF(K),K=LOW,IHI)
   60 CONTINUE
      RETURN
 900  CONTINUE
      WRITE(IOUT,1003)IFAILED
      STOP ' SCHMO FAILURE IN RBASIS '
 1000 FORMAT(2X,I3,(8F10.6))
 1001 FORMAT(' ON - BASIS FOR REDUCED W-MATRIX ')
 1002 FORMAT(' ORIGINAL - BASIS FOR W-MATRIX ')
 1003 FORMAT(5X,'SCHMO FAILURE IN RBASIS:BASIS FUNCTION=',I3)
 1004 FORMAT(5X,'SCHMO FAILURE:LOOP=',I3,' BF=',I3)
      END
      SUBROUTINE rptcon(NFIXDIR,FIXDIR,VALDIR,NDEGF,NATOMS,
     1                  GM,KILLER,KLASS,IOUT)
      IMPLICIT REAL*8(A-H,O-Z)
      INTEGER FIXDIR(6,2),KLASS(2)
      DIMENSION KILLER(2),VALDIR(2)
      DIMENSION GM(3,2)
      common/pscent/maxnew,psc(30,10),dpsc(30,10),ipsc(30,10)
      DATA TWO/2.D0/,ZERO/0.D0/,PI/3.141592654D0/
      if(maxnew.eq.0)go to 99
      do 40 i=1,maxnew
      do 41 k=1,3
   41 gm(k,natoms+i)=0
      do 42 k=1,3
      do 42 j=1,natoms
   42 gm(k,natoms+i)=gm(k,natoms+i)+gm(k,j)*psc(j,i)
   40 continue
   99 continue
C
      IF(NFIXDIR.EQ.0)GO TO 900
      NF=NDEGF+NFIXDIR
      N3=NATOMS*3
C
      MYCLASS=0
      ZNORM=ZERO
      DO 200 I=1,NFIXDIR
      II=FIXDIR(1,I)
      JJ=FIXDIR(2,I)
      GO TO (400,600,400,600,700),KLASS(I)
      STOP ' ILLEGAL KLASS '
  400 CONTINUE
C         DISTANCE CONTRAINT
      MYCLASS=1
      X = (GM(1,II)-GM(1,JJ))**2 + (GM(2,II)-GM(2,JJ))**2+
     1         (GM(3,II)-GM(3,JJ))**2
      IF(KLASS(I).EQ.3)GO TO 150
      WRITE(IOUT,1028)II,JJ,DSQRT(X),VALDIR(I)
      ZNORM=ZNORM+(X-VALDIR(I)**2)**2
      GO TO 200
  150 CONTINUE
C         DISTANCE DIFFERENCE CONSTRAINT
      kk=FIXDIR(3,I)
      LL=FIXDIR(4,I)
      y = (GM(1,kk)-GM(1,LL))**2 + (GM(2,kk)-GM(2,LL))**2+
     1         (GM(3,kk)-GM(3,LL))**2
      WRITE(IOUT,1027)II,JJ,DSQRT(X),KK,LL,DSQRT(Y)
      GO TO 200
  700 CONTINUE
C         dihedral ANGLE CONSTRAINT
      KK=FIXDIR(3,I)
      LL=FIXDIR(6,I)
      KKU=(KK-1)*3
      LLU=(LL-1)*3
      CALL DIHED1(GM(1,II),GM(1,JJ),GM(1,KK),GM(1,LL),DIH1)
      WRITE(IOUT,1030)II,JJ,KK,LL,acos(DIH1)*180.d0/pi,VALDIR(I)
      XX=VALDIR(I)*PI/180.D0
      GO TO 200
  600 CONTINUE
C         ANGLE CONSTRAINT
      KK=FIXDIR(3,I)
      CALL ANGL(GM,II,JJ,KK,ANG,CA,R13,R12,R23)
      IF(KLASS(I).EQ.4)GO TO 170
      WRITE(IOUT,1029)II,JJ,KK,ANG,VALDIR(I)
      XX=VALDIR(I)*PI/180.D0
      GO TO 200
  170 CONTINUE
C         ANGLE DIFFERENCE
      IIO=II
      JJO=JJ
      KKO=KK
      II=FIXDIR(4,I)
      JJ=FIXDIR(5,I)
      KK=FIXDIR(6,I)
      CALL ANGL(GM,II,JJ,KK,ANGB,CB,R13,R12,R23)
      CAT=CA
      CA=CB
      WRITE(IOUT,1026)IIO,JJO,KKO,ANG,II,JJ,KK,ANGB
  200 CONTINUE
      ZNORM=DSQRT(ZNORM)
      IF(MYCLASS.EQ.1)WRITE(IOUT,1001)ZNORM
C
  900 CONTINUE
      RETURN
 1000 FORMAT(5X,'GRADIENT ',A7,' CONSTRAINTS:DIMENSION=',I3,
     X     (/5X,8F12.6))
 1001 FORMAT(/5X,'DISTANCE NORM=',E15.8)
 1026 FORMAT(5X,'ANG(',I2,',',I2,',',I2,')  ='
     X       ,F12.6,' ANG(',I2,',',I2,',',I2,')  =',F12.6)
 1027 FORMAT(5X,'|R(',I2,') - R(',I2,')|=',F12.6,
     X ' |R(',I2,') - R(',I2,')|=',F12.6)
 1028 FORMAT(5X,'|R(',I2,') - R(',I2,')|=',F12.6,
     X ' CONSTRAINT VALUE=',F12.6)
 1029 FORMAT(5X,'ANG(',I2,',',I2,
     X       ',',I2,')  =',F12.6,' CONSTRAINT VALUE=',F12.6)
 1030 FORMAT(5X,'DIH ANG(',I2,',',I2,
     X       ',',I2,',',i2,')  =',F12.6,' CONSTRAINT VALUE=',F12.6)
      END
      SUBROUTINE SCHMO(N,V,IOUT,IFAIL)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION V(N,N)
      IFAIL=0
      DO 100 I=1,N
      Z=0.D0
      DO 90 J=1,N
   90 Z=Z+V(J,I)**2
      Z=DSQRT(Z)
      DO 80 J=1,N
   80 V(J,I)=V(J,I)/Z
  100 CONTINUE
C
      DO 500 I=2,N
      DO 300 J=1,I-1
      DP=DOT(V(1,I),V(1,J),N)
      DO 250 K=1,N
  250 V(K,I)=V(K,I)-DP*V(K,J)
  300 CONTINUE
      Z=0.D0
      DO 260 K=1,N
  260 Z=Z+V(K,I)**2
      Z=DSQRT(Z)
      IF(Z.LT.1.0D-06)THEN
      WRITE(IOUT,1000)I,Z
      IFAIL=I
      RETURN
      ENDIF
      DO 270 K=1,N
  270 V(K,I)=V(K,I)/Z
  500 CONTINUE
      RETURN
 1000 FORMAT(5X,'BAD VECTOR IN SCHMO:I=',I3,' NORM=',E15.6)
      END
      SUBROUTINE SCHMOM(N,M,V,IOUT,IFAIL)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION V(N,M)
      IFAIL=0
      DO 100 I=1,M
      Z=0.D0
      DO 90 J=1,N
   90 Z=Z+V(J,I)**2
      Z=DSQRT(Z)
      DO 80 J=1,N
   80 V(J,I)=V(J,I)/Z
  100 CONTINUE
C
      DO 500 I=2,M
      DO 300 J=1,I-1
      DP=DOT(V(1,I),V(1,J),N)
      DO 250 K=1,N
  250 V(K,I)=V(K,I)-DP*V(K,J)
  300 CONTINUE
      Z=0.D0
      DO 260 K=1,N
  260 Z=Z+V(K,I)**2
      Z=DSQRT(Z)
      IF(Z.LT.1.0D-06)THEN
      WRITE(IOUT,1000)I,Z
      IFAIL=I
      RETURN
      ENDIF
      DO 270 K=1,N
  270 V(K,I)=V(K,I)/Z
  500 CONTINUE
      RETURN
 1000 FORMAT(5X,'BAD VECTOR IN SCHMO:I=',I3,' NORM=',E15.6)
      END
      SUBROUTINE TBAK(NL,NS,T,X,Y,IGO)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION T(NL,NS),X(2),Y(2)
      DO 10 I=1,NL
   10 Y(I)=0.D0
      DO 20 I=1,NS
      DO 20 J=1,NL
      Y(J)=Y(J)+T(J,I)*X(I)
   20 CONTINUE
      IF(IGO.EQ.1)RETURN
      DO 30 I=1,NL
   30 X(I)=Y(I)
      RETURN
      END
      SUBROUTINE TOINTRL(MASS,GM,NACME,GRAD,IOUT,NAME)
      IMPLICIT REAL*8(A-H,O-Z)
C       NACME(XYZ,ATOM)
      REAL*8 MASS(3),NACME(3,3),GM(3,3),CCORDI(2,2),PCORDI(2,2)
     1      ,NACINTC(2,3),NACINTP(2,2),COMD(2),BI(3,3),GRAD(3)
      EQUIVALENCE(N1,NDIAT(1)),(N2,NDIAT(2)),(N3,NDIAT(3))
      CHARACTER*3 NAME
      COMMON/DIATAT/NDIAT(30)
C
      PI=DACOS (-1.D0)
      RTD=180.D0/PI
      WRITE(IOUT,1003)NAME,NACME
      TMASSD=MASS(N2)+MASS(N3)
C         RELATIVE TO COM OF TRIATOM
C         BUILD BI MATRIX  TO DETERMINE INTERNAL NACME'S
      CALL BCOM(MASS,BI)
      DO 110 I=1,2
      COMD(I)=( MASS(N2)*GM(I,N2) + MASS(N3)*GM(I,N3) )/TMASSD
      CCORDI(I,2)=GM(I,N3)-GM(I,N2)
      CCORDI(I,1)=GM(I,N1)-COMD(I)
      X=0.D0
      Y=0.D0
      Z=0.D0
      DO 115 J=1,3
      X=X + NACME(I,J)*BI(J,1)
      Y=Y + NACME(I,J)*BI(J,2)
      Z=Z + NACME(I,J)*BI(J,3)
  115 CONTINUE
      NACINTC(I,1)=X
      NACINTC(I,2)=Y
      NACINTC(I,3)=Z
  110 CONTINUE
C      WRITE(IOUT,1009)((NACINTC(I,J),J=1,3),I=1,2)
      DO 120 I=1,2
      PCORDI(1,I)=DSQRT( CCORDI(1,I)**2 + CCORDI(2,I)**2 )
      X=DACOS ( CCORDI(1,I)/PCORDI(1,I) )
      IF(CCORDI(2,I).LT.0.D0)X=2.D0*PI-X
      PCORDI(2,I)=X*RTD
      NACINTP(1,I)=(CCORDI(1,I)*NACINTC(1,I)+CCORDI(2,I)*NACINTC(2,I))
     1             /PCORDI(1,I)
      NACINTP(2,I)=CCORDI(1,I)*NACINTC(2,I)-CCORDI(2,I)*NACINTC(1,I)
  120 CONTINUE
      WRITE(IOUT,1008)PCORDI(1,1),PCORDI(1,2),
     X               PCORDI(2,1)-PCORDI(2,2)
C      WRITE(IOUT,1005)NACINTP
      DBR=NACINTP(1,1)
      DLR=NACINTP(1,2)
      DGAMMA=(NACINTP(2,1)-NACINTP(2,2) )/2.D0
      DG =(NACINTP(2,2)-NACINTP(2,1))/( 2.D0*PCORDI(1,1) )
      DGAMMA=(NACINTP(2,1)-NACINTP(2,2) )/2.D0
      DOMEGA=(NACINTP(2,2)+NACINTP(2,1))/2.D0
C      WRITE(IOUT,1013)' TRIATOM'
      GRAD(1)=DLR
      GRAD(2)=DBR
      GRAD(3)=DGAMMA
      WRITE(IOUT,1004)NAME,GRAD
      WRITE(IOUT,1014)NAME,DOMEGA,NACINTC(1,3),NACINTC(2,3)
C      WRITE(IOUT,1011)DG,PCORDI(1,1)
      RETURN
 1003 FORMAT(/5X,'CARTESIAN ',A3,/7X,'X',14X,'Y',/(1X,3F15.9) )
 1004 FORMAT(/5X,A3,' IN JACOBI r  R gma:', 3(1X,E16.9))
 1005 FORMAT(/5X,'  POLAR NACMES',/7X,'R',12X,'THETA',
     2       (2(/1X,2E16.9)) )
 1006 FORMAT(/5X,' INTERNAL POLAR COORDINATES',/7X,'R',12X,'THETA',
     2       (2(/1X,2E16.9)) )
C1007 FORMAT(' BI MATRIX:',/(2X,3F12.6) )
 1008 format(/,' R  r  gamma  ',3F15.9)
 1009 FORMAT(/,' INTERNAL CARTESIAN NACMES',/' X',3E16.9,/' Y',3E16.9)
 1011 FORMAT(' REVISED VERSION: - 1/R DGM=',E16.9,' R =',F12.6)
 1013 FORMAT(/5X,' FIXED ORIGIN IS ',A8 )
 1014 FORMAT(5X,A3,' TR-ROT: ANG=',F12.6,' X3=',F12.6,
     1           ' Y3= ',F12.6 )
      END
      SUBROUTINE EXIT(I)
      IF(I.EQ.0)STOP 0
      IF(I.EQ.50)STOP 50
      IF(I.EQ.100)STOP 100
      IF(I.EQ.1)STOP 1
      STOP ' INVALID EXIT CODE '
      END
      subroutine filenamer(igo,idrt1,is1,idrt2,is2,filen,iout)
      dimension length(10),ioff(10)
      character*72 filen
      character*1 num(10),blank
      character *35 header(10), headt
      data length/18,18,34,19,19,35,4*0/
      data ioff/10,10,14,11,11,15,4*0/
      data num/'1','2','3','4','5','6','7','8','9','0'/,blank/' '/
      data header(1)/'intgrd.drt .state                  '/
c                     1234567890123456789012345678901234567890
      data header(2)/'intgrd.drt .state                  '/
      data header(3)/'intgrd.nad.drt .state .drt .state  '/
      data header(4)/'cartgrd.drt .state                  '/
c                     1234567890123456789012345678901234567890
      data header(5)/'cartgrd.drt .state                 '/
      data header(6)/'cartgrd.nad.drt .state .drt .state '/
      do 1 i=1,72
 1    filen(i:i)=blank
      headt=header(igo)
      do 2 i=1,35
 2    filen(i:i)=headt(i:i)
      ifg=ioff(Igo)
      filen(ifg+1:ifg+1)=num(idrt1)
      filen(ifg+8:ifg+8)=num(is1)
      if((igo.eq.3).or.(igo.eq.6))then
      filen(ifg+13:ifg+13)=num(idrt2)
      filen(ifg+20:ifg+20)=num(is2)
      endif
      write(iout,1000)filen
 1000 format(5x,'filename= ',A35)
      return
      end
      SUBROUTINE NUMATM(IUNIT,IP,NATOMS)
      IMPLICIT REAL*8(A-H,O-Z)
      real *8 mass
      character*3 name
      COMMON/message/imessu
      DIMENSION GM(3,100),name(100),char(100),mass(100)
C
      READ(IUNIT,*,END=250)name(1),char(1),(gm(j,1),j=1,3),mass(1)
      natoms=1
      DO 252 I=2,100
      READ(IUNIT,*,END=253)name(i),char(i),(gm(j,i),j=1,3),mass(i)
      NATOMS=I
  252 CONTINUE
  253 CONTINUE
      IF(IP.LE.0)GO TO 260
      WRITE(IP,1004)IUNIT
      WRITE(IP,1005)(J,(GM(I,J),I=1,3),J=1,NATOMS)
      GO TO 260
  250 CONTINUE
      WRITE(IP,1003)
      CALL EXIT(100)
  260 CONTINUE
      RETURN
 1003 FORMAT(5X,'**** NO CURRENT GEOMETRY FILE ****' )
 1004 FORMAT(5X,'**** CURRENT GEOMETRY TAKEN FROM UNIT:',I4 )
 1005 FORMAT(5X,'CURRENT GEOMETRY:',/(2X,I3,2X,3F12.6) )
      END
      SUBROUTINE readcolg(IUNIT,gm,NATOMS,ip,name,mass,char)
      IMPLICIT REAL*8(A-H,O-Z)
      real *8 mass
      character*3 name
      DIMENSION GM(3,100),name(100),char(100),mass(100)
      COMMON/message/imessu
C
      DO 252 I=1,natoms
      READ(IUNIT,*,END=250)name(i),char(i),(gm(j,i),j=1,3),mass(i)
  252 CONTINUE
      return
  250 CONTINUE
      write(ip,1003)
      CALL EXIT(100)
 1003 FORMAT(5X,'**** NO GEOM FILE ****' )
 1004 FORMAT(5X,'**** CURRENT GEOMETRY TAKEN FROM UNIT:',I4 )
 1005 FORMAT(5X,'CURRENT GEOMETRY:',/(2X,I3,2X,3F12.6) )
      END
      SUBROUTINE writcolg(IUNIT,gm,NATOMS,ip,name,mass,char)
      IMPLICIT REAL*8(A-H,O-Z)
      real *8 mass
      character*3 name
      DIMENSION GM(3,100),name(100),char(100),mass(100)
      COMMON/message/imessu
C
      DO 252 I=1,natoms
      write(IUNIT,1003)name(i),char(i),(gm(j,i),j=1,3),mass(i)
  252 CONTINUE
      return
 1003 format(1x,a3,2x,f4.0,1x,3(f13.8,1x),2x,f10.6)
 1004 FORMAT(5X,'**** CURRENT GEOMETRY TAKEN FROM UNIT:',I4 )
 1005 FORMAT(5X,'CURRENT GEOMETRY:',/(2X,I3,2X,3F12.6) )
      END



