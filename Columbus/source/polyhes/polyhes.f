!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      PROGRAM POLYHES
c          columbus version   august 2002
c          3 state version    dec 2002
      IMPLICIT REAL*8 (A-H,O-Z)
      integer ncor
      parameter (ncor=1500000)
      real*8 RS8(ncor)
      integer IRS8(ncor)
      REAL*4 XT0,XU0,XS0,XT1,XU1,XS1,XT,XU,XS
      CHARACTER*(24) FDATE
      DATA IFAIL/0/
C
cmd   CALL GETSIZ(ICORE)
cmd   CALL GETMEM(ICORE,IRS8,IAP,IFAIL)
cmd   IF(IFAIL.NE.0)GO TO 900
C
cmd   CALL SYSTIME(XT0,XU0,XS0)
cmd   WRITE(6,*)' POLYHES CLOCK STARTS: ',FDATE()
cmd   CALL VMAIN(RS8(IAP),RS8(IAP),NCOR)
      CALL VMAIN(RS8,IRS8,NCOR)
cmd   WRITE(6,*)' POLYHES CLOCK ENDS:   ',FDATE()
cmd   CALL SYSTIME(XT1,XU1,XS1)
C
c     XT=XT1-XT0
c     XU=XU1-XU0
c     XS=XS1-XS0
c     WRITE(6,10) XT,XU,XS
c  10 FORMAT(//,' FINAL TIMING RESULTS ',/,
c    1 '  TOTAL TIME (SEC.S)       USER TIME          SYSTEM TIME ',/,
cmd  2 3(3X,F10.3,7X))
      STOP 'NORMAL END OF POLYHES'
  900 continue
      STOP ' MEMORY ALLOCATION FAILURE '
      END
c
c*********************************************
c
      SUBROUTINE VMAIN(A,IA,NCOR)
      IMPLICIT INTEGER (A-Z)
      DIMENSION A(NCOR),IA(NCOR)
C
      nfixdir=0
      open (unit=74,file='geom',form='formatted')
      REWIND 74
      CALL NUMATM(74,0,NATOMS)
      open(75,file='cicgrad',form='unformatted')
      rewind 75
      read(75,end=1)nfixdir
 1    continue
      close(74)
      close(75)
C
      MFIXDIR=nfixdir
      MATOMS=NATOMS
      M3=NATOMS*3
      N3=MATOMS*3
      N31=(2*N3+1)*N3
      N3S=N3*N3
      NPREC=INTOWP(1)
      NDIM=N3+MFIXDIR+5
      nd2=ndim*(ndim+1)/2
      ND1=NDIM*(NDIM+1)
      WRITE(6,1001)MATOMS,N3,N31,N3S,NPREC
      NACME=  1
      MACME=  NACME+M3*NPREC
      LACME=  MACME+M3*NPREC
      origin=1+m3*nprec*(6+mfixdir)
C      SCR=    LACME+N3*NPREC
      disp=origin
      gm=    disp+m3*nprec
      scr=    gm+m3*nprec
      HESSS=  SCR+n31*NPREC
      EGRADH= HESSS+Nd2*NPREC
      GGRADH= EGRADH+N31*NPREC
      XGRADH= GGRADH+N31*NPREC
      HESS=   XGRADH+4*N31*NPREC
c        stores up to 6 gradients and gradients for hessian
      GNgrad= nacme
      GNgradh=egradh

      GRAD=   HESS+ND1*NPREC
      EVAL=   GRAD+NDIM*NPREC
      ROOT=   EVAL+NDIm*NPREC
      VECT=   ROOT+Ndim*NPREC
      RVECT=  VECT+Nd1*NPREC
      BVECT=  RVECT+N3*3*NPREC
      SCR1=   BVECT+Nd1*NPREC
      SCR2=   SCR1+max(n31,Nd1)*NPREC
      TVECT=  SCR2+max(Nd1,n31)*NPREC
      VVECT=  TVECT+max(N3s,nd1)*NPREC
      LAMBDA= VVECT+max(N3s,nd1)*NPREC
      KAMBDA= LAMBDA+(MFIXDIR+5)*2*NPREC
      ICHAR=  KAMBDA+(MFIXDIR+5)
      Idisp1= ICHAR + N3*NPREC*2
      idisp2= idisp1+ n3*nprec*2
      dispold=idisp2+ n3*nprec
      ITOP=   dispold+ N3*NPREC
C
      WRITE(6,1000)NACME,SCR,HESSS,EGRADH,GGRADH,XGRADH,HESS,GRAD,
     1             EVAL,ROOT,VECT,MACME,LACME,RVECT,BVECT,SCR1,SCR2,
     2             TVECT,VVECT,LAMBDA,KAMBDA,ICHAR,Idisp1,idisp2,
     3             gm,disp,gngrad,gngradh,itop
      IF(ITOP.GT.NCOR)STOP 'INSUFFICIENT MEMORY IN POLYHES'
      CALL POLYHSD(IA(NACME),IA(NACME)
     1      ,IA(SCR),IA(HESSS),IA(EGRADH),IA(GGRADH),IA(XGRADH)
     2      ,IA(HESS),IA(GRAD),IA(EVAL),IA(ROOT),IA(VECT)
     3      ,IA(MACME),IA(MACME),IA(LACME),IA(LACME),IA(RVECT)
     4      ,IA(BVECT),IA(SCR1),IA(SCR2),IA(TVECT),IA(VVECT)
     5      ,IA(LAMBDA),IA(KAMBDA),IA(ICHAR),IA(Idisp1),ia(idisp2)
     6      ,N3,M3,n31,MATOMS,MFIXDIR,IA(gngrad),ia(gngradh),
     7      ia(dispold),ia(disp),ia(gm),mfixdir+5)
      RETURN
 1000 FORMAT(/2X,'NACME SCR HESSS EGRADH GGRADH XGRADH HESS GRAD',/8I7,
     1       /2X,'EVAL ROOT VECT MACME LACME RVECT BVECT   SCR1',/8I7,
     2       /2X,'SCR2 TVECT VVECT LAMBDA KAMBDA ICHAR idisp1 idisp2',
     3       /8I7,/2x,'gm  disp gngradh gngrad itop', /5i7)
 1001 FORMAT(/2X,'MEMORY LOCATION PARAMETERS:MATOMS=',I3,' N3=',I5,
     1      ' N31=',I5,' N3S=',I5,' NPREC=',I1)
      END
      SUBROUTINE POLYHSD(NACME,RNACME
     1      ,SCR,HESSS,EGRADH,GGRADH,XGRADH
     2      ,HESS,GRAD,EVAL,ROOT,VECT
     3      ,MACME,RMACME,LACME,RLACME,RVECT
     4      ,BVECT,SCR1,SCR2,sm,sinv,LAMBDA,KLASS
     5      ,CHAR,HDISP,IHDISP
     6      ,N3,M3,n31d,MATOMS,MFIXDIR,drygrad,drygradh
     7      ,dispold,disp,gm,nlam)
      IMPLICIT REAL*8(A-H,O-Z)
      character *10 condition(3)
      character*3 yes,yon,no
      CHARACTER*6 VERB,char(2),label(6)
      CHARACTER*19 KMES(3)
      character*72 filen
      LOGICAL SAME
      INTEGER SIMULT,ACROSS,GMFILE,RESTART,ACCEL,SADDLE,
     1        ATOM(30),STATE1,STATE2,KILLER(30),ORIGIN,
     2        FIXDIR(6,30),KLASS(2),ACTUAL,
     3        statev,state3
C       EQUIVALENT (NACME,RNACME)  (MACME,RMACME)  (LACME,RLACME)
C       equivalent (nacme,drygrad), (drygradh,egradh)
c                           (m3,2)    (n31,1)
      REAL*8 MASS(30),GM(3,2),DISP(n3),DAMPV(90),xgradr(100)
     1      ,SCR(2),HESSS(2),EGRADH(N3,2),GGRADH(N3,2),dipolr(50)
     2      ,HESS(2),GRAD(N3),EVAL(N3),ROOT(N3),VECT(2)
     3      ,NORM,MACME(3,MATOMS),RMACME(N3),LAMBDA(nlam,2)
     4      ,RVECT(2),BVECT(m3,2),SCR1(2),SCR2(2),VALDIR(30)
     5      ,sm(2),sinv(2),NACME(3,MATOMS),RNACME(N3)
     6      ,LACME(3,MATOMS),RLACME(N3),XGRADH(N3,2)
     7      ,HDISP(2),drygrad(m3,6),drygradh(n31d,10),lag(30)
     8      ,dispold(n3)
      real*8 RNL(6),dlx(6),dp(6),dpp(6),GXHD2(6),RAVG1(6)
      DIMENSION GRAD1(3),GRAD2(3),GRAD3(3),GRD(3,3),ipflag(5),
     1      ihdisp(2),idrtv(3),statev(3),ipairs(3,5),criter(5)
      equivalence(idrtv(1),idrt1),(idrtv(2),idrt2),(idrtv(3),idrt3)
      equivalence(statev(1),state1),(statev(2),state2),
     x           (statev(3),state3)
      EQUIVALENCE(GRD(1,1),GRAD1),(GRD(1,2),GRAD2),(GRD(1,3),GRAD3)
      COMMON/message/imessu
      COMMON/DIATAT/NDIAT(30)
      COMMON/CHESS/Norr,nlag,ncgnt,mstate
      common/pscent/maxnew,psc(30,10),dpsc(30,10),ipsc(30,10)
      NAMELIST/NACINT/NCALC,NACOUT,MASS,IDISP
     1,              SIMULT,NEWTON,VALDIR,IDBG
     2,              ZEIG,SCALE,RESTART,ACCEL,IHESS,DAMP
     3,              DAMPV,FIXDIR,CONV,MAXIT,MHESS,ATOM
     4,              KZ,IPFLG,IHESEQ1,TOLROT,ACTUAL
     5,              NSFIG,KSCALE,NORR,LINEQ,dpsc,ipsc,ipflag
      NAMELIST/CROSS/STATE1,STATE2,MOTION,ACROSS,ACTUAL,DERIVC
C         NEWTON-RAPHSON CONVERGENCE CRITERIA
      DATA CONDITION/'CONTINUES','CONVERGED','HALTED'/
      data ipairs/2,2,2, 1,2,3, 1,3,3, 2,3,3, 3,3,2/
      DATA CONV/1.0D-06/,MAXIT/20/,NSFIG/5/,ipflag/5*0/
C
      DATA FIXDIR/180*0/,VALDIR/30*0.D0/
      DATA NCALC/1/,NACOUT/2/,SIMULT/0/,INTC/1/,IORG/2/
      DATA IPFLG/-1/,IDBG/-1/,LINEQ/0/
      DATA GMFILE/1/,RESTART/0/,ACCEL/0/,DAMP/0.0D+0/
      DATA IDISP/0/,ZEIG/5.D-6/,SCALE/1.D0/
     1     ,EIGROT/1.0D-3/,TOLROT/1.0D-3/
      DATA IHESS/0/,SADDLE/0/,ZERO/0.D0/,MHESS/1/
      DATA ICIU1,ICIU2/31,41/
      DATA KZ/3/,IHESEQ1/0/,KSCALE/0/
      DATA KMES/' ENTIRE ','ENTIRE - GEOMETRIC', ' ENERGY PART OF '/
      DATA GRD/9*1/, NPROP/4/,maxlag/2/,yes,no/'yes','no'/
      data criter/5*0.0/
      data label/'e-grad','g(12) ','h(12) ','h(13)','h(23)','g(23)'/
C
C      ITEST  NE 0  NO TAPE10
C      DAMPV        0 < DAMPV(I) < 1  RETAIN DAMPV(I) OF GRADIENT IN
C                   DIRECTION OF ITH EIGENVALUE
C      DAMP         SET ALL ZERO ENTRIES OF DAMPV TO DAMP
C      NEWTON EQ 1  USE HESS AND GRAD TO DETERMINE DISP
C                   FOR NEW GEOM ( ITEST=0 SIMULT=1 NEWTON=1)
C      ACROSS EQ 1  GRADIENT IS (E(J)-E(I))*GRAD(K)
C                   GET E(J) AND E(I) FROM SAXE TAPE12
C      RESTART = 1  NEWTON=1 GET GM AND GRAD FROM PROGRESS  TAPE
C                   TO GENERATE NEXT STEP VIA NEWTON
C                   IHESS=1 GET GM AND IDISP FROM PROGRESS  TAPE
C                   TO GENERATE NEXT STEP FOR HESSIAN CONSTRUCTION
C      ACCEL  EQ 1  ACCELERATE SEARCH USING SCALE
C
C      IHESS  EQ 1  DRIVE HESSIAN CONSTRUCTION
C
C      MHESS  EQ 1  UPPER TRIANGLE DIAGONALIZED (DEFAULT)
C               -1  LOWER TRIANGLE DIAGONALIZED
C                0  AVERAGE OF OFF DIAGONAL DIAGONALIZED
C      LAMBDA(1,*)  LAGRANGE MULTIPLIER USED TO CONSTRUCT RHS WITH
C      LAMBDA(2,*)  USED TO CONSTRAIN INTERNAL COORDINATE
C
C      FIXDIR       FIXDIR(1,*)=I FIXDIR(2,*)=J - R(I,J) FIXED AT VALDIR
C                   FIXDIR(1,*)=I FIXDIR(2,*)=J  FIXDIR(3,*)=K -
C                   ANGLE(I,J,K ) FIXED AT VALDIR(I)
C                   FIXDIR(I,J,K,0,0,L) ANGLE(I,J,K,L)
C                   TO INTRODUCE THIS OPTION WITH THE HESSIAN USER MUST
C                   REGENERATE WORKING HESSIAN USING RESTART
C      IHESEQ1      =1 USE A UNIT HESSIAN ( W PORTION ONLY)
C
C      ACTUAL       =  N  number of LM needed for degeneracy
C      KSCALE       =  0  SCALE ENTIRE GRADIENT
C                   =  1  SCALE GRADIENT WITHOUT GEOMETRIC CONTRAINTS
C                   >  1  SCALE ENERGY PART OF GRADIENT ONLY
C
C                      FILE USAGE
C      I70       PROPERTY UNIT                  70
C      I73       TRANSMOMIN                     73
C                DISPLFL
C      I74       GEOMETRY FILE                  74
C      I75       DISPLACEMENT                   75
C      I76       CONSTRAINTS internal           76
C      I77       CONSTRAINTS cartesian          77
C      I99       unused                         99
C      N32       PROGRESS FILE                  32
C
C      ICNTU     HIJ Continuity                 51
C      IHEsspu   hessian pieces                 52
c      iciu1     b-matrix                       31
c                energy
C      ITAP12                                   12
C      ITAP13                                   13
C      ITAP10    GRADIENTSINTERNAL              10
C               GRADIENTS CARTESIAN             11
C      IMESSU    message unit                   14
C
C         UNITS
      ITAP10=10
      ITAP11=11
      imessu=14
      INP=5
      IOUT=6
       n32=32
       n32h=33
      I70=70
      I73=73
      I74=74
      I75=75
      I76=76
      I77=77
      I99=99
      ICNTU=51
      ihesspu=52
C
      open(imessu,file='message',form='formatted')
      open(n32,file='progress',form='formatted')
      open(n32h,file='progrsh',form='unformatted')
      open(icntu,file='continuity',form='unformatted')
      open(ihesspu,file='h-pieces',form='formatted')


      PI=ACOS(-1.D0)
      NORR=1
C
      WRITE(IOUT,1001)
      open (unit=i74,file='geom',form='formatted')
      REWIND I74
      CALL NUMATM(I74,0,NATOMS)
      ij=0
      DO 2 I=1,NATOMS
      atom(i)=i
      do 2 j=1,3
      ij=ij+1
      DAMPV(Ij)=ZERO
 2    DISP(Ij)=zero
C
      open(iciu1,file='energy.polyhes')
      read(iciu1,*)es1,es2,edif1
      read(iciu1,*)nstate
      read(iciu1,*)(eval(k),k=1,nstate)
      close(iciu1)
c
      mstate=2
      open(i73,file='transmomin',form='formatted')
      REWIND I73
      READ(I73,*,End=920)
      READ(I73,*,End=920)(idrtv(k),statev(k),k=1,2)
      READ(I73,*,end=923)idrtv(3),statev(3),idrtv(3),statev(3)
      mstate=3
 923  continue
      close(i73)
C
c  kount internal coordinates
      call filenamer(1,idrt1,state1,idrt1,state1,filen,iout)
      open(itap10,file=filen)
      ndegfi=0
      do 924 k=1,natoms*3
      read(itap10,*,end=925)x
      ndegfi=ndegfi+1
 924  continue
 925  continue
      close(itap10)
C
      open(inp,file='polyhesin')
      REWIND INP
      READ(INP,NACINT,ERR=921)
C
      if(ihess.ne.0)then
      method=0
      ihdisp(1)=0
      char(1)='skip'
      open(i73,file='../DISPLACEMENT/displfl',form='formatted')
      read(i73,*)mdegfi
      read(i73,*)yon
      read(i73,*)yon
      if(yon.eq.yes)then
      method=1
      char(1)='  '
      endif
      kount=1
      write(iout,1086)
 4    continue
      kount=kount+1
      read(i73,*,end=8)ihdisp(kount),hdisp(kount),char(kount)
      if(hdisp(kount).lt.0)ihdisp(kount)=-ihdisp(kount)
      write(iout,1085)ihdisp(kount),hdisp(kount),char(kount)
      go to 4
  8   continue
      kount=kount-1
      maxdo=kount
      write(iout,1089)kount
      endif
C
      SCLNAC=SCALE
C
      same= idrt1.eq.idrt2
      NCGNT=1
      if(same .and. (state1.eq.state2))then
      mstate=1
      write(iout,1074)statev(1)
C
      else
      if(same)then
      ncgnt=(mstate+2)*(mstate-1)/2
      WRITE(IOUT,1044)mstate,'    ',(statev(k),k=1,mstate)
      endif
      if(.not.same)then
      WRITE(IOUT,1044)mstate,'non',(statev(k),k=1,mstate)
      ncgnt=ncgnt-1
      endif
C
      endif
      ncgnt1=ncgnt+1
      ndo=1
      if(ncgnt.le.1)ndo=0
      if(ncgnt.gt.2)ndo=3
C
      NTDEGF=NATOMS*3
      NDEGF=NATOMS*3-3
C      NDEGFI=NDEGF-3
      WRITE(IOUT,1057)KMES(KSCALE+1)
C
      NROT=  NDEGF-NDEGFI
C
      maxnew=0
      NFIXDIR=0
      DO 3 I=1,30
      IF(FIXDIR(1,I).EQ.0)GO TO 3
      NFIXDIR=I
      do 44 j=1,6
   44 if(fixdir(j,i).gt.natoms)maxnew=max0(fixdir(j,i),maxnew)
    3 CONTINUE
      maxnew=maxnew-natoms
      if(maxnew.gt.0)call mkpscent(natoms,maxnew,psc,dpsc,ipsc,mass,
     x    iout)
C
      lgrad=ndegfi

C
c       constraint gradients
       nfixdir=0
       open(i75,file='cicgrad',form='unformatted')
       rewind i75
       read(i75,end=281)nfixdir,(klass(k),k=1,nfixdir)
       if(nfixdir.eq.0)go to 281
       write(iout,1069)nfixdir,(klass(k),k=1,nfixdir)
  281  CONTINUE

C
      nlag= NFIXDIR+NCGNT
C
      NINTI=    NDEGFI+NFIXDIR+NCGNT
      NINT=     NINTI+NROT
      NDEGFL=   NDEGF+NCGNT
      NDEGFIL=  NDEGFI+NCGNT
C
      NINT2=    NINT*(NINT+1)/2
      NINTI2=   NINTI*(NINTI+1)/2
      NDEGFL2=  NDEGFL*(NDEGFL+1)/2
      NDEGFIL2= NDEGFIL*(NDEGFIL+1)/2
      NDEGFI2=  NDEGFI*(NDEGFI+1)/2
      NDEGF2=   NDEGF*(NDEGF+1)/2
      WRITE(IOUT,1043)NDEGF,NROT,NFIXDIR,NINTI,NINT
c
      if(nlag.gt.0)then
      DO 6 I=1,NLAG
      do 6 j=1,2
    6 LAMBDA(I,j)=ZERO
      endif
      CALL GETLM(N32,LAMBDA,IOUT,RESTART)
c
      if (nfixdir.ne.0) then
       read(i75)(grad(k+ncgnt+lgrad),k=1,nfixdir)
       write(iout,1040)(k,grad(k+ncgnt+lgrad),k=1,nfixdir)
       close(i75)
       open(i75,file='intcgrd',form='formatted')
       write(iout,1070)
       do 280 i=1,nfixdir
       read(i75,1080)(drygrad(k,i+ncgnt1),k=1,lgrad)
       write(iout,1071)i,(drygrad(k,i+ncgnt1),k=1,lgrad)
  280  continue
      endif
      close(I75)
c
      DO 7 I=1,NATOMS
    7 NDIAT(I)=ATOM(I)
C
      MEWTON=NEWTON
C
C  get the b matrix
      open(iciu1,file='bmatrix')
      read(iciu1,*,end=131)((bvect(i,j),j=1,lgrad),i=1,natoms*3)
      go to 133
 131  continue
      do 132 j=1,lgrad
      do 134 i=1,natoms*3
 134  bvect(i,j)=0.d0
 132  bvect(j,j)=1.00
 133  continue
      close(iciu1)
c  build the s matrix
      call  makes(bvect,sm,sinv,natoms*3,lgrad,lgrad+1,scr1,iout)
c
      IF(RESTART.EQ.0)GO TO 130
C           restart section
      IF(NEWTON.EQ.0)GO TO 135
      WRITE(IOUT,1027)N32
      CALL GETPRG(N32,lgrad,GM,RNACME,drygrad(1,2),
     X           Edif1,edif2,ES1,LAMBDA,IOUT,m3)
      criter(1)=edif1
      criter(5)=edif2
      if(ncgnt.eq.0)then
      criter(1)=0.
      else
      do k=1,ncgnt
      LAG(k)=LAMBDA(k,1)
      enddo
      endif
      WRITE(IOUT,1021)(J,(GM(I,J),I=1,3),J=1,NATOMS)
c      CALL GETPROP1(N32,DIPOLR,XGRADR,lgrad,IOUT)
      GO TO 450
  135 CONTINUE
c      CALL GETHPRG(N32,NATOMS,GM,IGO,IOUT)
c      WRITE(IOUT,1034)N32,IGO
c      IF(IGO.LT.MAXDO)then
c      WRITE(IOUT,1011)IGO
c      WRITE(Imessu,1011)IGO
c      call exit2(50)
c      endif
c      CALL GETDDH(N32,lgrad,EGRADH,GGRADH,XGRADH,RNACME,RMACME,
c     1            RLACME,IOUT,METHOD,LAMBDA,NFIXDIR,SAME)
c      do k=1,ncgnt
c      LAG(k)=LAMBDA(NFIXDIR+k,1)
c      enddo
      stop ' not implemented yet '
      GO TO 410
  130 CONTINUE
C
C  Get ci energies
      open(iciu1,file='energy.polyhes')
      read(iciu1,*)es1,es2
      read(iciu1,*)nstate
      read(iciu1,*)(eval(i),i=1,nstate)
      close(iciu1)
      EJMEIA=eval(statev(2))-eval(statev(1))
      edif2= eval(statev(3))-eval(statev(2))
      edif1=ejmeia
      criter(1)=EJMEIA
      criter(5)=edif2
      if(ncgnt.eq.0)
     xWRITE(IOUT,1026)ICIU1,eval(statev(1))
       if(ncgnt.eq.2)
     xWRITE(IOUT,1026)ICIU1,(eval(statev(k)),k=1,2),EJMEIA
      if(ncgnt.eq.5)
     xWRITE(IOUT,1076)ICIU1,(eval(statev(k)),k=1,3),EJMEIA,edif2
      write(iout,1091)(criter(k),k=1,ncgnt)
C
      rewind i74
      call readcolg(i74,gm,NATOMS,iout)
      WRITE(IOUT,1021)(J,(GM(I,J),I=1,3),J=1,NATOMS)
      CALL DIST(NATOMS,GM,IOUT)
      CALL FANGL(NATOMS,GM,IOUT)
      if(natoms.gt.3)call FDANGL(NATOMS,GM,IOUT)
C
C          ENERGY  GRADIENTS
C
      call filenamer(1,idrt1,state1,idrt1,state1,filen,iout)
      open(itap10,file=filen)
      read(itap10,*)(rnacme(k),k=1,ndegfi)
      close(itap10)
      if(ncgnt.gt.0)then
      do 41 mcon=1,ncgnt
      ist1=statev(ipairs(1,mcon))
      ist2=statev(ipairs(2,mcon))
      ihd=ipairs(3,mcon)
      idr1=idrtv(ipairs(1,mcon))
      idr2=idrtv(ipairs(2,mcon))
      call filenamer(ihd,idr1,ist1,idr2,ist2,filen,iout)
      open(itap10,file=filen)
      read(itap10,*)(drygrad(k,mcon+1),k=1,ndegfi)
      close(itap10)
   41 continue
c      for analysis below
      DO 49 I=1,lgrad
  49  SCR(I)=drygrad(I,1)+drygrad(i,2)/2.d0
c
c        energies to  energy differences
      do 48 k=1,ndegfi
      if(ncgnt.gt.2)drygrad(k,6)=drygrad(k,6)-drygrad(k,2)
      drygrad(k,2)=drygrad(k,2)-rnacme(k)
   48 continue
      endif
      if(ndo.gt.0)then
      CALL SGNHIJ(ICNTU,LGRAD,drygrad(1,3),m3,ndo,IOUT)
      call sethij(ICNTU,LGRAD,drygrad(1,3),m3,ndo,iset)
      endif
      IF(IPFLG.GT.2)THEN
      WRITE(IOUT,1092)(label(k),k=1,ncgnt1)
      do 51 k=1,lgrad
  51  write(iout,1083)k,(drygrad(k,l),l=1,nfixdir+ncgnt1)
      endif
C
c       analyze gradients
      write(iout,1093)
      SCRN=SQRT(DOT(SCR,SCR,NATOMS*3))
      do l=1,ncgnt
      RNM=SQRT(DOT(drygrad(1,l+1),drygrad(1,l+1),lgrad))
      do k=1,ncgnt
      dpp(k)=dot(drygrad(1,l+1),drygrad(1,k+1),lgrad)
      RNl(k)=SQRT(DOT(drygrad(1,k+1),drygrad(1,k+1),LGRAD))
      if(k.eq.l)then
      dp(k)=0.d0
      else
      DP(k)=dpp(k)/(RNL(k)*RNM)
      DP(k)=ACOS(DP(k))
      endif
      GXHD2(k)=RNL(k)*RNM/2*SIN(DP(k))
      DP(k)=DP(k)/PI*180.
      RAVG1(k)=DOT(SCR,drygrad(1,k+1),LGRAD)/RNL(k)
      RAVG2=DOT(SCR,drygrad(1,l+1),LGRAD)/RNM
      RAVG1(k)=RAVG1(k)/(RNL(k)*SCRN)
      RAVG2=RAVG2/(SCRN*RNM)
      enddo
      WRITE(IOUT,1060)l,RNM**2,(k,dpp(k),DP(k),k=1,ncgnt)
c     x                ,DLX(k),GXHD2(k),SCRN,
c     x                RAVG2,RAVG1(k)
      enddo
      iovl=0
      do l=1,NCGNT
      RNl(l)=sqrt(DOT(drygrad(1,l+1),drygrad(1,l+1),LGRAD))
         do k=1,NCGNT
         if(k.le.l)then
         iovl=iovl+1
         dpme=dot(drygrad(1,l+1),drygrad(1,k+1),lgrad)
         dpme=dpme/(rnl(k)*rnl(l))
         scr1(iovl)=dpme
         endif
         enddo
      enddo
      iovl=0
      write(iout,1095)
      do l=1,ncgnt
      ibot=iovl+1
      iovl=iovl+l
      write(iout,1096)(scr1(k),k=ibot,iovl)
      enddo
      CALL GIVENS (NCGNT,-NCGNT,NCGNT,scr1,SCR,ROOT,VECT)
      write(iout,1094)(root(k),k=1,NCGNT)
c         end gradient analysis

C          CONSTRUCT HESSIAN
      IF(IHESS.NE.0)then
      SIMULT=1
      CALL ASMHES(N32,n32h,NATOMS,lgrad,ncgnt,m3,n31d,GM,drygrad,
     1            drygradh,DISP,iout,EJMEIA,edif2,IGO,
     2            LAMBDA,NFIXDIR,ES1,SAME,ihdisp,hdisp,char,
     3            maxdo,icntu)
      criter(1)=ejmeia
      criter(5)=edif2
      IF(IGO.LT.MAXDO)then
      WRITE(IOUT,1011)
      WRITE(Imessu,1011)
      call exit2(50)
      endif
      go to 410
      endif
c
C
C
      ICODE=0
C         NEWTON-RAPHSON
      if(ncgnt.gt.0)then
      do i=1,ncgnt
      grad(i+lgrad)=criter(i)
      enddo
      endif
      DO 85 I=1,lgrad
   85 GRAD(I)=RNACME(I)
      gnorm=0.d0
      IF(NFIXDIR.NE.0)THEN
C        CORRECT GRADIENT FOR CONSTRAINTS
      CALL FREEZG(NFIXDIR,lgrad,m3,GRAD,drygrad(1,ncgnt+2),LAMBDA,
     x            IOUT,ncgnt)
      do k=1,nfixdir
      gnorm=gnorm+grad(lgrad+ncgnt+k)**2
      enddo
      ENDIF
      CALL CHKNRP(m3,lgrad,N32,IOUT,CRiter(1),criter(5),
     1           GM,newdir,drygrad,SCALE,LAMBDA,IGO,MAXIT,CONV,
     2           ES1,gnorm,dispold)
      if(ncgnt.gt.0)then
      do k=1,ncgnt
      LAG(k)=LAMBDA(k,1)
      enddo
      endif
      if(igo.eq.-1)then
      WRITE(IOUT,1025)'FAILED'
      WRITE(Imessu,1025)'FAILED'
      CALL EXIT2(100)
      endif
c      if(igo.eq.0)  continue
c      if(igo.eq.1)  converged
c      if(igo.eq.2)  halted
      ICODE=igo
      GO TO 450
C
  410 CONTINUE
      if(ncgnt.gt.0)then
      do k=1,ncgnt
      lag(k)=LAMBDA(k,1)
      enddo
      endif
C         ANALYZE W-MATRIX
C      CALL ANALW(rvect,m3,lgrad,LAMBDA,RMACME,RLACME,EGRADH,GGRADH,
C     1          XGRADH,SCR,SCR1,SCR2,IOUT,N32,CDIF,MHESS,DISP,
C     2          HESS,NROT,NFIXDIR,drygrad(1,4),same)
      call qdif(lgrad,hdisp,ihdisp,char)
      CALL BLDHESA(lgrad,ncgnt,drygradh,HESS,HDISP,ihdisp,
     1             IOUT,LAG,MHESS,SAME,ihesspu)
      if(ncgnt.gt.0)then
      do k=1,ncgnt
      LAMBDA(k,1)=lag(k)
      enddo
      endif
      ICODE=0
      WRITE(IOUT,1025)'STARTS'
      WRITE(Imessu,1025)'STARTS'
C     CALL PUTHES(N32,NATOMS,lgrad,HESS,IOUT,NFIXDIR)
      NEWTON=1
C
C
  450 CONTINUE
      IF(lambda(1,1).EQ.0.D0)THEN
      CALL RESTLAG(lgrad,drygrad(1,1),drygrad(1,2),lambda(1,1)
     x           ,IOUT,ncgnt)
      ENDIF
C
      DO 250 I=1,lgrad
      x=0
      do 249 k=1,ncgnt
  249 x=x+drygrad(i,k+1)*lambda(k,1)
  250 GRAD(I)=RNACME(I)+x
      IF(NFIXDIR.NE.0)THEN
C        CORRECT GRADIENT FOR CONSTRAINTS
      CALL FREEZG(NFIXDIR,lgrad,m3,GRAD,drygrad(1,ncgnt+2)
     x           ,LAMBDA,IOUT,ncgnt)
      ENDIF
      IF(SAME)THEN
      do k=1,ncgnt
      GRAD(Lgrad+k)=criter(k)
      enddo
      ELSE
      GRAD(Lgrad+1)=CRiter(1)
      ENDIF
C
  300 CONTINUE

      WRITE(IOUT,1015)
      DO 332 I=1,NINTI2
  332 HESS(I)=ZERO
C        USE DIVIDED DIFFERENCE HESSIAN FOR R-R BLCOK
C     OR USE SCALED UNIT HESSIAN
      CALL GETHES(ncgnt,ihesspu,lgrad,HESS,lambda(1,1),
     x            IOUT)
      x=0
      lg1=lgrad*(lgrad+1)/2
      do 302 ij=1,lg1
 302  x=x+hess(ij)**2
      IF((DSQRT(X).GT.1.0E-09).AND.(IHESEQ1.NE.1))go to 334
      WRITE(IOUT,1038)
      IJ=0
      DO 303 I=1,lgrad
      do 303 j=1,i
      IJ=IJ+1
C  303 HESS(IJ)=1.D0+DFLOAT(I)*1.0D-04
C  303 HESS(IJ)=sinv((i-1)*lgrad+j)
  303 HESS(IJ)=sm((i-1)*lgrad+j)
  334 continue
C
C   add g and h
       ISTART=LGRAD*(LGRAD+1)/2
       DO 305 L=1,ncgnt
       DO 304 K=1,LGRAD
       ISTART=ISTART+1
 304   HESS(ISTART)=DRYGRAD(K,L+1)
       ISTART=ISTART+L
 305   CONTINUE
C
      IF(NFIXDIR.GT.0)THEN
C        FIX LAGRANGE MULTIPLIER PORTION OF HESSIAN
      call BLDCHES(NFIXDIR,lgrad,m3,hESS,drygrad(1,ncgnt+2),SCR,SCR,
     1    LAMBDA,IOUT,ipflag(1),ncgnt)
      ENDIF
C
C         OVERWRITE SCALE
      REWIND INP
      IF(IERRI.EQ.0)READ(INP,NACINT)
      SCALE=SCLNAC
      IF(ACCEL.EQ.0)SCALE=1.D0
      GNORM=0.D0
      DO 311 I=1,NINTI
  311 GNORM=GNORM+GRAD(I)**2
      GNORM=DSQRT(GNORM)
      WRITE(IOUT,1020)SCALE,GNORM,(GRAD(I),I=1,NINTI)
C         SCALE AND CHANGE SIGN OF GRADIENTS
      DO 307 I=1,NINTI
  307 GRAD(I)=-GRAD(I)
      ihi=lgrad
      IF(KSCALE.EQ.0)ihi=ninti
      IF(KSCALE.EQ.1)then
      DO 309 I=lgrad+1,lgrad+ncgnt
 309  GRAD(i)=GRAD(I)*SCALE
      endif
      DO 308 I=1,ihi
  308 GRAD(I)=GRAD(I)*SCALE
C
      DO 301 I=1,NINTI2
  301 HESSS(I)=HESS(I)
      if(ipflg.gt.1)then
      WRITE(IOUT,1049)' NR ',' HESSIAN '
      CALL PRVOM(HESS,NINTI,2,IOUT)
      WRITE(IOUT,1084)(GRAD(I),I=1,NINTI)
      endif
      CALL GIVENS (NINTI,NINTI,NINTI,HESSS,SCR,ROOT,VECT)
      IHI=0
      WRITE(IOUT,1018)
      DO 322 I=1,NINTI
      LOW=IHI+1
      IHI=IHI+NINTI
  322 WRITE(IOUT,1036)I,ROOT(I),(VECT(J),J=LOW,IHI)
      CALL CHKONB(NINTI,VECT,IOUT)
      DO 321 I=1,NINTI
      IF(DAMPV(I).EQ.0.D0)DAMPV(I)=DAMP
  321 CONTINUE
      WRITE(IOUT,1039)(DAMPV(I),I=1,NINTI)
      CALL SOLNR(NINTI,GRAD,VECT,ROOT,ZEIG,DAMPV,DISP,SCR,IOUT,
     X           SADDLE,NLAG)
      CALL CHKI(NINTI,HESS,GRAD,DISP,SCR,IOUT)
      WRITE(IOUT,1032)(DISP(I),I=1,NINTI)
      dpo=SQRT(dot(dispold,dispold,lgrad))
      dpn=sqrt(dot(disp,disp,lgrad))
      sclb=1.
      if(dpo.gt.1.0d-8)then
      ov=dot(disp,dispold,lgrad)/(dpo*dpn)
        if(ov.lt.0)then
        write(iout,1079)
        sclb=0.5
        endif
      endif
      do 324 i=1,lgrad
      dispold(i)=disp(i)
  324 disp(i)=disp(i)*sclb
C      CALL TBAK(ndegfi,ndegfi,sm,DISP,SCR,0)
      if(nlag.gt.0)then
      DO 323 I=1,NLAG
  323 LAMBDA(I,2)=DISP(lgrad+I)+LAMBDA(I,1)
      WRITE(IOUT,1033)'OLD',(LAMBDA(I,1),I=1,NLAG)
      WRITE(IOUT,1033)'NEW',(LAMBDA(I,2),I=1,NLAG)
      endif
C
      dispn= dpn/sqrt(float(lgrad))
      IF(dispn.GT.CONV)GO TO 371
      write(iout,1055)'displacement'
      ICODE=1
c      WRITE(IOUT,1025)'CONVERGED'
c      WRITE(Imessu,1025)'CONVERGED'
  371 CONTINUE
      OPEN(I75,FILE='displace', FORM='FORMATTED')
      rewind i75
      write(i75,1080)(disp(i),i=1,lgrad)
C      WRITE(IOUT,1072)(DISP(I),I=1,lgrad)
      close(i75)

C
C        THE FOLLOWING CODE IS USED TO HELP DEAL WITH OSCILLATORY BEHAVI
C        OF SAME SYMMETRY MEX
      IF(.NOT.SAME)GO TO 370
      WRITE(IOUT,1056)
      DO 361 I=1,lgrad
      x=0.
      if(ncgnt.gt.0)then
      do 360 k=1,ncgnt
  360 x=x+drygrad(I,k+1)*lambda(k,2)
      endif
  361 grad(i)=rnacme(i)+x
      IF(NFIXDIR.NE.0)THEN
C        CORRECT GRADIENT FOR CONSTRAINTS
      CALL FREEZG(NFIXDIR,lgrad,m3,GRAD,drygrad(1,ncgnt+2),
     x          LAMBDA(1,2),IOUT,ncgnt)
      ENDIF
      IF(IPFLG.GT.2)THEN
      write(iout,1081)(lambda(k,2),k=1,ncgnt)
      write(iout,1092)(label(k),k=1,ncgnt1)
      Do I=1,lgrad
      WRITE(IOUT,1053)I,(drygrad(I,k),k=1,ncgnt+1)
      enddo
      ENDIF
      X=CRTERN**2
      DO 362 I=1,ninti
  362 X=X+GRAD(I)**2
      X=DSQRT(X)
      WRITE(IOUT,1054)X,GNORM,CONV,(GRAD(I),I=1,Ninti)
      IF(X.GT.CONV)GO TO 370
      WRITE(IOUT,1055)'new gradient'
      ICODE=1
      WRITE(IOUT,1025)'CONVERGED'
      WRITE(Imessu,1025)'CONVERGED'
  370 CONTINUE
C
C          PUT GRADIENTS NEW LAGRANGE MULTIPLIER ON PROGRESS FILE
      call WRTPGS(n32,NATOMS,lgrad,GM,EDIF1,edif2,NEWDIR,
     1             drygrad,drygrad(1,2),dispOLD,SCALE,ncgnt,
     2             LAMBDA(1,2),LAMBDA(1,1),gnorm,ES1,NFIXDIR)
      call sethij(ICNTU,LGRAD,drygrad(1,3),m3,ndo,Iset)

C          APPEND DIPOLE DATA
      IF(RESTART.EQ.0)THEN
      call filenamer(4,idrt1,state1,idrt1,state1,filen,iout)
      open(itap10,file=filen)
      read(itap10,*)(rnacme(k),k=1,ntdegf)
      close(itap10)
      do mcon=1,ncgnt
      ist1=statev(ipairs(1,mcon))
      ist2=statev(ipairs(2,mcon))
      ihd=ipairs(3,mcon)
      idr1=idrtv(ipairs(1,mcon))
      idr2=idrtv(ipairs(2,mcon))
      call filenamer(ihd+3,idr1,ist1,idr2,ist2,filen,iout)
      open(itap10,file=filen)
      read(itap10,*)(drygrad(k,mcon+1),k=1,ntdegf)
      close(itap10)
      enddo
      do 348 k=1,ntdegf
      drygrad(k,6)=drygrad(k,6)-drygrad(k,2)
      drygrad(k,2)=drygrad(k,2)-rnacme(k)
  348 continue
C      CALL PUTPROP(I70,IOUT,NPROP,N32,Ntdegf,drygrad,m3)
      ELSE
C      CALL PUTPROP1(N32,DIPOLR,XGRADR,NDEGFt)
      ENDIF
c
      if(ncgnt.eq.2)then
      ioff=3
      write(iout,1087)' cartesian'
      call mkcp(itap10,iciu1,iout,ioff,natoms*3)
      ioff=0
      write(iout,1087)' internal '
      call mkcp(itap10,iciu1,iout,ioff,ndegfi)
      endif
C
      WRITE(IOUT,1025)condition(icode+1)
      WRITE(Imessu,1025)condition(icode+1)
      CALL EXIT2(ICODE)
  920 WRITE(IOUT,*)'cant read transmomin on unit=',I73
      WRITE(Imessu,*)'cant read transmomin on unit=',I73
      CALL EXIT2(100)
  921 WRITE(IOUT,*)'BAD NAMELIST FILE=',INP
      WRITE(Imessu,*)'BAD NAMELIST FILE=',INP
      CALL EXIT2(100)
 1000 FORMAT(5X,'NO NAMELIST INPUT- DEFAULT MASSES USED ' )
 1001 FORMAT(/15X,'SEARCH ALGORITHM FOR MINIMUM ENERGY CROSSING ' )
 1002 FORMAT(/5X,' CURRENT GEOMETRY ',/(1X,I2,1X,3F15.8) )
 1003 FORMAT(/5X,'RAW ',A6,' GRAD',/7X,'X',14X,'Y',14X,'Z'/(1X,3F15.9))
 1010 FORMAT(' POINTERS TO GEOMETRY AND NACMES:',2I6 )
 1011 FORMAT('  HESSIAN CONSTRUCTION MODE')
 1012 FORMAT(' MASS COMBINATION:',5(' M(',I2,')=',F12.6),
     1       (/20X,5(' M(',I2,')=',F12.6) ))
 1015 FORMAT(5X,'DISPLACEMENT VECTOR GENERATED FROM H ! D> = -G> ' )
 1016 FORMAT('  CI-ENERGIES FROM UNIT ',I4,
     1       /5X,' E1=',E20.12,' E2=',E20.12,' E2-E1=',E20.12)
 1017 FORMAT(/5X,'DEL-E SCALED GRADIENTS',/7X,'X',14X,'Y',/(1X,2E16.9) )
 1018 FORMAT(/5X,'EIGENVALUES AND EIGENVECTORS OF HESSIAN' )
 1019 FORMAT(/5X,'CANT READ CI ENERGY FROM HEADER:UNIT=',I3)
 1020 FORMAT(/5X,'SCALE FACTOR FOR GRADIENT',E15.8,5X,' NORM=',E15.8,
     1       /5X,' UNSCALED',' GRADIENTS ',(/1X,3E15.8))
 1021 FORMAT(5X,'CURRENT GEOMETRY:',/(2X,I3,2X,3F12.6) )
 1022 FORMAT(5X,'**** CURRENT GEOMETRY TAKEN FROM UNIT:',I4 )
 1023 FORMAT(5X,'**** NO CURRENT GEOMETRY FILE ****' )
 1024 FORMAT(/5X,'**** NO GRADIENT POINTER ON TAPE=',I2,' ******' )
 1025 FORMAT(5X,'ITERATION STATUS  ',A20)
 1026 FORMAT('  CI-ENERGIES FROM UNITS ',I4,' E1 E2 E2-E1=',3E14.6)
 1027 FORMAT(5X,'RESTART SEARCH FROM PROGRESS FILE=',I3)
 1029 FORMAT(5X,' NO INPUT FOUND ON INPUT UNIT=',I5)
 1030 FORMAT(5X,' CANT READ ',I5,' ABORT' )
 1031 FORMAT(5X,'ATOM MAP=',10I3)
 1032 FORMAT(5X,'SOLUTION IN INTERNAL BASIS:',/,(2X,6F12.6))
 1033 FORMAT(5X,'LAGRANGE MULIPLIER:',A3,'=',30F12.6)
 1034 FORMAT(5X,'RESTART HESSIAN CONSTRUCT FROM PROGRESS FILE='
     1     ,I3,' IDISP=',I3)
 1035 FORMAT(5X,'STATE ENERGIES: ECI=',2E20.12,'  ENUC=',E20.12)
 1036 FORMAT(/,' ROOT=',I3,' EVAL=',E15.8,
     1       /,' EVECT=',6F12.6,/,(7X,6F12.6))
 1037 FORMAT(5X,'NUCLEAR DISPLACEMENTS:',/,(2X,6F12.6))
 1038 FORMAT(5X,'**** UNIT HESSIAN USED ****' )
 1039 FORMAT(5X,' DAMPING VECTOR:',/,(2X,6E12.5))
 1040 FORMAT(5X,' constraint(',i3,') =',E15.8)
 1041 FORMAT(5X,' COORDINATES IN ', I2,' DIMENSIONS SEARCHED')
 1042 FORMAT(5X,'SCHMO FAILURE IN IBASIS-MVIBV',I3)
 1043 FORMAT(5X,'DIMENSIONS:NDEGF=',I3,' NROT=',I3,' NFIX=',I3,' IVAR=',
     1       I3,' IVAR+ROT=',I3)
 1044 FORMAT(/15X,i2,' STATE ENERGY MINIMIZED ',a3,
     1      ' CONICAL INTERSECTION', /23X,' STATES:', 3I5)
 1045 FORMAT(5X,'CONSTRAINT CONDITION ',I2,' R(',I2,',',I2,
     X       ')=     ',F12.6)
 1046 FORMAT(5X,'CONSTRAINT CONDITION ',I2,' ANG(',I2,',',I2,
     X       ',',I2,')=',F12.6)
 1047 FORMAT(5X,'CONSTRAINT CONDITION ',I2,' R(',I2,',',I2,
     X       ')=         R(',I2,',',I2,')')
 1048 FORMAT(5X,'CONSTRAINT CONDITION ',I2,' ANG(',I2,',',I2,
     X       ',',I2,')=ANG(',I2,',',I2,',',I2,')')
 1049 FORMAT(3X,A8,1X,A8)
 1050 FORMAT(5X,'NORM OF RHS: ORIG BASIS ',E15.8,' INT BASIS ',E15.8)
 1051 FORMAT(5X,'GRADIENT: ORIGINAL BASIS',(/5X,5E15.8))
 1052 FORMAT(15X,'GRADIENT DECOMPOSITION')
 1053 FORMAT(3X,I2,(6f12.5))
 1054 FORMAT(/5X,'CURRENT GRADIENT FROM NEW LM - NORM=',
     X       E15.8,' OLD NORM=',E15.8,' CONV=',E15.8,(/5X,6E12.5))
 1055 FORMAT(/5X,'ITERATION STATUS  HALTED -' a15,' CRITERION' )
 1056 FORMAT(/15X,'RE-ANALYZE GRADIENT')
 1057 FORMAT(/15X,'SCALE RHS METHOD: ',A20,' GRADIENT')
 1058 FORMAT(/15X,'CANONICAL COORDINATES',/13X,'r',12X,'R',12X,'GMA',
     X       /3X,'g   ',3F12.6,/3X,'h   ',3F12.6,/3X,'seam',3F12.6)
 1059 FORMAT(/15X,'JACOBI ANALYSIS OF G-H PLANE ')
 1060 FORMAT(/5X,'l=',i3,3x,'|grad|=',f10.5,
     x         /1x,' k     |<g(k)|g(l)>|     ANG ',
     X        /,(i3,5x,f10.5,5x,f8.3))
c     X       /5X,' DEL=  ', F7.3,5X,' gxh/2=',E15.6, /5X,
c     X      ' NORM-AV=', F10.5,
c     X      ' C-AVG-G=',F10.5,' C-AVG-H=',F10.5,/)
 1061  format(3x,'Linear equation matrix:',/(5x,4f10.5,3x,f12.7))
 1062  FORMAT(5X,'CONSTRAINT CONDITION ',I2,' ANG(',I2,',',I2,
     X       ',',I2,',',i2,' )= ',f12.6)
 1069 FORMAT(5X,' KLASS VECTOR:',/' NDIM=',i3,' KLASS=',(10i3))
 1070 FORMAT(5X,' CONSTRAINT gradients')
 1071 FORMAT(1X,i3, 6f10.6,/(4x,6f10.6))
 1072 FORMAT(5X,'SOLUTION IN s-INTERNAL BASIS:',/,(2X,6F12.6))
 1074 FORMAT(/15X,'  ENERGY MINIMIZation STATE:', I5)
 1076 FORMAT('  CI-ENERGIES UNIT=',I4,' E1 E2 E3  E2-E1 E3-E2=',/
     x 3f14.8,2x,2f10.6)
 1079 FORMAT(5X,'DISP OSCILLATING SCALE BY 1/2')
 1080 format(d15.8)
 1081 format(2x,'LagrangeMults:',2x,5(1x,F10.5,1x))
 1083 format(3x,i2,6f12.6,(3x,6f12.6))
 1084 FORMAT(/5X,' -SCALED GRADIENTS ',(/1X,3E15.8))
 1085 format(2x,3(i4, f12.6, 2x,a6))
 1086 FORMAT(5X,'DIVIDED DIFFERENCE PARAEMTERS')
 1087 format(5x,'g-h analysis in ',a10,' coordinates')
 1089 format(5x,i3, ' evaulations to determine hessian ')
 1091 FORMAT(5X,'CONIC CONSTRAINTS:',5F12.6)
 1092 FOrmat(11x,6(2x,a6,4x))
 1093 FORMAT(/15X,' ANALYSIS OF G-H PLANE ')
 1094 FORMAt(5X,'EIGENVALUES OF GH OVERLAP',/(6F10.5))
 1095 format(/5x,'g-h overlap matrix')
 1096 format(5x,(6f10.6))
      END
      subroutine ANALW(rvect,ndegf,m3,LAMBDA,RMACME,RLACME,EGRADH,GGRADH
     1          XGRADH,SCR,SCR1,SCR2,IOUT,N32,CDIF,MHESS,DISP,
     2          HESS,NROT,NFIXDIR,drygrad,SAME)
      IMPLICIT REAL*8(A-H,O-Z)
      real*8 LAMBDA(2)
      LOGICAL SAME
      DIMENSION RVECT(NDEGF,1),EGRADH(NDEGF,1),GGRADH(NDEGF,1),
     1          SCR(2),RMACME(NDEGF),DISP(2),HESS(2),GM(2),
     2          BF(2),SCR1(2),
     3          XGRADH(NDEGF,1),RLACME(NDEGF),drygrad(m3,4)
C
      nrot=0
      WRITE(IOUT,1004)
      NELIM=NFIXDIR+1
      IF(SAME)NELIM=NELIM+1
      RLAG=LAMBDA(NFIXDIR+1)
      RMAG=LAMBDA(NFIXDIR+2)
      CALL GETEGG(N32,NDEGF,EGRADH,GGRADH,XGRADH,IOUT,ICDF)
      call qdif(lgrad,hdisp,ihdisp,char)
      CALL BLDHESA(lgrad,EGRADH,GGRADH,XGRADH,HESS,HDISP,ihdisp,
     1             IOUT,RLAG,RMAG,MHESS,SAME,ihesspu)
      call BLDCHES(NFIXDIR,ndegf,m3,hESS,drygrad,SCR,SCR,rLAMBDA,
     1    IOUT,0,ncgnt)
      LAMBDA(NFIXDIR+1)=RLAG
      LAMBDA(NFIXDIR+2)=RMAG
      DO 10 I=1,NDEGF
   10 RVECT(I,NROT+1)=RMACME(I)
      IF(SAME)THEN
      DO 11 I=1,NDEGF
   11 RVECT(I,NROT+2)=RLACME(I)
      ENDIF
C
      IF(NFIXDIR.NE.0)THEN
      JOFF=1
      IF(SAME)JOFF=2
      DO 30 I=1,NFIXDIR
      DO 35 K=1,NDEGF
   35 RVECT(K,JOFF+I)=drygrad(k,i)
   30 CONTINUE
      ENDIF
C
      CALL RBASIS(NELIM,RVECT,NDEGF,SCR,SCR1,BF,IOUT,1)
      NDEGR=NDEGF-NELIM
      WRITE(IOUT,1002)NDEGF,NDEGR
      CALL RHESS(NDEGF,NDEGR,BF,HESS,SCR,SCR1,IOUT,1,
     1          'W-MATRIX')
      IOFF=NDEGF*NDEGR
      CALL GIVENS (NDEGR,NDEGR,NDEGR,HESS,SCR,BF(IOFF+1),SCR1)
      IHI=0
      WRITE(IOUT,1001)
      DO 20 I=1,NDEGR
      LOW=IHI+1
      IHI=IHI+NDEGR
      WRITE(IOUT,1000)I,BF(I+IOFF),(SCR1(J),J=LOW,IHI)
      CALL TBAK(NDEGF,NDEGR,BF,SCR1(LOW),SCR,1)
      WRITE(IOUT,1003)(SCR(J),J=1,NDEGF)
   20 CONTINUE
      RETURN
 1000 FORMAT(/,' ROOT=',I3,' EVAL=',E15.8,
     1       /,' EVECT=',6F12.6,/,(7X,6F12.6))
 1001 FORMAT(/5X,'EIGENVALUES AND EIGENVECTORS OF W-MATRIX' )
 1002 FORMAT(/15X,'ANALYSIS OF W MATRIX',/5X,'NDEGF=',I3,' NDEGFS=',
     1        I3,/5X,'GRADIENTS IN SEAM SUBSPACE FOLLOW: ',
     2        '1=EGRAD 2=GGRAD 3=XGRAD',/)
 1003 FORMAT(' EV-INT',6F12.6,/,(7X,6F12.6))
 1004 FORMAT(/15X,'CONSTRUCT  W MATRIX',/)
      END
      SUBROUTINE ANGL(GM,II,JJ,KK,ANG,CA,R13,R12,R23)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION GM(3,2)
      DATA ZERO/0.D0/,TWO/2.D0/,PI/3.141592654D0/
C         LAW OF COSINES
      R13=ZERO
      R12=ZERO
      R23=ZERO
      DO 10 K=1,3
      R12=R12+(GM(K,II)-GM(K,JJ))**2
      R23=R23+(GM(K,JJ)-GM(K,KK))**2
      R13=R13+(GM(K,II)-GM(K,KK))**2
   10 CONTINUE
      CA=-(R13-R12-R23)/(TWO*DSQRT(R12*R23))
      ANG=DACOS(CA)*180.D0/PI
      R12=DSQRT(R12)
      R23=DSQRT(R23)
      R13=DSQRT(R13)
      RETURN
      END
      SUBROUTINE ASMHES(IPRU,IPRUH,NATOMS,NDEGF,NCGNT,LNG,LNH,GMP,
     1                  GRADS,GRADSH,DISPP,IOUT,
     2                  EDIFP,EDIF2P,IDISPP,LAMBDAC,NFIXDIR,ES1,
     3                  SAME,IHDISP,HDISP,CHAR,MAXDO,ICNTU)
      IMPLICIT REAL*8(A-H,O-Z)
      LOGICAL SAME
      CHARACTER*6 CHAR,FIX,SKIP
      REAL*8 LAMBDA(30),LAMBDAC(2),LAMBD0(30),zeroa(90)
      DIMENSION GRADS(LNG,6),GRADSH(LNH,6),HESSP(2),DISPP(NDEGF),GMP(2)
      DIMENSION GM(90),HDISP(90),IHDISP(90),CHAR(30)
      COMMON/MESSAGE/IMESSU
      data scale/1.d0/,newdir/1/,istatus/-1/,fix,skip/'fixc','skip'/
      data zeroa/90*0.0/
C
C
      IDISP=0
      IOFF=NDEGF
      REWIND IPRUH
      ndo=0
      if(ncgnt.eq.2)ndo=1
      if(ncgnt.gt.2)ndo=3
C
  100 CONTINUE
      IF(IDISP.EQ.0)THEN
C           REFERENCE CALC
C         PUT H(IJ)S ON  CONTINUITY UNIT
      CALL SETHIJ(ICNTU,NDEGF,GRADs(1,3),LNG,ndo,ISET)
      CALL GETLM(IPRU,LAMBDA0,IOUT,0)
      DO 235 K= 1,NCGNT
  235 LAMBD0(K)=LAMBDA(NFIXDIR+K)
      REWIND IPRUH
      WRITE(IPRUH)ES1,EDIFP,EDIF2P,(LAMBD0(K),K=1,NCGNT),
     X            (GMP(K),K=1,NATOMS*3)
      write(ipruh)idisp
      DO 11 I=1,LNH
      DO 11 J=1,ncgnt+1
  11  GRADSH(I,J)=0.D0
      WRITE(IPRUH)((GRADSH(II,JJ),II=1,LNH),JJ=1,ncgnt+1)
      ENDIF
C
      rewind ipruh
      read(ipruh)
      read(ipruh)idisp
      IDISP=IDISP+1
      READ(IPRUH)((GRADSH(II,JJ),II=1,LNH),JJ=1,Ncgnt+1)
      IF((CHAR(IDISP).ne.FIX).and.(CHAR(IDISP).ne.SKIP))then
      CALL SGNHIJ(ICNTU,ndegf,grads,lng,ndo,IOUT)
      DO 250 K=1,NCGNT+1
      IOFFSET=(IOFF+IHDISP(IDISP))*NDEGF
      DO 20 I=1,NDEGF
      GRADSH(I+IOFFSET,k)=GRADS(I,K)
   20 CONTINUE
  250 CONTINUE
      endif
      REWIND IPRUH
      read(ipruh)
      write(ipruh)idisp
      WRITE(IPRUH)((GRADSH(II,JJ),II=1,LNH),JJ=1,Ncgnt+1)
      IDISPP=IDISP
      IF(IDISP.LT.MAXDO)RETURN
c
      rewind ipruh
      read(IPRUH)ES1,EDIFP,EDIF2P,(LAMBDAC(K+nfixdir),K=1,NCGNT),
     X            (GMP(K),K=1,NATOMS*3)
      JK=0
      IOFFSET=IOFF*NDEGF
      DO 27 K=1,NCGNT+1
      DO 26 J=1,NDEGF
      GRADS(J,K)=GRADSH(J+IOFFset,k)
  26  CONTINUE
  27  continue
      NEWDIR=0
      CALL WRTPGS(IPRU,NATOMS,NDEGF,GM,EDIF,edif2,NEWDIR,
     1  grads,GRADs(1,2),zeroa,SCALE,NCGNT,LAMBDAC,LAMBDAC,
     2  GNORMP,es1,NFIXDIR)
      IDISPP=IDISP
      RETURN
  900 CONTINUE
      WRITE(IOUT,1000)
      WRITE(IMESSU,1000)
      CALL EXIT2(100)
 1000 FORMAT(5X,'CANT FIND DD-HESSIAN DATA - ABORT' )
      END
      Subroutine BLDCHES(NFIXDIR,ndegf,m3,hESS,drygrad,SCR,iSCR,rLAMBDA,
     1    IOUT,iprint,ncgnt)
      IMPLICIT REAL*8(A-H,O-Z)
      LOGICAL LIK,LJK,LKK,LIL,LJL,LKL
      INTEGER THESS,ISCR(2)
      DIMENSION HESS(2),SCR(2),RLAMBDA(2),drygrad(m3,4)
      DATA TWO/2.D0/,ZERO/0.D0/,PI/3.141592654D0/
C
C        STATEMENT FUNCTION GRADIENTS
C
      ndegfc=ndegf+ncgnt
      THESS=NDEGFc*(NDEGFc+1)/2
C
      if(iprint.lt.2)go to 52
      WRITE(IOUT,1027)'WITHOUT','PRIMITIVE',NDEGF
      IHI=0
      DO 51 I=1,NDEGFC
      LOW=IHI+1
      IHI=IHI+I
      WRITE(IOUT,1028)I,(HESS(K),K=LOW,IHI)
   51 CONTINUE
   52 continue
C
C            R-LAMBDA BLOCK
      DO 699 KFIXDIR=1,NFIXDIR
C
      DO 140 I=1,Ndegf
      thess=thess+1
      HESS(THESS)=drygrad(i,kfixdir)
  140 CONTINUE
      DO 141 I=1,KFIXDIR+ncgnt
      thess=thess+1
      HESS(THESS)=ZERO
  141 CONTINUE
  699 CONTINUE
C
C            R-R BLOCK not yet  coded in this version
C
  901 continue
      if(iprint.lt.1)go to 851
      L=NDEGF+NFIXDIR+ncgnt
      WRITE(IOUT,1027)'WITH','ORIGINAL ',L
      IHI=0
      DO 850 I=1,L
      LOW=IHI+1
      IHI=IHI+I
      WRITE(IOUT,1028)I,(HESS(K),K=LOW,IHI)
  850 CONTINUE
  851 continue
C
      RETURN
  999 CONTINUE
      WRITE(IOUT,1024)KOUNT,NDEGF
      STOP ' ERROR BLDCHES '
 1024 FORMAT(5X,'PRIMITIVE NUCLEAR DEGREE OF FREEDOM ERROR:LOCAL=',
     1      I3,' PASSED=',I3)
 1025 FORMAT(2X,I3,(8F12.6),(/5X,8F12.6))
 1026 FORMAT(5X,'R-LAMBDA HESSIAN BLOCK: PRIMITIVE BASIS DIM=',I3 )
 1027 FORMAT(5X,'HESSIAN ',A7,' CONSTRAINTS:',A9,
     1    ' BASIS DIMENSION=',I3)
 1028 FORMAT(5X,I3,7E15.8,(/8X,7E15.8))
      END
      SUBROUTINE BLDHESA(NDIM,ncgnt,lnh,dryGRADH,HESS,hdisp,
     1               ihdisp,icdf,IOUT,LAMBDA,MHESS,SAME,hessu)
      IMPLICIT REAL*8(A-H,O-Z)
      LOGICAL SAME
      REAL*8 LAMBDA(2)
      CHARACTER*8 MOT(3)
C
      DIMENSION dryGRADH(LNh,2),HESS(2),DISP(NDIM),icdf(ndim)
      character*12 label(5)
      character*5 label1(5)
      DATA MOT/' LOWER  ','AVERAGE ',' UPPER  '/
      DATA TWO/2.D0/
      data label/'ENERGY-GRAD','EDIF-GRAD','H(IJ) GRAD',
     x           'H(IK) GRAD','H(JK) GRAD'/
C
      maxdo=2*ndim+1
      do 50 k=1,ncgnt+1
      WRITE(IOUT,1005)label(k)
      ix=0
      DO 50 I=1,MAXDO
      low=ix+1
      ix=ix+ndim
      WRITE(IOUT,1001)I,(dryGRADH(J,k),J=low,ix)
 50   CONTINUE
      IF(LAMBDA(1).EQ.0.D0)CALL RESTLAG(NDIM,dryGRADH(ndim*NDIM+1,1)
     x      ,dryGRADH(NDIM*NDIM+1,2),LAMBDA,IOUT,ncgnt)
c         cdif=,1 forward, 0 centered, -1 backward, 2 = fixed
      write(iout,1002)(i,icdf(i),disp(I),i=1,ndim)
C      write(ihessu,1007)'&hessian'
      do 100 k=1,ncgnt
      call ddhes(ndim,drygradh(1,k),icdf,disp,iout,mhess,hess)
      write(ihessu,1006)label1(k),(hess(l),l=1,ndim*(ndim+1)/2)
 100  continue
c      write(ihessu,1007) '/'
C
      RETURN
 1001 FORMAT(2X,I3,7E15.8,(/5X,7E15.8))
 1002 format('direction method disp',/,(i3,2x,i2,2x,f8.5))
 1003 FORMAT(5X,'I=NDIM+1',' J=',I2,' HESS(',I2,')=',E15.8)
 1004 FORMAT(5X,'*** CENTERED DIFFERENCE USED ***',/)
 1005 FORMAT(/5X,A12)
 1006 format(1x,a6,/(5e13.6))
 1007 format(1x,a10)
      end
      subroutine ddhes(ndim,gradh,icdf,disp,iout,mhess,hess)
      implicit real*8(a-h,o-z)
      dimension gradh(ndim,2),icdf(2),disp(2),hess(2)
      IJ=0
      do 90 i=1,ndim
      do 90 j=1,i
      ij=ij+1
      hess(ij)=0
 90   if(i.eq.j)hess(ij)=1.d0
      ij=0
      DO 100 I=1,NDIM
      if(icdf(i).eq.2)then
      ij=ij+i
      go to 100
      endif
      if(icdf(i).eq.0)then
      ioffh=ndim+i+1
      ioffl=ndim-i+1
      endif
      if(icdf(i).eq.1)then
      ioffh=ndim+i+1
      ioffl=ndim+1
      endif
      if(icdf(i).eq.-1)then
      ioffh=ndim +1
      ioffl=ndim-i+1
      endif
      DO 101 J=1,I
      IJ=IJ+1
      if(icdf(j).eq.2)go to 101
      if(icdf(j).eq.0)then
      joffh=ndim+j+1
      joffl=ndim-j+1
      endif
      if(icdf(j).eq.1)then
      joffh=ndim+j+1
      joffl=ndim+1
      endif
      if(icdf(j).eq.-1)then
      joffh=ndim +1
      joffl=ndim-j+1
      endif

       HESSIJ=GRADH(J,ioffh)-GRADH(J,ioffl)
       HESSIJ=HESSIJ/DISP(I)
       HESSJI=GRADH(I,joffh)-GRADH(I,joffl)
       HESSJI=HESSJI/DISP(J)
      HESS(IJ)=HESSIJ
      IF(MHESS.EQ.-1)HESS(IJ)=HESSJI
      IF(MHESS.EQ.0)HESS(IJ)=(HESSJI+HESSIJ)/2.D0
      WRITE(IOUT,1000)I,J,IJ,HESSIJ,HESSJI
  101 continue
  100 CONTINUE
 1000 FORMAT(5X,'I=',I2,' J=',I2,' HESS(',I2,')=',2E15.8)
      return
      end

      SUBROUTINE CHKI(NDIM,S,RHS,LHS,WMAT,IOUT)
      IMPLICIT REAL*8(A-H,O-Z)
      real*8 LHS
      DIMENSION RHS(NDIM),LHS(NDIM),WMAT(NDIM,NDIM),S(2)
      IX=0
      DO 10 I=1,NDIM
      DO 10 J=1,I
      IX=IX+1
      WMAT(I,J)=S(IX)
      WMAT(J,I)=S(IX)
   10 CONTINUE
      DO 20 I=1,NDIM
      DP=DOT(WMAT(1,I),LHS,NDIM)
      IF(DABS(DP-RHS(I)).LT.1.0D-10)GO TO 20
      WRITE(IOUT,1000)I,DP,RHS(I)
   20 CONTINUE
      RETURN
 1000 FORMAT(5X,'CHKI FAILURE: ROW=',I2,' HX=',E15.8,' RHS=',E15.8)
      END
      SUBROUTINE CHKJ(NDIM,RHS,LHS,WMAT,IOUT)
      IMPLICIT REAL*8(A-H,O-Z)
      real*8 LHS
      DIMENSION RHS(NDIM),LHS(NDIM),WMAT(NDIM,NDIM),S(2)
      DO 20 I=1,NDIM
      dp=0.
      do 21 j=1,ndim
   21 DP=DP+WMAT(i,J)*LHS(J)
      IF(DABS(DP-RHS(I)).LT.1.0D-10)GO TO 20
      WRITE(IOUT,1000)I,DP,RHS(I)
   20 CONTINUE
      RETURN
 1000 FORMAT(5X,'CHKI FAILURE: ROW=',I2,' HX=',E15.8,' RHS=',E15.8)
      END
      SUBROUTINE BCOM(MASS,B)
      IMPLICIT REAL*8(A-H,O-Z)
      REAL*8 MASS(3),B(3,3),MD,MT
      EQUIVALENCE(N1,NDIAT(1)),(N2,NDIAT(2)),(N3,NDIAT(3))
      COMMON/DIATAT/NDIAT(30)
      MD=MASS(N2)+MASS(N3)
C         INVERSE TRANSFORMATION FROM ATOM CENTERED COORDINATES
C         TO INTERNAL COORDINATES RELATIVE TO
C         FRAME AT COM OF TRIATOM
      MT=MD+MASS(N1)
      B(N1,1)=MD/MT
      B(N2,1)=-MASS(N1)/MT
      B(N3,1)=-MASS(N1)/MT
      B(N1,2)=0.D0
      B(N2,2)=-MASS(N3)/MD
      B(N3,2)= MASS(N2)/MD
      B(N1,3)=1.D0
      B(N2,3)=1.D0
      B(N3,3)=1.D0
      RETURN
      END
      SUBROUTINE CHKONB(NDIM,WMAT,IOUT)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION WMAT(NDIM,NDIM)
      DATA ZERO,ONE/0.D0,1.D0/
      DO 10 I=1,NDIM
      TEST=ZERO
      DO 10 J=1,I
      IF(J.EQ.I)TEST=ONE
      DP=DOT(WMAT(1,I),WMAT(1,J),NDIM)
      IF(DABS(TEST-DP).LT.1.0D-08)GO TO 10
      WRITE(IOUT,1000)I,J,DP
   10 CONTINUE
      RETURN
 1000 FORMAT(5X,'CHKONB FAILURE: (',I2,' ! ',I2,')=',F15.8)
      END
      SUBROUTINE CHKNRP(m3,ndegf,IPRU,IOUT,EDIF1c,edif2c,GMC,newdirc,
     1            dryGRADc,SCALEC,LAMBDAC,IPATH,MAXIT,CONV,ES1,gnorm,
     2            dispc)
      IMPLICIT REAL*8(A-H,O-Z)
      REAL*8 LAMBDA(30),LAMBDAC(2),LAMBD0(2),LAMBDAR(30)
      DIMENSION GMC(2),dryGRADC(m3,2),EGRADC(2),disp(90)
      DIMENSION GM(90),GRAD(450),EGRAD(90),dispc(2)
      DATA RHSN/100.D0/,ESTATE/0.0D0/
      COMMON/message/imessu
      common/chess/norr,nlag,ncgntx,mstate
      NAMELIST/PROGRS/GM,EDIF1,edif2,NEWDIR,EGRAD,GRAD,ncgnt,
     1                SCALE,LAMBDA,RHSN,ESTATE1,LAMBDAR,
     2                natoms,nfixdir,disp
C      path = -1 diverge
c      path = 0  continue
c      path = 1  stop converged
c      path = 2  stop maxit
      do i=1,ndegf
       disp(i)=0.0d0
      enddo
      ncgnt=ncgntx
      IPATH=0
      SCALE=SCALEC
      NEWDIR=0
      REWIND IPRU
      READ(IPRU,PROGRS,END=150)
      if(mstate.eq.1)go to 200
      GO TO 175
  150 CONTINUE
      if(mstate.eq.1)go to 200
      CALL RESTLAG(NDEGF,drygradc(1,1),drygradc(1,2),
     x            lambda,IOUT,ncgnt)
      GO TO 201
  175 CONTINUE
C        CRITERIA ARE: (1) ENERGY DIFFERNECE WHICH CAN BE NEGATIVE
C                      (2) ESTATE1
C                      (3) NORM OF RHS
      if(ncgnt.gt.2)then
      IF((DABS(EDIF1c).LT.DABS(EDIF1)).or.(ABS(EDIF2c).LT.DABS(EDIF2))
     x   )GO TO 200
      else
      IF(DABS(EDIF1c).LT.DABS(EDIF1))GO TO 200
      endif
C        MAGNITUDE OF ENERGY DIFFERENCE INCREASED
      IPATH=-1
  200 CONTINUE
      IF(ES1.LT.ESTATE1)IPATH=0
  201 CONTINUE
      SCALEC=SCALE
      GNORM=gnorm+EDIF1c**2
      if(ncgnt.gt.2)gnorm=gnorm+edif2c**2
      if(nlag.gt.0)then
      do k=1,nlag
      LAMBDAC(k)=LAMBDA(k)
      enddo
      endif
      DO 202 I=1,NDEGF
      dispc(i)=disp(i)
      x=drygradc(i,1)
      if(nlag.gt.0)then
      do 203 k=1,nlag
      x=x+LAMBDA(k)*drygradc(i,k+1)
 203  CONTINUE
      endif
 202  GNORM=GNORM+x**2
      GNORM=DSQRT(GNORM)
      IF((IPATH.NE.-1) .OR. (GNORM.LT.RHSN))GO TO 204
      WRITE(IOUT,1004)'DIVERGED',NEWDIR,EDIF1c,EDIF1,GNORM,RHSN,
     X                 ES1,ESTATE1
      if(ncgnt.gt.2)write(iout,1009)edif2c,edif2
      WRITE(IOUT,1007)(LAMBDA(I),I=1,nlag)
      IPATH=-1
      RETURN
 204  CONTINUE
      NEWDIR=NEWDIR+1
      newdirc=newdir
      WRITE(IOUT,1004)'CONTINUE',NEWDIR,EDIF1C,EDIF1,GNORM,RHSN,
     X                 ES1,ESTATE1
      if(ncgnt.gt.2)write(iout,1009)edif2c,edif2
C      REWIND IPRU
c      call WRTPGS(ipru,NATOMS,ndegf,GMc,EDIF1c,edif2c,NEWDIR,
c     1             drygradc,drygradc(1,2),SCALE,ncgnt,
c     2             LAMBDA,LAMBDAR,gnorm,ES1,NFIXDIR)
      WRITE(IOUT,1007)(LAMBDA(I),I=1,nlag)
      IPATH=0
      IF(GNORM.GT.CONV)GO TO 206
      WRITE(IOUT,1003)'   ',NEWDIR,GNORM,EDIF1c,CONV,MAXIT
      IPATH=1
      RETURN
  206 CONTINUE
      IF(NEWDIR.LT.MAXIT)RETURN
      WRITE(IOUT,1003)'NOT',NEWDIR,GNORM,EDIF1c,CONV,MAXIT
      IPATH=2
      RETURN
C
 1000 FORMAT(5X,'NO PROGRESS FILE-INITIALZING FILE=',I3)
 1001 FORMAT(5X,'NO PROGRESS FILE- ABORT' )
 1002 FORMAT(5X,'NO PROGRESS FILE-FOR HESSIAN RETRIVAL' )
 1003 FORMAT(5X,'NEWTON RAPHSON ITERATIONS ',A3,' CONVERGED',
     1          ' AFTER ',I3,' ITERATIONS ',
     2      /15X,'G-NORM=',E15.8,' VALUE=',E15.8,
     3      /15X,'CONV  =',E15.8,' MAXIT=',I3 )
 1004 FORMAT(5X,'NEWTON RAPHSON ITERATIONS ',A8, ': ',/5X,
     1       'ITER=',I4,4X,' EDIF: CURRENT=', E15.8,' LAST=',E15.8,
     2       /15X,'NORM RHS: CURRENT=', E15.8,' LAST=',E15.8,
     3       /15X,'E(1)    : CURRENT=', E15.8,' LAST=',E15.8)
 1005 FORMAT(5X,'NORM OF RHS=',E15.8)
 1007 FORMAT(15X,'CURRENT LAGRANGE MULTIPLIERS:',6F12.6,(/27X,6F12.6))
 1009 FORMAT(15X,'EDIF2    : CURRENT=', E15.8,' LAST=',E15.8)
      END
      SUBROUTINE CHKR(N,A,V,IOUT,NV,TOL,JFAIL)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION A(2),V(N,NV)
C
      DP1=DSQRT(DOT(A,A,N))
      DO 100 I=1,NV
      IFAIL=I
      DP2=DSQRT(DOT(V(1,I),V(1,I),N))
      DP=DOT(A,V(1,I),N)
      WRITE(IOUT,1001)JFAIL,I,DP,DP1,DP2
      IF(DP.GT.TOL)GO TO 900
  100 CONTINUE
      RETURN
  900 CONTINUE
      WRITE(IOUT,1002)
      WRITE(IOUT,1000)JFAIL,IFAIL,TOL,DP,DP1,DP2
      WRITE(IOUT,1003)'ROT ',(V(K,IFAIL),K=1,N)
      WRITE(IOUT,1003)'GRAD',(A(K),K=1,N)
 1000 FORMAT(5X,'ROTATION VECTOR FAILURE:  TEST=',I1,
     1          ' DIREC=',I1,' TOLERANCE=',E15.8,/5X,
     2  ' DP=',E15.8,' NORMG=', E15.8,' NORMR=',E15.8)
 1001 FORMAT(5X,'TEST=',I2,' DIRECTION=',I2,' DP=',E15.8,' NORMG=',
     X       E15.8,' NORMR=',E15.8)
 1002 FORMAT(15X,'BAD ROTATION VECTOR OR GRADIENT:')
 1003 FORMAT(2X,A4,/,(5F12.6))
      CALL EXIT2(100)
      END
      SUBROUTINE CMPRS(NATOMS,HESS,II,IOUT)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION HESS(2)
      N3=NATOMS*3
      WRITE(IOUT,1000)'ORGINAL',N3
      CALL MATOUT(HESS,N3,N3,N3,N3,IOUT)
      IX=0
      IY=0
      DO 10 I=1,N3
      IF((I-1)/3+1.NE.II)GO TO 11
      IY=IY+N3
      GO TO 10
   11 CONTINUE
      DO 20 J=1,N3
      IY=IY+1
      IF((J-1)/3+1.EQ.II)GO TO 20
      IX=IX+1
      HESS(IX)=HESS(IY)
   20 CONTINUE
   10 CONTINUE
      NDO=N3-3
      WRITE(IOUT,1000)'NDEF',NDO
      CALL MATOUT(HESS,NDO,NDO,NDO,NDO,IOUT)
C
      IX=0
      DO 100 I=1,NDO
      DO 100 J=1,I
      IX=IX+1
      IJ=(I-1)*NDO+J
      HESS(IX)=HESS(IJ)
  100 CONTINUE
      RETURN
 1000 FORMAT(/5X,'SQUARE ', A8,' HESSIAN FROM CMPRS: DIMENSION =',I3)
      END
      SUBROUTINE CONCON0(NATOMS,GM,SCR,II,JJ,RLAMBDA,X)
      implicit real*8(a-h,o-z)
      dimension gm(3,10),scr(2)
      common/pscent/maxnew,psc(30,10),dpsc(30,10),ipsc(30,10)
      data two/2.d0/
C
      X = (GM(1,II)-GM(1,JJ))**2 + (GM(2,II)-GM(2,JJ))**2+
     1         (GM(3,II)-GM(3,JJ))**2
      DO 128 K=1,3
      if(ii.le.natoms)then
      iiu=(ii-1)*3
      SCR(IIU+K)=SCR(IIU+K)+RLAMBDA*TWO*(GM(K,II)-GM(K,JJ))
      else
      i=ii-natoms
      do 111 j=1,natoms
      iiu=(j-1)*3
 111  SCR(IIU+K)=SCR(IIU+K)+RLAMBDA*TWO*(GM(K,II)-GM(K,JJ))*psc(j,i)
      endif
C
      if(jj.le.natoms)then
      jju=(jj-1)*3
      SCR(JJU+K)=SCR(JJU+K)-RLAMBDA*TWO*(GM(K,II)-GM(K,JJ))
      else
      i=jj-natoms
      do 112 j=1,natoms
      jju=(j-1)*3
  112 SCR(JJU+K)=SCR(JJU+K)-RLAMBDA*TWO*(GM(K,II)-GM(K,JJ))*psc(j,i)
      endif
  128 CONTINUE
      return
      end
      SUBROUTINE CONCON1(NATOMS,GM,SCR,II,JJ,KK,RLAMBDA)
      implicit real*8(a-h,o-z)
      dimension gm(3,10),scr(2)
      common/pscent/maxnew,psc(30,10),dpsc(30,10),ipsc(30,10)
      data two/2.d0/
C        STATEMENT FUNCTION GRADIENTS
      C1(X12,X23,R12,R23)=-(X23/R23+CA*X12/R12)/R12
      C2(X12,X23,R12,R23)=(X23-X12)/(R12*R23)
     1   +CA*(X12/R12**2-(X23/R23**2))
      C3(X12,X23,R12,R23)=(X12/R12+CA*X23/R23)/R23
C
      CALL ANGL(GM,II,JJ,KK,ANG,CA,R13,R12,R23)
      DO 138 K=1,3
C
      X12=GM(K,II)-GM(K,JJ)
      X23=GM(K,JJ)-GM(K,KK)
      if(ii.le.natoms)then
      iiu=(ii-1)*3
      SCR(IIU+K)=SCR(IIU+K)+RLAMBDA*C1(X12,X23,R12,R23)
      else
      i=ii-natoms
      do 111 j=1,natoms
      iiu=(j-1)*3
 111  SCR(IIU+K)=SCR(IIU+K)+RLAMBDA*C1(X12,X23,R12,R23)*psc(j,i)
      endif
C
      if(kk.le.natoms)then
      KKU=(KK-1)*3
      SCR(KKU+K)=SCR(KKU+K)+RLAMBDA*C3(X12,X23,R12,R23)
      else
      i=kk-natoms
      do 112 j=1,natoms
      kku=(j-1)*3
 112  SCR(KKU+K)=SCR(KKU+K)+RLAMBDA*C3(X12,X23,R12,R23)*psc(j,i)
      endif
C
      if(jj.le.natoms)then
      jju=(jj-1)*3
      SCR(JJU+K)=SCR(JJU+K)+RLAMBDA*C2(X12,X23,R12,R23)
      else
      i=jj-natoms
      do 113 j=1,natoms
      jju=(j-1)*3
 113  SCR(JJU+K)=SCR(JJU+K)+RLAMBDA*C2(X12,X23,R12,R23)*psc(j,i)
      endif
C
  138 CONTINUE
C
      return
      end
      SUBROUTINE CONCON2(NATOMS,GM,SCR,II,JJ,KK,LL,RLAMBDA)
      implicit real*8(a-h,o-z)
      dimension gm(3,10),scr(2)
      common/pscent/maxnew,psc(30,10),dpsc(30,10),ipsc(30,10)
      data two/2.d0/
C

      DO 138 K=1,3
C
      call gdih(gm(1,II),gm(1,JJ),gm(1,kk),gm(1,ll),1,k,gradiik)
      if(ii.le.natoms)then
      iiu=(ii-1)*3
      SCR(IIU+K)=SCR(IIU+K)+RLAMBDA*gradiik
      else
      i=ii-natoms
      do 111 j=1,natoms
      iiu=(j-1)*3
 111  SCR(IIU+K)=SCR(IIU+K)+RLAMBDA*gradiik*psc(j,i)
      endif
C
      CALL GDIH(GM(1,II),GM(1,JJ),GM(1,KK),GM(1,LL),3,K,GRADIIK)
      if(kk.le.natoms)then
      KKU=(KK-1)*3
      SCR(KKU+K)=SCR(KKU+K)+RLAMBDA*gradIIk
      else
      i=kk-natoms
      do 112 j=1,natoms
      kku=(j-1)*3
 112  SCR(KKU+K)=SCR(KKU+K)+RLAMBDA*gradiik*psc(j,i)
      endif
C
      call gdih(gm(1,II),gm(1,JJ),gm(1,kk),gm(1,ll),2,k,gradiik)
      if(jj.le.natoms)then
      jju=(jj-1)*3
      SCR(JJU+K)=SCR(JJU+K)+RLAMBDA*gradiik
      else
      i=jj-natoms
      do 113 j=1,natoms
      jju=(j-1)*3
 113  SCR(JJU+K)=SCR(JJU+K)+RLAMBDA*gradiik*psc(j,i)
      endif
C
      call gdih(gm(1,II),gm(1,JJ),gm(1,kk),gm(1,ll),4,k,gradiik)
      if(ll.le.natoms)then
      llu=(ll-1)*3
      SCR(LLU+K)=SCR(LLU+K)+RLAMBDA*gradiik
      else
      i=ll-natoms
      do 114 l=1,natoms
      llu=(l-1)*3
 114  SCR(LLU+K)=SCR(LLU+L)+RLAMBDA*gradiik*psc(j,i)
      endif
C
  138 CONTINUE
C
      return
      end

      subroutine  gDIH(C1,C2,C3,C4,ii,kk,grad)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION C1(3),C2(3),C3(3),C4(3)
     1          ,RGM(3,4),GM(3,4),RM1(3,3)
      DATA ZERO/0.D0/,ONE/1.D0/,PI/3.141592654D0/
      data h/0.001/
      do 10 i=1,3
      gm(i,1)=c1(i)
      gm(i,2)=c2(i)
      gm(i,3)=c3(i)
      gm(i,4)=c4(i)
 10   continue
      gm(kk,ii)=gm(kk,ii)+h
      call dihed1(gm(1,1),gm(1,2),gm(1,3),gm(1,4),dihp)
      gm(kk,ii)=gm(kk,ii)-h*2.d0
      call dihed1(gm(1,1),gm(1,2),gm(1,3),gm(1,4),dihm)
      grad=(dihp-dihm)/(2*h)
      return
      end

      SUBROUTINE DIHEDx(C1,C2,C3,C4,DIH)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION C1(3),C2(3),C3(3),C4(3)
     1          ,RGM(3,4),GM(3,4),RM1(3,3)
      DATA ZERO/0.D0/,ONE/1.D0/,PI/3.141592654D0/
      R=ZERO
      DO 10 I=1,3
      GM(I,1)=C1(I)-C2(I)
      GM(I,2)=ZERO
      GM(I,3)=C3(I)-C2(I)
      GM(I,4)=C4(I)-C2(I)
      R=R+GM(I,3)**2
   10 CONTINUE
      R=SQRT(R)
      THETA=ACOS(GM(3,3)/R)
      PHI=ATAN(GM(2,3)/GM(1,3))
      if(gm(1,3).lt.0)phi=phi+pi
      DO 11 I=1,2
      RM1(3,I)=ZERO
  11  RM1(I,3)=ZERO
      RM1(3,3)=ONE
      RM1(1,1)=COS(PHI)
      RM1(2,2)=COS(PHI)
      RM1(1,2)=SIN(PHI)
      RM1(2,1)=-SIN(PHI)
      DO 15 I=1,4
   15 CALL EBC(RGM(1,I),RM1,GM(1,I),3,3,1)
      DO 21 I=1,3
      RM1(2,I)=ZERO
   21 RM1(I,2)=ZERO
      RM1(2,2)=ONE
      RM1(1,1)=COS(THETA)
      RM1(3,3)=COS(THETA)
      RM1(1,3)=-SIN(THETA)
      RM1(3,1)= SIN(THETA)
      DO 25 I=1,4
   25 CALL EBC(GM(1,I),RM1,RGM(1,I),3,3,1)
      if(abs(gm(1,3))/r.gt.1.0e-08)go to 900
      if(abs(gm(2,3))/r.gt.1.0e-08)go to 900
      A1=ATAN(GM(2,1)/GM(1,1))
      if(gm(1,1).lt.0)a1=a1+pi
      A4=ATAN(GM(2,4)/GM(1,4))
      if(gm(1,4).lt.0)a4=a4+pi
      DIH=(A4-A1)/PI*180.
      RETURN
  900 continue
      WRITE(6,1000)gm(1,3),gm(2,3),gm(3,3),theta/pi*180.,phi/pi*180.
      DIH=-999
 1000 format('DIHEDRAL ANGLE ALGORITHM ERROR',/5x,5f12.6)
      END
      SUBROUTINE DIHED1(C1,C2,C3,C4,DIH)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION C1(3),C2(3),C3(3),C4(3)
     1          ,RGM(3,4),GM(3,4),RM1(3,3)
      DATA ZERO/0.D0/,ONE/1.D0/,PI/3.141592654D0/
      R=ZERO
      rl1=0
      rl2=0
      dot=0
      DO 10 I=1,3
      GM(I,1)=C1(I)-C2(I)
      GM(I,2)=ZERO
      GM(I,3)=C3(I)-C2(I)
      GM(I,4)=C4(I)-C2(I)
      dot=dot+gm(i,1)*(gm(i,4)-gm(i,3))
      rl1=rl1+gm(i,1)**2
      rl2=rl2+(gm(i,4)-gm(i,3))**2
      R=R+GM(I,3)**2
   10 CONTINUE
      R=SQRT(R)
      THETA=ACOS(GM(3,3)/R)
      PHI=ATAN(GM(2,3)/GM(1,3))
      if(gm(1,3).lt.0)phi=phi+pi
      DO 11 I=1,2
      RM1(3,I)=ZERO
  11  RM1(I,3)=ZERO
      RM1(3,3)=ONE
      RM1(1,1)=COS(PHI)
      RM1(2,2)=COS(PHI)
      RM1(1,2)=SIN(PHI)
      RM1(2,1)=-SIN(PHI)
      DO 15 I=1,4
   15 CALL EBC(RGM(1,I),RM1,GM(1,I),3,3,1)
      DO 21 I=1,3
      RM1(2,I)=ZERO
   21 RM1(I,2)=ZERO
      RM1(2,2)=ONE
      RM1(1,1)=COS(THETA)
      RM1(3,3)=COS(THETA)
      RM1(1,3)=-SIN(THETA)
      RM1(3,1)= SIN(THETA)
      DO 25 I=1,4
   25 CALL EBC(GM(1,I),RM1,RGM(1,I),3,3,1)
      if(abs(gm(1,3))/r.gt.1.0e-08)go to 900
      if(abs(gm(2,3))/r.gt.1.0e-08)go to 900
      do 31 i=3,3
      rl1=rl1-gm(i,1)**2
      rl2=rl2-(gm(i,4)-gm(i,3))**2
      dot=dot-gm(i,1)*(gm(i,4)-gm(i,3))
   31 continue
c  (-) for consistency with other dih method
      dih=-dot/sqrt(rl1*rl2)
      RETURN
  900 continue
      WRITE(6,1000)gm(1,3),gm(2,3),gm(3,3),theta/pi*180.,phi/pi*180.
      DIH=-999
 1000 format('DIHEDRAL ANGLE ALGORITHM ERROR',/5x,5f12.6)
      END
      SUBROUTINE DIHED(C1,C2,C3,C4,DIH)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION C1(3),C2(3),C3(3),C4(3)
     1          ,RGM(3,4),GM(3,4),RM1(3,3)
      DATA ZERO/0.D0/,ONE/1.D0/,PI/3.141592654D0/
      R=ZERO
      rl1=0
      rl2=0
      dot=0
      DO 10 I=1,3
      GM(I,1)=C1(I)-C2(I)
      GM(I,2)=ZERO
      GM(I,3)=C3(I)-C2(I)
      GM(I,4)=C4(I)-C2(I)
      dot=dot+gm(i,1)*(gm(i,4)-gm(i,3))
      rl1=rl1+gm(i,1)**2
      rl2=rl2+(gm(i,4)-gm(i,3))**2
      R=R+GM(I,3)**2
   10 CONTINUE
      R=SQRT(R)
      THETA=ACOS(GM(3,3)/R)
      PHI=ATAN(GM(2,3)/GM(1,3))
      if(gm(1,3).lt.0)phi=phi+pi
      DO 11 I=1,2
      RM1(3,I)=ZERO
  11  RM1(I,3)=ZERO
      RM1(3,3)=ONE
      RM1(1,1)=COS(PHI)
      RM1(2,2)=COS(PHI)
      RM1(1,2)=SIN(PHI)
      RM1(2,1)=-SIN(PHI)
      DO 15 I=1,4
   15 CALL EBC(RGM(1,I),RM1,GM(1,I),3,3,1)
      DO 21 I=1,3
      RM1(2,I)=ZERO
   21 RM1(I,2)=ZERO
      RM1(2,2)=ONE
      RM1(1,1)=COS(THETA)
      RM1(3,3)=COS(THETA)
      RM1(1,3)=-SIN(THETA)
      RM1(3,1)= SIN(THETA)
      DO 25 I=1,4
   25 CALL EBC(GM(1,I),RM1,RGM(1,I),3,3,1)
      if(abs(gm(1,3))/r.gt.1.0e-08)go to 900
      if(abs(gm(2,3))/r.gt.1.0e-08)go to 900
      do 31 i=3,3
      rl1=rl1-gm(i,1)**2
      rl2=rl2-(gm(i,4)-gm(i,3))**2
      dot=dot-gm(i,1)*(gm(i,4)-gm(i,3))
   31 continue
c  (-) for consistency with other dih method
      dih=-dot/sqrt(rl1*rl2)
      dih=180.d0/pi*acos(dih)
      RETURN
  900 continue
      WRITE(6,1000)gm(1,3),gm(2,3),gm(3,3),theta/pi*180.,phi/pi*180.
      DIH=-999
 1000 format('DIHEDRAL ANGLE ALGORITHM ERROR',/5x,5f12.6)
      END

      SUBROUTINE DIST(NATOMS,GM,IOUT)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION GM(3,NATOMS)
      DATA TOA/0.52917715D0/
      DIJ(I,J)=DSQRT((GM(1,I)-GM(1,J))**2+(GM(2,I)-GM(2,J))**2+
     1         (GM(3,I)-GM(3,J))**2)
      WRITE(IOUT,1001)'AU      '
      DO 10 I=2,NATOMS
      WRITE(IOUT,1000)I,(DIJ(I,J),J=1,I-1)
   10 CONTINUE
      WRITE(IOUT,1001)'ANGSTROM'
      DO 20 I=2,NATOMS
      WRITE(IOUT,1000)I,(DIJ(I,J)*TOA,J=1,I-1)
   20 CONTINUE
      RETURN
 1000 FORMAT(2X,I3,2X,6F12.6,/,(7X,6F12.6))
 1001 FORMAT(/5X,'INTERNUCLEAR DISTANCES IN ',A8)
      END
C
      SUBROUTINE FANGL(NATOMS,GM,IOUT)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION GM(3,2)
      WRITE(IOUT,1000)
      DO 10 I=1,NATOMS-2
      DO 11 J=I+1,NATOMS-1
      DO 11 K=J+1,NATOMS
      CALL ANGL(GM,I,J,K,ANG1,W,X,Y,Z)
      CALL ANGL(GM,J,K,I,ANG2,W,X,Y,Z)
      CALL ANGL(GM,K,I,J,ANG3,W,X,Y,Z)
      WRITE(IOUT,1001)I,J,K,ANG1, J,K,I,ANG2, K,I,J,ANG3
   11 CONTINUE
   10 CONTINUE
      RETURN
 1000 FORMAT(/5X,'INTERNUCLEAR ANGLES IN DEGREES',/)
 1001 FORMAT(2X,3(3(I2,1X),F6.1,2X))
      END
      SUBROUTINE FDANGL(NATOMS,GM,IOUT)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION GM(3,2)
      WRITE(IOUT,1000)
      IEVE=0
      DO 10 I=1,NATOMS-3
      DO 11 J=I+1,NATOMS-2
      DO 11 K=J+1,NATOMS-1
      DO 11 L=K+1,NATOMS
      I0=I
      J0=J
      K0=K
      L0=L
      CALL DIHED(GM(1,I),GM(1,J),GM(1,K),GM(1,L),DIH)
      IEVE=IEVE+1
      IF(IAND(IEVE,1).NE.0)THEN
      I1=I
      J1=J
      K1=K
      L1=L
      DIH1=DIH
      GO TO 11
      ENDIF
      WRITE(IOUT,1001)I1,J1,K1,L1,DIH1,I,J,K,L,DIH
   11 CONTINUE
      IF(IAND(IEVE,1).NE.0)WRITE(IOUT,1001)I0,J0,K0,L0,DIH
   10 CONTINUE
      RETURN
 1000 FORMAT(/5X,'INTERNUCLEAR DIHEDRAL IN DEGREES',/)
 1001 FORMAT(2X,3(4(I2,1X),F6.1,2X))
      END
      SUBROUTINE FREEZG(NFIXDIR,lgrad,m3,GRAD,drygrad,RLAMBDA,IOUT,
     x                  ncgnt)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION drygrad(m3,2),grad(2),RLAMBDA(2)
      DATA TWO/2.D0/,ZERO/0.D0/,PI/3.141592654D0/
C
      IF(NFIXDIR.EQ.0)GO TO 900
      nf=lgrad+NCGNT+nfixdir
      WRITE(IOUT,1000)'WITHOUT',NF,(GRAD(I),I=1,NF)
      DO 200 I=1,NFIXDIR
      do 201 j=1,lgrad
 201  grad(j)=grad(j)+drygrad(j,i)*rlambda(i+ncgnt)
 200  continue
C
      WRITE(IOUT,1000)'WITH',NF,(GRAD(I),I=1,NF)
  900 CONTINUE
      RETURN
 1000 FORMAT(5X,'GRADIENT ',A7,' CONSTRAINTS:DIMENSION=',I3,
     X     (/5X,8F12.6))
 1001 FORMAT(/5X,'DISTANCE NORM=',E15.8)
 1026 FORMAT(5X,'ANG(',I2,',',I2,',',I2,')  ='
     X       ,F12.6,' ANG(',I2,',',I2,',',I2,')  =',F12.6)
 1027 FORMAT(5X,'|R(',I2,') - R(',I2,')|=',F12.6,
     X ' |R(',I2,') - R(',I2,')|=',F12.6)
 1028 FORMAT(5X,'|R(',I2,') - R(',I2,')|=',F12.6,
     X ' CONSTRAINT VALUE=',F12.6)
 1029 FORMAT(5X,'ANG(',I2,',',I2,
     X       ',',I2,')  =',F12.6,' CONSTRAINT VALUE=',F12.6)
 1030 FORMAT(5X,'DIH ANG(',I2,',',I2,
     X       ',',I2,',',i2,')  =',F12.6,' CONSTRAINT VALUE=',F12.6)
      END
      SUBROUTINE GAUSS(B,NMIX)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION B(1)
      DATA ZERO/1.0D-12/
      IND(I)=(I-1)*NMIX1

C           GAUSSIAN ELIMINATION
      NMIX1=NMIX+1
C
C           BEGIN THE ELIMINATION
C
      DO 140 I=1,NMIX
      M=IND(I)
      IF(DABS(B(M+I)).LT.ZERO)GO TO 70
      FX=1./B(M+I)
      GO TO 95
  70  CONTINUE
      IF(I.EQ.NMIX)GO TO 1000
      I1=I+1
C           PIVOT SECTION
      DO 90 J=I1,NMIX
      INJ=IND(J)
      IF(DABS(B(INJ+I)).LT.ZERO)GO TO 90
      FX=1./B(INJ+I)
      DO 85 L=I,NMIX1
      TEMP=B(INJ+L)
      B(INJ+L)=B(M+L)
      B(M+L)=TEMP
  85  CONTINUE
      GO TO 95
  90  CONTINUE
      GO TO 1000
  95  CONTINUE
      DO 100 J=I,NMIX1
      B(M+J)=B(M+J)*FX
 100  CONTINUE
C         END PIVOT
      DO 130 J=1,NMIX
      IF(I.EQ.J)GO TO 130
      L=IND(J)
      Y=B(L+I)
      DO 120 K=I,NMIX1
      B(L+K)=B(L+K)-Y*B(M+K)
 120  CONTINUE
 130  CONTINUE
 140  CONTINUE
C
C       MOVE THE SOLUTION THE TO BEGINNING OF CORE
C
      DO 150 I=1,NMIX
      B(I)=B(IND(I)+NMIX1)
 150  CONTINUE
      RETURN
1000  CONTINUE
      WRITE(6,1010)
1010  FORMAT('  ABORT GAUSS  SINGULAR MATRIX ')
      STOP
      END
      SUBROUTINE PUTPROP(IPRPU,IOUTU,NPROP,IPRU,ndegf,drygrad,m3)
     x
      IMPLICIT REAL*8(A-H,O-Z)
      INTEGER FIXDIR
      CHARACTER*8 TITLE
      CHARACTER*4 JHDG
      DIMENSION IHDG(17),JHDG(28),TITLE(2,17),PR(11)
      REAL*8 LAMBDA(30),LAMBDAC(2),LAMBDAR(30),drygrad(m3,4)
      DIMENSION GMC(2),GGRADC(2),HESSC(2),EGRADC(2),XGRADC(2)
      DIMENSION GM(90),GGRAD(90),HESS(465),EGRAD(90),XGRAD(90)
      DIMENSION DIPOLCI(50),HIJ(2),dipolr(2),xgradr(2),disp(90)
      DATA RHSN/100.D0/,ESTATE/0.0D0/
      NAMELIST/PROGRS/GM,EDIF,NEWDIR,EGRAD,GGRAD,XGRAD,disp,
     1                SCALE,HESS,LAMBDA,LAMBDAR,RHSN,ESTATE1
      NAMELIST/PRPRTY/DIPOLCI,XGRAD,egrad,ggrad
C
      DATA TITLE/'POTENTIA','L       ','ELECTIC ','FIELD   ','FIELD GR',
     1'ADIENT  ','DIPOLE M','OMENT   ','QUAD. MO','MENT    ','THIRD MO',
     2'MENT    ','PLANAR D','ENSITY  ','LINE DEN','SITY    ','CHARGE D',
     3'ENSITY  ','OVERLAP ','        ','SECOND M','OMENT   ','DIAMAGNE',
     4'TIC SHLD','KINETIC ','        ','MODEL PO','TENTIAL ','NET POPU',
     5'LATION  ','GROSS PO','PULATION','OVLP POP','ULATION '/
      DATA JHDG/'1/R ','OVLP','DX  ','DXY ','DEN ','X   ','Y   ','Z   ',
     1'XX  ','YY  ','ZZ  ','XY  ','XZ  ','YZ  ','RSQ ','XXX ','YYY ',
     2'ZZZ ','XXY ','XXZ ','XYY ','YYZ ','XZZ ','YZZ ','XYZ ',
     3'    ','A   ','B   '/
      DATA IHDG/1,6,9,6,9,16,3,4,5,2,9,9,26,27,26,26,26/
      DO 10 I=1,50
      IF(I.LE.30)XGRAD(I)=0.D0
   10 DIPOLCI(I)=0.D0
C
      OPEN(IPRPU,FORM='UNFORMATTED')
      REWIND IPRPU
      READ(IPRPU,END=200)NP,IST,JST,OVIJ
      DO 30 N=1,NP
      READ(IPRPU)NC,NT
      IF(NC.EQ.NPROP)GO TO 31
   30 CONTINUE
      GO TO 200
   31 CONTINUE
C
      REWIND IPRPU
      IA=IHDG(NPROP)
      IB=IA+NT-1
      WRITE (IOUTU,400) (TITLE(I,NPROP),I=1,2)
      WRITE (IOUTU,420) (JHDG(I),I=IA,IB)
C
      JB=0
  100 CONTINUE
      READ(IPRPU,END=200)NP,IST,JST,OVIJ
      DO 110 N=1,NP
      READ(IPRPU)NC,NT,NX,(PR(J),J=1,NT)
C
      IF(NC.NE.NPROP)GO TO 110
      JA=JB+1
      JB=JB+NT
      DO 111 I=1,NT
  111 DIPOLCI(I+JA-1)=PR(I)
      WRITE (IOUTU,434)IST,JST,(DIPOLCI(J),J=JA,JB)
  110 CONTINUE
      GO TO 100
  200 CONTINUE
C            REPOSITION PROGRESS FILE
      REWIND IPRU
      READ(IPRU,PROGRS,END=900)
C          RESTORE ORIGINAL PHASE TO HIJ
      DO 11 I=1,NDEGF
      egrad(i)=drygrad(i,1)
      ggrad(i)=drygrad(i,2)
   11 XGRAD(I)=drygrad(I,3)
C
      WRITE(IPRU,PRPRTY)
      RETURN
  900 CONTINUE
      WRITE(IOUTU,1000)
      RETURN
C
      ENTRY GETPROP1(IPRU,DIPOLR,XGRADR,NDEGF,IOUTU)
      REWIND IPRU
      READ(IPRU,PRPRTY,END=901)
      DO 803 I=1,50
  803 DIPOLR(I)=DIPOLCI(I)
      DO 804 I=1,NDEGF
  804 XGRADR(I)=XGRAD(I)
      RETURN
  901 CONTINUE
      WRITE(IOUTU,1001)
      RETURN
C
      ENTRY PUTPROP1(IPRU,DIPOLR,XGRADR,NDEGF)
      REWIND IPRU
      READ(IPRU,PROGRS,END=900)
      DO 801 I=1,50
  801 DIPOLCI(I)=DIPOLR(I)
      DO 802 I=1,NDEGF
  802 XGRAD(I)=XGRADR(I)
      WRITE(IPRU,PRPRTY)
      RETURN
C
  400 FORMAT(20X,2A8)
  420 FORMAT (3X,'STATES',2X,10(8X,A4,2X))
  434 FORMAT (3X,2I3,2X,5F14.8/(11X,5F14.8))
 1000 FORMAT(/5X,' NO PROGRESS FILE PROPERTIES NOT APPENDED')
 1001 FORMAT(/5X,' NO PRPRTY FILE PROPERTIES NOT APPENDED')
      END
      SUBROUTINE GETDDH(IPRU,NDEGF,EGRADP,GGRADP,XGRADP,
     1        EGRADPP,GGRADPP,XGRADPP,IOUT,ICDF,LAMBDAC,NFIXDIR)
      IMPLICIT REAL*8(A-H,O-Z)
      REAL*8 LAMBDAC(2),LAMBD0(2)
      DIMENSION EGRADPP(NDEGF),GGRADPP(NDEGF),EGRADP(NDEGF,2),
     1          GGRADP(NDEGF,2),XGRADP(NDEGF,2),XGRADPP(NDEGF)
      DIMENSION EGRADH(930),GGRADH(930),GM(90),DISP(90),XGRADH(930)
      COMMON/message/imessu
      NAMELIST/PROGRSH/GGRADH,EGRADH,XGRADH,IDISP,GM,EDIF,DISP,
     X                 LAMBD0,EREF
      MAXDO=NDEGF+1
      IF(ICDF.GT.0)MAXDO=NDEGF*2+1
      REWIND IPRU
      READ(IPRU,PROGRSH,END=900)
      IX=0
      DO 125 J=1,MAXDO
      IF(J.LE.NDEGF)THEN
      XGRADPP(J)=XGRADH(J)
      GGRADPP(J)=GGRADH(J)
      EGRADPP(J)=EGRADH(J)
      ENDIF
      DO 125 I=1,NDEGF
      IX=IX+1
      XGRADP(I,J)=XGRADH(IX)
      GGRADP(I,J)=GGRADH(IX)
      EGRADP(I,J)=EGRADH(IX)
  125 CONTINUE
      LAMBDAC(NFIXDIR+1)=LAMBD0(1)
      LAMBDAC(NFIXDIR+2)=LAMBD0(2)
      RETURN
  900 CONTINUE
      WRITE(IOUT,1000)
      CALL EXIT2(100)
 1000 FORMAT(5X,'CANT FIND DD-HESSIAN DATA - ABORT' )
      END
      SUBROUTINE GETEGG(IPRU,NDEGF,EGRADP,GGRADP,XGRADP,IOUT,ICDF)
      IMPLICIT REAL*8(A-H,O-Z)
      REAL*8 LAMBD0(2)
      DIMENSION EGRADP(NDEGF,2),GGRADP(NDEGF,2),XGRADP(NDEGF,2)
      DIMENSION EGRADH(930),XGRADH(930),GGRADH(930),GM(90),DISP(90)
      NAMELIST/PROGRSH/GGRADH,EGRADH,IDISP,XGRADH,
     1                 GM,EDIF,DISP,LAMBD0,EREF
      MAXDO=NDEGF*2+1
      REWIND IPRU
      READ(IPRU,PROGRSH,END=900)
      IX=0
      DO 225 J=1,MAXDO
      DO 225 I=1,NDEGF
      IX=IX+1
      XGRADP(I,J)=XGRADH(IX)
      GGRADP(I,J)=GGRADH(IX)
      EGRADP(I,J)=EGRADH(IX)
  225 CONTINUE
      RETURN
  900 CONTINUE
      WRITE(IOUT,1000)
      CALL EXIT2(100)
 1000 FORMAT(5X,'CANT FIND DD-HESSIAN DATA - ABORT' )
      END
      SUBROUTINE GETGM(IPRU,GMC,IOUTU)
      IMPLICIT REAL*8(A-H,O-Z)
      INTEGER FIXDIR
      REAL*8 LAMBDA(30),LAMBDAR(30)
      DIMENSION GMC(2)
      DIMENSION GM(90),GRAD(900),EGRAD(90),disp(90)
      DATA RHSN/100.D0/,ESTATE/0.0D0/
      NAMELIST/PROGRS/GM,EDIF1,edif2,NEWDIR,EGRAD,GRAD,SCALE,ncgnt,
     1                LAMBDA,RHSN,ESTATE1,LAMBDAR,natoms,nfixdir,
     2                natoms,disp
C
      REWIND IPRU
      READ(IPRU,PROGRS,END=950,err=951)
      DO 105 I=1,3*NATOMS
      GMC(I)=GM(I)
  105 CONTINUE
      RETURN
  950 CONTINUE
      WRITE(IOUT,1001)
      CALL EXIT2(100)
 1001 FORMAT(5X,'NO PROGRESS FILE- ABORT' )
  951 write(iout,1002)
      call exit2(100)
 1002 FORMAT(5X,'PROGRESS error in getgm' )
      END
C
      SUBROUTINE GETIGM(IPRU,NATOMS,ncgnt,GMC,IOUTU)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION GMC(2)
      DATA RHSN/100.D0/,ESTATE/0.0D0/
      COMMON/message/imessu
C
      REWIND IPRU
      read(IPRUH)ES1,EDIFP,EDIF2P,(X,K=1,NCGNT),
     X            (GMc(K),K=1,NATOMS*3)
      RETURN
  950 CONTINUE
      WRITE(IOUT,1001)
      CALL EXIT2(100)
 1001 FORMAT(5X,'NO PROGRESS FILE- ABORT' )
      END
C
      SUBROUTINE GETLM(IPRU,LAMBDAC,IOUT,IRSTRT)
      IMPLICIT REAL*8(A-H,O-Z)
      REAL*8 LAMBDA(30),LAMBDAC(2),LAMBDAR(30)
      DIMENSION GM(90),GRAD(450),EGRAD(90),disp(90)
      COMMON/message/imessu
      COMMON/CHESS/Norr,nlag,ncgntx,mstate
      NAMELIST/PROGRS/GM,EDIF1,NEWDIR,EGRAD,GRAD,SCALE,natoms,
     1              LAMBDA,RHSN,ESTATE1,LAMBDAR,nfixdir,ncgnt,
     2              edif2,disp
      REWIND IPRU
      READ(IPRU,PROGRS,END=950,err=951)
      DO 105 I=1,nlag
      LAMBDAC(I)=LAMBDA(I)
      IF(IRSTRT.EQ.0)GO TO 105
      LAMBDAC(I)=LAMBDAR(I)
  105 CONTINUE
      RETURN
  950 CONTINUE
      RETURN
  951 write(iout,1001)ipru
      call exit2(100)
 1001 FORMAT(5X,'PROGRESS error in getlm unit=',i4)
      END
      SUBROUTINE GETHPRG(IPRU,NATOMS,GMC,IDISPC,IOUT)
      IMPLICIT REAL*8(A-H,O-Z)
      REAL*8 LAMBD0(2)
      DIMENSION GMC(2),GGRADC(2),HESSC(2),EGRADC(2)
      DIMENSION GM(90),GGRAD(90),HESS(465),EGRAD(90)
      DIMENSION GGRADH(930),EGRADH(930),DISP(90),XGRADH(930)
      DATA RHSN/100.D0/,ESTATE/0.0D0/
      COMMON/message/imessu
      NAMELIST/PROGRSH/GGRADH,EGRADH,IDISP,GM,EDIF,DISP,LAMBD0,EREF
     X                ,XGRADH
C
      REWIND IPRU
      READ(IPRU,PROGRSH,END=900)
      DO 110 I=1,3*NATOMS
  110 GMC(I)=GM(I)
      IDISPC=IDISP
      RETURN
  900 CONTINUE
      WRITE(IOUT,1001)
      CALL EXIT2(100)
 1001 FORMAT(5X,'NO PROGRESS FILE- ABORT' )
      END
C
      SUBROUTINE GETPRG(IPRU,NDEGF,GMC,EGRADC,GRADC,
     1                  EDIF1p,edif2p,ES1,LAMBDAC,IOUT,m3)
      IMPLICIT REAL*8(A-H,O-Z)
      INTEGER FIXDIR
      REAL*8 LAMBDA(30),LAMBDAC(2),LAMBDAR(30)
      DIMENSION GMC(2),GRADC(2),EGRADC(2)
      DIMENSION GM(90),EGRAD(90),GRAD(450)
      DATA RHSN/100.D0/,ESTATE/0.0D0/
      COMMON/message/imessu
      COMMON/CHESS/Norr,nlag,ncgntx,mstate
      NAMELIST/PROGRS/GM,EDIF1,edif2,NEWDIR,EGRAD,GRAD,ncgnt,
     1           natoms,SCALE,LAMBDA,LAMBDAR,RHSN,ESTATE1,nfixdir
C
C            RESTART PATH USE OLD LAGRANGE MULTIPLIERS
      ncgnt=ncgntx
      REWIND IPRU
      READ(IPRU,PROGRS,END=900)
      ES1=ESTATE1
      EDIF1p=EDIF1
      EDIF2p=EDIF2
      do 105 i=1,m3*ncgnt
  105 gradc(i)=GRAD(I)
      DO 106 I=1,NDEGF
  106 EGRADC(I)=EGRAD(I)
      DO 107 I=1,3*NATOMS
  107 GMC(I)=GM(I)
      DO 108 I=1,nlag
  108 LAMBDAC(I)=LAMBDAR(I)
      RETURN
  900 CONTINUE
      WRITE(IOUT,1001)
      CALL EXIT2(100)
 1001 FORMAT(5X,'NO PROGRESS FILE- ABORT' )
      END
C
      SUBROUTINE GETHES(ncgnt,IPRU,NDEGF,HESSC,lambda1,IOUT)
      IMPLICIT REAL*8(A-H,O-Z)
      REAL*8 lambda1(ncgnt)
      character*6 label
      DIMENSION HESSC(2)
      DIMENSION HESS(1000)
      DATA RHSN/100.D0/,ESTATE/0.0D0/
      COMMON/message/imessu
C
      REWIND IPRU
      NDEGF2=NDEGF*(NDEGF+1)/2
      read(ipru,1010,end=950)label
      read(ipru,1011)(hessc(k),k=1,ndegf2)
      do108 l=1,ncgnt
      read(ipru,1010)label
      read(ipru,1011)(hess(k),k=1,ndegf2)
      DO 108 IJ=1,NDEGF2
      HESSC(IJ)=HESSC(IJ)+lambda1(k)*hess(ij)
  108 CONTINUE
      RETURN
  950 CONTINUE
       write(iout,1000)
       return
 1000  format(10x,'USE unit hessian')
 1010 format(1x,a6)
 1011 format(5e13.6)
      END
      SUBROUTINE JNTMO(NATOMS,GM,VECT,XDSP)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION GM(2),VECT(2)
      DO 10 I=1,NATOMS*3
      GM(I)=GM(I)+XDSP*VECT(I)
   10 CONTINUE
      RETURN
      END
      subroutine mkcp(ipru,iciu1,iout,ioff,nat3)
      implicit real*8(a-h,o-z)
      dimension gvs2(90),gv(90),hv(90),sv(90),gvn(90),hvn(90)
      pi=acos(-1.d0)
      call gtfgh(IPRU,iciu1,IOUT,GVs2,GV,HV,SV,EREF,nat3,ioff)
      call mkRGH(SV,GVS2,HV,GVN,HVN,NAT3,beta)
      DPGHN=DOT(GVN,HVN,NAT3)
      GVND=SQRT(DOT(GVN,GVN,NAT3))
      HVND=SQRT(DOT(HVN,HVN,NAT3))
      HXR=DOT(HVN,GVN,NAT3)/GVND
      HYR=DOT(HVN,HVN,NAT3)/HVND
      GXR=DOT(GVN,GVN,NAT3)/GVND
      GYR=DOT(GVN,HVN,NAT3)/HVND
      SXR=DOT(SV,GVN,NAT3)/GVND
      SYR=DOT(SV,HVN,NAT3)/HVND
      WRITE(iout,1044)BETA/PI*180.,DPGHN,GVND,HVND, GVND*HVND
      WRITE(iout,1014)'ROTD',SXR,SYR,0.,GXR,GYR,HXR,HYR
      WRITE(iout,1027)'HR',(HVN(J),J=1,NAT3)
      WRITE(iout,1027)'GR',(GVN(J),J=1,NAT3)
 1014 FORMAT(2X,A6,' SX=',F7.4,' SY=',F7.4,' SZ=',F7.4,
     X    /8X, ' GX=',F9.6,' GY=',F9.6,' HX=',F9.6,' HY=',F9.6)
 1027 FORMAT(/3X,A3,'=',3(F15.8,','),/,(6X,3(F15.8,',')))
 1044 FORMAT(/15X,'ROTATION TO ON  G AND H:  BETA=',F12.5,
     X    /5X, ' <G|H> NEW =',E12.5,
     X    /5X, ' |G|/2 NEW =',E10.5, 5X,  ' |H|   NEW =',E10.5,
     X    /5X, ' GXH/2 NEW =',E10.5)
      return
      end
      SUBROUTINE gtfgh(IPRU,iciu1,IOUT,EGV,GV,HV,SV,EREF,len,ioff)
      IMPLICIT REAL*8(A-H,O-Z)
      character*72 filen
      integer state1, state2
      REAL*8 LAMBDA(30),LAMBD0(2),LAMBDAR(30),DIPOLCIP(2)
      DIMENSION EGV(90),GV(90),HV(90),GMP(90),DIPOLCI(50)
      DIMENSION GM(90),rnacme(90),rmacme(90),rlacme(90),
     1          SV(90)
      COMMON/message/imessu
C
      open(iciu1,file='energy.polyhes')
      rewind iciu1
      read(iciu1,*)es1,es2
      close(iciu1)
      open(ipru,file='transmomin',form='formatted')
      REWIND IPRU
      READ(IPRU,*,end=920)
      READ(IPRU,*,end=920)idrt1,state1,idrt2,state2
      close(ipru)
  102 CONTINUE
      EREF=Es1
C      EGV(IPUT)=EGRAD(IPT)
C      GV(IPUT)=GGRAD(IPT)
C      HV(IPUT)=XGRAD(IPT)
C      SV(IPUT)=EGV(IPUT)+GV(IPUT)/2
C      egv(i)=gv(i)/2.
      call filenamer(ioff+1,idrt1,state1,idrt1,state1,filen,iout)
      open(ipru,file=filen)
      read(ipru,*)(rnacme(k),k=1,len)
      close(ipru)
      call filenamer(ioff+2,idrt2,state2,idrt2,state2,filen,iout)
      open(ipru,file=filen)
      read(ipru,*)(rmacme(k),k=1,len)
      close(ipru)
      call filenamer(ioff+3,idrt1,state1,idrt2,state2,filen,iout)
      open(ipru,file=filen)
      read(ipru,*)(rlacme(k),k=1,len)
      close(ipru)
C
      do 48 k=1,len
      sv(k)=(rmacme(k)+rnacme(k))/2.
      gv(k)=rmacme(k)-rnacme(k)
      egv(k)=gv(k)/2.
      hv(k)=rlacme(k)
   48 continue
C
C      WRITE(IOUT,1001)
      RETURN
 920  CONTINUE
      WRITE(IOUT,1000)
 1000 FORMAT(5X,'transmom end of file encountered')
 1001 FORMAT(5X,'**** EGV GV AND HV FROM PROGRESS FILE ****')
      RETURN
      END
      SUBROUTINE mkrGH(SV,GVS2,HV,GVN,HVN,NAT3,beta0)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION GVS2(2),HV(2),GVN(2),HVN(2),sv(2),scr(100)
      do 10 i=1,nat3
      scr(i)=gvs2(i)
   10 scr(i+nat3)=hv(i)
      CALL SCHMOM(nat3,2,SCR,6,IFAIL)
      HX=DOT(SCR(1),HV,NAT3)
      HY=DOT(SCR(NAT3+1),HV,NAT3)
      Gx=DOT(SCR(1),GVS2,NAT3)
      GY=DOT(SCR(NAT3+1),GVS2,NAT3)
      SX=DOT(SCR(1),SV,NAT3)
      SY=DOT(SCR(NAT3+1),SV,NAT3)
C
      DENOB=DOT(HV,HV,NAT3)-DOT(GVS2,GVS2,NAT3)
      BETA0=0.25*ATAN(2.*DOT(GVS2,HV,NAT3)/DENOB)
      CALL RBB(NAT3,BETA0,GVS2,HV,GVN,HVN)
 200  continue
      return
      end
      subroutine mkpscent(natoms,maxnew,psc,dpsc,ipsc,mass,iout)
      IMPLICIT REAL*8(A-H,O-Z)
      dimension psc(30,2),dpsc(30,2),ipsc(30,2)
      real*8 mass(2),masst
      write(iout,1001)
      do 20 i=1,maxnew
      masst=0
      do 30 j=1,natoms
   30 psc(j,i)=0.d0
      do 31 j=1,natoms
      jj=ipsc(j,i)
      if(jj.eq.0)go to 31
      if(jj.gt.0)then
      cf=dpsc(j,i)
      else
      cf=mass(iabs(jj))
      masst=masst+cf
      endif
      psc(iabs(jj),i)=cf
   31 continue
      if(masst.lt.0.01)masst=1
      do 32 j=1,natoms
   32 psc(j,i)=psc(j,i)/masst
      write(iout,1000)i,(j,psc(j,i),j=1,natoms)
   20 continue
      return
 1000 format(2x,i2,6(1x,i2,f9.5))
 1001 format(/15x,'PSEUDO CENTERS')
      end

      SUBROUTINE NUMATM(IUNIT,IP,NATOMS)
      IMPLICIT REAL*8(A-H,O-Z)
      real *8 mass
      character*3 name
      COMMON/message/imessu
      DIMENSION GM(3,100),name(100),char(100),mass(100)
C
      READ(IUNIT,*,END=250)name(1),char(1),(gm(j,1),j=1,3),mass(1)
      natoms=1
      DO 252 I=2,100
      READ(IUNIT,*,END=253)name(i),char(i),(gm(j,i),j=1,3),mass(i)
      NATOMS=I
  252 CONTINUE
  253 CONTINUE
      IF(IP.LE.0)GO TO 260
      WRITE(IP,1004)IUNIT
      WRITE(IP,1005)(J,(GM(I,J),I=1,3),J=1,NATOMS)
      GO TO 260
  250 CONTINUE
      WRITE(IP,1003)
      CALL EXIT2(100)
  260 CONTINUE
      RETURN
 1003 FORMAT(5X,'**** NO CURRENT GEOMETRY FILE ****' )
 1004 FORMAT(5X,'**** CURRENT GEOMETRY TAKEN FROM UNIT:',I4 )
 1005 FORMAT(5X,'CURRENT GEOMETRY:',/(2X,I3,2X,3F12.6) )
      END
      SUBROUTINE readcolg(IUNIT,gm,NATOMS,ip)
      IMPLICIT REAL*8(A-H,O-Z)
      real *8 mass
      character*3 name
      DIMENSION GM(3,100),name(100),char(100),mass(100)
      COMMON/message/imessu
C
      DO 252 I=1,natoms
      READ(IUNIT,*,END=250)name(i),char(i),(gm(j,i),j=1,3),mass(i)
  252 CONTINUE
      return
  250 CONTINUE
      write(ip,1003)
      CALL EXIT2(100)
 1003 FORMAT(5X,'**** NO GEOM FILE ****' )
 1004 FORMAT(5X,'**** CURRENT GEOMETRY TAKEN FROM UNIT:',I4 )
 1005 FORMAT(5X,'CURRENT GEOMETRY:',/(2X,I3,2X,3F12.6) )
      END
C
      SUBROUTINE RBB(N,BETA,A,B,AN,BN)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION A(N),B(N),AN(N),BN(N)
      C2B=COS(2*BETA)
      S2B=SIN(2*BETA)
      DO 10 I=1,N
      AN(I)=C2B*A(I)-S2B*B(I)
 10   BN(I)=S2B*A(I)+C2B*B(I)
      RETURN
      END
      SUBROUTINE RBASIS(NR,R,N,SCR,ISCR,BF,IOUT,IPFLG)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION R(N,NR),SCR(2),BF(2),ISCR(2,2)
C         ISCR(1,*) LIST OF AVAILABLE BF
C         ISCR(2,*) CURRENT BF ( NR BF NOT COUNTED )
      DO 2 I=1,N
    2 ISCR(1,I)=I
      LOOP=0
  500 CONTINUE
      NDO=N**2
      DO 5 I=1,NDO
    5 SCR(I)=0.D0
      IJ=0
      IF(NR.EQ.0)GO TO 11
      DO 10 I=1,NR
      DO 10 J=1,N
      IJ=IJ+1
      SCR(IJ)=R(J,I)
   10 CONTINUE
   11 CONTINUE
      IOFF=NR*N
      KOFF=0
      DO 40 I=1,N
      IF(ISCR(1,I).LE.0)GO TO 40
      SCR(ISCR(1,I)+IOFF)=1.D0
      KOFF=KOFF+1
      ISCR(2,KOFF)=I
      IOFF=IOFF+N
      IF(KOFF.EQ.(N-NR))GO TO 41
   40 CONTINUE
   41 CONTINUE
      NDO=N-NR
C
      IF(IPFLG.LE.0)GO TO 46
      IHI=0
      WRITE(IOUT,1002)
      DO 45 I=1,N
      LOW=IHI+1
      IHI=IHI+N
      WRITE(IOUT,1000)I,(SCR(K),K=LOW,IHI)
   45 CONTINUE
   46 CONTINUE
C
      CALL SCHMO(N,SCR,IOUT,IFAIL)
      IF(IFAIL.EQ.0)GO TO 47
      IF(IFAIL.LE.NR)GO TO 900
      IPUT=IFAIL-NR
      ISCR(1,ISCR(2,IPUT))=-1
      LOOP=LOOP+1
      WRITE(IOUT,1004)LOOP,ISCR(2,IPUT)
      IF(LOOP.GT.NR)GO TO 900
      GO TO 500
   47 CONTINUE
      IX=0
      IY=NR*N
      DO 50 I=1,NDO
      DO 50 J=1,N
      IX=IX+1
      IY=IY+1
   50 BF(IX)=SCR(IY)
C
      IHI=0
      WRITE(IOUT,1001)
      DO 60 I=1,N-NR
      LOW=IHI+1
      IHI=IHI+N
      WRITE(IOUT,1000)I,(BF(K),K=LOW,IHI)
   60 CONTINUE
      RETURN
 900  CONTINUE
      WRITE(IOUT,1003)IFAILED
      STOP ' SCHMO FAILURE IN RBASIS '
 1000 FORMAT(2X,I3,(8F10.6))
 1001 FORMAT(' ON - BASIS FOR REDUCED W-MATRIX ')
 1002 FORMAT(' ORIGINAL - BASIS FOR W-MATRIX ')
 1003 FORMAT(5X,'SCHMO FAILURE IN RBASIS:BASIS FUNCTION=',I3)
 1004 FORMAT(5X,'SCHMO FAILURE:LOOP=',I3,' BF=',I3)
      END
      SUBROUTINE RHESS(NL,NS,TRAN,H,SCR,SCR1,IOUT,IPFLG,NAME)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION TRAN(NL,NS),H(2),SCR(2),SCR1(2)
      CHARACTER*8 NAME
      COMMON/message/imessu
C
      IX=0
      DO 10 I=1,NL
      DO 10 J=1,I
      IX=IX+1
      IJ=(I-1)*NL+J
      SCR(IJ)=H(IX)
      IJ=(J-1)*NL+I
      SCR(IJ)=H(IX)
   10 CONTINUE
      IF(IPFLG.GT.0)THEN
      WRITE(IOUT,1000)'ORIGINAL',NAME
      CALL PRVOM(H,NL,2,IOUT)
      ENDIF
C
      CALL EBC(SCR1,SCR,TRAN,NL,NL,NS)
      CALL EBTC(SCR,TRAN,SCR1,NS,NL,NS)
C
      IX=0
      DO 20 I=1,NS
      DO 20 J=1,I
      IX=IX+1
      IJ=(I-1)*NS+J
      H(IX)=SCR(IJ)
      JI=(J-1)*NS+I
      IF(DABS(SCR(JI)-SCR(IJ)).GT.1.0D-08)GO TO 900
   20 CONTINUE
C
      IF(IPFLG.GT.0)THEN
      WRITE(IOUT,1000)'REDUCED',NAME
      CALL PRVOM(H,NS,2,IOUT)
      ENDIF
C
      RETURN
  900 CONTINUE
      STOP ' ERROR IN RHESS '
 1000 FORMAT(3X,A8,1X,A8)
      END
      SUBROUTINE RESTLAG(N,A,B,RLAG,IOUT,ncgnt)
      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION A(N),B(N),RLAG(NCGNT)
      AB=0.D0
      BB=0.D0
      DO 10 I=1,N
      AB=AB+A(I)*B(I)
      BB=BB+B(I)*B(I)
   10 CONTINUE
      RLAG(1)=-AB/BB
      do k=2,ncgnt
      rlag(k)=0.d0
      enddo
      WRITE(IOUT,1000)RLAG(1)
      RETURN
 1000 FORMAT(5X,'LAGRANG MULT=',E15.8,' FROM RESTLAG')
      END
      SUBROUTINE RDT30(N30,E,NSTATE,IOUTU,IERR)
      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION E(2)
      IERR=0
      REWIND N30
      READ(N30,END=60)N,NSTATE,E0
   10 READ(N30,END=20)IT,IUNIT,(E(I),I=1,NSTATE)
      GO TO 10
   20 CONTINUE
C      DO 21 I=1,NSTATE
C   21 E(I)=E(I)+E0
      WRITE(IOUTU,1001)(E(I),I=1,NSTATE)
      RETURN
   60 CONTINUE
      WRITE(IOUTU,1000)N30
      IERR=1
      RETURN
 1000 FORMAT(5X,'UNIT ',I3,' EMPTY ')
 1001 FORMAT(5X,'CI ENERGIES FROM ALCHEMY TAPE',(/2X,5E20.13))
      END
      SUBROUTINE rptcon(NFIXDIR,FIXDIR,VALDIR,NDEGF,NATOMS,
     1                  GM,KILLER,KLASS,IOUT)
      IMPLICIT REAL*8(A-H,O-Z)
      INTEGER FIXDIR(6,2),KLASS(2)
      DIMENSION KILLER(2),VALDIR(2)
      DIMENSION GM(3,2)
      common/pscent/maxnew,psc(30,10),dpsc(30,10),ipsc(30,10)
      DATA TWO/2.D0/,ZERO/0.D0/,PI/3.141592654D0/
      if(maxnew.eq.0)go to 99
      do 40 i=1,maxnew
      do 41 k=1,3
   41 gm(k,natoms+i)=0
      do 42 k=1,3
      do 42 j=1,natoms
   42 gm(k,natoms+i)=gm(k,natoms+i)+gm(k,j)*psc(j,i)
   40 continue
   99 continue
C
      IF(NFIXDIR.EQ.0)GO TO 900
      NF=NDEGF+NFIXDIR
      N3=NATOMS*3
C
      MYCLASS=0
      ZNORM=ZERO
      DO 200 I=1,NFIXDIR
      II=FIXDIR(1,I)
      JJ=FIXDIR(2,I)
      GO TO (400,600,400,600,700),KLASS(I)
      STOP ' ILLEGAL KLASS '
  400 CONTINUE
C         DISTANCE CONTRAINT
      MYCLASS=1
      X = (GM(1,II)-GM(1,JJ))**2 + (GM(2,II)-GM(2,JJ))**2+
     1         (GM(3,II)-GM(3,JJ))**2
      IF(KLASS(I).EQ.3)GO TO 150
      WRITE(IOUT,1028)II,JJ,DSQRT(X),VALDIR(I)
      ZNORM=ZNORM+(X-VALDIR(I)**2)**2
      GO TO 200
  150 CONTINUE
C         DISTANCE DIFFERENCE CONSTRAINT
      kk=FIXDIR(3,I)
      LL=FIXDIR(4,I)
      y = (GM(1,kk)-GM(1,LL))**2 + (GM(2,kk)-GM(2,LL))**2+
     1         (GM(3,kk)-GM(3,LL))**2
      WRITE(IOUT,1027)II,JJ,DSQRT(X),KK,LL,DSQRT(Y)
      GO TO 200
  700 CONTINUE
C         dihedral ANGLE CONSTRAINT
      KK=FIXDIR(3,I)
      LL=FIXDIR(6,I)
      KKU=(KK-1)*3
      LLU=(LL-1)*3
      CALL DIHED1(GM(1,II),GM(1,JJ),GM(1,KK),GM(1,LL),DIH1)
      WRITE(IOUT,1030)II,JJ,KK,LL,acos(DIH1)*180.d0/pi,VALDIR(I)
      XX=VALDIR(I)*PI/180.D0
      GO TO 200
  600 CONTINUE
C         ANGLE CONSTRAINT
      KK=FIXDIR(3,I)
      CALL ANGL(GM,II,JJ,KK,ANG,CA,R13,R12,R23)
      IF(KLASS(I).EQ.4)GO TO 170
      WRITE(IOUT,1029)II,JJ,KK,ANG,VALDIR(I)
      XX=VALDIR(I)*PI/180.D0
      GO TO 200
  170 CONTINUE
C         ANGLE DIFFERENCE
      IIO=II
      JJO=JJ
      KKO=KK
      II=FIXDIR(4,I)
      JJ=FIXDIR(5,I)
      KK=FIXDIR(6,I)
      CALL ANGL(GM,II,JJ,KK,ANGB,CB,R13,R12,R23)
      CAT=CA
      CA=CB
      WRITE(IOUT,1026)IIO,JJO,KKO,ANG,II,JJ,KK,ANGB
  200 CONTINUE
      ZNORM=DSQRT(ZNORM)
      IF(MYCLASS.EQ.1)WRITE(IOUT,1001)ZNORM
C
  900 CONTINUE
      RETURN
 1000 FORMAT(5X,'GRADIENT ',A7,' CONSTRAINTS:DIMENSION=',I3,
     X     (/5X,8F12.6))
 1001 FORMAT(/5X,'DISTANCE NORM=',E15.8)
 1026 FORMAT(5X,'ANG(',I2,',',I2,',',I2,')  ='
     X       ,F12.6,' ANG(',I2,',',I2,',',I2,')  =',F12.6)
 1027 FORMAT(5X,'|R(',I2,') - R(',I2,')|=',F12.6,
     X ' |R(',I2,') - R(',I2,')|=',F12.6)
 1028 FORMAT(5X,'|R(',I2,') - R(',I2,')|=',F12.6,
     X ' CONSTRAINT VALUE=',F12.6)
 1029 FORMAT(5X,'ANG(',I2,',',I2,
     X       ',',I2,')  =',F12.6,' CONSTRAINT VALUE=',F12.6)
 1030 FORMAT(5X,'DIH ANG(',I2,',',I2,
     X       ',',I2,',',i2,')  =',F12.6,' CONSTRAINT VALUE=',F12.6)
      END
      SUBROUTINE SCHMO(N,V,IOUT,IFAIL)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION V(N,N)
      IFAIL=0
      DO 100 I=1,N
      Z=0.D0
      DO 90 J=1,N
   90 Z=Z+V(J,I)**2
      Z=DSQRT(Z)
      DO 80 J=1,N
   80 V(J,I)=V(J,I)/Z
  100 CONTINUE
C
      DO 500 I=2,N
      DO 300 J=1,I-1
      DP=DOT(V(1,I),V(1,J),N)
      DO 250 K=1,N
  250 V(K,I)=V(K,I)-DP*V(K,J)
  300 CONTINUE
      Z=0.D0
      DO 260 K=1,N
  260 Z=Z+V(K,I)**2
      Z=DSQRT(Z)
      IF(Z.LT.1.0D-06)THEN
      WRITE(IOUT,1000)I,Z
      IFAIL=I
      RETURN
      ENDIF
      DO 270 K=1,N
  270 V(K,I)=V(K,I)/Z
  500 CONTINUE
      RETURN
 1000 FORMAT(5X,'BAD VECTOR IN SCHMO:I=',I3,' NORM=',E15.6)
      END
      SUBROUTINE SCHMOM(N,M,V,IOUT,IFAIL)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION V(N,M)
      IFAIL=0
      DO 100 I=1,M
      Z=0.D0
      DO 90 J=1,N
   90 Z=Z+V(J,I)**2
      Z=DSQRT(Z)
      DO 80 J=1,N
   80 V(J,I)=V(J,I)/Z
  100 CONTINUE
C
      DO 500 I=2,M
      DO 300 J=1,I-1
      DP=DOT(V(1,I),V(1,J),N)
      DO 250 K=1,N
  250 V(K,I)=V(K,I)-DP*V(K,J)
  300 CONTINUE
      Z=0.D0
      DO 260 K=1,N
  260 Z=Z+V(K,I)**2
      Z=DSQRT(Z)
      IF(Z.LT.1.0D-06)THEN
      WRITE(IOUT,1000)I,Z
      IFAIL=I
      RETURN
      ENDIF
      DO 270 K=1,N
  270 V(K,I)=V(K,I)/Z
  500 CONTINUE
      RETURN
 1000 FORMAT(5X,'BAD VECTOR IN SCHMO:I=',I3,' NORM=',E15.6)
      END
      SUBROUTINE SOLNR(N,RHS,VEC,EVAL,TOL,DAMP,SOL,SCR,ITP19,
     X                 ISADDLE,NFIXDIR)
      IMPLICIT REAL*8(A-H,O-Z)
      LOGICAL FLAG
      DIMENSION RHS(N),VEC(N,N),EVAL(N),DAMP(N),SOL(N),SCR(N)
      DATA ZERO/0.D0/
      FLAG=.FALSE.
C
      WRITE(ITP19,1001)
      RHSNRM1=0.D0
      DO 10 I=1,N
   10 RHSNRM1=RHSNRM1+RHS(I)**2
      RHSNRM1=DSQRT(RHSNRM1)
      DO 100 J=1,N
      DP=ZERO
      CNORM=ZERO
      PNORM=ZERO
      DO 30 K=1,N
      CNORM=CNORM+RHS(K)**2
   30 DP=DP+RHS(K)*VEC(K,J)
      SCR(J)=DP
      IF(DABS(EVAL(J)).GT.TOL)GO TO 100
      FLAG=.TRUE.
CDRY      DMPF=1.D0-DABS(EVAL(J)/DAMP(J))
CDRY 7/95
      DMPF=DAMP(J)
      WRITE(ITP19,1004)J,EVAL(J),TOL,DMPF
      DO 40 K=1,N
      RHS(K)=RHS(K)-DP*DMPF*VEC(K,J)
   40 PNORM=PNORM+RHS(K)**2
      CNORM=DSQRT(CNORM)
      PNORM=DSQRT(PNORM)
CDRY 7/20/98
      DP=0.0
      DO 31 K=1,N
   31 DP=DP+RHS(K)*VEC(K,J)
      SCR(J)=DP
C
      WRITE(ITP19,1002)J,DP,DMPF,CNORM,PNORM,SCR(J)
  100 CONTINUE
      IF(FLAG)WRITE(ITP19,1000)RHS
      WRITE(ITP19,1003)(SCR(I),I=1,N)
      RHSNRM2=0.D0
      DO 11 I=1,N
   11 RHSNRM2=RHSNRM2+SCR(I)**2
      RHSNRM2=DSQRT(RHSNRM2)
      WRITE(ITP19,1005)RHSNRM1,RHSNRM2
C
      DO 210 J=1,N
      X=ZERO
      DO 200 I=1,N
CDRY      C=SCR(I)/(EVAL(I)+DAMP(I))
      C=SCR(I)/EVAL(I)
      IF(DABS(EVAL(I)).LT.TOL)C=SCR(I)/(EVAL(I)+DAMP(I))
      IF((I.GT.NFIXDIR).AND.(ISADDLE.EQ.0).AND.(EVAL(I).LT.0.D0))
     1          C=SCR(I)
      X=X+VEC(J,I)*C
  200 CONTINUE
      SOL(J)=X
  210 CONTINUE
C
      RETURN
 1000 FORMAT(5X,'PROJECTED GRADIENT:',4E15.8,(/2X,5E15.8))
 1001 FORMAT(/15X,' GRADIENT PROJECTION ROUTINE',/)
 1002 FORMAT(5X,'< ',I1,' ! GRAD >=',E15.8,' DAMPING=',E15.8
     1,/5X,' NORM BEFORE(AFTER) PROJECTION=',E15.8,'(',E15.8,')',
     2 ' PROJECTED RHS:',E15.8)
 1003 FORMAT(5X,'GRADIENTS IN EIGENSTATE BASIS ',4E15.8,(/2X,5E15.8))
 1004 FORMAT(5X,'DAMPING FOR ROOT=',I2,' EVAL=',E11.4,' TOL=',E11.4,
     1          'DAMP FACT=',E11.4)
 1005 FORMAT(5X,'FROM SOLNR: NORM RHS- OB=',F15.8,' ESB=',F15.8)
      END
      SUBROUTINE SetHIJ(IPRU,NDEGF,HIJP,lng,nset,Iset)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION HIJP(lng,2)
C
      iset=0
      REWIND IPRU
      read(ipru,end=800)
      iset=1
 800  continue
      REWIND IPRU
      do10  j=1,nset
   10 write(ipru)(hijp(i,j),i=1,ndegf)
      return
      end
      SUBROUTINE SGNHIJ(IPRU,NDEGF,HIJP,n,ndo,IOUT)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION HIJP(n,ndo)
      DIMENSION XGRAD(150)
C
      REWIND IPRU
      do 10 k=1,ndo
      DOT=0.D0
      read(ipru,end=900)(xgrad(i),i=1,ndegf)
      RNORM2=0.D0
      RNORM1=0.D0
      DO 100 I=1,NDEGF
      RNORM1=RNORM1+XGRAD(I)**2
      RNORM2=RNORM2+HIJP(I,k)**2
      DOT=DOT+HIJP(I,k)*XGRAD(I)
  100 CONTINUE
      RNORM1=DSQRT(RNORM1)
      RNORM2=DSQRT(RNORM2)
      dot=DOT/(RNORM1*RNORM2)
      IF(DOT.GE.0.0D0)GO TO 700
      WRITE(IOUT,1000)k,DOt
      DO 101 I=1,NDEGF
  101 HIJP(I,k)=-HIJP(I,k)
  700 continue
      if(abs(dot).gt.0.5)go to 10
      write(6,1001)dot,k
 10   continue
  900 continue
      RETURN
 1000 FORMAT(5X,'CHANGE SIGN OF HIJ(',I3,') OVERLAP =',E12.6)
 1001 format(5x,'Possible g-h switch dot=', e12.6,  ' k=', i3)
      END
      SUBROUTINE TBAK(NL,NS,T,X,Y,IGO)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION T(NL,NS),X(2),Y(2)
      DO 10 I=1,NL
   10 Y(I)=0.D0
      DO 20 I=1,NS
      DO 20 J=1,NL
      Y(J)=Y(J)+T(J,I)*X(I)
   20 CONTINUE
      IF(IGO.EQ.1)RETURN
      DO 30 I=1,NL
   30 X(I)=Y(I)
      RETURN
      END
      SUBROUTINE TOINTRL(MASS,GM,NACME,GRAD,IOUT,NAME)
      IMPLICIT REAL*8(A-H,O-Z)
C       NACME(XYZ,ATOM)
      REAL*8 MASS(3),NACME(3,3),GM(3,3),CCORDI(2,2),PCORDI(2,2)
     1      ,NACINTC(2,3),NACINTP(2,2),COMD(2),BI(3,3),GRAD(3)
      EQUIVALENCE(N1,NDIAT(1)),(N2,NDIAT(2)),(N3,NDIAT(3))
      CHARACTER*3 NAME
      COMMON/DIATAT/NDIAT(30)
C
      PI=DACOS (-1.D0)
      RTD=180.D0/PI
      WRITE(IOUT,1003)NAME,NACME
      TMASSD=MASS(N2)+MASS(N3)
C         RELATIVE TO COM OF TRIATOM
C         BUILD BI MATRIX  TO DETERMINE INTERNAL NACME'S
      CALL BCOM(MASS,BI)
      DO 110 I=1,2
      COMD(I)=( MASS(N2)*GM(I,N2) + MASS(N3)*GM(I,N3) )/TMASSD
      CCORDI(I,2)=GM(I,N3)-GM(I,N2)
      CCORDI(I,1)=GM(I,N1)-COMD(I)
      X=0.D0
      Y=0.D0
      Z=0.D0
      DO 115 J=1,3
      X=X + NACME(I,J)*BI(J,1)
      Y=Y + NACME(I,J)*BI(J,2)
      Z=Z + NACME(I,J)*BI(J,3)
  115 CONTINUE
      NACINTC(I,1)=X
      NACINTC(I,2)=Y
      NACINTC(I,3)=Z
  110 CONTINUE
C      WRITE(IOUT,1009)((NACINTC(I,J),J=1,3),I=1,2)
      DO 120 I=1,2
      PCORDI(1,I)=DSQRT( CCORDI(1,I)**2 + CCORDI(2,I)**2 )
      X=DACOS ( CCORDI(1,I)/PCORDI(1,I) )
      IF(CCORDI(2,I).LT.0.D0)X=2.D0*PI-X
      PCORDI(2,I)=X*RTD
      NACINTP(1,I)=(CCORDI(1,I)*NACINTC(1,I)+CCORDI(2,I)*NACINTC(2,I))
     1             /PCORDI(1,I)
      NACINTP(2,I)=CCORDI(1,I)*NACINTC(2,I)-CCORDI(2,I)*NACINTC(1,I)
  120 CONTINUE
      WRITE(IOUT,1008)PCORDI(1,1),PCORDI(1,2),
     X               PCORDI(2,1)-PCORDI(2,2)
C      WRITE(IOUT,1005)NACINTP
      DBR=NACINTP(1,1)
      DLR=NACINTP(1,2)
      DGAMMA=(NACINTP(2,1)-NACINTP(2,2) )/2.D0
      DG =(NACINTP(2,2)-NACINTP(2,1))/( 2.D0*PCORDI(1,1) )
      DGAMMA=(NACINTP(2,1)-NACINTP(2,2) )/2.D0
      DOMEGA=(NACINTP(2,2)+NACINTP(2,1))/2.D0
C      WRITE(IOUT,1013)' TRIATOM'
      GRAD(1)=DLR
      GRAD(2)=DBR
      GRAD(3)=DGAMMA
      WRITE(IOUT,1004)NAME,GRAD
      WRITE(IOUT,1014)NAME,DOMEGA,NACINTC(1,3),NACINTC(2,3)
C      WRITE(IOUT,1011)DG,PCORDI(1,1)
      RETURN
 1003 FORMAT(/5X,'CARTESIAN ',A3,/7X,'X',14X,'Y',/(1X,3F15.9) )
 1004 FORMAT(/5X,A3,' IN JACOBI r  R gma:', 3(1X,E16.9))
 1005 FORMAT(/5X,'  POLAR NACMES',/7X,'R',12X,'THETA',
     2       (2(/1X,2E16.9)) )
 1006 FORMAT(/5X,' INTERNAL POLAR COORDINATES',/7X,'R',12X,'THETA',
     2       (2(/1X,2E16.9)) )
C1007 FORMAT(' BI MATRIX:',/(2X,3F12.6) )
 1008 format(/,' R  r  gamma  ',3F15.9)
 1009 FORMAT(/,' INTERNAL CARTESIAN NACMES',/' X',3E16.9,/' Y',3E16.9)
 1011 FORMAT(' REVISED VERSION: - 1/R DGM=',E16.9,' R =',F12.6)
 1013 FORMAT(/5X,' FIXED ORIGIN IS ',A8 )
 1014 FORMAT(5X,A3,' TR-ROT: ANG=',F12.6,' X3=',F12.6,
     1           ' Y3= ',F12.6 )
      END
      SUBROUTINE WRTPGS(IPRU,NATOMS,NDEGF,GM,EDIF,edif2,NEWDIR,
     1                  EGRAD,GGRAD,DISP,SCALE,ncgnt,
     2                  LAMBDA,LAMBDAR,RHSN,ESTATE1,NFIXDIR)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION GM(2),EGRAD(2),GGRAD(2),disp(2)
      REAL*8 LAMBDA(5),LAMBD0(5),LAMBDAR(5)
      nlag=nfixdir+ncgnt
      NDEGF2=NDEGF*(NDEGF+1)/2
      rewind ipru
      WRITE(IPRU,1000)'PROGRS  '
      WRITE(IPRU,1003)'  NATOMS',NATOMS
      WRITE(IPRU,1003)'   NCGNT',NCGNT
      WRITE(IPRU,1003)' NFIXDIR',NFIXDIR
      WRITE(IPRU,1002)'      GM',(GM(I),I=1,3*NATOMS)
      WRITE(IPRU,1002)'    DISP',(DISP(I),I=1,NDEGF)
      WRITE(IPRU,1002)'   EGRAD',(EGRAD(I),I=1,NDEGF)
      if(ncgnt.ne.0)
     1WRITE(IPRU,1002)'    GRAD',(GGRAD(I),I=1,natoms*3*ncgnt)
      if(nlag.ne.0)then
      WRITE(IPRU,1002)'  LAMBDA',(LAMBDA(I),I=1,NFIXDIR+ncgnt)
      WRITE(IPRU,1002)' LAMBDAR',(LAMBDAR(I),I=1,NFIXDIR+ncgnt)
      endif
      WRITE(IPRU,1002)'   SCALE',SCALE
      WRITE(IPRU,1002)'   EDIF1',EDIF
      WRITE(IPRU,1002)'   EDIF2',EDIF2
      WRITE(IPRU,1002)'    RHSN',RHSN
      WRITE(IPRU,1002)' ESTATE1',ESTATE1
      WRITE(IPRU,1003)'  NEWDIR',NEWDIR
      WRITE(IPRU,1001)
      RETURN
 1000 FORMAT(1X,'&',A8)
 1001 FORMAT(1X,'/END')
 1002 FORMAT(1X,A8,'=',5(E20.10,',')/(11X,(5(E20.10,','))))
 1003 FORMAT(1X,A8,'=',15(I5,',')/(11X,(15(I5,','))))
      END
      subroutine filenamer(igo,idrt1,is1,idrt2,is2,filen,iout)
      dimension length(10),ioff(10)
      character*72 filen
      character*1 num(10),blank
      character *36 header(10), headt
      data length/18,18,34,19,19,35,4*0/
      data ioff/10,10,14,11,11,15,4*0/
      data num/'1','2','3','4','5','6','7','8','9','0'/,blank/' '/
      data header(1)/'intgrd.drt .state                  '/
c                     1234567890123456789012345678901234567890
      data header(2)/'intgrd.drt .state                  '/
      data header(3)/'intgrd.nad.drt .state .drt .state  '/
      data header(4)/'cartgrd.drt .state                  '/
c                     1234567890123456789012345678901234567890
      data header(5)/'cartgrd.drt .state                 '/
      data header(6)/'cartgrd.nad.drt .state .drt .state '/
      do 1 i=1,72
 1    filen(i:i)=blank
      headt=header(igo)
      do 2 i=1,35
 2    filen(i:i)=headt(i:i)
      ifg=ioff(Igo)
      filen(ifg+1:ifg+1)=num(idrt1)
      filen(ifg+8:ifg+8)=num(is1)
      if((igo.eq.3).or.(igo.eq.6))then
      filen(ifg+13:ifg+13)=num(idrt2)
      filen(ifg+20:ifg+20)=num(is2)
      endif
C      write(iout,1000)filen
 1000 format(5x,'filename= ',A35)
      return
      end
      subroutine qdif(lgrad,hdisp,ihdisp,char)
      implicit real*8(a-h,o-z)
      character*6 char
      dimension hdisp(2),ihdisp(2),char(2)
      do 100 i=1,lgrad
      ic2=0
      ic2m=0
      i2=i*2+1
      i2m=i2-1
      if((char(i2m).eq.'fixc').or.(char(i2).eq.'fixc'))then
      ihdisp(i)=2
      go to 100
      endif
      if((char(i2m).ne.'skip').and.(char(i2m).ne.'fixc'))ic2m=1
      if(ic2m*ic2.eq.1)then
      ihdisp(i)=0
      hdisp(i)=2.*abs(hdisp(i2))
      go to 100
      endif
      if(ic2m.eq.0)then
      ihdisp(i)=1
      hdisp(i)=abs(hdisp(i2))
      go to 100
      endif
      if(ic2m.eq.1)then
      ihdisp(i)=-1
      hdisp(i)=abs(hdisp(i2))
      endif
 100  continue
      return
      end
      subroutine makes(b,s,sinv,l,m,m1,scr,iout)
      implicit real*8(a-h,o-z)
      dimension b(l,m),s(m,m),sinv(m,m),scr(m1,m)
c
      ij=0
      do 10 i=1,m
      do 10 j=1,i
      s(j,i)=dot(b(1,i),b(1,j),l)
 10   s(i,j)=s(j,i)
C
C      call  EBTC(s,B,b,m,l,m)
C
      do 20 inv=1,m
      do 11 i=1,m
      do 12 j=1,m
 12   scr(j,i)=s(j,i)
 11   scr(m1,i)=0.
      scr(m1,inv)=1.d0
      call gauss(scr,m)
      do 13 j=1,m
 13   sinv(j,inv)=scr(j,1)
 20   continue
      ij=0
      do 25 i=1,m
      scr(ij+i,1)=-1.d0
      do 25 j=1,m
      ij=ij+1
 25   if(i.ne.j)scr(ij,1)=0
      call   APBC(scr,s,sinv,m,m,m)
      test=dot(scr,scr,m*m)
      if(sqrt(test).gt.1.0e-08)then
      write(iout,1000)sqrt(test)
      write(iout,1001)sinv
      write(iout,1001)s
      stop ' sinv failure'
 1000 format(5x,'test=', e15.8)
 1001  format(5x,3f12.5)
      endif
      return
      end
