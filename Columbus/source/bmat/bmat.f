!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      program bmat
c
c     Construction of the B matrix
c
c     COLUMBUS distribution version 5.9
c
c

      implicit real*8 (a-h,o-z)
c
c  option flet must not be used ******
c
      parameter(nmax=50,maxgeo=20,nopt=18)
      dimension lopt(nopt),title(16)
      dimension type(nmax),numb1(nmax),numb2(nmax),chg(nmax),
     1 iatno1(nmax)
      dimension bpm1(3*nmax-6,3*nmax),fcart(3*nmax*(3*nmax+1)/2)
      dimension index(3*nmax)
      integer p,q
      dimension qqq(3*nmax-6),fii(3*nmax-6)
      character*4 wk(nopt), wo
      dimension ww(2)
      equivalence(b,ww)
      logical writ,icartf
      dimension xxin(4,20),ccin(4,20),xin(4),cin(4)
      character*2 symb,symbo,type
      dimension symb(nmax)
      dimension c((3*nmax-6)*(3*nmax-6)),f(3*nmax),fi(3*nmax-6),
     & cc(3*nmax),xa(nmax),ya(nmax),za(nmax),xm(3*nmax),xy(3*nmax),
     & qq1(3*nmax),qq(3*nmax-6),b(3*nmax,3*nmax-6)
      dimension lwrk(3*nmax-6),mwrk(3*nmax-6),ki(3*nmax),ia(nmax)
      dimension ifix(20)
      dimension ga(3*nmax-6,maxgeo),xa1(3*nmax-6,maxgeo)
      dimension ra(3*nmax-6,maxgeo)
      dimension foij(maxgeo*(maxgeo+1)/2),fnij(maxgeo,maxgeo)
      dimension cd(3*nmax-6),cs((3*nmax-6)*(3*nmax-5)/2)
      character*8 fmtc
      character*80 line
c
      real*8     zero,     one,     half,       sixp5
      parameter( zero=0d0, one=1d0, half=0.5d0, sixp5=6.5d0 )
      real*8     told1,      yinc1,       yinc2,       xinc
      parameter( told1=1d-8, yinc1=1.2d0, yinc2=0.2d0, xinc=0.01d0 )
      real*8     small
      parameter( small=0.5d-6 )
      real*8     ang
      parameter( ang = 1.889726342d0 )
      real*8     ajoule
      parameter( ajoule = 0.22936758d0 )
      real*8     thre
      parameter( thre=0.001d0 )
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      data wk/
     & 'card','fmat','disp','oldf','punc','prin','angs','fixc',
     & 'fint','fcar','exfi','thre','flet','schl','sadl','inte',
     & 'noop','form'/
c
      inp=5
      iout=6
      ipun=7
      fmtc='(4f14.8)'
      niter=0
      inp2=31
c        file geomfl geometry file
      open(unit=inp2,file='geom',status='unknown')
      call progheader(iout)
c
c     contents of tape31(=inp2)
c     1)the number of iterations
c     2)cartesian coordinates
c     3)matrix of force constants
c     4)after the first iteration also old internal coordinates and
c     forces
c
c     contents of tape5(=inp)
c     1)the options card,fmat,exfi etc.
c     2)the definition of the internal coordinates
c   if fcar is specified:
c     3)the definition of the new internal coordinates
c
      rewind inp2
      do 1 i=1,nopt
      lopt(i)=0
    1 continue
      icartf=.false.
      ix=inp
      ncmax=3*nmax
      nqmax=ncmax-6
      nfix=0
      irep=0
      iconv=0
      xmiter=one
      xmaxit=one
c
c       dimension of the b matrix must conform to b(ncmax,nqmax)
c
   10  read (inp,'(a)') line
ctm
c  with g95 backspace doesn't work
c  so use character*80 line as buffer for
c  the last read line
      read (line,500) wo,x
      do 20 ii=1,nopt
         if (wk(ii).eq.wo) then
           i=ii
          go to 30
         endif
   20 continue
      go to 40
   30 lopt(i)=1
      write (iout,510) wo,x
      write (0,510) wo,x
   32 if(i.eq.1) na = (x + xinc)
      natp=na
      if (i.eq.3) lopt(3) = (x + xinc)
      if (i.eq.3.and.lopt(3).eq.0) lopt(3)=1
      go to 10
   40 continue
      if (na.gt.0.and.na.le.nmax) go to 50
      write (iout,520) na
      stop
   50 continue
      ipu=0
      n=na
      if (lopt(1).eq.1) go to 110
      write(iout,525)
      stop
  110 write (iout,530)
      na=0
   71 read(inp2,401,end=70)symbo,atmnu,cx,cy,cz,atmas
  401 format(1x,a2,2x,f5.1,4f14.8)
      na=na+1
      i3=na*3
      symb(na)=symbo
      type(na)=symbo
      chg(na)=atmnu
      ia(na)=chg(na)+0.1
      xa(na)=cx
      ya(na)=cy
      za(na)=cz
      xm(i3)=atmas
         if (xm(i3).eq.zero) xm(i3)=one
         if (lopt(7).eq.1) go to 60
         xa(na)=xa(na)/ang
         ya(na)=ya(na)/ang
         za(na)=za(na)/ang
   60    continue
      write (iout,550) na,ia(na),xa(na),ya(na),za(na),xm(i3)
         xm(i3)=one/xm(i3)
         xm(i3-1)=xm(i3)
         xm(i3-2)=xm(i3)
      go to 71
   70 continue
      n=na
      if(na.gt.0 .and. na.le.nmax)go to 72
      write(iout,520)na
      stop
   72 continue
      nek=3*na
      write(0,*) 'nek=',nek,'na=',na
      if (lopt(3).gt.0) then
c *** read displacements
   90 continue
      write(*,*)'Read displacements'
      ndi=lopt(3)
      read (line,600) (xxin(j,1),ccin(j,1),j=1,1)
      write(*,*)"internal displacement",(xxin(j,1),ccin(j,1),j=1,4)
      do 100 i=2,ndi
         read (inp,600) (xxin(j,i),ccin(j,i),j=1,4)
         write(*,*)"internal displacement",(xxin(j,i),ccin(j,i),j=1,4)
  100 continue
      goto 140
      endif
c
c *** calculate bmat and internal coordinates
  140 continue
      writ=.false.
ca
      lopt(6)=1
ca
      if (lopt(6).eq.1) writ=.true.
      call machb (nek,b,ncmax,nqmax,xa,ya,za,qq,n,inp,iout,nq,writ,
     1 .false.,ncard)
c
c     write(iout,*)
c     call prblkt('B matrix',b,nek,nek,nq,'cartc','intc',
c    & 1,iout)
c     write(iout,*)
c
      if(lopt(6).ne.1) goto 151
      write (iout,610)
      do 150 i=1,nq
         write (iout,*)
         write (iout,*)'  intc:',i
         write (iout,620) (b(j,i),j=1,nek)
  150 continue
  151 continue
c
c *** calculate (bmb+)**-1
      i1=0
      do 170 i=1,nq
      do 170 j=1,nq
         i1=i1+1
         s=zero
         do 160 k=1,nek
  160    s=s+b(k,i)*b(k,j)*xm(k)
  170 c(i1)=s
      tol = told1
      call osinv1 (c,nq,d,tol,lwrk,mwrk)
      write (iout,630) d
      if (lopt(3).gt.0) go to 320
c
c
  320 continue
      write (iout,730)
      write (iout,740) (qq(j),j=1,nq)
      ii=0
  325 ii=ii+1
         do 330 j=1,4
            xin(j)=xxin(j,ii)
            cin(j)=ccin(j,ii)
  330    continue
         write (iout,750) (xin(j),cin(j),j=1,4)
         do 340 j=1,nq
            cc(j)=zero
  340    continue
         do 350 j=1,4
            j1=xin(j)+0.1
            if (j1.le.0) go to 350
            cc(j1)=cin(j)
  350    continue
         do 360 j=1,nq
            qq1(j)=cc(j)
  360    continue
      go to 362
  361 do 363 i=1,ncard
      backspace inp
  363 continue
      call machb(nek,b,ncmax,nqmax,xa,ya,za,qq,n,inp,iout,nq,.false.,
     1 .false.,ncard)
      irep=0
  366 i1=0
      do 364 i=1,nq
      do 364 j=1,nq
      i1=i1+1
      s=zero
      do 365 k=1,nek
  365 s=s+b(k,i)*b(k,j)*xm(k)
  364 c(i1)=s
      call osinv1(c,nq,d,tol,lwrk,mwrk)
      if(irep.gt.0) go to 380
  362 continue
         i3=0
         do 370 i=1,n
            xy(i)=xa(i)
            j=i+n
            xy(j)=ya(i)
            j=j+n
            xy(j)=za(i)
  370    continue
      if(iconv.eq.0) go to 380
  371 miter=miter+1
      if(miter.gt.maxit)goto 459
      xmiter=miter
      irep=0
      do 372 i=1,nq
  372 qq1(i)=cc(i)/xmaxit
  380    continue
         irep=irep+1
         ij=0
         do 400 i=1,nq
            s=zero
            do 390 j=1,nq
               ij=ij+1
               s=s+c(ij)*qq1(j)
  390       continue
            fi(i)=s
  400    continue
         do 420 i=1,nek
            s=zero
            do 410 j=1,nq
               s=s+b(i,j)*fi(j)
  410       continue
            qq1(i)=s*xm(i)
  420    continue
         i3=0
         do 430 i=1,n
            i3=i3+3
            xy(i)=xy(i)+qq1(i3-2)
            j=i+n
            xy(j)=xy(j)+qq1(i3-1)
            j=j+n
            xy(j)=xy(j)+qq1(i3)
  430    continue
         i2=2*n+1
         do 440 i=1,ncard
            backspace inp
  440    continue
        call machb (nek,b,ncmax,nqmax,xy(1),xy(n+1),xy(i2),qq1,n,inp,
     1    iout,nq,.false.,.false.,ncard)
         write (iout,760) irep
         write (iout,740) (qq1(i),i=1,nq)
         qmax=zero
         do 450 i=1,nq
            qq1(i)=qq(i)+xmiter/xmaxit*cc(i)-qq1(i)
            if (qmax.lt.abs(qq1(i))) qmax=abs(qq1(i))
  450    continue
      if(irep.lt.9.and.qmax.gt.small) go to 366
      if(qmax.gt.small) write(iout,801)
      if(iconv.gt.0.and.qmax.le.small) go to 371
c-      if(iconv.gt.0.and.qmax.gt.small) call exit
      if(iconv.gt.0 .and. qmax.gt.small) call bummer(
     & 'convergence error in bmat, iconv=', iconv, faterr )
      if(qmax.le.small) go to 459
      iconv=1
      maxit=5
      xmaxit=maxit
      miter=0
      go to 361
  459 write(iout,770)
         if (ipu.eq.0) go to 460
cmd   write(ipu,780) (xin(i),cin(i),i=1,4), (title(i),i=3,8)
      write(ipu,780) (xin(i),cin(i),i=1,4)
  460    continue
         i3=0
      ij=0
         do 470 i=1,n
            i3=i3+3
            ym=one/xm(i3)
            if (ym .lt. sixp5) iat = (ym + yinc1) * half
            if (ym .ge. sixp5) iat = (ym + yinc2) * half
            xiat=ia(i)
            if (ia(i).eq.0) xiat=iat
            j=i+n
            j1=j+n
            x=xy(i)-xa(i)
            y=xy(j)-ya(i)
            z=xy(j1)-za(i)
            if(abs(x).gt.thre) ij=1
            if(abs(y).gt.thre) ij=1
            if(abs(z).gt.thre) ij=1
            write (iout,790) i,xiat,x,y,z,xy(i),xy(j),xy(j1),ym
            if (ipu.eq.0) go to 470
            write(ipu,800) symb(i),xiat,xy(i),xy(j),xy(j1)
470      continue
      if(lopt(2).eq.1) go to 482
      if(lopt(13).eq.1)go to 482
  480 continue
      if(ii.lt.ndi)go to 325
  482 rewind inp2
      if(lopt(3).gt.0) go to 483
      jj=1
      do 481 i=1,nq
      ij1=jj+i-1
      read(inp2,720)(c(j),j=jj,ij1)
      jj=jj+nq
  481 continue
  483 rewind inp2
      niter=niter+1
      jj=0
      write(iout,*) 'new cartesian coordinates [au]'
c      do 491 i=1,natp
      do 491 i=1,na
c      write(inp2,920)type(i),numb1(i),numb2(i),chg(i),iatno1(i)
c     numb=numb2(i)
      xiat=chg(i)
c     do 492 j=1,numb
      jj=jj+1
      jj1=jj+n
      jj2=jj1+n
      x=xy(jj)*ang
      y=xy(jj1)*ang
      z=xy(jj2)*ang
      xxmk=one/xm(3*jj)
c     write(inp2,fmtc)x,y,z,xxmk
      write(inp2,401) type(i),chg(i),x,y,z,xxmk
      write(iout,792)jj,xiat,x,y,z
c 492 continue
  491 continue
      if(lopt(3).gt.0) go to 487
      jj=1
      do 485 i=1,nq
      ij1=jj+i-1
      write(inp2,720)(c(j),j=jj,ij1)
      jj=jj+nq
  485 continue
c  I have no idea what is qqq and fii...
cmd   do 486 i=1,nq
c 486 write(inp2,660) qqq(i),fii(i)
  487 continue
      rewind inp2
  490 continue
      call bummer('normal termination',0,3)
      stop
c
  500 format (a4,6x,f10.5)
  501 format ('(4f',f4.1,')')
  502 format(10x,2f10.3)
  510 format(1x,a4,'  option is on ',2f10.5)
  511 format(a2,a4)
  520 format (' too many nuclei ',i5)
  525 format ('  card option must be specified')
  530 format (20x,'nuclear coordinates in angstrom '/)
  540 format(2x,a4,4x,5f10.5)
  550 format (5x,i2,3x,i2,3f12.7,3x,f12.6)
  560 format (3d15.7)
  570 format (//' forces'/)
  580 format (1x,3f12.6)
  590 format (/' forces do not vanish, sum=',f15.7/)
  600 format (2f10.6)
  610 format (/' b matrix '/)
  615 format (/' (bmb+)**-1(bm) matrix'/)
  620 format (1x,4(3f10.6,3x))
  630 format(/6x,'determinant  = ',e16.5)
  635 format(/' internal coordinates '/)
  640 format (/' internal coordinates and forces '/)
  650 format (2x,i2,4x,2f12.7)
  652 format(' inverse hessian matrix ill defined')
  660 format (8f10.7)
  670 format (/' force constants '/)
  672 format (/'  cartesian force constants '/)
  677 format (/'  new internal force constants '/)
  680 format (/' old geometry and forces '/)
  690 format (/' geometry change and new internal coordinates'/)
  700 format (/' improved force constant matrix '/)
  710 format (1x,10f10.5)
  720 format (8f10.5)
  725 format (13f10.5)
  730 format (/' original internal coordinates '/)
  740 format (2x,5f12.7,3x,5f12.7)
  750 format (/' internal displacement',4(f4.0,f12.6))
  760 format (' step=',i4)
  770 format (/' cartesian displacements ',
     & 'and new cartesian coordinates'/)
  772 format(/' cartesian coordinates in a.u.'/)
  780 format('displ. ',4(f3.0,1h,,f6.4),3x,3(a4,a6))
  790 format(1x,i3,'   n',f3.0,3f12.7,6x,3f12.7,6x,f12.6)
  792 format(1x,i3,'   n',f3.0,3f12.7)
  800 format('n=',a4,4x,5f10.6)
  801 format(/' *****caution, within 9 steps no convergence*****'/)
  900 format(i4)
  910 format(/' program bmat, iteration number',i4)
  920 format(a3,2i3,f3.0,i3)
  930 format(4f14.8)
c 940 format(4f14.8,24x)
c
      end
      subroutine nom (u)
      implicit real*8 (a-h,o-z)
      dimension u(3)
      parameter(one=1d0)
      x=one/sqrt(scalar(u,u))
      do 10 i=1,3
         u(i)=u(i)*x
   10 continue
      return
c
      end
      real*8 function s2(x)
      implicit real*8 (a-h,o-z)
      parameter(one=1d0)
      s2 = sqrt(one - x**2)
      return
c
      end
      real*8 function scalar(u,v)
      implicit real*8 (a-h,o-z)
      dimension u(3), v(3)
      parameter(zero=0d0)
      scalar=zero
      do 10 i=1,3
         scalar=scalar+u(i)*v(i)
   10 continue
      return
c
      end
      subroutine normal (u,v,w)
      implicit real*8 (a-h,o-z)
      dimension u(3), v(3), w(3)
c
c     99999...  w wird ein senkrecht auf die ebene(u,v) stehender einhei
c      tor
c
      w(1)=u(2)*v(3)-u(3)*v(2)
      w(2)=u(3)*v(1)-u(1)*v(3)
      w(3)=u(1)*v(2)-u(2)*v(1)
      call nom (w)
      return
c
      end
      subroutine machb (nek,bmat,ncmax,nqmax,xa,ya,za,qq,n,inp,iout,
     & nq,wri,qonly,ncard)
c
c     # 20-feb-02 some obsolete hollerith junk removed. -rls
c
      implicit real*8 (a-h,o-z)
      logical wri, qonly
c
c     nq ist die zahl der inneren koordinaten
c     nek ist  3* die zahl der kerne fuer die krafte berechnet werden
c     .... bmat is the transpose b matrix
c     datenangabe fuer jede koordinate eine karte typ und die atome
c     typ kann sein stretch,bend,out,torsion,tors2,linear1,linear2 links
c     gebuendelt zu schreiben. bei stretch 2, bei bend 3,bei den anderen
c     4 atome in format f10.3, die nummern der atome
c
      dimension qq(1)
      character*4 typ, tipus(7)
      character*1 we, wort(3)
      dimension a(4), ia(4), u(3), v(3), w(3), z(3), x(3), uu(3), vv(3),
     1 ww(3), zz(3), uv(12)
      equivalence (ka,ia(1)), (kb,ia(2)), (kc,ia(3)), (kd,ia(4))
      equivalence (uv(1),uu(1)), (uv(4),vv(1)), (uv(7),ww(1)), (uv(10),z
     1z(1))
      dimension bmat(ncmax,nqmax), xa(1), ya(1), za(1)
c
      real*8     zero,     one,     two,     three,     tenth
      parameter( zero=0d0, one=1d0, two=2d0, three=3d0, tenth=0.1d0 )
      real*8     pi
      parameter( pi = 3.14159265358979323846264338328d0 )
      real*8     pis2
      parameter( pis2 = pi / two )
      real*8     twopi
      parameter( twopi = two * pi )
c
      data tipus/'stre','bend','out ','tors','lin1','lin2','tor2'/
      data wort/ 'k', ' ', 'f' /
c
      ncard    = 0
      o        = one
      nab      = nek
      scaleold = zero
      scalex   = one
      nab      = nab / three + tenth
      i        = 0
      c1       = zero
      matf     = 0
      if (wri) write (iout,330)
   10 read (inp,340) we
      backspace inp
      do 20 k=1,3
         if (we.eq.wort(k)) go to 50
   20 continue
   30 continue
      c1 = sqrt(one / c1) * scalex
      qq(i)=qq(i)*c1
      if (qonly) go to 310
      do 40 k=1,nek
         bmat(k,i)=bmat(k,i)*c1
   40 continue
      go to 310
   50 continue
      ncard=ncard+1
      read (inp,350) we,scale,c,typ,a
       if (scale.eq. zero) scale=one
       if (c.eq. zero) c=one
       if (we.eq.wort(2)) c1=c1+c**2
       if (we.ne.wort(1)) go to 100
         if (we.eq.wort(1)) then
          scaleold=scalex
          scalex=scale
         endif ! end of: if (we.eq.wort(1)) then
       if (i.eq.0) go to 80
       if (wri) write (iout,360)
      c1 = sqrt(one / c1) * scaleold
      if (qonly) go to 70
        do k=1,nek
        bmat(k,i)=bmat(k,i)*c1
        enddo ! end of: do k=1,nek
   70 qq(i)=qq(i)*c1
   80 i=i+1
      qq(i)=zero
      c1=c**2
      if (qonly) go to 100
        do j=1,nek
           bmat(j,i)=zero
        enddo !end of: do j=1,nek
  100 if (we.ne.wort(3)) go to 110
      matf=1
      go to 30
  110 do 120 k=1,4
         ia(k)=a(k)+0.1
  120 continue
      do 130 k=1,7
         if (typ.eq.tipus(k)) go to 140
  130 continue
      error=7
      write (iout,370) i
      go to 320
  140 if (wri) write (iout,380) i,typ,scale,ia,c
      if (ka.lt.1.or.ka.gt.nab.or.kb.lt.1.or.kb.gt.nab) go to 300
      if (k.gt.1.and.(kc.lt.1.or.kc.gt.nab)) go to 300
      if (k.gt.2.and.(kd.lt.1.or.kd.gt.nab)) go to 300
      go to (150,160,180,210,230,250,211), k
c
c     ..... stretch
c
  150 call vektor (uu,r1,ka,kb,xa,ya,za)
      vv(1)=-uu(1)
      vv(2)=-uu(2)
      vv(3)=-uu(3)
      ia(3)=0
      ia(4)=0
      qq(i)=qq(i)+r1*c
      go to 270
c
c     .....bending
c
  160 call vektor (u,r1,ka,kc,xa,ya,za)
      call vektor (v,r2,kb,kc,xa,ya,za)
      co=scalar(u,v)
      si=s2(co)
      do 170 l=1,3
         uu(l)=(co*u(l)-v(l))/(si*r1)
         vv(l)=(co*v(l)-u(l))/(si*r2)
         ww(l)=-uu(l)-vv(l)
  170 continue
      ia(4)=0
      qq(i)=qq(i)+c*arcos(co)
      go to 270
c
c     .....out of plane
c
  180 call vektor (u,r1,ka,kd,xa,ya,za)
      call vektor (v,r2,kb,kd,xa,ya,za)
      call vektor (w,r3,kc,kd,xa,ya,za)
      call normal (v,w,z)
      steta=scalar(u,z)
      cteta=s2(steta)
      cfi1=scalar(v,w)
      sfi1=s2(cfi1)
      cfi2=scalar(w,u)
      cfi3=scalar(v,u)
      den=cteta*sfi1**2
      st2=(cfi1*cfi2-cfi3)/(r2*den)
      st3=(cfi1*cfi3-cfi2)/(r3*den)
      do 190 l=1,3
         vv(l)=z(l)*st2
         ww(l)=z(l)*st3
  190 continue
      call normal (z,u,x)
      call normal (u,x,z)
      do 200 l=1,3
         uu(l)=z(l)/r1
         zz(l)=-uu(l)-vv(l)-ww(l)
  200 continue
      cx = -c
      if (steta .lt. zero) cx = c
      qq(i) = qq(i) - cx * arcos(cteta)
      go to 270
c
c     ..... torsion
c
  210 call vektor (u,r1,ka,kb,xa,ya,za)
      call vektor (v,r2,kc,kb,xa,ya,za)
      call vektor (w,r3,kc,kd,xa,ya,za)
      call normal (u,v,z)
      call normal (w,v,x)
      co=scalar(u,v)
      co2=scalar(v,w)
      si=s2(co)
      si2=s2(co2)
      do 220 l=1,3
         uu(l)=z(l)/(r1*si)
         zz(l)=x(l)/(r3*si2)
         vv(l)=(r1*co/r2-one)*uu(l)-r3*co2/r2*zz(l)
         ww(l)=-uu(l)-vv(l)-zz(l)
  220 continue
      co=scalar(z,x)
      u(1)=z(2)*x(3)-z(3)*x(2)
      u(2)=z(3)*x(1)-z(1)*x(3)
      u(3)=z(1)*x(2)-z(2)*x(1)
      co2=scalar(u,v)
      s=arcos(-co)
      if (co2 .lt. zero) s = -s
      if (s .gt. pis2)   s = s - twopi
      qq(i) = qq(i) - c * s
c
c     .... remember that the range of this coordinate is -pi/2 to 3*pi/2
c     .... in order to shift the discontinuity off the planar position
c
      go to 270
c
c     ..... torsion1  (only other range as in tors)
c
  211 call vektor (u,r1,ka,kb,xa,ya,za)
      call vektor (v,r2,kc,kb,xa,ya,za)
      call vektor (w,r3,kc,kd,xa,ya,za)
      call normal (u,v,z)
      call normal (w,v,x)
      co=scalar(u,v)
      co2=scalar(v,w)
      si=s2(co)
      si2=s2(co2)
      do 221 l=1,3
         uu(l)=z(l)/(r1*si)
         zz(l)=x(l)/(r3*si2)
         vv(l)=(r1*co/r2-one)*uu(l)-r3*co2/r2*zz(l)
         ww(l)=-uu(l)-vv(l)-zz(l)
  221 continue
      co=scalar(z,x)
      u(1)=z(2)*x(3)-z(3)*x(2)
      u(2)=z(3)*x(1)-z(1)*x(3)
      u(3)=z(1)*x(2)-z(2)*x(1)
      co2=scalar(u,v)
      s=arcos(-co)
      if (co2 .lt. zero) s = -s
      if (s .gt. zero) s = s - twopi
      qq(i) = qq(i) - c * s
c
c     .... remember that the range of this coordinate is 0 to pi/2
c     .... use it for perpendicular geometries
c
      go to 270
c
c     ......linear complanar bending
c
  230 call vektor (u,r1,ka,kc,xa,ya,za)
      call vektor (v,r2,kd,kc,xa,ya,za)
      call vektor (x,r2,kb,kc,xa,ya,za)
      co=scalar(v,u)
      co2=scalar(x,v)
      qq(i)=qq(i)+c*(pi-arcos(co)-arcos(co2))
      call normal (v,u,w)
      call normal (u,w,z)
      call normal (x,v,w)
      call normal (w,x,u)
c
c     ..... coordinate positiv if atom a moves towards atom d
c
      do 240 l=1,3
         uu(l)=z(l)/r1
         vv(l)=u(l)/r2
         ww(l)=-uu(l)-vv(l)
  240 continue
      ia(4)=0
      go to 270
c
c     .....linear perpendicular bending
c
  250 call vektor (u,r1,ka,kc,xa,ya,za)
      call vektor (v,r2,kd,kc,xa,ya,za)
      call vektor (z,r2,kb,kc,xa,ya,za)
      call normal (v,u,w)
      call normal (z,v,x)
      do 260 l=1,3
         uu(l)=w(l)/r1
         vv(l)=x(l)/r2
         ww(l)=-uu(l)-vv(l)
  260 continue
      ia(4)=0
      co=scalar(u,w)
      co2=scalar(z,w)
      qq(i)=qq(i)+c*(pi-arcos(co)-arcos(co2))
  270 if (qonly) go to 10
      do 290 j=1,4
         m=ia(j)
         if (m.le.0) go to 290
         m=m-1
         j1=3*(j-1)
         do 280 l=1,3
            m1=3*m+l
            l1=j1+l
            bmat(m1,i)=uv(l1)*c+bmat(m1,i)
  280    continue
  290 continue
      go to 10
  300 error=6
      write (iout,390) i
  310 nq=i
  320 return
c
  330 format (/' definition of internal coordinates'/)
cmd  340 format (a4)
cmd  350 format (a4,6x,f10.4,a4,6x,5f10.4)
  340 format (a1)
  350 format (a1,f9.5,f10.4,a4,6x,4f10.4)
  360 format (1x)
  370 format (/' undefined int. coordinate type at no. ',i3/1x,40('*'))
  380 format (1x,i3,1h.,a8,f13.8,1x,4i10,f12.5)
  390 format (/' atoms erronously defined, coordinate no. ',i3/1x,
     & 40('*'))
c
      end
      real*8 function arcos(x)
      implicit real*8 (a-h,o-z)
      real*8    zero,    one,     onem,      two,     small
      parameter(zero=0d0,one=1d0, onem=-1d0, two=2d0, small=1d-11)
      real*8     pi
      parameter( pi = 3.14159265358979323846264338328d0 )
      real*8     pis2
      parameter( pis2 = pi / two )
c
      if (x .ge. one) go to 10
      if (x .le. onem) go to 20
      x1 = sqrt(one - x**2)
      if (abs(x) .lt. small) go to 30
      s = atan( x1 / x )
      if (x .lt. zero) s = s + pi
      arcos = s
      return
   10 arcos = zero
      return
   20 arcos = pi
      return
   30 arcos = pis2
      return
c
      end
      subroutine vektor (u,r,i,j,xa,ya,za)
      implicit real*8 (a-h,o-z)
      dimension u(3), xa(1), ya(1), za(1)
c
c       bildet den normierten entfernungsvektor vom kern j nach kern i
c        und die entfernung r
c
      u(1)=xa(i)-xa(j)
      u(2)=ya(i)-ya(j)
      u(3)=za(i)-za(j)
      r = sqrt(scalar(u,u))
      call nom (u)
      return
c
      end
      subroutine osinv1 (a,n,d,tol,l,m)
      implicit real*8 (a-h,o-z)
c
c     parameters%  a - input matrix , destroyed in computation and repla
c                      by resultant inverse (must be a general matrix)
c                  n - order of matrix a
c                  d - resultant determinant
c            l and m - work vectors of lenght n
c                tol - if pivot element is less than this parameter the
c                      matrix is taken for singular (usually = 1.0e-8)
c     a determinant of zero indicates that the matrix is singular
c
      dimension a(*), m(*), l(*)
c
      real*8    zero,     one
      parameter(zero=0d0, one=1d0)
      d = one
      nk=-n
      do 180 k=1,n
         nk=nk+n
         l(k)=k
         m(k)=k
         kk=nk+k
         biga=a(kk)
         do 20 j=k,n
            iz=n*(j-1)
         do 20 i=k,n
            ij=iz+i
c
c     10 follows
c
            if (abs(biga)-abs(a(ij))) 10,20,20
   10       biga=a(ij)
            l(k)=i
            m(k)=j
   20    continue
         j=l(k)
         if (j-k) 50,50,30
   30    ki=k-n
         do 40 i=1,n
            ki=ki+n
            holo=-a(ki)
            ji=ki-k+j
            a(ki)=a(ji)
   40    a(ji)=holo
   50    i=m(k)
         if (i-k) 80,80,60
   60    jp=n*(i-1)
         do 70 j=1,n
            jk=nk+j
            ji=jp+j
            holo=-a(jk)
            a(jk)=a(ji)
   70    a(ji)=holo
   80    if (abs(biga)-tol) 90,100,100
   90    d = zero
         return
  100    do 120 i=1,n
            if (i-k) 110,120,110
  110       ik=nk+i
            a(ik)=a(ik)/(-biga)
  120    continue
         do 150 i=1,n
            ik=nk+i
            ij=i-n
         do 150 j=1,n
            ij=ij+n
            if (i-k) 130,150,130
  130       if (j-k) 140,150,140
  140       kj=ij-i+k
            a(ij)=a(ik)*a(kj)+a(ij)
  150    continue
         kj=k-n
         do 170 j=1,n
            kj=kj+n
            if (j-k) 160,170,160
  160       a(kj)=a(kj)/biga
  170    continue
         d=d*biga
         a(kk)=one/biga
  180 continue
      k=n
  190 k=k-1
      if (k) 260,260,200
  200 i=l(k)
      if (i-k) 230,230,210
  210 jq=n*(k-1)
      jr=n*(i-1)
      do 220 j=1,n
         jk=jq+j
         holo=a(jk)
         ji=jr+j
         a(jk)=-a(ji)
  220 a(ji)=holo
  230 j=m(k)
      if (j-k) 190,190,240
  240 ki=k-n
      do 250 i=1,n
         ki=ki+n
         holo=a(ki)
         ji=ki+j-k
         a(ki)=-a(ji)
  250 a(ji)=holo
      go to 190
  260 return
c
      end
      subroutine print_revision(name,nlist)
      implicit none
      integer nlist
      character*12 name
      character*70 string
  50  format('* ',a,t12,a,t40,a,t68,' *')
      write(string,50) name(1:len_trim(name)) // '.f',
     .   '$Revision: 2.3.14.1 $','$Date: 2013/04/11 14:37:29 $'
      call substitute(string)
      write(nlist,'(a)') string
      return
      end


       subroutine progheader(nlist)
       implicit none
       integer nlist
       character*70 str
      write (nlist,"(30x,'program ""bmat"" 7.0'/
     &  28x,'columbus program system'//
     &  13x,'this program constructs the B-matrix '/)")
c
c     # print out the name and address of the local programmer
c     # responsible for maintaining this program.
c
      call who2c( 'ARGOS', nlist )
 20   format(5('****'),'*** File revision status: ***',5('****'))
      write(nlist,20)
      call print_revision('bmat       ',nlist)
 21   format(17('****'))
      write(nlist,21)
      end

      subroutine substitute(str)
      implicit none
      character*70 str
      integer i
       do i=1, len_trim(str)
         if (str(i:i).eq.'$') str(i:i)=' '
       enddo
      return
      end



