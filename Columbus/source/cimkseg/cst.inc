!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
C
C     Common variables
C
      INTEGER             ST1(8),      ST10(6),     ST11(4)
      INTEGER             ST11D(4),    ST12(4),     ST13(4),     ST14(2)
      INTEGER             ST2(8),      ST3(8),      ST4(6),      ST4P(6)
      INTEGER             ST5(6),      ST6(6),      ST7(6),      ST8(6)
C
      COMMON / CST    /   ST1,         ST2,         ST3,         ST4
      COMMON / CST    /   ST4P,        ST5,         ST6,         ST7
      COMMON / CST    /   ST8,         ST10,        ST11D,       ST11
      COMMON / CST    /   ST12,        ST13,        ST14
C
