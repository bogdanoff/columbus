!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
C
C     Common variables
C
      INTEGER             N0INT,       N0LP,        N1INT,       N1LP
      INTEGER             N2INT,       N2LP
C
      COMMON / SOINF  /   N2INT,       N1INT,       N0INT,       N2LP
      COMMON / SOINF  /   N1LP,        N0LP
C
