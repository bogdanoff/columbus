!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
C
C     Common variables
C
      INTEGER             INTBL2(MAXSYM),           INTBLK
      INTEGER             INTBLL(MAXSYM),           K1FX(MXNSEG)
      INTEGER             K1SX(MXNSEG),             KSYMX(MXNSEG)
      INTEGER             NINKLX(MXNSEG),           STW12X(MXNSEG)
      INTEGER             STW1X(MXNSEG),            STW2X(MXNSEG)
      INTEGER             STW3X(MXNSEG),            STX12X(MXNSEG)
      INTEGER             STX1X(MXNSEG),            STX2X(MXNSEG)
      INTEGER             STX3X(MXNSEG)
C
      COMMON / BLINT3 /   INTBLK,      KSYMX,       K1SX,        K1FX
      COMMON / BLINT3 /   STW12X,      STW1X,       STW2X,       STW3X
      COMMON / BLINT3 /   STX12X,      STX1X,       STX2X,       STX3X
      COMMON / BLINT3 /   NINKLX,      INTBL2,      INTBLL
C
