!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
C
C     Common variables
C
      INTEGER             D0W,         D0X,         D0Y,         D0Z
      INTEGER             DDWE,        DDWI,        DDXE,        DDXI
      INTEGER             DDYE,        DDYI,        DDZE,        DDZI
      INTEGER             DG0X,        DG2X,        DG4X,        DYW
      INTEGER             DYX,         DYZ,         W4X,         WW0X
      INTEGER             WW0XSO,      WW2X,        WX2X,        WX2XSO
      INTEGER             WZ2X,        X4X,         XX0X,        XX0XSO
      INTEGER             XX2X,        XX2XSO,      XX2XSO2,     XZ2X
      INTEGER             Y4X,         YW1X,        YW1XSO,      YW3X
      INTEGER             YX1X,        YX1XSO,      YX3X,        YY0X
      INTEGER             YY0XSO,      YY2EX,       YY2XSO,      YZ1X
      INTEGER             YZ1XSO,      Z4X,         ZZ0X,        ZZ0XSO
C
      COMMON / COUNTER/   DG0X,        DG2X,        DG4X,        ZZ0X
      COMMON / COUNTER/   YY0X,        XX0X,        WW0X,        YZ1X
      COMMON / COUNTER/   YX1X,        YW1X,        YY2EX,       WW2X
      COMMON / COUNTER/   XX2X,        XZ2X,        WZ2X,        WX2X
      COMMON / COUNTER/   YX3X,        YW3X,        Z4X,         Y4X
      COMMON / COUNTER/   X4X,         W4X,         ZZ0XSO,      YY0XSO
      COMMON / COUNTER/   XX0XSO,      WW0XSO,      YZ1XSO,      YX1XSO
      COMMON / COUNTER/   YW1XSO,      YY2XSO,      XX2XSO,      WX2XSO
      COMMON / COUNTER/   DYZ,         DYX,         DYW,         D0Z
      COMMON / COUNTER/   D0Y,         D0W,         D0X,         DDZI
      COMMON / COUNTER/   DDYI,        DDXI,        DDWI,        DDZE
      COMMON / COUNTER/   DDYE,        DDXE,        DDWE,        XX2XSO2
C
