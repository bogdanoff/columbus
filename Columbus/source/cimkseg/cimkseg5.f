!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      subroutine oneyxw(
     &   indsymbra,indsymket, conftbra,conftket,
     &   indxbra, indxket,cibra,sgmabra, ciket, sgmaket,
     &   transf, intbuf, scr1,   scr2,iwx )

c
c  one-external xy and wy cases
c
c  10-dec-90 /cnndx/ added, /f4x/ removed. -rls
c  01-dec-89 /f4x/ added for nndx(*). vivvp() and vvvip() added
c          for titan optimization. -rls
c  modified by i.shavitt and k. kim, august 1989
c  (inserted rank-1 updates and matrix-vector products for cray)
c
         IMPLICIT NONE

C====>Begin Module ONEYXW                 File oneyxw.f                 
C---->Makedcls Options: All variables                                   
C
C     External functions
C
      EXTERNAL            DDOT_WR
C
      REAL*8              DDOT_WR
C
C     Parameter variables
C
      INTEGER             MAXSYM
      PARAMETER           (MAXSYM = 8)
      INTEGER             NMOMX
      PARAMETER           (NMOMX = 1023)
      INTEGER             MULTMX
      PARAMETER           (MULTMX = 19)
      INTEGER             NMULMX
      PARAMETER           (NMULMX = 9)
C
      REAL*8              ONE
      PARAMETER           (ONE = 1D0)
C
C     Argument variables
C
      INTEGER             CONFTBRA(*), CONFTKET(*), INDSYMBRA(*)
      INTEGER             INDSYMKET(*),             INDXBRA(*)
      INTEGER             INDXKET(*),  IWX
C
      REAL*8              CIBRA(*),    CIKET(*),    INTBUF(*),   SCR1(*)
      REAL*8              SCR2(*),     SGMABRA(*),  SGMAKET(*)
      REAL*8              TRANSF(*)
C
C     Local variables
C
      INTEGER             BRA,         CLEV,        KET,         M1LP
      INTEGER             M1LPR,       MUL,         NBRA,        NBRAA
      INTEGER             NKET,        NKETA,       NMB1,        NMB2
      INTEGER             NY,          STARTX,      TASKID,      XS
      INTEGER             XSYM,        YS,          YSYM
C
      LOGICAL             SKIP,        WALK
C
      REAL*8              SIGN,        VALSQ
C
      INCLUDE 'cnndx.inc'
      INCLUDE 'counter.inc'
      INCLUDE 'data.inc'
      INCLUDE 'inf3.inc'
      INCLUDE 'onex_stat.inc'
      INCLUDE 'ox.inc'
      INCLUDE 'sizes_stat.inc'
      INCLUDE 'solxyz.inc'
      INCLUDE 'vppass.inc'
C====>End Module   ONEYXW                 File oneyxw.f                 

      bra  = 0
      ket  = 0
      walk = .false.
c
c     iwx=1 xy loops
c     iwx=2 wy loops
c
      if(spnodd)niot = niot + 1
      mul = 0
      call upwkst(hilev,niot,rwlphd,mlppr,mlp,clev)
c
700   continue
      call upwlknew( ket, bra, walk, mul,segsum(2),segsum(1),skip )
      if (walk)then
         if(spnodd)niot = niot - 1
C HPM   return
c       call _f_hpm_stop_(33,266)
C HPM
         return
      else
         if (skip) goto 710
      endif
      nketa = indxket(ket)
      if (nketa.lt.0) go to 710
      nbraa = indxbra(bra)
      if (nbraa.lt.0) go to 710
      nbra = conftbra(nbraa)
      xs = indsymbra(nbraa)
      ys = indsymket(nketa)
      ysym = sym21(ys)
      xsym = sym21(xs)
      nket=nket+1
      if (nbas(ysym).eq.0) goto 710

c bra >= ket 

      if (iwx.eq.1) then
         nmb1=(nbraa-1)/isize_xx+1
         nmb2=(nketa-1)/isize_yy+1
         cnt1x_yx(nmb2,nmb1)=cnt1x_yx(nmb2,nmb1)+1
         yx1x=yx1x+1
       else
         nmb1=(nbraa-1)/isize_ww+1
         nmb2=(nketa-1)/isize_yy+1
         cnt1x_yw(nmb2,nmb1)=cnt1x_yw(nmb2,nmb1)+1
         yw1x=yw1x+1
      endif

710   continue
c
      if ( mul.gt.1 .or. hilev .lt. niot ) go to 700
      if(spnodd)niot = niot - 1
      return
30050 format(' 700',20i6)
      end
      subroutine onex_sp(
     & indsymbra, conftbra, indxbra, cibra,
     & sgmabra, indsymket,conftket,indxket, ciket,sgmaket,
     & intbuf, transf, lpbuf,  scr1,   scr2,
     & segsm,  cist, intask,segtype)



c*****
c     one external case
c
c     february 1988  new version by hans lischka, university of vienna
c     aodriven being implemented here. vp --- 18-apr-95
c      23-dec-98 direct FT calculation implemented (Michal Dallos)
c*****
c
c     indsym   gives the symmetry of the internal walk
c     conft    ci vector offset number for valid walks
c     indxyz   index vector for y and z paths
c     ciyz,sigmyz ci and sigma vector for y and z walks
c     index1   index vector for segment 1
c     ci1      ci vector for segment 1
c     sigm1    sigma vector for segment 1
c     intbuf   buffer for two electron integrals
c     transf   lin. comb. of integrals
c     lpbuf    buffer for formula tape
c     scr1,scr2 scratch arrays
c     ipth1(2) start number for internal paths for segments 1 and 2
c     segsm(2) number of internal walks in segments 1 and 2
c     cist(2)   starting number for the ci vector in segments 1 and 2
c     first=0  first call (include yz loops)
c          =1  subsequent call (omit yz loops)
c     itask=  1 (yz) 2(xy) 3(wy)
c
         IMPLICIT NONE

C====>Begin Module ONEX_SP                File onex_sp.f                
C---->Makedcls Options: All variables                                   
C
C     External functions
C
      EXTERNAL            DDOT_WR
      EXTERNAL            RL
C
      INTEGER             RL
C
      REAL*8              DDOT_WR
C
C     Parameter variables
C
      INTEGER             MAXBF
      PARAMETER           (MAXBF = 35000)
      INTEGER             NVMAX
      PARAMETER           (NVMAX = 40)
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
      INTEGER             MAXSYM
      PARAMETER           (MAXSYM = 8)
      INTEGER             NROWMX
      PARAMETER           (NROWMX = 1023)
      INTEGER             NIMOMX
      PARAMETER           (NIMOMX = 128)
      INTEGER             NFILMX
      PARAMETER           (NFILMX = 55)
      INTEGER             MXUNIT
      PARAMETER           (MXUNIT = 99)
      INTEGER             MXINFO
      PARAMETER           (MXINFO = 20)
      INTEGER             MULTMX
      PARAMETER           (MULTMX = 19)
      INTEGER             NMULMX
      PARAMETER           (NMULMX = 9)
C
      REAL*8              TWO
      PARAMETER           (TWO = 2.0D0)
C
C     Argument variables
C
      INTEGER             CIST(2),     CONFTBRA(*), CONFTKET(*)
      INTEGER             INDSYMBRA(*),             INDSYMKET(*)
      INTEGER             INDXBRA(*),  INDXKET(*),  INTASK
      INTEGER             SEGSM(2),    SEGTYPE(2)
C
      REAL*8              CIBRA(*),    CIKET(*),    INTBUF(*)
      REAL*8              LPBUF(*),    SCR1(*),     SCR2(*)
      REAL*8              SGMABRA(*),  SGMAKET(*),  TRANSF(*)
C
C     Local variables
C
      INTEGER             BRA,         CLEV,        CODEM,       CODEP
      INTEGER             DIFF,        FIN,         I,           II
      INTEGER             IND1,        IND2,        INDI,        INDI2
      INTEGER             INDII,       INTST,       IS,          ITASK
      INTEGER             J1,          JS,          K1,          KET
      INTEGER             KLS,         KS,          L1,          LOOP8
      INTEGER             LOOP8_CAL,   LPST,        LS,          MAXBL
      INTEGER             MUL,         MXORB3,      NI3,         NMB1
      INTEGER             NMB1A,       NMB2,        NMB2A,       NTRYPR
      INTEGER             START,       TASKID,      TOTINT
C
      LOGICAL             SKIP,        WALK
C
      REAL*8              VALUE1,      WORD8
C
      INCLUDE 'cfiles.inc'
C
C     Equivalenced common variables
C
      INTEGER             FILINT,      FILLP
C
      INCLUDE 'counter.inc'
      INCLUDE 'csym.inc'
      INCLUDE 'dainfo.inc'
      INCLUDE 'data.inc'
      INCLUDE 'inf.inc'
      INCLUDE 'inf1.inc'
      INCLUDE 'inf3.inc'
      INCLUDE 'onex_stat.inc'
      INCLUDE 'ox.inc'
      INCLUDE 'sizes_stat.inc'
      INCLUDE 'solxyz.inc'
      INCLUDE 'special.inc'
C====>End Module   ONEX_SP                File onex_sp.f                
      equivalence (filint,nunits(9))
      equivalence (fillp,nunits(12))
C
      if (segtype(2).ne.1 .and. segtype(2).ne.2) then
        call bummer('onex_sp: second segment must be yz segment',0,2)
      endif
      call wzero(maxbf*3,valv,1)
      call izero_wr(maxbf,headv,1)
      call izero_wr(maxbf,icodev,1)
      call izero_wr(maxbf,xbtv,1)
      call izero_wr(maxbf,ybtv,1)
      call izero_wr(maxbf,yktv,1)
c
c
      loop8_cal = 1
      niot   = intorb
      sqrt2  = sqrt(two)
      intst  = bl4xt + bl2xt + 1
      indi   = 0
      indii  = 0
      ind2   = 0
      loop8  = 1
      totint = 0
      mxorb3 = mxorb * 3
      ket    = 0
      bra    = 0
      walk   = .false.
      value1 = 0.0d+00
      itask = max(1,intask-1)
c
c     # initialize necessary stack arrays for FT calculation.
c
      call istack
c
c   # onex_ft liefert mlppr=bra und mlp=ket zurueck !!!
c
      call onex_ft(.true.,indxbra,indxket,segsm,itask)
      maxbl = mxorb3 + bufszi

      maxbl = mxorb3 + bufszi
c
      do 10 l1 = 1, intorb
c
         ls = orbsym(l1)
         hilev = l1
c
         do 20 k1 = 1, l1
c
            ks    = orbsym(k1)
            kls   = mult(ks,ls)
c
            do 30 j1 = 1, k1
c
               js = orbsym(j1)
               is = mult(js,kls)
               isym = sym21(is)
               ni = nbas(isym)
               ni3 = ni * 3
               totint = totint + ni3
               ntrypr=itask
               indi = indii
               indii = indi+ni3
               indi1 = indi+1
               indi2 = indi+2
               if ( indii .le. ind2 ) go to 60
c
c              # read integrals into buffer.
c
               diff = ind2 - indi
               if ( diff .ne. 0 ) then
                  do 50 i = 1, diff
                     intbuf(i) = intbuf(indi+i)
50                continue
               endif
               ind1 = diff
               ind2 = ind1 + bufszi
               if ( ind2 .gt. maxbl ) then
                  write(0,20000)ind2,maxbl
                  call bummer( 'onex: (ind2-maxbl)=',
     &             (ind2-maxbl), faterr )
               endif
               start = ind1 + 1
c                 call diraccnew(
c    &             filint,        1,      intbuf(start), (bufszi),
c    &             dalen(filint), intst,  fin  )
               intst = intst + 1
               indi = 0
               indii = ni3
               indi1 = 1
               indi2 = 2
60             continue
c               write(6,30010)ind1,ind2,indi,indii,
c     &          isym,ni,l1,k1,j1,kls,intst
c               write(6,30020)(intbuf(i),i=1,indii)
c
c              # next internal loop
c
100            continue
c
c              #  word8 contains code,wtlphd,mlppr,mlp
c              #
c              #  mlppr   weight of the bra part of the loop
c              #  mlp     weight of the ket part of the loop
c              #  code   0  new set of indices (l,k,j)
c              #         1  end of record
c              #         2  new entry yz-xy,or xy-wy
c              #         3  val1*(ij/kl)+val2*(jk/il)
c              #         4  val1*(ij/kl)+val2*(ik/jl)
c              #         5  val1*(ik/jl)+val2*(jk/il)
c              #         6  loop 12
c              #         7  val*(ij/kl)
c              #         8  val*(ik/jl)
c              #         9  val*(jk/il)
c
c              #  ntrypr   1  yz
c              #           2  xy
c              #           3  wy
c
                code=icodev(loop8_cal)
                rwlphd=headv(loop8_cal)
                mlppr= ybtv(loop8_cal)
                mlp= yktv(loop8_cal)
               codep = code + 1
               go to (
     &          103, 105,  110,  106,
     &          106, 106,  106,  107,
     &          107, 107              ), codep
c
103            continue
c   #  code   0  new set of indices (l,k,j)
               loop8_cal = loop8_cal + 1
               go to 30
c
105            continue
c   #  code   1  new record of FT
c
                call onex_ft(.false.,indxbra,indxket,segsm,
     .                         itask)
               loop8_cal = 1
               go to 100
c
110            continue
c   #  code   2  new entry yz-xy,or xy-wy
c
               call bummer('onex_sp, invalid code',code,2)
               go to 100
c
106            continue
c
               value1 = valv(1,loop8_cal)
               value2 = valv(2,loop8_cal)
               loop8_cal = loop8_cal + 1
               go to 108
c
107            continue
c
               value1 = valv(1,loop8_cal)
               loop8_cal = loop8_cal + 1
c
108            continue
c
               if ( ntrypr.ne.itask) goto 100
               if ( ni .eq. 0 ) go to 100
ct             if ( iskip .eq. 0 .and. ntrypr .ne. 1 ) go to 100
c               write(6,30030)code,wtlphd,rwlphd,mlppr,mlp,
c     &          ntrypr,ipth1,segsum
c              # check for the availability of segments.
c
c

               codem = code - 2
               go to(
     &          120,  140,  160,  160,
     &          180,  200,  220        ), codem
c***********************************************************************
c
c              formation of linear combinations of integrals
c
c***********************************************************************
120            continue
c
c              # value1*(ij/kl)+value2*(jk/il)
c
               go to 250
140            continue
c
c              # value1*(ij/kl)+value2*(ik/jl)
c
               go to 250
160            continue
c
c              # value1*(ik/jl)+value2*(jk/il)
c
               go to 250
180            continue
c
c              # value1*(ij/kl)
c
               go to 250
200            continue
c
c              # value1*(ik/jl)
c
               go to 250
220            continue
c
c              # value1*(jk/il)
c
250            continue
               if ( ntrypr .gt. 1 ) go to 260
c***********************************************************************
c
c              zy
c
c***********************************************************************
600            continue

               if(spnodd)niot = niot + 1
               mul=0
               call upwkst( hilev,  niot, rwlphd, mlppr,  mlp, clev )
650            continue
             call upwlknew( ket, bra, walk, mul,segsm(2),segsm(1),skip)
               if(walk)then
                  if(spnodd)niot = niot - 1
                  go to 100
               else
                 if (skip) goto 610
               endif
c
c      everything must refer to segment boundaries!!!
c
               nmb1a=indxket(ket)
               if(nmb1a.lt.0)go to 610
               nmb2a=indxbra(bra)
               if(nmb2a.lt.0)go to 610
               yz1x=yz1x+1

         nmb1=(nmb1a-1)/isize_zz+1
         nmb2=(nmb2a-1)/isize_yy+1
         cnt1x_yz(nmb2,nmb1)=cnt1x_yz(nmb2,nmb1)+1


610            continue
               if ( mul.gt.1 .or. hilev .lt. niot ) go to 650
               if(spnodd)niot = niot - 1
               go to 100
c
c              # yx or yw
c
260            continue
                  segsum(1)=segsm(1)
                  segsum(2)=segsm(2)
c
              call oneyxw(
     &          indsymbra,indsymket, conftbra,conftket,
     &          indxbra, indxket,cibra,sgmabra, ciket, sgmaket,
     &          transf,    intbuf, scr1,   scr2, (ntrypr-1)  )

               go to 100
30          continue
20       continue
10    continue
      if ( totint .ne. n1ext ) then
         write(0,20010) totint, n1ext
         call bummer( 'onex: (totint-n1ext)=', (totint-n1ext), faterr )
      endif
C HPM   return
c       call _f_hpm_stop_(35,1408)
C HPM
      return
20000 format(' insufficient space for two electron integrals in onex'
     & /' space wanted ',i6,'  space available ',i6)
20010 format(' onex nmb.of int. calculated',i6,' from sort',i6)
c30040 format(' 600',20i6)
30005 format(' 30',20i6)
30020 format(1x,10e13.6)
c30025 format(' code',2i6)
      end
      subroutine onex_ft(initial,indxbra,indxket,segsm,
     &  itask)

c
c  construct the one-external formula tape.
c  FT calculation for the twoex subroutine
c  modified by Michal Dallos XII/1998
c
c  code  loop type     description
c  ----  ---------     ----------------
c
c   0  -- new level --
c   1  -- new record --
c   2  -- new vertex --
c
c   3       1ab         (yz,xy,wy)
c           6ab         (yz,xy,wy)
c           8ab         (yz,xy,wy)
c   4       2a'b'       (yz,xy,wy)
c   5       3ab         (yz,xy,wy)
c   6       12bc        (yz,xy,wy)
c   7   (not used)
c   8       2b'         (yz,xy,wy)
c           7'          (yz,xy,wy)
c           8b          (yz,xy,wy)
c           10          (yz,xy,wy)
c   9       1b          (yz,xy,wy)
c           6b          (yz,xy,wy)
c
c
c  itask  1 : yz
c         2 : xy
c         3 : wy
c


         IMPLICIT NONE

C====>Begin Module ONEX_FT                File onex_ft.f                
C---->Makedcls Options: All variables                                   
C
C     Parameter variables
C
      INTEGER             NROWMX
      PARAMETER           (NROWMX = 2**10-1)
      INTEGER             NIMOMX
      PARAMETER           (NIMOMX = 128)
      INTEGER             MAXBF
      PARAMETER           (MAXBF = 35000)
      INTEGER             NFILMX
      PARAMETER           (NFILMX = 55)
      INTEGER             LBFT
      PARAMETER           (LBFT = 2048)
      INTEGER             MAXTASK
      PARAMETER           (MAXTASK = 16384)
C
C     Argument variables
C
      INTEGER             INDXBRA(*),  INDXKET(*),  ITASK
      INTEGER             SEGSM(2)
C
      LOGICAL             INITIAL
C
C     Local variables
C
      CHARACTER*2         CTASK(3)
C
      INTEGER             BTAILX(3),   KTAILX(3),   TASKID
C
      LOGICAL             BREAK2
C
      REAL*8              FTBUF(LBFT)
C
      INCLUDE 'cdb.inc'
      INCLUDE 'cfiles.inc'
C
C     Equivalenced common variables
C
      INTEGER             NLIST
C
      INCLUDE 'cftb.inc'
      INCLUDE 'cli.inc'
      INCLUDE 'cprt.inc'
      INCLUDE 'cst.inc'
      INCLUDE 'cvtran.inc'
      INCLUDE 'inf3.inc'
      INCLUDE 'lcontrol.inc'
      INCLUDE 'special.inc'
      INCLUDE 'tasklst.inc'
      INCLUDE 'timerlst.inc'
C====>End Module   ONEX_FT                File onex_ft.f                

      equivalence (nlist,nunits(1))


      data    btailx/2,3,4/, ktailx/1,2,2/
      data    ctask/'YZ','XY','WY'/

      ibf = 0
       call ptimer('tloop','resm',1)



      if (initial) jumpinposition=0
      go to  (990,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15)
     & (jumpinposition+1)
c
15    call bummer('onex_ft invalid jumpinposition',jumpinposition+1,2)
12    call bummer('onex_ft invalid jumpinposition',jumpinposition+1,2)
 8    call bummer('onex_ft invalid jumpinposition',jumpinposition+1,2)
 4    call bummer('onex_ft invalid jumpinposition',jumpinposition+1,2)
990   continue
c
      nintl = 3
      mo(0) = 0
      nloop = 0
      qdiag = .false.
c
      lmo = 1
400   continue ! lmo = 1, niot
c
         kmo = 1
         if (kmo .gt. (lmo-1)) go to 201
200      continue ! kmo = 1, (lmo-1)
c
            jmo = 1
            if (jmo .gt. (kmo-1)) go to 101
100         continue ! jmo = 1, (kmo-1)
c**********************************************************************
c              j<k<l
c**********************************************************************
               if(qprt.ge.5)write(nlist,6100)lmo,kmo,jmo
               nmo = 3
               mo(1) = jmo
               mo(2) = kmo
               mo(3) = lmo
c
               ver = itask
                  btail = btailx(ver)
                  ktail = ktailx(ver)
c
                  if (iskip.eq.0) then
                   if (.not.((btail.eq.1).or.(ktail.eq.1))) go to 5
                  endif
c
c                 # 1ab loops:
c
                  numv = 2
                  vtpt = 2
                  code(1) = 3
                  code(2) = 9
1                 call loopcalc( btail, ktail, st1, db1, 8, 2,
     &                indxbra, indxket, segsm)

                  if (break) then
                  jumpinposition=1
                  goto 999
                  endif

c
c                 # 2a'b' loops:
c
                  numv = 2
                  vtpt = 2
                  code(1) = 4
                  code(2) = 8
2                 call loopcalc( btail, ktail, st2, db2, 8, 2,
     &                indxbra, indxket, segsm)
                  if (break) then
                  jumpinposition=2
                  goto 999
                  endif
c
c                 # 3ab loops:
c
                  numv = 2
                  vtpt = 3
                  code(1) = 5
                  code(2) = 0
3                 call loopcalc( btail, ktail, st3, db3, 8, 2,
     &                indxbra, indxket, segsm)
                  if (break) then
                  jumpinposition=3
                  goto 999
                  endif
c
c    #         put code 0
5              call wcode_dir(ibf,0,icodev,5,break2)
               if (break2) goto 999

               jmo = jmo + 1
               if (jmo .le. (kmo-1)) go to 100
101            continue
c**********************************************************************
c           j=k<l
c**********************************************************************
            if(qprt.ge.5)write(nlist,6100)lmo,kmo,kmo
            nmo = 2
            mo(1) = kmo
            mo(2) = lmo
c
            ver = itask
               btail = btailx(ver)
               ktail = ktailx(ver)
c
               if (iskip.eq.0) then
                if (.not.((btail.eq.1).or.(ktail.eq.1))) go to 9
               endif
c
c              # 6ab loops:
c
               numv = 2
               vtpt = 1
               code(1) = 3
               code(2) = 9
6              call loopcalc( btail, ktail, st6, db6, 6, 2,
     &                indxbra, indxket, segsm)
               if (break) then
               jumpinposition=6
               goto 999
               endif

c
c              # 7' loops:
c
               numv = 1
               vtpt = 1
               code(1) = 8
7              call loopcalc( btail, ktail, st7, db7, 6, 2,
     &                indxbra, indxket, segsm)
             if (break) then
               jumpinposition=7
               goto 999
               endif
c
c    #         put code 0
9              call wcode_dir(ibf,0,icodev,9,break2)
               if (break2) goto 999

            kmo = kmo + 1
            if (kmo .le. (lmo-1)) go to 200
201         continue
c
         jmo = 1
         if (jmo .gt. (lmo-1)) go to 301
300      continue ! jmo = 1, (lmo-1)
c**********************************************************************
c           j<k=l
c**********************************************************************
            if(qprt.ge.5)write(nlist,6100)lmo,lmo,jmo
            nmo = 2
            mo(1) = jmo
            mo(2) = lmo
c
            ver = itask
               btail = btailx(ver)
               ktail = ktailx(ver)
c
               if (iskip.eq.0) then
                if (.not.((btail.eq.1).or.(ktail.eq.1))) go to 13
               endif
c
c              # 8ab loops:
c
               numv = 2
               vtpt = 2
               code(1) = 3
               code(2) = 8
10             call loopcalc( btail, ktail, st8, db8, 6, 2,
     &                indxbra, indxket, segsm)
               if (break) then
               jumpinposition=10
               goto 999
               endif

c
c              # 10 loops:
c
               numv = 1
               vtpt = 1
               code(1) = 8
11             call loopcalc( btail, ktail, st10, db10, 6, 2,
     &                indxbra, indxket, segsm)
               if (break) then
               jumpinposition=11
               goto 999
               endif

c
c    #         put code 0
13          call wcode_dir(ibf,0,icodev,13,break2)
            if (break2) goto 999
            jmo = jmo + 1
            if (jmo .le. (lmo-1)) go to 300
301         continue
c**********************************************************************
c        j=k=l
c**********************************************************************
         if(qprt.ge.5)write(nlist,6100)lmo,lmo,lmo
         nmo = 1
         mo(1) = lmo
c
         ver = itask
            btail = btailx(ver)
            ktail = ktailx(ver)
c
            if (iskip.eq.0) then
             if (.not.((btail.eq.1).or.(ktail.eq.1))) go to 16
            endif
c
c           # 12bc loops:
c
            numv = 2
            vtpt = 1
            code(1) = 6
            code(2) = 0
14          call loopcalc( btail, ktail, st12, db12(1,0,2), 4, 2,
     &                indxbra, indxket, segsm)
            if (break) then
            jumpinposition=14
            goto 999
            endif
c
c    #         put code 0
16       call wcode_dir(ibf,0,icodev,16,break2)
         if (break2) goto 999
         lmo = lmo + 1
         if (lmo .le. niot) go to 400
c
      icodev(ibf) = 0
      jumpinposition = 0
c
c     if (nlist.gt.0) write(nlist,6010) nloop, ctask(itask)
      loopcnt(currtsk)=nloop
6010  format(' one-external ft construction complete.',
     + i12,' loops of task ',a2,' constructed.')
c999   call timer("onexft",5,times(11),6)
 999  call ptimer('tloop','susp',1)
C HPM   return
c       call _f_hpm_stop_(36,1959)
C HPM
      return
6100  format(' int3:  lmo,kmo,jmo = ',3i4)
      end
      subroutine allin_sp(
     & reflst, indsymbra, conftbra,
     & indxbra, cibra, sigmbra,
     & indsymket,conftket,indxket,ciket,sigmket,
     & intbuf, lpbuf,  segsm,
     & href, itask ,segtype,cist)


c*****
c     all internal case
c     one segment pair only
c     segment pair type is steered through JUMP
c     itask=1  z/z
c           2  y/y
c           3  x/x
c           4  w/w
c
c     modified 03/09/90  to clean w/x segment logic and allow 0
c                        segments. -eas
c     modified 08/20/89  to remove jpthz common block (eas)
c     new version february 1988
c     by
c     hans lischka, university of vienna
c     23-dec-98 direct FT calculation implemented (Michal Dallos)
c*****
c
c     href     h matrix in reference space (for iskip=-2 only)
c     reflst   integer list of zpath references (0=nonref,i=ith ref)
c     indsym   gives the symmetry of the internal walk
c     conft    ci vector offset number for valid walks
c     indxyz   index vector for y and z paths
c     ciyz,sigmyz ci and sigma vector for y and z walks
c     index1   index vector for segment 1
c     index2   index vector for segment 2
c     ci1      ci vector for segment 1
c     ci2      ci vector for segment 2
c     sigm1    sigma vector for segment 1
c     sigm2    sigma vector for segment 2
c     intbuf   buffer for two electron integrals
c     lpbuf    buffer for formula tape
c     ipth1(2) start number for internal paths for segments 1 and 2
c     segsm(2) number of internal walks in segments 1 and 2
c
         IMPLICIT NONE
C====>Begin Module ALLIN_SP               File allin_sp.f               
C---->Makedcls Options: All variables                                   
C
C     External functions
C
      EXTERNAL            RL
C
      INTEGER             RL
C
C     Parameter variables
C
      INTEGER             MAXBF
      PARAMETER           (MAXBF = 35000)
      INTEGER             NROWMX
      PARAMETER           (NROWMX = 1023)
      INTEGER             MAXINT
      PARAMETER           (MAXINT = 128)
      INTEGER             NVMAX
      PARAMETER           (NVMAX = 40)
      INTEGER             MAXSYM
      PARAMETER           (MAXSYM = 8)
      INTEGER             NIMOMX
      PARAMETER           (NIMOMX = 128) 
      INTEGER             NFILMX
      PARAMETER           (NFILMX = 55)
      INTEGER             MXUNIT
      PARAMETER           (MXUNIT = 99)
C
      REAL*8              ZERO
      PARAMETER           (ZERO = 0D0)
C
C     Argument variables
C
      INTEGER             CIST(2),     CONFTBRA(*), CONFTKET(*)
      INTEGER             INDSYMBRA(*),             INDSYMKET(*)
      INTEGER             INDXBRA(*),  INDXKET(*),  ITASK
      INTEGER             REFLST(*),   SEGSM(2),    SEGTYPE(2)
C
      REAL*8              CIBRA(*),    CIKET(*),    HREF(*)
      REAL*8              INTBUF(*),   LPBUF(*),    SIGMBRA(*)
      REAL*8              SIGMKET(*)
C
C     Local variables
C
      INTEGER             BUFSZC,      CASE12(MAXINT*(MAXINT+1)/2)
      INTEGER             CODE,        CODEM,       CODEP,       FIN
      INTEGER             I,           ICOUNT,      IDUM,        IINT
      INTEGER             IINT1,       IINT2,       IMD,         IND
      INTEGER             INDI(MAXINT*(MAXINT+1)/2)
      INTEGER             INDK(MAXINT*(MAXINT+1)/2),             INDONE
      INTEGER             INTST,       IREM,        ISGEXC,      LOOP8
      INTEGER             LOOP8_CAL,   LPST,        NSYM,        TASKID
      INTEGER             TOTINT
C
      LOGICAL             LSTSEG
C
      REAL*8              HINT,        VALUE1,      VALUE2,      VALUE3
      REAL*8              WORD8
C
      INCLUDE 'allin1.inc'
      INCLUDE 'cfiles.inc'
C
C     Equivalenced common variables
C
      INTEGER             FILINT,      FILLP
C
      INCLUDE 'dainfo.inc'
      INCLUDE 'inf.inc'
      INCLUDE 'inf3.inc'
      INCLUDE 'special.inc'
C====>End Module   ALLIN_SP               File allin_sp.f               
C
      equivalence (filint,nunits(9))
      equivalence (fillp,nunits(12))

C HPM  subroutine_start
c       call _f_hpm_start_(37,2265, "pciudg5.f","allin_sp")
C HPM
      cist1=cist(1)
      cist2=cist(2)
      segsm1=segsm(1)
      segsm2=segsm(2)

c     write(6,*) '================== allin_sp ======================='
c     write(6,*) 'ipth1,segsm,cist=',ipth1,segsm,cist1,cist2
c     write(6,*) 'index1,index2'
c     call iwritex(index1,min(10,segsm(1)))
c     call iwritex(index2,min(10,segsm(2)))

      call wzero(maxbf*3,valv,1)
      call izero_wr(maxbf,headv,1)
      call izero_wr(maxbf,icodev,1)
      call izero_wr(maxbf,xbtv,1)
      call izero_wr(maxbf,ybtv,1)
      call izero_wr(maxbf,yktv,1)
c
c     allins requires ci1<ci2
c     to be fixed soon
c

      code   = 0
      value1 = zero
      value2 = zero
      value3 = zero
      hint   = zero
      loop8_cal = 1
c
c     # initialize necessary stack arrays for FT calculation.
c
      call istack
c
c
      intst = bl4xt + bl2xt + bl1xt + 1
c
      bufszc=bufszi
c
c     read first block of integrals
c
c        call diraccnew(
c    &    filint,        1,      intbuf(1), (bufszi),
c    &    dalen(filint), intst,  fin  )
         intst=intst+1
       call allin_ft(.true.,indxbra,indxket,segsm,
     &  itask)

      lpst=lpst+1
      iint=1
      iint1=iint+1
      iint2=iint+2
      loop8=1

          jump=itask

      totint=1
100   continue
c
c     formula tape
c
c     word8 contains code,wtlphd,mlppr,mlp
c
c     mlppr    weight of the bra part of the loop
c     mlp      weight of the ket part of the loop
c
c     code   1   new record
c            3   new level(new integral)
c            4   val1*(ij/kl)+val2(jk/il)
c            5   val1*(ij/kl)+val2*(ik/jl)
c            6   val1*(ik/jl)+val2*(jk/il)
c            7   val*(ij/kl)
c            8   val*(ik/jl)
c            9   val*(jk/il)
c           10   val1*(ii/il)+val2*(il/ll)+val3*(i/h/l)
c
c     jump   1   z
c            2   y
c            3   x
c            4   w
c
c
       code=icodev(loop8_cal)
       rwlphd=headv(loop8_cal)
       mlppr= ybtv(loop8_cal)
       mlp= yktv(loop8_cal)
       hilev = hltv(loop8_cal)
c
c     write(6,30015)code,mlppr,mlp,wtlphd,jump
c     if (code.gt.3)
c     write(6,*) 'allin_sp:code,jump=',code,jump,itask,loop8
      codep = code + 1

      go to (10000,110,120,300,130,130,130,140,140,140,150),
     + codep
110   continue
c
c     read new block from formula tape
c
       call allin_ft(.false.,indxbra,indxket,segsm,
     .  itask)

      lpst=lpst+1
      loop8_cal = 1
      go to 100

120   continue
      call bummer('invalid code in allin_sp:',code,2)
300   continue
      loop8_cal = loop8_cal + 1
c
c     new set of internal indices i,j,k,l
c
      iint=iint+3
      totint=totint+3
      if(totint.gt.n0ext)go to 10000
      jump= itask
      iint2=iint+2
      if(iint2.le.bufszc)go to 310
c
c     read new block of integrals
c
      irem=mod(bufszc,3)
      if(irem.eq.0) go to 305
      do 304 i=1,irem
         intbuf(i)=intbuf(iint-1+i)
304   continue
c305   call diraccnew(
c     & filint,        1,      intbuf(1+irem), (bufszi),
c     & dalen(filint), intst,  fin  )
305   continue
      bufszc=bufszi+irem
      intst=intst+1
      iint=1
      iint2=iint+2
c
310   continue
c
      iint1=iint+1
      go to 100
c
130   continue
c
      loop8_cal = loop8_cal + 1
      go to 160
c
140   continue
c
      loop8_cal = loop8_cal + 1
      go to 160
c
150   continue
c
      loop8_cal = loop8_cal + 1
c
160   continue
c
c     write(6,32000)code,wtlphd,mlppr,mlp,jump
      if(iskip.le.0.and.jump.ne.1)go to 100
cmd
c     if ((iskip.eq.2).and.(.not.flag)) go to 100
cmd
c     if(jump.le.2.and.first.gt.0)go to 100
      codem=code-3
      go to (340,360,380,400,410,420,430),codem
c***********************************************************************
c
c     linear combinations of integrals
c
c***********************************************************************
c
340   continue
c
c     value1*(ij/kl)+value2*(jk/il)
c
      go to 500
360   continue
c
c     value1*(ij/kl)+value2*(ik/jl)
c
      go to 500
380   continue
c
c     value1*(ik/jl)+value2*(jk/il)
c
      go to 500
400   continue
c
c     value1*(ij/kl)
c
      go to 500
410   continue
c
c     value1*(ik/jl)
c
      go to 500
420   continue
c
c     value1*(jk/il)
c
      go to 500
430   continue
c
c     value1*(ii/il)+value2*(il/ll)+value3*(i/h/l)
c
500   continue


c
c  settings in common allin1 are set in allin
c  but we must check for segment availability


       call allins(reflst,indsymbra,conftbra,indxbra,
     +   cibra,sigmbra,indsymket,conftket,indxket,
     .   ciket,sigmket,hint,href,intorb)
c
c     go to next loop
c
      go to 100
10000 continue
C HPM   return
c       call _f_hpm_stop_(37,2506)
C HPM
      return
30015 format(' code',20i4)
32000 format(' code',20i6)
      end
      subroutine allin_ft(initial,indxbra,indxket,segsm,
     & itask)

c
c  construct the all-internal formula tape.
c  modified by Michal Dallos XII/1998
c
c  code  loop type      description
c  ----  ---------      ------------
c
c   1  -- new record --
c   2  -- new vertex --
c   3  -- new level --
c
c   4       1ab         (z,y,x,w)
c           4ab         (z,y,x,w)
c           6ab         (z,y,x,w)
c           8ab         (z,y,x,w)
c           12ac        (z,y,x,w)
c   5       2a'b'       (z,y,x,w)
c   6       3ab         (z,y,x,w)
c           12bc        (z,y,x,w)
c   7                   (z,y,x,w)
c   8       2b'         (z,y,x,w)
c           7'          (z,y,x,w)
c           10          (z,y,x,w)
c           11b         (z,y,x,w)
c           13          (z,y,x,w)
c   9       1b          (z,y,x,w)
c           4b          (z,y,x,w)
c           4b'         (z,y,x,w)
c           5           (z,y,x,w)
c           6b          (z,y,x,w)
c           8b          (z,y,x,w)
c           12c         (z,y,x,w)
c  10       12abc       (z,y,x,w)
c

c
c  itask = 1   z/z
c          2   y/y
c          3   x/x
c          4   w/w
c
c
         IMPLICIT NONE
C
C====>Begin Module ALLIN_FT               File allin_ft.f               
C---->Makedcls Options: All variables                                   
C
C     Parameter variables
C
      INTEGER             NROWMX
      PARAMETER           (NROWMX = 2**10-1)
      INTEGER             NIMOMX
      PARAMETER           (NIMOMX = 128)
      INTEGER             MAXBF
      PARAMETER           (MAXBF = 35000)
      INTEGER             NFILMX
      PARAMETER           (NFILMX = 55)
      INTEGER             LBFT
      PARAMETER           (LBFT = 2048)
      INTEGER             MAXSYM
      PARAMETER           (MAXSYM = 8)
      INTEGER             MAXTASK
      PARAMETER           (MAXTASK = 16384)
C
C     Argument variables
C
      INTEGER             INDXBRA(*),  INDXKET(*),  ITASK
      INTEGER             SEGSM(2)
C
      LOGICAL             INITIAL
C
C     Local variables
C
      INTEGER             TASKID
C
      LOGICAL             BREAK2
C
      REAL*8              FTBUF(LBFT)
C
      INCLUDE 'cdb.inc'
      INCLUDE 'cfiles.inc'
C
C     Equivalenced common variables
C
      INTEGER             NLIST
C
      INCLUDE 'cftb.inc'
      INCLUDE 'cli.inc'
      INCLUDE 'cprt.inc'
      INCLUDE 'cst.inc'
      INCLUDE 'csym.inc'
      INCLUDE 'cvtran.inc'
      INCLUDE 'drt.inc'
      INCLUDE 'inf3.inc'
      INCLUDE 'lcontrol.inc'
      INCLUDE 'special.inc'
      INCLUDE 'tasklst.inc'
      INCLUDE 'timerlst.inc'
C====>End Module   ALLIN_FT               File allin_ft.f               
      equivalence (nlist,nunits(1))

c

C HPM  subroutine_start
c       call _f_hpm_start_(38,2855, "pciudg5.f","allin_ft")
C HPM
      ibf = 0
      call ptimer('tloop','resm',1)
c     call timer("allinft",6,times(10),6)


      if (initial) jumpinposition=0
      go to  (990,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,
     & 20,21,22,23,24,25) (jumpinposition+1)
c
4     call bummer ('invalid jumpinposition',jumpinposition+1,2)
9     call bummer ('invalid jumpinposition',jumpinposition+1,2)
13    call bummer ('invalid jumpinposition',jumpinposition+1,2)
17    call bummer ('invalid jumpinposition',jumpinposition+1,2)
21    call bummer ('invalid jumpinposition',jumpinposition+1,2)
24    call bummer ('invalid jumpinposition',jumpinposition+1,2)
990   continue
c
      nintl = 4
      mo(0) = 0
      nloop = 0
      qdiag = .false.
c
      lmo = 2
800   continue ! lmo = 2, niot
         llev = lmo-1
         lsym = arcsym(1,llev)
c
         kmo = 1
       if (kmo.gt.(lmo-1)) go to 401
400      continue ! kmo = 1, (lmo-1)
            klev = kmo-1
            klsym = mult(arcsym(1,klev),lsym)
c
            jmo = 1
          if (jmo .gt. (kmo-1)) go to 201
200         continue ! jmo = 1, (kmo-1)
               jlev = jmo-1
               jklsym = mult(arcsym(1,jlev),klsym)
c
               imo = 1
             if ( imo .gt. (jmo-1)) go to 101
100            continue ! imo = 1, (jmo-1)
                  ilev = imo-1
                  if(arcsym(1,ilev).ne.jklsym)go to 102
c***********************************************************
c                 i<j<k<l loops:
c***********************************************************
                  if(qprt.ge.5)write(nlist,6100)lmo,kmo,jmo,imo
                  nmo = 4
                  mo(1) = imo
                  mo(2) = jmo
                  mo(3) = kmo
                  mo(4) = lmo
c
                  ver = itask
c
                  if ((iskip.le.0).and.(ver.ne.1)) go to 5
c
c                    # 1ab loops:
c
                     numv = 2
                     code(1) = 4
                     code(2) = 9
                     vtpt = 2
1                    call loopcalc( ver, ver, st1, db1, 8, 0,
     &               indxbra, indxket,segsm)


                     if (break) then
                     jumpinposition=1
                     goto 999
                     endif
c
c                    # 2a'b' loops (bra-ket interchange from 2ab):
c
                     numv = 2
                     code(1) = 5
                     code(2) = 8
                     vtpt = 2
2                    call loopcalc( ver, ver, st2, db2, 8, 0,
     &               indxbra, indxket,segsm)
                     if (break) then
                     jumpinposition=2
                     goto 999
                     endif
c
c                    # 3ab loops:
c
                     numv = 2
                     code(1) = 6
                     code(2) = 0
                     vtpt = 3
3                    call loopcalc( ver, ver, st3, db3, 8, 0,
     &               indxbra, indxket,segsm)
                     if (break) then
                     jumpinposition=3
                     goto 999
                     endif
c
c    #         put code 3
5                 call wcode_dir(ibf,3,icodev,5,break2)
                  if (break2) goto 999
c
102               imo = imo + 1
                if ( imo .le. (jmo-1)) go to 100
101               continue
c
               imo = jmo
               if(klsym.ne.1)go to 202
c***********************************************************
c              i=j<k<l loops:
c***********************************************************
               if(qprt.ge.5)write(nlist,6100)lmo,kmo,jmo,imo
               nmo = 3
               mo(1) = jmo
               mo(2) = kmo
               mo(3) = lmo
c
               ver = itask
c
               if ((iskip.le.0).and.(ver.ne.1)) go to 10
c
c                 # 4ab loops:
c
                  numv = 2
                  code(1) = 4
                  code(2) = 9
                  vtpt = 2
6                 call loopcalc( ver, ver, st4, db4, 6, 0,
     &               indxbra, indxket,segsm)
                  if (break) then
                  jumpinposition=6
                  goto 999
                  endif
c
c                 # 4b' loops (bra-ket interchange from 4b):
c
                  numv = 1
                  code(1) = 9
                  vtpt = 1
7                 call loopcalc( ver, ver, st4p, db4p, 6, 0,
     &               indxbra, indxket,segsm)
                 if (break) then
                  jumpinposition=7
                  goto 999
                  endif
c
c                 # 5 loops:
c
                  numv = 1
                  code(1) = 9
                  vtpt = 1
8                 call loopcalc( ver, ver, st5, db5, 6, 0,
     &               indxbra, indxket,segsm)
                  if (break) then
                  jumpinposition=8
                  goto 999
                  endif
c
c    #         put code 3
10             call wcode_dir(ibf,3,icodev,10,break2)
               if (break2) goto 999
c
202            jmo = jmo + 1
             if (jmo .le. (kmo-1)) go to 200
c
201         continue
            jmo = kmo
            imo = 1
          if (imo .gt. (jmo-1)) go to 301
300         continue ! imo = 1, (jmo-1)
               ilev = imo-1
               if(arcsym(1,ilev).ne.lsym)go to 302
c***********************************************************
c              i<j=k<l loops:
c***********************************************************
               if(qprt.ge.5)write(nlist,6100)lmo,kmo,jmo,imo
               nmo = 3
               mo(1) = imo
               mo(2) = kmo
               mo(3) = lmo
c
               ver = itask
c
               if ((iskip.le.0).and.(ver.ne.1)) go to 14
c
c                 # 6ab loops:
c
                  numv = 2
                  code(1) = 4
                  code(2) = 9
                  vtpt = 1
11                call loopcalc( ver, ver, st6, db6, 6, 0,
     &               indxbra, indxket,segsm)
                  if (break) then
                  jumpinposition=11
                  goto 999
                  endif
c
c                 # 7' loops (bra-ket interchange from 7):
c
                  numv = 1
                  code(1) = 8
                  vtpt = 1
12                call loopcalc( ver, ver, st7, db7, 6, 0,
     &               indxbra, indxket,segsm)
                  if (break) then
                  jumpinposition=12
                  goto 999
                  endif
c
c    #         put code 3
14             call wcode_dir(ibf,3,icodev,14,break2)
               if (break2) goto 999
c
302            imo = imo + 1
             if (imo .le. (jmo-1)) go to 300
301         continue
c
            imo = jmo
c***********************************************************
c           i=j=k<l loops (deferred until i<j=k=l )
c***********************************************************
            kmo = kmo + 1
          if (kmo.le.(lmo-1)) go to 400
401      continue
c
         kmo = lmo
c
         jmo = 1
         if (jmo .gt. (kmo-1)) go to 601
600      continue ! jmo = 1, (kmo-1)
            jlev = jmo-1
            jklsym = arcsym(1,jlev)
c
          imo = 1
          if (imo .gt. (jmo-1)) go to 501
500         continue ! imo = 1, (jmo-1)
               ilev = imo-1
               if(arcsym(1,ilev).ne.jklsym)go to 502
c***********************************************************
c              i<j<k=l loops:
c***********************************************************
               if(qprt.ge.5)write(nlist,6100)lmo,kmo,jmo,imo
               nmo = 3
               mo(1) = imo
               mo(2) = jmo
               mo(3) = lmo
c
               ver = itask
c
               if ((iskip.le.0).and.(ver.ne.1)) go to 18
c
c                 # 8ab loops:
c
                  numv = 2
                  code(1) = 4
                  code(2) = 9
                  vtpt = 2
15                call loopcalc( ver, ver, st8, db8, 6, 0,
     &               indxbra, indxket,segsm)
                  if (break) then
                  jumpinposition=15
                  goto 999
                  endif
c
c                 # 10 loops:
c
                  numv = 1
                  code(1) = 8
                  vtpt = 1
16                call loopcalc( ver, ver, st10, db10, 6, 0,
     &               indxbra, indxket,segsm)
                  if (break) then
                  jumpinposition=16
                  goto 999
                  endif
c
c
c    #         put code 3
18             call wcode_dir(ibf,3,icodev,18,break2)
               if (break2) goto 999
502            imo = imo + 1
               if (imo .le. (jmo-1)) go to 500
c
501         continue
c
            imo = jmo
c***********************************************************
c           i=j<k=l loops:
c***********************************************************
            if(qprt.ge.5)write(nlist,6100)lmo,kmo,jmo,imo
            nmo = 2
            mo(1) = jmo
            mo(2) = lmo
c
            ver = itask
c
            if ((iskip.le.0).and.(ver.ne.1)) go to 22
c
c              # 11b loops:
c
               numv = 1
               code(1) = 8
               vtpt = 1
19             call loopcalc( ver, ver, st11, db11(1,0,2), 4, 0,
     &               indxbra, indxket,segsm)
               if (break) then
               jumpinposition=19
               goto 999
               endif
c
c              # 13 loops:
c
               numv = 1
               code(1) = 8
               vtpt = 2
20             call loopcalc( ver, ver, st13, db13, 4, 0,
     &               indxbra, indxket,segsm)
               if (break) then
               jumpinposition=20
               goto 999
               endif
c
c    #         put code 3
22          call wcode_dir(ibf,3,icodev,22,break2)
            if (break2) goto 999

            jmo = jmo + 1
          if (jmo .le. (kmo-1)) go to 600
601      continue
c
         jmo = kmo
c
         imo = 1
         if (imo .gt. (jmo-1)) go to 701
700      continue ! imo = 1, (jmo-1)
            ilev = imo-1
            if(arcsym(1,ilev).ne.lsym)go to 702
c***********************************************************
c           i<j=k=l loops:
c***********************************************************
            if(qprt.ge.5)write(nlist,6100)lmo,kmo,jmo,imo
            nmo = 2
            mo(1) = imo
            mo(2) = lmo
c
            ver = itask
c
            if ((iskip.le.0).and.(ver.ne.1)) go to 25
c
c              # 12abc loops:
c
               numv = 3
               code(1) = 9
               code(2) = 6
               code(3) = 4
               code(4) = 10
23             call loopcalc( ver, ver, st12, db12, 4, 0,
     &               indxbra, indxket,segsm)
               if (break) then
               jumpinposition=23
               goto 999
               endif
c
c    #         put code 3
25          call wcode_dir(ibf,3,icodev,25,break2)
            if (break2) goto 999
c
702         imo = imo + 1
            if (imo .le. (jmo-1)) go to 700
701      continue
c
         imo = jmo
c***********************************************************
c        i=j=k=l loops (none for off-diagonal contributions):
c***********************************************************
         lmo = lmo + 1
         if (lmo.le.niot) go to 800
c
cmd   call dump(1)
      icodev(ibf)=0
      jumpinposition=0
c
c     if (nlist.gt.0) write(nlist,6010)nloop,itask
      loopcnt(currtsk)=nloop
6010  format(' all-internal ft.',
     + i12,' loops (task ',i1,') constructed.')
c999   call timer("allinft",5,times(10),6)
999   call ptimer('tloop','susp',1)
c
C HPM   return
c       call _f_hpm_stop_(38,3249)
C HPM
      return
6100  format(' int4:  lmo,kmo,jmo,imo = ',4i4)
      end
      subroutine allins(
     & reflst, indsymbra, conftbra,
     & indxbra,  cibra,    sigmabra, indsymket,conftket,indxket,
     & ciket,    sigmaket,
     & hint,   href ,niot )

      implicit none
C====>Begin Module ALLINS                 File allins.f                 
C---->Makedcls Options: All variables                                   
C
C     Parameter variables
C
      INTEGER             MAXSYM
      PARAMETER           (MAXSYM = 8)
      INTEGER             MULTMX
      PARAMETER           (MULTMX = 19)
      INTEGER             NMULMX
      PARAMETER           (NMULMX = 9)
C
C     Argument variables
C
      INTEGER             CONFTBRA(*), CONFTKET(*), INDSYMBRA(*)
      INTEGER             INDSYMKET(*),             INDXBRA(*)
      INTEGER             INDXKET(*),  NIOT,        REFLST(*)
C
      REAL*8              CIBRA(*),    CIKET(*),    HINT,        HREF(*)
      REAL*8              SIGMABRA(*), SIGMAKET(*)
C
C     Local variables
C
      INTEGER             BRA,         CLEV,        I,           KET
      INTEGER             M1,          M1LP,        M1LPR,       MUL
      INTEGER             NIJ,         NMAX,        NMB1,        NMB1A
      INTEGER             NMB2,        NMB2A,       NMIN,        NY
      INTEGER             SUM_CNFW,    SUM_CNFX,    SUM_NBAS,    SY
      INTEGER             SYM,         TASKID
C
      LOGICAL             SKIP,        WALK
C
      INCLUDE 'allin1.inc'
      INCLUDE 'allint_stat.inc'
      INCLUDE 'counter.inc'
      INCLUDE 'data.inc'
      INCLUDE 'inf3.inc'
      INCLUDE 'sizes_stat.inc'
      INCLUDE 'solxyz.inc'
C====>End Module   ALLINS                 File allins.f                 

       sum_nbas=0
       do i=1,nmsym
        sum_nbas=sum_nbas+nbas(i)
        sum_cnfw=sum_cnfw+cnfw(i)
        sum_cnfx=sum_cnfx+cnfx(i)
       enddo



C

c
C HPM  subroutine_start
c       call _f_hpm_start_(39,3451, "pciudg5.f","allins")
C HPM
      bra  = 0
      ket  = 0
      walk = .false.
      mul  = 0
      if(spnodd)niot = niot + 1
      call upwkst(hilev,  niot,rwlphd, mlppr,  mlp, clev  )
c
260   continue
      call upwlknew( ket, bra, walk, mul,segsm2,segsm1,skip )
      if ( walk )then
         if(spnodd)niot = niot - 1
C HPM   return
c       call _f_hpm_stop_(39,3465)
C HPM
         return
      else
         if (skip) goto 520
      endif
         nmb1a=indxbra(bra)
         if(nmb1a.lt.0)go to 520
         nmb2a=indxket(ket)
         if(nmb2a.lt.0)go to 520
525      sy=indsymbra(nmb1a)
         sym=sym21(sy)
         go to (600,610,630,630),jump
600      continue
c***********************************************************************
c
c     walks passing through z
c
c***********************************************************************
         zz0x=zz0x+1
         nmb1=(nmb1a-1)/isize_zz+1
         nmb2=(nmb2a-1)/isize_zz+1

         cnt0x_zz(nmb1,nmb2)=cnt0x_zz(nmb1,nmb2)+1

         go to 520
c***********************************************************************
c
c     walks passing through y
c
c***********************************************************************
610      continue
         nmb1=(nmb1a-1)/isize_yy+1
         nmb2=(nmb2a-1)/isize_yy+1
         cnt0x_yy(nmb1,nmb2)=cnt0x_yy(nmb1,nmb2)+1
c    .                  int( 10.0d0*nbas(sym)/dble(sum_nbas))
         yy0x=yy0x+1
         go to 520
c***********************************************************************
c
c     walks passing through x or w
c
c***********************************************************************
630      continue
         if(jump.eq.3)then
         nmb1=(nmb1a-1)/isize_xx+1
         nmb2=(nmb2a-1)/isize_xx+1
            cnt0x_xx(nmb1,nmb2)=cnt0x_xx(nmb1,nmb2)+1
c    .         int(10.0d0*cnfx(sym)/dble(sum_cnfx))
            xx0x=xx0x+1
         else
         nmb1=(nmb1a-1)/isize_ww+1
         nmb2=(nmb2a-1)/isize_ww+1
            cnt0x_ww(nmb1,nmb2)=cnt0x_ww(nmb1,nmb2)+1 
c    .        int( 10.0d0*cnfw(sym)/dble(sum_cnfw))
            ww0x=ww0x+1
         endif
520   continue
      if(mul.gt.1 .or. hilev.lt.niot)go to 260
999   if(spnodd)niot = niot - 1
C HPM   return
c       call _f_hpm_stop_(39,3553)
C HPM
      return
30030 format(' 510',20i6)
      end
