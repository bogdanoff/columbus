!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
c      subroutine readda( unit, record, buffer, buflen )
cc
cc----------------------------------------------------------
cc this is a primitive i/o routine to be used by all higher
cc level routines requiring i/o of this type. machine i/o
cc peculiarites are meant to reside in these routines only.
cc----------------------------------------------------------
cc
cc routine name: readda
cc version: 1.2                        date: 12/17/90
cc author: eric stahlberg - ohio state university
cc purpose: this routine will read a given vector from a
cc          direct access file. the length of the vector is given
cc          in minimum necessary real*8 words to access the entire
cc          vector. io errors reported in encoded form: ioerr*100+unit
cc parameters:
cc     unit:   unit number of file to be read
cc     record: record number to be read
cc     buffer: buffer to read record into
cc     buflen: number of real*8 words to read from record
cc
c      implicit integer(a-z)
cc
cctm
cc     mapunit(origunit,sequenceunits)
cc     reqperunit (origunit)
c      integer maxunits,maxsequnits
c      parameter (maxunits=99,maxsequnits=4)
c      integer mapunit(maxunits,maxsequnits)
c      integer recperunit(maxunits),firstfreebl
c      character*30 filename(maxunits)
c      common /mapping/ mapunit,recperunit,firstfreebl,filename
cctm
c
c      integer unit,record,buflen
c      real*8 buffer ( buflen )
c*mdc*if ibmmvs
c*c these variables help in the logical record to physical
c*c record reduction
c*c     index*:  loop counter
c*c     nphyrc:  number of complete physical records
c*c     ibmlen:  real*8 words in 1/2 track of 3380 device
c*c     phyrec:  physical record buffer
c*c     phyrcn:  physical record number for this logical record
c*      integer index,index2,nphyrc,ibmlen,phyrcn
c*      parameter (ibmlen=2932)
c*      real*8 phyrec (ibmlen)
c*mdc*endif
cc
cc common block for unit information
c      integer         mxunit
c      parameter       (mxunit=99)
cc recnum retains next record to be accessed
cc reclen retains record length in open
cc seqwrt retains true if sequential write is to be enforced
c      integer         recnum(mxunit)
c      integer         reclen(mxunit)
c      logical         seqwrt(mxunit)
c      common  /esio88/   recnum, reclen, seqwrt
c      save    /esio88/
cc
cc     # bummer error types
c      integer wrnerr, nfterr, faterr
c      parameter (wrnerr=0, nfterr=1, faterr=2)
cc
c      integer ioerr
c      character*40 filnamn
cc
c*mdc*if debug
c*      print*,'readda: unit=,record=,buflen=',unit,record,buflen
c*mdc*endif
cc
cc
cc perform error checking
cc
c      if (unit.gt.mxunit.or.unit.le.0) goto 910
c      if (buflen.ne.reclen(unit)) goto 920
c      if (seqwrt(unit)) goto 930
c*mdc*if ibmmvs
c*       if (reclen(unit).gt.ibmlen) then
c*c recreate da logical record from smaller physical records
c*c number of complete physical records in the logical record
c*         nphyrc=buflen/ibmlen
c*c point to first physical record for this logical record
c*         if (nphyrc*ibmlen.eq.buflen) then
c*            phyrcn=(record-1)*nphyrc+1
c*         else
c*            phyrcn=(record-1)*(nphyrc+1)+1
c*         endif
c*c read complete records
c*         do 10 index=1,nphyrc
c*c
c*c read the physical record
c*c
c*         read (unit, rec = phyrcn+index-1, iostat=ioerr, err=900 )
c*     +            phyrec
c*c
c*c copy physical record to buffer
c*c
c*            do 20 index2=1,ibmlen
c* 20           buffer((index-1)*ibmlen+index2)=phyrec(index2)
c* 10     continue
c*c
c*c check for remainder of record
c*c
c*         if (nphyrc*ibmlen.ne.buflen) then
c*            read (unit,rec=phyrcn+nphyrc,
c*     +         iostat=ioerr, err=900 ) phyrec
c*            do 30 index2=1,ibmlen
c* 30           buffer(buflen-ibmlen+index2)=phyrec(index2)
c*         endif
c*       else
c*         read (unit,rec=record) buffer
c*       endif
c*mdc*elseif ibmcms
c*c ibmcms quirk: length of data xferred counted in 4 byte words
c*c
c*      len2=2*buflen
c*      ipos=(record-1)*len2+1
c*      call rwfil (unit, 1, ipos, buffer, len2, ierr)
c*      if (ierr.ne.0) then
c*        call bummer('readda: from rwfil, (ierr*100+unit)=',
c*     +  (ierr*100+unit),faterr)
c*      endif
c*mdc*else
cctm
cc      map from unit to correct sequnit
c       rectmp = (record-1)/recperunit(unit) +1
c       if (rectmp.gt.maxsequnits)
c     . call bummer('could not open more files',rectmp,2)
c       unitnew= mapunit(unit,rectmp )
cc################
c       if (unitnew.le.0) then
cc    must open new file
c*mdc*if decalpha sgipower
c*         lrecl=2*buflen
c*mdc*else
c         lrecl=8*buflen
c*mdc*endif
c      write(filnamn,'(a,a1,i1)')
c     . filename(unit)(1:fstrlen(filename(unit)))
c     .     ,'_',rectmp
c      if (firstfreebl.eq.99)
c     . call bummer('no more unit numbers available',firstfreebl,2)
c      mapunit(unit,rectmp)=firstfreebl
c      firstfreebl=firstfreebl+1
c      unitnew= mapunit(unit,rectmp )
c      write(6,*) 'info: opening file ',filnamn,' unit=',unitnew
c      open(unit=unitnew,access='direct',form='unformatted',status=
c     +  'unknown',file=filnamn,iostat=ioerr,recl=lrecl,err=900)
c       endif
cc#################
c       recnew = record - (rectmp-1)*recperunit(unit)
cc!!       write(6,*) rectmp,unit,unitnew,record,recnew
c       read (unitnew, rec = recnew ,iostat=ioerr, err=900) buffer
cc      call writex(buffer,buflen,'readda')
cctm
c*mdc*endif
cc point to next record
c 902  recnum(unit)=record+1
cc
c*mdc*if ( debug .and. osu)
c*      print *, (buffer(i), i=1, min(100,buflen) )
c*mdc*endif
c      return
c 899  format(' current reclen=',i10/
c     & ' opened reclen=',i10/' recnum=',i10)
cc
cc error conditions
c 900  continue
c      write(*,899) reclen(unit),buflen,record
c      call bummer('readda: i/o error,  (ioerr*100+unit)=',
c     & (ioerr*100+unit), faterr )
c 910  continue
c      call bummer('readda: invalid unit number ',unit,faterr)
c 920  continue
c      write(*,899) reclen(unit),buflen,record
c      call bummer('readda: buffer length gt record length ',unit,faterr
c 930  continue
c      write(*,899) reclen(unit),buflen,record
c      call bummer('readda: unit opened for seq. write',unit,faterr)
c      end
cc
c ----------------
c
        subroutine testread
c
        implicit none
        integer size,iostat,ioerr,err,faterr,rec
        parameter (size=32768)
        real*8 buffer(1:size)
c
        faterr=2
c
        read (38,rec=1,iostat=ioerr,err=900) buffer
        write (*,*) ' o.k. '
        return
900     call bummer ('readda: i/o error,  ioerr =',
     & ioerr, faterr )
c
        end
c
c -------------------

         subroutine cosmoinitial_mod(mtype,natoms,xyz,symgrp,nsym)
            implicit   none 
         integer maxat,natoms,dbglvl
      parameter(maxat=350)
      integer mtype(maxat),nsym,i,j
      real*8 xyz(3,maxat)
      character*3 symgrp
c
        integer icosin,icosa2,icosa3,icosa,icosco,icossu,
     . icosout,icolum
c
      common /cosmovarmod/icosin, icosa2, icosa3, icosa, icosco,
     & icossu, icosout, icolum
c
        save /cosmovarmod/
c
c     data icosin/30/
c     data icosa2/35/
c      data icosa3/38/
c     data icosa3/56/
c     data icosa/40/
c     data icosco/43/
c     data icossu/44/
c     data icosout/51/
c     data icolum/55/
c
       icosin=30
       icosa2=35
c      icosa3=38
       icosa3=56
       icosa=40
       icosco=43
       icossu=44
       icosout=51
       icolum=55
c
c
       natoms=1
       dbglvl = 0
       open(77,file='geom',status='old')
444    read(77,111,end=999) mtype(natoms),xyz(1,natoms)
     . ,xyz(2,natoms),xyz(3,natoms)
       natoms=natoms+1
       go to 444
999    continue
       close(unit=77,status='keep')
       natoms=natoms-1
111    format(6x,i2,2x,3f14.8)
       open(78,file='infofl',status='old')
       read(78,112) symgrp
        if(symgrp.eq.'c1') then
           nsym=0
        else if(symgrp.eq.'cs ') then
           nsym=1
        else if(symgrp.eq.'ci ') then
           nsym=2
        else if(symgrp.eq.'c2 ') then
           nsym=3
        else if(symgrp.eq.'d2 ') then
           nsym=4
        else if(symgrp.eq.'c2v') then
           nsym=5
        else if(symgrp.eq.'c2h') then
           nsym=6
        else if(symgrp.eq.'d2h') then
           nsym=7
        else
         call bummer('wrong sym. group',0,2)
        endif
112     format(a3)
c
c opening files needed for cosmo module
       open(icosin,file='cosmo.inp',form='formatted'
     ., err=333)
       open(icosa2,file='a2mat.tmp',form='unformatted'
     ., status='unknown',err=333)
       open(icosa3,file='a3mat.tmp',form='unformatted'
     ., status='unknown',err=333)
       open(icosa,file='amat',form='unformatted'
     ., status='unknown',err=333)
       open(icosco,file='cosurf'
     ., status='unknown',err=333)
       open(icossu,file='sude'
     ., status='unknown',err=333)
       open(icosout,file='out.cosmo',form='formatted'
     ., status='unknown',err=333)
       open(icolum,file='Col_cosmo',form='formatted'
     ., status='unknown',err=333)
c     ., status='new',err=333)
c
31    go to 334
c
333   call bummer('error while opening cosmo files',0,2)
334   continue
c initialization for cosmo module
          if(dbglvl.eq.1) then
      write(*,*)
      write(*,*)'natoms,nsym = ',natoms,nsym
      write(*,*)'mtype = '
      write(*,*)(mtype(i),i=1,natoms)
      write(*,*)
      write(*,*)'xyz = '
      write(*,*)
      write(*,*)((xyz(i,j),i=1,3),j=1,natoms)
      write(*,*)
          endif !dbglvl
         return
         end

c
c -----------------------------
c
      subroutine moread2(
     & unit,   flcod,  filerr, syserr,
     & mxtitl, ntitle, titles, afmt,
     & nsym,   nbfpsy, nmopsy, labels,
     & clen,   c  )
c
c subroutine: moread
c purpose   : universal interface read of mo coefficient file
c author    : eric stahlberg, osu chemistry (may 1989)
c
c version   : 2.1  (feb 1990)
c
c variable definitions
c
c *** in variables ***
c unit  : unit number of mo file
c flcod: file read operation code
c           10=read header only
c           20=read after header mocoef field
c           30=read after header energy field
c           40=read after header orbocc field
c           + block number to be read (0 means all blocks)
c         fopcod lt 10 implies all blocks and that fopcod
c mxtitl: maximum number of title cards which may be read
c clen  : available space in array c
c *** out variables ***
c filerr: mo file error number
c syserr: i/o system error - if filerr eq 1
c ntitle: number of title cards read
c titles: actual title cards
c afmt  : format with which values were read
c nsym  : number of irreps in point group
c nbfpsy: number of basis functions per mo of each irrep
c nmopsy: number of mos per irrep
c labels: symmetry labels for each irrep
c c     : coefficient array space
c
      implicit none 
      integer unit, flcod, filerr, syserr, mxtitl, ntitle, nsym
      integer nbfpsy(*), nmopsy(*), clen
      real*8 c(*)
      character*80 titles(*)
      character*80 afmt
      character*4 labels(*)
c
c     # local variables.
c
      integer totspc, index, iversn, i, isym, imo, ifld
      integer symblk, fopcod
      logical ldio
      real*8 cdummy
      character*6 inname, field(4), fldfmt, flname, inline
c
c     # abelian point groups only.
c
      integer    maxsym
      parameter (maxsym=8)
c
c     # latest file version
c
      integer    lstver
      parameter (lstver=2)
c
c     # last valid fop code.
c
      integer    lstfop
      parameter (lstfop=4)
c
      real*8     zero
      parameter( zero=0d0 )
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
c     # initialize some static constants.
c
      flname = 'mocoef'
      call allcap(flname)
c
      field(1) = 'header'
      field(2) = 'mocoef'
      field(3) = 'energy'
      field(4) = 'orbocc'
      do 10 ifld = 1, 4
         call allcap(field(ifld))
10    continue
      fldfmt  = ' (a6) '
c
c     # parse out symmetry block and real fopcod from fopcod.
c
      if (flcod.ge.10) then
         symblk = flcod - (flcod/10)*10
         fopcod = (flcod-symblk) / 10
      else
         fopcod = flcod
         symblk = 0
      endif
c
c     # check fopcod for valid value.
c
      if ((fopcod.gt.lstfop).or.(fopcod.lt.1)) then
         filerr=-5
         goto 1000
      endif
c
c     # check symblk for valid value.
c
      if ((symblk.gt.maxsym).or.(symblk.lt.0)) then
         filerr=-7
         goto 1000
      endif
c
c
c     # initialize state (assume success).
c
      filerr = 0
      syserr = 0
      ldio   = .false.
c
c     # address to begin filling retrieved data into c(:).
c
      index=0
c
c     # rewind file.
c
c      rewind (unit)
      rewind (unit,iostat=syserr)
c
c     # read initial line for parsing.
c
      read (unit,5001,iostat=syserr) iversn, inname
      if (syserr.ne.0) goto 1100
5001  format(i2,a6)
      call allcap(inname)
c
c     # (optional checks on file version number go here)
c
      if ( (iversn .gt. lstver)
     & .or. (inname .ne. flname) ) then
         filerr=-4
         goto 1000
      endif
c
c     # read field 1, header.
c
      read (unit,fmt=fldfmt,iostat=syserr) inline
      if (syserr.ne.0) goto 1100
      call allcap(inline)
      if (inline.ne.field(1)) then
         filerr=-14
         goto 1000
      endif
c
c     # read in title information.
c
      read (unit,*,iostat=syserr) ntitle
      if (syserr.ne.0) goto 1100
      if (ntitle.gt.mxtitl) then
         filerr=-2
         goto 1000
      endif
      do 100 i=1,ntitle
         read (unit,5002,iostat=syserr) titles(i)
100   continue
      if (syserr.ne.0) goto 1100
5002  format (a80)
c
c     # read in symmetry information (dimensions).
c
      read (unit,*,iostat=syserr) nsym
      if (syserr.ne.0) goto 1100
      if (symblk.gt.nsym) then
         filerr=-8
         goto 1000
      endif
      if ((nsym.gt.maxsym).or.(nsym.lt.1)) then
         filerr=-3
         goto 1000
      endif
c
c     # read in nbfpsy() and nmopsy().
c
      read (unit,*,iostat=syserr) (nbfpsy(i),i=1,nsym),
     + (nmopsy(i),i=1,nsym)
      if (syserr.ne.0) goto 1100
c
c     # check for adequate space.
c
      totspc = 0
      do 140 isym=1,nsym
c
c        # check to see if block is to be read.
c
         if (symblk.eq.0.or.isym.eq.symblk) then
            if (fopcod.le.2) then
               totspc=totspc+nmopsy(isym)*nbfpsy(isym)
            else
               totspc=totspc+nmopsy(isym)
            endif
         endif
140   continue
      if ((totspc.gt.clen).and.(fopcod.gt.1)) then
         filerr=-1
         goto 1000
      endif
c
c     # clear out c vector before reading data.
c
      do 141 i=1,clen
         c(i) = zero
141   continue
c
c     # read in label information
c
      read (unit,5003,iostat=syserr)(labels(i),i=1,nsym)
      if (syserr.ne.0) goto 1100
5003  format(8(a4,1x))
c
c     # test for symmetry and title read only and return if necessary.
c
      if (fopcod.eq.1) return
c
c     # loop until to get to next field
c     # drop out of loop if field is reached or eof detected
c
130   read (unit,fldfmt,end=137,iostat=syserr) inline
      if (syserr.ne.0) goto 1100
      call allcap(inline)
      if (inline.eq.field(fopcod)) goto 135
      goto 130
c
c     # end of file detected before field is matched.
c
137   continue
      filerr=-6
      goto 1000
c
c     # end of until loop.
135   continue
c
c     # field is matched, begin reading in c() at position index.
c
      read (unit,5004,iostat=syserr) afmt
5004  format(a80)
      if (syserr.ne.0) goto 1100
c
c     # parse format designation.
c
      if (afmt.eq.'(*)') then
         ldio=.true.
      else
         ldio=.false.
      endif
c
c     # read as coefficients only if fopcod eq 2.
c
      if (fopcod.eq.2) then
c
c        # read in coefficients
c
         do 110 isym=1,nsym
            do 120 imo=1,nmopsy(isym)
c
c              # read block if it is to be read.
c
               if (isym.eq.symblk.or.symblk.eq.0) then
                  if (ldio) then
                     read (unit,*,iostat=syserr)
     +                (c(index+i),i=1,nbfpsy(isym))
c                  write(*,*) 'MOREAD1'
                  else
                     read (unit,fmt=afmt,iostat=syserr)
     +                (c(index+i),i=1,nbfpsy(isym))
c                  write(*,*) 'MOREAD2'
                  endif
                  if (syserr.ne.0) goto 1100
                  index=index+nbfpsy(isym)
c                  write(*,*) 'MOREAD3'
               else
c
c                 # skip values which are not to be read.
c
                  if (ldio) then
                     read (unit,*,iostat=syserr)
     +                (cdummy,i=1,nbfpsy(isym))
c                  write(*,*) 'MOREAD4'
                  else
                     read (unit,fmt=afmt,iostat=syserr)
     +                (cdummy,i=1,nbfpsy(isym))
c                  write(*,*) 'MOREAD5'
                  endif
                  if (syserr.ne.0) goto 1100
c                  write(*,*) 'MOREAD6'
               endif
120         continue
110      continue
c
      elseif ((fopcod.eq.3).or.(fopcod.eq.4)) then
c
c        # read in other fields for fopcod 3,4.
c
         do 150 isym=1,nsym
            if (symblk.eq.0.or.symblk.eq.isym) then
c
c              # read if block is to be read.
c
               if (ldio) then
                  read (unit,*,iostat=syserr)
     +             (c(index+i),i=1,nmopsy(isym))
c                  write(*,*) 'MOREAD7'
               else
                  read (unit,fmt=afmt,iostat=syserr)
     +             (c(index+i),i=1,nmopsy(isym))
c                  write(*,*) 'MOREAD8'
               endif
               if (syserr.ne.0) goto 1100
               index=index+nmopsy(isym)
c                  write(*,*) 'MOREAD9'
            else
c
c              # skip values.
c
               if (ldio) then
                  read (unit,*,iostat=syserr)
     +             (cdummy,i=1,nmopsy(isym))
c                  write(*,*) 'MOREAD10'
               else
                  read (unit,fmt=afmt,iostat=syserr)
     +             (cdummy,i=1,nmopsy(isym))
c                  write(*,*) 'MOREAD11'
               endif
               if (syserr.ne.0) goto 1100
c                  write(*,*) 'MOREAD12'
            endif
150      continue
      endif
c
      return
c
c     # error handling portion.
c
c     # system errors.
1100  continue
      call bummer('moread: system file error',syserr,wrnerr)
      filerr=1
      return
c
c     # file errors.
1000  continue
      return
c
      end
c
c -------------
c
