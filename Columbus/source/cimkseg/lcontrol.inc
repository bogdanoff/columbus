!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
C
C     Common variables
C
      INTEGER             BTAIL,       CLEV,        ILEV,        IMO
      INTEGER             JKLSYM,      JLEV,        JMO
      INTEGER             JUMPINPOSITION,           KLEV,        KLSYM
      INTEGER             KMO,         KTAIL,       LLEV,        LMO
      INTEGER             LSYM,        VER
C
      LOGICAL             BREAK
C
      COMMON / LCONTROL/  JUMPINPOSITION,           IMO,         JMO
      COMMON / LCONTROL/  KMO,         LMO,         VER,         CLEV
      COMMON / LCONTROL/  BTAIL,       KTAIL,       LLEV,        KLEV
      COMMON / LCONTROL/  JLEV,        ILEV,        LSYM,        KLSYM
      COMMON / LCONTROL/  JKLSYM,      BREAK
C
