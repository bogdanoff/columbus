!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
C
C     Common variables
C
      INTEGER             C2EX0EX,     C3EX1EX,     CDG4EX,      CSFPRN
      INTEGER             DAVCOR,      FILELOC(10),  FINALV,      FINALW
      INTEGER             FRCSUB,      FROOT,       FTCALC,      IBKTHV
      INTEGER             IBKTV,       ICITHV,      ICITV,       IORTLS
      INTEGER             ISTRT,       IVMODE,      MAXSEG,      NBKITR
      INTEGER             NCOUPLE,     NITER,       NO0EX,       NO1EX
      INTEGER             NO2EX,       NO3EX,       NO4EX,       NODIAG
      INTEGER             NOLDHV,      NOLDV,       NRFITR,      NROOT
      INTEGER             NSEG0X(4),   NSEG1X(4),   NSEG2X(4)
      INTEGER             NSEG3X(4),   NSEG4X(4),   NSEGD(4)
      INTEGER             NSEGSO(4),   NTYPE,       NUNITV,      NVBKMN
      INTEGER             NVBKMX,      NVCIMN,      NVCIMX,      NVRFMN
      INTEGER             NVRFMX,      RTMODE,      VOUT
C
      REAL*8              CTOL,        ETOL(NVMAX), ETOLBK(NVMAX)
      REAL*8              LRTSHIFT
C
      COMMON / INDATA /   LRTSHIFT,    ETOLBK,      ETOL,        CTOL
      COMMON / INDATA /   NROOT,       NOLDV,       NOLDHV,      NUNITV
      COMMON / INDATA /   NBKITR,      NITER,       DAVCOR,      CSFPRN
      COMMON / INDATA /   IVMODE,      ISTRT,       VOUT,        IORTLS
      COMMON / INDATA /   NVBKMX,      IBKTV,       IBKTHV,      NVCIMX
      COMMON / INDATA /   ICITV,       ICITHV,      FRCSUB,      NVBKMN
      COMMON / INDATA /   NVCIMN,      MAXSEG,      NTYPE,       NRFITR
      COMMON / INDATA /   NVRFMX,      NVRFMN,      FROOT,       FTCALC
      COMMON / INDATA /   RTMODE,      NCOUPLE,     NO0EX,       NO1EX
      COMMON / INDATA /   NO2EX,       NO3EX,       NO4EX,       NODIAG
      COMMON / INDATA /   NSEGD,       NSEG0X,      NSEG1X,      NSEG2X
      COMMON / INDATA /   NSEG3X,      NSEG4X,      NSEGSO,      CDG4EX
      COMMON / INDATA /   C3EX1EX,     C2EX0EX,     FILELOC,     FINALV
      COMMON / INDATA /   FINALW
C
