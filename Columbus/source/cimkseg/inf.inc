!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
C
C     Common variables
C
      INTEGER             BUFSZI,      BUFSZL,      CONFYZ,      DIMCI
      INTEGER             INTORB,      LAMDA1,      MAXBL2,      MAXLP3
      INTEGER             MINBL3,      MINBL4,      MXBL23,      MXBLD
      INTEGER             MXORB,       N0EXT,       N1EXT,       N2EXT
      INTEGER             N3EXT,       N4EXT,       ND0EXT,      ND2EXT
      INTEGER             ND4EXT,      NELI,        NEXT,        NFCT
      INTEGER             NINTPT,      NMONEL,      NVALW,       NVALWK
      INTEGER             NVALX,       NVALY,       NVALZ,       PTHW
      INTEGER             PTHX,        PTHY,        PTHYZ,       PTHZ
      INTEGER             SPCCI,       TOTSPC
C
      COMMON / INF    /   PTHZ,        PTHY,        PTHX,        PTHW
      COMMON / INF    /   NINTPT,      DIMCI,       LAMDA1,      BUFSZI
      COMMON / INF    /   NMONEL,      INTORB,      MAXLP3,      BUFSZL
      COMMON / INF    /   MAXBL2,      MXBL23,      MXORB,       MXBLD
      COMMON / INF    /   NEXT,        TOTSPC,      MINBL4,      MINBL3
      COMMON / INF    /   ND4EXT,      ND2EXT,      ND0EXT,      N4EXT
      COMMON / INF    /   N3EXT,       N2EXT,       N1EXT,       N0EXT
      COMMON / INF    /   SPCCI,       CONFYZ,      PTHYZ,       NVALWK
      COMMON / INF    /   NVALZ,       NVALY,       NVALX,       NVALW
      COMMON / INF    /   NFCT,        NELI
C
