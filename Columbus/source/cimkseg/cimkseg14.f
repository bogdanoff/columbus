!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
*@if defined (essl) || (defined(ibm) && defined(vector))
*@define esslvec
*@endif
c
cc
c     wtbrhd > wtlphd
c
c         bra = bra + k
c         ket = ket + b             fur wx,zy,xy,wy
c
c     andersherum fuer zz,xx,yy,ww
c
c




      subroutine so1int(  indsymbra, indsymket,conftbra,conftket,
     &       cibra, ciket, sigmabra,sigmaket, indxbra,indxket,
     &       intbuf,      transf,      lpbuf,       scr1,
     &       segsm,       cist, intask )
c*****
c     one external case
c
c     31-mar-00 modified from onex in ciudg5.f by z. zhang
c     february 1988  new version by hans lischka, university of vienna
c*****
c
c     indsymbra   gives the symmetry of the internal walk
c     indsymket
c     conftbra    ci vector offset number for valid walks
c     conftbra
c     indxbra   index vector for bra segment
c     indxket   index vector for ket segment
c     cibra     ci vector for bra segment
c     ciket     ci vector for ket segment
c     sigmabra  sigma vector for bra segment
c     sigmaket  sigma vector for ket segment
c     intbuf   buffer for two electron integrals
c     transf   lin. comb. of integrals
c     lpbuf    buffer for formula tape
c     scr1     scratch array
c     segsm(2) number of internal walks in segments 1 and 2
c     cist(2)   starting number for the ci vector in segments 1 and 2
c     task      task type: 1 yz 3 xy  4 wy
c
         IMPLICIT NONE

C
C     External functions
C
      EXTERNAL            ddot_wr
      EXTERNAL            RL
C
      INTEGER         RL
C
      REAL*8            ddot_wr
C
C     Parameter variables
C
      INTEGER         MAXSYM
      PARAMETER           (MAXSYM = 8)
      INTEGER         NIMOMX
      PARAMETER           (NIMOMX = 128)
      INTEGER         NFILMX
      PARAMETER           (NFILMX = 55)
      INTEGER         MXUNIT
      PARAMETER           (MXUNIT = 99)
      INTEGER         NVMAX
      PARAMETER           (NVMAX = 40)
      INTEGER         MAXBF
      PARAMETER           (MAXBF = 35000)
      INTEGER         MAXS
      PARAMETER           (MAXS = 11)
      INTEGER         MAXNUM
      PARAMETER           (MAXNUM = 134)
      INTEGER         FATERR
      PARAMETER           (FATERR = 2)
C
C     Argument variables
C
      INTEGER INDSYMBRA(*),INDSYMKET(*),CONFTBRA(*),CONFTKET(*)
      INTEGER indxbra(*),indxket(*),cist(2),segsm(2),intask
      REAL*8CIBRA(*),CIKET(*),SIGMABRA(*),SIGMAKET(*)
      REAL*8            INTBUF(*),   LPBUF(*),    SCR1(*)
      REAL*8            TRANSF(*)
C
C     Local variables
C
      INTEGER         CODE,        CODEP,       DELB,        DIFF
      INTEGER         FIN,         I,           IND1,        IND2
      INTEGER         INDI,        INDII,       INTST,       IS
      INTEGER         IWX,         J1,          JS,          KSG
      INTEGER         LOOP8,       LOOP8_CAL,   LPST,        LXYZ
      INTEGER         M,           MAXBL,       MLP,         MLPPR
      INTEGER         MLPPRM,      MLPPRZ,      MXORB3,      MXYZ
      INTEGER         NI3,         NMB1,        NMB1A,       NMB2
      INTEGER         NMB2A,       NTRYPR,      START,       SVAL
      INTEGER         TEMP,        TOTINT,      WTBRHD,      WTLPHD
      INTEGER         XYZSYM, TASK
C
      REAL*8            VALUE1,      WORD8

      LOGICAL             ICH
C
C     Common variables
C
      INTEGER         BLST1E(MAXSYM),           CNFW(MAXSYM)
      INTEGER         CNFX(MAXSYM),             LMDA(MAXSYM,MAXSYM)
      INTEGER         LMDB(MAXSYM,MAXSYM),      MNBAS
      INTEGER         NBAS(MAXSYM),             NMBPR(MAXSYM)
      INTEGER         NMSYM,       SCRLEN
      INTEGER         STRTBW(MAXSYM,MAXSYM)
      INTEGER         STRTBX(MAXSYM,MAXSYM),    SYM12(MAXSYM)
      INTEGER         SYM21(MAXSYM)
C
      COMMON /DATA   /  SYM12,       SYM21,       NBAS
      COMMON /DATA   /  LMDA,        LMDB,        NMBPR,       STRTBW
      COMMON /DATA   /  STRTBX,      BLST1E,      NMSYM,       MNBAS
      COMMON /DATA   /  CNFX,        CNFW,        SCRLEN
C
c   SYM12 (i)     contains the numbers 1 through 8
c   SYM21 (i)     contains the numbers 1 through 8  (nonsens in smprep!)
c   NBAS  (*)     number of external basis functions per irrep
c-----------------------------------------------------------------------
c   nmbpr (i)     number of symmetry pairs to generate the external exte
c   lmda  (i,j)   symmetry pair j : lmd(i)=lmdb(i,j)*lmda(i,j)
c   lmdb  (i,j)   with  lmdb(i,j).ge.lmda(i,j) , lmdb(i,j).lt.lmdb(i,j+1
c   REQUIRED FOR x,w externals
c-----------------------------------------------------------------------
c   mnbas         maximum number of external basis functions per irrep
c   cnfx  (*)   number of configurations per valid internal x walk of ir
c   cnfw  (*)   number of configurations per valid internal w walk of ir
c   nmsym       number of irreps
c     strtbw (i,wxsym) and strtbx(i,wxsym) give the starting points
c     in the external space for paths going through w and x,respectively
c     i is one of the possible values of lmdb(j,wxsym),j=1,nmbpr(wxsym)
c-----------------------------------------------------------------------
c
c   strtbw(i,sym)   contains the offset of the starting points in the ex
c                   space for a given internal path of w symmetry
c                   i= contains possible values of one irrep of the pair
c   strtbx(i,sym)   contains the offset of the starting points in the ex
c                   space for a given internal path of x symmetry
c                   i= contains possible values of one irrep of the pair
c-----------------------------------------------------------------------
c   blste1e(*)     unused ????
c   scrlen         unused ????
c

C     Common variables
C
      INTEGER         BUFSZI,      BUFSZL,      CONFYZ,      DIMCI
      INTEGER         INTORB,      LAMDA1
      INTEGER         MAXBL2,      MAXLP3
      INTEGER         MINBL3,      MINBL4,      MXBL23,      MXBLD
      INTEGER         MXORB,       N0EXT
      INTEGER         N1EXT,       N2EXT
      INTEGER         N3EXT,       N4EXT
      INTEGER         ND0EXT,      ND2EXT,      ND4EXT,      NELI
      INTEGER         NEXT,        NFCT,        NINTPT
      INTEGER         NMONEL,      NVALW,       NVALWK
      INTEGER         NVALX,       NVALY,       NVALZ,       PTHW
      INTEGER         PTHX,        PTHY,        PTHYZ,       PTHZ
      INTEGER         SPCCI,       TOTSPC
C
      COMMON /INF    /  PTHZ,        PTHY,        PTHX,        PTHW
      COMMON /INF    /  NINTPT,      DIMCI
      COMMON /INF    /  LAMDA1,      BUFSZI
      COMMON /INF    /  NMONEL,      INTORB
      COMMON /INF    /  MAXLP3,      BUFSZL
      COMMON /INF    /  MAXBL2,      MXBL23,      MXORB,       MXBLD
      COMMON /INF    /  NEXT,        TOTSPC,      MINBL4,      MINBL3
      COMMON /INF    /  ND4EXT,      ND2EXT,      ND0EXT,      N4EXT
      COMMON /INF    /  N3EXT,       N2EXT,       N1EXT,       N0EXT
      COMMON /INF    /  SPCCI,       CONFYZ,      PTHYZ,       NVALWK
      COMMON /INF    /  NVALZ,       NVALY,       NVALX,       NVALW
      COMMON /INF    /  NFCT,        NELI
C
C
c     BUFSZI      buffer size of the diagonal integral file diagint
c,      BUFSZL,   buffer size of the formula tape files
c      CONFYZ,    number of y plus z configurations
c     DIMCI       total number of configurations
c    INTORB,      number of internal orbitals
c       LAMDA1    gives the number of the totally symmetric irrep in sym
c                 (nonsens in smprep ???? )
c        MAXBL2   maximum number of two-electron integrals to be held in
c                 core for the two-external case
c    MAXLP3       maximum number of loop values to be held in core for t
c                 three-external case, i.e. max. number of loops per l v
c        MINBL3,  minimum number of two electron integrals to be held in
c                 core for the three-external case --> ununsed ????
c    MINBL4,      minimum number of two-electron integrals to be held in
c                 core for the four-external case  ---> unused ???
c    MXBL23,      = MAXBL2/3
c     MXBLD       block size of the diagonal two-electron integrals
c                 i.e. all nd4ex,nd2ex or nd0ex must be kept in memory (
c    MXORB,       maxium number of external orbitals per irrep
c    N0EXT        number of all-internal off-diagonal integrals
c      N1EXT,     number of one-external off-diagonal integrals
c     N2EXT       number of two-external off-diagonal integrals
c     N3EXT,      number of three-external off-diagonal integrals
c      N4EXT      number of four-external off-diagonal integrals
c       ND0EXT,   number of all-internal diagonal integrals
c     ND2EXT,     number of two-external diagonal integrals
c     ND4EXT,     number of four-external diagonal integrals
c    NELI         number of electrons contained in DRT
c        NEXT,    total number of external orbitals
c     NFCT,       total number of frozen core orbitals
c     NINTPT,     total number of (valid and invalid) internal paths
c      NMONEL,    total number of one-electron integrals (nmsym lower tr
c   NVALW,        number of valid w walks
c  NVALWK         total number of valid walks
c     NVALX,      number of valid x walks
c   NVALY,        number of valid y walks
c  NVALZ,         number of valid z walks
c    PTHW         number of internal w paths
c      PTHX,      number of internal x paths
c       PTHY,     number of internal y paths
c       PTHYZ,    number of internal y and z paths
c       PTHZ      number of internal z paths
c       SPCCI,    maximum memory available for CI/sigma vector
c       TOTSPC    total available core memory  (=lcore)


C     Common variables
C
      INTEGER         N0INT,       N0LP,        N1INT,       N1LP
      INTEGER         N2INT,       N2LP
C
      COMMON /SOINF  /  N2INT,       N1INT,       N0INT,       N2LP
      COMMON /SOINF  /  N1LP,        N0LP
C
c
c  n2int   two-internal so ints
c  n1int   one-internal so ints
c  n0int   all-external so ints
c  n2lp    number of two-internal loops
c  n1lp    number of one-internal loops
c  n0lp    number of zero-internal loops
c
C     Common variables
C
      INTEGER         ORBSYM(NIMOMX)
C
      COMMON /INF1   /  ORBSYM
C
c
c  orbsym(i)  : spatial symmetry of internal orbital i
c
C     Common variables
C
      INTEGER         BL0XT,       BL1XT,       BL2XT, BL4XT
      INTEGER         FILV,        FILW,        ISKIP
      INTEGER         NZPTH
C
      COMMON /INF3   /  ISKIP,       NZPTH  , BL4XT
      COMMON /INF3   /  BL2XT,       BL1XT,       BL0XT,       FILV
      COMMON /INF3   /  FILW
C
c BL0XT,          number of blocks of all-internal integrals
c    BL1XT,       number of blocks of one-external integrals
c    BL2XT,       number of blocks of two-external integrals
c   FILV,         unit number of v-vector file
c      FILW,      unit number of w vector file
c      ISKIP      controls operation of mult
c NZPTH           number of valid z walks ( possibly mixed-up with nvalz

C     Common variables
C
      INTEGER         NUNITS(NFILMX)
C
      COMMON /CFILES /  NUNITS
c
c nunits(1)     tape6 ,iwrite
c        2      nin,tape5,shout
c        3      nslist
c        4      fildia,ndiagf
c        5      pseudg,filsgm
c        6      filew,hvfile
c        7      filev,civfl
c        8      filind
c        9      filint
c       10      fildrt
c       11      fillpd
c       12      fillp
c       13      filfti,fillpi
c       14      flindx
c       15      ciinv,inufv
c       16      civout,outufv
c       17      fockdg
c       18      d1fl
c       20      fvfile
c       21      filsc4,filcii,scrfil
c       22      filtot
c       23      fiacpf
c       26      filsc5
c       27      mcrest
c       28      ciuvfl
c       29      cirefv
c       30      filden
c       31      ifil4w
c       32      ifil4x
c       33      ifil3w
c       34      ifil3x
c       35      filtpq
c       36      aofile
c       37      aofile2
c       38      drtfil
c       39      filsti
c       42      mocoef,vectrf
c       45      prtap
c

c
c

      integer filint,fillp
      equivalence (filint,nunits(9))
      equivalence (fillp,nunits(12))
C     Common variables
C
      INTEGER         BL0INT,      BL1INT,      BL2INT
C
      COMMON /SOINF3 /  BL2INT,      BL1INT,      BL0INT
C
c
c start records for
c bl0int  all-external so
c bl1int  one-external so
c bl2int  two-external so
c

C     Common variables
C
      INTEGER         DALEN(MXUNIT)
C
      COMMON /DAINFO /  DALEN
C
c    dalen (i)   record length of unit i

C     Common variables
C
      INTEGER         BRA,         CISTRT,      ISYM,        KET
      INTEGER         NI,          SEGSUM
C
      COMMON /CSO1S  /  KET,         BRA,         SEGSUM,      CISTRT
      COMMON /CSO1S  /  ISYM,        NI
C
c   stuff for SO 1int

      integer multmx
      parameter(multmx=19)
      integer nmulmx
      parameter(nmulmx=9)

c
C
      INTEGER         LXYZIR(3), SPNIR, MULTP,NMUL, HMULT,NEXW
C
      LOGICAL             SKIPSO,      SPNODD,      SPNORB
C
      COMMON /SOLXYZ /  SKIPSO,      SPNORB,      SPNODD,      LXYZIR
      COMMON /SOLXYZ /  SPNIR(multmx,multmx),MULTP(nmulmx), NMUL
      COMMON /SOLXYZ /  HMULT,NEXW(8,4)
C

c
c  SKIPSO   Spinorbit-CI calculation
c  SPNODD   odd spin
c  SPNORB    CI
c  LXYZIR   irreducible representation of the three L components
c  SPNIR
c  MULTP
c  NMUL
c  HMULT    highest multiplicity
c  NEXW     number of external walks per symmetry
c
c
c


      INTEGER DG0X,DG2X,DG4X,ZZ0X,YY0X,XX0X,WW0X
      INTEGER YZ1X,YX1X,YW1X,YY2EX,WW2X,XX2X,XZ2X,WZ2X,WX2X
      INTEGER YX3X,YW3X,Z4X,Y4X,X4X,W4X
      INTEGER ZZ0XSO,YY0XSO,XX0XSO,WW0XSO
      INTEGER YZ1XSO,YX1XSO,YW1XSO,YY2XSO,XX2XSO,WX2XSO
      INTEGER DYZ, DYX, DYW, D0Z,D0Y,D0W,D0X
      INTEGER DDZI, DDYI, DDXI, DDWI, DDZE, DDYE, DDXE, DDWE
      INTEGER XX2XSO2
C
      COMMON /COUNTER/DG0X,DG2X,DG4X,ZZ0X,YY0X,XX0X,WW0X
      COMMON /COUNTER/YZ1X,YX1X,YW1X,YY2EX,WW2X,XX2X,XZ2X,WZ2X,WX2X
      COMMON /COUNTER/YX3X,YW3X,Z4X,Y4X,X4X,W4X
      COMMON /COUNTER/ZZ0XSO,YY0XSO,XX0XSO,WW0XSO
      COMMON /COUNTER/YZ1XSO,YX1XSO,YW1XSO,YY2XSO,XX2XSO,WX2XSO
      COMMON /COUNTER/DYZ, DYX, DYW, D0Z,D0Y,D0W,D0X
      COMMON /COUNTER/DDZI, DDYI, DDXI, DDWI, DDZE, DDYE, DDXE, DDWE
      COMMON/COUNTER/XX2XSO2

c just for error checking
c records the number of contributions
c dg0x  0 ext. diagonal case
c dg2x  2 ext. diagonal case
c dg4x  4 ext. diagonal case
c zz0x  0 ext.  zz case
c yy0x  0 ext.  yy case
c xx0x  0 ext.  xx case
c ww0x  0 ext.  ww case
c yz1x  1 ext.  yz case
c yx1x  1 ext.  yx case
c yw1x  1 ext.  yw case
c yy2ex  2 ext.  yy case
c ww2x   2ext.   ww case
c xx2x   2ext.   xx case
c xz2x   2ext.   xz case
c wz2x   2ext.   wz case
c wx2x   2ext.   wx case
c yx3x   3ext.   yx case
c yw3x   3ext.   yw case
c z4x    4ext.   z case
c y4x    4ext.   y case
c x4x    4ext.   x case
c w4x    4ext.   w case
c


C
C     Common variables
C
      INTEGER         HEADV(MAXBF),             IBF
      INTEGER         ICODEV(MAXBF),            XBTV(MAXBF)
      INTEGER         XKTV(MAXBF), YBTV(MAXBF), YKTV(MAXBF)
      INTEGER         HLTV(MAXBF)
C
      REAL*8            VALV(3,MAXBF)
C
      COMMON /SPECIAL/  VALV,        HEADV,       ICODEV,      XKTV
      COMMON /SPECIAL/  XBTV,        YBTV,        YKTV
      COMMON /SPECIAL/  HLTV, IBF
C
c
c  internal stuff for loop calculation
c
c
c
c     # dummy:
c
c     #spin coupling table
c
C     Common variables
C
      INTEGER         MULT(MAXSYM,MAXSYM),      NSYM,        SSYM
C
      COMMON /CSYM   /  MULT,        NSYM,        SSYM
C
c mult   standard multiplication table
c nsym   number of irreps
c ssym   state symmetry
C     Common variables
C
      INTEGER         BMS(MAXNUM,3,2),          KMS(MAXNUM,3,2)
      INTEGER         NUMNZ(3,MAXS,2)
C
      REAL*8            MTXSO(MAXNUM,3,MAXS,2)
C
      COMMON /CMTXSO /  BMS,         KMS,         NUMNZ,       MTXSO
C
c  some internal tables for
c  computation of so-loops
c
c     # local:

c
c     # bummer error type.
c
c
c
cso5.5
c
c    ##  initialise the elements from common/lcontrol/
c
c     write(6,*) 'so1int indxbra:'
c     write(6,34) indxbra(1:20)
c     write(6,*) 'so1int indxket:'
c     write(6,34) indxket(1:20)
c     write(6,*) 'so1int conftbra:'
c     write(6,34) indxbra(1:20)
c     write(6,*) 'so1int conftket:'
c     write(6,34) indxket(1:20)
 34   format(20i4)


      call wzero(maxbf*3,valv,1)
      call izero_wr(maxbf,headv,1)
      call izero_wr(maxbf,icodev,1)
      call izero_wr(maxbf,xbtv,1)
      call izero_wr(maxbf,xktv,1)
      call izero_wr(maxbf,ybtv,1)
      call izero_wr(maxbf,yktv,1)
      if (intask.gt.1) then
        task=intask-1
      else
        task=intask
      endif

c
c     # initialize necessary stack arrays for FT calculation.
c
      call istack
c
c     # initialize loop8_cal
c
      loop8_cal = 1
c
cso5.5
c
      intst  = bl4xt+ bl2xt + bl1xt + bl0xt + bl2int + 1
      indi   = 0
      indii  = 0
      ind2   = 0
      loop8  = 1
      totint = 0
      mxorb3 = mxorb * 3
c
c     # mxorb should be the maximum number of so integrals consistent
c     # with the current internal level, the value used here should be
c     # sufficient !!??
c
      mxorb3 = mxorb * 3
      ket    = 0
      bra    = 0
      value1 = (0)
c
c     # read first record of formula tape
c
      call so1int_ft(.true.,indxbra,indxket,segsm,task)
      loop8_cal=1
      maxbl = mxorb3 + bufszi
c
      do 250 j1 = 1, intorb
c
        js = orbsym(j1)
c
        ni3 = 0
        do 120 lxyz = 1, 3
          xyzsym = lxyzir(lxyz)
          is = mult(js, xyzsym)
          isym = sym21(is)
          ni = nbas(isym)
          ni3 = ni3 + ni
  120   enddo
c
        totint = totint + ni3
        ntrypr=task
c
c       # indi   ending position of the last integral block
c       # indii  ending position of the current integral block
c
        indi = indii
        indii = indi+ni3
        if ( indii .gt. ind2 ) then
c
c         # read integrals into buffer.
c
          diff = ind2 - indi
          do 130 i = 1, diff
            intbuf(i) = intbuf(indi+i)
  130     enddo
          ind1 = diff
          ind2 = ind1 + bufszi
          if ( ind2 .gt. maxbl ) then
            write(0,20000)ind2,maxbl
            call bummer('so1int: (ind2-maxbl)=',(ind2-maxbl),faterr)
          endif
          start = ind1 + 1
c         call diraccnew(
c    &         filint,        1,      intbuf(start), (bufszi),
c    &         dalen(filint), intst,  fin  )
          intst = intst + 1
          indi = 0
          indii = ni3
        endif
c     write(6,30010)ind1,ind2,indi,indii,isym,ni,j1,intst
c     write(6,'(6e13.6)') (intbuf(i),i=1,indii)
c
c     # next internal loop
c
  140   continue
c
c     #  word8 contains code,wtlphd,mlppr,mlp
c     #
c     #  wtbrhd  weight of the bra part of the loop head
c     #  wtlphd  weight of the ket part of the loop head
c     #  mlppr   weight of the bra part of the loop
c     #  mlp     weight of the ket part of the loop
c     #  code   0  new set of indices (l,k,j)
c     #         1  end of record
c     #         2  new entry yz-xy,or xy-wy
c     #         3  val1*(ij/kl)+val2*(jk/il)
c     #
c     #  ntrypr   1  yz
c     #           2  xy
c     #           3  wy
c
          code      = icodev(loop8_cal)
          wtbrhd    = xbtv  (loop8_cal)
          wtlphd    = xktv  (loop8_cal)
          mlppr     = ybtv  (loop8_cal)
          mlp       = yktv  (loop8_cal)
          loop8_cal = loop8_cal+1
        codep = code + 1
c
        go to ( 250,  150,  160,  170 ), codep
c
  150   continue
c
          call so1int_ft(.false.,indxbra,indxket,segsm,task)
          loop8_cal = 1
        go to 140
c
  160   continue
c
        call bummer('so1int: should never occur',0,2)
        go to 140
c
  170   continue
c
          value1 = valv(1,loop8_cal-1)
c
        if ( ni3 .eq. 0 ) go to 140
  190   continue
c
c      we now have the convention of bra >= ket
c      but this might refer to the SO-CI on top
c      of it where the ordering seems to be
c      #loopextensions bra < # loopextensions ket
c
        ich = .false.
        if(wtbrhd .gt. wtlphd)then
          ich = .true.
          value1 = - value1
        endif
c
        sval = max(wtlphd, wtbrhd)/2
        delb = 1
        if(wtbrhd .eq. wtlphd)then
          delb = 2
        endif
        mxyz = 0
        ni = 0
c       write(6,*) 'code,mlppr,mlp:',code,mlppr,mlp
c
        do 240 lxyz = 1, 3
          mxyz = mxyz + ni
          xyzsym = sym12(lxyzir(lxyz))
          isym = mult(js, xyzsym)
          ni = nbas(isym)
          if(ni .eq. 0) goto 240
          do 230 m = 1, numnz(lxyz, sval, delb)
            do 200 i = 1, ni
              transf(i) = value1 * intbuf(indi+mxyz+i) *
     &                           mtxso(m,lxyz,sval,delb)
  200       enddo
c
c       now still bra (y) > ket (z)
c       so we keep the association mlp/ket/indxket/ciket/sigmaket
c                                  mlppr/bra/indxbra/cibra/sigmabra
c
           if (ich) then
            ket=mlp+bms(m,lxyz,delb)
            bra=mlppr+kms(m,lxyz,delb)
           else
            ket=mlp+kms(m,lxyz,delb)
            bra=mlppr+bms(m,lxyz,delb)
           endif
c
           if (bra.le.0 .or. ket.le.0) goto 230
            if (bra.gt.segsm(1) .or. ket.gt.segsm(2)) goto 230
            if ( ntrypr .gt. 1 ) go to 210
c
c***********************************************************************
c
c           zy
c
c***********************************************************************
c
              nmb1a=indxket(ket)
              nmb2a=indxbra(bra)
            if(nmb1a.lt.0 .or. nmb2a.lt.0 )go to 230

            yz1xso=yz1xso+1

            nmb1=conftket(nmb1a)+1
            nmb2=conftbra(nmb2a)+1
            go to 230
c
c***********************************************************************
c
c           # yx,yw
c
c***********************************************************************
c
  210       continue
c
            call so1s( indsymbra,indsymket,conftbra,conftket,
     .                 indxbra,indxket,cibra,ciket,sigmabra,
     .                 sigmaket,transf,scr1,ntrypr-1,segsm)
  230     enddo
  240   enddo
        go to 140
  250 enddo
      if ( totint .ne. n1int ) then
        write(0,20010) totint, n1int
        call bummer('so1int: (totint-n1int)=',(totint-n1int),faterr)
      endif
      return
20000 format(' insufficient space for so integrals in so1int'
     & /' space needed ',i6,'  space available ',i6)
20010 format(' so1int nmb.of int. calculated',i6,' from sort',i6)
30040 format(' 600',20i6)
30005 format(' 30',20i6)
c30025 format(' code',2i6)
      end
      subroutine so1s( indsymbra,indsymket,conftbra,conftket,
     .   indxbra,indxket,cibra,ciket,sigmabra,sigmaket,
     .   transf,scr1,iwx,segsm)
c
c  one-external xy and wy cases
c
c  modified from oneyxw in ciudg5.f by z. zhang 31-mar-00
c
         IMPLICIT NONE

C
C     External functions
C
      EXTERNAL            ddot_wr
C
      REAL*8            ddot_wr
C
C     Parameter variables
C
      INTEGER         MAXSYM
      PARAMETER           (MAXSYM = 8)
C
      REAL*8            ONE
      PARAMETER           (ONE = 1D0)
C
C     Argument variables
C
      INTEGER INDSYMBRA(*),INDSYMKET(*),CONFTBRA(*),CONFTKET(*)
      INTEGER INDXBRA(*),INDXKET(*),iwx,segsm(2)
C
      REAL*8  CIBRA(*), CIKET(*), SIGMABRA(*), SIGMAKET(*)
      REAL*8  TRANSF(*),SCR1(*)
C
C     Local variables
C
      INTEGER         M1LP,        M1LPR,       NMB1,        NMB1A
      INTEGER         NMB2,        NMB2A,       NY,          STARTX
      INTEGER         XS,          XSYM,        YS,          YSYM
C
      REAL*8            SIGN
C
c
c*start common block data -eas (8/28/89)
c include(data.common)
C     Common variables
C
      INTEGER         BLST1E(MAXSYM),           CNFW(MAXSYM)
      INTEGER         CNFX(MAXSYM),             LMDA(MAXSYM,MAXSYM)
      INTEGER         LMDB(MAXSYM,MAXSYM),      MNBAS
      INTEGER         NBAS(MAXSYM),             NMBPR(MAXSYM)
      INTEGER         NMSYM,       SCRLEN
      INTEGER         STRTBW(MAXSYM,MAXSYM)
      INTEGER         STRTBX(MAXSYM,MAXSYM),    SYM12(MAXSYM)
      INTEGER         SYM21(MAXSYM)
C
      COMMON /DATA   /  SYM12,       SYM21,       NBAS
      COMMON /DATA   /  LMDA,        LMDB,        NMBPR,       STRTBW
      COMMON /DATA   /  STRTBX,      BLST1E,      NMSYM,       MNBAS
      COMMON /DATA   /  CNFX,        CNFW,        SCRLEN
C
c   SYM12 (i)     contains the numbers 1 through 8
c   SYM21 (i)     contains the numbers 1 through 8  (nonsens in smprep!)
c   NBAS  (*)     number of external basis functions per irrep
c-----------------------------------------------------------------------
c   nmbpr (i)     number of symmetry pairs to generate the external exte
c   lmda  (i,j)   symmetry pair j : lmd(i)=lmdb(i,j)*lmda(i,j)
c   lmdb  (i,j)   with  lmdb(i,j).ge.lmda(i,j) , lmdb(i,j).lt.lmdb(i,j+1
c   REQUIRED FOR x,w externals
c-----------------------------------------------------------------------
c   mnbas         maximum number of external basis functions per irrep
c   cnfx  (*)   number of configurations per valid internal x walk of ir
c   cnfw  (*)   number of configurations per valid internal w walk of ir
c   nmsym       number of irreps
c     strtbw (i,wxsym) and strtbx(i,wxsym) give the starting points
c     in the external space for paths going through w and x,respectively
c     i is one of the possible values of lmdb(j,wxsym),j=1,nmbpr(wxsym)
c-----------------------------------------------------------------------
c
c   strtbw(i,sym)   contains the offset of the starting points in the ex
c                   space for a given internal path of w symmetry
c                   i= contains possible values of one irrep of the pair
c   strtbx(i,sym)   contains the offset of the starting points in the ex
c                   space for a given internal path of x symmetry
c                   i= contains possible values of one irrep of the pair
c-----------------------------------------------------------------------
c   blste1e(*)     unused ????
c   scrlen         unused ????
c

c*end common block data -eas (8/28/89)
c
C     Common variables
C
      INTEGER         BRA,         CISTRT,      ISYM,        KET
      INTEGER         NI,          SEGSUM
C
      COMMON /CSO1S  /  KET,         BRA,         SEGSUM,      CISTRT
      COMMON /CSO1S  /  ISYM,        NI
C
c   stuff for SO 1int

c
        integer nmomx
        parameter (nmomx=1023)
c
c
C     Common variables
C
      INTEGER         NNDX(NMOMX)
C
      COMMON /CNNDX  /  NNDX
C

c
c  nndx(i) = i*(i-1)/2
c


      INTEGER DG0X,DG2X,DG4X,ZZ0X,YY0X,XX0X,WW0X
      INTEGER YZ1X,YX1X,YW1X,YY2EX,WW2X,XX2X,XZ2X,WZ2X,WX2X
      INTEGER YX3X,YW3X,Z4X,Y4X,X4X,W4X
      INTEGER ZZ0XSO,YY0XSO,XX0XSO,WW0XSO
      INTEGER YZ1XSO,YX1XSO,YW1XSO,YY2XSO,XX2XSO,WX2XSO
      INTEGER DYZ, DYX, DYW, D0Z,D0Y,D0W,D0X
      INTEGER DDZI, DDYI, DDXI, DDWI, DDZE, DDYE, DDXE, DDWE
      INTEGER XX2XSO2
C
      COMMON /COUNTER/DG0X,DG2X,DG4X,ZZ0X,YY0X,XX0X,WW0X
      COMMON /COUNTER/YZ1X,YX1X,YW1X,YY2EX,WW2X,XX2X,XZ2X,WZ2X,WX2X
      COMMON /COUNTER/YX3X,YW3X,Z4X,Y4X,X4X,W4X
      COMMON /COUNTER/ZZ0XSO,YY0XSO,XX0XSO,WW0XSO
      COMMON /COUNTER/YZ1XSO,YX1XSO,YW1XSO,YY2XSO,XX2XSO,WX2XSO
      COMMON /COUNTER/DYZ, DYX, DYW, D0Z,D0Y,D0W,D0X
      COMMON /COUNTER/DDZI, DDYI, DDXI, DDWI, DDZE, DDYE, DDXE, DDWE
      COMMON/COUNTER/XX2XSO2

c just for error checking
c records the number of contributions
c dg0x  0 ext. diagonal case
c dg2x  2 ext. diagonal case
c dg4x  4 ext. diagonal case
c zz0x  0 ext.  zz case
c yy0x  0 ext.  yy case
c xx0x  0 ext.  xx case
c ww0x  0 ext.  ww case
c yz1x  1 ext.  yz case
c yx1x  1 ext.  yx case
c yw1x  1 ext.  yw case
c yy2ex  2 ext.  yy case
c ww2x   2ext.   ww case
c xx2x   2ext.   xx case
c xz2x   2ext.   xz case
c wz2x   2ext.   wz case
c wx2x   2ext.   wx case
c yx3x   3ext.   yx case
c yw3x   3ext.   yw case
c z4x    4ext.   z case
c y4x    4ext.   y case
c x4x    4ext.   x case
c w4x    4ext.   w case
c


C
c
*@ifdef debug
*      common/ctestx/itest(40)
*@endif
c
c     # dummy:
c
c     # local:
c
c
c
c     iwx=1 xy loops
c     iwx=2 wy loops
c
      nmb1a = indxket(ket)
      if (nmb1a.lt.0) go to 710
      nmb2a = indxbra(bra)
      if (nmb2a.lt.0) go to 710
      nmb1 = conftket(nmb1a)+1
      nmb2 = conftbra(nmb2a)+1
      ys = indsymket(nmb1a)
      xs = indsymbra(nmb2a)
      ysym = sym21(ys)
      xsym = sym21(xs)
c     write(6,30050)mlppr,m1lpr,mlp,m1lp,nmb1,nmb2,ysym,xsym,iwx
      if (ysym-isym) 730,780,750
c
c     lmd(y).lt.lmd(i)
c
730   continue
      ny = nbas(ysym)
      if (ny.eq.0) go to 710
      if (iwx.eq.1) then
        startx = nmb2+strtbx(isym,xsym)
        yx1xso=yx1xso+1
      else
        startx = nmb2+strtbw(isym,xsym)
        yw1xso=yw1xso+1
      endif
      go to 710
c
c     lmd(y).gt.lmd(i)
c
750   continue
      ny = nbas(ysym)
      if(ny.eq.0)go to 710
      if(iwx.eq.1)then
        startx = nmb2+strtbx(ysym,xsym)
        yx1xso=yx1xso+1
        sign = -one
      else
        startx = nmb2+strtbw(ysym,xsym)
        yw1xso=yw1xso+1
        sign = one
      endif
      go to 710
c
c     lmd(y)=lmd(i),lmd(x)=1 or lmd(w)=1
c
780   continue
c
c     # note: expda(), expds(), avxv0(), svxv2u(), gmxv(), vvvip(),
c     #       and vivvp() should be inlined if possible to reduce
c     #       overhead.
c
      if(iwx.eq.1)then
        if(ni.gt.1)then
          startx = nmb2+strtbx(isym,xsym)
          yx1xso=yx1xso+1
        endif
      else
        startx = nmb2+strtbw(isym,xsym)
        yw1xso=yw1xso+1
      endif
710   continue
c
      return
30050 format(' 700',20i6)
      end
      subroutine so2int(  reflst,
     .  indsymbra,indsymket,conftbra,conftket,cibra,ciket,
     .  sgmabra,sgmaket,indxbra,indxket,
     &       intbuf,      lpbuf,
     &       segsm,       cist,      href,task  )
c*****
c     all internal case
c
c     modified from allin in ciudg5.f by z. zhang 31-mar-00
c     modified 03/09/90  to clean w/x segment logic and allow 0
c                        segments. -eas
c     modified 08/20/89  to remove jpthz common block (eas)
c     february 1988  new version by hans lischka, university of vienna
c*****
c
c     href     h matrix in reference space (for iskip=-2 only)
c     reflst   integer list of zpath references (0=nonref,i=ith ref)
c     indsym   gives the symmetry of the internal walk
c     conft    ci vector offset number for valid walks
c     indxyz   index vector for y and z paths
c     ciyz,sigmyz ci and sigma vector for y and z walks
c     index1   index vector for segment 1
c     index2   index vector for segment 2
c     ci1      ci vector for segment 1
c     ci2      ci vector for segment 2
c     sigm1    sigma vector for segment 1
c     sigm2    sigma vector for segment 2
c     intbuf   buffer for two electron integrals
c     lpbuf    buffer for formula tape
c     ipth1(2) start number for internal paths for segments 1 and 2
c     segsm(2) number of internal walks in segments 1 and 2
c     cist(2)   starting number for the ci vector in segments 1 and 2
         IMPLICIT NONE

C
C     External functions
C
      EXTERNAL            RL
C
      INTEGER         RL
C
C     Parameter variables
C
      INTEGER         MAXSYM
      PARAMETER           (MAXSYM = 8)
      INTEGER         NIMOMX
      PARAMETER           (NIMOMX = 128)
      INTEGER         NFILMX
      PARAMETER           (NFILMX = 55)
      INTEGER         MXUNIT
      PARAMETER           (MXUNIT = 99)
      INTEGER         NVMAX
      PARAMETER           (NVMAX = 40)
      INTEGER         MAXBF
      PARAMETER           (MAXBF = 35000)
C
      REAL*8            ZERO
      PARAMETER           (ZERO = 0D0)
C
C     Argument variables
C
      INTEGER REFLST(*),INDSYMBRA(*),INDSYMKET(*),CONFTBRA(*)
      INTEGER CONFTKET(*),INDXBRA(*),INDXKET(*),CIST(2),TASK,SEGSM(2)
C
      LOGICAL             LSTSEG
C
      REAL*8CIBRA(*), CIKET(*),SGMABRA(*),SGMAKET(*),     HREF(*)
      REAL*8            INTBUF(*),   LPBUF(*)
C
C     Local variables
C
      INTEGER         BUFSZC,      CODE,        CODEP,       FIN
      INTEGER         I,           II,          IINT,        IJSYM
      INTEGER         INTST,       IREM,        ISYM,        JJ
      INTEGER         JSYM,        KSG1,        KSG2,        LOOP8
      INTEGER         LOOP8_CAL,   LPST,        LXYZ,        MLPPRZ
      INTEGER         NXYZ,        TOTINT
C
      REAL*8            HINT(3),     HINT3(3),    VALUE,       WORD8
      LOGICAL             IX
C
c
C     Common variables
C
      INTEGER         BUFSZI,      BUFSZL,      CONFYZ,      DIMCI
      INTEGER         INTORB,      LAMDA1
      INTEGER         MAXBL2,      MAXLP3
      INTEGER         MINBL3,      MINBL4,      MXBL23,      MXBLD
      INTEGER         MXORB,       N0EXT
      INTEGER         N1EXT,       N2EXT
      INTEGER         N3EXT,       N4EXT
      INTEGER         ND0EXT,      ND2EXT,      ND4EXT,      NELI
      INTEGER         NEXT,        NFCT,        NINTPT
      INTEGER         NMONEL,      NVALW,       NVALWK
      INTEGER         NVALX,       NVALY,       NVALZ,       PTHW
      INTEGER         PTHX,        PTHY,        PTHYZ,       PTHZ
      INTEGER         SPCCI,       TOTSPC
C
      COMMON /INF    /  PTHZ,        PTHY,        PTHX,        PTHW
      COMMON /INF    /  NINTPT,      DIMCI
      COMMON /INF    /  LAMDA1,      BUFSZI
      COMMON /INF    /  NMONEL,      INTORB
      COMMON /INF    /  MAXLP3,      BUFSZL
      COMMON /INF    /  MAXBL2,      MXBL23,      MXORB,       MXBLD
      COMMON /INF    /  NEXT,        TOTSPC,      MINBL4,      MINBL3
      COMMON /INF    /  ND4EXT,      ND2EXT,      ND0EXT,      N4EXT
      COMMON /INF    /  N3EXT,       N2EXT,       N1EXT,       N0EXT
      COMMON /INF    /  SPCCI,       CONFYZ,      PTHYZ,       NVALWK
      COMMON /INF    /  NVALZ,       NVALY,       NVALX,       NVALW
      COMMON /INF    /  NFCT,        NELI
C
C
c     BUFSZI      buffer size of the diagonal integral file diagint
c,      BUFSZL,   buffer size of the formula tape files
c      CONFYZ,    number of y plus z configurations
c     DIMCI       total number of configurations
c    INTORB,      number of internal orbitals
c       LAMDA1    gives the number of the totally symmetric irrep in sym
c                 (nonsens in smprep ???? )
c        MAXBL2   maximum number of two-electron integrals to be held in
c                 core for the two-external case
c    MAXLP3       maximum number of loop values to be held in core for t
c                 three-external case, i.e. max. number of loops per l v
c        MINBL3,  minimum number of two electron integrals to be held in
c                 core for the three-external case --> ununsed ????
c    MINBL4,      minimum number of two-electron integrals to be held in
c                 core for the four-external case  ---> unused ???
c    MXBL23,      = MAXBL2/3
c     MXBLD       block size of the diagonal two-electron integrals
c                 i.e. all nd4ex,nd2ex or nd0ex must be kept in memory (
c    MXORB,       maxium number of external orbitals per irrep
c    N0EXT        number of all-internal off-diagonal integrals
c      N1EXT,     number of one-external off-diagonal integrals
c     N2EXT       number of two-external off-diagonal integrals
c     N3EXT,      number of three-external off-diagonal integrals
c      N4EXT      number of four-external off-diagonal integrals
c       ND0EXT,   number of all-internal diagonal integrals
c     ND2EXT,     number of two-external diagonal integrals
c     ND4EXT,     number of four-external diagonal integrals
c    NELI         number of electrons contained in DRT
c        NEXT,    total number of external orbitals
c     NFCT,       total number of frozen core orbitals
c     NINTPT,     total number of (valid and invalid) internal paths
c      NMONEL,    total number of one-electron integrals (nmsym lower tr
c   NVALW,        number of valid w walks
c  NVALWK         total number of valid walks
c     NVALX,      number of valid x walks
c   NVALY,        number of valid y walks
c  NVALZ,         number of valid z walks
c    PTHW         number of internal w paths
c      PTHX,      number of internal x paths
c       PTHY,     number of internal y paths
c       PTHYZ,    number of internal y and z paths
c       PTHZ      number of internal z paths
c       SPCCI,    maximum memory available for CI/sigma vector
c       TOTSPC    total available core memory  (=lcore)


C     Common variables
C
      INTEGER         N0INT,       N0LP,        N1INT,       N1LP
      INTEGER         N2INT,       N2LP
C
      COMMON /SOINF  /  N2INT,       N1INT,       N0INT,       N2LP
      COMMON /SOINF  /  N1LP,        N0LP
C
c
c  n2int   two-internal so ints
c  n1int   one-internal so ints
c  n0int   all-external so ints
c  n2lp    number of two-internal loops
c  n1lp    number of one-internal loops
c  n0lp    number of zero-internal loops
c
C     Common variables
C
      INTEGER         ORBSYM(NIMOMX)
C
      COMMON /INF1   /  ORBSYM
C
c
c  orbsym(i)  : spatial symmetry of internal orbital i
c
C     Common variables
C
      INTEGER         NUNITS(NFILMX)
C
      COMMON /CFILES /  NUNITS
c
c nunits(1)     tape6 ,iwrite
c        2      nin,tape5,shout
c        3      nslist
c        4      fildia,ndiagf
c        5      pseudg,filsgm
c        6      filew,hvfile
c        7      filev,civfl
c        8      filind
c        9      filint
c       10      fildrt
c       11      fillpd
c       12      fillp
c       13      filfti,fillpi
c       14      flindx
c       15      ciinv,inufv
c       16      civout,outufv
c       17      fockdg
c       18      d1fl
c       20      fvfile
c       21      filsc4,filcii,scrfil
c       22      filtot
c       23      fiacpf
c       26      filsc5
c       27      mcrest
c       28      ciuvfl
c       29      cirefv
c       30      filden
c       31      ifil4w
c       32      ifil4x
c       33      ifil3w
c       34      ifil3x
c       35      filtpq
c       36      aofile
c       37      aofile2
c       38      drtfil
c       39      filsti
c       42      mocoef,vectrf
c       45      prtap
c

c
c

      integer filint,fillp
      equivalence (filint,nunits(9))
      equivalence (fillp,nunits(12))
C     Common variables
C
      INTEGER         BL0XT,       BL1XT,       BL2XT, BL4XT
      INTEGER         FILV,        FILW,        ISKIP
      INTEGER         NZPTH
C
      COMMON /INF3   /  ISKIP,       NZPTH  , BL4XT
      COMMON /INF3   /  BL2XT,       BL1XT,       BL0XT,       FILV
      COMMON /INF3   /  FILW
C
c BL0XT,          number of blocks of all-internal integrals
c    BL1XT,       number of blocks of one-external integrals
c    BL2XT,       number of blocks of two-external integrals
c   FILV,         unit number of v-vector file
c      FILW,      unit number of w vector file
c      ISKIP      controls operation of mult
c NZPTH           number of valid z walks ( possibly mixed-up with nvalz

C     Common variables
C
      INTEGER         DALEN(MXUNIT)
C
      COMMON /DAINFO /  DALEN
C
c    dalen (i)   record length of unit i

C     Common variables
C
      INTEGER         CIST1,       CIST2,       JUMP,        MLP
      INTEGER         MLPPR,       SEGSM1,      SEGSM2,      WTBRHD
      INTEGER         WTLPHD
C
      COMMON /CSO2S  /  WTLPHD,      MLP,         MLPPR,       SEGSM1
      COMMON /CSO2S  /  SEGSM2,      CIST1,       CIST2,       JUMP
      COMMON /CSO2S  /  WTBRHD
C
c stuff for SO 2int
      integer multmx
      parameter(multmx=19)
      integer nmulmx
      parameter(nmulmx=9)

c
C
      INTEGER         LXYZIR(3), SPNIR, MULTP,NMUL, HMULT,NEXW
C
      LOGICAL             SKIPSO,      SPNODD,      SPNORB
C
      COMMON /SOLXYZ /  SKIPSO,      SPNORB,      SPNODD,      LXYZIR
      COMMON /SOLXYZ /  SPNIR(multmx,multmx),MULTP(nmulmx), NMUL
      COMMON /SOLXYZ /  HMULT,NEXW(8,4)
C

c
c  SKIPSO   Spinorbit-CI calculation
c  SPNODD   odd spin
c  SPNORB    CI
c  LXYZIR   irreducible representation of the three L components
c  SPNIR
c  MULTP
c  NMUL
c  HMULT    highest multiplicity
c  NEXW     number of external walks per symmetry
c
c
c
C     Common variables
C
      INTEGER         HEADV(MAXBF),             IBF
      INTEGER         ICODEV(MAXBF),            XBTV(MAXBF)
      INTEGER         XKTV(MAXBF), YBTV(MAXBF), YKTV(MAXBF)
      INTEGER         HLTV(MAXBF)
C
      REAL*8            VALV(3,MAXBF)
C
      COMMON /SPECIAL/  VALV,        HEADV,       ICODEV,      XKTV
      COMMON /SPECIAL/  XBTV,        YBTV,        YKTV
      COMMON /SPECIAL/  HLTV, IBF
C
c
c  internal stuff for loop calculation
c
c
C     Common variables
C
      INTEGER         MULT(MAXSYM,MAXSYM),      NSYM,        SSYM
C
      COMMON /CSYM   /  MULT,        NSYM,        SSYM
C
c mult   standard multiplication table
c nsym   number of irreps
c ssym   state symmetry
c
c     # dummy:

c     # local:
c
c
c
cso5.5
c
c    ##  initialise the elements from common/lcontrol/
c
c     write(*,*)'reflst(*)'
c     write(*,33) (reflst(i),i=1,24)
c     write(*,*)'indxbra(*)'
c     write(*,33) (indxbra(i),i=1,24)
c     write(*,*)'indxket(*)'
c     write(*,33) (indxket(i),i=1,24)
c     write(*,*)'conftbra(*)'
c     write(*,33) (conftbra(i),i=1,24)
c     write(*,*)'conftket(*)'
c     write(*,33) (conftket(i),i=1,24)

 33   format(12i4)
      call wzero(maxbf*3,valv,1)
      call izero_wr(maxbf,headv,1)
      call izero_wr(maxbf,icodev,1)
      call izero_wr(maxbf,xbtv,1)
      call izero_wr(maxbf,xktv,1)
      call izero_wr(maxbf,ybtv,1)
      call izero_wr(maxbf,yktv,1)
c
c     # initialize necessary stack arrays for FT calculation.
c
      call istack
c
c     # initialize loop8_cal
c
      loop8_cal = 1
c
cso5.5
c
      code   = 0
      value = zero
      totint = 0
      iint = 1
      loop8 = 1
c
      intst = bl4xt+bl2xt + bl1xt + bl0xt + 1
c
      bufszc=bufszi
c
c     read first block of integrals
c
c     call diraccnew(
c    &     filint,        1,      intbuf(1), (bufszi),
c    &     dalen(filint), intst,  fin  )
      intst=intst+1
c
c      calculate the first block of formula
c
        call so2int_ft(.true.,indxbra,indxket,segsm,task)
        loop8_cal=1
c
      ii = 1
      jj = 2
c
1000  continue
c
      jump = task
      nxyz = 0
      isym = orbsym(ii)
      jsym = orbsym(jj)
      ijsym = mult(isym, jsym)
      do 120 lxyz = 1, 3
        if(ijsym .eq. lxyzir(lxyz)) then
          nxyz = nxyz + 1
        endif
  120 enddo
c     write(6,*) 'ii,jj,ijszm=',ii,jj,ijsym,nxyz
c     write(6,*) 'intbuf(1:10)='
c     write(6,34) intbuf(1:15)
 34   format(5e18.12)
c
      if ( nxyz .eq. 0 ) then
c     # no integrals to process. skip over the formulas.
c     # do until (code = 3) ...
 280    continue
c     # word8 -> lpbuf(loop8) fixed 06-may-91. eas/rls
          code      = icodev(loop8_cal)
          wtbrhd    = xbtv  (loop8_cal)
          wtlphd    = xktv  (loop8_cal)
          mlppr     = ybtv  (loop8_cal)
          mlp       = yktv  (loop8_cal)
          loop8_cal = loop8_cal+1
c
        if ( code .eq. 3 .or. code.eq.0 ) then
c         # exit.
          goto 282
        elseif ( code .eq. 1 ) then
c         # read a new formula buffer.
            call so2int_ft(.false.,indxbra,indxket,segsm,task)
            loop8_cal = 1
        elseif ( code .eq. 2 ) then
          call bummer('so2int invalid code',code,2)
        endif
        goto 280
 282    continue
c       # all the formulas have been skipped for this level.
c       # proceed on to the next level.
        goto 2000
      endif
c
c     new set of internal indices i,j,k=l=intorb+1
c
      totint=totint+nxyz
c
c           # read the integrals into the intbuf(*) buffer.
c
      if(totint.gt.n2int) go to 10000
      if((iint-1+nxyz).le.bufszc) go to 102
c
c     read new block of integrals
c
      irem=bufszc-iint+1
      do 304 i=1,irem
        intbuf(i)=intbuf(iint-1+i)
304   enddo
c     call diraccnew(
c    &     filint,        1,      intbuf(1+irem), (bufszi),
c    &     dalen(filint), intst,  fin  )
       bufszc=bufszi+irem
      intst=intst+1
      iint=1
102   continue
      do 221 lxyz = 1, 3
        if(ijsym.eq.lxyzir(lxyz))then
          hint(lxyz) = intbuf(iint)
          iint=iint+1
        else
          hint(lxyz)=0
        endif
221   enddo
c
  140 continue
c
c     formula tape
c
c     word8 contains code,wtlphd,mlppr,mlp
c
c     wtbrhd   weight of the bra loop head
c     wtlphd   weight of the loop head
c     mlppr    weight of the bra part of the loop
c     mlp      weight of the ket part of the loop
c
c     code   1   new record
c            2   new vertex
c            3   new level(new integral)
c            4   val1*(ij/kl)+val2(jk/il)
c
c     jump   1   z
c            2   y
c            3   x
c            4   w
c
        code      = icodev(loop8_cal)
        wtbrhd    = xbtv  (loop8_cal)
        wtlphd    = xktv  (loop8_cal)
        mlppr     = ybtv  (loop8_cal)
        mlp       = yktv  (loop8_cal)
        loop8_cal = loop8_cal+1
c       write(6,*) 'code,mlppr,mlp=',code,mlppr,mlp
      codep = code + 1
      go to (10000,150,160,2000,400), codep
c
  150 continue
c
c     compute  next formulas
c
        call so2int_ft(.false.,indxbra,indxket,segsm,task)
        loop8_cal = 1
      go to 140
c
  160 continue
      call bummer('so2int invalid codep',codep,2)
      go to 140
c
 400  continue
c
        value=valv(1,loop8_cal-1)
c
c      write(6,32000)code,wtbrhd,wtlphd,mlppr,mlp,jump,value
c potentially troublesome or unnecessary
c
cc alt: alle bra<ket  (eventuell nur fuer z|z, nicht jedoch z|z')
cc alt:  bra+m2, ket+m1
cc alt: falls wtbrhd > wtlphd   bra+m1, ket+m2 -value
c
ci neu:  alle bra>ket
c  neu   bra+m1, ket+m2, value oder -value
c  neu  fall wtbrhd <= wtlphd
c          -value,
c  in so2s keine return statements
c
calt   all-internal: s|s  bra<ket
calt                 s|s' bra<ket ???
c

      value=-value
      ix=.true.
      if (wtbrhd.le.wtlphd) then
         value=-value
         ix=.false.
      endif

c
c     value*(i|lxyz|j)
c
      do 222 lxyz = 1, 3
        hint3(lxyz) = value * hint(lxyz)
 222  enddo
        segsm1=segsm(1)
        segsm2=segsm(2)
        cist1 = cist(1)
        cist2 = cist(2)
c     write(6,*) 'so2int:',code,mlppr,mlp,wtbrhd,wtlphd,jump,value
c     write(6,*) 'hint3:',hint3
c
        call so2s( reflst, indsymbra,indsymket,conftbra,conftket,
     .     indxbra,indxket,cibra,ciket,sgmabra,sgmaket,
     +     hint3,  href,ix)
c
c     go to next loop
c
      go to 140
c
c     end of do loops over segments of ci vector
c
 2000 continue
c
      ii = ii + 1
      if(ii .lt. jj) goto 1000
c
      jj = jj + 1
      ii = 1
      if(jj .le. intorb) goto 1000
c
10000 continue
30015 format(' code',20i4)
32000 format(' code',6i6,f20.12)
      end
       subroutine so2s( reflst, indsymbra,indsymket,conftbra,conftket,
     .     indxbra,indxket,cibra,ciket,sigmbra,sigmket,
     +     hint3,  href,ix)
c
c  modified from allins in ciudg5.f by z. zhang 31-mar-00
c
         IMPLICIT NONE

C
C     Parameter variables
C
      INTEGER         MAXSYM
      PARAMETER           (MAXSYM = 8)
      INTEGER         MAXS
      PARAMETER           (MAXS = 11)
      INTEGER         MAXNUM
      PARAMETER           (MAXNUM = 134)
C
C     Argument variables
C
      INTEGER         REFLST(*),INDSYMBRA(*),INDSYMKET(*)
      INTEGER         CONFTBRA(*),CONFTKET(*),INDXBRA(*)
      INTEGER         INDXKET(*)
C
      REAL*8            CIBRA(*), CIKET(*),SIGMBRA(*),SIGMKET(*)
      REAL*8            HINT3(3),    HREF(*)
      LOGICAL             IX
C
C     Local variables
C
      INTEGER         DELB,        LXYZ,        M,           M1
      INTEGER         M1LP,        M1LPR,       M2,          NIJ
      INTEGER         NMAX,        NMB1,        NMB1A,       NMB2
      INTEGER         NMB2A,       NMIN,        NY,          SVAL
      INTEGER         SY,          SYM
      INTEGER         BRA,KET
C
      REAL*8            HINT
C
c
c*start common block data -eas (8/28/89)
c include(data.common)
C     Common variables
C
      INTEGER         BLST1E(MAXSYM),           CNFW(MAXSYM)
      INTEGER         CNFX(MAXSYM),             LMDA(MAXSYM,MAXSYM)
      INTEGER         LMDB(MAXSYM,MAXSYM),      MNBAS
      INTEGER         NBAS(MAXSYM),             NMBPR(MAXSYM)
      INTEGER         NMSYM,       SCRLEN
      INTEGER         STRTBW(MAXSYM,MAXSYM)
      INTEGER         STRTBX(MAXSYM,MAXSYM),    SYM12(MAXSYM)
      INTEGER         SYM21(MAXSYM)
C
      COMMON /DATA   /  SYM12,       SYM21,       NBAS
      COMMON /DATA   /  LMDA,        LMDB,        NMBPR,       STRTBW
      COMMON /DATA   /  STRTBX,      BLST1E,      NMSYM,       MNBAS
      COMMON /DATA   /  CNFX,        CNFW,        SCRLEN
C
c   SYM12 (i)     contains the numbers 1 through 8
c   SYM21 (i)     contains the numbers 1 through 8  (nonsens in smprep!)
c   NBAS  (*)     number of external basis functions per irrep
c-----------------------------------------------------------------------
c   nmbpr (i)     number of symmetry pairs to generate the external exte
c   lmda  (i,j)   symmetry pair j : lmd(i)=lmdb(i,j)*lmda(i,j)
c   lmdb  (i,j)   with  lmdb(i,j).ge.lmda(i,j) , lmdb(i,j).lt.lmdb(i,j+1
c   REQUIRED FOR x,w externals
c-----------------------------------------------------------------------
c   mnbas         maximum number of external basis functions per irrep
c   cnfx  (*)   number of configurations per valid internal x walk of ir
c   cnfw  (*)   number of configurations per valid internal w walk of ir
c   nmsym       number of irreps
c     strtbw (i,wxsym) and strtbx(i,wxsym) give the starting points
c     in the external space for paths going through w and x,respectively
c     i is one of the possible values of lmdb(j,wxsym),j=1,nmbpr(wxsym)
c-----------------------------------------------------------------------
c
c   strtbw(i,sym)   contains the offset of the starting points in the ex
c                   space for a given internal path of w symmetry
c                   i= contains possible values of one irrep of the pair
c   strtbx(i,sym)   contains the offset of the starting points in the ex
c                   space for a given internal path of x symmetry
c                   i= contains possible values of one irrep of the pair
c-----------------------------------------------------------------------
c   blste1e(*)     unused ????
c   scrlen         unused ????
c

c*end common block data -eas (8/28/89)
c
c*start common block inf3 -eas (8/28/89)
c include(inf3.common)
C     Common variables
C
      INTEGER         BL0XT,       BL1XT,       BL2XT, BL4XT
      INTEGER         FILV,        FILW,        ISKIP
      INTEGER         NZPTH
C
      COMMON /INF3   /  ISKIP,       NZPTH  , BL4XT
      COMMON /INF3   /  BL2XT,       BL1XT,       BL0XT,       FILV
      COMMON /INF3   /  FILW
C
c BL0XT,          number of blocks of all-internal integrals
c    BL1XT,       number of blocks of one-external integrals
c    BL2XT,       number of blocks of two-external integrals
c   FILV,         unit number of v-vector file
c      FILW,      unit number of w vector file
c      ISKIP      controls operation of mult
c NZPTH           number of valid z walks ( possibly mixed-up with nvalz

c*end common block inf3 -eas (8/28/89)
c
C     Common variables
C
      INTEGER         CIST1,       CIST2,       JUMP,        MLP
      INTEGER         MLPPR,       SEGSM1,      SEGSM2,      WTBRHD
      INTEGER         WTLPHD
C
      COMMON /CSO2S  /  WTLPHD,      MLP,         MLPPR,       SEGSM1
      COMMON /CSO2S  /  SEGSM2,      CIST1,       CIST2,       JUMP
      COMMON /CSO2S  /  WTBRHD
C
c stuff for SO 2int
c
c     # dummy:
c
c     #spin coupling table
c
C     Common variables
C
      INTEGER         BMS(MAXNUM,3,2),          KMS(MAXNUM,3,2)
      INTEGER         NUMNZ(3,MAXS,2)
C
      REAL*8            MTXSO(MAXNUM,3,MAXS,2)
C
      COMMON /CMTXSO /  BMS,         KMS,         NUMNZ,       MTXSO
C
c  some internal tables for
c  computation of so-loops


      INTEGER DG0X,DG2X,DG4X,ZZ0X,YY0X,XX0X,WW0X
      INTEGER YZ1X,YX1X,YW1X,YY2EX,WW2X,XX2X,XZ2X,WZ2X,WX2X
      INTEGER YX3X,YW3X,Z4X,Y4X,X4X,W4X
      INTEGER ZZ0XSO,YY0XSO,XX0XSO,WW0XSO
      INTEGER YZ1XSO,YX1XSO,YW1XSO,YY2XSO,XX2XSO,WX2XSO
      INTEGER DYZ, DYX, DYW, D0Z,D0Y,D0W,D0X
      INTEGER DDZI, DDYI, DDXI, DDWI, DDZE, DDYE, DDXE, DDWE
      INTEGER XX2XSO2
C
      COMMON /COUNTER/DG0X,DG2X,DG4X,ZZ0X,YY0X,XX0X,WW0X
      COMMON /COUNTER/YZ1X,YX1X,YW1X,YY2EX,WW2X,XX2X,XZ2X,WZ2X,WX2X
      COMMON /COUNTER/YX3X,YW3X,Z4X,Y4X,X4X,W4X
      COMMON /COUNTER/ZZ0XSO,YY0XSO,XX0XSO,WW0XSO
      COMMON /COUNTER/YZ1XSO,YX1XSO,YW1XSO,YY2XSO,XX2XSO,WX2XSO
      COMMON /COUNTER/DYZ, DYX, DYW, D0Z,D0Y,D0W,D0X
      COMMON /COUNTER/DDZI, DDYI, DDXI, DDWI, DDZE, DDYE, DDXE, DDWE
      COMMON/COUNTER/XX2XSO2

c just for error checking
c records the number of contributions
c dg0x  0 ext. diagonal case
c dg2x  2 ext. diagonal case
c dg4x  4 ext. diagonal case
c zz0x  0 ext.  zz case
c yy0x  0 ext.  yy case
c xx0x  0 ext.  xx case
c ww0x  0 ext.  ww case
c yz1x  1 ext.  yz case
c yx1x  1 ext.  yx case
c yw1x  1 ext.  yw case
c yy2ex  2 ext.  yy case
c ww2x   2ext.   ww case
c xx2x   2ext.   xx case
c xz2x   2ext.   xz case
c wz2x   2ext.   wz case
c wx2x   2ext.   wx case
c yx3x   3ext.   yx case
c yw3x   3ext.   yw case
c z4x    4ext.   z case
c y4x    4ext.   y case
c x4x    4ext.   x case
c w4x    4ext.   w case
c


C
c
c     # local:
c
      do 420 lxyz = 1, 3
        if(hint3(lxyz) .eq. 0.0) goto 420
        hint = hint3(lxyz)
        sval = max(wtlphd, wtbrhd)/2
        delb = 1
        if(wtbrhd .eq. wtlphd)delb = 2
c       write(6,*) 'segsm1,segsm2=',segsm1,segsm2
c
c  compared to 5.8  some integralroutines assumed bra>ket
c   while others assumed ket < bra
c
c
c       write(6,*) 'm=1..',numnz(lxyz, sval, delb)
        do 520 m = 1, numnz(lxyz, sval, delb)
          hint = hint3(lxyz)*mtxso(m,lxyz,sval,delb)
          m2 = bms(m,lxyz,delb)
          m1 = kms(m,lxyz,delb)
c         write(6,*) 'm,m1,m2,ix=',m,m1,m2,ix
          if (ix) then
           bra = mlppr+m2
           ket = mlp +m1
          else
           bra = mlppr+m1
           ket = mlp + m2
          endif
c         write(6,*) 'so2s: bra,ket=',bra,ket,
c    .             '(',mlppr,',',mlp,')',hint
c
          if(bra.le.0)go to 520
          if(bra.gt.segsm1)goto 520
          nmb1a=indxbra(bra)
          if(nmb1a.lt.0)go to 520
          nmb1=conftbra(nmb1a)+1
c         if (nmb1.eq.6016) then
c      write(6,*) ix,m1,m2,'nmb1,ket,nmb2a,nmb2',
c    .  nmb1,ket,segsm2,indxket(ket),conftket(indxket(ket))+1
c       endif
          if(ket.le.0)go to 520
          if(ket.gt.segsm2)goto 520
          nmb2a=indxket(ket)
          if(nmb2a.lt.0)go to 520
          nmb2=conftket(nmb2a)+1
525       sy=indsymbra(nmb1a)
          sym=sym21(sy)
c          write(6,30030)mlppr,m1lpr,mlp,m1lp,nmb1,nmb2,
c    &     sym,jump,cist1,cist2
          go to (600,610,630,630),jump
600       continue
c**********************************************************************
c
c      walks passing through z
c
c**********************************************************************
         zz0xso=zz0xso+1
         go to 520

c**********************************************************************
c
c      walks passing through y
c
c**********************************************************************
610       continue
c          goto 520
          ny=nbas(sym)
          if(ny.eq.0)go to 520
         yy0xso=yy0xso+1
          go to 520
c**********************************************************************
c
c      walks passing through x or w
c
c**********************************************************************
630       continue
          if(jump.eq.3)then
            nij=cnfx(sym)
            xx0xso=xx0xso+1
          elseif (jump.eq.4) then
            nij=cnfw(sym)
            ww0xso=ww0xso+1
          else
           call bummer('invalid jump',jump,2)
          endif
520     enddo
420   enddo
      return
30030 format('so2s',20i6)
      end
      subroutine sotbl
c
c  modified by z. zhang 31-mar-00 from an earlier version by
c  s. yabushita
c
         IMPLICIT NONE

C
C     Parameter variables
C
      INTEGER         MAXS
      PARAMETER           (MAXS = 11)
      INTEGER         MAXNUM
      PARAMETER           (MAXNUM = 134)
C
      REAL*8            ONE
      PARAMETER           (ONE = 1D0)
      REAL*8            HALF
      PARAMETER           (HALF = 5D-1)
      REAL*8            HALFM
      PARAMETER           (HALFM = -5D-1)
      REAL*8            TWO
      PARAMETER           (TWO = 2D0)
      REAL*8            THRE
      PARAMETER           (THRE = 3D0)
C
C     Local variables
C
      INTEGER         I,           J,           M,           MBRA
      INTEGER         MKET,        MP,          NLX,         NLY
      INTEGER         NLZ,         S,           SP
C
      REAL*8            GMS,         HMS,         PMS,         PMSM
      REAL*8            QMS
C
c
c     # numnz(lxyz, s, delb):
c       number of non zero lxyz coupling coef. < s',bms | lxyz | s, kms
c
c     # bms  (i, lxyz, delb):
c       bra ms value of the ith non zero lxyz coupling coef.
c       bms = 1 ... 2s' + 1, same data structure for all s values
c
c     # kms  (i, lxyz, delb):
c       ket ms value of the ith non zero lxyz coupling coef.
c       kms = 1 ... 2s  + 1, same data structure for all s values
c
c     # mtxso(i, lxyz, s, delb):
c       ith non zero coupling coefficient of lxyz
c
c
C     Common variables
C
      INTEGER         BMS(MAXNUM,3,2),          KMS(MAXNUM,3,2)
      INTEGER         NUMNZ(3,MAXS,2)
C
      REAL*8            MTXSO(MAXNUM,3,MAXS,2)
C
      COMMON /CMTXSO /  BMS,         KMS,         NUMNZ,       MTXSO
C
c  some internal tables for
c  computation of so-loops
c
c
c     # delb = 1 case, s' = s - 1
c
      do 40 sp = 1, maxs
        numnz(1,sp,1) = 4*(sp-1)
        numnz(2,sp,1) = 4*(sp-1)
        numnz(3,sp,1) = 2*sp-1
        numnz(1,1,1) = 1
        numnz(2,1,1) = 1
c
        s=sp-1
c
        m = 0
        pms = sqrt((s+m+one)*(s+m+two))/
     &        sqrt(two*(two*s+one)*(two*s+thre))
        qms = sqrt((s+m+one)*(s-m+one))/
     &        sqrt((two*s+one)*(two*s+thre))
c
        bms(1,1,1) = 1
        kms(1,1,1) = 3
        mtxso(1,1,sp,1) = -pms
c        mtxso(1,1,sp,1) = pms
c
        bms(1,2,1) = 1
        kms(1,2,1) = 2
        mtxso(1,2,sp,1) = -pms
c        mtxso(1,2,sp,1) = pms
c
        bms(1,3,1) = 1
        kms(1,3,1) = 1
        mtxso(1,3,sp,1) = -qms
c        mtxso(1,3,sp,1) = qms
        if(sp.eq.1) goto 40
c
        m = 1
        pms = sqrt((s+m+one)*(s+m+two))/
     &        sqrt(two*(two*s+one)*(two*s+thre))
        pmsm = sqrt((s-m+one)*(s-m+two))/
     &         sqrt(two*(two*s+one)*(two*s+thre))
        qms = sqrt((s+m+one)*(s-m+one))/
     &        sqrt((two*s+one)*(two*s+thre))
c
c       # lx
c
        bms(2,1,1)=3
        kms(2,1,1)=1
        mtxso(2,1,sp,1)=pmsm
c        mtxso(2,1,sp,1)=-pmsm
c
        bms(3,1,1)=2
        kms(3,1,1)=4
        mtxso(3,1,sp,1)=-pms/sqrt(two)
c        mtxso(3,1,sp,1)=pms/sqrt(two)
c
        bms(4,1,1)=3
        kms(4,1,1)=5
        mtxso(4,1,sp,1)=-pms/sqrt(two)
c        mtxso(4,1,sp,1)=pms/sqrt(two)

c
c       # ly
c
        bms(2,2,1)=2
        kms(2,2,1)=1
        mtxso(2,2,sp,1)=pmsm
c        mtxso(2,2,sp,1)=-pmsm
c
        bms(3,2,1)=2
        kms(3,2,1)=5
        mtxso(3,2,sp,1)=pms/sqrt(two)
c        mtxso(3,2,sp,1)=-pms/sqrt(two)
c
        bms(4,2,1)=3
        kms(4,2,1)=4
        mtxso(4,2,sp,1)=-pms/sqrt(two)
c        mtxso(4,2,sp,1)=pms/sqrt(two)
c
c       # lz
c
        bms(2,3,1)=2
        kms(2,3,1)=2
        mtxso(2,3,sp,1)=-qms
c        mtxso(2,3,sp,1)=qms
c
        bms(3,3,1)=3
        kms(3,3,1)=3
        mtxso(3,3,sp,1)=-qms
c        mtxso(3,3,sp,1)=qms
        if(sp.eq.2) goto 40
c
c       # m = 2 to s
c
        do 30 m = 2, s
          pms = sqrt((s+m+one)*(s+m+two))/
     &          sqrt(two*(two*s+one)*(two*s+thre))/sqrt(two)
          pmsm = sqrt((s-m+one)*(s-m+two))/
     &           sqrt(two*(two*s+one)*(two*s+thre))/sqrt(two)
          qms = sqrt((s+m+one)*(s-m+one))/
     &          sqrt((two*s+one)*(two*s+thre))
          nlx = numnz(1,m,1)+1
          nly = numnz(2,m,1)+1
          nlz = numnz(3,m,1)+1
          do 20 mp = m-1, m+1, 2
            bms(nlx,1,1) = 2*m
            kms(nlx,1,1) = 2*mp
            mtxso(nlx,1,sp,1)=pmsm
c            mtxso(nlx,1,sp,1)=-pmsm
            if(mp.eq.m+1) then
               mtxso(nlx,1,sp,1)=-pms
c               mtxso(nlx,1,sp,1)=pms
            endif
c
            bms(nlx+1,1,1) = 2*m+1
            kms(nlx+1,1,1) = 2*mp+1
            mtxso(nlx+1,1,sp,1)=pmsm
c            mtxso(nlx+1,1,sp,1)=-pmsm
            if(mp.eq.m+1) then
              mtxso(nlx+1,1,sp,1)=-pms
c              mtxso(nlx+1,1,sp,1)=pms
            endif
c
            bms(nly,2,1) = 2*m
            kms(nly,2,1) = 2*mp+1
            mtxso(nly,2,sp,1)=pmsm
c            mtxso(nly,2,sp,1)=-pmsm
            if(mp.eq.m+1) then
               mtxso(nly,2,sp,1)=pms
c               mtxso(nly,2,sp,1)=-pms
            endif
c
            bms(nly+1,2,1) = 2*m+1
            kms(nly+1,2,1) = 2*mp
            mtxso(nly+1,2,sp,1)=-pmsm
c            mtxso(nly+1,2,sp,1)=pmsm
            if(mp.eq.m+1) then
              mtxso(nly+1,2,sp,1)=-pms
c              mtxso(nly+1,2,sp,1)=pms
            endif
c
            nlx = nlx+2
            nly = nly+2
   20     enddo

          bms(nlz,3,1)=2*m
          kms(nlz,3,1)=2*m
          mtxso(nlz,3,sp,1)=-qms
c          mtxso(nlz,3,sp,1)=qms
c
          bms(nlz+1,3,1)=2*m+1
          kms(nlz+1,3,1)=2*m+1
          mtxso(nlz+1,3,sp,1)=-qms
c          mtxso(nlz+1,3,sp,1)=qms
   30   enddo
   40 enddo
c
c     # delb = 2 case, s' = s
c
      do 90 s = 1, maxs
        numnz(1,s,2) = 4*s-2
        numnz(2,s,2) = 4*s-2
        numnz(3,s,2) = 2*s
c
c       # lx, 2 non zero elements
c
        bms(1, 1, 2) = 1
        kms(1, 1, 2) = 2
        mtxso(1, 1, s, 2) = halfm
c
        bms(2, 1, 2) = 2
        kms(2, 1, 2) = 1
        mtxso(2, 1, s, 2) = half
c
c       # ly, 2 non zero elements
c
        bms(1, 2, 2) = 1
        kms(1, 2, 2) = 3
        mtxso(1, 2, s, 2) = half
c
        bms(2, 2, 2) = 3
        kms(2, 2, 2) = 1
        mtxso(2, 2, s, 2) = halfm
c
c       # lz, 2 non zero elements
c
        m=1
        hms = m/sqrt(two*s*(s+one))
        bms(1, 3, 2) = 2
        kms(1, 3, 2) = 3
        mtxso(1, 3, s, 2) = -hms
c
        bms(2, 3, 2) = 3
        kms(2, 3, 2) = 2
        mtxso(2, 3, s, 2) = hms
c
        do 80 m = 2, s
          gms = half*sqrt((s-m+one)*(s+m))/
     &          sqrt(two*s*(s+one))
          hms = m/sqrt(two*s*(s+one))
c
          nlx = numnz(1,m-1,2)+1
          mbra = 2*m - 3
          mket = 2*m + 2
          do 50 i = 1, 4
            bms(nlx,1,2) = mbra + i
            kms(nlx,1,2) = mket - i
            mtxso(nlx,1,s,2) = gms
            if(mod(i,2) .eq. 0) mtxso(nlx,1,s,2) = -gms
            nlx = nlx+1
   50     enddo
c
          nly = numnz(2,m-1,2)+1
          do 70 i = 1, 2
            mbra = m+i-2
            mket = m-i+1
            if(i .eq. 2) gms = -gms
            do 60 j = 1, 2
              bms(nly,2,2) = 2*mbra+j-1
              kms(nly,2,2) = 2*mket+j-1
              mtxso(nly,2,s,2) = gms
              nly = nly + 1
   60       enddo
   70     enddo
c
          nlz = numnz(3,m-1,2)
          bms(nlz+1,3,2) = 2*m
          kms(nlz+1,3,2) = 2*m+1
          mtxso(nlz+1,3,s,2) = -hms
c
          bms(nlz+2,3,2) = 2*m+1
          kms(nlz+2,3,2) = 2*m
          mtxso(nlz+2,3,s,2) = hms
   80   enddo
   90 enddo
c
      return
      end
      subroutine so0int( conftbra,indsymbra,
     . indxbra,cibra,sgmabra,
     . conftket, indsymket,indxket,ciket,sgmaket,
     &       intbuf,       lpbuf,
     &       scrj,         scr1,         scr2,         scr3,
     &       segsm,        cist ,task)

c
c     intbuf   buffer for two electron integrals
c     lpbuf    buffer for formula tape
c     scfj,scrk,scrkt j and k matrices
c     scr1,scr2,scr3 scratch matrices
c     segsm(2) number of internal walks in segments 1 and 2
c     cist(2)   starting number for the ci vector in segments 1 and 2
c     task  61 yy
c           62 xx
c           63 wx
c
         IMPLICIT NONE
C     Common variables
C
      INTEGER         BUFSZI,      BUFSZL,      CONFYZ,      DIMCI
      INTEGER         INTORB,      LAMDA1
      INTEGER         MAXBL2,      MAXLP3
      INTEGER         MINBL3,      MINBL4,      MXBL23,      MXBLD
      INTEGER         MXORB,       N0EXT
      INTEGER         N1EXT,       N2EXT
      INTEGER         N3EXT,       N4EXT
      INTEGER         ND0EXT,      ND2EXT,      ND4EXT,      NELI
      INTEGER         NEXT,        NFCT,        NINTPT
      INTEGER         NMONEL,      NVALW,       NVALWK
      INTEGER         NVALX,       NVALY,       NVALZ,       PTHW
      INTEGER         PTHX,        PTHY,        PTHYZ,       PTHZ
      INTEGER         SPCCI,       TOTSPC
C
      COMMON /INF    /  PTHZ,        PTHY,        PTHX,        PTHW
      COMMON /INF    /  NINTPT,      DIMCI
      COMMON /INF    /  LAMDA1,      BUFSZI
      COMMON /INF    /  NMONEL,      INTORB
      COMMON /INF    /  MAXLP3,      BUFSZL
      COMMON /INF    /  MAXBL2,      MXBL23,      MXORB,       MXBLD
      COMMON /INF    /  NEXT,        TOTSPC,      MINBL4,      MINBL3
      COMMON /INF    /  ND4EXT,      ND2EXT,      ND0EXT,      N4EXT
      COMMON /INF    /  N3EXT,       N2EXT,       N1EXT,       N0EXT
      COMMON /INF    /  SPCCI,       CONFYZ,      PTHYZ,       NVALWK
      COMMON /INF    /  NVALZ,       NVALY,       NVALX,       NVALW
      COMMON /INF    /  NFCT,        NELI
C
C
c     BUFSZI      buffer size of the diagonal integral file diagint
c,      BUFSZL,   buffer size of the formula tape files
c      CONFYZ,    number of y plus z configurations
c     DIMCI       total number of configurations
c    INTORB,      number of internal orbitals
c       LAMDA1    gives the number of the totally symmetric irrep in sym
c                 (nonsens in smprep ???? )
c        MAXBL2   maximum number of two-electron integrals to be held in
c                 core for the two-external case
c    MAXLP3       maximum number of loop values to be held in core for t
c                 three-external case, i.e. max. number of loops per l v
c        MINBL3,  minimum number of two electron integrals to be held in
c                 core for the three-external case --> ununsed ????
c    MINBL4,      minimum number of two-electron integrals to be held in
c                 core for the four-external case  ---> unused ???
c    MXBL23,      = MAXBL2/3
c     MXBLD       block size of the diagonal two-electron integrals
c                 i.e. all nd4ex,nd2ex or nd0ex must be kept in memory (
c    MXORB,       maxium number of external orbitals per irrep
c    N0EXT        number of all-internal off-diagonal integrals
c      N1EXT,     number of one-external off-diagonal integrals
c     N2EXT       number of two-external off-diagonal integrals
c     N3EXT,      number of three-external off-diagonal integrals
c      N4EXT      number of four-external off-diagonal integrals
c       ND0EXT,   number of all-internal diagonal integrals
c     ND2EXT,     number of two-external diagonal integrals
c     ND4EXT,     number of four-external diagonal integrals
c    NELI         number of electrons contained in DRT
c        NEXT,    total number of external orbitals
c     NFCT,       total number of frozen core orbitals
c     NINTPT,     total number of (valid and invalid) internal paths
c      NMONEL,    total number of one-electron integrals (nmsym lower tr
c   NVALW,        number of valid w walks
c  NVALWK         total number of valid walks
c     NVALX,      number of valid x walks
c   NVALY,        number of valid y walks
c  NVALZ,         number of valid z walks
c    PTHW         number of internal w paths
c      PTHX,      number of internal x paths
c       PTHY,     number of internal y paths
c       PTHYZ,    number of internal y and z paths
c       PTHZ      number of internal z paths
c       SPCCI,    maximum memory available for CI/sigma vector
c       TOTSPC    total available core memory  (=lcore)



C
C     External functions
C
      EXTERNAL            RL
C
      INTEGER         RL
C
C     Parameter variables
C
      INTEGER         MAXSYM
      PARAMETER           (MAXSYM = 8)
      INTEGER         NFILMX
      PARAMETER           (NFILMX = 55)
      INTEGER         MXUNIT
      PARAMETER           (MXUNIT = 99)
      INTEGER         NVMAX
      PARAMETER           (NVMAX = 40)
      INTEGER         MAXBF
      PARAMETER           (MAXBF = 35000)
      INTEGER         FATERR
      PARAMETER           (FATERR = 2)
      INTEGER         IREAD
      PARAMETER           (IREAD = 1)
C
      REAL*8            ZERO
      PARAMETER           (ZERO = 0.0D0)
C
C     Argument variables
C
      INTEGER conftbra(*),indsymbra(*),indxbra(*),conftket(*)
      INTEGER indsymket(*),indxket(*),SEGSM(2),TASK,CIST(2)
C
      LOGICAL             LSTSEG
C
      REAL*8CIBRA(*),CIKET(*),SGMABRA(*),SGMAKET(*)
      REAL*8            INTBUF(*),   LPBUF(*),    SCR1(*),     SCR2(*)
      REAL*8            SCR3(*),     SCRJ(MXBL23*2,3)
C
C     Local variables
C
      INTEGER         BLSTRA(9,3), FIN,         I,           IJ
      INTEGER         IND1,        IND2,        INTST,       ISYM
      INTEGER         IWX,         JSYM,        LOOP8
      INTEGER         LOOP8_CAL,   LPST,        LXYZ,        MAXBL
      INTEGER         MLPPR0,      MXNMB,       NI,          NI2
      INTEGER         NIJ,         NJ,          NMAX(3),     NMAX1
      INTEGER         NMBINT,      NMBUF,       NMINT1,      RBUFI
      INTEGER         RBUFL,       START,       TOTINT
C
      REAL*8            VALUE,       WORD8
      LOGICAL             IX
C
C     Common variables
C
      INTEGER         BLST1E(MAXSYM),           CNFW(MAXSYM)
      INTEGER         CNFX(MAXSYM),             LMDA(MAXSYM,MAXSYM)
      INTEGER         LMDB(MAXSYM,MAXSYM),      MNBAS
      INTEGER         NBAS(MAXSYM),             NMBPR(MAXSYM)
      INTEGER         NMSYM,       SCRLEN
      INTEGER         STRTBW(MAXSYM,MAXSYM)
      INTEGER         STRTBX(MAXSYM,MAXSYM),    SYM12(MAXSYM)
      INTEGER         SYM21(MAXSYM)
C
      COMMON /DATA   /  SYM12,       SYM21,       NBAS
      COMMON /DATA   /  LMDA,        LMDB,        NMBPR,       STRTBW
      COMMON /DATA   /  STRTBX,      BLST1E,      NMSYM,       MNBAS
      COMMON /DATA   /  CNFX,        CNFW,        SCRLEN
C
c   SYM12 (i)     contains the numbers 1 through 8
c   SYM21 (i)     contains the numbers 1 through 8  (nonsens in smprep!)
c   NBAS  (*)     number of external basis functions per irrep
c-----------------------------------------------------------------------
c   nmbpr (i)     number of symmetry pairs to generate the external exte
c   lmda  (i,j)   symmetry pair j : lmd(i)=lmdb(i,j)*lmda(i,j)
c   lmdb  (i,j)   with  lmdb(i,j).ge.lmda(i,j) , lmdb(i,j).lt.lmdb(i,j+1
c   REQUIRED FOR x,w externals
c-----------------------------------------------------------------------
c   mnbas         maximum number of external basis functions per irrep
c   cnfx  (*)   number of configurations per valid internal x walk of ir
c   cnfw  (*)   number of configurations per valid internal w walk of ir
c   nmsym       number of irreps
c     strtbw (i,wxsym) and strtbx(i,wxsym) give the starting points
c     in the external space for paths going through w and x,respectively
c     i is one of the possible values of lmdb(j,wxsym),j=1,nmbpr(wxsym)
c-----------------------------------------------------------------------
c
c   strtbw(i,sym)   contains the offset of the starting points in the ex
c                   space for a given internal path of w symmetry
c                   i= contains possible values of one irrep of the pair
c   strtbx(i,sym)   contains the offset of the starting points in the ex
c                   space for a given internal path of x symmetry
c                   i= contains possible values of one irrep of the pair
c-----------------------------------------------------------------------
c   blste1e(*)     unused ????
c   scrlen         unused ????
c

C     Common variables
C
      INTEGER         N0INT,       N0LP,        N1INT,       N1LP
      INTEGER         N2INT,       N2LP
C
      COMMON /SOINF  /  N2INT,       N1INT,       N0INT,       N2LP
      COMMON /SOINF  /  N1LP,        N0LP
C
c
c  n2int   two-internal so ints
c  n1int   one-internal so ints
c  n0int   all-external so ints
c  n2lp    number of two-internal loops
c  n1lp    number of one-internal loops
c  n0lp    number of zero-internal loops
c
C     Common variables
C
      INTEGER         NUNITS(NFILMX)
C
      COMMON /CFILES /  NUNITS
c
c nunits(1)     tape6 ,iwrite
c        2      nin,tape5,shout
c        3      nslist
c        4      fildia,ndiagf
c        5      pseudg,filsgm
c        6      filew,hvfile
c        7      filev,civfl
c        8      filind
c        9      filint
c       10      fildrt
c       11      fillpd
c       12      fillp
c       13      filfti,fillpi
c       14      flindx
c       15      ciinv,inufv
c       16      civout,outufv
c       17      fockdg
c       18      d1fl
c       20      fvfile
c       21      filsc4,filcii,scrfil
c       22      filtot
c       23      fiacpf
c       26      filsc5
c       27      mcrest
c       28      ciuvfl
c       29      cirefv
c       30      filden
c       31      ifil4w
c       32      ifil4x
c       33      ifil3w
c       34      ifil3x
c       35      filtpq
c       36      aofile
c       37      aofile2
c       38      drtfil
c       39      filsti
c       42      mocoef,vectrf
c       45      prtap
c

c
c

      integer filint,fillp
      equivalence (filint,nunits(9))
      equivalence (fillp,nunits(12))
C     Common variables
C
      INTEGER         BL0XT,       BL1XT,       BL2XT, BL4XT
      INTEGER         FILV,        FILW,        ISKIP
      INTEGER         NZPTH
C
      COMMON /INF3   /  ISKIP,       NZPTH  , BL4XT
      COMMON /INF3   /  BL2XT,       BL1XT,       BL0XT,       FILV
      COMMON /INF3   /  FILW
C
c BL0XT,          number of blocks of all-internal integrals
c    BL1XT,       number of blocks of one-external integrals
c    BL2XT,       number of blocks of two-external integrals
c   FILV,         unit number of v-vector file
c      FILW,      unit number of w vector file
c      ISKIP      controls operation of mult
c NZPTH           number of valid z walks ( possibly mixed-up with nvalz

C     Common variables
C
      INTEGER         BL0INT,      BL1INT,      BL2INT
C
      COMMON /SOINF3 /  BL2INT,      BL1INT,      BL0INT
C
c
c start records for
c bl0int  all-external so
c bl1int  one-external so
c bl2int  two-external so
c

C     Common variables
C
      INTEGER         DALEN(MXUNIT)
C
      COMMON /DAINFO /  DALEN
C
c    dalen (i)   record length of unit i

C     Common variables
C
      INTEGER         CIST1,       CIST2,       CODE,        KLSYM
      INTEGER         MLP,         MLPPR,       NMIJ,        SGSM1
      INTEGER         SGSM2,       WTBRHD,      WTLPHD
C
      COMMON /CSO0   /  KLSYM,       NMIJ,        WTLPHD,      MLPPR
      COMMON /CSO0   /  MLP,         CODE,        SGSM1,       SGSM2
      COMMON /CSO0   /  CIST1,       CIST2,       WTBRHD
C
c  segment information for SO 0int
      integer multmx
      parameter(multmx=19)
      integer nmulmx
      parameter(nmulmx=9)

c
C
      INTEGER         LXYZIR(3), SPNIR, MULTP,NMUL, HMULT,NEXW
C
      LOGICAL             SKIPSO,      SPNODD,      SPNORB
C
      COMMON /SOLXYZ /  SKIPSO,      SPNORB,      SPNODD,      LXYZIR
      COMMON /SOLXYZ /  SPNIR(multmx,multmx),MULTP(nmulmx), NMUL
      COMMON /SOLXYZ /  HMULT,NEXW(8,4)
C

c
c  SKIPSO   Spinorbit-CI calculation
c  SPNODD   odd spin
c  SPNORB    CI
c  LXYZIR   irreducible representation of the three L components
c  SPNIR
c  MULTP
c  NMUL
c  HMULT    highest multiplicity
c  NEXW     number of external walks per symmetry
c
c
c
C     Common variables
C
      INTEGER         HEADV(MAXBF),             IBF
      INTEGER         ICODEV(MAXBF),            XBTV(MAXBF)
      INTEGER         XKTV(MAXBF), YBTV(MAXBF), YKTV(MAXBF)
      INTEGER         HLTV(MAXBF)
C
      REAL*8            VALV(3,MAXBF)
C
      COMMON /SPECIAL/  VALV,        HEADV,       ICODEV,      XKTV
      COMMON /SPECIAL/  XBTV,        YBTV,        YKTV
      COMMON /SPECIAL/  HLTV, IBF
C
c
c  internal stuff for loop calculation
c
c
c
c     # numval(code) contains the number of loop values for code.
c     integer numval(2:12)
c
c    ##  initialise the elements from common/lcontrol/
c
      call wzero(maxbf*3,valv,1)
      call izero_wr(maxbf,headv,1)
      call izero_wr(maxbf,icodev,1)
      call izero_wr(maxbf,xbtv,1)
      call izero_wr(maxbf,xktv,1)
      call izero_wr(maxbf,ybtv,1)
      call izero_wr(maxbf,yktv,1)
c
c     # initialize necessary stack arrays for FT calculation.
c
      call istack
c
c     # initialize loop8_cal
c
      loop8_cal = 1
c
cso5.5
c
c     #      code=  2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12
c     data numval / 1, 1, 2, 1, 1, 2, 1, 1,  1,  1,  1 /
c
      lpst   = 0
      ind2   = 0
      loop8  = 1
      totint = 0
      maxbl  = maxbl2 + bufszi
      iwx    = 0
      value  = zero
      word8  = zero
      rbufl  = rl( bufszl )
      rbufi  = ( bufszi )
c
      intst = bl4xt+ bl2xt + bl1xt + bl0xt +
     & bl2int + bl1int + 1
        call so0int_ft(.true.,indxbra,indxket,segsm,task)
        loop8_cal=1
c
      mxnmb = 0
      do 100 lxyz = 1, 3
        klsym = lxyzir(lxyz)
c
c       # determination of the block structure of the spin
c       # orbit integrals for given angular momentum operator
c
        nmbint = 0
        nmint1 = 0
        nmij   = nmbpr(klsym)
c
        if ( nmij .eq. 0 ) then
c         # no integrals to process. goto the next lxyz
c
          goto 100
        endif
c
        do 120 ij = 1, nmij
          jsym = lmdb(klsym,ij)
          isym = lmda(klsym,ij)
          ni = nbas(isym)
          if ( isym .eq. jsym ) then
            ni2 = ((ni - 1) * ni) / 2
            nmbint = nmbint + ni2 + ni
            nmint1 = nmint1 + ni2
          else
            nj = nbas(jsym)
            nij = ni * nj
            nmbint = nmbint + nij
          endif
 120    enddo
        mxnmb = max(mxnmb, 2*nmbint)
        totint = totint + nmbint
 100  enddo
c
      if(totint .ne. n0int)then
        write(0, 20001)totint, n0int
        call bummer('so0int: (totint-n0int)=',(totint-n0int),faterr)
      endif
20001 format('incompatible number of so integrals in so0int'
     &     /'number calculated ',i6,'  number from cisrt ',i6)
c
c     # read the integrals into the intbuf(*) buffer.
c
      ind1   = 0
      nmbuf = (totint - 1) / bufszi + 1
c
      ind2 = ind1 + nmbuf * bufszi
c
      if ( ind2 .gt. maxbl ) then
        write(0,20000) ind2, maxbl
        call bummer('so0int: (ind2-maxbl)=',(ind2-maxbl),faterr)
      endif
c
      if ( nmbuf .gt. 0 ) then
        start = ind1 + 1
c
c         call diraccnew(
c    &         filint,        iread,  intbuf(start), rbufi*nmbuf,
c    &         dalen(filint), intst,  fin )
c
          intst = intst + nmbuf
      else
        return
      endif
c
 180  continue
c
c     # process the formula tape
c
c     # word8 contains code,wtbrhd,wtlphd,mlppr,mlp
c     #
c     # wtbrhd   number of upper bra walks .
c     # wtlphd   number of upper ket walks .
c     # mlppr    weight of the bra part of the loop
c     # mlp      weight of the ket part of the loop
c
c
c     # code   0  end of formula tape
c     #        1  end of record
c     #        2  yy
c     #        3  xx
c     #        4  wx
c
        code      = icodev(loop8_cal)
        wtbrhd    = xbtv  (loop8_cal)
        wtlphd    = xktv  (loop8_cal)
        mlppr     = ybtv  (loop8_cal)
        mlp       = yktv  (loop8_cal)
c       write(6,*) 'so0int: code,val=',code,valv(1,loop8_cal)
        loop8_cal = loop8_cal+1
c
c     write(6,80800) iseg1,iseg2,isgflg,code
c
      if ( code .eq. 0 ) then
c       # end of formula tape
        return
      elseif ( code .eq. 1 ) then
c       # read the next buffer of formulas.
          call so0int_ft(.false.,indxbra,indxket,segsm,task)
          loop8_cal = 1
      elseif ( code .eq. 2 ) then
c
c       # yy loop.
c
          value  = valv(1,loop8_cal-1)
          sgsm1 = segsm(1)
          sgsm2 = segsm(2)
          cist1 = cist(1)
          cist2 = cist(2)
c
c       # update the sigma vector.
        do lxyz = 1, 3
          klsym=lxyzir(lxyz)
          call soyy( indsymbra,conftbra,indxbra,indsymket,conftket,
     .               indxket, cibra,sgmabra,ciket,sgmaket,
     &           scrj(1,lxyz), blstra(1,lxyz), lxyz,   value,cist )
        enddo
      elseif ( code .eq. 3 ) then
c
c       # 1b-4b,8b,11b
c
        iwx = 1
          value  = valv(1,loop8_cal-1)
          sgsm1 = segsm(1)
          sgsm2 = segsm(2)
          cist1 = cist(1)
          cist2 = cist(2)
        if ( wtbrhd.gt.wtlphd) then
          ix=.true.
          value=-value
        else
          ix=.false.
        endif
c       ix = (wtbrhd .gt. wtlphd)
        do lxyz = 1, 3
          klsym=lxyzir(lxyz)
c
c  check for correct arguments
c

          call soxxww( indsymbra,indsymket,conftbra,conftket,
     .                 indxbra,indxket,cibra,ciket,sgmabra,sgmaket,
     &                 scrj(1,lxyz), scr1,   scr2,
     &         scr3,   strtbx, iwx,    lxyz,   value,  nmax(lxyz),ix)

        enddo
      elseif ( code .eq. 4 ) then
c
c       # xw paths
c       # 1b type loops
c
c       # the meaning of 1b and 2b must be interchanged.
c
          value = valv(1,loop8_cal-1)
c
c         # potentially w is first segment and x is second.
c
c         # update the sigma vector.
          sgsm1 = segsm(1)
          sgsm2 = segsm(2)
          cist1 = cist(1)
          cist2 = cist(2)
          do lxyz = 1, 3
            klsym = lxyzir(lxyz)
            call sowx(  indsymbra,indsymket,conftbra,conftket,
     &      indxbra,indxket,cibra,sgmabra,ciket,sgmaket,
     &       scrj(1,lxyz), scr1,         scr2,
     &       scr3,         lxyz,         value,        nmax(lxyz)  )
          enddo
      else
        call bummer('so0int: unknown code=',code,faterr)
      endif
c
c     # loop back for the next formula contribution.
      goto 180
c
 3333 format(' 110',20i6)
20000 format(' insufficient space for spin orbit integrals in so0int'
     &     /' space needed ',i6,'  space available ',i6)
20070 format(' so1int. nmb. of int. calculated',i6,' from sort',i6)
80800 format(' new loop value; iseg1,iseg2,isgflg',10i5)
      end
c
      subroutine soyy( indsymbra,conftbra,indxbra,indsymket,conftket,
     .                 indxket,cibra,sgmabra,ciket,sgmaket,
     &                 trans, blstra,lxyz,  value ,CISTRT )
c
c  modified from yy in ciudg6.f by z. zhang 31-mar-00
c
         IMPLICIT NONE

C
C     Parameter variables
C
      INTEGER         MAXSYM
      PARAMETER           (MAXSYM = 8)
      INTEGER         MAXS
      PARAMETER           (MAXS = 11)
      INTEGER         MAXNUM
      PARAMETER           (MAXNUM = 134)
C
C     Argument variables
C
      INTEGER         BLSTRA(9),  LXYZ
      INTEGER   INDSYMBRA(*),CONFTBRA(*),INDXBRA(*),INDXKET(*)
      INTEGER   CONFTKET(*),INDSYMKET(*),CISTRT(*)
C
      REAL*8   CIBRA(*),CIKET(*),SGMABRA(*),SGMAKET(*)
      REAL*8   TRANS(*),    VALUE

C
C     Local variables
C
      INTEGER         DELB,        M1,          M1LP,        M1LPR
      INTEGER         NMB1,        NMB1A,       NMB2,        NMB2A
      INTEGER         SVAL,        YS,          YSPR,        YSYM
      INTEGER         YSYMPR, BRA,KET
C
      LOGICAL             BRNEKT
C
      REAL*8            VAL
C
c
c*start common block twoext -rls 18-dec-89
C     Common variables
C
      INTEGER         CIST1,       CIST2,       CODE,        KLSYM
      INTEGER         MLP,         MLPPR,       NMIJ,        SGSM1
      INTEGER         SGSM2,       WTBRHD,      WTLPHD
C
      COMMON /CSO0   /  KLSYM,       NMIJ,        WTLPHD,      MLPPR
      COMMON /CSO0   /  MLP,         CODE,        SGSM1,       SGSM2
      COMMON /CSO0   /  CIST1,       CIST2,       WTBRHD
C
c  segment information for SO 0int
c*end common block twoext -rls 18-dec-89
c
c*start common block data -eas (8/28/89)
c include(data.common)
C     Common variables
C
      INTEGER         BLST1E(MAXSYM),           CNFW(MAXSYM)
      INTEGER         CNFX(MAXSYM),             LMDA(MAXSYM,MAXSYM)
      INTEGER         LMDB(MAXSYM,MAXSYM),      MNBAS
      INTEGER         NBAS(MAXSYM),             NMBPR(MAXSYM)
      INTEGER         NMSYM,       SCRLEN
      INTEGER         STRTBW(MAXSYM,MAXSYM)
      INTEGER         STRTBX(MAXSYM,MAXSYM),    SYM12(MAXSYM)
      INTEGER         SYM21(MAXSYM)
C
      COMMON /DATA   /  SYM12,       SYM21,       NBAS
      COMMON /DATA   /  LMDA,        LMDB,        NMBPR,       STRTBW
      COMMON /DATA   /  STRTBX,      BLST1E,      NMSYM,       MNBAS
      COMMON /DATA   /  CNFX,        CNFW,        SCRLEN
C
c   SYM12 (i)     contains the numbers 1 through 8
c   SYM21 (i)     contains the numbers 1 through 8  (nonsens in smprep!)
c   NBAS  (*)     number of external basis functions per irrep
c-----------------------------------------------------------------------
c   nmbpr (i)     number of symmetry pairs to generate the external exte
c   lmda  (i,j)   symmetry pair j : lmd(i)=lmdb(i,j)*lmda(i,j)
c   lmdb  (i,j)   with  lmdb(i,j).ge.lmda(i,j) , lmdb(i,j).lt.lmdb(i,j+1
c   REQUIRED FOR x,w externals
c-----------------------------------------------------------------------
c   mnbas         maximum number of external basis functions per irrep
c   cnfx  (*)   number of configurations per valid internal x walk of ir
c   cnfw  (*)   number of configurations per valid internal w walk of ir
c   nmsym       number of irreps
c     strtbw (i,wxsym) and strtbx(i,wxsym) give the starting points
c     in the external space for paths going through w and x,respectively
c     i is one of the possible values of lmdb(j,wxsym),j=1,nmbpr(wxsym)
c-----------------------------------------------------------------------
c
c   strtbw(i,sym)   contains the offset of the starting points in the ex
c                   space for a given internal path of w symmetry
c                   i= contains possible values of one irrep of the pair
c   strtbx(i,sym)   contains the offset of the starting points in the ex
c                   space for a given internal path of x symmetry
c                   i= contains possible values of one irrep of the pair
c-----------------------------------------------------------------------
c   blste1e(*)     unused ????
c   scrlen         unused ????
c

c*end common block data -eas (8/28/89)
c
c     # dummy:
c
c     #spin coupling table
c
C     Common variables
C
      INTEGER         BMS(MAXNUM,3,2),          KMS(MAXNUM,3,2)
      INTEGER         NUMNZ(3,MAXS,2)
C
      REAL*8            MTXSO(MAXNUM,3,MAXS,2)
C
      COMMON /CMTXSO /  BMS,         KMS,         NUMNZ,       MTXSO
C
c  some internal tables for
c  computation of so-loops


      INTEGER DG0X,DG2X,DG4X,ZZ0X,YY0X,XX0X,WW0X
      INTEGER YZ1X,YX1X,YW1X,YY2EX,WW2X,XX2X,XZ2X,WZ2X,WX2X
      INTEGER YX3X,YW3X,Z4X,Y4X,X4X,W4X
      INTEGER ZZ0XSO,YY0XSO,XX0XSO,WW0XSO
      INTEGER YZ1XSO,YX1XSO,YW1XSO,YY2XSO,XX2XSO,WX2XSO
      INTEGER DYZ, DYX, DYW, D0Z,D0Y,D0W,D0X
      INTEGER DDZI, DDYI, DDXI, DDWI, DDZE, DDYE, DDXE, DDWE
      INTEGER XX2XSO2
C
      COMMON /COUNTER/DG0X,DG2X,DG4X,ZZ0X,YY0X,XX0X,WW0X
      COMMON /COUNTER/YZ1X,YX1X,YW1X,YY2EX,WW2X,XX2X,XZ2X,WZ2X,WX2X
      COMMON /COUNTER/YX3X,YW3X,Z4X,Y4X,X4X,W4X
      COMMON /COUNTER/ZZ0XSO,YY0XSO,XX0XSO,WW0XSO
      COMMON /COUNTER/YZ1XSO,YX1XSO,YW1XSO,YY2XSO,XX2XSO,WX2XSO
      COMMON /COUNTER/DYZ, DYX, DYW, D0Z,D0Y,D0W,D0X
      COMMON /COUNTER/DDZI, DDYI, DDXI, DDWI, DDZE, DDYE, DDXE, DDWE
      COMMON/COUNTER/XX2XSO2

c just for error checking
c records the number of contributions
c dg0x  0 ext. diagonal case
c dg2x  2 ext. diagonal case
c dg4x  4 ext. diagonal case
c zz0x  0 ext.  zz case
c yy0x  0 ext.  yy case
c xx0x  0 ext.  xx case
c ww0x  0 ext.  ww case
c yz1x  1 ext.  yz case
c yx1x  1 ext.  yx case
c yw1x  1 ext.  yw case
c yy2ex  2 ext.  yy case
c ww2x   2ext.   ww case
c xx2x   2ext.   xx case
c xz2x   2ext.   xz case
c wz2x   2ext.   wz case
c wx2x   2ext.   wx case
c yx3x   3ext.   yx case
c yw3x   3ext.   yw case
c z4x    4ext.   z case
c y4x    4ext.   y case
c x4x    4ext.   x case
c w4x    4ext.   w case
c


C
c
c     # local:
c
      sval = max(wtlphd, wtbrhd)/2
      delb = 1
      if(wtbrhd .eq. wtlphd) delb = 2
c
      do 610 m1 = 1, numnz(lxyz, sval, delb)
        bra=mlppr+kms(m1,lxyz,delb)
        if(wtbrhd .gt. wtlphd) bra=mlppr+bms(m1,lxyz,delb)
        if (bra.le.0) goto 610
c       if (bra.gt.sgsm1) return
        if (bra.gt.sgsm1) goto 610
        nmb1a=indxbra(bra)
        if(nmb1a.lt.0) go to 610
        nmb1=conftbra(nmb1a)+1
        ket=mlp+bms(m1,lxyz,delb)
        if(wtbrhd .gt. wtlphd) ket=mlp+kms(m1,lxyz,delb)
        if (ket.le.0) goto 610
c       if (ket.gt.sgsm2) return
        if (ket.gt.sgsm2) goto 610
        nmb2a=indxket(ket)
        if(nmb2a.lt.0) go to 610
c
c     dies entfernt die ueberfluessigen Beitraege!!!
c
        if(mlppr .eq. mlp .and. bra .lt. ket) goto 610
        nmb2=conftket(nmb2a)+1
        yspr=indsymbra(nmb1a)
        ys=indsymket(nmb2a)
        ysympr=sym21(yspr)
        ysym=sym21(ys)
        brnekt= .not.((nmb1.eq.nmb2).and.(cist1.eq.cist2))
        val=value*mtxso(m1,lxyz,sval,delb)
        if(wtbrhd .gt. wtlphd) val = -val
c       yy2xso=yy2xso+1
c        write(6,30000)m1,m1lpr,nmb1,m1lp,nmb2,ysympr,ysym
        call soyy2x(    klsym,         ysym,        ysympr,
     +   ciket(nmb2), cibra(nmb1),  sgmaket(nmb2),sgmabra(nmb1),
     +   trans,         blstra,        brnekt,        val )
610   enddo
      return
30000 format(' yy',20i4)
      end
      subroutine soyy2x(    klsym,         symbra,        symket,
     &       cibra,         ciket,         sigbra,        sigket,
     &       tran,          blstra,        brnekt,        val  )
c
c  31-mar-00  modified from yy2x in ciudg6.f by z. zhang
c  10-nov-89  matrix-vector product calls added. -rls
c
         IMPLICIT NONE

C
C     Parameter variables
C
      INTEGER         MAXSYM
      PARAMETER           (MAXSYM = 8)
C
C     Argument variables
C
      INTEGER         BLSTRA(*),   KLSYM,       SYMBRA,      SYMKET
C
      LOGICAL             BRNEKT
C
      REAL*8            CIBRA(*),    CIKET(*),    SIGBRA(*)
      REAL*8            SIGKET(*),   TRAN(*),     VAL
C
C     Local variables
C
      INTEGER         HSTRT,       IJ,          ISYM,        JSYM
      INTEGER         MAXYS,       NI,          NIJ,         NJ
      INTEGER         NMIJ
C
c
c*start common block data -eas (8/28/89)
c include(data.common)
C     Common variables
C
      INTEGER         BLST1E(MAXSYM),           CNFW(MAXSYM)
      INTEGER         CNFX(MAXSYM),             LMDA(MAXSYM,MAXSYM)
      INTEGER         LMDB(MAXSYM,MAXSYM),      MNBAS
      INTEGER         NBAS(MAXSYM),             NMBPR(MAXSYM)
      INTEGER         NMSYM,       SCRLEN
      INTEGER         STRTBW(MAXSYM,MAXSYM)
      INTEGER         STRTBX(MAXSYM,MAXSYM),    SYM12(MAXSYM)
      INTEGER         SYM21(MAXSYM)
C
      COMMON /DATA   /  SYM12,       SYM21,       NBAS
      COMMON /DATA   /  LMDA,        LMDB,        NMBPR,       STRTBW
      COMMON /DATA   /  STRTBX,      BLST1E,      NMSYM,       MNBAS
      COMMON /DATA   /  CNFX,        CNFW,        SCRLEN
C
c   SYM12 (i)     contains the numbers 1 through 8
c   SYM21 (i)     contains the numbers 1 through 8  (nonsens in smprep!)
c   NBAS  (*)     number of external basis functions per irrep
c-----------------------------------------------------------------------
c   nmbpr (i)     number of symmetry pairs to generate the external exte
c   lmda  (i,j)   symmetry pair j : lmd(i)=lmdb(i,j)*lmda(i,j)
c   lmdb  (i,j)   with  lmdb(i,j).ge.lmda(i,j) , lmdb(i,j).lt.lmdb(i,j+1
c   REQUIRED FOR x,w externals
c-----------------------------------------------------------------------
c   mnbas         maximum number of external basis functions per irrep
c   cnfx  (*)   number of configurations per valid internal x walk of ir
c   cnfw  (*)   number of configurations per valid internal w walk of ir
c   nmsym       number of irreps
c     strtbw (i,wxsym) and strtbx(i,wxsym) give the starting points
c     in the external space for paths going through w and x,respectively
c     i is one of the possible values of lmdb(j,wxsym),j=1,nmbpr(wxsym)
c-----------------------------------------------------------------------
c
c   strtbw(i,sym)   contains the offset of the starting points in the ex
c                   space for a given internal path of w symmetry
c                   i= contains possible values of one irrep of the pair
c   strtbx(i,sym)   contains the offset of the starting points in the ex
c                   space for a given internal path of x symmetry
c                   i= contains possible values of one irrep of the pair
c-----------------------------------------------------------------------
c   blste1e(*)     unused ????
c   scrlen         unused ????
c


      INTEGER DG0X,DG2X,DG4X,ZZ0X,YY0X,XX0X,WW0X
      INTEGER YZ1X,YX1X,YW1X,YY2EX,WW2X,XX2X,XZ2X,WZ2X,WX2X
      INTEGER YX3X,YW3X,Z4X,Y4X,X4X,W4X
      INTEGER ZZ0XSO,YY0XSO,XX0XSO,WW0XSO
      INTEGER YZ1XSO,YX1XSO,YW1XSO,YY2XSO,XX2XSO,WX2XSO
      INTEGER DYZ, DYX, DYW, D0Z,D0Y,D0W,D0X
      INTEGER DDZI, DDYI, DDXI, DDWI, DDZE, DDYE, DDXE, DDWE
      INTEGER XX2XSO2
C
      COMMON /COUNTER/DG0X,DG2X,DG4X,ZZ0X,YY0X,XX0X,WW0X
      COMMON /COUNTER/YZ1X,YX1X,YW1X,YY2EX,WW2X,XX2X,XZ2X,WZ2X,WX2X
      COMMON /COUNTER/YX3X,YW3X,Z4X,Y4X,X4X,W4X
      COMMON /COUNTER/ZZ0XSO,YY0XSO,XX0XSO,WW0XSO
      COMMON /COUNTER/YZ1XSO,YX1XSO,YW1XSO,YY2XSO,XX2XSO,WX2XSO
      COMMON /COUNTER/DYZ, DYX, DYW, D0Z,D0Y,D0W,D0X
      COMMON /COUNTER/DDZI, DDYI, DDXI, DDWI, DDZE, DDYE, DDXE, DDWE
      COMMON/COUNTER/XX2XSO2

c*end common block data -eas (8/28/89)
c
*@ifdef debug
*      common/ctestx/itest(40)
*@endif
c
c     # dummy:
c
c     # local:
c
      maxys = max(symbra,symket)
      nmij = nmbpr(klsym)
      do 100 ij = 1, nmij
        jsym = lmdb(klsym,ij)
        isym = lmda(klsym,ij)
        nj = nbas(jsym)
        ni = nbas(isym)
        nij = ni*nj
        if(nij.eq.0) go to 100
c        write(6,30000)ij,jsym,isym,nj,ni,maxys,symbra,symket,hstrt
        if(klsym.ne.1) then
          if(jsym.ne.maxys) go to 100
          if(symket.gt.symbra) then
c
c           # symket=jsym isym=symbra 1a,b
c
            yy2xso=yy2xso+1
          else
c
c           # symket=isym symbra=jsym 2a,b
c
            yy2xso=yy2xso+1
          endif
          return
        endif
c
c       # ksym=lsym symket=symbra
c
        if ( symket .eq. jsym ) then
            yy2xso=yy2xso+1
          return
        endif
  100 enddo
c
      return
30000 format(' yy2x',9i4,2f10.6)
      end
      subroutine soxxww( indsymbra,indsymket,conftbra,conftket,
     .    indxbra,indxket,cibra,ciket,sgmabra,sgmaket,
     &           trans1, scr1,   scr2,
     &           scr3,   strtab, iwx,    lxyz,   value,  nmax ,ix )
c
c     two external x'-x and w'-w
c
c     modified from xxww in ciudg6.f by z. zhang 31-mar-00
c
         IMPLICIT NONE

C
C     Parameter variables
C
      INTEGER         MAXSYM
      PARAMETER           (MAXSYM = 8)
      INTEGER         MAXS
      PARAMETER           (MAXS = 11)
      INTEGER         MAXNUM
      PARAMETER           (MAXNUM = 134)
C
C     Argument variables
C
      INTEGER  INDSYMBRA(*),INDSYMKET(*),CONFTBRA(*),CONFTKET(*)
      INTEGER  INDXBRA(*),INDXKET(*),IWX,LXYZ,NMAX
      INTEGER         STRTAB(8,8)
      LOGICAL             IX
C
      REAL*8 CIBRA(*),CIKET(*),SGMABRA(*),SGMAKET(*), SCR1(*),SCR2(*)
      REAL*8 SCR3(*),TRANS1(*),   VALUE
C
C     Local variables
C
      INTEGER         DELB,        I,           M1,          M1LP
      INTEGER         M1LPR,       NMB1,        NMB1A,       NMB2
      INTEGER         NMB2A,       SVAL,        XS,          XSPR
      INTEGER         XSYM,        XSYMPR, BRA,KET, mm1,mm2
C
      LOGICAL             BRNEKT
C
      REAL*8            TRANS(10000),             VAL
C
C     Common variables
C
      INTEGER         BLST1E(MAXSYM),           CNFW(MAXSYM)
      INTEGER         CNFX(MAXSYM),             LMDA(MAXSYM,MAXSYM)
      INTEGER         LMDB(MAXSYM,MAXSYM),      MNBAS
      INTEGER         NBAS(MAXSYM),             NMBPR(MAXSYM)
      INTEGER         NMSYM,       SCRLEN
      INTEGER         STRTBW(MAXSYM,MAXSYM)
      INTEGER         STRTBX(MAXSYM,MAXSYM),    SYM12(MAXSYM)
      INTEGER         SYM21(MAXSYM)
C
      COMMON /DATA   /  SYM12,       SYM21,       NBAS
      COMMON /DATA   /  LMDA,        LMDB,        NMBPR,       STRTBW
      COMMON /DATA   /  STRTBX,      BLST1E,      NMSYM,       MNBAS
      COMMON /DATA   /  CNFX,        CNFW,        SCRLEN
C
c   SYM12 (i)     contains the numbers 1 through 8
c   SYM21 (i)     contains the numbers 1 through 8  (nonsens in smprep!)
c   NBAS  (*)     number of external basis functions per irrep
c-----------------------------------------------------------------------
c   nmbpr (i)     number of symmetry pairs to generate the external exte
c   lmda  (i,j)   symmetry pair j : lmd(i)=lmdb(i,j)*lmda(i,j)
c   lmdb  (i,j)   with  lmdb(i,j).ge.lmda(i,j) , lmdb(i,j).lt.lmdb(i,j+1
c   REQUIRED FOR x,w externals
c-----------------------------------------------------------------------
c   mnbas         maximum number of external basis functions per irrep
c   cnfx  (*)   number of configurations per valid internal x walk of ir
c   cnfw  (*)   number of configurations per valid internal w walk of ir
c   nmsym       number of irreps
c     strtbw (i,wxsym) and strtbx(i,wxsym) give the starting points
c     in the external space for paths going through w and x,respectively
c     i is one of the possible values of lmdb(j,wxsym),j=1,nmbpr(wxsym)
c-----------------------------------------------------------------------
c
c   strtbw(i,sym)   contains the offset of the starting points in the ex
c                   space for a given internal path of w symmetry
c                   i= contains possible values of one irrep of the pair
c   strtbx(i,sym)   contains the offset of the starting points in the ex
c                   space for a given internal path of x symmetry
c                   i= contains possible values of one irrep of the pair
c-----------------------------------------------------------------------
c   blste1e(*)     unused ????
c   scrlen         unused ????
c

C     Common variables
C
      INTEGER         CIST1,       CIST2,       CODE,        KLSYM
      INTEGER         MLP,         MLPPR,       NMIJ,        SGSM1
      INTEGER         SGSM2,       WTBRHD,      WTLPHD
C
      COMMON /CSO0   /  KLSYM,       NMIJ,        WTLPHD,      MLPPR
      COMMON /CSO0   /  MLP,         CODE,        SGSM1,       SGSM2
      COMMON /CSO0   /  CIST1,       CIST2,       WTBRHD
C
c  segment information for SO 0int

      INTEGER DG0X,DG2X,DG4X,ZZ0X,YY0X,XX0X,WW0X
      INTEGER YZ1X,YX1X,YW1X,YY2EX,WW2X,XX2X,XZ2X,WZ2X,WX2X
      INTEGER YX3X,YW3X,Z4X,Y4X,X4X,W4X
      INTEGER ZZ0XSO,YY0XSO,XX0XSO,WW0XSO
      INTEGER YZ1XSO,YX1XSO,YW1XSO,YY2XSO,XX2XSO,WX2XSO
      INTEGER DYZ, DYX, DYW, D0Z,D0Y,D0W,D0X
      INTEGER DDZI, DDYI, DDXI, DDWI, DDZE, DDYE, DDXE, DDWE
      INTEGER XX2XSO2
C
      COMMON /COUNTER/DG0X,DG2X,DG4X,ZZ0X,YY0X,XX0X,WW0X
      COMMON /COUNTER/YZ1X,YX1X,YW1X,YY2EX,WW2X,XX2X,XZ2X,WZ2X,WX2X
      COMMON /COUNTER/YX3X,YW3X,Z4X,Y4X,X4X,W4X
      COMMON /COUNTER/ZZ0XSO,YY0XSO,XX0XSO,WW0XSO
      COMMON /COUNTER/YZ1XSO,YX1XSO,YW1XSO,YY2XSO,XX2XSO,WX2XSO
      COMMON /COUNTER/DYZ, DYX, DYW, D0Z,D0Y,D0W,D0X
      COMMON /COUNTER/DDZI, DDYI, DDXI, DDWI, DDZE, DDYE, DDXE, DDWE
      COMMON /COUNTER/XX2XSO2

c just for error checking
c records the number of contributions
c dg0x  0 ext. diagonal case
c dg2x  2 ext. diagonal case
c dg4x  4 ext. diagonal case
c zz0x  0 ext.  zz case
c yy0x  0 ext.  yy case
c xx0x  0 ext.  xx case
c ww0x  0 ext.  ww case
c yz1x  1 ext.  yz case
c yx1x  1 ext.  yx case
c yw1x  1 ext.  yw case
c yy2ex  2 ext.  yy case
c ww2x   2ext.   ww case
c xx2x   2ext.   xx case
c xz2x   2ext.   xz case
c wz2x   2ext.   wz case
c wx2x   2ext.   wx case
c yx3x   3ext.   yx case
c yw3x   3ext.   yw case
c z4x    4ext.   z case
c y4x    4ext.   y case
c x4x    4ext.   x case
c w4x    4ext.   w case
c


C
C     Common variables
C
      INTEGER         BMS(MAXNUM,3,2),          KMS(MAXNUM,3,2)
      INTEGER         NUMNZ(3,MAXS,2)
C
      REAL*8            MTXSO(MAXNUM,3,MAXS,2)
C
      COMMON /CMTXSO /  BMS,         KMS,         NUMNZ,       MTXSO
C
c  some internal tables for
c  computation of so-loops
c
c     # local:
c      real*8 val, trans(nmax)
c
      if (iwx.ne.1) call bummer ('soxxww: iwx=',iwx,2)
      sval = max(wtlphd, wtbrhd)/2
      delb = 1
      if(wtbrhd .eq. wtlphd)delb = 2
c
      do 1210 m1=1,numnz(lxyz, sval, delb)

c       bra=mlppr+kms(m1,lxyz,delb)
c       ket=mlp+bms(m1,lxyz,delb)
c
c  assumed the old program bra>ket for xx ww or vice versa?
c

          mm2 = bms(m1,lxyz,delb)
          mm1 = kms(m1,lxyz,delb)
          if (ix) then
           bra = mlppr+mm2
           ket = mlp +mm1
          else
           bra = mlppr+mm1
           ket = mlp + mm2
          endif
c       write(6,*) 'soxxww: loop1210:',lxyz,m1,'braket',bra,ket
        if(bra.le.0) go to 1210
c       if(bra.gt.sgsm1)return
        if(bra.gt.sgsm1) goto 1210
        nmb1a=indxbra(bra)
        if(nmb1a.lt.0) go to 1210
        nmb1=conftbra(nmb1a)+1
        if(ket.le.0) go to 1210
c       if(ket.gt.sgsm2)return
        if(ket.gt.sgsm2) goto 1210
        nmb2a=indxket(ket)
        if(nmb2a.lt.0) go to 1210
        nmb2=conftket(nmb2a)+1
        xspr=indsymbra(nmb1a)
        xsympr=sym21(xspr)
        xs=indsymket(nmb2a)
        xsym=sym21(xs)
        brnekt=nmb1+cist1.ne.nmb2+cist2
        val=value*mtxso(m1,lxyz,sval,delb)
        do 1211 i = 1, nmax
          trans(i) = val * trans1(i)
1211    enddo
c
c       xx2xso=xx2xso+1

        call soxxww2x(  klsym,         xsympr,        xsym  ,
     +   cibra(nmb1),   ciket(nmb2),  sgmabra(nmb1), sgmaket(nmb2),
     +   trans,         strtab,        scr1,          scr2,
     +   scr3,          brnekt,        iwx,           0  )
c
c       # switch should be 1 and the following if should be executed
c        goto 1210
        if(klsym.ne.1.and.mlppr.ne.mlp)then
c  thomas
c       xx2xso2=xx2xso2+1
          call soxxww2x(  klsym,         xsym  ,        xsympr,
     +     ciket(nmb2),   cibra(nmb1), sgmaket(nmb2), sgmabra(nmb1),
     +     trans,         strtab,        scr1,          scr2,
     +     scr3,          brnekt,        iwx,           1  )
c
        endif


c
1210  enddo
      return
      end
      subroutine sowx(indsymbra,indsymket,conftbra,conftket,indxbra,
     &  indxket,cibra,sgmabra,ciket,sgmaket,
     &       tran,         scr1,         scr2,
     &       scr3,         lxyz,         value,        nmax  )
c
c     two external w'-x
c
c     modified from xw in ciudg6.f by z. zhang 31-mar-00
c
         IMPLICIT NONE

C
C     Parameter variables
C
      INTEGER         MAXSYM
      PARAMETER           (MAXSYM = 8)
      INTEGER         MAXS
      PARAMETER           (MAXS = 11)
      INTEGER         MAXNUM
      PARAMETER           (MAXNUM = 134)
C
C     Argument variables
C
      INTEGER         LXYZ,        NMAX
      INTEGER INDSYMBRA(*),INDSYMKET(*),CONFTBRA(*),CONFTKET(*)
      INTEGER INDXBRA(*),INDXKET(*)
C
      REAL*8      CIBRA(*),CIKET(*),SGMABRA(*),   SCR1(*),     SCR2(*)
      REAL*8            SCR3(*),  SGMAKET(*),   TRAN(*)
      REAL*8            VALUE
C
C     Local variables
C
      INTEGER         DELB,        I,           M1,          M1LP
      INTEGER         M1LPR,       NMB1,        NMB1A,       NMB2
      INTEGER         NMB2A,       SVAL,        WS,          WSYM
      INTEGER         XS,          XSYM, BRA,KET
C
      REAL*8            TRANS(10000),             VAL
C
c
c*start common block data -eas (8/28/89)
c include(data.common)
C     Common variables
C
      INTEGER         BLST1E(MAXSYM),           CNFW(MAXSYM)
      INTEGER         CNFX(MAXSYM),             LMDA(MAXSYM,MAXSYM)
      INTEGER         LMDB(MAXSYM,MAXSYM),      MNBAS
      INTEGER         NBAS(MAXSYM),             NMBPR(MAXSYM)
      INTEGER         NMSYM,       SCRLEN
      INTEGER         STRTBW(MAXSYM,MAXSYM)
      INTEGER         STRTBX(MAXSYM,MAXSYM),    SYM12(MAXSYM)
      INTEGER         SYM21(MAXSYM)
C
      COMMON /DATA   /  SYM12,       SYM21,       NBAS
      COMMON /DATA   /  LMDA,        LMDB,        NMBPR,       STRTBW
      COMMON /DATA   /  STRTBX,      BLST1E,      NMSYM,       MNBAS
      COMMON /DATA   /  CNFX,        CNFW,        SCRLEN
C
c   SYM12 (i)     contains the numbers 1 through 8
c   SYM21 (i)     contains the numbers 1 through 8  (nonsens in smprep!)
c   NBAS  (*)     number of external basis functions per irrep
c-----------------------------------------------------------------------
c   nmbpr (i)     number of symmetry pairs to generate the external exte
c   lmda  (i,j)   symmetry pair j : lmd(i)=lmdb(i,j)*lmda(i,j)
c   lmdb  (i,j)   with  lmdb(i,j).ge.lmda(i,j) , lmdb(i,j).lt.lmdb(i,j+1
c   REQUIRED FOR x,w externals
c-----------------------------------------------------------------------
c   mnbas         maximum number of external basis functions per irrep
c   cnfx  (*)   number of configurations per valid internal x walk of ir
c   cnfw  (*)   number of configurations per valid internal w walk of ir
c   nmsym       number of irreps
c     strtbw (i,wxsym) and strtbx(i,wxsym) give the starting points
c     in the external space for paths going through w and x,respectively
c     i is one of the possible values of lmdb(j,wxsym),j=1,nmbpr(wxsym)
c-----------------------------------------------------------------------
c
c   strtbw(i,sym)   contains the offset of the starting points in the ex
c                   space for a given internal path of w symmetry
c                   i= contains possible values of one irrep of the pair
c   strtbx(i,sym)   contains the offset of the starting points in the ex
c                   space for a given internal path of x symmetry
c                   i= contains possible values of one irrep of the pair
c-----------------------------------------------------------------------
c   blste1e(*)     unused ????
c   scrlen         unused ????
c

c*end common block data -eas (8/28/89)
c
c*start common block twoext -rls 18-dec-89
C     Common variables
C
      INTEGER         CIST1,       CIST2,       CODE,        KLSYM
      INTEGER         MLP,         MLPPR,       NMIJ,        SGSM1
      INTEGER         SGSM2,       WTBRHD,      WTLPHD
C
      COMMON /CSO0   /  KLSYM,       NMIJ,        WTLPHD,      MLPPR
      COMMON /CSO0   /  MLP,         CODE,        SGSM1,       SGSM2
      COMMON /CSO0   /  CIST1,       CIST2,       WTBRHD
C
c  segment information for SO 0int


      INTEGER DG0X,DG2X,DG4X,ZZ0X,YY0X,XX0X,WW0X
      INTEGER YZ1X,YX1X,YW1X,YY2EX,WW2X,XX2X,XZ2X,WZ2X,WX2X
      INTEGER YX3X,YW3X,Z4X,Y4X,X4X,W4X
      INTEGER ZZ0XSO,YY0XSO,XX0XSO,WW0XSO
      INTEGER YZ1XSO,YX1XSO,YW1XSO,YY2XSO,XX2XSO,WX2XSO
      INTEGER DYZ, DYX, DYW, D0Z,D0Y,D0W,D0X
      INTEGER DDZI, DDYI, DDXI, DDWI, DDZE, DDYE, DDXE, DDWE
      INTEGER XX2XSO2
C
      COMMON /COUNTER/DG0X,DG2X,DG4X,ZZ0X,YY0X,XX0X,WW0X
      COMMON /COUNTER/YZ1X,YX1X,YW1X,YY2EX,WW2X,XX2X,XZ2X,WZ2X,WX2X
      COMMON /COUNTER/YX3X,YW3X,Z4X,Y4X,X4X,W4X
      COMMON /COUNTER/ZZ0XSO,YY0XSO,XX0XSO,WW0XSO
      COMMON /COUNTER/YZ1XSO,YX1XSO,YW1XSO,YY2XSO,XX2XSO,WX2XSO
      COMMON /COUNTER/DYZ, DYX, DYW, D0Z,D0Y,D0W,D0X
      COMMON /COUNTER/DDZI, DDYI, DDXI, DDWI, DDZE, DDYE, DDXE, DDWE
      COMMON/COUNTER/XX2XSO2

c just for error checking
c records the number of contributions
c dg0x  0 ext. diagonal case
c dg2x  2 ext. diagonal case
c dg4x  4 ext. diagonal case
c zz0x  0 ext.  zz case
c yy0x  0 ext.  yy case
c xx0x  0 ext.  xx case
c ww0x  0 ext.  ww case
c yz1x  1 ext.  yz case
c yx1x  1 ext.  yx case
c yw1x  1 ext.  yw case
c yy2ex  2 ext.  yy case
c ww2x   2ext.   ww case
c xx2x   2ext.   xx case
c xz2x   2ext.   xz case
c wz2x   2ext.   wz case
c wx2x   2ext.   wx case
c yx3x   3ext.   yx case
c yw3x   3ext.   yw case
c z4x    4ext.   z case
c y4x    4ext.   y case
c x4x    4ext.   x case
c w4x    4ext.   w case
c


C
c*end common block twoext -rls 18-dec-89
c
c     # dummy:
c
c
c     #spin coupling table
c
C     Common variables
C
      INTEGER         BMS(MAXNUM,3,2),          KMS(MAXNUM,3,2)
      INTEGER         NUMNZ(3,MAXS,2)
C
      REAL*8            MTXSO(MAXNUM,3,MAXS,2)
C
      COMMON /CMTXSO /  BMS,         KMS,         NUMNZ,       MTXSO
C
c  some internal tables for
c  computation of so-loops
c
c
c     # local:
c      real*8 val, trans(nmax)
c
c
      sval = max(wtlphd, wtbrhd)/2
      delb=2
      if(wtbrhd .ne. wtlphd)delb=1
c
      do 2410 m1=1,numnz(lxyz, sval, delb)
        val=-value
        if(wtbrhd .le. wtlphd)then
          bra=mlppr+bms(m1,lxyz,delb)
        else
          bra=mlppr+kms(m1,lxyz,delb)
        endif
        if(bra.le.0) go to 2410
c       if(bra.gt.sgsm1)return
        if(bra.gt.sgsm1) goto 2410
        if(wtbrhd .le. wtlphd)then
          ket=mlp+kms(m1,lxyz,delb)
        else
          ket=mlp+bms(m1,lxyz,delb)
          val = -val
        endif
        if(ket.le.0) go to 2410
c       if(ket.gt.sgsm2)return
        if(ket.gt.sgsm2) goto 2410
        nmb1a=indxket(ket)
        nmb2a=indxbra(bra)
        if(nmb1a.lt.0) go to 2410
        nmb1=conftket(nmb1a)+1
        nmb2=conftbra(nmb2a)+1
        ws=indsymbra(nmb2a)
        xs=indsymket(nmb1a)
        xsym=sym21(xs)
        wsym=sym21(ws)
        val=val*mtxso(m1,lxyz,sval,delb)
        do 105 i=1,nmax
          trans(i)=tran(i)*val
105     enddo
c
c      wx2xso=wx2xso+1
        call soxw2x(    klsym,         wsym,          xsym,
     &   cibra(nmb2),   ciket(nmb1),  sgmabra(nmb2), sgmaket(nmb1),
     &   trans,         strtbw,        strtbx,        scr1,
     &   scr2,          scr3,          1  )

c        goto 2410
        if ( klsym .ne. 1 ) then
c
          call soxw2x(    klsym,         xsym,          wsym,
     &     ciket(nmb1),   cibra(nmb2),   sgmaket(nmb1), sgmabra(nmb2),
     &     trans,         strtbx,        strtbw,        scr1,
     &     scr2,          scr3,          2  )
c
        endif
c
2410  enddo
      return
      end
      subroutine soxxww2x(  klsym,         symket,        symbra,
     &       ciket,         cibra,         sigket,        sigbra,
     &       trans,         strtab,        scr1,          scr2,
     &       scr3,          brnekt,        iwx,           switch  )
c
c  modified from xxww2x in ciudg6.f by z. zhang 31-mar-00
c
         IMPLICIT NONE

C
C     Parameter variables
C
      INTEGER         MAXSYM
      PARAMETER           (MAXSYM = 8)
C
      REAL*8            ONE
      PARAMETER           (ONE = 1D0)
C
C     Argument variables
C
      INTEGER         IWX,         KLSYM,       STRTAB(8,8), SWITCH
      INTEGER         SYMBRA,      SYMKET
C
      LOGICAL             BRNEKT
C
      REAL*8            CIBRA(*),    CIKET(*),    SCR1(*),     SCR2(*)
      REAL*8            SCR3(*),     SIGBRA(*),   SIGKET(*)
      REAL*8            TRANS(*)
C
C     Local variables
C
      INTEGER         HSTRT,       IJ,          ISYM,        JSYM
      INTEGER         NI,          NIJ,         NJ,          NMIJ
      INTEGER         NY,          STRTB,       STRTK,       YSYM
C
      REAL*8            FACTOR
C
c
c*start common block data -eas (8/28/89)
c include(data.common)
C     Common variables
C
      INTEGER         BLST1E(MAXSYM),           CNFW(MAXSYM)
      INTEGER         CNFX(MAXSYM),             LMDA(MAXSYM,MAXSYM)
      INTEGER         LMDB(MAXSYM,MAXSYM),      MNBAS
      INTEGER         NBAS(MAXSYM),             NMBPR(MAXSYM)
      INTEGER         NMSYM,       SCRLEN
      INTEGER         STRTBW(MAXSYM,MAXSYM)
      INTEGER         STRTBX(MAXSYM,MAXSYM),    SYM12(MAXSYM)
      INTEGER         SYM21(MAXSYM)
C
      COMMON /DATA   /  SYM12,       SYM21,       NBAS
      COMMON /DATA   /  LMDA,        LMDB,        NMBPR,       STRTBW
      COMMON /DATA   /  STRTBX,      BLST1E,      NMSYM,       MNBAS
      COMMON /DATA   /  CNFX,        CNFW,        SCRLEN
C
c   SYM12 (i)     contains the numbers 1 through 8
c   SYM21 (i)     contains the numbers 1 through 8  (nonsens in smprep!)
c   NBAS  (*)     number of external basis functions per irrep
c-----------------------------------------------------------------------
c   nmbpr (i)     number of symmetry pairs to generate the external exte
c   lmda  (i,j)   symmetry pair j : lmd(i)=lmdb(i,j)*lmda(i,j)
c   lmdb  (i,j)   with  lmdb(i,j).ge.lmda(i,j) , lmdb(i,j).lt.lmdb(i,j+1
c   REQUIRED FOR x,w externals
c-----------------------------------------------------------------------
c   mnbas         maximum number of external basis functions per irrep
c   cnfx  (*)   number of configurations per valid internal x walk of ir
c   cnfw  (*)   number of configurations per valid internal w walk of ir
c   nmsym       number of irreps
c     strtbw (i,wxsym) and strtbx(i,wxsym) give the starting points
c     in the external space for paths going through w and x,respectively
c     i is one of the possible values of lmdb(j,wxsym),j=1,nmbpr(wxsym)
c-----------------------------------------------------------------------
c
c   strtbw(i,sym)   contains the offset of the starting points in the ex
c                   space for a given internal path of w symmetry
c                   i= contains possible values of one irrep of the pair
c   strtbx(i,sym)   contains the offset of the starting points in the ex
c                   space for a given internal path of x symmetry
c                   i= contains possible values of one irrep of the pair
c-----------------------------------------------------------------------
c   blste1e(*)     unused ????
c   scrlen         unused ????
c
      INTEGER DG0X,DG2X,DG4X,ZZ0X,YY0X,XX0X,WW0X
      INTEGER YZ1X,YX1X,YW1X,YY2EX,WW2X,XX2X,XZ2X,WZ2X,WX2X
      INTEGER YX3X,YW3X,Z4X,Y4X,X4X,W4X
      INTEGER ZZ0XSO,YY0XSO,XX0XSO,WW0XSO
      INTEGER YZ1XSO,YX1XSO,YW1XSO,YY2XSO,XX2XSO,WX2XSO
      INTEGER DYZ, DYX, DYW, D0Z,D0Y,D0W,D0X
      INTEGER DDZI, DDYI, DDXI, DDWI, DDZE, DDYE, DDXE, DDWE
      integer xx2xso2
C
      COMMON /COUNTER/DG0X,DG2X,DG4X,ZZ0X,YY0X,XX0X,WW0X
      COMMON /COUNTER/YZ1X,YX1X,YW1X,YY2EX,WW2X,XX2X,XZ2X,WZ2X,WX2X
      COMMON /COUNTER/YX3X,YW3X,Z4X,Y4X,X4X,W4X
      COMMON /COUNTER/ZZ0XSO,YY0XSO,XX0XSO,WW0XSO
      COMMON /COUNTER/YZ1XSO,YX1XSO,YW1XSO,YY2XSO,XX2XSO,WX2XSO
      COMMON /COUNTER/DYZ, DYX, DYW, D0Z,D0Y,D0W,D0X
      COMMON /COUNTER/DDZI, DDYI, DDXI, DDWI, DDZE, DDYE, DDXE, DDWE
      common /counter/ xx2xso2


C     Common variables
C
      INTEGER         MULT(MAXSYM,MAXSYM),      NSYM,        SSYM
C
      COMMON /CSYM   /  MULT,        NSYM,        SSYM
C
c mult   standard multiplication table
c nsym   number of irreps
c ssym   state symmetry
c*end common block data -eas (8/28/89)
c
*@ifdef debug
*      common/ctestx/itest(40)
*@endif
c
c     # dummy:
c
c     # local:
c
c
c     treatment of external space for w-w and x-x cases
c     symket*symbra=lsym*ksym
c     iwx=1 x-x loops
c     iwx=2 w-w loops
c     switch=0 1a,b type loops for ksym.ne.lsym
c           =1 2a,b type loops for ksym.ne.lsym
c
      hstrt=1
      nmij=nmbpr(klsym)
      do 100 ij=1,nmij
        jsym=lmdb(klsym,ij)
        isym=lmda(klsym,ij)
        nj=nbas(jsym)
        ni=nbas(isym)
        nij=ni*nj
        if (switch.eq.0) then
          xx2xso=xx2xso+1
        else
          xx2xso2=xx2xso2+1
        endif
110     continue
100   enddo
      return
30000 format('soxxww2x',20i4)
      end
      subroutine soxw2x(    klsym,         symket,        symbra,
     &       ciket,         cibra,         sigket,        sigbra,
     &       trans,         strtbk,        strtbb,        scr1,
     &       scr2,          scr3,          switch  )
c
c  modified from xw2x in ciudg6.f by z. zhang 31-mar-00
c
         IMPLICIT NONE

C
C     Parameter variables
C
      INTEGER         MAXSYM
      PARAMETER           (MAXSYM = 8)
C
      REAL*8            ZERO
      PARAMETER           (ZERO = 0D0)
      REAL*8            ONE
      PARAMETER           (ONE = 1D0)
C
C     Argument variables
C
      INTEGER         KLSYM,       STRTBB(8,8), STRTBK(8,8), SWITCH
      INTEGER         SYMBRA,      SYMKET
C
      REAL*8            CIBRA(*),    CIKET(*),    SCR1(*),     SCR2(*)
      REAL*8            SCR3(*),     SIGBRA(*),   SIGKET(*)
      REAL*8            TRANS(*)
C
C     Local variables
C
      INTEGER         HSTRT,       IJ,          ISYM,        JSYM
      INTEGER         NI,          NIJ,         NJ,          NMIJ
      INTEGER         NY,          STRTB,       STRTK,       YSYM
C
      REAL*8            FACTOR
C
c
c*start common block data -eas (8/28/89)
c include(data.common)
C     Common variables
C
      INTEGER         BLST1E(MAXSYM),           CNFW(MAXSYM)
      INTEGER         CNFX(MAXSYM),             LMDA(MAXSYM,MAXSYM)
      INTEGER         LMDB(MAXSYM,MAXSYM),      MNBAS
      INTEGER         NBAS(MAXSYM),             NMBPR(MAXSYM)
      INTEGER         NMSYM,       SCRLEN
      INTEGER         STRTBW(MAXSYM,MAXSYM)
      INTEGER         STRTBX(MAXSYM,MAXSYM),    SYM12(MAXSYM)
      INTEGER         SYM21(MAXSYM)
C
      COMMON /DATA   /  SYM12,       SYM21,       NBAS
      COMMON /DATA   /  LMDA,        LMDB,        NMBPR,       STRTBW
      COMMON /DATA   /  STRTBX,      BLST1E,      NMSYM,       MNBAS
      COMMON /DATA   /  CNFX,        CNFW,        SCRLEN
C
c   SYM12 (i)     contains the numbers 1 through 8
c   SYM21 (i)     contains the numbers 1 through 8  (nonsens in smprep!)
c   NBAS  (*)     number of external basis functions per irrep
c-----------------------------------------------------------------------
c   nmbpr (i)     number of symmetry pairs to generate the external exte
c   lmda  (i,j)   symmetry pair j : lmd(i)=lmdb(i,j)*lmda(i,j)
c   lmdb  (i,j)   with  lmdb(i,j).ge.lmda(i,j) , lmdb(i,j).lt.lmdb(i,j+1
c   REQUIRED FOR x,w externals
c-----------------------------------------------------------------------
c   mnbas         maximum number of external basis functions per irrep
c   cnfx  (*)   number of configurations per valid internal x walk of ir
c   cnfw  (*)   number of configurations per valid internal w walk of ir
c   nmsym       number of irreps
c     strtbw (i,wxsym) and strtbx(i,wxsym) give the starting points
c     in the external space for paths going through w and x,respectively
c     i is one of the possible values of lmdb(j,wxsym),j=1,nmbpr(wxsym)
c-----------------------------------------------------------------------
c
c   strtbw(i,sym)   contains the offset of the starting points in the ex
c                   space for a given internal path of w symmetry
c                   i= contains possible values of one irrep of the pair
c   strtbx(i,sym)   contains the offset of the starting points in the ex
c                   space for a given internal path of x symmetry
c                   i= contains possible values of one irrep of the pair
c-----------------------------------------------------------------------
c   blste1e(*)     unused ????
c   scrlen         unused ????
c


      INTEGER DG0X,DG2X,DG4X,ZZ0X,YY0X,XX0X,WW0X
      INTEGER YZ1X,YX1X,YW1X,YY2EX,WW2X,XX2X,XZ2X,WZ2X,WX2X
      INTEGER YX3X,YW3X,Z4X,Y4X,X4X,W4X
      INTEGER ZZ0XSO,YY0XSO,XX0XSO,WW0XSO
      INTEGER YZ1XSO,YX1XSO,YW1XSO,YY2XSO,XX2XSO,WX2XSO
      INTEGER DYZ, DYX, DYW, D0Z,D0Y,D0W,D0X
      INTEGER DDZI, DDYI, DDXI, DDWI, DDZE, DDYE, DDXE, DDWE
      INTEGER XX2XSO2
C
      COMMON /COUNTER/DG0X,DG2X,DG4X,ZZ0X,YY0X,XX0X,WW0X
      COMMON /COUNTER/YZ1X,YX1X,YW1X,YY2EX,WW2X,XX2X,XZ2X,WZ2X,WX2X
      COMMON /COUNTER/YX3X,YW3X,Z4X,Y4X,X4X,W4X
      COMMON /COUNTER/ZZ0XSO,YY0XSO,XX0XSO,WW0XSO
      COMMON /COUNTER/YZ1XSO,YX1XSO,YW1XSO,YY2XSO,XX2XSO,WX2XSO
      COMMON /COUNTER/DYZ, DYX, DYW, D0Z,D0Y,D0W,D0X
      COMMON /COUNTER/DDZI, DDYI, DDXI, DDWI, DDZE, DDYE, DDXE, DDWE
      COMMON/COUNTER/XX2XSO2


C     Common variables
C
      INTEGER         MULT(MAXSYM,MAXSYM),      NSYM,        SSYM
C
      COMMON /CSYM   /  MULT,        NSYM,        SSYM
C
c mult   standard multiplication table
c nsym   number of irreps
c ssym   state symmetry
c*end common block data -eas (8/28/89)
c
c     # dummy:
c
c     # local:
c
*@ifdef debug
*      common/ctestx/itest(40)
*@endif
c
c     treatment of external space for x-w cases
c     symket*symbra=lsym*ksym
c     switch=1 ket=w
c              bra=x
c     switch=2 ket=x
c              bra=w
c
      hstrt=1
      nmij=nmbpr(klsym)
      do 100 ij=1,nmij
        jsym=lmdb(klsym,ij)
        isym=lmda(klsym,ij)
        nj=nbas(jsym)
        ni=nbas(isym)
c        write(6,30000)ij,jsym,isym,nj,ni,hstrt,symbra,symket,switch
       wx2xso=wx2xso+1
100   enddo
      return
30000 format('soxw2x',20i4)
      end
      subroutine so2int_ft(initial,indxbra,indxket,segsm,
     & tsk)
c
c  construct the 2-internal so formula tape.
c  modified from allin_ft in ciudg5.f by z. zhang 31-mar-00
c
c  code  loop type      description
c  ----  ---------      ------------
c
c   1  -- new record --
c   2  -- new vertex --
c   3  -- new level --
c
c   4       8b          (z,y,x,w)
c
         IMPLICIT NONE

C
C     Parameter variables
C
      INTEGER         MAXBF
      PARAMETER           (MAXBF = 35000)
      INTEGER         NFILMX
      PARAMETER           (NFILMX = 55)
      INTEGER         LBFT
      PARAMETER           (LBFT = 2048)
C
C     Argument variables
C
      INTEGER    INDXBRA(*),INDXKET(*),SEGSM(2),TSK
C
      LOGICAL             INITIAL
C
C     Local variables
C
      LOGICAL             BREAK2
      Integer  ii
C
C     Common variables
C
      INTEGER         BTAIL,       CLEV,        ILEV,        IMO
      INTEGER         JKLSYM,      JLEV,        JMO
      INTEGER         JUMPINPOSITION,           KLEV,        KLSYM
      INTEGER         KMO,         KTAIL,       LLEV,        LMO
      INTEGER         LSYM,        VER
C
      LOGICAL             BREAK
C
      COMMON /LCONTROL/ JUMPINPOSITION,           IMO,         JMO
      COMMON /LCONTROL/ KMO,         LMO,         VER,         CLEV
      COMMON /LCONTROL/ BTAIL,       KTAIL,       LLEV,        KLEV
      COMMON /LCONTROL/ JLEV,        ILEV,        LSYM,        KLSYM
      COMMON /LCONTROL/ JKLSYM,      BREAK
C
c
c  needed to save current settings controlling loop calculation
c  if buffer is full
c
C     Common variables
C
      INTEGER         HEADV(MAXBF),             IBF
      INTEGER         ICODEV(MAXBF),            XBTV(MAXBF)
      INTEGER         XKTV(MAXBF), YBTV(MAXBF), YKTV(MAXBF)
      INTEGER         HLTV(MAXBF)
C
      REAL*8            VALV(3,MAXBF)
C
      COMMON /SPECIAL/  VALV,        HEADV,       ICODEV,      XKTV
      COMMON /SPECIAL/  XBTV,        YBTV,        YKTV
      COMMON /SPECIAL/  HLTV, IBF
C
c
c  internal stuff for loop calculation
c
c
C     Common variables
C
      INTEGER         CODE(4),     HILEV,       MO(0:4),     NINTL
      INTEGER         NIOT,        NMO,         NUMV
C
      LOGICAL             QDIAG
C
      COMMON /CLI    /  QDIAG,       NIOT,        NUMV,        HILEV
      COMMON /CLI    /  CODE,        MO,          NMO,         NINTL
C
c
c internally needed for formula construction
c
c  qdiag  true: allow both diagonal and off-diagonal loops
c         false: off-diagonal loops only
c
c  niot   total number of internal orbitals
c  numv    ???
c  hilev   ???
c  code(*) interaction type
c  mo(*)   contains the current external indices i,j,k,l
c  nmo     number of mo indices to be stored in mo(*)
c  nintl   ???

C     Common variables
C
      INTEGER         ST1(8),      ST10(6),     ST11(4)
      INTEGER         ST11D(4),    ST12(4),     ST13(4),     ST14(2)
      INTEGER         ST2(8),      ST3(8),      ST4(6),      ST4P(6)
      INTEGER         ST5(6),      ST6(6),      ST7(6),      ST8(6)
C
      COMMON /CST    /  ST1,         ST2,         ST3,         ST4
      COMMON /CST    /  ST4P,        ST5,         ST6,         ST7
      COMMON /CST    /  ST8,         ST10,        ST11D,       ST11
      COMMON /CST    /  ST12,        ST13,        ST14
C
c  tables for formula tape construction
C     Common variables
C
      INTEGER         DB1(8,0:2,2),             DB10(6,0:2)
      INTEGER         DB11(4,0:2,2),            DB12(4,0:2,3)
      INTEGER         DB13(4,0:2), DB14(2,0:2,2)
      INTEGER         DB2(8,0:2,2),             DB3(8,0:2,2)
      INTEGER         DB4(6,0:2,2),             DB4P(6,0:2)
      INTEGER         DB5(6,0:2),  DB6(6,0:2,2)
      INTEGER         DB7(6,0:2),  DB8(6,0:2,2)
C
      COMMON /CDB    /  DB1,         DB2,         DB3,         DB4
      COMMON /CDB    /  DB4P,        DB5,         DB6,         DB7
      COMMON /CDB    /  DB8,         DB10,        DB11,        DB12
      COMMON /CDB    /  DB13,        DB14
C
c  internal stuff for loop construction
C     Common variables
C
      INTEGER         NUNITS(NFILMX)
C
      COMMON /CFILES /  NUNITS
c
c nunits(1)     tape6 ,iwrite
c        2      nin,tape5,shout
c        3      nslist
c        4      fildia,ndiagf
c        5      pseudg,filsgm
c        6      filew,hvfile
c        7      filev,civfl
c        8      filind
c        9      filint
c       10      fildrt
c       11      fillpd
c       12      fillp
c       13      filfti,fillpi
c       14      flindx
c       15      ciinv,inufv
c       16      civout,outufv
c       17      fockdg
c       18      d1fl
c       20      fvfile
c       21      filsc4,filcii,scrfil
c       22      filtot
c       23      fiacpf
c       26      filsc5
c       27      mcrest
c       28      ciuvfl
c       29      cirefv
c       30      filden
c       31      ifil4w
c       32      ifil4x
c       33      ifil3w
c       34      ifil3x
c       35      filtpq
c       36      aofile
c       37      aofile2
c       38      drtfil
c       39      filsti
c       42      mocoef,vectrf
c       45      prtap
c

c
c

      integer nlist
      equivalence (nlist,nunits(1))
C     Common variables
C
      INTEGER         VTPT
C
      REAL*8            OLDVAL,      VCOEFF(4),   VTRAN(2,2,5)
C
      COMMON /CVTRAN /  VTRAN,       VCOEFF,      OLDVAL,      VTPT
C
c  for loop construction
C     Common variables
C
      INTEGER        NLOOP
C
      REAL*8            FTBUF(LBFT)
C
      COMMON /CFTB   /  NLOOP
C
c  nloop   number of loops generated
C     Common variables
C
      INTEGER         QPRT
C
      COMMON /CPRT   /  QPRT
C
c
c   printing level for formula tape construction
c   -> eliminate
c
c
c
c      integer kmom1
      integer multmx
      parameter(multmx=19)
      integer nmulmx
      parameter(nmulmx=9)

c
C
      INTEGER         LXYZIR(3), SPNIR, MULTP,NMUL, HMULT,NEXW
C
      LOGICAL             SKIPSO,      SPNODD,      SPNORB
C
      COMMON /SOLXYZ /  SKIPSO,      SPNORB,      SPNODD,      LXYZIR
      COMMON /SOLXYZ /  SPNIR(multmx,multmx),MULTP(nmulmx), NMUL
      COMMON /SOLXYZ /  HMULT,NEXW(8,4)
C

c
c  SKIPSO   Spinorbit-CI calculation
c  SPNODD   odd spin
c  SPNORB   spinorbit CI
c  LXYZIR   irreducible representation of the three L components
c  SPNIR
c  MULTP
c  NMUL
c  HMULT    highest multiplicity
c  NEXW     number of external walks per symmetry
c
c
C
      INTEGER MAXTASK
      parameter (maxtask=16384)
      INTEGER BSEG(MAXTASK), KSEG(MAXTASK),
     .        TASK(MAXTASK),NTASK,
     .        SLICE(MAXTASK),LOOPCNT(MAXTASK),
     .        CURRTSK
      REAL*8 TASKTIMES(MAXTASK),MFLOP(MAXTASK)
      COMMON/TASKLST/MFLOP,
     .      TASKTIMES,BSEG,KSEG,TASK,SLICE,
     .      LOOPCNT,NTASK,CURRTSK


c
c tasklist
c BSEG      bra segment
c KSEG      ket segment
c TASK      task type
c NTASK     total number of task
c SLICE     segmentation type
c loopcnt   loopcnt for debugging
c currtsk   current task
c tasktimes time spent in task
c

c
c
      ibf = 0  ! initialize loop buffer pointer
      if (initial) jumpinposition=0 !
      goto (999, 1) (jumpinposition+1)
c
 999  continue ! first call to so2int_ft
c
      nintl = 7
      mo(0) = 0
      nloop = 0
      qdiag = .false.

      if(spnodd) niot=niot+1
      lmo = niot + 1
      kmo = lmo
c      kmom1 = kmo - 1
c      if(spnodd)kmom1 = kmom1 - 1
      jmo = 2
 600  continue
      if(spnodd)then
        if (jmo .gt. (kmo-2))goto 601
      else
        if (jmo .gt. (kmo-1))goto 601
      endif
      imo = 1
 500  continue
      if (imo .gt. (jmo-1))goto 501
c      do 600 jmo = 2, kmom1
c      do 500 imo = 1, (jmo-1)
c***********************************************************
c     i<j<k=l loops:
c***********************************************************
      if(qprt.ge.5)write(nlist,6100)lmo,kmo,jmo,imo
      nmo = 3
      mo(1) = imo
      mo(2) = jmo
      mo(3) = lmo
c
      ver = tsk
c
c     # 8b loops:
c
      numv = 1
      code(1) = 4
      vtpt = 1
 1    call loopcalc( ver, ver, st8, db8(1,0,2), 6, 0,
     &     indxbra,indxket,segsm)
      if(break) then
        jumpinposition = 1
        return              ! loop buffer is full
      endif
c
 3    call wcode_dir(ibf,3,icodev,3,break2)
      if(break2) return          ! loop buffer is full
 502  imo = imo + 1
      goto 500
 501  continue
      jmo = jmo + 1
      goto 600
 601  continue
c
      icodev(ibf+1)=0
      jumpinposition=0
c
c     write(nlist,6010)nloop
c6010  format('2-internal so ft construction complete.',
c    + i8,' loops constructed.')
c     do ii=1,ibf+1
c         write(6,*) 'so2int:',ii,icodev(ii),valv(ii,1)
c     enddo
c
      loopcnt(currtsk)=nloop
      if(spnodd) niot=niot-1
      return
6100  format(' so2:  lmo,kmo,jmo,imo = ',4i4)
      end
      subroutine so1int_ft(initial,indxbra,indxket,segsm,tsk)
c
c  construct the 1-internal so formula tape.
c  modified from onex in ciudg5.f by z. zhang 31-mar-00
c
c  code  loop type     description
c  ----  ---------     ----------------
c
c   0  -- new level --
c   1  -- new record --
c   2  -- new vertex --
c
c   3       8b         (yz,xy,wy)
c
         IMPLICIT NONE

C
C     Parameter variables
C
      INTEGER         MAXBF
      PARAMETER           (MAXBF = 35000)
      INTEGER         NFILMX
      PARAMETER           (NFILMX = 55)
      INTEGER         LBFT
      PARAMETER           (LBFT = 2048)
C
C     Argument variables
C
      INTEGER    INDXBRA(*),INDXKET(*),SEGSM(2),TSK
C
      LOGICAL             INITIAL
C
C     Local variables
C
      INTEGER         BTAILX(3),   KTAILX(3),ii
C
      LOGICAL             BREAK2
C
c
C     Common variables
C
      INTEGER         BTAIL,       CLEV,        ILEV,        IMO
      INTEGER         JKLSYM,      JLEV,        JMO
      INTEGER         JUMPINPOSITION,           KLEV,        KLSYM
      INTEGER         KMO,         KTAIL,       LLEV,        LMO
      INTEGER         LSYM,        VER
C
      LOGICAL             BREAK
C
      COMMON /LCONTROL/ JUMPINPOSITION,           IMO,         JMO
      COMMON /LCONTROL/ KMO,         LMO,         VER,         CLEV
      COMMON /LCONTROL/ BTAIL,       KTAIL,       LLEV,        KLEV
      COMMON /LCONTROL/ JLEV,        ILEV,        LSYM,        KLSYM
      COMMON /LCONTROL/ JKLSYM,      BREAK
C
c
c  needed to save current settings controlling loop calculation
c  if buffer is full
c
c
c
C     Common variables
C
      INTEGER         HEADV(MAXBF),             IBF
      INTEGER         ICODEV(MAXBF),            XBTV(MAXBF)
      INTEGER         XKTV(MAXBF), YBTV(MAXBF), YKTV(MAXBF)
      INTEGER         HLTV(MAXBF)
C
      REAL*8            VALV(3,MAXBF)
C
      COMMON /SPECIAL/  VALV,        HEADV,       ICODEV,      XKTV
      COMMON /SPECIAL/  XBTV,        YBTV,        YKTV
      COMMON /SPECIAL/  HLTV, IBF
C
c
c  internal stuff for loop calculation
c
c
c
C     Common variables
C
      INTEGER         CODE(4),     HILEV,       MO(0:4),     NINTL
      INTEGER         NIOT,        NMO,         NUMV
C
      LOGICAL             QDIAG
C
      COMMON /CLI    /  QDIAG,       NIOT,        NUMV,        HILEV
      COMMON /CLI    /  CODE,        MO,          NMO,         NINTL
C
c
c internally needed for formula construction
c
c  qdiag  true: allow both diagonal and off-diagonal loops
c         false: off-diagonal loops only
c
c  niot   total number of internal orbitals
c  numv    ???
c  hilev   ???
c  code(*) interaction type
c  mo(*)   contains the current external indices i,j,k,l
c  nmo     number of mo indices to be stored in mo(*)
c  nintl   ???

c
C     Common variables
C
      INTEGER         ST1(8),      ST10(6),     ST11(4)
      INTEGER         ST11D(4),    ST12(4),     ST13(4),     ST14(2)
      INTEGER         ST2(8),      ST3(8),      ST4(6),      ST4P(6)
      INTEGER         ST5(6),      ST6(6),      ST7(6),      ST8(6)
C
      COMMON /CST    /  ST1,         ST2,         ST3,         ST4
      COMMON /CST    /  ST4P,        ST5,         ST6,         ST7
      COMMON /CST    /  ST8,         ST10,        ST11D,       ST11
      COMMON /CST    /  ST12,        ST13,        ST14
C
c  tables for formula tape construction

C     Common variables
C
      INTEGER         DB1(8,0:2,2),             DB10(6,0:2)
      INTEGER         DB11(4,0:2,2),            DB12(4,0:2,3)
      INTEGER         DB13(4,0:2), DB14(2,0:2,2)
      INTEGER         DB2(8,0:2,2),             DB3(8,0:2,2)
      INTEGER         DB4(6,0:2,2),             DB4P(6,0:2)
      INTEGER         DB5(6,0:2),  DB6(6,0:2,2)
      INTEGER         DB7(6,0:2),  DB8(6,0:2,2)
C
      COMMON /CDB    /  DB1,         DB2,         DB3,         DB4
      COMMON /CDB    /  DB4P,        DB5,         DB6,         DB7
      COMMON /CDB    /  DB8,         DB10,        DB11,        DB12
      COMMON /CDB    /  DB13,        DB14
C
c  internal stuff for loop construction
c
C     Common variables
C
      INTEGER         NUNITS(NFILMX)
C
      COMMON /CFILES /  NUNITS
c
c nunits(1)     tape6 ,iwrite
c        2      nin,tape5,shout
c        3      nslist
c        4      fildia,ndiagf
c        5      pseudg,filsgm
c        6      filew,hvfile
c        7      filev,civfl
c        8      filind
c        9      filint
c       10      fildrt
c       11      fillpd
c       12      fillp
c       13      filfti,fillpi
c       14      flindx
c       15      ciinv,inufv
c       16      civout,outufv
c       17      fockdg
c       18      d1fl
c       20      fvfile
c       21      filsc4,filcii,scrfil
c       22      filtot
c       23      fiacpf
c       26      filsc5
c       27      mcrest
c       28      ciuvfl
c       29      cirefv
c       30      filden
c       31      ifil4w
c       32      ifil4x
c       33      ifil3w
c       34      ifil3x
c       35      filtpq
c       36      aofile
c       37      aofile2
c       38      drtfil
c       39      filsti
c       42      mocoef,vectrf
c       45      prtap
c

c
c

      integer nlist
      equivalence (nlist,nunits(1))
c
C     Common variables
C
      INTEGER         VTPT
C
      REAL*8            OLDVAL,      VCOEFF(4),   VTRAN(2,2,5)
C
      COMMON /CVTRAN /  VTRAN,       VCOEFF,      OLDVAL,      VTPT
C
c  for loop construction
C     Common variables
C
      INTEGER        NLOOP
C
      REAL*8            FTBUF(LBFT)
C
      COMMON /CFTB   /  NLOOP
C
c  nloop   number of loops generated
      integer multmx
      parameter(multmx=19)
      integer nmulmx
      parameter(nmulmx=9)

c
C
      INTEGER         LXYZIR(3), SPNIR, MULTP,NMUL, HMULT,NEXW
C
      LOGICAL             SKIPSO,      SPNODD,      SPNORB
C
      COMMON /SOLXYZ /  SKIPSO,      SPNORB,      SPNODD,      LXYZIR
      COMMON /SOLXYZ /  SPNIR(multmx,multmx),MULTP(nmulmx), NMUL
      COMMON /SOLXYZ /  HMULT,NEXW(8,4)
C

c
c  SKIPSO   Spinorbit-CI calculation
c  SPNODD   odd spin
c  SPNORB   spinorbit CI
c  LXYZIR   irreducible representation of the three L components
c  SPNIR
c  MULTP
c  NMUL
c  HMULT    highest multiplicity
c  NEXW     number of external walks per symmetry
c
c
C
      INTEGER MAXTASK
      parameter (maxtask=16384)
      INTEGER BSEG(MAXTASK), KSEG(MAXTASK),
     .        TASK(MAXTASK),NTASK,
     .        SLICE(MAXTASK),LOOPCNT(MAXTASK),
     .        CURRTSK
      REAL*8 TASKTIMES(MAXTASK),MFLOP(MAXTASK)
      COMMON/TASKLST/MFLOP,
     .      TASKTIMES,BSEG,KSEG,TASK,SLICE,
     .      LOOPCNT,NTASK,CURRTSK


c
c tasklist
c BSEG      bra segment
c KSEG      ket segment
c TASK      task type
c NTASK     total number of task
c SLICE     segmentation type
c loopcnt   loopcnt for debugging
c currtsk   current task
c tasktimes time spent in task
c

c
c
c      integer lmom1
c
      data    btailx/2,3,4/, ktailx/1,2,2/
c
      ibf = 0  ! initialize loop buffer pointer
      if (initial) jumpinposition=0 !
      goto (999, 1) (jumpinposition+1)

 999  continue ! first call to so1int_ft
c
      nintl = 7
      mo(0) = 0
      nloop = 0
      qdiag = .false.
c
      if(spnodd) niot=niot+1
      lmo = niot + 1
      jmo = 1
 300  continue
      if(.not.spnodd)then
        if(jmo .gt. (lmo-1)) goto 301
      else if(spnodd)then
        if(jmo .gt. (lmo-2)) goto 301
      endif
c      do 300 jmo = 1, lmom1
c**********************************************************************
c     j<k=l
c**********************************************************************
      nmo = 2
      mo(1) = jmo
      mo(2) = lmo
c
      ver = tsk
      btail = btailx(ver)
      ktail = ktailx(ver)
c
c     # 8b loops:
c
      numv = 1
      vtpt = 1
      code(1) = 3
 1    call loopcalc( btail, ktail, st8, db8(1,0,2), 6, 2,
     &     indxbra,indxket,segsm )
c
      if(break) then
        jumpinposition = 1
        return                 ! loop buffer is full
      endif

 291  continue
 3    call wcode_dir(ibf,0,icodev,3,break2)
      if(break2) return          ! loop buffer is full
      jmo = jmo + 1
      goto 300
 301  continue
      icodev(ibf+1)=0
      jumpinposition=0
c
c     write(nlist,6010) nloop
c6010  format('1-internal so ft construction complete.',
c    + i8,' loops constructed.')
c      do ii=1,ibf+1
c       write(6,*) 'so1int :',ii,icodev(ii),valv(ii,1)
c      enddo
c
      loopcnt(currtsk)=nloop
      if(spnodd) niot=niot-1
      return
      end
      subroutine so0int_ft(initial,indxbra,indxket,segsm,
     & tsk)
c
c  construct the 0-internal so formula tape.
c  modified from twoex_ft in ciudg6.f by z. zhang 31-mar-00
c
c  code  loop type     description
c  ----  ---------     --------------
c
c   0  -- new level --
c   1  -- new record --
c
c   2       8b          (yy)
c   3       8b          (xx)
c   4       8b          (wx)
c
         IMPLICIT NONE

C
C     Parameter variables
C
      INTEGER         MAXBF
      PARAMETER           (MAXBF = 35000)
      INTEGER         NFILMX
      PARAMETER           (NFILMX = 55)
      INTEGER         LBFT
      PARAMETER           (LBFT = 2048)
C
C     Argument variables
C
      INTEGER INDXBRA(*),INDXKET(*),SEGSM(2),TSK
C
      LOGICAL             INITIAL
C
C     Local variables
C
      LOGICAL             BREAK2
      integer     ii
C
c
C     Common variables
C
      INTEGER         BTAIL,       CLEV,        ILEV,        IMO
      INTEGER         JKLSYM,      JLEV,        JMO
      INTEGER         JUMPINPOSITION,           KLEV,        KLSYM
      INTEGER         KMO,         KTAIL,       LLEV,        LMO
      INTEGER         LSYM,        VER
C
      LOGICAL             BREAK
C
      COMMON /LCONTROL/ JUMPINPOSITION,           IMO,         JMO
      COMMON /LCONTROL/ KMO,         LMO,         VER,         CLEV
      COMMON /LCONTROL/ BTAIL,       KTAIL,       LLEV,        KLEV
      COMMON /LCONTROL/ JLEV,        ILEV,        LSYM,        KLSYM
      COMMON /LCONTROL/ JKLSYM,      BREAK
C
c
c  needed to save current settings controlling loop calculation
c  if buffer is full
c
c
c
C     Common variables
C
      INTEGER         HEADV(MAXBF),             IBF
      INTEGER         ICODEV(MAXBF),            XBTV(MAXBF)
      INTEGER         XKTV(MAXBF), YBTV(MAXBF), YKTV(MAXBF)
      INTEGER         HLTV(MAXBF)
C
      REAL*8            VALV(3,MAXBF)
C
      COMMON /SPECIAL/  VALV,        HEADV,       ICODEV,      XKTV
      COMMON /SPECIAL/  XBTV,        YBTV,        YKTV
      COMMON /SPECIAL/  HLTV, IBF
C
c
c  internal stuff for loop calculation
c
c
c
C     Common variables
C
      INTEGER         CODE(4),     HILEV,       MO(0:4),     NINTL
      INTEGER         NIOT,        NMO,         NUMV
C
      LOGICAL             QDIAG
C
      COMMON /CLI    /  QDIAG,       NIOT,        NUMV,        HILEV
      COMMON /CLI    /  CODE,        MO,          NMO,         NINTL
C
c
c internally needed for formula construction
c
c  qdiag  true: allow both diagonal and off-diagonal loops
c         false: off-diagonal loops only
c
c  niot   total number of internal orbitals
c  numv    ???
c  hilev   ???
c  code(*) interaction type
c  mo(*)   contains the current external indices i,j,k,l
c  nmo     number of mo indices to be stored in mo(*)
c  nintl   ???

c
C     Common variables
C
      INTEGER         ST1(8),      ST10(6),     ST11(4)
      INTEGER         ST11D(4),    ST12(4),     ST13(4),     ST14(2)
      INTEGER         ST2(8),      ST3(8),      ST4(6),      ST4P(6)
      INTEGER         ST5(6),      ST6(6),      ST7(6),      ST8(6)
C
      COMMON /CST    /  ST1,         ST2,         ST3,         ST4
      COMMON /CST    /  ST4P,        ST5,         ST6,         ST7
      COMMON /CST    /  ST8,         ST10,        ST11D,       ST11
      COMMON /CST    /  ST12,        ST13,        ST14
C
c  tables for formula tape construction

C     Common variables
C
      INTEGER         DB1(8,0:2,2),             DB10(6,0:2)
      INTEGER         DB11(4,0:2,2),            DB12(4,0:2,3)
      INTEGER         DB13(4,0:2), DB14(2,0:2,2)
      INTEGER         DB2(8,0:2,2),             DB3(8,0:2,2)
      INTEGER         DB4(6,0:2,2),             DB4P(6,0:2)
      INTEGER         DB5(6,0:2),  DB6(6,0:2,2)
      INTEGER         DB7(6,0:2),  DB8(6,0:2,2)
C
      COMMON /CDB    /  DB1,         DB2,         DB3,         DB4
      COMMON /CDB    /  DB4P,        DB5,         DB6,         DB7
      COMMON /CDB    /  DB8,         DB10,        DB11,        DB12
      COMMON /CDB    /  DB13,        DB14
C
c  internal stuff for loop construction
c
C     Common variables
C
      INTEGER         NUNITS(NFILMX)
C
      COMMON /CFILES /  NUNITS
c
c nunits(1)     tape6 ,iwrite
c        2      nin,tape5,shout
c        3      nslist
c        4      fildia,ndiagf
c        5      pseudg,filsgm
c        6      filew,hvfile
c        7      filev,civfl
c        8      filind
c        9      filint
c       10      fildrt
c       11      fillpd
c       12      fillp
c       13      filfti,fillpi
c       14      flindx
c       15      ciinv,inufv
c       16      civout,outufv
c       17      fockdg
c       18      d1fl
c       20      fvfile
c       21      filsc4,filcii,scrfil
c       22      filtot
c       23      fiacpf
c       26      filsc5
c       27      mcrest
c       28      ciuvfl
c       29      cirefv
c       30      filden
c       31      ifil4w
c       32      ifil4x
c       33      ifil3w
c       34      ifil3x
c       35      filtpq
c       36      aofile
c       37      aofile2
c       38      drtfil
c       39      filsti
c       42      mocoef,vectrf
c       45      prtap
c

c
c

      integer nlist
      equivalence (nlist,nunits(1))
c
C     Common variables
C
      INTEGER         VTPT
C
      REAL*8            OLDVAL,      VCOEFF(4),   VTRAN(2,2,5)
C
      COMMON /CVTRAN /  VTRAN,       VCOEFF,      OLDVAL,      VTPT
C
c  for loop construction
c
C     Common variables
C
      INTEGER        NLOOP
C
      REAL*8            FTBUF(LBFT)
C
      COMMON /CFTB   /  NLOOP
C
c  nloop   number of loops generated
c
c
c
c
      integer multmx
      parameter(multmx=19)
      integer nmulmx
      parameter(nmulmx=9)

c
C
      INTEGER         LXYZIR(3), SPNIR, MULTP,NMUL, HMULT,NEXW
C
      LOGICAL             SKIPSO,      SPNODD,      SPNORB
C
      COMMON /SOLXYZ /  SKIPSO,      SPNORB,      SPNODD,      LXYZIR
      COMMON /SOLXYZ /  SPNIR(multmx,multmx),MULTP(nmulmx), NMUL
      COMMON /SOLXYZ /  HMULT,NEXW(8,4)
C

c
c  SKIPSO   Spinorbit-CI calculation
c  SPNODD   odd spin
c  SPNORB   spinorbit CI
c  LXYZIR   irreducible representation of the three L components
c  SPNIR
c  MULTP
c  NMUL
c  HMULT    highest multiplicity
c  NEXW     number of external walks per symmetry
c
c
C
      INTEGER MAXTASK
      parameter (maxtask=16384)
      INTEGER BSEG(MAXTASK), KSEG(MAXTASK),
     .        TASK(MAXTASK),NTASK,
     .        SLICE(MAXTASK),LOOPCNT(MAXTASK),
     .        CURRTSK
      REAL*8 TASKTIMES(MAXTASK),MFLOP(MAXTASK)
      COMMON/TASKLST/MFLOP,
     .      TASKTIMES,BSEG,KSEG,TASK,SLICE,
     .      LOOPCNT,NTASK,CURRTSK


c
c tasklist
c BSEG      bra segment
c KSEG      ket segment
c TASK      task type
c NTASK     total number of task
c SLICE     segmentation type
c loopcnt   loopcnt for debugging
c currtsk   current task
c tasktimes time spent in task
c

c
c
ctm   integer lmo, kmo
c
      ibf = 0
      if (initial) jumpinposition=0 !
      goto (999, 1, 2, 3, 4) (jumpinposition+1)

 999  continue ! first call to so1int_ft
      nintl = 7
      mo(0) = 0
      nloop = 0
      qdiag = .true.
c
      if(spnodd) niot=niot+1
      lmo = niot + 1
      nmo = 1
      mo(1) = lmo
c
c     # yy loop 8b:
c
      if (tsk.ne.1) goto 802
      numv = 1
      vtpt = 3
      code(1) = 2
 1    call loopcalc( 2, 2, st8, db8(1,0,2), 6, 4,
     &     indxbra,indxket,segsm)
c
      if(break)then
        jumpinposition = 1
        return                 ! loop buffer is full
      endif
c
c     # xx loop 8b:
c
802   if (tsk.ne.2) goto 803
      numv = 1
      vtpt = 1
      code(1) = 3
 2    call loopcalc( 3, 3, st8, db8(1,0,2), 6, 4,
     &     indxbra,indxket,segsm)
c
      if(break)then
        jumpinposition = 2
        return                 ! loop buffer is full
      endif
c
c     # wx loop 8b:
c
803   if (tsk.ne.3) goto 4
      numv = 1
      vtpt = 4
      code(1) = 4

 3    call loopcalc( 4, 3, st8, db8(1,0,2), 6, 4,
     &     indxbra,indxket,segsm)
c
      if(break) then
        jumpinposition = 3
        return                 ! loop buffer is full
      endif
c
 4    call wcode_dir(ibf,0,icodev,4,break2)
      if(break2) return          ! loop buffer is full
c
c     write(nlist,6010) nloop
c6010  format('0-internal so ft construction complete.',
c    + i8,' loops constructed.',i6,' ft buffers used.')
c      do ii=1,ibf+1
c       write(6,*) 'so0int :',ii,icodev(ii),valv(ii,1)
c      enddo
c
      loopcnt(currtsk)=nloop
      if(spnodd) niot=niot-1
      return
      end
