!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
C
C     Common variables
C
      INTEGER*8           CNT1X_YW(150,150),        CNT1X_YX(150,150)
      INTEGER*8           CNT1X_YZ(150,150)
C
      COMMON / ONEX_STAT/ CNT1X_YZ,    CNT1X_YX,    CNT1X_YW
C
