!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
C
C     Common variables
C
      INTEGER             DG_HDLE,     DRT_HDLE,    EX3W_HDLE
      INTEGER             EX3X_HDLE,   EX4W_HDLE,   EX4X_HDLE,   HD_HDLE
      INTEGER             ME,          NXTTASK,     OFDG_HDLE,   V_HDLE
      INTEGER             W_HDLE
C
      COMMON / GAPOINTER/ W_HDLE,      V_HDLE,      HD_HDLE
      COMMON / GAPOINTER/ EX3W_HDLE,   EX4W_HDLE,   EX3X_HDLE
      COMMON / GAPOINTER/ EX4X_HDLE,   ME,          NXTTASK
      COMMON / GAPOINTER/ DRT_HDLE,    DG_HDLE,     OFDG_HDLE
C
