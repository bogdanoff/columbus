!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
C
C     Common variables
C
      INTEGER             BMS(MAXNUM,3,2),          KMS(MAXNUM,3,2)
      INTEGER             NUMNZ(3,MAXS,2)
C
      REAL*8              MTXSO(MAXNUM,3,MAXS,2)
C
      COMMON / CMTXSO /   BMS,         KMS,         NUMNZ,       MTXSO
C
