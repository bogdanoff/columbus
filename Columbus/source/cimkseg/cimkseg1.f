!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      program cimkseg
      IMPLICIT NONE
      include "../colib/getlcl.h" 
        INTEGER taskID

c ##########################################################
c porting between parallel and sequential version
c take a different and hopefully more transparent
c approach:
c In the sequential version replace all ga-calls
c partially by dummy routines and partially by
c subroutines carrying out the right operation
c This way we hopefully achieve a more transparent
c and reliable porting procedure. Additionally
c this provides an easy way to switch between
c parallel/sequential version by namelist input
c without further modifications
c
c modifications for the sequential version:
c
c me: always 0
c listing and summary file unit numbers as usual
c    (parallel mode they are negative except for node 0)
c ga_initialize -> dummy
c ga_nodeid     -> returns 0
c ga_nnodes     -> returns 1
c ga_brdcst     -> dummy
c ga_sync       -> dummy
c ga_set_memory_limit -> dummy
c ga_create     -> dummy, return true
c ga_error      -> call bummer
c ga_create_mutexes -> dummy
c ga_destroy_mutexes -> dummy
c ga_print_distribution -> dummy
c ga_distribution -> dummy ??
c ga_igop         -> dummy
c ga_dgop         -> dummy
c ga_lock         -> dummy
c ga_unlock       -> dummy
c ga_fence        -> dummy
c ga_init_fence   -> dummy
c ga_get          -> replace by getvec
c ga_put          -> replace by putvec
c ga_acc          -> replace by putvec with acc
c ga_fill_patch   -> replace by wzero
c ga_zero         -> replace by wzero
c mitob           -> integer to byte conversion
c mdtob           -> double precision to byte conversion
c
c  all ga calls are in ciudg1.f, ciudg2.f, ciudg3.f, ciudg4.f
c
c ga.dcl modifizieren
c

       integer ga_nodeid
       integer  ga_nnodes
       integer   ga_read_inc
       logical  ga_create
       logical  ga_create_mutexes
       logical  ga_destroy_mutexes

      integer   MT_BYTE         ! byte
      integer   MT_INT          ! integer
      integer   MT_LOG          ! logical
      integer   MT_REAL         ! real
      integer   MT_DBL          ! double precision
      integer   MT_SCPL         ! single precision complex
      integer   MT_DCPL         ! real*8 complex


        integer mitob
        integer mdtob
C
C
C
C     Parameter variables
C
      INTEGER             WRNERR
      PARAMETER           (WRNERR = 0)
      INTEGER             NFTERR
      PARAMETER           (NFTERR = 1)
      INTEGER             FATERR
      PARAMETER           (FATERR = 2)
      INTEGER             LCORED
      PARAMETER           (LCORED = 5000000)
      INTEGER             MXUNIT, vdiskd
      PARAMETER (mxunit=98,vdiskd=200000)
*@ifdef int64
      integer isize
      PARAMETER (  isize=8)
*@else
*      integer isize
*      PARAMETER (  isize=4)
*@endif
c hl
      integer drtsiz,llenci
      COMMON/drtsize/drtsiz,llenci
c hl
C
C     Local variables
C
      INTEGER             IERR,        IFIRST,      LCORE
      INTEGER             OFFSET, vdisk,global,stack
      integer ex3w,ex4w,ex3x,ex4x,reclen,nsubmx
      integer lenci,nnodes,fileloc(10),dg,odg
C
       real*8, allocatable :: a(:)
       real*8, allocatable, TARGET :: vdsk(:)
       
      integer forbyt
      external forbyt



       integer  isflvdsk(mxunit),voffset,vsize,top
       integer  filestart(mxunit)
        integer mem1
        double precision, pointer :: mem2(:)
        COMMON /VPOINTER/mem2,isflvdsk,voffset,vsize,filestart,top(2)
c
c emulating virtual disk
c vdsk(voffset:voffset+vsize)  virtual disk space
c isflvdsk(*)                  is unit virtual disk or global array or l
c filestart(*)                 offset of the first element of file on vd
c top                          first free element of vdisk
c
c
       integer w_hdle,hd_hdle,ex3w_hdle,ex4w_hdle,ex3x_hdle,ex4x_hdle
       integer v_hdle,me,nxttask,drt_hdle,dg_hdle,ofdg_hdle
       COMMON /GAPOINTER/w_hdle, v_hdle, hd_hdle,ex3w_hdle,ex4w_hdle,
     .   ex3x_hdle,ex4x_hdle,me,nxttask,drt_hdle,dg_hdle,ofdg_hdle

c
c  ga only
c w_hdle : handle for sigma vector
c v_hdle : handle for ci vector
c hd_hdle: handle for diag vector
c ex3w_hdle: handle for fil3w
c ex3x_hdle: handle for fil3x
c ex4w_hdle: handle for fil4w
c ex4x_hdle: handle for fil4x
c me         "my" ga process id
c nxttask    next task number
c drt_hdle : handle for drtfil
c dg_hdle  : handle for diagint
c ofdg_hdle: handle for ofdgint
c
c      file locations
c      fileloc(*) = ofdg,diag,fil3x,fil3w,fil4x,fil4w,drtfil
c      value:   0 local disk  1: virtual disk  2: global array
c               3 distribute one proc sys
c               4 distribute two proc sys
c               5 distribute four proc sys
c      default:      1    1    2     2     2      2
c
c      flofdg=1
c      fldiag=2
c      fl3x=3
c      fl3w=4
c      fl4x=5
c      fl4w=6
c      fldrt=7
c      ldsk=0
c      vdsk=1
c      glar=2
c      done=3
c      dtwo=4
c      dfor=5
       integer flofdg,fldiag,fl3x,fl3w,fl4x,fl4w,fldrt
       integer ldsk,vdk,glar,done,dtwo,dfor
       parameter (flofdg=2,fldiag=1,fl3x=3,fl3w=4,fl4x=5,fl4w=6)
       parameter (fldrt=7,ldsk=0,vdk=1,glar=2,done=3,dtwo=4,dfor=5)


       logical status
        character*20 myname


c
c  allocate space and call the driver routine.
c
c  ifirst = first usable space in the the real*8 work array a(*).
c  lcore  = length of workspace array in working precision.
c  mem1   = additional machine-dependent memory allocation variable.
c  a(*)   = workspace array. a(ifirst : ifirst+lcore-1) is useable.
c
c
c     # mem1 and a(*) should be declared below within mdc blocks.
c
c     # local...
c     # lcored = default value for lcore.
c     # ierr   = error return code.
c
c
c     # bummer error types.
c
c     # sets up any communications necessary for parallel execution,
c     # and does nothing in the sequential case.
c




*@ifdef hpm
*       call f_hpminit_(taskID,"ciudg")
*       call _f_hpm_start_(1,385, "pciudg1.f", "ciudg_main")
*@endif

      call ga_initialize()
      me= ga_nodeid()
      nnodes = ga_nnodes()
      if (me.eq.0) then
      call getlcl( lcored, lcore, ierr )
      if ( ierr .ne. 0 ) then
         call bummer('ciudg: from getlcl: ierr=',ierr,faterr)
      endif
      endif
      call ga_brdcst(25201,lcore,isize,0)
      call ga_sync()

      if (me.eq.0) then
           call opn1st
           call pgname
      endif
      call ga_sync()
      call ibummr(6)

c
c
c     get filesizes         from cisrtif
c     get nint, next, lenci from cidrtfl
c     get max subspace dim  form ciudgin
c
c     stack = 2000000 DP
c     global = (nsubmx*2+1)*lenci + n4ext+n3ext
c     vdisk = n0ext+n1ext+n2ext+ndg0ext+ndg2ext+ndg4ext+oneldg
c     lcore = lcore-stack-global-vdisk
c

       if (me.eq.0)
     .   call getinfo(vdisk,lenci,nsubmx,ex3w,ex4w,ex3x,ex4x,
     .                reclen,drtsiz,fileloc,dg,odg,llenci)
        call ga_brdcst(25101,vdisk,isize,0)
        call ga_brdcst(25102,lenci,isize,0)
        call ga_brdcst(25103,nsubmx,isize,0)
        call ga_brdcst(25104,ex3w,isize,0)
        call ga_brdcst(25105,ex4w,isize,0)
        call ga_brdcst(25106,ex3x,isize,0)
        call ga_brdcst(25107,ex4x,isize,0)
        call ga_brdcst(25108,reclen,isize,0)
        call ga_brdcst(25109,drtsiz,isize,0)
        call ga_brdcst(25110,dg  ,isize,0)
        call ga_brdcst(25111,odg ,isize,0)
        call ga_brdcst(25112,fileloc,7*isize,0)
        call ga_brdcst(25113,llenci,isize,0)
        call ga_sync()


       stack=0
       global = 0
       if (fileloc(flofdg).eq.glar)
     . global =  global + ((dg-1)/nnodes+1)
       if (fileloc(fldiag).eq.glar)
     . global =  global + ((odg-1)/nnodes+1)
       if (fileloc(fl3x).eq.glar)
     . global =  global + ((ex3x-1)/nnodes+1)
       if (fileloc(fl3w).eq.glar)
     . global =  global + ((ex3w-1)/nnodes+1)
       if (fileloc(fl4x).eq.glar)
     . global =  global + ((ex4x-1)/nnodes+1)
       if (fileloc(fl4w).eq.glar)
     . global =  global + ((ex4w-1)/nnodes+1)
       if (fileloc(fldrt).eq.glar) then
*@ifdef int64
        global =  global + int(dble((drtsiz-1)/nnodes+1)*7.0d0)
*@else
*        global =  global + int(dble((drtsiz-1)/nnodes+1)*3.5d0)
*@endif
       endif
       global =  global + 1
       lcore=lcore-global-vdisk-stack
        if (me.eq.0) then
c      write(0,6010) global,vdisk,stack,lcore
       write(6,6010) global,vdisk,stack,lcore
        endif
6010   format(' Main memory management:'/
     .        ' global  ',i15,' DP per process'/
     .        ' vdisk   ',i15,' DP per process'/
     .        ' stack   ',i15,' DP per process'/
     .        ' core    ',i15,' DP per process')

        call ga_sync()
        if (lcore.lt.1) 
     .  call bummer('insufficient memory',0,'2')
c
c     # use standard f90 memory allocation.
c
c
      allocate( a(lcore),stat=ierr )
      if ( ierr .ne. 0 ) then
         call bummer('cimkseg: allocate failed ierr=',ierr,faterr)
      endif

       ifirst = 1
       mem1   = 0
c
c     # allocate memory for virtual disk
c
       if (vdisk.eq.0) then
         voffset=0
         vsize  =0
       else
       vsize=vdisk
       voffset=0
       allocate(mem2(vdisk+10))
       write(6,*) 'allocating vdisk of size',vdisk+10, ' DP'
       endif

c
c       create v-ga
c
c       status = ga_create(MT_DBL,lenci,nsubmx,arrayname,chunk1,chunk2,v
        if (llenci.gt.0) then
        status = ga_create(MT_DBL,llenci,nsubmx,'civect',
     .       (llenci-1)/nnodes+1,nsubmx,v_hdle)
        else
        status = ga_create(MT_DBL,lenci,nsubmx,'civect',
     .       (lenci-1)/nnodes+1,nsubmx,v_hdle)
        endif
        if (.not. status) call ga_error('ga_create v-array failed',2)

        call statusga(v_hdle)
c
c       create w-ga
c
c       status = ga_create(MT_DBL,lenci,nsubmx,arrayname,chunk1,chunk2,w
        if (llenci.gt.0) then
        status = ga_create(MT_DBL,llenci,nsubmx,'sigvec',
     .        (llenci-1)/nnodes+1,nsubmx,w_hdle)
        else
        status = ga_create(MT_DBL,lenci,nsubmx,'sigvec',
     .        (lenci-1)/nnodes+1,nsubmx,w_hdle)
        endif
        if (.not. status) call bummer('ga_create w-array failed',0,2)
        call statusga(w_hdle)

c
c
c       create hdiag-ga
c
c       status = ga_create(MT_DBL,lenci,1,arrayname,chunk1,chunk2,hd_hdl
        if (llenci.gt.0) then
        status = ga_create(MT_DBL,llenci,1,'hdg',
     .        (llenci-1)/nnodes+1,1,hd_hdle)
        else
        status = ga_create(MT_DBL,lenci,1,'hdg',
     .        (lenci-1)/nnodes+1,1,hd_hdle)
         endif
        if (.not. status) call bummer('ga_create hd-array failed',0,2)
        call statusga(hd_hdle)
c

c
c       create fil3w integral ga
c
        if (fileloc(fl3w).eq.glar) then
        status = ga_create(MT_DBL,ex3w,1,'ex3w',
     .        (ex3w-1)/nnodes +1,1,ex3w_hdle)
        if (.not. status)
     .    call bummer('ga_create fil3w-array failed',0,2)
        call statusga(ex3w_hdle)
        endif
c
c       create fil4w integral ga
c
        if (fileloc(fl4w).eq.glar) then
        status = ga_create(MT_DBL,ex4w,1,'ex4w',
     .        (ex4w-1)/nnodes +1,1,ex4w_hdle)
        if (.not. status)
     .     call bummer('ga_create fil4w-array failed',0,2)
        call statusga(ex4w_hdle)
        endif

c
c       create fil3x integral ga

        if (fileloc(fl3x).eq.glar) then
        status = ga_create(MT_DBL,ex3x,1,'ex3x',
     .        (ex3x-1)/nnodes+1,1,ex3x_hdle)
        if (.not. status)
     .        call ga_error('ga_create fil3x-array failed',0)
        call statusga(ex3x_hdle)
        endif

c
c       create fil4x integral ga
c
        if (fileloc(fl4x).eq.glar) then
        status = ga_create(MT_DBL,ex4x,1,'ex4x',
     .        (ex4x-1)/nnodes+1,1,ex4x_hdle)
        if (.not. status)
     .     call bummer('ga_create fil4x-array failed',0,2)
        call statusga(ex4x_hdle)
        endif

c       create diagint integral ga
c
        if (fileloc(fldiag).eq.glar) then
        status = ga_create(MT_DBL,dg,1,'diag',
     .        (dg-1)/nnodes+1,1,dg_hdle)
        if (.not. status)
     .     call bummer('ga_create dg-array failed',0,2)
        call statusga(dg_hdle)
        endif

c
c       create ofdgint integral ga
c
        if (fileloc(flofdg).eq.glar) then
        status = ga_create(MT_DBL,odg,1,'ofdg',
     .        (odg-1)/nnodes+1,1,ofdg_hdle)
        if (.not. status)
     .     call bummer('ga_create ofdg-array failed',0,2)
        call statusga(ofdg_hdle)
        endif

c


c
c       create drt-ga
c
        if (fileloc(fldrt).eq.glar) then
        status = ga_create(MT_DBL,forbyt(drtsiz),7,'drt',
     .       (forbyt(drtsiz)-1)/nnodes+1,7,drt_hdle)
        if (.not. status) call ga_error('ga_create drt-array failed',2)

        call statusga(drt_hdle)
        endif

c
c       create nxttask

        status = ga_create(MT_INT,nnodes,1,'nxttask',
     .          1,1,nxttask)
        if (.not. status)
     .     call bummer('ga_create nxttask failed',0,2)
        call statusga(nxttask)
       status = ga_create_mutexes(5)
       if (.not.status)
     .   call ga_error('inittask,ga_create_mutexes failed',1)

      write(6,*) 'CIUDG version 5.9.7 ( 5-Oct-2004)'

      call driver( a(ifirst), (lcore), (mem1), (ifirst) )
c
c
c     # return from driver() implies successful execution.
c
c     # message-passing cleanup: stub if not in parallel
c
       status = ga_destroy_mutexes( )
       if (.not.status)
     .   call ga_error('inittask,ga_destroy_mutexes failed',1)


      call ga_terminate()

      if (me.eq.0) call bummer('normal termination',0,3)

*@ifdef hpm
*        call _f_hpm_stop_(1,764)
*       call f_hpmterminate_(taskID)
*@endif

      stop 'end of ciudg'
      end
      subroutine getinfo(vdisk,lenci,nsubmx,
     .  szex3w,szex4w,szex3x,szex4x,maxbuf,drtsiz,floc,
     .  szdg,szodg,llenci2)
      implicit none
C HPM  include
C HPM   include 'f_hpm.h'
        INTEGER taskID
C HPM
c      file locations
c      fileloc(*) = ofdg,diag,fil3x,fil3w,fil4x,fil4w,drtfil
c      value:   0 local disk  1: virtual disk  2: global array
c               3 distribute one proc sys
c               4 distribute two proc sys
c               5 distribute four proc sys
c      default:      1    1    2     2     2      2
c
c      flofdg=1
c      fldiag=2
c      fl3x=3
c      fl3w=4
c      fl4x=5
c      fl4w=6
c      fldrt=7
c      ldsk=0
c      vdsk=1
c      glar=2
c      done=3
c      dtwo=4
c      dfor=5
       integer flofdg,fldiag,fl3x,fl3w,fl4x,fl4w,fldrt
       integer ldsk,vdk,glar,done,dtwo,dfor
       parameter (flofdg=2,fldiag=1,fl3x=3,fl3w=4,fl4x=5,fl4w=6)
       parameter (fldrt=7,ldsk=0,vdk=1,glar=2,done=3,dtwo=4,dfor=5)

      integer mxnseg
      parameter (mxnseg=200)
      integer maxsym
      parameter (maxsym=8)
      integer vdisk,floc(7)
      integer version,ntit,nsife,i
      integer bufszi, nd4ext, nd2ext, nd0ext,
     .  n4ext, n3ext, n2ext, n1ext, n0ext,
     .  n2int, n1int, n0int,minbl4, minbl3, maxbl2,
     .  nbas(maxsym), maxbuf,
     .  nmot,niot,nfct,nfvt,nrow,nsym,ssym,xbar(4),nvalw(4),ncsft
       real*8 csfprn(16),etolbk(16),ETOL(16),CTOL(16),RTOLBK(16),
     .    RTOL(16),RTOLCI(16),G,PTTOL(16),LRTSHIFT
       integer nsegwx(4),nseg0x(4),nseg1x(4),nseg2x(4),nseg3x(4),
     .  nseg4x(4),nsegso(4),cdg4ex,c3ex1ex,c2ex0ex,fileloc(10)
       integer lvlprt,nroot,noldv,noldhv,nunitv,nbkitr,niter,
     .         davcor,uciopt(5),ivmode,istrt,vout,iortls,
     .         nvbkmx,ibktv,ibkthv,nvcimx,icitv,icithv,frcsub,
     .         nciitr,mode,ninitv,nvbkmn,nvcimn,maxseg,ntype,
     .         pspace,ssmode,gset,aodrv,nrfitr,ncorel,ptgref,ptlevl,
     .         ptmeth,ptiter,nvrfmx,nvrfmn
c
       integer iden,itran,iroot,froot,rtmode,ptnavst,ptnvmx,ptlinsol,
     .         ftcalc,ptshift,ptwavst,ptmtst,ncouple,pth0type,skipso,
     .         no0ex,no1ex,no2ex,no3ex,no4ex ,nodiag,
     .         finalv,finalw,llenci2,llenci
       integer lenci,drtsiz,nsubmx,nmonel,irec,
     .         nnseg0(mxnseg),nnseg1(mxnseg),nnseg2(mxnseg),
     .         nnseg3(mxnseg),
     .         nnseg4(mxnseg),nnsegso(mxnseg),nnsegwx(mxnseg)
     .         ,zerotasks(1000)

*@ifdef bit64
       integer*8 global
*@else
*       integer global
*@endif
       integer szex3w,szex4w,szex3x,szex4x,szdg,szodg
       integer directhd

c
        integer cosmocalc
        common/cosmo2/cosmocalc
c

      NAMELIST / INPUT  / LVLPRT,      NROOT,       NOLDV,       NOLDHV
     x,                   NUNITV,      NBKITR,      NITER,       DAVCOR
     x,                   CSFPRN,      ETOLBK,      ETOL,        CTOL
     x,                   UCIOPT,      IVMODE,      ISTRT,       VOUT
     x,                   IORTLS
     x,                   NVBKMX,      IBKTV,       IBKTHV,      NVCIMX
     x,                   ICITV,       ICITHV,      FRCSUB,      NCIITR
     x,                   RTOLBK,      RTOL,        RTOLCI,      MODE
     x,                   NINITV,      NVBKMN,      NVCIMN,      MAXSEG
     x,                   NTYPE,       PSPACE,      SSMODE,      GSET
     x,                   G,           AODRV,       NRFITR,      NCOREL
     x,                   PTGREF,      PTLEVL,      PTMETH,      PTITER
     x,                   PTTOL,       NVRFMX,      NVRFMN
     x,                   IDEN
     x,                   ITRAN,       IROOT,       FROOT,       RTMODE
     x,                   PTNAVST,     PTNVMX,      PTLINSOL,    FTCALC
     x,                   PTSHIFT,     PTWAVST,     PTMTST
     x,                   LRTSHIFT,    NCOUPLE,     PTH0TYPE,    SKIPSO
     x,                   no0ex,no1ex,no2ex,no3ex,no4ex ,nodiag
     x,                   nsegwx,nseg0x,nseg1x,nseg2x,nseg3x,nseg4x,
     .                    nsegso,cdg4ex,c3ex1ex,c2ex0ex,fileloc,
     .                    finalv,finalw,llenci,cosmocalc,
     .                    nnseg0,nnseg1,nnseg2,nnseg3,nnseg4,nnsegso,
     .                    nnsegwx,zerotasks,molcas,directhd
       integer molcas


C
C     Common variables
C
      integer             nfilmx
      parameter ( nfilmx=55)
      INTEGER             NUNITS(NFILMX)
C
      COMMON / CFILES /   NUNITS
C     Common variables
C
      integer fildrt,filsti,filinp
      equivalence (filinp,nunits(2))
      equivalence (fildrt,nunits(10))
      equivalence (filsti,nunits(39))


*@ifdef sp4  
*       integer*8 fsize
*@else
       integer fsize
*@endif
       external fsize

       character*10 fstr
       character*14 location(0:5)
       data location/'local disk    ','virtual disk ','global array  ',
     .   'distr. (sg)   ','distr. (dual) ','distr. (quadr)'/

c
        rewind(fildrt)
        read(fildrt,*)
        read(fildrt,*)
        read(fildrt,*)
        read(fildrt,*)
        read(fildrt,'(a10)') fstr
c      # read line
        read(fildrt,fstr)
     .  nmot,niot,nfct,nfvt,nrow,nsym,ssym,xbar,nvalw,lenci
c
c       bufsiz=(4+1+4)*nrow+indxlen+nvwalk*2+1
c
c      drtsiz = nwalk+9*nrow*maxseg + 2*nvalw
C HPM  subroutine_start
c       call _f_hpm_start_(2,929, "pciudg1.f","getinfo")
C HPM
        drtsiz=(xbar(1)+xbar(2)+xbar(3)+xbar(4))
     .        +(4+1+4)*nrow*mxnseg
     .        +(nvalw(1)+nvalw(2)+nvalw(3)+nvalw(4))*2+1
        rewind(fildrt)
c       write(0,*) 'number of internal orbitals:',niot
c       write(0,*) 'number of external orbitals:',nmot-niot-nfct-nfvt
c       write(0,*) 'length of CI vector: ',lenci


c
c      # read namelist input
c
c      fileloc(*) = ofdg,diag,fil3x,fil3w,fil4x,fil4w
c      value:   0 local disk  1: virtual disk  2: global array
c               3 process 0 distributes to local disk on 1 to n
c               4 process 0 distributes to local disk on 2,4,6,...
c               5 process 0 distributes to local disk on 4,8,10,...
       fileloc(fldiag)=ldsk
       fileloc(flofdg)=ldsk
       fileloc(fl3x)=ldsk
       fileloc(fl3w)=ldsk
       fileloc(fl4x)=ldsk
       fileloc(fl4w)=ldsk
       fileloc(fldrt)=ldsk
c      set defaults
       nvbkmx=1
       nvrfmx=1
       llenci=-1

       open(filinp,file='ciudgin',status='old')
       read(filinp,input,end=110)
       close(filinp)
c110    cosmocalc =cosmocalc
 110    nsubmx =nvcimx
        nsubmx =nvcimx
       fileloc(fldiag)=ldsk
       fileloc(flofdg)=ldsk
       fileloc(fl3x)=ldsk
       fileloc(fl3w)=ldsk
       fileloc(fl4x)=ldsk
       fileloc(fl4w)=ldsk
       fileloc(fldrt)=ldsk


       if (llenci.gt.0 .and. ( niter.gt.0 .or. nbkitr.gt.0))
     .    llenci=-1
        write(6,*) 'using llenci=',llenci



c
c    fileloc must have the values 0 .. 4, only
c
        do i=1,7
         if (fileloc(i).ne.0)
     .     call bummer('invalid fileloc values, fl<>0 ',
     .      fileloc(i),2)
        enddo

        global = 1 
        vdisk = 1
        write(6,*) 'nsubmx=',nsubmx,' lenci=',lenci
        write(6,3) 'drt:  ',drtsiz*7,(drtsiz*7.)/262144.
2       format(20('===='))
3       format(a,t20,i8,3x,'(',f18.2,' MB)')
        write(6,2)
c
        do i=1,7
         floc(i)=fileloc(i)
        enddo
        llenci2=llenci

C HPM   return
c       call _f_hpm_stop_(2,1102)
C HPM
       return
       end
        subroutine statusga (g_a)
        implicit none
C HPM  include
C HPM   include 'f_hpm.h'
        INTEGER taskID
C HPM
        integer g_a
       integer ga_nodeid
       integer  ga_nnodes
       integer   ga_read_inc
       logical  ga_create
       logical  ga_create_mutexes
       logical  ga_destroy_mutexes

      integer   MT_BYTE         ! byte
      integer   MT_INT          ! integer
      integer   MT_LOG          ! logical
      integer   MT_REAL         ! real
      integer   MT_DBL          ! double precision
      integer   MT_SCPL         ! single precision complex
      integer   MT_DCPL         ! real*8 complex


        integer mitob
        integer mdtob
C

C HPM   return
        end
       subroutine pgname
       implicit none
C HPM  include
C HPM   include 'f_hpm.h'
        INTEGER taskID
C HPM
      INTEGER             NFILMX
      PARAMETER           (NFILMX = 55)


C     Common variables
C
      INTEGER             NUNITS(NFILMX)
C
      COMMON / CFILES /   NUNITS


       integer tape6,nslist

C HPM  subroutine_start
c       call _f_hpm_start_(4,1320, "pciudg1.f","pgname")
C HPM
       tape6=nunits(1)
       nslist=nunits(3)

c+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      write(nslist,6010)
      write(tape6,6010)
6010  format('1'/' program ciudg      '
     & /' multireference single and double excitation configuration'
     & /' interaction based on the graphical unitary group approach.'/)
      write(tape6,6011)
6011  format(/' references:',t15,'h. lischka, r. shepard, f. b. brown,',
     & ' and i. shavitt,'
     & /t19,'int. j. quantum chem. s 15, 91 (1981).'
     & /t15,'r. shepard, r. a. bair, r. a. eades, a. f. wagner,'
     & /t19,'m. j. davis, l. b. harding, and t. h. dunning,'
     & /t19,'int j. quantum chem. s 17, 613 (1983).'
     & /t15,'r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,'
     & /t19,'h. schiffer, h. lischka, and m. schindler,'
     & /t19,'j. comp. chem. 6, 200 (1985).'
     & /t15,'r. shepard, i. shavitt, r. m. pitzer, d.',
     & ' c. comeau, m. pepper'
     & /t19,'h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and',
     & /t19,'j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).')
c    & //' version date: 21-jun-2000'/)
c
c     # print out the name and address of the local programmer
c     # responsible for maintaining this program.
c
      call who2c( 'CIUDG', tape6 )
*@ifdef direct
*      call headwr(tape6,'CIUDG_D','09/20/00')
*@else
      call headwr(tape6,'CIUDG','-varseg3-')
*@endif
C HPM   return
c       call _f_hpm_stop_(4,1357)
C HPM
       return
       end
