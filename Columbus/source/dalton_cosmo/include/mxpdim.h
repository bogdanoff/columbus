C. MXPST : max number of strings needed in local arrays in CISIG9/DENSIT
C. MXPTP : max number of RAS types
C. MXOCTP: max number of occupation types
      PARAMETER (MXPST = 30 000, MXPTP = 50, MXOCTP = 100)
