!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      subroutine writsude(nip,sude,isude,icossu)
c
c write sude information to file sude (icossu)

      implicit real*8 (a-h,o-z)

      dimension sude(*),isude(*)

      write(icossu,*) nip
      write(icossu,*) (sude(i),i=1,4*nip)
      write(icossu,*) (isude(i),i=1,4*nip)
      return

      end
