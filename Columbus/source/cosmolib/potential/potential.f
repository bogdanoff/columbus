!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
C
       program potenrg
       implicit none



       real*8 x,y,z,dx,dy,dz
       integer i,j,k,nx,ny,nz
       real*8, pointer ::ppoints(:,:),phi(:)
       real*8, allocatable :: work(:)
       integer lwork,npp
       common /potentialptr/ppoints,phi,npp        

        open(unit=6,file='elpotls',form='formatted')
       call potheader

        open(unit=8,file='elpotin',form='formatted',status='old',
     .      err=198)
        i=0
        read(8,*,err=199) npp
        allocate(ppoints(3,npp))
        do i=1,npp
        read(8,*,end=200,err=199) x,y,z 
        ppoints(1,i)=x
        ppoints(2,i)=y
        ppoints(3,i)=z
        enddo
        write(6,301) npp
 301    format(' READ ',i4,' points')
        write(6,302)(i,(ppoints(j,i),j=1,3),i=1,npp) 
 302    format('#',i4,' ',3f10.6)
        close(8)
        allocate(phi(npp))
        lwork=10000000
        allocate(work(lwork))
        call potential(work,lwork,ppoints,npp,phi,'nocoef')

        open(unit=8,file='elpotpts',form='formatted')
        write(8,*) npp, 'points'
        write(8,303) (i,(ppoints(j,i),j=1,3),phi(i),i=1,npp)
        write(6,*) '-------- Computed Potential  --------'
        write(6,303) (i,(ppoints(j,i),j=1,3),phi(i),i=1,npp)
 303    format('#',i4,' xyz=',3f10.6,' phi=',f10.6)
        close(8)
        deallocate(work,phi,ppoints)
        stop
 198    call bummer('could not open input file elpotin',0,2)
 199    call bummer('could not correctly read elpotin, line',i,2)
 200    call bummer('perlimnary end of elpotin, line',i,2)
        end


        subroutine potheader

        write(6,101) 
        write(6,102) 

 101    format('****************************************************'/
     .         '******     Electrostatic Potential             *****'/
     .         '******         -------------                   *****'/
     .         '******     COLUMBUS 5.9.x                      *****'/
     .         '******     exptl. Version (Dalton, only)       *****'/
     .         '******         -------------                   *****'/
     .         '******        Thomas Mueller                   *****'/
     .         '******    Central Institute of Applied Math.   *****'/
     .         '******    Research Centre Juelich              *****'/
     .         '******    D-52425 Juelich, Germany             *****'/
     .         '****************************************************')

 102    format(' Input files:  elpotin                              '/
     .         '            :  nocoef                               '/
     .         ' Listing file: elpotls                              '/
     .         ' Output file: elpotpts                              '/
     .           //
     .         ' nocoef     :    SCF mocoef file                    '/  
     .         '             or  MCSCF nocoef* file                 '/  
     .         '             or  CI    nocoef* file                 '/  
     .           //
     .         ' elpotin structure                                  '/  
     .         '  Line 1:  number of points                         '/
     .         '  subsequently x,y,z coordinates, one point per line'/
     .           //
     .         ' elpotpts file structure                            '/
     .         '  <npp> points                                      '/
     .         ' # ptnr  <x y z coords> < potential >               ')

        return
        end
       





      subroutine potential(work,lwork,
     .  ppoints, npp, phi, fnocoef)
c
c      work(lwork) workspace
c      ppoints(3,npp) potential to be evaulated at (x,y,z; 1:npp)
c      phi(npp) potential  (output)
c
c      Based upon the elementary one-electron hamilton integrals
c      and symmetry treatment of DALTON 1.x
c      Rewrite and reoptimisation of an initial stripped down
c      version by Silmar do Monte. 
c 

      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
C     -------------------------------------------------------
      PARAMETER (LUCMD = 5, LUPRI = 6)
c
c Silmar - 12/09/01- New common block(Vne) to access the
c Nuclear attraction integrals
c      
      INTEGER LWORK
       REAL*8 WORK(LWORK),WRKDLM 
       DATA WRKDLM/8H*WRKDLM*/
       integer maxorb
       parameter(maxorb=255)
       integer npp
       real*8 ppoints(3,npp), phi(npp)
       character*(*) fnocoef
c
      integer ntitle,mxtitle
      parameter (mxtitle = 6)
      parameter (nfilmx =55)
      integer nunits(nfilmx)
      integer filerr,typecalc
      integer noffstri
      integer noffsrec,noffslin(8)
      integer k,info(5) 
      integer dbglvl 
      integer faterr
      integer nbfpsy(8),nmopsy,nps,nrestp
      dimension noffstri(8)
      dimension noffsrec(8)
      dimension nmopsy(8)
      logical loop
      parameter(faterr=2)
      character*80 title(mxtitle)
      character*80 afmt
      character*4 labels(8)
      integer  forbyt,atebyt,nbmt
      external forbyt,atebyt
c common block for file units
      integer prtab,fmocoef,outputunit
      common/cfiles/nunits
      equivalence(nunits(45),prtab)
      equivalence(nunits(42),fmocoef)
      equivalence(nunits(1),outputunit)
      equivalence(nunits(12),nrestp)
c
c nrestp: unit of mcscf natural orbitals
c
c common block to get the root number      
      integer root
      common/rootnumber/root
c
       common/cosmoblock/nbfpsy
c
c In case of MCSCF calculation the nsym to be used should come
c from this common block
c
      integer nxy, mult, nsym,nsymm, nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
c
c
c Opens a file containing the coordinates of the charges
c point is the counter for the number of points
c
      dbglvl = 0
c
      maxsize=maxorb*(maxorb+1)/2
c
c     always read the nocoefs
c     if necessary extract them from restart by mofmt
c
      open(prtab,file=fnocoef,status='old')
      nlist = lupri
c First read the title section to determine maxvect
c
      call moread(prtab,10,filerr,syserr,mxtitle,ntitle,
     & title,afmt,nsym,nbfpsy,nmopsy,labels,0,WORK) 
      if(filerr.ne.0) call moerr(filerr,syserr,faterr)
c      
c     locmo :  start address of mo coefficients
c     lococc:  start address of occupation number vector
c     locdens:  start address of density matrix
c     loctop:  start address of free part
c
c     noffstri(8)  symmetry block offsets for triangular 
c                  stored matrix
c     noffsrec(8)  symmetry block offsets for rectangular
c                  stored matrix
c     i.e. access 3rd symmetry block : core(locmo+noffsrec(3))
c
      maxocc  = 0 
      maxvect = 0
      maxdens = 0 
      noffslin(1)=0
      noffstri(1)=0
      noffsrec(1)=0
      do i = 1,nsym
	    noffslin(i)=noffslin(i-1)+nbfpsy(i-1)
	    noffstri(i)=noffstri(i-1)+nbfpsy(i-1)*(nbfpsy(i-1)+1)/2
	    noffsrec(i)=noffsrec(i-1)+nbfpsy(i-1)**2
         maxvect = maxvect + nbfpsy(i)**2
         maxocc  = maxocc  + nbfpsy(i) 
         maxdens = maxdens + nbfpsy(i)*(nbfpsy(i)+1)/2
      enddo
      norb=maxocc
c      
      locmo= 1
      lococc= locmo + maxvect
      locdens= lococc + maxocc
      loctop = locdens + maxdens
c
      if (loctop.gt.maxsize) 
     .  call bummer('loctop is greater than maxsize',0,2)
c
c reads the molecular orbital coefficients(20)      
c      
      call moread(prtab,20,filerr,syserr,mxtitle,ntitle,
     & title,afmt,nsym,nbfpsy,nmopsy,labels,maxvect,WORK(locmo)) 
      if(filerr.ne.0) call moerr(filerr,syserr,faterr)
c       
c      
c read the occupation numbers(40)      
c      
      call moread(prtab,40,filerr,syserr,mxtitle,ntitle,
     & title,afmt,nsym,nbfpsy,nmopsy,labels,maxocc,WORK(lococc)) 
      if(filerr.ne.0) call moerr(filerr,syserr,faterr)
      close(unit=prtab,status='keep')
c
c  This subroutine transforms diagonal density to AO density
c
         call trfblock(WORK(lococc),WORK(locmo), WORK(locdens),
     .                 nsym,nbfpsy,noffsrec,noffstri,maxdens,
     .                  maxvect,maxocc)
       if(dbglvl.eq.1) then
       call plblks('AO density  ',WORK(locdens),
     .               nsym, nbfpsy, 'AO  ',afmt,6)
       endif
c               
C
         WRITE(*,'(/A/A,I8,A)')
     &      ' DALTON: user specified work memory size used,',
     &      '          -m = "',LWORK,'"'
      LMWORK=LWORK
      NBYTES = (LMWORK) * 8
      XMBYTES = NBYTES
      XMBYTES = XMBYTES / (1024*1024)
      WRITE(*,'(/A,I12,A,F8.2,A)') ' Work memory size (LMWORK) :',
     &   LMWORK,' =',XMBYTES,' megabytes.'
      IWOFF=0
C
C     Set memory traps.
C
      WORK(IWOFF+0+loctop) = WRKDLM
      WORK(IWOFF+2+LMWORK) = WRKDLM
C
c      call headwr(LUPRI,'DALTON','5.4.0.0 ') 
      CALL GNRLIN(WORK(loctop),LMWORK-loctop+1)
      
      write(*,*)'***Electrostatic Potential Calculation***'
       loop =.true.
       first=.false.
c
c  loop for the point charges starts
c       
      call EXEHER(WORK(locdens),maxdens,
     .  WORK(loctop), LMWORK-loctop+1, IWOFF, WRKDLM) 
      call bummer('normal termination',0,3)
      return
      END
c
C  /* Deck bndchk */
      SUBROUTINE BNDCHK(WORK, LMWORK, IWOFF, WRKDLM, PROG)



      IMPLICIT DOUBLE PRECISION (A-H,O-Z)


c#if defined (VAR_MPI)
c#include <infmpi.h>
c      INCLUDE 'mpif.h'
c#endif
      DIMENSION WORK(LMWORK)
      CHARACTER*6 PROG
C
C     -------------------------------------------------------
      PARAMETER (LUCMD = 5, LUPRI = 6)
C
C     Check memory traps. Gives error message if any of the programs
C     have been outside the declared memory area.
C
      IF (WORK(IWOFF + 1) .NE. WRKDLM .OR.
     &    WORK(IWOFF + LMWORK + 2) .NE. WRKDLM) THEN
c         WRITE (LUPRI,'(//A,A6,A)')
c     *      ' >>> WARNING, ',PROG,' has been out of bounds.'
c         IF (WORK(IWOFF + 1) .NE. WRKDLM) WRITE (LUPRI,'(/A)')
c     *      ' >>> WORK(0) has been destroyed'
c         IF (WORK(IWOFF + LMWORK + 2) .NE. WRKDLM) WRITE (LUPRI,'(/A)')
c     *      ' >>> WORK(LMWORK+1) has been destroyed'
         CALL QUIT('WARNING, ' // PROG // ' has been out of bounds.')
c#if defined (VAR_MPI)
c         CALL MPI_ABORT(MPI_COMM_WORLD,IERR,IERR)
c#endif
      END IF
      RETURN
      END
C  /* Deck exeher */
      SUBROUTINE EXEHER(DENS,ndens,WORK, LMWORK, IWOFF, WRKDLM)



      IMPLICIT DOUBLE PRECISION (A-H,O-Z)

       integer ndens
       double precision dens(ndens)

      DIMENSION WORK(LMWORK)
C
C     -------------------------------------------------------
      PARAMETER (LUCMD = 5, LUPRI = 6)
C
C     Run Integral section
C
C     We need to open an extra output file needed for CM and shielding
C     polarizabilities, kr and sc, oct-95
C
      OPEN (UNIT=66, FILE = 'DALTON.CM', FORM='FORMATTED')
C
c      WRITE(LUPRI, '(A/)') '    Starting in Integral Section -'
C     The dollar sign in the previous line may not be standard.
C     It causes the End of Record to be skipped.
ct     OPEN(LUCMD, FILE = 'DALTON.INP')
        OPEN(LUCMD, FILE = 'daltcomm')
      CALL QENTER('HERMIT')
      CALL HERCTL(DENS,ndens,WORK(IWOFF+2),LMWORK)
      CALL QEXIT('HERMIT')
      CALL BNDCHK(WORK, LMWORK, IWOFF, WRKDLM, 'HERMIT')
      CLOSE(LUCMD)
      CLOSE(24)
c      WRITE(LUPRI, '(/A)') '- End of Integral Section'
      RETURN
      END
C  /* Deck gnrlin */
      SUBROUTINE GNRLIN(WORK,LWORK)
C
C     GENERAL input
C



      IMPLICIT DOUBLE PRECISION (A-H,O-Z)








      PARAMETER (MXCENT = 130, MXCOOR = 3*MXCENT)









      PARAMETER (MXSHEL = 200, MXPRIM = 800, MXCORB = 400,


     *           MXORBT = MXCORB*(MXCORB + 1)/2)
      DIMENSION WORK(LWORK)
C
C     -------------------------------------------------------
      PARAMETER (LUCMD = 5, LUPRI = 6)
      LOGICAL INPPRC, NEWGEO
      COMMON /SIRIPC/ INPPRC, NEWGEO
C
C$Id: potential.f,v 1.3 2005/12/13 11:26:44 hlischka Exp $
C
      LOGICAL TESTIN, OPTIMI, RNALL,  RNHERM, RNSIRI, RNABAC, GEOCNV,
     &        HRINPC, SRINPC, ABINPC, RDINPC, RDMLIN, PARCAL, DIRCAL,
     &        WRINDX, WLKREJ, WALKIN, RNRESP, RNINTS, USRIPR, SEGBAS,
     &        DOCCSD, MINIMI, NEWSYM, NEWBAS, NEWPRP
      COMMON /GNRINF/ PANAS,  TESTIN, OPTIMI, RNALL,  RNHERM, RNSIRI, 
     &                RNABAC, GEOCNV, HRINPC, SRINPC, ABINPC, RDINPC, 
     &                RDMLIN, GRADML, PARCAL, DIRCAL, KCHARG, WRINDX, 
     &                WLKREJ, WALKIN, RNRESP, RNINTS, USRIPR, ITERNR, 
     &                ITERMX, IPRUSR, SEGBAS, DOCCSD, MINIMI, NEWSYM, 
     &                LENBAS, NEWBAS, NEWPRP, BASDIR
      CHARACTER*60 BASDIR
      LOGICAL MOLGRD, MOLHES, DIPDER, POLAR,  TSTINP,
     &        VIB,    RESTAR, DOWALK, GDALL,  CCSD,
     &        H2MO,   DOSYM,  DOLRES, DOEXCI, SHIELD,
     &        SPNSPN, MAGSUS, VCD,    NACME,  AAT,
     &        NOLOND, FCKDDR, ECD, NODIFC, DODRCT,
     &        SUPMAT, PARALL, MOLGFA,
     &        SPINRO, MASSVE, DARWIN, ABALNR, VROA, NOCMC,
     &        EXPFCK, RAMAN, QUADRU, NQCC
      COMMON /ABAINF/ IPRDEF, NWNABA,
     &                MOLGRD, MOLHES, DIPDER, POLAR,  TSTINP,
     &                VIB,    RESTAR, DOWALK, GDALL,  CCSD,
     &                H2MO,   DOSYM(8), DOLRES, DOEXCI,
     &                NTCSY(8), NSCSY(8), SHIELD, SPNSPN,
     &                MAGSUS, VCD, NACME, AAT, NOLOND, FCKDDR, ECD,
     &                DODRCT, SUPMAT, PARALL, MOLGFA, SPINRO,
     &                MASSVE, DARWIN, ABALNR, VROA,
     &                NODIFC, ISOTOP(MXCENT), NOCMC, EXPFCK,
     &                RAMAN, QUADRU, NQCC
      LOGICAL FTRONV, GTRONV, HTRONV, RTRONV, FTWOXP, SORTXP,
     &                PTRTXP, NPVTXP, FABRHS, FTRCTL
      COMMON /EXEINF/ FTRONV, GTRONV, HTRONV, RTRONV, NASTXP, FTWOXP,
     &                SORTXP, PTRTXP, NPVTXP, FABRHS, FTRCTL, IPTRAB
      LOGICAL ADDSTO
C
C     Worst case scenario for HUCEXC; a user running STO-3G (Hueckel basis
C     half the size of the total basis set size
C
      COMMON /HUCKEL/ HUCCNT, HUCEXC(MXSHEL), ADDSTO, NHUCAO(8), 
     &                NHUCBA, IHUCPT(MXSHEL)

      PARAMETER (NDIR = 7, NTABLE = 23)
                 
C
C     Initialize /SIRIPC/
C
      INPPRC = .FALSE.
C
C     Initialize /ABAINF/
C
      MOLGRD = .FALSE.
      MOLHES = .FALSE.
      DOWALK = .FALSE.
C
C     Initialize /GNRINF/
C
      SEGBAS = .TRUE.
      WALKIN = .FALSE.
      HRINPC = .FALSE.
      SRINPC = .FALSE.
      ABINPC = .FALSE.
      RDINPC = .FALSE.
      RDMLIN = .FALSE.
      TESTIN = .FALSE.
      OPTIMI = .FALSE.
      ITERNR = 0
      ITERMX = 20
      USRIPR = .FALSE.
      RNALL  = .FALSE.
      RNHERM = .FALSE.
      RNSIRI = .FALSE.
      RNABAC = .FALSE.
      RNRESP = .FALSE.
      PARCAL = .FALSE.
      DIRCAL = .FALSE.
      RNINTS = .FALSE.
      DOCCSD = .FALSE.
      MINIMI = .FALSE.
      NEWSYM = .FALSE.
      NEWBAS = .TRUE.
      NEWPRP = .TRUE.
      PANAS  = 0.0D0
C
C     Initialize /EXEINF/
C
      FTRONV = .TRUE.
      FTWOXP = .TRUE.
      FABRHS = .TRUE.
      FTRCTL = .TRUE.
C
C     Initialize /HUCKEL/
C
      ADDSTO = .TRUE.
      HUCCNT = 2.0D0
      CALL IZ000(NHUCAO,8)
      CALL IZ000(IHUCPT,MXSHEL)


      USRIPR=.true.
      RNHERM=.true.
      return
      END
C  /* Deck exeaba */

c
c This subroutine transforms the diagonal density from MO basis to A
c    + O basis

c
        subroutine trfblock(occ,mo,dens,nsym,nbfpsy,noffsrec,noffstri,
     .     maxdens,maxmo,maxocc)
	implicit none
	integer nsym,nbfpsy(nsym),noffsrec(nsym),noffstri(nsym)
	integer maxdens,maxmo,maxocc
	real*8  occ(*),mo(*),dens(*)
	integer isym,icntdens,icntocc,icnt2,icnt3,i,mu,nu
        real*8 fac 
	 call wzero(maxdens,dens,1)

	 do isym=1,nsym
          if(nbfpsy(isym).ne.0) then
	    icntdens=noffstri(isym)
	    if (isym.eq.1) then 
	        icntocc=0
	    else
	        icntocc=icntocc+nbfpsy(isym-1)
	    endif
	    do mu = 1, nbfpsy(isym)
	      do nu = 1, mu
                if (nu.ne.mu) then
	          fac = 1.0d0
	        else
		  fac = 0.5d0
	        endif
		icntdens=icntdens+1
		 do i= 1,nbfpsy(isym)
		 icnt2=noffsrec(isym)+(i-1)*nbfpsy(isym)+mu
		 icnt3=noffsrec(isym)+(i-1)*nbfpsy(isym)+nu
         dens(icntdens)= 
     .        dens(icntdens)+ fac*mo(icnt2)*occ(icntocc+i)*mo(icnt3)
	        enddo
	      enddo
	    enddo
          endif !for nbfpsy.ne.0
      	 enddo
	 return
	 end



c
c This subroutine expands a rectangular matrix back into
c a triangular form 

         subroutine rectri(nsym,nbfpsy,maxvect,maxdens,
     .	 Vmo,noffsrec,noffstri,V)
         implicit none
         integer k,l
	 integer maxvect,maxdens,icnttri,nsym,nbfpsy(8)
	 integer i,noffsrec(8),noffstri(8)
	 double precision Vmo(maxvect),V(maxdens)
         icnttri=0
         i=0
         k=0
	 l=0
	 do i=1,nsym
	        if(nbfpsy(i).ne.0) then 
           do k=1,nbfpsy(i) 
              do l=1,k
	             
         icnttri=icnttri+1
	 
         V(icnttri)=Vmo(nbfpsy(i)*(l-1)+k+
     . 	  noffsrec(i))
              enddo
	   enddo
                endif
	 enddo
	 return
	 end
         


