!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/




C  /* Deck herctl */
      SUBROUTINE HERCTL(DENS,ndens,WORK,LWORK)
C
C     The code for calculation of one- and two-electron integrals
C     was written by T. Helgaker in 1984 at the University of Oslo.
C
C     General contractions were implemented by T. Helgaker at the
C     University of Aarhus in Feb-Mar 1988.
C
C     Symmetry was implemented by P. R. Taylor and T. Helgaker at
C     NASA Ames in Apr 1988.
C
C     The supermatrix code is written by O. Kvalheim at the University
C     of Bergen.
C
C     Spin-orbit integrals by O. Vahtras and T. Helgaker at the
C     University of Oslo, Nov 1989.
C
C     Cartesian and spherical moments integrals by T. Helgaker,
C     University of Oslo, Sep and Oct 1990
C
C     Integrals for indirect spin-spin coupling tensors by T. Helgaker
C     and O. Vahtras at the University of Oslo, Feb 1991.
C
C     Half-derivative overlap integrals for NACMES by T. Helgaker,
C     at University of Aarhus, Jun 1991
C
C     Overlap matrix differentiated with respect to external magetic
C     field, K. Ruud & T. Helgaker, Oct 1991
C
C     Electronic angular momentum around fixed center or nuclei,
C     K. Ruud & T. Helgaker, Oct 1991
C
C     One-electron contribution to the magnetic moment of molecules,
C     K. Ruud & T. Helgaker, Nov. 1991
C
C     Kinetic energy integrals, K. Ruud, Nov. 1991
C
C     Cosine and sine integrals, T. Helgaker, Jun. 1993
C
C     Mass-velocity and Darwin integrals
C     S. Kirpekar & H.J.Aa. Jensen, Jul. 1993
C
C     Magnetic field derivative integrals of dipole length
C     K.Ruud, Aug.-93
C



      IMPLICIT DOUBLE PRECISION (A-H,O-Z)


C
C     -------------------------------------------------------
      PARAMETER (LUCMD = 5, LUPRI = 6)
C
C     -------------------------------------------------------
C     make DUMMY write protected (on most computers)
      PARAMETER ( DUMMY = 1.0D20 , IDUMMY = - 9 999 999 )






      PARAMETER (MXCENT = 130, MXCOOR = 3*MXCENT)



C
      LOGICAL SKIP, TSTINP, HAMILT, DIPLEN, SPNORB, SOTEST, SUPMAT,
     &        DIPVEL, QUADRU, SECMOM, ONEPRP, CARMOM, SPHMOM, OCTUPO,
     &        FERMI, PSO, SPIDIP, DSO, ALLATM, NMRISS, TRIANG, SDFC,
     &        PROPRI, HDO, S1MAG, S2MAG, ANGMOM, ANGLON, LONMOM, MAGMOM,
     &        S1MAGT, MGMOMT, KINENE, S2MAGT, DSUSNL, DSUSLL, DSUSLH,
     &        DIASUS, DSUTST, NUCSNL, NUCSLO, NUCSHI, NSNLTS, NSLTST,
     &        NELFLD, NSTTST, EFGCAR, EFGSPH, NUCPOT, S1MAGL, S1MAGR,
     &        HDOBR, S1MLT, S1MRT, HDOBRT, NPOTST, MGMO2T, HBDO, SUSCGO,
     &        NSTCGO, EXPIKR, DIRAC, DARWIN, MASSVL, CM1, CM2, NOTWO,
     &        SQHDOL, SQHDOR, S1ELE, S1ELB, ONEELD, GFACDI, THETA
      COMMON /CBIHER/ EXPKR(3), THRESH, ISOTOP(MXCENT),
     &                IPRDEF, IORCAR, IORSPH, NPQUAD,
     &                SKIP, TSTINP, HAMILT, DIPLEN, SPNORB,
     &                SOTEST, SUPMAT, DIPVEL, QUADRU, SECMOM,
     &                ONEPRP, CARMOM, SPHMOM, OCTUPO, FERMI, PSO,
     &                SPIDIP, DSO, ALLATM, NPATOM, IPATOM(MXCENT),
     &                NMRISS, TRIANG, SDFC, PROPRI, HDO, S1MAG, S2MAG,
     &                ANGMOM, ANGLON, LONMOM, MAGMOM, S1MAGT, MGMOMT,
     &                KINENE, S2MAGT, DSUSNL, DSUSLL, DSUSLH, DIASUS,
     &                DSUTST, NUCSNL, NUCSLO, NUCSHI, NSNLTS, NSLTST,
     &                NELFLD, NSTTST, EFGCAR, EFGSPH, NUCPOT, S1MAGL,
     &                S1MAGR, HDOBR, S1MLT, S1MRT, HDOBRT, NPOTST,
     &                MGMO2T, PRTHRS, HBDO, SUSCGO, NSTCGO, EXPIKR,
     &                DIRAC, DARWIN, MASSVL, CM1, CM2, NOTWO,
     &                SQHDOL, SQHDOR, S1ELE, S1ELB, ONEELD, GFACDI,
     &                THETA
      LOGICAL  RUNONE, DORLM, ALLRLM, CAVUSR
      COMMON /CBIHR1/ RUNONE, IPRONE, DORLM, LMAX, ALLRLM, CAVUSR
      LOGICAL  RUNTWO, RTNTWO, TKTIME
      COMMON /CBIHR2/ RUNTWO, IPRTWO, IPRNTA, IPRNTB, IPRNTC, IPRNTD,
     &                THRTWO, RTNTWO, TKTIME
      LOGICAL RUNSUP, NOSSUP, OLDSUP
      COMMON /CBIHRS/ RUNSUP, IPRSUP, NOSSUP, OLDSUP, THRSUP
      LOGICAL RUNERI, TIMERI, PMSAB, PMSCD, PMS12, RTNERI, OFFCNT,
     &        DIASRT, OLDCR1, CANIND,WRTSCR,NOWRIT,DOSTRA, EXTPRI,
     &        INTPRI, DODIST, INTSKP, DISTST,WRTINT,FCKINT
      COMMON /CBIERI/ RUNERI, IPRERI, TIMERI, RTNERI, OFFCNT,
     &                PMSAB,  PMSCD, PMS12, IAOBCH, IPRNT1, IPRNT2,
     &                DIASRT, OLDCR1, CANIND,WRTSCR,NOWRIT, DOSTRA,
     &                EXTPRI, IPROD1, IPROD2,INTPRI, DODIST, INTSKP,
     &                LBFINP, MAXDST, DISTST,WRTINT,FCKINT, NDMAT
C
      COMMON /HRUNIT/ LUONEL, LUINTA, LUSUPM, LUDASP, LUSOL
C
      LOGICAL SET
C
      DIMENSION WORK(LWORK)
      integer ndens
      double precision DENS(ndens)
C
C---------------------------------------------------------------------
C
      CALL GETTIM(TIMHER,WALHER)
C
C     Unit assignments
C
      LUONEL = 16
      LUINTA = 11
      LUSUPM = 17
      LUDASP = 24
      LUSOL  = 49
C
C     *************************
C     ***** Input Section *****
C     *************************
C
      TIMSTR = SECOND()
      CALL HERINP(WORK,LWORK,NSYMOP)
      write(6,*) 'HERMIT INPUT READ'
      TIMINP = SECOND() - TIMSTR
      CALL FLSHFO(LUPRI)
C
C     ************************************
C     ***** Calculation of Integrals *****
C     ************************************

c     iprone printoption 
      CALL ONEDRV(DENS,ndens,
     .  WORK,LWORK,IPRONE,.FALSE.,0,.FALSE.,.TRUE.,
     &                  .TRUE.,.FALSE.,NSYMOP)

C
C     ******************************
C     ***** End of Calculation *****
C     ******************************
C
      CALL GETTIM(TEND,WEND)
      TIMHER = TEND - TIMHER
      WALHER = WEND - WALHER
      CALL TIMTXT('>>>> Total CPU  time used in HERMIT:',TIMHER,LUPRI)
      CALL TIMTXT('>>>> Total wall time used in HERMIT:',WALHER,LUPRI)
      RETURN
      END
C  /* Deck herinp */
      SUBROUTINE HERINP(WORK,LWORK,NSYMOP)

C
C <<< General Input for HERMIT >>>
C



      IMPLICIT DOUBLE PRECISION (A-H,O-Z)


C
C     -------------------------------------------------------
      PARAMETER (LUCMD = 5, LUPRI = 6)
C
C     -------------------------------------------------------
C     IRAT  = (real word length) / (integer word length)
C     IRAT2 = (real word length) / (half-integer word length)
C             if available and used, otherwise IRAT2 = IRAT
C     LRAT  = (real word length) / (logical word length)






      PARAMETER (IRAT = 2, IRAT2 = 2, LRAT = 2)









      PARAMETER (MXCENT = 130, MXCOOR = 3*MXCENT)









      PARAMETER (MXSHEL = 200, MXPRIM = 800, MXCORB = 400,


     *           MXORBT = MXCORB*(MXCORB + 1)/2)
      PARAMETER (MXQN=6, MXAQN=MXQN*(MXQN+1)/2)
      PARAMETER (NDIR = 6, NTABLE = 76)
      PARAMETER (D0 = 0.0D0)
      logical SET,NEWDEF
      CHARACTER WORD*7, PROMPT*1, TABDIR(NDIR)*7, TABLE(NTABLE)*7,
     &          WORD1*7
      DIMENSION WORK(LWORK)
C
C
C$Id: herdrv.f,v 1.4 2005/12/13 14:41:51 hlischka Exp $
C
      LOGICAL TESTIN, OPTIMI, RNALL,  RNHERM, RNSIRI, RNABAC, GEOCNV,
     &        HRINPC, SRINPC, ABINPC, RDINPC, RDMLIN, PARCAL, DIRCAL,
     &        WRINDX, WLKREJ, WALKIN, RNRESP, RNINTS, USRIPR, SEGBAS,
     &        DOCCSD, MINIMI, NEWSYM, NEWBAS, NEWPRP
      COMMON /GNRINF/ PANAS,  TESTIN, OPTIMI, RNALL,  RNHERM, RNSIRI, 
     &                RNABAC, GEOCNV, HRINPC, SRINPC, ABINPC, RDINPC, 
     &                RDMLIN, GRADML, PARCAL, DIRCAL, KCHARG, WRINDX, 
     &                WLKREJ, WALKIN, RNRESP, RNINTS, USRIPR, ITERNR, 
     &                ITERMX, IPRUSR, SEGBAS, DOCCSD, MINIMI, NEWSYM, 
     &                LENBAS, NEWBAS, NEWPRP, BASDIR
      CHARACTER*60 BASDIR
      LOGICAL SKIP, TSTINP, HAMILT, DIPLEN, SPNORB, SOTEST, SUPMAT,
     &        DIPVEL, QUADRU, SECMOM, ONEPRP, CARMOM, SPHMOM, OCTUPO,
     &        FERMI, PSO, SPIDIP, DSO, ALLATM, NMRISS, TRIANG, SDFC,
     &        PROPRI, HDO, S1MAG, S2MAG, ANGMOM, ANGLON, LONMOM, MAGMOM,
     &        S1MAGT, MGMOMT, KINENE, S2MAGT, DSUSNL, DSUSLL, DSUSLH,
     &        DIASUS, DSUTST, NUCSNL, NUCSLO, NUCSHI, NSNLTS, NSLTST,
     &        NELFLD, NSTTST, EFGCAR, EFGSPH, NUCPOT, S1MAGL, S1MAGR,
     &        HDOBR, S1MLT, S1MRT, HDOBRT, NPOTST, MGMO2T, HBDO, SUSCGO,
     &        NSTCGO, EXPIKR, DIRAC, DARWIN, MASSVL, CM1, CM2, NOTWO,
     &        SQHDOL, SQHDOR, S1ELE, S1ELB, ONEELD, GFACDI, THETA
      COMMON /CBIHER/ EXPKR(3), THRESH, ISOTOP(MXCENT),
     &                IPRDEF, IORCAR, IORSPH, NPQUAD,
     &                SKIP, TSTINP, HAMILT, DIPLEN, SPNORB,
     &                SOTEST, SUPMAT, DIPVEL, QUADRU, SECMOM,
     &                ONEPRP, CARMOM, SPHMOM, OCTUPO, FERMI, PSO,
     &                SPIDIP, DSO, ALLATM, NPATOM, IPATOM(MXCENT),
     &                NMRISS, TRIANG, SDFC, PROPRI, HDO, S1MAG, S2MAG,
     &                ANGMOM, ANGLON, LONMOM, MAGMOM, S1MAGT, MGMOMT,
     &                KINENE, S2MAGT, DSUSNL, DSUSLL, DSUSLH, DIASUS,
     &                DSUTST, NUCSNL, NUCSLO, NUCSHI, NSNLTS, NSLTST,
     &                NELFLD, NSTTST, EFGCAR, EFGSPH, NUCPOT, S1MAGL,
     &                S1MAGR, HDOBR, S1MLT, S1MRT, HDOBRT, NPOTST,
     &                MGMO2T, PRTHRS, HBDO, SUSCGO, NSTCGO, EXPIKR,
     &                DIRAC, DARWIN, MASSVL, CM1, CM2, NOTWO,
     &                SQHDOL, SQHDOR, S1ELE, S1ELB, ONEELD, GFACDI,
     &                THETA
      LOGICAL  RUNONE, DORLM, ALLRLM, CAVUSR
      COMMON /CBIHR1/ RUNONE, IPRONE, DORLM, LMAX, ALLRLM, CAVUSR
      LOGICAL SOLVNT
      COMMON /CBISOL/ ORICAV(3),RCAV(3),EPDIEL,
     &                LCAVMX,LMTOT,LMNTOT,NCNTCV,SOLVNT
C     ... LCAVMX = max l in moment expansion for solvent cavity
C         LMTOT  = number of spherical components for R(l,m)
C         LMNTOT = number of cartesian components for RC(L,M,N)
      COMMON /ORGCOM/ CMXYZ(3), ORIGIN(3), DIPORG(3), GAGORG(3),
     &                CAVORG(3)
      LOGICAL RELCAL
      COMMON/RELCOM/RELCAL,ITRCNT(9)
      COMMON /HRUNIT/ LUONEL, LUINTA, LUSUPM, LUDASP, LUSOL
C
      DATA TABDIR /'*END OF', '*READIN', '*ONEINT', '*TWOINT',
     &             '*SUPINT', '*ER2INT'/
      DATA TABLE  /'.PRINT ', '.INPTES', '.NOSUP ', '.SPIN-O',
     &             '.DIPLEN', '.NO HAM', '.SOTEST', '.DIPVEL',
     &             '.QUADRU', '.PHASEO', '.SECMOM', '.SUPONL',
     &             '.CARMOM', '.SPHMOM', '.FC    ', '.PSO   ',
     &             '.SD    ', '.DSO   ', '.POINTS', '.SELECT',
     &             '.QUASUM', '.SD+FC ', '.PROPRI', '.HDO   ',
     &             '.S1MAG ', '.S2MAG ', '.ANGMOM', '.ANGLON',
     &             '.LONMOM', '.MAGMOM', '.S1MAGT', '.MGMOMT',
     &             '.KINENE', '.S2MAGT', '.DSUSNL', '.DSUSLL',
     &             '.DSUSLH', '.DIASUS', '.DSUTST', '.NSTNOL',
     &             '.NSTLON', '.NST   ', '.NSNLTS', '.NSLTST',
     &             '.NELFLD', '.NSTTST', '.EFGCAR', '.EFGSPH',
     &             '.S1MAGL', '.S1MAGR', '.HDOBR ', '.S1MLT ',
     &             '.HDOBRT', '.S1MRT ', '.NUCPOT', '.NPOTST',
     &             '.MGMO2T', '.MGMTHR', '.HBDO  ', '.SUSCGO',
     &             '.NSTCGO', '.EXPIKR', '.MASSVE', '.DARWIN',
     &             '.CM-1  ', '.CM-2  ', '.SQHDOL', '.SQHDOR',
     &             '.NOTWO ', '.GAUGEO', '.DIPORG', '.GFACDI',
     $             '.S1ELE ', '.S1ELB ', '.ONEELD', '.THETA '/
C
C     Check if input has been processed earlier.
C
      IPRDEF = IPRUSR + 1
      IF (HRINPC) GOTO 1000
      HRINPC = .TRUE.
C
C     Initialize /CBIHER/
C
      SKIP   = .FALSE.
      TSTINP = .FALSE.
      ONEPRP = .FALSE.
      HAMILT = .TRUE.
      DIPLEN = .FALSE.
      DIPVEL = .FALSE.
      QUADRU = .FALSE.
      SPNORB = .FALSE.
      SOTEST = .FALSE.
      NOTWO  = DIRCAL
      SUPMAT = .NOT. DIRCAL
      SECMOM = .FALSE.
      CARMOM = .FALSE.
      SPHMOM = .FALSE.
      OCTUPO = .FALSE.
      FERMI  = .FALSE.
      PSO    = .FALSE.
      SPIDIP = .FALSE.
      DSO    = .FALSE.
      SDFC   = .FALSE.
      PROPRI = .FALSE.
      HDO    = .FALSE.
      S1MAG  = .FALSE.
      S2MAG  = .FALSE.
      ANGMOM = .FALSE.
      ANGLON = .FALSE.
      LONMOM = .FALSE.
      MAGMOM = .FALSE.
      S1MAGT = .FALSE.
      MGMOMT = .FALSE.
      KINENE = .FALSE.
      S2MAGT = .FALSE.
      DSUSNL = .FALSE.
      DSUSLL = .FALSE.
      DSUSLH = .FALSE.
      DIASUS = .FALSE.
      DSUTST = .FALSE.
      NUCSNL = .FALSE.
      NUCSLO = .FALSE.
      NUCSHI = .FALSE.
      NSTTST = .FALSE.
      NSLTST = .FALSE.
      NELFLD = .FALSE.
      NSNLTS = .FALSE.
      EFGCAR = .FALSE.
      EFGSPH = .FALSE.
      S1MAGL = .FALSE.
      S1MAGR = .FALSE.
      HDOBR  = .FALSE.
      S1MLT  = .FALSE.
      S1MRT  = .FALSE.
      HDOBRT = .FALSE.
      NUCPOT = .FALSE.
      NPOTST = .FALSE.
      MGMO2T = .FALSE.
      HBDO   = .FALSE.
      SUSCGO = .FALSE.
      NSTCGO = .FALSE.
      MASSVL = .FALSE.
      DARWIN = .FALSE.
      CM1    = .FALSE.
      CM2    = .FALSE.
      SQHDOL = .FALSE.
      SQHDOR = .FALSE.
      S1ELE  = .FALSE.
      S1ELB  = .FALSE.
      ONEELD = .FALSE.
      GFACDI = .FALSE.
      THETA  = .FALSE.
      PRTHRS = 1.0D-10
      NPQUAD = 40
      ALLATM = .TRUE.
      TRIANG = .TRUE.
      EXPIKR = .FALSE.
      CALL DZERO(ORIGIN,3)
      CALL DZERO(GAGORG,3)
      CALL DZERO(DIPORG,3)
      CALL DZERO(EXPKR ,3)
C
C     Initialize /CBISOL/ (10-Dec-92 th+hjaaj)
C     -- not used in Hermit; SOLVNT must be false
C        in order to skip solvent modules in ONEDRV
C        and cavity center in VIBMAS
C
      SOLVNT = .FALSE.
C
C     Initialize /RELCOM/
C
      RELCAL = .FALSE.
C
C
      WORD1 = WORD
C
C     ***** Process input for COMMON  /CBIHER/  *****
C
            iprdef = 2
            SUPMAT = .FALSE.
            PROPRI = .FALSE.
            HDO = .FALSE.
            KINENE = .FALSE.
            ONEPRP = .FALSE.
            NELFLD = .FALSE.
            EFGCAR = .FALSE.
            NUCPOT = .false.
            NOTWO = .TRUE.
            SUPMAT = .FALSE.
  180 CONTINUE
       CALL REAINI(IPRDEF,RELCAL,TSTINP)
        word = '*READIN'  
    2   CALL REAINP(WORD,IPRDEF,RELCAL,TSTINP)
C
    1 CONTINUE
C
       set = .true.
       newdef = .false.
       RUNONE = .true.
       IPRONE = .false.
c      IPRONE = IPRDEF
       DORLM  = .FALSE.
       ALLRLM = .FALSE.
       CAVUSR = .FALSE.
 1000 CONTINUE
      CALL READINN(WORK,LWORK,.TRUE.,NSYMOP)
      CALL SETDCH
      RETURN
C
 9000 WRITE (LUPRI,'(//A)') ' *** ERROR *** unable to open input file'
      CALL QUIT('*** ERROR (HERINP) unable to open input file (unit 5)')
      END
C  /* Deck setdch */
      SUBROUTINE SETDCH
C



      IMPLICIT DOUBLE PRECISION (A-H,O-Z)








      PARAMETER (MXCENT = 130, MXCOOR = 3*MXCENT)









      PARAMETER (MXSHEL = 200, MXPRIM = 800, MXCORB = 400,


     *           MXORBT = MXCORB*(MXCORB + 1)/2)
      PARAMETER (MXQN=6, MXAQN=MXQN*(MXQN+1)/2)
C
      COMMON /SYMMET/ FMULT(0:7), PT(0:7), MAXREP, MAXOPR, MULT(0:7),
     &                ISYMAX(3,2), ISYMAO(MXQN,MXAQN), NPARSU(8),
     &                NAOS(8), NPARNU(8,8), IPTSYM(MXCORB,0:7),
     &                IPTCNT(3*MXCENT,0:7,2), NCRREP(0:7,2),
     &                IPTCOR(3*MXCENT,2), NAXREP(0:7,2), IPTAX(3,2),
     &                IPTXYZ(3,0:7,2), IPTNUC(MXCENT,0:7),ISOP(0:7),
     &                NROTS,NINVC,NREFL,IXVAL(0:7,0:7),NCOS(8,2),
     &                ICLASS(MXCORB)
      LOGICAL DOREPS, DOCOOR, DCORD, DCORGD, DOPERT
      COMMON /DORPS/ DOREPS(0:7), DOCOOR(3,MXCENT), NDCORD(2),
     &               DCORD(MXCENT,3,2), DCORGD(0:MXCENT,3,2),
     &               DOPERT(0:3*MXCENT,2)
C
      DO 100 IREP = 0, MAXREP
         DOREPS(IREP) = .TRUE.
  100 CONTINUE
      RETURN
      END


      SUBROUTINE ONEDRV(DENS,NDENS,
     .      WORK,LWORK,IPRINT,PROPTY,MAXDIF,DIFINT,NODC,
     &                  NODV,DIFDIP,NSYMOP)
c     CALL ONEDRV(DENS,WORK,LWORK,IPRONE,.FALSE.,0,.FALSE.,.TRUE.,
c    &                  .TRUE.,.FALSE.)


C



      IMPLICIT DOUBLE PRECISION (A-H,O-Z)


C
C     -------------------------------------------------------
      PARAMETER (LUCMD = 5, LUPRI = 6)






      PARAMETER (MXCENT = 130, MXCOOR = 3*MXCENT)



      PARAMETER (MXQN=6, MXAQN=MXQN*(MXQN+1)/2)






      PARAMETER (MXSHEL = 200, MXPRIM = 800, MXCORB = 400,


     *           MXORBT = MXCORB*(MXCORB + 1)/2)
C
C     -------------------------------------------------------
C     IRAT  = (real word length) / (integer word length)
C     IRAT2 = (real word length) / (half-integer word length)
C             if available and used, otherwise IRAT2 = IRAT
C     LRAT  = (real word length) / (logical word length)






      PARAMETER (IRAT = 2, IRAT2 = 2, LRAT = 2)



C
      LOGICAL DIFINT, NODC, NODV, DIFDIP, PROPTY, DOINT(2,2)
      DIMENSION WORK(LWORK)
      CHARACTER*4 NAMN
      LOGICAL NOORBT,GAUNUC
      COMMON /NUCLEI/ CHARGE(MXCENT), CORD(3,MXCENT), NOORBT(MXCENT),
     &                NUCPRE(MXCENT), NUCNUM(MXCENT,8), NUCDEG(MXCENT),
     &                ISTBNU(MXCENT), GNUEXP(MXCENT), NAMN(MXCENT),
     &                NUCIND, NUCDEP, NTRACO, ITRACO(3), NATOMS, NFLOAT,
     &                NBASIS, NLARGE, NSMALL, NPBAS, NPLRG, NPSML,
     &                NUCNET, INCENT(MXCENT),INUNIQ(MXCENT),GAUNUC
      CHARACTER NAMEX*6, NAMDEP*6, NAMDPX*6
      COMMON /NUCLEC/ NAMEX(MXCOOR), NAMDEP(MXCENT), NAMDPX(MXCOOR)
      LOGICAL SHARE, SEGM, SPHR
      COMMON /SHELLS/ CENT(MXSHEL,3,2), NHKT(MXSHEL),   KHKT(MXSHEL),
     &                KCKT(MXSHEL),     ISTBAO(MXSHEL), NUCO(MXSHEL),
     &                JSTRT(MXSHEL),    NSTRT(MXSHEL),  MST(MXSHEL),
     &                NCENT(MXSHEL),    SHARE(MXSHEL),  NRCO(MXSHEL),
     &                NUMCF(MXSHEL),    NBCH(MXSHEL),   KSTRT(MXSHEL),
     &                SEGM(MXSHEL),     LCLASS(MXSHEL), NOTWOC(MXSHEL),
     &                IPTSHL(MXSHEL),   NUMCFT(MXSHEL), SPHR(MXSHEL),
     &                KMAX, NLRGSH, NSMLSH, NORBS
      COMMON /SYMMET/ FMULT(0:7), PT(0:7), MAXREP, MAXOPR, MULT(0:7),
     &                ISYMAX(3,2), ISYMAO(MXQN,MXAQN), NPARSU(8),
     &                NAOS(8), NPARNU(8,8), IPTSYM(MXCORB,0:7),
     &                IPTCNT(3*MXCENT,0:7,2), NCRREP(0:7,2),
     &                IPTCOR(3*MXCENT,2), NAXREP(0:7,2), IPTAX(3,2),
     &                IPTXYZ(3,0:7,2), IPTNUC(MXCENT,0:7),ISOP(0:7),
     &                NROTS,NINVC,NREFL,IXVAL(0:7,0:7),NCOS(8,2),
     &                ICLASS(MXCORB)
      LOGICAL SOLVNT
      COMMON /CBISOL/ ORICAV(3),RCAV(3),EPDIEL,
     &                LCAVMX,LMTOT,LMNTOT,NCNTCV,SOLVNT
C     ... LCAVMX = max l in moment expansion for solvent cavity
C         LMTOT  = number of spherical components for R(l,m)
C         LMNTOT = number of cartesian components for RC(L,M,N)
C
      integer ibtand,ibtor,ibtshl,ibtshr,ibtxor
       integer ndens
      double precision DENS(NDENS)


      IBTAND(I,J) = IAND(I,J)
      IBTOR(I,J)  = IOR(I,J)
      IBTSHL(I,J) = ISHFT(I,J)
      IBTSHR(I,J) = ISHFT(I,-J)
      IBTXOR(I,J) = IEOR(I,J)












C
      DOINT(1,1) = .TRUE.
      DOINT(2,1) = .TRUE.
      DOINT(1,2) = .TRUE.
      DOINT(2,2) = .TRUE.
      
C
C     ***** Number of basis functions *****
C
      NBAST  = 0
      NNBAST = 0
      DO 100 KB = 0, MAXREP
         NBASI = 0
         DO 200 ISHELL = 1, KMAX
            IF (IBTAND(KB,ISTBAO(ISHELL)).EQ.0) THEN
               NBASI = NBASI + KHKT(ISHELL)
            END IF
  200    CONTINUE
         NBAST  = NBAST  + NBASI
         NNBAST = NNBAST + NBASI*(NBASI + 1)/2
  100 CONTINUE
      NNBASX = NBAST*(NBAST + 1)/2
C
      if (NNBASX.ne.ndens) 
     .  call bummer('ONEDRV:inconsistent density dimension',nnbasx,2)
      KSTHMA = 1
      KDENMA = KSTHMA 
      KFACIN = KDENMA 
      KCOORC = KFACIN +   NUCDEP
      KSIGNC = KCOORC + 3*NUCDEP
      KNCENT = KSIGNC + 3*NUCDEP
      KJSYMC = KNCENT +  (NUCDEP + 1)/IRAT
      KJCENT = KJSYMC +  (NUCDEP + 1)/IRAT
      KTLMD  = KJCENT +  (NUCDEP + 1)/IRAT
      IF (SOLVNT .AND. MAXDIF .GE. 2) THEN
         KLAST  = KTLMD + LMNTOT*6*NUCDEP
      ELSE
         KLAST  = KTLMD
      END IF
      LWRK   = LWORK  - KLAST + 1
      IF (KLAST .GT. LWORK) CALL STOPIT('ONEDRV',' ',KLAST,LWORK)
      CALL ONEDR1(DENS,WORK(KFACIN),
     &            WORK(KCOORC),WORK(KSIGNC),WORK(KNCENT),WORK(KJSYMC),
     &            WORK(KJCENT),WORK(KTLMD),WORK(KLAST),LWRK,
     &            IPRINT,PROPTY,MAXDIF,
     &            NODC,NODV,DIFDIP,NBAST,NNBASX,NNBAST,DOINT,.TRUE.,
     .            NSYMOP)
      RETURN
      END
C  /* Deck onedr1 */
      SUBROUTINE ONEDR1(DENS,FACINT,COORC,SIGNC,NCENTC,
     &                  JSYMC,JCENTC,TLMD,WORK,LWORK,IPRINT,PROPTY,
     &                  MAXDIF,NODC,NODV,DIFDIP,NBAST,NNBASX,NNBAST,
     &                  DOINT,TOFILE,NSYMOP)
C
C     TUH
C
C     This program calculates overlap and one-electron Hamiltonian
C     matrix elements and the first and second derivatives of these
C     elements using the McMurchie/Davidson scheme.  See L. E. McMurchie
C     & E. R. Davidson, J. Comp. Phys. 26 (1978) 218, and also V. R.
C     Saunders in "Methods in Computational Molecular Physics", Reidel
C     1983.
C
C     Symmetry included  880406  TUH & PRT
C



      IMPLICIT DOUBLE PRECISION (A-H,O-Z)








      PARAMETER (MXCENT = 130, MXCOOR = 3*MXCENT)



      PARAMETER (MXQN=6, MXAQN=MXQN*(MXQN+1)/2)






      PARAMETER (MXSHEL = 200, MXPRIM = 800, MXCORB = 400,


     *           MXORBT = MXCORB*(MXCORB + 1)/2)
C
C     -------------------------------------------------------
C     IRAT  = (real word length) / (integer word length)
C     IRAT2 = (real word length) / (half-integer word length)
C             if available and used, otherwise IRAT2 = IRAT
C     LRAT  = (real word length) / (logical word length)






      PARAMETER (IRAT = 2, IRAT2 = 2, LRAT = 2)



C
C     -------------------------------------------------------
      PARAMETER (LUCMD = 5, LUPRI = 6)
      PARAMETER (LUTEMP = 48)
      PARAMETER (D0 = 0.00 D00)
C
      LOGICAL NODC, NODV, FRSDER, SECDER, DIFDIP, PROPTY, DOINT(2,2),
     &        TOFILE
      DIMENSION DENS(NNBASX),  
     &          FACINT(NUCDEP), WORK(LWORK), COORC(3,NUCDEP),
     &          SIGNC(3,NUCDEP), NCENTC(NUCDEP), JSYMC(NUCDEP),
     &          JCENTC(NUCDEP), TLMD(3*NUCDEP*LMNTOT,2)
      LOGICAL         LDIAG, ONECEN, SPHRA, SPHRB, SPHRAB
      COMMON /ONECOM/ CORAX,CORAY,CORAZ,CORBX,CORBY,CORBZ,
     &                SIGNBX,SIGNBY,SIGNBZ,HKAB,
     &                NHKTA,NHKTB,KHKTA,KHKTB,KHKTAB,
     &                KCKTA,KCKTB,KCKTAB,IDENA,IDENB,
     &                NCENTA,NCENTB,ICENTA,ICENTB,ONECEN,NUCA,NUCB,
     &                JSTA,JSTB,MULA,MULB,MAB,LDIAG,JMAX,
     &                ISTEPU,ISTEPV,NAHGTF,NUMCFA,NUMCFB,
     &                SPHRAB,SPHRA,SPHRB
      COMMON /LMNS/ LVALUA(MXAQN), MVALUA(MXAQN), NVALUA(MXAQN),
     *              LVALUB(MXAQN), MVALUB(MXAQN), NVALUB(MXAQN)
      COMMON /ENERGY/ ENERKE, GRADKE(MXCOOR), HESSKE(MXCOOR,MXCOOR),
     &                ENERNA, GRADNA(MXCOOR), HESSNA(MXCOOR,MXCOOR),
     &                ENEREE, GRADEE(MXCOOR), HESSEE(MXCOOR,MXCOOR),
     &                ENERNN, GRADNN(MXCOOR), HESSNN(MXCOOR,MXCOOR),
     &                        GRADFS(MXCOOR), HESFS1(MXCOOR,MXCOOR),
     &                                        HESFS2(MXCOOR,MXCOOR),
     &                                        HESREL(MXCOOR,MXCOOR),
     &                                        HESRL2(MXCOOR,MXCOOR),
     &                                        HESRL3(MXCOOR,MXCOOR)
      COMMON /TAYSOL/ ESOLTT, GSOLTT(MXCOOR), GSOLNN(MXCOOR),
     &                HSOLT2(MXCOOR,MXCOOR), ESOLNN,
     &                HSOLNN(MXCOOR,MXCOOR), HSOLTT(MXCOOR,MXCOOR)
      LOGICAL DOCART,SPH
      COMMON /CCOM/ THRS,NHTYP,DOCART,IUTYP,
     &              KHK(MXQN),KCK(MXQN),SPH(MXQN),NHKOFF(MXQN)
      CHARACTER*4 GTOTYP
      COMMON /CCOMC/ GTOTYP(MXQN*(MXQN+1)*(MXQN+2)/6)
      CHARACTER*4 NAMN
      LOGICAL NOORBT,GAUNUC
      COMMON /NUCLEI/ CHARGE(MXCENT), CORD(3,MXCENT), NOORBT(MXCENT),
     &                NUCPRE(MXCENT), NUCNUM(MXCENT,8), NUCDEG(MXCENT),
     &                ISTBNU(MXCENT), GNUEXP(MXCENT), NAMN(MXCENT),
     &                NUCIND, NUCDEP, NTRACO, ITRACO(3), NATOMS, NFLOAT,
     &                NBASIS, NLARGE, NSMALL, NPBAS, NPLRG, NPSML,
     &                NUCNET, INCENT(MXCENT),INUNIQ(MXCENT),GAUNUC
      CHARACTER NAMEX*6, NAMDEP*6, NAMDPX*6
      COMMON /NUCLEC/ NAMEX(MXCOOR), NAMDEP(MXCENT), NAMDPX(MXCOOR)
      LOGICAL SHARE, SEGM, SPHR
      COMMON /SHELLS/ CENT(MXSHEL,3,2), NHKT(MXSHEL),   KHKT(MXSHEL),
     &                KCKT(MXSHEL),     ISTBAO(MXSHEL), NUCO(MXSHEL),
     &                JSTRT(MXSHEL),    NSTRT(MXSHEL),  MST(MXSHEL),
     &                NCENT(MXSHEL),    SHARE(MXSHEL),  NRCO(MXSHEL),
     &                NUMCF(MXSHEL),    NBCH(MXSHEL),   KSTRT(MXSHEL),
     &                SEGM(MXSHEL),     LCLASS(MXSHEL), NOTWOC(MXSHEL),
     &                IPTSHL(MXSHEL),   NUMCFT(MXSHEL), SPHR(MXSHEL),
     &                KMAX, NLRGSH, NSMLSH, NORBS
      COMMON /SYMMET/ FMULT(0:7), PT(0:7), MAXREP, MAXOPR, MULT(0:7),
     &                ISYMAX(3,2), ISYMAO(MXQN,MXAQN), NPARSU(8),
     &                NAOS(8), NPARNU(8,8), IPTSYM(MXCORB,0:7),
     &                IPTCNT(3*MXCENT,0:7,2), NCRREP(0:7,2),
     &                IPTCOR(3*MXCENT,2), NAXREP(0:7,2), IPTAX(3,2),
     &                IPTXYZ(3,0:7,2), IPTNUC(MXCENT,0:7),ISOP(0:7),
     &                NROTS,NINVC,NREFL,IXVAL(0:7,0:7),NCOS(8,2),
     &                ICLASS(MXCORB)
      COMMON /SYMIND/ INDFA(8,MXAQN),INDFB(8,MXAQN),ISOFRA(8),ISOFRB(8)
      COMMON /CSYM1/ BUF(600), IBUF(600), LENGTH, INDMAX
      LOGICAL SOLVNT
      COMMON /CBISOL/ ORICAV(3),RCAV(3),EPDIEL,
     &                LCAVMX,LMTOT,LMNTOT,NCNTCV,SOLVNT
C     ... LCAVMX = max l in moment expansion for solvent cavity
C         LMTOT  = number of spherical components for R(l,m)
C         LMNTOT = number of cartesian components for RC(L,M,N)
C
       double precision, pointer :: point(:,:)
       double precision, pointer :: phi(:)

       common /potentialptr/point,phi,npp
       integer ndeg,nucnumch(8)
       double precision nucpotential,result(2)

      integer ibtand,ibtor,ibtshl,ibtshr,ibtxor


      IBTAND(I,J) = IAND(I,J)
      IBTOR(I,J)  = IOR(I,J)
      IBTSHL(I,J) = ISHFT(I,J)
      IBTSHR(I,J) = ISHFT(I,-J)
      IBTXOR(I,J) = IEOR(I,J)












c      IF (IPRINT .GE. 5) CALL TITLER('Output from ONEDR1','*',103)
C
      TIMHER = D0
      TIMINT = D0
      FRSDER = MAXDIF .GE. 1
      SECDER = MAXDIF .EQ. 2
      TOLS   = THRS*THRS
      TOLOG  = - LOG(TOLS)
C
C     **************************************************************
C     ***** Set up total density and Fock matrices in AO basis *****
C     **************************************************************
C
        LWRK  = LWORK 
C
C
C     ************************************************************
C     ***** Triangular loop over symmetry independent shells *****
C     ************************************************************


       do ipp=1,npp
CTM     make sure to have filled in DENMAT
        call nucpro_pot(point(1,ipp),nucpotential,ndeg,nucnumch,
     .   nsymop)

        result=0.0d0

      
      CALL IZ000(ISOFRA, 8)
      IDENA = 0
      DO 100 ISHELA = 1,KMAX
         NHKTA = NHKT(ISHELA)
         KHKTA = KHKT(ISHELA)
         KCKTA = KCKT(ISHELA)
         ICA   = LCLASS(ISHELA)
         SPHRA = SPHR(ISHELA)
	 CALL LMNVAL(NHKTA,KCKTA,LVALUA,MVALUA,NVALUA)
	 NCENTA = NCENT(ISHELA)
         ICENTA = NUCNUM(NCENTA,1)
         MULA   = ISTBAO(ISHELA)
         MULTA  = MULT(MULA)
         NUCA   = NUCO(ISHELA)
         NUMCFA = NUMCF(ISHELA)
         JSTA   = JSTRT(ISHELA)
         CORAX  = CENT(ISHELA,1,1)
         CORAY  = CENT(ISHELA,2,1)
         CORAZ  = CENT(ISHELA,3,1)
         IDENB0 = 0
C
C        Compute symmetry integral pointers for contributions
C        from this block.  Note that at present this assumes all
C        components from a shell are included.
C
         DO 600 I = 1, 8
            ISOFRB(I) = 0
            DO 610 J = 1, MXAQN
              INDFA(I,J) = -10 000 000
610         CONTINUE
600      CONTINUE
         DO 620 NA = 1, KHKTA
            DO 630 IREP = 0, MAXREP
            IF (IBTAND(MULA,IBTXOR(IREP,ISYMAO(NHKTA,NA))).EQ.0) THEN
               ISOFRA(IREP+1)    = ISOFRA(IREP+1) + 1
               INDFA (IREP+1,NA) = ISOFRA(IREP+1)
            END IF
630         CONTINUE
620      CONTINUE
         IF (IPRINT .GT. 20) THEN
c            WRITE(LUPRI,'(A,I4)')' IA address offsets for shell ',I
c    + SHELA

            DO 640 NA = 1,KHKTA
c               WRITE(LUPRI,'(8(1X,I5))') (INDFA(I,NA), I = 1,MAXREP+1)
640         CONTINUE
         END IF
      DO 110 ISHELB = 1,ISHELA
         LDIAG = ISHELA .EQ. ISHELB
         NHKTB = NHKT(ISHELB)
         KHKTB = KHKT(ISHELB)
         KCKTB = KCKT(ISHELB)
         ICB   = LCLASS(ISHELB)
         SPHRB = SPHR(ISHELB)
	 CALL LMNVAL(NHKTB,KCKTB,LVALUB,MVALUB,NVALUB)
	 NCENTB = NCENT(ISHELB)
         NHKTAB = NHKTA + NHKTB
         MULB   = ISTBAO(ISHELB)
         MULTB  = MULT(MULB)
         NUCB   = NUCO(ISHELB)
         NUMCFB = NUMCF(ISHELB)
         JSTB   = JSTRT(ISHELB)
         CORBX0 = CENT(ISHELB,1,1)
         CORBY0 = CENT(ISHELB,2,1)
         CORBZ0 = CENT(ISHELB,3,1)
         KHKTAB = KHKTA*KHKTB
         KCKTAB = KCKTA*KCKTB
         MAB    = IBTOR(MULA,MULB)
         KAB    = IBTAND(MULA,MULB)
         HKAB   = FMULT(KAB)
C
         SPHRAB = SPHRA .OR. SPHRB
C
C        Compute symmetry integral pointers for contributions
C        from this block.  Note that at present this assumes all
C        components from a shell are included
C
         DO 700 I = 1, 8
            DO 710 J = 1, MXAQN
              INDFB(I,J) = -10 000 000
710         CONTINUE
700      CONTINUE
         DO 720 NB = 1, KHKTB
            DO 730 IREP = 0, MAXREP
            IF (IBTAND(MULB,IBTXOR(IREP,ISYMAO(NHKTB,NB))).EQ.0) THEN
               ISOFRB(IREP+1)    = ISOFRB(IREP+1) + 1
               INDFB (IREP+1,NB) = ISOFRB(IREP+1)
            END IF
730         CONTINUE
720      CONTINUE
         IF (IPRINT .GT. 20) THEN
c            WRITE(LUPRI,'(A,I4)')' IB address offsets for shell ',I
c    + SHELB

            DO 740 NB = 1, KHKTB
c               WRITE(LUPRI,'(8(1X,I5))') (INDFB(I,NB), I = 1,MAXREP+1)
740         CONTINUE
         ENDIF
         IF(.NOT.DOINT(ICA,ICB)) GOTO 110
c         IF (IPRINT .GE. 05) WRITE (LUPRI, 1000) ISHELA, ISHELB
c         IF (IPRINT .GE. 10) THEN
c             WRITE (LUPRI,'(A,2I10)') ' NHKT   ', NHKTA, NHKTB
c             WRITE (LUPRI,'(A,2I10)') ' KHKT   ', KHKTA, KHKTB
c             WRITE (LUPRI,'(A,2I10)') ' KCKT   ', KCKTA, KCKTB
c             WRITE (LUPRI,'(A,2I10)') ' NCENT  ', NCENTA, NCENTB
c             WRITE (LUPRI,'(A,2I10)') ' ISTBAO ', MULA, MULB
c             WRITE (LUPRI,'(A,2I10)') ' MULT   ', MULTA, MULTB
c             WRITE (LUPRI,'(A,2I10)') ' NUC    ', NUCA, NUCB
c             WRITE (LUPRI,'(A,2I10)') ' NUMCF  ', NUMCFA, NUMCFB
c             WRITE (LUPRI,'(A,2I10)') ' JST    ', JSTA, JSTB
c             WRITE (LUPRI,'(A,2F12.6)') ' CORAX    ', CORAX, CORBX0
c             WRITE (LUPRI,'(A,2F12.6)') ' CORAY    ', CORAY, CORBY0
c             WRITE (LUPRI,'(A,2F12.6)') ' CORAZ    ', CORAZ, CORBZ0
c         END IF
C
C        Initialization for nuclear attraction integrals
C
         JMAX = NHKTAB - 2
         IF (PROPTY) JMAX = JMAX + MAXDIF
         ISTEPU = JMAX + 1
         ISTEPV = ISTEPU*ISTEPU
         NAHGTF = ISTEPU*ISTEPV
         NATOMC = 0
CTM  : since all nucind atoms now have a charge of null
CTM  : we consider only the extra one with charge=1

CTM      DO 120 IATOMC = 1,NUCIND
CTM         MULC   = ISTBNU(IATOMC)
            MULC   = NDEGCHARGE
            MABC   = IBTOR(MULC,KAB)
CTM         CORCX0 = CORD(1,IATOMC)
CTM         CORCY0 = CORD(2,IATOMC)
CTM         CORCZ0 = CORD(3,IATOMC)
            CORCX0 = point(1,ipp)
            CORCY0 = point(2,ipp)
            CORCZ0 = point(3,ipp)
            FACTOR = - FMULT(IBTAND(MULC,KAB))/HKAB
CTM         DO 130 ISYMOP = 0, MAXOPR
            DO 130 ISYMOP = 0, MAXREP
c              write(6,*) 'maxrep,isymop',maxrep,isymop,
c    .      'and=',ibtand(isymop,mabc),mabc
               IF (IBTAND(ISYMOP,MABC) .EQ. 0) THEN
                  NATOMC = NATOMC + 1
                  JSYMC(NATOMC)   = ISYMOP
                  JCENTC(NATOMC)  = IATOMC
                  SIGNC(1,NATOMC) = PT(IBTAND(ISYMAX(1,1),ISYMOP))
                  SIGNC(2,NATOMC) = PT(IBTAND(ISYMAX(2,1),ISYMOP))
                  SIGNC(3,NATOMC) = PT(IBTAND(ISYMAX(3,1),ISYMOP))
                  COORC(1,NATOMC) = SIGNC(1,NATOMC)*CORCX0
                  COORC(2,NATOMC) = SIGNC(2,NATOMC)*CORCY0
                  COORC(3,NATOMC) = SIGNC(3,NATOMC)*CORCZ0
                  FACINT(NATOMC)  = FACTOR
CTM               NCENTC(NATOMC)  = NUCNUM(IATOMC,ISYMOP+1)
                  NCENTC(NATOMC)  = NUCNUMCH(ISYMOP+1)
               END IF
c                 write(6,995) ishela,ishelb,
c    . natomc,jsymc(natomc),jcentc(natomc),
c    .  signc(1,natomc),signc(2,natomc),signc(3,natomc),
c    .  coorc(1,natomc),coorc(2,natomc),coorc(3,natomc),
c    .  facint(natomc),ncentc(natomc)
995   format('loop: shelab:',2i4,'natomc,jsymc,jcentc=',
     .  3i4,'sign',3f8.4,'coor',3f8.4,'fac',f8.4,
     .  'ncentc=',i8)
  130       CONTINUE
CTM  120    CONTINUE
C
	 CALL ONESOP(DENS,RESULT,FACINT,COORC,
CTM  &               WORK(KLAST),LWRK,
     &               WORK, LWRK,
     &               IPRINT,PROPTY,MAXDIF,IDENB0,CORBX0,CORBY0,CORBZ0,
     &               DIFDIP,SECDER,NATOMC,TOLOG,TOLS,JSYMC,JCENTC,
     &               NCENTC,SIGNC,NNBASX,WORK(KFCM),TLMD(1,1))
c      write(6,990) point(1,ipp),point(2,ipp),point(3,ipp),
c    .       result(1),result(2)/dble(ndegcharge)
  110    IDENB0 = IDENB0 + KHKTB*MULTB
         IDENA = IDENA + KHKTA*MULTA
  100 CONTINUE
C
C     ***** End loop over symmetry independent orbitals *****
       write(6,*) 'Epot, ndeg=',result(2),ndeg 
       result(2)=result(2)/(maxrep+1)
       write(6,990) point(1,ipp),point(2,ipp),point(3,ipp),
     .       result(1),result(2),nucpotential, nucpotential+result(2)
 990   format('POTENTIAL AT',3f10.6,'S=',f10.7,'PHI (E,N,tot)=',3f12.7)
       phi(ipp)=result(2)+nucpotential
       enddo

C
      RETURN
 1000 FORMAT (//,2X,'***************************************',
     *         /,2X,'********** ISHELA/B =',I3,',',I3,' **********',
     *         /,2X,'***************************************',/)
      END


      SUBROUTINE ONEDR1CH(VNE,FACINT,COORC,SIGNC,NCENTC,
     &                  JSYMC,JCENTC,TLMD,WORK,LWORK,IPRINT,PROPTY,
     &                  MAXDIF,NODC,NODV,DIFDIP,NBAST,NNBASX,NNBAST,
     &                  DOINT,TOFILE,NSYMOP)
C
C     TUH
C
C     This program calculates overlap and one-electron Hamiltonian
C     matrix elements and the first and second derivatives of these
C     elements using the McMurchie/Davidson scheme.  See L. E. McMurchie
C     & E. R. Davidson, J. Comp. Phys. 26 (1978) 218, and also V. R.
C     Saunders in "Methods in Computational Molecular Physics", Reidel
C     1983.
C
C     Symmetry included  880406  TUH & PRT
C



      IMPLICIT DOUBLE PRECISION (A-H,O-Z)








      PARAMETER (MXCENT = 130, MXCOOR = 3*MXCENT)



      PARAMETER (MXQN=6, MXAQN=MXQN*(MXQN+1)/2)






      PARAMETER (MXSHEL = 200, MXPRIM = 800, MXCORB = 400,


     *           MXORBT = MXCORB*(MXCORB + 1)/2)
C
C     -------------------------------------------------------
C     IRAT  = (real word length) / (integer word length)
C     IRAT2 = (real word length) / (half-integer word length)
C             if available and used, otherwise IRAT2 = IRAT
C     LRAT  = (real word length) / (logical word length)






      PARAMETER (IRAT = 2, IRAT2 = 2, LRAT = 2)



C
C     -------------------------------------------------------
      PARAMETER (LUCMD = 5, LUPRI = 6)
      PARAMETER (LUTEMP = 48)
      PARAMETER (D0 = 0.00 D00)
C
      LOGICAL NODC, NODV, FRSDER, SECDER, DIFDIP, PROPTY, DOINT(2,2),
     &        TOFILE
      DIMENSION VNE(NNBASX),  
     &          FACINT(NUCDEP), WORK(LWORK), COORC(3,NUCDEP),
     &          SIGNC(3,NUCDEP), NCENTC(NUCDEP), JSYMC(NUCDEP),
     &          JCENTC(NUCDEP), TLMD(3*NUCDEP*LMNTOT,2)
      LOGICAL         LDIAG, ONECEN, SPHRA, SPHRB, SPHRAB
      COMMON /ONECOM/ CORAX,CORAY,CORAZ,CORBX,CORBY,CORBZ,
     &                SIGNBX,SIGNBY,SIGNBZ,HKAB,
     &                NHKTA,NHKTB,KHKTA,KHKTB,KHKTAB,
     &                KCKTA,KCKTB,KCKTAB,IDENA,IDENB,
     &                NCENTA,NCENTB,ICENTA,ICENTB,ONECEN,NUCA,NUCB,
     &                JSTA,JSTB,MULA,MULB,MAB,LDIAG,JMAX,
     &                ISTEPU,ISTEPV,NAHGTF,NUMCFA,NUMCFB,
     &                SPHRAB,SPHRA,SPHRB
      COMMON /LMNS/ LVALUA(MXAQN), MVALUA(MXAQN), NVALUA(MXAQN),
     *              LVALUB(MXAQN), MVALUB(MXAQN), NVALUB(MXAQN)
      COMMON /ENERGY/ ENERKE, GRADKE(MXCOOR), HESSKE(MXCOOR,MXCOOR),
     &                ENERNA, GRADNA(MXCOOR), HESSNA(MXCOOR,MXCOOR),
     &                ENEREE, GRADEE(MXCOOR), HESSEE(MXCOOR,MXCOOR),
     &                ENERNN, GRADNN(MXCOOR), HESSNN(MXCOOR,MXCOOR),
     &                        GRADFS(MXCOOR), HESFS1(MXCOOR,MXCOOR),
     &                                        HESFS2(MXCOOR,MXCOOR),
     &                                        HESREL(MXCOOR,MXCOOR),
     &                                        HESRL2(MXCOOR,MXCOOR),
     &                                        HESRL3(MXCOOR,MXCOOR)
      COMMON /TAYSOL/ ESOLTT, GSOLTT(MXCOOR), GSOLNN(MXCOOR),
     &                HSOLT2(MXCOOR,MXCOOR), ESOLNN,
     &                HSOLNN(MXCOOR,MXCOOR), HSOLTT(MXCOOR,MXCOOR)
      LOGICAL DOCART,SPH
      COMMON /CCOM/ THRS,NHTYP,DOCART,IUTYP,
     &              KHK(MXQN),KCK(MXQN),SPH(MXQN),NHKOFF(MXQN)
      CHARACTER*4 GTOTYP
      COMMON /CCOMC/ GTOTYP(MXQN*(MXQN+1)*(MXQN+2)/6)
      CHARACTER*4 NAMN
      LOGICAL NOORBT,GAUNUC
      COMMON /NUCLEI/ CHARGE(MXCENT), CORD(3,MXCENT), NOORBT(MXCENT),
     &                NUCPRE(MXCENT), NUCNUM(MXCENT,8), NUCDEG(MXCENT),
     &                ISTBNU(MXCENT), GNUEXP(MXCENT), NAMN(MXCENT),
     &                NUCIND, NUCDEP, NTRACO, ITRACO(3), NATOMS, NFLOAT,
     &                NBASIS, NLARGE, NSMALL, NPBAS, NPLRG, NPSML,
     &                NUCNET, INCENT(MXCENT),INUNIQ(MXCENT),GAUNUC
      CHARACTER NAMEX*6, NAMDEP*6, NAMDPX*6
      COMMON /NUCLEC/ NAMEX(MXCOOR), NAMDEP(MXCENT), NAMDPX(MXCOOR)
      LOGICAL SHARE, SEGM, SPHR
      COMMON /SHELLS/ CENT(MXSHEL,3,2), NHKT(MXSHEL),   KHKT(MXSHEL),
     &                KCKT(MXSHEL),     ISTBAO(MXSHEL), NUCO(MXSHEL),
     &                JSTRT(MXSHEL),    NSTRT(MXSHEL),  MST(MXSHEL),
     &                NCENT(MXSHEL),    SHARE(MXSHEL),  NRCO(MXSHEL),
     &                NUMCF(MXSHEL),    NBCH(MXSHEL),   KSTRT(MXSHEL),
     &                SEGM(MXSHEL),     LCLASS(MXSHEL), NOTWOC(MXSHEL),
     &                IPTSHL(MXSHEL),   NUMCFT(MXSHEL), SPHR(MXSHEL),
     &                KMAX, NLRGSH, NSMLSH, NORBS
      COMMON /SYMMET/ FMULT(0:7), PT(0:7), MAXREP, MAXOPR, MULT(0:7),
     &                ISYMAX(3,2), ISYMAO(MXQN,MXAQN), NPARSU(8),
     &                NAOS(8), NPARNU(8,8), IPTSYM(MXCORB,0:7),
     &                IPTCNT(3*MXCENT,0:7,2), NCRREP(0:7,2),
     &                IPTCOR(3*MXCENT,2), NAXREP(0:7,2), IPTAX(3,2),
     &                IPTXYZ(3,0:7,2), IPTNUC(MXCENT,0:7),ISOP(0:7),
     &                NROTS,NINVC,NREFL,IXVAL(0:7,0:7),NCOS(8,2),
     &                ICLASS(MXCORB)
      COMMON /SYMIND/ INDFA(8,MXAQN),INDFB(8,MXAQN),ISOFRA(8),ISOFRB(8)
      COMMON /CSYM1/ BUF(600), IBUF(600), LENGTH, INDMAX
      LOGICAL SOLVNT
      COMMON /CBISOL/ ORICAV(3),RCAV(3),EPDIEL,
     &                LCAVMX,LMTOT,LMNTOT,NCNTCV,SOLVNT
C     ... LCAVMX = max l in moment expansion for solvent cavity
C         LMTOT  = number of spherical components for R(l,m)
C         LMNTOT = number of cartesian components for RC(L,M,N)
C
       double precision, pointer :: point(:,:)
       double precision, pointer :: chg(:)

       common /potentialptr/point,chg,npp
       integer ndeg,nucnumch(8)
       double precision nucpotential,result(2)

      integer ibtand,ibtor,ibtshl,ibtshr,ibtxor


      IBTAND(I,J) = IAND(I,J)
      IBTOR(I,J)  = IOR(I,J)
      IBTSHL(I,J) = ISHFT(I,J)
      IBTSHR(I,J) = ISHFT(I,-J)
      IBTXOR(I,J) = IEOR(I,J)












c      IF (IPRINT .GE. 5) CALL TITLER('Output from ONEDR1','*',103)
C
      TIMHER = D0
      TIMINT = D0
      FRSDER = MAXDIF .GE. 1
      SECDER = MAXDIF .EQ. 2
      TOLS   = THRS*THRS
      TOLOG  = - LOG(TOLS)
C
C     **************************************************************
C     ***** Set up total density and Fock matrices in AO basis *****
C     **************************************************************
C
        LWRK  = LWORK 
C
C
C     ************************************************************
C     ***** Triangular loop over symmetry independent shells *****
C     ************************************************************


      CALL IZ000(ISOFRA, 8)
      IDENA = 0
      DO 100 ISHELA = 1,KMAX
         NHKTA = NHKT(ISHELA)
         KHKTA = KHKT(ISHELA)
         KCKTA = KCKT(ISHELA)
         ICA   = LCLASS(ISHELA)
         SPHRA = SPHR(ISHELA)
	 CALL LMNVAL(NHKTA,KCKTA,LVALUA,MVALUA,NVALUA)
	 NCENTA = NCENT(ISHELA)
         ICENTA = NUCNUM(NCENTA,1)
         MULA   = ISTBAO(ISHELA)
         MULTA  = MULT(MULA)
         NUCA   = NUCO(ISHELA)
         NUMCFA = NUMCF(ISHELA)
         JSTA   = JSTRT(ISHELA)
         CORAX  = CENT(ISHELA,1,1)
         CORAY  = CENT(ISHELA,2,1)
         CORAZ  = CENT(ISHELA,3,1)
         IDENB0 = 0
C
C        Compute symmetry integral pointers for contributions
C        from this block.  Note that at present this assumes all
C        components from a shell are included.
C
         DO 600 I = 1, 8
            ISOFRB(I) = 0
            DO 610 J = 1, MXAQN
              INDFA(I,J) = -10 000 000
610         CONTINUE
600      CONTINUE
         DO 620 NA = 1, KHKTA
            DO 630 IREP = 0, MAXREP
            IF (IBTAND(MULA,IBTXOR(IREP,ISYMAO(NHKTA,NA))).EQ.0) THEN
               ISOFRA(IREP+1)    = ISOFRA(IREP+1) + 1
               INDFA (IREP+1,NA) = ISOFRA(IREP+1)
            END IF
630         CONTINUE
620      CONTINUE
         IF (IPRINT .GT. 20) THEN
c            WRITE(LUPRI,'(A,I4)')' IA address offsets for shell ',I
c    + SHELA

            DO 640 NA = 1,KHKTA
c               WRITE(LUPRI,'(8(1X,I5))') (INDFA(I,NA), I = 1,MAXREP+1)
640         CONTINUE
         END IF
      DO 110 ISHELB = 1,ISHELA
         LDIAG = ISHELA .EQ. ISHELB
         NHKTB = NHKT(ISHELB)
         KHKTB = KHKT(ISHELB)
         KCKTB = KCKT(ISHELB)
         ICB   = LCLASS(ISHELB)
         SPHRB = SPHR(ISHELB)
	 CALL LMNVAL(NHKTB,KCKTB,LVALUB,MVALUB,NVALUB)
	 NCENTB = NCENT(ISHELB)
         NHKTAB = NHKTA + NHKTB
         MULB   = ISTBAO(ISHELB)
         MULTB  = MULT(MULB)
         NUCB   = NUCO(ISHELB)
         NUMCFB = NUMCF(ISHELB)
         JSTB   = JSTRT(ISHELB)
         CORBX0 = CENT(ISHELB,1,1)
         CORBY0 = CENT(ISHELB,2,1)
         CORBZ0 = CENT(ISHELB,3,1)
         KHKTAB = KHKTA*KHKTB
         KCKTAB = KCKTA*KCKTB
         MAB    = IBTOR(MULA,MULB)
         KAB    = IBTAND(MULA,MULB)
         HKAB   = FMULT(KAB)
C
         SPHRAB = SPHRA .OR. SPHRB
C
C        Compute symmetry integral pointers for contributions
C        from this block.  Note that at present this assumes all
C        components from a shell are included
C
         DO 700 I = 1, 8
            DO 710 J = 1, MXAQN
              INDFB(I,J) = -10 000 000
710         CONTINUE
700      CONTINUE
         DO 720 NB = 1, KHKTB
            DO 730 IREP = 0, MAXREP
            IF (IBTAND(MULB,IBTXOR(IREP,ISYMAO(NHKTB,NB))).EQ.0) THEN
               ISOFRB(IREP+1)    = ISOFRB(IREP+1) + 1
               INDFB (IREP+1,NB) = ISOFRB(IREP+1)
            END IF
730         CONTINUE
720      CONTINUE
         IF (IPRINT .GT. 20) THEN
c            WRITE(LUPRI,'(A,I4)')' IB address offsets for shell ',I
c    + SHELB

            DO 740 NB = 1, KHKTB
c               WRITE(LUPRI,'(8(1X,I5))') (INDFB(I,NB), I = 1,MAXREP+1)
740         CONTINUE
         ENDIF
         IF(.NOT.DOINT(ICA,ICB)) GOTO 110
C
C        Initialization for nuclear attraction integrals
C
         JMAX = NHKTAB - 2
         IF (PROPTY) JMAX = JMAX + MAXDIF
         ISTEPU = JMAX + 1
         ISTEPV = ISTEPU*ISTEPU
         NAHGTF = ISTEPU*ISTEPV
         NATOMC = 0
CTM  : since all nucind atoms now have a charge of null
CTM  : we consider only the extra one with charge=1

         DO 120 IATOMC = 1,NUCIND
            MULC   = ISTBNU(IATOMC)
            MABC   = IBTOR(MULC,KAB)
            CORCX0 = CORD(1,IATOMC)
            CORCY0 = CORD(2,IATOMC)
            CORCZ0 = CORD(3,IATOMC)
            FACTOR = - FMULT(IBTAND(MULC,KAB))*CHARGE(IATOMC)/HKAB
            DO 130 ISYMOP = 0, MAXOPR
               IF (IBTAND(ISYMOP,MABC) .EQ. 0) THEN
                  NATOMC = NATOMC + 1
                  JSYMC(NATOMC)   = ISYMOP
                  JCENTC(NATOMC)  = IATOMC
                  SIGNC(1,NATOMC) = PT(IBTAND(ISYMAX(1,1),ISYMOP))
                  SIGNC(2,NATOMC) = PT(IBTAND(ISYMAX(2,1),ISYMOP))
                  SIGNC(3,NATOMC) = PT(IBTAND(ISYMAX(3,1),ISYMOP))
                  COORC(1,NATOMC) = SIGNC(1,NATOMC)*CORCX0
                  COORC(2,NATOMC) = SIGNC(2,NATOMC)*CORCY0
                  COORC(3,NATOMC) = SIGNC(3,NATOMC)*CORCZ0
                  FACINT(NATOMC)  = FACTOR
                  NCENTC(NATOMC)  = NUCNUM(IATOMC,ISYMOP+1)
               END IF
  130       CONTINUE
  120       CONTINUE

         DO 121 IATOMC = 1,NPP         
            MULC   = ISTBNU(IATOMC)
C  compute ndegcharge for all centres 1, npp
            MULC   = NDEGCHARGE
            MABC   = IBTOR(MULC,KAB)
            CORCX0 = point(1,IATOMC)
            CORCY0 = point(2,IATOMC)
            CORCZ0 = point(3,IATOMC)
            FACTOR = - FMULT(IBTAND(MULC,KAB))*CHG(IATOMC)/HKAB
            DO 131 ISYMOP = 0, MAXREP
               IF (IBTAND(ISYMOP,MABC) .EQ. 0) THEN
                  NATOMC = NATOMC + 1
                  JSYMC(NATOMC)   = ISYMOP
                  JCENTC(NATOMC)  = IATOMC
                  SIGNC(1,NATOMC) = PT(IBTAND(ISYMAX(1,1),ISYMOP))
                  SIGNC(2,NATOMC) = PT(IBTAND(ISYMAX(2,1),ISYMOP))
                  SIGNC(3,NATOMC) = PT(IBTAND(ISYMAX(3,1),ISYMOP))
                  COORC(1,NATOMC) = SIGNC(1,NATOMC)*CORCX0
                  COORC(2,NATOMC) = SIGNC(2,NATOMC)*CORCY0
                  COORC(3,NATOMC) = SIGNC(3,NATOMC)*CORCZ0
                  FACINT(NATOMC)  = FACTOR
C  compute nucnucnum for all centres 1, npp
                  NCENTC(NATOMC)  = NUCNUCNUM(IATOMC,ISYMOP+1)
               END IF
  131       CONTINUE
  121       CONTINUE

	 CALL ONESOP(VNE,RESULT,FACINT,COORC,
     &               WORK, LWRK,
     &               IPRINT,PROPTY,MAXDIF,IDENB0,CORBX0,CORBY0,CORBZ0,
     &               DIFDIP,SECDER,NATOMC,TOLOG,TOLS,JSYMC,JCENTC,
     &               NCENTC,SIGNC,NNBASX,WORK(KFCM),TLMD(1,1),.false.)
  110    IDENB0 = IDENB0 + KHKTB*MULTB
         IDENA = IDENA + KHKTA*MULTA
  100 CONTINUE
C
C     ***** End loop over symmetry independent orbitals *****
C
      RETURN
 1000 FORMAT (//,2X,'***************************************',
     *         /,2X,'********** ISHELA/B =',I3,',',I3,' **********',
     *         /,2X,'***************************************',/)
      END
