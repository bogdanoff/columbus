!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
       subroutine irreducible(isym,x,y,z,valid)
c
c Silmar - 10/23/01 - This subroutine reads a point
c and analyses whether the point is in a given
c irreducible set or not. The irreducible set is defined
c by the symmetry used.
c
c
c INPUT ARGUMENTS:
c
c isym -> symmetry point group(integer number)
c 0,1,2,...,7 for C1,Cs,Ci,C2,D2,C2v,C2h,D2h, respectively
c x,y,z -> Coordinates of the point
c
c OUTPUT ARGUMENT:
c
c valid -> If true the point is in an irreducible set. If
c false it is not.
c
       implicit none
        real*8 x,y,z
       integer isym
       logical valid
c
        go to (1,2,3,4,5,6,7,8),isym+1
        write(*,*) 'invalid isym, isym=',isym
        stop
c
    1  continue
       valid=.true.
       go to 55
c
c For Cs the symmetry plane is the xy plane!
c
c
    2  continue
       valid=(z.ge.0.0d0)
       go to 55
c
    3  continue
       valid=(z.ge.0.0d0)
       go to 55
c
c For C2 the C2 axis is the z axis!
c
c
    4  continue
       valid=(x.ge.0.0d0)
       go to 55
c
    5  continue
       valid=(x.ge.0.0d0.and.y.ge.0.0d0)
       go to 55
c
c
c For C2v the C2 axis is the z axis!
c
c
    6  continue
       valid=(x.ge.0.0d0.and.y.ge.0.0d0)
       go to 55
c
c
c For C2h the C2 axis is the z axis!
C
c
    7  continue
       valid=(x.ge.0.0d0.and.z.ge.0.0d0)
       go to 55
c
    8  continue
       valid=(x.ge.0.0d0.and.y.ge.0.0d0.and.z.ge.0.0d0)
   55  continue
       return
       end
