!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      subroutine cosmo(icase, xyz, mtype, natoms, isym, cosurf,
     &                 nps, npspher, phi, qcos, ediel, elast,
     &                 a1mat, a2mat, a3mat, dcos)
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c PARAMETER:
c icase:   defines the action
c xyz:     atomic coordinates [bohr]
c mtype:   nuclear charges
c natoms:  number of atoms
c isym:    symmetry identifier
c cosurf:  cavity surface (inner and outer cavity) [bohr]
c nps:     number of segments
c npspher: number of segments on the outer cavity
c phi:     potential on the segments
c qcos:    screening charge of the segments
c ediel:   dielectric energy
c elast:   last SCF energy (inclusive dielectric part)
c a1mat:
c a2mat:   axmat -> arrays for the A matrices
c a3mat:
c dcos:    Grad. contribution from cavity
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      implicit real*8(a-h,o-z)

      include 'cosinc.h'

c-------------------COMMON BLOCKS---------------------------------------
c
c I/O units

      include 'cosmovar.inc'
      save /cosmovar/
      include 'coss.inc'

      save /coss/

c-----------------------------------------------------------------------
c
      dimension   xyz(3,natoms), phi(nps), qcos(nps)
      dimension   mtype(natoms)
      dimension   a2mat(npspher,nps), a3mat(npspher*(npspher+1)/2)

c use max parameters from cosinc.h

      dimension   cosurf(3,2*maxnps), a1mat(maxnps*(maxnps+1)/2)
      dimension   iatsp(2*maxnps)
      dimension   ar(2*maxnps)

c Only used with case=2
      dimension   dirsm(3*maxnspa), dirsmh(3*maxnsph)
      dimension   nar(2*maxnps)
      dimension   dirvec(3*maxnppa), dirtm(3*maxnppa), xsp(3*2*maxnps)
      dimension   basgrd(3*1082)
c     fill basgrd with the 1082 grid
c     include 'basgrd.h'

c Only for gradient case=7
      dimension sude(4*6*maxat)
      dimension isude(4*6*maxat)
      dimension dcos(3,natoms)


c mtype holds nuc. charges and smtype atomic symbols
      character*8 smtype(maxat)

c pse for mapping nuc charge -> element symbol
      parameter (ipse=83)
      character*8 pse(ipse)
      data pse/'h ','he','li','be','b ','c ','n ','o ','f ','ne','na',
     &         'mg','al','si','p ','s ','cl','ar','k ','ca','sc','ti',
     &         'v ','cr','mn','fe','co','ni','cu','zn','ga','ge','as',
     &         'se','br','kr','rb','sr','y ','zr','nb','mo','tc','ru',
     &         'rh','pd','ag','cd','in','sn','sb','te','i ','xe','cs',
     &         'ba','la','ce','pr','nd','pm','sm','eu','gd','tb','dy',
     &         'ho','er','tm','yb','lu','hf','ta','w ','re','os','ir',
     &         'pt','au','hg','tl','pb','bi'/
c to be saved
      save iatsp, ar, smtype


c-------------------CASE=1 (INITIALIZATION)-----------------------------
       call basgrdinit(basgrd) 
      if(icase .eq. 1) then
         call cosmoinit(mtype, natoms)

c     CHECK VALUES
        if (natoms .gt. maxat) then
c        write(icolum,'(a/a)')'ERROR cosmoinit: number of atoms too big'
c        stop
      call bummer('ERROR cosmoinit: number of atoms too big',natoms,2)
        endif
        if (nspa .gt. maxnspa) then
         call bummer('ERROR cosmoinit: nspa > maxnspa',nspa,2)
c         write(icolum,'(a/a)')'ERROR cosmoinit: nspa > maxnspa'
c        stop
        endif
        if (nsph .gt. maxnsph) then
         call bummer('ERROR cosmoinit: nsph > maxnsph',nsph,2)
c        write(icolum,'(a/a)')'ERROR cosmoinit: nsph > maxnsph'
c        stop
        endif
        if (nppa .gt. maxnppa) then
         call bummer('ERROR cosmoinit: nppa > maxnppa',nppa,2)
c        write(icolum,'(a/a)')'ERROR cosmoinit: nppa > maxnppa'
c        stop
        endif
      endif
c-------------------CASE=2 (CAVITY CONSTRUCTION)------------------------
      if(icase .eq. 2) then
c
c  set up direction vectors and basis grid
c
        call dvfill(nspa,dirsm,icolum)
        call dvfill(nsph,dirsmh,icolum)

        if (nppa.eq.1082) then
            do i = 1, 3*nppa
               dirvec(i) = basgrd(i)
            enddo
        else
            call dvfill(nppa,dirvec,icolum)
        end if
c convert mtype (nuc. charges) to atomic symbols
        do i=1,natoms
           j= mtype(i)
           if(j .gt. ipse) then
              smtype(i) = '--'
           else
              smtype(i) = pse(mtype(i))
           endif
        enddo
c
        call constanten(xyz, smtype, natoms, cosurf, iatsp, dirsm,
     &   dirsmh, dirvec, nar, ar, dirtm, xsp, a1mat, isym, nps, npspher)
c
c
c     CHECK VALUES

        if (nps .gt. maxnps) then
         write(icolum,'(a/a)')'ERROR cosmoinit: nps > maxnps'
         stop
        endif
        if (npspher .gt. maxnps) then
         write(icolum,'(a/a)')'ERROR cosmo: npspher > maxnps'
         stop
        endif
      endif
c-------------------CASE=3 (CALC. SCREENING CHARGES)--------------------
c-------------------CASE=5 (CALC DELTA q(DELTA PHI))--------------------
c
      if(icase .eq. 3 .or. icase .eq. 5) then

c     Calc. screening charges: qcos=-A**(-1)*phi

      fac = fepsi
      scal = -1.d0

      if(icase .eq. 5) then
         if(fn2 .lt. 0.d0) then
            write(icolum,'(a)')
     &      'ERROR cosmo: refraction_index is not defined in cosmo.inp'
            stop
         endif
         fac = fn2
      endif

      call coschol2(a1mat, qcos, phi, nps, scal)

c     Calculate Ediel = fac*0.5d0*qcos*Phi
         ediel = 0.d0
         scalar=0.d0
         do i=1,nps
           scalar = scalar + phi(i)*qcos(i)
         enddo
         ediel =  fac * 0.5d0 * scalar
      endif
c-------------------CASE=4 (OUTLYING CHARGE CORR, OUTPUT)---------------
c-------------------CASE=6 (SAME FOR EXCITED STATES)--------------------
      if(icase .eq. 4 .or. icase .eq. 6) then

c     ifac 0: normal calc. using f(epsilon)
c     ifac 1: excited state calc. using f(n**2)

      ifac = 0
      if(icase .eq. 6) ifac =1

      call  outcorr(a1mat, a2mat, a3mat, phi, qcos, ediel, elast,
     &                  cosurf, iatsp, ar, xyz, mtype, smtype, natoms,
     &                  isym, nps, npspher, ifac)
      endif
c-------------------CASE=7 (GRAD. CONTRIBUTION FROM CAVITY (A MATRIX))--
      if(icase .eq. 7) then
c
c        first read dimensions from files
c
         call cosgrddim(icosout,icossu,icolum,lcavity,nps,fepsi,nip)
c
c        init: read data from out.cosmo and sude files
c
         call cosgrdinit(icosout,icossu,icolum,lcavity,nps,fepsi,
     &                      nip,sude,isude,cosurf,iatsp,qcos,ar)
c
c        calculate gradient contribution
c
         call cavitygrd(isym,natoms,xyz,nps,cosurf,iatsp,qcos,ar,
     &                  nip,sude,isude,lcavity,dcos)
      endif

c DEGUG OUTPUT

      if(icase .eq. 9) then
        write(icolum,'(a)')'====== DEBUG COSMO ======'
        write(icolum,'(a,i5)')'      CASE =',icase
        write(icolum,'(a,f14.7)')'eps = ',eps
        write(icolum,'(a,f14.7)')'fepsi = ',fepsi
        write(icolum,'(a,i5)')   'nppa = ',nppa
        write(icolum,'(a,i5)')   'nspa = ',nspa
        write(icolum,'(a,i5)')   'nsph = ',nsph
        write(icolum,'(a,f14.7)')'disex = ',disex
        write(icolum,'(a,f14.7)')'routf = ',routf
        write(icolum,'(a,f14.7)')'rsolv = ',rsolv
        write(icolum,'(a,i5)')   'lcavity = ',lcavity
        write(icolum,'(a,f14.7)')'phsran = ',phsran
        write(icolum,'(a,f14.7)')'ampran = ',ampran
        write(icolum,'(a)')'# of atom   radius (bohr, inc. rsovl)'
        do i=1,natoms
          write(icolum,'(i5,a,f14.7)')i,'   ',srad(i)
        enddo
        write(icolum,'(a)')'======  END DEBUG COSMO ======'
c END DEBUG OUTPUT
      endif

      return
      end
