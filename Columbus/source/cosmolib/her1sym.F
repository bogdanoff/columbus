!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
#if defined (VAR_SBLAS)
#define DDOT SDOT
#endif 
C  /* Deck onesop */
      SUBROUTINE ONESOP(STHMAT,DENMAT,FOCMAT,FACINT,COORC,WORK,LWORK,
     &                  IPRINT,PROPTY,MAXDIF,IDENB0,CORBX0,CORBY0,
     &                  CORBZ0,DIFDIP,SECDER,NATOMC,TOLOG,TOLS,JSYMC,
     &                  JCENTC,NCENTC,SIGNC,NNBASX,FCM,TLMD)
#include <implicit.h>
#include <priunit.h>
#include <iratdef.h>
C
      LOGICAL SECDER, DIFDIP, PROPTY
      DIMENSION WORK(LWORK)
      DIMENSION STHMAT(NNBASX,3), DENMAT(NNBASX), FOCMAT(NNBASX),
     &          FACINT(*), COORC(3,*), JSYMC(*),
     &          JCENTC(*), NCENTC(*), SIGNC(3,*),
     &          FCM(*), TLMD(*)
#include <cbisol.h>
#include <onecom.h>
C
c      write(0,*) 'onesop: lwork=',lwork 
      IF (PROPTY) THEN
         NADER = 28*NATOMC*KCKTAB
      ELSE
         NADER = KCKTAB
      END IF
      KSDER0 = 1
      KSDER1 = KSDER0 +  2*KCKTAB
      KSDER2 = KSDER1 +  6*KCKTAB
      KADER  = KSDER2 + 12*KCKTAB
      KSINT0 = KADER  +    NADER
      KDINT1 = KSINT0 +    KCKTAB
      KDSHEL = KDINT1 +  9*KCKTAB
      KFSHEL = KDSHEL +    KHKTAB
      KLAST  = KFSHEL +    KHKTAB
      IF (SOLVNT) THEN
         IF (MAXDIF .EQ. 0) THEN
            KRLMNT = KLAST
            KRLMTB = KRLMNT + LMNTOT*KCKTAB
            KLAST  = KRLMTB
            KLMNO  = KLAST
         ELSE IF (MAXDIF .EQ. 1) THEN
            KRLMNT = KLAST
            KRLMTB = KRLMNT + 7*LMNTOT*KCKTAB
            KLAST  = KRLMTB
            KLMNO  = KLAST
         ELSE
            KRLMNT = KLAST
            KRLMTB = KRLMNT + 7*LMNTOT*KCKTAB
            KLMNO  = KRLMTB + 21*KCKTAB
            KLAST  = KLMNO  + (3*LMNTOT + 1)/IRAT
         END IF
      ELSE
         KRLMNT = KLAST
         KRLMTB = KLAST
         KLMNO  = KLAST
      END IF
      LWRK   = LWORK  - KLAST + 1
      IF (KLAST .GT. LWORK) CALL STOPIT('ONESOP',' ',KLAST,LWORK)
C
      CALL ONESO1(STHMAT,WORK(KSDER0),WORK(KSDER1),WORK(KSDER2),
     &            WORK(KADER),WORK(KSINT0),WORK(KDINT1),
     &            DENMAT,FOCMAT,FACINT,COORC,WORK(KLAST),LWRK,IPRINT,
     &            PROPTY,MAXDIF,IDENB0,CORBX0,CORBY0,CORBZ0,DIFDIP,
     &            SECDER,NATOMC,TOLOG,TOLS,JSYMC,JCENTC,NCENTC,SIGNC,
     &            NNBASX,WORK(KDSHEL),WORK(KFSHEL),WORK(KRLMNT),
     &            WORK(KRLMTB),FCM,TLMD,WORK(KLMNO))
      RETURN
      END
C  /* Deck oneso1 */
      SUBROUTINE ONESO1(STHMAT,STDER0,STDER1,STDER2,ADER,SINT0,
     &                  DINT1,DENMAT,FOCMAT,FACINT,COORC,WORK,LWORK,
     &                  IPRINT,PROPTY,MAXDIF,IDENB0,CORBX0,CORBY0,
     &                  CORBZ0,DIFDIP,SECDER,NATOMC,TOLOG,TOLS,JSYMC,
     &                  JCENTC,NCENTC,SIGNC,NNBASX,DSHELL,FSHELL,
     &                  RLMINT,RLMTAB,FCM,TLMD,LMNO)
#include <implicit.h>
#include <mxcent.h>
#include <maxaqn.h>
#include <mxorb.h>
#include <iratdef.h>
#include <priunit.h>
#include <dummy.h>
      PARAMETER (D1 = 1.0D0, DP5 = 0.5D0)
C
      LOGICAL SECDER, DIFDIP, PROPTY, FULMAT, ANTI
      DIMENSION WORK(LWORK)
      DIMENSION STHMAT(NNBASX,3), STDER0(KCKTAB,2), STDER1(KCKTAB,3,2),
     &          STDER2(KCKTAB,6,2), ADER(*),
     &          DSHELL(KHKTAB),FSHELL(KHKTAB),
     &          SINT0(KCKTAB), DINT1(KCKTAB,3,3),
     &          DENMAT(NNBASX),FOCMAT(NNBASX),
     &          FACINT(*), COORC(3,*), JSYMC(*),
     &          JCENTC(*), NCENTC(*), SIGNC(3,*),
     &          RLMINT(*), RLMTAB(*), FCM(*), TLMD(*), LMNO(*)
#include <onecom.h>
#include <nuclei.h>
#include <symmet.h>
#include <cbisol.h>
C
#include <ibtfun.h>
      ITRI(I,J) = MAX(I,J)*(MAX(I,J) - 1)/2 + MIN(I,J)
c      IF (IPRINT .GT. 4) CALL TITLER('Output from ONESO1','*',103)
C
C     *****************************************
C     ***** Loop over symmetry operations *****
C     *****************************************
C
      IDENB = IDENB0 - KHKTB
      DO 100 ISYMOP = 0, MAXOPR
      IF(IBTAND(ISYMOP,MAB) .EQ. 0) THEN
         IDENB  = IDENB + KHKTB
         ICENTB = NUCNUM(NCENTB,ISYMOP+1)
         ONECEN = ICENTA .EQ. ICENTB
         SIGNBX = PT(IBTAND(ISYMAX(1,1),ISYMOP))
         SIGNBY = PT(IBTAND(ISYMAX(2,1),ISYMOP))
         SIGNBZ = PT(IBTAND(ISYMAX(3,1),ISYMOP))
         CORBX  = SIGNBX*CORBX0
         CORBY  = SIGNBY*CORBY0
         CORBZ  = SIGNBZ*CORBZ0
c         IF (IPRINT .GE. 05) WRITE (LUPRI, 1010) ISYMOP
c         IF (IPRINT .GE. 10) THEN
c            WRITE (LUPRI,'(A,1F12.6)') ' CORBX    ', CORBX
c            WRITE (LUPRI,'(A,1F12.6)') ' CORBY    ', CORBY
c            WRITE (LUPRI,'(A,1F12.6)') ' CORBZ    ', CORBZ
c         END IF
C
C        **********************************************
C        ***** Calculation of Cartesian integrals *****
C        **********************************************
C
         CALL ONEPRM(STDER0,STDER1,STDER2,ADER,SINT0,DINT1,
     &               RLMINT,RLMTAB,FCM,WORK,LWORK,IPRINT,PROPTY,MAXDIF,
     &               NATOMC,TOLOG,TOLS,SECDER,DIFDIP,FACINT,COORC,
     &               GNUEXP,NCENTC)
C
C        *************************************************
C        ***** Transform to spherical harmonic basis *****
C        *************************************************
C
         IF (SPHRAB) THEN
            IF (PROPTY) THEN
               IF (ONECEN) THEN
                  CALL SPHRM1(ADER,ADER,10*NATOMC,WORK,LWORK,PROPTY,
     &                        IPRINT)
               ELSE
                  CALL SPHRM1(STDER0,STDER0,2,WORK,LWORK,PROPTY,IPRINT)
                  CALL SPHRM1(STDER1,STDER1,6,WORK,LWORK,PROPTY,IPRINT)
                  IF (SECDER) THEN
                     CALL SPHRM1(STDER2,STDER2,12,WORK,LWORK,PROPTY,
     &                           IPRINT)
                  END IF
                  CALL SPHRM1(ADER,ADER,28*NATOMC,WORK,LWORK,PROPTY,
     &                        IPRINT)
                  IF (DIFDIP) THEN
                     CALL SPHRM1(DINT1,DINT1,9,WORK,LWORK,PROPTY,IPRINT)
                  END IF
               END IF
               CALL SPHRM1(SINT0,SINT0,1,WORK,LWORK,PROPTY,IPRINT)
               IF (SOLVNT) THEN
                  CALL SPHRM1(RLMINT,RLMINT,7*LMNTOT,WORK,LWORK,PROPTY,
     &                        IPRINT)
                  IF (SECDER) THEN
                     CALL SPHRM1(RLMTAB,RLMTAB,21,WORK,LWORK,PROPTY,
     &                           IPRINT)
                  END IF
               END IF
            ELSE
               CALL SPHRM1(STDER0,STDER0,2,WORK,LWORK,PROPTY,IPRINT)
               CALL SPHRM1(ADER,ADER,1,WORK,LWORK,PROPTY,IPRINT)
               IF (SOLVNT) THEN
                  CALL SPHRM1(RLMINT,RLMINT,LMNTOT,WORK,LWORK,PROPTY,
     &                        IPRINT)
               END IF
            END IF
         END IF
C
C        ******************************
C        ***** Expectation values *****
C        ******************************
C
         IF (PROPTY) THEN
C
C           One-electron Hamiltonian integrals and reorthonormalization
C           ===========================================================
C
C           Collect density and Fock elements
C           was eliminated in 05-09-01
C
            IF (ONECEN) THEN
C
C              Nuclear attraction
C
c               CALL AVENA1(ADER,NATOMC,SECDER,NCENTC,JCENTC,MAXCMP,
c     &                     JSYMC,SIGNC,DSHELL)
            ELSE
C
C              Kinetic energy and reorthonormalization
C
c               CALL AVEKFS(STDER0,STDER1,STDER2,ISYMOP,MAXCMP,SECDER,
c     &                     DSHELL,FSHELL)
C
C              Nuclear attraction
C
c               CALL AVENA2(ADER,NATOMC,ISYMOP,SECDER,NCENTC,MAXCMP,
c     &                     JCENTC,JSYMC,SIGNC,DSHELL)
            END IF
c            IF (IPRINT .GT. 50) THEN
c               CALL HEADER('Kinetic energy integral gradient',-1)
c               CALL PRIGRD(GRADKE)
c               CALL HEADER('Nuclear attraction integral gradient',-1)
c               CALL PRIGRD(GRADNA)
c               CALL HEADER('Reorthonormalization gradient',-1)
c               CALL PRIGRD(GRADFS)
c            END IF
C
C           Dipole gradient
C           ===============
C
C
C           Solvent contributions
C           =====================
            IF (SOLVNT) THEN
            END IF
         END IF
C
C        *******************************************
C        ***** Transform integrals to SO basis *****
C        *******************************************
C
         FULMAT = .TRUE.
         ANTI   = .FALSE.
C
C        Overlap integrals
C        =================
C
         CALL SYM1S(STDER0(1,1),STHMAT(1,1),ISYMOP,MULA,MULB,NHKTA,
     &              NHKTB,KHKTA,KHKTB,HKAB,LDIAG,FULMAT,DUMMY,IDUMMY,
     &              IPRINT)
C
C        Kinetic energy integrals
C        ========================
C
         CALL SYM1S(STDER0(1,2),STHMAT(1,3),ISYMOP,MULA,MULB,NHKTA,
     &              NHKTB,KHKTA,KHKTB,HKAB,LDIAG,FULMAT,DUMMY,IDUMMY,
     &              IPRINT)
C
C        Nuclear attraction integrals
C        ============================
C
         CALL SYM1S(ADER,STHMAT(1,2),ISYMOP,MULA,MULB,NHKTA,NHKTB,
     &              KHKTA,KHKTB,HKAB,LDIAG,FULMAT,DUMMY,IDUMMY,
     &              IPRINT)
C
C        **************************************************
C        ***** Write differentiated integrals on file *****
C        **************************************************
C
         IF (PROPTY .AND. (SECDER .OR. DIFDIP)) THEN
            CALL WD1SYM(STDER1,ADER,RLMINT,FCM,WORK,LWORK,JSYMC,JCENTC,
     &                  ISYMOP,NATOMC,IPRINT)
         END IF
      END IF
  100 CONTINUE
      RETURN
 1010 FORMAT (//,2X,'***************************************',
     *         /,2X,'******** Symmetry operation ',I2,' ********',
     *         /,2X,'***************************************',/)
      END
C  /* Deck wd1sym */
      SUBROUTINE WD1SYM(STDER1,ADER,RLMINT,FCM,WORK,LWORK,JSYMC,JCENTC,
     &                  ISYMOP,NATOMC,IPRINT)
#include <implicit.h>
#include <priunit.h>
      DIMENSION STDER1(KCKTAB,3,2), ADER(*), JSYMC(*), JCENTC(*),
     &          RLMINT(*), FCM(*), WORK(LWORK)
#include <onecom.h>
C
      KOMAT = 1
      KLAST  = KOMAT + 6*KCKTAB
      IF (KLAST .GT. LWORK) CALL STOPIT('WD1SYM',' ',KLAST,LWORK)
      CALL WD1SY1(STDER1,ADER,RLMINT,FCM,WORK(KOMAT),JSYMC,JCENTC,
     &            ISYMOP,NATOMC,IPRINT)
      RETURN
      END
C  /* Deck wd1sy1 */
      SUBROUTINE WD1SY1(STDER1,ADER,RLMINT,FCM,OMAT,JSYMC,JCENTC,ISYMOP,
     &                  NATOMC,IPRINT)
#include <implicit.h>
#include <priunit.h>
      DIMENSION STDER1(KCKTAB,3,2), ADER(KCKTAB,NATOMC,*), JSYMC(*),
     &          JCENTC(*), OMAT(KCKTAB,3,2), RLMINT(KCKTAB,LMNTOT,7),
     &          FCM(*)
#include <cbisol.h>
#include <onecom.h>
#include <ader.h>
      IF (IPRINT .GT. 5) THEN
         NADER = 28*NATOMC
         IF (ONECEN) NADER = 10*NATOMC
c         CALL HEADER('First derivative overlap matrix',-1)
         CALL OUTPUT(STDER1(1,1,1),1,KHKTAB,1,3,KCKTAB,3,1,LUPRI)
c         CALL HEADER('First derivative kinetic energy matrix',-1)
         CALL OUTPUT(STDER1(1,1,2),1,KHKTAB,1,3,KCKTAB,3,1,LUPRI)
c         CALL HEADER('ADER',-1)
         CALL OUTPUT(ADER,1,KHKTAB,1,NADER,KCKTAB,NADER,1,LUPRI)
      END IF
C
C     Write differentiated integrals on file
C
C     Overlap and kinetic energy integrals
C     ====================================
C
      IF (.NOT.ONECEN) THEN
         CALL dcopy_wr(6*KCKTAB,STDER1,1,OMAT,1)
         CALL DRSYM1(OMAT(1,1,1),OMAT(1,1,2),NCENTA,NCENTB,ISYMOP,MULA,
     &               MULB,NHKTA,NHKTB,KHKTA,KHKTB,KHKTAB,KCKTAB,HKAB,
     &               LDIAG,IPRINT)
      END IF
C
C     Nuclear attraction integrals
C     ============================
C
      DO 200 IATOMC = 1, NATOMC
         DO 300 ICMPAB = 1, KHKTAB
            OMAT(ICMPAB,1,1) = ADER(ICMPAB,IATOMC,IA000X)
            OMAT(ICMPAB,2,1) = ADER(ICMPAB,IATOMC,IA000Y)
            OMAT(ICMPAB,3,1) = ADER(ICMPAB,IATOMC,IA000Z)
            IF (.NOT.ONECEN) THEN
               OMAT(ICMPAB,1,2) = ADER(ICMPAB,IATOMC,IA0X00)
               OMAT(ICMPAB,2,2) = ADER(ICMPAB,IATOMC,IA0Y00)
               OMAT(ICMPAB,3,2) = ADER(ICMPAB,IATOMC,IA0Z00)
            END IF
  300    CONTINUE
         ISYMC  = JSYMC(IATOMC)
         ICENTC = JCENTC(IATOMC)
         CALL DASYM1(OMAT(1,1,1),OMAT(1,1,2),ONECEN,NCENTA,NCENTB,
     &               ICENTC,ISYMOP,ISYMC,MULA,MULB,NHKTA,NHKTB,KHKTA,
     &               KHKTB,KHKTAB,KCKTAB,HKAB,LDIAG,IPRINT)
  200 CONTINUE
C
C     Solvent contribution
C     ====================
C
      IF (SOLVNT) THEN
        DO 400 ICMPAB = 1, KHKTAB
          OMAT(ICMPAB,1,1)=ddot_wr(LMNTOT,FCM,1,RLMINT(ICMPAB,1,5),K
     + CKTAB)

          OMAT(ICMPAB,2,1)=ddot_wr(LMNTOT,FCM,1,RLMINT(ICMPAB,1,6),K
     + CKTAB)

          OMAT(ICMPAB,3,1)=ddot_wr(LMNTOT,FCM,1,RLMINT(ICMPAB,1,7),K
     + CKTAB)

          OMAT(ICMPAB,1,2)=ddot_wr(LMNTOT,FCM,1,RLMINT(ICMPAB,1,2),K
     + CKTAB)

          OMAT(ICMPAB,2,2)=ddot_wr(LMNTOT,FCM,1,RLMINT(ICMPAB,1,3),K
     + CKTAB)

          OMAT(ICMPAB,3,2)=ddot_wr(LMNTOT,FCM,1,RLMINT(ICMPAB,1,4),K
     + CKTAB)

  400   CONTINUE
        CALL DASYM1(OMAT(1,1,1),OMAT(1,1,2),ONECEN,NCENTA,NCENTB,
     &              NCNTCV,ISYMOP,0,MULA,MULB,NHKTA,NHKTB,KHKTA,
     &              KHKTB,KHKTAB,KCKTAB,HKAB,LDIAG,IPRINT)
      END IF
      RETURN
      END
C  /* Deck dsym1 */
C  /* Deck drsym1 */
      SUBROUTINE DRSYM1(SAODER,TAODER,ICENTA,ICENTB,ISYMOP,MULA,MULB,
     &                  NHKTA,NHKTB,KHKTA,KHKTB,KHKTAB,KCKTAB,HKAB,
     &                  LDIAG,IPRINT)
C
C     Arrange calculation of symmetry-adapted integral derivatives
C     from distinct AO integral derivatives
C                                                PRT & TUH  880428
C
#include <implicit.h>
#include <priunit.h>
#include <mxcent.h>
#include <mxorb.h>
#include <maxaqn.h>
#include <ccom.h>
      DIMENSION SAODER(KCKTAB,*), TAODER(KCKTAB,*)
      LOGICAL LDIAG, FULMAT, ANTI
#include <nuclei.h>
#include <symmet.h>
#include <ibtfun.h>
      IF (IPRINT .GT. 10) THEN
c         CALL HEADER('Subroutine DRSYM1',-1)
c         WRITE (LUPRI,'(A,2I5)') ' ICENTA/B ', ICENTA, ICENTB
c         WRITE (LUPRI,'(A, I5)') ' ISYMOP   ', ISYMOP
c         WRITE (LUPRI,'(A,2I5)') ' NHKTA/B  ', NHKTA, NHKTB
c         WRITE (LUPRI,'(A,2I5)') ' KHKTA/B  ', KHKTA, KHKTB
c         WRITE (LUPRI,'(A,2I5)') ' MULA/B   ', MULA, MULB
c         WRITE (LUPRI,'(A,F12.6)') ' HKAB   ', HKAB
c         WRITE (LUPRI,'(A, L5)') ' LDIAG    ', LDIAG
      END IF
      FULMAT = .FALSE.
      ANTI   = .FALSE.
      NMATS = 3*NUCDEP*(MAXREP+1)
      DO 10 ICL = 1,2
C
C        Determine factors to account for use of transl. invariance
C
         IF (ICL .EQ. 1) THEN
            JCENT = ICENTA
         ELSE
            JCENT = ICENTB
         ENDIF
         MULJ = ISTBNU(JCENT)
C
C     Run over Cartesian directions
C
      DO 20 JCDIR = 1,3
         ISYTYJ = ISYMAX(JCDIR,1)
C
C     Run over irrep's of the differentiation operator
C
      DO 30 IREPD = 0,MAXREP
         IF (IBTAND(MULJ,IBTXOR(IREPD,ISYTYJ)) .EQ. 0) THEN
            IF (ICL .EQ. 1) THEN
               FAC = HKAB
            ELSE
               FAC = - HKAB*PT(IBTAND(ISYTYJ,ISYMOP))
     *                     *PT(IBTAND(IREPD, ISYMOP))
            END IF
            IF (IPRINT .GT. 20) THEN
c               WRITE (LUPRI, '(/A,3I5)') ' ICL, JCDIR, IREPD ',
c     *                                     ICL, JCDIR, IREPD
c               WRITE (LUPRI, '(A,2I5)') ' MULJ, ISYTYJ ', MULJ, ISYTYJ
c               WRITE (LUPRI, '(A,F12.6)') ' FAC ', FAC
            END IF
            IMAT0 = (IPTCNT(3*(JCENT-1)+JCDIR,IREPD,1)-1)*(MAXREP+1)+1
            IF (IREPD .EQ. 0) THEN
C
C              Overlap matrix - totally symmetric perturbation
C
               CALL SYM1S(SAODER(1,JCDIR),DUMMY,ISYMOP,MULA,MULB,
     *                    NHKTA,NHKTB,KHKTA,KHKTB,FAC,LDIAG,FULMAT,
     *                    THRS,IMAT0,IPRINT)
C
C              Kinetic energy  - totally symmetric perturbation
C
               IMAT0 = NMATS + IMAT0
               CALL SYM1S(TAODER(1,JCDIR),DUMMY,ISYMOP,MULA,MULB,
     *                    NHKTA,NHKTB,KHKTA,KHKTB,FAC,LDIAG,FULMAT,
     *                    THRS,IMAT0,IPRINT)
            ELSE
C
C              Overlap matrix - non-symmetric perturbation
C
               CALL SYM1N(SAODER(1,JCDIR),DUMMY,IREPD,ISYMOP,MULA,
     *                    MULB,NHKTA,NHKTB,KHKTA,KHKTB,FAC,LDIAG,
     *                    FULMAT,ANTI,THRS,IMAT0,IPRINT)
C
C              Kinetic energy  - non-symmetric perturbation
C
               IMAT0 = NMATS + IMAT0
               CALL SYM1N(TAODER(1,JCDIR),DUMMY,IREPD,ISYMOP,MULA,
     *                    MULB,NHKTA,NHKTB,KHKTA,KHKTB,FAC,LDIAG,
     *                    FULMAT,ANTI,THRS,IMAT0,IPRINT)
            END IF
         END IF
30    CONTINUE
20    CONTINUE
10    CONTINUE
      RETURN
      END
C  /* Deck dasym1 */
      SUBROUTINE DASYM1(CAODER,AAODER,ONECEN,ICENTA,ICENTB,ICENTC,
     &                  ISYMOP,JSYMOP,MULA,MULB,NHKTA,NHKTB,
     &                  KHKTA,KHKTB,KHKTAB,KCKTAB,HKAB,LDIAG,
     &                  IPRINT)
C
C     Arrange calculation of symmetry-adapted integral derivatives
C     from distinct AO nuclear attraction integral derivatives
C                                                PRT & TUH  880502
C
#include <implicit.h>
#include <priunit.h>
#include <mxcent.h>
#include <mxorb.h>
#include <maxaqn.h>
      LOGICAL LDIAG, ONECEN
      DIMENSION CAODER(KCKTAB,*), AAODER(KCKTAB,*)
      LOGICAL FULMAT, ANTI
#include <ccom.h>
#include <nuclei.h>
#include <symmet.h>
#include <ibtfun.h>
      IF (IPRINT .GT. 10) THEN
c         CALL HEADER('Subroutine DASYM1',-1)
c         WRITE (LUPRI,'(A,2I5)') ' ICENTA/B ', ICENTA, ICENTB
c         WRITE (LUPRI,'(A, I5)') ' ISYMOP   ', ISYMOP
c         WRITE (LUPRI,'(A, I5)') ' ICENTC   ', ICENTC
c         WRITE (LUPRI,'(A, I5)') ' JSYMOP   ', JSYMOP
c         WRITE (LUPRI,'(A,2I5)') ' NHKTA/B  ', NHKTA, NHKTB
c         WRITE (LUPRI,'(A,2I5)') ' KHKTA/B  ', KHKTA, KHKTB
c         WRITE (LUPRI,'(A,2I5)') ' MULA/B   ', MULA, MULB
c         WRITE (LUPRI,'(A,F12.6)') ' HKAB   ', HKAB
c         WRITE (LUPRI,'(A, L5)') ' LDIAG    ', LDIAG
c         WRITE (LUPRI,'(A, L5)') ' ONECEN   ', ONECEN
      END IF
      FULMAT = .FALSE.
      ANTI   = .FALSE.
      NMATS = 3*NUCDEP*(MAXREP+1)
      IF (ONECEN) THEN
         ICLMX = 2
      ELSE
         ICLMX = 3
      ENDIF
      DO 10 ICL = 1,ICLMX
C
C        Determine factors to account for use of transl. invariance
C
         IF (ICL .EQ. 1) THEN
            JCENT = ICENTC
         ELSE IF (ICL .EQ. 2) THEN
            JCENT = ICENTA
         ELSE
            JCENT = ICENTB
         ENDIF
         MULJ = ISTBNU(JCENT)
C
C     Run over Cartesian directions
C
      DO 20 JCDIR = 1,3
         ISYTYJ = ISYMAX(JCDIR,1)
         IF (ICL .EQ. 3) THEN
            DO 50 I = 1, KHKTAB
               CAODER(I,JCDIR) = CAODER(I,JCDIR) + AAODER(I,JCDIR)
50          CONTINUE
         ENDIF
C
C     Run over irreps of the differentiation operator
C
      DO 30 IREPD = 0,MAXREP
         IF (IBTAND(MULJ,IBTXOR(IREPD,ISYTYJ)) .EQ. 0) THEN
            IF (ONECEN) THEN
               IF (ICL .EQ. 1) THEN
                  FAC = HKAB*PT(IBTAND(ISYTYJ,JSYMOP))
     *                      *PT(IBTAND(IREPD ,JSYMOP))
               ELSE
                  FAC = - HKAB
               ENDIF
            ELSE
               IF (ICL .EQ. 1) THEN
                  FAC = HKAB*PT(IBTAND(ISYTYJ,JSYMOP))
     *                      *PT(IBTAND(IREPD ,JSYMOP))
               ELSE IF (ICL .EQ. 2) THEN
                  FAC = HKAB
               ELSE
                  FAC = - HKAB*PT(IBTAND(ISYTYJ,ISYMOP))
     *                        *PT(IBTAND(IREPD, ISYMOP))
               END IF
            END IF
            IF (IPRINT .GT. 20) THEN
c               WRITE (LUPRI, '(/A,3I5)') ' ICL, JCDIR, IREPD ',
c     *                                     ICL, JCDIR, IREPD
c               WRITE (LUPRI, '(A,2I5)') ' MULJ, ISYTYJ ', MULJ, ISYTYJ
c               WRITE (LUPRI, '(A,F12.6)') ' FAC ', FAC
            END IF
            IMAT0 = NMATS
     *            + (IPTCNT(3*(JCENT-1)+JCDIR,IREPD,1)-1)*(MAXREP+1)+1
            IF (IREPD .EQ. 0) THEN
               IF (ICL .EQ. 2 .AND. .NOT.ONECEN) THEN
                  CALL SYM1S(AAODER(1,JCDIR),DUMMY,ISYMOP,MULA,MULB,
     *                       NHKTA,NHKTB,KHKTA,KHKTB,FAC,LDIAG,FULMAT,
     *                       THRS,IMAT0,IPRINT)
               ELSE
                  CALL SYM1S(CAODER(1,JCDIR),DUMMY,ISYMOP,MULA,MULB,
     *                       NHKTA,NHKTB,KHKTA,KHKTB,FAC,LDIAG,FULMAT,
     *                       THRS,IMAT0,IPRINT)
               END IF
            ELSE
               IF (ICL .EQ. 2 .AND. .NOT.ONECEN) THEN
                  CALL SYM1N(AAODER(1,JCDIR),DUMMY,IREPD,ISYMOP,MULA,
     *                       MULB,NHKTA,NHKTB,KHKTA,KHKTB,FAC,LDIAG,
     *                       FULMAT,ANTI,THRS,IMAT0,IPRINT)
               ELSE
                  CALL SYM1N(CAODER(1,JCDIR),DUMMY,IREPD,ISYMOP,MULA,
     *                       MULB,NHKTA,NHKTB,KHKTA,KHKTB,FAC,LDIAG,
     *                       FULMAT,ANTI,THRS,IMAT0,IPRINT)
               END IF
            END IF
         END IF
30    CONTINUE
20    CONTINUE
10    CONTINUE
      RETURN
      END
C  /* Deck sym1s */
      SUBROUTINE SYM1S(AO,SO,KB,MULA,MULB,NHKTA,NHKTB,KHKTA,KHKTB,
     *                 HKAB,LDIAG,FULMAT,THRESH,IMAT0,IPRINT)
C
C     Take block of distinct AO integral (derivatives) and
C     generate symmetrized contributions to SO integral
C     (derivatives) for the totally symmetric case
C                                          880407  PRT
C     Modified tuh 880819
C
#include <implicit.h>
#include <priunit.h>
#include <mxorb.h>
#include <mxcent.h>
#include <maxaqn.h>
      PARAMETER (LUTEMP = 48, IREPO = 0)
      LOGICAL LDIAG, FULMAT
      DIMENSION AO(*), SO(*)
#include <csym1.h>
#include <symmet.h>
#include <symind.h>
#include <ibtfun.h>
      IF (IPRINT .GT. 10) THEN
c         CALL HEADER('Subroutine SYM1S',-1)
c         WRITE (LUPRI, '(A, I5)') ' KB ', KB
c         WRITE (LUPRI, '(A,2I5)') ' NHKTA/B ', NHKTA, NHKTB
      END IF
C
C     Loop over all irreps in molecule
C
      DO 100 IREP = 0, MAXREP
         IF (FULMAT) THEN
            INDOFF = NPARSU(IREP + 1)
         ELSE
            IMAT = IMAT0 + IREP
         END IF
C
C        Loop over AOs which are of symmetry IREP in stabilizer MULA
C
         DO 200 NA = 1, KHKTA
         IF (IBTAND(MULA,IBTXOR(IREP,ISYMAO(NHKTA,NA))).EQ.0) THEN
            NAT = KHKTB*(NA - 1)
            IF (LDIAG) THEN
               KHKTBB = NA
            ELSE
               KHKTBB = KHKTB
            END IF
            DO 300 NB = 1,KHKTBB
            IF (IBTAND(MULB,IBTXOR(IREP,ISYMAO(NHKTB,NB))).EQ.0) THEN
C
C              Weight and parity factor
C
               FAC = HKAB*PT(IBTAND(KB,IBTXOR(IREP,ISYMAO(NHKTB,NB))))
C
C              Locate SO integrals to which AO's contribute
C
               INDA = INDFA(IREP + 1,NA)
               INDB = INDFB(IREP + 1,NB)
               INDM = MAX(INDA,INDB)
               RINT = FAC*AO(NAT+NB)
               IF (FULMAT) THEN
                  IND  = INDOFF + (INDM*(INDM - 3))/2 + INDA + INDB
                  SO(IND) = SO(IND) + RINT
               ELSE
                  IF (ABS(RINT) .GT. THRESH) THEN
                     IND  = (INDM*(INDM - 3))/2 + INDA + INDB
                     INDMAX = MAX(IND,INDMAX)
                     LABEL = IND*2**16 + IMAT
                     IF (IPRINT .GT. 20) THEN
c                       WRITE (LUPRI,'(A,F12.6,2I3,2I2,I4,I2,I5)')
c     *                     'SYM1S - NA/B,IREPA/B,IND,IREPO,IMAT',
c     *                     RINT, NA, NB, IREP, IREP, IND, IREPO,
c     *                     IMAT
                     END IF
                     IF (LENGTH .EQ. 600) THEN
c                        WRITE (LUTEMP) BUF, IBUF, LENGTH
c                        IF (IPRINT .GT. 5) WRITE (LUPRI,'(/A,I4,A)')
c     *                     ' Buffer of length',LENGTH,
c     *                     ' has been written in SYM1S.'
                        LENGTH = 0
                     ENDIF
                     LENGTH = LENGTH + 1
                     BUF (LENGTH) = RINT
                     IBUF(LENGTH) = LABEL
                  ENDIF
               ENDIF
            END IF
300         CONTINUE
         END IF
200      CONTINUE
100   CONTINUE
      RETURN
      END
C  /* Deck sym1n */
      SUBROUTINE SYM1N(AO,SO,IREPO,KB,MULA,MULB,NHKTA,NHKTB,
     *                 KHKTA,KHKTB,HKAB,LDIAG,FULMAT,ANTI,THRESH,
     *                 IMAT0,IPRINT)
C
C     Take block of distinct AO integral (derivatives) and
C     generate symmetrized contributions to SO integral
C     (derivatives) over non-symmetric operators
C                                          880408  PRT
C     Modified tuh 880819
C
#include <implicit.h>
#include <priunit.h>
#include <maxaqn.h>
#include <mxorb.h>
#include <mxcent.h>
      PARAMETER (LUTEMP = 48)
      LOGICAL LDIAG, FULMAT, ANTI
      DIMENSION AO(*), SO(*)
#include <csym1.h>
#include <symmet.h>
#include <symind.h>
#include <ibtfun.h>
      IF (IPRINT .GT. 10) THEN
c         CALL HEADER('Subroutine SYM1N',-1)
c         WRITE (LUPRI, '(A,2I5)') ' IREPO, KB ', IREPO, KB
c         WRITE (LUPRI, '(A,2I5)') ' NHKTA/B ', NHKTA, NHKTB
      END IF
C
C     Loop over irreps for first basis function - those for second
C     are obtained from operator symmetry IREPO
C
      DO 100 IREPA = 0, MAXREP
         IREPB = IBTXOR(IREPO,IREPA)
         IF (FULMAT) INDOFF = NPARNU(IREPO+1,MAX(IREPA,IREPB)+1)
         IF (ANTI .AND. (IREPA .LT. IREPB)) THEN
            FAB = - HKAB
         ELSE
            FAB = HKAB
         END IF
C
C        Loop over AOs which are of symmetry IREPA in stabilizer MULA
C
         DO 200 NA = 1, KHKTA
         IF (IBTAND(MULA,IBTXOR(IREPA,ISYMAO(NHKTA,NA))).EQ.0) THEN
            NAT = KHKTB*(NA - 1)
            IF (LDIAG) THEN
               KHKTBB = NA
            ELSE
               KHKTBB = KHKTB
            ENDIF
            DO 300 NB = 1,KHKTBB
            IF (NA.EQ.NB .AND. LDIAG .AND. IREPA .LT. IREPB) GOTO 300
            IF (IBTAND(MULB,IBTXOR(IREPB,ISYMAO(NHKTB,NB))).EQ.0) THEN
C
C              Weight and parity factor
C
               FAC = FAB*PT(IBTAND(KB,IBTXOR(IREPB,ISYMAO(NHKTB,NB))))
C
C              Locate SO integrals to which AOs contribute
C
               INDA = INDFA(IREPA + 1,NA)
               INDB = INDFB(IREPB + 1,NB)
               RINT = FAC*AO(NAT+NB)
               IF (FULMAT) THEN
                  IF (IREPA .GE. IREPB) THEN
                     IND  = INDOFF + NAOS(IREPB+1)*(INDA-1) + INDB
                  ELSE
                     IND  = INDOFF + NAOS(IREPA+1)*(INDB-1) + INDA
                  ENDIF
                  SO(IND) = SO(IND) + RINT
               ELSE
                  IF (ABS(RINT) .GT. THRESH) THEN
                     IF (IREPA .GE. IREPB) THEN
                        IND  = NAOS(IREPB+1)*(INDA-1) + INDB
                     ELSE
                        IND  = NAOS(IREPA+1)*(INDB-1) + INDA
                     ENDIF
                     INDMAX = MAX(IND,INDMAX)
                     LABEL  = IND*2**16 + IMAT0 + MAX(IREPA,IREPB)
                     IF (IPRINT .GT. 20) THEN
c                       WRITE (LUPRI,'(A,F12.6,2I3,2I2,I4,I2,I5)')
c     *                     'SYM1N - NA/B,IREPA/B,IND,IREPO,IMAT',
c     *                     RINT, NA, NB, IREPA, IREPB, IND, IREPO,
c     *                     IMAT0 + MAX(IREPA,IREPB)
                     END IF
                     IF (LENGTH .EQ. 600) THEN
c                       WRITE (LUTEMP) BUF, IBUF, LENGTH
c                        IF (IPRINT .GT. 5) WRITE (LUPRI,'(/A,I4,A)')
c     *                     ' Buffer of length',LENGTH,
c     *                     ' has been written in SYM1N.'
                        LENGTH = 0
                     ENDIF
                     LENGTH = LENGTH + 1
                     BUF(LENGTH)  = RINT
                     IBUF(LENGTH) = LABEL
                  ENDIF
               ENDIF
            END IF
300         CONTINUE
         END IF
200      CONTINUE
100   CONTINUE
      RETURN
      END
C  /* Deck sphrm1 */
      SUBROUTINE SPHRM1(CI,SPI,NTYPE,WORK,LWORK,PROPTY,IPRINT)
#include <implicit.h>
      LOGICAL PROPTY
      DIMENSION CI(KCKTAB,*), SPI(KCKTAB,*), WORK(LWORK)
#include <onecom.h>
      KTMP  = 1
      KTMQ  = KTMP + KCKTA*KCKTB
      KLAST = KTMQ + KCKTA*KHKTB
      IF (KLAST .GT. LWORK) CALL STOPIT('SPHRM1',' ',KLAST,LWORK)
      DO 100 I = 1, NTYPE
         CALL SPHRMX(CI(1,I),SPI(1,I),WORK(KTMP),WORK(KTMQ),PROPTY,
     &               IPRINT)
  100 CONTINUE
      RETURN
      END
C  /* Deck sphrmx */
      SUBROUTINE SPHRMX(CI,SPI,TMPINT,HALF,PROPTY,IPRINT)
C
C     Transform a block of Cartesian integrals to spherical harmonics
C
C                                          920511  PRT
C
#include <implicit.h>
#include <priunit.h>
#include <maxaqn.h>
#include <mxorb.h>
#include <mxcent.h>
      DIMENSION CI(*), SPI(*), HALF(*), TMPINT(*)
      LOGICAL PROPTY
#include <onecom.h>
#include <sphtrm.h>
#include <ibtfun.h>
C
      IF (IPRINT .GT. 10) THEN
c         WRITE(LUPRI,'(A,2I6)') ' KCKT? ', KCKTA, KCKTB
c         WRITE(LUPRI,'(A,2L6)') ' SPHR? ', SPHRA, SPHRB
c         WRITE(LUPRI,'(A,4I6)') ' KHKT? ', KHKTA, KHKTB
c         CALL HEADER('Cartesian integrals in SPHRMX',-1)
         IJ = 0
         DO 100 I = 1, KCKTA
            DO 110 J = 1,KCKTB
               IJ = IJ + 1
c               WRITE(LUPRI,'(2I4,D13.6)') I,J, CI(IJ)
 110        CONTINUE
 100     CONTINUE
      END IF
C
      IF (SPHRA .OR. SPHRB) THEN
C
C        Collect integrals to be transformed
C        ===================================
C
         ICOFF = 0
         DO 200 ICOMPA = 1,KCKTA
            DO 220 ICOMPB = 1,KCKTB
               ICOFF = ICOFF + 1
               TMPINT((ICOMPA-1)*KCKTB + ICOMPB) = CI(ICOFF)
  220       CONTINUE
  200    CONTINUE
C
C        Transform second index (B) if required
C        ======================================
C
         IF (SPHRB) THEN
            CALL MXM(CSP(ISPADR(NHKTB)),KHKTB,TMPINT,KCKTB,HALF,KCKTA)
         ELSE
            CALL dcopy_wr(KCKTA*KCKTB,TMPINT,1,HALF,1)
         END IF
C
C        Transpose half transformed integrals
C        ====================================
C
         DO 300 ICOMPA = 1,KCKTA
            DO 310 ICOMPB = 1,KHKTB
               TMPINT((ICOMPB-1)*KCKTA + ICOMPA)
     &               = HALF((ICOMPA-1)*KHKTB + ICOMPB)
  310       CONTINUE
  300    CONTINUE
C
C        Transform first index (A) if required
C        =====================================
C
         IF (SPHRA) THEN
            CALL MXM(CSP(ISPADR(NHKTA)),KHKTA,TMPINT,KCKTA,HALF,KHKTB)
         ELSE
            CALL dcopy_wr(KCKTA*KHKTB,TMPINT,1,HALF,1)
         END IF
C
C        Collect transformed integrals
C        =============================
C
         ISOFF = 0
         DO 400 ICOMPA = 1,KHKTA
            DO 410 ICOMPB = 1,KHKTB
               ISOFF = ISOFF + 1
               SPI(ISOFF) = HALF((ICOMPB-1)*KHKTA + ICOMPA)
  410        CONTINUE
  400     CONTINUE
      END IF
C
      IF (IPRINT .GT. 10) THEN
c         CALL HEADER('Spherical integrals in SPHRMX',-1)
         IJ = 0
         DO 500 I = 1, KHKTA
            DO 510 J = 1,KHKTB
               IJ = IJ + 1
c               WRITE(LUPRI,'(A,(2I4,D13.6))') '++',I,J,SPI(IJ)
 510        CONTINUE
 500     CONTINUE
      END IF
      RETURN
      END
