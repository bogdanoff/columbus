!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      subroutine wrtsif(x,lenght,itypeb,lstflg)
c
c    # SIFS writing routine for 1-e property integrals.
c    # Labels are generated and below threshold integrals
c    # will not be stored. (The original HERMIT will store
c    # all property integrals.
c
c
      common /forsif/ info, naos, nsym, ifmt1, ifmt2, nbfn,
     .  last1  
      common /more/ imore
      common /ccom/ thrs, nhtyp, docart
      double precision  thrs
      integer nhtyp
      logical docart
      integer n1maxx, l1recx, maxsym, nipv
      parameter (n1maxx = 3272, l1recx = 4096,
     &		 maxsym = 8, nipv = 2)
      integer info(6), naos(maxsym), nsym, ifmt1, ifmt2, nbfn
      integer n1max, luonel, itypea, itypeb, lenght, last
      double precision  x(n1maxx), values(n1maxx), buffer(l1recx)
      integer labels(nipv,n1maxx)
      integer icount, ileft, num, nrec, ierr, imore, iwrite
       double precision zero
      if (lstflg.eq.2 .and.last1.eq.2) then   
         lstflg=2
      else
         lstflg=1
      endif 
      zero=0.0d0 
      iwrite = 0
      itypea = 1
      luonel = 16
      last = 0
      n1max = info(3)
      icount = 0
      ncount = 0
      nbuf = 0
      if (imore.ge.4) then
	call bummer('wrtsif: order greater than 4',0,2)
      endif
      nbfn=0 
      do i=1,maxsym
       nbfn=nbfn+naos(i)
      enddo
      do i=1,nbfn
	do j=1,i
	  ncount = ncount + 1
	  if (abs(x(ncount)).gt.thrs) then
	    icount = icount + 1
	    labels(1,icount) = i
	    labels(2,icount) = j
	    values(icount) = x(ncount)
	      if ((icount).eq.n1max) then
	      num = icount
c              call sifew1(luonel, info, nipv, num, last, itypea,
c     &		itypeb, 0, values, labels, zero, 0,
c     &		buffer, nrec, ierr)
               icount=0
	      iwrite = iwrite + 1
c	      if (ierr.ne.0) then
c		 call bummer('wrtsif: from sifew1(), ierr=',ierr,2)
c	      endif
	      nbuf = nbuf + 1
	    endif
	  endif
        end do
      end do
      if (ncount .ne. lenght) then
         write(*,*) 'ncount,lenght=',ncount,lenght 
	call bummer('wrtsif: conflict ncount/lenght',0,1)
      endif
c     ileft = icount - nbuf*n1max
      ileft = icount
      if (ileft.gt.0) then
	last=1
	if ((lstflg.eq.2).and.(imore.eq.0)) last=2
	num = ileft
c        call sifew1(luonel, info, nipv, num, last, itypea,
c     &		itypeb, 0, values, labels, zero, 0,
c     &		buffer, nrec, ierr)
	iwrite = iwrite + 1
c        if (ierr.ne.0) then
c          call bummer('wrtsif: from sifew1(), ierr=',ierr,2)
c	endif
      endif
      last = 1
      if ((lstflg.eq.2).and.(imore.eq.0)) last=2
      if (last.eq.2 .and. iwrite.eq.0) then
c        call sifew1(luonel, info, nipv, num, last, itypea,
c     &		itypeb, 0, values, labels, zero, 0,
c     &		buffer, nrec, ierr)
      endif
      if (lstflg.eq.2 .and.last1.eq.2) then   
c        call sifsk1(luonel,info,ierr)
       endif 
      return
      end

