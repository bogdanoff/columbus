!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      subroutine outcorr(a1mat, a2mat, a3mat, phi, qcos, ediel, etot,
     &                  cosurf, iatsp, ar, xyz, mtype, smtype, natoms,
     &                  isym, nps, npspher, ifac)

c
c  outlyinig charge correction and final output
c  phi: in this subroutine: potential on the outer cavity!!!

      implicit real*8 (a-h,o-z)

      include 'cosinc.h'

c-------------------COMMON BLOCKS---------------------------------------
c
c I/O units
      include 'cosmovar.inc'
      include 'coss.inc'
c from outside
      dimension    xyz(3,natoms),mtype(natoms)
      dimension    cosurf(3,2*maxnps), iatsp(nps), ar(nps)
      dimension    phi(npspher), qcos(nps)
      dimension    a1mat(nps*(nps+1)/2)
      dimension    a2mat(npspher,nps)
      dimension    a3mat(npspher*(npspher+1)/2)
      character*8 smtype
      dimension smtype(natoms)
c
c alloc here
c

      dimension   phic(maxnps)
      dimension   qcoso(maxnps), qcosc(maxnps)

c     ifac 0: normal cosmo calculation f(epsilon) will be used
c     ifac 1: excited states f(n**2) will be used

      fdiel = fepsi
      if(ifac .eq. 1) fdiel = fn2

c------------------------READ A MATRICES--------------------------------
c     rea:d lower triangle of A1 matrix from file

      rewind(icosa)
      read(icosa) (a1mat(i),i=1,nps*(nps+1)/2)

c     read a2mat
      rewind(icosa2)

      do i=1,nps
          read(icosa2) (a2mat(j,i),j=1,npspher)
      enddo

c     read a3mat

      rewind(icosa3)
      do i=1,npspher
        read(icosa3) (a3mat(i+j*(j-1)/2),j=i,npspher)
      enddo

c-------------------POT. ON OUTER CAVITY--------------------------------
c     calc. pot on outer cavity: phi = phi_sol + A2 * qcos
c     phi_sol: pot. on outer cavity arising from solute (hold in phi)

      do i=1,nps

c qcos is scaled (fdiel) inside the cosmomodule
c at the end of the calculation we rescale it
c for the outlying charge correction
c          qcos(i) = qcos(i)/fdiel
          do j=1,npspher
              phi(j)=phi(j)+a2mat(j,i)*qcos(i)
          enddo
      enddo

c-----------CHARGE ON OUTER CAVITY-------------------------------------

c     Cholesky decomposition of A3
      call coschol1(a3mat,npspher,info)
      if (info.lt.0) then
         write(icolum,'(a)')
     &         'cosmo: a3mat not positive definite (case=4)'
         stop
      endif

c     calculate qcoso = - A3**(-1) * phi
      scal=-1.d0
      call coschol2(a3mat,qcoso,phi,npspher,scal)

c---------------CORRECT CHARGES-----------------------------------------
c     calc. corr. charges (qcosc)
      do i=1, npspher
          qcosc(i)=qcos(i)+qcoso(i)
      enddo
      do i=npspher+1, nps
          qcosc(i)=qcos(i)
      enddo

c     calculate total screening charge
      qsum  = 0.d0
      qsumo = 0.d0
      do i = 1, nps
          qsum  = qsum  + qcos(i)
      enddo
      do i = 1, npspher
          qsumo = qsumo + qcoso(i)
      enddo

c-----------------CORRECTED POTENTIALS AND ENERGY-----------------------
c     calculate corrected potentials (phic) and energy correction (de)
c     phic = - A1 * qcosc (from  0 = phic + A1 * qcosc)
c     de  =  (0.5d0*fdiel* scalar(qcosc,phic,nps) - ediel)
c

      de = 0.d0
      do i=1,nps
          phic(i) = 0.d0
      enddo

      ia1=0
      do i=1,nps
          do j=1,i-1
              ia1=ia1+1
              phic(i) = phic(i) - qcosc(j) * a1mat(ia1)
              phic(j) = phic(j) - qcosc(i) * a1mat(ia1)
              de = de - qcosc(i) * qcosc(j) * a1mat(ia1)
          enddo
          ia1=ia1+1
          phic(i) = phic(i) - qcosc(i) * a1mat(ia1)
          de = de - 0.5d0 * qcosc(i)**2 * a1mat(ia1)
      enddo
c      de  = 0.5d0 * (fdiel * de - ediel)
       de  =  (fdiel * de - ediel)
c------FINAL OUTPUT, WRITE COSMO FILE-----------------------------------
c version: version info from include file cosinc.h

      call writecosmo(icosout, qsum, qsumo, etot, ediel, de, xyz,
     &               mtype, smtype, natoms, iatsp, qcosc, phic, ar,
     &               cosurf, version, isym, nps, npspher, ifac )

c----------------------------------------------------------------------c

      return

      end

