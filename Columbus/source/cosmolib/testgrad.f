!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      program testgrad

c
c Testprogram for gradient part
c

      implicit real*8 (a-h,o-z)
      include 'cosinc.h'

      parameter(natoms=3)
      parameter(isym=0)

      include 'cosmovar.inc'
      include 'coss.inc'

      dimension xyz(3,natoms)
      dimension cosurf(3,2*maxnps)
      dimension dcos(3,maxat)
      dimension qcos(maxnps)
      dimension mtype(natoms)
      character*80 zeile
      character*2 symb
      icosout=1
      icossu=2
      icolum=6
      icosin=4
      ico=4711
      open(icosout,file='out.cosmo', form='formatted',
     &     status='unknown',err=1000)
      open(icosin,file='cosmo.inp', form='formatted',
     &     status='unknown',err=1000)
      open(icossu,file='sude',status='unknown',err=1000)
      open(ico,file='coord', form='formatted',
     &     status='unknown',err=1000)

      read(ico,'(a)') zeile
      do i=1,natoms
        read(ico,'(3(f20.14,2x),a)') xyz(1,i), xyz(2,i), xyz(3,i), symb
      enddo
      close(ico)
      mtype(1) =8
      mtype(2) =1
      mtype(3) =1


      call cosmo(7, xyz, mtype, natoms, isym, cosurf,
     &                 nps, npspher, phi, qcos, ediel, elast,
     &                 a1mat, a2mat, a3mat, dcos)

c     print gradient dcos
      write(icolum,'(a)')'Grad. contrib. q*A*q inc. surface deriv.'
        do i=1,natoms,3
           write(6,'(a,3d14.7)') 'd/dx ',(dcos(1,j),j=1,3)
           write(6,'(a,3d14.7)') 'd/dy ',(dcos(2,j),j=1,3)
           write(6,'(a,3d14.7)') 'd/dz ',(dcos(3,j),j=1,3)
        enddo
c print qcos
       write(icolum,'(a)')'  cosurf x->z           qcos:'
       do i=1,nps
          write(icolum,'(4f14.7)') cosurf(1,i),cosurf(2,i),
     &         cosurf(3,i), qcos(i)
       enddo
      write(icolum,*)'nps: ',nps,'fepsi:',fepsi

      stop
 1000 write(6,*) 'Problems with file'
      end
