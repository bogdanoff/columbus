!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
       subroutine coschol2(a,x,y,n,scal)

c written by A. Klamt
c modifed version !

c this routine solves the linear system Cx = y based on
c Cholesky factorization
c input:   a =  upper triangle of Cholesky matrix
c input:   y =  vector of length n
c output:  x =  vector of length n

c    modified for inclusion of a scaling factor scal, i.e.  a*x = scal*y

       implicit none  
       real*8 a(*), x(*), y(*), summe, scal
       integer n,i,k,indk,indi

       indk=0
       do k = 1, n
         summe=scal*y(k)
         do i = k-1, 1,-1
           summe=summe-a(i+indk)*x(i)
         enddo
         x(k)=summe*a(k+indk)
         indk=indk+k
       enddo

       indk=n*(n-1)/2
       do k = n,1,-1
         summe=x(k)
         indi=indk
         do i = k+1, n
           indi=indi+i-1
           summe=summe-a(k+indi)*x(i)
         enddo
         x(k)=summe*a(k+indk)
         indk=indk-k+1
       enddo

       end
