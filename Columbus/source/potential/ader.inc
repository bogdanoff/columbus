!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
C
C     Common variables
C
      INTEGER             IA0000,      IA000X,      IA000Y,      IA000Z
      INTEGER             IA00XX,      IA00XY,      IA00XZ,      IA00YY
      INTEGER             IA00YZ,      IA00ZZ,      IA0X00,      IA0X0X
      INTEGER             IA0X0Y,      IA0X0Z,      IA0Y00,      IA0Y0X
      INTEGER             IA0Y0Y,      IA0Y0Z,      IA0Z00,      IA0Z0X
      INTEGER             IA0Z0Y,      IA0Z0Z,      IAXX00,      IAXY00
      INTEGER             IAXZ00,      IAYY00,      IAYZ00,      IAZZ00
C
      COMMON / ADER   /   IA0000,      IA0X00,      IA0Y00,      IA0Z00
      COMMON / ADER   /   IAXX00,      IAXY00,      IAXZ00,      IAYY00
      COMMON / ADER   /   IAYZ00,      IAZZ00,      IA000X,      IA000Y
      COMMON / ADER   /   IA000Z,      IA00XX,      IA00XY,      IA00XZ
      COMMON / ADER   /   IA00YY,      IA00YZ,      IA00ZZ,      IA0X0X
      COMMON / ADER   /   IA0X0Y,      IA0X0Z,      IA0Y0X,      IA0Y0Y
      COMMON / ADER   /   IA0Y0Z,      IA0Z0X,      IA0Z0Y,      IA0Z0Z
C
