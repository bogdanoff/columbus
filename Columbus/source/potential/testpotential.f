!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
C
       program testpotenrg
       implicit none
       character*20 elpot,gau
       integer natoms_gau,natoms_elpot,n_gau,n_elpot
       real*8 initpt_gau(3),initpt_elpot(3)
       real*8  mxd,mxr,maxreldiff,maxdiff,vec_gau(4),vec_elpot(4)
       real*8 maxpot,lmxd,lmxr
       real*8, allocatable :: gau_pot(:),elpot_pot(:)
        integer  npts_gau(3),npts_elpot(3)
       integer i,j,l,k
       logical ok,isthere
       integer ldens


        write(6,*) 'Name of the potential cube file'
        read(5,*) elpot
        write(6,*) 'Name of the gaussion potential cube file'
        read(5,*) gau
        write(6,*) 'DENSITY (1) OR ELPOT(0) '
        read(5,*) ldens
        inquire(file=trim(elpot),exist=isthere)
        if (.not. isthere) call bummer('elpot file missing',0,2)
        inquire(exist=isthere,file=trim(gau))
        if (.not. isthere) call bummer('gaupot file missing',0,2)

        open(unit=10,file=trim(elpot),form='formatted')
        open(unit=11,file=trim(gau),form='formatted')
        read(10,*)
        read(10,*)
        read(11,*)
        read(11,*)
        read(11,*) natoms_gau, (initpt_gau(i),i=1,3) 
        read(10,*) natoms_elpot, (initpt_elpot(i),i=1,3) 
         if (natoms_gau.ne.natoms_elpot) then
           write(6,*) 'inconsistent number of atoms:',
     .                 natoms_gau,natoms_elpot
           stop 'natoms'
         endif
         ok=.true.
         do i=1,3
           ok=abs(initpt_gau(i)-initpt_elpot(i)).lt.1.d-6 .and. ok
         enddo
         if (.not. ok) then
 110    format('inconsistent initial points:'/,3f10.6/,' vs '/,3f10.6)
            write(6,110) (initpt_gau(i),i=1,3),(initpt_elpot(i),i=1,3)
c           stop
         endif

         do j=1,3

         read(11,*) npts_gau(j), (vec_gau(i),i=1,3)
         read(10,*) npts_elpot(j), (vec_elpot(i),i=1,3)
         ok=.true.
         do i=1,3
           ok=abs(vec_gau(i)-vec_elpot(i)).lt.1.d-6 .and. ok
         enddo
         ok=ok.and.(npts_gau(j).eq.npts_elpot(j))
         if (.not. ok) then
  111    format('inconsistent vector ',i1,' definition:/',i4,3f10.6/,
     .        ' vs '/,i4,3f10.6)
            write(6,111) j,
     .               npts_gau(j),(vec_gau(i),i=1,3), 
     .               npts_elpot(j),(vec_elpot(i),i=1,3)
c           stop
         endif
         enddo
         do j=1,natoms_gau
         read(11,*) n_gau, (vec_gau(i),i=1,4)
         read(10,*) n_elpot, (vec_elpot(i),i=1,4)
         ok=.true.
         do i=1,4
           ok=abs(vec_gau(i)-vec_elpot(i)).lt.1.d-6 .and. ok
         enddo
         ok=ok.and.(n_gau.eq.n_elpot)
         if (.not. ok) then
  112    format('inconsistent atom ',i1,' position:'/,i4,4f10.6/,
     .        ' vs '/,i4,4f10.6)
            write(6,112) j,
     .               n_gau,(vec_gau(i),i=1,4), 
     .               n_elpot,(vec_elpot(i),i=1,4)
c           stop
         endif
         enddo
          open (file='testpot.data',unit=8)
         allocate(gau_pot(max(npts_gau(1),npts_gau(2),npts_gau(3)) ))
         allocate(elpot_pot(max(npts_gau(1),npts_gau(2),npts_gau(3))))
         maxdiff=0.0d0
         maxreldiff=0.0d0
         maxpot=0.0d0
         do i=1,npts_gau(1)
          do j=1,npts_gau(2)
             read(11,*) (gau_pot(l),l=1,npts_gau(3))
             read(10,*) (elpot_pot(l),l=1,npts_gau(3))
             mxr=0.0d0
             mxd=0.0d0
             if (ldens.eq.1) then
             do l=1,npts_gau(3)
               lmxd=(abs(gau_pot(l))-abs(elpot_pot(l)))
               mxd=max(mxd,lmxd)
               lmxr= lmxd/abs(gau_pot(l))
               mxr=max(mxr,lmxr)
               maxpot=max(maxpot,abs(gau_pot(l)))
             if (lmxr.gt.0.01 .or. lmxd.gt.1.d-6) then
               write(6,13)  lmxr,lmxd,i,j
             endif
             enddo  
             else
             do l=1,npts_gau(3)
               lmxd=(abs(gau_pot(l)-elpot_pot(l)))
               maxpot=max(maxpot,abs(gau_pot(l)))
               mxd=max(mxd,lmxd)
               lmxr= lmxd/abs(gau_pot(l))
               mxr=max(mxr,lmxr)
             if (lmxr.gt.0.01 .or. lmxd.gt.1.d-6) then
               write(8,13)  lmxr,lmxd,i,j
             endif
             enddo  
             endif
 13          format('difference:rel=',f12.8,' absolute=',f12.8,
     .       'line=',2i4)
             maxreldiff=max(maxreldiff,mxr)
             maxdiff=max(maxdiff,mxd)
          enddo
         enddo
         if (ldens.eq.0) then
         write(6,*) 'maximum potential difference=',maxdiff
         write(6,*) 'maximum relative potential difference=',maxreldiff
         write(6,*) 'maximum potential=',maxpot
         else
         write(6,*) 'maximum density difference=',maxdiff
         write(6,*) 'maximum relative density difference=',maxreldiff
         write(6,*) 'maximum densiy=',maxpot
         endif
         close(10)
         close(11)
         close(8)
 100    format(6(1x,es13.5))
         stop
         end
       
