!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      program trci2den

      IMPLICIT NONE
      include "../colib/getlcl.h"

c
c
c*********************************************************************
c
c  allocate space and call the driver routine.
c
c  ifirst = first usable space in the the real*8 work array a(*).
c  lcore  = length of workspace array in working precision.
c  mem1   = additional machine-dependent memory allocation variable.
c  a(*)   = workspace array. a(ifirst : ifirst+lcore-1) is useable.
c
      integer ifirst, lcore
c
c     # mem1 and a(*) should be declared below within mdc blocks.
c
c     # local...
c     # lcored = default value for lcore.
c     # ierr   = error return code.
c
      integer lcored, ierr
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
c     # sets up any communications necessary for parallel execution,
c     # and does nothing in the sequential case.
c

c
c
c     # use standard f90 memory allocation.
c
      integer mem1
      parameter ( lcored = 2 500 000 )
      real*8, allocatable :: a(:)
c
      call getlcl( lcored, lcore, ierr )
      if ( ierr .ne. 0 ) then
         call bummer('trci2den: from getlcl: ierr=',ierr,faterr)
      endif
c
      allocate( a(lcore),stat=ierr )
      if ( ierr .ne. 0 ) then
         call bummer('trci2den: allocate failed ierr=',ierr,faterr)
      endif

      ifirst = 1
      mem1   = 0
c
c
c     # lcore, mem1, and ifirst should now be defined.
c     # the values of lcore, mem1, and ifirst are passed to
c     # driver() to be printed.
c     # since these arguments are passed by expression, they
c     # must not be modified within driver().
c
c
      call driver( a(ifirst), (lcore), (mem1), (ifirst) )
c
c
c     # return from driver() implies successful execution.
c
c     # message-passing cleanup: stub if not in parallel
c
      call bummer('normal termination',0,3)
      stop 'end of trci2den'
      end
c
c****************************************************************
c
      subroutine driver( core, lcore, mem1, ifirst)
c
      implicit none 
c
c  ##  parameter & common block section
c
      integer nmotx
      parameter (nmotx=510)
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      real*8 onem,half
      parameter (onem=-1.0d+00,half=0.5d+00)
c
      integer maxvec
      parameter (maxvec=16)
c
      integer   maxsym
      parameter(maxsym=8)
c
      integer   ninfmx
      parameter(ninfmx=10)
ctm   this is strictly wrong, because n1edbf depends on
ctm   8 versus 16 bit packing!
ctm   However, since n1edbf (8bit)> n1edbf(16bit) it will
ctm   still work.

      integer lend1e, n1edbf
      parameter (lend1e=2047,n1edbf=1636)
c
      integer mctype,mapivm,mapmi,mapma,mapmd,mapmv,mapml,
     & mapmm,symi,symv,symm,syma,symd,mcres
      common/cmomap/mctype(nmotx),mapivm(nmotx),mapmi(nmotx),
     & mapma(nmotx),mapmd(nmotx),mapmv(nmotx),mapml(0:nmotx),
     & mapmm(nmotx),
     & symi(nmotx),symv(nmotx),symm(nmotx),
     & syma(nmotx),symd(nmotx),mcres(nmotx)
c
c     # symmetry labels
      character*4 labels(8)
      common /slabel/labels
c
      integer   nfilmx
      parameter(nfilmx=55)
      integer         nunits
      common /cfiles/ nunits(nfilmx)
      character*60    fname
      common /cfname/ fname(nfilmx)
      integer nslist,nlist,den1st1,den1st2,den1sum,den2st1,
     &        den2st2,den2sum,indfl,den1_tr,den2_tr
      equivalence (nunits(1),nlist)
      equivalence (nunits(3),nslist)
      equivalence(nunits(10),den1st1)
      equivalence(nunits(11),den1st2)
      equivalence(nunits(12),den1sum)
      equivalence(nunits(13),den2st1)
      equivalence(nunits(14),den2st2)
      equivalence(nunits(15),den2sum)
      equivalence(nunits(16),indfl)
      equivalence(nunits(17),den1_tr)
      equivalence(nunits(18),den2_tr)
c
      integer prtlvl
      common /indata/  prtlvl
c
c  ##  integer section
c
      integer bperacc
      integer cpt(30)
      integer i,inroot1,inroot2,iroot,isweep,ifirst,infocd(ninfmx),
     &        isym,idummy,ietype,ierr,irm1(8),irm2(8)
      integer kntin(36)
      integer lcore,lenm1e,lenm2e,last_st1,last_st2,last_sum
      integer mem1, mapm(nmotx)
      integer ncsf,nel1st,nel,nroot,ntitld,nsym,nmot,nmpsy(maxsym),
     &        n1embf,n2embf,nndx(nmotx),nnmt,nm,nsm(maxsym),
     &        nnsm(maxsym),nbuf_st1,nbuf_st2,nbuf_sum,nenrgy,nmap
      integer outroot
      integer recpervec,roots(maxvec)
      integer ssym(nmotx)
      integer vecspace
c
c  ##  real*8 sectionc
c
      real*8 energy
      real*8 core(*)
c
c  ##  character section
c
      character*80 title(20)
      character*8 bfnlab(nmotx)
c
c  ##  external section
c
      integer  atebyt,forbyt
      external atebyt,forbyt
c

c------------------------------------------------------------------
c  initialize nndx(*)
c
      nndx(1)=0
      do i=2,nmotx
      nndx(i)=nndx(i-1)+(i-1)
      enddo ! end of: do i=2,nmotx
c
c     open the listing files.
c
      open( unit = nlist, file = fname(1), status = 'unknown')
      open( unit = nslist, file= fname(3), status = 'unknown')

      call who2c( 'trci2den', nlist )
      call datain
c     # open the necessary info containing files
c
c  ##
c  ##  read in the 1e-desity for: SUM
c  ##
c     # read the header information from the ci one-particle
c     # density matrix file.
c
      open(unit=den1sum,file=fname(12),status='unknown',
     & form='unformatted')
      call prtfn(den1sum,5,'ci 1-particle density file for: SUM',
     & nlist)
c
      call izero_wr(maxsym,nmpsy,1)
      call rdhcid(
     & nlist,    den1sum,    infocd,   ntitld,
     & title,    nsym,       nmot,     nmpsy)
c
      lenm1e = infocd(2)
      n1embf = infocd(3)
      lenm2e = infocd(4)
      n2embf = infocd(5)
c
c  ##  write header information to the transition density output file
      write(nlist,*)
      write(nlist,*)' Openning file:',fname(17),'unit=',den1_tr,
     & 'for 1-e transition density matrix'
      open( unit = den1_tr,file = fname(17), status = 'unknown' ,
     & form = 'unformatted' )
      nenrgy=1
      nmap=0
      ietype=-1
      energy=10.0
      do i = 1,nmotx
       write (bfnlab(i),fmt='(a5,i3.3)') 'tout:',i
       mapm(i) = i
      enddo
      call sifwh(
     & den1_tr,   ntitld,   nsym,    nmot,
     & ninfmx,    nenrgy,   nmap,    title,
     & nmpsy,     labels,   infocd,  bfnlab,
     & ietype,    energy,   idummy,  idummy,
     & ierr)
      if (ierr.gt.0) call bummer("error in sifwh",0,faterr)
c
      nnmt=0
      nmot=0
        do isym=1,maxsym
        nm=nmpsy(isym)
        nsm(isym)=nmot
        nnsm(isym)=nnmt
        nnmt = nnmt + nndx(nm+1)
        nmot=nmot+nm
        enddo !end of: do isym=1,nsym
        do i=1,nmotx
        mapmm(i)=i
        enddo ! end of: do i=1,nmotx
c
c   # 1:cid1_sum,2:buf,3:val,4:ilab,5:scr
      cpt(1)=1
      cpt(2)=cpt(1)+atebyt(nnmt)
      cpt(3)=cpt(2)+atebyt(lenm1e)
      cpt(4)=cpt(3)+atebyt(n1embf)
      cpt(5)=cpt(4)+forbyt(2*n1embf)
      if (cpt(5).gt.lcore) call bummer(
     & "Not enough memory allocated",cpt(5),faterr)
c
      call wzero(nnmt,core(cpt(1)),1)
c     # read in the ci one-particle density matrix.
      call rdden1(
     & den1sum,       lenm1e,        n1embf,        infocd,
     & core(cpt(2)),  core(cpt(4)),  core(cpt(3)),  core(cpt(1)),
     & nndx,          nnmt,          nsm,           ssym,
     & nnsm,          mapmm,         nsym,          nmpsy,
     & kntin )
c
       if (prtlvl.ge.1) then
        call plblks('ci d1 matrix:sum',core(cpt(1)),nsym,nmpsy,
     &   '  mo',1,nlist)
       endif ! end of: if (prtlvl.ge.1) then
c
c  ##
c  ##  read in the 1e-desity for: STATE1
c  ##
      open(unit=den1st1,file=fname(10),status='unknown',
     & form='unformatted')
      call prtfn(den1st1,5,'ci 1-particle density file for: state1',
     & nlist)
c
      call rdhcid(nlist,den1st1,infocd,ntitld,title,nsym,nmot,
     & nmpsy)
c
c   # 1:cid1_sum,3:cid1_st1,4:buf,5:val,6:ilab,7:scr
      cpt(3)=cpt(2)+atebyt(nnmt)
      cpt(4)=cpt(3)+atebyt(lenm1e)
      cpt(5)=cpt(4)+atebyt(n1embf)
      cpt(6)=cpt(5)+forbyt(2*n1embf)
      if (cpt(6).gt.lcore) call bummer(
     & "Not enough memory allocated",cpt(6),faterr)
c
c     # read in the ci one-particle density matrix.
      call wzero(nnmt,core(cpt(2)),1)
      call rdden1(
     & den1st1,       lenm1e,        n1embf,        infocd,
     & core(cpt(3)),  core(cpt(5)),  core(cpt(4)),  core(cpt(2)),
     & nndx,          nnmt,          nsm,           ssym,
     & nnsm,          mapmm,         nsym,          nmpsy,
     & kntin )
c
       if (prtlvl.ge.1) then
        call plblks('ci d1 matrix:st1',core(cpt(2)),nsym,nmpsy,
     &   '  mo',1,nlist)
       endif ! end of: if (prtlvl.ge.1) then
c
      call daxpy_wr(nnmt,onem,core(cpt(2)),1,core(cpt(1)),1)
c
c  ##
c  ##  read in the 1e-desity for: STATE2
c  ##
      open(unit=den1st2,file=fname(11),status='unknown',
     & form='unformatted')
      call prtfn(den1st2,5,'ci 1-particle density file for state 1',
     & nlist)
c
      call izero_wr(maxsym,nmpsy,1)
      call rdhcid(nlist,den1st2,infocd,ntitld,title,nsym,nmot,
     & nmpsy)
c
c     # read in the ci one-particle density matrix.
      call wzero(nnmt,core(cpt(2)),1)
      call rdden1(
     & den1st2,       lenm1e,        n1embf,        infocd,
     & core(cpt(3)),  core(cpt(5)),  core(cpt(4)),  core(cpt(2)),
     & nndx,          nnmt,          nsm,           ssym,
     & nnsm,          mapmm,         nsym,          nmpsy,
     & kntin )
c
       if (prtlvl.ge.1) then
        call plblks('ci d1 matrix:st2',core(cpt(2)),nsym,nmpsy,
     &   '  mo',1,nlist)
       endif ! end of: if (prtlvl.ge.1) then
c
      call daxpy_wr(nnmt,onem,core(cpt(2)),1,core(cpt(1)),1)
      call dscal_wr(nnmt,half,core(cpt(1)),1)
c
      call plblks('ci d1 matrix:0.5(sum-st1-st2)',core(cpt(1)),
     & nsym,nmpsy,'  mo',1,nlist)
c
c  ##
c  ##  write out the 1-el transition density
c
c   | 1:d1_tr,2:buf,3:labbuf,4:valbuf
      cpt(3)=cpt(2)+atebyt(lend1e)
      cpt(4)=cpt(3)+forbyt(2*n1edbf)
      cpt(5)=cpt(4)+atebyt(lend1e)
      if (cpt(5).gt.lcore) call bummer(
     & "Not enough memory allocated",cpt(5),faterr)
      irm1(1) = 1
      irm2(1) = nmpsy(1)
      do i = 2,nsym
         irm1(i) = irm1(i-1) + nmpsy(i-1)
         irm2(i) = irm2(i-1) + nmpsy(i)
      enddo

      call wtden1(
     & den1_tr,       infocd,        lend1e,        n1edbf,
     & core(cpt(2)),  core(cpt(3)),  core(cpt(4)),  core(cpt(1)),
     & nsym,          irm1,          irm2,          mapm)
c
c  ##
c  ##  now calculate the 2-e transition density
c
c   | 1:d2_st1,2:lab_st1,3:buf,4:d2_st2,5:lab_st2,6:d2_sum,7:lab_sum
c   | 8:d2_tr,9:lab_tr
      cpt(1)=1
      cpt(2)=cpt(1)+atebyt(n2embf)
      cpt(3)=cpt(2)+forbyt(n2embf*4)
      cpt(4)=cpt(3)+atebyt(lenm2e)
      cpt(5)=cpt(4)+atebyt(n2embf)
      cpt(6)=cpt(5)+forbyt(n2embf*4)
      cpt(7)=cpt(6)+atebyt(n2embf)
      cpt(8)=cpt(7)+forbyt(n2embf*4)
      cpt(9)=cpt(8)+atebyt(n2embf)
      cpt(10)=cpt(9)+forbyt(n2embf*4)
      if (cpt(10).gt.lcore) call bummer(
     & "Not enough memory allocated",cpt(10),faterr)
c
      call read_d2(
     & core(cpt(1)),   core(cpt(2)),   core(cpt(3)),   core(cpt(4)),
     & core(cpt(5)),   core(cpt(6)),   core(cpt(7)),   core(cpt(8)),
     & core(cpt(9)),   infocd)
c
      call bummer('normal termination',0,3)
      end
c
c#######################################################################
c
      subroutine read_d2(
     & d2_st1,    lab_st1,   buffer,     d2_st2,
     & lab_st2,   d2_sum,    lab_sum,    valbuf,
     & labbuf,    infocd)
c
       implicit none  
c     TWO ELECTRON PART
c
c
c  ##  parameter and common block section
c
      real*8 zero,half,halfm
      parameter (zero=0.0d+00,half=0.5d+00,halfm=-0.5d+00)
c
      integer   ninfmx
      parameter(ninfmx=10)
c
      integer nipv
      parameter (nipv=4)
c
      real*8 thres
      parameter(thres = 1.0d-11)
c
      integer   nfilmx
      parameter(nfilmx=55)
      integer         nunits
      common /cfiles/ nunits(nfilmx)
      character*60    fname
      common /cfname/ fname(nfilmx)
      integer nlist,den1st1,den1st2,den1sum,den2st1,den2st2,
     & den2sum,indfl,den1_tr,den2_tr
      equivalence (nunits(1),nlist)
      equivalence(nunits(10),den1st1)
      equivalence(nunits(11),den1st2)
      equivalence(nunits(12),den1sum)
      equivalence(nunits(13),den2st1)
      equivalence(nunits(14),den2st2)
      equivalence(nunits(15),den2sum)
      equivalence(nunits(16),indfl)
      equivalence(nunits(17),den1_tr)
      equivalence(nunits(18),den2_tr)
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
       integer*8  m16,m32,m48,mand
      parameter(m16=-16,m32=-32,m48=-48,mand=2**16-1)
c
c  ##  integer section
c
      integer infocd(ninfmx),iucid2,ierr,icount_st1,icount_st2,
     &        icount_sum,ind(4),i,icount_tr,iout
      integer j
      integer lab_st1(nipv,*),lab_st2(nipv,*),lab_sum(nipv,*),
     &        last_st1,last_st2,last_sum,labbuf(nipv,*)
      integer nbuf_st1,nbuf_st2,nbuf_sum
      integer*8 indbuffer(10000),indbuf
      integer ii
c
c  ##  real*8 section
c
      real*8 buffer
      real*8 d2_st1(*),d2_st2(*),d2_sum(*),d2tr
      real*8 valbuf(*)
c
c  ##  logical section
c
      logical lreadst1,lreadst2,lreadsum,lend_st1,lend_st2,lend_sum
c
c-----------------------------------------------------------------------
c
c  ##  open the index file for the 2-e density matrix
      open(unit=indfl, file=fname(16), status='old',form='unformatted')
c  ##
c  ##  open the 2-el transition desity file
      call sifo2f(den1_tr, den2_tr, fname(18), infocd, iucid2, ierr)
      if (ierr .ne. 0) then
         call bummer('error returned by sifo2f, ierr=',ierr,faterr)
      endif
c  ##
c  ##  open the 2-desity for: ST1
      call sifo2f(den1st1, den2st1, fname(13), infocd, iucid2, ierr)
      if (ierr .ne. 0) then
         call bummer('ci d1 matrix:ST1, from sifo2f, ierr=',
     &    ierr,faterr)
      endif
c  ##
c  ##  open the 2-desity for: ST2
      call sifo2f(den1st2, den2st2, fname(14), infocd, iucid2, ierr)
      if (ierr .ne. 0) then
         call bummer('ci d1 matrix:ST2, from sifo2f, ierr=',
     &    ierr,faterr)
      endif
c  ##
c  ##  open the 2-desity for: SUM
      call sifo2f(den1sum, den2sum, fname(15), infocd, iucid2, ierr)
      if (ierr .ne. 0) then
         call bummer('ci d1 matrix:SUM, from sifo2f, ierr=',
     &    ierr,faterr)
      endif
c
      lend_st1=.false.
      lend_st2=.false.
      lend_sum=.false.
      lreadst1=.true.
      lreadst2=.true.
      lreadsum=.true.
      icount_st1=1
      icount_st2=1
      icount_sum=1
      icount_tr=1
      iout=0
100   continue
c     read(indfl,'(4i3)',end=1000)(ind(i),i=1,4)
      read(indfl,end=1000) indbuffer
      do ii=1,10000
       if (indbuffer(ii).eq.0) goto 1000
       indbuf=indbuffer(ii)
       ind(4)=int(iand(indbuf,mand),4)
       ind(3)=int(iand(ishft(indbuf,m16),mand),4)
       ind(2)=int(iand(ishft(indbuf,m32),mand),4)
       ind(1)=int(iand(ishft(indbuf,m48),mand),4)
c       write(6,'(4i4)') (ind(i),i=1,4)
      d2tr=zero
      if (lreadst1) then
       call rd_d2bl(
     &  d2_st1,     lab_st1,   nbuf_st1,   buffer,
     &  last_st1,   den2st1,   infocd)
       lreadst1=.false.
      endif ! end of:if (lreadst1) then
c
      if (lreadst2) then
       call rd_d2bl(
     &  d2_st2,     lab_st2,   nbuf_st2,   buffer,
     &  last_st2,   den2st2,   infocd)
       lreadst2=.false.
      endif ! end of:if (lreadst2) then
c
      if (lreadsum) then
       call rd_d2bl(
     &  d2_sum,     lab_sum,   nbuf_sum,   buffer,
     &  last_sum,   den2sum,   infocd)
       lreadsum=.false.
      endif ! end of:if (lreadsum) then
c
c     if (ind(1).eq.4 .and. ind(2).eq.4. .and. ind(3).eq.2 .and.
c    & ind(4).eq.2) then
c     write(*,*)'ind:',ind,icount_st1
c     write(*,*)'d2_st1(icount_st1)',d2_st1(icount_st1)
c     write(*,*)'d2_st2(icount_st2)',d2_st2(icount_st2)
c     write(*,*)'d2_sum(icount_sum)',d2_sum(icount_sum)
c     write(*,*)'readlabel',lab_st1(1,icount_st1),lab_st1(2,icount_st1),
c    & lab_st1(3,icount_st1),lab_st1(4,icount_st1)
c     endif
c
      if (.not. lend_st1) 
     &call contribution(
     & ind,   d2_st1(icount_st1),   lab_st1(1,icount_st1),  icount_st1,
     & d2tr,  halfm)
c
      if (.not. lend_st2)
     &call contribution(
     & ind,   d2_st2(icount_st2),   lab_st2(1,icount_st2),  icount_st2,
     & d2tr,  halfm)
c
      if (.not. lend_sum)
     &call contribution(
     & ind,   d2_sum(icount_sum),   lab_sum(1,icount_sum),  icount_sum,
     & d2tr,  half)
c
      if(dabs(d2tr).gt.thres) then
       call  d2tr_out(
     & d2tr,     ind,    labbuf,   valbuf,
     & infocd,   iout,   .false.)
      endif
c
c     write(*,*)'sum=',icount_tr,ind,d2tr
c     icount_tr=icount_tr+1
c
       if (icount_st1.gt.nbuf_st1) then
        lreadst1=.true.
        icount_st1=1
        if (last_st1.eq.2) lend_st1=.true.
       endif! end of: if (icount_st1.eq.nbuf_st1) then
c
       if (icount_st2.gt.nbuf_st2) then
        lreadst2=.true.
        icount_st2=1
        if (last_st2.eq.2) lend_st2=.true.
       endif! end of: if (icount_st2.eq.nbuf_st2) then
c
       if (icount_sum.gt.nbuf_sum) then
        lreadsum=.true.
        icount_sum=1
        if (last_sum.eq.2) lend_sum=.true.
       endif ! end of: if (icount_sum.eq.nbuf_sum) then
      if (lend_st1.and.lend_st2.and.lend_sum) go to 1000
c
      enddo
      go to 100
1000  continue
c
       call  d2tr_out(
     & d2tr,    ind,  labbuf,  valbuf,
     & infocd,  iout, .true.)
c
      return
      end
c
c***********************************************************************
c
      subroutine d2tr_out(
     & d2tr,  ind,   labbuf,  valbuf,
     & info,  iout,  lastcall)
c
       implicit none
c  ##  paramter section
c
      integer nrec
      save  nrec
      data  nrec /0/
c
      integer   ninfmx
      parameter(ninfmx=10)
c
c     # sif parameters
      integer   msame,   nmsame,   nomore
      parameter(msame=0, nmsame=1, nomore=2)
c
      integer itypea2, itypeb2, nipv2
      parameter(itypea2 = 3, itypeb2 = 1, nipv2 = 4)
c
c     # sifs buffer length
      integer nsifbf
      parameter(nsifbf=4096)
c
      integer ifmt, ibvtyp, ibitv(1)
      data ifmt, ibvtyp, ibitv /0, 0, 0/
c
c  bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer   nfilmx
      parameter(nfilmx=55)
      integer         nunits
      common /cfiles/ nunits(nfilmx)
      character*60    fname
      common /cfname/ fname(nfilmx)
      integer den2_tr
      equivalence(nunits(18),den2_tr)
c
c  ##  integer section
c
      integer ind(4),iout,info(ninfmx),i,ierr
      integer j
      integer labbuf(4,*),last
      integer outmx
      integer reqnum
c
c  ##  real*8 section
c
      real*8 buffer(nsifbf)
      real*8 d2tr
      real*8 valbuf(*)
c
c  ##  logical section
c
      logical lastcall
c
      save reqnum
c-----------------------------------------------------------
      outmx = info(5)
      ierr=0
c
      if (.not.lastcall) then
      iout=iout+1
        do i=1,4
        labbuf(i,iout)=ind(i)
        enddo
      valbuf(iout)=d2tr
      endif

c  ## if buffer is full then write to cid2fl
       if((iout.eq.outmx).and.(.not.lastcall))then
        last=msame
c
        call sif2w8( den2_tr, info, reqnum, ierr )
         if ( ierr .ne. 0 )
     &    call bummer('1:d2tr_out: sif2w8, ierr=', ierr, faterr )
c
        call sifew2(
     &   den2_tr,   info,     nipv2,   iout,
     &   last,      itypea2,  itypeb2, 
     &   ibvtyp,    valbuf,   labbuf,  ibitv,
     &   buffer,    0,        nrec,    reqnum,
     &   ierr   )
        if (ierr.ne.0)
     &   call bummer('2:d2tr_out: sifew2 ierr=', ierr, faterr)
c
        return
       endif ! end of: if(iout.eq.outmx)then
c
      if (lastcall) then
       last=nomore
c
        call sif2w8( den2_tr, info, reqnum, ierr )
         if ( ierr .ne. 0 )
     &    call bummer('3:d2tr_out: sif2w8, ierr=', ierr, faterr )
c
        call sifew2(
     &   den2_tr,   info,     nipv2,   iout,
     &   last,      itypea2,  itypeb2, 
     &   ibvtyp,    valbuf,   labbuf,  ibitv,
     &   buffer,    0,         nrec ,  reqnum,
     &   ierr   )
        if (ierr.ne.0)
     &   call bummer('4:d2tr_out: sifew2 ierr=', ierr, faterr)
c
       endif ! end of: if (lastcall) then
c
      return
      end
c
c***********************************************************************
c
      subroutine contribution(ind,d2,indden,icount,d2tr,factor)
c
       implicit none
      integer ind(4),indden(4),icount
      real*8 d2,d2tr
      real*8 factor
c
c     write(*,*)'I got:'
c     write(*,*)'ind=',ind
c     write(*,*)'indden=',indden
      if ((ind(1).eq.indden(1)).and.(ind(2).eq.indden(2)).and.
     & (ind(3).eq.indden(3)).and.(ind(4).eq.indden(4))) then
       d2tr=d2tr+factor*d2
       icount=icount+1
c      write(*,*)'found',ind,d2
      endif
c
      return
      end
c
c***********************************************************************
c
      block data xx
      implicit logical(a-z)
c da file record parameters
C     Common variables
C

      integer   nfilmx
      parameter(nfilmx=55)
      integer         nunits
      common /cfiles/ nunits(nfilmx)
      character*60    fname
      common /cfname/ fname(nfilmx)

c------------------------------------------------------------------
c     # nlist = standard listing file.
      integer      nlist
      equivalence (nlist,nunits(1))
      data nlist/6/, fname(1)/'trci2denls'/
c------------------------------------------------------------------
c     # nin = user input file.
      integer      nin
      equivalence (nin,nunits(2))
      data nin/5/, fname(2)/'trci2denin'/
c------------------------------------------------------------------
c     # nslist = summary listing file.
      integer      nslist
      equivalence (nslist,nunits(3))
      data nslist/7/, fname(3)/'trci2densm'/
c------------------------------------------------------------------
c     # den1st1 = input 1-e density file for state 1
      integer      den1st1
      equivalence (den1st1,nunits(10))
      data den1st1/10/, fname(10)/'cid1fl.st1'/
c------------------------------------------------------------------
c     # den1st2 = input 1-e density file for state 2
      integer      den1st2
      equivalence (den1st2,nunits(11))
      data den1st2/11/, fname(11)/'cid1fl.st2'/
c------------------------------------------------------------------
c     # den1sum = input 1-e density file for state sum
      integer      den1sum
      equivalence (den1sum,nunits(12))
      data den1sum/12/, fname(12)/'cid1fl.sum'/
c------------------------------------------------------------------
c     # den2st1 = input 2-e density file for state 1
      integer      den2st1
      equivalence (den2st1,nunits(13))
      data den2st1/13/, fname(13)/'cid2fl.st1'/
c------------------------------------------------------------------
c     # den2st2 = input 2-e density file for state 2
      integer      den2st2
      equivalence (den2st2,nunits(14))
      data den2st2/14/, fname(14)/'cid2fl.st2'/
c------------------------------------------------------------------
c     # den2sum = input 2-e density file for state sum
      integer      den2sum
      equivalence (den2sum,nunits(15))
      data den2sum/15/, fname(15)/'cid2fl.sum'/
c------------------------------------------------------------------
c     # cid2indfl = 2-e density index file
      integer      indfl
      equivalence (indfl,nunits(16))
      data   indfl/16/, fname(16)/'cid2indfl'/
c------------------------------------------------------------------
c     # den1_tr = output vector file for the symm. 1-e tras. density
      integer      den1_tr
      equivalence (den1_tr,nunits(17))
      data   den1_tr/17/, fname(17)/'cid1fl.tr'/
c------------------------------------------------------------------
c     # den2sum = output vector file for the symm. 2-e tras. density
      integer      den2_tr
      equivalence (den2_tr,nunits(18))
      data   den2_tr/18/, fname(18)/'cid2fl.tr'/
c------------------------------------------------------------------
c include(data.common)
      integer maxsym
      parameter (maxsym=8)
      integer symtab,sym12,sym21,nbas,lmda,lmdb,nmbpr,strtbw,strtbx
      integer blst1e,nmsym,mnbas,cnfx,cnfw,scrlen
      common/data/symtab(maxsym,maxsym),sym12(maxsym),sym21(maxsym),
     + nbas(maxsym),lmda(maxsym,maxsym),lmdb(maxsym,maxsym),
     + nmbpr(maxsym),strtbw(maxsym,maxsym),strtbx(maxsym,maxsym),
     + blst1e(maxsym),nmsym,mnbas,cnfx(maxsym),cnfw(maxsym),scrlen
c
      data symtab/
     & 1,2,3,4,5,6,7,8,
     & 2,1,4,3,6,5,8,7,
     & 3,4,1,2,7,8,5,6,
     & 4,3,2,1,8,7,6,5,
     & 5,6,7,8,1,2,3,4,
     & 6,5,8,7,2,1,4,3,
     & 7,8,5,6,3,4,1,2,
     & 8,7,6,5,4,3,2,1/
c
      data sym12/1,2,3,4,5,6,7,8/

      end
c
c***********************************************************************
c
      subroutine datain
c
c  read user input.
c
       implicit none
      integer   nfilmx
      parameter(nfilmx=55)
      integer         nunits
      common /cfiles/ nunits(nfilmx)
      integer      nlist
      equivalence (nlist,nunits(1))
      integer      nin
      equivalence (nin,nunits(2))
c
      character*60    fname
      common /cfname/ fname(nfilmx)
c
      character*60 fnamex
      equivalence (fname(nfilmx),fnamex)
c
      integer nmotx
      parameter (nmotx=510)
c
      integer prtlvl
      common /indata/  prtlvl
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
c  ##  integer section
c
       integer ierr,i,j
c
c  ##  character section
c
      character*60 d1fl1,d1fl2,d1flsum,d2fl1,d2fl2,d2flsum
c
*@ifndef nonamelist
      namelist /input/ prtlvl,d1fl1,d1fl2,d1flsum,d2fl1,d2fl2,d2flsum
*@endif
c
      open( unit = nin, file = fname(2), status = 'unknown')
c
c     read and echo standard input until eof is reached.
c
      write(nlist,6010)
6010  format(' echo of the input for program transci:')
      call echoin( nin, nlist, ierr )
      if ( ierr .ne. 0 ) then
         call bummer(' datain: from echoin, ierr=',ierr,faterr)
      endif
c
      rewind nin
c
c defaults
       prtlvl=0
       d1fl1 = fname(10)
       d1fl2 = fname(11)
       d1flsum = fname(12)
       d2fl1 = fname(13)
       d2fl2 = fname(14)
       d2flsum = fname(15)
c
c     # read namelist input and keep processing if eof is reached.
c
*@ifdef nonamelist
*@elif defined  nml_required
*      read( nin, nml=input, end=3 )
*@else
      read( nin, input, end=3 )
*@endif
c
3     continue
c
      fname(10) = d1fl1
      fname(11) = d1fl2
      fname(12) = d1flsum
      fname(13) = d2fl1
      fname(14) = d2fl2
      fname(15) = d2flsum
c
      ierr = 0
c
      if ( prtlvl.lt.0) then
       call bummer('negative print level makes no sense!!!',prtlvl,0)
      ierr=ierr+1
      endif

      if (ierr.ne.0)
     . call bummer('input file errors, exiting',ierr,2)
c
      write(nlist,6020)
      close(unit=nin)
c
      return
6020  format(1x,72('-'))
      end
c
c**********************************************************************
c
c deck rdhcid
      subroutine rdhcid(nlist,denfl,infocd,ntitle,title,nsym,nmot,
     &                  nbpsy)
c
c  read the header info from the ci density matrix file.
c
c  written by ron shepard.
c
       implicit none
      character*80 title(*)
      integer infocd(*)
      integer ntitle,nsym,nmot,ninfo,nenrgy,ierr,nmap
      logical error
      integer nmotx
      parameter (nmotx=510)
      integer i, ntitmx, denfl,nlist,idummy, nsymx, nengmx
      parameter( ntitmx = 30, nsymx=8, nengmx=20)
      real*8 cienrg(nengmx)
      character*4 slabel(nsymx)
      character*8 cidlab(nmotx)
      integer nbpsy(nsymx),cietyp(nengmx)
c
      real*8    one,     zero
      parameter(one=1d0, zero=0d0)
c
      integer nmotn, nsymn
c
c  bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
      error = .false.
c
      rewind denfl
      call sifrh1(
     1 denfl, ntitle, nsym, nmot,
     2 ninfo, nenrgy, nmap, ierr)
c
      if (ierr .ne. 0) then
         call bummer('rdhcid: from sifrh1, ierr=',ierr,faterr)
      elseif (ntitle .gt. ntitmx) then
         call bummer('rdhcid: from sifrh1, ntitle=',ntitle,faterr)
      elseif (ninfo .gt. 10) then
         call bummer('rdhcid: from sifrh1, ninfo=',ninfo,faterr)
      elseif ( nenrgy .ne. 1) then
         call bummer('rdhcid: from sifrh1, nenrgy=',nenrgy,faterr)
      endif
cc     #ignore map
      nmap = 0
      call sifrh2(
     1 denfl,   ntitle,  nsym,    nmot,
     2 ninfo,   nenrgy,  nmap,    title,
     3 nbpsy,   slabel,  infocd,  cidlab,
     4 cietyp,  cienrg,  idummy,  idummy,
     5 ierr)
c     write(nlist,*)'ntitle=',ntitle
c
20    continue
c     if (error) call bummer('rdhcid:  inconsitent nbpsy', 0, faterr)
c
6010  format(' *** rdhcid error *** ',a,3i8)
c     write(nlist,6020)(title(i),i=1,ntitle)
6020  format(/' ci density file header information:'/(1x,a))
      write(nlist,6040)(i,nbpsy(i),i=1,nsym)
6040  format(' (isym:nbpsy)',8(i3,':',i3))
c
      return
      end
c
c**********************************************************************
c
      subroutine prtfn(funit,iunit,fdiscr,nlist)
c
c  print a fully qualified file name for an opened file.
c
c  funit  = fortran unit number.
c  iunit  = internal unit number.
c  fdiscr = brief description of the file.
c  nlist  = listing file unit number.
c
      implicit integer(a-z)
      character*(*) fdiscr
      integer funit,iunit,nlist
c
      character*60 fname
c
      inquire(unit=funit,name=fname)
      write(nlist,6100)fdiscr,funit,iunit,fname
6100  format(/1x,a,': fortran unit=',i2,', name(',i2,')=',a)
c
      return
      end

c**********************************************************************
c
c deck rdden1
      subroutine rdden1(
     & ntape, lbuf,   nipbuf,  infomo,
     & buf,   ilab,   val,     d1,
     & nndx,   nnmt,  ns,      sym,
     & nns,    map,   nsym,    nmpsy,
     & kntin)
c
c  read a 1-e density matrix
c
c  input:
c  ntape = input unit number.
c  lbuf = buffer length.
c  nipbuf = maximum number of integrals in a buffer.
c  infomo = info array
c  buf(*) = buffer for integrals and packed labels.
c  ilab(*)= unpacked labels.
c  val(*) = unpacked values.
c  sym(*) = symmetry of each orbital
c  ns(*) = symmetry offset for orbitals
c  nns(*) = symmetry offset for lower triangular matrices
c  nnmt = total size of symmetric, symmetry-packed, lower-triangular-
c         packed matrix.
c  map(*) = orbital index mapping vector.
c
c  output:
c  d1(*) = 1-e density matrix
c
c  written by ron shepard.
c  modified by gsk
c
      implicit integer(a-z)
c
      integer   nipv,   msame,  nmsame,     nomore
      parameter(nipv=2, msame=0,nmsame = 1, nomore = 2 )
      real*8 buf(lbuf),d1(*),val(nipbuf),fcore
      integer ilab(nipv,nipbuf),nndx(*),sym(*),ns(*),nns(*),map(*)
      integer nmpsy(*)
      integer infomo(*),kntin(*)
      integer   iretbv,    idummy(1),lasta,lastb
      parameter(iretbv = 0)
      real*8    zero
      parameter(zero = 0.0d0)
      integer    itypea,  itypbd
      parameter( itypea=0,itypbd=7)
c
      fcore= zero
c
      call wzero(nnmt,d1,1)
c     write(*,*)'ntape,itypea, itypbd',ntape,itypea, itypbd
      call sifr1x(
     1 ntape, infomo, itypea, itypbd,
     2 nsym,  nmpsy,  idummy, buf,
     3 val,   ilab,   map,    sym,
     4 nnmt,  d1,     fcore,  kntin,
     5 lasta,lastb,   last,   nrec,
     6 ierr)
c     write(*,*)'2:ntape,itypea, itypbd',ntape,itypea, itypbd
c     write(*,*)'2:fcore',fcore,ierr
      if ( ierr.ne. 0) then
        call bummer('rdden1: from sifr1x, ierr=',ierr,faterr)
      elseif (fcore .ne. zero) then
          write(6,*)' frozen core orbitals are not yet allowed'
        call bummer('rdden1: from sifr1x(), fcore=',fcore,faterr)
      endif
      return
      end
c
c***********************************************************************
c
c deck indast
      subroutine rd_d2bl(
     & vals,   labs,   nbuf,   buffer,
     & last,   sfile,  info)
c
c
       implicit none
c  ##  paramter & common block section
c
      integer nipv,iretbv
      parameter (nipv=4,iretbv=0)
c
      integer   ninfmx
      parameter(ninfmx=10)
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
c  ##  integer section
c
      integer info(ninfmx),itypea,itypeb,ifmt,ibvtyp,ierr,ibitv(1),i
      integer last,labs(4,*)
      integer nbuf
      integer sfile
c
c  ##  real*8
c
      real*8 buffer(*)
      real*8 vals(*)
c
c
c-----------------------------------------------------
c
      call sifrd2(sfile,   info,   nipv,    iretbv,
     &            buffer,  nbuf,   last,    itypea,
     &            itypeb,  ibvtyp,  vals,
     &            labs,    ibitv,  ierr)
c
      if (ierr.gt.0) call bummer("error in sifrd2",0,faterr)
      return
      end
c
c*******************************************************************
c
c deck wtden1
ctm numtot nicht initialisiert !
      subroutine wtden1(
     & ntape,  info,   lbuf,   nipbuf,
     & buf,    ilab,   val,    d1,
     & nsym,   irx1,   irx2,   map)
c
c  write the 1-e d1(*) matrix elements to the sifs file, d1matfl
c
c  input:
c  ntape = output unit number.
c  lbuf = buffer length.
c  nipbuf = maximum number of integrals in a buffer.
c  buf(*) = buffer for integrals and packed labels.
c  ilab(*)= unpacked labels.
c  val(*) = unpacked values.
c  d1(*) = 1-e density matrix
c  nsym,irx1(*),irx2(*) = symmetry range info.
c  map(*) = orbital index mapping vector.
c
c  written by gary kedziora
c
      implicit integer(a-z)
c
      integer info(10)
      integer    ifmt,     ibvtyp
      parameter( ifmt = 0, ibvtyp = 0)
      real*8 fcore
c
      integer   nipv
      parameter(nipv=2)
c
c     # sifs continuaton flag values
      integer   msame,   nmsame,  nomore
      parameter(msame=0, nmsame=1,nomore=2)
c
      real*8 buf(lbuf),d1(*),val(nipbuf)
      integer ilab(nipv,nipbuf),irx1(nsym),irx2(nsym),map(*)
c
      real*8    small
      parameter(small=1d-10)
c
c     # bummer error types
      integer wrnerr, nfterr, faterr
      parameter(wrnerr=0, nfterr=1, faterr=2)
c
c
c specification of the various ietypes
      itypea  = 0
      itypebd1= 7
      numtot = 0
c
c fcore contribution gets computed in tran for now.
c
      fcore   = 0.0
c
c  write 1-e density matrix elements and labels to sifs file
c
      last = nmsame
      pq=0
      iknt=0
      do 130 isym=1,nsym
         do 120 p=irx1(isym),irx2(isym)
            do 110 q=irx1(isym),p
               pq=pq+1
               if(iknt.eq.nipbuf)then
c                 # if the rare event happens that this is the last time
c                 # through the nested loops and iknt.eq.nipbuf then
c                 # set last=nomore
                  if ((isym .eq. nsym) .and. (p .eq. irx2(nsym)) .and.
     +                (q .eq. p)) then
                     last = nomore
                  else
                     last = msame
                  endif
                  numtot = numtot + iknt
                  call sifew1(
     &              ntape, info,  nipv,    iknt,
     &              last,  itypea,itypebd1,
     &              ibvtyp,val,   ilab,    fcore,
     &              ibitv, buf,   nrec,    ierr)
                  if ( ierr .ne. 0 )
     &               call bummer('wtden1:  sifew1, ierr=', ierr, faterr)
                  numtot = numtot - iknt
                  iknt = 0
               endif
               if (last .eq. nomore) return
               iknt=iknt+1
               val(iknt)=d1(pq)
               ilab(1,iknt)=map(p)
               ilab(2,iknt)=map(q)
110         continue
120      continue
130   continue
c
c dump the rest of the d1 matrix elements
c
      last = nomore
      numtot = iknt + numtot
      call sifew1(
     & ntape, info,  nipv,    iknt,
     & last,  itypea,itypebd1,
     & ibvtyp,val,   ilab,    fcore,
     & ibitv, buf,   nrec,    ierr)
      return
c
      end
c
c*******************************************************************
c

