!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      subroutine writecosmo(icosout,qsum, qsumo, etot, ediel, de,
     &         xyz, mtype, smtype, natoms, iatsp, qcosc, phic,
     &         ar, cosurf, info, isym, nps, npspher, ifac)

c----------------------------------------------------------------------c
c Write COSMO file
c
c icosout:   filehandle for outputfile (COSMO file)
c qsum:   total charge
c qsumo:  total outer charge
c etot:   total energie
c ediel:  dielectric energy
c de:     energy correction
c smytpe: array of atomic symbols (fe,h,na...)
c xyz:    atomic coordinates
c mtype:  array of nuclear charges (to count the number of
c         iatoms per element)
c natoms: number of atoms
c iatsp:  number of atom related to a segment
c qcosc:  array of corrected screening charges
c phic:   array of the coordcted potential on the seg.
c ar:     array of the area of the segments
c cosurf: array coordinates of surface segments
c info:   information like versions etc...
c ifac:   0 -> normal cosmo calculations using f(epsilon)
c ifac:   1 -> excited state calculatio using f(n**2)
c
c---------------------------------------------------------------------c

      implicit real*8 (a-h,o-z)

      include 'cosinc.h'

      parameter( bohr = 0.529177249d0 )
      parameter(threshold=1.0d-8)


c-------------------COMMON BLOCKS---------------------------------------
c
c cosmo variables
      common /cos/ eps, fepsi, disex, disex2, rsolv, routf,
     &               nspa, nsph, nppa, area, refind, fn2,
     &               lcavity, srad(maxat), tm(3,3,maxat),
     &               nn(3,maxat), volume, phsran, ampran
      save /cos/


c from outside

      character*80 info
      character*8 smtype

      dimension smtype(natoms)
      dimension xyz(3,natoms), mtype(natoms) ,phic(nps), qcosc(nps)
      dimension cosurf(3,2*maxnps), iatsp(nps), ar(nps)

c allocated here
      character*6  cavity
      dimension iatrep(maxrepl,maxat), nrep(maxat)
      dimension xfinal(maxrepl), yfinal(maxrepl), zfinal(maxrepl)
      dimension xa(maxrepl), ya(maxrepl), za(maxrepl)
      logical valid

c---------------CAPITAL LETTERS FOR CAR FILE----------------------------
c
c     pse for mapping nuc charge -> element symbol
      parameter (ipse=83)
      character*2 pse(ipse)
      character*5 label
      character*2 label2
      data pse/'H ','HE','LI','BE','B ','C ','N ','O ','F ','NE','NA',
     &         'MG','AL','SI','P ','S ','CL','AR','K ','CA','SC','TI',
     &         'V ','CR','MN','FE','CO','NI','CU','ZN','GA','GE','AS',
     &         'SE','BR','KR','RB','SR','Y ','ZR','NB','MO','TC','RU',
     &         'RH','PD','AG','CD','IN','SN','SB','TE','I ','XE','CS',
     &         'BA','LA','CE','PR','ND','PM','SM','EU','GD','TB','DY',
     &         'HO','ER','TM','YB','LU','HF','TA','W ','RE','OS','IR',
     &         'PT','AU','HG','TL','PB','BI'/

      integer icnt(ipse)
c
c-----------------------------------------------------------------------
c     print input

      write(icosout,'(a)') '$info'
      write(icosout,'(a)') info

c-------  symmetry --------------
      npsirrep = nps
      if(isym .gt. 0) then
c        get symmetry factor
         call replicate(isym,1.d0,1.d0,1.d0,np,xfinal,yfinal,zfinal)
         isymf = np
         symf = dble(isymf)
         nps = nps * isymf
         npspher = npspher * isymf
c area and volume are already calc. for the whole molecule
         qsum = qsum*symf
         qsumo = qsumo*symf
         ediel = ediel*symf
         de = de*symf

c get atom numbers for replicated atoms

         valid = .false.
         do i=1,natoms
           call irreducible(isym,xyz(1,i),xyz(2,i),xyz(3,i),valid)
           if(valid) then
             np = 0
             call replicate(isym,xyz(1,i),xyz(2,i),xyz(3,i),np,
     &                    xfinal,yfinal,zfinal)
             nrep(i)=np
             do irep=1,np
               do ir=1,natoms
                  d=sqrt((xfinal(irep)-xyz(1,ir))**2+(yfinal(irep)
     &              -xyz(2,ir))**2+(zfinal(irep)-xyz(3,ir))**2)
                 if(d .lt. threshold) iatrep(irep,i) = ir
               enddo
             enddo
           endif
         enddo

         write(icosout,'(a,i3,a)') 'Symmetry used (isym:',isym,')'
      endif
c---------------------------------

      write(icosout,'(a)') '$cosmo'

      if (fepsi .eq. 1.) then
         write(icosout,'(a)') '  epsilon=infinity'
      else
         write(icosout,'(a,f7.4)') '  epsilon=', eps
      endif
      if(ifac .eq. 1) then
         write(icosout,'(a,f7.4)') '  refraction index=', refind
      endif
      write(icosout,'(a,i5)') '  nppa=',  nppa
      write(icosout,'(a,i5)') '  nspa=', nspa
      write(icosout,'(a,g12.6)') '  disex=', disex
      write(icosout,'(a,f5.2)') '  rsolv=', rsolv * bohr
      write(icosout,'(a,f5.2)') '  routf=', routf
      if (lcavity .eq. 0) then
          cavity = 'open'
      else
          cavity = 'closed'
      endif
      write(icosout,'(a,a6)') '  cavity ', cavity
      write(icosout,'(a,a)') '  amat file= amat'
      write(icosout,'(a,g9.2)') '  phsran=',  phsran
      write(icosout,'(a,g9.2)') '  ampran=', ampran
c-----------------------------------------------------------------------
c     write calculated parameters and variables $cosmo_data

      npsd = nps+npspher
      write(icosout,10) fepsi,disex2,maxnps,nsph,nps,npsd,npspher,
     &                 area,volume
 10   format('$cosmo_data',/,
     &       '  fepsi=',f8.5,/,
     &       '  disex2=',g12.6,/,
     &       '  maxnps=',i5,/,
     &       '  nsph=',i5,/,
     &       '  nps=',i5,/,
     &       '  npsd=',i5,/,
     &       '  npspher=',i5,/,
     &       '  area=',f8.2,/,
     &       '  volume=',f8.2)
      if(ifac .eq. 1) then
         write(icosout,'(a,f8.5)') '  f(n**2)=', fn2
      endif

c-----------------------------------------------------------------------
c     print final geometry (coordinates and their radii)
      write(icosout,'(a)') '$coord_rad'
      write(icosout,'(a,a)') '#atom   x                  y          ',
     &         '        z             element  radius [A]'

      do i=1,natoms
          write(icosout,20) i,(xyz(j,i),j=1,3),smtype(i)(1:2),
     &                     (srad(i)-rsolv)*bohr
      enddo
 20   format(1x,i3,3(1x,f18.14),2x,a2,2x,f10.5)
c-----------------------------------------------------------------------
c print car format

      write(icosout,'(a,/,a,/,a,/,a,/,a)') '$coord_car',
     &      '!BIOSYM archive 3','PBC=OFF',
     &      'coordinates from COSMO calculation', '!DATE '

 30   format(a5,3f15.9,' COSM 1      ',a8,a2,f7.3)

      do j=1,ipse
         icnt(j) = 0
      enddo

      do i=1,natoms
        ii=mtype(i)
        icnt(ii)=icnt(ii)+1
        len = 2
        if(ii .gt. ipse) then
           label = '--'
        else
           label(1:2) = pse(ii)
           if(label(2:2) .eq. ' ') len=1
        endif
        label2(1:2) = label(1:2)
        write(label(len+1:5),'(i1)') icnt(ii)
        write(icosout,30) label,xyz(1,i)*bohr,xyz(2,i)*bohr,
     &                   xyz(3,i)*bohr,smtype(i),label2,0.d0
      enddo
      write(icosout,'(a/a)') 'end','end'

c----------------------------------------------------------------------c
      write(icosout,40) qsum, qsumo, qsum + qsumo
 40   format('$screening_charge',/,
     &       '  cosmo      = ',f10.6,/,
     &       '  correction = ',f10.6,/,
     &       '  total      = ',f10.6)

c----------------------------------------------------------------------c
      write(icosout,50) etot,etot+de,ediel,ediel+de
 50   format('$cosmo_energy',/,
     &       '  Total energy [a.u.]            =   ', f17.10,/,
     &       '  Total energy corrected [a.u.]  =   ', f17.10,/,
     &       '  Dielectric energy [a.u.]       =   ', f17.10,/,
     &       '  Dielectric energy corr. [a.u.] =   ', f17.10)

      write(icosout,60)
 60   format('$segment_information',/,
     & '# n             - segment number',/,
     & '# atom          - atom associated with segment n',/,
     & '# position      - segment coordinates [a.u.]',/,
     & '# charge        - segment charge (corrected)',/,
     & '# area          - segment area [A**2]',/,
     & '# potential     - solute potential on segment (A length scale)'/
     & '#'/
     & '#  n   atom',14x,'position (X, Y, Z)',19x,
     &        'charge         area        charge/area     potential',
     &        /'#'/'#')
      if(ifac .eq. 1) then
        write(icosout,'(a/a/a)')'# EXCITED STATE CALCULATION. Charges ',
     &   '# and Potentials with respect to the density relaxation ',
     &   '# (relativ to the frozen potential of the ground state).'
      endif
c first print the irrep segments in the right order
      num = 0
      do i=1,npsirrep
        num=num+1
        kat = iatsp(i)
        q=qcosc(i)
        p=phic(i)/bohr
        a=ar(i)*bohr*bohr
        write(icosout,'(i5,i5,7f15.9)')
     &             num,kat,(cosurf(j,i),j=1,3),q,a,q/a,p
      enddo
c print replicates
      if(isym .gt. 0) then
        do i=1,npsirrep
          kat = iatsp(i)
          q=qcosc(i)
          p=phic(i)/bohr
          a=ar(i)*bohr*bohr
          np = 0
          npa= 0
c         repl. of seg.
          call replicate(isym,cosurf(1,i),cosurf(2,i),cosurf(3,i),np,
     &                    xfinal,yfinal,zfinal)
c         repl. of atom
          call replicate(isym,xyz(1,kat),xyz(2,kat),xyz(3,kat),npa,
     &                    xa,ya,za)
          do irep=2,np
            num=num+1
c
c search the nearest atom from the set of replicates
c
            d1 = 1000000000.d0
            do ira=1,npa
               d=sqrt((xfinal(irep)-xa(ira))**2+(yfinal(irep)
     &          -ya(ira))**2+(zfinal(irep)-za(ira))**2)
               if(d .lt. d1) then
                 katrep = iatrep(ira,kat)
                 d1 = d
               endif
            enddo
            write(icosout,'(i5,i5,7f15.9)')
     &      num,katrep,xfinal(irep),yfinal(irep),zfinal(irep),q,a,q/a,p
          enddo
        enddo
      endif

      return

      end

