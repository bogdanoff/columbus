!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
       subroutine coschol1(a,n,info)

c written by A. Klamt

c this routine performs a Cholesky factorization
c input:   a =  packed lower triangle of a
c               symmetric positive definite n*n matrix
c output:  a =  lower triangle of Cholesky matrix ( inverse pivot elemen

       implicit none

       real*8 a(*), summe
       integer n,info,i,j,k,indk,indi,is,kk

       indk=0
       info = 0
       do k = 1, n
         indi=indk
         kk=k+indk
         do i = k, n
            summe = 0.d0
            do j = 1, k-1
               summe = summe + a(j+indi)*a(j+indk)
            enddo
            summe = a(k+indi) - summe
            if (i.eq.k) then
               if (summe.lt.0.0d0 ) then
                  info = -1
                  summe = a(kk)
               endif
               a(kk) = 1.d0/sqrt(summe)
            else
               a(k+indi) = summe*a(kk)
            endif
            indi=indi+i
         enddo
         indk=indk+k
       enddo

       end
