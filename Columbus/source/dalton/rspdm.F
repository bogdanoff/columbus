!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
C  /* Deck rspdm */
      SUBROUTINE RSPDM(ILSYM,IRSYM,NCLDIM,NCRDIM,CL,CR, RHO1,RHO2,
     *                 ISPIN1,ISPIN2,TDM,NORHO2,XINDX,WORK,
     *                 KFREE,LFREE)
C
C     CONSTRUCT ONE (RHO1) AND  TWO (RHO2) PARTICLE DENSITY
C     MATRICES TO BE USED IN RESPONSE CALCULATION.
C
C  OUTPUT:
C
C  RHO1(KL)   = (L/ E(KL) /R)
C
C  RHO2       :  TWO-BODY TRANSITION DENSITY MATRIX FOR STATE
C               (L/ AND /R) PACKED WITH SQUARE DISTRIBUTIONS
C
C  RHO2(IJKL) =    RHO2[NASHT,NASHT,NASHT,NASHT]
C
C  RHO2(IJKL) =    (L/ E(IJ,KL) - DELTA(JK) E(IL) /R )
C
#include <implicit.h>
C
      DIMENSION CL(*), CR(*),RHO1(NASHT,*)
      DIMENSION RHO2(NASHT,NASHT,NASHT,*)
      DIMENSION XINDX(2),WORK(2)
C
      PARAMETER ( D0 = 0.0D0, D1 = 1.0D0 , SMALL = 1.0D-8)
C
C Used from common blocks:
C   INFINP : LSYM,FLAG(*)
C   INFORB : NASHT,N2ASHX,NNASHX
C   INFVAR : ?? not used ?? /920920-hjaaj
C   INFPRI : LUW6
C
#include <maxorb.h>
#include <infinp.h>
#include <inforb.h>
#include <infvar.h>
#include <infpri.h>
#include <infrsp.h>
C
C
      INTEGER RHOTYP
      LOGICAL USEPSM, CSFEXP, TDM, NORHO2
C
C     Treat NASHT .eq. 1 as a special case
C
      IF (NASHT .EQ. 0) GO TO 9999
      IF (NASHT .EQ. 1) THEN
         IF ( (ILSYM.EQ.IRSYM) .AND. (ABS(CL(1)-D1).LT.SMALL)
     *                         .AND. (ABS(CR(1)-D1).LT.SMALL) ) THEN
            RHO1(1,1) = D1
C           RHO1 = 1.0 both for ISPIN1 = 0 and ISPIN1 = 1
         ELSE
            RHO1(1,1) = D0
         END IF
         IF (.NOT. NORHO2)
     *   RHO2(1,1,1,1) = D0
         GO TO 9999
      END IF
C
C     Set RHOTYP and USEPSM
C     RHOTYP = 2 for non-symmetrized density matrix
C     (PV(ij,kl) usually .ne. PV(ij,lk))
C     USEPSM tells densid if it is allowed to use permutation symmetry
C     for Ms = 0.
C
      RHOTYP = 2
      IF (FLAG(66)) THEN
         USEPSM = .FALSE.
      ELSE
         USEPSM = .TRUE.
      END IF
C
C
      CSFEXP = .NOT.FLAG(27)
C
      IF ( IPRRSP.GT.65 ) THEN
         WRITE(LUW6,'(/A)')' ***RSPDM BEFORE CALLING DENSI'
         WRITE(LUW6,'(/A,/I6,4I7,I8)')
     *     ' ILSYM  IRSYM  NCLDIM  NCRDIM  KFREE   LFREE :',
     *       ILSYM, IRSYM, NCLDIM, NCRDIM, KFREE,  LFREE
      END IF
      IF ( IPRRSP.GT.120 ) THEN
         WRITE(LUW6,'(/A)')' *RSPDM* LEFT REFERENCE VECTOR'
         CALL OUTPUT(CL,1,NCLDIM,1,1,NCLDIM,1,1,LUW6)
         WRITE(LUW6,'(/A)')' *RSPDM* RIGHT REFERENCE VECTOR'
         CALL OUTPUT(CR,1,NCRDIM,1,1,NCRDIM,1,1,LUW6)
      END IF
      IF (.NOT. NORHO2)
     *CALL SETVEC(RHO2,D0,N2ASHX*N2ASHX)
      CALL SETVEC(RHO1,D0,N2ASHX)
      CALL DENSID(ILSYM,IRSYM,NCLDIM,NCRDIM,CL,CR,RHO1,RHO2,
     *            RHOTYP,CSFEXP,USEPSM,NORHO2,ISPIN1,ISPIN2,TDM,
     *            XINDX,WORK,KFREE,LFREE)
C     CALL DENSID(ILSYM,IRSYM,NCLDIM,NCRDIM,CL,CR,RHO1,RHO2,
C    *            RHOTYP,CSFEXP,USEPSM,NORHO2,ISPIN1,ISPIN2,TDM,
C    *            XNDXCI,WORK,KFREE,LFREE)
C
      IF ( (.NOT. NORHO2 ) .AND. IPRRSP .GE. 120 ) THEN
         WRITE(LUW6,'(/A)')
     *       ' TWO BODY DENSITY MATRIX (non-zero elements):'
         DO 240 L = 1, NASHT
            DO 240 K = 1, NASHT
               WRITE(LUW6,'(/A,2I5)') ' DISTRIBUTION K,L: ',K,L
               CALL OUTPUT(RHO2(1,1,K,L),1,NASHT,1,NASHT,
     *                     NASHT,NASHT,1,LUW6)
  240    CONTINUE
      END IF
C
      IF ( IPRRSP.GT.95 ) THEN
         WRITE(LUW6,'(/A)')' *** LEAVING RSPDM'
         WRITE(LUW6,'(/A,/I6,4I7,I8)')
     *     ' ILSYM  IRSYM  NCLDIM  NCRDIM  KFREE   LFREE :',
     *       ILSYM, IRSYM, NCLDIM, NCRDIM, KFREE,  LFREE
      END IF
      IF ( IPRRSP.GT.90 ) THEN
         WRITE (LUW6,1100)
         CALL OUTPUT(RHO1,1,NASHT,1,NASHT,NASHT,NASHT,1,LUW6)
      ENDIF
 1100 FORMAT(/' DV = One-el. density matrix, active part, MO-basis')
C
 9999 RETURN
C     ... end of RSPDM.
      END
