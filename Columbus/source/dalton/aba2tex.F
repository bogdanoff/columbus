!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
C  /* Deck twoinp */
      SUBROUTINE TWOINP(WORD)
C
C     TUH
C
#include <implicit.h>
#include <priunit.h>
#include <mxcent.h>
      PARAMETER (NTABLE = 20)
      LOGICAL NEWDEF
      CHARACTER PROMPT*1, WORD*7, TABLE(NTABLE)*7, WORD1*7
#include <abainf.h>
#include <cbitwo.h>
      DATA TABLE /'.SKIP  ', '.PRINT ', '.DIRTST', '.FIRST ', '.SECOND',
     *            '.PTRSKI', '.SORSKI', '.INTSKI',
     *            '.PTRPRI', '.SORPRI', '.INTPRI',
     *            '.NODC  ', '.NODV  ', '.NOPV  ', '.RETURN',
     *            'XXXXXXX', '.TIME  ', '.NOCONT', '.STOP  ','.PTRNOD'/
C
      NEWDEF = WORD .EQ. '*TWOEXP'
      MAXOLD = MAXDIF
      ICHANG = 0
      IF (NEWDEF) THEN
         WORD1 = WORD
  100    CONTINUE
            READ (LUCMD, '(A7)') WORD
            PROMPT = WORD(1:1)
            IF (PROMPT .EQ. '!' .OR. PROMPT .EQ. '#') THEN
               GO TO 100
            ELSE IF (PROMPT .EQ. '.') THEN
               ICHANG = ICHANG + 1
               DO 200 I = 1, NTABLE
                  IF (TABLE(I) .EQ. WORD) THEN
                     GO TO (1,2,3,4,5,6,7,8,9,10,
     &                      11,12,13,14,15,16,17,18,19,20),I
                  END IF
  200          CONTINUE
               IF (WORD .EQ. '.OPTION') THEN
                 CALL PRTAB(NTABLE,TABLE,WORD1//' input keywords',LUPRI)
                 GO TO 100
               END IF
               WRITE (LUPRI,'(/,3A,/)') ' Keyword "',WORD,
     *            '" not recognized in TWOINP.'
               CALL PRTAB(NTABLE,TABLE,WORD1//' input keywords',LUPRI)
               CALL QUIT('Illegal keyword in TWOINP.')
    1          CONTINUE
                  SKIP = .TRUE.
               GO TO 100
    2          CONTINUE
                  READ (LUCMD, '(I5)') IPRALL
                  IF (IPRALL .EQ. IPRDEF) ICHANG = ICHANG - 1
                  IPRINT = IPRALL
                  IPRPTR = IPRALL
                  IPRSOR = IPRALL
               GO TO 100
    3             CONTINUE
                  DIRTST = .TRUE.
               GO TO 100
    4             MAXDIF = 1
                  IF (MAXDIF .EQ. MAXOLD) ICHANG = ICHANG - 1
               GO TO 100
    5             MAXDIF = 2
                  IF (MAXDIF .EQ. MAXOLD) ICHANG = ICHANG - 1
               GO TO 100
    6             RUNPTR = .FALSE.
               GO TO 100
    7             RUNSOR = .FALSE.
               GO TO 100
    8             RUNINT = .FALSE.
               GO TO 100
    9             READ (LUCMD, '(I5)') IPRPTR
                  IF (IPRPTR .EQ. IPRDEF) ICHANG = ICHANG - 1
               GO TO 100
   10             READ (LUCMD, '(I5)') IPRSOR
                  IF (IPRSOR .EQ. IPRDEF) ICHANG = ICHANG - 1
               GO TO 100
   11             READ (LUCMD, '(5I5)') IPRINT, IPRNTA, IPRNTB,
     *                                          IPRNTC, IPRNTD
                  IPRSUM = IPRNTA + IPRNTB + IPRNTC + IPRNTD
                  IF (IPRINT .EQ. IPRDEF .AND. IPRSUM .EQ. 0) THEN
                     ICHANG = ICHANG - 1
                  END IF
               GO TO 100
   12             NODC = .TRUE.
               GO TO 100
   13             NODV = .TRUE.
               GO TO 100
   14             NOPV = .TRUE.
               GO TO 100
   15             RETUR = .TRUE.
               GO TO 100
   16             CONTINUE
C              GO TO 100
   17             TKTIME = .TRUE.
               GO TO 100
   18             NOCONT = .TRUE.
               GO TO 100
   19             CUT    = .TRUE.
               GO TO 100
   20             NODPTR = .TRUE.
               GO TO 100
            ELSE IF (PROMPT .EQ. '*') THEN
               GO TO 300
            ELSE
               WRITE (LUPRI,'(/,3A,/)') ' Prompt "',WORD,
     *            '" not recognized in TWOINP.'
               CALL PRTAB(NTABLE,TABLE,WORD1//' input keywords',LUPRI)
               CALL QUIT('Illegal prompt in TWOINP.')
            END IF
      END IF
  300 CONTINUE
      IF (ICHANG .EQ. 0) RETURN
      IF (NEWDEF) THEN
         CALL HEADER('Changes of defaults for TWOEXP:',0)
      END IF
      IF (SKIP) THEN
         WRITE (LUPRI,'(A)') ' TWOEXP skipped in this run.'
      ELSE IF (NEWDEF) THEN
         IF (IPRALL .NE. IPRDEF) THEN
            WRITE (LUPRI,'(A,I5)') ' Print level in TWOEXP:',IPRALL
         END IF
         IF (RUNPTR) THEN
            IF (IPRPTR .NE. IPRDEF) THEN
               WRITE (LUPRI,'(A,I5)') ' Print level in PTRAN :',IPRPTR
            END IF
            IF (NODPTR) THEN
               WRITE (LUPRI,'(A)') ' PV matrix transformed to AO '
     &              //'basis using noddy routine for comparison.'
            END IF
         ELSE
            WRITE (LUPRI,'(A)') ' PTRAN skipped in this run.'
         END IF
         IF (RUNSOR) THEN
            IF (IPRSOR .NE. IPRDEF) THEN
               WRITE (LUPRI,'(A,I5)') ' Print level in PSORT :',IPRSOR
            END IF
         ELSE
            WRITE (LUPRI,'(A)') ' PSORT skipped in this run.'
         END IF
         IF (RUNINT) THEN
            IF (MAXDIF .NE. MAXOLD) THEN
               WRITE (LUPRI,'(A,I1)') ' Maximum differentiation: ',
     *                                MAXDIF
            END IF
            IF (IPRINT .NE. IPRDEF) THEN
               WRITE (LUPRI,'(A,I5)') ' Print level in TWOINT:',
     *                                IPRINT
            END IF
            IF (IPRNTA + IPRNTB + IPRNTC + IPRNTD .GT. 0) THEN
               WRITE(LUPRI,'(2A,4I3)')' Extra output for the following',
     *                    ' shells:', IPRNTA, IPRNTB, IPRNTC, IPRNTD
               IF (RETUR) WRITE (LUPRI,'(A)')
     *             ' Program will exit TWOINT after these shells.'
            END IF
            IF (NODC) WRITE (LUPRI,'(/,2A)') ' Inactive one-electron',
     *         ' density matrix neglected in TWOEXP.'
            IF (NODV) WRITE (LUPRI,'(/,2A)') ' Active one-electron',
     *         ' density matrix neglected in TWOEXP.'
            IF (NOPV) WRITE (LUPRI,'(/,2A)') ' Active two-electron',
     *         ' density matrix neglected in TWOEXP.'
            IF (NOCONT) WRITE (LUPRI,'(/,2A)')
     *         ' Derivative integrals will not be contracted.'
            IF (TKTIME) WRITE (LUPRI,'(/,2A)') ' Detailed timing for',
     *         ' integral calculation will be provided.'
            IF (DIRTST) WRITE (LUPRI,'(/A)') ' Direct calculation '//
     &         'of Fock matrices and integral distributions tested.'
         ELSE
            WRITE (LUPRI,'(A)') ' TWOINT skipped in this run.'
         END IF
         IF (CUT) THEN
            WRITE (LUPRI,'(/,A)') ' Program is stopped after TWOEXP.'
         END IF
      END IF
      RETURN
      END
C  /* Deck twoini */
      SUBROUTINE TWOINI
C
C     Initialize /CBITWO/
C
#include <implicit.h>
#include <mxcent.h>
#include <abainf.h>
#include <cbitwo.h>
C
      SKIP   = .FALSE.
      CUT    = .FALSE.
      RUNPTR = .TRUE.
      RUNSOR = .TRUE.
      RUNINT = .TRUE.
      IPRALL = IPRDEF
      IF (MOLHES) THEN
         MAXDIF = 2
      ELSE IF (MOLGRD .OR. DIPDER) THEN
         MAXDIF = 1
      ELSE
         SKIP = .TRUE.
      END IF
      IPRINT = IPRDEF
      IPRNTA = 0
      IPRNTB = 0
      IPRNTC = 0
      IPRNTD = 0
      IPRPTR = IPRDEF
      IPRSOR = IPRDEF
      NODC   = .FALSE.
      NODV   = .FALSE.
      NOPV   = .FALSE.
      NOCONT = .FALSE.
      RETUR  = .FALSE.
      TKTIME = .FALSE.
      DIRTST = .FALSE.
      NODPTR = .FALSE.
      RETURN
      END
C  /* Deck twoexp */
      SUBROUTINE TWOEXP(WORK,LWORK,PASS)
C
C     TUH
C
#include <implicit.h>
#include <priunit.h>
#include <mxcent.h>
#include <maxaqn.h>
#include <mxorb.h>
#include <dummy.h>
      LOGICAL PASS, ANTI, ANTIDM, DOINT(2,2,2)
      DIMENSION WORK(LWORK)
#include <abainf.h>
#include <exeinf.h>
#include <cbitwo.h>
#include <inforb.h>
#include <symmet.h>
#include <nuclei.h>
#include <infpar.h>
#include <colinf.h>
      COMMON /PTRFIL/ JOBIN, JOBOUT, LUMC, LUSCR, LUDA, LUPSO, LUPAO,
     &                LUPAS
C
      PARAMETER (MXDMAT = 2*MXCOOR)
      INTEGER IFCTYP(MXDMAT), ISYMDM(MXDMAT)
      integer itm
      logical op 
C
C     Control routine for calculation of expectation values
C     of differentiated two-electron integrals
C
      IF (SKIP) RETURN
cgk
C  the following has been moved from ptran, because in a columbus
C  calculation luda needs to be set in psrtc without execution of
C  ptran (transformation done by tran.x).  No harm done?
C
C     Initialize /PTRFIL/
C
      JOBIN  =  5
      JOBOUT =  6
      LUMC   = 20
      LUSCR  = 12
      LUDA   = 24
      LUPSO  = 28
      LUPAO  = 29
      LUPAS  = 27
cgk
C
C     If not executed earlier, variables are saved, else restored.
C     Necessary for unified code.
C
      IF (FTWOXP) THEN
         NASTXP = NASHT
         SORTXP = RUNSOR
         PTRTXP = RUNPTR
         NPVTXP = NOPV
         FTWOXP = .FALSE.
      ELSE
         NASHT  = NASTXP
         RUNSOR = SORTXP
         RUNPTR = PTRTXP
         NOPV   = NPVTXP
      END IF
C
      DOINT(1,1,1) = .TRUE.
      DOINT(2,2,1) = .TRUE.
      DOINT(1,2,1) = .FALSE.
      DOINT(2,1,1) = .FALSE.
      DOINT(1,1,2) = .TRUE.
      DOINT(2,2,2) = .TRUE.
      DOINT(1,2,2) = .FALSE.
      DOINT(2,1,2) = .FALSE.
C
      IF (IPRALL .GE. 1) THEN
         CALL TIMERH('START ',TIMEIN,TIMOUT)
         WRITE (LUPRI,'(A,/)')
     *     '1 <<<<<<<<<< Output from TWOEXP >>>>>>>>>> '
      END IF
C
C     No active orbitals
C
      IF (NASHT .EQ. 0) NODV = .TRUE.
      IF (NASHT .LT. 2) THEN
         RUNPTR = .FALSE.
         RUNSOR = .FALSE.
         NOPV   = .TRUE.
      END IF
C
C     *******************************************************
C     ***** Set up COMMON /BLOCKS/ for PSORT and TWOINT *****
C     *******************************************************
C
      CALL PAOVEC(0,IPRALL)
C
C     **********************************************************
C     ***** Set up two-electron density matrix in AO basis *****
C     **********************************************************
C
      ANTI = .FALSE.
C
C     Transformation to AO basis
C
cgk
      IF (RUNPTR .and. (.not. colbus)) THEN
cgk
         IF (IPRPTR .GE. 2) CALL TIMERH('START ',TIMSTR,TIMEND)
         CALL PTRAN(NODPTR,WORK,LWORK,IPRPTR,ANTI)
         IF (IPRPTR .GE. 2) CALL TIMERH('PTRAN ',TIMSTR,TIMEND)
      END IF
C
C     Final sorting of integrals
C
cdd   IF (RUNSOR) THEN
      if ((RUNSOR).and.(.not.d1astr)) then
cdd 
         IF (IPRPTR .GE. 2) CALL TIMERH('START ',TIMSTR,TIMEND)
         CALL PSORG(WORK,WORK,LWORK,IPRSOR,ANTI)
         IF (IPRPTR .GE. 2) CALL TIMERH('PSORT ',TIMSTR,TIMEND)
      END IF
C
C     ****************************************
C     ***** Calculate Expectation Values *****
C     ****************************************
C
      IF (RUNINT) THEN
C
C        One-electron density matrices
C
         IF (NODV) THEN
            NDMAT = 1
         ELSE
            NDMAT = 2
         END IF
         KDMAT = 1
         IF (FCKDDR .AND. EXPFCK) THEN
            ITYPE = 6
            IF (DIPDER .AND. .NOT. MOLHES) ITYPE = -6
            NFMAT = 3*NUCDEP*NDMAT
            KFMAT = KDMAT + NDMAT*N2BASX
            KLAST = KFMAT + NFMAT*N2BASX
         ELSE
            ITYPE = 2
            NFMAT = 0
            KLAST = KDMAT + NDMAT*N2BASX
            KFMAT = KDMAT
         END IF
         LWRK  = LWORK - KLAST + 1
         IF(KLAST.GT.LWORK) CALL STOPIT('TWOEXP','GETDMT',KLAST,LWORK)
         IF (NFMAT .GT. 0) THEN
            CALL DZERO(WORK(KFMAT),NFMAT*N2BASX)
C           dimension ISYMDM(MXDMAT), IFCTYP(MXDMAT)
            IF (NFMAT .GT. MXDMAT) THEN
               WRITE (LUPRI,'(/A,2I10)')
     &         ' MXDMAT too small in TWOEXP; NFMAT,MXDMAT=',NFMAT,MXDMAT
               CALL QUIT('MXDMAT too small in TWOEXP')
            END IF
            DO I = 1,NFMAT
               ISYMDM(I) = 0
               IFCTYP(I) = 1
            END DO
         END IF
cgk
         if (.not.colbus) then
c        ...not needed after all...
            CALL GETDMT(WORK(KDMAT),NDMAT,WORK(KLAST),LWRK,NODC,NODV,
     &               IPRINT)
         endif 
cgk
C
         IF (PARALL) THEN
C hjaaj 941114 next line seems dangerous, therefore I have changed it
Cold
C           NDMAT = 1
Cnew
            IF (NDMAT .NE. 1) CALL QUIT
     &         (' ERROR in TWOEXP: PARALL only implemented for SCF')
            KNSTAT = KLAST
            KINDEX = KNSTAT + MAXNOD
            KSORT  = KINDEX + MAXTSK
            KTIMES = KSORT  + MAXTSK + 2
            KLAST  = KTIMES + MAXTSK
            LWRK   = LWORK - KLAST
C
            IF (KLAST .GT. LWORK) THEN
               CALL STOPIT('TWOEXP','PARDRV',KLAST,LWORK)
            END IF
C
            IF (KPRINT .GT. 0) CALL GETTIM(CPU1,WALL1)
C
cgk         CALL PARDRV(WORK(KLAST),LWRK,WORK(KFMAT),WORK(KDMAT),
cgk  &                  NDMAT,ISYMDM,IFCTYP,ITYPE,MAXDIF,IATOM,
cgk  &                  NODV,NOPV,NOCONT,TKTIME,RETUR,WORK(KNSTAT),
cgk  &                  WORK(KINDEX),WORK(KSORT),WORK(KTIMES))
C
C           Symmetrize skeleton Fock matrices
C
            CALL SKLFCK(WORK(KFMAT),WORK(KLAST),LWRK,IPRINT,.FALSE.,
     &                  .FALSE.,.TRUE.,.FALSE.,NODV,MAXDIF,.FALSE.,
     &                  NDMAT,ISYMDM,IFCTYP,IATOM)
C
            IF (KPRINT .GT. 0) THEN
               CALL GETTIM(CPU2,WALL2)
               CPU    = CPU2 - CPU1
               WALL   = WALL2 - WALL1
               TOTWAL = TOTWAL + WALL
C
               IMINS  = INT(TOTWAL)/60
               IHOURS = IMINS/60
               IMINS  = IMINS - 60*IHOURS
               ISECS  = NINT(TOTWAL) - 3600*IHOURS - 60*IMINS
C
               WRITE(6,'(A,I5.2,A,I2.2,A,I2.2)')
     &              '>>>> Total wall time used in PARDRV so far   :',
     &              IHOURS,':',IMINS,':',ISECS
               IF (KPRINT .GT. 5) THEN
                  WRITE(LUPRI,'(A,F11.2,/A,F11.2)')
     &              '>>>> CPU  time used in PARDRV last iteration :',
     &                CPU,
     &              '>>>> Wall time used in PARDRV last iteration :',
     &                WALL
               END IF
            END IF
         ELSE
cdd
          if (.not.d1astr) then
cdd
            IF (IPRINT .GE. 2) CALL TIMERH('START ',TIMSTR,TIMEND)
            CALL TWOINT(WORK(KLAST),LWRK,WORK(KFMAT),WORK(KDMAT),NDMAT,
     &                ISYMDM,IFCTYP,DUMMY,IDUMMY,NUMDIS,1,ITYPE,MAXDIF,
     &                0,NODV,NOPV,NOCONT,TKTIME,IPRINT,IPRNTA,IPRNTB,
     &                IPRNTC,IPRNTD,RETUR,IDUMMY,DOINT)
            IF (IPRINT .GE. 2) CALL TIMERH('TWOINT',TIMSTR,TIMEND)
cdd
	  endif
cdd
         END IF
      END IF
C
      IF (IPRALL .GE. 1) CALL TIMERH ('TWOEXP',TIMEIN,TIMOUT)
#if !defined SYS_CRAY
      IF (RUNSOR) then
      inquire(unit=lupao,opened=op,iostat=itm) 
      if (op .and. itm.eq.0)
     .CLOSE(LUPAO,STATUS='DELETE')
      endif
#endif
      PASS = .TRUE.
C
C     Testing of direct integral calculation
C
      IF (DIRTST) THEN
         CALL FCKTES(WORK,LWORK,MAXDIF,NODV,NOPV,NOCONT,TKTIME,IPRINT,
     &               IPRNTA,IPRNTB,IPRNTC,IPRNTD,RETUR)
      END IF
      IF (CUT) THEN
         WRITE (LUPRI,'(/,A)')
     &          ' Program stopped after TWOEXP as required.'
         WRITE (LUPRI,'(A)') ' No restart file has been written.'
         CALL QUIT(' ***** End of ABACUS (in TWOEXP) *****')
      END IF
      RETURN
      END
C  /* Deck getdmt */
      SUBROUTINE GETDMT(DMAT,NDMAT,WORK,LWORK,NODC,NODV,IPRINT)
#include <implicit.h>
#include <priunit.h>
#include <mxcent.h>
      LOGICAL NODC, NODV
      DIMENSION DMAT(NBAST,NBAST,NDMAT), WORK(LWORK)
#include <abainf.h>
#include <inforb.h>
      IF (IPRINT .GT. 5) CALL TITLER('Output from GETDMT','*',103)
C
      KCMO  = 1
      KDV   = KCMO  + NCMOT
      KDTSQ = KDV   + NNASHX
      KDASQ = KDTSQ + N2BASX
      KLAST = KDASQ + N2BASX
      IF (KLAST.GT.LWORK) CALL STOPIT('GETDMT','ONEDSF',KLAST,LWORK)
C
      CALL ONEDSF(WORK(KCMO),WORK(KDV),WORK(KDTSQ),WORK(KDASQ),
     &            IPRINT,NODC,NODV)
      ISYMDM = 0
      CALL DSOTAO(WORK(KDTSQ),DMAT(1,1,1),NBAST,ISYMDM,IPRINT)
      CALL DSOTAO(WORK(KDASQ),DMAT(1,1,2),NBAST,ISYMDM,IPRINT)
C
      RETURN
      END
C  /* Deck onedsf */
      SUBROUTINE ONEDSF(CMO,DV,DTSO,DASO,IPRINT,NODC,NODV)
C
C     This subroutine calculates the total and active one-electron
C     density matrices in SO basis (contravariant).  Input is
C     one-electron active density matrix.
C                                         880420  PRT
C
#include <implicit.h>
#include <priunit.h>
#include <mxorb.h>
#include <mxcent.h>
#include <iratdef.h>
      PARAMETER (D0 = 0.0D0)
      INTEGER R, S, U, V, UV, UR, US, VR, VS
      LOGICAL NODC, NODV
      DIMENSION CMO(*), DV(*), DTSO(NBAST,NBAST), DASO(NBAST,NBAST)
C
C Used in common blocks:
C   ABAINF : CCSD
C
#include <abainf.h>
#include <inforb.h>
#include <nuctap.h>
C
C     Read input from LUMC
C
      REWIND LUMC
      IF (CCSD) THEN
         CALL MOLLAB('CANORB  ',LUMC,LUPRI)
         CALL READSQ(LUMC,IRAT*NCMOT,CMO)
         CALL MOLLAB('CCEFF   ',LUMC,LUPRI)
         CALL READSQ(LUMC,IRAT*NNASHX,DV)
      ELSE
         CALL MOLLAB('SIR IPH ',LUMC,LUPRI)
         READ (LUMC)
         READ (LUMC)
         CALL READSQ(LUMC,IRAT*NCMOT,CMO)
         IF (NASHT .GT. 0) THEN
            CALL READSQ(LUMC,IRAT*NNASHX,DV)
         ELSE
            READ (LUMC)
         END IF
      END IF
C
C     Print Section
C
      IF (IPRINT .GT. 03) THEN
         WRITE (LUPRI, '(//,A,/)') ' <<<<< Subroutine ONEDSF >>>>>'
         WRITE (LUPRI, '(A,8I5)') ' NISH ', (NISH(I),I = 1,NSYM)
         WRITE (LUPRI, '(A,8I5)') ' NASH ', (NASH(I),I = 1,NSYM)
         WRITE (LUPRI, '(A,8I5)') ' NOCC ', (NOCC(I),I = 1,NSYM)
         WRITE (LUPRI, '(A,8I5)') ' NORB ', (NORB(I),I = 1,NSYM)
         WRITE (LUPRI, '(A,8I5)') ' NBAS ', (NBAS(I),I = 1,NSYM)
         IF (IPRINT .GE. 05) THEN
            CALL HEADER('Occupied molecular orbitals',0)
            IEND = 0
            DO 1000 ISYM = 1,NSYM
               IF (NBAS(ISYM) .EQ. 0) GOTO 1000
               IF (NOCC(ISYM) .EQ. 0) GOTO 1100
               WRITE (LUPRI, '(//,A,I5,/)') ' Symmetry ', ISYM
               IENDI = 0
               DO 1200 I = 1, NOCC(ISYM)
                  WRITE (LUPRI, '(/,A,I5,/)')
     *                     ' Molecular orbital ', I
                  WRITE (LUPRI, '(6F12.6)')
     *               (CMO(IEND+IENDI+J), J = 1, NBAS(ISYM))
                  IENDI = IENDI + NBAS(ISYM)
1200           CONTINUE
1100           CONTINUE
               IEND = IEND + NORB(ISYM)*NBAS(ISYM)
1000        CONTINUE
            CALL HEADER('Active density matrix (MO basis)',-1)
            CALL OUTPAK(DV,NASHT,1,LUPRI)
         END IF
      END IF
C
C     ***** Construct contravariant SO matrices *****
C
      CALL DZERO(DTSO,N2BASX)
      CALL DZERO(DASO,N2BASX)
      ICEND = 0
      DO 110 ISYM = 1,NSYM
         DO 100 R = 1, NBAS(ISYM)
         DO 100 S = 1,R
C
            DTRS = D0
C
C           (I) Inactive contribution
C
            ICENDI = 0
            DO 300 I = 1, NISH(ISYM)
               DTRS = DTRS + CMO(ICEND+ICENDI+R)*CMO(ICEND+ICENDI+S)
               ICENDI = ICENDI + NBAS(ISYM)
  300       CONTINUE
            DTRS = DTRS + DTRS
            IF (NODC) DTRS = D0
C
C           (II) Active contribution
C
            DVRS = D0
            IF (.NOT. NODV) THEN
               IASHI = IASH(ISYM)
               UV = ((IASHI + 1)*(IASHI + 2))/2
               IDVEND = ICEND + NISH(ISYM)*NBAS(ISYM)
               ICENDU = IDVEND
               DO 400 U = 1,NASH(ISYM)
                  ICENDV = IDVEND
                  DO 410 V = 1, U
                     DUV = DV(UV)
                     IF (ABS(DUV) .GT. D0) THEN
                        TEMP = CMO(ICENDU+R)*CMO(ICENDV+S)
                        IF (U .NE. V) TEMP = TEMP
     *                       + CMO(ICENDU+S)*CMO(ICENDV+R)
                        DVRS = DVRS + DUV*TEMP
                     END IF
                     UV = UV + 1
                     ICENDV = ICENDV + NBAS(ISYM)
  410             CONTINUE
                  UV = UV + IASHI
                  ICENDU = ICENDU + NBAS(ISYM)
  400          CONTINUE
            END IF
            IR = IBAS(ISYM) + R
            IS = IBAS(ISYM) + S
            DTSO(IR,IS) = DTRS + DVRS
            DTSO(IS,IR) = DTRS + DVRS
            DASO(IR,IS) = DVRS
            DASO(IS,IR) = DVRS
  100    CONTINUE
         ICEND = ICEND + NORB(ISYM)*NBAS(ISYM)
110   CONTINUE
C
C     ***** Print Section *****
C
      IF (IPRINT .GE. 10) THEN
         CALL HEADER('Total SO density matrix (not folded)',-1)
         CALL OUTPUT(DTSO,1,NBAST,1,NBAST,NBAST,NBAST,1,LUPRI)
         CALL HEADER('Active SO density matrix (not folded)',-1)
         CALL OUTPUT(DASO,1,NBAST,1,NBAST,NBAST,NBAST,1,LUPRI)
      END IF
      RETURN
      END
