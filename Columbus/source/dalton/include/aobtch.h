      LOGICAL ACTVBT
      COMMON /AOBTCH/ EXPBT(MXPRIM),  CCFBT(MXPRIM*MXCONT),
     &                CORXBT(MXSHEL), CORYBT(MXSHEL), CORZBT(MXSHEL),
     &                NHKTBT(MXSHEL), KCKTBT(MXSHEL), KHKTBT(MXSHEL),
     &                NPRFBT(MXSHEL), NCTFBT(MXSHEL), ISTBBT(MXSHEL),
     &                MULTBT(MXSHEL), NCNTBT(MXSHEL),
     &                INDXBT(MXSHEL*MXCONT,0:7),
     &                KNDXBT(MXSHEL),
     &                KEXPBT(MXSHEL), KCCFBT(MXSHEL),
     &                KAOSRT(MXSHEL),
     &                NORBBT(MXSHEL), ACTVBT(MXSHEL,4),
     &                NAOBCH, NBASIS, NGAB, IORBRP(0:7),
     &                MAXQN, KQNBT(MXQN), NQNBT(MXQN)
