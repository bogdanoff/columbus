      LOGICAL OMEGSQ,T2TCOR,OMEGOR,CC3LR,RSPIM,LSEC,LCOR,NEWGAM,INTTR
      COMMON /CCSDSYM/ NT1AMX, NT2AMX, NT1AM(8), NT2AM(8), NNBST(8),
     &                 NDISAO(8), NDSRHF(8), IDSAOG(8,8), ILMRHF(8),
     &                 ILMVIR(8), IT1AM(8,8), IT2AM(8,8), IAODIS(8,8),
     &                 NLAMDT, NLMRHF, IRHF(8), IVIR(8) , NEMAT1(8),
     &                 IEMAT1(8,8), NGAMMA(8), NT1AO(8), NT2AO(8),
     &                 IT1AO(8,8), IT2AO(8,8), N2BST(8), NT2BCD(8),
     &                 IT2BCD(8,8), NT2BGD(8), IT2BGD(8,8), IT2SQ(8,8),
     &                 NMATIJ(8), IMATIJ(8,8), IGAMMA(8,8),IDSRHF(8,8),
     &                 IT1AOT(8,8), IT1AMT(8,8), IT2BCT(8,8),
     &                 IT2BGT(8,8), IFCVIR(8,8), IFCRHF(8,8),
     &                 IMATAB(8,8), NMATAB(8), NT2AOS(8), IT2AOS(8,8),
     &                 NT2SQ(8), NMIJP(8), IMIJP(8,8), NT2ORT(8),
     &                 IT2ORT(8,8),
     &                 ICKID(8,8),NCKI(8),ICKI(8,8),ICKITR(8,8),
     &                 NTOTOC(8),NTRAOC(8),ICKASR(8,8),NCKIJ(8),
     &                 ICKAD(8,8),NCKA(8),ICKA(8,8),ICKALP(8,8),
     &                 ICKATR(8,8),NCKATR(8),ISAIK(8,8),ISAIKJ(8,8),
     &                 NMAJIK(8),ISJIK(8,8),ISJIKA(8,8),ISAIKL(8,8),
     &                 IMATAV(8,8),NMATAV(8),ICKDAO(8,8),NT1AOX,
     &                 ICKBD(8,8),ISYMOP,IPRCC,NGLMDT(8),NGLMRH(8),
     &                 IGLMRH(8,8),IGLMVI(8,8),NT2MMO(8,8),NT2MAO(8,8),
     &                 IT2SP(8,8),ISEC(8),ICOR(8),
     &                 ILRHSI(8),ILVISI(8),NLRHSI,NLRHFR(8),
     &                 I3ODEL(8,8),I3ORHF(8,8),N3ORHF(8),IT2AIJ(8,8),
     &                 NT2AIJ(8),NMAIJK(8),IMAIJK(8,8),NCKASR(8),
     &                 IMAIJA(8,8),NMAIJA(8),ID2IJG(8,8),ID2AIG(8,8),
     &                 ID2ABG(8,8),ND2IJG(8),ND2AIG(8),ND2ABG(8),
     &                 IFCKDO(8),IFCKDV(8),
     &                 OMEGSQ,T2TCOR,OMEGOR,CC3LR,RSPIM,LSEC,LCOR,
     &                 NEWGAM,INTTR
C
      INTEGER A,B,C,D,E,F,G,P,Q,R,S,I,J,K,L,M,N
