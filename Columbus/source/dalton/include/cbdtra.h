      LOGICAL SRTCOO, SRTSYM
      COMMON /CBDTRA/ THRP,THRQ,
     &                IPRTRD,LUWTRA,NUMSIM,JTER,
     &                ISTW(MAXORB),ISTX(MAXORB),
     &                ISYMO(MAXORB),ISYMA(MAXORB),
     &                LUINTG,LUHTID,LUDAT1,LUDAT2,
     &                LASTAD(700,3),LASTAE(700),
     &                NPQ,NCD,NUMCHA,LDABUF,LDAMAX,
     &                LTRI,LPQCD,LDABUG,LPQRS,
     &                SRTCOO(3), SRTSYM(8)
