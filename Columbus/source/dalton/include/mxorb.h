#if defined (VAR_TESTIBM)
      PARAMETER (MXSHEL =  15, MXPRIM =  60, MXCORB =  30,
#else
#if defined (VAR_SIRBIG)
      PARAMETER (MXSHEL = 400, MXPRIM = 2500, MXCORB = 1200,
#else
      PARAMETER (MXSHEL = 500, MXPRIM = 2500, MXCORB = 2500,
#endif
#endif
     *           MXORBT = MXCORB*(MXCORB + 1)/2)
