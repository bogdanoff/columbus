      PARAMETER (MXFREQ = 50)
      LOGICAL SKIP,DIRECT,CCRSTR,CCSD,CCSDT,CC2,CC3,CC1B,CCPT,CCP3,
     &        CCRT,CCR3,CCR1A,CCR1B,CCS,MP2,CCP2,CCD,CC1A,FROIMP,
     &        FROEXP,NOCCIT,CCSAVE,STOLD,MCC21,MCC22,MCC2SD,COMBI,
     &        JACEXP,RESTLR,CCT,ONEP,RELORB,NOFCON,OSCSTR,POLARI,
     &        FTST,TSTDEN,DIPMOM,QUADRU,NQCC
      COMMON /CCSDINP/ FREQ(MXFREQ),IPRINT,MXDIIS,SKIP,
     &                 DIRECT,CCRSTR,CCSD,CCSDT,CC2,CC3,CC1B,
     &                 CCPT,CCP3,CCRT,CCR3,CCR1A,CCR1B,
     &                 CCS,MP2,CCP2,CCD,CC1A,FROIMP,FROEXP,NOCCIT,
     &                 CCSAVE,STOLD,MCC21,MCC22,MCC2SD,COMBI,JACEXP,
     &                 RESTLR,CCT,LURES,LUEXE,ONEP,
     &                 RELORB,CIS,OSCSTR,POLARI,FTST,NFREQ,ICHANG,
     &                 MXLRV,TSTDEN,DIPMOM,QUADRU,NQCC
