       double precision, pointer :: point(:,:)
       double precision, pointer :: gradient(:)
       double precision, pointer :: pcharge(:),phi(:)
        integer idbg,npp,ippblocksize,ngrad
        logical  density,hmat,grad,nad,pot,selfe

       common /potentialptr/point,phi,npp,ippblocksize,density,
     .                hmat,pcharge,grad,nad,pot,gradient,ngrad,idbg,
     .                selfe

