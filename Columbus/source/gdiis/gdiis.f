!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      subroutine absmax(n, a, imax, xmax)
c     # this routine returns the highest absolute value in the
c     # array a, from a(1) to a(n)
c     #
c     # input:
c     # n      = vector length.
c     # a(1:n) = input vector
c     #
c     # output:
c     # imax   = index of the largest array element.
c     # xmax   = abs(a(imax)) the magnitude of the largest element.
       implicit none
      integer n, imax
      real*8 a(n), xmax
c
      integer j
c
      imax = 1
      xmax = abs(a(1))
      do 100 j = 2, n
        if (xmax .lt. abs(a(j))) then
           xmax = abs(a(j))
           imax = j
        end if
 100  continue
c
      return
      end
      real*8 function arc1(x, y)
c
       implicit none 
       real*8 x,y,s
c
      real*8    zero,     small
      parameter(zero=0d0, small=1d-11)
      real*8     pi
      parameter( pi=3.14159265358979323846264338328d0 )
      real*8     two
      parameter( two=2d0 )
      real*8     pis2
      parameter( pis2 = pi / two )
c
      if (abs(x) .lt. small) go to 10
      s = atan( y / x )
      if (x .lt. zero) s = s + pi
      arc1 = s
      return
10    arc1 = pis2
c
      return
      end
      subroutine bbt(nq,nek,b,ibcontr,x,y,t)
c
       implicit none
c  This subroutine computes y= B(Bt)x. t is just storage
c  parameters: INPUT
c              nq=number of internal coordinates
c              nek=3*na, number of Cartesians
c              b(199,nq): contains the non-zero elements of B
c              ibcontr(99,nq): coding info for B
c              x(nq): input vector
c              OUTPUT
c              y: B(Bt)x
c
      integer nq,nek
      integer ibcontr(99,nq)
c
      real*8 b(199,nq),x(nq),y(nq),t(nek)
c
c
      call btxv(nq,nek,b,ibcontr,x,t)
      call bxv(nq,nek,b,ibcontr,t,y)
c
      return
      end
      subroutine bdiag1(nq,nek,b,ibcontr,dm1)
c
       implicit none
c  parameters: INPUT
c              nq=number of internal coordinates
c              nek=3*na, number of Cartesians
c              b(199,nq): contains the non-zero elements of B
c              ibcontr(99,nq): coding info for B
c              OUTPUT
c              dm1=diag(BtransposeB)**-1 (inverse)
c
c   constants
      real*8          zero,  half,  one,   two, three,  four,  five,
     &                ten,   ten6,  tenm8, pi,  acc
      common /number/ zero,  half,  one,   two, three,  four,  five,
     &                ten,   ten6,  tenm8, pi,  acc
c
      real*8 eps
      parameter(eps=1.0d-9)
c
      integer i,iat3,iatom
      integer k3,k
      integer nq,nek,natom
      integer ibcontr(99,nq)
c
      real*8 b(199,nq),dm1(nek)
c
c------------------------------------------------------------
c
      call wzero(nek,dm1,1)
      do i=1,nq
        natom=ibcontr(2,i)
        k3=0
        do k=1,natom
          k3=k3+3
          iatom=ibcontr(k+2,i)
          iat3=iatom*3
          dm1(iat3-2)=dm1(iat3-2)+b(k3-2,i)**2
          dm1(iat3-1)=dm1(iat3-1)+b(k3-1,i)**2
          dm1(iat3)=dm1(iat3)+b(k3,i)**2
        end do
      end do
      do k=1,nek
        if(abs(dm1(k)).gt.eps) then
          dm1(k)=one/dm1(k)
        else
          dm1(k)=eps
        end if
      end do
c
      return
      end
      subroutine bdiag(nq,nek,b,ibcontr,dm1)
c
       implicit none
c  parameters: INPUT
c              nq=number of internal coordinates
c              nek=3*na, number of Cartesians
c              b(199,nq): contains the non-zero elements of B
c              ibcontr(99,nq): coding info for B
c              OUTPUT
c              dm1=diag(BBtranspose)**-1 (inverse)
c
c   constants
      real*8          zero,  half,  one,   two, three,  four,  five,
     & ten,   ten6,  tenm8, pi,  acc
      common /number/ zero,  half,  one,   two, three,  four,  five,
     & ten,   ten6,  tenm8, pi,  acc
c
      integer i
      integer k3,k
      integer nq,nek,natom
      integer ibcontr(99,nq)
c
      real*8 b(199,nq),dm1(nq),s
c
c
      do i=1,nq
         s=zero
         natom=ibcontr(2,i)
         k3=0
         do k=1,natom
            k3=k3+3
            s=s+b(k3-2,i)**2
            s=s+b(k3-1,i)**2
            s=s+b(k3,i)**2
         end do                 ! do k=1,natom
         if (s.ne.0.0d+00) then
            dm1(i)=one/s
         else
            dm1(i)=zero
         endif
      end do                    ! do i=1,nq
c
      return
      end
      subroutine bfgs(
     & ngeo, nq, lq,    md,
     & qd,   fd, zk,    nfix,
     & ifix, c,  fcold, writ,
     & nlist)
c
c     this subroutine performs a bfgs update on the
c     inverse  force constant matrix c
c     see r. fletcher, practical methods of optimization
c     p. 44 (wiley, chichester, 1980)
c     parameters
c     ngeo (input) - number of geometries
c     nq (input)   - number of int. coordinates
c     qd(lq,md)  (input)  - internal coordinates
c     fd(lq,md)  (input)  - internal forces
c     zk (workspace)
c     nfix (input) - number of fixed coordinates
c     ifix(nfix)  (input) - fixed coordinates
c     c(nq**2)    (input and output) inverse force const. matrix
c     writ   if true then print hessian
ctm
       implicit none
c     # dummy:
      integer ngeo, nq, lq, md, nfix, nlist
      integer ifix(*)
      real*8 qd(lq,md), fd(lq,md), zk(*), c(*), fcold(lq)
      logical writ
c
c     # local:
      integer    lqmax
      parameter (lqmax= 800)
c
      integer adr, i, ij, j, ji, nm1
      integer iwork(lqmax)
c
      real*8 den, deter, qdi, qdj, s, ss
      real*8 b(lqmax*lqmax), a(lqmax*lqmax), dummy(lqmax), eigen(lqmax)
      real*8 oldc(lqmax*lqmax)
c
      logical singul
c
      character*8 labr,       labc
      parameter(  labr='row', labc='col' )
c
      real*8    zero,     one,     xtold
      parameter(zero=0d0, one=1d0, xtold=1d-8)
c
      if (lqmax .lt. lq)
     & call bummer ('bfgs: change lqmax to ',lq,2)

ctm
      call dcopy_wr(nq*nq,c,1,oldc,1)
c     call prblkc('old invers hessian',c,nq,nq,nq,labr,labc,1,nlist)
      nm1=ngeo-1
      if (nm1.lt.1) return
      do 810 i=1,nq
         s = zero
         do 800 j=1,nq
            ji=(j-1)*nq+i
            s=s+c(ji)*(fd(j,ngeo)-fd(j,nm1))
800      continue
         zk(i)=s
810   continue
      den = zero
      s=den
      do 820 i=1,nq
         s=s+zk(i)*(fd(i,ngeo)-fd(i,nm1))
         den=den+(fd(i,ngeo)-fd(i,nm1))*(qd(i,ngeo)-qd(i,nm1))
820   continue
c     # den is delta f*delta q
      s = one - s / den
      do 871 i=1,nq
         qdi=qd(i,ngeo)-qd(i,nm1)
         do 870 j=1,i
            qdj=qd(j,ngeo)-qd(j,nm1)
            ij=(j-1)*nq+i
            ji=(i-1)*nq+j
            ss= (qdi*qdj*s+qdi*zk(j)+qdj*zk(i))/den
            c(ij)=c(ij)-ss
            if (i.ne.j) c(ji)=c(ji)-ss
870      continue
871   continue
c
      adr = 0
      do 881 i = 1, nq
         do 880 j = 1, i
            adr    = adr + 1
            ij     = (i-1) * nq + j
            a(adr) = c(ij)
880      continue
881   continue
      call plblkt('Updated inverse HESSIAN matrix',a,nq,labr,1,nlist)
      call givens(nq,-nq,nq,a,b,eigen,dummy,0)
      write(nlist,*)
      write(nlist,*) 'Eigenvalues of updated HESSIAN'
      write(nlist,900) (one/eigen(i),i=1,nq)
900   format(10f8.3)
      singul = .false.
      do 910 i = 1, nq
         if (eigen(i) .lt. zero) then
c           # hessian matrix is indefinite.
            singul = .true.
            goto 911
         endif
910   continue
911   continue
      if (singul) then
         call dcopy_wr(nq*nq,oldc,1,c,1)
         write(nlist,*)
         write(nlist,*) 'because at least one eigenvalue ' //
     &    'is negative, the old hessian is kept'
      endif
c      call prblkc('invers hessian',c,nq,nq,nq,labr,labc,1,nlist)
      call dcopy_wr(nq*nq,c,1,b,1)
      call osinv(b,nq,deter,xtold,zk,iwork)
      if (deter .eq. zero) then
         write(nlist,920)
920      format('Hessian is singular')
      endif
      if (writ) then
         call prblkc('new hessian',b,nq,nq,nq,labr,labc,1,nlist)
      endif
      call osinv(b,nq,deter,xtold,zk,iwork)
      call dcopy_wr(nq*nq,b,1,c,1)
      end
      subroutine bread(na, nq, nprim, ibcode, ibcontr, wri)
c
c   this routine reads the b matrix information and converts it to the
c   integer arrays ibcode(6,nprim) and ibcontr(99,nq). nprim is
c   the number of primitive internal coordinates, ibcode(1,ip) is the
c   type: 1=stretching, 2=invr (1/r), 3=inv6 (1/r**6),4=bending,
c   5=out of plane, 6=torsion, 7=linear1, 8=linear2
c   ibcode(2,ip) is the coefficient of the coordinate in the composite
c   internal coordinate. It is the product of the overall scale factor
c   for the coordinate and the normalized coefficiient. It
c   is expressed as an integer, in units of 10**(-8) (to avoid
c   defining a structure containing both real and integer elements)
c   ibcode(3..6,ip) are the atoms participating in the coordinate
c   for stretchings only ibcode(3,ip) and ibcode(4,ip) are used etc.
c   ibcontr(nprim+1) is an array describing the contraction:
c   in goes from ibcontr(1,k-1)+1 to ibcontr(1,k), except when k=1.
c   ibcontr(2,k) holds the number of different atoms appearing in
c   the contracted internal coordinate (max. 10), and ibcontr(3..20,k)
c   gives these atoms.
c
C                                   INPUT DATA
C        EACH ELEMENTARY VALENCE COORDINATE ON A SEPARATE CARD
C        COL. 1 'K' OR' ' (BLANK). IF 'K' A NEW COORDINATE BEGINS, IF
C        BLANK THEN THE COMPOSITE INTERNAL COORDIMNATE BEGUN EARLIER IS
C         CONTINUED. ANY OTHER CHARACTER TERMINATES THE INPUT
C        COLS. 2-9 SCALE FACTOR FOR THE TOTAL COORDINATE (ONLY IF THERE
C        IS K IN COLUMN 1. BLANK OR ZERO IS INTERPRETED AS 1.0
C        COLS. 21-24 COORDINATE TYPE STRE,INVR,BEND,OUT ,TORS,LIN1,LIN2
C        COLS. 31-40,41-50,51-60,61-70 PARTICIPATING ATOMS A,B,C,D
C         FORMAT 4F10.X
C         A AND B ARE GIVEN FOR 'STRE' ORDER ARBITRARY
C         A AND B ARE GIVEN FOR -INVR- ORDER ARBITRARY
C        A,B,C FOR BEND - A AND B ARE END ATOMS, C IS THE APEX ATOM A-C-
C        ATOM A OUT OF THE BCD PLANE - C IS THE CENTRAL ATOM - COORDINA
C        TE POSITIVE IF A IS DISPLACED TOWARD THE VECTOR PRODUCT DB*DC
C        TORSION A-B-C-D, POSITIVE AS IN THE WILSON-DECIUS-CROSS BOOK
C        NOTE THAT THE VALUE OF THE COORDINATE VARIES BETWEEN -PI/2 TO
C        3PI/2   N O T  BETWEEN -PI/2 TO +PI/2
C        LIN1 L  COLLINEAR BENDING A-B-C DISTORTED IN THE PLANR OF ABD
C        POSITIVE IF A MOVES TOWARD D
C         LIN2 LINEAR BENDING A-C-B DISTORTED PERPENDICULAR TO THE PLANE
C        ABD - POSITIVE IF A MOVES TOWARD THE VECTOR CROSS PRODUCT CD*CA
C        THE LINEAR BENDINGS ARE A-C-B, I. E. THE APEX ATOM IS THIRD
c
       implicit none
c     ##  parameter & common block section
c
      integer   ntyp
      parameter(ntyp=8)
c
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
c     # unit numbers
      integer       inp, inp2, nlist, ipun, iarc, icond, itest
      common /tape/ inp, inp2, nlist, ipun, iarc, icond, itest
c
      integer        ngeom,   nforc,   nintc,  nfmat,   nform,
     & nener,   inpf3,   nmess,  nhess,   nhessinv,
     & ncuriter
      common /tape2/ ngeom,   nforc,   nintc,  nfmat,   nform,
     & nener,   inpf3,   nmess,  nhess,   nhessinv,
     & ncuriter
c
c     ##  integer section
c
      integer ibcode(6,*), ibcontr(99,*), ia(4), i, ilow, ihigh, iatom
      integer ka, kb, kc, kd, k, kk
      integer l
      integer nprim
      integer ncard, nq, na
      EQUIVALENCE (KA,IA(1)), (KB,IA(2)), (KC,IA(3)), (KD,IA(4))
c
c     ##  real*8 section
c
      real*8 a(4)
      real*8 c1, ccx, ccc, c, cc
c
      real*8    zero,     one,     ainc,      ten8
      parameter(zero=0d0, one=1d0, ainc=1d-2, ten8=1d8)
c
c     ##  logical section
c
      logical wri, already
c
c     ##  data section
c
      CHARACTER*1 WEW(80), WORT(3), WE
      CHARACTER*4 TIPUS(ntyp), BLANK, TYP, TLAST
      CHARACTER*10 TENCHA, LBLANK
      DATA TIPUS/'STRE','INVR','INV6','BEND','OUT ','TORS',
     1 'LIN1','LIN2'/
      DATA WORT/'K',' ','C'/
      DATA BLANK/'    '/
      DATA LBLANK/'          '/
c
c-----------------------------------------------------------------------
c
      NCARD = 0
c     # this is the (contracted) coordinate counter
      I     = 0
      C1    = zero
      CCX   = zero
      TLAST = BLANK
      IF (WRI) WRITE (nlist,10)
10    FORMAT (/' DEFINITION OF INTERNAL COORDINATES'/)
20    READ (nintc,30,END=45) WEW
30    FORMAT (80A1)
      WE = WEW(1)
      BACKSPACE  nintc
      DO K = 1, 3
         IF (WE .EQ. WORT(K)) GO TO 60
      enddo
c
45    CONTINUE
      IF (I .EQ. 0) THEN
         WRITE(nlist,*) ' NO INTERNAL COORDINATES WERE FOUND ON',nintc
         IF(ICOND.NE.0) WRITE(ICOND,*)
     1    ' NO INTERNAL COORDINATES WERE FOUND ON',nintc
         CALL bummer
     &    ('NO INTERNAL COORDINATES WERE FOUND ON',nintc,faterr)
      ENDIF
      C1 = SQRT(one / C1) * CCX
c     # put in the last value for ibcontr
      nq = i
      ibcontr(1,nq) = ncard
      if(i .eq. 1) then
         ilow = 1
      else
         ilow = ibcontr(1,i-1) + 1
      end if
      ihigh = ibcontr(1,i)
      do k = ilow, ihigh
         ibcode(2,k) = ibcode(2,k) * c1
      enddo
      GO TO 370
c
60    CONTINUE
C     # CHECK IF THE BLANK IS REALLY BLANK
      if (we .eq. wort(2)) then
         READ (nintc,65) TENCHA
         BACKSPACE nintc
         IF (TENCHA .NE. LBLANK) GO TO 45
      endif
65    FORMAT(A10)
      NCARD = NCARD + 1
      READ (nintc,70) WE,CC,C,TYP,A
70    FORMAT (A1,F9.5,F10.4,A4,6X,4F10.4)
      if(typ.eq.blank.and.ncard.eq.1) then
         call bummer('first coordinate must be defined',0,faterr)
      endif
      IF (TYP .EQ. BLANK) TYP = TLAST
      TLAST = TYP
      if (cc .eq. zero) cc = one
      if (we .eq. wort(1)) then
         CCC = CCX
         CCX = CC
         ibcontr(1,i) = ncard - 1
      endif
      IF (C .EQ. zero) C = one
      IF (WE .EQ. WORT(2) .OR. WE .EQ. WORT(3)) C1 = C1 + C**2
      if (we .eq. wort(1)) then
         if (i .ne. 0) then
            IF (WRI) WRITE (nlist,'(1x)')
            C1 = SQRT(one / C1) * CCC
            if(i .eq. 1) then
               ilow = 1
            else
               ilow = ibcontr(1,i-1) + 1
            end if
            ihigh = ibcontr(1,i)
            do k = ilow, ihigh
               ibcode(2,k) = ibcode(2,k) * c1
            enddo
         endif
         I  = I + 1
         C1 = C**2
      endif
      do k=1,4
         ia(k) = a(k) + ainc
         ibcode(k+2,ncard) = ia(k)
      enddo
      do k = 1, ntyp
         IF (TYP.EQ.TIPUS(K)) GO TO 170
      enddo
      WRITE (nlist,160) I
      IF (ICOND.NE.0) WRITE (ICOND,160) I
160   FORMAT (/' UNDEFINED INT.COORDINATE TYPE AT NO. ',I3/1x,40('*'))
      CALL bummer(
     & 'UNDEFINED INT.COORDINATE TYPE AT NO.',i,faterr)
c
170   IF (WRI) WRITE (nlist,180) I,TYP,IA,C,CCX
180   FORMAT (1X,I3,1H.,A8,4I10,F12.5,F12.6)
      IF (KA.LT.1.OR.KA.GT.NA.OR.KB.LT.1.OR.KB.GT.NA) GO TO 350
      IF (K.GT.3.AND.(KC.LT.1.OR.KC.GT.NA)) GO TO 350
      IF (K.GT.4.AND.(KD.LT.1.OR.KD.GT.NA)) GO TO 350
      ibcode(1,ncard) = k
      ibcode(2,ncard) = c * ten8
      go to 20
c
350   WRITE (nlist,360) I
      IF (ICOND.NE.0) WRITE (ICOND,360) I
360   FORMAT (/' ATOMS ERRONOUSLY DEFINED,COORDINATE NO. ',I3/
     & 1X,40('*'))
370   continue
c     # fill in incontr
      do 500 i = 1, nq
         if(i .eq. 1) then
            ilow = 1
         else
            ilow = ibcontr(1,i-1) + 1
         end if
         ihigh = ibcontr(1,i)
c        # zero out ibcontr
         do 460 kk = 3, 12
            ibcontr(kk,i) = 0
460      continue
c        # number of different primitives
         nprim = 0
         DO 490 k = ilow, ihigh
            do 480 l = 3, 6
               iatom = ibcode(l,k)
               if(iatom .eq. 0) go to 480
               already = .false.
               do 470 kk = 3, 20
                  if(iatom .eq. ibcontr(kk,i)) already = .true.
                  if(ibcontr(kk+1,i) .eq. 0) go to 475
470            continue
475            if( .not. already) then
                  nprim = nprim + 1
                  if(nprim .gt. 18) call bummer('nprim.gt.18',0,faterr)
                  ibcontr(nprim+2,i) = iatom
                  ibcontr(2,i) = nprim
               endif
480         continue
490      continue
500   continue
      RETURN
      end
      subroutine btb(nek,nq,b,ibcontr,x,y,t)
c
       implicit none
c  This subroutine computes y= (Bt)Bx. t is just storage
c  parameters: INPUT
c              nq=number of internal coordinates
c              nek=3*na, number of Cartesians
c              b(199,nq): contains the non-zero elements of B
c              ibcontr(99,nq): coding info for B
c              x(nq): input vector
c              OUTPUT
c              y: (Bt)Bx
c
      integer nq,nek
      integer ibcontr(99,nq)
c
      real*8 b(199,nq),x(nek),y(nek),t(nq)
c
      call bxv(nq,nek,b,ibcontr,x,t)
      call btxv(nq,nek,b,ibcontr,t,y)
c
      return
      end
      subroutine btxv(nq,nek,b,ibcontr,v,w)
c
       implicit none
c  this routine multiplies a vector v by B transpose and puts
c  the result in w
c  parameters: INPUT
c              nq=number of internal coordinates
c              nek=3*na, number of Cartesians
c              b(199,nq): contains the non-zero elements of B
c              ibcontr(99,nq): coding info for B
c              v(nq): input vector
c              OUTPUT
c              w=B(transpose)*v
c
      integer i,iatom,iat3
      integer k,k3
      integer nq,nek,natom
      integer ibcontr(99,nq)
c
      real*8 b(199,nq),v(nq),w(nek)
c
c-------------------------------------------------------
c
      call wzero(nek,w,1)
      do i=1,nq
         natom=ibcontr(2,i)
         k3=0
         do k=1,natom
            k3=k3+3
            iatom=ibcontr(k+2,i)
            iat3=iatom*3
            w(iat3-2)=w(iat3-2)+b(k3-2,i)*v(i)
            w(iat3-1)=w(iat3-1)+b(k3-1,i)*v(i)
            w(iat3)=w(iat3)+b(k3,i)*v(i)
         end do
      end do
c
      return
      end
      subroutine bxv(nq,nek,b,ibcontr,w,v)
c
       implicit none
c  parameters: INPUT
c              nq=number of internal coordinates
c              nek=3*na, number of Cartesians
c              b(199,nq): contains the non-zero elements of B
c              ibcontr(99,nq): coding info for B
c              w(nek): input vector
c              OUTPUT
c              v=B*w
c
c   constants
      real*8          zero,  half,  one,   two, three,  four,  five,
     & ten,   ten6,  tenm8, pi,  acc
      common /number/ zero,  half,  one,   two, three,  four,  five,
     & ten,   ten6,  tenm8, pi,  acc
c
      integer i,iatom,iat3
      integer nq,nek,natom
      integer k3,k
      integer ibcontr(99,nq)
c
      real*8 b(199,nq),v(nq),w(nek),s
c
c-----------------------------------------------------------
c
      do i=1,nq
         s=zero
         natom=ibcontr(2,i)
         k3=0
         do k=1,natom
            k3=k3+3
            iatom=ibcontr(k+2,i)
            iat3=iatom*3
            s=s+b(k3-2,i)*w(iat3-2)
            s=s+b(k3-1,i)*w(iat3-1)
            s=s+b(k3,i)*w(iat3)
         end do
         v(i)=s
      end do
c
      return
      end
      SUBROUTINE CALLD1 (
     &  M1,      MD,     LQ,   NQ,
     &  QD,      FD,     C,    A,    L,
     &  M,       DQ,     DG,   DC,   NFIX,
     &  IFIX,    MEXCLU, dqd)
c
      IMPLICIT none
C     THIS IS THE CALLING PROGRAM FOR GEOMETRY DIIS
C     PARAMETERS: nlist,ICOND: OUTPUT FILES
C     M1 (INPUT) NUMBER OF GEOMETRIES+1
C     MD (INPUT) MAXIMUM NUMBER OF GEOMETRIES
C     LQ (INPUT) MAXIMUM NUMBER OF INT. COORDINATES
C     NQ (INPUT) ACTUAL NUMBER OF INTERNAL COORDINATES
C     QD  (INPUT) M1-1 SETS OF PREVIOUS COORDINATES
C     FD  (INPUT) M1-1 SETS OF INTERNAL FORCES
C     DQD (INPUT) M1-1 SETS OF error vectors
C     A  INPUT AND OUTPUT) DIIS MATRIX
C     C   INVERSE FORCE CONSTANTS (FOR WEIGHING)
C     L, M  WORK VECTORS, NQ LONG
C     DQ, DG (OUTPUT) INTERPOLATED GEOMETRIES AND FORCES
C     DC   (OUTPUT) DIIS COEFFICIENTS
C     NFIX = (INPUT) NUMBER OF FIXED COORDINATES
C     IFIX = (INPUT) VECTOR OF FIXED COORDINATES
C     MEXCLU (INPUT) NUMBBER OF EXCLUDED VECTORS
c
c   unit numbers
      integer       inp, inp2, nlist, ipun, iarc, icond, itest
      common /tape/ inp, inp2, nlist, ipun, iarc, icond, itest
c
      integer i,IER
      integer j1,jn,j
      integer lq
      integer m1,md,ngeo,mexclu
      integer nfix,nq
      integer ifix(nfix)
c
      REAL*8 L(*),M(*)
      real*8 QD(LQ,MD), FD(LQ,MD), C(*), A(*),
     1 DQ(NQ), DG(NQ), DC(*),XLAM, DQD(LQ,MD)
c
c---------------------------------------------------------------
      ngeo=m1-1
c
 890  continue
c
c     # calculate the diis matrix and obtain the coeficents
c
      call gdiis_sub1(md,mexclu,lq,m1,nq,qd,fd,c,a,l,m,dq,dg,dc,xlam,
     1     nfix,ifix,ier,nlist,dqd)
c
      if (ier.ne.0) then
         mexclu=mexclu+1
         if (mexclu.lt.ngeo-1) then
            write (nlist,920) mexclu
  920       format (' geom. diis matrix singular, excluded',i4)
            go to 890
         else
c
c     # if nothing is left, preper for simple relaxation
c
            write(nlist,930)
 930        format (' no diis ')
            call dcopy_wr(nq,qd(1,ngeo),1,dq,1)
            call dcopy_wr(nq,fd(1,ngeo),1,dg,1)
         end if
      else
c
c     # diis is succesful, print the coeficents and the diis matrix
c
         write (nlist,900)
 900     format (1x,11HDIIS MATRIX)
         do 910 i=1,m1
            j1=(i-1)*m1+1
            jn=(i-1)*m1+i
            write (nlist,1230) (a(j),j=j1,jn)
 1230       format (1x,10e12.4,/,1(1x,10e12.4))
 910     continue
         write (nlist,940) (dc(i),i=1,ngeo)
         write (icond,940) (dc(i),i=1,ngeo)
 940     format (/1X,17HDIIS COEFFICIENTS,/1X,1(10F10.4,/))
         write (nlist,950) xlam
 950     format (/1x,8HLAMBDA =,e12.4)
      end if
      return
      end
      SUBROUTINE CALLD (
     &  M1,      MD,     LQ,   NQ,
     &  QD,      FD,     C,    A,    L,
     &  M,       DQ,     DG,   DC,   NFIX,
     &  IFIX,    MEXCLU)
c
      IMPLICIT none
C     THIS IS THE CALLING PROGRAM FOR GEOMETRY DIIS
C     PARAMETERS: nlist,ICOND: OUTPUT FILES
C     M1 (INPUT) NUMBER OF GEOMETRIES+1
C     MD (INPUT) MAXIMUM NUMBER OF GEOMETRIES
C     LQ (INPUT) MAXIMUM NUMBER OF INT. COORDINATES
C     NQ (INPUT) ACTUAL NUMBER OF INTERNAL COORDINATES
C     QD  (INPUT) M1-1 SETS OF PREVIOUS COORDINATES
C     FD  (INPUT) M1-1 SETS OF INTERNAL FORCES
C     A  INPUT AND OUTPUT) DIIS MATRIX
C     C   INVERSE FORCE CONSTANTS (FOR WEIGHING)
C     L, M  WORK VECTORS, NQ LONG
C     DQ, DG (OUTPUT) INTERPOLATED GEOMETRIES AND FORCES
C     DC   (OUTPUT) DIIS COEFFICIENTS
C     NFIX = (INPUT) NUMBER OF FIXED COORDINATES
C     IFIX = (INPUT) VECTOR OF FIXED COORDINATES
C     MEXCLU (INPUT) NUMBBER OF EXCLUDED VECTORS
c
c   unit numbers
      integer       inp, inp2, nlist, ipun, iarc, icond, itest
      common /tape/ inp, inp2, nlist, ipun, iarc, icond, itest
c
      integer i,IER
      integer j1,jn,j
      integer lq
      integer m1,md,ngeo,MEXCLU
      integer nfix,nq
      integer IFIX(NFIX)
c
      REAL*8 L(*), M(*)
      real*8 QD(LQ,MD), FD(LQ,MD), C(*), A(*),
     1 DQ(NQ), DG(NQ), DC(*),XLAM
c
c---------------------------------------------------------------
      ngeo=m1-1
c
 890  continue
c
c     # calculate the diis matrix and obtain the coeficents
c
      call gdiis_sub (md,mexclu,lq,m1,nq,qd,fd,c,a,l,m,dq,dg,dc,xlam,
     1     nfix,ifix,ier,nlist)
c
      if (ier.ne.0) then
c
c     # if the diis matrix was singular, exclude one step
c     # and construct the diis matrix again
c
         mexclu=mexclu+1
         if (mexclu.lt.ngeo-1) then
            write (nlist,920) mexclu
  920       format (' GEOM. DIIS MATRIX SINGULAR, EXCLUDED',I4)
            go to 890
         else
c
c     # if nothing is left, preper for simple relaxation
c
            write(nlist,930)
 930        format (' NO DIIS ')
            call dcopy_wr(nq,qd(1,ngeo),1,dq,1)
            call dcopy_wr(nq,fd(1,ngeo),1,dg,1)
         end if
      else
c
c     # diis is succesful, print the coeficents and the diis matrix
c
         write (nlist,900)
 900     format (1x,11HDIIS MATRIX)
         do 910 i=1,m1
            j1=(i-1)*m1+1
            jn=(i-1)*m1+i
            write (nlist,1230) (a(j),j=j1,jn)
 1230       format (1x,10e12.4,/,1(1x,10e12.4))
 910     continue
         write (nlist,940) (dc(i),i=1,ngeo)
         write (icond,940) (dc(i),i=1,ngeo)
 940     format (/' DIIS COEFFICIENTS',/1x,(10f10.4,/))
         write (nlist,950) xlam
 950     format(/' LAMBDA =',e12.4)
      end if
      return
      end
      subroutine congrad(
     & nq,   nek,   b,    ibcontr,   x,
     & y,    dm1,   c,    t,         p,
     & r,    z,     oper, condamp)
c
c   this routine solves iteratively the equation Ax=c
c   inek,b,ibcontr,t are strictly arguments for "oper"
c   A is the operation = oper can be bbt or btb;
c   bbt forms y= (BBtranspose)x; t is a temporary variable, nek long
c   Btb forms y= Btranspose*B*x ; t is a  temp. variable
c   Algorithm describe e.g. by A. Greenbaum, C. Li and H. Z. Chao,
c   "Practical Iterative Methods for Large-Scale Computations",
c   ed.  D.L. Boley, D.G. Truhlar, Y. Saad, R.E. Wyatt, L.E. Collins
c   North-Holland, Amsterdam 1989.
c
c   The righ-hand side is c, and the final solution is x.
c   dm1 will hold the inverse diagonal of the matrix A=BBt
c
c   For general implementation, replace oneiter and its arguments
c   and also bdiag. The latter forms and inverts diag(A)**-1
c   precon multiplies a vector with diag(A)**-1
c   y,dm1,t,p,r,z are just workspaces
c
c   Parameters: INPUT
c   nq: number of internal coordinates
c   nek: number of Cartesians (3*na)
c   b: B matrix
c   ibcontr: contraction info for the compactly stored B matrix
c   c: right hand side of the equation
c   dm1: inverse diagonal of the matrix (in this case BBt)
c      OUTPUT
c   x: solution of the equation
c   oper: the external subroutine
c   new parameters added by ph
c   condamp:  damping factor
c   nlist: output filnumber
c
       implicit none
c     ##  parameter & common section
c
c     # constants
      real*8          zero,  half,  one,   two, three,  four,  five,
     & ten,   ten6,  tenm8, pi,  acc
      common /number/ zero,  half,  one,   two, three,  four,  five,
     & ten,   ten6,  tenm8, pi,  acc
c
c     # unit numbers
      integer       inp, inp2, nlist, ipun, iarc, icond, itest
      common /tape/ inp, inp2, nlist, ipun, iarc, icond, itest
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      real*8 onem
      integer maxiter
      parameter(onem=-1.0d0, maxiter=1000)
c
      real*8    small
      parameter(small=1d-14)
c
c     ##  integer section
c
      integer i0, i1, iter, ii, imx
      integer nq, nek


      integer ibcontr(99,nq)
c
c     ##  real*8 section
c
      real*8 aa(2),a(2),absmx
      real*8 b(199,nq),bb
      real*8 c(nq),condamp
      real*8 dm1(nq), damp
      real*8 p(nq)
      real*8 r(nq)
      real*8 sum,s
      real*8 t(nek)
      real*8 x(nq)
      real*8 y(nq)
      real*8 z(nq)
c
c     ##  external section
c
      real*8   ddot_wr
      external ddot_wr
      external oper
c
c-------------------------------
c     # set up circular counter
      i0=1
      i1=2
      damp=condamp
c     # set the first colums of x to D**-1*c   x0=DM*c
      call precon(c,dm1,x,nq)
c     # y=A*x0
      call oper(nq,nek,b,ibcontr,x,y,t)
c     # put in a little damping
      call daxpy_wr(nq,damp,x,1,y,1)
c     # r0=c-Ax0
      call dcopy_wr(nq,c,1,r,1)
      call daxpy_wr(nq,onem,y,1,r,1)
c     # z=DM*r0
      call precon(r,dm1,z,nq)
c     # p0=z
      call dcopy_wr(nq,z,1,p,1)
c     # aa0=z(t)r0
      sum=ddot_wr(nq,z,1,r,1)
      aa(1)=sum
c
      iter=0
100   continue
      iter=iter+1
c     # y=Ap(k-1)
      call oper(nq,nek,b,ibcontr,p,y,t)
c     # put in a little damping
      call daxpy_wr(nq,damp,p,1,y,1)
c     # a(k-1)=aa(k-1)/p(k-1)(t)y
      sum=ddot_wr(nq,p,1,y,1)
      a(i0)=aa(i0)/sum
c&ps
      if(sum .eq. zero) a(i0) = aa(i0)
c&ps
c     # x(k)=x(k-1)+a(k-1)p(k-1)
      call daxpy_wr(nq,a(i0),p,1,x,1)
c     # r(k)=r(k-1)-a(k-1)*y
      s=-a(i0)
      call daxpy_wr(nq,s,y,1,r,1)
c     # check for convergence
      call absmax(nq,r,imx,absmx)
      if((absmx .lt. small) .or. (iter .gt. maxiter)) go to 1000
c     # z=DM*r(k)
      call precon(r,dm1,z,nq)
c     # aa(k)=z(t)*r(k);   bb=aa(k)/aa(k-1)
      sum=ddot_wr(nq,z,1,r,1)
      aa(i1)=sum
      bb=aa(i1)/aa(i0)
c     # p(k)=z+bb*p(k-1)
      call dscal_wr(nq,bb,p,1)
      call daxpy_wr(nq,one,z,1,p,1)
c
      ii=i1
      i1=i0
      i0=ii
      go to 100
1000  continue
      write(nlist,
     & '(15x,''linear equation solved in '',i3,'' itererations'')')
     & iter
      write(nlist,'(15x,''lin. eq. sol. residuum= '',e13.8)') absmx
c
      if (iter .gt. maxiter) then
         call bummer(
     &    'error in congrad, no convergence, iter=', iter, faterr)
      endif
      return
      end
      subroutine coninthes1(fc,nq,nqint,c)
c
       implicit none
c   this soubroutine builds the Q (second derivative) matrix
c   for the conical intersection calculation
c
c          q     fi2   hi
c          =     ---   --
c    Q =   fi2+   0    0
c    =     ---
c          hi+    0    0
c          --
c
c    with fi2 is derivative vector of the forces (SIGN!!!!)
c         ---
c         hi  is derivative of the coupling (SIGN!!!!!)
c         --
c         q   is the second derivative of the functional.
c         =
c
c    presently:  q = h(diag)
c                =   =
c                which assumes that 1) h1=h2 and t
c                                      =   =
c                             2) the sec. deriv. of the coupling is 0
c
C
c    these are the parameters for the global geometry optimization
c
      integer nq,nqint
      real*8 fc(nq),c(nq,nq)
c
c memory depository
      integer lcx
      parameter (lcx=99000)
      real*8 bl
      common bl(lcx)
c
c local variables
      logical yesno,yesno1,yesno2
      integer nunit,info,iii,jjj,file,i,j,k,ij,icount
c&      integer ibloecke,ibreite,
      integer iscr,kpvt
c
c dimensions
      integer ma,lq,mq, mq2
      parameter (ma=200)
      parameter (lq=3*ma)
      parameter (mq=lq-6+2)
c   md is the max. number of gdiis geometry steps (or bfgs)
      integer md
      parameter (md=100)
c
c  common related to conical intersection calculations
      real*8 fc1,fc2,h1,fi1,fi2,hi,zeta1,zeta2,de,fd1,fd2,fdh
      integer iconint
      common /conint/ fc1(lq),fc2(lq),h1(lq),
     &     fi1(lq),fi2(lq),hi(lq),zeta1,zeta2,de,
     &     fd1(lq,md),fd2(lq,md),fdh(lq,md),iconint
c
c control parameters
      logical writ,lgdi,lfdi,lmur
      integer itopti,mxopti,icoormx,iangs,igeom,ifmat,isad,nfix,
     &        ifix,mgdi,ipu
      real*8  thract, shftpt,chmax,coormx
      common /opti/itopti,mxopti,chmax,thract,shftpt,coormx,iangs,
     +     igeom,ifmat,isad,nfix,ifix(mq),mgdi,lgdi,lfdi,lmur,writ,ipu
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
c   unit numbers
      integer       inp, inp2, nlist, ipun, iarc, icond, itest
      common /tape/ inp, inp2, nlist, ipun, iarc, icond, itest
c
      integer        ngeom,   nforc,   nintc,  nfmat,   nform,
     &               nener,   inpf3,   nmess,  nhess,   nhessinv,
     &               ncuriter
      common /tape2/ ngeom,   nforc,   nintc,  nfmat,   nform,
     &               nener,   inpf3,   nmess,  nhess,   nhessinv,
     &               ncuriter
c
c  # file names
      character*10     gdiisin,  gdiisls,  gdiissm,  default,
     &                 geomfl,   forcfl,   intcfl,   fmatfl,
     &                 formfl,   convfl,   gdiisfl,  gdiismg,
     &                 hessfl,   hessinvfl,curiterfl,zetafile,convfl2
      common /flnames/ gdiisin,  gdiisls,  gdiissm,  default,
     &                 geomfl,   forcfl,   intcfl,   fmatfl,
     &                 formfl,   convfl,   gdiisfl,  gdiismg,
     &                 hessfl,   hessinvfl,curiterfl,zetafile,convfl2
c
c allocate local memeory
c
      call getmem(nq*nq,iscr)
      call getmem(nq,kpvt)
c
c look whether hessian files exist
      inquire(file='hessian_st1',exist=yesno1)
      inquire(file='hessian_st2',exist=yesno2)
      yesno=yesno1.and.yesno2
      if (yesno) then
         write(nlist,
     &    '(/3x,''Hessian matrix read from files: '',2a12/)')
     &    'hessian_st1','hessian_st2'
c
        open(nhess,file='hessian_st1',status='old',form='formatted')
        file=nhess
c
c&        ibloecke=int(nqint/6.0001)+1
c&        do k=1,ibloecke
c&           do j=1,nqint
c&              ibreite=min(6,(nqint-6*(k-1)))
c&              ij=(j-1)*nqint+(k-1)*6
c&              read(file,*) (bl(iscr-1+i),i=ij+1,ij+ibreite)
c&           enddo                ! do j=1,nqint
c&           read(file,*)
c&        enddo                   ! do k=1,ibloecke
c& 30     format(6f14.8)
        icount=iscr-1
        do k=1,nqint
          read(file,30) (bl(i+icount),i=1,nqint)
        icount=icount+nqint
        enddo ! do k=1,nqint
 30     format(8f13.6)
        close(file)
        call dcopy_wr(nqint*nqint,bl(iscr),1,c,1)
        call dscal_wr(nqint*nqint,1.d0+zeta1,c,1)
c&???        call dscal_wr(nqint*nqint,1d0-0.4d0,c,1)
c
        open(nhess,file='hessian_st2',status='old',form='formatted')
        file=nhess
c
c&        ibloecke=int(nqint/6.0001)+1
c&        do k=1,ibloecke
c&           do j=1,nqint
c&              ibreite=min(6,(nqint-6*(k-1)))
c&              ij=(j-1)*nqint+(k-1)*6
c&              read(file,*) (bl(iscr-1+i),i=ij+1,ij+ibreite)
c&           enddo                ! do j=1,nqint
c&           read(file,*)
c&        enddo                   ! do k=1,ibloecke
        icount=iscr-1
        do k=1,nqint
          read(file,30) (bl(i+icount),i=1,nqint)
        icount=icount+nqint
        enddo ! do k=1,nqint
        close(file)
        call daxpy_wr(nqint*nqint,-1.d0*zeta1,bl(iscr),1,c,1)
c&???        call daxpy_wr(nqint*nqint,0.4d0,bl(iscr),1,c,1)
c
c  ## evantually consider also the coupling hessian
c
        inquire(file='hessian_nad',exist=yesno)
        if (yesno) then
         write(nlist,
     &    '(/3x,''Coupling hessian matrix read from file: '',a10/)')
     &    'hessian_nad'
         open(nhess,file='hessian_nad',status='old',form='formatted')
         file=nhess
c
         icount=iscr-1
         do k=1,nqint
          read(file,30) (bl(i+icount),i=1,nqint)
         icount=icount+nqint
         enddo ! do k=1,nqint
         close(file)
         call daxpy_wr(nqint*nqint,zeta2,bl(iscr),1,c,1)
c&???        call daxpy_wr(nqint*nqint,0.4d0,bl(iscr),1,c,1)
        endif ! if(yesno)
c
c
c copy q into the proper position in c
c
        call dcopy_wr(nqint*nqint,c,1,bl(iscr),1)
        ij=0
        do iii=1,nqint
           call dcopy_wr(nqint,bl(iscr+ij),1,c(1,iii),1)
           ij=ij+nqint
        enddo                   ! do iii=1,nqint
c
      else
c
c  ## build up hessian from read the diagonals
c
        write(nlist,
     & '(/3x,''Hessian matrix constructed form diag el. only:'')')
c
        if (ifmat.lt.1) then
          write(nlist,*) 'no fmat ,no geometry relaxation'
          call bummer('no fmat ,no geometry relaxation',0,faterr)
        end if                   ! end of: if (ifmat.lt.1) then
c
        inquire(file=fmatfl,opened=yesno,number=nunit)
        if(.not.yesno) then
         open(unit=nfmat,file=fmatfl,status='unknown',form='formatted')
        else
         nfmat=nunit
        endif ! end of: if(.not.yesno) then
c
        read (nfmat,1170) (fc(j),j=1,nqint)
 1170   format (8f10.7)
c
        write (nlist,1180)
        write (nlist,1220) (fc(j),j=1,nqint)
 1220   format (1x,10f10.5,/,(1x,10f10.5))
 1180   format (/3x,'Force constants:' )
        do iii=1,nqint
          do jjj=1,nqint
           if (iii.eq.jjj) then
            c(iii,jjj)=fc(iii)
           else
            c(iii,jjj)=0.0
           endif
          enddo ! do jjj=1,nqint
        enddo ! do iii=1,nqint
      endif                     ! if (yesno)
c
c now buld up Q (q part is already in c)
c
c fi2 and hi parts (the minus sign is required since fi2 and hi
c                   are forces)
c
        do iii=1,nqint
           c(iii,nqint+1)=-(fi1(iii)-fi2(iii))
           c(nqint+1,iii)=-(fi1(iii)-fi2(iii))
           if(nq.eq.nqint+2) then
              c(iii,nqint+2)=-hi(iii)
              c(nqint+2,iii)=-hi(iii)
           endif
        enddo   ! do iii=1,nqint
c
c  zero out the rest
c
        c(nqint+1,nqint+1)=0.d0
        if(nq.eq.nqint+2) then
           c(nqint+1,nqint+2)=0.d0
           c(nqint+2,nqint+1)=0.d0
           c(nqint+2,nqint+2)=0.d0
        endif
c
c deal with fixc
c
        do i=1,nfix
         iii=ifix(i)
         do jjj=1,nq
          if(iii.ne.jjj) then
            c(iii,jjj)=0.d0
            c(jjj,iii)=0.d0
          endif
         enddo                  ! do jjj=1,nq
        enddo                     ! do i=1,nfix
c
c print out
c
        call prblkt('Q matrix',c,nq,nq,nq,'int','int',1,nlist)
c
c now calculate the inverse
c
         write(nlist,'(/3x,''Calculating the inverse Q matrix'')')
         call dgetrf_wr(nq,nq,c,nq,bl(kpvt),info)
          if (info.ne.0) call bummer(
     &     'coninthes:error in dgetrf_wr, info=',info,faterr)
         call dgetri_wr(nq,c, nq,bl(kpvt), bl(iscr),nq*nq,info)
          if (info.ne.0) call bummer(
     &     'coninthes:error in dgetri, info=',info,faterr)
c
c print out
c
        call prblkt('Inverse Q matrix',c,nq,nq,nq,'int','int',1,nlist)
        write(nlist,'(/)')
c
c
c return local memeory
c
        call retmem(2)
c
        return
        end
      subroutine coninthes2(fc,nq,c)
c&&&&&
c& nincs fixc. l. coninthes2a
c& nem emlekszem miert volt coninthes2a, mas invertalas!!!
c&
c
       implicit none
c   this soubroutine builds the Q (second derivative) matrix
c   for the avoided crossing minimalizations
c
c
c    Q = -2*(fi1-fi2) x (fi1-fi2) + 2 de*(h1-h2)
c    =      --  --      --  --          == ==
c
c    with fi1 and fi2 beeing the force on state 1 and 2 respectivelly
c         ---     ---
c
      integer nq
      real*8 fc(nq),c(nq,nq)
c
c memory depository
      integer lcx
      parameter (lcx=99000)
      real*8 bl
      common bl(lcx)
c
c local variables
      logical yesno,yesno1,yesno2
      integer nunit,info,iii,jjj,i,k,file,icount
c&      integer ibloecke,ibreite
      integer iscr,kpvt
c
c dimensions
      integer ma,lq,mq, mq2
      parameter (ma=200)
      parameter (lq=3*ma)
      parameter (mq=lq-6+2)
c   md is the max. number of gdiis geometry steps (or bfgs)
      integer md
      parameter (md=100)
c
c  common related to conical intersection calculations
      real*8 fc1,fc2,h1,fi1,fi2,hi,zeta1,zeta2,de,fd1,fd2,fdh
      integer iconint
      common /conint/ fc1(lq),fc2(lq),h1(lq),
     &     fi1(lq),fi2(lq),hi(lq),zeta1,zeta2,de,
     &     fd1(lq,md),fd2(lq,md),fdh(lq,md),iconint
c
c control parameters
      logical writ,lgdi,lfdi,lmur
      integer itopti,mxopti,icoormx,iangs,igeom,ifmat,isad,nfix,
     &        ifix,mgdi,ipu
      real*8  thract, shftpt,chmax,coormx
      common /opti/itopti,mxopti,chmax,thract,shftpt,coormx,iangs,
     +     igeom,ifmat,isad,nfix,ifix(mq),mgdi,lgdi,lfdi,lmur,writ,ipu
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
c   unit numbers
      integer       inp, inp2, nlist, ipun, iarc, icond, itest
      common /tape/ inp, inp2, nlist, ipun, iarc, icond, itest
c
      integer        ngeom,   nforc,   nintc,  nfmat,   nform,
     &               nener,   inpf3,   nmess,  nhess,   nhessinv,
     &               ncuriter
      common /tape2/ ngeom,   nforc,   nintc,  nfmat,   nform,
     &               nener,   inpf3,   nmess,  nhess,   nhessinv,
     &               ncuriter
c
c  # file names
      character*10     gdiisin,  gdiisls,  gdiissm,  default,
     &                 geomfl,   forcfl,   intcfl,   fmatfl,
     &                 formfl,   convfl,   gdiisfl,  gdiismg,
     &                 hessfl,   hessinvfl,curiterfl,zetafile,convfl2
      common /flnames/ gdiisin,  gdiisls,  gdiissm,  default,
     &                 geomfl,   forcfl,   intcfl,   fmatfl,
     &                 formfl,   convfl,   gdiisfl,  gdiismg,
     &                 hessfl,   hessinvfl,curiterfl,zetafile,convfl2
c
c allocate local memeory
c
      call getmem(nq*nq,iscr)
      call getmem(nq,kpvt)
c
c look whether hessian files exist
      inquire(file='hessian_st1',exist=yesno1)
      inquire(file='hessian_st2',exist=yesno2)
      yesno=yesno1.and.yesno2
      if (yesno) then
         write(nlist,
     &    '(/3x,''Hessian matrix read from files: '',2a12/)')
     &    'hessian_st1','hessian_st2'
c
        open(nhess,file='hessian_st1',status='old',form='formatted')
        file=nhess
c
c&        ibloecke=int(nq/6.0001)+1
c&        do k=1,ibloecke
c&           do j=1,nq
c&              ibreite=min(6,(nq-6*(k-1)))
c&              ij=(j-1)*nq+(k-1)*6
c&              read(file,*) (scr(i),i=ij+1,ij+ibreite)
c&           enddo                ! do j=1,nq
c&           read(file,*)
c&        enddo                   ! do k=1,ibloecke
c& 30     format(6f14.8)
        icount=iscr-1
        do k=1,nq
          read(file,30) (bl(i+icount),i=1,nq)
        icount=icount+nq
        enddo ! do k=1,nq
 30     format(8f13.6)
        close(file)
        call dcopy_wr(nq*nq,bl(iscr),1,c,1)
c
        open(nhess,file='hessian_st2',status='old',form='formatted')
        file=nhess
c
c&        ibloecke=int(nq/6.0001)+1
c&        do k=1,ibloecke
c&           do j=1,nq
c&              ibreite=min(6,(nq-6*(k-1)))
c&              ij=(j-1)*nq+(k-1)*6
c&              read(file,*) (scr(i),i=ij+1,ij+ibreite)
c&           enddo                ! do j=1,nq
c&           read(file,*)
c&        enddo                   ! do k=1,ibloecke
        icount=iscr-1
        do k=1,nq
          read(file,30) (bl(i+icount),i=1,nq)
        icount=icount+nq
        enddo ! do k=1,nq
        close(file)
        call daxpy_wr(nq*nq,-1.d0,bl(iscr),1,c,1)
c
        call dscal_wr(nq*nq,2.d0*de,c,1)
c
      else
        call wzero(nq*nq,c,1)
      endif                     ! if (yesno)
c
c now buld up Q (the minus sign is required since fi1 and fi2 are forces
c
        do iii=1,nq
          do jjj=1,nq
            c(iii,jjj)=
     &          c(iii,jjj)+2.d0*(fi2(iii)-fi1(iii))*(fi2(jjj)-fi1(jjj))
          enddo ! do jjj=1,nq
        enddo ! do iii=1,nq
c
c print out
c
        call prblkt('Q matrix',c,nq,nq,nq,'int','int',1,nlist)
c
c now calculate the inverse
c
         write(nlist,'(/3x,''Calculating the inverse Q matrix'')')
         call dgetrf_wr(nq,nq,c,nq,bl(kpvt),info)
          if (info.ne.0) call bummer(
     &     'coninthes:error in dgetrf, info=',info,faterr)
         call dgetri_wr(nq,c, nq,bl(kpvt), bl(iscr),nq*nq,info)
          if (info.ne.0) call bummer(
     &     'coninthes:error in dgetri, info=',info,faterr)
c
c print out
c
        call prblkt('Inverse Q matrix',c,nq,nq,nq,'int','int',1,nlist)
        write(nlist,'(/)')
c
        return
        end
      subroutine conintsav(nq,nqint,cqsave,zeta1,zeta2)
c
       implicit none
c  this soubroutine writes out zeta values
c
c
c argumets
      integer nq,nqint
      real*8 cqsave(nq),zeta1,zeta2
c
c local variables
      integer nunit
      logical yesno
c
c  # unit numbers
      integer       inp,inp2,nlist,ipun,iarc,icond,itest
      common /tape/ inp,inp2,nlist,ipun,iarc,icond,itest
c
      integer        ngeom,   nforc,   nintc,  nfmat,   nform,
     &               nener,   inpf3,   nmess,  nhess,   nhessinv,
     &               ncuriter
      common /tape2/ ngeom,   nforc,   nintc,  nfmat,   nform,
     &               nener,   inpf3,   nmess,  nhess,   nhessinv,
     &               ncuriter
c
c  # file names
      character*10     gdiisin,  gdiisls,  gdiissm,  default,
     &                 geomfl,   forcfl,   intcfl,   fmatfl,
     &                 formfl,   convfl,   gdiisfl,  gdiismg,
     &                 hessfl,   hessinvfl,curiterfl,zetafile,convfl2
      common /flnames/ gdiisin,  gdiisls,  gdiissm,  default,
     &                 geomfl,   forcfl,   intcfl,   fmatfl,
     &                 formfl,   convfl,   gdiisfl,  gdiismg,
     &                 hessfl,   hessinvfl,curiterfl,zetafile,convfl2
c
c---------------------------------------------------------------
c
      if(nq.eq.nqint+2) then
         write(nlist,'(/,"Changes of the Lagrange multipliers",/,
     &        10x,"old value ",5x,"change",8x,"new value",/,
     &        3x,"zeta1: ",f10.6,3x,f10.6,3x,f10.6,/,
     &        3x,"zeta2: ",f10.6,3x,f10.6,3x,f10.6)')
     &        zeta1,cqsave(nq-1),zeta1+cqsave(nq-1),
     &        zeta2,cqsave(nq),zeta2+cqsave(nq)
c
         zeta1=zeta1+cqsave(nq-1)
         zeta2=zeta2+cqsave(nq)
      else
         write(nlist,'(/,"Changes of the Lagrange multipliers",/,
     &        10x,"old value ",5x,"change",8x,"new value",/,
     &        3x,"zeta1: ",f10.6,3x,f10.6,3x,f10.6)')
     &        zeta1,cqsave(nq),zeta1+cqsave(nq)
c
         zeta1=zeta1+cqsave(nq)
      endif
c&test
c&      zeta2=0.d0
c
c writes to file
c
      inquire(file=zetafile,opened=yesno,number=nunit)
      if(.not.yesno) then
        open(unit=nhessinv,file=zetafile,status='unknown',
     +  form='formatted')
        nunit=nhessinv
      endif
      rewind nunit
      write(nunit,'(2F20.10)') zeta1,zeta2
      close(nunit)
c
c all done
      return
      end
      subroutine conintset(etot1,etot,de,zeta1,zeta2,iconint)
c
       implicit none
      real*8 etot1,etot,zeta1,zeta2,de
      integer iconint
c conversion factor of the energy
      real*8 ajoul
      parameter (ajoul=4.359814d0)
c
c   unit numbers
      integer       inp, inp2, nlist, ipun, iarc, icond, itest
      common /tape/ inp, inp2, nlist, ipun, iarc, icond, itest
c
      integer        ngeom,   nforc,   nintc,  nfmat,   nform,
     &               nener,   inpf3,   nmess,  nhess,   nhessinv,
     &               ncuriter
      common /tape2/ ngeom,   nforc,   nintc,  nfmat,   nform,
     &               nener,   inpf3,   nmess,  nhess,   nhessinv,
     &               ncuriter
c
c  # file names
      character*10     gdiisin,  gdiisls,  gdiissm,  default,
     &                 geomfl,   forcfl,   intcfl,   fmatfl,
     &                 formfl,   convfl,   gdiisfl,  gdiismg,
     &                 hessfl,   hessinvfl,curiterfl,zetafile, convfl2
      common /flnames/ gdiisin,  gdiisls,  gdiissm,  default,
     &                 geomfl,   forcfl,   intcfl,   fmatfl,
     &                 formfl,   convfl,   gdiisfl,  gdiismg,
     &                 hessfl,   hessinvfl,curiterfl,zetafile,convfl2
C
c    these are the parameters for the global geometry optimization
      logical writ,lgdi,lfdi,lmur
      integer itopti,mxopti,icoormx,iangs,igeom,ifmat,isad,nfix,
     &        ifix,mgdi,ipu
      real*8  thract, shftpt,chmax,coormx,de2
      integer ma,lq,mq, mq2
      parameter (ma=200)
      parameter (lq=3*ma)
      parameter (mq=lq-6+2)
      common /opti/itopti,mxopti,chmax,thract,shftpt,coormx,iangs,
     +     igeom,ifmat,isad,nfix,ifix(mq),mgdi,lgdi,lfdi,lmur,writ,ipu
c
      integer ios
c
c
c-------------------------------------------------------
c
      open(unit=nener,file='energy.gdiis',status='old',form='formatted')
      read(nener,*) etot1,etot,de2
      close(unit=nener)
c
      de=etot1-etot
      if(iconint.eq.1) then
         write(nlist,1111)
         write(icond,1111)
      else if(iconint.eq.2) then
         write(nlist,1111)
         write(icond,1111)
      else
         write(nlist,*)'!!!!!! I do not know, what I am doing ?????'
      endif
c
      write(nlist,1113) etot1,etot,de
      write(icond,1113) etot1,etot,de
 1111 format(/10x,' this is a conical intersection search',/)
 1112 format(/10x,' this is an avoided crossing optimization',/)
 1113 format(15x,' energy(state1)   =',f20.10,/,
     &     15x,' energy(state2)   =',f20.10,/,
     &     15x,' energy difference=',f20.10)
c restore energy of the first state
      etot=etot1
c convert energy to aJoul to be consistent with other quantities
c
      de = ajoul * de
c
      if(iconint.ne.1) goto 1010
c
c   read lagrange multipliers
c
      open(unit=nener,file=zetafile,status='old',form='formatted',
     &     iostat=ios,err=999)
      write(nlist,*) 'conintset, ios= ',ios
      read(nener,*) zeta1,zeta2
      goto 998
 999  continue
c
c no zeta file, initialize zeta
c
      zeta1=-1.d0
      zeta2=0.0
 998  continue
      close(nener)
c
 1010 continue
c
c switch off bfgs
c
      lmur=.false.
      write(nlist,*) 'bfgs  option is switched off'
c
      return
      end
      subroutine convchk(nq,cq,fi,exitcode,iconint)
c
       implicit none
c     ##  parameter & common block section
c
c     # constants
      real*8          zero,  half,  one,   two, three,  four, five,
     &                ten,   ten6,  tenm8, pi,  acc
      common /number/ zero,  half,  one,   two, three,  four, five,
     &                ten,   ten6,  tenm8, pi,  acc
c
c     # unit numbers
      integer       inp, inp2, nlist, ipun, iarc, icond, itest
      common /tape/ inp, inp2, nlist, ipun, iarc, icond, itest
c
      integer        ngeom,   nforc,   nintc,  nfmat,   nform,
     &               nener,   inpf3,   nmess,  nhess,   nhessinv,
     &               ncuriter
      common /tape2/ ngeom,   nforc,   nintc,  nfmat,   nform,
     &               nener,   inpf3,   nmess,  nhess,   nhessinv,
     &               ncuriter
c     # file names
      character*10     gdiisin,  gdiisls,  gdiissm,  default,
     &                 geomfl,   forcfl,   intcfl,   fmatfl,
     &                 formfl,   convfl,   gdiisfl,  gdiismg,
     &                 hessfl,   hessinvfl,curiterfl,zetafile,convfl2
      common /flnames/ gdiisin,  gdiisls,  gdiissm,  default,
     &                 geomfl,   forcfl,   intcfl,   fmatfl,
     &                 formfl,   convfl,   gdiisfl,  gdiismg,
     &                 hessfl,   hessinvfl,curiterfl,zetafile,convfl2
c
c     #  values determining the convergence
      real*8              cmax,fmax,crms,frms
      common/convergence/ cmax,fmax,crms,frms
c
      integer i,iconverg(4),inorm
      integer nq,iconint
c
      real*8 cq(nq),cmax_act,crms_act,cmean
      real*8 fi(nq),fmax_act,frms_act,fmean
      real*8 norm
c
c     ##  character section
c
      character*3 yesno(0:1)
      character*79 exitcode
c
c-----------------------------------------------------------
c
      yesno(0)=' no'
      yesno(1)='yes'
      call izero_wr(4,iconverg,1)
c
c     # evaluate the maximal coordinate and maximal force
      cmax_act = zero
      fmax_act = zero
      cmean = zero
      fmean = zero
      do i=1,nq
         if (cmax_act.lt.abs(cq(i))) cmax_act = abs(cq(i))
         if (fmax_act.lt.abs(fi(i))) fmax_act = abs(fi(i))
         cmean=cmean+cq(i)
         fmean=fmean+fi(i)
      enddo
      cmean = cmean / ((nq))
      fmean = fmean / ((nq))
c
c     # evaluate the RMS for coordinate and force
c     #  formula: rms = sqrt(1/(n-1) sum(x(i)-x_mean)**2, i=1,n)
c
      crms_act = zero
      frms_act = zero
      do i=1,nq
         crms_act = crms_act + (cq(i)-cmean)**2
         frms_act = frms_act + (fi(i)-fmean)**2
      enddo                     ! do i=1,nq
      norm = one
      if (nq.gt.1) norm = (nq-1)
      crms_act = sqrt(crms_act/norm)
      frms_act = sqrt(frms_act/norm)
c
      if (cmax_act .lt. cmax) iconverg(1) = 1
      if (fmax_act .lt. fmax) iconverg(2) = 1
      if (crms_act .lt. crms) iconverg(3) = 1
      if (frms_act .lt. frms) iconverg(4) = 1
c
      write(nlist,
     & '(//,5x,''Convergence of the geometry optimizations:''/)')
      write(nlist,
     & '(8x,''criterion'',18x,''required'',7x,''actual'',8x,''conv.'')')
      write(nlist,'(3x,70(1h-))')
      write(nlist,
     & '(5x,''Maximal coordinate change:'',t35,f10.7,5x,f10.7,5x,a3)')
     & cmax,cmax_act,yesno(iconverg(1))
      write(nlist,
     & '(5x,''Coordinate rms value'',t35,f10.7,5x,f10.7,5x,a3)')
     & crms,crms_act,yesno(iconverg(3))
      write(nlist,
     & '(5x,''Maximal gradient change:'',t35,f10.7,5x,f10.7,5x,a3)')
     & fmax,fmax_act,yesno(iconverg(2))
      write(nlist,
     & '(5x,''Gradient rms value'',t35,f10.7,5x,f10.7,5x,a3)')
     & frms,frms_act,yesno(iconverg(4))
c
      write(nener,
     & '(''max-step='',2x,f10.7,2x,a3)')cmax_act,yesno(iconverg(1))
      write(nener,
     & '(''rms-step='',2x,f10.7,2x,a3)')crms_act,yesno(iconverg(3))
      write(nener,
     & '(''max-force='',1x,f10.7,2x,a3)')fmax_act,yesno(iconverg(2))
      write(nener,
     & '(''rms-force='',1x,f10.7,2x,a3)')frms_act,yesno(iconverg(4))
c
      inorm = 0
      do i = 1, 4
         inorm = inorm + iconverg(i)
      enddo                     ! do i=1,4
      if (inorm.eq.4.or.(inorm.eq.2.and.iconint.eq.1)) then
c&      if (inorm .eq. 4) then
         exitcode='converged; convergence reached'
         write(nlist,
     &    '(//10x,''*** Geometry optimization converged! ***''//)')
      else
         exitcode='new geom follows'
         write(nlist,
     &    '(//10x,''*** Geometry optimization not converged! ***''//)')
      endif
c
      return
      end
      subroutine cosfit( e0, e1, f0, f1, x, emin, ifail)
c
       implicit none
c     # dummy:
      integer ifail
      real*8 e0, e1, f0, f1, x, emin
c
c     # unit numbers
      integer       inp,inp2,nlist,ipun,iarc,icond,itest
      common /tape/ inp,inp2,nlist,ipun,iarc,icond,itest
c
c     # local:
      integer i
      real*8 a, aa, aam, aap, b, bb, bbm, bbp, c, curv, denom,
     & denomm, denomp, dev, dev0, dev0m, dev0p, devmin, disc,
     & e, ee, ff1, ff1m, ff1p, om, omhigh, omlog1, omlog2,
     & omlow, step, x1, xm, xp, xx, xxm, xxp
c
      real*8    zero
      parameter(zero=0d0)
      real*8    one,     two,     three,     half,      smalld
      parameter(one=1d0, two=2d0, three=3d0, half=5d-1, smalld=1d-6)
      real*8     thous
      parameter( thous=1d3 )
      real*8     pi
      parameter( pi=3.14159265358979323846264338328d0 )
      real*8     twopi
      parameter( twopi= two * pi )
c
c      write(nlist,*)'e0,e1,f0,f1',e0,e1,f0,f1
CPP
      if ( .true. ) go to 500
C   leave out cosfit - use cubic
CPP
      devmin=1000000.0d0
      omlow=0.005d0
      omhigh=6.0d0
      omlog1=log(omlow)
      omlog2=log(omhigh)
      step = (omlog2 - omlog1) / thous
      do 100 i=1,1001
         xx=omlog1+(i-1)*step
         x=exp(xx)
         bb=-f0/x
         denom = cos(x) - one
         if (abs(denom) .lt. smalld) denom = sign(denom,smalld)
         aa=(e1-e0-bb*sin(x))/denom
         ee=e0-aa
         ff1=aa*x*sin(x)-bb*x*cos(x)
         dev=abs(f1-ff1)
         if (dev.lt.devmin) then
            devmin=dev
            om=x
            e=ee
            a=aa
            b=bb
            ifail=0
            xxp=xx+step
            xxm=xx-step
            xp=exp(xxp)
            xm=exp(xxm)
            bbp=-f0/xp
            bbm=-f0/xm
            denomp = cos(xp) - one
            if(abs(denomp) .lt. smalld) denomp = sign(denomp,smalld)
            denomm = cos(xm) - one
            if(abs(denomm) .lt. smalld) denomm = sign(denomm,smalld)
            aap=(e1-e0-bbp*sin(xp))/denomp
            aam=(e1-e0-bbm*sin(xm))/denomm
            ff1p=aap*xp*sin(xp)-bbp*xp*cos(xp)
            ff1m=aam*xm*sin(xm)-bbm*xm*cos(xm)
            dev0=f1-ff1
            dev0p=f1-ff1p
            dev0m=f1-ff1m
            if (dev0 * dev0p .lt. zero) then
               om=dev0/(dev0-dev0p)*(xp-x)+x
               write(nlist,*) 'a,b,x, ap,bp,xp', a,b,x,aap,bbp,xp
               write(nlist,*) 'dev0,devp,x,xp,om',dev0,dev0p, x,xp,om
            else if (dev0 * dev0m .lt. zero) then
               om=dev0/(dev0-dev0m)*(xm-x)+x
               write(nlist,*) 'dev0,devm,x,xm,om',dev0,dev0m, x,xm,om
            else
               ifail=1
            endif
            if (ifail.eq.0) then
               b=-f0/om
               denom = cos(om) - one
               if(abs(denom) .lt. smalld) denom = sign(denom,smalld)
               a=(e1-e0-b*sin(om))/denom
               e=e0-a
               go to 200
            end if
         end if
100   continue
      ifail=1
      go to 500
200   continue
      x=atan(b/a)/om
      curv=-(a*cos(om*x)+b*sin(om*x))*om**2
      if (curv .lt. zero) then
         x1 = x * om - pi
         if(x1 .lt. zero .and. e1 .lt. e0) x1 = x1 + twopi
         x = x1 / om
      end if
      emin=e+a*cos(om*x)+b*sin(om*x)
      write(nlist,*) 'x,emin', x,emin
      write(nlist,*) 'e,a,b,om',e,a,b,om
      go to 1000
c   ifail
500   continue
      a = two * (e0 - e1) - f0 - f1
      b = three * (e1 - e0) + two * f0 + f1
      c = -f0
      disc = b**2 - three * a * c
      if( disc .gt. zero ) then
         x = (-b + sqrt(disc)) / (three * a)
      else
         x = half
      end if
      emin=((a*x+b)*x+c)*x+e0
      write(nlist,*) 'a,b,c,disc',a,b,c,disc
      write(nlist,*)'cubic,x,emin',x,emin
1000  continue
      end
      REAL*8 FUNCTION DARCOS (X)
c
       implicit none
      real*8 x
c
      real*8 x1, s
c
      real*8    zero,     one,     half,      small
      parameter(zero=0d0, one=1d0, half=5d-1, small=1d-11)
      real*8     pi
      parameter( pi=3.14159265358979323846264338328d0 )
      real*8     pis2
      parameter( pis2= half * pi )
c
      IF (X .GE. one) GO TO 10
      IF (X .LE. -one) GO TO 20
      X1 = SQRT(one - X**2)
      IF (ABS(X) .LT. small) GO TO 30
      S = ATAN( X1 / X)
      IF (X .LT. zero) S = S + pi
      DARCOS = S
      RETURN
   10 DARCOS = zero
      RETURN
   20 DARCOS = pi
      RETURN
   30 DARCOS = pis2
      RETURN
      END
      subroutine distort (
     & nek,    nq,    na,      qc,      qq,
     & qq1,    xa,    ibcode,  ibcontr, b,
     & shftpt, xy,    qcsave)
c
c   this routine distorts the molecule along given internal
c   coordinates and gives the Cartesians as a result
c
c   INPUT
c   nek=3*na, (input) number of cartesians
c   na (input) number of nuclei
c   nq (input) number of internal coordinates
c   qc (input) new internal coordinates
c   qq (input) original internal coordinates
c   qq1 (workspace, contains the final coordinates on entry)
c   xa(3,na)(input) cartesians in angstrom units
c   ibcode(6,*): coding info for the B matrix
c   ibcontr(99,nq): contraction for the B matrix
c   b (workspace) b matrix
c   shftpt (input) see machb
c   OUTPUT
c   xy(3,na)  (output) distorted cartesians
c
c
       implicit none
c     ##  parameter & common block section
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
c   this is the maximum internal coordinate change
c   otherwise use scaling
      real*8     maxstep
      parameter (maxstep=0.10d0)
c
c     # convergence tolerance
      real*8 toler
*@ifdef orig
*     parameter (toler=1.0d-9)
*@else
      parameter (toler=5.0d-9)
*@endif

c
c     # maximal number of iterations
      integer maxiter
      parameter (maxiter=150)
c
c     # unit numbers
      integer       inp, inp2, nlist, ipun, iarc, icond, itest
      common /tape/ inp, inp2, nlist, ipun, iarc, icond, itest
c
      real*8     onem
      parameter (onem=-1.0d+00)
c
      real*8          condamp
      common /options/condamp
c
c     # constants
      real*8          zero,  half,  one,   two, three,  four,  five,
     & ten,   ten6,  tenm8, pi,  acc
      common /number/ zero,  half,  one,   two, three,  four,  five,
     & ten,   ten6,  tenm8, pi,  acc
c
      integer    lcx
      parameter (lcx=99000)
      real*8    bl
      common // bl(lcx)
c
      integer           lonl, lalw
      common/transform/ lonl, lalw
c
c     ##  integer section
c
      integer ibcontr(99,*),ibcode(6,*),ix,iy,idm1,it,ip,ir,iz,iu,
     & i, irep, imax
      integer lincalc
      integer nek, nq, na
c
c     ##  real*8 section
c
      real*8 b(199,nq)
      real*8 delmax
      real*8 qc(nq),qq(nq),qq1(nq),qcsave(nq)
      real*8 scale, sc1, shftpt
      real*8 xa(3,na),xy(3,na)
      external btb,bbt
c
c-------------------------------------------------------------
c     # qq1 is the current set of internal coordinates,
c     # qq is the original set
c     # qq1 will be replaced by the current error
c
c     ##  header information
c
      write(nlist,'(//,5x,60(1h*)/)')
      write(nlist,
     & '(10x,''Beginning the iterative coordinate transformation:'')')
      write(nlist,'(12x,''internal --> cartesian coordinates''//)')
      if (lonl.eq.1) write(nlist,
     & '(10x,''Performing linear calculation only.'')')
      write(nlist,'(5x,''Main parameters:'')')
      write(nlist,'(10x,''Number of iterations:'',t45,i3)')maxiter
      write(nlist,'(10x,''Convergance criterion:'',t45,e13.8)')toler
      write(nlist,
     & '(10x,''Max. int. coor. change in 1 iter:'',t45,f6.3)')
     & maxstep
      write(icond,'(//,5x,60(1h*)/)')
      write(icond,
     & '(10x,''Beginning the iterative coordinate transformation:'')')
      write(icond,'(12x,''cartesian --> internal coordinates''//)')
      write(icond,'(5x,''Main parameters:'')')
      write(icond,'(10x,''Number of iterations:'',t45,i3)')maxiter
      write(icond,'(10x,''Convergance criterion:'',t45,e13.8)')toler
      write(icond,
     & '(10x,''Max. int. coor. change in 1 iter:'',t45,f6.3)')
     & maxstep
c
c     # define temporary storage
      call getmem(nek,ix)
      call getmem(nek,iy)
      call getmem(nek,idm1)
      call getmem(nek,it)
      call getmem(nek,ip)
      call getmem(nek,ir)
      call getmem(nek,iz)
      call getmem(nek,iu)
c
500   lincalc=lonl
c
c     # calculate the diagonal of the matrix BtB
      call bdiag1(nq,nek,b,ibcontr,bl(idm1))
      call dcopy_wr(nq,qq,1,qq1,1)
c     #  qc holds the desired set:
      call dcopy_wr(nq,qc,1,qcsave,1)
      call daxpy_wr(nq,one,qq,1,qc,1)
c     #  xy is the current set of Cartesians
      call dcopy_wr(nek,xa,1,xy,1)
c     #  set the first value of the change
      call dcopy_wr(nq,qc,1,bl(ix),1)
c
      write(nlist,'(/,10x,''Geometries:'')')
      write(nlist,'(12x,''starting'',9x,''change'',11x,''final'')')
      write(nlist,'(8x,50(1h-))')
      do  i=1,nq
         write(nlist,'(8x,f13.8,2(4x,f13.8))')qq(i),qcsave(i),qc(i)
      enddo
      write(nlist,*)
      write(icond,'(/,10x,''Geometries:'')')
      write(icond,'(12x,''starting'',9x,''change'',11x,''desired'')')
      write(icond,'(8x,50(1h-))')
      do  i=1,nq
         write(icond,'(8x,f13.8,2(4x,f13.8))')qq(i),qcsave(i),qc(i)
      enddo
      write(icond,*)
c
c     #------------------------------------------------
c     ##  beginning the main loop
c
      do irep=1,maxiter
c
         if (lincalc.ne.1) then
            write(nlist,
     &       '(/,2x,5(1h-),2x,''Beginning iteration: '',i3,2x,5(1h-)/)')
     &       irep
         endif
         write(nlist,'(10x,''Starting geometry:'')')
         write(nlist,'(8x,f16.10)')(qq1(i),i=1,nq)
c
c        # calculate the error in the internal coordinates,
c        # put it in qq1
         call dscal_wr(nq,onem,qq1,1)
         call daxpy_wr(nq,one,qc,1,qq1,1)
c        # do not calculate actual - should values
c        # rather, look at the change
         call absmax(nq,qq1,imax,delmax)
         write(nlist,'(10x,''Max. abs. geometry deviation: '',e15.8)')
     &    delmax
c
c        # for irep=2
         if ((lincalc.eq.1).and.(irep.eq.2)) then
            write(nlist,'(10x,''Performed linear calculation'')')
            go to 1000
         endif
c
c        # check the convergence and quit
         if(delmax.lt. toler) then
            write(nlist,'(/,10x,''* * *  Convergence reached  * * *'')')
            go to 1000
         endif
c
c        # scale back the change
         if((delmax.gt.maxstep).and.(lincalc.ne.1)) then
            scale=maxstep/delmax
            call dscal_wr(nq,scale,qq1,1)
            write(nlist,
     &       '(10x,''Step scaling active, actual steps size:'')')
            do  i=1,nq
               write(nlist,'(28x,f13.8)')qq1(i)
            enddo
         else
            scale = one
         endif
c
c        # delta X = Bt * (BBt)**-1 * Delta Q  to first order
c        # first evaluate  x=[(BBt)**-1]Delta Q
c
c        # new method: x is the conj. grad solution of Bt*B*x=Bt*q
         call btxv(nq,nek,b,ibcontr,qq1,bl(iu))
         write(nlist,*)'Solving: Bt*B*x=Bt*q'
         call congrad(
     &    nek,     nq,        b,      ibcontr,   bl(ix),
     &    bl(iy),  bl(idm1),  bl(iu), bl(it),    bl(ip),
     &    bl(ir),  bl(iz),    btb,    condamp)
         call daxpy_wr(nek,one,bl(ix),1,xy,1)
c        # calculate the "should" value of the internal coordinates
c        # this is  the original value plus the scaled change
         sc1 = one - one / scale
         call dcopy_wr(nq,qc,1,bl(ix),1)
         call daxpy_wr(nq,sc1,qq1,1,bl(ix),1)
c
c        # coordinates
c        # save the old values of the int. coord.
c        # in bl(ix) with negative sign
         call dcopy_wr(nq,qc,1,bl(ix),1)
         call daxpy_wr(nq,onem,qq1,1,bl(ix),1)
c        # this call to the b matrix routine only serves to
c        # calculate the
         call machbnew(na,xy,nq,.true.,shftpt,ibcode,ibcontr,b,qq1)
         do i=1,nq
            call fixtor(bl(ix-1+i),qq1(i))
         enddo                  ! do i=1,nq
c
c        ##  calculate change
         call daxpy_wr(nq,onem,qq1,1,bl(ix),1)
      enddo                     ! do irep=1,maxiter
c
c     # end of the main loop
c---------------------------------------
c
c     ##  convergence not reached
c
c     #  if allowed perform linear calculation:
      if (lalw .eq. 1) then
         lonl = 1
         call dcopy_wr(nq,qcsave,1,qc,1)
         call bummer('Changing coputational mode to linear',
     &    0,wrnerr)
         write(nlist,
     &    '(//5x,'' * * *  Calculation not converged  * * *'')')
         write(nlist,'(10x,''performing linear calculation'')')
         write(nlist,'(10x,''Set flag: lonl= '',i1)')lonl
         write(icond,
     &    '(//5x,'' * * *  Calculation not converged  * * *'')')
         write(icond,'(10x,''performing linear calculation'')')
         write(icond,'(10x,''Set flag: lonl= '',i1)')lonl
         go to 500
      endif                     ! if (lalw.eq.1)
c
c     #  nothing helps => quit
      call bummer(
     & 'Calculation not converged, iter=',irep,faterr)
c
1000  continue
      write(nlist,
     & '(/10x,''End of the iterative coordinate transformation'')')
      write(nlist,'(/,5x,60(1h*)/)')
      write(icond,
     & '(/10x,''End of the iterative coordinate transformation'')')
      write(icond,'(/,5x,60(1h*)/)')
c
      call retmem(8)
      return
      end
      SUBROUTINE ENMAX(NQ,FI,CC,THRACT,IMAX)
c
      IMPLICIT none
C       ESTIMATE THE LARGEST ENERGY CHANGE
c
      integer  imax,nq,i
c
      real*8 FI(NQ), CC(NQ), EMAX, CCC, EE, THRACT
c
      real*8    zero,     half,      dispmn
      parameter(zero=0d0, half=5d-1, dispmn=0.0015d0)
c
c-----------------------------------------------------
      EMAX = zero
      IMAX = 0
C      IF THE DISPLACEMENT IS VERY SMALL BUT THERE IS A SIZABLE FORCE
C      THEN DO NOT STOP YET. THE MINIMUM DISPLACEMENT HERE IS
C      0.0015 A. WITH THE LIMIT 1.0E-6, this limits the forces to
C      6.7E-4
      DO 100 I=1,NQ
         CCC=CC(I)
c        ?????????????????????????????????????
c        should the following satement be:
c        IF( ABS(CCC) .LT. dispmn ) CCC = sign(ccc,dispmn)
c        24-oct-01 rls
c        ?????????????????????????????????????
         IF( ABS(CCC) .LT. dispmn ) CCC = dispmn
         EE=ABS(FI(I)*CCC)
         IF (EE .GT .EMAX) THEN
            EMAX = EE
            IMAX = I
         END IF
100   CONTINUE
      THRACT= half * EMAX
c
      return
      END
      subroutine fixtor (should, found)
c
c     #  fix torsional angles if they turn around fully
c
       implicit none
c     # dummy:
      real*8 should, found
c
c     # constants
      real*8          zero,  half,  one,   two, three,  four,  five,
     & ten,   ten6,  tenm8, pi,  acc
      common /number/ zero,  half,  one,   two, three,  four,  five,
     & ten,   ten6,  tenm8, pi,  acc
c
c     # local:
      integer nf
      real*8 adevi, deviat, offset, upper, xlower, xnfold
c
      real*8     scalup,       scaldn
      parameter( scalup=1.1d0, scaldn=0.9d0 )
c
      real*8 xnfo(8)
      data xnfo/1.0d0,2.0d0,3.0d0,4.0d0,6.0d0,9.0d0,4.5d0,1.5d0/
c
      deviat = should - found
c
c     # the purpose of the following code is to identify cases when a
c     # torsional angle changes suddenly by 2*pi
c     # because of the 1/n normalization, the change may be 2*pi,
c     # 2*pi/2, /3, /4, /6 or /9 or /4.5 or /1.5
      adevi = abs(deviat)
      if(adevi .gt. half) then
         do  1285 nf = 1, 8
            xnfold = xnfo(nf)
c           # this is the number of torsional angles linearly combined
c           # it can be 1,2,3,4,6 or 9
            offset = two * pi / xnfold
            upper  = scalup * offset
            xlower = scaldn * offset
            if(adevi .lt. upper .and. adevi .gt. xlower) then
               if(deviat .gt. zero) then
c                  qq1(i) = qq1(i) + offset
                  found = found + offset
                  go to 1286
               else
c                  qq1(i) = qq1(i) - offset
                  found = found - offset
                  go to 1286
               endif
            endif
1285     continue
      endif
1286  continue
c
      end
      program gdiis
c
       implicit none
c     ##  parameter & common block section
c
      integer lqmax
      parameter (lqmax= 800)
c
      integer    lcx
      parameter (lcx=99000)
      real*8    bl
      common // bl(lcx)
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer lcore,iov,last,nganz
      common /ganz/ lcore,iov,last,nganz(81)
c
c     # units
      real*8        ang,debye,cbm,ajoule,evolt,ckalmo,dkel,cmm1,hertz
      common /unit/ ang,debye,cbm,ajoule,evolt,ckalmo,dkel,cmm1,hertz
c
c     # constants
      real*8          zero,  half,  one,   two, three,  four,  five,
     & ten,   ten6,  tenm8, pi,  acc
      common /number/ zero,  half,  one,   two, three,  four,  five,
     & ten,   ten6,  tenm8, pi,  acc
c
c     #  values determining the convergence
      real*8              cmax,fmax,crms,frms
      common/convergence/ cmax,fmax,crms,frms
c
      real*8          condamp
      common /options/condamp
c
      integer           lonl, lalw
      common/transform/ lonl, lalw
c
      integer        nreq,maxmem,nadr
      COMMON /MMANAG/NREQ,MAXMEM,NADR(400)
c
c     # ma is the max. number of atoms
      integer ma
      parameter (ma=200)
c     # lq is the max. number of cartesisans
      integer lq
      parameter (lq=3*ma)
c     # mq is the max. number of internal coordinates,
      integer mq, mq2, maxcard
c&one line modified
      parameter (mq=lq-6+2)
      parameter (mq2=mq**2)
      parameter (maxcard=3*mq)
c     # md is the max. number of gdiis geometry steps (or bfgs)
      integer md
      parameter (md=100)
c
c     # common related to input data
      character*3 symb
      real*8 xa,etot,f,mass
      integer ia,na
      common /geom/ xa(3,ma),  etot,     f(lq),  mass(ma),  ia(ma),
     & na
      common /cgeom/ symb(ma)
c&new lines
c
c     # common related to conical intersection
c     # and avoided crossing optimizations
c
c     # iconint  is the control parameter and will be set in readall
c            =  0     usual geometry optimization
c            =  1     conical intersection calculation a la Yarkony
c            =  2     avoided crossing optimization
c
      real*8 fc1,fc2,h1,fi1,fi2,hi,zeta1,zeta2,de,fd1,fd2,fdh
      integer iconint
      common /conint/ fc1(lq),fc2(lq),h1(lq),
     &     fi1(lq),fi2(lq),hi(lq),zeta1,zeta2,de,
     &     fd1(lq,md),fd2(lq,md),fdh(lq,md),iconint
c&new lines end
c
c     # common holding iteration information
      real*8 energy, qd, fd
      common /energy/ energy(md),qd(lq,md),fd(LQ,MD)
c
C
c     # these are the parameters for the global geometry optimization
      logical writ,lgdi,lfdi,lmur
      integer itopti,mxopti,iangs,igeom,ifmat,isad,nfix,
c&lines changed mgdi
     & ifix,mgdi,ipu
      real*8  thract, shftpt,chmax,coormx
      common /opti/itopti,mxopti,chmax,thract,shftpt,coormx,iangs,
     + igeom,ifmat,isad,nfix,ifix(mq),mgdi,lgdi,lfdi,lmur,writ,ipu
c
c     # file names
      character*10     gdiisin,  gdiisls,  gdiissm,  default,
     & geomfl,   forcfl,   intcfl,   fmatfl,
     & formfl,   convfl,   gdiisfl,  gdiismg,
c&line changed
     & hessfl,   hessinvfl,curiterfl,zetafile, convfl2
      common /flnames/ gdiisin,  gdiisls,  gdiissm,  default,
     & geomfl,   forcfl,   intcfl,   fmatfl,
     & formfl,   convfl,   gdiisfl,  gdiismg,
c&line changed
     & hessfl,   hessinvfl,curiterfl,zetafile, convfl2
c
c     # styles
      character*10 geomst,forcst
c     # formats
      character*30 geomfm,forcfm
      common /style/ geomst, forcst, geomfm, forcfm
c
c     # unit numbers
      integer       inp, inp2, nlist, ipun, iarc, icond, itest
      common /tape/ inp, inp2, nlist, ipun, iarc, icond, itest
c
      integer        ngeom,   nforc,   nintc,  nfmat,   nform,
     & nener,   inpf3,   nmess,  nhess,   nhessinv,
     & ncuriter
      common /tape2/ ngeom,   nforc,   nintc,  nfmat,   nform,
     & nener,   inpf3,   nmess,  nhess,   nhessinv,
     & ncuriter
c
c     ##  integer section
c
      integer adr
      integer curr_iter
      integer i, ii, iii, j, jj, jjj, iijj, ifail, icmx, icmy,
     & imax,ibcode(6,maxcard),ibcontr(99,mq),ij
      integer maxcom
c&line changed
      integer nek, nq1, nunit, nq, nprim, nqint, ngeo
      integer ierr
c
c     ##  real*8 section
c
      real*8 a(md,md), aeig(lqmax*lqmax)
      real*8 b(199,mq),beig(lqmax*5)
      real*8 cq(mq),c(mq2),csave(mq2), cqsave(mq)
      real*8 deltae, dismax, dq(lq),dg(lq),dc(md)
      real*8 etot1, edec, et, edecr, emin,eigen(lqmax),
     & eigvec(lqmax*lqmax),edel
      real*8 f0, f1, fmax1, fi(mq),fc(mq), fcold(lq)
      real*8 l(lq)
      real*8 m(lq)
      real*8 sss, ssa, scfac
      real*8 xx1, xx2, xx, xy(3,ma)
      real*8 qq(mq),qq1(mq)
      real*8 zk(lq)
c&new line
      real*8 dqd(lq,md)
c
      real*8    zeroc
      parameter(zeroc=0d0)
c
c     ##  logical section
c
      logical yesno, enup
c&line modified
      logical qonly, dodiis, lcoup
c
c     ##  character section
c
      character*5 check
      character*79 exitcode
c
c     ##  external section
c
      real*8 ddot_wr
      integer idamax_wr
      external ddot_wr, idamax_wr
C
C     THIS IS THE NUMBER OF GEOMETRIES OMITTED FROM GDIIS
      integer mexclu
      DATA MEXCLU/0/
c
c------------------------------------------------------------------
c
c& new line
      iconint=0
c
      qonly = .false.
c
c     # memory:
c
      lcore=lcx
      call retall
C
C     SET DEFAULTS
C
      call init
c
c     # open input and output files
c
      open(unit=nlist,file=gdiisls,status='unknown',form='formatted')
      open(unit=icond,file=gdiissm,status='unknown',form='formatted')
c
c     # use nlist temporarily to initialize the skpend() routine.
      call iskpend( nlist, 1, ierr)
      if ( ierr .ne. 0 ) then
         call bummer(' gdiis: from iskpend(), ierr=', ierr, faterr )
      endif
c
c     # write out header for gdiisls
      write(nlist,321)
      write(icond,321)
      write(icond,322)
      write(nlist,323)
321   format(30x,'program "gdiis"'/
     + 17x,'geometry optimization by direct inversion'/
     + 27x,'in the iterative subspace')
322   format(30x,'SUMMARY FILE')
323   format(
     + 12x,'program source: P. Pulay''s TEXAS ',
     + 'modified by P. G. Szalay'/
     + 18x,'updated to version 5.6 by Michal Dallos',/
     + 18x,'conical intersection version  by Peter G. Szalay',/
     + 27x,'version date: 31-jan-02'/)
c
      call headwr(nlist,'GDIIS','5.9.0')
c
      inquire(file=gdiisin,exist=yesno)
      if(yesno) then
         open(unit=inp,file=gdiisin,status='unknown',form='formatted')
      else
         call bummer('could not open file gdiisin',0,2) 
      endif
c
c     # deltae is the energy associated with the offset forces
c     # the zero is arbitrary but it is taken as sum q(i)*f(i)
      deltae=zero
c
c&line modified
      call readall(check,iconint,lcoup)
c&change start
      if(iconint.eq.0) then
c
c     ##  open the geoconvfl file and read out the last energy value
c
       open(unit=nener,file=convfl,status='old',form='formatted')
       call readener
c
       etot1=etot
c
      else  !if(iconint.eq.0)
c
c      # in case of a conical intersection search read the other energy,
c      # read zeta1 and zeta2 and switch off some options
c
       call conintset(etot1,etot,de,zeta1,zeta2,iconint)
c&       open(unit=nener,file=convfl,status='old',form='formatted')
       open(unit=nener,file=convfl,status='unknown',form='formatted')
c
      endif !if(iconint.eq.0)
c&change end
c
c     # set the file position to append after the last record
      call skpend( nener, 1 )
c
      nek=3*na
c
c     ##  construct b matrix
c
      inquire(file=intcfl,opened=yesno,number=nunit)
      if(.not.yesno) then
         open(unit=nintc,file=intcfl,status='unknown',form='formatted')
      else
         nintc=nunit
      endif
c     read the title line
      read(nintc,*)
c& changed
      call bread(na,nqint,nprim,ibcode,ibcontr,writ)
      nq1=na*3-6
c& three lines changed
      if (nqint.ne.nq1.and.na.gt.2) then
         write (nlist,210) nqint,nq1
         write (icond,210) nqint,nq1
      end if
210   format (' warning: there are ',i4, ' int.coor., 3n-6= ',i4)
c
c&two lines changed
      call machbnew(na,xa,nqint,qonly,shftpt,ibcode,ibcontr,b,qq)
      call printbmat(nqint,nek,b,ibcontr)
c
c     ##  transform gradients
c
c&changes start
      if(iconint.eq.0) then
         nq=nqint
         call intforc(nq,nek,b,ibcontr,f,fi,qq,icmy,fmax1)
      else if(iconint.eq.1) then
         if(lcoup) then
            nq=nqint+2
         else
            nq=nqint+1
         endif
         call intforc1(nqint,nek,b,ibcontr,fc1,fc2,h1,fi1,fi2,hi,fi,
     &        zeta1,zeta2,de,qq,icmy,fmax1)
      else if(iconint.eq.2) then
         nq=nqint
         call intforc2(nqint,nek,b,ibcontr,fc1,fc2,fi1,fi2,fi,de,
     &        qq,icmy,fmax1)
      endif
c&change ends
c
c     ## apply fixc
c
      do ii=1,nfix
         jj=ifix(ii)
         fi(jj)=zeroc
      enddo                     ! end of: do ii=1,nfix
      if (nfix .gt.0) call onecol (nlist,nq,qq,fi,
     & 'internal coordinates and forces after fixc')
c
      if (ipu.eq.1) then
         do 330 i=1,nq
            write (ipun,1170) qq(i),fi(i),i
330      continue
      endif
1170  format (2f16.8,i4,2f12.8)
c
c     ##  if available, read in the hessian or inverse hessian matrix
c     #  priority order:
c     #  1. inverse hessian matrix
c     #  2. hessian matrix
c     #  3. diagonal elements of the hessian matrix
c
c     ## if inverse hessian file exists, read it
c
c&new line
      if(iconint.ne.0) goto 1000
c
      inquire(file=hessinvfl,exist=yesno)
      if (yesno) then
         write(nlist,
     &    '(/3x,''Inverse Hessian matrix read from file: '',a10/)')
     &    hessinvfl
         call hssread(c,nq,ifix,nfix,1)
         go to 1000
      endif                     ! if (yesno)
c
c     ## if hessian file exists (and inverse hessian file do not exists)
c     ## read it
c
      inquire(file=hessfl,exist=yesno)
      if (yesno) then
         write(nlist,
     &    '(/3x,''Hessian matrix read from file: '',a10/)')
     &    hessfl
         call hssread(c,nq,ifix,nfix,2)
         go to 1000
      endif                     ! if (yesno)
c
c     ## no hessian or inverse hessian file found, read the diagonals
c
      write(nlist,'(/3x,''Hessian matrix constructed from' //
     & ' diagonal elements only:'')')
c
c     # read in the force constants and invert them. also deal with fixc
c
      if (ifmat.lt.1) then
         write(nlist,*) 'no fmat ,no geometry relaxation'
         call bummer('no fmat ,no geometry relaxation',0,faterr)
      end if                    ! end of: if (ifmat.lt.1) then
c
      inquire(file=fmatfl,opened=yesno,number=nunit)
      if(.not.yesno) then
         open(unit=nfmat,file=fmatfl,status='unknown',form='formatted')
      else
         nfmat=nunit
      endif                     ! end of: if(.not.yesno) then
c
      call readdiag(fc,nq,nfix,ifix,nfmat,nlist,icond,lmur)
      call dcopy_wr(nq,fc,1,fcold,1)
c
      do iii=1,nq
         do jjj=1,nq
            iijj=(jjj-1)*nq+iii
            if (iii.eq.jjj) then
               c(iijj)=fc(iii)
            else
               c(iijj)=zeroc
            endif
         enddo                  ! do jjj=1,nq
      enddo                     ! do iii=1,nq
c
      call hsswrite(c,csave,nq)
c
1000  continue
c&new lines
c
c     ## deal with the hessian matrix for conical intersaction search
c
      if(iconint.eq.1) then
         call coninthes1(fc,nq,nqint,c)
c         call dcopy_wr(nqint,fc,1,fcold,1) !! for bfgs this will be
c    +  need?

      else if(iconint.eq.2) then
         call coninthes2(fc,nq,c)
c         call dcopy_wr(nqint,fc,1,fcold,1) !! for bfgs this will be
c    +  need?

      endif
c&new lines end
c
c     ## calculate the inverse-hessian matrix eigenvalues
c
      adr=0
      do i=1,nq
         do j=1,i
            adr=adr+1
            ij=(i-1)*nq+j
            aeig(adr)=c(ij)
         enddo
      enddo
c
      call givens(nq,nq,nq,aeig,beig,eigen,eigvec,nq)
c
      write(nlist,*)'Eigenvalues of the HESSIAN matrix'
      write(nlist,'(10f12.3)')(one/eigen(i),i=1,nq)
c
      call prblkt('eigenvectors of the inverse HESSIAN matrix',
     + eigvec,nq,nq,nq,'csf ','vec ',1,nlist)
c
c     # automatic geometry optimization - read from inpf3 (file 33)
c     # automatic optimization past the first step
c
      inquire(file=gdiisfl,opened=yesno,number=nunit)
      if(.not.yesno) then
         open(unit=inpf3,file=gdiisfl,status='unknown',
     +    form='formatted')
      else
         inpf3=nunit
      endif
      rewind inpf3
      i=1
680   read (inpf3,'(16x,f15.8)',end=720) energy(i)
      read (inpf3,*)
      do j=1,nq
c&lines modified
        if(iconint.ne.1) then
         read (inpf3,1170,end=720) qd(j,i),fd(j,i),ii,qd(j,i+1),
     +          dqd(j,i)
c         dqd(j,i)=qd(j,i+1)-qd(j,i)
        else    !if(iconint.ne.1)
         read (inpf3,1171,end=720) qd(j,i),fd1(j,i),fd2(j,i),fdh(j,i),
     +          ii,qd(j,i+1),dqd(j,i)
 1171    format (4f16.8,i4,2f16.8)
        endif   !if(iconint.ne.1)
c&end modif
      enddo                     ! do j=1,nq
      i=i+1
      go to 680
720   continue
c
c     # set the file position to append after the last record.
      call skpend( inpf3, 1 )
c
      ngeo=i-1
c
c     ##  try to correct the internal coordinates in the case that
c     ##   a change by 2*pi occurred (for a torsion)
c
      if (ngeo.gt.0) then
c&lines modified
        do j=1,nqint
          call fixtor(qd(j,ngeo+1),qq(j))
        enddo ! do j=1,nq
c
c copy current data into the big depository
c
        do j=1,nqint
          qd(j,ngeo+1)=qq(j)
          fd(j,ngeo+1)=fi(j)
          if(iconint.eq.1) then
            fd1(j,ngeo+1)=fi1(j)
            fd2(j,ngeo+1)=fi2(j)
            fdh(j,ngeo+1)=hi(j)
          endif
        enddo ! do j=1,nq
      end if                    ! if (ngeo.gt.0)
c&modif ends
c
      ngeo=ngeo+1
      energy(ngeo)=etot1
      etot1=energy(ngeo)+deltae
c
c     ## read out the current iteration number
c
      open(unit=ncuriter,file=curiterfl,status='old',form='formatted')
      read (ncuriter,*)curr_iter
      close (unit=ncuriter)
c
c     ##  calculate the change in energy to the previous iteration
      edel=zero
      if (ngeo.gt.1) edel=energy(ngeo)-energy(ngeo-1)
c
c     #-----------------------------
c     ## Newton-Raphson step
c
      if (.not.lgdi.or.ngeo.lt.mgdi) then
         write(nlist,'(10x,''Performing Newton-Raphson step''/)')
         write(icond,'(10x,''Performing Newton-Raphson step''/)')
c
c        # there is no space for the full force constant matrix so
c        # BFGS cannot be used
         if (lmur.and.ngeo.ge.mgdi) then
            call bummer('lmur.and.ngeo.ge.2',0,faterr)
         end if
         call relaxd(nq,fi,c,cq,edecr,dismax,icmx,one)
c&new lines
c
c        # correct edecr (substract lagrangian contribution)
c        # if conical intersection search
c
         if(iconint.eq.1)  edecr=edecr+cq(nq-1)*fi(nq-1)
c&new lines end
         edec=edecr*ajoule
         et=etot1+edec
         write (nlist,490) edecr,edec,et
         write (icond,490) edecr,edec,et
490      format (/' projected energy lowering= ',f13.8,' ajoule ',
     &    f13.8,' hartree',' total energy= ',f13.8,' hartree')
         if (dismax.gt.coormx) then
            do 725 ii=1,nqint
               if(abs(cq(ii)) .gt. coormx)
     &          cq(ii)=cq(ii)*(coormx/abs(dismax))
725         continue
            dismax=coormx
         end if
         call dcopy_wr(nq,qq,1,qq1,1)
         CALL daxpy_wr(NQ,one,CQ,1,QQ1,1)
c
         CALL ONECOL(nlist,NQ,CQ,QQ1,
     1    ' GEOMETRY CHANGE AND NEW INTERNAL COORDINATES')
         CALL onecol(ICOND,NQ,CQ,QQ1,
     1    ' GEOMETRY CHANGE AND NEW INTERNAL COORDINATES')
C        # ZERO OUT THE FORCE IN THE DIRECTION OF THE FIXED COORDINATES
         DO  740 I=1,NFIX
            II=IFIX(I)
            FI(II)=ZERO
740      CONTINUE
c&lines modified
c
c        # calculate thract = estimated energy change per coordinate
c        # or rather the maximum thereof
         call absmax(nqint,cq,icmx,dismax)
         call enmax (nqint,fi,cq,thract,imax)
         write(nlist,340)curr_iter,etot,etot1,icmy,fmax1,icmx,cq(icmx),
     1        imax,thract
         write(icond,340)curr_iter,etot,etot1,icmy,fmax1,icmx,cq(icmx),
     1        imax,thract
 340     format(' ROUND',I4,2F15.8,/' MAX. FORCE=',I4,F11.7,/,
     1    ' MAX. COORD. CHANGE= ', I3,F11.7,/' MAX. ENERGY CHANGE',
     2    I4,E12.5)
c&end modif
c
c        # write out current iter data
c
C        # IF THERE IS GEOMETRY OPT., WRITE OUT THE EXPECTED GEOM. TOO
C        # ROUND No., TOTAL E, MAX. FORCE, MAX. CHANGE, MAX. EN. CHANGE
         WRITE(INPF3,345) curr_iter,ETOT1,FMAX1,ICMY,CQ(ICMX),ICMX,
     1    edel
345      format(' iter=',i3,'| etot=',f15.8,'| mxforce=',f10.6,
     &    ' on c.',i3,'| mxstep=',f10.6,' for c.',i3,'| dener= ',e13.7)
c&lines modified
         if(iconint.ne.1) then
            write(inpf3,'(" old coordinate |      force    |",
     &           "no.|new coord. |displacement")')
         else
            write(inpf3,'(" old coordinate | force state1  |",
     &           "force state2  | coupling vector|",
     &           "no.|    new coord. |   displacement ")')
         endif
          do 350 i=1,nq
            if(iconint.ne.1) then
             write(inpf3,1170) qq(i),fi(i),i,qq(i)+cq(i),cq(i)
            else
             write(inpf3,1171) qq(i),fi1(i),fi2(i),hi(i),i,qq(i)+cq(i),
     1              cq(i)
            endif
 350     continue
c&end modif
c
         call dcopy_wr(nq,cq,1,cqsave,1)
c        # distort geometry
         call distort(
c&line modif
     &    nek,   nqint,      na,       cq,        qq,
     &    qq1,      xa,      ibcode,   ibcontr,   b,
     &    shftpt,   xy,      cqsave)
c
c        # detemine convergence and write out new geometry
c
c&new lines
         if(iconint.eq.1) call conintsav(nq,nqint,cqsave,zeta1,zeta2)
c&new lines end
c&line modif
         call convchk(nqint,cqsave,fi,exitcode,iconint)
         call writgeom(
     &    xy,       symb,   ia,    na,    gdiismg,
     &    geomst,   geomfm, iangs, nlist, icond,
     &    exitcode, mass)
c
      else !if (.not.lgdi.or.ngeo.lt.2)
c
c  ##---------------------------
c  ##  GEOMETRY DIIS RELAXATION
c  ##---------------------------
c
       write(nlist,'(10x,''Performing gdiis relaxation'')')
       write(icond,'(10x,''Performing gdiis relaxation''/)')
       write (icond,50) ngeo
50     format (/1x,'number of geometries in gdiis ngeo =',i4/)
       write (nlist,50) ngeo
c
       if (ngeo.gt.itopti) then
         write (icond,53) itopti,ngeo, itopti
         write(nlist,53) itopti,ngeo,itopti
 53      format(' gdiis uses only the last ',i4,' geometries stored in
     +     file gdiisfl. ngeo = ',i4,' itopti = ',i4)
       end if ! if (ngeo.gt.itopti)
c
c       MD IS THE MAX. NUMBER OF GDIIS STEPS
       if (ngeo.gt.md-1) then
         write(nlist,55) md-1
         write(icond,55) md-1
 55      format ('  too many steps in geometry diis, max=', I3)
         call bummer
     &     ('too many steps in geometry diis, max=',md-1,faterr)
       end if ! if (ngeo.gt.md-1)
c
       if(iconint.ne.1) then
c
c  ##----------------------------
c  ##  PLAIN OPTIMIZATION VERSION
c  ##----------------------------
c
c  ##   UPDATE HESSIAN
c
         if (lmur) then
          call bfgs(ngeo,nq,lq,md,qd,fd,zk,nfix,ifix,c,fcold,writ,
     +       nlist)
c
          do 666,iii=1,nq*nq
             if (mod(iii,nq+1).eq.1) then
              fc(int(iii/(nq+1))+1)=c(iii)
             endif ! if (mod(iii,nq+1).eq.1)
 666      continue ! do 666,iii=1,nq*nq
          call hsswrite(c,csave,nq)
         endif ! if (lmur)
c
c  ##  check whether energy goes up
c
         enup=.false.
         if (etot1.gt.energy(ngeo-1).and.isad.eq.0)then
          enup=.true.
          write(nlist,940) etot,etot1,energy(ngeo-1)
          write(icond,940) etot,etot1,energy(ngeo-1)
 940      format(' energy goes up in non-saddle point calculation',
     1     3f15.8 )
C           # contrary to the previous opinion, leave GDIIS in,
C           # even if the energy goes up
C           # DO NOT CONTINUE WITH GDIIS IF IT WAS GDIIS BEFORE
            call dcopy_wr(nq,QD(1,NGEO-1),1,QQ1,1)
            call dcopy_wr(nq,FD(1,NGEO-1),1,DG,1)
c
C           # CALCULATE THE DISPACEMENT VECTOR BETWEEN THE LAST
c           # POINT (0) AND THE PREVIOUS ONE (1)
            CALL daxpy_wr(NQ,-ONE,QQ,1,QQ1,1)
C           # QQ1 CONTAINS Q(1)-Q(0)
            F0=ddot_wr(NQ,QQ1,1,FI,1)*AJOULE
            F1=ddot_wr(NQ,QQ1,1,DG,1)*AJOULE
            IFAIL=0
            CALL COSFIT(ETOT1,ENERGY(NGEO-1),F0,F1,XX,EMIN,IFAIL)
            WRITE(ICOND,*) 'COSFIT, XX=,FAIL', XX,EMIN,IFAIL
            WRITE(nlist,*) 'COSFIT, XX=,FAIL', XX,EMIN,IFAIL
            MAXCOM=idamax_wr(NQ,QQ1,1)
            XX2=ONE
C           # The interpolation goes wrong if a component is
C           # truncated later so do it approximately now
            IF(ABS(XX*QQ1(MAXCOM)).GT.COORMX) THEN
               XX1=XX*ABS(COORMX/(QQ1(MAXCOM)*XX))
               WRITE(ICOND,950) XX,XX1
               WRITE(nlist,950) XX,XX1
               XX2=XX1/XX
               XX=XX1
            END IF              ! IF(ABS(XX*QQ1(MAXCOM)).GT.COORMX)
950         FORMAT(
     &       ' EXTRAPOLATION EXCEEDS MAX. DISPLACEMENT, SCALED BACK',
     &       2F10.5)
C           # PUT THE INTERPOLATED GEOMETRY IN DQ
            call dcopy_wr(nq,qq,1,dq,1)
            CALL daxpy_wr(NQ,XX,QQ1,1,DQ,1)
C           # INTERPOLATED GEOM. IN DQ
C           # Q1-Q0 is still in QQ1
C           # ESTIMATE THE GRADIENTS AT THE EXTRAPOLATED POSITION
C           # FI+XX*(DG-FI)=XX*DG+(1-XX)FI
            CALL dscal_wr(NQ,XX,DG,1)
            CALL daxpy_wr(NQ,(ONE-XX),FI,1,DG,1)
C           # PROJECT THIS TO THE LINE Q1-Q0 UNLESS XX HAS BEEN
C           # ARTIFICIALLY CUT BACK
            IF(IFAIL.EQ.0) THEN
               SSS=ddot_wr(NQ,QQ1,1,QQ1,1)
               SSA=ddot_wr(NQ,QQ1,1,DG,1)
               CALL daxpy_wr(NQ,-SSA/SSS*XX2,QQ1,1,DG,1)
               call wzero(nq,dg,1)
            END IF ! IF(IFAIL.EQ.0)
c
         else ! if (etot1.gt.energy(ngeo-1).and.isad.eq.0)
           if (ngeo.gt.itopti) then
            mexclu=ngeo-itopti
           endif
c
c  ## perform diis extrapolation
c  ## on output:
c  ## dq: extrapolated geometry
c  ## dg: extrapolated force
c
           call calld (
     &             ngeo+1,   md,   lq,   nq,
     &             qd,      fd,       c ,   a,    l,
     &             m,       dq,       dg,   dc,   nfix,
     &             ifix,    mexclu)
c
         end if ! if (etot1.gt.energy(ngeo-1).and.isad.EQ.0)
c
c  ## reduce the step size if energy goes up
c
         scfac=one
         if(enup.and.isad.eq.0) scfac=half
c
c  ##  displacements
c  ##  on output: cq: relaxations step
c
         call relaxd(nq,dg,c,cq,edec,dismax,icmx,scfac)
c
c  ##  calculate the new geometry into qq1 (diis + relaxation)
c
         call dcopy_wr(nq,dq,1,qq1,1)
         call daxpy_wr(nq,one,cq,1,qq1,1)
c
c  ##  put the actual geometry change (diis + relaxation) into cq
c
         dismax=zero
         do 960 I=1,NQ
          if(i.le.nqint) cq(i)=dq(i)+cq(i)-qd(i,ngeo)
          if(abs(cq(i)).gt.dismax) then
            dismax=abs(cq(i))
            icmx=i
          end if                 ! if(abs(cq(i)).gt.dismax)
 960     continue
c
c  ##  distortion too large
c  ##  do not scale the whole vector, only the offending component
c
         if (dismax.gt.coormx) then
          do 970 ii=1,nqint
            if(abs(cq(ii)).gt.coormx) cq(ii)=cq(ii)*(coormx/abs(dismax))
 970      continue
          dismax=coormx
          call dcopy_wr(nq,qq,1,qq1,1)
          call daxpy_wr(nq,one,cq,1,qq1,1)
         end if ! if (dismax.gt.coormx)
c
c  ## printout
c
         call onecol(nlist,nq,cq,qq1,
     &  ' geometry change and new internal coordinates')
         call twocol(icond,nq,cq,qq1,
     &  ' geometry change and new internal coordinates')
c
c       ZERO OUT THE FORCE IN THE DIRECTION OF THE FIXED COORDINATES
c   obsolate??
         DO  975 I=1,NFIX
         II=IFIX(I)
         FI(II)=ZERO
 975     CONTINUE
c
c ##  calculate the max. estimated energy change/coordinate
c
         call absmax(nqint,cq,icmx,dismax)
         call enmax(nqint,fi,cq,thract,imax)
         write(nlist,340) itopti,etot,etot1,icmy,fmax1,icmx,cq(icmx),
     &        imax,thract
         write(icond,340)itopti,etot,etot1,icmy,fmax1,icmx,cq(icmx),
     &        imax,thract
c
c  ##   write archive file
c  ##   round no., total e, max. force, max. change, max. en. change
c
         write(inpf3,345)  curr_iter,etot1,fmax1,icmy,cq(icmx),icmx,
     &    edel
         write(inpf3,'(" old coordinate |      force    |",
     &           "no.|new coord. |displacement")')
         do 360 i=1,nq
          write(inpf3,1170) qq(i),fi(i),i,qq(i)+cq(i),cq(i)
 360     continue
c
c  ## calculate the new cartesian coordinates
c
         call dcopy_wr(nq,cq,1,cqsave,1)
         call distort(
     &        nek,  nqint,    na,      cq,       qq,
     &        qq1,     xa,    ibcode,  ibcontr,  b,
     &        shftpt,  xy,    cqsave)
c
c  ## detemine convergence and write out new geometry
c
         call convchk(nqint,cqsave,fi,exitcode,iconint)
         call writgeom(
     &        xy,        symb,    ia,     na,     gdiismg,
     &        geomst,    geomfm,  iangs,  nlist,  icond,
     &        exitcode,  mass)
c
       else !(iconint.ne.1)
c
c  ##-----------------------------
c  ##  CONICAL INTERSECTION SEARCH
c  ##-----------------------------
         if(mod(curr_iter,2).eq.0) then
           dodiis=.true.
           write(nlist,*) 'doing diis extrapolation'
         else
           dodiis=.false.
           write(nlist,*) 'doing relaxation only'
         endif
c
c  ## calculate the geometry change for the current point
c
         call relaxd(nq,fi,c,cq,edecr,dismax,icmx,one)
         call dcopy_wr(nq,qq,1,qq1,1)
         call daxpy_wr(nq,one,cq,1,qq1,1)
         call dcopy_wr(nq,cq,1,dqd(1,ngeo),1)
         call onecol(nlist,nq,cq,qq1,
     &  ' predicted relaxation step')
c
c  ## simple relaxation step
c
         if(.not.dodiis) goto 143
c
c  ##   UPDATE OF HESSIAN NOT AVAILABLE
c
         if (lmur) then
           call bummer
     &           ('no update of hessian for ci search',0,faterr)
         endif ! if (lmur)
c
         if (ngeo.gt.itopti) then
          mexclu=ngeo-itopti
         endif
c
c  ## perform diis extrapolation
c  ## on output:
c  ## dq: extrapolated geometry
c  ## dg: extrapolated force
c  ## fi1, fi2, hi are also filled up with extrapolated quantities
c
         call calld1 (
     &             ngeo+1,   md,   lq,   nq,
     &             qd,      fd,       c ,   a,    l,
     &             m,       dq,       dg,   dc,   nfix,
     &             ifix,    mexclu,   dqd)
c
c  ## copy the extrapolated force into fi and geom to qq1
c
c&         call dcopy_wr(nqint,dg,1,fi,1)
         call dcopy_wr(nqint,dq,1,qq1,1)
c
c  ## assume that the new point is degenerate
c
c&         if(iconint.eq.1) fi(nq-1)=0.d0
c&         write(nlist,*) 'fi(nq),fi(nq-1)',fi(nq),fi(nq-1)
c
c  ## in the gdiis case add two contributions:
c  ##                              interpolation and relaxation
c
         dismax=zero
         do 1960 I=1,NQ
            if(i.le.nqint) cq(i)=dq(i)-qd(i,ngeo)
            if(abs(cq(i)).gt.dismax) then
             dismax=abs(cq(i))
             icmx=i
            end if                 ! if(abs(cq(i)).gt.dismax)
 1960    continue
c
c  ##  distortion too large
c  ##  do not scale the whole vector, only the offending component
c
         if (dismax.gt.coormx) then
          do 1970 ii=1,nqint
            if(abs(cq(ii)).gt.coormx) cq(ii)=cq(ii)*(coormx/abs(dismax))
 1970     continue
          dismax=coormx
          call dcopy_wr(nq,qq,1,qq1,1)
          CALL daxpy_wr(nq,one,cq,1,qq1,1)
         end if ! if (dismax.gt.coormx)
c
 143     continue
c
c  ## printout
c
       call onecol(nlist,nq,cq,qq1,
     &  ' geometry change and new internal coordinates')
       call twocol(icond,nq,cq,qq1,
     &  ' geometry change and new internal coordinates')
c
c       ZERO OUT THE FORCE IN THE DIRECTION OF THE FIXED COORDINATES
c  obsolate?
         DO  1975 I=1,NFIX
         II=IFIX(I)
         FI(II)=ZERO
 1975    CONTINUE
c
c  ##  calculate the max. estimated energy change/coordinate
c
         call absmax(nqint,cq,icmx,dismax)
         call enmax(nqint,fi,cq,thract,imax)
         write(nlist,340) itopti,etot,etot1,icmy,fmax1,icmx,cq(icmx),
     &        imax,thract
         write(icond,340)itopti,etot,etot1,icmy,fmax1,icmx,cq(icmx),
     &        imax,thract
c
c  ##   write archive file only if diis has been performed
c  ##   round no., total e, max. force, max. change, max. en. change
c
         if(dodiis) then
          write(inpf3,345)  curr_iter,etot1,fmax1,icmy,cq(icmx),icmx,
     &    edel
          write(inpf3,'(" old coordinate | force state1  |",
     &           "force state2  | coupling vector|",
c&     &           "no.|    new coord. |   displacement ")')
     &           "no.|    new coord. |   error vector(NR)")')
          do 1360 i=1,nq
          write(inpf3,1171) qq(i),fd1(i,ngeo),fd2(i,ngeo),fdh(i,ngeo),i,
     &           qq(i)+cq(i),dqd(i,ngeo)
c&error!!           write(inpf3,1171) qq(i),fi1(i),fi2(i),hi(i),i,qq(i)+
c&error!!     &           dqd(i,ngeo)
 1360     continue
c
         endif
c
c  ## calculate the new cartesian coordinates
c
         call dcopy_wr(nq,cq,1,cqsave,1)
         call distort(
     &        nek,  nqint,    na,      cq,       qq,
     &        qq1,     xa,    ibcode,  ibcontr,  b,
     &        shftpt,  xy,    cqsave)
c
c  ##  detemine convergence and write out new geometry
c
         call conintsav(nq,nqint,cqsave,zeta1,zeta2)
         call convchk(nqint,cqsave,fi,exitcode,iconint)
         call writgeom(
     &        xy,        symb,    ia,     na,     gdiismg,
     &        geomst,    geomfm,  iangs,  nlist,  icond,
     &        exitcode,  mass)
         call retmem(1)
         call bummer('normal termination',0,3)
         stop 'end of gdiis'
c
       end if !(iconint.ne.1)
c
      end if !if (.not.lgdi.or.ngeo.lt.2)
c
      call retmem(1)
      call bummer('normal termination',0,3)
      stop 'end of gdiis'
C
      end
      subroutine gdiis_sub1(mm1,mexclu,mnq,m1,nq,q,g,h,a,ll,mm,dq,dg,c,
     1 xlam,nfix,ifix,ier,nlist,dqd)
c
       implicit none
c     #dummy:
      integer mm1, mexclu, mnq, m1, nq, nfix, ier, nlist
      integer ll(nq), mm(nq), ifix(nfix)
      real*8 xlam
      real*8 q(mnq,mm1), g(mnq,mm1), h(*), a(m1,m1),
     & dq(nq), dg(nq), c(m1), dqd(mnq,mm1)
c
c     # common related to conical intersection calculations:
c     # ma is the max. number of atoms
      integer ma
      parameter (ma=200)
c     # lq is the max. number of cartesisans
      integer lq
      parameter (lq=3*ma)
c     # md is the max. number of gdiis geometry steps (or bfgs)
      integer md
      parameter (md=100)
      real*8 fc1,fc2,h1,fi1,fi2,hi,zeta1,zeta2,de,fd1,fd2,fdh
      integer*4 iconint
      common /conint/ fc1(lq),fc2(lq),h1(lq),
     &     fi1(lq),fi2(lq),hi(lq),zeta1,zeta2,de,
     &     fd1(lq,md),fd2(lq,md),fdh(lq,md),iconint
c
c   # constants:
      real*8          zero,  half,  one,   two, three,  four,  five,
     &                ten,   ten6,  tenm8, pi,  acc
      common /number/ zero,  half,  one,   two, three,  four,  five,
     &                ten,   ten6,  tenm8, pi,  acc
c     # local:
      integer i, ii, j, k, m
      real*8 cofmx, det, gdmin, prod, s, si, sj, sj1, sj2, sjh, xdi
c
      real*8    gdinit,      gdfact,      small,       told
      parameter(gdinit=1d10, gdfact=1d-3, small=1d-10, told=1d-11)
c
c
c    constructs and solve diis equations for conical intersection search
c
c     parameters
c     mm1= maximal value of m1
c     mexclu - omit the first mexclu geometry-force set
c     mnq= maximal value of nq
c     m1 = number of points +1
c          (nq.ge.m1 and m1.ge.3 must be hold)
c     nq = number of variables(coordinates)
c          (dimension of the q and g vectors)
c     q  = vectors of the variables
c          stored columnwise as ((q(i,j),i=1,nq),j=1,m)
c     g  = vectors of forces (first derivatives)
c          stored similarly to q
c     dgd  = error vectors stored similarly to q
c     h  =inverse of the hessian(second derivatives) NOT IN THIS VERSION
c     a  = diis matrix
c     ll(*),mm(*) = workspace
c     dq = vector of predicted variables
c     dg = vector of predicted gradients at dq
c     c  = vector of diis coefficient
c     xlam= lagrangian multiplier
c     nfix (input) number of fixed coordinates
c     ifix(nfix) (input) fixed coordinates
c     ier= error code
c        =0 no error
c        =1 the diis matrix is singular
c           (the gradients are linearly dependent)
c
c     method
c     p.pulay,chem.phys.lett.,23,393(1980)
c     p. pulay, j. comp. chem. 1982
c     p.csaszar, p. pulay, j. chem. struct.
c     p.g. szalay unpublished
c
      ier=0
      m=m1-1
c      write(nlist,*) 'mexclu=',mexclu
      if(mexclu.gt.m) mexclu=m
c
c     # construct diis matrix (includes the excluded ones!!!)
c
      do 70 i=1,m
         do 20 ii=1,nq
            dq(ii)=dqd(ii,i)
c            write(nlist,*) 'dqd(',ii,',',i,')=',dqd(ii,i)
 20      continue
         do 60 j=1,i
            do 40 ii=1,nq
               dg(ii)=dqd(ii,j)
c            write(nlist,*) 'dqd(',ii,',',j,')=',dqd(ii,j)
 40         continue
            s=zero
            do 50 k=1,nq-2
               s=s+dq(k)*dg(k)
 50         continue
            a(i,j)=s
            a(j,i)=s
 60      continue
         a(i,m1)=-one
         a(m1,i)=-one
 70   continue
      a(m1,m1)=zero
c
c     # diis matrix is done
c
c
c     # now exclude rows and columns if requested
c
      if (mexclu.gt.0) then
         do 72 i=1,mexclu
            do 71 j=i,m1
               a(i,j)=zero
               if (i.eq.j) a(i,j)=one
               a(j,i)=a(i,j)
 71         continue
            c(i)=zero
 72      continue
         if (m-mexclu.eq.1) then
            c(m)=one
            go to 135
c           # no diis if there is only one degree of freedom
         end if
      end if
c
c     # add the smallest diagonal element (times 10^-3) to all diagonals
c     # scale all elements by the trace (normalized)  ????
c
      gdmin = gdinit
      do 100 i=1,m
         gdmin=min(gdmin,a(i,i))
100   continue
      gdmin = gdmin * gdfact
      prod = one
      do 102 i = 1, m
         a(i,i)=a(i,i)+gdmin
         prod=prod*a(i,i)
102   continue
c
c-      prod=prod**(-one/m)
      prod = one
      do 104 i = 1, m
         do 106 j = 1, m
            a(i,j) = a(i,j) * prod
106      continue
104   continue
c
c     # invert the diis matrix
c
      call osinv (a,m1,det,told,ll,mm)
      write(nlist,*) 'determinant of the diis mx=',det
c     # if singular, det is exactly zero
      if(det.eq.zero) then
         ier = 1
         return
      endif
c
c     # diis coefficients and the lagrangian
c
      cofmx=zero
      do 130 i=1,m
         c(i)=-a(i,m1)
         if (abs(c(i)).gt.cofmx) cofmx=abs(c(i))
 130  continue
c     # if the coefitients aree too large, error
       if (cofmx.gt.ten) then
          ier=1
          return
       end if
c     new line by ph
c     xlam=-a(m1,m1)   old line
      xlam=-a(m1,m1)/prod
c
c     # predicted variables and gradients
c
 135  do 150 i=1,nq-2
         si=0.0
         sj=0.0
         sj1=0.0
         sj2=0.0
         sjh=0.0
         do 140 j=1,m
            si=si+c(j)*q(i,j)
            sj1=sj1+c(j)*fd1(i,j)
            sj2=sj2+c(j)*fd2(i,j)
            sjh=sjh+c(j)*fdh(i,j)
  140    continue
c         write(nlist,'(2F16.10)') zeta1,zeta2
         dq(i)=si
         fi1(i)=sj1
         fi2(i)=sj2
         hi(i)=sjh
         dg(i)=sj1 + zeta1*(sj1-sj2) + zeta2*sjh
c         write(nlist,'(1x,i3,6F10.6)') i,si,sj1,sj2,sjh,dq(i),dg(i)
  150 continue
c
c     # zero out fixed coordinates
c
      do 170 i=1,nfix
        ii=ifix(i)
        dq(ii)=q(ii,1)
  170 continue
c
c      do 180,iii=1,nq
c       WRITE(nlist,190) Q(III,m),DQ(III),DG(III)
c  190  FORMAT(1X,4F16.12)
c 180  continue
c
c     # recover the this matrix
c
      call osinv (a,m1,det,told,ll,mm)
      return
c
      end
      subroutine gdiis_sub(
     & mm1,  mexclu, mnq,   m1,
     & nq,   q,       g,     h,
     & a,    ll,      mm,    dq,
     & dg,   c,       xlam,  nfix,
     & ifix, ier,     nlist)
c
       implicit none
c     # dummy:
      integer mm1, mexclu, mnq, m1, nq, nfix, ier, nlist
      integer ll(nq), mm(nq), ifix(nfix)
      real*8 xlam
      real*8 q(mnq,mm1), g(mnq,mm1), h(*), a(m1,m1),
     & dq(nq), dg(nq), c(m1)
c
c     # constants
      real*8          zero,  half,  one,   two, three,  four,  five,
     & ten,   ten6,  tenm8, pi,  acc
      common /number/ zero,  half,  one,   two, three,  four,  five,
     & ten,   ten6,  tenm8, pi,  acc
c
c     # local:
      integer i, ifx, ii, iik, j, jj, k, m
      real*8 cofmx, det, gdmin, prod, s, si, sj, xdi
c
      real*8    gdinit,      gdfact,      small,       told
      parameter(gdinit=1d10, gdfact=1d-3, small=1d-10, told=1d-11)
c
c     constructs and solve diis equations
c
c     parameters
c     mm1= maximal value of m1
c     mexclu - omit the first mexclu geometry-force set
c     mnq= maximal value of nq
c     m1 = number of points +1
c          (nq.ge.m1 and m1.ge.3 must be hold)
c     nq = number of variables(coordinates)
c          (dimension of the q and g vectors)
c     q  = vectors of the variables
c          stored columnwise as ((q(i,j),i=1,nq),j=1,m)
c     g  = vectors of forces (first derivatives)
c          stored similarly to q
c     h  = inverse of the hessian (second derivatives)
c     a  = diis matrix
c     ll(*),mm(*) = workspace
c     dq = vector of predicted variables
c     dg = vector of predicted gradients at dq
c     c  = vector of diis coefficient
c     xlam= lagrangian multiplier
c     nfix (input) number of fixed coordinates
c     ifix(nfix) (input) fixed coordinates
c     ier= error code
c        =0 no error
c        =1 the diis matrix is singular
c           (the gradients are linearly dependent)
c
c     method
c     p.pulay,chem.phys.lett.,23,393(1980)
c     p. pulay, j. comp. chem. 1982
c     p.csaszar, p. pulay, j. chem. struct.
c
c
      ier    = 0
      m      = m1 - 1
c      write(nlist,*) 'mexclu=', mexclu
      if(mexclu .gt. m) mexclu = m
c
c     # construct diis matrix (includes the excluded ones!!!)
c
      do 70 i = 1, m
c        # calculate the error vector for the i'th iterate
c        # dq(ii,i) = sum_jj h(ii,jj)*g(jj,i) (negative of NR step)
         do 20 ii = 1, nq
            s = zero
            do 12 jj = 1, nq
               iik = (ii-1) * nq + jj
               xdi = h(iik)
c              # omit fixed coordinates
               do 15 ifx = 1, nfix
                  if(ifix(ifx) .eq. jj .and. ifix(ifx) .eq. ii) then
                     xdi=zero
                  endif
15             continue
               s=s+xdi*g(jj,i)
12          continue
            dq(ii)=s
20       continue
         do 60 j=1,i
            do 40 ii=1,nq
               dg(ii)=g(ii,j)
40          continue
c           # error matrix ala Ahlrichs:  a(i,j) = sum_k dq(k,i)*dg(k,j)
c           #original error vector would be a(i,j)=sum_k dq(k,i)*dq(k,j)
            s=zero
            do 50 k=1,nq
               s=s+dq(k)*dg(k)
50          continue
            a(i,j)=s
            a(j,i)=s
60       continue
         a(i,m1)=-one
         a(m1,i)=-one
70    continue
      a(m1,m1)=zero
c
c     # diis matrix is done
c
c
c     # now exclude rows and columns if requested
c
      if (mexclu.gt.0) then
         do 72 i=1,mexclu
            do 71 j=i,m1
               a(i,j)=zero
               if (i.eq.j) a(i,j)=one
               a(j,i)=a(i,j)
71          continue
            c(i)=zero
72       continue
         if (m-mexclu.eq.1) then
            c(m)=one
            go to 135
c           # no diis if there is only one degree of freedom
         end if
      end if
c
c     # add the smallest diagonal element (times 10^-3) to all diagonals
c     # scale all elements by the trace (normalized)  ????
c
      gdmin = gdinit
      do 100 i=1,m
         gdmin=min(gdmin,a(i,i))
100   continue
      gdmin = gdmin * gdfact
      prod = one
      do 102 i = 1, m
         a(i,i)=a(i,i)+gdmin
         prod=prod*a(i,i)
102   continue
c
c-      prod=prod**(-one/m)
      prod = one
      do 104 i = 1, m
         do 106 j = 1, m
            a(i,j) = a(i,j) * prod
106      continue
104   continue
c
c     # invert the diis matrix
c
      call osinv (a,m1,det,told,ll,mm)
      write(nlist,*) 'determinant of the diis mx=',det
c     # if singular, det is exactly zero
      if(det.eq.zero) then
         ier = 1
         return
      endif
c
c     # diis coefficients and the lagrangian
c
      cofmx = zero
      do 130 i = 1, m
         c(i) = -a(i,m1)
         if (abs(c(i)) .gt. cofmx) cofmx = abs(c(i))
130   continue
c     # if the coeficients are too large, error
      if (cofmx .gt. ten) then
         ier=1
         return
      end if
c     # new line by ph
c     xlam=-a(m1,m1)   old line
      xlam = -a(m1,m1) / prod
c
c     # predicted variables and gradients
c
135   do 150 i=1,nq
         si = zero
         sj = zero
         do 140 j=1,m
            si=si+c(j)*q(i,j)
            sj=sj+c(j)*g(i,j)
140      continue
         dq(i)=si
         dg(i)=sj
150   continue
c
c     # zero out fixed coordinates
c
      do 170 i=1,nfix
         ii=ifix(i)
         dq(ii)=q(ii,1)
170   continue
c
c     # recover the this matrix
c
      call osinv (a,m1,det,told,ll,mm)
      return
c
      end
      SUBROUTINE GETMEM(AMOUNT,ADDR)
C  THIS SUBROUTINE RESERVES AMOUNT SPACE IN THE COMMON BLOCK
C  BL, CHECKS FOR MAX. MEMORY, AND RETURNS AN INTEGER ADDRESS
C  BL(ADDR) TO BL(ADDR+AMOUNT-1) IS RESERVED.
C  USE: E.G. TO RESERVE 3 MATRICES,  A(N,N), B(N,3*K),C(N,3*K),
C  AND CALCULATE C=A*B. LET US ASSUME THAT ONLY C IS NEEDED LATER,
C  THEN GIVE C THE LOWER ADDRESS!!!
C  IN THE MAIN (CALLING) PROGRAM:
C     CALL GETMEM(N*3*K,IC)
C     CALL GETMEM(N**2,IA)
C     CALL GETMEM(N*3*K,IB)
C     CALL CALCCMX(BL(IA),BL(IB),BL(IC),N,3*K)
C  ....
C     IN THE CALLED PROGRAM CALCCMX:
C     SUBROUTINE CALCCMX(A,B,C,N,K3)
C     IMPLICIT REAL*8 (A-H,O-Z)
C     DIMENSION A(N,N),B(N,K3),C(N,K3)
C     ... GIVE VALUES TO A AND B
C     CALL SETA(A,N)
C     CALL SETB(B,N,K3)
C     CALL MTRXMUL(A,B,C,N,N,K3)
C     CALL RETMEM(2)
C      THESE LAST CALL FREES THE SPACE TAKEN UP BY A AND B
c
       implicit none
c     # dummy:
      INTEGER AMOUNT, ADDR
c
      integer        nreq,maxmem,nadr
      COMMON /MMANAG/NREQ,MAXMEM,NADR(400)
c
      integer       lcore,iov,last,lflag,   inuc,ibas,na,nbf,nsh,ncf,
     & ncs,nsy,   nsym,nganz,    lopt
      COMMON /GANZ/ LCORE,IOV,LAST,LFLAG(4),INUC,IBAS,NA,NBF,NSH,NCF,
     & NCS,NSY(4),NSYM,NGANZ(35),LOPT(30)
c
C     # NREQ, MAXMEM AND NADR(1) MUST BE INITIALIZED TO 0
C     # SEE RETALL
C     # LCORE IS THE MAXIMUM AVAILABLE MEMORY
c
c     # local:
      integer ixx
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      NREQ = NREQ + 1
      ADDR = NADR(NREQ) + 1
      IXX  = ADDR + AMOUNT - 1
      IF (IXX .GT. LCORE) THEN
         WRITE(*,*) 'MEMORY OVERFLOW,NEEDED',IXX,'AVAILABLE',LCORE
         CALL bummer('Not enough memory',0,faterr)
      END IF
      IF (IXX .GT. MAXMEM) MAXMEM=IXX
      NADR(NREQ+1) = IXX
C
      END
      subroutine hssread(c,nq,ifix,nfix,hesstype)
c
c     # subroutine read the hessian (inverse hessian matrix) and returns
c     # the inverse hessian matrix
c
       implicit none
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
c     # constants
      real*8          zero,  half,  one,   two, three,  four,  five,
     & ten,   ten6,  tenm8, pi,  acc
      common /number/ zero,  half,  one,   two, three,  four,  five,
     & ten,   ten6,  tenm8, pi,  acc
c
c     # unit numbers
      integer       inp,inp2,nlist,ipun,iarc,icond,itest
      common /tape/ inp,inp2,nlist,ipun,iarc,icond,itest
c
      integer        ngeom,   nforc,   nintc,  nfmat,   nform,
     & nener,   inpf3,   nmess,  nhess,   nhessinv,
     & ncuriter
      common /tape2/ ngeom,   nforc,   nintc,  nfmat,   nform,
     & nener,   inpf3,   nmess,  nhess,   nhessinv,
     & ncuriter
c
c     # file names
      character*10     gdiisin,  gdiisls,  gdiissm,  default,
     & geomfl,   forcfl,   intcfl,   fmatfl,
     & formfl,   convfl,   gdiisfl,  gdiismg,
     & hessfl,   hessinvfl,curiterfl,zetafile,convfl2
      common /flnames/ gdiisin,  gdiisls,  gdiissm,  default,
     & geomfl,   forcfl,   intcfl,   fmatfl,
     & formfl,   convfl,   gdiisfl,  gdiismg,
     & hessfl,   hessinvfl,curiterfl,zetafile,convfl2
c
c     ##  integer section
c
      integer i, j, icount, info, ii, ifix(*)
      integer file
      integer hesstype
      integer k, kpvt(1000)
      integer nq, nfix
c
c     ##  real*8 section
c
      real*8 c(*), scr(1000)
c
c-----------------------------------------------------------------
c
c     #  hesstype = 1 read inverse hessian matrix
c     #  hesstype = 2 read hessian matrix
c
      if (hesstype.eq.1) then
         open(nhessinv,file=hessinvfl,status='old',form='formatted')
         file=nhessinv
      elseif(hesstype.eq.2) then
         open(nhess,file=hessfl,status='old',form='formatted')
         file=nhess
      else
         call bummer('hssread: internal error, call programmer',
     &    0,faterr)
      endif
c
      icount=0
      do k=1,nq
         read(file,30) (c(i+icount),i=1,nq)
         icount=icount+nq
      enddo
30    format(8f13.6)
      close(file)
c
c     ##  apply fixc to hessian matrix
c
      do i=1,nfix
         ii= ifix(i)
c        # zero out the ii-th row and column of the hessian matrix
         icount = 0
         do j=1,nq
            if (ii .eq. j) call wzero(nq,c(icount+1),1)
            c(icount+ii)=zero
            icount = icount + nq
         enddo
c        # set ii-th diagonal element to one
         c(nq*(ii-1)+ii)=one
      enddo
c
c     ##  calculate the inverse of the hessian matrix
c
      if (hesstype.eq.2) then
         call prblkt('Hessian matrix (including fixc)',
     &    c,nq,nq,nq,'int','int',1,nlist)
         write(nlist,'(/3x,''Calculating the inverse Hessian matrix'')')
         call dgetrf_wr(nq,nq,c,nq,kpvt,info)
         if (info.ne.0) call bummer(
     &    'hssread:error in dgetrf, info=',info,faterr)
         call dgetri_wr(nq,c, nq,kpvt, scr,1000,info)
         if (info.ne.0) call bummer(
     &    'hssread:error in dgetri, info=',info,faterr)
      endif
c
      call prblkt('Inverse Hessian matrix (including fixc)',
     & c(1),nq,nq,nq,'int','int',1,nlist)
      write(nlist,*)
c
      return
      end
      subroutine hsswrite( c, csave, nq)
c
       implicit none
c     # ma is the max. number of atoms
      integer    ma
      parameter (ma=200)
c     # lq is the max. number of cartesisans
      integer    lq
      parameter (lq=3*ma)
c     # mq is the max. number of internal coordinates,
      integer    mq
      parameter (mq=lq-6+2)
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer        ngeom,   nforc,   nintc,  nfmat,   nform,
     & nener,   inpf3,   nmess,  nhess,   nhessinv,
     & ncuriter
      common /tape2/ ngeom,   nforc,   nintc,  nfmat,   nform,
     & nener,   inpf3,   nmess,  nhess,   nhessinv,
     & ncuriter
c
c     # file names
      character*10     gdiisin,  gdiisls,  gdiissm,  default,
     & geomfl,   forcfl,   intcfl,   fmatfl,
     & formfl,   convfl,   gdiisfl,  gdiismg,
     & hessfl,   hessinvfl,curiterfl,zetafile,convfl2
      common /flnames/ gdiisin,  gdiisls,  gdiissm,  default,
     & geomfl,   forcfl,   intcfl,   fmatfl,
     & formfl,   convfl,   gdiisfl,  gdiismg,
     & hessfl,   hessinvfl,curiterfl,zetafile,convfl2
c
c     ##  integer section
c
      integer icount, i, info
      integer j
      integer kpvt(mq)
      integer nq
c
c     ## real*8 section
c
      real*8 c(*), csave(*), scr(2*mq)
c
c--------------------------------------------------------------
c
      open(nhess,file=hessfl,status='unknown',form='formatted')
      open(nhessinv,file=hessinvfl,status='unknown',form='formatted')
c
c     ## write out the inverse hessian matrix
c
      icount=0
      do j=1,nq
         write(nhessinv,30)(c(i+icount),i=1,nq)
         icount = icount + nq
      enddo
c
c     ## calculate the hessian matrix out of the inverse hessian matrix
c
      call dcopy_wr(nq*nq,c,1,csave,1)
      call dgetrf_wr(nq,nq,c,nq,kpvt,info)
      if (info.ne.0) call bummer(
     & 'hsswrite:error in dgetrf, info=',info,faterr)
      call dgetri_wr(nq,c, nq,kpvt, scr,2*mq,info)
      if (info.ne.0) call bummer(
     & 'hsswrite:error in dgetri, info=',info,faterr)
c
c     ## write out the hessian matrix
c
      icount=0
      do j=1,nq
         write(nhess,30)(c(i+icount),i=1,nq)
         icount = icount + nq
      enddo
      close(nhess)
c
30    format(8f13.6)
c
      call dcopy_wr(nq*nq,csave,1,c,1)
      return
      end
      SUBROUTINE INIT
c
      IMPLICIT none
C     .... PRELIMINARY INPUT PROCESSING,SET INITIAL VALUES
C
c
c   unit numbers
      integer       inp,inp2,nlist,ipun,iarc,icond,itest
      common /tape/ inp,inp2,nlist,ipun,iarc,icond,itest
c
      integer        ngeom,   nforc,   nintc,  nfmat,   nform,
     &               nener,   inpf3,   nmess,  nhess,   nhessinv,
     &               ncuriter
      common /tape2/ ngeom,   nforc,   nintc,  nfmat,   nform,
     &               nener,   inpf3,   nmess,  nhess,   nhessinv,
     &               ncuriter
c
c   file names
      character*10     gdiisin,  gdiisls,  gdiissm,  default,
     &                 geomfl,   forcfl,   intcfl,   fmatfl,
     &                 formfl,   convfl,   gdiisfl,  gdiismg,
     &                 hessfl,   hessinvfl,curiterfl,zetafile,convfl2
      common /flnames/ gdiisin,  gdiisls,  gdiissm,  default,
     &                 geomfl,   forcfl,   intcfl,   fmatfl,
     &                 formfl,   convfl,   gdiisfl,  gdiismg,
     &                 hessfl,   hessinvfl,curiterfl,zetafile,convfl2
c
      real*8        ang,debye,cbm,ajoule,evolt,ckalmo,dkel,cmm1,hertz
      common /unit/ ang,debye,cbm,ajoule,evolt,ckalmo,dkel,cmm1,hertz
c
c   constants
      real*8          zero,  half,  one,   two, three,  four,  five,
     &                ten,   ten6,  tenm8, pi,  acc
      common /number/ zero,  half,  one,   two, three,  four,  five,
     &                ten,   ten6,  tenm8, pi,  acc
c
c     # common numbers
c
      zero  = 0.0d0
      half  = 0.5d0
      one   = 1.0d0
      two   = 2.0d0
      three = 3.0d0
      four  = 4.0d0
      five  = 5.0d0
      ten   = 10.0d0
      ten6  = 1.0d+6
      tenm8 = 1.0d-8
      pi    = 3.14159265358979323846264338328d0
c
c     # common tape
c
      nlist = 8
      ipun  = 7
      inp   = 30
      inp2  = 31
      icond = 9
c
c     # common tape2
c
      ngeom    = 71
      nforc    = 72
      nintc    = 73
      nfmat    = 74
      nform    = 75
      nener    = 76
      inpf3    = 77
      nmess    = 78
      nhess    = 79
      nhessinv = 80
      ncuriter = 81
c
c     # file names
c
      gdiisin   = 'gdiisin'
      gdiisls   = 'gdiisls'
      gdiissm   = 'gdiissm'
      default   = 'gdiisin'
      hessfl    = 'hessian'
      hessinvfl = 'hessianinv'
      curiterfl = 'curr_iter'
c
c     # common units
c
      ang    = 1.889725988d0
      debye  = 0.39342658d0
      cbm    = 0.117946332d30
      ajoule = 0.229371044d0
      evolt  = 0.036749026d0
      ckalmo = 1.5936018d-3
      dkel   = 3.1667911d-6
      cmm1   = 4.5563352d-6
      hertz  = 1.51982983d-16
c
      return
      end
      subroutine intforc1(
     &  nq,nek,b,ibcontr,fc1,fc2,h1,fi1,fi2,hi,fi,zeta1,zeta2,de,
     &  qq,mxf,fmax1)
c
       implicit none
c    nq (input) number of coordinates
c    nek (input) 3 times the number of atoms (# of cartesians)
c    b(199,nq) (input) b matrix stored compressed
c    ibcontr(99,nq) coding of b
c    lq,mq dimensions of b (in the main program)
c    fc1(1..nek) (input) cartesian forces state 1
c    fc2(1..nek) (input) cartesian forces state 2
c    h1(1..nek) (input) cartesian coupling between state 1 and 2
c    fi1(1..nq) (output) internal forces state 1
c    fi2(1..nq) (output) internal difference
c    fi(1..nq+2) (output) extended internal force vector
c    hi(1..nq) (output) coupling in internal coiordinates
c    zeta1,zeta2  lagrange multipliers
c    de energy difference
c    nlist,icond (input)  output files
c    qq (input) coordinates
c    fmax1: maximum internal force
c
c   unit numbers
      integer       inp, inp2, nlist, ipun, iarc, icond, itest
      common /tape/ inp, inp2, nlist, ipun, iarc, icond, itest
c
      real*8 condamp
      common /options/condamp
c
      integer lcx
      parameter (lcx=99000)
      real*8 bl
      common bl(lcx)
c
      integer ix,iy,idm,ic,it,ip,ir,iz,i,imd
      integer mxf
      integer nq,nek
      integer ibcontr(99,nq)
c
      real*8 b(199,nq),qq(nq+2),fmax1
      real*8 fc1(nek),fc2(nek),h1(nek),fi1(nq+2),fi2(nq+2),hi(nq+2),de
      real*8 zeta1,zeta2,fi(nq+2)
c
      real*8 bbt
      external bbt
c
      real*8 onem
      parameter (onem=-1.d0)
c
      real*8 ddot_wr
c
c-------------------------------------------------------
c
      call getmem(nq,ix)
      call getmem(nq,iy)
      call getmem(nq,idm)
      call getmem(nq,ic)
      call getmem(nek,it)
      call getmem(nq,ip)
      call getmem(nq,ir)
      call getmem(nq,iz)
c
c  transform first the force of state 1
c
      call bxv(nq,nek,b,ibcontr,fc1,bl(ic))
      call bdiag(nq,nek,b,ibcontr,bl(idm))
      call congrad(
     & nq,     nek,     b,      ibcontr,    bl(ix),
     & bl(iy), bl(idm), bl(ic), bl(it),     bl(ip),
     & bl(ir), bl(iz),  bbt,    condamp)
      call dcopy_wr(nq,bl(ix),1,fi1,1)
c
c  transform the force of state 2
c
      call bxv(nq,nek,b,ibcontr,fc2,bl(ic))
      call bdiag(nq,nek,b,ibcontr,bl(idm))
      call congrad(
     & nq,     nek,     b,      ibcontr,    bl(ix),
     & bl(iy), bl(idm), bl(ic), bl(it),     bl(ip),
     & bl(ir), bl(iz),  bbt,    condamp)
      call dcopy_wr(nq,bl(ix),1,fi2,1)
c
c  transform the coupling
c
      call bxv(nq,nek,b,ibcontr,h1,bl(ic))
      call bdiag(nq,nek,b,ibcontr,bl(idm))
      call congrad(
     & nq,     nek,     b,      ibcontr,    bl(ix),
     & bl(iy), bl(idm), bl(ic), bl(it),     bl(ip),
     & bl(ir), bl(iz),  bbt,    condamp)
      call dcopy_wr(nq,bl(ix),1,hi,1)
c
c  print out...
c
      write(nlist,*) 'zeta1=',zeta1,'zeta2=',zeta2
      call threecol (nlist,nq,qq,fi1,fi2,hi,
     &   'internal coordinates and forces (fi1,fi2,hi)')
      call threecol (icond,nq,qq,fi1,fi2,hi,
     &   'internal coordinates and forces (fi1,fi2,hi)')
c
c now calculate the quantities required in the optimization procedure
c
c   fi = fi1 + zeta1*(fi1-fi2) + zeta2*hi
c
      call dcopy_wr(nq,fi1,1,fi,1)
      call daxpy_wr(nq,zeta1,fi1,1,fi,1)
      call daxpy_wr(nq,onem*zeta1,fi2,1,fi,1)
      call daxpy_wr(nq,zeta2,hi,1,fi,1)
c
c   fi1(nq+1)=-delta E,  fi1(nq+2)=0
c
      fi(nq+1)=-de
c&test      fi(nq+1)=de
      fi(nq+2)=0
c
c   assign lagrange multipliers to coordinate vector
c
      qq(nq+1)=zeta1
      qq(nq+2)=zeta2
c
c  print out...
c
      call onecol (nlist,nq+2,qq,fi,'internal coordinates and forces ')
      call onecol (icond,nq+2,qq,fi,'internal coordinates and forces ')
c
c calculate some statistics
c
      call absmax(nq,fi,mxf,fmax1)
      fmax1=sign(fmax1,fi(mxf))
      write(nlist,400) mxf,fmax1
      write(icond,400) mxf,fmax1
 400  format(' maximum force on coord. ',i4, ' = ',f12.7)
c      fmax1=ddot_wr(nq,fi2,1,fi2,1)
c      write(nlist,401) fmax1,de
c      write(icond,401) fmax1,de
c 401  format(' norm of df vector: ',f12.7,' deltaE:',f12.7)
c
      call retmem(8)
      end
      subroutine intforc2(
     &  nq,nek,b,ibcontr,fc1,fc2,fi1,fi2,fi,de,
     &  qq,mxf,fmax1)
c
       implicit none
c    nq (input) number of coordinates
c    nek (input) 3 times the number of atoms (# of cartesians)
c    b(199,nq) (input) b matrix stored compressed
c    ibcontr(99,nq) coding of b
c    lq,mq dimensions of b (in the main program)
c    fc1(1..nek) (input) cartesian forces state 1
c    fc2(1..nek) (input) cartesian forces state 2
c    fi1(1..nq) (output) internal forces state 1
c    fi2(1..nq) (output) internal forces state 2
c    fi(1..nq) (output)  internal difference
c    de         (inpu)   energy difference
c    nlist,icond (input)  output files
c    qq (input) coordinates
c    fmax1: maximum internal force
c
c   unit numbers
      integer       inp, inp2, nlist, ipun, iarc, icond, itest
      common /tape/ inp, inp2, nlist, ipun, iarc, icond, itest
c
      real*8 condamp
      common /options/condamp
c
      integer lcx
      parameter (lcx=99000)
      real*8 bl
      common bl(lcx)
c
      integer ix,iy,idm,ic,it,ip,ir,iz,i,imd
      integer mxf
      integer nq,nek
      integer ibcontr(99,nq)
c
      real*8 b(199,nq),qq(nq+2),fmax1
      real*8 fc1(nek),fc2(nek),fi1(nq),fi2(nq),fi(nq),de
c
      real*8 bbt
      external bbt
c
      real*8 onem
      parameter (onem=-1.d0)
c
c-------------------------------------------------------
c
      call getmem(nq,ix)
      call getmem(nq,iy)
      call getmem(nq,idm)
      call getmem(nq,ic)
      call getmem(nek,it)
      call getmem(nq,ip)
      call getmem(nq,ir)
      call getmem(nq,iz)
c
c  transform first the force of state 1
c
      call bxv(nq,nek,b,ibcontr,fc1,bl(ic))
      call bdiag(nq,nek,b,ibcontr,bl(idm))
      call congrad(
     & nq,     nek,     b,      ibcontr,    bl(ix),
     & bl(iy), bl(idm), bl(ic), bl(it),     bl(ip),
     & bl(ir), bl(iz),  bbt,    condamp)
      call dcopy_wr(nq,bl(ix),1,fi1,1)
c
c  transform the force of state 2
c
      call bxv(nq,nek,b,ibcontr,fc2,bl(ic))
      call bdiag(nq,nek,b,ibcontr,bl(idm))
      call congrad(
     & nq,     nek,     b,      ibcontr,    bl(ix),
     & bl(iy), bl(idm), bl(ic), bl(it),     bl(ip),
     & bl(ir), bl(iz),  bbt,    condamp)
      call dcopy_wr(nq,bl(ix),1,fi2,1)
c
c now calculate the quantities required in the optimization procedure
c
c   fi = 2* de * (fi1 + -fi2)
c
      call dcopy_wr(nq,fi1,1,fi,1)
      call daxpy_wr(nq,onem,fi2,1,fi,1)
      call dscal_wr(nq,2.d0*de,fi,1)
c
c  print out...
c
      call threecol (nlist,nq,qq,fi1,fi2,fi,
     &   'internal coordinates and forces (fi1,fi2,fi))')
      call threecol (icond,nq,qq,fi1,fi2,fi,
     &   'internal coordinates and forces (fi1,fi2,fi))')
c
c calculate some statistics

      call absmax(nq+2,fi,mxf,fmax1)
      fmax1=sign(fmax1,fi(mxf))
      write(nlist,400) mxf,fmax1
      write(icond,400) mxf,fmax1
 400  format(' maximum force on coord. ',i4, ' = ',f12.7)
 310  continue
      call retmem(8)
      end
      subroutine intforc(
     & nq,   nek, b,  ibcontr,
     & f,    fi,  qq, mxf,
     & fmax1)
c
       implicit none
c    nq (input) number of coordinates
c    nek (input) 3 times the number of atoms (# of cartesiians)
c    b(199,nq) (input) b matrix stored compressed
c    ibcontr(99,nq) coding of b
c    lq,mq dimensions of b (in the main program)
c    f(1..nek) (input) cartesian forces
c    fi(1..nq) (output) descartes forces
c    nlist,icond (input)  output files
c    qq (input) coordinates
c    fmax1: maximum internal force
c
c     # unit numbers
      integer       inp, inp2, nlist, ipun, iarc, icond, itest
      common /tape/ inp, inp2, nlist, ipun, iarc, icond, itest
c
      real*8 condamp
      common /options/condamp
c
      integer    lcx
      parameter (lcx=99000)
      real*8    bl
      common // bl(lcx)
c
      integer ix,iy,idm,ic,it,ip,ir,iz
      integer mxf
      integer nq,nek
      integer ibcontr(99,nq)
c
      real*8 b(199,nq),f(nek),fi(nq),qq(nq),fmax1
c
      external bbt
c
c-------------------------------------------------------
c
      call getmem(nq,ix)
      call getmem(nq,iy)
      call getmem(nq,idm)
      call getmem(nq,ic)
      call getmem(nek,it)
      call getmem(nq,ip)
      call getmem(nq,ir)
      call getmem(nq,iz)
      call bxv(nq,nek,b,ibcontr,f,bl(ic))
      call bdiag(nq,nek,b,ibcontr,bl(idm))
      call congrad(
     & nq,     nek,     b,      ibcontr,    bl(ix),
     & bl(iy), bl(idm), bl(ic), bl(it),     bl(ip),
     & bl(ir), bl(iz),  bbt,    condamp)
      call dcopy_wr(nq,bl(ix),1,fi,1)
ctm   call twocol (nlist,nq,qq,fi,'internal coordinates and forces')
      call onecol (nlist,nq,qq,fi,'internal coordinates and forces')
      call onecol (icond,nq,qq,fi,'internal coordinates and forces')
      call absmax(nq,fi,mxf,fmax1)
      fmax1=sign(fmax1,fi(mxf))
      write(nlist,400) mxf,fmax1
      write(icond,400) mxf,fmax1
 400  format(' maximum force on coord. ',i4, ' = ',f12.7)
      call retmem(8)
      end
      subroutine machbnew(
     & na,     xa,     nq,      qonly,
     & shftpt, ibcode, ibcontr, bmat,
     & qq)
c
c     parameters: input
c     nek=3*na, na=number of nuclei
c     xa(3,*): nuclear coordinates (in Angstrom)
c     nq=number of internal coordinates
c     qonly: if .true., calculate only the coordinate values
c     shftpt: a constant which determines at which point does
c       a torsional coordinate change by 2*pi
c     ibcode(6,nprim): the encoding of primitive internal coordinates
c     ibcontr(99,nq): contains the contraction pattern, the
c     total number of atoms and the atoms participating in the
c     coordinate (max. 18)
c                 output
c     bmat(199,nq): contains the B matrix elements (for at most 18 atoms=
c         54 Cartesians). Note that this is in a way the transpose of
c         B since the SECOND subscript is the internal coordinate
c     qq(nq): the values of the internal coordinates
c
      IMPLICIT REAL*8 (A-H,O-Z)
c
c     # dummy:
      integer na, nq
      integer ibcode(6,*), ibcontr(99,nq)
      logical qonly
      real*8 shftpt
      real*8 xa(3,*), bmat(199,nq), qq(nq)
c
c     # unit numbers
      integer       inp, inp2, nlist, ipun, iarc, icond, itest
      common /tape/ inp, inp2, nlist, ipun, iarc, icond, itest
c
c     # constants
      real*8          zero,  half,  one,   two, three,  four,  five,
     & ten,   ten6,  tenm8, pi,  acc
      common /number/ zero,  half,  one,   two, three,  four,  five,
     & ten,   ten6,  tenm8, pi,  acc
c
c     # local:
      integer i, iatoms, ikk, imm, ipr, iprim1,
     & iprim2, itype, j, j3, ka, kb, kc, kd, kk, l, m
      real*8 arc1, c, cfi1, cfi2, cfi3, co, co2, cteta, cx, den,
     & r1, r2, r3, rm1, rm2, rm6, rm8, s, s2, sfi1, si,
     & si2, si3, st2, st3, steta
c
      integer IA(4)
      real*8 U(3), V(3), W(3), Z(3), X(3), UU(3), VV(3),
     & WW(3), ZZ(3), UV(12)
c
      EQUIVALENCE (KA,IA(1)), (KB,IA(2)), (KC,IA(3)), (KD,IA(4))
      EQUIVALENCE (UV(1),UU(1)), (UV(4),VV(1)), (UV(7),WW(1))
      EQUIVALENCE (UV(10),ZZ(1))
c
      real*8    thous,     six
      parameter(thous=1d3, six=6d0)
c
      real*8   ddot_wr, darcos
      external ddot_wr, darcos
c
c------------------------------------------------------------------
c
      call wzero(nq,qq,1)
      do 1000 i=1,nq
         if(i.eq.1) then
            iprim1=1
         else
            iprim1=ibcontr(1,i-1)+1
         end if
         iprim2=ibcontr(1,i)
         iatoms=ibcontr(2,i)
         if(.not.qonly) then
            call wzero(3*iatoms,bmat(1,i),1)
         end if
         do 900  ipr=iprim1,iprim2
            itype=ibcode(1,ipr)
            c=ibcode(2,ipr)*1.0d-8
            ka=ibcode(3,ipr)
            kb=ibcode(4,ipr)
            kc=ibcode(5,ipr)
            kd=ibcode(6,ipr)
            GO TO (190,200,205,210,230,260,280,300), itype
C
C           # STRETCH
C
190         CALL VEKTOR1(UU,R1,KA,KB,XA)
            VV(1)=-UU(1)
            VV(2)=-UU(2)
            VV(3)=-UU(3)
            QQ(I)=QQ(I)+R1*C
            GO TO 320
C
C           # INVERSE
C
200         CALL VEKTOR1(UU,R1,KA,KB,XA)
            RM1=ONE/R1
            RM2=RM1**2
            UU(1)=-RM2*UU(1)
            UU(2)=-RM2*UU(2)
            UU(3)=-RM2*UU(3)
            VV(1)=-UU(1)
            VV(2)=-UU(2)
            VV(3)=-UU(3)
            IA(3)=0
            IA(4)=0
            QQ(I)=QQ(I)+RM1*C
            GO TO 320
c
C           # inverse sixth power multiplied by 100
c
205         CONTINUE
            UU(1)=XA(1,KA)-XA(1,KB)
            UU(2)=XA(2,KA)-XA(2,KB)
            UU(3)=XA(3,KA)-XA(3,KB)
            RM2=ONE/(UU(1)**2+UU(2)**2+UU(3)**2)
            RM6 = RM2**3 * thous
            RM8 = -RM6 * RM2 * six
            UU(1)=UU(1)*RM8
            UU(2)=UU(2)*RM8
            UU(3)=UU(3)*RM8
            VV(1)=-UU(1)
            VV(2)=-UU(2)
            VV(3)=-UU(3)
            IA(3)=0
            IA(4)=0
            QQ(I)=QQ(I)+RM6*C
            GO TO 320
c
C           # BENDING
C
210         CALL VEKTOR1(U,R1,KA,KC,XA)
            CALL VEKTOR1(V,R2,KB,KC,XA)
            CO=ddot_wr(3,U,1,V,1)
            SI=S2(CO)
            DO 220 L=1,3
               UU(L)=(CO*U(L)-V(L))/(SI*R1)
               VV(L)=(CO*V(L)-U(L))/(SI*R2)
               WW(L)=-UU(L)-VV(L)
220         CONTINUE
            IA(4)=0
            QQ(I)=QQ(I)+C*DARCOS(CO)
            GO TO 320
C
C           # OUT OF PLANE
C
230         CALL VEKTOR1(U,R1,KA,KD,XA)
            CALL VEKTOR1(V,R2,KB,KD,XA)
            CALL VEKTOR1(W,R3,KC,KD,XA)
            CALL NORMAL (V,W,Z)
            STETA=ddot_wr(3,U,1,Z,1)
            CTETA=S2(STETA)
            CFI1=ddot_wr(3,V,1,W,1)
            SFI1=S2(CFI1)
            CFI2=ddot_wr(3,W,1,U,1)
            CFI3=ddot_wr(3,V,1,U,1)
            DEN=CTETA*SFI1**2
            ST2=(CFI1*CFI2-CFI3)/(R2*DEN)
            ST3=(CFI1*CFI3-CFI2)/(R3*DEN)
            DO 240 L=1,3
               VV(L)=Z(L)*ST2
               WW(L)=Z(L)*ST3
240         CONTINUE
            CALL NORMAL (Z,U,X)
            CALL NORMAL (U,X,Z)
            DO 250 L=1,3
               UU(L)=Z(L)/R1
               ZZ(L)=-UU(L)-VV(L)-WW(L)
250         CONTINUE
            CX=-C
            IF (STETA.LT.0.0) CX=C
            QQ(I)=QQ(I)-CX*DARCOS(CTETA)
            GO TO 320
C
C           # TORSION
C
260         CALL VEKTOR1(U,R1,KA,KB,XA)
            CALL VEKTOR1(V,R2,KC,KB,XA)
            CALL VEKTOR1(W,R3,KC,KD,XA)
            CALL NORMAL (U,V,Z)
            CALL NORMAL (W,V,X)
            CO=ddot_wr(3,U,1,V,1)
            CO2=ddot_wr(3,V,1,W,1)
            SI=S2(CO)
            SI2=S2(CO2)
            DO 270 L=1,3
               UU(L)=Z(L)/(R1*SI)
               ZZ(L)=X(L)/(R3*SI2)
               VV(L)=(R1 * CO / R2 - one) *
     &          UU(L) - R3 * CO2 / R2 * ZZ(L)
               WW(L)=-UU(L)-VV(L)-ZZ(L)
270         CONTINUE
            CO=ddot_wr(3,Z,1,X,1)
            U(1)=Z(2)*X(3)-Z(3)*X(2)
            U(2)=Z(3)*X(1)-Z(1)*X(3)
            U(3)=Z(1)*X(2)-Z(2)*X(1)
            SI3=SQRT(U(1)**2+U(2)**2+U(3)**2)
            CO2=ddot_wr(3,U,1,V,1)
            S=ARC1(-CO,SI3)
            IF (CO2 .LT. zero) S = -S
            IF (S .GT. SHFTPT) S = S - two * pi
            QQ(I)=QQ(I)-C*S
C
C           # REMEMBER THAT THE RANGE OF THIS COORDINATE
c           # IS -PI/2 TO 3*PI/2 IN ORDER TO SHIFT THE
c           # DISCONTINUITY OFF THE PLANAR POSITION
C
            GO TO 320
C
C           # LINEAR COPLANAR BENDING
C
280         CALL VEKTOR1(U,R1,KA,KC,XA)
            CALL VEKTOR1(V,R2,KD,KC,XA)
            CALL VEKTOR1(X,R2,KB,KC,XA)
            CO=ddot_wr(3,V,1,U,1)
            CO2=ddot_wr(3,X,1,V,1)
            QQ(I)=QQ(I)+C*(pi-DARCOS(CO)-DARCOS(CO2))
            CALL NORMAL (V,U,W)
            CALL NORMAL (U,W,Z)
            CALL NORMAL (X,V,W)
            CALL NORMAL (W,X,U)
C
C           # COORDINATE POSITIVE IF ATOM A MOVES TOWARDS ATOM D
C
            DO 290 L=1,3
               UU(L)=Z(L)/R1
               VV(L)=U(L)/R2
               WW(L)=-UU(L)-VV(L)
290         CONTINUE
            IA(4)=0
            GO TO 320
C
C           # LINEAR PERPENDICULAR BENDING
C
300         CALL VEKTOR1(U,R1,KA,KC,XA)
            CALL VEKTOR1(V,R2,KD,KC,XA)
            CALL VEKTOR1(Z,R2,KB,KC,XA)
            CALL NORMAL (V,U,W)
            CALL NORMAL (Z,V,X)
            DO 310 L=1,3
               UU(L)=W(L)/R1
               VV(L)=X(L)/R2
               WW(L)=-UU(L)-VV(L)
310         CONTINUE
            IA(4)=0
            CO=ddot_wr(3,U,1,W,1)
            CO2=ddot_wr(3,Z,1,W,1)
            QQ(I) = QQ(I) + C * (pi - DARCOS(CO) - DARCOS(CO2))
320         IF (QONLY) GO TO 900
            DO 340 J=1,4
               M=IA(J)
               IF (M.LE.0) GO TO 340
               iatoms=ibcontr(2,i)
               do 330 kk=1,iatoms
c                 # search the atoms participating in the coordinate.
c                 # Is one of them (ikk) the same as the atom
                  ikk=ibcontr(kk+2,i)
                  if(ikk.eq.m) then
                     imm=kk
                     go to 335
                  end if
330            continue
               write(nlist,*) 'this should not happen, ' //
     &          'i,ibcontr,ia(1..4),j=',
     1          i,(ibcontr(kk,i),kk=1,iatoms+2),ia,j
335            continue
               j3=j*3
*@ifdef debug
*      write(*,*) 'bmat(imm*3..,',i,')=',bmat(imm*3-2,i),
*     .   bmat(imm*3-1,i),bmat(imm*3,i)
*      write(*,*) 'uv(*)=',uv(j3-2),uv(j3-1),uv(j3), 'c=',c,' j3=',j3
*@endif
               bmat(imm*3-2,i)=bmat(imm*3-2,i)+uv(j3-2)*c
               bmat(imm*3-1,i)=bmat(imm*3-1,i)+uv(j3-1)*c
               bmat(imm*3,i)=bmat(imm*3,i)+uv(j3)*c
340         CONTINUE
900      continue
1000  continue
      END
      subroutine nom (u)
       implicit none 
       integer i
      real*8 u(3),x
c
      real*8    one
      parameter(one=1d0)
c
      real*8   ddot_wr
      external ddot_wr
c
      x = one / sqrt( ddot_wr(3,u,1,u,1) )
      do 10 i = 1, 3
         u(i) = u(i) * x
   10 continue

      return
      end
      SUBROUTINE NORMAL (U,V,W)
      IMPLICIT none
       real*8 U(3), V(3), W(3)
C
C     99999...  W WIRD EIN SENKRECHT AUF DIE EBENE(U,V) STEHENDER EINHEI
C      TOR
C
      W(1)=U(2)*V(3)-U(3)*V(2)
      W(2)=U(3)*V(1)-U(1)*V(3)
      W(3)=U(1)*V(2)-U(2)*V(1)
      CALL NOM (W)
C
      RETURN
      END
      SUBROUTINE ONECOL(IFILE,N,Q,F,TITLE)
c
      IMPLICIT none 
c
C     THIS SUBROUTINE PRINTS INTERNAL COORDINATES AND FORCES, OR
C     SIMILAR QUANTITIES, IN ONE COLUMNS
C     PARAMETERS:
C     IFILE: OUTPUT FILE
C     N:(INPUT) NUMBER OF COORDINATES
C     Q(1:N),F(1:N) (INPUT) COORDINATES & FORCES
C     TITLE (CHARACTER STRING), (INPUT) TITLE
c
      integer i,n,ifile
      real*8 Q(*),F(*)
      CHARACTER*(*) TITLE
c
      WRITE(IFILE,100) TITLE
 100  FORMAT(/1X,A45,/)
      DO 200 I=1,N
        WRITE(IFILE,300) I,Q(I),F(I)
 200  CONTINUE
 300  FORMAT((1X,I3,2X,F12.7,1X,F12.7))
c
      return
      END
      subroutine osinv (a, n, d, tol, l, m)
c
c     parameters:  a - input matrix , destroyed in computation and repla
c                      by resultant inverse (must be a general matrix)
c                  n - order of matrix a
c                  d - resultant determinant
c            l and m - work vectors of length n
c                tol - if pivot element is less than this parameter the
c                      matrix is taken for singular (usually = 1.0e-8)
c     a determinant of zero indicates that the matrix is singular
c
       implicit none
c     # dummy:
      integer n
      integer m(*), l(*)
      real*8 d, tol
      real*8 a(*)
c
c     # local:
      integer i, ij, ik, iz, j, ji, jk, jp, jq, jr, k, ki, kj, kk, nk
      real*8 biga, holo
c
      real*8    zero,     one
      parameter(zero=0d0, one=1d0)
c
      d=one
      nk=-n
      do 180 k=1,n
         nk=nk+n
         l(k)=k
         m(k)=k
         kk=nk+k
         biga=a(kk)
         do 21 j=k,n
            iz=n*(j-1)
            do 20 i=k,n
               ij=iz+i
               if (abs(biga) - abs(a(ij)) .lt. zero) then
                  biga = a(ij)
                  l(k) = i
                  m(k) = j
               endif
20          continue
21       continue
         j=l(k)
         if (j .gt. k) then
            ki=k-n
            do 40 i=1,n
               ki=ki+n
               holo=-a(ki)
               ji=ki-k+j
               a(ki)=a(ji)
               a(ji)=holo
40          continue
         endif
         i=m(k)
         if (i .gt. k) then
            jp=n*(i-1)
            do 70 j=1,n
               jk=nk+j
               ji=jp+j
               holo=-a(jk)
               a(jk)=a(ji)
               a(ji)=holo
70          continue
         endif
         if (abs(biga) .lt. tol) then
            d = zero
            return
         endif
         do 120 i=1,n
            if (i .ne. k) then
               ik=nk+i
               a(ik)=a(ik)/(-biga)
            endif
120      continue
         do 151 i=1,n
            ik=nk+i
            ij=i-n
            do 150 j=1,n
               ij=ij+n
               if (i .ne. k) then
                  if (j .ne. k) then
                     kj=ij-i+k
                     a(ij)=a(ik)*a(kj)+a(ij)
                  endif
               endif
150         continue
151      continue
         kj=k-n
         do 170 j=1,n
            kj=kj+n
            if (j .ne. k) a(kj) = a(kj) / biga
170      continue
         d=d*biga
         a(kk) = one / biga
180   continue
c     # do k = n, 1, -1    ??????
      k=n
190   k=k-1
      if (k .gt. 0) then
         i=l(k)
         if (i .gt. k) then
            jq=n*(k-1)
            jr=n*(i-1)
            do 220 j=1,n
               jk=jq+j
               holo=a(jk)
               ji=jr+j
               a(jk)=-a(ji)
               a(ji)=holo
220         continue
         endif
         j=m(k)
         if (j .gt. k) then
            ki=k-n
            do 250 i=1,n
               ki=ki+n
               holo=a(ki)
               ji=ki+j-k
               a(ki)=-a(ji)
               a(ji)=holo
250         continue
         endif
         go to 190
      endif
      return
      end
      subroutine precon(x,dm1,y,nq)
c
c     # calculate y=dm1*x
c
       implicit none
      integer nq,i
      real*8 x(nq),dm1(nq),y(nq)
c
      do i=1,nq
        y(i)=dm1(i)*x(i)
      end do
      return
      end
      subroutine printbmat(nq,nek,b,ibcontr)
c
       implicit none
c  parameters: INPUT
c              nq=number of internal coordinates
c              nek=3*na, number of Cartesians
c              b(199,nq): contains the non-zero elements of B
c              ibcontr(99,nq): coding info for B
c
c   unit numbers
      integer       inp, inp2, nlist, ipun, iarc, icond, itest
      common /tape/ inp, inp2, nlist, ipun, iarc, icond, itest
c
      integer i,iatom,iat3,icount
      integer nq,natom,nek
      integer k3,k
      integer ibcontr(99,nq)
c
      real*8 b(199,nq),bmat(99000)
c
c-----------------------------------------------------------
c
      icount=0
      do i=1,nq
         natom=ibcontr(2,i)
         k3=0
         do k=1,natom
            k3=k3+3
            iatom=ibcontr(k+2,i)
            iat3=iatom*3
            bmat(iat3-2+icount)=b(k3-2,i)
            bmat(iat3-1+icount)=b(k3-1,i)
            bmat(iat3+icount)=b(k3,i)
         end do
         icount = icount + nek
      end do
c
      write(nlist,*)
      call prblkt('B matrix',bmat,nek,nek,nq,'cartc','intc',
     & 1,nlist)
      write(nlist,*)
c
      return
      end
      subroutine readall(check,iconint,lcoup)
c
       implicit none
c   this subroutine reads in inputs, set up defaults
c---------------------------------------------------
c
c     ##  parameter & common block section
c
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      character*5 check
      integer iconint
      logical lcoup
c     # ma is the max. number of atoms
      integer    ma
      parameter (ma=200)
c     # lq is the max. number of cartesisans
      integer    lq
      parameter (lq=3*ma)
c     # mq is the max. number of internal coordinates,
      integer    mq
      parameter (mq=lq-6+2)
c
c     # common related to input data
      character*3 symb
      real*8 xa,etot,f,mass
      integer ia,na
      common /geom/ xa(3,ma),  etot,     f(lq),  mass(ma),  ia(ma),
     & na
      common /cgeom/ symb(ma)
c
c     # unit numbers
      integer       inp,inp2,nlist,ipun,iarc,icond,itest
      common /tape/ inp,inp2,nlist,ipun,iarc,icond,itest
c
      integer        ngeom,   nforc,   nintc,  nfmat,   nform,
     & nener,   inpf3,   nmess,  nhess,   nhessinv,
     & ncuriter
      common /tape2/ ngeom,   nforc,   nintc,  nfmat,   nform,
     & nener,   inpf3,   nmess,  nhess,   nhessinv,
     & ncuriter
c
c     # file names
      character*10     gdiisin,  gdiisls,  gdiissm,  default,
     & geomfl,   forcfl,   intcfl,   fmatfl,
     & formfl,   convfl,   gdiisfl,  gdiismg,
     & hessfl,   hessinvfl,curiterfl,zetafile,  convfl2
      common /flnames/ gdiisin,  gdiisls,  gdiissm,  default,
     & geomfl,   forcfl,   intcfl,   fmatfl,
     & formfl,   convfl,   gdiisfl,  gdiismg,
     & hessfl,   hessinvfl,curiterfl,zetafile,  convfl2
c
c     # styles
      character*10 geomst,forcst
c     # formats
      character*30 geomfm,forcfm
      common /style/ geomst, forcst, geomfm, forcfm
c
      integer           lonl, lalw
      common/transform/ lonl, lalw
c
c     #  values determining the convergence
      real*8              cmax,fmax,crms,frms
      common/convergence/ cmax,fmax,crms,frms
c
c     # these are the parameters for the global geometry optimization
      logical writ,lgdi,lfdi,lmur
      integer itopti,mxopti,iangs,igeom,ifmat,isad,nfix,
     & ifix,mgdi,ipu
      real*8  thract, shftpt,chmax,coormx
      common /opti/itopti,mxopti,chmax,thract,shftpt,coormx,iangs,
     + igeom,ifmat,isad,nfix,ifix(mq),mgdi,lgdi,lfdi,lmur,writ,ipu
c
      real*8           condamp
      common /options/ condamp
c
      integer nopt
      parameter (nopt=37)
c
c     ##  integer section
c
      integer i
      integer nc, nunit
c
c     ##  real*8 section
c
c     ##  character section
c
      character*4 wk(nopt),wo
      character*10 blk10
      character*80 wo80
      character*80 helpstring
      character*30 wo1(3),blk30
c
c     ##  logical section
c
      logical yesno
c
c     ##  data section
c
      data blk30/' '/
      data wk/'ener','geom','forc','intc','fmfl',
     & 'form','prin','fixc','fint','auto',
     & 'sadd','shft','maxi','opti','maxc',
     & 'gdii','fdia','bfgs','punc','angs',
     & 'nuse','mess','cond','lonl','lalw',
     & 'cmax','crms','fmax','frms','for1',
     & 'for2','coup','ene1','ene2','zeta',
     & 'coni','avoi'/
c
c------------------------------------------------------------
c
c     # set defaults first
c
c     # no current geom
      igeom=0
c     # no fmat file
      ifmat=0
c     # no saddle point search
      isad=0
c     # iteration count
      itopti=1
c     # punch
      ipu=0
c     # coordinates in a.u.
      iangs=0
c     # max. number of iteration
      mxopti=20
c     # no fixed coordinate
      nfix=0
c     # diis starts at iteration mgdi
      mgdi=2
c     # no diis
      lgdi=.false.
c     # no diagonal f mx
      lfdi=.false.
c     # no hessian update
      lmur=.false.
c     # no writing of the b matrix
      writ=.false.
c     # set default shift point for machb
      shftpt=1.57079632795d0
c     # set the default maximum coordinate change
      coormx=0.3d0
c     # convergenc criterion
      check='energ'
      chmax=1.d-6
c     # default damping for congrad
      condamp=0.0d0
c     # file option defaults
      geomfl=default
      forcfl=default
      gdiisfl='gdiisfl'
      intcfl='intcfl'
      fmatfl='fmatfl'
      formfl=default
      convfl='geoconvfl'
      convfl2='geoconvfl2'
      zetafile='zetafl'
      gdiismg='gdiismg'
c     # style option defaults
      geomst='texas     '
      forcst='texas     '
c     # format option defaults
      geomfm='(2x,A2,6x,5F10.6)'
      forcfm='(3F10.6)'
c     # do not allow linearized int -> cart trasnformation
      lonl=0
      lalw=0
c     # cmax - maximal coordinate change value
      cmax = 1.0d-03
c     # fmax - maximal force value
      fmax = 1.0d-03
c     # crms - maximal coordinate square norm value
      crms = 2.0d-04
c     # frms - maximal force square norm value
      frms = 2.0d-04
c     # inclusion of coupling in conical intersection search
      lcoup=.false.
c
c     # search for control card
c
100   read (inp,1010,end=400) wo
      do 200 i=1,nopt
         if (wk(i).eq.wo) go to 300
200   continue
      go to 100
1010  format (a4)
c
c     # control card has been found
c
300   continue
      write (nlist,301) wo
301   format (1x,a4,2x,'* option is set * ')
      wo1(1)=blk30
      wo1(2)=blk30
      wo1(3)=blk30
      go to ( 1, 2, 3, 4, 5,
     & 6, 7, 8, 9,10,
     & 11,12,13,14,15,
     & 16,17,18,19,20,
     & 21,22,23,24,25,
     & 26,27,28,29,30,
     & 31,32,33,34,35,
     & 36,37),i
c
c     # energy input
c
500   format(a4,1x,10a,30a)
510   format(a4,i4,a10)
520   format(a4,6x,f10.6)
540   format(a4,i4)
c
1     continue
      backspace inp
      read(inp,500) wo,wo1(1)
      if(wo1(1).ne.blk30) convfl=wo1(1)
      write(nlist,*)'   actual value: ener  = ',convfl
      goto 100
c
c     # geometry input
2     continue
      igeom=1
      backspace inp
      read(inp,'(a80)') helpstring
      read(helpstring,'(a4,i4,a10)') wo,na,blk10
c     write(*,*) helpstring
c     write(*,*) 'wo,na,blk10=',wo,na,blk10
      wo1(1)=blk10
      read(helpstring,'(18x,a30)') wo1(2)
      read(helpstring,'(48x,a30)') wo1(3)
      if(wo1(1).ne.blk30) geomfl=wo1(1)
      if(wo1(2).ne.blk30) geomfm=wo1(2)
      if(wo1(3).ne.blk30) geomst=wo1(3)
      if (na.eq.0) then
         write(nlist,*)'   No. of atoms = default'
      else
         write(nlist,*)'   No. of atoms =',na
      endif
      write(nlist,*)'   actual value: geomfl= ',geomfl
      write(nlist,*)'   actual value: geomfm= ',geomfm
c     # now read geometry
      call readgeom
      goto 100
c
c     # forc file options
c
3     continue
      backspace inp
      read(inp,'(a80)') helpstring
      read(helpstring,'(a4,1x,a10)') wo,blk10
      wo1(1)=blk10
      read(helpstring,'(15x,a30)') wo1(2)
      read(helpstring,'(45x,a30)') wo1(3)
      if(wo1(1).ne.blk30) forcfl=wo1(1)
      if(wo1(2).ne.blk30) forcfm=wo1(2)
      if(wo1(3).ne.blk30) forcst=wo1(3)
      write(nlist,*)'   actual value: forcfl= ',forcfl
      write(nlist,*)'   actual value: forcfm= ',forcfm
c     # now read force
      call readforc(0)
      goto 100
c
c     # intc option
4     continue
      backspace inp
      read(inp,510) wo,nc,blk10
      wo1(1)=blk10
      if(nc.gt.0) then
         if(wo1(1).ne.blk30.and.wo1(1).ne.gdiisin) intcfl=wo1(1)
         inquire(file=intcfl,opened=yesno,number=nunit)
         if(.not.yesno) then
            open(file=intcfl,unit=nintc,form='formatted',
     &       status='unknown')
         else
            nintc=nunit
         endif
         do 41 i=1,nc
            read(inp,42) wo80
            write(nintc,42) wo80
42          format(A80)
41       continue
         rewind nintc
      else
         if(wo1(1).ne.blk30) intcfl=wo1(1)
      endif
      write(nlist,*)'   actual value: intcfl= ',intcfl
      goto 100
c
c     # fmfl   option
5     continue
      backspace inp
      read(inp,510) wo,nc,blk10
      wo1(1)=blk10
      if(nc.gt.0) then
         if(wo1(1).ne.blk30.and.wo1(1).ne.gdiisin) intcfl=wo1(1)
         inquire(file=fmatfl,opened=yesno,number=nunit)
         if(.not.yesno) then
            open(file=fmatfl,unit=nfmat,form='formatted',
     &       status='unknown')
         else
            nfmat=nunit
         endif
         do 51 i=1,nc
            read(inp,52) wo80
            write(nfmat,52) wo80
52          format(A80)
51       continue
         rewind nfmat
      else
         if(wo1(1).ne.blk30) fmatfl=wo1(1)
      endif
      ifmat=1
      write(nlist,*)'   actual value: fmatfl= ',fmatfl
      goto 100
c
c     # formfl option
c
6     continue
      call bummer(' Flag: form not supported',0,faterr)
      goto 100
c
c     # print option - print b matrix
c
7     writ=.true.
      go  to 100
c
c     # fixc option   (fix some internal coordinates)
c
8     continue
      backspace inp
      nfix=nfix+1
      read(inp,540) wo,ifix(nfix)
      write(nlist,*)'echo: wo,ifix(nfix)',wo,ifix(nfix)
      write(nlist,*)'   fixed coordinate: fixc =',ifix(nfix)
      go to 100
c
c     # fint option - add internal forces (offset values)
9     continue
      write(nlist,*) 'Option ',wo,'is not yet installed'
C     # MINUS 1  SIGNALS THAT THERE ARE AS MANY INTERNAL FORCES
C     # AS COORDINATES (DEFAULT)
      go to 100
c
c     # auto option
c
10    continue
      call bummer(' Flag: auto not supported',0,faterr)
      goto 100
c
c     # saddle option -saddle points
c
11    isad=1
c     # saddlepoint calculation does not permit hessian update
      if (lmur.eqv..true.) then
         lmur=.false.
         write(nlist,*) 'bfgs  option is switched off because of'
     +    ,' saddlepoint calculation'
      endif
      go to 100
c
c     # shif option (changes the shift point for torsions)
c
12    continue
      backspace inp
      read(inp,500) wo,shftpt
      go to 100
c
c     # maxi option: maximum change permitted in the
c     # coordinates. default is
c
13    continue
      backspace inp
      read(inp,520) wo,coormx
      go to 100
c
c     # opti option: maximum number of iterations
14    continue
      backspace inp
      read(inp,540) wo,mxopti
      write(nlist,*)'   actual value: opti = ',mxopti
      if (mxopti.le.0) call bummer(
     & 'wrong value of opti=',mxopti,faterr)
      goto 100
c
c     # maxc option: max. change in internal coordinates at convergence
15    continue
      write(nlist,*)
     & 'maxc not implemented use: cmax, fmax, crms, frms'
      go to 100
c
c     # gdiis option (geometry diis)
16    continue
      backspace inp
      read(inp,540) wo,mgdi
      lgdi=.true.
      if(mgdi.le.2) mgdi=2
      write(nlist,*)'   actual value: gdiis =',mgdi
      go to 100
c
c     # fdia option: diagonal force constant matrix
c
17    continue
      lfdi=.true.
      go to 100
c
c     # bfgs update, in case of saddlepoint calculation option is
c     # not switched on
c
18    continue
      if (isad.eq.1) then
         lmur=.false.
         write(nlist,*) 'bfgs  option is switched off because of'
     +    ,' saddlepoint calculation'
      else
         lmur=.true.
      endif
      go to 100
c
c     # punch option
c
19    ipu=ipun
      go to 100
c
c     # angs option
c
20    iangs=1
      go to 100
c
c     # nuse option:  iteration count
21    continue
      backspace inp
      read(inp,510) wo,itopti
      write(nlist,*)'   actual value: nuse  =',itopti
      go to 100
c
c     # message option
22    continue
      backspace inp
      read(inp,500) wo,wo1(1)
      if(wo1(1).ne.blk30) gdiismg=wo1(1)
      write(nlist,*)'   actual value: mess  =',gdiismg
      go to 100
c
c     # cond option : value for damping in congrad
c
23    continue
      backspace inp
      read(inp,520) wo,condamp
      write(nlist,*)'condamp=',condamp
      go to 100
c
c     # lonl: linear only int -> cart
24    continue
      backspace inp
      read(inp,540) wo,lonl
      write(nlist,*)'   actual value: lonl =',lonl
      go to 100
c
c     # lalw: allow linear int -> cart
25    continue
      backspace inp
      read(inp,540) wo,lalw
      write(nlist,*)'   actual value: lalw =',lalw
      go to 100
c
c     # cmax - maximal coordinate change value
26    continue
      backspace inp
      read(inp,'(a4,6x,f20.10)') wo,cmax
      write(nlist,'(a25,f20.10)')'   actual value: cmax =',cmax
      go to 100
c
c     # crms - maximal coordinate square norm value
27    continue
      backspace inp
      read(inp,'(a4,6x,f20.10)') wo,crms
      write(nlist,'(a25,f20.10)')'   actual value: crms =',crms
      go to 100
c
c     # fmax - maximal force value
28    continue
      backspace inp
      read(inp,'(a4,6x,f20.10)') wo,fmax
      write(nlist,'(a25,f20.10)')'   actual value: fmax =',fmax
      go to 100
c
c     # frms - maximal force square norm value
29    continue
      backspace inp
      read(inp,'(a4,6x,f20.10)') wo,frms
      write(nlist,'(a25,f20.10)')'   actual value: frms =',frms
      go to 100
c
c     force state 1
c
 30   continue
      backspace inp
      read(inp,'(a80)') helpstring
      read(helpstring,'(a4,1x,a10)') wo,blk10
      wo1(1)=blk10
      read(helpstring,'(15x,a30)') wo1(2)
      read(helpstring,'(45x,a30)') wo1(3)
      if(wo1(1).ne.blk30) forcfl=wo1(1)
      if(wo1(2).ne.blk30) forcfm=wo1(2)
      if(wo1(3).ne.blk30) forcst=wo1(3)
      write(nlist,*)'   actual value: forcfl= ',forcfl
      write(nlist,*)'   actual value: forcfm= ',forcfm
c  now read force
      call readforc(1)
      goto 100
c
c     forc state 2
c
 31   continue
      backspace inp
      read(inp,'(a80)') helpstring
      read(helpstring,'(a4,1x,a10)') wo,blk10
      wo1(1)=blk10
      read(helpstring,'(15x,a30)') wo1(2)
      read(helpstring,'(45x,a30)') wo1(3)
      if(wo1(1).ne.blk30) forcfl=wo1(1)
      if(wo1(2).ne.blk30) forcfm=wo1(2)
      if(wo1(3).ne.blk30) forcst=wo1(3)
      write(nlist,*)'   actual value: forcfl= ',forcfl
      write(nlist,*)'   actual value: forcfm= ',forcfm
c  now read force
      call readforc(2)
      goto 100
c
c     non-adiabatic coupling
c
 32   continue
      backspace inp
      read(inp,'(a80)') helpstring
      read(helpstring,'(a4,1x,a10)') wo,blk10
      wo1(1)=blk10
      read(helpstring,'(15x,a30)') wo1(2)
      read(helpstring,'(45x,a30)') wo1(3)
      if(wo1(1).ne.blk30) forcfl=wo1(1)
      if(wo1(2).ne.blk30) forcfm=wo1(2)
      if(wo1(3).ne.blk30) forcst=wo1(3)
      write(nlist,*)'   actual value: forcfl= ',forcfl
      write(nlist,*)'   actual value: forcfm= ',forcfm
c  now read force
      call readforc(3)
      lcoup=.true.
      goto 100
c
 33   continue
      backspace inp
      read(inp,500) wo,wo1(1)
      if(wo1(1).ne.blk30) convfl=wo1(1)
      write(nlist,*)'   actual value: ener1  = ',convfl
      goto 100
c
 34   continue
      backspace inp
      read(inp,500) wo,wo1(1)
      if(wo1(1).ne.blk30) convfl2=wo1(1)
      write(nlist,*)'   actual value: ener2  = ',convfl2
      goto 100
c
 35   continue
      backspace inp
      read(inp,500) wo,wo1(1)
      if(wo1(1).ne.blk30) zetafile=wo1(1)
      write(nlist,*)'   actual value: zetafile  = ',zetafile
      goto 100
c
c      conical intersection calculation a la Yarkony
c
 36   iconint=1
      go to 100
c
c      avoided crossing optimization
c
 37   iconint=2
      go to 100
c
400   continue
c
c this is out of order due to unfurtante change of the meaning of parame
c     # delete gdiisfl if ngeo.eq.0 and iopti.eq.1
c
c      if(ngeo.eq.0.and.itopti.eq.1) then
c         inquire(file=gdiisfl,exist=yesno)
c         if(yesno) then
c            open(file=gdiisfl,unit=inpf3,status='unknown')
c            close(unit=inpf3,status='delete')
c         endif
c      endif
c
c     ##  no geometry input found => exit
      if (igeom.eq.0) call bummer('missing input geometry',
     & 0,faterr)
c
      return
      end
      subroutine readdiag(f,nq,nfix,ifix,inp,nlist,icond,lmur)
c
       implicit none
c     this routine reads a diagonal force constant matrix
c     parameters:
c     f(nq) - output (force constants, inverted in program)
c     nq - input, number of int. coordinates'
c     nfix  input - number of coordinates to be fixed
c     ifix  input - coordinates to be fixed
c      inp  input - main input file
c     nlist and icond input - output files
c
c   constants
      real*8          zero,  half,  one,   two, three,  four,  five,
     & ten,   ten6,  tenm8, pi,  acc
      common /number/ zero,  half,  one,   two, three,  four,  five,
     & ten,   ten6,  tenm8, pi,  acc
c
      integer nfix, ifix(*), inp, i, icond
      integer j
      integer nq, nlist
      real*8  f(nq)
      logical lmur
c
c-----------------------------------------------------------
c
1180  format (/3x,'Force constants:' )
      read (inp,1170) (f(j),j=1,nq)
1170  format (8f10.7)
c
      write (nlist,1180)
      write (nlist,1220) (f(j),j=1,nq)
1220  format (1x,10f10.5,/,(1x,10f10.5))
      do 340 i=1,nq
         f(i)=one/f(i)
340   continue
c     end of reading the force constants
      return
      end
      subroutine readener
c
       implicit none
c     this routine reads the energy form the convergence file
c
c     input:
c          convfl        file name
c          nlist         unit for the listings
c
c     output:
c          etot         enery
c
c   written by Peter Szalay jun. 95
c
c     # ma is the max. number of atoms
      integer ma
      parameter (ma=200)
c     # lq is the max. number of cartesisans
      integer lq
      parameter (lq=3*ma)
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
c     # common related to input data
      character*3 symb
      real*8 xa,etot,f,mass
      integer ia,na
      common /geom/ xa(3,ma),  etot,     f(lq),  mass(ma),  ia(ma),
     &              na
      common /cgeom/ symb(ma)
c
c     # unit numbers
      integer inp,inp2,nlist,ipun,iarc,icond,itest
      common /tape/ inp,inp2,nlist,ipun,iarc,icond,itest
c
c     # file names
      character*10     gdiisin,  gdiisls,  gdiissm,  default,
     &                 geomfl,   forcfl,   intcfl,   fmatfl,
     &                 formfl,   convfl,   gdiisfl,  gdiismg,
     &                 hessfl,   hessinvfl,curiterfl,zetafile,convfl2
      common /flnames/ gdiisin,  gdiisls,  gdiissm,  default,
     &                 geomfl,   forcfl,   intcfl,   fmatfl,
     &                 formfl,   convfl,   gdiisfl,  gdiismg,
     &                 hessfl,   hessinvfl,curiterfl,zetafile,convfl2
c
      integer        ngeom,   nforc,   nintc,  nfmat,   nform,
     &               nener,   inpf3,   nmess,  nhess,   nhessinv,
     &               ncuriter
      common /tape2/ ngeom,   nforc,   nintc,  nfmat,   nform,
     &               nener,   inpf3,   nmess,  nhess,   nhessinv,
     &               ncuriter
c
      character*80 char2
      character*4 char
c
c------------------------------------------------------------
c
  5   read(nener,'(a80)',end=20)char2
      go to 5
 20   continue
      read(char2,'(a4)')char
      read(char2(5:),*)etot
c
      if ((char.ne.'eci=').and.(char.ne.'emc=')) then
       write(nlist,
     &  '(//,''In the file: '',a10,'' the last value is NOT'')')
     &  convfl
       write(nlist,
     &  '(''the energy value !!!'')')
       write(nlist,*)'Last line read in: ',char
       write(nlist,
     &  '(''ERROR in runsp.perl, contact the programmer'')')
        call bummer
     &  ('inconsistent energy value, see gdiisls for details',
     &  0,faterr)
      endif
c
      write (nlist,10) etot
 10   format (//4x,' Energy of the optimized state:',F20.10)
c
      return
      end
      subroutine readforc(ifif)
c
       implicit none
c     this routine reads the force file
c
c     input:
c          ifif         where to put the force
c                     0: f(*)  standard geom. opt.
c                     1: fc1(*)  state one (conical intersection)
c                     2: fc1(*)  state two (conical intersection)
c                     3: fc1(*)  coupling  (conical intersection)
c          na         number of atoms
c          forcfl      file name
c          forcst      what form the file containes geometry
c                          currently known:  TEXAS
c          forcfm      format of the geometry input
c
c     output:
c          f(*)         cartesian forces
c
c   written by Peter Szalay july 2001
c
      integer ifif
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
c   ma is the max. number of atoms
      integer ma
      parameter (ma=200)
c   lq is the max. number of cartesisans
      integer lq
      parameter (lq=3*ma)
c   md is the max. number of gdiis geometry steps (or bfgs)
      integer md
      parameter (md=100)
c
c  common related to input data
      character*3 symb
      real*8 xa,etot,f,mass
      integer ia,na
      common /geom/ xa(3,ma),  etot,     f(lq),  mass(ma),  ia(ma),
     &              na
      common /cgeom/ symb(ma)
c&new lines
c
c  common related to conical intersection calculations
      real*8 fc1,fc2,h1,fi1,fi2,hi,zeta1,zeta2,de,fd1,fd2,fdh
      integer*4 iconint
      common /conint/ fc1(lq),fc2(lq),h1(lq),
     &     fi1(lq),fi2(lq),hi(lq),zeta1,zeta2,de,
     &     fd1(lq,md),fd2(lq,md),fdh(lq,md),iconint
c&new lines end
c
c   file names
      character*10     gdiisin,  gdiisls,  gdiissm,  default,
     &                 geomfl,   forcfl,   intcfl,   fmatfl,
     &                 formfl,   convfl,   gdiisfl,  gdiismg,
     &                 hessfl,   hessinvfl,curiterfl,zetafile,convfl2
      common /flnames/ gdiisin,  gdiisls,  gdiissm,  default,
     &                 geomfl,   forcfl,   intcfl,   fmatfl,
     &                 formfl,   convfl,   gdiisfl,  gdiismg,
     &                 hessfl,   hessinvfl,curiterfl,zetafile,convfl2
c
c   styles
      character*10 geomst,forcst
      character*30 geomfm,forcfm
      common /style/ geomst,forcst,geomfm,forcfm
c
c   unit numbers
      integer       inp, inp2, nlist, ipun, iarc, icond, itest
      common /tape/ inp, inp2, nlist, ipun, iarc, icond, itest
c
      integer        ngeom,   nforc,   nintc,  nfmat,   nform,
     &               nener,   inpf3,   nmess,  nhess,   nhessinv,
     &               ncuriter
      common /tape2/ ngeom,   nforc,   nintc,  nfmat,   nform,
     &               nener,   inpf3,   nmess,  nhess,   nhessinv,
     &               ncuriter
c
c  ##  integer section
c
      integer i, ii, icount
      integer nek, nunit
c
c  ## real*8 section
c
      real*8 xfqx
c
      logical yesno
c
c-----------------------------------------------------------------
c
c      if(ifif.ne.0) iconint=iconint+1
      nek=3*na
c
      inquire(file=forcfl,opened=yesno,number=nunit)
      if(.not.yesno) then
         nunit=nforc
         open(unit=nunit,file=forcfl,status='unknown',form='formatted')
      endif
c
      if(ifif.eq.0) then
         write(nlist,10)
      elseif(ifif.eq.1) then
         write(nlist,11)
      elseif(ifif.eq.2) then
         write(nlist,12)
      elseif(ifif.eq.3) then
         write(nlist,13)
      endif
      write(nlist,
     & '(6x,1h#,2x,''Symb'',8x,''fx'',10x,''fy'',10x,''fz'')')
      write(nlist,'(4x,50(1h-))')
      write(icond,10)
      write(icond,
     & '(6x,1h#,2x,''Symb'',8x,''fx'',10x,''fy'',10x,''fz'')')
      write(icond,'(4x,49(1h-))')
 10   format (//,10x,'Cartesian forces',/)
 11   format (//,10x,'Cartesian forces state 1',/)
 12   format (//,10x,'Cartesian forces state 2',/)
 13   format (//,10x,'State Couplings in cartesian coordinates',/)
c
        if(forcst.eq.'texas     '.or.forcst.eq.'TEXAS     ') then
         icount = 0
 100     read (nunit,forcfm,END=200) (f(icount+i),i=1,3)
         icount = icount + 3
         go to 100
 200     continue
           do ii=1,nek
           f(ii)=-f(ii)*8.2387295d0
           enddo ! do ii=1,nek
c
           icount = 0
           do ii=1,na
           write (nlist,101)ii,symb(ii),(f(icount+i),i=1,3)
           write (icond,101)ii,symb(ii),(f(icount+i),i=1,3)
           icount = icount + 3
           enddo ! do ii=1,na
 101     format (5x,i2,3x,a3,2x,3f12.6)
        else
         write(nlist,*) ' error in readforc: style ',forcst,
     +        ' is not known'
         write(nlist,*) ' program terminates'
         call bummer
     &    ('error in readforc: error in style',0,faterr)
        end if
c
      xfqx=f(1)
      do 1100 i=2,nek
         xfqx=xfqx+f(i)
 1100 continue
      if (abs(xfqx).gt.1.0e-5) write (nlist,1101) xfqx
 1101 format (/,1x,26hforces do not vanish, sum=,f15.7,/)
c
c now copy the force to the right place
c
      if(ifif.eq.0) then
         return
      elseif(ifif.eq.1) then
         do ii=1,nek
            fc1(ii)=f(ii)
         enddo                  ! do ii=1,nek
      elseif(ifif.eq.2) then
         do ii=1,nek
            fc2(ii)=f(ii)
         enddo                  ! do ii=1,nek
      elseif(ifif.eq.3) then
         do ii=1,nek
            h1(ii)=f(ii)
         enddo                  ! do ii=1,nek
      endif
c
      return
      end
      subroutine readgeom
c
       implicit none
c     this routine reads the geom geomfl
c
c     input:
c          geomfl       geomfl name
c          geomst       what form the geomfl containes geometry
c                          currently known:  TEXAS
c          geomfm       format of the geometry input
c          iangs        =1   input in angstroms
c                       =0   input in bohr
c          nlist         unit for the listings
c          na           number of atoms (in some cases)
c
c     output:
c          x(*),y(*),z(*)      cartesian coordinates
c          symb(*)             element symbols
c          ia                  atomic number
c          na                  number of atoms (in some cases)
c
c   written by Peter Szalay jun. 95
c
c     ##  parameter & common block section
c
c     # unit numbers
      integer       inp,inp2,nlist,ipun,iarc,icond,itest
      common /tape/ inp,inp2,nlist,ipun,iarc,icond,itest
c
c     # ma is the max. number of atoms
      integer ma
      parameter (ma=200)
c     # lq is the max. number of cartesisans
      integer lq
      parameter (lq=3*ma)
c     # mq is the max. number of internal coordinates,
      integer mq
      parameter (mq=lq-6+2)
c
c     # common related to input data
      character*3 symb
      real*8 xa,etot,f,mass
      integer ia,na
      common /geom/ xa(3,ma),  etot,     f(lq),  mass(ma),  ia(ma),
     & na
      common /cgeom/ symb(ma)
c
c     # file names
      character*10     gdiisin,  gdiisls,  gdiissm,  default,
     & geomfl,   forcfl,   intcfl,   fmatfl,
     & formfl,   convfl,   gdiisfl,  gdiismg,
     & hessfl,   hessinvfl,curiterfl,zetafile,convfl2
      common /flnames/ gdiisin,  gdiisls,  gdiissm,  default,
     & geomfl,   forcfl,   intcfl,   fmatfl,
     & formfl,   convfl,   gdiisfl,  gdiismg,
     & hessfl,   hessinvfl,curiterfl,zetafile,convfl2
c
c     # styles
      character*10 geomst,forcst
c     # formats
      character*30 geomfm,forcfm
      common /style/ geomst, forcst, geomfm, forcfm
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
c     # these are the parameters for the global geometry optimization
      logical writ,lgdi,lfdi,lmur
      integer itopti,mxopti,iangs,igeom,ifmat,isad,nfix,
     & ifix,mgdi,ipu
      real*8  thract, shftpt,chmax,coormx
      common /opti/itopti,mxopti,chmax,thract,shftpt,coormx,iangs,
     + igeom,ifmat,isad,nfix,ifix(mq),mgdi,lgdi,lfdi,lmur,writ,ipu
c
      real*8        ang,debye,cbm,ajoule,evolt,ckalmo,dkel,cmm1,hertz
      common /unit/ ang,debye,cbm,ajoule,evolt,ckalmo,dkel,cmm1,hertz
c
c     # unit numbers
      integer        ngeom,   nforc,   nintc,  nfmat,   nform,
     & nener,   inpf3,   nmess,  nhess,   nhessinv,
     & ncuriter
      common /tape2/ ngeom,   nforc,   nintc,  nfmat,   nform,
     & nener,   inpf3,   nmess,  nhess,   nhessinv,
     & ncuriter
c
c     ##  integer section
c
      integer i, nunit
c
      real*8 xiat
c
      logical yesno
c
      inquire(file=geomfl,opened=yesno,number=nunit)
      if(.not.yesno) then
         nunit=ngeom
         open(unit=nunit,file=geomfl,status='unknown',form='formatted')
      endif
c
      write (nlist,10)
      write(nlist,1000)
1000  format
     & (6x,1h#,2x,'Symb',2x,'Nuc',6x,1hx,11x,1hy,11x,1hz,10x,'mass')
      write(nlist,'(4x,65(1h-))')
      write (icond,10)
      write(icond,1010)
1010  format
     & (6x,1h#,2x,'Symb',2x,'Nuc',6x,1hx,11x,1hy,11x,1hz,10x,'mass')
      write(icond,'(4x,65(1h-))')
10    format
     & (//10x,'Initial Cartesian nuclear coordinates in angstrom'/)
c
      if(geomst.eq.'texas     '.or.geomst.eq.'TEXAS     ') then
         if(na.eq.0) then
            na=1
101         continue
            read(nunit,geomfm,end=103) symb(na),xiat,xa(1,na),xa(2,na),
     &       xa(3,na),mass(na)
            ia(na)=xiat+0.1
            if (iangs.ne.1) then
               xa(1,na)=xa(1,na)/ang
               xa(2,na)=xa(2,na)/ang
               xa(3,na)=xa(3,na)/ang
            endif
            write (nlist,102) na,symb(na),ia(na),xa(1,na),xa(2,na),
     &       xa(3,na),mass(na)
            write (icond,102) na,symb(na),ia(na),xa(1,na),xa(2,na),
     &       xa(3,na),mass(na)
102         format (5x,i2,3x,a3,3x,i2,4f12.7)
            na=na+1
            go to 101
103         continue
            na=na-1
         else
            do 201 i=1,na
               read(nunit,geomfm)symb(i),xiat,xa(1,i),xa(2,i),xa(3,i),
     &          mass(i)
               ia(i)=xiat+0.1
               if (iangs.ne.1) then
                  xa(1,i)=xa(1,i)/ang
                  xa(2,i)=xa(2,i)/ang
                  xa(3,i)=xa(3,i)/ang
               endif
               write (nlist,102) i,symb(i),ia(i),
     &          xa(1,i),xa(2,i),xa(3,i), mass(i)
               write (icond,102) i,symb(i),ia(i),
     &          xa(1,i),xa(2,i),xa(3,i), mass(i)
201         continue
         endif
      else
         write(nlist,*) ' error in readgeom: geomst ',geomst,
     +    ' is not known'
         write(nlist,*) ' program terminates'
         call bummer('error in readgeom: error in geomst',0,faterr)
      end if
c
      if(na.eq.0.or.na.gt.ma) then
         write(nlist,*)'Actual number of atoms',na
         write(nlist,*)'Maximal number of atoms',ma
         call bummer(
     &    ' too many or too few atoms. na=',na,faterr)
      endif
      write(nlist,'(/)')
c
      return
      end
      subroutine relaxd(nq,fi,c,cq,edecr,dismax,icmx,scfac)
c
       implicit none
c     this routine calculates the new internal coordinates
c     and the projected energy lowerings
c     in this version,. it can only use diagonal force constants for
c       relaxation
c     nq (input) number of internal coordinates
c     fi (input) internal forces
c     c (input) inverse force constant matrix
c     cq  (output) change in the internal coordinates
c     edecr (input) decrease of the total energy in this step
c     dismax (output) maximum change in the internal coordinates
c     icmx (output) coordinate of the largest displacement
c     scfac  scale factor for the displacements, normally 1
c
c     # constants
      real*8          zero,  half,  one,   two, three,  four,  five,
     & ten,   ten6,  tenm8, pi,  acc
      common /number/ zero,  half,  one,   two, three,  four,  five,
     & ten,   ten6,  tenm8, pi,  acc
c
      integer i,icmx,ij
      integer j
      integer nq
c
      real*8 c(*),cq(*)
      real*8 dismax
      real*8 edecr
      real*8 fi(*)
      real*8 scfac
c
c-----------------------------------------------------------------
c
      edecr=zero
      do 480 i=1,nq
         cq(i)=zero
         do 485 j=1,nq
            ij=(i-1)*nq+j
            cq(i)=cq(i)+c(ij)*fi(j)
485      continue
         edecr=edecr-cq(i)*fi(i)
480   continue
      edecr=edecr*half
      if(scfac.ne.one) call dscal_wr(nq,scfac,cq,1)
      call absmax(nq,cq,icmx,dismax)
c
      return
      end
      SUBROUTINE RETALL
C     RESETS THE OCCUPIED MEMORY TO NOTHING
       implicit none 
       integer        nreq,maxmem,nadr
      COMMON /MMANAG/NREQ,MAXMEM,NADR(400)
c
      NREQ   = 0
      MAXMEM = 0
      NADR(1)= 0
c
      return
      END
      SUBROUTINE RETMEM(N)
c
C     # REMOVES THE RESERVATION FOR THE LAST OCCUPIED BLOCK
c
       implicit none 
       integer n
c
      integer        nreq,maxmem,nadr
      COMMON /MMANAG/NREQ,MAXMEM,NADR(400)
c
      integer i

      DO 100 I=1,N
         IF (NREQ.GT.0) NREQ=NREQ-1
100   CONTINUE
c
      return
      END
      REAL*8 FUNCTION S2 (X)
c
      IMPLICIT none
c
      real*8 x
c
      real*8    one
      parameter(one=1d0)
c
      S2 = SQRT( one - X**2)
c
      RETURN
      END
      SUBROUTINE THREECOL(IFILE,N,Q,F1,F2,F3,TITLE)
c
      IMPLICIT none
c
C     THIS SUBROUTINE PRINTS INTERNAL COORDINATES AND FORCES, OR
C     SIMILAR QUANTITIES, IN ONE COLUMNS
C     PARAMETERS:
C     IFILE: OUTPUT FILE
C     N:(INPUT) NUMBER OF COORDINATES
C     Q(1:N),F(1:N) (INPUT) COORDINATES & FORCES
C     TITLE (CHARACTER STRING), (INPUT) TITLE
c
      integer i,n,ifile
      real*8 Q(*),F1(*),F2(*),F3(*)
      CHARACTER*(*) TITLE
c
      WRITE(IFILE,100) TITLE
 100  FORMAT(/1X,A45,/)
      DO 200 I=1,N
        WRITE(IFILE,300) I,Q(I),F1(I),F2(I),F3(I)
 200  CONTINUE
 300  FORMAT((1X,I3,2X,F12.7,1X,F12.7,1X,F12.7,1X,F12.7))
c
      return
      END
      SUBROUTINE TWOCOL(IFILE,N,Q,F,TITLE)
c
      IMPLICIT none
c
C     THIS SUBROUTINE PRINTS INTERNAL COORDINATES AND FORCES, OR
C     SIMILAR QUANTITIES, IN TWO COLUMNS
C     PARAMETERS:
C     IFILE: OUTPUT FILE
C     N:(INPUT) NUMBER OF COORDINATES
C     Q(1:N),F(1:N) (INPUT) COORDINATES & FORCES
C     TITLE (CHARACTER STRING), (INPUT) TITLE
c
      integer i,n,ifile,nn,ii
      real*8 Q(*),F(*)
      CHARACTER*(*) TITLE
c
      WRITE(IFILE,100) TITLE
 100  FORMAT(1X,A60,/)
      NN=(N+1)/2
      DO 200 I=1,NN
      II=I+NN
      IF (II.LE.N) THEN
        WRITE (IFILE,300) I,Q(I),F(I),II,Q(II),F(II)
      ELSE
        WRITE(IFILE,300) I,Q(I),F(I)
      END IF
 200  CONTINUE
 300  FORMAT(2(1X,I3,2X,F12.7,1X,F12.7))
c
      return
      END
      SUBROUTINE VEKTOR1(U,R,I,J,XA)
c
      IMPLICIT none
c
      integer i,j
      real*8 U(3),XA(3,*),r,rr
C
C       BILDET DEN NORMIERTEN ENTFERNUNGSVEKTOR VOM KERN J NACH KERN I
C        UND DIE ENTFERNUNG R
C
      real*8    one
      parameter(one=1d0)
c
      U(1)=XA(1,I)-XA(1,J)
      U(2)=XA(2,I)-XA(2,J)
      U(3)=XA(3,I)-XA(3,J)
      R=SQRT(u(1)**2+u(2)**2+u(3)**2)
      rr = one / R
      u(1)=u(1)*rr
      u(2)=u(2)*rr
      u(3)=u(3)*rr
C
      return
      END
      subroutine writgeom(
     & xy,        symb,      ia,      na,    file,
     & style,     format,    iangs,   nlist, icond,
     & exitcode,  mass)
c
       implicit none
c     this routine reads the geom file
c
c     input:
c          file       file name
c          style      what form the file containes geometry
c                      currently known:  TEXAS
c          format     format of the geometry input
c          iangs       =1   input in angstroms
c                      =0   input in bohr
c          nlist       unit for the listings
c          icond      unit for the short listings
c          xy(*,*)    cartesian coordinates
c          symb(*)             element symbols
c          ia                  atomic number
c          na                  number of atoms
c          icond               exit code
c
c   written by Peter Szalay jun. 95
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      real*8        ang,debye,cbm,ajoule,evolt,ckalmo,dkel,cmm1,hertz
      common /unit/ ang,debye,cbm,ajoule,evolt,ckalmo,dkel,cmm1,hertz
c
      integer        ngeom,   nforc,   nintc,  nfmat,   nform,
     & nener,   inpf3,   nmess,  nhess,   nhessinv,
     & ncuriter
      common /tape2/ ngeom,   nforc,   nintc,  nfmat,   nform,
     & nener,   inpf3,   nmess,  nhess,   nhessinv,
     & ncuriter
c
c  ##  integer section
c
      integer ia(*), icond, iangs, i
      integer nunit, nlist, na
c
      real*8 mass(*)
      real*8 xy(3,*), xiat
c
c  ##  character section
c
      character*(*) symb(*),exitcode
      character*(*) format,file
      character*6 style
c
      logical yesno
c
c-------------------------------------------------------
c
      inquire(file=file,opened=yesno,number=nunit)
      if(.not.yesno) then
         nunit=nmess
         open(unit=nunit,file=file,status='unknown',form='formatted')
      endif
      rewind nunit
c
      write(nlist,'(//5x,''New cartesian geometry written on file:'')')
      write(nlist,'(17x,''UNIT='',i3,''  FILENAME= '',a10)') nunit,file
      write(nlist,'(5x,''with exitcode= '',a40)')exitcode
      write(icond,'(//5x,''New cartesian geometry written on file:'')')
      write(icond,'(17x,''UNIT='',i3,''  FILENAME= '',a10)') nunit,file
      write(icond,'(5x,''with exitcode= '',a40)')exitcode
c
      write (nunit,'(a40)') exitcode
      write (nlist,10)
      write (icond,10)
10    format (/20x,'nuclear coordinates in angstrom ',/)
c
      if(style.eq.'texas     '.or.style.eq.'TEXAS     ') then
         do i=1,na
            xiat=ia(i)
            write(nlist,format) symb(i),xiat,xy(1,i),xy(2,i),xy(3,i),
     &       mass(i)
            write(icond,format) symb(i),xiat,xy(1,i),xy(2,i),xy(3,i),
     &       mass(i)
         enddo
c
         write (nlist,20)
         write (icond,20)
         do i=1,na
            if (iangs.ne.1) then
               xy(1,i)=xy(1,i)*ang
               xy(2,i)=xy(2,i)*ang
               xy(3,i)=xy(3,i)*ang
            endif               ! if (iangs.ne.1)
            xiat=ia(i)
            write(nlist,format) symb(i),xiat,xy(1,i),xy(2,i),xy(3,i),
     &       mass(i)
            write(icond,format) symb(i),xiat,xy(1,i),xy(2,i),xy(3,i),
     &       mass(i)
            write(nunit,format) symb(i),xiat,xy(1,i),xy(2,i),xy(3,i),
     &       mass(i)
         enddo
      else
         write(nlist,*) ' error in writgeom: style ',style,
     &    ' is not known'
         write(nlist,*) ' program terminates'
         call bummer('error in writgeom: error in style',0,faterr)
      end if
c
      return
20    format (/20x,'nuclear coordinates in a.u.',/)
      end

