!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
cmcscf9.f
cmcscf part=9 of 10.  miscellaneous mcscf utility routines.
cversion=5.6 last modified: 16-jan-2001
      integer function numpbf(l)
c
c  determine the number of 2-e integrals that can be held in a working
c  precision buffer of length l, where the buffer is assumed to be
c  formatted as:
c
c  buffer format: value-buffer...packed-label-buffer...information-word
c
c  examples:
c   1-label/word:  n=(l-1)/2
c   2-labels/word: n=(2*(l-1))/3
c   4-labels/word: n=(4*(l-1))/5
c   ...
c   m-labels/word: n=(m*(l-1))/(m+1)
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
*@ifdef future
*CC  anyone?
*@else
      numpbf=(2*(l-1))/3
*@endif
c
      return
      end
      subroutine rdbks(bukpt,h2,buk,ibuk,lenbuk,npbuk,tag)
c
c  read a chain of da records of sorted integrals.
c
c  input:
c  bukpt  = record number for the first bucket in the chain.
c  h2(*)  = array for integrals (initialized prior to entry).
c  buk(*) = da record buffer.
c  ibuk(*)= integral label buffer.
c  lenbuk = length of da records.
c  npbuk  = number of integrals held in each record.
c
c  output:
c  h2(*)  = array of integrals.
c
c  written by ron shepard.
c
      use tranlibrary, only : dardr
      implicit integer(a-z)
      real*8 h2(*),buk(lenbuk)
      integer ibuk(npbuk)
      character*(*) tag
c
      integer iword(2)
c
      next=bukpt
100   continue
      if(next.eq.0)return
c     call da2r(buk,next)
      call dardr(5,buk,next)
c
      call ulab32(buk(lenbuk),iword,2)
      next=iword(1)
      num=iword(2)
c
c
c  ***note*** for odd num, an extra integer label is unpacked
c  and not used.  space should be allocated in the calling
c  routine to allow the maximum npbuk labels to be unpacked.
c  this is usually done automatically by allocating integer
c  arrays from a working precision workspace.
c
      call ulab32(buk(npbuk+1),ibuk,num)
c
      do 300 i=1,num
c     write(0,99) trim(tag),i,ibuk(i),buk(i)
 99   format(a,i8,i8,f12.8)
300   h2(ibuk(i))=buk(i)
c
      go to 100
c
      end
      subroutine rdbks0(bukpt,h2,buk,ibuk,lenbuk,npbuk,i0)
c
c  read a chain of da records of sorted integrals.
c  offset of i0 is used to address integrals in h2(*).
c
c  input:
c  bukpt  = record number for the first bucket in the chain.
c  h2(*)  = array for integrals (initialized prior to entry).
c  buk(*) = da record buffers.
c  ibuk(*)= integral label buffers.
c  lenbuk = length of da records.
c  npbuk  = number of integrals held in each record.
c  i0     = address offset for array h2(*).
c
c  output:
c  h2(*)  = updated array of integrals.
c
c  written by ron shepard.
c
      use tranlibrary, only : dardr
      implicit integer(a-z)
      real*8 h2(*),buk(lenbuk)
      integer ibuk(npbuk)
c
      integer iword(2)
c
c     write(0,*) 'rdbks0:bukpt=',bukpt
      next=bukpt
100   continue
      if(next.eq.0)return
c     call da2r(buk,next)
      call dardr(5,buk,next)
c
      call ulab32(buk(lenbuk),iword,2)
      next=iword(1)
      num=iword(2)
c
c
c  ***note*** for odd num, an extra integer label is unpacked
c  and not used.  space should be allocated in the calling
c  routine to allow the maximum npbuk labels to be unpacked.
c  this is usually done automatically by allocating integer
c  arrays from a working precision workspace.
c
      call ulab32(buk(npbuk+1),ibuk,num)
c
c     write(0,93)(i,ibuk(i)-i0,buk(i),i=1,num)
 93   format ('R ',i8,i8,f12.8)
      do 300 i=1,num
300   h2(ibuk(i)-i0)=h2(ibuk(i)-i0)+buk(i)
c
      go to 100
c
      end
c deck rdlab2
      subroutine seqw(iunit,n,a)
c
c  sequential write of a(*) to unit iunit.
c
      integer iunit,n
      real*8 a(n)
      write(iunit)a
      return
      end
      subroutine seqr(iunit,n,a)
c
c  sequential read of a(*) from unit iunit.
c
      integer iunit,n
      real*8 a(n)
      read(iunit)a
      return
      end
      subroutine wtda(buk,ibuk,npbuk,bufb,lenbuk,num,last)
c
c  write a single da record and pack addresses and
c  chaining information.
c
c  input:
c  buk(*)  = integrals.
c  ibuk(*) = unpacked labels.
c  bufb(*) = da record buffer.
c  npbuk   = number of integrals in each da record.
c  lenbuk  = length of da records.
c  num     = number of integrals in this record.
c  last    = record number of the last da record of this chain.
c
c  output:
c  last    = record number of this da record.
c  num     = set to zero.
c
c  written by ron shepard.
c
      use tranlibrary, only : dawrt
      implicit integer(a-z)
      real*8 buk(npbuk),bufb(lenbuk)
      integer ibuk(npbuk)
c
      integer iword(2)
c
c     write(0,*) 'wtda: i,val,ilab',last,num,'= last,num'
c     write(0,93) (i,buk(i),ibuk(i),i=1,num)
c93   format(i8,f12.8,i10) 
      call dcopy_wr(num,buk,1,bufb,1)
c
c  ***note*** for odd num, an extra integral label is packed
c  into the buffer.
c
      call plab32(bufb(npbuk+1),ibuk,num)
c
      iword(1)=last
      iword(2)=num
      call plab32(bufb(lenbuk),iword,2)
c
c     call da2w(bufb,last)
      call dawrt(5,bufb,last)
      num=0
c
      return
      end

c deck wtstv1_grd
      subroutine wtstv1_grd(ntape,info,lbuf,nipbuf,buf,
     & ilab,val,d1,spind,
     & nsym,irx1,irx2,map,ifmt,issymm)
c
c  write the 1-e d1(*) and spind(*) elements.
c
c  input:
c  ntape = output unit number.
c  lbuf = buffer length.
c  nipbuf = maximum number of integrals in a buffer.
c  buf(*) = buffer for integrals and packed labels.
c  ilab(*)= unpacked labels.
c  val(*) = unpacked values.
c  nsym,irx1(*),irx2(*) = symmetry range info.
c  map(*) = orbital index mapping vector.
c
c  version log
c  01-jun-21  added the spind(*) to be written
c  05-oct-09  adapted from wtstv1 by felix plasser
c
      implicit integer(a-z)
c
      integer info(6), ikntf, ikntq, ikntsd
      integer    ibvtyp
      parameter( ibvtyp = 0)
      real*8 fcore
      integer   nipv
      parameter(nipv=2)
c
      integer ibitv, ierr, iknt, isym, itypea, itypebd1, itypespind
      integer last, lbuf, nipbuf, nrec
      integer nsym, ntape, numtot, p, pq, q
cfp
      logical issymm
c
      integer   msame,   nmsame,  nomore
      parameter(msame=0, nmsame=1,nomore=2)
c
      real*8 buf(lbuf),d1(*),spind(*),val(nipbuf)
      integer ilab(nipv,nipbuf),irx1(nsym),irx2(nsym),map(*)
c
      real*8    small
      parameter(small=1d-10)
c
c specification of the various ietypes the ones used here
c are listed below
c
c itypea = 0   symmetric matrix
c itypebd1 = 7 one particle density matrix
c itypebspind = 10 spin-density matrix
c itypea = 2  anti-symmetric matrix
c itypebd1 = 9 anit-symm. one particle densyity matrix

c
      if ( issymm ) then
       itypea  = 0
       itypebd1= 7
       itypebspind= 10
cfp_tmp
c       write(*,*)'+-+-+ symmetric', issymm
      else
       itypea  = 2
       itypebd1= 9
c       write(*,*)'+-+-+ antisymm.', issymm
      endif
      fcore   = 0.0
c
c  d1(*) integrals:
c
      last = nmsame
      numtot = 0
      pq=0
      iknt=0
      do 130 isym=1,nsym
         do 120 p=irx1(isym),irx2(isym)
            do 110 q=irx1(isym),p
               pq=pq+1
               if(abs(d1(pq)).gt.small)then
                  if(iknt.eq.nipbuf)then
                     last = msame
                     numtot = numtot + iknt
                     call sifew1(
     &                 ntape, info,  nipv,    iknt,
     &                 last,  itypea,itypebd1,
     &                 ibvtyp,val,   ilab,    fcore,
     &                 ibitv, buf,   nrec,    ierr)
                     numtot = numtot - iknt
                  endif
                  iknt=iknt+1
                  val(iknt)=d1(pq)
                  ilab(1,iknt)=map(p)
                  ilab(2,iknt)=map(q)
               endif
110         continue
120      continue
130   continue
c
c dump the rest of the d1 matrix elements
c
      if ( issymm ) then
         last = nmsame
      else
         last = nomore
      endif

      numtot = iknt + numtot
      call sifew1(
     & ntape, info,  nipv,    iknt,
     & last,  itypea,itypebd1,
     & ibvtyp,val,   ilab,    fcore,
     & ibitv, buf,   nrec,    ierr)
c
c  spind(*) integrals:
c

      if ( issymm ) then
       pq=0
       ikntsd=0
       do 230 isym=1,nsym
          do 220 p=irx1(isym),irx2(isym)
             do 210 q=irx1(isym),p
                pq=pq+1
                if(abs(spind(pq)).gt.small)then
                   if(ikntsd.eq.nipbuf)then
                      numtot = numtot + ikntsd
                      last = msame
                      call sifew1(
     &                  ntape, info,  nipv,    ikntsd,
     &                  last,  itypea,itypebspind ,
     &                  ibvtyp,val,   ilab,    fcore,
     &                  ibitv, buf,   nrec,    ierr)
                        numtot = numtot - ikntsd
                   endif
                   ikntsd=ikntsd+1
                   val(ikntsd)=spind(pq)
                   ilab(1,ikntsd)=map(p)
                   ilab(2,ikntsd)=map(q)
                endif
210          continue
220       continue
230    continue

c dump the rest of the spind matrix elements
c
       last = nomore
       numtot = numtot + ikntsd
       call sifew1(
     &  ntape, info,  nipv,    ikntsd,
     &  last,  itypea,itypebspind ,
     &  ibvtyp,val,   ilab,    fcore,
     &  ibitv, buf,   nrec,    ierr)
c
      endif
      return
      end
c deck wtstv1
      subroutine wtstv1(ntape,info,lbuf,nipbuf,buf,
     & ilab,val,d1,fmc,
     & qmc,nsym,irx1,irx2,map,ifmt)
c
c  write the 1-e d1(*), fmc(*), and qmc(*) integrals.
c
c  input:
c  ntape = output unit number.
c  lbuf = buffer length.
c  nipbuf = maximum number of integrals in a buffer.
c  buf(*) = buffer for integrals and packed labels.
c  ilab(*)= unpacked labels.
c  val(*) = unpacked values.
c  s1(*) = overlap integrals.
c  t1(*) = 1-e kinetic energy integrals.
c  v1(*) = 1-e potential energy integrals.
c  nsym,irx1(*),irx2(*) = symmetry range info.
c  map(*) = orbital index mapping vector.
c
c  modified by galen f. gawboy to utilize sifs routines
c  written by ron shepard.
c  version log
c  10-jul-96 sif version added Hans Lischka
c
      implicit integer(a-z)
c
      integer info(6), ikntf, ikntq
      integer    ibvtyp
      parameter( ibvtyp = 0)
      real*8 fcore
      integer   nipv
      parameter(nipv=2)
c
      integer ibitv, ierr, iknt, isym, itypea, itypebd1
      integer itypebf, itypebq, last, lbuf, nipbuf, nrec
      integer nsym, ntape, numtot, p, pq, q
c
      integer   msame,   nmsame,  nomore
      parameter(msame=0, nmsame=1,nomore=2)
c
      real*8 buf(lbuf),qmc(*),fmc(*),d1(*),val(nipbuf)
      integer ilab(nipv,nipbuf),irx1(nsym),irx2(nsym),map(*)
c
      real*8    small
      parameter(small=1d-10)
c
c specification of the various ietypes the ones used here
c are listed below
c
c itypea = 0   symmetric matrix
c itypebd1 = 7 one particle density matrix
c itypebf = 8  generalized fock matrix
c itypebq = 9  effective fock matrix
c
      itypea  = 0
      itypebd1= 7
      itypebf = 8
      itypebq = 9
      fcore   = 0.0
c
c  d1(*) integrals:
c
      last = nmsame
      numtot = 0
      pq=0
      iknt=0
      do 130 isym=1,nsym
         do 120 p=irx1(isym),irx2(isym)
            do 110 q=irx1(isym),p
               pq=pq+1
               if(abs(d1(pq)).gt.small)then
                  if(iknt.eq.nipbuf)then
                     last = msame
                     numtot = numtot + iknt
                     call sifew1(
     &                 ntape, info,  nipv,    iknt,
     &                 last,  itypea,itypebd1,
     &                 ibvtyp,val,   ilab,    fcore,
     &                 ibitv, buf,   nrec,    ierr)
                     numtot = numtot - iknt
                  endif
                  iknt=iknt+1
                  val(iknt)=d1(pq)
                  ilab(1,iknt)=map(p)
                  ilab(2,iknt)=map(q)
               endif
110         continue
120      continue
130   continue
c
c dump the rest of the d1 matrix elements
c
      last = nmsame
      numtot = iknt + numtot
      call sifew1(
     & ntape, info,  nipv,    iknt,
     & last,  itypea,itypebd1,
     & ibvtyp,val,   ilab,    fcore,
     & ibitv, buf,   nrec,    ierr)
c
c
c  fmc matrix elements
c
      pq=0
      ikntf=0
      do 230 isym=1,nsym
         do 220 p=irx1(isym),irx2(isym)
            do 210 q=irx1(isym),p
               pq=pq+1
               if(abs(fmc(pq)).gt.small)then
                  if(ikntf.eq.nipbuf)then
                     numtot = numtot + ikntf
                     last = msame
                     call sifew1(
     &                 ntape, info,  nipv,    ikntf,
     &                 last,  itypea,itypebf ,
     &                 ibvtyp,val,   ilab,    fcore,
     &                 ibitv, buf,   nrec,    ierr)
                       numtot = numtot - ikntf
                  endif
                  ikntf=ikntf+1
                  val(ikntf)=fmc(pq)
                  ilab(1,ikntf)=map(p)
                  ilab(2,ikntf)=map(q)
               endif
210         continue
220      continue
230   continue
      numtot = numtot + ikntf
      last = nmsame
      call sifew1(
     & ntape, info,  nipv,    ikntf,
     & last,  itypea,itypebf ,
     & ibvtyp,val,   ilab,    fcore,
     & ibitv, buf,   nrec,    ierr)
c
c  qmc matrix elements
c
      pq=0
      ikntq=0
      do 330 isym=1,nsym
         do 320 p=irx1(isym),irx2(isym)
            do 310 q=irx1(isym),p
               pq=pq+1
               if(abs(qmc(pq)).gt.small)then
                  if(ikntq.eq.nipbuf)then
                     numtot = numtot + ikntq
                     last = msame
                     call sifew1(
     &                 ntape, info,  nipv,    ikntq,
     &                 last,  itypea,itypebq ,
     &                 ibvtyp,val,   ilab,    fcore,
     &                 ibitv, buf,   nrec,    ierr)
                      numtot = numtot - ikntq
                  endif
                  ikntq=ikntq+1
                  val(ikntq)=qmc(pq)
                  ilab(1,ikntq)=map(p)
                  ilab(2,ikntq)=map(q)
               endif
310         continue
320      continue
330   continue
      numtot = numtot + ikntq
      last = nomore
      call sifew1(
     & ntape, info,  nipv,    ikntq,
     & last,  itypea,itypebq ,
     & ibvtyp,val,   ilab,    fcore,
     & ibitv, buf,   nrec,    ierr)
      return
      end
c deck wtlabd2
      subroutine wtlabd2(ntape,iwait,info,lenbuf,buf,nipbuf,val,
     & nipv,ilab,num,last,ifmt)
c
c  write a buffer of labeled 2-e integrals to ntape and
c  pack the values from val(*) and the labels from ilab(*,*).
c
c sifs modifications by galen f. gawboy
c
c  input:
c  ntape = input unit number.
c  lenbuf = record length.
c  nipbuf = maximum number of values.
c  val(*) = values.
c  nipv = number of integer labels per value.
c         1, 2, or 4 : partially or fully unpacked labels.
c
      implicit integer(a-z)
      integer info(10), itypea, itypeb, ifmt, ibvtyp,iwait
      integer ntape,lenbuf,nipbuf,nipv,ilab(4,nipbuf),num,last
      real*8 buf(lenbuf),val(nipbuf)
      integer reqnum, ierr, nrec, ibitv
c
      integer   msame,     nmsame,     nomore
      parameter(msame = 0, nmsame = 1, nomore =  2 )
c
c  bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
c tell sifew2 that we are writing a four index array of 2-particle
c density matrix elements into an unformatted record with no bit
c vector.
c
      itypea = 3
      itypeb = 1
      ibvtyp = 0
c
c for debugging purposes write out the metrix elements.
c
c      write(nlist,'(/1x,a,i6/)')
c     & 'wtlab2: values and labels.nrec=',nrec
c      call siffw2(
c     & info, nipv,num, last
c     & itypea, itypeb, ifmt, ibvtyp,
c     & val, ilab, ibitv, nlist,
c     & ierr)
c      if (ierr .ne. 0) then
c        call bummer('wtlab2: from siffw2, ierr=',ierr,faterr)
c      endif
c
c wait for any previous i/o request.
c
c      call sif2w8( ntape, info, reqnum, ierr)
c      if (ierr .ne. 0) then
c         call bummer('wtlab2: from sif2w8, ierr=',ierr,faterr)
c      endif
c
c pack and write the buffer.
c
      call sifew2(
     & ntape,  info,   nipv,   num,
     & last,   itypea, itypeb, 
     & ibvtyp, val,    ilab,   ibitv,
     & buf,    iwait,  nrec,   reqnum,
     & ierr)
      if(ierr.ne.0) then
          call bummer('wtlab2: from sifew2, ierr=',ierr,faterr)
      endif
c
      return
      end
