!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
C
C     Common variables
C
      INTEGER             lcavity,     nn(3,maxat), nppa,        nspa
      INTEGER             nsph
C
      REAL*8              ampran,      area,        disex,       disex2
      REAL*8              eps,         fepsi,       fn2,         phsran
      REAL*8              refind,      routf,       rsolv
      REAL*8              srad(maxat), tm(3,3,maxat),            volume
C
      COMMON / coss   /   eps,         fepsi,       disex,       disex2
      COMMON / coss   /   rsolv,       routf      
      COMMON / coss   /   area,        refind,      fn2
      COMMON / coss   /   srad,        tm
      COMMON / coss   /   volume,      phsran,      ampran,     lcavity
      COMMON / coss   /   nppa,        nspa ,       nsph,        nn
C
