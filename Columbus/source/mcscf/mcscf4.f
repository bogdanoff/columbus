!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
cmcscf4.f
cmcscf part=4 of 10.  miscellaneous routines.
cversion=5.6 last modified: 16-jan-2001
c
c
      subroutine mosort(
     & core,   lastb,   avcore,   avc2is,
     & add,    h1,      moints,    lenbuf,
     & nipbuf, lenbuk,  npbuk,    ecore0,
     & bdg2e)
c
c  control 2-e integral sorting.
c
c  input:
c  core(*)  = workspace.
c  avcore   = amount of core available.
c  avc2is   = amount of core allocated for second-half integral sort.
c  add(*)   = pre-calculated address arrays.
c  h1(*)    = mo-h1 integrals.
c  lenbuf   = length of buffers for input integrals.
c  nipbuf   = number of integrals in each input buffer.
c  lenbfs   = length of output buffers of sorted integrals.
c
c  output:
c  h1(*)    = modified to include effective inactive-orbital terms.
c  ecore0   = e0 for core orbitals.
c  lastb(*) = record pointer of last bucket written of each bin.
c  lenbuk   = length of da records of sorted integrals.
c  npbuk    = number of integrals in each da record.
c
c  written by ron shepard.
c
      use tranlibrary, only : daopw,daclw,daopr 
      implicit integer(a-z)
      real*8 core(avcore),h1(*),ecore0
      integer add(*),lastb(*)
c
      common/cfiles/iunits(55)
      equivalence (iunits(1),nlist)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      equivalence (nxtot(7),ndot)
      equivalence (nxtot(10),nact)
      equivalence (nxtot(13),nvrt)
      equivalence (nxy(1,25),nvdt)
c
      common/caddb/addpt(24),numint(12),bukpt(12),intoff(12),szh(15)
      equivalence (bukpt(12),nbuk)
c  addpt(*) assignments:
c  1:dd,   2:dd2, 3:ad,   4:aa,   5:vd,   6:va,   7:vv,   8:vv2,
c  9:vv3, 10:o1, 11:o2,  12:o3,  13:o4,  14:o5,  15:o6,  16:o7,
c 17:o8,  18:o9, 19:o10, 20:o11, 21:o12, 22:o13, 23:o14, 24:(tot)
c
      common/cbufs/stape,lenbfs,h2bpt
c
      common/clda/ldamin,ldamax,ldainc
c
c  ldamin=minimum da record length for integral sorting.
c  ldamax=maximum da record length.
c  ldainc=size increment for da record length
c
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
c
c  local core pointers:
c
      integer cpt1(8),cpt2(4)
c
      integer  forbyt
      external forbyt
c
c  bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      real*8 zero
      parameter(zero=0d0)

cmd
c
      integer nmotx
      parameter (nmotx=1023)
c
cmd
      integer ntitmax,nenrgmx
      parameter (ntitmax=10,nenrgmx=10)
      real*8 bdg2e(*),energy(nenrgmx)
      integer moints,nbft,ierr,nsymx,ntitxx,ninfomo,nenrgy,nmap
      integer ietype(nenrgmx),nmpsy(8),infomo(10)
      integer imtype 
      character*80 titlemo(ntitmax)
      character*4  slabel(8)
      character*8  molab(nmotx)
      logical testl1_r, testl2_r
      integer testi1_r
c
c  function numpbf(l) gives the number of integrals held in a
c  working precision buffer of length=l.
c
      if(flags(3))call timer(' ',1,event1,nlist)
c
c  initialize core energy.
c
      ecore0=zero
      call ecore(h1,ecore0)
c
c  allocate space for both halves of the integral sort:
c
c  core allocation for first-half sort:
c  1:bufi,2:vbufi,3:ibufi,4:buk,5:ibuk,6:bknt,7:bufb
c
      lenbuk=0
      npbuk=0
c
c  nbuk has been calculated previously based on avc2is.
c  determine lenbuk.
c
      do 100 lenbkx=ldamin,ldamax,ldainc
      npbkx=numpbf(lenbkx)
      req=lenbuf+nipbuf+forbyt(4*nipbuf)+
     &    (npbkx*nbuk)+forbyt(npbkx*nbuk)+
     &    2*forbyt(nbuk)+lenbkx
      if(req.gt.avcore)go to 200
      lenbuk=lenbkx
      npbuk=npbkx
100   continue
200   if(lenbuk.lt.ldamin)go to 1000
c
c  assign pointers for first-half sort:
c
      cpt1(1)=1
      cpt1(2)=cpt1(1)+lenbuf
      cpt1(3)=cpt1(2)+nipbuf
      cpt1(4)=cpt1(3)+forbyt(4*nipbuf)
      cpt1(5)=cpt1(4)+(npbuk*nbuk)
      cpt1(6)=cpt1(5)+forbyt(npbuk*nbuk)
      cpt1(7)=cpt1(6)+forbyt(nbuk)
      cpt1(8)=cpt1(7)+lenbuk
c-    write(nlist,6010)'cpt1(*)=',cpt1
6010  format(' sortmo: ',a,8i8)
c
c  pointers for second-half sort:
c  1:buk,2:ibuk,3:bufs+core
c
      cpt2(1)=1
      cpt2(2)=cpt2(1)+lenbuk
      cpt2(3)=cpt2(2)+forbyt(npbuk+1)
      cpt2(4)=cpt2(3)+lenbfs
c-    write(nlist,6010)'cpt2(*)=',cpt2
      avcisx=avcore-cpt2(4)+1
      write(nlist,6020)avc2is,avcisx
6020  format(/' mosort: allocated sort2 space, avc2is=',i12,
     +  ' available sort2 space, avcisx=',i12)
      if(avc2is.gt.avcisx)go to 1010
c
c  first-half sort:
c
       inquire(file='moints', exist=testl1_r,
     & opened=testl2_r,number=testi1_r)

       if(testl1_r .and. testl2_r) Close(testi1_r)


      open(file='moints',unit=moints,form='unformatted') 
        call sifrh1( 
     &  moints, ntitxx, nsymx, nbft, 
     &  ninfomo, nenrgy, nmap, ierr )
!
      if ( ierr .ne. 0 ) then
         call bummer('mosort: from sifrh1, ierr=',ierr,faterr)
      elseif ( ntitxx .gt. ntitmax ) then
         call bummer('mosort: from sifrh1, ntitao=',ntitxx,faterr)
      elseif ( nbft .gt. nmotx ) then
         call bummer('mosort: from sifrh1, nbft=',nbft,faterr)
      elseif ( ninfomo .gt. 10 ) then
         call bummer('mosort: from sifrh1, ninfomo=',ninfao,faterr)
      elseif ( nenrgy .gt. nenrgmx ) then
         call bummer('mosort: from sifrh1, nenrgy=',nenrgy,faterr)
      endif

      call sifrh2( 
     &  moints, ntitxx, nsymx,   nbft, 
     &  ninfomo, nenrgy, 0,   titlemo, 
     &  nmbpsy,  slabel, infomo, molab, 
     &  ietype, energy, imtype, map, 
     &  ierr   )

c
c  assign pointers for first-half sort:
c
      cpt1(1)=1
      cpt1(2)=cpt1(1)+infomo(4) 
      cpt1(3)=cpt1(2)+infomo(5)
      cpt1(4)=cpt1(3)+forbyt(4*infomo(5))
      cpt1(5)=cpt1(4)+(npbuk*nbuk)
      cpt1(6)=cpt1(5)+forbyt(npbuk*nbuk)
      cpt1(7)=cpt1(6)+forbyt(nbuk)
      cpt1(8)=cpt1(7)+lenbuk
      ifmt=0
      if (nbft.gt.255) ifmt=1

      call sifsk1(moints,infomo,ierr ) 
      if(flags(3))call timer(' ',1,event2,nlist)
c     call da2ow(lenbuk)
c     last parameter file size required for vdisk and GA only
      call daopw( 5, lenbuk, 100 )
      call mosrt1(avc2is,moints,infomo(4),infomo(5),
     +  core(cpt1(1)),core(cpt1(2)),core(cpt1(3)),h1,
     +  lenbuk,core(cpt1(4)),npbuk,core(cpt1(5)),
     +  lastb,core(cpt1(6)),core(cpt1(7)),
     +  add(addpt( 1)),add(addpt( 2)),add(addpt( 3)),add(addpt( 4)),
     +  add(addpt( 5)),add(addpt( 6)),add(addpt( 7)),add(addpt( 8)),
     +  add(addpt( 9)),add(addpt(10)),add(addpt(11)),add(addpt(12)),
     +  add(addpt(13)),add(addpt(14)),add(addpt(15)),add(addpt(16)),
     +  add(addpt(17)),add(addpt(18)),add(addpt(19)),add(addpt(20)),
     +  bdg2e,
     .   moints,infomo,ifmt)
c     call da2cw
      call daclw(5)
      close(moints)
      if(flags(3))call timer('mosrt1',3,event2,nlist)
c
c  update core reference energy with modified integrals.
c
      call ecore(h1,ecore0)
c
c  second-half sort:
c  for cases where avc2is is greater than the number of integrals
c  to be addressed, reduce the core allocated.
c
      avc2x=min(avc2is,intoff(12))
c
c     call da2or
      call daopr(5)
      rewind stape
      call mosrt2(lastb,core(cpt2(3)),avc2x,
     +  core(cpt2(1)),lenbuk,core(cpt2(2)),npbuk)
      if(flags(3))call timer('mosrt2',4,event2,nlist)
      if(flags(3))call timer('mosort',4,event1,nlist)
      return
c
1000  write(nlist,6100)avcore
6100  format(/' *** mosort: not enough core. avcore=',i8,' ***')
      call bummer('mosort: avcore=',avcore,faterr)
1010  write(nlist,6110)
6110  format(/' *** mosort: avc2is not large enough.')
      call bummer('mosort: avc2is error, avcore=',avcore,faterr)
      return
      end
      subroutine mosrt1(avc2is,itape,lenbuf,nipbuf,
     +  bufi,vali,ibufi,u,
     +  lenbuk,buk,npbuk,ibuk,lastb,bknt,bufb,
     +  adddd,adddd2,addad,addaa,addvd,addva,addvv,addvv2,addvv3,
     +  off1,off2,off3,off4,off5,off6,off7,off8,
     +  off9,off10,off11,bdg2e,
     .  moints,infomo,ifmt)
c
c  read and sorts the 2-e mo integrals.  the
c  integrals are first divided into types depending on the
c  number of inactive, active, and virtual orbitals.
c  partial sums are collected where appropriate, and the
c  integrals are assigned addresses and placed into buckets
c  for direct access i/o.  the following is a list of the
c  integral types, assigned indices, u(*) matrix contributions
c
c  where:    u(xy)= h1(xy) +   sum  [2(xy|kk)-(xk|yk)],
c                            k(=inactive)
c
c  and the addressing scheme used for each integral type.
c  indices i,j,k,l are for inactive orbitals,
c  indices p,q,r,s are for active orbitals, and
c  indices a,b,c,d are for virtual orbitals.
c
c   #    type    labels     u(*)   addressing scheme
c  ---   ----    -------    ----   -----------------
c   1    dddd    (ij|kl)    udd    -----
c   2    addd    (pi|jk)    uad    -----
c   3    adad    (pi|qj)    uaa    off6(pq)+adddd2(j,i)
c   4    aadd    (pq|ij)    uaa    off6(pq)+adddd(ij)
c   5    aaad    (pq|ri)    ---    off2(pq)+addad(i,r)
c   6    aaaa    (pq|rs)    ---    off1(pq)+addaa(rs)
c   7    vddd    (ai|jk)    uvd    -----
c   8    vdad    (ai|pj)    uva    p[off10(ai)+addad(j,p)]
c   9    vdaa    (ai|pq)    ---    off3(pq)+addvd(a,i)
c  10    vdvd    (ai|bj)    uvv    p[off11(ij)+addvv3(b,a)]
c  11    vadd    (ap|ij)    uva    p[off10(ai)+addad(j,p)]
c  12    vaad    (aq|pi)    ---    off4(q,p)+addvd(a,i)
c  13    vaaa    (ar|pq)    ---    off5(pq)+addva(a,r)
c  14    vavd    (ap|bi)    ---    p[off9(ai)+addva(b,p)
c  15    vava    (ap|bq)    ---    off8(pq)+addvv2(b,a)
c  16    vvdd    (ab|ij)    uvv    p[off11(ij)+addvv3(b,a)]
c  17    vvad    (ab|pi)    ---    p[off9(ai)+addva(b,p)
c  18    vvaa    (ab|pq)    ---    off7(pq)+addvv(ab)
c  19    vvvd     -----     ---    -----
c  20    vvva     -----     ---    -----
c  21    vvvv     -----     ---    -----
c
cmd
c   In case you run "direct mcscf" (flags(22)=.true.) following
c   integrals will be omitted:
c
c   #    integral type:        except:
c  ---   -------------         -------
c   8       vdad                ----
c   10      vdvd               (ia|ia)
c   11      vadd                ----
c   14      vavd                ----
c   16      ddvv               (ii|aa)
c   17      vvad                ----
cmd
c  input:
c  u(*)       :1-e h matrix.
c  bufi(*)    :input integral buffer.
c  vali(*)    :input integral value bufer.
c  ibufi(*)   :input integral label buffer.
c  buk(*)     :output integral buffer.
c  ibuk(*)    :output integral label buffer.
c  bufb(*)    :output da record buffer.
c  off*(*)    :addressing arrays for various types of integrals.
c  add**(*)   :addressing arrays.
c  orbtyp(*)  :gives the type (fc=0,d=1,a=2,v=3,fv=4) of each orbital.
c  symm(*)    :gives the symmetry of each orbital.
c  orbidx(*)  :gives the orbital number within its type.
c  avc2is     :available core during second-half of the integral sort.
c
c  output:
c  u(*)       :modified to include the inactive-orbital contributions
c              to form the effective 1-e hamiltonian matrix.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      integer  moints,infomo(10),ifmt,ierr,last,nbuf
      integer lvlprt,itypeb,itypea
      common/prtout/lvlprt
c
      common/cfiles/iunits(55)
      equivalence (iunits(1),nlist)
c
      common/caddb/addpt(24),numint(12),bukpt(12),intoff(12),szh(15)
      equivalence (bukpt(12),nbuk)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      equivalence (nxtot(4),nmot)
      integer nmpsy(8),nsm(8),nnsm(8),nvpsy(8)
      equivalence (nxy(1,5),nmpsy(1))
      equivalence (nxy(1,6),nsm(1))
      equivalence (nxy(1,7),nnsm(1))
      equivalence (nxy(1,17),nvpsy(1))
      equivalence (nxtot(1),nbft)
      equivalence (nxtot(17),ndimd)
      equivalence (nxtot(18),ndima)
      equivalence (nxtot(19),ndimv)
c
c
      integer nmotx
      parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer symm(nmotx),orbidx(nmotx),orbtyp(nmotx),nndx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
      equivalence (iorbx(1,3),symm(1))
      equivalence (iorbx(1,6),orbidx(1))
      equivalence (iorbx(1,7),orbtyp(1))
c
      real*8 bufi(lenbuf),vali(nipbuf)
      real*8 u(*),buk(npbuk,nbuk),bufb(lenbuk)
      parameter (nipv=4)
      integer ibufi(nipv,nipbuf),ibuk(npbuk,nbuk),lastb(nbuk),bknt(nbuk)
      integer adddd(*),addaa(*),addvv(*)
      integer adddd2(ndimd,ndimd),addad(ndimd,ndima)
      integer addvd(ndimv,ndimd),addva(ndimv,ndima)
      integer addvv2(ndimv,ndimv),addvv3(ndimv,ndimv)
      integer off1(*),off2(*),off3(*)
      integer off4(ndima,ndima)
      integer off5(*),off6(*),off7(*),off8(*),off9(*),off10(*),off11(*)
c
      logical qnosrt(0:4)
      integer iknti(22),iknto(22),addx(2),bukx(2)
      real*8 vint,valx(2)
      real*8     two,    three,    four
      parameter (two=2d0,three=3d0,four=4d0)
      parameter (more=0)
c
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
cmd
      real*8 bdg2e(*)
      icount1 = 1
      icount2 = 1
cmd

c
      data qnosrt/.true., .false., .false., .false., .true./
c
c  initialize some sorting arrays.
c
cmd
c     for debug ...
c
cmd   open(unit=99,file='int_test',status='unknown')
cmd   write(99,*)nbft
cmd
      do 10 i=1,22
      iknto(i)=0
10    iknti(i)=0
c
      do 20 i=1,nbuk
      lastb(i)=0
20    bknt(i)=0
c
c  process labeled 2-e integrals.  the integral file must be
c  positioned correctly prior to entry.
c
c
100   continue
c
c  read a buffer of labeled 2-e integrals.
c
ct  use sifr1d call 
ct  no label ordering is assumed !
ct    call rdlab2(itape,lenbuf,bufi,nipbuf,vali,nipv,ibufi,num,last)
c     subroutine sifrd2(
c    & aoint2,  info,    nipv,    iretbv,
c    & buffer,  num,     last,    itypea,
c    & itypeb,  ifmt,    ibvtyp,  values,
c    & labels,  ibitv,   ierr )

        itypea=3
        itypeb=0
        call sifrd2(moints,infomo,nipv,0,bufi,num,last,itypea,
     .       itypeb,itmp,vali,ibufi,itmp,ierr)

c
c  process integrals in vali(*)
c
      do 30000 int=1,num
c
      vint=vali(int)
      l1=ibufi(1,int)
      if(l1.eq.0)go to 30000
      l2=ibufi(2,int)
      l3=ibufi(3,int)
      l4=ibufi(4,int)
      t1=orbtyp(l1)
      if(qnosrt(t1))go to 22000
      t2=orbtyp(l2)
      if(qnosrt(t2))go to 22000
      t3=orbtyp(l3)
      if(qnosrt(t3))go to 22000
      t4=orbtyp(l4)
      if(qnosrt(t4))go to 22000
c
      if(t1.lt.t2)then
         t=l1
         l1=l2
         l2=t
         t=t1
         t1=t2
         t2=t
      endif
      if(t3.lt.t4)then
         t=l3
         l3=l4
         l4=t
         t=t3
         t3=t4
         t4=t
      endif
      tt1=nndx(t1)+t2
      tt2=nndx(t3)+t4
      if(tt1.lt.tt2)then
         t=l1
         l1=l3
         l3=t
         t=l2
         l2=l4
         l4=t
         t=tt1
         tt1=tt2
         tt2=t
      endif
      type=nndx(tt1)+tt2
      iknti(type)=iknti(type)+1
      go to (1000,2000,3000,4000,5000,6000,7000,
     +  8000,9000,10000,11000,12000,13000,14000,
     +  15000,16000,17000,18000,19000,20000,21000),type
c*****************************************************************
1000  continue
c
c  dddd  place integral into u(*)
c
c  place indices in canonical order.
c
      if(l1.lt.l2)then
         t=l1
         l1=l2
         l2=t
      endif
      if(l3.lt.l4)then
         t=l3
         l3=l4
         l4=t
      endif
      ll1=nndx(l1)+l2
      ll2=nndx(l3)+l4
      if(ll1.lt.ll2)then
         t=l1
         l1=l3
         l3=t
         t=l2
         l2=l4
         l4=t
      endif
      i=l1
      j=l2
      k=l3
      l=l4
      isym=symm(i)
      jsym=symm(j)
      ksym=symm(k)
      lsym=symm(l)
      if(i.ne.j)go to 1070
      if(k.ne.l)go to 1050
      if(i.ne.k)go to 1040
c (ii|ii)
      i=i-nsm(isym)
      ii=nnsm(isym)+nndx(i)+i
      u(ii)=u(ii)+vint
      go to 1130
1040  continue
c (ii|kk)
      i=i-nsm(isym)
      k=k-nsm(ksym)
      ii=nnsm(isym)+nndx(i)+i
      kk=nnsm(ksym)+nndx(k)+k
      u(ii)=u(ii)+two*vint
      u(kk)=u(kk)+two*vint
      go to 1130
1050  continue
      if(i.ne.k)go to 1060
c (ii|il)
      i=i-nsm(isym)
      l=l-nsm(isym)
      il=nnsm(isym)+nndx(i)+l
      u(il)=u(il)+vint
      go to 1130
1060  continue
c (ii|kl)
      k=k-nsm(ksym)
      l=l-nsm(ksym)
      kl=nnsm(ksym)+nndx(k)+l
      u(kl)=u(kl)+two*vint
      go to 1130
1070  continue
      if(k.ne.l)go to 1090
      if(j.ne.k)go to 1080
c (ij|jj)
      i=i-nsm(isym)
      j=j-nsm(isym)
      ij=nnsm(isym)+nndx(i)+j
      u(ij)=u(ij)+vint
      go to 1130
1080  continue
c (ij|kk)
      i=i-nsm(isym)
      j=j-nsm(isym)
      ij=nnsm(isym)+nndx(i)+j
      u(ij)=u(ij)+two*vint
      go to 1130
1090  continue
      if(j.ne.l)go to 1110
      if(i.ne.k)go to 1100
c (ij|ij)
      i=i-nsm(isym)
      j=j-nsm(jsym)
      ii=nnsm(isym)+nndx(i)+i
      jj=nnsm(jsym)+nndx(j)+j
      u(ii)=u(ii)-vint
      u(jj)=u(jj)-vint
      go to 1130
1100  continue
c (ij|kj)
      i=i-nsm(isym)
      k=k-nsm(isym)
      ik=nnsm(isym)+nndx(i)+k
      u(ik)=u(ik)-vint
      go to 1130
1110  continue
      if(j.ne.k)go to 1120
c (ij|jl)
      i=i-nsm(isym)
      l=l-nsm(isym)
      il=nnsm(isym)+nndx(i)+l
      u(il)=u(il)-vint
      go to 1130
1120  continue
      if(i.ne.k)go to 1130
c (ij|il)
      j=j-nsm(jsym)
      l=l-nsm(jsym)
      jl=nnsm(jsym)+nndx(j)+l
      u(jl)=u(jl)-vint
1130  continue
      go to 30000
c*****************************************************************
2000  continue
c
c  addd     place integral in u(*)
c
      p=l1
      psym=symm(p)
      i=l2
      j=l3
      k=l4
      if(j.ne.k)go to 2020
      if(i.ne.j)go to 2010
c  (pi|ii)
      p=p-nsm(psym)
      i=i-nsm(psym)
      pi=nnsm(psym)+nndx(max(i,p))+min(i,p)
      u(pi)=u(pi)+vint
      go to 2050
2010  continue
c  (pi|jj)
      p=p-nsm(psym)
      i=i-nsm(psym)
      pi=nnsm(psym)+nndx(max(i,p))+min(i,p)
      u(pi)=u(pi)+two*vint
      go to 2050
2020  if(i.ne.j)go to 2030
c  (pi|ik)
      p=p-nsm(psym)
      k=k-nsm(psym)
      pk=nnsm(psym)+nndx(max(k,p))+min(k,p)
      u(pk)=u(pk)-vint
      go to 2050
2030  if(i.ne.k)go to 2050
c  (pi|ji)
      p=p-nsm(psym)
      j=j-nsm(psym)
      pj=nnsm(psym)+nndx(max(j,p))+min(j,p)
      u(pj)=u(pj)-vint
2050  continue
c  (pi|jk)
      go to 30000
c*****************************************************************
3000  continue
c
c  adad   place integral into u(*) and sort.
c
      if(l2.eq.l4)then
         psym=symm(l1)
         p=l1-nsm(psym)
         q=l3-nsm(psym)
         pq=nnsm(psym)+nndx(max(p,q))+min(p,q)
         u(pq)=u(pq)-vint
       endif
      if(bukpt(6).eq.0)go to 30000
      p=orbidx(l1)
      i=orbidx(l2)
      q=orbidx(l3)
      j=orbidx(l4)
      if(p.lt.q)then
         t=p
         p=q
         q=t
         t=i
         i=j
         j=t
      endif
      pq=nndx(p)+q
      addx(1)=intoff(6)+off6(pq)+adddd2(j,i)
      bukx(1)=bukpt(6)+(addx(1)-1)/avc2is
      valx(1)=vint
      if(p.ne.q .or. i.eq.j)go to 24001
c  (pi|pj)=(pj|pi)
      addx(2)=intoff(6)+off6(pq)+adddd2(i,j)
      bukx(2)=bukpt(6)+(addx(2)-1)/avc2is
      valx(2)=vint
      go to 24002
c*****************************************************************
4000  continue
c
c  aadd    place in u(*) and sort.
c
      if(l3.eq.l4)then
         psym=symm(l1)
         p=l1-nsm(psym)
         q=l2-nsm(psym)
         pq=nnsm(psym)+nndx(max(p,q))+min(p,q)
         u(pq)=u(pq)+two*vint
      endif
      if(bukpt(6).eq.0)go to 30000
      p=orbidx(l1)
      q=orbidx(l2)
      i=orbidx(l3)
      j=orbidx(l4)
      pq=nndx(max(p,q))+min(p,q)
      ij=nndx(max(i,j))+min(i,j)
      addx(1)=intoff(6)+off6(pq)+adddd(ij)
      bukx(1)=bukpt(6)+(addx(1)-1)/avc2is
      valx(1)=vint
      go to 24001
c*****************************************************************
5000  continue
c
c  aaad    sort integral
c
      if(bukpt(2).eq.0)go to 30000
      p=orbidx(l1)
      q=orbidx(l2)
      r=orbidx(l3)
      i=orbidx(l4)
      pq=nndx(max(p,q))+min(p,q)
      addx(1)=intoff(2)+off2(pq)+addad(i,r)
      bukx(1)=bukpt(2)
      valx(1)=vint
      go to 24001
c*****************************************************************
6000  continue
c
c  aaaa   sort integral
c
      if(bukpt(1).eq.0)go to 30000
      p=orbidx(l1)
      q=orbidx(l2)
      r=orbidx(l3)
      s=orbidx(l4)
      pq=nndx(max(p,q))+min(p,q)
      rs=nndx(max(r,s))+min(r,s)
      addx(1)=off1(max(pq,rs))+addaa(min(pq,rs))
      bukx(1)=bukpt(1)
      valx(1)=vint
      go to 24001
c*****************************************************************
7000  continue
c
c  vddd     place integral in u(*)
c
      a=l1
      asym=symm(a)
      i=l2
      j=l3
      k=l4
      if(j.ne.k)go to 7020
      if(i.ne.j)go to 7010
c  (ai|ii)
      a=a-nsm(asym)
      i=i-nsm(asym)
      ai=nnsm(asym)+nndx(max(i,a))+min(i,a)
      u(ai)=u(ai)+vint
      go to 7050
7010  continue
c  (ai|jj)
      a=a-nsm(asym)
      i=i-nsm(asym)
      ai=nnsm(asym)+nndx(max(i,a))+min(i,a)
      u(ai)=u(ai)+two*vint
      go to 7050
7020  if(i.ne.j)go to 7030
c  (ai|ik)
      a=a-nsm(asym)
      k=k-nsm(asym)
      ak=nnsm(asym)+nndx(max(k,a))+min(k,a)
      u(ak)=u(ak)-vint
      go to 7050
7030  if(i.ne.k)go to 7050
c  (ai|ji)
      a=a-nsm(asym)
      j=j-nsm(asym)
      aj=nnsm(asym)+nndx(max(j,a))+min(j,a)
      u(aj)=u(aj)-vint
7050  continue
c  (ai|jk)
      go to 30000
c*****************************************************************
8000  continue
c
c  vdad  place in u(*) and sort integral
c
      asym=symm(l1)
      if(l2.eq.l4)then
c   (ai|pi)
         a=l1-nsm(asym)
         p=l3-nsm(asym)
         ap=nnsm(asym)+nndx(max(a,p))+min(a,p)
         u(ap)=u(ap)-vint
      endif
c
c  p(ai,pj) = 4*(ai|pj) - (aj|pi) - (ap|ij)
c  p(aj,pi) = 4*(aj|pi) - (ai|pj) - (ap|ij)
c  p(ai,pi) = 3*(ai|pi) - (ap|ii)
c
      if(bukpt(10).eq.0)go to 30000
      a=orbidx(l1)
      i=orbidx(l2)
      isym=symm(l2)
      p=orbidx(l3)
      psym=symm(l3)
      j=orbidx(l4)
      if(i.eq.j)then
c  p(ai,pi) term.
         if(asym.ne.isym)go to 24000
         ai=addvd(a,i)
         addx(1)=intoff(10)+off10(ai)+addad(i,p)
         bukx(1)=bukpt(10)+(addx(1)-1)/avc2is
         valx(1)=three*vint
         go to 24001
      endif
c  p(ai,pj) term.
      nx=0
      if(asym.eq.isym)then
         nx=1
         ai=addvd(a,i)
         addx(1)=intoff(10)+off10(ai)+addad(j,p)
         bukx(1)=bukpt(10)+(addx(1)-1)/avc2is
         valx(1)=four*vint
      endif
c  p(aj,pi) term.
      if(psym.eq.isym)then
         aj=addvd(a,j)
         nx=nx+1
         addx(nx)=intoff(10)+off10(aj)+addad(i,p)
         bukx(nx)=bukpt(10)+(addx(nx)-1)/avc2is
         valx(nx)=-vint
      endif
      go to (24000,24001,24002),nx+1
c*****************************************************************
9000  continue
c
c  vdaa    sort integral
c
      if(bukpt(3).eq.0)go to 30000
      a=orbidx(l1)
      i=orbidx(l2)
      p=orbidx(l3)
      q=orbidx(l4)
      pq=nndx(max(p,q))+min(p,q)
      addx(1)=intoff(3)+off3(pq)+addvd(a,i)
      bukx(1)=bukpt(3)
      valx(1)=vint
      go to 24001
c*****************************************************************
10000 continue
c
c  vdvd    place in u(*) and sort integral.
c    p(ai,bj) = 4*(ai|bj) - (ij|ab) - (bi|aj)
c    p(bi,aj) = 4*(bi|aj) - (ij|ab) - (ai|bj)
c    p(ai,bi) = 3*(ai|bi) - (ii|ab)
c    p(bi,ai) = 3*(bi|ai) - (ii|ab)
c    p(ai,aj) = 3*(ai|aj) - (ij|aa)
c    p(ai,ai) = 3*(ai|ai) - (ii|aa)
c
cmd
cmd   write(99,9999)l1,l2,l3,l4,vint
cmd
      asym=symm(l1)
      if (flags(22)) go to 10010
      if(l2.eq.l4)then
c  (ai|bi)
         a=l1-nsm(asym)
         b=l3-nsm(asym)
         ab=nnsm(asym)+nndx(max(a,b))+min(a,b)
         u(ab)=u(ab)-vint
      endif
10010 if(bukpt(11).eq.0)go to 30000
      a=orbidx(l1)
      i=orbidx(l2)
      isym=symm(l2)
      b=orbidx(l3)
      bsym=symm(l3)
      j=orbidx(l4)
      if(i.lt.j)then
         a=orbidx(l3)
         asym=symm(l3)
         i=orbidx(l4)
         isym=symm(l4)
         b=orbidx(l1)
         bsym=symm(l1)
         j=orbidx(l2)
      endif
      ij=nndx(i)+j
      it=1
      if(a.eq.b)it=it+1
      if(i.eq.j)it=it+2
      go to (10100,10200,10300,10400),it
10100  continue
      nx=0
      if(asym.eq.isym)then
c  p(ai,bj) term.
         nx=1
         addx(1)=intoff(11)+off11(ij)+addvv3(b,a)
         bukx(1)=bukpt(11)+(addx(1)-1)/avc2is
         valx(1)=four*vint
      endif
      if(bsym.eq.isym)then
c  p(bi,aj) term.
         nx=nx+1
         addx(nx)=intoff(11)+off11(ij)+addvv3(a,b)
         bukx(nx)=bukpt(11)+(addx(nx)-1)/avc2is
         valx(nx)=-vint
      endif
      go to (24000,24001,24002),nx+1
10200 continue
c  p(ai,aj) term.
      if(asym.eq.isym)then
         addx(1)=intoff(11)+off11(ij)+addvv3(a,a)
         bukx(1)=bukpt(11)+(addx(1)-1)/avc2is
         valx(1)=three*vint
         go to 24001
      endif
      go to 30000
10300 continue
      if(asym.eq.isym)then
c  p(ai,bi) term.
         addx(1)=intoff(11)+off11(ij)+addvv3(b,a)
         bukx(1)=bukpt(11)+(addx(1)-1)/avc2is
         valx(1)=three*vint
c  p(bi,ai) term.
         addx(2)=intoff(11)+off11(ij)+addvv3(a,b)
         bukx(2)=bukpt(11)+(addx(2)-1)/avc2is
         valx(2)=three*vint
         go to 24002
      endif
      go to 30000
10400 continue
      if(asym.eq.isym)then
c  p(ai,ai) term.
         addx(1)=intoff(11)+off11(ij)+addvv3(a,a)
         bukx(1)=bukpt(11)+(addx(1)-1)/avc2is
         valx(1)=three*vint
         if(flags(22))then
         bdg2e(icount1)=bdg2e(icount1)+three*vint
         icount1 = icount1 +1

         endif
         go to 24001
      endif
      go to 30000
c*****************************************************************
11000 continue
c
c  vadd    place in u(*) and sort integral
c
c  p(ai,pj) = 4*(ai|pj) - (aj|pi) - (ap|ij)
c  p(aj,pi) = 4*(aj|pi) - (ai|pj) - (ap|ij)
c  p(ai,pi) = 3*(ai|pi) - (ap|ii)
c
      asym=symm(l1)
      if(l3.eq.l4)then
c  (ap|ii)
         a=l1-nsm(asym)
         p=l2-nsm(asym)
         ap=nnsm(asym)+nndx(max(a,p))+min(a,p)
         u(ap)=u(ap)+two*vint
      endif
      if(bukpt(10).eq.0)go to 30000
      a=orbidx(l1)
      p=orbidx(l2)
      psym=symm(l2)
      i=orbidx(l3)
      isym=symm(l3)
      j=orbidx(l4)
      nx=0
c  p(ai,pj) and p(ai,pi) terms.
      if(asym.eq.isym)then
         ai=addvd(a,i)
         nx=1
         addx(1)=intoff(10)+off10(ai)+addad(j,p)
         bukx(1)=bukpt(10)+(addx(1)-1)/avc2is
         valx(1)=-vint
         if(i.eq.j)go to 24001
      endif
c  p(aj,pi) term.
      if(psym.eq.isym)then
         aj=addvd(a,j)
         nx=nx+1
         addx(nx)=intoff(10)+off10(aj)+addad(i,p)
         bukx(nx)=bukpt(10)+(addx(nx)-1)/avc2is
         valx(nx)=-vint
      endif
      go to (24000,24001,24002),nx+1
c*****************************************************************
12000 continue
c
c  vaad    sort integral
c
      if(bukpt(4).eq.0)go to 30000
      a=orbidx(l1)
      q=orbidx(l2)
      p=orbidx(l3)
      i=orbidx(l4)
      addx(1)=intoff(4)+off4(q,p)+addvd(a,i)
      bukx(1)=bukpt(4)
      valx(1)=vint
      go to 24001
c*****************************************************************
13000 continue
c
c  vaaa    sort integral
c
      if(bukpt(5).eq.0)go to 30000
      a=orbidx(l1)
      r=orbidx(l2)
      p=orbidx(l3)
      q=orbidx(l4)
      pq=nndx(max(p,q))+min(p,q)
      addx(1)=intoff(5)+off5(pq)+addva(a,r)
      bukx(1)=bukpt(5)
      valx(1)=vint
      go to 24001
c*****************************************************************
14000 continue
c
c  vavd    sort integral
c
c  p(ap,bi) = 4*(ap|bi)-(ab|pi)-(bp|ai)
c  p(bp,ai) = 4*(bp|ai)-(ab|pi)-(ap|bi)
c  p(ap,ai) = 3*(ap|ai)-(aa|pi)
c
      if(bukpt(9).eq.0)go to 30000
      a=orbidx(l1)
      asym=symm(l1)
      p=orbidx(l2)
      psym=symm(l2)
      b=orbidx(l3)
      bsym=symm(l3)
      i=orbidx(l4)
c  p(ap,ai) term.
      if(a.eq.b .and. asym.eq.psym)then
         ai=addvd(a,i)
         addx(1)=intoff(9)+off9(ai)+addva(a,p)
         bukx(1)=bukpt(9)+(addx(1)-1)/avc2is
         valx(1)=three*vint
         go to 24001
      endif
c  p(ap,bi) term.
      nx=0
      if(asym.eq.psym)then
         nx=1
         bi=addvd(b,i)
         addx(1)=intoff(9)+off9(bi)+addva(a,p)
         bukx(1)=bukpt(9)+(addx(1)-1)/avc2is
         valx(1)=four*vint
      endif
c  p(bp,ai) term.
      if(bsym.eq.psym)then
         nx=nx+1
         ai=addvd(a,i)
         addx(nx)=intoff(9)+off9(ai)+addva(b,p)
         bukx(nx)=bukpt(9)+(addx(nx)-1)/avc2is
         valx(nx)=-vint
      endif
      go to (24000,24001,24002),nx+1
c*****************************************************************
15000 continue
c
c  vava    sort integral
c
      if(bukpt(8).eq.0)go to 30000
      a=orbidx(l1)
      p=orbidx(l2)
      b=orbidx(l3)
      q=orbidx(l4)
      if(p.lt.q)then
         a=orbidx(l3)
         p=orbidx(l4)
         b=orbidx(l1)
         q=orbidx(l2)
      endif
      pq=nndx(p)+q
      addx(1)=intoff(8)+off8(pq)+addvv2(b,a)
      bukx(1)=bukpt(8)+(addx(1)-1)/avc2is
      valx(1)=vint
      if(p.ne.q .or. a.eq.b)go to 24001
c  (ap|bp) = (bp|ap)
      addx(2)=intoff(8)+off8(pq)+addvv2(a,b)
      bukx(2)=bukpt(8)+(addx(2)-1)/avc2is
      valx(2)=vint
      go to 24002
c*****************************************************************
16000 continue
c
c  vvdd    place in u(*) and sort integral
c    p(ai,bj) = 4*(ai|bj) - (ij|ab) - (bi|aj)
c    p(bi,aj) = 4*(bi|aj) - (ij|ab) - (ai|bj)
c    p(ai,bi) = 3*(ai|bi) - (ii|ab)
c    p(bi,ai) = 3*(bi|ai) - (ii|ab)
c    p(ai,aj) = 3*(ai|aj) - (ij|aa)
c    p(ai,ai) = 3*(ai|ai) - (ii|aa)
c
cmd
cmd   write(99,9999)l1,l2,l3,l4,vint
cmd
      asym=symm(l1)
      if (flags(22)) go to 16100
      if(l3.eq.l4)then
c  (ab|ii)
         a=l1-nsm(asym)
         b=l2-nsm(asym)
         ab=nnsm(asym)+nndx(max(a,b))+min(a,b)
         u(ab)=u(ab)+two*vint
      endif
16100 if(bukpt(11).eq.0)go to 30000
      a=orbidx(l1)
      b=orbidx(l2)
      bsym=symm(l2)
      i=orbidx(l3)
      isym=symm(l3)
      j=orbidx(l4)
      ij=nndx(max(i,j))+min(i,j)
      nx=0
      if(a.ne.b .and. asym.eq.isym)then
c  p(ai,bj) and p(ai,bi) terms.
         nx=1
         addx(1)=intoff(11)+off11(ij)+addvv3(b,a)
         bukx(1)=bukpt(11)+(addx(1)-1)/avc2is
         valx(1)=-vint
      endif
c  p(bi,aj), p(bi,ai), p(ai,aj), and p(ai,ai) terms.
16001 if(bsym.eq.isym)then
         nx=nx+1
         addx(nx)=intoff(11)+off11(ij)+addvv3(a,b)
         bukx(nx)=bukpt(11)+(addx(nx)-1)/avc2is
         valx(nx)=-vint

         if(flags(22))then
         bdg2e(icount2)=bdg2e(icount2)-vint
         icount2 = icount2 + 1
         endif

      endif
      go to (24000,24001,24002),nx+1
c*****************************************************************
17000 continue
c
c  vvad    sort integral
c  p(ap,bi) = 4*(ap|bi)-(ab|pi)-(bp|ai)
c  p(bp,ai) = 4*(bp|ai)-(ab|pi)-(ap|bi)
c  p(ap,ai) = 3*(ap|ai)-(aa|pi)
c
      if(bukpt(9).eq.0)go to 30000
      a=orbidx(l1)
      asym=symm(l1)
      b=orbidx(l2)
      bsym=symm(l2)
      p=orbidx(l3)
      psym=symm(l3)
      i=orbidx(l4)
      nx=0
      if(asym.eq.psym)then
         nx=1
         bi=addvd(b,i)
         addx(1)=intoff(9)+off9(bi)+addva(a,p)
         bukx(1)=bukpt(9)+(addx(1)-1)/avc2is
         valx(1)=-vint
         if(a.eq.b)go to 24001
      endif
      if(bsym.eq.psym)then
         nx=nx+1
         ai=addvd(a,i)
         addx(nx)=intoff(9)+off9(ai)+addva(b,p)
         bukx(nx)=bukpt(9)+(addx(nx)-1)/avc2is
         valx(nx)=-vint
      endif
      go to (24000,24001,24002),nx+1
c*****************************************************************
18000 continue
c
c  vvaa    sort integral
c
      if(bukpt(7).eq.0)go to 30000
      a=orbidx(l1)
      b=orbidx(l2)
      p=orbidx(l3)
      q=orbidx(l4)
      pq=nndx(max(p,q))+min(p,q)
      ab=nndx(max(a,b))+min(a,b)
      addx(1)=intoff(7)+off7(pq)+addvv(ab)
      bukx(1)=bukpt(7)+(addx(1)-1)/avc2is
      valx(1)=vint
      go to 24001
c*****************************************************************
19000 continue
c
c  vvvd    integral is not needed
c
      go to 30000
c*****************************************************************
20000 continue
c
c  vvva    integral is not needed
c
      go to 30000
c*****************************************************************
21000 continue
c
c  vvvv    integral is not needed
      go to 30000
c*****************************************************************
22000 continue
c
c  ffff,fffx, etc.    integral is not needed
c
      iknti(22)=iknti(22)+1
      go to 30000
c*****************************************************************
c
c  place integrals in appropriate buckets.
c
24002 continue
      iknto(type)=iknto(type)+1
      b=bukx(2)
      if(bknt(b).eq.npbuk)
     + call wtda(buk(1,b),ibuk(1,b),npbuk,bufb,lenbuk,bknt(b),lastb(b))
      bknt(b)=bknt(b)+1
      buk(bknt(b),b)=valx(2)
      ibuk(bknt(b),b)=addx(2)
c
24001 continue
      iknto(type)=iknto(type)+1
      b=bukx(1)
      if(bknt(b).eq.npbuk)
     + call wtda(buk(1,b),ibuk(1,b),npbuk,bufb,lenbuk,bknt(b),lastb(b))
      bknt(b)=bknt(b)+1
      buk(bknt(b),b)=valx(1)
      ibuk(bknt(b),b)=addx(1)
c
24000 continue
c
cmd
c30000 write(99,9999)l1,l2,l3,l4,vint
 9999 format(4i3,3x,d20.13)
cmd
30000 continue
      if(last.eq.more)go to 100
c
c  dump any partially full buckets.
c
      do 30010 b=1,nbuk
      if(bknt(b).gt.0)
     + call wtda(buk(1,b),ibuk(1,b),npbuk,bufb,lenbuk,bknt(b),lastb(b))
30010 continue
c
      ikntit=0
      ikntot=0
      do 30020 i=1,22
         ikntot=ikntot+iknto(i)
         ikntit=ikntit+iknti(i)
30020 continue
      if (lvlprt.gt.1) then
      write(nlist,60010)(i,iknti(i),iknto(i),i=1,22)
      write(nlist,60020)ikntit,ikntot
      endif
60010 format(' mosrt1: integrals of each type (type:in:out)'/
     +  (2(1x,'(',i3,':',i8,':',i8,')')))
60020 format(' mosrt1: total in=',i8,4x,'total out=',i8)
c
cmd
c     write(58,*)icount1,icount2
cmd   close(99)
cmd
      return
      end
      subroutine mosrt2(lastb,core,avc2is,buk,lenbkx,
     +  ibuk,npbukx)
c
c  perform the second-half bin sort of 2-e integral types 6-11.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/cis2/add0,ibpt,ic0,icmax,ic0mx,avc2,nterm,ibk,lenbuk,npbuk
c
      common/cbufs/stape,lenbfs,h2bpt
c
      real*8 core(*),buk(*)
      integer lastb(*),ibuk(*)
c
      common/caddb/addpt(24),numint(12),bukpt(12),intoff(12),szh(15)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nvpsy(8)
      equivalence (nxy(1,17),nvpsy(1))
      equivalence (nxtot(7),ndot)
      equivalence (nxtot(10),nact)
      equivalence (nxtot(13),nvrt)
      integer tsymdd(8),tsmdd2(8),tsymad(8),tsymvd(8)
      integer tsymva(8),tsymvv(8),tsmvv2(8)
      equivalence (nxy(1,21),tsymdd(1))
      equivalence (nxy(1,22),tsmdd2(1))
      equivalence (nxy(1,23),tsymad(1))
      equivalence (nxy(1,25),tsymvd(1))
      equivalence (nxy(1,26),tsymva(1))
      equivalence (nxy(1,27),tsymvv(1))
      equivalence (nxy(1,28),tsmvv2(1))
c
c
      integer nmotx
      parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer symx(nmotx)
      equivalence (iorbx(1,4),symx(1))
      equivalence (ix0(1),ix0d),(ix0(2),ix0a)
c
c  prepare for sort: initialize /cis2/
c  add0  = address offset for placing integrals in core.
c  ibpt  = beginning of current buffer space.
c  ic0   = running offset within core(*).
c  icmax = address of the last integral in core(*).
c  ic0mx = minimum of icmax and the address of last buffer
c          element in core(*), ic0mx=min(icmax,ibpt+lenbfs-1).
c
      add0=0
      ibpt=1
      ic0=0
      icmax=0
      ic0mx=0
      avc2=avc2is
      lenbuk=lenbkx
      npbuk=npbukx
c
      ibk=0
      do 10 i=1,5
10    ibk=max(bukpt(i),ibk)
c  ...on loop exit, ibk is one less than the first required
c  bucket chain number for the second half sort.
c
c  type-6 integrals: off6(pq)+adddd(ij), off6(pq)+adddd2(j,i)
c
      if(bukpt(6).eq.0)go to 700
      do 620 p=1,nact
      psym=symx(ix0a+p)
      do 610 q=1,p
      pqsym=mult(symx(ix0a+q),psym)
      nterm=tsymdd(pqsym)
c
      if(ic0+nterm.gt.ic0mx)call sdas(core,core(ibpt),lastb,buk,ibuk)
      ic0=ic0+nterm
c
      nterm=tsmdd2(pqsym)
      if(ic0+nterm.gt.ic0mx)call sdas(core,core(ibpt),lastb,buk,ibuk)
610   ic0=ic0+nterm
620   continue
c
700   continue
c
c  type-7 integrals: off7(pq)+addvv(ab)
c
      if(bukpt(7).eq.0)go to 800
      do 720 p=1,nact
      psym=symx(ix0a+p)
      do 710 q=1,p
      pqsym=mult(symx(ix0a+q),psym)
      nterm=tsymvv(pqsym)
      if(nterm.eq.0)go to 710
c
      if(ic0+nterm.gt.ic0mx)call sdas(core,core(ibpt),lastb,buk,ibuk)
710   ic0=ic0+nterm
720   continue
c
800   continue
c
c  type-8 integrals: off8(pq)+addvv2(b,a)
c
      if(bukpt(8).eq.0)go to 900
      do 820 p=1,nact
      psym=symx(ix0a+p)
      do 810 q=1,p
      pqsym=mult(symx(ix0a+q),psym)
      nterm=tsmvv2(pqsym)
      if(nterm.eq.0)go to 810
c
      if(ic0+nterm.gt.ic0mx)call sdas(core,core(ibpt),lastb,buk,ibuk)
810   ic0=ic0+nterm
820   continue
c
900   continue
c
c  type-9 integrals: off9(ai)+addva(b,p)
c
      if(bukpt(9).eq.0)go to 1000
      nai=tsymvd(1)
      do 910 ai=1,nai
      nterm=tsymva(1)
      if(nterm.eq.0)go to 910
c
      if(ic0+nterm.gt.ic0mx)call sdas(core,core(ibpt),lastb,buk,ibuk)
910   ic0=ic0+nterm
c
1000  continue
c
c  type-10 integrals: off10(ai)+addad(j,p)
c
      if(bukpt(11).eq.0)go to 1100
      nai=tsymvd(1)
      do 1010 ai=1,nai
      nterm=tsymad(1)
      if(nterm.eq.0)go to 1010
c
      if(ic0+nterm.gt.ic0mx)call sdas(core,core(ibpt),lastb,buk,ibuk)
1010  ic0=ic0+nterm
c
1100  continue
c
c  type-11 integrals: off11(ij)+addvv3(b,a)
c
      if(bukpt(11).eq.0)go to 1200
      do 1120 i=1,ndot
      na=nvpsy(symx(ix0d+i))
      do 1110 j=1,i
      nb=nvpsy(symx(ix0d+j))
      nterm=na*nb
      if(nterm.eq.0)go to 1110
c
      if(ic0+nterm.gt.ic0mx)call sdas(core,core(ibpt),lastb,buk,ibuk)
1110  ic0=ic0+nterm
1120  continue
c
1200  continue
c
c  all done.  dump the last buffer of sorted integrals.
c
      if(ic0+1.ne.ibpt)call seqw(stape,lenbfs,core(ibpt))
c
      return
      end
      subroutine motran(c,k,fdd,faa,qaa,qvv,d1,iorder,naar,
     +  cr,cno,occ,cn,sa,sx,ia,ib,engy,den)
c
c  transform the current orbital coefficients to resolve
c  redundant variables of the current iteration, to determine the
c  natural orbitals of the current iteration, and to prepare for
c  the next mcscf iteration.
c
c  input:
c  c(*)     = current orbital coefficients.
c  k(*)     = packed k(*) vector, used to form exp(-k).
c  fdd(*)   = inactive orbital fock matrix.
c  faa(*)   = active orbital fock matrix.
c  qaa(*)   = active orbital q matrix.
c  qvv(*)   = virtual orbital q matrix.
c  d1(*)    = active orbital one-particle density matrix.
c  iorder(*)= active-active mapping vector.
c  naar     = number of allowed active-active rotations.
c  sa(*)    = scratch matrix.
c  sx(*)    = scratch vector.
c  ia(*)    = integer pointer vector.
c  ib(*)    = integer pointer vector.
c
c  output:
c  c(*),k(*)= unchanged.
c  cr(*)    = resolved current orbital coefficients.
c  cno(*)   = natural orbitals for the current iteration.
c  occ(*)   = natural orbital occupations for the current iteration.
c  cn(*)    = orbital coefficients for the next mcscf iteration.
c  engy(*)  = resolved orbital energy values
c  den(*)   = resolved diagonal density matrix elements
c
c  written by ron shepard.
c  02-aug-95 mosign() added to correct some orbital phase problems.
c
      implicit integer(a-z)
c
      common/cfiles/iunits(55)
      equivalence (iunits(1),nlist)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nbpsy(8)
      equivalence (nxy(1,1),nbpsy(1))
      integer nmpsy(8),nsm(8),n2sm(8)
      equivalence (nxy(1,5),nmpsy(1))
      equivalence (nxy(1,6),nsm(1))
      equivalence (nxy(1,8),n2sm(1))
      equivalence (nxtot(6),n2tm)
      integer ndpsy(8)
      equivalence (nxy(1,9),ndpsy(1))
      integer napsy(8),nnsa(8)
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxy(1,15),nnsa(1))
      integer nsbm(8)
      equivalence (nxy(1,32),nsbm(1))
      integer ird1(8),ird2(8)
      equivalence (nxy(1,37),ird1(1))
      equivalence (nxy(1,38),ird2(1))
      integer ira1(8)
      equivalence (nxy(1,39),ira1(1))
c
c
      integer nmotx
      parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer invx(nmotx)
      equivalence (iorbx(1,5),invx(1))
      equivalence (ix0(1),ix0d),(ix0(2),ix0a)
c
      integer iorder(*)
      real*8 c(*),k(*)
      real*8 fdd(*),faa(*),qaa(*),qvv(*),d1(*)
      real*8 cr(*),cno(*),occ(*),cn(*)
      real*8 sa(*),sx(*)
      integer ia(*),ib(*)
      real*8 engy(*), den(*)
c
      call timer(' ',1,event,nlist)
c
c**********************************************************************
c  form the transformation matrix to resolve redundant variables.
c**********************************************************************
c
c  t(*) matrix is returned in cno(*).
c  cr(*) and cn(*) are used for scratch.
c
      call reslvm(cno,fdd,faa,qaa,qvv,d1,cr,cn,sa,sx,ia,ib,engy,den)
c
c**********************************************************************
c  form the exp(-k) transformation matrix.
c**********************************************************************
c
c  rearrange the k(*) matrix blocks and store the square-packed
c  intermediate result in cn(*)
c
      call korder(k,cn,iorder,naar)
c
      do 110 isym=1,nsym
      nm=nmpsy(isym)
      if(nm.eq.0)go to 110
c
c  form the matrix exp(-k) and store in cn(*).
c  cr(*), sa(*) and occ(*) are used for scratch.
c
      x1y1=n2sm(isym)+1
      call expmk(nm,cn(x1y1),cr,sa,occ)
c
c        # adjust the orbital phases of t(:,:) and exp(-k).
c        # t(:,:) is stored in cno(:,:), exp(-k) is stored in cn(:,:).
         call mosign( nm, cn(  x1y1) )
         call mosign( nm, cno( x1y1) )
c
110   continue
c
c**********************************************************************
c  form the natural orbital transformation for the current iteration.
c**********************************************************************
c
c  u(*) is initialized as t(*) to resolve the inactive and virtual
c  orbitals which are occupation degenerate.
c  u(*) is stored in sa(*), t(*) is in cno(*).
c
      call dcopy_wr(n2tm,cno,1,sa,1)
c
      do 210 isym=1,nsym
      nm=nmpsy(isym)
      if(nm.eq.0)go to 210
      ni=ndpsy(isym)
      np=napsy(isym)
c
c  occupations are stored in occ(*).
c  updated u(*) is returned in sa(*), cr(*) is used for scratch.
c
      x1y1=n2sm(isym)+1
      x1=nsm(isym)+1
      i1=ird1(isym)
      p1=ira1(isym)
      pq1=nnsa(isym)+1
      call notran(nm,ni,np,sa(x1y1),occ(x1),d1(pq1),
     +  invx(ix0d+i1),invx(ix0a+p1),cr,sx,1d0)
c
c  adjust orbital phases of u(:,:).
c  u(:,:) is stored in sa(:,:)
         call mosign( nm, sa(x1y1) )
c
210   continue
c
c**********************************************************************
c  apply the set of transformations to the input orbital coefficients.
c**********************************************************************
c
c  current orbitals are in c(*)
c  t(*) transformation to resolve orbitals is in cno(*).
c  exp(-k) matrix is in cn(*).
c  u(*) natural orbital transformation is in sa(*).
c
c  form exp(-k) * t and store in cr(*).
c
      do 310 isym=1,nsym
      nm=nmpsy(isym)
      if(nm.eq.0)go to 310
      x1y1=n2sm(isym)+1
      call mxma(cn(x1y1),1,nm, cno(x1y1),1,nm, cr(x1y1),1,nm, nm,nm,nm)
310   continue
c
c  form the orbitals for the next mcscf iteration:
c  cn = c * (exp(-k)*t)
c     = c * cr
c
      do 320 isym=1,nsym
      nm=nmpsy(isym)
      if(nm.eq.0)go to 320
      nb=nbpsy(isym)
      x1y1=n2sm(isym)+1
      b1x1=nsbm(isym)+1
      call mxma(c(b1x1),1,nb, cr(x1y1),1,nm, cn(b1x1),1,nb, nb,nm,nm)
320   continue
c
c  resolve the orbitals of the current iteration:
c  cr = c * t
c     = c * cno
c
      do 330 isym=1,nsym
      nm=nmpsy(isym)
      if(nm.eq.0)go to 330
      nb=nbpsy(isym)
      x1y1=n2sm(isym)+1
      b1x1=nsbm(isym)+1
      call mxma(c(b1x1),1,nb, cno(x1y1),1,nm, cr(b1x1),1,nb, nb,nm,nm)
330   continue
c
c  form the natural orbitals of the current iteration.
c  cno = c * u
c      = c * sa
c
      do 340 isym=1,nsym
      nm=nmpsy(isym)
      if(nm.eq.0)go to 340
      nb=nbpsy(isym)
      x1y1=n2sm(isym)+1
      b1x1=nsbm(isym)+1
      call mxma(c(b1x1),1,nb, sa(x1y1),1,nm, cno(b1x1),1,nb, nb,nm,nm)
340   continue
c
      call timer('motran',4,event,nlist)
      return
      end
      subroutine notran(n,ni,np,u,occ,d1,invd,inva,sa,sx,deltaab)
c
c  compute the transformation to diagonalize the one-particle
c  density matrix over the active orbitals.
c
c  arguments:
c  n      = number of mos.
c  ni     = number of inactive orbitals.
c  np     = number of active orbitals.
c  u(*)   = initial transformation. cfp: and output transformation
c  occ(*) = natural orbital occupations.
c  d1(*)  = density matrix.
c  invd(*)= inactive-orbital to mo mapping.
c  inva(*)= active-orbital to mo mapping.
c  sa(*)  = scratch matrix.
c  sx(*)  = scratch vector.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/cfiles/iunits(55)
      equivalence (iunits(1),nlist)
c
      integer invd(*),inva(*)
      real*8 u(n,n),occ(n),d1(*),sa(*),sx(*)
c
      real*8 deltaab,twodab
cfp      parameter(two=2d0)
c
c  eps   = small active-orbital occupation tolerance.
c  twome = large active-orbital occupation tolerance.
c
      real*8 eps,twome
      parameter(eps=1d-3, twome=2d0-eps)
c
      twodab = 2d0*deltaab
c
      if(np.ne.0)then
c
c  diagonalize the d1(*) matrix.
c  only the lower triangle is required in the eispac call.
c
      pq=0
      do 110 p=1,np
      do 110 q=1,p
      pq=pq+1
      pq2=(q-1)*np+p
      sa(pq2)=d1(pq)
110   continue
c
c  put the eigenvectors in sa(*), eigenvalues in sx(*), and
c  use occ(*) for scratch.
c
      call eigip(np,np,sa,sx,occ)
c
c  check for extreme occupations and print a warning message.
c  small occupations may result in zero hessian eigenvalues due to
c  rotations between active and virtual orbitals while large
c  occupations may result in zero hessian eigenvalues due to rotations
c  between active and inactive orbitals.
c
      do 115 i=1,np
      if(sx(i).lt.eps)write(nlist,6010)'small',i,sx(i)
      if(sx(i).gt.twome)write(nlist,6010)'large',i,sx(i)
115   continue
c
      endif
c
c  compute the occ(*) vector.
c  zero for virtual orbitals, two for inactive orbitals,
c  and eigenvalues for active orbitals (in reverse order).
c
cfp: twodab=0 for a transition density matrix
crs: twodab=0 for a spin density matrix
c
      call wzero(n,occ,1)
      do 120 i=1,ni
120   occ(invd(i))=twodab
      do 130 p=1,np
130   occ(inva(p))=sx(np+1-p)
c
c  copy the vectors back into u(*) (in reverse order).
c
      pq2=0
      do 140 p=1,np
      mp=inva(np+1-p)
      do 140 q=1,np
      pq2=pq2+1
140   u(inva(q),mp)=sa(pq2)
c
      return
6010  format(' *** warning *** ',a,' active-orbital occupation. i=',i3,
     +  ' nocc=',1pe11.4)
      end
      subroutine stspnos(c,d1,cno,occ,title,ntitle,sa,sx,deltaab)
c
c  compute the state specific NOs
c  subroutine adapted from motran
c  - felix plasser 2010-07-29
c
c *** change:
c  input:
c  c(*)     = current orbital coefficients.
c  d1(*)    = state specific active orbital one-particle density matrix
c  title(*) = title for output file
c  ntitle   = number of titles
c  sa(*)    = scratch matrix.
c  sx(*)    = scratch vector.
c
c  output:
c  c(*) = unchanged
c  cno(*)   = natural orbitals for the current iteration.
c  occ(*)   = natural orbital occupations for the current iteration.
c
      implicit integer(a-z)
c
      common/cfiles/iunits(55)
      equivalence (iunits(1),nlist)
      equivalence (iunits(24),mocout)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nbpsy(8)
      equivalence (nxy(1,1),nbpsy(1))
      integer nmpsy(8),nsm(8),n2sm(8)
      equivalence (nxy(1,5),nmpsy(1))
      equivalence (nxy(1,6),nsm(1))
      equivalence (nxy(1,8),n2sm(1))
      equivalence (nxtot(6),n2tm)
      integer ndpsy(8)
      equivalence (nxy(1,9),ndpsy(1))
      integer napsy(8),nnsa(8)
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxy(1,15),nnsa(1))
      integer nsbm(8)
      equivalence (nxy(1,32),nsbm(1))
      integer ird1(8),ird2(8)
      equivalence (nxy(1,37),ird1(1))
      equivalence (nxy(1,38),ird2(1))
      integer ira1(8)
      equivalence (nxy(1,39),ira1(1))
c
      integer nbfmxp,ninfomx,nenrgymx,nmapmx
      parameter (nbfmxp=1023,ninfomx=6,nenrgymx=5,nmapmx=2)
c
      character*4 slabelao(nbfmxp)
      character*8 bfnlabao(nbfmxp)
      character*8 bfnlabmo(nbfmxp)
      integer info,nmap,ietype,imtype,map,nenrgy
      real*8 energy
      common /forsif/ info(ninfomx),nenrgy,nmap,
     . ietype(nenrgymx), energy(nenrgymx),
     . imtype(nmapmx), map(nbfmxp*nmapmx),
     . slabelao, bfnlabao
c
      integer nmotx
      parameter (nmotx=1023)
      real*8 one
      parameter (one=1.d0)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer invx(nmotx)
      equivalence (iorbx(1,5),invx(1))
      equivalence (ix0(1),ix0d),(ix0(2),ix0a)
c
c      integer iorder(*)
      integer ntitle,filerr,syserr
c
      real*8 deltaab
      real*8 c(*),d1(*)
      real*8 cno(*),occ(*)
      real*8 sa(*),sx(*)
c
      character*80 title(*)
      character*40 cfmt
      parameter (cfmt='(1p3d25.15)')
c      integer ia(*),ib(*)
c      real*8 engy(*), den(*)
c
      call timer(' ',1,event,nlist)
c
c
      do 700 isym=1,nsym
       nm=nmpsy(isym)
       if(nm.eq.0)go to 700
       x1y1=n2sm(isym)+1
c
c  initialize as a unit matrix.
c
       call wzero(nm*nm,sa(x1y1),1)
       xx=x1y1
       do 110 x=1,nm
         sa(xx)=one
         xx=xx+nm+1
110    enddo
700   continue

c
c**********************************************************************
c  form the natural orbital transformation for the current iteration.
c**********************************************************************
c
c  u(*) is initialized as a unit matrix
c  u(*) is stored in sa(*).
c
c      call dcopy_wr(n2tm,cno,1,sa,1)
c
      do 210 isym=1,nsym
      nm=nmpsy(isym)
      if(nm.eq.0)go to 210
      ni=ndpsy(isym)
      np=napsy(isym)
c
c  occupations are stored in occ(*).
c  updated u(*) is returned in sa(*), cno(*) is used for scratch.
c
      x1y1=n2sm(isym)+1
      x1=nsm(isym)+1
      i1=ird1(isym)
      p1=ira1(isym)
      pq1=nnsa(isym)+1
      call notran(nm,ni,np,sa(x1y1),occ(x1),d1(pq1),
     +  invx(ix0d+i1),invx(ix0a+p1),cno,sx,deltaab)
c
c  adjust orbital phases of u(:,:).
c  u(:,:) is stored in sa(:,:)
         call mosign( nm, sa(x1y1) )
c
210   continue
c
c**********************************************************************
c  apply the set of transformations to the input orbital coefficients.
c**********************************************************************
c
c  current orbitals are in c(*)
c  u(*) natural orbital transformation is in sa(*).
c
c  form the natural orbitals of the current iteration.
c  cno = c * u
c      = c * sa
c
      do 340 isym=1,nsym
      nm=nmpsy(isym)
      if(nm.eq.0)go to 340
      nb=nbpsy(isym)
      x1y1=n2sm(isym)+1
      b1x1=nsbm(isym)+1
      call mxma(c(b1x1),1,nb, sa(x1y1),1,nm, cno(b1x1),1,nb, nb,nm,nm)
340   continue

c      write(nlist,*) 'Occupation' 
c      write(nlist,*) occ(1:67)
c
c write the NOs to a file
      call mowrit(mocout,10,filerr,syserr,ntitle,title,cfmt,nsym,
     &     nbpsy,nmpsy,slabelao,cno)
      call mowrit(mocout,20,filerr,syserr,ntitle,title,cfmt,nsym,
     &     nbpsy,nmpsy,slabelao,cno)
      call mowrit(mocout,40,filerr,syserr,ntitle,title,cfmt,nsym,
     &     nbpsy,nmpsy,slabelao,occ)
c
      call timer('stspnos',4,event,nlist)
      return
      end
      subroutine prtdrt(levprt,nact,nrow,nsym,
     +  nj,njskp,modrt,syml,a,b,l,xbar,y,xp,z)
c
c  print drt information from levels levprt(1) to levprt(2).
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/cfiles/iunits(55)
      equivalence (iunits(1),nlist)
c
      integer nj(0:nact),njskp(0:nact),modrt(0:nact),syml(0:nact)
      integer l(0:3,nrow),xbar(nsym,nrow),y(nsym,0:3,nrow)
      integer a(nrow),b(nrow)
      integer xp(nsym,nrow),z(nsym,nrow)
      integer levprt(2)
c
6010  format(/' level',i3,' through level',i3,' of the drt:'//
     +  1x,'row',' lev',' a',' b','  mo',' syml',
     +  '  l0 ',' l1 ',' l2 ',' l3 ',
     +  'isym',' xbar ', '  y0  ','  y1  ','  y2  ','  xp  ','   z')
6030  format(/1x,i3,i4,i2,i2,i4,i4,i5,3i4,i4,6i6)
6040  format(1x,36x,i4,6i6)
6050  format(1x,36('.'))
c
c  restrict array references to a meaningful range.
c
      l1=min(levprt(1),levprt(2))
      l2=max(levprt(1),levprt(2))
      l1=min(max(l1,0),nact)
      l2=min(max(l2,0),nact)
c
      write(nlist,6010)l1,l2
c
      do 300 ilev=l2,l1,-1
      ir1=njskp(ilev)+1
      ir2=njskp(ilev)+nj(ilev)
c
      do 200 irow=ir1,ir2
c
      isym=1
      write(nlist,6030)irow,ilev,a(irow),b(irow),
     +  modrt(ilev),syml(ilev),(l(i,irow),i=0,3),
     +  isym,xbar(isym,irow),(y(isym,i,irow),i=0,2),
     +  xp(isym,irow),z(isym,irow)
c
      if(nsym.eq.1)go to 200
      do 100 isym=2,nsym
      write(nlist,6040)
     +  isym,xbar(isym,irow),(y(isym,i,irow),i=0,2),
     +  xp(isym,irow),z(isym,irow)
100   continue
c
200   continue
c
      write(nlist,6050)
300   continue
c
      return
      end
      subroutine rcmprs(m,ndimr,ncfx,nolds,br,cr,wr,zr,t,scr,r,bxr,cxr)
c
c  compress the current expansion space spanned by the columns
c  of r(*,1:m) to a subspace of dimension (m-1).  the method used
c  is to delete the component of the last trial vector r(*,m) that
c  is orthogonal to the last approximate z vector, where z=r*zr.
c
c  input:
c  m     = number of columns of r(*) in the calling program.
c  ndimr = number of rows of r(*) in the calling program.
c  ncfx  = number of rows of cxr(*) in calling program.
c  nolds = number of columns of cr(*).
c  br    = subspace b matrix. br( m*(m+1)/2 )
c  cr    = subspace c matrix. cr(m,nolds)
c  wr    = subspace w vector.
c  zr    = subspace z vector. z = r * zr
c  scr   = scratch. scr( m*(m+1)/2 )
c  r     = expansion vectors. r(ndimr,m)
c  bxr   = bxr matrix-vector products. bxr(ndimr,m)
c  cxr   = cxr matrix-vector products. cxr(ncfx,m)
c
c  output:
c  r(*,*), bxr(*,*), cxr(*,*) and all representations are updated.
c  t     = transformation matrix. t(m,m-1) where r' = r * t.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      real*8 br(*),cr(m,*),wr(m),zr(m),t(m,*),scr(*)
      real*8 r(*),bxr(*),cxr(*)
c
      real*8 dnrm2_wr
      real*8 one,term
      parameter(one=1d0)
c
      mm1=m-1
      nnmm1=(m*mm1)/2
c
c  form the transformation matrix.
c
      call wzero(m*mm1,t,1)
c
      term=zr(m)/dnrm2_wr(mm1,zr,1)**2
c
      do 100 i=1,mm1
      t(i,i)=one
100   t(m,i)=term*zr(i)
c
      call orthot(m,mm1,t,0)
c
c  transform basis vectors r' = r * t.
c
      call maaxb(r,1,ndimr, t,1,m, scr,1, ndimr,m,mm1)
c
c  transform bxr' = bxr * t.
c
      call maaxb(bxr,1,ndimr, t,1,m, scr,1, ndimr,m,mm1)
c
c  transform cxr' = cxr * t.
c
      if(ncfx.ne.0)call maaxb(cxr,1,ncfx, t,1,m, scr,1, ncfx,m,mm1)
c
c  transform br' = t(transpose) * br * t.
c
      call uthu(t,br,scr,scr(nnmm1+1),m,mm1)
      call dcopy_wr(nnmm1,scr,1,br,1)
c
c  transform cr' = t(transpose) * cr.
c  compute as cr'(transpose) = cr(transpose) * t.
c
      if(nolds.ne.0)call maaxb(cr,m,1, t,1,m, scr,1, nolds,m,mm1)
c
c  transform wr' = t(transpose) * wr.
c
      call mxma(t,m,1, wr,1,1, scr,1,1, mm1,m,1)
      call dcopy_wr(mm1,scr,1,wr,1)
c
c  transform zr' = t(transpose) * zr.
c
      call mxma(t,m,1, zr,1,1, scr,1,1, mm1,m,1)
      call dcopy_wr(mm1,scr,1,zr,1)
c
      return
      end
c
c*******************************************************************+
c
      subroutine rddrt(
     & ndot,   nrow,   nwalk,   ncsf,
     & lenbuf, buf,    doub,    modrt,
     & syml,   nj,     njskp,   a,
     & b,      l,      y,       xbar,
     & xp,     z,      limvec,  r,
     & occl,   drtprt, modrt_1, syml_1,
     & ist )
c
c  read drt arrays.  header info has already been read.  drt file
c  should be positioned correctly to begin reading drt arrays and
c  pointer vectors.
c
c  24-oct-91 buf(*) used to buffer doub(*) and symd(*). -rls
c  28-sep-91 vrsion=2 support added. -rls
c  written by ron shepard.
c
       implicit none
c  ##  parameter & common block section
c
c     # bummer error types.
      integer    wrnerr,   nfterr,   faterr
      parameter( wrnerr=0, nfterr=1, faterr=2)
c
      integer nmotx
      parameter (nmotx=1023)
c
      integer      iorbx,         ix0
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer orbtyp(nmotx)
      equivalence (iorbx(1,7),orbtyp(1))
c
      integer      nxy,      mult,     nsym,nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nmpsy(8), nsm(8)
      equivalence (nxy(1,5),nmpsy(1))
      equivalence (nxy(1,6),nsm(1))
      integer               nmot
      equivalence (nxtot(4),nmot)
      integer                nact
      equivalence (nxtot(10),nact)
c
      integer       iunits
      common/cfiles/iunits(55)
      integer nlist,ndrt
      equivalence (iunits(1),nlist)
      equivalence (iunits(17),ndrt)
c
c    #  maximal No. of different DRTs
      integer maxnst
      parameter (maxnst=8)
c
c    #  maximal No. of states in one DRT
      integer mxavst
      parameter (mxavst = 50)
c
c    #  maximal total number of states
      integer maxstat
      parameter (maxstat = maxnst*mxavst)
c
      integer navst,nst,navst_max
      real*8 heig,wavst
      common /avstat/ wavst(maxnst,mxavst), heig(maxnst,mxavst),
     & navst(maxnst),nst,navst_max
c
      integer nrow_f,ncsf_f,nwalk_f,ssym
      common/drtf/ nrow_f(maxnst),ncsf_f(maxnst),
     & nwalk_f(maxnst),ssym(maxnst)
c
      integer lenbuf_f(maxnst)
c
c  ##  integer section
c
      integer nwalk,nrow
c
      integer a(nrow)
      integer buf(*), b(nrow),b0
      integer doub(*)
      integer ist,imd,i,itemp,ierr
      integer j
      integer lenbuf,l(0:3,nrow),limvec(nwalk),levprt(2),li
      integer modrt(0:nact), modrt_1(0:nact)
      integer ndot, ncsf,nx(0:1,0:1),ntypex(0:5),
     &        nj(0:nact), njskp(0:nact), nlevel,ndotx,nactx
      integer occl(*)
      integer r(nwalk)
      integer syml_1(0:nact), syml(0:nact)
      integer xbar(nsym,nrow), xp(nsym,nrow)
      integer y(nsym,0:3,nrow)
      integer z(nsym,nrow)
c
c  ##  character section
c
      character*60 fname
c
c  ##  logical section
c
      logical drtprt
c
c  ##  data section
c
      integer actx(0:5),doubx(0:5)
      data actx/5,5,5,2,5,5/, doubx/0,5,5,1,5,5/
c
c---------------------------------------------------------------
c
      if (nst.eq.1) then
       fname = 'mcdrtfl'
      elseif(ist.ge.10) then
       write(fname,'(a8,i2)')'mcdrtfl.',ist
      else
       write(fname,'(a8,i1)')'mcdrtfl.',ist
      endif
      call trnfln( 1, fname )
      open(unit=ndrt,file=fname,status='old')
c
c  read over header sections
      read(ndrt,*, iostat=ierr)
       if ( ierr .ne. 0 ) then
        call bummer('rddrt: title ierr=',ierr,faterr)
       endif
c     # first header record.
      call rddbl( ndrt, lenbuf, buf, -nsym )
c     # second header record.
      call rddbl( ndrt, lenbuf, buf, -nsym )
c
c     # nsm(*) has not yet been set...
c     # hack in a fix here.
      itemp = 0
      do 10 i = 1, nsym
         nsm(i) = itemp
         itemp = itemp + nmpsy(i)
10    continue
c
c     # read in the drt arrays.
c
      nlevel = nact + 1
c
c     # slabel(*)...
      call rddbl( ndrt, lenbuf, buf, -nsym )
c
      if ( ndot .gt. 0 ) then
c
c        # doub(*)...
c        # use buf(b0+1:b0+ndot) for scratch.
         b0 = 2 * ndot + 1
         call rddbl( ndrt, lenbuf, buf(b0+1), ndot )
c
c        # symd(*)...
c        # use buf(b0+ndot+1:b0+2*ndot+1) for scratch.
         call rddbl( ndrt, lenbuf, buf(b0+ndot+1), ndot )
c
c        # move into buf(1:2*ndot+1) and convert into doub(*).
         do 20 i = 1, ndot
            buf( 2*(i-1) + 1 ) = buf(b0 + ndot + i)
            buf( 2*(i-1) + 2 ) = buf(b0 + i)
20       continue
         buf( b0 ) = 0
         call symcvt( nsym, 2, buf, nmpsy, nsm, ndotx, 1, doub, ierr )
         if ( ierr .ne. 0 ) then
            call bummer('rddrt: from doub symcvt, ierr=', ierr, faterr )
         elseif ( ndot .ne. ndotx ) then
            call bummer('rddrt: from symcvt, ndotx=', ndotx, faterr )
         endif
      endif
c
c     # modrt(*)...
c     # use a(*) for scratch.
      call rddbl( ndrt, lenbuf, a, nact )
c
c     # syml(*)...
      syml(0)=0
      call rddbl( ndrt, lenbuf, syml(1), nact )
cmd
      if (ist.eq.1) then
         do imd = 0,nact
         syml_1(imd) = syml(imd)
         enddo
      else
         do imd = 0,nact
         if(syml_1(imd).ne.syml(imd)) then
            write(nlist,*)
     &    'list of active orbitals is not equal for all drt tables'
           call bummer('rddrt: syml input error',0, faterr )
         endif
         enddo
      endif
cmd
c
c     # move into buf(*) and convert into modrt(*).
      do 30 i = 1, nact
         buf( 2*(i-1) + 1 ) = syml(i)
         buf( 2*(i-1) + 2 ) = a(i)
30    continue
      buf( 2*nact + 1 ) = 0
      modrt(0) = 0
      call symcvt( nsym, 2, buf, nmpsy, nsm, nactx, 1, modrt(1), ierr )
      if ( ierr .ne. 0 ) then
         call bummer('rddrt: from modrt symcvt, ierr=', ierr, faterr )
      elseif ( nact .ne. nactx ) then
         call bummer('rddrt: from symcvt, nactx=', nactx, faterr )
      endif
cmd
      if (ist.eq.1) then
         do imd = 0,nact
         modrt_1(imd) = modrt(imd)
         enddo
      else
         do imd = 0,nact
         if(modrt_1(imd).ne.modrt(imd)) then
            write(nlist,*)
     &    'list of active orbitals is not equal for all drt tables'
            call bummer('rddrt:modrt input error',0, faterr )
         endif
         enddo
      endif
cmd
c
c     # nj(*)...
      call rddbl( ndrt, lenbuf, nj, nlevel )
c
c     # njskp(*)...
      call rddbl( ndrt, lenbuf, njskp, nlevel )
c
c     # a(*)...
      call rddbl( ndrt, lenbuf, a, nrow )
c
c     # b(*)...
      call rddbl( ndrt, lenbuf, b, nrow )
c
c     # l(*)...
      call rddbl( ndrt, lenbuf, l, 4*nrow )
c
c     # y(*,*,*)...
      call rddbl( ndrt, lenbuf, y, 4*nrow*nsym )
c
c     # xbar(*,*)...
      call rddbl( ndrt, lenbuf, xbar, nrow*nsym )
c
c     # xp(*,*)...
      call rddbl( ndrt, lenbuf, xp, nrow*nsym )
c
c     # z(*,*)...
      call rddbl( ndrt, lenbuf, z, nrow*nsym )
c
c     # limvec(*)...
      call rddbl( ndrt, lenbuf, limvec, nwalk )
c
c     # r(*)...
      call rddbl( ndrt, lenbuf, r, nwalk )
c
c-    write(nlist,6070) 'modrt(*)', (modrt(i), i=1,nact )
c
c-    if ( ndot .ne. 0 )
c-   & write(nlist,6070) 'doub(*)', (doub(i), i=1,ndot )
6070  format(/1x,a,(t10,10i4,10x,10i4))
c
      if(.not.drtprt) then
c        # print out the entire drt structure...
         levprt(1)=0
         levprt(2)=nact
         call prtdrt(levprt,nact,nrow,nsym,
     &    nj,njskp,modrt,syml,a,b,l,xbar,y,xp,z)
c
c        # ...and the reverse index array.
         write(nlist,6080) r
      endif
6080  format(/' r(*)',5(t8,10i5,10x,10i5/))
c
c     # change limvec(*) into skip/map form.
c     # on input: 0=deleted walk, 1=retained walk.
c     # on output: i>0 retained csf number i, i<0 then skip -i walks to
c     #            the next retained walk.  e.g.
c     # input:  1  0  1  1  0  0  1  1  1  0  0  0  1 ...
c     # output: 1 -1  2  3 -2 -1  4  5  6 -3 -2 -1  7 ...
c
      nx(0,0)=0
      nx(1,0)=1
      nx(0,1)=ncsf+1
      nx(1,1)=0
      do 2400 i = nwalk, 1, -1
         li=limvec(i)
         nx(0,0)=nx(li,0)-1
         nx(0,1)=nx(0,1)-li
         limvec(i)=nx(0,li)
2400  continue
c
      if ( nx(0,1) .ne. 1 ) then
         write(nlist,*)'rddrt: limvec(*) error'
         call bummer('rddrt: nx(0,1)=',nx(0,1),faterr)
      endif
c
      if ( .not. drtprt ) then
         write(nlist,6090) limvec
      endif
6090  format(/' ind(*)',5(t8,10i5,10x,10i5/))
c
c     # adjust orbtyp(*) for the active orbitals just read in.
c
      do 3100 i=1,nact
         j=modrt(i)
         occl(j)=i
         orbtyp(j)=actx(orbtyp(j))
3100  continue
c
c     # adjust orbtyp(*) for the inactive orbitals.
c
      do 3200 i=1,ndot
         j=doub(i)
         orbtyp(j)=doubx(orbtyp(j))
3200  continue
c
c     # orbtyp(*) is in its final form.  check for consistency.
c     # 0 = frozen core orbital.
c     # 1 = inactive orbital.
c     # 2 = active orbital.
c     # 3 = virtual orbital.
c     # 4 = frozen virtual orbital.
c     # 5 = error.
c
c-    write(nlist,6200)'orbtyp(*)',(orbtyp(i),i=1,nmot)
6200  format(1x,a/(5(1x,10i1)))
      do 3300 i=0,5
         ntypex(i)=0
3300  continue
      do 3400 i=1,nmot
         ntypex(orbtyp(i))=ntypex(orbtyp(i))+1
3400  continue
c-    write(nlist,6110)(i,ntypex(i),i=0,5)
6110  format(/' type:ntype',6(i8,':',i3))
c
      if(   (ntypex(5) .ne. 0)
     & .or. (ntypex(0)+ntypex(1) .ne. ndot)
     & .or. (ntypex(2) .ne. nact)           )then
         write(nlist,*)'rddrt: orbtyp(*) error.'
         call bummer('rddrt: orbtyp(*) error.',0,faterr)
      endif
c
      close(unit=ndrt)
c
      return
      end
c
c***********************************************************************
c
      subroutine rdhdrt(
     & title,  ndot,   nact,   nrow,
     & ssym,   smult,  nelt,   nela,
     & nwalk,  ncsf,   lenbuf, namomx,
     & slabel, ist)
c
c  read the header information from the drt file.
c
c  explicit variable declaration: 16-apr-02, m.dallos
c  28-sep-91 vrsion=2 support added. -rls
c  written by ron shepard.
c
       implicit none
      integer iunits
      common/cfiles/iunits(55)
      integer nlist,nslist,ndrt
      equivalence (iunits(1),nlist)
      equivalence (iunits(9),nslist)
      equivalence (iunits(17),ndrt)
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer lvlprt
      common/prtout/lvlprt
c
c    #  maximal No. of different DRTs
      integer maxnst
      parameter (maxnst=8)
c
c    #  maximal No. of states in one DRT
      integer mxavst
      parameter (mxavst = 50)
c
c    #  maximal total number of states
      integer maxstat
      parameter (maxstat = maxnst*mxavst)
c
      integer navst,nst,navst_max
      real*8 heig,wavst
      common /avstat/ wavst(maxnst,mxavst), heig(maxnst,mxavst),
     & navst(maxnst),nst,navst_max
c
      integer nxy,mult,nsym,nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
c
c  ##  integer section
c
      integer buf(50)
      integer ist,ierr,i
      integer lenbuf
      integer ndot, nact, nrow, ssym, smult, nelt, nela, nwalk, ncsf,
     &        namomx,nhdint,nsymd
      integer vrsion
c
c  ##  character section
c
      character*80 title
      character*60 fname
      character*4 slabel(20)
c
c---------------------------------------------------------------------
c
      if (nst.eq.1) then
       fname = 'mcdrtfl'
      elseif(ist.ge.10) then
       write(fname,'(a8,i2)')'mcdrtfl.',ist
      else
       write(fname,'(a8,i1)')'mcdrtfl.',ist
      endif
      call trnfln( 1, fname )
      open(unit=ndrt,file=fname,status='old')
c
      read(ndrt, '(a)', iostat=ierr) title
      if ( ierr .ne. 0 ) then
         call bummer('rdhdrt: title ierr=',ierr,faterr)
      endif
c
      write(nlist,6010)title
      write(nslist,6010)title
6010  format(/' DRT file header:'/1x,a)
c
c     # first header record.
      call rddbl( ndrt, 3, buf, 3 )
c
      vrsion = buf(1)
      lenbuf = buf(2)
      nhdint = buf(3)
c
      if ( vrsion .ne. 2 ) then
         write(nlist,*)
     &    '*** warning: rdhead: drt version .ne. 2 *** vrsion=',vrsion
      endif
c
c     # second header record.
      call rddbl( ndrt, lenbuf, buf, nhdint )
c
      ndot  = buf(1)
      nact  = buf(2)
      nsymd = buf(3)
      nrow  = buf(4)
      ssym  = buf(5)
      smult = buf(6)
      nelt  = buf(7)
      nela  = buf(8)
      nwalk = buf(9)
      ncsf  = buf(10)
c
      if (lvlprt.ge.1) then
      write(nlist,6100)
     & ndot, nact, nsymd, nrow, ssym, smult,
     & nelt, nela, nwalk, ncsf, lenbuf
      endif
6100  format(
     & ' ndot=  ',i6,4x,'nact=  ',i6,4x,'nsymd= ',i6,4x,'nrow=  ',i6/
     & ' ssym=  ',i6,4x,'smult= ',i6,4x,'nelt=  ',i6,4x,'nela=  ',i6/
     & ' nwalk= ',i6,4x,'ncsf=  ',i6,4x,'lenbuf=',i6)
c
      read(ndrt,7000)(slabel(i),i=1,nsym)
c
      write(nlist,'(1x,"Molecular symmetry group:",t30,a4)')
     & slabel(ssym)
      write(nlist,'(1x,"Total number of electrons:",t29,i4)')nelt
      write(nlist,'(1x,"Spin multiplicity:",t30,i3)')smult
      write(nlist,'(1x,"Number of active orbitals:",t30,i3)')nact
      write(nlist,'(1x,"Number of active electrons:",t30,i3)')nela
      write(nlist,'(1x,"Total number of CSFs:",t26,i7)')ncsf
      write(nslist,'(1x,"Molecular symmetry group:",t30,a4)')
     & slabel(ssym)
      write(nslist,'(1x,"Total number of electrons:",t30,i4)')nelt
      write(nslist,'(1x,"Spin multiplicity:",t30,i3)')smult
      write(nslist,'(1x,"Number of active orbitals:",t30,i3)')nact
      write(nslist,'(1x,"Number of active electrons:",t30,i3)')nela
      write(nslist,'(1x,"Total number of CSFs:",t26,i7)')ncsf
c
      if ( nact .gt. namomx ) then
         write(nlist,6900)'nact,namomx', nact, namomx
         call bummer('rdhead: nact=',nact,faterr)
      elseif ( nsym .ne. nsymd ) then
         write(nlist,6900)'nsym,nsymd', nsym, nsymd
         call bummer('rdhead: nsymd=',nsymd,faterr)
      endif
c
      close (unit=ndrt)
c
      return
6900  format(/' error: ',a,2i10)
7000  format(/,20a4)
      end
      subroutine rdlv(i,n,na,a,alab,qfskip)
c
c  search unit i for label alab and read the corresponding vector.
c
c  i   = unit number.
c  n   = expected vector length.
c  na  = actual vector length.  na=-1 for unsatisfied search.
c  a(*)= if(na.le.n) the vector is returned.
c  alab= character label used to locate the correct record.
c  qfskip= .true. if first record should be skipped.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/cfiles/iunits(55)
      equivalence (iunits(1),nlist)
c
      logical qfskip
      real*8 a(n)
      character*(*) alab
c
      character*8 label
c
c  search for the correct label.
c
      na=0
      rewind i
      if(qfskip)read(i)
10    read(i,end=20)label,na
      if(alab.ne.label)then
         read(i)
         go to 10
      endif
c
c  ...correct label.  check length.
c
      if(na.ne.n)write(nlist,6010)alab,na,n
6010  format(/' rdlv: alab=',a,' na,n=',2i10)
      if(na.le.n)call seqr(i,na,a)
      return
c
c  ...unsatisfied search for alab.
20    write(nlist,6020)alab
6020  format(/' rdlv: record not found. alab=',a)
      na=-1
      return
      end
      subroutine reslvm(t,fdd,faa,qaa,qvv,d1,sa,sb,sc,sx,ia,ib,engy,den)
c
c subroutine modified to create information for mr mp pt
c  (modified to save and write to file orbital energies and density
c   elements) -eas 4/91
c
c  compute the transformation matrix to resolve the redundant rotation
c  variables of the mcscf wave function.
c
c  arguments:
c  t(*)  = transformation matrix.
c  fdd(*)= inactive orbtal fock matrix.
c  faa(*)= active orbital fock matrix.
c  qaa(*)= active orbital q matrix.
c  d1(*) = active orbital density matrix.
c  sa(*) = scratch matrix.
c  sb(*) = scratch matrix.
c  sc(*) = scratch matrix.
c  sx(*) = scratch vector.
c  ia(*) = integer scratch vector.
c  ib(*) = integer scratch vector.
c  engy(*) = calculated orbital energies
c  den(*)  = density matrix elements for engy elements
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      integer ia(*),ib(*)
      real*8 t(*),fdd(*),faa(*),qaa(*),qvv(*),d1(*)
      real*8 sa(*),sb(*),sc(*),sx(*)
      real*8 engy(*),den(*)
c
      common/cfiles/iunits(55)
      equivalence (iunits(1),nlist)
      equivalence (iunits(22),mrptfl)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nmpsy(8),nsm(8),n2sm(8)
      equivalence (nxy(1,5),nmpsy(1))
      equivalence (nxy(1,6),nsm(1))
      equivalence (nxy(1,8),n2sm(1))
      integer ndpsy(8),n2sd(8)
      equivalence (nxy(1,9),ndpsy(1))
      equivalence (nxy(1,12),n2sd(1))
      integer napsy(8),nsa(8),nnsa(8),n2sa(8)
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxy(1,14),nsa(1))
      equivalence (nxy(1,15),nnsa(1))
      equivalence (nxy(1,16),n2sa(1))
      integer nvpsy(8),n2sv(8)
      equivalence (nxy(1,17),nvpsy(1))
      equivalence (nxy(1,20),n2sv(1))
      integer ird1(8),ird2(8)
      equivalence (nxy(1,37),ird1(1))
      equivalence (nxy(1,38),ird2(1))
      integer ira1(8),ira2(8)
      equivalence (nxy(1,39),ira1(1))
      equivalence (nxy(1,40),ira2(1))
      integer irv1(8),irv2(8)
      equivalence (nxy(1,41),irv1(1))
      equivalence (nxy(1,42),irv2(1))
c
c
      integer nmotx
      parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx),invx(nmotx),fcimsk(nmotx)
      equivalence (iorbx(1,1),nndx(1))
      equivalence (iorbx(1,5),invx(1))
      equivalence (iorbx(1,8),fcimsk(1))
      equivalence (ix0(1),ix0d),(ix0(2),ix0a),(ix0(3),ix0v)
c
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
c
      real*8 one
      parameter(one=1d0)
      real*8 two
      parameter (two=2.0d0)
c
      do 700 isym=1,nsym
      nm=nmpsy(isym)
      if(nm.eq.0)go to 700
      x1y1=n2sm(isym)+1
c
c  initialize as a unit matrix.
c
      call wzero(nm*nm,t(x1y1),1)
      xx=x1y1
      do 110 x=1,nm
      t(xx)=one
110   xx=xx+nm+1
c
c**********************************************************************
c  inactive orbitals.
c**********************************************************************
c
c  transform to diagonalize the fdd(*) matrix.
c
      ni=ndpsy(isym)
      if(ni.ne.0)then
c
c  get mo labels for inactive orbitals.
c
      i1=ird1(isym)
      i2=ird2(isym)
      ix=0
      do 210 i=i1,i2
      ix=ix+1
210   ia(ix)=invx(ix0d+i)
c
c  store transformation coefficients in sb(*) and eigenvalues in sc(*).
c
      call dcopy_wr(ni*ni,fdd(n2sd(isym)+1),1,sb,1)
      call eigip(ni,ni,sb,sc,sx)
c
c  # save eigenvalues and implied double occupancy
c
      do 1200 imo = 1,ni
         engy(ia(imo)+nsm(isym)) = sc(imo)
         den (ia(imo)+nsm(isym)) = 2.0d0
 1200 continue
c
      write(nlist,6020)isym,(sc(i),i=1,ni)
6020  format(/' fdd(*) eigenvalues. symmetry block',i3/(1x,10f12.6))
c
c  form t' = t * u.  since t=1 for these orbitals, just copy.
c
      ji=0
      do 220 i=1,ni
      mi=ia(i)
      do 220 j=1,ni
      ji=ji+1
      mj=ia(j)
      mji=n2sm(isym)+(mi-1)*nm+mj
      t(mji)=sb(ji)
220   continue
c
      endif
c
c**********************************************************************
c  active orbitals.
c**********************************************************************
c
c  loop over invariant subspaces, mask out resolution type, and update
c  t(*).  note that for cases where an orbital belongs to more than one
c  subspace that the final results may depend on the order of the
c  applied transformations.
c
      np=napsy(isym)
      if(np.ne.0)then
      p1=ira1(isym)
      p2=ira2(isym)
c
c # save diagonal elements of qaa and d1 for all isym orbitals here
c # invariant subspace changes will be replaced as created
c
      ix = 0
      do 1300 imo=p1,p2
       ix = ix + 1
c following is for faa
c       engy(invx(ix0a+imo)+nsm(isym))=faa(n2sa(isym) + np*(ix-1)+ix)
c correct for qaa
       engy(invx(ix0a+imo)+nsm(isym))=qaa(nnsa(isym) + (ix*(ix+1))/2)
       den(invx(ix0a+imo)+nsm(isym))=d1(nnsa(isym) + (ix*(ix+1))/2)
 1300 continue
c
      do 500 field=1,8
      power=field-1
c
c**********************************************************************
c  type=2, natural orbital resolution.
c**********************************************************************
c
      npi=0
      do 310 p=p1,p2
      pmo=invx(ix0a+p)
      pmask=fcimsk(pmo+nsm(isym))
      pfield=mod(pmask/10**power,10)
      if(pfield.eq.2)then
         npi=npi+1
         ia(npi)=pmo
         ib(npi)=p-nsa(isym)
      endif
310   continue
c
      if(npi.ne.0)then
c
c  copy the lower triangle of the density matrix.
c
      do 320 pi=1,npi
      p=ib(pi)
      do 320 qi=1,pi
      q=ib(qi)
      pq=nnsa(isym)+nndx(p)+q
      pq2=(qi-1)*npi+pi
      sb(pq2)=d1(pq)
320   continue
c
c  diagonalize d(*).
c  vectors are returned in sb(*), occupations in sc(*).
c
      call eigip(npi,npi,sb,sc,sx)
c
c  copy t(*) columns to sa(*).
c
      xp=1
      do 330 pi=1,npi
      pmo=ia(pi)
      yp=(pmo-1)*nm+x1y1
      call dcopy_wr(nm,t(yp),1,sa(xp),1)
330   xp=xp+nm
c
c  transform. t' = t * u
c  store in sc(*).
c
      call mxma(sa,1,nm, sb,1,npi, sc,1,nm, nm,npi,npi)
c
c  copy back to t(*).
c  reverse the order to have higher occupations first.
c
      xp=1
      do 340 pi=npi,1,-1
      pmo=ia(pi)
      yp=(pmo-1)*nm+x1y1
      call dcopy_wr(nm,sc(xp),1,t(yp),1)
340   xp=xp+nm
c
      endif
c
c**********************************************************************
c  type=3, faa(*) resolution.
c**********************************************************************
c
      npi=0
      do 350 p=p1,p2
      pmo=invx(ix0a+p)
      pmask=fcimsk(pmo+nsm(isym))
      pfield=mod(pmask/10**power,10)
      if(pfield.eq.3)then
         npi=npi+1
         ia(npi)=pmo
         ib(npi)=p-nsa(isym)
      endif
350   continue
c
      if(npi.ne.0)then
c
c  copy the lower triangle of the faa(*) matrix.
c
      do 360 pi=1,npi
      p=ib(pi)
      do 360 qi=1,pi
      q=ib(qi)
      pq=n2sa(isym)+(p-1)*np+q
      pq2=(qi-1)*npi+pi
      sb(pq2)=faa(pq)
360   continue
c
c  diagonalize faa(*).
c  vectors are returned in sb(*), eigenvalues in sc(*).
c
      call eigip(npi,npi,sb,sc,sx)
c
c  copy t(*) columns to sa(*).
c
      xp=1
      do 370 pi=1,npi
      pmo=ia(pi)
      yp=(pmo-1)*nm+x1y1
      call dcopy_wr(nm,t(yp),1,sa(xp),1)
370   xp=xp+nm
c
c  transform. t' = t * u
c  store in sc(*).
c
      call mxma(sa,1,nm, sb,1,npi, sc,1,nm, nm,npi,npi)
c
c  copy back to t(*).
c
      xp=1
      do 380 pi=1,npi
      pmo=ia(pi)
      yp=(pmo-1)*nm+x1y1
      call dcopy_wr(nm,sc(xp),1,t(yp),1)
380   xp=xp+nm
c
      endif
c
c**********************************************************************
c  type=4, qaa(*) resolution.
c**********************************************************************
c
      npi=0
      do 390 p=p1,p2
      pmo=invx(ix0a+p)
      pmask=fcimsk(pmo+nsm(isym))
      pfield=mod(pmask/10**power,10)
      if(pfield.eq.4)then
         npi=npi+1
         ia(npi)=pmo
         ib(npi)=p-nsa(isym)
      endif
390   continue
c
      if(npi.ne.0)then
c
c  copy the lower triangle of the qaa(*) matrix.
c
      do 400 pi=1,npi
      p=ib(pi)
      do 400 qi=1,pi
      q=ib(qi)
      pq=nnsa(isym)+nndx(p)+q
      pq2=(qi-1)*npi+pi
      sb(pq2)=qaa(pq)
400   continue
c
c  diagonalize qaa(*).
c  vectors are returned in sb(*), eigenvalues in sc(*).
c
      call eigip(npi,npi,sb,sc,sx)
c
c    # save eigenvalues - need to verify density elements
c
      do 1410 pi=1,npi
        engy(ia(pi)+nsm(isym)) = sc(pi)
        write(nlist,*)'i,qaaresolved',pi,sc(pi)
 1410 continue
c
c  copy t(*) columns to sa(*).
c
      xp=1
      do 410 pi=1,npi
      pmo=ia(pi)
      yp=(pmo-1)*nm+x1y1
      call dcopy_wr(nm,t(yp),1,sa(xp),1)
410   xp=xp+nm
c
c  transform. t' = t * u
c  store in sc(*).
c
      call mxma(sa,1,nm, sb,1,npi, sc,1,nm, nm,npi,npi)
c
c  copy back to t(*).
c
      xp=1
      do 420 pi=1,npi
      pmo=ia(pi)
      yp=(pmo-1)*nm+x1y1
      call dcopy_wr(nm,sc(xp),1,t(yp),1)
420   xp=xp+nm
c
      endif
c
500   continue
c
      endif
c
c**********************************************************************
c  virtual orbitals.
c**********************************************************************
c
c  transform to diagonalize the qvv(*) matrix.
c
      na=nvpsy(isym)
      if(flags(19).and.(na.ne.0))then
c
c  get the mo labels for the virtual orbitals.
c
      a1=irv1(isym)
      a2=irv2(isym)
      ax=0
      do 610 a=a1,a2
      ax=ax+1
610   ia(ax)=invx(ix0v+a)
c
c  store transformation coefficients in sb(*) and eigenvalues in sc(*).
c
      call dcopy_wr(na*na,qvv(n2sv(isym)+1),1,sb,1)
      call eigip(na,na,sb,sc,sx)
c
      write(nlist,6030)isym,(sc(i),i=1,na)
6030  format(/' qvv(*) eigenvalues. symmetry block',i3/(1x,10f12.6))
c
c  # save eigenvalues and implied virtual occupancy
c
      do 1400 imo = 1,na
         engy(ia(imo)+nsm(isym)) = sc(imo)
         den (ia(imo)+nsm(isym)) = 0.0d0
 1400 continue
c
c  form t' = t * u.  since t=1 for these orbitals, just copy.
c
      ba=0
      do 620 a=1,na
      ma=ia(a)
      do 620 b=1,na
      mb=ia(b)
      ba=ba+1
      mba=n2sm(isym)+(ma-1)*nm+mb
      t(mba)=sb(ba)
620   continue
c
      endif
c
700   continue
c
c     # write out the mp vectors
c

c      call plblks('d1 matrix',d1,nsym, napsy,'amo',1,nlist)
c      call prblks('faa matrix',faa,nsym,napsy,napsy,
c     +         'amo','amo',1,nlist)
c      call plblks('qaa matrix',qaa,nsym,napsy,'amo',1,nlist)
c
      open (unit=mrptfl,file='mrptfl',form='formatted',status='unknown')
      write (mrptfl,*) '4 compact or full orbital order'
      write (mrptfl,*) '(3d25.12)'
      mpmot = 0
      do 1490 isym=1,nsym
         mpmot = mpmot + nmpsy(isym)
 1490 continue
      write (mrptfl,*) mpmot
      do 1500 isym=1,nsym
        do 1510 i=1,nmpsy(isym)
        write (mrptfl,'(3d25.12)') engy(i+nsm(isym))/two,
     &   den(i+nsm(isym))
 1510   continue
 1500 continue
      close(unit=mrptfl)
c
      return
      end
c
c****************************************************************
c
      subroutine restrt(
     & nsym,    ninf,    nmot,    nbmt,
     & nmpsy,   nbpsy,   ntitle,  qconvg,
     & title,   coninf,  cr,      cno,
     & occ,     cn,      hvec,    cl)

ctk  adapted ... writing simulated state averaging info into the
ctk  restart file to satisfy CIGRD requirements... just a higly
ctk  temporary solution :)))
c  write out the restart information to unit nrestp.
c
c modified by Agnes Chang to include the morbl file in the
c restart file
c  modified by: michal dallos
c  version date: 16-jan-2001
c
       implicit none
c  ##  parameter & commne section
c
      integer ntol
      parameter (ntol=4)
c
c    #  maximal No. of different DRTs
      integer maxnst
      parameter (maxnst=8)
c
c    #  maximal No. of states in one DRT
      integer mxavst
      parameter (mxavst = 50)
c
c    #  maximal total number of states
      integer maxstat
      parameter (maxstat = maxnst*mxavst)
c
      real*8 tol,rtolci
      common/toler/tol(12),rtolci(mxavst)
c
      integer iunits
      common/cfiles/iunits(55)
      integer nlist,nrestp
      equivalence (iunits(1),nlist)
      equivalence (iunits(12),nrestp)
c
      integer nrow_f,ncsf_f,nwalk_f,ssym
      common/drtf/ nrow_f(maxnst),ncsf_f(maxnst),
     & nwalk_f(maxnst),ssym(maxnst)
c
c     counter.h
c
c     cpt2(ist+1) = cpt2(ist) + forbyt(nsym*nrow_f(ist))
c     cpt3(ist+1) = cpt3(ist) + forbyt(nwalk_f(ist))
c     cpt4(ist+1) = cpt4(ist) + atebyt(ncsf_f(ist))
c     cpt2_tot = cpt2_tot + nsym*nrow_f(ist)
c     cpt3_tot = cpt3_tot + nwalk_f(ist)
c     cpt4_tot = cpt4_tot + ncsf_f(ist)*navst_(ist)
c
      integer cpt2,cpt2_tot,cpt3,cpt3_tot,cpt4,cpt4_tot
      integer ncsf_max
      common/counter/cpt2(maxnst+1),cpt3(maxnst+1),cpt4(maxnst+1),
     & ncsf_max,cpt2_tot,cpt3_tot,cpt4_tot
c
      integer navst,nst,navst_max
      real*8 heig,wavst,norm
      common /avstat/ wavst(maxnst,mxavst), heig(maxnst,mxavst),
     & navst(maxnst),nst,navst_max
c
c  ##  integer section
c
      integer i,j
      integer lenbuf_f(maxnst)
      integer maxnavst,nbmt,nmpsy(8),nbpsy(8)
      integer ntitle,ninf,nmot,nsym
c
c  ##  real*8 section
c
      real*8 cn(nbmt),cl(nbmt),coninf(ninf),cr(nbmt),cno(nbmt)
      real*8 hvec(cpt4_tot)
      real*8 occ(nmot)
c
c  ##  character section
c
      character*80 title(ntitle)
      character*8 label
c
c  ##  logical section
c
      logical qconvg

c
c-------------------------------------------------------------
c
c    ## first calculate the maximum number of states in one drt
      maxnavst = 0
      do i=1,nst
         if ( maxnavst.lt.navst(i))  maxnavst = navst (i)
      enddo
c
c  write out header info:
c
      rewind nrestp
      write(nrestp)
     &   nsym,ninf,ntol,nmot,nbmt,nst,
     &  (ncsf_f(i),i=1,nst),qconvg,ntitle,
     +  nmpsy,nbpsy,title,
     &  (navst(i),i=1,nst),maxnavst,
     &  ((wavst(i,j),i=1,nst),j=1,maxnavst),
     &  ((heig(i,j),i=1,nst),j=1,maxnavst)
c
c  write out convergence info:
c  this array contains: emc, repnuc, demc, wnorm, knorm, apxde.
c
      label='coninf'
      write(nrestp)label,ninf
      write(nrestp)coninf
c
c  write out convergence criteria:
c
      label='tol'
      write(nrestp)label,ntol
      write(nrestp)tol
c
c  write out current orbitals.
c
      label='morb'
      write(nrestp)label,nbmt
      write(nrestp)cr
c
c  write out current natural orbitals and occupation numbers.
c
      label='norb'
      write(nrestp)label,nbmt
      write(nrestp)cno
      label='nocc'
      write(nrestp)label,nmot
      write(nrestp)occ
c
c  write out mcscf orbitals for the next iteration.
c
      label='morbn'
      write(nrestp)label,nbmt
      write(nrestp)cn
ca
ca write out mcscf orbitals for the last iteration,
ca ie. before resolution, and from which hessian and density
ca matrices were computed.
ca
      label='morbl'
      write(nrestp)label,nbmt
      write(nrestp)cl
c
c  write out all hmc(*) eigenvectors of states used in averaging
c
      label='hvec'
      write(nrestp)label, cpt4_tot
      write(nrestp)hvec
c
c  all done.
c
      rewind nrestp
      write(nlist,6010)nrestp
6010  format(/' restrt: restart information saved on the restart file',
     +  ' (unit=',i3,').')
      return
      end
c
c*************************************************************
c
      subroutine scmprs(m,ndimr,ncfx,noldr,nmaxr,
     +  mr,cr,pr,t,scr,s,cxs,mxs)
c
c  compress the current expansion space spanned by the columns
c  of s(*,1:m) to a subspace of dimension (m-1).  the method used
c  is to delete the component of the last trial vector s(*,m) that
c  is orthogonal to the last approximate p vector, where p=s*pr.
c
c  input:
c  m     = number of columns of s(*) in the calling program.
c  ndimr = number of rows of cxs(*) in the calling program.
c  ncfx  = number of rows of s(*) in the calling program.
c  noldr = number of rows of cr(*) to transform.
c  nmaxr = dimension for cr(*) matrix.
c  mr    = subspace m matrix. mr( m*(m+1)/2 )
c  cr    = subspace c matrix. cr(nmaxr,m)
c  pr    = subspace p vector. p = s * pr
c  scr   = scratch. scr( m*(m+1)/2 )
c  s     = subspace expansion vectors. s(ncfx,m)
c  cxs   = cxs matrix-vector products. cxs(ndimr,m)
c  mxs   = mxs matrix-vector products. mxs(ncfx,m)
c
c  output:
c  s(*,*), cxs(*,*), mxs(*,*) and all representations are updated.
c  t     = transformation matrix. t(m,m-1) where s' = s * t.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      real*8 mr(*),cr(*),pr(m),t(m,*),scr(*)
      real*8 s(*),cxs(*),mxs(*)
c
      real*8 dnrm2_wr
      real*8 one,term
      parameter(one=1d0)
c
      mm1=m-1
      nnmm1=(m*mm1)/2
c
c  form the transformation matrix.
c
      call wzero(m*mm1,t,1)
c
      term=pr(m)/dnrm2_wr(mm1,pr,1)**2
c
      do 100 i=1,mm1
      t(i,i)=one
100   t(m,i)=term*pr(i)
c
      call orthot(m,mm1,t,0)
c
c  transform the basis vectors s' = s * t.
c
      call maaxb(s,1,ncfx, t,1,m, scr,1, ncfx,m,mm1)
c
c  transform cxs' = cxs * t.
c
      call maaxb(cxs,1,ndimr, t,1,m, scr,1, ndimr,m,mm1)
c
c  transform mr' = t(transpose) * mr * t.
c
      call uthu(t,mr,scr,scr(nnmm1+1),m,mm1)
      call dcopy_wr(nnmm1,scr,1,mr,1)
c
c  transform cr' = cr * t.
c
      if(noldr.ne.0)call maaxb(cr,1,nmaxr, t,1,m, scr,1, noldr,m,mm1)
c
c  transform pr' = t(transpose) * pr.
c
      call mxma(t,m,1, pr,1,1, scr,1,1, mm1,m,1)
      call dcopy_wr(mm1,scr,1,pr,1)
c
      return
      end
      subroutine sdas(core,buf,lastb,buk,ibuk)
c
c  bookeeping for second-half integral sort.
c  if buf(*) is full, it is dumped.
c  if core(*) is empty, it is filled.
c
c  input:
c  buf(*)  = current buffer space for sorted integrals.
c            buf(1) is equivalenced to core(ibpt) by call.
c  core(*) = workspace of total length equal to avc2+lenbfs.
c  nterm   = number of terms for which the attempt to increment
c            buffer pointers failed.
c  add0    = integral addressing offset.
c  ibpt    = first location in core(*) for current buffer.
c  ic0     = running core(*) offset.
c  icmax   = address in core(*) of last integral.
c  ic0mx   = maximum ic0 value before this routine is called again.
c  avc2    = number of integrals held in each da chain.
c
c  output:
c  add0,ibpt,ic0,icmax,and ic0mx are appropriately updated.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/cfiles/iunits(55)
      equivalence (iunits(1),nlist)
c
      common/cis2/add0,ibpt,ic0,icmax,ic0mx,avc2,nterm,ibk,lenbuk,npbuk
c
      common/cbufs/stape,lenbfs,h2bpt
c
      real*8 core(*),buf(lenbfs),buk(lenbuk)
      integer ibuk(npbuk),lastb(*)
c
c  bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
c  check buffer space:
c
      if(ic0+nterm .le. ibpt+lenbfs-1)go to 100
c
c  not enough space is left in the buffer.  dump buffer and
c  adjust pointers.
c
      write(stape)buf
      ibpt=ic0+1
c
c  check to make sure output buffer length is sufficient.
c
      if(nterm.gt.lenbfs)then
         write(nlist,6010)nterm,lenbfs
         write(nlist,*)'*** Increase lenbfs in mcscfin ***'
         call bummer('sdas: insufficient buffer length, nterm=',
     &    nterm,faterr)
      endif
c
c  space for a full buffer must remain in core(*).
c
      if(ic0.le.avc2)go to 100
c
c  copy remaining integrals to the beginning of core(*) and
c  adjust pointers.
c
      ncopy=icmax-ic0
      call dcopy_wr(ncopy,core(ibpt),1,core(1),1)
      ic0=0
      ibpt=1
      icmax=ncopy
c
100   continue
c
c  check the number of integrals in core:
c
      if(ic0+nterm.le.icmax)go to 3000
c
c  core(*) has run out of integrals.  copy all the integrals in the
c  current buffer space to the beginning of core(*) and read in the
c  next set of chained da records.
c
      if(ibpt.eq.1)go to 1000
      ncopy=icmax-ibpt+1
      call dcopy_wr(ncopy,core(ibpt),1,core(1),1)
      ic0=ic0-ibpt+1
      ibpt=1
      icmax=ncopy
c
1000  ibk=ibk+1
      next=lastb(ibk)
      call wzero(avc2,core(icmax+1),1)
      a0=add0-icmax
c
      call rdbks0(next,core,buk,ibuk,lenbuk,npbuk,a0)
c
      add0=add0+avc2
      icmax=icmax+avc2
c
3000  continue
      ic0mx=min(ibpt+lenbfs-1,icmax)
c
      return
6010  format(/' sdas: error. nterm,lenbfs=',2i10)
      end
c
c***********************************************************************
c
      subroutine solvek(
     & avcsk,    core,    nadt,    naar,
     & nvdt,     nvat,    ndimr,   iorder,
     & add,      wnorm,   w,       hmcvec,
     & k,        knorm,   apxde,   cmo,
     & d1,       bdg2e,   fva,     qva,
     & fad,      qad,     fdd,     qvv,
     & idim,     mxcsao,  lenbuf,  modrt,
     & xp,       z,       xbar,    rev,
     & ind,      lenbuk,  nipbuk,  lastb,
     & uaa,      uad,     uvd,     uva,
     & qcoupl)
c
c  set parameters and allocate workspace for
c  the solution of the orbital correction vector, k(*).
c
c  argument list:
c  avcsk     = available core.
c  core(*)   = workspace array.
c  nadt      = number of active-inactive rotations.
c  naar      = number of active-active rotations.
c  nvdt      = number of virtual-inactive rotations.
c  nvat      = number of virtual-active rotations.
c  add(*)    = off** and add** arrays.
c  emc       = current mcscf energy.
c  wnorm     = w(*) vector norm.
c  w(*)      = gradient vector (g.b.t. terms).
c  hmcvec(*) = current hmc eigenvector.
c  k(*)      = orbital correction vector.
c  knorm     = norm of the k(*) vector.
c  apxde     = approximate energy lowering next mcscf iteration.
c
c  written by ron shepard.
c  version date: 07-mar-85
c
cmb  state mixing introduced - parameter navst added
cmb  necessary for the construcion of the "m"-hessian block - all
cmb  eigenvalues passed (in heig); make the impression, we have a
cmb  (ncsf*navst)**2 ci problem, not a ncsf**2 one. this will be passed
cmb  to micro, which only for projection reasons must know about the
cmb  averaging... - tom kovar may-90
cmb

       implicit none
c  ##  parameter & common block
c
c
c    #  maximal No. of different DRTs
      integer maxnst
      parameter (maxnst=8)
c
c    #  maximal No. of states in one DRT
      integer mxavst
      parameter (mxavst = 50)
c
c    #  maximal total number of states
      integer maxstat
      parameter (maxstat = maxnst*mxavst)
c
      integer nxy, mult, nsym, nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nbpsy(8)
      equivalence (nxy(1,1),nbpsy(1))
      integer nmpsy(8)
      equivalence (nxy(1,5),nmpsy(1))
      integer ndpsy(8)
      equivalence (nxy(1,9),ndpsy(1))
      integer napsy(8)
      equivalence (nxy(1,13),napsy(1))
      integer nvpsy(8)
      equivalence (nxy(1,17),nvpsy(1))
      integer ird1(8),ird2(8)
      equivalence (nxy(1,37),ird1(1))
      equivalence (nxy(1,38),ird2(1))
      integer ira1(8),ira2(8)
      equivalence (nxy(1,39),ira1(1))
      equivalence (nxy(1,40),ira2(1))
c
      integer nbft
      equivalence (nxtot(1),nbft)
c
c  addpt(*) assignments:
c  1:dd,   2:dd2, 3:ad,   4:aa,   5:vd,   6:va,   7:vv,   8:vv2,
c  9:vv3, 10:o1, 11:o2,  12:o3,  13:o4,  14:o5,  15:o6,  16:o7,
c 17:o8,  18:o9, 19:o10, 20:o11, 21:o12, 22:o13, 23:o14, 24:(tot)
      integer addpt,numint,bukpt,intoff,szh
      common/caddb/addpt(24),numint(12),bukpt(12),intoff(12),szh(15)
c
      integer iunits
      common/cfiles/iunits(55)
      integer nlist, cfile, nvfile
      equivalence (iunits(1),nlist)
      equivalence (iunits(8),nvfile)
      equivalence (iunits(23),cfile)
c
      integer niter, nstate, ncol, ncoupl, noldv, noldhv, nunitv,
     &        nciitr, mxvadd, nvcimx, nmiter, nvrsmx
      common /inputc/niter,nstate(8),ncol(8),ncoupl,
     +  noldv(8),noldhv(8),nunitv,nciitr,mxvadd,nvcimx,nmiter,nvrsmx
c
      integer nflag
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
c
      real*8 tol,rtolci
      common/toler/tol(12),rtolci(mxavst)
c
      real*8 two,eight,zero
      parameter(two=2d0,eight=8d0,zero=0d0)
c
c
      integer nmotx
      parameter (nmotx=1023)

      real*8 bdiagthr
      parameter (bdiagthr=1.d-8)
c
*@ifdef direct
*      include 'param.h'
*      integer ifock, ipq
*      common /fockij/ ifock(ndi4+1)
*      common /pairij / ipq(ndi4+1)
*@else
c  bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
*@endif
c
      integer msize,csize
      common/size/ msize(maxnst),csize(4,maxnst)
c
      integer navst,nst,navst_max
      real*8 heig,wavst,norm
      common /avstat/ wavst(maxnst,mxavst), heig(maxnst,mxavst),
     & navst(maxnst),nst,navst_max
c
      integer nrow_f,ncsf_f,nwalk_f,ssym
      common/drtf/ nrow_f(maxnst),ncsf_f(maxnst),
     & nwalk_f(maxnst),ssym(maxnst)
c
      integer hblkw, nhbw, htape
      common/chbw/hblkw(15*maxstat),nhbw,htape
c
c  ##  integer section
c
      integer add(*), avcsk
      integer bpt
      integer cpt(29)
      integer event
      integer iorder(*), imd, ind(*), ib(15), ist, ib_m(maxstat),
     &        ib_c(4,maxstat), ist_m, ist_c(4), i, ii, ixx, ij, il,
     &        icpt, idim, ipt, isize, iblk, icount, ipos, icount2
      integer j
      integer lastb(*), lenbuk, lenbuf
      integer modrt(0:*), mnmxrs, mxrs2, mxrs1, mxcsao, mxrsx,
     &        mxrs
      integer ndims, nstat_min, nmaxr, nmaxs, nnewr, nncfx, nipbuk,
     &        noldr, nndxf, ncsfx, nnews, nolds, ns, nr, nadt,
     &        nvdt, naar, ndimr, nvat, nrx
      integer req
      integer sz, sizeb
c
c  ##  real*8 section
c
      real*8 apxde
      real*8 bdg2e(*)
      real*8 cmo(*), core(avcsk)
      real*8 d1(*)
      real*8 eshsci
      real*8 fva(*), fad(*), fdd(*)
      real*8 qva(*), qad(*), qvv(*)
      real*8 hmcvec(*)
      real*8 k(*), knorm
      real*8 pnorm
      real*8 rev(*), rtol
      real*8 uaa(*),uvd(*),uad(*),uva(*)
      real*8 w(*), wnorm
      real*8 xp(*), xbar(*)
      real*8 z(*)
c
c  ##  character section
c
      character*60 fname
c
c  ##  logical section
c
      logical qpcon, qcoupl
c
c  ##  external section
c
      integer  forbyt, atebyt
      external forbyt, atebyt
c
c---------------------------------------------------------------------
c
      nndxf(i)=(i*(i-1))/2
c
      nncfx = 0
      if (qcoupl) then
        do ist= 1, nst
        nncfx = nncfx + navst(ist)*ncsf_f(ist)
        enddo
      endif
c
      if(flags(3))call timer(' ',1,event,nlist)
c
c  begin allocation of core arrays.
c  core: 1:b
c
      cpt(1)=1
c
c  read in blocks of the hessian matrix from the external file
c  and set pointers, ib(*). ib(i)=0 means that hessian block does
c  not exist in memory.
c
      do 10 i=1,15
10    ib(i)=0
      do i=1,maxstat
      ib_m(i) = 0
         do j=1,4
         ib_c(j,i) = 0
         enddo
      enddo
c
      ist_m = 1
      do i=1,4
      ist_c(i) = 1
      enddo
c
      rewind htape
      ipt=1
      do 100 i=1,nhbw
         iblk=hblkw(i)
         if (iblk.eq.15) go to 70
         if (iblk.gt.10) go to 50
         isize=szh(iblk)
         if(ipt+isize .gt. avcsk)then
            write(nlist,6040)i,iblk,isize,avcsk
            write(nlist,
     &       '("requested mem./available mem.: ",i10," / ",i10)')
     &       ipt+isize,avcsk
            call bummer('solvek: 1 avcsk=',avcsk,faterr)
         endif
         ib(iblk)=ipt
         call rdhb(isize,core(cpt(1)-1+ipt),iblk)
         go to 100
c
 50      continue
         isize=csize(iblk-10,ist_c(iblk-10))
         if(ipt+isize .gt. avcsk)then
            write(nlist,6040)i,iblk,isize,avcsk
            write(nlist,
     &       '("requested mem./available mem.: ",i10," / ",i10)')
     &       ipt+isize,avcsk
            call bummer('solvek: 1 avcsk=',avcsk,faterr)
         endif
         ib_c(iblk-10,ist_c(iblk-10))=ipt
         call rdhb(isize,core(cpt(1)-1+ipt),iblk)
         ist_c(iblk-10) = ist_c(iblk-10) + 1
         go to 100
c
cmb
cmb      remember for block 15, hmc was written only once -
cmb      let's expand
 70      continue
         isize=msize(ist_m)
         if(ipt+isize .gt. avcsk)then
            write(nlist,6040)i,iblk,isize,avcsk
            write(nlist,
     &       '("requested mem./available mem.: ",i10," / ",i10)')
     &        ipt+isize,avcsk
            call bummer('solvek: 1 avcsk=',avcsk,faterr)
         endif
         ib_m(ist_m) = ipt

         call wzero(isize,core(cpt(1)-1+ipt),1)
         call  rdhb((ncsf_f(ist_m)*(ncsf_f(ist_m)+1))/2,
     &     core(cpt(1)-1+ipt),iblk)

         ist_m = ist_m + 1
 100   ipt=ipt+isize
c100   continue
      sizeb=ipt-1
c-    write(nlist,6030)ib
c
c  ##
c  ## the csf-csf hessian block, m(*), must be constructed from the
c  ## hmc(*) matrix.
c  ##  m(ij) = 2*(h(ij)-emc*delta(ij))
c  ##
c
c
      do ist = 1,nst
c
       if(ib_m(ist).ne.0) then
        ipos = cpt(1)-1+ib_m(ist)
        ncsfx = ncsf_f(ist)*(ncsf_f(ist)+1)/2
        call dscal_wr(ncsfx,two,core(ipos),1)
c
c  #  copy the H matrix into all navst(ist) blocks for the
c  #  DRT Nr. ist
        icount = ncsfx
           do i=2,navst(ist)
           call dcopy_wr(ncsfx,core(ipos),1,core(ipos+icount),1)
           icount = icount + ncsfx
           enddo ! i=1,navst(ist)
c
c  #  weight the individual blocks for the DRT Nr. ist
        icount = 0
           do i=1,navst(ist)
           call dscal_wr(ncsfx,wavst(ist,i),core(ipos+icount),1)
           icount = icount + ncsfx
           enddo ! i=1,navst(ist)
c
c  #  substract two*heig(ist,i)*wavst(ist,i) from the diagonal
        icount2 = 0
        icount = 0
           do i=1,navst(ist)
              do j=1,ncsf_f(ist)
              icount = icount + j
              core(ipos-1+icount)=core(ipos-1+icount)-
     &         two*heig(ist,i)*wavst(ist,i)
              enddo ! j=1,ncsf_f(ist)
            if (flags(8)) then
             write(nlist,
     &        '(/2x,''M matrix DRT Nr. '',i2,2x,''state Nr. '',i3)')
     &        ist,i
             call plblkt('M matrix ',core(ipos+icount2),ncsf_f(ist),
     &        ' row',1,nlist)
             icount2 = icount2 + ncsf_f(ist)*(ncsf_f(ist)+1)/2
            endif ! (flags(8))
           enddo ! i=1,navst(ist)
       endif ! (ib_m(ist).ne.0)
      enddo ! ist = 1,nst
c
c  get diagonal elements of the b(*) matrix and the m(*) matrix.
c  core: 1:b,2:bdiag,3:mdiag
ctm 
c  add checks for vanishing bmat elements 
c     this will cause the linear equation solver  in cigrad 
c     failing to converge
c
      cpt(2)=cpt(1)+sizeb
      if(cpt(2)+ndimr.gt.avcsk)then
         write(nlist,6050)cpt(1),cpt(2),ndimr
         write(nlist,
     &   '("requested mem./available mem.: ",i10, " / ",i10)')
     &   cpt(2)+ndimr, avcsk
         call bummer('solvek: 2 avcsk=',avcsk,faterr)
      endif
c
      if (flags(6)) then
      write(nlist,*)
      write(nlist,*)'Diagonal elements of B-matrix:'
      write(nlist,*)
      endif
c
      ipt=cpt(2)
      bpt=cpt(1)-1+ib(1)
      if(ib(1).ne.0) then
       call dxyxy(core(bpt),core(ipt),add(addpt(22)),
     &  nsym,ira1,ira2,napsy,ndpsy)
         do i=ipt,ipt+nadt-1
           if (abs(core(i)).lt.bdiagthr) then
             write(nlist,1001) 'badad:',i,core(i),bdiagthr
1001    format('WARNING presumably (nearly) redundant ' //
     &         'orbital rotations',/
     &   a,'element ',i4,'=',f18.12,'.lt.',f18.12)
           endif
         enddo
         if (flags(6)) then
         write(nlist,*)'B(ad,ad) diagonal elements:'
         write(nlist,1000)(core(i),i=ipt,ipt+nadt-1)
         write(nlist,*)
         endif
      endif
c
      ipt=ipt+nadt
      bpt=cpt(1)-1+ib(3)
      if(ib(3).ne.0) then
       call dspv(naar,core(bpt),core(ipt))
         do i=ipt,ipt+naar-1
           if (abs(core(i)).lt.bdiagthr) then
             write(nlist,1001) 'baaaa:',i,core(i),bdiagthr
           endif
         enddo

         if (flags(6)) then
         write(nlist,*)'B(aa,aa) diagonal elements:'
         write(nlist,1000)(core(i),i=ipt,ipt+naar-1)
         write(nlist,*)
         endif
      endif
c
      ipt=ipt+naar
      bpt=cpt(1)-1+ib(6)
      if(ib(6).ne.0) then
         if (flags(22)) then
*@ifdef direct
*           call bdiag1e(fdd,qvv,core(ipt))
*           do i=1,nvdt
*              core(ipt+i-1)=core(ipt+i-1)+4.0d+00*bdg2e(i)
*           enddo
*@endif
         else
           call dxyxy(core(bpt),core(ipt),add(addpt(20)),
     +      nsym,ird1,ird2,ndpsy,nvpsy)
         endif
         do i=ipt,ipt+nvdt-1
           if (abs(core(i)).lt.bdiagthr) then
             write(nlist,1001) 'bvdvd:',i,core(i),bdiagthr
           endif
         enddo

         if (flags(6)) then
         write(nlist,*)'B(vd,vd) diagonal elements:'
         write(nlist,1000)(core(i),i=ipt,ipt+nvdt-1)
         write(nlist,*)
         endif
      endif
c
      ipt=ipt+nvdt
      bpt=cpt(1)-1+ib(10)
      if(ib(10).ne.0) then
      call dxyxy(core(bpt),core(ipt),add(addpt(21)),
     +  nsym,ira1,ira2,napsy,nvpsy)
         do i=ipt,ipt+nvat-1
           if (abs(core(i)).lt.bdiagthr) then
             write(nlist,1001) 'bvava:',i,core(i),bdiagthr
           endif
         enddo

         if (flags(6)) then
         write(nlist,*)'B(va,va) diagonal elements:'
         write(nlist,1000)(core(i),i=ipt,ipt+nvat-1)
         write(nlist,*)
         endif
      endif
c
      cpt(3)=cpt(2)+ndimr
      if(cpt(3)+nncfx.gt.avcsk)then
         write(nlist,6060)(cpt(i),i=1,3),nncfx
         write(nlist,
     &    '("requested mem./available mem.: ",i10, " / ",i10)')
     &    cpt(3)+nncfx,avcsk
         call bummer('solvek: 3 avcsk=',avcsk,faterr)
      endif
c
c
      if ((.not.flags(11)).and.flags(12).and.qcoupl) then
c
         rewind iunits(20)
         ipt=cpt(3)
         do ist=1,nst
         read(iunits(20))(core(ipt+i-1),i=1,ncsf_f(ist))
         ipt = ipt + ncsf_f(ist)
         enddo
c
         if (flags(6).and.qcoupl) then
           do ist=1,nst
           write(nlist,*)'H matrix diagonal elements for the block: ',
     &         ist
           write(nlist,1000)(core(i),i=ipt,ipt+ncsf_f(ist)-1)
           write(nlist,*)
           ipt = ipt + ncsf_f(ist)
           enddo
          endif ! (flags(6).and.qcoupl)
c
      else ! ((.not.flags(11)).and.flags(12).and.qcoupl)
c
         ipt=cpt(3)
         do ist = 1,nst
         icount = 0
            do i=1,navst(ist)
             if (qcoupl) then
              call dspv(ncsf_f(ist),core(cpt(1)+ib_m(ist)-1+icount),
     &         core(ipt))
             endif ! (qcoupl)
            ipt = ipt + ncsf_f(ist)
            icount = icount + ncsf_f(ist)*(ncsf_f(ist)+1)/2
            enddo ! i=1,navst(ist)
         enddo ! ist = 1,nst
c
       if (flags(6).and.qcoupl) then
        ipt=cpt(3)
          do ist=1,nst
             do i=1,navst(ist)
             write(nlist,
     &        '(/2x,''Mdiag DRT Nr. '',i2,2x,''start Nr. '',i3)')
     &        ist,j
             call prtvec(nlist,'Mdiag',core(ipt),ncsf_f(ist))
             ipt = ipt + ncsf_f(ist)
             enddo ! i=1,navst(ist)
          enddo ! ist=1,nst
       endif ! (flags(6).and.qcoupl)
c
      endif ! ((.not.flags(11)).and.flags(12).and.qcoupl)
c
c
c  determine the number of expansion vectors r(*) and s(*) to
c  use in the iterative solution of the orbital correction vector.
c  minimum number of r(*) vectors is 2.  minimum number of s(*)
c  vectors is 2 plus one vector for each lower energy state in  hmc.
c
c  the optimal maximum number of vectors is somewhat machine dependent.
c  each iteration requires the computation of a set of matrix-vector
c  products and the manipulation of the trial vectors and subspace
c  representations.  the smaller the subspace dimensions, the less
c  effort is required for these manipulations.  however, the larger
c  the subspace dimensions the fewer iterations will be required and
c  the fewer matrix-vector products will be needed.
c
c  this version keeps nmaxs=nmaxr when qcoupl = .false.
c
cmb  from now on, instead of ncfx use nncfx = navst*ncfx_
c    calculated in the beginnig of the routine.
c
      ndims = 1
      if (qcoupl) ndims=max(1,nncfx)
      req=-1
          nstat_min = nstate(1)
          do ist = 2,nst
          if (nstat_min .gt. nstate(ist)) nstat_min = nstate(ist)
          enddo
      mnmxrs=nstat_min+2
cmd   mxrsx=min(nvrsmx,max(ncfx-1,ndimr))
      mxrsx=min(nvrsmx,max(nncfx-1,ndimr))
      mxrsx=max(2,mxrsx)
      nmaxr=0
      nmaxs=0
c
      do 210 nrx=mnmxrs,mxrsx
      nr=max(2,min(nrx,ndimr))
cmd   ns=max(0,min(nrx,ncfx-1))
      ns=max(0,min(nrx,nncfx-1))
      mxrs=max(nr,ns)
      mxrs1=max(nr+1,ns)
      mxrs2=mxrs1**2
c
      req=sizeb+ ndimr+ nncfx+ ns*nncfx+ nr*ndimr+
     +  nncfx+ nr*ndimr+ nncfx*nr+ ndimr*ns+ ns*nncfx+
     +  nndxf(nr+1)+ ns*nr+ nndxf(ns+1)+ nr+ mxrs2+
     +  mxrs1+ nndxf(mxrs+1)+ forbyt(ns)
      if(req.gt.avcsk)go to 220
c
c  save new parameters.
c
      nmaxr=nr
      nmaxs=ns
210   continue
220   continue
      if(nmaxr.eq.0)then
         write(nlist,6010)avcsk,req
         write(nlist,
     &    '("requested mem./available mem.: ",i10, " / ",i10)')
     &    req,avcsk
         call bummer('solvek: 4 avcsk=',avcsk,faterr)
      endif
c
c  initialize s(*) vectors.
c  core: 1:b,2:bdiag,3:mdiag,4:s
c
      cpt(4)=cpt(3)+nncfx
      nolds=0
      nnews=0
c
c  initialize r(*) vector: r(*,1)=w
c  core: 1:b,2:bdiag,3:mdiag,4:s,5:r
c
      cpt(5)=cpt(4)+nmaxs*nncfx
      icpt=cpt(5)
      call dcopy_wr(ndimr,w,1,core(icpt),1)
      noldr=0
      nnewr=1
      call orthot(ndimr,nnewr,core(cpt(5)),0)
c
c  allocate the other workspace arrays from core.
c  core: 1:b,2:bdiag,3:mdiag,4:s,5:r,6:p,7:bxr,8:cxr,9:cxs,10:mxs,
c       11:br,12:cr,13:mr,14:wr,15:zr,16:pr,17:scr,18:kpvt
c       19:scr1,20:scr2,21:scr3,22:f1,23:f2,24:f3,25:h2,26:buk,27:ibuk
c       28:rest_scr
c
      cpt(6)=cpt(5)+nmaxr*ndimr
      cpt(7)=cpt(6)+nncfx
      cpt(8)=cpt(7)+nmaxr*ndimr
      cpt(9)=cpt(8)+nncfx*nmaxr
      cpt(10)=cpt(9)+ndimr*nmaxs
      cpt(11)=cpt(10)+nncfx*nmaxs
      cpt(12)=cpt(11)+nndxf(nmaxr+1)
      cpt(13)=cpt(12)+nmaxr*nmaxs
      cpt(14)=cpt(13)+nndxf(nmaxs+1)
      cpt(15)=cpt(14)+nmaxr
      mxrs=max(nmaxr,nmaxs)
      mxrs1=max(nmaxr+1,nmaxs)
      mxrs2=mxrs1**2
      cpt(16)=cpt(15)+mxrs2
      cpt(17)=cpt(16)+mxrs1
      cpt(18)=cpt(17)+nndxf(mxrs+1)
      cpt(19)=cpt(18)+forbyt(nmaxs)
c
c    #
c    #  memory allocation for the direct Bxr
c    #
c
      if (flags(22)) then
      cpt(20)=cpt(19)+atebyt(idim)
      cpt(21)=cpt(20)+atebyt(idim)
      cpt(22)=cpt(21)+atebyt(idim)
      cpt(23)=cpt(22)+atebyt(idim)
      cpt(24)=cpt(23)+atebyt(idim)
      cpt(25)=cpt(24)+atebyt(idim)
      else
      cpt(20)=cpt(19)
      cpt(21)=cpt(20)
      cpt(22)=cpt(21)
      cpt(23)=cpt(22)
      cpt(24)=cpt(23)
      cpt(25)=cpt(24)
      endif
c
c    #
c    #  memory allocation for the direct Mxs
c    #
c
      if ((.not.flags(11)).and.flags(12)) then
      cpt(26)=cpt(25)+numint(1)
      cpt(27)=cpt(26)+lenbuk
      cpt(28)=cpt(27)+forbyt(nipbuk+1)
      cpt(29)=cpt(28)+atebyt(lenbuf)
      else
      cpt(26)=cpt(25)
      cpt(27)=cpt(26)
      cpt(28)=cpt(27)
      cpt(29)=cpt(28)
      endif
c
c    #  check memory requirements
      if(cpt(29)-1 .gt. avcsk)then
         write(nlist,6020)avcsk,cpt
         write(nlist,
     &    '("requested mem./available mem.: ",i10," / ",i10)')
     &    cpt(29)-1,avcsk
         call bummer('solvek: 5 avcsk=',avcsk,faterr)
      endif
c
c  psci parameter setup:
c
c  rtol is the convergence tolerance for p(*) and k(*) this iteration.
c  set equal to wnorm**2/8 and clip to ensure that:
c     tol(6) .le. rtol .le. tol(7)
c  this choice assures that slightly more than the minimal accuracy
c  required for second-order convergence is returned in the k(*) vector.
c
c  eshsci is used to increase the neighborhood of convergence of the
c  psci equation.  if it is of order wnorm, then second-order
c  convergence near the solution is not affected.
c
cVP variable of tol(10)
c
      if (tol(10).ne.tol(11)) then
        tol(10) = tol(10) - tol(12)
        if (tol(10).lt.tol(11)) tol(10)=tol(11)
        if (tol(10).lt.zero) tol(10)=zero
      endif
c
      rtol=min(max(tol(6),wnorm**2/eight),tol(7))
      eshsci=tol(10)+wnorm/eight
CSB
      write(nlist,*)
      write(nlist,*) ' tol(10)=',tol(10),' eshsci=',eshsci

cmd
c  read the aaaa block of integrals

      if ((.not.flags(11)).and.flags(12).and.(numint(1).gt.0)) then
      call wzero(numint(1),core(cpt(25)),1)
      call rdbks(lastb(bukpt(1)),core(cpt(25)),core(cpt(26)),
     & core(cpt(27)),lenbuk,nipbuk,'solvek')
      endif


c
c  perform micro-iterations to solve for the orbital
c  correction vector.  if qpcon=.true. on return, then
c  the p(*) vector is also returned.
c
      qpcon=.false.
cmb   ncfx passe to micro together with navst; for most cases (except
cmb   for the projection of hmcvec out of s/p) the product navst*ncsf
cmb will be used in the old algorithm

cmd
      if (flags(25)) then
      write(nlist,'(/,"Total memory for SOLVEK:",i15)') avcsk
      write(nlist,'("Free memory before entering MICRO:",i15)')
     & avcsk - cpt(29)+1
      write(nlist,'( "b    :",i15)') cpt(2)-cpt(1)
      write(nlist,'( "bdiag:",i15)') cpt(3)-cpt(2)
      write(nlist,'( "mdiag:",i15)') cpt(4)-cpt(3)
      write(nlist,'( "s    :",i15)') cpt(5)-cpt(4)
      write(nlist,'( "r    :",i15)') cpt(6)-cpt(5)
      write(nlist,'( "p    :",i15)') cpt(7)-cpt(6)
      write(nlist,'( "bxr  :",i15)') cpt(8)-cpt(7)
      write(nlist,'( "cxr  :",i15)') cpt(9)-cpt(8)
      write(nlist,'( "cxs  :",i15)') cpt(10)-cpt(9)
      write(nlist,'( "mxs  :",i15)') cpt(11)-cpt(10)
      write(nlist,'( "br   :",i15)') cpt(12)-cpt(11)
      write(nlist,'( "cr   :",i15)') cpt(13)-cpt(12)
      write(nlist,'( "mr   :",i15)') cpt(14)-cpt(13)
      write(nlist,'( "wr   :",i15)') cpt(15)-cpt(14)
      write(nlist,'( "zr   :",i15)') cpt(16)-cpt(15)
      write(nlist,'( "pr   :",i15)') cpt(17)-cpt(16)
      write(nlist,'( "scr  :",i15)') cpt(18)-cpt(17)
      write(nlist,'( "kpvt :",i15)') cpt(19)-cpt(18)
      write(nlist,'( "scr1 :",i15)') cpt(20)-cpt(19)
      write(nlist,'( "scr2 :",i15)') cpt(21)-cpt(20)
      write(nlist,'( "scr3 :",i15)') cpt(22)-cpt(21)
      write(nlist,'( "f1   :",i15)') cpt(23)-cpt(22)
      write(nlist,'( "f2   :",i15)') cpt(24)-cpt(23)
      write(nlist,'( "f3   :",i15)') cpt(25)-cpt(24)
      write(nlist,'( "h2   :",i15)') cpt(26)-cpt(25)
      write(nlist,'( "buk  :",i15)') cpt(27)-cpt(26)
      write(nlist,'( "ibuk :",i15,/)') cpt(28)-cpt(27)
      write(nlist,'( "ftbuf:",i15,/)') cpt(29)-cpt(28)
      endif

      call micro(
     & nmiter,         rtol,           tol(5),         eshsci,
     & core(cpt(1)),   ib,             ib_m,           ib_c,
     & w,              hmcvec,         core(cpt(2)),   core(cpt(3)),
     & iorder,         add(addpt(3)),  add(addpt(5)),  add(addpt(6)),
     & nadt,           naar,           nvdt,           nvat,
     & noldr,          nnewr,          nolds,          nnews,
     & ndimr,          nmaxr,          ndims,          nmaxs,
     & core(cpt(5)),   core(cpt(7)),   core(cpt(8)),   core(cpt(4)),
     & core(cpt(9)),   core(cpt(10)),  core(cpt(11)),  core(cpt(12)),
     & core(cpt(13)),  core(cpt(14)),  core(cpt(15)),  core(cpt(16)),
     & core(cpt(17)),  core(cpt(18)),  k,              core(cpt(6)),
     & apxde,          knorm,          qpcon,          qcoupl,
     & core(cpt(19)),  core(cpt(20)),  core(cpt(21)),  core(cpt(22)),
     & core(cpt(23)),  core(cpt(24)),  cmo,            d1,
     & fva,            qva,            fad,            qad,
     & fdd,            qvv,            idim,           mxcsao,
     & core(cpt(29)),  lenbuf,         modrt,          core(cpt(25)),
     & add,            xp,             z,              xbar,
     & rev,            ind,            uaa,            uad,
     & uvd,            uva,            nipbuk,         lastb,
     & lenbuk,         core(cpt(28)),  avcsk-cpt(29)+1)
c
c  save the p(*) vector for use in solving hmcvec in the next
c  mcscf iteration.
c
      if(qcoupl .and. qpcon)then
         ipt = cpt(6)
         do 315 ist=1,nst
c
      if (ist.ge.10) then
      write(fname,'(a6,i2)')'mcvfl.',ist
      else
      write(fname,'(a6,i1)')'mcvfl.',ist
      endif
      open(unit=nvfile,file=fname,status='unknown',
     &  form='unformatted')
c
           do 310 i=1,noldv(ist)
310        read(nvfile)
           pnorm = 0.0d+00
           do i=1,ncsf_f(ist)
                   pnorm = pnorm + abs(core(ipt-1+i))
                   enddo
         if (pnorm.eq.0.0d+00) go to 315
         call seqw(nvfile,ncsf_f(ist),core(ipt))
         noldv(ist)=noldv(ist)+1
         close (unit=nvfile)
315          ipt = ipt + ncsf_f(ist)
      endif
c
      if(flags(3))call timer('solvek total',4,event,nlist)
      return
1000  format(9d13.7)
6020  format(/' *** error *** solvek: avcsk,cpt(*)=',i8/(1x,10i8))
6010  format(/' *** error *** solvek: avcsk,req=',2i10)
6060  format(/' *** error *** solvek: cpt(1:3),nncfx=',4i10)
6050  format(/' *** error *** solvek: cpt(1:2),ndimr=',3i10)
6030  format(/' ib(*)='/(1x,8i10))
6040  format(/' *** error *** solvek: i,iblk,isize,avcsk=',4i10)
      end
      subroutine sorthn(u,s,su)
c
c  orthonormalize the orbital coefficients in u(*) by symmetry
c  blocks.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nbpsy(8),nnsb(8)
      equivalence (nxy(1,1),nbpsy(1))
      equivalence (nxy(1,3),nnsb(1))
      integer nmpsy(8),nnsm(8)
      equivalence (nxy(1,5),nmpsy(1))
      equivalence (nxy(1,7),nnsm(1))
      integer nsbm(8)
      equivalence (nxy(1,32),nsbm(1))
c
      real*8 u(*),s(*),su(*)
c
      do 100 isym=1,nsym
      nbi=nbpsy(isym)
      if(nbi.eq.0)go to 100
      nmi=nmpsy(isym)
      if(nmi.eq.0)go to 100
      nnbpt=nnsb(isym)+1
      n2pt=nsbm(isym)+1
      call orthos(nbi,nmi,u(n2pt),s(nnbpt),su)
100   continue
c
      return
      end
      subroutine tltpsp(id,ia,iv,u,udd,uda,uaa,uvd,uva,uvv)
c
c  transpose lower-triangle packed array u(*) to sub-block packed
c  arrays udd(*), uda(*)...,uvv(*).
cmd
c  In case of "direct mcscf" this subroutine also adds the 2-e
c   contribution (calculated in add_u) to uvv and uva.
cmd
c
c  on input:
c  id(*)  is the inverse mapping array for inactive orbitals.
c  ia(*)  is the inverse mapping array for active orbitals.
c  iv(*)  is the inverse mapping array for virtual orbitals.
c  u(*)  contains the input matrix lower triangle packed by symmetry.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nmpsy(8),nnsm(8)
      equivalence (nxy(1,5),nmpsy(1))
      equivalence (nxy(1,7),nnsm(1))
      integer ndpsy(8),n2sd(8)
      equivalence (nxy(1,9),ndpsy(1))
      equivalence (nxy(1,12),n2sd(1))
      integer napsy(8),nnsa(8)
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxy(1,15),nnsa(1))
      integer nvpsy(8),nnsv(8)
      equivalence (nxy(1,17),nvpsy(1))
      equivalence (nxy(1,19),nnsv(1))
      integer nsad(8),nsvd(8),nsva(8)
      equivalence (nxy(1,29),nsad(1))
      equivalence (nxy(1,30),nsvd(1))
      equivalence (nxy(1,31),nsva(1))
c
c
      integer nmotx
      parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
c
      integer id(*),ia(*),iv(*)
      real*8 u(*),udd(*),uda(*),uaa(*),uvd(*),uva(*),uvv(*)
c
      i2=0
      p2=0
      a2=0
      do 700 isym=1,nsym
      if(nmpsy(isym).eq.0)go to 700
      uoff=nnsm(isym)
      nd=ndpsy(isym)
      na=napsy(isym)
      nv=nvpsy(isym)
      i1=i2+1
      i2=i2+nd
      p1=p2+1
      p2=p2+na
      a1=a2+1
      a2=a2+nv
c
c  dd block:
c
      if(nd.eq.0)go to 200
      ij=n2sd(isym)
      do 110 i=i1,i2
      mi=id(i)
      do 110 j=i1,i2
      mj=id(j)
      mij=uoff+nndx(max(mi,mj))+min(mi,mj)
      ij=ij+1
      udd(ij)=u(mij)
110   continue
c
200   continue
c
c  da block:
c
      if(nd.eq.0)go to 300
      if(na.eq.0)go to 300
      pi=nsad(isym)
      do 210 p=p1,p2
      mp=ia(p)
      do 210 i=i1,i2
      mi=id(i)
      mpi=uoff+nndx(max(mp,mi))+min(mp,mi)
      pi=pi+1
      uda(pi)=u(mpi)
210   continue
c
300   continue
c
c  aa block:
c
      if(na.eq.0)go to 400
      pq=nnsa(isym)
      do 310 p=p1,p2
      mp=ia(p)
      do 310 q=p1,p
      mq=ia(q)
      mpq=uoff+nndx(mp)+mq
      pq=pq+1
      uaa(pq)=u(mpq)
310   continue
c
400   continue
c
c  vd block:
c
      if(nd.eq.0)go to 500
      if(nv.eq.0)go to 500
      ai=nsvd(isym)
      do 410 i=i1,i2
      mi=id(i)
      do 410 a=a1,a2
      ma=iv(a)
      mai=uoff+nndx(max(ma,mi))+min(ma,mi)
      ai=ai+1
      uvd(ai)=u(mai)
410   continue
c
500   continue
c
c  va block:
      if(na.eq.0)go to 600
      if(nv.eq.0)go to 600
      ap=nsva(isym)
      do 510 p=p1,p2
      mp=ia(p)
      do 510 a=a1,a2
      ma=iv(a)
      map=uoff+nndx(max(ma,mp))+min(ma,mp)
      ap=ap+1
cmd   uva(ap)=u(map)
      uva(ap)=uva(ap)+u(map)
      u(map)=uva(ap)
510   continue
c
600   continue
c
c  vv block:
c
      if(nv.eq.0)go to 700
      ab=nnsv(isym)
      do 610 a=a1,a2
      ma=iv(a)
      do 610 b=a1,a
      mb=iv(b)
      mab=uoff+nndx(ma)+mb
      ab=ab+1
cmd   uvv(ab)=u(mab)
      uvv(ab)=uvv(ab)+u(mab)
      u(mab)=uvv(ab)
610   continue
c
700   continue
c
      return
      end
      subroutine tran1(c,ao,mo,scr)
c
c  transform 1-e matrices by symmetry blocks from
c  an ao basis to an mo basis.  blocks are assumed to
c  be lower-triangular packed by symmetry.
c
c  input:
c  c(*)  = transformation coefficients.
c  ao(*) = ao basis matrix.
c  scr(*)= scratch vector.
c
c  output:
c  mo(*) = mo basis matrix, c(transpose) * ao * c.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nbpsy(8),nnsb(8)
      equivalence (nxy(1,1),nbpsy(1))
      equivalence (nxy(1,3),nnsb(1))
      integer nmpsy(8),nnsm(8)
      equivalence (nxy(1,5),nmpsy(1))
      equivalence (nxy(1,7),nnsm(1))
      integer nsbm(8)
      equivalence (nxy(1,32),nsbm(1))
c
      real*8 c(*),ao(*),mo(*),scr(*)
c
      do 100 isym=1,nsym
      nbi=nbpsy(isym)
      if(nbi.eq.0)go to 100
      nmi=nmpsy(isym)
      if(nmi.eq.0)go to 100
      cpt=nsbm(isym)+1
      apt=nnsb(isym)+1
      mpt=nnsm(isym)+1
      call uthu(c(cpt),ao(apt),mo(mpt),scr,nbi,nmi)
100   continue
      return
      end
      subroutine xphd1(h,d,hx,dx,npsy,nsym)
c
c  expand lower-triangular-packed arrays h(*) and d(*) into
c  symmetric square-packed arrays by symmetry blocks.
c
c  written by ron shepard.
c
      implicit integer(a-z)
      real*8 h(*),hx(*),d(*),dx(*)
      integer npsy(nsym)
c
      n2=1
      p0=0
      do 1300 psym=1,nsym
      np=npsy(psym)
      if(np.eq.0)go to 1300
      ip1=n2
      i0p=n2-1
      do 1200 p=1,np
      ipt=ip1
cdir$ ivdep
cvocl loop,novrec
      do 1100 t=1,p
      hx(i0p+t)=h(p0+t)
      dx(i0p+t)=d(p0+t)
      hx(ipt)=h(p0+t)
      dx(ipt)=d(p0+t)
      ipt=ipt+np
1100  continue
      p0=p0+p
      i0p=i0p+np
      ip1=ip1+1
1200  continue
      n2=n2+np*np
1300  continue
      return
      end
      subroutine xphd2(h2,d2,hx,dx,uvsym,uv,off1,addaa,smpt)
c
c  expand and gather the (uv) distribution of 2-e integrals and
c  density matrix elements.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
c
      integer nmotx
      parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
c
      real*8 h2(*),hx(*),d2(*),dx(*)
      integer smpt(8),off1(*),addaa(*)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer napsy(8),nsa(8)
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxy(1,14),nsa(1))
      integer ira1(8),ira2(8)
      equivalence (nxy(1,39),ira1(1))
      equivalence (nxy(1,40),ira2(1))
c
      if(uvsym.eq.1)go to 1000
c
c  psym .ne. tsym case:
c  because of matrix symmetry, only tsym.gt.psym blocks are gathered.
c
      ipt=1
      do 300 psym=1,nsym
      tsym=mult(psym,uvsym)
      if(tsym.lt.psym)go to 300
      np=napsy(psym)
      if(np.eq.0)go to 300
      nt=napsy(tsym)
      if(nt.eq.0)go to 300
      smpt(tsym)=ipt
      p1=ira1(psym)
      p2=ira2(psym)
      t1=ira1(tsym)
      t2=ira2(tsym)
      do 200 t=t1,t2
      do 100 p=p1,p2
      pt=nndx(t)+p
      ptuv=off1(max(pt,uv))+addaa(min(pt,uv))
      hx(ipt)=h2(ptuv)
      dx(ipt)=d2(ptuv)
      ipt=ipt+1
100   continue
200   continue
300   continue
      return
1000  continue
c
c  psym.eq.tsym case:
c
      n2=1
      do 1300 psym=1,nsym
      np=napsy(psym)
      if(np.eq.0)go to 1300
      smpt(psym)=n2
      nsap=nsa(psym)
      ip1=n2
      i0p=n2-1
      do 1200 p=1,np
      ipt=ip1
      pt0=nndx(nsap+p)+nsap
cdir$ ivdep
cvocl loop,novrec
      do 1100 t=1,p
      pt=pt0+t
      ptuv=off1(max(pt,uv))+addaa(min(pt,uv))
      hx(i0p+t)=h2(ptuv)
      dx(i0p+t)=d2(ptuv)
      hx(ipt)=h2(ptuv)
      dx(ipt)=d2(ptuv)
      ipt=ipt+np
1100  continue
      i0p=i0p+np
      ip1=ip1+1
1200  continue
      n2=n2+np*np
1300  continue
      return
      end
      subroutine xpnd2(d2,dx,uvsym,uv,off1,addaa,smpt)
c
c  expand and gather the (uv) distribution of
c  two-particle density matrix elements.
c
c  written by ron shepard.
c
      implicit integer(a-z)
c
c
      integer nmotx
      parameter (nmotx=1023)
c
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer nndx(nmotx)
      equivalence (iorbx(1,1),nndx(1))
c
      real*8 d2(*),dx(*)
      integer smpt(8),off1(*),addaa(*)
c
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer napsy(8),nsa(8)
      equivalence (nxy(1,13),napsy(1))
      equivalence (nxy(1,14),nsa(1))
      integer ira1(8),ira2(8)
      equivalence (nxy(1,39),ira1(1))
      equivalence (nxy(1,40),ira2(1))
c
      if(uvsym.eq.1)go to 1000
c
c  psym .ne. tsym case:
c  because of matrix symmetry, only tsym.gt.psym blocks are gathered.
c
      ipt=1
      do 300 psym=1,nsym
      tsym=mult(psym,uvsym)
      if(tsym.lt.psym)go to 300
      np=napsy(psym)
      if(np.eq.0)go to 300
      nt=napsy(tsym)
      if(nt.eq.0)go to 300
      smpt(tsym)=ipt
      p1=ira1(psym)
      p2=ira2(psym)
      t1=ira1(tsym)
      t2=ira2(tsym)
      do 200 t=t1,t2
      do 100 p=p1,p2
      pt=nndx(t)+p
      ptuv=off1(max(pt,uv))+addaa(min(pt,uv))
      dx(ipt)=d2(ptuv)
      ipt=ipt+1
100   continue
200   continue
300   continue
      return
1000  continue
c
c  psym.eq.tsym case:
c
      n2=1
      do 1300 psym=1,nsym
      np=napsy(psym)
      if(np.eq.0)go to 1300
      smpt(psym)=n2
      nsap=nsa(psym)
      ip1=n2
      i0p=n2-1
      do 1200 p=1,np
      ipt=ip1
      pt0=nndx(nsap+p)+nsap
cdir$ ivdep
cvocl loop,novrec
      do 1100 t=1,p
      pt=pt0+t
      ptuv=off1(max(pt,uv))+addaa(min(pt,uv))
      dx(i0p+t)=d2(ptuv)
      dx(ipt)=d2(ptuv)
      ipt=ipt+np
1100  continue
      i0p=i0p+np
      ip1=ip1+1
1200  continue
      n2=n2+np*np
1300  continue
      return
      end
c deck wmcd1f_grd
      subroutine wmcd1f_grd(nlist,mcd1fl,emc1,emc2,repnuc,
     & orbtyp,
     & buffer,valbuf,labbuf,ntitle,title,
     & nsym,nmot,nmpsy,ndpsy,napsy,nvpsy,
     & elast,mapmd,mapma,mapmv,
     & ird1,ird2,ira1,ira2,irv1,irv2,irm1,irm2,
     & nsa,n2sa,nsm,nnsm,nnmt,nsd,n2sd,nsv,n2sv,mcres,
     & nndx,daa,sdaa,
     & d1,mapmm,
     & mapim,mapar,mvres,bfnlab,deltaab,issymm)
c
c  write the mcscf 1-particle (transition) density matrix file.
c
c  this version writes the inactive-orbital terms along with the active
c  orbital terms.  these terms should be handled implicitly by
c  subsequent programs.  this will be implemented at a later time. -rls
c
c  input:
c  nlist = listing file.
c  mcd1fl = unit number for the 1-particle mcscf density matrix file.
c  lend1e,n1edbf = output file record parameters.
c  lend1e,n1edbf = output file record parameters.
c  buffer(*),valbuf(*),labbuf(*) = output buffer arrays.
c  ntitle = number of descriptive titles.
c  title(*) = titles.
c  nsym,nmot,nmpsy,naspy,ndpsy = symmetry arrays.
c  elast = last mcscf energy.
c  mapmd(*),mapma(*),mapmv(*) = inactive-to-mo, active-to-mo, and
c                               virtual-to-mo mapping vectors.  these
c                               map to symmetry-reduced mo indices.
c  ird1,ird2,ira1,ira2,irv1,irv2,irm1,irm2 = symmetry range arrays.
c  nsa,n2sa,nsm,nnsm,nnmt,nsd,n2sd,nsv,n2sv = symmetry array offsets.
c  mcres(*) = mcscf orbital resolution vector.
c  nndx(*) = addressing array.
c  daa(*) = active-active 1-particle density matrix.
c  deltaab = 1 for density, 0 for transition density
c
c  output:
c  d1(*) = mcscf array indexed by mo indices.
c  mapmm(1:nmot) = identity mapping vector.
c  mapim(1:nmot) = mo-to-internal mapping vector.
c
c  mvres(1:nmot) = orbital resolution mapping vector for use in cigrd
c                  and btran
c
c                mvres definitions
c
c  mvres(i) = -5   designates an active orbital which does not belong to
c                  invariant subspace.
c  mvres(i) = -4   designates a frozen virtual orbital.
c  mvres(i) = -3   designates a frozen core orbital
c  mvres(i) = -2   designates a doubly occupied orbital.
c  mvres(i) = -1   designates a virtual orbital
c  mvres(i) =  0   designates an active orbital which was artificially
c                  frozen during the mcscf optimization.
c  mvres(i) = n    orbital belongs to invariant orbital subspace "n".
c
c  version log:
c  01-jun-21  added the spin-density matrix to be written to the sifs
c  05-oct-09  subroutine created as an adaptation of wmcd1f -felix plasser
c
      implicit none
c
      integer orbtyp(nmot), ninfo, mvres(nmot),ietype(6)
      integer mapar(nmot),  imtype
      integer kntd1(36),info(10)
      real*8 energy(6),fcore
      integer ndpsy(nsym),napsy(nsym),nvpsy(nsym),nmpsy(nsym)
      integer mapim(*),mcres(*),nndx(*)
      integer mapmd(*),mapma(*),mapmv(*),mapmm(*)
      integer ird1(nsym),ird2(nsym),ira1(nsym),ira2(nsym)
      integer irv1(nsym),irv2(nsym)
      integer nsa(nsym),n2sa(nsym)
      integer nsm(nsym),nnsm(nsym)
      integer nsv(nsym),n2sv(nsym)
      integer nsd(nsym),n2sd(nsym)
      real*8 emc1, emc2, repnuc
      real*8 daa(*), sdaa(*)
      real*8 d1(nnmt),spind(nnmt)
c
      character*8 bfnlab(nmot)
      integer   nipv
      parameter(nipv=2)
      integer labbuf(nipv,n1edbf)
      real*8 buffer(lend1e),valbuf(n1edbf)
      real*8 deltaab
      logical issymm
c
      integer nenrgy,nmap,nimot,isym,i,ibogus,p,mi,mcd1fl,ierr,pq
      integer mpq0,p0,np
      integer nlist,ntitle,mp,q,mq,mmpq,mij0,ij0,i0,ni,j,mj,mmij
      integer irm1,irm2,nsym,nmot,elast,nnmt
c
      character*80 title(*)
c
      character*4 slabel(8)
c
      integer  lena1e, n1eabf,lenbft,
     &        lend2e,n2edbf,lend1e,n1edbf,ifmt1,ifmt2
      common/cfinfo/lena1e,n1eabf,lenbft,
     &  lend2e,n2edbf,lend1e,n1edbf,ifmt1,ifmt2


c
c  local...
      integer nipsy(8)
c
c      real*8   small
c      parameter(small = 1.0d-10)
c
      real*8    twodab
c  bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      twodab = 2d0*deltaab
c
c sif parameters
      ninfo = 6
c
c fsplit
c
      info(1) = 2
      info(2) = lend1e
      info(3) = n1edbf
      info(4) = lend2e
      info(5) = n2edbf
      info(6) = ifmt1 
c
c the info array is now defined
c
      nenrgy = 3
c
c
c
c  mcscf energy of the bra
c
      ietype(1) = -1025
      energy(1) = emc1
c
c mcscf energy of the ket
c
      ietype(2) = -1025
      energy(2) =  emc2
c
      ietype(3) = -1
      energy(3) =  repnuc
c
c the energy array is now defined.
c how many map vectors will be written to the mcd1fl.
c
      nmap = 1
c
c this map is an orbital resolution map.
c
      imtype = 2
c
      nimot=0
      do 10 isym=1,nsym
         nipsy(isym)=napsy(isym)+ndpsy(isym)
         nimot=nimot+nipsy(isym)
10    continue
c
c  compute the mo-to-internal mapping vector...
c
      do 20 i=1,nmot
         mapim(i)=0
20    continue
c
c
c compute the orbital resolution vector
c
      do 21 i=1,nmot
         if ( orbtyp(i) .eq. 0) then
           mvres(i) = -3
         elseif (orbtyp(i) .eq. 1) then
           mvres(i) = -2
         elseif (orbtyp(i) .eq. 2 ) then
           if( mcres(i) .eq. 0 ) then
              mvres(i) = -5
           else
              mvres(i) = mcres(i)
           endif
         elseif (orbtyp(i) .eq. 3 ) then
           mvres(i) = -1
         elseif (orbtyp(i) .eq. 4 ) then
           mvres(i) = -4
         else
c
c this "should never happen"
c
           write(nlist,*) 'this should not have happened. if it did rddr
     & t has a  serious bug which should be attended to immediately.'
           call bummer('wmcd1f : orbtyp(*) error',0,faterr)
         endif
 21   continue
c
c define some fudged symmetry labels
c
      do 22 i = 1,nsym
             write(slabel(i),'(a3,i1)')'sym',i
 22   continue
c
c assign some bogus basis function labels
c
      do 23 ibogus=1,nmot
         write(bfnlab(ibogus),'(a5,i3)')'md1mo',ibogus
 23   continue
c
      do 50 isym=1,nsym
         do 30 i=ird1(isym),ird2(isym)
            mapim(mapmd(i)+nsm(isym))=1
30       continue
         do 40 p=ira1(isym),ira2(isym)
            mapim(mapma(p)+nsm(isym))=1
40       continue
50    continue
      mi=0
      do 60 i=1,nmot
         if(mapim(i).eq.1)then
            mi=mi+1
            mapim(i)=mi
         endif
60    continue
      if(mi.ne.nimot)then
         write(nlist,*)'wmcd1f: mi=',mi,' nimot=',nimot
         call bummer('wmcd1f: map error, mi=',mi,faterr)
      endif
c
c  mo-to-mo mapping vector...
c
      do 70 i=1,nmot
         mapmm(i)=i
70    continue
c
c      ntitle=ntitle+1
c      if ( deltaab .eq. 1) then
c       title(ntitle)='symm. mcscf density matrix from mcscf.x'
c      else
c       if ( issymm ) then
c      title(ntitle)='symm. mcscf trans. density matrix from mcscf.x'
c       else
c      title(ntitle)='antisymm. mcscf trans. dens. mat. from mcscf.x'
c       endif
c      endif
c
c  write the header info...
c
      rewind mcd1fl
c
c
c put the orbital resolution vector into mapar(*)
c
      do 99 i=1,nmot
          mapar(i)=mvres(i)
c          write(nlist,*)i,' mapar ',mapar(i)
 99   continue
c
c write density matrix file header
c
      call sifwh(
     & mcd1fl,  ntitle,   nsym,  nmot,
     &  ninfo,  nenrgy,   nmap, title,
     &  nmpsy,  slabel,   info, bfnlab,
     & ietype,  energy, imtype,  mapar,
     & ierr )
      if( ierr .ne. 0 ) call bummer('wmcd1f:sifwh ierr=',ierr,faterr)
c
c  compute d1(*) array, indexed by mo indices...
c
      call wzero(nnmt,d1,1)
      call wzero(nnmt,spind,1)
c
c  active-active orbital contributions
c
      pq=0
      do 130 isym=1,nsym
         mpq0=nnsm(isym)
         p0=nsa(isym)
         np=napsy(isym)
c         pq0=n2sa(isym) -p0 -(p0+1)*np
         do 120 p=ira1(isym),ira2(isym)
            mp=mapma(p)
            do 110 q=ira1(isym),p
               pq=pq+1
               mq=mapma(q)
               mmpq=mpq0+nndx(mp)+mq
               d1(mmpq)=daa(pq)
c               qmc(mmpq)=qaa(pq)
c               fmc(mmpq)=faa(pq0 + p*np + q)
               spind(mmpq)=sdaa(pq)
110         continue
120      continue
130   continue
c
c  inactive-inactive contributions...
c
cfp: maybe the inner loop could be removed
      do 230 isym=1,nsym
         mij0=nnsm(isym)
         i0=nsd(isym)
         ni=ndpsy(isym)
         ij0=n2sd(isym) - i0 -(i0+1)*ni
         do 220 i=ird1(isym),ird2(isym)
            mi=mapmd(i)
            do 210 j=ird1(isym),i
               mj=mapmd(j)
               mmij=mij0+nndx(mi)+mj
c               fmc(mmij)=fdd(ij0 + i*ni + j)
c               qmc(mmij)=fmc(mmij)
210         continue
            d1(mmij)=twodab
            spind(mmij)=twodab
220      continue
230   continue
c

      call wtstv1_grd(mcd1fl, info,lend1e,n1edbf, buffer,
     & labbuf,valbuf, d1, spind,
     & nsym, irm1,irm2,mapmm,ifmt1,issymm)
c
      write(nlist,*)'d1(*) and spind(*) written to the '
     & //'1-particle density matrix file.'
c
      return
      end
c deck wmcd1f
      subroutine wmcd1f(nlist,mcd1fl,demc,knorm,wnorm,apxde,repnuc,
     & orbtyp,
     & buffer,valbuf,labbuf,ntitle,title,
     & nsym,nmot,nmpsy,ndpsy,napsy,nvpsy,
     & elast,mapmd,mapma,mapmv,
     & ird1,ird2,ira1,ira2,irv1,irv2,irm1,irm2,
     & nsa,n2sa,nsm,nnsm,nnmt,nsd,n2sd,nsv,n2sv,mcres,
     & nndx,daa,faa,fdd,
     & qaa,qad,qva,qvv,
     & d1,fmc,qmc,mapmm,
     & mapim,mapar,mvres,bfnlab)
c
c  write the mcscf 1-particle density matrix file.
c
c  this version writes the inactive-orbital terms along with the active
c  orbital terms.  these terms should be handled implicitly by
c  subsequent programs.  this will be implemented at a later time. -rls
c
c  input:
c  nlist = listing file.
c  mcd1fl = unit number for the 1-particle mcscf density matrix file.
c  lend1e,n1edbf = output file record parameters.
c  lend1e,n1edbf = output file record parameters.
c  buffer(*),valbuf(*),labbuf(*) = output buffer arrays.
c  ntitle = number of descriptive titles.
c  title(*) = titles.
c  nsym,nmot,nmpsy,naspy,ndpsy = symmetry arrays.
c  elast = last mcscf energy.
c  mapmd(*),mapma(*),mapmv(*) = inactive-to-mo, active-to-mo, and
c                               virtual-to-mo mapping vectors.  these
c                               map to symmetry-reduced mo indices.
c  ird1,ird2,ira1,ira2,irv1,irv2,irm1,irm2 = symmetry range arrays.
c  nsa,n2sa,nsm,nnsm,nnmt,nsd,n2sd,nsv,n2sv = symmetry array offsets.
c  mcres(*) = mcscf orbital resolution vector.
c  nndx(*) = addressing array.
c  daa(*) = active-active 1-particle density matrix.
c  faa(*),fdd(*) = mcscf fock matrices (assumed to be symmetric).
c  qaa(*),qad(*),qva(*),qvv(*) = blocks of the mcscf q(*) matrix.
c
c  output:
c  d1(*),fmc(*),qmc(*) = mcscf arrays indexed by mo indices.
c  mapmm(1:nmot) = identity mapping vector.
c  mapim(1:nmot) = mo-to-internal mapping vector.
c
c  mvres(1:nmot) = orbital resolution mapping vector for use in cigrd
c                  and btran
c
c                mvres definitions
c
c  mvres(i) = -5   designates an active orbital which does not belong to
c                  invariant subspace.
c  mvres(i) = -4   designates a frozen virtual orbital.
c  mvres(i) = -3   designates a frozen core orbital
c  mvres(i) = -2   designates a doubly occupied orbital.
c  mvres(i) = -1   designates a virtual orbital
c  mvres(i) =  0   designates an active orbital which was artificially
c                  frozen during the mcscf optimization.
c  mvres(i) = n    orbital belongs to invariant orbital subspace "n".
c
c  written by ron shepard, based on routine wrmode() by hans lischka.
c  version log:
c  10-jul-96  sifs routines added nto present version -hl
c  14-jul-93  use of sifs routines to write out mcd1fl, inclusion of
c             the orbital resolution mapping vector in the header. -gfg
c  07-jan-89  use wtstv1() to write arrays to the d1 file. -rls
c  15-nov-88  write out mo indices instead of internal indices, add
c             mapim(*) and qmc(*) to the d1 file. -rls
c  01-sep-88  write out labeled files instead of full arrays, split the
c             routine into 1-particle section and 2-part. section. -rls
c  00-jun-87  wrmode() written by hans lischka.
c
      implicit integer(a-z)
c
      integer orbtyp(nmot), ninfo, mvres(nmot),ietype(6)
      integer mapar(nmot),  imtype
      integer kntd1(36),info(10)
      real*8 energy(6),fcore
      integer ndpsy(nsym),napsy(nsym),nvpsy(nsym),nmpsy(nsym)
      integer mapim(*),mcres(*),nndx(*)
      integer mapmd(*),mapma(*),mapmv(*),mapmm(*)
      integer ird1(nsym),ird2(nsym),ira1(nsym),ira2(nsym)
      integer irv1(nsym),irv2(nsym)
      integer nsa(nsym),n2sa(nsym)
      integer nsm(nsym),nnsm(nsym)
      integer nsv(nsym),n2sv(nsym)
      integer nsd(nsym),n2sd(nsym)
      real*8 elast, demc, repnuc, knorm, wnorm, apxde
      real*8 daa(*),faa(*),fdd(*),qaa(*),qad(*),qva(*),qvv(*)
      real*8 d1(nnmt),fmc(nnmt),qmc(nnmt)
c
      character*8 bfnlab(nmot)
      integer   nipv
      parameter(nipv=2)
      integer labbuf(nipv,n1edbf)
      real*8 buffer(lend1e),valbuf(n1edbf)
c
      character*80 title(*)
c
      character*4 slabel(8)
c
      integer  lena1e, n1eabf,lenbft,
     &        lend2e,n2edbf,lend1e,n1edbf,ifmt1,ifmt2
      common/cfinfo/lena1e,n1eabf,lenbft,
     &  lend2e,n2edbf,lend1e,n1edbf,ifmt1,ifmt2

c
c  local...
      integer nipsy(8)
c
      real*8   small
      parameter(small = 1.0d-10)
c
      real*8    one,     two,     half
      parameter(one=1d0, two=2d0, half=.5d0)
c
c  bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
c sif parameters
      ninfo = 6
c
c fsplit
c
      info(1) = 2
      info(2) = lend1e
      info(3) = n1edbf
      info(4) = lend2e
      info(5) = n2edbf
      info(6) = ifmt1
c
c the info array is now defined
c
      nenrgy = 6
c
c
c
      ietype(1) = -1
      energy(1) =  repnuc
c
c  mcscf energy
c
      ietype(2) = -1025
      energy(2) = elast
c
c demc
c
      ietype(3) = -2050
      energy(3) =  demc
c
c wnorm
c
      ietype(4) = -2051
      energy(4) = wnorm
c
c knorm
c
      ietype(5) = -2052
      energy(5) = knorm
c
c apxde
c
      ietype(6) = -2053
      energy(6) = apxde
c
c the energy array is now defined.
c how many map vectors will be written to the mcd1fl.
c
      nmap = 1
c
c this map is an orbital resolution map.
c
      imtype = 2
c
      nimot=0
      do 10 isym=1,nsym
         nipsy(isym)=napsy(isym)+ndpsy(isym)
         nimot=nimot+nipsy(isym)
10    continue
c
c  compute the mo-to-internal mapping vector...
c
      do 20 i=1,nmot
         mapim(i)=0
20    continue
c
c
c compute the orbital resolution vector
c
      do 21 i=1,nmot
         if ( orbtyp(i) .eq. 0) then
           mvres(i) = -3
         elseif (orbtyp(i) .eq. 1) then
           mvres(i) = -2
         elseif (orbtyp(i) .eq. 2 ) then
           if( mcres(i) .eq. 0 ) then
              mvres(i) = -5
           else
              mvres(i) = mcres(i)
           endif
         elseif (orbtyp(i) .eq. 3 ) then
           mvres(i) = -1
         elseif (orbtyp(i) .eq. 4 ) then
           mvres(i) = -4
         else
c
c this "should never happen"
c
           write(nlist,*) 'this should not have happened. if it did rddr
     & t has a  serious bug which should be attended to immediately.'
           call bummer('wmcd1f : orbtyp(*) error',0,faterr)
         endif
 21   continue
c
c define some fudged symmetry labels
c
      do 22 i = 1,nsym
             write(slabel(i),'(a3,i1)')'sym',i
 22   continue
c
c assign some bogus basis function labels
c
      do 23 ibogus=1,nmot
         write(bfnlab(ibogus),'(a5,i3)')'md1mo',ibogus
 23   continue
c
      do 50 isym=1,nsym
         do 30 i=ird1(isym),ird2(isym)
            mapim(mapmd(i)+nsm(isym))=1
30       continue
         do 40 p=ira1(isym),ira2(isym)
            mapim(mapma(p)+nsm(isym))=1
40       continue
50    continue
      mi=0
      do 60 i=1,nmot
         if(mapim(i).eq.1)then
            mi=mi+1
            mapim(i)=mi
         endif
60    continue
      if(mi.ne.nimot)then
         write(nlist,*)'wmcd1f: mi=',mi,' nimot=',nimot
         call bummer('wmcd1f: map error, mi=',mi,faterr)
      endif
c
c  mo-to-mo mapping vector...
c
      do 70 i=1,nmot
         mapmm(i)=i
70    continue
c
      ntitle=ntitle+1
      title(ntitle)='mcscf density matrices from program mcscf'
c
c  write the header info...
c
      rewind mcd1fl
c
c
c put the orbital resolution vector into mapar(*)
c
      do 99 i=1,nmot
          mapar(i)=mvres(i)
c          write(nlist,*)i,' mapar ',mapar(i)
 99   continue
c
c write density matrix file header
c
      call sifwh(
     & mcd1fl,  ntitle,   nsym,  nmot,
     &  ninfo,  nenrgy,   nmap, title,
     &  nmpsy,  slabel,   info, bfnlab,
     & ietype,  energy, imtype,  mapar,
     & ierr )
      if( ierr .ne. 0 ) call bummer('wmcd1f:sifwh ierr=',ierr,faterr)
c
c  compute d1(*), fmc(*), and qmc(*) arrays, indexed by mo indices...
c
      call wzero(nnmt,d1,1)
      call wzero(nnmt,fmc,1)
      call wzero(nnmt,qmc,1)
c
c  active-active orbital contributions...
c  faa(pq)=faa(qp) for converged wave functions.
c
      pq=0
      do 130 isym=1,nsym
         mpq0=nnsm(isym)
         p0=nsa(isym)
         np=napsy(isym)
         pq0=n2sa(isym) -p0 -(p0+1)*np
         do 120 p=ira1(isym),ira2(isym)
            mp=mapma(p)
            do 110 q=ira1(isym),p
               pq=pq+1
               mq=mapma(q)
               mmpq=mpq0+nndx(mp)+mq
               d1(mmpq)=daa(pq)
               qmc(mmpq)=qaa(pq)
               fmc(mmpq)=faa(pq0 + p*np + q)
110         continue
120      continue
130   continue
c
c  inactive-inactive contributions...
c  fdd(*)=qdd(*) and should be diagonal.
c
      do 230 isym=1,nsym
         mij0=nnsm(isym)
         i0=nsd(isym)
         ni=ndpsy(isym)
         ij0=n2sd(isym) - i0 -(i0+1)*ni
         do 220 i=ird1(isym),ird2(isym)
            mi=mapmd(i)
            do 210 j=ird1(isym),i
               mj=mapmd(j)
               mmij=mij0+nndx(mi)+mj
               fmc(mmij)=fdd(ij0 + i*ni + j)
               qmc(mmij)=fmc(mmij)
210         continue
            d1(mmij)=two
220      continue
230   continue
c
c  inactive-active contributions...
c  f(ip)=q(ip) for converged mcscf wave functions.
c
      ip=0
      do 330 isym=1,nsym
         mip0=nnsm(isym)
         do 320 p=ira1(isym),ira2(isym)
            mp=mapma(p)
            do 310 i=ird1(isym),ird2(isym)
               ip=ip+1
               mi=mapmd(i)
               mip=mip0+nndx(max(mi,mp))+min(mi,mp)
               qmc(mip)=qad(ip)
               fmc(mip)=qad(ip)
310         continue
320      continue
330   continue
c
c  virtual-inactive contributions...
c  f(ai)=q(ai)=0  for converged mcscf wave functions.
c
c  virtual-active contributions...
c  f(ap)=0 for converged mcscf wave functions.
c
      ap=0
      do 430 isym=1,nsym
         map0=nnsm(isym)
         do 420 p=ira1(isym),ira2(isym)
            mp=mapma(p)
            do 410 a=irv1(isym),irv2(isym)
               ap=ap+1
               ma=mapmv(a)
               map=map0+nndx(max(mp,ma))+min(mp,ma)
               qmc(map)=qva(ap)
410         continue
420      continue
430   continue
c
c  virtual-virtual contributions...
c
      do 530 isym=1,nsym
         mab0=nnsm(isym)
         a0=nsv(isym)
         na=nvpsy(isym)
         ab0=n2sv(isym) -a0 -(a0+1)*na
         do 520 a=irv1(isym),irv2(isym)
            ma=mapmv(a)
            do 510 b=irv1(isym),a
               mb=mapmv(b)
               mab=mab0+nndx(ma)+mb
               qmc(mab)=qvv(ab0 + a*na +b)
510         continue
520      continue
530   continue
c
      call wtstv1(mcd1fl, info,lend1e,n1edbf, buffer,
     & labbuf,valbuf, d1,fmc,
     & qmc, nsym, irm1,irm2,mapmm,ifmt1)
c
      write(nlist,*)'d1(*), fmc(*), and qmc(*) written to the '
     & //'1-particle density matrix file.'
c
      return
      end
c deck wmcd2f
      subroutine wmcd2f(nlist,mcd2fu,
     & buffer,valbuf,labbuf,namot,
     & mapma,syma,mult,ira1,
     & ira2,ndmot,mapmd,symd,nsm,
     & d1,d2,mcd2fl,deltaab)
c
c  write the mcscf 2-particle density matrix file.
c
c  this version writes the inactive-orbital terms along with the active
c  orbital terms.  these terms should be handled implicitly by
c  subsequent programs.  this will be implemented at a later time. -rls
c
c  input:
c  nlist = listing file.
c  mcd2fu = 2-particle density file unit number.
c  lend2e, n2edbf = output buffer parameters.
c  buffer(*), valbuf(*), labbuf(*) = output buffers.
c  namot = total number of active orbitals.
c  mapma(*) = active-to-mo mapping vector. (symmetry reduced mo)
c  syma(*) = active orbital symmetries.
c  mult(8,8) = irrep multiplication table.
c  ira1(*),ira2(*) = active orbital symmetry ranges.
c  ndmot = total number of inactive orbitals.
c  mapmd(*) = inactive-to-mo mapping vector. (symmetry reduced mo)
c  symd(*) = inactive orbital symmetries.
c  nsm(*) = mo symmetry offset array.
c  d1(*) = lower-triangular symmetry packed 1-particle density matrix
c          indexed by active orbitals.
c  d2(*) = canonical symmetry packed 2-particle density matrix indexed
c          by active orbitals.
c  deltaab = 1 for density, 0 for transition density
c
c  written by ron shepard, based on routine wrmode() by hans lischka.
c  05-oct-09  adjustments for SA-MCSCF gradient -felix plasser
c  10-jul-96  sifs modifications incorporated -hans lischka
c  14-jul-93  sifs modifications  -galen f. gawboy
c  15-nov-88  write out mo indices instead of internal indices. -rls
c  01-sep-88  write out labeled files instead of full arrays, split the
c             routine into 1-particle section and 2-part. section. -rls
c  00-jun-87  wrmode() written by hans lischka.
c
c todo: the zero elements in transition density matrices are now explicitely stored.
c          but they could be left out. -felix plasser
      implicit integer(a-z)
c
      integer info(10)
      integer mapma(*),syma(*),mapmd(*),symd(*)
      integer mult(8,8),ira1(*),ira2(*),nsm(*)
      real*8 d1(*),d2(*)
c
      parameter(nipv=4)
      integer labbuf(nipv,n2edbf)
      real*8 buffer(lend2e),valbuf(n2edbf)
      real*8 deltaab
c
c second order density matrix output file name.
c
cfp: the file name is passed from the main routine
      character*60 mcd2fl
c
c define last and asynchronous i/o parameters
c
      integer    msame  , nmsame  , nomore ,  iwait
      parameter( msame=0, nmsame=1, nomore=2, iwait=0)
c
c
      common/cfiles/iunits(55)
      integer  lena1e, n1eabf,lenbft,
     &        lend2e,n2edbf,lend1e,n1edbf,ifmt1,ifmt2
      common/cfinfo/lena1e,n1eabf,lenbft,
     &  lend2e,n2edbf,lend1e,n1edbf,ifmt1,ifmt2


c
c
c  local...
      real*8 term
      real*8    one,     two,     four,     half,      small
      parameter(one=1d0, two=2d0, four=4d0, half=.5d0, small=1d-10)
      real*8    onedab,twodab,fourdab
c
      onedab = 1d0*deltaab
      twodab = 2d0*deltaab
      fourdab = 4d0*deltaab
c
      info(1) = 2
      info(2) = lend1e
      info(3) = n1edbf
      info(4) = lend2e
      info(5) = n2edbf
      info(6) = ifmt1 
c
c the info array is now defined.
c
      numout=0
      iknt=0
c
c  position and open mcd2fl
c
      call sifo2f( iunits(15), iunits(16),mcd2fl,info,mcd2fu,ierr)
      if (ierr .ne. 0) then
         call bummer('wmcd2f: from sifo2f, ierr=', ierr,faterr)
      endif
c
c  aaaa density matrix elements...
c
      pqrs=0
      do 140 p=1,namot
         psym=syma(p)
         mp=mapma(p)+nsm(psym)
         do 130 q=1,p
            qsym=syma(q)
            mq=mapma(q)+nsm(qsym)
            pqsym=mult(qsym,psym)
            do 120 r=1,p
               rsym=syma(r)
               mr=mapma(r)+nsm(rsym)
               ssym=mult(rsym,pqsym)
               if(r.eq.p)then
                  s2=min(ira2(ssym),q)
               else
                  s2=min(ira2(ssym),r)
               endif
               do 110 s=ira1(ssym),s2
                  ms=mapma(s)+nsm(ssym)
                  pqrs=pqrs+1
                  term=d2(pqrs)
                  if(abs(term).gt.small)then
                     numout=numout+1
                     if(iknt.eq.n2edbf)then
                      call wtlabd2(mcd2fu,iwait,info,lend2e,
     &               buffer,n2edbf,valbuf,nipv,labbuf,iknt,msame,ifmt2)
                     endif
                     iknt=iknt+1
                     valbuf(iknt)=term
                     labbuf(1,iknt)=mp
                     labbuf(2,iknt)=mq
                     labbuf(3,iknt)=mr
                     labbuf(4,iknt)=ms
                  endif
110            continue
120         continue
130      continue
140   continue
c
c  aadd and adad density matrix elements...
c  d(pq,ii) =  two  * d1(pq)
c  d(pi,qi) = -half * d1(pq)
c
      pq=0
      do 230 p=1,namot
         psym=syma(p)
         mp=mapma(p)+nsm(psym)
         do 220 q=ira1(psym),p
            mq=mapma(q)+nsm(psym)
            pq=pq+1
            term=d1(pq)
            if(abs(term).gt.small)then
               numout=numout+2*ndmot
               do 210 i=1,ndmot
                  isym=symd(i)
                  mi=mapmd(i)+nsm(isym)
c
                  if(iknt.eq.n2edbf)then
                    call wtlabd2(mcd2fu,iwait,info,lend2e,
     &               buffer,n2edbf,valbuf,nipv,labbuf,iknt,msame,ifmt2)
                  endif
                  iknt=iknt+1
                  valbuf(iknt)=two*term
                  labbuf(1,iknt)=mp
                  labbuf(2,iknt)=mq
                  labbuf(3,iknt)=mi
                  labbuf(4,iknt)=mi
c
                  if(iknt.eq.n2edbf)then
                    call wtlabd2(mcd2fu,iwait,info,lend2e,
     &               buffer,n2edbf,valbuf,nipv,labbuf,iknt,msame,ifmt2)
                  endif
                  iknt=iknt+1
                  valbuf(iknt)=-half*term
                  labbuf(1,iknt)=mp
                  labbuf(2,iknt)=mi
                  labbuf(3,iknt)=mq
                  labbuf(4,iknt)=mi
210            continue
            endif
220      continue
230   continue
c
c  dddd density matrix elements...
c  d(iiii)  =  two
c  d(ii,jj) =  four
c  d(ij,ij) = -one
c
      numout=numout+ndmot*ndmot
      do 320 i=1,ndmot
         isym=symd(i)
         mi=mapmd(i)+nsm(isym)
         do 310 j=1,(i-1)
            jsym=symd(j)
            mj=mapmd(j)+nsm(jsym)
c
            if(iknt.eq.n2edbf)then
              call wtlabd2(mcd2fu,iwait,info,lend2e,
     &         buffer,n2edbf,valbuf,nipv,labbuf,iknt,msame,ifmt2)
            endif
            iknt=iknt+1
            valbuf(iknt)=fourdab
            labbuf(1,iknt)=mi
            labbuf(2,iknt)=mi
            labbuf(3,iknt)=mj
            labbuf(4,iknt)=mj
c
            if(iknt.eq.n2edbf)then
              call wtlabd2(mcd2fu,iwait,info,lend2e,
     &          buffer,n2edbf,valbuf,nipv,labbuf,iknt,msame,ifmt2)
            endif
            iknt=iknt+1
            valbuf(iknt)=-onedab
            labbuf(1,iknt)=mi
            labbuf(2,iknt)=mj
            labbuf(3,iknt)=mi
            labbuf(4,iknt)=mj
310      continue
c
         if(iknt.eq.n2edbf)then
            call wtlabd2(mcd2fu,iwait,info,lend2e,
     &        buffer,n2edbf,valbuf,nipv,labbuf,iknt,msame,ifmt2)
         endif
         iknt=iknt+1
         valbuf(iknt)=twodab
         labbuf(1,iknt)=mi
         labbuf(2,iknt)=mi
         labbuf(3,iknt)=mi
         labbuf(4,iknt)=mi
320   continue
c
c  dump the last buffer...
c
c
      call wtlabd2(mcd2fu,iwait,info,lend2e,
     & buffer,n2edbf,valbuf,nipv,labbuf,iknt,nomore,ifmt2)
c
c close mcd2fl
c
      call sifc2f(mcd2fu,info,ierr)
      if (ierr .ne. 0) then
        call bummer(' wmcd2f: from sifc2f, ierr=',ierr,faterr)
      endif
c
      write(nlist,'(1x,i10,1x,a)')numout,'d2(*) elements written to'
     & //' the 2-particle density matrix file: '//mcd2fl
c
      return
      end
c
c**********************************************************
c
      subroutine readin(first)
c
c  read in user information.
c
c  17-jan-90 wnorm equivalence fixed. -rls/eas
c  written by ron shepard.
c
cVP 28-dec-94 variable energy shift parameter tol(10)
c   equivalent to variable damping. Adding tol(11) and tol(12)
c   written by V. Parasuk

cmb   reading in number of states to average over (navst) and their
cmb   weights (wavst).

       implicit none
c  ##  parameter & common section
c
c
      real*8 one
      parameter (one=1.0d+00)
c
c    #  maximal No. of different DRTs
      integer maxnst
      parameter (maxnst=8)
c
c    #  maximal No. of states in one DRT
      integer mxavst
      parameter (mxavst = 50)
c
c    #  maximal total number of states
      integer maxstat
      parameter (maxstat = maxnst*mxavst)
c
      integer nxy, mult, nsym, nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nbpsy(8),nmpsy(8)
      equivalence (nxy(1,1),nbpsy(1))
      equivalence (nxy(1,5),nmpsy(1))
      integer nmot
      equivalence (nxtot(4),nmot)
c
      integer iunits
      common/cfiles/iunits(55)
      integer nlist, nin, nslist
      equivalence (iunits(1),nlist)
      equivalence (iunits(2),nin)
      equivalence (iunits(9),nslist)
c
c  use iorbx(*,i) for i = 1 to 6 as local scratch space.
c  orbtyp(*) and fcimsk(*) are the only arrays used elswhere.
c
c
      integer nmotx
      parameter (nmotx=1023)
c
      integer iorbx, ix0
      common/corbc/iorbx(nmotx,8),ix0(3)
      integer orbtyp(nmotx),fcimsk(nmotx)
      equivalence (iorbx(1,7),orbtyp(1))
      equivalence (iorbx(1,8),fcimsk(1))
c
      integer npath(nmotx)
      equivalence (iorbx(1,1),npath(1))
      integer fciorb(3,nmotx)
      equivalence (iorbx(1,2),fciorb(1,1))
      integer fcore(nmotx)
      equivalence (iorbx(1,5),fcore(1))
      integer fvrt(nmotx)
      equivalence (iorbx(1,6),fvrt(1))
c
      real*8 tol,rtolci
      common/toler/tol(12),rtolci(mxavst)
c
      real*8 coninf
      common/cconi/coninf(6)
      real*8 wnorm
      equivalence (coninf(4),wnorm)
c
      integer asprojection
      common/mprojection/ asprojection
c
      integer nflag
      parameter (nflag=50)
      logical flags
      common/lflags/flags(nflag)
c
      integer niter, nstate, ncol, ncoupl, noldv, noldhv, nunitv,
     &        nciitr, mxvadd, nvcimx, nmiter, nvrsmx
      common /inputc/niter,nstate(8),ncol(8),ncoupl,
     +  noldv(8),noldhv(8),nunitv,nciitr,mxvadd,nvcimx,nmiter,nvrsmx
c
      integer navst,nst,navst_max
      real*8 heig,wavst,norm
      common /avstat/ wavst(maxnst,mxavst), heig(maxnst,mxavst),
     & navst(maxnst),nst,navst_max
c
      integer nrow_f,ncsf_f,nwalk_f,ssym
      common/drtf/ nrow_f(maxnst),ncsf_f(maxnst),
     & nwalk_f(maxnst),ssym(maxnst)
c
      integer dintsz
      common/dirint/ dintsz
c
c
      integer cdir
      common/c_block/ cdir
c
      integer lvlprt
      common/prtout/lvlprt
      
      integer stape,lenbfs,h2bpt
      common/cbufs/stape,lenbfs,h2bpt
c
c  bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer nbfmxp,        ninfomx,   nenrgymx,   nmapmx
      parameter (nbfmxp=1023, ninfomx=6, nenrgymx=5, nmapmx=2)
c
      character*4 slabelao(nbfmxp)
      character*8 bfnlabao(nbfmxp)
      character*8 bfnlabmo(nbfmxp)
       integer info,nmap,ietype,imtype,map,nenrgy
       real*8  energy
      common /forsif/ info(ninfomx),nenrgy,nmap,
     . ietype(nenrgymx), energy(nenrgymx),
     . imtype(nmapmx), map(nbfmxp,nmapmx),
     . slabelao, bfnlabao
c
c  ##  integer section
c
      integer lenbuf_f(maxnst)
      integer ierr, ixx, i1, ist, i
      integer j
      integer maxvst, maxnavst
      integer nfci, nfcore, nfvrt
c
c  ##  real*8 section
c
      real*8 wavst_save
      character*14 chstate
      logical first,prtwarn
c matruc 200/01/17:
c variable has to be declared *before* referencing
      integer cosmocalc
      common/cosmo2/cosmocalc
c
*@ifndef  nonamelist
      namelist/input/flags,npath,niter,tol,nstate,fciorb,ncol,
     +  ncoupl,noldv,noldhv,nunitv,nciitr,mxvadd,nvcimx,rtolci,wnorm,
     +  nmiter,nvrsmx,fcore,fvrt,nmpsy,navst,wavst,dintsz,cdir,lvlprt,
     &  asprojection,cosmocalc,lenbfs
*@endif
c
c
      integer fcorex(0:5),         fvrtx(0:5)
      data    fcorex/5,5,5,0,5,5/, fvrtx/5,5,5,4,5,5/
c
c**********************************************************************
c
c michal2{
c
c Silmar - 19/09/02 - Common block for cosmocalc
c
c      integer cosmocalc
c      common/cosmo2/cosmocalc

c
c michal2}
c
      ierr = 0
c
      if (first) write(nlist,6010)
6010  format(/' user input information:'/)
c
c  set up default values
c
c michal2{
c
       cosmocalc = 0
c
c michal2}
c
      do 100 i=1,nflag
         flags(i)=.false.
100   continue
c
c  initialize npath(*),fciorb(*,*),fcore(*),
c  fvrt(*),orbtyp(*), and fcimsk(*).
c
      do 122 j=1,8
         do 120 i=1,nmotx
            iorbx(i,j)=0
120      continue
122   continue
c
c  initialize navst, wavst
      do i=1,maxnst
      navst(i) = 0
        do j=1,mxavst
        wavst(i,j) = 0.0d+00
        enddo
      enddo
c
      asprojection = 1
c
      tol(1)=1e-4
      tol(2)=1e-4
      tol(3)=1e-4
      tol(4)=1e-8
      tol(5)=1e-4
cmd
      tol(6)=1e-6
      tol(7)=1e-5
c     tol(6)=1e-5
cmd   tol(7)=1e-1
      tol(8)=1e0
      tol(9)=1e0
      tol(10)=0e0
      tol(11)=0e0
      tol(12)=0e0
c
      niter=1
      ncoupl=1
        do ist = 1,8
        nstate(ist) = 0
        ncol(ist) = 0
        noldv(ist)=0
        noldhv(ist)=0
        enddo
      nunitv=1
      nciitr=25
      mxvadd=20
      nvcimx=20
      nmiter=30
      nvrsmx=30
      do 160 i=1,mxavst
         rtolci(i)=1d-2
160   continue
      wnorm=0e0
c     avstat defaults ....
      navst(1) = 1
      wavst(1,1) = 1.0d+00
      dintsz = 500
c    # no direct C*r,C*s
      cdir = 0
c    # minimal print levle mode
      lvlprt = 0
      lenbfs = 131071
c
c**********************************************************************
c
c
c  read and echo the user input...
c
      if (first) then
      write(nlist,'(1x,a)' ) '======== echo of the mcscf input ========'
      call echoin( nin, nlist, ierr )
      endif
c
      if ( ierr .ne. 0 ) then
         call bummer('readin: from echoin, ierr=',ierr,faterr)
      endif
c
      rewind nin
c
c  read namelist input...
c
*@ifdef nonamelist
*C    Cray-T3D unterstuetzt keinen Namelist-Input
*C    listenorientierte Eingabe nach folgendem Muster
*C     , , , , , / NPATH
*C     , , ,     / NSTATE, NITER
*C     , ,  ,    / TOL
*C     , ,       / NMITER, NVRSMX
*C     , ,       / NCOL, NCOUPL
*C     , ,       / NMPSY
*C     , ,       / FCIORB : 1. Paar
*C     , ,       / FCIORB : 2. Paar etc.   bis EOF
*        read(nin,*) (npath(i),i=1,nflag)
*        read(nin,*) nstate,niter
*        read(nin,*) (tol(i),i=1,10)
*        read(nin,*) nmiter, nvrsmx
*        read(nin,*) ncol, ncoupl
*        read(nin,*) (nmpsy(i),i=1,nsym)
*        i=1
*  198   read(nin,*,end=200) fciorb(1,i),fciorb(2,i),fciorb(3,i)
*        i=i+1
*        goto 198
*@elif defined  nml_required
*      read( nin, nml=input, end=200 )
*@else
      read( nin, input, end=200 )
*@endif
200   continue
c**********************************************************************
c
c  process and print out the input data just read.
c
      do 230 i=1,nmotx
         if(npath(i))210,231,220
210      flags(-npath(i))=.false.
         go to 230
220      flags(npath(i))=.true.
230   continue
231   continue
cmd
      if (first) return
cmd
c
c  initialize nmpsy(*) to include frozen-core, inactive, active,
c  virtual, and frozen virtual orbitals.
c
      do i=1,8
         nmpsy(i)=nbpsy(i)
      enddo ! do i=1,8
c
c  check nmpsy(*).
c
      do 240 i=1,nsym
         if(nmpsy(i).gt.nbpsy(i))then
            write(nlist,*)'readin: nmpsy(*) error.'
            call bummer('readin: nmpsy(*) error.',0,faterr)
         endif
240   continue
c
c  initialize orbtyp(*)=3 (virtual orbital).
c  set orbtyp(*)=0 for frozen core orbitals.
c               =4 for frozen virtual orbitals.
c
      nmot=0
      do 250 i=1,nsym
         nmot=nmot+nmpsy(i)
250   continue
      do 260 i=1,nmot
         orbtyp(i)=3
260   continue
c
      nfcore=0
      do 270 i=1,nmotx
         j=fcore(i)
         if(j.le.0)go to 271
         nfcore=nfcore+1
         orbtyp(j)=fcorex(orbtyp(j))
270   continue
271   continue
c
      nfvrt=0
      do 280 i=1,nmotx
         j=fvrt(i)
         if(j.le.0)go to 281
         nfvrt=nfvrt+1
         orbtyp(j)=fvrtx(orbtyp(j))
280   continue
281   continue
c-    write(nlist,6020)'nfcore=',nfcore
      if(nfcore.gt.0)write(nlist,6020)'fcore(*)=',(fcore(i),i=1,nfcore)
c-    write(nlist,6020)'nfvrt=',nfvrt
      if(nfcore.gt.0)write(nlist,6020)'fvrt(*)=',(fvrt(i),i=1,nfvrt)
6020  format(1x,a,(1x,20i4))
      if(nfcore.ne.0 .or. nfvrt.ne.0)then
         write(nlist,*)'fcore(*) and fvrt(*) are not allowed'
         call bummer('readin: nfcore nfvrt error',0,faterr)
      endif
c
c  make sure tol(6) is consistent with tol(2) and tol(3)...
c
      if( tol(6) .gt. min(tol(2),tol(3))/8 )then
         write(nlist,6020)
     &    'warning: resetting tol(6) due to possible inconsistency.'
         tol(6) = min( tol(2), tol(3) ) / 8
      endif
c
c    #  MCSCF optimization procedure parmeters
c
      write(nlist,6025)
6025  format(//,1x,3(1h*),2x,
     & 'MCSCF optimization procedure parmeters:',2x,3(1h*)//)
      write(nlist,6030)niter,nmiter,nvrsmx
6030  format(' maximum number of mcscf iterations:',t45,'niter= ',i5//
     +  ' maximum number of psci micro-iterations:',t45,'nmiter=',i5/
     +  ' maximum r,s subspace dimension allowed:',t45,'nvrsmx=',i5)
c
c
c    #   for the Direct MCSCF print maximal size
c    #   of the integral block in the integral splitting
c
      if (flags(22).or.flags(24))
     &  write(nlist,3002)dintsz
3002  format(
     & 'Maximal size of the half transformed int. file: ',i6,2x,
     & 'MB')
c
c    #   the matrix block: C[ncsf,vd]  may be hold in memory
c
      if (cdir.eq.1) then
      write(nlist,3003)
3003  format(
     & /' The C matrix block C[ncsf,vd] will not be calculated.',
     & /' The matrix-vector contributions for this block',
     & ' will be calculated directly.')
      endif
c
c    #   the B matrix blocks
c
      if (flags(22)) then
      write(nlist,3004)
3004  format(
     & /"The B matrix blocks: B[vd,ad],B[vd,vd] and B[va,vd]",
     & " will not be calculated.",
     & /" The matrix-vector contributions for this blocks",
     &  " will be calculated directly.")
      endif

      if (flags(26)) then
       write(nlist,3005)
3005  format(
     . /"AO integrals are read from Seward integral files ",
     . /" ... RUNFILE, ONEINT , ORDINT ")
      elseif (flags(27)) then
       write(nlist,3006)
3006  format(
     . /"AO integrals are read from Dalton2 integral files ",
     . /" ... AOONEINT AOTWOINT ")
      endif

c
      write(nlist,6040)(tol(i),i=1,12)
6040  format(1p/
     +  ' tol(1)= ',e11.4,'. . . . delta-emc convergence criterion.',/
     +  ' tol(2)= ',e11.4,'. . . . wnorm convergence criterion.',/
     +  ' tol(3)= ',e11.4,'. . . . knorm convergence criterion.'/
     +  ' tol(4)= ',e11.4,'. . . . apxde convergence criterion.'/
     +  ' tol(5)= ',e11.4,'. . . . small diagonal matrix element',
     +            ' tolerance.'/
     +  ' tol(6)= ',e11.4,'. . . . minimum ci-psci residual norm.'/
     +  ' tol(7)= ',e11.4,'. . . . maximum ci-psci residual norm.'/
     +  ' tol(8)= ',e11.4,'. . . . maximum abs(k(xy)) allowed.'/
     +  ' tol(9)= ',e11.4,'. . . . wnorm coupling tolerance.'/
     +  ' tol(10)=',e11.4,'. . . . maximum psci ',
     +                             'emergency shift parameter.'/
     +  ' tol(11)=',e11.4,'. . . . minimum psci ',
     +                             'emergency shift parameter.'/
     +  ' tol(12)=',e11.4,'. . . . increment of psci ',
     +                             'emergency shift parameter.')
c
c  if ncol was not set in the read, set to one larger than necessary
c  so that the first higher excited state may be monitored.
c
      do ist=1,8
          nstate(ist)=max(nstate(ist),0)
          if (ncol(ist).eq.0) ncol(ist) = nstate(ist)+navst(ist)+1
      enddo
c
c     state averaging
c
      nst = 0
      maxnavst = 0
      do i=1,8
      if (navst(i).ne.0) nst = nst + 1
        if (maxnavst.lt.navst(i)) maxnavst = navst(i)
      enddo
      if (maxnavst.gt.mxavst) then
      call bummer('readin: navst exceeds maxvst =',mxavst,faterr)
      endif
c     normalize the weights
      norm = 0.0d+00
      do ist=1,nst
         do i= 1,navst(ist)
         norm = norm + wavst(ist,i)
         enddo
      enddo
      do ist=1,nst
         do i= 1,navst(ist)
         wavst(ist,i) = wavst(ist,i)/norm
         enddo
      enddo
c
      write(nlist,
     & '(//,1x,3(1h*)," State averaging informations: ",3(1h*)//)')
c
      if (nst.eq.1) then
      write(nlist,6053)nst
      write(nslist,6053)nst
      else
      write(nlist,6054)nst
      write(nslist,6054)nst
      endif
6053  format(' MCSCF calculation performed for ',i2,1x,'DRT.'/)
6054  format(' MCSCF calculation performed for ',i2,1x,'DRTs.'/)

      write(nlist,
     & '('' DRT  first state   no.of aver.states   weights'')')
      write(nslist,
     & '('' DRT  first state   no.of aver. states   weights'')')
      prtwarn=.false.
      wavst_save=wavst(1,1)
      do ist=1,nst
      write(chstate(1:),'(i1)')nstate(ist)
      write(chstate(2:),'(a13)') '.excit. state'
      if (nstate(ist).eq.0) chstate = 'ground state'
      write(nlist,6057)ist,chstate,navst(ist),
     & (wavst(ist,i),i=1, navst(ist))
      write(nslist,6057)ist,chstate,navst(ist),
     & (wavst(ist,i),i=1, navst(ist))
        do i=1,navst(ist)
         if (wavst(ist,i).ne.wavst_save) prtwarn=.true.
        enddo ! end of: do i=1,navst(ist)
      enddo ! end of: do ist=1,nst
       if (prtwarn) call bummer(
     &  'use equal weights for correct state-averaging procedure!',
     &  0, wrnerr)
6057  format(i3,3x,a14,6x,i3,13x,8(f5.3,1x))
c
      write(nlist,6050)
6050  format(/' The number of hmc(*) eigenvalues and',
     +  ' eigenvectors calculated',
     +  ' each iteration per DRT:')
      write(nlist,'(1x,''DRT.   no.of eigenv.(=ncol)'')')
      do ist=1,nst
      write(nlist,'(1x,i4,6x,i3)')ist,ncol(ist)
      enddo
c
c
         if ((nst.ne.1).and.(lvlprt.ge.1)) then
         write(nlist,*)
         do ist = 1,nst
         write (nlist,6110) ist,navst(ist),(ist,ixx,wavst(ist,ixx),
     &    ixx=1,navst(ist))
         enddo
         endif
6110  format (1x,'state averaging: navst(',i2,') = ',i2/
     $(18x,'wavst(',2i2,') = ',f8.6))
c
c
      if (nst.eq.1) then
         if(nstate(1).eq.0)write(nlist,6060)
         if(nstate(1).gt.0)write(nlist,6070)nstate(1)
      endif
6060  format(/' orbital coefficients are optimized for the',
     +  ' ground state (nstate=0).')
6070  format(/' orbital coefficients are optimized for the',i3,
     +  ' (=nstate) excited state.')
c
      nfci=0
c
c    fciorb(1,i) = symmetry
c    fciorb(2,i) = mo of this symmetry
c    fciorb(3,i) = mask
c
      do 290 i=1,nmotx
         j=fciorb(2,i)
         if(j.le.0)go to 291
            do i1= 1,fciorb(1,i)-1
            j = j + nmpsy(i1)
            enddo
         fcimsk(j)=fciorb(3,i)
         nfci=i
290   continue
291   continue
c
      if(nfci.gt.0)then
         write(nlist,6026)
6026     format
     &    (/' Orbitals included in invariant subspaces:',/,3x,
     &    'symmetry',3x,'orbital',3x,'mask')
         do i=1,nfci
            j=fciorb(2,i)
            do i1= 1,fciorb(1,i)-1
               j = j + nmpsy(i1)
            enddo
            write(nlist,'(5x,i3,5x,i3,1h(,i3,1h),3x,i3)')
     &       fciorb(1,i),fciorb(2,i),j,fcimsk(j)
         enddo
      else
         write(nlist,6080)
6080     format(/' no fciorb(*,*) specified.')
      endif
c
c  flags(1) must be .true., otherwise print out option list and stop.
c
      if(.not.flags(1))then
         do 300 i=2,nflag
            flags(i)=.true.
300      continue
      endif
c
      write(nlist,6100)
6100  format(/' npath(*) options:')
c
      if(.not.flags(1))write(nlist,6201)
6201  format(' the following is the list of npath(*) options:',/
     +        '     npath=1 or flags(1)=.true. is required.')
c
      if(flags(2))write(nlist,6202)ncoupl
6202  format('  2:  orbital-state coupling terms will be included',
     +  ' beginning on iteration ncoupl=',i3)
c
      if(flags(3))write(nlist,6203)
6203  format('  3:  print intermediate timing information.')
c
      if(flags(4))write(nlist,6204)
6204  format('  4:  print 1-e matrices each iteration.')
c
      if(flags(5))write(nlist,6205)
6205  format('  5:  print the hmc(*) matrix each iteration.')
c
      if(flags(6))write(nlist,6206)
6206  format('  6:  print the hessian matrix each iteration.')
c
      if(flags(7))write(nlist,6207)
6207  format('  7:  print the iterative hmc(*) diagonalization',
     +  ' information.')
c
      if(flags(8))write(nlist,6208)
6208  format('  8:  print the iterative psci solution information.')
c
      if(flags(9))write(nlist,6209)
6209  format('  9:  suppress the drt listing.')
c
      if(flags(10))write(nlist,6210)
6210  format(' 10:  suppress the hmc(*) eigenvector listing.')
c
      if(flags(11))write(nlist,6211)
6211  format(' 11:  construct the hmc(*) matrix explicitly.')
c
      if(flags(12))then
         write(nlist,6212)nunitv,nciitr,
     +     mxvadd,nvcimx, (rtolci(i),i=1,mxavst),wnorm
6212     format(' 12:  diagonalize the hmc(*) matrix iteratively.'/
     +    t8,' nunitv=',i2,' nciitr=',i2,
     +    ' mxvadd=',i2,' nvcimx=',i2/
     +    t8,'rtolci(*),wnorm=',(t28,1p8e11.4))
      write(nlist,'(3x,"noldv = ",8i3)')(noldv(ist),ist=1,nst)
      endif
c
      if(flags(13))write(nlist,6213)
6213  format(' 13:  get initial orbitals from the formatted',
     +  ' file, mocoef.')
c
      if(flags(14))write(nlist,6214)
6214  format(' 14:  use initial orbitals from the 1-e',
     +  ' hamiltonian diagonalization, hc=sce.')
c
      if(flags(15))write(nlist,6215)
6215  format(' 15:  get initial orbitals, morb,',
     +  ' from the restart file.')
c
      if(flags(16))write(nlist,6216)
6216  format(' 16:  get initial orbitals, morbn,',
     +  ' from the restart file.')
c
      if(flags(17))write(nlist,6217)
6217  format(' 17:  print the final natural orbitals and occupations.')
c
      if(flags(18))write(nlist,6218)
6218  format(' 18:  skip the 2-e integral transformation on',
     +  ' the first iteration.')
c
      if(flags(19))write(nlist,6219)
6219  format(' 19:  transform the virtual orbitals to',
     +  ' diagonalize qvv(*).')
c
      if(flags(20))write(nlist,6220)
6220  format(' 20:  choose the maximum overlap psci vector instead',
     +  ' of the lowest energy vector each iteration.')
c
      if(flags(21))write(nlist,6221)
6221  format(' 21:  write out the one- and two- electron density',
     +  ' for further use (files:mcd1fl, mcd2fl).')
c
      if(flags(22))write(nlist,6222)
6222  format(' 22:  perform a direct MCSCF calculation.')
c
      if(flags(23))write(nlist,6223)
6223  format(' 23:  use the old integral transformation.')
c
      if(flags(24))write(nlist,6224)
6224  format(' 24:  use the direct integral transformation.')
c
      if(flags(25)) then
      write(nlist,6225)
      endif
6225  format(' 25:  print memory requirements at different levels')

      if(flags(26))write(nlist,6226)
6226  format(' 26:  Read AO integrals in MOLCAS native format ')

      if(flags(27))write(nlist,6227)
6227  format(' 27:  Read AO integrals in DALTON2 native format ')
   
      if(flags(28)) write(nlist,6228)
6228  format(' 28:  use all-state projection in the mitro-iterations')

      if(flags(29)) write(nlist,6229)
6229  format(' 29:  Read MO coefficients in MOLCAS LUMORB format')

      if(flags(30)) write(nlist,6230)
6230  format(' 30:  Compute mcscf (transition) density matrices')

      if(flags(31)) write(nlist,6231)
6231  format(' 31:  In combination with full in-core AO-MO ',/
     .            ' transformation in-core half-sorted integral file')
     
      if(flags(32)) write(nlist,6232)
6232  format(' 32:  Force MO and NO coefficient print out')

      if(.not.flags(1))call bummer('readin: npath=1 required',0,faterr)
      return
      end
c
c**********************************************************************
c
      subroutine mosign( n, t )
c
c  adjust the phase of the orbital transformation matrix t(:,:)
c
c  02-aug-95 written by ron shepard.
c
       implicit none 
       integer n
      real*8 t(n,n)
c
      integer i, j
c
      real*8     zero,     bigd
      parameter( zero=0d0, bigd=0.8d0 )
c
c     # adjust the orbital phases so that t(:,:) is as close to
c     # a unit matrix as possible.  if a diagonal element is
c     # smaller than bigd in magnitude, then don't bother
c     # adjusting anything.
c
      do 200 i = 1, n
         if ( abs( t(i,i) ) .ge. bigd ) then
            if ( t(i,i) .lt. zero ) then
               do 100 j = 1, n
                  t(j,i) = -t(j,i)
100            continue
            endif
         endif
200   continue
      return
      end
c
c**********************************************************************
c
      subroutine rdhb(size,hess,num)
      implicit integer(a-z)
c
      integer iunits,nlist
      common/cfiles/iunits(55)
      equivalence (iunits(1),nlist)
c
c    #  maximal No. of different DRTs
      integer maxnst
      parameter (maxnst=8)
c
c    #  maximal No. of states in one DRT
      integer mxavst
      parameter (mxavst = 50)
c
c    #  maximal total number of states
      integer maxstat
      parameter (maxstat = maxnst*mxavst)
c
      integer hblkw,nhbw,htape
      common/chbw/hblkw(15*maxstat),nhbw,htape
c
      integer lvlprt
      common/prtout/lvlprt
c
      integer blhsz
      parameter (blhsz = 4096)
c
      real*8 hess(size)
      integer i,numx,sizex,blhszx,icount
c
c  bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      read(htape)numx,sizex,blhszx
      if(sizex.ne.size .or. numx.ne.num
     & .or. blhszx.ne.blhsz)then
         write(nlist,6010)size,num,blhsz,sizex,numx,blhszx
         call bummer('rdhb: file error',0,faterr)
      endif
c
       icount = 1
       do i=1,int(size/blhsz)
          call seqrbf(htape,hess(icount),blhsz)
          icount = icount + blhsz
       enddo
      if (mod(size,blhsz).gt.0)
     & call seqrbf(htape,hess(icount),mod(size,blhsz))
      if (lvlprt.ge.1) write(nlist,6020) numx,size,htape
c     if (num.lt.11) then
c        write(0,6020) num,size,htape 
c        ifrom=1
c        do i=1,(size+1)/8
c         ibis =min(i*8,size)
c         write(0,6019) i,(hess(ii),ii=ifrom,ibis)
c         ifrom=ifrom+8
c6019   format(i8,4x,8f12.8)
c        enddo
c     endif

      return
6010  format(/' *** error *** rdhb: size,num,blhsz,
     & sizex,numx,blhszx=',6i10)
6020  format(' rdhb: hessian block',i3,' size=',i10,
     +  ' read from unit=',i3)
      end
c
c**********************************************************************
c
      subroutine wrthb(size,hess,num)
      implicit none 
      integer iunits,nlist
      common/cfiles/iunits(55)
      equivalence (iunits(1),nlist)
c
c    #  maximal No. of different DRTs
      integer maxnst
      parameter (maxnst=8)
c
c    #  maximal No. of states in one DRT
      integer mxavst
      parameter (mxavst = 50)
c
c    #  maximal total number of states
      integer maxstat
      parameter (maxstat = maxnst*mxavst)
c
      integer hblkw,nhbw,htape
      common/chbw/hblkw(15*maxstat),nhbw,htape
c
      integer lvlprt
      common/prtout/lvlprt
c
      integer blhsz
      parameter (blhsz = 4096)
c
      integer size,num
      real*8 hess(size)
      integer i,icount,j
      integer ifrom,ibis,ii
c
      nhbw=nhbw+1
      hblkw(nhbw)=num
*@ifdef  obsolete 
*      if (num.lt.11) then
*         write(0,6021) num,size
*         ifrom=1
*         do i=1,(size+1)/8
*          ibis =min(i*8,size)
*          write(0,6019) i,(hess(ii),ii=ifrom,ibis)
*          ifrom=ifrom+8
*6019   format(i8,4x,8f12.8)
*6021   format('wrthb : num=',i4,4x,'size=',i12)
*         enddo
*      endif
*@endif 

      write(htape)num,size,blhsz
      icount = 1
      do i=1,int(size/blhsz)
          call seqwbf(htape,hess(icount),blhsz)
          icount = icount + blhsz
      enddo
      if (mod(size,blhsz).gt.0)
     &call seqwbf(htape,hess(icount),mod(size,blhsz))
      if (lvlprt.ge.1) write(nlist,6020) num, size, htape, blhsz
6020  format(' wrthb: hessian block',i3,' size=',i8,
     +  ' written to unit=',i3,'in blocks of size',i8)
      return
      end

