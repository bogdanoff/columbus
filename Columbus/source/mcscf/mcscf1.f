!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
cmcscf1.f
cmcscf part=1 of 10.  workspace allocation routine
cversion=5.6 last modified: 16-jan-2001
c deck mcscf
      program mcscf
      use tranlibrary , only : admfreeunit,daclrd
      implicit logical(a-z)
      include "../colib/getlcl.h"

c
c  allocate space and call the driver routine.
c
c  ifirst = first usable space in the the real*8 work array a(*).
c  lcore  = length of workspace array in working precision.
c  mem1   = additional machine-dependent memory allocation variable.
c  a(*)   = workspace array. a(ifirst : ifirst+lcore-1) is useable.
c
      integer ifirst, lcore
c
c     # mem1 and a(*) should be declared below within mdc blocks.
c
c     # local...
c     # lcored = default value for lcore.
c     # ierr   = error return code.
c
      integer mem1,lcored, ierr
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
c     # sets up any communications necessary for parallel execution,
c     # and does nothing in the sequential case.
c
c
       parameter ( lcored = 2 500 000 )
c
c     # use standard f90 memory allocation.
c
      real*8, allocatable,target :: a(:)
c

      integer voffset,vsize,vtop,hwmark
      real*8, allocatable, target :: vdisk(:)  
      real*8, pointer :: vpointer(:)
      common /VDISK/ vpointer, voffset(10),vsize,vtop,hwmark
      integer ga_nodeid

*@ifdef parallel
*c       call mpi_init(ierr)
*       call pbeginf()
*       call ga_initialize()
*@endif 
      call getlcl( lcored, lcore, ierr )
      if ( ierr .ne. 0 ) then
         call bummer('mcscf: from getlcl: ierr=',ierr,faterr)
      endif
*@ifdef parallel
*      call ga_brdcst(12711,lcore,8,0) 
*@endif 
c
c     allocate (vdisk(1000000),stat=ierr)
c     if (ierr.ne.0) then
c      call bummer('mcscf: allocate vpointer failed: ierr=',ierr,faterr)
c     endif
c     vtop=1
c     vsize=1000000
c     voffset(1:10)=-1
c     vpointer => vdisk 

      allocate( a(lcore) ,stat=ierr)
      if (ierr.ne.0) then
         call bummer('mcscf: allocate failed: ierr=',ierr,faterr)
      endif
*@ifdef parallel
*      write(0,*) ga_nodeid(), 'allocated ', lcore 
*@endif 
      hwmark=lcore
      vpointer=>a
      voffset(1:10)=-1


      ifirst = 1
      mem1   = 0

      call assign_unitnumbers
c
c     # lcore, mem1, and ifirst should now be defined.
c     # the values of lcore, mem1, and ifirst are passed to
c     # driver() to be printed.
c     # since these arguments are passed by expression, they
c     # must not be modified within driver().
c
c
*@ifdef parallel
*      if (ga_nodeid().eq.0) then 
*      call driver( a(ifirst), (lcore), (mem1), (ifirst) )
*      else
*      qconv=.false.
*c     subroutine driver_wrap(core,icore,lcore,nlist,nmot,
*c    .                       nexo,nsym,nbpsy,sewd,dlt2,lum,c,sao,
*c    .                        qcoupl,qsort,qconvg,lcoremain,flag31)
* 
*      call driver_wrap(a(ifirst),a(ifirst),lcore, 0,0, 
*     .            0,  0    ,0    ,0   ,0   ,0  ,0,  0,
*     .                                  0,   0,0,0,0)       
*      endif
*@else
      call driver( a(ifirst), (lcore), (mem1), (ifirst) )
*@endif

!     close all units assigned unit-numbers by admfreeunit
!     eliminate plethora of close statements
!     i = admfreeunit(2)
 

c
c
c     # return from driver() implies successful execution.
c
c     # message-passing cleanup: stub if not in parallel
c
      deallocate(a)
*@ifdef parallel
*       call ga_terminate()
*       call mpi_finalize(ierr)
*@endif 
      call bummer('normal termination',0,3)
      stop 'end of mcscf'
      end

      subroutine assign_unitnumbers
       
      use tranlibrary , only : admfreeunit,dainit,daini
      implicit none

c
c    #  maximal No. of different DRTs
      integer maxnst
      parameter (maxnst=8)
c
c    #  maximal No. of states in one DRT
      integer mxavst
      parameter (mxavst = 50)
c
c    #  maximal total number of states
      integer maxstat
      parameter (maxstat = maxnst*mxavst)
c

      integer nlist,nin,aoints,aoints2,mocoef,nvfile,nslist,nft
      integer ndft,nrestp,ntemp,scrtfl,mcd1fl,mcd2fl,ndrt,mos,hdiagf
      integer info2,mrptfl,tmin,mocout,htape,stape,nunit1,nunit2
      integer  iunits,jj
      common/cfiles/iunits(55)
      equivalence (iunits(1),nlist)
      equivalence (iunits(2),nin)
      equivalence (iunits(3),aoints)
      equivalence (iunits(4),aoints2)
      equivalence (iunits(7),mocoef)
      equivalence (iunits(8),nvfile)
      equivalence (iunits(9),nslist)
      equivalence (iunits(10),nft)
      equivalence (iunits(11),ndft)
      equivalence (iunits(12),nrestp)
      equivalence (iunits(13),ntemp)
      equivalence (iunits(14),scrtfl)
      equivalence (iunits(15),mcd1fl)
      equivalence (iunits(16),mcd2fl)
      equivalence (iunits(17),ndrt)
      equivalence (iunits(19),mos)
      equivalence (iunits(20),hdiagf)
      equivalence (iunits(21),info2)
      equivalence (iunits(22),mrptfl)
      equivalence (iunits(23),tmin)
      equivalence (iunits(24),mocout)
      integer hblkw, nhbw,lenbfs,h2bpt,len1,irec1,len2,irec2
c
      common/chbw/hblkw(15*maxstat),nhbw,htape
c
      common/cbufs/stape,lenbfs,h2bpt
c
      common/da2/nunit2,len2,irec2
c



      nslist=admfreeunit(1)
      aoints=admfreeunit(1)
      aoints2=admfreeunit(1)
      mocoef=admfreeunit(1)
      htape=admfreeunit(1)
      stape=admfreeunit(1)
      nrestp=admfreeunit(1)
      ntemp=admfreeunit(1)
      mcd1fl=admfreeunit(1)
      mcd2fl=admfreeunit(1)
      mrptfl=admfreeunit(1)
      scrtfl=admfreeunit(1)
      nunit2=admfreeunit(1)
      nin=admfreeunit(1)
      tmin=admfreeunit(1)
      mocout=admfreeunit(1)
      mos=admfreeunit(1)
      hdiagf=admfreeunit(1)
      info2=admfreeunit(1)
      nlist=admfreeunit(1)
      nft=admfreeunit(1)
      ndft=admfreeunit(1)
      nvfile=admfreeunit(1)
      ndrt=admfreeunit(1)
!     jj=admfreeunit(3)
      call dainit
      call daini( 5, nunit2, 'mcsda2', 0,0 )
      return
      end

