!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
      subroutine madva(b,x,y,s)
c
c  form the matrix-vector product  b * x using the scratch
c  vector s and update y.
c
c  y(ip) = y(ip) + sum(ar) b(ar,ip)*x(ar)
c
c  written by ron shepard.
c
       implicit none
      integer nmomx
      parameter(nmomx=1023)
c
      integer mctype,mapivm,mapmi,mapma,mapmd,mapmv,mapml,
     & mapmm,symi,symv,symm,syma,symd,mcres
      common/cmomap/mctype(nmomx),mapivm(nmomx),mapmi(nmomx),
     & mapma(nmomx),mapmd(nmomx),mapmv(nmomx),mapml(0:nmomx),
     & mapmm(nmomx),
     & symi(nmomx),symv(nmomx),symm(nmomx),
     & syma(nmomx),symd(nmomx),mcres(nmomx)
c
c
      integer nxy,mult,nsym,nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer ndpsy(8),namot,nvpsy(8)
      equivalence (nxy(1,9),ndpsy(1))
      equivalence (nxy(1,17),nvpsy(1))
      equivalence (nxtot(10),namot)
c
      integer nipsy,nsi,nnsi,n2si,iri1,iri2,nimot,nnit,n2it,
     & nsmi,nmit,nsvi,nvit,nsad,nsvd,nsva,symtmm,symtii,symtvi,
     & intoff_ci
      common/csymi/
     & nipsy(8),nsi(8),nnsi(8),n2si(8),iri1(8),iri2(8),nimot,nnit,
     & n2it,nsmi(8),nmit,nsvi(8),nvit,nsad(8),nsvd(8),nsva(8),
     & symtmm(8),symtii(8),symtvi(8),intoff_ci(8,8)
c
      real*8 b(*),x(*),y(*),s(*)
c
c   ##  integer section
c
      integer ar
      integer i,ip
      integer na,ni
      integer psym,p,pr
      integer r,rsym
c
      pr=1
      ip=1
      do 400 p=1,namot
         psym=symm(mapma(p))
         ni=ndpsy(psym)
         if(ni.eq.0)go to 400
c
         ar=1
         do 300 r=1,namot
            rsym=symm(mapma(r))
            na=nvpsy(rsym)
            if(na.eq.0)go to 300
c
c  form s(ip)=sum(a) b(a,i,pr)*x(ar)  and update y(*).
c  b(na,ni), x(na), s(ni).
c
            call mtxv(b(pr),ni,x(ar),na,s)
            do 100 i=1,ni
               y(ip-1+i)=y(ip-1+i)+s(i)
100         continue
            pr=pr+na*ni
            ar=ar+na
300      continue
         ip=ip+ni
400   continue
c
      return
      end
c
c*******************************************************************
c
      subroutine mvaad(b,x,y,s)
c
c  form the matrix-vector product b * x using the scratch
c  vector s and update y.
c
c  y(ar) = y(ar) + sum(ip) b(ar,ip)*x(ip)
c
c  written by ron shepard.
c
       implicit none
      integer nmomx
      parameter(nmomx=1023)
c
      integer mctype,mapivm,mapmi,mapma,mapmd,mapmv,mapml,
     & mapmm,symi,symv,symm,syma,symd,mcres
      common/cmomap/mctype(nmomx),mapivm(nmomx),mapmi(nmomx),
     & mapma(nmomx),mapmd(nmomx),mapmv(nmomx),mapml(0:nmomx),
     & mapmm(nmomx),
     & symi(nmomx),symv(nmomx),symm(nmomx),
     & syma(nmomx),symd(nmomx),mcres(nmomx)
c
c
      integer nxy,mult,nsym,nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer ndpsy(8),namot,nvpsy(8)
      equivalence (nxy(1,9),ndpsy(1))
      equivalence (nxy(1,17),nvpsy(1))
      equivalence (nxtot(10),namot)
c
      integer nipsy,nsi,nnsi,n2si,iri1,iri2,nimot,nnit,n2it,
     & nsmi,nmit,nsvi,nvit,nsad,nsvd,nsva,symtmm,symtii,symtvi,
     & intoff_ci
      common/csymi/
     & nipsy(8),nsi(8),nnsi(8),n2si(8),iri1(8),iri2(8),nimot,nnit,
     & n2it,nsmi(8),nmit,nsvi(8),nvit,nsad(8),nsvd(8),nsva(8),
     & symtmm(8),symtii(8),symtvi(8),intoff_ci(8,8)
c
      real*8 b(*),x(*),y(*),s(*)
c
c   ##  integer section
c
      integer a,ar
      integer ip
      integer na,ni
      integer p,pr,psym
      integer r,rsym
c
      pr=1
      ip=1
      do 400 p=1,namot
         psym=symm(mapma(p))
         ni=ndpsy(psym)
         if(ni.eq.0)go to 400
c
         ar=1
         do 300 r=1,namot
            rsym=symm(mapma(r))
            na=nvpsy(rsym)
            if(na.eq.0)go to 300
c
c  form s(ar)=sum(i) b(a,i,pr)*x(ip)  and update y(*).
c  b(na,ni), x(ni), s(na).
c
            call mxv(b(pr),na,x(ip),ni,s)
            do 100 a=1,na
               y(ar-1+a)=y(ar-1+a)+s(a)
100         continue
            pr=pr+na*ni
            ar=ar+na
300      continue
         ip=ip+ni
400   continue
c
      return
      end
      subroutine mvava(b,x,y,s)
c
c  form the matrix-vector product  b * x using the scratch
c  vector s and update y.
c
c  y(ap) = y(ap) + sum(cr) b(ap,cr)*x(cr)
c
c  written by ron shepard.
c
       implicit none
      integer nmomx
      parameter(nmomx=1023)
c
      integer mctype,mapivm,mapmi,mapma,mapmd,mapmv,mapml,
     & mapmm,symi,symv,symm,syma,symd,mcres
      common/cmomap/mctype(nmomx),mapivm(nmomx),mapmi(nmomx),
     & mapma(nmomx),mapmd(nmomx),mapmv(nmomx),mapml(0:nmomx),
     & mapmm(nmomx),
     & symi(nmomx),symv(nmomx),symm(nmomx),
     & syma(nmomx),symd(nmomx),mcres(nmomx)
c
      integer nxy,mult,nsym,nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer namot,nvpsy(8)
      equivalence (nxtot(10),namot)
      equivalence (nxy(1,17),nvpsy(1))
c
      integer nipsy,nsi,nnsi,n2si,iri1,iri2,nimot,nnit,n2it,
     & nsmi,nmit,nsvi,nvit,nsad,nsvd,nsva,symtmm,symtii,symtvi,
     & intoff_ci
      common/csymi/
     & nipsy(8),nsi(8),nnsi(8),n2si(8),iri1(8),iri2(8),nimot,nnit,
     & n2it,nsmi(8),nmit,nsvi(8),nvit,nsad(8),nsvd(8),nsva(8),
     & symtmm(8),symtii(8),symtvi(8),intoff_ci(8,8)
c
      real*8 b(*),x(*),y(*),s(*)
c
c   ##  integer section
c
      integer a,ap
      integer c,cr
      integer na,nc
      integer psym,p,pq
      integer r,rsym


c
      pq=1
      ap=1
      do 400 p=1,namot
         psym=symm(mapma(p))
         na=nvpsy(psym)
         if(na.eq.0)go to 400
c
         cr=1
         do 300 r=1,p
            rsym=symm(mapma(r))
            nc=nvpsy(rsym)
            if(nc.eq.0)go to 300
c
c  form s(ap)=sum(c) b(c,a,pr)*x(cr)  and update y(*).
c  b(nc,na), x(nc), s(na).
c
            call mtxv(b(pq),na,x(cr),nc,s)
            do 100 a=1,na
               y(ap-1+a)=y(ap-1+a)+s(a)
100         continue
c
            if(p.ne.r)then
c
c  form s(cr)=sum(a) b(c,a,pr)*x(ap)  and update y(*).
c  b(nc,na), x(na), s(nc).
c
               call mxv(b(pq),nc,x(ap),na,s)
               do 200 c=1,nc
                  y(cr-1+c)=y(cr-1+c)+s(c)
200            continue
            endif
            pq=pq+na*nc
            cr=cr+nc
300      continue
         ap=ap+na
400   continue
c
      return
      end
      subroutine mvdvd(b,x,y,s)
c
c  form the matrix-vector product b * x using the scratch
c  vector s and update y.
c
c  y(ai) = y(ai) + sum(cj) b(ai,cj)*x(cj)
c
c  written by ron shepard.
c
       implicit none
      integer nmomx
      parameter(nmomx=1023)
c
      integer mctype,mapivm,mapmi,mapma,mapmd,mapmv,mapml,
     & mapmm,symi,symv,symm,syma,symd,mcres
      common/cmomap/mctype(nmomx),mapivm(nmomx),mapmi(nmomx),
     & mapma(nmomx),mapmd(nmomx),mapmv(nmomx),mapml(0:nmomx),
     & mapmm(nmomx),
     & symi(nmomx),symv(nmomx),symm(nmomx),
     & syma(nmomx),symd(nmomx),mcres(nmomx)
c
c
      integer nxy,mult,nsym,nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer ndmot,nvpsy(8)
      equivalence (nxtot(7),ndmot)
      equivalence (nxy(1,17),nvpsy(1))
c
      integer nipsy,nsi,nnsi,n2si,iri1,iri2,nimot,nnit,n2it,
     & nsmi,nmit,nsvi,nvit,nsad,nsvd,nsva,symtmm,symtii,symtvi,
     & intoff_ci
      common/csymi/
     & nipsy(8),nsi(8),nnsi(8),n2si(8),iri1(8),iri2(8),nimot,nnit,
     & n2it,nsmi(8),nmit,nsvi(8),nvit,nsad(8),nsvd(8),nsva(8),
     & symtmm(8),symtii(8),symtvi(8),intoff_ci(8,8)
c
      real*8 b(*),x(*),y(*),s(*)
c
c   ##  integer section
c
      integer a,ai
      integer c,cj
      integer isym,i,ij
      integer j,jsym
      integer na,nc
c
      ij=1
      ai=1
      do 400 i=1,ndmot
         isym=symm(mapmd(i))
         na=nvpsy(isym)
         if(na.eq.0)go to 400
c
         cj=1
         do 300 j=1,i
            jsym=symm(mapmd(j))
            nc=nvpsy(jsym)
            if(nc.eq.0)go to 300
c
c  form s(ai)=sum(c) b(c,a,ij)*x(cj)  and update y(*).
c  b(nc,na), x(nc), s(na).
c
            call mtxv(b(ij),na,x(cj),nc,s)
            do 100 a=1,na
               y(ai-1+a)=y(ai-1+a)+s(a)
100         continue
            if(i.ne.j)then
c
c  form s(cj)=sum(a) b(c,a,ij)*x(ai)  and update y(*).
c  b(nc,na), x(na), s(nc).
c
               call mxv(b(ij),nc,x(ai),na,s)
               do 200 c=1,nc
                  y(cj-1+c)=y(cj-1+c)+s(c)
200            continue
            endif
            ij=ij+na*nc
            cj=cj+nc
300      continue
         ai=ai+na
400   continue
c
      return
      end
c deck noeff
      subroutine noeff(cmo,d1,cnoao,occ,sa,sb,sx)
c
c    diagonalise the effective one-particle density matrix
c    and transform the natural orbitals from mo- to ao-basis
c
c  input:
c  cmo(*)   = mo coefficients.
c  d1(*)    = one particle density matrix
c  sa(*)    = scratch matrix.
c  sb(*)    = scratch matrix
c  sx(*)    = scratch vector.
c
c  output:
c  cnoao    = effecitive natural orbitals in the the ao basis
c  occ      = neffecitive atural orbital occupations
c
c  27-aug-90 mxm() call added. -rls
c  (m.e.)
c
       implicit none
      real*8 cmo(*)
      real*8 d1(*)
      real*8 cnoao(*),occ(*)
      real*8 sa(*),sx(*),sb(*)
c
      integer nxy,mult,nsym,nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nbpsy(8),nbmt,nmpsy(8),nsm(8),nnsm(8),n2sm(8)
      equivalence (nxy(1,1),nbpsy(1))
      equivalence (nxy(1,5),nmpsy(1))
      equivalence (nxy(1,6),nsm(1))
      equivalence (nxy(1,7),nnsm(1))
      equivalence (nxy(1,8),n2sm(1))
      equivalence (nxtot(16),nbmt)
c
      integer nipsy,nsi,nnsi,n2si,iri1,iri2,nimot,nnit,n2it,
     & nsmi,nmit,nsvi,nvit,nsad,nsvd,nsva,symtmm,symtii,symtvi,
     & intoff_ci
      common/csymi/
     & nipsy(8),nsi(8),nnsi(8),n2si(8),iri1(8),iri2(8),nimot,nnit,
     & n2it,nsmi(8),nmit,nsvi(8),nvit,nsad(8),nsvd(8),nsva(8),
     & symtmm(8),symtii(8),symtvi(8),intoff_ci(8,8)
c
c   ##  integer section
c
      integer imo,isym
      integer nm,nb
      integer pq1
      integer x1,x1y1
c
cgk debug
      imo = 1
      do 200 isym=1,nsym
          nm=nmpsy(isym)
          if(nm.eq.0)go to 200
          x1y1=n2sm(isym)+1
          x1=nsm(isym)+1
          pq1=nnsm(isym)+1
c
          call diaden( nm, sa(x1y1), occ(x1), d1(pq1), sb, sx )
          nb = nbpsy(isym)
c
          call mxm( cmo(imo),nb, sa(x1y1),nm, cnoao(imo),nm )
          imo = imo + nmpsy(isym)*nbpsy(isym)
c
200   continue
c
      return
c
      end
c deck orthot
      subroutine orthot(na,nb,u,ierr)
c
c  use the modified schmidt procedure to produce orthonormal vectors.
c
c  input:
c  na   = number of rows in u(*,*).
c  nb   = number of vectors (columns) in u(*,*).
c  u(*) = initial input vectors.
c
c  output:
c  ierr = 0 for successful completion, negative for argument error,
c         or positive for orthonormalization error.
c  u(*) = modified vectors such that u(transpose) * u = 1 (the
c         unit matrix of dimension nb).  the transformation matrix t,
c         where u(new) = u(old)*t, is upper triangular and is not
c         stored explicitly.  if ierr>0 then the first ierr-1 vectors
c         are orthonormal, the ierr'th vector has a zero norm, and the
c         remaining nb vector are modified but not necessarily
c         orthonormal.
c
c  written by ron shepard.
c  version date: 1-june-1988
c
       implicit none
      integer i, ierr
      integer j
      integer k
      integer na,nb
      real*8 u(na,nb)
c
      real*8 ddot_wr,dnrm2_wr
      real*8    zero,    one,    term
      parameter(zero=0d0,one=1d0)
c
c  check input.
c
      if(nb.lt.0 .or. nb.gt.na)then
         ierr=-1
         return
      endif
c
c  successively normalize each vector, then subtract that
c  normalized component from the remaining updated vectors.
c  (note that classical grahm-schmidt subracts the components of
c  the original vectors from the updated vector followed by
c  normalization. -rls)
c
      do 200 k=1,nb
c
c  normalize u(*,k).
c
         term=dnrm2_wr(na,u(1,k),1)
         if(term.eq.zero)then
            ierr=k
            return
         endif
         term=one/term
         call dscal_wr(na,term,u(1,k),1)
c
c  orthogonalize the remaining vectors to u(*,k).
c
         do 100 j=(k+1),nb
            term=-ddot_wr(na,u(1,k),1,u(1,j),1)
            if(term.ne.zero)call daxpy_wr(na,term,u(1,k),1,u(1,j),1)
100      continue
200   continue
c
      ierr=0
      return
      end
c deck prtdrt
      subroutine prtdrt(levprt,nact,nrow,nsym,
     & nj,njskp,modrt,syml,a,b,l,xbar,y,xp,z)
c
c  print drt information from levels levprt(1) to levprt(2).
c
c  written by ron shepard.
c
       implicit none
      integer nfilmx, iunits
      parameter(nfilmx=31)
      common/cfiles/iunits(nfilmx)
      integer nlist
      equivalence (iunits(1),nlist)
c
      integer nrow,nsym, nact
      integer a(nrow)
      integer b(nrow)
      integer i,ileb,ir1, ir2, irow, isam, isym, ilev
      integer levprt(2),l(0:3,nrow), l1, l2
      integer modrt(0:nact)
      integer nj(0:nact),njskp(0:nact)
      integer syml(0:nact)
      integer xp(nsym,nrow),xbar(nsym,nrow)
      integer y(nsym,0:3,nrow)
      integer z(nsym,nrow)
c
6010  format(/' level',i3,' through level',i3,' of the drt:'//
     & 1x,'row',' lev',' a',' b','  mo',' syml',
     & '  l0 ',' l1 ',' l2 ',' l3 ',
     & 'isym',' xbar ', '  y0  ','  y1  ','  y2  ','  xp  ','   z')
6030  format(/1x,i3,i4,i2,i2,i4,i4,i5,3i4,i4,6i6)
6040  format(1x,36x,i4,6i6)
6050  format(1x,36('.'))
c
c  restrict array references to a meaningful range.
c
      l1=min(levprt(1),levprt(2))
      l2=max(levprt(1),levprt(2))
      l1=min(max(l1,0),nact)
      l2=min(max(l2,0),nact)
c
      write(nlist,6010)l1,l2
      do 300 ilev=l2,l1,-1
         ir1=njskp(ilev)+1
         ir2=njskp(ilev)+nj(ilev)
         do 200 irow=ir1,ir2
            isym=1
            write(nlist,6030)irow,ilev,a(irow),b(irow),
     &       modrt(ilev),syml(ilev),(l(i,irow),i=0,3),
     &       isym,xbar(isym,irow),(y(isym,i,irow),i=0,2),
     &       xp(isym,irow),z(isym,irow)
            if(nsym.eq.1)go to 200
            do 100 isym=2,nsym
               write(nlist,6040)
     &          isym,xbar(isym,irow),(y(isym,i,irow),i=0,2),
     &          xp(isym,irow),z(isym,irow)
100         continue
200      continue
         write(nlist,6050)
300   continue
c
      return
      end
c deck prtfn
      subroutine prtfn(funit,iunit,fdiscr,nlist)
c
c  print a fully qualified file name for an opened file.
c
c  funit  = fortran unit number.
c  iunit  = internal unit number.
c  fdiscr = brief description of the file.
c  nlist  = listing file unit number.
c
      implicit integer(a-z)
      character*(*) fdiscr
      integer funit,iunit,nlist
c
      character*60 fname
c
      inquire(unit=funit,name=fname)
      write(nlist,6100)fdiscr,funit,iunit,fname
6100  format(/1x,a,': fortran unit=',i2,', name(',i2,')=',a)
c
      return
      end
c deck rdhdrt_ci
      subroutine rdhdrt_ci(cidrt,nsym,nmot,nmpsy,nexo,title,ibuf)
c
c  read the first few records of the formatted ci drt file to
c  check for consistency.
c
c  input:
c  cidrt = drt unit number.
c  nsym = number of symmetry blocks on the integral file.
c  nmot = total number of orbitals from the integral file.
c  nmpsy(*) = number of orbitals in each symmetry block.
c  ibuf(*) = integer buffer for reading drt arrays.
c
c  output:
c  title = drt title.
c  explicit variable declaration: 17-apr-02, m.dallos
c  modified to utilize rddbl by galen f. gawboy february 1, 1992.
c  nexo(*)  = number of external orbitals in each symmetry block
c
      implicit integer(a-z)
c
      integer nfilmx, iunits
      parameter(nfilmx=31)
      common/cfiles/iunits(nfilmx)
      equivalence (iunits(1),nlist)
c
c      include 'option.inc/list'
      integer numopt
      parameter(numopt=10)
      integer cigopt,print,fresdd,fresaa,fresvv,samcflag
      common/option/cigopt(numopt)
      equivalence (cigopt(1),print)
      equivalence (cigopt(2),fresdd)
      equivalence (cigopt(3),fresaa)
      equivalence (cigopt(4),fresvv)
      equivalence (cigopt(5),samcflag)
c
c symmetry label information
      character*4 labels(8)
      character*80 cfmt
      common /symlab/ labels
      character*80 title
      integer nmpsy(8),ibuf(*),nexo(8)
c
      integer nmot
c
      logical error
c
c  bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
*@ifdef spinorbit
      logical spnorb, spnodd
      integer lxyzir(3)
*@endif

c
      error=.false.
c
      rewind cidrt
c
c read the cidrt title
c
      read(cidrt,'(a)',iostat=ierr)title
      if(ierr.ne.0)call bummer('rchdrt: title ioerr=',ioerr,faterr)
*@ifdef spinorbit
      read(cidrt,*,iostat=ierr)spnorb, spnodd, lxyzir
      if(ierr.ne.0)call bummer('rdhead: spnorb, ierr=',ierr,faterr)
      if (spnorb) call bummer('rdhead: no SO-CI calcs',0,faterr)
*@endif

c
c  get number of integer drt parameters, buffer length, and version.
c
      call rddbl(cidrt, 3, ibuf, 3 )
      vrsion  = ibuf(1)
      lenbuf  = ibuf(2)
      lidrt   = ibuf(3)
c
*@ifdef spinorbit
      if ( vrsion .ne. 6 ) then
         call bummer
     .    ('DRT version 6 required (index vector compression)'
     .  // ' csym(*) deleted',vrsion,2)
      endif
*@else
*      if ( vrsion .ne. 3 ) then
*         write(nlist,*)
*     &    '*** warning: rdhdrt: drt version .ne. 3 *** vrsion=',vrsion
*      endif
*@endif
c
c  read the header integers.
c
       call rddbl(cidrt, lenbuf, ibuf, lidrt)
c
c  check some variables for consistency...
c
      nmotd=ibuf(1)
      if(nmot.gt.nmotd)then
         write(nlist,6010)'nmot',nmot,nmotd
         error=.true.
      endif
c
      nsymd=ibuf(6)
      if(nsym.ne.nsymd)then
         write(nlist,6010)'nsym',nsym,nsymd
         error=.true.
      endif
c
      niot=ibuf(2)
c
      nfctd=ibuf(3)
      nfvtd=ibuf(4)
c
c
      if(nfctd.ne.0)then
         write(nlist,6010)'nfctd',nfctd
         error=.true.
      endif
      if(nfvtd.ne.0)then
         write(nlist,6010)'nfvtd',nfvtd
         error=.true.
      endif
c
c     read in symmetry labels
c
      call rddbl(cidrt, lenbuf, ibuf, nsymd )
c        do 10 i =1,nsym
c           write(labels(i), '(a4)' ) ibuf(i)
c10       continue
      call wtlabel(labels,nsymd,'(a4)',ibuf)


c
c
      write(nlist,'(1x,a,8(a4,1x))') 'symmetry labels: ',
     +                            (labels(i),i=1,nsym)
c
c  read the orbital-to-level mapping vector, map(*)...
c  not used here.
c
       call rddbl( cidrt, lenbuf, ibuf, -nmotd)
c
c
c  read the reference occupations for the internal orbitals mu(*)...
c  not used here.
c
      call rddbl(cidrt, lenbuf, ibuf, -niot)
c
c
c  read nmpsy(*) on the drt (includes frozen orbitals) and compare
c  to the input values...
c  use ibuf(*) as a buffer.
c
      call rddbl( cidrt, lenbuf, ibuf, nsym)
c
c
      do 20 i=1,nsym
         if(nmpsy(i).ne.ibuf(i))then
            write(nlist,6010)'nmpsy(*)',i,nmpsy(i),ibuf(i)
            error=.true.
         endif
20    continue
c

c
c  read nexo(*) on the drt (includes frozen orbitals) and compare
c  to the input values...
c  use ibuf(*) as a buffer.
c
      call rddbl( cidrt, lenbuf, nexo, nsym)



50    continue
c
c  write out drt info.
c
      write(nlist,6030)title
      write(nlist,6040)nmotd,nfctd,nfvtd,nmot,niot
c
      if(error)call bummer('rchdrt: drt error',0,faterr)
c
      return
c
6010  format(' *** rchdrt error *** ',a,3i8)
6030  format(/' ci drt information:'/(1x,a))
6040  format(' nmotd =',i4,' nfctd =',i4,' nfvtd =',i4,' nmot  =',i4/
     & ' niot  =',i4)
      end
c deck rcmprs
      subroutine rcmprs(nmaxr,ndimr,ndims,noldr,nolds,ncfx,
     & br,cr,wr,zr,r,bxr,cxr,scr)
c
c  compress the r(*,*) expansion vector space to a reduced dimension
c  and update the corresponding reduced arrays.
c
c  this version reduces the space all the way to a dimension of one
c  determined by the current reduced vector zr(*).
c
c  7-june-88  reduce to one expansion vector (rls).
c  written by ron shepard.
c
      implicit integer(a-z)
      real*8 br(*),cr(nmaxr,*),wr(*),zr(*),r(ndimr,*),
     & bxr(ndimr,*),cxr(ndims,*),scr(*)
c
      real*8 temp, ddot_wr
c
      real*8    zero,    one
      parameter(zero=0d0,one=1d0)
c
c  reduce r(*,*).
c
      call dscal_wr(ndimr,zr(1),r(1,1),1)
      do 10 i=2,noldr
         temp=zr(i)
         if(temp.ne.zero)call daxpy_wr(ndimr,temp,r(1,i),1,r(1,1),1)
10    continue
c
c  reduce bxr(*,*).
c
      call dscal_wr(ndimr,zr(1),bxr(1,1),1)
      do 20 i=2,noldr
         temp=zr(i)
         if(temp.ne.zero)call daxpy_wr(ndimr,temp,bxr(1,i),1,bxr(1,1),1)
20    continue
c
      if(nolds.ne.0)then
c
c  reduce cxr(*,*).
c
         call dscal_wr(ncfx,zr(1),cxr(1,1),1)
         do 30 i=2,noldr
            temp=zr(i)
            if(temp.ne.zero)call daxpy_wr(ncfx,temp,cxr(1,i),1,cxr(1
     + ,1),1)

30       continue
c
c  reduce cr(*,*).
c
         call mxma(cr,nmaxr,1, zr,1,1, scr,1,1, nolds,noldr,1)
         call dcopy_wr(nolds,scr,1,cr(1,1),nmaxr)
      endif
c
c  reduce wr(*).
c
      wr(1)=ddot_wr(noldr,wr,1,zr,1)
c
c  reduce br(*).
c
      call wzero(noldr,scr,1)
      call smxvu(br,zr,scr,noldr,scr,0)
      br(1)=ddot_wr(noldr,scr,1,zr,1)
c
c  reduce zr(*).
c
      zr(1)=one
c
c  update the expansion vector dimension.
c
      noldr=1
c
      return
      end
c deck rdbks
      subroutine rdbks(add0,lendar,nvpbk,maxrd,idafl,
     & irecpt,values,dabuf,dabufv,idabuf)
      use paralib 
c
c  read a set of chained direct access records.
c
c  input:
c  add0 = address offset to be used.
c  lendar = da record length.
c  nvpbk = maximum number of values in each da record.
c  maxrd = maximum number of values to be read.
c  idafl = da file index.
c  irecpt = first da record in the chain.
c  dabuf(1:lendar) = da record buffer.
c  dabufv(1:nvpbk) = da record values buffer.
c  idabuf(1:nvpbk) = da addresses buffer.
c
c  output:
c  values(1:maxrd) = values from the da file.
c
      implicit integer(a-z)
c
      real*8 values(maxrd),dabuf(lendar),dabufv(nvpbk)
      integer idabuf(nvpbk)
c
      integer info(2)
c
c  initialize values(*)...
c
      call wzero(maxrd,values,1)
c
c  process buffers.
c
      irec=irecpt
100   continue
      if(irec.ne.0)then
c
         call dardr(idafl,dabuf,irec)
c
c  unpack record info...
c
         nuw=2
         call ulab32(dabuf(lendar),info,nuw)
         irec=info(1)
         num=info(2)
c        if(irec.ne.0)call dafnd(idafl,irec)
c
c  move/unpack values...
c
         call dcopy_wr(num,dabuf,1,dabufv,1)
c
c  unpack labels...
c
         nuw=num
         call ulab32(dabuf(nvpbk+1),idabuf,nuw)
c
c  process integrals in the buffer...
c
         do 10 i=1,num
            values(idabuf(i)-add0)=dabufv(i)
c           if ((idabuf(i)-add0).lt.0 .or. (idabuf(i)-add0).gt.maxrd)
c    .      write(6,2001) i,idabuf(i),idabuf(i)-add0
 2001    format('rdbks: i=',2i8,' to ',i8)
10       continue
c
c  next buffer.
c
         go to 100
      endif
c
c  finished...
c
      return
      end
c deck rdhcid
      subroutine rdhcid(nlist,denfl,infocd,ntitle,title,nsym,nmot,
     &                  nbpsy,lheuristic)
c
c  read the header info from the ci density matrix file.
c
c  written by ron shepard.
c
       implicit none
      character*80 title(*)
      integer infocd(*)
      integer ntitle,nsym,nmot,ninfo,nenrgy,ierr,nmap
      logical error,lheuristic
      integer i, nmomx, ntitmx, denfl,nlist,idummy, nsymx, nengmx
      parameter(nmomx = 1023, ntitmx = 30, nsymx=8, nengmx=20)
      real*8 cienrg(nengmx)
      character*4 slabel(nsymx)
      character*8 cidlab(nmomx)
      integer nbpsy(nsymx),cietyp(nengmx), nbpsyn(nsymx)
      integer nbpsy_fc(nsymx),nexo_c(nsymx),nextfc(nsymx)

       real*8 energy
       integer ienergy
       common /sifcid/ energy,ienergy

c
      real*8    one,     zero
      parameter(one=1d0, zero=0d0)

      integer assume_fc,nmpsy_fc(nsymx),mapmo_fc(nmomx),nexo_fc(nsymx)
      common /fcversion/ assume_fc,nmpsy_fc,mapmo_fc,nexo_fc

c
      integer nmotn, nsymn
c
c  bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
      error = .false.
c
      rewind denfl
      call sifrh1(
     1 denfl, ntitle, nsymn, nmotn,
     2 ninfo, nenrgy, nmap, ierr)
c
      if (ierr .ne. 0) then
         call bummer('rdhcid: from sifrh1, ierr=',ierr,faterr)
      elseif (ntitle .gt. ntitmx) then
         call bummer('rdhcid: from sifrh1, ntitle=',ntitle,faterr)
      elseif (ninfo .gt. 10) then
         call bummer('rdhcid: from sifrh1, ninfo=',ninfo,faterr)
      elseif (nsymn .ne. nsym) then
         call bummer('rdhcid: nsym inconsistant ',nsymn,wrnerr)
c     elseif ( nenrgy .ne. 1) then
c        call bummer('rdhcid: from sifrh1, nenrgy=',nenrgy,faterr)
      elseif (nmot .ne. nmotn .and. assume_fc.eq.0) then
         call bummer('rdhcid: inconsistent nmot, nmotn=',nmotn,faterr)
      endif
c
c     #ignore map
         nmap = 0
      call sifrh2(
     1 denfl, ntitle, nsym, nmotn,
     2 ninfo, nenrgy, nmap, title,
     3 nbpsyn, slabel, infocd, cidlab,
     4 cietyp, cienrg, idummy, idummy,
     5 ierr)
c
      do 20 i=1,nsym
         if(nbpsy(i).ne.nbpsyn(i) .and. assume_fc.eq.0)then
            write(nlist,6010)'nmpsy(*)',i,nbpsy(i),nbpsyn(i)
            error=.true.
         endif
         nmpsy_fc(i)=nbpsyn(i)
20    continue
      if (error) call bummer('rdhcid:  inconsitent nbpsy', 0, faterr)

c
c      search for nexo header
c
       lheuristic=.false.
       do i=1,ntitle
        if (title(i)(1:8).eq.'NEXO(*)=') then
          read(title(i)(9:),'(8i4)') nexo_fc(1:8)
          write(6,'(a,8i4)') 'extracted nexo info=',nexo_fc(1:8)
        elseif (title(i)(1:6).eq.'CI+DV3') then
           write(6,*) 'This is an heuristic CI+DV3 density!'
           lheuristic=.true.
        elseif (title(i)(1:6).eq.'CI+DV2') then
           write(6,*) 'This is an heuristic CI+DV2 density!'
           lheuristic=.true.
        elseif (title(i)(1:6).eq.'CI+DV1') then
           write(6,*) 'This is an heuristic CI+DV1 density!'
           lheuristic=.true.
        endif
       enddo

c
6010  format(' *** rdhcid error *** ',a,3i8)
      write(nlist,6020)(title(i),i=1,ntitle)
6020  format(/' ci density file header information:'/(1x,a))
      write(nlist,6040)(i,nbpsy(i),i=1,nsym)
6040  format(' (isym:nbpsy)',8(i3,':',i3))
ctm   print energy and type of calculation 
      call sifpre(nlist, nenrgy, cienrg, cietyp )
      energy=cienrg(1)
      ienergy=cietyp(1)
      return
      end
c deck rdhint
      subroutine rdhint(ntape, ntitle, title, infomo,
     & nmot, nsym, nmpsy, labels)
c
c  read the header info from the labeled integral file.
c
c  13-aug-01 labels(*) added to the argument list. -rls
c  written by ron shepard.
c  modified by gsk to read sifs header
c
       implicit none
      integer nfilmx
      parameter(nfilmx=31)
      integer iunits
      common/cfiles/iunits(nfilmx)
      equivalence (iunits(1),nlist)
c
      integer ietype, itty, ierr, nlist, i, idummy, nmap, nmot, ntitle,
     & nenrgy, nsym, ntape, ninfo
      integer mapdum(1)
      real*8 efrzc
      character*80 title(*)
      character*8 labels(*)
      integer infomo(*)
      real*8 sifsce
      external sifsce
c
      integer   ntitmx,    enrgmx,    nmomx, nsymx
      parameter(ntitmx=20, enrgmx=20, nmomx=1023, nsymx=8)
      real*8 cenergy(enrgmx)
      character*4 slabel(nsymx)
c     character*4 nmpsy(nmomx)
      integer nmpsy(nsymx)
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
c     # read the file header info.
c
      write(nlist,6100) 'moint file header information'
c
      call sifrh1(
     & ntape, ntitle, nsym, nmot,
     & ninfo, nenrgy, nmap, ierr )
c
      if (ierr .ne. 0) then
         call bummer('rdhint: from sifrh1, ierr=',ierr,faterr)
      elseif( ntitle .gt. ntitmx ) then
         call bummer('rdhint: from sifrh1,ntitmo=',ntitle,faterr)
      elseif( nmot .gt. nmomx) then
         call bummer('rdhint: from sifrh1, nmotmo=',nmot,faterr)
      elseif(nenrgy .gt. enrgmx ) then
         call bummer('rdhint: from sifrh1, nenrgymo=',nenrgy,faterr)
      endif
chl      write(nlist,*)'moint file header information'
c
c     # make space for the mo titles
c
      itty=0
      itty= min(itty,(ntitmx-ntitle))
*
      write(nlist,6150)'ntitle =', ntitle, 'nsym =', nsym, 'nmot =',nmot
      write(nlist,6150)'ninfo =', ninfo, 'nenrgy =', nenrgy
c
c     # ignore any mo map vectors.
c
      nmap = 0
c
      call sifrh2( ntape, ntitle, nsym, nmot,
     & ninfo, nenrgy, nmap, title(itty+1),
     & nmpsy, slabel, infomo, labels,
     & ietype, cenergy, mapdum,nmap,
     & ierr )
      if( ierr .ne. 0) then
         call bummer('rdhint: from sifrh2, ierr=',ierr,faterr)
      endif
c
chl      write(nlist,*)'moint file header information'
      do 9 i = 1, nsym
         write(nlist,6100) slabel(i), nmpsy(i)
9     continue
      write(nlist,6100) 'info(*)=', (infomo(i),i=1,ninfo)
      write(nlist,6500) 'labels(:)=', (i,labels(i),i=1,nmot)
c
      write(nlist,6100)'moint file header information'
      call wtitle(nlist,itty,ntitle,ntitle,ntitmx,title)
c
      write(nlist,6100)'moint core energy(*) values:'
      call sifpre(nlist,nenrgy,cenergy,ietype)
c
      write(nlist,6200)'total mo core energy =',
     & (sifsce(nenrgy,cenergy,ietype))
c
6100  format(1x,a,(10i5))
6150  format(3(1x,a10,i4))
c6020  format(/' integral file header information:'/(1x,a))
c     write(nlist,6030)nmot,nsym,(i,nmpsy(i),i=1,nsym)
6200  format(/,1x,a25,(1x,1p4e20.12))
6030  format(' total number of orbitals:',i4/
     & ' total number of irreps:',i2/
     & ' (isym:nmpsy)',8(i3,':',i3))
6500  format(1x,a/(8(i4,': ',a8)))
c
      return
      end
c
c**************************************************************
c
c deck rdhmcd
      subroutine rdhmcd(
     & nlist,    denfl,    infomd,    ntitle,
     & title,    nsym,     nipsy,     nmpsy,
     & elast,    mcietp,   mcenrg,    nimot,
     & nmot,     mapim,    mcres,     slabel,
     & mcd1lab,  mapar,    nenrgy)
c
c  read the header info from the mcscf density matrix file.
c
c  input:
c  nlist = listing file unit number.
c  denfl = density file unit number.
c
c  output:
c  ntitle = number of titles.
c  title(*) = descriptive titles.
c  nsym = number of symmetry blocks.
c  ninfo = number of record definition parameters.
c  infomd(1:ninfo) = record definition parameters.
c  nenrgy = number of energis. the first element must be the
c          repulsion energy.
c  mcenrg(1:nenrgy) = mcscf energies
c  mcietp(1:nenrgy) = mcscf energy types
c  nmap   = number of map vectors.
c  imtype = number of map vector types.
c  map(1:nmot,1:nmap) = basis function map vectors.
c
c  nipsy = number of mcscf-occupied orbitals of each symmetry.
c  nmpsy = number of basis functions of each symmetry.
c  elast = mcscf energy.
c  nimot = total number of mcscf-occupied orbitals.
c  nmot = total number of orbitals.
c  mapim(*) = internal-to-mo mapping vector.
c  mcres(*) = mcscf orbital resolution array.  this array determines
c             the types of orbital transformations that may be applied
c             to the density matrices that are consistent with the
c             original mc csf expansion set.
c
c  modified by galen f. gawboy to incorporate sifs i/o routines.
c  written by ron shepard.
c
       implicit none
c  ##  parameter setcion
c
      real*8    one,     zero
      parameter(one=1d0, zero=0d0)
c
c  bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
c  ##  integer section
c
      integer denfl
      integer i, ierr, imtype
      integer j
      integer knts, k, knte, knt
      integer mapknt
      integer nipknt, nsym, nimot, nmot, nlist, ntitle, nenrgy,
     &        ntitd1, ninfo, nmap
      real*8 mcenrg(*), elast
      integer ntitmx,mcietp(*),info
      parameter( ntitmx = 30 )
      integer   nmomx, nsymx
      parameter(nmomx=1023,nsymx=8)
      character*80 title(*)
      character*4 slabel(*)
      integer nipsy(*),nmpsy(*),mapim(*),mcres(*)
      integer infomd(*), nmpsyd(nsymx)
      integer mapar(*)
      character*8 mcd1lab(*)
ctm
c      character*80 dt(10)
c      integer  n1(8)
c      character*4  st(8)
c      integer n2(10)
c      character*8 mt(255)
c      real*8 mce(10)
c      integer mte(10)
c      integer imt(10)
c      integer map(255*10)
ctm
c
      rewind denfl
c
      call sifrh1(
     1 denfl, ntitd1, nsym, nmot,
     2 ninfo, nenrgy, nmap, ierr)
c     write(*,*) '1: nenrgy=',nenrgy
c
      if (ierr .ne. 0) then
         call bummer('rdhmcd: from sifrh1, ierr=',ierr,faterr)
      elseif (ntitd1 .gt. ntitmx) then
         call bummer('rdhmcd: from sifrh1, ntitd1=',ntitd1,faterr)
      elseif (ninfo .gt. 10) then
         call bummer('rdhmcd: from sifrh1, ninfo=',ninfo,faterr)
      elseif (ninfo .ne. 6) then
         call bummer('rdhmcd: from sifrh1, ninfo6=',ninfo,faterr)
      elseif ( nenrgy .ne. 6) then
         call bummer('rdhmcd: from sifrh1, nenrgy=',nenrgy,faterr)
      elseif (nmot .gt. nmomx) then
         call bummer('rdhmcd: from sifrh1, nmot=',nmot,faterr)
      endif
c
c      read(denfl,iostat=ierr)ntitle,(title(i),i=1,ntitle),nsym,
c     & (nipsy(i),i=1,nsym),(nmpsy(i),i=1,nsym),elast,nimot,nmot,
c     & (mapim(i),i=1,nmot),(mcres(i),i=1,nmot)
c      if(ierr.ne.0)call bummer('rdhmcd: ierr=',ierr,faterr)
c

ctm  the variable nenrgy is destroyed
ctm  after return from the call to sifrh2  (t3e only)
ctm  replacing mapar by a local variable prevents this,
ctm  however, after return to the calling function nenergy is
ctm  undefined again

      call sifrh2(
     1 denfl, ntitd1, nsym, nmot,
     2 ninfo, nenrgy, nmap, title,
     3 nmpsyd, slabel, infomd, mcd1lab,
     4 mcietp, mcenrg, imtype, mapar,
     5 ierr)
c
c extract nipsy(1:nsym), mcres(1:nmot), mapim(1:nmot), and compute
c nimot from mvres(1:nmot) and check for illegal orbital types.
c
c
       mapknt = 0
       nipknt = 0
       knt = 0
       nipsy(1) = 0
       do 104 k = 1,nsym
          knts = knt + 1
          knte = knt + nmpsyd(k)
          nipknt = 0
          do 103 j = knts,knte
             if( mapar(j) .gt. 0) then
                nipknt = nipknt + 1
                mcres(j) = mapar(j)
                mapknt = mapknt + 1
                mapim(j) = mapknt
             elseif(mapar(j) .eq. -5) then
                nipknt = nipknt + 1
                mcres(j) = 0
                mapknt = mapknt + 1
                mapim(j) =  mapknt
             elseif( mapar(j) .eq. -2) then
                nipknt = nipknt + 1
                mcres(j) = 0
                mapknt = mapknt + 1
                mapim(j) = mapknt
             elseif( mapar(j) .eq. -1) then
                nipknt = nipknt
                mcres(j) = 0
                mapim(j) = 0
             elseif( mapar(j) .eq. -3) then
               write(nlist,*)'frozen core orbitals are not allowed '
               call bummer('rdhmcd: illegal orbital type, mapar=',
     1           mapar(j),faterr)
             elseif(mapar(j) .eq. -4) then
               write(nlist,*)'frozen virtual orbitals are not allowed'
               call bummer('rdhmcd: illegal orbital type, mapar=',
     1           mapar(j),faterr)
             elseif(mapar(j) .eq. 0) then
               write(nlist,*)'artificially frozen active orbital '
               write(nlist,*)'j = ',j
               call bummer('rdhmcd: illegal orbital type, mapar=',
     &          mapar(j),faterr)
             endif
             knt = knt + 1
 103      continue
          nipsy(k) = nipknt
          nimot = nimot + nipknt
 104   continue
c
       elast = mcenrg(2)
c
c in the future it would be good to add a test ot check to make sure
c that the density matrix being read  in is that of a converged wave
c function.
c
       call sifpre(nlist, nenrgy, mcenrg, mcietp)
      write(nlist,6020)(title(i),i=1,ntitle)
6020  format(/' mcscf density file header information:'/(1x,a))
      write(nlist,6030)elast,nsym,(i,nipsy(i),i=1,nsym)
6030  format(' elast:  ',1pe25.15,' hartree.'/
     & ' total number of irreps:',i2/' (isym:nipsy)',8(i3,':',i3))
      write(nlist,6040)(i,nmpsyd(i),i=1,nsym)
6040  format(' (isym:nmpsy)',8(i3,':',i3))
      write(nlist,*)'nonzero mcres(*) orbital masks:'
      do 10 i=1,nmot
         if(mcres(i).ne.0)write(nlist,6050)i,mcres(i)
10    continue
6050  format(' orbital=',i4,' mask=',i10.8)
c
      write(nlist,'(1x,a)')'mo-to-internal mapping vector [i:mo(i)]:'
      write(nlist,6060)(i,mapim(i),i=1,nmot)
6060  format(10(i5,':',i3))
c
      return
      end
c deck rdlab2
      subroutine rdlab2(ntape,lenbuf,buf,nipbuf,val,nipv,ilab,num,last)
c
c  read a buffer of labeled 2-e integrals from ntape and
c  unpack the values into val(*) and the labels into ilab(*,*).
c
c  input:
c  ntape = input unit number.
c  nipv = number of integer labels per value.
c         0 : just decode record info (val(*) and ilab(*) are not
c             referenced).
c         1, 2, or 4 : partially or fully unpacked labels.
c
      implicit integer(a-z)
      integer ntape,lenbuf,nipbuf,nipv,ilab(*),num,last
      real*8 buf(lenbuf),val(nipbuf)
c
c  bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
c  read the buffer.
c
      read(ntape,iostat=istat)buf
      if(istat.ne.0)call bummer('rdlab2: istat=',istat,faterr)
c
c  decode the info.
c
      call decod2(lenbuf,buf,nipbuf,val,nipv,ilab,num,last)
c
      return
      end
c deck rdlv
      subroutine rdlv(iunit,n,na,a,alab,qfskip,nlist)
c
c  search unit iunit for label alab and read the corresponding vector.
c
c  iunit = unit number.
c  n     = expected vector length.
c  na    = actual vector length.  na=-1 for unsatisfied search.
c  a(*)  = if(na.le.n) the vector is returned.
c  alab  = character label used to locate the correct record.
c          first 8 characters only are used.
c  qfskip= .true. if first record should be skipped.
c
      implicit integer(a-z)
c
      logical qfskip
      real*8 a(n)
      character*(*) alab
c
      character*8 label,inlab
c
c  convert input label to upper case for subsequent comparisons.
c
      inlab=alab
      call allcap(inlab)
c
c  search for the correct label.
c
      na=0
      rewind iunit
      if(qfskip)read(iunit)
10    read(iunit,end=20)label,na
      call allcap(label)
      if(inlab.ne.label)then
         read(iunit)
         go to 10
      endif
c
c  ...correct label.  check length.
c
      if(na.ne.n)write(nlist,6010)alab,na,n
6010  format(/' rdlv: alab=',a,' na,n=',2i10)
      if(na.le.n)call seqr(iunit,na,a,istat)
      if(istat.ne.0)call bummer('rdlv: from seqr, istat=',istat,faterr)
      return
c
c  ...unsatisfied search for alab.
20    write(nlist,6020)alab
6020  format(/' rdlv: record not found. alab=',a)
      call bummer('rdlv: file error.',0,faterr)
      na=-1
      return
      end
c deck rdmcd2
      subroutine rdmcd2(sfile,info,itpbin,mapim,
     &                  nndx,offii,addii,bufin,
     &                  valin,ilabin,niiiit,d2)
c
c  read the mcscf two-particle density matrix elements.
c
c  input:
c  sfile = sequential input file positioned to read the first record.
c  mapim(*) = mo-to-internal mapping vector.
c  offii(*), addii(*) = density matrix addressing arrays.
c  bufin(*) = input buffer.
c  valin(*) = input value buffer.
c  ilabin(4,*) = unpacked label buffer.
c  niiiit = number of iiii density matrix elements.
c
c  output:
c  d2(*) = two-particle density matrix.
c
      implicit integer(a-z)
c
      integer   msame,     nmsame,     nomore,     iretbv
      parameter(msame = 0, nmsame = 1, nomore = 2, iretbv = 0)
c
      integer wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer info(*),itpbin,itypea, ibitv(1)
c
      parameter(nipv=4)
      integer mapim(*),nndx(*),ilabin(nipv,*),offii(*),addii(*)
      real*8 d2(niiiit),bufin(*),valin(*)
c
ctm itypea is return value !
c     parameter (itypea=3)
       data itypea /3/
c
c  initialize...
c
      call wzero(niiiit,d2,1)
c
c  read and process the buffers...
c
100   continue
c
      call sifrd2(
     1 sfile,   info,    nipv,   iretbv,
     2 bufin,   nbuf,    last,   itypea,
     3 itpbin,  ibvtyp, valin,
     4 ilabin,  ibitv,   ierr)
c
      if( ierr .ne. 0) then
        call bummer('rdmcd2: from sifrd2, ierr=',ierr,faterr)
      elseif( itypea .ne. 3) then
        call bummer('rdmcd2: from sifrd2, itypea=',itypea,faterr)
      elseif( ibvtyp  .ne. 0 ) then
        call bummer('rdmcd2: from sifrd2, ibvtyp=',ibvtyp,faterr)
      endif
c
c  mo labels are returned.
c
      do 200 iint=1,nbuf
c
         i=mapim(ilabin(1,iint))
         j=mapim(ilabin(2,iint))
         k=mapim(ilabin(3,iint))
         l=mapim(ilabin(4,iint))
c
         ii=max(i,j)
         jj=min(i,j)
         kk=max(k,l)
         ll=min(k,l)
c
         iijj=nndx(ii)+jj
         kkll=nndx(kk)+ll
c
         d2(offii(max(iijj,kkll))+addii(min(iijj,kkll)))=valin(iint)
200   continue
c
c  another buffer?...
      if(last.eq.msame) go to 100
c
      return
      end
c
c*************************************************************
c
c deck rdmch
      subroutine rdmch(
     & mchess,   nlist,   nhblk,   avcsl,
     & b,        ib,      ib1,     sizeb,
     & ib_m,     ib_c)
c
c  read the hessian blocks from the mcscf hessian file.
c
c  input:
c  mchess = hessian file unit number.
c  nhblk = number of hessian blocks.
c  avcsl = available workspace.
c  ib1 = initial pointer value.
c
c  output:
c  b(*) = hessian blocks.
c  ib(*) = hessian block pointers.
c  sizeb = total size of hessian blocks
c
       implicit none
c    #  maximal No. of different DRTs
      integer maxnst
      parameter (maxnst=8)
c
c    #  maximal No. of states in one DRT
      integer mxavst
      parameter (mxavst = 50)
c
c    #  maximal total number of states
      integer maxstat
      parameter (maxstat = maxnst*mxavst)
c
      integer mdir,cdir
      common/direct/mdir,cdir
c
c  bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer navst,nst,navst_max
      real*8 heig,wavst,norm
      common /avstat/ wavst(maxnst,mxavst), heig(maxnst,mxavst),
     & navst(maxnst),nst,navst_max
c
c  ##  integer section
c
      integer avcsl
      integer blhsz
      integer ib(15),i, isize, iblk, ierr, icount, iptold, ipt, ib1,
     &        ist_m, ist_c(4), ib_m(maxstat), ib_c(4,maxstat)
      integer j
      integer mchess
      integer nlist, nhblk
      integer sizeb
c
      real*8 b(avcsl)
c
      integer atebyt
      external atebyt
c
         do  i=1,15
         ib(i)=0
         enddo !i=1,15
c
         do i=1,maxstat
         ib_m(i) = 0
            do j=1,4
            ib_c(j,i) = 0
            enddo ! j=1,4
         enddo ! i=1,maxstat
         ist_m = 1
         do i=1,4
         ist_c(i) = 1
         enddo ! i=1,4
c
      rewind mchess
      ipt=ib1
100      continue
         iptold=ipt
c        write(nlist,*) 'Reading mc Hessian ...',mchess
         read(mchess,iostat=ierr,end=200)iblk,isize, blhsz
c
          if(ierr.ne.0) then
           write(*,*) 'iblk,isize,blhsz=',iblk,isize,blhsz
           call bummer('rdmch: ierr=',ierr,faterr)
          endif ! (ierr.ne.0)
c
         if (iblk.le.10) then
          ib(iblk) = ipt
         elseif(iblk.eq.15) then
          ib_m(ist_m) = ipt
          ist_m = ist_m + 1
          ib(iblk) = ipt
         else
          ib_c(iblk-10,ist_c(iblk-10)) = ipt
          ist_c(iblk-10) = ist_c(iblk-10) + 1
          ib(iblk) = ipt
         endif
c
          if(ipt+isize-1 .gt. avcsl)then
           write(nlist,6010)ib,iblk,isize,avcsl
           write(nlist,
     &      '("requested mem./available mem.: ",i10," / ",i10)')
     &      ipt+isize-1, avcsl
           call bummer('rdmch: not enough memory',avcsl,faterr)
          endif ! (ipt+isize-1 .gt. avcsl)
c
         call rdhb(b(ipt),isize,blhsz)
c
c  #   for the H matrix we will construct navst(ist) M matrix blocks
c  #   so we have to change the dimension
          if (iblk.eq.15) then
           ipt=ipt+atebyt(navst(ist_m-1)*isize)
          elseif (iblk.lt.15) then
           ipt=ipt+atebyt(isize)
          endif
c
         go to 100
c
c
c   ###   skip the actual hessian block due to dirrect construcion
c         of the matrix-vector product
c
250      ipt = iptold
         ib(iblk)=0
         go to 100
c
200   sizeb=ipt-ib1
c
      return
6010  format(/' rdmch: ib(*)='/1x,15i8/' iblk,isize,avcsl=',3i8)
      end
c
c********************************************************
c
      subroutine rdhb(hess,size,blhsz)
       implicit none
      integer nfilmx,iunits
      parameter(nfilmx=31)
      common/cfiles/iunits(nfilmx)
      integer nlist,mchess
      equivalence (iunits(1),nlist)
      equivalence (iunits(13),mchess)
c
      integer blhsz
c
      real*8 hess(*)
      integer i,num,size,icount
c
c  bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
c
       icount = 1
       do i=1,int(size/blhsz)
          call seqrbf(mchess,hess(icount),blhsz)
          icount = icount + blhsz
       enddo
c
      if (mod(size,blhsz).gt.0)
     & call seqrbf(mchess,hess(icount),mod(size,blhsz))
c
      return
      end
c
c**********************************************************************
c
c deck rddrt_mc
      subroutine rddrt_mc(
     & nsym,    namot,    ist,      ndmot,
     & nmot,    nrow,     nwalk,    ncsfm,
     & lendrt,  buf,      doub,     syml,
     & nj,      njskp,    a,        b,
     & l,       y,
     & xbar,
     & xp,       z,
     & limvec,   r,
     & nmpsy,    mctype,   mapml,   drtprt,
     & modrt)
c
c  read the mcscf drt arrays.  header info has already been read.
c  the drt file should be positioned correctly to begin reading drt
c  arrays and pointer vectors.
c
c  explicit variable declaration: 17-apr-02, m.dallos
c  02-feb-92 nmpsy test moved into rdhmcd1. -gfg
c  01-feb-92 vrsion=2 support and buf(*) to buffer doub(*) and symd(*)
c      added-
c  written by ron shepard.
c
       implicit none
c  ##  parameter & common block section
c
c  bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
c     #  maximal No. of different DRTs
      integer maxnst
      parameter (maxnst=8)
c
c     #  maximal No. of states in one DRT
      integer mxavst
      parameter (mxavst = 50)
c
      integer navst,nst,navst_max
      real*8 heig,wavst,norm
      common /avstat/ wavst(maxnst,mxavst), heig(maxnst,mxavst),
     & navst(maxnst),nst,navst_max
c
      integer nfilmx
      parameter(nfilmx=31)
      integer iunits
      common/cfiles/iunits(nfilmx)
      integer ndrt,nlist
      equivalence (iunits(1),nlist)
      equivalence (iunits(10),ndrt)
c
c  ##  integer section
c
      integer nrow,nwalk,namot,nsym,nmot
c
      integer a(nrow)
      integer buf(*),doub(*),b(nrow),b0
      integer ist,itemp,i,ierr
      integer l(0:3,nrow),limvec(nwalk),levprt(2),lendrt,lenbuf,li
      integer mctype(*),mapml(0:*)
      integer nj(0:namot),njskp(0:namot),nmpsy(nsym),nfudge(8),
     &        ndmotx,modrt(0:nmot),nlevel,ndmot,numi,namotx,ncsfm
      integer r(nwalk)
      integer syml(0:namot)
      integer xbar(nsym,nrow),xp(nsym,nrow)
      integer y(nsym,0:3,nrow)
      integer z(nsym,nrow)
c
c  ##  logical section
c
      logical drtprt
c
c  ##  character section
c
      character*60 fname
c
c  ##  data section
c
      integer nx(0:1,0:1),actx(0:5),doubx(0:5),ntypex(0:5)
      data actx/5,5,5,2,5,5/, doubx/0,5,5,1,5,5/
c
c-----------------------------------------------------------------
c
c  mcscf orbital types:
c    0=f_core, 1=inactive, 2=active, 3=virt, 4=f_virt, 5=error
c
      if (nst.eq.1) then
       fname = 'mcdrtfl'
      elseif(ist.ge.10) then
       write(fname,'(a8,i2)')'mcdrtfl.',ist
      else
       write(fname,'(a8,i1)')'mcdrtfl.',ist
      endif
      call trnfln( 1, fname )
      open(unit=ndrt,file=fname,status='old')
c
c    read over header sections
      read(ndrt,*, iostat=ierr)
       if ( ierr .ne. 0 ) then
        call bummer('rddrt: title ierr=',ierr,faterr)
       endif
c     # first header record.
      call rddbl( ndrt, lendrt, buf , -nsym )
c     # second header record.
      call rddbl( ndrt, lendrt, buf , -nsym )
c
c define the nfudge array.
c
      itemp = 0
      do 666 i=1,nsym
        nfudge(i) = itemp
        itemp = itemp + nmpsy(i)
666   continue
c
c  read in drt arrays.
c
      nlevel=namot+1
c
c skip the slabel array
c
      call rddbl( ndrt, lendrt, buf, -nsym)
c
      if (ndmot .gt. 0 ) then
c
c read the doub(*) array, use buf(b0 + 1 : b0 + ndmot) for scratch.
c
         b0 = 2 * ndmot + 1
         call rddbl( ndrt, lendrt, buf(b0+1), ndmot )
c
c read the symd(*) array. (this array gives the symmetry of
c the doubly occupied orbitals).
c
c use buf(b0 + ndmot + 1 : b0 + 2*ndmot + 1) for scratch
c
         call rddbl( ndrt, lendrt, buf(b0 + ndmot + 1), ndmot)
c
c now move into buf(1:2*ndmot + 1) and create the doub(*) array
c which used to be in the mcdrtfl in the first place. there is
c probably an excellent reason for having to do this ......
c
         do 667 i=1,ndmot
            buf( 2*(i-1) + 1 ) = buf( b0 + ndmot + i )
            buf( 2*(i-1) + 2 ) = buf( b0 + i )
667      continue
c
         buf(b0) = 0
         call symcvt(
     &    nsym, 2, buf, nmpsy, nfudge, ndmotx, 1, doub, ierr )
         if ( ierr .ne. 0 ) then
            call bummer('rdmdrt: from doub symcvt, ierr=',ierr, faterr)
         elseif (ndmot .ne. ndmotx) then
            call bummer('rdmdrt: from symcvt, ndmotx=',ndmotx, faterr)
         endif
      endif
c
c now we get to recreate the old modrt array.
c read the new modrt array, using what will be the a array for
c scratch space.
c
      call rddbl( ndrt, lendrt, a, namot)
c
c read the symmetries of the active orbitals. syml is actually of use.
c
      syml(0) = 0
      call rddbl( ndrt, lendrt, syml(1), namot )
c
c move into buf(*) and convert into modrt(*)
c
      do 668 i=1,namot
        buf(2*(i-1) + 1 ) = syml(i)
        buf(2*(i-1) + 2 ) = a(i)
668   continue
      buf( 2*namot + 1) = 0
      modrt(0) = 0
      call symcvt(
     &  nsym, 2, buf, nmpsy, nfudge, namotx, 1, modrt(1), ierr)
      if ( ierr .ne. 0 ) then
         call bummer('rdhdrt: from modrt symcvt, ierr=',ierr,faterr)
      elseif( namot .ne. namotx) then
         call bummer('rdhdrt: from symcvt, namotx=',namotx, faterr)
      endif
c  nmpsy(*)...
c  check for consistency with info from integral file.
c      call rddbl(ndrt,lendrt,buf,nsym)
c      do 10 j=1,nsym
c         if(buf(j).ne.nmpsy(j))then
c            write(nlist,6010)'input files',(nmpsy(i),i=1,nsym)
c            write(nlist,6010)'drt file',(buf(i),i=1,nsym)
c            call bummer('rdmdrt: inconsistent drt',0,faterr)
c         endif
c10    continue
c6010  format(1x,a,':',t20,' nmpsy(*)=',8i4)
c
c  doub(*)...
c      call rddbl(ndrt,lendrt,doub,ndmot)
      do 20 i=1,ndmot
         mctype(doub(i))=doubx(mctype(doub(i)))
20    continue
c
c  mapml(*)...
c      mapml(0)=0
c      call rddbl(ndrt,lendrt,mapml(1),namot)
      do 30 i=1,namot
         mapml(i) = modrt(i)
         mctype(mapml(i))=actx(mctype(mapml(i)))
30    continue
c
cc  syml(*)...
c      syml(0)=0
c      call rddbl(ndrt,lendrt,syml(1),namot)
c
c  nj(*)...
c
      call rddbl(ndrt,lendrt,nj,nlevel)
c
c  njskp(*)...
      call rddbl(ndrt,lendrt,njskp,nlevel)
c
c  a(*)...
      call rddbl(ndrt,lendrt,a,nrow)
c
c  b(*)...
      call rddbl(ndrt,lendrt,b,nrow)
c
c  l(*)...
      call rddbl(ndrt,lendrt,l,4*nrow)
c
c  y(*,*,*)...
      numi=nsym*4*nrow
      call rddbl(ndrt,lendrt,y,numi)
c
c  xbar(*,*)...
      numi=nsym*nrow
      call rddbl(ndrt,lendrt,xbar,numi)
c
c  xp(*,*)...
      numi=nsym*nrow
      call rddbl(ndrt,lendrt,xp,numi)
c
c  z(*,*)...
      numi=nsym*nrow
      call rddbl(ndrt,lendrt,z,numi)
c
c  limvec(*)...
      call rddbl(ndrt,lendrt,limvec,nwalk)
c
c  r(*)...
      call rddbl(ndrt,lendrt,r,nwalk)
c
      write(nlist,6070)'mapml(*)',(mapml(i),i=1,namot)
      if(ndmot.ne.0)write(nlist,6070)'doub(*)',(doub(i),i=1,ndmot)
6070  format(/1x,a,(t10,10i4,10x,10i4))
c
      levprt(1)=0
      levprt(2)=namot
      if(.not.drtprt)call prtdrt(levprt,namot,nrow,nsym,
     & nj,njskp,mapml,syml,a,b,l,xbar,y,xp,z)
c
      if(.not.drtprt)write(nlist,6080)r
6080  format(/' r(*)',5(t8,10i5,10x,10i5/))
c
c  change limvec(*) into skip/map form.
c  on input: 0=deleted walk, 1=retained walk.
c  on output: i>0 retained csf number i, i<0 then skip -i walks to
c             the next retained walk.  e.g.
c  input:  1  0  1  1  0  0  1  1  1  0  0  0  1 ...
c  output: 1 -1  2  3 -2 -1  4  5  6 -3 -2 -1  7 ...
c
      nx(0,0)=0
      nx(1,0)=1
      nx(0,1)=ncsfm+1
      nx(1,1)=0
      do 2400 i=nwalk,1,-1
         li=limvec(i)
         nx(0,0)=nx(li,0)-1
         nx(0,1)=nx(0,1)-li
         limvec(i)=nx(0,li)
2400  continue
c
      if(nx(0,1).ne.1)then
         write(nlist,*)'rddrt: limvec(*) error'
         stop
      endif
c
      if(.not.drtprt)write(nlist,6090)limvec
6090  format(/' ind(*)',5(t8,10i5,10x,10i5/))
c
c  check the mcscf orbital type array for consistency...
c
      do 3300 i=0,5
         ntypex(i)=0
3300  continue
      do 3400 i=1,nmot
         ntypex(mctype(i))=ntypex(mctype(i))+1
3400  continue
c
      if( (ntypex(5).ne.0) .or.
     & (ntypex(0)+ntypex(1).ne.ndmot) .or.
     & (ntypex(2).ne.namot) )then
         write(nlist,*)'ndmot = ',ndmot
         write(*,*)'namot = ',namot
         write(nlist,*)'ntypex(*)=',ntypex
         call bummer('rdmdrt: incorrect ntypex(*)',0,faterr)
      endif
c
      close(unit=ndrt)
c
      return
      end
c
c**************************************************************
c
      subroutine rdrsth
c
c  read in the restart file header
c
       implicit none
c   ##   parameter & common section
c
      real*8 zero
      parameter (zero=0.0d+00)
c
c  bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer nxy,mult,nsym,nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nbpsy(8),nmpsy(8)
      equivalence (nxy(1,1),nbpsy(1))
      equivalence (nxy(1,5),nmpsy(1))
      integer nmot,nbmt
      equivalence (nxtot(4),nmot)
      equivalence (nxtot(16),nbmt)
c
      integer nfilmx
      parameter(nfilmx=31)
c
      integer iunits, nlist, mcrest
      common/cfiles/iunits(nfilmx)
      equivalence (iunits(1),nlist)
      equivalence (iunits(12),mcrest)
c
c    #  maximal No. of different DRTs
      integer maxnst
      parameter (maxnst=8)
c
c    #  maximal No. of states in one DRT
      integer mxavst
      parameter (mxavst = 50)
c
      integer navst,nst,navst_max
      real*8 heig,wavst,norm
      common /avstat/ wavst(maxnst,mxavst), heig(maxnst,mxavst),
     & navst(maxnst),nst,navst_max
c
      integer nrow_f,ncsf_f,nwalk_f,ssym
      common/drtf/ nrow_f(maxnst),ncsf_f(maxnst),
     & nwalk_f(maxnst),ssym(maxnst)
c
      logical qconvg
      character*80 title(10)
c
c  ##  integer section
c
      integer i, ist, ist2
      integer j
      integer nmotx, ntol, ntitle, ninf, nmpsyx(8),
     &        navstx(maxnst)
      integer maxnavst
c
      real*8 wavstx(maxnst,mxavst)
c
c
      do ist=1,maxnst
      navstx(ist)=0
         do i=1,mxavst
         wavstx(ist,i) = zero
         enddo ! i=1,mxavst
      enddo ! ist=1,maxnst
c
c
c  read the header info:
c
      rewind mcrest
cgk   read(mcrest)nsym,ninf,ntol,nmot,nbmt,ncsf,qconvg,ntitle,
cgk  +  nmpsy,nbpsy,(title(it),it=1,ntitle),navst,wavst,heig
chl      read(mcrest)nsym,ninf,ntol,nmot,nbmt,ncsf_f,qconvg,ntitle,
chl     +  nmpsy,nbpsy
chl read new restart file structure
      read(mcrest)
     &   nsym,ninf,ntol,nmotx,nbmt,nst,
     &  (ncsf_f(i),i=1,nst),qconvg,ntitle,
     &  nmpsyx,nbpsy,(title(i),i=1,ntitle),
     &  (navstx(i),i=1,nst),maxnavst,
     &  ((wavstx(i,j),i=1,nst),j=1,maxnavst),
     &  ((heig(i,j),i=1,nst),j=1,maxnavst)
c
c  ##  set default values for navstx and wavstx
c
c
c  ##  performe consistency check
c
      if (nmot.ne.nmotx) then
       write(nlist,
     &  '(//'' !!! MO integrals and RESTART file do not match !!!'')')
       write(nlist,*)'nmot, nmotx = ',nmot, nmotx
       write(0,
     &  '(//'' !!! MO integrals and RESTART file do not match !!!'')')
       write(0,*)'nmot, nmotx = ',nmot, nmotx
       call bummer ('input error',0,faterr)
      endif
c
      do i=1,8
       if (nmpsy(i).ne.nmpsyx(i)) then
        write(nlist,
     &   '(//''!!!  MO integrals and RESTART file do not match !!!'')')
        write(nlist,*)'nbpsy =  ',nbpsy
        write(0,
     &   '(//''!!!  MO integrals and RESTART file do not match !!!'')')
        write(0,*)'nbpsy =  ',nbpsy
        call bummer ('input error',0,faterr)
       endif
      enddo ! i=1,8
c
      do ist=1,maxnst
         do i=1,mxavst
          if ((navst(ist).ne.navstx(ist)).or.
     &     (wavst(ist,i).ne.wavstx(ist,i))) then
           write(nlist,
     &      '(//'' !!! MCSCFIN and RESTART files do not match !!!'')')
           write(nlist,'(/2x,''Informations from RESTART file:'')')
           write(0,
     &      '(//'' !!! MCSCFIN and RESTART files do not match !!!'')')
           write(0,'(/2x,''Informations from RESTART file:'')')
              write(nlist,'(3x,''navst'',1x,''wavst'')')
              write(0,'(3x,''navst'',1x,''wavst'')')
              do ist2=1,maxnst
              write(nlist,'(3x,i2,2x,15(1x,f6.4))')navstx(ist2),
     &         (wavstx(ist2,j),j=1,mxavst)
              write(0,'(3x,i2,2x,15(1x,f6.4))')navstx(ist2),
     &         (wavstx(ist2,j),j=1,mxavst)
              enddo ! ist2=1,maxnst
           write(nlist,'(/2x,''Informations from MCSCFIN file:'')')
             write(nlist,'(3x,''navst'',1x,''wavst'')')
           write(0,'(/2x,''Informations from MCSCFIN file:'')')
             write(0,'(3x,''navst'',1x,''wavst'')')
              do ist2=1,maxnst
              write(nlist,'(3x,i2,2x,15(1x,f6.4))')navst(ist2),
     &         (wavst(ist2,j),j=1,mxavst)
              write(0,'(3x,i2,2x,15(1x,f6.4))')navst(ist2),
     &         (wavst(ist2,j),j=1,mxavst)
              enddo ! ist2=1,maxnst

           call bummer ('input error',0,faterr)
          endif
         enddo ! i=1,mxavst
      enddo ! ist=1,maxnst
c
cmd
cgk   write(nlist,1000)
cgk   write(nlist,2000)(title(it),it=1,ntitle)
cgk   write(nlist,3000)navst
cgk   write(nlist,4000)(ist,wavst(ist),ist=1,navst)
cgk   write(nlist,5000)(ist,heig(ist),ist=1,navst)
1000  format(/' mcscf restart tape header information:')
2000  format(/,(1x,a))
3000  format(/,' number of averaged states: ',i3)
4000  format(/,' corresponding weights: ',
     +       /,10( i2,'-te state ',' wavst(*): ',f8.5,', ')  )
5000  format(/,' corresponding energies: ',
     +       /,10( i2,'-te state ',' heig(*):  ',f12.6),', '  )
c
      return
c
      end
c deck rdsh1
      subroutine rdsh1(
     & ntape, lbuf, nipbuf, infomo,
     & buf,   ilab,  val,  s1,
     & h1,    nndx,  nnmt, ns,
     & sym,   nns,   map,nsym,
     & nmpsy, kntin)
c
c  read the 1-e s1(*) and h1(*) integrals.
c
c  input:
c  ntape = input unit number.
c  lbuf = buffer length.
c  nipbuf = maximum number of integrals in a buffer.
c  infomo = info array
c  buf(*) = buffer for integrals and packed labels.
c  ilab(*)= unpacked labels.
c  val(*) = unpacked values.
c  sym(*) = symmetry of each orbital
c  ns(*) = symmetry offset for orbitals
c  nns(*) = symmetry offset for lower triangular matrices
c  nnmt = total size of symmetric, symmetry-packed, lower-triangular-
c         packed matrix.
c  map(*) = orbital index mapping vector.
c
c  output:
c  s1(*) = overlap integrals.
c  h1(*) = 1-e hamiltonian matrix = t1(*) + v1(*).
c
c  written by ron shepard.
c  sifs version by kedziora
c
      implicit integer(a-z)
c
      integer   nipv,   msame,  nmsame,     nomore
      parameter(nipv=2, msame=0,nmsame = 1, nomore = 2 )
      real*8 buf(lbuf),h1(*),s1(*),val(nipbuf),fcore
      integer ilab(nipv,nipbuf),nndx(*),sym(*),ns(*),nns(*),map(*)
      integer nmpsy(*)
      integer infomo(*),kntin(*)
      integer   iretbv,    idummy(1),lasta,lastb
      parameter(iretbv = 0)
      real*8    zero
      parameter(zero = 0.0d0)
      integer    itypea,  itypbt,  itypbv, itypbs
      parameter( itypea=0,itypbs=0,itypbt=1,itypbv=2)
c
      rewind ntape
c
c  s1(*) integrals:
c
      call wzero(nnmt,s1,1)
      call izero_wr(36,kntin,1)
c
c note that the first order cidensity file now only contains the first
c order density matrix. thus if lastb is equal to 7 the program will
c jump to the end of the routine. a little ugly but it works.
c
      fcore=(0)
      call sifr1x(
     1 ntape, infomo, itypea, itypbs,
     2 nsym,  nmpsy,  idummy, buf,
     3 val,   ilab,   map,    sym,
     4 nnmt,  s1,     fcore,  kntin,
     5 lasta,lastb,   last,   nrec,
     6 ierr)
      if ( ierr.ne. 0) then
        call bummer('rdsh1: from sifr1x, ierr=',ierr,faterr)
      elseif ( lasta .ne. 0) then
        call bummer('rdsh1: from sifrd1x, lasta=',lasta,faterr)
      elseif ( lastb .ne. itypbs) then
        call bummer('rdsh1: from sifrd1s1, lastb=',lastb,faterr)
      elseif (fcore .ne. zero) then
c-        write(6,*)' frozen core orbitals are not yet allowed'
c-        write(6,*)'fcore =',fcore
        call bummer('rdsh1: from sifr1x, fcore=',fcore,faterr)
      endif
ctm ???
cmd   if( itypbin .eq. 7) go to 007
c
c t1(*) integrals:
c
      call wzero(nnmt,h1,1)
      call sifr1x(
     1 ntape, infomo, itypea, itypbt,
     2 nsym,  nmpsy,  idummy, buf,
     3 val,   ilab,   map,    sym,
     4 nnmt,  h1,     fcore,  kntin,
     5 lasta,lastb,   last,   nrec,
     6 ierr)
      if ( ierr.ne. 0) then
        call bummer('rdsh1: from sifr1x, ierr=',ierr,faterr)
      elseif ( lasta .ne. 0) then
        call bummer('rdsh1: from sifrd1x, lasta=',lasta,faterr)
      elseif ( lastb .ne. 1) then
        call bummer('rdsh1: from sifrd1s1, lastb=',lastb,faterr)
      elseif (fcore .ne. zero) then
c-        write(6,*)' frozen core orbitals are not yet allowed'
c-        write(6,*)'fcore = ',fcore
        call bummer('rdsh1: from sifr1x, fcore=',fcore,faterr)
      endif
c
c v1(*) integrals:
c
      call sifr1x(
     1 ntape, infomo, itypea, itypbv,
     2 nsym,  nmpsy,  idummy, buf,
     3 val,   ilab,   map,    sym,
     4 nnmt,  h1,     fcore,  kntin,
     5 lasta,lastb,   last,   nrec,
     6 ierr)
      if ( ierr.ne. 0) then
        call bummer('rdsh1: from sifr1x, ierr=',ierr,faterr)
      elseif ( lasta .ne. 0) then
        call bummer('rdsh1: from sifrd1x, lasta=',lasta,faterr)
      elseif ( lastb .ne. 2) then
        call bummer('rdsh1: from sifrd1s1, lastb=',lastb,faterr)
      elseif (fcore .ne. zero) then
c-        write(6,*)' frozen core orbitals are not yet allowed'
c-        write(6,*)'fcore = ',fcore
        call bummer('rdsh1: from sifr1x, fcore=',fcore,faterr)
      endif
007   continue
      return
      end
      subroutine rdstv1(ntape,info,lbuf,nipbuf,buf,
     & ilab,values,d1,f1,
     & q1,nndx,nnmt,ns,
     & sym,nns,map,ifmt)
c
c  read the 1-e s1(*), t1(*), and v1(*) integrals.
c
c  input:
c  ntape = input unit number.
c  lbuf = buffer length.
c  nipbuf = maximum number of integrals in a buffer.
c  buf(*) = buffer for integrals and packed labels.
c  ilab(*)= unpacked labels.
c  values(*) = unpacked values.
c  sym(*) = symmetry of each orbital.
c  ns(*) = symmetry offset for orbitals.
c  nns(*) = symmetry offset for lower triangular matrices
c  nnmt = total size of symmetric, symmetry-packed, lower-triangular-
c         packed matrix.
c  map(*) = orbital index mapping vector.
c
c  output:
c  d1(*) = first order density matrix.
c  f1(*) = fmc matrix.
c  q1(*) = qmc matrix.
c
c  written by ron shepard.
c

c
      implicit integer(a-z)
c
      integer   nipv
      parameter(nipv=2)
c
c last parameters.
c
      integer   msame,     nmsame,     nomore
      parameter(msame = 0, nmsame = 1, nomore = 2)
c
      real*8 buf(*),q1(*),f1(*),d1(*),values(*),fcore
      integer ilab(nipv,*),nndx(*),sym(*),ns(*),nns(*),map(*)
      integer info(*)
c
c some fixed record writing parameters.
c
c
      integer   ibvtyp,itypea,itypebd1,itypebf,itypebq
c
c  s1(*) integrals:
c
      fcore = 0d0
c
      call wzero(nnmt,d1,1)
100   continue
      call sifrd1(
     1 ntape,    info,   nipv,     iretbv,
     2 buf,      num,    last,     itypea,
     3 itypebd1, ibvtyp,   values,
     4 ilab,     fcore,  ibitv,    ierr)
c
c
      do 200 i=1,num
         p=map(ilab(1,i))
         q=map(ilab(2,i))
         psym=sym(p)
         p=p-ns(psym)
         q=q-ns(psym)
         pq=nndx(max(p,q))+min(p,q)+nns(psym)
         d1(pq)=values(i)
200   continue
      if(last.eq.msame)go to 100
c
c  t1(*) integrals:
c
      call wzero(nnmt,f1,1)
1100  continue
      call sifrd1(
     1 ntape,    info,   nipv,     iretbv,
     2 buf,      num,    last,     itypea,
     3 itypebf,  ibvtyp,   values,
     4 ilab,     fcore,  idummy,    ierr)
c
c
      do 1200 i=1,num
         p=map(ilab(1,i))
         q=map(ilab(2,i))
         psym=sym(p)
         p=p-ns(psym)
         q=q-ns(psym)
         pq=nndx(max(p,q))+min(p,q)+nns(psym)
         f1(pq)=values(i)
1200  continue
      if(last.eq.msame)go to 1100
c
c  v1(*) integrals:
c
      call wzero(nnmt,q1,1)
2100  continue
      call sifrd1(
     1 ntape,    info,   nipv,     iretbv,
     2 buf,      num,    last,     itypea,
     3 itypebq,  ibvtyp,   values,
     4 ilab,     fcore,  ibitv,    ierr)
c
c
      do 2200 i=1,num
         p=map(ilab(1,i))
         q=map(ilab(2,i))
         psym=sym(p)
         p=p-ns(psym)
         q=q-ns(psym)
         pq=nndx(max(p,q))+min(p,q)+nns(psym)
         q1(pq)=values(i)
2200  continue
      if(last.eq.msame)go to 2100
c
      return
      end
c
c**********************************************************************
c
      subroutine readin_gr
c
c  read the program input and, optionally, the file names.
c
       implicit none
c  ##  parameter & common section
c
      integer nfilmx
      parameter(nfilmx=31)
      character*60 fname
      common/cfname/fname(nfilmx)
      integer iunits
      integer nlist,nin
      common/cfiles/iunits(nfilmx)
      equivalence (iunits(1),nlist)
      equivalence (iunits(2),nin)
c
      integer nmomx,nsymx
      parameter(nmomx=1023,nsymx=8)
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer nxy,mult,nsym,nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
      integer nmpsy(8),nmot
      equivalence (nxy(1,5),nmpsy(1))
      equivalence (nxtot(4),nmot)
c
      integer mctype,mapivm,mapmi,
     & mapma,mapmd,mapmv,mapml,mapmm,
     & symi,symv,symm,syma,symd,mcres
      common/cmomap/mctype(nmomx),mapivm(nmomx),mapmi(nmomx),
     & mapma(nmomx),mapmd(nmomx),mapmv(nmomx),mapml(0:nmomx),
     & mapmm(nmomx),
     & symi(nmomx),symv(nmomx),symm(nmomx),
     & syma(nmomx),symd(nmomx),mcres(nmomx)
c
c      include 'option.inc/list'
      integer numopt
      parameter(numopt=10)
      integer cigopt,print,fresdd,fresaa,fresvv,samcflag,solvelpck
      common/option/cigopt(numopt)
      equivalence (cigopt(1),print)
      equivalence (cigopt(2),fresdd)
      equivalence (cigopt(3),fresaa)
      equivalence (cigopt(4),fresvv)
      equivalence (cigopt(5),samcflag)
      equivalence (cigopt(6),solvelpck)
cfp: samcflag added (specifies whether the input comes from a SA-MCSCF
c     computation)
c
      integer nipsy,nsi,nnsi,n2si,iri1,iri2,nimot,nnit,n2it,
     & nsmi,nmit,nsvi,nvit,nsad,nsvd,nsva,symtmm,symtii,symtvi,
     & intoff_ci
      common/csymi/
     & nipsy(8),nsi(8),nnsi(8),n2si(8),iri1(8),iri2(8),nimot,nnit,
     & n2it,nsmi(8),nmit,nsvi(8),nvit,nsad(8),nsvd(8),nsva(8),
     & symtmm(8),symtii(8),symtvi(8),intoff_ci(8,8)
c
      integer lenm1e,n1embf,lenm2e,n2embf
      common/ctmofi/lenm1e,n1embf,lenm2e,n2embf
      integer lenbft
      common/cmfti/lenbft
c
      integer mdir,cdir
      common/direct/mdir,cdir
c
      integer nmiter,nvrsmx
      real*8 rtol,dtol
      common/cleqi/rtol,dtol,nmiter,nvrsmx
c
      integer ldamin,ldamax,ldainc
      common/cldain/ldamin,ldamax,ldainc
c
c      include 'cresi.inc/list'
      real*8  wndtol, wnatol, wnvtol, ftol
      logical qfresd, qfresa, qfresv, qresaf
      common/cresi/ wndtol, wnatol, wnvtol, ftol, qfresd, qfresa,
     & qfresv, qresaf
c
      real*8 energy1,energy2
      integer nadcalc,drt1,drt2,root1,root2
      common /nad/ energy1,energy2,nadcalc,drt1,drt2,root1,root2
c
      integer prtmicro
      common/output/prtmicro

      integer assume_fc,nmpsy_fc(nsymx),mapmo_fc(nmomx),nexo_fc(nsymx)
      common /fcversion/ assume_fc,nmpsy_fc,mapmo_fc,nexo_fc

c    mctype should not be in namelist
      namelist/input/cigopt,ldamin,ldamax,ldainc,
     & lenm1e,n1embf,lenm2e,n2embf,lenbft,
     & nmpsy,nmot,nsym,
     & mctype,mcres,wndtol,wnatol,wnvtol,ftol,
     & print,fresdd,fresaa,fresvv,
     & rtol,dtol,nmiter,nvrsmx,mdir,cdir,prtmicro,
     & nadcalc,root1,root2,drt1,drt2,assume_fc,samcflag,solvelpck
c
c  ##  integer section
c
      integer i
c
c  ##  character section
c
      character*80 line
c
c------------------------------------------------------------------
c
*@ifdef crayctss
*      call ddiopon('ibm')
*@endif
      call izero_wr(numopt,cigopt,1)
c
c   ##  prtmicro - print out the M*s,C*s,C*r vectors
      prtmicro=0
c
c   ##  mdir - direct M*s calculation
      mdir = 1
c
c   ##  cdir - direct C*s and C*r calculation
      cdir = 1
c
c   ##  nadcalc - read in the symmetric 1- and 2-el. transition density
c   ##            instead of the oridinal 1- and 2-electron CI density
      nadcalc = 0
c   #   old behaviour
      assume_fc = 0 
c
c  print = cigopt(1)
c            =  0  normal print.
c           .ge.1  print 1-e arrays.
c           .ge.2  print indexing arrays.
c  fresdd = cigopt(2)
c            =  0  do not include dd resolutions.
c           .ne.0  include dd resolutions.
c  fresaa = cigopt(3)
c            =  0  do not include aa resolutions.
c           .ne.0  include aa resolutions.
c  fresvv = cigopt(4)
c            =  0  do not include vv resolutions.
c           .ne.0  include vv resolutions.
c
      solvelpck=0
      samcflag=0
c
      nmot=0
      nsym=0
      call izero_wr(8,nmpsy,1)
      root1=0
      root2=0
      drt1=0
      drt2=0

c
c  initialize all of the mcscf orbitals to be of type virtual...
c
      call iset(nmomx,3,mctype,1)
c
c  orbital resolution tolerances...
c
      wndtol=1.d-6
      wnatol=1.d-5
      wnvtol=1.d-6
c
c  final effective densitymatrix corresponds to a
c  full variationaly determined wavefunktion up to the
c  order of magnitude:
c
      ftol=1.d-5
c
c  linear equation stuff...
c
      rtol=1.d-6
      dtol=1.d-6
      nmiter=50
      nvrsmx=50
c
c  echo the user input...
c
      write(nlist,'(/1x,72(''=''),t5,''echo of the user input'')')
10    read(nin,'(a)',end=20)line
      write(nlist,'(1x,a)')line
      go to 10
20    write(nlist,'(1x,72(''=''))')
      rewind nin
c
      read(nin,input,end=100)
c
c  read the new filenames if any...
c
*@ifdef sun
*!C     compiler bug: after namelist input, normal read is impossible
*!C
*@else
30    continue
         read (nin,*,end=100)i,line
         if(i.ge.3 .and. i.lt.nfilmx)fname(i)=line
      go to 30
*@endif
c
100   continue
c
c      if (assume_fc.ne.0 .and. nadcalc.gt.0) then
c      call bummer
c    .  ('frozen core with non-adiabatic coupling not supported',0,2)
c      endif

      write(nlist,6030)cigopt
6030  format(/' input parameters:'/' cigopt(*)=',(1x,10i3))
      write(nlist,6032)ldamin,ldamax,ldainc,
     & lenm1e,n1embf,lenm2e,n2embf
6032  format(
     & ' ldamin=',i8,' ldamax=',i8,' ldainc=',i8/
     & ' lenm1e=',i8,' n1embf=',i8,' lenm2e=',i8,' n2embf=',i8)
c
      do i=1,nfilmx-1
      if (iunits(i).ne.0)
     & write(nlist,6040)i,iunits(i),fname(i)
      enddo
6040  format(/' internal fortran  initial'
     &       /'   unit    unit    filename'
     &       /'  ------  ------   --------'
     & /(1x,i6,i8,4x,a))
c
      write(nlist,'(/''  Calculation mode:'')')
      if (nadcalc.eq.0)then
       write(nlist,'(''  MR-CISD/MR_AQCC calculation.''/)')
      elseif (nadcalc.eq.1)then
       write(nlist,
     & '('' DCI(J,I,alpha) non-adiab. coupling term calculation.''/)')
      elseif (nadcalc.eq.2)then
       write(nlist,
     & '('' DCSF(J,I,alpha) non-adiab. coupling term calculation.''/)')
      elseif (nadcalc.eq.3)then
       write(nlist,
     & '('' DCI(J,I,alpha)+DCSF(J,I,alpha) non-adiab. coupl. term.''/)')
      else
       call bummer ('Wrong nadcalc value, nadcalc=',nadcalc,faterr)
      endif
c
      if (nadcalc.gt.1) then
        if (root1.eq.0)
     .   call bummer (' invalid value for root1 ',root1,faterr)
        if (root2.eq.0)
     .   call bummer (' invalid value for root2 ',root2,faterr)
        if (drt1.eq.0)
     .   call bummer (' invalid value for drt1 ',drt1,faterr)
        if (drt2.eq.0)
     .   call bummer (' invalid value for drt2 ',drt2,faterr)
      endif
c
      if ((nadcalc.gt.1) .and. ((root1+root2.lt.2) .or.
     & (drt1+drt1.lt.2))) then
       write(nlist,*)' root1',root1
       write(nlist,*)' root2',root2
       write(nlist,*)' drt1',drt1
       write(nlist,*)' drt2',drt2
       call bummer(
     &  ' for nadcalc>0 specify valid root1,root2,drt1,drt2!!!',
     &  0,faterr)
      endif
c
      return
      end
c
c**********************************************************************
c
      subroutine readin_mc
c
c  read the MCSCF user input.
c
c  based on the readin subroutine from mcscf4.f
c  modiffied by: M.Dallos, University Vienna,Austria
c
c  Note: * If you make changes to the readin subroutine in mcscf4.f,
c           make sure that this subroutine is also changed !
c        * all the namelist variables (exept navst, wavst) are kept
c           as local !
c
c
       implicit none
c  ##  common & parameter section
c
c
      integer       mdir_gr, cdir_gr
      common/direct/mdir_gr, cdir_gr
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer nfilmx, iunits
      parameter(nfilmx=31)
      common/cfiles/iunits(nfilmx)
      integer nlist,mcscfin
      equivalence (iunits(1),nlist)
      equivalence (iunits(19),mcscfin)
c
      integer nmotx
      parameter (nmotx=1023)
c
c    #  maximal No. of different DRTs
      integer maxnst
      parameter (maxnst=8)
c
c    #  maximal No. of states in one DRT
      integer mxavst
      parameter (mxavst = 50)
c
c    #  maximal total number of states
      integer maxstat
      parameter (maxstat = maxnst*mxavst)
c
      integer navst,nst,navst_max
      real*8 heig,wavst,norm
      common /avstat/ wavst(maxnst,mxavst), heig(maxnst,mxavst),
     & navst(maxnst),nst,navst_max
c
c  ##  integer section
c
      integer asprojection
      integer cdir
      integer dintsz
      integer fciorb(3,nmotx),fcore(nmotx),fvrt(nmotx)
      integer ist,i
      integer j
      integer lvlprt
      integer maxnavst, mxvadd
      integer npath(nmotx), niter, nstate(8), ncol(8), ncoupl,
     & noldv(8), noldhv(8), nunitv, nciitr, nvcimx, nmiter,
     & nvrsmx,nbpsy(8), nmpsy(8)
c
c  ##  logical section
c
      integer nflag
      parameter (nflag=50)
      logical flags(nflag)
c
c  ##  real*8 section
c
      real*8 tol(12),rtolci(10)
      real*8 wnorm
c
c
*@ifndef nonamelist
      namelist/input/flags,npath,niter,tol,nstate,fciorb,ncol,
     + ncoupl,noldv,noldhv,nunitv,nciitr,mxvadd,nvcimx,rtolci,wnorm,
     + nmiter,nvrsmx,fcore,fvrt,nmpsy,navst,wavst,dintsz,cdir,lvlprt,
     & asprojection
*@endif
c

      integer ierr
      character*14 chstate
c
c**********************************************************************
c
      ierr = 0
c
      write(nlist,6010)
6010  format(/' MCSCF user input information:'/)
c
c  initialize navst, wavst
      do i=1,maxnst
         navst(i) = 0
         do j=1,mxavst
            wavst(i,j) = 0.0d+00
         enddo
      enddo
      navst(1)=1
      wavst(1,1)=1.0d+00
ctm
      call izero_wr(nmotx,npath,1)
ctm
c
      do  i=1,nflag
         flags(i)=.false.
      enddo                     ! i=1,nflag
c
c**********************************************************************
c
c
c  read and echo the user input...
c
      write(nlist,'(1x,a)' ) '======== echo of the mcscf input ========'
      call echoin( mcscfin, nlist, ierr )
c
      if ( ierr .ne. 0 ) then
         call bummer('readin: from echoin, ierr=',ierr,faterr)
      endif
c
      rewind mcscfin
c
c  read namelist input...
c
*@ifdef nonamelist
*C    Cray-T3D unterstuetzt keinen Namelist-Input
*C    listenorientierte Eingabe nach folgendem Muster
*C     , , , , , / NPATH
*C     , , ,     / NSTATE, NITER
*C     , ,  ,    / TOL
*C     , ,       / NMITER, NVRSMX
*C     , ,       / NCOL, NCOUPL
*C     , ,       / NMPSY
*C     , ,       / FCIORB : 1. Paar
*C     , ,       / FCIORB : 2. Paar etc.   bis EOF
*        read(nin,*) (npath(i),i=1,nflag)
*        read(nin,*) nstate,niter
*        read(nin,*) (tol(i),i=1,10)
*        read(nin,*) nmiter, nvrsmx
*        read(nin,*) ncol, ncoupl
*        read(nin,*) (nmpsy(i),i=1,nsym)
*        i=1
*  198   read(nin,*,end=200) fciorb(1,i),fciorb(2,i),fciorb(3,i)
*        i=i+1
*        goto 198
*@elif defined nml_required
*      read( mcscfin, nml=input, end=200 )
*@else
      read( mcscfin, input, end=200 )
*@endif
200   continue
c**********************************************************************
c
c  process and print out the input data just read.
c
      do 230 i=1,nmotx
         if(npath(i))210,231,220
210      flags(-npath(i))=.false.
         go to 230
220      flags(npath(i))=.true.
230   continue
231   continue
c
c     state averaging
c
      nst = 0
      maxnavst = 0
      do i=1,8
         if (navst(i).ne.0) nst = nst + 1
         if (maxnavst.lt.navst(i)) maxnavst = navst(i)
      enddo
      if (maxnavst.gt.mxavst) then
         call bummer('readin: navst exceedes mxavst =,',mxavst,faterr)
      endif
c     normalize the weights
      norm = 0.0d+00
      do ist=1,nst
         do i= 1,navst(ist)
            norm = norm + wavst(ist,i)
         enddo
      enddo
      do ist=1,nst
         do i= 1,navst(ist)
            wavst(ist,i) = wavst(ist,i)/norm
         enddo
      enddo
c
      write(nlist,6060)
6060  format(/' ***  State averaging information: ***'/)
c
      if (nst.eq.1) then
         write(nlist,6053)nst
      else
         write(nlist,6054)nst
      endif
6053  format(' MCSCF calculation performed for ',i2,1x,'DRT.'/)
6054  format(' MCSCF calculation performed for ',i2,1x,'DRTs.'/)

      write(nlist,
     & '('' DRT  first state   no.of aver. states'',3x,''weights'')')
      do ist=1,nst
         write(chstate(1:),'(i1)')nstate(ist)
         write(chstate(2:),'(a13)') '.excit. state'
         if (nstate(ist).eq.0) chstate = 'ground state'
         write(nlist,6057)ist,chstate,navst(ist),
     &    (wavst(ist,i),i=1, navst(ist))
      enddo
6057  format(i3,3x,a14,6x,i3,13x,8(f5.3,1x))
c
c     ##  validity check for user input
c
      if (mdir_gr.eq.1) then
         if (flags(11).or.(.not.flags(12))) then
            write(nlist,6070)
            call bummer ('mdir=1 && flags(11) || ! flags(12)',0,faterr)
         endif                  !  (flags(11).or.(.not.falgs(12))
      endif                     ! (mdir_gr.eq.1)
6070  format(/' !!! incompatible mcscfin/cigrdin input options !!!'/
     & 5x,'Solution:'/
     & 8x,'set in mcscfin file: flags(11)=0,flags(12)=1'/
     & 8x,'or set in cigrdin: mdir=0')
c
      return
      end
c
c**********************************************************************
c
c deck rmhdrt
      subroutine rdhdrt_mc(
     & title,   nrow,    ist,   ssym,
     & smult,   nelt,    nela,  nwalk,
     & ncsfm,   lenbuf,  nmomx, namot,
     & ndmot,   nsym_ci, ibuf,  lencor)
c
c  read the header information from the mcscf drt file.
c
c  written by ron shepard.
c  modified by galen gawboy 2-1-92.
c  extension for state averaging by: michal dallos, X.99
c
       implicit none
c  bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
c     #  maximal No. of different DRTs
      integer maxnst
      parameter (maxnst=8)
c
c     #  maximal No. of states in one DRT
      integer mxavst
      parameter (mxavst = 50)
c
      integer navst,nst,navst_max
      real*8 heig,wavst,norm
      common /avstat/ wavst(maxnst,mxavst), heig(maxnst,mxavst),
     & navst(maxnst),nst,navst_max
c
      integer nfilmx
      parameter(nfilmx=31)
      integer iunits
      common/cfiles/iunits(nfilmx)
      integer ndrt,nlist
      equivalence (iunits(1),nlist)
      equivalence (iunits(10),ndrt)

      integer numopt
      parameter(numopt=10)
      integer cigopt,print
      common/option/cigopt(numopt)
      equivalence (cigopt(1),print)
c
c  ##  integer section
c
      integer ist, ierr, ibuf(*), i
      integer lenbuf,lencor
      integer ndmot, nsym_mc, nmomx, namot, nhdint, nrow,
     &        ncsfm, nwalk, nelt, nela, nsym_ci
      integer ssym, smult
      integer vrsion
c
c  ##  character section
c
      character*80 title
      character*80 fmt
      character*60 fname
      character*4 slabel(20)
c
c-----------------------------------------------------------------
c
      if (nst.eq.1) then
       fname = 'mcdrtfl'
      elseif(ist.ge.10) then
       write(fname,'(a8,i2)')'mcdrtfl.',ist
      else
       write(fname,'(a8,i1)')'mcdrtfl.',ist
      endif
      call trnfln( 1, fname )
      open(unit=ndrt,file=fname,status='old')
c
      read(ndrt,'(a)',iostat=ierr)title
      if(ierr.ne.0) then
         call bummer('rmhdrt: title ierr=',ierr,faterr)
      endif
      write(nlist,6010)title
6010  format(/' mcscf drt header information:'/1x,a)
c
c read the first header record
c
      call rddbl( ndrt, 3, ibuf, 3 )
      vrsion = ibuf(1)
      lenbuf = ibuf(2)
      nhdint = ibuf(3)
c
      if (lenbuf.gt.2*lencor) then
       write(nlist,
     &  '("requested mem./available mem.: ",i10," / ",i10)')
     &  lenbuf/2, lencor
       call bummer('rdhdrt_mc: 1 lencor=',lencor,faterr)
      endif
c
      if ( vrsion .ne. 2 ) then
       call bummer
     &  ('*** warning: rdhmdrt: drt version .ne. 2 *** vrsion=',
     &  vrsion,0)
      endif
c
      call rddbl( ndrt, lenbuf, ibuf, nhdint)
      ndmot = ibuf(1)
      namot = ibuf(2)
      nsym_mc  = ibuf(3)
      nrow  = ibuf(4)
      ssym  = ibuf(5)
      smult = ibuf(6)
      nelt  = ibuf(7)
      nela  = ibuf(8)
      nwalk = ibuf(9)
      ncsfm = ibuf(10)
c
      read(ndrt,7000)(slabel(i),i=1,nsym_mc)
7000  format(/,20a4)

      if (print.gt.0) write(nlist,6100)
     & ndmot,namot,nsym_mc,nrow,ssym,smult,nelt,nela,nwalk,ncsfm,lenbuf
6100  format(
     & ' ndmot= ',i6,4x,'namot= ',i6,4x,'nsym=  ',i6,4x,'nrow=  ',i6/
     & ' ssym=  ',i6,4x,'smult= ',i6,4x,'nelt=  ',i6,4x,'nela=  ',i6/
     & ' nwalk= ',i6,4x,'ncsfm= ',i6,4x,'lenbuf=',i6)
c
      write(nlist,'(1x,"Molecular symmetry group:",t30,a4)')
     & slabel(ssym)
      write(nlist,'(1x,"Total number of electrons:",t29,i4)')nelt
      write(nlist,'(1x,"Spin multiplicity:",t30,i3)')smult
      write(nlist,'(1x,"Number of doubly occ.orbitals:",t30,i3)')ndmot
      write(nlist,'(1x,"Number of active orbitals:",t30,i3)')namot
      write(nlist,'(1x,"Number of active electrons:",t30,i3)')nela
      write(nlist,'(1x,"Total number of CSFs:",t26,i7)')ncsfm
c
      if(namot.gt.nmomx)then
         write(nlist,6900)'namot,nmomx',namot,nmomx
         call bummer('rmhdrt: namot=',namot,faterr)
      endif
      if(ndmot.gt.nmomx)then
         write(nlist,6900)'ndmot,nmomx',ndmot,nmomx
         call bummer('rmhdrt: ndmot=',ndmot,faterr)
      endif
      if (nsym_mc.ne.nsym_ci) then
       write(nlist,'(//,''nsym(MCSCF)='',i3,3x,''nsym(CI)='',i3)')
     &  nsym_mc, nsym_ci
       call bummer ('Inconsistent CI/MCSCF DRT input',0,faterr)
      endif
c
6900  format(1x,a,2i8)
c
      close(unit=ndrt)
c
      return
      end
c
c---------------------------------------------------------------------
c
c deck scmprs
      subroutine scmprs(nmaxr,ndimr,ndims,noldr,nolds,ncfx,
     & mr,cr,fr,pr,s,mxs,cxs,scr)
c
c  compress the s(*,*) expansion vector space to a reduced dimension
c  and update the corresponding reduced arrays.
c
c  this version reduces the space all the way to a dimension of one
c  determined by the current reduced vector pr(*).
c
c  7-june-88  reduce to one expansion vector (rls).
c  written by ron shepard.
c
      implicit integer(a-z)
      real*8 mr(*),cr(nmaxr,*),fr(*),pr(*),s(ndims,*),
     & mxs(ndims,*),cxs(ndimr,*),scr(*)
c
      real*8 temp, ddot_wr
c
      real*8    zero,    one
      parameter(zero=0d0,one=1d0)
c
c  reduce s(*,*).
c
      call dscal_wr(ncfx,pr(1),s(1,1),1)
      do 10 i=2,nolds
         temp=pr(i)
         if(temp.ne.zero)call daxpy_wr(ncfx,temp,s(1,i),1,s(1,1),1)
10    continue
c
c  reduce mxs(*,*).
c
      call dscal_wr(ncfx,pr(1),mxs(1,1),1)
      do 20 i=2,nolds
         temp=pr(i)
         if(temp.ne.zero)call daxpy_wr(ncfx,temp,mxs(1,i),1,mxs(1,1),1)
20    continue
c
c  reduce cxs(*,*).
c
      call dscal_wr(ndimr,pr(1),cxs(1,1),1)
      do 30 i=2,nolds
         temp=pr(i)
         if(temp.ne.zero)call daxpy_wr(ndimr,temp,cxs(1,i),1,cxs(1,1),1)
30    continue
c
c  reduce cr(*,*).
c
      call mxma(cr,1,nmaxr, pr,1,1, scr,1,1, noldr,nolds,1)
      call dcopy_wr(noldr,scr,1,cr(1,1),1)
c
c  reduce fr(*).
c
      fr(1)=ddot_wr(nolds,fr,1,pr,1)
c
c  reduce mr(*).
c
      call wzero(nolds,scr,1)
      call smxvu(mr,pr,scr,nolds,scr,0)
      mr(1)=ddot_wr(nolds,scr,1,pr,1)
c
c  reduce pr(*).
c
      pr(1)=one
c
c  update the expansion vector dimension.
c
      nolds=1
c
      return
      end
c deck sdmat
      subroutine sdmat(symw,nsv,ism,yb,yk,xbar,xp,z,r,ind,
     & val,icode,hvect,tden,ncsf,qt,qind)
c
c  construct the symmetric one- and two-particle transition density
c  matrix element contributions for off-diagonal loops.
c
c  tden(n,ij)  = (1/2)*<mc| eij + eji |n>
c  tden(n,ijkl)= (1/4)*<mc| eijkl + ejikl + eijlk + ejilk |n>
c
c  *note* with this definition of the tden(*) matrix elements, the dot
c  product of tden(*) with hvect(*) gives a d*(*) matrix element of
c  the form <mc| e |mc>.
c
c  the loop values are in val(*), some of which have factors included
c  to facilitate their use in the hamiltonian matrix.
c
c  qind version: 31-may-85
c  symmetry ft version: 18-oct-84
c  programmed by ron shepard.
c
      implicit integer(a-z)
c
c
      integer nxy,mult,nsym,nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
c
      integer ism(nsv),yb(nsv),yk(nsv)
      integer xbar(*),xp(*),z(*)
      real*8 val(3),hvect(ncsf),tden(ncsf,3)
      integer r(*),ind(*)
      logical qt(3),qind,qtx(3)
      real*8 t(3),half,fourth
      parameter(half=5d-1, fourth=25d-2)
c
c  account for factors and assign the go to variable.
c
      iop=icode-1
      go to(2,3,4,5,6,7,8,9,10,11,12,13,14,15),iop
c
2     continue
c  iiil, illl, il
      igo = 7
      t(1)=half*val(1)
      t(2)=half*val(2)
      t(3)=half*val(3)
      qt(1)=.true.
      qt(2)=.true.
      qt(3)=.true.
      qtx(1)=.true.
      qtx(2)=.true.
      qtx(3)=.true.
      go to 50
c
3     continue
c  ijlk, iklj
      igo = 4
      t(1)=fourth*val(1)
      t(2)=fourth*val(2)
      qt(1)=.true.
      qt(2)=.true.
      qtx(1)=.true.
      qtx(2)=.true.
      qtx(3)=.false.
      go to 50
c
4     continue
c  ijkl, ilkj
      igo = 5
      t(1)=fourth*val(1)
      t(3)=fourth*val(2)
      qt(1)=.true.
      qt(3)=.true.
      qtx(1)=.true.
      qtx(2)=.false.
      qtx(3)=.true.
      go to 50
c
5     continue
c  ikjl, iljk
      igo = 6
      t(2)=fourth*val(1)
      t(3)=fourth*val(2)
      qt(2)=.true.
      qt(3)=.true.
      qtx(1)=.false.
      qtx(2)=.true.
      qtx(3)=.true.
      go to 50
c
6     continue
c  iikl, ilki
c  ijll, illj
      igo = 4
      t(1)=half*val(1)
      t(2)=fourth*val(2)
      qt(1)=.true.
      qt(2)=.true.
      qtx(1)=.true.
      qtx(2)=.true.
      qtx(3)=.false.
      go to 50
c
7     continue
c  ikkl, ilkk
      igo = 5
      t(1)=fourth*val(1)
      t(3)=half*val(2)
      qt(1)=.true.
      qt(3)=.true.
      qtx(1)=.true.
      qtx(2)=.false.
      qtx(3)=.true.
      go to 50
c
8     continue
c  iiil, il
      igo = 5
      t(1)=half*val(1)
      t(3)=half*val(2)
      qt(1)=.true.
      qt(3)=.true.
      qtx(1)=.true.
      qtx(2)=.false.
      qtx(3)=.true.
      go to 50
c
9     continue
c  illl, il
      igo = 6
      t(2)=half*val(1)
      t(3)=half*val(2)
      qt(2)=.true.
      qt(3)=.true.
      qtx(1)=.false.
      qtx(2)=.true.
      qtx(3)=.true.
      go to 50
c
10    continue
c  ijkl
c  ijlk
c  ikkl
c  iklk
      igo = 1
      t(1)=fourth*val(1)
      qt(1)=.true.
      qtx(1)=.true.
      qtx(2)=.false.
      qtx(3)=.false.
      go to 50
c
11    continue
c  iikl
c  ijll
      igo = 1
      t(1)=half*val(1)
      qt(1)=.true.
      qtx(1)=.true.
      qtx(2)=.false.
      qtx(3)=.false.
      go to 50
c
12    continue
c  iklj
c  ikjl
c  ilki
c  ikli
c  ilik
c  illj
c  iljl
      igo = 2
      t(2)=fourth*val(1)
      qt(2)=.true.
      qtx(1)=.false.
      qtx(2)=.true.
      qtx(3)=.false.
      go to 50
c
13    continue
c  illi
c  half*ilil
      igo = 2
      t(2)=half*val(1)
      qt(2)=.true.
      qtx(1)=.false.
      qtx(2)=.true.
      qtx(3)=.false.
      go to 50
c
14    continue
c  ilkj
c  iljk
      igo = 3
      t(3)=fourth*val(1)
      qt(3)=.true.
      qtx(1)=.false.
      qtx(2)=.false.
      qtx(3)=.true.
      go to 50
c
15    continue
c  ilkk
c  il
      igo = 3
      t(3)=half*val(1)
      qt(3)=.true.
      qtx(1)=.false.
      qtx(2)=.false.
      qtx(3)=.true.
c
50    continue
c
      do 1300 isv=1,nsv
         ytb=yb(isv)
         ytk=yk(isv)
         tsym=ism(isv)
         xt=xp(tsym)
         zt=z(tsym)
         hsym=mult(tsym,symw)
         xbh=xbar(hsym)
c
         if(qind)then
c
c  csf selection case.  ind(*) vector is required.
c
            do 1200 j=1,xt
               b=ytb+r(zt+j)
               k=ytk+r(zt+j)
               bmax=ytb+r(zt+j)+xbh
100            continue
               b=b+1
               k=k+1
110            continue
               if(b.gt.bmax)go to 1200
               ib=ind(b)
               ik=ind(k)
               db=min(ib,ik)
               if(db.gt.0)then
                  go to (1001,1002,1003,1012,1013,1023,1123),igo
               else
                  b=b-db
                  k=k-db
                  go to 110
               endif
c
1001           continue
               tden(ib,1)=tden(ib,1)+hvect(ik)*t(1)
               tden(ik,1)=tden(ik,1)+hvect(ib)*t(1)
               go to 100
1002           continue
               tden(ib,2)=tden(ib,2)+hvect(ik)*t(2)
               tden(ik,2)=tden(ik,2)+hvect(ib)*t(2)
               go to 100
1003           continue
               tden(ib,3)=tden(ib,3)+hvect(ik)*t(3)
               tden(ik,3)=tden(ik,3)+hvect(ib)*t(3)
               go to 100
1012           continue
               tden(ib,1)=tden(ib,1)+hvect(ik)*t(1)
               tden(ik,1)=tden(ik,1)+hvect(ib)*t(1)
               tden(ib,2)=tden(ib,2)+hvect(ik)*t(2)
               tden(ik,2)=tden(ik,2)+hvect(ib)*t(2)
               go to 100
1013           continue
               tden(ib,1)=tden(ib,1)+hvect(ik)*t(1)
               tden(ik,1)=tden(ik,1)+hvect(ib)*t(1)
               tden(ib,3)=tden(ib,3)+hvect(ik)*t(3)
               tden(ik,3)=tden(ik,3)+hvect(ib)*t(3)
               go to 100
1023           continue
               tden(ib,2)=tden(ib,2)+hvect(ik)*t(2)
               tden(ik,2)=tden(ik,2)+hvect(ib)*t(2)
               tden(ib,3)=tden(ib,3)+hvect(ik)*t(3)
               tden(ik,3)=tden(ik,3)+hvect(ib)*t(3)
               go to 100
1123           continue
               tden(ib,1)=tden(ib,1)+hvect(ik)*t(1)
               tden(ik,1)=tden(ik,1)+hvect(ib)*t(1)
               tden(ib,2)=tden(ib,2)+hvect(ik)*t(2)
               tden(ik,2)=tden(ik,2)+hvect(ib)*t(2)
               tden(ib,3)=tden(ib,3)+hvect(ik)*t(3)
               tden(ik,3)=tden(ik,3)+hvect(ib)*t(3)
               go to 100
1200        continue
         else
c
c  no csf selection.  ind(i)=i for all i.
c
            do 1240 ix=1,3
               if(qtx(ix))then
                  do 1230 j=1,xt
                     ib=ytb+r(zt+j)
                     ik=ytk+r(zt+j)
                     do 1210 i=1,xbh
                        tden(ik+i,ix)=tden(ik+i,ix)+hvect(ib+i)*t(ix)
1210                 continue
                     do 1220 i=1,xbh
                        tden(ib+i,ix)=tden(ib+i,ix)+hvect(ik+i)*t(ix)
1220                 continue
1230              continue
               endif
1240        continue
         endif
1300  continue
c
      return
      end
c deck sdmatd
      subroutine sdmatd(symw,nsv,ism,yb,xbar,xp,z,r,ind,
     & val,icode,hvect,tden,ncsf,qind)
c
c  construct the symmetric one- and two-particle transition density
c  matrix element contributions for diagonal loops.
c
c  tden(n,ii)  =<mc| eii   |n>
c  tden(n,iill)=<mc| eiill |n>
c  tden(n,ilil)=(1/2)* <mc| eilli |n>
c
c  *note* with this definition of the tden(*) matrix elements, the dot
c  product of tden(*) with hvect(*) gives a d*(*) matrix element of
c  the form <mc| e |mc>.
c
c  the loop values are in val(*), some of which have factors included
c  to facilitate their use in the hamiltonian matrix.
c
c  qind version: 31-may-85
c  symmetry ft version: 18-oct-84
c  programmed by ron shepard.
c
      implicit integer(a-z)
c
c
      integer nxy,mult,nsym,nxtot
      common/csymb/nxy(8,42),mult(8,8),nsym,nxtot(24)
c
      integer ism(nsv),yb(nsv)
      integer xbar(*),xp(*),z(*)
      real*8 val(3),hvect(ncsf),tden(ncsf,3)
      integer r(*),ind(*)
      logical qind,qtx(2)
      real*8 t(2),half,two
      parameter(half=5d-1, two=2d0)
c
c  account for factors and assign the go-to variable.
c
      iop=icode-1
      go to(2,3,4,5,6,7),iop
c
2     continue
c  iill, illi
      t(1)=val(1)
      t(2)=half*val(2)
      igo=3
      qtx(1)=.true.
      qtx(2)=.true.
      go to 50
3     continue
c  iill
      t(1)=val(1)
      igo=1
      qtx(1)=.true.
      qtx(2)=.false.
      go to 50
4     continue
c  illi
      t(2)=half*val(1)
      igo=2
      qtx(1)=.false.
      qtx(2)=.true.
      go to 50
c
5     continue
c  half*llll, ll
      t(1)=two*val(1)
      t(2)=val(2)
      igo=3
      qtx(1)=.true.
      qtx(2)=.true.
      go to 50
6     continue
c  half*llll
      t(1)=two*val(1)
      igo=1
      qtx(1)=.true.
      qtx(2)=.false.
      go to 50
7     continue
c  ll
      t(2)=val(1)
      igo=2
      qtx(1)=.false.
      qtx(2)=.true.
c
50    continue
c
      do 1300 isv=1,nsv
         ytb=yb(isv)
         tsym=ism(isv)
         xt=xp(tsym)
         zt=z(tsym)
         hsym=mult(tsym,symw)
         xbh=xbar(hsym)
c
         if(qind)then
c
c  csf selection case.  ind(*) is required.
c
            do 1200 j=1,xt
               b=ytb+r(zt+j)
               bmax=ytb+r(zt+j)+xbh
100            continue
               b=b+1
110            continue
               if(b.gt.bmax)go to 1200
               ib=ind(b)
               if(ib.gt.0)then
                  go to (1001,1002,1012),igo
               else
                  b=b-ib
                  go to 110
               endif
c
1001           continue
               tden(ib,1)=tden(ib,1)+hvect(ib)*t(1)
               go to 100
1002           continue
               tden(ib,2)=tden(ib,2)+hvect(ib)*t(2)
               go to 100
1012           continue
               tden(ib,1)=tden(ib,1)+hvect(ib)*t(1)
               tden(ib,2)=tden(ib,2)+hvect(ib)*t(2)
               go to 100
1200        continue
         else
c
c  no csf selection.  ind(i)=i for all i.
c
            do 1240 ix=1,2
               if(qtx(ix))then
                  do 1230 j=1,xt
                     ib=ytb+r(zt+j)
                     do 1210 i=1,xbh
                        tden(ib+i,ix)=tden(ib+i,ix)+hvect(ib+i)*t(ix)
1210                 continue
1230              continue
               endif
1240        continue
         endif
1300  continue
c
      return
      end
c deck seqr
      subroutine seqr(iunit,n,a,istat)
c
c  sequential read of a(*) from unit iunit.
c  on input, n must be > 0.
c
      integer iunit,n,istat
      real*8 a(n)
      read(iunit,iostat=istat)a
      return
      end
c deck sm11sq
      subroutine sm11sq(a,a2,n)
c
c  expand a symmetric packed matrix with diagonal elements stored
c  to square form.
c
      implicit integer(a-z)
      real*8 a(*),a2(n,*)
c
      a2(1,1)=a(1)
      ij1=2
      do 20 i=2,n
         im1=i-1
         call dcopy_wr(  i, a(ij1),1, a2(1,i),1)
         call dcopy_wr(im1, a(ij1),1, a2(i,1),n)
         ij1=ij1+i
20    continue
c
      return
      end
      subroutine  wtlabel(slabel,nsym,fmt,buf)
      implicit none
      integer nsym
      integer i
      character*4 slabel(nsym),buf(nsym)
       character*(*) fmt
      do 10 i = 1, nsym
         write( slabel(i), '(a4)' ) buf(i)
10    continue
      return
      end

