!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
c deck dalen
      subroutine dalen(nlist,lcore1,lcore2,lcore3,
     & numtot,nbkmx,    nbuk,lendar,nvpbk,nvpsg)
c
c  determine the direct access sorting parameters.
c
c  input:
c  lcore1 = amount of space available for buckets, label
c           buckets, and a single da buffer during first half sort.
c  lcore2 = amount of space available for a da buffer, a value buffer,
c           a label buffer, an h2(*) segment, and a d2(*) segment during
c           the fock matrix construction.
c  lcore3 = amount of space available for a da buffer, a value buffer,
c           a label buffer, and an h2(*) segment during the q(*) matrix
c           construction.
c  numtot = total number of integral/density values to be sorted.
c  nbkmx = maximum number of buckets allowed.
c
c  output:
c  nbuk = number of buckets to be allocated for the sort.
c  lendar = direct access record length.
c  nvpbk = number of integerals in each da record.
c  nvpsg = number of integrals in each segment.
c
c  written by ron shepard
c  version: 11-jan-89
c
      implicit integer(a-z)
c
      common/cldain/ldamin,ldamax,ldainc
c
c  bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
c  compute the number of integrals that can be held in a da buffer of
c  length=l given in working precision words.  the buffer must hold
c  integrals, packed_integer labels, and two packed_integer values for
c  record and chaining info.
c
*@ifdef future
*@else
      nvpbkf(l)=((l-1)*2)/3
*@endif
c
c  make sure the record length is between ldamin and ldamax.  use
c  ldainc as record length increment.
c
c  external functions:
c  atebyt(n) = number of words required to hold n integrals.
c  forbyt(n) = number of words required to hold n unpacked labels.
c
      do 10 len=ldamax,ldamin,-ldainc
         lendar=len
         nvpbk=min( nvpbkf(lendar), numtot )
         av2=lcore2-(atebyt(nvpbk)+forbyt(nvpbk)+atebyt(lendar))
         if(av2.le.0)goto 10
c
         av3=lcore3-(atebyt(nvpbk)+forbyt(nvpbk)+atebyt(lendar))
         if(av3.le.0)goto 10
c
         nvpsg=min( av2/2, av3, numtot )
         nbuk=(numtot-1)/nvpsg +1
         if(nbuk.gt.nbkmx)goto 10

         req1=atebyt(nvpbk*nbuk)+forbyt(nvpbk*nbuk)+atebyt(lendar)
         if(req1.le.lcore1)then
c
c           ...suitable sorting parameters have been found.
c
            write(nlist,6020)nbuk,lendar,nvpbk,nvpsg
            return
         endif
10    continue
c  ...exit from loop means not enough space is available.
c
      write(nlist,6010)lcore1,lcore2,lcore3,lendar,av2,av3,req1,
     & nbkmx,nbuk
6010  format(/' *** error *** larger workspace required for efficient',
     & ' integral sorting:'/
     & ' lcore1 =',i8/
     & ' lcore2 =',i8/
     & ' lcore3 =',i8/
     & ' ldamin =',i8/
     & ' av2    =',i8/
     & ' av3    =',i8/
     & ' req1   =',i8/
     & ' nbkmx  =',i8/
     & ' nbuk   =',i8)
c
      call bummer('dalen: space allocation error',0,faterr)
c
      return
6020  format(/' dalen: da sorting parameters, nbuk=',i4,' lendar=',i8,
     & ' nvpbk=',i12,' nvpsg=',i12)
      end
c deck tql2
*@ifdef cray
*!C  use library routine.
*@else
      subroutine tql2(nm,n,d,e,z,ierr)
c
      integer          i,           ierr,        ii,          j
      integer          k,           l,           l1,          m
      integer          mml,         n,           nm
      real*8 b,           c,           d(n),        d1mach
      real*8 abs,         sign,        sqrt,        e(n)
      real*8 f,           g,           h,           machep
      real*8 p,           r,           s,           z(nm,n)
c
c     this subroutine is a translation of the algol procedure tql2,
c     num. math. 11, 293-306(1968) by bowdler, martin, reinsch, and
c     wilkinson.
c     handbook for auto. comp., vol.ii-linear algebra, 227-240(1971).
c
c     30-aug-90 more universal change to avoid vax overflow. -rls
c     26-may-82 vax-specific change to avoid overflow.  this was
c               originally done approximately april 1980 while at
c               batelle columbus laboratory. -ron shepard
c
c     this subroutine finds the eigenvalues and eigenvectors
c     of a symmetric tridiagonal matrix by the ql method.
c     the eigenvectors of a full symmetric matrix can also
c     be found if  tred2  has been used to reduce this
c     full matrix to tridiagonal form.
c
c     on input
c
c        nm must be set to the row dimension of two-dimensional
c          array parameters as declared in the calling program
c          dimension statement;
c
c        n is the order of the matrix;
c
c        d contains the diagonal elements of the input matrix;
c
c        e contains the subdiagonal elements of the input matrix
c          in its last n-1 positions.  e(1) is arbitrary;
c
c        z contains the transformation matrix produced in the
c          reduction by  tred2, if performed.  if the eigenvectors
c          of the tridiagonal matrix are desired, z must contain
c          the identity matrix.
c
c      on output
c
c        d contains the eigenvalues in ascending order.  if an
c          error exit is made, the eigenvalues are correct but
c          unordered for indices 1,2,...,ierr-1;
c
c        e has been destroyed;
c
c        z contains orthonormal eigenvectors of the symmetric
c          tridiagonal (or full) matrix.  if an error exit is made,
c          z contains the eigenvectors associated with the stored
c          eigenvalues;
c
c        ierr is set to
c          zero       for normal return,
c          j          if the j-th eigenvalue has not been
c                     determined after 30 iterations.
c
c     questions and comments should be directed to b. s. garbow,
c     applied mathematics division, argonne national laboratory
c
c     -----------------------------------------------------------------
c
c                machep is a machine dependent parameter specifying
c                the relative precision of floating point arithmetic.
c                machep = 16.0d0**(-13) for long form arithmetic
c                on s360
c     data machep/z3410000000000000/
c
      data machep/1.d-14/
c
      ierr = 0
      if (n .eq. 1) go to 1001
c
      do 100 i = 2, n
  100 e(i-1) = e(i)
c
      f = 0.0d0
      b = 0.0d0
      e(n) = 0.0d0
c
      do 240 l = 1, n
         j = 0
         h = machep * (abs(d(l)) + abs(e(l)))
         if (b .lt. h) b = h
c                look for small sub-diagonal element
         do 110 m = l, n
            if (abs(e(m)) .le. b) go to 120
c                e(n) is always zero, so there is no exit
c                through the bottom of the loop
  110    continue
c
  120    if (m .eq. l) go to 220
  130    if (j .eq. 30) go to 1000
         j = j + 1
c                form shift
         l1 = l + 1
         g = d(l)
         p = (d(l1) - g) / (2.0d0 * e(l))
c
c  changed for limited vax exponent range. 5/26/82 (rls)
c  cdabs(*)  works correctly even for very large or small p
c-vax         r = sqrt(p*p+1.0d0)
c+vax         r=cdabs(dcmplx(p,1.d0))
c  changed again 30-aug-90 in a more universal way.  this code should
c  work correctly on any machine.  two sqrt()s are required instead of
c  one, but this statement is executed only o(n) times. -ron shepard
c
         if( p .eq. 0.0d0 )then
            r = 1.0d0
         else
            r = abs(p)
            r = sqrt(r) * ( sqrt(r + 1.0d0/r) )
         endif
c
         d(l) = e(l) / (p + sign(r,p))
         h = g - d(l)
c
         do 140 i = l1, n
  140    d(i) = d(i) - h
c
         f = f + h
c                ql transformation
         p = d(m)
         c = 1.0d0
         s = 0.0d0
         mml = m - l
c                for i=m-1 step -1 until l do --
         do 200 ii = 1, mml
            i = m - ii
            g = c * e(i)
            h = c * p
            if (abs(p) .lt. abs(e(i))) go to 150
            c = e(i) / p
            r = sqrt(c*c+1.0d0)
            e(i+1) = s * p * r
            s = c / r
            c = 1.0d0 / r
            go to 160
  150       c = p / e(i)
            r = sqrt(c*c+1.0d0)
            e(i+1) = s * e(i) * r
            s = 1.0d0 / r
            c = c * s
  160       p = c * d(i) - s * g
            d(i+1) = h + s * (c * g + s * d(i))
c                form vector
            do 180 k = 1, n
               h = z(k,i+1)
               z(k,i+1) = s * z(k,i) + c * h
               z(k,i) = c * z(k,i) - s * h
  180       continue
c
  200    continue
c
         e(l) = s * p
         d(l) = c * p
         if (abs(e(l)) .gt. b) go to 130
  220    d(l) = d(l) + f
  240 continue
c                order eigenvalues and eigenvectors
      do 300 ii = 2, n
         i = ii - 1
         k = i
         p = d(i)
c
         do 260 j = ii, n
            if (d(j) .ge. p) go to 260
            k = j
            p = d(j)
  260    continue
c
         if (k .eq. i) go to 300
         d(k) = d(i)
         d(i) = p
c
         do 280 j = 1, n
            p = z(j,i)
            z(j,i) = z(j,k)
            z(j,k) = p
  280    continue
c
  300 continue
c
      go to 1001
c                set error -- no convergence to an
c                eigenvalue after 30 iterations
 1000 ierr = l
 1001 return
c                last card of tql2
      end
*@endif
*@ifdef obsolete
*c deck dawrt
*c
*c  this routine and its entry points are for manipulating temporary
*c  direct access files.  the records are to be written consecutively
*c  and read randomly within each file.
*c  the proper sequence of calls is:
*c
*c  idafl=1     ! 2 for the second file, 3 for the third file, etc.
*c  call daini(idafl)
*c  call daopw(idafl,len)
*c  do
*c     call dawrt(idafl,buffer,last)
*c  enddo
*c  call daclw(idafl)
*c  call daopr(idafl)
*c  do
*c     call dafnd(idafl,irec)
*c     call dardr(idafl,buffer,irec)
*c  enddo
*c  call daclr(idafl)
*c
*c  07-aug-90 colib calls added. -rls
*c
*      subroutine dawrt(idafl,buffer,last)
*c
*c  write a da record.
*c
*c  input:
*c  idafl = internal da file number (1,2,3...)
*c  buffer(*) = working precision buffer to be written.
*c
*c  output:
*c  last = pointer to the written record.
*c
*      implicit integer(a-z)
*c
*      parameter(ndafmx=2)
*      common/cdafls/daunit(ndafmx)
*c
*      character*60 dafnam
*      common/cdanms/dafnam(ndafmx)
*c
*      integer dalen(ndafmx),danext(ndafmx),dastat(ndafmx)
*      save dalen,danext,dastat
*c
*      integer   closed,  openw,  open,  openr
*      parameter(closed=1,openw=2,open=3,openr=4)
*c
*      integer   iwrite,  iread
*      parameter(iwrite=1,iread=2)
*c
*c     # bummer error types.
*      integer   wrnerr,  nfterr,  faterr
*      parameter(wrnerr=0,nfterr=1,faterr=2)
*c
*      real*8 buffer(*)
*c
*c     # the calling program should not depend on last being incremented
*c     # by one.
*c
*      if(idafl.gt.0 .and. idafl.le.ndafmx)then
*         if(dastat(idafl).eq.openw)then
*            last=danext(idafl)
*            call writda( daunit(idafl), last, buffer, dalen(idafl) )
*            danext(idafl) = danext(idafl) + 1
*         else
*            call bummer('dawrt: stat=',dastat(idafl),faterr)
*         endif
*      else
*         call bummer('dawrt: idafl=',idafl,faterr)
*      endif
*      return
*c
*      entry daopw(idafl,len)
*c
*c     # open a temporary direct access file for writing.
*c     # idafl = internal da file number.
*c     # len = buffer length in working precision words.
*c     # daunit(*) = fortran unit numbers for internal da file numbers.
*c     # dafnam(*) = character file names.
*c
*      if(idafl.gt.0 .and. idafl.le.ndafmx)then
*         if(dastat(idafl).eq.closed)then
*            dastat(idafl)=openw
*            dalen(idafl)=len
*            danext(idafl)=1
*@ifdef oldcray
*            lrecl=8*len
*            open(unit=daunit(idafl),access='direct',form='unformatted',
*     &       recl=lrecl,status='scratch')
*@elif defined oldvax
*            lrecl=2*len
*            open(unit=daunit(idafl),access='direct',form='unformatted',
*     &       recl=lrecl,status='scratch')
*@elif defined oldibm
*            lrecl=8*len
*            open(unit=daunit(idafl),file=dafnam(idafl),
*     &      form='unformatted')
*            do 3456 ixx=1,10
* 3456       write (daunit(idafl)) dumm
*            close(daunit(idafl),status='keep')
*            open(unit=daunit(idafl),file=dafnam(idafl),access='direct',
*     &      form='unformatted',recl=lrecl)
*@else
*            call openda( daunit(idafl), dafnam(idafl), len,
*     &       'scratch', 'random' )
*@endif
*         else
*            call bummer('daopw: stat=',dastat(idafl),faterr)
*         endif
*      else
*         call bummer('daopw: idafl=',idafl,faterr)
*      endif
*      return
*c
*      entry daclw(idafl)
*c
*c     # close the unit for writing.
*c
*      if(idafl.gt.0 .and. idafl.le.ndafmx)then
*         if(dastat(idafl).eq.openw)then
*            dastat(idafl)=open
*         else
*            call bummer('daclw: stat=',dastat(idafl),faterr)
*         endif
*      else
*         call bummer('daclw: idafl=',idafl,faterr)
*      endif
*      return
*c
*      entry daopr(idafl)
*c
*c     # open the unit for reading.
*c
*      if(idafl.gt.0 .and. idafl.le.ndafmx)then
*         if(dastat(idafl).eq.open)then
*            dastat(idafl)=openr
*         else
*            call bummer('daopr: stat=',dastat(idafl),faterr)
*         endif
*      else
*         call bummer('daopr: idafl=',idafl,faterr)
*      endif
*      return
*c
*      entry dafnd(idafl,irec)
*c
*c     # find the designated record on the file.
*c
*      if(idafl.gt.0 .and. idafl.le.ndafmx)then
*         if(dastat(idafl).eq.openr)then
*@ifdef vax
*            find( unit=daunit(idafl), rec=irec )
*@endif
*            continue
*         else
*            call bummer('dafnd: stat=',dastat(idafl),faterr)
*         endif
*      else
*         call bummer('dafnd: idafl=',idafl,faterr)
*      endif
*      return
*c
*      entry dardr(idafl,buffer,irec)
*c
*c     # read the designated record from the file.
*c
*      if(idafl.gt.0 .and. idafl.le.ndafmx)then
*         if(dastat(idafl).eq.openr)then
*            call readda( daunit(idafl), irec, buffer, dalen(idafl) )
*         else
*            call bummer('dardr: stat=',dastat(idafl),faterr)
*         endif
*      else
*         call bummer('dardr: idafl=',idafl,faterr)
*      endif
*      return
*c
*      entry daclr(idafl)
*c
*c     # close the unit for reading so that it may be opened again with
*c     # a new record length.
*c
*      if(idafl.gt.0 .and. idafl.le.ndafmx)then
*         if(dastat(idafl).eq.openr)then
*            call closda( daunit(idafl), 'delete', iparm)
*            dastat(idafl)=closed
*         else
*            call bummer('daclr: stat=',dastat(idafl),faterr)
*         endif
*      else
*         call bummer('daclr: idafl=',idafl,faterr)
*      endif
*      return
*c
*      entry daini(idafl)
*c
*c     # initialization of the da*() routines for a particular file.
*c     # this entry must be called once before any other da*() calls
*c     # for each da file.  file unit numbers and filenames, if used,
*c     # are assigned elsewhere (e.g. block data).
*c
*      if(idafl.gt.0 .and. idafl.le.ndafmx)then
*         dalen(idafl)=-1
*         danext(idafl)=-1
*         dastat(idafl)=closed
*      else
*         call bummer('daini: idafl=',idafl,faterr)
*      endif
*      return
*c
*      end
*@endif
c deck rdden1
      subroutine rdden1(
     & ntape, lbuf, nipbuf, infomo,
     & buf,   ilab,  val,  s1,
     & d1,    nndx,  nnmt, ns,
     & sym,   nns,   map,nsym,
     & nmpsy, kntin, mapmo_fc,nmpsy_fc,
     & assume_fc,nadcalc)
c
c  read a 1-e density matrix
c
c  input:
c  ntape = input unit number.
c  lbuf = buffer length.
c  nipbuf = maximum number of integrals in a buffer.
c  infomo = info array
c  buf(*) = buffer for integrals and packed labels.
c  ilab(*)= unpacked labels.
c  val(*) = unpacked values.
c  sym(*) = symmetry of each orbital
c  ns(*) = symmetry offset for orbitals
c  nns(*) = symmetry offset for lower triangular matrices
c  nnmt = total size of symmetric, symmetry-packed, lower-triangular-
c         packed matrix.
c  map(*) = orbital index mapping vector.
c  mapmo_fc(*) = map frozen core to full density
c  s1(*)   = scratch 
c
c  output:
c  d1(*) = 1-e density matrix
c
c  written by ron shepard.
c  modified by gsk
c
      implicit integer(a-z)
c
      integer   nipv,   msame,  nmsame,     nomore,nmomx
      parameter(nipv=2, msame=0,nmsame = 1, nomore = 2,nmomx=1023 )
      real*8 buf(lbuf),d1(*),s1(*),val(nipbuf),fcore
      integer ilab(nipv,nipbuf),nndx(*),sym(*),ns(*),nns(*),map(*)
      integer mapmo_fc(*),nmpsy_fc(*)
      integer nmpsy(*)
      integer infomo(*),kntin(*)
      integer   iretbv,    idummy(1),lasta,lastb
      parameter(iretbv = 0)
      real*8    zero
      parameter(zero = 0.0d0)
      integer    itypea,  itypbd
      parameter( itypea=0,itypbd=7)
      real*8  tmprowcol(nmomx) 
      integer nndxof(8),ndxof(8),nadcalc
c
      fcore= zero
c
      
      nmot=ns(nsym)+nmpsy(nsym)
      call wzero(nnmt,s1,1)
      call wzero(nnmt,d1,1)
      write(6,'(20i4)') map(1:nmot)
c     call plblks('initial array', s1, nsym, nmpsy_fc,' MO', 3, 6) 
c     sym-array is initialized in add1a 
c     and is overwritten by sifr1x
c     use scratch array instead!
      call sifr1x(
     1 ntape, infomo, itypea, itypbd,
     2 nsym,  nmpsy_fc,  idummy, buf,
     3 val,   ilab,   map,    tmprowcol,
     4 nnmt,  s1,     fcore,  kntin,
     5 lasta,lastb,   last,   nrec,
     6 ierr)


       call plblks('d1 array', s1, nsym, nmpsy_fc,' MO', 3, 6) 

      if ( ierr.ne. 0) then
        call bummer('rdden1: from sifr1x, ierr=',ierr,faterr)
      elseif (fcore .ne. zero .and. assume_fc.eq.0 ) then
        call bummer('rdden1: from sifr1x(), fcore=',fcore,faterr)
      endif

      if (assume_fc.eq.1) then
c
c     remap the one-electron density elements
c
      nndxof(1)=0
      ndxof(1)=0
      do isym=2,nsym
         nndxof(isym)=nndxof(isym-1)+
     .          nmpsy_fc(isym-1)*(nmpsy_fc(isym-1)+1)/2
         ndxof(isym)=ndxof(isym-1)+nmpsy_fc(isym-1)
      enddo

      ij=0
      do isym=1,nsym
        do ibf=1,nmpsy_fc(isym)
         do jbf=1,ibf
          ij=ij+1
          imo=ndxof(isym)+ibf
          jmo=ndxof(isym)+jbf
          n_imo=mapmo_fc(imo)
          n_jmo=mapmo_fc(jmo)
          n_imo=n_imo-ns(isym)
          n_jmo=n_jmo-ns(isym)
           if (n_imo.lt.n_jmo) then
               tmp=n_imo
               n_imo=n_jmo
               n_jmo=tmp
           endif 
         ijnew=nns(isym)
     .           + n_imo*(n_imo-1)/2+n_jmo
         d1(ijnew)=s1(ij)
         enddo
        enddo
      enddo

c  add frozen core elements

       if (nadcalc.eq.0) then 
       do i=1,nmot
        if (mapmo_fc(i).lt.0) then
         imo=abs(mapmo_fc(i))
         isym=sym(imo)
         imo=imo-ns(isym)
         ijnew=nns(isym)+ imo*(imo+1)/2
         d1(ijnew)=2.0d0
        endif
       enddo
       endif 
      else
        call dcopy_wr(nnmt,s1,1,d1,1)
      endif
      return
      end
c deck wtd1f
      subroutine wtd1f(ntape,info,buf,
     & ilab,val,d1,fmc,
     & nsym,irx1,irx2,map,ifmt)
c
c  write the 1-e d1(*) and f(*) matrices
c
c  input:
c  ntape = output unit number.
c  buf(*) = buffer for integrals and packed labels.
c  ilab(*)= unpacked labels.
c  val(*) = unpacked values.
c  nsym,irx1(*),irx2(*) = symmetry range info.
c  map(*) = orbital index mapping vector.
c
c  written by ron shepard.
c   modified by gsk for sifs io
c
      implicit integer(a-z)
c
      integer info(*), ikntf, ikntq
      integer    ifmt,     ibvtyp
c again hardcoding 8bit packing 
      parameter( ibvtyp = 0)
      real*8 fcore
      integer   nipv
      parameter(nipv=2)
c
      integer ibitv, ierr, iknt, isym, itypea, itypebd1
      integer itypebf, itypebq, last, nrec
      integer nsym, ntape, numtot, p, pq, q
c
      integer   msame,   nmsame,  nomore
      parameter(msame=0, nmsame=1,nomore=2)
c
      real*8 buf(*),fmc(*),d1(*),val(*)
      integer ilab(nipv,*),irx1(nsym),irx2(nsym),map(*)
c
      real*8    small
      parameter(small=1e-10)
c
c specification of the various ietypes the ones used here
c are listed below
c
c itypea = 0   symmetric matrix
c itypebd1 = 7 one particle density matrix
c itypebf = 8  generalized fock matrix
c
      itypea  = 0
      itypebd1= 7
      itypebf = 8
      fcore   = 0.0
      nipbuf = info(3)
c
c  effective d1(*) integrals:
c
      last = msame
      numtot = 0
      pq=0
      iknt=0
      do 130 isym=1,nsym
         do 120 p=irx1(isym),irx2(isym)
            do 110 q=irx1(isym),p
               pq=pq+1
               if(abs(d1(pq)).gt.small)then
                  if(iknt.eq.nipbuf)then
                     last = msame
                     numtot = numtot + iknt
c hardcoded 8bit packing
                     call sifew1(
     &                 ntape, info,  nipv,    iknt,
     &                 last,  itypea,itypebd1,
     &                 ibvtyp,val,   ilab,    fcore,
     &                 ibitv, buf,   nrec,    ierr)
                     numtot = numtot - iknt
                  endif
                  iknt=iknt+1
                  val(iknt)=d1(pq)
                  ilab(1,iknt)=map(p)
                  ilab(2,iknt)=map(q)
               endif
110         continue
120      continue
130   continue
c
c dump the rest of the d1 matrix elements
c
      last = nmsame
      numtot = iknt + numtot
c again hardcoding 8bit packing 
      call sifew1(
     & ntape, info,  nipv,    iknt,
     & last,  itypea,itypebd1,
     & ibvtyp,val,   ilab,    fcore,
     & ibitv, buf,   nrec,    ierr)
c
c
c  generalized fock matrix elements
c
      last = msame
      pq=0
      ikntf=0
      do 230 isym=1,nsym
         do 220 p=irx1(isym),irx2(isym)
            do 210 q=irx1(isym),p
               pq=pq+1
               if(abs(fmc(pq)).gt.small)then
                  if(ikntf.eq.nipbuf)then
                     numtot = numtot + ikntf
                     last = msame
c again hardcoding 8bit packing 
                     call sifew1(
     &                 ntape, info,  nipv,    ikntf,
     &                 last,  itypea,itypebf, 
     &                 ibvtyp,val,   ilab,    fcore,
     &                 ibitv, buf,   nrec,    ierr)
                       numtot = numtot - ikntf
                  endif
                  ikntf=ikntf+1
                  val(ikntf)=fmc(pq)
                  ilab(1,ikntf)=map(p)
                  ilab(2,ikntf)=map(q)
               endif
210         continue
220      continue
230   continue
      numtot = numtot + ikntf
      last = nomore
c again hardcoding 8bit packing 
      call sifew1(
     & ntape, info,  nipv,    ikntf,
     & last,  itypea,itypebf ,
     & ibvtyp,val,   ilab,    fcore,
     & ibitv, buf,   nrec,    ierr)
c
      return
      end
c deck wtitle
      subroutine wtitle( nlist, ittl0, blksiz, ntitle, ntitmx, title )
c
c  write some titles and update the title(*) buffer parameters.
c
c  input:
c  nlist  = output unit number. if nlist<=0 then don't print.
c  ittl0  = offset for the current title block.
c  blksiz = size of the current block.
c  ntitle = old high-water mark.
c  ntitmx = buffer size.
c  title(1:ntitmx) = title buffer.
c                   title( ittl0+1 : ittl0+blksiz ) will be printed.
c
c  output:
c  ittl0 = updated offset, subject to mod( ittl0, ntitmx ).
c  ntitle = updated high-water mark with 0<=ntitle<=ntitmx.
c
c  08-oct-90 (columbus day) written by ron shepard.
c
      implicit logical(a-z)
c
      integer nlist, ittl0, blksiz, ntitle, ntitmx
      character*80 title(ntitmx)
c
      integer i1, i2, i
c
      i1 = ittl0 + 1
      i2 = min( ittl0 + blksiz, ntitmx )
c
      if ( nlist .gt. 0 ) then
c-         write(nlist,6100) i1, i2
         write(nlist,6200) ( title(i), i = i1, i2 )
      endif
c
      ntitle = min(  max( ntitle, i2 ), ntitmx )
      ittl0  = mod( i2, ntitmx )
c
      return
6100  format(' wtitle: title(',i2,':',i2,')=')
6200  format(1x,a)
      end
