#! /usr/bin/perl
#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/


use Shell qw(pwd cd date mv cat);

$cbus=$ENV{"COLUMBUS"};

print ("\nInstalling reaction path package version 2.3\n\n");



$dir=pwd;
chomp($dir);
$wdir=$dir;

print ("Give the full path where the MEPPACK should be installed:\n");
print ("Default: $wdir/bin\n");
$dir=<STDIN>;
if ($dir eq "\n") {$dir="$wdir/bin"}

if (-d "$dir") {
  print ("$dir already exists:\n");
  print ("Overwrite (y/n) ?\n");
  $usrin=<STDIN>;
  if ($usrin eq "y\n") {
    system("rm -r $dir/*");
  } else {
    die "\n\nERROR: Directory $dir already exists !\n\n";
  }
} else {
  mkdir ("$dir");
}

system("cp -pu $wdir/source/* $dir");
chdir("$dir");
system("make");
chdir("$dir");



print ("\nInstallation finished\n\n");
