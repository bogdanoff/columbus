!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
program mwdisp ! written by Bernhard Sellner
! (c) Bernhard Sellner 2011 
! part of MEPPACK
! This program is part of the reaction path package.
! It performs mass weighted displacement along all 
! internal coordinates to be used for calculating 
! the hessian


use prec_mod
use units_mod
use general_mod
use def_mod
use nml_mod

implicit none
real(kind=dpr) Gn12(:,:),bmatrix(:,:),geom(:,:),M(:)
real(kind=dpr) Z(:),intgeom(:),Gp12(:,:),ig(:,:,:)
integer i,j,k,l,ipiv(:),lwork,it,du1,du2,du3
integer filestat
character*1 n,t,S(:)
character*5 version
allocatable Gn12,ipiv,bmatrix,geom,M,S,Z,intgeom,Gp12,ig

! Unit ..................................... input ..................... working
! bmatrix ... bmatrix ...................... [A|rad/A] ................. [A|rad/A]
! M ......... mass ......................... [amu] ..................... [amu]
! intgeom ... internal geometry ............ [A|rad] ................... [A|rad]
! Gp12 ...... =G^(+1/2) .................... [(A|rad/(sqrt(amu)*A)] .... [(A|rad/(sqrt(amu)*A)]
! Gn12 ...... =G^(-1/2) .................... [(sqrt(amu)*A)/(A|rad)] ... [(sqrt(amu)*A)/(A|rad)]
!
! ig ........ displaced int. geometries .... [A|rad] 

version='2.3.0'
filestat=0

open(unit=1,file='mwdisp.log',form='formatted',iostat=filestat)
write(1,fmt='(A)',iostat=filestat)''
if (filestat > 0) call error_sub("Problem writing mwdisp.log in program mwdisp.x ",2)

write(1,fmt='(A)')''
write(1,fmt='(A)')''
write(1,fmt='(A,A)')'Program started: mwdisp.x version ',version
write(1,fmt='(A)')''

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Read Input
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

call def_geninp(mem,nproc,elprog,coordprog,printl,prtstep,mld,&
               &inihess,inihmod,hessmod,mixmod,hessstep,chess,&
               &consttyp,fixed_seed,iseed,restart,linconv,hdisp)

open(unit=20,file='geninp',status='old',iostat=filestat)
  if (filestat > 0) call error_sub("No geninp file for program mwdisp.x ",2)
  read(20, NML= geninp)
close(20)

call def_ircpar(rpsteps,maxhypstep,inirtrust,Rhyp,&
           &conv_grad,conv_disp,conv_uwdisp,conv_cons,miciter,&
           &conv_alpha,conv_rad,soph_l,maxrtrust,minrtrust,lratio,uratio)

open(unit=20,file='ircpar',status='old',iostat=filestat)
  if (filestat > 0) call error_sub("No geninp file for program mwdisp.x ",2)
  read(20, NML= ircpar)
close(20)

allocate(bmatrix(3*nat,nintc))
allocate(Gn12(nintc,nintc))
allocate(Gp12(nintc,nintc))
allocate(Z(nat))
allocate(geom(nat,3))
allocate(M(nat))
allocate(S(nat))
allocate(intgeom(nintc))
allocate(ig(nintc,nintc,2))

open(unit=10,file='geom.start',status='old',form='formatted',iostat=filestat)
if (filestat > 0) call error_sub("No geom.start file for program mwdisp.x ",2)
do i=1,nat
  read(10,fmt=*)S(i),Z(i),geom(i,1),geom(i,2),geom(i,3),M(i)
enddo
close(10)

open(unit=15,file='curr_intgeom',form='formatted',status='old',iostat=filestat)
if (filestat > 0) call error_sub("No curr_intgeom file for program mwdisp.x ",2)
 do i=1,nintc
    read(15,fmt=*)intgeom(i)
 enddo
close(15)

open(unit=30,file='curr_bmatrix',status='old',form='formatted',iostat=filestat)
if (filestat > 0) call error_sub("No curr_bmatrix file for program mwdisp.x ",2)
  do j=1,nat
    do i=1,3 ! iteration over x,y,z
      du1=nintc/3
      do l=1,du1
        read(30,fmt=*)(bmatrix((j-1)*3+i,((l-1)*3+k)),k=1,3,1)
      enddo
    enddo
  enddo
close(30)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! calculate displacements
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

call getGmatrices(bmatrix,M,Gn12,Gp12,nintc,nat)

if (printl .ge. 2) then
  write(1,fmt='(A)')'Used gmatrix Gp12 in case of preparing mwdisplacements'
  write(1,fmt='(A)')''
  do i=1,nintc
    do j=1,(nintc-1)
      write(1,fmt=outfmt,advance='no')Gp12(i,j)
    enddo
    write(1,fmt=outfmt,advance='yes')Gp12(i,j)
  enddo
  write(1,fmt='(A)')''
endif

call get_ig(Gp12,intgeom,ig,nintc,hdisp)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Write output
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

open(unit=29,file='digs',status='replace',form='formatted',iostat=filestat)
if (filestat > 0) call error_sub("Problem writing digs in program mwdisp.x ",2)
do i=1,nintc
  do k=1,2
    do j=1,nintc
      write(29,fmt=outfmt)ig(i,j,k)
    enddo
    write(29,fmt='(A)')'   '
  enddo
enddo
close(29)

deallocate(bmatrix)
deallocate(Gn12)
deallocate(Gp12)
deallocate(Z)
deallocate(geom)
deallocate(M)
deallocate(S)
deallocate(intgeom)
deallocate(ig)

write(1,fmt='(A)')'Program finished: mwdisp.x with SUCCESS'

close(1)

contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Subroutines
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

subroutine get_ig(Gp12,intgeom,ig,nintc,disp)
! This subroutine calculates displaced geometries along mass weigthed coordinates

use prec_mod
use units_mod
use general_mod
integer nintc
real(kind=dpr) Gn12(nintc,nintc),Gp12(nintc,nintc),disp
real(kind=dpr) bmatrix(3*nat,nintc),M(nat),intgeom(nintc)
real(kind=dpr) ig(nintc,nintc,2),wv1(nintc),wv2(nintc)
integer i,j,k

! intgeom ... internal geometry ............ [A|rad] 
! Gp12 ...... =G^(+1/2) .................... [(A|rad/(sqrt(amu)*A)] 
! ig ........ displaced int. geometries .... [A|rad]
! disp ...... dq for num. derivative ....... [A*sqrt(amu)]

do i=1,nintc
  wv1=0.0_dpr
  wv1(i)=disp ! wass weigthed displacement
  wv2=0.0_dpr
  call uwdisp(wv2,wv1,Gp12,nintc)
  
  ig(i,:,1)=intgeom-wv2
  ig(i,:,2)=intgeom+wv2
enddo

end subroutine

end program
