!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
program orientgeom ! written by Bernhard Sellner
! (c) Bernhard Sellner 2011
! part of MEPPACK
! This program is part of the reaction path package.
! It orients one geometry to a given reference 
! minimizing the least squares fit. The method 
! using quaternions is implemented
! The main subroutine is in units_mod.f90


use prec_mod
use units_mod
use general_mod
use def_mod
use nml_mod

implicit none
real(kind=dpr) geom2(:,:),geom1(:,:),Z(:),M(:),geomf(:,:)
integer i,j,k,l,filestat
character*5 version
character version_number,S(:)
allocatable geom1,geom2,Z,M,S,geomf

! Unit .................... input .... working
! geom* ... geometries .... [a.u.] ... [a.u.]
! M ....... atomic mass ... [amu] .... [amu] 

version='2.3.0'
filestat=0

open(unit=1,file='orientgeom.log',form='formatted',iostat=filestat)
write(1,fmt='(A)',iostat=filestat)''
if (filestat > 0) call error_sub("Problem writing orientgeom.log in program orientgeom.x ",2)

write(1,fmt='(A)')''
write(1,fmt='(A)')''
write(1,fmt='(A,A)')'Program started: orientgeom.x version ',version
write(1,fmt='(A)')''

call def_geninp(mem,nproc,elprog,coordprog,printl,prtstep,mld,&
               &inihess,inihmod,hessmod,mixmod,hessstep,chess,&
               &consttyp,fixed_seed,iseed,restart,linconv,hdisp)

open(unit=20,file='geninp',status='old',iostat=filestat)
  if (filestat > 0) call error_sub("No geninp file for program orientgeom.x ",2)
  read(20, NML= geninp)
close(20)

allocate(geom1(nat,3))
allocate(geom2(nat,3))
allocate(geomf(nat,3))
allocate(Z(nat))
allocate(M(nat))
allocate(S(nat))

open(unit=10,file='geom',form='formatted',status='old',iostat=filestat)
if (filestat > 0) call error_sub("No geom file for program orientgeom.x ",2)
do i=1,nat
  read(10,fmt=*)S(i),Z(i),geom1(i,1),geom1(i,2),geom1(i,3),M(i)
enddo
close(10)
open(unit=10,file='ref_geom',form='formatted',status='old',iostat=filestat)
if (filestat > 0) call error_sub("No ref_geom file for program orientgeom.x ",2)
do i=1,nat
  read(10,fmt=*)S(i),Z(i),geom2(i,1),geom2(i,2),geom2(i,3),M(i)
enddo
close(10)

if (printl.ge.1) then
  write(1,fmt='(A)')''
  write(1,fmt='(A)')'Cartesian geometry [a.u. resp. amu] to be adjusted:'
  do i=1,nat
    write(unit=1,fmt='(1X,A2,2X,F5.1,4F14.8)',advance='yes')S(i),Z(i),&
         &geom1(i,1),geom1(i,2),geom1(i,3),M(i)
  enddo
  write(1,fmt='(A)')''
  write(1,fmt='(A)')'Cartesian reference geometry [a.u. resp. amu]:'
  do i=1,nat
    write(unit=1,fmt='(1X,A2,2X,F5.1,4F14.8)',advance='yes')S(i),Z(i),&
         &geom2(i,1),geom2(i,2),geom2(i,3),M(i)
  enddo
  write(1,fmt='(A)')''
endif

call orient(geom1,geom2,geomf,nat,M,printl)

open(unit=30,file='new_geom',status='replace',form='formatted',iostat=filestat)
if (filestat > 0) call error_sub("Problem writing new_geom in program orientgeom.x ",2)
do i=1,nat
  write(unit=30,fmt='(1X,A2,2X,F5.1,4F14.8)',advance='yes')S(i),Z(i),&
  &geomf(i,1),geomf(i,2),geomf(i,3),M(i)
enddo
close(30)

if (printl.ge.1) then
  write(1,fmt='(A)')''
  write(1,fmt='(A)')'Adjusted cartesian geometry [a.u. resp. amu]:'
  do i=1,nat
    write(unit=1,fmt='(1X,A2,2X,F5.1,4F14.8)',advance='yes')S(i),Z(i),&
         &geomf(i,1),geomf(i,2),geomf(i,3),M(i)
  enddo
  write(1,fmt='(A)')''
endif

write(1,fmt='(A)')'Program finished: orientgeom.x with SUCCESS'

close(1)

deallocate(geom1)
deallocate(geom2)
deallocate(geomf)
deallocate(Z)
deallocate(M)
deallocate(S)

end program
