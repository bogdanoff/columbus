!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
program check_Rtrust 
! (c) Bernhard Sellner 2011
! part of MEPPACK
! This program is part of the reaction path package.
! The purpose is to compare the change in El. structure calculation
! and the change predicted by the algorithm. According to what the ratio
! is, the trust radius (step size on the hypersphere) is adjusted.

use prec_mod
use units_mod
use general_mod
use def_mod
use nml_mod

implicit none
real(kind=dpr) n_E,c_E,Qex,rtrust,dx(:),de
real(kind=dpr) ratio,cC_angs,nC_angs
integer i,j,k,l,it,filestat
character*5 version
character*1 n,t
allocatable dx
logical yesno

! Unit ....................................................... input ........... working
! n_E ....... Energy at the new geometry ..................... [a.u.] .......... [aJ]
! c_E ....... Energy at the current geometry ................. [a.u.] .......... [aJ]
! Qex ....... Energy change predicted by the algorithm ....... [aJ] ............ [aJ]
! rtrust .... Trust radius ................................... [A*sqrt(amu)] ... [A*sqrt(amu)]
! dx ........ step in orthogonal complement to restriction ... [A*sqrt(amu)] ... [A*sqrt(amu)]
! cC_angs ... Constraint at current geometry in Angstrom ..... [A*sqrt(amu)] ... [A*sqrt(amu)]
! nC_angs ... Constraint at new geometry in Angstrom ......... [A*sqrt(amu)] ... [A*sqrt(amu)]

version='2.3.0'

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Read Input
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

call def_geninp(mem,nproc,elprog,coordprog,printl,prtstep,mld,&
               &inihess,inihmod,hessmod,mixmod,hessstep,chess,&
               &consttyp,fixed_seed,iseed,restart,linconv,hdisp)

filestat=0
open(unit=10,file='geninp',status='old',iostat=filestat)
  if (filestat > 0) call error_sub("No geninp file for program check_Rtrust.x ",2)
  read(10, NML= geninp)
close(10)

call def_ircpar(rpsteps,maxhypstep,inirtrust,Rhyp,&
           &conv_grad,conv_disp,conv_uwdisp,conv_cons,miciter,&
           &conv_alpha,conv_rad,soph_l,maxrtrust,minrtrust,lratio,uratio)

open(unit=20,file='ircpar',status='old',iostat=filestat)
  if (filestat > 0) call error_sub("No ircpar file for program check_Rtrust.x ",2)
  read(20, NML= ircpar)
close(20)

allocate(dx(nintc-1))

open(unit=9,file='curr_iter',form='formatted',status='old',iostat=filestat)
  if (filestat > 0) call error_sub("No curr_iter file for program check_Rtrust.x ",2)
  read(9,fmt=*)it
close(9)

open(unit=1,file='check_Rtrust.log',form='formatted',iostat=filestat)
if (filestat > 0) call error_sub("Problem writing check_Rtrust.log in program check_Rtrust.x ",2)

write(1,fmt='(A)')''
write(1,fmt='(A)')''
write(1,fmt='(A)')''
write(1,fmt='(A,A)')'Program started : check_Rtrust.x version ',version
write(1,fmt='(A)')''

inquire(file='rtrust',exist=yesno)
if (yesno .eqv. .true.) then
  open(unit=20,file='rtrust',form='formatted')
    read(20,fmt=*)rtrust
  close(20)
else
  rtrust=inirtrust
endif

open(unit=11,file='curr_energy',form='formatted',status='old',iostat=filestat)
  if (filestat > 0) call error_sub("No curr_energy file for program check_Rtrust.x ",2)
  read(11,fmt=*)c_E
close(11)
open(unit=12,file='new_energy',form='formatted',status='old',iostat=filestat)
  if (filestat > 0) call error_sub("No new_energy file for program check_Rtrust.x ",2)
  read(12,fmt=*)n_E
close(12)

open(unit=11,file='curr_C_angs',form='formatted',status='old',iostat=filestat)
  if (filestat > 0) call error_sub("No curr_C_angs file for program check_Rtrust.x ",2)
  read(11,fmt=*)cC_angs
close(11)
open(unit=11,file='new_C_angs',form='formatted',status='old',iostat=filestat)
  if (filestat > 0) call error_sub("No new_C_angs file for program check_Rtrust.x ",2)
  read(11,fmt=*)nC_angs
close(11)

if (printl .ge. 2) then
  write(1,fmt='(A)')'Current energy in a u :'
  write(1,fmt=outfmt)c_E
  write(1,fmt='(A)')'New energy in a u :'
  write(1,fmt=outfmt)n_E
  write(1,fmt='(A)')'Current constraint [A*sqrt(amu)]'
  write(1,fmt=outfmt)cC_angs
  write(1,fmt='(A)')'New constraint [A*sqrt(amu)]'
  write(1,fmt=outfmt)nC_angs
  write(1,fmt='(A)')''
endif

open(unit=36,file='delta_x',form='formatted',status='old',iostat=filestat)
 if (filestat > 0) call error_sub("No delta_x file for program check_Rtrust.x ",2)
 do i=1,nintc-1
    read(36,fmt=*)dx(i)
 enddo
close(36)

open(unit=17,file='Qex',form='formatted',status='old',iostat=filestat)
  if (filestat > 0) call error_sub("No Qex file for program check_Rtrust.x ",2)
  read(17,fmt=*)Qex
close(17)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Calculate ratio and update trust radius
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

de=(n_E-c_E)*au2aj

ratio=de/Qex

! limes:
if ( ((dabs(de) .lt. 2.0E-8_dpr) .and. (dabs(Qex) .lt. 2.0E-8_dpr)) ) then
  ratio=1.0_dpr
endif

if (printl .ge. 1) then
  write(1,fmt='(A)',advance='no')'dE [aJ]:      '
  write(1,fmt=outfmt)dE
  write(1,fmt='(A)')''

  write(1,fmt='(A)',advance='no')'Qex [aJ]:     '
  write(1,fmt=outfmt)Qex
  write(1,fmt='(A)')''

  write(1,fmt='(A)',advance='no')'Ratio de/Qex: '
  write(1,fmt=outfmt)ratio
  write(1,fmt='(A)')''
endif

if (printl .ge. 2) then
  open(unit=27,file='see_ratio',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_ratio in program check_Rtrust.x ",2)
  write(27,fmt=outfmt)ratio
  close(27)

  open(unit=49,file='see_energy.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_energy.dat in program check_Rtrust.x ",2)
  write(49,fmt=outfmt,advance='yes')de
  close(49)
endif

if ((((hessmod.eq."calc").or.(hessmod.eq.'calcmw')).and. (mod(it,hessstep).eq.0)).or.&
&(((inihess.eq."calc").or.(inihess.eq.'calcmw')).and. (it.eq.1)))then
  write(1,fmt='(A)')'Adaption of trust radius deactivated'
  write(1,fmt='(A)')''
  write(1,fmt='(A)')'Setting trust radius to maxrtrust'
  write(1,fmt='(A)')''
  rtrust=maxrtrust
else
  if (ratio .lt. lratio) then
    write(1,fmt='(A)')'Lowering trust radius'
    write(1,fmt='(A)')''
    rtrust=rtrust/2.0_dpr
  endif

  if ((ratio.gt.uratio).and.(dabs(norm(dx,nintc-1)-rtrust).lt.conv_rad).and.(dabs(nC_angs).lt.dabs(cC_angs))) then
    write(1,fmt='(A)')'Increasing trust radius'
    write(1,fmt='(A)')''
    rtrust=rtrust*dsqrt(2.0_dpr)
  endif

  if (rtrust.gt.maxrtrust) then
    write(1,fmt='(A)')'Setting trust radius to maxrtrust'
    write(1,fmt='(A)')''
    rtrust=maxrtrust
  endif

  if (rtrust.lt.minrtrust) then
    write(1,fmt='(A)')'Setting trust radius to minrtrust'
    write(1,fmt='(A)')''
    rtrust=minrtrust
  endif

  if (printl .ge. 1) then
    write(1,fmt='(A)',advance='no')'New rtrust:'
    write(1,fmt=outfmt,advance='yes')rtrust
    write(1,fmt='(A)')''
  endif

  if (de.gt. 1.0E-3_dpr) then
    ! decide wether to reject the step
    if ((ratio .lt. 0.0_dpr).and.(rtrust.gt.minrtrust)) then
      write(1,fmt='(A)')'This step should be rejected: ratio < 0'
      write(1,fmt='(A)')''
      open(unit=20,file='reject',status='replace',form='formatted',iostat=filestat)
      if (filestat > 0) call error_sub("Problem writing reject in program check_Rtrust.x ",2)
      write(20,fmt='(A)')'Reject this step'
      close(20)
      if (rtrust.eq.minrtrust) then
        write(1,fmt='(A)')'Algorithm is trapped '
        write(1,fmt='(A)')''
        call error_sub('Algorithm is trapped; rtrust=minrtrust',2)
      endif
    endif
  endif
endif ! if ((((hessmod.eq."calc") ...

if (printl .ge. 2) then
  open(unit=27,file='see_rtrust',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_rtrust in program check_Rtrust.x ",2)
  write(27,fmt=outfmt)rtrust
  close(27)
endif

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Write output
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

open(unit=20,file='rtrust',status='replace',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing rtrust in program check_Rtrust.x ",2)
  write(20,fmt=outfmt)rtrust
close(20)

deallocate(dx)

write(1,fmt='(A)')'Program finished: check_Rtrust.x with SUCCESS '

close(1)

end program
