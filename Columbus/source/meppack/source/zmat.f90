!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
program zmat ! written by Bernhard Sellner
! (c) Bernhard Sellner 2011
! part of MEPPACK
! This program is part of the reaction path package.
! It handles everything directly related to Z-matrix 


use prec_mod
use units_mod
use general_mod
use def_mod
use nml_mod

implicit none
real(kind=dpr) wsr1,wsr2,wsr3,wsr4,wsr5
real(kind=dpr) hstr,hbend,htors,hdiag(:),intgeom(:)
real(kind=dpr) Z(:),geom(:,:),ngeom(:,:),M(:),wvr1(3),wvr2(3)
real(kind=dpr) dist(:),dist1(:),dist2(:),dist3(:),dist4(:)
real(kind=dpr) bmatrix(:,:),grad(:,:),intgrad(:)
integer stremap(:,:),bendmap(:,:),torsmap(:,:)
integer i,j,k,l,wi1,wi2,wi3,zmap(:,:),atord(:)
integer ii,du1,wi4,wi5,wi6,filestat
integer grd_c2i,geom_c2i,geom_i2c,intcfl,bmat
character S(:),wc1,wc2,wc3
character*5 version
allocatable hdiag,zmap,intgeom,geom,ngeom,M,S,Z,dist
allocatable dist1,dist2,dist3,dist4,atord,bmatrix,grad
allocatable intgrad,stremap,bendmap,torsmap
NAMELIST /zmatin/ grd_c2i,geom_c2i,geom_i2c,intcfl,bmat,&
                 &hstr,hbend,htors

! Units ......................... input .... working
! geom ... cart. geom ........... [a.u.] ... [A]
! grad ... cartesian gradient ... [a.u.] ... [aJ/A]
!
! hstr, hbend, htors ... values for diag. hess. ... [aJ/((A|rad)^2)]
! hdiag  ............... diagonal hessian ......... [aJ/((A|rad)^2)] 
! intgeom .............. internal geometry ........ [A|rad]
! ngeom ................ new cart. geom in zmat ... [a.u.] 
! dist* ................ arrays of distances ...... [a.u.]
! bmatrix .............. bmatrix .................. [A|rad/A]
! intgrad .............. internal gradient ........ [aJ/(A|rad)]
!
! atord ... ordering of atoms (not used); taken from geom
!
! zmap, stremap, bendmap, torsmap ......... mapping arrays
! grd_c2i,geom_c2i,geom_i2c,intcfl,bmat ... to guide the program

version='2.3.0'
filestat=0

open(unit=1,file='zmat.log',form='formatted',iostat=filestat)
write(1,fmt='(A)',iostat=filestat)''
if (filestat > 0) call error_sub("Problem writing zmat.log in program zmat.x ",2)

write(1,fmt='(A)')''
write(1,fmt='(A)')''
write(1,fmt='(A,A)')'Program started: zmat.x version ',version
write(1,fmt='(A)')''

! initial values: do nothing
grd_c2i  = 0
geom_c2i = 0
geom_i2c = 0
intcfl   = 0
bmat     = 0

!initialize diagonal hessian elements
hstr=0.50E+01
hbend=0.10E+01
htors=0.35E+00

call def_geninp(mem,nproc,elprog,coordprog,printl,prtstep,mld,&
               &inihess,inihmod,hessmod,mixmod,hessstep,chess,&
               &consttyp,fixed_seed,iseed,restart,linconv,hdisp)

open(unit=20,file='geninp',status='old',iostat=filestat)
  if (filestat > 0) call error_sub("No geninp file for program zmat.x ",2)
  read(20, NML= geninp)
close(20)

open(unit=21,file='zmatin',status='old',iostat=filestat)
  if (filestat > 0) call error_sub("No zmatin file for program zmat.x ",2)
  read(21, NML= zmatin)
close(21)

allocate(grad(nat,3))
allocate(hdiag(nintc))
allocate(zmap(nat,3))
allocate(intgeom(nintc))
allocate(Z(nat))
allocate(geom(nat,3))
allocate(ngeom(nat,3))
allocate(M(nat))
allocate(S(nat))
allocate(atord(nat))
allocate(bmatrix(3*nat,nintc))
allocate(intgrad(nintc))
allocate(stremap(nat-1,2))
allocate(bendmap(nat-2,3))
allocate(torsmap(nat-3,4))

!initialize atom order
do i=1,nat
  atord(i)=i
enddo

!initialize mapping vector
do i=1,nat
  do j=1,3
    zmap(i,j)=0
  enddo
enddo

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! intcfl
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

if (intcfl.eq.1) then
  write(1,fmt='(A)')''
  write(1,fmt='(A)')'Constructing intcfl file'
  write(1,fmt='(A)')''

  open(unit=10,file='geom',form='formatted',status='old',iostat=filestat)
  if (filestat > 0) call error_sub("No geom file for program zmat.x ",2)
  do i=1,nat
    read(10,fmt=*)S(i),Z(i),geom(i,1),geom(i,2),geom(i,3),M(i)
  enddo
  close(10)

  zmap(atord(2),1)=atord(1)

  allocate(dist(nat))
  allocate(dist1(nat))
  allocate(dist2(nat))
  allocate(dist3(nat))

  do i=3,nat
    do j=1,i
      dist(j)=norm(geom(atord(i),:)-geom(atord(j),:),3)
    enddo
    k=1
    do j=1,i
      if (j.ne.i) then
        dist1(k)=dist(j)
        k=k+1
      endif
    enddo
    !1st neighbour
    wsr1=minval(dist1(1:(i-1)))
    do j=1,nat
      if (wsr1.eq.dist(j)) then
        zmap(atord(i),1)=atord(j)
      endif
    enddo
    k=1
    do j=1,nat-1
      if (dist1(j).ne.wsr1) then
        dist2(k)=dist1(j)
        k=k+1
      endif
    enddo        
    !2nd neighbour
    wsr1=minval(dist2(1:(i-2)))
    do j=1,nat
      if (wsr1.eq.dist(j)) then
        zmap(atord(i),2)=atord(j)
      endif
    enddo
    if (i.ge.4) then
      k=1
      do j=1,nat-2
        if (dist2(j).ne.wsr1) then
          dist3(k)=dist2(j)
          k=k+1
        endif
      enddo
      !3rd neighbour
      wsr1=minval(dist3(1:(i-3)))
      do j=1,nat
        if (wsr1.eq.dist(j)) then
          zmap(atord(i),3)=atord(j)
        endif
      enddo
    endif
  enddo

  deallocate(dist3)
  deallocate(dist)
  deallocate(dist1)
  deallocate(dist2)

  if (printl.ge.1) then
    write(1,fmt='(A)')'Array of neighbours '
    write(1,fmt='(A)')'Atom  1st  2nd  3rd '
    do i=1,nat
      write(1,fmt='(4(I4,X))')i,zmap(i,1),zmap(i,2),zmap(i,3)
    enddo
    write(1,fmt='(A)')''
  endif
  
  !create mapping list
  do i=2,nat
    stremap(i-1,1)=zmap(atord(i),1)
    stremap(i-1,2)=atord(i)
  enddo
  do i=3,nat
    bendmap(i-2,1)=zmap(atord(i),2)
    bendmap(i-2,2)=atord(i)
    bendmap(i-2,3)=zmap(atord(i),1)
  enddo
  do i=4,nat
    torsmap(i-3,1)=zmap(atord(i),3)
    torsmap(i-3,2)=zmap(atord(i),2)
    torsmap(i-3,3)=zmap(atord(i),1)
    torsmap(i-3,4)=atord(i)
  enddo

  do i=1,nat-1
    hdiag(i)=hstr
  enddo
  do i=1,nat-2
    hdiag(i+nat-1)=hbend
  enddo
  do i=1,nat-3
    hdiag(i+nat*2-3)=htors
  enddo

  if (printl.ge.1) then
    write(1,fmt='(A)')'Diagonal hessian elements'
    do i=1,nintc
      write(1,fmt='(F12.8)')hdiag(i)
    enddo
    write(1,fmt='(A)')''
  endif

  open (unit=9,file='intcfl',form='formatted',status='replace',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing intcfl in program zmat.x ",2)
  write (9,fmt='(A)')'TEXAS'
  ! write stretching
  do i=2,nat
    write (9,fmt='(A,I4,I5,A,4X,I5,A)')'K                   STRE',(i-1),stremap(i-1,1),'.',stremap(i-1,2),'.' 
  enddo
  ! write angles
  do i=3,nat
    write (9,fmt='(A,I4,I6,A,4X,I5,A,4X,I5,A)')'K            1.     BEND'&
    &,(i+nat-3),bendmap(i-2,1),'.',bendmap(i-2,2),'.',bendmap(i-2,3),'.'
  enddo
  !write dihedrals
  do i=4,nat
    write (9,fmt='(A,I4,I6,A,4X,I5,A,4X,I5,A,4X,I5,A)')'K1.0000000   1.     TORS'&
    &,(i+2*nat-6),torsmap(i-3,1),'.',torsmap(i-3,2),'.',torsmap(i-3,3),'.',torsmap(i-3,4),'.'
  enddo
  wi2=(nintc-mod(nintc,8))/8
  wi1=mod(nintc,8)
  do i=1,wi2
   do j=1,7
     write (9,fmt='(E9.2E2)',advance='no')hdiag(j+(i-1)*8)
   enddo
   write (9,fmt='(E9.2E2)',advance='yes')hdiag(8+(i-1)*8)
  enddo
  if (wi1 .ne. 0) then
    do j=1,wi1
       write (9,fmt='(E9.2E2)',advance='no')hdiag(j+(wi2*8))
     enddo
    write (9,fmt='(X,8(E9.2E2))')
  endif
  close (9)

  write(1,fmt='(A)')''
  write(1,fmt='(A)')'intcfl contruction finished'
  write(1,fmt='(A)')''

elseif (intcfl.eq.0) then

  if (printl.ge.2) then
    write(1,fmt='(A)')''  
    write(1,fmt='(A)')'Old intcfl file used'  
    write(1,fmt='(A)')''  
  endif

  !read intcfl
  open(unit=16,file='intcfl',form='formatted',status='old',iostat=filestat)
  if (filestat > 0) call error_sub("No intcfl file for program zmat.x ",2)
  read(16,fmt=*)
  do i=2,nat
    read(16,fmt=*)wc1,wc2,wi1,wsr1,wsr2
    stremap(i-1,1)=idint(wsr1)
    stremap(i-1,2)=idint(wsr2)
  enddo
  do i=3,nat
    read(16,fmt=*)wc1,wsr4,wc2,wi5,wsr1,wsr2,wsr3
      bendmap(i-2,1)=idint(wsr1)
      bendmap(i-2,2)=idint(wsr2)
      bendmap(i-2,3)=idint(wsr3)
  enddo
  do i=4,nat
    read(16,fmt=*)wc1,wsr5,wc2,wi6,wsr1,wsr2,wsr3,wsr4
      torsmap(i-3,1)=idint(wsr1)
      torsmap(i-3,2)=idint(wsr2)
      torsmap(i-3,3)=idint(wsr3)
      torsmap(i-3,4)=idint(wsr4)
  enddo
  close(16)
  
endif

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! geom_c2i
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

if (geom_c2i.eq.1) then
  if (printl.ge.1) then
    write(1,fmt='(A)')''
    write(1,fmt='(A)')'Converting cartesian geometry to internal'
    write(1,fmt='(A)')''
  endif

  !convert geom to internal
  open(unit=10,file='geom',form='formatted',status='old',iostat=filestat)
  if (filestat > 0) call error_sub("No geom file for program zmat.x ",2)
  do i=1,nat
    read(10,fmt=*)S(i),Z(i),geom(i,1),geom(i,2),geom(i,3),M(i)
  enddo
  close(10)
  ! from a.u. to Angstrom
  geom=geom*au2ang

  do i=2,nat
    wvr1=geom(stremap(i-1,2),:)-geom(stremap(i-1,1),:)
    intgeom(i-1)=norm(wvr1,3)
  enddo

  if (nat.ge.3) then
    do i=3,nat
      intgeom(i+nat-3)=calc_angle(geom(bendmap(i-2,2),:),geom(bendmap(i-2,3),:)&
                                 &,geom(bendmap(i-2,1),:),geom(bendmap(i-2,3),:),3)
    enddo
  endif

  if (nat.ge.4) then
    do i=4,nat
      intgeom(i+2*nat-6)=calc_tors(geom(torsmap(i-3,4),:),geom(torsmap(i-3,3),:)&
                                  &,geom(torsmap(i-3,2),:),geom(torsmap(i-3,1),:),3)
    enddo
  endif

  open(unit=15,file='intgeom',status='replace',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing intgeom in program zmat.x ",2)
  do i=1,nintc
    write(15,fmt='(F14.8)')intgeom(i)
  enddo
  close(15)

  if (printl.ge.1) then
    write(1,fmt='(A)')''
    write(1,fmt='(A)')'Converting cartesian geometry to internal: finished'
    write(1,fmt='(A)')''
  endif
endif

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! bmat
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

if (bmat.eq.1) then
  if (printl.ge.1) then
    write(1,fmt='(A)')''
    write(1,fmt='(A)')'Constructing B matrix'
    write(1,fmt='(A)')''
  endif

  open(unit=10,file='geom',form='formatted',status='old',iostat=filestat)
  if (filestat > 0) call error_sub("No geom file for program zmat.x ",2)
  do i=1,nat
    read(10,fmt=*)S(i),Z(i),geom(i,1),geom(i,2),geom(i,3),M(i)
  enddo
  close(10)
  ! from a.u. to Angstrom
  geom=geom*au2ang
  ! create bmatrix
  call createbmat(bmatrix,intgeom,geom,stremap,bendmap,torsmap,nintc,nat)

  open(unit=40,file='bmatrix',status='replace',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing bmatrix in program zmat.x ",2)
  do j=1,nat
    do ii=1,3 ! iteration over x,y,z
      du1=nintc/3
      do l=1,du1
        write(unit=40,fmt='(3(E23.14E3))')bmatrix((j-1)*3+ii,((l-1)*3+1)),&
        &bmatrix((j-1)*3+ii,((l-1)*3+2)),bmatrix((j-1)*3+ii,((l-1)*3+3))
      enddo
    enddo
  enddo

  if (printl.ge.1) then
    write(1,fmt='(A)')''
    write(1,fmt='(A)')'B matrix contructed'
    write(1,fmt='(A)')''
  endif
endif

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! grd_c2i
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

if ( grd_c2i.eq.1) then
  if (printl.ge.1) then
    write(1,fmt='(A)')''
    write(1,fmt='(A)')'Cartesian gradient to internal'
    write(1,fmt='(A)')''
  endif

  ! convert cartesian gradient to internal
  open(unit=35,file='cartgrd',form='formatted',status='old',iostat=filestat)
  if (filestat > 0) call error_sub("No cartgrd file for program zmat.x ",2)
  do i=1,nat
    read(unit=35,fmt=*)(grad(i,j),j=1,3,1)
  enddo
  close(35)

  call grad2int(bmatrix,grad,intgrad,nintc,nat)

  open(unit=45,file='gradE_q',status='replace',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing gradE_q in program zmat.x ",2)
    do i=1,nintc
      write(45,fmt='(F14.8)')intgrad(i)
    enddo
  close (45)

  if (printl.ge.1) then
    write(1,fmt='(A)')''
    write(1,fmt='(A)')'Cartesian gradient to internal: finished'
    write(1,fmt='(A)')''
  endif
endif

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! geom_i2c
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

if (geom_i2c.eq.1) then
  if (printl.ge.1) then
    write(1,fmt='(A)')''
    write(1,fmt='(A)')'Construct cartesian geometry from internal'
    write(1,fmt='(A)')''
  endif

  open(unit=15,file='intgeom',form='formatted',status='old',iostat=filestat)
  if (filestat > 0) call error_sub("No intgeom file for program zmat.x ",2)
   do i=1,nintc
      read(15,fmt=*)intgeom(i)
   enddo
  close(15)

  !convert intgeom from A to a.u.
  do i=1,nat-1
    intgeom(i)=intgeom(i)/au2ang
  enddo

  !first atom
  ngeom(atord(1),1)=0.0_dpr
  ngeom(atord(1),2)=0.0_dpr
  ngeom(atord(1),3)=0.0_dpr

  !sec. atom
  ngeom(atord(2),1)=intgeom(1)
  ngeom(atord(2),2)=0.0_dpr
  ngeom(atord(2),3)=0.0_dpr

  !third atom
  if (stremap(3-1,1).eq.1) then
    ngeom(atord(3),1)=intgeom(2)*dcos(intgeom(nat))
    ngeom(atord(3),2)=intgeom(2)*dsin(intgeom(nat))
  elseif (stremap(3-1,1).eq.2) then
    ngeom(atord(3),1)=ngeom(stremap(3-1,1),1)-intgeom(2)*dcos(intgeom(nat))
    ngeom(atord(3),2)=intgeom(2)*dsin(intgeom(nat))
  endif

  ngeom(atord(3),3)=0.0_dpr

  !nth atom
  if (nat.ge.4) then
    do i=4,nat
      call genrot(ngeom(torsmap(i-3,4),:),ngeom(torsmap(i-3,3),:),ngeom(torsmap(i-3,2),:),ngeom(torsmap(i-3,1),:),&
                 &intgeom(i-1),-intgeom(i+2*nat-6),intgeom(i+nat-3),nintc)
    enddo
  endif

  open(unit=30,file='zmat_geom',status='replace',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing zmat_geom in program zmat.x ",2)
  do i=1,nat
    write(unit=30,fmt='(1X,A2,2X,F5.1,4F14.8)',advance='yes')S(atord(i)),Z(atord(i)),&
    &ngeom(atord(i),1),ngeom(atord(i),2),ngeom(atord(i),3),M(atord(i))
  enddo
  close(30)

  if (printl.ge.1) then
    write(1,fmt='(A)')''
    write(1,fmt='(A)')'Construct cartesian geometry from internal: finished'
    write(1,fmt='(A)')''
  endif
endif

deallocate(hdiag)
deallocate(zmap)
deallocate(intgeom)
deallocate(geom)
deallocate(M)
deallocate(S)
deallocate(Z)
deallocate(grad)
deallocate(intgrad)
deallocate(bmatrix)
deallocate(ngeom)
deallocate(atord)
deallocate(stremap)
deallocate(bendmap)
deallocate(torsmap)

write(1,fmt='(A)')'Program finished: zmat.x with SUCCESS'

close(1)

contains

subroutine grad2int(bmatrix,grad,intgrad,dimn,wnat)
! This subroutine converts the cartesian gradient 
! to the internal gradient

use prec_mod
use units_mod
use general_mod

implicit none
integer dimn,wnat
integer i,j,k,l,ii,jj,info,lwork,du1
real(kind=dpr) bmatrix(3*wnat,dimn),grad(wnat,3),intgrad(dimn)
real(kind=dpr) wmr1(dimn,dimn),wmr2(dimn,dimn),wmr3(dimn,dimn)
real(kind=dpr) U(dimn,dimn),d(dimn),e(dimn),work(:)
real(kind=dpr) tau(dimn),tgrd(3*wnat),wvr1(dimn)
allocatable work

! Unit ............................ input .......... working
! grad ...... cart. gradient  ..... [a.u.] ......... [aJ/A]
! bmatrix ... bmatrix ............. [A|rad/A] ...... [A|rad/A]
! intgrad ... internal gradient ... [aJ/(A|rad)] ... [aJ/(A|rad)]

do i=1,wnat
  do j=1,3
    tgrd((i-1)*3+j)=grad(i,j)
  enddo
enddo

!This converts from a.u. to aJ/A
tgrd=tgrd*(au2aj/au2ang)

if (printl.ge.2) then
  write(1,fmt='(A)')'Cartesian gradient / a.u.'
  do i=1,wnat-1
    write(1,fmt='(3(X,F14.8))')grad(i,1),grad(i,2),grad(i,3)
  enddo
  write(1,fmt='(3(X,F14.8))')grad(wnat,1),grad(wnat,2),grad(wnat,3)
  write(1,fmt='(A)')''
  write(1,fmt='(A)')'Cartesian gradient / aJ/A'
  do i=1,3*wnat
    write(1,fmt='(X,F14.8)')tgrd(i)
  enddo
  write(1,fmt='(A)')''
  write(1,fmt='(A)')'B matrix A|rad/A'
  write(1,fmt='(A)')''
  do j=1,wnat
    do ii=1,3 ! iteration over x,y,z
      du1=dimn/3
      do l=1,du1
        write(unit=1,fmt='(3(E23.14E3))')bmatrix((j-1)*3+ii,((l-1)*3+1)),&
        &bmatrix((j-1)*3+ii,((l-1)*3+2)),bmatrix((j-1)*3+ii,((l-1)*3+3))
      enddo
    enddo
  enddo
  write(1,fmt='(A)')''
endif

!new:wmr1= bmatrixT*bmatrix
call DGEMM('t','n',dimn,dimn,3*wnat,1.0_dpr,bmatrix,3*wnat,bmatrix,3*wnat,0.0_dpr,wmr1,dimn)

info=0
U=wmr1
!bring into tridiagonal form
allocate(work(dimn))
call DSYTRD('U',dimn,U,dimn,d,e,tau,work,-1,info)
if (info.ne.0) call error_sub("Problem with DSYTRD in sub grad2int",2)
lwork=idint(work(1))
deallocate(work)
allocate(work(lwork))
call DSYTRD('U',dimn,U,dimn,d,e,tau,work,lwork,info)
if (info.ne.0) call error_sub("Problem with DSYTRD in sub grad2int",2)
call DORGTR('U',dimn,U,dimn,tau,work,-1,info)
if (info.ne.0) call error_sub("Problem with DORGTR in sub grad2int",2)
lwork=idint(work(1))
deallocate(work)
allocate(work(lwork))
call DORGTR('U',dimn,U,dimn,tau,work,lwork,info)
if (info.ne.0) call error_sub("Problem with DORGTR in sub grad2int",2)
!calculate Eval and Evecs
call DSTEQR('V',dimn,d,e,U,dimn,work,info)
if (info.ne.0) call error_sub("Problem with DSTEQR in sub grad2int",2)
deallocate(work)

wmr3=0.0_dpr
do i=1,dimn
  wmr3(i,i)=1.0_dpr/d(i)
  !print*,'d(i)',d(i)
enddo

call DGEMM('n','t',dimn,dimn,dimn,1.0_dpr,wmr3,dimn,U,dimn,0.0_dpr,wmr2,dimn)
call DGEMM('n','n',dimn,dimn,dimn,1.0_dpr,U,dimn,wmr2,dimn,0.0_dpr,wmr3,dimn)

call DGEMV('t',3*wnat,dimn,1.0_dpr,bmatrix,3*wnat,tgrd,1,0.0_dpr,wvr1,1)
call DGEMV('n',dimn,dimn,1.0_dpr,wmr3,dimn,wvr1,1,0.0_dpr,intgrad,1)

if (printl.ge.2) then
  write(1,fmt='(A)')''
  write(1,fmt='(A)')'Internal gradient / aJ*A|rad^-1'
  write(1,fmt='(A)')''
  do i=1,(dimn-1)
    write(1,fmt=outfmt)intgrad(i)
  enddo
  write(1,fmt=outfmt)intgrad(dimn)
  write(1,fmt='(A)')''
endif

end subroutine

subroutine createbmat(bmatrix,intgeom,geom,stremap,bendmap,torsmap,wnintc,wnat)
! This subroutine creates the bmatrix. 

use prec_mod
use units_mod
use general_mod

implicit none
integer wnintc,wnat
integer i,j,k,l,ii,jj
integer stremap(wnat-1,2),bendmap(wnat-2,3),torsmap(wnat-3,4)
real(kind=dpr) bmat(wnintc,3*wnat),bmatrix(3*wnat,wnintc),intgeom(wnintc),geom(wnat,3)
real(kind=dpr) wvr1(3),wvr2(3),wvr3(3),wvr4(3),wvr5(3),wvr6(3),wvr7(3)
real(kind=dpr) wsr1,wsr2,wsr3,wsr4,wsr5,wsr6,wsr7

! bmatrix ... bmatrix ............. [A|rad/A] 
! geom ...... cart. geom .......... [A]
! intgeom ... internal geometry ... [A|rad]

bmat=0.0_dpr

!stretchings
do i=2,wnat
  wvr1=geom(stremap(i-1,2),:)-geom(stremap(i-1,1),:)
  wvr1=wvr1/norm(wvr1,3)  
  do j=1,3
    bmat(i-1,(stremap(i-1,2)-1)*3+j)=wvr1(j)
    bmat(i-1,(stremap(i-1,1)-1)*3+j)=-wvr1(j)
  enddo
enddo

!angles
do i=3,wnat
  wvr1=geom(bendmap(i-2,2),:)-geom(bendmap(i-2,3),:) 
  wvr2=geom(bendmap(i-2,1),:)-geom(bendmap(i-2,3),:) 
  wsr1=norm(wvr1,3)
  wsr2=norm(wvr2,3)
  ! could also be looked up in intgeom
  wvr1=wvr1/wsr1
  wvr2=wvr2/wsr2
  wvr3=(dcos(intgeom(i+wnat-3))*wvr1-wvr2)/(wsr1*dsin(intgeom(i+wnat-3)))
  wvr4=(dcos(intgeom(i+wnat-3))*wvr2-wvr1)/(wsr2*dsin(intgeom(i+wnat-3)))
  wvr5=((wsr1-wsr2*dcos(intgeom(i+wnat-3)))*wvr1+(wsr2-wsr1*dcos(intgeom(i+wnat-3)))*wvr2)/&
      &(wsr1*wsr2*dsin(intgeom(i+wnat-3)))

  do j=1,3
    bmat(i+wnat-3,(bendmap(i-2,3)-1)*3+j)=wvr5(j)
    bmat(i+wnat-3,(bendmap(i-2,1)-1)*3+j)=wvr4(j)
    bmat(i+wnat-3,(bendmap(i-2,2)-1)*3+j)=wvr3(j)
  enddo
enddo

do i=4,wnat
  wvr1=geom(torsmap(i-3,3),:)-geom(torsmap(i-3,4),:)  !43
  wvr2=geom(torsmap(i-3,2),:)-geom(torsmap(i-3,3),:) !32
  wvr3=geom(torsmap(i-3,2),:)-geom(torsmap(i-3,1),:) !12
  wsr1=norm(wvr1,3)
  wsr2=norm(wvr2,3)
  wsr3=norm(wvr3,3)
  wvr1=wvr1/wsr1
  wvr2=wvr2/wsr2
  wvr3=wvr3/wsr3

  call crossprod(wvr3,-wvr2,wvr6)
  wsr4=calc_angle(geom(torsmap(i-3,3),:),geom(torsmap(i-3,2),:),geom(torsmap(i-3,1),:),geom(torsmap(i-3,2),:),3)
  wvr4=-wvr6*(1.0_dpr/(wsr3*(dsin(wsr4)**2)))  ! st1
  do j=1,3
    bmat(i+2*wnat-6,(torsmap(i-3,1)-1)*3+j)=wvr4(j)
  enddo

  call crossprod(wvr1,wvr2,wvr7)
  wsr5=calc_angle(geom(torsmap(i-3,4),:),geom(torsmap(i-3,3),:),geom(torsmap(i-3,2),:),geom(torsmap(i-3,3),:),3)
  wvr4=-wvr7*(1.0_dpr/(wsr1*(dsin(wsr5)**2)))  ! st4
  do j=1,3
    bmat(i+2*wnat-6,(torsmap(i-3,4)-1)*3+j)=wvr4(j)
  enddo

  wvr4=((wsr2-wsr3*dcos(wsr4))/(wsr2*wsr3*dsin(wsr4)))*(1/(dsin(wsr4)))*wvr6&  !st2
      &+(dcos(wsr5)/(wsr2*dsin(wsr5)))*(1/(dsin(wsr5)))*wvr7
  do j=1,3
    bmat(i+2*wnat-6,(torsmap(i-3,2)-1)*3+j)=wvr4(j)
  enddo

  wvr4=((wsr2-wsr1*dcos(wsr5))/(wsr2*wsr1*dsin(wsr5)))*(1/(dsin(wsr5)))*wvr7&  !st3
      &+(dcos(wsr4)/(wsr2*dsin(wsr4)))*(1/(dsin(wsr4)))*wvr6
  do j=1,3
    bmat(i+2*wnat-6,(torsmap(i-3,3)-1)*3+j)=wvr4(j)
  enddo
enddo

do i=1,3*wnat
  do j=1,wnintc
    bmatrix(i,j)=bmat(j,i)
  enddo
enddo

end subroutine

subroutine genrot(vecL,vecK,vecJ,vecI,rlk,a_ijkl,a_jkl,wnintc)
! This subroutine calculates the cartesian position of the nth atom 
! (n>3) in the zmat arrangement.

use prec_mod
use units_mod
use general_mod

implicit none
integer wnintc
integer i,j,k,l,ii,jj
real(kind=dpr) vecI(3),vecJ(3),vecK(3),vecL(3)
real(kind=dpr) wvr1(3),wvr2(3),wvr3(3),wvr4(3)
real(kind=dpr) wmr1(3,3),wmr2(3,3),wmr3(3,3)
real(kind=dpr) wsr1,wsr2,wsr3,wsr4,wsr5,wsr6,intgeom(wnintc)
real(kind=dpr) rlk,a_ijkl,a_jkl
real(kind=dpr) Mrot(3,3)

! vec* ..... a.u.
! rlk ...... a.u.
! a_ijkl ... rad
! a_jkl .... rad

!bc
wvr1=vecK-vecJ
wvr1=-wvr1/norm(wvr1,3)

!initial guess
vecL=wvr1*rlk

!ab
wvr2=vecJ-vecI
wvr2=wvr2/norm(wvr2,3)

call crossprod(wvr2,wvr1,wvr3)
!n
wvr3=wvr3/norm(wvr3,3)

!rotation about bending
wvr4=wvr3
wmr1=0.0_dpr
wmr2=0.0_dpr
wmr3=0.0_dpr
Mrot=0.0_dpr
do i=1,3
  wmr1(i,i)=1.0_dpr
enddo

wmr1=wmr1*dcos(a_jkl)

call tp(wmr2,wvr4,wvr4,3)

wmr2=wmr2*(1-dcos(a_jkl))

wmr3(1,2)=-wvr4(3)
wmr3(1,3)=wvr4(2)
wmr3(2,1)=wvr4(3)
wmr3(2,3)=-wvr4(1)
wmr3(3,1)=-wvr4(2)
wmr3(3,2)=wvr4(1)

wmr3=wmr3*dsin(a_jkl)

Mrot=wmr1+wmr2+wmr3

call DGEMV('n',3,3,1.0_dpr,Mrot,3,vecL,1,0.0_dpr,wvr2,1)

!rotation about torsion
wvr4=wvr1
wmr1=0.0_dpr
wmr2=0.0_dpr
wmr3=0.0_dpr
Mrot=0.0_dpr
do i=1,3
  wmr1(i,i)=1.0_dpr
enddo

wmr1=wmr1*dcos(a_ijkl)

call tp(wmr2,wvr4,wvr4,3)

wmr2=wmr2*(1-dcos(a_ijkl))

wmr3(1,2)=-wvr4(3)
wmr3(1,3)=wvr4(2)
wmr3(2,1)=wvr4(3)
wmr3(2,3)=-wvr4(1)
wmr3(3,1)=-wvr4(2)
wmr3(3,2)=wvr4(1)

wmr3=wmr3*dsin(a_ijkl)

Mrot=wmr1+wmr2+wmr3

call DGEMV('n',3,3,1.0_dpr,Mrot,3,wvr2,1,0.0_dpr,vecL,1)

!displace vector
vecL=vecL+vecK

end subroutine

function calc_angle(vec4,vec3,vec2,vec1,vdim)
!This function calculates the angle [rad]:
!             <R4-R3,R2-R1>
!a = acos ---------------------
!          ||R4-R3||*||R2-R1||

use prec_mod
use units_mod

implicit none
integer i,j,vdim
real(kind=dpr) vec1(vdim),vec2(vdim),vec3(vdim),vec4(vdim),wvr1(vdim),wvr2(vdim)
real(kind=dpr) wsr1,wsr2,calc_angle

wvr1=vec4-vec3
wvr2=vec2-vec1

calc_angle=dacos(sp(wvr1,wvr2,vdim)/(norm(wvr1,vdim)*norm(wvr2,vdim)))

end function

function calc_tors(vec4,vec3,vec2,vec1,vdim)
!This funtion calculates the torsional angle [rad]:
!     R4
!       \
!        R3-R2
!             \
!              R1

use prec_mod
use units_mod
implicit none
integer i,j,vdim
real(kind=dpr) vec1(vdim),vec2(vdim),vec3(vdim),vec4(vdim),wvr1(vdim),wvr2(vdim)
real(kind=dpr) wsr1,wsr2,calc_tors,wvr3(vdim)
real(kind=dpr) wmr1(3,3)

wvr1=vec3-vec2
wvr1=wvr1/norm(wvr1,vdim)

wvr2=vec1-vec2
wvr3=vec4-vec3

wvr2=wvr2/norm(wvr2,vdim)
wvr3=wvr3/norm(wvr3,vdim)

wvr2=wvr2-sp(wvr1,wvr2,vdim)*wvr1
wvr3=wvr3-sp(wvr1,wvr3,vdim)*wvr1

wmr1(:,1)=wvr2
wmr1(:,2)=wvr1
wmr1(:,3)=wvr3

wsr1=calcdet(wmr1)

wsr2=sp(wvr2,wvr3,vdim)/(norm(wvr2,vdim)*norm(wvr3,vdim))

if (wsr1.gt.0.0_dpr) then
  !MOLDEN:
  if (wsr2.lt.-1.0_dpr) then
    calc_tors=-dacos(-1.0_dpr) 
  elseif (wsr2.gt.1.0_dpr) then
    calc_tors=-dacos(1.0_dpr)
  else
    calc_tors=-dacos(sp(wvr2,wvr3,vdim)/(norm(wvr2,vdim)*norm(wvr3,vdim)))
  endif
  !COLUMBUS:
  !calc_tors=2.0_dpr*pi-dacos(sp(wvr2,wvr3,vdim)/(norm(wvr2,vdim)*norm(wvr3,vdim)))
else
  if (wsr2.lt.-1.0_dpr) then
    calc_tors=-dacos(-1.0_dpr)
  elseif (wsr2.gt.1.0_dpr) then
    calc_tors=-dacos(1.0_dpr)
  else
    calc_tors=dacos(sp(wvr2,wvr3,vdim)/(norm(wvr2,vdim)*norm(wvr3,vdim)))
  endif
endif

end function

end program
