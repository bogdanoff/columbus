!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
! (c) Bernhard Sellner 2011
! part of MEPPACK

MODULE nml_mod

use prec_mod

implicit none
integer nintc,Nat,printl,iseed,inidirect,nmode,rpsteps,maxhypstep,consttyp
integer hessstep,mem,nproc,images,maxit,startci,startdiis,maxgdiis,miciter
integer prtstep,bmupd,restart,linconv,nofidiis,nprocneb,initneb
integer licneb,doneb,adjneb,nim,mld,cipoints
real(kind=dpr) Rhyp,inirtrust,scmin,scmax,conv_grad,conv_disp,matfact
real(kind=dpr) conv_cons,conv_alpha,conv_rad,conv_uwdisp,stepres
real(kind=dpr) maxrtrust,minrtrust,lratio,uratio,hdisp
real(kind=dpr) maxddiis
logical fixed_seed,chess,soph_l
character*6 elprog
character*6 inihess,inihmod,mixmod,hessmod,tangtype,nebtype,coordprog
character*10 hup

NAMELIST /ircpar/ rpsteps,maxhypstep,inidirect,nmode,inirtrust,Rhyp,&
                 &conv_grad,conv_disp,conv_uwdisp,conv_cons,miciter,&
                 &conv_alpha,conv_rad,soph_l,maxrtrust,minrtrust,lratio,uratio
NAMELIST /geninp/ mem,nproc,elprog,coordprog,printl,prtstep,mld,nintc,Nat,&
                 &inihess,inihmod,hessmod,mixmod,hessstep,chess,&
                 &consttyp,fixed_seed,iseed,restart,linconv,hdisp
NAMELIST /nebpar/ licneb,nim,adjneb,initneb,doneb,maxit,nprocneb,matfact,&
                 &tangtype,nebtype,startci,startdiis,maxgdiis,&
                 &maxddiis,nofidiis,bmupd,stepres,scmin,scmax,conv_grad,conv_disp,&
                 &cipoints

END MODULE nml_mod
