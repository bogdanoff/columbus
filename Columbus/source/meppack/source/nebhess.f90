!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
program nebhess ! written by Bernhard Sellner
! (c) Bernhard Sellner 2011
! part of MEPPACK
! This program is part of the reaction path package.
! It calculates the mass weighted hessian for the 
! relaxation of a NEB.


use prec_mod
use units_mod
use general_mod
use def_mod
use nml_mod

implicit none
real(kind=dpr) du1,du2,du3,geom(:,:,:),M(:,:),hess(:,:),conhess(:,:)
real(kind=dpr) wmr1(:,:),wsr1,wsr2,c_E(:),scons(:),carthess(:,:),Z(:,:)
integer i,j,k,l,ii,it,lines,filestat,di1,jj
integer lastim,optdim,wi1,wi2,imdim
character*1 n,t,S(:,:)
character*5 version
character*40 fname1,fname2,fname3,fl,img1(:),img2(:)
allocatable geom,M,Z,hess,conhess,S
allocatable wmr1,c_E,scons,img1,img2,carthess

! Unit ...................................... input ..................... working               
! M .......... atomic mass .................. [amu] ..................... [amu]
!
! hess ....... hessian mw ................... [aJ/(A|rad)^2] ............ [aJ/(A|rad)^2]
! scons ...... array of spring constants .... [aJ/(A^2*amu)] ............ [aJ/(A^2*amu)]
! carthess ... hessian mw ................... [aJ/(A^2)] ................ [aJ/(A^2)]
! conhess .... hessian of constraint ........ [aJ/(A^2*amu)] ............ [aJ/(A^2*amu)]
! c_E ........ array of energies ............ [a.u.] .................... [aJ]
!
! matfact .... scaling for hessian (cart) ... [aJ/(A^2)]
! matfact .... scaling for hessian (int) .... [aJ/(A|rad)^2]
!
! img1 ....... dir name "CALC.c""img1"".d""img2"
! img2 ....... dir name "CALC.c""img1"".d""img2"

version='2.3.0'
filestat=0

open(unit=1,file='nebhess.log',form='formatted',iostat=filestat)
write(1,fmt='(A)',iostat=filestat)''
if (filestat > 0) call error_sub("Problem writing nebhess.log in program nebhess.x ",2)

write(1,fmt='(A)')''
write(1,fmt='(A)')''
write(1,fmt='(A,A)')'Program started: nebhess.x version ',version
write(1,fmt='(A)')''

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Read Input
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

call def_geninp(mem,nproc,elprog,coordprog,printl,prtstep,mld,&
               &inihess,inihmod,hessmod,mixmod,hessstep,chess,&
               &consttyp,fixed_seed,iseed,restart,linconv,hdisp)

open(unit=20,file='geninp',status='old',iostat=filestat)
  if (filestat > 0) call error_sub("No geninp file for program nebhess.x ",2)
  read(20, NML= geninp)
close(20)

call def_nebpar(optdim,licneb,adjneb,initneb,doneb,maxit,nprocneb,&
               &matfact,tangtype,nebtype,startci,startdiis,maxgdiis,maxddiis,&
               &nofidiis,bmupd,stepres,scmin,scmax,conv_grad,conv_disp,cipoints)

open(unit=20,file='nebpar',status='old',iostat=filestat)
  if (filestat > 0) call error_sub("No nebpar file for program nebhess.x ",2)
  read(20, NML= nebpar)
close(20)

open(unit=9,file='curr_iter',form='formatted',status='old',iostat=filestat)
  if (filestat > 0) call error_sub("No curr_iter file for program nebhess.x ",2)
  read(9,fmt=*)it
close(9)

! inconsistent input check:
if (((coordprog.eq.'cart').and.(inihess.ne.'unitma')).or.&
   &((coordprog.eq.'cart').and.(chess.eqv..true.))) then
  call error_sub("If coordprog.eq.cart then inihess has to be unitma and chess.eqv..false !",2)
endif

allocate(img1(0:optdim+1))
allocate(img2(0:optdim+1))
allocate(geom(0:optdim+1,nat,3))
allocate(M(0:optdim+1,nat))
allocate(S(0:optdim+1,nat))
allocate(Z(0:optdim+1,nat))
allocate(hess(optdim*nintc,optdim*nintc))
allocate(wmr1(nintc,nintc))
allocate(c_E(0:optdim+1))
allocate(scons(optdim))
allocate(carthess(3*nat*optdim,3*nat*optdim))

open(unit=30,file='displfl',status='old',iostat=filestat)
if (filestat > 0) call error_sub("No displfl file for program nebhess.x ",2)
do i=1,3
  read(30,*,iostat=filestat)
enddo

do i=0,(optdim+1)
  read(30,fmt=*,iostat=filestat)img1(i),img2(i)
enddo
close(30)

do i=0,(optdim+1)
  fname2='c'//trim(adjustl(img1(i)))//'.d'//trim(adjustl(img2(i)))

  fname1='curr_geom.'//fname2
  open(unit=10,file=fname1,status='old',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("No curr_geom.* file for program nebhess.x ",2)
  do j=1,nat
    read(10,fmt=*)S(i,j),Z(i,j),geom(i,j,1),geom(i,j,2),geom(i,j,3),M(i,j)
  enddo
  close(10)

enddo

!initialize
hess=0.0_dpr

if (it.eq.0) then
  if (inihess.eq.'intcfl') then
    do ii=1,optdim
      fname2='c'//trim(adjustl(img1(ii)))//'.d'//trim(adjustl(img2(ii)))

      fname1='intcfl.'//fname2
      ! count number of lines in intcfl
      lines = 0
      filestat = 0
      open(unit=30,file=fname1,status='old',iostat=filestat)
      if (filestat > 0) call error_sub("No intcfl file for program nebhess.x ",2)
      do while (filestat == 0)
        read(30,*,iostat=filestat)
        if (filestat /= 0) exit
          lines=lines+1
        enddo
      close(30)

      open(unit=30,file=fname1,status='old',form='formatted',iostat=filestat)
      if (filestat > 0) call error_sub("No intcfl file for program nebhess.x ",2)
      k=int(nintc/8)
      if (mod(nintc,8).ne.0) then
      do i=1,(lines-k-1)
        read(30,fmt=*)
      enddo
      else
      do i=1,(lines-k)
        read(30,fmt=*)
      enddo
      endif
      ! Construct the hessian
      wmr1=0.0_dpr

      write(1,fmt='(A)')'Initial guess for hessian: intcfl'
      write(1,fmt='(A)')''
      do i=1,k
      read(30,fmt=*)(wmr1(((i-1)*8+j),((i-1)*8+j)),j=1,8)
      enddo
      if (mod(nintc,8).ne.0) then
        read(30,fmt=*)(wmr1(k*8+j,k*8+j),j=1,mod(nintc,8))
      else
      endif
      close(30)

      do i=1,nintc
        do j=1,nintc
          hess((ii-1)*nintc+i,(ii-1)*nintc+j)=wmr1(i,j)
        enddo
      enddo
    enddo

    !scale hessian
    hess=hess*matfact
  elseif ((inihess.eq.'unitma').and.(coordprog.ne.'cart')) then
    write(1,fmt='(A)')'Initial guess for hessian: unit matrix'
    write(1,fmt='(A)')''

    do i=1,optdim*nintc
      hess(i,i)=1.0_dpr
    enddo

    !scale hessian
    hess=hess*matfact
  elseif ((inihess .eq. 'unitma').and.(coordprog.eq.'cart')) then
    write(1,fmt='(A)')'Initial guess for hessian: unit matrix in cartesian coordinate space'
    write(1,fmt='(A)')''

    do i=1,optdim*3*nat
      carthess(i,i)=1.0_dpr
    enddo

    !scale hessian
    carthess=carthess*matfact
  elseif (inihess.eq.'calc') then
    do ii=1,optdim
      fname2='c'//trim(adjustl(img1(ii)))//'.d'//trim(adjustl(img2(ii)))

      fname1='hessian.'//fname2
      open(unit=43,file=fname1,status='old',form='formatted',iostat=filestat)
      if (filestat > 0) call error_sub("No hessian.* file for program nebhess.x ",2)
      wi1=nintc/8
      wi2=mod(nintc,8)
      do i=1,nintc
        do j=1,wi1
          read(43,fmt=*)(wmr1(i,(j-1)*8+k),k=1,8)
        enddo
        read(43,fmt=*)(wmr1(i,wi1*8+k),k=1,wi2)
      enddo
      close(43)

      do i=1,nintc
        do j=1,nintc
          hess((ii-1)*nintc+i,(ii-1)*nintc+j)=wmr1(i,j)
        enddo
      enddo
    enddo
    if (printl .ge. 2) then
      write(1,fmt='(A)')'Hessian read in:'
        do i=1,optdim*nintc
          do j=1,(optdim*nintc-1)
            write(1,fmt='(F10.4)',advance='no')hess(i,j)
          enddo
          write(1,fmt='(F10.4)',advance='yes')hess(i,optdim*nintc)
        enddo
      write(1,fmt='(A)')''
    endif

    !scale hessian
    hess=hess*matfact
  endif
elseif (it.ge.1) then   ! it >=1 and all other cases
  if (coordprog.ne.'cart') then
    if ((hessmod .eq. 'calc').or.(hessmod .eq. 'calcmw')) then
      if (mod(it,hessstep).eq.0) then
        if (hessmod .eq. 'calc') then
          do ii=1,optdim
            fname2='c'//trim(adjustl(img1(ii)))//'.d'//trim(adjustl(img2(ii)))

            fname1='hessian.'//fname2
            open(unit=43,file=fname1,status='old',form='formatted',iostat=filestat)
            if (filestat > 0) call error_sub("No hessian.* file for program nebhess.x ",2)
            wi1=nintc/8
            wi2=mod(nintc,8)
            do i=1,nintc
              do j=1,wi1
                read(43,fmt=*)(wmr1(i,(j-1)*8+k),k=1,8)
              enddo
              read(43,fmt=*)(wmr1(i,wi1*8+k),k=1,wi2)
            enddo
            close(43)

            do i=1,nintc
              do j=1,nintc
                hess((ii-1)*nintc+i,(ii-1)*nintc+j)=wmr1(i,j)
              enddo
            enddo
          enddo
          if (printl .ge. 2) then
            write(1,fmt='(A)')'Hessian read in:'
              do i=1,optdim*nintc
                do j=1,(optdim*nintc-1)
                  write(1,fmt='(F10.4)',advance='no')hess(i,j)
                enddo
                write(1,fmt='(F10.4)',advance='yes')hess(i,optdim*nintc)
              enddo
            write(1,fmt='(A)')''
          endif
          !scale hessian
          hess=hess*matfact
        endif
      elseif (mod(it,hessstep).ne.0) then
        if (mixmod .eq. 'update') then
          write(1,fmt='(A)')'Updating of hessian does not exist'
          call error_sub("Updating of hessian does not exist",2)
        endif
      endif
    elseif ((hessmod .eq. 'update')) then
      if ((mixmod .eq. 'update').and.(mod(it,hessstep).eq.0)) then
        write(1,fmt='(A)')'Updating of hessian does not exist'
        call error_sub("Updating of hessian does not exist",2)
      else
        write(1,fmt='(A)')'Keeping hessian constant '
        write(1,fmt='(A)')''
        open(unit=13,file='mwhessian',form='formatted',status='old',iostat=filestat)
        if (filestat > 0) call error_sub("No mwhessian file for program nebhess.x ",2)
        do i=1,optdim*nintc
          read(13,fmt=*)(hess(i,j),j=1,optdim*nintc)
        enddo
        close(13)
      endif
    endif
  elseif (coordprog.eq.'cart') then
    if ((mixmod .eq. 'update').and.(mod(it,hessstep).eq.0)) then
      write(1,fmt='(A)')'Updating of hessian does not exist'
      call error_sub("Updating of hessian does not exist",2)
    else
      write(1,fmt='(A)')'Keeping hessian constant'
      write(1,fmt='(A)')''
      open(unit=13,file='mwhessian',form='formatted',status='old',iostat=filestat)
      if (filestat > 0) call error_sub("No mwhessian file for program nebhess.x ",2)
      do i=1,optdim*3*nat
        read(13,fmt=*)(carthess(i,j),j=1,optdim*3*nat)
      enddo
      close(13)
    endif
  endif
endif

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Hessian of the constraint
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

if (chess.eqv..true.) then
  if (coordprog.eq.'cart') then
    imdim=3*nat
  elseif (coordprog.ne.'cart') then
    imdim=nintc
  endif

  allocate(conhess(optdim*imdim,optdim*imdim))

  conhess=0.0_dpr
  do i=0,(optdim+1)
    fname2='c'//trim(adjustl(img1(i)))//'.d'//trim(adjustl(img2(i)))

    fname1='curr_energy.'//fname2
    open(unit=11,file=fname1,status='old',form='formatted',iostat=filestat)
    if (filestat > 0) call error_sub("No curr_energy.* file for program nebhess.x ",2)
    read(11,fmt=*)c_E(i)
    close(11)
  enddo

  ! convert energies to aJ
  do i=0,(optdim+1)
    c_E(i)=c_E(i)*au2aj
  enddo

  call springcons(c_E,optdim,scons,scmin,scmax)

  do i=1,optdim
    do j=1,imdim
      conhess((i-1)*imdim+j,(i-1)*imdim+j)=2*scons(i)
    enddo
  enddo

  do i=2,optdim
    do j=1,imdim
      !testing
      conhess((i-2)*imdim+j,(i-1)*imdim+j)=-scons(i)
      conhess((i-1)*imdim+j,(i-2)*imdim+j)=-scons(i)
    enddo
  enddo

  if (printl .ge. 2) then
    write(1,fmt='(A)')'Hessian of constraint:'
      do i=1,imdim*optdim
        do j=1,(imdim*optdim-1)
          write(1,fmt='(F10.6)',advance='no')conhess(i,j)
        enddo
        write(1,fmt='(F10.6)',advance='yes')conhess(i,optdim*imdim)
      enddo
    write(1,fmt='(A)')''
  endif
endif

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Write output
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

if (coordprog.ne.'cart') then
  if (printl .ge. 2) then
    write(1,fmt='(A)')'Final Hessian :'
      do i=1,optdim*nintc
        do j=1,(optdim*nintc-1)
          write(1,fmt='(F10.4)',advance='no')hess(i,j)
        enddo
        write(1,fmt='(F10.4)',advance='yes')hess(i,optdim*nintc)
      enddo
    write(1,fmt='(A)')''
  endif

  open(unit=40,file='mwhessian',status='replace',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing mwhessian in program nebhess.x ",2)
  
  do i=1,optdim*nintc
    do j=1,(optdim*nintc-1)
      write(40,fmt=outfmt,advance='no')hess(i,j)
    enddo
    write(40,fmt=outfmt,advance='yes')hess(i,optdim*nintc)
  enddo

  close(40)

  if (chess.eqv..true.) then
    open(unit=40,file='mwconhess',status='replace',form='formatted',iostat=filestat)
    if (filestat > 0) call error_sub("Problem writing mwconhess in program nebhess.x ",2)
    do i=1,optdim*nintc
      do j=1,(optdim*nintc-1)
        write(40,fmt=outfmt,advance='no')conhess(i,j)
      enddo
      write(40,fmt=outfmt,advance='yes')conhess(i,optdim*nintc)
    enddo
    close(40)
  endif
elseif (coordprog.eq.'cart') then
  if (printl .ge. 2) then
    write(1,fmt='(A)')'Final Hessian :'
      do i=1,optdim*3*nat
        do j=1,(optdim*3*nat-1)
          write(1,fmt='(F10.4)',advance='no')carthess(i,j)
        enddo
        write(1,fmt='(F10.4)',advance='yes')carthess(i,optdim*3*nat)
      enddo
    write(1,fmt='(A)')''
  endif

  open(unit=40,file='mwhessian',status='replace',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing mwhessian in program nebhess.x ",2)
  do i=1,optdim*3*nat
    do j=1,(optdim*3*nat-1)
      write(40,fmt=outfmt,advance='no')carthess(i,j)
    enddo
    write(40,fmt=outfmt,advance='yes')carthess(i,optdim*3*nat)
  enddo
  close(40)

  if (chess.eqv..true.) then
    open(unit=40,file='mwconhess',status='replace',form='formatted',iostat=filestat)
    if (filestat > 0) call error_sub("Problem writing mwconhess in program nebhess.x ",2)
    do i=1,optdim*3*nat
      do j=1,(optdim*3*nat-1)
        write(40,fmt=outfmt,advance='no')conhess(i,j)
      enddo
      write(40,fmt=outfmt,advance='yes')conhess(i,optdim*3*nat)
    enddo
    close(40)
  endif
endif

deallocate(img1)
deallocate(img2)
deallocate(geom)
deallocate(M)
deallocate(S)
deallocate(Z)
deallocate(hess)
deallocate(wmr1)
deallocate(c_E)
deallocate(scons)
deallocate(carthess)

if (chess.eqv..true.) then
  deallocate(conhess)
endif

write(1,fmt='(A)')'Program finished: nebhess.x with SUCCESS'

close(1)

end program
