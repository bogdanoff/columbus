!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
program RS_PCO ! written by Bernhard Sellner
! (c) Bernhard Sellner 2011
! part of MEPPACK
! This program is part of the reaction path package.
! It performs an unconstrained optimization in the
! orthogonal complement or if necessary 
! a restricted optimization by imposing a step size. 
! Langrangian function:
! L = e(q) - lambda * r(q)
! all calculations in internal coordinates 
! units: Angstrom, radians, aJoule
! mass weighted: amu 


use prec_mod
use units_mod
use general_mod
use def_mod
use nml_mod

implicit none
real(kind=dpr) rtrust,C_angs,hess(:,:),gradC_q(:),gradE_q(:),Tcb(:,:)
real(kind=dpr) Tmti(:,:)
real(kind=dpr) dx(:),dy,dq(:),intgeom(:),Qex,lambda,redgrad(:),redhess(:,:)
real(kind=dpr) geom(:,:),M(:),Z(:),bmatrix(:,:),Gn12(:,:),Gp12(:,:)
real(kind=dpr) refintg(:),newintg(:),du1,wv1(:),conhess(:,:)
integer i,j,k,l,step,it,wi1,filestat
character*1 n,t,S(:)
character*5 version
allocatable hess,gradC_q,gradE_q,Tcb,Tmti,dx,dq,intgeom,redgrad,redhess
allocatable geom,M,Z,S,bmatrix,Gn12,Gp12,refintg,newintg,wv1,conhess
logical res_step,yesno

! Unit .................................................... input ............ working
! rtrust  ... trust radius ................................ [A*sqrt(amu)] .... [A*sqrt(amu)]
! hess    ... hessian mw .................................. [aJ/(A^2*amu)] ... [aJ/(A^2*amu)]
! gradE_q ... energy gradient in int. coord. .............. [aJ/(A|rad)] ..... [aJ/(A*sqrt(amu))] 
!
! C_angs  ... constraint .................................. [A*sqrt(amu)]
! gradC_q ... gradient of the constraint in int. coord. ... [1]
! Tcb     ... space of the constraint ..................... [1]
! Tmti    ... orthogonal comp. to Tcb ..................... [1]
! dx      ... step in Tmti ................................ [A*sqrt(amu)]
! dy      ... step in Tcb ................................. [A*sqrt(amu)]
! dq      ... total step in int. coord.  .................. [A*sqrt(amu)]
! Qex     ... change of energy predicted by program ....... [aJ] 
! lambda  ... langrangian multiplier ...................... [aJ/(A*sqrt(amu))]
! redhess ... reduced hessian ............................. [aJ/(A^2*amu)]
! redgrad ... reduced gradient ............................ [aJ/(A*sqrt(amu))]
!
! miciter ... maximal number of iterations in restriced step
! soph_l  ... n: conventional app. to lambda; y: lambda for exact solution

version='2.3.0'
filestat=0

open(unit=1,file='RS_PCO.log',form='formatted',iostat=filestat)
write(1,fmt='(A)',iostat=filestat)''
if (filestat > 0) call error_sub("Problem writing RS_PCO.log in program RS_PCO.x ",2)

write(1,fmt='(A)')''
write(1,fmt='(A)')''
write(1,fmt='(A,A)')'Program started: RS_PCO.x version ',version
write(1,fmt='(A)')''

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Read Input
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!call error_sub('test',2)

call def_geninp(mem,nproc,elprog,coordprog,printl,prtstep,mld,&
               &inihess,inihmod,hessmod,mixmod,hessstep,chess,&
               &consttyp,fixed_seed,iseed,restart,linconv,hdisp)

open(unit=20,file='geninp',status='old',iostat=filestat)
  if (filestat > 0) call error_sub("No geninp file for program RS_PCO.x ",2)
  read(20, NML= geninp)
close(20)

call def_ircpar(rpsteps,maxhypstep,inirtrust,Rhyp,&
           &conv_grad,conv_disp,conv_uwdisp,conv_cons,miciter,&
           &conv_alpha,conv_rad,soph_l,maxrtrust,minrtrust,lratio,uratio)

open(unit=20,file='ircpar',status='old',iostat=filestat)
  if (filestat > 0) call error_sub("No ircpar file for program RS_PCO.x ",2)
  read(20, NML= ircpar)
close(20)

inquire(file='rtrust',exist=yesno)

if (yesno .eqv. .true.) then
  open(unit=20,file='rtrust',form='formatted')
    read(20,fmt=*)rtrust
  close(20)
else
  rtrust=inirtrust
endif

allocate(gradC_q(nintc))
allocate(gradE_q(nintc))
allocate(hess(nintc,nintc))
allocate(Tcb(nintc,1))
allocate(Tmti(nintc,(nintc-1)))
allocate(dx(nintc-1))
allocate(dq(nintc))
allocate(intgeom(nintc))
allocate(refintg(nintc))
allocate(newintg(nintc))
allocate(redgrad(nintc-1))
allocate(redhess(nintc-1,nintc-1))
allocate(bmatrix(3*nat,nintc))
allocate(Z(nat))
allocate(geom(nat,3))
allocate(M(nat))
allocate(S(nat))
allocate(Gn12(nintc,nintc))
allocate(Gp12(nintc,nintc))
allocate(wv1(nintc))
allocate(conhess(nintc,nintc))

open(unit=9,file='curr_iter',form='formatted',status='old',iostat=filestat)
  if (filestat > 0) call error_sub("No curr_iter file for program RS_PCO.x ",2)
  read(9,fmt=*)it
close(9)

open(unit=14,file='curr_gradE_q',form='formatted',status='old',iostat=filestat)
  if (filestat > 0) call error_sub("No curr_gradE_q file for program RS_PCO.x ",2)
  do i=1,nintc
    read(14,fmt=*)gradE_q(i)
  enddo
close(14)

if (printl .ge. 2) then
  write(1,fmt='(A)')'Current internal gradient:'
  do i=1,nintc
    write(1,fmt=outfmt)gradE_q(i)
  enddo
  write(1,fmt='(A)')''
endif

open(unit=13,file='mwhessian',form='formatted',status='old',iostat=filestat)
if (filestat > 0) call error_sub("No mwhessian file for program RS_PCO.x ",2)
do i=1,nintc
  read(13,fmt=*)(hess(i,j),j=1,nintc)
enddo
close(13)

open(unit=15,file='curr_intgeom',form='formatted',status='old',iostat=filestat)
  if (filestat > 0) call error_sub("No curr_intgeom file for program RS_PCO.x ",2)
  do i=1,nintc
    read(15,fmt=*)intgeom(i)
  enddo
close(15)

if (printl .ge. 1) then
  write(1,fmt='(A)')'Current internal coordinates:'
  do i=1,nintc
    write(1,fmt=outfmt)intgeom(i)
  enddo
  write(1,fmt='(A)')''
endif

open(unit=15,file='ref_intgeom',form='formatted',status='old',iostat=filestat)
  if (filestat > 0) call error_sub("No ref_intgeom file for program RS_PCO.x ",2)
  do i=1,nintc
    read(15,fmt=*)refintg(i)
  enddo
close(15)

if (printl .ge. 2) then
  write(1,fmt='(A)')'Reference internal coordinates:'
  do i=1,nintc
    write(1,fmt=outfmt)refintg(i)
  enddo
  write(1,fmt='(A)')''
endif

open(unit=10,file='geom.start',form='formatted',status='old',iostat=filestat)
if (filestat > 0) call error_sub("No geom.start file for program RS_PCO.x ",2)
do i=1,nat
  read(10,fmt=*)S(i),Z(i),geom(i,1),geom(i,2),geom(i,3),M(i)
enddo
close(10)

open(unit=30,file='curr_bmatrix',form='formatted',status='old',iostat=filestat)
  if (filestat > 0) call error_sub("No curr_bmatrix file for program RS_PCO.x ",2)
  do j=1,nat
    do i=1,3 ! iteration over x,y,z
      wi1=nintc/3
      do l=1,wi1
        read(30,fmt=*)(bmatrix((j-1)*3+i,((l-1)*3+k)),k=1,3,1)
      enddo
    enddo
  enddo
close(30)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Prepare mass weigthed input
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

call getGmatrices(bmatrix,M,Gn12,Gp12,nintc,nat)
if (printl .ge. 2) then
  write(1,fmt='(A)')'Used gmatrix Gp12 in RS_PCO.x'
  do i=1,nintc
    do j=1,(nintc-1)
      write(1,fmt=outfmt,advance='no')Gp12(i,j)
    enddo
    write(1,fmt=outfmt,advance='yes')Gp12(i,j)
  enddo
  write(1,fmt='(A)')''
endif

call constraint(refintg,intgeom,Gn12,Gp12,C_angs,gradC_q,conhess,nintc,Rhyp,printl)

open(unit=39,file='con_hess',status='replace',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing con_hess in program RS_PCO.x ",2)
  do i=1,nintc
    do j=1,(nintc-1)
      write(39,fmt=outfmt,advance='no')conhess(i,j)
    enddo
    write(39,fmt=outfmt,advance='yes')conhess(i,nintc)
  enddo
close (40)

open(unit=40,file='curr_gradC_q',status='replace',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing curr_gradC_q in program RS_PCO.x ",2)
  do i=1,nintc
    write(40,fmt=outfmt)gradC_q(i)
  enddo
close (40)

open(unit=60,file='curr_C_angs',status='replace',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing curr_C_angs in program RS_PCO.x ",2)
  write(unit=60,fmt=outfmt)C_angs
close(60)

call mwgrad(gradE_q,Gp12,nintc)

open(unit=45,file='mw_gradE_q',status='replace',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing mw_gradE_q in program RS_PCO.x ",2)
  do i=1,nintc
    write(45,fmt=outfmt)gradE_q(i)
  enddo
close (45)

if (printl .ge. 2) then
  write(1,fmt='(A)')'Current mw internal gradient:'
  do i=1,nintc
    write(1,fmt=outfmt)gradE_q(i)
  enddo
  write(1,fmt='(A)')''
endif

if (printl .ge. 2) then
  write(1,fmt='(A)')'Hessian used in RS_PCO.x:'
    do i=1,nintc
      do j=1,(nintc-1)
        write(1,fmt=outfmt,advance='no')hess(i,j)
      enddo
      write(1,fmt=outfmt,advance='yes')hess(i,nintc)
    enddo
  write(1,fmt='(A)')''
endif

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Perform restricted step projected constraint optimization
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! build up Tcb and Tmti matrices
call builtT(Tcb,Tmti,gradC_q,nintc)

open(unit=425,file='curr_tcb',status='replace',form='formatted',iostat=filestat)
if (filestat > 0) call error_sub("Problem writing curr_tcb in program RS_PCO.x ",2)
do i=1,nintc
  write(425,fmt=outfmt)Tcb(i,1)
enddo
close(25)

open(unit=426,file='curr_tmti',status='replace',form='formatted',iostat=filestat)
if (filestat > 0) call error_sub("Problem writing curr_tmti in program RS_PCO.x ",2)
do i=1,nintc
  do j=1,(nintc-2)
    write(426,fmt=outfmt,advance='no')Tmti(i,j)
  enddo
  write(426,fmt=outfmt,advance='yes')Tmti(i,nintc-1)
enddo
close(25)

if (printl .ge. 2) then
  write(1,fmt='(A)')'Tcb matrix:'
  do i=1,nintc
    write(1,fmt=outfmt)Tcb(i,1)
  enddo
  write(1,fmt='(A)')''
  write(1,fmt='(A)')'Tmti matrix:'
  do i=1,nintc
    do j=1,(nintc-2)
      write(1,fmt=outfmt,advance='no')Tmti(i,j)
    enddo
    write(1,fmt=outfmt,advance='yes')Tmti(i,nintc-1)
  enddo
  write(1,fmt='(A)')''
endif

! calculate dy
dy=(-C_angs)/(norm(gradC_q,nintc))
if (printl.ge.2) then
  write(1,fmt='(A)',advance='no')'dy='
  write(1,fmt=outfmt)dy
  write(1,fmt='(A)')''
endif

! calculate unconstrained dx
call uconst_dx(dx,Tmti,hess,gradE_q,gradC_q,Tcb,C_angs,redhess,redgrad,nintc)
if (printl.ge.2) then
  write(1,fmt='(A)')'Unconstrained dx='
  write(1,fmt=outfmt)dx
  write(1,fmt='(A)')''
endif

if (norm(dx,nintc-1).gt.(rtrust)) then
  write(1,fmt='(A)')'Rejecting dx, performing an optimization with step restriction'
  write(1,fmt='(A)')''
  call RS_langr(redgrad,redhess,dx,rtrust,miciter,conv_alpha,conv_rad,nintc-1,printl)
  res_step=.true.
else
  write(1,fmt='(A)')'dx is inside the trust region'
  write(1,fmt='(A)')''
  res_step=.false.
endif

if (printl .ge. 2) then
  open(unit=45,file='see_dx.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_dx.dat in program RS_PCO.x ",2)
  do i=1,(nintc-1)
      write(45,fmt='(f13.6)',advance='no')dx(i)
  enddo
  write(45,fmt='(f13.6)',advance='yes')dx(nintc)
  close(45)
endif

if (printl.ge.2) then
  write(1,fmt='(A)',advance='no')'norm of dx='
  du1=norm(dx,nintc-1)
  write(1,fmt=outfmt)du1
  write(1,fmt='(A)')''
endif

! evaluate dq=Tcb*dy+Tmti*dx
! dq=Tcb*dy
dq=Tcb(:,1)*dy
!dq=Tmti*dx+dq
call DGEMV('n',nintc,(nintc-1),1.0_dpr,Tmti,nintc,dx,1,1.0_dpr,dq,1)

if (printl .ge. 1) then
  write(1,fmt='(A)')'Total step in internal coordinates:'
  do i=1,nintc
    write(1,fmt=outfmt)dq(i)
  enddo
  write(1,fmt='(A)')''
endif

!calculate new_intgeom and write out
call getsol(newintg,intgeom,dq,Gp12,nintc)

if (printl .ge. 1) then
  write(1,fmt='(A)')'New internal coordinates:'
  do i=1,nintc
    write(1,fmt=outfmt)newintg(i)
  enddo
  write(1,fmt='(A)')''
endif

call getQex(Qex,dq,gradE_q,hess,nintc)

if (printl .ge. 2) then
  write(1,fmt='(A)',advance='no')'Predicted change by algorithm Qex:'
  write(1,fmt=outfmt)Qex
  write(1,fmt='(A)')''
endif

! calculate lambda
call get_lambda(lambda,hess,dq,gradC_q,gradE_q,soph_l,nintc)

if (printl .ge. 2) then
  open(unit=149,file='see_lambda.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_lambda.dat in program RS_PCO.x ",2)
  write(149,fmt=outfmt,advance='yes')lambda
  close(149)
endif

if (printl .ge. 1) then
  write(1,fmt='(A)',advance='no')'Langrange multiplier:'
  write(1,fmt=outfmt)lambda
  write(1,fmt='(A)')''
endif

if (printl .ge. 2) then
! Debug master equations:

  !equation1
  wv1=0.0_dpr
  call DGEMV('n',nintc,nintc,1.0_dpr,hess,nintc,dq,1,0.0_dpr,wv1,1)
  wv1=wv1-gradC_q*lambda
  wv1=wv1+gradE_q

  open(unit=48,file='see_eq1.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_eq1.dat in program RS_PCO.x ",2)
  do i=1,(nintc-1)
      write(48,fmt=outfmt,advance='no')wv1(i)
  enddo
  write(48,fmt=outfmt,advance='yes')wv1(nintc)
  close(48)

  ! equation2
  du1=0.0_dpr

  do i=1,nintc
    du1=du1+gradC_q(i)*dq(i)
  enddo
  du1=du1+C_angs

  open(unit=49,file='see_eq2.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_eq2.dat in program RS_PCO.x ",2)
  write(49,fmt=outfmt,advance='yes')du1
  close(49)
endif

! check convergence
call check_conv(Tmti,gradE_q,dq,C_angs,conv_grad,conv_disp,conv_cons,conv_uwdisp,M,Gp12,nintc,nat)

! calculate new constraint
call constraint(refintg,newintg,Gn12,Gp12,C_angs,gradC_q,conhess,nintc,Rhyp,printl)

open(unit=40,file='new_gradC_q',status='replace',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing new_gradC_q in program RS_PCO.x ",2)
  do i=1,nintc
    write(40,fmt=outfmt)gradC_q(i)
  enddo
close (40)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Write output
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


open(unit=60,file='new_C_angs',status='replace',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing new_C_angs in program RS_PCO.x ",2)
  write(unit=60,fmt=outfmt)C_angs
close(60)

! write out: dq, Qex(dx resp dq), Tcb, Tmti, lambda
open(unit=25,file='new_intgeom',status='replace',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing new_intgeom in program RS_PCO.x ",2)
  do i=1,nintc
    write(25,fmt=outfmt)newintg(i)
  enddo
close(25)

open(unit=26,file='delta_q',status='replace',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing delta_q in program RS_PCO.x ",2)
  do i=1,nintc
    write(26,fmt=outfmt)dq(i)
  enddo
close(26)

open(unit=36,file='delta_x',status='replace',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing delta_x in program RS_PCO.x ",2)
  do i=1,nintc-1
    write(36,fmt=outfmt)dx(i)
  enddo
close(36)

open(unit=236,file='delta_y',status='replace',form='formatted',iostat=filestat)
if (filestat > 0) call error_sub("Problem writing delta_y in program RS_PCO.x ",2)
write(236,fmt=outfmt)dy
close(236)

open(unit=27,file='Qex',status='replace',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing Qex in program RS_PCO.x ",2)
  write(27,fmt=outfmt)Qex
close(27)

if (printl .ge. 2) then
  open(unit=27,file='see_Qex',status='unknown',position='append',form='formatted',iostat=filestat)
    if (filestat > 0) call error_sub("Problem writing see_Qex in program RS_PCO.x ",2)
    write(27,fmt=outfmt)Qex
  close(27)
endif

open(unit=28,file='lambda',status='replace',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing lambda in program RS_PCO.x ",2)
  write(28,fmt=outfmt)lambda
close(28)

open(unit=29,file='Tmti',status='replace',form='formatted',iostat=filestat)
if (filestat > 0) call error_sub("Problem writing Tmti in program RS_PCO.x ",2)
do i=1,nintc
  do j=1,(nintc-2)
    write(29,fmt=outfmt,advance='no')Tmti(i,j)
  enddo
  write(29,fmt=outfmt,advance='yes')Tmti(i,nintc-1)
enddo
close(13)

deallocate(gradC_q)
deallocate(gradE_q)
deallocate(hess)
deallocate(Tcb)
deallocate(Tmti)
deallocate(dx)
deallocate(dq)
deallocate(intgeom)
deallocate(refintg)
deallocate(newintg)
deallocate(redgrad)
deallocate(redhess)
deallocate(bmatrix)
deallocate(Z)
deallocate(geom)
deallocate(M)
deallocate(S)
deallocate(Gn12)
deallocate(Gp12)
deallocate(wv1)
deallocate(conhess)

write(1,fmt='(A)')'Program finished: RS_PCO.x with SUCCESS'

close(1)

contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Subroutines
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

subroutine getsol(newintg,currintg,dq,Gp12,dimn)
! calculates the soulution out of the mass weighted 
! solution.
! dq=G^(1/2)T*dq

! input:
! Gp12 ....... =G^(+1/2) .......... [(A|rad/(sqrt(amu)*A)]
! dq ......... displacement ....... [sqrt(amu)*A]
! currintg ... current int.geom ... [A|rad]

! output:
! newintg .... new int.geom ....... [A|rad]

use prec_mod
use units_mod
use general_mod

implicit none
integer dimn
real(kind=dpr) wv1(dimn),Gp12(dimn,dimn),dq(dimn)
real(kind=dpr) currintg(dimn),newintg(dimn),ws1 
integer i,j,k

wv1=0.0_dpr
call DGEMV('t',dimn,dimn,1.0_dpr,Gp12,dimn,dq,1,0.0_dpr,wv1,1)

if (printl .ge. 2) then
  open(unit=45,file='see_displ.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_displ.dat in sub getsol ",2)
  do i=1,(nintc-1)
    write(45,fmt='(f13.6)',advance='no')wv1(i)
  enddo
  write(45,fmt='(f13.6)',advance='yes')wv1(dimn)
  close(45)
endif

newintg=currintg+wv1

end subroutine

subroutine check_conv(Tmti,gradE_q,dq,C_angs,conv_grad,conv_disp,conv_cons,conv_uwdisp,M,Gp12,dimn,numat)
! checks the convergenc of the optimization on the 
! hypersphere according to 4 conditions:
!
! norm of energy gradient in the Tmti subspace
! norm of displacement in mw interal coordinates
! absolut value of constraint
! maximal displacement in interal coordinates in A|rad

! input
! gradE_q ....... mass weigthed gradient ..................... [aJ/(A*sqrt(amu))]
! dq ............ step in internal coordinates ............... [A*sqrt(amu)]
! Gp12 .......... =G^(+1/2) .................................. [(A|rad/(sqrt(amu)*A)]
! Tmti .......... orthogonal complement to constraint ........ [1]
! C_angs ........ constraint ................................. [A*sqrt(amu)]
! M ............. masses ..................................... [amu]
! conv_grad ..... threshold for norm of projected gradient ... [aJ/(A*sqrt(amu))]
! conv_disp ..... threshold for norm of displacement ......... [A*sqrt(amu)]
! conv_cons ..... threshold for constraint ................... [A*sqrt(amu)]
! conv_uwdisp ... threshold for unweighted displacement ...... [A|rad]

use prec_mod
use units_mod
use general_mod

implicit none
integer dimn,numat
real(kind=dpr) Tmti(dimn,dimn-1),gradE_q(dimn),dq(dimn),C_angs
real(kind=dpr) wv1(dimn-1),curr_grad,curr_disp,curr_cons,Gp12(dimn,dimn)
real(kind=dpr) conv_grad,conv_disp,conv_cons,M(numat),mtot,wv2(dimn),curr_uwdisp
real(kind=dpr) conv_uwdisp
integer i,j,k
character*1 n,t,co

mtot=0.0_dpr
do i=1,numat
 mtot=mtot+M(i)
enddo

call DGEMV('t',dimn,dimn,1.0_dpr,Gp12,dimn,dq,1,0.0_dpr,wv2,1)
! largest displacement
curr_uwdisp=maxval(wv2)

!  wv1= (Tmti)t*gradE_q
call DGEMV('t',dimn,(dimn-1),1.0_dpr,Tmti,dimn,gradE_q,1,0.0_dpr,wv1,1)

curr_grad=norm(wv1,dimn-1)/dsqrt(dble(dimn-1))
curr_disp=norm(dq,dimn)/dsqrt(dble(dimn))
curr_cons=dabs(C_angs)

if ((curr_uwdisp.lt.conv_uwdisp).and.(curr_grad.lt.conv_grad).and.&
  &(curr_disp.lt.conv_disp).and.(curr_cons.lt.conv_cons)) then
  co='T'
else
  co='F'
endif

open(unit=47,file='convergence',status='replace',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing convergence in sub check_conv ",2)
  write(47,fmt='(A)')co
close(47)

if ((printl .ge. 1).or.(co .eq. 'T')) then
  write(1,fmt='(A)')'Convergence '
  write(1,fmt='(A)')'                                            Currently              Threshold&
                     &             Converged'
  write(1,fmt='(A)',advance='no')'||T*grad||/sqrt(N-1) [aJ/(A*sqrt(amu))] = '
  write(1,fmt=outfmt,advance='no')curr_grad
  write(1,fmt=outfmt,advance='no')conv_grad
  if (curr_grad.lt.conv_grad) then
    write(1,fmt='(A)',advance='yes')'       YES '
  else
    write(1,fmt='(A)',advance='yes')'        NO '
  endif
  write(1,fmt='(A)',advance='no')'||dqM||/sqrt(N) ......... [A*sqrt(amu)] = '
  write(1,fmt=outfmt,advance='no')curr_disp
  write(1,fmt=outfmt,advance='no')conv_disp
  if (curr_disp.lt.conv_disp) then
    write(1,fmt='(A)',advance='yes')'       YES '
  else
    write(1,fmt='(A)',advance='yes')'        NO '
  endif
  write(1,fmt='(A)',advance='no')'max(dq) ....................... [A|rad] = '
  write(1,fmt=outfmt,advance='no')curr_uwdisp
  write(1,fmt=outfmt,advance='no')conv_uwdisp
  if (curr_uwdisp.lt.conv_uwdisp) then
    write(1,fmt='(A)',advance='yes')'       YES '
  else
    write(1,fmt='(A)',advance='yes')'        NO '
  endif
  write(1,fmt='(A)',advance='no')'abs(constraint) ......... [A*sqrt(amu)] = '
  write(1,fmt=outfmt,advance='no')curr_cons
  write(1,fmt=outfmt,advance='no')conv_cons
  if (curr_cons.lt.conv_cons) then
    write(1,fmt='(A)',advance='yes')'       YES '
  else
    write(1,fmt='(A)',advance='yes')'        NO '
  endif
  write(1,fmt='(A)')''
endif

if (printl .ge. 1) then
  open(unit=27,file='see_conv.dat',status='unknown',position='append',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("Problem writing see_conv.dat in sub check_conv ",2)
  write(27,fmt='(4(F14.8,X))')curr_grad,curr_disp,curr_uwdisp,curr_cons
  close(27)
endif

end subroutine

subroutine getQex(Qex,dq,gradE_q,hess,dimn)
! calculates the energy change predicted by the algorithm

!input
! dq ........ step in internal coordinates ............... [sqrt(amu)*A]
! gradE_q ... mass weigthed gradient ..................... [aJ/(A*sqrt(amu))]
! hess ...... hessian mw ................................. [aJ/(A^2*amu)]

!output
! Qex ....... Energy change predicted by the algorithm ... [aJ]  

use prec_mod
use units_mod
use general_mod

implicit none
integer dimn
real(kind=dpr) dq(dimn),gradE_q(dimn),hess(dimn,dimn),Qex
real(kind=dpr) workv1(dimn)

Qex=0.0_dpr

Qex=sp(dq,gradE_q,dimn)

! workv1 = hess * dq
call DGEMV('n',dimn,dimn,1.0_dpr,hess,dimn,dq,1,0.0_dpr,workv1,1)

Qex=Qex+0.5_dpr*sp(dq,workv1,dimn) 

end subroutine

end program
