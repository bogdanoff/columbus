#! /usr/bin/perl
#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/

use strict;
use warnings;
use Shell qw(pwd cd cp mkdir date mv cat);
use lib $ENV{"MEP"};
use defaults qw(test writerestart getkeyw definit prepdef max);

my ($wdir);

# (c) Bernhard Sellner 2011
# part of MEPPACK

# This is the analyze script for the MEP program package.
# Based on a file named a_intcfl, internal coordinates 
# are evaluated

print ("\n\nAnalysis script for MEPPACK version 2.3\n\n");

$wdir=pwd;
chomp($wdir);

if ((-e "$wdir/ircpar") and (-e "$wdir/nebpar")) {
  die "Please delete one of the files:\n\nnebpar\nircpar\n\n";
}elsif (-e "$wdir/ircpar") {
  system ("rm -rf ANALYSIS");
  irc_analysis();
}elsif (-e "$wdir/nebpar") {
  system ("rm -rf ANALYSIS");
  neb_analysis();
} else {
  die ("\n\nCannot find ircpar resp. nebpar\n\n");
}

print ("\n\nAnalysis finished\n\n");

sub distance_analysis {
  my (@crd,@images,$lines,$optdim);
  my ($i,$meppath,@dists,@updist,@downdist);
  my ($trash);

  $meppath=$ENV{"MEP"};
  
  chdir ("$wdir/temp");

  system ("rm -rf $wdir/temp/*");

  system("cp -r $wdir/results/DISPLACEMENT/ $wdir/temp");

  $lines=0;
  open (DFL,"$wdir/temp/DISPLACEMENT/displfl") or die "Cannot open $wdir/temp/DISPLACEMENT/displfl";
  while (<DFL>) {
    $lines++;
  }
  close (DFL);
  open (DFL,"$wdir/temp/DISPLACEMENT/displfl") or die "Cannot open $wdir/temp/DISPLACEMENT/displfl";
  chomp($_=<DFL>);
  chomp($_=<DFL>);
  chomp($_=<DFL>);
  for ($i=0;$i<=$lines-4;$i++) {
    chomp($_=<DFL>);
    ($crd[$i],$images[$i])=split(/\s+/,$_);
  }
  $optdim=$lines-5;
  close(DFL);

  cp ("$wdir/geninp", "$wdir/temp");
  for ($i=1;$i<=$optdim+1;$i++) {
    cp ("$wdir/temp/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]/geom","$wdir/temp/geom");
    cp ("$wdir/temp/DISPLACEMENT/CALC.c$crd[$i-1].d$images[$i-1]/geom","$wdir/temp/ref_geom");
    system("$meppath/orientgeom.x");
    system("grep \'MW Distance\' $wdir/temp/orientgeom.log >>mwdist.temp");
    cp ("$wdir/temp/new_geom", "$wdir/temp/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]/geom");
    system("rm -f geom new_geom ref_geom orientgeom*");
  }

  open (MWDT,"<$wdir/temp/mwdist.temp") or die "Cannot read $wdir/temp/mwdist.temp\n";

  for ($i=1;$i<=$optdim+1;$i++) {
    $_=<MWDT>;
    ($trash,$dists[$i])=split(/=\s+/,$_);
  }
  close (MWDT);

  $updist[0]=0;
  $downdist[$optdim+1]=0;

  for ($i=1;$i<=($optdim+1);$i++) {
    $updist[$i]=$updist[$i-1]+$dists[$i];
  }
  for ($i=$optdim;$i>=0;$i--) {
    $downdist[$i]=$downdist[$i+1]+$dists[$i+1];
  }

  open (DD,">$wdir/ANALYSIS/mwdist.dat") or die "Cannot write $wdir/ANALYSIS/mwdist.dat\n";
    for ($i=0;$i<=($optdim+1);$i++) {
      printf DD ("%14.8f", $updist[$i]);
      printf DD ("%14.8f", $downdist[$i]);
      print DD ("\n");
    }
  close(DD);

  system("rm -rf $wdir/temp/*");

  chdir ("$wdir");

}


sub irc_analysis {

my ($wdir,$lines,$i,$nmode,$trash,$nintc,$nat,$j);
my ($rpsteps,@cinf,$work1,$work2);
my ($optdim,$mcscf,$ci,$mcstates,$cistates,@mcenergy,@cienergy);
my (@crd,@images,@mce_ev,@mce_kcal,$elprog);

$wdir=pwd;
chomp($wdir);


$nintc=&defaults::getkeyw('geninp','nintc',$nintc,$wdir);
$rpsteps=&defaults::getkeyw('geninp','nintc',$nintc,$wdir);
$elprog=&defaults::getkeyw('geninp','elprog',$elprog,$wdir);

open(CST,"$wdir/curr_step") or die "Cannot open curr_step\n";
chomp($_=<CST>);
$rpsteps=$_;
close(CST);
$rpsteps=$rpsteps-1;

#read dispfl
$lines=0;
open (DFL,"$wdir/results/DISPLACEMENT/displfl") or die "Cannot open $wdir/DISPLACEMENT/displfl";
while (<DFL>) {
  $lines++;
}
close (DFL);
open (DFL,"$wdir/results/DISPLACEMENT/displfl") or die "Cannot open $wdir/DISPLACEMENT/displfl";
chomp($_=<DFL>);
chomp($_=<DFL>);
chomp($_=<DFL>);
for ($i=0;$i<=$lines-4;$i++) {
  chomp($_=<DFL>);
  ($crd[$i],$images[$i])=split(/\s+/,$_);
}
$optdim=$lines-5;
close(DFL);
#count lines

mkdir "$wdir/ANALYSIS";

# get global convergence information

open(LOG,"<irc.log") or die "Cannot read irc.log\n";
$i=1;
while ($_=<LOG>) {
  if (/converged within/){
    chomp($_);
    ($work1,$work2)=split(/converged within\s+/,$_);
    ($cinf[$i],$trash)=split(/\s+/,$work2);
    $i++;
  }
}
close(LOG);

open(GC,">$wdir/ANALYSIS/global_conv.dat") or die "Cannot write global_conv.dat\n";
print GC ("Step     Iterations\n");
for ($i=1;$i<=$rpsteps;$i++) {
  printf GC ("%4u", $i);
  printf GC ("%15u", $cinf[$i]);
  print GC ("\n");
}
close(GC);

open(DIS,"$wdir/results/DISPLACEMENT/displfl") or die "cannot open $wdir/results/DISPLACEMENT/displfl\n";
$lines=-3;
while ($_=<DIS>){
  $lines=$lines+1;
  if ($lines == 2) {
    chomp ($_);
    ($nmode,$trash)=split(/   /,$_);
  }
}
close(DIS);

#do transformation with hand writen intcfl

chdir "$wdir/temp";

system("rm -rf $wdir/temp/*");

cp ("$wdir/a_intcfl", "$wdir/temp/intcfl");

# create inputfile for cart -> int
open(C2I,">$wdir/temp/cart2intin") or die "Cannot write to car2intin ";
  print C2I " &input\n";
  print C2I " calctype='cart2int',\n";
  print C2I " /&end \n";
close(C2I);

open (RES,">$wdir/ANALYSIS/intc.dat") or die "cannot open $wdir/ANALYSIS/inct\n";

for ($i=0;$i<=($lines-1);$i++) {
  cp ("$wdir/results/DISPLACEMENT/CALC.c$nmode.d$i/geom", "$wdir/temp");
  cp ("$wdir/results/DISPLACEMENT/CALC.c$nmode.d$i/cartgrd", "$wdir/temp");
  # transform cart ->int
  system('$COLUMBUS/cart2int.x');
  system('rm -f cart2intls bummer bmatrix geom cartgrd');
  open (IG,"$wdir/temp/intgeom") or die "cannot open $wdir/temp/intgeom\n";

  for ($j=1;$j<=($nintc-1);$j++) {
    chomp($_=<IG>);
    print RES ("$_");
  }
  chomp($_=<IG>);
  print RES ("$_\n");
  close (IG);
}

close (RES);

getenergy("irc");

chdir ("$wdir");

distance_analysis();

chdir ("$wdir");

} # sub irc_analysis

sub neb_analysis {

my ($wdir,$lines,$i,$nmode,$trash,$nintc,$nat,$j);
my ($rpsteps,@cinf,$work1,$work2,@crd,@images);
my ($optdim,$mcscf,$ci,$mcstates,$cistates,@mcenergy,@cienergy,@dists,@updist,@downdist);
my (@mce_ev,@mce_kcal,@ci_ev,@ci_kcal);

$wdir=pwd;
chomp($wdir);

$nintc=&defaults::getkeyw('geninp','nintc',$nintc,$wdir);
$nat=&defaults::getkeyw('geninp','nat',$nat,$wdir);

mkdir "$wdir/ANALYSIS";

# read first and last point from dispfl
$lines=0;
open (DFL,"$wdir/results/DISPLACEMENT/displfl") or die "Cannot open $wdir/DISPLACEMENT/displfl";
while (<DFL>) {
  $lines++;
}
close (DFL);
open (DFL,"$wdir/results/DISPLACEMENT/displfl") or die "Cannot open $wdir/DISPLACEMENT/displfl";
chomp($_=<DFL>);
chomp($_=<DFL>);
chomp($_=<DFL>);
for ($i=0;$i<=$lines-4;$i++) {
  chomp($_=<DFL>);
  ($crd[$i],$images[$i])=split(/\s+/,$_);
}
$optdim=$lines-5;
close(DFL);

#do transformation with hand writen intcfl


chdir "$wdir/temp";

system("rm -rf $wdir/temp/*");

cp ("$wdir/a_intcfl", "$wdir/temp/intcfl");

# create inputfile for cart -> int
open(C2I,">$wdir/temp/cart2intin") or die "Cannot write to car2intin ";
  print C2I " &input\n";
  print C2I " calctype='cart2int',\n";
  print C2I " /&end \n";
close(C2I);

open (RES,">$wdir/ANALYSIS/intc.dat") or die "cannot open $wdir/ANALYSIS/inct\n";

for ($i=0;$i<=($optdim+1);$i++) {
  cp ("$wdir/results/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]/geom", "$wdir/temp");
        open(CG,">$wdir/temp/cartgrd") or die "Cannot write cartgrd";
        for ($j=1;$j<=$nat;$j++) {
        print CG ("   0.0            0.0            0.0\n");
        }

  # transform cart ->int
  system('$COLUMBUS/cart2int.x');
  system('rm -f cart2intls bummer bmatrix geom cartgrd');
  open (IG,"$wdir/temp/intgeom") or die "cannot open $wdir/temp/intgeom\n";

  for ($j=1;$j<=($nintc-1);$j++) {
    chomp($_=<IG>);
    print RES ("$_");
  }
  chomp($_=<IG>);
  print RES ("$_\n");
  close (IG);
}

close (RES);

getenergy("neb");

chdir ("$wdir");

distance_analysis();

&defaults::angles($wdir);

} # sub neb_analysis

sub getenergy {
  my ($type)=@_; 
  my ($mcscf,$mcstates,$ci,$cistates,@mcenergy,@cienergy);
  my ($i,$j,$trash,$optdim,$work1,@mce_ev,@mce_kcal,@crd,@images); 
  my ($lines,$inppath,$mem,$meppath,@genp,$coltype);
  my (@ci_ev,@ci_kcal);

  $meppath=$ENV{"MEP"};

  $genp[0][0]=("mem");

  &defaults::prepdef($meppath,$wdir,$type);
  &defaults::definit($wdir,@genp);
  for($i=0;$i<@genp;$i++){
    $genp[$i][1]=&defaults::getkeyw('geninp',$genp[$i][0],$genp[$i][1],$wdir);
  }
  $mem=$genp[0][1];

  if ($mem lt 200000){
    $coltype=7;
  } else {
    $coltype=5;
  }

system("rm -f $wdir/def");
$lines=0;
open (DFL,"$wdir/results/DISPLACEMENT/displfl") or die "Cannot open $wdir/DISPLACEMENT/displfl";
while (<DFL>) {
  $lines++;
}
close (DFL);
open (DFL,"$wdir/results/DISPLACEMENT/displfl") or die "Cannot open $wdir/DISPLACEMENT/displfl";
chomp($_=<DFL>);
chomp($_=<DFL>);
chomp($_=<DFL>);
for ($i=0;$i<=$lines-4;$i++) {
  chomp($_=<DFL>);
  ($crd[$i],$images[$i])=split(/\s+/,$_);
}
$optdim=$lines-5;
close(DFL);

  if ($type eq "neb"){
    $inppath="results/DISPLACEMENT/CALC.c$crd[1].d$images[1]";
  } elsif ($type eq "irc") {
    $inppath="gradjob";
  }

# get energies
$mcscf=0;
$mcstates=1;
$ci=0;
$cistates=0;

open (CT,"<$wdir/$inppath/control.run") or die "Cannot read control.run\n";
while ($_=<CT>) {
  if (/mcscf/) {
    $mcscf=1;
  }
  if (/ciudg/) {
    $ci=1;
  }
}
close(CT);

if ($mcscf == 1) {
  open (MC,"<$wdir/$inppath/mcscfin") or die "Cannot read mcscfin\n";
  while ($_=<MC>) {
    if (/NAVST/){
      ($trash,$mcstates)=split(/=/,$_);
      chomp($mcstates);
      chop($mcstates);
    }
  }
  close(MC);

  for ($i=0;$i<=($optdim+1);$i++) {
    open(MC,"<$wdir/results/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]/LISTINGS/mcscfsm.all") or die "Cannot read mcscfsm.all\n";
    while ($_=<MC>) {
      if (/Individual total energies for all states/) {
        for ($j=1;$j<=$mcstates;$j++) {
          $_=<MC>;
          if ($coltype ==5) {
            ($trash,$mcenergy[$i][$j])=split(/=/,$_);
            chomp($mcenergy[$i][$j]);
          } elsif ($coltype ==7) {
            ($trash,$mcenergy[$i][$j])=split(/=/,$_);
            ($mcenergy[$i][$j],$trash)=split(/,/,$mcenergy[$i][$j]);
            chomp($mcenergy[$i][$j]);
          }
        }
      }
    }
  }
  close(MC);

  open (MCE,">$wdir/ANALYSIS/energy_mcscf.dat") or die "Cannot write $wdir/ANALYSIS/energy_mcscf.dat\n";
  for ($i=0;$i<=($optdim+1);$i++) {
    for ($j=1;$j<=$mcstates;$j++) {
      print MCE ("$mcenergy[$i][$j]  ");
    }
    print MCE ("\n");
  }
  close(MCE);

  $work1=&defaults::max(@mcenergy);

  for ($i=0;$i<=($optdim+1);$i++) {
    for ($j=1;$j<=$mcstates;$j++) {
      $mce_ev[$i][$j]=($mcenergy[$i][$j]-$work1)*27.2114;
      $mce_kcal[$i][$j]=($mcenergy[$i][$j]-$work1)*627.510;
    }
  } 

  open (MCE,">$wdir/ANALYSIS/energy_mcscf_eV.dat") or die "Cannot write $wdir/ANALYSIS/energy_mcscf_eV.dat\n";
  for ($i=0;$i<=($optdim+1);$i++) {
    for ($j=1;$j<=$mcstates;$j++) {
      printf MCE ("%14.8f", $mce_ev[$i][$j]);
    }
    print MCE ("\n");
  }
  close(MCE);

  open (MCE,">$wdir/ANALYSIS/energy_mcscf_kcal.dat") or die "Cannot write $wdir/ANALYSIS/energy_mcscf_kcal.dat\n";
  for ($i=0;$i<=($optdim+1);$i++) {
    for ($j=1;$j<=$mcstates;$j++) {
      printf MCE ("%14.8f", $mce_kcal[$i][$j]);
    }
    print MCE ("\n");
  }
  close(MCE);

}

if ($ci == 1) {

  open (MC,"<$wdir/$inppath/ciudgin") or die "Cannot read ciudgin\n";
  while ($_=<MC>) {
    if (/NROOT/){
      ($trash,$cistates)=split(/=/,$_);
    }
  }
  close(MC);

  for ($i=0;$i<=($optdim+1);$i++) {
    system("grep 'eci       =' $wdir/results/DISPLACEMENT/CALC.c$crd[$i].d$images[$i]/LISTINGS/ciudgsm.all >$wdir/temp/energy");
    open(EE,"<$wdir/temp/energy") or die "Cannot read $wdir/temp/energy";
    for ($j=1;$j<=$cistates;$j++) {
      $_=<EE>;
      ($trash,$cienergy[$i][$j])=split(/eci       =/,$_);
      ($cienergy[$i][$j],$trash)=split(/deltae/,$cienergy[$i][$j]);
    }
    close(EE);
    system("rm -f $wdir/temp/energy");
  }

  open (CIE,">$wdir/ANALYSIS/energy_ci.dat") or die "Cannot write $wdir/ANALYSIS/energy_ci.dat\n";
    for ($i=0;$i<=($optdim+1);$i++) {
      for ($j=1;$j<=$cistates;$j++) {
        print CIE ("$cienergy[$i][$j]  ");
      }
      print CIE ("\n");
    }
  close(CIE);

  $work1=&defaults::max(@cienergy);

  for ($i=0;$i<=($optdim+1);$i++) {
    for ($j=1;$j<=$cistates;$j++) {
      $ci_ev[$i][$j]=($cienergy[$i][$j]-$work1)*27.2114;
      $ci_kcal[$i][$j]=($cienergy[$i][$j]-$work1)*627.510;
    }
  } 

  open (MCE,">$wdir/ANALYSIS/energy_ci_eV.dat") or die "Cannot write $wdir/ANALYSIS/energy_ci_eV.dat\n";
  for ($i=0;$i<=($optdim+1);$i++) {
    for ($j=1;$j<=$cistates;$j++) {
      printf MCE ("%14.8f", $ci_ev[$i][$j]);
    }
    print MCE ("\n");
  }
  close(MCE);

  open (MCE,">$wdir/ANALYSIS/energy_ci_kcal.dat") or die "Cannot write $wdir/ANALYSIS/energy_ci_kcal.dat\n";
  for ($i=0;$i<=($optdim+1);$i++) {
    for ($j=1;$j<=$cistates;$j++) {
      printf MCE ("%14.8f", $ci_kcal[$i][$j]);
    }
    print MCE ("\n");
  }
  close(MCE);

}

}
