!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
program guessgeom 
! (c) Bernhard Sellner 2011
! part of MEPPACK
! This program is part of the reaction path package.
! The purpose is to make an initial guess for a geometry on the hypershpere
! and to provide the reference geometry .

use prec_mod
use units_mod
use general_mod
use def_mod
use nml_mod

implicit none
real(kind=dpr) geom(:,:),Z(:),M(:),du1,du2,grad(:,:),guess(:,:)
real(kind=dpr) geom2(:,:),Z2(:),M2(:),x1,x2,e1,e2,freq(:)
real(kind=dpr) lq(:,:,:),refgeom(:,:),disp(:,:)
real(kind=dpr) wv1(:),wv2(:)
integer i,j,k,l,step,seed(:),n,df,filestat
integer ii,jj,wi1,wi2
character version_number,S(:),S2(:),trash1,trash2
character*5 version
character*31 wc1,wc2
allocatable geom,S,Z,M,geom2,S2,Z2,M2,seed,freq,lq,refgeom,grad,disp
allocatable guess,wv1,wv2

! Unit ............................................. input .............. working               
! geom ...... starting point ....................... [a.u.] ............. [a.u.]
! guess ..... Initial guess on the hypersphere ..... [a.u.] ............. [a.u.]
! geom2 ..... previous geometry used for extrap. ... [a.u.] ............. [a.u.]
! grad ...... Cartesian gradient ................... [a.u.] ............. [a.u.]
! refgeom ... Center of Hypersphere ................ [a.u.] ............. [a.u.]
! lq ........ mw normal modes ...................... [1] ................ [1]
! freq ...... frequencies .......................... [cm^-1]
! step ...... current step in the MEP
! Z ......... charge of nucleus .................... [e]
! M ......... atomic mass .......................... [amu] .............. [amu]
! S ......... atomic symbols
! Rhyp ...... Radius on the hypershpere ............ [a.u.*sqrt(amu)] ... [a.u.*sqrt(amu)]

version='2.3.0'
filestat=0

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Read Input
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

open(unit=1,file='guessgeom.log',form='formatted',iostat=filestat)
if (filestat > 0) call error_sub("Problem writing guessgeom.log in program check_Rtrust.x ",2)

write(1,fmt='(A)')''
write(1,fmt='(A)')''
write(1,fmt='(A)')''
write(1,fmt='(A,A)')'Program started: guessgeom.x version ',version
write(1,fmt='(A)')''
write(1,fmt='(A)')''

call def_geninp(mem,nproc,elprog,coordprog,printl,prtstep,mld,&
               &inihess,inihmod,hessmod,mixmod,hessstep,chess,&
               &consttyp,fixed_seed,iseed,restart,linconv,hdisp)
open(unit=20,file='geninp',status='old',iostat=filestat)
  if (filestat > 0) call error_sub("No geninp file for program guessgeom.x ",2)
  read(20, NML= geninp)
close(20)

call def_ircpar(rpsteps,maxhypstep,inirtrust,Rhyp,&
           &conv_grad,conv_disp,conv_uwdisp,conv_cons,miciter,&
           &conv_alpha,conv_rad,soph_l,maxrtrust,minrtrust,lratio,uratio)

open(unit=20,file='ircpar',status='old',iostat=filestat)
  if (filestat > 0) call error_sub("No ircpar file for program guessgeom.x ",2)
  read(20, NML= ircpar)
close(20)

df=3*nat

allocate(Z(nat))
allocate(geom(nat,3))
allocate(guess(nat,3))
allocate(M(nat))
allocate(S(nat))
allocate(Z2(nat))
allocate(geom2(nat,3))
allocate(M2(nat))
allocate(S2(nat))
allocate(freq(df))
allocate(lq(df,nat,3))
allocate(refgeom(nat,3))
allocate(wv1(nintc))
allocate(wv2(nintc))
allocate(disp(nat,3))

open(unit=9,file='curr_step',form='formatted',status='old',iostat=filestat)
  if (filestat > 0) call error_sub("No curr_step file for program guessgeom.x ",2)
  read(9,fmt=*)step
close(9)

open(unit=10,file='geom',form='formatted',status='old',iostat=filestat)
if (filestat > 0) call error_sub("No geom file for program guessgeom.x ",2)
do i=1,nat
  read(10,fmt=*)S(i),Z(i),geom(i,1),geom(i,2),geom(i,3),M(i)
enddo
close(10)
  
if (step.gt.1) then
  open(unit=10,file='geom_prev',form='formatted',status='old',iostat=filestat)
  if (filestat > 0) call error_sub("No geom_prev file for program guessgeom.x ",2)
  do i=1,nat
    read(10,fmt=*)S2(i),Z2(i),geom2(i,1),geom2(i,2),geom2(i,3),M2(i)
  enddo
  close(10)
endif

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Make an initial guess
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

if (step.gt.1) then

  open(unit=35,file='cartgrd',form='formatted',status='old',iostat=filestat)
  if (filestat > 0) call error_sub("No cartgrd file for program guessgeom.x ",2)
  allocate(grad(nat,3))
  do i=1,nat
    read(unit=35,fmt=*)(grad(i,j),j=1,3,1)
  enddo
  close(35)

  write(1,fmt='(A)',advance='no')'Reference geometry for step'
  write(1,fmt='(I5)',advance='yes')step
  write(1,fmt='(A)')''
  write(1,fmt='(A)')'Using the gradient'
  write(1,fmt='(A)')' '

  if (printl.ge.1) then
    write(1,fmt='(A)')'Cartesian gradient'
    do i=1,nat
      do j=1,2
        write(1,fmt=outfmt,advance='no')grad(i,j)
      enddo
      write(1,fmt=outfmt,advance='yes')grad(i,3)
    enddo
    write(1,fmt='(A)')''
  endif

  if (consttyp .eq. 1) then
    do i=1,nat
      ! geometries: from [a.u.] to [sqrt(amu)*a.u.]
      geom(i,:)=geom(i,:)*dsqrt(M(i))
      geom2(i,:)=geom2(i,:)*dsqrt(M(i))
      grad(i,:)=-grad(i,:)/dsqrt(M(i))
    enddo
  endif

  if (printl.ge.1) then
    write(1,fmt='(A)')'Mass weighted cartesian gradient'
    do i=1,nat
      do j=1,2
        write(1,fmt=outfmt,advance='no')grad(i,j)
      enddo
      write(1,fmt=outfmt,advance='yes')grad(i,3)
    enddo
    write(1,fmt='(A)')''
  endif

  grad=grad/col_norm(grad,nat)
  refgeom=geom+(Rhyp/2.0_dpr)*grad
  guess=geom+Rhyp*grad

  if (consttyp .eq. 1) then
    do i=1,nat
      ! backconversion: geometries: from [sqrt(amu)*a.u.] to [a.u.]
      guess(i,:)=guess(i,:)/dsqrt(M(i))
      refgeom(i,:)=refgeom(i,:)/dsqrt(M(i))
    enddo
  endif

else

  write(1,fmt='(A)')'Initial guess on the hypershere'
  write(1,fmt='(A)')''
  if (consttyp .eq. 0) then
    write(1,fmt='(A)')'using cartesian coordiantes:'
    write(1,fmt='(A)')''
  elseif (consttyp .eq. 1) then
    write(1,fmt='(A)')'Using mass weigthed cartesian coordiantes:'
    write(1,fmt='(A)')''
  endif

  if (inidirect.eq.0) then
    if (fixed_seed.eqv..true.) then
      call random_seed(size=n)
      allocate(seed(n))
      seed=iseed
      call random_seed(put=seed)
      deallocate(seed)
    else
      call random_seed
    endif  

    write(1,fmt='(A)')'Random'
    write(1,fmt='(A)')''

    do i=1,nat
      do j=1,3
        call random_number(e1)
        call random_number(e2)
        x1=dsqrt(-2.0_dpr*dlog(e1))*dcos(2.0_dpr*pi*e2)
        x2=dsqrt(-2.0_dpr*dlog(e1))*dsin(2.0_dpr*pi*e2)

        if (printl .ge. 2) then 
          write(1,fmt='(A)')'Random numbers:'
          write(1,fmt=outfmt)e1
          write(1,fmt=outfmt)e2
          write(1,fmt='(A)')'Gaussian random numbers:'
          write(1,fmt=outfmt)x1
          write(1,fmt=outfmt)x2
        endif

        if (consttyp .eq. 0) then
          geom2(i,j)=x1
        elseif (consttyp .eq. 1) then
          geom2(i,j)=x1*dsqrt(M(i))
        endif
      enddo
    enddo

    if (consttyp .eq. 1) then
      do i=1,nat
        ! geometries: from [a.u.] to [sqrt(amu)*a.u.]
        geom(i,:)=geom(i,:)*dsqrt(M(i))
      enddo
    endif

    geom2=geom2/col_norm(geom2,nat)
    refgeom=geom+(Rhyp/2.0_dpr)*geom2
    guess=geom+Rhyp*geom2

    if (consttyp .eq. 1) then
      do i=1,nat
        ! backconversion: geometries: from [sqrt(amu)*a.u.] to [a.u.]
        guess(i,:)=guess(i,:)/dsqrt(M(i))
        refgeom(i,:)=refgeom(i,:)/dsqrt(M(i))
      enddo
    endif
  elseif (abs(inidirect).eq.1) then
    write(1,fmt='(A)')'Normal modes:'
    open(unit=34,file='suscalls',form='formatted',status='old',iostat=filestat)
    if (filestat > 0) call error_sub("No suscalls file for program guessgeom.x ",2)
    do while (wc1.ne.'cartesian')
      read(34,fmt=*)wc1
      wc1=trim(adjustl(wc1))
    enddo

    wi1=int((3*nat)/8)
    if (mod((3*nat),8).ne.0) wi1=wi1+1

    do i=1,wi1-1
      read(34,fmt=*)
      read(34,fmt=*)
      read(34,fmt=*)
      read(34,fmt=*)trash1,(freq(j),j=(i-1)*8+1,(i-1)*8+8,1)
      read(34,fmt=*)
      do ii=1,nat
        do jj=1,3
          read(34,fmt='(6X,A1,X,A1)',advance='no')trash1,trash2
          if (((jj.eq.1).and.(trash2.eq.'x')).or.&
             &((jj.eq.2).and.(trash2.eq.'y')).or.&
             &((jj.eq.3).and.(trash2.eq.'z'))) then
            read(34,fmt=*)(lq(k,ii,jj),k=(i-1)*8+1,(i-1)*8+8,1)
          else
            do k=(i-1)*8+1,(i-1)*8+8
              lq(k,ii,jj)=0.0_dpr
            enddo
            backspace(34)
          endif 
        enddo
      enddo
    enddo
    i=wi1
    if (mod((3*nat),8).eq.0) then
      wi2=8
    else
      wi2=mod((3*nat),8)
    endif
    read(34,fmt=*)
    read(34,fmt=*)
    read(34,fmt=*)
    read(34,fmt=*)trash1,(freq(j),j=(i-1)*8+1,(i-1)*8+wi2,1)
    read(34,fmt=*)
    do ii=1,nat
      do jj=1,3
        filestat=0
        read(34,fmt='(A7)',advance='no',iostat=filestat)trash1
        if (filestat.eq.0) then
          backspace(34)
          read(34,fmt='(6X,A1,X,A1)',advance='no')trash1,trash2
          if (((jj.eq.1).and.(trash2.eq.'x')).or.&
             &((jj.eq.2).and.(trash2.eq.'y')).or.&
             &((jj.eq.3).and.(trash2.eq.'z'))) then
            read(34,fmt=*)(lq(k,ii,jj),k=(i-1)*8+1,(i-1)*8+wi2,1)
          else
            do k=(i-1)*8+1,(i-1)*8+wi2
              lq(k,ii,jj)=0.0_dpr
            enddo
            backspace(34)
          endif
        else
          do k=(i-1)*8+1,(i-1)*8+wi2
            lq(k,ii,jj)=0.0_dpr
          enddo
          backspace(34)
        endif
      enddo
    enddo
    close(34)


    if (consttyp .eq. 1) then
      do i=1,nat
        ! geometries: from [a.u.] to [sqrt(amu)*a.u.]
        geom(i,:)=geom(i,:)*dsqrt(M(i))
      enddo
    endif

    if (consttyp .eq. 0) then
      do i=1,nat
        lq(nmode,i,:)=lq(nmode,i,:)/dsqrt(M(i))
      enddo
      lq(nmode,:,:)=lq(nmode,:,:)/norm(lq(nmode,:,:),nat*3)
    endif

    if (inidirect.gt.0) then
      write(1,fmt='(A,I3)')'In positive direction of mode ',nmode
      write(1,fmt='(A)')''
      do i=1,nat
        do j=1,3
          refgeom(i,j)=geom(i,j)+(Rhyp/2.0_dpr)*lq(nmode,i,j)
          guess(i,j)=geom(i,j)+Rhyp*lq(nmode,i,j)
        enddo
      enddo 
    else
      write(1,fmt='(A,I3)')'in negative direction of mode ',nmode
      write(1,fmt='(A)')''
      do i=1,nat
        do j=1,3
          refgeom(i,j)=geom(i,j)-(Rhyp/2.0_dpr)*lq(nmode,i,j)
          guess(i,j)=geom(i,j)-Rhyp*lq(nmode,i,j)
        enddo
      enddo 
    endif
 
    if (consttyp .eq. 1) then
      do i=1,nat
        ! backconversion: geometries: from [sqrt(amu)*a.u.] to [a.u.]
        guess(i,:)=guess(i,:)/dsqrt(M(i))
        refgeom(i,:)=refgeom(i,:)/dsqrt(M(i))
      enddo
    endif

  elseif (abs(inidirect).eq.2) then
    !along the gradient
    open(unit=35,file='cartgrd',form='formatted',status='old',iostat=filestat)
    if (filestat > 0) call error_sub("No cartgrd file for program guessgeom.x ",2)
    allocate(grad(nat,3))
    do i=1,nat
      read(unit=35,fmt=*)(grad(i,j),j=1,3,1)
    enddo
    close(35)
    
    if (consttyp .eq. 1) then
      do i=1,nat
        geom(i,:)=geom(i,:)*dsqrt(M(i))
        grad(i,:)=grad(i,:)/dsqrt(M(i))
      enddo
    endif

    grad=grad/col_norm(grad,nat)

    if (inidirect.gt.0) then
      write(1,fmt='(A)')'In positive direction of gradient'
      write(1,fmt='(A)')''
      refgeom=geom
      guess=geom+(Rhyp/2.0_dpr)*grad
    else
      write(1,fmt='(A)')'in negative direction of gradient'
      write(1,fmt='(A)')''
      refgeom=geom
      guess=geom-(Rhyp/2.0_dpr)*grad
    endif
 
    if (consttyp .eq. 1) then
      do i=1,nat
        ! backconversion: geometries: from [sqrt(amu)*a.u.] to [a.u.]
        guess(i,:)=guess(i,:)/dsqrt(M(i))
        refgeom(i,:)=refgeom(i,:)/dsqrt(M(i))
      enddo
    endif

    deallocate(grad)
  elseif (abs(inidirect).eq.3) then
    open(unit=10,file='pointinggeom',form='formatted',status='old',iostat=filestat)
    if (filestat > 0) call error_sub("No pointinggeom file for program guessgeom.x ",2)
    do i=1,nat
      read(10,fmt=*)S2(i),Z2(i),geom2(i,1),geom2(i,2),geom2(i,3),M2(i)
    enddo
    close(10)

    if (printl.ge.1) then
      write(1,fmt='(A)')'Using the geometry from file pointinggeom [a.u. resp. amu]:'
      do i=1,nat
        write(unit=1,fmt='(1X,A2,2X,F5.1,4F14.8)',advance='yes')S(i),Z(i),geom2(i,1),geom2(i,2),geom2(i,3),M(i)
      enddo
      write(1,fmt='(A)')''
    endif
 
    if (consttyp .eq. 1) then
      do i=1,nat
        ! geometries: from [a.u.] to [sqrt(amu)*a.u.]
        geom(i,:)=geom(i,:)*dsqrt(M(i))
        geom2(i,:)=geom2(i,:)*dsqrt(M(i))
      enddo
    endif

    disp=geom2-geom
    disp=disp/col_norm(disp,nat)

    if (inidirect.gt.0) then
      write(1,fmt='(A)')'Pointing in the direction of this geometry'
      write(1,fmt='(A)')''
      refgeom=geom
      guess=geom+(Rhyp/2.0_dpr)*disp
    else
      write(1,fmt='(A)')'Pointing away from this geometry'
      write(1,fmt='(A)')''
      refgeom=geom
      guess=geom-(Rhyp/2.0_dpr)*disp
    endif

    if (consttyp .eq. 1) then
      do i=1,nat
        ! backconversion: geometries: from [sqrt(amu)*a.u.] to [a.u.]
        guess(i,:)=guess(i,:)/dsqrt(M(i))
        refgeom(i,:)=refgeom(i,:)/dsqrt(M(i))
      enddo
    endif
  endif
endif

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Write output
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

open(unit=40,file='geom',status='replace',form='formatted',iostat=filestat)
if (filestat > 0) call error_sub("Problem writing geom in program guessgeom.x ",2)
if (printl.ge.1) then
  write(1,fmt='(A)')'Geometry [a.u. resp. amu]:'
  do i=1,nat
    write(unit=1,fmt='(1X,A2,2X,F5.1,4F14.8)',advance='yes')S(i),Z(i),guess(i,1),guess(i,2),guess(i,3),M(i)
  enddo
  write(1,fmt='(A)')''
endif
  
do i=1,nat
  write(unit=40,fmt='(1X,A2,2X,F5.1,4F14.8)',advance='yes')S(i),Z(i),guess(i,1),guess(i,2),guess(i,3),M(i)
enddo
close(40)

open(unit=30,file='ref_geom',status='replace',form='formatted',iostat=filestat)
if (filestat > 0) call error_sub("Problem writing ref_geom in program guessgeom.x ",2)
if (printl.ge.1) then
  write(1,fmt='(A)')'Reference geometry [a.u. resp. amu]:'
  do i=1,nat
    write(unit=1,fmt='(1X,A2,2X,F5.1,4F14.8)',advance='yes')S(i),Z(i),refgeom(i,1),refgeom(i,2),refgeom(i,3),M(i)
  enddo
  write(1,fmt='(A)')''
endif

do i=1,nat
  write(unit=30,fmt='(1X,A2,2X,F5.1,4F14.8)',advance='yes')S(i),Z(i),refgeom(i,1),refgeom(i,2),refgeom(i,3),M(i)
enddo
close(30)

deallocate(Z)
deallocate(geom)
deallocate(guess)
deallocate(M)
deallocate(S)
deallocate(Z2)
deallocate(geom2)
deallocate(M2)
deallocate(S2)
deallocate(freq)
deallocate(lq)
deallocate(refgeom)
deallocate(wv1)
deallocate(wv2)
deallocate(disp)

write(1,fmt='(A)')'Program finished: guessgeom.x with SUCCESS' 

close(1)

end program
