!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
! part of MEPPACK

MODULE prec_mod
  implicit none
  integer, parameter        :: dpr      = kind(1.d0)
end module prec_mod

MODULE units_mod

! using values from 
! Handbook of Chemistry and Physics
! 86th edition 2005-2006

  use prec_mod
  implicit none
  real(kind=dpr), parameter :: au2ev    = 27.211384523_dpr         
  real(kind=dpr), parameter :: pi       = 3.141592653589793_dpr  
  real(kind=dpr), parameter :: au2ang   = 0.529177210818_dpr  
  real(kind=dpr), parameter :: au2aj    = 4.3597441775_dpr          
END MODULE units_mod
