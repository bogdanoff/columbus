!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
program hessian ! written by Bernhard Sellner
! (c) Bernhard Sellner 2011
! part of MEPPACK
! This program is part of the reaction path package.
! It calculates the mass weighted hessian


use prec_mod
use units_mod
use general_mod
use def_mod
use nml_mod

implicit none
real(kind=dpr) du1,du2,du3,Gn12(:,:),bmatrix(:,:),geom(:,:),M(:)
real(kind=dpr) Z(:),intgeom(:),Gp12(:,:),grad_ig(:,:,:),hess(:,:)
real(kind=dpr) refintg(:),C_angs,gradC_q(:),conhess(:,:),lambda
real(kind=dpr) gradE_q(:),bm1(:,:),bm2(:,:),wmr1(:,:)
integer i,j,k,l,ipiv(:),lwork,it,wi1,wi2,wi3,lines,filestat
character*1 n,t,S(:)
character*5 version
allocatable Gn12,ipiv,bmatrix,geom,M,S,Z,intgeom,Gp12,grad_ig,hess
allocatable refintg,gradC_q,conhess,gradE_q,bm1,bm2,wmr1

! Unit ..................................... input ..................... working
! bmatrix ... bmatrix ...................... [A|rad/A] ................. [A|rad/A]
! M ......... mass ......................... [amu] ..................... [amu]
! intgeom ... internal geometry ............ [A|rad] ................... [A*sqrt(amu)]
! refintg ... reference int. geom .......... [A|rad] ................... [A*sqrt(amu)]
! Gp12 ...... =G^(+1/2) .................... [(A|rad/(sqrt(amu)*A)] .... [(A|rad/(sqrt(amu)*A)]
! Gn12 ...... =G^(-1/2) .................... [(sqrt(amu)*A)/(A|rad)] ... [(sqrt(amu)*A)/(A|rad)]
! grad_ig ... int. grads for hessian ....... [aJ/(A|rad)] .............. [aJ/(A*sqrt(amu))]
! hess ...... hessian mw ................... [aJ/(A^2*amu)] ............ [aJ/(A^2*amu)]
! C_angs .... constraint ................... [A*sqrt(amu)] ............. [A*sqrt(amu)]
! gradC_q ... gradient of the constraint ... [1] ....................... [1]
! conhess ... hessian of the constraint .... [1/(A*sqrt(amu)] .......... [1/(A*sqrt(amu)]
! lambda .... langrangian multiplier ....... [aJ/(A*sqrt(amu))] ........ [aJ/(A*sqrt(amu))]
! gradE_q ... energy gradient .............. [aJ/(A|rad)] .............. [aJ/(A*sqrt(amu))]
! hdisp ..... dq for num. derivative ....... [A*sqrt(amu)] ............. [A*sqrt(amu)]

version='2.3.0'
filestat=0

open(unit=1,file='hessian.log',form='formatted',iostat=filestat)
if (filestat > 0) call error_sub("Problem writing check_Rtrust.log in program hessian.x ",2)

write(1,fmt='(A)')''
write(1,fmt='(A)')''
write(1,fmt='(A)')''
write(1,fmt='(A,A)')'Program started: hessian.x version ',version
write(1,fmt='(A)')''

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Read Input
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

call def_geninp(mem,nproc,elprog,coordprog,printl,prtstep,mld,&
               &inihess,inihmod,hessmod,mixmod,hessstep,chess,&
               &consttyp,fixed_seed,iseed,restart,linconv,hdisp)

open(unit=20,file='geninp',status='old',iostat=filestat)
  if (filestat > 0) call error_sub("No geninp file for program hessian.x ",2)
  read(20, NML= geninp)
close(20)

call def_ircpar(rpsteps,maxhypstep,inirtrust,Rhyp,&
           &conv_grad,conv_disp,conv_uwdisp,conv_cons,miciter,&
           &conv_alpha,conv_rad,soph_l,maxrtrust,minrtrust,lratio,uratio)

open(unit=20,file='ircpar',status='old',iostat=filestat)
  if (filestat > 0) call error_sub("No ircpar file for program hessian.x ",2)
  read(20, NML= ircpar)
close(20)

allocate(bmatrix(3*nat,nintc))
allocate(Gn12(nintc,nintc))
allocate(Gp12(nintc,nintc))
allocate(Z(nat))
allocate(geom(nat,3))
allocate(M(nat))
allocate(S(nat))
allocate(grad_ig(nintc,nintc,2))
allocate(hess(nintc,nintc))
allocate(refintg(nintc))
allocate(intgeom(nintc))
allocate(gradC_q(nintc))
allocate(gradE_q(nintc))
allocate(conhess(nintc,nintc))
allocate(bm1(3*nat,nintc))
allocate(bm2(3*nat,nintc))
allocate(wmr1(nintc,nintc))

open(unit=9,file='curr_iter',form='formatted',status='old',iostat=filestat)
  if (filestat > 0) call error_sub("No curr_iter file for program hessian.x ",2)
  read(9,fmt=*)it
close(9)

open(unit=10,file='geom',status='old',form='formatted',iostat=filestat)
if (filestat > 0) call error_sub("No geom file for program hessian.x ",2)
do i=1,nat
  read(10,fmt=*)S(i),Z(i),geom(i,1),geom(i,2),geom(i,3),M(i)
enddo
close(10)

if (it.ge.1) then
  open(unit=30,file='prev_bmatrix',status='old',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("No prev_bmatrix file for program hessian.x ",2)
    do j=1,nat
      do i=1,3 ! iteration over x,y,z
        wi1=nintc/3
        do l=1,wi1
          read(30,fmt=*)(bm1((j-1)*3+i,((l-1)*3+k)),k=1,3,1)
        enddo
      enddo
    enddo
  close(30)
endif

open(unit=30,file='curr_bmatrix',status='old',form='formatted',iostat=filestat)
if (filestat > 0) call error_sub("No curr_bmatrix file for program hessian.x ",2)
  do j=1,nat
    do i=1,3 ! iteration over x,y,z
      wi1=nintc/3
      do l=1,wi1
        read(30,fmt=*)(bm2((j-1)*3+i,((l-1)*3+k)),k=1,3,1)
      enddo
    enddo
  enddo
close(30)

if (it.ge.1) then
  du1=0.0_dpr
  do i=1,3*nat
    do j=1,nintc
      du1=du1+(bm1(i,j)-bm2(i,j))**2
    enddo
  enddo
  du1=du1/(dble(3)*dble(nat)*dble(nintc))
  du1=dsqrt(du1)

  if (printl.ge.1) then
    write(1,fmt='(A)',advance='no')'Difference current and previous bmatrix:'
    write(1,fmt=outfmt,advance='yes')du1
    write(1,fmt='(A)')''
  endif
endif

if (it.eq.0) then
  if (inihess.eq.'intcfl') then
    ! count number of lines in intcfl
    lines = 0
    filestat = 0
    open(unit=30,file='intcfl',status='old',iostat=filestat)
    if (filestat > 0) call error_sub("No intcfl file for program hessian.x ",2)
    do while (filestat == 0)
      read(30,*,iostat=filestat)
      if (filestat /= 0) exit
        lines=lines+1
      enddo
    close(30)

    open(unit=30,file='intcfl',status='old',form='formatted',iostat=filestat)
    if (filestat > 0) call error_sub("No intcfl file for program hessian.x ",2)
    k=int(nintc/8)
    if (mod(nintc,8).ne.0) then
    do i=1,(lines-k-1)
      read(30,fmt=*)
    enddo
    else
      do i=1,(lines-k)
        read(30,fmt=*)
      enddo
    endif

    ! Construct the hessian
    write(1,fmt='(A)')'Initial guess for hessian: intcfl'
    write(1,fmt='(A)')''
    hess=0.0_dpr
    do i=1,k
      read(30,fmt=*)(hess((i-1)*8+j,(i-1)*8+j),j=1,8)
    enddo
    if (mod(nintc,8).ne.0) then
      read(30,fmt=*)(hess(k*8+j,k*8+j),j=1,mod(nintc,8))
    else
    endif
    close(30)

    write(1,fmt='(A)')'Initial mass weigthing of hessian'
    write(1,fmt='(A)')''
    call getGmatrices(bm2,M,Gn12,Gp12,nintc,nat)
    call mwhess(hess,Gp12,nintc)
  elseif (inihess .eq. 'unitma') then
    write(1,fmt='(A)')'Initial guess for hessian: unit matrix'
    write(1,fmt='(A)')''
    
    hess=0.0_dpr
    do i=1,nintc
      hess(i,i)=1.0_dpr
    enddo

    write(1,fmt='(A)')'Initial mass weigthing of hessian'
    write(1,fmt='(A)')''
    call getGmatrices(bm2,M,Gn12,Gp12,nintc,nat)
    call mwhess(hess,Gp12,nintc)
  elseif (inihess.eq.'calc') then
    write(1,fmt='(A)')'Calculating initial hessian with Columbus'
    write(1,fmt='(A)')''
    call calc_hess(hess,nintc,nat)

    write(1,fmt='(A)')'Mass weigthing hessian'
    write(1,fmt='(A)')''
    call getGmatrices(bm2,M,Gn12,Gp12,nintc,nat)
    call mwhess(hess,Gp12,nintc)
    if (printl .ge. 2) then
      write(1,fmt='(A)')'Calculated hessian:'
        do i=1,nintc
          do j=1,(nintc-1)
            write(1,fmt=outfmt,advance='no')hess(i,j)
          enddo
          write(1,fmt=outfmt,advance='yes')hess(i,nintc)
        enddo
      write(1,fmt='(A)')''
    endif
  elseif (inihess.eq.'calcmw') then 
    write(1,fmt='(A)')'Calculating initial hessian at mass weigthed displacements'
    write(1,fmt='(A)')''
    call getGmatrices(bm2,M,Gn12,Gp12,nintc,nat)
    call calc_mwhess(grad_ig,hess,Gp12,hdisp,nintc)
    if (printl .ge. 2) then
      write(1,fmt='(A)')'Calculated hessian:'
        do i=1,nintc
          do j=1,(nintc-1)
            write(1,fmt=outfmt,advance='no')hess(i,j)
          enddo
          write(1,fmt=outfmt,advance='yes')hess(i,nintc)
        enddo
      write(1,fmt='(A)')''
    endif
  endif
elseif (it.eq.1) then
  if (inihmod.eq.'newh') then
    if (inihess.eq.'intcfl') then
      ! count number of lines in intcfl
      lines = 0
      filestat = 0
      open(unit=30,file='intcfl',status='old',iostat=filestat)
      if (filestat > 0) call error_sub("No intcfl file for program hessian.x ",2)
      do while (filestat == 0)
        read(30,*,iostat=filestat)
        if (filestat /= 0) exit
          lines=lines+1
        enddo
      close(30)

      open(unit=30,file='intcfl',status='old',form='formatted',iostat=filestat)
      if (filestat > 0) call error_sub("No intcfl file for program hessian.x ",2)
      k=int(nintc/8)
      do i=1,(lines-k-1)
        read(30,fmt=*)
      enddo

      ! Construct the hessian
      write(1,fmt='(A)')'Reading hessian from intcfl'
      write(1,fmt='(A)')''
      hess=0.0_dpr
      do i=1,k
        read(30,fmt=*)(hess((i-1)*8+j,(i-1)*8+j),j=1,8)
      enddo
      read(30,fmt=*)(hess(k*8+j,k*8+j),j=1,mod(nintc,8))
      close(30)

      write(1,fmt='(A)')'Mass weigthing of hessian'
      write(1,fmt='(A)')''
      call getGmatrices(bm2,M,Gn12,Gp12,nintc,nat)
      call mwhess(hess,Gp12,nintc)
    elseif (inihess .eq. 'unitma') then
      write(1,fmt='(A)')'Initial guess for hessian: unit matrix'
      write(1,fmt='(A)')''
    
      hess=0.0_dpr
      do i=1,nintc
        hess(i,i)=1.0_dpr
      enddo
      write(1,fmt='(A)')'Mass weigthing of hessian'
      write(1,fmt='(A)')''
      call getGmatrices(bm2,M,Gn12,Gp12,nintc,nat)
      call mwhess(hess,Gp12,nintc)
    endif
  endif
  if (hessmod .eq. 'calc') then
    if (mod(it,hessstep).eq.0) then
      write(1,fmt='(A)')'Calculating hessian with Columbus'
      write(1,fmt='(A)')''
      call calc_hess(hess,nintc,nat)
      write(1,fmt='(A)')'Mass weigthing hessian'
      write(1,fmt='(A)')''
      call getGmatrices(bm2,M,Gn12,Gp12,nintc,nat)
      if (printl .ge. 2) then
        write(1,fmt='(A)')'Used gmatrix Gp12 (hessmod=calc)'
        do i=1,nintc
          do j=1,(nintc-1)
            write(1,fmt=outfmt,advance='no')Gp12(i,j)
          enddo
          write(1,fmt=outfmt,advance='yes')Gp12(i,j)
        enddo
        write(1,fmt='(A)')''
      endif
      call mwhess(hess,Gp12,nintc)
      if (printl .ge. 2) then
        write(1,fmt='(A)')'Calculated hessian:'
          do i=1,nintc
            do j=1,(nintc-1)
              write(1,fmt=outfmt,advance='no')hess(i,j)
            enddo
            write(1,fmt=outfmt,advance='yes')hess(i,nintc)
          enddo
        write(1,fmt='(A)')''
      endif
    else
      if (inihmod.eq.'newh') then
        write(1,fmt='(A)')'ReCalculating hessian with Columbus'
        write(1,fmt='(A)')''
        call calc_hess(hess,nintc,nat)
        write(1,fmt='(A)')'Mass weigthing hessian'
        write(1,fmt='(A)')''
        call getGmatrices(bm1,M,Gn12,Gp12,nintc,nat)
        call mwhess(hess,Gp12,nintc)
        if ((mixmod .eq. 'update')) then
        else
          write(1,fmt='(A)')'Taking old hessian'
        endif
        write(1,fmt='(A)')''
      else
        write(1,fmt='(A)')'Updating hessian '
        call getGmatrices(bm1,M,Gn12,Gp12,nintc,nat)
        call newupdhess(hess,bm1,bm1,nintc,nat)        
        write(1,fmt='(A)')''
      endif
    endif
  elseif (hessmod .eq. 'calcmw') then
    if (mod(it,hessstep).eq.0) then
      write(1,fmt='(A)')'Calculating hessian at mass weigthed displacements'
      write(1,fmt='(A)')''
      call getGmatrices(bm2,M,Gn12,Gp12,nintc,nat)  
      if (printl .ge. 2) then
        write(1,fmt='(A)')'Used gmatrix Gp12 (hessmod=calcmw)'
        do i=1,nintc
          do j=1,(nintc-1)
            write(1,fmt=outfmt,advance='no')Gp12(i,j)
          enddo
          write(1,fmt=outfmt,advance='yes')Gp12(i,j)
        enddo
        write(1,fmt='(A)')''
      endif
      call calc_mwhess(grad_ig,hess,Gp12,hdisp,nintc)
      if (printl .ge. 2) then
        write(1,fmt='(A)')'Calculated hessian:'
          do i=1,nintc
            do j=1,(nintc-1)
              write(1,fmt=outfmt,advance='no')hess(i,j)
            enddo
            write(1,fmt=outfmt,advance='yes')hess(i,nintc)
          enddo
        write(1,fmt='(A)')''
      endif
    else
      if (inihmod.eq.'newh') then
        write(1,fmt='(A)')'ReCalculating hessian at mass weigthed displacements'
        write(1,fmt='(A)')''
        call getGmatrices(bm1,M,Gn12,Gp12,nintc,nat)  
        call calc_mwhess(grad_ig,hess,Gp12,hdisp,nintc)
        ! Undo massweigthing with old bmatrix
        call uwhess(hess,Gn12,nintc)
        ! Massweight with new bmatrix
        call getGmatrices(bm2,M,Gn12,Gp12,nintc,nat)  
        call mwhess(hess,Gp12,nintc)
      else 
        write(1,fmt='(A)')'Updating hessian '
        call getGmatrices(bm1,M,Gn12,Gp12,nintc,nat)
        call newupdhess(hess,bm1,bm1,nintc,nat)        
        write(1,fmt='(A)')''
      endif
    endif
    if (printl .ge. 2) then
      write(1,fmt='(A)')'Calculated hessian:'
      do i=1,nintc
        do j=1,(nintc-1)
          write(1,fmt=outfmt,advance='no')hess(i,j)
        enddo
        write(1,fmt=outfmt,advance='yes')hess(i,nintc)
      enddo
      write(1,fmt='(A)')''
    endif
  elseif ((hessmod .eq. 'update')) then
    if (mod(it,hessstep).eq.0) then
        write(1,fmt='(A)')'Updating hessian (hessmod=update) '
        write(1,fmt='(A)')''
        call getGmatrices(bm1,M,Gn12,Gp12,nintc,nat)
        call newupdhess(hess,bm1,bm1,nintc,nat)
    elseif (mod(it,hessstep).ne.0) then
      write(1,fmt='(A)')'Keeping hessian constant '  
      write(1,fmt='(A)')''
    endif
  endif
elseif (it.ge.2) then   ! it >=2 and all other cases
  if ((hessmod .eq. 'calc').or.(hessmod .eq. 'calcmw')) then
    if (mod(it,hessstep).eq.0) then
      if (hessmod .eq. 'calc') then
        write(1,fmt='(A)')'Calculating hessian with Columbus'
        write(1,fmt='(A)')''
        call calc_hess(hess,nintc,nat)
        write(1,fmt='(A)')'Mass weigthing hessian'
        write(1,fmt='(A)')''
        call getGmatrices(bm2,M,Gn12,Gp12,nintc,nat)
        call mwhess(hess,Gp12,nintc)
      elseif (hessmod .eq. 'calcmw') then
        write(1,fmt='(A)')'Calculating hessian at mass weigthed displacements'
        write(1,fmt='(A)')''
        call getGmatrices(bm2,M,Gn12,Gp12,nintc,nat)
        call calc_mwhess(grad_ig,hess,Gp12,hdisp,nintc)
      endif
      if (printl .ge. 2) then
        write(1,fmt='(A)')'Calculated hessian:'
          do i=1,nintc
            do j=1,(nintc-1)
              write(1,fmt=outfmt,advance='no')hess(i,j)
            enddo
            write(1,fmt=outfmt,advance='yes')hess(i,nintc)
          enddo
        write(1,fmt='(A)')''
      endif
    elseif (mod(it,hessstep).ne.0) then
      if (mixmod .eq. 'update') then
        write(1,fmt='(A)')'Updating hessian in mixmode'
        write(1,fmt='(A)')''
        call getGmatrices(bm1,M,Gn12,Gp12,nintc,nat)
        call newupdhess(hess,bm1,bm1,nintc,nat)
      endif
    endif
  elseif ((hessmod .eq. 'update')) then
    if ((mixmod .eq. 'update').and.(mod(it,hessstep).eq.0)) then
      write(1,fmt='(A)')'Updating hessian '
      write(1,fmt='(A)')''
      call getGmatrices(bm1,M,Gn12,Gp12,nintc,nat)
      call newupdhess(hess,bm1,bm1,nintc,nat)
    else
      write(1,fmt='(A)')'Keeping hessian constant '
      write(1,fmt='(A)')''
    endif
  endif
endif

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Hessian of the constraint
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

if (chess.eqv..true.) then

  open(unit=15,file='ref_intgeom',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("No ref_intgeom file for program hessian.x ",2)
  do i=1,nintc
    read(15,fmt=*)refintg(i)
  enddo
  close(15)

  open(unit=15,file='curr_intgeom',form='formatted',iostat=filestat)
  if (filestat > 0) call error_sub("No curr_intgeom file for program hessian.x ",2)
  do i=1,nintc
    read(15,fmt=*)intgeom(i)
  enddo
  close(15)

  if (it.eq.0) then
    open(unit=14,file='curr_gradE_q',form='formatted',iostat=filestat)
    if (filestat > 0) call error_sub("No curr_gradE_q file for program hessian.x ",2)
      do i=1,nintc
        read(14,fmt=*)gradE_q(i)
      enddo
    close(14)
    call getGmatrices(bm2,M,Gn12,Gp12,nintc,nat)
    call mwgrad(gradE_q,Gp12,nintc)  

    call constraint(refintg,intgeom,Gn12,Gp12,C_angs,gradC_q,conhess,nintc,Rhyp,printl)
    call get_lambda(lambda,hess,gradC_q,gradC_q,gradE_q,.false.,nintc)

    if (printl .ge. 2) then
      write(1,fmt='(A)')'Hessian of constraint:'
        do i=1,nintc
          do j=1,(nintc-1)
            write(1,fmt=outfmt,advance='no')conhess(i,j)
          enddo
          write(1,fmt=outfmt,advance='yes')conhess(i,nintc)
        enddo
      write(1,fmt='(A)')''
    endif

    write(1,fmt='(A)',advance='no')'Langrangian multiplier: '
    write(1,fmt=outfmt,advance='yes')lambda
    write(1,fmt='(A)')''
    write(1,fmt='(A)')'Adding second derivative of constraint using conv. L. multiplier'
    write(1,fmt='(A)')''
    hess=hess-lambda*conhess
  elseif (it.eq.1) then ! it >=1
    if (hessmod.eq.'calc') then
      if (mod(it,hessstep).eq.0) then
        write(1,fmt='(A)')'Adding second derivative of constraint using L. multiplier from file'
        write(1,fmt='(A)')''
        open(unit=128,file='lambda',status='old',form='formatted',iostat=filestat)
        if (filestat > 0) call error_sub("No lambda file for program hessian.x ",2)
        read(128,fmt=*)lambda
        close(128)
        call getGmatrices(bm2,M,Gn12,Gp12,nintc,nat)
        if (printl .ge. 2) then
          write(1,fmt='(A)')'Used gmatrix Gp12 for hessian of constraint' 
          do i=1,nintc
            do j=1,(nintc-1)
              write(1,fmt=outfmt,advance='no')Gp12(i,j)
            enddo
            write(1,fmt=outfmt,advance='yes')Gp12(i,j)
          enddo
          write(1,fmt='(A)')''
        endif
        call constraint(refintg,intgeom,Gn12,Gp12,C_angs,gradC_q,conhess,nintc,Rhyp,printl)
        if (printl .ge. 2) then
          write(1,fmt='(A)')'Calculated hessian of constraint in case of calc:'
            do i=1,nintc
              do j=1,(nintc-1)
                write(1,fmt=outfmt,advance='no')conhess(i,j)
              enddo
              write(1,fmt=outfmt,advance='yes')conhess(i,nintc)
            enddo
          write(1,fmt='(A)')''
        endif
        hess=hess-lambda*conhess
      else
        if (inihmod.eq.'newh') then
          write(1,fmt='(A)')'Adding second derivative of constraint using L. multiplier from file'
          write(1,fmt='(A)')''
          open(unit=128,file='lambda',status='old',form='formatted',iostat=filestat)
          if (filestat > 0) call error_sub("No lambda file for program hessian.x ",2)
          read(128,fmt=*)lambda
          close(128)
          call getGmatrices(bm2,M,Gn12,Gp12,nintc,nat)
          call constraint(refintg,intgeom,Gn12,Gp12,C_angs,gradC_q,conhess,nintc,Rhyp,printl)
          hess=hess-lambda*conhess
        endif
      endif
    endif

    if (hessmod.eq.'calcmw') then
      if (mod(it,hessstep).eq.0) then
        write(1,fmt='(A)')'Adding second derivative of constraint using L. multiplier from file'
        write(1,fmt='(A)')''
        open(unit=128,file='lambda',status='old',form='formatted',iostat=filestat)
        if (filestat > 0) call error_sub("No lambda file for program hessian.x ",2)
        read(128,fmt=*)lambda
        close(128)
        call getGmatrices(bm2,M,Gn12,Gp12,nintc,nat)
        if (printl .ge. 2) then
          write(1,fmt='(A)')'Used gmatrix Gp12 for hessian of constraint'
          do i=1,nintc
            do j=1,(nintc-1)
              write(1,fmt=outfmt,advance='no')Gp12(i,j)
            enddo
            write(1,fmt=outfmt,advance='yes')Gp12(i,j)
          enddo
          write(1,fmt='(A)')''
        endif
        call constraint(refintg,intgeom,Gn12,Gp12,C_angs,gradC_q,conhess,nintc,Rhyp,printl)
        if (printl .ge. 2) then
          write(1,fmt='(A)')'Calculated hessian of constraint in case of calcmw:'
          do i=1,nintc
            do j=1,(nintc-1)
              write(1,fmt=outfmt,advance='no')conhess(i,j)
            enddo
            write(1,fmt=outfmt,advance='yes')conhess(i,nintc)
          enddo
          write(1,fmt='(A)')''
        endif
        hess=hess-lambda*conhess
      else
        if (inihmod.eq.'newh') then
          write(1,fmt='(A)')'Adding second derivative of constraint using L. multiplier from file'
          write(1,fmt='(A)')''
          open(unit=128,file='lambda',status='old',form='formatted',iostat=filestat)
          if (filestat > 0) call error_sub("No lambda file for program hessian.x ",2)
          read(128,fmt=*)lambda
          close(128)
          call getGmatrices(bm2,M,Gn12,Gp12,nintc,nat)
          call constraint(refintg,intgeom,Gn12,Gp12,C_angs,gradC_q,conhess,nintc,Rhyp,printl)
          hess=hess-lambda*conhess
        endif
      endif
    endif

    if ((inihess.eq.'intcfl').or.(inihess.eq.'unitma')) then
      write(1,fmt='(A)')'Adding second derivative of constraint using L. multiplier from file'
      write(1,fmt='(A)')''
      open(unit=128,file='lambda',status='old',form='formatted',iostat=filestat)
      if (filestat > 0) call error_sub("No lambda file for program hessian.x ",2)
      read(128,fmt=*)lambda
      close(128)
      call getGmatrices(bm1,M,Gn12,Gp12,nintc,nat)
      call constraint(refintg,intgeom,Gn12,Gp12,C_angs,gradC_q,conhess,nintc,Rhyp,printl)
      hess=hess-lambda*conhess
    endif
  elseif (it.ge.2) then
    if (((hessmod.eq.'calc').or.(hessmod.eq.'calcmw')).and.(mod(it,hessstep).eq.0))  then
      write(1,fmt='(A)')'Adding second derivative of constraint using L. multiplier from file'
      write(1,fmt='(A)')''
      open(unit=128,file='lambda',status='old',form='formatted',iostat=filestat)
      if (filestat > 0) call error_sub("No lambda file for program hessian.x ",2)
      read(128,fmt=*)lambda
      close(128)
      call getGmatrices(bm2,M,Gn12,Gp12,nintc,nat)
      call constraint(refintg,intgeom,Gn12,Gp12,C_angs,gradC_q,conhess,nintc,Rhyp,printl)
      hess=hess-lambda*conhess
    endif
  endif
endif

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Write output
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

if (printl .ge. 2) then
  write(1,fmt='(A)')'Final Hessian :'
    do i=1,nintc
      do j=1,(nintc-1)
        write(1,fmt=outfmt,advance='no')hess(i,j)
      enddo
      write(1,fmt=outfmt,advance='yes')hess(i,nintc)
    enddo
  write(1,fmt='(A)')''
endif

open(unit=40,file='mwhessian',status='replace',form='formatted',iostat=filestat)
if (filestat > 0) call error_sub("Problem writing mwhessian in program hessian.x ",2)
do i=1,nintc
  do j=1,(nintc-1)
    write(40,fmt=outfmt,advance='no')hess(i,j)
  enddo
  write(40,fmt=outfmt,advance='yes')hess(i,nintc)
enddo
close(40)

deallocate(bmatrix)
deallocate(Gn12)
deallocate(Gp12)
deallocate(Z)
deallocate(geom)
deallocate(M)
deallocate(S)
deallocate(grad_ig)
deallocate(hess)
deallocate(refintg)
deallocate(intgeom)
deallocate(gradC_q)
deallocate(gradE_q)
deallocate(conhess)
deallocate(bm1)
deallocate(bm2)
deallocate(wmr1)

write(1,fmt='(A)')'Program finished: hessian.x with SUCCESS '

close(1)

contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Subroutines
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

subroutine newupdhess(hess,bm1,bm2,nintc,nat)
! This subroutine updates the hessian according to the 
! BFGS procedure modified by Powell which keeps the 
! the hessian positive definite in case it already 
! had this property

use prec_mod
use units_mod
use general_mod

implicit none
integer nintc,nat
real(kind=dpr) old_gEq(nintc),new_gEq(nintc),old_gCq(nintc),new_gCq(nintc),lambda,old_h(nintc)
real(kind=dpr) new_h(nintc),z(nintc),hess(nintc,nintc),theta,dq(nintc),wv1(nintc)
real(kind=dpr) du1,du2,du3,wm1(nintc,nintc),Gp12(nintc,nintc),Gn12(nintc,nintc)
real(kind=dpr) bm1(3*nat,nintc),bm2(3*nat,nintc),refintg(nintc),oldintg(nintc),newintg(nintc)
integer i,j,k,l,filestat
character*1 n,t
character*5 version
logical bfgs,powell

! Units ................................................. input .................... working
! refintg .... reference int. geom ...................... [A|rad] .................. [A*sqrt(amu)]
! newintg .... new int. geom ............................ [A|rad] .................. [A*sqrt(amu)]
! oldintg .... old int.geom ............................. [A|rad] .................. [A*sqrt(amu)]
! old_gEq .... old energy gradient ...................... [aJ/(A|rad)] ............. [aJ/(A*sqrt(amu))]
! new_gEq .... new energy gradient ...................... [aJ/(A|rad)] ............. [aJ/(A*sqrt(amu))]
! dq ......... total step in internal coordinates ....... [sqrt(amu)*A] ............ [sqrt(amu)*A]
! lambda ..... langrangian multiplier ................... [aJ/(A*sqrt(amu))] ....... [aJ/(A*sqrt(amu))]
!
! Gp12 ....... =G^(+1/2) ................................ [(A|rad/(sqrt(amu)*A)]
! Gn12 ....... =G^(-1/2) ................................ [(sqrt(amu)*A)/(A|rad)]
! old_gCq .... old gradient of the constraint ........... [1]
! new_gCq .... new gradient of the constraint ........... [1]
! theta ...... used to preserve positive definiteness ... [1]
! hess ....... hessian .................................. [aJ/(A^2*amu)]

filestat=0

open(unit=13,file='mwhessian',form='formatted',status='old',iostat=filestat)
if (filestat > 0) call error_sub("No mwhessian file for sub newupdhess",2)
do i=1,nintc
  read(13,fmt=*)(hess(i,j),j=1,nintc)
enddo
close(13)

open(unit=15,file='ref_intgeom',form='formatted',status='old',iostat=filestat)
if (filestat > 0) call error_sub("No ref_intgeom file for sub newupdhess",2)
 do i=1,nintc
    read(15,fmt=*)refintg(i)
 enddo
close(15)

open(unit=15,file='prev_intgeom',form='formatted',status='old',iostat=filestat)
if (filestat > 0) call error_sub("No prev_intgeom file for sub newupdhess",2)
 do i=1,nintc
    read(15,fmt=*)oldintg(i)
 enddo
close(15)

open(unit=15,file='curr_intgeom',form='formatted',status='old',iostat=filestat)
if (filestat > 0) call error_sub("No curr_intgeom file for sub newupdhess",2)
 do i=1,nintc
    read(15,fmt=*)newintg(i)
 enddo
close(15)

open(unit=14,file='prev_gradE_q',form='formatted',status='old',iostat=filestat)
if (filestat > 0) call error_sub("No prev_gradE_q file for sub newupdhess",2)
  do i=1,nintc
    read(14,fmt=*)old_gEq(i)
  enddo
close(14)

open(unit=14,file='curr_gradE_q',form='formatted',status='old',iostat=filestat)
if (filestat > 0) call error_sub("No curr_gradE_q file for sub newupdhess",2)
  do i=1,nintc
    read(14,fmt=*)new_gEq(i)
  enddo
close(14)

open(unit=26,file='delta_q',form='formatted',status='old',iostat=filestat)
if (filestat > 0) call error_sub("No delta_q file for sub newupdhess",2)
 do i=1,nintc
    read(26,fmt=*)dq(i)
 enddo
close(26)

open(unit=28,file='lambda',form='formatted',status='old',iostat=filestat)
if (filestat > 0) call error_sub("No lambda file for sub newupdhess",2)
  read(28,fmt=*)lambda
close(28)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Update hessian
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! mass weighting
call getGmatrices(bm1,M,Gn12,Gp12,nintc,nat)
call constraint(refintg,oldintg,Gn12,Gp12,C_angs,old_gCq,conhess,nintc,Rhyp,printl)
call mwgrad(old_gEq,Gp12,nintc)

call getGmatrices(bm2,M,Gn12,Gp12,nintc,nat)
call constraint(refintg,newintg,Gn12,Gp12,C_angs,new_gCq,conhess,nintc,Rhyp,printl)
call mwgrad(new_gEq,Gp12,nintc)

old_h=old_gEq-lambda*old_gCq
new_h=new_gEq-lambda*new_gCq

du1=sp((new_h-old_h),dq,nintc)
call DGEMV('n',nintc,nintc,1.0_dpr,hess,nintc,dq,1,0.0_dpr,wv1,1)
du2=0.2_dpr*sp(dq,wv1,nintc)
du3=sp(dq,wv1,nintc)

! check condition and get theta

if (du1.ge.du2) then
  theta=1.0_dpr
else
  theta=(((1.0_dpr-0.2_dpr)*du3)/(du3-du1))
endif

z=theta*(new_h-old_h)+(1.0_dpr-theta)*wv1

!wm1=z x zt
call tp(wm1,z,z,nintc)
hess=hess+wm1/sp(z,dq,nintc)

call tp(wm1,wv1,wv1,nintc)
hess=hess-wm1/du3

if (printl .ge. 2) then
  write(1,fmt='(A)')'Updated hessian:'
    do i=1,nintc
      do j=1,(nintc-1)
        write(1,fmt=outfmt,advance='no')hess(i,j)
      enddo
      write(1,fmt=outfmt,advance='yes')hess(i,nintc)
    enddo
  write(1,fmt='(A)')''
endif

end subroutine

subroutine calc_mwhess(grad_ig,hess,Gp12,disp,nintc)
! This subroutine calculates the mass weighted hessian using 
! mass weighted gradients at mw displacements

use prec_mod
use units_mod
use general_mod

integer nintc
real(kind=dpr) Gp12(nintc,nintc),hess(nintc,nintc),grad_ig(nintc,nintc,2),disp
integer i,j,k,filestat

! Units .................................... input .................... working
! grad_ig ... int. grads for hessian ....... [aJ/(A|rad)] .............. [aJ/(A*sqrt(amu))]
!
! hess ...... hessian ...................... [aJ/(A^2*amu)]
! Gp12 ...... =G^(+1/2) .................... [(A|rad/(sqrt(amu)*A)]
! disp ...... dq for num. derivative ....... [A*sqrt(amu)]

filestat=0

open(unit=29,file='grad_digs',status='old',form='formatted',iostat=filestat)
if (filestat > 0) call error_sub("No grad_digs file for sub calc_mwhess",2)
do i=1,nintc
  do k=1,2
    do j=1,nintc
      read(29,fmt=*)grad_ig(i,j,k)
    enddo
    read(29,fmt=*)
  enddo
enddo
close(29)

call get_mwhess(grad_ig,hess,Gp12,disp,nintc)

end subroutine

subroutine calc_hess(hess,nintc,nat)
! This subroutine reads the  
! COLUMBUS hessian

use prec_mod
use units_mod
use general_mod

integer nintc,nat
real(kind=dpr) hess(nintc,nintc),hess1(nintc,nintc),ws1,du1
real(kind=dpr) bm1(3*nat,nintc),bm2(3*nat,nintc)
integer i,j,k,wi1,wi2,filestat

! hess ...... hessian ...................... [aJ/((A|rad)^2)]

filestat=0

open(unit=43,file='hessian',form='formatted',status='old',iostat=filestat)
if (filestat > 0) call error_sub("No hessian file for sub calc_hess",2)
wi1=nintc/8
wi2=mod(nintc,8)
do i=1,nintc
  do j=1,wi1
    read(43,fmt=*)(hess(i,(j-1)*8+k),k=1,8)
  enddo
  read(43,fmt=*)(hess(i,wi1*8+k),k=1,wi2)
enddo
close(43)

end subroutine

subroutine get_mwhess(grad_ig,hess,Gp12,disp,nintc)
! This subroutine calculates the numerical second derivatives 

use prec_mod
use units_mod
use general_mod

integer nintc
real(kind=dpr) Gp12(nintc,nintc)
real(kind=dpr) disp,hess(nintc,nintc),ws1 
real(kind=dpr) grad_ig(nintc,nintc,2),wv1(nintc),wv2(nintc)
integer i,j,k

! grad_ig ... int. grads for hessian ... [aJ/(A*sqrt(amu))]
! hess ...... hessian .................. [aJ/(A^2*amu)]
! Gp12 ...... =G^(+1/2) ................ [(A|rad/(sqrt(amu)*A)]
! disp ...... dq for num. derivative ... [A*sqrt(amu)]

hess=0.0_dpr

do i=1,nintc
  wv1=grad_ig(i,:,1)
  wv2=grad_ig(i,:,2)

  !getmassweighted gradients
  call mwgrad(wv1,Gp12,nintc)
  call mwgrad(wv2,Gp12,nintc)
 
  ! make numeric differentiation
  hess(:,i)=(wv2-wv1)/(2.0_dpr*disp)  
enddo

! symmetrize hessian
do i=1,(nintc-1)
  do j=(i+1),nintc
    ws1=hess(i,j)-hess(j,i)
    if (dabs(ws1).gt.0.001) then
      print*,'Warning: Hessian not symmetric:'
      print*,'Element   i           j deviation:'
      print*,i,j,ws1
      write(1,fmt='(A)')'Warning: Hessian not symmetric:'
      write(1,fmt='(A16,X,I5,A1,X,I5,A2)',advance='no')'difference in  (',i,',',j,')='
      write(1,fmt=outfmt)ws1
      write(1,fmt='(A)')' '
    endif
    ws1=(hess(i,j)+hess(j,i))/2.0_dpr
    hess(i,j)=ws1
    hess(j,i)=ws1
  enddo
enddo

end subroutine

end program
