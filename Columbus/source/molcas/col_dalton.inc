!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
        integer luscr,ibit08,ifield,mx2buf
        integer irat,irat2,lrat
*@ifdef int64
c       16bit packing
        parameter (ibit08=65535,ifield=16,mx2buf=682)
        PARAMETER (IRAT = 1, IRAT2 = 1, LRAT = 1)
*@else
*c       8bit packing
*        parameter (ibit08=255,ifield=8,mx2buf=682)
*@if defined (VAR_STAR2)
*      PARAMETER (IRAT = 2, IRAT2 = 4, LRAT = 2)
*@else
*        PARAMETER (IRAT = 2, IRAT2 = 2, LRAT = 2)
*@endif
*@endif
        integer dibuffer(2*mx2buf), nbuf
        real*8 dalbuffer,dbuffer(mx2buf)
        common /col_dalton/luscr,nbuf,dalbuffer(mx2buf+irat*mx2buf)
        equivalence(dalbuffer(1),dbuffer(1))
        equivalence(dalbuffer(mx2buf+1),dibuffer(1))
