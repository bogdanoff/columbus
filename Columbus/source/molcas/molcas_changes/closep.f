!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
*fordeck closep $Revision: 2.3 $           
      SubRoutine CloseP
************************************************************************
*                                                                      *
* Object: to close the handling of the 2nd order density matrix.       *
*                                                                      *
* Called from: Seward                                                  *
*                                                                      *
* Calling    : QEnter                                                  *
*              QExit                                                   *
*                                                                      *
*     Author: Roland Lindh, Dept. of Theoretical Chemistry,            *
*             University of Lund, SWEDEN                               *
*                                                                      *
*                                                                      *
* Copyright 1992 R. Lindh, Dept. of Theor. Chem. Univ. of Lund, Sweden *
************************************************************************
      Implicit Real*8 (A-H,O-Z)
      Include 'print.inc'
      Include 'WrkSpc.inc'
      Include 'real.inc'
      Include 'pso.inc'
      Include 'aces_gamma.inc'
CStart Molpro
c;      common/codeplus/luhf
CEnd
*
      iRout = 249
      iPrint = nPrint(iRout)
*     Call qEnter('CloseP')
*
      If (Gamma_On) Then
************ columbus interface ****************************************
*  using Close instead of DaClos for the half-sorted effective
*  two-particle density matrix produces internal errors
c        Close(LuGamma)
         Call DaClos(LuGamma)
         Call GetMem('Bin','Free','Real',ipBin,2*lBin)
         Call GetMem('G_Toc','Free','Real',ipG_Toc,iDummy)
         Call GetMem('SO2cI','Free','Inte',ipSO2cI,iDummy)
      End If
*
      If (lPSO) Then
         Call GetMem(' G2 ','Free','Real',ipG2,nG2)
         Call GetMem(' G1 ','Free','Real',ipG1,nG1)
CStart Molpro
c;         Call GetMem('CMO  ','Free','Real',ipCMO  ,mCMO)
c;         If(luhf.ne.0) then
c;           Call GetMem('D01 ','Free','Real',ipD01,nDens1)
c;           Call GetMem('CMO1','Free','Real',ipCMO1,mCMO1)
c;           Call GetMem(' G11','Free','Real',ipG11,nG11)
c;           Call GetMem(' G21','Free','Real',ipG21,nG21)
c;         End If
CEnd
      End If
Cstart .not.Molpro
c... for molpro this causes crash if lPSO=.false.
      Call GetMem('CMO  ','Free','Real',ipCMO  ,mCMO)
Cend
      Call GetMem('DSVar','Free','Real',ipDSVar,nDens)
      Call GetMem('DS   ','Free','Real',ipDS   ,nDens)
      Call GetMem('DVar ','Free','Real',ipDVar ,nDens)
      Call GetMem('D0   ','Free','Real',ipD0   ,nDens)
*
      If (ip_Z_p_k.ne.ip_Dummy) Call Free_Work(ip_Z_p_k)
*
*     Call qExit('CloseP')
      Return
      End
