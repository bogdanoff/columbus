!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
*fordeck prepp $Revision: 2.3 $         
CStart Molcas
      SubRoutine PrepP
CElse
c;      SubRoutine PrepP(orbmolpro,fockmolpro,d1molpro,den1,den2,
c;     >    methodmolpro,iclos,iact)
CEnd
************************************************************************
*                                                                      *
* Object: to set up the handling of the 2nd order density matrix.      *
*                                                                      *
* Called from: Alaska                                                  *
*                                                                      *
* Calling    : QEnter                                                  *
*              OpnOne                                                  *
*              GetMem                                                  *
*              RdOne                                                   *
*              PrMtrx                                                  *
*              ClsOne                                                  *
*              ErrOne                                                  *
*              QExit                                                   *
*                                                                      *
*     Author: Roland Lindh, Dept. of Theoretical Chemistry,            *
*             University of Lund, SWEDEN                               *
*             January '92                                              *
*                                                                      *
* Copyright 1992 R. Lindh, Dept. of Theor. Chem. Univ. of Lund, Sweden *
************************************************************************
      Implicit Real*8 (A-H,O-Z)
#include "itmax.fh"
#include "info.fh"
      Include 'print.inc'
      Include 'real.inc'
      Include 'WrkSpc.inc'
      Include 'pso.inc'
      Include 'etwas.inc'
      Include 'aces_gamma.inc'
************ columbus interface ****************************************
      Include 'columbus_gamma.inc'
      Include 'setup.inc'
      Include 'shinf.inc'
************************************************************************
CStart Molpro
c;      Real*8 orbmolpro(*),d1molpro(*),fockmolpro(*),den1(*),den2(*)
c;      integer iclos(8),iact(8)
c;      Character *(*) methodmolpro
c;      include "common/lmp2gr"
CEnd
      Integer nFro(0:7)
      Character*8 RlxLbl,Method, KSDFT*16
      Logical lopen,lPrint
*
*...  Prologue
      iRout = 250
      iPrint = nPrint(iRout)
CStart Molcas
      lPrint=.True.
CElse
c;      lPrint=.False.
CEnd
      Call qEnter('PrepP')
      iD0Lbl=1
      iComp=1
*
      lsa=.False.
      Gamma_On=.False.
      Gamma_mrcisd=.FALSE.
      lPSO=.false.
      Case_2C=.False.
      Case_3C=.False.
      ip_Z_p_k=ip_Dummy                   

      nDens = 0
      Do 1 iIrrep = 0, nIrrep - 1
         nDens = nDens + nBas(iIrrep)*(nBas(iIrrep)+1)/2
 1    Continue
*
CStart Molcas
*
*...  Get the method label
      Call Get_cArray('Relax Method',Method,8)
CElse
c;      Method = methodmolpro
c;      ExFac=Get_ExFac(' ')
c;      CoulFac=Get_CoulFac()
CEnd
      nCMo = n2Tot
      mCMo = n2Tot
      If (Method.eq. 'KS-DFT  ' .or.
     &    Method.eq. 'CASDFT  ' ) Then
         Call Get_iScalar('Multiplicity',iSpin)
         Call Get_cArray('DFT functional',KSDFT,16)
         ExFac=Get_ExFac(KSDFT)
         CoulFac=One
      Else
         iSpin=0
Cstart Molcas
         ExFac=One
         CoulFac=One
Cend
      End If
*
*...  Check the wave function type
*
*                                                                      *
************************************************************************
*                                                                      *
      If ( Method.eq.'RHF-SCF ' .or.
     &     Method.eq.'UHF-SCF ' .or.
     &    (Method.eq.'KS-DFT  '.and.iSpin.eq.1) .or.
     &     Method.eq.'ROHF    ' ) then
         If (lPrint) Then
            Write (6,*)
            Write (6,'(2A)') ' Wavefunction type: ',Method
            If (Method.eq.'KS-DFT  ')
     &         Write (6,'(2A)') ' Functional type:   ',KSDFT
            Write (6,*)
         End If
*                                                                      *
************************************************************************
*                                                                      *
      Else if ( Method.eq.'Corr. WF' ) then
         If (lPrint) Then
            Write (6,*)
            Write (6,*)
     &         ' Wavefunction type: an Aces 2 correlated wavefunction'
            Write (6,*)
         End If
         Gamma_On=.True.
cstart Molpro
c;         call error('Aces_Gamma called','prepp')
celse
         Call Aces_Gamma()
cend
       Else if (Method(1:7).eq.'MR-CISD') then
************ columbus interface ****************************************
*do not reconstruct the two-particle density from the one-particle
*density or partial two-particle densities but simply read them from
*file

        nShell=mSkal
        nPair=nShell*(nShell+1)/2
        nQuad=nPair*(nPair+1)/2

*---- Allocate Table Of Content for half sorted gammas.
*
      Call GetMem('G_Toc','Allo','Real',ipG_Toc,nQuad+2)

*  find free unit number
        lgtoc=61
        lgtoc=isFreeUnit(lgtoc)
        idisk=0
*  read table of contents of half-sorted gamma file
         Call DaName(lgtoc,'gtoc')
         Call ddafile(lgtoc,2,Work(ipg_toc),nQuad+2,idisk)
         Call Daclos(lgtoc)
         n=int(Work(ipg_toc+nQuad))
         lbin=int(Work(ipg_toc+nQuad+1))
         if (n.ne.nQuad) then 
           Write (6,*) 'n.ne.nQuad,n,nQuad=',n,nQuad
           Call Abend()
         endif
           
c       open(unit=lgtoc,file='gtoc',form='unformatted')
c        read(lgtoc) n
c        if (n.ne.nquad) stop 'quad error'
c        call read_lgtoc(lgtoc,Work(ipg_toc),n)
c        read(lgtoc) lbin
c       close (lgtoc)
        Gamma_On=.True.
        Gamma_mrcisd=.TRUE.
*       open gamma file
         LuGamma=60
         LuGamma=isfreeunit(LuGamma)
*        closed in closep
         Call DaName_MF(LuGamma,'GAMMA')
*  allocate space for bins
         Call GetMem('Bin','Allo','Real',ipBin,2*lBin)
*  compute SO2cI array
         Call GetMem('SO2cI','Allo','Inte',ipSO2cI,2*nSOs)
         call so2ci(iWork(ipSO2cI),iWork(ipSOsh),nsos)
*                                                                      *
************************************************************************
*                                                                      *
      Else if ( Method.eq.'RASSCF  ' .or.
     &          Method.eq.'CASSCF  ' .or.
     &          Method.eq.'CASDFT  ') then
*
cstart Molpro
c;         call imove(iclos,nISh,8)
c;         call imove(iact,nASh,8)
c;         call izero(nFro,8)
c;         lPSO=.true.
celse
         Call Get_iArray('nAsh',nAsh,nIrrep)
cend
         nAct = 0
         Do iIrrep = 0, nIrrep-1
            nAct = nAct + nAsh(iIrrep)
         End Do
Cstart Molpro
c;         lPSO=.true.
celse
         If (nAct.gt.0) lPSO=.true.
cend
*
         nDSO = nDens
         mIrrep=nIrrep
         Call ICopy(nIrrep,nBas,1,mBas,1)
         If (lPrint) Then
            Write (6,*)
            Write (6,'(2A)') ' Wavefunction type: ', Method
            If (Method.eq.'CASDFT  ')
     &         Write (6,'(2A)') ' Functional type:   ',KSDFT
            Write (6,*)
         End If
*                                                                      *
************************************************************************
*                                                                      *
      Else if ( Method.eq.'CASSCFSA' .or.
     &          Method.eq.'RASSCFSA' ) then
         Call Get_iArray('nAsh',nAsh,nIrrep)
         nAct = 0
         Do iIrrep = 0, nIrrep-1
            nAct = nAct + nAsh(iIrrep)
         End Do
         If (nAct.gt.0) lPSO=.true.
         nDSO = nDens
         Call Get_iScalar('SA ready',iGo)
         If (iGO.eq.1) lSA=.true.
         mIrrep=nIrrep
         Call ICopy(nIrrep,nBas,1,mBas,1)
         If (lPrint.and.lSA) Then
            Write (6,*)
            Write (6,'(2A)') ' Wavefunction type: State average ',
     &                         Method(1:6)
            Write (6,*)
         Else If (lPrint) Then
            Write (6,*)
            Write (6,'(2A)') ' Wavefunction type: ', Method
         End If
         Method='RASSCF  '
*                                                                      *
************************************************************************
*                                                                      *
      Else
         Write (6,*)
         Write (6,*) ' Wavefunction type:',Method
         Write (6,*) ' Illegal type of wave function!'
         Write (6,*) ' ALASKA can not continue'
         Write (6,*)
         Call Quit_OnUserError()
      End If
*
*...  Read the (non) variational 1st order density matrix
*...  density matrix in AO/SO basis
         nsa=1
         If (lsa) nsa=5
         Call GetMem('D0   ','Allo','Real',ipD0,nDens*nsa)
         Call GetMem('DVar ','Allo','Real',ipDVar,nDens*nsa)
CStart Molcas
         if (.not.gamma_mrcisd) then 
         Call Get_D1ao(ipD1ao,length)
         If ( length.ne.nDens ) Then
            Write (6,*) 'PrepP: length.ne.nDens'
            Write (6,*) 'length=',length
            Write (6,*) 'nDens=',nDens
            Call Abend()
         End If
         Call DCopy(nDens,Work(ipD1ao),1,Work(ipD0),1)
         Call Free_Work(ipD1ao)
         endif 
*
         Call Get_D1ao_Var(ipD1ao_Var,length)
         Call DCopy(nDens,Work(ipD1ao_Var),1,Work(ipDVar),1)
*        if (gamma_mrcisd) then
*         Call DCopy(nDens,Work(ipD1ao_Var),1,Work(ipD0),1)
*        endif 
         Call Free_Work(ipD1ao_Var)
*
         If (Method.eq.'UHF-SCF ' .or.
     &       Method.eq.'ROHF    ' .or.
     &       Method.eq.'Corr. WF' ) Then
            Call Get_D1sao(ipDS,Length)
            Call Get_D1sao_Var(ipDSVar,Length)
         Else
            Call GetMem('DS   ','Allo','Real',ipDS,nDens)
            Call GetMem('DSVar','Allo','Real',ipDSVar,nDens)
            Call FZero(Work(ipDS),nDens)
            Call FZero(Work(ipDSVar),nDens)
         End If
*
*   This is necessary for the ci-lag
*
CElse
c;      Call GetMem('DS   ','Allo','Real',ipDS,nDens)
c;      Call GetMem('DSVar','Allo','Real',ipDSVar,nDens)
c;      if(ncphf.ne.0) then
c;        call fzero(Work(ipD0),ndens)
c;         lPSO=.false.
c;        goto 1000
c;      else
c;        call fmove(d1molpro,Work(ipD0),nDens)
c;      end if
CEnd
*
*     Unfold density matrix
*
************ columbus interface ****************************************
*do not modify the effective density matrices and fock matrices   
      if (.not.gamma_mrcisd) then
      ij = -1
      Do 10 iIrrep = 0, nIrrep-1
         Do 11 iBas = 1, nBas(iIrrep)
            Do 12 jBas = 1, iBas-1
               ij = ij + 1
               Work(ipDVar +ij) = Half*Work(ipDVar +ij)
                  Work(ipD0   +ij) = Half*Work(ipD0   +ij)
                  Work(ipDS   +ij) = Half*Work(ipDS   +ij)
                  Work(ipDSVar+ij) = Half*Work(ipDSVar+ij)
 12         Continue
            ij = ij + 1
 11      Continue
 10   Continue
      endif
      If (iPrint.ge.99) Then
         RlxLbl='D1AO    '
         Call PrMtrx(RlxLbl,iD0Lbl,iComp,ipD0)
         RlxLbl='D1AO-Var'
         Call PrMtrx(RlxLbl,iD0Lbl,iComp,ipDVar)
         RlxLbl='DSAO    '
         Call PrMtrx(RlxLbl,iD0Lbl,iComp,ipDS)
         RlxLbl='DSAO-Var'
         Call PrMtrx(RlxLbl,iD0Lbl,iComp,ipDSVar)
      End If
*
*...  Get the MO-coefficients
************ columbus interface ****************************************
*     not that the columbus mcscf MO coefficients have been written
*     to the RUNFILE !

         If (Method.eq.'UHF-SCF ' .or.
     &       Method.eq.'ROHF    ' .or.
     &       Method.eq.'Corr. WF'      ) Then
            nsa=2
         Else
            nsa=1
            If (lsa) nsa=2
         End If
         Call GetMem('CMO','Allo','Real',ipCMO,nsa*mCMO)
         ipCMOa=ipCMO
         ipCMOb=ipCMO+(nsa-1)*mCMO
CStart Molcas
         Call Get_CMO(ipTemp,nTemp)
         Call DCopy(nTemp,Work(ipTemp),1,Work(ipCMO),1)
         Call Free_Work(ipTemp)
CElse
c;      call fmove(orbmolpro,Work(ipCMO),mCMO)
CEnd
         If (iPrint.ge.99) Then
            ipTmp1 = ipCMO
            Do iIrrep = 0, nIrrep-1
               Call RecPrt(' CMO''s',' ',
     &                     Work(ipTmp1),nBas(iIrrep),
     &                     nBas(iIrrep))
               ipTmp1 = ipTmp1 + nBas(iIrrep)**2
            End Do
         End If
*
*
*...  Get additional information in the case of a RASSCF wave function
*...  Get the number of inactive, active and frozen orbitals
CStart Molcas
************ columbus interface ****************************************
*  no need for MRCI gradient
         If (.not.lpso .or. gamma_mrcisd ) Goto 1000
         Call Get_iScalar('nSym',i)
         Call Get_iArray('nIsh',nIsh,i)
         Call Get_iArray('nAsh',nAsh,i)
         Call Get_iArray('nFro',nFro,i)
         If (iPrint.ge.99) Then
            Write (6,*) ' nISh=',nISh
            Write (6,*) ' nASh=',nASh
            Write (6,*) ' nFro=',nFro
         End If
CElse
c;      call imove(iclos,nISh,8)
c;      call imove(iact,nASh,8)
c;      call izero(nFro,8)
CEnd
         nAct = 0
         nTst = 0
         Do iIrrep = 0, nIrrep-1
            nAct = nAct + nAsh(iIrrep)
            nTst = nTst + nFro(iIrrep)
         End Do
         If (nTst.ne.0) Then
            Write (6,*)
            Write (6,*) ' No frozen orbitals are allowed!'
            Write (6,*) ' ALASKA can not continue'
            Write (6,*)
            Call Quit_OnUserError()
         End If
*
*...  Get the one body density for the active orbitals
         nG1 = nAct*(nAct+1)/2
         nsa=1
         If (lsa) nsa=2
         Call GetMem(' G1 ','Allo','Real',ipG1,nG1*nsa)
CStart Molcas
         Call Get_D1MO(ipTemp,nTemp)
         Call DCopy(nTemp,Work(ipTemp),1,Work(ipG1),1)
         Call Free_Work(ipTemp)
CElse
c;      call fmove (den1,Work(ipG1),nG1)
CEnd
         If (iPrint.ge.99) Call TriPrt(' G1',' ',Work(ipG1),nAct)
*
*...  Get the two body density for the active orbitals
         nG2 = nG1*(nG1+1)/2
         nsa=1
         if (lsa) NSA=2
         Call GetMem(' G2 ','Allo','Real',ipG2,nsa*nG2)
CStart Molcas
         Call Get_P2MO(ipTemp,nTemp)
         Call DCopy(nTemp,Work(ipTemp),1,Work(ipG2),1)
         Call Free_Work(ipTemp)
         If (iPrint.ge.99) Call TriPrt(' G2',' ',Work(ipG2),nG1)
         If (lsa) Then

*  CMO1 Ordinary CMO's
*
*  CMO2 CMO*Kappa
*
           Call Get_LCMO(ipLCMO,Length)
           Call DCopy(Length,Work(ipLCMO),1,Work(ipCMO+mcmo),1)
           Call Free_Work(ipLCMO)
           If (iPrint.ge.99) Then
            ipTmp1 = ipCMO+mcmo
            Do iIrrep = 0, nIrrep-1
               Call RecPrt('LCMO''s',' ',
     &                     Work(ipTmp1),nBas(iIrrep),
     &                     nBas(iIrrep))
               ipTmp1 = ipTmp1 + nBas(iIrrep)**2
            End Do
           End If
*
* P are stored as
*                            _                     _
*   P1=<i|e_pqrs|i> + sum_i <i|e_pqrs|i>+<i|e_pqrs|i>
*   P2=sum_i <i|e_pqrs|i>
*
           Call Get_PLMO(ipPLMO,Length)
           Call DCopy(Length,Work(ipPLMO),1,Work(ipG2+ng2),1)
           Call Free_Work(ipPLMO)
           Call Daxpy(ng2,1.0d0,Work(ipG2+ng2),1,Work(ipG2),1)
           If(iPrint.ge.99)Call TriPrt(' G2L',' ',Work(ipG2+ng2),nG1)
           If(iPrint.ge.99)Call TriPrt(' G2T',' ',Work(ipG2),nG1)
*
           Call Get_D2AV(ipD2AV,Length)
           If (ng2.ne.Length) Then
              Write (6,*) 'Prepp: D2AV, ng2.ne.Length!'
              Write (6,*) 'ng2,Length=',ng2,Length
              Call Abend()
           End If
           Call DCopy(Length,Work(ipD2AV),1,Work(ipG2+ng2),1)
           Call Free_Work(ipD2AV)
           If (iPrint.ge.99) Call TriPrt('G2A',' ',
     &                Work(ipG2+ng2),nG2)
*
*
*  Densities are stored as:
*
*       ipd0 AO:
*
*       D1 = <i|E_pq|i>
*
*       D2 = inactive density matrix
*                   _                 _
*       D3 = sum_i <i|E_pq|i>+<i|E_pq|i>
*
*       D4 = sum_i <i|E_pq|i>
*
*       D5 = sum_i sum_o k_po <i|E_oq|i> +k_oq <i|E_po|i>
*
*       G1 = <i|e_ab|i>
*       G2 = sum i <i|e_ab|i>
           Call Getmem('TMP','ALLO','REAL',ipt,2*ndens)
           Call Get_D1I(Work(ipCMO),Work(ipD0+ndens),Work(ipT),
     &                nish,nbas,nIrrep)

           call Get_D1A(Work(ipcmo),Work(ipg1),Work(ipD0),
     &                 nIrrep,nbas,nish,nash,ndens)
           Call DAXPY(ndens,1.0d0,Work(ipD0+ndens),1,Work(ipD0),1)
           If (iprint.gt.90)  Call PrMtrx('D0',iD0Lbl,iComp,ipD0)

*
           Call Get_DLMO(ipDLMO,Length)
           nG1 = nAct*(nAct+1)/2
           If ( length.ne.ng1 ) Then
              Write (6,*) 'PrepP: length.ne.nG1'
              Write (6,*) 'DLMO'
              Write (6,*) 'length=',length
              Write (6,*) 'nG1=',nG1
              Call Abend()
           End If
           Call DCopy(Length,Work(ipDLMO),1,Work(ipT),1)
           Call Free_Work(ipDLMO)
           call Get_D1A(Work(ipcmo),Work(ipT),Work(ipD0+2*ndens),
     &                 nIrrep,nbas,nish,nash,ndens)
*
*   This is necessary for the kap-lag
*
           Call Get_D1AV(ipD1AV,Length)
           If (ng1.ne.Length) Then
              Write (6,*) 'Prepp: D1AV, ng1.ne.Length!'
              Write (6,*) 'ng1,Length=',ng1,Length
              Call Abend()
           End If
           Call DCopy(Length,Work(ipD1AV),1,Work(ipg1+ng1),1)
           Call Free_Work(ipD1AV)
           Call Get_D1A(Work(ipcmo),Work(ipg1+ng1),Work(ipD0+3*ndens),
     &                 nIrrep,nbas,nish,nash,ndens)
           Call DAXPY(ndens,1.0d0,Work(ipD0+ndens),1,
     &                            Work(ipD0+3*ndens),1)
*
           Call Get_DLAO(ipDLAO,Length)
           Call DCopy(Length,Work(ipDLAO),1,Work(ipD0+4*ndens),1)
           Call Free_Work(ipDLAO)
           Call Getmem('TMP','FREE','REAL',ipt,ndens)
         End If
CElse
c;      call fmove (den2,Work(ipG2),nG2)
CEnd
         If (iPrint.ge.99) Call TriPrt(' G2',' ',Work(ipG2),nG1)
*
*...  Close 'RELAX' file
1000     Continue
*
*...  Epilogue, end
      Call qExit('PrepP')
      Return
      End

CStart Molpro
c;      SubRoutine PrepP1(orbmolpro,fockmolpro,d1molpro,den1,den2,
c;     >    methodmolpro,iclos,iact)
c;************************************************************************
c;*                                                                      *
c;* Object: handling of the 2nd order density matrix for UHF gradients.  *
c;*                                                                      *
c;* Called from: Alaska                                                  *
c;*                                                                      *
c;* Calling    : QEnter                                                  *
c;*              OpnOne                                                  *
c;*              GetMem                                                  *
c;*              RdOne                                                   *
c;*              PrMtrx                                                  *
c;*              ClsOne                                                  *
c;*              ErrOne                                                  *
c;*              QExit                                                   *
c;*                                                                      *
c;* Author     : F. Eckert; Routine modified from PrepP, April 97        *
c;************************************************************************
c;      Implicit Real*8 (A-H,O-Z)
c;#include "itmax.fh"
c;#include "info.fh"
c;      include "common/print.inc"
c;      include "common/real.inc"
c;      include "common/WrkSpc.inc"
c;      include "common/pso.inc"
c;      include "common/etwas.inc"
c;      Real*8 orbmolpro(*),d1molpro(*),fockmolpro(*),den1(*),den2(*)
c;      integer iclos(8),iact(8)
c;      Character *(*) methodmolpro
c;      Integer  nFro(0:7)
c;      Character*8 RlxLbl,Method
cstart .not.molpro
c;      Real*8 cTemp
c;      Equivalence (Method,cTemp)
cend
c;*
c;*...  Prologue
c;      iRout = 250
c;      iPrint = nPrint(iRout)
c;      nDens1 = 0
c;      Do 1 iIrrep = 0, nIrrep - 1
c;         nDens1 = nDens1 + nBas(iIrrep)*(nBas(iIrrep)+1)/2
c; 1    Continue
c;*
c;      Method = methodmolpro
c;*
c;*...  Read the variational 1st order density matrix
c;*...  density matrix in AO/SO basis
c;      Call GetMem('D01 ','Allo','Real',ipD01,nDens1)
c;      call fmove(d1molpro,Work(ipD01),nDens1)
c;*
c;*     Unfold density matrix
c;*
c;      ij = -1
c;      Do 10 iIrrep = 0, nIrrep-1
c;         Do 11 iBas = 1, nBas(iIrrep)
c;            Do 12 jBas = 1, iBas-1
c;               ij = ij + 1
c;               Work(ipD01+ij) = Half*Work(ipD01+ij)
c; 12         Continue
c;            ij = ij + 1
c; 11      Continue
c; 10   Continue
c;      call imove(iclos,nISh1,8)
c;      call imove(iact,nASh1,8)
c;      call izero(nFro,8)
c;         nAct = 0
c;         nTst = 0
c;         Do 100 iIrrep = 0, nIrrep-1
c;            nAct = nAct + nAsh1(iIrrep)
c;            nTst = nTst + nFro(iIrrep)
c; 100     Continue
c;         If (nTst.ne.0) Then
c;            Write (*,*)
c;            Write (*,*) ' No frozen orbitals are allowed!'
c;            Write (*,*) ' ALASKA can not continue'
c;            stop 'frozen orbital error'
c;         End If
c;*
c;*...  Get the MO-coefficients
c;      Call GetMem('CMO1','Allo','Real',ipCMO1,mCMO)
c;      call fmove(orbmolpro,Work(ipCMO1),mCMO)
c;         If (iPrint.ge.99) Then
c;            ipTmp1 = ipCMO1
c;            Do 300 iIrrep = 0, nIrrep-1
c;               Call RecPrt(' CMO1''s',' ',
c;     &                     Work(ipTmp1),nBas(iIrrep),
c;     &                     nBas(iIrrep))
c;               ipTmp1 = ipTmp1 + nBas(iIrrep)**2
c; 300        Continue
c;         End If
c;*
c;*...  Get the one body density for the active orbitals
c;         nG11 = nAct*(nAct+1)/2
c;         Call GetMem(' G11','Allo','Real',ipG11,nG11)
c;      call fmove (den1,Work(ipG11),nG11)
c;         If (iPrint.ge.99) Call TriPrt(' G11',' ',Work(ipG11),nAct)
c;*
c;*...  Get the two body density for the active orbitals
c;         nG21 = nG11*(nG11+1)/2
c;         Call GetMem(' G21','Allo','Real',ipG21,nG21)
c;      call fmove (den2,Work(ipG21),nG21)
c;      If (iPrint.ge.99) Call TriPrt(' G21',' ',Work(ipG21),nG11)
c;*
c;*...  Close 'RELAX' file
c;1000  iRc=-1
c;*
c;      Return
c;      End
cEnd
      Subroutine Get_D1I(CMO,D1It,D1I,nish,nbas,nsym)
      Implicit Real*8 (A-H,O-Z)
      Dimension CMO(*), D1I(*),D1It(*)
      Parameter ( Zero=0.0d0 , Two=2.0d0 )
      Integer nbas(nsym),nish(nsym)

      iOff1 = 0
      Do iSym = 1,nSym
        iBas = nBas(iSym)
        iOrb = nIsh(iSym)
        If ( iBas.ne.0 ) then
          iOff2 = iOff1
          Do i = 1,iBas
            Do j = 1,iBas
              Sum = Zero
              Do k = 0,iOrb-1
                Sum = Sum + Two * CMO(iOff1+k*iBas+i)
     &                          * CMO(iOff1+k*iBas+j)
              End Do
              D1I(iOff2 + j) = Sum
            End Do
            iOff2 = iOff2 + iBas
          End Do
          iOff1 = iOff1 + iBas*iBas
        End If
      End Do
      Call Fold2(nsym,nBas,D1I,D1It)
      Return
      End

      Subroutine Get_D1A(CMO,D1A_MO,D1A_AO,
     &                    nsym,nbas,nish,nash,ndens)


      Implicit Real*8 (A-H,O-Z)

      Dimension CMO(*) , D1A_MO(*) , D1A_AO(*)
      Integer nbas(nsym),nish(nsym),nash(nsym)

      Include 'WrkSpc.inc'
      itri(i,j)=Max(i,j)*(Max(i,j)-1)/2+Min(i,j)

      Call qEnter('Get_D1A')

      Zero = 0.0d0

      iOff1 = 1
      iOff2 = 1
      iOff3 = 1
      ii=0
      Call GetMem('Scr1','Allo','Real',ip1,2*ndens)
      Do iSym = 1,nSym
        iBas = nBas(iSym)
        iAsh = nAsh(iSym)
        iIsh = nIsh(iSym)
        Call dCopy(iBas*iBas,Zero,0,Work(ip1-1+iOff3),1)
        If ( iAsh.ne.0 ) then
          Call GetMem('Scr1','Allo','Real',iTmp1,iAsh*iAsh)
          Call GetMem('Scr2','Allo','Real',iTmp2,iAsh*iBas)
          ij=0
          Do i=1,iash
           Do j=1,iash
            Work(iTmp1+ij)=D1A_MO(itri(i+ii,j+ii))
            ij=ij+1
           End do
          End do
          ii=ii+iash
          Call DGEMM('N','T',
     &                iBas,iAsh,iAsh,
     &                1.0d0,CMO(iOff2+iIsh*iBas),iBas,
     &                Work(iTmp1),iAsh,
     &                0.0d0,Work(iTmp2),iBas)
          Call DGEMM('N','T',
     &                iBas,iBas,iAsh,
     &                1.0d0,Work(iTmp2),iBas,
     &                CMO(iOff2+iIsh*iBas),iBas,
     &                0.0d0,Work(ip1-1+iOff3),iBas)
          Call GetMem('Scr2','Free','Real',iTmp2,iAsh*iBas)
          Call GetMem('Scr1','Free','Real',iTmp1,iAsh*iAsh)
        End If
        iOff1 = iOff1 + (iAsh*iAsh+iAsh)/2
        iOff2 = iOff2 + iBas*iBas
        iOff3 = iOff3 + iBas*iBas
      End Do
      Call Fold2(nsym,nBas,work(ip1),D1A_AO)
      Call GetMem('Scr1','FREE','Real',ip1,ndens)
      Call qExit('Get_D1A')
      Return
      End


      Subroutine Fold2(nSym,nBas,A,B)

      Implicit Real*8 (A-H,O-Z)

      Dimension nBas(*) , A(*) , B(*)

      iOff1 = 0
      iOff2 = 0
      Do iSym = 1, nSym
        mBas = nBas(iSym)
        Do iBas= 1, mBas
          Do jBas = 1 , iBas-1
            B(iOff2+jBas) =   A(iOff1+jBas)
          End Do
          B(iOff2+iBas) =  A(iOff1+iBas)
          iOff1 = iOff1 + mBas
          iOff2 = iOff2 + iBas
        End Do
      End Do

      Return
      end
************ columbus interface ****************************************
*read table of contents for gamma file

        subroutine read_lgtoc(lgtoc,gtoc,n)
        integer n,lgtoc
        real*8 gtoc(n)
          read(lgtoc) gtoc
        return
         end

