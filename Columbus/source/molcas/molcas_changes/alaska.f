!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
*fordeck alaska $Revision: 2.4 $             
************************************************************************
*                                                                      *
* Copyright 1990, 1991, and 1992 by                                    *
* Roland Lindh                                                         *
* Dept. of Theoretical Chemistry                                       *
* University of Lund, SWEDEN                                           *
*                                                                      *
* and                                                                  *
*                                                                      *
* IBM                                                                  *
*                                                                      *
* No part of this code may be copied or redistributed without the      *
* written permission of the copyright owner.                           *
* The copyright owner does not take any responsibility for any         *
* errors in the code or documentation.                                 *
*                                                                      *
************************************************************************
Cstart Molcas
      Subroutine Alaska(LuSpool,ireturn)
celse
c;      Subroutine Alaska(iclos,iclos1,iact,iact1,den1,den11,den2,den21,
c;     >                  d1ao,d1ao1,d1ao2,fock,fock1,orb,orb1,
c;     >                  methodmolpro,one_only)
cend
************************************************************************
*                                                                      *
*  Object: Driver for the one and two electron integral gradient       *
*          program ALASKA.                                             *
*                                                                      *
*          Alaska is a derivative code of Seward 3.1.                  *
*                                                                      *
* Called from: None                                                    *
*                                                                      *
* Calling    : QEnter                                                  *
*              XuFlow (IBM)                                            *
*              SetUp0                                                  *
*              GetMem                                                  *
*              GetInf                                                  *
*              Inputg                                                  *
*              DrvN1                                                   *
*              Drvh1                                                   *
*              PrepP                                                   *
*              Drvg1                                                   *
*              CloseP                                                  *
*                                                                      *
*  Author: Roland Lindh, IBM Almaden Research Center, San Jose, CA     *
*          July '89 - May '90                                          *
*                                                                      *
* Copyright 1990 IBM                                                   *
*                                                                      *
*          Roland Lindh, Dept. of Theoretical Chemistry, University of *
*          Lund, SWEDEN. Modified to gradient calculations September   *
*          1991 - February 1992.                                       *
*                                                                      *
* (c) 1995, Dept. of Theoretical Chemistry, Univeristy of Lund, Sweden *
************************************************************************
      Implicit Real*8 (A-H,O-Z)
      External MPP, RF_On
      Include 'real.inc'
#include "itmax.fh"
#include "info.fh"
      Include 'WrkSpc.inc'
      Include 'sphere.inc'
      Include 'herrw.inc'
      Include 'print.inc'
      Include 'disp.inc'
      Include 'rctfld.inc'
      Include 'nsd.inc'
      Include 'setup.inc'
      Include 'columbus_gamma.inc'
#ifdef _MOLCAS_
      Logical OldTst, DoRys, MPP, Node0, RF_On
#else
      Logical OldTst, DoRys, MPP, Use_Drvg1, Node0, RF_On
#endif
cstart molpro
c;      logical one_only
c;      include "common/lmp2gr"
c;      character *(*) methodmolpro
c;      dimension iclos(8),iclos1(8),iact(8),iact1(8),
c;     >          den1(*),d1ao(*),den2(*),fock(*),orb(*),
c;     >          den11(*),den21(*),d1ao1(*),d1ao2(*),
c;     >          fock1(*),orb1(*)
c;      common/cprint/ iprin_mol(40)
c;      common/codeplus/luhf
cend
************ columbus interface ****************************************
        character*8 Method         
        Integer lcartgrd
*                                                                      *
************************************************************************
*                                                                      *
      Call CWTime(TCpu1,TWall1)
*                                                                      *
************************************************************************
*                                                                      *
*     Prologue
*                                                                      *
************************************************************************
*                                                                      *
      iRout=1
*                                                                      *
************************************************************************
*                                                                      *
*     Print program header
*
#ifdef _MOLCAS_
      Call qEnter('Alaska')
      Call Mem_Info('ALASKA')
#else
      qtimes=iprin_mol(39).ge.2
      if(qtimes)  Call qEnter('Alaska')
#endif
*                                                                      *
************************************************************************
*                                                                      *
*     Get the input information as Seward dumped on INFO.
*
#ifdef _MOLCAS_
      nDiff=1
      DoRys=.True.
      Call IniSew(Info,DoRys,nDiff)
      If (RF_On()) Then
         If (NonEq_Ref) Then
            Write (6,*) 'NonEq=.True., invalid option'
            Call Abend
         End If
         Call Init_RctFld(.False.,iCharge_Ref)
      End If
#endif
*
      OldTst = Test
*                                                                      *
************************************************************************
*                                                                      *
*     Input specific for the gradient calculation.
*
      Call Inputg(LuSpool,Info)
CStart Molpro
c;      Onenly = one_only
CEnd
*
*---- Since the input has changed some of the shell information
*     regenerate the tabulated shell information.
*
      Call Set_Basis_Mode('Valence')
      Call Def_Shells(iWork(ipiSD),nSD,mSkal)
      iPrint=nPrint(iRout)
      If (iPrint.ge.99) Call GetMem(' LIST ','LIST','REAL',iDum,iDum)
      Call GetMem('Grad','Allo','Real',ipGrad,lDisp(0))
      Call GetMem('Temp','Allo','Real',ipTemp,lDisp(0))
      Call DCopy(lDisp(0),Zero,0,Work(ipGrad),1)
CStart Molcas
*
*     remove LuSpool
*
       Call Close_LuSpool(LuSpool)
CEnd
*
*-----Start computing the gradients
*                                                                      *
************************************************************************
*                                                                      *
*     Compute nuclear contributions.
*
#ifdef _MOLCAS_
      If (Node0().or.HF_Force) Then
#else
      Use_Drvg1=ncphf.le.2
      If (Node0().or..not.Use_Drvg1.or.HF_Force) Then
#endif
       if (NO_NUC) then
         write(6,*) 'Skipping Nuclear Charge Contribution'
       else
         Call DrvN1(Work(ipGrad),Work(ipTemp),lDisp(0))
         If (iPrint.ge.15)
     &      Call PrGrad(' Total Nuclear contribution',
     &                   Work(ipGrad),lDisp(0),lIrrep,ChDisp,iPrint)
       endif
      End If
*                                                                      *
************************************************************************
*                                                                      *
      If (Test) Go To 999
*                                                                      *
************************************************************************
*                                                                      *
*-----Compute contribution due to the derivative of the one-
*     electron hamiltonian and the contribution due the re-
*     normalization.
*
#ifdef _MOLCAS_
      If (.Not.MPP().or.Onenly) Then
         Call Drvh1(Work(ipGrad),Work(ipTemp),lDisp(0))
      End If ! .Not.MPP().or.Onenly
#else
      If (.Not.MPP().or.Onenly.or..not.Use_Drvg1) Then
         Call Drvh1(Work(ipGrad),Work(ipTemp),lDisp(0),
     &                      fock,fock1,d1ao,d1ao1)
      End If ! .Not.MPP().or.Onenly
#endif
*                                                                      *
************************************************************************
*                                                                      *
*     Compute the DFT contribution to the gradient
*
#ifdef _MOLCAS_
      Call DrvDFTg(Work(ipGrad),Work(ipTemp),lDisp(0))
#endif
*                                                                      *
************************************************************************
*                                                                      *
      If (Onenly) Go To 998
*                                                                      *
************************************************************************
*                                                                      *
*-----Compute contribution due to 2-electron integrals.
*
      Call GetMem('MemHid','ALLO','REAL',idum,MemHid)
*
CStart Molcas
*                                                                      *
************************************************************************
*                                                                      *
      If (Cholesky.or.Do_RI) Then
         If (Cholesky) Then
            If (iPrint.ge.6) Write (6,*) 'Cholesky-ERI gradients!'
         Else
            If (iPrint.ge.6) Write (6,*) 'RI-ERI gradients!'
         End If
         Call Drvg1_RI (Work(ipGrad),Work(ipTemp),lDisp(0))
      Else
         If (iPrint.ge.6) Write (6,*) 'Conventional ERI gradients!'
         Call Drvg1    (Work(ipGrad),Work(ipTemp),lDisp(0))
      End If
*
      Call DScal(lDisp(0),Half,Work(ipTemp),1)
      If (iPrint.ge.15) Call PrGrad(' Two-electron contribution',
     &          Work(ipTemp),lDisp(0),lIrrep,ChDisp,iPrint)
*
*-----Accumulate contribution to the gradient
*
      Call GR_DArray(Work(ipGrad),lDisp(0))
      Call DaXpY(lDisp(0),One,Work(ipTemp),1,
     &                        Work(ipGrad),1)
*
*                                                                      *
************************************************************************
*                                                                      *
CElse
c;      Call PrepP(orb,fock,d1ao,den1,den2,methodmolpro,iclos,iact)
c;      if(luhf.ne.0)
c;     >  Call PrepP1(orb1,fock1,d1ao1,den11,den21,methodmolpro,
c;     >              iclos1,iact1)
c;c     if (ncphf.eq.3) then
c;c        Call Drvgr(Work(ipTemp),lDisp(0))
c;c        call prttimes
c;c     else if(ncphf.ge.4) then
c;      if(ncphf.ge.3) then
c;         call t2tran_drv(orb,orb1,d1ao,d1ao1,d1ao2,fock,
c;     >                   Work(ipTemp),lDisp(0))
c;      else
c;         Call Drvg1(Work(ipGrad),Work(ipTemp),lDisp(0),
c;     &                   fock,fock1,d1ao,d1ao1)
c;      endif
c;      Call CloseP
c;      If (Use_Drvg1) Call GR_DArray(Work(ipTemp),lDisp(0))
c;      Call DScal(lDisp(0),Half,Work(ipTemp),1)
c;      If (iPrint.ge.15) Call PrGrad(' Two-electron contribution',
c;     &          Work(ipTemp),lDisp(0),lIrrep,ChDisp,iPrint)
c;*
c;*-----Accumulate contribution to the gradient
c;*
c;      If (Use_Drvg1) Call GR_DArray(Work(ipGrad),lDisp(0))
c;      Call DaXpY(lDisp(0),One,Work(ipTemp),1,
c;     &                        Work(ipGrad),1)
c;*
CEnd
*
      Call GetMem('MemHid','Free','REAL',idum,MemHid)
 998  Continue
*                                                                      *
************************************************************************
*                                                                      *
      Call CWTime(TCpu2,TWall2)
      Call SavTim(7,TCpu2-TCpu1,TWall2-TWall1)
*                                                                      *
************************************************************************
*                                                                      *
*-----Apply the translational and rotational invariance of
*     the energy.
*
      If (TRSymm) Then
         If (iPrint.ge.99) Then
            Call PrGrad(
     &       ' Molecular gradients (no TR) ',
     &           Work(ipGrad),lDisp(0),lIrrep,ChDisp,iPrint)
            Call RecPrt(' The A matrix',' ',Work(ipAm),
     &                  lDisp(0),lDisp(0))
         End If
         Call DCopy(lDisp(0),Work(ipGrad),1,Work(ipTemp),1)
CStart blas4
c;         Call DGeMV_X('N',lDisp(0),lDisp(0),
c;     &              One,Work(ipAm),lDisp(0),
c;     &              Work(ipTemp),1,
c;     &              Zero,Work(ipGrad),1)
Celse

         Call DGeMV('N',lDisp(0),lDisp(0),
     &              One,Work(ipAm),lDisp(0),
     &              Work(ipTemp),1,
     &              Zero,Work(ipGrad),1)
CEnd
         Call GetMem('AMtrx','Free','Real',ipAm,lDisp(0)**2)
      End If ! TRSymm
*                                                                      *
************************************************************************
*                                                                      *
*-----Equivalence option
*
      If (lEq) Then
         Do i = 1, lDisp(0)
            If (IndxEq(i).ne.i) Work(ipGrad-1+i) =
     &         Work(ipGrad-1+IndxEq(i))
         End Do  
      End If ! lEq
*                                                                      *
************************************************************************
*                                                                      *
 999  Continue
*                                                                      *
************************************************************************
*                                                                      *
#ifdef _MOLCAS_
      nCnttp_Valence=0
      Do iCnttp = 1, nCnttp
         If (AuxCnttp(iCnttp)) Go To 1999
         nCnttp_Valence = nCnttp_Valence+1
      End Do
 1999 Continue
*
      If (iPrint.ge.4) then
         If (HF_Force) Then
            Call PrGrad('Hellmann-Feynman Forces ',
     &                 Work(ipGrad),lDisp(0),lIrrep,ChDisp,iPrint)
         Else
            Call PrGrad(' Molecular gradients',
     &                    Work(ipGrad),lDisp(0),lIrrep,ChDisp,iPrint)
         End If
      End If
      Call Add_Info('Grad',Work(ipGrad),lDisp(0),6)
*
      Call Get_cArray('Relax Method',Method,8) 
*---- Molcas format
*
*-----Write gradient to runfile.
*
*     Save the gradient
*
      Call Get_iScalar('Unique atoms',nsAtom)
      l1 = 3*nsAtom
      Call GetMem('RELAX','Allo','Real',ipRlx,l1)
      mdc = 0
      ndc = 0
      Do iCnttp = 1, nCnttp_Valence
*
*        Skip gradients for pseudo atoms
*
         If (pChrg(iCnttp).or.nFragType(iCnttp).gt.0.or.
     &       FragCnttp(iCnttp)) Then
            mdc=mdc+nCntr(iCnttp)
         Else
            Do iCnt = 1, nCntr(iCnttp)
               mdc=mdc+1
               ndc=ndc+1
               Do iCar = 1, 3
                  If (InxDsp(mdc,iCar).ne.0) Then
                     temp = Work(ipGrad-1+InxDsp(mdc,iCar))
                     Work(ipRlx-1+(ndc-1)*3+iCar) = temp
                  Else
*
*                    Put in explicit zero if gradient is zero
*                    by symmetry.
*
                     Work(ipRlx-1+(ndc-1)*3+iCar) = Zero
                  End If
               End Do
            End Do
         End If
      End Do  
      If (HF_Force) Then
         Call Put_dArray('HF-forces',Work(ipRlx),l1)
      Else
         Call Put_Grad(Work(ipRlx),l1)
      End If
      Call GetMem('RELAX','Free','Real',ipRlx,l1)
#else
*
*-----Molpro 
*
      Call TrGrd_Alaska(Work(ipGrad),ldisp(0))
*
#endif

      
************ columbus interface ****************************************
* print full cartesian gradient in Columbus format
* 
       if (Method.eq.'MR-CISD ') then                                                  
         Call TrGrd_Alaska(Work(ipGrad),ldisp(0),.true.)                         
       endif                                            


*
*-----At the end of the calculation free all memory to check for
*     corruption of the memory.
*
      Call GetMem('Temp','Free','Real',ipTemp,lDisp(0))
      Call GetMem('Grad','Free','Real',ipGrad,lDisp(0))
*
      If (iPrint.ge.99) Then
         Call GetMem(' LIST ','LIST','REAL',iDum,iDum)
      End If
*
CStart Molcas
*
*     Epilogue
*
      Call ClsSew
      Call qExit('Alaska')
*
      If (iPrint.ge.6) Then
         Call qStat(' ')
         Call FastIO('STATUS')
      End If
*
      If (Test) Then
         ireturn=20
      Else
         ireturn=0
      End If
      return
CElse
c;      if(qtimes) then
c;        Call qExit('Alaska')
c;        Call qStat(' ')
c;      end if
c;      Return
cend
      End

      SubRoutine TrGrd_Alaska(GradIn,nGrad,columbus)
********************************************************************
*								   *
*      Transforms a symmetry adapted gradient to unsymmetric  form *
*								   *
*       Written by Anders Bernhardsson				   *
*       960427							   *
*								   *
********************************************************************
      Implicit Real*8(a-h,o-z)
      parameter (tol=1d-8)
#include "itmax.fh"
#include "info.fh"
      Include 'disp.inc'
      Include 'real.inc'
      Include 'WrkSpc.inc'
CStart Molcas
#include <SysDef.fh>
      Real*8 CGrad(3,MxAtom)
CElse
c;      include "common/cgeom"
c;      common /savegr/ grsave
c;      common /grad/ de(3,maxcen)
CEnd
      dimension GradIn(nGrad),A(3)
      Logical TF,TstFnc,columbus
      TF(mdc,iIrrep,iComp) = TstFnc(iOper,nIrrep,iCoSet(0,0,mdc),
     &                       nIrrep/nStab(mdc),iChTbl,iIrrep,iComp,
     &                       nStab(mdc))
      mdc=0
      iIrrep=0
*
CStart Molcas
      Call DCopy(3*MxAtom,Zero,0,CGrad,1)
      iCen=0
Celse
c;      call fzero(de,3*maxcen)
CEnd
      nCnttp_Valence=0
      Do iCnttp = 1, nCnttp
         If (AuxCnttp(iCnttp)) Go To 999
         nCnttp_Valence = nCnttp_Valence+1
      End Do
 999  Continue
*
      Do iCnttp=1,nCnttp_Valence
         ixyz = ipCntr(iCnttp)
         Do iCnt=1,nCntr(iCnttp)
            mdc=mdc+1
            Call DCopy(3,Work(ixyz),1,A,1)
            Do iCo=0,nIrrep/nStab(mdc)-1
               kop=iCoSet(iCo,0,mdc)
               nDispS = IndDsp(mdc,iIrrep)
               A1=DBLE(iPrmt(NrOpr(kop,iOper,nIrrep),1))*A(1)
               A2=DBLE(iPrmt(NrOpr(kop,iOper,nIrrep),2))*A(2)
               A3=DBLE(iPrmt(NrOpr(kop,iOper,nIrrep),4))*A(3)
CStart Molcas
               iCen=iCen+1
CElse
c;c...       seek molpro atom number
c;           do iCen=1,nCen
c;           if ((A1-rr(1,iCen))**2+(A2-rr(2,iCen))**2+(A3-rr(3,iCen))**2
c;     >           .le. tol ) goto 55
c;           end do
c;           write (6,*) 'TrGrd_Alaska programming error'
c;           call fehler
c;           Call Abend()
c;55         continue
CEnd
               Do iCar=0,2
                  iComp = 2**iCar
                  If ( TF(mdc,iIrrep,iComp)) Then
                     nDispS = nDispS + 1
                     XR=DBLE(iPrmt(NrOpr(kop,iOper,nIrrep),icomp))
CStart Molcas
                     CGrad(iCar+1,iCen)=XR*GradIn(nDispS)
CElse
c;                     de(iCar+1,iCen)=XR*GradIn(nDispS)
CEnd
                  End If
               End Do
            End Do
         End Do
         ixyz=ixyz+3
      End Do
*
CStart Molcas
C     Call RecPrt('CGrad',' ',CGrad,3,iCen)

************ columbus interface ****************************************
* print full cartesian gradient in Columbus format on cartgrd
      if (columbus) then
      lcartgrd=60
      lcartgrd=isFreeUnit(lcartgrd)  
      open(unit=lcartgrd,file='cartgrd', form='formatted')
      DO 300 IATOM = 1,icen 
         write (60,1010) (cgrad(j,iatom), j=1,3)
  300 CONTINUE
      close(lcartgrd)
 1010 format (3d15.6)
      endif 
CElse
c;      name=grsave
c;      ifil=nint((grsave-name)*10)
c;      call sreibw (de,3*maxcen,ifil,name,0)
CEnd
      Return
      End
