!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
c
c  MOLCAS WRAPPER FOR READING INTEGRAL FILES ETC.
c


c#################################################################
       logical function molcas_avail()
*@ifdef molcas
        molcas_avail=.true.
*@else
*        molcas_avail=.false.
*@endif
       return
       end

*@ifndef molcas 
*c################MOLCAS STUBS######################################
**
*        subroutine inisew
*        call bummer('dummy inisew should never be called',0,2)
*        return
*        end
*        subroutine setup_ints 
*        call bummer('dummy setup_ints should never be called',0,2)
*        return
*        end
*       
*       
*        subroutine inimem 
*        call bummer('dummy inimem should never be called',0,2)
*        return
*        end
**
*        subroutine daclos
*        call bummer('dummy daclos should never be called',0,2)
*        return
*        end
*        
*        subroutine daname_mf
*        call bummer('dummy daname_mf should never be called',0,2)
*        return
*        end
**
*        subroutine put_cmo 
*        call bummer('dummy put_cmo should never be called',0,2)
*        return
*        end
**
*        subroutine mo2out_molcas 
*        call bummer('dummy mo2out_molcas should never be called',0,2)
*        return
*        end
**
*        subroutine put_dscalar
*        call bummer('dummy put_dscalar should never be called',0,2)
*        return
*        end
**
*        subroutine mo2out_molcas_fin
*        call bummer('dummy mo2out_molcas_fin should never be called',
*     .      0,2)
*        return
*        end
**
*        subroutine mo2out_molcas_init
*        call bummer('dummy mo2out_molcas_init should never be called',
*     .      0,2)
*        return
*        end
**
**
*       
**
**
*    
*        subroutine ddafile
*        call bummer('dummy ddafile should never be called',0,2)
*        return
*        end
**
*        subroutine link_it 
*        call bummer('dummy link_it should never be called',0,2)
*        return
*        end
**
*        subroutine getenvinit
*        call bummer('dummy getenvinit should never be called',0,2)
*        return
*        end
**
*        
*        subroutine fioinit 
*        call bummer('dummy fioinit should never be called',0,2)
*        return
*        end
**
*        subroutine namerun(dum)     
*        character*(*) dum
*        call bummer('dummy namerun should never be called',0,2)
*        return
*        end
**
*        subroutine get_dscalar (dum1,dum2)
*        character*(*) dum1
*        real*8 dum2
*        call bummer('dummy get_dscalar should never be called',0,2)
*        return
*        end
**
*        subroutine mcget_iscalar (dum1,dum2)
*        character*(*) dum1
*        integer dum2
*        call bummer('dummy mcget_iscalar should never be called',0,2)
*        return
*        end
**
*        subroutine mcget_iarray (dum1,dum2,dum3)
*        character*(*) dum1
*        integer dum2,dum3(dum2)
*        call bummer('dummy mcget_iarray should never be called',0,2)
*        return
*        end
**
*        subroutine mcget_carray(dum1,dum2,dum3)
*        character*(*) dum1
*        integer dum3
*        character*3 dum2(dum3)
*        call bummer('dummy mcget_carray should never be called',0,2)
*        return
*        end
*        
*        subroutine mcget_darray (dum1,dum2,dum3)
*        character*(*) dum1
*        integer dum3
*        real*8 dum2(dum3)
*        call bummer('dummy mcget_iarray should never be called',0,2)
*        return
*        end
*        
*        subroutine mcput_darray (dum1,dum2,dum3)
*        character*(*) dum1
*        integer dum3
*        real*8 dum2(dum3)
*        call bummer('dummy mcput_darray should never be called',0,2)
*        return
*        end
*        subroutine mcput_iscalar (dum1,dum2)
*        character*(*) dum1
*        integer dum2
*        call bummer('dummy mcput_iscalar should never be called',0,2)
*        return
*        end
**
*        subroutine mcput_carray 
*        call bummer('dummy mcput_carray should never be called',0,2)
*        return
*        end
**
*        
*        subroutine mcopnord(dum1,dum2,name,num3)
*        character*(*) name
*        integer dum1,dum2,dum3
*        call bummer('dummy mcopnord should never be called',0,2)
*        return
*        end
**
*        subroutine mcrdord(i1,i2,n1,n2,n3,n4,buffer,i3,i4)
*        integer*4 i1,i2,n1,n2,n3,n4,i3,i4
*        real*8 buffer
*        call bummer('dummy mcrdord should never be called',0,2)
*        return
*        end
* 
*       Subroutine RdOneXX(i_rc,i_Option,InLab,
*     .         i_Comp,Data,i_SymLab,ret_data,fname)
*       Implicit Integer (A-Z)
*        call bummer('dummy rdonexx should never be called',0,2)
*        return
*        end
**
**
*        
**
*C####################### MOLCAS INTERFACE ############################
*@else
*
*@ifdef molcas_ext
*        subroutine getenvinit
*        call getenvinit_()
*        return 
*        end
*        subroutine fioinit
*         call fioinit_()
*        return
*        end
*        subroutine NameRun( file)
*        character*(*) file
*        call NameRun_(file)
*        return
*        end
*       
*        subroutine get_dscalar(name,val)
*        character*(*) name
*        real*8 val
*        call get_dscalar_(name,val)
*        return
*        end
*        subroutine dcopy_(n,x,incx,y,incy)
*        call dcopy(n,x,incx,y,incy)
*        return
*        end
*        subroutine put_dscalar(name,val)
*         character*(*) name
*         real*8 val
*         call put_dscalar_(name,val)
*         return
*        end
**
*        subroutine daclos(iunit)
*         integer iunit
*         call daclos_(iunit)
*         return
*        end
**
*        subroutine daname_mf(iunit,name)
*         integer iunit
*         character*(*) name
*         call daname_mf_(iunit,name)
*         return
*        end
**
*        subroutine put_cmo(mos,len)
*         integer len
*         real*8 mos(len)
*         call put_cmo_(mos,len)
*        return
*        end
**
*        subroutine drvk2_(l1,l2,l3)
*        return
*        end
**
**
*        subroutine cmpctr_(l1,l2,l3)
*        return
*        end
**
*        subroutine cmpcts_(l1,l2,l3)
*        return
*        end
**
*        subroutine crapso_(l1,l2,l3)
*        return
*        end
*        subroutine dorys_(l3)
*        return
*        end
**
*        subroutine izero_(n,iarr,inc)
*          integer n, iarr(*),inc
*          call izero_wr(n,iarr,inc)
*         return
*         end
**
*         subroutine ddafile(i1,i2,r1,i3,i4)
*             integer i1,i2,i3,i4
*             real*8 r1(*)
*          call ddafile_(i1,i2,r1,i3,i4)
*          return
*          end
**
*@else
        subroutine drvk2(l1,l2,l3)
        return
        end
*
*
        subroutine cmpctr(l1,l2,l3)
        return
        end
*
        subroutine cmpcts(l1,l2,l3)
        return
        end
*
        subroutine crapso(l1,l2,l3)
        return
        end
        subroutine dorys(l3)
        return
        end
*
        subroutine izero(n,iarr,inc)
         integer n,iarr(*),inc
*
         call izero_wr(n,iarr,inc)
        return
        end
*@endif
*
        subroutine mcget_iscalar(name,ival)
        character*(*) name
        integer ival
*@ifdef molcas_int64
         integer*8 iloc
*@else
*         integer*4 iloc
*@endif 
*@ifdef molcas_ext
*        call get_iscalar_(name,iloc)
*@else
        call get_iscalar(name,iloc)
*@endif
        ival=iloc
        return
        end
*
        subroutine mcget_iarray(name,ival,l)
        character*(*) name
        integer l,ival(l)
*@ifdef molcas_int64
         integer*8 ll
         integer*8, allocatable :: iloc(:)
*@else
*         integer*4 ll
*         integer*4, allocatable :: iloc(:)
*@endif
         ll=l
         allocate(iloc(ll))
*@ifdef molcas_ext
*        call get_iarray_(name,iloc,ll)
*@else
        call get_iarray(name,iloc,ll)
*@endif
c        write(0,*) 'mcget_iarray:',name,iloc(1),ll
         if (iloc(1).eq.-987654321) then 
            l=ll
            deallocate(iloc)
            return
         endif  
        do ll=1,l
         ival(ll)=iloc(ll)
        enddo
        deallocate(iloc)
        return
        end
*
*
        subroutine mcget_carray(name,label,n)
        character*(*) name
        integer n 
        character*8 label(*)
        character*10, allocatable :: label2(:)
*@ifdef molcas_int64
         integer*8 ln
*@else
*         integer*4 ln
*@endif
*@ifdef molcas_label10
        if (name(1:18).eq.'unique basis names') then
c       call to mcput_carray/mcget_carray assume *8
c       Molcas starts using *10 1:6 atom 7:10 basis
        allocate (label2(n/8))
        ln=(n/8)*10  
*@ifdef molcas_ext
*        call get_carray_(name,label2,ln)
*@else
        call get_carray(name,label2,ln)
*@endif
        do i=1,n/8
c         write(0,'(a,a)') 'full label=',label2(i)(1:10)
          label(i)(1:4)=label2(i)(1:4)
          label(i)(5:8)=label2(i)(7:10)
        enddo
        deallocate(label2)
        else
         ln=n
*@ifdef molcas_ext
*        call get_carray_(name,label,ln)
*@else
        call get_carray(name,label,ln)
*@endif
         endif 
*@else 
*        ln=n
*@ifdef molcas_ext
*        call get_carray_(name,label,ln)
*@else
*        call get_carray(name,label,ln)
*@endif
*@endif 


        return
        end
*
*
        subroutine mcput_carray(name,label,n)
        character*(*) name
        integer n,i
        character*8 label(*)
        character*10, allocatable :: label2(:)
*@ifdef molcas_int64
         integer*8 ln
*@else
*         integer*4 ln
*@endif

c       since MOLCAS 7.7 the various label sizes
c       have changed 

*@ifdef molcas_label10
        if (name(1:18).eq.'unique basis names') then
c       call to mcput_carray/mcget_carray assume *8
c       Molcas starts using *10
        allocate (label2(n/8))
        ln=(n/8)*10         
        do i=1,n/8
          label2(i)(1:4)=label(i)(1:4)
          label2(i)(5:6)='  '
          label2(i)(7:10)=label(i)(5:8)
        enddo
*@ifdef molcas_ext
*        call put_carray_(name,label2,ln)
*@else
        call put_carray(name,label2,ln)
*@endif
        deallocate(label2)
        else
         ln=n
*@ifdef molcas_ext
*        call put_carray_(name,label,ln)
*@else
        call put_carray(name,label,ln)
*@endif
        endif 
*@else
*           ln=n
*@ifdef molcas_ext
*        call put_carray_(name,label,ln)
*@else
*        call put_carray(name,label,ln)
*@endif
*@endif 
        return
        end
*
        subroutine mcput_iscalar (label,ival)
        character*(*) label
        integer ival
*@ifdef molcas_int64
         integer*8 ln
*@else
*         integer*4 ln
*@endif
         ln=ival
*@ifdef molcas_ext
*        call put_iscalar_(label,ln)
*@else
        call put_iscalar(label,ln)
*@endif
        return
        end
*
*
*
*
*
        subroutine mcget_darray(name,darr,n)
        character*(*) name
        integer n
        real*8 darr(*) 
*@ifdef molcas_int64
         integer*8 ln
*@else
*         integer*4 ln
*@endif
         ln=n
*@ifdef molcas_ext
*        call get_darray_(name,darr,ln)
*@else
        call get_darray(name,darr,ln)
*@endif
        return
        end
*
        subroutine mcput_darray(name,darr,n)
        character*(*) name
        integer n
        real*8 darr(*)
*@ifdef molcas_int64
         integer*8 ln
*@else
*         integer*4 ln
*@endif
         ln=n
*@ifdef molcas_ext
*        call put_darray_(name,darr,ln)
*@else
        call put_darray(name,darr,ln)
*@endif
        return
        end
*
        subroutine mcput_cmo(darr,n)
        integer n
        real*8 darr(*)
*@ifdef molcas_int64
         integer*8 ln
*@else
*         integer*4 ln
*@endif
         ln=n
*@ifdef molcas_ext
*        call put_cmo_(darr,ln)
*@else
        call put_cmo(darr,ln)
*@endif
        return
        end
*
*
*
*
        subroutine mcopnord(irc,i2,name,i1)
        character*(*) name
        integer irc,i2,i1
      
*@ifdef molcas_int64
        integer*8 lirc,li2,li1
*@else
*        integer*4 lirc,li2,li1
*@endif
        li2=i2
        li1=i1
*@ifdef molcas_ext
*        call opnord_(lirc,li2,name,li1)
*@else
        call opnord(lirc,li2,name,li1)
*@endif
        irc=lirc
        return
        end
*
        subroutine mcrdord(i1,i2,n1,n2,n3,n4,buffer,i3,i4)
        integer i1,i2,n1,n2,n3,n4,i3,i4
        real*8 buffer(*)
*@ifdef molcas_int64
        integer*8 li1,li2,ln1,ln2,ln3,ln4,li3,li4
*@else
*        integer*4 li1,li2,ln1,ln2,ln3,ln4,li3,li4
*@endif
        
         li1=i1
         li2=i2
         li3=i3
         li4=i4
         ln1=n1
         ln2=n2
         ln3=n3
         ln4=n4
  
*@ifdef molcas_ext
*        call rdord_(li1,li2,ln1,ln2,ln3,ln4,buffer,li3,li4)
*@else
        call rdord(li1,li2,ln1,ln2,ln3,ln4,buffer,li3,li4)
*@endif
        i1=li1
        i4=li4
        return
        end
 
 
*
*
*
       subroutine link_it
       implicit none
c----------------------------------------------------------------------
c*
c     Define default processing options
c*
c----------------------------------------------------------------------
c     Include 'global.inc'
ccomdeck global.INC $Revision: 2.26.8.4 $
c----------------------------------------------------------------------
c* 
*@ifdef molcas_int64
        integer*8 maxbfn,mxatom,mxroot,mxact,mxina,mxbas,
     &             mxorb,mxsym,mxatms,mtit,mxtitl,kbuf
*@else
*        integer maxbfn,mxatom,mxroot,mxact,mxina,mxbas,
*     .             mxorb,mxsym,mxatms,mtit,mxtitl,kbuf
*@endif
ccomdeck maxbfn.inc $Revision: 2.26.8.4 $
       Parameter(maxbfn=10000)
       Parameter (mxAtom = 5000)
       Parameter (mxroot = 600)
       Parameter (mxact  =  100)
       Parameter (mxina  = maxbfn)
       Parameter (mxbas = maxbfn)
       Parameter (mxOrb = maxbfn)
       Parameter (mxSym = 8)
       Parameter ( mxAtms  = mxatom      )
*@ifdef molcas_int64
      Integer*8   nSym,nAtoms
      Integer*8   nBas(mxSym)
      Integer*8   nOrb(mxSym)
      Integer*8   iOper(mxSym)
      Integer*8   nDel(mxSym)
      Integer*8   nFro(mxSym)
*@else
*      Integer   nSym,nAtoms
*      Integer   nBas(mxSym)
*      Integer   nOrb(mxSym)
*      Integer   iOper(mxSym)
*      Integer   nDel(mxSym)
*      Integer  nFro(mxSym)
*@endif
c      Character*4 AtLbl(mxAtms)
c      Character*4 BsLbl(2,mxOrb)
      Integer LenIn, LENIN4
      parameter (LenIn=6)
      Parameter (LENIN4=LENIN+4)
      Character*(LENIN4) BsLbl(mxOrb)
      Real*8 Coor(3,MxAtms)
      Real*8 PotNuc
*@ifdef molcas_ext
*      Common /Info_ / PotNuc,Coor,AtLbl,BsLbl,
*     *                nAtoms,nSym,nBas,nOrb,nDel,nFro,iOper
*@else
      Common /Info_motra/ PotNuc,Coor,BsLbl,nAtoms,nSym,nBas,nOrb,
     &                    nDel,nFro,iOper
*@endif
c
c
c----------------------------------------------------------------------
c
c     Allocate space to store the MO-coefficients and occupations
c
c----------------------------------------------------------------------
c
*@ifdef molcas_ext
*      Real*8 Occ(mxOrb)
*      Common /MObas_/ Occ
*@else
      Real*8 Occ(mxOrb)
      Common /MObas/ Occ
*@endif
c
c----------------------------------------------------------------------
c
c     Allocate space to store the one-electron inntegral file header
c     and the header of the input source of MO coefficients
c
c----------------------------------------------------------------------
c
      Character*144 Header
      Character*80  VecTit
*@ifdef molcas_ext
*      Common /Head_ / Header,VecTit
*@else
      Common /Head  / Header,VecTit
*@endif
c
c----------------------------------------------------------------------
c
c     Allocate space to store the title
c
c----------------------------------------------------------------------
c
      Parameter ( mxTitl = 10      )
c
      Character*72 Title(mxTitl)
*@ifdef molcas_ext
*      Common /Tit_  / mTit,Title
*@else
      Common /Tit   / mTit,Title
*@endif
c
c----------------------------------------------------------------------
c
c     Allocate space to store logical switches for routing and
c     printing
c
c----------------------------------------------------------------------
c
*@ifdef molcas_int64
      Integer*8   Debug
      Integer*8   iPrint
      Integer*8   iOneOnly
      Integer*8   iVecTyp
      Integer*8   iAutoCut
      Integer*8   iRFpert
      Integer*8   ihdf5
*@else
*      Integer   Debug
*      Integer   iPrint
*      Integer   iOneOnly
*      Integer   iVecTyp
*      Integer   iSPdel
*      Integer   iAutoCut
*      Integer   iRFpert
*@endif
*@ifdef molcas_ext 
*      Common /Switches_/
*     &Debug,iPrint,iOneOnly,iVecTyp,iSPdel,iAutoCut,iRFpert
*@else
      Common /Switches/ Debug,iPrint,iOneOnly,iVecTyp,
     &                  iAutoCut,iRFpert,ihdf5
*@endif
c
c----------------------------------------------------------------------
c
c     Save cutting threshold for AUTO cut option
c
c----------------------------------------------------------------------
c
      Real*8 CutThrs(mxSym)
      Common /Cut/ CutThrs
c
c----------------------------------------------------------------------
c
c     Definee file names and unit numbers.
c
c----------------------------------------------------------------------
c
cfordeck files.inc $Revision: 2.26.8.4 $
c
c----------------------------------------------------------------------
c
c     Store file names and unit numbers.
c
c----------------------------------------------------------------------
c
c     Input files for MO coefficients
      Character*180   FnInpOrb,FnJobIph
c     one- and two-electron integrals in AO basis
      Character*8   FnOneAO,FnTwoAO
c     one- and two-electron integrals in MO basis
      Character*8   FnOneMO,FnTwoMO
c     half transformed two-electron integrals
      Character*8   FnHalf
c     EXTRACT file
      Character*8   FnExt
c     COMFILE file
      Character*8   FnCom
*@ifdef molcas_int64
      Integer*8       LuCom
      Integer*8       LuExt
      Integer*8       LuHalf
      Integer*8       LuOneMO,LuTwoMO
      Integer*8       LuOneAO,LuTwoAO
      Integer*8       LuInpOrb,LuJobIph
*@else
*      Integer       LuCom
*      Integer       LuExt
*      Integer       LuHalf
*      Integer       LuOneMO,LuTwoMO
*      Integer       LuOneAO,LuTwoAO
*      Integer       LuInpOrb,LuJobIph
*@endif
*@ifdef molcas_ext
*      Common /Files/
*     &  FnInpOrb,FnJobIph,FnOneAO,FnTwoAO,
*     &  FnOneMO,FnTwoMO,FnHalf,FnExt,FnCom,
*     &  LuInpOrb,LuJobIph,LuOneAO,LuTwoAO,
*     &  LuOneMO,LuTwoMO,LuHalf,LuExt,LuCom
*@else
      Common /Motra_Files/
     &  FnInpOrb,FnJobIph,FnOneAO,FnTwoAO,
     &  FnOneMO,FnTwoMO,FnHalf,FnExt,FnCom,
     &  LuInpOrb,LuJobIph,LuOneAO,LuTwoAO,
     &  LuOneMO,LuTwoMO,LuHalf,LuExt,LuCom
*@endif
c
c----------------------------------------------------------------------
c
c     allocate space to store the table of contents of various files
c
c----------------------------------------------------------------------
c
*@ifdef molcas_int64
      Integer*8   TcJobIph(1024)
      Integer*8   TcOneMO(1024)
*@else
*      Integer   TcJobIph(1024)
*      Integer   TcOneMO(1024)
*@endif
*@ifdef molcas_ext
*      Common /Tocs_/ TcJobIph,TcOneMO
*@else
      Common /Tocs/ TcJobIph,TcOneMO
*@endif
c----------------------------------------------------------------------
c
c     Define TOC for the electron repulsion integrals in MO basis
c
c     Define the buffer size of the electron repulsion
c     integrals in MO basis
c----------------------------------------------------------------------
c
*@ifdef molcas_int64
      Integer*8 nTraToc,nTraBuf
      Parameter(nTraToc=106,nTraBuf=9600)
      Integer*8 iTraToc(nTraToc)
*@else
*      Integer nTraToc,nTraBuf
*      Parameter(nTraToc=106,nTraBuf=9600)
*      Integer   iTraToc(nTraToc)
*@endif
*@ifdef molcas_ext
*      Common /TraToc/ iTraToc
*@else
      Common /TraToc/ iTraToc
*@endif
c
c
c
      Parameter ( kBuf = nTraBuf )
c
      FnInpOrb='INPORB'
      FnJobIph='JOBIPH'
      LuInpOrb=10
      LuJobIph=15
      FnOneAO='ONEINT'
      FnTwoAO='ORDINT'
      LuOneAO=20
      LuTwoAO=40
      FnOneMO='TRAONE'
      FnTwoMO='TRAINT'
      LuOneMO=30
      LuTwoMO=50
      FnHalf='TEMP1'
      LuHalf=60
      FnExt='EXTRACT'
      LuExt=18
      FnCom='COMFILE'
      LuCom=22
c----------------------------------------------------------------------
c
c
      Debug=0
      iPrint=0
      iOneOnly=0
      iVecTyp=2
      iAutoCut=0
      iRFpert=0
*
      End
*
*
*
      Subroutine RdOneXX(i_rc,i_Option,InLab,
     .         i_Comp,Data,i_SymLab,ret_data,fname)
*
C*********************************************************************
C                                                                      *
C   (c) Copyright 1993 by the authors of MOLCAS. All rights reserved   *
C                                                                      *
C----------------------------------------------------------------------*
C                                                                      *
C     Purpose: Read data from one-electron integral file               *
C                                                                      *
C     Calling parameters:                                              *
C     rc      : Return code                                            *
C     Option  : Switch to set options                                  *
C     InLab   : Identifier for the data to read                        *
C     Comp    : Composite identifier to select components              *
C     Data    : contains on output the requested data                  *
C     SymLab  : symmetry label of the requested data                   *
c     ret_data: valid data in field Data (DP units)
C                                                                      *
C     Global data declarations (Include file):                         *
C     Parm    : Table of paramaters                                    *
C     rcParm  : Table of return codes                                  *
C     Switch  : Table of options                                       *
C    Common  : Common block containing ToC                            *
C     Data    : Data definitions                                       *
C                                                                      *
C     Local data declarations:                                         *
C     Label   : character*8, used to covert incoming names             *
C     TmpBuf  : I/O buffer                                             *
C     HldBuf  : I/O buffer                                             *
C                                                                     *
C---------------------------------------------------------------------*
C                                                                      *
C     written by:                                                      *
C     P.O. Widmark and M.P. Fuelscher                                  *
C     University of Lund, Sweden, 1993                                 *
C                                                                      *
C----------------------------------------------------------------------*
C                                                                      *
C     history: none                                                    *
C                                                                      *
C*********************************************************************
*@ifdef molcas_int64
       Implicit Integer*8 (A-Z)
*@ifndef int64
*        integer*4 i_rc,i_option,i_comp,i_listlabels,i_symlab
*@endif
*@else
*      Implicit Integer (A-Z)
*@endif
c----------------------------------------------------------------------*
c                                                                      *
c Return codes:                                                        *
c   rc0000 - No error                                                  *
c   rcOP01 - file is already opened                                    *
c   rcOP02 - file specified as old is not existent                     *
c   rcOP03 - invalid file identifier                                   *
c   rcOP04 - unknown option has been specified                         *
c   rcCL01 - file is not opened                                        *
c   rcRD01 - file is not opened                                        *
c   rcRD02 - illegal options                                           *
c   rcRD03 - information not available                                 *
c   rcRD04 - nSym not defined                                          *
c   rcWR01 - file is not opened                                        *
c   rcWR02 - nSym<1 or nSym>8                                          *
c   rcWR03 - nSym note defined                                         *
c   rcWR04 - Sum(nBas(iSym))>2*mxBas                                   *
c   rcWR05 - Sum(nBas(iSym))<1                                         *
c   rcWR06 - Min(nBas(iSym)<0                                          *
c   rcWR07 - Max(nBas(iSym)>mxBas                                      *
c   rcWR08 - nAtm<1 or nAtm>mxAtm                                      *
c   rcWR09 - nAtm not defined                                          *
c   rcWR10 - nBas not defined                                          *
c   rcWR11 - to many operators                                         *
c                                                                      *
c----------------------------------------------------------------------*
      Parameter (rc0000 = 0)
      Parameter (rcOP01 = rc0000+1)
      Parameter (rcOP02 = rcOP01+1)
      Parameter (rcOP03 = rcOP02+1)
      Parameter (rcOP04 = rcOP03+2)
      Parameter (rcCL01 = rcOP04+1)
      Parameter (rcRD01 = rcCL01+1)
      Parameter (rcRD02 = rcRD01+1)
      Parameter (rcRD03 = rcRD02+1)
      Parameter (rcRD04 = rcRD03+1)
      Parameter (rcWR01 = rcRD04+1)
      Parameter (rcWR02 = rcWR01+1)
      Parameter (rcWR03 = rcWR02+1)
      Parameter (rcWR04 = rcWR03+1)
      Parameter (rcWR05 = rcWR04+1)
      Parameter (rcWR06 = rcWR05+1)
      Parameter (rcWR07 = rcWR06+1)
      Parameter (rcWR08 = rcWR07+1)
      Parameter (rcWR09 = rcWR08+1)
      Parameter (rcWR10 = rcWR09+1)
      Parameter (rcWR11 = rcWR10+1)
c
cifordeck OneFlags.inc $Revision: 2.26.8.4 $
c----------------------------------------------------------------------*
c                                                                      *
c Switches for one electron integral handlers:                         *
c   sOpSiz - Return only size of array                                 *
c   sNoOri - Do not read origin of operator                            *
c   sNoNuc - Do not read nuclear contribution                          *
c   sRdFst - Read first operator                                       *
c   sRdNxt - Read next operator                                        *
c   sNew   - New file, create.                                         *
c   sXXXXX -                                                           *
c                                                                      *
c----------------------------------------------------------------------*
      Parameter (sOpSiz = 1)
      Parameter (sNoOri = 2*sOpSiz)
      Parameter (sNoNuc = 2*sNoOri)
      Parameter (sRdFst = 2*sNoNuc)
      Parameter (sRdNxt = 2*sRdFst)
      Parameter (sRdCur = 2*sRdNxt)
      Parameter (sXXXXX = 2*sRdCur)
      Parameter (sNew   = 1)
ccomdeck OneDat.INC $Revision: 2.26.8.4 $
c----------------------------------------------------------------------*
c                                                                      *
c     Define I/O options                                               *
c                                                                      *
c----------------------------------------------------------------------*
      Parameter ( sWrite = 1          )
      Parameter ( sRead  = 2          )
c----------------------------------------------------------------------*
c                                                                      *
c     Define data conversion factors (machine dependent)               *
c                                                                      *
c----------------------------------------------------------------------*
c----------------------------------------------------------------------*
       Integer    ItoB,      RtoB,      RtoI
*@ifdef molcas_int64
       Parameter( ItoB = 8 , RtoB = 8 , RtoI = 1  )
*@else
*      Parameter( ItoB = 4 , RtoB = 8 , RtoI = 2  )
*@endif
c
       Parameter (mxAtom = 500)
       Parameter(maxbfn=5000)
       Parameter (mxroot = 100)
       Parameter (mxact  =  50)
       Parameter (mxina  = 100)
       Parameter (mxbas = maxbfn)
       Parameter (mxOrb = maxbfn)
       Parameter (mxSym = 8)
c----------------------------------------------------------------------*
c                                                                      *
c     Define Common /OneDat/                                           *
c                                                                      *
c----------------------------------------------------------------------*
c                                                                      *
c Parameters:                                                          *
c   NaN    - Not a Number = Variable undefined                         *
c   NotNaN - A Number = Variable is defined                            *
c   nAuxDt - extra space for origin and nuclear contribution.          *
c   nTitle - length of title.                                          *
c   MxOp   - Max number of operators                                   *
c   LenOp  - Length of TOC field defining operator                     *
c   MxSym  - Max number of symmetry representations                    *
c   MxAtom - Max number of atoms in system                             *
c   MxBas  - Max number of total basis functions                       *
c   PhyRc  - physical buffer size for DAFILE                           *
c   nBuf   - logical internal buffer size for reading/writing matrices *
c                                                                      *
c Pointers:                                                            *
c   pFID   - File identifier                                           *
c   pVersN - Version number                                            *
c   pTitle - Titleof the problem                                       *
c   pOp    - Operator list                                             *
c   pSym   - Number of irred. representations                          *
c   pSymOp - generator of irred. representation                        *
c   pBas   - Number of basis functions per irred. representation       *
c   pAtom  - Atoms in system                                           *
c   pCoord - Coordinates of atoms in system                            *
c   pPot   - Nuclear-Nuclear repulsion                                 *
c   pCoM   - Coordinates of center of mass                             *
c   pCoC   - Coordinates of center of charge                           *
c   pALbl  - Atom labels                                               *
c   pType  - Basis function symmetry labels                            *
c   pChrge - Charge of atoms in system                                 *
c   pOption- Various options - direct, expert mode, ...                *
c   pNext  - Next free record                                          *
c                                                                      *
c Offsets:                                                             *
c   oLabel - Label of operator                                         *
c   oComp  - Component number                                          *
c   oSymLb - Symmetry label of operator                                *
c   oAddr  - Disk address                                              *
c                                                                      *
c----------------------------------------------------------------------*
      Parameter ( NaN=-1        )
      Parameter ( NotNaN=0      )
      Parameter ( nAuxDt=4      )
c    Bug Fix RL 970429
c     Parameter ( nTitle=RtoI*9 )
      Parameter ( nTitle=(72*2)/ItoB)
c
      Parameter ( PhyRec=1024   )
      Parameter ( nBuf=4*PhyRec )
      Parameter ( MxOp=16384    )
      Parameter ( LenOp=5       )
c
      Parameter ( pFID   = 1                       )
      Parameter ( pVersN = pFID   + 1              )
      Parameter ( pTitle = pVersN + 1              )
      Parameter ( pOp    = pTitle + nTitle + 1     )
      Parameter ( pSym   = pOp    + MxOp*LenOp     )
      Parameter ( pSymOp = pSym   + 1              )
      Parameter ( pBas   = pSymOp + MxSym          )
      Parameter ( pAtom  = pBas   + MxSym          )
      Parameter ( pCoord = pAtom  + 1              )
      Parameter ( pPot   = pCoord + 6*MxAtom + 1   )
      Parameter ( pCoM   = pPot   + 2 + 1          )
      Parameter ( pCoC   = pCoM   + 6 + 1          )
      Parameter ( pALbl  = pCoC   + 6 + 1          )
      Parameter ( pType  = pALbl  + MxAtom + 1     )
      Parameter ( pChrge = pType  + 4*MxBas + 1    )
      Parameter ( pIndex = pChrge + 2*MxAtom + 1   )
      Parameter ( pNext  = pIndex + 2*MxAtom + 1   )
      Parameter ( pOption= pNext  + 1              )
      Parameter ( pEnd   = pOption+ 1              )
      Parameter ( lToc   = 1024*((pEnd+1022)/1024) )
c
      Parameter ( oLabel = 0          )
      Parameter ( oComp  = oLabel + 2 )
      Parameter ( oSymLb = oComp  + 1 )
      Parameter ( oAddr  = oSymLb + 1 )
c
      Parameter ( pLu    = 1          )
      Parameter ( pOpen  = pLu    + 1 )
      Parameter ( lAux   = pOpen  + 1 )
c
      Dimension AuxOne(lAux)
      Dimension TocOne(lToc)
*@ifdef molcas_ext
*      Common /OneDat_/ AuxOne,TocOne
*@else
      Common /OneDat/ AuxOne,TocOne
*@endif
c
      Character*(*) InLab
      Dimension Data(*)
      Dimension nBas(8)
      CHARACTER*(*) fname
c
      Character*8 TmpLab,Label
      Dimension LabTmp(2)
      Equivalence (TmpLab,LabTmp)
c
      Parameter (lBuf=1024)
      Real*8    TmpBuf(lBuf),AuxBuf(4)
      Logical debug, Close
c
      integer*4 i1,i2,i3,i4,i5,ii
c
      Data CurrOp/1/
      Save CurrOp
c----------------------------------------------------------------------*
c     Start procedure:                                                 *
c     Define inline function (symmetry multiplication)                 *
c----------------------------------------------------------------------*
      MulTab(i,j)=iEor(i-1,j-1)+1
c
      rc=i_rc
      Option=i_option
      comp=i_comp
      symlab=i_symlab
      listlabels=i_listlabels
*
C----------------------------------------------------------------------*
C     Pick up the file definitions                                     *
C----------------------------------------------------------------------*
*@ifdef molcas_ext
*      Call qEnter_('RdOne')
*@else
      Call qEnter('RdOne')
*@endif
      rc    = rc0000
      LuOne = AuxOne(pLu  )
      Open  = AuxOne(pOpen)
c----------------------------------------------------------------------*
c     Check the file status                                            *
c----------------------------------------------------------------------*
      Close=.False.
      If ( Open.ne.1 ) Then
*
C------- Well, I'll open and close it for you under the default name
*
         LuOne=77
*@ifdef molcas_ext
*         LuOne=isFreeUnit_(LuOne)
*@else
         LuOne=isFreeUnit(LuOne)
*@endif
         if (len_trim(fname).gt.8) then
            stop 'RdOneXX: fname > 8'
         elseif (len_trim(fname).lt.8) then
            Label=FNAME(1:len_trim(fname))
            do ii=len_trim(fname)+1,8
            label(ii:ii)=' '
            enddo
         else
         Label=FNAME(1:8)
         endif
         iRC=-1
         iOpt=0
*@ifdef molcas_ext
*         Call OpnOne_(iRC,iOpt,Label,LuOne)
*@else
         Call OpnOne(iRC,iOpt,Label,LuOne)
*@endif
         If (iRC.ne.0) Then
            Write (6,*) 'RdOne: Error opening file'
*@ifdef molcas_ext
*            Call Abend_
*@else
            Call Abend
*@endif
         End If
         Close=.True.
      End If                            
c----------------------------------------------------------------------*
c     Truncate the label to 8 characters and convert it to upper case  *
c----------------------------------------------------------------------*
      Label=InLab
*@ifdef molcas_ext
*      Call UpCase_(Label)
*@else
      Call UpCase(Label)
*@endif
      TmpLab=Label
c----------------------------------------------------------------------*
c     Print debugging information                                      *
c----------------------------------------------------------------------*
      debug=.false.
      If(iAnd(int(option,8),int(1024,8)).ne.0) debug=.true.
      If(debug) Then
         Write(6,*) '<<< Entering RdOne >>>'
         Write(6,'(a,z8)') ' rc on entry:     ',rc
         Write(6,'(a,a)')  ' Label on entry:  ',Label
         Write(6,'(a,z8)') ' Comp on entry:   ',Comp
         Write(6,'(a,z8)') ' SymLab on entry: ',SymLab
         Write(6,'(a,z8)') ' Option on entry: ',Option
      End If
c----------------------------------------------------------------------*
c     Check reading mode                                               *
c----------------------------------------------------------------------*
      If((iAnd(iAnd(option,sRdFst),sRdNxt)).ne.0) then
         Write (6,*) 'RdOne: Invalid option(s)'
         Write (6,*) 'option=',option
*@ifdef molcas_ext
*         Call QTrace_()
*         Call Abend_()
*@else
         Call QTrace()
         Call Abend()
*@endif
      Else If((iAnd(iAnd(option,sRdFst),sRdCur)).ne.0) then
         Write (6,*) 'RdOne: Invalid option(s)'
         Write (6,*) 'option=',option
*@ifdef molcas_ext
*         Call QTrace_()
*         Call Abend_()
*@else
         Call QTrace()
         Call Abend()
*@endif
      Else If((iAnd(iAnd(option,sRdNxt),sRdCur)).ne.0) then
         Write (6,*) 'RdOne: Invalid option(s)'
         Write (6,*) 'option=',option
*@ifdef molcas_ext
*         Call QTrace_()
*         Call Abend_()
*@else
         Call QTrace()
         Call Abend()
*@endif
      End If
c----------------------------------------------------------------------*
c     Load back TocOne                                                 *
c----------------------------------------------------------------------*
      iDisk=0
      ia1=2
*@ifdef molcas_ext
*      Call iDaFile_(LuOne,ia1,TocOne,lToc,iDisk)
*@else
      Call iDaFile(LuOne,ia1,TocOne,lToc,iDisk)
*@endif
c----------------------------------------------------------------------*
c     Read data from ToC                                               *
c----------------------------------------------------------------------*
      NoGo=sRdFst+sRdNxt+sRdCur
c----------------------------------------------------------------------*
c     Read operators from integral records                             *
c----------------------------------------------------------------------*
      If (iAnd(option,sRdNxt).ne.0) Then
         CurrOp=CurrOp+1
         If (CurrOp.gt.MxOp) Then
            CurrOp=0
         Else If(TocOne(pOp+LenOp*(CurrOp-1)+oLabel).eq.Nan) Then
            CurrOp=0
         Else
            i=CurrOp
            LabTmp(1)=TocOne(pOp+LenOp*(i-1)+oLabel  )
*@ifndef molcas_int64
*c           due to equivalence statement between character*8 and integer*4
*            LabTmp(2)=TocOne(pOp+LenOp*(i-1)+oLabel+1 )
*@endif
            Label=TmpLab
            InLab=Label
            SymLab=TocOne(pOp+LenOp*(i-1)+oSymLb)
            Comp=TocOne(pOp+LenOp*(i-1)+oComp   )
         End If
      Else If(iAnd(option,sRdFst).ne.0) Then
         CurrOp=1
         If(TocOne(pOp+LenOp*(CurrOp-1)+oLabel).eq.Nan) Then
            CurrOp=0
         Else
            i=CurrOp
            LabTmp(1)=TocOne(pOp+LenOp*(i-1)+oLabel  )
*@ifndef molcas_int64
*            LabTmp(2)=TocOne(pOp+LenOp*(i-1)+oLabel+1 )
*@endif
            Label=TmpLab
            InLab=Label
            SymLab=TocOne(pOp+LenOp*(i-1)+oSymLb)
            Comp=TocOne(pOp+LenOp*(i-1)+oComp   )
         End If
      Else If(iAnd(option,sRdCur).ne.0) Then
         If(CurrOp.lt.1 .or. CurrOp.gt.MxOp) Then
            CurrOp=0
         Else If(TocOne(pOp+LenOp*(CurrOp-1)+oLabel).eq.Nan) Then
            CurrOp=0
         Else
            i=CurrOp
            LabTmp(1)=TocOne(pOp+LenOp*(i-1)+oLabel  )
            Label=TmpLab
            InLab=Label
            SymLab=TocOne(pOp+LenOp*(i-1)+oSymLb)
            Comp=TocOne(pOp+LenOp*(i-1)+oComp   )
         End If
      Else
         CurrOp=0
         Do 500 i=MxOp,1,-1
            LabTmp(1)=TocOne(pOp+LenOp*(i-1)+oLabel  )
*@ifndef molcas_int64
*            LabTmp(2)=TocOne(pOp+LenOp*(i-1)+oLabel+1 )
*@endif
            CmpTmp=TocOne(pOp+LenOp*(i-1)+oComp   )
            TmpCmp=Comp
            If(TmpLab.eq.Label .and. CmpTmp.eq.TmpCmp) CurrOp=i
            if (debug) then 
               if (CurrOp.ne.0) then
               if (debug) then 
                write(6,*) 'picked Label',Tmplab,' Component',cmptmp,
     .                      'currop=',currop
               endif
                goto 501
               endif     
            endif
  500      Continue
  501      continue
      End If
      If(CurrOp.eq.0) Then
         rc=rcRD03
         if (debug) then
         Write (*,*) 'RdOne: Information not available'
         Write (*,*) 'Option=',Option
         Write (*,*) 'Comp=',Comp
         Write (*,*) 'SymLab=',SymLab
         Write (*,*) 'Label=',Label
         endif 
         Go To 999
      End If
      SymLab=TocOne(pOp+LenOp*(CurrOp-1)+oSymLb)
      If(debug) Then
        write(6,*) 'Computed Symlab=',Symlab
      endif
      Len=0
*@ifdef molcas_ext
*      Call Get_iScalar_('nSym',in)
*      Call Get_iArray_('nBas',nBas,in)
*@else
      Call Get_iScalar('nSym',in)
      Call Get_iArray('nBas',nBas,in)
*@endif
      Do 510 i=1,in
      Do 510 j=1,i
         ij=MulTab(i,j)-1
         If(iAnd(2**ij,SymLab).ne.0) Then
            If(i.eq.j) Then
               Len=Len+nBas(i)*(nBas(i)+1)/2
            Else
               Len=Len+nBas(i)*nBas(j)
            End If
         End If
  510   Continue
      Data(1)=Len
      If ( IAND(option,sOpSiz).eq.0 ) Then
         IndAux = 0
         IndDta = 0
         iDisk=TocOne(pOp+LenOp*(CurrOp-1)+oAddr)
         Do i = 0,Len+3,lBuf
           nCopy  = MAX(int(0,8),MIN(lBuf,Len+int(4,8)-i))
           nSave  = MAX(int(0,8),MIN(lBuf,Len-i))
           ia1=2
*@ifdef molcas_ext
*           Call dDaFile_(LuOne,ia1,TmpBuf,nCopy,iDisk)
*@else
           Call dDaFile(LuOne,ia1,TmpBuf,nCopy,iDisk)
*@endif
C Problem: molcas is compiled with fortran (integer*8) Blas
C          or blas wrapper,
C          Columbus is here using essl with integer*4 args
C          and links in the "wrong library"
*@if defined ( molcas_int64 ) && ( ! defined ( int64 ))
*           i1=nSave
*           i2=1
*           call dcopy_wr(i1,tmpBuf,i2,Data(IndDta+1),i2)
*@else
           Call dCopy_wr(nSave,TmpBuf,1,Data(IndDta+1),1)
*@endif
           IndDta = IndDta+RtoI*nSave
           Do j = nSave+1,nCopy
             IndAux = IndAux+1
            AuxBuf(IndAux) = TmpBuf(nSave+IndAux)
             AuxBuf(IndAux) = TmpBuf(j)
           End Do
         End Do
         ret_data=inddta 
         If(iAnd(sNoOri,option).eq.0) Then
*@if defined (molcas_int64) && (! defined (int64))
*           i1=3
*           i2=1
*           call dcopy_wr(i1,Auxbuf,i2,Data(IndDta+1),i2)
*@else
           Call dCopy_wr(3,AuxBuf,1,Data(IndDta+1),1)
           if (debug) then
             write(6,'(a,3f12.6)') 
     .         'Orign of Operator:',(Auxbuf(i1),i1=1,3)
           endif
*@endif
         ret_data=inddta+RtoI*3 
         End If
         If(iAnd(sNoNuc,option).eq.0) Then
*@if defined (molcas_int64) && (! defined (int64))
*            i1=1 
*            i2=1
*            call dcopy_wr(i1,Auxbuf(4),i2,Data(IndDta+RtoI*3+1),i2)
*@else
           Call dCopy_wr(1,AuxBuf(4),1,Data(IndDta+RtoI*3+1),1)
           if (debug) then
             write(6,'(a,f12.6)') 
     .         'Nuclear Contribution:',Auxbuf(4)
           endif
         ret_data=inddta+RtoI*3+RtoI*1 
*@endif
         End If
      End If
*
 999  Continue
        ret_data=ret_data/RtoI  
c      in DP units
      If (Close) Then
c       Write (*,*) ' I will close the file for you!'
         iRC=-1
         iOpt=0
*@ifdef molcas_ext
*         Call ClsOne_(iRC,iOpt)
*@else
         Call ClsOne(iRC,iOpt)
*@endif
         If (iRC.ne.0) Then
            Write (6,*) 'RdOne: Error closing file'
*@ifdef molcas_ext
*            Call Abend_
*@else
            Call Abend
*@endif
         End If
      End If              
c----------------------------------------------------------------------*
c     Terminate procedure                                              *
c----------------------------------------------------------------------*
      i_rc=int(rc,4)
      i_symlab=int(symlab,4)
*@ifdef molcas_ext
*      Call qExit_('RdOne')
*@else
      Call qExit('RdOne')
*@endif
      Return
      End
*
*
      Subroutine iRdOne(rc,Option,InLab,Comp,iData,SymLab)
*@ifdef molcas_int64
       Implicit Integer*8 (A-Z)
*@else
*      Implicit Integer (A-Z)
*@endif
*
      Character*(*) InLab
      Dimension iData(*)
*
      Call RdOnexx(rc,Option,InLab,Comp,iData,SymLab,ret_data,'ONEINT')
      return
      end
*
*@ifdef molcas_ext
*      SUBROUTINE GEN_INT_(rc,iSymp,iSymq,iSymr,iSyms,ipq1,numpq,Xint)
*@else
      SUBROUTINE GEN_INT(rc,iSymp,iSymq,iSymr,iSyms,ipq1,numpq,Xint)
*@endif
*
      Implicit real*8 (a-h,o-z)
      INTEGER   rc
      INTEGER   iSymp,iSymq,iSymr,iSyms
      INTEGER   pq,pq2,numpq,pq1_save
      Real*8    Xint(*)
c
c
      stop 'no support for cholesky'
c
c
      Return
      END
c
cC*******************************************************
c
c
c
*@endif


      subroutine wrtmof_molcas
     .  (unit,nsym, nmpsy, nbpsy, c1,switch,label)
c
c  write a formatted orbital coefficient file.
c
      implicit none
      integer nsym,unit
      integer nmpsy(nsym), nbpsy(nsym)
      real*8 c1(*)
      character*(*) label,switch
      integer j, k, nprev
c
      integer i1, i2, ierr, isym, nm, nb, m, i
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)

      if (switch(1:3).eq.'mos') then
      write(unit,100)
 100  format("#INPORB 2.2"/"#INFO")
      write(unit,101) label
 101  format('*',a)
      write(unit,102) 0,nsym,0 
 102  format(3i8)
      write(unit,103) (nbpsy(i),i=1,nsym)
      write(unit,103) (nmpsy(i),i=1,nsym)
 103  format(8i8)
      write(unit,104)
 104  format("#ORB")
      i1=0
      i2=0
      do 20 isym=1,nsym
         nm=nmpsy(isym)
         nb=nbpsy(isym)
         nprev=0
         do 10 m=1,nm
            write(unit,105) isym,m
 105   format("* ORBITAL",2i5)
 106   format(5(1X,ES21.14))
            i1=i2+1
            i2=i2+nb
            write(unit,106,iostat=ierr) (c1(i), i=i1,i2 )
            if(ierr.ne.0)then
               write(6,*)'isym,m=',isym,m
               call bummer('wrtmof: ierr=',ierr,faterr)
            endif
10       continue
20    continue
c
c     write(6,'(1x,i6,a)') i2,
c    .  ' mo coefficients written in LUMORB format.'
      return
      endif
      if (switch(1:3).eq.'occ') then
       write(unit,201)
 201  format("#OCC"/"* OCCUPATION NUMBERS")
       i1=0
       i2=0
       do isym=1,nsym
          nm=nmpsy(isym)
          nb=nbpsy(isym)
          i1=i2+1
          i2=i2+nm
          write(unit,106,iostat=ierr) (c1(i),i=i1,i2)
       enddo
       return
       endif
      return
      end

      subroutine rdmof_molcas
     .  (unit,nsym, nmpsy, nbpsy, c1,switch)
c
c  read a formatted orbital coefficient file.
c
      implicit none
      integer nsym,unit,itmp,itmp2
      integer nmpsy(nsym), nbpsy(nsym)
      real*8 c1(*)
      character*(*) switch
      character*80  cdata
      integer j, k, nprev
c
      integer i1, i2, ierr, isym, nm, nb, m, i
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      rewind(unit)
      read(unit,'(a)') cdata
      if ( cdata(1:11).ne."#INPORB 1.0" .and.
     .     cdata(1:11).ne."#INPORB 2.2" ) then
        call bummer('invalid formatted molcas format',0,2)
      endif
      read(unit,'(a)') cdata
      read(unit,'(a)') cdata
c     write(6,*) 'title string:',cdata(1:len_trim(cdata))
      read(unit,102)  itmp,nsym
 102  format(2i8)
      read(unit,103) (nbpsy(i),i=1,nsym)
      read(unit,103) (nmpsy(i),i=1,nsym)
 103  format(8i8)


      if (switch(1:3).eq.'mos') then
 1001 read(unit,'(a)',end=1002) cdata
      if (cdata(1:4).ne.'#ORB') goto 1001
      goto 1003     
 1002 call bummer("could not find orbital section",0,2) 
 1003  continue
      i1=0
      i2=0
      do 20 isym=1,nsym
         nm=nmpsy(isym)
         nb=nbpsy(isym)
         nprev=0
         do 10 m=1,nm
            read (unit,105) itmp,itmp2
            if (itmp.ne.isym .or. itmp2.ne.m) then
              call bummer('invalid orbital ordering',0,2)
            endif 
 105   format(9x,2i5)
 106   format(4e18.12)
            i1=i2+1
            i2=i2+nb
            read(unit,*,iostat=ierr) (c1(i), i=i1,i2 )
            if(ierr.ne.0)then
               write(6,*)'isym,m=',isym,m
               call bummer('wrtmof: ierr=',ierr,faterr)
            endif
10       continue
20    continue  
c
      write(6,'(1x,i6,a)') i2,
     .  ' mo coefficients read in LUMORB format.'
      return
      endif

      if (switch(1:3).eq.'occ') then 

  200  continue
       read(unit,'(a)',end=201) cdata
       if (cdata(1:4).ne.'#OCC') goto 200
       goto 202
  201  call bummer('could not find #OCC string',0,2)
  202  continue
      read(unit,'(a)') cdata
       i1=1
       i2=-1
       do isym=1,nsym
         nm=nmpsy(isym)
         i2=i1+nm
         read(unit,*,iostat=ierr) (c1(i),i=i1,i2-1)
         i1=i2
       enddo
      write(6,'(1x,i6,a)') i2-1,
     .  ' occupation numbers read in LUMORB format.'
      endif

      if (switch(1:6).eq.'energy') then
  300  continue
       read(unit,'(a)',end=301) cdata
       if (cdata(1:4).ne.'#ONE') goto 300
       goto 302
  301  call bummer('could not find #ONE string',0,2)
  302  continue
      read(unit,'(a)') cdata
       i1=1
       do isym=1,nsym
         nm=nmpsy(isym)
         i2=i1+nm
         read(unit,*,iostat=ierr) (c1(i),i=i1,i2-1)
         i1=i2
       enddo
      write(6,'(1x,i6,a)') i2-1,
     .  ' one electron energies read in LUMORB format.'
      endif



      return
      end
*@ifdef molcas 
*
      subroutine mo2out_molcas (valbuf,lbuf,nval) 
      implicit none
      include 'col_molcas.inc'
*
      integer nval 
      real*8 valbuf(nval)
      integer lbuf(4,nval)
      integer nshell,ishell_c
      integer kbin,i, ishell_a,ishell_b,ishell_d
      integer ishell_ab,ishell_cd,ishell_abcd
      integer index_a,index_b,index_c,index_d
      integer index_ab,index_cd,index_abcd
      integer ndim_a,ndim_b,ndim_c,ndim_d,ndim_ab
      integer ndim_cd,ndim_abcd
      logical indexation,dofock,dograd
      integer maxmem
       real*8 thrao
*
      Integer IWORKLEN, ICWORKLEN
      PARAMETER (IWORKLEN=8)
      PARAMETER (ICWORKLEN=IWORKLEN)
c for boundary checks: ./configure -compiler g95 -bound      
c      PARAMETER (IWORKLEN=552 800 000)
c      PARAMETER (ICWORKLEN=552 800)
      Real*8    Work(1:IWORKLEN)
      Integer   iWork(1:IWORKLEN)
      Integer   mxMem, ip_Dummy, ip_iDummy
      Equivalence (Work,iWork)
*@ifdef molcas_ext
*      Common /WrkSpc_/Work,mxMem
*      Common /iWrkSpc_/ ip_Dummy, ip_iDummy
*@else
      Common /WrkSpc/Work,mxMem
      Common /iWrkSpc/ ip_Dummy, ip_iDummy
*@endif
!
ccomdeck shinf.inc $Revision: 2.26.8.4 $
c     ipShBF   - ptr to iWork field with dim. nShlls*nIrrep
c                iWork(ipShBF + irrep*nShlls+iShell - 1):
c                  # SO functions in irrep for shell iShell
c     ipShLC   - ptr to iWork field with dim. nShlls*nIrrep
c                iWork(ipShLC + irrep*nShlls+iShell - 1):
c                  position of 1st component of iShell in irrep
c     ipShSh   - ptr to iWork field with dim. nShlls*nIrrep
c                iWork(ipShSh + irrep*nShlls+iShell - 1):
c                  pseudo shell index of iShell in irrep
c                  (not any iShell contributes to all irreps)
c     nShIrp(0:nIrrep-1)
c              - # of shells contributing to each irrep
c     nShBFMx  - largest shell size (of all irrep, all shells)
c     ipSOSh   - ptr to iWork field with dim. nDim
c                iWork(ipSOSh+sum{0..irp-1}(nBas(irp-1))+iSO - 1):
c                  shell the SO index iSO in irp belongs to
c     ipicntr  - ptr to iwork field holding center number for
c                each shell
c
*@ifdef molcas_ext
*      Integer ipShBF,ipShLC,ipShSh,nShIrp(0:7),ipSOSh,nShBFMx,ipicntr
*      Common /ShInf_/ipShBF,ipShLC,ipShSh,nShIrp,ipSOSh,nShBFMx,ipicntr
*@else
      Integer ipShBF,ipShLC,ipShSh,nShIrp(0:7),ipSOSh,nShBFMx,ipicntr
      Common /ShInf/ipShBF,ipShLC,ipShSh,nShIrp,ipSOSh,nShBFMx,ipicntr
*@endif
 
      integer itri,j
      real*8 backchain,factor
*
      itri(i,j)=max(i,j)*(Max(i,j)-1)/2+min(i,j)
    
      if (.not. associated(iso2ci_ptr)) 
     .   call bummer('iso2ci_ptr fails',0,2)
c     write(0,*) 'mo2out_molcas: nval=',nval
     
*@ifdef molcas_ext
*      call so2ci_(iso2ci_ptr,iWork(ipSOsh),nsos2)
*@else
      call so2ci(iso2ci_ptr,iWork(ipSOsh),nsos2)
*@endif
c     write(6,*) 'Contiguous Index:'
c     write(6,53) (j,(iso2ci_ptr(i,j),i=1,2),j=1,nsos2)
      do i=1,nval
51       format('lbuf(*,',i4,')=',4i4)
52       format('ishell=',4i4,'ab,cd,abcd=',3i8)
53       format('#'i8,': ',2i8)
c        write(0,51) i,lbuf(1:4,i)
c        factor=8.d0
c        if (lbuf(1,i).eq.lbuf(2,i)) factor=factor*0.5d0
c        if (lbuf(3,i).eq.lbuf(4,i)) factor=factor*0.5d0
c        if ((lbuf(1,i).eq.lbuf(3,i)).and.
c    .       (lbuf(2,i).eq.lbuf(4,i))) factor=factor*0.5d0 
c        valbuf(i)=valbuf(i)*factor
*
        ishell_a = iWork(ipSOsh-1+lbuf(1,i))
        ishell_b = iWork(ipSOsh-1+lbuf(2,i))
        ishell_c = iWork(ipSOsh-1+lbuf(3,i))
        ishell_d = iWork(ipSOsh-1+lbuf(4,i))
        ishell_ab=itri(ishell_a,ishell_b)
        ishell_cd=itri(ishell_c,ishell_d)
        ishell_abcd=itri(ishell_ab,ishell_cd)
c        write(0,52) ishell_a,ishell_b,ishell_c,ishell_d,
c    .     ishell_ab,ishell_cd,ishell_abcd
         if (ishell_a.gt.nsos2 .or. ishell_b.gt.nsos2 
     .   .or. ishell_c.gt.nsos2 .or. ishell_d.gt.nsos2) stop 9988
        index_a = iso2ci_ptr(1,lbuf(1,i))
        index_b = iso2ci_ptr(1,lbuf(2,i))
        index_c = iso2ci_ptr(1,lbuf(3,i))
        index_d = iso2ci_ptr(1,lbuf(4,i))
        ndim_a = iso2ci_ptr(2,lbuf(1,i))
        ndim_b = iso2ci_ptr(2,lbuf(2,i))
        ndim_c = iso2ci_ptr(2,lbuf(3,i))
        ndim_d = iso2ci_ptr(2,lbuf(4,i))
        ndim_ab = ndim_a*ndim_b 
        ndim_cd = ndim_c*ndim_d 
*
        if (ishell_a.gt.ishell_b) then
           index_ab=(index_b-1)*ndim_a+index_a
        elseif  (ishell_a.eq.ishell_b) then
            index_ab=itri(index_a,index_b)
        else
             index_ab=(index_a-1)*ndim_b+index_b
        endif
*
        if (ishell_c.gt.ishell_d) then
           index_cd=(index_d-1)*ndim_c+index_c
        elseif  (ishell_c.eq.ishell_d) then
            index_cd=itri(index_c,index_d)
        else
             index_cd=(index_c-1)*ndim_d+index_d
        endif
    
        if (ishell_ab.gt.ishell_cd) then
           index_abcd=(index_cd-1)*ndim_ab+index_ab
        elseif  (ishell_ab.eq.ishell_cd) then
            index_abcd=itri(index_ab,index_cd)
        else
             index_abcd=(index_ab-1)*ndim_cd+index_cd
        endif
 
         kbin=int(Bin_ptr(1,nbin,ishell_abcd))
c        write(0,*) 'BIN*',Bin_ptr(1,nbin,ishell_abcd),kbin
         kbin=kbin+1
         if (kbin.gt.nbin) stop 9977
         if (ishell_abcd.gt.nquad) stop 9976
c        write(6,113) valbuf(i),ishell_abcd,kbin,
c    .   index_a,index_b,index_c,index_d,index_ab,index_cd,index_abcd,
c    .   ishell_a,ishell_b,ishell_c,ishell_d,ishell_ab,ishell_cd,
c    .   ishell_abcd
 113     format('integral:',f18.10,'iquad=',i8,'#element',i6,
     .   'index:',4i4,3i8,' shells=',4i4,3i8)
     
         bin_ptr(1,kbin,ishell_abcd)=valbuf(i)
         bin_ptr(2,kbin,ishell_abcd)=index_abcd
         bin_ptr(1,nbin,ishell_abcd)=dble(kbin)
         if (kbin.eq.nbin-1) then
           backchain=dble(idisk)
c          write(0,*) 'mo2out_molcas: calling ddafile ' 
           call ddafile(lugamma,iwrite,bin_ptr(1,1,ishell_abcd),
     &                  2*nbin,idisk)
           bin_ptr(1,nbin,ishell_abcd)=0.0d0
           bin_ptr(2,nbin,ishell_abcd)=backchain
         endif
         enddo
         call wzero(nval,valbuf,1)
         call izero_wr(nval*4,lbuf,1) 
*
        return
        end
*
*
*
*
*
c     
       
*
      subroutine mo2out_molcas_init  
      implicit none
      include 'col_molcas.inc'
      integer nshell,nskal
      integer i, ishell_a,ishell_b,ishell_d
      integer ishell_ab,ishell_cd,ishell_abcd
      integer index_a,index_b,index_c,index_d
      integer index_ab,index_cd,index_abcd
      integer ndim_a,ndim_b,ndim_c,ndim_d,ndim_ab
      integer ndim_cd,ndim_abcd
      logical indexation,dofock,dograd
      integer maxmem
       real*8 thrao
*
       Integer Basis_Mode, kCnttp
       Common /BM/ Basis_Mode, kCnttp
       Integer Valence_Mode, Auxiliary_Mode, Fragment_Mode, 
     &  With_Auxiliary_Mode, With_Fragment_Mode, All_Mode
      Parameter (Valence_Mode=0, Auxiliary_Mode=1, Fragment_Mode=2,
     &  With_Auxiliary_Mode=3, With_Fragment_Mode=4, All_Mode=5)
*
*
      Integer Active, Inactive, Produced
      Parameter (  Active=34343434,
     &           Inactive=43344334,
     &           Produced=54334555)
      Integer GT_Status
      Integer k2_Status
      Integer PP_Status
      Integer RctFld_Status
      Integer T_Status
      Integer HerRW_Status
      Integer Sphere_Status
      Integer Info_Status
      Integer iSD_Status
      Integer Rys_Status
      Integer ERI_Status
      Integer DoFock_Status
      Integer Indexation_Status
      Integer XMem_Status
      Integer NQ_Status
      Integer Seward_Status
*@ifdef molcas_ext
*      Common /Status_Fields_/ k2_Status, RctFld_Status, GT_Status,
*     &                       PP_Status, T_Status, HerRW_Status,
*     &                       Sphere_Status, Info_Status, iSD_Status,
*     &                       Rys_Status, ERI_Status, DoFock_Status,
*     &                       Indexation_Status, 
*     &                       XMem_Status, NQ_Status, Seward_Status
*@else
      Common /Status_Fields/ k2_Status, RctFld_Status, GT_Status,
     &                       PP_Status, T_Status, HerRW_Status,
     &                       Sphere_Status, Info_Status, iSD_Status,
     &                       Rys_Status, ERI_Status, DoFock_Status,
     &                       Indexation_Status, 
     &                       XMem_Status, NQ_Status, Seward_Status
*@endif
*
      Integer IWORKLEN, ICWORKLEN
      PARAMETER (IWORKLEN=8)
      PARAMETER (ICWORKLEN=IWORKLEN)
c for boundary checks: ./configure -compiler g95 -bound      
c      PARAMETER (IWORKLEN=552 800 000)
c      PARAMETER (ICWORKLEN=552 800)
      Real*8    Work(1:IWORKLEN)
      Integer   iWork(1:IWORKLEN)
      Integer   mxMem, ip_Dummy, ip_iDummy
      Equivalence (Work,iWork)
*@ifdef molcas_ext
*      Common /WrkSpc_/Work,mxMem
*      Common /iWrkSpc_/ ip_Dummy, ip_iDummy
*@else
      Common /WrkSpc/Work,mxMem
      Common /iWrkSpc/ ip_Dummy, ip_iDummy
*@endif
!
ccomdeck shinf.inc $Revision: 2.26.8.4 $
c     ipShBF   - ptr to iWork field with dim. nShlls*nIrrep
c                iWork(ipShBF + irrep*nShlls+iShell - 1):
c                  # SO functions in irrep for shell iShell
c     ipShLC   - ptr to iWork field with dim. nShlls*nIrrep
c                iWork(ipShLC + irrep*nShlls+iShell - 1):
c                  position of 1st component of iShell in irrep
c     ipShSh   - ptr to iWork field with dim. nShlls*nIrrep
c                iWork(ipShSh + irrep*nShlls+iShell - 1):
c                  pseudo shell index of iShell in irrep
c                  (not any iShell contributes to all irreps)
c     nShIrp(0:nIrrep-1)
c              - # of shells contributing to each irrep
c     nShBFMx  - largest shell size (of all irrep, all shells)
c     ipSOSh   - ptr to iWork field with dim. nDim
c                iWork(ipSOSh+sum{0..irp-1}(nBas(irp-1))+iSO - 1):
c                  shell the SO index iSO in irp belongs to
c     ipicntr  - ptr to iwork field holding center number for
c                each shell
c
c........x.........x.........x.........x.........x.........x.........x
      Integer ipShBF,ipShLC,ipShSh,nShIrp(0:7),ipSOSh,nShBFMx,ipicntr
*@ifdef molcas_ext
*c   this applies to xlf if compiled with -qextname in molcas 
*c                                  and   -qnoextname  in columbus
*      Common /ShInf_/ ipShBF,ipShLC,ipShSh,nShIrp,ipSOSh,nShBFMx,ipicntr
*      Integer         ipiSD, indij, nindij, ipk2, nk2,
*     &                ipAux, nAux, ipZeta, MemR, ipiZet, MemI,
*     &                mSkal, ipiSOSym,nSOs, ipPD, ipFT, nFT, ipDijs,
*     &                MxDij, ipmem_int, MemMax_int
* 
**
*      Common /iSetup_/ ipiSD, indij, nindij, ipk2, nk2,
*     &                ipAux, nAux, ipZeta, MemR, ipiZet, MemI,
*     &                mSkal, ipiSOSym,nSOs, ipPD, ipFT, nFT, ipDijs,
*     &                MxDij, ipmem_int, MemMax_int
*@else
      Common /ShInf/ ipShBF,ipShLC,ipShSh,nShIrp,ipSOSh,nShBFMx,ipicntr
      Integer         ipiSD, indij, nindij, ipk2, nk2,
     &                ipAux, nAux, ipZeta, MemR, ipiZet, MemI,
     &                mSkal, ipiSOSym,nSOs, ipPD, ipFT, nFT, ipDijs,
     &                MxDij, ipmem_int, MemMax_int
 
*
      Common /iSetup/ ipiSD, indij, nindij, ipk2, nk2,
     &                ipAux, nAux, ipZeta, MemR, ipiZet, MemI,
     &                mSkal, ipiSOSym,nSOs, ipPD, ipFT, nFT, ipDijs,
     &                MxDij, ipmem_int, MemMax_int
*@endif 
      Integer info(10)
 
      
    
*
      lugamma=60
*@ifdef molcas_ext
*      call daname_mf_(lugamma,'GAMMA')
*c-----Precompute k2 entities.
*c
*      call inimem_()
*      call inisew_(info,.FALSE.,1)
*      call set_basis_mode_('Valence')
*@else
      call daname_mf(lugamma,'GAMMA')
c-----Precompute k2 entities.
c
      call inimem()
      call inisew(info,.FALSE.,1)
      call set_basis_mode('Valence')
*@endif
*
c     Call Def_Shells(iWork(ipiSD),nSD,mSkal)
*
      Indexation=.True.
      DoFock=.False.
      DoGrad=.True.
      thrao=0.0d0
*@ifdef molcas_ext
*      Call SetUp_Ints_(nSkal,Indexation,ThrAO,DoFock,DoGrad)
*@else
      Call SetUp_Ints(nSkal,Indexation,ThrAO,DoFock,DoGrad)
*@endif
      write(0,*) 'leaving setup_ints nsos=',nsos
      write(0,*) ipiSD, indij, nindij, ipk2, nk2,
     &                ipAux, nAux, ipZeta, MemR, ipiZet, MemI,
     &                mSkal, ipiSOSym,nSOs, ipPD, ipFT, nFT, ipDijs,
     &                MxDij, ipmem_int, MemMax_int
*
      mSkal=nSkal
      nPair=nSkal*(nSkal+1)/2
      nQuad =nPair*(nPair+1)/2
      nsos2=nsos
      nbin=min(10000000/(2*nquad),1024)
      idisk=0
      iwrite=1  
      write(0,*) 'mo2out_init:',mskal,npair,nquad,nsos,nbin
      return
      end
*
      subroutine mo2out_molcas_fin
      implicit none
      include 'col_molcas.inc'
      integer  iquad
      real*8 backchain
*
      write(6,*) 'dumping all quads to disk ...'
         do iquad=1,nquad
            backchain=dble(idisk)
c           write(6,111) iquad,int(bin_ptr(1,nbin,iquad)),
c    .        int(bin_ptr(2,nbin,iquad)),idisk
 111  format('dumped iquad=',i4,' # elements=',i6,' backchain=',i8,
     .      ' gtoc=',i8)
       call ddafile(lugamma,iwrite,bin_ptr(1,1,iquad),2*nbin,idisk)
            gtoc_ptr(iquad)=backchain
c           write(6,*) 'iquad,idisk=',iquad,idisk,
c    .                 ' backchain=',backchain
         enddo
      return
      end
*@endif

