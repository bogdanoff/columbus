#!/usr/bin/perl
#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/


 use Shell qw(pwd mv cp ln cat);

 @sections=();    # links the sections of columbus input
 @section_names=("GENERAL","MCSCF","MRCI","GRAD","NAC","GEOMOPT");
 %general=();
 %mrci=();
 %grad=();
 %mcscf=();


print "************************************************************************************\n";
print "*                   COLUMBUS version 7.0 beta                                      *\n";
print "*                                                                                  *\n";
print "*           multi-reference single and double excitation configuration             *\n";
print "*           interaction based on the graphical unitary group approach              *\n";
print "*                                                                                  *\n";
print "*                                 authors:                                         *\n";
print "*                                                                                  *\n";
#print "* $Revision: 1.1.6.2 $   $Date: 2013/04/11 14:43:12 $   *\n";
print "*                                                                                  *\n";
print "************************************************************************************\n";

#system(" pwd; ls -l ");

#cleanuplinks();

$input="COLUMBUSINP";
unless ( -f $input ) {  diemolcas("cannot find input file $input");}
&get_columbus_config();
&setdefaults();
&analyse_input();


if ($runmcscf) { &execute_mcscf();}
if ($runmrci) { &execute_mrci();}
if ($rungrad) { &execute_grad();}
if ($rungeomupdate) { &execute_slapaf();}

&cleanuplinks();

&stopit("ok");

exit;


sub cleanuplinks
  { my @files;
   open CURRDIR,"./";
   @files=<CURRDIR>;
   foreach ( @files ) { if ( -l $i ) { print "removing link $_\n"; unlink ($_);}}
  return;
 }   

 sub get_columbus_config
  { my @config;
    $colpath=$ENV{COLUMBUS};
    print "COLUMBUS=$colpath\n";
    if ( ! -d $colpath ) { diemolcas("COLUMBUS environment variable not set!");}
    open FIN,"<$colpath/../install.config";
    @config=<FIN>;
    close FIN;
    for ($i=0; $i<=$#config; $i++) 
    { if ( $config[$i] =~ /MPI_STARTUP/) { $mpistartup=$config[$i]; 
               $mpistartup=~s/^MPI_STARTUP//; $mpistartup=~s/_NPROC_.*$/_NPROC_ /; chop $mpistartup; last; }
    } 
    return;
 }

sub getmolcasinfo
{ 
# get information from  molcasinfo_col file
# ***EXTRACTED INFO FROM MOLCAS RUNFILE*** 
# Nuclear Repulsion Energy=        5.98892689
# NSYM =                      4
# AO BASIS FUNCTIONS =       23    11    17     7
# FROZEN CORE =               0     0     0     0
# FROZEN VIRTUAL =            0     0     0     0
# MO ORBITALS   =            23    11    17     7
# IRREP LABELS =          a1     b1     b2     a2 


  callprog("getmolcasinfo.x"," > molcasinfo_col",0 );
 open FIN,"<molcasinfo_col";
  $_=<FIN>; chop;
  $_=<FIN>; chop;
  $_=<FIN>; chop; s/[A-Za-z=]//g; s/^ *//g;
  $general{nsym}=$_; 
  $nsym=$_;
  $_=<FIN>; chop; s/[A-Za-z=]//g; s/^ *//g;
  @{$general{aobasis}}[1..$nsym]=split(/\s+/);
  $_=<FIN>; chop; s/[A-Za-z=]//g; s/^ *//g;
  @{$general{frozencore}}[1..$nsym]=split(/\s+/);
  $_=<FIN>; chop; s/[A-Za-z=]//g; s/^ *//g;
  @{$general{frozenvirt}}[1..$nsym]=split(/\s+/);
  $_=<FIN>; chop; s/[A-Za-z=]//g; s/^ *//g;
  @{$general{mobasis}}[1..$nsym]=split(/\s+/);
  $_=<FIN>; s/^.*=//; s/^ *//g;
  @{$general{irreplabels}}[1..$nsym]=split(/\s+/);
 close FIN;
return;
}

sub setdefaults{
 
  $general{coremem}=100;
  $mrci{nvcimx} =10;
  $mrci{nvcimn} =1;
  $mrci{rtol}=0.001;
  $mrci{genspace}="n";
  $mrci{finalv}=0; 
  $mrci{finalw}=-1; 
  $mrci{nbkiter}=1; 
  $mcscf{niter}=30;
  return;
}

sub analyse_cidrtls
{
  open CIDRTLS,"<cidrtls";
  while (<CIDRTLS>)
   { if (/^ total csf counts:/) {last;}
     if (/ reference csfs selected from/) {s/^ *//; ($nref,$x)=split(/\s+/);}}

  $z=<CIDRTLS>; $y=<CIDRTLS>; $x=<CIDRTLS>; $w=<CIDRTLS>;
  chop $z,$y,$x,$w;
  $z=~s/^.*://;
  $y=~s/^.*://;
  $w=~s/^.*://;
  $x=~s/^.*://;

  print "----------------- cidrtls ---------------------------\n";
  printf "configuration space:    %10d z CSFs \n",$z;
  printf "                        %10d y CSFs \n",$y;
  printf "                        %10d x CSFs \n",$x;
  printf "                        %10d w CSFs \n",$w;
  printf " total number of CSFs:  %10d  ( %6d reference configurations ) \n",$z+$y+$x+$w,$nref;
  close CIDRTLS;
  print "-----------------------------------------------------\n";
  return;
}

sub analyse_cidrtmsls
{
  open CIDRTLS,"<cidrtmsls";
   $idrt=0;
   $iidrt=0;
  while (<CIDRTLS>)
   { if (/ reference csfs selected from/) {s/^ *//; $iidrt++; 
                                         ($nref[$iidrt],$x)=split(/\s+/);}
     if (/^ total csf counts:/) { 
                                  $idrt++; $z=<CIDRTLS>; $y=<CIDRTLS>; $x=<CIDRTLS>; $w=<CIDRTLS>;
                                  chop $z,$y,$x,$w; $z=~s/^.*://;$y=~s/^.*://; $w=~s/^.*://; $x=~s/^.*://; 

# $savestr[$idrt]= sprintf("----------------- cidrtmsls  DRT %4d ----------------\n",$idrt);
  $savestr[$idrt]=  sprintf "configuration space:    %10d z CSFs \n",$z;   
  $savestr[$idrt].=sprintf "                        %10d y CSFs \n",$y ;  
  $savestr[$idrt].=sprintf "                        %10d x CSFs \n",$x ; 
  $savestr[$idrt].=sprintf "                        %10d w CSFs \n",$w ;
  $savestr[$idrt].= sprintf " total number of CSFs:  %10d  ",$z+$y+$x+$w;
 }}
  close CIDRTLS;

  for ($ldrt=1; $ldrt<=$ndrt; $ldrt++)
  { printf("----------------- cidrtmsls  DRT %4d ----------------\n",$ldrt);
    if ($genspace eq "n") { print $savestr[$idrt-($ndrt-$ldrt)*2];}
         else             { print $savestr[$idrt-($ndrt-$ldrt)];} 
   printf " ( %6d ref CSFs)\n-----------------------------------------------------\n", $nref[$ldrt];
   }
  return;
}





#---------------------------

sub stopit{
$option=shift @_;
 if ($option eq "ok") { system("echo 0 > returncode");}
 else
    { system("echo 95 > returncode");
      die( "$option\n"); }
 return;
}

sub checkterm{
 $option=shift @_;
  if ( ! -f "bummer") {return;}
 open BUMMER,"<bummer";
  $_=<BUMMER>;
 close BUMMER;
 if (!(/normally terminated/ || /warning error encountered/)) { system("echo 130 > returncode"); die ("$option failed");}
 return;
}

sub diemolcas{
 $option=shift @_;
 system("echo 130 > returncode");
# note, in $MOLCAS/src/Include/warnings.fh  return codes are defined 
# 130: general error
 die ("***** ERROR $option ***** \n");
 return;
}

#---------------------------

sub analyse_input{
open FIN,"<$input";
@ALL_INPUT=<FIN>; chomp @ALL_INPUT; 
close FIN;

while ($#ALL_INPUT) { $_=shift @ALL_INPUT; if (/\&COLUMBUS/) {last;}}
if ($#ALL_INPUT<0) {stopit("no COLUMBUS entry found in $input\n");} 
# remove leading and trailing blanks
for ($i=0; $i<=$#ALL_INPUT; $i++) { $ALL_INPUT[$i]=~s/^ *//; $ALL_INPUT[$i]=~s/ *$//; }


# get information from  runfile file
  getmolcasinfo();
# print basis info

 print "--------------- Basis set information from RUNFILE -------------\n";
 printf "%-10s"," irred. repr. "; for ($i=1;$i<=$nsym;$i++) { printf "%4s", ${$general{irreplabels}}[$i];} print "\n";
 printf "%-10s"," AO basis     "; for ($i=1;$i<=$nsym;$i++) { printf "%4d", ${$general{aobasis}}[$i];} print "\n";
 printf "%-10s"," frozen core  "; for ($i=1;$i<=$nsym;$i++) { printf "%4d", ${$general{frozencore}}[$i];} print "\n";
 printf "%-10s"," deleted virt."; for ($i=1;$i<=$nsym;$i++) { printf "%4d", ${$general{frozenvirt}}[$i];} print "\n";
 printf "%-10s"," MO basis     "; for ($i=1;$i<=$nsym;$i++) { printf "%4d", ${$general{mobasis}}[$i];} print "\n";
 print "----------------------------------------------------------------\n";
 



 for ($isection=0; $isection<=$#section_names; $isection++)
  { $sections[$isection]=extract_section($section_names[$isection],\@ALL_INPUT);
    #print "testing section $section_names[$isection] $#ALL_INPUT \n"; 
    if ($#{$sections[$isection]} > -1 ) {
           if ($isection == 1) { $runmcscf=1;}
           if ($isection == 2) { $runmrci=1;}
           if ($isection == 3) { $rungrad=1;}
           if ($isection == 4) { $runnac=1;}
           if ($isection == 5) { $rungeomupdate=1;}
           &parsetype($isection);
          print " ... initialize section ",$section_names[$isection]," \n";
         #if ($isection == 0 ) { foreach $l ( keys (%general) ) {print "general. $l:", $general{$l},"\n"; }}
         #if ($isection == 2 ) { foreach $l ( keys %mrci    ) {print "mrci.    $l  $mrci{$l}\n"; }}
          }
  }
# order of sections:
# "GENERAL","MCSCF","MRCI","GRAD","NAC","GEOMOPT"   index 0:5

# print "GENERAL","MCSCF","MRCI","GRAD","NAC","GEOMOPT","\n";
# print "   1   , $runmcscf, $runmrci, $rungrad , $runnac, $rungeomupdate \n";
  checkgeneral;
  if ($rungrad) { &checkgrad();}
  if ($runmrci) { &checkmrci();}
  if ($runmcscf) { &checkmcscf();}
 return;
}


sub checkgrad {

  if ($runmrci && ${$mrci{nroot}}[1] < $grad{root} ) 
    { print " #roots in MRCI section < root specified in GRAD section\n";
      print " setting MRCI root to GRAD root\n";
     ${$mrci{nroot}}[1]=$grad{root}; }
  if ($runmcscf && ${$mcscf{nroot}}[1] < $grad{root} ) 
    { print " #roots in MCSCF section < root specified in GRAD section\n";
      print " setting MCSCF root to GRAD root\n";
     ${$mcscf{nroot}}[1]=$grad{root}; }
  if ($runmrci && $mrci{rtol}> 0.0002) 
    { print " accuracy in in MRCI section for gradients too high\n";
      print " resetting to 0.0002 \n"; 
      $mrci{rtol}=0.0002;}
 return;
 }

#------------------------
sub printorbvec{
 my @vec,$outstr;
  @vec=@_;
# print "elements:",$#vec,join(':',@vec),"\n";
  do                    
   { $outstr=' ';
     if ( $#vec >= 12 ) { for ($i=1;$i<13;$i++) { $outstr = "$outstr " . shift(@vec);}
                         print FIN "$outstr \n";}
     $outstr=' ';
     if ($#vec < 12 && $#vec > -1) {$outstr= $outstr . join (' ',@vec) . " \n";
           print FIN  "$outstr";} 
   }
   until ($#vec < 13 );
  return;
}


#---------------------------
sub create_cidrtin { 
  if ($mrci{noauto}) {return;}
  open FIN,">cidrtin";
    print FIN " N / no spin orbit CI\n";
    print FIN " ${$mrci{multiplicity}}[1] /input the spin multiplicity \n";
    print FIN " ${$mrci{electrons}}[1][1] /input the total number of electrons \n";
    print FIN " $nsym  /input the number of irreps (1-8) \n";
    print FIN " Y /enter symmetry labels: \n";
    for ($i=1;$i<=$nsym;$i++) {print FIN ${$general{irreplabels}}[$i]," \n";}
    for ($i=1;$i<=$nsym;$i++) {print FIN ${$general{aobasis}}[$i],"  ";} print FIN " / number of basis functions \n";
    print FIN " ${$mrci{symmetry}}[1] /input the molecular spatial symmetry (irrep 1:nsym) \n";


## compute frozen core vector
     @corevec=();
     for ($i=1;$i<=$nsym;$i++) {for ($j=1; $j<=${$mrci{frozencore}}[$i];$j++) { push @corevec,$i,$j;}}
      if ($#corevec > 0 ) { printorbvec(@corevec);}
      print FIN "  /number of frozen core orbitals \n";
## compute frozen virtual vector
     @corevec=();
     for ($i=1;$i<=$nsym;$i++) {for ($j=${$general{mobasis}}[$i]-${$mrci{frozenvirt}}[$i]+1; $j<=${$general{mobasis}}[$i];$j++) 
                         { push @corevec,$i,$j;}}
      if ($#corevec > 0 ) { printorbvec(@corevec);}
      print FIN "  /number of deleted virtual orbitals \n";
## compute internal vector REFDOCC:REFRAS:REFCAS:REFAUX
     @corevec=();
     for ($i=1;$i<=$nsym;$i++) 
       {for ($j=${$mrci{frozencore}}[$i]+1; $j<=${$mrci{frozencore}}[$i]+${$mrci{refdocc}}[$i];$j++) { push @corevec,$i,$j;}}
     for ($i=1;$i<=$nsym;$i++) 
       {for ($j=${$mrci{frozencore}}[$i]+${$mrci{refdocc}}[$i]+1; $j<=${$mrci{frozencore}}[$i]+${$mrci{refdocc}}[$i]+${$mrci{refras}}[$i];$j++) { push @corevec,$i,$j;}}
     for ($i=1;$i<=$nsym;$i++) 
       {for ($j=${$mrci{frozencore}}[$i]+${$mrci{refdocc}}[$i]+${$mrci{refras}}[$i]+1; 
          $j<=${$mrci{frozencore}}[$i]+${$mrci{refdocc}}[$i]+${$mrci{refras}}[$i]+${$mrci{refcas}}[$i];$j++) { push @corevec,$i,$j;}}
     for ($i=1;$i<=$nsym;$i++) 
       {for ($j=${$mrci{frozencore}}[$i]+${$mrci{refdocc}}[$i]+${$mrci{refras}}[$i]+${$mrci{refcas}}[$i]+1; 
          $j<=${$mrci{frozencore}}[$i]+${$mrci{refdocc}}[$i]+${$mrci{refras}}[$i]+${$mrci{refcas}}[$i]+${$mrci{refaux}}[$i];$j++) { push @corevec,$i,$j;}}
      if ($#corevec > 0 ) { printorbvec(@corevec); }
      print FIN "  /internal orbitals \n";

##  need splitting of lines in all print statements!

     $refdocc=0; 
     $refaux=0;
     $refcas=0;
     $refras=0;
     for ($i=1;$i<=$nsym;$i++) {$refdocc+=${$mrci{refdocc}}[$i];
                                $refaux+=${$mrci{refaux}}[$i];
                                $mrci{nfrozen}+=${$mrci{frozencore}}[$i];
                                $refcas+=${$mrci{refcas}}[$i];
                                $refras+=${$mrci{refras}}[$i];}
     print FIN " $refdocc  /input the number of ref-csf doubly-occupied orbitals\n";

##  compute reference occupation vector occminr, occmaxr
    $nactref=${$mrci{electrons}}[1][1]-$refdocc-$refdocc-$mrci{nfrozen}-$mrci{nfrozen};
     @occminr=();
     for ($i=1;$i<$refras;$i++) {push @occminr, 0;}
     for ($i=1;$i<=$refcas;$i++) {push @occminr, $refras+$refras-${$mrci{electrons}}[1][2];}
     for ($i=1;$i<=$refaux+1;$i++) {push @occminr, $nactref-${$mrci{electrons}}[1][3];}
# falls kein aktiver Raum, darf auch nichts geschrieben werden!
#    for ($i=1;$i<=$$mrci{refaux};$i++) {push @occminr, $nactref-$electrons[3];}
     printorbvec(@occminr);
#    print FIN " ",join(' ',@occminr)," / occminr \n"; 
     @occmaxr=();
     for ($i=1;$i<$refras;$i++) {push @occmaxr, $refras+$refras;}
     for ($i=1;$i<=$refcas;$i++) {push @occmaxr, $nactref;}
     for ($i=1;$i<=$refaux+1;$i++) {push @occmaxr, $nactref;}
#    for ($i=1;$i<=$$mrci{refaux};$i++) {push @occmaxr, $nactref;}
     printorbvec(@occmaxr);
#    print FIN " / occmaxr \n"; 
#    print FIN " ",join(' ',@occmaxr)," / occmaxr \n"; 
     print FIN "  / bminr \n";
     print FIN "  / bmaxr \n";
     @stepr=();
     for ($i=1;$i<=$refras+$refcas+$refaux;$i++) {push @stepr, 1111;}
     printorbvec(@stepr);
#    print FIN "  / stepr\n"; 
     print FIN "  $mrci{exlevel} /input the maximum excitation level from the reference csfs \n";
     print FIN "  / default occmin \n";
     print FIN "  / default occmax \n";
     print FIN "  / default bmin   \n";
     print FIN "  / default bmax   \n";
     print FIN "  / default step   \n";
     print FIN " -1 / manual arc removing step \n";
     print FIN " $mrci{genspace} /impose generalized interacting space restrictions? \n";
     print FIN " -1 / manual arc removing step \n";
     print FIN "  / default printing drt \n";
     print FIN " ${$mrci{symmetry}}[1]  / default reference symmetry\n"; 
     print FIN " N /keep all of the z-walks as references? \n";
     print FIN " Y /generate walks while applying reference drt restrictions? \n";
     print FIN " N /impose additional orbital-group occupation restrictions? \n";
     print FIN " N /apply primary reference occupation restrictions? \n";
     print FIN " N /manually select individual walks? \n";
     @weight=();
     for ($i=1;$i<=$refdocc+$refras+$refcas+$refaux;$i++) {push @weight, 4;}
     $weight[0]=-1;
     printorbvec(@weight);
     print FIN "  / defaults \n";
     print FIN " 0 /input reference walk number (0 to end) \n";
     @weight=();
     for ($i=1;$i<=$refdocc+$refras+$refcas+$refaux;$i++) {push @weight, 0;}
     printorbvec(@weight);
     print FIN " /  \n";
     print FIN " N /this is an obsolete prompt. \n";
     print FIN " 0 /input mrsdci walk number (0 to end) \n"; 
     print FIN " $mrci{title} \n";
     print FIN "cidrtfl\n";
     print FIN " Y /write the drt file?\n";
     close FIN;
   # system("cat cidrtin");
  return;}


sub create_cisrtin { 
  if ($noauto) {return;}
  open CISRTIN, ">cisrtin";
   print CISRTIN " \&input\n molcas=0 \n maxbl3=60000\n maxbl4=60000\n \&end\n";
  close  CISRTIN;
return;}


sub create_ciudgin {
  if ($general{noauto}) {return;}
  open CIUDGIN, ">ciudgin";
  print CIUDGIN " \&input\n";
  print CIUDGIN " iden=0\n nroot=${$mrci{nroot}}[1]\n";
  print CIUDGIN " nciitr=$mrci{nciiter}\n nbkitr=$mrci{nbkiter}\n";
  print CIUDGIN " nvcimx=$mrci{nvcimx} \n nvbkmx=$mrci{nvcimx} \n nvrfmx=$mrci{nvcimx}\n";
  print CIUDGIN " nvcimn=$mrci{nvcimn} \n nvbkmn=$mrci{nvcimn} \n nvrfmn=$mrci{nvcimn}\n";
  for ($i=1; $i<=${$mrci{nroot}}[1]; $i++) { push @tol, $mrci{rtol};}
  print CIUDGIN " rtolci= ",join(',',@tol)," \n";
  print CIUDGIN " rtolbk= ",join(',',@tol)," \n";
  print CIUDGIN " csfprn=10 \n";
  if ($mrci{mracpf}) { print CIUDGIN " NTYPE=3, GSET=2 \n";}
  elsif ($mrci{mraqcc}) { print CIUDGIN " NTYPE=3, GSET=3 \n";}
  elsif ($mrci{mraqccv}) { print CIUDGIN " NTYPE=3, GSET=4 \n";}
  else { print CIUDGIN " DAVCOR=10\n";}
  if (grep(/iterative/i,$mrci{refspace})) { print CIUDGIN " IVMODE=8 \n";}
  elsif (grep(/full/i,$mrci{refspace}))  {print CIUDGIN " IVMODE=3 \n";}
  else {print CIUDGIN " IVMODE=0 \n";}
  print CIUDGIN " \MOLCAS=1 \n";
  if ($rungrad) {print CIUDGIN " IDEN=2\n";}
  if ($mrci{parallel}) 
    {  # add parallel input 
       print CIUDGIN " MAXSEG=8 \n ";
       print CIUDGIN " NSEG0X=1,1,2,2 \n ";
       print CIUDGIN " NSEG1X=1,1,2,2 \n ";
       print CIUDGIN " NSEG2X=1,1,2,2 \n ";
       print CIUDGIN " NSEGWX=1,1,2,2 \n ";
       print CIUDGIN " NSEG3X=1,1,2,2 \n ";
       print CIUDGIN " NSEG4X=1,1,2,2 \n ";
       print CIUDGIN " FILELOC=1,1,2,2,2,2,2,2 \n ";
       print CIUDGIN " FINALV= $mrci{finalv}, FINALW=$mrci{finalw} \n"; 
     }
       
   print CIUDGIN " /\&end \n";
  close CIUDGIN;   
  return;}

sub create_ciudginms {
  if ($general{noauto}) {return;}
  for ($idrt=1; $idrt<=$mrci{ndrt}; $idrt++)
  {
  open CIUDGIN, ">ciudgin.drt$idrt";
  print CIUDGIN " \&input\n";
  print CIUDGIN " iden=0\n nroot=$nroot[$idrt]\n";
  print CIUDGIN " nciitr=$mrci{nciiter}\n nbkitr=$mrci{nbkiter}\n";
  print CIUDGIN " nvcimx=$mrci{nvcimx} \n nvbkmx=$mrci{nvcimx} \n nvrfmx=$mrci{nvcimx}\n";
  print CIUDGIN " nvcimn=$mrci{nvcimn} \n nvbkmn=$mrci{nvcimn} \n nvrfmn=$mrci{nvcimn}\n";
  for ($i=1; $i<=$$mrci{nroot}[$idrt]; $i++) { push @tol, $mrci{rtol};}
  print CIUDGIN " rtolci= ",join(',',@tol)," \n";
  print CIUDGIN " rtolbk= ",join(',',@tol)," \n";
  print CIUDGIN " csfprn=10 \n";
  if ($mrci{mracpf}) { print CIUDGIN " NTYPE=3, GSET=2 \n";}
  elsif ($mrci{mraqcc}) { print CIUDGIN " NTYPE=3, GSET=3 \n";}
  elsif ($mrci{mraqccv}) { print CIUDGIN " NTYPE=3, GSET=4 \n";}
  else { print CIUDGIN " DAVCOR=10\n";}
  if (grep(/iterative/i,$mrci{refspace})) { print CIUDGIN " IVMODE=8 \n";}
  elsif (grep(/full/i,$mrci{refspace}))  {print CIUDGIN " IVMODE=3 \n";}
  else {print CIUDGIN " IVMODE=0 \n";}
  if ($mrci{parallel})
    {  # add parallel input 
       print CIUDGIN " MAXSEG=8 \n ";
       print CIUDGIN " NSEG0X=1,1,2,2 \n ";
       print CIUDGIN " NSEG1X=1,1,2,2 \n ";
       print CIUDGIN " NSEG2X=1,1,2,2 \n ";
       print CIUDGIN " NSEGWX=1,1,2,2 \n ";
       print CIUDGIN " NSEG3X=1,1,2,2 \n ";
       print CIUDGIN " NSEG4X=1,1,2,2 \n ";
       print CIUDGIN " FILELOC=1,1,2,2,2,2,2,2 \n ";
       print CIUDGIN " FINALV= $mrci{finalv}, FINALW=$mrci{finalw} \n";
     }
   print CIUDGIN " /\&end \n";
  close CIUDGIN;
  }
  return;}



sub create_cidrtmsin {
  if ($noauto) {return;}
  open FIN,">cidrtmsin";
    print FIN " $mrci{ndrt} / number of DRTs \n";
    print FIN " N / no spin orbit CI\n";
    print FIN " $$mrci{multiplicity}[1] /input the spin multiplicity \n";
    print FIN " $$mrci{electrons}[1][1] /input the total number of electrons \n";
    print FIN " $nsym  /input the number of irreps (1-8) \n";
    print FIN " Y /enter symmetry labels: \n";
    for ($i=1;$i<=$nsym;$i++) {print FIN "$$general{irreplabels}[$i] \n";}
    for ($i=1;$i<=$nsym;$i++) {print FIN " $$general{aobasis}[$i] ";} print FIN " / number of basis functions \n";
## compute frozen core vector
     @corevec=();
     for ($i=1;$i<=$nsym;$i++) {for ($j=1; $j<=$$mrci{frozencore}[$i];$j++) { push @corevec,$i,$j;}}
      if ($#corevec > 0 ) { printorbvec(@corevec);}
      print FIN "  /number of frozen core orbitals \n";
## compute frozen virtual vector
     @corevec=();
     for ($i=1;$i<=$nsym;$i++) {for ($j=$$general{mobasis}[$i]-$frozenvirt[$i]+1; $j<=$$general{mobasis}[$i];$j++) { push @corevec,$i,$j;}}
      if ($#corevec > 0 ) { printorbvec(@corevec); }
      print FIN "  /number of deleted virtual orbitals \n";
## compute internal vector REFDOCC:REFRAS:REFCAS:REFAUX
     @corevec=();
     for ($i=1;$i<=$nsym;$i++)
       {for ($j=$$mrci{frozencore}[$i]+1; $j<=$$mrci{frozencore}[$i]+$$mrci{refdocc}[$i];$j++) { push @corevec,$i,$j;}}
     for ($i=1;$i<=$nsym;$i++)
       {for ($j=$$mrci{frozencore}[$i]+$$mrci{refdocc}[$i]+1; $j<=$$mrci{frozencore}[$i]+$$mrci{refdocc}[$i]+$$mrci{refras}[$i];$j++) { push @corevec,$i,$j;}}
     for ($i=1;$i<=$nsym;$i++)
       {for ($j=$$mrci{frozencore}[$i]+$$mrci{refdocc}[$i]+$$mrci{refras}[$i]+1;
          $j<=$$mrci{frozencore}[$i]+$$mrci{refdocc}[$i]+$$mrci{refras}[$i]+$$mrci{refcas}[$i];$j++) { push @corevec,$i,$j;}}
     for ($i=1;$i<=$nsym;$i++)
       {for ($j=$$mrci{frozencore}[$i]+$$mrci{refdocc}[$i]+$$mrci{refras}[$i]+$$mrci{refcas}[$i]+1;
          $j<=$$mrci{frozencore}[$i]+$$mrci{refdocc}[$i]+$$mrci{refras}[$i]+$$mrci{refcas}[$i]+$$mrci{refaux}[$i];$j++) { push @corevec,$i,$j;}}
      if ($#corevec > 0 ) { printorbvec(@corevec);}
      print FIN "  /internal orbitals \n";

##  need splitting of lines in all print statements!

     $refdocc=0;
     $refaux=0;
     $refcas=0;
     $refras=0;
     for ($i=1;$i<=$nsym;$i++) {$refdocc+=$refdocc[$i];
                                $refaux+=$refaux[$i];
                                $refcas+=$refcas[$i];
                                $refras+=$refras[$i];}

      for ($idrt=1; $idrt <=$mrci{ndrt}; $idrt++)
       { print FIN " $$mrci{symmetry}[$idrt] / input the molecular spatial symmetry\n";
         print FIN " $refdocc  /input the number of ref-csf doubly-occupied orbitals\n";
##       compute reference occupation vector occminr, occmaxr
         $nactref=$$mrci{electrons}[$idrt][1]-$refdocc-$refdocc;
         for ($i=1;$i<=$nsym;$i++) {$nactref=$nactref-$$mrci{frozencore}[$i]*2;}
         @occminr=();
         for ($i=1;$i<$refras;$i++) {push @occminr, 0;}
         for ($i=1;$i<=$refcas;$i++) {push @occminr, $refras+$refras-$mrci{electrons}[$idrt][2];}
         for ($i=1;$i<=$refaux+1;$i++) {push @occminr, $nactref-$mrci{electrons}[$idrt][3];}
         print FIN " ",join(' ',@occminr)," / occminr \n";
         @occmaxr=();
         for ($i=1;$i<$refras;$i++) {push @occmaxr, $refras+$refras;}
         for ($i=1;$i<=$refcas;$i++) {push @occmaxr, $nactref;}
         for ($i=1;$i<=$refaux+1;$i++) {push @occmaxr, $nactref;}
         print FIN " ",join(' ',@occmaxr),"  / occmaxr\n";
         print FIN "  / bminr \n";
         print FIN "  / bmaxr \n";
         @stepr=();
         for ($i=1;$i<=$refras+$refcas+$refaux;$i++) {push @stepr, 1111;}
         printorbvec(@stepr);
#        print FIN "  / stepr\n";
         print FIN "  $exlevel /input the maximum excitation level from the reference csfs \n";
         print FIN "  / default occmin \n";
         print FIN "  / default occmax \n";
         print FIN "  / default bmin   \n";
         print FIN "  / default bmax   \n";
         print FIN "  / default step   \n";
        }
     print FIN " $genspace /impose generalized interacting space restrictions? \n";
     print FIN "  / default printing drt \n";
     print FIN "  / default printing drt \n";
     print FIN "  / default printing drt \n";



      for ($idrt=1; $idrt <=$mrci{ndrt}; $idrt++)
      { print FIN " $$mrci{symmetry}[$idrt]  / default reference symmetry\n";
        print FIN " N /keep all of the z-walks as references? \n";
        print FIN " Y /generate walks while applying reference drt restrictions? \n";
        print FIN " N /impose additional orbital-group occupation restrictions? \n";
        print FIN " N /apply primary reference occupation restrictions? \n";
        print FIN " N /manually select individual walks? \n";
        print FIN "  / defaults \n"; 
        print FIN " 0 /input reference walk number (0 to end) \n";
        print FIN " 0 0 0 0 0 0 0 0 \n";
      }

      for ($idrt=1; $idrt <=$mrci{ndrt}; $idrt++)
       { print FIN " 0 /input mrsdci walk number (0 to end) \n";
         print FIN " / \n";
       }
       print FIN " $mrci{title} \n";
     close FIN;

  return;
 }

 sub run_tran
 { # create tranin
   open TRAN,">tranin";
    print TRAN " &input\n SEWARD=1\n LUMORB=1\n THRESH= 1.d-10 \n &end\n";
   close TRAN;
   # create link to RASSCF orbitals
    if (! $runmcscf ){ cp ("$ENV{Project}.RasOrb","mocoef_lumorb");}
     else            { cp ("mocoef_mc.lumorb mocoef_lumorb");}
   # create links to ORDINT, ONEINT
    ln("-s", "$ENV{Project}.OrdInt","ORDINT");
    ln("-s","$ENV{Project}.OneInt","ONEINT");
    print " ... executing AO-MO transformation \n";
    callprog("tran.x"," ",0 );
   return;
 }

  sub getwtimes
   {$/="";
   #print "reading performance data...\n"; 
   open FIN,"performance_data"; 
   while (<FIN>) { if (/wall clock times for the individual iterations/) { print ; last;}}
   close FIN;
   $/="\n";
  }

 sub extract_section{
  my $section,$allinput,@section; 
  ($section,$allinput)= @_;
  @section=();
  my $i;
  my $found=0;
  for ($i=0; $i<=$#$allinput; $i++)
  { $_=$$allinput[$i];
    if ($_ =~ /END_SECTION_$section/) { $found=0; last;} 
    if ( $_ =~/BEGIN_SECTION_$section/ ) {  $found=1; next;} 
    if ( $found ) { s/^ *//; 
                    push @section, $_;}
  }
# print "found section $section \n";
# print join(':',@section),"\n";
  return \@section;
  } 


  sub parsetype {
   my ($type)=@_;
# order of sections:
# "GENERAL","MCSCF","MRCI","GRAD","NAC","GEOMOPT"   index 0:5
   if ($type == 0) 
    {   # general section
        while ($#{$sections[$type]} > -1 )
        { $_ = shift @{$sections[$type]};  
          if (/^Title/i) { $general{title}= shift @{$sections[$type]};next; }      
          if (/^NOAUTO/i) {$general{noauto}=1;next;}
          if (/^TEST/i) {$general{runtest}=1;next;}
          if (/^MEMORY/i) {$inp=shift @{$sections[$type]};
                           if (/MB/i) {$fac=1024*128; } else {$fac=1;}
                           $inp=~s/\D//g; 
                           $general{coremem}=$inp*$fac;
                          next;}
          if (/^NCPU/i) {$general{ncpu}=shift @{$sections[$type]};next;}
          if (/^PRINT/i) {$general{print}=shift @{$sections[$type]};next;}
          die("found invalid keyword: $_\n");
        }
      return;
    }

   if ($type == 1)
    {   # mcscf 
        while ($#{$sections[$type]} > -1 )
        { $_ = shift @{$sections[$type]}; 
          if (/^Title/i) { $mcscf{title}= shift @{$sections[$type]};next; }
          if (/^ITERATIONS/i) {$inp=shift @{$sections[$type]};
                               $inp=~s/^ *//; @inp =split(/\s+/,$inp); if ($inp[0]) {$mcscf{niter}=$inp[0];}
                                if ($inp[1]) {$mcscf{nciiter}=$inp[1];} if ($inp[2]) {$mcscf{nmicroiter}=$inp[2]}; next;}
          if (/^ACCURACY/i)  {$inp=shift @{$sections[$type]}; 
                              $inp=~s/^ *//; ($mcscf{wnorm},$mcscf{knorm},$mcscf{apxde})=split(/\s+/,$inp);next;}
          if (/^DOCC/i) {$inp=shift @{$sections[$type]}; $inp=~s/^ *//;
                            @{$mcscf{docc}}[1..$nsym]= split(/\s+/,$inp); next;}
          if (/^RAS/i) {$inp=shift @{$sections[$type]}; $inp=~s/^ *//;
                            @{$mcscf{ras}}[1..$nsym]= split(/\s+/,$inp); next;}
          if (/^CAS/i) {$inp=shift @{$sections[$type]}; $inp=~s/^ *//;
                            @{$mcscf{cas}}[1..$nsym]= split(/\s+/,$inp); next;}
          if (/^AUX/i) {$inp=shift @{$sections[$type]}; $inp=~s/^ *//;
                            @{$mcscf{aux}}[1..$nsym]= split(/\s+/,$inp); next;}
          if (/^DIAG/i) {$mcscf{diag}=shift @{$sections[$type]};; next;}
          if (/^DRT$/i) { $mcscf{ndrt}++; $indrt=$mcscf{ndrt}; next;}
          if (/^Multiplicity/i) {${$mcscf{multiplicity}}[$indrt]=shift @{$sections[$type]}; next;}
          if (/^Electrons/i) {$inp=shift @{$sections[$type]}; $inp=~s/^ *//;
                              @{${$mcscf{electrons}}[$indrt]}[1..3] = split(/\s+/,$inp);next;}
          if (/^Symmetry/i) {${$mcscf{symmetry}}[$indrt]=shift @{$sections[$type]}; next;}
          if (/^NROOT/i) {${$mcscf{nroot}}[$indrt]=shift @{$sections[$type]}; next;}
          if (/END_DRT/i)  {$indrt=0;next;}
          die("found invalid keyword: $_\n");
        }
      return;
    }

   if ($type == 2)
    {   # mrci  
        while ($#{$sections[$type]} > -1 )
        { $_ = shift @{$sections[$type]}; 
          if (/^Title/i) { $mrci{title}= shift @{$sections[$type]};next; }
          if (/^EXLVL/i) {$mrci{exlevel}= shift @{$sections[$type]}; next; }
          if (/^SUBSPACEDIM/i) {$inp=shift @{$sections[$type]};
                                $inp=~s/^ *//; @inp=split(/\s+/,$inp); 
                                if ($inp[0] >0 ) { $mrci{nvcimx}=$inp[0];} 
                                if ($inp[1] >0 ) { print "SETTING nvcimn=$inp[1]\n"; $mrci{nvcimn}=$inp[1];} next;}
          if (/^ITERATIONS/i) {$inp=shift @{$sections[$type]}; 
                               $inp=~s/^ *//; ($mrci{nciiter},$mrci{nbkiter})=split(/\s+/,$inp); next;}
          if (/^ACCURACY/i)  {$mrci{rtol}=shift @{$sections[$type]}; next;}
          if (/^MRAQCC/i)    {$mrci{mraqcc}=1; next;}
          if (/^MRACPF/i)    {$mrci{mracpf}=1; next;}
          if (/^MRAQCC-V/i)  {$mrci{mraqccv}=1; next;}
          if (/^MRCISD/i)    {$mrci{mrcisd}=1; next;}
          if (/^FROZENCORE/i) {$inp=shift @{$sections[$type]}; $inp=~s/^ *//;  
                            @{$mrci{frozencore}}[1..$nsym]= split(/\s+/,$inp); next;}
          if (/^REFDOCC/i) {$inp=shift @{$sections[$type]}; $inp=~s/^ *//;  
                            @{$mrci{refdocc}}[1..$nsym]= split(/\s+/,$inp); next;}
          if (/^REFRAS/i) {$inp=shift @{$sections[$type]}; $inp=~s/^ *//; 
                            @{$mrci{refras}}[1..$nsym]= split(/\s+/,$inp); next;}
          if (/^REFCAS/i) {$inp=shift @{$sections[$type]}; $inp=~s/^ *//; 
                            @{$mrci{refcas}}[1..$nsym]= split(/\s+/,$inp); next;}
          if (/^REFAUX/i) {$inp=shift @{$sections[$type]}; $inp=~s/^ *//;  
                            @{$mrci{refaux}}[1..$nsym]= split(/\s+/,$inp); next;}
          if (/^GENSPACE/i) {$mrci{genspace}="y"; next;}
          if (/^REFSPACE/i) {$mrci{refspace}=shift @{$sections[$type]};; next;}
          if (/^PARALLEL/i) {$mrci{parallel}=1; next;}
          if (/^DRT$/i) { $mrci{ndrt}++; $indrt=$mrci{ndrt}; next;}
          if (/^Multiplicity/i) {${$mrci{multiplicity}}[$indrt]=shift @{$sections[$type]}; next;}
          if (/^Electrons/i) {$inp=shift @{$sections[$type]}; $inp=~s/^ *//;  
                              @{${$mrci{electrons}}[$indrt]}[1..3] = split(/\s+/,$inp);next;}   
          if (/^Symmetry/i) {${$mrci{symmetry}}[$indrt]=shift @{$sections[$type]}; next;}
          if (/^NROOT/i) {${$mrci{nroot}}[$indrt]=shift @{$sections[$type]}; next;}
          if (/END_DRT/i)  {$indrt=0;next;}
          die("found invalid keyword: $_\n");
        }
      return;
    } 

   if ($type == 3)
    {   # grad 
        while ($#{$sections[$type]} > -1 )
        { $_ = shift @{$sections[$type]}; 
          if (/^Title/i) { $grad{title}= shift @{$sections[$type]} }
          if (/^ROOT/i) { $grad{root}= shift @{$sections[$type]} }
          if (/^PARALLEL_AOMO/i) { $grad{parallelaomo}= shift @{$sections[$type]} }
          if (/^PARALLEL_CIGRD/i) { $grad{parallelcigrd}= shift @{$sections[$type]} }
          if (/^PARALLEL_ALASKA/i) { $grad{parallelalaska}= shift @{$sections[$type]} }
        }
      return;
    }

   if ($type == 4)
    {   # nac 
        %nac=();
        while ($#{$sections[$type]} > -1 )
        { $_ = shift @{$sections[$type]}; 
          if (/^Title/i) { $nac{title}= shift @{$sections[$type]} }
        }
      return;
    }

   if ($type == 5)
    {   # geomopt 
        %geomopt=();
        while ($#{$sections[$type]} > -1 )
        { $_ = shift @{$sections[$type]}; 
          if (/^Title/i) { $geomopt{title}= shift @{$sections[$type]} }
        }
      return;
    }
   return;
  }
  
  sub checkgeneral{
   my ($i,$err); 
  if (! $colpath ) { diemolcas ("COLPATH keyword missing\n");}
  foreach $i ( "ciudg.x", "cidrt.x", "cidrtms.x", "pciudg.x", "tran.x", "mcdrt.x", "mcuft.x", "mcscf.x", "cigrd.x" )
   { if ( ! -f "$colpath/$i" ) { print "invalid COLPATH: $colpath/ciudg.x missing\n"; $err++; }}
  if ($err) {diemolcas("exiting ... \n"); }

 print "------------------ runtime options ------------------\n";
 printf "%-15s  %8.3f MB \n ", "columbus main memory ", $general{coremem}*8/(1024*1024);
 if ($general{runtest}) { print " +++++++ TESTING INPUT ++++++++++++++++++++\n";}
 print "----------------------------------------------------\n";
   return;
   }

  sub checkmrci{


  if ($mrci{exlevel} < 0 || $mrci{exlevel} > 2 ) { diemolcas ("invalid EXLVL entry: $mrci{exlevel}\n");}
  if ($mrci{nvcimx} <= 0 || $mrci{nvicmx} > 40 ) { diemolcas ("invalid (first) entry for SUBSPACEDIM: $mrci{nvcimx} \n");}
  if ($mrci{nvcimn} <= 0  || $mrci{nvcimn} >= $mrci{nvcimx}) 
                                     { diemolcas ("invalid entry for SUBSPACEDIM: $mrci{nvcimx} \n");}
  if ($mrci{nciiter} <=0 || $mrci{nbkiter} < 0 ) { diemolcas("invalid entries for ITERATION: $mrci{nciiter},$mrci{nbkiter}\n");}
  if ($mrci{rtol} > 1 || $mrci{rtol} < 0 ) { diemolcas ("invalid or unreasonable entries for ACCURACY: $mrci{rtol} \n");}
  if ( ($mrci{mraqcc}+$mrci{mracpf}+$mrci{mraqccv}+$mrci{mrcisd}) != 1 ) 
        { diemolcas ("only one keyword out of MRCISD, MRACPF, MRQCC, MRAQCC-V must appear\n");}
  if ($mrci{refspace}) { if (! grep (/none|full|iterative/i,$mrci{refspace})) { diemolcas(" invalid REFSPACE entry:$mrci{refspace}\n");}}
               else { $mrci{refspace}='FULL';}
  print "\n";
 print "--------------SECTION MRCI: general specifications -------------\n";
 $i=1;
 printf "%-10s"," internal MOs "; 
 for ($i=1;$i<=$nsym;$i++) { 
     ${$mrci{internalspace}}[$i]= ${$mrci{refdocc}}[$i]+${mrci{refcas}}[$i]+${$mrci{refras}}[$i]+${$mrci{refaux}}[$i];
     printf "%4d",${$mrci{internalspace}}[$i];} 
 print "\n"; $zeroext=0;
 printf "%-10s"," external MOs "; 
  for ($i=1;$i<=$nsym;$i++) { printf "%4d", ${$general{mobasis}}[$i]-${$mrci{internalspace}}[$i]-${$mrci{frozencore}}[$i];
                            if (${$general{mobasis}}[$i]-${$mrci{internalspace}}[$i]-${$mrci{frozencore}}[$i] <=0 ) {$zeroext++;}} 
 print "\n";
 if ($zeroext) {diemolcas("Due to a bug computations with zero external MOs are temporarily disabled!\n");}

 # run some checks 
  $error=0;
  for ( $i=1;$i<=$nsym;$i++) {
       if (${$mrci{internalspace}}[$i]>${$general{mobasis}}[$i])
       { print "inconsistent orbital numbers in irreducible representation $i\n";
         $error++;}}
  if ($error) {diemolcas ("inconsistent orbital numbers in $error irreps\n");}

# some checks 
  for ($idrt=2; $idrt<=$mrci{ndrt}; $idrt++)
   { if ($$mrci{electrons}[$idrt][1] != $$mrci{electrons}[1][1] )
       { diemolcas("MULTIDRT: total number of electrons must not vary among the DRTs\n");}
   }
   for ($idrt=1; $idrt<=$mrci{ndrt}; $idrt++)
   { if (${$mrci{nroot}}[$idrt] > $mrci{nvcimx} +1 ) { print "INFO: max. SUBSPACEDIMENSION too low for desired number of roots,\n";
                                        printf "INFO: resetting maximum subspace dimension to %4d \n",${$mrci{nroot}}[$idrt]+4;
                                        $mrci{nvcimx}=${$mrci{nroot}}[$idrt]+4;}
     if (${$mrci{nroot}}[$idrt] > $mrci{nvcimn} ) { print "INFO: min. SUBSPACEDIMENSION too low for desired number of roots \n";
                                     printf "INFO: resetting minimum subspace dimension to %4d \n",${$mrci{nroot}}[$idrt] ;
                                     $mrci{nvcimn}=${$mrci{nroot}}[$idrt];}
     if (${$mrci{symmetry}}[$idrt] <=0 || ${$mrci{symmetry}}[$idrt] > $nsym) {diemolcas("invalid SYMMETRY: valid range 1 .. $nsym \n");}

   }

 for ($idrt=1; $idrt<=$mrci{ndrt}; $idrt++)
 {
 print "--------- configuration space specification  (DRT $idrt) --------\n";
 if ($mrci{title}) {printf " %-25s %-80s\n", "Title:",  $mrci{title};}
 if (${$mrci{electrons}}[$idrt]) {printf " %-25s total: %4d  holes:%4d  auxel: %4d \n", "Electrons:", @{${$mrci{electrons}}[$idrt]}[1..3];}
 if (${$mrci{multiplicity}}[$idrt]) {printf " %-25s %4d \n", "Multiplicity:",  ${$mrci{multiplicity}}[$idrt];}
 if (${$mrci{symmetry}}[$idrt]) {printf " %-25s %4d \n", "Symmetry     ",  ${$mrci{symmetry}}[$idrt];    }
 if ($mrci{exlevel}) {printf " %-25s %4d \n", "Excitation level ",$mrci{exlevel}; }
 printf " %-25s %4d \n", "number of roots ",${$mrci{nroot}}[$idrt];
 printf " %-25s "," reference docc"; for ($i=1;$i<=$nsym;$i++) { printf "%4d", ${$mrci{refdocc}}[$i];} print "\n";
 printf " %-25s "," reference ras"; for ($i=1;$i<=$nsym;$i++) { printf "%4d", ${$mrci{refras}}[$i];} print "\n";
 printf " %-25s "," reference cas"; for ($i=1;$i<=$nsym;$i++) { printf "%4d", ${$mrci{refcas}}[$i];} print "\n";
 printf " %-25s "," reference aux"; for ($i=1;$i<=$nsym;$i++) { printf "%4d", ${$mrci{refaux}}[$i];} print "\n";
 printf " %-25s "," frozen core  "; for ($i=1;$i<=$nsym;$i++) { printf "%4d", ${$mrci{frozencore}}[$i];} print "\n";
 print "----------------------------------------------------\n";
 }



 # make sure that there are at least two active orbitals
 $tot=0;
 for ($i=1;$i<=$nsym;$i++) {$tot=$tot+${$mrci{internalspace}}[$i];}
 if ($tot < 2){ diemolcas("specify at least two active orbitals ");}

#if ($general{print}) { print "* nvcimx=$mrci{nvcimx} \n";
#                       print "* nvcimn=$mrci{nvcimn} \n"; }

  return;
  }


 sub execute_mrci
 {
create_cisrtin();

# create all input files and run cidrt.x/cidrtms.x

if ( $mrci{ndrt} == 1 ) 
                { create_cidrtin();
                  callprog("cidrt.x","<cidrtin > cidrtls ",0 );
                  analyse_cidrtls();
                  create_ciudgin();
                  if ($general{runtest}) {stopit("testing input");}
                }
else            { create_cidrtmsin();
                  callprog("cidrtms.x","<cidrtmsin > cidrtmsls ",0 );
                  analyse_cidrtmsls();
                  create_ciudginms();
                  if ($general{runtest}) {stopit("testing input");}
                }

# execute AO-MO transformation, ciudg.x/pciudg.x and wavefunction analysis

if ($mrci{ndrt}==1) {
                  run_tran();
                  $mpirun=$mpistartup; $mpirun=~s/_NPROC_/$general{ncpu}/;
                  callprog("ciudg.x"," ",$mrci{parallel} );
                  system("cat  ciudgsm");
                  if ($mrci{parallel})
                   {system("$colpath/perlscripts/tmodel.pl ciudg.timings > performance_data");
                    print "\n\n **** parallelization degree per Davidson iterations: ****\n";
                    getwtimes();
                    print "for more details see performance_data\n\n\n";}
                  open CIPCIN,">cipcin";
                  print CIPCIN " 1\n";
                  close CIPCIN;
                  callprog("cipc.x","<cipcin > cipcls",0);
                  $w=pwd();chop $w;
                     print "====> full listing          on $w/ciudgls\n";
                     print "====> short listing         on $w/ciudgsm\n";
                   print "====> wavefunction analysis on $w/cipcls\n";
                 }
else             { for ($idrt=1; $idrt<=$mrci{ndrt}; $idrt++)
                   { print "=========================  MRCI for DRT $idrt ===============================\n";
                     cp("-f","ciudgin.drt$idrt","ciudgin");
                     unlink cidrtfl;
                     ln("-s","cidrtfl.$idrt","cidrtfl");
                     callprog("ciudg.x"," ",$mrci{parallel} );
                     system("cat ciudgsm");
                     if ($mrci{parallel})
                     {system("$colpath/perlscripts/tmodel.pl ciudg.timings > performance_data");
                     print "parallelization degree per Davidson iterations:\n";
                     getwtimes();
                     print "for more details see performance.data\n";}
                     open CIPCIN,">cipcin";
                     print CIPCIN " 1\n";
                     close CIPCIN;
                     callprog("cipc.x","<cipcin > cipcls",0);
                     mv("ciudgls","ciudgls.drt$idrt"); mv("ciudgsm","ciudgsm.drt$idrt");
                     mv("civout","civout.drt$idrt"); mv("civfl","civfl.drt$idrt");
                     $w=pwd();chop $w;
                     print "====> full listing          on $w/ciudgls.drt$idrt\n";
                     print "====> short listing         on $w/ciudgsm.drt$idrt\n";
                     print "====> wavefunction analysis on $w/cipcls.drt$idrt\n";
                   }
                 }
# NOs in molcas format schreiben und molcas property aufrufen

   return;
}

  sub create_mcscfin
  { open FIN,">mcscfin";
    print FIN "  &input \n NPATH=1,2,3,9,10";
    if ($mcscf{diag} =~ /hmat_full/ ) { print FIN ",11,-12"; }
    if ($mcscf{diag} =~ /^hmat_iter/ ) { print FIN ",11,12"; }
    if ($mcscf{diag} =~ /nohmat_iter/ ) { print FIN ",-11,12"; }
    print FIN ",-13,17,19,21,26,29 \n";
  # in-core version 31
  # print MOs 32
    for ($idrt=1; $idrt<=$mcscf{ndrt}; $idrt++) 
     { print FIN " NAVST( $idrt ) = ",${$mcscf{nroot}}[$idrt],"\n";
        for ($j=1; $j<= ${$mcscf{nroot}}[$idrt]; $j++) 
       { print FIN "WAVST($idrt,$j) =  1.000 \n"; }}
  # CDIR = 1
    if ($mcscf{niter}) {print FIN " NITER=",$mcscf{niter},"\n";}
    if ($mcscf{nmiter}) {print FIN " NMITER=",$mcscf{nmiter},"\n"; }
    if ($mcscf{nciiter}) {print FIN " NCIITR=",$mcscf{nciiter},"\n";}
  # NSTATE
    if ($mcscf{wnorm}) {print FIN " TOL(2)= $mcscf{wnorm} \n";}
    if ($mcscf{knorm}) {print FIN " TOL(3)= $mcscf{knorm} \n";}
    if ($mcscf{apxde}) {print FIN " TOL(4)= $mcscf{apxde} \n";}
    print FIN " FCIORB= ";
    printorbvec(@{$mcscf{fciorb}});
    print FIN "\n/&end\n";
    close FIN;
    return;
   }


  sub checkmcscf{
  if ($mcscf{diag}) { if (! grep (/hmat_full|hmat_iter|nohmat_iter/i,$mcscf{refspace})) { diemolcas(" invalid DIAG entry:$mcscf{refspace}\n");}}
               else { $mcscf{diag}='nohmat_iter';}
  print "\n";
 print "--------------SECTION MCSCF: general specifications -------------\n";
 $i=1;
 for ($i=1;$i<=$nsym;$i++) { ${$mcscf{internalspace}}[$i]= ${$mcscf{docc}}[$i]+${mcscf{cas}}[$i]+${$mcscf{ras}}[$i]+${$mcscf{aux}}[$i];}
 print "\n"; $zeroext=0;
 # run some checks 
  $error=0;
  for ( $i=1;$i<=$nsym;$i++) {
       if (${$mcscf{internalspace}}[$i]>${$general{mobasis}}[$i])
       { print "inconsistent orbital numbers in irreducible representation $i\n";
         $error++;}}
  if ($error) {diemolcas ("inconsistent orbital numbers in $error irreps\n");}

# some checks 
  for ($idrt=2; $idrt<=$mrci{ndrt}; $idrt++)
   { if (${$mcscf{electrons}}[$idrt][1] != ${$mrci{electrons}[1][1]} )
       { print ("Warning:  total number of electrons must not varies among DRTs\n");}
   }

 for ($idrt=1; $idrt<=$mcscf{ndrt}; $idrt++)
 {
 print "--------- configuration space specification  (DRT $idrt) --------\n";
 if ($mcscf{title}) {printf " %-25s %-80s\n", "Title:",  $mcscf{title};}
 if (${$mcscf{electrons}}[$idrt]) {printf " %-25s total: %4d  holes:%4d  auxel: %4d \n", "Electrons:", @{${$mcscf{electrons}}[$idrt]}[1..3];}
 if (${$mcscf{multiplicity}}[$idrt]) {printf " %-25s %4d \n", "Multiplicity:",  ${$mcscf{multiplicity}}[$idrt];}
 if (${$mcscf{symmetry}}[$idrt]) {printf " %-25s %4d \n", "Symmetry     ",  ${$mcscf{symmetry}}[$idrt];    }
 printf " %-25s %4d \n", "number of roots ",${$mcscf{nroot}}[$idrt];
 printf " %-25s ","  docc"; for ($i=1;$i<=$nsym;$i++) { printf "%4d", ${$mcscf{docc}}[$i];} print "\n";
 printf " %-25s ","  ras "; for ($i=1;$i<=$nsym;$i++) { printf "%4d", ${$mcscf{ras}}[$i];} print "\n";
 printf " %-25s ","  cas "; for ($i=1;$i<=$nsym;$i++) { printf "%4d", ${$mcscf{cas}}[$i];} print "\n";
 printf " %-25s ","  aux "; for ($i=1;$i<=$nsym;$i++) { printf "%4d", ${$mcscf{aux}}[$i];} print "\n";
 print "----------------------------------------------------\n";
 printf " %-25s ","internal "; for ($i=1;$i<=$nsym;$i++) { printf "%4d", ${$mcscf{internalspace}}[$i];} print "\n";
 print "----------------------------------------------------\n";
 }
 # make sure that there are at least two active orbitals
 $tot=0;
 for ($i=1;$i<=$nsym;$i++) {$tot=$tot+${$mcscf{internalspace}}[$i];}
 if ($tot < 2){ diemolcas("specify at least two active orbitals ");}

 if ($general{print}) {
   print "* title=$mcscf{title}\n";
   print "* nciiter=$mcscf{nciiter} nmicroiter=$mcscf{nmicroiter}  niter=$mcscf{niter} \n"; 
   print "* wnorm=$mcscf{wnorm}  knorm=$mcscf{knorm}  apxde=$mcscf{apxde} \n"; 
   print "* DOCC=",join(':',@{$mcscf{docc}}[1..$nsym]),"\n";
   print "* RAS =",join(':',@{$mcscf{ras}}[1..$nsym]),"\n";
   print "* CAS =",join(':',@{$mcscf{cas}}[1..$nsym]),"\n";
   print "* AUX =",join(':',@{$mcscf{aux}}[1..$nsym]),"\n";
   print "* DIAG= $mcscf{diag} \n";
   for ($i=1; $i<=$mcscf{ndrt} ; $i++)
   {  print "* DRT$i mult=${$mcscf{multiplicity}}[$i] electrons=",join(':',@{${$mcscf{electrons}}[$i]}[1..3])," symmetry=${$mcscf{symmetry}}[$i] nroot=${$mcscf{nroot}}[$i] \n"; }
 }



  return;
  }


 sub execute_mcscf
 {
# create all input files and run mcdrt.x/mcuft.x
 for ($idrt=1; $idrt<=$mcscf{ndrt}; $idrt++)
  { create_mcdrtin($idrt);
    callprog("mcdrt.x","<mcdrtin > mcdrtls ",0);
    analyse_mcdrtls($idrt);
    callprog("mcuft.x"," ",0);
    if ($mcscf{ndrt} > 1) {
    for $j ("mcdrtls","mcdrtfl","mcuftls","mcdftfl","mcoftfl","mcdrtin")
      { unless(rename("$j","$j.$idrt")) {die("failed renaming $j to $j.$idrt \n");}}}
  }
 &create_mcscfin(); 
 if ($general{runtest}) {stopit("testing input");}
 ln("-s","$ENV{Project}.SymInfo","molcas.SymInfo");
 if (! -l "ORDINT") { ln("-s","$ENV{Project}.OrdInt","ORDINT");}
 if (! -l "ONEINT" ) {ln("-s","$ENV{Project}.OneInt","ONEINT");}
 if (! -l "RUNFILE" ){ln("-s","$ENV{Project}.RunFile","RUNFILE");}
 if (! -l "ONEREL" ) {ln("-s","$ENV{Project}.OneRel","ONEREL");}

 if ( -e "$ENV{Project}.RasOrb" ) { print "using existing RAS orbitals \n";
                                    cp("$ENV{Project}.RasOrb","mocoef_lumorb");}
 elsif (-e "$ENV{Project}.ScfOrb" ){print "using existing SCF orbitals \n";
                                    cp("$ENV{Project}.ScfOrb","mocoef_lumorb");}
 elsif (-e "mocoef_lumorb" ){print "using existing Columbus orbitals mocoef_lumorb \n";}
 else {die("need starting orbitals from SCF or RASSCF \n");}
 callprog("mcscf.x"," ",0);
 # &analyse_mcscf();
  system("cat mcscfsm");
 for ($idrt=1; $idrt<=$mcscf{ndrt}; $idrt++)
  { for ($istate=1; $istate<=${$mcscf{nroot}}[$idrt]; $istate++) 
    { open MCPCIN, ">mcpcin";
      print MCPCIN "  $drt / \n  $state / \n ";
      print MCPCIN "  2 / \n  0.001 / \n  0 / \n  0 / \n";
      close MCPCIN ;
      callprog("mcpc.x","<mcpcin > mcpcls ",0);
      mv("mcpcls","mcpcls.drt$idrt.state$istate");
    }}

 $w=pwd();chop $w;
 print "====> full listing          on $w/mcscfls\n";
 print "====> short listing         on $w/mcscfsm\n";
 print "====> wavefunction analysis on $w/mcpcls.drt#.state#\n";
 return;
 }

#---------------------------
sub create_mcdrtin {
  local $mydrt; 
   ($mydrt)=@_;
  if ($mrci{noauto}) {return;}
  open FIN,">mcdrtin";
    print FIN " ${$mcscf{multiplicity}}[$mydrt] /input the spin multiplicity \n";
    print FIN " ${$mcscf{electrons}}[$mydrt][1] /input the total number of electrons \n";
    print FIN " $nsym  /input the number of irreps (1-8) \n";
    print FIN " Y /enter symmetry labels: \n";
    for ($i=1;$i<=$nsym;$i++) {print FIN ${$general{irreplabels}}[$i]," \n";}
    print FIN " ${$mcscf{symmetry}}[$mydrt] /input the molecular spatial symmetry (irrep 1:nsym) \n";


## compute docc vector
     @corevec=();
     for ($i=1;$i<=$nsym;$i++) {for ($j=1; $j<=${$mcscf{docc}}[$i];$j++) { push @corevec,$i,$j;}}
      if ($#corevec > 0 ) { printorbvec(@corevec);}
      print FIN "  /list of doubly occupied orbitals \n";
## compute internal vector REFRAS:REFCAS:REFAUX
     @corevec=();
     @fciorb=();
     for ($i=1;$i<=$nsym;$i++)
       {for ($j=${$mcscf{docc}}[$i]+1; $j<=${$mcscf{docc}}[$i]+${$mcscf{ras}}[$i];$j++) { push @corevec,$i,$j; push @fciorb,$i,$j,40;}}
     for ($i=1;$i<=$nsym;$i++)
       {for ($j=${$mcscf{docc}}[$i]+${$mcscf{ras}}[$i]+1; $j<=${$mcscf{docc}}[$i]+${$mcscf{ras}}[$i]+${$mcscf{cas}}[$i];$j++) { push @corevec,$i,$j; push @fciorb,$i,$j,400;}}
     for ($i=1;$i<=$nsym;$i++)
       {for ($j=${$mcscf{docc}}[$i]+${$mcscf{ras}}[$i]+${$mcscf{cas}}[$i]+1; $j<=${$mcscf{docc}}[$i]+${$mcscf{ras}}[$i]+${$mcscf{cas}}[$i]+${$mcscf{aux}}[$i];$j++) { push @corevec,$i,$j;push @fciorb,$i,$j,4000;}}
      if ($#corevec > 0 ) { printorbvec(@corevec); }
      print FIN "  / active orbitals \n";
##  need splitting of lines in all print statements!
     if ($mydrt == 1 ) { $mcscf{fciorb}=\@fciorb;} 

     $docc=0; $aux=0; $cas=0; $ras=0;
     for ($i=1;$i<=$nsym;$i++) {$docc+=${$mcscf{docc}}[$i]; $aux+=${$mcscf{aux}}[$i]; $cas+=${$mcscf{cas}}[$i]; $ras+=${$mcscf{ras}}[$i];}

##  compute reference occupation vector occminr, occmaxr
    $nact=${$mcscf{electrons}}[$mydrt][1]-$docc-$docc;
     my @fciorb=();
     @occminr=();
     for ($i=1;$i<$ras;$i++) {push @occminr, 0; }
     for ($i=1;$i<=$cas;$i++) {push @occminr, $refras+$refras-${$mcscf{electrons}}[$mydrt][2]; }
     for ($i=1;$i<=$aux+1;$i++) {push @occminr, $nact-${$mcscf{electrons}}[$mydrt][3]; }
     printorbvec(@occminr);
    #print FIN " / occmin\n";
     @occmaxr=();
     for ($i=1;$i<$ras;$i++) {push @occmaxr, $ras+$ras;}
     for ($i=1;$i<=$cas;$i++) {push @occmaxr, $nact;}
     for ($i=1;$i<=$aux+1;$i++) {push @occmaxr, $nact;}
     printorbvec(@occmaxr);
    #print FIN " / occmax\n";
     print FIN "  / bmin \n";
     print FIN "  / bmax \n";
     @stepr=();
     for ($i=1;$i<=$ras+$cas+$aux;$i++) {push @stepr, 1111;}
     printorbvec(@stepr);
    #print FIN " /step masks \n";
     print FIN "0 / input the number of vertices to be deleted  \n";
     print FIN "n / arcs to be manually removed   \n";
     print FIN "  / drt levels to be printed      \n";
     print FIN "y / keep all of these walks       \n";
     print FIN "/\n";
     print FIN "0/ input walk number (0 to end) \n"; 
     print FIN "mcdrt_title\n";
     print FIN "\n";
     print FIN "y/ write the drt file \n";
     print FIN "y/ include step vectors \n";
     close FIN;
  return;
 }


  sub analyse_mcdrtls
  {local $mydrt;
    ($mydrt) = @_;
   my ($info,@info);
   my @multlabel=(" ","singlet","doublet","triplet","quartet","quintet","sextet","heptet","octet","nonet");
   open MCDRTFL,"<mcdrtfl";
   while (<MCDRTFL>) { if (/info/) { $info=<MCDRTFL>; last;}}
  close MCDRTFL;
  $info=~s/^ *//; chop $info; @info=split(/\s+/,$info);
  print "----------- MCSCF wavefunction specification (DRT # $mydrt )------------\n";
  printf " #ndocc =    %3d  #nact = %3d  symmetry=%4s  multiplicity = %8s \n", 
              @info[0,1], ${$general{irreplabels}}[$info[4]], $multlabel[$info[5]];
  printf " %-25s  %3d %-25s %3d \n", "#electrons",$info[6],"#active electrons",$info[7];
  printf " %-20s  %8d %-20s %8d \n", "#walks",$info[8],"#CSFs",$info[9];
  return;
 } 

 sub analyse_mcscf
  {
     local($skip,$x,@x,$etot,$edrop,$wnorm,$knorm,$nconv);
      open(MCSCFSM,"mcscfsm");
      $skip=0; $/="\n";
      while (<MCSCFSM>)
      { if (/final mcscf/)
          { $_ = <MCSCFSM> ; chop ;
            $nconv = /not conv/;
            s/[a-z=\*]/ /g;
            @x = split(/\s+/,$_);
            $etot = $x[2]; $edrop=$x[3]; $wnorm=$x[4];
            $knorm= $x[5]; }
        if (/Individual/i) { $skip=1;}
        if (/DRT #/i) {$x[$skip]=$_ ; $skip=$skip+1; }
      }
      if ($skip > 0 )
           { printf " Individual total energies for all states: \n";
             $skip--; printf " @x[1..$skip] "; }
       else
           { printf
  "%-30s%15.8f\n%-30s%15.8f\n%-30s%15.8f\n%-30s%15.8f\n",
                    "Total Energy:", $etot ,
                    "knorm:", $knorm,
                    "wnorm: ",$wnorm,
                    "last energy change:", $edrop  ; }
  return;
  }
    
sub create_cidrtin_step {
  if ($mrci{noauto}) {return;}
  open FIN,">cidrtin.cigrd";
    print FIN " N / no spin orbit CI\n";
    print FIN " ${$mrci{multiplicity}}[1] /input the spin multiplicity \n";
    print FIN " ${$mrci{electrons}}[1][1] /input the total number of electrons \n";
    print FIN " $nsym  /input the number of irreps (1-8) \n";
    print FIN " Y /enter symmetry labels: \n";
    for ($i=1;$i<=$nsym;$i++) {print FIN ${$general{irreplabels}}[$i]," \n";}
    for ($i=1;$i<=$nsym;$i++) {print FIN ${$general{aobasis}}[$i],"  ";} print FIN " / number of basis functions \n";
    print FIN " ${$mrci{symmetry}}[1] /input the molecular spatial symmetry (irrep 1:nsym) \n";
    print FIN "  /list of frozen core orbitals \n";
    print FIN "  /list of frozen virtual orbitals \n";



## compute internal vector FROZEN:REFDOCC:REFRAS:REFCAS:REFAUX
     @corevec=();
     for ($i=1;$i<=$nsym;$i++)
       {for ($j=1; $j<=${$mrci{frozencore}}[$i];$j++) { push @corevec,$i,$j;}}
     for ($i=1;$i<=$nsym;$i++)
       {for ($j=${$mrci{frozencore}}[$i]+1; $j<=${$mrci{frozencore}}[$i]+${$mrci{refdocc}}[$i];$j++) { push @corevec,$i,$j;}}
     for ($i=1;$i<=$nsym;$i++)
       {for ($j=${$mrci{frozencore}}[$i]+${$mrci{refdocc}}[$i]+1; $j<=${$mrci{frozencore}}[$i]+${$mrci{refdocc}}[$i]+${$mrci{refras}}[$i];$j++) { push @corevec,$i,$j;}}
     for ($i=1;$i<=$nsym;$i++)
       {for ($j=${$mrci{frozencore}}[$i]+${$mrci{refdocc}}[$i]+${$mrci{refras}}[$i]+1;
          $j<=${$mrci{frozencore}}[$i]+${$mrci{refdocc}}[$i]+${$mrci{refras}}[$i]+${$mrci{refcas}}[$i];$j++) { push @corevec,$i,$j;}}
     for ($i=1;$i<=$nsym;$i++)
       {for ($j=${$mrci{frozencore}}[$i]+${$mrci{refdocc}}[$i]+${$mrci{refras}}[$i]+${$mrci{refcas}}[$i]+1;
          $j<=${$mrci{frozencore}}[$i]+${$mrci{refdocc}}[$i]+${$mrci{refras}}[$i]+${$mrci{refcas}}[$i]+${$mrci{refaux}}[$i];$j++) { push @corevec,$i,$j;}}
      if ($#corevec > 0 ) { printorbvec(@corevec); }
      print FIN "  /internal orbitals \n";

##  need splitting of lines in all print statements!

     $refdocc=0;
     $refaux=0;
     $refcas=0;
     $refras=0;
     $nfrozen=0;
     for ($i=1;$i<=$nsym;$i++) {$refdocc+=${$mrci{refdocc}}[$i];
                                $refaux+=${$mrci{refaux}}[$i];
                                $refcas+=${$mrci{refcas}}[$i];
                                $nfrozen+=${$mrci{frozencore}}[$i];
                                $refras+=${$mrci{refras}}[$i];}
     print FIN  $refdocc+$nfrozen,"  /input the number of ref-csf doubly-occupied orbitals\n";

##  compute reference occupation vector occminr, occmaxr
    $nactref=${$mrci{electrons}}[1][1]-$refdocc-$refdocc - $nfrozen-$nfrozen;
     @occminr=();
     for ($i=1;$i<$refras;$i++) {push @occminr, 0;}
     for ($i=1;$i<=$refcas;$i++) {push @occminr, $refras+$refras-${$mrci{electrons}}[1][2];}
     for ($i=1;$i<=$refaux+1;$i++) {push @occminr, $nactref-${$mrci{electrons}}[1][3];}
     printorbvec(@occminr);
     @occmaxr=();
     for ($i=1;$i<$refras;$i++) {push @occmaxr, $refras+$refras;}
     for ($i=1;$i<=$refcas;$i++) {push @occmaxr, $nactref;}
     for ($i=1;$i<=$refaux+1;$i++) {push @occmaxr, $nactref;}
     printorbvec(@occmaxr);
     print FIN "  / bminr \n";
     print FIN "  / bmaxr \n";
     @stepr=();
     for ($i=1;$i<=$refras+$refcas+$refaux;$i++) {push @stepr, 1111;}
     printorbvec(@stepr);
     print FIN "  $mrci{exlevel} /input the maximum excitation level from the reference csfs \n";
     print FIN "  / default occmin \n";
     print FIN "  / default occmax \n";
     print FIN "  / default bmin   \n";
     print FIN "  / default bmax   \n";
     @stepr=();
     for ($i=1;$i<=$nfrozen;$i++) {push @stepr, 1000;}
     for ($i=1;$i<=$refcas+$refras+$refcas+$refaux;$i++) {push @stepr, 1111;}
     printorbvec(@stepr);
     print FIN "  / default step   \n";
     print FIN " -1 / manual arc removing step \n";
     print FIN " $mrci{genspace} /impose generalized interacting space restrictions? \n";
     print FIN " -1 / manual arc removing step \n";
     print FIN "  / default printing drt \n";
     print FIN " ${$mrci{symmetry}}[1]  / default reference symmetry\n";
     print FIN " N /keep all of the z-walks as references? \n";
     print FIN " Y /generate walks while applying reference drt restrictions? \n";
     print FIN " N /impose additional orbital-group occupation restrictions? \n";
     print FIN " N /apply primary reference occupation restrictions? \n";
     print FIN " N /manually select individual walks? \n";
     @weight=();
     for ($i=1;$i<=$nfrozen+$refdocc+$refras+$refcas+$refaux;$i++) {push @weight, 4;}
     $weight[0]=-1;
     printorbvec(@weight);
     print FIN "  / defaults \n";
     print FIN " 0 /input reference walk number (0 to end) \n";
     @weight=();
     for ($i=1;$i<=$nfrozen+$refdocc;$i++) {push @weight, 2;}
     for ($i=1;$i<=$refras+$refcas+$refaux;$i++) {push @weight, 0;}
     printorbvec(@weight);
     print FIN " /  \n";
     print FIN " N /this is an obsolete prompt. \n";
     print FIN " 0 /input mrsdci walk number (0 to end) \n";
     print FIN " $mrci{title} \n";
     print FIN "cidrtfl\n";
     print FIN " Y /write the drt file?\n";
     close FIN;
   # system("cat cidrtin");
  return;}

   sub execute_grad
   {
     if ($mrci{nfrozen} > 0) { mv("cidrtfl","cidrtfl.ci");
                               print "creating step vector based frozen core cidrtin input ... \n"; 
                               &create_cidrtin_step();
                               callprog("cidrt.x","<cidrtin.cigrd > cidrtls.cigrd",0);
                               open TRAN,">tranin";
                               print TRAN " &input\n SEWARD=1\n LUMORB=1\n THRESH= 1.d-10 \n &end\n";
                               close TRAN;
                               callprog("tran.x"," ",0);
                               create_cigrdin(1);  }
     else { create_cigrdin(0);}
     ln("-s","cid1fl.drt1.state1","cid1fl");
     ln("-s","cid2fl.drt1.state1","cid2fl");
     callprog("cigrd.x"," ",1);
     unless (rename("effd1fl","modens")) {die ("rename error $!\n");}
     unless (rename("effd2fl","modens2")){ die ("rename error $!\n");}
     unlink("cid1fl","cid2fl");
###
     unlink("cid1fl.drt1.state1","cid2fl.drt1.state1","moints","civfl");
     unlink("$ENV{Project}.OrdInt","mchess");
###

     open TRAN,">tranin";print TRAN " \&input \n seward=1 \n lumorb=1 \n denopt=1\n \&end\n"; close TRAN;
## debug
    $savem=$general{coremem};
#    $general{coremem}=30;
     callprog("tran.x"," ",0);
#    $general{coremem}=$savem;
###
     unlink("modens","modens2");
###
     open MCINPUT,">calaska.input";
     print MCINPUT "\n &ALASKA &END\n";
     print MCINPUT "VERBOSE\nSHOW GRADIENT CONTRIBUTIONS\nEnd of Input\n";
     close MCINPUT; 
     cp("moocef_lumorb","INPORB");
     system("molcas calaska.input ");
     print " cartesian gradient by alaska \n";
     system("cat cartgrd");
###
     unlink("GTOC","GAMMA");
###
     return;
                                    
   }

     sub changekeyword {
       local (@tmpv,$file_old,$file_new,$keyword,$value,$found);
       ($file_old,$file_new,$keyword,$value)=@_;
#
       $/="\n";
       open OLD, "<$file_old";
       while(<OLD>) {if (! /^\s+$/ ) {push @tmpv,$_;}
                     if ( /[&\$]end\s+$/ ) { last;}}
       @tmpvv=();
       while (<OLD>) { push @tmpvv,$_;}
       close OLD;
#      print "changekyword old file $file_old \n";
#      system ("cat $file_old");

#      print "in tmpv=",join(':',@tmpv),"NNNN\n";
#
       open NEW, ">$file_new";
       $found=0;
       while ( $#tmpv > 0 )
        { $_ = shift @tmpv;
         if (/$keyword/i) { print NEW "  $keyword=$value\n";$found=1; }
#                           print "Substituting $keyword=$value\n"; }
         else         { print NEW $_;}
        }
#      if (! $found == 1){ die "keyword $keyword not found in file: $file_old\n";}
       if (! $found == 1) { # print "Adding $keyword=$value\n";
                            print NEW " $keyword=$value\n"; }
       print NEW shift @tmpv;
       print NEW @tmpvv;
       close NEW;
#      print "changekyword new file $file_new \n";
#      system ("cat $file_new");
       return 0;
      }

      sub create_cigrdin
      { local $assume_fc; ($assume_fc)=@_;
    open CIGRDIN, ">cigrdin";
    print CIGRDIN " \&input \n nmiter= 100, print=0, fresdd=1, \n";
    print CIGRDIN " fresaa=1, fresvv=1, \n";
    if ($mcscf{diag} =~ /hmat_full/ || $mcscf{diag} =~ /^hmat_iter/ ) 
        { print CIGRDIN " mdir=0, \n";} # if flag 11 is set in mcscfin 
       else {print CIGRDIN " mdir=1 \n";} 
    
    print CIGRDIN " cdir=0, \n"; #
#   print CIGRDIN " cdir=1, \n"; # if the mcscf calc. was run with linear
#                               # convergence only.
    print CIGRDIN " rtol=1e-6, dtol=1e-6,\n";
    print CIGRDIN " wndtol=1e-7,wnatol=1e-7,wnvtol=1e-7 \n";
    print CIGRDIN " assume_fc=$assume_fc \&end\n";
    close CIGRDIN;
    return;}

#  callprog ("ciudg.x","<cidrtin > cidrtls",  1 || o) 
      sub callprog
     { local ($name,$pargs,$par); ($name,$pargs,$par)=@_;
       my ($trailer,$commandstring);
         if (grep(/>/,$pargs) ) 
           { $trailer=" $pargs 2>> runcerr  ";} 
         else   { $trailer=" $pargs >> runcerr 2>&1 "; }
       if ($par)
       { if ($name=~/cigrd/) { $commandstring=$mpistartup; $commandstring=~s/_NPROC_/2/;
                               my $cigrdmem; $cigrdmem=  $general{coremem}*$general{ncpu}/2 ;
                               $commandstring="$commandstring $colpath/p$name -m " . $cigrdmem . "  $trailer ";} 
                   else      { $commandstring=$mpistartup; $commandstring=~s/_NPROC_/$general{ncpu}/; 
                               $commandstring="$commandstring $colpath/p$name -m $general{coremem}  $trailer ";} 
         if ($general{print}>0) { print " executing  $commandstring \n"; }
         system("set >> runcerr 2>&1; ps axl >> runcerr 2>&1; ipcs >> runcerr 2>&1 ;  $commandstring") ;
       }
       else  
       { $commandstring="$colpath/$name -m $general{coremem} $trailer"; 
         if ($general{print}>0) { print " executing  $commandstring \n"; }
         system(" $commandstring" ) ; }
      checkterm($name);
      return;
     }
