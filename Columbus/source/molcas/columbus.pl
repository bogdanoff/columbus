#!/usr/bin/perl
#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/


 use Shell qw(pwd);

sub cleanuplinks
  { my @files;
   open CURRDIR,"./";
   @files=<CURRDIR>;
   foreach ( @files ) { if ( -l $i ) { print "removing link $_\n"; unlink ($_);}}
  return;
 }   

sub getmolcasinfo
{ 
# get information from  molcasinfo_col file
# ***EXTRACTED INFO FROM MOLCAS RUNFILE*** 
# Nuclear Repulsion Energy=        5.98892689
# NSYM =                      4
# AO BASIS FUNCTIONS =       23    11    17     7
# FROZEN CORE =               0     0     0     0
# FROZEN VIRTUAL =            0     0     0     0
# MO ORBITALS   =            23    11    17     7
# IRREP LABELS =          a1     b1     b2     a2 


  system("$colpath/getmolcasinfo.x > molcasinfo_col;");
 open FIN,"<molcasinfo_col";
  $_=<FIN>; chop;
  $_=<FIN>; chop;
  $_=<FIN>; chop; s/[A-Za-z=]//g; s/^ *//g;
  $nsym=$_; 
  $_=<FIN>; chop; s/[A-Za-z=]//g; s/^ *//g;
  @aobasis=split(/\s+/);
  $_=<FIN>; chop; s/[A-Za-z=]//g; s/^ *//g;
  @frozencore=split(/\s+/);
  $_=<FIN>; chop; s/[A-Za-z=]//g; s/^ *//g;
  @frozenvirt=split(/\s+/);
  $_=<FIN>; chop; s/[A-Za-z=]//g; s/^ *//g;
  @mobasis=split(/\s+/);
  $_=<FIN>; s/^.*=//; s/^ *//g;
  @irreplabels=split(/\s+/);
  unshift @aobasis, 0;
  unshift @frozencore, 0;
  unshift @frozenvirt, 0;
  unshift @mobasis, 0;
  unshift @irreplabels, " ";
 close FIN;
return;
}

sub setdefaults{
 
  $coremem=100;
  $nvcimx =10;
  $nvcimn =1;
  $rtol=0.001;
  $genspace="n";
  return;
}

sub analyse_cidrtls
{
  open CIDRTLS,"<cidrtls";
  while (<CIDRTLS>)
   { if (/^ total csf counts:/) {last;}
     if (/ reference csfs selected from/) {s/^ *//; ($nref,$x)=split(/\s+/);}}

  $z=<CIDRTLS>; $y=<CIDRTLS>; $x=<CIDRTLS>; $w=<CIDRTLS>;
  chop $z,$y,$x,$w;
  $z=~s/^.*://;
  $y=~s/^.*://;
  $w=~s/^.*://;
  $x=~s/^.*://;

  print "----------------- cidrtls ---------------------------\n";
  printf "configuration space:    %10d z CSFs \n",$z;
  printf "                        %10d y CSFs \n",$y;
  printf "                        %10d x CSFs \n",$x;
  printf "                        %10d w CSFs \n",$w;
  printf " total number of CSFs:  %10d  ( %6d reference configurations ) \n",$z+$y+$x+$w,$nref;
  close CIDRTLS;
  print "-----------------------------------------------------\n";
  return;
}

sub analyse_cidrtmsls
{
  open CIDRTLS,"<cidrtmsls";
   $idrt=0;
   $iidrt=0;
  while (<CIDRTLS>)
   { if (/ reference csfs selected from/) {s/^ *//; $iidrt++; 
                                         ($nref[$iidrt],$x)=split(/\s+/);}
     if (/^ total csf counts:/) { 
                                  $idrt++; $z=<CIDRTLS>; $y=<CIDRTLS>; $x=<CIDRTLS>; $w=<CIDRTLS>;
                                  chop $z,$y,$x,$w; $z=~s/^.*://;$y=~s/^.*://; $w=~s/^.*://; $x=~s/^.*://; 

# $savestr[$idrt]= sprintf("----------------- cidrtmsls  DRT %4d ----------------\n",$idrt);
  $savestr[$idrt]=  sprintf "configuration space:    %10d z CSFs \n",$z;   
  $savestr[$idrt].=sprintf "                        %10d y CSFs \n",$y ;  
  $savestr[$idrt].=sprintf "                        %10d x CSFs \n",$x ; 
  $savestr[$idrt].=sprintf "                        %10d w CSFs \n",$w ;
  $savestr[$idrt].= sprintf " total number of CSFs:  %10d  ",$z+$y+$x+$w;
 }}
  close CIDRTLS;

  for ($ldrt=1; $ldrt<=$ndrt; $ldrt++)
  { printf("----------------- cidrtmsls  DRT %4d ----------------\n",$ldrt);
    if ($genspace eq "n") { print $savestr[$idrt-($ndrt-$ldrt)*2];}
         else             { print $savestr[$idrt-($ndrt-$ldrt)];} 
   printf " ( %6d ref CSFs)\n-----------------------------------------------------\n", $nref[$ldrt];
   }
  return;
}





#---------------------------

sub stopit{
$option=shift @_;
 if ($option eq "ok") { system("echo 0 > returncode");}
 else
    { system("echo 95 > returncode");
      die( "$option\n"); }
 return;
}

sub checkterm{
 $option=shift @_;
 open BUMMER,"<bummer";
  $_=<BUMMER>;
 close BUMMER;
 if (!/normally terminated/) { system("echo 93 > returncode"); die ("$option failed");}
 return;
}

sub diemolcas{
 $option=shift @_;
 system("echo 92 > returncode");
 die ("***** ERROR $option ***** \n");
 return;
}

#---------------------------

sub analyse_input{
open FIN,"<$input";

@ALL_INPUT=<FIN>;
 chomp @ALL_INPUT; 
close FIN;

while ($#ALL_INPUT) { $_=shift @ALL_INPUT; if (/\&COLUMBUS/) {last;}}

if ($#ALL_INPUT<0) {stopit("no COLUMBUS entry found in $input\n");} 
if ( grep(/MULTIDRT/, @ALL_INPUT)) { $multidrt=1;}
@tmp=grep(/END_DRT/, @ALL_INPUT) ; $ndrt=$#tmp+1;
if ($multidrt) { print "MULTI-DRT input: $ndrt DRTs found\n";}

# extract all non-drt specific keywords an values from input file
 shift @ALL_INPUT; 
 while ($#ALL_INPUT > -1)
  { $_ = shift @ALL_INPUT; 
    if (/^Title/i) { $title=shift @ALL_INPUT;  next;}
    if (/^EXLVL/i) {$exlevel=shift @ALL_INPUT; next;}
    if (/^SUBSPACEDIM/i) {$inp=shift @ALL_INPUT; $inp=~s/^ *//; ($nvcimx,$nvcimn)=split(/\s+/,$inp); if (! $nvcimn) {$nvcimn=1;} next;}
    if (/^ITERATIONS/i) {$inp=shift @ALL_INPUT;  $inp=~s/^ *//; $nciiter=$inp; next;}
    if (/^ACCURACY/i)  {$rtol=shift @ALL_INPUT;  next;}
    if (/^REFSPACE/i)  {$refspace=shift @ALL_INPUT;  next;}
    if (/^MRAQCC/i)    {$mraqcc=1;next;}
    if (/^MRACPF/i)    {$mracpf=1;next;}
    if (/^MRAQCC-V/i)    {$mraqccv=1;next;}
    if (/^MRCISD/i)    {$mrcisd=1;next;}
     if (/^Multiplicity/i) {$multiplicity=shift @ALL_INPUT;next;}
    if (/^TEST/i)  {$runtest=1;}
    if (/^MEMORY/i)  {$_=shift @ALL_INPUT; if (/MB/i) {$fac=1;}
                                           else  {diemolcas("invalid MEMORY unit (MB)");}
                      s/\D//g; $coremem=$fac*$_;}
     if (/^REFDOCC/i) {$refdocc=shift @ALL_INPUT; next;}
     if (/^REFRAS/i) {$refras=shift @ALL_INPUT;next;}
     if (/^REFCAS/i) {$refcas=shift @ALL_INPUT; next;}
     if (/^REFAUX/i) {$refaux=shift @ALL_INPUT; next;}
     if (/^GENSPACE/i) {$genspace="y";next;}
     if (/^NOAUTO/i) {$noauto=1;next;}
     if (/^PARALLEL/i) {$parallel=1;next;}
    if (/^End of Input/i) {$endofinput=1;last;}
  }
 if (! $endofinput) {stopit("input file $input invalid (footing) $_\n");}
 close FIN;

# check for valid input

  if ($exlevel < 0 || $exlevel > 2 ) { diemolcas ("invalid EXLVL entry: $exlevel\n");}
  if ($nvcimx <= 0 ) { diemolcas ("invalid (first) entry for SUBSPACEDIM: $nvcimx \ne");}
  if ($nvcimn <= 0  || $nvcimn >= $nvcimx) { diemolcas ("invalid entry for SUBSPACEDIM: $nvcimx \n");}
  if ($nciiter <=0 || $nbkiter < 0 ) { diemolcas("invalid entries for ITERATION: $nciiter,$nbkiter\n");}
  if ($rtol > 1 || $rtol < 0 ) { diemolcas ("invalid or unreasonable entries for ACCURACY: $rtol \n");}
  if ( ($mraqcc+$mracpf+$mraqccv+$mrcisd) != 1 ) { diemolcas ("only one keyword out of MRCISD, MRACPF, MRQCC, MRAQCC-V must appear\n");}
  if (! $colpath ) { diemolcas ("COLPATH keyword missing\n");}
# if ( ! -f "$colpath/molcas.x" ) { diemolcas ("invalid COLPATH: $colpath/molcas.x missing\n");} 
  if ( ! -f "$colpath/ciudg.x" ) { diemolcas ("invalid COLPATH: $colpath/ciudg.x missing\n"); }
  if ( ! -f "$colpath/cidrt.x" ) { diemolcas ("invalid COLPATH: $colpath/cidrt.x missing\n"); }
  if ( ! -f "$colpath/cidrtms.x" ) { diemolcas ("invalid COLPATH: $colpath/cidrtms.x missing\n");} 
  if ( ! -f "$colpath/pciudg.x" ) { diemolcas ("invalid COLPATH: $colpath/pciudg.x missing\n"); }
  if ( $mrcisd ) { $nbkiter=1;} else {$nbkiter=0;}

  if ($refspace) { if (! grep (/none|full|refspace/i,$refspace)) { diemolcas(" invalid REFSPACE entry:$refspace\n");}}
               else { $refspace='FULL';}
 

# get information from  motrainfo file
  getmolcasinfo();
# print basis info

 print "--------------- Basis set information from RUNFILE -------------\n";
 printf "%-10s"," irred. repr. "; for ($i=1;$i<=$nsym;$i++) { printf "%4s", $irreplabels[$i];} print "\n";
 printf "%-10s"," AO basis     "; for ($i=1;$i<=$nsym;$i++) { printf "%4d", $aobasis[$i];} print "\n";
 printf "%-10s"," frozen core  "; for ($i=1;$i<=$nsym;$i++) { printf "%4d", $frozencore[$i];} print "\n";
 printf "%-10s"," deleted virt."; for ($i=1;$i<=$nsym;$i++) { printf "%4d", $frozenvirt[$i];} print "\n";
 printf "%-10s"," MO basis     "; for ($i=1;$i<=$nsym;$i++) { printf "%4d", $mobasis[$i];} print "\n";


  $refdocc=~s/^ *//;
  @inp=split(/\s+/,$refdocc);
  for ($i=1;$i<=$nsym;$i++) { $refdocc[$i]=$inp[$i-1];}

  $refras =~s/^ *//;
  @inp=split(/\s+/,$refras);
  for ($i=1;$i<=$nsym;$i++) { $refras[$i]=$inp[$i-1];}

  $refcas =~s/^ *//;
  @inp=split(/\s+/,$refcas);
  for ($i=1;$i<=$nsym;$i++) { $refcas[$i]=$inp[$i-1];}

  $refaux =~s/^ *//;
  @inp=split(/\s+/,$refaux);
  for ($i=1;$i<=$nsym;$i++) { $refaux[$i]=$inp[$i-1];}
  print "\n"; 
 printf "%-10s"," internal MOs "; for ($i=1;$i<=$nsym;$i++) { printf "%4d", $refdocc[$i]+$refras[$i]+$refaux[$i];;} print "\n";
 $zeroext=0;
 printf "%-10s"," external MOs "; for ($i=1;$i<=$nsym;$i++) { printf "%4d", $mobasis[$i]-$refdocc[$i]-$refras[$i]-$refaux[$i];
                                                             if ($mobasis[$i]-$refdocc[$i]-$refras[$i]-$refaux[$i] ==0 ) {$zeroext++;}} print "\n";
 print "----------------------------------------------------------------\n";

 if ($zeroext) {diemolcas("Due to a bug computations with zero external MOs are temporarily disabled!\n");}

 # run some checks 
  $error=0;
  for ( $i=1;$i<=$nsym;$i++) {
       $internalspace[$i] = $refdocc[$i]+$refras[$i]+$refcs[$i]+$refaux[$i];
       if ($internalspace[$i]>$mobasis[$i])
       { print "inconsistent orbital numbers in irreducible representation $i\n";
         $error++;}}
  if ($error) {diemolcas ("inconsistent orbital numbers in $error irreps\n");}



  if ($ndrt )
 {
# extract all drt-specific keywords 
 open FIN,"<$input";
 while(<FIN>) { if (/\&COLUMBUS/) { $foundcolumbus=1;last;}}
 for ($idrt=1; $idrt<=$ndrt; $idrt++)
 { print "extracting drt information for drt $idrt\n";
   while (<FIN>) {if (grep(/DRT$idrt/,$_)) {last;}}
   $checkelectrons=0;
   while (<FIN>)
    {if (/^Electrons/i) {$electrons=<FIN>;chop $electrons; $checkelectrons=1;next;}   # total, holes RAS, elect AUX
     if (/^Symmetry/i) {$symmetry[$idrt]=<FIN>;chop $symmetry[$idrt]; next;}
     if (/^NROOT/i) {$nroot[$idrt]=<FIN>; chop $nroot[$idrt]; next;}
     if (/^END_DRT/i) {$endofinput=1;last;}
    } 
  if (! $checkelectrons) {diemolcas("must specify number of electrons");}
  $electrons=~s/^ *//;
  @inp=split(/\s+/,$electrons);
  for ($i=1;$i<=3;$i++) { $electrons[$idrt][$i]=$inp[$i-1];}

# some checks 

  for ($idrt=2; $idrt<=$ndrt; $idrt++) 
   { if ($electrons[$idrt][1] != $electrons[1][1] ) 
       { diemolcas("MULTIDRT: total number of electrons must not vary among the DRTs\n");}
   }
   for ($idrt=1; $idrt<=$ndrt; $idrt++)
   { if ($nroot[$idrt] > $nvcimx +1 ) { print "INFO: max. SUBSPACEDIMENSION too low for desired number of roots,\n";
                                        printf "INFO: resetting maximum subspace dimension to %4d \n",$nroot[$idrt]+4;
                                        $nvcimx=$nroot[$idrt]+4;}
     if ($nroot[$idrt] > $nvcimn ) { print "INFO: min. SUBSPACEDIMENSION too low for desired number of roots \n";
                                     printf "INFO: resetting minimum subspace dimension to %4d \n",$nroot[$idrt] ;
                                     $nvcimn=$nroot[$idrt];}
     if ($symmetry[$idrt] <=0 || $symmetry[$idrt] > $nsym) {diemolcas("invalid SYMMETRY: valid range 1 .. $nsym \n");}

   }
   


 print "--------- configuration space specification  (DRT $idrt) --------\n";
 if ($title) {printf " %-25s %-80s\n", "Title:",  $title;}
 if ($electrons) {printf " %-25s total: %4d  holes:%4d  auxel: %4d \n", "Electrons:", $electrons[$idrt][1],
     $electrons[$idrt][2],$electrons[$idrt][3];}
 if ($multiplicity) {printf " %-25s %4d \n", "Multiplicity:",  $multiplicity;}
 if ($symmetry[$idrt]) {printf " %-25s %4d \n", "Symmetry     ",  $symmetry[$idrt];    }
 if ($exlevel) {printf " %-25s %4d \n", "Excitation level ",$exlevel; }
 printf " %-25s %4d \n", "number of roots ",$nroot[$idrt];
 printf " %-25s "," reference docc"; for ($i=1;$i<=$nsym;$i++) { printf "%4d", $refdocc[$i];} print "\n";
 printf " %-25s "," reference ras"; for ($i=1;$i<=$nsym;$i++) { printf "%4d", $refras[$i];} print "\n";
 printf " %-25s "," reference cas"; for ($i=1;$i<=$nsym;$i++) { printf "%4d", $refcas[$i];} print "\n";
 printf " %-25s "," reference aux"; for ($i=1;$i<=$nsym;$i++) { printf "%4d", $refaux[$i];} print "\n";
 print "----------------------------------------------------\n";

 # run some checks

  }

  close FIN;

 print "------------------ runtime options ------------------\n";
 printf "%-15s  %8.3f MB \n ", "columbus main memory ", $coremem*8/(1024*1024);
 if ($runtest) { print " +++++++ TESTING INPUT ++++++++++++++++++++\n";}
 print "----------------------------------------------------\n";

  $error=0;
  for ( $i=1;$i<=$nsym;$i++) 
  {for ($idrt=2;$idrt<=$ndrt;$idrt++)
  { if ($internalspace[$idrt][$i] != $internalspace[$idrt][1] ) { $error++;}
  }}
  if ($error) {stopit('varying internal orbital spaces among the different DRTs')}

  }  # $ndrt
 else
 { open FIN,"<$input";
 while(<FIN>) { if (/\&COLUMBUS/) { $foundcolumbus=1;last;}}
    $checkelectrons=0;
   while (<FIN>)
    {if (/^Electrons/i) {$electrons=<FIN>;chop $electrons;$checkelectrons=1; next;}   # total, holes RAS, elect AUX
     if (/^Symmetry/i) {$symmetry=<FIN>;chop $symmetry; next;}
     if (/^NROOT/i) {$nroot=<FIN>; chop $nroot; next;}
     if (/^END of input/i) {$endofinput=1;last;}
    }

  if (! $checkelectrons) {diemolcas("must specify number of electrons");}
  $electrons=~s/^ *//;
  @inp=split(/\s+/,$electrons);
  for ($i=1;$i<=3;$i++) { $electrons[$i]=$inp[$i-1];}

   { if ($nroot > $nvcimx +1 ) { print "INFO: max. SUBSPACEDIMENSION too low for desired number of roots,\n";
                                        printf "INFO: resetting maximum subspace dimension to %4d \n",$nroot+4;
                                        $nvcimx=$nroot+4;}
     if ($nroot > $nvcimn ) { print "INFO: min. SUBSPACEDIMENSION too low for desired number of roots \n";
                                     printf "INFO: resetting minimum subspace dimension to %4d \n",$nroot ;
                                     $nvcimn=$nroot;}
     if ($symmetry <=0 || $symmetry > $nsym) {diemolcas("invalid SYMMETRY: $symmetry  valid range 1 .. $nsym \n");}

   }



 print "--------- configuration space specification  (SINGLE DRT CASE) --------\n";
 if ($title) {printf " %-25s %-80s\n", "Title:",  $title;}
 if ($electrons) {printf " %-25s total: %4d  holes:%4d  auxel: %4d \n", "Electrons:", $electrons[1],
     $electrons[2],$electrons[3];}
 if ($multiplicity) {printf " %-25s %4d \n", "Multiplicity:",  $multiplicity;}
 if ($symmetry) {printf " %-25s %4d \n", "Symmetry     ",  $symmetry;    }
 if ($exlevel) {printf " %-25s %4d \n", "Excitation level ",$exlevel; }
 printf " %-25s %4d \n", "number of roots ",$nroot;
 printf " %-25s "," reference docc"; for ($i=1;$i<=$nsym;$i++) { printf "%4d", $refdocc[$i];} print "\n";
 printf " %-25s "," reference ras"; for ($i=1;$i<=$nsym;$i++) { printf "%4d", $refras[$i];} print "\n";
 printf " %-25s "," reference cas"; for ($i=1;$i<=$nsym;$i++) { printf "%4d", $refcas[$i];} print "\n";
 printf " %-25s "," reference aux"; for ($i=1;$i<=$nsym;$i++) { printf "%4d", $refaux[$i];} print "\n";
 print "----------------------------------------------------\n";

 # make sure that there are at least two active orbitals
 $tot=0;
 for ($i=1;$i<=$nsym;$i++) {$tot=$tot+$refdocc[$i]+$refcas[$i]+$refras[$i]+$refaux[$i];}
 if ($tot < 2){ diemolcas("specify at least two active orbitals ");}
 
 print "------------------ runtime options ------------------\n";
 printf "%-15s  %8.3f MB \n ", "columbus main memory ", $coremem;
 if ($runtest) { print " +++++++ TESTING INPUT ++++++++++++++++++++\n";}
 print "----------------------------------------------------\n";

 }

 return;
}
#------------------------
sub printorbvec{
 my @vec,$outstr;
  @vec=@_;
# print "elements:",$#vec,join(':',@vec),"\n";
  do                    
   { $outstr=' ';
     if ( $#vec >= 10 ) { for ($i=1;$i<11;$i++) { $outstr = "$outstr " . shift(@vec);}
                         print FIN "$outstr \n";}
     $outstr=' ';
     if ($#vec < 10 && $#vec > -1) {$outstr= $outstr . join (' ',@vec) . " \n";
           print FIN  "$outstr";} 
   }
   until ($#vec < 11 );
  return;
}


#---------------------------
sub create_cidrtin { 
  if ($noauto) {return;}
  open FIN,">cidrtin";
    print FIN " N / no spin orbit CI\n";
    print FIN " $multiplicity /input the spin multiplicity \n";
    print FIN " $electrons[1] /input the total number of electrons \n";
    print FIN " $nsym  /input the number of irreps (1-8) \n";
    print FIN " Y /enter symmetry labels: \n";
    for ($i=1;$i<=$nsym;$i++) {print FIN "SY$i \n";}
    for ($i=1;$i<=$nsym;$i++) {print FIN " $aobasis[$i] ";} print FIN " / number of basis functions \n";
    print FIN " $symmetry /input the molecular spatial symmetry (irrep 1:nsym) \n";

## compute frozen core vector
     @corevec=();
     for ($i=1;$i<=$nsym;$i++) {for ($j=1; $j<=$frozencore[$i];$j++) { push @corevec,$i,$j;}}
      if ($#corevec > 0 ) { printorbvec(@corevec);}
      print FIN "  /number of frozen core orbitals \n";
## compute frozen virtual vector
     @corevec=();
     for ($i=1;$i<=$nsym;$i++) {for ($j=$mobasis[$i]-$frozenvirt[$i]+1; $j<=$mobasis[$i];$j++) { push @corevec,$i,$j;}}
      if ($#corevec > 0 ) { printorbvec(@corevec);}
      print FIN "  /number of deleted virtual orbitals \n";
## compute internal vector REFDOCC:REFRAS:REFCAS:REFAUX
     @corevec=();
     for ($i=1;$i<=$nsym;$i++) 
       {for ($j=$frozencore[$i]+1; $j<=$frozencore[$i]+$refdocc[$i];$j++) { push @corevec,$i,$j;}}
     for ($i=1;$i<=$nsym;$i++) 
       {for ($j=$frozencore[$i]+$refdocc[$i]+1; $j<=$frozencore[$i]+$refdocc[$i]+$refras[$i];$j++) { push @corevec,$i,$j;}}
     for ($i=1;$i<=$nsym;$i++) 
       {for ($j=$frozencore[$i]+$refdocc[$i]+$refras[$i]+1; 
          $j<=$frozencore[$i]+$refdocc[$i]+$refras[$i]+$refcas[$i];$j++) { push @corevec,$i,$j;}}
     for ($i=1;$i<=$nsym;$i++) 
       {for ($j=$frozencore[$i]+$refdocc[$i]+$refras[$i]+$refcas[$i]+1; 
          $j<=$frozencore[$i]+$refdocc[$i]+$refras[$i]+$refcas[$i]+$refaux[$i];$j++) { push @corevec,$i,$j;}}
      if ($#corevec > 0 ) { printorbvec(@corevec); }
      print FIN "  /internal orbitals \n";

##  need splitting of lines in all print statements!

     $refdocc=0; 
     $refaux=0;
     $refcas=0;
     $refras=0;
     for ($i=1;$i<=$nsym;$i++) {$refdocc+=$refdocc[$i];
                                $refaux+=$refaux[$i];
                                $refcas+=$refcas[$i];
                                $refras+=$refras[$i];}
     print FIN " $refdocc  /input the number of ref-csf doubly-occupied orbitals\n";

##  compute reference occupation vector occminr, occmaxr
    $nactref=$electrons[1]-$refdocc-$refdocc;
     for ($i=1;$i<=$nsym;$i++) {$nactref-=$frozencore[$i]*2;}
     @occminr=();
     for ($i=1;$i<$refras;$i++) {push @occminr, 0;}
     for ($i=1;$i<=$refcas;$i++) {push @occminr, $refras+$refras-$electrons[2];}
     for ($i=1;$i<=$refaux+1;$i++) {push @occminr, $nactref-$electrons[3];}
# falls kein aktiver Raum, darf auch nichts geschrieben werden!
#    for ($i=1;$i<=$refaux;$i++) {push @occminr, $nactref-$electrons[3];}
     printorbvec(@occminr);
#    print FIN " ",join(' ',@occminr)," / occminr \n"; 
     @occmaxr=();
     for ($i=1;$i<$refras;$i++) {push @occmaxr, $refras+$refras;}
     for ($i=1;$i<=$refcas;$i++) {push @occmaxr, $nactref;}
     for ($i=1;$i<=$refaux+1;$i++) {push @occmaxr, $nactref;}
#    for ($i=1;$i<=$refaux;$i++) {push @occmaxr, $nactref;}
     printorbvec(@occmaxr);
#    print FIN " / occmaxr \n"; 
#    print FIN " ",join(' ',@occmaxr)," / occmaxr \n"; 
     print FIN "  / bminr \n";
     print FIN "  / bmaxr \n";
     @stepr=();
     for ($i=1;$i<=$refras+$refcas+$refaux;$i++) {push @stepr, 1111;}
     printorbvec(@stepr);
#    print FIN "  / stepr\n"; 
     print FIN "  $exlevel /input the maximum excitation level from the reference csfs \n";
     print FIN "  / default occmin \n";
     print FIN "  / default occmax \n";
     print FIN "  / default bmin   \n";
     print FIN "  / default bmax   \n";
     print FIN "  / default step   \n";
     print FIN " -1 / manual arc removing step \n";
     print FIN " $genspace /impose generalized interacting space restrictions? \n";
     print FIN " -1 / manual arc removing step \n";
     print FIN "  / default printing drt \n";
     print FIN " $symmetry  / default reference symmetry\n"; 
     print FIN " N /keep all of the z-walks as references? \n";
     print FIN " Y /generate walks while applying reference drt restrictions? \n";
     print FIN " N /impose additional orbital-group occupation restrictions? \n";
     print FIN " N /apply primary reference occupation restrictions? \n";
     print FIN " N /manually select individual walks? \n";
     @weight=();
     for ($i=1;$i<=$refdocc+$refras+$refcas+$refaux;$i++) {push @weight, 4;}
     $weight[0]=-1;
     printorbvec(@weight);
     print FIN "  / defaults \n";
     print FIN " 0 /input reference walk number (0 to end) \n";
     @weight=();
     for ($i=1;$i<=$refdocc+$refras+$refcas+$refaux;$i++) {push @weight, 0;}
     printorbvec(@weight);
     print FIN " /  \n";
     print FIN " N /this is an obsolete prompt. \n";
     print FIN " 0 /input mrsdci walk number (0 to end) \n"; 
     print FIN " $title \n";
     print FIN "cidrtfl\n";
     print FIN " Y /write the drt file?\n";
     close FIN;
   # system("cat cidrtin");
  return;}


sub create_cisrtin { 
  if ($noauto) {return;}
  open CISRTIN, ">cisrtin";
   print CISRTIN " \&input\n molcas=0 \n maxbl3=60000\n maxbl4=60000\n \&end\n";
  close  CISRTIN;
return;}


sub create_ciudgin {
  if ($noauto) {return;}
  open CIUDGIN, ">ciudgin";
  print CIUDGIN " \&input\n";
  print CIUDGIN " iden=0\n nroot=$nroot\n";
  print CIUDGIN " nciitr=$nciiter\n nbkitr=$nbkiter\n";
  print CIUDGIN " nvcimx=$nvcimx \n nvbkmx=$nvcimx \n nvrfmx=$nvcimx\n";
  print CIUDGIN " nvcimn=$nvcimn \n nvbkmn=$nvcimn \n nvrfmn=$nvcimn\n";
  for ($i=1; $i<=$nroot; $i++) { push @tol, $rtol;}
  print CIUDGIN " rtolci= ",join(',',@tol)," \n";
  print CIUDGIN " rtolbk= ",join(',',@tol)," \n";
  print CIUDGIN " csfprn=10 \n";
  if ($mracpf) { print CIUDGIN " NTYPE=3, GSET=2 \n";}
  elsif ($mraqcc) { print CIUDGIN " NTYPE=3, GSET=3 \n";}
  elsif ($mraqccv) { print CIUDGIN " NTYPE=3, GSET=4 \n";}
  else { print CIUDGIN " DAVCOR=10\n";}
  if (grep(/iterative/i,$refspace)) { print CIUDGIN " IVMODE=8 \n";}
  elsif (grep(/full/i,$refspace))  {print CIUDGIN " IVMODE=3 \n";}
  else {print CIUDGIN " IVMODE=0 \n";}
   print CIUDGIN " /\&end \n";
  close CIUDGIN;   
  return;}

sub create_ciudginms {
  if ($noauto) {return;}
  for ($idrt=1; $idrt<=$ndrt; $idrt++)
  {
  open CIUDGIN, ">ciudgin.drt$idrt";
  print CIUDGIN " \&input\n";
  print CIUDGIN " iden=0\n nroot=$nroot[$idrt]\n";
  print CIUDGIN " nciitr=$nciiter\n nbkitr=$nbkiter\n";
  print CIUDGIN " nvcimx=$nvcimx \n nvbkmx=$nvcimx \n nvrfmx=$nvcimx\n";
  print CIUDGIN " nvcimn=$nvcimn \n nvbkmn=$nvcimn \n nvrfmn=$nvcimn\n";
  for ($i=1; $i<=$nroot[$idrt]; $i++) { push @tol, $rtol;}
  print CIUDGIN " rtolci= ",join(',',@tol)," \n";
  print CIUDGIN " rtolbk= ",join(',',@tol)," \n";
  print CIUDGIN " csfprn=10 \n";
  if ($mracpf) { print CIUDGIN " NTYPE=3, GSET=2 \n";}
  elsif ($mraqcc) { print CIUDGIN " NTYPE=3, GSET=3 \n";}
  elsif ($mraqccv) { print CIUDGIN " NTYPE=3, GSET=4 \n";}
  else { print CIUDGIN " DAVCOR=10\n";}
  if (grep(/iterative/i,$refspace)) { print CIUDGIN " IVMODE=8 \n";}
  elsif (grep(/full/i,$refspace))  {print CIUDGIN " IVMODE=3 \n";}
  else {print CIUDGIN " IVMODE=0 \n";}
   print CIUDGIN " /\&end \n";
  close CIUDGIN;
  }
  return;}



sub create_cidrtmsin {
  if ($noauto) {return;}
  open FIN,">cidrtmsin";
    print FIN " $ndrt / number of DRTs \n";
    print FIN " N / no spin orbit CI\n";
    print FIN " $multiplicity /input the spin multiplicity \n";
    print FIN " $electrons[1][1] /input the total number of electrons \n";
    print FIN " $nsym  /input the number of irreps (1-8) \n";
    print FIN " Y /enter symmetry labels: \n";
    for ($i=1;$i<=$nsym;$i++) {print FIN "SY$i \n";}
    for ($i=1;$i<=$nsym;$i++) {print FIN " $aobasis[$i] ";} print FIN " / number of basis functions \n";
## compute frozen core vector
     @corevec=();
     for ($i=1;$i<=$nsym;$i++) {for ($j=1; $j<=$frozencore[$i];$j++) { push @corevec,$i,$j;}}
      if ($#corevec > 0 ) { printorbvec(@corevec);}
      print FIN "  /number of frozen core orbitals \n";
## compute frozen virtual vector
     @corevec=();
     for ($i=1;$i<=$nsym;$i++) {for ($j=$mobasis[$i]-$frozenvirt[$i]+1; $j<=$mobasis[$i];$j++) { push @corevec,$i,$j;}}
      if ($#corevec > 0 ) { printorbvec(@corevec); }
      print FIN "  /number of deleted virtual orbitals \n";
## compute internal vector REFDOCC:REFRAS:REFCAS:REFAUX
     @corevec=();
     for ($i=1;$i<=$nsym;$i++)
       {for ($j=$frozencore[$i]+1; $j<=$frozencore[$i]+$refdocc[$i];$j++) { push @corevec,$i,$j;}}
     for ($i=1;$i<=$nsym;$i++)
       {for ($j=$frozencore[$i]+$refdocc[$i]+1; $j<=$frozencore[$i]+$refdocc[$i]+$refras[$i];$j++) { push @corevec,$i,$j;}}
     for ($i=1;$i<=$nsym;$i++)
       {for ($j=$frozencore[$i]+$refdocc[$i]+$refras[$i]+1;
          $j<=$frozencore[$i]+$refdocc[$i]+$refras[$i]+$refcas[$i];$j++) { push @corevec,$i,$j;}}
     for ($i=1;$i<=$nsym;$i++)
       {for ($j=$frozencore[$i]+$refdocc[$i]+$refras[$i]+$refcas[$i]+1;
          $j<=$frozencore[$i]+$refdocc[$i]+$refras[$i]+$refcas[$i]+$refaux[$i];$j++) { push @corevec,$i,$j;}}
      if ($#corevec > 0 ) { printorbvec(@corevec);}
      print FIN "  /internal orbitals \n";

##  need splitting of lines in all print statements!

     $refdocc=0;
     $refaux=0;
     $refcas=0;
     $refras=0;
     for ($i=1;$i<=$nsym;$i++) {$refdocc+=$refdocc[$i];
                                $refaux+=$refaux[$i];
                                $refcas+=$refcas[$i];
                                $refras+=$refras[$i];}


      for ($idrt=1; $idrt <=$ndrt; $idrt++)
       { print FIN " $symmetry[$idrt] / input the molecular spatial symmetry\n";
         print FIN " $refdocc  /input the number of ref-csf doubly-occupied orbitals\n";
##       compute reference occupation vector occminr, occmaxr
         $nactref=$electrons[$idrt][1]-$refdocc-$refdocc;
         for ($i=1;$i<=$nsym;$i++) {$nactref=$nactref-$frozencore[$i]*2;}
         @occminr=();
         for ($i=1;$i<$refras;$i++) {push @occminr, 0;}
         for ($i=1;$i<=$refcas;$i++) {push @occminr, $refras+$refras-$electrons[$idrt][2];}
         for ($i=1;$i<=$refaux+1;$i++) {push @occminr, $nactref-$electrons[$idrt][3];}
         print FIN " ",join(' ',@occminr)," / occminr \n";
         @occmaxr=();
         for ($i=1;$i<$refras;$i++) {push @occmaxr, $refras+$refras;}
         for ($i=1;$i<=$refcas;$i++) {push @occmaxr, $nactref;}
         for ($i=1;$i<=$refaux+1;$i++) {push @occmaxr, $nactref;}
         print FIN " ",join(' ',@occmaxr),"  / occmaxr\n";
         print FIN "  / bminr \n";
         print FIN "  / bmaxr \n";
         @stepr=();
         for ($i=1;$i<=$refras+$refcas+$refaux;$i++) {push @stepr, 1111;}
         printorbvec(@stepr);
#        print FIN "  / stepr\n";
         print FIN "  $exlevel /input the maximum excitation level from the reference csfs \n";
         print FIN "  / default occmin \n";
         print FIN "  / default occmax \n";
         print FIN "  / default bmin   \n";
         print FIN "  / default bmax   \n";
         print FIN "  / default step   \n";
        }
     print FIN " $genspace /impose generalized interacting space restrictions? \n";
     print FIN "  / default printing drt \n";
     print FIN "  / default printing drt \n";
     print FIN "  / default printing drt \n";



      for ($idrt=1; $idrt <=$ndrt; $idrt++)
      { print FIN " $symmetry[$idrt]  / default reference symmetry\n";
        print FIN " N /keep all of the z-walks as references? \n";
        print FIN " Y /generate walks while applying reference drt restrictions? \n";
        print FIN " N /impose additional orbital-group occupation restrictions? \n";
        print FIN " N /apply primary reference occupation restrictions? \n";
        print FIN " N /manually select individual walks? \n";
        print FIN "  / defaults \n"; 
        print FIN " 0 /input reference walk number (0 to end) \n";
        print FIN " 0 0 0 0 0 0 0 0 \n";
      }

      for ($idrt=1; $idrt <=$ndrt; $idrt++)
       { print FIN " 0 /input mrsdci walk number (0 to end) \n";
         print FIN " / \n";
       }
       print FIN " $title \n";
     close FIN;

  return;
 }

 sub run_tran
 { # create tranin
   open TRAN,">tranin";
    print TRAN " &input\n SEWARD=1\n LUMORB=1\n &end\n";
   close TRAN;
   # create link to RASSCF orbitals
    system ("cp $ENV{Project}.RasOrb mocoef_lumorb");
   # create links to ORDINT, ONEINT
    system ("ln -s $ENV{Project}.OrdInt ORDINT");
    system ("ln -s $ENV{Project}.OneInt ONEINT");
    system("$colpath/tran.x -m $coremem ");
    checkterm("tran.x");
   return;
 }



print "************************************************************************************\n";
print "*                   COLUMBUS version 7.0 beta (may 5th, 2012)                      *\n";
print "*                                                                                  *\n";
print "*           multi-reference single and double excitation configuration             *\n";
print "*           interaction based on the graphical unitary group approach              *\n";
print "*                                                                                  *\n";    
print "*                                 authors:                                         *\n";
print "*                                                                                  *\n";    
print "*                                                                                  *\n";    
print "************************************************************************************\n";

system(" pwd; ls -l ");

#cleanuplinks();
 
$input="COLUMBUSINP";  
unless ( -f $input ) {  diemolcas("cannot find input file $input");}
$colpath=$ENV{COLUMBUS};
print "COLUMBUS=$colpath\n"; 
if ( ! -d $colpath ) { diemolcas("COLUMBUS environment variable not set!");}


setdefaults();

analyse_input();



create_cisrtin();


if (!$multidrt) { create_cidrtin();
                  system("$colpath/cidrt.x  -m $coremem < cidrtin > cidrtls ");
                  analyse_cidrtls();
                  create_ciudgin();
                  if ($runtest) {stopit("testing input");}
                }
else            { create_cidrtmsin(); 
                  system("$colpath/cidrtms.x -m $coremem < cidrtmsin > cidrtmsls ");
                  analyse_cidrtmsls();
                  create_ciudginms();
                  if ($runtest) {stopit("testing input");}
                }

if (!$multidrt) { 
                  run_tran();
                  checkterm("molcas.x");
                  if ($parallel) {stopit("execute parallel CI manually");}
                  system("$colpath/ciudg.x -m $coremem");
                  checkterm("ciudg.x");
                  system("cat ciudgsm");
                  open CIPCIN,">cipcin";
                  print CIPCIN " 1\n";
                  close CIPCIN;
                  system("$colpath/cipc.x -m $coremem < cipcin > cipcls");
                  $w=pwd();chop $w; 
                     print "====> full listing          on $w/ciudgls\n";
                     print "====> short listing         on $w/ciudgsm\n";
                   print "====> wavefunction analysis on $w/cipcls\n";
                 }
else             { for ($idrt=1; $idrt<=$ndrt; $idrt++)
                   { print "=========================  MRCI for DRT $idrt ===============================\n";
                     system ("cp -f ciudgin.drt$idrt ciudgin");
                     system("rm -f cidrtfl; ln -s cidrtfl.$idrt cidrtfl");
                     system("$colpath/molcas.x -m $coremem  ");
                     checkterm("molcas.x");
                     if ($parallel) {stopit("execute parallel CI manually");}
                     system("$colpath/ciudg.x -m $coremem");
                     checkterm("ciudg.x");
                     system("cat ciudgsm");
                     open CIPCIN,">cipcin";
                     print CIPCIN " 1\n";
                     close CIPCIN;
                     system("$colpath/cipc.x -m $coremem < cipcin > cipcls.drt$idrt");
                     system("mv ciudgls ciudgls.drt$idrt; mv ciudgsm ciudgsm.drt$idrt");
                     system("mv civout civout.drt$idrt; mv civfl civfl.drt$idrt");
                     $w=pwd();chop $w; 
                     print "====> full listing          on $w/ciudgls.drt$idrt\n";
                     print "====> short listing         on $w/ciudgsm.drt$idrt\n";
                     print "====> wavefunction analysis on $w/cipcls.drt$idrt\n";
                   }
                 }
# NOs in molcas format schreiben und molcas property aufrufen
# mehr error check bzgl. CSF space definition
# multidrt/transition moments???

  cleanuplinks();





stopit("ok");

exit

