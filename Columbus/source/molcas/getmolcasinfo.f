!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
       program molcasinfo

       implicit none
       real*8 repnuc
       integer nsym,nbpsy(8),ntot,mclau,i 
       integer nfrozen(8),nfvirt(8),norb(8)
       character*4 slabel(8)
       character*3 tmplabel2(8)
       character*8 tmplabel(3)
       equivalence(tmplabel2,tmplabel)
       call link_it()
       call getenvinit()
       call fioinit()
       call NameRun('RUNFILE')
       call get_dscalar('PotNuc',repnuc)
       call mcget_iscalar('nsym',nsym)
       call mcget_iarray('nbas',nbpsy,nsym)
        ntot=sum(nbpsy(1:nsym))
       call mcget_iarray('nfro',nfrozen,nsym)
       call mcget_iarray('ndel',nfvirt,nsym)
       call mcget_iarray('norb',norb,nsym)
        mclau=24
        call mcget_carray('irreps',tmplabel,mclau)
        do i=1,nsym
          slabel(i)(2:4)=tmplabel2(i)
          slabel(i)(1:1)=' '
        enddo

        write(6,*) '***EXTRACTED INFO FROM MOLCAS RUNFILE*** '
        write(6,'(a,f18.8)') ' Nuclear Repulsion Energy=',repnuc
        write(6,'(a,t25,8i6)')   ' NSYM =',nsym
        write(6,'(a,t25,8i6)')   ' AO BASIS FUNCTIONS =',nbpsy(1:nsym)
        write(6,'(a,t25,8i6)')   ' FROZEN CORE =',nfrozen(1:nsym)
        write(6,'(a,t25,8i6)')   ' FROZEN VIRTUAL =',nfvirt(1:nsym)
        write(6,'(a,t25,8i6)')   ' MO ORBITALS   =',norb(1:nsym)
        write(6,'(a,t25,8(a4,3x))') ' IRREP LABELS =',slabel(1:nsym)
        stop
        end







