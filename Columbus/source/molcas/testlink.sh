#!/bin/sh
#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/


  getkeyword () {
     INP=`grep $1 $2 | head -1 | sed -e 's/ *$//' `
     grep "^$1 " $2 > /dev/null
     if [ $? -eq 0 ] ; then
       VALUE=${INP#"$1"}
       eval "$1"=\'"${VALUE# *}"\'
       echo "found keyword $1 in $2 with value: $VALUE "
       return 0
     else
       echo "could not find keywork $1 in $2 "
       return 1
     fi
    }

  getkeywtmp () {
     INP=`grep $1 $2 | head -1 | sed -e 's/ *$//' `
     grep "^$1 " $2 > /dev/null
     if [ $? -eq 0 ] ; then
       VALUE=${INP#"$1"}
       eval TMP=\'"${VALUE# *}"\'
       echo "found keyword $1 in $2 with value $TMP "
       return 0
     else
       return 1
     fi
    }




    chkenv () {
     eval "TMPP=\$$1"
     echo $TMPP
     if [ -z "$TMPP" ] ; then
      echo "$1 environment variable not defined "
      echo "exiting ..."
     fi
     return 0
   }


########  read environment settings from $COLUMBUS/../install.config #####################
echo " testing link procedures for molcas "
# should already be set by install.automatic

echo " =====  colinstall.sh  ... version 03/03/2002 (tm)      ===== "
echo " =====  Columbus $COLUMBUSVERSION installation starting ===== "
chkenv COLUMBUS
chkenv MOLCAS 

getkeyword RANLIB "$COLUMBUS/../install.config"
getkeyword BLASLIBRARY "$COLUMBUS/../install.config"
getkeyword LAPACKLIBRARY "$COLUMBUS/../install.config"
getkeyword MOLCAS "$COLUMBUS/../install.config"
getkeyword MCFLAGS "$COLUMBUS/../install.config"


# check for Symbols file and libmolcas.a

if [ -f "$MOLCAS/lib/libmolcas.a" ]; 
  then 
  echo "found libmolcas.a ... fine "
else
  echo "missing libmolcas.a  ( $MOLCAS/lib/libmolcas.a ) "
  exit 2
fi 

if [ -f "$MOLCAS/Symbols" ];
  then
  echo "found Symbols file ... fine "
else
  echo "missing Symbols  ( $MOLCAS/Symbols ) "
  exit 3
fi

### try to evaluate proper value for MCFLAGS
# what integer size are they using

NEW_MCFLAGS="molcas"
ADDUNDERSCORE=0
M_CFLAGS=`grep CFLAGS $MOLCAS/Symbols`
M_I8=`echo $M_CFLAGS | grep 'I8 ' `
# grep success = 0  failure = 1
if [ $? != 0 ] ; then
  echo "they are using integer*4 "
  echo "with MOLCAS this implies a 64bit system "
  INTSIZE=4
else
  echo "they are using integer*8 "
  echo "with MOLCAS this implies a 64bit system "
  NEW_MCFLAGS="$NEW_MCFLAGS molcas_int64"
  INTSIZE=8
fi 

echo "suggested MCFLAGS=$NEW_MCFLAGS"

# figure out the f90 compiler
M_F90=`grep '^F90=' $MOLCAS/Symbols`
M_CC=`grep '^CC=' $MOLCAS/Symbols`
M_F90=`echo $M_F90 | sed -e 's/^.*=//' | sed -e "s/'//g" `
M_CC=`echo $M_CC | sed -e 's/^.*=//'  | sed -e "s/'//g" `
echo "Fortran compiler ${M_F90##/*/} ( $M_F90 ) "
echo "C compiler ${M_CC##/*/} ( $M_CC ) "


################ INTEL ####################################


INTEL=`echo $M_F90 | grep ifort `
X=$?

if  [ "$X" == 0 ] &&  [ "$INTSIZE" == 8 ] 
  then
   echo "***try port***:   linuxifc.emt64"
   echo "***MCFLAGS****:   $NEW_MCFLAGS" 
   SUGGESTED=linuxifc.emt64
fi 

if  [ "$X" == 0 ] &&  [ "$INTSIZE" == 4 ] 
  then
   echo "***try port***:   linuxifc"
   echo "***MCFLAGS****:   $NEW_MCFLAGS" 
   SUGGESTED=linuxifc
fi 

################  XLF  ####################################

XLF=`echo $M_F90 | grep xlf `
X=$?
if [ "$X" == 0 ] 
  then
#  xlf compiler may require the -qextname option
  QEXT=`grep '^F90FLAGS=' $MOLCAS/Symbols`
  QEXT=`echo $QEXT | grep qextname `
  $Y=$?
  if [ $? == 0 ] ; then
     NEW_MCFLAGS="$NEW_MCFLAGS molcas_ext"
     ADDUNDERSCORE=1
  fi
  if [ $INTSIZE == 4 ]; then
       echo "***try port***: rs6000.4.x (AIX 32bit OS)"
       SUGGESTED=rs6000.4.x
  else
       echo "***try port***: rs6000.5.x (AIX 64bit OS) or sp4int64 "
       SUGGESTED=sp4int64
  fi
  echo "***MCFLAGS****:   $NEW_MCFLAGS" 
fi 

################ Portland   ####################################





################ GNU (g95)   ####################################



##################LINK TESTS####################################
#
#  first the fortran code must be ported with the current MCFLAGS/Keywords
#  using cmdc.pl 
# 

# now link

# MOLCAS V6.5PL970 
MOLCASOBS="abend.o aixcls.o aixerr.o aixopn.o aixrd.o aixwr.o allocdisk.o cdafile.o cio.o crdrun.o cxrdrun.o daclos.o daname_mf.o daname.o ddafile.o drdrun.o dxrdrun.o fastio.o fioinit.o get_dscalar.o getenvc.o getenvf.o gxrdrun.o icloc.o idafile_.o idafile.o inique.o inquire.o irdrun.o isfreeunit.o ixrdrun.o king.o mkrun.o mpdafile.o namerun.o opnrun.o qenter.o qexit.o qtrace.o remdmp.o stdfmt.o strnln.o ffrun.o ffxrun.o sysexpand.o sysinf.o sysmessages.o sysputs.o systemc.o systemf.o timingc.o upcase.o wr_motra_info.o xquit.o xflush.o daname_main.o iprintlevel.o hbomb.o extract.o cextract.o get_iscalar.o get_iarray.o get_carray.o dmpone.o opnone.o clsone.o opnord.o getord.o int2real.o loadints.o allocramd.o rdord.o  ordin1.o ordin2.o rdord_.o upkr8.o tcd_r8.o rld_r8.o one_ulp.o init_getint.o decideoncholesky.o getint.o daname_mf_wa.o rdchovec.o get_darray.o prgminitf.o prgminit.o mystring.o get_progname.o"

if [ "$SUGGESTED" = "sp4int64" ] ; then
 MOLCASOBS="$MOLCASOBS icopy.o"
fi

gmake  -f ./makefile.testmolcas  MMID="$SUGGESTED" MOLCASOBS="$MOLCASOBS"  LIBMOLCAS="$MOLCAS/lib/libmolcas.a"  molcas.a

exit


