!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
ccnvrt.f
ccnvrt part=1 of 1.  p and k supermatrix program.
cversion=5.5 last modified: 15-mar-00
c
c deck cnvrt
c
c*******************************************************************
c
c   this computer program contains work performed partially by the
c   argonne national laboratory theoretical chemistry group under
c   the auspices of the office of basic energy sciences,
c   division of chemical sciences, u.s. department of energy,
c   under contract w-31-109-eng-38.
c
c   these programs may not be (re)distributed without the
c   written consent of the argonne theoretical chemistry group.
c
c   since these programs are under development, correct results
c   are not guaranteed.
c
c*******************************************************************
c
      program cnvrt
c
c  this program transforms the integral file into one that is
c  compatible with the scf program by changing the indices or labels
c  of the one-electron integrals and by calculating the p and k/2
c  elements for the two-electron integrals.
c
c  version log:
c  15-mar-00 remove subroutine name conflict (rmp)
c  17-dec-99 improve array allocation (rmp)
c  03-feb-99 adjust formats (rmp)
c  10-jul-98 more cleanup and sifr1n patch (rmp)
c  11-apr-96 minor cleanup (rmp)
c  11-sep-91 parallel version included in columbus distribution. -rjh
c  07-jul-91 preliminary parallel version using tcgmsg. -rjh
c  25-may-91 sifs version. minor cleanup. -rmp/rls
c  08-90 allocate arrays (rmp)
c  08-89 mdc update, zero-out improvement (rmp,mpn)
c  08-88 add sun, revise for distribution (eas,rmp)
c  1983 (cnvrt)   major revisions, r. m. pitzer
c  1981 (convert) major revisions, f. b. brown (i. shavitt group)
c  1979 (soconv)  y. yamaguchi & b. brooks (h. f. schaefer group)
c
c  things to do in this code:
c  * replace hashsort code with with simple canonical sort code.
c  * add asynchronous i/o support.
c
c  cmdc info:
c  keyword   description
c  -------   -----------
c  ibm       ibm (mainframe) code.
c  sun       sun code.
c  cray      cray code. use compiler option for no double precision.
c  parallel  tcgmsg parallel code.
c  int64
c
      implicit none
      integer   ntitmx,   nbfmx,    ninfmx,  nengmx,   nmapmx,   nstu
      parameter(ntitmx=20,nbfmx=510,ninfmx=9,nengmx=40,nmapmx=20,nstu=8)
c
      integer     nlist, ifile, ifile2, nfile, idebug
      common /c1/ nlist, ifile, ifile2, nfile, idebug
c
      integer     nst, ns, isfr, nu, nnbft
      common /c2/ nst, ns, isfr, nu, nnbft
c
c     # core(1:lencor) is used for miscellaneous  workspace.
      integer    lencor
      parameter( lencor=10000000 )
c
      real*8 core(lencor)
c
      character*80 title(ntitmx)
      character*8 bfnlab(nbfmx)
      real*8 energy(nengmx)
      integer ietype(nengmx), imtype(nmapmx), info(ninfmx)
      character*4  slabel(nstu)
c
      real*8 repnuc
      integer i, buffer, values, labels, l1rec, n1max, l2rec, n2max,
     & symb, nso, mnl, ms, mapin, map, nsopr, nblpr, ipq, ninput,
     & ntitle, isto, ninfo, ierr, nmap, nenrgy, itotal, stvc, ibitv,
     & itemp, spo, lblij, plabs, psup, ksup, ilbl, klbl
      character*60 fname
c
c     # bummer error types.
      integer    wrnerr,  nfterr,  faterr
      parameter (wrnerr=0,nfterr=1,faterr=2)
c
c     # nbufo1 = number of 1-e values.
c     # nbufo2 = number of 2-e values.
      integer    nbufo1,      nbufo2
*@ifdef cray
*      parameter( nbufo1=4095, nbufo2=2730 )
*@else
      parameter( nbufo1=2304, nbufo2=1152 )
*@endif
c
      integer  atebyt, forbyt
      external atebyt, forbyt
c
      real*8   sifsce
      external sifsce
c
*@ifdef parallel
*      integer  nodeid
*      external nodeid
*@endif
c
c     # parallel message passing initialization, or stub as appropriate.
c     call pbginf
c
c     # open the output listing file.  write to file 'cnvrtls'.
c
      nlist=6
*@ifdef ibm
*C     # use preconnected unit 6
*@elif defined  parallel
*      fname = 'cnvrtls'
*      call trnfln( 1, fname )
*      call pfname( 1, fname )
*      open ( nlist, file=fname, status='unknown' )
*@else
      fname = 'cnvrtls'
      call trnfln( 1, fname )
      open ( nlist, file=fname, status='unknown' )
*@endif
c
      call ibummr( nlist )
c
c     # open the input file.  read from file 'cnvrtin'.
c
      ninput=5
      fname = 'cnvrtin'
      call trnfln( 1, fname )
*@ifdef parallel
*      if ( nodeid() .ne. 0 ) call pfname( 1, fname )
*C     # this distributes the input file to all nodes.
*      call pfcopy( 99, 0, fname )
*@else
c     # translate the filename and open.
*@endif
      open ( ninput, file=fname, status='old' )
c
      nmap   = 0
      ierr   = 0
      ninfo  = 0
      isfr   = 0
      nenrgy = 0
c
      isto   = 0
      ifile  = 4
      nfile  = 3
      idebug = 0
      ifile2 = 8
      read (ninput,*) isto, ifile, nfile, idebug, ifile2
c
      close (ninput)
c
      if ( ifile  .eq. 0 ) ifile  = 4
      if ( ifile2 .eq. 0 ) ifile2 = 8
      if ( nfile  .eq. 0 ) nfile  = 3
c
      write (nlist,10)
   10 format(28x,'program "cnvrt" 5.5'/
     1       28x,'columbus program system'/
     2       28x,'supermatrix formation program'//
     3       28x,'Programmed by Russell M. Pitzer'/
     4       28x,'version date: 15-mar-00')
c
      call who2c( 'CNVRT', nlist )
c
c     # open ifile.  read from file 'aoints'.
c     # open nfile.  write to file 'pkints'.
c
      fname = 'aoints'
      call trnfln( 1, fname )
*@ifdef parallel
*      call pfname( 1, fname )
*@endif
      open (ifile, file=fname, status='old', form='unformatted')
c
      fname = 'pkints'
      call trnfln( 1, fname )
*@ifdef parallel
*      call pfname( 1, fname )
*@endif
      open (nfile,file=fname, status='unknown', form='unformatted')
c
      write (nlist,40) ifile, nfile
   40 format(/'integrals are read in from file',i3,' (ifile).'/
     1  'integrals are written out on file',i3,' (nfile).')
c
      call sifrh1(  ifile,   ntitle,  nst,     isfr,    ninfo,
     $     nenrgy,  nmap,    ierr )
c
      if ( ierr .ne. 0 ) then
        call bummer('from sifrh1, ierr=', ierr, faterr )
      elseif ( ntitle .gt. ntitmx ) then
        call bummer('from sifrh1, ntitle=', ntitle, faterr )
      elseif ( isfr .gt. nbfmx ) then
        call bummer('from sifrh1, isfr=', isfr, faterr )
      elseif ( ninfo .gt. ninfmx ) then
        call bummer('from sifrh1, ninfo=', ninfo, faterr )
      elseif ( nenrgy .gt. nengmx ) then
        call bummer('from sifrh1, nenrgy=', nenrgy, faterr )
      elseif ( nmap .gt. nmapmx ) then
        call bummer('from sifrh1, nmap=', nmap, faterr )
      endif
c
c     # read the second header record.
c     # core(*)==> symb(1:isfr), nso(1:nst), mnl(1:isfr), ms(1:isfr),
c     #            mapin(1:isfr), map(1:isfr,1:nmap)
c
      symb  = 1
      nso   = symb  + forbyt( isfr )
      mnl   = nso   + forbyt( nst )
      ms    = mnl   + forbyt( isfr )
      mapin = ms    + forbyt( isfr )
      map   = mapin + forbyt( isfr )
c
      call sifrh2( ifile,     ntitle,    nst,       isfr,      ninfo,
     &  nenrgy,    nmap,      title,     core(nso), slabel,    info,
     &  bfnlab,    ietype,    energy,    imtype,    core(map), ierr )
c
      if ( ierr .ne. 0 ) then
        call bummer('from sifrh2(), ierr=', ierr, faterr )
      endif
c
      write(nlist,'(/a)') 'input SIFS file header information:'
      write(nlist,'(a)') (title(i),i=1,ntitle)
c
      write(nlist,'(/a)') 'input energy(*) values:'
      call sifpre( nlist, nenrgy, energy, ietype )
c
      repnuc = sifsce( nenrgy, energy, ietype )
      write(nlist,"(/'total core energy =',1pe20.12)") repnuc
c
      write(nlist,"(/'nsym =',i2,' nbft =',i4/)") nst, isfr
c
c     # initialize some addressing arrays.
c     # extract the bfn-to-center map vector, and
c     # the bfn-to-bfntyp map vector from map(*,*).
c
            call getmap( nmap, imtype, core(symb), core(mnl), core(ms),
     &                   core(mapin), core(map), core(nso) )
c
c     # extract some file and record length parameters.
      l1rec  = info(2)
      n1max  = info(3)
      l2rec  = info(4)
      n2max  = info(5)
c
c     # process the 1-e integral arrays.
c
c     # core(*)==> symb(1:isfr), nso(1:nst), mnl(1:isfr), ms(1:isfr),
c     #            mapin(1:isfr), buffer(1:l1rec), values(1:n1max),
c     #            labels(1:2,1:n1max), stvc(1:nnbft,0:3), spo(1:nbufo1)
c     #            lblij(1:nbufo1), plabs(1:nbufo1)
      buffer = mapin  + forbyt( isfr )
      values = buffer + atebyt( l1rec )
      labels = values + atebyt( n1max )
      stvc   = labels + forbyt( ((n1max + 3)/4)*8 )
      spo    = stvc   + atebyt( 4*nnbft )
      lblij  = spo    + atebyt( nbufo1 )
      plabs  = lblij  + forbyt( nbufo1 )
      itotal = plabs  + forbyt( nbufo1 ) - 1
c
      if ( itotal .gt. lencor ) then
        call bummer('before out1e(), itotal=', itotal, faterr )
      endif
c
            call out1e( l1rec,        n1max,        core(buffer),
     &    core(values), core(labels), core(stvc),   title,
     &    ninfo,        info,         core(symb),   core(nso),
     &    core(mnl),    core(ms),     core(mapin),  slabel,
     &    bfnlab,       nbufo1,       core(spo),    core(lblij),
     &    core(plabs),  repnuc )
c
c     # process the 2-e integral arrays.
c     # core(*)==> symb(1:isfr), nso(1:nst), ipq(1:isfr+1),
c     #            buffer(1:l2rec), values(1:n2max),
c     #            labels(1:4,1:n2max), ibitv(1:n2max), psup(1:nbufo2),
c     #            ksup(1:nbufo2), ilbl(1:nbufo2), klbl(1:nbufo2),
c     #            plabs(1:nbufo2)
c
      nsopr  = nso    + forbyt( nst )
      nblpr  = nsopr  + forbyt( nst )
      ipq    = nblpr  + forbyt( nst )
      buffer = ipq    + forbyt( isfr + 1 )
      values = buffer + atebyt( l2rec )
      labels = values + atebyt( n2max )
      ibitv  = labels + forbyt( ((n2max + 1)/2)*8 )
      psup   = ibitv  + forbyt( ((n2max+63)/64)*64 )
      ksup   = psup   + atebyt( nbufo2 )
      ilbl   = ksup   + atebyt( nbufo2 )
      klbl   = ilbl   + forbyt( nbufo2 )
      plabs  = klbl   + forbyt( nbufo2 )
      itotal = plabs  + atebyt( nbufo2 ) - 1
c
      if ( itotal .gt. lencor ) then
        call bummer('before phk(), itotal=', itotal, faterr )
      endif
c
c     # open the input 2-e integral file.
      itemp = ifile2
      fname = 'aoints2'
      call trnfln( 1, fname )
*@ifdef parallel
*      call pfname( 1, fname )
*@endif
      call sifo2f( ifile, itemp, fname, info, ifile2, ierr )
      if ( ierr .ne. 0 ) then
        call bummer('from sifo2f(), ierr=', ierr, faterr )
      endif
c
            call phk( l2rec,        n2max,        core(symb),
     &  core(nso),    core(nsopr),  core(nblpr),  core(ipq),
     &  core(buffer), core(values), core(labels), core(ibitv),
     &  info,         nbufo2,       core(psup),   core(ksup),
     &  core(ilbl),   core(klbl),   core(plabs) )
c
      call sifc2f( ifile2, info, ierr )
      if ( ierr .ne. 0 ) then
        call bummer('from sifc2f(), ierr=', ierr, faterr )
      endif
c
c     # message passing cleanup: stub otherwise
c     call pend
c
      close (ifile)
      close (nfile)
c
      stop 'end of cnvrt'
      end
c deck getmap
      subroutine getmap( nmap, imtype, symb,       mnl,       ms,
     &                   mapin,       map,       nso       )
c
c  extract mnl(*) and ms(*) from map(*,*).
c
c  25-may-91 written at anl
c
      implicit none
      integer     nst, ns, isfr, nu, nnbft
      common /c2/ nst, ns, isfr, nu, nnbft
c
c     # dummy:
      integer nmap
      integer imtype(*), symb(*), mnl(isfr), ms(isfr), mapin(*),
     &        map(isfr,*), nso(*)
c
c     # local:
      integer    mnltyp,   mstyp
      parameter( mnltyp=4, mstyp=3 )
      integer i, nbft, imnl, ims, nbi, j
c
      integer nndxf
      nndxf(i) = (i * (i - 1)) / 2
c
c     # set the default mapin(*) values.
      do i = 1, isfr
        mapin(i) = i
      enddo
c
      nbft  = 0
      nnbft = 0
      do 30 i = 1, nst
        nbi      = nso(i)
        do 20 j = (nbft+1), (nbft+nbi)
          symb(j) = i
   20   continue
        nbft  = nbft + nbi
        nnbft = nnbft + nndxf( nbi + 1 )
   30 continue
c
      imnl = 0
      ims  = 0
      do i = 1, nmap
        if ( imtype(i) .eq. mnltyp ) then
          imnl = i
        elseif ( imtype(i) .eq. mstyp ) then
          ims = i
        endif
      enddo
c
c     # extract the bfntyp(*) array from map(*,*).
      if ( imnl .eq. 0 ) then
c       # default: set everything to an s-type function.
        call iset( isfr, 1, mnl, 1 )
      else
       call icopy_wr( isfr, map(1,imnl), 1, mnl, 1 )
      endif
c
c     # extract the bfn-to-center array from map(*,*).
      if ( ims .eq. 0 ) then
c       # default: set everything to one center.
        call iset( isfr, 1, ms, 1 )
      else
        call icopy_wr( isfr, map(1,ims), 1, ms, 1 )
      endif
c
      return
      end
c deck out1e
      subroutine out1e( l1rec,        n1max,        buffer,
     &    values,       labels,       stvc,         title,
     &    ninfo,        info,         symb,         nso,
     &    mnl,          ms,           mapin,        slabel,
     &    bfnlab,       nbufo,        spo,          lblij,
     &    plabs,        repnuc )
c
c  read in the stvc(*,*) array and write the output file header and
c  1-e integral arrays.
c
c  25-may-91 SIFS version. -rmp/rls
c
      implicit none
      integer     nlist, ifile, ifile2, nfile, idebug
      common /c1/ nlist, ifile, ifile2, nfile, idebug
c
      integer    nbfmx
      parameter( nbfmx=510 )
c
      integer     nst, ns, isfr, nu, nnbft
      common /c2/ nst, ns, isfr, nu, nnbft
c
c     # dummy:
      integer l1rec, n1max, ninfo, nbufo
      integer info(*), symb(*), nso(*), mnl(isfr), ms(isfr), mapin(*),
     &        lblij(nbufo)
      real*8 buffer(l1rec), values(n1max), labels(2,n1max),
     & stvc(nnbft,0:3), spo(nbufo), repnuc
      character*80 title(*)
      character*8 bfnlab(isfr)
      character*4 slabel(nst)
c
c     # see dump1() for actual plabs(*) declaration.
      integer plabs(*)
c
c     # local:
      integer i, j, ij, ierr, ist, iso, is, itypeb, ibfld, isym, ni,
     & ibufo
      real*8 score, tcore, vcore, vecore
      character*8 chrtyp
c
      integer nd(8), lxyzir(3), inam(4)
      character*3 ityp(8), mtype(nbfmx)
c
      real*8     zero,       small
      parameter( zero=0.0d0, small=1.0d-15 )
c
c     # bummer error types.
      integer    wrnerr,  nfterr,  faterr
      parameter (wrnerr=0,nfterr=1,faterr=2)
c
      integer    more,   nomore
      parameter( more=0, nomore=1 )
c
c     # dummy lxyzir(*) values, default inam(*) and nd(*) values:
      data lxyzir / 3*1 /
      data inam   / 1, 2, 3, 4 /
      data nd     / 8*1 /
c
      ierr   = 0
      score  = zero
      tcore  = zero
      vcore  = zero
      vecore = zero
c
      write(nlist,'(a,8i5)') 'symmetry  =',(i,i=1,nst)
      write(nlist,'(a,8(1x,a4))') 'slabel(*) =',(slabel(i),i=1,nst)
      write(nlist,'(a,8i5)') 'nbpsy(*)  =',(nso(i),i=1,nst)
c
      write(nlist,'(/a,(7i10))') 'info(*) =',(info(i),i=1,ninfo)
c
      write(nlist,'(/a)') 'input orbital labels, i:bfnlab(i)='
      write(nlist,"(6(i4,':',a8))") (i,bfnlab(i),i=1,isfr)
c
c     # write out the ms(*) and mnl(*) map vectors:
c
      write(nlist,'(/a)') 'bfn_to_center map(*), i:map(i)'
      write(nlist,"(10(i4,':',i3))") (i,ms(i),i=1,isfr)
c
      write(nlist,'(/a)') 'bfn_to_orbital_type map(*), i:map(i)'
      write(nlist,"(10(i4,':',i3))") (i,mnl(i),i=1,isfr)
c
c     # read in the stvc(*,*) array.
c
            call rdstvc(  ifile,   info,    buffer,  values,  labels,
     &  nst,     nso,     mapin,   nnbft,   stvc,    score,   tcore,
     &  vcore,   vecore,  symb,    ierr )
c
      write(nlist,6010) score, tcore, vcore, vecore
c
cccc
c     # temporary patch for 5.4 version of sifr1n
      if(ierr.eq.-4) ierr=0
cccc
      if ( ierr .ne. 0 ) then
        call bummer('stvc: from rdstvc(), ierr=', ierr, faterr )
      endif
c
c     # write the output file header.
c
c     # check for nonzero core potential integrals.
c     # set nu=3 for stv; nu=4 for stvc.
c
      nu = 3
      do 100 i = 1, nnbft
        if ( stvc(i,3) .ne. zero ) then
          nu = 4
          go to 101
        endif
100   continue
101   continue
c
c     # set the character irrep labels.
      do i = 1, nst
        ityp(i) = slabel(i)(2:4)
      enddo
c
c     # set ns = number of symmetry-distinct centers.
c     # set mtype(i) = character center-label for each distinct center.
      ns = 0
      do i = 1, isfr
        ns = max( ns, ms(i) )
        mtype( ms(i) ) = bfnlab(i)(4:6)
      enddo
c
c     # add in the core contributions from the 1-e hamiltonian arrays.
      repnuc = repnuc + tcore + vcore + vecore
c
c     # first output header record:
      write (nfile) title(1), repnuc, nst, ns, isfr, nu
c
c     # second output header record:
      write (nfile) (nd(ist),ist=1,nst), (ityp(ist), ist=1,nst),
     & (nso(ist),ist=1,nst), (mtype(is), is=1,ns),
     & (ms(iso),iso=1,isfr), (mnl(iso), iso=1,isfr),
     & (inam(i), i=1,nu), lxyzir
c
c     # write out the output 1-e records:
c
      do 700 itypeb = 0, (nu-1)
c
        call siftyp( 0, itypeb, chrtyp )
c
        if ( idebug .ge. 1 ) then
c         # write out the array.
          call plsbkc( 'Total ' // chrtyp // ' array:',
     &     stvc(1,itypeb), nst, slabel, nso, bfnlab, 2, nlist )
        endif
c
        ibufo = 0
        ibfld = 0
c
        ij = 0
        do 600 isym = 1, nst
          ni = nso(isym)
          do 500 i = 1, ni
            do 400 j = 1, i
              ij = ij + 1
c
              if ( abs(stvc(ij,itypeb)) .gt. small ) then
                if ( ibufo .eq. nbufo ) then
c
c                 # buffers are full. pack the labels and
c                 # dump to the output file.
c
                  call dump1( nfile,  more,   ibufo,  nnbft,
     &                nbufo,  spo,    lblij,  plabs )
c
                  ibfld = ibfld + 1
                  ibufo = 0
                endif
                ibufo        = ibufo + 1
                lblij(ibufo) = ij
                spo(ibufo)   = stvc(ij,itypeb)
              endif
400         continue
500       continue
600     continue
c
c       # dump the last record of this type.
c
            call dump1( nfile,  nomore, ibufo,  nnbft,
     &          nbufo,  spo,    lblij,  plabs )
c
        ibufo = ibfld * nbufo + ibufo
        ibfld = ibfld + 1
        write(nlist,"('out1e:',i12,1x,a,' integrals written in',i3,
     &                ' records.')") ibufo, chrtyp, ibfld
700   continue
c
      return
6010  format(1p/'out1e:',
     & t9,'score  =',e25.15/
     & t9,'tcore  =',e25.15/
     & t9,'vcore  =',e25.15/
     & t9,'vecore =',e25.15)
      end
c deck rdstvc
      subroutine rdstvc(  aoints,  info,    buffer,  values,  labels,
     &  nsym,    nbpsy,   mapin,   nnbft,   stvc,    score,   tcore,
     &  vcore,   vecore,  symb,    ierr )
c
c  read the overlap, kinetic energy, potential energy, and
c  core potential arrays.
c
c  on exit, the integral file is positioned after the
c  last 1-e integral record.
c
c  input:
c  aoints = input file unit number.
c  info(*) = info array for this file.
c  buffer(1:l1rec) = record buffer.
c  values(1:n1max) = value buffer.
c  labels(1:2,1:n1max) = orbital label buffer.
c  nsym = number of symmetry blocks.
c  nbpsy(*) = number of basis functions per symmetry block.
c  mapin(*) = input_ao-to-ao mapping vector.
c  nnbft = leading dimension of stvc(*,1:3).
c
c  output:
c  stvc(*) = the overlap s1(*) matrix is returned in stvc(*,1), the
c           total kinetic matrix T1_total(*) is returned in stvc(*,2),
c           the 1-e potential matrix V1_total(*) is returned in
c           stvc(*,3), and any remaining hamiltonian contributions are
c           returned in stvc(*,4).
c           all arrays are returned symmetry-blocked lower-triangle
c           packed by rows.
c           all 1-e contributions on the integral file are summed
c           into this array.  consequently, the entries on the file
c           must be only the distinct array elements.
c  score  = frozen core contribution. tr( s1 * dfc ) = nfrzct
c  tcore  = frozen core contribution. tr( t1 * dfc )
c  vcore  = frozen core contribution. tr( v1 * dfc )
c  vecore = frozen core contribution. tr( h1 * dfc )
c  symb(1:nbft) = symmetry index of each basis function
c  ierr = error return code.
c       =  0 for normal return.
c       = -1 if no arrays were found on the integral file.
c       = -n if n symmetry blocking errors were detected.
c       >  0 for iostat error.
c
c  25-may-91 rdstvc() created from modified sifstv(). -rmp/rls
c  31-oct-90 sifstv() created from sifrsh(). -rls
c  08-oct-90 (columbus day) 1-e fcore change.  sifr1n() interface
c            used. ierr added. -rls
c  04-oct-90 sifskh() call added. -rls
c  26-jul-89 written by ron shepard.
c
      implicit none
      integer  aoints, nsym,   nnbft,  ierr
      integer  info(*),        nbpsy(nsym),    labels(2,*),
     & mapin(*),       symb(*)
      real*8   score,  tcore,  vcore,  vecore
      real*8   buffer(*),      values(*),      stvc(nnbft,4)
c
c     # local...
      integer    itypea,   btypmx
      parameter( itypea=0, btypmx=6 )
c
      integer i,      nntot,  isym,   nrec,   last,   lastb,  lasta
      integer symtot(36), idummy(1), btypes(0:btypmx)
      real*8  fcore(4)
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
      integer nndxf
c
c     # set the btypes(*) array:
c     #            0:s1, 1:t1, 2:v1, 3:vec, 4:vfc, 5:vref, 6:generic_h1
      data btypes/ 1,    2,    3,    4,     4,     4,      4          /
c
      nndxf(i) = (i * (i - 1)) / 2
c
c     # nntot = the actual number of elements in the arrays.
      nntot=0
      do isym=1,nsym
        nntot = nntot + nndxf( nbpsy(isym) + 1 )
      enddo
c
      if ( nntot .gt. nnbft ) then
c       # inconsistent nnbft value.
        call bummer('rdstvc: (nntot-nnbft)=',(nntot-nnbft),wrnerr)
        ierr = -2
        return
      endif
c
c     # initialize the output arrays...
c
      call wzero( nntot, stvc(1,1), 1 )
      call wzero( nntot, stvc(1,2), 1 )
      call wzero( nntot, stvc(1,3), 1 )
      call wzero( nntot, stvc(1,4), 1 )
      fcore(1) = (0)
      fcore(2) = (0)
      fcore(3) = (0)
      fcore(4) = (0)
c
      call sifr1n(  aoints,  info,    itypea,  btypmx,  btypes,
     &     buffer,  values,  labels,  nsym,    nbpsy,   idummy,
     &     mapin,   nnbft,   stvc,    fcore,   symb,    symtot,
     &     lasta,   lastb,   last,    nrec,    ierr )
c
c     # save the appropriate core values.
      score  = fcore(1)
      tcore  = fcore(2)
      vcore  = fcore(3)
      vecore = fcore(4)
c
      return
      end
c deck phk
      subroutine phk( l2rec,        n2max,        symb,
     &  nso,          nsopr,        nblpr,        ipq,
     &  buffer,       values,       labels,       ibitv,
     &  info,         nbufo,        p,            hk,
     &  ilbl,         klbl,         plabs  )
c
      implicit none
      integer    intmax,        ihash
      parameter( intmax=100000, ihash=1023 )
      integer    intmxp,          intmxe
      parameter( intmxp=intmax+1, intmxe=intmxp+ihash)
      integer    nipv
      parameter( nipv=4 )
c
      integer     nlist, ifile, ifile2, nfile, idebug
      common /c1/ nlist, ifile, ifile2, nfile, idebug
c
      integer     ierr, igscnt, nint, ij, kl, indx
      common /c3/ ierr, igscnt, nint, ij, kl, indx
c
      real*8        aj,         ak
      integer lblij,  lblkl,         igs,         inext
      common /pass/ aj(intmax), ak(intmax),
     & lblij(intmax), lblkl(intmax), igs(intmax), inext(intmxe)
c
      integer     nst, ns, isfr, nu, nnbft
      common /c2/ nst, ns, isfr, nu, nnbft
c
c     # dummy:
      integer l2rec, n2max, nbufo
      integer symb(*), nso(*), nsopr(*), nblpr(*), ipq(*),
     & labels(nipv,n2max), ibitv(*), info(*), ilbl(nbufo), klbl(nbufo)
      real*8 buffer(l2rec), values(n2max)
      real*8 p(nbufo), hk(nbufo), plabs(*)
c
c     # local:
      integer nbi, nbft, ierrx, ibfld, pkflag, last, iretbv, itypea,
     & itypeb, ifmt, ibvtyp, nbufi, ibufi, isym, i, jsym, j, ksym, k,
     & lsym, l, itemp, it, jt, kt, lt, nsymof, iint, ibufo
      real*8 val, ajt, akt
c
      integer    msame
      parameter( msame=0, iretbv=1 )
c
      real*8     criter,         a1s4
      parameter( criter=1.0d-15, a1s4=0.25d0 )
c
      integer    more,   nomore
      parameter( more=0, nomore=1 )
c
c     # bummer error types.
      integer    wrnerr,  nfterr,  faterr
      parameter (wrnerr=0,nfterr=1,faterr=2)
c
      integer nndxf
      nndxf(i) = (i * (i - 1)) / 2
c
c     # initialize the symmetry offset arrays.
      nbft  = 0
      nnbft = 0
      do i = 1, nst
        nbi      = nso(i)
        nsopr(i) = nbft
        nblpr(i) = nnbft
        nbft  = nbft + nbi
        nnbft = nnbft + nndxf( nbi + 1 )
      enddo
c
      do i = 1, isfr+1
        ipq(i) = nndxf(i)
      enddo
c
      ierrx  = 0
      nbufi  = 0
      itypea = 0
      itypeb = 0
c
      if ( idebug .ge. 2 ) then
        write (nlist,'(a,a)') 'last i  j  k  l   it jt kt lt',
     &    '     irreps           val'
      endif
c
      ierr   = 0
      ibufo  = 0
      ibfld  = 0
      igscnt = intmxe
      nint   = 0
      last   = msame
c
c     # do while (last=msame)...
  100 if ( last .eq. msame ) then
        call sifrd2(  ifile2,  info,    nipv,    iretbv,  buffer,
     &       nbufi,   last,    itypea,  itypeb,  ibvtyp,
     &       values,  labels,  ibitv,   ierrx )
c
        if ( ierrx .ne. 0 .and. ierrx.ne.-4 ) then
          call bummer('phk: from sifrd2(), ierrx=', ierrx, faterr )
        elseif ( itypea .ne. 3 ) then
          call bummer('phk: from sifrd2(), itypea=', itypea, faterr )
        elseif ( itypeb .ne. 0 ) then
          call bummer('phk: from sifrd2(), itypeb=', itypeb, faterr )
        endif
c
        do 530 ibufi = 1, nbufi
c
          isym   = symb( labels(1,ibufi) )
          i      = labels(1,ibufi) - nsopr(isym)
          jsym   = symb( labels(2,ibufi) )
          j      = labels(2,ibufi) - nsopr(jsym)
          ksym   = symb( labels(3,ibufi) )
          k      = labels(3,ibufi) - nsopr(ksym)
          lsym   = symb( labels(4,ibufi) )
          l      = labels(4,ibufi) - nsopr(lsym)
          pkflag = ibitv(ibufi)
          val    = values(ibufi)
c
c         # place blocks in canonical order
c
          if(isym-jsym) 120,110,130
110       if(i-j) 125,130,130
120       itemp=isym
          isym=jsym
          jsym=itemp
125       itemp=i
          i=j
          j=itemp
c
130       if(ksym-lsym) 150,140,160
140       if(k-l) 155,160,160
150       itemp=ksym
          ksym=lsym
          lsym=itemp
155       itemp=k
          k=l
          l=itemp
c
160       if(isym-ksym) 180,165,200
165       if(i-k) 185,170,200
170       if(jsym-lsym) 190,175,200
175       if(j-l) 195,200,200
180       itemp=isym
          isym=ksym
          ksym=itemp
185       itemp=i
          i=k
          k=itemp
190       itemp=jsym
          jsym=lsym
          lsym=itemp
195       itemp=j
          j=l
          l=itemp
c
200       if ( idebug .ge. 2 ) then
            it=i+nsopr(isym)
            jt=j+nsopr(jsym)
            kt=k+nsopr(ksym)
            lt=l+nsopr(lsym)
            write (nlist,'(i2,3(2x,4i3),1pd25.15)')
     &        pkflag,i,j,k,l,it,jt,kt,lt,isym,jsym,ksym,lsym,val
          endif
c
          if(isym.eq.jsym) then
            if(isym.eq.ksym) then
c-------------------------------------------------------------------
c
c             # all symmetries are the same.
c
c-------------------------------------------------------------------
              nsymof=nblpr(isym)
              ij=nsymof+ipq(i)+j
              if(i.eq.j) then
                if(k.eq.l) then
                  if(i.eq.k) then
c                   # (11:11)
                    kl=ij
                    call hashpk
                    aj(indx)=val
                    ak(indx)=val+val
                  else
c                   # (22:11)
                    kl=nsymof+ipq(k+1)
                    call hashpk
                    aj(indx)=val
                    ij=nsymof+ipq(i)+k
                    kl=ij
                    call hashpk
                    ak(indx)=ak(indx)+val
                  endif
                else
                  kl=nsymof+ipq(k)+l
                  call hashpk
                  aj(indx)=val
                  if(i.eq.k) then
c                   # (22:21)
                    ak(indx)=val+val
                  else
c                   # (33:21)
                    ij=(nsymof+ipq(i))+k
                    kl=(nsymof+ipq(i))+l
                    call hashpk
                    ak(indx)=ak(indx)+val
                  endif
                endif
              elseif(k.eq.l) then
                kl=nsymof+ipq(k+1)
                call hashpk
                aj(indx)=val
                if(j.lt.k) then
c                 # (31:22)
                  ij=nsymof+ipq(i)+k
                  kl=nsymof+ipq(l)+j
                  call hashpk
                  ak(indx)=ak(indx)+val
                elseif(j.eq.k) then
c                 # (21:11)
                  ak(indx)=val+val
                else
c                 # (32:11)
                  ij=(nsymof+k)+ipq(i)
                  kl=(nsymof+k)+ipq(j)
                  call hashpk
                  ak(indx)=ak(indx)+val
                endif
              elseif(i.eq.k) then
                if(j.eq.l) then
c                 # (21:21)
                  kl=ij
                  call hashpk
                  aj(indx)=val
                  ak(indx)=ak(indx)+val
                  ij=nsymof+ipq(i+1)
                  kl=nsymof+ipq(j+1)
                else
c                 # (32:31)
                  kl=nsymof+ipq(k)+l
                  call hashpk
                  aj(indx)=val
                  ak(indx)=ak(indx)+val
                  ij=nsymof+ipq(i+1)
                  kl=nsymof+ipq(j)+l
                endif
                call hashpk
                ak(indx)=val+val
              elseif(j.eq.k) then
c               # (32:21)
                kl=nsymof+ipq(k)+l
                call hashpk
                aj(indx)=val
                ak(indx)=ak(indx)+val
                ij=nsymof+ipq(i)+l
                kl=nsymof+ipq(j+1)
                call hashpk
                ak(indx)=val+val
              elseif(j.eq.l) then
c               # (31:21)
                kl=nsymof+ipq(k)+l
                call hashpk
                aj(indx)=val
                ak(indx)=ak(indx)+val
                ij=nsymof+ipq(i)+k
                kl=nsymof+ipq(j+1)
                call hashpk
                ak(indx)=val+val
              else
                kl=nsymof+ipq(k)+l
                call hashpk
                aj(indx)=val
                ij=(nsymof+ipq(i))+k
                if(j.gt.k) then
c                 # (43:21)
                  kl=(nsymof+ipq(j))+l
                  call hashpk
                  ak(indx)=ak(indx)+val
                  ij=(nsymof+ipq(i))+l
                  kl=(nsymof+ipq(j))+k
                elseif(j.lt.l) then
c                 # (41:32)
                  kl=nsymof+ipq(l)+j
                  call hashpk
                  ak(indx)=ak(indx)+val
                  ij=(nsymof+ipq(i))+l
                  kl=nsymof+ipq(k)+j
                else
c                 # (42:31)
                  kl=nsymof+ipq(j)+l
                  call hashpk
                  ak(indx)=ak(indx)+val
                  ij=(nsymof+ipq(i))+l
                  kl=nsymof+ipq(k)+j
                endif
                call hashpk
                ak(indx)=ak(indx)+val
              endif
            else
c-------------------------------------------------------------------
c
c             # coulomb integral -- two symmetries
c
c-------------------------------------------------------------------
              ij=nblpr(isym)+ipq(i)+j
              kl=nblpr(ksym)+ipq(k)+l
              call hashpk
              aj(indx)=val
            endif
          else
            if(isym.eq.ksym) then
c-------------------------------------------------------------------
c
c             # exchange integral -- two symmetries
c
c-------------------------------------------------------------------
              if(j.lt.l) then
c               # (41:32)
                ij=nblpr(isym)+ipq(i)+k
                kl=nblpr(jsym)+ipq(l)+j
                call hashpk
                ak(indx)=ak(indx)+val
              elseif(j.eq.l) then
c               # (31:21), (21:21)
                ij=nblpr(isym)+ipq(i)+k
                kl=nblpr(jsym)+ipq(j+1)
                call hashpk
                ak(indx)=val+val
              else
                kl=nblpr(jsym)+ipq(j)+l
                if(i.gt.k) then
c                 # (43:21)
                  ij=nblpr(isym)+ipq(i)+k
                  call hashpk
                  ak(indx)=ak(indx)+val
                else
c                 # (32:31)
                  ij=nblpr(isym)+ipq(i+1)
                  call hashpk
                  ak(indx)=val+val
                endif
              endif
            endif
          endif
c-------------------------------------------------------------------
c
c         # process groups of integrals and write out new file.
c
c-------------------------------------------------------------------
          if ( pkflag .eq. 0 .or. nint .eq. 0 ) go to 530
          if(ierr.eq.1) then
            write (nlist,*) 'intmax too small ', nint
            call bummer('phk: intmax too small, nint=',nint,faterr)
          endif
          do 520 iint = 1, nint
            ajt = aj(iint)
            akt = ak(iint)
            if(abs(ajt).lt.criter.and.abs(akt).lt.criter) go to 520
            akt = a1s4 * akt
            if ( ibufo .eq. nbufo ) then
c
c             # buffers are full. pack the labels and
c             # dump to the output file.
c
              call dump2( nfile,  more,   ibufo,  nbufo,  p,
     &            hk,     ilbl,   klbl,   plabs )
c
              ibfld = ibfld + 1
              ibufo=0
            endif
            ibufo       = ibufo + 1
            ilbl(ibufo) = lblij(iint)
            klbl(ibufo) = lblkl(iint)
            p(ibufo)    = ajt - akt
            hk(ibufo)   = akt
  520     continue
          igscnt = nint
          nint = 0
  530   continue
        go to 100
      endif
c
c     # write out the last record.
c
            call dump2( nfile,  nomore, ibufo,  nbufo,  p,
     $          hk,     ilbl,   klbl,   plabs )
c
      ibufo = ibfld * nbufo + ibufo
      ibfld = ibfld + 1
c
      write (nlist,910) ibufo, ibufo, ibfld
c
      return
  910 format('phk:',i12,' p and',i9,' k/2 integrals in',i6,' blocks.')
      end
c deck hashpk
      subroutine hashpk
      implicit integer(a-z)
c
      integer    intmax,        ihash,      ihash1
      parameter (intmax=100000, ihash=1023, ihash1=ihash+1 )
      integer    intmxp,          intmxe,              ntmxe4
      parameter( intmxp=intmax+1, intmxe=intmxp+ihash, ntmxe4=intmxe/4 )
c
      real*8     a0
      parameter( a0=0d0 )
c
      integer     ierr, igscnt, nint, ij, kl, indx
      common /c3/ ierr, igscnt, nint, ij, kl, indx
c
      real*8        aj,         ak
      integer lblij,  lblkl,         igs,         inext
      common /pass/ aj(intmax), ak(intmax),
     & lblij(intmax), lblkl(intmax), igs(intmax), inext(intmxe)
c
c     # local:
      integer j, k
c
      if ( nint .eq. 0 ) then
        if ( igscnt .gt. ntmxe4 ) then
          call izero_wr( intmxe, inext, 1 )
        else
          do j = 1, igscnt
            inext(igs(j)) = 0
          enddo
        endif
      endif
      k = mod( 4 * ij + kl, ihash1 ) + intmxp
30    j = inext(k)
      if ( j .ne. 0 ) then
        if ( lblij(j) .ne. ij .or. lblkl(j) .ne. kl ) then
c         # keep looping to resolve the collision.
          k = j
          go to 30
        else
          indx=j
        endif
      else
        nint = nint + 1
        if ( nint .gt. intmax ) then
c         # workspace is full. return an error code.
          ierr = 1
          indx = intmax
        else
          inext(k)    = nint
          igs(nint)   = k
          aj(nint)    = a0
          ak(nint)    = a0
          lblij(nint) = ij
          lblkl(nint) = kl
          indx        = nint
        endif
      endif
      return
      end
c deck dump1
      subroutine dump1( nfile,  last,   num,    nnbft,
     &          lenv,   values, labij,  plabs )
c
c  pack the (ij) labels into plabs(*) and dump to nfile.
c
c  this routine is for localization of nonportable code.
c
c  input:
c  nfile  = output file unit number.
c  last   = 0/1 code for the last record.
c  num    = number of elements to be packed.
c  nnbft  = length of a totally symmetric 1-e array.  this is also
c           the maximum (ij) label, and is used for consistency
c           verification.
c  lenv   = length of the values(*) array on the output record.
c  values(*) = array values.
c  labij(1:num) = array of unpacked labels.
c  plabs(*) = array of packed labels to be written to the output file.
c
c  the colib routines, plab16() and plab32(), should eventually be
c  called directly, but this requires a coordinated change with
c  several programs. -rls
c
c  25-may-91 written by ron shepard.
c
      implicit none
c     # dummy.
      integer nfile, last, num, nnbft, lenv
      real*8 values(lenv)
      integer labij(num)
c
*@ifdef int64
      integer plabs(lenv)
*@else
*c     # for machines that support 16-bit integer declarations.
*      integer*2 plabs(lenv)
*@endif
c
c     # local:
      integer i
c
      integer    lmax15
      parameter( lmax15=2**15-1 )
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
c     # check space requirements.
c
      if ( num .gt. lenv ) then
        call bummer('dump1: (num-lenv)=', (num-lenv), faterr )
      endif
c
*@ifdef int64
C
C     # (ij) labels are packed 1/integer word.
C     # no extra error checking is necessary.
C
*@else
*c
*c     # (ij) labels are packed 1/integer*2 word.
*c
*      if ( nnbft .gt. lmax15 ) then
*c       # need unsigned integer moves to access all 16 bits.
*        call bummer('dump1: (nnbft-lmax15)=',(nnbft-lmax15),faterr)
*      endif
*@endif
c
      do i = 1, num
        plabs(i) = labij(i)
      enddo
c
      write (nfile) last, num, plabs, values
c
      return
      end
c deck dump2
      subroutine dump2( nfile,  last,   num,    lenv,   psup,
     &  ksup,   labij,  labkl, plabs )
c
c  pack the (ij) and (kl) labels into plabs(*) and dump to nfile.
c
c  this routine is for localization of nonportable code.
c
c  input:
c  nfile  = output file unit number.
c  last   = 0/1 code for the last record.
c  num    = number of elements to be packed.
c  lenv   = length of the values(*) array on the output record.
c  psup(*) = p-supermatrix array values.
c  ksup(*) = k-supermatrix array values. (actually, k/2)
c  labij(1:num) = array of unpacked labels.
c  labkl(1:num) = array of unpacked labels.
c  plabs(*) = array of packed labels to be written to the output file.
c
c  the colib routines, plab16() and plab32(), should eventually be
c  called directly, but this requires a coordinated change with
c  several programs. -rls
c
c  25-may-91 written by ron shepard.
c
      implicit none
c     # dummy.
      integer nfile, last, num, lenv
      real*8 psup(lenv), ksup(lenv)
      integer labij(num), labkl(num)
c
*@ifdef int64
      integer plabs(lenv)
*@else
*c     # for machines that support 16-bit integer declarations.
*      integer*2 plabs(lenv,2)
*@endif
c
c     # local:
      integer i
c
c     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
c
c     # check space requirements.
c
      if ( num .gt. lenv ) then
        call bummer('dump2: (num-lenv)=', (num-lenv), faterr )
      endif
c
      do i = 1, num
*@ifdef int64
*@ifdef cray
*        plabs(i) = or( shiftl( labij(i), 32 ), labkl(i) )
*@else
         plabs(i) = ior( ishft(labij(i),32),labkl(i))
*@endif
*@else
*        plabs(i,1) = labij(i)
*        plabs(i,2) = labkl(i)
*@endif
      enddo
c
      write (nfile) last, num, plabs, psup, ksup
c
      return
      end
