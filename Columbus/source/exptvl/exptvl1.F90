!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
!exptvl1.f
!exptvl part=1 of 2.  workspace allocation routine
! deck exptvl
      program exptvl
       use exptvl_data
       implicit none
      include "../colib/getlcl.h" 
!  allocate space and call the driver routine.
!
!  ifirst = first usable space in the the real*8 work array a(*).
!  lcore  = length of workspace array in working precision.
!  mem1   = additional machine-dependent memory allocation variable.
!  a(*)   = workspace array. a(ifirst : ifirst+lcore-1) is useable.
!
      integer ifirst, lcore
!
!     # mem1 and a(*) should be declared below within mdc blocks.
!
!     # local...
!     # lcored = default value for lcore.
!     # ierr   = error return code.
!
      integer lcored, ierr
!
!     # bummer error types.
      integer   wrnerr,  nfterr,  faterr
      parameter(wrnerr=0,nfterr=1,faterr=2)
!
!
!
!     # use standard f90 memory allocation.
!
      integer mem1
      parameter ( lcored = 1000000 )
      real*8, allocatable :: a(:)
!
      call getlcl( lcored, lcore, ierr )
      if ( ierr .ne. 0 ) then
         call bummer('exptvl: from getlcl: ierr=',ierr,faterr)
      endif
!
      allocate( a(lcore),stat=ierr )
      if ( ierr .ne. 0 ) then
         call bummer('exptvl: allocate failed ierr=',ierr,faterr)
      endif

      ifirst = 1
      mem1   = 0
!
!     # lcore, mem1, and ifirst should now be defined.
!     # the values of lcore, mem1, and ifirst are passed to
!     # driver() to be printed.
!     # since these arguments are passed by expression, they
!     # must not be modified within driver().
!
      call driver( a(ifirst), (lcore), (mem1), (ifirst) )
!
!     # return from driver() implies successful execution.
!
      call bummer('normal termination',0,3)
      stop 'end of exptvl'
      end
