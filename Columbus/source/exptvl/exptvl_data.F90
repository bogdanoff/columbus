!*******************************************************************************
! This file is part of the COLUMBUS Program System.
! Copyright (C) 1980-2023, the COLUMBUS authors.
! For more information see https://gitlab.com/columbus-program-system/columbus
! COLUMBUS is free software; you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License, version 3.0.
! COLUMBUS is distributed in the hope that it will be useful, but it is
! provided "as is" and without any express or implied warranties.
! For more details see the full text of the license in the file LICENSE.
!******************************************************************************/
module exptvl_data
  TYPE, PUBLIC :: stdfilestyp
  INTEGER :: nlist,aoints,nin,mofile,nofile
  END TYPE stdfilestyp
  TYPE (stdfilestyp) :: stdfiles

  data stdfiles%nlist /6/
  data stdfiles%aoints /4/
  data stdfiles%nin /5/
  data stdfiles%mofile /20/
  data stdfiles%nofile /21/
  integer :: btypmx
  parameter(btypmx=34)


  TYPE, PUBLIC :: nucltyp
  REAL*8 nucdip(btypmx)
  END TYPE nucltyp
  TYPE (nucltyp) :: nucl


   TYPE, PUBLIC :: inputtyp
      integer lvlprt, moment, iquad,seward,dalton2,locrange(3)
      integer pipekmezeyloc,quadrup,pmmos
      real*8 xpoint(3),lthresh(2)
      real*8 mo_occ_threshold,mo_default_radius
      real*8 mo_radius_multiplier,cylinder_radius,maxoverlap
      character*50 mofilen,nofilen
   END TYPE inputtyp
   TYPE (inputtyp) :: inputdata


end module exptvl_data
