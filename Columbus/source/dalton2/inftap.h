C     Most file variables used in Dalton
C     ("TAP" for tapes here for historical reasons)
      INTEGER         LUCME, LUMOL, LUPROP, LUSOL, LUINTA,
     &                LUONEL, LUSUPM, LUTLM, LUDA1, LUITMP,
     &                LU2DER, LUDASP, LURDR, LURDI, LUGDR, LUGDI,
     &                LUGDT, LURDT, LUDFCK, LUSFDA, LUFDC, LUWLK,
     &                LUPAO, LUPAS, LUNR1, LUNR3, LUNR5,
     &                LUINTR, LUMOLDEN, LUR12, LUORDA, LUMINT
      INTEGER         LUAHSO, LUCRV1, LUCRV2, LUXYVE, LUCRVE,
     &                LURSP3, LURSP4, LURSP5, LUMHSO, LURSP
      INTEGER         LUINTM, LUIT1, LUIT2, LUIT3, LUIT5, LUINF,
     &                LUH2AC, LUSIFC, LBINTM, LBINTD, LBONEL, LBINFO
      CHARACTER ABATLM*10, FNSOL*8, ABARDR*9, ABARDI*10, ABAGDR*9,
     &          ABAGDI*10, ABAGDT*10, ABARDT*10, ABADFK*10, ABASF*9,
     &          ABAWLK*10, ABATRJ*10, ABAIRC*10, ABANR1*10, ABANR3*10, 
     &          ABANR5*10, FNONEL*8, FNINTM*8, FNSUPM*8, FNSIFC*6,
     &          LBSIFC*8
      COMMON /INFTAP/ LUCME, LUMOL, LUPROP, LUSOL, LUINTA,
     &                LUONEL, LUSUPM, LUTLM, LUDA1, LUITMP,
     &                LU2DER, LUDASP, LURDR, LURDI, LUGDR, LUGDI,
     &                LUGDT, LURDT, LUDFCK, LUSFDA, LUFDC, LUWLK,
     &                LUPAO, LUPAS, LUNR1, LUNR3, LUNR5,
     &                LUINTR, LUMOLDEN, LUR12(20), LUORDA, LUMINT
      COMMON /RSPTAP/ LUAHSO, LUCRV1, LUCRV2, LUXYVE, LUCRVE,
     &                LURSP3, LURSP4, LURSP5, LUMHSO, LURSP
      COMMON /SIRTAP/ LUINTM, LUIT1, LUIT2, LUIT3, LUIT5, LUINF,
     &                LUH2AC, LUSIFC, LBINTM, LBINTD, LBONEL, LBINFO,
     &                LBSIFC
      COMMON /CHRTAP/ ABATLM, FNSOL, ABARDR, ABARDI, ABAGDR, ABAGDI,
     &                ABAGDT, ABARDT, ABADFK, ABASF, ABAWLK, ABATRJ,
     &                ABAIRC, ABANR1, ABANR3, ABANR5, FNINTM, FNONEL,
     &                FNSUPM, FNSIFC
