#!/bin/csh
#*******************************************************************************
# This file is part of the COLUMBUS Program System.
# Copyright (C) 1980-2023, the COLUMBUS authors.
# For more information see https://gitlab.com/columbus-program-system/columbus
# COLUMBUS is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License, version 3.0.
# COLUMBUS is distributed in the hope that it will be useful, but it is
# provided "as is" and without any express or implied warranties.
# For more details see the full text of the license in the file LICENSE.
#******************************************************************************/

set TEILA = ( ARGOS CIDRTMS1 CIDRTMS2 COLIB DALTON1 DALTON2 TRAN )
set TEILB = ( TRAN_D )
set SINGLE = ( N2-multsegs N2-multsegs-ftcalc ethylen-mcscf-stateav ethylen-mrci-arg ethylen-mrci-dalt ethylen-mrci-dalt-ftcalc h2o-mraqcc h2o-mrci h2o-sraqcc h2o-srci-arg h2o-srci-dalt transci transmom )
set GEOM = ( butadiene-mrci ethylene-mrci h2o-sraqcc h2o-srci o3-mraqcc o3-mrci o3-mrci-ex ) 
set DIRECT = ( h2o-mraqcc h2o-mrci n2-pvdz n2-pvqzm n2-pvtz ) 

set SCIUDG = ( hcho-mraqcc1 hcho-mraqcc2 hcho-mraqcc3 hcho-mraqcc4 hcho-mrsdci )
set PCIUDG = ( hcho-mraqcc1 hcho-mraqcc2 hcho-mrci ) 

setenv testdir `pwd`

# TEILA

  cd $testdir/$TEILA[1] 
  cp argosls $COLUMBUS/EXAMPLES/$TEILA[1]/argosls.reference
  cp istatls $COLUMBUS/EXAMPLES/$TEILA[1]/istatls.reference
    
  cd $testdir/$TEILA[5] 
  cp daltonls $COLUMBUS/EXAMPLES/$TEILA[5]/daltonls.reference
  cp istatls $COLUMBUS/EXAMPLES/$TEILA[5]/istatls.reference

  cd $testdir/$TEILA[6] 
  cp daltonls $COLUMBUS/EXAMPLES/$TEILA[6]/daltonls.reference
  cp istatls $COLUMBUS/EXAMPLES/$TEILA[6]/istatls.reference

  cd $testdir/$TEILA[7] 
  cp istatls.mo $COLUMBUS/EXAMPLES/$TEILA[6]/istatls.mo.reference

  cd $testdir/$TEILA[2]
  foreach i ( 1 2 3 4 )
   cp cidrtfl.$i $COLUMBUS/EXAMPLES/$TEILA[2]/cidrtfl.$i.reference
  end
   cp cidrtmsls $COLUMBUS/EXAMPLES/$TEILA[2]/cidrtmsls.reference

  cd $testdir/$TEILA[3]
  foreach i ( 1 2 3 4 )
   cp cidrtfl.$i $COLUMBUS/EXAMPLES/$TEILA[3]/cidrtfl.$i.reference
  end
   cp cidrtmsls $COLUMBUS/EXAMPLES/$TEILA[3]/cidrtmsls.reference


# single point

  
  foreach i ( 1 2 3 4 5 6 7 8 9 10 11 12 13 )
  cd $testdir/single-point-calculations/$SINGLE[$i]

  if ( ! -d $COLUMBUS/EXAMPLES/single-point-calculations/$SINGLE[$i]/LISTINGS ) then
  mkdir $COLUMBUS/EXAMPLES/single-point-calculations/$SINGLE[$i]/LISTINGS
  endif

  cp logfile.$SINGLE[$i] $COLUMBUS/EXAMPLES/single-point-calculations/$SINGLE[$i]/LISTINGS/logfile
  cp LISTINGS/* $COLUMBUS/EXAMPLES/single-point-calculations/$SINGLE[$i]/LISTINGS/
  end

# geom opt     

  
  foreach i ( 1 2 3 4 5 6 7 )
  cd $testdir/geometry-optimizations/$GEOM[$i]
  if ( ! -d $COLUMBUS/EXAMPLES/geometry-optimizations/$GEOM[$i]/LISTINGS ) then
  mkdir $COLUMBUS/EXAMPLES/geometry-optimizations/$GEOM[$i]/LISTINGS
  endif
  cp logfile.$GEOM[$i] $COLUMBUS/EXAMPLES/geometry-optimizations/$GEOM[$i]/LISTINGS/logfile
  cp LISTINGS/* $COLUMBUS/EXAMPLES/geometry-optimizations/$GEOM[$i]/LISTINGS/
  end

# direct


  foreach i ( 1 2 3 4 5  )
  cd $testdir/direct/$DIRECT[$i]
  if ( ! -d $COLUMBUS/turbocol.5.5/EXAMPLES/$DIRECT[$i]/LISTINGS ) then
  mkdir $COLUMBUS/turbocol.5.5/EXAMPLES/$DIRECT[$i]/LISTINGS
  endif
  cp logfile.$DIRECT[$i] $COLUMBUS/turbocol.5.5/EXAMPLES/$DIRECT[$i]/LISTINGS/logfile
  cp LISTINGS/* $COLUMBUS/turbocol.5.5/EXAMPLES/$DIRECT[$i]/LISTINGS/
  end


#sciudg

  foreach i ( 1 2 3 4 5  )
  cd $testdir/sciudg/$SCIUDG[$i]
  if ( ! -d $COLUMBUS/parallel.5.5/EXAMPLES/sciudg/$SCIUDG[$i]/LISTINGS ) then
  mkdir $COLUMBUS/parallel.5.5/EXAMPLES/sciudg/$SCIUDG[$i]/LISTINGS
  endif
  cp logfile.$SCIUDG[$i] $COLUMBUS/parallel.5.5/EXAMPLES/sciudg/$SCIUDG[$i]/LISTINGS/logfile 
  cp LISTINGS/* $COLUMBUS/parallel.5.5/EXAMPLES/sciudg/$SCIUDG[$i]/LISTINGS
  end



#pciudg

  foreach i ( 1 2 3   )
  cd $testdir/pciudg/$PCIUDG[$i]
  if ( ! -d $COLUMBUS/parallel.5.5/EXAMPLES/pciudg/$PCIUDG[$i]/LISTINGS ) then
  mkdir $COLUMBUS/parallel.5.5/EXAMPLES/pciudg/$PCIUDG[$i]/LISTINGS
  endif
  cp logfile.$PCIUDG[$i] $COLUMBUS/parallel.5.5/EXAMPLES/pciudg/$PCIUDG[$i]/LISTINGS/logfile 
  cp LISTINGS/* $COLUMBUS/parallel.5.5/EXAMPLES/pciudg/$PCIUDG[$i]/LISTINGS
  end


