/*
    Copyright (C) 2008 Jiri Pittner <jiri.pittner@jh-inst.cas.cz> or <jiri@pittnerovi.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "gelfand.h"
#include "lbinom.h"

#include <cstdlib>
#include <cmath>

//implementation of class gelfpat methods

istream& operator>>(istream  &s, gelfpat &g)
{
int i,n,t,nn;
s>>n; 
if(n==0) laerror("illegal dimension of gelfand pattern");
g.resize(abs(n));

if(n<0) {nn= -n;}
else    { nn=n*(n+1)/2;} /*negative - flag to read only 1st row*/
for(i=0; i<nn; i++)
        {
	s>>t;
        g[i]= (occnum)t;
        }
g.check(n<0);
return s;
}


ostream& operator<<(ostream &s, const gelfpat &g)
{
int i,j,n;
n=g.nrows();
s << n <<'\n';
for(i=n;i>0;i--)
        {
        for(j=1;j<=n-i;j++) s << ' ';
        for(j=1;j<=i;j++) s << (int)g(j,i)  << ' ';
	s << '\n';
        }
return s;
}

ostream& operator<<(ostream &s, const gelfweight &x)
        {
       int i,n;
       n=x.size();
      s << n <<'\n';
      for(i=1;i<=n;i++) s << (int)x[i] << (i==n ? '\n' : ' '); //endl cannot be used in the conditional expression, since it is a function
       return s;
       }




void gelfpat::check(bool onlyfirstline) const /*not written efficiently*/
{
register int i,j;
if(onlyfirstline)
        {
        for(i=0;i<nn-1;i++)
                if(v[i] < v[i+1]) laerror("betweenes condition not fulfilled");
        }
else
        {
        for(j=nn; j>1;j--)
                {
                for(i=1; i<j; i++)
                        if((*this)(i,j)<(*this)(i,j-1) || (*this)(i,j-1) < (*this)(i+1,j))
                                laerror("betweenes condition not fulfilled");
                }
        }
}

int gelfpat::find(const gelfpat *kde,int hm) const /*returns -1 or index of found gelfpat, array kde must be in lexicographic order*/                 
{
register int dm,sm;
register int q;

dm = -1; hm++;
do
  {
  sm = (dm+hm)/2;
  if (!(q = ((*this)!= kde[sm]))) return(sm);
  else if (q<0) dm = sm; else hm = sm;
  }
while (hm > dm+1);
return(-1);
}


/*calculate weights of gelfand pattern (eigenvalues of diagonal generators)*/
void gelfpat::weight(gelfweight &w) const
{
if(w.size()!=nn) laerror("incompatible dimensions in gelfpat::weight");
register int sh,sl;

sl=0;
for(int i=1; i<=nn; ++i)
        {
        sh=0; for(int j=1;j<=i;j++) sh+= (*this)(j,i);
        w[i] = sh-sl;
	sl=sh;
        }
}



/*hammermesh eq 10-25, highest weight == generalized partition of r into n parts*/
int UNirrepdim(const Partition x)
{
double prod;
int i,j,n;
Partition p(n=x.size());
for(i=1;i<=n;i++) p[i-1]= x[i-1]+n-i;

prod=1.;
for(j=p.size();j>=2;j--)
        {
        for(i=1;i<j;i++) prod*= (p[i-1]-p[j-1]);
        prod /= ifact(j-1);
        }

return((int)floor(prod+0.2));
}

const Partition Partition::adjoint() const
{
Partition q(0,nn);
  int i,j;

  for (i=0;i<nn;i++) {
    for(j=0; j<nn&&v[j]>i; ++j);
    q[i]=j;
  }
return q;
}

//matsen-pauncz hook length formula
int SRirrepdim(const Partition p)
{
  int r_tabenum;
  int i,j;
  double prod;
  Partition pp;
  if((i=p.sum())<=p.size()) pp=p;
  else
	{
	pp.resize(i);
	for(j=0;j<p.size();++j) pp[j]=p[j];
	for(;j<i;++j) pp[j]=0;
	}
  Partition q=pp.adjoint();

  prod=1.0;
  for (i=1;i<=q[0];i++)
    for (j=1;j<=p[i-1];j++) prod*=((double)p[i-1]-j+q[j-1]-i+1);
  r_tabenum=(int) floor(ifact(p.sum())/prod+.2);
  return r_tabenum;
}


//calculations of elementary and non-elementary generator matrix elements
void calcelemgen(Ugenerators &E,int i, NRVec<gelfpat> &gelfbasis,int ket,gelfpat &gwork)
/*gwork is a current working pattern - copy of basis[ket], to avoid local malloc call, must be modified and at the end reset to its original shape */
{
/*Use the fact that basis is being generated in lexicographical order
and hence the possible bra numbers are < ket number and they have been
generated already. Possible bra means here having nonzero matrix element.
Also we search in a lexicographically sorted array of gelfand patterns
and hence we can search by bisection or even perhaps better. Bisection
is implemented here.
Cf. equation 13.50 in Spin eigenfunctions by  Pauncz for more details.*/

int bra;
register int j;
register occnum *ptr;

if(i>=gwork.nrows()) laerror("i in calcelemgen must be smaller than n");


/*increment all elements in i-th line*/
for(j=1;j<=i;j++)
	{
	ptr= &gwork(j,i);
	/*if result is legal gelfpat calc. matrix element*/
	if(*ptr < gwork(j,i+1) && (i==1 || j==1 || *ptr < gwork(j-1,i-1)) )
		{
		register int k,d,dprime,ii;
		register double prod,denom;
		register occnum *Gbase;

		(*ptr)++;
		bra=gwork.find(gelfbasis,ket-1); /*ket-1: bra must be different from ket*/		
		(*ptr)--; /*put working gelfpat into original shape*/
		if(bra<0) laerror("algorithm error - bra not yet in basis");

		/*calculate matrix element wrto this bra vector*/
		/*Gelem avoided and pointers used instead for more efficiency*/
		denom=prod=1.0;
		d= *ptr +i-j;
		dprime=d+1;
		ii=i+1;
		Gbase= &gwork(1,ii)-1;
		for(k=1; k<=ii; k++) prod*= (Gbase[k]+ii-k-dprime);
		if(i>1)
			{
			ii=i-1;
                	Gbase= &gwork(1,ii)-1;
			for(k=1; k<=ii; k++) prod*= (Gbase[k]+ii-k-d);
			}
		Gbase= &gwork(1,i)-1;
		for(k=1; k<=i; k++) if(k!=j) {register int p; p=Gbase[k]+i-k; denom*= ((p-d)*(p-dprime)); }
		prod=sqrt(fabs(prod/denom));

		/*save the matrix element*/
		if(prod) E(i,i+1).add(bra,ket,(REAL)prod);
		}
	}
}



//calculate the matrix elements of remaining generators (1-particle CI coupling coefficients)
void calcothergen(Ugenerators &E,const int n)
{
//calculate other raising generators from commutation rules 13.47 in Spin eigenf.
//diagonal and elementary raising ones must have been done already
int i,j;
for(i=1; i<=n-2;i++)
    for(j=i+1; j<n; j++)
	E(i,j+1)= commutator(E(i,j),E(j,j+1)); /*recurrent formula, order in j loop crucial*/
}


//calculate 2-particle CI coupling coefficients
//NOTE that this assumes the full CI - that we have matrix elements of E's in the full basis
//since this we perform essentially resolution of identity in the pair of E's
SparseMat<REAL> twobodyop(const Ugenerators &E, const int r,const int s,const int t,const int u,const bool onlyhalf,const bool normal)
{
int idim=E(1,1).nrows();
SparseMat<REAL> m(idim,idim);

if(s==t && normal) 
	{
	if(onlyhalf)
		{
		if(r<=u) m-=E(r,u);
		}
	else m.axpy(-1.,E(r,u),E.transp(r,u));
	}
if(onlyhalf) m.setsymmetric();//let gemm calculate only a half
m.gemm(1.,E(r,s),E.transp(r,s)?'t':'n',E(t,u),E.transp(t,u)?'t':'n',1.);
if(onlyhalf) m.defineunsymmetric();

return m;
}

//@@@better use non-symmetrized version and contract it with equivalence-weighted grand-list of integrals 
//and a symmetrized version thereof which can be contracted with a non-redundant list of 2-electron integrals
//this routine is a major bottleneck in the calculation, is rather naive now
SparseMat<REAL> symtwobodyop(const Ugenerators &E, const int r,const int s,const int t,const int u,const bool onlyhalf,const bool normal)
{
SparseMat<REAL> m;

//@@@@@@@here can be saved also, something like if(r==s?.5:1)*if(t==u?.5:1)(Ers+Esr)*(Etu+Eut), symetrizovane E predpocitat, one-body parts treat extra
m=twobodyop(E,r,s,t,u,onlyhalf,normal);
if(r!=s) m+=twobodyop(E,s,r,t,u,onlyhalf,normal);
if(t!=u) m+=twobodyop(E,r,s,u,t,onlyhalf,normal);
if(r!=s && t!=u) m+=twobodyop(E,s,r,u,t,onlyhalf,normal);

if(!(r==t && s==u))
    	{
	if(r!=t&&r!=u&&s!=t&&s!=u) 
		{
		//in this case the two E's commute 
		m*=2.;
		}
	else
   		{
		m+=twobodyop(E,t,u,r,s,onlyhalf,normal);
       		if(r!=s) m+=twobodyop(E,t,u,s,r,onlyhalf,normal);
       		if(t!=u) m+=twobodyop(E,u,t,r,s,onlyhalf,normal);
       		if(r!=s && t!=u) m+=twobodyop(E,u,t,s,r,onlyhalf,normal);
		}
	}
if(onlyhalf) m.setsymmetric();
m.simplify();
return m;
}


SparseMat<REAL> antisymtwobodyop(const Ugenerators &E, const int p, const int q, const int r,const int s, const bool onlyhalf,const bool normal)
{
SparseMat<REAL> m;
m=twobodyop(E,p,q,r,s,onlyhalf,normal);
m-=twobodyop(E,s,r,q,p,onlyhalf,normal);
m.simplify();
return m;
}

SparseMat<REAL> antisymonebodyop(const Ugenerators &E, const int p, const int q, const bool onlyhalf)
{
int idim=E(p,q).nrows();
SparseMat<REAL> m(idim,idim);
if(onlyhalf)
	{
	if(E.transp(p,q)) {m -=E(p,q);}
	else	{m=E(p,q);}
	}
else
	{
	if(E.transp(p,q)) {m=E(p,q).transpose() - E(p,q);}
	else {m=E(p,q)-E(p,q).transpose();}
	}
return m;
}

//generate all antisymmetrized one-body operators
//for simplicity, store it as fourindex with latter two indices=0
fourindex<index4type, SparseMat<REAL> > allantisymonebodyop(const Ugenerators &E, const bool onlyhalf)
{
int n=E.nrows();
fourindex<index4type, SparseMat<REAL> > result(n);
for(int i=1; i<=n; ++i)
    for(int j=i+1; j<=n; ++j)
	result.add(i,j,0,0,antisymonebodyop(E,i,j,onlyhalf));
return result;
}

//generate all antisymmetrized two-body operators
fourindex<index4type, SparseMat<REAL> > allantisymtwobodyop(const Ugenerators &E, const bool onlyhalf, const bool normal)
{
int n=E.nrows();
fourindex<index4type, SparseMat<REAL> > result(n);
result.setsymmetry(T2ijab_aces);
for(int i=1; i<=n; ++i)
    for(int j=i+1; j<=n; ++j)
	{
	result.add(i,j,i,j,antisymtwobodyop(E,i,j,i,j,onlyhalf,normal));
	result.add(i,j,j,j,antisymtwobodyop(E,i,j,j,j,onlyhalf,normal));
	result.add(j,i,i,i,antisymtwobodyop(E,j,i,i,i,onlyhalf,normal));
	for(int k=1; k<=n; ++k)	
		if(k!=i && k!=j)
			{
			result.add(i,k,j,k,antisymtwobodyop(E,i,k,j,k,onlyhalf,normal));
			result.add(i,j,k,k,antisymtwobodyop(E,i,j,k,k,onlyhalf,normal));
			result.add(i,k,k,j,antisymtwobodyop(E,i,k,k,j,onlyhalf,normal));
			}
	for(int k=j+1; k<=n;++k)
		for(int l=k+1; l<=n;++l)
			{
			result.add(i,k,j,l,antisymtwobodyop(E,i,k,j,l,onlyhalf,normal));
			result.add(i,l,j,k,antisymtwobodyop(E,i,l,j,k,onlyhalf,normal));
			result.add(i,j,k,l,antisymtwobodyop(E,i,j,k,l,onlyhalf,normal));
			result.add(i,l,k,j,antisymtwobodyop(E,i,l,k,j,onlyhalf,normal));
			result.add(i,k,l,j,antisymtwobodyop(E,i,k,l,j,onlyhalf,normal));
			result.add(i,j,l,k,antisymtwobodyop(E,i,j,l,k,onlyhalf,normal));
			}
	}

return result;
}

////////////////////////////////////////////////////////////////////////////////
//recursive generation of gelfand basis
static int gelf_count;
static gelfpat gelfwork;
static gelfweight weightwork;
NRVec<gelfpat> *gelfbasiswork;
Ugenerators *Ework;


void gelfpat_process(gelfpat &g) /*execute for each generated pattern*/
{

if(*gelfbasiswork) (*gelfbasiswork)[gelf_count] |= g; /*indexed from 0*/

// g.check(); //debug

//calculate weight
g.weight(weightwork);

/*add contributions to diagonal generators; their matrices indexed from 1*/
        for(int i=1; i<=g.nrows(); i++) if(weightwork[i]) (*Ework)(i,i).add(gelf_count,gelf_count,weightwork[i]);
        /*calculate matrices of elementar raising generators E_i,i+1*/
        for(int i=1; i< g.nrows(); i++) calcelemgen(*Ework,i,*gelfbasiswork,gelf_count,g);

gelf_count++;
}



/* generates gelfand patterns in lexicographical order */
void gelfgen(int i,int j)
{
occnum *var,h,l;
var= &gelfwork(i,j);
h= gelfwork(i,j+1);
l= gelfwork(i+1,j+1);
for(*var = h; *var >= l; (*var)--)
        {
        if(i<j) gelfgen(i+1,j);
        else if(j>1) gelfgen(1,j-1);
        else gelfpat_process(gelfwork);
        }
}


//generates the gelfand basis and calculates matrix elements of U generators
int gelfpat_gener(const gelfpat &g, NRVec<gelfpat> *gelfbasis, Ugenerators *E)
{
gelfwork |= g;
gelf_count=0;
weightwork.resize(g.nrows());
int n=g.nrows();
int idim=UNirrepdim(g);
gelfbasiswork=gelfbasis;
Ework=E;
(*gelfbasis).resize(idim);
(*E).resize(n); for(int i=0; i<n*(n+1)/2; i++) (*E)[i].resize(idim,idim);
if(g.nrows()==1) gelfpat_process(gelfwork); else gelfgen(1,g.nrows()-1);
calcothergen((*E),g.nrows());
return gelf_count;
}




// for generation of partitions
static int partitioncount;
static int partitiontyp;
static int partitiontypabs;
static Partition partit;
static void (*partitionprocess) (int,Partition &);


void partgen(int remain,int pos)
{
int hi,lo;
if(remain==0) {++partitioncount; partitionprocess(pos,partit); return;}
if(partitiontyp) lo=(remain+partitiontypabs-pos-1)/(partitiontypabs-pos); else lo=1;
hi=remain;
if(partitiontyp>0) hi -= partitiontypabs-pos-1;
if(pos>0 && partit[pos-1] < hi) hi= partit[pos-1];
for(partit[pos]=hi; partit[pos]>=lo; --partit[pos]) partgen(remain-partit[pos],pos+1);
partit[pos]=0;
}

int partgener(int size,int n,int typ,void (*process) (int,Partition &))
{
if(size<0||n<0) laerror("invalid n for partgener");
if(n==0) return 0;
if(typ>0 && n<typ) return 0;
partitioncount=0;
partitiontyp=typ;
partitiontypabs=typ>=0?typ:-typ;
partitionprocess=process;
if(typ<0 && -typ>n) typ=-n;
if(size<( typ==0?n:(typ>0?typ:-typ))) laerror("partgener: insufficient size");
partit.resize(size);
for(int i=0; i<size;++i) partit[i]=0;
partgen(n,0);
return partitioncount;
}

int ipow(int x, int i)
{
int y=1;
if(x==0)
        {
        if(i>0) return(0);
        laerror("zero base and nonpositive exponent of a power");
        }
if(i) {
        if(i<0) laerror("negative exponent in ipow");
        do
                {
                if(i&1) y *= x;
                x *= x;
                }
        while((i >>= 1)/*!=0*/);
        }
return(y);
}




