/*
    Copyright (C) 2008 Jiri Pittner <jiri.pittner@jh-inst.cas.cz> or <jiri@pittnerovi.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "drt.h"
#include <ext/hash_map>
#include <unistd.h>
#include "bitvector.h"
#include "qsort.h"
#include "bisection.h"
#include "permutation.h"
#include "lbinom.h"

using namespace __gnu_cxx; //needed for the gnu extensions to stl

#include "options.h"

Partition::Partition(const abcrow &abc) : NRVec<int>(0,abc.a+abc.b+abc.c)
{int i= -1; 
for(int j=0; j<abc.a; j++) v[++i]=2;  
for(int j=0; j<abc.b; j++) v[++i]=1;
}


//generate paldus table from gelfand one
paldustab::paldustab(const gelfpat &g) :  NRVec<abcrow>(g.orbnum())
{
for(int i=g.orbnum(); i>0; --i)
	{
	if(g(1,i)>2) laerror("Gelfand pattern not representable as Paldus table");
	int j=1;
	v[i-1].a=0; while(j<=i) if(g(j++,i)==2) ++v[i-1].a; else {j--; break;}
	v[i-1].b=0; while(j<=i) if(g(j++,i)==1) ++v[i-1].b; else {j--; break;}
	v[i-1].c= i-v[i-1].b-v[i-1].a;
	v[i-1].error=0;
	}
}

//I/O of paldus tables

istream& operator>>(istream  &s, abcrow &x)
{
int a,b,c;

        cin >>a >>b >>c;
        x.a=a; x.b=b; x.c=c;
return s;
}


istream& operator>>(istream  &s, paldustab &x)
{
int n;
cin>>n;
x.resize(n);
for(int i=x.size(); i>0; --i)
        {
        cin >>x[i];
	if(x[i].a+x[i].b+x[i].c!=i) laerror("inconsistency in paldus table input");
        }
return s;
}

ostream& operator<<(ostream &s, const abcrow &x)
{
s <<(int) x.a <<" "<<(int)x.b <<" "<<(int)x.c;
return s;
}

ostream& operator<<(ostream &s, const paldustab &x)
{
for(int i=x.size(); i>0; --i) s <<x[i] <<"\n";
return s;
}


void paldustab::weight(gelfweight &w) const
{
if(w.size()!=nn) laerror("incompatible dimensions in gelfpat::weight");
register int sh,sl;

sl=0;
for(int i=0; i<nn; ++i)
        {
        sh=v[i].a*2+v[i].b;
        w[i+1] = sh-sl;
        sl=sh;
        }
}


//DRT  stuff

ABCdelta abcdelta[4]={{0,0,1},{0,1,0},{1,-1,1},{1,0,0}};

const abcrow abcrow::operator+(const ABCdelta &d) const
                {
                abcrow r;
                if(d.b+b>=0)
                        {
                        r.a=a+d.a;
                        r.b=b+d.b;
                        r.c=c+d.c;
                        r.error=0;
                        }
                else
                        r.error=1;
                return r;
                }


const abcrow abcrow::operator-(const ABCdelta &d) const
                {
                abcrow r;
                int t;
                r.error=0;
                t=a-d.a; if(t<0) r.error=1; r.a=t;
                t=b-d.b; if(t<0) r.error=1; r.b=t;
                t=c-d.c; if(t<0) r.error=1; r.c=t;
                return r;
                };

const ABCdelta abcrow::operator-(const abcrow &rhs) const
{
ABCdelta r;
r.a=a;
r.b=b;
r.c=c;
r.a-=rhs.a;
r.b-=rhs.b;
r.c-=rhs.c;
return r;
}


//linear search, bisection would be possible if needed

drtentry *findabc(const abcrow &abc, drtentry *list)
{
drtentry *p;
p=list;
while(p)
	{
	if(abc == p->abc) return p;
	p= p->next;
	}
return p;
}


//insert into the list on a lexicographic position
drtentry *insertabc(const abcrow &abc, drtentry **list)
{
drtentry *n,*p,*last;
last=p= *list;
while(p)
        {
        if(abc == p->abc) return p;
	if(abc > p->abc) break;
	last=p;
        p= p->next;
        }
n=new drtentry(abc);
if(*list == NULL) *list=n;
else //insert into the list
	{
	last->next=n;
	n->next=p; //might be NULL
	}
return n;
}


drt::drt(const abcrow &toprow) //generate whole DRT for given top row
{
norb=toprow.a+toprow.b+toprow.c;
list = new drtentry* [norb+1];

//generate all distinct rows in lexicographic order

list[norb] = new drtentry(toprow);
for(int i=norb-1; i>=0; --i)
	{
	list[i]=NULL;
	drtentry *source=list[i+1];
	while(source) //for all entries of previous row
		{
		for(int step=0; step<4; ++step) //try all step vectors
			{
			abcrow tmp= source->abc - abcdelta[step];
			if(!tmp.error)
				{
				drtentry *currpos;
				currpos=insertabc(tmp,list+i);
				currpos->upchain[step]=source;
				source->downchain[step]=currpos;
				}
			}
		source = source->next;
		}
	}
//check that at level 0 there is single zero entry
if(list[0]->next || list[0]->abc.a || list[0]->abc.b ||list[0]->abc.c )
	laerror("inconsistency in drt generation");

//for consistency it is needed to have also correct lexx at the bottom
list[0]->lexx=1;

//setup vertex and edge weighting for calculation of lexical indices
int totentries=0;
for(int i=1; i<=norb; ++i)
	{
	drtentry *p = list[i];
	while(p)
		{
		if(i==1) p->lexx=1; 
		else
		    {
		    lexindex tmp=0;
		    for(int j=0; j<4; ++j)
			if(p->downchain[j])
				{
				lexindex hlp = p->downchain[j]->lexx;
				p->lexx += hlp;
				p->lexy[j]=tmp;
				tmp+= hlp;
				}
		    }
		p=p->next;
		++totentries;
		}
	}

//verify the number of entries vs. shavitt formula
//int e=min(toprow.a,toprow.c);
//int nabc=(toprow.a+1)*(toprow.c+1)*(toprow.b+1+e/2)-(e+2)*(e+1)*e/6;
//this formula as taken from Matsen-Pauncz book is wrong - probably some misprint there
}


drt::~drt()
{
if(list)
	{
	int i;
	for(i=0; i<=norb; i++)
		{
		drtentry *p=list[i];
		while(p) {drtentry *q= p->next; delete p; p=q;}
		}
	delete[] list;
	}
}


extern ostream& operator<<(ostream &s, const drt &d)
{
drtentry **list=d.getlist();
if(!list || d.orbnum()==0) return s;
for(int i=d.orbnum(); i>=0; i--)
	{
	s << "DRT Level "<<i<<":\n";
	drtentry *p=list[i];
	while(p) 
		{
		s <<"        "<<p->abc << " : " <<p->lexx <<
		" : " <<p->lexy[0]<<" " <<p ->lexy[1]<<" "<<p->lexy[2]<<" "<<p->lexy[3] <<"\n";
		p=p->next;
		}
	}
return s;
}


//transformation between paldus array and path in shavitt graph (drt)
//actually we do not need DRT in principle, but let's follow pointers instead of adding numbers
paldustab::paldustab(const shavittpath &s, const drt &d) : NRVec<abcrow>(d.orbnum())
{
if(nn != s.size()) laerror("incompatible DRT and Shavitt path");
const drtentry *p=d[nn];
v[nn-1] = p->abc;
for(int i=nn-1; i>0; --i)
	{
	p= p->downchain[s[nn-1-i]];
	v[i-1] = p->abc;
	}
}


shavittpath::shavittpath(const paldustab &p) : NRVec<stepnumber>(p.orbnum())
{
abcrow zero;
zero.a=zero.b=zero.c=0;
for(int i=nn; i>0; --i)
	{
	ABCdelta d;
	int k;
	d= p[i] - (i>1? p[i-1] : zero);
	for(k=0; k<4; ++k)  if(d == abcdelta[k]) goto found;
	laerror("illegal paldus table");
	found:
	v[nn-i]=k;
	}
}

ostream& operator<<(ostream &o, const shavittpath &s)
{
for(int i=0; i<s.size(); ++i)  o<< (char) ('0'+s[i]);
return o;
}


void shavittpath::weight(gelfweight &w, const bool reversed) const
{
if(nn != w.size()) laerror("incompatible DRT, weight, and Shavitt path");
if(reversed) for(int i=0; i<nn; ++i) w[i+1] = (v[i]+1)/2;
else for(int i=0; i<nn; ++i) w[i+1] = (v[nn-1-i]+1)/2;
}


const lexindex shavittpath::index(const drt &d) const
{
lexindex s=0;
const drtentry *p = d[nn];
for(int i=0; i<nn; ++i)
	{
	s+= p->lexy[v[i]];
	p= p->downchain[v[i]];
	if(!p) return (lexindex) -1; //no such path exists in DRT
	}
return s;
}



//generate recursively all paths in the shavitt graph and process them
//paths will be in gelfand lexicografic order provided the DRT has been ordered on each level

static shavittpath workpath;
static lexindex drt_count;
static void (*drt_process)(const shavittpath &);
static int drt_norb;
void shavitt_gen(const int level, const drtentry *vertex)
{
if(level<0)
        {
        drt_count++;
        (*drt_process)(workpath);
	workpath.copyonwrite();
        return;
        }
for(int i=0; i<4; ++i)
    if(vertex->downchain[i])
        {
        workpath[drt_norb-1-level]=i;
        shavitt_gen(level-1,vertex->downchain[i]);
        }

}



lexindex shavitt_gener(const drt &d, void (*process)(const shavittpath &))
{ 
drt_count=0;
drt_norb=d.orbnum();
drt_process=process;
workpath.resize(drt_norb);
shavitt_gen(drt_norb-1,d[drt_norb]);
workpath.resize(0);
return drt_count;
}




static shavittpath workpath2;
static const shavittpath *shavitt_p0;
static std::list<shavittpath> *shavitt_list;
static lexindex drt_count2;
static int drt_norb2;

void shavitt_gen_same(const int level, const drtentry *vertex)
{
if(level<0)
        {
	++drt_count2;
	shavitt_list->push_front(workpath2);
	workpath2.copyonwrite();
        return;
        }
stepnumber t=(*shavitt_p0)[drt_norb2-1-level];
if(t==1 || t==2) //try all possibilities
    {
    for(int i=1; i<3; ++i)
    	if(vertex->downchain[i])
        	{
        	workpath2[drt_norb2-1-level]=i;
        	shavitt_gen_same(level-1,vertex->downchain[i]);
        	} 
    }
else //follow the same edge as in original path
    {
    if(vertex->downchain[t])
	{
	workpath2[drt_norb2-1-level]=t;
	shavitt_gen_same(level-1,vertex->downchain[t]);
	}
    }
}


//generate all paths in DRT with the same weight as given path 
//cannot be called from process of shavitt_gener, shares statics

int spinfunctions(std::list<shavittpath> &list, const shavittpath &p, const drt &d)
{
if(p.orbnum()!=d.orbnum()) laerror("incompatible shavittpath and drt");
shavitt_list= &list;
drt_count2=0;
drt_norb2=d.orbnum();
workpath2.resize(drt_norb2);
shavitt_p0= &p;
shavitt_gen_same(drt_norb2-1,d[drt_norb2]);
workpath2.resize(0);
return drt_count2;
}


//slater determinant stuff




static int abindex;
static NRMat<unsigned char> *abmatrix;
static int abnsingles;
static unsigned char *abwork;

void abgener(const int nbeta, const int rightmost)
{
if(nbeta==0)
	{
	memcpy(&(*abmatrix)(abindex++,0), abwork, abnsingles*sizeof(unsigned char));
	return;
	}
for(int p=rightmost; p>=nbeta-1; --p)
	{
	abwork[p]=1;
	abgener(nbeta-1,p-1);
	abwork[p]=0;
	}
}

//cache results in a hash table
struct ABKEY
	{
	orbindex nsingles;
	orbindex nalpha;
	const bool operator==(const ABKEY &rhs) const {return !memcmp(this,&rhs,sizeof(ABKEY));};
	} abkey;

typedef union {ABKEY key; size_t t;} KEY;

struct myhash {
  size_t operator()(ABKEY x) const {KEY key; key.t=0; key.key=x; return key.t; }
};

typedef hash_map<ABKEY,NRMat<unsigned char>,myhash > ABHASH;
static ABHASH abhash;

const NRMat<unsigned char> alphabeta(const int nsingles,const int nalpha)
{
abkey.nsingles=nsingles;
abkey.nalpha=nalpha;
ABHASH::iterator f=abhash.find(abkey);
if(f != abhash.end()) return f->second;
//cout <<"GENERATE "<<nsingles<<' '<<nalpha<<"\n";

int n=ibinom(nsingles,nalpha);
NRMat<unsigned char> s(n,nsingles);
abmatrix=&s;
abindex=0;
abnsingles=nsingles;
abwork = new unsigned char[nsingles];
memset(abwork,0,nsingles*sizeof(unsigned char));
abgener(nsingles-nalpha,abnsingles-1);
delete[] abwork;
abhash.insert(std::pair<ABKEY,NRMat<unsigned char> >(abkey,s));
return s;
}



//generate all Slater determinants consistent with given weight and S==M_S
NRVec<slaterdet> slaterdets(const shavittpath &p, const int nelectrons, const int spin2)
{
if((nelectrons+spin2)&1) laerror("slaterdets: inconsistent nelectrons and spin");
NRVec<int> occupied(nelectrons);
int noccupied=0;
int nsingles=0;
int ndoubles=0;
int norb=p.orbnum();

//count singly and doubly occupied orbitals
for(int i=0; i<norb; ++i)
	{
	switch(p[norb-1-i])
		{
		case 3: ndoubles++;
			occupied[noccupied++]= -i-1;
			break;
		case 2:
		case 1: nsingles++;
			occupied[noccupied++]=i+1;
			break;
		case 0: break;
		default: laerror("illegal shavittpath");
		}
	}
if(2*ndoubles+nsingles != nelectrons) laerror("slaterdets: inconsistent nelectrons");

//generate all alpha,beta permutations for singly occupied orbitals
int nalpha=(nsingles+spin2)/2;
const NRMat<unsigned char> ab=alphabeta(nsingles,nalpha); 

//generate the determinants
NRVec<slaterdet> slaters(ab.nrows());
for(int i=0; i<ab.nrows(); ++i)
	{
	slaters[i].resize(nelectrons);
	int ielectron=0;
	int isingle=0;
	for(int j=0; j<noccupied; ++j)
		{
		if(occupied[j]<0) //doubly occupied
			{
			slaters[i][ielectron++] = -occupied[j]; //alpha
			slaters[i][ielectron++] = occupied[j]; //beta
			}
		else 		//singly occupied
			{
			slaters[i][ielectron++] = ab(i,isingle++)? -occupied[j]:occupied[j]; //alpha or beta
			}
		}
	}
return slaters;
}


REAL slatercoef(const shavittpath &p, const drt &d, const slaterdet &s)
{
if(s.electrons()!=d.electrons() || p.orbnum() != d.orbnum()) laerror("inconsisten input to slatercoef"); //some more elaborate checks skipped

int nalpha=0;
int nbeta=0;
int slatindex=0;
REAL r=1.;
int n=p.orbnum();
const drtentry *x = d[0];

for(int k=1; k<=n; ++k)
	{
	x= x->upchain[p[n-k]];
	if(!x) laerror("slatercoef: shavittpath inconsistent with given drt");
	switch(p[n-k])
		{
                case 1:
			r *= x->abc.a + x->abc.b - (s[slatindex]>0 ? nbeta:nalpha);
			r /= x->abc.b;
			goto common;
                case 2:
			r *= (s[slatindex]>0 ? nbeta:nalpha) + 1 - x->abc.a;
			r /= x->abc.b + 2;
			if((x->abc.b + (s[slatindex]>0?1:0) )&1) r= -r;
		common:
			if(k!= abs(s[slatindex])) laerror("slatercoef: inconsistent slater determinant 12");	
			if(s[slatindex++]>0) nalpha++; else nbeta++;
                        break;
                case 3:
                        if(x->abc.b & 1) r= -r;
                        if(k!=s[slatindex] || -k != s[slatindex+1]) laerror("slatercoef: inconsistent slater determinant 3");
                        nalpha++;
                        nbeta++;
                        slatindex+=2;
                        break;
		default: ; //also 0
		}
	}

return r>=0.?sqrt(r):-sqrt(-r);
}

//little trafo matrix
const NRMat<REAL> safs_slater(/*const*/ std::list<shavittpath> &list, const drt &d, const NRVec<slaterdet> &slaters)
{
int nsafs=list.size();
int ndets=slaters.size();
NRMat<REAL> trafo(ndets,nsafs);

std::list<shavittpath>::iterator ii;
ii=list.begin();
for(int i=0; ii!=list.end(); ++ii,++i)
	for(int j=0; j<ndets; ++j)
		trafo(j,i) = slatercoef(*ii,d,slaters[j]);

return trafo;
}

static SparseMat<REAL> CItr;
static bitvector CItrafo_done;
static const drt *CIdrt;
int slaterfile;
static bool useinverselex;

static void shavitt_CItrafo(const shavittpath &p)
{
if(debug) 
	{
	gelfweight pw(p.size());
	p.weight(pw);
	cout << "path: "<<p.index(*CIdrt)<<": " <<p << " : weight " <<pw<<endl;
	}
if(CItrafo_done[(unsigned int)p.index(*CIdrt)]) return;

//generate all spin adapted functions with the same weight
typedef std::list<shavittpath> SAFS;
SAFS safs;
spinfunctions(safs,p,*CIdrt);

//generate all slaters
NRVec<slaterdet> slaters=slaterdets(p,CIdrt->electrons(),CIdrt->spin2());

//calculate the block of the trafo matrix
NRMat<REAL> trafo=safs_slater(safs,*CIdrt,slaters);

//build incrementally the transformation matrix
//essentially direct sum, but scattered in right index back to original lex. indices in gelfand basis
int n0=CItr.nrows();
CItr.incsize(trafo.nrows(),0);
REAL tmp;
SAFS::iterator ptr=safs.begin();
for(SPMatindex j=0; j<(SPMatindex)trafo.ncols(); ++j, ++ptr)
	{
	lexindex ind=ptr->index(*CIdrt);
	CItrafo_done.set(ind);//delete from list to be done
	for(SPMatindex i=0; i<(SPMatindex)trafo.nrows(); ++i)
		if((tmp=trafo(i,j))!=0.) CItr.add(n0+i,useinverselex? (*CIdrt).cisize()-1-ind : ind ,tmp);
	}

//append the slater list into the file
for(int i=0; i<slaters.size(); ++i) slaters[i].put(slaterfile,0);
}

typedef double SLdist; //float might give underflow problems for many electrons

//is not good measure, the interpolation search can even be slower than bisection
SLdist distslaterdet(const int n, const int *range, const spinorbindex *x, const spinorbindex *y)
{
SLdist scale=1;
SLdist r=0;

/*
cout <<"comparing \n";
for(int i=0; i<n; ++i) cout <<x[i]<<" ";
cout <<"\nand \n";
for(int i=0; i<n; ++i) cout <<y[i]<<" ";
cout <<"\n";
*/

for(int i=0; i<n; ++i)
	{
	if(range[i]>0) //in limited CI some orbitals might be always occ.
		{
		int xx= x[i]<0? -2*x[i]-1:2*x[i]-2;
		int yy= y[i]<0? -2*y[i]-1:2*y[i]-2;
		int d=xx-yy;
		if(d) return scale*d/(1+range[i]);
		}
	scale/=(1+range[i]);
	}
return r;
}

const NRVec<int> slaterbasis::range() const
{
NRVec<int> r(mm);
const spinorbindex *first=&(*this)(0,0);
const spinorbindex *last=&(*this)(nn-1,0);
for(int i=0; i<mm; ++i)
	{
	int xx= first[i]<0? -2*first[i]-1:2*first[i]-2;
        int yy= last[i]<0? -2*last[i]-1:2*last[i]-2;
	r[i]=yy-xx;
	}
return r;
}

bool slaterbasis::checkzero() const
{
for(int i=0; i<nn; ++i) for(int j=0; j<mm; ++j)
	{
	if((*this)(i,j)==0) return true;
	}
return false;
}


//assumes the dets are in canonical order like 1 -1 2 -2 3 4 -4 -5 6 -6
int compareslaterdet(const int n, const spinorbindex *x, const spinorbindex *y)
{
int d=0;

for(int i=0; i<n; ++i)
	{
	int xx= x[i]<0? -2*x[i]+1:2*x[i];
	int yy= y[i]<0? -2*y[i]+1:2*y[i];
	d=xx-yy;
	if(d) return d;
	}
return d;
}


//sorting helpers - not static, used from two places
SPMatindex *slsort_permbase;
slaterbasis *slsort_sldetbase;
int slsort_nelectrons;
int slsort_slbascmp(int i, int j)
{
return compareslaterdet(slsort_nelectrons,(*slsort_sldetbase)[slsort_permbase[i]],(*slsort_sldetbase)[slsort_permbase[j]]);
}

int slsort_slbascmp2(int i, int j)
{
return compareslaterdet(slsort_nelectrons,(*slsort_sldetbase)[slsort_permbase[j]],(*slsort_sldetbase)[slsort_permbase[i]]);
}

void slsort_slbasswap(int i, int j)
{
SPMatindex t=slsort_permbase[i]; slsort_permbase[i]=slsort_permbase[j]; slsort_permbase[j]=t;
}


//generation of whole CI vector trafo matrix and slater list
const SparseMat<REAL> CItrafo(const int slatlistfile, const drt &d, const int sortdir, const bool inverselex)
{
const lexindex cisize=d.cisize();
CItrafo_done.resize(cisize);
CItrafo_done.clear();
CItr.resize(0,cisize);
CIdrt=&d;


useinverselex=inverselex;

slaterfile=slatlistfile;

if(cisize!= shavitt_gener(d,shavitt_CItrafo)) laerror("CItrafo algorithm error or inconsistent drt data");

if(CItr.ncols()!=(SPMatindex)cisize) laerror("CItrafo algorithm error");

//optionally resort the slater basis into lex order, transform the trafo matrix indexing
if(sortdir)
	{
	lseek(slaterfile,0,SEEK_SET);
	slaterbasis slaters(CItr.nrows(),d.electrons());
	slaters.get(slaterfile,false);

	NRVec<SPMatindex> perm(CItr.nrows());
	for(lexindex i=0; i<(lexindex)CItr.nrows(); ++i) perm[i]=i;
	slsort_sldetbase=&slaters;
	slsort_permbase=&perm[0];
	slsort_nelectrons=d.electrons();
	genqsort(0,(int)CItr.nrows()-1,sortdir>0 ? slsort_slbascmp : slsort_slbascmp2 ,slsort_slbasswap);

	lseek(slaterfile,0,SEEK_SET);
	for(lexindex i=0; i<(lexindex)CItr.nrows(); ++i)
		if(d.electrons()*(int)sizeof(spinorbindex) != write(slaterfile, slaters[perm[i]] ,d.electrons()*sizeof(spinorbindex))) laerror("write error in CItrafo");
	NRVec<SPMatindex> perminv = inversepermutation(perm);
	CItr.permuterows(perminv);
	}

return CItr;
}

extern ostream & operator<<(ostream &s, const slaterdet&x)
{
  int i, n;

  n = x.size();
  for(i=0; i<n; i++) s << x[i] << (i == n-1 ? '\n' : ' ');
  return s;
}

static spinorbindex *slbase;
static void slswap(const int i, const int j) {spinorbindex t=slbase[i]; slbase[i]=slbase[j]; slbase[j]=t;}
static int slcmp(const int i, const int j) {return slbase[i]-slbase[j];}
void slaterdet::sort()
{
int i;
for(i=0; i<nn; ++i) v[i]= v[i]<0? -2*v[i]+1:2*v[i];
slbase=v;
genqsort(0,nn-1,slcmp,slswap);
for(i=0; i<nn; ++i) v[i]= v[i]&1? -(v[i]>>1) : v[i]>>1 ;
}

static int nelectr;
const int *slranges;
static SLdist distslaterdet2(const spinorbindex *x, const spinorbindex *y)
{
return distslaterdet(nelectr,slranges,x,y);
}
static SLdist distslaterdet3(const spinorbindex *x, const spinorbindex *y)
{
return -distslaterdet(nelectr,slranges,x,y);
}


static int comparslaterdet2(const spinorbindex *x, const spinorbindex *y)
{
return compareslaterdet(nelectr,x,y);
}
static int comparslaterdet3(const spinorbindex *x, const spinorbindex *y)
{
return -compareslaterdet(nelectr,x,y);
}

//find index of given det in the basis. det is assumed sort()ed, basis elementwise sort()ed and lexically ordered
lexindex slaterdet::find(const slaterbasis &slbasis, const int order, const NRVec<int> *slrange) const
{
nelectr=slbasis.ncols();
if(!slrange) return bisection_find((lexindex)0,(lexindex)slbasis.nrows()-1,v,&slbasis(0,0),nelectr,order>0 ? comparslaterdet2 : comparslaterdet3);
else {slranges= (*slrange); return interpolation_find((lexindex)0,(lexindex)slbasis.nrows()-1,v,&slbasis(0,0),nelectr,order > 0 ? distslaterdet2 : distslaterdet3);}
}


///////////////////////////////////////////////////////
//calculation of the Eij (i<=j) generators from DRT 

//some auxiliary routines and data structures
static const drt *Edrt;
static Ugenerators *Eptr;

static void makeEdiagonal(const shavittpath &p)
{
//cout << "E_from_DRT path: "<<p.index(*Edrt)<<": " <<p <<endl;
//note: path elements are stored from the top level down
const lexindex ket= useinverselex? (*Edrt).cisize()-1-p.index(*Edrt) : p.index(*Edrt);
for(int i=0; i<p.size(); ++i) 
	{
	int t = (p[p.size()-1-i]+1)/2;
	if(t>0) (*Eptr)(i+1,i+1).add(ket,ket,t);
	}
}

//loop segments according to Shavitt IJQCH S12, 5 (1978)
static double f_one(int b) {return 1.;}
static double f_mone(int b) {return -1.;}
static double f_sqrtbb1(int b) {return sqrt(b/(b+1.));}
static double f_sqrtb1b(int b) {return sqrt((b+1.)/b);}
static double f_sqrtb2b1(int b) {return sqrt((b+2.)/(b+1.));}
static double f_sqrtb1b2(int b) {return sqrt((b+1.)/(b+2.));}
static double f_sqrtx(int b) {return sqrt(b*b-1.)/b;}
static double f_sqrty(int b) {return sqrt((b+2)*(b+2)-1.)/(b+2);}
static double f_oneb(int b) {return 1./b;}
static double f_moneb2(int b) {return -1./(b+2);}


typedef struct segment
	{
	int bra_b_dif;
	stepnumber brastep;
	stepnumber ketstep;
	double (*factor)(int b);
	}
	segment;

static const int Ntops=4;
static const segment tops[Ntops] = 
	{ {0,0,2,f_one}, {0,1,3,f_sqrtbb1},{0,0,1,f_one},{0,2,3,f_sqrtb2b1} };

static const int Nbottoms=4;
static const segment bottoms[Nbottoms] =
	{ {-1,2,0,f_one}, {-1,3,1,f_sqrtb1b}, {1,1,0,f_one}, {1,3,2,f_sqrtb1b2} };

static const int Nmiddles=10;
static const segment middles[Nmiddles] =
{
	{-1,0,0,f_one},
	{-1,1,1,f_sqrtx},
	{-1,2,2,f_mone},
	{-1,3,3,f_mone},
	{1,0,0,f_one},
        {1,1,1,f_mone},
	{1,2,2,f_sqrty},
	{1,3,3,f_mone},
	{-1,2,1,f_oneb},
	{1,1,2,f_moneb2}
};

static std::list<lexindex> *upwalk_list;
static int current_j;

void upwalk_builder(const drtentry *vertex, lexindex l)
{
if(vertex==(*Edrt)[Edrt->orbnum()]) //terminate recursion
	{
	upwalk_list->push_front(l);
	return;
	}
for(stepnumber s=0; s<4;++s)
	{
	const drtentry *vnew=vertex->upchain[s];
	if(vnew) upwalk_builder(vnew,l+vnew->lexy[s]);
	}
}

static unsigned int N_loops;

//setup all matrix elements for given loop considering all possible upper and lower walks
void loop_evaluate(const drtentry *bottomvertex,lexindex loopbra, lexindex loopket, int i, REAL matel)
{
N_loops++;
//cout <<"loop done: "<< i <<" "<<current_j<<" "<<loopbra<<" "<<loopket<<" "<<matel<<endl;

std::list<lexindex>::iterator upwalk;
for(upwalk=upwalk_list->begin(); upwalk!=upwalk_list->end(); ++upwalk) //for all upper walks
	{
	//cout <<"upwalk contribution "<<*upwalk<<endl;
	for(lexindex downwalk=0; downwalk<bottomvertex->lexx; ++downwalk) //lower walks for a consecutive sequence in their lexical numbers
		{
		lexindex bra=loopbra+ *upwalk + downwalk;
		lexindex ket=loopket+ *upwalk + downwalk;
		if(useinverselex) 
			{
			bra=(*Edrt).cisize()-1-bra;
			ket=(*Edrt).cisize()-1-ket;
			}
		(*Eptr)(i,current_j).add(bra,ket,matel);
		}
	}
}


void loop_builder(const drtentry *oldbra,const drtentry *oldket,lexindex lexbra,lexindex lexket,int i,REAL matel)
{
for(int o=0; o<Nbottoms; ++o) //try all possibilities for loop bottoms
      if(oldbra->abc.b == oldket->abc.b + bottoms[o].bra_b_dif)
            {
            const drtentry *bravertex=oldbra->downchain[bottoms[o].brastep];
            const drtentry *ketvertex=oldket->downchain[bottoms[o].ketstep];
            if(bravertex && ketvertex && bravertex==ketvertex)
		loop_evaluate(bravertex,lexbra+oldbra->lexy[bottoms[o].brastep],lexket+oldket->lexy[bottoms[o].ketstep],i,matel*(*bottoms[o].factor)((oldket->abc.b)));
            }

if(i<=1) return;

for(int o=0; o<Nmiddles; ++o) //try all possibilities for loop middles
	if(oldbra->abc.b == oldket->abc.b + middles[o].bra_b_dif)
            {
            const drtentry *bravertex=oldbra->downchain[middles[o].brastep];
            const drtentry *ketvertex=oldket->downchain[middles[o].ketstep];
            if(bravertex && ketvertex)
                loop_builder(bravertex,ketvertex,lexbra+oldbra->lexy[middles[o].brastep],lexket+oldket->lexy[middles[o].ketstep],i-1,matel*(*middles[o].factor)((oldket->abc.b)));
            }
}


//main GUGA E generator matrix element computation algorithm
unsigned int E_from_DRT(Ugenerators &E, const drt &d, const bool inverselex)
{
int i,j;
N_loops=0;
const int n=d.orbnum();
const lexindex cisize=d.cisize();

//static for recursive generators
useinverselex=inverselex;

//statics for recursive routines
Edrt=&d;
Eptr=&E;

E.resize(0); //clear old contents
E.resize(n);
for(j=1; j<=n; ++j) for(i=1; i<=j;++i) E(i,j).resize(cisize,cisize);

//First the easy task of diagonal elements
//just generate all paths and according to step numbers add contributions to the E's
if(cisize!= shavitt_gener(d,makeEdiagonal)) laerror("E_from_DRT algorithm error or inconsistent drt data");

//Now the nontrivial i<j case
//1. Loop over all DRT vertices from top left downto level 2
//2. Generate all possible upper walks ending in given vertex and their lexical index contributions
//3. Find all loops possible from allowed segment types starting at given vertex
//   Calculate the matrix element composed of the factors on the way
//   Calculate contributions of the loop to bra and ket lexical indices
//4. For given loop compute bra and ket lexical indicex ranges for possible lower walks (explicit search not necessary) and assign all the equal matrix elements

const drtentry *jvertex;
for(j=n; j>=2; --j) for(jvertex=d[j]; jvertex!=NULL; jvertex=jvertex->next)
	{
	//generate all upper walks ending in jvertex
	//and compute lexical contributions for them - this is loop-independent
	//therefore it can be precomputed
	upwalk_list= new std::list<lexindex>;
	upwalk_builder(jvertex,0);
	current_j=j;

	//now generate loops starting from jvertex
	for(int o=0; o<Ntops; ++o) //try all possibilities for loop tops
	    {
	    const drtentry *bravertex=jvertex->downchain[tops[o].brastep];
	    const drtentry *ketvertex=jvertex->downchain[tops[o].ketstep];
	    if(bravertex && ketvertex)
		{
		REAL f= (*tops[o].factor)(jvertex->abc.b);
		//now try continuation of the loop - recursive loop builder
		loop_builder(bravertex,ketvertex,jvertex->lexy[tops[o].brastep],jvertex->lexy[tops[o].ketstep],j-1,f);
		}
	    }
	delete upwalk_list;
	}
return N_loops;
}


int slater_excitation(const int nelec, const spinorbindex *bra, const spinorbindex *ket, const bool assumesorted, const spinorbindex norb)
{
int ndiff;
if(assumesorted) //this algorithm should be a bit faster and does not need any allocation; spinorblists must be sorted like 1 3 5 -1 -2 -4
	{
	laerror("this is wrong, it was for sorting 1 -1 2 -3 ...");
	int ib=0; int ik=0;
	ndiff=nelec; //assume all is different and subtract on matches
	while(ib<nelec && ik <nelec)
		{
		if(abs(bra[ib])==abs(ket[ik]))
			{
			if(bra[ib]==ket[ik]) {ib++; ik++; ndiff--;} //matches
			else
				{
				//advance the alpha one and proceed
				if(ib<nelec && bra[ib]>0) ib++;
				if(ik<nelec && ket[ik]>0) ik++;
				}
			}
		else
			{
			//try to find at least same orbital regardless of spin and proceed
			while(ib<nelec && abs(bra[ib])< abs(ket[ik])) ib++;
			while(ik<nelec && abs(ket[ik])<abs(bra[ib])) ik++;
			}
		}
	}
else //straightforward occupation count algorithm
	{
	if(norb<=0) laerror("algorithm requires to know number of orbitals");
	NRVec<char> occ(2*norb+1);
	occ=0;
	for(int i=0; i<nelec; ++i) 
		{
		occ[norb+bra[i]]++; 
		occ[norb+ket[i]]--;
		}
	ndiff=0;
	for(int i= -norb; i<=norb; ++i) if(occ[norb+i]) ndiff++;
	if(ndiff&1) laerror("inconsistency in slater_excitations");
	ndiff/=2;
	}
return ndiff;
}
