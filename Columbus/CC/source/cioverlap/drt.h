/*
    Copyright (C) 2008 Jiri Pittner <jiri.pittner@jh-inst.cas.cz> or <jiri@pittnerovi.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _DRT_H_
#define _DRT_H_

#include <list>

#include "vec.h"
#include "gelfand.h"



struct abcrow;

struct ABCdelta
	{
	int a;
	int b;
	int c;
	const bool operator==(const ABCdelta &rhs) const
		{return !memcmp(this,&rhs,sizeof(ABCdelta));};
	};

extern ABCdelta abcdelta[4];

struct abcrow
	{
	orbindex a;
	orbindex b;
	orbindex c;
	orbindex error;//compiler would pad the space anyway, use this as a bool flag
	const abcrow operator+(const ABCdelta &d) const;
        const abcrow operator-(const ABCdelta &d) const;
	const ABCdelta operator-(const abcrow &rhs) const;
	const bool operator>(const abcrow &rhs) const
		{
		if(a!=rhs.a) return a>rhs.a;
		if(b!=rhs.b) return b>rhs.b;
		if(c!=rhs.c) return c>rhs.c;
		return 0;
		}//might be done more efficiently
	const bool operator==(const abcrow &rhs) const
		{
		return a==rhs.a && b==rhs.b && c==rhs.c; //do not care about error (efficiency?)
		};
	};


extern istream& operator>>(istream  &s, abcrow &x);
extern ostream& operator<<(ostream &s, const abcrow &x);

class shavittpath;
class drt;

class paldustab : public NRVec<abcrow> 
	{
public:
	paldustab(): NRVec<abcrow>() {};
	paldustab(const int n): NRVec<abcrow>(n) {};
	paldustab(const paldustab &rhs) : NRVec<abcrow>(rhs) {};
	explicit paldustab(const gelfpat &g); //generate paldus table from a gelfand one
	explicit paldustab(const shavittpath &s, const drt &d);
	//indexing from 1
	abcrow & operator[](const int i) {return v[i-1];};
        const abcrow & operator[](const int i) const {return v[i-1];};
	const int orbnum() const {return nn;} //just alias to size
	const int electrons() const {return 2*v[nn-1].a+v[nn-1].b;}
	const int spin2() const {return v[nn-1].b;}
	void weight(gelfweight &w) const; //calculate its weight
	};

extern istream& operator>>(istream  &s, paldustab &x);
extern ostream& operator<<(ostream &s, const paldustab &x);


//note - we might use iterators or STL lists, but let's do it simply the C way here
//the chaining might be tricky and dependent on the implementation of iterator anyway

struct drtentry
	{
	abcrow abc;
	lexindex lexx; 
	lexindex lexy[4]; 
	struct drtentry * upchain[4];
	struct drtentry * downchain[4];
	struct drtentry *next;
	drtentry() {memset(this,0,sizeof(drtentry));};//guarantee zero pointers
	drtentry(const abcrow&rhs) {memset(this,0,sizeof(drtentry)); abc=rhs;};
	};

extern drtentry *findabc(const abcrow&abc, drtentry *list);


class drt  //copy constructor and assingment might be implemented if needed, using reference counting 
	{
	int norb;
	struct drtentry **list;
public:
	drt(): norb(0), list(NULL) {};
	drt(const abcrow &toprow);
	~drt();
	const int orbnum() const {return norb;}
	const int electrons() const {return 2*list[norb]->abc.a+list[norb]->abc.b;}
	const int spin2() const {return list[norb]->abc.b;}
	struct drtentry **getlist() const {return list;}
	const struct drtentry * operator[](const int i) const {return list[i];}
	const lexindex cisize() const {return list[norb]->lexx;};
	};

extern ostream& operator<<(ostream &s, const drt &d);

typedef unsigned char stepnumber; //actually 2 bits per step are sufficient, some packing might be possible

class shavittpath : public NRVec<stepnumber>
//note: 0-th element is the highest level - to facilitate lexicographic comparison
	{
public:
	shavittpath() : NRVec<stepnumber> () {};
	shavittpath(const int n) : NRVec<stepnumber> (n) {};
	shavittpath(const shavittpath &rhs) : NRVec<stepnumber>(rhs) {};
	explicit shavittpath(const paldustab &p);
	void weight(gelfweight &w, const bool reversed=false) const; //calculate its weight
	//operator== inherited, comparisons are in lexicographic gelfand order
	const bool operator>(const shavittpath &rhs) const {return memcmp(v,rhs.v,nn*sizeof(stepnumber)) <0;};
	const bool operator<(const shavittpath &rhs) const {return memcmp(v,rhs.v,nn*sizeof(stepnumber)) >0;};
	const lexindex index(const drt &d) const;
	const int orbnum() const {return nn;};
	};

extern ostream& operator<<(ostream &o, const shavittpath &s);

//generation of all paths in the shavitt graph
extern lexindex shavitt_gener(const drt &d, void (*process)(const shavittpath &));

//calculation of matrix elements of E's in the drt-shavitt path representation
extern unsigned int E_from_DRT(Ugenerators &E, const drt &drt, const bool inverselex=false);

//conversion of a CI vector from the drt==gelfand basis to slater determinants


class slaterbasis : public NRMat<spinorbindex> 
	{
public:
	slaterbasis() : NRMat<spinorbindex>() {};
	slaterbasis(const int n, const int m) : NRMat<spinorbindex>(n,m) {};
	const NRVec<int> range() const;
	bool checkzero() const;
	};

extern int compareslaterdet(const int n, const spinorbindex *x, const spinorbindex *y);
class slaterdet : public NRVec<spinorbindex>
	{
public:
	slaterdet() : NRVec<spinorbindex>() {};
	slaterdet(const int nelec) : NRVec<spinorbindex>(nelec) {};
	slaterdet(const slaterdet &rhs) : NRVec<spinorbindex>(rhs) {};
	slaterdet(const spinorbindex *x, const int nelec) : NRVec<spinorbindex>(x,nelec) {};
	int compare(const slaterdet &rhs);
	bool operator>(const slaterdet &rhs) {return compareslaterdet(nn,v,rhs.v)>0;};
	bool operator<(const slaterdet &rhs) {return compareslaterdet(nn,v,rhs.v)<0;};
	const int electrons() const {return nn;};
	void sort(); //get orbital numbers in ascending order and alpha followed by beta for double occupation
	lexindex find(const slaterbasis &slbasis, const int order, const NRVec<int> *slrange=NULL) const; //assuming already sorted, is not checked!
		//if ranges of allowed orbital numbers at different positions are available, interpolation search can be done
	};


//compute mutual excitation of two slater determinants, optionally assume that
//the orbital indices (spinorbital up to a sign) are sorted in an ascending order
extern int slater_excitation(const int nelec, const spinorbindex *bra, const spinorbindex *ket, const bool assumesorted,const spinorbindex norb=0);
inline int slater_excitation(const slaterdet &bra, const slaterdet &ket, const bool assumesorted, const spinorbindex norb=0) 
	{
	if(bra.size()!=ket.size()) return -1;
	return slater_excitation(bra.size(),(const spinorbindex *)bra, (const spinorbindex *)ket, assumesorted,norb);
	}

extern ostream & operator<<(ostream &s, const slaterdet&x);

extern int spinfunctions(std::list<shavittpath> &list, const shavittpath &p, const drt &d); //note - we might precalculate the # of safs and replace the list by array

extern const NRMat<unsigned char> alphabeta(const int nsingles,const int nalpha); //generate permutations of alpha and beta spins (beta = 1, alpha =0)

extern NRVec<slaterdet> slaterdets(const shavittpath &p, const int nelectrons, const int spin2);

//single conversion coefficient (if performance is needed, something could be saved by doing whole batch of slaterdets at once)
extern REAL slatercoef(const shavittpath &p, const drt &d, const slaterdet &s);

//generate little dense matrix of transformations from safs to dets for a fixed weight
extern const NRMat<REAL> safs_slater(std::list<shavittpath> &list, const drt &d, const NRVec<slaterdet> &slaters);

//some sorting helpers for slater det basis sort
extern SPMatindex *slsort_permbase;
extern slaterbasis *slsort_sldetbase;
extern int slsort_nelectrons;
extern int slsort_slbascmp(int i, int j);
extern int slsort_slbascmp2(int i, int j);
extern void slsort_slbasswap(int i, int j);

//@@@implement generation of DRT with limited excitation level for an external (bottom) part

//@@@implement loop algorithm for two-body coupling constants, in a form suitable for integral-driven Hamilonian build or direct CI

//@@@implement the import of the columbus truncated-CI drt
//to make it independent on our little fci
//if the import includes the lexy values, the index() should automatically work also for a general truncated CI
//posssibly they store explicitly only "internal" part of DRT, take this into account...

//generate sparse matrix for the whole CI vector trafo and the basis of slater dets
//will we correctly generate all lex. paths from an imported drt? hope yes!
//rows are in Slater basis, columns in Gelfand (DRT) basis
extern const SparseMat<REAL> CItrafo(const int slatlistfile, const drt &d, const int sortdirection=0, const bool inverselex=false); 


#endif
