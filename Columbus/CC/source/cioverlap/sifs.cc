/*
    Copyright (C) 2008 Jiri Pittner <jiri.pittner@jh-inst.cas.cz> or <jiri@pittnerovi.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#define EPSPRINT .05

#include "la.h"
#include "fourindex.h"
#include "nonclass.h"
#include "basic.h"

#ifdef USE_TRACEBACK
#include "traceback.h"
#endif

#include "sifs.h"
using namespace LA;

#if 0
ssize_t readall(int fd, void *buf, size_t count)
{
ssize_t r;
size_t len=0;
do 
	{
	r=read(fd, ((char *)buf)+len, count-len);
	cout <<"READ "<<count-len<<" "<<r<<endl;
	if(r<0) {perror("read error"); return r;}
	if(r==0) return len;
	len+=r;
	}
while(len<count);
return len;
}
#endif


int getnextlab10(const uint64_t *lab10, int *lab10bit, int *lab10word)
{

if(*lab10bit == 0)
	{
	++(*lab10word);
	*lab10bit = 64;
	}

uint64_t tmp = lab10[*lab10word];

if(*lab10bit >= N10) //label is within one word
	{
	*lab10bit -= N10;
	return (tmp>>(*lab10bit)) & MASK(N10);
	}

//else glue it from two words
int r;
r = tmp & MASK(*lab10bit); //remaining bits from previous word
int n = N10- *lab10bit; //thus many we need from next one
r <<= n;

++(*lab10word);
tmp = lab10[*lab10word];
*lab10bit = 64-n;
r |= (tmp>>(*lab10bit)) & MASK(n); //get remaining bits

return r;
}

sifs::sifs(INT f)
{
fd=f;
lseek64(f,0L,SEEK_SET);

FREC mark1,mark2;

//read first record
if(sizeof(FREC)!= readall(f,&mark1,sizeof(FREC))) laerror("cannot read sif file 1a");
if(sizeof(sifs_header)!= readall(f,&header,sizeof(sifs_header))) laerror("cannot read sif file 3a");
if(sizeof(FREC)!= readall(f,&mark2,sizeof(FREC))) laerror("cannot read sif file 2a");
if(mark1!=mark2) 
	{
	cerr<< mark1<<" "<<sizeof(sifs_header)<<" "<<mark2<<endl;
	laerror("record mismatch in sif file 1a (maybe 32/64 bit integer problem? recompile me with/without option -D FORCE_FORTRAN_INT_64_RECORD32. See instructions in installnx.pl");
	}

//allocate
titles= new TITLE[header.ntitle];
nbpsy= new INT[header.nsym];
slabel= new SLABEL[header.nsym];
infos= new INT[header.ninfo];
bfnlabs = new BFNLAB[header.nbas];
etypes = new INT[header.nenergy];
energies= new REAL[header.nenergy];
mtypes= new INT[header.nmap];
map.resize(header.nmap,header.nbas);

//and read from second record
if(sizeof(FREC)!= readall(f,&mark1,sizeof(FREC))) laerror("cannot read sif file 1b");
if(header.ntitle*sizeof(TITLE) != readall(f,titles,header.ntitle*sizeof(TITLE))) laerror("cannot read sif file 3b");
if(header.nsym*sizeof(INT)  != readall(f,nbpsy,header.nsym*sizeof(INT))) laerror("cannot read sif file 4b");
if(header.nsym*sizeof(SLABEL)  != readall(f,slabel,header.nsym*sizeof(SLABEL))) laerror("cannot read sif file 5b");
if(header.ninfo*sizeof(INT)  != readall(f,infos,header.ninfo*sizeof(INT))) laerror("cannot read sif file 6b");
if(header.nbas*sizeof(BFNLAB) != readall(f,bfnlabs,header.nbas*sizeof(BFNLAB))) laerror("cannot read sif file 7b");
if(header.nenergy*sizeof(INT)  != readall(f,etypes,header.nenergy*sizeof(INT))) laerror("cannot read sif file 8b");
if(header.nenergy*sizeof(REAL) != readall(f,energies,header.nenergy*sizeof(REAL))) laerror("cannot read sif file 9b");
if(header.nmap*sizeof(INT)  != readall(f,mtypes,header.nmap*sizeof(INT))) laerror("cannot read sif file 10b");
map.get(f,false);
if(sizeof(FREC)!= readall(f,&mark2,sizeof(FREC))) laerror("cannot read sif file 2b");
if(mark1!=mark2) laerror("record mismatch in sif file 1b");
dataoffset=lseek64(f,0L,SEEK_CUR);
data2offset=0L;
}

ostream& operator<<(ostream &o, const  block_header &b)
{
o<< "number of integrals in block = "<<b.num<<endl;
o<< "integral symmetry type = "<<b.itypea<<endl;
o<< "integral type = "<<b.itypeb<<endl;
o<< "last = "<<b.last<<endl;
o<< "format type = "<<b.ifmt<<endl;
o<< "bit vector type = "<<b.ibvtyp<<endl;
o<< "offset to labels = "<<sizeof(REAL)*(b.lab1-2)<<endl;
o<< "reserved = "<<b.reserved<<endl;
return o;
}


ostream& operator<<(ostream &o, const sifs &s)
{
o<< "ntitle= "<<s.header.ntitle<<endl;
o<< "nsym= "<<s.header.nsym<<endl;
o<< "ninfo= "<<s.header.ninfo<<endl;
o<< "nbas= "<<s.header.nbas<<endl;
o<< "nenergy= "<<s.header.nenergy<<endl;
o<< "nmap= "<<s.header.nmap<<endl;
o<<endl;

o<<"TITLES:\n";
for(int i=0; i<s.header.ntitle; ++i) o<<s.titles[i]<<endl;

o<<"NBF AND LABEL PER SYMMETRY:\n";
for(int i=0; i<s.header.nsym; ++i) o<< s.nbpsy[i]<<" "<<s.slabel[i]<<endl;

o<<"INFOS:\n";
for(int i=0; i<s.header.ninfo; ++i) o<< s.infos[i] <<" ";
if(s.infos[0]==2) o<<"2-el integrals separately\n"; else o<<"2-el integrals in the same file\n";
o<< "1-el integral record length = "<<s.infos[1]<<endl;
o<< "max number of 1-el integrals in the record = "<<s.infos[2]<<endl;
o<< "2-el integral record length = "<<s.infos[3]<<endl;
o<< "max number of 2-el integrals in the record = "<<s.infos[4]<<endl;

o<<endl;

o<<"BFNLABS:\n";
for(int i=0; i<s.header.nbas; ++i) o<< s.bfnlabs[i]<<endl;

o<<"ENERGIES:\n";
for(int i=0; i<s.header.nenergy; ++i) o<< "Type="<<s.etypes[i]<<" E= "<<s.energies[i]<<endl;

o<<"MAPTYPES:\n";
for(int i=0; i<s.header.nmap; ++i) o<< s.mtypes[i] <<" ";
o<<endl;


o<<"MAP:\n"<<s.map<<endl;

return o;
}


int read_sifs_onel(sifs &s, NRSMat<REAL> &x, int &itypea, int &itypeb, int &last, REAL &fcore)
{
FREC mark1,mark2;
REAL buf[s.infos[1]];
off64_t start=lseek64(s.fd,0L,SEEK_CUR);
int r;
block_header_real h;

x.resize(s.header.nbas);
x=0.;
bool first=1;
int nblocks=0;
last=0;
do	
	{
	off64_t offset0 = lseek64(s.fd,0L,SEEK_CUR);
	r=readall(s.fd,&mark1,sizeof(FREC));
	if(r<0) return -2;
	if(r==0) return -1;
	if(r!=sizeof(FREC)) laerror("inconsistent block");
	if(mark1>sizeof(REAL)*s.infos[1]) laerror("unexpectedly large block");
	if(mark1!=readall(s.fd,&buf,mark1)) laerror("cannot read block");
	h.r=buf[0];
	if(first) 
		{
		cout <<"starting header "<<h.h.itypea<<" "<<h.h.itypeb<<" size "<<h.h.num << " last "<<h.h.last<<endl;
		itypea=h.h.itypea;
		itypeb=h.h.itypeb;
		first=0;
		}
	else
		{
		cout <<"continuation header "<<h.h.itypea<<" "<<h.h.itypeb<<" size "<<h.h.num << " last "<<h.h.last<<endl;
		if(itypea!=h.h.itypea) cout <<"Warning: inconsistent itypea (missing last=1 at previous block)\n";
		if(itypeb!=h.h.itypeb)  cout <<"Warning: inconsistent itypeb (missing last=1 at previous block)\n";
		if(itypea!=h.h.itypea || itypeb!=h.h.itypeb) 
			{
			//integrals actully finished in previous block (dalton bug workaround)
			if(offset0 != lseek64(s.fd,offset0,SEEK_SET)) laerror("cannot seek in integral file");
			last = 1;
			return nblocks;
			}
		}
	//cout <<h.h<<endl;
	if(sizeof(FREC)!=readall(s.fd,&mark2,sizeof(FREC))) laerror("inconsistent block 2");
	if(mark1!=mark2) laerror("inconsistent block 3");
	++nblocks;

	//check block
	if(h.h.reserved) laerror("reserved should be 0");
	if(h.h.ibvtyp) laerror("nonzero ibvtyp unsupported");
	if(h.h.itypea>3)  laerror("itypea>3 unsupported");
	//if(h.h.itypea==2)  stored in SMAT but antisymmetric matrix!
	if(h.h.itypea==3) {s.data2offset=start; lseek64(s.fd,start,SEEK_SET); return -3;}
	if(h.h.ifmt>2) laerror("unsupported ifmt");
	if(h.h.num <0 || h.h.num > s.infos[2]) laerror("excessive number of integrals in block");

	//process block
	fcore = buf[h.h.num+1];
	//cout << "test "<<h.h.num <<" "<<h.h.lab1<<endl;
	bit8ind2 *lab8ptr = (bit8ind2 *) &buf[h.h.lab1-1];
	uint64_t *lab10 = (uint64_t *) &buf[h.h.lab1-1];
	bit16ind2 *lab16ptr = (bit16ind2 *) &buf[h.h.lab1-1];
	int lab10bit=64;
        int lab10word=0;
	if(h.h.num+2!=h.h.lab1-1) laerror("unexpected hole in integral block");
	int i,j;
	for(int ii=0; ii<h.h.num; ++ii)
		{
		int iieff;
		switch(h.h.ifmt) {
		   case 0: //8-bit packing
			//possible endianity non-portability!!!
			iieff = (ii&0xfffffffc) + 3 - (ii&3);
			i=lab8ptr[iieff].i;
			j=lab8ptr[iieff].j;
			break;
		   case 2: //16-bit packing
			//!! not tested
			iieff = (ii&0xfffffffe) + 1 - (ii&1);
			i=lab16ptr[iieff].i;
                        j=lab16ptr[iieff].j;
			break;
		   case 1: //10-bit packing
			i=getnextlab10(lab10,&lab10bit,&lab10word);
			j=getnextlab10(lab10,&lab10bit,&lab10word);
			break;
		}
		//cout <<"test2 "<<ii<<" "<<" "<<i<<" "<<j<<" "<<buf[ii+1]<<endl;
		if(i<=0||i>x.nrows()) laerror("i index out of range");
		if(j<=0||j>x.ncols()) laerror("j index out of range");
		x(i-1,j-1) = buf[ii+1];
		}
	}
while(!h.h.last);
last=h.h.last;
return nblocks;
}

