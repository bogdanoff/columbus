/*
    Copyright (C) 2008 Jiri Pittner <jiri.pittner@jh-inst.cas.cz> or <jiri@pittnerovi.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "lbinom.h"
#include <math.h>
//gnu scientific library, presently used for zeta only, later also gamma and incomplete gamma can be taken from it
#ifndef NO_GSL
#include <gsl/gsl_sf_zeta.h>
#else
//needs libexps
extern "C" double myzeta(double);
#endif

#define MAXFACT 70
unsigned long lfact(const unsigned int n)
{
register int j;
static unsigned int ntop=12;
static unsigned long a[MAXFACT+1]={1L,1L,2L,6L,24L,120L,720L,5040L,
40320L,
362880L,
3628800L,
39916800L,
479001600L};

if (n < 0) laerror("negative argument of factorial");
if (n > MAXFACT) laerror("overflow in factorial");
        while (ntop<n) {
                j=ntop++;
                a[ntop]=a[j]*ntop;
                }
        return a[n];
}
#undef MAXFACT



#define MAXBINOM 50
#define ibidxmaxeven(n) ((n-2)*(n-2)/4)
#define ibidx(n,k) (k-2+(n&1?(n-3)*(n-3)/4:(n/2-1)*(n/2-2)))
unsigned long lbinom(unsigned long n, unsigned long k)
{
register unsigned long p,value;
register unsigned long d;
static unsigned long ibitab[ibidxmaxeven(MAXBINOM)]= /*only nontrivial are stored,
                                            zero initialization by compiler assumed*/
{
6,
10,
15,20,
21,35,
28,56,70
};


if(k>n||k<0) return(0);
if(k>n/2) k=n-k;
if(k==0) return(1);
if(k==1) return(n);
int ind=0;
if(n<=MAXBINOM)
        {
        ind=ibidx(n,k);
        if (ibitab[ind]) return ibitab[ind];
        }
/* nonrecurent method used anyway */
d=n-k;
p=1;
for(;n>d;n--) p *= n;
value=p/lfact(k);
if(n<=MAXBINOM) ibitab[ind]=value;
return value;
}
#undef ibidx
#undef ibidxmaxeven
#undef MAXBINOM


#define MAXFACT 170
REAL ifact(int n)
{
register int j;
static  int ntop=12;
static REAL a[MAXFACT+1]={1L,1L,2L,6L,24L,120L,720L,5040L,
40320L,
362880L,
3628800L,
39916800L,
479001600L};

if (n < 0) laerror("negative argument of factorial");
if (n > MAXFACT) laerror("overflow in factorial");
        while (ntop<n) {
                j=ntop++;
                a[ntop]=a[j]*ntop;
                }
        return a[n];
}
#undef MAXFACT



#define MAXBINOM 40
#define ibidxmaxeven(n) ((n-2)*(n-2)/4)
#define ibidx(n,k) (k-2+(n&1?(n-3)*(n-3)/4:(n/2-1)*(n/2-2)))
int ibinom(int n, int k)
{
register int p,value;
register int d;
static int ibitab[ibidxmaxeven(MAXBINOM)]= /*only nontrivial are stored,
                                            zero initialization by compiler assumed*/
{
6,
10,
15,20,
21,35,
28,56,70
};


if(k>n||k<0) return(0);
if(k>n/2) k=n-k;
if(k==0) return(1);
if(k==1) return(n);
int ind=0;
if(n<=MAXBINOM)
        {
        ind=ibidx(n,k);
        if (ibitab[ind]) return ibitab[ind];
        }
/* nonrecurent method used anyway */
d=n-k;
p=1;
for(;n>d;n--) p *= n;
value=p/lfact(k);
if(n<=MAXBINOM) ibitab[ind]=value;
return value;
}
#undef ibidx
#undef ibidxmaxeven
#undef MAXBINOM


#define MAXFACT2 300
REAL ifact2(int n)
{
register int j;
static int ntop=12;
static REAL a[MAXFACT2+1]={1.,1.,2.,3.,8.,15.,48.,105.,384.,945.,3840.,10395.,46080. };

if (n < -1) laerror("illegal argument of double factorial"); /* in some cases advantageous to define it to have value 1 for -1 */
if (n<2) return 1.0;
if (n > MAXFACT2) laerror("overflow in double factorial");
        while (ntop<n) {
                j=ntop++;
                a[ntop]=a[j-1]*ntop;
                }
        return a[n];
}
#undef MAXFACT2



REAL dipow(REAL x,int i)
{
REAL y=1.0;
if(x==0.0)
        {
        if(i>0) return(0.0);
        if(i==0)  return(1.0);
/*ieee inf.*/
        return(FP_INFINITE);
        }
if(i) {
        if(i<0) {i= -i; x= 1.0/x;}
        do
                {
                if(i&1) y *= x;
                x *= x;
                }
        while((i >>= 1)/*!=0*/);
        }
return(y);
}


#define eps 1e-13
REAL smartpow(REAL x,REAL n)
{
REAL a;

if(abs((a=floor(n+0.5))-n)<eps && abs(n)< (1<<(8*sizeof(int)-1))) return(dipow(x,(int)a));
if(x==0.0)
        {
        if(n>0.0) return(0.0);
/*ieee*/
        if(n<0.) return(FP_INFINITE);
        return FP_NAN;
        }
/*n==0.0 can be excluded because it will go to dipow*/
if(x<0.0)
        {
        REAL b,p;
        /*try to calculate odd root - like (-1/27)^(-1/3)==-3 is OK*/
        a= 1.0/n;
        if(abs((b=floor(a+0.5))-a)<eps) if(((int)b)%2) return(-pow(-x,n));
        for(b=3.0;b<1000.0;b+=2.0)
                {
                p=n*b;
                if(abs((a=floor(p+0.5))-p)<eps) break;
                }
        if(b<1000.0) return(((int)abs(a))%2?-pow(-x,n):pow(-x,n));
        laerror("negative base and general exponent of a power");
        }
if(abs(abs(n)-0.5)<eps)
        {
        if(n>0.0) return sqrt(x); else return sqrt(1.0/x);
        }
return(pow(x,n));
}




#define MAXBERN 50
REAL ibernum(int n)
{
static REAL bern[MAXBERN]={1.0,0.166666666666666666,-0.03333333333333333333,0.0238095238095238,-0.033333333333333333,0.07575757575757575757};
static int top=5;
register int i;

if(n<0) laerror("negative index of Bernoulli number");
if(n==1) return(-0.5);
if(n&1) return(0.0);
n /= 2;
if( n >= MAXBERN) /* use zeta function for it */
        {int m;
        m=2*n;
        return(
#ifndef NO_GSL
		gsl_sf_zeta
#else
		myzeta
#endif
		((double)m)*(n&1?2.0:-2.0)*ifact(m)/dipow(2.0*pi,m));
        }
if(n<=top) return(bern[n]);

/* precalculate and save for next use, because the calc. is expensive */
/* could be calculated just one at need, but let's do all of them */
for(i=top+1;i<=n;i++)
        {int m;
        m=2*i;
        bern[i]=
#ifndef NO_GSL
                gsl_sf_zeta
#else
                myzeta
#endif
		((double)m)*(i&1?2.0:-2.0)*ifact(m)/dipow(2.0*pi,m);
        }
top=n;
return(bern[n]);
}
#undef MAXBERN



REAL stirfact(REAL n) /* n should not be less than 9 or 7 at worst */
{
REAL logz,cor,nn,z;

cor=0.0;
z=n+1.0;
logz=log(z);
if(z>=2.0) /*does not have sense for smaller anyway and could make troubles*/
        {
        register int i,ii,m;
        m=(int)ceil(0.5-log(eps/10000.)/logz/2.0);
        if(m<2)m=2; if(m>16)m=16;
        nn=z*z;
        for(i=m;i>0;i--) {ii=2*i;cor = cor/nn + ibernum(ii)/((double)ii*(ii-1));}
        cor /=z;
        }

/*0.5*ln(2*pi)=0.91893853320467275836*/
return((z-0.5)*logz-z+0.918938533204672741780329736406+cor);
/*cor=log((1.0/(24.0*n)+1.0)/(12.0*n)+1.0)*/
}




#define MAXFACT 100
#define LIMITFACT 150
REAL factln(int n)
{
static int top=MAXFACT;
static REAL a[LIMITFACT+1]={
0.0,
0.0,
0.69314718055994530843,
1.7917594692280549573,
3.1780538303479457518,
4.7874917427820458116,
6.579251212010101213,
8.5251613610654146669,
10.604602902745250859,
12.801827480081469091,
15.10441257307551588,
17.502307845873886549,
19.987214495661884683,
22.552163853123424531,
25.191221182738679829,
27.899271383840890337,
30.671860106080671926,
33.505073450136890756,
36.395445208033052609,
39.339884187199494647,
42.335616460753485057,
45.380138898476907627,
48.471181351835227247,
51.606675567764376922,
54.784729398112318677,
58.003605222980517908,
61.261701761002001376,
64.557538627006337606,
67.889743137181540078,
71.257038967168014665,
74.658236348830158136};


if(n < 0) laerror("negative argument of factln");
if(n < 2) return(0.0);
if(n <= LIMITFACT) /*supposed LIMITFACT > MAXFACT*/
	if(a[n]) return(a[n]); else 
		{
		if(n<=MAXFACT) return(a[n]= log(ifact(n)));

		{
		register int j;
		if(!a[top]) a[top]= log(ifact(top));
		while (top<n) {
			j=top++;
			a[top]=a[j]+log((double)top);
			}
		return a[n];
		}
		}
else return(stirfact((double)n));
}
#undef LIMITFACT

REAL mygamma(REAL x)
{
bool f;
REAL p;

if(x>MAXFACT) laerror("too big argument in gamma");
f= (fabs(x-floor(x+0.5))<eps);
if(x<=0.0 && f) laerror("nonpositive integer argument in gamma");
if(f) return ifact((int)floor(x+0.5)-1);

if(x<8.0)
        {
        p=1.0;
        while(x<8.0) {p /= x; x += 1.0;}
        return(p*exp(stirfact(x-1.0)));
        }
return(exp(stirfact(x-1.0)));
}
#undef MAXFACT


REAL gammln(REAL x)
{
bool f;

f= (abs(x-floor(x+0.5))<eps);
if(x<=0.0 && f) laerror("nonpositive integer argument in gamma");
if(abs(x-floor(x+0.5))<eps) return(factln(int(x+0.5)-1));
if(x<8.0)
        {
        REAL p;
        p=0.0;
        while(x<8.0) {p -= log(abs(x)); x += 1.0;}
        return(p+stirfact(x-1.0));
        }
return(stirfact(x-1.0));
}
#undef eps





#define ITMAX 200
#define EPS 5.0e-14
void gser(REAL *gamser,REAL a,REAL x,REAL *gln)
{
        int n;
        REAL sum,del,ap;

        *gln=gammln(a);
        if (x <= 0.0) {
                if (x < 0.0) laerror("negative argument in gamma");
                *gamser=0.0;
                return;
        } else {
                ap=a;
                del=sum=1.0/a;
                for (n=1;n<=ITMAX;n++) {
                        ap += 1.0;
                        del *= x/ap;
                        sum += del;
                        if (abs(del) < abs(sum)*EPS) {
                                *gamser=sum*exp(-x+a*log(x)-(*gln));
                                return;
                        }
                }
                laerror("gamma incomplete: cannot reach sufficient precission");
                return;
        }
}

#undef ITMAX
#undef EPS

#define ITMAX 200
#define EPS 5.0e-14

void gcf(REAL *gammcf,REAL a,REAL x,REAL *gln)
{
        int n;
        REAL gold=0.0,g,fac=1.0,b1=1.0;
        REAL b0=0.0,anf,ana,an,a1,a0=1.0;

        *gln=gammln(a);
        a1=x;
        for (n=1;n<=ITMAX;n++) {
                an=(double) n;
                ana=an-a;
                a0=(a1+a0*ana)*fac;
                b0=(b1+b0*ana)*fac;
                anf=an*fac;
                a1=x*a0+anf*a1;
                b1=x*b0+anf*b1;
                if (a1) {
                        fac=1.0/a1;
                        g=b1*fac;
                        if (abs((g-gold)/g) < EPS) {
                                *gammcf=exp(-x+a*log(x)-(*gln))*g;
                                return;
                        }
                        gold=g;
                }
        }
        laerror("gamma incomplete: cannot reach sufficient precission");
}

#undef ITMAX
#undef EPS



REAL gammp(REAL a,REAL x)
{
        REAL gamser,gammcf,gln;

        if (x < 0.0 || a <= 0.0) laerror("invalid arguments for incomplete gamma");
        if (x < (a+1.0)) {
                gser(&gamser,a,x,&gln);
                return gamser;
        } else {
                gcf(&gammcf,a,x,&gln);
                return 1.0-gammcf;
        }
}

