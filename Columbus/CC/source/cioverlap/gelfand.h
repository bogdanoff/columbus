/*
    Copyright (C) 2008 Jiri Pittner <jiri.pittner@jh-inst.cas.cz> or <jiri@pittnerovi.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _GELFAND_H_
#define _GELFAND_H_
#include "la.h"
#include "sparsemat.h"
#include "fourindex.h"
#include "matexp.h"
#include "basic.h"

using namespace std;
using namespace LA;

struct abcrow;

//indexed from 1
class gelfweight : public NRVec<occnum> {
public:
        gelfweight(): NRVec<occnum>() {};
        gelfweight(const int n):NRVec<occnum>(n) {};
	inline occnum& operator[](const int i) {return v[i-1];}
	inline const occnum& operator[](const int i) const {return v[i-1];}
};
extern ostream& operator<<(ostream &s, const gelfweight &x);


//this inheritance makes a convenient, but not the most efficient implementation, since reference counting is not essential here

class gelfpat : public NRSMat<occnum> {
public:
	gelfpat(): NRSMat<occnum>() {};
	gelfpat(const int n):NRSMat<occnum>(n) {};
	gelfpat(const gelfpat &rhs) : NRSMat<occnum>(rhs) {};
	inline int orbnum() const {return nn;}
	inline int partnum() const {int i,s=0; for(i=0; i<nn; ++i) s+=v[i]; return s;}
	inline const occnum& operator() (const int i, const int j) const {return v[i-1+(nn+j+1)*(nn-j)/2];}
	inline occnum& operator() (const int i, const int j)  {return v[i-1+(nn+j+1)*(nn-j)/2];}
	void check(bool onlyfirstline=0) const;
	//NOTE: operators < > == != ASSUME THE SAME TOP ROW - patterns belonging to the same basis
	//storage order of the patterns corresponds to lexicographic order
	inline bool operator==(const gelfpat &rhs) const {return !memcmp(v+nn,rhs.v+nn,sizeof(occnum)*nn*(nn-1)/2);}
	inline int operator!=(const gelfpat &rhs) const {return memcmp(v+nn,rhs.v+nn,sizeof(occnum)*nn*(nn-1)/2);}
	inline bool operator<(const gelfpat &rhs) const {return memcmp(v+nn,rhs.v+nn,sizeof(occnum)*nn*(nn-1)/2)<0;}
	inline bool operator>(const gelfpat &rhs) const {return memcmp(v+nn,rhs.v+nn,sizeof(occnum)*nn*(nn-1)/2)>0;}
	int find(const gelfpat *basis, int bassize) const;
	void weight(gelfweight &w) const; //calculate its weight
};
extern istream& operator>>(istream  &s, gelfpat &x);
extern ostream& operator<<(ostream &s, const gelfpat &x);


class Partition : public NRVec<int> {
public:
	Partition() : NRVec<int>() {};
	explicit Partition(const int n):NRVec<int>(n) {};
	Partition(const int a, const int n):NRVec<int>(a,n) {};
	inline Partition(const gelfpat &g) : NRVec<int>(0,g.orbnum()) {int n=g.orbnum(); for(int i=0;i<n; ++i) v[i]=g(i+1,n);}; //allow implicit type conversions for convenience
	Partition(const abcrow &abc); 
	const Partition adjoint() const;	
	inline int sum() const {register int i,s=0; for(i=0;i<nn; ++i) s+=v[i]; return s;};

};
extern int UNirrepdim(const Partition p); //size of the UN irrep generated from this pattern (depends on the top row only)
extern int SRirrepdim(const Partition p); //size of the SR irrep generated from this pattern (depends on the top row only)




//general routine for contraction of operator with CI vectors, each of them can be dense or sparse
//not OK for complex-valued
template <class oper, class vector>
inline REAL contract(const oper &op, const bool tr, const vector &bra, const vector &ket)
{
return tr?ket*(op*bra):bra*(op*ket);
}

//and specialization for bra,ket==sparse matrix with one dimension ==1
template <class T>
inline T contract(const SparseMat<T> &op, const bool tr, const SparseMat<T> &bra, const SparseMat<T> &ket)
{
if(bra.nrows()!=1 && bra.ncols()!=1 || ket.nrows()!=1 && ket.ncols()!=1 ) laerror("dimension of bra or ket != 1 in contract");

SparseMat<T> tmp(tr?op.ncols():op.nrows(),1),result(1,1); 

tmp.gemm((T)0,op,tr?'t':'n',ket,ket.nrows()==1?'t':'n',(T)1);
result.gemm((T)1,bra,bra.ncols()==1?'t':'n',tmp,'n',(T)1);

return result.trace();
}

//functions for CI coupling coefficients
extern int gelfpat_gener(const gelfpat &g);
extern SparseMat<REAL> twobodyop(const Ugenerators &E, const int r,const int s,const int t,const int u,const bool onlyhalf=1,const bool normal=1);
extern SparseMat<REAL> symtwobodyop(const Ugenerators &E, const int r,const int s,const int t,const int u,const bool onlyhalf=1,const bool normal=1);

//for CC amplitudes
extern SparseMat<REAL> antisymonebodyop(const Ugenerators &E, const int p, const int q, const bool onlyhalf=1);
extern SparseMat<REAL> antisymtwobodyop(const Ugenerators &E, const int r,const int s,const int t,const int u,const bool onlyhalf=1,const bool normal=1);
extern fourindex<index4type, SparseMat<REAL> > allantisymtwobodyop(const Ugenerators &E, const bool onlyhalf=1, const bool normal=1);
extern fourindex<index4type, SparseMat<REAL> > allantisymonebodyop(const Ugenerators &E, const bool onlyhalf=1);


//functions to get density matrices, for both dense and sparse CI vectors
template<class vector>
NRSMat<REAL> onedensity(const Ugenerators &E, const vector &ket)
{
int n,i,j;
n=E.nrows();
NRSMat<REAL> result(n);
for(i=1;i<=n;++i) for(j=i;j<=n;++j) result(i,j)=contract(E(i,j),0,ket,ket);
return result;
}

template<class vector>
NRMat<REAL> onetrdensity(const Ugenerators &E, const vector &bra, const vector &ket)
{
int n,i,j;
n=E.nrows();
NRMat<REAL> result(n,n);
for(i=1;i<=n;++i) for(j=1;j<=n;++j) result(i,j)=contract(E(i,j),E.transp(i,j),bra,ket);
return result;
}

//twodensity
template<class vector>
fourindex<index4type,REAL> twodensity(const Ugenerators &E, const vector &ket)
{
int r,s,t,u;
int n=E.nrows();
fourindex<index4type,REAL> result(n);
for(r=1; r<=n; r++) for(s=1; s<=r; s++)
        for(t=1; t<=r; t++)
           {
           int h=t;
           if(t==r && s<t) h=s;  /*ensure rs>=tu if r==t*/
           for(u=1; u<=h; u++)
                {
		REAL tmp= contract(symtwobodyop(E,r,s,t,u,0),ket,ket);//generate full representation to support contraction with sparse ket
#ifdef SPARSEEPSILON
                   if(abs(tmp)>SPARSEEPSILON) 
#else
		     if(tmp)
#endif
			result.add(r,s,t,u,tmp);
                }
           }
return result;
}


//commutator of E matrices
inline SparseMat<REAL> Ecommutator(const Ugenerators &E, const int p, const int q, const int r, const int s)
{
return commutator(E(p,q),E(r,s),E.transp(p,q),E.transp(r,s));
}


//generation of gelfand basis
extern int gelfpat_gener(const gelfpat &g, NRVec<gelfpat> *gelfbasis, Ugenerators *E);

//generation of partitions
extern int partgener(int size,int n,int typ,void (*process) (int,Partition &));

extern double ifact(const int i);

#endif
