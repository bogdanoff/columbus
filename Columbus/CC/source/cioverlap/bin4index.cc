/*
    Copyright (C) 2008 Jiri Pittner <jiri.pittner@jh-inst.cas.cz> or <jiri@pittnerovi.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#define EPSPRINT .05

#include "la.h"
#include "fourindex.h"
#include "nonclass.h"

#include "basic.h"

#ifdef USE_TRACEBACK
#include "traceback.h"
#endif

using namespace std;
using namespace LA;

int main(int argc, char **argv)
{
#ifdef USE_TRACEBACK
sigtraceback(SIGSEGV,1);
sigtraceback(SIGABRT,1);
sigtraceback(SIGBUS,1);
sigtraceback(SIGFPE,1);
#endif

cout.setf(ios::scientific);
    cout.precision(16);

if(argc<2) laerror("missing file name on command line");
int f=open(argv[1],O_RDONLY);
if(f == -1) {perror("cannot open file"); laerror("cannot open file");}
fourindex_ext<unsigned short,REAL> a(f);
cout <<a;

}
