/*
    Copyright (C) 2008 Jiri Pittner <jiri.pittner@jh-inst.cas.cz> or <jiri@pittnerovi.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _sifs_h
#define _sifs_h

#include <stdint.h>

#include "la.h"

#include "fourindex.h"

using namespace std;
using namespace LA;

//extern ssize_t readall(int fd, void *buf, size_t count);
#define readall read

//change this to unsigned char for basis <255 functions
typedef unsigned short TWOEL_INDEX;

//comment this out if Dalton was compiled with 32-bit integers
 #define FORCE_FORTRAN_INT_64_RECORD32

#ifdef FORCE_FORTRAN_INT_64_RECORD32
typedef unsigned int FREC; //type with a size matching to fortran sequential I/O record separator - change as necessary
typedef int64_t INT; //same...
#else
typedef unsigned int FREC;
typedef int INT;
#endif

typedef char SLABEL[4];
typedef char BFNLAB[8];
typedef char TITLE[80];

#define N10 10
#define MASK(N) ((1L<<(N))-1)

int getnextlab10(const uint64_t *lab10, int *lab10bit, int *lab10word);

struct sifs_header
	{
	INT version;
	INT ntitle;
	INT nsym;
	INT nbas;
	INT ninfo;
	INT nenergy;
	INT nmap;
	} __attribute__((packed)) ;

struct block_header //enforce fixed bit order, !!!may depend on endianity and be non-portable with the fortran-generated packinng; this works on x86
	{
	unsigned int last :2  __attribute__((packed)) ;
	unsigned int ifmt :3  __attribute__((packed)) ;
	unsigned int reserved :11 __attribute__((packed)) ;
        unsigned int itypeb :10 __attribute__((packed)) ;
	unsigned int itypea :3 __attribute__((packed)) ;
	unsigned int ibvtyp :3 __attribute__((packed)) ;
	unsigned int lab1 :16  __attribute__((packed)) ; //offset to labels +1(fortran)+1(this header) in REAL words size
	unsigned int num :16  __attribute__((packed)) ;
	};

union block_header_real
	{
	struct block_header h;
	REAL r;
	};

class sifs
        {
	//intentionally no copy constructor and assignment to prevent double deleting
	private:
	sifs &operator=(const sifs &rhs);
	sifs(const sifs &rhs);
	public:
	sifs(INT fd);
	~sifs() {delete[] titles; delete[] nbpsy; delete[] slabel; delete[] infos; delete[] bfnlabs; delete[] etypes; delete[] energies; delete[] mtypes;};

	//data
	sifs_header header;
	TITLE *titles;
	INT *nbpsy;
	SLABEL *slabel;
	INT *infos;
	BFNLAB *bfnlabs;
	INT *etypes;
	REAL *energies;
	INT *mtypes;
	NRMat<INT> map;
	off64_t dataoffset;
	off64_t data2offset;
	int fd;
        };

struct bit8ind2
	{
	unsigned char i;
	unsigned char j;
	} __attribute__((packed)) ;

struct bit8ind4
	{
	unsigned char i;
        unsigned char j;
        unsigned char k;
        unsigned char l;
        } __attribute__((packed)) ;

struct bit16ind2
        {
        unsigned short i;
        unsigned short j;
        } __attribute__((packed)) ;

struct bit16ind4
        {
        unsigned short i;
        unsigned short j;
        unsigned short k;
        unsigned short l;
        } __attribute__((packed)) ;


int read_sifs_onel(sifs &sifs, NRSMat<REAL> &r, int &itypea, int &itypeb, int &last, REAL &fcore); //returns nblocks read on success, -1 on file end, -2 on error, -3 if 2-el integrals are found

ostream& operator<<(ostream &o, const sifs &s);
ostream& operator<<(ostream &o, const block_header &b);


template<typename I, typename T>
class fourindex_sifs {
private: //at the moment for simplicity forbid some operations, otherwise reference counting on the buffer has to be done
	fourindex_sifs();
	fourindex_sifs(const fourindex_sifs &rhs);
	fourindex_sifs & operator=(const fourindex_sifs &rhs);
protected:
	off64_t offset0;
	matel4stored<I,T> matel;
	REAL *buffer;
	bit8ind4 *lab8;
	uint64_t *lab10;
	int lab10bit;
	int lab10word;
	bit16ind4 *lab16;
	int fd;
	int nn;
	int last;
	int ninteg;
	int iinteg;
	bool first;
	int itypea,itypeb;
	int nblocks;
	int blocksize;
	int maxinteg;
	int ifmt;
	fourindexsymtype symmetry;

	//methods
	void tryread() 
		{
		FREC mark1,mark2;
		block_header_real h;

		//const_cast<fourindex_sifs<I,T> *>(this)->current=NULL;
		if(last && iinteg>=ninteg) return;
		int r=readall(fd,&mark1,sizeof(FREC));
		if(r<=0) laerror("unexpected end of file while reading 2-electron");
		if(r!=sizeof(FREC)) laerror("inconsistent block");
		if(mark1>sizeof(T)*blocksize) laerror("unexpectedly large block 2");
		if(mark1!=readall(fd,buffer,mark1)) laerror("cannot read block 2");
		h.r=buffer[0];
		if(first)
                	{
                	itypea=h.h.itypea;
                	itypeb=h.h.itypeb;
                	first=0;
			if(h.h.ifmt>=1 && sizeof(I)<2) laerror("recompile with appropriate integral index type TWOEL_INDEX");
                	}
        	else
                	{
                	if(itypea!=h.h.itypea) laerror("inconsistent itypea 2");
                	if(itypeb!=h.h.itypeb) laerror("inconsistent itypeb 2");
                	}
        	//cout <<h.h<<endl;
		if(sizeof(FREC)!=readall(fd,&mark2,sizeof(FREC))) laerror("inconsistent block 2");
		if(mark1!=mark2) laerror("inconsistent block 3");
        	++nblocks;
		//check block
		if(h.h.reserved) laerror("reserved should be 0");
	        if(h.h.ibvtyp) laerror("nonzero ibvtyp unsupported");
       	        if(h.h.itypea!=3)  laerror("unsupported itypea");
		if(h.h.ifmt>2) laerror("unsupported ifmt");
	        if(h.h.num > maxinteg) laerror("excessive number of integrals in block 2");
		ninteg=h.h.num;
		last=h.h.last;
		ifmt=h.h.ifmt;
		iinteg=0;
		lab10bit=64;
		lab10word=0;
		lab8 = (bit8ind4 *) &buffer[h.h.lab1-1];
		lab10 = (uint64_t *) &buffer[h.h.lab1-1];
        	lab16 = (bit16ind4 *) &buffer[h.h.lab1-1];
		setupmatel();
		}
	void setupmatel() 
		{
		int ii;
		matel.elem=buffer[iinteg+1];
		switch(ifmt) {
		  case 0: //8-bit packing
			//possible endianity non-portability!!!
			ii = (iinteg&0xfffffffe) + 1 - (iinteg&1);
			memcpy(&matel.index,lab8+ii,4*sizeof(I));
			break;
		  case 2: //16-bit packing
			//!! not tested
			memcpy(&matel.index,lab16+iinteg,4*sizeof(I));
			break;
		  case 1: //10-bit packing //@@@@@@check index order
			matel.index.indiv.i = getnextlab10(lab10,&lab10bit,&lab10word);
			matel.index.indiv.j = getnextlab10(lab10,&lab10bit,&lab10word);
			matel.index.indiv.k = getnextlab10(lab10,&lab10bit,&lab10word);
			matel.index.indiv.l = getnextlab10(lab10,&lab10bit,&lab10word);
			break;
		}
		}
	void next() {++iinteg; if(iinteg>=ninteg) tryread(); else setupmatel(); }
	bool eof() const {return last && iinteg>=ninteg; }


public:
        fourindex_sifs(const sifs &s)
		{
		fd= s.fd;
		offset0 = lseek64(fd,0L,SEEK_CUR);
		nn=s.header.nbas;
		maxinteg=s.infos[4];
		blocksize=s.infos[3];
		if(sizeof(T)!=8) laerror("recompile with a proper element type");
		buffer= new T[blocksize];
		//cout <<"allocated "<<blocksize*sizeof(T)<<endl;
		nblocks=last=iinteg=ninteg=0;
		first=1;
		symmetry=twoelectronrealmullikan;
		}
	~fourindex_sifs() {if(buffer) delete[] buffer;}
	void rewind() {if(offset0!=lseek64(fd,offset0,SEEK_SET)) {perror("seek error"); laerror("cannot seek in fourindex_sifs");} nblocks=iinteg=ninteg=last=0;};
	int size() const {return nn;}
	int typea() const {return itypea;}
	int typeb() const {return itypeb;}
	int blocks() const {return nblocks;}

	//file output
	void put(I i, I j, I k, I l, const T &elem);//@@@to be done
	void flush();//@@@to be done
	

//iterator and permute-iterator are both implemented as poiters to the original class, using private functions of this class
//this is possible, since one instance of this class can have only one active iterator at a time
	
//iterator
        class iterator {
        private:
                fourindex_sifs *base; 
        public:
		iterator() {};
                iterator(fourindex_sifs *p): base(p) {};
                ~iterator() {};
                bool operator!=(const iterator &rhs) const {return base!=rhs.base;} //should only be used for comparison with end()
                iterator &operator++() {if(base) base->next(); if(base->eof()) base=NULL; return *this;} 
		iterator operator++(int) {laerror("postincrement not possible");}
                const matel4stored<I,T> * operator->() const {return &base->matel;}
                const matel4stored<I,T> & operator*() const {return base->matel;}
		bool notNULL() const {return base;}
        };
        iterator begin()  {rewind(); tryread(); if(!eof()) return this; else return NULL;}
        iterator end() const {return iterator(NULL);}


//piterator ... iterate over all allowed permutations; conveniently expressed via the basic iterator which does the block-buffering
        class piterator {
        private:
                fourindex_sifs *base;
		matel4<I,T> my;
		int permindex;
		typename fourindex_sifs::iterator it;

		//private methods
                void setup(void) //make a copy of *it to my with scaled element and anti/permuted indices
                        {
                        if(base->symmetry==undefined_symmetry) laerror("fourindex symmetry has not been set");
                        if(!it.notNULL()) {permindex=0; memset(&my,0,sizeof(my)); return;} //we rely that end() is NULL
                        for(int i=0; i<4; ++i)
                                my.index.packed[i] = it->index.packed[fourindex_permutations[base->symmetry][permindex][i]];
                        my.elem = it->elem * fourindex_permutations[base->symmetry][permindex][4];
                        //redundancy due to possibly equal indices
                        //if the processing of individual term becomes very costly, an alternative would be to screen permutations yielding identical result
			symmetry_faktor(base->symmetry, it->index, my.elem);
                        };
        public:
		piterator() {};
                piterator(fourindex_sifs *p): base(p),permindex(0) {if(p) {it=p->begin(); setup();}};
		piterator(fourindex_sifs &x): base(&x),permindex(0) {it= x.begin(); setup();};
                ~piterator() {};
                bool operator!=(const piterator &rhs) const {return base!=rhs.base;} //should only be used for comparison with end()
                piterator &operator++() {if(++permindex>=fourindex_permnumbers[base->symmetry]) {permindex=0; ++it;} if(it.notNULL()) setup(); else base=NULL; return *this;} 
		piterator operator++(int) {laerror("postincrement not possible");}
                const matel4<I,T> * operator->() const {return &my;}
                const matel4<I,T> & operator*() const {return my;}
		bool end(void) {return !base;}
                bool notend(void) {return base;}
	};
	piterator pbegin() {return piterator(*this);}
	piterator pend() const {return piterator(NULL);} //inefficient, use end() or notend() instead


};


template <class I, class T>
ostream& operator<<(ostream &s, fourindex_sifs<I,T> &x)
                {
                int n;
                n=x.size();
                s << n << '\n';
                typename fourindex_sifs<I,T>::iterator it=x.begin();
                while(it!=x.end())
                        {
                        s << (typename LA_traits_io<I>::IOtype)it->index.indiv.i << ' ' << (typename LA_traits_io<I>::IOtype)it->index.indiv.j<<  ' ' <<(typename LA_traits_io<I>::IOtype)it->index.indiv.k << ' ' << (typename LA_traits_io<I>::IOtype)it->index.indiv.l  << ' ' << (typename LA_traits_io<T>::IOtype) it->elem << '\n';
                        ++it;
                        }
                s << "-1 -1 -1 -1\n";
                return s;
                }





#endif
