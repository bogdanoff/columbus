/*
    Copyright (C) 2008 Jiri Pittner <jiri.pittner@jh-inst.cas.cz> or <jiri@pittnerovi.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <sys/types.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>

#ifdef USE_TRACEBACK
#include "traceback.h"
#endif

#include "options.h"

//LA
#include "vec.h"
#include "mat.h"
#include "smat.h"
#include "nonclass.h"
#include "qsort.h"


#include "basic.h"
#include "gelfand.h"
#include "drt.h"


inline short int abs(const short int i) {return i>=0 ? i : -i;}


int main(int argc, char **argv)
{
#ifdef USE_TRACEBACK
sigtraceback(SIGSEGV,1);
sigtraceback(SIGABRT,1);
sigtraceback(SIGBUS,1);
sigtraceback(SIGFPE,1);
#endif

int f;

cout.setf(ios::fixed);
//cout.setf(ios::scientific);
    cout.precision(12);

REAL threshold=0.;
if(argc>2 && !strcmp(argv[1],"-t"))
        {
        sscanf(argv[2],"%lf",&threshold);
        argc -=2; argv+=2;
        }



//sorts -u the ci vector in slater basis with repeatedly generated slater determinants (from mycipc.x)
//assumes that the same slaters are generated in the same orbital order (no canonicalization done here)
if(argc<6)  laerror("civecconsolidate [-t thr] civecin slaterfilein civecout slaterfileout consolidatelistfile < nelec orbnumshift");

int nelec,orbnumshift=0;

cin >>nelec>>orbnumshift ;

//read ci vector
NRMat<REAL> civec;
f=open(argv[1],O_RDONLY|O_LARGEFILE); if(f<0) perror("cannot open file");
civec.get(f,true);
close(f);
int ncivec=civec.nrows();
lexindex cisize=civec.ncols();

cout <<"number of CI vectors = "<<ncivec<<endl;
cout <<"input CI size = "<<cisize<<endl;

int ff=open(argv[5],O_RDONLY|O_LARGEFILE); //try to open consolidatelistfile
if(ff>=0) //it exists from previous calculation - just process the ci vector
	{
	lexindex newcisize;
	int maxlist;
	if(sizeof(newcisize)!=read(ff,&newcisize,sizeof(newcisize))) laerror("cannot read newcisize");
	if(sizeof(maxlist)!=read(ff,&maxlist,sizeof(maxlist))) laerror("cannot read maxlist");

	cout <<"using an existing consolidatelistfile with newcisize = "<<newcisize<<endl;
        if(threshold>0.) laerror("thresholding in civecconsolidate is not compatible with reuse of consolidatelistfile");

	off64_t begin = lseek64(ff,0L,SEEK_CUR);

	f=open(argv[3],O_CREAT|O_TRUNC|O_WRONLY|O_LARGEFILE,0777);
	if(write(f,&ncivec,sizeof(int))!=sizeof(int)) laerror("cannot write");
	if(write(f,&newcisize,sizeof(int))!=sizeof(int)) laerror("cannot write");
	for(int i=0; i<ncivec; ++i) 
		{
		lseek64(ff,begin,SEEK_SET);
		for(int j=0; j<newcisize; ++j)
			{
			lexindex ii[maxlist];
			REAL sum=0.;
			lexindex nlist;
			if(sizeof(nlist)!=read(ff,&nlist,sizeof(nlist))) laerror("cannot read nlist");
			if(nlist*sizeof(lexindex)!=read(ff,ii,nlist*sizeof(lexindex))) laerror("cannot read list");
			for(int k=0; k<nlist; ++k) sum += civec(i,ii[k]);
			if(write(f,&sum,sizeof(REAL))!=sizeof(REAL)) laerror("cannot write");
			}
		}
	close(f);
	}

else //consolidatelistfile has to be created

{
cout <<"creating a new consolidatelistfile\n";
ff=open(argv[5],O_CREAT|O_TRUNC|O_WRONLY|O_LARGEFILE,0777);
if(ff<0) {perror("consolidatelistfile cannot open"); laerror("io error");}
lexindex dummy=0;
//placeholder for data written later
write(ff,&dummy,sizeof(dummy));
write(ff,&dummy,sizeof(dummy));

//read slaterfile
cout <<"reading slater determinants, no. of electrons = "<<nelec<<endl;
slaterbasis sl1f(cisize,nelec);
f=open(argv[2],O_RDONLY|O_LARGEFILE); if(f<0) perror("cannot open file");
sl1f.get(f,false);
if(sl1f.checkzero()) laerror("malformed slaterfile encountered, perhaps cipc/mcpc was compiled without --assume byterecl");
close(f);


//reorder slater basis and ci vectors to common order
//this is not necessary
//canonicalize(sl1f,civec);

NRVec<SPMatindex> slperm1(cisize);
for(lexindex i=0; i<cisize; ++i) slperm1[i]=i;
slsort_sldetbase=&sl1f;
slsort_permbase=&slperm1[0];
slsort_nelectrons=nelec;
genqsort(0,(int)cisize-1,slsort_slbascmp,slsort_slbasswap);

//apply permutation and merge repeated entries
slaterbasis slnew(cisize,nelec);
NRMat<REAL> civecnew(0.,ncivec,cisize);
int maxlist=1;
lexindex nlist=1;
int alloclist=64;
lexindex *list= new lexindex[alloclist];
unsigned int newcisize=0;
memcpy(slnew[newcisize],sl1f[slperm1[0]],nelec*sizeof(spinorbindex));
for(int j=0; j<ncivec; ++j) civecnew(j,0)=civec(j,slperm1[0]);
list[0] = slperm1[0];
lexindex i=1;
while(i<cisize)
	{
	while(i<cisize && !memcmp(slnew[newcisize],sl1f[slperm1[i]],nelec*sizeof(spinorbindex)))
		{
		list[nlist++] = slperm1[i];
		if(nlist>=alloclist)
			{
			alloclist*=2;
			lexindex *list2 =  new lexindex[alloclist];
			memcpy(list2,list,alloclist/2*sizeof(lexindex));
			delete[] list;
			list=list2;
			}
		for(int j=0; j<ncivec; ++j) civecnew(j,newcisize)+=civec(j,slperm1[i]);
		++i;
		}
	if(i<cisize)
		{
		if(write(ff,&nlist,sizeof(nlist))!=sizeof(nlist)) laerror("cannot write");
		if(write(ff,list,nlist*sizeof(lexindex))!=nlist*sizeof(lexindex)) laerror("cannot write");
		if(nlist>maxlist) maxlist=nlist;
		nlist=1;
		list[0]=slperm1[i];
		++newcisize;
		memcpy(slnew[newcisize],sl1f[slperm1[i]],nelec*sizeof(spinorbindex));
		for(int j=0; j<ncivec; ++j) civecnew(j,newcisize)=civec(j,slperm1[i]);
		++i;
		}
	}
++newcisize;
if(nlist>maxlist) maxlist=nlist;
if(write(ff,&nlist,sizeof(nlist))!=sizeof(nlist)) laerror("cannot write");
if(write(ff,list,nlist*sizeof(lexindex))!=nlist*sizeof(lexindex)) laerror("cannot write");
	
cout <<"Output consolidated CI size (before thresholding) = "<<newcisize<<endl;


//thresholding
NRVec<char> omit(newcisize);
omit.clear();
unsigned int newcisizethr=0;
if(threshold>0.)
    for(int j=0; j<newcisize; ++j)
	{
	++newcisizethr;
	for(int i=0; i<ncivec; ++i) if(abs(civecnew(i,j)) > threshold) goto passed;
	omit[j]=1;
	--newcisizethr;
	passed: ;
	}
else
    newcisizethr=newcisize;

cout <<"Output consolidated CI size (after thresholding) = "<<newcisizethr<<endl;



//write result
f=open(argv[3],O_CREAT|O_TRUNC|O_WRONLY|O_LARGEFILE,0777);
if(write(f,&ncivec,sizeof(int))!=sizeof(int)) laerror("cannot write");
if(write(f,&newcisizethr,sizeof(newcisizethr))!=sizeof(int)) laerror("cannot write");
for(int i=0; i<ncivec; ++i) 
	for(int j=0; j<newcisize; ++j)
            if(!omit[j])
		{
		if(write(f,&civecnew(i,j),sizeof(REAL))!=sizeof(REAL)) laerror("cannot write");
		}
close(f);

f=open(argv[4],O_CREAT|O_TRUNC|O_WRONLY|O_LARGEFILE,0777);
for(int j=0; j<newcisize; ++j)
    if(!omit[j])
	{
	if(orbnumshift)
		{
		for(int i=0; i<nelec;++i) 
			{
			int sgn=slnew(j,i)>0?1:-1;
			slnew(j,i) = sgn* (abs(slnew(j,i))-orbnumshift);
			if(abs(slnew(j,i))-orbnumshift <=0) laerror("wrong orbnumshift specified");
			}
		}
	if(write(f,slnew[j],nelec*sizeof(spinorbindex))!=nelec*sizeof(spinorbindex)) laerror("cannot write");
	}
close(f);

//rewind consolidatelistfile and write newcisize and maxlist
if(threshold>0.) 
	{
	cout <<"consolidatelistfile not reusable with thresholding, removed\n";
	close(ff);
	unlink(argv[5]);
	}
else
	{
	cout <<"consolidatelistfile maxlist = "<<maxlist<<endl;
	lseek64(ff,0L,SEEK_SET);
	if(write(ff,&newcisize,sizeof(newcisize))!=sizeof(newcisize)) laerror("cannot write");
	if(write(ff,&maxlist,sizeof(maxlist))!=sizeof(maxlist)) laerror("cannot write");
	close(ff);
	}
}


}


