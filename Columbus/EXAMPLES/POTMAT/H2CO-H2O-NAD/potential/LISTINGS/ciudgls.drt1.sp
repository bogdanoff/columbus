1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at

    Thomas Mueller
    Central Institute of Applied Mathematics
    Research Centre Juelich
    D-52425 Juelich, Germany
    Internet: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      09/20/00    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 using llenci=          -1
================================================================================
four external integ    0.25 MB location: local disk    
three external inte    0.50 MB location: local disk    
four external integ    0.25 MB location: local disk    
three external inte    0.50 MB location: local disk    
diagonal integrals     0.12 MB location: local disk    
off-diagonal integr    0.53 MB location: local disk    
computed file size in DP units
fil3w:       65534
fil3x:       65534
fil4w:       32767
fil4x:       32767
ofdgint:       16380
diagint:       69615
computed file size in DP units
fil3w:      524272
fil3x:      524272
fil4w:      262136
fil4x:      262136
ofdgint:      131040
diagint:      556920
 nsubmx=           8  lenci=       22512
global arrays:       382704   (              2.92 MB)
vdisk:                    0   (              0.00 MB)
drt:                1023631   (              3.90 MB)
================================================================================
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core           99999999 DP per process
 CIUDG version 5.9.7 ( 5-Oct-2004)
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 0,
  GSET = 0,
   DAVCOR =10,
  NCOREL = 12
  NROOT = 3
  IVMODE = 3
  NBKITR = 1
  NVBKMN = 3
  NVBKMX = 8
  RTOLBK = 1e-4,1e-4,1e-4,
  NITER = 90
  NVCIMN = 5
  NVCIMX = 8
  RTOLCI = 1e-4,1e-4,1e-4,
  IDEN  = 1
  CSFPRN = 10,
 /&end
 ------------------------------------------------------------------------

 ** list of control variables **
 nrfitr =   30      nvrfmx =    8      nvrfmn =    5
 lvlprt =    0      nroot  =    3      noldv  =    0      noldhv =    0
 nunitv =    3      ntype  =    0      nbkitr =    1      niter  =   90
 ivmode =    3      vout   =    0      istrt  =    0      iortls =    0
 nvbkmx =    8      ibktv  =   -1      ibkthv =   -1      frcsub =    0
 nvcimx =    8      icitv  =   -1      icithv =   -1      maxseg =    4
 iden   =    1      itran  =    0      froot  =    0      rtmode =    0
 ftcalc =    1      lrtshift=1.0000    ncouple=    1      skipso  =   F
 ncorel =   12      csfprn  =  10      ctol   = 1.00E-02  davcor  =  10
 smalld = 1.00E-03

 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-04    1.000E-04
    2        1.000E-04    1.000E-04
    3        1.000E-04    1.000E-04
 
 units and filenames:
    1: ( 6)    ciudgls                                                     
    2: ( 5)    ciudgin                                                     
    3: ( 7)    ciudgsm                                                     
    4: ( 4)    cihdiag                                                     
    5: ( 3)    cihpseu                                                     
    6: (10)    cihvfl                                                      
    7: (11)    civfl                                                       
    8: (12)    diagint                                                     
    9: (13)    ofdgint                                                     
   10: (17)    cidrtfl                                                     
   11: (20)    ciftdfl                                                     
   12: (21)    ciftofl                                                     
   13: (24)    ciftifl                                                     
   14: ( 8)    ciflind                                                     
   15: (15)    civin                                                       
   16: (16)    civout                                                      
   18: (18)    d1fl                                                        
   20: (53)    cifvfl                                                      
   21: (25)    ciscr4                                                      
   22: (41)    ciftotd                                                     
   23: (42)    flacpfd                                                     
   26: (26)    ciscr5                                                      
   27: (47)    restart                                                     
   28: (48)    civout                                                      
   29: (49)    cirefv                                                      
   31: (31)    fil4w                                                       
   32: (32)    fil4x                                                       
   33: (33)    fil3w                                                       
   34: (34)    fil3x                                                       
   36: (36)    aoints                                                      
   37: (37)    aoints2                                                     
   38: (38)    drtfil                                                      
   39: (50)    cisrtif                                                     
   42: (50)    mocoef                                                      
   45: (52)    nocoef_ci                                                   
   46: (53)    civfl_restart                                               
 ------------------------------------------------------------------------

 workspace allocation information: lcore= 100000000 mem1=         0 ifirst=         1

 integral file titles:
 Hermit Integral Program : SIFS version  wizard2.itc.unvie 11:58:27.585 04-Feb-08
  cidrt_title DRT#1                                                              
  title                                                                          
 mofmt: formatted orbitals label=morbl   wizard2.itc.unvie 11:58:36.899 04-Feb-08
 SIFS file created by program tran.      wizard2.itc.unvie 11:58:37.076 04-Feb-08

 core energy values from the integral file:
 energy( 1)=  3.090529302074E+01, ietype=   -1,    core energy of type: Nuc.Rep.

 total core repulsion energy =  3.090529302074E+01

 drt header information:
  cidrt_title DRT#1                                                              
 spnorb, spnodd, lxyzir,hmult F F           0           0           0
           0
 nmot  =    32 niot  =    10 nfct  =     0 nfvt  =     0
 nrow  =    79 nsym  =     1 ssym  =     1 lenbuf=  1600
 nwalk,xbar:       1344      336     1008        0        0
 nvalwt,nvalw:     1344      336     1008        0        0
 ncsft:           22512
 total number of valid internal walks:    1344
 nvalz,nvaly,nvalx,nvalw =      336    1008       0       0

 cisrt info file parameters:
 file number  12 blocksize   4095
 mxbld   4095
 nd4ext,nd2ext,nd0ext   506   440   110
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int    37191    60060    41745    14520     1980        0        0        0
 minbl4,minbl3,maxbl2   756   756   759
 maxbuf 32767
 number of external orbitals per symmetry block:  22
 nmsym   1 number of internal orbitals  10

 formula file title:
 Hermit Integral Program : SIFS version  wizard2.itc.unvie 11:58:27.585 04-Feb-08
  cidrt_title DRT#1                                                              
  title                                                                          
 mofmt: formatted orbitals label=morbl   wizard2.itc.unvie 11:58:36.899 04-Feb-08
 SIFS file created by program tran.      wizard2.itc.unvie 11:58:37.076 04-Feb-08
  cidrt_title DRT#1                                                              
 file nmb.  24 block size     0
 pthz,pthy,pthx,pthw:   336  1008     0     0 total internal walks:    1344
 maxlp3,n3xtlp,n2xtlp,n1xtlp,n0xtlp,n2lp,n1lp,n0lp     0     0     0     0
 orbsym(*)= 1 1 1 1 1 1 1 1 1 1

 setref:      336 references kept,
                0 references were marked as invalid, out of
              336 total.
 limcnvrt: found         336  valid internal walksout of          336 
  walks (skipping trailing invalids)
  ... adding          336  segmentation marks segtype=           1
 limcnvrt: found        1008  valid internal walksout of         1008 
  walks (skipping trailing invalids)
  ... adding         1008  segmentation marks segtype=           2
 limcnvrt: found           0  valid internal walksout of            0 
  walks (skipping trailing invalids)
  ... adding            0  segmentation marks segtype=           3
 limcnvrt: found           0  valid internal walksout of            0 
  walks (skipping trailing invalids)
  ... adding            0  segmentation marks segtype=           4

 number of external paths / symmetry
 vertex x     231
 vertex w     253

 lprune: l(*,*,*) pruned with nwalk=     336 nvalwt=     336 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    1008 nvalwt=    1008 nprune=       0



                   segmentation summary for type diagonal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1         336|       336|         0|       336|         0|         1|
 -------------------------------------------------------------------------------
  Y 2        1008|     22176|       336|      1008|       336|         2|
 -------------------------------------------------------------------------------
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS
----------------------------------------------------------------------------------------------------
     1  2   1    11      one-ext yz   1X  2 1    1008     336      22176        336    1008     336
     2  1   1     1      allint zz    OX  1 1     336     336        336        336     336     336
     3  2   2     5      0ex2ex yy    OX  2 2    1008    1008      22176      22176    1008    1008
     4  1   1    75      dg-024ext z  DG  1 1     336     336        336        336     336     336
     5  2   2    45      4exdg024 y   DG  2 2    1008    1008      22176      22176    1008    1008
----------------------------------------------------------------------------------------------------
 DIAGEL COUNTS: DG0X,DG2X,DG4X=       62916        9184        1008
 diagonal elements written to file   4

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      3 vectors will be written to unit 11 beginning with logical record   1

            3 vectors will be created
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     16268 2x:         0 4x:         0
All internal counts: zz :     59780 yy:         0 xx:         0 ww:         0
One-external counts: yz :         0 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:         0    task #   2:     49275    task #   3:         0    task #   4:      8071
task #   5:         0    task #
 reference space has dimension     336

    root           eigenvalues
    ----           ------------
       1        -114.2395235871
       2        -114.0854014958
       3        -113.8883020695

 strefv generated    3 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt=         336

         vector  1 from unit 11 written to unit 49 filename cirefv                                                      
 ufvoutnew: ... writing  recamt=         336

         vector  2 from unit 11 written to unit 49 filename cirefv                                                      
 ufvoutnew: ... writing  recamt=         336

         vector  3 from unit 11 written to unit 49 filename cirefv                                                      

 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=   336)...
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:             22512
 number of initial trial vectors:                         3
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               8
 number of roots to converge:                             3
 number of iterations:                                    1
 residual norm convergence criteria:               0.000100  0.000100  0.000100

          starting bk iteration   1

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:         0 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:         0    task #   4:      8071
task #   5:      3765    task #
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:         0 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:         0    task #   4:      8071
task #   5:      3765    task #
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:         0 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:         0    task #   4:      8071
task #   5:      3765    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     1.00000000     0.00000000     0.00000000
 ref:   2     0.00000000    -1.00000000     0.00000000
 ref:   3     0.00000000     0.00000000     1.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -114.2395235871  2.2737E-13  8.8797E-02  4.9247E-01  1.0000E-04
 mr-sdci #  1  2   -114.0854014958  1.4211E-13  0.0000E+00  5.1060E-01  1.0000E-04
 mr-sdci #  1  3   -113.8883020695  5.6843E-14  0.0000E+00  4.9761E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.10   0.05   0.00   0.10         0.    0.0000
    2    1    0   0.06   0.03   0.00   0.06         0.    0.0000
    3    5    0   0.00   0.00   0.00   0.00         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1900s 
time spent in multnx:                   0.1900s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1000s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.5800s 

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -114.2395235871  2.2737E-13  8.8797E-02  4.9247E-01  1.0000E-04
 mr-sdci #  1  2   -114.0854014958  1.4211E-13  0.0000E+00  5.1060E-01  1.0000E-04
 mr-sdci #  1  3   -113.8883020695  5.6843E-14  0.0000E+00  4.9761E-01  1.0000E-04
 
 expansion vectors are not transformed.
 matrix-vector products are not transformed.

    4 expansion eigenvectors written to unit nvfile (= 11)
    3 matrix-vector products written to unit nhvfil (= 10)

 from bk iterations: iconv=   1

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:             22512
 number of initial trial vectors:                         4
 number of initial matrix-vector products:                3
 maximum dimension of the subspace vectors:               8
 number of roots to converge:                             3
 number of iterations:                                   90
 residual norm convergence criteria:               0.000100  0.000100  0.000100

          starting ci iteration   1

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1    -0.98585366    -0.00000004     0.00000007    -0.16760837
 ref:   2    -0.00000003     1.00000000     0.00000000    -0.00000002
 ref:   3    -0.00000006     0.00000000    -1.00000000    -0.00000007

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -114.3171146392  7.7591E-02  6.6075E-03  1.1270E-01  1.0000E-04
 mr-sdci #  1  2   -114.0854014958 -1.9895E-13  0.0000E+00  5.1060E-01  1.0000E-04
 mr-sdci #  1  3   -113.8883020695  2.8422E-14  0.0000E+00  4.9761E-01  1.0000E-04
 mr-sdci #  1  4   -111.5551366514  1.4246E+02  0.0000E+00  1.1201E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.10   0.06   0.00   0.10         0.    0.0000
    2    1    0   0.05   0.04   0.00   0.05         0.    0.0000
    3    5    0   0.44   0.18   0.00   0.44         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.02   0.02   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6200s 
time spent in multnx:                   0.6200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6300s 

          starting ci iteration   2

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1    -0.98390836    -0.00000005     0.00000026     0.11202965    -0.13918941
 ref:   2    -0.00000004     1.00000000     0.00000000     0.00000003    -0.00000001
 ref:   3    -0.00000019     0.00000000    -1.00000000     0.00000083     0.00000017

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1   -114.3209830725  3.8684E-03  7.4473E-04  4.2795E-02  1.0000E-04
 mr-sdci #  2  2   -114.0854014958 -5.6843E-14  0.0000E+00  5.1060E-01  1.0000E-04
 mr-sdci #  2  3   -113.8883020695  1.1653E-12  0.0000E+00  4.9761E-01  1.0000E-04
 mr-sdci #  2  4   -112.3767261973  8.2159E-01  0.0000E+00  1.5032E+00  1.0000E-04
 mr-sdci #  2  5   -111.3758646210  1.4228E+02  0.0000E+00  1.1512E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.10   0.05   0.00   0.10         0.    0.0000
    2    1    0   0.06   0.05   0.00   0.06         0.    0.0000
    3    5    0   0.46   0.19   0.00   0.46         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.02   0.02   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6500s 
time spent in multnx:                   0.6500s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3200s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6600s 

          starting ci iteration   3

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.98321693    -0.00000005     0.00000043    -0.12224501    -0.00114496    -0.13542271
 ref:   2    -0.00000004     1.00000000     0.00000000    -0.00000004     0.00000001    -0.00000001
 ref:   3    -0.00000029     0.00000000    -1.00000000    -0.00000170    -0.00000041     0.00000042

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1   -114.3216166650  6.3359E-04  7.5238E-05  1.3738E-02  1.0000E-04
 mr-sdci #  3  2   -114.0854014958  5.6843E-14  0.0000E+00  5.1060E-01  1.0000E-04
 mr-sdci #  3  3   -113.8883020695  3.2969E-12  0.0000E+00  4.9761E-01  1.0000E-04
 mr-sdci #  3  4   -112.5903690758  2.1364E-01  0.0000E+00  1.2092E+00  1.0000E-04
 mr-sdci #  3  5   -111.9504417208  5.7458E-01  0.0000E+00  1.6854E+00  1.0000E-04
 mr-sdci #  3  6   -111.2561538402  1.4216E+02  0.0000E+00  1.0451E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.10   0.06   0.00   0.10         0.    0.0000
    2    1    0   0.06   0.04   0.00   0.06         0.    0.0000
    3    5    0   0.43   0.19   0.00   0.43         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6200s 
time spent in multnx:                   0.6200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6300s 

          starting ci iteration   4

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.98298959    -0.00000006     0.00000059    -0.10292861    -0.03534350     0.11585163     0.09201305
 ref:   2    -0.00000005     1.00000000     0.00000000    -0.00000013     0.00000002    -0.00000004     0.00000003
 ref:   3    -0.00000032     0.00000000    -1.00000000    -0.00000328    -0.00000040    -0.00000059    -0.00000012

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1   -114.3216780996  6.1435E-05  1.1790E-05  5.5077E-03  1.0000E-04
 mr-sdci #  4  2   -114.0854014958 -2.8422E-14  0.0000E+00  5.1060E-01  1.0000E-04
 mr-sdci #  4  3   -113.8883020696  6.2528E-12  0.0000E+00  4.9761E-01  1.0000E-04
 mr-sdci #  4  4   -112.9843923692  3.9402E-01  0.0000E+00  1.0710E+00  1.0000E-04
 mr-sdci #  4  5   -111.9973390021  4.6897E-02  0.0000E+00  1.6470E+00  1.0000E-04
 mr-sdci #  4  6   -111.4543017413  1.9815E-01  0.0000E+00  1.3538E+00  1.0000E-04
 mr-sdci #  4  7   -111.1800069659  1.4209E+02  0.0000E+00  1.2968E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.10   0.05   0.00   0.10         0.    0.0000
    2    1    0   0.07   0.04   0.00   0.07         0.    0.0000
    3    5    0   0.44   0.20   0.00   0.44         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6300s 
time spent in multnx:                   0.6300s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3000s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.6600s 

          starting ci iteration   5

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.98290724     0.00000006    -0.00000067    -0.09670517     0.00366746    -0.11723752    -0.09959325    -0.02940349
 ref:   2    -0.00000005    -1.00000000     0.00000000    -0.00000008    -0.00000006    -0.00000001     0.00000002    -0.00000008
 ref:   3    -0.00000033     0.00000000     1.00000000    -0.00000417     0.00000016     0.00000021     0.00000045    -0.00000041

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1   -114.3216875469  9.4473E-06  1.1493E-06  1.7956E-03  1.0000E-04
 mr-sdci #  5  2   -114.0854014958 -2.8422E-14  0.0000E+00  5.1060E-01  1.0000E-04
 mr-sdci #  5  3   -113.8883020696  2.5295E-12  0.0000E+00  4.9761E-01  1.0000E-04
 mr-sdci #  5  4   -113.1909265799  2.0653E-01  0.0000E+00  8.7201E-01  1.0000E-04
 mr-sdci #  5  5   -112.0872517514  8.9913E-02  0.0000E+00  1.6653E+00  1.0000E-04
 mr-sdci #  5  6   -111.5638296183  1.0953E-01  0.0000E+00  1.2192E+00  1.0000E-04
 mr-sdci #  5  7   -111.2098770950  2.9870E-02  0.0000E+00  1.1781E+00  1.0000E-04
 mr-sdci #  5  8   -111.0970253132  1.4200E+02  0.0000E+00  1.7173E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.09   0.06   0.00   0.09         0.    0.0000
    2    1    0   0.06   0.04   0.00   0.06         0.    0.0000
    3    5    0   0.45   0.20   0.00   0.45         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.03   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.030000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6400s 
time spent in multnx:                   0.6300s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3200s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6700s 

          starting ci iteration   6

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.98288653    -0.00000006     0.00000075     0.08951827    -0.02107290    -0.03241583
 ref:   2    -0.00000005     1.00000000     0.00000000     0.00000007     0.00000004    -0.00000003
 ref:   3    -0.00000033     0.00000000    -1.00000000     0.00000534    -0.00000030     0.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  6  1   -114.3216885269  9.7995E-07  2.3571E-07  7.5687E-04  1.0000E-04
 mr-sdci #  6  2   -114.0854014958  1.1369E-13  0.0000E+00  5.1060E-01  1.0000E-04
 mr-sdci #  6  3   -113.8883020696  3.4390E-12  0.0000E+00  4.9761E-01  1.0000E-04
 mr-sdci #  6  4   -113.3442609754  1.5333E-01  0.0000E+00  7.5253E-01  1.0000E-04
 mr-sdci #  6  5   -112.1671561483  7.9904E-02  0.0000E+00  1.6438E+00  1.0000E-04
 mr-sdci #  6  6   -111.4587289813 -1.0510E-01  0.0000E+00  1.8180E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.09   0.04   0.00   0.09         0.    0.0000
    2    1    0   0.06   0.05   0.00   0.06         0.    0.0000
    3    5    0   0.43   0.18   0.00   0.43         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6100s 
time spent in multnx:                   0.6100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.2900s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6200s 

          starting ci iteration   7

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.98289164    -0.00000006     0.00000076     0.07529164    -0.06715733     0.02205210     0.00007160
 ref:   2    -0.00000005     1.00000000     0.00000000    -0.00000014    -0.00000026     0.00000019    -0.00000013
 ref:   3    -0.00000033     0.00000000    -1.00000000     0.00000643    -0.00000030    -0.00000022     0.00000004

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  7  1   -114.3216887346  2.0772E-07  3.5852E-08  2.9709E-04  1.0000E-04
 mr-sdci #  7  2   -114.0854014958  1.7053E-13  0.0000E+00  5.1060E-01  1.0000E-04
 mr-sdci #  7  3   -113.8883020696  4.5475E-12  0.0000E+00  4.9761E-01  1.0000E-04
 mr-sdci #  7  4   -113.4165732771  7.2312E-02  0.0000E+00  7.1038E-01  1.0000E-04
 mr-sdci #  7  5   -112.4706230554  3.0347E-01  0.0000E+00  1.2872E+00  1.0000E-04
 mr-sdci #  7  6   -112.0046777006  5.4595E-01  0.0000E+00  1.6093E+00  1.0000E-04
 mr-sdci #  7  7   -110.9317417746 -2.7814E-01  0.0000E+00  1.8879E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.10   0.04   0.00   0.09         0.    0.0000
    2    1    0   0.06   0.05   0.00   0.06         0.    0.0000
    3    5    0   0.46   0.20   0.00   0.46         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.02   0.02   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6400s 
time spent in multnx:                   0.6300s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6600s 

          starting ci iteration   8

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.98288931    -0.00000004     0.00000081     0.06827065    -0.06380088    -0.00490264    -0.04103309     0.03229612
 ref:   2    -0.00000005     1.00000000     0.00000000    -0.00000048    -0.00000056    -0.00000018    -0.00000004    -0.00000016
 ref:   3    -0.00000033     0.00000000    -1.00000000     0.00000746    -0.00000040     0.00000031    -0.00000024     0.00000026

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  8  1   -114.3216887637  2.9126E-08  4.4978E-09  1.0930E-04  1.0000E-04
 mr-sdci #  8  2   -114.0854014958  3.4106E-13  0.0000E+00  5.1060E-01  1.0000E-04
 mr-sdci #  8  3   -113.8883020696  3.9790E-12  0.0000E+00  4.9761E-01  1.0000E-04
 mr-sdci #  8  4   -113.4689416717  5.2368E-02  0.0000E+00  6.3214E-01  1.0000E-04
 mr-sdci #  8  5   -112.8653246910  3.9470E-01  0.0000E+00  9.6659E-01  1.0000E-04
 mr-sdci #  8  6   -112.0364028895  3.1725E-02  0.0000E+00  1.6289E+00  1.0000E-04
 mr-sdci #  8  7   -111.1987419865  2.6700E-01  0.0000E+00  1.7179E+00  1.0000E-04
 mr-sdci #  8  8   -110.7066036589 -3.9042E-01  0.0000E+00  1.6774E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.10   0.05   0.00   0.10         0.    0.0000
    2    1    0   0.05   0.04   0.00   0.05         0.    0.0000
    3    5    0   0.45   0.21   0.00   0.45         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.030000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6300s 
time spent in multnx:                   0.6300s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3200s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6600s 

          starting ci iteration   9

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.98289213    -0.00000010    -0.00000068    -0.04499608     0.09463818    -0.04163422
 ref:   2     0.00000005     1.00000000     0.00000000     0.00000109     0.00000101    -0.00000032
 ref:   3     0.00000033     0.00000000     1.00000000    -0.00000903     0.00000024     0.00000086

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  9  1   -114.3216887676  3.8209E-09  0.0000E+00  5.0955E-05  1.0000E-04
 mr-sdci #  9  2   -114.0854014958  1.5632E-12  9.7674E-02  5.1060E-01  1.0000E-04
 mr-sdci #  9  3   -113.8883020696  8.8107E-12  0.0000E+00  4.9761E-01  1.0000E-04
 mr-sdci #  9  4   -113.5116380317  4.2696E-02  0.0000E+00  6.1544E-01  1.0000E-04
 mr-sdci #  9  5   -113.0876455983  2.2232E-01  0.0000E+00  7.9500E-01  1.0000E-04
 mr-sdci #  9  6   -111.4068561745 -6.2955E-01  0.0000E+00  1.5146E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.10   0.06   0.00   0.10         0.    0.0000
    2    1    0   0.06   0.04   0.00   0.06         0.    0.0000
    3    5    0   0.43   0.19   0.00   0.43         0.    0.0000
    4   75    0   0.02   0.00   0.00   0.01         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6300s 
time spent in multnx:                   0.6200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6400s 

          starting ci iteration  10

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.98289213    -0.00000008    -0.00000068     0.04499608     0.09463818    -0.00000366    -0.04163422
 ref:   2    -0.00000005     0.98399976     0.00100968    -0.00000116     0.00000066    -0.17816690     0.00001501
 ref:   3    -0.00000033    -0.00091874     0.99999940     0.00000903     0.00000024     0.00059292     0.00000081

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 10  1   -114.3216887676  1.7053E-13  0.0000E+00  5.0955E-05  1.0000E-04
 mr-sdci # 10  2   -114.1712468092  8.5845E-02  7.0389E-03  1.1404E-01  1.0000E-04
 mr-sdci # 10  3   -113.8883026820  6.1243E-07  0.0000E+00  4.9760E-01  1.0000E-04
 mr-sdci # 10  4   -113.5116380317  3.1264E-13  0.0000E+00  6.1544E-01  1.0000E-04
 mr-sdci # 10  5   -113.0876455983  9.3792E-13  0.0000E+00  7.9500E-01  1.0000E-04
 mr-sdci # 10  6   -111.4669082704  6.0052E-02  0.0000E+00  1.1264E+00  1.0000E-04
 mr-sdci # 10  7   -111.4068561740  2.0811E-01  0.0000E+00  1.5146E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.10   0.05   0.00   0.10         0.    0.0000
    2    1    0   0.06   0.04   0.00   0.06         0.    0.0000
    3    5    0   0.46   0.18   0.00   0.46         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.02   0.02   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6400s 
time spent in multnx:                   0.6400s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.2900s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.6700s 

          starting ci iteration  11

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.98289213     0.00000003     0.00000068     0.04499608    -0.09463818    -0.00000482    -0.04163422    -0.00000039
 ref:   2    -0.00000005     0.98144980    -0.00123351    -0.00000069    -0.00000285     0.10707051    -0.00000642     0.15903042
 ref:   3    -0.00000033    -0.00108786    -0.99999890     0.00000903    -0.00000022    -0.00090766     0.00000088    -0.00043163

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 11  1   -114.3216887676 -1.4211E-13  0.0000E+00  5.0955E-05  1.0000E-04
 mr-sdci # 11  2   -114.1758681747  4.6214E-03  1.2714E-03  5.2305E-02  1.0000E-04
 mr-sdci # 11  3   -113.8883033270  6.4504E-07  0.0000E+00  4.9760E-01  1.0000E-04
 mr-sdci # 11  4   -113.5116380317  8.5834E-12  0.0000E+00  6.1544E-01  1.0000E-04
 mr-sdci # 11  5   -113.0876455985  2.0225E-10  0.0000E+00  7.9500E-01  1.0000E-04
 mr-sdci # 11  6   -112.5233793478  1.0565E+00  0.0000E+00  1.5092E+00  1.0000E-04
 mr-sdci # 11  7   -111.4068561745  4.8368E-10  0.0000E+00  1.5146E+00  1.0000E-04
 mr-sdci # 11  8   -111.3478709751  6.4127E-01  0.0000E+00  1.1578E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.11   0.05   0.00   0.11         0.    0.0000
    2    1    0   0.07   0.04   0.00   0.07         0.    0.0000
    3    5    0   0.43   0.16   0.00   0.43         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.03   0.02   0.00   0.03         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6400s 
time spent in multnx:                   0.6400s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.2700s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6600s 

          starting ci iteration  12

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.98289213    -0.00000031    -0.00000065    -0.04499608     0.09463818    -0.00002098
 ref:   2    -0.00000005     0.98044272     0.00158275     0.00000157     0.00000059    -0.05471711
 ref:   3    -0.00000033    -0.00133164     0.99999644    -0.00000910     0.00000034     0.00208324

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 12  1   -114.3216887676  0.0000E+00  0.0000E+00  5.0955E-05  1.0000E-04
 mr-sdci # 12  2   -114.1768591305  9.9096E-04  4.8132E-04  2.7509E-02  1.0000E-04
 mr-sdci # 12  3   -113.8883093401  6.0131E-06  0.0000E+00  4.9760E-01  1.0000E-04
 mr-sdci # 12  4   -113.5116380318  9.9419E-11  0.0000E+00  6.1544E-01  1.0000E-04
 mr-sdci # 12  5   -113.0876455993  7.6713E-10  0.0000E+00  7.9500E-01  1.0000E-04
 mr-sdci # 12  6   -112.3413464284 -1.8203E-01  0.0000E+00  1.6894E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.13   0.06   0.00   0.13         0.    0.0000
    2    1    0   0.07   0.05   0.00   0.07         0.    0.0000
    3    5    0   0.48   0.20   0.00   0.48         0.    0.0000
    4   75    0   0.01   0.00   0.00   0.01         0.    0.0000
    5   45    0   0.02   0.02   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.7100s 
time spent in multnx:                   0.7100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3300s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.7100s 

          starting ci iteration  13

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.98289213     0.00000078     0.00000083     0.04499608    -0.00002649    -0.09463819     0.00007086
 ref:   2    -0.00000004     0.97939129    -0.00204259     0.00001621     0.06540777     0.00002142     0.03037857
 ref:   3    -0.00000033    -0.00149025    -0.99997885     0.00000718    -0.00624494    -0.00000180    -0.00058192

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 13  1   -114.3216887676  2.8422E-14  0.0000E+00  5.0955E-05  1.0000E-04
 mr-sdci # 13  2   -114.1773056189  4.4649E-04  1.3304E-04  1.6067E-02  1.0000E-04
 mr-sdci # 13  3   -113.8883198437  1.0504E-05  0.0000E+00  4.9759E-01  1.0000E-04
 mr-sdci # 13  4   -113.5116380375  5.6727E-09  0.0000E+00  6.1544E-01  1.0000E-04
 mr-sdci # 13  5   -113.4319833127  3.4434E-01  0.0000E+00  9.7950E-01  1.0000E-04
 mr-sdci # 13  6   -113.0876455635  7.4630E-01  0.0000E+00  7.9500E-01  1.0000E-04
 mr-sdci # 13  7   -111.6690319458  2.6218E-01  0.0000E+00  1.7520E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.10   0.06   0.00   0.10         0.    0.0000
    2    1    0   0.06   0.05   0.00   0.06         0.    0.0000
    3    5    0   0.48   0.21   0.00   0.48         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.009999
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6600s 
time spent in multnx:                   0.6600s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3300s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.6800s 

          starting ci iteration  14

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.98289213     0.00000277     0.00000151    -0.00009785    -0.04499607     0.09463777     0.00021046    -0.00063981
 ref:   2    -0.00000003    -0.97760741     0.00405951    -0.11866395    -0.00002378    -0.00001315     0.04902271    -0.09458201
 ref:   3    -0.00000033     0.00158520     0.99979682     0.02007413    -0.00000645    -0.00000210    -0.00055344     0.00002167

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 14  1   -114.3216887676  5.6843E-14  0.0000E+00  5.0954E-05  1.0000E-04
 mr-sdci # 14  2   -114.1774248444  1.1923E-04  2.2921E-05  7.1126E-03  1.0000E-04
 mr-sdci # 14  3   -113.8883567195  3.6876E-05  0.0000E+00  4.9745E-01  1.0000E-04
 mr-sdci # 14  4   -113.7506977831  2.3906E-01  0.0000E+00  4.8622E-01  1.0000E-04
 mr-sdci # 14  5   -113.5116380370  7.9655E-02  0.0000E+00  6.1544E-01  1.0000E-04
 mr-sdci # 14  6   -113.0876464355  8.7207E-07  0.0000E+00  7.9500E-01  1.0000E-04
 mr-sdci # 14  7   -111.6931185013  2.4087E-02  0.0000E+00  1.7096E+00  1.0000E-04
 mr-sdci # 14  8   -111.1145047967 -2.3337E-01  0.0000E+00  1.2375E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.11   0.06   0.00   0.11         0.    0.0000
    2    1    0   0.06   0.05   0.00   0.06         0.    0.0000
    3    5    0   0.49   0.23   0.00   0.49         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6800s 
time spent in multnx:                   0.6800s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3500s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.7000s 

          starting ci iteration  15

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.98289213    -0.00002309    -0.00007478    -0.00136188     0.04498607    -0.00645660
 ref:   2     0.00000004    -0.97758773     0.00685065     0.09733174    -0.00034998    -0.11527194
 ref:   3    -0.00000034     0.00163383     0.99860431    -0.05278393     0.00001837     0.00023904

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 15  1   -114.3216887676  2.1316E-12  0.0000E+00  5.0893E-05  1.0000E-04
 mr-sdci # 15  2   -114.1774497333  2.4889E-05  7.8956E-06  3.9033E-03  1.0000E-04
 mr-sdci # 15  3   -113.8884570298  1.0031E-04  0.0000E+00  4.9699E-01  1.0000E-04
 mr-sdci # 15  4   -113.8328227330  8.2125E-02  0.0000E+00  3.9805E-01  1.0000E-04
 mr-sdci # 15  5   -113.5116414590  3.4221E-06  0.0000E+00  6.1551E-01  1.0000E-04
 mr-sdci # 15  6   -112.3443761065 -7.4327E-01  0.0000E+00  1.3151E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.11   0.05   0.00   0.11         0.    0.0000
    2    1    0   0.07   0.05   0.00   0.07         0.    0.0000
    3    5    0   0.46   0.19   0.00   0.46         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6700s 
time spent in multnx:                   0.6700s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6800s 

          starting ci iteration  16

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.98289181    -0.00005588    -0.00111436     0.00559548     0.04442278    -0.02565754     0.04257443
 ref:   2     0.00000022     0.97747375    -0.01969038     0.09346337     0.00081743     0.08297233     0.09000381
 ref:   3    -0.00000029    -0.00165839    -0.98206813    -0.18851517     0.00000918     0.00057695    -0.00076208

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 16  1   -114.3216887677  9.5156E-11  0.0000E+00  4.8299E-05  1.0000E-04
 mr-sdci # 16  2   -114.1774549664  5.2331E-06  6.1160E-06  2.3967E-03  1.0000E-04
 mr-sdci # 16  3   -113.8889174710  4.6044E-04  0.0000E+00  4.9123E-01  1.0000E-04
 mr-sdci # 16  4   -113.8716348420  3.8812E-02  0.0000E+00  3.4851E-01  1.0000E-04
 mr-sdci # 16  5   -113.5118232037  1.8174E-04  0.0000E+00  6.1924E-01  1.0000E-04
 mr-sdci # 16  6   -112.8245839822  4.8021E-01  0.0000E+00  1.0581E+00  1.0000E-04
 mr-sdci # 16  7   -111.5774519555 -1.1567E-01  0.0000E+00  1.4339E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.09   0.04   0.00   0.09         0.    0.0000
    2    1    0   0.06   0.05   0.00   0.06         0.    0.0000
    3    5    0   0.47   0.20   0.00   0.47         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.02   0.02   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.020000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6400s 
time spent in multnx:                   0.6400s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6600s 

          starting ci iteration  17

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.98289111     0.00001109     0.00107426     0.00091858     0.04293419     0.01490324    -0.08223654     0.00296521
 ref:   2     0.00000003     0.97742851    -0.07249972    -0.05993345    -0.00066671     0.08228409     0.00764071    -0.09748511
 ref:   3    -0.00000034    -0.00166884    -0.65117802     0.75892202     0.00001666     0.00071482     0.00020466     0.00081218

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 17  1   -114.3216887679  2.1177E-10  0.0000E+00  4.1635E-05  1.0000E-04
 mr-sdci # 17  2   -114.1774567863  1.8200E-06  1.1851E-06  1.4476E-03  1.0000E-04
 mr-sdci # 17  3   -113.8921656303  3.2482E-03  0.0000E+00  3.9414E-01  1.0000E-04
 mr-sdci # 17  4   -113.8854606435  1.3826E-02  0.0000E+00  4.3339E-01  1.0000E-04
 mr-sdci # 17  5   -113.5123177920  4.9459E-04  0.0000E+00  6.2915E-01  1.0000E-04
 mr-sdci # 17  6   -112.9691722755  1.4459E-01  0.0000E+00  9.1424E-01  1.0000E-04
 mr-sdci # 17  7   -112.3716221968  7.9417E-01  0.0000E+00  1.3897E+00  1.0000E-04
 mr-sdci # 17  8   -111.2058487790  9.1344E-02  0.0000E+00  1.3151E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.10   0.06   0.00   0.10         0.    0.0000
    2    1    0   0.06   0.03   0.00   0.06         0.    0.0000
    3    5    0   0.47   0.21   0.00   0.47         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.030001
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6500s 
time spent in multnx:                   0.6500s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6800s 

          starting ci iteration  18

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.98289097     0.00001784    -0.00235318    -0.00074691     0.04272112    -0.05349490
 ref:   2    -0.00000001    -0.97742598    -0.08784938    -0.02609438    -0.00042936     0.03928892
 ref:   3    -0.00000034     0.00167211    -0.30276712     0.95306219     0.00000839    -0.00022830

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 18  1   -114.3216887679  1.2136E-11  0.0000E+00  4.1165E-05  1.0000E-04
 mr-sdci # 18  2   -114.1774573281  5.4174E-07  6.5153E-07  8.4906E-04  1.0000E-04
 mr-sdci # 18  3   -113.8987811475  6.6155E-03  0.0000E+00  3.0272E-01  1.0000E-04
 mr-sdci # 18  4   -113.8872465729  1.7859E-03  0.0000E+00  4.8470E-01  1.0000E-04
 mr-sdci # 18  5   -113.5123365599  1.8768E-05  0.0000E+00  6.3062E-01  1.0000E-04
 mr-sdci # 18  6   -112.3381264572 -6.3105E-01  0.0000E+00  1.6293E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.11   0.08   0.00   0.11         0.    0.0000
    2    1    0   0.06   0.05   0.00   0.06         0.    0.0000
    3    5    0   0.47   0.19   0.00   0.47         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6600s 
time spent in multnx:                   0.6600s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3300s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6700s 

          starting ci iteration  19

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.98289105    -0.00000635     0.00130700    -0.00028846    -0.04274588    -0.04310495    -0.07116020
 ref:   2    -0.00000002    -0.97742427    -0.08843027     0.01759573     0.00041192    -0.03633848     0.02957773
 ref:   3    -0.00000033     0.00167362    -0.21394103    -0.97684425    -0.00000786    -0.00001312    -0.00025931

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 19  1   -114.3216887679  2.3022E-12  0.0000E+00  4.1049E-05  1.0000E-04
 mr-sdci # 19  2   -114.1774575748  2.4673E-07  2.7177E-07  6.4347E-04  1.0000E-04
 mr-sdci # 19  3   -113.9035192960  4.7381E-03  0.0000E+00  2.8747E-01  1.0000E-04
 mr-sdci # 19  4   -113.8875740511  3.2748E-04  0.0000E+00  4.9177E-01  1.0000E-04
 mr-sdci # 19  5   -113.5123367135  1.5359E-07  0.0000E+00  6.3045E-01  1.0000E-04
 mr-sdci # 19  6   -112.6855664346  3.4744E-01  0.0000E+00  1.5086E+00  1.0000E-04
 mr-sdci # 19  7   -112.2979504802 -7.3672E-02  0.0000E+00  1.5212E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.11   0.07   0.00   0.11         0.    0.0000
    2    1    0   0.06   0.05   0.00   0.06         0.    0.0000
    3    5    0   0.46   0.19   0.00   0.46         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.02   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6600s 
time spent in multnx:                   0.6500s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3200s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6700s 

          starting ci iteration  20

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.98289105     0.00000032     0.00018046     0.00002594     0.04277314    -0.00125985    -0.08576313     0.00103020
 ref:   2    -0.00000002    -0.97741096     0.08905194     0.01054901    -0.00048955     0.02835669     0.00212635     0.04538612
 ref:   3    -0.00000033     0.00167493     0.13682437    -0.99059321     0.00001054     0.00016019    -0.00001937    -0.00085614

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 20  1   -114.3216887679  1.4211E-13  0.0000E+00  4.1040E-05  1.0000E-04
 mr-sdci # 20  2   -114.1774577750  2.0017E-07  3.5538E-08  2.7965E-04  1.0000E-04
 mr-sdci # 20  3   -113.9123253410  8.8060E-03  0.0000E+00  2.5609E-01  1.0000E-04
 mr-sdci # 20  4   -113.8878451458  2.7109E-04  0.0000E+00  4.9552E-01  1.0000E-04
 mr-sdci # 20  5   -113.5123394889  2.7754E-06  0.0000E+00  6.3025E-01  1.0000E-04
 mr-sdci # 20  6   -113.2087739590  5.2321E-01  0.0000E+00  9.7430E-01  1.0000E-04
 mr-sdci # 20  7   -112.4104245179  1.1247E-01  0.0000E+00  1.3525E+00  1.0000E-04
 mr-sdci # 20  8   -111.6452641753  4.3942E-01  0.0000E+00  1.7393E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.12   0.07   0.00   0.12         0.    0.0000
    2    1    0   0.06   0.05   0.00   0.06         0.    0.0000
    3    5    0   0.46   0.19   0.00   0.46         0.    0.0000
    4   75    0   0.01   0.00   0.00   0.01         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6700s 
time spent in multnx:                   0.6700s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3200s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6900s 

          starting ci iteration  21

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.98289105     0.00000082     0.00030040     0.00003701     0.04277380    -0.00936825
 ref:   2    -0.00000004     0.97742026    -0.08382925    -0.00797095    -0.00048095    -0.10878096
 ref:   3    -0.00000033    -0.00167526    -0.11557807     0.99329616     0.00001033     0.00055734

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 21  1   -114.3216887679  0.0000E+00  0.0000E+00  4.1040E-05  1.0000E-04
 mr-sdci # 21  2   -114.1774578031  2.8172E-08  1.0413E-08  1.3220E-04  1.0000E-04
 mr-sdci # 21  3   -113.9166273559  4.3020E-03  0.0000E+00  2.4155E-01  1.0000E-04
 mr-sdci # 21  4   -113.8879202079  7.5062E-05  0.0000E+00  4.9619E-01  1.0000E-04
 mr-sdci # 21  5   -113.5123394950  6.0971E-09  0.0000E+00  6.3024E-01  1.0000E-04
 mr-sdci # 21  6   -112.2761731620 -9.3260E-01  0.0000E+00  1.4579E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.11   0.06   0.00   0.11         0.    0.0000
    2    1    0   0.06   0.05   0.00   0.06         0.    0.0000
    3    5    0   0.47   0.22   0.00   0.47         0.    0.0000
    4   75    0   0.01   0.00   0.00   0.01         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6700s 
time spent in multnx:                   0.6700s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3400s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6900s 

          starting ci iteration  22

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.98289105    -0.00000188    -0.00005763    -0.00004386    -0.04279206    -0.02750687    -0.03928701
 ref:   2    -0.00000004     0.97742055     0.08376225    -0.00790546     0.00049278     0.05574452    -0.09743178
 ref:   3    -0.00000033    -0.00167492     0.11510010     0.99335123    -0.00001209    -0.00117567     0.00027909

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 22  1   -114.3216887679  5.6843E-14  0.0000E+00  4.1041E-05  1.0000E-04
 mr-sdci # 22  2   -114.1774578094  6.2313E-09  0.0000E+00  7.9462E-05  1.0000E-04
 mr-sdci # 22  3   -113.9166695128  4.2157E-05  4.0791E-02  2.4128E-01  1.0000E-04
 mr-sdci # 22  4   -113.8879247761  4.5682E-06  0.0000E+00  4.9620E-01  1.0000E-04
 mr-sdci # 22  5   -113.5123396584  1.6346E-07  0.0000E+00  6.3011E-01  1.0000E-04
 mr-sdci # 22  6   -112.9118460727  6.3567E-01  0.0000E+00  1.3535E+00  1.0000E-04
 mr-sdci # 22  7   -111.6945673043 -7.1586E-01  0.0000E+00  1.4116E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.10   0.05   0.00   0.10         0.    0.0000
    2    1    0   0.08   0.06   0.00   0.08         0.    0.0000
    3    5    0   0.45   0.17   0.00   0.45         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.03   0.02   0.00   0.03         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6700s 
time spent in multnx:                   0.6700s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6900s 

          starting ci iteration  23

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.98289105    -0.00000189    -0.00043110    -0.00002030     0.04279543    -0.02742883     0.00216849     0.03997487
 ref:   2    -0.00000003     0.97742051    -0.08049098    -0.01092393    -0.00056061     0.05439120     0.05026704     0.08836399
 ref:   3    -0.00000033    -0.00167507    -0.14798022     0.98872762    -0.00009312    -0.00231609     0.02213440    -0.00451893

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 23  1   -114.3216887679  2.8422E-14  0.0000E+00  4.1041E-05  1.0000E-04
 mr-sdci # 23  2   -114.1774578094  2.2737E-12  0.0000E+00  7.9461E-05  1.0000E-04
 mr-sdci # 23  3   -113.9391496980  2.2480E-02  5.3719E-03  1.0497E-01  1.0000E-04
 mr-sdci # 23  4   -113.8880026585  7.7882E-05  0.0000E+00  4.9441E-01  1.0000E-04
 mr-sdci # 23  5   -113.5123412813  1.6229E-06  0.0000E+00  6.3008E-01  1.0000E-04
 mr-sdci # 23  6   -112.9124503703  6.0430E-04  0.0000E+00  1.3539E+00  1.0000E-04
 mr-sdci # 23  7   -112.2989768564  6.0441E-01  0.0000E+00  1.3071E+00  1.0000E-04
 mr-sdci # 23  8   -111.6514473516  6.1832E-03  0.0000E+00  1.4408E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.10   0.06   0.00   0.10         0.    0.0000
    2    1    0   0.06   0.05   0.00   0.06         0.    0.0000
    3    5    0   0.47   0.20   0.00   0.47         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.009998
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6500s 
time spent in multnx:                   0.6500s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3200s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6800s 

          starting ci iteration  24

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.98289105     0.00000187     0.00024151     0.00027001    -0.04281834     0.00263936
 ref:   2    -0.00000004    -0.97742034     0.07949719     0.01729062     0.00050630    -0.00251468
 ref:   3    -0.00000037     0.00167751     0.22748657    -0.97219919    -0.00219322     0.05093625

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 24  1   -114.3216887679  5.6843E-14  0.0000E+00  4.1039E-05  1.0000E-04
 mr-sdci # 24  2   -114.1774578094  8.0547E-11  0.0000E+00  7.9935E-05  1.0000E-04
 mr-sdci # 24  3   -113.9426257271  3.4760E-03  2.5266E-03  7.1094E-02  1.0000E-04
 mr-sdci # 24  4   -113.8929095681  4.9069E-03  0.0000E+00  4.7699E-01  1.0000E-04
 mr-sdci # 24  5   -113.5124429322  1.0165E-04  0.0000E+00  6.2970E-01  1.0000E-04
 mr-sdci # 24  6   -111.9569730901 -9.5548E-01  0.0000E+00  1.6291E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.11   0.07   0.00   0.11         0.    0.0000
    2    1    0   0.06   0.05   0.00   0.06         0.    0.0000
    3    5    0   0.47   0.19   0.00   0.47         0.    0.0000
    4   75    0   0.01   0.00   0.00   0.01         0.    0.0000
    5   45    0   0.01   0.01   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6600s 
time spent in multnx:                   0.6600s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3200s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6800s 

          starting ci iteration  25

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.98289105     0.00000184     0.00057541     0.00125151     0.04288942    -0.00438789    -0.00261901
 ref:   2    -0.00000004    -0.97742034    -0.06508598     0.04879375    -0.00044123    -0.00440637     0.00253367
 ref:   3    -0.00000035     0.00168009    -0.59446674    -0.78173705     0.01152261    -0.18045872    -0.05036872

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 25  1   -114.3216887679  0.0000E+00  0.0000E+00  4.1038E-05  1.0000E-04
 mr-sdci # 25  2   -114.1774578095  1.4978E-11  0.0000E+00  7.9617E-05  1.0000E-04
 mr-sdci # 25  3   -113.9486677304  6.0420E-03  6.0350E-03  1.2687E-01  1.0000E-04
 mr-sdci # 25  4   -113.9251425739  3.2233E-02  0.0000E+00  2.7527E-01  1.0000E-04
 mr-sdci # 25  5   -113.5126935933  2.5066E-04  0.0000E+00  6.2969E-01  1.0000E-04
 mr-sdci # 25  6   -112.5457475094  5.8877E-01  0.0000E+00  1.3175E+00  1.0000E-04
 mr-sdci # 25  7   -111.9569619688 -3.4201E-01  0.0000E+00  1.6300E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.11   0.06   0.00   0.11         0.    0.0000
    2    1    0   0.06   0.05   0.00   0.06         0.    0.0000
    3    5    0   0.46   0.19   0.00   0.46         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.02   0.02   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6600s 
time spent in multnx:                   0.6600s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3300s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6800s 

          starting ci iteration  26

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.98289105     0.00000184     0.00047573     0.00015005    -0.04297687    -0.00386217    -0.00279775     0.00065725
 ref:   2    -0.00000004    -0.97742034    -0.01462461     0.07932807    -0.00036684    -0.01513797     0.00388371    -0.01964801
 ref:   3    -0.00000031     0.00167918    -0.97288743    -0.13310101    -0.00040302    -0.07129815    -0.06839286     0.16056304

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 26  1   -114.3216887679 -2.8422E-14  0.0000E+00  4.1038E-05  1.0000E-04
 mr-sdci # 26  2   -114.1774578095  1.1653E-12  0.0000E+00  7.9689E-05  1.0000E-04
 mr-sdci # 26  3   -113.9673261414  1.8658E-02  5.1767E-03  1.2574E-01  1.0000E-04
 mr-sdci # 26  4   -113.9414389212  1.6296E-02  0.0000E+00  6.2728E-02  1.0000E-04
 mr-sdci # 26  5   -113.5134814732  7.8788E-04  0.0000E+00  6.2782E-01  1.0000E-04
 mr-sdci # 26  6   -112.7801394180  2.3439E-01  0.0000E+00  1.2297E+00  1.0000E-04
 mr-sdci # 26  7   -111.9598936553  2.9317E-03  0.0000E+00  1.6099E+00  1.0000E-04
 mr-sdci # 26  8   -111.4728858402 -1.7856E-01  0.0000E+00  1.2173E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.10   0.05   0.00   0.10         0.    0.0000
    2    1    0   0.06   0.05   0.00   0.06         0.    0.0000
    3    5    0   0.47   0.19   0.00   0.47         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.019999
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6500s 
time spent in multnx:                   0.6500s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6800s 

          starting ci iteration  27

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.98289105    -0.00000181    -0.00029232     0.00001088     0.04293457    -0.00583765
 ref:   2    -0.00000004     0.97742050     0.01060799     0.07927186     0.00004644    -0.03863213
 ref:   3    -0.00000032    -0.00167688     0.97921496    -0.06392469     0.00164021     0.03633101

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 27  1   -114.3216887679  0.0000E+00  0.0000E+00  4.1037E-05  1.0000E-04
 mr-sdci # 27  2   -114.1774578095  7.4152E-11  0.0000E+00  7.9483E-05  1.0000E-04
 mr-sdci # 27  3   -113.9705055699  3.1794E-03 -7.0542E-04  9.2014E-02  1.0000E-04
 mr-sdci # 27  4   -113.9425665038  1.1276E-03  0.0000E+00  3.0097E-02  1.0000E-04
 mr-sdci # 27  5   -113.5135568519  7.5379E-05  0.0000E+00  6.2752E-01  1.0000E-04
 mr-sdci # 27  6   -112.1288727658 -6.5127E-01  0.0000E+00  1.6999E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.11   0.07   0.00   0.11         0.    0.0000
    2    1    0   0.06   0.05   0.00   0.06         0.    0.0000
    3    5    0   0.46   0.21   0.00   0.46         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6500s 
time spent in multnx:                   0.6500s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3400s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.6700s 

          starting ci iteration  28

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.98289105    -0.00000181     0.00031636     0.00000445     0.04294723     0.00123991    -0.00616396
 ref:   2     0.00000000     0.97742043    -0.00958066    -0.07829944     0.00269877     0.09501301     0.04018102
 ref:   3     0.00000034    -0.00167684    -0.97866452     0.06890235     0.00251349     0.00655937     0.05004790

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 28  1   -114.3216887679  2.5580E-13  0.0000E+00  4.1018E-05  1.0000E-04
 mr-sdci # 28  2   -114.1774578095  3.9790E-13  0.0000E+00  7.9215E-05  1.0000E-04
 mr-sdci # 28  3   -113.9707211574  2.1559E-04  7.0629E-04  8.3242E-02  1.0000E-04
 mr-sdci # 28  4   -113.9426552207  8.8717E-05  0.0000E+00  3.2578E-02  1.0000E-04
 mr-sdci # 28  5   -113.5139323067  3.7545E-04  0.0000E+00  6.2800E-01  1.0000E-04
 mr-sdci # 28  6   -113.0309360700  9.0206E-01  0.0000E+00  1.1033E+00  1.0000E-04
 mr-sdci # 28  7   -111.2184912459 -7.4140E-01  0.0000E+00  1.6359E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.11   0.05   0.00   0.11         0.    0.0000
    2    1    0   0.06   0.03   0.00   0.06         0.    0.0000
    3    5    0   0.46   0.20   0.00   0.46         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6600s 
time spent in multnx:                   0.6600s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6800s 

          starting ci iteration  29

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.98289105     0.00000181    -0.00018296    -0.00004518     0.04296757     0.00091321     0.00025988    -0.00617980
 ref:   2    -0.00000001    -0.97742039     0.00586398     0.07899546     0.00186407     0.07568743     0.08978865     0.04275676
 ref:   3    -0.00000028     0.00167660     0.98095029    -0.04928905     0.00072922     0.00191109    -0.00822580     0.04923574

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 29  1   -114.3216887679  1.1369E-13  0.0000E+00  4.1019E-05  1.0000E-04
 mr-sdci # 29  2   -114.1774578095  8.8107E-13  0.0000E+00  7.9037E-05  1.0000E-04
 mr-sdci # 29  3   -113.9730487032  2.3275E-03  1.7233E-03  6.2016E-02  1.0000E-04
 mr-sdci # 29  4   -113.9428054918  1.5027E-04  0.0000E+00  3.0264E-02  1.0000E-04
 mr-sdci # 29  5   -113.5144413855  5.0908E-04  0.0000E+00  6.2987E-01  1.0000E-04
 mr-sdci # 29  6   -113.0696328723  3.8697E-02  0.0000E+00  1.1093E+00  1.0000E-04
 mr-sdci # 29  7   -112.3930160877  1.1745E+00  0.0000E+00  1.5201E+00  1.0000E-04
 mr-sdci # 29  8   -111.2171887575 -2.5570E-01  0.0000E+00  1.6298E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.10   0.05   0.00   0.10         0.    0.0000
    2    1    0   0.07   0.04   0.00   0.07         0.    0.0000
    3    5    0   0.47   0.19   0.00   0.47         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.030001
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6600s 
time spent in multnx:                   0.6600s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.2900s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6900s 

          starting ci iteration  30

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.98289105    -0.00000181    -0.00013234     0.00006869    -0.04296592    -0.00043722
 ref:   2    -0.00000001     0.97742044     0.00569099    -0.07872095    -0.00244760     0.03758586
 ref:   3    -0.00000030    -0.00167631     0.98105412     0.03613169     0.00078899    -0.03142543

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 30  1   -114.3216887679  8.5265E-14  0.0000E+00  4.1021E-05  1.0000E-04
 mr-sdci # 30  2   -114.1774578095  4.3201E-12  0.0000E+00  7.9374E-05  1.0000E-04
 mr-sdci # 30  3   -113.9740623207  1.0136E-03  2.0630E-04  3.9892E-02  1.0000E-04
 mr-sdci # 30  4   -113.9429676758  1.6218E-04  0.0000E+00  2.4598E-02  1.0000E-04
 mr-sdci # 30  5   -113.5147482866  3.0690E-04  0.0000E+00  6.2642E-01  1.0000E-04
 mr-sdci # 30  6   -112.0991605211 -9.7047E-01  0.0000E+00  1.7391E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.10   0.05   0.00   0.10         0.    0.0000
    2    1    0   0.07   0.05   0.00   0.07         0.    0.0000
    3    5    0   0.45   0.17   0.00   0.45         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.03   0.01   0.00   0.03         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6600s 
time spent in multnx:                   0.6600s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.2900s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6700s 

          starting ci iteration  31

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.98289105    -0.00000180    -0.00010847     0.00008347    -0.04289037     0.00264259     0.00041684
 ref:   2     0.00000002     0.97742071     0.00629427    -0.07832392    -0.00810373    -0.08040504     0.00992810
 ref:   3    -0.00000028    -0.00167612     0.98121215     0.03507910     0.00089823    -0.00034900     0.04231350

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 31  1   -114.3216887679  2.2737E-13  0.0000E+00  4.0993E-05  1.0000E-04
 mr-sdci # 31  2   -114.1774578095  1.2847E-11  0.0000E+00  7.9799E-05  1.0000E-04
 mr-sdci # 31  3   -113.9741245800  6.2259E-05 -1.8773E-04  4.3186E-02  1.0000E-04
 mr-sdci # 31  4   -113.9429879240  2.0248E-05  0.0000E+00  2.6722E-02  1.0000E-04
 mr-sdci # 31  5   -113.5163754414  1.6272E-03  0.0000E+00  6.3011E-01  1.0000E-04
 mr-sdci # 31  6   -113.1983624006  1.0992E+00  0.0000E+00  1.0475E+00  1.0000E-04
 mr-sdci # 31  7   -111.5743062198 -8.1871E-01  0.0000E+00  1.7275E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.10   0.07   0.00   0.10         0.    0.0000
    2    1    0   0.06   0.05   0.00   0.06         0.    0.0000
    3    5    0   0.46   0.17   0.00   0.46         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6500s 
time spent in multnx:                   0.6500s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6700s 

          starting ci iteration  32

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.98289105    -0.00000179     0.00005388     0.00010799     0.04279693     0.00273445     0.00288599     0.00114239
 ref:   2    -0.00000004     0.97742041    -0.00466176    -0.07884372     0.00499811    -0.07951035     0.07610527     0.03412562
 ref:   3    -0.00000021    -0.00167552    -0.98176222     0.03002960    -0.00192499    -0.00034793    -0.02501297     0.03506558

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 32  1   -114.3216887679  8.5265E-13  0.0000E+00  4.0945E-05  1.0000E-04
 mr-sdci # 32  2   -114.1774578096  2.6205E-11  0.0000E+00  7.8102E-05  1.0000E-04
 mr-sdci # 32  3   -113.9745388938  4.1431E-04  4.4329E-04  3.5412E-02  1.0000E-04
 mr-sdci # 32  4   -113.9430543935  6.6469E-05  0.0000E+00  2.4743E-02  1.0000E-04
 mr-sdci # 32  5   -113.5184929900  2.1175E-03  0.0000E+00  6.3355E-01  1.0000E-04
 mr-sdci # 32  6   -113.1984798363  1.1744E-04  0.0000E+00  1.0498E+00  1.0000E-04
 mr-sdci # 32  7   -112.2665565510  6.9225E-01  0.0000E+00  1.5684E+00  1.0000E-04
 mr-sdci # 32  8   -111.5063934413  2.8920E-01  0.0000E+00  1.6922E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.11   0.06   0.00   0.11         0.    0.0000
    2    1    0   0.06   0.05   0.00   0.06         0.    0.0000
    3    5    0   0.46   0.19   0.00   0.46         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.02   0.02   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6500s 
time spent in multnx:                   0.6500s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3200s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6800s 

          starting ci iteration  33

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.98289105    -0.00000178    -0.00002904    -0.00015078    -0.04287730    -0.00282989
 ref:   2    -0.00000007     0.97742052    -0.00501527     0.07851870    -0.00621826    -0.04959766
 ref:   3    -0.00000023    -0.00167534    -0.98151920    -0.02573712     0.00340045     0.03284738

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 33  1   -114.3216887679  7.6739E-13  0.0000E+00  4.0913E-05  1.0000E-04
 mr-sdci # 33  2   -114.1774578096  8.7255E-12  0.0000E+00  7.8273E-05  1.0000E-04
 mr-sdci # 33  3   -113.9748036034  2.6471E-04 -6.9518E-05  2.8463E-02  1.0000E-04
 mr-sdci # 33  4   -113.9431204635  6.6070E-05  0.0000E+00  2.2912E-02  1.0000E-04
 mr-sdci # 33  5   -113.5190417246  5.4873E-04  0.0000E+00  6.2817E-01  1.0000E-04
 mr-sdci # 33  6   -112.5696800064 -6.2880E-01  0.0000E+00  1.6335E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.11   0.08   0.00   0.11         0.    0.0000
    2    1    0   0.06   0.05   0.00   0.06         0.    0.0000
    3    5    0   0.46   0.21   0.00   0.46         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6500s 
time spent in multnx:                   0.6500s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3500s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.6800s 

          starting ci iteration  34

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.98289105    -0.00000178    -0.00003492    -0.00015244    -0.04259435    -0.00429291     0.00742932
 ref:   2     0.00000002     0.97742027    -0.00459575     0.07866022    -0.01220377     0.09120709    -0.03663136
 ref:   3    -0.00000024    -0.00167530    -0.98150830    -0.02554582     0.00316797    -0.00968864    -0.03761204

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 34  1   -114.3216887679  1.1369E-12  0.0000E+00  4.0813E-05  1.0000E-04
 mr-sdci # 34  2   -114.1774578096  8.4697E-12  0.0000E+00  7.7488E-05  1.0000E-04
 mr-sdci # 34  3   -113.9748212829  1.7679E-05  7.0083E-05  2.5554E-02  1.0000E-04
 mr-sdci # 34  4   -113.9431225110  2.0475E-06  0.0000E+00  2.2120E-02  1.0000E-04
 mr-sdci # 34  5   -113.5207300089  1.6883E-03  0.0000E+00  6.3400E-01  1.0000E-04
 mr-sdci # 34  6   -113.1580307934  5.8835E-01  0.0000E+00  1.0826E+00  1.0000E-04
 mr-sdci # 34  7   -111.6112067380 -6.5535E-01  0.0000E+00  1.7200E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.10   0.07   0.00   0.10         0.    0.0000
    2    1    0   0.06   0.05   0.00   0.06         0.    0.0000
    3    5    0   0.46   0.21   0.00   0.46         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.03   0.02   0.00   0.03         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6500s 
time spent in multnx:                   0.6500s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3500s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.6700s 

          starting ci iteration  35

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.98289105    -0.00000178    -0.00012964    -0.00020894    -0.04185933    -0.00848457    -0.00420159    -0.00678435
 ref:   2    -0.00000001     0.97742027    -0.00379365     0.07894590    -0.01104254     0.06707973    -0.08464947     0.05751990
 ref:   3    -0.00000020    -0.00167531    -0.98166017    -0.02183041     0.00635867    -0.01077413     0.01323981     0.03474504

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 35  1   -114.3216887679  7.1054E-13  0.0000E+00  4.0765E-05  1.0000E-04
 mr-sdci # 35  2   -114.1774578096 -5.6843E-14  0.0000E+00  7.7527E-05  1.0000E-04
 mr-sdci # 35  3   -113.9750422804  2.2100E-04  2.1701E-04  2.1649E-02  1.0000E-04
 mr-sdci # 35  4   -113.9431891272  6.6616E-05  0.0000E+00  2.0444E-02  1.0000E-04
 mr-sdci # 35  5   -113.5317506873  1.1021E-02  0.0000E+00  6.5559E-01  1.0000E-04
 mr-sdci # 35  6   -113.2269608970  6.8930E-02  0.0000E+00  1.0630E+00  1.0000E-04
 mr-sdci # 35  7   -112.4116791117  8.0047E-01  0.0000E+00  1.4871E+00  1.0000E-04
 mr-sdci # 35  8   -111.5523801098  4.5987E-02  0.0000E+00  1.6786E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.10   0.05   0.00   0.10         0.    0.0000
    2    1    0   0.07   0.05   0.00   0.07         0.    0.0000
    3    5    0   0.46   0.19   0.00   0.46         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.030001
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6500s 
time spent in multnx:                   0.6500s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6800s 

          starting ci iteration  36

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.98289105    -0.00000178     0.00019293    -0.00024822     0.04143934     0.00637983
 ref:   2    -0.00000003     0.97742029     0.00397999     0.07874043     0.01449777    -0.03460936
 ref:   3    -0.00000021    -0.00167529     0.98153267    -0.01939046    -0.01039924     0.02036346

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 36  1   -114.3216887679  3.1264E-13  0.0000E+00  4.0753E-05  1.0000E-04
 mr-sdci # 36  2   -114.1774578096  2.5580E-13  0.0000E+00  7.7610E-05  1.0000E-04
 mr-sdci # 36  3   -113.9751769479  1.3467E-04  2.2613E-05  1.5488E-02  1.0000E-04
 mr-sdci # 36  4   -113.9432320016  4.2874E-05  0.0000E+00  1.9135E-02  1.0000E-04
 mr-sdci # 36  5   -113.5406379328  8.8872E-03  0.0000E+00  6.0709E-01  1.0000E-04
 mr-sdci # 36  6   -112.5614400849 -6.6552E-01  0.0000E+00  1.6289E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.11   0.05   0.00   0.11         0.    0.0000
    2    1    0   0.06   0.04   0.00   0.06         0.    0.0000
    3    5    0   0.46   0.19   0.00   0.46         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6500s 
time spent in multnx:                   0.6500s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.2900s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6700s 

          starting ci iteration  37

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.98289105     0.00000178     0.00018071     0.00023940     0.03958492    -0.01593186    -0.00113602
 ref:   2     0.00000000    -0.97742032     0.00416106    -0.07859369     0.02514994     0.07154567     0.01577882
 ref:   3     0.00000021     0.00167529     0.98150423     0.01925065    -0.01268478    -0.01570901     0.01355110

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 37  1   -114.3216887679  2.2737E-13  0.0000E+00  4.0705E-05  1.0000E-04
 mr-sdci # 37  2   -114.1774578096  2.8422E-13  0.0000E+00  7.7656E-05  1.0000E-04
 mr-sdci # 37  3   -113.9751819408  4.9929E-06 -2.1537E-05  1.6624E-02  1.0000E-04
 mr-sdci # 37  4   -113.9432348390  2.8373E-06  0.0000E+00  1.9761E-02  1.0000E-04
 mr-sdci # 37  5   -113.5474134620  6.7755E-03  0.0000E+00  6.2454E-01  1.0000E-04
 mr-sdci # 37  6   -113.2432403792  6.8180E-01  0.0000E+00  1.0371E+00  1.0000E-04
 mr-sdci # 37  7   -112.1002271814 -3.1145E-01  0.0000E+00  1.6790E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.10   0.05   0.00   0.10         0.    0.0000
    2    1    0   0.07   0.05   0.00   0.07         0.    0.0000
    3    5    0   0.45   0.18   0.00   0.45         0.    0.0000
    4   75    0   0.01   0.00   0.00   0.01         0.    0.0000
    5   45    0   0.01   0.01   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.019997
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6400s 
time spent in multnx:                   0.6400s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.2900s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6700s 

          starting ci iteration  38

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.98289105     0.00000177     0.00022176    -0.00026641     0.03890671    -0.01667056     0.00566763     0.00323189
 ref:   2     0.00000004    -0.97742018     0.00360004     0.07887206     0.01823867     0.06779599     0.05795127     0.07677891
 ref:   3     0.00000020     0.00167519     0.98150092    -0.01806216    -0.01470911    -0.01587194    -0.00736601     0.01106829

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 38  1   -114.3216887679  3.6948E-13  0.0000E+00  4.0676E-05  1.0000E-04
 mr-sdci # 38  2   -114.1774578096  6.2244E-12  0.0000E+00  7.6844E-05  1.0000E-04
 mr-sdci # 38  3   -113.9752470428  6.5102E-05  7.3380E-05  1.3981E-02  1.0000E-04
 mr-sdci # 38  4   -113.9432572265  2.2388E-05  0.0000E+00  1.8857E-02  1.0000E-04
 mr-sdci # 38  5   -113.5582325253  1.0819E-02  0.0000E+00  6.3976E-01  1.0000E-04
 mr-sdci # 38  6   -113.2448520435  1.6117E-03  0.0000E+00  1.0412E+00  1.0000E-04
 mr-sdci # 38  7   -112.3736191924  2.7339E-01  0.0000E+00  1.5464E+00  1.0000E-04
 mr-sdci # 38  8   -111.8438205633  2.9144E-01  0.0000E+00  1.6245E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.10   0.06   0.00   0.10         0.    0.0000
    2    1    0   0.06   0.05   0.00   0.06         0.    0.0000
    3    5    0   0.46   0.19   0.00   0.46         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.019999
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6500s 
time spent in multnx:                   0.6500s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3200s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6700s 

          starting ci iteration  39

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.98289105    -0.00000177     0.00026445     0.00029691    -0.03804345    -0.00909396
 ref:   2    -0.00000006     0.97742015     0.00385637    -0.07866494    -0.02469462     0.04709087
 ref:   3    -0.00000020    -0.00167520     0.98139340     0.01711407     0.01877914    -0.01693122

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 39  1   -114.3216887679  3.6948E-13  0.0000E+00  4.0662E-05  1.0000E-04
 mr-sdci # 39  2   -114.1774578096  3.9790E-13  0.0000E+00  7.6788E-05  1.0000E-04
 mr-sdci # 39  3   -113.9752918452  4.4802E-05 -1.1388E-05  1.1560E-02  1.0000E-04
 mr-sdci # 39  4   -113.9432750985  1.7872E-05  0.0000E+00  1.8433E-02  1.0000E-04
 mr-sdci # 39  5   -113.5740762227  1.5844E-02  0.0000E+00  5.8306E-01  1.0000E-04
 mr-sdci # 39  6   -112.6472706153 -5.9758E-01  0.0000E+00  1.6286E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.11   0.06   0.00   0.10         0.    0.0000
    2    1    0   0.06   0.05   0.00   0.06         0.    0.0000
    3    5    0   0.46   0.20   0.00   0.46         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6600s 
time spent in multnx:                   0.6500s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3300s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6600s 

          starting ci iteration  40

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.98289107     0.00000176     0.00028951     0.00030886    -0.03740119     0.01786308    -0.00641394
 ref:   2     0.00000000    -0.97742006     0.00370900    -0.07873471    -0.02778288    -0.08301240     0.02821640
 ref:   3     0.00000022     0.00167517     0.98142327     0.01708693     0.01929969     0.01862138     0.00234088

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 40  1   -114.3216887679  9.0949E-13  0.0000E+00  4.0515E-05  1.0000E-04
 mr-sdci # 40  2   -114.1774578096  1.4779E-12  0.0000E+00  7.6539E-05  1.0000E-04
 mr-sdci # 40  3   -113.9752946963  2.8511E-06  1.1533E-05  1.0453E-02  1.0000E-04
 mr-sdci # 40  4   -113.9432757361  6.3762E-07  0.0000E+00  1.8160E-02  1.0000E-04
 mr-sdci # 40  5   -113.5747703772  6.9415E-04  0.0000E+00  5.8851E-01  1.0000E-04
 mr-sdci # 40  6   -113.1293452982  4.8207E-01  0.0000E+00  1.1239E+00  1.0000E-04
 mr-sdci # 40  7   -111.9194848602 -4.5413E-01  0.0000E+00  1.7506E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.11   0.07   0.00   0.11         0.    0.0000
    2    1    0   0.06   0.05   0.00   0.06         0.    0.0000
    3    5    0   0.46   0.20   0.00   0.46         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.02   0.02   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6500s 
time spent in multnx:                   0.6500s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3400s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6800s 

          starting ci iteration  41

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.98289106     0.00000176     0.00034451     0.00035323    -0.03509903     0.02171303     0.00702785     0.00413696
 ref:   2     0.00000005    -0.97742013     0.00341600    -0.07889429    -0.02022574    -0.06404550     0.08139302    -0.06038478
 ref:   3     0.00000022     0.00167518     0.98125206     0.01613019     0.02672995     0.02202065     0.01514789    -0.00905691

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 41  1   -114.3216887679  1.5916E-12  0.0000E+00  4.0403E-05  1.0000E-04
 mr-sdci # 41  2   -114.1774578096  2.7001E-12  0.0000E+00  7.6920E-05  1.0000E-04
 mr-sdci # 41  3   -113.9753323766  3.7680E-05  3.7050E-05  8.9808E-03  1.0000E-04
 mr-sdci # 41  4   -113.9432947268  1.8991E-05  0.0000E+00  1.7301E-02  1.0000E-04
 mr-sdci # 41  5   -113.6074901099  3.2720E-02  0.0000E+00  5.9131E-01  1.0000E-04
 mr-sdci # 41  6   -113.1651325938  3.5787E-02  0.0000E+00  1.1159E+00  1.0000E-04
 mr-sdci # 41  7   -112.2997572473  3.8027E-01  0.0000E+00  1.5161E+00  1.0000E-04
 mr-sdci # 41  8   -111.8595019314  1.5681E-02  0.0000E+00  1.6991E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.09   0.05   0.00   0.09         0.    0.0000
    2    1    0   0.07   0.03   0.00   0.07         0.    0.0000
    3    5    0   0.47   0.20   0.00   0.47         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6500s 
time spent in multnx:                   0.6400s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.2900s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.6700s 

          starting ci iteration  42

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.98289106    -0.00000176    -0.00035770     0.00036650     0.03242105    -0.01896985
 ref:   2     0.00000006     0.97742007    -0.00355688    -0.07877043     0.02535088     0.02840485
 ref:   3     0.00000023    -0.00167522    -0.98123084     0.01560490    -0.02769374     0.00871675

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 42  1   -114.3216887679  1.9895E-13  0.0000E+00  4.0407E-05  1.0000E-04
 mr-sdci # 42  2   -114.1774578096  3.2401E-12  0.0000E+00  7.6739E-05  1.0000E-04
 mr-sdci # 42  3   -113.9753568957  2.4519E-05  2.8342E-06  6.7200E-03  1.0000E-04
 mr-sdci # 42  4   -113.9433056735  1.0947E-05  0.0000E+00  1.7180E-02  1.0000E-04
 mr-sdci # 42  5   -113.6360692729  2.8579E-02  0.0000E+00  4.9114E-01  1.0000E-04
 mr-sdci # 42  6   -112.5778256347 -5.8731E-01  0.0000E+00  1.6391E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.10   0.05   0.00   0.10         0.    0.0000
    2    1    0   0.06   0.05   0.00   0.06         0.    0.0000
    3    5    0   0.47   0.20   0.00   0.47         0.    0.0000
    4   75    0   0.02   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6700s 
time spent in multnx:                   0.6600s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3200s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6800s 

          starting ci iteration  43

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.98289106    -0.00000173    -0.00034658    -0.00035582     0.03053566     0.02712473    -0.00193427
 ref:   2     0.00000005     0.97741995    -0.00360260     0.07872447     0.03066043    -0.06323606    -0.02218205
 ref:   3     0.00000023    -0.00167518    -0.98121058    -0.01557279    -0.02972908     0.01630710     0.02856785

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 43  1   -114.3216887679 -2.8422E-14  0.0000E+00  4.0391E-05  1.0000E-04
 mr-sdci # 43  2   -114.1774578096  3.7801E-12  0.0000E+00  7.6419E-05  1.0000E-04
 mr-sdci # 43  3   -113.9753573009  4.0519E-07 -2.7278E-06  7.0162E-03  1.0000E-04
 mr-sdci # 43  4   -113.9433060475  3.7402E-07  0.0000E+00  1.7302E-02  1.0000E-04
 mr-sdci # 43  5   -113.6396240473  3.5548E-03  0.0000E+00  4.9978E-01  1.0000E-04
 mr-sdci # 43  6   -113.1412777591  5.6345E-01  0.0000E+00  1.1215E+00  1.0000E-04
 mr-sdci # 43  7   -112.0703804977 -2.2938E-01  0.0000E+00  1.7050E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.09   0.05   0.00   0.09         0.    0.0000
    2    1    0   0.07   0.04   0.00   0.07         0.    0.0000
    3    5    0   0.46   0.21   0.00   0.46         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.02   0.02   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6500s 
time spent in multnx:                   0.6500s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3300s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6700s 

          starting ci iteration  44

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.98289106    -0.00000173    -0.00037140     0.00037703     0.02979278     0.02740137    -0.00270501    -0.00692937
 ref:   2     0.00000008     0.97742005    -0.00336843    -0.07887934     0.02096115    -0.06114799    -0.04193204    -0.09594561
 ref:   3     0.00000024    -0.00167516    -0.98110241     0.01520563    -0.03388743     0.01675677    -0.03768553    -0.00354901

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 44  1   -114.3216887679  1.4211E-13  0.0000E+00  4.0380E-05  1.0000E-04
 mr-sdci # 44  2   -114.1774578096  3.1832E-12  0.0000E+00  7.6811E-05  1.0000E-04
 mr-sdci # 44  3   -113.9753697890  1.2488E-05  1.3515E-05  5.9235E-03  1.0000E-04
 mr-sdci # 44  4   -113.9433127735  6.7260E-06  0.0000E+00  1.6857E-02  1.0000E-04
 mr-sdci # 44  5   -113.6562271695  1.6603E-02  0.0000E+00  4.9136E-01  1.0000E-04
 mr-sdci # 44  6   -113.1417057783  4.2802E-04  0.0000E+00  1.1246E+00  1.0000E-04
 mr-sdci # 44  7   -112.1108064093  4.0426E-02  0.0000E+00  1.6359E+00  1.0000E-04
 mr-sdci # 44  8   -111.9923877129  1.3289E-01  0.0000E+00  1.5822E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.10   0.06   0.00   0.10         0.    0.0000
    2    1    0   0.06   0.05   0.00   0.06         0.    0.0000
    3    5    0   0.48   0.20   0.00   0.48         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.030001
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6600s 
time spent in multnx:                   0.6600s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3200s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6900s 

          starting ci iteration  45

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.98289106    -0.00000171    -0.00036223    -0.00037283    -0.02786194     0.01965034
 ref:   2     0.00000011     0.97741994    -0.00348764     0.07878541    -0.02600052    -0.04550147
 ref:   3     0.00000025    -0.00167521    -0.98109854    -0.01502314     0.03354871    -0.01426809

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 45  1   -114.3216887679  8.5265E-13  0.0000E+00  4.0394E-05  1.0000E-04
 mr-sdci # 45  2   -114.1774578096  8.1286E-12  0.0000E+00  7.6637E-05  1.0000E-04
 mr-sdci # 45  3   -113.9753781095  8.3205E-06 -1.4049E-06  4.8115E-03  1.0000E-04
 mr-sdci # 45  4   -113.9433167145  3.9410E-06  0.0000E+00  1.6817E-02  1.0000E-04
 mr-sdci # 45  5   -113.6706950602  1.4468E-02  0.0000E+00  4.4817E-01  1.0000E-04
 mr-sdci # 45  6   -112.3889932440 -7.5271E-01  0.0000E+00  1.6727E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.10   0.06   0.00   0.10         0.    0.0000
    2    1    0   0.06   0.05   0.00   0.06         0.    0.0000
    3    5    0   0.48   0.21   0.00   0.48         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6600s 
time spent in multnx:                   0.6600s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3300s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6700s 

          starting ci iteration  46

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.98289107     0.00000170    -0.00037266    -0.00037716    -0.02747597    -0.02704109     0.00029471
 ref:   2    -0.00000008    -0.97741990    -0.00345196     0.07880019    -0.02720165     0.07782676    -0.01855150
 ref:   3    -0.00000026     0.00167519    -0.98111273    -0.01502628     0.03394565    -0.01619075     0.03985189

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 46  1   -114.3216887679  2.5580E-13  0.0000E+00  4.0314E-05  1.0000E-04
 mr-sdci # 46  2   -114.1774578096  1.9895E-13  0.0000E+00  7.6495E-05  1.0000E-04
 mr-sdci # 46  3   -113.9753783270  2.1747E-07  1.4483E-06  4.5486E-03  1.0000E-04
 mr-sdci # 46  4   -113.9433167507  3.6188E-08  0.0000E+00  1.6786E-02  1.0000E-04
 mr-sdci # 46  5   -113.6708595022  1.6444E-04  0.0000E+00  4.4835E-01  1.0000E-04
 mr-sdci # 46  6   -113.0755367153  6.8654E-01  0.0000E+00  1.1585E+00  1.0000E-04
 mr-sdci # 46  7   -111.5489003876 -5.6191E-01  0.0000E+00  1.7266E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.10   0.04   0.00   0.10         0.    0.0000
    2    1    0   0.07   0.05   0.00   0.07         0.    0.0000
    3    5    0   0.47   0.22   0.00   0.47         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010002
time for cinew                         0.009998
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6600s 
time spent in multnx:                   0.6600s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3200s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6800s 

          starting ci iteration  47

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.98289107     0.00000170     0.00038068    -0.00038662    -0.02533175    -0.02951304     0.00382483     0.00227008
 ref:   2     0.00000010    -0.97742004     0.00332247     0.07889183    -0.01906058     0.05711595     0.10452478     0.01707443
 ref:   3     0.00000028     0.00167511     0.98098099    -0.01473367     0.04063948    -0.02167715     0.02048390     0.05118039

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 47  1   -114.3216887679  1.9895E-13  0.0000E+00  4.0294E-05  1.0000E-04
 mr-sdci # 47  2   -114.1774578096  8.2139E-12  0.0000E+00  7.7003E-05  1.0000E-04
 mr-sdci # 47  3   -113.9753854207  7.0938E-06  6.4324E-06  3.8802E-03  1.0000E-04
 mr-sdci # 47  4   -113.9433214699  4.7192E-06  0.0000E+00  1.6351E-02  1.0000E-04
 mr-sdci # 47  5   -113.6937681695  2.2909E-02  0.0000E+00  4.0102E-01  1.0000E-04
 mr-sdci # 47  6   -113.1144565147  3.8920E-02  0.0000E+00  1.1789E+00  1.0000E-04
 mr-sdci # 47  7   -112.1531626660  6.0426E-01  0.0000E+00  1.4843E+00  1.0000E-04
 mr-sdci # 47  8   -111.4634946863 -5.2889E-01  0.0000E+00  1.6952E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.10   0.04   0.00   0.10         0.    0.0000
    2    1    0   0.07   0.05   0.00   0.07         0.    0.0000
    3    5    0   0.46   0.21   0.00   0.46         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.02   0.02   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.030003
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6600s 
time spent in multnx:                   0.6600s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3300s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.7000s 

          starting ci iteration  48

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.98289105    -0.00000164    -0.00035656     0.00037249     0.02314169    -0.02778099
 ref:   2    -0.00000013     0.97741995    -0.00337356    -0.07885403     0.02133472     0.02655189
 ref:   3    -0.00000031    -0.00167520    -0.98100152     0.01466157    -0.03883670     0.03182746

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 48  1   -114.3216887679  1.9327E-12  0.0000E+00  4.0341E-05  1.0000E-04
 mr-sdci # 48  2   -114.1774578096  1.4722E-11  0.0000E+00  7.6751E-05  1.0000E-04
 mr-sdci # 48  3   -113.9753896582  4.2375E-06  4.6260E-07  2.8152E-03  1.0000E-04
 mr-sdci # 48  4   -113.9433231744  1.7045E-06  0.0000E+00  1.6394E-02  1.0000E-04
 mr-sdci # 48  5   -113.7034623709  9.6942E-03  0.0000E+00  3.7118E-01  1.0000E-04
 mr-sdci # 48  6   -112.2471290344 -8.6733E-01  0.0000E+00  1.6857E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.10   0.05   0.00   0.10         0.    0.0000
    2    1    0   0.07   0.05   0.00   0.07         0.    0.0000
    3    5    0   0.47   0.18   0.00   0.46         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.009998
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6600s 
time spent in multnx:                   0.6500s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.2900s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6700s 

          starting ci iteration  49

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.98289104    -0.00000160    -0.00034969     0.00036692     0.02236198     0.03365973    -0.00778831
 ref:   2     0.00000015     0.97741985    -0.00338961    -0.07884086     0.02296341    -0.06165332    -0.01914206
 ref:   3     0.00000029    -0.00167515    -0.98099342     0.01465362    -0.03953677     0.01512036     0.05612538

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 49  1   -114.3216887679  2.2737E-13  0.0000E+00  4.0387E-05  1.0000E-04
 mr-sdci # 49  2   -114.1774578096  2.7001E-12  0.0000E+00  7.6340E-05  1.0000E-04
 mr-sdci # 49  3   -113.9753897137  5.5455E-08 -4.4469E-07  2.9272E-03  1.0000E-04
 mr-sdci # 49  4   -113.9433232096  3.5185E-08  0.0000E+00  1.6415E-02  1.0000E-04
 mr-sdci # 49  5   -113.7038504713  3.8810E-04  0.0000E+00  3.6904E-01  1.0000E-04
 mr-sdci # 49  6   -113.1662074488  9.1908E-01  0.0000E+00  1.1197E+00  1.0000E-04
 mr-sdci # 49  7   -111.5388585397 -6.1430E-01  0.0000E+00  1.6988E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.10   0.05   0.00   0.10         0.    0.0000
    2    1    0   0.08   0.05   0.00   0.08         0.    0.0000
    3    5    0   0.46   0.20   0.00   0.46         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010002
time for cinew                         0.009998
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6600s 
time spent in multnx:                   0.6600s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6800s 

          starting ci iteration  50

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.98289103    -0.00000158     0.00034296     0.00036273     0.02098039     0.03524368     0.00741468     0.00552197
 ref:   2     0.00000012     0.97742003     0.00328938    -0.07891076     0.01650360    -0.04642692     0.11316399    -0.01288038
 ref:   3     0.00000028    -0.00167508     0.98093712     0.01455757    -0.04242848     0.01793056     0.01291814    -0.06284997

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 50  1   -114.3216887679  1.7053E-13  0.0000E+00  4.0390E-05  1.0000E-04
 mr-sdci # 50  2   -114.1774578097  8.2423E-12  0.0000E+00  7.6866E-05  1.0000E-04
 mr-sdci # 50  3   -113.9753920272  2.3135E-06  2.4729E-06  2.5353E-03  1.0000E-04
 mr-sdci # 50  4   -113.9433244512  1.2416E-06  0.0000E+00  1.6253E-02  1.0000E-04
 mr-sdci # 50  5   -113.7120399023  8.1894E-03  0.0000E+00  3.4556E-01  1.0000E-04
 mr-sdci # 50  6   -113.1856116568  1.9404E-02  0.0000E+00  1.1401E+00  1.0000E-04
 mr-sdci # 50  7   -112.1337601332  5.9490E-01  0.0000E+00  1.5006E+00  1.0000E-04
 mr-sdci # 50  8   -111.4858007342  2.2306E-02  0.0000E+00  1.6621E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.11   0.08   0.00   0.11         0.    0.0000
    2    1    0   0.06   0.05   0.00   0.06         0.    0.0000
    3    5    0   0.47   0.20   0.00   0.47         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.030003
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6600s 
time spent in multnx:                   0.6600s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3400s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6900s 

          starting ci iteration  51

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.98289100    -0.00000152    -0.00032556     0.00035355    -0.01999080    -0.02449329
 ref:   2     0.00000018     0.97741989    -0.00333307    -0.07888563    -0.01836773     0.04475030
 ref:   3     0.00000031    -0.00167515    -0.98094599     0.01453693     0.04184105     0.02034085

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 51  1   -114.3216887679  3.8938E-12  0.0000E+00  4.0389E-05  1.0000E-04
 mr-sdci # 51  2   -114.1774578097  1.8815E-11  0.0000E+00  7.6473E-05  1.0000E-04
 mr-sdci # 51  3   -113.9753935144  1.4872E-06 -1.6895E-07  1.9346E-03  1.0000E-04
 mr-sdci # 51  4   -113.9433248862  4.3500E-07  0.0000E+00  1.6264E-02  1.0000E-04
 mr-sdci # 51  5   -113.7145536374  2.5137E-03  0.0000E+00  3.3768E-01  1.0000E-04
 mr-sdci # 51  6   -112.2250541252 -9.6056E-01  0.0000E+00  1.6768E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.10   0.05   0.00   0.10         0.    0.0000
    2    1    0   0.07   0.06   0.00   0.07         0.    0.0000
    3    5    0   0.47   0.21   0.00   0.47         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020004
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6600s 
time spent in multnx:                   0.6600s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3300s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6800s 

          starting ci iteration  52

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.98289101    -0.00000153    -0.00032991     0.00035555    -0.01998280     0.03360525    -0.00379779
 ref:   2     0.00000017     0.97741992    -0.00332316    -0.07889018    -0.01838575    -0.07328934    -0.00303724
 ref:   3     0.00000031    -0.00167516    -0.98094933     0.01453820     0.04184639     0.01429982     0.03860334

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 52  1   -114.3216887679 -5.6843E-14  0.0000E+00  4.0359E-05  1.0000E-04
 mr-sdci # 52  2   -114.1774578097  1.7053E-13  0.0000E+00  7.6606E-05  1.0000E-04
 mr-sdci # 52  3   -113.9753935317  1.7312E-08  1.7235E-07  1.8503E-03  1.0000E-04
 mr-sdci # 52  4   -113.9433248896  3.4885E-09  0.0000E+00  1.6257E-02  1.0000E-04
 mr-sdci # 52  5   -113.7145536744  3.6994E-08  0.0000E+00  3.3763E-01  1.0000E-04
 mr-sdci # 52  6   -113.1837281542  9.5867E-01  0.0000E+00  1.1368E+00  1.0000E-04
 mr-sdci # 52  7   -111.5517968393 -5.8196E-01  0.0000E+00  1.6968E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.11   0.05   0.00   0.11         0.    0.0000
    2    1    0   0.06   0.04   0.00   0.06         0.    0.0000
    3    5    0   0.46   0.20   0.00   0.46         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.009998
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6500s 
time spent in multnx:                   0.6500s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6600s 

          starting ci iteration  53

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.98289100    -0.00000149    -0.00031361    -0.00034460    -0.01786061    -0.03666545    -0.00206897    -0.00319033
 ref:   2     0.00000012     0.97742007    -0.00326721     0.07892597    -0.01477180     0.04027209    -0.11177022     0.00705211
 ref:   3     0.00000029    -0.00167509    -0.98090928    -0.01448469     0.04436465    -0.01957148    -0.01610455     0.04064007

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 53  1   -114.3216887679  8.5265E-13  0.0000E+00  4.0366E-05  1.0000E-04
 mr-sdci # 53  2   -114.1774578097  8.8392E-12  0.0000E+00  7.7076E-05  1.0000E-04
 mr-sdci # 53  3   -113.9753947244  1.1926E-06  1.1367E-06  1.6508E-03  1.0000E-04
 mr-sdci # 53  4   -113.9433254324  5.4274E-07  0.0000E+00  1.6138E-02  1.0000E-04
 mr-sdci # 53  5   -113.7200802421  5.5266E-03  0.0000E+00  3.0678E-01  1.0000E-04
 mr-sdci # 53  6   -113.2788017668  9.5074E-02  0.0000E+00  1.1446E+00  1.0000E-04
 mr-sdci # 53  7   -112.2705978572  7.1880E-01  0.0000E+00  1.4356E+00  1.0000E-04
 mr-sdci # 53  8   -111.5449776808  5.9177E-02  0.0000E+00  1.6901E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.12   0.08   0.00   0.11         0.    0.0000
    2    1    0   0.06   0.05   0.00   0.06         0.    0.0000
    3    5    0   0.46   0.19   0.00   0.46         0.    0.0000
    4   75    0   0.01   0.00   0.00   0.01         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.019997
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6700s 
time spent in multnx:                   0.6600s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3300s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6900s 

          starting ci iteration  54

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.98289095    -0.00000140     0.00029405    -0.00033812     0.01724182     0.03194250
 ref:   2     0.00000017     0.97741999     0.00328448     0.07891997     0.01526666    -0.02515815
 ref:   3     0.00000034    -0.00167517     0.98092096    -0.01448100    -0.04399761    -0.02292206

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 54  1   -114.3216887679  6.3665E-12  0.0000E+00  4.0383E-05  1.0000E-04
 mr-sdci # 54  2   -114.1774578097  1.7735E-11  0.0000E+00  7.6664E-05  1.0000E-04
 mr-sdci # 54  3   -113.9753954557  7.3136E-07  9.1424E-08  1.1638E-03  1.0000E-04
 mr-sdci # 54  4   -113.9433255125  8.0146E-08  0.0000E+00  1.6157E-02  1.0000E-04
 mr-sdci # 54  5   -113.7206223392  5.4210E-04  0.0000E+00  3.0541E-01  1.0000E-04
 mr-sdci # 54  6   -112.2907220486 -9.8808E-01  0.0000E+00  1.6739E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.11   0.08   0.00   0.11         0.    0.0000
    2    1    0   0.06   0.05   0.00   0.06         0.    0.0000
    3    5    0   0.46   0.20   0.00   0.46         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010002
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6600s 
time spent in multnx:                   0.6600s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3500s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6700s 

          starting ci iteration  55

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.98289092    -0.00000137     0.00028931     0.00033702     0.01777957    -0.03785455     0.01253869
 ref:   2    -0.00000020     0.97741995     0.00329203    -0.07891823     0.01441778     0.05660409     0.00837380
 ref:   3    -0.00000032    -0.00167516     0.98091851     0.01448036    -0.04373401    -0.01347490    -0.03672023

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 55  1   -114.3216887679  3.9790E-13  0.0000E+00  4.0446E-05  1.0000E-04
 mr-sdci # 55  2   -114.1774578097  7.3896E-13  0.0000E+00  7.6440E-05  1.0000E-04
 mr-sdci # 55  3   -113.9753954674  1.1702E-08 -8.7965E-08  1.2351E-03  1.0000E-04
 mr-sdci # 55  4   -113.9433255131  5.8569E-10  0.0000E+00  1.6159E-02  1.0000E-04
 mr-sdci # 55  5   -113.7207043050  8.1966E-05  0.0000E+00  3.0914E-01  1.0000E-04
 mr-sdci # 55  6   -113.3736934738  1.0830E+00  0.0000E+00  1.0425E+00  1.0000E-04
 mr-sdci # 55  7   -111.7748226366 -4.9578E-01  0.0000E+00  1.7068E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.10   0.06   0.00   0.10         0.    0.0000
    2    1    0   0.06   0.05   0.00   0.06         0.    0.0000
    3    5    0   0.47   0.21   0.00   0.47         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010002
time for cinew                         0.009998
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6500s 
time spent in multnx:                   0.6500s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3300s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6700s 

          starting ci iteration  56

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.98289091    -0.00000132     0.00027712    -0.00033106     0.01665730    -0.03989291     0.00350345    -0.01301759
 ref:   2     0.00000012     0.97742015     0.00324963     0.07893798     0.01237821     0.03333169     0.11578398    -0.01716979
 ref:   3     0.00000031    -0.00167511     0.98090468    -0.01446746    -0.04452815    -0.01551746     0.01383860     0.03567561

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 56  1   -114.3216887679  1.5916E-12  0.0000E+00  4.0415E-05  1.0000E-04
 mr-sdci # 56  2   -114.1774578097  9.9476E-12  0.0000E+00  7.6927E-05  1.0000E-04
 mr-sdci # 56  3   -113.9753958763  4.0890E-07  4.8113E-07  1.1502E-03  1.0000E-04
 mr-sdci # 56  4   -113.9433256049  9.1788E-08  0.0000E+00  1.6125E-02  1.0000E-04
 mr-sdci # 56  5   -113.7218251770  1.1209E-03  0.0000E+00  2.9691E-01  1.0000E-04
 mr-sdci # 56  6   -113.4226742409  4.8981E-02  0.0000E+00  1.0499E+00  1.0000E-04
 mr-sdci # 56  7   -112.2401997342  4.6538E-01  0.0000E+00  1.4541E+00  1.0000E-04
 mr-sdci # 56  8   -111.7720207655  2.2704E-01  0.0000E+00  1.7121E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.10   0.06   0.00   0.10         0.    0.0000
    2    1    0   0.06   0.05   0.00   0.06         0.    0.0000
    3    5    0   0.47   0.21   0.00   0.47         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.029995
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6500s 
time spent in multnx:                   0.6500s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3300s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6800s 

          starting ci iteration  57

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.98289085    -0.00000124     0.00026428    -0.00032962    -0.01697738    -0.02863980
 ref:   2    -0.00000020     0.97742004     0.00326903     0.07893580    -0.01186632     0.04600580
 ref:   3    -0.00000033    -0.00167514     0.98090901    -0.01446698     0.04458191     0.00675333

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 57  1   -114.3216887679  6.3096E-12  0.0000E+00  4.0383E-05  1.0000E-04
 mr-sdci # 57  2   -114.1774578097  1.1141E-11  0.0000E+00  7.6508E-05  1.0000E-04
 mr-sdci # 57  3   -113.9753961651  2.8878E-07 -3.4711E-08  8.9859E-04  1.0000E-04
 mr-sdci # 57  4   -113.9433256085  3.5572E-09  0.0000E+00  1.6128E-02  1.0000E-04
 mr-sdci # 57  5   -113.7219866153  1.6144E-04  0.0000E+00  2.9693E-01  1.0000E-04
 mr-sdci # 57  6   -112.4204817136 -1.0022E+00  0.0000E+00  1.6750E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.10   0.06   0.00   0.10         0.    0.0000
    2    1    0   0.06   0.05   0.00   0.06         0.    0.0000
    3    5    0   0.47   0.22   0.00   0.47         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.009998
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6500s 
time spent in multnx:                   0.6500s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3400s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6700s 

          starting ci iteration  58

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.98289086    -0.00000129     0.00026674     0.00033042    -0.01775779     0.03515078     0.00748245
 ref:   2    -0.00000019     0.97742012     0.00326441    -0.07893730    -0.01037406    -0.06639243    -0.00424461
 ref:   3    -0.00000033    -0.00167516     0.98090998     0.01446726     0.04426248     0.01221880    -0.01851924

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 58  1   -114.3216887679  1.1369E-13  0.0000E+00  4.0357E-05  1.0000E-04
 mr-sdci # 58  2   -114.1774578097  1.4211E-12  0.0000E+00  7.6894E-05  1.0000E-04
 mr-sdci # 58  3   -113.9753961685  3.3962E-09  3.5171E-08  8.4828E-04  1.0000E-04
 mr-sdci # 58  4   -113.9433256088  3.3506E-10  0.0000E+00  1.6126E-02  1.0000E-04
 mr-sdci # 58  5   -113.7221675286  1.8091E-04  0.0000E+00  3.0461E-01  1.0000E-04
 mr-sdci # 58  6   -113.4068928347  9.8641E-01  0.0000E+00  1.0387E+00  1.0000E-04
 mr-sdci # 58  7   -111.7132704836 -5.2693E-01  0.0000E+00  1.6984E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.10   0.06   0.00   0.10         0.    0.0000
    2    1    0   0.06   0.05   0.00   0.06         0.    0.0000
    3    5    0   0.47   0.22   0.00   0.47         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.009998
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6600s 
time spent in multnx:                   0.6600s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3500s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6700s 

          starting ci iteration  59

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.98289082    -0.00000123     0.00025076     0.00032678    -0.01811397     0.03724677     0.00016126    -0.00809829
 ref:   2     0.00000009     0.97742025     0.00323989    -0.07894240    -0.01050943    -0.03188783     0.11251310    -0.00893174
 ref:   3     0.00000031    -0.00167513     0.98090079     0.01446352     0.04406026     0.01678003     0.01086793     0.01712385

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 59  1   -114.3216887679  3.1264E-12  0.0000E+00  4.0288E-05  1.0000E-04
 mr-sdci # 59  2   -114.1774578097  5.6275E-12  0.0000E+00  7.7196E-05  1.0000E-04
 mr-sdci # 59  3   -113.9753964249  2.5638E-07  2.8730E-07  8.6761E-04  1.0000E-04
 mr-sdci # 59  4   -113.9433256206  1.1829E-08  0.0000E+00  1.6112E-02  1.0000E-04
 mr-sdci # 59  5   -113.7222053711  3.7842E-05  0.0000E+00  3.0856E-01  1.0000E-04
 mr-sdci # 59  6   -113.5299901426  1.2310E-01  0.0000E+00  9.8946E-01  1.0000E-04
 mr-sdci # 59  7   -112.3427548618  6.2948E-01  0.0000E+00  1.4087E+00  1.0000E-04
 mr-sdci # 59  8   -111.7036114056 -6.8409E-02  0.0000E+00  1.7041E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.09   0.05   0.00   0.09         0.    0.0000
    2    1    0   0.06   0.05   0.00   0.06         0.    0.0000
    3    5    0   0.47   0.20   0.00   0.47         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.030003
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6400s 
time spent in multnx:                   0.6400s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6700s 

          starting ci iteration  60

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.98289076    -0.00000118     0.00023727     0.00032867     0.01977551    -0.03205038
 ref:   2    -0.00000015     0.97742021     0.00324934    -0.07894367     0.00903432     0.02916037
 ref:   3    -0.00000034    -0.00167516     0.98090809     0.01446322    -0.04439237     0.00707044

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 60  1   -114.3216887679  6.5654E-12  0.0000E+00  4.0304E-05  1.0000E-04
 mr-sdci # 60  2   -114.1774578097  3.2401E-12  0.0000E+00  7.6990E-05  1.0000E-04
 mr-sdci # 60  3   -113.9753966129  1.8802E-07  2.6326E-08  6.5519E-04  1.0000E-04
 mr-sdci # 60  4   -113.9433256241  3.4685E-09  0.0000E+00  1.6108E-02  1.0000E-04
 mr-sdci # 60  5   -113.7253852852  3.1799E-03  0.0000E+00  3.0211E-01  1.0000E-04
 mr-sdci # 60  6   -112.5042975854 -1.0257E+00  0.0000E+00  1.6799E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.09   0.05   0.00   0.09         0.    0.0000
    2    1    0   0.06   0.05   0.00   0.06         0.    0.0000
    3    5    0   0.48   0.20   0.00   0.48         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.009998
time for cinew                         0.009998
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6500s 
time spent in multnx:                   0.6500s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6700s 

          starting ci iteration  61

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.98289074     0.00000119     0.00023432     0.00033069     0.02412730     0.03153844     0.01328848
 ref:   2     0.00000018    -0.97742023     0.00325397    -0.07894678     0.00157462    -0.05475585     0.00685372
 ref:   3     0.00000033     0.00167516     0.98090700     0.01446398    -0.04182779     0.02013860    -0.02134794

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 61  1   -114.3216887679  3.1264E-13  0.0000E+00  4.0329E-05  1.0000E-04
 mr-sdci # 61  2   -114.1774578097  2.8422E-14  0.0000E+00  7.7089E-05  1.0000E-04
 mr-sdci # 61  3   -113.9753966164  3.4624E-09 -2.5847E-08  7.0860E-04  1.0000E-04
 mr-sdci # 61  4   -113.9433256255  1.4486E-09  0.0000E+00  1.6103E-02  1.0000E-04
 mr-sdci # 61  5   -113.7288674075  3.4821E-03  0.0000E+00  3.5964E-01  1.0000E-04
 mr-sdci # 61  6   -113.5495676721  1.0453E+00  0.0000E+00  9.0475E-01  1.0000E-04
 mr-sdci # 61  7   -111.7918853547 -5.5087E-01  0.0000E+00  1.7000E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.09   0.04   0.00   0.09         0.    0.0000
    2    1    0   0.07   0.05   0.00   0.07         0.    0.0000
    3    5    0   0.47   0.20   0.00   0.47         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.009998
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6500s 
time spent in multnx:                   0.6500s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6600s 

          starting ci iteration  62

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.98289070     0.00000114     0.00022268    -0.00033264     0.02836030     0.03007834     0.00175435     0.01464189
 ref:   2    -0.00000005    -0.97742037     0.00323246     0.07894353     0.00377141    -0.03214476     0.11257171     0.03376586
 ref:   3    -0.00000031     0.00167515     0.98090383    -0.01446501    -0.03818828     0.02729319     0.01299681    -0.01866288

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 62  1   -114.3216887679  3.8938E-12  0.0000E+00  4.0188E-05  1.0000E-04
 mr-sdci # 62  2   -114.1774578097  4.7180E-12  0.0000E+00  7.7350E-05  1.0000E-04
 mr-sdci # 62  3   -113.9753967393  1.2290E-07  1.7293E-07  7.4269E-04  1.0000E-04
 mr-sdci # 62  4   -113.9433256284  2.9049E-09  0.0000E+00  1.6108E-02  1.0000E-04
 mr-sdci # 62  5   -113.7349826307  6.1152E-03  0.0000E+00  4.2184E-01  1.0000E-04
 mr-sdci # 62  6   -113.6040015610  5.4434E-02  0.0000E+00  8.6049E-01  1.0000E-04
 mr-sdci # 62  7   -112.2848060892  4.9292E-01  0.0000E+00  1.4446E+00  1.0000E-04
 mr-sdci # 62  8   -111.7628663932  5.9255E-02  0.0000E+00  1.7105E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.09   0.06   0.00   0.09         0.    0.0000
    2    1    0   0.06   0.05   0.00   0.06         0.    0.0000
    3    5    0   0.48   0.21   0.00   0.48         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.02   0.02   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6600s 
time spent in multnx:                   0.6600s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3500s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6800s 

          starting ci iteration  63

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.98289065     0.00000113    -0.00021057     0.00033700    -0.03115968    -0.01836586
 ref:   2     0.00000013    -0.97742036    -0.00324660    -0.07894821     0.00327409     0.05143376
 ref:   3     0.00000033     0.00167515    -0.98091062     0.01446342     0.03725204    -0.00799195

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 63  1   -114.3216887679  4.0075E-12  0.0000E+00  4.0180E-05  1.0000E-04
 mr-sdci # 63  2   -114.1774578097  1.7053E-13  0.0000E+00  7.7310E-05  1.0000E-04
 mr-sdci # 63  3   -113.9753968477  1.0844E-07 -2.8316E-08  6.5706E-04  1.0000E-04
 mr-sdci # 63  4   -113.9433256402  1.1795E-08  0.0000E+00  1.6101E-02  1.0000E-04
 mr-sdci # 63  5   -113.7578655245  2.2883E-02  0.0000E+00  3.7950E-01  1.0000E-04
 mr-sdci # 63  6   -112.5300082334 -1.0740E+00  0.0000E+00  1.6725E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.09   0.05   0.00   0.09         0.    0.0000
    2    1    0   0.07   0.04   0.00   0.07         0.    0.0000
    3    5    0   0.47   0.20   0.00   0.47         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.009998
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6600s 
time spent in multnx:                   0.6600s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6700s 

          starting ci iteration  64

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.98289066    -0.00000115    -0.00021240     0.00033712    -0.03145504     0.01962351    -0.00611619
 ref:   2    -0.00000011     0.97742043    -0.00323985    -0.07894865     0.00440215    -0.07250422     0.00109674
 ref:   3    -0.00000033    -0.00167518    -0.98091336     0.01446358     0.03679446     0.02751151     0.01525253

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 64  1   -114.3216887679 -2.8422E-14  0.0000E+00  4.0176E-05  1.0000E-04
 mr-sdci # 64  2   -114.1774578097  8.8107E-13  0.0000E+00  7.7665E-05  1.0000E-04
 mr-sdci # 64  3   -113.9753968535  5.7514E-09  2.8487E-08  5.8917E-04  1.0000E-04
 mr-sdci # 64  4   -113.9433256403  2.2055E-11  0.0000E+00  1.6100E-02  1.0000E-04
 mr-sdci # 64  5   -113.7579557508  9.0226E-05  0.0000E+00  3.8874E-01  1.0000E-04
 mr-sdci # 64  6   -113.4342747333  9.0427E-01  0.0000E+00  9.7709E-01  1.0000E-04
 mr-sdci # 64  7   -111.6705624189 -6.1424E-01  0.0000E+00  1.6972E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.09   0.05   0.00   0.09         0.    0.0000
    2    1    0   0.06   0.04   0.00   0.06         0.    0.0000
    3    5    0   0.48   0.20   0.00   0.48         0.    0.0000
    4   75    0   0.01   0.00   0.00   0.01         0.    0.0000
    5   45    0   0.02   0.02   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.009998
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6600s 
time spent in multnx:                   0.6600s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6700s 

          starting ci iteration  65

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.98289060     0.00000113    -0.00019550     0.00034825    -0.03598738     0.01366729     0.00056639     0.00644804
 ref:   2    -0.00000001    -0.97742046    -0.00322635    -0.07894125     0.00049663    -0.04072032    -0.11099893     0.00955698
 ref:   3     0.00000030     0.00167517    -0.98091013     0.01446681     0.02572877     0.04068581    -0.01798246    -0.01328808

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 65  1   -114.3216887679  5.9970E-12  0.0000E+00  3.9896E-05  1.0000E-04
 mr-sdci # 65  2   -114.1774578097  4.2633E-13  0.0000E+00  7.7706E-05  1.0000E-04
 mr-sdci # 65  3   -113.9753969770  1.2350E-07  1.6094E-07  6.8929E-04  1.0000E-04
 mr-sdci # 65  4   -113.9433256809  4.0667E-08  0.0000E+00  1.6122E-02  1.0000E-04
 mr-sdci # 65  5   -113.7940137246  3.6058E-02  0.0000E+00  4.6740E-01  1.0000E-04
 mr-sdci # 65  6   -113.5423504686  1.0808E-01  0.0000E+00  8.6403E-01  1.0000E-04
 mr-sdci # 65  7   -112.3206823099  6.5012E-01  0.0000E+00  1.4120E+00  1.0000E-04
 mr-sdci # 65  8   -111.6638175167 -9.9049E-02  0.0000E+00  1.7001E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.09   0.06   0.00   0.09         0.    0.0000
    2    1    0   0.06   0.04   0.00   0.06         0.    0.0000
    3    5    0   0.47   0.20   0.00   0.47         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.04   0.02   0.00   0.04         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.019997
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6600s 
time spent in multnx:                   0.6600s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3200s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6800s 

          starting ci iteration  66

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.98289056     0.00000120     0.00017305     0.00036355     0.03844076     0.01056497
 ref:   2     0.00000003    -0.97742052     0.00323748    -0.07894770    -0.00626694    -0.03246786
 ref:   3     0.00000032     0.00167512     0.98092448     0.01445908    -0.02615820    -0.00272400

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 66  1   -114.3216887679  2.5295E-12  0.0000E+00  3.9921E-05  1.0000E-04
 mr-sdci # 66  2   -114.1774578097  4.6612E-12  0.0000E+00  7.7894E-05  1.0000E-04
 mr-sdci # 66  3   -113.9753971060  1.2904E-07  7.9997E-10  6.4227E-04  1.0000E-04
 mr-sdci # 66  4   -113.9433257212  4.0287E-08  0.0000E+00  1.6100E-02  1.0000E-04
 mr-sdci # 66  5   -113.8408128946  4.6799E-02  0.0000E+00  3.4378E-01  1.0000E-04
 mr-sdci # 66  6   -112.3234224789 -1.2189E+00  0.0000E+00  1.6986E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.09   0.04   0.00   0.09         0.    0.0000
    2    1    0   0.06   0.05   0.00   0.06         0.    0.0000
    3    5    0   0.47   0.20   0.00   0.47         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.009998
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6500s 
time spent in multnx:                   0.6500s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6600s 

          starting ci iteration  67

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.98289056    -0.00000120     0.00017302     0.00036443     0.03856175    -0.00393207     0.00942216
 ref:   2     0.00000008     0.97742052     0.00323767    -0.07895322    -0.00920268     0.07085734     0.01485624
 ref:   3     0.00000029    -0.00167513     0.98092437     0.01446194    -0.02437727    -0.03912844    -0.03381730

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 67  1   -114.3216887679  6.2528E-13  0.0000E+00  3.9868E-05  1.0000E-04
 mr-sdci # 67  2   -114.1774578097  5.6843E-14  0.0000E+00  7.7915E-05  1.0000E-04
 mr-sdci # 67  3   -113.9753971060  4.6896E-12 -7.9963E-10  6.4375E-04  1.0000E-04
 mr-sdci # 67  4   -113.9433257248  3.6288E-09  0.0000E+00  1.6093E-02  1.0000E-04
 mr-sdci # 67  5   -113.8417017308  8.8884E-04  0.0000E+00  3.6440E-01  1.0000E-04
 mr-sdci # 67  6   -113.3513808123  1.0280E+00  0.0000E+00  9.8993E-01  1.0000E-04
 mr-sdci # 67  7   -111.6798225357 -6.4086E-01  0.0000E+00  1.6992E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.10   0.06   0.00   0.09         0.    0.0000
    2    1    0   0.05   0.04   0.00   0.05         0.    0.0000
    3    5    0   0.49   0.20   0.00   0.49         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.02   0.02   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.009998
time for cinew                         0.009998
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6600s 
time spent in multnx:                   0.6500s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3200s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6800s 

          starting ci iteration  68

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.98289052    -0.00000120    -0.00015381    -0.00039150     0.03912740    -0.00224872     0.00025488    -0.00945720
 ref:   2    -0.00000007     0.97742054    -0.00321979     0.07893505     0.00076715     0.05578480     0.10978063    -0.01746089
 ref:   3     0.00000023    -0.00167512    -0.98092153    -0.01446494    -0.01706157    -0.04540829     0.03501335     0.03295658

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 68  1   -114.3216887679  6.5370E-12  0.0000E+00  3.9516E-05  1.0000E-04
 mr-sdci # 68  2   -114.1774578097  1.7053E-13  0.0000E+00  7.7940E-05  1.0000E-04
 mr-sdci # 68  3   -113.9753972156  1.0961E-07  1.5203E-07  6.9846E-04  1.0000E-04
 mr-sdci # 68  4   -113.9433258432  1.1835E-07  0.0000E+00  1.6122E-02  1.0000E-04
 mr-sdci # 68  5   -113.8698568617  2.8155E-02  0.0000E+00  3.3832E-01  1.0000E-04
 mr-sdci # 68  6   -113.3762951630  2.4914E-02  0.0000E+00  9.9343E-01  1.0000E-04
 mr-sdci # 68  7   -112.1068446153  4.2702E-01  0.0000E+00  1.4815E+00  1.0000E-04
 mr-sdci # 68  8   -111.6795722592  1.5755E-02  0.0000E+00  1.7001E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.10   0.04   0.00   0.10         0.    0.0000
    2    1    0   0.05   0.04   0.00   0.05         0.    0.0000
    3    5    0   0.48   0.22   0.00   0.48         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.02   0.02   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.030003
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6600s 
time spent in multnx:                   0.6600s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3300s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6900s 

          starting ci iteration  69

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.98289051     0.00000126    -0.00013248     0.00041565    -0.03898319    -0.00360769
 ref:   2    -0.00000004    -0.97742069    -0.00323656    -0.07894803     0.00619692    -0.05541071
 ref:   3     0.00000024     0.00167506    -0.98093316     0.01445512     0.01750282    -0.00650742

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 69  1   -114.3216887679  5.4001E-13  0.0000E+00  3.9495E-05  1.0000E-04
 mr-sdci # 69  2   -114.1774578098  1.2022E-11  0.0000E+00  7.8239E-05  1.0000E-04
 mr-sdci # 69  3   -113.9753973288  1.1317E-07 -1.6103E-08  6.9928E-04  1.0000E-04
 mr-sdci # 69  4   -113.9433259042  6.0970E-08  0.0000E+00  1.6094E-02  1.0000E-04
 mr-sdci # 69  5   -113.8933255920  2.3469E-02  0.0000E+00  2.7103E-01  1.0000E-04
 mr-sdci # 69  6   -112.4085089446 -9.6779E-01  0.0000E+00  1.6731E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.09   0.06   0.00   0.09         0.    0.0000
    2    1    0   0.05   0.04   0.00   0.05         0.    0.0000
    3    5    0   0.48   0.21   0.00   0.48         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.03   0.02   0.00   0.03         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.009998
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6500s 
time spent in multnx:                   0.6500s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3300s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6600s 

          starting ci iteration  70

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.98289051    -0.00000126    -0.00013210    -0.00041561    -0.03902838     0.00454611     0.00011855
 ref:   2     0.00000002     0.97742071    -0.00323283     0.07894832     0.00533974     0.08616073    -0.01120499
 ref:   3    -0.00000023    -0.00167508    -0.98093536    -0.01445529     0.01797134    -0.03524780     0.04759128

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 70  1   -114.3216887679  0.0000E+00  0.0000E+00  3.9485E-05  1.0000E-04
 mr-sdci # 70  2   -114.1774578098  1.1369E-13  0.0000E+00  7.8341E-05  1.0000E-04
 mr-sdci # 70  3   -113.9753973305  1.7565E-09  1.6408E-08  6.7746E-04  1.0000E-04
 mr-sdci # 70  4   -113.9433259042  9.9476E-12  0.0000E+00  1.6094E-02  1.0000E-04
 mr-sdci # 70  5   -113.8934059763  8.0384E-05  0.0000E+00  2.6574E-01  1.0000E-04
 mr-sdci # 70  6   -113.2285088506  8.2000E-01  0.0000E+00  1.0694E+00  1.0000E-04
 mr-sdci # 70  7   -111.4616942935 -6.4515E-01  0.0000E+00  1.6803E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.09   0.06   0.00   0.09         0.    0.0000
    2    1    0   0.05   0.03   0.00   0.05         0.    0.0000
    3    5    0   0.48   0.22   0.00   0.48         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.02   0.02   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010002
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6400s 
time spent in multnx:                   0.6400s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3300s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.6600s 

          starting ci iteration  71

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.98289049     0.00000131     0.00010658     0.00047441    -0.03858610     0.00649254    -0.00358367    -0.00073796
 ref:   2     0.00000009    -0.97742058     0.00321853    -0.07893144    -0.00059889     0.07019493     0.09516219     0.01533155
 ref:   3    -0.00000018     0.00167516     0.98093024     0.01445711     0.01158622    -0.04303617     0.02726338     0.05857855

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 71  1   -114.3216887679  2.3022E-12  0.0000E+00  3.9330E-05  1.0000E-04
 mr-sdci # 71  2   -114.1774578098  8.5834E-12  0.0000E+00  7.7934E-05  1.0000E-04
 mr-sdci # 71  3   -113.9753974664  1.3585E-07  1.6860E-07  7.2252E-04  1.0000E-04
 mr-sdci # 71  4   -113.9433261146  2.1039E-07  0.0000E+00  1.6148E-02  1.0000E-04
 mr-sdci # 71  5   -113.9115828160  1.8177E-02  0.0000E+00  2.3047E-01  1.0000E-04
 mr-sdci # 71  6   -113.2646203904  3.6112E-02  0.0000E+00  1.0837E+00  1.0000E-04
 mr-sdci # 71  7   -112.1251539068  6.6346E-01  0.0000E+00  1.5032E+00  1.0000E-04
 mr-sdci # 71  8   -111.4014660535 -2.7811E-01  0.0000E+00  1.6563E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.11   0.07   0.00   0.10         0.    0.0000
    2    1    0   0.05   0.04   0.00   0.05         0.    0.0000
    3    5    0   0.48   0.23   0.00   0.48         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.02   0.02   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.019997
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6700s 
time spent in multnx:                   0.6600s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3700s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6900s 

          starting ci iteration  72

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.98289049    -0.00000137     0.00008227    -0.00053024     0.03800944     0.00823621
 ref:   2     0.00000010     0.97742073     0.00323082     0.07894421    -0.00275598     0.03951631
 ref:   3    -0.00000018    -0.00167509     0.98094132    -0.01444016    -0.01189900     0.01028803

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 72  1   -114.3216887679  1.1369E-13  0.0000E+00  3.9335E-05  1.0000E-04
 mr-sdci # 72  2   -114.1774578098  2.3050E-11  0.0000E+00  7.8146E-05  1.0000E-04
 mr-sdci # 72  3   -113.9753975952  1.2880E-07  1.2955E-08  6.8124E-04  1.0000E-04
 mr-sdci # 72  4   -113.9433262332  1.1860E-07  0.0000E+00  1.6104E-02  1.0000E-04
 mr-sdci # 72  5   -113.9235753652  1.1993E-02  0.0000E+00  1.8093E-01  1.0000E-04
 mr-sdci # 72  6   -112.3646534057 -8.9997E-01  0.0000E+00  1.6759E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.10   0.07   0.00   0.10         0.    0.0000
    2    1    0   0.05   0.04   0.00   0.05         0.    0.0000
    3    5    0   0.48   0.21   0.00   0.48         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.009998
time for cinew                         0.009998
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6500s 
time spent in multnx:                   0.6500s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3300s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6700s 

          starting ci iteration  73

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.98289050     0.00000136    -0.00008231    -0.00053108     0.03788312    -0.01075432    -0.00119229
 ref:   2     0.00000006    -0.97742074    -0.00323367     0.07894990    -0.00382821    -0.07910683     0.02360710
 ref:   3    -0.00000016     0.00167510    -0.98093977    -0.01444297    -0.01127494     0.03465306    -0.04936838

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 73  1   -114.3216887679  1.1369E-13  0.0000E+00  3.9320E-05  1.0000E-04
 mr-sdci # 73  2   -114.1774578098  8.5265E-14  0.0000E+00  7.8211E-05  1.0000E-04
 mr-sdci # 73  3   -113.9753975963  1.0888E-09 -1.2701E-08  6.9590E-04  1.0000E-04
 mr-sdci # 73  4   -113.9433262372  4.0438E-09  0.0000E+00  1.6099E-02  1.0000E-04
 mr-sdci # 73  5   -113.9237220504  1.4669E-04  0.0000E+00  1.8619E-01  1.0000E-04
 mr-sdci # 73  6   -113.1999980861  8.3534E-01  0.0000E+00  1.0831E+00  1.0000E-04
 mr-sdci # 73  7   -111.5177598578 -6.0739E-01  0.0000E+00  1.6855E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.10   0.06   0.00   0.10         0.    0.0000
    2    1    0   0.05   0.04   0.00   0.05         0.    0.0000
    3    5    0   0.47   0.21   0.00   0.47         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010002
time for cinew                         0.009998
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6500s 
time spent in multnx:                   0.6500s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3300s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6700s 

          starting ci iteration  74

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.98289049     0.00000139    -0.00006147    -0.00064082     0.03760056     0.01122500    -0.00415019    -0.00079431
 ref:   2     0.00000011    -0.97742060    -0.00321504     0.07891962     0.00156013     0.07372841     0.09706529     0.01369857
 ref:   3    -0.00000014     0.00167515    -0.98093613    -0.01443536    -0.00831081    -0.03675294     0.03044118    -0.05287566

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 74  1   -114.3216887679  8.8107E-13  0.0000E+00  3.9285E-05  1.0000E-04
 mr-sdci # 74  2   -114.1774578098  6.1675E-12  0.0000E+00  7.7724E-05  1.0000E-04
 mr-sdci # 74  3   -113.9753977031  1.0685E-07  1.3727E-07  6.6490E-04  1.0000E-04
 mr-sdci # 74  4   -113.9433265305  2.9326E-07  0.0000E+00  1.6144E-02  1.0000E-04
 mr-sdci # 74  5   -113.9309507930  7.2287E-03  0.0000E+00  1.5416E-01  1.0000E-04
 mr-sdci # 74  6   -113.2039230998  3.9250E-03  0.0000E+00  1.0945E+00  1.0000E-04
 mr-sdci # 74  7   -112.0116072518  4.9385E-01  0.0000E+00  1.5278E+00  1.0000E-04
 mr-sdci # 74  8   -111.5123326114  1.1087E-01  0.0000E+00  1.6813E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.10   0.06   0.00   0.10         0.    0.0000
    2    1    0   0.05   0.04   0.00   0.05         0.    0.0000
    3    5    0   0.47   0.20   0.00   0.47         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.03   0.02   0.00   0.03         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.009998
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6500s 
time spent in multnx:                   0.6500s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3200s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.6700s 

          starting ci iteration  75

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.98289049     0.00000139    -0.00004661     0.00076638    -0.03675508    -0.01541630
 ref:   2    -0.00000014    -0.97742080    -0.00322961    -0.07894377     0.00146145    -0.05612523
 ref:   3     0.00000014     0.00167515    -0.98093916     0.01441320     0.00767112     0.00380696

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 75  1   -114.3216887679  6.5370E-13  0.0000E+00  3.9325E-05  1.0000E-04
 mr-sdci # 75  2   -114.1774578098  1.9952E-11  0.0000E+00  7.7964E-05  1.0000E-04
 mr-sdci # 75  3   -113.9753977942  9.1056E-08 -5.3522E-09  6.2626E-04  1.0000E-04
 mr-sdci # 75  4   -113.9433267771  2.4661E-07  0.0000E+00  1.6108E-02  1.0000E-04
 mr-sdci # 75  5   -113.9357358952  4.7851E-03  0.0000E+00  1.3342E-01  1.0000E-04
 mr-sdci # 75  6   -112.5518189947 -6.5210E-01  0.0000E+00  1.5959E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.10   0.06   0.00   0.10         0.    0.0000
    2    1    0   0.05   0.04   0.00   0.05         0.    0.0000
    3    5    0   0.47   0.20   0.00   0.47         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.009998
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6500s 
time spent in multnx:                   0.6500s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3200s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.6700s 

          starting ci iteration  76

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.98289049    -0.00000139    -0.00004641     0.00076672    -0.03678146     0.01455368     0.00674193
 ref:   2    -0.00000013     0.97742077    -0.00322827    -0.07894242     0.00124679     0.08954824    -0.01673369
 ref:   3     0.00000013    -0.00167514    -0.98093973     0.01441260     0.00776003    -0.02919684     0.02740156

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 76  1   -114.3216887679  2.8422E-14  0.0000E+00  3.9327E-05  1.0000E-04
 mr-sdci # 76  2   -114.1774578098  1.1369E-13  0.0000E+00  7.7867E-05  1.0000E-04
 mr-sdci # 76  3   -113.9753977944  2.3198E-10  5.4147E-09  6.1972E-04  1.0000E-04
 mr-sdci # 76  4   -113.9433267773  2.2601E-10  0.0000E+00  1.6109E-02  1.0000E-04
 mr-sdci # 76  5   -113.9357415017  5.6065E-06  0.0000E+00  1.3235E-01  1.0000E-04
 mr-sdci # 76  6   -113.1600335968  6.0821E-01  0.0000E+00  1.1100E+00  1.0000E-04
 mr-sdci # 76  7   -111.7683131002 -2.4329E-01  0.0000E+00  1.6800E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.11   0.06   0.00   0.11         0.    0.0000
    2    1    0   0.06   0.04   0.00   0.06         0.    0.0000
    3    5    0   0.48   0.20   0.00   0.48         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.02   0.02   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010002
time for cinew                         0.010002
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6700s 
time spent in multnx:                   0.6700s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3200s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6900s 

          starting ci iteration  77

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.98289049    -0.00000140    -0.00003029     0.00117309    -0.03623874     0.01593728    -0.00727766    -0.00714413
 ref:   2     0.00000013     0.97742063    -0.00321535    -0.07889154    -0.00243237     0.07947874     0.08341586     0.02097765
 ref:   3    -0.00000013    -0.00167519    -0.98093639     0.01436612     0.00599885    -0.03227530     0.02245409    -0.02621283

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 77  1   -114.3216887679  2.8422E-14  0.0000E+00  3.9327E-05  1.0000E-04
 mr-sdci # 77  2   -114.1774578098  1.0800E-11  0.0000E+00  7.7116E-05  1.0000E-04
 mr-sdci # 77  3   -113.9753978862  9.1799E-08  1.0293E-07  5.7258E-04  1.0000E-04
 mr-sdci # 77  4   -113.9433276546  8.7732E-07  0.0000E+00  1.6271E-02  1.0000E-04
 mr-sdci # 77  5   -113.9397422284  4.0007E-03  0.0000E+00  1.0989E-01  1.0000E-04
 mr-sdci # 77  6   -113.1768601392  1.6827E-02  0.0000E+00  1.1293E+00  1.0000E-04
 mr-sdci # 77  7   -112.1383475692  3.7003E-01  0.0000E+00  1.5238E+00  1.0000E-04
 mr-sdci # 77  8   -111.7672970346  2.5496E-01  0.0000E+00  1.6789E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.10   0.06   0.00   0.10         0.    0.0000
    2    1    0   0.06   0.03   0.00   0.06         0.    0.0000
    3    5    0   0.48   0.20   0.00   0.48         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.029999
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6600s 
time spent in multnx:                   0.6600s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6900s 

          starting ci iteration  78

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.98289048     0.00000138     0.00001960     0.00269863    -0.03555296    -0.01521644
 ref:   2     0.00000016    -0.97742076     0.00322379    -0.07881267    -0.00423179    -0.03862781
 ref:   3    -0.00000014     0.00167523     0.98093553     0.01415216     0.00568763     0.01341518

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 78  1   -114.3216887680  1.4211E-12  0.0000E+00  3.9355E-05  1.0000E-04
 mr-sdci # 78  2   -114.1774578098  1.8815E-11  0.0000E+00  7.7303E-05  1.0000E-04
 mr-sdci # 78  3   -113.9753979565  7.0279E-08  6.8499E-09  4.9676E-04  1.0000E-04
 mr-sdci # 78  4   -113.9433305866  2.9320E-06  0.0000E+00  1.6867E-02  1.0000E-04
 mr-sdci # 78  5   -113.9422358282  2.4936E-03  0.0000E+00  9.2899E-02  1.0000E-04
 mr-sdci # 78  6   -112.5010286418 -6.7583E-01  0.0000E+00  1.6210E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.10   0.05   0.00   0.10         0.    0.0000
    2    1    0   0.05   0.03   0.00   0.05         0.    0.0000
    3    5    0   0.48   0.22   0.00   0.48         0.    0.0000
    4   75    0   0.01   0.00   0.00   0.01         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.019997
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6600s 
time spent in multnx:                   0.6600s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6800s 

          starting ci iteration  79

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.98289048     0.00000138     0.00001983     0.00273608    -0.03548413     0.01695359    -0.00450801
 ref:   2    -0.00000015    -0.97742076     0.00322583    -0.07883262    -0.00393291     0.08038044     0.02712303
 ref:   3     0.00000014     0.00167523     0.98093480     0.01415484     0.00555782    -0.02917166    -0.01067983

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 79  1   -114.3216887680  2.8422E-14  0.0000E+00  3.9356E-05  1.0000E-04
 mr-sdci # 79  2   -114.1774578098  2.8422E-14  0.0000E+00  7.7315E-05  1.0000E-04
 mr-sdci # 79  3   -113.9753979571  5.6642E-10 -6.6622E-09  5.0586E-04  1.0000E-04
 mr-sdci # 79  4   -113.9433306670  8.0367E-08  0.0000E+00  1.6904E-02  1.0000E-04
 mr-sdci # 79  5   -113.9422559169  2.0089E-05  0.0000E+00  9.4604E-02  1.0000E-04
 mr-sdci # 79  6   -113.1647618310  6.6373E-01  0.0000E+00  1.1072E+00  1.0000E-04
 mr-sdci # 79  7   -111.8025188168 -3.3583E-01  0.0000E+00  1.6802E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.10   0.06   0.00   0.10         0.    0.0000
    2    1    0   0.05   0.03   0.00   0.05         0.    0.0000
    3    5    0   0.47   0.21   0.00   0.47         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.02   0.02   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010002
time for cinew                         0.009998
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6500s 
time spent in multnx:                   0.6500s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3300s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6700s 

          starting ci iteration  80

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.98289048     0.00000138     0.00001161    -0.03508267    -0.00412780     0.01738818    -0.00518859    -0.00762668
 ref:   2     0.00000013    -0.97742061     0.00321280     0.00901071    -0.07841583     0.07633641     0.07030097     0.06460717
 ref:   3    -0.00000014     0.00167525     0.98093330     0.00197725     0.01486453    -0.02987283     0.01587585    -0.00387052

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 80  1   -114.3216887680  1.7053E-13  0.0000E+00  3.9349E-05  1.0000E-04
 mr-sdci # 80  2   -114.1774578099  7.1054E-12  0.0000E+00  7.6639E-05  1.0000E-04
 mr-sdci # 80  3   -113.9753980061  4.9084E-08  5.9070E-08  4.4481E-04  1.0000E-04
 mr-sdci # 80  4   -113.9438541389  5.2347E-04  0.0000E+00  7.9077E-02  1.0000E-04
 mr-sdci # 80  5   -113.9433172245  1.0613E-03  0.0000E+00  1.9297E-02  1.0000E-04
 mr-sdci # 80  6   -113.1671054381  2.3436E-03  0.0000E+00  1.1163E+00  1.0000E-04
 mr-sdci # 80  7   -112.1633339148  3.6082E-01  0.0000E+00  1.5431E+00  1.0000E-04
 mr-sdci # 80  8   -111.7137536903 -5.3543E-02  0.0000E+00  1.6615E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.10   0.05   0.00   0.10         0.    0.0000
    2    1    0   0.06   0.05   0.00   0.06         0.    0.0000
    3    5    0   0.48   0.21   0.00   0.48         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.009998
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6700s 
time spent in multnx:                   0.6700s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3300s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.7000s 

          starting ci iteration  81

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.98289047     0.00000135    -0.00000753    -0.03480611     0.00102087     0.01854570
 ref:   2    -0.00000018    -0.97742075    -0.00322169     0.00364123     0.07883515     0.05474162
 ref:   3     0.00000016     0.00167530    -0.98093057     0.00249140    -0.01460017    -0.02100323

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 81  1   -114.3216887680  1.2221E-12  0.0000E+00  3.9395E-05  1.0000E-04
 mr-sdci # 81  2   -114.1774578099  1.0601E-11  0.0000E+00  7.6797E-05  1.0000E-04
 mr-sdci # 81  3   -113.9753980433  3.7104E-08 -2.6037E-09  3.9306E-04  1.0000E-04
 mr-sdci # 81  4   -113.9448946924  1.0406E-03  0.0000E+00  7.2754E-02  1.0000E-04
 mr-sdci # 81  5   -113.9433234272  6.2026E-06  0.0000E+00  1.6575E-02  1.0000E-04
 mr-sdci # 81  6   -112.5985163489 -5.6859E-01  0.0000E+00  1.5803E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.11   0.06   0.00   0.11         0.    0.0000
    2    1    0   0.05   0.04   0.00   0.05         0.    0.0000
    3    5    0   0.48   0.21   0.00   0.48         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010002
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6700s 
time spent in multnx:                   0.6700s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3300s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.6900s 

          starting ci iteration  82

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.98289047     0.00000135    -0.00000734    -0.03483693     0.00101768    -0.01825862     0.00718624
 ref:   2     0.00000018    -0.97742074    -0.00322064     0.00343014     0.07884352    -0.08975965    -0.02013202
 ref:   3    -0.00000016     0.00167530    -0.98093087     0.00254889    -0.01460205     0.02774833    -0.00000414

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 82  1   -114.3216887680  0.0000E+00  0.0000E+00  3.9393E-05  1.0000E-04
 mr-sdci # 82  2   -114.1774578099 -5.6843E-14  0.0000E+00  7.6764E-05  1.0000E-04
 mr-sdci # 82  3   -113.9753980434  1.4253E-10  2.6421E-09  3.8801E-04  1.0000E-04
 mr-sdci # 82  4   -113.9448997527  5.0603E-06  0.0000E+00  7.1838E-02  1.0000E-04
 mr-sdci # 82  5   -113.9433234350  7.8304E-09  0.0000E+00  1.6555E-02  1.0000E-04
 mr-sdci # 82  6   -113.1410847014  5.4257E-01  0.0000E+00  1.1270E+00  1.0000E-04
 mr-sdci # 82  7   -111.8723316739 -2.9100E-01  0.0000E+00  1.6657E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.10   0.05   0.00   0.10         0.    0.0000
    2    1    0   0.06   0.05   0.00   0.06         0.    0.0000
    3    5    0   0.47   0.22   0.00   0.47         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.03   0.01   0.00   0.03         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6600s 
time spent in multnx:                   0.6600s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3300s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6900s 

          starting ci iteration  83

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.98289046    -0.00000133    -0.00000328    -0.03450622     0.00046133    -0.01988960     0.00785857    -0.01017112
 ref:   2    -0.00000015     0.97742061    -0.00321280     0.00092887     0.07892372    -0.07957086    -0.07373666     0.04323932
 ref:   3     0.00000017    -0.00167533    -0.98092895     0.00220186    -0.01454419     0.02934784    -0.00925680     0.00322109

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 83  1   -114.3216887680  7.6739E-13  0.0000E+00  3.9370E-05  1.0000E-04
 mr-sdci # 83  2   -114.1774578099  8.2991E-12  0.0000E+00  7.5985E-05  1.0000E-04
 mr-sdci # 83  3   -113.9753980760  3.2575E-08  3.5278E-08  3.3981E-04  1.0000E-04
 mr-sdci # 83  4   -113.9457649979  8.6525E-04  0.0000E+00  6.3141E-02  1.0000E-04
 mr-sdci # 83  5   -113.9433245289  1.0939E-06  0.0000E+00  1.6237E-02  1.0000E-04
 mr-sdci # 83  6   -113.1585212277  1.7437E-02  0.0000E+00  1.1459E+00  1.0000E-04
 mr-sdci # 83  7   -112.2036659685  3.3133E-01  0.0000E+00  1.5298E+00  1.0000E-04
 mr-sdci # 83  8   -111.8397905893  1.2604E-01  0.0000E+00  1.6541E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.11   0.07   0.00   0.11         0.    0.0000
    2    1    0   0.06   0.05   0.00   0.06         0.    0.0000
    3    5    0   0.47   0.20   0.00   0.47         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.03   0.01   0.00   0.03         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020004
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6700s 
time spent in multnx:                   0.6700s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3300s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6900s 

          starting ci iteration  84

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.98289045     0.00000131     0.00000114     0.03419676     0.00025920    -0.01638868
 ref:   2     0.00000018    -0.97742069     0.00321731    -0.00120532     0.07890951    -0.03720096
 ref:   3    -0.00000019     0.00167537     0.98092673    -0.00177567    -0.01451902     0.02018252

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 84  1   -114.3216887680  1.5348E-12  0.0000E+00  3.9394E-05  1.0000E-04
 mr-sdci # 84  2   -114.1774578099  7.4181E-12  0.0000E+00  7.6115E-05  1.0000E-04
 mr-sdci # 84  3   -113.9753980989  2.2948E-08  1.9812E-09  2.8066E-04  1.0000E-04
 mr-sdci # 84  4   -113.9463099934  5.4500E-04  0.0000E+00  5.7987E-02  1.0000E-04
 mr-sdci # 84  5   -113.9433249525  4.2366E-07  0.0000E+00  1.6231E-02  1.0000E-04
 mr-sdci # 84  6   -112.4975253413 -6.6100E-01  0.0000E+00  1.6325E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.11   0.07   0.00   0.11         0.    0.0000
    2    1    0   0.06   0.04   0.00   0.06         0.    0.0000
    3    5    0   0.49   0.20   0.00   0.49         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.02   0.02   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010002
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6900s 
time spent in multnx:                   0.6900s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3400s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.7000s 

          starting ci iteration  85

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.98289045    -0.00000131    -0.00000132    -0.03417418    -0.00025764     0.01871585    -0.00427880
 ref:   2    -0.00000020     0.97742070    -0.00321837     0.00132406    -0.07890535     0.07997254     0.02897048
 ref:   3     0.00000019    -0.00167537    -0.98092640     0.00173746     0.01451764    -0.02851972    -0.00039373

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 85  1   -114.3216887680  1.4211E-13  0.0000E+00  3.9389E-05  1.0000E-04
 mr-sdci # 85  2   -114.1774578099  8.5265E-14  0.0000E+00  7.6143E-05  1.0000E-04
 mr-sdci # 85  3   -113.9753980991  1.5487E-10 -1.9330E-09  2.8519E-04  1.0000E-04
 mr-sdci # 85  4   -113.9463119331  1.9397E-06  0.0000E+00  5.8439E-02  1.0000E-04
 mr-sdci # 85  5   -113.9433249549  2.3340E-09  0.0000E+00  1.6238E-02  1.0000E-04
 mr-sdci # 85  6   -113.1520635130  6.5454E-01  0.0000E+00  1.1233E+00  1.0000E-04
 mr-sdci # 85  7   -111.8029985878 -4.0067E-01  0.0000E+00  1.6788E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.10   0.06   0.00   0.10         0.    0.0000
    2    1    0   0.07   0.06   0.00   0.07         0.    0.0000
    3    5    0   0.47   0.20   0.00   0.47         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.03   0.02   0.00   0.03         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.009998
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6700s 
time spent in multnx:                   0.6700s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3400s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.6900s 

          starting ci iteration  86

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.98289044     0.00000130    -0.00000028     0.03401558     0.00016701     0.01925896    -0.00691014     0.00859068
 ref:   2     0.00000015    -0.97742059     0.00321121    -0.00004089     0.07893274     0.07557702     0.06805837    -0.06794545
 ref:   3    -0.00000020     0.00167539     0.98092541    -0.00152286    -0.01450770    -0.02905914     0.00856680    -0.00430209

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 86  1   -114.3216887680  6.2528E-13  0.0000E+00  3.9364E-05  1.0000E-04
 mr-sdci # 86  2   -114.1774578099  3.8369E-12  0.0000E+00  7.5614E-05  1.0000E-04
 mr-sdci # 86  3   -113.9753981137  1.4669E-08  1.7484E-08  2.4601E-04  1.0000E-04
 mr-sdci # 86  4   -113.9466513268  3.3939E-04  0.0000E+00  5.3241E-02  1.0000E-04
 mr-sdci # 86  5   -113.9433251448  1.8995E-07  0.0000E+00  1.6186E-02  1.0000E-04
 mr-sdci # 86  6   -113.1548273845  2.7639E-03  0.0000E+00  1.1332E+00  1.0000E-04
 mr-sdci # 86  7   -112.1790724193  3.7607E-01  0.0000E+00  1.5446E+00  1.0000E-04
 mr-sdci # 86  8   -111.6985918353 -1.4120E-01  0.0000E+00  1.6617E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.10   0.05   0.00   0.10         0.    0.0000
    2    1    0   0.06   0.05   0.00   0.06         0.    0.0000
    3    5    0   0.49   0.21   0.00   0.49         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.03   0.02   0.00   0.03         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6800s 
time spent in multnx:                   0.6800s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3300s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.7000s 

          starting ci iteration  87

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.98289043    -0.00000127     0.00000050     0.03378776    -0.00011099     0.01823116
 ref:   2    -0.00000020     0.97742067    -0.00321583    -0.00059745    -0.07891725     0.05332620
 ref:   3     0.00000022    -0.00167542    -0.98092351    -0.00120582     0.01449778    -0.02269027

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 87  1   -114.3216887680  1.2506E-12  0.0000E+00  3.9408E-05  1.0000E-04
 mr-sdci # 87  2   -114.1774578099  3.4674E-12  0.0000E+00  7.5705E-05  1.0000E-04
 mr-sdci # 87  3   -113.9753981245  1.0719E-08 -7.8551E-10  2.1251E-04  1.0000E-04
 mr-sdci # 87  4   -113.9468683046  2.1698E-04  0.0000E+00  5.2178E-02  1.0000E-04
 mr-sdci # 87  5   -113.9433252659  1.2111E-07  0.0000E+00  1.6192E-02  1.0000E-04
 mr-sdci # 87  6   -112.5918272999 -5.6300E-01  0.0000E+00  1.5964E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.11   0.08   0.00   0.11         0.    0.0000
    2    1    0   0.06   0.04   0.00   0.06         0.    0.0000
    3    5    0   0.48   0.21   0.00   0.48         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.009998
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6800s 
time spent in multnx:                   0.6800s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3500s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6900s 

          starting ci iteration  88

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.98289043    -0.00000127    -0.00000060    -0.03380758    -0.00011099    -0.01832442     0.00660383
 ref:   2     0.00000022     0.97742067     0.00321524     0.00046831    -0.07891905    -0.08913935    -0.02206314
 ref:   3    -0.00000022    -0.00167542     0.98092366     0.00123969     0.01449824     0.02757044    -0.00269323

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 88  1   -114.3216887680  1.4211E-13  0.0000E+00  3.9401E-05  1.0000E-04
 mr-sdci # 88  2   -114.1774578099  8.5265E-14  0.0000E+00  7.5701E-05  1.0000E-04
 mr-sdci # 88  3   -113.9753981245  4.6271E-11  7.9641E-10  2.0970E-04  1.0000E-04
 mr-sdci # 88  4   -113.9468704155  2.1108E-06  0.0000E+00  5.1747E-02  1.0000E-04
 mr-sdci # 88  5   -113.9433252663  4.0811E-10  0.0000E+00  1.6189E-02  1.0000E-04
 mr-sdci # 88  6   -113.1264826060  5.3466E-01  0.0000E+00  1.1480E+00  1.0000E-04
 mr-sdci # 88  7   -111.8639152053 -3.1516E-01  0.0000E+00  1.6657E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.11   0.05   0.00   0.10         0.    0.0000
    2    1    0   0.06   0.04   0.00   0.06         0.    0.0000
    3    5    0   0.49   0.23   0.00   0.49         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.03   0.02   0.00   0.03         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6900s 
time spent in multnx:                   0.6800s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3400s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.7100s 

          starting ci iteration  89

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.98289042    -0.00000126    -0.00000093     0.03364096    -0.00007032    -0.02002269     0.00918479     0.00976993
 ref:   2     0.00000017     0.97742059     0.00321108     0.00019179    -0.07893207    -0.07906316    -0.07348059    -0.04367011
 ref:   3    -0.00000023    -0.00167544     0.98092251    -0.00103881     0.01449223     0.02903876    -0.00802257    -0.00546370

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 89  1   -114.3216887680  1.2221E-12  0.0000E+00  3.9372E-05  1.0000E-04
 mr-sdci # 89  2   -114.1774578099  3.3538E-12  0.0000E+00  7.5195E-05  1.0000E-04
 mr-sdci # 89  3   -113.9753981335  9.0415E-09  9.8572E-09  1.8214E-04  1.0000E-04
 mr-sdci # 89  4   -113.9470477715  1.7736E-04  0.0000E+00  4.9194E-02  1.0000E-04
 mr-sdci # 89  5   -113.9433253555  8.9184E-08  0.0000E+00  1.6156E-02  1.0000E-04
 mr-sdci # 89  6   -113.1435319521  1.7049E-02  0.0000E+00  1.1677E+00  1.0000E-04
 mr-sdci # 89  7   -112.1989393709  3.3502E-01  0.0000E+00  1.5298E+00  1.0000E-04
 mr-sdci # 89  8   -111.8350191558  1.3643E-01  0.0000E+00  1.6586E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.10   0.05   0.00   0.10         0.    0.0000
    2    1    0   0.06   0.03   0.00   0.06         0.    0.0000
    3    5    0   0.49   0.22   0.00   0.49         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.03   0.02   0.00   0.03         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6800s 
time spent in multnx:                   0.6800s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3200s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.7000s 

          starting ci iteration  90

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     62916 2x:      9184 4x:      1008
All internal counts: zz :     59780 yy:    295468 xx:         0 ww:         0
One-external counts: yz :     99960 yx:         0 yw:         0
Two-external counts: yy :     38332 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     74102    task #   2:     49275    task #   3:    238006    task #   4:      8071
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.98289040     0.00000124    -0.00000092    -0.03351391    -0.00004444     0.01488906
 ref:   2     0.00000020    -0.97742063     0.00321338     0.00006109    -0.07892459     0.03559189
 ref:   3    -0.00000025     0.00167546     0.98092122     0.00084761     0.01448680    -0.02007945

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 90  1   -114.3216887680  1.9043E-12  0.0000E+00  3.9401E-05  1.0000E-04
 mr-sdci # 90  2   -114.1774578099  2.4158E-12  0.0000E+00  7.5281E-05  1.0000E-04
 mr-sdci # 90  3   -113.9753981399  6.3172E-09  5.2728E-10  1.4966E-04  1.0000E-04
 mr-sdci # 90  4   -113.9471535378  1.0577E-04  0.0000E+00  4.8815E-02  1.0000E-04
 mr-sdci # 90  5   -113.9433254176  6.2100E-08  0.0000E+00  1.6169E-02  1.0000E-04
 mr-sdci # 90  6   -112.5226961740 -6.2084E-01  0.0000E+00  1.6410E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.11   0.07   0.00   0.11         0.    0.0000
    2    1    0   0.06   0.04   0.00   0.06         0.    0.0000
    3    5    0   0.48   0.22   0.00   0.48         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.009998
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6800s 
time spent in multnx:                   0.6800s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3500s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6900s 

 mr-sdci  convergence not reached after 90 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 90  1   -114.3216887680  1.9043E-12  0.0000E+00  3.9401E-05  1.0000E-04
 mr-sdci # 90  2   -114.1774578099  2.4158E-12  0.0000E+00  7.5281E-05  1.0000E-04
 mr-sdci # 90  3   -113.9753981399  6.3172E-09  5.2728E-10  1.4966E-04  1.0000E-04
 mr-sdci # 90  4   -113.9471535378  1.0577E-04  0.0000E+00  4.8815E-02  1.0000E-04
 mr-sdci # 90  5   -113.9433254176  6.2100E-08  0.0000E+00  1.6169E-02  1.0000E-04
 mr-sdci # 90  6   -112.5226961740 -6.2084E-01  0.0000E+00  1.6410E+00  1.0000E-04

####################CIUDGINFO####################

   ci vector at position   1 energy= -114.321688767960
   ci vector at position   2 energy= -114.177457809890
   ci vector at position   3 energy= -113.975398139866

################END OF CIUDGINFO################

 
    3 of the   7 expansion vectors are transformed.
    3 of the   6 matrix-vector products are transformed.

    3 expansion eigenvectors written to unit nvfile (= 11)
    3 matrix-vector products written to unit nhvfil (= 10)
 maximum overlap with reference            1 (overlap=  0.982890404037474      )

information on vector: 1from unit 11 written to unit 16filename civout                                                      
 maximum overlap with reference            2 (overlap=  0.977420632414803      )

information on vector: 2from unit 11 written to unit 16filename civout                                                      
 maximum overlap with reference            3 (overlap=  0.980921224929882      )

information on vector: 3from unit 11 written to unit 16filename civout                                                      


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =      -114.3216887680

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7    8    9   10

                                          orbital     1    2    3    4    5    6    7    8    9   10

                                         symmetry   a    a    a    a    a    a    a    a    a    a  

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1  0.941346                        +-   +-   +-   +-   +-   +-   +-   +-           
 z*  1  1       3 -0.049023                        +-   +-   +-   +-   +-   +-   +-   +          - 
 z*  1  1       4 -0.020128                        +-   +-   +-   +-   +-   +-   +-        +-      
 z*  1  1       6 -0.038664                        +-   +-   +-   +-   +-   +-   +-             +- 
 z*  1  1      15 -0.018172                        +-   +-   +-   +-   +-   +-        +-   +-      
 z*  1  1      17 -0.014442                        +-   +-   +-   +-   +-   +-        +-        +- 
 z*  1  1      21 -0.213674                        +-   +-   +-   +-   +-   +    +-   +-    -      
 z*  1  1      24 -0.043927                        +-   +-   +-   +-   +-   +    +-    -   +     - 
 z*  1  1      26  0.050059                        +-   +-   +-   +-   +-   +    +-   +     -    - 
 z*  1  1      28 -0.010153                        +-   +-   +-   +-   +-   +    +-         -   +- 
 z*  1  1      41 -0.139471                        +-   +-   +-   +-   +-        +-   +-   +-      
 z*  1  1      43 -0.017894                        +-   +-   +-   +-   +-        +-   +-        +- 
 z*  1  1      44 -0.010075                        +-   +-   +-   +-   +-        +-   +    +-    - 
 z*  1  1      52 -0.014983                        +-   +-   +-   +-   +    +-   +-   +-         - 
 z*  1  1      53  0.012443                        +-   +-   +-   +-   +    +-   +-    -   +-      
 z*  1  1      55 -0.017796                        +-   +-   +-   +-   +    +-   +-    -        +- 
 z*  1  1      72 -0.024333                        +-   +-   +-   +-   +     -   +-   +-   +     - 
 z*  1  1      81 -0.020667                        +-   +-   +-   +-   +    +    +-   +-    -    - 
 z*  1  1      91 -0.021977                        +-   +-   +-   +-        +-   +-   +-   +-      
 z*  1  1      93 -0.018892                        +-   +-   +-   +-        +-   +-   +-        +- 
 z*  1  1     107  0.010462                        +-   +-   +-   +    +-   +-   +-   +-         - 
 z*  1  1     110  0.021134                        +-   +-   +-   +    +-   +-   +-    -        +- 
 z*  1  1     127  0.039863                        +-   +-   +-   +    +-    -   +-   +-   +     - 
 z*  1  1     146 -0.015722                        +-   +-   +-   +     -   +-   +-   +-   +-      
 z*  1  1     176 -0.024323                        +-   +-   +-        +-   +-   +-   +-   +-      
 z*  1  1     178 -0.017352                        +-   +-   +-        +-   +-   +-   +-        +- 
 y   1  1     371 -0.013803             13( a  )   +-   +-   +-   +-   +-   +-   +-         -      
 y   1  1     383  0.014864              3( a  )   +-   +-   +-   +-   +-   +-   +-              - 
 y   1  1     392 -0.012259             12( a  )   +-   +-   +-   +-   +-   +-   +-              - 
 y   1  1     404 -0.044620              2( a  )   +-   +-   +-   +-   +-   +-    -   +-           
 y   1  1     407  0.023223              5( a  )   +-   +-   +-   +-   +-   +-    -   +-           
 y   1  1     410 -0.020113              8( a  )   +-   +-   +-   +-   +-   +-    -   +-           
 y   1  1     413 -0.013044             11( a  )   +-   +-   +-   +-   +-   +-    -   +-           
 y   1  1     441  0.010954             17( a  )   +-   +-   +-   +-   +-   +-    -   +     -      
 y   1  1     448 -0.020615              2( a  )   +-   +-   +-   +-   +-   +-    -   +          - 
 y   1  1     451  0.026934              5( a  )   +-   +-   +-   +-   +-   +-    -   +          - 
 y   1  1     454 -0.018287              8( a  )   +-   +-   +-   +-   +-   +-    -   +          - 
 y   1  1     457 -0.022077             11( a  )   +-   +-   +-   +-   +-   +-    -   +          - 
 y   1  1     462 -0.019570             16( a  )   +-   +-   +-   +-   +-   +-    -   +          - 
 y   1  1     786  0.012020             10( a  )   +-   +-   +-   +-   +-    -   +-   +-           
 y   1  1     789 -0.011763             13( a  )   +-   +-   +-   +-   +-    -   +-   +-           
 y   1  1     813 -0.014140             15( a  )   +-   +-   +-   +-   +-    -   +-   +     -      
 y   1  1     824 -0.013483              4( a  )   +-   +-   +-   +-   +-    -   +-   +          - 
 y   1  1     830  0.023940             10( a  )   +-   +-   +-   +-   +-    -   +-   +          - 
 y   1  1     910  0.013812              2( a  )   +-   +-   +-   +-   +-    -   +    +-    -      
 y   1  1     913 -0.012776              5( a  )   +-   +-   +-   +-   +-    -   +    +-    -      
 y   1  1     916  0.010454              8( a  )   +-   +-   +-   +-   +-    -   +    +-    -      
 y   1  1    1223 -0.010441              7( a  )   +-   +-   +-   +-   +-   +    +-    -    -      
 y   1  1    1284 -0.028720              2( a  )   +-   +-   +-   +-   +-   +     -   +-    -      
 y   1  1    1287  0.033814              5( a  )   +-   +-   +-   +-   +-   +     -   +-    -      
 y   1  1    1290 -0.019673              8( a  )   +-   +-   +-   +-   +-   +     -   +-    -      
 y   1  1    1293 -0.026773             11( a  )   +-   +-   +-   +-   +-   +     -   +-    -      
 y   1  1    1298 -0.015166             16( a  )   +-   +-   +-   +-   +-   +     -   +-    -      
 y   1  1    1550 -0.012261              4( a  )   +-   +-   +-   +-   +-        +-   +-    -      
 y   1  1    1556  0.032041             10( a  )   +-   +-   +-   +-   +-        +-   +-    -      
 y   1  1    1666  0.010902             10( a  )   +-   +-   +-   +-   +-        +-   +     -    - 
 y   1  1    1987  0.013470              1( a  )   +-   +-   +-   +-    -   +-   +-   +-           
 y   1  1    2031  0.010341              1( a  )   +-   +-   +-   +-    -   +-   +-   +          - 
 y   1  1    2037  0.016872              7( a  )   +-   +-   +-   +-    -   +-   +-   +          - 
 y   1  1    2042  0.014416             12( a  )   +-   +-   +-   +-    -   +-   +-   +          - 
 y   1  1    2427  0.016696              1( a  )   +-   +-   +-   +-    -   +    +-   +-    -      
 y   1  1    2429 -0.011422              3( a  )   +-   +-   +-   +-    -   +    +-   +-    -      
 y   1  1    2432 -0.011767              6( a  )   +-   +-   +-   +-    -   +    +-   +-    -      
 y   1  1    2433  0.011255              7( a  )   +-   +-   +-   +-    -   +    +-   +-    -      
 y   1  1    2438  0.014610             12( a  )   +-   +-   +-   +-    -   +    +-   +-    -      
 y   1  1    3099  0.024023             13( a  )   +-   +-   +-   +-   +    +-   +-    -    -      
 y   1  1    3120  0.010121             12( a  )   +-   +-   +-   +-   +    +-   +-    -         - 
 y   1  1    3130 -0.013032             22( a  )   +-   +-   +-   +-   +    +-   +-    -         - 
 y   1  1    3166 -0.014130             14( a  )   +-   +-   +-   +-   +    +-    -   +-    -      
 y   1  1    3169 -0.022384             17( a  )   +-   +-   +-   +-   +    +-    -   +-    -      
 y   1  1    3179  0.013703              5( a  )   +-   +-   +-   +-   +    +-    -   +-         - 
 y   1  1    3185 -0.020894             11( a  )   +-   +-   +-   +-   +    +-    -   +-         - 
 y   1  1    3195 -0.010477             21( a  )   +-   +-   +-   +-   +    +-    -   +-         - 
 y   1  1    3417  0.014744              1( a  )   +-   +-   +-   +-   +     -   +-   +-    -      
 y   1  1    3422  0.010250              6( a  )   +-   +-   +-   +-   +     -   +-   +-    -      
 y   1  1    3423  0.016438              7( a  )   +-   +-   +-   +-   +     -   +-   +-    -      
 y   1  1    3431  0.024897             15( a  )   +-   +-   +-   +-   +     -   +-   +-    -      
 y   1  1    3448  0.017184             10( a  )   +-   +-   +-   +-   +     -   +-   +-         - 
 y   1  1    4957  0.012245              1( a  )   +-   +-   +-    -   +-   +-   +-   +-           
 y   1  1    5397  0.016600              1( a  )   +-   +-   +-    -   +-   +    +-   +-    -      
 y   1  1    7365  0.012070             11( a  )   +-   +-   +-   +    +-   +-    -   +-         - 
 y   1  1    7597  0.018936              1( a  )   +-   +-   +-   +    +-    -   +-   +-    -      
 y   1  1    7599  0.012895              3( a  )   +-   +-   +-   +    +-    -   +-   +-    -      
 y   1  1    7602  0.017301              6( a  )   +-   +-   +-   +    +-    -   +-   +-    -      
 y   1  1    7605 -0.012107              9( a  )   +-   +-   +-   +    +-    -   +-   +-    -      
 y   1  1    7614  0.015142             18( a  )   +-   +-   +-   +    +-    -   +-   +-    -      
 y   1  1    7622  0.012813              4( a  )   +-   +-   +-   +    +-    -   +-   +-         - 
 y   1  1    7628 -0.013629             10( a  )   +-   +-   +-   +    +-    -   +-   +-         - 
 y   1  1    8269  0.011500             13( a  )   +-   +-   +-   +     -   +-   +-   +-    -      
 y   1  1   11118 -0.013457              2( a  )   +-   +-    -   +-   +-   +-   +-   +-           
 y   1  1   11162 -0.019294              2( a  )   +-   +-    -   +-   +-   +-   +-   +          - 
 y   1  1   11171 -0.012164             11( a  )   +-   +-    -   +-   +-   +-   +-   +          - 
 y   1  1   11558 -0.023031              2( a  )   +-   +-    -   +-   +-   +    +-   +-    -      
 y   1  1   11567 -0.013512             11( a  )   +-   +-    -   +-   +-   +    +-   +-    -      
 y   1  1   12230 -0.013986             14( a  )   +-   +-    -   +-   +    +-   +-   +-    -      
 y   1  1   13440 -0.015082             14( a  )   +-   +-    -   +    +-   +-   +-   +-    -      
 y   1  1   13443  0.011136             17( a  )   +-   +-    -   +    +-   +-   +-   +-    -      
 y   1  1   13450  0.014488              2( a  )   +-   +-    -   +    +-   +-   +-   +-         - 
 y   1  1   13456  0.014590              8( a  )   +-   +-    -   +    +-   +-   +-   +-         - 
 y   1  1   15760 -0.015574              2( a  )   +-   +-   +    +-   +-    -   +-   +-    -      
 y   1  1   17762  0.010710              2( a  )   +-   +-   +     -   +-   +-   +-   +-         - 
 y   1  1   17768  0.011986              8( a  )   +-   +-   +     -   +-   +-   +-   +-         - 

 ci coefficient statistics:
           rq > 0.1                3
      0.1> rq > 0.01              99
     0.01> rq > 0.001            687
    0.001> rq > 0.0001          2627
   0.0001> rq > 0.00001         4308
  0.00001> rq > 0.000001        2897
 0.000001> rq                  11891
           all                 22512
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     16268 2x:         0 4x:         0
All internal counts: zz :     59780 yy:         0 xx:         0 ww:         0
One-external counts: yz :         0 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:         0    task #   2:     49275    task #   3:         0    task #   4:      8071
task #   5:         0    task #

 number of reference csfs (nref) is   336.  root number (iroot) is  1.

 pople ci energy extrapolation is computed with 16 correlated electrons.

 eref      =   -114.237520391342   "relaxed" cnot**2         =   0.967578504342
 eci       =   -114.321688767960   deltae = eci - eref       =  -0.084168376617
 eci+dv1   =   -114.324417632617   dv1 = (1-cnot**2)*deltae  =  -0.002728864657
 eci+dv2   =   -114.324509071061   dv2 = dv1 / cnot**2       =  -0.002820303102
 eci+dv3   =   -114.324606849770   dv3 = dv1 / (2*cnot**2-1) =  -0.002918081810
 eci+pople =   -114.324219822055   ( 16e- scaled deltae )    =  -0.086699430712


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 2) =      -114.1774578099

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7    8    9   10

                                          orbital     1    2    3    4    5    6    7    8    9   10

                                         symmetry   a    a    a    a    a    a    a    a    a    a  

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       2 -0.033658                        +-   +-   +-   +-   +-   +-   +-   +     -      
 z*  1  1       7  0.961800                        +-   +-   +-   +-   +-   +-   +    +-    -      
 z*  1  1      10  0.014954                        +-   +-   +-   +-   +-   +-   +     -   +     - 
 z*  1  1      14  0.041089                        +-   +-   +-   +-   +-   +-   +          -   +- 
 z*  1  1      29  0.107822                        +-   +-   +-   +-   +-   +     -   +-   +-      
 z*  1  1      32  0.045055                        +-   +-   +-   +-   +-   +     -   +    +-    - 
 z*  1  1      48 -0.017481                        +-   +-   +-   +-   +-        +    +-    -   +- 
 z*  1  1      60  0.010673                        +-   +-   +-   +-   +    +-    -   +-   +     - 
 z*  1  1      63  0.010178                        +-   +-   +-   +-   +    +-    -   +     -   +- 
 z*  1  1      67 -0.016836                        +-   +-   +-   +-   +    +-   +     -    -   +- 
 z*  1  1      77 -0.014179                        +-   +-   +-   +-   +     -   +    +-   +-    - 
 z*  1  1      84 -0.011090                        +-   +-   +-   +-   +    +     -   +-   +-    - 
 z*  1  1      98 -0.019526                        +-   +-   +-   +-        +-   +    +-    -   +- 
 z*  1  1     115 -0.039313                        +-   +-   +-   +    +-   +-    -   +-   +     - 
 z*  1  1     118 -0.011611                        +-   +-   +-   +    +-   +-    -   +     -   +- 
 z*  1  1     120  0.019296                        +-   +-   +-   +    +-   +-   +    +-    -    - 
 z*  1  1     122  0.018391                        +-   +-   +-   +    +-   +-   +     -    -   +- 
 z*  1  1     132  0.022954                        +-   +-   +-   +    +-    -   +    +-   +-    - 
 z*  1  1     183 -0.019230                        +-   +-   +-        +-   +-   +    +-    -   +- 
 z*  1  1     197 -0.075171                        +-   +-   +    +-   +-   +-   +-   +-    -      
 z*  1  1     200  0.021182                        +-   +-   +    +-   +-   +-   +-    -   +     - 
 z*  1  1     202 -0.034084                        +-   +-   +    +-   +-   +-   +-   +     -    - 
 z*  1  1     217  0.074941                        +-   +-   +    +-   +-    -   +-   +-   +-      
 z*  1  1     238  0.017136                        +-   +-   +    +-    -   +-   +-   +-   +     - 
 z*  1  1     252 -0.016864                        +-   +-   +    +-   +    +-   +-   +-    -    - 
 z*  1  1     316 -0.011753                        +-   +-        +-   +-   +-   +    +-    -   +- 
 y   1  1     406 -0.014378              4( a  )   +-   +-   +-   +-   +-   +-    -   +-           
 y   1  1     412  0.017159             10( a  )   +-   +-   +-   +-   +-   +-    -   +-           
 y   1  1     430 -0.010495              6( a  )   +-   +-   +-   +-   +-   +-    -   +     -      
 y   1  1     439 -0.012548             15( a  )   +-   +-   +-   +-   +-   +-    -   +     -      
 y   1  1     450  0.014017              4( a  )   +-   +-   +-   +-   +-   +-    -   +          - 
 y   1  1     456  0.016031             10( a  )   +-   +-   +-   +-   +-   +-    -   +          - 
 y   1  1     459 -0.018902             13( a  )   +-   +-   +-   +-   +-   +-    -   +          - 
 y   1  1     535 -0.012475              1( a  )   +-   +-   +-   +-   +-   +-   +     -    -      
 y   1  1     569  0.010585             13( a  )   +-   +-   +-   +-   +-   +-   +     -         - 
 y   1  1     581 -0.012435              3( a  )   +-   +-   +-   +-   +-   +-   +          -    - 
 y   1  1     590  0.010542             12( a  )   +-   +-   +-   +-   +-   +-   +          -    - 
 y   1  1     602 -0.010664              2( a  )   +-   +-   +-   +-   +-   +-        +-    -      
 y   1  1     605 -0.011617              5( a  )   +-   +-   +-   +-   +-   +-        +-    -      
 y   1  1     611  0.015133             11( a  )   +-   +-   +-   +-   +-   +-        +-    -      
 y   1  1     712  0.011462              2( a  )   +-   +-   +-   +-   +-   +-        +     -    - 
 y   1  1     715 -0.019169              5( a  )   +-   +-   +-   +-   +-   +-        +     -    - 
 y   1  1     718  0.010271              8( a  )   +-   +-   +-   +-   +-   +-        +     -    - 
 y   1  1     721  0.018015             11( a  )   +-   +-   +-   +-   +-   +-        +     -    - 
 y   1  1     726  0.011642             16( a  )   +-   +-   +-   +-   +-   +-        +     -    - 
 y   1  1     918 -0.010796             10( a  )   +-   +-   +-   +-   +-    -   +    +-    -      
 y   1  1     945  0.010064             15( a  )   +-   +-   +-   +-   +-    -   +    +-         - 
 y   1  1     984  0.016439             10( a  )   +-   +-   +-   +-   +-    -   +     -   +     - 
 y   1  1    1028 -0.029501             10( a  )   +-   +-   +-   +-   +-    -   +    +     -    - 
 y   1  1    1031  0.013072             13( a  )   +-   +-   +-   +-   +-    -   +    +     -    - 
 y   1  1    1086  0.010593              2( a  )   +-   +-   +-   +-   +-    -        +-   +-      
 y   1  1    1089 -0.011168              5( a  )   +-   +-   +-   +-   +-    -        +-   +-      
 y   1  1    1286  0.019991              4( a  )   +-   +-   +-   +-   +-   +     -   +-    -      
 y   1  1    1292  0.015548             10( a  )   +-   +-   +-   +-   +-   +     -   +-    -      
 y   1  1    1295 -0.017856             13( a  )   +-   +-   +-   +-   +-   +     -   +-    -      
 y   1  1    1732  0.022338             10( a  )   +-   +-   +-   +-   +-         -   +-   +-      
 y   1  1    2153  0.010087             13( a  )   +-   +-   +-   +-    -   +-   +    +-         - 
 y   1  1    2229 -0.010306              1( a  )   +-   +-   +-   +-    -   +-   +    +     -    - 
 y   1  1    2235 -0.015423              7( a  )   +-   +-   +-   +-    -   +-   +    +     -    - 
 y   1  1    2240 -0.013886             12( a  )   +-   +-   +-   +-    -   +-   +    +     -    - 
 y   1  1    2614  0.010072             12( a  )   +-   +-   +-   +-    -   +     -   +-   +-      
 y   1  1    3088 -0.013911              2( a  )   +-   +-   +-   +-   +    +-   +-    -    -      
 y   1  1    3094 -0.017808              8( a  )   +-   +-   +-   +-   +    +-   +-    -    -      
 y   1  1    3102 -0.018684             16( a  )   +-   +-   +-   +-   +    +-   +-    -    -      
 y   1  1    3153 -0.025045              1( a  )   +-   +-   +-   +-   +    +-    -   +-    -      
 y   1  1    3158 -0.017690              6( a  )   +-   +-   +-   +-   +    +-    -   +-    -      
 y   1  1    3159 -0.017896              7( a  )   +-   +-   +-   +-   +    +-    -   +-    -      
 y   1  1    3161  0.012634              9( a  )   +-   +-   +-   +-   +    +-    -   +-    -      
 y   1  1    3167  0.029688             15( a  )   +-   +-   +-   +-   +    +-    -   +-    -      
 y   1  1    3170 -0.020994             18( a  )   +-   +-   +-   +-   +    +-    -   +-    -      
 y   1  1    3184  0.011400             10( a  )   +-   +-   +-   +-   +    +-    -   +-         - 
 y   1  1    3209 -0.010830             13( a  )   +-   +-   +-   +-   +    +-    -    -   +-      
 y   1  1    3350 -0.010876             22( a  )   +-   +-   +-   +-   +    +-   +     -    -    - 
 y   1  1    3355  0.010508              5( a  )   +-   +-   +-   +-   +    +-        +-    -    - 
 y   1  1    3361 -0.015717             11( a  )   +-   +-   +-   +-   +    +-        +-    -    - 
 y   1  1    3430  0.022595             14( a  )   +-   +-   +-   +-   +     -   +-   +-    -      
 y   1  1    3433  0.018607             17( a  )   +-   +-   +-   +-   +     -   +-   +-    -      
 y   1  1    3734  0.019669             10( a  )   +-   +-   +-   +-   +     -   +    +-    -    - 
 y   1  1    5089 -0.011111              1( a  )   +-   +-   +-    -   +-   +-   +    +-    -      
 y   1  1    5199 -0.010005              1( a  )   +-   +-   +-    -   +-   +-   +    +     -    - 
 y   1  1    7333 -0.038297              1( a  )   +-   +-   +-   +    +-   +-    -   +-    -      
 y   1  1    7335 -0.014492              3( a  )   +-   +-   +-   +    +-   +-    -   +-    -      
 y   1  1    7338 -0.031168              6( a  )   +-   +-   +-   +    +-   +-    -   +-    -      
 y   1  1    7341  0.023845              9( a  )   +-   +-   +-   +    +-   +-    -   +-    -      
 y   1  1    7344 -0.010161             12( a  )   +-   +-   +-   +    +-   +-    -   +-    -      
 y   1  1    7347 -0.010941             15( a  )   +-   +-   +-   +    +-   +-    -   +-    -      
 y   1  1    7351 -0.010523             19( a  )   +-   +-   +-   +    +-   +-    -   +-    -      
 y   1  1    7358 -0.013745              4( a  )   +-   +-   +-   +    +-   +-    -   +-         - 
 y   1  1    7914 -0.013006             10( a  )   +-   +-   +-   +    +-    -   +    +-    -    - 
 y   1  1   11126 -0.011415             10( a  )   +-   +-    -   +-   +-   +-   +-   +-           
 y   1  1   11129  0.013106             13( a  )   +-   +-    -   +-   +-   +-   +-   +-           
 y   1  1   11253  0.014046              5( a  )   +-   +-    -   +-   +-   +-   +    +-    -      
 y   1  1   11360  0.017131              2( a  )   +-   +-    -   +-   +-   +-   +    +     -    - 
 y   1  1   11369  0.011003             11( a  )   +-   +-    -   +-   +-   +-   +    +     -    - 
 y   1  1   11734 -0.016065              2( a  )   +-   +-    -   +-   +-   +     -   +-   +-      
 y   1  1   12217  0.016003              1( a  )   +-   +-    -   +-   +    +-   +-   +-    -      
 y   1  1   12219 -0.010356              3( a  )   +-   +-    -   +-   +    +-   +-   +-    -      
 y   1  1   13427  0.010501              1( a  )   +-   +-    -   +    +-   +-   +-   +-    -      
 y   1  1   13432  0.010830              6( a  )   +-   +-    -   +    +-   +-   +-   +-    -      
 y   1  1   13616 -0.012286             14( a  )   +-   +-    -   +    +-   +-    -   +-   +-      
 y   1  1   13736  0.013917              2( a  )   +-   +-    -   +    +-   +-   +    +-    -    - 
 y   1  1   13742  0.013986              8( a  )   +-   +-    -   +    +-   +-   +    +-    -    - 
 y   1  1   15435  0.010807              7( a  )   +-   +-   +    +-   +-   +-   +-    -    -      
 y   1  1   15496  0.055279              2( a  )   +-   +-   +    +-   +-   +-    -   +-    -      
 y   1  1   15499 -0.027197              5( a  )   +-   +-   +    +-   +-   +-    -   +-    -      
 y   1  1   15502  0.036660              8( a  )   +-   +-   +    +-   +-   +-    -   +-    -      
 y   1  1   15505  0.019090             11( a  )   +-   +-   +    +-   +-   +-    -   +-    -      
 y   1  1   15768 -0.015933             10( a  )   +-   +-   +    +-   +-    -   +-   +-    -      
 y   1  1   15936 -0.011800              2( a  )   +-   +-   +    +-   +-    -    -   +-   +-      
 y   1  1   16424  0.010830              6( a  )   +-   +-   +    +-    -   +-   +-   +-    -      
 y   1  1   17744  0.010229              6( a  )   +-   +-   +     -   +-   +-   +-   +-    -      
 y   1  1   18048  0.010627              2( a  )   +-   +-   +     -   +-   +-   +    +-    -    - 
 y   1  1   18054  0.011513              8( a  )   +-   +-   +     -   +-   +-   +    +-    -    - 
 y   1  1   20056 -0.015996              8( a  )   +-   +-        +-   +-   +-   +-   +-    -      

 ci coefficient statistics:
           rq > 0.1                2
      0.1> rq > 0.01             112
     0.01> rq > 0.001            935
    0.001> rq > 0.0001          3551
   0.0001> rq > 0.00001         4282
  0.00001> rq > 0.000001        2006
 0.000001> rq                  11624
           all                 22512
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     16268 2x:         0 4x:         0
All internal counts: zz :     59780 yy:         0 xx:         0 ww:         0
One-external counts: yz :         0 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:         0    task #   2:     49275    task #   3:         0    task #   4:      8071
task #   5:         0    task #

 number of reference csfs (nref) is   336.  root number (iroot) is  2.

 pople ci energy extrapolation is computed with 16 correlated electrons.

 eref      =   -114.082480610951   "relaxed" cnot**2         =   0.960759217971
 eci       =   -114.177457809890   deltae = eci - eref       =  -0.094977198939
 eci+dv1   =   -114.181184789451   dv1 = (1-cnot**2)*deltae  =  -0.003726979561
 eci+dv2   =   -114.181337012391   dv2 = dv1 / cnot**2       =  -0.003879202501
 eci+dv3   =   -114.181502199471   dv3 = dv1 / (2*cnot**2-1) =  -0.004044389581
 eci+pople =   -114.180958694577   ( 16e- scaled deltae )    =  -0.098478083627


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 3) =      -113.9753981399

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7    8    9   10

                                          orbital     1    2    3    4    5    6    7    8    9   10

                                         symmetry   a    a    a    a    a    a    a    a    a    a  

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       2  0.965938                        +-   +-   +-   +-   +-   +-   +-   +     -      
 z*  1  1       5  0.033295                        +-   +-   +-   +-   +-   +-   +-        +     - 
 z*  1  1       7  0.032700                        +-   +-   +-   +-   +-   +-   +    +-    -      
 z*  1  1      19 -0.014812                        +-   +-   +-   +-   +-   +-        +     -   +- 
 z*  1  1      22  0.011162                        +-   +-   +-   +-   +-   +    +-   +-         - 
 z*  1  1      23  0.096985                        +-   +-   +-   +-   +-   +    +-    -   +-      
 z*  1  1      27  0.022261                        +-   +-   +-   +-   +-   +    +-        +-    - 
 z*  1  1      45 -0.017335                        +-   +-   +-   +-   +-        +-   +     -   +- 
 z*  1  1      51  0.014661                        +-   +-   +-   +-   +    +-   +-   +-    -      
 z*  1  1      54 -0.041585                        +-   +-   +-   +-   +    +-   +-    -   +     - 
 z*  1  1      56  0.039474                        +-   +-   +-   +-   +    +-   +-   +     -    - 
 z*  1  1      58 -0.011021                        +-   +-   +-   +-   +    +-   +-         -   +- 
 z*  1  1      71 -0.084120                        +-   +-   +-   +-   +     -   +-   +-   +-      
 z*  1  1      82 -0.018074                        +-   +-   +-   +-   +    +    +-    -   +-    - 
 z*  1  1      95 -0.021951                        +-   +-   +-   +-        +-   +-   +     -   +- 
 z*  1  1     106  0.039638                        +-   +-   +-   +    +-   +-   +-   +-    -      
 z*  1  1     109 -0.029545                        +-   +-   +-   +    +-   +-   +-    -   +     - 
 z*  1  1     111  0.031438                        +-   +-   +-   +    +-   +-   +-   +     -    - 
 z*  1  1     113  0.014716                        +-   +-   +-   +    +-   +-   +-         -   +- 
 z*  1  1     126 -0.034610                        +-   +-   +-   +    +-    -   +-   +-   +-      
 z*  1  1     129  0.020355                        +-   +-   +-   +    +-    -   +-   +    +-    - 
 z*  1  1     161  0.015434                        +-   +-   +-   +    +    +-   +-   +-    -    - 
 z*  1  1     180 -0.020864                        +-   +-   +-        +-   +-   +-   +     -   +- 
 z*  1  1     197 -0.010502                        +-   +-   +    +-   +-   +-   +-   +-    -      
 z*  1  1     211 -0.010311                        +-   +-   +    +-   +-   +-   +    +-    -    - 
 z*  1  1     313 -0.012080                        +-   +-        +-   +-   +-   +-   +     -   +- 
 y   1  1     346  0.016380             10( a  )   +-   +-   +-   +-   +-   +-   +-    -           
 y   1  1     349 -0.011985             13( a  )   +-   +-   +-   +-   +-   +-   +-    -           
 y   1  1     380 -0.012817             22( a  )   +-   +-   +-   +-   +-   +-   +-         -      
 y   1  1     384  0.020154              4( a  )   +-   +-   +-   +-   +-   +-   +-              - 
 y   1  1     390  0.015215             10( a  )   +-   +-   +-   +-   +-   +-   +-              - 
 y   1  1     419  0.012833             17( a  )   +-   +-   +-   +-   +-   +-    -   +-           
 y   1  1     492 -0.014649              2( a  )   +-   +-   +-   +-   +-   +-    -        +     - 
 y   1  1     495  0.021050              5( a  )   +-   +-   +-   +-   +-   +-    -        +     - 
 y   1  1     498 -0.012392              8( a  )   +-   +-   +-   +-   +-   +-    -        +     - 
 y   1  1     501 -0.019368             11( a  )   +-   +-   +-   +-   +-   +-    -        +     - 
 y   1  1     506 -0.014088             16( a  )   +-   +-   +-   +-   +-   +-    -        +     - 
 y   1  1     550  0.015380             16( a  )   +-   +-   +-   +-   +-   +-   +     -    -      
 y   1  1     601 -0.012355              1( a  )   +-   +-   +-   +-   +-   +-        +-    -      
 y   1  1     615 -0.010364             15( a  )   +-   +-   +-   +-   +-   +-        +-    -      
 y   1  1     777  0.012734              1( a  )   +-   +-   +-   +-   +-    -   +-   +-           
 y   1  1     791 -0.013894             15( a  )   +-   +-   +-   +-   +-    -   +-   +-           
 y   1  1     857 -0.010175             15( a  )   +-   +-   +-   +-   +-    -   +-        +-      
 y   1  1     874  0.024419             10( a  )   +-   +-   +-   +-   +-    -   +-        +     - 
 y   1  1     877 -0.010639             13( a  )   +-   +-   +-   +-   +-    -   +-        +     - 
 y   1  1     925 -0.011401             17( a  )   +-   +-   +-   +-   +-    -   +    +-    -      
 y   1  1    1220  0.019514              4( a  )   +-   +-   +-   +-   +-   +    +-    -    -      
 y   1  1    1226  0.019422             10( a  )   +-   +-   +-   +-   +-   +    +-    -    -      
 y   1  1    1229 -0.013345             13( a  )   +-   +-   +-   +-   +-   +    +-    -    -      
 y   1  1    1328 -0.018074              2( a  )   +-   +-   +-   +-   +-   +     -    -   +-      
 y   1  1    1331  0.024179              5( a  )   +-   +-   +-   +-   +-   +     -    -   +-      
 y   1  1    1334 -0.012767              8( a  )   +-   +-   +-   +-   +-   +     -    -   +-      
 y   1  1    1337 -0.020507             11( a  )   +-   +-   +-   +-   +-   +     -    -   +-      
 y   1  1    1342 -0.013186             16( a  )   +-   +-   +-   +-   +-   +     -    -   +-      
 y   1  1    1594 -0.010119              4( a  )   +-   +-   +-   +-   +-        +-    -   +-      
 y   1  1    1600  0.024071             10( a  )   +-   +-   +-   +-   +-        +-    -   +-      
 y   1  1    2081  0.012010              7( a  )   +-   +-   +-   +-    -   +-   +-        +     - 
 y   1  1    2086  0.012456             12( a  )   +-   +-   +-   +-    -   +-   +-        +     - 
 y   1  1    2471  0.011084              1( a  )   +-   +-   +-   +-    -   +    +-    -   +-      
 y   1  1    2482  0.011509             12( a  )   +-   +-   +-   +-    -   +    +-    -   +-      
 y   1  1    3087 -0.014700              1( a  )   +-   +-   +-   +-   +    +-   +-    -    -      
 y   1  1    3089  0.010063              3( a  )   +-   +-   +-   +-   +    +-   +-    -    -      
 y   1  1    3098 -0.025631             12( a  )   +-   +-   +-   +-   +    +-   +-    -    -      
 y   1  1    3108  0.016930             22( a  )   +-   +-   +-   +-   +    +-   +-    -    -      
 y   1  1    3118  0.011763             10( a  )   +-   +-   +-   +-   +    +-   +-    -         - 
 y   1  1    3154 -0.023270              2( a  )   +-   +-   +-   +-   +    +-    -   +-    -      
 y   1  1    3157  0.018396              5( a  )   +-   +-   +-   +-   +    +-    -   +-    -      
 y   1  1    3160 -0.023953              8( a  )   +-   +-   +-   +-   +    +-    -   +-    -      
 y   1  1    3168 -0.023159             16( a  )   +-   +-   +-   +-   +    +-    -   +-    -      
 y   1  1    3213 -0.010009             17( a  )   +-   +-   +-   +-   +    +-    -    -   +-      
 y   1  1    3229 -0.011389             11( a  )   +-   +-   +-   +-   +    +-    -    -   +     - 
 y   1  1    3267  0.012588              5( a  )   +-   +-   +-   +-   +    +-    -   +     -    - 
 y   1  1    3273 -0.019519             11( a  )   +-   +-   +-   +-   +    +-    -   +     -    - 
 y   1  1    3429 -0.026020             13( a  )   +-   +-   +-   +-   +     -   +-   +-    -      
 y   1  1    3436 -0.012979             20( a  )   +-   +-   +-   +-   +     -   +-   +-    -      
 y   1  1    3492  0.011215             10( a  )   +-   +-   +-   +-   +     -   +-    -   +     - 
 y   1  1    3536  0.021537             10( a  )   +-   +-   +-   +-   +     -   +-   +     -    - 
 y   1  1    4077  0.011349              1( a  )   +-   +-   +-   +-        +-   +-   +-    -      
 y   1  1    5090  0.010468              2( a  )   +-   +-   +-    -   +-   +-   +    +-    -      
 y   1  1    5441  0.011255              1( a  )   +-   +-   +-    -   +-   +    +-    -   +-      
 y   1  1    6057  0.011485              1( a  )   +-   +-   +-    -   +    +-   +-   +-    -      
 y   1  1    7267 -0.024750              1( a  )   +-   +-   +-   +    +-   +-   +-    -    -      
 y   1  1    7269 -0.022150              3( a  )   +-   +-   +-   +    +-   +-   +-    -    -      
 y   1  1    7272 -0.023902              6( a  )   +-   +-   +-   +    +-   +-   +-    -    -      
 y   1  1    7273 -0.017225              7( a  )   +-   +-   +-   +    +-   +-   +-    -    -      
 y   1  1    7275  0.010737              9( a  )   +-   +-   +-   +    +-   +-   +-    -    -      
 y   1  1    7284 -0.019552             18( a  )   +-   +-   +-   +    +-   +-   +-    -    -      
 y   1  1    7285 -0.021361             19( a  )   +-   +-   +-   +    +-   +-   +-    -    -      
 y   1  1    7292 -0.014415              4( a  )   +-   +-   +-   +    +-   +-   +-    -         - 
 y   1  1    7334 -0.018082              2( a  )   +-   +-   +-   +    +-   +-    -   +-    -      
 y   1  1    7337  0.012353              5( a  )   +-   +-   +-   +    +-   +-    -   +-    -      
 y   1  1    7340 -0.010762              8( a  )   +-   +-   +-   +    +-   +-    -   +-    -      
 y   1  1    7343 -0.011540             11( a  )   +-   +-   +-   +    +-   +-    -   +-    -      
 y   1  1    7609  0.014605             13( a  )   +-   +-   +-   +    +-    -   +-   +-    -      
 y   1  1    7716 -0.011718             10( a  )   +-   +-   +-   +    +-    -   +-   +     -    - 
 y   1  1    8257  0.010043              1( a  )   +-   +-   +-   +     -   +-   +-   +-    -      
 y   1  1    8385 -0.010179             19( a  )   +-   +-   +-   +     -   +-   +-   +     -    - 
 y   1  1   11206 -0.013138              2( a  )   +-   +-    -   +-   +-   +-   +-        +     - 
 y   1  1   11602 -0.015609              2( a  )   +-   +-    -   +-   +-   +    +-    -   +-      
 y   1  1   12218 -0.015344              2( a  )   +-   +-    -   +-   +    +-   +-   +-    -      
 y   1  1   13428  0.012086              2( a  )   +-   +-    -   +    +-   +-   +-   +-    -      
 y   1  1   13434  0.018205              8( a  )   +-   +-    -   +    +-   +-   +-   +-    -      
 y   1  1   13484 -0.013060             14( a  )   +-   +-    -   +    +-   +-   +-    -   +-      
 y   1  1   13538  0.015260              2( a  )   +-   +-    -   +    +-   +-   +-   +     -    - 
 y   1  1   13544  0.015241              8( a  )   +-   +-    -   +    +-   +-   +-   +     -    - 
 y   1  1   15430  0.021512              2( a  )   +-   +-   +    +-   +-   +-   +-    -    -      
 y   1  1   15436  0.015043              8( a  )   +-   +-   +    +-   +-   +-   +-    -    -      
 y   1  1   15495  0.018504              1( a  )   +-   +-   +    +-   +-   +-    -   +-    -      
 y   1  1   15500  0.010878              6( a  )   +-   +-   +    +-   +-   +-    -   +-    -      
 y   1  1   15503 -0.010356              9( a  )   +-   +-   +    +-   +-   +-    -   +-    -      
 y   1  1   17746  0.012285              8( a  )   +-   +-   +     -   +-   +-   +-   +-    -      
 y   1  1   17850  0.010109              2( a  )   +-   +-   +     -   +-   +-   +-   +     -    - 
 y   1  1   17856  0.012719              8( a  )   +-   +-   +     -   +-   +-   +-   +     -    - 
 y   1  1   20049 -0.011205              1( a  )   +-   +-        +-   +-   +-   +-   +-    -      
 y   1  1   20054 -0.011839              6( a  )   +-   +-        +-   +-   +-   +-   +-    -      
 y   1  1   20057  0.011172              9( a  )   +-   +-        +-   +-   +-   +-   +-    -      

 ci coefficient statistics:
           rq > 0.1                1
      0.1> rq > 0.01             115
     0.01> rq > 0.001            891
    0.001> rq > 0.0001          3232
   0.0001> rq > 0.00001         4397
  0.00001> rq > 0.000001        2796
 0.000001> rq                  11080
           all                 22512
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     16268 2x:         0 4x:         0
All internal counts: zz :     59780 yy:         0 xx:         0 ww:         0
One-external counts: yz :         0 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:         0    task #   2:     49275    task #   3:         0    task #   4:      8071
task #   5:         0    task #

 number of reference csfs (nref) is   336.  root number (iroot) is  3.

 pople ci energy extrapolation is computed with 16 correlated electrons.

 eref      =   -113.886903005922   "relaxed" cnot**2         =   0.964212411628
 eci       =   -113.975398139866   deltae = eci - eref       =  -0.088495133945
 eci+dv1   =   -113.978565167293   dv1 = (1-cnot**2)*deltae  =  -0.003167027426
 eci+dv2   =   -113.978682714290   dv2 = dv1 / cnot**2       =  -0.003284574424
 eci+dv3   =   -113.978809323354   dv3 = dv1 / (2*cnot**2-1) =  -0.003411183488
 eci+pople =   -113.978353965486   ( 16e- scaled deltae )    =  -0.091450959564
NO coefficients and occupation numbers are written to nocoef_ci.1

################################################################################
 one electron density matrix calculation 
################################################################################


 workspace allocation information: mxcore=99998791
 
 space required:
 
    space required for calls in multd2:
       onex          616
       allin           0
       diagon       1265
    max.      ---------
       maxnex       1265
 
    total core space usage:
       maxnex       1265
    max k-seg      22176
    max k-ind       1008
              ---------
       totmax      47633
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=    4368  DYX=       0  DYW=       0
   D0Z=    4312  D0Y=   18564  D0X=       0  D0W=       0
  DDZI=    3136 DDYI=    9184 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=    1008 DDXE=       0 DDWE=       0
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   2.0000000      2.0000000      1.9954065      1.9928030      1.9880347
   1.9865607      1.9735023      1.9194356      0.0861163      0.0300307
   0.0102381      0.0030834      0.0026432      0.0018808      0.0017437
   0.0014650      0.0012825      0.0011462      0.0008886      0.0007739
   0.0007264      0.0004647      0.0004454      0.0003436      0.0003169
   0.0003047      0.0001281      0.0001007      0.0000805      0.0000304
   0.0000150      0.0000082


 total number of electrons =   16.0000000000



          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        A   partial gross atomic populations
   ao class       1A         2A         3A         4A         5A         6A  
    O1_ s      -0.001092   1.999099  -0.021086   1.037328   0.910373   1.900859
    C1_ s       2.001021   0.000891   1.051779   0.641670   0.727710   0.051503
    H1_ s       0.000006   0.000004   0.471272   0.158263   0.172107   0.021521
    H2_ s       0.000064   0.000006   0.493441   0.155542   0.177844   0.012678
 
   ao class       7A         8A         9A        10A        11A        12A  
    O1_ s       1.280497   1.199307   0.034594   0.012900   0.005222   0.001934
    C1_ s       0.698163   0.720129   0.051522   0.016698   0.002862   0.000577
    H1_ s      -0.002691   0.000000   0.000000   0.000223   0.001033   0.000306
    H2_ s      -0.002466   0.000000   0.000000   0.000210   0.001121   0.000266
 
   ao class      13A        14A        15A        16A        17A        18A  
    O1_ s       0.000732   0.000988   0.000211   0.001004   0.001184   0.001141
    C1_ s       0.001247   0.000893   0.001321   0.000362   0.000098   0.000005
    H1_ s       0.000327   0.000000   0.000103   0.000048   0.000000   0.000000
    H2_ s       0.000336   0.000000   0.000109   0.000050   0.000000   0.000000
 
   ao class      19A        20A        21A        22A        23A        24A  
    O1_ s       0.000068   0.000252   0.000640   0.000361   0.000145   0.000165
    C1_ s       0.000820   0.000499   0.000037   0.000110   0.000300   0.000148
    H1_ s       0.000000   0.000011   0.000025  -0.000003   0.000000   0.000015
    H2_ s       0.000000   0.000012   0.000025  -0.000003   0.000000   0.000016
 
   ao class      25A        26A        27A        28A        29A        30A  
    O1_ s       0.000040   0.000079   0.000001   0.000017   0.000049   0.000002
    C1_ s       0.000277   0.000244   0.000105   0.000037   0.000025   0.000003
    H1_ s       0.000000  -0.000009   0.000012   0.000023   0.000003   0.000012
    H2_ s       0.000000  -0.000009   0.000010   0.000023   0.000004   0.000013
 
   ao class      31A        32A  
    O1_ s       0.000001   0.000001
    C1_ s       0.000008   0.000005
    H1_ s       0.000003   0.000001
    H2_ s       0.000003   0.000001


                        gross atomic populations
     ao           O1_        C1_        H1_        H2_
      s         8.367015   5.971072   0.822617   0.839296
    total       8.367015   5.971072   0.822617   0.839296
 

 Total number of electrons:   16.00000000

NO coefficients and occupation numbers are written to nocoef_ci.2
--------------------------------------------------------------------------------
   calculation for root #    2
--------------------------------------------------------------------------------
================================================================================
   DYZ=    4368  DYX=       0  DYW=       0
   D0Z=    4312  D0Y=   18564  D0X=       0  D0W=       0
  DDZI=    3136 DDYI=    9184 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=    1008 DDXE=       0 DDWE=       0
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   2.0000000      2.0000000      1.9913667      1.9861515      1.9789768
   1.9772533      1.9750218      1.0127259      1.0116881      0.0292816
   0.0097015      0.0070923      0.0046101      0.0027043      0.0020458
   0.0016920      0.0014446      0.0014127      0.0011172      0.0010591
   0.0010264      0.0006874      0.0006711      0.0006300      0.0005910
   0.0003201      0.0002813      0.0002030      0.0001067      0.0000618
   0.0000583      0.0000175


 total number of electrons =   16.0000000000



          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        A   partial gross atomic populations
   ao class       1A         2A         3A         4A         5A         6A  
    O1_ s      -0.001092   1.999099   1.661848   0.333024   1.667737   0.283806
    C1_ s       2.001021   0.000891   0.200840   1.096525   0.311239   1.012241
    H1_ s       0.000006   0.000004   0.066873   0.254821   0.000000   0.357492
    H2_ s       0.000064   0.000006   0.061806   0.301782   0.000000   0.323714
 
   ao class       7A         8A         9A        10A        11A        12A  
    O1_ s       1.286668   0.821980   0.141282   0.011327   0.002436   0.000151
    C1_ s       0.675549   0.040653   0.870406   0.016858   0.003400   0.003392
    H1_ s       0.001806   0.075503   0.000000   0.000568   0.001956   0.001754
    H2_ s       0.010999   0.074591   0.000000   0.000529   0.001909   0.001795
 
   ao class      13A        14A        15A        16A        17A        18A  
    O1_ s       0.004085   0.002450   0.001783   0.000288   0.000964   0.001240
    C1_ s       0.000525   0.000185   0.000219   0.001404   0.000329   0.000173
    H1_ s       0.000000   0.000035   0.000025   0.000000   0.000077   0.000000
    H2_ s       0.000000   0.000034   0.000019   0.000000   0.000076   0.000000
 
   ao class      19A        20A        21A        22A        23A        24A  
    O1_ s       0.000812   0.000614   0.000236   0.000082   0.000190   0.000055
    C1_ s       0.000223   0.000445   0.000777   0.000605   0.000270   0.000415
    H1_ s       0.000039   0.000000   0.000010   0.000000   0.000108   0.000073
    H2_ s       0.000043   0.000000   0.000004   0.000000   0.000103   0.000087
 
   ao class      25A        26A        27A        28A        29A        30A  
    O1_ s       0.000497   0.000123   0.000007   0.000007   0.000071   0.000005
    C1_ s       0.000095   0.000197   0.000154   0.000173   0.000022   0.000026
    H1_ s       0.000000   0.000000   0.000060   0.000011   0.000007   0.000015
    H2_ s      -0.000002   0.000000   0.000061   0.000012   0.000007   0.000016
 
   ao class      31A        32A  
    O1_ s       0.000008   0.000003
    C1_ s       0.000035   0.000015
    H1_ s       0.000009   0.000000
    H2_ s       0.000006   0.000000


                        gross atomic populations
     ao           O1_        C1_        H1_        H2_
      s         8.221787   6.239302   0.761251   0.777660
    total       8.221787   6.239302   0.761251   0.777660
 

 Total number of electrons:   16.00000000

NO coefficients and occupation numbers are written to nocoef_ci.3
--------------------------------------------------------------------------------
   calculation for root #    3
--------------------------------------------------------------------------------
================================================================================
   DYZ=    4368  DYX=       0  DYW=       0
   D0Z=    4312  D0Y=   18564  D0X=       0  D0W=       0
  DDZI=    3136 DDYI=    9184 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=    1008 DDXE=       0 DDWE=       0
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   2.0000000      2.0000000      1.9919778      1.9870667      1.9864594
   1.9761364      1.9728741      1.0143198      1.0130802      0.0244990
   0.0083472      0.0047257      0.0036237      0.0025007      0.0019839
   0.0018217      0.0017990      0.0013512      0.0013392      0.0011100
   0.0010802      0.0008510      0.0007598      0.0007022      0.0004482
   0.0003663      0.0003096      0.0002374      0.0001105      0.0000620
   0.0000474      0.0000097


 total number of electrons =   16.0000000000



          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        A   partial gross atomic populations
   ao class       1A         2A         3A         4A         5A         6A  
    O1_ s      -0.001092   1.999099   0.289084   1.512644   1.168988   1.650567
    C1_ s       2.001021   0.000891   1.004995   0.225011   0.647839   0.325566
    H1_ s       0.000006   0.000004   0.412911   0.259773  -0.033924   0.000001
    H2_ s       0.000064   0.000006   0.284988  -0.010362   0.203557   0.000003
 
   ao class       7A         8A         9A        10A        11A        12A  
    O1_ s       0.743761   0.750524   0.148806   0.008218   0.003437   0.000636
    C1_ s       0.809114   0.208893   0.863905   0.014145   0.002667   0.002162
    H1_ s       0.128905   0.023261   0.000157   0.000991   0.000884   0.001055
    H2_ s       0.291094   0.031641   0.000213   0.001145   0.001359   0.000873
 
   ao class      13A        14A        15A        16A        17A        18A  
    O1_ s       0.003293   0.001704   0.000543   0.000756   0.001696   0.000810
    C1_ s       0.000331   0.000629   0.001441   0.000796   0.000032   0.000541
    H1_ s       0.000000   0.000083   0.000000   0.000140   0.000029   0.000000
    H2_ s       0.000000   0.000085   0.000000   0.000130   0.000043   0.000000
 
   ao class      19A        20A        21A        22A        23A        24A  
    O1_ s       0.001165   0.001094   0.000591   0.000577   0.000008   0.000119
    C1_ s       0.000102   0.000017   0.000422   0.000247   0.000752   0.000371
    H1_ s       0.000042   0.000000   0.000032   0.000011   0.000000   0.000079
    H2_ s       0.000031   0.000000   0.000036   0.000017   0.000000   0.000133
 
   ao class      25A        26A        27A        28A        29A        30A  
    O1_ s       0.000075   0.000090   0.000006  -0.000001   0.000054   0.000024
    C1_ s       0.000325   0.000277   0.000182   0.000213   0.000033   0.000026
    H1_ s       0.000027   0.000000   0.000065   0.000010   0.000011   0.000001
    H2_ s       0.000021   0.000000   0.000057   0.000015   0.000013   0.000011
 
   ao class      31A        32A  
    O1_ s       0.000005   0.000000
    C1_ s       0.000012   0.000009
    H1_ s       0.000021   0.000000
    H2_ s       0.000009   0.000000


                        gross atomic populations
     ao           O1_        C1_        H1_        H2_
      s         8.287278   6.112966   0.794575   0.805181
    total       8.287278   6.112966   0.794575   0.805181
 

 Total number of electrons:   16.00000000

