 total ao core energy =   30.905293021
 MCSCF calculation performed for  1 DRT.

 DRT  first state   no.of aver. states   weights
  1   ground state          3             0.333 0.333 0.333

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a  
 Total number of electrons:    16
 Spin multiplicity:            1
 Number of active orbitals:    8
 Number of active electrons:  12
 Total number of CSFs:       336

 Number of active-double rotations:     16
 Number of active-active rotations:      0
 Number of double-virtual rotations:    44
 Number of active-virtual rotations:   176
 
 iter=    1 emc= -114.0245144196 demc= 1.1402E+02 wnorm= 4.4022E-01 knorm= 2.1480E-01 apxde= 2.1776E-02    *not converged* 
 iter=    2 emc= -114.0505583005 demc= 2.6044E-02 wnorm= 8.3511E-02 knorm= 4.0844E-01 apxde= 5.2187E-03    *not converged* 
 iter=    3 emc= -114.0579688521 demc= 7.4106E-03 wnorm= 2.4500E-02 knorm= 2.4006E-01 apxde= 1.3035E-03    *not converged* 
 iter=    4 emc= -114.0604165126 demc= 2.4477E-03 wnorm= 1.5224E-02 knorm= 2.5471E-01 apxde= 1.4718E-03    *not converged* 
 iter=    5 emc= -114.0632463906 demc= 2.8299E-03 wnorm= 1.3561E-02 knorm= 2.7074E-01 apxde= 1.9594E-03    *not converged* 
 iter=    6 emc= -114.0650314805 demc= 1.7851E-03 wnorm= 7.2129E-03 knorm= 1.8925E-01 apxde= 5.0282E-03    *not converged* 
 iter=    7 emc= -114.0658781331 demc= 8.4665E-04 wnorm= 1.0275E-02 knorm= 3.8667E-02 apxde= 1.0563E-02    *not converged* 
 iter=    8 emc= -114.0661315448 demc= 2.5341E-04 wnorm= 1.0717E-02 knorm= 4.7165E-03 apxde= 1.0716E-02    *not converged* 
 iter=    9 emc= -114.0661644301 demc= 3.2885E-05 wnorm= 1.0770E-02 knorm= 1.9551E-01 apxde= 1.0708E-02    *not converged* 
 iter=   10 emc= -114.0678552850 demc= 1.6909E-03 wnorm= 1.2269E-02 knorm= 2.1113E-01 apxde= 5.9232E-03    *not converged* 
 iter=   11 emc= -114.0697968142 demc= 1.9415E-03 wnorm= 1.0924E-02 knorm= 2.5031E-01 apxde= 1.4860E-03    *not converged* 
 iter=   12 emc= -114.0710272818 demc= 1.2305E-03 wnorm= 7.6249E-03 knorm= 5.0264E-02 apxde= 4.7023E-05    *not converged* 
 iter=   13 emc= -114.0710756294 demc= 4.8348E-05 wnorm= 3.3814E-04 knorm= 2.4293E-03 apxde= 8.7925E-08    *not converged* 
 iter=   14 emc= -114.0710757175 demc= 8.8104E-08 wnorm= 8.6784E-07 knorm= 2.5606E-06 apxde= 3.2083E-13    *not converged* 

 final mcscf convergence values:
 iter=   15 emc= -114.0710757175 demc= 3.1264E-13 wnorm= 2.6532E-07 knorm= 3.7942E-08 apxde= 5.0070E-15    *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 weight 0.333333 total energy=   -114.239523587
   DRT #1 state # 2 weight 0.333333 total energy=   -114.085401496
   DRT #1 state # 3 weight 0.333333 total energy=   -113.888302070
   ------------------------------------------------------------


