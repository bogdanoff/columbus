
 program "mcdrt 4.1 a3"

 distinct row table specification and csf
 selection for mcscf wavefunction optimization.

 programmed by: ron shepard

 version date: 17-oct-91


 This Version of Program mcdrt is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at

    Thomas Mueller
    Central Institute of Applied Mathematics
    Research Centre Juelich
    D-52425 Juelich, Germany
    Internet: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              MCDRT       **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 expanded keystroke file:
 /scratch/matruc/potential-examples/potmat/H2CO-H2O-NAD/direct/WORK/mcdrtky      
 
 input the spin multiplicity [  0]:
 spin multiplicity:    1    singlet 
 input the total number of electrons [  0]:
 nelt:     16
 input the number of irreps (1-8) [  0]:
 nsym:      1
 enter symmetry labels:(y,[n])
 enter 1 labels (a4):
 enter symmetry label, default=   1
 input the molecular spatial symmetry (irrep 1:nsym) [  0]:
 spatial symmetry is irrep number:      1
 
 input the list of doubly-occupied orbitals (sym(i),rmo(i),i=1,ndot):
 number of doubly-occupied orbitals:      2
 number of inactive electrons:      4
 number of active electrons:     12
 level(*)        1   2
 symd(*)         1   1
 slabel(*)     a   a  
 doub(*)         1   2
 
 input the active orbitals (sym(i),rmo(i),i=1,nact):
 nact:      8
 level(*)        1   2   3   4   5   6   7   8
 syml(*)         1   1   1   1   1   1   1   1
 slabel(*)     a   a   a   a   a   a   a   a  
 modrt(*)        3   4   5   6   7   8   9  10
 input the minimum cumulative occupation for each active level:
  a   a   a   a   a   a   a   a  
    3   4   5   6   7   8   9  10
 input the maximum cumulative occupation for each active level:
  a   a   a   a   a   a   a   a  
    3   4   5   6   7   8   9  10
 slabel(*)     a   a   a   a   a   a   a   a  
 modrt(*)        3   4   5   6   7   8   9  10
 occmin(*)       0   0   0   0   0   0   0  12
 occmax(*)      12  12  12  12  12  12  12  12
 input the minimum b value for each active level:
  a   a   a   a   a   a   a   a  
    3   4   5   6   7   8   9  10
 input the maximum b value for each active level:
  a   a   a   a   a   a   a   a  
    3   4   5   6   7   8   9  10
 slabel(*)     a   a   a   a   a   a   a   a  
 modrt(*)        3   4   5   6   7   8   9  10
 bmin(*)         0   0   0   0   0   0   0   0
 bmax(*)        12  12  12  12  12  12  12  12
 input the step masks for each active level:
 modrt:smask=
   3:1111   4:1111   5:1111   6:1111   7:1111   8:1111   9:1111  10:1111
 input the number of vertices to be deleted [  0]:
 number of vertices to be removed (a priori):      0
 number of rows in the drt:     38
 are any arcs to be manually removed?(y,[n])

 nwalk=     336
 input the range of drt levels to print (l1,l2):
 levprt(*)       0   8

 level  0 through level  8 of the drt:

 row lev a b syml lab rmo  l0  l1  l2  l3 isym xbar   y0    y1    y2    xp     z

  38   8 6 0   1 a    10    0   0   0   0   1     1     0     0     0   336     0
 ........................................

  35   7 6 0   1 a     9   38   0   0   0   1     1     0     0     0    28     0

  36   7 5 1   1 a     9    0   0  38   0   1     1     1     1     0   112    28

  37   7 5 0   1 a     9    0   0   0  38   1     1     1     1     1   196   140
 ........................................

  29   6 6 0   1 a     8   35   0   0   0   1     1     0     0     0     1     0

  30   6 5 1   1 a     8   36   0  35   0   1     2     1     1     0     6     1

  31   6 5 0   1 a     8   37  36   0  35   1     3     2     1     1    21     7

  32   6 4 2   1 a     8    0   0  36   0   1     1     1     1     0    15    55

  33   6 4 1   1 a     8    0   0  37  36   1     2     2     2     1    70    70

  34   6 4 0   1 a     8    0   0   0  37   1     1     1     1     1   105   231
 ........................................

  23   5 5 0   1 a     7   31  30   0  29   1     6     3     1     1     1     0

  24   5 4 1   1 a     7   33  32  31  30   1     8     6     5     2     5     2

  25   5 4 0   1 a     7   34  33   0  31   1     6     5     3     3    15    13

  26   5 3 2   1 a     7    0   0  33  32   1     3     3     3     1    10    60

  27   5 3 1   1 a     7    0   0  34  33   1     3     3     3     2    40   100

  28   5 3 0   1 a     7    0   0   0  34   1     1     1     1     1    50   286
 ........................................

  17   4 4 0   1 a     6   25  24   0  23   1    20    14     6     6     1     0

  18   4 3 1   1 a     6   27  26  25  24   1    20    17    14     8     4     3

  19   4 3 0   1 a     6   28  27   0  25   1    10     9     6     6    10    18

  20   4 2 2   1 a     6    0   0  27  26   1     6     6     6     3     6    64

  21   4 2 1   1 a     6    0   0  28  27   1     4     4     4     3    20   120

  22   4 2 0   1 a     6    0   0   0  28   1     1     1     1     1    20   316
 ........................................

  11   3 3 0   1 a     5   19  18   0  17   1    50    40    20    20     1     0

  12   3 2 1   1 a     5   21  20  19  18   1    40    36    30    20     3     4

  13   3 2 0   1 a     5   22  21   0  19   1    15    14    10    10     6    22

  14   3 1 2   1 a     5    0   0  21  20   1    10    10    10     6     3    67

  15   3 1 1   1 a     5    0   0  22  21   1     5     5     5     4     8   132

  16   3 1 0   1 a     5    0   0   0  22   1     1     1     1     1     6   330
 ........................................

   5   2 2 0   1 a     4   13  12   0  11   1   105    90    50    50     1     0

   6   2 1 1   1 a     4   15  14  13  12   1    70    65    55    40     2     5

   7   2 1 0   1 a     4   16  15   0  13   1    21    20    15    15     3    25

   8   2 0 2   1 a     4    0   0  15  14   1    15    15    15    10     1    69

   9   2 0 1   1 a     4    0   0  16  15   1     6     6     6     5     2   138

  10   2 0 0   1 a     4    0   0   0  16   1     1     1     1     1     1   335
 ........................................

   2   1 1 0   1 a     3    7   6   0   5   1   196   175   105   105     1     0

   3   1 0 1   1 a     3    9   8   7   6   1   112   106    91    70     1     6

   4   1 0 0   1 a     3   10   9   0   7   1    28    27    21    21     1    27
 ........................................

   1   0 0 0   0       0    4   3   0   2   1   336   308   196   196     1     0
 ........................................

 initial csf selection step:
 total number of walks in the drt, nwalk=     336
 keep all of these walks?(y,[n])
 individual walks will be generated from the drt.
 apply orbital-group occupation restrictions?(y,[n])
 apply reference occupation restrictions?(y,[n])
 manually select individual walks?(y,[n])

 step-vector based csf selection complete.
      336 csfs selected from     336 total walks.

 beginning step-vector based csf selection.
 enter [step_vector/disposition] pairs:

 enter the active orbital step vector, (-1/ to end):

 step-vector based csf selection complete.
      336 csfs selected from     336 total walks.

 beginning numerical walk selection:
 enter positive walk numbers to add walks,
 negative walk numbers to delete walks, and zero to end.

 input walk number (0 to end) [  0]:

 final csf selection complete.
      336 csfs selected from     336 total walks.
  drt construction and csf selection complete.
 
 input a title card, default=mdrt2_title
  title                                                                         
  
 input a drt file name, default=mcdrtfl
 drt and indexing arrays written to file:
 /scratch/matruc/potential-examples/potmat/H2CO-H2O-NAD/direct/WORK/mcdrtfl      
 
 write the drt file?([y],n)
 include step(*) vectors?([y],n)
 drt file is being written...


   List of selected configurations (step vectors)


   CSF#     1    3 3 3 3 3 3 0 0
   CSF#     2    3 3 3 3 3 1 2 0
   CSF#     3    3 3 3 3 3 1 0 2
   CSF#     4    3 3 3 3 3 0 3 0
   CSF#     5    3 3 3 3 3 0 1 2
   CSF#     6    3 3 3 3 3 0 0 3
   CSF#     7    3 3 3 3 1 3 2 0
   CSF#     8    3 3 3 3 1 3 0 2
   CSF#     9    3 3 3 3 1 2 3 0
   CSF#    10    3 3 3 3 1 2 1 2
   CSF#    11    3 3 3 3 1 2 0 3
   CSF#    12    3 3 3 3 1 1 2 2
   CSF#    13    3 3 3 3 1 0 3 2
   CSF#    14    3 3 3 3 1 0 2 3
   CSF#    15    3 3 3 3 0 3 3 0
   CSF#    16    3 3 3 3 0 3 1 2
   CSF#    17    3 3 3 3 0 3 0 3
   CSF#    18    3 3 3 3 0 1 3 2
   CSF#    19    3 3 3 3 0 1 2 3
   CSF#    20    3 3 3 3 0 0 3 3
   CSF#    21    3 3 3 1 3 3 2 0
   CSF#    22    3 3 3 1 3 3 0 2
   CSF#    23    3 3 3 1 3 2 3 0
   CSF#    24    3 3 3 1 3 2 1 2
   CSF#    25    3 3 3 1 3 2 0 3
   CSF#    26    3 3 3 1 3 1 2 2
   CSF#    27    3 3 3 1 3 0 3 2
   CSF#    28    3 3 3 1 3 0 2 3
   CSF#    29    3 3 3 1 2 3 3 0
   CSF#    30    3 3 3 1 2 3 1 2
   CSF#    31    3 3 3 1 2 3 0 3
   CSF#    32    3 3 3 1 2 1 3 2
   CSF#    33    3 3 3 1 2 1 2 3
   CSF#    34    3 3 3 1 2 0 3 3
   CSF#    35    3 3 3 1 1 3 2 2
   CSF#    36    3 3 3 1 1 2 3 2
   CSF#    37    3 3 3 1 1 2 2 3
   CSF#    38    3 3 3 1 0 3 3 2
   CSF#    39    3 3 3 1 0 3 2 3
   CSF#    40    3 3 3 1 0 2 3 3
   CSF#    41    3 3 3 0 3 3 3 0
   CSF#    42    3 3 3 0 3 3 1 2
   CSF#    43    3 3 3 0 3 3 0 3
   CSF#    44    3 3 3 0 3 1 3 2
   CSF#    45    3 3 3 0 3 1 2 3
   CSF#    46    3 3 3 0 3 0 3 3
   CSF#    47    3 3 3 0 1 3 3 2
   CSF#    48    3 3 3 0 1 3 2 3
   CSF#    49    3 3 3 0 1 2 3 3
   CSF#    50    3 3 3 0 0 3 3 3
   CSF#    51    3 3 1 3 3 3 2 0
   CSF#    52    3 3 1 3 3 3 0 2
   CSF#    53    3 3 1 3 3 2 3 0
   CSF#    54    3 3 1 3 3 2 1 2
   CSF#    55    3 3 1 3 3 2 0 3
   CSF#    56    3 3 1 3 3 1 2 2
   CSF#    57    3 3 1 3 3 0 3 2
   CSF#    58    3 3 1 3 3 0 2 3
   CSF#    59    3 3 1 3 2 3 3 0
   CSF#    60    3 3 1 3 2 3 1 2
   CSF#    61    3 3 1 3 2 3 0 3
   CSF#    62    3 3 1 3 2 1 3 2
   CSF#    63    3 3 1 3 2 1 2 3
   CSF#    64    3 3 1 3 2 0 3 3
   CSF#    65    3 3 1 3 1 3 2 2
   CSF#    66    3 3 1 3 1 2 3 2
   CSF#    67    3 3 1 3 1 2 2 3
   CSF#    68    3 3 1 3 0 3 3 2
   CSF#    69    3 3 1 3 0 3 2 3
   CSF#    70    3 3 1 3 0 2 3 3
   CSF#    71    3 3 1 2 3 3 3 0
   CSF#    72    3 3 1 2 3 3 1 2
   CSF#    73    3 3 1 2 3 3 0 3
   CSF#    74    3 3 1 2 3 1 3 2
   CSF#    75    3 3 1 2 3 1 2 3
   CSF#    76    3 3 1 2 3 0 3 3
   CSF#    77    3 3 1 2 1 3 3 2
   CSF#    78    3 3 1 2 1 3 2 3
   CSF#    79    3 3 1 2 1 2 3 3
   CSF#    80    3 3 1 2 0 3 3 3
   CSF#    81    3 3 1 1 3 3 2 2
   CSF#    82    3 3 1 1 3 2 3 2
   CSF#    83    3 3 1 1 3 2 2 3
   CSF#    84    3 3 1 1 2 3 3 2
   CSF#    85    3 3 1 1 2 3 2 3
   CSF#    86    3 3 1 1 2 2 3 3
   CSF#    87    3 3 1 0 3 3 3 2
   CSF#    88    3 3 1 0 3 3 2 3
   CSF#    89    3 3 1 0 3 2 3 3
   CSF#    90    3 3 1 0 2 3 3 3
   CSF#    91    3 3 0 3 3 3 3 0
   CSF#    92    3 3 0 3 3 3 1 2
   CSF#    93    3 3 0 3 3 3 0 3
   CSF#    94    3 3 0 3 3 1 3 2
   CSF#    95    3 3 0 3 3 1 2 3
   CSF#    96    3 3 0 3 3 0 3 3
   CSF#    97    3 3 0 3 1 3 3 2
   CSF#    98    3 3 0 3 1 3 2 3
   CSF#    99    3 3 0 3 1 2 3 3
   CSF#   100    3 3 0 3 0 3 3 3
   CSF#   101    3 3 0 1 3 3 3 2
   CSF#   102    3 3 0 1 3 3 2 3
   CSF#   103    3 3 0 1 3 2 3 3
   CSF#   104    3 3 0 1 2 3 3 3
   CSF#   105    3 3 0 0 3 3 3 3
   CSF#   106    3 1 3 3 3 3 2 0
   CSF#   107    3 1 3 3 3 3 0 2
   CSF#   108    3 1 3 3 3 2 3 0
   CSF#   109    3 1 3 3 3 2 1 2
   CSF#   110    3 1 3 3 3 2 0 3
   CSF#   111    3 1 3 3 3 1 2 2
   CSF#   112    3 1 3 3 3 0 3 2
   CSF#   113    3 1 3 3 3 0 2 3
   CSF#   114    3 1 3 3 2 3 3 0
   CSF#   115    3 1 3 3 2 3 1 2
   CSF#   116    3 1 3 3 2 3 0 3
   CSF#   117    3 1 3 3 2 1 3 2
   CSF#   118    3 1 3 3 2 1 2 3
   CSF#   119    3 1 3 3 2 0 3 3
   CSF#   120    3 1 3 3 1 3 2 2
   CSF#   121    3 1 3 3 1 2 3 2
   CSF#   122    3 1 3 3 1 2 2 3
   CSF#   123    3 1 3 3 0 3 3 2
   CSF#   124    3 1 3 3 0 3 2 3
   CSF#   125    3 1 3 3 0 2 3 3
   CSF#   126    3 1 3 2 3 3 3 0
   CSF#   127    3 1 3 2 3 3 1 2
   CSF#   128    3 1 3 2 3 3 0 3
   CSF#   129    3 1 3 2 3 1 3 2
   CSF#   130    3 1 3 2 3 1 2 3
   CSF#   131    3 1 3 2 3 0 3 3
   CSF#   132    3 1 3 2 1 3 3 2
   CSF#   133    3 1 3 2 1 3 2 3
   CSF#   134    3 1 3 2 1 2 3 3
   CSF#   135    3 1 3 2 0 3 3 3
   CSF#   136    3 1 3 1 3 3 2 2
   CSF#   137    3 1 3 1 3 2 3 2
   CSF#   138    3 1 3 1 3 2 2 3
   CSF#   139    3 1 3 1 2 3 3 2
   CSF#   140    3 1 3 1 2 3 2 3
   CSF#   141    3 1 3 1 2 2 3 3
   CSF#   142    3 1 3 0 3 3 3 2
   CSF#   143    3 1 3 0 3 3 2 3
   CSF#   144    3 1 3 0 3 2 3 3
   CSF#   145    3 1 3 0 2 3 3 3
   CSF#   146    3 1 2 3 3 3 3 0
   CSF#   147    3 1 2 3 3 3 1 2
   CSF#   148    3 1 2 3 3 3 0 3
   CSF#   149    3 1 2 3 3 1 3 2
   CSF#   150    3 1 2 3 3 1 2 3
   CSF#   151    3 1 2 3 3 0 3 3
   CSF#   152    3 1 2 3 1 3 3 2
   CSF#   153    3 1 2 3 1 3 2 3
   CSF#   154    3 1 2 3 1 2 3 3
   CSF#   155    3 1 2 3 0 3 3 3
   CSF#   156    3 1 2 1 3 3 3 2
   CSF#   157    3 1 2 1 3 3 2 3
   CSF#   158    3 1 2 1 3 2 3 3
   CSF#   159    3 1 2 1 2 3 3 3
   CSF#   160    3 1 2 0 3 3 3 3
   CSF#   161    3 1 1 3 3 3 2 2
   CSF#   162    3 1 1 3 3 2 3 2
   CSF#   163    3 1 1 3 3 2 2 3
   CSF#   164    3 1 1 3 2 3 3 2
   CSF#   165    3 1 1 3 2 3 2 3
   CSF#   166    3 1 1 3 2 2 3 3
   CSF#   167    3 1 1 2 3 3 3 2
   CSF#   168    3 1 1 2 3 3 2 3
   CSF#   169    3 1 1 2 3 2 3 3
   CSF#   170    3 1 1 2 2 3 3 3
   CSF#   171    3 1 0 3 3 3 3 2
   CSF#   172    3 1 0 3 3 3 2 3
   CSF#   173    3 1 0 3 3 2 3 3
   CSF#   174    3 1 0 3 2 3 3 3
   CSF#   175    3 1 0 2 3 3 3 3
   CSF#   176    3 0 3 3 3 3 3 0
   CSF#   177    3 0 3 3 3 3 1 2
   CSF#   178    3 0 3 3 3 3 0 3
   CSF#   179    3 0 3 3 3 1 3 2
   CSF#   180    3 0 3 3 3 1 2 3
   CSF#   181    3 0 3 3 3 0 3 3
   CSF#   182    3 0 3 3 1 3 3 2
   CSF#   183    3 0 3 3 1 3 2 3
   CSF#   184    3 0 3 3 1 2 3 3
   CSF#   185    3 0 3 3 0 3 3 3
   CSF#   186    3 0 3 1 3 3 3 2
   CSF#   187    3 0 3 1 3 3 2 3
   CSF#   188    3 0 3 1 3 2 3 3
   CSF#   189    3 0 3 1 2 3 3 3
   CSF#   190    3 0 3 0 3 3 3 3
   CSF#   191    3 0 1 3 3 3 3 2
   CSF#   192    3 0 1 3 3 3 2 3
   CSF#   193    3 0 1 3 3 2 3 3
   CSF#   194    3 0 1 3 2 3 3 3
   CSF#   195    3 0 1 2 3 3 3 3
   CSF#   196    3 0 0 3 3 3 3 3
   CSF#   197    1 3 3 3 3 3 2 0
   CSF#   198    1 3 3 3 3 3 0 2
   CSF#   199    1 3 3 3 3 2 3 0
   CSF#   200    1 3 3 3 3 2 1 2
   CSF#   201    1 3 3 3 3 2 0 3
   CSF#   202    1 3 3 3 3 1 2 2
   CSF#   203    1 3 3 3 3 0 3 2
   CSF#   204    1 3 3 3 3 0 2 3
   CSF#   205    1 3 3 3 2 3 3 0
   CSF#   206    1 3 3 3 2 3 1 2
   CSF#   207    1 3 3 3 2 3 0 3
   CSF#   208    1 3 3 3 2 1 3 2
   CSF#   209    1 3 3 3 2 1 2 3
   CSF#   210    1 3 3 3 2 0 3 3
   CSF#   211    1 3 3 3 1 3 2 2
   CSF#   212    1 3 3 3 1 2 3 2
   CSF#   213    1 3 3 3 1 2 2 3
   CSF#   214    1 3 3 3 0 3 3 2
   CSF#   215    1 3 3 3 0 3 2 3
   CSF#   216    1 3 3 3 0 2 3 3
   CSF#   217    1 3 3 2 3 3 3 0
   CSF#   218    1 3 3 2 3 3 1 2
   CSF#   219    1 3 3 2 3 3 0 3
   CSF#   220    1 3 3 2 3 1 3 2
   CSF#   221    1 3 3 2 3 1 2 3
   CSF#   222    1 3 3 2 3 0 3 3
   CSF#   223    1 3 3 2 1 3 3 2
   CSF#   224    1 3 3 2 1 3 2 3
   CSF#   225    1 3 3 2 1 2 3 3
   CSF#   226    1 3 3 2 0 3 3 3
   CSF#   227    1 3 3 1 3 3 2 2
   CSF#   228    1 3 3 1 3 2 3 2
   CSF#   229    1 3 3 1 3 2 2 3
   CSF#   230    1 3 3 1 2 3 3 2
   CSF#   231    1 3 3 1 2 3 2 3
   CSF#   232    1 3 3 1 2 2 3 3
   CSF#   233    1 3 3 0 3 3 3 2
   CSF#   234    1 3 3 0 3 3 2 3
   CSF#   235    1 3 3 0 3 2 3 3
   CSF#   236    1 3 3 0 2 3 3 3
   CSF#   237    1 3 2 3 3 3 3 0
   CSF#   238    1 3 2 3 3 3 1 2
   CSF#   239    1 3 2 3 3 3 0 3
   CSF#   240    1 3 2 3 3 1 3 2
   CSF#   241    1 3 2 3 3 1 2 3
   CSF#   242    1 3 2 3 3 0 3 3
   CSF#   243    1 3 2 3 1 3 3 2
   CSF#   244    1 3 2 3 1 3 2 3
   CSF#   245    1 3 2 3 1 2 3 3
   CSF#   246    1 3 2 3 0 3 3 3
   CSF#   247    1 3 2 1 3 3 3 2
   CSF#   248    1 3 2 1 3 3 2 3
   CSF#   249    1 3 2 1 3 2 3 3
   CSF#   250    1 3 2 1 2 3 3 3
   CSF#   251    1 3 2 0 3 3 3 3
   CSF#   252    1 3 1 3 3 3 2 2
   CSF#   253    1 3 1 3 3 2 3 2
   CSF#   254    1 3 1 3 3 2 2 3
   CSF#   255    1 3 1 3 2 3 3 2
   CSF#   256    1 3 1 3 2 3 2 3
   CSF#   257    1 3 1 3 2 2 3 3
   CSF#   258    1 3 1 2 3 3 3 2
   CSF#   259    1 3 1 2 3 3 2 3
   CSF#   260    1 3 1 2 3 2 3 3
   CSF#   261    1 3 1 2 2 3 3 3
   CSF#   262    1 3 0 3 3 3 3 2
   CSF#   263    1 3 0 3 3 3 2 3
   CSF#   264    1 3 0 3 3 2 3 3
   CSF#   265    1 3 0 3 2 3 3 3
   CSF#   266    1 3 0 2 3 3 3 3
   CSF#   267    1 2 3 3 3 3 3 0
   CSF#   268    1 2 3 3 3 3 1 2
   CSF#   269    1 2 3 3 3 3 0 3
   CSF#   270    1 2 3 3 3 1 3 2
   CSF#   271    1 2 3 3 3 1 2 3
   CSF#   272    1 2 3 3 3 0 3 3
   CSF#   273    1 2 3 3 1 3 3 2
   CSF#   274    1 2 3 3 1 3 2 3
   CSF#   275    1 2 3 3 1 2 3 3
   CSF#   276    1 2 3 3 0 3 3 3
   CSF#   277    1 2 3 1 3 3 3 2
   CSF#   278    1 2 3 1 3 3 2 3
   CSF#   279    1 2 3 1 3 2 3 3
   CSF#   280    1 2 3 1 2 3 3 3
   CSF#   281    1 2 3 0 3 3 3 3
   CSF#   282    1 2 1 3 3 3 3 2
   CSF#   283    1 2 1 3 3 3 2 3
   CSF#   284    1 2 1 3 3 2 3 3
   CSF#   285    1 2 1 3 2 3 3 3
   CSF#   286    1 2 1 2 3 3 3 3
   CSF#   287    1 2 0 3 3 3 3 3
   CSF#   288    1 1 3 3 3 3 2 2
   CSF#   289    1 1 3 3 3 2 3 2
   CSF#   290    1 1 3 3 3 2 2 3
   CSF#   291    1 1 3 3 2 3 3 2
   CSF#   292    1 1 3 3 2 3 2 3
   CSF#   293    1 1 3 3 2 2 3 3
   CSF#   294    1 1 3 2 3 3 3 2
   CSF#   295    1 1 3 2 3 3 2 3
   CSF#   296    1 1 3 2 3 2 3 3
   CSF#   297    1 1 3 2 2 3 3 3
   CSF#   298    1 1 2 3 3 3 3 2
   CSF#   299    1 1 2 3 3 3 2 3
   CSF#   300    1 1 2 3 3 2 3 3
   CSF#   301    1 1 2 3 2 3 3 3
   CSF#   302    1 1 2 2 3 3 3 3
   CSF#   303    1 0 3 3 3 3 3 2
   CSF#   304    1 0 3 3 3 3 2 3
   CSF#   305    1 0 3 3 3 2 3 3
   CSF#   306    1 0 3 3 2 3 3 3
   CSF#   307    1 0 3 2 3 3 3 3
   CSF#   308    1 0 2 3 3 3 3 3
   CSF#   309    0 3 3 3 3 3 3 0
   CSF#   310    0 3 3 3 3 3 1 2
   CSF#   311    0 3 3 3 3 3 0 3
   CSF#   312    0 3 3 3 3 1 3 2
   CSF#   313    0 3 3 3 3 1 2 3
   CSF#   314    0 3 3 3 3 0 3 3
   CSF#   315    0 3 3 3 1 3 3 2
   CSF#   316    0 3 3 3 1 3 2 3
   CSF#   317    0 3 3 3 1 2 3 3
   CSF#   318    0 3 3 3 0 3 3 3
   CSF#   319    0 3 3 1 3 3 3 2
   CSF#   320    0 3 3 1 3 3 2 3
   CSF#   321    0 3 3 1 3 2 3 3
   CSF#   322    0 3 3 1 2 3 3 3
   CSF#   323    0 3 3 0 3 3 3 3
   CSF#   324    0 3 1 3 3 3 3 2
   CSF#   325    0 3 1 3 3 3 2 3
   CSF#   326    0 3 1 3 3 2 3 3
   CSF#   327    0 3 1 3 2 3 3 3
   CSF#   328    0 3 1 2 3 3 3 3
   CSF#   329    0 3 0 3 3 3 3 3
   CSF#   330    0 1 3 3 3 3 3 2
   CSF#   331    0 1 3 3 3 3 2 3
   CSF#   332    0 1 3 3 3 2 3 3
   CSF#   333    0 1 3 3 2 3 3 3
   CSF#   334    0 1 3 2 3 3 3 3
   CSF#   335    0 1 2 3 3 3 3 3
   CSF#   336    0 0 3 3 3 3 3 3
