

     ******************************************
     **    PROGRAM:              MCPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 original author: Daniel Robertson, FSU
 later revisions: Ron Shepard, ANL;
                  Michal Dallos, University Vienna



 This Version of Program mcpc is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at

    Thomas Mueller
    Central Institute of Applied Mathematics
    Research Centre Juelich
    D-52425 Juelich, Germany
    Internet: th.mueller@fz-juelich.de



   ******  File header section  ******

 Headers form the restart file:
    Hermit Integral Program : SIFS version  wizard2.itc.unvie 11:58:32.237 04-Feb-08
     title                                                                          


   ******  DRT info section  ******

 Informations for the DRT no.  1
 Header form the DRT file: 
     title                                                                          
 Molecular symmetry group:   sym1
 Total number of electrons:   16
 Spin multiplicity:            1
 Number of active orbitals:    8
 Number of active electrons:  12
 Total number of CSFs:       336

   ***  Informations from the DRT number:   1

 
 Symmetry orbital summary:
 Symm.blocks:         1
 Symm.labels:         a  

 List of doubly occupied orbitals:
  1 a    2 a  

 List of active orbitals:
  3 a    4 a    5 a    6 a    7 a    8 a    9 a   10 a  


   ******  MCSCF convergence information:  ******

 MCSCF convergence criteria were satisfied.

 mcscf energy=  -114.0710757175    nuclear repulsion=    30.9052930207
 demc=             0.0000000000    wnorm=                 0.0000002653
 knorm=            0.0000000379    apxde=                 0.0000000000


 MCSCF calculation performmed for   1 symmetry.

 State averaging:
 No,  ssym, navst, wavst
  1    a      3   0.3333 0.3333 0.3333

 Input the DRT No of interest: [  1]:

In the DRT No.: 1 there are  3 states.

 Which one to take? [  1]:

 The CSFs for the state No  3 of the symmetry  a   will be printed
 according to the following print options :

 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 0) end.

 input menu number [  0]:
 csfs will be printed based on coefficient magnitudes.

 input the coefficient threshold (end with 0.) [ 0.0000]:

 List of active orbitals:
  3 a    4 a    5 a    6 a    7 a    8 a    9 a   10 a  

   csf       coeff       coeff**2    step(*)
  -----  ------------  ------------  ------------
      2  0.9813422766  0.9630326639  33333120
     23  0.1255757546  0.0157692701  33313230
     71 -0.0852787279  0.0072724614  33123330
     54 -0.0441901750  0.0019527716  33133212
     56  0.0402288588  0.0016183611  33133122
    126 -0.0368778986  0.0013599794  31323330
      7  0.0362924600  0.0013171427  33331320
      5  0.0354785451  0.0012587272  33333012
     27  0.0310841860  0.0009662266  33313032
    109 -0.0288223473  0.0008307277  31333212
     82 -0.0240851763  0.0005800957  33113232
     95 -0.0224665763  0.0005047471  33033123
    111  0.0223763774  0.0005007023  31333122
    161  0.0195263883  0.0003812798  31133322
    106  0.0188622578  0.0003557848  31333320
    180 -0.0188079218  0.0003537379  30333123
    129  0.0185169022  0.0003428757  31323132
    113  0.0162545608  0.0002642107  31333023
     58 -0.0145440615  0.0002115297  33133023
     45 -0.0143304582  0.0002053620  33303123
     19 -0.0116330828  0.0001353286  33330123
     76  0.0114845100  0.0001318940  33123033
    150  0.0107682448  0.0001159551  31233123
     22  0.0085909059  0.0000738037  33313302
    137  0.0079923702  0.0000638780  31313232
    313 -0.0076080156  0.0000578819  03333123
    169  0.0063219744  0.0000399674  31123233
    103 -0.0062370572  0.0000389009  33013233
    177 -0.0059000617  0.0000348107  30333312
    211 -0.0054316941  0.0000295033  13331322
    310 -0.0052928031  0.0000280138  03333312
    156  0.0050159131  0.0000251594  31213332
     42 -0.0047689134  0.0000227425  33303312
    147 -0.0036731426  0.0000134920  31233312
     92  0.0035209800  0.0000123973  33033312
    158  0.0031525546  0.0000099386  31213233
    167  0.0029570488  0.0000087441  31123332
    200 -0.0026369906  0.0000069537  13333212
     40  0.0024545080  0.0000060246  33310233
    206  0.0023278367  0.0000054188  13332312
    209 -0.0022005808  0.0000048426  13332123
    175 -0.0020471388  0.0000041908  31023333
    202  0.0020379253  0.0000041531  13333122
     32  0.0019851299  0.0000039407  33312132
    195  0.0019848461  0.0000039396  30123333
    101  0.0018804167  0.0000035360  33013332
     25  0.0018206593  0.0000033148  33313203
    163 -0.0015905450  0.0000025298  31133223
    188 -0.0015375929  0.0000023642  30313233
    220  0.0013859831  0.0000019209  13323132
     74  0.0013774633  0.0000018974  33123132
     14  0.0013468053  0.0000018139  33331023
     73  0.0012987902  0.0000016869  33123303
    115 -0.0012366538  0.0000015293  31332312
     38  0.0011830210  0.0000013995  33310332
    120  0.0010604318  0.0000011245  31331322
    252  0.0010011870  0.0000010024  13133322

 input the coefficient threshold (end with 0.) [ 0.0000]:

 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 0) end.

 input menu number [  0]:
