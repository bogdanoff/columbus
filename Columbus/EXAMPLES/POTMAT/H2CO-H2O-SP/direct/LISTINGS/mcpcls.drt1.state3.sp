

     ******************************************
     **    PROGRAM:              MCPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 original author: Daniel Robertson, FSU
 later revisions: Ron Shepard, ANL;
                  Michal Dallos, University Vienna



 This Version of Program mcpc is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at

    Thomas Mueller
    Central Institute of Applied Mathematics
    Research Centre Juelich
    D-52425 Juelich, Germany
    Internet: th.mueller@fz-juelich.de



   ******  File header section  ******

 Headers form the restart file:
    Hermit Integral Program : SIFS version  wizard2.itc.unvie 11:12:55.341 04-Feb-08
     title                                                                          


   ******  DRT info section  ******

 Informations for the DRT no.  1
 Header form the DRT file: 
     title                                                                          
 Molecular symmetry group:   sym1
 Total number of electrons:   16
 Spin multiplicity:            1
 Number of active orbitals:    8
 Number of active electrons:  12
 Total number of CSFs:       336

   ***  Informations from the DRT number:   1

 
 Symmetry orbital summary:
 Symm.blocks:         1
 Symm.labels:         a  

 List of doubly occupied orbitals:
  1 a    2 a  

 List of active orbitals:
  3 a    4 a    5 a    6 a    7 a    8 a    9 a   10 a  


   ******  MCSCF convergence information:  ******

 MCSCF convergence criteria were satisfied.

 mcscf energy=  -114.1323167004    nuclear repulsion=    28.6297069652
 demc=             0.0000000016    wnorm=                 0.0000006466
 knorm=            0.0000000380    apxde=                 0.0000000000


 MCSCF calculation performmed for   1 symmetry.

 State averaging:
 No,  ssym, navst, wavst
  1    a      3   0.3333 0.3333 0.3333

 Input the DRT No of interest: [  1]:

In the DRT No.: 1 there are  3 states.

 Which one to take? [  1]:

 The CSFs for the state No  3 of the symmetry  a   will be printed
 according to the following print options :

 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 0) end.

 input menu number [  0]:
 csfs will be printed based on coefficient magnitudes.

 input the coefficient threshold (end with 0.) [ 0.0000]:

 List of active orbitals:
  3 a    4 a    5 a    6 a    7 a    8 a    9 a   10 a  

   csf       coeff       coeff**2    step(*)
  -----  ------------  ------------  ------------
      2  0.9820732292  0.9644678274  33333120
     23 -0.1192469830  0.0142198430  33313230
     71 -0.0669074810  0.0044766110  33123330
    126  0.0460604885  0.0021215686  31323330
     54  0.0447681603  0.0020041882  33133212
     27 -0.0443334914  0.0019654585  33313032
    109 -0.0383944383  0.0014741329  31333212
    111  0.0358685432  0.0012865524  31333122
     56 -0.0343668204  0.0011810783  33133122
      5  0.0307249200  0.0009440207  33333012
     95 -0.0296049285  0.0008764518  33033123
     82 -0.0286550681  0.0008211129  33113232
      7  0.0233560723  0.0005455061  33331320
     58  0.0213887610  0.0004574791  33133023
    106  0.0203911177  0.0004157977  31333320
    161 -0.0195868086  0.0003836431  31133322
    129 -0.0183706482  0.0003374807  31323132
    180 -0.0176946537  0.0003131008  30333123
     45 -0.0161493471  0.0002608014  33303123
     19 -0.0144500500  0.0002088039  33330123
     76  0.0138448705  0.0001916804  33123033
    113  0.0136734624  0.0001869636  31333023
     51  0.0120071711  0.0001441722  33133320
     92  0.0106944198  0.0001143706  33033312
    177 -0.0103816822  0.0001077793  30333312
    313 -0.0080501267  0.0000648045  03333123
    103  0.0079968501  0.0000639496  33013233
    150 -0.0070331559  0.0000494653  31233123
    169  0.0069718867  0.0000486072  31123233
    310 -0.0065832335  0.0000433390  03333312
    211 -0.0060571343  0.0000366889  13331322
     16  0.0053909582  0.0000290624  33330312
     22 -0.0049309043  0.0000243138  33313302
    156  0.0048537596  0.0000235590  31213332
    131 -0.0039799987  0.0000158404  31323033
     40 -0.0030349296  0.0000092108  33310233
    167  0.0028885003  0.0000083434  31123332
    217 -0.0027176550  0.0000073856  13323330
    186 -0.0026211421  0.0000068704  30313332
    206  0.0024601105  0.0000060521  13332312
    163  0.0024356740  0.0000059325  31133223
     42 -0.0022210321  0.0000049330  33303312
    175  0.0020266585  0.0000041073  31023333
     32 -0.0019299243  0.0000037246  33312132
     25 -0.0018606858  0.0000034622  33313203
    147 -0.0017096676  0.0000029230  31233312
    137 -0.0016746166  0.0000028043  31313232
    209 -0.0016011901  0.0000025638  13332123
     14  0.0013528073  0.0000018301  33331023
    101 -0.0012895753  0.0000016630  33013332
    195  0.0012727311  0.0000016198  30123333
    202  0.0012464216  0.0000015536  13333122
    200 -0.0012132273  0.0000014719  13333212
     73  0.0011566633  0.0000013379  33123303
     69 -0.0011304629  0.0000012779  33130323
    228  0.0011288753  0.0000012744  13313232

 input the coefficient threshold (end with 0.) [ 0.0000]:

 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 0) end.

 input menu number [  0]:
