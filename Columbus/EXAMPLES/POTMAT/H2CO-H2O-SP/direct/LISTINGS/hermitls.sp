
 DALTON: user specified work memory size used,
          environment variable WRKMEM = "100000000           "

 Work memory size (LMWORK) :   100000000 =  762.94 megabytes.

 Default basis set library used :
        /sphome/kedziora/dalton/basis/                              


    $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    $$$$$$$$$$$  DALTON - An electronic structure program  $$$$$$$$$$$
    $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

               This is output from DALTON (beta-version 0.9) 

                          Principal authors:

            Trygve Helgaker,     University of Oslo,        Norway 
            Hans Joergen Jensen, University of Odense,      Denmark
            Poul Joergensen,     University of Aarhus,      Denmark
            Henrik Koch,         University of Aarhus,      Denmark
            Jeppe Olsen,         University of Lund,        Sweden 
            Hans Aagren,         University of Linkoeping,  Sweden 

                          Contributors:

            Torgeir Andersen,    University of Oslo,        Norway 
            Keld L. Bak,         University of Copenhagen,  Denmark
            Vebjoern Bakken,     University of Oslo,        Norway 
            Ove Christiansen,    University of Aarhus,      Denmark
            Paal Dahle,          University of Oslo,        Norway 
            Erik K. Dalskov,     University of Odense,      Denmark
            Thomas Enevoldsen,   University of Odense,      Denmark
            Asger Halkier,       University of Aarhus,      Denmark
            Hanne Heiberg,       University of Oslo,        Norway 
            Dan Jonsson,         University of Linkoeping,  Sweden 
            Sheela Kirpekar,     University of Odense,      Denmark
            Rika Kobayashi,      University of Aarhus,      Denmark
            Alfredo S. de Meras, Valencia University,       Spain  
            Kurt Mikkelsen,      University of Aarhus,      Denmark
            Patrick Norman,      University of Linkoeping,  Sweden 
            Martin J. Packer,    University of Sheffield,   UK     
            Kenneth Ruud,        University of Oslo,        Norway 
            Trond Saue,          University of Oslo,        Norway 
            Peter Taylor,        San Diego Superc. Center,  USA    
            Olav Vahtras,        University of Linkoeping,  Sweden

                                             Release Date:  August 1996
------------------------------------------------------------------------


      
     NOTE:
      
     This is an experimental code for the evaluation of molecular
     properties using (MC)SCF/CC wave functions. The authors accept
      no responsibility for the performance of the code or for the
     correctness of the results.
      
     The code (in whole or part) is not to be reproduced for further
     distribution without the written permission of T. Helgaker,
     H. J. Aa. Jensen or P. Taylor.
      
     If results obtained with this code are published, an
     appropriate citation would be:
      
     T. Helgaker, H. J. Aa. Jensen, P.Joergensen, H. Koch,
     J. Olsen, H. Aagren, T. Andersen, K. L. Bak, V. Bakken,
     O. Christiansen, P. Dahle, E. K. Dalskov, T. Enevoldsen,
     A. Halkier, H. Heiberg, D. Jonsson, S. Kirpekar, R. Kobayashi,
     A. S. de Meras, K. V. Mikkelsen, P. Norman, M. J. Packer,
     K. Ruud, T.Saue, P. R. Taylor, and O. Vahtras:
     DALTON, an electronic structure program"



     ******************************************
     **    PROGRAM:              DALTON      **
     **    PROGRAM VERSION:      5.4.0.0     **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************



 <<<<<<<<<< OUTPUT FROM GENERAL INPUT PROCESSING >>>>>>>>>>




 Default print level:        0

    Integral sections will be executed
    Starting in Integral Section -



 *************************************************************************
 ****************** Output from HERMIT input processing ******************
 *************************************************************************



 Default print level:        2


 Calculation of one- and two-electron Hamiltonian integrals.


 The following one-electron property integrals are calculated:

          - overlap integrals
          - Cartesian multipole moment integrals of orders 4 and lower
          - electronic angular momentum around the origin


 Changes of defaults for READIN:
 -------------------------------


 Maximum number of primitives per integral block :   10



 *************************************************************************
 ****************** Output from READIN input processing ******************
 *************************************************************************



  Title Cards
  -----------

                                                                          
                                                                          


                      SYMGRP:Point group information
                      ------------------------------

Point group: C1 

   * Character table

        |  E 
   -----+-----
    A   |   1

   * Direct product table

        | A  
   -----+-----
    A   | A  


  Atoms and basis sets
  --------------------

  Number of atom types:     5
  Total number of atoms:    7

  label    atoms   charge   prim    cont     basis   
  ----------------------------------------------------------------------
          No large orbitals attached.
  F  1        1      -1   10922********                                             
          No large orbitals attached.
  Ne 1        1       0   10922********                                             
  Ne 2        1       0   10922********                                             
  O  1        1       8      27      14      [10s4p1d|3s2p1d]                       
  C  1        1       6      27      14      [10s4p1d|3s2p1d]                       
  H  1        1       1       4       2      [4s|2s]                                
  H  2        1       1       4       2      [4s|2s]                                
  ----------------------------------------------------------------------
  ----------------------------------------------------------------------
  total:      7      15      62      32

  Spherical harmonic basis used.
  Threshold for integrals:  1.00D-15


  Cartesian Coordinates
  ---------------------

  Total number of coordinates: 21


   1   F  1     x     -3.7875261400
   2            y      2.9094373900
   3            z      0.4627765000

   4   Ne 1     x     -4.0749630000
   5            y      4.3595296100
   6            z      0.9331340200

   7   Ne 2     x     -2.2602714000
   8            y      2.5326640400
   9            z      0.5487980700

  10   O  1     x      1.2215838800
  11            y      1.0002342900
  12            z      0.6108873500

  13   C  1     x      1.0649708200
  14            y     -1.2887615000
  15            z     -0.5503910100

  16   H  1     x     -0.7441161300
  17            y     -1.9276542300
  18            z     -1.2783739700

  19   H  2     x      2.7902884900
  20            y     -2.4091856300
  21            z     -0.7066855100



   Interatomic separations (in Angstroms):
   ---------------------------------------

            F  1        Ne 1        Ne 2        O  1        C  1        H  1

   F  1    0.000000
   Ne 1    0.820928    0.000000
   Ne 2    0.833662    1.377717    0.000000
   O  1    2.837799    3.323389    2.013344    0.000000
   C  1    3.437538    4.116812    2.742993    1.360777    0.000000
   H  1    3.161425    3.942777    2.673875    2.117093    1.085902    0.000000
   H  2    4.518907    5.175021    3.797791    2.104833    1.091760    1.911693

            H  2

   H  2    0.000000




  Bond distances (angstroms):
  ---------------------------

                  atom 1     atom 2                           distance
                  ------     ------                           --------
  bond distance:    C  1       O  1                           1.360777
  bond distance:    H  1       C  1                           1.085902
  bond distance:    H  2       C  1                           1.091760


  Bond angles (degrees):
  ----------------------

                  atom 1     atom 2     atom 3                   angle
                  ------     ------     ------                   -----
  bond angle:       H  1       C  1       O  1                 119.409
  bond angle:       H  2       C  1       O  1                 117.820
  bond angle:       H  2       C  1       H  1                 122.772


  Nuclear repulsion energy :   28.629706965241


  Orbital exponents and contraction coefficients
  ----------------------------------------------


  O  1   1s    1     5484.671700    0.0018  0.0000  0.0000
   seg. cont.  2      825.234950    0.0140  0.0000  0.0000
               3      188.046960    0.0684  0.0000  0.0000
               4       52.964500    0.2327  0.0000  0.0000
               5       16.897570    0.4702  0.0000  0.0000
               6        5.799635    0.3585  0.0000  0.0000
               7       15.539616    0.0000 -0.1108  0.0000
               8        3.599934    0.0000 -0.1480  0.0000
               9        1.013762    0.0000  1.1308  0.0000
              10        0.270006    0.0000  0.0000  1.0000

  O  1   2px  11       15.539616    0.0709  0.0000
   seg. cont. 12        3.599934    0.3398  0.0000
              13        1.013762    0.7272  0.0000
              14        0.270006    0.0000  1.0000

  O  1   2py  15       15.539616    0.0709  0.0000
   seg. cont. 16        3.599934    0.3398  0.0000
              17        1.013762    0.7272  0.0000
              18        0.270006    0.0000  1.0000

  O  1   2pz  19       15.539616    0.0709  0.0000
   seg. cont. 20        3.599934    0.3398  0.0000
              21        1.013762    0.7272  0.0000
              22        0.270006    0.0000  1.0000

  O  1   3d2- 23        0.800000    1.0000

  O  1   3d1- 24        0.800000    1.0000

  O  1   3d0  25        0.800000    1.0000

  O  1   3d1+ 26        0.800000    1.0000

  O  1   3d2+ 27        0.800000    1.0000

  C  1   1s   28     3047.524900    0.0018  0.0000  0.0000
   seg. cont. 29      457.369510    0.0140  0.0000  0.0000
              30      103.948690    0.0688  0.0000  0.0000
              31       29.210155    0.2322  0.0000  0.0000
              32        9.286663    0.4679  0.0000  0.0000
              33        3.163927    0.3623  0.0000  0.0000
              34        7.868272    0.0000 -0.1193  0.0000
              35        1.881288    0.0000 -0.1609  0.0000
              36        0.544249    0.0000  1.1435  0.0000
              37        0.168714    0.0000  0.0000  1.0000

  C  1   2px  38        7.868272    0.0690  0.0000
   seg. cont. 39        1.881288    0.3164  0.0000
              40        0.544249    0.7443  0.0000
              41        0.168714    0.0000  1.0000

  C  1   2py  42        7.868272    0.0690  0.0000
   seg. cont. 43        1.881288    0.3164  0.0000
              44        0.544249    0.7443  0.0000
              45        0.168714    0.0000  1.0000

  C  1   2pz  46        7.868272    0.0690  0.0000
   seg. cont. 47        1.881288    0.3164  0.0000
              48        0.544249    0.7443  0.0000
              49        0.168714    0.0000  1.0000

  C  1   3d2- 50        0.800000    1.0000

  C  1   3d1- 51        0.800000    1.0000

  C  1   3d0  52        0.800000    1.0000

  C  1   3d1+ 53        0.800000    1.0000

  C  1   3d2+ 54        0.800000    1.0000

  H  1   1s   55       18.731137    0.0335  0.0000
   seg. cont. 56        2.825394    0.2347  0.0000
              57        0.640122    0.8138  0.0000
              58        0.161278    0.0000  1.0000

  H  2   1s   59       18.731137    0.0335  0.0000
   seg. cont. 60        2.825394    0.2347  0.0000
              61        0.640122    0.8138  0.0000
              62        0.161278    0.0000  1.0000


  Contracted Orbitals
  -------------------

   1  O  1    1s       1     2     3     4     5     6
   2  O  1    1s       7     8     9
   3  O  1    1s      10
   4  O  1    2px     11    12    13
   5  O  1    2py     15    16    17
   6  O  1    2pz     19    20    21
   7  O  1    2px     14
   8  O  1    2py     18
   9  O  1    2pz     22
  10  O  1    3d2-    23
  11  O  1    3d1-    24
  12  O  1    3d0     25
  13  O  1    3d1+    26
  14  O  1    3d2+    27
  15  C  1    1s      28    29    30    31    32    33
  16  C  1    1s      34    35    36
  17  C  1    1s      37
  18  C  1    2px     38    39    40
  19  C  1    2py     42    43    44
  20  C  1    2pz     46    47    48
  21  C  1    2px     41
  22  C  1    2py     45
  23  C  1    2pz     49
  24  C  1    3d2-    50
  25  C  1    3d1-    51
  26  C  1    3d0     52
  27  C  1    3d1+    53
  28  C  1    3d2+    54
  29  H  1    1s      55    56    57
  30  H  1    1s      58
  31  H  2    1s      59    60    61
  32  H  2    1s      62




  Symmetry Orbitals
  -----------------

  Number of orbitals in each symmetry:        32


  Symmetry  A  ( 1)

    1     O  1     1s         1
    2     O  1     1s         2
    3     O  1     1s         3
    4     O  1     2px        4
    5     O  1     2py        5
    6     O  1     2pz        6
    7     O  1     2px        7
    8     O  1     2py        8
    9     O  1     2pz        9
   10     O  1     3d2-      10
   11     O  1     3d1-      11
   12     O  1     3d0       12
   13     O  1     3d1+      13
   14     O  1     3d2+      14
   15     C  1     1s        15
   16     C  1     1s        16
   17     C  1     1s        17
   18     C  1     2px       18
   19     C  1     2py       19
   20     C  1     2pz       20
   21     C  1     2px       21
   22     C  1     2py       22
   23     C  1     2pz       23
   24     C  1     3d2-      24
   25     C  1     3d1-      25
   26     C  1     3d0       26
   27     C  1     3d1+      27
   28     C  1     3d2+      28
   29     H  1     1s        29
   30     H  1     1s        30
   31     H  2     1s        31
   32     H  2     1s        32

  Symmetries of electric field:  A  (1)  A  (1)  A  (1)

  Symmetries of magnetic field:  A  (1)  A  (1)  A  (1)


 Copy of input to READIN
 -----------------------

INTGRL                                                                          
                                                                                
                                                                                
s   5    0           0.10D-14                                                   
      -0.8    1    0                                                            
F  1  -3.787526140000000   2.909437390000000   0.462776500000000       *        
       0.4    2    0                                                            
Ne 1  -4.074963000000000   4.359529610000000   0.933134020000000       *        
Ne 2  -2.260271400000000   2.532664040000000   0.548798070000000       *        
       8.0    1    3    3    2    1                                             
O  1   1.221583880000000   1.000234290000000   0.610887350000000       *        
H   6   1                                                                       
       5484.67170000         0.00183110                                         
        825.23495000         0.01395010                                         
        188.04696000         0.06844510                                         
         52.96450000         0.23271430                                         
         16.89757000         0.47019300                                         
          5.79963530         0.35852090                                         
H   3   1                                                                       
         15.53961600        -0.11077750                                         
          3.59993360        -0.14802630                                         
          1.01376180         1.13076700                                         
H   1   1                                                                       
          0.27000580         1.00000000                                         
H   3   1                                                                       
         15.53961600         0.07087430                                         
          3.59993360         0.33975280                                         
          1.01376180         0.72715860                                         
H   1   1                                                                       
          0.27000580         1.00000000                                         
H   1   1                                                                       
          0.80000000         1.00000000                                         
       6.0    1    3    3    2    1                                             
C  1   1.064970820000000  -1.288761500000000  -0.550391010000000       *        
H   6   1                                                                       
       3047.52490000         0.00183470                                         
        457.36951000         0.01403730                                         
        103.94869000         0.06884260                                         
         29.21015500         0.23218440                                         
          9.28666300         0.46794130                                         
          3.16392700         0.36231200                                         
H   3   1                                                                       
          7.86827240        -0.11933240                                         
          1.88128850        -0.16085420                                         
          0.54424930         1.14345640                                         
H   1   1                                                                       
          0.16871440         1.00000000                                         
H   3   1                                                                       
          7.86827240         0.06899910                                         
          1.88128850         0.31642400                                         
          0.54424930         0.74430830                                         
H   1   1                                                                       
          0.16871440         1.00000000                                         
H   1   1                                                                       
          0.80000000         1.00000000                                         
       1.0    2    1    2                                                       
H  1  -0.744116130000000  -1.927654230000000  -1.278373970000000       *        
H  2   2.790288490000000  -2.409185630000000  -0.706685510000000       *        
H   3   1                                                                       
         18.73113700         0.03349460                                         
          2.82539370         0.23472695                                         
          0.64012170         0.81375733                                         
H   1   1                                                                       
          0.16127780         1.00000000                                         


 herdrv: noofopt=           5


 ************************************************************************
 ************************** Output from HERONE **************************
 ************************************************************************

 prop, itype T           1


   358 atomic overlap integrals written in   1 buffers.
 Percentage non-zero integrals:  67.80
 prop, itype T           2


   528 one-el. Hamil. integrals written in   1 buffers.
 Percentage non-zero integrals: 100.00
 prop, itype T           3


   361 kinetic energy integrals written in   1 buffers.
 Percentage non-zero integrals:  68.37


 inttyp=           1 noptyp=           1


                    +---------------------------------+
                    ! Integrals of operator: OVERLAP  !
                    +---------------------------------+



 finopt,noofopt,last1=           0           5           0
 inttyp=           8 noptyp=           1


                    +---------------------------------+
                    ! Integrals of operator: CM000000 !
                    +---------------------------------+



 finopt,noofopt,last1=           1           5           0
 inttyp=           8 noptyp=           3


                    +---------------------------------+
                    ! Integrals of operator: CM010000 !
                    +---------------------------------+

 typea=            1 typeb=            0 last=           1
 lstflg=           1


                    +---------------------------------+
                    ! Integrals of operator: CM000100 !
                    +---------------------------------+

 typea=            1 typeb=            1 last=           1
 lstflg=           1


                    +---------------------------------+
                    ! Integrals of operator: CM000001 !
                    +---------------------------------+

 typea=            1 typeb=            2 last=           2
 lstflg=           1


 finopt,noofopt,last1=           2           5           0
 inttyp=           8 noptyp=           6


                    +---------------------------------+
                    ! Integrals of operator: CM020000 !
                    +---------------------------------+

 typea=            1 typeb=            3 last=           1
 lstflg=           1


                    +---------------------------------+
                    ! Integrals of operator: CM010100 !
                    +---------------------------------+

 typea=            1 typeb=            4 last=           1
 lstflg=           1


                    +---------------------------------+
                    ! Integrals of operator: CM010001 !
                    +---------------------------------+

 typea=            1 typeb=            5 last=           1
 lstflg=           1


                    +---------------------------------+
                    ! Integrals of operator: CM000200 !
                    +---------------------------------+

 typea=            1 typeb=            6 last=           1
 lstflg=           1


                    +---------------------------------+
                    ! Integrals of operator: CM000101 !
                    +---------------------------------+

 typea=            1 typeb=            7 last=           1
 lstflg=           1


                    +---------------------------------+
                    ! Integrals of operator: CM000002 !
                    +---------------------------------+

 typea=            1 typeb=            8 last=           2
 lstflg=           1


 finopt,noofopt,last1=           3           5           0
 inttyp=           8 noptyp=          10


                    +---------------------------------+
                    ! Integrals of operator: CM030000 !
                    +---------------------------------+

 typea=            1 typeb=            9 last=           1
 lstflg=           1


                    +---------------------------------+
                    ! Integrals of operator: CM020100 !
                    +---------------------------------+

 typea=            1 typeb=           10 last=           1
 lstflg=           1


                    +---------------------------------+
                    ! Integrals of operator: CM020001 !
                    +---------------------------------+

 typea=            1 typeb=           11 last=           1
 lstflg=           1


                    +---------------------------------+
                    ! Integrals of operator: CM010200 !
                    +---------------------------------+

 typea=            1 typeb=           12 last=           1
 lstflg=           1


                    +---------------------------------+
                    ! Integrals of operator: CM010101 !
                    +---------------------------------+

 typea=            1 typeb=           13 last=           1
 lstflg=           1


                    +---------------------------------+
                    ! Integrals of operator: CM010002 !
                    +---------------------------------+

 typea=            1 typeb=           14 last=           1
 lstflg=           1


                    +---------------------------------+
                    ! Integrals of operator: CM000300 !
                    +---------------------------------+

 typea=            1 typeb=           15 last=           1
 lstflg=           1


                    +---------------------------------+
                    ! Integrals of operator: CM000201 !
                    +---------------------------------+

 typea=            1 typeb=           16 last=           1
 lstflg=           1


                    +---------------------------------+
                    ! Integrals of operator: CM000102 !
                    +---------------------------------+

 typea=            1 typeb=           17 last=           1
 lstflg=           1


                    +---------------------------------+
                    ! Integrals of operator: CM000003 !
                    +---------------------------------+

 typea=            1 typeb=           18 last=           2
 lstflg=           1


 finopt,noofopt,last1=           4           5           0
 inttyp=           8 noptyp=          15


                    +---------------------------------+
                    ! Integrals of operator: CM040000 !
                    +---------------------------------+

 typea=            1 typeb=           19 last=           1
 lstflg=           1


                    +---------------------------------+
                    ! Integrals of operator: CM030100 !
                    +---------------------------------+

 typea=            1 typeb=           20 last=           1
 lstflg=           1


                    +---------------------------------+
                    ! Integrals of operator: CM030001 !
                    +---------------------------------+

 typea=            1 typeb=           21 last=           1
 lstflg=           1


                    +---------------------------------+
                    ! Integrals of operator: CM020200 !
                    +---------------------------------+

 typea=            1 typeb=           22 last=           1
 lstflg=           1


                    +---------------------------------+
                    ! Integrals of operator: CM020101 !
                    +---------------------------------+

 typea=            1 typeb=           23 last=           1
 lstflg=           1


                    +---------------------------------+
                    ! Integrals of operator: CM020002 !
                    +---------------------------------+

 typea=            1 typeb=           24 last=           1
 lstflg=           1


                    +---------------------------------+
                    ! Integrals of operator: CM010300 !
                    +---------------------------------+

 typea=            1 typeb=           25 last=           1
 lstflg=           1


                    +---------------------------------+
                    ! Integrals of operator: CM010201 !
                    +---------------------------------+

 typea=            1 typeb=           26 last=           1
 lstflg=           1


                    +---------------------------------+
                    ! Integrals of operator: CM010102 !
                    +---------------------------------+

 typea=            1 typeb=           27 last=           1
 lstflg=           1


                    +---------------------------------+
                    ! Integrals of operator: CM010003 !
                    +---------------------------------+

 typea=            1 typeb=           28 last=           1
 lstflg=           1


                    +---------------------------------+
                    ! Integrals of operator: CM000400 !
                    +---------------------------------+

 typea=            1 typeb=           29 last=           1
 lstflg=           1


                    +---------------------------------+
                    ! Integrals of operator: CM000301 !
                    +---------------------------------+

 typea=            1 typeb=           30 last=           1
 lstflg=           1


                    +---------------------------------+
                    ! Integrals of operator: CM000202 !
                    +---------------------------------+

 typea=            1 typeb=           31 last=           1
 lstflg=           1


                    +---------------------------------+
                    ! Integrals of operator: CM000103 !
                    +---------------------------------+

 typea=            1 typeb=           32 last=           1
 lstflg=           1


                    +---------------------------------+
                    ! Integrals of operator: CM000004 !
                    +---------------------------------+

 typea=            1 typeb=           33 last=           2
 lstflg=           1


 finopt,noofopt,last1=           5           5           2
 inttyp=          18 noptyp=           3


                    +---------------------------------+
                    ! Integrals of operator: XANGMOM  !
                    +---------------------------------+

 angular moment
 typea=            2 typeb=            6 last=           1
 lstflg=           1


                    +---------------------------------+
                    ! Integrals of operator: YANGMOM  !
                    +---------------------------------+

 angular moment
 typea=            2 typeb=            7 last=           1
 lstflg=           1


                    +---------------------------------+
                    ! Integrals of operator: ZANGMOM  !
                    +---------------------------------+

 angular moment
 typea=            2 typeb=            8 last=           2
 lstflg=           2




 ************************************************************************
 ************************** Output from TWOINT **************************
 ************************************************************************

 calling sifew2:luinta,info,num,last,nrec
 calling sifew2:          11           2        4096        3272        4096
        2730        1768           2 -1431642152

 Number of two-electron integrals written:    130078 (93.1%)
 Kilobytes written:                             1573




 >>>> Total CPU  time used in HERMIT:   0.20 seconds
 >>>> Total wall time used in HERMIT:   0.00 seconds

- End of Integral Section
