

     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 This program allows the csf mixing coefficient and orbital expansion coefficient
 optimization using the graphical unitary group approach and the exponential
 operator mcscf method.
 references:  r. shepard and j. simons, ' int. j. quantum chem. symp. 14, 211 (1980).
              r. shepard, i. shavitt, and j. simons, j. chem. phys. 76, 543 (1982).
              r. shepard in "ab initio methods in quantum chemistry ii" advances in chemical
                  physics 69, edited by k. p. lawley (wiley, new york, 1987) pp. 63-200.
 Original autor: Ron Shepard, ANL
 Later revisions: Michal Dallos, University Vienna

 This Version of Program MCSCF is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at

    Thomas Mueller
    Central Institute of Applied Mathematics
    Research Centre Juelich
    D-52425 Juelich, Germany
    Internet: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.4.0.2     **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 Workspace allocation information:
       100000000 of real*8 words (  762.94 MB) of work space has been allocated.

 user input information:

 ======== echo of the mcscf input ========
 ------------------------------------------------------------------------
  &input
   niter=60,
   nmiter=30,
   nciitr=30,
   tol(3)=1.e-4,
   tol(2)=1.e-4,
   tol(1)=1.e-8,
   NSTATE=0,
   npath=1,3,9,10,13,17,19,21,-11,12, 2,
   ncoupl=5,
   FCIORB=  1,3,20,1,4,20,1,5,20,1,6,20,1,7,20,1,8,20,1,9,20,1,10,20
   NAVST(1) = 3,
   WAVST(1,1)=1 ,
   WAVST(1,2)=1 ,
   WAVST(1,3)=1 ,
  &end
 ------------------------------------------------------------------------


 ***  Integral file informations  ***


 input integral file : /scratch/matruc/potential-examples/potmat/H2CO-H2O-SP/dir
 ect

 Integral file header information:
 Hermit Integral Program : SIFS version  wizard2.itc.unvie 11:12:55.341 04-Feb-08

 Core type energy values:
 energy( 1)=  2.862970696524E+01, ietype=   -1,    core energy of type: Nuc.Rep.
 total ao core energy =   28.629706965


   ******  Basis set informations:  ******

 Number of irreps:                  1
 Total number of basis functions:  32

 irrep no.              1
 irrep label           A  
 no. of bas.fcions.    32


 ***  MCSCF optimization procedure parmeters:  ***


 maximum number of mcscf iterations:        niter=    60

 maximum number of psci micro-iterations:   nmiter=   30
 maximum r,s subspace dimension allowed:    nvrsmx=   30

 tol(1)=  1.0000E-08. . . . delta-emc convergence criterion.
 tol(2)=  1.0000E-04. . . . wnorm convergence criterion.
 tol(3)=  1.0000E-04. . . . knorm convergence criterion.
 tol(4)=  1.0000E-08. . . . apxde convergence criterion.
 tol(5)=  1.0000E-04. . . . small diagonal matrix element tolerance.
 tol(6)=  1.0000E-06. . . . minimum ci-psci residual norm.
 tol(7)=  1.0000E-05. . . . maximum ci-psci residual norm.
 tol(8)=  1.0000E+00. . . . maximum abs(k(xy)) allowed.
 tol(9)=  1.0000E+00. . . . wnorm coupling tolerance.
 tol(10)= 0.0000E+00. . . . maximum psci emergency shift parameter.
 tol(11)= 0.0000E+00. . . . minimum psci emergency shift parameter.
 tol(12)= 0.0000E+00. . . . increment of psci emergency shift parameter.


 *** State averaging informations: ***


 MCSCF calculation performed for  1 DRT.

 DRT  first state   no.of aver.states   weights
  1   ground state          3             0.333 0.333 0.333

 The number of hmc(*) eigenvalues and eigenvectors calculated each iteration per symmetry:
 Symm.   no.of eigenv.(=ncol)
             4

 orbital coefficients are optimized for the ground state (nstate=0).

 Orbitals included in invariant subspaces:
   symmetry   orbital   mask
       1       3(  3)    20
       1       4(  4)    20
       1       5(  5)    20
       1       6(  6)    20
       1       7(  7)    20
       1       8(  8)    20
       1       9(  9)    20
       1      10( 10)    20

 npath(*) options:
  2:  orbital-state coupling terms will be included beginning on iteration ncoupl=  5
  3:  print intermediate timing information.
  9:  suppress the drt listing.
 10:  suppress the hmc(*) eigenvector listing.
 12:  diagonalize the hmc(*) matrix iteratively.
        nunitv= 1 nciitr=30 mxvadd=20 nvcimx=20
       rtolci(*),wnorm=     1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 0.0000E+00
   noldv =   0
 13:  get initial orbitals from the formatted file, mocoef.
 17:  print the final natural orbitals and occupations.
 19:  transform the virtual orbitals to diagonalize qvv(*).
 21:  write out the one- and two- electron density for further use (files:mcd1fl, mcd2fl).


   ******  DRT info section  ******


 Informations for the DRT no.  1

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a  
 Total number of electrons:   16
 Spin multiplicity:            1
 Number of active orbitals:    8
 Number of active electrons:  12
 Total number of CSFs:       336
 
 !timer: initialization                  cpu_time=     0.003 walltime=     0.003

 faar:   0 active-active rotations allowed out of:  28 possible.


 Number of active-double rotations:     16
 Number of active-active rotations:      0
 Number of double-virtual rotations:    44
 Number of active-virtual rotations:   176

 Size of orbital-Hessian matrix B:                    30284
 Size of the orbital-state Hessian matrix C:         237888
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:         268172



   ****** Integral transformation section ******


 number of blocks to be transformed in-core is   0
 number of blocks to be transformed out of core is    1

 in-core ao list requires    0 segments.
 out of core ao list requires    1 segments.
 each segment has length  99991847 working precision words.

               ao integral sort statistics
 length of records:      4096
 number of buckets:   1
 scratch file used is da1:
 amount of core needed for in-core arrays:  3040

 twoao processed     130078 two electron ao integrals.

     259628 ao integrals were written into   96 records

 Source of the initial MO coeficients:

 Input MO coefficient file: /scratch/matruc/potential-examples/potmat/H2CO-H2O-SP/direct
 

               starting mcscf iteration...   1
 !timer:                                 cpu_time=     0.048 walltime=     0.048

 orbital-state coupling will not be calculated this iteration.

 number of transformed integrals put on file:     51865
 number of records written:    19
 !timer: 2-e transformation              cpu_time=     0.053 walltime=     0.057

 Size of orbital-Hessian matrix B:                    30284
 Total size of the state Hessian matrix M:                0
 Total size of HESSIAN-matrix for linear con          30284

 !timer: mosrt1                          cpu_time=     0.014 walltime=     0.014
 !timer: mosrt2                          cpu_time=     0.003 walltime=     0.004
 !timer: mosort                          cpu_time=     0.017 walltime=     0.017
 !timer: hdiag(*) construction           cpu_time=     0.000 walltime=     0.001
 !timer: hmcft                           cpu_time=     0.000 walltime=     0.001

 trial vectors are generated internally.

 trial vector  1 is unit matrix column     1
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 ciiter=  24 noldhv= 10 noldv= 10
 !timer: hmc(*) diagonalization          cpu_time=     0.264 walltime=     0.264

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -114.2196349719     -142.8493419372        0.0000004485        0.0000010000
    2      -114.0902640487     -142.7199710139        0.0000003642        0.0000010000
    3      -113.9285030650     -142.5582100303        0.0000009891        0.0000010000
    4      -113.8671845739     -142.4968915392        0.0021746386        0.0100000000
 !timer: hmcvec                          cpu_time=     0.265 walltime=     0.265
 !timer: rdft                            cpu_time=     0.028 walltime=     0.028
 !timer: mfad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfva                            cpu_time=     0.001 walltime=     0.000
 !timer: mfvd                            cpu_time=     0.000 walltime=     0.000
 !timer: bvaad                           cpu_time=     0.001 walltime=     0.001
 !timer: bvaadf                          cpu_time=     0.000 walltime=     0.000
 !timer: mqaa                            cpu_time=     0.000 walltime=     0.000
 !timer: mfaa                            cpu_time=     0.000 walltime=     0.000
 !timer: badad                           cpu_time=     0.001 walltime=     0.002
 !timer: badadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvava                           cpu_time=     0.003 walltime=     0.003
 !timer: bvavaf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvavd                           cpu_time=     0.001 walltime=     0.001
 !timer: bvavdf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvdadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdvd                           cpu_time=     0.000 walltime=     0.000
 !timer: hbcon                           cpu_time=     0.038 walltime=     0.039
 
  tol(10)=  0.000000000000000E+000  eshsci=  5.722905218843439E-002
 Total number of micro iterations:   10

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.97815369 pnorm= 0.0000E+00 rznorm= 6.6824E-06 rpnorm= 0.0000E+00 noldr= 10 nnewr= 10 nolds=  0 nnews=  0
 
 !timer: Direct Bxr time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  cpu_time=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: solvek total                    cpu_time=     0.005 walltime=     0.005

 fdd(*) eigenvalues. symmetry block  1
   -41.540634  -22.676799

 qvv(*) eigenvalues. symmetry block  1
     0.649080    0.671501    1.508885    1.511578    1.652049    1.681419    2.205051    2.213614    2.311474    2.342670
     2.498432    3.399700    3.411724    3.618472    3.948462    4.140301    4.248285    4.633965    4.931204    5.355064
     5.566795    6.158621
 *** warning *** large active-orbital occupation. i=  8 nocc= 1.9992E+00
 !timer: motran                          cpu_time=     0.002 walltime=     0.002

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 cpu_time=     0.382 walltime=     0.387

 all mcscf convergence criteria are not satisfied.
 iter=    1 emc= -114.0794673619 demc= 1.1408E+02 wnorm= 4.5783E-01 knorm= 2.0788E-01 apxde= 2.3050E-02    *not converged* 

               starting mcscf iteration...   2
 !timer:                                 cpu_time=     0.430 walltime=     0.435

 orbital-state coupling will not be calculated this iteration.

 number of transformed integrals put on file:     51865
 number of records written:    19
 !timer: 2-e transformation              cpu_time=     0.047 walltime=     0.046

 Size of orbital-Hessian matrix B:                    30284
 Total size of the state Hessian matrix M:                0
 Total size of HESSIAN-matrix for linear con          30284

 !timer: mosrt1                          cpu_time=     0.012 walltime=     0.012
 !timer: mosrt2                          cpu_time=     0.003 walltime=     0.003
 !timer: mosort                          cpu_time=     0.015 walltime=     0.015
 !timer: hdiag(*) construction           cpu_time=     0.001 walltime=     0.001
 !timer: hmcft                           cpu_time=     0.001 walltime=     0.001

   4 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.011
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.011
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.011
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 ciiter=  22 noldhv=  9 noldv=  9
 !timer: hmc(*) diagonalization          cpu_time=     0.244 walltime=     0.245

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -114.2324157002     -142.8621226655        0.0000033843        0.0000100000
    2      -114.1280513207     -142.7577582860        0.0000039840        0.0000100000
    3      -113.9605494193     -142.5902563845        0.0000043715        0.0000100000
    4      -113.8680161809     -142.4977231461        0.0072054662        0.0100000000
 !timer: hmcvec                          cpu_time=     0.246 walltime=     0.246
 !timer: rdft                            cpu_time=     0.029 walltime=     0.029
 !timer: mfad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfva                            cpu_time=     0.000 walltime=     0.001
 !timer: mfvd                            cpu_time=     0.000 walltime=     0.000
 !timer: bvaad                           cpu_time=     0.001 walltime=     0.001
 !timer: bvaadf                          cpu_time=     0.000 walltime=     0.000
 !timer: mqaa                            cpu_time=     0.000 walltime=     0.000
 !timer: mfaa                            cpu_time=     0.000 walltime=     0.000
 !timer: badad                           cpu_time=     0.001 walltime=     0.001
 !timer: badadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvava                           cpu_time=     0.003 walltime=     0.004
 !timer: bvavaf                          cpu_time=     0.001 walltime=     0.000
 !timer: bvavd                           cpu_time=     0.001 walltime=     0.001
 !timer: bvavdf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvdadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdvd                           cpu_time=     0.000 walltime=     0.000
 !timer: hbcon                           cpu_time=     0.038 walltime=     0.038
 
  tol(10)=  0.000000000000000E+000  eshsci=  1.100130792378017E-002
 Total number of micro iterations:    7

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0=-0.91289915 pnorm= 0.0000E+00 rznorm= 2.8247E-06 rpnorm= 0.0000E+00 noldr=  7 nnewr=  7 nolds=  0 nnews=  0
 
 !timer: Direct Bxr time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  cpu_time=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: solvek total                    cpu_time=     0.004 walltime=     0.004

 fdd(*) eigenvalues. symmetry block  1
   -41.302037  -22.629403

 qvv(*) eigenvalues. symmetry block  1
     0.657381    0.695661    1.500851    1.549901    1.678480    1.698903    2.220742    2.240480    2.321461    2.405986
     2.561032    3.446806    3.470521    3.652583    4.009350    4.186988    4.301415    4.669278    4.963253    5.400235
     5.604524    6.216509
 !timer: motran                          cpu_time=     0.003 walltime=     0.002

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 cpu_time=     0.353 walltime=     0.353

 all mcscf convergence criteria are not satisfied.
 iter=    2 emc= -114.1070054801 demc= 2.7538E-02 wnorm= 8.8010E-02 knorm= 4.0819E-01 apxde= 5.3500E-03    *not converged* 

               starting mcscf iteration...   3
 !timer:                                 cpu_time=     0.783 walltime=     0.788

 orbital-state coupling will not be calculated this iteration.

 number of transformed integrals put on file:     51865
 number of records written:    19
 !timer: 2-e transformation              cpu_time=     0.051 walltime=     0.051

 Size of orbital-Hessian matrix B:                    30284
 Total size of the state Hessian matrix M:                0
 Total size of HESSIAN-matrix for linear con          30284

 !timer: mosrt1                          cpu_time=     0.015 walltime=     0.015
 !timer: mosrt2                          cpu_time=     0.003 walltime=     0.003
 !timer: mosort                          cpu_time=     0.018 walltime=     0.018
 !timer: hdiag(*) construction           cpu_time=     0.001 walltime=     0.001
 !timer: hmcft                           cpu_time=     0.001 walltime=     0.001

   4 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.011 walltime=     0.011
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.011
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.011 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 ciiter=  14 noldhv= 18 noldv= 18
 !timer: hmc(*) diagonalization          cpu_time=     0.173 walltime=     0.174

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -114.2382936364     -142.8680006017        0.0000039188        0.0000074998
    2      -114.1364443168     -142.7661512820        0.0000045289        0.0000074998
    3      -113.9695986616     -142.5993056269        0.0000071853        0.0000074998
    4      -113.8760514892     -142.5057584545        0.0079595803        0.0100000000
 !timer: hmcvec                          cpu_time=     0.175 walltime=     0.175
 !timer: rdft                            cpu_time=     0.024 walltime=     0.024
 !timer: mfad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfvd                            cpu_time=     0.000 walltime=     0.000
 !timer: bvaad                           cpu_time=     0.001 walltime=     0.001
 !timer: bvaadf                          cpu_time=     0.000 walltime=     0.000
 !timer: mqaa                            cpu_time=     0.000 walltime=     0.000
 !timer: mfaa                            cpu_time=     0.000 walltime=     0.000
 !timer: badad                           cpu_time=     0.001 walltime=     0.001
 !timer: badadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvava                           cpu_time=     0.003 walltime=     0.003
 !timer: bvavaf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvavd                           cpu_time=     0.001 walltime=     0.001
 !timer: bvavdf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvdadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdvd                           cpu_time=     0.000 walltime=     0.000
 !timer: hbcon                           cpu_time=     0.033 walltime=     0.033
 
  tol(10)=  0.000000000000000E+000  eshsci=  3.369411295832747E-003
 Total number of micro iterations:    7

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.96307250 pnorm= 0.0000E+00 rznorm= 5.8488E-06 rpnorm= 0.0000E+00 noldr=  7 nnewr=  7 nolds=  0 nnews=  0
 
 !timer: Direct Bxr time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  cpu_time=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: solvek total                    cpu_time=     0.004 walltime=     0.004

 fdd(*) eigenvalues. symmetry block  1
   -41.259060  -22.622930

 qvv(*) eigenvalues. symmetry block  1
     0.658215    0.693394    1.482687    1.495402    1.666886    1.682517    1.972533    2.231449    2.320634    2.418162
     2.572006    3.454218    3.479408    3.657388    4.019563    4.193286    4.310863    4.660339    4.967315    5.408181
     5.609879    6.226400
 !timer: motran                          cpu_time=     0.002 walltime=     0.002

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 cpu_time=     0.285 walltime=     0.285

 all mcscf convergence criteria are not satisfied.
 iter=    3 emc= -114.1147788716 demc= 7.7734E-03 wnorm= 2.6955E-02 knorm= 2.6924E-01 apxde= 1.6436E-03    *not converged* 

               starting mcscf iteration...   4
 !timer:                                 cpu_time=     1.068 walltime=     1.073

 orbital-state coupling will not be calculated this iteration.

 number of transformed integrals put on file:     51864
 number of records written:    19
 !timer: 2-e transformation              cpu_time=     0.047 walltime=     0.047

 Size of orbital-Hessian matrix B:                    30284
 Total size of the state Hessian matrix M:                0
 Total size of HESSIAN-matrix for linear con          30284

 !timer: mosrt1                          cpu_time=     0.012 walltime=     0.012
 !timer: mosrt2                          cpu_time=     0.003 walltime=     0.003
 !timer: mosort                          cpu_time=     0.015 walltime=     0.015
 !timer: hdiag(*) construction           cpu_time=     0.000 walltime=     0.001
 !timer: hmcft                           cpu_time=     0.000 walltime=     0.001

   4 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.014 walltime=     0.014
 !timer: hmcxv                           cpu_time=     0.014 walltime=     0.014
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.014
 !timer: hmcxv                           cpu_time=     0.014 walltime=     0.014
 !timer: hmcxv                           cpu_time=     0.014 walltime=     0.014
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.014 walltime=     0.014
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.011
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 ciiter=  18 noldhv= 11 noldv= 11
 !timer: hmc(*) diagonalization          cpu_time=     0.231 walltime=     0.295

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -114.2415914176     -142.8712983829        0.0000002540        0.0000010000
    2      -114.1388136635     -142.7685206287        0.0000003143        0.0000010000
    3      -113.9730249315     -142.6027318968        0.0000008983        0.0000010000
    4      -113.8780766155     -142.5077835807        0.0083197636        0.0100000000
 !timer: hmcvec                          cpu_time=     0.232 walltime=     0.296
 !timer: rdft                            cpu_time=     0.030 walltime=     0.030
 !timer: mfad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfva                            cpu_time=     0.001 walltime=     0.000
 !timer: mfvd                            cpu_time=     0.000 walltime=     0.000
 !timer: bvaad                           cpu_time=     0.001 walltime=     0.001
 !timer: bvaadf                          cpu_time=     0.000 walltime=     0.000
 !timer: mqaa                            cpu_time=     0.000 walltime=     0.000
 !timer: mfaa                            cpu_time=     0.000 walltime=     0.000
 !timer: badad                           cpu_time=     0.000 walltime=     0.001
 !timer: badadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvava                           cpu_time=     0.004 walltime=     0.003
 !timer: bvavaf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvavd                           cpu_time=     0.000 walltime=     0.001
 !timer: bvavdf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvdadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdvd                           cpu_time=     0.000 walltime=     0.000
 !timer: hbcon                           cpu_time=     0.042 walltime=     0.041
 
  tol(10)=  0.000000000000000E+000  eshsci=  1.976689643152801E-003
 Total number of micro iterations:    7

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.96473022 pnorm= 0.0000E+00 rznorm= 4.9106E-06 rpnorm= 0.0000E+00 noldr=  7 nnewr=  7 nolds=  0 nnews=  0
 
 !timer: Direct Bxr time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  cpu_time=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: solvek total                    cpu_time=     0.004 walltime=     0.004

 fdd(*) eigenvalues. symmetry block  1
   -41.245973  -22.623607

 qvv(*) eigenvalues. symmetry block  1
     0.649216    0.677006    1.354847    1.494186    1.675682    1.683201    1.928435    2.238483    2.328031    2.422444
     2.575568    3.456426    3.483742    3.658694    4.023056    4.191052    4.314016    4.656145    4.967877    5.410459
     5.610919    6.229272
 !timer: motran                          cpu_time=     0.002 walltime=     0.002

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 cpu_time=     0.344 walltime=     0.407

 all mcscf convergence criteria are not satisfied.
 iter=    4 emc= -114.1178100042 demc= 3.0311E-03 wnorm= 1.5814E-02 knorm= 2.6324E-01 apxde= 1.5923E-03    *not converged* 

               starting mcscf iteration...   5
 !timer:                                 cpu_time=     1.412 walltime=     1.480

 orbital-state coupling will be calculated this iteration.

 number of transformed integrals put on file:     51865
 number of records written:    19
 !timer: 2-e transformation              cpu_time=     0.050 walltime=     0.050

 Size of orbital-Hessian matrix B:                    30284
 Size of the orbital-state Hessian matrix C:         237888
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:         268172

 !timer: mosrt1                          cpu_time=     0.014 walltime=     0.014
 !timer: mosrt2                          cpu_time=     0.003 walltime=     0.003
 !timer: mosort                          cpu_time=     0.017 walltime=     0.017
 !timer: hdiag(*) construction           cpu_time=     0.001 walltime=     0.001
 !timer: hmcft                           cpu_time=     0.001 walltime=     0.001

   4 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.011 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.011 walltime=     0.011
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.011 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.011 walltime=     0.011
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 ciiter=  28 noldhv=  4 noldv=  4
 !timer: hmc(*) diagonalization          cpu_time=     0.298 walltime=     0.298

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -114.2459507342     -142.8756576994        0.0000006419        0.0000010000
    2      -114.1407835043     -142.7704904695        0.0000006369        0.0000010000
    3      -113.9754271274     -142.6051340927        0.0000008718        0.0000010000
    4      -113.8791625365     -142.5088695018        0.0058623298        0.0100000000
 !timer: hmcvec                          cpu_time=     0.300 walltime=     0.299
 !timer: cadu                            cpu_time=     0.000 walltime=     0.000
 !timer: cvdu                            cpu_time=     0.000 walltime=     0.000
 !timer: rdft                            cpu_time=     0.167 walltime=     0.167
 !timer: mqva                            cpu_time=     0.001 walltime=     0.000
 !timer: mfva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfvd                            cpu_time=     0.000 walltime=     0.000
 !timer: bvaad                           cpu_time=     0.001 walltime=     0.001
 !timer: bvaadf                          cpu_time=     0.000 walltime=     0.000
 !timer: mfad                            cpu_time=     0.001 walltime=     0.000
 !timer: mqad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqaa                            cpu_time=     0.000 walltime=     0.000
 !timer: mfaa                            cpu_time=     0.000 walltime=     0.000
 !timer: badad                           cpu_time=     0.001 walltime=     0.001
 !timer: badadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvava                           cpu_time=     0.003 walltime=     0.003
 !timer: bvavaf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvavd                           cpu_time=     0.001 walltime=     0.001
 !timer: bvavdf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvdadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdvd                           cpu_time=     0.000 walltime=     0.000
 !timer: hbcon                           cpu_time=     0.189 walltime=     0.191
 
  tol(10)=  0.000000000000000E+000  eshsci=  1.639469027795015E-003
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.007
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.007
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.008
 performing all-state projection
 Total number of micro iterations:   26

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.26186315 pnorm= 2.8991E+01 rznorm= 3.2127E-06 rpnorm= 6.1334E-06 noldr= 25 nnewr= 25 nolds= 25 nnews= 25
 *** warning *** small z0.
 
 !timer: Direct Bxr time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  cpu_time=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    cpu_time=     0.638 walltime=     0.635
 !timer: solvek total                    cpu_time=     0.774 walltime=     0.773

 fdd(*) eigenvalues. symmetry block  1
   -41.241428  -22.620302

 qvv(*) eigenvalues. symmetry block  1
     0.612127    0.672451    1.320011    1.494991    1.682914    1.699067    1.929922    2.248717    2.335274    2.424532
     2.577294    3.458063    3.486189    3.659695    4.025413    4.186789    4.315925    4.659060    4.966467    5.411836
     5.612112    6.231099
 !timer: motran                          cpu_time=     0.003 walltime=     0.002

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 cpu_time=     1.334 walltime=     1.335

 all mcscf convergence criteria are not satisfied.
 iter=    5 emc= -114.1207204553 demc= 2.9105E-03 wnorm= 1.3116E-02 knorm= 3.3270E-02 apxde= 1.1146E-02    *not converged* 

               starting mcscf iteration...   6
 !timer:                                 cpu_time=     2.746 walltime=     2.815

 orbital-state coupling will be calculated this iteration.

 number of transformed integrals put on file:     51865
 number of records written:    19
 !timer: 2-e transformation              cpu_time=     0.047 walltime=     0.048

 Size of orbital-Hessian matrix B:                    30284
 Size of the orbital-state Hessian matrix C:         237888
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:         268172

 !timer: mosrt1                          cpu_time=     0.012 walltime=     0.012
 !timer: mosrt2                          cpu_time=     0.003 walltime=     0.003
 !timer: mosort                          cpu_time=     0.015 walltime=     0.015
 !timer: hdiag(*) construction           cpu_time=     0.001 walltime=     0.001
 !timer: hmcft                           cpu_time=     0.001 walltime=     0.001

   5 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           cpu_time=     0.014 walltime=     0.014
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.011 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.011
 !timer: hmcxv                           cpu_time=     0.011 walltime=     0.011
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.011
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 ciiter=  27 noldhv=  5 noldv=  5
 !timer: hmc(*) diagonalization          cpu_time=     0.304 walltime=     0.304

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -114.2465409014     -142.8762478666        0.0000002169        0.0000010000
    2      -114.1408479565     -142.7705549218        0.0000004485        0.0000010000
    3      -113.9754152646     -142.6051222298        0.0000006061        0.0000010000
    4      -113.8796662046     -142.5093731699        0.0073172223        0.0100000000
 !timer: hmcvec                          cpu_time=     0.305 walltime=     0.305
 !timer: cadu                            cpu_time=     0.000 walltime=     0.000
 !timer: cvdu                            cpu_time=     0.000 walltime=     0.000
 !timer: rdft                            cpu_time=     0.159 walltime=     0.159
 !timer: mqva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfva                            cpu_time=     0.001 walltime=     0.000
 !timer: mfvd                            cpu_time=     0.000 walltime=     0.000
 !timer: bvaad                           cpu_time=     0.001 walltime=     0.001
 !timer: bvaadf                          cpu_time=     0.000 walltime=     0.000
 !timer: mfad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqaa                            cpu_time=     0.000 walltime=     0.000
 !timer: mfaa                            cpu_time=     0.000 walltime=     0.000
 !timer: badad                           cpu_time=     0.001 walltime=     0.001
 !timer: badadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvava                           cpu_time=     0.003 walltime=     0.003
 !timer: bvavaf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvavd                           cpu_time=     0.000 walltime=     0.001
 !timer: bvavdf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvdadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdvd                           cpu_time=     0.000 walltime=     0.000
 !timer: hbcon                           cpu_time=     0.181 walltime=     0.181
 
  tol(10)=  0.000000000000000E+000  eshsci=  1.675952331240648E-003
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.008
 performing all-state projection
 Total number of micro iterations:   18

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.25127597 pnorm= 2.5781E+01 rznorm= 3.7924E-06 rpnorm= 7.9761E-06 noldr= 18 nnewr= 18 nolds= 17 nnews= 17
 *** warning *** small z0.
 
 !timer: Direct Bxr time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  cpu_time=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    cpu_time=     0.437 walltime=     0.436
 !timer: solvek total                    cpu_time=     0.530 walltime=     0.530

 fdd(*) eigenvalues. symmetry block  1
   -41.241160  -22.620329

 qvv(*) eigenvalues. symmetry block  1
     0.610130    0.670818    1.313168    1.495040    1.682874    1.701155    1.944483    2.248206    2.335946    2.424644
     2.577399    3.458213    3.486285    3.659772    4.025475    4.188628    4.316068    4.659403    4.967417    5.411941
     5.612446    6.231250
 !timer: motran                          cpu_time=     0.003 walltime=     0.002

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 cpu_time=     1.083 walltime=     1.084

 all mcscf convergence criteria are not satisfied.
 iter=    6 emc= -114.1209347075 demc= 2.1425E-04 wnorm= 1.3408E-02 knorm= 3.7516E-02 apxde= 1.3039E-02    *not converged* 

               starting mcscf iteration...   7
 !timer:                                 cpu_time=     3.828 walltime=     3.899

 orbital-state coupling will be calculated this iteration.

 number of transformed integrals put on file:     51865
 number of records written:    19
 !timer: 2-e transformation              cpu_time=     0.048 walltime=     0.048

 Size of orbital-Hessian matrix B:                    30284
 Size of the orbital-state Hessian matrix C:         237888
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:         268172

 !timer: mosrt1                          cpu_time=     0.012 walltime=     0.012
 !timer: mosrt2                          cpu_time=     0.003 walltime=     0.003
 !timer: mosort                          cpu_time=     0.015 walltime=     0.015
 !timer: hdiag(*) construction           cpu_time=     0.000 walltime=     0.001
 !timer: hmcft                           cpu_time=     0.000 walltime=     0.001

   5 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           cpu_time=     0.014 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.011
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.011
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.011
 !timer: hmcxv                           cpu_time=     0.011 walltime=     0.011
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.011 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 ciiter=  24 noldhv= 14 noldv= 14
 !timer: hmc(*) diagonalization          cpu_time=     0.263 walltime=     0.263

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -114.2472780876     -142.8769850529        0.0000003357        0.0000010000
    2      -114.1409655050     -142.7706724703        0.0000004709        0.0000010000
    3      -113.9753800405     -142.6050870057        0.0000007946        0.0000010000
    4      -113.8763899826     -142.5060969478        0.0075079647        0.0100000000
 !timer: hmcvec                          cpu_time=     0.263 walltime=     0.264
 !timer: cadu                            cpu_time=     0.000 walltime=     0.000
 !timer: cvdu                            cpu_time=     0.000 walltime=     0.000
 !timer: rdft                            cpu_time=     0.169 walltime=     0.169
 !timer: mqva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfvd                            cpu_time=     0.000 walltime=     0.000
 !timer: bvaad                           cpu_time=     0.001 walltime=     0.001
 !timer: bvaadf                          cpu_time=     0.000 walltime=     0.000
 !timer: mfad                            cpu_time=     0.001 walltime=     0.000
 !timer: mqad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqaa                            cpu_time=     0.000 walltime=     0.000
 !timer: mfaa                            cpu_time=     0.000 walltime=     0.000
 !timer: badad                           cpu_time=     0.001 walltime=     0.001
 !timer: badadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvava                           cpu_time=     0.003 walltime=     0.004
 !timer: bvavaf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvavd                           cpu_time=     0.001 walltime=     0.001
 !timer: bvavdf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvdadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdvd                           cpu_time=     0.000 walltime=     0.000
 !timer: hbcon                           cpu_time=     0.193 walltime=     0.194
 
  tol(10)=  0.000000000000000E+000  eshsci=  1.733630600679485E-003
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 performing all-state projection
 Total number of micro iterations:   17

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0=-0.25053147 pnorm= 5.3497E+00 rznorm= 3.8021E-06 rpnorm= 8.7108E-06 noldr= 17 nnewr= 17 nolds= 16 nnews= 16
 *** warning *** small z0.
 
 !timer: Direct Bxr time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  cpu_time=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    cpu_time=     0.404 walltime=     0.405
 !timer: solvek total                    cpu_time=     0.493 walltime=     0.493

 fdd(*) eigenvalues. symmetry block  1
   -41.240910  -22.620304

 qvv(*) eigenvalues. symmetry block  1
     0.607762    0.669102    1.305444    1.495121    1.682845    1.702864    1.961228    2.247575    2.336633    2.424728
     2.577483    3.458372    3.486329    3.659861    4.025492    4.190529    4.316203    4.660053    4.968345    5.412046
     5.612732    6.231391
 !timer: motran                          cpu_time=     0.002 walltime=     0.002

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 cpu_time=     1.017 walltime=     1.018

 all mcscf convergence criteria are not satisfied.
 iter=    7 emc= -114.1212078777 demc= 2.7317E-04 wnorm= 1.3869E-02 knorm= 1.7788E-01 apxde= 1.5000E-02    *not converged* 

               starting mcscf iteration...   8
 !timer:                                 cpu_time=     4.845 walltime=     4.917

 orbital-state coupling will be calculated this iteration.

 number of transformed integrals put on file:     51865
 number of records written:    19
 !timer: 2-e transformation              cpu_time=     0.052 walltime=     0.052

 Size of orbital-Hessian matrix B:                    30284
 Size of the orbital-state Hessian matrix C:         237888
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:         268172

 !timer: mosrt1                          cpu_time=     0.015 walltime=     0.015
 !timer: mosrt2                          cpu_time=     0.003 walltime=     0.003
 !timer: mosort                          cpu_time=     0.018 walltime=     0.018
 !timer: hdiag(*) construction           cpu_time=     0.001 walltime=     0.001
 !timer: hmcft                           cpu_time=     0.001 walltime=     0.001

   5 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           cpu_time=     0.015 walltime=     0.014
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.011 walltime=     0.011
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.014 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.011 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.011 walltime=     0.011
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.011 walltime=     0.011
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 ciiter=  24 noldhv= 12 noldv= 12
 !timer: hmc(*) diagonalization          cpu_time=     0.254 walltime=     0.254

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -114.2518304358     -142.8815374011        0.0000003297        0.0000010000
    2      -114.1423887586     -142.7720957239        0.0000004625        0.0000010000
    3      -113.9751127552     -142.6048197204        0.0000006230        0.0000010000
    4      -113.8930903928     -142.5227973580        0.0072101439        0.0100000000
 !timer: hmcvec                          cpu_time=     0.255 walltime=     0.255
 !timer: cadu                            cpu_time=     0.000 walltime=     0.000
 !timer: cvdu                            cpu_time=     0.000 walltime=     0.000
 !timer: rdft                            cpu_time=     0.160 walltime=     0.160
 !timer: mqva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfva                            cpu_time=     0.001 walltime=     0.000
 !timer: mfvd                            cpu_time=     0.000 walltime=     0.000
 !timer: bvaad                           cpu_time=     0.001 walltime=     0.001
 !timer: bvaadf                          cpu_time=     0.000 walltime=     0.000
 !timer: mfad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqaa                            cpu_time=     0.000 walltime=     0.000
 !timer: mfaa                            cpu_time=     0.001 walltime=     0.000
 !timer: badad                           cpu_time=     0.000 walltime=     0.001
 !timer: badadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvava                           cpu_time=     0.004 walltime=     0.004
 !timer: bvavaf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvavd                           cpu_time=     0.001 walltime=     0.001
 !timer: bvavdf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvdadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdvd                           cpu_time=     0.000 walltime=     0.000
 !timer: hbcon                           cpu_time=     0.184 walltime=     0.184
 
  tol(10)=  0.000000000000000E+000  eshsci=  2.242074969436178E-003
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 performing all-state projection
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 Total number of micro iterations:   19

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0=-0.35246651 pnorm= 4.5225E+00 rznorm= 3.5135E-06 rpnorm= 3.7583E-06 noldr= 17 nnewr= 17 nolds= 17 nnews= 17
 *** warning *** small z0.
 
 !timer: Direct Bxr time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  cpu_time=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    cpu_time=     0.435 walltime=     0.432
 !timer: solvek total                    cpu_time=     0.524 walltime=     0.524

 fdd(*) eigenvalues. symmetry block  1
   -41.241041  -22.619379

 qvv(*) eigenvalues. symmetry block  1
     0.593817    0.662979    1.268879    1.495790    1.682701    1.705024    2.039223    2.244108    2.338818    2.424449
     2.577305    3.458935    3.485442    3.660313    4.024814    4.196884    4.316395    4.664826    4.969300    5.412286
     5.613081    6.231430
 !timer: motran                          cpu_time=     0.003 walltime=     0.002

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 cpu_time=     1.037 walltime=     1.037

 all mcscf convergence criteria are not satisfied.
 iter=    8 emc= -114.1231106499 demc= 1.9028E-03 wnorm= 1.7937E-02 knorm= 2.0205E-01 apxde= 1.8421E-02    *not converged* 

               starting mcscf iteration...   9
 !timer:                                 cpu_time=     5.882 walltime=     5.954

 orbital-state coupling will be calculated this iteration.

 number of transformed integrals put on file:     51865
 number of records written:    19
 !timer: 2-e transformation              cpu_time=     0.051 walltime=     0.051

 Size of orbital-Hessian matrix B:                    30284
 Size of the orbital-state Hessian matrix C:         237888
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:         268172

 !timer: mosrt1                          cpu_time=     0.014 walltime=     0.014
 !timer: mosrt2                          cpu_time=     0.003 walltime=     0.003
 !timer: mosort                          cpu_time=     0.017 walltime=     0.017
 !timer: hdiag(*) construction           cpu_time=     0.001 walltime=     0.001
 !timer: hmcft                           cpu_time=     0.001 walltime=     0.001

   5 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           cpu_time=     0.014 walltime=     0.014
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.011 walltime=     0.011
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.011
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.011 walltime=     0.011
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.011 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.011 walltime=     0.011
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.011
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.011
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.009
 ciiter=  24 noldhv= 12 noldv= 12
 !timer: hmc(*) diagonalization          cpu_time=     0.273 walltime=     0.273

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -114.2587042524     -142.8884112176        0.0000005142        0.0000010000
    2      -114.1459017869     -142.7756087522        0.0000004209        0.0000010000
    3      -113.9747806548     -142.6044876201        0.0000007208        0.0000010000
    4      -113.9097858086     -142.5394927738        0.0047666647        0.0100000000
 !timer: hmcvec                          cpu_time=     0.274 walltime=     0.274
 !timer: cadu                            cpu_time=     0.000 walltime=     0.000
 !timer: cvdu                            cpu_time=     0.000 walltime=     0.000
 !timer: rdft                            cpu_time=     0.154 walltime=     0.154
 !timer: mqva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfva                            cpu_time=     0.001 walltime=     0.000
 !timer: mfvd                            cpu_time=     0.000 walltime=     0.000
 !timer: bvaad                           cpu_time=     0.001 walltime=     0.001
 !timer: bvaadf                          cpu_time=     0.000 walltime=     0.000
 !timer: mfad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqaa                            cpu_time=     0.000 walltime=     0.000
 !timer: mfaa                            cpu_time=     0.000 walltime=     0.000
 !timer: badad                           cpu_time=     0.001 walltime=     0.001
 !timer: badadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvava                           cpu_time=     0.003 walltime=     0.003
 !timer: bvavaf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvavd                           cpu_time=     0.001 walltime=     0.001
 !timer: bvavdf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvdadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdvd                           cpu_time=     0.000 walltime=     0.000
 !timer: hbcon                           cpu_time=     0.176 walltime=     0.176
 
  tol(10)=  0.000000000000000E+000  eshsci=  2.726435034753535E-003
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.007
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 performing all-state projection
 Total number of micro iterations:   17

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.68371649 pnorm= 2.7936E+00 rznorm= 7.5137E-06 rpnorm= 6.1459E-06 noldr= 16 nnewr= 16 nolds= 16 nnews= 16
 *** warning *** small z0.
 
 !timer: Direct Bxr time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  cpu_time=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    cpu_time=     0.411 walltime=     0.415
 !timer: solvek total                    cpu_time=     0.502 walltime=     0.502

 fdd(*) eigenvalues. symmetry block  1
   -41.240511  -22.616180

 qvv(*) eigenvalues. symmetry block  1
     0.574670    0.659798    1.226836    1.497546    1.683033    1.701673    2.113844    2.239482    2.339288    2.423778
     2.576888    3.459878    3.481746    3.661311    4.023832    4.199240    4.316490    4.676370    4.961379    5.412735
     5.613175    6.230585
 !timer: motran                          cpu_time=     0.002 walltime=     0.002

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 cpu_time=     1.025 walltime=     1.024

 all mcscf convergence criteria are not satisfied.
 iter=    9 emc= -114.1264622314 demc= 3.3516E-03 wnorm= 2.1811E-02 knorm= 2.4594E-01 apxde= 9.9976E-03    *not converged* 

               starting mcscf iteration...  10
 !timer:                                 cpu_time=     6.907 walltime=     6.978

 orbital-state coupling will be calculated this iteration.

 number of transformed integrals put on file:     51865
 number of records written:    19
 !timer: 2-e transformation              cpu_time=     0.047 walltime=     0.047

 Size of orbital-Hessian matrix B:                    30284
 Size of the orbital-state Hessian matrix C:         237888
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:         268172

 !timer: mosrt1                          cpu_time=     0.012 walltime=     0.012
 !timer: mosrt2                          cpu_time=     0.003 walltime=     0.003
 !timer: mosort                          cpu_time=     0.015 walltime=     0.015
 !timer: hdiag(*) construction           cpu_time=     0.001 walltime=     0.001
 !timer: hmcft                           cpu_time=     0.001 walltime=     0.001

   5 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.014
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.011 walltime=     0.011
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.011 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.011
 !timer: hmcxv                           cpu_time=     0.011 walltime=     0.011
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 ciiter=  20 noldhv=  6 noldv=  6
 !timer: hmc(*) diagonalization          cpu_time=     0.226 walltime=     0.226

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -114.2666124233     -142.8963193886        0.0000005134        0.0000010000
    2      -114.1510277636     -142.7807347288        0.0000002910        0.0000010000
    3      -113.9743743735     -142.6040813387        0.0000009936        0.0000010000
    4      -113.9229616727     -142.5526686379        0.0072619372        0.0100000000
 !timer: hmcvec                          cpu_time=     0.228 walltime=     0.228
 !timer: cadu                            cpu_time=     0.000 walltime=     0.000
 !timer: cvdu                            cpu_time=     0.000 walltime=     0.000
 !timer: rdft                            cpu_time=     0.167 walltime=     0.167
 !timer: mqva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfva                            cpu_time=     0.001 walltime=     0.000
 !timer: mfvd                            cpu_time=     0.000 walltime=     0.000
 !timer: bvaad                           cpu_time=     0.001 walltime=     0.001
 !timer: bvaadf                          cpu_time=     0.000 walltime=     0.000
 !timer: mfad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqaa                            cpu_time=     0.000 walltime=     0.000
 !timer: mfaa                            cpu_time=     0.001 walltime=     0.000
 !timer: badad                           cpu_time=     0.000 walltime=     0.001
 !timer: badadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvava                           cpu_time=     0.004 walltime=     0.004
 !timer: bvavaf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvavd                           cpu_time=     0.000 walltime=     0.001
 !timer: bvavdf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvdadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdvd                           cpu_time=     0.000 walltime=     0.000
 !timer: hbcon                           cpu_time=     0.191 walltime=     0.191
 
  tol(10)=  0.000000000000000E+000  eshsci=  2.177692534348491E-003
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 Total number of micro iterations:   15

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.96905137 pnorm= 6.2267E-01 rznorm= 9.6732E-06 rpnorm= 8.1170E-06 noldr= 14 nnewr= 14 nolds= 14 nnews= 14
 
 !timer: Direct Bxr time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  cpu_time=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    cpu_time=     0.353 walltime=     0.353
 !timer: solvek total                    cpu_time=     0.429 walltime=     0.429

 fdd(*) eigenvalues. symmetry block  1
   -41.237540  -22.610586

 qvv(*) eigenvalues. symmetry block  1
     0.546875    0.659470    1.173702    1.500672    1.683792    1.693770    2.177331    2.234141    2.336511    2.423498
     2.576933    3.461579    3.471803    3.663014    4.024275    4.200160    4.317038    4.682695    4.943882    5.413823
     5.614748    6.228460
 !timer: motran                          cpu_time=     0.002 walltime=     0.002

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 cpu_time=     0.913 walltime=     0.914

 all mcscf convergence criteria are not satisfied.
 iter=   10 emc= -114.1306715201 demc= 4.2093E-03 wnorm= 1.7422E-02 knorm= 2.0956E-01 apxde= 1.7810E-03    *not converged* 

               starting mcscf iteration...  11
 !timer:                                 cpu_time=     7.820 walltime=     7.892

 orbital-state coupling will be calculated this iteration.

 number of transformed integrals put on file:     51865
 number of records written:    19
 !timer: 2-e transformation              cpu_time=     0.050 walltime=     0.051

 Size of orbital-Hessian matrix B:                    30284
 Size of the orbital-state Hessian matrix C:         237888
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:         268172

 !timer: mosrt1                          cpu_time=     0.014 walltime=     0.014
 !timer: mosrt2                          cpu_time=     0.003 walltime=     0.003
 !timer: mosort                          cpu_time=     0.018 walltime=     0.017
 !timer: hdiag(*) construction           cpu_time=     0.001 walltime=     0.001
 !timer: hmcft                           cpu_time=     0.001 walltime=     0.001

   5 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           cpu_time=     0.015 walltime=     0.015
 !timer: hmcxv                           cpu_time=     0.014 walltime=     0.014
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.011 walltime=     0.011
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 ciiter=  20 noldhv=  8 noldv=  8
 !timer: hmc(*) diagonalization          cpu_time=     0.225 walltime=     0.225

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -114.2700671013     -142.8997740665        0.0000002235        0.0000010000
    2      -114.1533375683     -142.7830445336        0.0000004698        0.0000010000
    3      -113.9735092578     -142.6032162231        0.0000006570        0.0000010000
    4      -113.9270165716     -142.5567235369        0.0080910656        0.0100000000
 !timer: hmcvec                          cpu_time=     0.226 walltime=     0.226
 !timer: cadu                            cpu_time=     0.000 walltime=     0.000
 !timer: cvdu                            cpu_time=     0.001 walltime=     0.000
 !timer: rdft                            cpu_time=     0.154 walltime=     0.155
 !timer: mqva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfvd                            cpu_time=     0.001 walltime=     0.000
 !timer: bvaad                           cpu_time=     0.001 walltime=     0.001
 !timer: bvaadf                          cpu_time=     0.000 walltime=     0.000
 !timer: mfad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqaa                            cpu_time=     0.000 walltime=     0.000
 !timer: mfaa                            cpu_time=     0.000 walltime=     0.000
 !timer: badad                           cpu_time=     0.001 walltime=     0.001
 !timer: badadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvava                           cpu_time=     0.004 walltime=     0.004
 !timer: bvavaf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvavd                           cpu_time=     0.001 walltime=     0.001
 !timer: bvavdf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvdadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdvd                           cpu_time=     0.000 walltime=     0.000
 !timer: hbcon                           cpu_time=     0.180 walltime=     0.180
 
  tol(10)=  0.000000000000000E+000  eshsci=  7.277503359494261E-004
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 Total number of micro iterations:   12

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.99989167 pnorm= 1.8193E-02 rznorm= 6.1863E-07 rpnorm= 3.5319E-06 noldr= 11 nnewr= 11 nolds= 10 nnews= 10
 
 !timer: Direct Bxr time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  cpu_time=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    cpu_time=     0.263 walltime=     0.263
 !timer: solvek total                    cpu_time=     0.321 walltime=     0.321

 fdd(*) eigenvalues. symmetry block  1
   -41.236172  -22.606379

 qvv(*) eigenvalues. symmetry block  1
     0.521693    0.660605    1.128548    1.503422    1.681246    1.687826    2.210561    2.231970    2.330817    2.423072
     2.576494    3.458333    3.462286    3.663728    4.025320    4.201027    4.317111    4.665759    4.936316    5.414271
     5.615794    6.226534
 !timer: motran                          cpu_time=     0.002 walltime=     0.002

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 cpu_time=     0.800 walltime=     0.799

 all mcscf convergence criteria are not satisfied.
 iter=   11 emc= -114.1323046425 demc= 1.6331E-03 wnorm= 5.8220E-03 knorm= 1.4716E-02 apxde= 1.1950E-05    *not converged* 

               starting mcscf iteration...  12
 !timer:                                 cpu_time=     8.620 walltime=     8.691

 orbital-state coupling will be calculated this iteration.

 number of transformed integrals put on file:     51865
 number of records written:    19
 !timer: 2-e transformation              cpu_time=     0.050 walltime=     0.050

 Size of orbital-Hessian matrix B:                    30284
 Size of the orbital-state Hessian matrix C:         237888
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:         268172

 !timer: mosrt1                          cpu_time=     0.011 walltime=     0.012
 !timer: mosrt2                          cpu_time=     0.003 walltime=     0.003
 !timer: mosort                          cpu_time=     0.014 walltime=     0.014
 !timer: hdiag(*) construction           cpu_time=     0.001 walltime=     0.001
 !timer: hmcft                           cpu_time=     0.001 walltime=     0.001

   5 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.011
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.011
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 ciiter=  18 noldhv=  7 noldv=  7
 !timer: hmc(*) diagonalization          cpu_time=     0.206 walltime=     0.206

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -114.2700903811     -142.8997973463        0.0000005859        0.0000010000
    2      -114.1533585006     -142.7830654659        0.0000003852        0.0000010000
    3      -113.9735012146     -142.6032081798        0.0000005907        0.0000010000
    4      -113.9271791793     -142.5568861445        0.0064009977        0.0100000000
 !timer: hmcvec                          cpu_time=     0.207 walltime=     0.207
 !timer: cadu                            cpu_time=     0.000 walltime=     0.000
 !timer: cvdu                            cpu_time=     0.000 walltime=     0.000
 !timer: rdft                            cpu_time=     0.158 walltime=     0.159
 !timer: mqva                            cpu_time=     0.001 walltime=     0.000
 !timer: mfva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfvd                            cpu_time=     0.000 walltime=     0.000
 !timer: bvaad                           cpu_time=     0.001 walltime=     0.001
 !timer: bvaadf                          cpu_time=     0.000 walltime=     0.000
 !timer: mfad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqaa                            cpu_time=     0.000 walltime=     0.000
 !timer: mfaa                            cpu_time=     0.000 walltime=     0.000
 !timer: badad                           cpu_time=     0.000 walltime=     0.001
 !timer: badadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvava                           cpu_time=     0.003 walltime=     0.003
 !timer: bvavaf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvavd                           cpu_time=     0.001 walltime=     0.001
 !timer: bvavdf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvdadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdvd                           cpu_time=     0.000 walltime=     0.000
 !timer: hbcon                           cpu_time=     0.183 walltime=     0.183
 
  tol(10)=  0.000000000000000E+000  eshsci=  5.549055654686501E-006
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.008
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.008
 performing all-state projection
 performing all-state projection
 performing all-state projection
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.007
 !timer: hmcxv                           cpu_time=     0.007 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.008 walltime=     0.007
 performing all-state projection
 Total number of micro iterations:    9

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.99999998 pnorm= 2.6661E-04 rznorm= 6.0437E-07 rpnorm= 4.5011E-07 noldr=  7 nnewr=  7 nolds=  7 nnews=  7
 
 !timer: Direct Bxr time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  cpu_time=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    cpu_time=     0.180 walltime=     0.180
 !timer: solvek total                    cpu_time=     0.219 walltime=     0.220

 fdd(*) eigenvalues. symmetry block  1
   -41.236940  -22.604956

 qvv(*) eigenvalues. symmetry block  1
     0.519913    0.660642    1.124009    1.503586    1.680839    1.687757    2.210885    2.232169    2.330595    2.423016
     2.576362    3.458085    3.462233    3.663722    4.025370    4.200965    4.317070    4.665341    4.936855    5.414255
     5.615814    6.226774
 !timer: motran                          cpu_time=     0.002 walltime=     0.002

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 cpu_time=     0.678 walltime=     0.678

 all mcscf convergence criteria are not satisfied.
 iter=   12 emc= -114.1323166988 demc= 1.2056E-05 wnorm= 4.4392E-05 knorm= 1.9840E-04 apxde= 1.6424E-09    *not converged* 

               starting mcscf iteration...  13
 !timer:                                 cpu_time=     9.298 walltime=     9.370

 orbital-state coupling will be calculated this iteration.

 number of transformed integrals put on file:     51865
 number of records written:    19
 !timer: 2-e transformation              cpu_time=     0.047 walltime=     0.047

 Size of orbital-Hessian matrix B:                    30284
 Size of the orbital-state Hessian matrix C:         237888
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:         268172

 !timer: mosrt1                          cpu_time=     0.012 walltime=     0.012
 !timer: mosrt2                          cpu_time=     0.004 walltime=     0.003
 !timer: mosort                          cpu_time=     0.016 walltime=     0.015
 !timer: hdiag(*) construction           cpu_time=     0.000 walltime=     0.001
 !timer: hmcft                           cpu_time=     0.000 walltime=     0.001

   5 trial vectors read from nvfile (unit= 63).
 !timer: hmcxv                           cpu_time=     0.014 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.014 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.011 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 !timer: hmcxv                           cpu_time=     0.012 walltime=     0.012
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.010
 !timer: hmcxv                           cpu_time=     0.009 walltime=     0.008
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.011
 !timer: hmcxv                           cpu_time=     0.010 walltime=     0.009
 !timer: hmcxv                           cpu_time=     0.013 walltime=     0.013
 ciiter=  19 noldhv=  8 noldv=  8
 !timer: hmc(*) diagonalization          cpu_time=     0.228 walltime=     0.227

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -114.2700908505     -142.8997978158        0.0000005480        0.0000010000
    2      -114.1533584712     -142.7830654365        0.0000003702        0.0000010000
    3      -113.9735007795     -142.6032077447        0.0000006771        0.0000010000
    4      -113.9271951167     -142.5569020820        0.0036184267        0.0100000000
 !timer: hmcvec                          cpu_time=     0.228 walltime=     0.228
 !timer: cadu                            cpu_time=     0.000 walltime=     0.000
 !timer: cvdu                            cpu_time=     0.000 walltime=     0.000
 !timer: rdft                            cpu_time=     0.169 walltime=     0.169
 !timer: mqva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfva                            cpu_time=     0.001 walltime=     0.000
 !timer: mfvd                            cpu_time=     0.000 walltime=     0.000
 !timer: bvaad                           cpu_time=     0.001 walltime=     0.001
 !timer: bvaadf                          cpu_time=     0.000 walltime=     0.000
 !timer: mfad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqaa                            cpu_time=     0.001 walltime=     0.000
 !timer: mfaa                            cpu_time=     0.000 walltime=     0.000
 !timer: badad                           cpu_time=     0.000 walltime=     0.001
 !timer: badadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvava                           cpu_time=     0.004 walltime=     0.003
 !timer: bvavaf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvavd                           cpu_time=     0.001 walltime=     0.001
 !timer: bvavdf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvdadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdvd                           cpu_time=     0.000 walltime=     0.000
 !timer: hbcon                           cpu_time=     0.193 walltime=     0.193
 
  tol(10)=  0.000000000000000E+000  eshsci=  8.081994150717572E-008
 performing all-state projection
 Total number of micro iterations:    1

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 1.00000000 pnorm= 0.0000E+00 rznorm= 7.7176E-07 rpnorm= 9.0648E-09 noldr=  1 nnewr=  1 nolds=  0 nnews=  0
 
 !timer: Direct Bxr time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  cpu_time=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: solvek total                    cpu_time=     0.005 walltime=     0.005

 fdd(*) eigenvalues. symmetry block  1
   -41.236945  -22.604905

 qvv(*) eigenvalues. symmetry block  1
     0.519892    0.660643    1.123940    1.503589    1.680837    1.687757    2.210886    2.232170    2.330593    2.423017
     2.576363    3.458086    3.462236    3.663726    4.025373    4.200966    4.317072    4.665335    4.936856    5.414258
     5.615817    6.226780
 !timer: motran                          cpu_time=     0.002 walltime=     0.002

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 cpu_time=     0.493 walltime=     0.493

 all mcscf convergence criteria are satisfied.

 final mcscf convergence values:
 iter=   13 emc= -114.1323167004 demc= 1.6430E-09 wnorm= 6.4656E-07 knorm= 3.8012E-08 apxde= 1.2162E-14    *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 weight 0.333333 total energy=   -114.270090851
   DRT #1 state # 2 weight 0.333333 total energy=   -114.153358471
   DRT #1 state # 3 weight 0.333333 total energy=   -113.973500780
   ------------------------------------------------------------



          mcscf orbitals of the final iteration,  A   block   1

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
   1O1s       0.99715585    -0.00055233     0.00139591     0.02003700    -0.21409376    -0.00009505     0.00125333     0.06156392
   2O1s       0.01906076     0.00166213    -0.00387053    -0.04053897     0.50055321     0.00034596    -0.00588800    -0.14350351
   3O1s      -0.01233059     0.00077548    -0.00213172     0.00599307     0.51970958     0.00030149    -0.00076058    -0.24297096
   4O1px     -0.00003363     0.00031206    -0.12017600    -0.00070987    -0.00778079    -0.11352523    -0.63391274    -0.04300380
   5O1py     -0.00074137     0.00148056     0.01787948     0.09834668    -0.04532451    -0.24437790     0.09914204    -0.47235339
   6O1pz     -0.00037335     0.00079872    -0.01876528     0.04822348    -0.02427454     0.49655692    -0.09594990    -0.24233575
   7O1px      0.00023959     0.00019747    -0.04683352    -0.00034126    -0.00749914    -0.07784181    -0.47022987    -0.02265050
   8O1py      0.00295699     0.00215875     0.00752227     0.04266718    -0.02932194    -0.16813483     0.07684735    -0.28386005
   9O1pz      0.00151129     0.00110477    -0.00707770     0.02088136    -0.01646091     0.34115247    -0.06979772    -0.14478401
  10O1d2-    -0.00004571    -0.00011628     0.01082250    -0.00072926     0.00052072     0.00596743     0.01545855     0.00415232
  11O1d1-    -0.00035224    -0.00088839     0.00101399    -0.00808264     0.00639850    -0.01542583     0.00095598     0.01562222
  12O1d0      0.00002878     0.00008346     0.00037125     0.00063136    -0.00062594    -0.00541831     0.00034221    -0.00086394
  13O1d1+    -0.00002466    -0.00009528     0.00585624    -0.00026556     0.00060462     0.00004119     0.00915975     0.00127942
  14O1d2+     0.00017174     0.00039634     0.00129254     0.00412309    -0.00287362    -0.00484558     0.00284393    -0.00822197
  15C1s       0.00018407     0.99939893    -0.00755031     0.17472055    -0.01052197    -0.00003591     0.00026480    -0.03444808
  16C1s      -0.00022425     0.01661241     0.02152472    -0.39241029     0.00451976     0.00029299    -0.00095770     0.08639727
  17C1s       0.00462468    -0.01507137     0.02308869    -0.33980322    -0.09317755    -0.00048257     0.00186321    -0.00384842
  18C1px      0.00005399     0.00114525    -0.44309086    -0.01714919     0.00511277    -0.05297495     0.08357957     0.01605193
  19C1py     -0.00064645     0.00354163     0.05070922     0.10246345     0.16543307    -0.11335441    -0.01616186     0.30770880
  20C1pz     -0.00030547     0.00200348    -0.07649478     0.04655567     0.08248126     0.23095852     0.01132327     0.15483859
  21C1px      0.00024182     0.00054656    -0.18659951    -0.00531002    -0.00041890    -0.03453015     0.01891295    -0.00075629
  22C1py      0.00226372     0.00240976     0.02084383     0.06619419     0.01327997    -0.07454512    -0.00004781     0.07757464
  23C1pz      0.00116782     0.00131472    -0.03261146     0.03155009     0.00647197     0.15082760     0.00443880     0.03786801
  24C1d2-    -0.00006450     0.00006890     0.02342966     0.00635221     0.00456824    -0.00485417    -0.03306055     0.00433305
  25C1d1-    -0.00013278     0.00048576     0.00243708    -0.00905424     0.01529028     0.01389577    -0.00301216     0.01664130
  26C1d0     -0.00001269    -0.00001569     0.00073973     0.00470174     0.00014668     0.00468821    -0.00135264     0.00016479
  27C1d1+     0.00003143    -0.00003675     0.01278307    -0.00775999    -0.00150703    -0.00086040    -0.01711451    -0.00210946
  28C1d2+     0.00012907    -0.00031581     0.00280734    -0.00653123    -0.01134908     0.00440200    -0.00323938    -0.01261486
  29H1s      -0.00005925    -0.00434391     0.24647066    -0.16883597    -0.04884146     0.00012646    -0.09323290    -0.07296651
  30H1s       0.00017686     0.00093306     0.16647323    -0.05398924    -0.02807096     0.00007064    -0.11543052    -0.06927088
  31H2s       0.00002189    -0.00336201    -0.22572184    -0.19303171    -0.05434994    -0.00007496     0.09678402    -0.07400021
  32H2s       0.00019651     0.00151276    -0.16544505    -0.07514438    -0.03446114    -0.00012693     0.12425243    -0.06769198

               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
   1O1s       0.00002349    -0.07506949    -0.01773653     0.00195919     0.04914953    -0.00002957     0.02703485     0.01689064
   2O1s      -0.00019333     0.28717670     0.02450159    -0.00268070     0.02887989     0.00030033    -0.15969218    -0.09254207
   3O1s       0.00036449     0.33348166     0.33608060    -0.04100205    -1.50900780    -0.00019633    -0.22019582    -0.15961331
   4O1px      0.07577470    -0.04844306    -0.00770477    -0.09166403    -0.00488848     0.01218349     0.17328161    -0.30383252
   5O1py      0.16268444    -0.63877725    -0.00641503     0.01378191    -0.14841118     0.02739942    -0.09037058     0.00619638
   6O1pz     -0.33109836    -0.32518181    -0.00500943    -0.01416907    -0.07352962    -0.05550434    -0.00455934    -0.06646890
   7O1px      0.06650522    -0.00732996    -0.02132150    -0.29835967     0.04479167     0.03321782     0.07985485    -0.11803952
   8O1py      0.14301229    -0.12138702    -0.09430832     0.05983682     0.70357782     0.07188558     0.21946683     0.16806858
   9O1pz     -0.29117035    -0.06139869    -0.05122359    -0.03882096     0.35586626    -0.14647321     0.12517756     0.05567829
  10O1d2-    -0.00035600    -0.00293767     0.00185562    -0.00623916    -0.01274949    -0.01349884     0.00267607    -0.00751434
  11O1d1-    -0.00002108    -0.03287813     0.00237004    -0.00079078    -0.08723354     0.03886423    -0.04882733    -0.02930650
  12O1d0      0.00011292     0.00374141     0.00083633    -0.00033703     0.00677818     0.01307365     0.00711308     0.00385999
  13O1d1+     0.00061594    -0.00463657    -0.00195693    -0.00291102    -0.00589318    -0.00253701    -0.00670728    -0.00901847
  14O1d2+    -0.00005582     0.01289820    -0.00410961    -0.00018257     0.04321207     0.01229287     0.01638772     0.00821444
  15C1s      -0.00002778     0.11584699    -0.10489772     0.00504383    -0.02977780     0.00007632    -0.03358762    -0.01979035
  16C1s       0.00035061    -0.44667556     0.12179961    -0.00919724    -0.26808102     0.00282185    -0.97412587    -0.54457328
  17C1s      -0.00052061    -0.38301155     1.69645772    -0.08425156     1.51161643    -0.00540117     1.97895983     1.12312644
  18C1px     -0.11809287    -0.03541577     0.01068897     0.39759716    -0.02851895     0.20480715     0.33648698    -0.59521715
  19C1py     -0.25365430    -0.57780233    -0.21136015    -0.04250782    -0.46941593     0.44225170    -0.02087644     0.10528581
  20C1pz      0.51576172    -0.29235619    -0.10180843     0.07011005    -0.23648204    -0.90021851     0.06360796    -0.08429204
  21C1px     -0.10081709    -0.01056502     0.02968506     1.58496061     0.11964177    -0.23567521    -1.04108691     1.83688494
  22C1py     -0.21742149    -0.19353204    -0.89160453    -0.15191076     1.65474368    -0.51231862    -0.04824858    -0.33540113
  23C1pz      0.44162514    -0.09752429    -0.43140174     0.28776688     0.84035816     1.04617810    -0.25781988     0.25514195
  24C1d2-     0.00346175    -0.01025580     0.00106701    -0.00066520     0.00002460     0.00572964    -0.00447449     0.08447348
  25C1d1-    -0.00995628     0.02099200     0.00287037     0.00013645     0.05810758    -0.01645908    -0.03009528    -0.00921248
  26C1d0     -0.00337116    -0.00981325     0.00044144    -0.00012268    -0.00983833    -0.00562710     0.02182703     0.01628846
  27C1d1+     0.00071787     0.01762606    -0.00141104    -0.00013775     0.01464446     0.00127589    -0.06185211     0.00894044
  28C1d2+    -0.00315980     0.01293950    -0.00344148     0.00010403    -0.01354894    -0.00511771    -0.05050429    -0.02107367
  29H1s       0.00018034     0.08220858    -0.04664622    -0.01400397     0.13160752     0.00206478    -0.71920896     0.06264011
  30H1s       0.00015630     0.09618816    -1.34787665     1.89367127    -0.14470201     0.00229675    -0.58834425     0.58959832
  31H2s       0.00003065     0.07915378    -0.04359657     0.01975881     0.13000453     0.00087563    -0.32049311    -0.65357188
  32H2s      -0.00011007     0.09558048    -1.52231724    -1.72671575    -0.16913115    -0.00033281     0.19374053    -0.79040440

               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
   1O1s       0.06458863     0.01240836     0.02673359     0.00017791    -0.00181210     0.04223972    -0.00019815    -0.00002911
   2O1s      -0.37944409    -0.07746698    -0.20514647    -0.00039824    -0.00326255    -1.57441799     0.00724976     0.00114088
   3O1s      -0.09523737     0.01143706     0.21310122    -0.00193519     0.03053773     3.01606498    -0.01374223    -0.00211465
   4O1px      0.01676789    -0.14817143     0.01751998     0.18769721     0.84401564     0.00282246     0.00839427    -0.00007291
   5O1py     -0.51846271    -0.07691474    -0.28235491     0.40700830    -0.09764273     0.17198019     0.01763861    -0.00006582
   6O1pz     -0.24895495    -0.07113914    -0.13249503    -0.83213794     0.14214185     0.08456795    -0.03759490    -0.00021792
   7O1px      0.00438317     0.23340389    -0.02599926    -0.23498028    -1.33886345    -0.05521737     0.03141609    -0.00027761
   8O1py      1.13440179     0.17024622     0.42024475    -0.50957323     0.13245978    -1.09928106     0.07120698    -0.00044396
   9O1pz      0.55618217     0.13638625     0.19783605     1.04671168    -0.23735061    -0.55322992    -0.13284398     0.00281908
  10O1d2-     0.01925690    -0.11708045     0.00888338    -0.00553530    -0.12732685    -0.03339557     0.11804138    -0.19276614
  11O1d1-     0.03209878    -0.00068278     0.04251918     0.01919106    -0.01338991    -0.15101973    -0.34350563    -0.09113167
  12O1d0     -0.00113000    -0.00561134    -0.00558702     0.00615789    -0.00535992    -0.00033530    -0.11559643     0.03364025
  13O1d1+     0.00757722    -0.06084480     0.01153229    -0.00238944    -0.06471561     0.01782982     0.02587179     0.40376681
  14O1d2+    -0.01626472    -0.01690315    -0.01284427     0.00609314    -0.01058917     0.10836920    -0.10998440    -0.06230517
  15C1s       0.01862801    -0.00011228    -0.03257008    -0.00006338     0.00090969     0.02079167    -0.00010232    -0.00000834
  16C1s      -0.24458531     0.08945899     1.51309253     0.00231741    -0.02162410     0.15996814    -0.00076080    -0.00003748
  17C1s       1.53825479     0.03003912    -2.44804268     0.00000841    -0.03432596    -1.80644425     0.00813149     0.00112000
  18C1px     -0.09909758     0.87673929    -0.09431242    -0.00354058    -0.20601072    -0.00497190     0.03266737    -0.00020071
  19C1py      0.60744107    -0.05736460    -0.30296869    -0.00886969     0.01665653    -0.18312743     0.07105146    -0.00037018
  20C1pz      0.27594009     0.17228444    -0.17045712     0.01917319    -0.03888924    -0.09197261    -0.14231900     0.00111289
  21C1px      0.22509058    -1.41150020     0.18147762     0.07360279     1.32315277    -0.09548291    -0.02016663     0.00021181
  22C1py     -0.28334563     0.19886113     0.54073653     0.16901431    -0.19738573    -1.00157591    -0.03906210     0.00121301
  23C1pz     -0.08703683    -0.22482931     0.30807156    -0.33862651     0.20437589    -0.51406729     0.09094426    -0.00076743
  24C1d2-    -0.02826848     0.18388138     0.01802993    -0.00420801     0.13079884    -0.05500586    -0.13815444    -0.32931030
  25C1d1-     0.09737378     0.03419709     0.04379567     0.01180890     0.00629887    -0.02643240     0.38895366    -0.16456137
  26C1d0     -0.01978016     0.00575880     0.01201672     0.00383924     0.00555389    -0.02275085     0.13225794     0.05542170
  27C1d1+     0.01567450     0.09598933    -0.03653520    -0.00110406     0.06882288     0.04335833    -0.02053205     0.69562135
  28C1d2+    -0.02044747     0.01014249    -0.06975887     0.00337943     0.01698548     0.08476639     0.12260694    -0.10957353
  29H1s       0.18445053     0.65898601    -0.54657557    -0.00131815     0.20473091     0.05705045    -0.00037070    -0.00004467
  30H1s      -0.42191515    -1.37573556     1.31864488    -0.00039152     0.51954752    -0.00123876    -0.00009597    -0.00002427
  31H2s       0.37483132    -0.65912919    -0.44169313    -0.00013765    -0.18943178     0.05359491    -0.00014427    -0.00001289
  32H2s      -0.84619121     1.37806069     1.05419813     0.00284298    -0.52035764     0.03482090    -0.00005582     0.00003865

               MO   25        MO   26        MO   27        MO   28        MO   29        MO   30        MO   31        MO   32
   1O1s       0.00129471    -0.00040476    -0.00000804     0.03018372     0.01769392    -0.00000631    -0.00019000     0.04553974
   2O1s      -0.20058511    -0.02065129     0.00052276    -0.16187248    -0.28132041     0.00015008    -0.00231648     0.77994900
   3O1s       0.46674885     0.07080669    -0.00098778    -0.39432666     0.18418455    -0.00022646     0.00976359    -2.42083268
   4O1px      0.00488771    -0.22332657     0.00002886     0.01651465     0.00848952    -0.01507883     0.01943998     0.00378575
   5O1py      0.00254623     0.02734662     0.00014630     0.29938822     0.17986163    -0.03238335    -0.00279434     0.05119886
   6O1pz      0.00230439    -0.03761753    -0.00046131     0.15095250     0.09035402     0.06579216     0.00300704     0.02606394
   7O1px     -0.02053318     0.43690767    -0.00029635     0.00366775    -0.00403383     0.07671292    -0.26744117     0.08863651
   8O1py     -0.16957026    -0.08392834    -0.00091149    -0.05551507    -0.19848135     0.16521828     0.03577398     1.32889745
   9O1pz     -0.08782190     0.05868878     0.00268149    -0.02644488    -0.09855448    -0.33566315    -0.04331679     0.67365905
  10O1d2-     0.21261839    -0.63735315    -0.34469393     0.06862657    -0.07124749    -0.18763653     0.68165204    -0.11480071
  11O1d1-    -0.36221778    -0.06335654    -0.16888995     0.10289591     0.17768134     0.53679331     0.05902940    -0.88864693
  12O1d0      0.17241412    -0.02242354     0.05878423     0.03182441    -0.07011093     0.18151807     0.02743141     0.07290958
  13O1d1+    -0.28920491    -0.33992023     0.72609158    -0.07919456     0.11961579    -0.03327509     0.35491787    -0.06174376
  14O1d2+    -0.22145112    -0.07101649    -0.11340643    -0.16792146     0.07048397     0.17018854     0.07066281     0.43274798
  15C1s       0.02278455     0.00022483    -0.00000412    -0.02366660     0.03369055    -0.00000277    -0.00112542    -0.04105532
  16C1s       0.19748217     0.01880182     0.00001438    -0.45930645    -0.09245653     0.00001409    -0.00181568     0.64800365
  17C1s      -0.79542843    -0.04773389     0.00048815     0.78902211    -0.44254049     0.00015929     0.00671348     1.38933591
  18C1px      0.00630248    -0.22352677     0.00002931    -0.01817353    -0.00460736    -0.02455501    -0.02876722     0.05636784
  19C1py     -0.01182501     0.03022172     0.00013129    -0.16968415    -0.05103870    -0.05267611    -0.00411885     0.85811792
  20C1pz     -0.00432564    -0.03629356    -0.00001475    -0.08767785    -0.02613398     0.10722987    -0.00869848     0.43479073
  21C1px      0.00203397    -0.96351622     0.00005682     0.01458329    -0.00777969    -0.04824425     0.12904736     0.05706976
  22C1py     -0.02777707     0.09587645     0.00069355     0.33113665     0.27000763    -0.10365974    -0.02875484     0.70570792
  23C1pz     -0.01328750    -0.17322943    -0.00066022     0.16621755     0.13100742     0.21088982     0.01523385     0.36005431
  24C1d2-     0.07467496     0.43826172     0.21624173    -0.02753983     0.26308932    -0.17830471     0.89095278     0.07544065
  25C1d1-    -0.17821335     0.02004456     0.10455635     0.73736400     0.02865840     0.51275309     0.07893245     0.62714338
  26C1d0      0.07412780     0.01974908    -0.03717095    -0.13237854     0.14022610     0.17318043     0.03253169    -0.05791558
  27C1d1+    -0.12960653     0.22666231    -0.45448529     0.18957017    -0.26169677    -0.03296828     0.47385659     0.05964997
  28C1d2+    -0.08248616     0.05298562     0.07059383    -0.15608206    -0.41139124     0.16274563     0.10235509    -0.28547229
  29H1s       0.22873669    -0.61192362     0.00004088    -0.14324485     0.52886515    -0.00033653    -0.42027592    -0.02532779
  30H1s       0.13100405    -0.23436966    -0.00007626    -0.05634885    -0.01914663     0.00011964     0.22752574    -0.02827182
  31H2s       0.21292151     0.59884606     0.00003270    -0.12100661     0.56893201     0.00021205     0.37897372    -0.01697190
  32H2s       0.13469685     0.23432730     0.00004926    -0.06171792    -0.01586896    -0.00009946    -0.22157881    -0.04611755

          natural orbitals of the final iteration, block  1

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8

  occ(*)=     2.00000000     2.00000000     1.99806195     1.99507479     1.99248474     1.91802779     1.66793668     1.65175076
 
   1O1s       0.99715585    -0.00055228     0.00139968     0.02004722    -0.21409273    -0.00009504     0.00125082     0.06156405
   2O1s       0.01906076     0.00166213    -0.00387900    -0.04056283     0.50055117     0.00034595    -0.00588225    -0.14350351
   3O1s      -0.01233059     0.00077548    -0.00213778     0.00596761     0.51970952     0.00030150    -0.00075098    -0.24297091
   4O1px     -0.00003363     0.00031206    -0.12017560    -0.00070174    -0.00778161    -0.11352531    -0.63391112    -0.04302814
   5O1py     -0.00074137     0.00148056     0.01788474     0.09834478    -0.04532146    -0.24437803     0.09916101    -0.47235266
   6O1pz     -0.00037335     0.00079872    -0.01876254     0.04822435    -0.02427321     0.49655726    -0.09594021    -0.24234090
   7O1px      0.00023959     0.00019747    -0.04683318    -0.00033781    -0.00749925    -0.07784189    -0.47022902    -0.02266858
   8O1py      0.00295699     0.00215876     0.00752428     0.04266634    -0.02932072    -0.16813495     0.07685848    -0.28385778
   9O1pz      0.00151129     0.00110477    -0.00707659     0.02088171    -0.01646033     0.34115276    -0.06979207    -0.14478700
  10O1d2-    -0.00004571    -0.00011628     0.01082245    -0.00072997     0.00052080     0.00596743     0.01545840     0.00415287
  11O1d1-    -0.00035224    -0.00088839     0.00101347    -0.00808290     0.00639814    -0.01542583     0.00095541     0.01562215
  12O1d0      0.00002878     0.00008346     0.00037129     0.00063137    -0.00062591    -0.00541831     0.00034224    -0.00086392
  13O1d1+    -0.00002466    -0.00009528     0.00585621    -0.00026596     0.00060466     0.00004118     0.00915971     0.00127973
  14O1d2+     0.00017174     0.00039634     0.00129280     0.00412309    -0.00287343    -0.00484558     0.00284424    -0.00822183
  15C1s       0.00018402     0.99939893    -0.00753916     0.17472132    -0.01051392    -0.00003589     0.00026608    -0.03444860
  16C1s      -0.00022425     0.01661241     0.02149999    -0.39241137     0.00450161     0.00029294    -0.00096077     0.08639771
  17C1s       0.00462468    -0.01507137     0.02306802    -0.33980040    -0.09319340    -0.00048261     0.00186351    -0.00384771
  18C1px      0.00005399     0.00114525    -0.44309202    -0.01712095     0.00510724    -0.05297479     0.08357863     0.01605667
  19C1py     -0.00064645     0.00354163     0.05071535     0.10245437     0.16543865    -0.11335418    -0.01617296     0.30770468
  20C1pz     -0.00030547     0.00200348    -0.07649199     0.04655762     0.08248276     0.23095803     0.01131760     0.15483767
  21C1px      0.00024182     0.00054656    -0.18659986    -0.00529804    -0.00042114    -0.03453004     0.01891284    -0.00075491
  22C1py      0.00226372     0.00240976     0.02084828     0.06619271     0.01328330    -0.07454491    -0.00005056     0.07757336
  23C1pz      0.00116782     0.00131472    -0.03260932     0.03155208     0.00647311     0.15082718     0.00443742     0.03786770
  24C1d2-    -0.00006450     0.00006890     0.02343007     0.00635053     0.00456881    -0.00485418    -0.03306068     0.00433160
  25C1d1-    -0.00013278     0.00048576     0.00243639    -0.00905501     0.01528994     0.01389578    -0.00301281     0.01664127
  26C1d0     -0.00001269    -0.00001569     0.00074003     0.00470168     0.00014690     0.00468821    -0.00135264     0.00016467
  27C1d1+     0.00003143    -0.00003675     0.01278259    -0.00776074    -0.00150723    -0.00086040    -0.01711444    -0.00211005
  28C1d2+     0.00012907    -0.00031581     0.00280700    -0.00653096    -0.01134937     0.00440200    -0.00323892    -0.01261488
  29H1s      -0.00005925    -0.00434391     0.24646013    -0.16884989    -0.04884673     0.00012642    -0.09323009    -0.07296954
  30H1s       0.00017686     0.00093306     0.16646987    -0.05399896    -0.02807170     0.00007062    -0.11542786    -0.06927517
  31H2s       0.00002189    -0.00336201    -0.22573401    -0.19301520    -0.05436149    -0.00007495     0.09678649    -0.07399407
  32H2s       0.00019651     0.00151276    -0.16544988    -0.07513263    -0.03446657    -0.00012691     0.12425476    -0.06768566

               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16

  occ(*)=     0.74929398     0.02736930     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
 
   1O1s       0.00002348    -0.07506957    -0.01772929     0.00196139     0.04915380    -0.00002957     0.02704214     0.01687749
   2O1s      -0.00019331     0.28717688     0.02451579    -0.00267733     0.02887468     0.00030032    -0.15973603    -0.09246797
   3O1s       0.00036450     0.33348223     0.33585029    -0.04106914    -1.50907787    -0.00019632    -0.22023943    -0.15948151
   4O1px      0.07577460    -0.04844356    -0.00770902    -0.09166430    -0.00489450     0.01218351     0.17313890    -0.30391433
   5O1py      0.16268416    -0.63877523    -0.00645699     0.01377103    -0.14843152     0.02739949    -0.09037276     0.00623251
   6O1pz     -0.33109790    -0.32518091    -0.00503097    -0.01417446    -0.07354102    -0.05550446    -0.00459309    -0.06646997
   7O1px      0.06650515    -0.00733035    -0.02131507    -0.29835731     0.04481596     0.03321780     0.07980278    -0.11807446
   8O1py      0.14301211    -0.12138580    -0.09417087     0.05987456     0.70363350     0.07188551     0.21954486     0.16796828
   9O1pz     -0.29117002    -0.06139815    -0.05115458    -0.03880188     0.35589921    -0.14647313     0.12520405     0.05562146
  10O1d2-    -0.00035599    -0.00293767     0.00185460    -0.00623931    -0.01275013    -0.01349885     0.00267300    -0.00751518
  11O1d1-    -0.00002110    -0.03287820     0.00236220    -0.00079300    -0.08723483     0.03886425    -0.04883760    -0.02928017
  12O1d0      0.00011291     0.00374141     0.00083718    -0.00033682     0.00677859     0.01307366     0.00711467     0.00385645
  13O1d1+     0.00061594    -0.00463657    -0.00195812    -0.00291119    -0.00589451    -0.00253701    -0.00671146    -0.00901533
  14O1d2+    -0.00005583     0.01289824    -0.00410650    -0.00018156     0.04321154     0.01229288     0.01638966     0.00820483
  15C1s      -0.00002777     0.11584713    -0.10490214     0.00504397    -0.02976610     0.00007632    -0.03359496    -0.01977327
  16C1s       0.00035057    -0.44667590     0.12175540    -0.00921050    -0.26811470     0.00282184    -0.97436479    -0.54410844
  17C1s      -0.00052064    -0.38301145     1.69671937    -0.08420294     1.51147858    -0.00540122     1.97942633     1.12216585
  18C1px     -0.11809292    -0.03541591     0.01069075     0.39759485    -0.02853968     0.20480713     0.33620989    -0.59537162
  19C1py     -0.25365445    -0.57780367    -0.21140866    -0.04251867    -0.46937728     0.44225171    -0.02080716     0.10531582
  20C1pz      0.51576193    -0.29235692    -0.10183182     0.07010420    -0.23646784    -0.90021851     0.06357865    -0.08431117
  21C1px     -0.10081712    -0.01056506     0.02972016     1.58496673     0.11958883    -0.23567520    -1.04024133     1.83736015
  22C1py     -0.21742157    -0.19353234    -0.89139152    -0.15183981     1.65486459    -0.51231862    -0.04843745    -0.33541535
  23C1pz      0.44162528    -0.09752449    -0.43128900     0.28780316     0.84040559     1.04617809    -0.25771936     0.25524176
  24C1d2-     0.00346175    -0.01025584     0.00106520    -0.00066604     0.00002783     0.00572961    -0.00443526     0.08447523
  25C1d1-    -0.00995626     0.02099192     0.00286996     0.00013667     0.05810171    -0.01645894    -0.03010195    -0.00920093
  26C1d0     -0.00337115    -0.00981325     0.00044024    -0.00012308    -0.00983693    -0.00562705     0.02183480     0.01627843
  27C1d1+     0.00071787     0.01762605    -0.00140844    -0.00013715     0.01464391     0.00127585    -0.06184793     0.00896927
  28C1d2+    -0.00315980     0.01293955    -0.00343767     0.00010493    -0.01354792    -0.00511767    -0.05051283    -0.02104873
  29H1s       0.00018035     0.08220891    -0.04664080    -0.01400183     0.13158671     0.00206479    -0.71918577     0.06297149
  30H1s       0.00015631     0.09618841    -1.34788059     1.89368292    -0.14459138     0.00229676    -0.58805428     0.58987792
  31H2s       0.00003065     0.07915413    -0.04358976     0.01976269     0.12997972     0.00087561    -0.32080094    -0.65342739
  32H2s      -0.00011006     0.09558084    -1.52237038    -1.72670677    -0.16887127    -0.00033276     0.19339978    -0.79047656

               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24

  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
 
   1O1s       0.06459014     0.01240434     0.02672783     0.00017787    -0.00181034     0.04224034    -0.00019792    -0.00002911
   2O1s      -0.37946007    -0.07744420    -0.20510870    -0.00039795    -0.00327754    -1.57441978     0.00724118     0.00114082
   3O1s      -0.09519952     0.01145205     0.21311489    -0.00193521     0.03054080     3.01605379    -0.01372568    -0.00211450
   4O1px      0.01675113    -0.14817327     0.01751563     0.18769709     0.84401575     0.00281900     0.00839436    -0.00007291
   5O1py     -0.51848086    -0.07688641    -0.28232042     0.40700857    -0.09765046     0.17197808     0.01763968    -0.00006581
   6O1pz     -0.24896777    -0.07112578    -0.13247970    -0.83213782     0.14213798     0.08456636    -0.03759467    -0.00021791
   7O1px      0.00441616     0.23340627    -0.02599320    -0.23498012    -1.33886347    -0.05521518     0.03141566    -0.00027762
   8O1py      1.13442234     0.17017813     0.42016303    -0.50957384     0.13247738    -1.09927269     0.07120066    -0.00044403
   9O1pz      0.55619990     0.13635351     0.19779804     1.04671155    -0.23734184    -0.55322432    -0.13284653     0.00281907
  10O1d2-     0.01925052    -0.11708139     0.00888550    -0.00553530    -0.12732673    -0.03339219     0.11804124    -0.19276667
  11O1d1-     0.03210242    -0.00068336     0.04252016     0.01919106    -0.01339066    -0.15101463    -0.34350684    -0.09113184
  12O1d0     -0.00113062    -0.00561140    -0.00558695     0.00615789    -0.00535991    -0.00033546    -0.11559653     0.03364037
  13O1d1+     0.00757388    -0.06084509     0.01153324    -0.00238942    -0.06471535     0.01783390     0.02587201     0.40376785
  14O1d2+    -0.01626754    -0.01690278    -0.01284460     0.00609314    -0.01058856     0.10837112    -0.10998394    -0.06230530
  15C1s       0.01862954    -0.00011345    -0.03256990    -0.00006336     0.00090927     0.02078944    -0.00010220    -0.00000833
  16C1s      -0.24446440     0.08950884     1.51311952     0.00231686    -0.02161936     0.15996916    -0.00075992    -0.00003747
  17C1s       1.53800194     0.02987276    -2.44818420     0.00000875    -0.03431434    -1.80640815     0.00812141     0.00111988
  18C1px     -0.09904376     0.87674615    -0.09432127    -0.00354058    -0.20601084    -0.00496896     0.03266731    -0.00020071
  19C1py      0.60743117    -0.05740638    -0.30299577    -0.00886969     0.01665718    -0.18313821     0.07105047    -0.00037018
  20C1pz      0.27594755     0.17226548    -0.17047244     0.01917340    -0.03888895    -0.09197623    -0.14231941     0.00111288
  21C1px      0.22496861    -1.41151930     0.18147336     0.07360253     1.32315614    -0.09547166    -0.02016712     0.00021179
  22C1py     -0.28329944     0.19888674     0.54073645     0.16901415    -0.19737882    -1.00157044    -0.03906743     0.00121293
  23C1pz     -0.08704208    -0.22482116     0.30807028    -0.33862692     0.20438002    -0.51406266     0.09094104    -0.00076744
  24C1d2-    -0.02825605     0.18388271     0.01802656    -0.00420798     0.13079909    -0.05500992    -0.13815465    -0.32930996
  25C1d1-     0.09737664     0.03419059     0.04378441     0.01180865     0.00630298    -0.02643073     0.38895314    -0.16456130
  26C1d0     -0.01977895     0.00576027     0.01201789     0.00383918     0.00555363    -0.02275243     0.13225772     0.05542163
  27C1d1+     0.01567834     0.09598743    -0.03653804    -0.00110406     0.06882273     0.04335677    -0.02053167     0.69562071
  28C1d2+    -0.02044995     0.01014316    -0.06975482     0.00337944     0.01698312     0.08476403     0.12260730    -0.10957344
  29H1s       0.18446051     0.65896321    -0.54660157    -0.00131802     0.20473189     0.05704874    -0.00037039    -0.00004467
  30H1s      -0.42190642    -1.37567876     1.31870304    -0.00039190     0.51954600    -0.00124594    -0.00009593    -0.00002426
  31H2s       0.37477657    -0.65915920    -0.44170048    -0.00013744    -0.18943172     0.05358256    -0.00014392    -0.00001289
  32H2s      -0.84597371     1.37814504     1.05424673     0.00284289    -0.52036261     0.03479954    -0.00005552     0.00003867

               MO   25        MO   26        MO   27        MO   28        MO   29        MO   30        MO   31        MO   32

  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
 
   1O1s       0.00129445    -0.00040558    -0.00000804     0.03018459     0.01769344    -0.00000631    -0.00018999     0.04553985
   2O1s      -0.20059240    -0.02065974     0.00052275    -0.16187665    -0.28131944     0.00015006    -0.00232093     0.77994675
   3O1s       0.46677321     0.07084827    -0.00098776    -0.39433937     0.18419407    -0.00022644     0.00977268    -2.42083001
   4O1px      0.00488833    -0.22332631     0.00002885     0.01650875     0.00848326    -0.01507881     0.01944002     0.00378440
   5O1py      0.00254586     0.02734402     0.00014629     0.29938917     0.17985167    -0.03238328    -0.00279226     0.05119900
   6O1pz      0.00230435    -0.03761874    -0.00046130     0.15095161     0.09034767     0.06579203     0.00300807     0.02606370
   7O1px     -0.02053567     0.43690575    -0.00029636     0.00367748    -0.00402526     0.07671291    -0.26744149     0.08863739
   8O1py     -0.16958001    -0.08394464    -0.00091151    -0.05550847    -0.19847712     0.16521822     0.03576777     1.32889691
   9O1pz     -0.08782726     0.05868034     0.00268150    -0.02643940    -0.09855042    -0.33566308    -0.04331992     0.67365899
  10O1d2-     0.21262154    -0.63735265    -0.34469364     0.06861516    -0.07125790    -0.18763648     0.68165183    -0.11480049
  11O1d1-    -0.36221874    -0.06336111    -0.16888985     0.10289753     0.17767704     0.53679307     0.05903206    -0.88864726
  12O1d0      0.17241441    -0.02242252     0.05878417     0.03182311    -0.07011116     0.18151799     0.02743104     0.07290967
  13O1d1+    -0.28920345    -0.33992181     0.72609100    -0.07919860     0.11961071    -0.03327501     0.35491816    -0.06174379
  14O1d2+    -0.22145037    -0.07101610    -0.11340635    -0.16792192     0.07048497     0.17018846     0.07066186     0.43274802
  15C1s       0.02278446     0.00022537    -0.00000412    -0.02366729     0.03368954    -0.00000277    -0.00112517    -0.04105581
  16C1s       0.19748594     0.01881164     0.00001439    -0.45930677    -0.09244297     0.00001409    -0.00181802     0.64800501
  17C1s      -0.79544431    -0.04777355     0.00048812     0.78904363    -0.44253709     0.00015928     0.00670612     1.38934084
  18C1px      0.00630382    -0.22352669     0.00002931    -0.01817587    -0.00460823    -0.02455502    -0.02876733     0.05636772
  19C1py     -0.01182726     0.03022199     0.00013128    -0.16969040    -0.05104311    -0.05267614    -0.00412125     0.85811544
  20C1pz     -0.00432644    -0.03629340    -0.00001473    -0.08768146    -0.02613635     0.10722990    -0.00869969     0.43478948
  21C1px      0.00203854    -0.96351681     0.00005684     0.01457309    -0.00778708    -0.04824422     0.12904679     0.05706989
  22C1py     -0.02779074     0.09585317     0.00069356     0.33115367     0.27000163    -0.10365967    -0.02875643     0.70570676
  23C1pz     -0.01329318    -0.17324102    -0.00066027     0.16622359     0.13100277     0.21088968     0.01523293     0.36005377
  24C1d2-     0.07467080     0.43825861     0.21624222    -0.02752827     0.26309169    -0.17830477     0.89095392     0.07544402
  25C1d1-    -0.17821819     0.02003057     0.10455650     0.73736500     0.02863776     0.51275336     0.07893287     0.62714340
  26C1d0      0.07412744     0.01974971    -0.03717105    -0.13237435     0.14023022     0.17318051     0.03253213    -0.05791503
  27C1d1+    -0.12960690     0.22666203    -0.45448629     0.18956477    -0.26170227    -0.03296835     0.47385590     0.05965055
  28C1d2+    -0.08248236     0.05299375     0.07059397    -0.15609333    -0.41138777     0.16274571     0.10235364    -0.28547309
  29H1s       0.22873663    -0.61192577     0.00004088    -0.14324143     0.52886097    -0.00033652    -0.42027379    -0.02532962
  30H1s       0.13100588    -0.23436526    -0.00007626    -0.05636112    -0.01915694     0.00011964     0.22752592    -0.02827457
  31H2s       0.21291474     0.59884185     0.00003269    -0.12098683     0.56893708     0.00021206     0.37897591    -0.01696983
  32H2s       0.13469509     0.23433240     0.00004927    -0.06173156    -0.01588309    -0.00009946    -0.22157792    -0.04612466
 d1(*), fmc(*), and qmc(*) written to the 1-particle density matrix file.
        814 d2(*) elements written to the 2-particle density matrix file.
 !timer: writing the mc density files reqcpu_time=     0.002 walltime=     0.001


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        A   partial gross atomic populations
   ao class       1A         2A         3A         4A         5A         6A  
    O1_ s       1.999225   0.000095   0.000018   0.006077   1.773548   0.000000
    O1_ p       0.000003  -0.000595   0.083953   0.090326   0.014219   1.471481
    O1_ d       0.000000  -0.000124   0.003405   0.002593   0.000924   0.005539
    C1_ s       0.000381   2.000525   0.004025   1.262508  -0.038243   0.000000
    C1_ p       0.000399   0.000090   1.065541   0.077557   0.198701   0.433106
    C1_ d      -0.000016   0.000002   0.007507   0.005658   0.012990   0.007902
    H1_ s       0.000003  -0.000069   0.454737   0.234956   0.013374   0.000000
    H2_ s       0.000004   0.000076   0.378875   0.315400   0.016973   0.000000
 
   ao class       7A         8A         9A        10A        11A        12A  
    O1_ s       0.000047   0.126957   0.000000   0.000984   0.000000   0.000000
    O1_ p       1.552293   1.064608   0.166216   0.009867   0.000000   0.000000
    O1_ d      -0.000004   0.005093  -0.000035   0.000408   0.000000   0.000000
    C1_ s       0.000006   0.018499   0.000000   0.005260   0.000000   0.000000
    C1_ p       0.009082   0.359602   0.581491   0.010152   0.000000   0.000000
    C1_ d       0.017595   0.007308   0.001622   0.000165   0.000000   0.000000
    H1_ s       0.042006   0.034214   0.000000   0.000276   0.000000   0.000000
    H2_ s       0.046911   0.035471   0.000000   0.000256   0.000000   0.000000
 
   ao class      13A        14A        15A        16A        17A        18A  
 
   ao class      19A        20A        21A        22A        23A        24A  
 
   ao class      25A        26A        27A        28A        29A        30A  
 
   ao class      31A        32A  


                        gross atomic populations
     ao                                            O1_        C1_        H1_
      s         0.000000   0.000000   0.000000   3.906952   3.252961   0.779498
      p         0.000000   0.000000   0.000000   4.452370   2.735722   0.000000
      d         0.000000   0.000000   0.000000   0.017799   0.060733   0.000000
    total       0.000000   0.000000   0.000000   8.377121   6.049416   0.779498
 
 
     ao           H2_
      s         0.793966
    total       0.793966
 

 Total number of electrons:   16.00000000

 !timer: mcscf                           cpu_time=     9.801 walltime=     9.873
