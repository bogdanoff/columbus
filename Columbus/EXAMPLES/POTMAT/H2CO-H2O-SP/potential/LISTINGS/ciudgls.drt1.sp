1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at

    Thomas Mueller
    Central Institute of Applied Mathematics
    Research Centre Juelich
    D-52425 Juelich, Germany
    Internet: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      09/20/00    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 using llenci=          -1
================================================================================
four external integ    0.25 MB location: local disk    
three external inte    0.50 MB location: local disk    
four external integ    0.25 MB location: local disk    
three external inte    0.50 MB location: local disk    
diagonal integrals     0.12 MB location: local disk    
off-diagonal integr    0.53 MB location: local disk    
computed file size in DP units
fil3w:       65534
fil3x:       65534
fil4w:       32767
fil4x:       32767
ofdgint:       16380
diagint:       69615
computed file size in DP units
fil3w:      524272
fil3x:      524272
fil4w:      262136
fil4x:      262136
ofdgint:      131040
diagint:      556920
 nsubmx=           8  lenci=       22512
global arrays:       382704   (              2.92 MB)
vdisk:                    0   (              0.00 MB)
drt:                1023631   (              3.90 MB)
================================================================================
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core           99999999 DP per process
 CIUDG version 5.9.7 ( 5-Oct-2004)
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 0,
  GSET = 0,
   DAVCOR =10,
  NCOREL = 12
  NROOT = 3
  IVMODE = 3
  NBKITR = 1
  NVBKMN = 3
  NVBKMX = 8
  RTOLBK = 1e-4,1e-4,1e-4,
  NITER = 90
  NVCIMN = 5
  NVCIMX = 8
  RTOLCI = 1e-4,1e-4,1e-4,
  IDEN  = 1
  CSFPRN = 10,
 /&end
 ------------------------------------------------------------------------

 ** list of control variables **
 nrfitr =   30      nvrfmx =    8      nvrfmn =    5
 lvlprt =    0      nroot  =    3      noldv  =    0      noldhv =    0
 nunitv =    3      ntype  =    0      nbkitr =    1      niter  =   90
 ivmode =    3      vout   =    0      istrt  =    0      iortls =    0
 nvbkmx =    8      ibktv  =   -1      ibkthv =   -1      frcsub =    0
 nvcimx =    8      icitv  =   -1      icithv =   -1      maxseg =    4
 iden   =    1      itran  =    0      froot  =    0      rtmode =    0
 ftcalc =    1      lrtshift=1.0000    ncouple=    1      skipso  =   F
 ncorel =   12      csfprn  =  10      ctol   = 1.00E-02  davcor  =  10
 smalld = 1.00E-03

 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-04    1.000E-04
    2        1.000E-04    1.000E-04
    3        1.000E-04    1.000E-04
 
 units and filenames:
    1: ( 6)    ciudgls                                                     
    2: ( 5)    ciudgin                                                     
    3: ( 7)    ciudgsm                                                     
    4: ( 4)    cihdiag                                                     
    5: ( 3)    cihpseu                                                     
    6: (10)    cihvfl                                                      
    7: (11)    civfl                                                       
    8: (12)    diagint                                                     
    9: (13)    ofdgint                                                     
   10: (17)    cidrtfl                                                     
   11: (20)    ciftdfl                                                     
   12: (21)    ciftofl                                                     
   13: (24)    ciftifl                                                     
   14: ( 8)    ciflind                                                     
   15: (15)    civin                                                       
   16: (16)    civout                                                      
   18: (18)    d1fl                                                        
   20: (53)    cifvfl                                                      
   21: (25)    ciscr4                                                      
   22: (41)    ciftotd                                                     
   23: (42)    flacpfd                                                     
   26: (26)    ciscr5                                                      
   27: (47)    restart                                                     
   28: (48)    civout                                                      
   29: (49)    cirefv                                                      
   31: (31)    fil4w                                                       
   32: (32)    fil4x                                                       
   33: (33)    fil3w                                                       
   34: (34)    fil3x                                                       
   36: (36)    aoints                                                      
   37: (37)    aoints2                                                     
   38: (38)    drtfil                                                      
   39: (50)    cisrtif                                                     
   42: (50)    mocoef                                                      
   45: (52)    nocoef_ci                                                   
   46: (53)    civfl_restart                                               
 ------------------------------------------------------------------------

 workspace allocation information: lcore= 100000000 mem1=         0 ifirst=         1

 integral file titles:
 Hermit Integral Program : SIFS version  wizard2.itc.unvie 11:12:27.841 04-Feb-08
  cidrt_title                                                                    
  title                                                                          
 mofmt: formatted orbitals label=morbl   wizard2.itc.unvie 11:12:33.816 04-Feb-08
 SIFS file created by program tran.      wizard2.itc.unvie 11:12:33.887 04-Feb-08

 core energy values from the integral file:
 energy( 1)=  2.862970696524E+01, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -1.037873143466E+02, ietype=    5,   fcore energy of type: Vref(*) 

 total core repulsion energy = -7.515760738136E+01

 drt header information:
  cidrt_title                                                                    
 spnorb, spnodd, lxyzir,hmult F F           0           0           0
           0
 nmot  =    32 niot  =    10 nfct  =     0 nfvt  =     0
 nrow  =    79 nsym  =     1 ssym  =     1 lenbuf=  1600
 nwalk,xbar:       1344      336     1008        0        0
 nvalwt,nvalw:     1344      336     1008        0        0
 ncsft:           22512
 total number of valid internal walks:    1344
 nvalz,nvaly,nvalx,nvalw =      336    1008       0       0

 cisrt info file parameters:
 file number  12 blocksize   4095
 mxbld   4095
 nd4ext,nd2ext,nd0ext   506   440   110
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int    37191    60060    41745    14520     1980        0        0        0
 minbl4,minbl3,maxbl2   756   756   759
 maxbuf 32767
 number of external orbitals per symmetry block:  22
 nmsym   1 number of internal orbitals  10

 formula file title:
 Hermit Integral Program : SIFS version  wizard2.itc.unvie 11:12:27.841 04-Feb-08
  cidrt_title                                                                    
  title                                                                          
 mofmt: formatted orbitals label=morbl   wizard2.itc.unvie 11:12:33.816 04-Feb-08
 SIFS file created by program tran.      wizard2.itc.unvie 11:12:33.887 04-Feb-08
  cidrt_title                                                                    
 file nmb.  24 block size     0
 pthz,pthy,pthx,pthw:   336  1008     0     0 total internal walks:    1344
 maxlp3,n3xtlp,n2xtlp,n1xtlp,n0xtlp,n2lp,n1lp,n0lp     0     0     0     0
 orbsym(*)= 1 1 1 1 1 1 1 1 1 1

 setref:      336 references kept,
                0 references were marked as invalid, out of
              336 total.
 limcnvrt: found         336  valid internal walksout of          336 
  walks (skipping trailing invalids)
  ... adding          336  segmentation marks segtype=           1
 limcnvrt: found        1008  valid internal walksout of         1008 
  walks (skipping trailing invalids)
  ... adding         1008  segmentation marks segtype=           2
 limcnvrt: found           0  valid internal walksout of            0 
  walks (skipping trailing invalids)
  ... adding            0  segmentation marks segtype=           3
 limcnvrt: found           0  valid internal walksout of            0 
  walks (skipping trailing invalids)
  ... adding            0  segmentation marks segtype=           4

 number of external paths / symmetry
 vertex x     231
 vertex w     253

 lprune: l(*,*,*) pruned with nwalk=     336 nvalwt=     336 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    1008 nvalwt=    1008 nprune=       0



                   segmentation summary for type diagonal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1         336|       336|         0|       336|         0|         1|
 -------------------------------------------------------------------------------
  Y 2        1008|     22176|       336|      1008|       336|         2|
 -------------------------------------------------------------------------------
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS
----------------------------------------------------------------------------------------------------
     1  2   1    11      one-ext yz   1X  2 1    1008     336      22176        336    1008     336
     2  1   1     1      allint zz    OX  1 1     336     336        336        336     336     336
     3  2   2     5      0ex2ex yy    OX  2 2    1008    1008      22176      22176    1008    1008
     4  1   1    75      dg-024ext z  DG  1 1     336     336        336        336     336     336
     5  2   2    45      4exdg024 y   DG  2 2    1008    1008      22176      22176    1008    1008
----------------------------------------------------------------------------------------------------
 DIAGEL COUNTS: DG0X,DG2X,DG4X=       39620        7168        1008
 diagonal elements written to file   4

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      3 vectors will be written to unit 11 beginning with logical record   1

            3 vectors will be created
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     10332 2x:         0 4x:         0
All internal counts: zz :     51156 yy:         0 xx:         0 ww:         0
One-external counts: yz :         0 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:         0    task #   2:     43243    task #   3:         0    task #   4:      6068
task #   5:         0    task #
 reference space has dimension     336

    root           eigenvalues
    ----           ------------
       1        -114.2700908505
       2        -114.1533584712
       3        -113.9735007795

 strefv generated    3 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt=         336

         vector  1 from unit 11 written to unit 49 filename cirefv                                                      
 ufvoutnew: ... writing  recamt=         336

         vector  2 from unit 11 written to unit 49 filename cirefv                                                      
 ufvoutnew: ... writing  recamt=         336

         vector  3 from unit 11 written to unit 49 filename cirefv                                                      

 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=   336)...
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:             22512
 number of initial trial vectors:                         3
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               8
 number of roots to converge:                             3
 number of iterations:                                    1
 residual norm convergence criteria:               0.000100  0.000100  0.000100

          starting bk iteration   1

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:         0 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:         0    task #   4:      6068
task #   5:      3763    task #
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:         0 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:         0    task #   4:      6068
task #   5:      3763    task #
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:         0 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:         0    task #   4:      6068
task #   5:      3763    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     1.00000000     0.00000000     0.00000000
 ref:   2     0.00000000    -1.00000000     0.00000000
 ref:   3     0.00000000     0.00000000     1.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -114.2700908505  7.1054E-15  8.8382E-02  4.8452E-01  1.0000E-04
 mr-sdci #  1  2   -114.1533584712  1.4211E-14  0.0000E+00  4.9521E-01  1.0000E-04
 mr-sdci #  1  3   -113.9735007795 -6.3949E-14  0.0000E+00  4.8904E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.05   0.03   0.00   0.05         0.    0.0000
    2    1    0   0.02   0.01   0.00   0.02         0.    0.0000
    3    5    0   0.00   0.00   0.00   0.00         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.00   0.00   0.00   0.00         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0800s 
time spent in multnx:                   0.0800s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0500s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.2900s 

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -114.2700908505  7.1054E-15  8.8382E-02  4.8452E-01  1.0000E-04
 mr-sdci #  1  2   -114.1533584712  1.4211E-14  0.0000E+00  4.9521E-01  1.0000E-04
 mr-sdci #  1  3   -113.9735007795 -6.3949E-14  0.0000E+00  4.8904E-01  1.0000E-04
 
 expansion vectors are not transformed.
 matrix-vector products are not transformed.

    4 expansion eigenvectors written to unit nvfile (= 11)
    3 matrix-vector products written to unit nhvfil (= 10)

 from bk iterations: iconv=   1

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:             22512
 number of initial trial vectors:                         4
 number of initial matrix-vector products:                3
 maximum dimension of the subspace vectors:               8
 number of roots to converge:                             3
 number of iterations:                                   90
 residual norm convergence criteria:               0.000100  0.000100  0.000100

          starting ci iteration   1

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1    -0.98505660    -0.00000898     0.00002576    -0.17223091
 ref:   2    -0.00000845     1.00000000     0.00000000    -0.00000384
 ref:   3    -0.00002245     0.00000000    -1.00000000    -0.00002111

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -114.3489797346  7.8889E-02  6.4993E-03  1.0874E-01  1.0000E-04
 mr-sdci #  1  2   -114.1533584713  2.2318E-11  0.0000E+00  4.9521E-01  1.0000E-04
 mr-sdci #  1  3   -113.9735007803  8.2878E-10  0.0000E+00  4.8904E-01  1.0000E-04
 mr-sdci #  1  4   -111.6895188512  3.6532E+01  0.0000E+00  1.0240E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.05   0.03   0.00   0.05         0.    0.0000
    2    1    0   0.03   0.02   0.00   0.03         0.    0.0000
    3    5    0   0.21   0.08   0.00   0.21         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.02   0.02   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3100s 
time spent in multnx:                   0.3100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1500s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3200s 

          starting ci iteration   2

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     0.98320976     0.00001950    -0.00002235     0.11121124    -0.14467422
 ref:   2     0.00001753    -1.00000000     0.00000000     0.00002465     0.00000330
 ref:   3     0.00001981     0.00000000     1.00000000    -0.00000374    -0.00002274

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1   -114.3526373419  3.6576E-03  8.6655E-04  4.3920E-02  1.0000E-04
 mr-sdci #  2  2   -114.1533584722  9.8255E-10  0.0000E+00  4.9521E-01  1.0000E-04
 mr-sdci #  2  3   -113.9735007806  3.0396E-10  0.0000E+00  4.8904E-01  1.0000E-04
 mr-sdci #  2  4   -112.4452219449  7.5570E-01  0.0000E+00  1.4488E+00  1.0000E-04
 mr-sdci #  2  5   -111.5359188834  3.6378E+01  0.0000E+00  1.0475E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.04   0.03   0.00   0.04         0.    0.0000
    2    1    0   0.03   0.01   0.00   0.03         0.    0.0000
    3    5    0   0.22   0.10   0.00   0.22         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.01   0.01   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3000s 
time spent in multnx:                   0.3000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1500s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.3200s 

          starting ci iteration   3

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.98255697     0.00002936    -0.00002147    -0.12179875    -0.00034699     0.14052310
 ref:   2     0.00002532    -1.00000000     0.00000001    -0.00005065    -0.00001803    -0.00001202
 ref:   3     0.00001923     0.00000001     1.00000000     0.00000470    -0.00000516     0.00002242

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1   -114.3533350146  6.9767E-04  9.1677E-05  1.4861E-02  1.0000E-04
 mr-sdci #  3  2   -114.1533584761  3.8919E-09  0.0000E+00  4.9521E-01  1.0000E-04
 mr-sdci #  3  3   -113.9735007807  7.5929E-11  0.0000E+00  4.8904E-01  1.0000E-04
 mr-sdci #  3  4   -112.6087101687  1.6349E-01  0.0000E+00  1.1859E+00  1.0000E-04
 mr-sdci #  3  5   -112.0885108606  5.5259E-01  0.0000E+00  1.5885E+00  1.0000E-04
 mr-sdci #  3  6   -111.4484255514  3.6291E+01  0.0000E+00  9.6561E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.05   0.03   0.00   0.05         0.    0.0000
    2    1    0   0.03   0.01   0.00   0.03         0.    0.0000
    3    5    0   0.22   0.11   0.00   0.22         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3100s 
time spent in multnx:                   0.3100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1500s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3300s 

          starting ci iteration   4

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.98230670     0.00004087    -0.00001854     0.10143221    -0.02362225    -0.13367500    -0.07973730
 ref:   2    -0.00003127    -0.99999999     0.00000005     0.00013849    -0.00001288     0.00004175    -0.00001740
 ref:   3    -0.00001833     0.00000004     1.00000000    -0.00003822    -0.00000889    -0.00002694    -0.00000756

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1   -114.3534089823  7.3968E-05  1.6128E-05  6.1039E-03  1.0000E-04
 mr-sdci #  4  2   -114.1533584984  2.2291E-08  0.0000E+00  4.9521E-01  1.0000E-04
 mr-sdci #  4  3   -113.9735007828  2.0804E-09  0.0000E+00  4.8904E-01  1.0000E-04
 mr-sdci #  4  4   -113.0199552807  4.1125E-01  0.0000E+00  1.0561E+00  1.0000E-04
 mr-sdci #  4  5   -112.1055843144  1.7073E-02  0.0000E+00  1.5665E+00  1.0000E-04
 mr-sdci #  4  6   -111.5946240715  1.4620E-01  0.0000E+00  1.2030E+00  1.0000E-04
 mr-sdci #  4  7   -111.3575400899  3.6200E+01  0.0000E+00  1.2572E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.05   0.03   0.00   0.05         0.    0.0000
    2    1    0   0.02   0.01   0.00   0.02         0.    0.0000
    3    5    0   0.23   0.10   0.00   0.23         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3100s 
time spent in multnx:                   0.3100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1400s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3100s 

          starting ci iteration   5

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.98221309     0.00005003    -0.00001787    -0.09564745    -0.00646665     0.13432148     0.06197974    -0.06467950
 ref:   2     0.00003412    -0.99999997     0.00000006    -0.00023246     0.00002904    -0.00005349     0.00002195    -0.00000455
 ref:   3     0.00001825     0.00000005     1.00000000     0.00004383     0.00001181     0.00002327     0.00001578    -0.00000159

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1   -114.3534210945  1.2112E-05  1.8491E-06  2.1701E-03  1.0000E-04
 mr-sdci #  5  2   -114.1533585313  3.2891E-08  0.0000E+00  4.9521E-01  1.0000E-04
 mr-sdci #  5  3   -113.9735007830  1.7421E-10  0.0000E+00  4.8904E-01  1.0000E-04
 mr-sdci #  5  4   -113.2263138552  2.0636E-01  0.0000E+00  8.8184E-01  1.0000E-04
 mr-sdci #  5  5   -112.2131186794  1.0753E-01  0.0000E+00  1.5783E+00  1.0000E-04
 mr-sdci #  5  6   -111.6352065700  4.0582E-02  0.0000E+00  1.1279E+00  1.0000E-04
 mr-sdci #  5  7   -111.4173832815  5.9843E-02  0.0000E+00  1.3034E+00  1.0000E-04
 mr-sdci #  5  8   -111.3396357341  3.6182E+01  0.0000E+00  1.4773E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.05   0.03   0.00   0.05         0.    0.0000
    2    1    0   0.03   0.03   0.00   0.03         0.    0.0000
    3    5    0   0.22   0.09   0.00   0.22         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.01   0.01   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3100s 
time spent in multnx:                   0.3100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1600s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.3400s 

          starting ci iteration   6

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.98218300    -0.00006273    -0.00001734     0.08588856    -0.01643117     0.04226840
 ref:   2     0.00003582     0.99999990     0.00000008     0.00042998     0.00000237    -0.00009130
 ref:   3     0.00001825    -0.00000006     1.00000000    -0.00005211    -0.00000635    -0.00000851

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  6  1   -114.3534225627  1.4682E-06  3.9800E-07  9.4933E-04  1.0000E-04
 mr-sdci #  6  2   -114.1533586314  1.0004E-07  0.0000E+00  4.9521E-01  1.0000E-04
 mr-sdci #  6  3   -113.9735007831  1.0650E-10  0.0000E+00  4.8904E-01  1.0000E-04
 mr-sdci #  6  4   -113.4405733763  2.1426E-01  0.0000E+00  7.9501E-01  1.0000E-04
 mr-sdci #  6  5   -112.3068069027  9.3688E-02  0.0000E+00  1.5608E+00  1.0000E-04
 mr-sdci #  6  6   -111.5070097108 -1.2820E-01  0.0000E+00  1.7468E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.04   0.03   0.00   0.04         0.    0.0000
    2    1    0   0.03   0.02   0.00   0.03         0.    0.0000
    3    5    0   0.22   0.10   0.00   0.22         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3100s 
time spent in multnx:                   0.3100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1600s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.3200s 

          starting ci iteration   7

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.98218437    -0.00006602     0.00001776     0.06718700    -0.06448160    -0.04095155    -0.00736381
 ref:   2    -0.00003684     0.99999974    -0.00000003     0.00069742     0.00011251     0.00012249    -0.00000279
 ref:   3    -0.00001829    -0.00000001    -1.00000000    -0.00003044     0.00002223     0.00002319    -0.00000602

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  7  1   -114.3534229133  3.5063E-07  6.8580E-08  3.8474E-04  1.0000E-04
 mr-sdci #  7  2   -114.1533587967  1.6534E-07  0.0000E+00  4.9521E-01  1.0000E-04
 mr-sdci #  7  3   -113.9735007835  4.1670E-10  0.0000E+00  4.8904E-01  1.0000E-04
 mr-sdci #  7  4   -113.5729905832  1.3242E-01  0.0000E+00  7.5047E-01  1.0000E-04
 mr-sdci #  7  5   -112.5912999425  2.8449E-01  0.0000E+00  1.2154E+00  1.0000E-04
 mr-sdci #  7  6   -112.0142859842  5.0728E-01  0.0000E+00  1.4532E+00  1.0000E-04
 mr-sdci #  7  7   -111.0405001406 -3.7688E-01  0.0000E+00  1.8365E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.05   0.02   0.00   0.05         0.    0.0000
    2    1    0   0.03   0.03   0.00   0.03         0.    0.0000
    3    5    0   0.22   0.10   0.00   0.21         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.01   0.01   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3100s 
time spent in multnx:                   0.3000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1600s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3300s 

          starting ci iteration   8

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.98217958    -0.00008114     0.00001884    -0.05772993     0.06837735     0.02433806     0.03924595     0.02732795
 ref:   2    -0.00003732     0.99999940     0.00000006    -0.00107829    -0.00015849    -0.00011807     0.00000823     0.00002575
 ref:   3    -0.00001830     0.00000005    -1.00000000    -0.00000051    -0.00003936    -0.00002054    -0.00000804     0.00000459

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  8  1   -114.3534229695  5.6197E-08  9.8841E-09  1.5874E-04  1.0000E-04
 mr-sdci #  8  2   -114.1533590548  2.5811E-07  0.0000E+00  4.9521E-01  1.0000E-04
 mr-sdci #  8  3   -113.9735007840  5.2300E-10  0.0000E+00  4.8904E-01  1.0000E-04
 mr-sdci #  8  4   -113.6901979943  1.1721E-01  0.0000E+00  6.3935E-01  1.0000E-04
 mr-sdci #  8  5   -112.9223687740  3.3107E-01  0.0000E+00  9.5420E-01  1.0000E-04
 mr-sdci #  8  6   -112.0544827006  4.0197E-02  0.0000E+00  1.4940E+00  1.0000E-04
 mr-sdci #  8  7   -111.4806263515  4.4013E-01  0.0000E+00  1.5225E+00  1.0000E-04
 mr-sdci #  8  8   -110.8475276309 -4.9211E-01  0.0000E+00  1.6915E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.05   0.03   0.00   0.05         0.    0.0000
    2    1    0   0.03   0.02   0.00   0.03         0.    0.0000
    3    5    0   0.22   0.12   0.00   0.22         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3100s 
time spent in multnx:                   0.3100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1700s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3300s 

          starting ci iteration   9

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.98218248     0.00006624    -0.00001844     0.03354642    -0.09242029    -0.03426441
 ref:   2     0.00003753    -0.99999881    -0.00000032     0.00153340     0.00014179     0.00000548
 ref:   3     0.00001831    -0.00000020     0.99999999     0.00007711     0.00005582     0.00000010

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  9  1   -114.3534229782  8.7437E-09  0.0000E+00  7.5769E-05  1.0000E-04
 mr-sdci #  9  2   -114.1533593936  3.3878E-07  9.3510E-02  4.9521E-01  1.0000E-04
 mr-sdci #  9  3   -113.9735007867  2.7295E-09  0.0000E+00  4.8904E-01  1.0000E-04
 mr-sdci #  9  4   -113.7828045464  9.2607E-02  0.0000E+00  5.7750E-01  1.0000E-04
 mr-sdci #  9  5   -113.1117235966  1.8935E-01  0.0000E+00  7.5629E-01  1.0000E-04
 mr-sdci #  9  6   -111.4870143748 -5.6747E-01  0.0000E+00  1.4668E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.05   0.02   0.00   0.05         0.    0.0000
    2    1    0   0.03   0.02   0.00   0.03         0.    0.0000
    3    5    0   0.22   0.12   0.00   0.22         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.01   0.01   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3100s 
time spent in multnx:                   0.3100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1700s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3300s 

          starting ci iteration  10

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.98218248     0.00012334    -0.00001848    -0.03354602     0.09242005     0.00067926     0.03426298
 ref:   2    -0.00003831    -0.98399110     0.00091774    -0.00036825    -0.00029913     0.17821416    -0.00038616
 ref:   3    -0.00001831     0.00083943     0.99999951    -0.00008320    -0.00005531    -0.00051510     0.00000100

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 10  1   -114.3534229782  1.5632E-13  0.0000E+00  7.5766E-05  1.0000E-04
 mr-sdci # 10  2   -114.2369969350  8.3638E-02  6.2523E-03  1.0613E-01  1.0000E-04
 mr-sdci # 10  3   -113.9735012307  4.4391E-07  0.0000E+00  4.8904E-01  1.0000E-04
 mr-sdci # 10  4   -113.7828068813  2.3348E-06  0.0000E+00  5.7750E-01  1.0000E-04
 mr-sdci # 10  5   -113.1117238019  2.0522E-07  0.0000E+00  7.5629E-01  1.0000E-04
 mr-sdci # 10  6   -111.6035695514  1.1656E-01  0.0000E+00  1.0268E+00  1.0000E-04
 mr-sdci # 10  7   -111.4870137940  6.3874E-03  0.0000E+00  1.4668E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.04   0.03   0.00   0.04         0.    0.0000
    2    1    0   0.03   0.03   0.00   0.03         0.    0.0000
    3    5    0   0.22   0.11   0.00   0.22         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.00   0.00   0.00   0.00         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3000s 
time spent in multnx:                   0.3000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1800s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3200s 

          starting ci iteration  11

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.98218248    -0.00000034    -0.00001957    -0.03354684    -0.09240883     0.00309619     0.01280564    -0.03179128
 ref:   2    -0.00003739    -0.98199141     0.00105295    -0.00030092    -0.00010496    -0.10321453     0.14955138     0.05169909
 ref:   3    -0.00001831     0.00094482     0.99999928    -0.00008436     0.00005865     0.00061141    -0.00037134    -0.00012614

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 11  1   -114.3534229782  3.3467E-12  0.0000E+00  7.5734E-05  1.0000E-04
 mr-sdci # 11  2   -114.2408431127  3.8462E-03  1.1080E-03  4.7479E-02  1.0000E-04
 mr-sdci # 11  3   -113.9735014867  2.5600E-07  0.0000E+00  4.8904E-01  1.0000E-04
 mr-sdci # 11  4   -113.7828070273  1.4605E-07  0.0000E+00  5.7749E-01  1.0000E-04
 mr-sdci # 11  5   -113.1117330143  9.2124E-06  0.0000E+00  7.5625E-01  1.0000E-04
 mr-sdci # 11  6   -112.4804927078  8.7692E-01  0.0000E+00  1.4496E+00  1.0000E-04
 mr-sdci # 11  7   -111.4970193968  1.0006E-02  0.0000E+00  1.1105E+00  1.0000E-04
 mr-sdci # 11  8   -111.4856797875  6.3815E-01  0.0000E+00  1.4295E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.05   0.04   0.00   0.05         0.    0.0000
    2    1    0   0.03   0.03   0.00   0.03         0.    0.0000
    3    5    0   0.23   0.10   0.00   0.23         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.01   0.01   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3200s 
time spent in multnx:                   0.3200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1800s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3400s 

          starting ci iteration  12

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.98218247    -0.00009915    -0.00001374     0.03354883     0.09239064     0.00560007
 ref:   2    -0.00003792     0.98146273     0.00123827     0.00034243    -0.00006897     0.03678309
 ref:   3    -0.00001830    -0.00109063     0.99999847     0.00008002    -0.00005072    -0.00115758

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 12  1   -114.3534229782  5.6914E-12  0.0000E+00  7.5716E-05  1.0000E-04
 mr-sdci # 12  2   -114.2416040728  7.6096E-04  3.6507E-04  2.3902E-02  1.0000E-04
 mr-sdci # 12  3   -113.9735037949  2.3083E-06  0.0000E+00  4.8904E-01  1.0000E-04
 mr-sdci # 12  4   -113.7828072878  2.6051E-07  0.0000E+00  5.7747E-01  1.0000E-04
 mr-sdci # 12  5   -113.1117430914  1.0077E-05  0.0000E+00  7.5619E-01  1.0000E-04
 mr-sdci # 12  6   -112.2149124801 -2.6558E-01  0.0000E+00  1.6103E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.05   0.03   0.00   0.05         0.    0.0000
    2    1    0   0.03   0.01   0.00   0.03         0.    0.0000
    3    5    0   0.22   0.09   0.00   0.22         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.01   0.01   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3100s 
time spent in multnx:                   0.3100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1400s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3200s 

          starting ci iteration  13

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.98218245     0.00002236    -0.00003006     0.03358469     0.00178378    -0.09248039     0.00971800
 ref:   2    -0.00003742     0.98090557     0.00145546    -0.00006969    -0.04623984     0.00160785     0.02154988
 ref:   3    -0.00001832    -0.00119752     0.99999366     0.00014517     0.00325939    -0.00004262    -0.00039659

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 13  1   -114.3534229783  1.5419E-11  0.0000E+00  7.5488E-05  1.0000E-04
 mr-sdci # 13  2   -114.2419215811  3.1751E-04  9.7636E-05  1.3073E-02  1.0000E-04
 mr-sdci # 13  3   -113.9735081150  4.3201E-06  0.0000E+00  4.8903E-01  1.0000E-04
 mr-sdci # 13  4   -113.7828296846  2.2397E-05  0.0000E+00  5.7698E-01  1.0000E-04
 mr-sdci # 13  5   -113.2937606012  1.8202E-01  0.0000E+00  1.0655E+00  1.0000E-04
 mr-sdci # 13  6   -113.1115403694  8.9663E-01  0.0000E+00  7.5774E-01  1.0000E-04
 mr-sdci # 13  7   -111.8493266561  3.5231E-01  0.0000E+00  1.6723E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.05   0.03   0.00   0.05         0.    0.0000
    2    1    0   0.02   0.01   0.00   0.02         0.    0.0000
    3    5    0   0.23   0.10   0.00   0.22         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3100s 
time spent in multnx:                   0.3000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1400s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3300s 

          starting ci iteration  14

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.98218230     0.00010110    -0.00002222    -0.03388716    -0.00152836    -0.09194150     0.01773911    -0.01805343
 ref:   2    -0.00003701    -0.97971611    -0.00216295    -0.00790565    -0.09945565     0.00044303     0.04750736    -0.06860366
 ref:   3    -0.00001827     0.00126017    -0.99996401     0.00068679     0.00833417     0.00009599    -0.00032039    -0.00010789

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 14  1   -114.3534229784  1.1957E-10  0.0000E+00  7.3434E-05  1.0000E-04
 mr-sdci # 14  2   -114.2420053628  8.3782E-05  2.8044E-05  6.8803E-03  1.0000E-04
 mr-sdci # 14  3   -113.9735186559  1.0541E-05  0.0000E+00  4.8901E-01  1.0000E-04
 mr-sdci # 14  4   -113.7832766077  4.4692E-04  0.0000E+00  5.7151E-01  1.0000E-04
 mr-sdci # 14  5   -113.7112697513  4.1751E-01  0.0000E+00  6.3283E-01  1.0000E-04
 mr-sdci # 14  6   -113.1123186835  7.7831E-04  0.0000E+00  7.5433E-01  1.0000E-04
 mr-sdci # 14  7   -111.9017814012  5.2455E-02  0.0000E+00  1.5939E+00  1.0000E-04
 mr-sdci # 14  8   -111.5087406194  2.3061E-02  0.0000E+00  1.2875E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.05   0.03   0.00   0.05         0.    0.0000
    2    1    0   0.02   0.01   0.00   0.02         0.    0.0000
    3    5    0   0.22   0.10   0.00   0.22         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.00   0.00   0.00   0.00         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3000s 
time spent in multnx:                   0.3000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1500s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3300s 

          starting ci iteration  15

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.98218159    -0.00007030     0.00016318    -0.01133433     0.03412529    -0.05144630
 ref:   2    -0.00003684    -0.97955668    -0.00267414     0.09242181     0.00498880    -0.04878872
 ref:   3    -0.00001834     0.00128692    -0.99989781    -0.01421795    -0.00032407     0.00014357

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 15  1   -114.3534229787  3.2703E-10  0.0000E+00  6.7275E-05  1.0000E-04
 mr-sdci # 15  2   -114.2420225393  1.7177E-05  1.5073E-05  4.6464E-03  1.0000E-04
 mr-sdci # 15  3   -113.9735325403  1.3884E-05  0.0000E+00  4.8896E-01  1.0000E-04
 mr-sdci # 15  4   -113.8150632477  3.1787E-02  0.0000E+00  4.9059E-01  1.0000E-04
 mr-sdci # 15  5   -113.7832555149  7.1986E-02  0.0000E+00  5.7074E-01  1.0000E-04
 mr-sdci # 15  6   -112.3042406458 -8.0808E-01  0.0000E+00  1.3599E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.04   0.03   0.00   0.04         0.    0.0000
    2    1    0   0.03   0.03   0.00   0.03         0.    0.0000
    3    5    0   0.23   0.10   0.00   0.23         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3100s 
time spent in multnx:                   0.3100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1600s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3100s 

          starting ci iteration  16

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.98218130     0.00004806    -0.00010623     0.00312866     0.03490107    -0.02580573     0.06622188
 ref:   2    -0.00003711    -0.97950396    -0.00447729     0.07421215    -0.00029690     0.09103578     0.01797829
 ref:   3    -0.00001830     0.00131703    -0.99914076    -0.04141758     0.00030334    -0.00035781    -0.00006300

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 16  1   -114.3534229788  7.8799E-11  0.0000E+00  6.5662E-05  1.0000E-04
 mr-sdci # 16  2   -114.2420351750  1.2636E-05  2.9071E-06  2.5042E-03  1.0000E-04
 mr-sdci # 16  3   -113.9735973482  6.4808E-05  0.0000E+00  4.8863E-01  1.0000E-04
 mr-sdci # 16  4   -113.9173539225  1.0229E-01  0.0000E+00  3.8368E-01  1.0000E-04
 mr-sdci # 16  5   -113.7833743275  1.1881E-04  0.0000E+00  5.6584E-01  1.0000E-04
 mr-sdci # 16  6   -112.7401669041  4.3593E-01  0.0000E+00  1.0990E+00  1.0000E-04
 mr-sdci # 16  7   -112.2298809743  3.2810E-01  0.0000E+00  1.3740E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.04   0.03   0.00   0.04         0.    0.0000
    2    1    0   0.03   0.02   0.00   0.03         0.    0.0000
    3    5    0   0.22   0.09   0.00   0.22         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.01   0.01   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3000s 
time spent in multnx:                   0.3000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1500s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.3300s 

          starting ci iteration  17

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.98218128    -0.00004077     0.00025650    -0.00163277     0.03496244     0.00808273     0.07122732     0.00080273
 ref:   2    -0.00003712     0.97946153     0.01304458    -0.06772229     0.00065477    -0.08662527    -0.00764042     0.04574281
 ref:   3    -0.00001832    -0.00132928     0.98563320     0.16889306     0.00019930     0.00031949     0.00006676    -0.00025860

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 17  1   -114.3534229788  1.8694E-11  0.0000E+00  6.5207E-05  1.0000E-04
 mr-sdci # 17  2   -114.2420376218  2.4468E-06  6.2752E-07  1.1183E-03  1.0000E-04
 mr-sdci # 17  3   -113.9739162618  3.1891E-04  0.0000E+00  4.8367E-01  1.0000E-04
 mr-sdci # 17  4   -113.9593650032  4.2011E-02  0.0000E+00  3.1780E-01  1.0000E-04
 mr-sdci # 17  5   -113.7834265319  5.2204E-05  0.0000E+00  5.6494E-01  1.0000E-04
 mr-sdci # 17  6   -113.0453115632  3.0514E-01  0.0000E+00  8.1784E-01  1.0000E-04
 mr-sdci # 17  7   -112.2909254243  6.1044E-02  0.0000E+00  1.3398E+00  1.0000E-04
 mr-sdci # 17  8   -111.2954674389 -2.1327E-01  0.0000E+00  1.5028E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.05   0.02   0.00   0.05         0.    0.0000
    2    1    0   0.03   0.03   0.00   0.03         0.    0.0000
    3    5    0   0.22   0.08   0.00   0.22         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.01   0.01   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3100s 
time spent in multnx:                   0.3100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1400s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3300s 

          starting ci iteration  18

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.98218129     0.00004307    -0.00080076    -0.00160794     0.03490311    -0.00599369
 ref:   2    -0.00003721    -0.97943680    -0.03338015    -0.06215878     0.00102060    -0.01514344
 ref:   3    -0.00001833     0.00133184    -0.89036205     0.45525053     0.00010860     0.00030518

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 18  1   -114.3534229788  1.4211E-11  0.0000E+00  6.5011E-05  1.0000E-04
 mr-sdci # 18  2   -114.2420380457  4.2389E-07  2.0227E-07  6.0351E-04  1.0000E-04
 mr-sdci # 18  3   -113.9747511753  8.3491E-04  0.0000E+00  4.4841E-01  1.0000E-04
 mr-sdci # 18  4   -113.9687196962  9.3547E-03  0.0000E+00  3.2777E-01  1.0000E-04
 mr-sdci # 18  5   -113.7836747925  2.4826E-04  0.0000E+00  5.6664E-01  1.0000E-04
 mr-sdci # 18  6   -112.2617723284 -7.8354E-01  0.0000E+00  1.5579E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.05   0.03   0.00   0.05         0.    0.0000
    2    1    0   0.02   0.01   0.00   0.02         0.    0.0000
    3    5    0   0.23   0.09   0.00   0.23         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.01   0.01   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3100s 
time spent in multnx:                   0.3100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1400s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3200s 

          starting ci iteration  19

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.98218135     0.00003776    -0.00012248     0.00004103     0.03496135    -0.00997656    -0.01524664
 ref:   2    -0.00003735    -0.97944439    -0.06332616     0.01370949     0.00137605    -0.04347180    -0.04935489
 ref:   3    -0.00001832     0.00133301    -0.23341151    -0.97237672     0.00007310     0.00039441     0.00041094

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 19  1   -114.3534229788  2.2794E-11  0.0000E+00  6.4424E-05  1.0000E-04
 mr-sdci # 19  2   -114.2420382251  1.7946E-07  4.0172E-08  2.8280E-04  1.0000E-04
 mr-sdci # 19  3   -113.9835192742  8.7681E-03  0.0000E+00  2.5318E-01  1.0000E-04
 mr-sdci # 19  4   -113.9729241352  4.2044E-03  0.0000E+00  4.8047E-01  1.0000E-04
 mr-sdci # 19  5   -113.7836883183  1.3526E-05  0.0000E+00  5.6623E-01  1.0000E-04
 mr-sdci # 19  6   -113.0720136513  8.1024E-01  0.0000E+00  1.1643E+00  1.0000E-04
 mr-sdci # 19  7   -111.7834967066 -5.0743E-01  0.0000E+00  1.5726E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.05   0.02   0.00   0.05         0.    0.0000
    2    1    0   0.03   0.03   0.00   0.03         0.    0.0000
    3    5    0   0.22   0.08   0.00   0.22         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.01   0.01   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3100s 
time spent in multnx:                   0.3100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1400s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3300s 

          starting ci iteration  20

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.98218145     0.00004238     0.00154662     0.00032905    -0.03454809    -0.00988233    -0.03616125     0.03298418
 ref:   2    -0.00003736    -0.97944300     0.06348111     0.01066607    -0.00178319     0.03409906    -0.05876428     0.00009619
 ref:   3    -0.00001832     0.00133290     0.18805363    -0.98215698     0.00002286    -0.00102247     0.00024811     0.00037823

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 20  1   -114.3534229788  1.2754E-11  0.0000E+00  6.4016E-05  1.0000E-04
 mr-sdci # 20  2   -114.2420382511  2.6021E-08  1.4751E-08  1.5561E-04  1.0000E-04
 mr-sdci # 20  3   -113.9858720002  2.3527E-03  0.0000E+00  2.4319E-01  1.0000E-04
 mr-sdci # 20  4   -113.9730485377  1.2440E-04  0.0000E+00  4.8371E-01  1.0000E-04
 mr-sdci # 20  5   -113.7838441381  1.5582E-04  0.0000E+00  5.6876E-01  1.0000E-04
 mr-sdci # 20  6   -113.3342821357  2.6227E-01  0.0000E+00  8.5402E-01  1.0000E-04
 mr-sdci # 20  7   -111.8818590877  9.8362E-02  0.0000E+00  1.5108E+00  1.0000E-04
 mr-sdci # 20  8   -111.4742110540  1.7874E-01  0.0000E+00  1.5468E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.05   0.03   0.00   0.05         0.    0.0000
    2    1    0   0.02   0.01   0.00   0.02         0.    0.0000
    3    5    0   0.22   0.09   0.00   0.22         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.01   0.01   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.009999
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3000s 
time spent in multnx:                   0.3000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1400s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3200s 

          starting ci iteration  21

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.98218133    -0.00003870    -0.00039245     0.00003114     0.03550462    -0.05705204
 ref:   2    -0.00003734     0.97944338    -0.06332462     0.00994037     0.00216584    -0.01478321
 ref:   3    -0.00001831    -0.00133258    -0.17783157    -0.98405829    -0.00020811     0.00130434

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 21  1   -114.3534229789  8.4768E-12  0.0000E+00  6.3907E-05  1.0000E-04
 mr-sdci # 21  2   -114.2420382581  7.0117E-09  0.0000E+00  8.7226E-05  1.0000E-04
 mr-sdci # 21  3   -113.9864238473  5.5185E-04  3.5708E-02  2.3825E-01  1.0000E-04
 mr-sdci # 21  4   -113.9730821451  3.3607E-05  0.0000E+00  4.8425E-01  1.0000E-04
 mr-sdci # 21  5   -113.7842216125  3.7747E-04  0.0000E+00  5.6175E-01  1.0000E-04
 mr-sdci # 21  6   -112.4210721274 -9.1321E-01  0.0000E+00  1.4315E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.05   0.02   0.00   0.05         0.    0.0000
    2    1    0   0.03   0.02   0.00   0.03         0.    0.0000
    3    5    0   0.22   0.08   0.00   0.21         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.00   0.00   0.00   0.00         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3100s 
time spent in multnx:                   0.3000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1300s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3300s 

          starting ci iteration  22

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.98218133    -0.00003866    -0.00084115     0.00019841    -0.03556418     0.05131130     0.02802180
 ref:   2    -0.00003735     0.97944328     0.05859950     0.01922697    -0.00269672     0.00599676     0.03112856
 ref:   3    -0.00001834    -0.00133379     0.31596036    -0.94778100    -0.00254408    -0.01577333     0.04026777

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 22  1   -114.3534229789  6.3949E-14  0.0000E+00  6.3907E-05  1.0000E-04
 mr-sdci # 22  2   -114.2420382582  3.0070E-11  0.0000E+00  8.6920E-05  1.0000E-04
 mr-sdci # 22  3   -114.0079580535  2.1534E-02  7.3505E-03  1.3343E-01  1.0000E-04
 mr-sdci # 22  4   -113.9735284133  4.4627E-04  0.0000E+00  4.7066E-01  1.0000E-04
 mr-sdci # 22  5   -113.7843025080  8.0895E-05  0.0000E+00  5.6198E-01  1.0000E-04
 mr-sdci # 22  6   -112.4491595470  2.8087E-02  0.0000E+00  1.4090E+00  1.0000E-04
 mr-sdci # 22  7   -112.0842551400  2.0240E-01  0.0000E+00  1.3090E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.04   0.03   0.00   0.04         0.    0.0000
    2    1    0   0.03   0.02   0.00   0.03         0.    0.0000
    3    5    0   0.22   0.09   0.00   0.22         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3100s 
time spent in multnx:                   0.3100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1500s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3100s 

          starting ci iteration  23

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.98218132    -0.00003867    -0.00029495    -0.00107249    -0.03552295    -0.05133910    -0.01214715     0.02654473
 ref:   2     0.00003729     0.97944322    -0.03655317     0.04993136    -0.00321497    -0.00392376    -0.01740234     0.03585663
 ref:   3     0.00001662    -0.00133608    -0.82673403    -0.54328951     0.03580745    -0.03302009    -0.10018821    -0.09451825

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 23  1   -114.3534229789  1.1767E-11  0.0000E+00  6.3883E-05  1.0000E-04
 mr-sdci # 23  2   -114.2420382582  9.2513E-12  0.0000E+00  8.7063E-05  1.0000E-04
 mr-sdci # 23  3   -114.0231472741  1.5189E-02  3.0100E-02  2.5690E-01  1.0000E-04
 mr-sdci # 23  4   -113.9966097315  2.3081E-02  0.0000E+00  2.6833E-01  1.0000E-04
 mr-sdci # 23  5   -113.7858465113  1.5440E-03  0.0000E+00  5.6266E-01  1.0000E-04
 mr-sdci # 23  6   -112.4835200937  3.4361E-02  0.0000E+00  1.4362E+00  1.0000E-04
 mr-sdci # 23  7   -112.1723847931  8.8130E-02  0.0000E+00  1.2382E+00  1.0000E-04
 mr-sdci # 23  8   -111.6496604866  1.7545E-01  0.0000E+00  1.3595E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.05   0.02   0.00   0.05         0.    0.0000
    2    1    0   0.03   0.03   0.00   0.03         0.    0.0000
    3    5    0   0.23   0.10   0.00   0.23         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.01   0.01   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3200s 
time spent in multnx:                   0.3200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1600s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3300s 

          starting ci iteration  24

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.98218132     0.00003867     0.00059051    -0.00069965     0.03537412    -0.00674404
 ref:   2    -0.00003730    -0.97944323     0.01107053     0.06061205     0.00246057    -0.01349586
 ref:   3    -0.00001800     0.00133375     0.98072477    -0.10219953    -0.00758307     0.09354388

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 24  1   -114.3534229789  6.6507E-12  0.0000E+00  6.3921E-05  1.0000E-04
 mr-sdci # 24  2   -114.2420382582  7.8373E-12  0.0000E+00  8.7501E-05  1.0000E-04
 mr-sdci # 24  3   -114.0486742247  2.5527E-02  9.1480E-03  1.6933E-01  1.0000E-04
 mr-sdci # 24  4   -114.0059619549  9.3522E-03  0.0000E+00  8.5796E-02  1.0000E-04
 mr-sdci # 24  5   -113.7872041116  1.3576E-03  0.0000E+00  5.6552E-01  1.0000E-04
 mr-sdci # 24  6   -112.0204413618 -4.6308E-01  0.0000E+00  1.5671E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.06   0.04   0.00   0.06         0.    0.0000
    2    1    0   0.03   0.01   0.00   0.03         0.    0.0000
    3    5    0   0.21   0.09   0.00   0.21         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3200s 
time spent in multnx:                   0.3200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1500s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3300s 

          starting ci iteration  25

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.98218133     0.00003863    -0.00076577    -0.00075974     0.03531656     0.00773810     0.00442640
 ref:   2     0.00003738    -0.97944364    -0.01085082     0.05893236     0.00159065     0.07333171    -0.01161521
 ref:   3     0.00001825     0.00133155    -0.98158361    -0.04932913    -0.00398124    -0.12109753    -0.05785804

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 25  1   -114.3534229789  3.4035E-12  0.0000E+00  6.3791E-05  1.0000E-04
 mr-sdci # 25  2   -114.2420382583  8.8491E-11  0.0000E+00  8.7861E-05  1.0000E-04
 mr-sdci # 25  3   -114.0515409568  2.8667E-03 -9.0965E-03  1.4997E-01  1.0000E-04
 mr-sdci # 25  4   -114.0076890746  1.7271E-03  0.0000E+00  5.4194E-02  1.0000E-04
 mr-sdci # 25  5   -113.7873733275  1.6922E-04  0.0000E+00  5.6335E-01  1.0000E-04
 mr-sdci # 25  6   -112.2214900965  2.0105E-01  0.0000E+00  1.2140E+00  1.0000E-04
 mr-sdci # 25  7   -111.9951903207 -1.7719E-01  0.0000E+00  1.6128E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.05   0.02   0.00   0.05         0.    0.0000
    2    1    0   0.03   0.03   0.00   0.03         0.    0.0000
    3    5    0   0.22   0.08   0.00   0.22         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.01   0.01   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3100s 
time spent in multnx:                   0.3100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1400s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3200s 

          starting ci iteration  26

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.98218132     0.00003865     0.00095260    -0.00076365    -0.03531904     0.00382849     0.00353556     0.00800232
 ref:   2    -0.00003760    -0.97944417     0.00437175     0.05929993    -0.00070892     0.09674291     0.04397790    -0.03279327
 ref:   3    -0.00001736     0.00133494     0.97998807    -0.04197407     0.00747238    -0.01049323    -0.07022153    -0.13289151

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 26  1   -114.3534229789  1.2442E-11  0.0000E+00  6.3604E-05  1.0000E-04
 mr-sdci # 26  2   -114.2420382583  6.7196E-11  0.0000E+00  8.9364E-05  1.0000E-04
 mr-sdci # 26  3   -114.0584624996  6.9215E-03  2.9230E-03  7.9027E-02  1.0000E-04
 mr-sdci # 26  4   -114.0077066813  1.7607E-05  0.0000E+00  5.3201E-02  1.0000E-04
 mr-sdci # 26  5   -113.7874865854  1.1326E-04  0.0000E+00  5.6478E-01  1.0000E-04
 mr-sdci # 26  6   -112.6916459087  4.7016E-01  0.0000E+00  1.3364E+00  1.0000E-04
 mr-sdci # 26  7   -112.1191755239  1.2399E-01  0.0000E+00  1.4138E+00  1.0000E-04
 mr-sdci # 26  8   -111.6253559877 -2.4304E-02  0.0000E+00  1.2281E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.05   0.02   0.00   0.05         0.    0.0000
    2    1    0   0.03   0.03   0.00   0.03         0.    0.0000
    3    5    0   0.22   0.12   0.00   0.22         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3200s 
time spent in multnx:                   0.3200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1800s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3300s 

          starting ci iteration  27

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.98218132     0.00003865     0.00078707     0.00067318     0.03516171     0.00440677
 ref:   2    -0.00003768    -0.97944407     0.00498258    -0.05892540    -0.00134402     0.04111190
 ref:   3    -0.00001780     0.00133588     0.98157088     0.03280101    -0.00151954     0.02617780

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 27  1   -114.3534229789  7.7804E-12  0.0000E+00  6.3609E-05  1.0000E-04
 mr-sdci # 27  2   -114.2420382584  1.5554E-11  0.0000E+00  8.8715E-05  1.0000E-04
 mr-sdci # 27  3   -114.0599091880  1.4467E-03 -7.5525E-04  5.7079E-02  1.0000E-04
 mr-sdci # 27  4   -114.0078817123  1.7503E-04  0.0000E+00  5.1348E-02  1.0000E-04
 mr-sdci # 27  5   -113.7905799863  3.0934E-03  0.0000E+00  5.7451E-01  1.0000E-04
 mr-sdci # 27  6   -112.3414181676 -3.5023E-01  0.0000E+00  1.4190E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.04   0.03   0.00   0.04         0.    0.0000
    2    1    0   0.03   0.02   0.00   0.03         0.    0.0000
    3    5    0   0.22   0.09   0.00   0.22         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.01   0.01   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3000s 
time spent in multnx:                   0.3000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1500s 
time for vector access in mult:         0.0200s 
total time per CI iteration:            0.3300s 

          starting ci iteration  28

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.98218131    -0.00003866     0.00066394     0.00065161     0.03505314    -0.01525902     0.00631068
 ref:   2    -0.00003765     0.97944396     0.00416432    -0.05906746    -0.00199173    -0.09806294     0.02462243
 ref:   3    -0.00001783    -0.00133575     0.98194069     0.03252379    -0.00151648     0.01754101    -0.04836903

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 28  1   -114.3534229789  9.2371E-14  0.0000E+00  6.3610E-05  1.0000E-04
 mr-sdci # 28  2   -114.2420382584  1.9753E-12  0.0000E+00  8.8188E-05  1.0000E-04
 mr-sdci # 28  3   -114.0600135811  1.0439E-04  5.4121E-04  5.4355E-02  1.0000E-04
 mr-sdci # 28  4   -114.0078848282  3.1159E-06  0.0000E+00  5.1046E-02  1.0000E-04
 mr-sdci # 28  5   -113.7906360689  5.6083E-05  0.0000E+00  5.7441E-01  1.0000E-04
 mr-sdci # 28  6   -112.6351273837  2.9371E-01  0.0000E+00  1.2593E+00  1.0000E-04
 mr-sdci # 28  7   -112.1610711292  4.1896E-02  0.0000E+00  1.4977E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.05   0.03   0.00   0.05         0.    0.0000
    2    1    0   0.03   0.01   0.00   0.03         0.    0.0000
    3    5    0   0.22   0.10   0.00   0.22         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3100s 
time spent in multnx:                   0.3100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1400s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3200s 

          starting ci iteration  29

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.98218135     0.00003870     0.00096443     0.00077099     0.03359352    -0.02126838    -0.00929102     0.01557658
 ref:   2    -0.00003755    -0.97944408     0.00238977    -0.05972722     0.00036381     0.00622104    -0.10920319    -0.01305974
 ref:   3    -0.00001805     0.00133621     0.98098617     0.02089326     0.01014458     0.04029536     0.01158040    -0.06928241

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 29  1   -114.3534229789  8.5549E-12  0.0000E+00  6.3528E-05  1.0000E-04
 mr-sdci # 29  2   -114.2420382584  1.0637E-11  0.0000E+00  8.8206E-05  1.0000E-04
 mr-sdci # 29  3   -114.0610725544  1.0590E-03 -8.6393E-05  3.7936E-02  1.0000E-04
 mr-sdci # 29  4   -114.0082070881  3.2226E-04  0.0000E+00  4.4424E-02  1.0000E-04
 mr-sdci # 29  5   -113.7957428980  5.1068E-03  0.0000E+00  5.7046E-01  1.0000E-04
 mr-sdci # 29  6   -112.8968965600  2.6177E-01  0.0000E+00  1.0840E+00  1.0000E-04
 mr-sdci # 29  7   -112.5923618324  4.3129E-01  0.0000E+00  1.2905E+00  1.0000E-04
 mr-sdci # 29  8   -111.8700876186  2.4473E-01  0.0000E+00  1.5027E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.05   0.03   0.00   0.05         0.    0.0000
    2    1    0   0.02   0.01   0.00   0.02         0.    0.0000
    3    5    0   0.23   0.10   0.00   0.23         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.01   0.01   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.019999
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3100s 
time spent in multnx:                   0.3100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1500s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3300s 

          starting ci iteration  30

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.98218151     0.00003914    -0.00083099    -0.00059478    -0.01890824     0.06496981
 ref:   2    -0.00003755    -0.97944407    -0.00236904     0.05972227    -0.00083761    -0.00047308
 ref:   3    -0.00001788     0.00133563    -0.98114798    -0.02075686    -0.01407777    -0.02792128

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 30  1   -114.3534229789  1.4509E-11  0.0000E+00  6.3836E-05  1.0000E-04
 mr-sdci # 30  2   -114.2420382585  1.1190E-10  0.0000E+00  8.7022E-05  1.0000E-04
 mr-sdci # 30  3   -114.0610864222  1.3868E-05  9.2770E-05  3.7527E-02  1.0000E-04
 mr-sdci # 30  4   -114.0082394032  3.2315E-05  0.0000E+00  4.3132E-02  1.0000E-04
 mr-sdci # 30  5   -113.8430417387  4.7299E-02  0.0000E+00  5.6948E-01  1.0000E-04
 mr-sdci # 30  6   -112.9794547363  8.2558E-02  0.0000E+00  1.1734E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.05   0.02   0.00   0.05         0.    0.0000
    2    1    0   0.03   0.02   0.00   0.03         0.    0.0000
    3    5    0   0.22   0.10   0.00   0.22         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.01   0.01   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3100s 
time spent in multnx:                   0.3100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1500s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3200s 

          starting ci iteration  31

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.98218155    -0.00003939    -0.00049551    -0.00039058    -0.01504033    -0.06661158    -0.00938122
 ref:   2    -0.00003750     0.97944441    -0.00266129     0.05936639    -0.00473688     0.01381090    -0.03517040
 ref:   3    -0.00001778    -0.00133447    -0.98098883    -0.01652988    -0.00235001     0.00741855     0.04500841

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 31  1   -114.3534229789  4.4977E-12  0.0000E+00  6.3647E-05  1.0000E-04
 mr-sdci # 31  2   -114.2420382587  1.8427E-10  0.0000E+00  8.5300E-05  1.0000E-04
 mr-sdci # 31  3   -114.0614230655  3.3664E-04  7.5270E-05  2.4448E-02  1.0000E-04
 mr-sdci # 31  4   -114.0083840516  1.4465E-04  0.0000E+00  3.9930E-02  1.0000E-04
 mr-sdci # 31  5   -113.8558757397  1.2834E-02  0.0000E+00  5.5111E-01  1.0000E-04
 mr-sdci # 31  6   -113.1241573456  1.4470E-01  0.0000E+00  9.5923E-01  1.0000E-04
 mr-sdci # 31  7   -111.9559560434 -6.3641E-01  0.0000E+00  1.6579E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.05   0.03   0.00   0.05         0.    0.0000
    2    1    0   0.03   0.01   0.00   0.02         0.    0.0000
    3    5    0   0.23   0.10   0.00   0.23         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.01   0.01   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3200s 
time spent in multnx:                   0.3100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1500s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3200s 

          starting ci iteration  32

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.98218156    -0.00003937    -0.00052646    -0.00041567    -0.01478882    -0.06745036    -0.00023806     0.00649744
 ref:   2     0.00003760     0.97944453    -0.00287548     0.05918624    -0.00325533    -0.00389711     0.10047987    -0.01261735
 ref:   3     0.00001778    -0.00133446    -0.98095783    -0.01639515    -0.00279416     0.01107254    -0.02505897    -0.03755378

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 32  1   -114.3534229789  1.5845E-12  0.0000E+00  6.3538E-05  1.0000E-04
 mr-sdci # 32  2   -114.2420382587  2.4585E-12  0.0000E+00  8.5879E-05  1.0000E-04
 mr-sdci # 32  3   -114.0614302944  7.2289E-06 -8.8070E-05  2.4786E-02  1.0000E-04
 mr-sdci # 32  4   -114.0083887039  4.6522E-06  0.0000E+00  4.0511E-02  1.0000E-04
 mr-sdci # 32  5   -113.8561441948  2.6846E-04  0.0000E+00  5.4819E-01  1.0000E-04
 mr-sdci # 32  6   -113.1376392553  1.3482E-02  0.0000E+00  9.5946E-01  1.0000E-04
 mr-sdci # 32  7   -112.7166627798  7.6071E-01  0.0000E+00  1.2654E+00  1.0000E-04
 mr-sdci # 32  8   -111.7468445356 -1.2324E-01  0.0000E+00  1.6761E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.05   0.03   0.00   0.04         0.    0.0000
    2    1    0   0.03   0.02   0.00   0.03         0.    0.0000
    3    5    0   0.23   0.09   0.00   0.23         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3200s 
time spent in multnx:                   0.3100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1400s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3400s 

          starting ci iteration  33

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.98218156     0.00003940     0.00049261     0.00039357     0.01459965    -0.01643069
 ref:   2     0.00003760    -0.97944437     0.00264013    -0.05933977     0.00206274    -0.10379225
 ref:   3     0.00001778     0.00133442     0.98098220     0.01632581     0.00274715     0.00901243

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 33  1   -114.3534229789  0.0000E+00  0.0000E+00  6.3536E-05  1.0000E-04
 mr-sdci # 33  2   -114.2420382587  3.5172E-12  0.0000E+00  8.5062E-05  1.0000E-04
 mr-sdci # 33  3   -114.0614373025  7.0081E-06  7.0200E-05  2.3903E-02  1.0000E-04
 mr-sdci # 33  4   -114.0083916987  2.9948E-06  0.0000E+00  3.9900E-02  1.0000E-04
 mr-sdci # 33  5   -113.8563066765  1.6248E-04  0.0000E+00  5.4686E-01  1.0000E-04
 mr-sdci # 33  6   -112.6414814264 -4.9616E-01  0.0000E+00  1.3153E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.05   0.03   0.00   0.05         0.    0.0000
    2    1    0   0.03   0.02   0.00   0.03         0.    0.0000
    3    5    0   0.24   0.09   0.00   0.24         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.009999
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3300s 
time spent in multnx:                   0.3300s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1400s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3400s 

          starting ci iteration  34

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.98218157     0.00003926     0.00070825     0.00055195     0.01535740     0.01876210     0.01653327
 ref:   2     0.00003759    -0.97944413     0.00220518    -0.05957696     0.00075520     0.09782130    -0.05182993
 ref:   3     0.00001779     0.00133393     0.98096879     0.01464185     0.00000836    -0.01027034    -0.00785898

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 34  1   -114.3534229789  1.0658E-13  0.0000E+00  6.3487E-05  1.0000E-04
 mr-sdci # 34  2   -114.2420382587  6.7715E-11  0.0000E+00  8.3399E-05  1.0000E-04
 mr-sdci # 34  3   -114.0615674644  1.3016E-04  8.4813E-05  1.9514E-02  1.0000E-04
 mr-sdci # 34  4   -114.0084534406  6.1742E-05  0.0000E+00  3.8877E-02  1.0000E-04
 mr-sdci # 34  5   -113.8584020843  2.0954E-03  0.0000E+00  5.5356E-01  1.0000E-04
 mr-sdci # 34  6   -112.6442448308  2.7634E-03  0.0000E+00  1.3391E+00  1.0000E-04
 mr-sdci # 34  7   -112.4850126460 -2.3165E-01  0.0000E+00  1.4721E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.05   0.03   0.00   0.05         0.    0.0000
    2    1    0   0.03   0.01   0.00   0.03         0.    0.0000
    3    5    0   0.22   0.10   0.00   0.22         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3200s 
time spent in multnx:                   0.3200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1500s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3200s 

          starting ci iteration  35

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.98218159     0.00003908     0.00103753     0.00100254     0.01621678     0.03497713     0.00086262    -0.00503567
 ref:   2     0.00003759    -0.97944415     0.00218799    -0.05947256     0.00107950     0.01820466     0.10853126    -0.01464861
 ref:   3     0.00001779     0.00133381     0.98080247     0.01288851    -0.00117725    -0.02541794    -0.00105891     0.00186091

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 35  1   -114.3534229789  6.3949E-13  0.0000E+00  6.3413E-05  1.0000E-04
 mr-sdci # 35  2   -114.2420382588  2.5878E-11  0.0000E+00  8.3438E-05  1.0000E-04
 mr-sdci # 35  3   -114.0616299416  6.2477E-05 -3.8832E-05  1.9445E-02  1.0000E-04
 mr-sdci # 35  4   -114.0085584139  1.0497E-04  0.0000E+00  3.8015E-02  1.0000E-04
 mr-sdci # 35  5   -113.8586930856  2.9100E-04  0.0000E+00  5.4956E-01  1.0000E-04
 mr-sdci # 35  6   -113.4482943483  8.0405E-01  0.0000E+00  6.5198E-01  1.0000E-04
 mr-sdci # 35  7   -112.5738268195  8.8814E-02  0.0000E+00  1.2940E+00  1.0000E-04
 mr-sdci # 35  8   -112.0848311157  3.3799E-01  0.0000E+00  1.5934E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.04   0.03   0.00   0.04         0.    0.0000
    2    1    0   0.03   0.01   0.00   0.03         0.    0.0000
    3    5    0   0.22   0.08   0.00   0.22         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.00   0.00   0.00   0.00         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3000s 
time spent in multnx:                   0.3000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1300s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.3400s 

          starting ci iteration  36

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.98218110     0.00003923    -0.00060508    -0.00067935    -0.02373222     0.06029368
 ref:   2     0.00003765    -0.97944416    -0.00221698     0.05943590     0.00012917    -0.00888772
 ref:   3     0.00001808     0.00133371    -0.98092064    -0.01258091     0.00073477    -0.01479683

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 36  1   -114.3534229790  6.2208E-11  0.0000E+00  6.2011E-05  1.0000E-04
 mr-sdci # 36  2   -114.2420382588  4.6398E-12  0.0000E+00  8.3391E-05  1.0000E-04
 mr-sdci # 36  3   -114.0616602875  3.0346E-05  3.7416E-05  1.6751E-02  1.0000E-04
 mr-sdci # 36  4   -114.0085730325  1.4619E-05  0.0000E+00  3.7720E-02  1.0000E-04
 mr-sdci # 36  5   -113.8676100906  8.9170E-03  0.0000E+00  4.9037E-01  1.0000E-04
 mr-sdci # 36  6   -113.2650004478 -1.8329E-01  0.0000E+00  9.3906E-01  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.05   0.03   0.00   0.05         0.    0.0000
    2    1    0   0.02   0.01   0.00   0.02         0.    0.0000
    3    5    0   0.22   0.10   0.00   0.22         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.00   0.00   0.00   0.00         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3000s 
time spent in multnx:                   0.3000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1500s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3200s 

          starting ci iteration  37

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.98218111    -0.00003935     0.00050305     0.00056751    -0.02374613    -0.05748574     0.03124949
 ref:   2     0.00003762     0.97944453     0.00250464    -0.05905748     0.00018433     0.00232036    -0.06610671
 ref:   3     0.00001807    -0.00133349     0.98085718     0.01156537     0.00068880     0.01677558     0.01351009

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 37  1   -114.3534229790  5.5422E-13  0.0000E+00  6.1850E-05  1.0000E-04
 mr-sdci # 37  2   -114.2420382588  6.2023E-11  0.0000E+00  8.4489E-05  1.0000E-04
 mr-sdci # 37  3   -114.0617065814  4.6294E-05 -6.0635E-05  1.4973E-02  1.0000E-04
 mr-sdci # 37  4   -114.0086279538  5.4921E-05  0.0000E+00  3.7499E-02  1.0000E-04
 mr-sdci # 37  5   -113.8676110357  9.4515E-07  0.0000E+00  4.9050E-01  1.0000E-04
 mr-sdci # 37  6   -113.2733403013  8.3399E-03  0.0000E+00  9.7219E-01  1.0000E-04
 mr-sdci # 37  7   -112.4215076670 -1.5232E-01  0.0000E+00  1.5226E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.05   0.03   0.00   0.05         0.    0.0000
    2    1    0   0.02   0.01   0.00   0.02         0.    0.0000
    3    5    0   0.22   0.11   0.00   0.22         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.00   0.00   0.00   0.00         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.009999
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3000s 
time spent in multnx:                   0.3000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1600s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3200s 

          starting ci iteration  38

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.98218111    -0.00003942    -0.00041671    -0.00047165    -0.02430111    -0.05737427    -0.02774894     0.02443574
 ref:   2     0.00003762     0.97944425    -0.00222319     0.05933742    -0.00146065    -0.02205710     0.10052791     0.04564280
 ref:   3     0.00001807    -0.00133340    -0.98087982    -0.01135954     0.00048370     0.01637318    -0.00773461     0.00988936

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 38  1   -114.3534229790  4.2633E-14  0.0000E+00  6.1843E-05  1.0000E-04
 mr-sdci # 38  2   -114.2420382589  1.3983E-11  0.0000E+00  8.3171E-05  1.0000E-04
 mr-sdci # 38  3   -114.0617186462  1.2065E-05  4.5504E-05  1.3925E-02  1.0000E-04
 mr-sdci # 38  4   -114.0086405538  1.2600E-05  0.0000E+00  3.6632E-02  1.0000E-04
 mr-sdci # 38  5   -113.8680125058  4.0147E-04  0.0000E+00  4.8947E-01  1.0000E-04
 mr-sdci # 38  6   -113.3246072423  5.1267E-02  0.0000E+00  8.8567E-01  1.0000E-04
 mr-sdci # 38  7   -112.6190568876  1.9755E-01  0.0000E+00  1.2834E+00  1.0000E-04
 mr-sdci # 38  8   -111.7967070165 -2.8812E-01  0.0000E+00  1.7215E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.05   0.03   0.00   0.05         0.    0.0000
    2    1    0   0.03   0.01   0.00   0.03         0.    0.0000
    3    5    0   0.22   0.09   0.00   0.22         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.01   0.01   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3100s 
time spent in multnx:                   0.3100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1400s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3300s 

          starting ci iteration  39

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.98218112     0.00003937     0.00047325     0.00053220     0.02422630    -0.02044267
 ref:   2     0.00003766    -0.97944449     0.00244733    -0.05908063     0.00114912    -0.08341897
 ref:   3     0.00001807     0.00133338     0.98083943     0.01109967    -0.00036158     0.01647111

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 39  1   -114.3534229790  4.1922E-13  0.0000E+00  6.1802E-05  1.0000E-04
 mr-sdci # 39  2   -114.2420382589  1.2854E-11  0.0000E+00  8.4075E-05  1.0000E-04
 mr-sdci # 39  3   -114.0617294979  1.0852E-05 -5.0161E-05  1.3937E-02  1.0000E-04
 mr-sdci # 39  4   -114.0086528356  1.2282E-05  0.0000E+00  3.6909E-02  1.0000E-04
 mr-sdci # 39  5   -113.8680279482  1.5442E-05  0.0000E+00  4.8919E-01  1.0000E-04
 mr-sdci # 39  6   -112.7221948577 -6.0241E-01  0.0000E+00  1.4111E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.05   0.03   0.00   0.05         0.    0.0000
    2    1    0   0.03   0.03   0.00   0.03         0.    0.0000
    3    5    0   0.22   0.08   0.00   0.22         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.01   0.01   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3100s 
time spent in multnx:                   0.3100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1500s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3300s 

          starting ci iteration  40

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.98218112     0.00003937     0.00046553     0.00052357     0.02421683     0.01918018    -0.00710953
 ref:   2     0.00003766    -0.97944416     0.00210848    -0.05942228     0.00091808     0.10123965     0.03051348
 ref:   3     0.00001807     0.00133321     0.98084548     0.01054117    -0.00046389    -0.01352607     0.00940797

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 40  1   -114.3534229790  1.4211E-14  0.0000E+00  6.1792E-05  1.0000E-04
 mr-sdci # 40  2   -114.2420382589  3.5811E-11  0.0000E+00  8.1976E-05  1.0000E-04
 mr-sdci # 40  3   -114.0617571835  2.7686E-05  2.4985E-05  1.1857E-02  1.0000E-04
 mr-sdci # 40  4   -114.0086858389  3.3003E-05  0.0000E+00  3.5102E-02  1.0000E-04
 mr-sdci # 40  5   -113.8680424197  1.4472E-05  0.0000E+00  4.8997E-01  1.0000E-04
 mr-sdci # 40  6   -112.7810123630  5.8818E-02  0.0000E+00  1.2888E+00  1.0000E-04
 mr-sdci # 40  7   -112.3299761473 -2.8908E-01  0.0000E+00  1.6113E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.05   0.03   0.00   0.05         0.    0.0000
    2    1    0   0.02   0.01   0.00   0.02         0.    0.0000
    3    5    0   0.22   0.10   0.00   0.22         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.01   0.01   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3000s 
time spent in multnx:                   0.3000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1500s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3100s 

          starting ci iteration  41

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.98218128     0.00003928     0.00062768     0.00065822     0.02129917     0.03766709    -0.00123341     0.00375737
 ref:   2    -0.00003761    -0.97944414     0.00208270    -0.05942666     0.00058074     0.01871930    -0.10530605    -0.03035168
 ref:   3    -0.00001802     0.00133322     0.98072702     0.01019298     0.00247931    -0.02706526    -0.00044057     0.00298701

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 41  1   -114.3534229790  2.2666E-11  0.0000E+00  6.1506E-05  1.0000E-04
 mr-sdci # 41  2   -114.2420382589  6.0609E-12  0.0000E+00  8.1913E-05  1.0000E-04
 mr-sdci # 41  3   -114.0617731159  1.5932E-05 -1.9697E-05  1.2771E-02  1.0000E-04
 mr-sdci # 41  4   -114.0086966140  1.0775E-05  0.0000E+00  3.5146E-02  1.0000E-04
 mr-sdci # 41  5   -113.8704914947  2.4491E-03  0.0000E+00  5.0499E-01  1.0000E-04
 mr-sdci # 41  6   -113.5186632319  7.3765E-01  0.0000E+00  6.5148E-01  1.0000E-04
 mr-sdci # 41  7   -112.5762196931  2.4624E-01  0.0000E+00  1.3020E+00  1.0000E-04
 mr-sdci # 41  8   -112.0225577628  2.2585E-01  0.0000E+00  1.6554E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.06   0.04   0.00   0.06         0.    0.0000
    2    1    0   0.03   0.02   0.00   0.03         0.    0.0000
    3    5    0   0.22   0.09   0.00   0.22         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.009999
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3200s 
time spent in multnx:                   0.3200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1500s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3400s 

          starting ci iteration  42

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.98218114    -0.00003940    -0.00039077    -0.00043834    -0.02072317     0.06232180
 ref:   2     0.00003764     0.97944416    -0.00212241     0.05937904    -0.00071375    -0.01347060
 ref:   3     0.00001809    -0.00133316    -0.98078764    -0.01005193    -0.00249015    -0.01320783

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 42  1   -114.3534229790  5.3149E-12  0.0000E+00  6.1306E-05  1.0000E-04
 mr-sdci # 42  2   -114.2420382589  3.2472E-12  0.0000E+00  8.1924E-05  1.0000E-04
 mr-sdci # 42  3   -114.0617848881  1.1772E-05  1.7302E-05  1.0799E-02  1.0000E-04
 mr-sdci # 42  4   -114.0087061309  9.5169E-06  0.0000E+00  3.4897E-02  1.0000E-04
 mr-sdci # 42  5   -113.8705430343  5.1540E-05  0.0000E+00  5.0761E-01  1.0000E-04
 mr-sdci # 42  6   -113.2687047816 -2.4996E-01  0.0000E+00  9.3469E-01  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.05   0.02   0.00   0.05         0.    0.0000
    2    1    0   0.03   0.02   0.00   0.03         0.    0.0000
    3    5    0   0.22   0.10   0.00   0.22         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.01   0.01   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3100s 
time spent in multnx:                   0.3100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1500s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3300s 

          starting ci iteration  43

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.98218115    -0.00003943    -0.00036633    -0.00041565    -0.02073400    -0.05986770     0.02265646
 ref:   2    -0.00003762     0.97944438    -0.00232375     0.05916443    -0.00054567     0.00469239    -0.06675329
 ref:   3    -0.00001808    -0.00133310    -0.98073636    -0.00967318    -0.00259831     0.01559071     0.01375556

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 43  1   -114.3534229790  1.8474E-13  0.0000E+00  6.1254E-05  1.0000E-04
 mr-sdci # 43  2   -114.2420382589  2.0677E-11  0.0000E+00  8.2796E-05  1.0000E-04
 mr-sdci # 43  3   -114.0618032284  1.8340E-05 -2.7324E-05  9.9727E-03  1.0000E-04
 mr-sdci # 43  4   -114.0087229947  1.6864E-05  0.0000E+00  3.5303E-02  1.0000E-04
 mr-sdci # 43  5   -113.8705518743  8.8400E-06  0.0000E+00  5.0762E-01  1.0000E-04
 mr-sdci # 43  6   -113.2820775024  1.3373E-02  0.0000E+00  9.7882E-01  1.0000E-04
 mr-sdci # 43  7   -112.5113418963 -6.4878E-02  0.0000E+00  1.4920E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.05   0.03   0.00   0.05         0.    0.0000
    2    1    0   0.02   0.01   0.00   0.02         0.    0.0000
    3    5    0   0.22   0.10   0.00   0.22         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.01   0.01   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3000s 
time spent in multnx:                   0.3000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1500s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3100s 

          starting ci iteration  44

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.98218116     0.00003947    -0.00031434    -0.00036695    -0.02046456    -0.05859375    -0.02704203    -0.00878648
 ref:   2    -0.00003767    -0.97944421    -0.00212564     0.05933850     0.00022640    -0.02370852     0.09620546    -0.05816020
 ref:   3    -0.00001808     0.00133306    -0.98074906    -0.00958264    -0.00253962     0.01481756    -0.00819803    -0.01121364

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 44  1   -114.3534229790  4.6185E-13  0.0000E+00  6.1189E-05  1.0000E-04
 mr-sdci # 44  2   -114.2420382589  5.5849E-12  0.0000E+00  8.2006E-05  1.0000E-04
 mr-sdci # 44  3   -114.0618094564  6.2280E-06  1.9354E-05  9.0233E-03  1.0000E-04
 mr-sdci # 44  4   -114.0087278957  4.9010E-06  0.0000E+00  3.4829E-02  1.0000E-04
 mr-sdci # 44  5   -113.8706394734  8.7599E-05  0.0000E+00  5.0829E-01  1.0000E-04
 mr-sdci # 44  6   -113.3566416048  7.4564E-02  0.0000E+00  8.5149E-01  1.0000E-04
 mr-sdci # 44  7   -112.6492779579  1.3794E-01  0.0000E+00  1.2854E+00  1.0000E-04
 mr-sdci # 44  8   -111.7452833248 -2.7727E-01  0.0000E+00  1.6898E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.06   0.03   0.00   0.06         0.    0.0000
    2    1    0   0.03   0.03   0.00   0.03         0.    0.0000
    3    5    0   0.21   0.10   0.00   0.21         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3200s 
time spent in multnx:                   0.3200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1700s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3400s 

          starting ci iteration  45

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.98218117    -0.00003943     0.00035086     0.00040430     0.02024223    -0.01831243
 ref:   2    -0.00003771     0.97944439     0.00230149    -0.05914581    -0.00126266    -0.08374030
 ref:   3    -0.00001807    -0.00133306     0.98071702     0.00943776     0.00287754     0.01606019

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 45  1   -114.3534229790  3.8369E-13  0.0000E+00  6.1140E-05  1.0000E-04
 mr-sdci # 45  2   -114.2420382589  6.5086E-12  0.0000E+00  8.2645E-05  1.0000E-04
 mr-sdci # 45  3   -114.0618152010  5.7446E-06 -2.2739E-05  9.2115E-03  1.0000E-04
 mr-sdci # 45  4   -114.0087340321  6.1364E-06  0.0000E+00  3.5151E-02  1.0000E-04
 mr-sdci # 45  5   -113.8707917800  1.5231E-04  0.0000E+00  5.0789E-01  1.0000E-04
 mr-sdci # 45  6   -112.8527027390 -5.0394E-01  0.0000E+00  1.3739E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.05   0.03   0.00   0.05         0.    0.0000
    2    1    0   0.02   0.01   0.00   0.02         0.    0.0000
    3    5    0   0.22   0.10   0.00   0.22         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3100s 
time spent in multnx:                   0.3100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1500s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3200s 

          starting ci iteration  46

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.98218117    -0.00003944     0.00033422     0.00038429     0.02030347     0.01836198    -0.00149664
 ref:   2    -0.00003768     0.97944419     0.00210695    -0.05933483    -0.00025988     0.09443499     0.04320970
 ref:   3    -0.00001808    -0.00133299     0.98071544     0.00921429     0.00320718    -0.01480860     0.00669464

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 46  1   -114.3534229790  5.8975E-13  0.0000E+00  6.1084E-05  1.0000E-04
 mr-sdci # 46  2   -114.2420382590  1.5653E-11  0.0000E+00  8.1339E-05  1.0000E-04
 mr-sdci # 46  3   -114.0618268573  1.1656E-05  1.0919E-05  8.1581E-03  1.0000E-04
 mr-sdci # 46  4   -114.0087461337  1.2102E-05  0.0000E+00  3.4344E-02  1.0000E-04
 mr-sdci # 46  5   -113.8711107940  3.1901E-04  0.0000E+00  5.0516E-01  1.0000E-04
 mr-sdci # 46  6   -112.8722896483  1.9587E-02  0.0000E+00  1.2996E+00  1.0000E-04
 mr-sdci # 46  7   -112.3946289632 -2.5465E-01  0.0000E+00  1.6046E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.05   0.04   0.00   0.05         0.    0.0000
    2    1    0   0.03   0.03   0.00   0.03         0.    0.0000
    3    5    0   0.22   0.09   0.00   0.22         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.01   0.01   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.009999
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3100s 
time spent in multnx:                   0.3100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1700s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3200s 

          starting ci iteration  47

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.98218124    -0.00003939     0.00045893     0.00053522     0.01885195     0.03587497    -0.00111528     0.01240116
 ref:   2     0.00003765     0.97944417     0.00207917    -0.05935001    -0.00036345     0.01752533    -0.10273274    -0.04557575
 ref:   3     0.00001805    -0.00133301     0.98063374     0.00895645     0.00446337    -0.02536846    -0.00044972     0.00005236

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 47  1   -114.3534229790  3.8725E-12  0.0000E+00  6.1039E-05  1.0000E-04
 mr-sdci # 47  2   -114.2420382590  1.7124E-12  0.0000E+00  8.1266E-05  1.0000E-04
 mr-sdci # 47  3   -114.0618340757  7.2184E-06 -8.5829E-06  8.7131E-03  1.0000E-04
 mr-sdci # 47  4   -114.0087558032  9.6695E-06  0.0000E+00  3.4187E-02  1.0000E-04
 mr-sdci # 47  5   -113.8716350977  5.2430E-04  0.0000E+00  5.1209E-01  1.0000E-04
 mr-sdci # 47  6   -113.5777126563  7.0542E-01  0.0000E+00  5.6836E-01  1.0000E-04
 mr-sdci # 47  7   -112.5732595716  1.7863E-01  0.0000E+00  1.3111E+00  1.0000E-04
 mr-sdci # 47  8   -111.8824952825  1.3721E-01  0.0000E+00  1.7140E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.06   0.03   0.00   0.06         0.    0.0000
    2    1    0   0.02   0.01   0.00   0.02         0.    0.0000
    3    5    0   0.22   0.09   0.00   0.22         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.01   0.01   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3100s 
time spent in multnx:                   0.3100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1400s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3300s 

          starting ci iteration  48

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.98218110     0.00003950    -0.00029251    -0.00033715    -0.02055434     0.06262049
 ref:   2    -0.00003768    -0.97944420    -0.00211054     0.05930523     0.00078899    -0.01491842
 ref:   3    -0.00001810     0.00133296    -0.98067155    -0.00888662    -0.00432440    -0.01357959

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 48  1   -114.3534229790  5.2225E-12  0.0000E+00  6.0928E-05  1.0000E-04
 mr-sdci # 48  2   -114.2420382590  2.9274E-12  0.0000E+00  8.1297E-05  1.0000E-04
 mr-sdci # 48  3   -114.0618394303  5.3545E-06  8.5364E-06  7.5110E-03  1.0000E-04
 mr-sdci # 48  4   -114.0087627403  6.9371E-06  0.0000E+00  3.4098E-02  1.0000E-04
 mr-sdci # 48  5   -113.8720923306  4.5723E-04  0.0000E+00  5.0502E-01  1.0000E-04
 mr-sdci # 48  6   -113.2476887877 -3.3002E-01  0.0000E+00  9.9145E-01  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.05   0.04   0.00   0.05         0.    0.0000
    2    1    0   0.03   0.03   0.00   0.03         0.    0.0000
    3    5    0   0.22   0.09   0.00   0.22         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.01   0.01   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3100s 
time spent in multnx:                   0.3100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1700s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3300s 

          starting ci iteration  49

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.98218109     0.00003954    -0.00025172    -0.00029211    -0.02079967    -0.06190697     0.01882828
 ref:   2     0.00003770    -0.97944432    -0.00223270     0.05916694     0.00183586     0.01257725    -0.05611901
 ref:   3     0.00001810     0.00133294    -0.98063992    -0.00867665    -0.00492120     0.01423129     0.01312099

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 49  1   -114.3534229790  2.2027E-13  0.0000E+00  6.0963E-05  1.0000E-04
 mr-sdci # 49  2   -114.2420382590  8.4412E-12  0.0000E+00  8.1843E-05  1.0000E-04
 mr-sdci # 49  3   -114.0618486329  9.2027E-06 -1.0722E-05  6.7227E-03  1.0000E-04
 mr-sdci # 49  4   -114.0087724404  9.7002E-06  0.0000E+00  3.4371E-02  1.0000E-04
 mr-sdci # 49  5   -113.8725751660  4.8284E-04  0.0000E+00  5.0103E-01  1.0000E-04
 mr-sdci # 49  6   -113.2490434780  1.3547E-03  0.0000E+00  1.0101E+00  1.0000E-04
 mr-sdci # 49  7   -112.4747735905 -9.8486E-02  0.0000E+00  1.5385E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.05   0.03   0.00   0.05         0.    0.0000
    2    1    0   0.03   0.02   0.00   0.03         0.    0.0000
    3    5    0   0.21   0.11   0.00   0.21         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.00   0.00   0.00   0.00         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.009998
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3000s 
time spent in multnx:                   0.3000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1700s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3200s 

          starting ci iteration  50

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.98218110    -0.00003956    -0.00021921    -0.00025466    -0.02089118    -0.05788888    -0.02952425    -0.00120414
 ref:   2     0.00003772     0.97944423    -0.00211273     0.05929420     0.00157198    -0.02548505     0.09316107    -0.06384035
 ref:   3     0.00001810    -0.00133293    -0.98065039    -0.00864766    -0.00491512     0.01452026    -0.00760377    -0.00928682

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 50  1   -114.3534229790  9.9476E-14  0.0000E+00  6.0941E-05  1.0000E-04
 mr-sdci # 50  2   -114.2420382590  1.1937E-12  0.0000E+00  8.1476E-05  1.0000E-04
 mr-sdci # 50  3   -114.0618507014  2.0685E-06  7.1831E-06  6.0536E-03  1.0000E-04
 mr-sdci # 50  4   -114.0087747663  2.3259E-06  0.0000E+00  3.4023E-02  1.0000E-04
 mr-sdci # 50  5   -113.8725842052  9.0392E-06  0.0000E+00  5.0045E-01  1.0000E-04
 mr-sdci # 50  6   -113.3649211105  1.1588E-01  0.0000E+00  8.5629E-01  1.0000E-04
 mr-sdci # 50  7   -112.6567750427  1.8200E-01  0.0000E+00  1.2849E+00  1.0000E-04
 mr-sdci # 50  8   -111.7271612412 -1.5533E-01  0.0000E+00  1.6568E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.05   0.02   0.00   0.05         0.    0.0000
    2    1    0   0.03   0.03   0.00   0.03         0.    0.0000
    3    5    0   0.22   0.08   0.00   0.22         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3100s 
time spent in multnx:                   0.3100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1300s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3300s 

          starting ci iteration  51

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.98218112     0.00003953     0.00024236     0.00027792     0.02049084    -0.01933359
 ref:   2    -0.00003779    -0.97944435     0.00222374    -0.05917485    -0.00331498    -0.08266291
 ref:   3    -0.00001809     0.00133294     0.98062827     0.00858677     0.00539061     0.01650756

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 51  1   -114.3534229790  1.0161E-12  0.0000E+00  6.0877E-05  1.0000E-04
 mr-sdci # 51  2   -114.2420382590  2.4301E-12  0.0000E+00  8.1902E-05  1.0000E-04
 mr-sdci # 51  3   -114.0618526504  1.9490E-06 -9.4508E-06  6.3875E-03  1.0000E-04
 mr-sdci # 51  4   -114.0087768193  2.0530E-06  0.0000E+00  3.4270E-02  1.0000E-04
 mr-sdci # 51  5   -113.8729594793  3.7527E-04  0.0000E+00  5.0246E-01  1.0000E-04
 mr-sdci # 51  6   -113.0175368720 -3.4738E-01  0.0000E+00  1.2463E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.05   0.02   0.00   0.05         0.    0.0000
    2    1    0   0.03   0.03   0.00   0.03         0.    0.0000
    3    5    0   0.22   0.09   0.00   0.22         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3100s 
time spent in multnx:                   0.3100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1400s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3200s 

          starting ci iteration  52

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.98218111     0.00003954     0.00023347     0.00026542     0.02047572     0.01917502     0.00284425
 ref:   2    -0.00003771    -0.97944422     0.00210508    -0.05928747    -0.00175476     0.07497928     0.06954339
 ref:   3    -0.00001810     0.00133291     0.98061377     0.00845145     0.00604239    -0.01723961     0.00427861

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 52  1   -114.3534229790  3.6877E-12  0.0000E+00  6.0736E-05  1.0000E-04
 mr-sdci # 52  2   -114.2420382590  9.4857E-12  0.0000E+00  8.0946E-05  1.0000E-04
 mr-sdci # 52  3   -114.0618590673  6.4168E-06  5.0358E-06  5.8240E-03  1.0000E-04
 mr-sdci # 52  4   -114.0087832207  6.4015E-06  0.0000E+00  3.3799E-02  1.0000E-04
 mr-sdci # 52  5   -113.8741370764  1.1776E-03  0.0000E+00  5.0219E-01  1.0000E-04
 mr-sdci # 52  6   -113.0248405894  7.3037E-03  0.0000E+00  1.2735E+00  1.0000E-04
 mr-sdci # 52  7   -112.5007709522 -1.5600E-01  0.0000E+00  1.4723E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.06   0.03   0.00   0.06         0.    0.0000
    2    1    0   0.02   0.01   0.00   0.02         0.    0.0000
    3    5    0   0.22   0.09   0.00   0.22         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3100s 
time spent in multnx:                   0.3100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1300s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3300s 

          starting ci iteration  53

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.98218115     0.00003951     0.00031580     0.00038093     0.01809700     0.03838838    -0.00134779     0.02112308
 ref:   2    -0.00003770    -0.97944421     0.00208108    -0.05930975    -0.00170649     0.01541744    -0.10069640    -0.05507093
 ref:   3    -0.00001809     0.00133292     0.98056643     0.00831605     0.00763588    -0.02343059    -0.00074289     0.00002392

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 53  1   -114.3534229790  7.1765E-13  0.0000E+00  6.0723E-05  1.0000E-04
 mr-sdci # 53  2   -114.2420382590  3.1264E-13  0.0000E+00  8.0917E-05  1.0000E-04
 mr-sdci # 53  3   -114.0618616814  2.6142E-06 -4.3305E-06  6.2177E-03  1.0000E-04
 mr-sdci # 53  4   -114.0087879562  4.7355E-06  0.0000E+00  3.3665E-02  1.0000E-04
 mr-sdci # 53  5   -113.8752523105  1.1152E-03  0.0000E+00  5.0624E-01  1.0000E-04
 mr-sdci # 53  6   -113.6051113687  5.8027E-01  0.0000E+00  5.4336E-01  1.0000E-04
 mr-sdci # 53  7   -112.5690055228  6.8235E-02  0.0000E+00  1.3181E+00  1.0000E-04
 mr-sdci # 53  8   -111.8514400558  1.2428E-01  0.0000E+00  1.6880E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.05   0.03   0.00   0.05         0.    0.0000
    2    1    0   0.02   0.01   0.00   0.02         0.    0.0000
    3    5    0   0.22   0.09   0.00   0.22         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.00   0.00   0.00   0.00         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3000s 
time spent in multnx:                   0.3000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1400s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3300s 

          starting ci iteration  54

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.98218112    -0.00003958    -0.00020551    -0.00023225    -0.01915850     0.06177862
 ref:   2     0.00003771     0.97944423    -0.00211014     0.05926586     0.00203947    -0.01874064
 ref:   3     0.00001809    -0.00133290    -0.98058750    -0.00828109    -0.00754585    -0.01150986

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 54  1   -114.3534229790  2.4869E-13  0.0000E+00  6.0729E-05  1.0000E-04
 mr-sdci # 54  2   -114.2420382590  1.2932E-12  0.0000E+00  8.0964E-05  1.0000E-04
 mr-sdci # 54  3   -114.0618642709  2.5895E-06  4.6107E-06  5.4565E-03  1.0000E-04
 mr-sdci # 54  4   -114.0087923043  4.3481E-06  0.0000E+00  3.3670E-02  1.0000E-04
 mr-sdci # 54  5   -113.8754474767  1.9517E-04  0.0000E+00  5.0254E-01  1.0000E-04
 mr-sdci # 54  6   -113.2107472648 -3.9436E-01  0.0000E+00  1.0342E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.05   0.03   0.00   0.05         0.    0.0000
    2    1    0   0.03   0.03   0.00   0.03         0.    0.0000
    3    5    0   0.22   0.09   0.00   0.22         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.01   0.01   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3100s 
time spent in multnx:                   0.3100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1600s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3200s 

          starting ci iteration  55

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.98218109    -0.00003962    -0.00016049    -0.00018048    -0.01981395    -0.06189102     0.01999503
 ref:   2     0.00003777     0.97944431    -0.00219518     0.05916829     0.00367834     0.01903651    -0.05125570
 ref:   3     0.00001809    -0.00133289    -0.98056467    -0.00816012    -0.00834123     0.01143122     0.01243495

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 55  1   -114.3534229790  3.0980E-12  0.0000E+00  6.0772E-05  1.0000E-04
 mr-sdci # 55  2   -114.2420382590  4.6469E-12  0.0000E+00  8.1366E-05  1.0000E-04
 mr-sdci # 55  3   -114.0618691106  4.8397E-06 -5.2595E-06  4.7843E-03  1.0000E-04
 mr-sdci # 55  4   -114.0087976799  5.3756E-06  0.0000E+00  3.3887E-02  1.0000E-04
 mr-sdci # 55  5   -113.8768159180  1.3684E-03  0.0000E+00  4.9346E-01  1.0000E-04
 mr-sdci # 55  6   -113.2107721441  2.4879E-05  0.0000E+00  1.0314E+00  1.0000E-04
 mr-sdci # 55  7   -112.4625503938 -1.0646E-01  0.0000E+00  1.5504E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.06   0.03   0.00   0.05         0.    0.0000
    2    1    0   0.03   0.01   0.00   0.03         0.    0.0000
    3    5    0   0.21   0.09   0.00   0.21         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.01   0.01   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3100s 
time spent in multnx:                   0.3000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1400s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.3300s 

          starting ci iteration  56

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.98218108    -0.00003964    -0.00013419    -0.00014754    -0.02000057    -0.05791066    -0.03094144    -0.00482484
 ref:   2     0.00003775     0.97944425    -0.00211262     0.05926437     0.00319703    -0.02427998     0.09247028    -0.06330456
 ref:   3     0.00001809    -0.00133288    -0.98057172    -0.00814717    -0.00831994     0.01242378    -0.00748969    -0.00798051

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 56  1   -114.3534229790  1.3500E-13  0.0000E+00  6.0789E-05  1.0000E-04
 mr-sdci # 56  2   -114.2420382590  6.0396E-13  0.0000E+00  8.1106E-05  1.0000E-04
 mr-sdci # 56  3   -114.0618701087  9.9815E-07  3.3943E-06  4.2471E-03  1.0000E-04
 mr-sdci # 56  4   -114.0087990214  1.3415E-06  0.0000E+00  3.3633E-02  1.0000E-04
 mr-sdci # 56  5   -113.8768464446  3.0527E-05  0.0000E+00  4.9238E-01  1.0000E-04
 mr-sdci # 56  6   -113.3497567751  1.3898E-01  0.0000E+00  8.6829E-01  1.0000E-04
 mr-sdci # 56  7   -112.6616150320  1.9906E-01  0.0000E+00  1.2826E+00  1.0000E-04
 mr-sdci # 56  8   -111.7907049344 -6.0735E-02  0.0000E+00  1.6406E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.05   0.03   0.00   0.05         0.    0.0000
    2    1    0   0.02   0.01   0.00   0.02         0.    0.0000
    3    5    0   0.22   0.11   0.00   0.22         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3100s 
time spent in multnx:                   0.3100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1600s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3300s 

          starting ci iteration  57

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.98218110     0.00003961     0.00015332     0.00016639     0.01934740    -0.02265014
 ref:   2    -0.00003782    -0.97944433     0.00218876    -0.05918399    -0.00549886    -0.07837928
 ref:   3    -0.00001808     0.00133289     0.98055627     0.00811295     0.00888789     0.01511156

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 57  1   -114.3534229790  1.1440E-12  0.0000E+00  6.0705E-05  1.0000E-04
 mr-sdci # 57  2   -114.2420382590  1.2008E-12  0.0000E+00  8.1393E-05  1.0000E-04
 mr-sdci # 57  3   -114.0618710494  9.4072E-07 -4.6267E-06  4.5639E-03  1.0000E-04
 mr-sdci # 57  4   -114.0087999818  9.6037E-07  0.0000E+00  3.3806E-02  1.0000E-04
 mr-sdci # 57  5   -113.8775300437  6.8360E-04  0.0000E+00  4.9439E-01  1.0000E-04
 mr-sdci # 57  6   -113.0761052900 -2.7365E-01  0.0000E+00  1.1949E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.05   0.03   0.00   0.05         0.    0.0000
    2    1    0   0.02   0.01   0.00   0.02         0.    0.0000
    3    5    0   0.22   0.10   0.00   0.22         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.01   0.01   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3000s 
time spent in multnx:                   0.3000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1500s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3200s 

          starting ci iteration  58

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.98218111     0.00003961     0.00015264     0.00016244     0.01908531     0.02303854     0.00071525
 ref:   2    -0.00003773    -0.97944423     0.00210553    -0.05926157    -0.00331503     0.06485718     0.07751899
 ref:   3    -0.00001808     0.00133288     0.98054224     0.00803731     0.00966081    -0.01602512     0.00272429

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 58  1   -114.3534229790  4.4764E-12  0.0000E+00  6.0536E-05  1.0000E-04
 mr-sdci # 58  2   -114.2420382590  4.9809E-12  0.0000E+00  8.0701E-05  1.0000E-04
 mr-sdci # 58  3   -114.0618743532  3.3038E-06  2.7046E-06  4.2815E-03  1.0000E-04
 mr-sdci # 58  4   -114.0088031134  3.1317E-06  0.0000E+00  3.3512E-02  1.0000E-04
 mr-sdci # 58  5   -113.8798297117  2.2997E-03  0.0000E+00  4.9235E-01  1.0000E-04
 mr-sdci # 58  6   -113.0968117012  2.0706E-02  0.0000E+00  1.2250E+00  1.0000E-04
 mr-sdci # 58  7   -112.5194788513 -1.4214E-01  0.0000E+00  1.4338E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.05   0.03   0.00   0.05         0.    0.0000
    2    1    0   0.03   0.03   0.00   0.03         0.    0.0000
    3    5    0   0.21   0.10   0.00   0.21         0.    0.0000
    4   75    0   0.02   0.01   0.00   0.02         0.    0.0000
    5   45    0   0.00   0.00   0.00   0.00         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3100s 
time spent in multnx:                   0.3100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1700s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.3200s 

          starting ci iteration  59

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.98218114     0.00003960     0.00020729     0.00024406     0.01604905     0.03925264     0.00002804     0.01534036
 ref:   2    -0.00003771    -0.97944422     0.00208631    -0.05928280    -0.00302738     0.01381670    -0.10076854    -0.05210193
 ref:   3    -0.00001807     0.00133289     0.98051236     0.00795482     0.01138127    -0.02014854    -0.00060375     0.00025903

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 59  1   -114.3534229790  7.1765E-13  0.0000E+00  6.0505E-05  1.0000E-04
 mr-sdci # 59  2   -114.2420382590  9.2371E-14  0.0000E+00  8.0679E-05  1.0000E-04
 mr-sdci # 59  3   -114.0618756388  1.2856E-06 -2.2718E-06  4.5321E-03  1.0000E-04
 mr-sdci # 59  4   -114.0088057796  2.6662E-06  0.0000E+00  3.3389E-02  1.0000E-04
 mr-sdci # 59  5   -113.8817861218  1.9564E-03  0.0000E+00  4.9445E-01  1.0000E-04
 mr-sdci # 59  6   -113.5990735165  5.0226E-01  0.0000E+00  5.4397E-01  1.0000E-04
 mr-sdci # 59  7   -112.5612364131  4.1758E-02  0.0000E+00  1.3191E+00  1.0000E-04
 mr-sdci # 59  8   -112.0075101841  2.1681E-01  0.0000E+00  1.6790E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.05   0.03   0.00   0.05         0.    0.0000
    2    1    0   0.03   0.02   0.00   0.03         0.    0.0000
    3    5    0   0.22   0.10   0.00   0.22         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3200s 
time spent in multnx:                   0.3200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1600s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3300s 

          starting ci iteration  60

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.98218110    -0.00003966    -0.00013026    -0.00013458    -0.01801636     0.06003968
 ref:   2     0.00003773     0.97944425    -0.00211212     0.05924352     0.00377296    -0.02223526
 ref:   3     0.00001808    -0.00133287    -0.98052129    -0.00793172    -0.01127693    -0.00831260

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 60  1   -114.3534229790  5.6843E-13  0.0000E+00  6.0522E-05  1.0000E-04
 mr-sdci # 60  2   -114.2420382590  1.1298E-12  0.0000E+00  8.0726E-05  1.0000E-04
 mr-sdci # 60  3   -114.0618769986  1.3598E-06  2.6814E-06  4.0782E-03  1.0000E-04
 mr-sdci # 60  4   -114.0088082895  2.5098E-06  0.0000E+00  3.3441E-02  1.0000E-04
 mr-sdci # 60  5   -113.8825574540  7.7133E-04  0.0000E+00  4.8726E-01  1.0000E-04
 mr-sdci # 60  6   -113.1570055857 -4.4207E-01  0.0000E+00  1.0827E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.05   0.02   0.00   0.05         0.    0.0000
    2    1    0   0.03   0.03   0.00   0.03         0.    0.0000
    3    5    0   0.23   0.08   0.00   0.23         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3200s 
time spent in multnx:                   0.3200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1300s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3200s 

          starting ci iteration  61

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.98218105    -0.00003970    -0.00008270    -0.00007639    -0.01932524    -0.06151541     0.02276185
 ref:   2     0.00003780     0.97944431    -0.00217759     0.05916446     0.00602399     0.02527684    -0.04859435
 ref:   3     0.00001807    -0.00133287    -0.98050267    -0.00785196    -0.01218932     0.00757368     0.01168553

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 61  1   -114.3534229791  3.6309E-12  0.0000E+00  6.0554E-05  1.0000E-04
 mr-sdci # 61  2   -114.2420382590  2.3732E-12  0.0000E+00  8.1031E-05  1.0000E-04
 mr-sdci # 61  3   -114.0618797008  2.7022E-06 -2.9150E-06  3.4870E-03  1.0000E-04
 mr-sdci # 61  4   -114.0088116463  3.3568E-06  0.0000E+00  3.3622E-02  1.0000E-04
 mr-sdci # 61  5   -113.8851750944  2.6176E-03  0.0000E+00  4.7198E-01  1.0000E-04
 mr-sdci # 61  6   -113.1596865912  2.6810E-03  0.0000E+00  1.0511E+00  1.0000E-04
 mr-sdci # 61  7   -112.4569863388 -1.0425E-01  0.0000E+00  1.5555E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.05   0.03   0.00   0.04         0.    0.0000
    2    1    0   0.03   0.03   0.00   0.03         0.    0.0000
    3    5    0   0.22   0.10   0.00   0.22         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3200s 
time spent in multnx:                   0.3100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1700s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.3200s 

          starting ci iteration  62

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.98218104    -0.00003972    -0.00005900    -0.00004285    -0.01980789    -0.05854215    -0.03191883    -0.01128009
 ref:   2     0.00003778     0.97944425    -0.00211680     0.05924450     0.00492284    -0.02214694     0.09309867    -0.05804333
 ref:   3     0.00001808    -0.00133287    -0.98050635    -0.00784249    -0.01215434     0.00900294    -0.00747299    -0.00727443

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 62  1   -114.3534229791  9.9476E-14  0.0000E+00  6.0572E-05  1.0000E-04
 mr-sdci # 62  2   -114.2420382590  4.9027E-13  0.0000E+00  8.0804E-05  1.0000E-04
 mr-sdci # 62  3   -114.0618802832  5.8234E-07  1.7535E-06  3.0282E-03  1.0000E-04
 mr-sdci # 62  4   -114.0088126498  1.0035E-06  0.0000E+00  3.3412E-02  1.0000E-04
 mr-sdci # 62  5   -113.8853474334  1.7234E-04  0.0000E+00  4.6911E-01  1.0000E-04
 mr-sdci # 62  6   -113.3146272278  1.5494E-01  0.0000E+00  8.8747E-01  1.0000E-04
 mr-sdci # 62  7   -112.6614105477  2.0442E-01  0.0000E+00  1.2829E+00  1.0000E-04
 mr-sdci # 62  8   -111.9130729867 -9.4437E-02  0.0000E+00  1.6327E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.05   0.03   0.00   0.05         0.    0.0000
    2    1    0   0.03   0.02   0.00   0.03         0.    0.0000
    3    5    0   0.22   0.09   0.00   0.22         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.01   0.01   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.010000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.3100s 
time spent in multnx:                   0.3100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1500s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.3300s 

          starting ci iteration  63

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.98218107     0.00003970     0.00007611     0.00005825     0.01878353    -0.02654296
 ref:   2    -0.00003786    -0.97944430     0.00217110    -0.05919150    -0.00771515    -0.07093530
 ref:   3    -0.00001806     0.00133287     0.98049553     0.00782205     0.01276618     0.01213329

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 63  1   -114.3534229791  1.6485E-12  0.0000E+00  6.0457E-05  1.0000E-04
 mr-sdci # 63  2   -114.2420382590  3.6948E-13  0.0000E+00  8.0963E-05  1.0000E-04
 mr-sdci # 63  3   -114.0618808227  5.3957E-07 -2.5034E-06  3.3168E-03  1.0000E-04
 mr-sdci # 63  4   -114.0088131193  4.6951E-07  0.0000E+00  3.3529E-02  1.0000E-04
 mr-sdci # 63  5   -113.8865117772  1.1643E-03  0.0000E+00  4.7131E-01  1.0000E-04
 mr-sdci # 63  6   -113.1263923247 -1.8823E-01  0.0000E+00  1.1409E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.04   0.03   0.00   0.04         0.    0.0000
    2    1    0   0.05   0.04   0.00   0.05         0.    0.0000
    3    5    0   0.41   0.17   0.00   0.41         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.01   0.01   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.009998
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.5200s 
time spent in multnx:                   0.5200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.2600s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.5400s 

          starting ci iteration  64

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.98218108     0.00003970     0.00007944     0.00005836     0.01817622     0.02765326    -0.00074251
 ref:   2    -0.00003774    -0.97944424     0.00210578    -0.05924647    -0.00472484     0.05232021     0.08603530
 ref:   3    -0.00001806     0.00133287     0.98048412     0.00778066     0.01350816    -0.01264520     0.00055215

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 64  1   -114.3534229791  5.9757E-12  0.0000E+00  6.0256E-05  1.0000E-04
 mr-sdci # 64  2   -114.2420382590  1.5774E-12  0.0000E+00  8.0567E-05  1.0000E-04
 mr-sdci # 64  3   -114.0618826017  1.7790E-06  1.5087E-06  3.1851E-03  1.0000E-04
 mr-sdci # 64  4   -114.0088144769  1.3576E-06  0.0000E+00  3.3347E-02  1.0000E-04
 mr-sdci # 64  5   -113.8900348818  3.5231E-03  0.0000E+00  4.6712E-01  1.0000E-04
 mr-sdci # 64  6   -113.1605896777  3.4197E-02  0.0000E+00  1.1598E+00  1.0000E-04
 mr-sdci # 64  7   -112.5274971193 -1.3391E-01  0.0000E+00  1.4037E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.09   0.05   0.00   0.09         0.    0.0000
    2    1    0   0.05   0.04   0.00   0.05         0.    0.0000
    3    5    0   0.41   0.19   0.00   0.41         0.    0.0000
    4   75    0   0.01   0.00   0.00   0.01         0.    0.0000
    5   45    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.5700s 
time spent in multnx:                   0.5700s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.2800s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.5900s 

          starting ci iteration  65

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.98218112     0.00003968     0.00011485     0.00011558     0.01488208     0.04091692     0.00109953     0.01080636
 ref:   2    -0.00003772    -0.97944423     0.00209052    -0.05926650    -0.00404966     0.01121062    -0.10239117    -0.04429401
 ref:   3    -0.00001805     0.00133288     0.98046627     0.00773014     0.01498723    -0.01577275     0.00008557    -0.00149797

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 65  1   -114.3534229791  8.3844E-13  0.0000E+00  6.0204E-05  1.0000E-04
 mr-sdci # 65  2   -114.2420382590  1.4921E-13  0.0000E+00  8.0541E-05  1.0000E-04
 mr-sdci # 65  3   -114.0618832224  6.2064E-07 -1.2380E-06  3.3220E-03  1.0000E-04
 mr-sdci # 65  4   -114.0088160181  1.5412E-06  0.0000E+00  3.3241E-02  1.0000E-04
 mr-sdci # 65  5   -113.8926971467  2.6623E-03  0.0000E+00  4.6658E-01  1.0000E-04
 mr-sdci # 65  6   -113.5724922729  4.1190E-01  0.0000E+00  5.5816E-01  1.0000E-04
 mr-sdci # 65  7   -112.5518178818  2.4321E-02  0.0000E+00  1.3179E+00  1.0000E-04
 mr-sdci # 65  8   -112.1449531713  2.3188E-01  0.0000E+00  1.6647E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.09   0.04   0.00   0.09         0.    0.0000
    2    1    0   0.05   0.04   0.00   0.05         0.    0.0000
    3    5    0   0.40   0.17   0.00   0.40         0.    0.0000
    4   75    0   0.01   0.00   0.00   0.01         0.    0.0000
    5   45    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.030001
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.5600s 
time spent in multnx:                   0.5600s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.2500s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.5900s 

          starting ci iteration  66

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.98218108    -0.00003972    -0.00006429    -0.00004289    -0.01691766     0.05726491
 ref:   2     0.00003774     0.97944425    -0.00211049     0.05923651     0.00492768    -0.02426946
 ref:   3     0.00001805    -0.00133287    -0.98046822    -0.00771418    -0.01496661    -0.00480510

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 66  1   -114.3534229791  6.0396E-13  0.0000E+00  6.0225E-05  1.0000E-04
 mr-sdci # 66  2   -114.2420382590  6.1107E-13  0.0000E+00  8.0569E-05  1.0000E-04
 mr-sdci # 66  3   -114.0618839000  6.7766E-07  1.5630E-06  3.0739E-03  1.0000E-04
 mr-sdci # 66  4   -114.0088172900  1.2719E-06  0.0000E+00  3.3300E-02  1.0000E-04
 mr-sdci # 66  5   -113.8937002633  1.0031E-03  0.0000E+00  4.5852E-01  1.0000E-04
 mr-sdci # 66  6   -113.0914448505 -4.8105E-01  0.0000E+00  1.1360E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.09   0.04   0.00   0.09         0.    0.0000
    2    1    0   0.05   0.04   0.00   0.05         0.    0.0000
    3    5    0   0.38   0.15   0.00   0.38         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.00   0.00   0.00   0.00         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.5300s 
time spent in multnx:                   0.5300s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.2400s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.5500s 

          starting ci iteration  67

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.98218103    -0.00003975    -0.00002156     0.00001123    -0.01865683    -0.06047938     0.02362351
 ref:   2     0.00003781     0.97944429    -0.00216820     0.05916528     0.00773098     0.03075584    -0.05209333
 ref:   3     0.00001804    -0.00133288    -0.98045016    -0.00765681    -0.01594902     0.00327085     0.01286013

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 67  1   -114.3534229791  3.0838E-12  0.0000E+00  6.0225E-05  1.0000E-04
 mr-sdci # 67  2   -114.2420382590  1.0019E-12  0.0000E+00  8.0804E-05  1.0000E-04
 mr-sdci # 67  3   -114.0618854362  1.5362E-06 -1.8399E-06  2.5894E-03  1.0000E-04
 mr-sdci # 67  4   -114.0088193196  2.0296E-06  0.0000E+00  3.3465E-02  1.0000E-04
 mr-sdci # 67  5   -113.8968738740  3.1736E-03  0.0000E+00  4.3957E-01  1.0000E-04
 mr-sdci # 67  6   -113.1002704165  8.8256E-03  0.0000E+00  1.0751E+00  1.0000E-04
 mr-sdci # 67  7   -112.4937485210 -5.8069E-02  0.0000E+00  1.5518E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.08   0.04   0.00   0.08         0.    0.0000
    2    1    0   0.05   0.04   0.00   0.05         0.    0.0000
    3    5    0   0.41   0.17   0.00   0.41         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.019999
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.5600s 
time spent in multnx:                   0.5600s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.2600s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.5800s 

          starting ci iteration  68

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.98218102    -0.00003977     0.00000223     0.00004695    -0.01945220    -0.05980654    -0.03067225    -0.01602708
 ref:   2     0.00003779     0.97944424    -0.00212017     0.05923227     0.00620191    -0.01942455     0.09437393    -0.05031458
 ref:   3     0.00001804    -0.00133287    -0.98045016    -0.00764506    -0.01597959     0.00431325    -0.00848266    -0.00931869

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 68  1   -114.3534229791  1.4211E-13  0.0000E+00  6.0244E-05  1.0000E-04
 mr-sdci # 68  2   -114.2420382590  4.1922E-13  0.0000E+00  8.0609E-05  1.0000E-04
 mr-sdci # 68  3   -114.0618858614  4.2517E-07  9.8959E-07  2.1624E-03  1.0000E-04
 mr-sdci # 68  4   -114.0088201472  8.2759E-07  0.0000E+00  3.3290E-02  1.0000E-04
 mr-sdci # 68  5   -113.8972628625  3.8899E-04  0.0000E+00  4.3455E-01  1.0000E-04
 mr-sdci # 68  6   -113.2669062515  1.6664E-01  0.0000E+00  9.0499E-01  1.0000E-04
 mr-sdci # 68  7   -112.6669823503  1.7323E-01  0.0000E+00  1.2913E+00  1.0000E-04
 mr-sdci # 68  8   -112.0504923420 -9.4461E-02  0.0000E+00  1.6264E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.09   0.04   0.00   0.09         0.    0.0000
    2    1    0   0.05   0.04   0.00   0.05         0.    0.0000
    3    5    0   0.40   0.18   0.00   0.40         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.01   0.01   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.019999
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.5600s 
time spent in multnx:                   0.5600s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.2800s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.5900s 

          starting ci iteration  69

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.98218106     0.00003977     0.00001396    -0.00003476     0.01802066    -0.02980681
 ref:   2    -0.00003787    -0.97944426     0.00216118    -0.05919803    -0.00921205    -0.06131997
 ref:   3    -0.00001804     0.00133287     0.98044296     0.00763262     0.01648610     0.00709534

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 69  1   -114.3534229791  2.1458E-12  0.0000E+00  6.0100E-05  1.0000E-04
 mr-sdci # 69  2   -114.2420382590  8.5265E-14  0.0000E+00  8.0666E-05  1.0000E-04
 mr-sdci # 69  3   -114.0618862434  3.8207E-07 -1.4983E-06  2.4188E-03  1.0000E-04
 mr-sdci # 69  4   -114.0088203877  2.4048E-07  0.0000E+00  3.3366E-02  1.0000E-04
 mr-sdci # 69  5   -113.8990079022  1.7450E-03  0.0000E+00  4.3694E-01  1.0000E-04
 mr-sdci # 69  6   -113.1647177822 -1.0219E-01  0.0000E+00  1.0856E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.09   0.05   0.00   0.09         0.    0.0000
    2    1    0   0.05   0.04   0.00   0.05         0.    0.0000
    3    5    0   0.40   0.18   0.00   0.40         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.5600s 
time spent in multnx:                   0.5600s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.2800s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.5800s 

          starting ci iteration  70

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.98218107     0.00003976     0.00001730     0.00003444     0.01726043     0.03103461    -0.00062493
 ref:   2    -0.00003774    -0.97944424     0.00210669     0.05923396    -0.00546333     0.04039823     0.09320987
 ref:   3    -0.00001804     0.00133287     0.98043661    -0.00761413     0.01688259    -0.00654362    -0.00287842

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 70  1   -114.3534229791  5.8407E-12  0.0000E+00  5.9916E-05  1.0000E-04
 mr-sdci # 70  2   -114.2420382590  1.7053E-13  0.0000E+00  8.0519E-05  1.0000E-04
 mr-sdci # 70  3   -114.0618871956  9.5216E-07  8.4602E-07  2.3403E-03  1.0000E-04
 mr-sdci # 70  4   -114.0088208258  4.3811E-07  0.0000E+00  3.3262E-02  1.0000E-04
 mr-sdci # 70  5   -113.9030149911  4.0071E-03  0.0000E+00  4.3170E-01  1.0000E-04
 mr-sdci # 70  6   -113.2022212326  3.7503E-02  0.0000E+00  1.0891E+00  1.0000E-04
 mr-sdci # 70  7   -112.5278743794 -1.3911E-01  0.0000E+00  1.3849E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.10   0.05   0.00   0.10         0.    0.0000
    2    1    0   0.05   0.04   0.00   0.05         0.    0.0000
    3    5    0   0.41   0.17   0.00   0.41         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.009998
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.5800s 
time spent in multnx:                   0.5800s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.2700s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6000s 

          starting ci iteration  71

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.98218109     0.00003975     0.00004079     0.00000953     0.01452248     0.04231363     0.00095573     0.01006830
 ref:   2    -0.00003773    -0.97944423     0.00209464    -0.05925352    -0.00458502     0.00772351    -0.10489087    -0.03587288
 ref:   3    -0.00001803     0.00133288     0.98042592     0.00758055     0.01785265    -0.01073012     0.00183459    -0.00714092

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 71  1   -114.3534229791  4.7606E-13  0.0000E+00  5.9870E-05  1.0000E-04
 mr-sdci # 71  2   -114.2420382590  6.3949E-14  0.0000E+00  8.0508E-05  1.0000E-04
 mr-sdci # 71  3   -114.0618874989  3.0326E-07 -6.7193E-07  2.3974E-03  1.0000E-04
 mr-sdci # 71  4   -114.0088218533  1.0274E-06  0.0000E+00  3.3167E-02  1.0000E-04
 mr-sdci # 71  5   -113.9051977855  2.1828E-03  0.0000E+00  4.2957E-01  1.0000E-04
 mr-sdci # 71  6   -113.5378880237  3.3567E-01  0.0000E+00  5.7500E-01  1.0000E-04
 mr-sdci # 71  7   -112.5460550081  1.8181E-02  0.0000E+00  1.3164E+00  1.0000E-04
 mr-sdci # 71  8   -112.1619982451  1.1151E-01  0.0000E+00  1.6409E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.09   0.05   0.00   0.09         0.    0.0000
    2    1    0   0.05   0.04   0.00   0.05         0.    0.0000
    3    5    0   0.42   0.20   0.00   0.42         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.02   0.02   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.029999
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.5800s 
time spent in multnx:                   0.5800s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6100s 

          starting ci iteration  72

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.98218106    -0.00003977    -0.00000814     0.00003759    -0.01637312     0.05562095
 ref:   2     0.00003774     0.97944424    -0.00210849     0.05923297     0.00542393    -0.02485487
 ref:   3     0.00001803    -0.00133288    -0.98042591    -0.00757096    -0.01784850    -0.00357516

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 72  1   -114.3534229791  5.9686E-13  0.0000E+00  5.9890E-05  1.0000E-04
 mr-sdci # 72  2   -114.2420382590  1.2079E-13  0.0000E+00  8.0512E-05  1.0000E-04
 mr-sdci # 72  3   -114.0618878199  3.2104E-07  8.6179E-07  2.2629E-03  1.0000E-04
 mr-sdci # 72  4   -114.0088224569  6.0362E-07  0.0000E+00  3.3216E-02  1.0000E-04
 mr-sdci # 72  5   -113.9061787431  9.8096E-04  0.0000E+00  4.2180E-01  1.0000E-04
 mr-sdci # 72  6   -113.0113834631 -5.2650E-01  0.0000E+00  1.1928E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.09   0.05   0.00   0.09         0.    0.0000
    2    1    0   0.06   0.03   0.00   0.05         0.    0.0000
    3    5    0   0.40   0.14   0.00   0.40         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.019999
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.5700s 
time spent in multnx:                   0.5600s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.2300s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.5900s 

          starting ci iteration  73

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.98218102    -0.00003979     0.00002384     0.00007864    -0.01808793    -0.06064232     0.01651434
 ref:   2     0.00003782     0.97944426    -0.00215864     0.05917141     0.00856901     0.03802102    -0.05579853
 ref:   3     0.00001802    -0.00133288    -0.98040798    -0.00752868    -0.01893242    -0.00011788     0.01690734

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 73  1   -114.3534229791  2.0748E-12  0.0000E+00  5.9852E-05  1.0000E-04
 mr-sdci # 73  2   -114.2420382590  2.7001E-13  0.0000E+00  8.0649E-05  1.0000E-04
 mr-sdci # 73  3   -114.0618886474  8.2749E-07 -1.1625E-06  1.9058E-03  1.0000E-04
 mr-sdci # 73  4   -114.0088235579  1.1011E-06  0.0000E+00  3.3361E-02  1.0000E-04
 mr-sdci # 73  5   -113.9091299859  2.9512E-03  0.0000E+00  4.0123E-01  1.0000E-04
 mr-sdci # 73  6   -113.0346235530  2.3240E-02  0.0000E+00  1.0919E+00  1.0000E-04
 mr-sdci # 73  7   -112.5539014353  7.8464E-03  0.0000E+00  1.5595E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.09   0.05   0.00   0.09         0.    0.0000
    2    1    0   0.05   0.03   0.00   0.05         0.    0.0000
    3    5    0   0.42   0.16   0.00   0.42         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.5800s 
time spent in multnx:                   0.5800s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.2500s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.5900s 

          starting ci iteration  74

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.98218100    -0.00003980     0.00004751     0.00011571    -0.01929075    -0.06214526    -0.02667214    -0.01310197
 ref:   2     0.00003778     0.97944424    -0.00212064     0.05922599     0.00663092    -0.01651041     0.09404422    -0.04964355
 ref:   3     0.00001802    -0.00133288    -0.98040412    -0.00751292    -0.01912803    -0.00010949    -0.01122523    -0.01467475

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 74  1   -114.3534229791  3.1974E-13  0.0000E+00  5.9874E-05  1.0000E-04
 mr-sdci # 74  2   -114.2420382590  1.7053E-13  0.0000E+00  8.0531E-05  1.0000E-04
 mr-sdci # 74  3   -114.0618889693  3.2194E-07  5.5315E-07  1.5093E-03  1.0000E-04
 mr-sdci # 74  4   -114.0088242304  6.7244E-07  0.0000E+00  3.3222E-02  1.0000E-04
 mr-sdci # 74  5   -113.9098835423  7.5356E-04  0.0000E+00  3.9223E-01  1.0000E-04
 mr-sdci # 74  6   -113.2201389739  1.8552E-01  0.0000E+00  9.1681E-01  1.0000E-04
 mr-sdci # 74  7   -112.6893256201  1.3542E-01  0.0000E+00  1.3172E+00  1.0000E-04
 mr-sdci # 74  8   -112.1058163046 -5.6182E-02  0.0000E+00  1.5991E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.09   0.04   0.00   0.09         0.    0.0000
    2    1    0   0.05   0.04   0.00   0.05         0.    0.0000
    3    5    0   0.40   0.18   0.00   0.40         0.    0.0000
    4   75    0   0.01   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.019999
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.5700s 
time spent in multnx:                   0.5600s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.2700s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6000s 

          starting ci iteration  75

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.98218103    -0.00003980    -0.00003269     0.00010854     0.01770571    -0.03177654
 ref:   2     0.00003784     0.97944423     0.00215159     0.05920914    -0.00933450    -0.05307411
 ref:   3     0.00001801    -0.00133288     0.98040050    -0.00750763     0.01934453     0.00116102

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 75  1   -114.3534229791  1.4069E-12  0.0000E+00  5.9772E-05  1.0000E-04
 mr-sdci # 75  2   -114.2420382590  2.1316E-14  0.0000E+00  8.0524E-05  1.0000E-04
 mr-sdci # 75  3   -114.0618892430  2.7368E-07 -8.5215E-07  1.7157E-03  1.0000E-04
 mr-sdci # 75  4   -114.0088243031  7.2726E-08  0.0000E+00  3.3258E-02  1.0000E-04
 mr-sdci # 75  5   -113.9117206422  1.8371E-03  0.0000E+00  3.9663E-01  1.0000E-04
 mr-sdci # 75  6   -113.1941056367 -2.6033E-02  0.0000E+00  1.0249E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.08   0.05   0.00   0.08         0.    0.0000
    2    1    0   0.05   0.04   0.00   0.05         0.    0.0000
    3    5    0   0.43   0.19   0.00   0.43         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.5800s 
time spent in multnx:                   0.5800s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.2900s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.5900s 

          starting ci iteration  76

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.98218104    -0.00003980    -0.00003194     0.00010888     0.01725308     0.03211140     0.00247594
 ref:   2     0.00003775     0.97944423     0.00211122     0.05921983    -0.00590946     0.03424128     0.09428903
 ref:   3     0.00001802    -0.00133288     0.98039964    -0.00750455     0.01927214     0.00039334    -0.00706839

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 76  1   -114.3534229791  2.4230E-12  0.0000E+00  5.9720E-05  1.0000E-04
 mr-sdci # 76  2   -114.2420382590 -2.8422E-14  0.0000E+00  8.0528E-05  1.0000E-04
 mr-sdci # 76  3   -114.0618896833  4.4028E-07  4.8269E-07  1.6390E-03  1.0000E-04
 mr-sdci # 76  4   -114.0088243350  3.1849E-08  0.0000E+00  3.3229E-02  1.0000E-04
 mr-sdci # 76  5   -113.9144714329  2.7508E-03  0.0000E+00  3.9568E-01  1.0000E-04
 mr-sdci # 76  6   -113.2230539830  2.8948E-02  0.0000E+00  1.0194E+00  1.0000E-04
 mr-sdci # 76  7   -112.5468238116 -1.4250E-01  0.0000E+00  1.3790E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.09   0.06   0.00   0.09         0.    0.0000
    2    1    0   0.05   0.04   0.00   0.05         0.    0.0000
    3    5    0   0.41   0.18   0.00   0.41         0.    0.0000
    4   75    0   0.02   0.01   0.00   0.02         0.    0.0000
    5   45    0   0.01   0.01   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.5800s 
time spent in multnx:                   0.5800s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6000s 

          starting ci iteration  77

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.98218106    -0.00003981    -0.00001414    -0.00007214     0.01504933     0.04324068    -0.00196478     0.01433362
 ref:   2     0.00003773     0.97944424     0.00209995    -0.05924108    -0.00490679     0.00398363    -0.10371154    -0.04085062
 ref:   3     0.00001801    -0.00133288     0.98039226     0.00748002     0.01992155    -0.00575600     0.00475686    -0.01496465

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 77  1   -114.3534229791  3.3396E-13  0.0000E+00  5.9672E-05  1.0000E-04
 mr-sdci # 77  2   -114.2420382590  2.1316E-14  0.0000E+00  8.0530E-05  1.0000E-04
 mr-sdci # 77  3   -114.0618898565  1.7317E-07 -3.7443E-07  1.6635E-03  1.0000E-04
 mr-sdci # 77  4   -114.0088250573  7.2233E-07  0.0000E+00  3.3145E-02  1.0000E-04
 mr-sdci # 77  5   -113.9159916113  1.5202E-03  0.0000E+00  3.9245E-01  1.0000E-04
 mr-sdci # 77  6   -113.5158478947  2.9279E-01  0.0000E+00  5.7783E-01  1.0000E-04
 mr-sdci # 77  7   -112.5617675791  1.4944E-02  0.0000E+00  1.3277E+00  1.0000E-04
 mr-sdci # 77  8   -112.0362073816 -6.9609E-02  0.0000E+00  1.5585E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.09   0.05   0.00   0.09         0.    0.0000
    2    1    0   0.05   0.04   0.00   0.05         0.    0.0000
    3    5    0   0.41   0.16   0.00   0.41         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.5700s 
time spent in multnx:                   0.5700s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.2600s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.6000s 

          starting ci iteration  78

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.98218103     0.00003981     0.00003839     0.00010210    -0.01671601     0.05893328
 ref:   2    -0.00003775    -0.97944424    -0.00211210     0.05922594     0.00578274    -0.03072045
 ref:   3    -0.00001802     0.00133288    -0.98039219    -0.00747509    -0.01989219    -0.00399824

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 78  1   -114.3534229791  3.6238E-13  0.0000E+00  5.9691E-05  1.0000E-04
 mr-sdci # 78  2   -114.2420382590 -1.4211E-14  0.0000E+00  8.0530E-05  1.0000E-04
 mr-sdci # 78  3   -114.0618900303  1.7381E-07  4.6140E-07  1.5540E-03  1.0000E-04
 mr-sdci # 78  4   -114.0088252965  2.3922E-07  0.0000E+00  3.3181E-02  1.0000E-04
 mr-sdci # 78  5   -113.9167923585  8.0075E-04  0.0000E+00  3.8618E-01  1.0000E-04
 mr-sdci # 78  6   -112.9075799978 -6.0827E-01  0.0000E+00  1.2204E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.08   0.05   0.00   0.08         0.    0.0000
    2    1    0   0.05   0.04   0.00   0.05         0.    0.0000
    3    5    0   0.43   0.19   0.00   0.43         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.5800s 
time spent in multnx:                   0.5800s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.2900s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.5900s 

          starting ci iteration  79

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.98218101    -0.00003981     0.00005488     0.00012153    -0.01776237    -0.06166523    -0.00641688
 ref:   2     0.00003779     0.97944424    -0.00214785     0.05918678     0.00844622     0.05451346    -0.04622780
 ref:   3     0.00001800    -0.00133288    -0.98037816    -0.00745117    -0.02085426    -0.00481913     0.02051477

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 79  1   -114.3534229791  7.4607E-13  0.0000E+00  5.9640E-05  1.0000E-04
 mr-sdci # 79  2   -114.2420382590  3.5527E-14  0.0000E+00  8.0512E-05  1.0000E-04
 mr-sdci # 79  3   -114.0618903888  3.5855E-07 -6.3338E-07  1.3467E-03  1.0000E-04
 mr-sdci # 79  4   -114.0088256839  3.8740E-07  0.0000E+00  3.3281E-02  1.0000E-04
 mr-sdci # 79  5   -113.9186075028  1.8151E-03  0.0000E+00  3.6833E-01  1.0000E-04
 mr-sdci # 79  6   -112.9634513879  5.5871E-02  0.0000E+00  1.0917E+00  1.0000E-04
 mr-sdci # 79  7   -112.6328456376  7.1078E-02  0.0000E+00  1.5541E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.09   0.05   0.00   0.09         0.    0.0000
    2    1    0   0.05   0.04   0.00   0.05         0.    0.0000
    3    5    0   0.42   0.18   0.00   0.42         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.01   0.01   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.5800s 
time spent in multnx:                   0.5800s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.2900s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.6000s 

          starting ci iteration  80

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.98218099     0.00003981     0.00007574     0.00015044    -0.01917630    -0.06565495    -0.01674064     0.00074721
 ref:   2    -0.00003776    -0.97944423    -0.00211985     0.05922196     0.00651277    -0.01107842     0.08833353    -0.06155939
 ref:   3    -0.00001800     0.00133288    -0.98037224    -0.00743700    -0.02121599    -0.00435172    -0.01538099    -0.01910267

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 80  1   -114.3534229791  3.5527E-13  0.0000E+00  5.9646E-05  1.0000E-04
 mr-sdci # 80  2   -114.2420382590  0.0000E+00  0.0000E+00  8.0488E-05  1.0000E-04
 mr-sdci # 80  3   -114.0618906022  2.1333E-07  2.8377E-07  1.0067E-03  1.0000E-04
 mr-sdci # 80  4   -114.0088260278  3.4387E-07  0.0000E+00  3.3198E-02  1.0000E-04
 mr-sdci # 80  5   -113.9195255519  9.1805E-04  0.0000E+00  3.5567E-01  1.0000E-04
 mr-sdci # 80  6   -113.1883663769  2.2491E-01  0.0000E+00  9.2081E-01  1.0000E-04
 mr-sdci # 80  7   -112.7493516659  1.1651E-01  0.0000E+00  1.3559E+00  1.0000E-04
 mr-sdci # 80  8   -112.0800378703  4.3830E-02  0.0000E+00  1.5168E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.09   0.05   0.00   0.09         0.    0.0000
    2    1    0   0.05   0.04   0.00   0.05         0.    0.0000
    3    5    0   0.41   0.17   0.00   0.41         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.01   0.01   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.5700s 
time spent in multnx:                   0.5700s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.2800s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.6100s 

          starting ci iteration  81

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.98218100     0.00003982    -0.00006291     0.00014505     0.01777284    -0.03263267
 ref:   2    -0.00003778    -0.97944421     0.00214093     0.05921209    -0.00845623    -0.04437852
 ref:   3    -0.00001800     0.00133288     0.98037071    -0.00743436     0.02125406    -0.00172375

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 81  1   -114.3534229791  2.4158E-13  0.0000E+00  5.9621E-05  1.0000E-04
 mr-sdci # 81  2   -114.2420382590  1.0658E-13  0.0000E+00  8.0436E-05  1.0000E-04
 mr-sdci # 81  3   -114.0618907724  1.7022E-07 -4.0517E-07  1.1566E-03  1.0000E-04
 mr-sdci # 81  4   -114.0088260611  3.3294E-08  0.0000E+00  3.3218E-02  1.0000E-04
 mr-sdci # 81  5   -113.9207975084  1.2720E-03  0.0000E+00  3.6251E-01  1.0000E-04
 mr-sdci # 81  6   -113.2493496171  6.0983E-02  0.0000E+00  9.7516E-01  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.09   0.05   0.00   0.09         0.    0.0000
    2    1    0   0.06   0.05   0.00   0.06         0.    0.0000
    3    5    0   0.42   0.19   0.00   0.42         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.02   0.02   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010002
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.5900s 
time spent in multnx:                   0.5900s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6000s 

          starting ci iteration  82

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.98218100     0.00003982    -0.00006415     0.00014565     0.01770057     0.03194515     0.00753344
 ref:   2    -0.00003774    -0.97944423     0.00211487     0.05921926    -0.00608471     0.03055661     0.09329805
 ref:   3    -0.00001800     0.00133288     0.98037127    -0.00743336     0.02111240     0.00296218    -0.00755905

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 82  1   -114.3534229791  3.2685E-13  0.0000E+00  5.9635E-05  1.0000E-04
 mr-sdci # 82  2   -114.2420382590  6.3949E-14  0.0000E+00  8.0530E-05  1.0000E-04
 mr-sdci # 82  3   -114.0618909329  1.6053E-07  2.5250E-07  1.0952E-03  1.0000E-04
 mr-sdci # 82  4   -114.0088260733  1.2258E-08  0.0000E+00  3.3199E-02  1.0000E-04
 mr-sdci # 82  5   -113.9219781920  1.1807E-03  0.0000E+00  3.6574E-01  1.0000E-04
 mr-sdci # 82  6   -113.2661014458  1.6752E-02  0.0000E+00  9.7445E-01  1.0000E-04
 mr-sdci # 82  7   -112.5368672925 -2.1248E-01  0.0000E+00  1.3845E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.12   0.07   0.00   0.12         0.    0.0000
    2    1    0   0.05   0.04   0.00   0.05         0.    0.0000
    3    5    0   0.45   0.20   0.00   0.45         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.03   0.01   0.00   0.03         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010002
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6500s 
time spent in multnx:                   0.6500s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3200s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6600s 

          starting ci iteration  83

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.98218100    -0.00003984    -0.00004923    -0.00012945     0.01598938     0.04370824    -0.00551169     0.02018172
 ref:   2    -0.00003774     0.97944425     0.00210398    -0.05923048    -0.00510098     0.00008159    -0.10485444    -0.03784520
 ref:   3    -0.00001800    -0.00133288     0.98036694     0.00742558     0.02144773    -0.00200306     0.00454830    -0.01442074

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 83  1   -114.3534229791  2.1316E-14  0.0000E+00  5.9642E-05  1.0000E-04
 mr-sdci # 83  2   -114.2420382590  2.9843E-13  0.0000E+00  8.0556E-05  1.0000E-04
 mr-sdci # 83  3   -114.0618910409  1.0803E-07 -2.0182E-07  1.1073E-03  1.0000E-04
 mr-sdci # 83  4   -114.0088261963  1.2295E-07  0.0000E+00  3.3162E-02  1.0000E-04
 mr-sdci # 83  5   -113.9228464290  8.6824E-04  0.0000E+00  3.6261E-01  1.0000E-04
 mr-sdci # 83  6   -113.5256350874  2.5953E-01  0.0000E+00  5.6633E-01  1.0000E-04
 mr-sdci # 83  7   -112.5619069253  2.5040E-02  0.0000E+00  1.3230E+00  1.0000E-04
 mr-sdci # 83  8   -112.0195949630 -6.0443E-02  0.0000E+00  1.5280E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.10   0.05   0.00   0.10         0.    0.0000
    2    1    0   0.05   0.04   0.00   0.05         0.    0.0000
    3    5    0   0.45   0.17   0.00   0.45         0.    0.0000
    4   75    0   0.01   0.00   0.00   0.01         0.    0.0000
    5   45    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.030003
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6200s 
time spent in multnx:                   0.6200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.2600s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6500s 

          starting ci iteration  84

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.98218099     0.00003983     0.00006879    -0.00014172    -0.01756641     0.06421619
 ref:   2     0.00003775    -0.97944424    -0.00211532    -0.05922341     0.00604520    -0.03837503
 ref:   3     0.00001800     0.00133288    -0.98036579     0.00742331    -0.02149642    -0.00050575

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 84  1   -114.3534229791  7.1054E-15  0.0000E+00  5.9651E-05  1.0000E-04
 mr-sdci # 84  2   -114.2420382590  7.1054E-15  0.0000E+00  8.0551E-05  1.0000E-04
 mr-sdci # 84  3   -114.0618911429  1.0191E-07  2.1990E-07  9.9740E-04  1.0000E-04
 mr-sdci # 84  4   -114.0088262325  3.6184E-08  0.0000E+00  3.3182E-02  1.0000E-04
 mr-sdci # 84  5   -113.9234979160  6.5149E-04  0.0000E+00  3.5768E-01  1.0000E-04
 mr-sdci # 84  6   -112.8360313663 -6.8960E-01  0.0000E+00  1.2199E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.09   0.05   0.00   0.09         0.    0.0000
    2    1    0   0.07   0.04   0.00   0.07         0.    0.0000
    3    5    0   0.42   0.18   0.00   0.42         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.02   0.02   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010002
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6000s 
time spent in multnx:                   0.6000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.2900s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6100s 

          starting ci iteration  85

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.98218099     0.00003983     0.00007395     0.00014663    -0.01790941    -0.05562509    -0.03245423
 ref:   2     0.00003776    -0.97944422    -0.00213672     0.05920539     0.00776025     0.06878623    -0.03095110
 ref:   3     0.00001800     0.00133287    -0.98035862    -0.00741488    -0.02202455    -0.01043518     0.01572357

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 85  1   -114.3534229791  2.8422E-14  0.0000E+00  5.9637E-05  1.0000E-04
 mr-sdci # 85  2   -114.2420382590  1.9895E-13  0.0000E+00  8.0418E-05  1.0000E-04
 mr-sdci # 85  3   -114.0618912633  1.2043E-07 -2.7863E-07  9.0233E-04  1.0000E-04
 mr-sdci # 85  4   -114.0088263111  7.8634E-08  0.0000E+00  3.3228E-02  1.0000E-04
 mr-sdci # 85  5   -113.9241964625  6.9855E-04  0.0000E+00  3.4592E-01  1.0000E-04
 mr-sdci # 85  6   -112.9348317840  9.8800E-02  0.0000E+00  1.1319E+00  1.0000E-04
 mr-sdci # 85  7   -112.6376421869  7.5735E-02  0.0000E+00  1.5121E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.11   0.05   0.00   0.10         0.    0.0000
    2    1    0   0.05   0.04   0.00   0.05         0.    0.0000
    3    5    0   0.43   0.16   0.00   0.43         0.    0.0000
    4   75    0   0.01   0.00   0.00   0.01         0.    0.0000
    5   45    0   0.01   0.01   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6100s 
time spent in multnx:                   0.6000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.2600s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6300s 

          starting ci iteration  86

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.98218097     0.00003982     0.00008964     0.00016001    -0.01925981    -0.06734746    -0.00121272     0.01497151
 ref:   2     0.00003774    -0.97944423    -0.00211827     0.05921964     0.00615323    -0.00346213     0.08620552    -0.06525503
 ref:   3     0.00001800     0.00133287    -0.98035357    -0.00740847    -0.02240391    -0.00905615    -0.01427900    -0.01558545

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 86  1   -114.3534229791  7.8160E-14  0.0000E+00  5.9633E-05  1.0000E-04
 mr-sdci # 86  2   -114.2420382590 -2.8422E-14  0.0000E+00  8.0452E-05  1.0000E-04
 mr-sdci # 86  3   -114.0618913778  1.1447E-07  1.2920E-07  6.4390E-04  1.0000E-04
 mr-sdci # 86  4   -114.0088263806  6.9461E-08  0.0000E+00  3.3197E-02  1.0000E-04
 mr-sdci # 86  5   -113.9249831158  7.8665E-04  0.0000E+00  3.3203E-01  1.0000E-04
 mr-sdci # 86  6   -113.1797119515  2.4488E-01  0.0000E+00  9.3711E-01  1.0000E-04
 mr-sdci # 86  7   -112.7760553518  1.3841E-01  0.0000E+00  1.3661E+00  1.0000E-04
 mr-sdci # 86  8   -112.1101881932  9.0593E-02  0.0000E+00  1.4824E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.10   0.07   0.00   0.10         0.    0.0000
    2    1    0   0.06   0.05   0.00   0.06         0.    0.0000
    3    5    0   0.42   0.17   0.00   0.42         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.01   0.01   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.020000
time for cinew                         0.009998
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6000s 
time spent in multnx:                   0.6000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6300s 

          starting ci iteration  87

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.98218097    -0.00003985    -0.00007831    -0.00014928     0.01839942    -0.03472050
 ref:   2     0.00003773     0.97944420     0.00213144    -0.05920640    -0.00707782    -0.03683318
 ref:   3     0.00001800    -0.00133287     0.98035281     0.00740559     0.02243280    -0.00064005

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 87  1   -114.3534229791  5.6843E-14  0.0000E+00  5.9643E-05  1.0000E-04
 mr-sdci # 87  2   -114.2420382590  6.3949E-13  0.0000E+00  8.0313E-05  1.0000E-04
 mr-sdci # 87  3   -114.0618914683  9.0506E-08 -1.7799E-07  7.5461E-04  1.0000E-04
 mr-sdci # 87  4   -114.0088264622  8.1687E-08  0.0000E+00  3.3222E-02  1.0000E-04
 mr-sdci # 87  5   -113.9253532241  3.7011E-04  0.0000E+00  3.3733E-01  1.0000E-04
 mr-sdci # 87  6   -113.3305473524  1.5084E-01  0.0000E+00  9.3016E-01  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.11   0.08   0.00   0.11         0.    0.0000
    2    1    0   0.06   0.04   0.00   0.06         0.    0.0000
    3    5    0   0.41   0.16   0.00   0.41         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.01   0.01   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010002
time for cinew                         0.009998
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6000s 
time spent in multnx:                   0.6000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6200s 

          starting ci iteration  88

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.98218097    -0.00003985    -0.00007909    -0.00015094     0.01839876     0.03367621     0.01064806
 ref:   2    -0.00003772     0.97944424     0.00211422    -0.05923385    -0.00591546     0.02468137     0.09586412
 ref:   3    -0.00001800    -0.00133287     0.98035314     0.00740362     0.02239509     0.00128553    -0.00472153

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 88  1   -114.3534229791  7.1054E-14  0.0000E+00  5.9652E-05  1.0000E-04
 mr-sdci # 88  2   -114.2420382590  4.2633E-13  0.0000E+00  8.0550E-05  1.0000E-04
 mr-sdci # 88  3   -114.0618915303  6.1985E-08  1.1449E-07  7.1392E-04  1.0000E-04
 mr-sdci # 88  4   -114.0088266200  1.5773E-07  0.0000E+00  3.3149E-02  1.0000E-04
 mr-sdci # 88  5   -113.9256174186  2.6419E-04  0.0000E+00  3.4034E-01  1.0000E-04
 mr-sdci # 88  6   -113.3447708095  1.4223E-02  0.0000E+00  9.2866E-01  1.0000E-04
 mr-sdci # 88  7   -112.4869290139 -2.8913E-01  0.0000E+00  1.3762E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.10   0.05   0.00   0.10         0.    0.0000
    2    1    0   0.05   0.04   0.00   0.05         0.    0.0000
    3    5    0   0.42   0.19   0.00   0.42         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010002
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6000s 
time spent in multnx:                   0.6000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.3000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6100s 

          starting ci iteration  89

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.98218092     0.00003987    -0.00006810    -0.00016172     0.01749877     0.04457647    -0.00555893     0.02256005
 ref:   2     0.00003776    -0.97944426     0.00210604    -0.05922614    -0.00535567    -0.00310371    -0.10956622    -0.01856013
 ref:   3     0.00001801     0.00133287     0.98035129     0.00740659     0.02250849    -0.00159464     0.00172369    -0.00908973

 trial vector basis is being transformed.  new dimension:   5

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 89  1   -114.3534229791  1.6698E-12  0.0000E+00  5.9741E-05  1.0000E-04
 mr-sdci # 89  2   -114.2420382590  2.2737E-13  0.0000E+00  8.0599E-05  1.0000E-04
 mr-sdci # 89  3   -114.0618915808  5.0584E-08 -9.5353E-08  7.2142E-04  1.0000E-04
 mr-sdci # 89  4   -114.0088266646  4.4650E-08  0.0000E+00  3.3172E-02  1.0000E-04
 mr-sdci # 89  5   -113.9258351145  2.1770E-04  0.0000E+00  3.3919E-01  1.0000E-04
 mr-sdci # 89  6   -113.5645021807  2.1973E-01  0.0000E+00  5.6895E-01  1.0000E-04
 mr-sdci # 89  7   -112.5375922275  5.0663E-02  0.0000E+00  1.2997E+00  1.0000E-04
 mr-sdci # 89  8   -112.1067934858 -3.3947E-03  0.0000E+00  1.5479E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.10   0.05   0.00   0.10         0.    0.0000
    2    1    0   0.05   0.04   0.00   0.05         0.    0.0000
    3    5    0   0.42   0.17   0.00   0.42         0.    0.0000
    4   75    0   0.01   0.01   0.00   0.01         0.    0.0000
    5   45    0   0.02   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.029995
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6000s 
time spent in multnx:                   0.5900s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.2700s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6300s 

          starting ci iteration  90

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     39620 2x:      7168 4x:      1008
All internal counts: zz :     51156 yy:    258340 xx:         0 ww:         0
One-external counts: yz :     91224 yx:         0 yw:         0
Two-external counts: yy :     36316 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:     69668    task #   2:     43243    task #   3:    213410    task #   4:      6068
task #   5:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336
 calctciref: ... reading 
 cirefv                                                       recamt=
         336

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.98218093     0.00003984     0.00008122    -0.00016519    -0.01848894     0.06444142
 ref:   2     0.00003776    -0.97944424    -0.00211410    -0.05922402     0.00597581    -0.04036593
 ref:   3     0.00001801     0.00133287    -0.98035012     0.00740598    -0.02258033     0.00302130

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 90  1   -114.3534229791  3.5527E-14  0.0000E+00  5.9733E-05  1.0000E-04
 mr-sdci # 90  2   -114.2420382590  2.4158E-13  0.0000E+00  8.0560E-05  1.0000E-04
 mr-sdci # 90  3   -114.0618916265  4.5683E-08  9.2763E-08  6.3443E-04  1.0000E-04
 mr-sdci # 90  4   -114.0088266676  2.9318E-09  0.0000E+00  3.3179E-02  1.0000E-04
 mr-sdci # 90  5   -113.9260819290  2.4681E-04  0.0000E+00  3.3675E-01  1.0000E-04
 mr-sdci # 90  6   -112.8760972005 -6.8840E-01  0.0000E+00  1.2236E+00  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   11    0   0.09   0.05   0.00   0.09         0.    0.0000
    2    1    0   0.06   0.04   0.00   0.06         0.    0.0000
    3    5    0   0.44   0.18   0.00   0.44         0.    0.0000
    4   75    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   45    0   0.02   0.01   0.00   0.02         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.009998
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.6100s 
time spent in multnx:                   0.6100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.2800s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6200s 

 mr-sdci  convergence not reached after 90 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 90  1   -114.3534229791  3.5527E-14  0.0000E+00  5.9733E-05  1.0000E-04
 mr-sdci # 90  2   -114.2420382590  2.4158E-13  0.0000E+00  8.0560E-05  1.0000E-04
 mr-sdci # 90  3   -114.0618916265  4.5683E-08  9.2763E-08  6.3443E-04  1.0000E-04
 mr-sdci # 90  4   -114.0088266676  2.9318E-09  0.0000E+00  3.3179E-02  1.0000E-04
 mr-sdci # 90  5   -113.9260819290  2.4681E-04  0.0000E+00  3.3675E-01  1.0000E-04
 mr-sdci # 90  6   -112.8760972005 -6.8840E-01  0.0000E+00  1.2236E+00  1.0000E-04

####################CIUDGINFO####################

   ci vector at position   1 energy= -114.353422979084
   ci vector at position   2 energy= -114.242038259009
   ci vector at position   3 energy= -114.061891626519

################END OF CIUDGINFO################

 
    3 of the   7 expansion vectors are transformed.
    3 of the   6 matrix-vector products are transformed.

    3 expansion eigenvectors written to unit nvfile (= 11)
    3 matrix-vector products written to unit nhvfil (= 10)
 maximum overlap with reference            1 (overlap=  0.982180925026916      )

information on vector: 1from unit 11 written to unit 16filename civout                                                      
 maximum overlap with reference            2 (overlap=  0.979444240664304      )

information on vector: 2from unit 11 written to unit 16filename civout                                                      
 maximum overlap with reference            3 (overlap=  0.980350121392541      )

information on vector: 3from unit 11 written to unit 16filename civout                                                      


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =      -114.3534229791

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7    8    9   10

                                          orbital     1    2    3    4    5    6    7    8    9   10

                                         symmetry   a    a    a    a    a    a    a    a    a    a  

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1 -0.915804                        +-   +-   +-   +-   +-   +-   +-   +-           
 z*  1  1       3  0.067540                        +-   +-   +-   +-   +-   +-   +-   +          - 
 z*  1  1       4  0.020334                        +-   +-   +-   +-   +-   +-   +-        +-      
 z*  1  1       6  0.054438                        +-   +-   +-   +-   +-   +-   +-             +- 
 z*  1  1      15  0.018959                        +-   +-   +-   +-   +-   +-        +-   +-      
 z*  1  1      17  0.017487                        +-   +-   +-   +-   +-   +-        +-        +- 
 z*  1  1      21 -0.273629                        +-   +-   +-   +-   +-   +    +-   +-    -      
 z*  1  1      24 -0.062788                        +-   +-   +-   +-   +-   +    +-    -   +     - 
 z*  1  1      26  0.057058                        +-   +-   +-   +-   +-   +    +-   +     -    - 
 z*  1  1      28 -0.018529                        +-   +-   +-   +-   +-   +    +-         -   +- 
 z*  1  1      41  0.168694                        +-   +-   +-   +-   +-        +-   +-   +-      
 z*  1  1      43  0.020168                        +-   +-   +-   +-   +-        +-   +-        +- 
 z*  1  1      44  0.014803                        +-   +-   +-   +-   +-        +-   +    +-    - 
 z*  1  1      46 -0.017336                        +-   +-   +-   +-   +-        +-        +-   +- 
 z*  1  1      52 -0.016214                        +-   +-   +-   +-   +    +-   +-   +-         - 
 z*  1  1      53  0.012052                        +-   +-   +-   +-   +    +-   +-    -   +-      
 z*  1  1      55 -0.024960                        +-   +-   +-   +-   +    +-   +-    -        +- 
 z*  1  1      72  0.035686                        +-   +-   +-   +-   +     -   +-   +-   +     - 
 z*  1  1      81  0.016079                        +-   +-   +-   +-   +    +    +-   +-    -    - 
 z*  1  1      91  0.016991                        +-   +-   +-   +-        +-   +-   +-   +-      
 z*  1  1      93  0.024169                        +-   +-   +-   +-        +-   +-   +-        +- 
 z*  1  1     110 -0.019548                        +-   +-   +-   +    +-   +-   +-    -        +- 
 z*  1  1     127  0.034024                        +-   +-   +-   +    +-    -   +-   +-   +     - 
 z*  1  1     146 -0.011811                        +-   +-   +-   +     -   +-   +-   +-   +-      
 z*  1  1     176  0.029546                        +-   +-   +-        +-   +-   +-   +-   +-      
 z*  1  1     178  0.015055                        +-   +-   +-        +-   +-   +-   +-        +- 
 y   1  1     348 -0.011124             12( a  )   +-   +-   +-   +-   +-   +-   +-    -           
 y   1  1     354 -0.010287             18( a  )   +-   +-   +-   +-   +-   +-   +-    -           
 y   1  1     371 -0.012997             13( a  )   +-   +-   +-   +-   +-   +-   +-         -      
 y   1  1     383 -0.014262              3( a  )   +-   +-   +-   +-   +-   +-   +-              - 
 y   1  1     392 -0.011212             12( a  )   +-   +-   +-   +-   +-   +-   +-              - 
 y   1  1     404  0.039060              2( a  )   +-   +-   +-   +-   +-   +-    -   +-           
 y   1  1     407 -0.013490              5( a  )   +-   +-   +-   +-   +-   +-    -   +-           
 y   1  1     408  0.022819              6( a  )   +-   +-   +-   +-   +-   +-    -   +-           
 y   1  1     410  0.016266              8( a  )   +-   +-   +-   +-   +-   +-    -   +-           
 y   1  1     413 -0.010960             11( a  )   +-   +-   +-   +-   +-   +-    -   +-           
 y   1  1     441 -0.011150             17( a  )   +-   +-   +-   +-   +-   +-    -   +     -      
 y   1  1     448  0.021194              2( a  )   +-   +-   +-   +-   +-   +-    -   +          - 
 y   1  1     451 -0.016786              5( a  )   +-   +-   +-   +-   +-   +-    -   +          - 
 y   1  1     452  0.028640              6( a  )   +-   +-   +-   +-   +-   +-    -   +          - 
 y   1  1     454  0.016939              8( a  )   +-   +-   +-   +-   +-   +-    -   +          - 
 y   1  1     457 -0.023944             11( a  )   +-   +-   +-   +-   +-   +-    -   +          - 
 y   1  1     462  0.019793             16( a  )   +-   +-   +-   +-   +-   +-    -   +          - 
 y   1  1     813 -0.013043             15( a  )   +-   +-   +-   +-   +-    -   +-   +     -      
 y   1  1     824 -0.014591              4( a  )   +-   +-   +-   +-   +-    -   +-   +          - 
 y   1  1     830  0.025261             10( a  )   +-   +-   +-   +-   +-    -   +-   +          - 
 y   1  1     910  0.010270              2( a  )   +-   +-   +-   +-   +-    -   +    +-    -      
 y   1  1    1284 -0.023981              2( a  )   +-   +-   +-   +-   +-   +     -   +-    -      
 y   1  1    1287  0.017493              5( a  )   +-   +-   +-   +-   +-   +     -   +-    -      
 y   1  1    1288 -0.029906              6( a  )   +-   +-   +-   +-   +-   +     -   +-    -      
 y   1  1    1290 -0.015113              8( a  )   +-   +-   +-   +-   +-   +     -   +-    -      
 y   1  1    1293  0.025743             11( a  )   +-   +-   +-   +-   +-   +     -   +-    -      
 y   1  1    1298 -0.012365             16( a  )   +-   +-   +-   +-   +-   +     -   +-    -      
 y   1  1    1556 -0.031588             10( a  )   +-   +-   +-   +-   +-        +-   +-    -      
 y   1  1    1666 -0.016012             10( a  )   +-   +-   +-   +-   +-        +-   +     -    - 
 y   1  1    1794 -0.011019              6( a  )   +-   +-   +-   +-   +-         -   +    +-    - 
 y   1  1    2037 -0.017601              7( a  )   +-   +-   +-   +-    -   +-   +-   +          - 
 y   1  1    2042 -0.016247             12( a  )   +-   +-   +-   +-    -   +-   +-   +          - 
 y   1  1    2433  0.011714              7( a  )   +-   +-   +-   +-    -   +    +-   +-    -      
 y   1  1    2438  0.014067             12( a  )   +-   +-   +-   +-    -   +    +-   +-    -      
 y   1  1    3099 -0.021970             13( a  )   +-   +-   +-   +-   +    +-   +-    -    -      
 y   1  1    3106  0.013656             20( a  )   +-   +-   +-   +-   +    +-   +-    -    -      
 y   1  1    3120 -0.013917             12( a  )   +-   +-   +-   +-   +    +-   +-    -         - 
 y   1  1    3130 -0.014860             22( a  )   +-   +-   +-   +-   +    +-   +-    -         - 
 y   1  1    3166 -0.015093             14( a  )   +-   +-   +-   +-   +    +-    -   +-    -      
 y   1  1    3169 -0.025304             17( a  )   +-   +-   +-   +-   +    +-    -   +-    -      
 y   1  1    3180 -0.011573              6( a  )   +-   +-   +-   +-   +    +-    -   +-         - 
 y   1  1    3185  0.024469             11( a  )   +-   +-   +-   +-   +    +-    -   +-         - 
 y   1  1    3195 -0.012237             21( a  )   +-   +-   +-   +-   +    +-    -   +-         - 
 y   1  1    3423  0.014570              7( a  )   +-   +-   +-   +-   +     -   +-   +-    -      
 y   1  1    3431 -0.026614             15( a  )   +-   +-   +-   +-   +     -   +-   +-    -      
 y   1  1    3448 -0.018399             10( a  )   +-   +-   +-   +-   +     -   +-   +-         - 
 y   1  1    3451  0.011989             13( a  )   +-   +-   +-   +-   +     -   +-   +-         - 
 y   1  1    3458 -0.011884             20( a  )   +-   +-   +-   +-   +     -   +-   +-         - 
 y   1  1    4086 -0.011370             10( a  )   +-   +-   +-   +-        +-   +-   +-    -      
 y   1  1    4957 -0.015048              1( a  )   +-   +-   +-    -   +-   +-   +-   +-           
 y   1  1    5001 -0.013152              1( a  )   +-   +-   +-    -   +-   +-   +-   +          - 
 y   1  1    5397  0.019798              1( a  )   +-   +-   +-    -   +-   +    +-   +-    -      
 y   1  1    7279  0.015285             13( a  )   +-   +-   +-   +    +-   +-   +-    -    -      
 y   1  1    7291  0.010647              3( a  )   +-   +-   +-   +    +-   +-   +-    -         - 
 y   1  1    7597  0.026242              1( a  )   +-   +-   +-   +    +-    -   +-   +-    -      
 y   1  1    7599  0.012036              3( a  )   +-   +-   +-   +    +-    -   +-   +-    -      
 y   1  1    7601  0.019210              5( a  )   +-   +-   +-   +    +-    -   +-   +-    -      
 y   1  1    7602  0.011343              6( a  )   +-   +-   +-   +    +-    -   +-   +-    -      
 y   1  1    7605  0.011262              9( a  )   +-   +-   +-   +    +-    -   +-   +-    -      
 y   1  1    7614 -0.017036             18( a  )   +-   +-   +-   +    +-    -   +-   +-    -      
 y   1  1    7622  0.012717              4( a  )   +-   +-   +-   +    +-    -   +-   +-         - 
 y   1  1    7631 -0.011178             13( a  )   +-   +-   +-   +    +-    -   +-   +-         - 
 y   1  1   11118  0.010946              2( a  )   +-   +-    -   +-   +-   +-   +-   +-           
 y   1  1   11162  0.019954              2( a  )   +-   +-    -   +-   +-   +-   +-   +          - 
 y   1  1   11168  0.010401              8( a  )   +-   +-    -   +-   +-   +-   +-   +          - 
 y   1  1   11558 -0.019268              2( a  )   +-   +-    -   +-   +-   +    +-   +-    -      
 y   1  1   11562  0.011245              6( a  )   +-   +-    -   +-   +-   +    +-   +-    -      
 y   1  1   13440  0.018835             14( a  )   +-   +-    -   +    +-   +-   +-   +-    -      
 y   1  1   13450 -0.014589              2( a  )   +-   +-    -   +    +-   +-   +-   +-         - 
 y   1  1   13456 -0.015620              8( a  )   +-   +-    -   +    +-   +-   +-   +-         - 
 y   1  1   15760 -0.017111              2( a  )   +-   +-   +    +-   +-    -   +-   +-    -      
 y   1  1   15766 -0.010242              8( a  )   +-   +-   +    +-   +-    -   +-   +-    -      
 y   1  1   17752  0.012046             14( a  )   +-   +-   +     -   +-   +-   +-   +-    -      
 y   1  1   17762 -0.010794              2( a  )   +-   +-   +     -   +-   +-   +-   +-         - 
 y   1  1   17768 -0.012204              8( a  )   +-   +-   +     -   +-   +-   +-   +-         - 

 ci coefficient statistics:
           rq > 0.1                3
      0.1> rq > 0.01              98
     0.01> rq > 0.001            817
    0.001> rq > 0.0001          2771
   0.0001> rq > 0.00001         4353
  0.00001> rq > 0.000001        4986
 0.000001> rq                   9484
           all                 22512
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     10332 2x:         0 4x:         0
All internal counts: zz :     51156 yy:         0 xx:         0 ww:         0
One-external counts: yz :         0 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:         0    task #   2:     43243    task #   3:         0    task #   4:      6068
task #   5:         0    task #

 number of reference csfs (nref) is   336.  root number (iroot) is  1.

 pople ci energy extrapolation is computed with 16 correlated electrons.

 eref      =   -114.268222892639   "relaxed" cnot**2         =   0.966230619135
 eci       =   -114.353422979084   deltae = eci - eref       =  -0.085200086444
 eci+dv1   =   -114.356300133253   dv1 = (1-cnot**2)*deltae  =  -0.002877154169
 eci+dv2   =   -114.356400688661   dv2 = dv1 / cnot**2       =  -0.002977709578
 eci+dv3   =   -114.356508527363   dv3 = dv1 / (2*cnot**2-1) =  -0.003085548279
 eci+pople =   -114.356098232271   ( 16e- scaled deltae )    =  -0.087875339632


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 2) =      -114.2420382590

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7    8    9   10

                                          orbital     1    2    3    4    5    6    7    8    9   10

                                         symmetry   a    a    a    a    a    a    a    a    a    a  

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       2  0.021242                        +-   +-   +-   +-   +-   +-   +-   +     -      
 z*  1  1       7 -0.956870                        +-   +-   +-   +-   +-   +-   +    +-    -      
 z*  1  1      10 -0.034052                        +-   +-   +-   +-   +-   +-   +     -   +     - 
 z*  1  1      14 -0.059667                        +-   +-   +-   +-   +-   +-   +          -   +- 
 z*  1  1      29  0.156642                        +-   +-   +-   +-   +-   +     -   +-   +-      
 z*  1  1      32  0.052138                        +-   +-   +-   +-   +-   +     -   +    +-    - 
 z*  1  1      34 -0.016526                        +-   +-   +-   +-   +-   +     -        +-   +- 
 z*  1  1      36 -0.013666                        +-   +-   +-   +-   +-   +    +     -   +-    - 
 z*  1  1      48  0.019273                        +-   +-   +-   +-   +-        +    +-    -   +- 
 z*  1  1      60  0.028497                        +-   +-   +-   +-   +    +-    -   +-   +     - 
 z*  1  1      63  0.015717                        +-   +-   +-   +-   +    +-    -   +     -   +- 
 z*  1  1      67 -0.025003                        +-   +-   +-   +-   +    +-   +     -    -   +- 
 z*  1  1      77  0.018987                        +-   +-   +-   +-   +     -   +    +-   +-    - 
 z*  1  1      98  0.024609                        +-   +-   +-   +-        +-   +    +-    -   +- 
 z*  1  1     115  0.035577                        +-   +-   +-   +    +-   +-    -   +-   +     - 
 z*  1  1     118  0.010164                        +-   +-   +-   +    +-   +-    -   +     -   +- 
 z*  1  1     120 -0.022369                        +-   +-   +-   +    +-   +-   +    +-    -    - 
 z*  1  1     122 -0.016719                        +-   +-   +-   +    +-   +-   +     -    -   +- 
 z*  1  1     132  0.018628                        +-   +-   +-   +    +-    -   +    +-   +-    - 
 z*  1  1     183  0.018289                        +-   +-   +-        +-   +-   +    +-    -   +- 
 z*  1  1     197  0.057769                        +-   +-   +    +-   +-   +-   +-   +-    -      
 z*  1  1     200 -0.019740                        +-   +-   +    +-   +-   +-   +-    -   +     - 
 z*  1  1     202  0.033012                        +-   +-   +    +-   +-   +-   +-   +     -    - 
 z*  1  1     217  0.053080                        +-   +-   +    +-   +-    -   +-   +-   +-      
 z*  1  1     238  0.011801                        +-   +-   +    +-    -   +-   +-   +-   +     - 
 z*  1  1     252 -0.013246                        +-   +-   +    +-   +    +-   +-   +-    -    - 
 z*  1  1     316  0.012448                        +-   +-        +-   +-   +-   +    +-    -   +- 
 y   1  1     406  0.012697              4( a  )   +-   +-   +-   +-   +-   +-    -   +-           
 y   1  1     412 -0.014825             10( a  )   +-   +-   +-   +-   +-   +-    -   +-           
 y   1  1     439  0.011587             15( a  )   +-   +-   +-   +-   +-   +-    -   +     -      
 y   1  1     450 -0.020388              4( a  )   +-   +-   +-   +-   +-   +-    -   +          - 
 y   1  1     456 -0.020279             10( a  )   +-   +-   +-   +-   +-   +-    -   +          - 
 y   1  1     459 -0.017613             13( a  )   +-   +-   +-   +-   +-   +-    -   +          - 
 y   1  1     535  0.010890              1( a  )   +-   +-   +-   +-   +-   +-   +     -    -      
 y   1  1     569  0.011555             13( a  )   +-   +-   +-   +-   +-   +-   +     -         - 
 y   1  1     581  0.011435              3( a  )   +-   +-   +-   +-   +-   +-   +          -    - 
 y   1  1     606 -0.014121              6( a  )   +-   +-   +-   +-   +-   +-        +-    -      
 y   1  1     611  0.019540             11( a  )   +-   +-   +-   +-   +-   +-        +-    -      
 y   1  1     712 -0.012228              2( a  )   +-   +-   +-   +-   +-   +-        +     -    - 
 y   1  1     715  0.012165              5( a  )   +-   +-   +-   +-   +-   +-        +     -    - 
 y   1  1     716 -0.020836              6( a  )   +-   +-   +-   +-   +-   +-        +     -    - 
 y   1  1     718 -0.010204              8( a  )   +-   +-   +-   +-   +-   +-        +     -    - 
 y   1  1     721  0.021216             11( a  )   +-   +-   +-   +-   +-   +-        +     -    - 
 y   1  1     726 -0.012257             16( a  )   +-   +-   +-   +-   +-   +-        +     -    - 
 y   1  1     918 -0.013066             10( a  )   +-   +-   +-   +-   +-    -   +    +-    -      
 y   1  1     984  0.019973             10( a  )   +-   +-   +-   +-   +-    -   +     -   +     - 
 y   1  1    1028 -0.036002             10( a  )   +-   +-   +-   +-   +-    -   +    +     -    - 
 y   1  1    1031 -0.013033             13( a  )   +-   +-   +-   +-   +-    -   +    +     -    - 
 y   1  1    1286  0.018035              4( a  )   +-   +-   +-   +-   +-   +     -   +-    -      
 y   1  1    1292  0.013904             10( a  )   +-   +-   +-   +-   +-   +     -   +-    -      
 y   1  1    1295  0.012254             13( a  )   +-   +-   +-   +-   +-   +     -   +-    -      
 y   1  1    1732 -0.020777             10( a  )   +-   +-   +-   +-   +-         -   +-   +-      
 y   1  1    2235  0.016738              7( a  )   +-   +-   +-   +-    -   +-   +    +     -    - 
 y   1  1    2240  0.016351             12( a  )   +-   +-   +-   +-    -   +-   +    +     -    - 
 y   1  1    3088 -0.011995              2( a  )   +-   +-   +-   +-   +    +-   +-    -    -      
 y   1  1    3094 -0.012567              8( a  )   +-   +-   +-   +-   +    +-   +-    -    -      
 y   1  1    3102 -0.021543             16( a  )   +-   +-   +-   +-   +    +-   +-    -    -      
 y   1  1    3107  0.010818             21( a  )   +-   +-   +-   +-   +    +-   +-    -    -      
 y   1  1    3153 -0.011668              1( a  )   +-   +-   +-   +-   +    +-    -   +-    -      
 y   1  1    3159  0.017047              7( a  )   +-   +-   +-   +-   +    +-    -   +-    -      
 y   1  1    3167  0.034220             15( a  )   +-   +-   +-   +-   +    +-    -   +-    -      
 y   1  1    3170  0.015847             18( a  )   +-   +-   +-   +-   +    +-    -   +-    -      
 y   1  1    3184  0.012343             10( a  )   +-   +-   +-   +-   +    +-    -   +-         - 
 y   1  1    3340 -0.012564             12( a  )   +-   +-   +-   +-   +    +-   +     -    -    - 
 y   1  1    3350 -0.012600             22( a  )   +-   +-   +-   +-   +    +-   +     -    -    - 
 y   1  1    3361  0.018729             11( a  )   +-   +-   +-   +-   +    +-        +-    -    - 
 y   1  1    3430 -0.020380             14( a  )   +-   +-   +-   +-   +     -   +-   +-    -      
 y   1  1    3433 -0.025817             17( a  )   +-   +-   +-   +-   +     -   +-   +-    -      
 y   1  1    3624 -0.011313             10( a  )   +-   +-   +-   +-   +     -    -   +-   +     - 
 y   1  1    3734 -0.022827             10( a  )   +-   +-   +-   +-   +     -   +    +-    -    - 
 y   1  1    3744 -0.011386             20( a  )   +-   +-   +-   +-   +     -   +    +-    -    - 
 y   1  1    4087  0.010317             11( a  )   +-   +-   +-   +-        +-   +-   +-    -      
 y   1  1    5089  0.013963              1( a  )   +-   +-   +-    -   +-   +-   +    +-    -      
 y   1  1    5199  0.015058              1( a  )   +-   +-   +-    -   +-   +-   +    +     -    - 
 y   1  1    7333  0.043010              1( a  )   +-   +-   +-   +    +-   +-    -   +-    -      
 y   1  1    7335  0.011537              3( a  )   +-   +-   +-   +    +-   +-    -   +-    -      
 y   1  1    7337  0.026777              5( a  )   +-   +-   +-   +    +-   +-    -   +-    -      
 y   1  1    7338  0.016685              6( a  )   +-   +-   +-   +    +-   +-    -   +-    -      
 y   1  1    7339 -0.011087              7( a  )   +-   +-   +-   +    +-   +-    -   +-    -      
 y   1  1    7341  0.020631              9( a  )   +-   +-   +-   +    +-   +-    -   +-    -      
 y   1  1    7344 -0.011326             12( a  )   +-   +-   +-   +    +-   +-    -   +-    -      
 y   1  1    7350 -0.019229             18( a  )   +-   +-   +-   +    +-   +-    -   +-    -      
 y   1  1    7358  0.013636              4( a  )   +-   +-   +-   +    +-   +-    -   +-         - 
 y   1  1    7389 -0.012607             13( a  )   +-   +-   +-   +    +-   +-    -    -   +-      
 y   1  1    7773  0.012212              1( a  )   +-   +-   +-   +    +-    -    -   +-   +-      
 y   1  1   11254  0.013429              6( a  )   +-   +-    -   +-   +-   +-   +    +-    -      
 y   1  1   11360 -0.018431              2( a  )   +-   +-    -   +-   +-   +-   +    +     -    - 
 y   1  1   11734 -0.011387              2( a  )   +-   +-    -   +-   +-   +     -   +-   +-      
 y   1  1   12217  0.011444              1( a  )   +-   +-    -   +-   +    +-   +-   +-    -      
 y   1  1   13427 -0.011312              1( a  )   +-   +-    -   +    +-   +-   +-   +-    -      
 y   1  1   13616  0.016030             14( a  )   +-   +-    -   +    +-   +-    -   +-   +-      
 y   1  1   13736 -0.014794              2( a  )   +-   +-    -   +    +-   +-   +    +-    -    - 
 y   1  1   13742 -0.015515              8( a  )   +-   +-    -   +    +-   +-   +    +-    -    - 
 y   1  1   15496 -0.042927              2( a  )   +-   +-   +    +-   +-   +-    -   +-    -      
 y   1  1   15499  0.012022              5( a  )   +-   +-   +    +-   +-   +-    -   +-    -      
 y   1  1   15500 -0.017272              6( a  )   +-   +-   +    +-   +-   +-    -   +-    -      
 y   1  1   15502 -0.026629              8( a  )   +-   +-   +    +-   +-   +-    -   +-    -      
 y   1  1   15505  0.010612             11( a  )   +-   +-   +    +-   +-   +-    -   +-    -      
 y   1  1   15768 -0.011760             10( a  )   +-   +-   +    +-   +-    -   +-   +-    -      
 y   1  1   15936 -0.011774              2( a  )   +-   +-   +    +-   +-    -    -   +-   +-      
 y   1  1   17928  0.010414             14( a  )   +-   +-   +     -   +-   +-    -   +-   +-      
 y   1  1   18048 -0.010875              2( a  )   +-   +-   +     -   +-   +-   +    +-    -    - 
 y   1  1   18054 -0.012228              8( a  )   +-   +-   +     -   +-   +-   +    +-    -    - 
 y   1  1   20056  0.012574              8( a  )   +-   +-        +-   +-   +-   +-   +-    -      

 ci coefficient statistics:
           rq > 0.1                2
      0.1> rq > 0.01             102
     0.01> rq > 0.001           1020
    0.001> rq > 0.0001          3437
   0.0001> rq > 0.00001         4314
  0.00001> rq > 0.000001        4409
 0.000001> rq                   9228
           all                 22512
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     10332 2x:         0 4x:         0
All internal counts: zz :     51156 yy:         0 xx:         0 ww:         0
One-external counts: yz :         0 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:         0    task #   2:     43243    task #   3:         0    task #   4:      6068
task #   5:         0    task #

 number of reference csfs (nref) is   336.  root number (iroot) is  2.

 pople ci energy extrapolation is computed with 16 correlated electrons.

 eref      =   -114.151019720611   "relaxed" cnot**2         =   0.962943091552
 eci       =   -114.242038259009   deltae = eci - eref       =  -0.091018538398
 eci+dv1   =   -114.245411124654   dv1 = (1-cnot**2)*deltae  =  -0.003372865644
 eci+dv2   =   -114.245540922535   dv2 = dv1 / cnot**2       =  -0.003502663526
 eci+dv3   =   -114.245681110265   dv3 = dv1 / (2*cnot**2-1) =  -0.003642851255
 eci+pople =   -114.245193634557   ( 16e- scaled deltae )    =  -0.094173913946


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 3) =      -114.0618916265

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7    8    9   10

                                          orbital     1    2    3    4    5    6    7    8    9   10

                                         symmetry   a    a    a    a    a    a    a    a    a    a  

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       2  0.965932                        +-   +-   +-   +-   +-   +-   +-   +     -      
 z*  1  1       5  0.032829                        +-   +-   +-   +-   +-   +-   +-        +     - 
 z*  1  1       7  0.021195                        +-   +-   +-   +-   +-   +-   +    +-    -      
 z*  1  1      19 -0.018480                        +-   +-   +-   +-   +-   +-        +     -   +- 
 z*  1  1      23 -0.092243                        +-   +-   +-   +-   +-   +    +-    -   +-      
 z*  1  1      27 -0.030147                        +-   +-   +-   +-   +-   +    +-        +-    - 
 z*  1  1      45 -0.020357                        +-   +-   +-   +-   +-        +-   +     -   +- 
 z*  1  1      54  0.039186                        +-   +-   +-   +-   +    +-   +-    -   +     - 
 z*  1  1      56 -0.031387                        +-   +-   +-   +-   +    +-   +-   +     -    - 
 z*  1  1      58  0.017178                        +-   +-   +-   +-   +    +-   +-         -   +- 
 z*  1  1      71 -0.067555                        +-   +-   +-   +-   +     -   +-   +-   +-      
 z*  1  1      76  0.010224                        +-   +-   +-   +-   +     -   +-        +-   +- 
 z*  1  1      82 -0.020940                        +-   +-   +-   +-   +    +    +-    -   +-    - 
 z*  1  1      92  0.011491                        +-   +-   +-   +-        +-   +-   +-   +     - 
 z*  1  1      95 -0.027603                        +-   +-   +-   +-        +-   +-   +     -   +- 
 z*  1  1     106  0.038937                        +-   +-   +-   +    +-   +-   +-   +-    -      
 z*  1  1     109 -0.038188                        +-   +-   +-   +    +-   +-   +-    -   +     - 
 z*  1  1     111  0.045211                        +-   +-   +-   +    +-   +-   +-   +     -    - 
 z*  1  1     113  0.013514                        +-   +-   +-   +    +-   +-   +-         -   +- 
 z*  1  1     126  0.044771                        +-   +-   +-   +    +-    -   +-   +-   +-      
 z*  1  1     129 -0.018860                        +-   +-   +-   +    +-    -   +-   +    +-    - 
 z*  1  1     161 -0.015018                        +-   +-   +-   +    +    +-   +-   +-    -    - 
 z*  1  1     177 -0.014784                        +-   +-   +-        +-   +-   +-   +-   +     - 
 z*  1  1     180 -0.021391                        +-   +-   +-        +-   +-   +-   +     -   +- 
 z*  1  1     211 -0.010756                        +-   +-   +    +-   +-   +-   +    +-    -    - 
 z*  1  1     310 -0.010781                        +-   +-        +-   +-   +-   +-   +-   +     - 
 z*  1  1     313 -0.013325                        +-   +-        +-   +-   +-   +-   +     -   +- 
 y   1  1     346  0.018522             10( a  )   +-   +-   +-   +-   +-   +-   +-    -           
 y   1  1     349  0.012897             13( a  )   +-   +-   +-   +-   +-   +-   +-    -           
 y   1  1     380 -0.011266             22( a  )   +-   +-   +-   +-   +-   +-   +-         -      
 y   1  1     384  0.026113              4( a  )   +-   +-   +-   +-   +-   +-   +-              - 
 y   1  1     390  0.017164             10( a  )   +-   +-   +-   +-   +-   +-   +-              - 
 y   1  1     419  0.012173             17( a  )   +-   +-   +-   +-   +-   +-    -   +-           
 y   1  1     426 -0.011079              2( a  )   +-   +-   +-   +-   +-   +-    -   +     -      
 y   1  1     492 -0.017390              2( a  )   +-   +-   +-   +-   +-   +-    -        +     - 
 y   1  1     495  0.014254              5( a  )   +-   +-   +-   +-   +-   +-    -        +     - 
 y   1  1     496 -0.024474              6( a  )   +-   +-   +-   +-   +-   +-    -        +     - 
 y   1  1     498 -0.013023              8( a  )   +-   +-   +-   +-   +-   +-    -        +     - 
 y   1  1     501  0.023205             11( a  )   +-   +-   +-   +-   +-   +-    -        +     - 
 y   1  1     506 -0.014919             16( a  )   +-   +-   +-   +-   +-   +-    -        +     - 
 y   1  1     550  0.013129             16( a  )   +-   +-   +-   +-   +-   +-   +     -    -      
 y   1  1     615 -0.010417             15( a  )   +-   +-   +-   +-   +-   +-        +-    -      
 y   1  1     777 -0.011521              1( a  )   +-   +-   +-   +-   +-    -   +-   +-           
 y   1  1     791  0.012543             15( a  )   +-   +-   +-   +-   +-    -   +-   +-           
 y   1  1     874 -0.030339             10( a  )   +-   +-   +-   +-   +-    -   +-        +     - 
 y   1  1     877 -0.010966             13( a  )   +-   +-   +-   +-   +-    -   +-        +     - 
 y   1  1     925  0.010704             17( a  )   +-   +-   +-   +-   +-    -   +    +-    -      
 y   1  1    1220 -0.022058              4( a  )   +-   +-   +-   +-   +-   +    +-    -    -      
 y   1  1    1226 -0.021678             10( a  )   +-   +-   +-   +-   +-   +    +-    -    -      
 y   1  1    1229 -0.012112             13( a  )   +-   +-   +-   +-   +-   +    +-    -    -      
 y   1  1    1328  0.018168              2( a  )   +-   +-   +-   +-   +-   +     -    -   +-      
 y   1  1    1331 -0.013922              5( a  )   +-   +-   +-   +-   +-   +     -    -   +-      
 y   1  1    1332  0.023940              6( a  )   +-   +-   +-   +-   +-   +     -    -   +-      
 y   1  1    1334  0.011439              8( a  )   +-   +-   +-   +-   +-   +     -    -   +-      
 y   1  1    1337 -0.021547             11( a  )   +-   +-   +-   +-   +-   +     -    -   +-      
 y   1  1    1342  0.011832             16( a  )   +-   +-   +-   +-   +-   +     -    -   +-      
 y   1  1    1600  0.025524             10( a  )   +-   +-   +-   +-   +-        +-    -   +-      
 y   1  1    2081  0.013630              7( a  )   +-   +-   +-   +-    -   +-   +-        +     - 
 y   1  1    2086  0.014967             12( a  )   +-   +-   +-   +-    -   +-   +-        +     - 
 y   1  1    2482 -0.012090             12( a  )   +-   +-   +-   +-    -   +    +-    -   +-      
 y   1  1    3089 -0.014882              3( a  )   +-   +-   +-   +-   +    +-   +-    -    -      
 y   1  1    3098 -0.023175             12( a  )   +-   +-   +-   +-   +    +-   +-    -    -      
 y   1  1    3104  0.010536             18( a  )   +-   +-   +-   +-   +    +-   +-    -    -      
 y   1  1    3108 -0.016707             22( a  )   +-   +-   +-   +-   +    +-   +-    -    -      
 y   1  1    3118 -0.012675             10( a  )   +-   +-   +-   +-   +    +-   +-    -         - 
 y   1  1    3142  0.010733             12( a  )   +-   +-   +-   +-   +    +-   +-         -    - 
 y   1  1    3154  0.015323              2( a  )   +-   +-   +-   +-   +    +-    -   +-    -      
 y   1  1    3158  0.015292              6( a  )   +-   +-   +-   +-   +    +-    -   +-    -      
 y   1  1    3160  0.014627              8( a  )   +-   +-   +-   +-   +    +-    -   +-    -      
 y   1  1    3168  0.025492             16( a  )   +-   +-   +-   +-   +    +-    -   +-    -      
 y   1  1    3173 -0.013023             21( a  )   +-   +-   +-   +-   +    +-    -   +-    -      
 y   1  1    3229 -0.013880             11( a  )   +-   +-   +-   +-   +    +-    -    -   +     - 
 y   1  1    3268  0.010735              6( a  )   +-   +-   +-   +-   +    +-    -   +     -    - 
 y   1  1    3273 -0.023233             11( a  )   +-   +-   +-   +-   +    +-    -   +     -    - 
 y   1  1    3283  0.010128             21( a  )   +-   +-   +-   +-   +    +-    -   +     -    - 
 y   1  1    3429  0.029058             13( a  )   +-   +-   +-   +-   +     -   +-   +-    -      
 y   1  1    3436 -0.015350             20( a  )   +-   +-   +-   +-   +     -   +-   +-    -      
 y   1  1    3492  0.013564             10( a  )   +-   +-   +-   +-   +     -   +-    -   +     - 
 y   1  1    3536  0.024100             10( a  )   +-   +-   +-   +-   +     -   +-   +     -    - 
 y   1  1    5045  0.011727              1( a  )   +-   +-   +-    -   +-   +-   +-        +     - 
 y   1  1    5441 -0.014755              1( a  )   +-   +-   +-    -   +-   +    +-    -   +-      
 y   1  1    7267 -0.029226              1( a  )   +-   +-   +-   +    +-   +-   +-    -    -      
 y   1  1    7269 -0.019824              3( a  )   +-   +-   +-   +    +-   +-   +-    -    -      
 y   1  1    7271 -0.022571              5( a  )   +-   +-   +-   +    +-   +-   +-    -    -      
 y   1  1    7272 -0.013493              6( a  )   +-   +-   +-   +    +-   +-   +-    -    -      
 y   1  1    7273  0.015647              7( a  )   +-   +-   +-   +    +-   +-   +-    -    -      
 y   1  1    7278  0.013789             12( a  )   +-   +-   +-   +    +-   +-   +-    -    -      
 y   1  1    7284  0.030347             18( a  )   +-   +-   +-   +    +-   +-   +-    -    -      
 y   1  1    7292 -0.013780              4( a  )   +-   +-   +-   +    +-   +-   +-    -         - 
 y   1  1    7334 -0.016522              2( a  )   +-   +-   +-   +    +-   +-    -   +-    -      
 y   1  1    7338 -0.012957              6( a  )   +-   +-   +-   +    +-   +-    -   +-    -      
 y   1  1    7340 -0.010765              8( a  )   +-   +-   +-   +    +-   +-    -   +-    -      
 y   1  1    7641 -0.010972              1( a  )   +-   +-   +-   +    +-    -   +-    -   +-      
 y   1  1    9577  0.010243              1( a  )   +-   +-   +-        +-   +-   +-   +-    -      
 y   1  1   11206 -0.015380              2( a  )   +-   +-    -   +-   +-   +-   +-        +     - 
 y   1  1   11602  0.015113              2( a  )   +-   +-    -   +-   +-   +    +-    -   +-      
 y   1  1   12218  0.015365              2( a  )   +-   +-    -   +-   +    +-   +-   +-    -      
 y   1  1   13428  0.010810              2( a  )   +-   +-    -   +    +-   +-   +-   +-    -      
 y   1  1   13434  0.015952              8( a  )   +-   +-    -   +    +-   +-   +-   +-    -      
 y   1  1   13484 -0.015945             14( a  )   +-   +-    -   +    +-   +-   +-    -   +-      
 y   1  1   13538  0.016052              2( a  )   +-   +-    -   +    +-   +-   +-   +     -    - 
 y   1  1   13544  0.017038              8( a  )   +-   +-    -   +    +-   +-   +-   +     -    - 
 y   1  1   15430  0.021989              2( a  )   +-   +-   +    +-   +-   +-   +-    -    -      
 y   1  1   15436  0.014593              8( a  )   +-   +-   +    +-   +-   +-   +-    -    -      
 y   1  1   15495  0.013885              1( a  )   +-   +-   +    +-   +-   +-    -   +-    -      
 y   1  1   17746  0.012879              8( a  )   +-   +-   +     -   +-   +-   +-   +-    -      
 y   1  1   17796 -0.010435             14( a  )   +-   +-   +     -   +-   +-   +-    -   +-      
 y   1  1   17850  0.011007              2( a  )   +-   +-   +     -   +-   +-   +-   +     -    - 
 y   1  1   17856  0.013875              8( a  )   +-   +-   +     -   +-   +-   +-   +     -    - 
 y   1  1   20049 -0.012643              1( a  )   +-   +-        +-   +-   +-   +-   +-    -      
 y   1  1   20053 -0.011798              5( a  )   +-   +-        +-   +-   +-   +-   +-    -      
 y   1  1   20057 -0.010799              9( a  )   +-   +-        +-   +-   +-   +-   +-    -      

 ci coefficient statistics:
           rq > 0.1                1
      0.1> rq > 0.01             111
     0.01> rq > 0.001            981
    0.001> rq > 0.0001          3241
   0.0001> rq > 0.00001         4725
  0.00001> rq > 0.000001        5768
 0.000001> rq                   7685
           all                 22512
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:     10332 2x:         0 4x:         0
All internal counts: zz :     51156 yy:         0 xx:         0 ww:         0
One-external counts: yz :         0 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2=           0


LOOPCOUNT per task:
task #   1:         0    task #   2:     43243    task #   3:         0    task #   4:      6068
task #   5:         0    task #

 number of reference csfs (nref) is   336.  root number (iroot) is  3.

 pople ci energy extrapolation is computed with 16 correlated electrons.

 eref      =   -113.972064647252   "relaxed" cnot**2         =   0.962838359014
 eci       =   -114.061891626519   deltae = eci - eref       =  -0.089826979267
 eci+dv1   =   -114.065229744474   dv1 = (1-cnot**2)*deltae  =  -0.003338117954
 eci+dv2   =   -114.065358582237   dv2 = dv1 / cnot**2       =  -0.003466955718
 eci+dv3   =   -114.065497764482   dv3 = dv1 / (2*cnot**2-1) =  -0.003606137963
 eci+pople =   -114.065015103918   ( 16e- scaled deltae )    =  -0.092950456666
NO coefficients and occupation numbers are written to nocoef_ci.1

################################################################################
 one electron density matrix calculation 
################################################################################


 workspace allocation information: mxcore=99998791
 
 space required:
 
    space required for calls in multd2:
       onex          616
       allin           0
       diagon       1265
    max.      ---------
       maxnex       1265
 
    total core space usage:
       maxnex       1265
    max k-seg      22176
    max k-ind       1008
              ---------
       totmax      47633
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=    4368  DYX=       0  DYW=       0
   D0Z=    4312  D0Y=   18564  D0X=       0  D0W=       0
  DDZI=    2464 DDYI=    7168 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=    1008 DDXE=       0 DDWE=       0
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   2.0000000      2.0000000      1.9947816      1.9919045      1.9870258
   1.9861677      1.9618378      1.8825738      0.1228032      0.0427243
   0.0100748      0.0033881      0.0027150      0.0021828      0.0020047
   0.0016090      0.0013444      0.0012387      0.0009434      0.0009198
   0.0009185      0.0006287      0.0006042      0.0004325      0.0003657
   0.0003588      0.0001543      0.0001277      0.0001112      0.0000312
   0.0000190      0.0000090


 total number of electrons =   16.0000000000



          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        A   partial gross atomic populations
   ao class       1A         2A         3A         4A         5A         6A  
    O1_ s      -0.000624   1.999228  -0.007725   1.060165   0.928094   1.921012
    C1_ s       2.000617   0.000764   1.077683   0.622960   0.719496   0.035225
    H1_ s      -0.000069   0.000003   0.448574   0.159173   0.171658   0.014530
    H2_ s       0.000076   0.000004   0.476250   0.149607   0.167777   0.015400
 
   ao class       7A         8A         9A        10A        11A        12A  
    O1_ s       1.253008   1.157254   0.049911   0.018184   0.005547   0.000977
    C1_ s       0.709220   0.725319   0.072893   0.023962   0.002692   0.001174
    H1_ s      -0.000144   0.000000   0.000000   0.000310   0.000890   0.000622
    H2_ s      -0.000246   0.000000   0.000000   0.000268   0.000946   0.000615
 
   ao class      13A        14A        15A        16A        17A        18A  
    O1_ s       0.001744   0.000222   0.001205   0.001142   0.001297   0.001227
    C1_ s       0.000729   0.001598   0.000800   0.000373   0.000047   0.000012
    H1_ s       0.000107   0.000188   0.000000   0.000051   0.000000   0.000000
    H2_ s       0.000134   0.000175   0.000000   0.000043   0.000000   0.000000
 
   ao class      19A        20A        21A        22A        23A        24A  
    O1_ s       0.000824   0.000034   0.000283   0.000144   0.000427   0.000188
    C1_ s       0.000076   0.000886   0.000607   0.000485   0.000190   0.000207
    H1_ s       0.000022   0.000000   0.000016   0.000000  -0.000006   0.000020
    H2_ s       0.000021   0.000000   0.000013   0.000000  -0.000006   0.000018
 
   ao class      25A        26A        27A        28A        29A        30A  
    O1_ s       0.000145   0.000057   0.000105   0.000005   0.000000   0.000001
    C1_ s       0.000244   0.000302   0.000049   0.000101   0.000043   0.000003
    H1_ s      -0.000012   0.000000   0.000000   0.000013   0.000033   0.000014
    H2_ s      -0.000011   0.000000   0.000000   0.000009   0.000035   0.000014
 
   ao class      31A        32A  
    O1_ s       0.000003   0.000000
    C1_ s       0.000012   0.000005
    H1_ s       0.000001   0.000002
    H2_ s       0.000002   0.000002


                        gross atomic populations
     ao           O1_        C1_        H1_        H2_
      s         8.394084   5.998774   0.795996   0.811146
    total       8.394084   5.998774   0.795996   0.811146
 

 Total number of electrons:   16.00000000

NO coefficients and occupation numbers are written to nocoef_ci.2
--------------------------------------------------------------------------------
   calculation for root #    2
--------------------------------------------------------------------------------
================================================================================
   DYZ=    4368  DYX=       0  DYW=       0
   D0Z=    4312  D0Y=   18564  D0X=       0  D0W=       0
  DDZI=    2464 DDYI=    7168 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=    1008 DDXE=       0 DDWE=       0
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   2.0000000      2.0000000      1.9907410      1.9868299      1.9840475
   1.9815184      1.9656083      1.0092934      1.0071335      0.0393299
   0.0073026      0.0062930      0.0049546      0.0027244      0.0020320
   0.0017703      0.0017054      0.0014878      0.0013780      0.0011510
   0.0010333      0.0007207      0.0006907      0.0006113      0.0005830
   0.0003843      0.0002451      0.0001769      0.0001446      0.0000533
   0.0000365      0.0000193


 total number of electrons =   16.0000000000



          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        A   partial gross atomic populations
   ao class       1A         2A         3A         4A         5A         6A  
    O1_ s      -0.000624   1.999228   1.470221   0.534608   0.134420   1.772827
    C1_ s       2.000617   0.000764   0.318745   0.963841   1.066593   0.208686
    H1_ s      -0.000069   0.000003   0.105194   0.223358   0.401258   0.000001
    H2_ s       0.000076   0.000004   0.096581   0.265023   0.381777   0.000004
 
   ao class       7A         8A         9A        10A        11A        12A  
    O1_ s       1.311305   0.344926   0.664401   0.015648   0.002068   0.000179
    C1_ s       0.657380   0.641867   0.291607   0.022647   0.002452   0.003094
    H1_ s      -0.002160   0.011133   0.025221   0.000546   0.001257   0.001610
    H2_ s      -0.000917   0.011368   0.025904   0.000488   0.001525   0.001409
 
   ao class      13A        14A        15A        16A        17A        18A  
    O1_ s       0.004439   0.002609   0.001781   0.000833   0.000272   0.001397
    C1_ s       0.000515   0.000081   0.000205   0.000728   0.001433   0.000091
    H1_ s       0.000000   0.000017   0.000028   0.000101   0.000000   0.000000
    H2_ s       0.000000   0.000017   0.000018   0.000109   0.000000   0.000000
 
   ao class      19A        20A        21A        22A        23A        24A  
    O1_ s       0.001272   0.000697   0.000279   0.000042   0.000374   0.000158
    C1_ s       0.000083   0.000454   0.000728   0.000679   0.000247   0.000309
    H1_ s       0.000012   0.000000   0.000016   0.000000   0.000036   0.000075
    H2_ s       0.000011   0.000000   0.000011   0.000000   0.000034   0.000070
 
   ao class      25A        26A        27A        28A        29A        30A  
    O1_ s       0.000210   0.000136   0.000007   0.000005   0.000099   0.000003
    C1_ s       0.000361   0.000249   0.000104   0.000151   0.000032   0.000015
    H1_ s       0.000003   0.000000   0.000066   0.000010   0.000006   0.000017
    H2_ s       0.000009   0.000000   0.000069   0.000011   0.000007   0.000018
 
   ao class      31A        32A  
    O1_ s       0.000002   0.000002
    C1_ s       0.000022   0.000017
    H1_ s       0.000007   0.000000
    H2_ s       0.000005   0.000000


                        gross atomic populations
     ao           O1_        C1_        H1_        H2_
      s         8.263824   6.184799   0.767745   0.783632
    total       8.263824   6.184799   0.767745   0.783632
 

 Total number of electrons:   16.00000000

NO coefficients and occupation numbers are written to nocoef_ci.3
--------------------------------------------------------------------------------
   calculation for root #    3
--------------------------------------------------------------------------------
================================================================================
   DYZ=    4368  DYX=       0  DYW=       0
   D0Z=    4312  D0Y=   18564  D0X=       0  D0W=       0
  DDZI=    2464 DDYI=    7168 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=    1008 DDXE=       0 DDWE=       0
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   2.0000000      2.0000000      1.9917529      1.9873800      1.9854864
   1.9770699      1.9738052      1.0124892      1.0075727      0.0294748
   0.0082304      0.0048717      0.0041735      0.0025765      0.0022504
   0.0021338      0.0019574      0.0015294      0.0013344      0.0010454
   0.0009727      0.0008102      0.0007823      0.0007076      0.0004342
   0.0004137      0.0003063      0.0002086      0.0001360      0.0000466
   0.0000387      0.0000090


 total number of electrons =   16.0000000000



          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        A   partial gross atomic populations
   ao class       1A         2A         3A         4A         5A         6A  
    O1_ s      -0.000624   1.999228   0.199709   1.714906   1.010309   1.679990
    C1_ s       2.000617   0.000764   1.046282   0.080895   0.788410   0.297071
    H1_ s      -0.000069   0.000003   0.420578   0.142354   0.050219   0.000001
    H2_ s       0.000076   0.000004   0.325183   0.049225   0.136549   0.000008
 
   ao class       7A         8A         9A        10A        11A        12A  
    O1_ s       0.878534   0.228954   0.646713   0.010305   0.003754   0.000667
    C1_ s       0.731639   0.775966   0.317952   0.017055   0.002514   0.002446
    H1_ s       0.128377   0.003319   0.018937   0.001033   0.000820   0.000921
    H2_ s       0.235255   0.004250   0.023971   0.001082   0.001143   0.000837
 
   ao class      13A        14A        15A        16A        17A        18A  
    O1_ s       0.003760   0.001747   0.000711   0.000470   0.001903   0.001073
    C1_ s       0.000414   0.000662   0.001102   0.001664   0.000035   0.000457
    H1_ s       0.000000   0.000076   0.000230   0.000000   0.000007   0.000000
    H2_ s       0.000000   0.000091   0.000208   0.000000   0.000012   0.000000
 
   ao class      19A        20A        21A        22A        23A        24A  
    O1_ s       0.001133   0.000396   0.000960   0.000680   0.000010   0.000126
    C1_ s       0.000121   0.000543   0.000012   0.000121   0.000773   0.000389
    H1_ s       0.000043   0.000056   0.000000   0.000004   0.000000   0.000086
    H2_ s       0.000037   0.000049   0.000000   0.000005   0.000000   0.000106
 
   ao class      25A        26A        27A        28A        29A        30A  
    O1_ s       0.000095   0.000079   0.000007   0.000003   0.000064   0.000005
    C1_ s       0.000297   0.000334   0.000160   0.000187   0.000058   0.000010
    H1_ s       0.000021   0.000000   0.000068   0.000007   0.000008   0.000010
    H2_ s       0.000021   0.000000   0.000071   0.000011   0.000006   0.000021
 
   ao class      31A        32A  
    O1_ s       0.000010   0.000001
    C1_ s       0.000016   0.000008
    H1_ s       0.000012   0.000000
    H2_ s       0.000001   0.000000


                        gross atomic populations
     ao           O1_        C1_        H1_        H2_
      s         8.385680   6.068972   0.767124   0.778223
    total       8.385680   6.068972   0.767124   0.778223
 

 Total number of electrons:   16.00000000

