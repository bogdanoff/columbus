

     ******************************************
     **    PROGRAM:              MCPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 original author: Daniel Robertson, FSU
 later revisions: Ron Shepard, ANL;
                  Michal Dallos, University Vienna



 This Version of Program mcpc is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at

    Thomas Mueller
    Central Institute of Applied Mathematics
    Research Centre Juelich
    D-52425 Juelich, Germany
    Internet: th.mueller@fz-juelich.de



   ******  File header section  ******

 Headers form the restart file:
    Hermit Integral Program : SIFS version  wizard2.itc.unvie 11:12:27.841 04-Feb-08
     title                                                                          


   ******  DRT info section  ******

 Informations for the DRT no.  1
 Header form the DRT file: 
     title                                                                          
 Molecular symmetry group:   sym1
 Total number of electrons:   16
 Spin multiplicity:            1
 Number of active orbitals:    8
 Number of active electrons:  12
 Total number of CSFs:       336

   ***  Informations from the DRT number:   1

 
 Symmetry orbital summary:
 Symm.blocks:         1
 Symm.labels:         a  

 List of doubly occupied orbitals:
  1 a    2 a  

 List of active orbitals:
  3 a    4 a    5 a    6 a    7 a    8 a    9 a   10 a  


   ******  MCSCF convergence information:  ******

 MCSCF convergence criteria were satisfied.

 mcscf energy=  -114.1323167004    nuclear repulsion=    28.6297069652
 demc=             0.0000000016    wnorm=                 0.0000006466
 knorm=            0.0000000380    apxde=                 0.0000000000


 MCSCF calculation performmed for   1 symmetry.

 State averaging:
 No,  ssym, navst, wavst
  1    a      3   0.3333 0.3333 0.3333

 Input the DRT No of interest: [  1]:

In the DRT No.: 1 there are  3 states.

 Which one to take? [  1]:

 The CSFs for the state No  1 of the symmetry  a   will be printed
 according to the following print options :

 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 0) end.

 input menu number [  0]:
 csfs will be printed based on coefficient magnitudes.

 input the coefficient threshold (end with 0.) [ 0.0000]:

 List of active orbitals:
  3 a    4 a    5 a    6 a    7 a    8 a    9 a   10 a  

   csf       coeff       coeff**2    step(*)
  -----  ------------  ------------  ------------
      1  0.9199575478  0.8463218898  33333300
     21  0.2979545630  0.0887769216  33313320
     41 -0.1877375372  0.0352453829  33303330
     24  0.0741693567  0.0055010935  33313212
     26 -0.0716584011  0.0051349264  33313122
      3 -0.0694143413  0.0048183508  33333102
      6 -0.0617732554  0.0038159351  33333003
     72 -0.0384372670  0.0014774235  33123312
    127 -0.0360320410  0.0012983080  31323312
     55  0.0287188610  0.0008247730  33133203
    176 -0.0262475261  0.0006889326  30333330
     46  0.0254117063  0.0006457548  33303033
    110  0.0251285445  0.0006314437  31333203
     93 -0.0240918917  0.0005804192  33033303
     28  0.0232695111  0.0005414701  33313023
     52  0.0229856203  0.0005283387  33133302
     43 -0.0174489741  0.0003044667  33303303
     81 -0.0171871186  0.0002953970  33113322
     91 -0.0165825632  0.0002749814  33033330
      4 -0.0147900569  0.0002187458  33333030
     17 -0.0146045314  0.0002132923  33330303
     83  0.0142730674  0.0002037205  33113223
     15 -0.0134119535  0.0001798805  33330330
     44 -0.0129889552  0.0001687130  33303132
     89  0.0128333717  0.0001646954  33103233
    178 -0.0126174410  0.0001591998  30333303
    146  0.0125172918  0.0001566826  31233330
     53 -0.0116900788  0.0001366579  33133230
    148 -0.0091163834  0.0000831084  31233303
     75 -0.0079879388  0.0000638072  33123123
    144  0.0078682841  0.0000619099  31303233
    102 -0.0066614048  0.0000443743  33013323
    105  0.0065167966  0.0000424686  33003333
    309 -0.0061066326  0.0000372910  03333330
    179 -0.0060645490  0.0000367788  30333132
     87 -0.0059791730  0.0000357505  33103332
    311 -0.0058989146  0.0000347972  03333303
    187 -0.0057515693  0.0000330805  30313323
    108  0.0055848627  0.0000311907  31333230
    138  0.0048249815  0.0000232804  31313223
    130 -0.0044552105  0.0000198489  31323123
     94  0.0037946341  0.0000143992  33033132
    205  0.0036975566  0.0000136719  13332330
     57  0.0036391065  0.0000132431  33133032
    190  0.0033867140  0.0000114698  30303333
    136  0.0032181838  0.0000103567  31313322
    320 -0.0032038070  0.0000102644  03313323
     18  0.0028973996  0.0000083949  33330132
     50  0.0028541979  0.0000081464  33300333
    160  0.0027732394  0.0000076909  31203333
    157 -0.0025318563  0.0000064103  31213323
    207 -0.0021494089  0.0000046200  13332303
    191  0.0019245883  0.0000037040  30133332
    267 -0.0018238240  0.0000033263  12333330
    181  0.0016270808  0.0000026474  30333033
    312 -0.0015741982  0.0000024781  03333132
     39  0.0015509046  0.0000024053  33310323
    227  0.0012569357  0.0000015799  13313322
    112 -0.0012498453  0.0000015621  31333032
     96  0.0012114621  0.0000014676  33033033
    149  0.0011735729  0.0000013773  31233132
    323  0.0011714440  0.0000013723  03303333
    196  0.0011570343  0.0000013387  30033333
    208  0.0010854137  0.0000011781  13332132

 input the coefficient threshold (end with 0.) [ 0.0000]:

 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 0) end.

 input menu number [  0]:
