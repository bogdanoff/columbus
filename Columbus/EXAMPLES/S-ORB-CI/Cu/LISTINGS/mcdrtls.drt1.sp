
 program "mcdrt 4.1 a3"

 distinct row table specification and csf
 selection for mcscf wavefunction optimization.

 programmed by: ron shepard

 version date: 17-oct-91


 This Version of Program mcdrt is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              MCDRT       **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 expanded keystroke file:
 /bigscratch/Columbus_C70/Cu_SO/WORK/mcdrtky                                     
 
 input the spin multiplicity [  0]: spin multiplicity:    2    doublet 
 input the total number of electrons [  0]: nelt:     19
 input the number of irreps (1-8) [  0]: nsym:      8
 enter symmetry labels:(y,[n]) enter 8 labels (a4):
 enter symmetry label, default=   1
 enter symmetry label, default=   2
 enter symmetry label, default=   3
 enter symmetry label, default=   4
 enter symmetry label, default=   5
 enter symmetry label, default=   6
 enter symmetry label, default=   7
 enter symmetry label, default=   8
 input the molecular spatial symmetry (irrep 1:nsym) [  0]: spatial symmetry is irrep number:      1
 
 input the list of doubly-occupied orbitals (sym(i),rmo(i),i=1,ndot):
 number of doubly-occupied orbitals:      4
 number of inactive electrons:      8
 number of active electrons:     11
 level(*)        1   2   3   4
 symd(*)         1   6   7   8
 slabel(*)     ag  b1u b2u b3u
 doub(*)         1   1   1   1
 
 input the active orbitals (sym(i),rmo(i),i=1,nact):
 nact:      6
 level(*)        1   2   3   4   5   6
 syml(*)         1   1   1   2   3   4
 slabel(*)     ag  ag  ag  b1g b2g b3g
 modrt(*)        2   3   4   1   1   1
 input the minimum cumulative occupation for each active level:
  ag  ag  ag  b1g b2g b3g
    2   3   4   1   1   1
 input the maximum cumulative occupation for each active level:
  ag  ag  ag  b1g b2g b3g
    2   3   4   1   1   1
 slabel(*)     ag  ag  ag  b1g b2g b3g
 modrt(*)        2   3   4   1   1   1
 occmin(*)       0   0   0   0   0  11
 occmax(*)      11  11  11  11  11  11
 input the minimum b value for each active level:
  ag  ag  ag  b1g b2g b3g
    2   3   4   1   1   1
 input the maximum b value for each active level:
  ag  ag  ag  b1g b2g b3g
    2   3   4   1   1   1
 slabel(*)     ag  ag  ag  b1g b2g b3g
 modrt(*)        2   3   4   1   1   1
 bmin(*)         0   0   0   0   0   0
 bmax(*)        11  11  11  11  11  11
 input the step masks for each active level:
 modrt:smask=
   2:1111   3:1111   4:1111   1:1111   1:1111   1:1111
 input the number of vertices to be deleted [  0]: number of vertices to be removed (a priori):      0
 number of rows in the drt:     12
 are any arcs to be manually removed?(y,[n])
 nwalk=       3
 input the range of drt levels to print (l1,l2):
 levprt(*)       0   6

 level  0 through level  6 of the drt:

 row lev a b syml lab rmo  l0  l1  l2  l3 isym xbar   y0    y1    y2    xp     z

  12   6 5 1   4 b3g   1    0   0   0   0   1     1     0     0     0     3     0
                                            2     0     0     0     0     1     0
                                            3     0     0     0     0     1     0
                                            4     0     0     0     0     1     0
                                            5     0     0     0     0     0     0
                                            6     0     0     0     0     0     0
                                            7     0     0     0     0     0     0
                                            8     0     0     0     0     0     0
 ........................................

  10   5 5 0   3 b2g   1    0  12   0   0   1     0     0     0     0     1     0
                                            2     0     0     0     0     0     0
                                            3     0     0     0     0     0     0
                                            4     1     1     0     0     0     0
                                            5     0     0     0     0     0     0
                                            6     0     0     0     0     0     0
                                            7     0     0     0     0     0     0
                                            8     0     0     0     0     0     0

  11   5 4 1   3 b2g   1    0   0   0  12   1     1     1     1     1     3     0
                                            2     0     0     0     0     1     0
                                            3     0     0     0     0     1     0
                                            4     0     0     0     0     0     0
                                            5     0     0     0     0     0     0
                                            6     0     0     0     0     0     0
                                            7     0     0     0     0     0     0
                                            8     0     0     0     0     0     0
 ........................................

   8   4 4 0   2 b1g   1    0  11   0  10   1     0     0     0     0     1     0
                                            2     0     0     0     0     0     0
                                            3     1     1     0     0     0     0
                                            4     1     1     1     1     0     0
                                            5     0     0     0     0     0     0
                                            6     0     0     0     0     0     0
                                            7     0     0     0     0     0     0
                                            8     0     0     0     0     0     0

   9   4 3 1   2 b1g   1    0   0   0  11   1     1     1     1     1     3     0
                                            2     0     0     0     0     1     0
                                            3     0     0     0     0     0     0
                                            4     0     0     0     0     0     0
                                            5     0     0     0     0     0     0
                                            6     0     0     0     0     0     0
                                            7     0     0     0     0     0     0
                                            8     0     0     0     0     0     0
 ........................................

   6   3 3 0   1 ag    4    0   9   0   8   1     0     0     0     0     1     0
                                            2     1     1     0     0     0     0
                                            3     1     1     1     1     0     0
                                            4     1     1     1     1     0     0
                                            5     0     0     0     0     0     0
                                            6     0     0     0     0     0     0
                                            7     0     0     0     0     0     0
                                            8     0     0     0     0     0     0

   7   3 2 1   1 ag    4    0   0   0   9   1     1     1     1     1     3     0
                                            2     0     0     0     0     0     0
                                            3     0     0     0     0     0     0
                                            4     0     0     0     0     0     0
                                            5     0     0     0     0     0     0
                                            6     0     0     0     0     0     0
                                            7     0     0     0     0     0     0
                                            8     0     0     0     0     0     0
 ........................................

   4   2 2 0   1 ag    3    0   7   0   6   1     1     1     0     0     1     0
                                            2     1     1     1     1     0     0
                                            3     1     1     1     1     0     0
                                            4     1     1     1     1     0     0
                                            5     0     0     0     0     0     0
                                            6     0     0     0     0     0     0
                                            7     0     0     0     0     0     0
                                            8     0     0     0     0     0     0

   5   2 1 1   1 ag    3    0   0   0   7   1     1     1     1     1     2     1
                                            2     0     0     0     0     0     0
                                            3     0     0     0     0     0     0
                                            4     0     0     0     0     0     0
                                            5     0     0     0     0     0     0
                                            6     0     0     0     0     0     0
                                            7     0     0     0     0     0     0
                                            8     0     0     0     0     0     0
 ........................................

   2   1 1 0   1 ag    2    0   5   0   4   1     2     2     1     1     1     0
                                            2     1     1     1     1     0     0
                                            3     1     1     1     1     0     0
                                            4     1     1     1     1     0     0
                                            5     0     0     0     0     0     0
                                            6     0     0     0     0     0     0
                                            7     0     0     0     0     0     0
                                            8     0     0     0     0     0     0

   3   1 0 1   1 ag    2    0   0   0   5   1     1     1     1     1     1     2
                                            2     0     0     0     0     0     0
                                            3     0     0     0     0     0     0
                                            4     0     0     0     0     0     0
                                            5     0     0     0     0     0     0
                                            6     0     0     0     0     0     0
                                            7     0     0     0     0     0     0
                                            8     0     0     0     0     0     0
 ........................................

   1   0 0 0   0       0    0   3   0   2   1     3     3     2     2     1     0
                                            2     1     1     1     1     0     0
                                            3     1     1     1     1     0     0
                                            4     1     1     1     1     0     0
                                            5     0     0     0     0     0     0
                                            6     0     0     0     0     0     0
                                            7     0     0     0     0     0     0
                                            8     0     0     0     0     0     0
 ........................................

 initial csf selection step:
 total number of walks in the drt, nwalk=       3
 keep all of these walks?(y,[n]) individual walks will be generated from the drt.
 apply orbital-group occupation restrictions?(y,[n]) apply reference occupation restrictions?(y,[n]) manually select individual walks?(y,[n])
 step-vector based csf selection complete.
        3 csfs selected from       3 total walks.

 beginning step-vector based csf selection.
 enter [step_vector/disposition] pairs:

 enter the active orbital step vector, (-1/ to end):

 step-vector based csf selection complete.
        3 csfs selected from       3 total walks.

 beginning numerical walk selection:
 enter positive walk numbers to add walks,
 negative walk numbers to delete walks, and zero to end.

 input walk number (0 to end) [  0]:
 final csf selection complete.
        3 csfs selected from       3 total walks.
  drt construction and csf selection complete.
 
 input a title card, default=mdrt2_title
  title                                                                         
  
 input a drt file name, default=mcdrtfl
 drt and indexing arrays written to file:
 /bigscratch/Columbus_C70/Cu_SO/WORK/mcdrtfl                                     
 
 write the drt file?([y],n) include step(*) vectors?([y],n) drt file is being written...


   List of selected configurations (step vectors)


   CSF#     1    3 3 1 3 3 3
   CSF#     2    3 1 3 3 3 3
   CSF#     3    1 3 3 3 3 3
