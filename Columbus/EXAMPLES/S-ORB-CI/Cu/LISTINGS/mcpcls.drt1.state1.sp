

     ******************************************
     **    PROGRAM:              MCPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 original author: Daniel Robertson, FSU
 later revisions: Ron Shepard, ANL;
                  Michal Dallos, University Vienna



 This Version of Program mcpc is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



   ******  File header section  ******

 Headers form the restart file:
    Cu atom                                                                         
    aoints SIFS file created by argos.      zam792            14:38:58.196 03-Nov-14
     title                                                                          
     title                                                                          
     title                                                                          
     title                                                                          


   ******  DRT info section  ******

 Informations for the DRT no.  1
 Header form the DRT file: 
     title                                                                          
 Molecular symmetry group:   sym1
 Total number of electrons:   19
 Spin multiplicity:            2
 Number of active orbitals:    6
 Number of active electrons:  11
 Total number of CSFs:         3

   ***  Informations from the DRT number:   1

 
 Symmetry orbital summary:
 Symm.blocks:         1     2     3     4     5     6     7     8
 Symm.labels:         ag    b1g   b2g   b3g   au    b1u   b2u   b3u

 List of doubly occupied orbitals:
  1 ag   1 b1u  1 b2u  1 b3u

 List of active orbitals:
  2 ag   3 ag   4 ag   1 b1g  1 b2g  1 b3g

 Informations for the DRT no.  2
 Header form the DRT file: 
     title                                                                          
 Molecular symmetry group:    b1g
 Total number of electrons:   19
 Spin multiplicity:            2
 Number of active orbitals:    6
 Number of active electrons:  11
 Total number of CSFs:         1

   ***  Informations from the DRT number:   2

 
 Symmetry orbital summary:
 Symm.blocks:         1     2     3     4     5     6     7     8
 Symm.labels:         ag    b1g   b2g   b3g   au    b1u   b2u   b3u

 List of doubly occupied orbitals:
  1 ag   1 b1u  1 b2u  1 b3u

 List of active orbitals:
  2 ag   3 ag   4 ag   1 b1g  1 b2g  1 b3g

 Informations for the DRT no.  3
 Header form the DRT file: 
     title                                                                          
 Molecular symmetry group:    b2g
 Total number of electrons:   19
 Spin multiplicity:            2
 Number of active orbitals:    6
 Number of active electrons:  11
 Total number of CSFs:         1

   ***  Informations from the DRT number:   3

 
 Symmetry orbital summary:
 Symm.blocks:         1     2     3     4     5     6     7     8
 Symm.labels:         ag    b1g   b2g   b3g   au    b1u   b2u   b3u

 List of doubly occupied orbitals:
  1 ag   1 b1u  1 b2u  1 b3u

 List of active orbitals:
  2 ag   3 ag   4 ag   1 b1g  1 b2g  1 b3g

 Informations for the DRT no.  4
 Header form the DRT file: 
     title                                                                          
 Molecular symmetry group:    b3g
 Total number of electrons:   19
 Spin multiplicity:            2
 Number of active orbitals:    6
 Number of active electrons:  11
 Total number of CSFs:         1

   ***  Informations from the DRT number:   4

 
 Symmetry orbital summary:
 Symm.blocks:         1     2     3     4     5     6     7     8
 Symm.labels:         ag    b1g   b2g   b3g   au    b1u   b2u   b3u

 List of doubly occupied orbitals:
  1 ag   1 b1u  1 b2u  1 b3u

 List of active orbitals:
  2 ag   3 ag   4 ag   1 b1g  1 b2g  1 b3g


   ******  MCSCF convergence information:  ******

 MCSCF convergence criteria were satisfied.

 mcscf energy=  -196.1455018772    nuclear repulsion=     0.0000000000
 demc=             0.0000000080    wnorm=                 0.0000001330
 knorm=            0.0000000050    apxde=                 0.0000000000


 MCSCF calculation performmed for   4 symmetries.

 State averaging:
 No,  ssym, navst, wavst
  1    ag     3   0.1667 0.1667 0.1667
  2    b1     1   0.1667
  3    b2     1   0.1667
  4    b3     1   0.1667

 Input the DRT No of interest: [  1]:
In the DRT No.: 1 there are  3 states.

 Which one to take? [  1]:
 The CSFs for the state No  1 of the symmetry  ag  will be printed
 according to the following print options :

 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 9) export wave function files for cioverlap (all states).
 0) end.

 input menu number [  0]: csfs will be printed based on coefficient magnitudes.

 input the coefficient threshold (end with 0.) [ 0.0000]:
 List of active orbitals:
  2 ag   3 ag   4 ag   1 b1g  1 b2g  1 b3g

   csf       coeff       coeff**2    step(*)
  -----  ------------  ------------  ------------
      2 -0.7163498095  0.5131570496  313333
      3  0.6642635782  0.4412461014  133333
      1 -0.2135341872  0.0455968491  331333

 input the coefficient threshold (end with 0.) [ 0.0000]:
 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 9) export wave function files for cioverlap (all states).
 0) end.

 input menu number [  0]: