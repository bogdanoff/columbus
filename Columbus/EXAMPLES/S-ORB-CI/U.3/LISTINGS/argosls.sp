echo of the argos input file:
 ------------------------------------------------------------------------
  U^(+4)  spherical symmetry as D2h  Ermler core,SO. zz/rmp compact bs
    1   1   4   4   4   0   0   0   0   1   1   0   0   0   1
   8  1 ag  1b1g  1b2g  1b3g  1 au  1b1u  1b2u  1b3u
   7
    2   3   4
    2   5   6
    2   7   8
    3   5   7
    3   6   8
    4   5   8
    4   6   7
     6    1    1    1    2    3    4
     3    6    7    8
     7    5    6    6    7    7    8    8
     9    1    1    1    2    2    3    3    4    4
     6    6    1
     1    1    1    0    0    0
    -1   -1    4    0    0    0
     1   -1    0    0    0    0
     0    0    0    1    0    0
     0    0    0    0    1    0
     0    0    0    0    0    1
     3    3    2
     0    0    1
     0    1    0
     1    0    0
     7   10    3
     0    0    0    0    0    0    0    0    0    1
     0    0    0    0    1    0   -1    0    0    0
     0    0    4    0   -9    0   -9    0    0    0
     0    0    0   -1    0    0    0    0    1    0
     0    4    0   -9    0    0    0    0   -9    0
     0    0    0    0    0    1    0   -1    0    0
     4    0    0    0    0   -9    0   -9    0    0
     9   15    4
     1    1    0    0    0    0    0    0    0  -36    0    0    0    0    0
    -1    1    0    0    0    0    0    0    0    0   36  -36    0    0    0
     9    9   64    0    0    0    0    0    0   36 -576 -576    0    0    0
     0    0    0   16    0  -16    0    0    0    0    0    0    0    0    0
     0    0    0   -1    0   -1    0    0    0    0    0    0    0    0   36
     0    0    0    0    1    0    0    0    0    0    0    0    0   -9    0
     0    0    0    0   -9    0    0   16    0    0    0    0    0   -9    0
     0    0    0    0    0    0   -1    0    0    0    0    0    9    0    0
     0    0    0    0    0    0   -9    0   16    0    0    0   -9    0    0
     4    3    3
       2.1680000       -0.1289505  -0.0195499   0.0000000
       1.0090000        0.7955080  -0.0090364   0.0000000
       0.4025000        0.3649706   0.5279641   0.0000000
       0.1398000        0.0020985   0.5899125   1.0000000
     4    2    2
       6.7280000       -0.0033035   0.0000000
       1.4190000       -0.3142991   0.0000000
       0.6199000        0.7755420   0.0000000
       0.2445000        0.4902717   1.0000000
     4    4    2
       4.4360000        0.1957684   0.0000000
       1.8600000        0.4559656   0.0000000
       0.7552000        0.4265113   0.0000000
       0.2770000        0.1970811   1.0000000
     1    5    1
       1.6900000        1.0000000
     4    4
     7
    1    605.9017000    -75.1803130
    2    169.5519000   -721.3347170
    2     49.8812000   -256.0487980
    2     17.9193000   -122.3916170
    2      6.1090000    -33.5488740
    2      2.6710000    -10.7746390
    2      1.2229000     -0.9516470
     9
    0    131.5121000      6.0092240
    1     43.8521000     87.2350920
    2     15.3593000    467.5695190
    2      9.7364000   -567.7867430
    2      6.3681000    867.3893430
    2      4.2889000   -931.6114500
    2      3.0496000    754.8096310
    2      2.3616000   -324.4824520
    2      2.0820000     86.9469990
     9
    0     89.1060000      8.6337080
    1     28.2983000     96.9114690
    2     10.0177000    434.2983400
    2      6.7113000   -618.6219480
    2      4.5103000    906.4494630
    2      3.0945000   -945.1226200
    2      2.2584000    760.6777950
    2      1.7721000   -372.3424070
    2      1.5561000    109.5291520
     9
    0     48.0590000      7.2291500
    1     15.9288000     70.2405240
    2      5.0372000    328.0785520
    2      3.7460000   -576.6828000
    2      2.6533000    775.7001950
    2      1.9168000   -796.7519530
    2      1.4458000    647.6846310
    2      1.1784000   -348.7743840
    2      1.0371000     98.6955570
     9
    0    137.5646000      8.0505080
    1     28.9229000    116.5257490
    2     18.2575000   -503.9859310
    2     11.6596000   1010.8484500
    2      7.3913000   -840.6480100
    2      4.8940000    617.6749270
    2      3.6708000   -301.1949770
    2      2.8469000     64.1408160
    2      0.9036000     -0.7763950
     9
    0     89.10600000      0.21023000
    1     28.29830000     -5.58758600
    2     10.01770000    119.00933800
    2      6.71130000   -341.58435000
    2      4.51030000    524.08306800
    2      3.09450000   -559.86242600
    2      2.25840000    379.89343200
    2      1.77210000   -173.29950000
    2      1.55610000     51.32477200
     9
    0     48.05900000      0.02589900
    1     15.92880000     -0.51043700
    2      5.03720000     18.46356200
    2      3.74600000    -53.72435400
    2      2.65330000     82.13493300
    2      1.91680000    -84.82173200
    2      1.44580000     57.01284000
    2      1.17840000    -25.30576100
    2      1.03710000      6.20543500
     9
    0    137.56460000     -0.00264600
    1     28.92290000      0.04407600
    2     18.25750000     -4.64630100
    2     11.65960000     12.07324200
    2      7.39130000    -16.72577800
    2      4.89400000     14.38314500
    2      3.67080000     -7.33831800
    2      2.84690000      1.60663300
    2      0.90360000     -0.02633900
     7
    1    605.90170000      4.45229400
    2    169.55190000      3.33260100
    2     49.88120000      1.55692200
    2     17.91930000      0.01046000
    2      6.10900000      0.08204500
    2      2.67100000      0.02279300
    2      1.22290000     -0.01628200
  U   4  114.
     0.00000000    0.00000000    0.00000000
    1   1
    2   2
    3   3
    4   4
    1
 ------------------------------------------------------------------------
                              program "argos" 5.9
                            columbus program system

             this program computes integrals over symmetry orbitals
               of generally contracted gaussian atomic orbitals.
                        programmed by russell m. pitzer

                           version date: 20-aug-2001

references:

    symmetry analysis (equal contributions):
    r. m. pitzer, j. chem. phys. 58, 3111 (1973).

    ao integral evaluation (hondo):
    m. dupuis, j. rys, and h. f. king, j. chem. phys. 65, 111 (1976).

    general contraction of gaussian orbitals:
    r. c. raffenetti, j. chem. phys. 58, 4452 (1973).

    core potential ao integrals (meldps):
    l. e. mcmurchie and e. r. davidson, j. comput. phys. 44, 289 (1981)

    spin-orbit and core potential integrals:
    r. m. pitzer and n. w. winter, int. j. quantum chem. 40, 773 (1991)

 This Version of Program ARGOS is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de

*********************** File revision status: ***********************
* argos1.f  Revision: 2.15.12.1         Date: 2013/04/11 14:37:29   * 
* argos2.f  Revision: 2.4.12.1          Date: 2013/04/11 14:37:29   * 
* argos3.f  Revision: 2.3.12.1           Date: 2013/04/11 14:37:29  * 
* argos4.f  Revision: 2.4.6.1           Date: 2013/04/11 14:37:29   * 
* argos5.f  Revision: 2.2.20.1          Date: 2013/04/11 14:37:29   * 
* argos6.f  Revision: 2.22.6.1          Date: 2013/04/11 14:37:29   * 
********************************************************************

 workspace allocation parameters: lcore=  32768000 mem1=         0 ifirst=         1

 filenames and unit numbers:
 unit description                   filename
 ---- -----------                   ----------
   6  listing file:                 argosls                                                     
   4  1-e integral file:            aoints                                                      
   8  2-e integral file [fsplit=2]: aoints2                                                     
   5  input file:                   argosin                                                     

 argos input parameters and titles:
 ngen   =  1 ns     =  1 naords =  4 ncons  =  4 ngcs   =  4 itol   = 20
 icut   =  9 aoints =  4 only1e =  0 inrm   =  1 ncrs   =  1
 l1rec  =         0      l2rec  =         0      aoint2 =  8 fsplit =  1

 U^(+4)  spherical symmetry as D2h  Ermler core,SO. zz/rmp compact bs           
aoints SIFS file created by argos.      LIN-CMFP2-1.LUNET 11:21:22.120 09-Oct-20


irrep            1       2       3       4       5       6       7       8
degeneracy       1       1       1       1       1       1       1       1
label            ag     b1g     b2g     b3g      au     b1u     b2u     b3u


direct product table
   ( ag) ( ag) =  ag
   ( ag) (b1g) = b1g
   ( ag) (b2g) = b2g
   ( ag) (b3g) = b3g
   ( ag) ( au) =  au
   ( ag) (b1u) = b1u
   ( ag) (b2u) = b2u
   ( ag) (b3u) = b3u
   (b1g) ( ag) = b1g
   (b1g) (b1g) =  ag
   (b1g) (b2g) = b3g
   (b1g) (b3g) = b2g
   (b1g) ( au) = b1u
   (b1g) (b1u) =  au
   (b1g) (b2u) = b3u
   (b1g) (b3u) = b2u
   (b2g) ( ag) = b2g
   (b2g) (b1g) = b3g
   (b2g) (b2g) =  ag
   (b2g) (b3g) = b1g
   (b2g) ( au) = b2u
   (b2g) (b1u) = b3u
   (b2g) (b2u) =  au
   (b2g) (b3u) = b1u
   (b3g) ( ag) = b3g
   (b3g) (b1g) = b2g
   (b3g) (b2g) = b1g
   (b3g) (b3g) =  ag
   (b3g) ( au) = b3u
   (b3g) (b1u) = b2u
   (b3g) (b2u) = b1u
   (b3g) (b3u) =  au
   ( au) ( ag) =  au
   ( au) (b1g) = b1u
   ( au) (b2g) = b2u
   ( au) (b3g) = b3u
   ( au) ( au) =  ag
   ( au) (b1u) = b1g
   ( au) (b2u) = b2g
   ( au) (b3u) = b3g
   (b1u) ( ag) = b1u
   (b1u) (b1g) =  au
   (b1u) (b2g) = b3u
   (b1u) (b3g) = b2u
   (b1u) ( au) = b1g
   (b1u) (b1u) =  ag
   (b1u) (b2u) = b3g
   (b1u) (b3u) = b2g
   (b2u) ( ag) = b2u
   (b2u) (b1g) = b3u
   (b2u) (b2g) =  au
   (b2u) (b3g) = b1u
   (b2u) ( au) = b2g
   (b2u) (b1u) = b3g
   (b2u) (b2u) =  ag
   (b2u) (b3u) = b1g
   (b3u) ( ag) = b3u
   (b3u) (b1g) = b2u
   (b3u) (b2g) = b1u
   (b3u) (b3g) =  au
   (b3u) ( au) = b3g
   (b3u) (b1u) = b2g
   (b3u) (b2u) = b1g
   (b3u) (b3u) =  ag


                     nuclear repulsion energy    0.00000000


primitive ao integrals neglected if exponential factor below 10**(-20)
contracted ao and so integrals neglected if value below 10**(- 9)
symmetry orbital integrals written on units  4  8


                                     U  atoms

                              nuclear charge  14.00

           center            x               y               z
             1             0.00000000      0.00000000      0.00000000

                    3d orbitals

 orbital exponents  contraction coefficients
    2.168000      -0.1289505      -1.9549901E-02    0.000000    
    1.009000       0.7955080      -9.0364003E-03    0.000000    
   0.4025000       0.3649706       0.5279641        0.000000    
   0.1398000       2.0985000E-03   0.5899125        1.000000    

                     symmetry orbital labels
                     1 ag1           4 ag1           7 ag1
                     2 ag1           5 ag1           8 ag1
                     3 ag1           6 ag1           9 ag1
                     1b1g1           2b1g1           3b1g1
                     1b2g1           2b2g1           3b2g1
                     1b3g1           2b3g1           3b3g1

           symmetry orbitals
 ctr, ao    ag1    ag1    ag1   b1g1   b2g1   b3g1
  1, 200  0.258 -0.289  0.500  0.000  0.000  0.000
  1, 020  0.258 -0.289 -0.500  0.000  0.000  0.000
  1, 002  0.258  0.577  0.000  0.000  0.000  0.000
  1, 110  0.000  0.000  0.000  1.000  0.000  0.000
  1, 101  0.000  0.000  0.000  0.000  1.000  0.000
  1, 011  0.000  0.000  0.000  0.000  0.000  1.000

                    2p orbitals

 orbital exponents  contraction coefficients
    6.728000      -3.3034999E-03    0.000000    
    1.419000      -0.3142991        0.000000    
   0.6199000       0.7755420        0.000000    
   0.2445000       0.4902717        1.000000    

                     symmetry orbital labels
                     1b1u1           2b1u1
                     1b2u1           2b2u1
                     1b3u1           2b3u1

           symmetry orbitals
 ctr, ao   b1u1   b2u1   b3u1
  1, 100  0.000  0.000  1.000
  1, 010  0.000  1.000  0.000
  1, 001  1.000  0.000  0.000

                    4f orbitals

 orbital exponents  contraction coefficients
    4.436000       0.1957684        0.000000    
    1.860000       0.4559656        0.000000    
   0.7552000       0.4265113        0.000000    
   0.2770000       0.1970811        1.000000    

                     symmetry orbital labels
                     1 au1           2 au1
                     3b1u1           5b1u1
                     4b1u1           6b1u1
                     3b2u1           5b2u1
                     4b2u1           6b2u1
                     3b3u1           5b3u1
                     4b3u1           6b3u1

           symmetry orbitals
 ctr, ao    au1   b1u1   b1u1   b2u1   b2u1   b3u1   b3u1
  1, 300  0.000  0.000  0.000  0.000  0.000  0.000  0.258
  1, 030  0.000  0.000  0.000  0.000  0.258  0.000  0.000
  1, 003  0.000  0.000  0.258  0.000  0.000  0.000  0.000
  1, 210  0.000  0.000  0.000 -0.500 -0.387  0.000  0.000
  1, 201  0.000  0.500 -0.387  0.000  0.000  0.000  0.000
  1, 120  0.000  0.000  0.000  0.000  0.000  0.500 -0.387
  1, 021  0.000 -0.500 -0.387  0.000  0.000  0.000  0.000
  1, 102  0.000  0.000  0.000  0.000  0.000 -0.500 -0.387
  1, 012  0.000  0.000  0.000  0.500 -0.387  0.000  0.000
  1, 111  1.000  0.000  0.000  0.000  0.000  0.000  0.000

                    5g orbitals

 orbital exponents  contraction coefficients
    1.690000        1.000000    

                     symmetry orbital labels
                    10 ag1
                    11 ag1
                    12 ag1
                     4b1g1
                     5b1g1
                     4b2g1
                     5b2g1
                     4b3g1
                     5b3g1

           symmetry orbitals
 ctr, ao    ag1    ag1    ag1   b1g1   b1g1   b2g1   b2g1   b3g1   b3g1
  1, 400  0.072 -0.055  0.037  0.000  0.000  0.000  0.000  0.000  0.000
  1, 040  0.072  0.055  0.037  0.000  0.000  0.000  0.000  0.000  0.000
  1, 004  0.000  0.000  0.098  0.000  0.000  0.000  0.000  0.000  0.000
  1, 310  0.000  0.000  0.000  0.289 -0.109  0.000  0.000  0.000  0.000
  1, 301  0.000  0.000  0.000  0.000  0.000  0.204 -0.231  0.000  0.000
  1, 130  0.000  0.000  0.000 -0.289 -0.109  0.000  0.000  0.000  0.000
  1, 031  0.000  0.000  0.000  0.000  0.000  0.000  0.000 -0.204 -0.231
  1, 103  0.000  0.000  0.000  0.000  0.000  0.000  0.309  0.000  0.000
  1, 013  0.000  0.000  0.000  0.000  0.000  0.000  0.000  0.000  0.309
  1, 220 -0.433  0.000  0.073  0.000  0.000  0.000  0.000  0.000  0.000
  1, 202  0.000  0.327 -0.293  0.000  0.000  0.000  0.000  0.000  0.000
  1, 022  0.000 -0.327 -0.293  0.000  0.000  0.000  0.000  0.000  0.000
  1, 211  0.000  0.000  0.000  0.000  0.000  0.000  0.000  0.612 -0.231
  1, 121  0.000  0.000  0.000  0.000  0.000 -0.612 -0.231  0.000  0.000
  1, 112  0.000  0.000  0.000  0.000  0.655  0.000  0.000  0.000  0.000


                                U  core potential

                                   g potential
                   powers      exponentials    coefficients
                     1           605.9017       -75.18031    
                     2           169.5519       -721.3347    
                     2           49.88120       -256.0488    
                     2           17.91930       -122.3916    
                     2           6.109000       -33.54887    
                     2           2.671000       -10.77464    
                     2           1.222900      -0.9516470    

                                 s - g potential
                   powers      exponentials    coefficients
                     0           131.5121        6.009224    
                     1           43.85210        87.23509    
                     2           15.35930        467.5695    
                     2           9.736400       -567.7867    
                     2           6.368100        867.3893    
                     2           4.288900       -931.6114    
                     2           3.049600        754.8096    
                     2           2.361600       -324.4825    
                     2           2.082000        86.94700    

                                 p - g potential
                   powers      exponentials    coefficients
                     0           89.10600        8.633708    
                     1           28.29830        96.91147    
                     2           10.01770        434.2983    
                     2           6.711300       -618.6219    
                     2           4.510300        906.4495    
                     2           3.094500       -945.1226    
                     2           2.258400        760.6778    
                     2           1.772100       -372.3424    
                     2           1.556100        109.5292    

                                 d - g potential
                   powers      exponentials    coefficients
                     0           48.05900        7.229150    
                     1           15.92880        70.24052    
                     2           5.037200        328.0786    
                     2           3.746000       -576.6828    
                     2           2.653300        775.7002    
                     2           1.916800       -796.7520    
                     2           1.445800        647.6846    
                     2           1.178400       -348.7744    
                     2           1.037100        98.69556    

                                 f - g potential
                   powers      exponentials    coefficients
                     0           137.5646        8.050508    
                     1           28.92290        116.5257    
                     2           18.25750       -503.9859    
                     2           11.65960        1010.848    
                     2           7.391300       -840.6480    
                     2           4.894000        617.6749    
                     2           3.670800       -301.1950    
                     2           2.846900        64.14082    
                     2          0.9036000      -0.7763950    


                             U  spin-orbit potential

                                   p potential
                   powers      exponentials    coefficients
                     0           89.10600       0.2102300    
                     1           28.29830       -5.587586    
                     2           10.01770        119.0093    
                     2           6.711300       -341.5843    
                     2           4.510300        524.0831    
                     2           3.094500       -559.8624    
                     2           2.258400        379.8934    
                     2           1.772100       -173.2995    
                     2           1.556100        51.32477    

                                   d potential
                   powers      exponentials    coefficients
                     0           48.05900       2.5899000E-02
                     1           15.92880      -0.5104370    
                     2           5.037200        18.46356    
                     2           3.746000       -53.72435    
                     2           2.653300        82.13493    
                     2           1.916800       -84.82173    
                     2           1.445800        57.01284    
                     2           1.178400       -25.30576    
                     2           1.037100        6.205435    

                                   f potential
                   powers      exponentials    coefficients
                     0           137.5646      -2.6460000E-03
                     1           28.92290       4.4076000E-02
                     2           18.25750       -4.646301    
                     2           11.65960        12.07324    
                     2           7.391300       -16.72578    
                     2           4.894000        14.38315    
                     2           3.670800       -7.338318    
                     2           2.846900        1.606633    
                     2          0.9036000      -2.6339000E-02

                                   g potential
                   powers      exponentials    coefficients
                     1           605.9017        4.452294    
                     2           169.5519        3.332601    
                     2           49.88120        1.556922    
                     2           17.91930       1.0460000E-02
                     2           6.109000       8.2045000E-02
                     2           2.671000       2.2793000E-02
                     2           1.222900      -1.6282000E-02

lx: b3g          ly: b2g          lz: b1g

output SIFS file header information:
 U^(+4)  spherical symmetry as D2h  Ermler core,SO. zz/rmp compact bs           
aoints SIFS file created by argos.      LIN-CMFP2-1.LUNET 11:21:22.120 09-Oct-20

output energy(*) values:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.

total core energy =  0.000000000000E+00

nsym = 8 nbft=  47

symmetry  =    1    2    3    4    5    6    7    8
slabel(*) =   ag  b1g  b2g  b3g   au  b1u  b2u  b3u
nbpsy(*)  =   12    5    5    5    2    6    6    6

info(*) =         1      4096      3272      4096      2700         0

output orbital labels, i:bfnlab(i)=
   1:  1_U_3s   2:  2_U_3d   3:  3_U_3d   4:  4_U_3s   5:  5_U_3d   6:  6_U_3d
   7:  7_U_3s   8:  8_U_3d   9:  9_U_3d  10: 10_U_5g  11: 11_U_5g  12: 12_U_5g
  13: 13_U_3d  14: 14_U_3d  15: 15_U_3d  16: 16_U_5g  17: 17_U_5g  18: 18_U_3d
  19: 19_U_3d  20: 20_U_3d  21: 21_U_5g  22: 22_U_5g  23: 23_U_3d  24: 24_U_3d
  25: 25_U_3d  26: 26_U_5g  27: 27_U_5g  28: 28_U_4f  29: 29_U_4f  30: 30_U_2p
  31: 31_U_2p  32: 32_U_4f  33: 33_U_4f  34: 34_U_4f  35: 35_U_4f  36: 36_U_2p
  37: 37_U_2p  38: 38_U_4f  39: 39_U_4f  40: 40_U_4f  41: 41_U_4f  42: 42_U_2p
  43: 43_U_2p  44: 44_U_4f  45: 45_U_4f  46: 46_U_4f  47: 47_U_4f

bfn_to_center map(*), i:map(i)
   1:  1   2:  1   3:  1   4:  1   5:  1   6:  1   7:  1   8:  1   9:  1  10:  1
  11:  1  12:  1  13:  1  14:  1  15:  1  16:  1  17:  1  18:  1  19:  1  20:  1
  21:  1  22:  1  23:  1  24:  1  25:  1  26:  1  27:  1  28:  1  29:  1  30:  1
  31:  1  32:  1  33:  1  34:  1  35:  1  36:  1  37:  1  38:  1  39:  1  40:  1
  41:  1  42:  1  43:  1  44:  1  45:  1  46:  1  47:  1

bfn_to_orbital_type map(*), i:map(i)
   1:  3   2:  4   3:  4   4:  3   5:  4   6:  4   7:  3   8:  4   9:  4  10:  9
  11:  9  12:  9  13:  4  14:  4  15:  4  16:  9  17:  9  18:  4  19:  4  20:  4
  21:  9  22:  9  23:  4  24:  4  25:  4  26:  9  27:  9  28:  6  29:  6  30:  2
  31:  2  32:  6  33:  6  34:  6  35:  6  36:  2  37:  2  38:  6  39:  6  40:  6
  41:  6  42:  2  43:  2  44:  6  45:  6  46:  6  47:  6


       47 symmetry orbitals,       ag:  12   b1g:   5   b2g:   5   b3g:   5
                                   au:   2   b1u:   6   b2u:   6   b3u:   6

 socfpd: mcxu=    40110 mcxu2=    32693 left= 32727890
 
oneint:    75 S1(*)    integrals were written in  1 records.
oneint:    75 T1(*)    integrals were written in  1 records.
oneint:    75 V1(*)    integrals were written in  1 records.
oneint:   114 X(*)     integrals were written in  1 records.
oneint:   114 Y(*)     integrals were written in  1 records.
oneint:    88 Z(*)     integrals were written in  1 records.
oneint:   114 Im(px)   integrals were written in  1 records.
oneint:   114 Im(py)   integrals were written in  1 records.
oneint:    88 Im(pz)   integrals were written in  1 records.
oneint:    58 Im(lx)   integrals were written in  1 records.
oneint:    58 Im(ly)   integrals were written in  1 records.
oneint:    46 Im(lz)   integrals were written in  1 records.
oneint:   168 XX(*)    integrals were written in  1 records.
oneint:   101 XY(*)    integrals were written in  1 records.
oneint:   111 XZ(*)    integrals were written in  1 records.
oneint:   168 YY(*)    integrals were written in  1 records.
oneint:   111 YZ(*)    integrals were written in  1 records.
oneint:   127 ZZ(*)    integrals were written in  1 records.
oneint:    75 Veff(*)  integrals were written in  1 records.
oneint:    58 Im(SO:x) integrals were written in  1 records.
oneint:    58 Im(SO:y) integrals were written in  1 records.
oneint:    46 Im(SO:z) integrals were written in  1 records.
 

twoint:       68031 1/r12    integrals and       19 pk flags
                                 were written in    26 records.

 twoint: maximum mblu needed =    211872
 
driver: 1-e  integral workspace high-water mark =     62279
driver: 2-e  integral workspace high-water mark =    273563
driver: overall argos workspace high-water mark =    273563
