1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      09/20/00    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

================================================================================
four external integ     .50 MB location: local disk    
three external inte     .25 MB location: local disk    
four external integ     .50 MB location: local disk    
three external inte     .25 MB location: local disk    
diagonal integrals      .12 MB location: local disk    
off-diagonal integr     .19 MB location: local disk    
 nsubmx= 20  lenci= 2729
global arrays:       111889   (     .85 MB)
vdisk:                    0   (     .00 MB)
drt:                 164500   (     .63 MB)
================================================================================
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core            9999999 DP per process
 CIUDG version 5.9.3 (05-Dec-2002)
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
      lvlprt=0, nbkitr=1,niter=1000,ivmode=0,
      csfprn=10, nroot =1 , nunitv= 1   ,
      nvcimx=20, davcor=0, noldv=0, noldhv=0 ,
      ftcalc=0, iden=0
      rtol=0.001,0.001,0.001,0.001,0.001,0.001,0.001,
           0.001,0.001,0.001
  &end
  
 ------------------------------------------------------------------------
 bummer (warning):invalid ftcalc<>1 .. setting to 1         0

 ** list of control variables **
 nrfitr =   30      nvrfmx =   16      nvrfmn =    1
 lvlprt =    0      nroot  =    1      noldv  =    0      noldhv =    0
 nunitv =    1      ntype  =    0      nbkitr =    1      niter  = 1000
 ivmode =    0      vout   =    0      istrt  =    0      iortls =    0
 nvbkmx =   16      ibktv  =   -1      ibkthv =   -1      frcsub =    0
 nvcimx =   16      icitv  =   -1      icithv =   -1      maxseg =    4
 iden   =    0      itran  =    0      froot  =    0      rtmode =    0
 ftcalc =    1      lrtshift=1.0000    ncouple=    1      skipso  =   F
 ncorel =    0      csfprn  =  10      ctol   = 1.00E-02  davcor  =   0


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-04    1.000E-03
 
 units and filenames:
    1: ( 6)    ciudgls                                                     
    2: ( 5)    ciudgin                                                     
    3: ( 7)    ciudgsm                                                     
    4: ( 4)    cihdiag                                                     
    5: ( 3)    cihpseu                                                     
    6: (10)    cihvfl                                                      
    7: (11)    civfl                                                       
    8: (12)    diagint                                                     
    9: (13)    ofdgint                                                     
   10: (17)    cidrtfl                                                     
   11: (20)    ciftdfl                                                     
   12: (21)    ciftofl                                                     
   13: (24)    ciftifl                                                     
   14: ( 8)    ciflind                                                     
   15: (15)    civin                                                       
   16: (16)    civout                                                      
   18: (18)    d1fl                                                        
   20: (53)    cifvfl                                                      
   21: (25)    ciscr4                                                      
   22: (41)    ciftotd                                                     
   23: (42)    flacpfd                                                     
   26: (26)    ciscr5                                                      
   27: (47)    restart                                                     
   28: (48)    civout                                                      
   29: (49)    cirefv                                                      
   31: (31)    fil4w                                                       
   32: (32)    fil4x                                                       
   33: (33)    fil3w                                                       
   34: (34)    fil3x                                                       
   36: (36)    aoints                                                      
   37: (37)    aoints2                                                     
   38: (38)    drtfil                                                      
   39: (50)    cisrtif                                                     
   42: (50)    mocoef                                                      
   45: (52)    nocoef_ci                                                   
   46: (53)    civfl_restart                                               
 ------------------------------------------------------------------------

 workspace allocation information: lcore=   9999999 mem1= 809998120 ifirst=    586376
 bummer (warning):spinorbit calculation: setting iden=0         0

 integral file titles:
  Xe (6s6p3d1f)->[4s4p3d1f],C2v                                                  
 aoints SIFS file created by argos.      zam403          Wed Aug 20 14:51:12 2003
 cidrt_title                                                                     
 mo coefficients generated by scfpq                                              
 SIFS file created by program tran.      zam403          Wed Aug 20 14:51:14 2003

 core energy values from the integral file:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -9.957605651774E+00, ietype=    6,   fcore energy of type: H1(*)   
 energy( 3)= -5.319091990105E+00, ietype=    5,   fcore energy of type: Vref(*) 

 total core repulsion energy = -1.527669764188E+01

 drt header information:
 cidrt_title                                                                     
 spnorb, spnodd, lxyzir,hmult T F 4 3 2 5
 nmot  =    46 niot  =     2 nfct  =     2 nfvt  =     0
 nrow  =    13 nsym  =     8 ssym  =     1 lenbuf=  1600
 nwalk,xbar:         33        1        8       18        6
 nvalwt,nvalw:       33        1        8       18        6
 ncsft:            2729
 total number of valid internal walks:      33
 nvalz,nvaly,nvalx,nvalw =        1       8      18       6

 cisrt info file parameters:
 file number  12 blocksize   4095
 mxbld   4095
 nd4ext,nd2ext,nd0ext  1806   168     6
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int    60228    11046     1380      108        3        0       24      294
 minbl4,minbl3,maxbl2   504   366   507
 maxbuf 32767
 number of external orbitals per symmetry block:  11   3   3   3   1   7   7   7
 nmsym   8 number of internal orbitals   2

 formula file title:
  Xe (6s6p3d1f)->[4s4p3d1f],C2v                                                  
 aoints SIFS file created by argos.      zam403          Wed Aug 20 14:51:12 2003
 cidrt_title                                                                     
 mo coefficients generated by scfpq                                              
 SIFS file created by program tran.      zam403          Wed Aug 20 14:51:14 2003
 cidrt_title                                                                     
 file nmb.  24 block size     0
 pthz,pthy,pthx,pthw:     1     8    18     6 total internal walks:      33
 maxlp3,n3xtlp,n2xtlp,n1xtlp,n0xtlp,n2lp,n1lp,n0lp     0     0     0     0
 orbsym(*)= 1 6

 setref:        1 references kept,
                0 references were marked as invalid, out of
                1 total.
 limcnvrt: found 1  valid internal walksout of  1  walks (skipping trailing invalids)
  ... adding  1  segmentation marks segtype= 1
 limcnvrt: found 8  valid internal walksout of  8  walks (skipping trailing invalids)
  ... adding  4  segmentation marks segtype= 2
 limcnvrt: found 18  valid internal walksout of  18  walks (skipping trailing invalids)
  ... adding  6  segmentation marks segtype= 3
 limcnvrt: found 6  valid internal walksout of  6  walks (skipping trailing invalids)
  ... adding  4  segmentation marks segtype= 4

 number of external paths / symmetry
 vertex x     127      98      98      98      74     122     122     122
 vertex w     169      98      98      98      74     122     122     122



                   segmentation summary for type diagonal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1           1|         1|         0|         1|         0|         1|
 -------------------------------------------------------------------------------
  Y 2           8|        42|         1|         8|         1|         2|
 -------------------------------------------------------------------------------
  X 3          18|      1908|        43|        18|         9|         3|
 -------------------------------------------------------------------------------
  W 4           6|       778|      1951|         6|        27|         4|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>      2729


 297 dimension of the ci-matrix ->>>      2729


 297 dimension of the ci-matrix ->>>         7

                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      18       1       1908          1      18       1
     2  4   1    25      two-ext wz   2X  4 1       6       1        778          1       6       1
     3  4   3    26      two-ext wx   2X  4 3       6      18        778       1908       6      18
     4  2   1    11      one-ext yz   1X  2 1       8       1         42          1       8       1
     5  3   2    15      1ex3ex  yx   3X  3 2      18       8       1908         42      18       8
     6  4   2    16      1ex3ex  yw   3X  4 2       6       8        778         42       6       8
     7  1   1     1      allint zz    OX  1 1       1       1          1          1       1       1
     8  2   2     5      0ex2ex yy    OX  2 2       8       8         42         42       8       8
     9  3   3     6      0ex2ex xx    OX  3 3      18      18       1908       1908      18      18
    10  4   4     7      0ex2ex ww    OX  4 4       6       6        778        778       6       6
    11  1   1    75      dg-024ext z  DG  1 1       1       1          1          1       1       1
    12  2   2    45      4exdg024 y   DG  2 2       8       8         42         42       8       8
    13  3   3    46      4exdg024 x   DG  3 3      18      18       1908       1908      18      18
    14  4   4    47      4exdg024 w   DG  4 4       6       6        778        778       6       6
    15  1   1    81      so-0ext zz   SO  1 1       1       1          1          1       1       1
    16  2   2    82      so-0ext yy   SO  2 2       8       8         42         42       8       8
    17  2   2   101      so-2ext yy   SO  2 2       8       8         42         42       8       8
    18  3   3    83      so-0ext xx   SO  3 3      18      18       1908       1908      18      18
    19  3   3   102      so-2ext xx   SO  3 3      18      18       1908       1908      18      18
    20  4   4    84      so-0ext ww   SO  4 4       6       6        778        778       6       6
    21  2   1    91      so-1ext zy   SO  2 1       8       1         42          1       8       1
    22  3   2    93      so-1ext yx   SO  3 2      18       8       1908         42      18       8
    23  4   2    94      so-1ext yw   SO  4 2       6       8        778         42       6       8
    24  4   3   103      so-2ext wx   SO  4 3       6      18        778       1908       6      18
----------------------------------------------------------------------------------------------------
 DIAGEL COUNTS: DG0X,DG2X,DG4X= 64 48 32
 diagonal elements written to file   4

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 0) unit vectors formed using lowest hdiag elements.            

      1 vectors will be written to unit 11 beginning with logical record   1
         initial trial vectors are constructed from only  z  walks.
 stdiag: initial vector  1: energy of csf   1 is      -15.2766976419
 genvec: trial vector  1 is unit matrix column     1
    ---------end of vector generation---------


         vector  1 from unit 11 written to unit 49 filename cirefv                                                      

         vector  1 from unit 11 written to unit 49 filename cirefv                                                      

 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=     1)...
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              2729
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:              16
 number of roots to converge:                             1
 number of iterations:                                    1
 residual norm convergence criteria:                .000100

          starting bk iteration   1

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        64 2x:        48 4x:        32
All internal counts: zz :         0 yy:         0 xx:         0 ww:         0
One-external counts: yz :         2 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         1 wz:         3 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         6 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         0    task #   2:         2    task #   3:         0    task #   4:         1
task #   5:         0    task #   6:         0    task #   7:        -1    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:        -1    task #  12:        11
task #  13:        25    task #  14:        10    task #  15:        -1    task #  16:         0
task #  17:         0    task #  18:         0    task #  19:         0    task #  20:         0
task #  21:         1    task #  22:         0    task #  23:         0    task #  24:         0

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -15.2766976419  0.0000E+00  4.5793E-02  3.5170E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .00    .00    .00    .00
    2   25    0    .01    .00    .00    .01
    3   26    0    .00    .00    .00    .00
    4   11    0    .00    .00    .00    .00
    5   15    0    .00    .00    .00    .00
    6   16    0    .00    .00    .00    .00
    7    1    0    .01    .00    .00    .01
    8    5    0    .00    .00    .00    .00
    9    6    0    .00    .00    .00    .00
   10    7    0    .00    .00    .00    .00
   11   75    0    .00    .00    .00    .00
   12   45    0    .00    .00    .00    .00
   13   46    0    .00    .00    .00    .00
   14   47    0    .00    .00    .00    .00
   15   81    0    .00    .00    .00    .00
   16   82    0    .00    .00    .00    .00
   17  101    0    .00    .00    .00    .00
   18   83    0    .00    .00    .00    .00
   19  102    0    .00    .00    .00    .00
   20   84    0    .00    .00    .00    .00
   21   91    0    .01    .00    .00    .01
   22   93    0    .00    .00    .00    .00
   23   94    0    .00    .00    .00    .00
   24  103    0    .00    .00    .00    .00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .000000
time for cinew                          .000000
time for eigenvalue solver              .000000
time for vector access                  .000000
================================================================
time spent in mult:                      .0300s 
time spent in multnx:                    .0300s 
integral transfer time:                  .0000s 
time spent for loop construction:        .0000s 
time for vector access in mult:          .0000s 
total time per CI iteration:             .0300s 

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -15.2766976419  0.0000E+00  4.5793E-02  3.5170E-01  1.0000E-04
 
 expansion vectors are not transformed.
 matrix-vector products are not transformed.

    2 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)

 from bk iterations: iconv=   1

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              2729
 number of initial trial vectors:                         2
 number of initial matrix-vector products:                1
 maximum dimension of the subspace vectors:              16
 number of roots to converge:                             1
 number of iterations:                                 1000
 residual norm convergence criteria:                .001000

          starting ci iteration   1

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        64 2x:        48 4x:        32
All internal counts: zz :         0 yy:         0 xx:         6 ww:         1
One-external counts: yz :         2 yx:        37 yw:        20
Two-external counts: yy :        12 ww:        12 xx:        45 xz:         1 wz:         3 wx:        19
Three-ext.   counts: yx :        64 yw:        41

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         6 yx:        79 yw:        30
SO-2ex       counts: yy :        12 xx:       296 wx:       304
====================================================================================================


 xx2xso2= 136


LOOPCOUNT per task:
task #   1:         0    task #   2:         2    task #   3:         7    task #   4:         1
task #   5:        13    task #   6:        10    task #   7:        -1    task #   8:        -1
task #   9:         1    task #  10:         0    task #  11:        -1    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:        -1    task #  16:         2
task #  17:         3    task #  18:         7    task #  19:         9    task #  20:         1
task #  21:         1    task #  22:        14    task #  23:         7    task #  24:         7

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1      .98947141     -.14472849

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -15.3217249119  4.5027E-02  7.2724E-04  4.4573E-02  1.0000E-03
 mr-sdci #  1  2    -13.1720760056 -2.1046E+00  0.0000E+00  1.2573E+00  1.0000E-03
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .01    .00    .00    .01
    2   25    0    .00    .00    .00    .00
    3   26    0    .01    .00    .00    .01
    4   11    0    .01    .00    .00    .01
    5   15    0    .01    .00    .00    .01
    6   16    0    .01    .00    .00    .01
    7    1    0    .01    .00    .00    .01
    8    5    0    .01    .00    .00    .01
    9    6    0    .01    .00    .00    .01
   10    7    0    .02    .00    .00    .02
   11   75    0    .00    .00    .00    .00
   12   45    0    .00    .00    .00    .00
   13   46    0    .00    .00    .00    .00
   14   47    0    .01    .00    .00    .00
   15   81    0    .01    .00    .00    .01
   16   82    0    .01    .00    .00    .01
   17  101    0    .00    .00    .00    .00
   18   83    0    .01    .00    .00    .01
   19  102    0    .01    .00    .00    .01
   20   84    0    .01    .00    .00    .01
   21   91    0    .00    .00    .00    .00
   22   93    0    .01    .00    .00    .00
   23   94    0    .01    .00    .00    .01
   24  103    0    .01    .00    .00    .01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .000000
time for cinew                          .000000
time for eigenvalue solver              .000000
time for vector access                  .000000
================================================================
time spent in mult:                      .1900s 
time spent in multnx:                    .1700s 
integral transfer time:                  .0000s 
time spent for loop construction:        .0000s 
time for vector access in mult:          .0000s 
total time per CI iteration:             .2000s 

          starting ci iteration   2

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        64 2x:        48 4x:        32
All internal counts: zz :         0 yy:         0 xx:         6 ww:         1
One-external counts: yz :         2 yx:        37 yw:        20
Two-external counts: yy :        12 ww:        12 xx:        45 xz:         1 wz:         3 wx:        19
Three-ext.   counts: yx :        64 yw:        41

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         6 yx:        79 yw:        30
SO-2ex       counts: yy :        12 xx:       296 wx:       304
====================================================================================================


 xx2xso2= 136


LOOPCOUNT per task:
task #   1:         0    task #   2:         2    task #   3:         7    task #   4:         1
task #   5:        13    task #   6:        10    task #   7:        -1    task #   8:        -1
task #   9:         1    task #  10:         0    task #  11:        -1    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:        -1    task #  16:         2
task #  17:         3    task #  18:         7    task #  19:         9    task #  20:         1
task #  21:         1    task #  22:        14    task #  23:         7    task #  24:         7

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1      .98876896      .12142637      .08712968

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1    -15.3224258670  7.0096E-04  1.5332E-05  6.1406E-03  1.0000E-03
 mr-sdci #  2  2    -13.5735919520  4.0152E-01  0.0000E+00  1.0890E+00  1.0000E-03
 mr-sdci #  2  3    -12.6954658140 -2.5812E+00  0.0000E+00  1.4211E+00  1.0000E-03
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .01    .00    .00    .01
    2   25    0    .00    .00    .00    .00
    3   26    0    .01    .00    .00    .01
    4   11    0    .00    .00    .00    .00
    5   15    0    .01    .00    .00    .01
    6   16    0    .01    .00    .00    .01
    7    1    0    .01    .00    .00    .01
    8    5    0    .01    .00    .00    .01
    9    6    0    .00    .00    .00    .00
   10    7    0    .02    .00    .00    .02
   11   75    0    .00    .00    .00    .00
   12   45    0    .00    .00    .00    .00
   13   46    0    .01    .00    .00    .01
   14   47    0    .00    .00    .00    .00
   15   81    0    .01    .00    .00    .01
   16   82    0    .01    .00    .00    .01
   17  101    0    .00    .00    .00    .00
   18   83    0    .01    .00    .00    .01
   19  102    0    .01    .00    .00    .01
   20   84    0    .01    .00    .00    .01
   21   91    0    .00    .00    .00    .00
   22   93    0    .01    .00    .00    .01
   23   94    0    .01    .00    .00    .01
   24  103    0    .01    .00    .00    .01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .000000
time for cinew                          .000000
time for eigenvalue solver              .000000
time for vector access                  .000000
================================================================
time spent in mult:                      .1700s 
time spent in multnx:                    .1700s 
integral transfer time:                  .0000s 
time spent for loop construction:        .0000s 
time for vector access in mult:          .0000s 
total time per CI iteration:             .1700s 

          starting ci iteration   3

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        64 2x:        48 4x:        32
All internal counts: zz :         0 yy:         0 xx:         6 ww:         1
One-external counts: yz :         2 yx:        37 yw:        20
Two-external counts: yy :        12 ww:        12 xx:        45 xz:         1 wz:         3 wx:        19
Three-ext.   counts: yx :        64 yw:        41

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         6 yx:        79 yw:        30
SO-2ex       counts: yy :        12 xx:       296 wx:       304
====================================================================================================


 xx2xso2= 136


LOOPCOUNT per task:
task #   1:         0    task #   2:         2    task #   3:         7    task #   4:         1
task #   5:        13    task #   6:        10    task #   7:        -1    task #   8:        -1
task #   9:         1    task #  10:         0    task #  11:        -1    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:        -1    task #  16:         2
task #  17:         3    task #  18:         7    task #  19:         9    task #  20:         1
task #  21:         1    task #  22:        14    task #  23:         7    task #  24:         7

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1      .98861341      .09219009     -.10333090      .05888329

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1    -15.3224424592  1.6592E-05  3.8581E-07  9.5348E-04  1.0000E-03
 mr-sdci #  3  2    -14.1929948353  6.1940E-01  0.0000E+00  8.9906E-01  1.0000E-03
 mr-sdci #  3  3    -12.7941077302  9.8642E-02  0.0000E+00  1.1380E+00  1.0000E-03
 mr-sdci #  3  4    -12.6834852795 -2.5932E+00  0.0000E+00  1.4073E+00  1.0000E-03
 

 mr-sdci  convergence criteria satisfied after  3 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1    -15.3224424592  1.6592E-05  3.8581E-07  9.5348E-04  1.0000E-03
 mr-sdci #  3  2    -14.1929948353  6.1940E-01  0.0000E+00  8.9906E-01  1.0000E-03
 mr-sdci #  3  3    -12.7941077302  9.8642E-02  0.0000E+00  1.1380E+00  1.0000E-03
 mr-sdci #  3  4    -12.6834852795 -2.5932E+00  0.0000E+00  1.4073E+00  1.0000E-03

####################CIUDGINFO####################

   ci vector at position   1 energy=  -15.322442459181

################END OF CIUDGINFO################

 
    1 of the   5 expansion vectors are transformed.
    1 of the   4 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
 maximum overlap with reference  1 (overlap= 0.988613407817815704 )

information on vector: 1from unit 11 written to unit 16filename civout                                                      


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -15.3224424592

                                                       internal orbitals

                                          level       1    2

                                          orbital     1   23

                                         symmetry     1    6

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1   .988613                        +-   +- 
 y   3  2      10  -.022549              1(   8)   +-   +  
 y   3  2      11  -.027000              2(   8)   +-   +  
 y   3  2      14   .023104              5(   8)   +-   +  
 y   3  3      17   .022549              1(   7)   +-   +  
 y   3  3      18   .027000              2(   7)   +-   +  
 y   3  3      21  -.023104              5(   7)   +-   +  
 x   1  1     357  -.010221    6(   1)   2(   6)    -    - 
 w   1  1    1957  -.036990    3(   1)   3(   1)   +-      
 w   1  1    1960   .040396    3(   1)   4(   1)   +-      
 w   1  1    1961  -.027738    4(   1)   4(   1)   +-      
 w   1  1    1976  -.012693    4(   1)   7(   1)   +-      
 w   1  1    2024  -.027597    1(   3)   1(   3)   +-      
 w   1  1    2025   .030487    1(   3)   2(   3)   +-      
 w   1  1    2026  -.021019    2(   3)   2(   3)   +-      
 w   1  1    2030  -.027597    1(   4)   1(   4)   +-      
 w   1  1    2031   .030487    1(   4)   2(   4)   +-      
 w   1  1    2032  -.021019    2(   4)   2(   4)   +-      
 w   1  1    2038  -.016106    1(   6)   2(   6)   +-      
 w   1  1    2039  -.017381    2(   6)   2(   6)   +-      
 w   1  1    2042  -.014686    3(   6)   3(   6)   +-      
 w   1  1    2048   .013245    2(   6)   5(   6)   +-      
 w   1  1    2122  -.012354    2(   2)   1(   5)   +     - 
 w   1  1    2135   .013130    1(   1)   2(   6)   +     - 
 w   1  1    2138   .012097    4(   1)   2(   6)   +     - 
 w   1  1    2140   .012098    6(   1)   2(   6)   +     - 
 w   1  1    2148  -.016094    3(   1)   3(   6)   +     - 
 w   1  1    2149   .019192    4(   1)   3(   6)   +     - 
 w   1  1    2161  -.012354    5(   1)   4(   6)   +     - 
 w   1  1    2173  -.013650    6(   1)   5(   6)   +     - 
 w   1  1    2205  -.010100    2(   4)   2(   7)   +     - 
 w   1  1    2207  -.011498    1(   4)   3(   7)   +     - 
 w   1  1    2208   .013818    2(   4)   3(   7)   +     - 
 w   1  1    2211   .010702    2(   4)   4(   7)   +     - 
 w   1  1    2226  -.010100    2(   3)   2(   8)   +     - 
 w   1  1    2228  -.011498    1(   3)   3(   8)   +     - 
 w   1  1    2229   .013818    2(   3)   3(   8)   +     - 
 w   1  1    2232   .010702    2(   3)   4(   8)   +     - 

 ci coefficient statistics:
           rq > 0.1                1
      0.1> rq > 0.01              37
     0.01> rq > 0.001            210
    0.001> rq > 0.0001           399
   0.0001> rq > 0.00001          650
  0.00001> rq > 0.000001         544
 0.000001> rq                    888
           all                  2729
