echo of the argos input file:
 ------------------------------------------------------------------------
  Eu(Eu+3/E+2 avg)  spherical symmetry as D2h  Ermler core,SO. srb bs c
   1  1  4  4  4  0  0  0  0  1  1  0  0  0  1
   8  1 ag  1b1g  1b2g  1b3g  1 au  1b1u  1b2u  1b3u
   7
   2  3  4
   2  5  6
   2  7  8
   3  5  7
   3  6  8
   4  5  8
   4  6  7
     6    1    1    1    2    3    4
     3    6    7    8
     7    5    6    6    7    7    8    8
     9    1    1    1    2    2    3    3    4    4
     6    6    1
     1    1    1    0    0    0
    -1   -1    4    0    0    0
     1   -1    0    0    0    0
     0    0    0    1    0    0
     0    0    0    0    1    0
     0    0    0    0    0    1
     3    3    2
     0    0    1
     0    1    0
     1    0    0
     7   10    3
     0    0    0    0    0    0    0    0    0    1
     0    0    0    0    1    0   -1    0    0    0
     0    0    4    0   -9    0   -9    0    0    0
     0    0    0   -1    0    0    0    0    1    0
     0    4    0   -9    0    0    0    0   -9    0
     0    0    0    0    0    1    0   -1    0    0
     4    0    0    0    0   -9    0   -9    0    0
     9   15    4
     1    1    0    0    0    0    0    0    0  -36    0    0    0    0    0
    -1    1    0    0    0    0    0    0    0    0   36  -36    0    0    0
     9    9   64    0    0    0    0    0    0   36 -576 -576    0    0    0
     0    0    0   16    0  -16    0    0    0    0    0    0    0    0    0
     0    0    0   -1    0   -1    0    0    0    0    0    0    0    0   36
     0    0    0    0    1    0    0    0    0    0    0    0    0   -9    0
     0    0    0    0   -9    0    0   16    0    0    0    0    0   -9    0
     0    0    0    0    0    0   -1    0    0    0    0    0    9    0    0
     0    0    0    0    0    0   -9    0   16    0    0    0   -9    0    0
     5    3    4
       2.7980000        -.0762040    .0190910   -.0242985    .0000000
       1.2070000         .6766533   -.2311595    .0721271    .0000000
        .4672000         .4514026   -.0649069    .5489359    .0000000
        .1388000         .0150650    .5520045    .5503919   1.0000000
        .0462800        -.0004574    .5814318    .0055554    .0000000
     6    2    4
      22.0400000        -.0022989    .0006192    .0000000    .0000000
       1.7740000        -.2464627    .0546326    .0000000    .0000000
        .7814000         .6853267   -.1667537    .0000000    .0000000
        .2806000         .5432780   -.1779390   1.0000000    .0000000
        .0574000         .0155093    .4845824    .0000000    .0000000
        .0220800        -.0032987    .6226101    .0000000   1.0000000
     7    4    2
     117.3000000         .0081307    .0000000
      39.3792287         .0529186    .0000000
      15.5729960         .1748484    .0000000
       6.6531120         .3314254    .0000000
       2.8378087         .3955707    .0000000
       1.1553320         .3093342   1.0000000
        .4229580         .1371208    .0000000
     1    5    1
       3.8600000        1.0000000
     3    3
     6
    2      1.5367000     -1.4815710
    2      3.7249000     -7.6930980
    2      7.2920000    -22.8010060
    2     16.9618000    -52.7907030
    2     49.9249000   -174.6073000
    1    152.3949000    -33.4477420
     8
    2      1.4729000    -29.1086220
    2      1.7186000     96.4794080
    2      2.2814000   -184.9956510
    2      3.3670000    352.8970640
    2      5.2524000   -337.1682740
    2      8.2126000    266.1636660
    1     24.8215000     23.8750550
    0     22.9765000      6.6506390
     8
    2      1.3230000    -37.4066770
    2      1.5106000    109.7762530
    2      1.9762000   -196.6241300
    2      2.8343000    348.3294370
    2      4.3182000   -352.8189700
    2      6.4156000    244.0931850
    1     18.5803000     33.1438520
    0     31.1657000      5.3344070
     8
    2      1.2068000     38.3016970
    2      1.3952000   -117.4455030
    2      1.8200000    214.8863070
    2      2.5877000   -257.0061340
    2      3.7942000    257.9661250
    2      5.2445000    -91.2683410
    1      9.2309000     48.1772540
    0     29.1258000      6.8921010
     8
    2      1.32300000     20.90628800
    2      1.51060000    -54.43438000
    2      1.97620000     73.03154000
    2      2.83430000    -60.54635600
    2      4.31820000     20.76608600
    2      6.41560000      4.78851600
    1     18.58030000      -.80926800
    0     31.16570000       .10879800
     8
    2      1.20680000     -8.47253300
    2      1.39520000     24.04347000
    2      1.82000000    -33.08107000
    2      2.58770000     29.65918000
    2      3.79420000    -12.49863600
    2      5.24450000       .64346800
    1      9.23090000       .72122000
    0     29.12580000      -.02620200
     6
    2      1.53670000      -.02943300
    2      3.72490000       .10635000
    2      7.29200000      -.12410200
    2     16.96180000       .26681100
    2     49.92490000      -.10040600
    1    152.39490000       .83438700
  Eu  4  117.
      .00000000     .00000000     .00000000
   1  1
   2  2
   3  3
   4  4
   1
  
 ------------------------------------------------------------------------
                              program "argos" 5.4
                            columbus program system

             this program computes integrals over symmetry orbitals
               of generally contracted gaussian atomic orbitals.
                        programmed by russell m. pitzer

                           version date: 03-feb-1999

references:

    symmetry analysis (equal contributions):
    r. m. pitzer, j. chem. phys. 58, 3111 (1973).

    ao integral evaluation (hondo):
    m. dupuis, j. rys, and h. f. king, j. chem. phys. 65, 111 (1976).

    general contraction of gaussian orbitals:
    r. c. raffenetti, j. chem. phys. 58, 4452 (1973).

    core potential ao integrals (meldps):
    l. e. mcmurchie and e. r. davidson, j. comput. phys. 44, 289 (1981)

    spin-orbit and core potential integrals:
    r. m. pitzer and n. w. winter, int. j. quantum chem. 40, 773 (1991)

 This Version of Program ARGOS is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at


 workspace allocation parameters: lcore=  12000000 mem1= 806696856 ifirst=    170800

 filenames and unit numbers:
 unit description                   filename
 ---- -----------                   ----------
   6  listing file:                 argosls                                                     
   4  1-e integral file:            aoints                                                      
   8  2-e integral file [fsplit=2]: aoints2                                                     
   5  input file:                   argosin                                                     

 argos input parameters and titles:
 ngen   =  1 ns     =  1 naords =  4 ncons  =  4 ngcs   =  4 itol   = 20
 icut   =  9 aoints =  4 only1e =  0 inrm   =  1 ncrs   =  1
 l1rec  =         0      l2rec  =         0      aoint2 =  8 fsplit =  1

 Eu(Eu+3/E+2 avg)  spherical symmetry as D2h  Ermler core,SO. srb bs c          
aoints SIFS file created by argos.      zam403          Wed Aug 20 13:18:32 2003


irrep            1       2       3       4       5       6       7       8
degeneracy       1       1       1       1       1       1       1       1
label            ag     b1g     b2g     b3g      au     b1u     b2u     b3u


direct product table
   ( ag) ( ag) =  ag
   ( ag) (b1g) = b1g
   ( ag) (b2g) = b2g
   ( ag) (b3g) = b3g
   ( ag) ( au) =  au
   ( ag) (b1u) = b1u
   ( ag) (b2u) = b2u
   ( ag) (b3u) = b3u
   (b1g) ( ag) = b1g
   (b1g) (b1g) =  ag
   (b1g) (b2g) = b3g
   (b1g) (b3g) = b2g
   (b1g) ( au) = b1u
   (b1g) (b1u) =  au
   (b1g) (b2u) = b3u
   (b1g) (b3u) = b2u
   (b2g) ( ag) = b2g
   (b2g) (b1g) = b3g
   (b2g) (b2g) =  ag
   (b2g) (b3g) = b1g
   (b2g) ( au) = b2u
   (b2g) (b1u) = b3u
   (b2g) (b2u) =  au
   (b2g) (b3u) = b1u
   (b3g) ( ag) = b3g
   (b3g) (b1g) = b2g
   (b3g) (b2g) = b1g
   (b3g) (b3g) =  ag
   (b3g) ( au) = b3u
   (b3g) (b1u) = b2u
   (b3g) (b2u) = b1u
   (b3g) (b3u) =  au
   ( au) ( ag) =  au
   ( au) (b1g) = b1u
   ( au) (b2g) = b2u
   ( au) (b3g) = b3u
   ( au) ( au) =  ag
   ( au) (b1u) = b1g
   ( au) (b2u) = b2g
   ( au) (b3u) = b3g
   (b1u) ( ag) = b1u
   (b1u) (b1g) =  au
   (b1u) (b2g) = b3u
   (b1u) (b3g) = b2u
   (b1u) ( au) = b1g
   (b1u) (b1u) =  ag
   (b1u) (b2u) = b3g
   (b1u) (b3u) = b2g
   (b2u) ( ag) = b2u
   (b2u) (b1g) = b3u
   (b2u) (b2g) =  au
   (b2u) (b3g) = b1u
   (b2u) ( au) = b2g
   (b2u) (b1u) = b3g
   (b2u) (b2u) =  ag
   (b2u) (b3u) = b1g
   (b3u) ( ag) = b3u
   (b3u) (b1g) = b2u
   (b3u) (b2g) = b1u
   (b3u) (b3g) =  au
   (b3u) ( au) = b3g
   (b3u) (b1u) = b2g
   (b3u) (b2u) = b1g
   (b3u) (b3u) =  ag


                     nuclear repulsion energy     .00000000


primitive ao integrals neglected if exponential factor below 10**(-20)
contracted ao and so integrals neglected if value below 10**(- 9)
symmetry orbital integrals written on units  4  8


                                     Eu atoms

                              nuclear charge  17.00

           center            x               y               z
             1              .00000000       .00000000       .00000000

                    3d orbitals

 orbital exponents  contraction coefficients
    2.798000      -7.6204000E-02   1.9091000E-02  -2.4298500E-02   0.0000000E+00
    1.207000        .6766533       -.2311595       7.2127099E-02   0.0000000E+00
    .4672000        .4514026      -6.4906901E-02    .5489359       0.0000000E+00
    .1388000       1.5065000E-02    .5520045        .5503919        1.000000    
   4.6280000E-02  -4.5740000E-04    .5814318       5.5553999E-03   0.0000000E+00

                     symmetry orbital labels
                     1 ag1           4 ag1           7 ag1          10 ag1
                     2 ag1           5 ag1           8 ag1          11 ag1
                     3 ag1           6 ag1           9 ag1          12 ag1
                     1b1g1           2b1g1           3b1g1           4b1g1
                     1b2g1           2b2g1           3b2g1           4b2g1
                     1b3g1           2b3g1           3b3g1           4b3g1

           symmetry orbitals
 ctr, ao    ag1    ag1    ag1   b1g1   b2g1   b3g1
  1, 200   .258  -.289   .500   .000   .000   .000
  1, 020   .258  -.289  -.500   .000   .000   .000
  1, 002   .258   .577   .000   .000   .000   .000
  1, 110   .000   .000   .000  1.000   .000   .000
  1, 101   .000   .000   .000   .000  1.000   .000
  1, 011   .000   .000   .000   .000   .000  1.000

                    2p orbitals

 orbital exponents  contraction coefficients
    22.04000      -2.2988999E-03   6.1920003E-04   0.0000000E+00   0.0000000E+00
    1.774000       -.2464627       5.4632603E-02   0.0000000E+00   0.0000000E+00
    .7814000        .6853267       -.1667537       0.0000000E+00   0.0000000E+00
    .2806000        .5432780       -.1779390        1.000000       0.0000000E+00
   5.7400000E-02   1.5509300E-02    .4845824       0.0000000E+00   0.0000000E+00
   2.2080000E-02  -3.2986999E-03    .6226101       0.0000000E+00    1.000000    

                     symmetry orbital labels
                     1b1u1           2b1u1           3b1u1           4b1u1
                     1b2u1           2b2u1           3b2u1           4b2u1
                     1b3u1           2b3u1           3b3u1           4b3u1

           symmetry orbitals
 ctr, ao   b1u1   b2u1   b3u1
  1, 100   .000   .000  1.000
  1, 010   .000  1.000   .000
  1, 001  1.000   .000   .000

                    4f orbitals

 orbital exponents  contraction coefficients
    117.3000       8.1306997E-03   0.0000000E+00
    39.37923       5.2918598E-02   0.0000000E+00
    15.57300        .1748484       0.0000000E+00
    6.653112        .3314254       0.0000000E+00
    2.837809        .3955707       0.0000000E+00
    1.155332        .3093342        1.000000    
    .4229580        .1371208       0.0000000E+00

                     symmetry orbital labels
                     1 au1           2 au1
                     5b1u1           7b1u1
                     6b1u1           8b1u1
                     5b2u1           7b2u1
                     6b2u1           8b2u1
                     5b3u1           7b3u1
                     6b3u1           8b3u1

           symmetry orbitals
 ctr, ao    au1   b1u1   b1u1   b2u1   b2u1   b3u1   b3u1
  1, 300   .000   .000   .000   .000   .000   .000   .258
  1, 030   .000   .000   .000   .000   .258   .000   .000
  1, 003   .000   .000   .258   .000   .000   .000   .000
  1, 210   .000   .000   .000  -.500  -.387   .000   .000
  1, 201   .000   .500  -.387   .000   .000   .000   .000
  1, 120   .000   .000   .000   .000   .000   .500  -.387
  1, 021   .000  -.500  -.387   .000   .000   .000   .000
  1, 102   .000   .000   .000   .000   .000  -.500  -.387
  1, 012   .000   .000   .000   .500  -.387   .000   .000
  1, 111  1.000   .000   .000   .000   .000   .000   .000

                    5g orbitals

 orbital exponents  contraction coefficients
    3.860000        1.000000    

                     symmetry orbital labels
                    13 ag1
                    14 ag1
                    15 ag1
                     5b1g1
                     6b1g1
                     5b2g1
                     6b2g1
                     5b3g1
                     6b3g1

           symmetry orbitals
 ctr, ao    ag1    ag1    ag1   b1g1   b1g1   b2g1   b2g1   b3g1   b3g1
  1, 400   .072  -.055   .037   .000   .000   .000   .000   .000   .000
  1, 040   .072   .055   .037   .000   .000   .000   .000   .000   .000
  1, 004   .000   .000   .098   .000   .000   .000   .000   .000   .000
  1, 310   .000   .000   .000   .289  -.109   .000   .000   .000   .000
  1, 301   .000   .000   .000   .000   .000   .204  -.231   .000   .000
  1, 130   .000   .000   .000  -.289  -.109   .000   .000   .000   .000
  1, 031   .000   .000   .000   .000   .000   .000   .000  -.204  -.231
  1, 103   .000   .000   .000   .000   .000   .000   .309   .000   .000
  1, 013   .000   .000   .000   .000   .000   .000   .000   .000   .309
  1, 220  -.433   .000   .073   .000   .000   .000   .000   .000   .000
  1, 202   .000   .327  -.293   .000   .000   .000   .000   .000   .000
  1, 022   .000  -.327  -.293   .000   .000   .000   .000   .000   .000
  1, 211   .000   .000   .000   .000   .000   .000   .000   .612  -.231
  1, 121   .000   .000   .000   .000   .000  -.612  -.231   .000   .000
  1, 112   .000   .000   .000   .000   .655   .000   .000   .000   .000


                                Eu core potential

                                   f potential
                   powers      exponentials    coefficients
                     2           1.536700       -1.481571    
                     2           3.724900       -7.693098    
                     2           7.292000       -22.80101    
                     2           16.96180       -52.79070    
                     2           49.92490       -174.6073    
                     1           152.3949       -33.44774    

                                 s - f potential
                   powers      exponentials    coefficients
                     2           1.472900       -29.10862    
                     2           1.718600        96.47941    
                     2           2.281400       -184.9957    
                     2           3.367000        352.8971    
                     2           5.252400       -337.1683    
                     2           8.212600        266.1637    
                     1           24.82150        23.87505    
                     0           22.97650        6.650639    

                                 p - f potential
                   powers      exponentials    coefficients
                     2           1.323000       -37.40668    
                     2           1.510600        109.7763    
                     2           1.976200       -196.6241    
                     2           2.834300        348.3294    
                     2           4.318200       -352.8190    
                     2           6.415600        244.0932    
                     1           18.58030        33.14385    
                     0           31.16570        5.334407    

                                 d - f potential
                   powers      exponentials    coefficients
                     2           1.206800        38.30170    
                     2           1.395200       -117.4455    
                     2           1.820000        214.8863    
                     2           2.587700       -257.0061    
                     2           3.794200        257.9661    
                     2           5.244500       -91.26834    
                     1           9.230900        48.17725    
                     0           29.12580        6.892101    


                             Eu spin-orbit potential

                                   p potential
                   powers      exponentials    coefficients
                     2           1.323000        20.90629    
                     2           1.510600       -54.43438    
                     2           1.976200        73.03154    
                     2           2.834300       -60.54636    
                     2           4.318200        20.76609    
                     2           6.415600        4.788516    
                     1           18.58030       -.8092680    
                     0           31.16570        .1087980    

                                   d potential
                   powers      exponentials    coefficients
                     2           1.206800       -8.472533    
                     2           1.395200        24.04347    
                     2           1.820000       -33.08107    
                     2           2.587700        29.65918    
                     2           3.794200       -12.49864    
                     2           5.244500        .6434680    
                     1           9.230900        .7212200    
                     0           29.12580      -2.6202000E-02

                                   f potential
                   powers      exponentials    coefficients
                     2           1.536700      -2.9433000E-02
                     2           3.724900        .1063500    
                     2           7.292000       -.1241020    
                     2           16.96180        .2668110    
                     2           49.92490       -.1004060    
                     1           152.3949        .8343870    

lx: b3g          ly: b2g          lz: b1g

output SIFS file header information:
 Eu(Eu+3/E+2 avg)  spherical symmetry as D2h  Ermler core,SO. srb bs c          
aoints SIFS file created by argos.      zam403          Wed Aug 20 13:18:32 2003

output energy(*) values:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.

total core energy =  0.000000000000E+00

nsym = 8 nbft=  59

symmetry  =    1    2    3    4    5    6    7    8
slabel(*) =   ag  b1g  b2g  b3g   au  b1u  b2u  b3u
nbpsy(*)  =   15    6    6    6    2    8    8    8

info(*) =         1      4096      3272      4096      2700

output orbital labels, i:bfnlab(i)=
   1:  1_Eu3s   2:  2_Eu3d   3:  3_Eu3d   4:  4_Eu3s   5:  5_Eu3d   6:  6_Eu3d
   7:  7_Eu3s   8:  8_Eu3d   9:  9_Eu3d  10: 10_Eu3s  11: 11_Eu3d  12: 12_Eu3d
  13: 13_Eu5g  14: 14_Eu5g  15: 15_Eu5g  16: 16_Eu3d  17: 17_Eu3d  18: 18_Eu3d
  19: 19_Eu3d  20: 20_Eu5g  21: 21_Eu5g  22: 22_Eu3d  23: 23_Eu3d  24: 24_Eu3d
  25: 25_Eu3d  26: 26_Eu5g  27: 27_Eu5g  28: 28_Eu3d  29: 29_Eu3d  30: 30_Eu3d
  31: 31_Eu3d  32: 32_Eu5g  33: 33_Eu5g  34: 34_Eu4f  35: 35_Eu4f  36: 36_Eu2p
  37: 37_Eu2p  38: 38_Eu2p  39: 39_Eu2p  40: 40_Eu4f  41: 41_Eu4f  42: 42_Eu4f
  43: 43_Eu4f  44: 44_Eu2p  45: 45_Eu2p  46: 46_Eu2p  47: 47_Eu2p  48: 48_Eu4f
  49: 49_Eu4f  50: 50_Eu4f  51: 51_Eu4f  52: 52_Eu2p  53: 53_Eu2p  54: 54_Eu2p
  55: 55_Eu2p  56: 56_Eu4f  57: 57_Eu4f  58: 58_Eu4f  59: 59_Eu4f

bfn_to_center map(*), i:map(i)
   1:  1   2:  1   3:  1   4:  1   5:  1   6:  1   7:  1   8:  1   9:  1  10:  1
  11:  1  12:  1  13:  1  14:  1  15:  1  16:  1  17:  1  18:  1  19:  1  20:  1
  21:  1  22:  1  23:  1  24:  1  25:  1  26:  1  27:  1  28:  1  29:  1  30:  1
  31:  1  32:  1  33:  1  34:  1  35:  1  36:  1  37:  1  38:  1  39:  1  40:  1
  41:  1  42:  1  43:  1  44:  1  45:  1  46:  1  47:  1  48:  1  49:  1  50:  1
  51:  1  52:  1  53:  1  54:  1  55:  1  56:  1  57:  1  58:  1  59:  1

bfn_to_orbital_type map(*), i:map(i)
   1:  3   2:  4   3:  4   4:  3   5:  4   6:  4   7:  3   8:  4   9:  4  10:  3
  11:  4  12:  4  13:  9  14:  9  15:  9  16:  4  17:  4  18:  4  19:  4  20:  9
  21:  9  22:  4  23:  4  24:  4  25:  4  26:  9  27:  9  28:  4  29:  4  30:  4
  31:  4  32:  9  33:  9  34:  6  35:  6  36:  2  37:  2  38:  2  39:  2  40:  6
  41:  6  42:  6  43:  6  44:  2  45:  2  46:  2  47:  2  48:  6  49:  6  50:  6
  51:  6  52:  2  53:  2  54:  2  55:  2  56:  6  57:  6  58:  6  59:  6


       59 symmetry orbitals,       ag:  15   b1g:   6   b2g:   6   b3g:   6
                                   au:   2   b1u:   8   b2u:   8   b3u:   8
 !timer: syminp required                     user=      .010 walltime=      .000

 socfpd: mcxu=    40110 mcxu2=    32693 left= 11959890
 !timer: socfpd required                     user=      .010 walltime=      .000
 
oneint:   120 S1(*)    integrals were written in  1 records.
oneint:   120 T1(*)    integrals were written in  1 records.
oneint:   120 V1(*)    integrals were written in  1 records.
oneint:   182 X(*)     integrals were written in  1 records.
oneint:   182 Y(*)     integrals were written in  1 records.
oneint:   142 Z(*)     integrals were written in  1 records.
oneint:   182 Im(px)   integrals were written in  1 records.
oneint:   182 Im(py)   integrals were written in  1 records.
oneint:   142 Im(pz)   integrals were written in  1 records.
oneint:    91 Im(lx)   integrals were written in  1 records.
oneint:    91 Im(ly)   integrals were written in  1 records.
oneint:    72 Im(lz)   integrals were written in  1 records.
oneint:   265 XX(*)    integrals were written in  1 records.
oneint:   162 XY(*)    integrals were written in  1 records.
oneint:   179 XZ(*)    integrals were written in  1 records.
oneint:   265 YY(*)    integrals were written in  1 records.
oneint:   179 YZ(*)    integrals were written in  1 records.
oneint:   204 ZZ(*)    integrals were written in  1 records.
oneint:   120 Veff(*)  integrals were written in  1 records.
oneint:    84 Im(SO:x) integrals were written in  1 records.
oneint:    84 Im(SO:y) integrals were written in  1 records.
oneint:    68 Im(SO:z) integrals were written in  1 records.
 
 !timer: oneint required                     user=      .140 walltime=      .000
 !timer: seg1mn required                     user=      .160 walltime=      .000

twoint:      166885 1/r12    integrals and       19 pk flags
                                 were written in    62 records.

 twoint: maximum mblu needed =    308829
 !timer: twoint required                     user=    13.670 walltime=    16.000
 
 driver: 1-e integral  workspace high-water mark =     61848
 driver: 2-e integral  workspace high-water mark =    363449
 driver: overall argos workspace high-water mark =    363449
 !timer: argos required                      user=    13.830 walltime=    16.000
