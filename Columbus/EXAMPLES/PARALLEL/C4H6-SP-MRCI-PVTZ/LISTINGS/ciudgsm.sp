1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=   420)...
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -155.0173791292  8.1712E-14  4.7844E-01  1.3812E+00  1.0000E-03

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  1  1   -155.0173791292  8.1712E-14  4.7844E-01  1.3812E+00  1.0000E-03

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -155.4045208009  3.8714E-01  1.9788E-02  2.5888E-01  1.0000E-03
 mr-sdci #  2  1   -155.4188820489  1.4361E-02  1.6304E-03  8.0467E-02  1.0000E-03
 mr-sdci #  3  1   -155.4201869407  1.3049E-03  1.9775E-04  2.4644E-02  1.0000E-03
 mr-sdci #  4  1   -155.4203528526  1.6591E-04  4.7145E-05  1.1016E-02  1.0000E-03
 mr-sdci #  5  1   -155.4204009270  4.8074E-05  1.5046E-05  7.1418E-03  1.0000E-03
 mr-sdci #  6  1   -155.4204130277  1.2101E-05  4.8763E-06  3.6061E-03  1.0000E-03
 mr-sdci #  7  1   -155.4204190204  5.9927E-06  1.9196E-06  2.2919E-03  1.0000E-03
 mr-sdci #  8  1   -155.4204208664  1.8460E-06  4.4963E-07  1.2724E-03  1.0000E-03
 mr-sdci #  9  1   -155.4204212964  4.3000E-07  6.5437E-08  4.3538E-04  1.0000E-03

 mr-sdci  convergence criteria satisfied after  9 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  9  1   -155.4204212964  4.3000E-07  6.5437E-08  4.3538E-04  1.0000E-03

 number of reference csfs (nref) is    33.  root number (iroot) is  1.

 eref      =   -155.015472364017   "relaxed" cnot**2         =   0.886031253238
 eci       =   -155.420421296366   deltae = eci - eref       =  -0.404948932349
 eci+dv1   =   -155.466572818688   dv1 = (1-cnot**2)*deltae  =  -0.046151522323
 eci+dv2   =   -155.472509213303   dv2 = dv1 / cnot**2       =  -0.052087916937
 eci+dv3   =   -155.480198221078   dv3 = dv1 / (2*cnot**2-1) =  -0.059776924712
 eci+pople =   -155.473253479140   ( 22e- scaled deltae )    =  -0.457781115123
