1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      09/20/00    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 using llenci= -1
================================================================================
four external integ   76.75 MB location: distr. (dual) 
three external inte   31.25 MB location: distr. (dual) 
four external integ   74.00 MB location: distr. (dual) 
three external inte   30.75 MB location: distr. (dual) 
diagonal integrals     0.25 MB location: virtual disk  
off-diagonal integr    5.00 MB location: virtual disk  
computed file size in DP units
fil3w:     4095875
fil3x:     4030341
fil4w:    10059469
fil4x:     9699032
ofdgint:       32760
diagint:      655200
computed file size in DP units
fil3w:    32767000
fil3x:    32242728
fil4w:    80475752
fil4x:    77592256
ofdgint:      262080
diagint:     5241600
 nsubmx= 6  lenci= 8741625
global arrays:     ********   (            867.01 MB)
vdisk:               687960   (              5.25 MB)
drt:                1531782   (              5.84 MB)
================================================================================
 Main memory management:
 global         19067843 DP per process
 vdisk            687960 DP per process
 stack                 0 DP per process
 core           30244197 DP per process
Array Handle=-1000 Name:'civect' Data Type:double
Array Dimensions:8741625x6
Process=0	 owns array section: [1:1456938,1:6] 
Process=1	 owns array section: [1456939:2913876,1:6] 
Process=2	 owns array section: [2913877:4370814,1:6] 
Process=3	 owns array section: [4370815:5827752,1:6] 
Process=4	 owns array section: [5827753:7284690,1:6] 
Process=5	 owns array section: [7284691:8741625,1:6] 
Array Handle=-999 Name:'sigvec' Data Type:double
Array Dimensions:8741625x6
Process=0	 owns array section: [1:1456938,1:6] 
Process=1	 owns array section: [1456939:2913876,1:6] 
Process=2	 owns array section: [2913877:4370814,1:6] 
Process=3	 owns array section: [4370815:5827752,1:6] 
Process=4	 owns array section: [5827753:7284690,1:6] 
Process=5	 owns array section: [7284691:8741625,1:6] 
Array Handle=-998 Name:'hdg' Data Type:double
Array Dimensions:8741625x1
Process=0	 owns array section: [1:1456938,1:1] 
Process=1	 owns array section: [1456939:2913876,1:1] 
Process=2	 owns array section: [2913877:4370814,1:1] 
Process=3	 owns array section: [4370815:5827752,1:1] 
Process=4	 owns array section: [5827753:7284690,1:1] 
Process=5	 owns array section: [7284691:8741625,1:1] 
Array Handle=-997 Name:'drt' Data Type:double
Array Dimensions:109413x7
Process=0	 owns array section: [1:18236,1:7] 
Process=1	 owns array section: [18237:36472,1:7] 
Process=2	 owns array section: [36473:54708,1:7] 
Process=3	 owns array section: [54709:72944,1:7] 
Process=4	 owns array section: [72945:91180,1:7] 
Process=5	 owns array section: [91181:109413,1:7] 
Array Handle=-996 Name:'nxttask' Data Type:integer
Array Dimensions:1x1
Process=0	 owns array section: [1:1,1:1] 
Process=1	 owns array section: [0:-1,0:-1] 
Process=2	 owns array section: [0:-1,0:-1] 
Process=3	 owns array section: [0:-1,0:-1] 
Process=4	 owns array section: [0:-1,0:-1] 
Process=5	 owns array section: [0:-1,0:-1] 
 CIUDG version 5.9.7 ( 5-Oct-2004)
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 0,
  GSET = 0,
   DAVCOR =10,
  NCOREL = 22
  NROOT = 1
  IVMODE = 3
  NBKITR = 1
  NVBKMN = 1
  NVBKMX = 6
  NVRFMX = 6
  RTOLBK = 1e-3,1e-3,
  NITER =  10
  NVCIMN = 1
  NVCIMX =  6
  RTOLCI = 1e-3,1e-3,
  IDEN  = 1
  CSFPRN = 10,
  FTCALC = 1
  MAXSEG = 20
  nsegd = 1,1,2,2
  nseg0x = 1,1,2,2
  nseg1x = 1,1,2,2
  nseg2x = 1,1,2,2
  nseg3x = 1,1,2,2
  nseg4x = 1,1,2,2,
  c2ex0ex=0
  c3ex1ex=0
  cdg4ex=1
  fileloc=1,1,4,4,4,4
  &end
 ------------------------------------------------------------------------

 ** list of control variables **
 nrfitr =   30      nvrfmx =    6      nvrfmn =    1
 lvlprt =    0      nroot  =    1      noldv  =    0      noldhv =    0
 nunitv =    1      ntype  =    0      nbkitr =    1      niter  =   10
 ivmode =    3      vout   =    0      istrt  =    0      iortls =    0
 nvbkmx =    6      ibktv  =   -1      ibkthv =   -1      frcsub =    0
 nvcimx =    6      icitv  =   -1      icithv =   -1      maxseg =   20
 iden   =    1      itran  =    0      froot  =    0      rtmode =    0
 ftcalc =    1      lrtshift=1.0000    ncouple=    1      skipso  =   F
 ncorel =   22      csfprn  =  10      ctol   = 1.00E-02  davcor  =  10


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-03    1.000E-03
 
 units and filenames:
    1: ( 6)    ciudgls                                                     
    2: ( 5)    ciudgin                                                     
    3: ( 7)    ciudgsm                                                     
    4: ( 4)    cihdiag                                                     
    5: ( 3)    cihpseu                                                     
    6: (10)    cihvfl                                                      
    7: (11)    civfl                                                       
    8: (12)    diagint                                                     
    9: (13)    ofdgint                                                     
   10: (17)    cidrtfl                                                     
   11: (20)    ciftdfl                                                     
   12: (21)    ciftofl                                                     
   13: (24)    ciftifl                                                     
   14: ( 8)    ciflind                                                     
   15: (15)    civin                                                       
   16: (16)    civout                                                      
   18: (18)    d1fl                                                        
   20: (53)    cifvfl                                                      
   21: (25)    ciscr4                                                      
   22: (41)    ciftotd                                                     
   23: (42)    flacpfd                                                     
   26: (26)    ciscr5                                                      
   27: (47)    restart                                                     
   28: (48)    civout                                                      
   29: (49)    cirefv                                                      
   31: (31)    fil4w                                                       
   32: (32)    fil4x                                                       
   33: (33)    fil3w                                                       
   34: (34)    fil3x                                                       
   36: (36)    aoints                                                      
   37: (37)    aoints2                                                     
   38: (38)    drtfil                                                      
   39: (50)    cisrtif                                                     
   42: (50)    mocoef                                                      
   45: (52)    nocoef_ci                                                   
   46: (53)    civfl_restart                                               
 ------------------------------------------------------------------------
 broadcasting INDATA    
 broadcasting ACPFINFO  
 broadcasting CFILES    

 workspace allocation information: lcore=  30244197 mem1=         1 ifirst=         1

 integral file titles:
 Hermit Integral Program : SIFS version  j25             Tue Oct 19 13:59:08 2004
  cidrt_title                                                                    
 Hermit Integral Program : SIFS version  grimming        Tue Aug  7 18:24:26 2001
  title                                                                          
 mofmt: formatted orbitals label=morb    grimming        Tue Aug  7 18:38:16 2001
 SIFS file created by program tran.      j25             Tue Oct 19 13:59:19 2004

 core energy values from the integral file:
 energy( 1)=  1.034429979483E+02, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -1.274375457176E+02, ietype=    6,   fcore energy of type: H1(*)   
 energy( 3)= -1.191864289308E+02, ietype=    5,   fcore energy of type: Vref(*) 

 total core repulsion energy = -1.431809767001E+02

 drt header information:
  cidrt_title                                                                    
 spnorb, spnodd, lxyzir,hmult F F 0 0 0 0
 nmot  =   146 niot  =    13 nfct  =     4 nfvt  =     0
 nrow  =   104 nsym  =     4 ssym  =     1 lenbuf=  1600
 nwalk,xbar:      14155      800     5110     3772     4473
 nvalwt,nvalw:     8735      420     4326     1882     2107
 ncsft:         8741625
 total number of valid internal walks:    8735
 nvalz,nvaly,nvalx,nvalw =      420    4326    1882    2107

 cisrt info file parameters:
 file number  12 blocksize   4095
 mxbld  20475
 nd4ext,nd2ext,nd0ext 16770  3354   182
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int  9468510  3698334   597417    46410     1461        0        0        0
 minbl4,minbl3,maxbl2  7650  7650  7653
 maxbuf 32767
 number of external orbitals per symmetry block:  46  47  18  18
 nmsym   4 number of internal orbitals  13

 formula file title:
 Hermit Integral Program : SIFS version  j25             Tue Oct 19 13:59:08 2004
  cidrt_title                                                                    
 Hermit Integral Program : SIFS version  grimming        Tue Aug  7 18:24:26 2001
  title                                                                          
 mofmt: formatted orbitals label=morb    grimming        Tue Aug  7 18:38:16 2001
 SIFS file created by program tran.      j25             Tue Oct 19 13:59:19 2004
  cidrt_title                                                                    
 file nmb.  24 block size     0
 pthz,pthy,pthx,pthw:   800  5110  3772  4473 total internal walks:   14155
 maxlp3,n3xtlp,n2xtlp,n1xtlp,n0xtlp,n2lp,n1lp,n0lp     0     0     0     0
 orbsym(*)= 1 1 1 2 2 2 2 2 2 3 3 4 4

 setref:       33 references kept,
                0 references were marked as invalid, out of
               33 total.
 limcnvrt: found 420  valid internal walksout of  800  walks (skipping trailing invalids)
  ... adding  420  segmentation marks segtype= 1
 limcnvrt: found 4326  valid internal walksout of  5110  walks (skipping trailing invalids)
  ... adding  4326  segmentation marks segtype= 2
 limcnvrt: found 1882  valid internal walksout of  3772  walks (skipping trailing invalids)
  ... adding  1882  segmentation marks segtype= 3
 limcnvrt: found 2107  valid internal walksout of  4473  walks (skipping trailing invalids)
  ... adding  2107  segmentation marks segtype= 4
 broadcasting SOLXYZ    
 broadcasting INDATA    
 broadcasting REPNUC    
 broadcasting INF       
 broadcasting SOINF     
 broadcasting DATA      
 broadcasting MOMAP     
 broadcasting CLI       
 broadcasting CSYM      
 broadcasting SLABEL    
 broadcasting MOMAP     
 broadcasting DRT       
 broadcasting INF1      
 broadcasting DRTINFO   
loaded      142 onel-dg  integrals(   1records ) at       1 on virtual disk
loaded    16770 4ext-dg  integrals(   5records ) at    4096 on virtual disk
loaded     3354 2ext-dg  integrals(   1records ) at   24571 on virtual disk
loaded      182 0ext-dg  integrals(   1records ) at   28666 on virtual disk
loaded     2551 onel-of  integrals(   1records ) at   32761 on virtual disk
loaded   597417 2ext-og  integrals( 146records ) at   36856 on virtual disk
loaded    46410 1ext-og  integrals(  12records ) at  634726 on virtual disk
loaded     1461 0ext-og  integrals(   1records ) at  683866 on virtual disk

 number of external paths / symmetry
 vertex x    2422    2486    1674    1674
 vertex w    2551    2486    1674    1674

 lprune: l(*,*,*) pruned with nwalk=     800 nvalwt=     420 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    5110 nvalwt=    4326 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    1598 nvalwt=     942 nprune=      44

 lprune: l(*,*,*) pruned with nwalk=    2174 nvalwt=     940 nprune=      58

 lprune: l(*,*,*) pruned with nwalk=    1934 nvalwt=    1054 nprune=      43

 lprune: l(*,*,*) pruned with nwalk=    2539 nvalwt=    1053 nprune=      54



                   segmentation summary for type diagonal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1         800|       420|         0|       420|         0|         1|
 -------------------------------------------------------------------------------
  Y 2        5110|    127072|       420|      4326|       420|      1021|
 -------------------------------------------------------------------------------
  X 3        1598|   1907604|    127492|       942|      4746|      6111|
 -------------------------------------------------------------------------------
  X 4        2084|   2076328|   2035096|       940|      5688|      7681|
 -------------------------------------------------------------------------------
  W 5        1874|   2251162|   4111424|      1054|      6628|      9361|
 -------------------------------------------------------------------------------
  W 6        2449|   2379039|   6362586|      1053|      7682|     11089|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>   8741625


 lprune: l(*,*,*) pruned with nwalk=     800 nvalwt=     420 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    5110 nvalwt=    4326 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    1598 nvalwt=     942 nprune=      44

 lprune: l(*,*,*) pruned with nwalk=    2174 nvalwt=     940 nprune=      58

 lprune: l(*,*,*) pruned with nwalk=    1934 nvalwt=    1054 nprune=      43

 lprune: l(*,*,*) pruned with nwalk=    2539 nvalwt=    1053 nprune=      54



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1         800|       420|         0|       420|         0|         1|
 -------------------------------------------------------------------------------
  Y 2        5110|    127072|       420|      4326|       420|      1021|
 -------------------------------------------------------------------------------
  X 3        1598|   1907604|    127492|       942|      4746|      6111|
 -------------------------------------------------------------------------------
  X 4        2084|   2076328|   2035096|       940|      5688|      7681|
 -------------------------------------------------------------------------------
  W 5        1874|   2251162|   4111424|      1054|      6628|      9361|
 -------------------------------------------------------------------------------
  W 6        2449|   2379039|   6362586|      1053|      7682|     11089|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>   8741625


 lprune: l(*,*,*) pruned with nwalk=     800 nvalwt=     420 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    5110 nvalwt=    4326 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    1598 nvalwt=     942 nprune=      44

 lprune: l(*,*,*) pruned with nwalk=    2174 nvalwt=     940 nprune=      58

 lprune: l(*,*,*) pruned with nwalk=    1934 nvalwt=    1054 nprune=      43

 lprune: l(*,*,*) pruned with nwalk=    2539 nvalwt=    1053 nprune=      54



                   segmentation summary for type one-external
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1         800|       420|         0|       420|         0|         1|
 -------------------------------------------------------------------------------
  Y 2        5110|    127072|       420|      4326|       420|      1021|
 -------------------------------------------------------------------------------
  X 3        1598|   1907604|    127492|       942|      4746|      6111|
 -------------------------------------------------------------------------------
  X 4        2084|   2076328|   2035096|       940|      5688|      7681|
 -------------------------------------------------------------------------------
  W 5        1874|   2251162|   4111424|      1054|      6628|      9361|
 -------------------------------------------------------------------------------
  W 6        2449|   2379039|   6362586|      1053|      7682|     11089|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>   8741625


 lprune: l(*,*,*) pruned with nwalk=     800 nvalwt=     420 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    5110 nvalwt=    4326 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    1598 nvalwt=     942 nprune=      44

 lprune: l(*,*,*) pruned with nwalk=    2174 nvalwt=     940 nprune=      58

 lprune: l(*,*,*) pruned with nwalk=    1934 nvalwt=    1054 nprune=      43

 lprune: l(*,*,*) pruned with nwalk=    2539 nvalwt=    1053 nprune=      54



                   segmentation summary for type two-external
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1         800|       420|         0|       420|         0|         1|
 -------------------------------------------------------------------------------
  Y 2        5110|    127072|       420|      4326|       420|      1021|
 -------------------------------------------------------------------------------
  X 3        1598|   1907604|    127492|       942|      4746|      6111|
 -------------------------------------------------------------------------------
  X 4        2084|   2076328|   2035096|       940|      5688|      7681|
 -------------------------------------------------------------------------------
  W 5        1874|   2251162|   4111424|      1054|      6628|      9361|
 -------------------------------------------------------------------------------
  W 6        2449|   2379039|   6362586|      1053|      7682|     11089|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>   8741625


 lprune: l(*,*,*) pruned with nwalk=     800 nvalwt=     420 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    5110 nvalwt=    4326 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    1598 nvalwt=     942 nprune=      44

 lprune: l(*,*,*) pruned with nwalk=    2174 nvalwt=     940 nprune=      58

 lprune: l(*,*,*) pruned with nwalk=    1934 nvalwt=    1054 nprune=      43

 lprune: l(*,*,*) pruned with nwalk=    2539 nvalwt=    1053 nprune=      54



                   segmentation summary for type three-external
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1         800|       420|         0|       420|         0|         1|
 -------------------------------------------------------------------------------
  Y 2        5110|    127072|       420|      4326|       420|      1021|
 -------------------------------------------------------------------------------
  X 3        1598|   1907604|    127492|       942|      4746|      6111|
 -------------------------------------------------------------------------------
  X 4        2084|   2076328|   2035096|       940|      5688|      7681|
 -------------------------------------------------------------------------------
  W 5        1874|   2251162|   4111424|      1054|      6628|      9361|
 -------------------------------------------------------------------------------
  W 6        2449|   2379039|   6362586|      1053|      7682|     11089|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>   8741625


 lprune: l(*,*,*) pruned with nwalk=     800 nvalwt=     420 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    5110 nvalwt=    4326 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    1598 nvalwt=     942 nprune=      44

 lprune: l(*,*,*) pruned with nwalk=    2174 nvalwt=     940 nprune=      58

 lprune: l(*,*,*) pruned with nwalk=    1934 nvalwt=    1054 nprune=      43

 lprune: l(*,*,*) pruned with nwalk=    2539 nvalwt=    1053 nprune=      54



                   segmentation summary for type four-external
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1         800|       420|         0|       420|         0|         1|
 -------------------------------------------------------------------------------
  Y 2        5110|    127072|       420|      4326|       420|      1021|
 -------------------------------------------------------------------------------
  X 3        1598|   1907604|    127492|       942|      4746|      6111|
 -------------------------------------------------------------------------------
  X 4        2084|   2076328|   2035096|       940|      5688|      7681|
 -------------------------------------------------------------------------------
  W 5        1874|   2251162|   4111424|      1054|      6628|      9361|
 -------------------------------------------------------------------------------
  W 6        2449|   2379039|   6362586|      1053|      7682|     11089|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>   8741625

 broadcasting CIVCT     
 broadcasting DATA      
 broadcasting INF       
 broadcasting CNFSPC    
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS
----------------------------------------------------------------------------------------------------
     1  2   2    21      two-ext yy   2X  2 2    5110    5110     127072     127072    4326    4326
     2  3   3    22      two-ext xx   2X  3 3    1598    1598    1907604    1907604     942     942
     3  4   3    22      two-ext xx   2X  3 3    2084    1598    2076328    1907604     940     942
     4  4   4    22      two-ext xx   2X  3 3    2084    2084    2076328    2076328     940     940
     5  5   5    23      two-ext ww   2X  4 4    1874    1874    2251162    2251162    1054    1054
     6  6   5    23      two-ext ww   2X  4 4    2449    1874    2379039    2251162    1053    1054
     7  6   6    23      two-ext ww   2X  4 4    2449    2449    2379039    2379039    1053    1053
     8  3   1    24      two-ext xz   2X  3 1    1598     800    1907604        420     942     420
     9  4   1    24      two-ext xz   2X  3 1    2084     800    2076328        420     940     420
    10  5   1    25      two-ext wz   2X  4 1    1874     800    2251162        420    1054     420
    11  6   1    25      two-ext wz   2X  4 1    2449     800    2379039        420    1053     420
    12  5   3    26      two-ext wx   2X  4 3    1874    1598    2251162    1907604    1054     942
    13  6   3    26      two-ext wx   2X  4 3    2449    1598    2379039    1907604    1053     942
    14  5   4    26      two-ext wx   2X  4 3    1874    2084    2251162    2076328    1054     940
    15  6   4    26      two-ext wx   2X  4 3    2449    2084    2379039    2076328    1053     940
    16  2   1    11      one-ext yz   1X  2 1    5110     800     127072        420    4326     420
    17  3   2    13      one-ext yx   1X  3 2    1598    5110    1907604     127072     942    4326
    18  4   2    13      one-ext yx   1X  3 2    2084    5110    2076328     127072     940    4326
    19  5   2    14      one-ext yw   1X  4 2    1874    5110    2251162     127072    1054    4326
    20  6   2    14      one-ext yw   1X  4 2    2449    5110    2379039     127072    1053    4326
    21  3   2    31      thr-ext yx   3X  3 2    1598    5110    1907604     127072     942    4326
    22  4   2    31      thr-ext yx   3X  3 2    2084    5110    2076328     127072     940    4326
    23  5   2    32      thr-ext yw   3X  4 2    1874    5110    2251162     127072    1054    4326
    24  6   2    32      thr-ext yw   3X  4 2    2449    5110    2379039     127072    1053    4326
    25  1   1     1      allint zz    OX  1 1     800     800        420        420     420     420
    26  2   2     2      allint yy    OX  2 2    5110    5110     127072     127072    4326    4326
    27  3   3     3      allint xx    OX  3 3    1598    1598    1907604    1907604     942     942
    28  4   3     3      allint xx    OX  3 3    2084    1598    2076328    1907604     940     942
    29  4   4     3      allint xx    OX  3 3    2084    2084    2076328    2076328     940     940
    30  5   5     4      allint ww    OX  4 4    1874    1874    2251162    2251162    1054    1054
    31  6   5     4      allint ww    OX  4 4    2449    1874    2379039    2251162    1053    1054
    32  6   6     4      allint ww    OX  4 4    2449    2449    2379039    2379039    1053    1053
    33  1   1    75      dg-024ext z  DG  1 1     800     800        420        420     420     420
    34  2   2    45      4exdg024 y   DG  2 2    5110    5110     127072     127072    4326    4326
    35  3   3    46      4exdg024 x   DG  3 3    1598    1598    1907604    1907604     942     942
    36  4   4    46      4exdg024 x   DG  3 3    2084    2084    2076328    2076328     940     940
    37  5   5    47      4exdg024 w   DG  4 4    1874    1874    2251162    2251162    1054    1054
    38  6   6    47      4exdg024 w   DG  4 4    2449    2449    2379039    2379039    1053    1053
----------------------------------------------------------------------------------------------------
 broadcasting TASKLST   
 DIAGEL COUNTS: DG0X,DG2X,DG4X= 214466 53393 8315
 diagonal elements written to file   4

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:   11918 2x:       0 4x:       0
All internal counts: zz :   32118 yy:       0 xx:       0 ww:       0
One-external counts: yz :       0 yx:       0 yw:       0
Two-external counts: yy :       0 ww:       0 xx:       0 xz:       0 wz:       0 wx:       0
Three-ext.   counts: yx :       0 yw:       0

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 reference space has dimension      33

    root           eigenvalues
    ----           ------------
       1        -155.0173791292

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt= 420

         vector  1 from unit 11 written to unit 49 filename cirefv                                                      
 ufvoutnew: ... writing  recamt= 420

         vector  1 from unit 11 written to unit 49 filename cirefv                                                      

 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=   420)...
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:           8741625
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                    1
 residual norm convergence criteria:               0.001000

          starting bk iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:  214466 2x:   53393 4x:    8315
All internal counts: zz :   32118 yy:       0 xx:       0 ww:       0
One-external counts: yz :  161582 yx:       0 yw:       0
Two-external counts: yy :       0 ww:       0 xx:       0 xz:    5228 wz:    6717 wx:       0
Three-ext.   counts: yx :       0 yw:       0

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 420

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -155.0173791292  1.9185E-13  4.7844E-01  1.3812E+00  1.0000E-03
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   21    0   0.00   0.00   0.00   0.00         0.    0.0000
    2   22    0   0.00   0.00   0.00   0.00         0.    0.0000
    3   22    0   0.00   0.00   0.00   0.00         0.    0.0000
    4   22    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   23    0   0.00   0.00   0.00   0.00         0.    0.0000
    6   23    0   0.00   0.00   0.00   0.00         0.    0.0000
    7   23    0   0.00   0.00   0.00   0.00         0.    0.0000
    8   24    0   0.16   0.00   0.00   0.06         0.    0.0016
    9   24    4   0.17   0.00   0.00   0.06         0.    0.0015
   10   25    2   0.16   0.00   0.00   0.08         0.    0.0021
   11   25    5   0.14   0.00   0.00   0.06         0.    0.0018
   12   26    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   26    0   0.00   0.00   0.00   0.00         0.    0.0000
   14   26    0   0.00   0.00   0.00   0.00         0.    0.0000
   15   26    0   0.00   0.00   0.00   0.00         0.    0.0000
   16   11    3   0.31   0.18   0.00   0.31         0.    0.1309
   17   13    0   0.00   0.00   0.00   0.00         0.    0.0000
   18   13    0   0.00   0.00   0.00   0.00         0.    0.0000
   19   14    0   0.00   0.00   0.00   0.00         0.    0.0000
   20   14    0   0.00   0.00   0.00   0.00         0.    0.0000
   21   31    0   0.00   0.00   0.00   0.00         0.    0.0000
   22   31    0   0.00   0.00   0.00   0.00         0.    0.0000
   23   32    0   0.00   0.00   0.00   0.00         0.    0.0000
   24   32    0   0.00   0.00   0.00   0.00         0.    0.0000
   25    1    1   0.07   0.06   0.00   0.07         0.    0.0244
   26    2    0   0.00   0.00   0.00   0.00         0.    0.0000
   27    3    0   0.00   0.00   0.00   0.00         0.    0.0000
   28    3    0   0.00   0.00   0.00   0.00         0.    0.0000
   29    3    0   0.00   0.00   0.00   0.00         0.    0.0000
   30    4    0   0.00   0.00   0.00   0.00         0.    0.0000
   31    4    0   0.00   0.00   0.00   0.00         0.    0.0000
   32    4    0   0.00   0.00   0.00   0.00         0.    0.0000
   33   75    1   0.01   0.01   0.00   0.01         0.    0.0082
   34   45    1   0.10   0.05   0.00   0.09         0.    0.0168
   35   46    5   0.43   0.01   0.00   0.38         0.    0.0032
   36   46    0   0.67   0.01   0.00   0.60         0.    0.0033
   37   47    2   0.60   0.01   0.00   0.53         0.    0.0035
   38   47    4   0.70   0.01   0.00   0.63         0.    0.0037
================================================================
================ TIMING STATISTICS PER NODE    ================
node# twait    ths   tcin   teig  tvect  tga   ttotal 
  1   0.05   0.02   0.16   0.00   0.00   0.10   1.14
  2   0.69   0.03   0.16   0.00   0.00   0.00   1.22
  3   0.11   0.03   0.16   0.00   0.00   0.10   1.26
  4   0.56   0.03   0.16   0.00   0.00   0.00   1.24
  5   0.00   0.03   0.16   0.00   0.00   0.12   1.24
  6   0.30   0.03   0.16   0.00   0.00   0.09   1.26
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                     3.5081s 
time spent in multnx:                   2.8831s 
integral transfer time:                 0.0163s 
time spent for loop construction:       0.3405s 
syncronization time in mult:            1.7160s 
total time per CI iteration:            7.3488s 
 ** parallelization degree for mult section:0.6715

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)           0.01        0.00          0.000
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.02        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:            0.03        0.00          0.000
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)         133.40        0.24        566.725
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)        133.40        0.18        743.389
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.10        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:          266.90        0.42        642.183
========================================================


 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -155.0173791292  1.9185E-13  4.7844E-01  1.3812E+00  1.0000E-03
 
 expansion vectors are not transformed.
 matrix-vector products are not transformed.

    2 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)

 from bk iterations: iconv=   1

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:           8741625
 number of initial trial vectors:                         2
 number of initial matrix-vector products:                1
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                   10
 residual norm convergence criteria:               0.001000

          starting ci iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:  214466 2x:   53393 4x:    8315
All internal counts: zz :   32118 yy:  490036 xx:  110475 ww:  123428
One-external counts: yz :  161582 yx:  486981 yw:  504573
Two-external counts: yy :  191370 ww:   45189 xx:   45699 xz:    5228 wz:    6717 wx:   69923
Three-ext.   counts: yx :  573671 yw:  607988

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 420

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.94308219    -0.33255974

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -155.4045208009  3.8714E-01  1.9788E-02  2.5888E-01  1.0000E-03
 mr-sdci #  1  2   -151.9040168701  8.7230E+00  0.0000E+00  0.0000E+00  1.0000E-03
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   21    5   2.47   0.09   0.00   2.47         0.    0.1070
    2   22    0   5.46   0.01   0.00   5.40         0.    0.0126
    3   22    4   3.09   0.00   0.00   2.95         0.    0.0033
    4   22    1   4.52   0.01   0.00   4.46         0.    0.0085
    5   23    1   5.08   0.01   0.00   5.04         0.    0.0106
    6   23    0   2.23   0.00   0.00   2.07         0.    0.0031
    7   23    5   4.83   0.01   0.00   4.76         0.    0.0080
    8   24    5   0.13   0.00   0.00   0.07         0.    0.0016
    9   24    5   0.14   0.00   0.00   0.08         0.    0.0015
   10   25    5   0.17   0.00   0.00   0.11         0.    0.0021
   11   25    1   0.18   0.00   0.00   0.10         0.    0.0018
   12   26    3   8.64   0.01   0.00   8.48         0.    0.0186
   13   26    1   2.15   0.00   0.00   1.99         0.    0.0032
   14   26    1   2.12   0.00   0.00   2.02         0.    0.0031
   15   26    5   5.50   0.01   0.00   5.34         0.    0.0112
   16   11    1   0.36   0.18   0.00   0.36         0.    0.1309
   17   13    5   3.38   0.15   0.00   3.34         0.    0.1882
   18   13    2   2.46   0.18   0.00   2.42         0.    0.1800
   19   14    1   2.58   0.16   0.00   2.54         0.    0.1908
   20   14    4   2.83   0.19   0.00   2.77         0.    0.1882
   21   31    5   3.39   0.00   0.00   3.34         0.    0.0014
   22   31    2   3.45   0.00   0.00   3.40         0.    0.0013
   23   32    3   3.57   0.00   0.00   3.52         0.    0.0015
   24   32    1   3.79   0.00   0.00   3.74         0.    0.0014
   25    1    1   0.07   0.05   0.00   0.07         0.    0.0244
   26    2    4   0.71   0.35   0.00   0.71         0.    0.3369
   27    3    4   0.80   0.03   0.00   0.76         0.    0.0313
   28    3    5   0.38   0.02   0.00   0.29         0.    0.0197
   29    3    0   1.05   0.03   0.00   1.00         0.    0.0238
   30    4    4   1.33   0.04   0.00   1.28         0.    0.0349
   31    4    5   0.57   0.02   0.00   0.46         0.    0.0190
   32    4    2   1.12   0.04   0.00   1.07         0.    0.0258
   33   75    5   0.01   0.01   0.00   0.01         0.    0.0082
   34   45    5   0.11   0.05   0.00   0.10         0.    0.0000
   35   46    2   9.83   0.01   0.00   9.77         0.    0.0000
   36   46    4  11.16   0.01   0.00  11.10         0.    0.0000
   37   47    3  12.33   0.01   0.00  12.26         0.    0.0000
   38   47    0  13.19   0.01   0.00  13.12         0.    0.0000
================================================================
================ TIMING STATISTICS PER NODE    ================
node# twait    ths   tcin   teig  tvect  tga   ttotal 
  1   2.62   0.05   0.19   0.00   0.00   0.23  24.91
  2   3.70   0.06   0.19   0.00   0.00   0.38  24.93
  3   7.68   0.06   0.19   0.00   0.00   0.15  24.92
  4   0.00   0.06   0.19   0.00   0.00   0.19  24.92
  5   4.63   0.06   0.19   0.00   0.00   0.24  24.92
  6   3.47   0.06   0.19   0.00   0.00   0.49  24.91
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                   125.1903s 
time spent in multnx:                 122.7691s 
integral transfer time:                 0.0607s 
time spent for loop construction:       1.7278s 
syncronization time in mult:           22.0933s 
total time per CI iteration:          149.5103s 
 ** parallelization degree for mult section:0.8500

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)           0.00        0.00          0.000
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.03        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:            0.04        0.00          0.000
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)         668.86        0.89        748.332
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)        668.86        0.78        859.410
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.49        0.00        138.295
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:         1338.21        1.68        798.642
========================================================


          starting ci iteration   2

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:  214466 2x:   53393 4x:    8315
All internal counts: zz :   32118 yy:  490036 xx:  110475 ww:  123428
One-external counts: yz :  161582 yx:  486981 yw:  504573
Two-external counts: yy :  191370 ww:   45189 xx:   45699 xz:    5228 wz:    6717 wx:   69923
Three-ext.   counts: yx :  573671 yw:  607988

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 420

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.94106063    -0.04020517     0.33583990

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1   -155.4188820489  1.4361E-02  1.6304E-03  8.0467E-02  1.0000E-03
 mr-sdci #  2  2   -152.6218678548  7.1785E-01  0.0000E+00  0.0000E+00  1.0000E-03
 mr-sdci #  2  3   -151.8991802420  8.7182E+00  0.0000E+00  2.2397E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   21    1   2.38   0.08   0.00   2.38         0.    0.1070
    2   22    5   6.11   0.01   0.00   6.07         0.    0.0126
    3   22    5   3.78   0.00   0.00   3.69         0.    0.0033
    4   22    5   3.34   0.01   0.00   3.29         0.    0.0085
    5   23    1   4.10   0.01   0.00   4.05         0.    0.0106
    6   23    3   2.12   0.00   0.00   2.00         0.    0.0031
    7   23    2   3.63   0.01   0.00   3.57         0.    0.0080
    8   24    5   0.11   0.00   0.00   0.06         0.    0.0016
    9   24    1   0.15   0.00   0.00   0.08         0.    0.0015
   10   25    2   0.17   0.00   0.00   0.10         0.    0.0021
   11   25    1   0.18   0.00   0.00   0.10         0.    0.0018
   12   26    1   8.95   0.02   0.00   8.83         0.    0.0186
   13   26    0   1.99   0.00   0.00   1.89         0.    0.0032
   14   26    4   2.40   0.00   0.00   2.28         0.    0.0031
   15   26    5   5.50   0.01   0.00   5.37         0.    0.0112
   16   11    3   0.35   0.18   0.00   0.35         0.    0.1309
   17   13    2   4.28   0.15   0.00   4.24         0.    0.1882
   18   13    5   2.50   0.18   0.00   2.45         0.    0.1800
   19   14    2   2.37   0.16   0.00   2.32         0.    0.1908
   20   14    4   4.41   2.03   0.00   4.35         0.    0.1882
   21   31    0   5.17   0.00   0.00   5.12         0.    0.0014
   22   31    3   5.58   0.00   0.00   5.53         0.    0.0013
   23   32    1   5.56   0.00   0.00   5.51         0.    0.0015
   24   32    4   3.96   0.00   0.00   3.88         0.    0.0014
   25    1    2   0.07   0.06   0.00   0.07         0.    0.0244
   26    2    5   0.69   0.34   0.00   0.69         0.    0.3369
   27    3    1   1.29   0.03   0.00   1.23         0.    0.0313
   28    3    0   0.55   0.02   0.00   0.44         0.    0.0197
   29    3    0   1.16   0.04   0.00   1.10         0.    0.0238
   30    4    2   1.54   0.04   0.00   1.47         0.    0.0349
   31    4    5   0.64   0.02   0.00   0.50         0.    0.0190
   32    4    3   1.32   0.04   0.00   1.25         0.    0.0258
   33   75    1   0.01   0.01   0.00   0.01         0.    0.0082
   34   45    3   0.10   0.05   0.00   0.10         0.    0.0000
   35   46    2  10.60   0.01   0.00  10.54         0.    0.0000
   36   46    4  11.99   0.01   0.00  11.93         0.    0.0000
   37   47    3  13.19   0.01   0.00  13.13         0.    0.0000
   38   47    0  13.80   0.01   0.00  13.73         0.    0.0000
================================================================
================ TIMING STATISTICS PER NODE    ================
node# twait    ths   tcin   teig  tvect  tga   ttotal 
  1   0.08   0.06   0.25   0.00   0.00   0.28  23.82
  2   0.13   0.06   0.25   0.00   0.00   0.31  23.18
  3   0.10   0.06   0.25   0.00   0.00   0.26  23.18
  4   0.08   0.06   0.25   0.00   0.00   0.23  23.18
  5   0.00   0.06   0.25   0.00   0.00   0.22  23.18
  6   0.07   0.06   0.25   0.00   0.00   0.40  23.18
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                   136.0646s 
time spent in multnx:                 133.6934s 
integral transfer time:                 0.0569s 
time spent for loop construction:       3.5729s 
syncronization time in mult:            0.4598s 
total time per CI iteration:          139.7200s 
 ** parallelization degree for mult section:0.9966

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)           0.97        0.00        925.112
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)          0.97        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.06        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:            2.01        0.00        911.461
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)         667.89        0.87        769.910
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)        667.89        0.82        810.126
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.46        0.00        198.886
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:         1336.24        1.69        788.707
========================================================


          starting ci iteration   3

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:  214466 2x:   53393 4x:    8315
All internal counts: zz :   32118 yy:  490036 xx:  110475 ww:  123428
One-external counts: yz :  161582 yx:  486981 yw:  504573
Two-external counts: yy :  191370 ww:   45189 xx:   45699 xz:    5228 wz:    6717 wx:   69923
Three-ext.   counts: yx :  573671 yw:  607988

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 420

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.94081904    -0.04304852    -0.02765025     0.33502511

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1   -155.4201869407  1.3049E-03  1.9775E-04  2.4644E-02  1.0000E-03
 mr-sdci #  3  2   -152.7608796579  1.3901E-01  0.0000E+00  0.0000E+00  1.0000E-03
 mr-sdci #  3  3   -152.1019770098  2.0280E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mr-sdci #  3  4   -151.8979459224  8.7170E+00  0.0000E+00  2.2587E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   21    5   3.99   0.09   0.00   3.99         0.    0.1070
    2   22    1   4.36   0.01   0.00   4.31         0.    0.0126
    3   22    3   1.94   0.00   0.00   1.85         0.    0.0033
    4   22    2   4.83   0.01   0.00   4.79         0.    0.0085
    5   23    5   3.96   0.01   0.00   3.91         0.    0.0106
    6   23    1   3.62   0.00   0.00   3.51         0.    0.0031
    7   23    4   5.14   0.01   0.00   5.10         0.    0.0080
    8   24    4   0.08   0.00   0.00   0.05         0.    0.0016
    9   24    0   0.10   0.00   0.00   0.05         0.    0.0015
   10   25    4   0.11   0.00   0.00   0.07         0.    0.0021
   11   25    0   0.11   0.00   0.00   0.07         0.    0.0018
   12   26    2   9.06   0.01   0.00   8.88         0.    0.0186
   13   26    3   3.46   0.00   0.00   3.37         0.    0.0032
   14   26    3   2.00   0.00   0.00   1.90         0.    0.0031
   15   26    1   5.34   0.01   0.00   5.24         0.    0.0112
   16   11    3   0.32   0.18   0.00   0.32         0.    0.1309
   17   13    2   2.14   0.15   0.00   2.09         0.    0.1882
   18   13    1   2.41   0.17   0.00   2.36         0.    0.1800
   19   14    0   3.96   0.16   0.00   3.92         0.    0.1908
   20   14    4   2.69   0.18   0.00   2.64         0.    0.1882
   21   31    3   3.26   0.00   0.00   3.23         0.    0.0014
   22   31    1   5.61   0.00   0.00   5.56         0.    0.0013
   23   32    2   3.75   0.00   0.00   3.68         0.    0.0015
   24   32    0   3.82   0.00   0.00   3.77         0.    0.0014
   25    1    0   0.07   0.05   0.00   0.07         0.    0.0244
   26    2    5   0.68   0.34   0.00   0.68         0.    0.3369
   27    3    2   0.77   0.03   0.00   0.74         0.    0.0313
   28    3    2   0.37   0.02   0.00   0.29         0.    0.0197
   29    3    4   0.78   0.04   0.00   0.74         0.    0.0238
   30    4    4   0.99   0.04   0.00   0.94         0.    0.0349
   31    4    1   0.45   0.02   0.00   0.36         0.    0.0190
   32    4    2   0.83   0.04   0.00   0.78         0.    0.0258
   33   75    4   0.01   0.01   0.00   0.01         0.    0.0082
   34   45    5   0.09   0.05   0.00   0.09         0.    0.0000
   35   46    3  10.74   0.01   0.00  10.69         0.    0.0000
   36   46    4  11.86   0.01   0.00  11.80         0.    0.0000
   37   47    5  13.01   0.01   0.00  12.94         0.    0.0000
   38   47    0  13.67   0.01   0.00  13.60         0.    0.0000
================================================================
================ TIMING STATISTICS PER NODE    ================
node# twait    ths   tcin   teig  tvect  tga   ttotal 
  1   0.07   0.05   0.27   0.00   0.00   0.18  22.23
  2   0.00   0.06   0.27   0.00   0.00   0.33  22.23
  3   0.05   0.06   0.27   0.00   0.00   0.35  22.23
  4   0.07   0.06   0.27   0.00   0.00   0.28  22.23
  5   0.13   0.06   0.27   0.00   0.00   0.23  22.23
  6   0.06   0.06   0.27   0.00   0.00   0.09  22.23
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                   130.4030s 
time spent in multnx:                 128.3945s 
integral transfer time:                 0.0517s 
time spent for loop construction:       1.7024s 
syncronization time in mult:            0.3836s 
total time per CI iteration:          133.3661s 
 ** parallelization degree for mult section:0.9971

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)           1.95        0.00       1026.375
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)          1.95        0.00        632.090
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.08        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:            3.98        0.01        757.388
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)         666.91        0.77        871.491
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)        666.91        0.70        955.883
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.44        0.00        192.747
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:         1334.26        1.47        910.629
========================================================


          starting ci iteration   4

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:  214466 2x:   53393 4x:    8315
All internal counts: zz :   32118 yy:  490036 xx:  110475 ww:  123428
One-external counts: yz :  161582 yx:  486981 yw:  504573
Two-external counts: yy :  191370 ww:   45189 xx:   45699 xz:    5228 wz:    6717 wx:   69923
Three-ext.   counts: yx :  573671 yw:  607988

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 420

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1    -0.94032319    -0.09435292    -0.10624395     0.22027921     0.21697722

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1   -155.4203528526  1.6591E-04  4.7145E-05  1.1016E-02  1.0000E-03
 mr-sdci #  4  2   -153.6657715561  9.0489E-01  0.0000E+00  0.0000E+00  1.0000E-03
 mr-sdci #  4  3   -152.2859004440  1.8392E-01  0.0000E+00  1.6738E+00  1.0000E-04
 mr-sdci #  4  4   -151.9946244076  9.6678E-02  0.0000E+00  2.0173E+00  1.0000E-04
 mr-sdci #  4  5   -151.4749468769  8.2940E+00  0.0000E+00  1.1213E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   21    4   2.35   0.09   0.00   2.35         0.    0.1070
    2   22    3   4.04   0.01   0.00   4.00         0.    0.0126
    3   22    0   1.98   0.00   0.00   1.89         0.    0.0033
    4   22    2   3.22   0.01   0.00   3.18         0.    0.0085
    5   23    1   5.38   0.01   0.00   5.34         0.    0.0106
    6   23    4   3.49   0.00   0.00   3.37         0.    0.0031
    7   23    5   3.63   0.01   0.00   3.58         0.    0.0080
    8   24    1   0.10   0.00   0.00   0.05         0.    0.0016
    9   24    3   0.11   0.00   0.00   0.06         0.    0.0015
   10   25    4   0.13   0.00   0.00   0.08         0.    0.0021
   11   25    3   0.14   0.00   0.00   0.08         0.    0.0018
   12   26    5   9.17   0.01   0.00   9.04         0.    0.0186
   13   26    3   3.32   0.00   0.00   1.79         0.    0.0032
   14   26    1   2.11   0.00   0.00   2.01         0.    0.0031
   15   26    2   7.04   0.01   0.00   6.90         0.    0.0112
   16   11    4   0.34   0.18   0.00   0.33         0.    0.1309
   17   13    2   2.20   0.15   0.00   2.15         0.    0.1882
   18   13    3   2.35   0.17   0.00   2.29         0.    0.1800
   19   14    5   3.66   0.16   0.00   2.17         0.    0.1908
   20   14    4   2.56   0.19   0.00   2.51         0.    0.1882
   21   31    5   3.33   0.00   0.00   3.28         0.    0.0014
   22   31    2   3.76   0.00   0.00   3.69         0.    0.0013
   23   32    2   5.01   0.00   0.00   4.96         0.    0.0015
   24   32    0   5.23   0.00   0.00   5.18         0.    0.0014
   25    1    4   0.07   0.05   0.00   0.07         0.    0.0244
   26    2    0   0.73   0.35   0.00   0.72         0.    0.3369
   27    3    5   1.08   0.03   0.00   1.03         0.    0.0313
   28    3    1   0.48   0.02   0.00   0.37         0.    0.0197
   29    3    3   0.96   0.04   0.00   0.92         0.    0.0238
   30    4    5   1.07   0.04   0.00   1.02         0.    0.0349
   31    4    2   0.56   0.02   0.00   0.43         0.    0.0190
   32    4    4   0.99   0.04   0.00   0.94         0.    0.0258
   33   75    3   0.01   0.01   0.00   0.01         0.    0.0082
   34   45    2   0.09   0.05   0.00   0.09         0.    0.0000
   35   46    3  10.92   0.01   0.00  10.86         0.    0.0000
   36   46    4  11.93   0.01   0.00  11.87         0.    0.0000
   37   47    1  13.82   0.01   0.00  13.76         0.    0.0000
   38   47    0  14.01   0.01   0.00  13.94         0.    0.0000
================================================================
================ TIMING STATISTICS PER NODE    ================
node# twait    ths   tcin   teig  tvect  tga   ttotal 
  1   0.00   0.08   0.40   0.00   0.00   0.15  22.52
  2   0.04   0.08   0.40   0.00   0.00   0.27  22.52
  3   0.05   0.08   0.40   0.00   0.00   0.34  22.52
  4   0.10   0.08   0.40   0.00   0.00   1.74  22.52
  5   0.08   0.08   0.40   0.00   0.00   0.25  22.52
  6   0.01   0.08   0.40   0.00   0.00   1.71  22.52
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                   131.3432s 
time spent in multnx:                 126.3100s 
integral transfer time:                 0.0546s 
time spent for loop construction:       1.7222s 
syncronization time in mult:            0.2848s 
total time per CI iteration:          135.1431s 
 ** parallelization degree for mult section:0.9978

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)           1.94        0.00        921.603
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)          1.94        0.00       1435.842
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.07        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:            3.95        0.00       1071.772
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)         666.92        2.24        297.669
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)        666.92        2.21        302.434
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.46        0.00        183.571
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:         1334.30        4.45        299.968
========================================================


          starting ci iteration   5

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:  214466 2x:   53393 4x:    8315
All internal counts: zz :   32118 yy:  490036 xx:  110475 ww:  123428
One-external counts: yz :  161582 yx:  486981 yw:  504573
Two-external counts: yy :  191370 ww:   45189 xx:   45699 xz:    5228 wz:    6717 wx:   69923
Three-ext.   counts: yx :  573671 yw:  607988

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 420

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.93986960     0.09710010    -0.07794346    -0.11844623     0.18546504    -0.22959689

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1   -155.4204009270  4.8074E-05  1.5046E-05  7.1418E-03  1.0000E-03
 mr-sdci #  5  2   -154.5211801101  8.5541E-01  0.0000E+00  0.0000E+00  1.0000E-03
 mr-sdci #  5  3   -152.4086252820  1.2272E-01  0.0000E+00  1.4998E+00  1.0000E-04
 mr-sdci #  5  4   -152.0198140094  2.5190E-02  0.0000E+00  1.7871E+00  1.0000E-04
 mr-sdci #  5  5   -151.9634908887  4.8854E-01  0.0000E+00  1.5148E+00  1.0000E-04
 mr-sdci #  5  6   -151.4437214745  8.2627E+00  0.0000E+00  2.6427E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   21    0   2.30   0.08   0.00   2.29         0.    0.1070
    2   22    3   4.09   0.01   0.00   4.05         0.    0.0126
    3   22    3   1.92   0.00   0.00   1.83         0.    0.0033
    4   22    5   3.20   0.01   0.00   3.16         0.    0.0085
    5   23    3   5.42   0.01   0.00   5.36         0.    0.0106
    6   23    0   2.02   0.00   0.00   1.93         0.    0.0031
    7   23    4   3.43   0.01   0.00   3.39         0.    0.0080
    8   24    3   0.09   0.00   0.00   0.05         0.    0.0016
    9   24    1   0.12   0.00   0.00   0.07         0.    0.0015
   10   25    5   0.14   0.00   0.00   0.08         0.    0.0021
   11   25    3   0.12   0.00   0.00   0.07         0.    0.0018
   12   26    5   9.03   0.01   0.00   8.91         0.    0.0186
   13   26    2   1.82   0.00   0.00   1.73         0.    0.0032
   14   26    1   2.18   0.00   0.00   2.07         0.    0.0031
   15   26    3   7.30   0.01   0.01   7.16         0.    0.0112
   16   11    0   0.32   0.18   0.00   0.32         0.    0.1309
   17   13    2   2.14   0.15   0.00   2.10         0.    0.1882
   18   13    4   2.68   0.18   0.00   2.64         0.    0.1800
   19   14    5   2.25   0.16   0.00   2.20         0.    0.1908
   20   14    3   2.60   0.19   0.00   2.55         0.    0.1882
   21   31    1   3.13   0.00   0.00   3.09         0.    0.0014
   22   31    2   3.48   0.00   0.00   3.43         0.    0.0013
   23   32    1   3.76   0.00   0.00   3.69         0.    0.0015
   24   32    5   5.26   0.00   0.00   5.20         0.    0.0014
   25    1    1   0.07   0.05   0.00   0.07         0.    0.0244
   26    2    4   0.67   0.34   0.00   0.67         0.    0.3369
   27    3    0   0.91   0.03   0.00   0.87         0.    0.0313
   28    3    2   1.64   0.02   0.00   1.54         0.    0.0197
   29    3    0   0.83   0.04   0.00   0.79         0.    0.0238
   30    4    5   1.18   0.04   0.00   1.13         0.    0.0349
   31    4    5   0.45   0.02   0.00   0.35         0.    0.0190
   32    4    2   0.98   0.04   0.00   0.93         0.    0.0258
   33   75    5   0.01   0.01   0.00   0.01         0.    0.0082
   34   45    4   0.09   0.05   0.00   0.09         0.    0.0000
   35   46    1  12.32   0.01   0.00  12.26         0.    0.0000
   36   46    2  12.89   0.01   0.00  12.83         0.    0.0000
   37   47    4  14.70   0.01   0.00  14.64         0.    0.0000
   38   47    0  15.26   0.01   0.00  15.19         0.    0.0000
================================================================
================ TIMING STATISTICS PER NODE    ================
node# twait    ths   tcin   teig  tvect  tga   ttotal 
  1   1.31   0.10   0.53   0.00   0.00   0.18  23.70
  2   1.38   0.11   0.53   0.00   0.00   0.24  23.70
  3   0.00   0.11   0.53   0.00   0.00   0.29  23.70
  4   1.40   0.11   0.53   0.00   0.00   0.35  23.70
  5   1.38   0.11   0.53   0.00   0.00   0.11  23.70
  6   1.43   0.11   0.53   0.00   0.00   0.35  23.70
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                   130.8173s 
time spent in multnx:                 128.7376s 
integral transfer time:                 0.0530s 
time spent for loop construction:       1.7120s 
syncronization time in mult:            6.8891s 
total time per CI iteration:          142.2239s 
 ** parallelization degree for mult section:0.9500

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)           1.95        0.00       1070.518
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)          1.94        0.00       1479.486
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.08        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:            3.96        0.00       1170.572
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)         666.92        0.76        871.806
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)        666.92        0.75        890.548
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.44        0.00        238.191
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:         1334.29        1.52        880.286
========================================================


          starting ci iteration   6

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:  214466 2x:   53393 4x:    8315
All internal counts: zz :   32118 yy:  490036 xx:  110475 ww:  123428
One-external counts: yz :  161582 yx:  486981 yw:  504573
Two-external counts: yy :  191370 ww:   45189 xx:   45699 xz:    5228 wz:    6717 wx:   69923
Three-ext.   counts: yx :  573671 yw:  607988

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 420

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1    -0.93987620    -0.00211908

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  6  1   -155.4204130277  1.2101E-05  4.8763E-06  3.6061E-03  1.0000E-03
 mr-sdci #  6  2   -152.7284105780 -1.7928E+00  0.0000E+00  1.9882E+00  1.0000E-03
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   21    2   2.47   0.09   0.00   2.46         0.    0.1070
    2   22    5   5.27   0.01   0.00   5.23         0.    0.0126
    3   22    2   1.93   0.00   0.00   1.82         0.    0.0033
    4   22    1   3.29   0.01   0.00   3.25         0.    0.0085
    5   23    2   4.28   0.01   0.00   4.22         0.    0.0106
    6   23    5   2.09   0.00   0.00   1.98         0.    0.0031
    7   23    4   3.56   0.01   0.00   3.52         0.    0.0080
    8   24    4   0.11   0.00   0.00   0.06         0.    0.0016
    9   24    0   0.12   0.00   0.00   0.06         0.    0.0015
   10   25    1   0.16   0.00   0.00   0.10         0.    0.0021
   11   25    1   0.15   0.00   0.00   0.09         0.    0.0018
   12   26    4   7.55   0.01   0.00   7.43         0.    0.0186
   13   26    3   1.92   0.00   0.00   1.82         0.    0.0032
   14   26    1   2.04   0.00   0.00   1.95         0.    0.0031
   15   26    2   5.83   0.01   0.00   5.70         0.    0.0112
   16   11    4   0.34   0.18   0.00   0.34         0.    0.1309
   17   13    0   2.20   0.15   0.00   2.16         0.    0.1882
   18   13    5   2.41   0.18   0.00   2.36         0.    0.1800
   19   14    4   2.24   0.16   0.00   2.21         0.    0.1908
   20   14    3   2.72   0.19   0.00   2.65         0.    0.1882
   21   31    0   3.34   0.00   0.00   3.30         0.    0.0014
   22   31    3   5.05   0.00   0.00   5.01         0.    0.0013
   23   32    2   5.15   0.00   0.00   5.11         0.    0.0015
   24   32    4   5.18   0.00   0.00   5.11         0.    0.0014
   25    1    2   0.07   0.05   0.00   0.07         0.    0.0244
   26    2    2   0.74   0.35   0.00   0.73         0.    0.3369
   27    3    0   1.16   0.03   0.00   1.12         0.    0.0313
   28    3    4   0.37   0.02   0.00   0.29         0.    0.0197
   29    3    5   1.10   0.04   0.00   1.04         0.    0.0238
   30    4    1   1.24   0.04   0.00   1.19         0.    0.0349
   31    4    3   0.59   0.02   0.00   0.46         0.    0.0190
   32    4    4   1.13   0.04   0.00   1.08         0.    0.0258
   33   75    0   0.01   0.01   0.00   0.01         0.    0.0082
   34   45    1   0.10   0.05   0.00   0.10         0.    0.0000
   35   46    5   9.66   0.01   0.00   9.61         0.    0.0000
   36   46    3  10.16   0.01   0.00  10.11         0.    0.0000
   37   47    1  13.49   0.01   0.00  13.42         0.    0.0000
   38   47    0  13.58   0.01   0.00  13.52         0.    0.0000
================================================================
================ TIMING STATISTICS PER NODE    ================
node# twait    ths   tcin   teig  tvect  tga   ttotal 
  1   0.13   0.03   0.14   0.00   0.00   0.18  20.73
  2   0.07   0.03   0.14   0.00   0.00   0.28  20.73
  3   0.08   0.03   0.14   0.00   0.00   0.26  20.73
  4   0.09   0.03   0.14   0.00   0.00   0.30  20.73
  5   0.04   0.03   0.14   0.00   0.00   0.33  20.73
  6   0.00   0.03   0.14   0.00   0.00   0.23  20.73
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                   122.8219s 
time spent in multnx:                 120.6660s 
integral transfer time:                 0.0555s 
time spent for loop construction:       1.7322s 
syncronization time in mult:            0.4104s 
total time per CI iteration:          124.3737s 
 ** parallelization degree for mult section:0.9967

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)           1.95        0.00        935.653
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)          1.95        0.00       1007.096
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.08        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:            3.97        0.00        926.204
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)         666.92        0.78        850.198
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)        666.92        0.78        849.796
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.44        0.00        225.491
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:         1334.28        1.57        849.217
========================================================


          starting ci iteration   7

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:  214466 2x:   53393 4x:    8315
All internal counts: zz :   32118 yy:  490036 xx:  110475 ww:  123428
One-external counts: yz :  161582 yx:  486981 yw:  504573
Two-external counts: yy :  191370 ww:   45189 xx:   45699 xz:    5228 wz:    6717 wx:   69923
Three-ext.   counts: yx :  573671 yw:  607988

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 420

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1    -0.93979305    -0.03219220     0.02668144

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  7  1   -155.4204190204  5.9927E-06  1.9196E-06  2.2919E-03  1.0000E-03
 mr-sdci #  7  2   -154.4184481086  1.6900E+00  0.0000E+00  4.6611E+00  1.0000E-03
 mr-sdci #  7  3   -151.6920228208 -7.1660E-01  0.0000E+00  1.4122E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   21    4   2.36   0.09   0.00   2.36         0.    0.1070
    2   22    4   4.57   0.01   0.00   4.52         0.    0.0126
    3   22    4   2.06   0.00   0.00   1.95         0.    0.0033
    4   22    0   3.25   0.01   0.00   3.21         0.    0.0085
    5   23    5   4.04   0.01   0.00   3.98         0.    0.0106
    6   23    0   2.14   0.00   0.00   2.02         0.    0.0031
    7   23    2   3.48   0.01   0.00   3.42         0.    0.0080
    8   24    2   0.08   0.00   0.00   0.05         0.    0.0016
    9   24    3   0.09   0.00   0.00   0.05         0.    0.0015
   10   25    2   0.11   0.00   0.00   0.07         0.    0.0021
   11   25    4   0.12   0.00   0.00   0.07         0.    0.0018
   12   26    2   9.05   0.01   0.00   8.92         0.    0.0186
   13   26    2   1.95   0.00   0.00   1.85         0.    0.0032
   14   26    3   2.15   0.00   0.00   2.04         0.    0.0031
   15   26    4   7.53   0.01   0.00   7.39         0.    0.0112
   16   11    5   0.32   0.18   0.00   0.32         0.    0.1309
   17   13    1   2.31   0.15   0.00   2.26         0.    0.1882
   18   13    2   2.43   0.17   0.00   2.39         0.    0.1800
   19   14    5   2.44   0.16   0.00   2.38         0.    0.1908
   20   14    3   2.73   0.19   0.00   2.69         0.    0.1882
   21   31    1   3.08   0.00   0.00   3.04         0.    0.0014
   22   31    4   3.72   0.00   0.00   3.66         0.    0.0013
   23   32    3   3.93   0.00   0.00   3.87         0.    0.0015
   24   32    2   4.00   0.00   0.00   3.93         0.    0.0014
   25    1    3   0.07   0.06   0.00   0.07         0.    0.0244
   26    2    4   0.69   0.34   0.00   0.69         0.    0.3369
   27    3    1   1.12   0.03   0.00   1.07         0.    0.0313
   28    3    1   0.40   0.02   0.00   0.31         0.    0.0197
   29    3    3   0.94   0.04   0.00   0.89         0.    0.0238
   30    4    5   1.39   0.04   0.00   1.33         0.    0.0349
   31    4    0   0.45   0.02   0.00   0.36         0.    0.0190
   32    4    0   1.04   0.04   0.00   0.98         0.    0.0258
   33   75    2   0.01   0.01   0.00   0.01         0.    0.0082
   34   45    4   0.09   0.05   0.00   0.09         0.    0.0000
   35   46    3  11.26   0.01   0.00  11.20         0.    0.0000
   36   46    5  13.03   0.01   0.00  12.97         0.    0.0000
   37   47    1  14.28   0.01   0.00  14.22         0.    0.0000
   38   47    0  14.30   0.01   0.00  14.24         0.    0.0000
================================================================
================ TIMING STATISTICS PER NODE    ================
node# twait    ths   tcin   teig  tvect  tga   ttotal 
  1   0.03   0.04   0.19   0.00   0.00   0.27  21.47
  2   0.02   0.04   0.19   0.00   0.00   0.21  21.47
  3   0.09   0.04   0.19   0.00   0.00   0.36  21.47
  4   0.06   0.04   0.19   0.00   0.00   0.27  21.47
  5   0.07   0.04   0.19   0.00   0.00   0.30  21.47
  6   0.00   0.04   0.19   0.00   0.00   0.18  21.47
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                   127.0327s 
time spent in multnx:                 124.8263s 
integral transfer time:                 0.0537s 
time spent for loop construction:       1.7136s 
syncronization time in mult:            0.2757s 
total time per CI iteration:          128.8050s 
 ** parallelization degree for mult section:0.9978

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)           0.00        0.00          0.000
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.05        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:            0.05        0.00          0.000
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)         668.86        0.82        816.436
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)        668.86        0.78        853.360
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.47        0.00        235.879
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:         1338.20        1.61        833.742
========================================================


          starting ci iteration   8

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:  214466 2x:   53393 4x:    8315
All internal counts: zz :   32118 yy:  490036 xx:  110475 ww:  123428
One-external counts: yz :  161582 yx:  486981 yw:  504573
Two-external counts: yy :  191370 ww:   45189 xx:   45699 xz:    5228 wz:    6717 wx:   69923
Three-ext.   counts: yx :  573671 yw:  607988

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 420

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1    -0.93969610     0.05614189     0.04294181     0.03005050

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  8  1   -155.4204208664  1.8460E-06  4.4963E-07  1.2724E-03  1.0000E-03
 mr-sdci #  8  2   -154.8647846548  4.4634E-01  0.0000E+00  4.5802E+00  1.0000E-03
 mr-sdci #  8  3   -152.1635558744  4.7153E-01  0.0000E+00  1.3574E+00  1.0000E-04
 mr-sdci #  8  4   -151.6895785207 -3.3024E-01  0.0000E+00  2.6217E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   21    2   2.41   0.09   0.00   2.41         0.    0.1070
    2   22    3   6.00   0.01   0.00   5.95         0.    0.0126
    3   22    3   1.94   0.00   0.00   1.83         0.    0.0033
    4   22    2   5.11   0.01   0.00   5.05         0.    0.0085
    5   23    1   6.14   0.01   0.00   6.08         0.    0.0106
    6   23    4   2.22   0.00   0.00   2.10         0.    0.0031
    7   23    1   5.39   0.01   0.00   5.33         0.    0.0080
    8   24    0   0.12   0.00   0.00   0.06         0.    0.0016
    9   24    3   0.12   0.00   0.00   0.06         0.    0.0015
   10   25    4   0.18   0.00   0.00   0.10         0.    0.0021
   11   25    0   0.15   0.00   0.00   0.09         0.    0.0018
   12   26    1   9.91   0.01   0.00   9.80         0.    0.0186
   13   26    5   2.03   0.00   0.00   1.92         0.    0.0032
   14   26    0   2.13   0.00   0.00   2.03         0.    0.0031
   15   26    3   8.21   0.01   0.00   8.09         0.    0.0112
   16   11    1   0.36   0.18   0.00   0.35         0.    0.1309
   17   13    1   2.40   0.15   0.00   2.36         0.    0.1882
   18   13    5   4.05   0.18   0.00   4.01         0.    0.1800
   19   14    3   3.91   0.16   0.00   3.86         0.    0.1908
   20   14    4   4.30   0.19   0.00   4.23         0.    0.1882
   21   31    0   4.86   0.00   0.00   4.80         0.    0.0014
   22   31    5   4.20   0.00   0.00   4.13         0.    0.0013
   23   32    3   4.14   0.00   0.00   4.08         0.    0.0015
   24   32    4   4.46   0.00   0.00   4.38         0.    0.0014
   25    1    3   0.07   0.05   0.00   0.07         0.    0.0244
   26    2    4   0.75   0.36   0.00   0.74         0.    0.3369
   27    3    2   1.00   0.03   0.00   0.94         0.    0.0313
   28    3    2   0.55   0.02   0.00   0.43         0.    0.0197
   29    3    3   0.95   0.03   0.00   0.90         0.    0.0238
   30    4    0   1.27   0.04   0.00   1.21         0.    0.0349
   31    4    5   0.64   0.02   0.00   0.50         0.    0.0190
   32    4    1   1.14   0.04   0.00   1.07         0.    0.0258
   33   75    4   0.01   0.01   0.00   0.01         0.    0.0082
   34   45    0   0.10   0.05   0.00   0.10         0.    0.0000
   35   46    4  13.42   0.01   0.00  13.37         0.    0.0000
   36   46    5  14.48   0.01   0.00  14.42         0.    0.0000
   37   47    2  16.26   0.01   0.00  16.20         0.    0.0000
   38   47    0  16.77   0.01   0.00  16.71         0.    0.0000
================================================================
================ TIMING STATISTICS PER NODE    ================
node# twait    ths   tcin   teig  tvect  tga   ttotal 
  1   0.00   0.07   0.28   0.00   0.00   0.29  25.76
  2   0.05   0.07   0.28   0.00   0.00   0.25  25.76
  3   0.06   0.07   0.28   0.00   0.00   0.21  25.76
  4   0.05   0.07   0.28   0.00   0.00   0.37  25.76
  5   0.06   0.07   0.28   0.00   0.00   0.28  25.76
  6   0.00   0.07   0.28   0.00   0.00   0.30  25.76
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                   152.1267s 
time spent in multnx:                 149.7694s 
integral transfer time:                 0.0582s 
time spent for loop construction:       1.7335s 
syncronization time in mult:            0.2276s 
total time per CI iteration:          154.5670s 
 ** parallelization degree for mult section:0.9985

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)           1.95        0.00        787.297
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)          1.95        0.00        984.494
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.09        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:            3.99        0.00        829.617
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)         666.92        0.84        794.072
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)        666.92        0.86        776.338
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.43        0.00        210.987
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:         1334.26        1.70        784.420
========================================================


          starting ci iteration   9

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:  214466 2x:   53393 4x:    8315
All internal counts: zz :   32118 yy:  490036 xx:  110475 ww:  123428
One-external counts: yz :  161582 yx:  486981 yw:  504573
Two-external counts: yy :  191370 ww:   45189 xx:   45699 xz:    5228 wz:    6717 wx:   69923
Three-ext.   counts: yx :  573671 yw:  607988

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 420

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1    -0.93965441     0.06603857    -0.06620718     0.00420354     0.02094175

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  9  1   -155.4204212964  4.3000E-07  6.5437E-08  4.3538E-04  1.0000E-03
 mr-sdci #  9  2   -154.9303509593  6.5566E-02  0.0000E+00  4.0356E+00  1.0000E-03
 mr-sdci #  9  3   -152.6481781607  4.8462E-01  0.0000E+00  2.8849E+00  1.0000E-04
 mr-sdci #  9  4   -151.9923059050  3.0273E-01  0.0000E+00  5.8578E-01  1.0000E-04
 mr-sdci #  9  5   -151.6299720951 -3.3352E-01  0.0000E+00  1.9246E+00  1.0000E-04
 
 
 total energy in cosmo calc 
e(nroot) + repnuc=    -155.4204212964
dielectric energy =       0.0000000000
deltaediel =       0.0000000000
deltaelast =       0.0000000000
 

 mr-sdci  convergence criteria satisfied after  9 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  9  1   -155.4204212964  4.3000E-07  6.5437E-08  4.3538E-04  1.0000E-03
 mr-sdci #  9  2   -154.9303509593  6.5566E-02  0.0000E+00  4.0356E+00  1.0000E-03
 mr-sdci #  9  3   -152.6481781607  4.8462E-01  0.0000E+00  2.8849E+00  1.0000E-04
 mr-sdci #  9  4   -151.9923059050  3.0273E-01  0.0000E+00  5.8578E-01  1.0000E-04
 mr-sdci #  9  5   -151.6299720951 -3.3352E-01  0.0000E+00  1.9246E+00  1.0000E-04

####################CIUDGINFO####################

   ci vector at position   1 energy= -155.420421296366

################END OF CIUDGINFO################

 
    1 of the   6 expansion vectors are transformed.
 nnormold( 1 )= 0.920946250204074723
 nnormold( 2 )= 0.537960872601168378E-05
 nnormold( 3 )= 0.249356793903019248E-05
 nnormold( 4 )= 0.108815451186782916E-05
 nnormold( 5 )= 0.131511796443539374E-06
 nnormnew( 1 )= 0.921077584996921384
    1 of the   5 matrix-vector products are transformed.
 nnormold( 1 )= 137.962397956517179
 nnormold( 2 )= 0.565069890276256978E-03
 nnormold( 3 )= 0.281492160097965044E-03
 nnormold( 4 )= 0.123127123817563274E-03
 nnormold( 5 )= 0.125269656565545715E-04
 nnormnew( 1 )= 137.981171333370185

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
 maximum overlap with reference  1 (overlap= 0.939654414451172482 )

information on vector: 1from unit 11 written to unit 16filename civout                                                      


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =      -155.4204212964

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7    8    9   10   11   12   13

                                          orbital     5    6    7   54   55   56   57   58   59  107  108  127  128

                                         symmetry   Ag   Ag   Ag   Bu   Bu   Bu   Bu   Bu   Bu   Au   Au   Bg   Bg 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1  0.153133                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-           
 z*  1  1       2 -0.906638                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +-      
 z*  1  1       3 -0.055582                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +     - 
 z*  1  1       4  0.054105                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-             +- 
 z*  1  1       5  0.062528                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +-      
 z*  1  1       6  0.103927                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +     - 
 z*  1  1       8 -0.106376                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +     -    - 
 z*  1  1       9  0.062245                        +-   +-   +-   +-   +-   +-   +-   +-   +-        +-   +-      
 z*  1  1      11 -0.024875                        +-   +-   +-   +-   +-   +-   +-   +-   +-        +-        +- 
 z*  1  1      12  0.062547                        +-   +-   +-   +-   +-   +-   +-   +-   +-             +-   +- 
 y   1  1     422  0.013397              2( Au )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -           
 y   1  1     493 -0.012670              1( Bg )   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +     -      
 y   1  1     548 -0.010159              2( Au )   +-   +-   +-   +-   +-   +-   +-   +-   +-    -        +     - 
 y   1  1     583  0.012231              1( Bg )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -    -      
 y   1  1     584 -0.014557              2( Bg )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -    -      
 y   1  1     620 -0.011788              2( Au )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +          -    - 
 y   1  1     621  0.010627              3( Au )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +          -    - 
 y   1  1     674 -0.014636              2( Au )   +-   +-   +-   +-   +-   +-   +-   +-   +-         -   +-      
 y   1  1     829  0.013675              2( Ag )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +     -      
 y   1  1     832  0.013006              5( Ag )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +     -      
 y   1  1     836 -0.010184              9( Ag )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +     -      
 y   1  1    2817  0.010506              1( Ag )   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +     -      
 y   1  1    2965 -0.012357             10( Bu )   +-   +-   +-   +-   +-   +-   +-    -   +-   +-        +     - 
 y   1  1    2967 -0.013120             12( Bu )   +-   +-   +-   +-   +-   +-   +-    -   +-   +-        +     - 
 y   1  1    3146 -0.010969              5( Bu )   +-   +-   +-   +-   +-   +-   +-    -   +-   +     -   +-      
 y   1  1    3151 -0.011897             10( Bu )   +-   +-   +-   +-   +-   +-   +-    -   +-   +     -   +-      
 y   1  1    3153 -0.013586             12( Bu )   +-   +-   +-   +-   +-   +-   +-    -   +-   +     -   +-      
 y   1  1    3341 -0.010047             12( Ag )   +-   +-   +-   +-   +-   +-   +-    -   +-   +         +-    - 
 y   1  1   10728  0.011966              7( Bu )   +-   +-   +-   +-   +-   +-   +    +-   +-   +-         -    - 
 y   1  1   11054 -0.014454              6( Ag )   +-   +-   +-   +-   +-   +-   +    +-   +-    -        +-    - 
 y   1  1   15604 -0.010075             12( Bu )   +-   +-   +-   +-   +-    -   +-   +-   +-   +-        +     - 
 y   1  1   15790 -0.010234             12( Bu )   +-   +-   +-   +-   +-    -   +-   +-   +-   +     -   +-      
 y   1  1   20426 -0.013107              7( Ag )   +-   +-   +-   +-   +-   +    +-   +-   +-   +-    -    -      
 y   1  1   58273 -0.013748              5( Bu )   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +     -      
 y   1  1   58413  0.011317              5( Ag )   +-   +-    -   +-   +-   +-   +-   +-   +-   +-        +     - 
 y   1  1   58599  0.013056              5( Ag )   +-   +-    -   +-   +-   +-   +-   +-   +-   +     -   +-      
 y   1  1   58784 -0.010922              6( Bu )   +-   +-    -   +-   +-   +-   +-   +-   +-   +         +-    - 
 y   1  1   58791 -0.011221             13( Bu )   +-   +-    -   +-   +-   +-   +-   +-   +-   +         +-    - 
 y   1  1   78375 -0.011391              5( Bu )   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +     -      
 y   1  1   78376  0.012565              6( Bu )   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +     -      
 y   1  1   78380 -0.016418             10( Bu )   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +     -      
 y   1  1   78382 -0.018018             12( Bu )   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +     -      
 y   1  1   78517  0.010210              7( Ag )   +-    -   +-   +-   +-   +-   +-   +-   +-   +-        +     - 
 y   1  1   78521  0.011117             11( Ag )   +-    -   +-   +-   +-   +-   +-   +-   +-   +-        +     - 
 y   1  1   78522  0.012381             12( Ag )   +-    -   +-   +-   +-   +-   +-   +-   +-   +-        +     - 
 y   1  1   78708  0.014234             12( Ag )   +-    -   +-   +-   +-   +-   +-   +-   +-   +     -   +-      
 y   1  1   78712  0.010173             16( Ag )   +-    -   +-   +-   +-   +-   +-   +-   +-   +     -   +-      
 y   1  1   78892  0.012050             12( Bu )   +-    -   +-   +-   +-   +-   +-   +-   +-   +         +-    - 
 y   1  1  101795 -0.010977              9( Ag )    -   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +-      
 y   1  1  101976  0.010893              6( Bu )    -   +-   +-   +-   +-   +-   +-   +-   +-   +         +-    - 
 y   1  1  101983  0.011355             13( Bu )    -   +-   +-   +-   +-   +-   +-   +-   +-   +         +-    - 
 x   1  1 3351659 -0.013938    7( Bu )   3( Au )   +-    -   +-   +-   +-   +-    -   +    +-   +-    -   +-      
 x   1  1 3360026  0.010967    6( Ag )   3( Au )   +-    -   +-   +-   +-   +-    -   +    +-   +-         -   +- 
 w   1  1 4112533  0.010485    7( Bu )   7( Bu )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-                
 w   1  1 4113805  0.010550    1( Bg )   1( Bg )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-                
 w   1  1 4116808 -0.017679    6( Ag )   7( Bu )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +          -      
 w   1  1 4118689 -0.015654    1( Au )   1( Bg )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +          -      
 w   1  1 4129042  0.013500    6( Ag )   6( Ag )   +-   +-   +-   +-   +-   +-   +-   +-   +-             +-      
 w   1  1 4131231  0.010131    1( Au )   1( Au )   +-   +-   +-   +-   +-   +-   +-   +-   +-             +-      
 w   1  1 4384478  0.010125    5( Bu )   5( Bu )   +-   +-   +-   +-   +-   +-   +-        +-   +-        +-      
 w   1  1 7036798  0.011906    5( Bu )   5( Bu )   +-   +-   +     -   +-   +-   +-        +-   +-   +     -   +- 
 w   1  1 7722695  0.010153    5( Bu )   9( Bu )   +-   +    +-    -   +-   +-   +-        +-   +-   +     -   +- 

 ci coefficient statistics:
           rq > 0.1                4
      0.1> rq > 0.01              58
     0.01> rq > 0.001          16767
    0.001> rq > 0.0001        248367
   0.0001> rq > 0.00001      1116968
  0.00001> rq > 0.000001     2014799
 0.000001> rq                5344662
           all               8741625
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:   11918 2x:       0 4x:       0
All internal counts: zz :   32118 yy:       0 xx:       0 ww:       0
One-external counts: yz :       0 yx:       0 yw:       0
Two-external counts: yy :       0 ww:       0 xx:       0 xz:       0 wz:       0 wx:       0
Three-ext.   counts: yx :       0 yw:       0

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.153132805806    -23.749698472789
     2     2     -0.906637528366    140.535086863724
     3     3     -0.055581697059      8.619309699666
     4     4      0.054105400902     -8.394675403409
     5     5      0.062527530868     -9.694523017966
     6     6      0.103927247615    -16.126798057044
     7     7     -0.004893509382      0.759445835648
     8     8     -0.106376068186     16.504157730712
     9     9      0.062245277057     -9.656203142345
    10    10      0.004890336885     -0.759499918619
    11    11     -0.024875001323      3.862702625742
    12    12      0.062546712953     -9.706601860005
    13    13      0.005556844741     -0.861534677024
    14    14      0.000350510053     -0.053953449969
    15    15     -0.001068375083      0.165738452561
    16    16      0.004054005799     -0.628146239169
    17    17     -0.000393673725      0.061149206833
    18    18     -0.000634386725      0.098435866898
    19    19     -0.000067331589      0.010106817614
    20    20     -0.000035845580      0.005603258267
    21    21      0.000211017886     -0.032760719835
    22    22     -0.001225129846      0.190027975989
    23    23      0.000070506439     -0.010723246082
    24    24      0.000087007807     -0.013498737530
    25    25      0.000195682605     -0.030301513527
    26    26     -0.000097614866      0.015076857263
    27    27      0.005140612725     -0.797259854070
    28    28      0.000247081437     -0.038033632803
    29    29     -0.000972534493      0.150990195503
    30    30      0.003858650926     -0.598166952910
    31    31     -0.000472486243      0.073109508233
    32    32     -0.000590950265      0.091732505415
    33    33     -0.000048210967      0.007484850811

 number of reference csfs (nref) is    33.  root number (iroot) is  1.

 pople ci energy extrapolation is computed with 22 correlated electrons.

 eref      =   -155.015472364016   "relaxed" cnot**2         =   0.886031253238
 eci       =   -155.420421296366   deltae = eci - eref       =  -0.404948932349
 eci+dv1   =   -155.466572818688   dv1 = (1-cnot**2)*deltae  =  -0.046151522323
 eci+dv2   =   -155.472509213303   dv2 = dv1 / cnot**2       =  -0.052087916937
 eci+dv3   =   -155.480198221078   dv3 = dv1 / (2*cnot**2-1) =  -0.059776924712
 eci+pople =   -155.473253479140   ( 22e- scaled deltae )    =  -0.457781115123
NO coefficients and occupation numbers are written to nocoef_ci.1
 entering drivercid: firstcall,firstnonref= T F

################################################################################
 one electron density matrix calculation 
################################################################################


 workspace allocation information: mxcore=30240192
 
 space required:
 
    space required for calls in multd2:
       onex         2491
       allin           0
       diagon      12755
    max.      ---------
       maxnex      12755
 
    total core space usage:
       maxnex      12755
    max k-seg    2379039
    max k-ind       4326
              ---------
       totmax    4779485
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=    5435  DYX=   16031  DYW=   16939
   D0Z=    1746  D0Y=   22064  D0X=    7405  D0W=    8533
  DDZI=    2933 DDYI=   29580 DDXI=   11282 DDWI=   12531
  DDZE=       0 DDYE=    4326 DDXE=    1882 DDWE=    2107
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   2.0000000      2.0000000      2.0000000      2.0000000      1.9755411
   1.9728170      1.9717178      0.0125607      0.0116454      0.0103244
   0.0093066      0.0064497      0.0051293      0.0043826      0.0025321
   0.0021013      0.0015760      0.0011119      0.0009752      0.0009082
   0.0007113      0.0005582      0.0004520      0.0004419      0.0004174
   0.0003267      0.0002762      0.0002509      0.0002315      0.0001546
   0.0001379      0.0001154      0.0001075      0.0000944      0.0000842
   0.0000778      0.0000694      0.0000638      0.0000595      0.0000535
   0.0000494      0.0000469      0.0000440      0.0000352      0.0000300
   0.0000247      0.0000199      0.0000174      0.0000161      0.0000100
   0.0000093      0.0000081      0.0000046


*****   symmetry block SYM2   *****

 occupation numbers of nos
   1.9995655      1.9995616      1.9851383      1.9784006      1.9754717
   1.9729353      0.0142228      0.0123892      0.0105242      0.0093159
   0.0087276      0.0066669      0.0051558      0.0039709      0.0023037
   0.0016997      0.0011466      0.0009649      0.0009239      0.0007253
   0.0006625      0.0005462      0.0004665      0.0004334      0.0003909
   0.0002767      0.0002359      0.0001830      0.0001705      0.0001364
   0.0001218      0.0001093      0.0000864      0.0000796      0.0000722
   0.0000686      0.0000630      0.0000615      0.0000592      0.0000579
   0.0000507      0.0000466      0.0000350      0.0000330      0.0000263
   0.0000216      0.0000178      0.0000158      0.0000129      0.0000090
   0.0000080      0.0000048      0.0000044


*****   symmetry block SYM3   *****

 occupation numbers of nos
   1.9287718      0.1018458      0.0061610      0.0053503      0.0032942
   0.0027349      0.0019929      0.0006932      0.0005184      0.0004503
   0.0003337      0.0002644      0.0002127      0.0001687      0.0001098
   0.0000861      0.0000654      0.0000328      0.0000275      0.0000224


*****   symmetry block SYM4   *****

 occupation numbers of nos
   1.8845092      0.0542992      0.0060270      0.0043436      0.0031750
   0.0021586      0.0009323      0.0006368      0.0005636      0.0004487
   0.0003311      0.0002607      0.0002110      0.0001720      0.0001143
   0.0000817      0.0000646      0.0000314      0.0000251      0.0000213


 total number of electrons =   30.0000000000



          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        Ag  partial gross atomic populations
   ao class       1Ag        2Ag        3Ag        4Ag        5Ag        6Ag 
    C1_ s       0.828078   0.013791   1.984577   0.742037   1.006667   0.247179
    C2_ s       0.682266   1.985503   0.013847   1.111303   0.106696   0.876239
    H1_ s       0.165622  -0.000035   0.000716   0.037003   0.490986   0.138365
    H2_ s       0.251377  -0.000068   0.000822   0.031652   0.342452  -0.003037
    H3_ s       0.072658   0.000809   0.000038   0.078005   0.028740   0.714070
 
   ao class       7Ag        8Ag        9Ag       10Ag       11Ag       12Ag 
    C1_ s       0.594275   0.003101   0.006318   0.003092   0.004191   0.002627
    C2_ s       1.088999   0.005714   0.000875   0.005700   0.001065   0.002794
    H1_ s       0.018814   0.000234   0.003521   0.000127   0.001158   0.000374
    H2_ s       0.235059   0.000802   0.000767   0.000103   0.002478   0.000293
    H3_ s       0.034571   0.002710   0.000164   0.001302   0.000414   0.000361
 
   ao class      13Ag       14Ag       15Ag       16Ag       17Ag       18Ag 
    C1_ s       0.001296   0.003209   0.000626   0.000558   0.000632   0.000388
    C2_ s       0.003147   0.000136   0.001326   0.000726   0.000578   0.000478
    H1_ s       0.000128   0.000377   0.000195   0.000242   0.000122   0.000029
    H2_ s       0.000124   0.000638   0.000021   0.000057   0.000138   0.000156
    H3_ s       0.000434   0.000023   0.000364   0.000518   0.000105   0.000061
 
   ao class      19Ag       20Ag       21Ag       22Ag       23Ag       24Ag 
    C1_ s       0.000312   0.000289   0.000162   0.000146   0.000204   0.000267
    C2_ s       0.000234   0.000374   0.000405   0.000283   0.000124   0.000066
    H1_ s       0.000151   0.000163   0.000061   0.000069   0.000054   0.000025
    H2_ s       0.000245   0.000055   0.000074  -0.000001   0.000056   0.000050
    H3_ s       0.000033   0.000026   0.000010   0.000062   0.000014   0.000034
 
   ao class      25Ag       26Ag       27Ag       28Ag       29Ag       30Ag 
    C1_ s       0.000111   0.000087   0.000098   0.000115   0.000135   0.000046
    C2_ s       0.000224   0.000157   0.000120   0.000070   0.000049   0.000084
    H1_ s       0.000031   0.000017   0.000025   0.000000   0.000018   0.000004
    H2_ s       0.000010   0.000016  -0.000004   0.000015   0.000012   0.000014
    H3_ s       0.000042   0.000050   0.000037   0.000051   0.000017   0.000007
 
   ao class      31Ag       32Ag       33Ag       34Ag       35Ag       36Ag 
    C1_ s       0.000033   0.000038   0.000032   0.000015   0.000018   0.000019
    C2_ s       0.000069   0.000059   0.000036   0.000037   0.000036   0.000019
    H1_ s       0.000015   0.000004   0.000018   0.000024   0.000003   0.000004
    H2_ s       0.000011   0.000008   0.000009   0.000005   0.000015   0.000032
    H3_ s       0.000010   0.000006   0.000012   0.000014   0.000012   0.000004
 
   ao class      37Ag       38Ag       39Ag       40Ag       41Ag       42Ag 
    C1_ s       0.000000   0.000012   0.000013   0.000011   0.000019   0.000000
    C2_ s       0.000016   0.000018   0.000015   0.000005   0.000002  -0.000005
    H1_ s       0.000020   0.000013   0.000004   0.000024   0.000011   0.000009
    H2_ s       0.000030   0.000014   0.000017   0.000008   0.000007   0.000001
    H3_ s       0.000003   0.000006   0.000011   0.000005   0.000010   0.000042
 
   ao class      43Ag       44Ag       45Ag       46Ag       47Ag       48Ag 
    C1_ s       0.000008   0.000008   0.000007   0.000007   0.000002   0.000006
    C2_ s       0.000007   0.000007   0.000006   0.000007   0.000001   0.000002
    H1_ s       0.000013   0.000003   0.000010   0.000001   0.000005   0.000002
    H2_ s       0.000010   0.000007   0.000004   0.000008   0.000006   0.000001
    H3_ s       0.000005   0.000009   0.000004   0.000002   0.000006   0.000005
 
   ao class      49Ag       50Ag       51Ag       52Ag       53Ag 
    C1_ s       0.000001   0.000000   0.000004   0.000000   0.000001
    C2_ s       0.000003   0.000001  -0.000001   0.000000   0.000000
    H1_ s       0.000002   0.000002   0.000003   0.000003   0.000002
    H2_ s       0.000005   0.000001   0.000002   0.000005   0.000001
    H3_ s       0.000005   0.000006   0.000001   0.000000   0.000001

                        Bu  partial gross atomic populations
   ao class       1Bu        2Bu        3Bu        4Bu        5Bu        6Bu 
    C1_ s       0.391157   1.607963   1.184800   0.339032   1.024016   0.712262
    C2_ s       1.607726   0.390524   0.407899   0.931470   0.286607   0.614690
    H1_ s       0.000085   0.000531   0.140742   0.196030   0.094278   0.400404
    H2_ s       0.000105   0.000481   0.224237   0.006223   0.570441   0.015929
    H3_ s       0.000492   0.000062   0.027461   0.505645   0.000130   0.229650
 
   ao class       7Bu        8Bu        9Bu       10Bu       11Bu       12Bu 
    C1_ s       0.003534   0.005156   0.002606   0.003692   0.003115   0.002072
    C2_ s       0.005693   0.006162   0.003920   0.003246   0.002355   0.003675
    H1_ s       0.001724   0.000234   0.001296   0.000509   0.001308   0.000301
    H2_ s       0.000628   0.000462   0.000223   0.001667   0.001422   0.000242
    H3_ s       0.002643   0.000376   0.002478   0.000202   0.000527   0.000377
 
   ao class      13Bu       14Bu       15Bu       16Bu       17Bu       18Bu 
    C1_ s       0.002365   0.002638   0.000783   0.000477   0.000395   0.000210
    C2_ s       0.001823   0.000467   0.000965   0.000723   0.000382   0.000477
    H1_ s       0.000145   0.000538   0.000124   0.000071   0.000230   0.000018
    H2_ s       0.000199   0.000317   0.000228   0.000037   0.000122   0.000103
    H3_ s       0.000624   0.000011   0.000204   0.000391   0.000017   0.000157
 
   ao class      19Bu       20Bu       21Bu       22Bu       23Bu       24Bu 
    C1_ s       0.000414   0.000373   0.000232   0.000090   0.000114   0.000247
    C2_ s      -0.000026   0.000190   0.000258   0.000243   0.000189   0.000038
    H1_ s       0.000285   0.000048   0.000052   0.000010   0.000014   0.000093
    H2_ s       0.000247   0.000031   0.000064   0.000074   0.000045   0.000053
    H3_ s       0.000003   0.000084   0.000057   0.000129   0.000106   0.000002
 
   ao class      25Bu       26Bu       27Bu       28Bu       29Bu       30Bu 
    C1_ s       0.000191   0.000178   0.000133   0.000096   0.000036   0.000062
    C2_ s       0.000142   0.000065   0.000064   0.000060   0.000086   0.000072
    H1_ s       0.000012   0.000030   0.000002   0.000013   0.000027  -0.000004
    H2_ s      -0.000003   0.000001   0.000020   0.000007   0.000018   0.000006
    H3_ s       0.000050   0.000003   0.000017   0.000007   0.000003   0.000000
 
   ao class      31Bu       32Bu       33Bu       34Bu       35Bu       36Bu 
    C1_ s       0.000005   0.000027   0.000040  -0.000004   0.000007   0.000027
    C2_ s       0.000109   0.000035   0.000013   0.000046   0.000019   0.000017
    H1_ s       0.000002   0.000015   0.000007   0.000018   0.000029   0.000006
    H2_ s       0.000005   0.000005   0.000015   0.000012   0.000012   0.000017
    H3_ s       0.000001   0.000028   0.000011   0.000009   0.000005   0.000002
 
   ao class      37Bu       38Bu       39Bu       40Bu       41Bu       42Bu 
    C1_ s       0.000002   0.000017   0.000009   0.000011   0.000006   0.000004
    C2_ s       0.000011   0.000020   0.000015   0.000009   0.000013   0.000003
    H1_ s       0.000008   0.000010   0.000017   0.000005   0.000009   0.000019
    H2_ s       0.000010   0.000005   0.000012   0.000009   0.000019   0.000008
    H3_ s       0.000032   0.000009   0.000006   0.000024   0.000004   0.000012
 
   ao class      43Bu       44Bu       45Bu       46Bu       47Bu       48Bu 
    C1_ s       0.000004   0.000008   0.000004   0.000001   0.000003   0.000003
    C2_ s       0.000013   0.000007   0.000007   0.000001   0.000005   0.000002
    H1_ s       0.000009   0.000002   0.000004   0.000010   0.000003   0.000003
    H2_ s       0.000003   0.000015   0.000006   0.000006   0.000004   0.000003
    H3_ s       0.000006   0.000001   0.000006   0.000004   0.000003   0.000005
 
   ao class      49Bu       50Bu       51Bu       52Bu       53Bu 
    C1_ s      -0.000001   0.000001   0.000002   0.000001   0.000002
    C2_ s       0.000008   0.000000  -0.000001   0.000000   0.000001
    H1_ s       0.000001   0.000002   0.000003   0.000002   0.000000
    H2_ s       0.000001   0.000002   0.000003   0.000001   0.000001
    H3_ s       0.000005   0.000005   0.000000   0.000001   0.000001

                        Au  partial gross atomic populations
   ao class       1Au        2Au        3Au        4Au        5Au        6Au 
    C1_ s       0.683186   0.064749   0.002628   0.002172   0.001424   0.001042
    C2_ s       1.219906   0.035536   0.003211   0.002551   0.001248   0.001477
    H1_ s       0.006995   0.000558   0.000287   0.000048   0.000137   0.000115
    H2_ s       0.005463   0.000641   0.000018   0.000203   0.000291   0.000000
    H3_ s       0.013222   0.000361   0.000017   0.000377   0.000194   0.000102
 
   ao class       7Au        8Au        9Au       10Au       11Au       12Au 
    C1_ s       0.000975   0.000310   0.000052   0.000107   0.000052   0.000106
    C2_ s       0.000731   0.000276   0.000062   0.000032   0.000104   0.000153
    H1_ s       0.000142   0.000025   0.000005   0.000301   0.000003   0.000004
    H2_ s       0.000037   0.000005   0.000270   0.000001   0.000087   0.000000
    H3_ s       0.000107   0.000078   0.000130   0.000010   0.000087   0.000002
 
   ao class      13Au       14Au       15Au       16Au       17Au       18Au 
    C1_ s       0.000138   0.000086   0.000084   0.000033   0.000008   0.000001
    C2_ s       0.000069   0.000083   0.000020   0.000044   0.000035   0.000001
    H1_ s       0.000004   0.000000   0.000001   0.000002   0.000006   0.000010
    H2_ s       0.000000  -0.000001   0.000003   0.000004   0.000001   0.000012
    H3_ s       0.000001   0.000001   0.000001   0.000002   0.000016   0.000010
 
   ao class      19Au       20Au 
    C1_ s       0.000001   0.000001
    C2_ s       0.000002   0.000003
    H1_ s       0.000016   0.000000
    H2_ s       0.000006   0.000009
    H3_ s       0.000002   0.000010

                        Bg  partial gross atomic populations
   ao class       1Bg        2Bg        3Bg        4Bg        5Bg        6Bg 
    C1_ s       1.219785   0.018444   0.002830   0.003419   0.001630   0.000802
    C2_ s       0.632715   0.034960   0.002615   0.000336   0.001193   0.000994
    H1_ s       0.011996   0.000225   0.000008   0.000538   0.000059   0.000004
    H2_ s       0.013694   0.000173   0.000278   0.000053   0.000086   0.000187
    H3_ s       0.006319   0.000498   0.000296  -0.000003   0.000207   0.000171
 
   ao class       7Bg        8Bg        9Bg       10Bg       11Bg       12Bg 
    C1_ s       0.000057   0.000173   0.000103   0.000104   0.000124   0.000108
    C2_ s       0.000696   0.000290   0.000132   0.000063   0.000042   0.000147
    H1_ s       0.000169   0.000006   0.000018   0.000169   0.000051   0.000001
    H2_ s      -0.000001   0.000058   0.000099   0.000099   0.000066   0.000001
    H3_ s       0.000012   0.000109   0.000212   0.000014   0.000048   0.000004
 
   ao class      13Bg       14Bg       15Bg       16Bg       17Bg       18Bg 
    C1_ s       0.000135   0.000059   0.000047   0.000049   0.000011   0.000000
    C2_ s       0.000055   0.000106   0.000063   0.000025   0.000036   0.000000
    H1_ s       0.000005   0.000001   0.000001   0.000002   0.000012   0.000000
    H2_ s       0.000015   0.000001   0.000002   0.000001   0.000005   0.000014
    H3_ s       0.000000   0.000006   0.000001   0.000004   0.000001   0.000017
 
   ao class      19Bg       20Bg 
    C1_ s       0.000001   0.000002
    C2_ s       0.000001   0.000001
    H1_ s       0.000020   0.000001
    H2_ s       0.000001   0.000010
    H3_ s       0.000001   0.000008


                        gross atomic populations
     ao           C1_        C2_        H1_        H2_        H3_
      s        12.738529  12.100472   1.720068   1.710281   1.730650
    total      12.738529  12.100472   1.720068   1.710281   1.730650
 

 Total number of electrons:   30.00000000

