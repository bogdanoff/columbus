1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      09/20/00    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

================================================================================
four external integ     .25 MB location: local disk    
three external inte     .25 MB location: local disk    
four external integ     .25 MB location: local disk    
three external inte     .25 MB location: local disk    
diagonal integrals      .12 MB location: local disk    
off-diagonal integr     .22 MB location: local disk    
 nsubmx= 16  lenci= 116831
global arrays:      3855423   (   29.41 MB)
vdisk:                    0   (     .00 MB)
drt:                1954225   (    7.45 MB)
================================================================================
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core           11999999 DP per process
 CIUDG version 5.9.3 (05-Dec-2002)
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
      lvlprt=0, nbkitr=1, niter=2000, ivmode=0,
      nvcimx=16, nroot=10, nunitv=10,
      davcor=0,
      ctol=0.01,
      csfprn=10,
      rtol=0.001,0.001,0.001,0.001,0.001,0.001,0.001,
           0.001,0.001,0.001
  
  &end
  
 ------------------------------------------------------------------------

 ** list of control variables **
 nrfitr =   30      nvrfmx =   16      nvrfmn =   10
 lvlprt =    0      nroot  =   10      noldv  =    0      noldhv =    0
 nunitv =   10      ntype  =    0      nbkitr =    1      niter  = 2000
 ivmode =    0      vout   =    0      istrt  =    0      iortls =    0
 nvbkmx =   16      ibktv  =   -1      ibkthv =   -1      frcsub =    0
 nvcimx =   16      icitv  =   -1      icithv =   -1      maxseg =    4
 iden   =    1      itran  =    0      froot  =    0      rtmode =    0
 ftcalc =    1      lrtshift=1.0000    ncouple=    1      skipso  =   F
 ncorel =    0      csfprn  =  10      ctol   = 1.00E-02  davcor  =   0


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-04    1.000E-03
    2        1.000E-04    1.000E-03
    3        1.000E-04    1.000E-03
    4        1.000E-04    1.000E-03
    5        1.000E-04    1.000E-03
    6        1.000E-04    1.000E-03
    7        1.000E-04    1.000E-03
    8        1.000E-04    1.000E-03
    9        1.000E-04    1.000E-03
   10        1.000E-04    1.000E-03
 
 units and filenames:
    1: ( 6)    ciudgls                                                     
    2: ( 5)    ciudgin                                                     
    3: ( 7)    ciudgsm                                                     
    4: ( 4)    cihdiag                                                     
    5: ( 3)    cihpseu                                                     
    6: (10)    cihvfl                                                      
    7: (11)    civfl                                                       
    8: (12)    diagint                                                     
    9: (13)    ofdgint                                                     
   10: (17)    cidrtfl                                                     
   11: (20)    ciftdfl                                                     
   12: (21)    ciftofl                                                     
   13: (24)    ciftifl                                                     
   14: ( 8)    ciflind                                                     
   15: (15)    civin                                                       
   16: (16)    civout                                                      
   18: (18)    d1fl                                                        
   20: (53)    cifvfl                                                      
   21: (25)    ciscr4                                                      
   22: (41)    ciftotd                                                     
   23: (42)    flacpfd                                                     
   26: (26)    ciscr5                                                      
   27: (47)    restart                                                     
   28: (48)    civout                                                      
   29: (49)    cirefv                                                      
   31: (31)    fil4w                                                       
   32: (32)    fil4x                                                       
   33: (33)    fil3w                                                       
   34: (34)    fil3x                                                       
   36: (36)    aoints                                                      
   37: (37)    aoints2                                                     
   38: (38)    drtfil                                                      
   39: (50)    cisrtif                                                     
   42: (50)    mocoef                                                      
   45: (52)    nocoef_ci                                                   
   46: (53)    civfl_restart                                               
 ------------------------------------------------------------------------

 workspace allocation information: lcore=  11999999 mem1= 809998120 ifirst=    586376
 bummer (warning):spinorbit calculation: setting iden=0         0

 integral file titles:
  Eu(Eu+3/E+2 avg)  spherical symmetry as D2h  Ermler core,SO. srb bs c          
 aoints SIFS file created by argos.      zam403          Wed Aug 20 13:18:32 2003
 Eu^2 pvdz  5s^2 5p^6 froze. 4f^7 cas. all sd.                                   
 mo coefficients generated by scfpq                                              
 SIFS file created by program tran.      zam403          Wed Aug 20 13:18:51 2003

 core energy values from the integral file:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -6.626372896121E+01, ietype=    6,   fcore energy of type: H1(*)   

 total core repulsion energy = -6.626372896121E+01

 drt header information:
 Eu^2 pvdz  5s^2 5p^6 froze. 4f^7 cas. all sd.                                   
 spnorb, spnodd, lxyzir,hmult T T 4 3 2 9
 nmot  =    59 niot  =     8 nfct  =     4 nfvt  =    38
 nrow  =   106 nsym  =     8 ssym  =     5 lenbuf=  1600
 nwalk,xbar:      34892     6864    12012    12012     4004
 nvalwt,nvalw:    26741     1716     9009    12012     4004
 ncsft:          116831
 total number of valid internal walks:   26741
 nvalz,nvaly,nvalx,nvalw =     1716    9009   12012    4004

 cisrt info file parameters:
 file number  12 blocksize   4095
 mxbld   4095
 nd4ext,nd2ext,nd0ext   110   140    56
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int      357      498      804      198      150       18       15       18
 minbl4,minbl3,maxbl2    45    45    48
 maxbuf 32767
 number of external orbitals per symmetry block:   4   1   1   1   0   1   1   1
 nmsym   8 number of internal orbitals   7

 formula file title:
  Eu(Eu+3/E+2 avg)  spherical symmetry as D2h  Ermler core,SO. srb bs c          
 aoints SIFS file created by argos.      zam403          Wed Aug 20 13:18:32 2003
 Eu^2 pvdz  5s^2 5p^6 froze. 4f^7 cas. all sd.                                   
 mo coefficients generated by scfpq                                              
 SIFS file created by program tran.      zam403          Wed Aug 20 13:18:51 2003
 Eu^2 pvdz  5s^2 5p^6 froze. 4f^7 cas. all sd.                                   
 file nmb.  24 block size     0
 pthz,pthy,pthx,pthw:  6864 12012 12012  4004 total internal walks:   34892
 maxlp3,n3xtlp,n2xtlp,n1xtlp,n0xtlp,n2lp,n1lp,n0lp     0     0     0     0
 orbsym(*)= 5 6 6 7 7 8 8

 setref:     1716 references kept,
             5148 references were marked as invalid, out of
             6864 total.
 limcnvrt: found 1716  valid internal walksout of  6864  walks (skipping trailing invalids)
  ... adding  617  segmentation marks segtype= 1
 limcnvrt: found 9009  valid internal walksout of  12012  walks (skipping trailing invalids)
  ... adding  2940  segmentation marks segtype= 2
 limcnvrt: found 12012  valid internal walksout of  12012  walks (skipping trailing invalids)
  ... adding  3430  segmentation marks segtype= 3
 limcnvrt: found 4004  valid internal walksout of  4004  walks (skipping trailing invalids)
  ... adding  1470  segmentation marks segtype= 4

 number of external paths / symmetry
 vertex x       6       6       6       6       3       6       6       6
 vertex w      16       6       6       6       3       6       6       6



                   segmentation summary for type diagonal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1        6861|      1716|         0|      1716|         0|         1|
 -------------------------------------------------------------------------------
  Y 2       12011|      9009|      1716|      9009|      1716|         2|
 -------------------------------------------------------------------------------
  X 3       12012|     72072|     10725|     12012|     10725|         6|
 -------------------------------------------------------------------------------
  W 4        4004|     34034|     82797|      4004|     22737|        10|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>    116831


 297 dimension of the ci-matrix ->>>    116831


 297 dimension of the ci-matrix ->>>         7

                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1   12012    6861      72072       1716   12012    1716
     2  4   1    25      two-ext wz   2X  4 1    4004    6861      34034       1716    4004    1716
     3  4   3    26      two-ext wx   2X  4 3    4004   12012      34034      72072    4004   12012
     4  2   1    11      one-ext yz   1X  2 1   12011    6861       9009       1716    9009    1716
     5  3   2    15      1ex3ex  yx   3X  3 2   12012   12011      72072       9009   12012    9009
     6  4   2    16      1ex3ex  yw   3X  4 2    4004   12011      34034       9009    4004    9009
     7  1   1     1      allint zz    OX  1 1    6861    6861       1716       1716    1716    1716
     8  2   2     5      0ex2ex yy    OX  2 2   12011   12011       9009       9009    9009    9009
     9  3   3     6      0ex2ex xx    OX  3 3   12012   12012      72072      72072   12012   12012
    10  4   4     7      0ex2ex ww    OX  4 4    4004    4004      34034      34034    4004    4004
    11  1   1    75      dg-024ext z  DG  1 1    6861    6861       1716       1716    1716    1716
    12  2   2    45      4exdg024 y   DG  2 2   12011   12011       9009       9009    9009    9009
    13  3   3    46      4exdg024 x   DG  3 3   12012   12012      72072      72072   12012   12012
    14  4   4    47      4exdg024 w   DG  4 4    4004    4004      34034      34034    4004    4004
    15  1   1    81      so-0ext zz   SO  1 1    6861    6861       1716       1716    1716    1716
    16  2   2    82      so-0ext yy   SO  2 2   12011   12011       9009       9009    9009    9009
    17  2   2   101      so-2ext yy   SO  2 2   12011   12011       9009       9009    9009    9009
    18  3   3    83      so-0ext xx   SO  3 3   12012   12012      72072      72072   12012   12012
    19  3   3   102      so-2ext xx   SO  3 3   12012   12012      72072      72072   12012   12012
    20  4   4    84      so-0ext ww   SO  4 4    4004    4004      34034      34034    4004    4004
    21  2   1    91      so-1ext zy   SO  2 1   12011    6861       9009       1716    9009    1716
    22  3   2    93      so-1ext yx   SO  3 2   12012   12011      72072       9009   12012    9009
    23  4   2    94      so-1ext yw   SO  4 2    4004   12011      34034       9009    4004    9009
    24  4   3   103      so-2ext wx   SO  4 3    4004   12012      34034      72072    4004   12012
----------------------------------------------------------------------------------------------------
 DIAGEL COUNTS: DG0X,DG2X,DG4X= 339878 111419 25025
 diagonal elements written to file   4

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 0) unit vectors formed using lowest hdiag elements.            

     10 vectors will be written to unit 11 beginning with logical record   1
         initial trial vectors are constructed from only  z  walks.
 stdiag: initial vector  1: energy of csf 980 is      -93.3280834276
 stdiag: initial vector  2: energy of csf 981 is      -93.3280834276
 stdiag: initial vector  3: energy of csf 982 is      -93.3280834276
 stdiag: initial vector  4: energy of csf 983 is      -93.3280834276
 stdiag: initial vector  5: energy of csf 964 is      -93.1190310851
 stdiag: initial vector  6: energy of csf 965 is      -93.1190310851
 stdiag: initial vector  7: energy of csf 966 is      -93.1190310851
 stdiag: initial vector  8: energy of csf 782 is      -93.1189998955
 stdiag: initial vector  9: energy of csf 783 is      -93.1189998955
 stdiag: initial vector 10: energy of csf 784 is      -93.1189998955
 genvec: trial vector  1 is unit matrix column   980
 genvec: trial vector  2 is unit matrix column   981
 genvec: trial vector  3 is unit matrix column   982
 genvec: trial vector  4 is unit matrix column   983
 genvec: trial vector  5 is unit matrix column   964
 genvec: trial vector  6 is unit matrix column   965
 genvec: trial vector  7 is unit matrix column   966
 genvec: trial vector  8 is unit matrix column   782
 genvec: trial vector  9 is unit matrix column   783
 genvec: trial vector 10 is unit matrix column   784
    ---------end of vector generation---------


         vector  1 from unit 11 written to unit 49 filename cirefv                                                      

         vector  2 from unit 11 written to unit 49 filename cirefv                                                      

         vector  3 from unit 11 written to unit 49 filename cirefv                                                      

         vector  4 from unit 11 written to unit 49 filename cirefv                                                      

         vector  5 from unit 11 written to unit 49 filename cirefv                                                      

         vector  6 from unit 11 written to unit 49 filename cirefv                                                      

         vector  7 from unit 11 written to unit 49 filename cirefv                                                      

         vector  8 from unit 11 written to unit 49 filename cirefv                                                      

         vector  9 from unit 11 written to unit 49 filename cirefv                                                      

         vector 10 from unit 11 written to unit 49 filename cirefv                                                      

 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=  1716)...
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:            116831
 number of initial trial vectors:                        10
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:              16
 number of roots to converge:                            10
 number of iterations:                                    1
 residual norm convergence criteria:                .000100   .000100   .000100   .000100   .000100   .000100   .000100   .000100
                                                    .000100   .000100

          starting bk iteration   1

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:         0 xx:         0 ww:         0
One-external counts: yz :    263356 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:     44665 wz:     23975 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :     82216 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :    195563 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:         0    task #   4:    181187
task #   5:         0    task #   6:         0    task #   7:     35392    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:      9956    task #  12:     12056
task #  13:     14446    task #  14:      5083    task #  15:     77631    task #  16:         0
task #  17:         0    task #  18:         0    task #  19:         0    task #  20:         0
task #  21:     94274    task #  22:         0    task #  23:         0    task #  24:         0
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:         0 xx:         0 ww:         0
One-external counts: yz :    263356 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:     44665 wz:     23975 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :     82216 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :    195563 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:         0    task #   4:    181187
task #   5:         0    task #   6:         0    task #   7:     35392    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:      9956    task #  12:     12056
task #  13:     14446    task #  14:      5083    task #  15:     77631    task #  16:         0
task #  17:         0    task #  18:         0    task #  19:         0    task #  20:         0
task #  21:     94274    task #  22:         0    task #  23:         0    task #  24:         0
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:         0 xx:         0 ww:         0
One-external counts: yz :    263356 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:     44665 wz:     23975 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :     82216 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :    195563 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:         0    task #   4:    181187
task #   5:         0    task #   6:         0    task #   7:     35392    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:      9956    task #  12:     12056
task #  13:     14446    task #  14:      5083    task #  15:     77631    task #  16:         0
task #  17:         0    task #  18:         0    task #  19:         0    task #  20:         0
task #  21:     94274    task #  22:         0    task #  23:         0    task #  24:         0
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:         0 xx:         0 ww:         0
One-external counts: yz :    263356 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:     44665 wz:     23975 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :     82216 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :    195563 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:         0    task #   4:    181187
task #   5:         0    task #   6:         0    task #   7:     35392    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:      9956    task #  12:     12056
task #  13:     14446    task #  14:      5083    task #  15:     77631    task #  16:         0
task #  17:         0    task #  18:         0    task #  19:         0    task #  20:         0
task #  21:     94274    task #  22:         0    task #  23:         0    task #  24:         0
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:         0 xx:         0 ww:         0
One-external counts: yz :    263356 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:     44665 wz:     23975 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :     82216 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :    195563 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:         0    task #   4:    181187
task #   5:         0    task #   6:         0    task #   7:     35392    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:      9956    task #  12:     12056
task #  13:     14446    task #  14:      5083    task #  15:     77631    task #  16:         0
task #  17:         0    task #  18:         0    task #  19:         0    task #  20:         0
task #  21:     94274    task #  22:         0    task #  23:         0    task #  24:         0
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:         0 xx:         0 ww:         0
One-external counts: yz :    263356 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:     44665 wz:     23975 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :     82216 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :    195563 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:         0    task #   4:    181187
task #   5:         0    task #   6:         0    task #   7:     35392    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:      9956    task #  12:     12056
task #  13:     14446    task #  14:      5083    task #  15:     77631    task #  16:         0
task #  17:         0    task #  18:         0    task #  19:         0    task #  20:         0
task #  21:     94274    task #  22:         0    task #  23:         0    task #  24:         0
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:         0 xx:         0 ww:         0
One-external counts: yz :    263356 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:     44665 wz:     23975 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :     82216 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :    195563 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:         0    task #   4:    181187
task #   5:         0    task #   6:         0    task #   7:     35392    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:      9956    task #  12:     12056
task #  13:     14446    task #  14:      5083    task #  15:     77631    task #  16:         0
task #  17:         0    task #  18:         0    task #  19:         0    task #  20:         0
task #  21:     94274    task #  22:         0    task #  23:         0    task #  24:         0
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:         0 xx:         0 ww:         0
One-external counts: yz :    263356 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:     44665 wz:     23975 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :     82216 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :    195563 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:         0    task #   4:    181187
task #   5:         0    task #   6:         0    task #   7:     35392    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:      9956    task #  12:     12056
task #  13:     14446    task #  14:      5083    task #  15:     77631    task #  16:         0
task #  17:         0    task #  18:         0    task #  19:         0    task #  20:         0
task #  21:     94274    task #  22:         0    task #  23:         0    task #  24:         0
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:         0 xx:         0 ww:         0
One-external counts: yz :    263356 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:     44665 wz:     23975 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :     82216 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :    195563 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:         0    task #   4:    181187
task #   5:         0    task #   6:         0    task #   7:     35392    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:      9956    task #  12:     12056
task #  13:     14446    task #  14:      5083    task #  15:     77631    task #  16:         0
task #  17:         0    task #  18:         0    task #  19:         0    task #  20:         0
task #  21:     94274    task #  22:         0    task #  23:         0    task #  24:         0
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:         0 xx:         0 ww:         0
One-external counts: yz :    263356 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:     44665 wz:     23975 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :     82216 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :    195563 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:         0    task #   4:    181187
task #   5:         0    task #   6:         0    task #   7:     35392    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:      9956    task #  12:     12056
task #  13:     14446    task #  14:      5083    task #  15:     77631    task #  16:         0
task #  17:         0    task #  18:         0    task #  19:         0    task #  20:         0
task #  21:     94274    task #  22:         0    task #  23:         0    task #  24:         0

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1      .00000000      .49965323     -.86566403      .00000000      .01797003      .01303774      .00000000      .00000000
 ref:   2      .37468834     -.48378716     -.27939186     -.73950997      .00579980     -.01262374     -.00988011     -.01166322
 ref:   3     -.55855240      .43271239      .24989568     -.66143783     -.00518750      .01129101      .01472840      .01738650
 ref:   4      .73889537      .57242469      .33058091     -.12500000     -.00686242      .01493661     -.01948384     -.02300018
 ref:   5     -.00010435     -.00020226      .00058494      .00000000      .56434926      .25616987     -.38316034      .32122911
 ref:   6      .00018075     -.00011678      .00033772      .00000000      .32582720      .14789974      .66365318     -.55638514
 ref:   7      .00000000      .00052224      .00030206      .00000000      .29142870     -.66142775      .00000000      .00000000
 ref:   8      .00000000      .01520016     -.02636042      .00000000     -.63900007     -.28116901      .00000000      .00000000
 ref:   9     -.02496083      .02687034      .00931982      .00000000      .22592064     -.49704128     -.39309276     -.46888702
 ref:  10      .03222429      .02081368      .00721910      .00000000      .17499738     -.38500652      .50748057      .60533055

                ci   9         ci  10
 ref:   1      .01329159      .01740325
 ref:   2     -.01286953      .00561687
 ref:   3      .01151086     -.00502388
 ref:   4      .01522743     -.00664597
 ref:   5     -.24367388     -.55363307
 ref:   6     -.14068518     -.31964020
 ref:   7      .62916324     -.28589489
 ref:   8     -.29560043     -.65139648
 ref:   9     -.52255266      .23030343
 ref:  10     -.40476755      .17839227

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -93.3284319580  2.7065E+01  1.5189E-02  8.4981E-02  1.0000E-04
 mr-sdci #  1  2    -93.3283740109  2.7065E+01  0.0000E+00  8.5029E-02  1.0000E-04
 mr-sdci #  1  3    -93.3282579477  2.7065E+01  0.0000E+00  8.5124E-02  1.0000E-04
 mr-sdci #  1  4    -93.3280834276  2.7064E+01  0.0000E+00  8.5264E-02  1.0000E-04
 mr-sdci #  1  5    -93.1242879892  2.6861E+01  0.0000E+00  9.7788E-02  1.0000E-04
 mr-sdci #  1  6    -93.1220885942  2.6858E+01  0.0000E+00  9.7912E-02  1.0000E-04
 mr-sdci #  1  7    -93.1199292278  2.6856E+01  0.0000E+00  9.7887E-02  1.0000E-04
 mr-sdci #  1  8    -93.1177532225  2.6854E+01  0.0000E+00  9.9840E-02  1.0000E-04
 mr-sdci #  1  9    -93.1156518031  2.6852E+01  0.0000E+00  1.0021E-01  1.0000E-04
 mr-sdci #  1 10    -93.1135684714  2.6850E+01  0.0000E+00  1.0111E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .33    .13    .00    .32
    2   25    0    .19    .07    .00    .18
    3   26    0    .00    .00    .00    .00
    4   11    0   2.18   1.23    .00   2.17
    5   15    0    .00    .00    .00    .00
    6   16    0    .00    .00    .00    .00
    7    1    0    .51    .28    .00    .51
    8    5    0    .00    .00    .00    .00
    9    6    0    .00    .00    .00    .00
   10    7    0    .00    .00    .00    .00
   11   75    0    .07    .05    .00    .07
   12   45    0    .21    .10    .00    .20
   13   46    0    .36    .08    .00    .35
   14   47    0    .15    .04    .00    .15
   15   81    0    .52    .00    .00    .52
   16   82    0    .00    .00    .00    .00
   17  101    0    .00    .00    .00    .00
   18   83    0    .00    .00    .00    .00
   19  102    0    .00    .00    .00    .00
   20   84    0    .00    .00    .00    .00
   21   91    0    .61    .00    .00    .61
   22   93    0    .00    .00    .00    .00
   23   94    0    .00    .00    .00    .00
   24  103    0    .00    .00    .00    .00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .150000
time for cinew                          .570000
time for eigenvalue solver              .000000
time for vector access                  .000000
================================================================
time spent in mult:                     5.1300s 
time spent in multnx:                   5.0800s 
integral transfer time:                  .0000s 
time spent for loop construction:       1.9800s 
time for vector access in mult:          .0000s 
total time per CI iteration:           51.7700s 

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -93.3284319580  2.7065E+01  1.5189E-02  8.4981E-02  1.0000E-04
 mr-sdci #  1  2    -93.3283740109  2.7065E+01  0.0000E+00  8.5029E-02  1.0000E-04
 mr-sdci #  1  3    -93.3282579477  2.7065E+01  0.0000E+00  8.5124E-02  1.0000E-04
 mr-sdci #  1  4    -93.3280834276  2.7064E+01  0.0000E+00  8.5264E-02  1.0000E-04
 mr-sdci #  1  5    -93.1242879892  2.6861E+01  0.0000E+00  9.7788E-02  1.0000E-04
 mr-sdci #  1  6    -93.1220885942  2.6858E+01  0.0000E+00  9.7912E-02  1.0000E-04
 mr-sdci #  1  7    -93.1199292278  2.6856E+01  0.0000E+00  9.7887E-02  1.0000E-04
 mr-sdci #  1  8    -93.1177532225  2.6854E+01  0.0000E+00  9.9840E-02  1.0000E-04
 mr-sdci #  1  9    -93.1156518031  2.6852E+01  0.0000E+00  1.0021E-01  1.0000E-04
 mr-sdci #  1 10    -93.1135684714  2.6850E+01  0.0000E+00  1.0111E-01  1.0000E-04
 
 expansion vectors are not transformed.
 matrix-vector products are not transformed.

   11 expansion eigenvectors written to unit nvfile (= 11)
   10 matrix-vector products written to unit nhvfil (= 10)

 from bk iterations: iconv=   1

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:            116831
 number of initial trial vectors:                        11
 number of initial matrix-vector products:               10
 maximum dimension of the subspace vectors:              16
 number of roots to converge:                            10
 number of iterations:                                 2000
 residual norm convergence criteria:                .001000   .001000   .001000   .001000   .001000   .001000   .001000   .001000
                                                    .001000   .001000

          starting ci iteration   1

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1      .00025858      .49965250     -.86566441      .00000000      .01797002     -.01303775      .00000394     -.00000421
 ref:   2     -.36952951     -.48419130     -.27973769      .73950997      .00578704      .01259902     -.01169169      .01312284
 ref:   3      .55132201      .43331493      .25041172      .66143783     -.00516845     -.01125416      .01742775     -.01956103
 ref:   4     -.73115584      .57162727      .32989625      .12500000     -.00688772     -.01498540     -.02305002      .02587148
 ref:   5      .00019350     -.00020215      .00058504      .00000000      .56433909     -.25622052     -.37685425     -.32856419
 ref:   6     -.00034596     -.00011697      .00033755      .00000000      .32584382     -.14781422      .65277724      .56912881
 ref:   7     -.00005496      .00052224      .00030206      .00000000      .29143015      .66142805     -.00008579     -.00009351
 ref:   8      .00000822      .01520014     -.02636043      .00000000     -.63899959      .28117006     -.00003132      .00003458
 ref:   9      .02688446      .02689725      .00934284      .00000000      .22587457      .49688819     -.40213355      .46125597
 ref:  10     -.03478112      .02077892      .00718929      .00000000      .17505797      .38520189      .51871368     -.59500312

                ci   9         ci  10         ci  11
 ref:   1      .01329158      .01740326      .00004087
 ref:   2     -.01288593      .00562731     -.05638891
 ref:   3      .01153531     -.00503948      .08413080
 ref:   4      .01519505     -.00662525     -.11157706
 ref:   5     -.24371288     -.55362657      .00035191
 ref:   6     -.14061502     -.31965259     -.00065446
 ref:   7      .62916294     -.28589325     -.00024067
 ref:   8     -.29559913     -.65139708      .00000040
 ref:   9     -.52237666      .23024626      .01378777
 ref:  10     -.40499695      .17846447     -.01778602

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -93.3410190090  1.2587E-02  3.2221E-04  1.2142E-02  1.0000E-03
 mr-sdci #  1  2    -93.3283740110  6.7750E-11  0.0000E+00  8.5029E-02  1.0000E-03
 mr-sdci #  1  3    -93.3282579478  1.5099E-10  0.0000E+00  8.5124E-02  1.0000E-03
 mr-sdci #  1  4    -93.3280834276 -7.1054E-15  0.0000E+00  8.5264E-02  1.0000E-03
 mr-sdci #  1  5    -93.1242879913  2.0603E-09  0.0000E+00  9.7788E-02  1.0000E-03
 mr-sdci #  1  6    -93.1220886009  6.6662E-09  0.0000E+00  9.7912E-02  1.0000E-03
 mr-sdci #  1  7    -93.1199685633  3.9335E-05  0.0000E+00  9.8127E-02  1.0000E-03
 mr-sdci #  1  8    -93.1177981191  4.4897E-05  0.0000E+00  9.9802E-02  1.0000E-03
 mr-sdci #  1  9    -93.1156518104  7.2939E-09  0.0000E+00  1.0021E-01  1.0000E-03
 mr-sdci #  1 10    -93.1135684738  2.3752E-09  0.0000E+00  1.0111E-01  1.0000E-03
 mr-sdci #  1 11    -92.7928426811  2.6529E+01  0.0000E+00  1.8489E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .34    .13    .00    .33
    2   25    0    .19    .07    .00    .19
    3   26    0   5.15    .19    .00   5.15
    4   11    0   2.19   1.24    .00   2.19
    5   15    0   4.42   1.63    .00   4.42
    6   16    0   2.59    .91    .00   2.59
    7    1    0    .47    .25    .00    .47
    8    5    0   2.23    .81    .00   2.22
    9    6    0   8.29    .73    .00   8.28
   10    7    0   1.76    .21    .00   1.76
   11   75    0    .08    .05    .00    .08
   12   45    0    .21    .11    .00    .21
   13   46    0    .56    .07    .00    .56
   14   47    0    .22    .04    .00    .22
   15   81    0    .53    .00    .00    .53
   16   82    0   1.60    .00    .00   1.59
   17  101    0    .27    .00    .00    .27
   18   83    0   1.66    .00    .00   1.66
   19  102    0   4.13    .00    .00   4.12
   20   84    0    .43    .00    .00    .43
   21   91    0    .62    .00    .00    .62
   22   93    0   2.28    .00    .00   2.28
   23   94    0   1.17    .00    .00   1.17
   24  103    0   2.94    .00    .00   2.94
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .150000
time for cinew                          .660000
time for eigenvalue solver              .000000
time for vector access                  .000000
================================================================
time spent in mult:                    44.3300s 
time spent in multnx:                  44.2800s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.4400s 
time for vector access in mult:          .0000s 
total time per CI iteration:           45.1400s 

          starting ci iteration   2

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     -.00045930      .49763797      .86682392     -.00000076     -.00027238      .01796934     -.01303645      .00004285
 ref:   2      .36798250     -.48519565      .27890881     -.73951056      .03010925      .00620314      .01285985     -.00497360
 ref:   3     -.54938667      .43442811     -.24985093     -.66143750     -.04523360     -.00579336     -.01164423      .00742140
 ref:   4      .73007369      .57168709     -.32797176     -.12499825      .06122877     -.00604264     -.01446442     -.00984673
 ref:   5     -.00025632     -.00020251     -.00058404      .00000000      .07980246      .56607933     -.25405816      .49260545
 ref:   6      .00046041     -.00011307     -.00034047      .00000000     -.15141049      .32282040     -.15143301     -.85325083
 ref:   7      .00008065      .00052364     -.00030144      .00000000     -.01320699      .29121669      .66139124     -.00100898
 ref:   8     -.00001769      .01513869      .02639579     -.00000002      .00679693     -.63898924      .28111113     -.00021716
 ref:   9     -.02879175      .02688519     -.00925140      .00000007      .55181574      .23460705      .50355421     -.10022388
 ref:  10      .03732443      .02083954     -.00717852     -.00000001     -.72852675      .16351660      .37657765      .12545743

                ci   9         ci  10         ci  11         ci  12
 ref:   1     -.01329162      .01740325     -.00025686     -.00002161
 ref:   2      .01286199      .00562203      .03079303     -.04719801
 ref:   3     -.01149977     -.00503174     -.04635647      .07031822
 ref:   4     -.01524150     -.00663495      .06312885     -.09286011
 ref:   5      .24400913     -.55356513     -.00219327     -.00006626
 ref:   6      .14009661     -.31976115      .00427859      .00011123
 ref:   7     -.62915726     -.28588756      .00284631     -.00002300
 ref:   8      .29559619     -.65139845     -.00026580     -.00002285
 ref:   9      .52111403      .22978004     -.22548661     -.00264094
 ref:  10      .40663239      .17906442      .29266916      .00355952

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1    -93.3414330798  4.1407E-04  2.2314E-05  3.3131E-03  1.0000E-03
 mr-sdci #  2  2    -93.3283743286  3.1763E-07  0.0000E+00  8.5027E-02  1.0000E-03
 mr-sdci #  2  3    -93.3282581775  2.2965E-07  0.0000E+00  8.5122E-02  1.0000E-03
 mr-sdci #  2  4    -93.3280834276  2.9132E-13  0.0000E+00  8.5264E-02  1.0000E-03
 mr-sdci #  2  5    -93.1261767456  1.8888E-03  0.0000E+00  1.0518E-01  1.0000E-03
 mr-sdci #  2  6    -93.1242876545  2.1991E-03  0.0000E+00  9.7788E-02  1.0000E-03
 mr-sdci #  2  7    -93.1220883764  2.1198E-03  0.0000E+00  9.7912E-02  1.0000E-03
 mr-sdci #  2  8    -93.1188167190  1.0186E-03  0.0000E+00  9.5385E-02  1.0000E-03
 mr-sdci #  2  9    -93.1156518257  1.5278E-08  0.0000E+00  1.0021E-01  1.0000E-03
 mr-sdci #  2 10    -93.1135684781  4.3284E-09  0.0000E+00  1.0111E-01  1.0000E-03
 mr-sdci #  2 11    -93.0715508118  2.7871E-01  0.0000E+00  1.7731E-01  1.0000E-04
 mr-sdci #  2 12    -92.7110790932  2.6447E+01  0.0000E+00  1.0041E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .33    .12    .00    .33
    2   25    0    .20    .07    .00    .19
    3   26    0   5.11    .19    .00   5.11
    4   11    0   2.22   1.23    .00   2.21
    5   15    0   4.59   1.71    .00   4.58
    6   16    0   2.69    .94    .00   2.69
    7    1    0    .49    .27    .00    .49
    8    5    0   2.27    .84    .00   2.26
    9    6    0   8.29    .73    .00   8.29
   10    7    0   1.72    .20    .00   1.72
   11   75    0    .09    .05    .00    .08
   12   45    0    .20    .10    .00    .20
   13   46    0    .57    .08    .00    .57
   14   47    0    .23    .04    .00    .23
   15   81    0    .53    .00    .00    .53
   16   82    0   1.61    .00    .00   1.60
   17  101    0    .28    .00    .00    .27
   18   83    0   1.66    .00    .00   1.66
   19  102    0   4.14    .00    .00   4.13
   20   84    0    .43    .00    .00    .43
   21   91    0    .63    .00    .00    .63
   22   93    0   2.26    .00    .00   2.25
   23   94    0   1.16    .00    .00   1.16
   24  103    0   2.96    .00    .00   2.95
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .030000
time for cinew                          .530000
time for eigenvalue solver              .000000
time for vector access                  .000000
================================================================
time spent in mult:                    44.6600s 
time spent in multnx:                  44.5600s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.5700s 
time for vector access in mult:          .0100s 
total time per CI iteration:           45.2400s 

          starting ci iteration   3

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1      .00054159     -.49024755      .87102480      .00002228      .00058719      .01797012     -.01303730      .00005107
 ref:   2     -.36753320      .48776482      .27495727     -.73948311     -.03994051      .00589789      .01274690     -.00077859
 ref:   3      .54888928     -.43679937     -.24634473     -.66146118      .06070620     -.00533551     -.01147578      .00121840
 ref:   4     -.73007445     -.57406917     -.32278372     -.12503533     -.08484408     -.00665959     -.01468769     -.00183405
 ref:   5      .00026711      .00020022     -.00058357     -.00000004     -.01501934      .56481973     -.25499618      .49662109
 ref:   6     -.00048064      .00010519     -.00034545      .00000003      .02728911      .32504261     -.14986420     -.86003389
 ref:   7     -.00007993     -.00052594     -.00029677     -.00000003      .00265065      .29138243      .66142860     -.00253820
 ref:   8      .00002119     -.01491340      .02652378      .00000068     -.00075809     -.63901382      .28113611     -.00000583
 ref:   9      .02887261     -.02694845     -.00901056     -.00000136     -.23349473      .22810923      .50056438      .05504222
 ref:  10     -.03746621     -.02092055     -.00701791     -.00000086      .30645884      .17209789      .38045567     -.07371417

                ci   9         ci  10         ci  11         ci  12         ci  13
 ref:   1     -.01328997     -.01740552     -.00010803      .00012129      .00003603
 ref:   2      .01292456     -.00566757     -.00260201      .03522742     -.03644222
 ref:   3     -.01159125      .00509671      .00358036     -.05224695      .05441016
 ref:   4     -.01512734      .00656016     -.00359035      .06812564     -.07228343
 ref:   5      .24314180      .55428125      .05245413      .00003300     -.00003142
 ref:   6      .14155394      .31850817     -.09999846     -.00006735      .00004323
 ref:   7     -.62918192      .28587004     -.00168672      .00045820      .00015168
 ref:   8      .29557807      .65138330     -.00554239      .00004214     -.00000517
 ref:   9      .52503440     -.23574154     -.55359950     -.02004498     -.01008030
 ref:  10      .40154483     -.17120865      .73009246      .02532681      .01293327

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1    -93.3414565712  2.3491E-05  0.0000E+00  7.6404E-04  1.0000E-03
 mr-sdci #  3  2    -93.3283755413  1.2127E-06  1.5218E-02  8.5018E-02  1.0000E-03
 mr-sdci #  3  3    -93.3282589852  8.0774E-07  0.0000E+00  8.5117E-02  1.0000E-03
 mr-sdci #  3  4    -93.3280834277  7.8902E-11  0.0000E+00  8.5264E-02  1.0000E-03
 mr-sdci #  3  5    -93.1538633890  2.7687E-02  0.0000E+00  8.4712E-02  1.0000E-03
 mr-sdci #  3  6    -93.1242878841  2.2961E-07  0.0000E+00  9.7787E-02  1.0000E-03
 mr-sdci #  3  7    -93.1220884613  8.4921E-08  0.0000E+00  9.7912E-02  1.0000E-03
 mr-sdci #  3  8    -93.1190771316  2.6041E-04  0.0000E+00  9.5108E-02  1.0000E-03
 mr-sdci #  3  9    -93.1156519363  1.1063E-07  0.0000E+00  1.0021E-01  1.0000E-03
 mr-sdci #  3 10    -93.1135685618  8.3759E-08  0.0000E+00  1.0111E-01  1.0000E-03
 mr-sdci #  3 11    -93.1128041846  4.1253E-02  0.0000E+00  1.0421E-01  1.0000E-04
 mr-sdci #  3 12    -92.7760526076  6.4974E-02  0.0000E+00  1.7268E-01  1.0000E-04
 mr-sdci #  3 13    -92.6942707386  2.6431E+01  0.0000E+00  1.1065E-01  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .34    .12    .00    .34
    2   25    0    .20    .07    .00    .19
    3   26    0   5.13    .19    .00   5.13
    4   11    0   2.22   1.23    .00   2.21
    5   15    0   4.55   1.66    .00   4.54
    6   16    0   2.65    .94    .00   2.65
    7    1    0    .48    .28    .00    .47
    8    5    0   2.17    .81    .00   2.17
    9    6    0   8.27    .72    .00   8.27
   10    7    0   1.73    .20    .00   1.73
   11   75    0    .08    .05    .00    .08
   12   45    0    .20    .09    .00    .19
   13   46    0    .57    .08    .00    .56
   14   47    0    .24    .03    .00    .24
   15   81    0    .53    .00    .00    .53
   16   82    0   1.60    .00    .00   1.60
   17  101    0    .28    .00    .00    .27
   18   83    0   1.66    .00    .00   1.66
   19  102    0   4.12    .00    .00   4.12
   20   84    0    .43    .00    .00    .43
   21   91    0    .63    .00    .00    .63
   22   93    0   2.26    .00    .00   2.25
   23   94    0   1.15    .00    .00   1.15
   24  103    0   2.95    .01    .00   2.94
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .030000
time for cinew                         1.400000
time for eigenvalue solver              .000000
time for vector access                  .000000
================================================================
time spent in mult:                    44.4400s 
time spent in multnx:                  44.3500s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.4800s 
time for vector access in mult:          .0100s 
total time per CI iteration:           45.8800s 

          starting ci iteration   4

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1      .00038381     -.48465208     -.87093017      .00032918      .00228919      .01794446     -.01530669     -.00021633
 ref:   2      .36661324      .48215556     -.27505146     -.73978808     -.04162827      .00592286      .01495147      .00093929
 ref:   3     -.54806357     -.43296919      .24642897     -.66118782      .06221874     -.00535763     -.01342665     -.00136048
 ref:   4      .73115671     -.56609897      .32289449     -.12467621     -.08284607     -.00669049     -.01742878      .00163345
 ref:   5     -.00026789      .00040144      .00058353     -.00000016     -.01491744      .56477514     -.25429630     -.49613386
 ref:   6      .00048032      .00016495      .00034543     -.00000004      .02730052      .32502301     -.14830049      .86029992
 ref:   7      .00008187     -.00099820      .00029687      .00000029      .00242960      .29149038      .65793180      .00128068
 ref:   8      .00000963     -.01609706     -.02652091      .00000999     -.00112382     -.63894578      .28274110     -.00008318
 ref:   9     -.02881663     -.02925677      .00901576      .00001545     -.23404232      .22822163      .50214214     -.05530869
 ref:  10      .03750904     -.02233465      .00702194      .00001220      .30579384      .17220199      .38380700      .07372575

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14
 ref:   1      .01492380      .01739981     -.00064552     -.07418480     -.00044808      .00130005
 ref:   2     -.01451134      .00567302     -.00207757      .07239348      .03578265     -.03768718
 ref:   3      .01299251     -.00510147      .00311575     -.06416376     -.05273881      .05552051
 ref:   4      .01711709     -.00656728     -.00424047     -.08949983      .06743789     -.07078098
 ref:   5     -.24504641     -.55428632      .05204568      .00083787      .00003932     -.00004459
 ref:   6     -.14131261     -.31851990     -.10040498      .00016071     -.00006612      .00004057
 ref:   7      .63280135     -.28584792     -.00037180     -.00187641      .00044405      .00018125
 ref:   8     -.29393689     -.65140371     -.00693255     -.00826173     -.00002032      .00012615
 ref:   9     -.52014108      .23567698     -.55619262     -.01489384     -.02015867     -.00984069
 ref:  10     -.40202342      .17121891      .72818634     -.01162254      .02524033      .01311438

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1    -93.3414565729  1.7215E-09  0.0000E+00  7.6409E-04  1.0000E-03
 mr-sdci #  4  2    -93.3410012050  1.2626E-02  3.3282E-04  1.2301E-02  1.0000E-03
 mr-sdci #  4  3    -93.3282589852  4.4054E-12  0.0000E+00  8.5117E-02  1.0000E-03
 mr-sdci #  4  4    -93.3280834278  1.1694E-10  0.0000E+00  8.5264E-02  1.0000E-03
 mr-sdci #  4  5    -93.1538838577  2.0469E-05  0.0000E+00  8.4654E-02  1.0000E-03
 mr-sdci #  4  6    -93.1242878886  4.4504E-09  0.0000E+00  9.7787E-02  1.0000E-03
 mr-sdci #  4  7    -93.1221323315  4.3870E-05  0.0000E+00  9.8060E-02  1.0000E-03
 mr-sdci #  4  8    -93.1190774009  2.6922E-07  0.0000E+00  9.5108E-02  1.0000E-03
 mr-sdci #  4  9    -93.1156797876  2.7851E-05  0.0000E+00  1.0023E-01  1.0000E-03
 mr-sdci #  4 10    -93.1135685623  4.2717E-10  0.0000E+00  1.0111E-01  1.0000E-03
 mr-sdci #  4 11    -93.1128077248  3.5402E-06  0.0000E+00  1.0420E-01  1.0000E-04
 mr-sdci #  4 12    -92.7940357028  1.7983E-02  0.0000E+00  1.8613E-01  1.0000E-04
 mr-sdci #  4 13    -92.7760514767  8.1781E-02  0.0000E+00  1.7269E-01  1.0000E-04
 mr-sdci #  4 14    -92.6942301260  2.6431E+01  0.0000E+00  1.1054E-01  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .34    .13    .00    .33
    2   25    0    .20    .07    .00    .19
    3   26    0   5.12    .19    .00   5.12
    4   11    0   2.22   1.25    .00   2.21
    5   15    0   4.53   1.67    .00   4.52
    6   16    0   2.55    .89    .00   2.55
    7    1    0    .47    .26    .00    .47
    8    5    0   2.18    .78    .00   2.17
    9    6    0   8.23    .73    .00   8.23
   10    7    0   1.71    .20    .00   1.71
   11   75    0    .09    .05    .00    .08
   12   45    0    .19    .10    .00    .19
   13   46    0    .55    .08    .00    .55
   14   47    0    .21    .04    .00    .21
   15   81    0    .52    .00    .00    .51
   16   82    0   1.63    .00    .00   1.63
   17  101    0    .28    .00    .00    .28
   18   83    0   1.65    .00    .00   1.65
   19  102    0   4.02    .00    .00   4.01
   20   84    0    .43    .00    .00    .43
   21   91    0    .63    .00    .00    .63
   22   93    0   2.27    .00    .00   2.27
   23   94    0   1.18    .00    .00   1.18
   24  103    0   2.95    .00    .00   2.95
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .050000
time for cinew                          .750000
time for eigenvalue solver              .000000
time for vector access                  .000000
================================================================
time spent in mult:                    44.1500s 
time spent in multnx:                  44.0700s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.4400s 
time for vector access in mult:          .0000s 
total time per CI iteration:           44.9600s 

          starting ci iteration   5

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     -.00493521      .48339943      .87091571     -.00061730     -.00252692      .03607050      .01833027     -.00037403
 ref:   2     -.36207277     -.48398714      .27508869      .74003887      .04185976     -.03540772      .00554453      .00108900
 ref:   3      .54397012      .43732582     -.24642142      .66096233     -.06242950      .03193773     -.00501690     -.00149598
 ref:   4     -.73644061      .55775774     -.32290755      .12438057      .08256424      .04270471     -.00623945      .00143784
 ref:   5      .00027304     -.00053792     -.00058364     -.00000191      .01478947      .14471484      .56763567     -.48819481
 ref:   6     -.00047830     -.00021467     -.00034547     -.00000061     -.02735201      .07426376      .32657971      .86457842
 ref:   7     -.00009455      .00132805     -.00029660      .00000467     -.00211287     -.38440352      .28405929     -.01914659
 ref:   8     -.00017295      .01727647      .02652151      .00000045      .00181690     -.33327045     -.64372434     -.00116234
 ref:   9      .02851881      .03164346     -.00901467      .00000486      .23528079     -.60789863      .21961884     -.05741065
 ref:  10     -.03773432      .02365242     -.00702107      .00000421     -.30480228     -.47766585      .16551250      .07242578

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15
 ref:   1      .01539120      .01743335     -.00056335     -.04258234      .00101683     -.06153784      .00448926
 ref:   2     -.01488444      .00563894     -.00216789      .04131048     -.03633590      .05784782     -.04073439
 ref:   3      .01337430     -.00507093      .00319448     -.03743189      .05322760     -.04970356      .05816772
 ref:   4      .01819003     -.00653181     -.00414579     -.05123038     -.06674786     -.07879239     -.06679351
 ref:   5     -.32837139     -.55490345      .05013492      .00588340     -.00004395     -.00010426     -.00003220
 ref:   6     -.16589456     -.31895408     -.10164874     -.00100750      .00006536     -.00007845      .00004664
 ref:   7      .83083013     -.28416980      .00533394     -.01055529     -.00043380      .00029291      .00014994
 ref:   8     -.15923641     -.65283514     -.01234625     -.14777876      .00006455      .00151121     -.00002460
 ref:   9     -.27714750      .23294094     -.56630951     -.26340608      .02023827      .00221152     -.01009806
 ref:  10     -.22610297      .16948950      .72024376     -.21177914     -.02517794      .00279030      .01288352

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1    -93.3414565752  2.2765E-09  0.0000E+00  7.6338E-04  1.0000E-03
 mr-sdci #  5  2    -93.3414322709  4.3107E-04  2.3141E-05  3.3793E-03  1.0000E-03
 mr-sdci #  5  3    -93.3282589855  2.9652E-10  0.0000E+00  8.5117E-02  1.0000E-03
 mr-sdci #  5  4    -93.3280835272  9.9368E-08  0.0000E+00  8.5264E-02  1.0000E-03
 mr-sdci #  5  5    -93.1538846865  8.2883E-07  0.0000E+00  8.4657E-02  1.0000E-03
 mr-sdci #  5  6    -93.1269743365  2.6864E-03  0.0000E+00  1.0384E-01  1.0000E-03
 mr-sdci #  5  7    -93.1242876170  2.1553E-03  0.0000E+00  9.7788E-02  1.0000E-03
 mr-sdci #  5  8    -93.1190784233  1.0225E-06  0.0000E+00  9.5113E-02  1.0000E-03
 mr-sdci #  5  9    -93.1173617265  1.6819E-03  0.0000E+00  9.7594E-02  1.0000E-03
 mr-sdci #  5 10    -93.1135686209  5.8597E-08  0.0000E+00  1.0111E-01  1.0000E-03
 mr-sdci #  5 11    -93.1128090052  1.2804E-06  0.0000E+00  1.0420E-01  1.0000E-04
 mr-sdci #  5 12    -93.0763066154  2.8227E-01  0.0000E+00  1.7438E-01  1.0000E-04
 mr-sdci #  5 13    -92.7760523807  9.0398E-07  0.0000E+00  1.7271E-01  1.0000E-04
 mr-sdci #  5 14    -92.7111056285  1.6876E-02  0.0000E+00  1.0052E-01  1.0000E-04
 mr-sdci #  5 15    -92.6941719162  2.6430E+01  0.0000E+00  1.1030E-01  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .34    .12    .00    .34
    2   25    0    .19    .07    .00    .19
    3   26    0   5.17    .19    .00   5.16
    4   11    0   2.21   1.26    .00   2.21
    5   15    0   4.57   1.69    .00   4.56
    6   16    0   2.64    .93    .00   2.64
    7    1    0    .50    .29    .00    .49
    8    5    0   2.26    .83    .00   2.26
    9    6    0   8.28    .72    .00   8.27
   10    7    0   1.72    .20    .00   1.72
   11   75    0    .08    .06    .00    .08
   12   45    0    .21    .10    .00    .20
   13   46    0    .57    .08    .00    .57
   14   47    0    .24    .05    .00    .24
   15   81    0    .53    .00    .00    .53
   16   82    0   1.64    .00    .00   1.64
   17  101    0    .26    .00    .00    .25
   18   83    0   1.65    .00    .00   1.65
   19  102    0   4.13    .00    .00   4.13
   20   84    0    .43    .00    .00    .43
   21   91    0    .63    .00    .00    .62
   22   93    0   2.28    .00    .00   2.27
   23   94    0   1.19    .00    .00   1.19
   24  103    0   2.89    .00    .00   2.89
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .070000
time for cinew                          .790000
time for eigenvalue solver              .000000
time for vector access                  .000000
================================================================
time spent in mult:                    44.6100s 
time spent in multnx:                  44.5300s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.5900s 
time for vector access in mult:          .0100s 
total time per CI iteration:           45.4800s 

          starting ci iteration   6

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1      .26103429     -.40664032     -.87090656     -.00085080     -.04545059     -.03075065     -.01812086      .00255705
 ref:   2     -.56807818      .20559568     -.27518953      .74012927      .06722237     -.00206689     -.00575183     -.00208303
 ref:   3      .69469746     -.06701804      .24634759      .66087949     -.07625077      .02101191      .00520317      .00204075
 ref:   4     -.30969671     -.87020309      .32290262      .12427523     -.00316427     -.10535813      .00648343      .00367664
 ref:   5     -.00007745      .00061549      .00058381     -.00000415     -.01500526     -.03025800     -.56607558     -.29177257
 ref:   6     -.00052486     -.00007207      .00034558     -.00000216     -.02696676      .01372629     -.32573459     -.23512095
 ref:   7      .00068141     -.00121019      .00029610      .00001169      .06000506      .04896267     -.28815065      .83797245
 ref:   8      .00936330     -.01457863     -.02652174      .00000044      .11207949      .08364713      .64104672      .13502193
 ref:   9      .04124926     -.01086849      .00901435      .00001069      .33508474     -.03942379     -.22449956      .26002535
 ref:  10     -.01852166     -.04054622      .00702064      .00001110     -.01841395      .36981161     -.16926329      .16672708

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1     -.00025646      .01755264     -.00335355     -.00114752     -.02873711      .03492464     -.03553349     -.03241331
 ref:   2     -.00050711      .00548701      .00523295     -.00118449      .05658166     -.01319471      .00993593      .05833504
 ref:   3      .00096216     -.00493396     -.00484963      .00235004     -.06674740     -.00120596      .00619550     -.06796252
 ref:   4     -.00223141     -.00642849     -.00247768     -.00430830      .01992927      .08266894     -.09197750      .01399967
 ref:   5      .51540306     -.55792999      .09418958      .08653207      .00021302     -.00019659     -.00015141     -.00008907
 ref:   6     -.84721835     -.32171724      .10789301     -.06911605     -.00035286      .00034094      .00011443      .00006265
 ref:   7     -.05387359     -.27465683     -.34515908     -.10629006      .00050787      .00005355      .00016983     -.00009100
 ref:   8     -.00374896     -.66138343      .31666860      .11115889     -.00770375      .00960391      .00438796      .00420543
 ref:   9      .04721382      .21388137      .78877733     -.30814814     -.02987246      .00508464      .00129925      .01518636
 ref:  10     -.07777353      .16168402      .21693456      .85088888      .00916952      .02895084      .01470463     -.00381914

 trial vector basis is being transformed.  new dimension:  10

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  6  1    -93.3414565944  1.9240E-08  0.0000E+00  7.5396E-04  1.0000E-03
 mr-sdci #  6  2    -93.3414565307  2.4260E-05  0.0000E+00  7.8198E-04  1.0000E-03
 mr-sdci #  6  3    -93.3282589871  1.5552E-09  1.5287E-02  8.5117E-02  1.0000E-03
 mr-sdci #  6  4    -93.3280838315  3.0436E-07  0.0000E+00  8.5261E-02  1.0000E-03
 mr-sdci #  6  5    -93.1545002254  6.1554E-04  0.0000E+00  8.2652E-02  1.0000E-03
 mr-sdci #  6  6    -93.1535501559  2.6576E-02  0.0000E+00  8.5830E-02  1.0000E-03
 mr-sdci #  6  7    -93.1242877540  1.3701E-07  0.0000E+00  9.7788E-02  1.0000E-03
 mr-sdci #  6  8    -93.1197630612  6.8464E-04  0.0000E+00  9.6235E-02  1.0000E-03
 mr-sdci #  6  9    -93.1190745786  1.7129E-03  0.0000E+00  9.5109E-02  1.0000E-03
 mr-sdci #  6 10    -93.1135687715  1.5060E-07  0.0000E+00  1.0111E-01  1.0000E-03
 mr-sdci #  6 11    -93.1133389034  5.2990E-04  0.0000E+00  1.0298E-01  1.0000E-04
 mr-sdci #  6 12    -93.1127291635  3.6423E-02  0.0000E+00  1.0435E-01  1.0000E-04
 mr-sdci #  6 13    -92.7763243791  2.7200E-04  0.0000E+00  1.7021E-01  1.0000E-04
 mr-sdci #  6 14    -92.7756160382  6.4510E-02  0.0000E+00  1.7470E-01  1.0000E-04
 mr-sdci #  6 15    -92.6965552213  2.3833E-03  0.0000E+00  1.1326E-01  1.0000E-04
 mr-sdci #  6 16    -92.6921629409  2.6428E+01  0.0000E+00  1.0711E-01  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .34    .12    .00    .34
    2   25    0    .20    .06    .00    .20
    3   26    0   5.13    .19    .00   5.13
    4   11    0   2.20   1.23    .00   2.20
    5   15    0   4.47   1.62    .00   4.47
    6   16    0   2.62    .90    .00   2.62
    7    1    0    .50    .29    .00    .49
    8    5    0   2.25    .82    .00   2.25
    9    6    0   8.29    .73    .00   8.29
   10    7    0   1.72    .19    .00   1.72
   11   75    0    .08    .06    .00    .08
   12   45    0    .21    .10    .00    .20
   13   46    0    .57    .08    .00    .57
   14   47    0    .24    .04    .00    .24
   15   81    0    .53    .00    .00    .53
   16   82    0   1.61    .00    .00   1.61
   17  101    0    .27    .00    .00    .27
   18   83    0   1.67    .00    .00   1.66
   19  102    0   4.11    .00    .00   4.11
   20   84    0    .43    .00    .00    .43
   21   91    0    .63    .00    .00    .63
   22   93    0   2.28    .00    .00   2.27
   23   94    0   1.17    .00    .00   1.17
   24  103    0   2.95    .00    .00   2.95
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .050000
time for cinew                         1.420000
time for eigenvalue solver              .010000
time for vector access                  .000000
================================================================
time spent in mult:                    44.4700s 
time spent in multnx:                  44.4300s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.4300s 
time for vector access in mult:          .0000s 
total time per CI iteration:           45.9500s 

          starting ci iteration   7

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1      .25903980     -.40772147     -.86071426     -.00085104      .04363015      .03299090      .02150413     -.00251549
 ref:   2     -.56634268      .20982108     -.27225191      .74012920     -.06773957      .00218639      .00678733      .00209578
 ref:   3      .69396218     -.07211278      .24396460      .66087956      .07685089     -.02100206     -.00611131     -.00205195
 ref:   4     -.31613174     -.86827926      .31814660      .12427532      .00299929      .10466683     -.00779785     -.00369272
 ref:   5     -.00007428      .00061503      .00112674     -.00000415      .01466127      .03052271      .56495764      .29169213
 ref:   6     -.00052601     -.00006894      .00063831     -.00000216      .02698453     -.01343115      .32524612      .23508711
 ref:   7      .00067231     -.00121544      .00055182      .00001169     -.05971404     -.04931442      .28755106     -.83800957
 ref:   8      .00928964     -.01461926     -.02864130      .00000043     -.11111923     -.08489413     -.64216234     -.13499033
 ref:   9      .04116143     -.01116717      .01021401      .00001069     -.33547587      .03715386      .22464617     -.26003965
 ref:  10     -.01881343     -.04042101      .00695969      .00001110      .02098175     -.36955097      .16974783     -.16673262

                ci   9         ci  10         ci  11
 ref:   1      .00031478     -.01960542      .13272241
 ref:   2      .00052498     -.00611903      .04160614
 ref:   3     -.00097786      .00549042     -.03705292
 ref:   4      .00220889      .00721896     -.04982752
 ref:   5     -.51550551      .55899941     -.00211307
 ref:   6      .84715901      .32239999     -.00119629
 ref:   7      .05383267      .27517760     -.00105234
 ref:   8      .00377889      .66005548      .01479733
 ref:   9     -.04722495     -.21334686     -.00656957
 ref:  10      .07777037     -.16144457     -.00184386

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  7  1    -93.3414565950  5.2195E-10  0.0000E+00  7.5406E-04  1.0000E-03
 mr-sdci #  7  2    -93.3414565311  3.8617E-10  0.0000E+00  7.8195E-04  1.0000E-03
 mr-sdci #  7  3    -93.3409643055  1.2705E-02  3.5460E-04  1.2633E-02  1.0000E-03
 mr-sdci #  7  4    -93.3280838315  1.4211E-14  0.0000E+00  8.5261E-02  1.0000E-03
 mr-sdci #  7  5    -93.1545058682  5.6428E-06  0.0000E+00  8.2634E-02  1.0000E-03
 mr-sdci #  7  6    -93.1535584307  8.2747E-06  0.0000E+00  8.5807E-02  1.0000E-03
 mr-sdci #  7  7    -93.1243194356  3.1682E-05  0.0000E+00  9.7880E-02  1.0000E-03
 mr-sdci #  7  8    -93.1197630665  5.2653E-09  0.0000E+00  9.6235E-02  1.0000E-03
 mr-sdci #  7  9    -93.1190745888  1.0241E-08  0.0000E+00  9.5109E-02  1.0000E-03
 mr-sdci #  7 10    -93.1135819732  1.3202E-05  0.0000E+00  1.0113E-01  1.0000E-03
 mr-sdci #  7 11    -92.7964030761 -3.1694E-01  0.0000E+00  1.8856E-01  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .34    .13    .00    .34
    2   25    0    .19    .06    .00    .19
    3   26    0   5.14    .19    .00   5.14
    4   11    0   2.20   1.25    .00   2.20
    5   15    0   4.53   1.65    .00   4.52
    6   16    0   2.66    .93    .00   2.66
    7    1    0    .49    .27    .00    .49
    8    5    0   2.29    .85    .00   2.29
    9    6    0   8.18    .71    .00   8.18
   10    7    0   1.67    .19    .00   1.67
   11   75    0    .09    .05    .00    .08
   12   45    0    .20    .10    .00    .20
   13   46    0    .57    .09    .00    .57
   14   47    0    .23    .04    .00    .23
   15   81    0    .54    .00    .00    .53
   16   82    0   1.55    .00    .00   1.54
   17  101    0    .27    .00    .00    .27
   18   83    0   1.66    .00    .00   1.66
   19  102    0   4.15    .00    .00   4.15
   20   84    0    .41    .00    .00    .41
   21   91    0    .64    .00    .00    .64
   22   93    0   2.28    .00    .00   2.27
   23   94    0   1.17    .00    .00   1.17
   24  103    0   2.96    .00    .00   2.95
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .060000
time for cinew                          .630000
time for eigenvalue solver              .000000
time for vector access                  .000000
================================================================
time spent in mult:                    44.4100s 
time spent in multnx:                  44.3500s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.5100s 
time for vector access in mult:          .0100s 
total time per CI iteration:           45.1100s 

          starting ci iteration   8

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     -.26131774      .40834715     -.85735671      .00085701      .04364432      .03349070     -.05156838     -.00252308
 ref:   2      .56086367     -.21854369     -.27599552     -.74012732     -.06773545      .00234380     -.01609295      .00209347
 ref:   3     -.69062403      .08251645      .24944977     -.66088124      .07684662     -.02114150      .01426489     -.00204996
 ref:   4      .33098565      .86490061      .31124389     -.12427754      .00299772      .10447422      .01966401     -.00368958
 ref:   5      .00008034     -.00061065      .00155148      .00000423      .01467351      .03096989     -.45066680      .29188719
 ref:   6      .00053392      .00006424      .00074914      .00000218      .02698975     -.01320570     -.25307595      .23522544
 ref:   7     -.00064828      .00122724      .00076720     -.00001165     -.05970988     -.04908176     -.22957412     -.83790435
 ref:   8     -.00937278      .01464118     -.03077220     -.00000067     -.11116143     -.08644358      .70687586     -.13506198
 ref:   9     -.04090418      .01177045      .01088779     -.00001069     -.33546228      .03765371     -.24224460     -.26001243
 ref:  10      .01944240      .04018439      .00768672     -.00001095      .02098117     -.36909585     -.18903539     -.16671182

                ci   9         ci  10         ci  11         ci  12
 ref:   1     -.00001779     -.03274874     -.08202783     -.10972624
 ref:   2     -.00043375     -.01015451     -.02528331     -.03469139
 ref:   3      .00089839      .00899496      .02216274      .03106853
 ref:   4     -.00232793      .01251043      .03208781      .04063977
 ref:   5      .50864688      .65984910      .00653522      .00005970
 ref:   6     -.85106393      .37281592      .00082229     -.00082699
 ref:   7     -.05728665      .32481208      .00424623     -.00004516
 ref:   8     -.00236303      .49438535     -.32210936      .00235996
 ref:   9      .04660011     -.15401277      .11720048     -.00397054
 ref:  10     -.07815644     -.12145512      .06929292      .00365670

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  8  1    -93.3414565974  2.3933E-09  0.0000E+00  7.5316E-04  1.0000E-03
 mr-sdci #  8  2    -93.3414565314  3.3529E-10  0.0000E+00  7.8183E-04  1.0000E-03
 mr-sdci #  8  3    -93.3414299642  4.6566E-04  2.5467E-05  3.5483E-03  1.0000E-03
 mr-sdci #  8  4    -93.3280838316  1.5184E-11  0.0000E+00  8.5261E-02  1.0000E-03
 mr-sdci #  8  5    -93.1545058690  8.2274E-10  0.0000E+00  8.2634E-02  1.0000E-03
 mr-sdci #  8  6    -93.1535596156  1.1849E-06  0.0000E+00  8.5807E-02  1.0000E-03
 mr-sdci #  8  7    -93.1274950095  3.1756E-03  0.0000E+00  1.0226E-01  1.0000E-03
 mr-sdci #  8  8    -93.1197630670  5.0233E-10  0.0000E+00  9.6235E-02  1.0000E-03
 mr-sdci #  8  9    -93.1190751257  5.3684E-07  0.0000E+00  9.5110E-02  1.0000E-03
 mr-sdci #  8 10    -93.1150528090  1.4708E-03  0.0000E+00  1.0047E-01  1.0000E-03
 mr-sdci #  8 11    -93.0847653162  2.8836E-01  0.0000E+00  1.6998E-01  1.0000E-04
 mr-sdci #  8 12    -92.7111760803 -4.0155E-01  0.0000E+00  1.0093E-01  1.0000E-04
 
 root number  3 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .34    .12    .00    .34
    2   25    0    .20    .07    .00    .20
    3   26    0   5.14    .19    .00   5.14
    4   11    0   2.22   1.25    .00   2.21
    5   15    0   4.57   1.69    .00   4.56
    6   16    0   2.66    .92    .00   2.65
    7    1    0    .49    .28    .00    .49
    8    5    0   2.27    .83    .00   2.26
    9    6    0   8.20    .73    .00   8.20
   10    7    0   1.73    .20    .00   1.73
   11   75    0    .09    .05    .00    .08
   12   45    0    .20    .09    .00    .19
   13   46    0    .58    .09    .00    .58
   14   47    0    .23    .03    .00    .23
   15   81    0    .52    .00    .00    .52
   16   82    0   1.61    .00    .00   1.60
   17  101    0    .28    .00    .00    .28
   18   83    0   1.66    .00    .00   1.65
   19  102    0   4.04    .00    .00   4.04
   20   84    0    .41    .00    .00    .41
   21   91    0    .61    .00    .00    .61
   22   93    0   2.27    .00    .00   2.26
   23   94    0   1.18    .00    .00   1.17
   24  103    0   2.95    .00    .00   2.95
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .080000
time for cinew                          .560000
time for eigenvalue solver              .000000
time for vector access                  .000000
================================================================
time spent in mult:                    44.4500s 
time spent in multnx:                  44.3500s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.5400s 
time for vector access in mult:          .0000s 
total time per CI iteration:           45.1000s 

          starting ci iteration   9

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1      .07836974     -.38651347     -.90213159     -.00085791      .04558938     -.10082916     -.01297804      .00459653
 ref:   2     -.60922486      .21466912     -.14553412      .74012702      .06844668      .02958748      .00027466      .00145146
 ref:   3      .72874637     -.07598290      .09527177      .66088151     -.07422355     -.03544104      .01918815     -.00127772
 ref:   4     -.24696128     -.87642589      .35395640      .12427785     -.01728783      .00472648     -.10905546     -.00179046
 ref:   5      .00025498      .00057664      .00159958     -.00000425      .05464323     -.07106589     -.01361227     -.61751110
 ref:   6     -.00034990     -.00008944      .00089836     -.00000220      .01540544     -.04943384      .02338606     -.35289169
 ref:   7      .00081422     -.00123179      .00059781      .00001164      .06539591      .02428924      .05348743     -.31885781
 ref:   8      .00279450     -.01385651     -.03243173      .00000067     -.11544668      .25892236      .03246243      .52416681
 ref:   9      .04244390     -.01130481      .00179385      .00001065      .28813394      .18908329     -.03700390     -.18900031
 ref:  10     -.01689224     -.04065004      .01064845      .00001097     -.00185501     -.00370724      .37675487     -.13995943

                ci   9         ci  10         ci  11         ci  12         ci  13
 ref:   1     -.00270776      .00026116     -.00809774      .07876421      .08683814
 ref:   2      .00203591      .00050913     -.00245536      .02238923      .02866444
 ref:   3     -.00199911     -.00096509      .00218580     -.01872144     -.02630482
 ref:   4     -.00361852      .00223567      .00307671     -.03328923     -.03021299
 ref:   5      .29615821     -.51367015      .49247348      .00035614     -.00037940
 ref:   6      .23784959      .84820456      .28249297     -.00162069      .00167697
 ref:   7     -.83568524      .05492626      .24244901      .00144732     -.00073783
 ref:   8     -.13680172      .00366616      .71441973      .02280242     -.01045688
 ref:   9     -.25934353     -.04708950     -.23257205     -.02212410      .01380699
 ref:  10     -.16622831      .07783299     -.17520110      .00877125     -.00873391

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  9  1    -93.3414566092  1.1867E-08  0.0000E+00  7.4708E-04  1.0000E-03
 mr-sdci #  9  2    -93.3414565315  8.9084E-11  0.0000E+00  7.8197E-04  1.0000E-03
 mr-sdci #  9  3    -93.3414563480  2.6384E-05  0.0000E+00  8.3259E-04  1.0000E-03
 mr-sdci #  9  4    -93.3280838316  4.6541E-12  1.5389E-02  8.5261E-02  1.0000E-03
 mr-sdci #  9  5    -93.1547850275  2.7916E-04  0.0000E+00  8.1928E-02  1.0000E-03
 mr-sdci #  9  6    -93.1542848381  7.2522E-04  0.0000E+00  8.3634E-02  1.0000E-03
 mr-sdci #  9  7    -93.1535272373  2.6032E-02  0.0000E+00  8.5929E-02  1.0000E-03
 mr-sdci #  9  8    -93.1222138123  2.4507E-03  0.0000E+00  9.7712E-02  1.0000E-03
 mr-sdci #  9  9    -93.1197629653  6.8784E-04  0.0000E+00  9.6235E-02  1.0000E-03
 mr-sdci #  9 10    -93.1190749776  4.0222E-03  0.0000E+00  9.5109E-02  1.0000E-03
 mr-sdci #  9 11    -93.1129222837  2.8157E-02  0.0000E+00  1.0197E-01  1.0000E-04
 mr-sdci #  9 12    -92.7755561863  6.4380E-02  0.0000E+00  1.7397E-01  1.0000E-04
 mr-sdci #  9 13    -92.6963211459 -8.0003E-02  0.0000E+00  1.1263E-01  1.0000E-04
 
 root number  4 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .35    .13    .00    .34
    2   25    0    .19    .06    .00    .19
    3   26    0   5.16    .20    .00   5.15
    4   11    0   2.20   1.22    .00   2.20
    5   15    0   4.53   1.70    .00   4.53
    6   16    0   2.66    .95    .00   2.65
    7    1    0    .50    .29    .00    .49
    8    5    0   2.26    .82    .00   2.26
    9    6    0   8.29    .75    .00   8.28
   10    7    0   1.75    .20    .00   1.75
   11   75    0    .08    .06    .00    .08
   12   45    0    .20    .09    .00    .19
   13   46    0    .57    .09    .00    .57
   14   47    0    .24    .04    .00    .23
   15   81    0    .54    .00    .00    .54
   16   82    0   1.60    .00    .00   1.60
   17  101    0    .28    .00    .00    .27
   18   83    0   1.66    .00    .00   1.66
   19  102    0   4.13    .00    .00   4.13
   20   84    0    .43    .00    .00    .43
   21   91    0    .62    .00    .00    .62
   22   93    0   2.28    .00    .00   2.28
   23   94    0   1.18    .00    .00   1.17
   24  103    0   2.95    .00    .00   2.95
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .050000
time for cinew                         1.430000
time for eigenvalue solver              .000000
time for vector access                  .000000
================================================================
time spent in mult:                    44.6500s 
time spent in multnx:                  44.5600s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.6000s 
time for vector access in mult:          .0000s 
total time per CI iteration:           46.1300s 

          starting ci iteration  10

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     -.07883786      .38656029     -.90207042     -.00109415      .04518310     -.10104280      .01273889     -.00459654
 ref:   2      .60924465     -.21360348     -.14566518      .73151353      .06921709      .03027745     -.00084882     -.00145229
 ref:   3     -.72900511      .07552362      .09526449      .65282115     -.07375025     -.03424451     -.02004360      .00127696
 ref:   4      .24599796      .87670527      .35406033      .12250511     -.01729040      .00463663      .10892292      .00179032
 ref:   5     -.00025427     -.00057715      .00159951     -.00001055      .05435517     -.07131087      .01346606      .61751102
 ref:   6      .00034986      .00008968      .00089836     -.00000872      .01524432     -.04942196     -.02351395      .35289165
 ref:   7     -.00081553      .00123084      .00059803      .00004487      .06554379      .02417680     -.05332630      .31885803
 ref:   8     -.00281125      .01385815     -.03242952     -.00015722     -.11440818      .25945670     -.03184206     -.52416676
 ref:   9     -.04245593      .01125845      .00179788     -.00027530      .28881405      .18785263      .03790418      .18900040
 ref:  10      .01684889      .04066662      .01065350     -.00022151     -.00141750     -.00260711     -.37676274      .13995950

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14
 ref:   1     -.00270641     -.00026103      .00809782     -.00021506     -.07876407      .08683814
 ref:   2      .00221723     -.00049633      .00246712     -.11368832     -.02229538      .02867117
 ref:   3     -.00183454      .00097676     -.00217515     -.10198225      .01880564     -.02629879
 ref:   4     -.00358605     -.00223332     -.00307461     -.01943933      .03330528     -.03021184
 ref:   5      .29616554      .51366611     -.49247387      .00006271     -.00035619     -.00037940
 ref:   6      .23783806     -.84820781     -.28249317      .00003915      .00162066      .00167697
 ref:   7     -.83568813     -.05491498     -.24244803     -.00023016     -.00144714     -.00073782
 ref:   8     -.13680668     -.00366464     -.71441977      .00046996     -.02280281     -.01045691
 ref:   9     -.25934912      .04709261      .23257201      .00089947      .02212338      .01380694
 ref:  10     -.16623467     -.07783108      .17520101      .00076752     -.00877188     -.00873396

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 10  1    -93.3414566093  4.4547E-11  0.0000E+00  7.4709E-04  1.0000E-03
 mr-sdci # 10  2    -93.3414565316  1.5871E-10  0.0000E+00  7.8198E-04  1.0000E-03
 mr-sdci # 10  3    -93.3414563480  4.6434E-12  0.0000E+00  8.3259E-04  1.0000E-03
 mr-sdci # 10  4    -93.3409075247  1.2824E-02  3.8709E-04  1.3114E-02  1.0000E-03
 mr-sdci # 10  5    -93.1547863482  1.3207E-06  0.0000E+00  8.1923E-02  1.0000E-03
 mr-sdci # 10  6    -93.1542876637  2.8256E-06  0.0000E+00  8.3628E-02  1.0000E-03
 mr-sdci # 10  7    -93.1535289649  1.7276E-06  0.0000E+00  8.5924E-02  1.0000E-03
 mr-sdci # 10  8    -93.1222138123  2.7214E-12  0.0000E+00  9.7712E-02  1.0000E-03
 mr-sdci # 10  9    -93.1197630981  1.3272E-07  0.0000E+00  9.6235E-02  1.0000E-03
 mr-sdci # 10 10    -93.1190749782  6.6770E-10  0.0000E+00  9.5109E-02  1.0000E-03
 mr-sdci # 10 11    -93.1129222843  5.8082E-10  0.0000E+00  1.0197E-01  1.0000E-04
 mr-sdci # 10 12    -92.7998815160  2.4325E-02  0.0000E+00  1.9204E-01  1.0000E-04
 mr-sdci # 10 13    -92.7755561682  7.9235E-02  0.0000E+00  1.7397E-01  1.0000E-04
 mr-sdci # 10 14    -92.6963211453 -7.9295E-02  0.0000E+00  1.1263E-01  1.0000E-04
 
 root number  4 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .34    .12    .00    .33
    2   25    0    .19    .07    .00    .19
    3   26    0   5.14    .19    .00   5.14
    4   11    0   2.20   1.22    .00   2.20
    5   15    0   4.55   1.65    .00   4.54
    6   16    0   2.67    .93    .00   2.66
    7    1    0    .49    .27    .00    .49
    8    5    0   2.27    .84    .00   2.26
    9    6    0   8.29    .73    .00   8.29
   10    7    0   1.73    .20    .00   1.72
   11   75    0    .09    .05    .00    .09
   12   45    0    .20    .10    .00    .20
   13   46    0    .57    .08    .00    .57
   14   47    0    .23    .04    .00    .23
   15   81    0    .53    .00    .00    .52
   16   82    0   1.61    .00    .00   1.60
   17  101    0    .27    .00    .00    .27
   18   83    0   1.62    .00    .00   1.62
   19  102    0   3.95    .00    .00   3.94
   20   84    0    .42    .00    .00    .42
   21   91    0    .63    .00    .00    .62
   22   93    0   2.26    .00    .00   2.26
   23   94    0   1.18    .00    .00   1.17
   24  103    0   2.95    .00    .00   2.95
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .070000
time for cinew                          .760000
time for eigenvalue solver              .000000
time for vector access                  .000000
================================================================
time spent in mult:                    44.3800s 
time spent in multnx:                  44.2800s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.4900s 
time for vector access in mult:          .0000s 
total time per CI iteration:           45.2100s 

          starting ci iteration  11

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1      .08040311     -.38654690     -.90193349      .00305891      .04517574      .10104736     -.01272329     -.00459648
 ref:   2     -.60793332      .20908364     -.14626755     -.73161940      .06926092     -.03039107      .00098710     -.00144597
 ref:   3      .73107718     -.07488629      .09505897     -.64861333     -.07371431      .03412896      .02018332      .00128271
 ref:   4     -.24256226     -.87785461      .35421604     -.12026022     -.01728643     -.00464127     -.10889727      .00179147
 ref:   5      .00025201      .00057840      .00159935     -.00004828      .05434865      .07131978     -.01346002      .61750973
 ref:   6     -.00034963     -.00009075      .00089830     -.00002827      .01524099      .04942106      .02351915      .35289082
 ref:   7      .00081947     -.00122729      .00059865      .00012726      .06555013     -.02418878      .05333013      .31886140
 ref:   8      .00286742     -.01385779     -.03242462      .00010383     -.11438990     -.25946649      .03179989     -.52416651
 ref:   9      .04249563     -.01110430      .00180972      .00019452      .28882397     -.18782012     -.03795166      .18900096
 ref:  10     -.01670431     -.04072364      .01066291      .00011255     -.00140655      .00255139      .37675688      .13995976

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15
 ref:   1      .00271039      .00026109     -.00809802     -.00057445     -.07876416     -.00021509      .08683792
 ref:   2     -.00171012      .00052408     -.00249731     -.08587441     -.02240861      .09329776      .02877207
 ref:   3      .00229401     -.00095183      .00214782     -.07775533      .01870417      .08339567     -.02620870
 ref:   4      .00367738      .00223808      .00306922     -.01532390      .03328600      .01572819     -.03019491
 ref:   5     -.29619946     -.51364910      .49247206     -.00115403     -.00035627      .00039059     -.00037889
 ref:   6     -.23779173      .84822148      .28249217     -.00059238      .00162060      .00025976      .00167731
 ref:   7      .83569051      .05486638      .24245251      .00205618     -.00144694     -.00119232     -.00073938
 ref:   8      .13678382      .00365539      .71442172      .00364197     -.02280278      .00100797     -.01045555
 ref:   9      .25930822     -.04710975     -.23256832      .00714557      .02212343      .00189991      .01380951
 ref:  10      .16620577      .07781962     -.17519811      .00587048     -.00877178      .00148086     -.00873195

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 11  1    -93.3414566095  2.5171E-10  0.0000E+00  7.4698E-04  1.0000E-03
 mr-sdci # 11  2    -93.3414565320  3.1147E-10  0.0000E+00  7.8186E-04  1.0000E-03
 mr-sdci # 11  3    -93.3414563480  1.0402E-11  0.0000E+00  8.3259E-04  1.0000E-03
 mr-sdci # 11  4    -93.3414262896  5.1876E-04  2.8930E-05  3.7988E-03  1.0000E-03
 mr-sdci # 11  5    -93.1547863606  1.2486E-08  0.0000E+00  8.1923E-02  1.0000E-03
 mr-sdci # 11  6    -93.1542877638  1.0011E-07  0.0000E+00  8.3628E-02  1.0000E-03
 mr-sdci # 11  7    -93.1535291145  1.4961E-07  0.0000E+00  8.5924E-02  1.0000E-03
 mr-sdci # 11  8    -93.1222138125  1.2929E-10  0.0000E+00  9.7712E-02  1.0000E-03
 mr-sdci # 11  9    -93.1197638439  7.4585E-07  0.0000E+00  9.6237E-02  1.0000E-03
 mr-sdci # 11 10    -93.1190749804  2.1458E-09  0.0000E+00  9.5109E-02  1.0000E-03
 mr-sdci # 11 11    -93.1129222861  1.8032E-09  0.0000E+00  1.0197E-01  1.0000E-04
 mr-sdci # 11 12    -93.0983406426  2.9846E-01  0.0000E+00  1.6665E-01  1.0000E-04
 mr-sdci # 11 13    -92.7755561879  1.9716E-08  0.0000E+00  1.7397E-01  1.0000E-04
 mr-sdci # 11 14    -92.7112715186  1.4950E-02  0.0000E+00  1.0185E-01  1.0000E-04
 mr-sdci # 11 15    -92.6963211237 -2.3410E-04  0.0000E+00  1.1263E-01  1.0000E-04
 
 root number  4 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .34    .12    .00    .34
    2   25    0    .20    .06    .00    .20
    3   26    0   5.14    .20    .00   5.14
    4   11    0   2.21   1.24    .00   2.21
    5   15    0   4.54   1.67    .00   4.53
    6   16    0   2.66    .92    .00   2.66
    7    1    0    .49    .29    .00    .49
    8    5    0   2.26    .82    .00   2.26
    9    6    0   8.29    .74    .00   8.28
   10    7    0   1.72    .19    .00   1.72
   11   75    0    .09    .05    .00    .08
   12   45    0    .20    .10    .00    .20
   13   46    0    .58    .08    .00    .58
   14   47    0    .23    .04    .00    .23
   15   81    0    .53    .00    .00    .52
   16   82    0   1.59    .00    .00   1.58
   17  101    0    .28    .00    .00    .27
   18   83    0   1.66    .00    .00   1.66
   19  102    0   4.12    .00    .00   4.12
   20   84    0    .43    .00    .00    .43
   21   91    0    .64    .00    .00    .63
   22   93    0   2.27    .00    .00   2.27
   23   94    0   1.18    .00    .00   1.17
   24  103    0   2.95    .00    .00   2.95
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .060000
time for cinew                          .800000
time for eigenvalue solver              .000000
time for vector access                  .000000
================================================================
time spent in mult:                    44.6000s 
time spent in multnx:                  44.5200s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.5200s 
time for vector access in mult:          .0100s 
total time per CI iteration:           45.4700s 

          starting ci iteration  12

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     -.08651760     -.38596856     -.90089953     -.03604778     -.00431727      .04822713      .09949316     -.01304597
 ref:   2      .64792496      .23291444     -.13436482     -.69083002      .09440163      .05626526     -.02304749     -.00101904
 ref:   3     -.69710373     -.03283617      .10844288     -.68599056      .05954182     -.08198236      .04310674      .01784952
 ref:   4      .23687837     -.87468323      .35770041     -.14231250      .00874908     -.01852734     -.00363134     -.10925064
 ref:   5     -.00024057      .00058515      .00159899      .00001015      .00050739      .05623268      .06980442     -.01365249
 ref:   6      .00034976     -.00009427      .00089806      .00001002     -.00238855      .01665020      .04899086      .02333997
 ref:   7     -.00083890     -.00121599      .00060097     -.00000268      .01226271      .06386904     -.02502058      .05356336
 ref:   8     -.00308779     -.01383835     -.03238794     -.00127335      .00943775     -.12198163     -.25570534      .03268336
 ref:   9     -.04259589     -.01041940      .00190577     -.00243285      .05571995      .28015733     -.19292942     -.03647688
 ref:  10      .01612878     -.04093648      .01071047     -.00072183      .00731865     -.00314171      .00500127      .37665916

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1     -.00459646      .00270887     -.00026111      .00809787      .07870801      .00350769     -.00570669      .08665432
 ref:   2     -.00144256     -.00207005     -.00053911      .00246418      .02106781      .06482604      .07357971      .03267364
 ref:   3      .00128578      .00196952      .00093834     -.00217770     -.01996125      .06033927      .06701651     -.02273322
 ref:   4      .00179207      .00361451     -.00224065     -.00307503     -.03355506      .01259611      .01312056     -.02955230
 ref:   5      .61750948     -.29620933      .51364466     -.49247316      .00038158     -.00120402      .00103876     -.00032212
 ref:   6      .35289056     -.23777943     -.84822498     -.28249306     -.00159946     -.00102568      .00066499      .00171665
 ref:   7      .31886215      .83569717     -.05485383     -.24244959      .00136105      .00411329     -.00322417     -.00091866
 ref:   8     -.52416651      .13679906     -.00365274     -.71442009      .02289616     -.00421602      .00379757     -.01026114
 ref:   9      .18900100      .25933031      .04711452      .23257139     -.02193095     -.00941433      .00545598      .01413307
 ref:  10      .13995974      .16622573     -.07781638      .17520050      .00891299     -.00664384      .00518493     -.00845807

 trial vector basis is being transformed.  new dimension:  10

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 12  1    -93.3414566109  1.3839E-09  0.0000E+00  7.4638E-04  1.0000E-03
 mr-sdci # 12  2    -93.3414565329  9.5404E-10  0.0000E+00  7.8139E-04  1.0000E-03
 mr-sdci # 12  3    -93.3414563481  8.3386E-11  0.0000E+00  8.3254E-04  1.0000E-03
 mr-sdci # 12  4    -93.3414561052  2.9816E-05  0.0000E+00  8.9133E-04  1.0000E-03
 mr-sdci # 12  5    -93.1554932679  7.0691E-04  1.6962E-02  8.0539E-02  1.0000E-03
 mr-sdci # 12  6    -93.1547728562  4.8509E-04  0.0000E+00  8.2013E-02  1.0000E-03
 mr-sdci # 12  7    -93.1542754906  7.4638E-04  0.0000E+00  8.3680E-02  1.0000E-03
 mr-sdci # 12  8    -93.1535277205  3.1314E-02  0.0000E+00  8.5933E-02  1.0000E-03
 mr-sdci # 12  9    -93.1222138124  2.4500E-03  0.0000E+00  9.7712E-02  1.0000E-03
 mr-sdci # 12 10    -93.1197635988  6.8862E-04  0.0000E+00  9.6235E-02  1.0000E-03
 mr-sdci # 12 11    -93.1190749800  6.1527E-03  0.0000E+00  9.5109E-02  1.0000E-04
 mr-sdci # 12 12    -93.1129222844  1.4582E-02  0.0000E+00  1.0197E-01  1.0000E-04
 mr-sdci # 12 13    -92.7755566043  4.1642E-07  0.0000E+00  1.7397E-01  1.0000E-04
 mr-sdci # 12 14    -92.7746025247  6.3331E-02  0.0000E+00  1.7381E-01  1.0000E-04
 mr-sdci # 12 15    -92.6972490847  9.2796E-04  0.0000E+00  1.1494E-01  1.0000E-04
 mr-sdci # 12 16    -92.6963183213  4.1554E-03  0.0000E+00  1.1263E-01  1.0000E-04
 
 root number  5 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .35    .13    .00    .34
    2   25    0    .19    .07    .00    .19
    3   26    0   5.04    .19    .00   5.03
    4   11    0   2.16   1.22    .00   2.16
    5   15    0   4.54   1.67    .00   4.53
    6   16    0   2.66    .94    .00   2.65
    7    1    0    .49    .28    .00    .48
    8    5    0   2.27    .85    .00   2.26
    9    6    0   8.29    .74    .00   8.29
   10    7    0   1.73    .20    .00   1.73
   11   75    0    .08    .06    .00    .08
   12   45    0    .21    .10    .00    .20
   13   46    0    .56    .08    .00    .56
   14   47    0    .24    .04    .00    .23
   15   81    0    .52    .00    .00    .52
   16   82    0   1.61    .00    .00   1.61
   17  101    0    .27    .00    .00    .27
   18   83    0   1.66    .00    .00   1.66
   19  102    0   4.13    .00    .00   4.13
   20   84    0    .43    .00    .00    .43
   21   91    0    .63    .00    .00    .63
   22   93    0   2.28    .00    .00   2.27
   23   94    0   1.17    .00    .00   1.17
   24  103    0   2.95    .00    .00   2.95
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .040000
time for cinew                         1.430000
time for eigenvalue solver              .010000
time for vector access                  .000000
================================================================
time spent in mult:                    44.4600s 
time spent in multnx:                  44.3700s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.5700s 
time for vector access in mult:          .0000s 
total time per CI iteration:           45.9400s 

          starting ci iteration  13

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1      .09606212      .38365019      .89972176     -.05883779      .00354721     -.04821485     -.09950254     -.01303787
 ref:   2     -.67237264     -.23662875      .12908185     -.66678029     -.09252941     -.05651057      .02324097     -.00120675
 ref:   3      .67819755     -.00590626     -.11592543     -.70422839     -.05990641      .08182797     -.04298333      .01772976
 ref:   4     -.21929701      .87530192     -.36025434     -.15923686     -.00919547      .01850426      .00365027     -.10926775
 ref:   5      .00022383     -.00059334     -.00159819      .00003409      .00190785     -.05623372     -.06980327     -.01365482
 ref:   6     -.00034830      .00010183     -.00089769      .00002294      .00392343     -.01664377     -.04899563      .02334354
 ref:   7      .00087022      .00119467     -.00060383     -.00004419     -.01916696     -.06389940      .02504269      .05354243
 ref:   8      .00342959      .01375533      .03234574     -.00209661     -.00661685      .12195427      .25572613      .03266569
 ref:   9      .04275288      .00923659     -.00200419     -.00390144     -.04423827     -.28030440      .19304204     -.03658637
 ref:  10     -.01513093      .04128175     -.01076739     -.00144406     -.00617429      .00312378     -.00498874      .37664318

                ci   9         ci  10         ci  11
 ref:   1     -.00460053     -.00273218      .00454381
 ref:   2     -.00138374      .00243512     -.01844586
 ref:   3      .00132090     -.00175025     -.00539997
 ref:   4      .00179560     -.00359066      .00282490
 ref:   5      .61754627      .29614590     -.00505191
 ref:   6      .35291822      .23773373     -.00272934
 ref:   7      .31876331     -.83573417      .02235997
 ref:   8     -.52417096     -.13666017     -.02160410
 ref:   9      .18902204     -.25902119     -.07040785
 ref:  10      .13994927     -.16618247     -.01524434

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 13  1    -93.3414566131  2.1505E-09  0.0000E+00  7.4567E-04  1.0000E-03
 mr-sdci # 13  2    -93.3414565347  1.8220E-09  0.0000E+00  7.8081E-04  1.0000E-03
 mr-sdci # 13  3    -93.3414563481  4.7837E-11  0.0000E+00  8.3251E-04  1.0000E-03
 mr-sdci # 13  4    -93.3414561818  7.6622E-08  0.0000E+00  8.7079E-04  1.0000E-03
 mr-sdci # 13  5    -93.1672733132  1.1780E-02  9.1722E-04  1.7905E-02  1.0000E-03
 mr-sdci # 13  6    -93.1547728613  5.1501E-09  0.0000E+00  8.2012E-02  1.0000E-03
 mr-sdci # 13  7    -93.1542754962  5.6710E-09  0.0000E+00  8.3679E-02  1.0000E-03
 mr-sdci # 13  8    -93.1535277296  9.0861E-09  0.0000E+00  8.5933E-02  1.0000E-03
 mr-sdci # 13  9    -93.1222138566  4.4186E-08  0.0000E+00  9.7712E-02  1.0000E-03
 mr-sdci # 13 10    -93.1197655253  1.9264E-06  0.0000E+00  9.6242E-02  1.0000E-03
 mr-sdci # 13 11    -92.7466402409 -3.7243E-01  0.0000E+00  2.5600E-01  1.0000E-04
 
 root number  5 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .34    .12    .00    .34
    2   25    0    .20    .06    .00    .20
    3   26    0   5.14    .20    .00   5.14
    4   11    0   2.22   1.24    .00   2.22
    5   15    0   4.53   1.66    .00   4.53
    6   16    0   2.66    .92    .00   2.66
    7    1    0    .49    .28    .00    .49
    8    5    0   2.26    .83    .00   2.25
    9    6    0   8.04    .73    .00   8.03
   10    7    0   1.73    .21    .00   1.73
   11   75    0    .08    .06    .00    .08
   12   45    0    .21    .10    .00    .20
   13   46    0    .56    .09    .00    .56
   14   47    0    .24    .03    .00    .24
   15   81    0    .53    .00    .00    .53
   16   82    0   1.61    .00    .00   1.61
   17  101    0    .27    .00    .00    .27
   18   83    0   1.66    .00    .00   1.66
   19  102    0   4.11    .00    .00   4.10
   20   84    0    .40    .00    .00    .40
   21   91    0    .61    .00    .00    .60
   22   93    0   2.27    .00    .00   2.27
   23   94    0   1.18    .00    .00   1.17
   24  103    0   2.95    .00    .00   2.95
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .060000
time for cinew                          .580000
time for eigenvalue solver              .000000
time for vector access                  .000000
================================================================
time spent in mult:                    44.2900s 
time spent in multnx:                  44.2300s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.5300s 
time for vector access in mult:          .0100s 
total time per CI iteration:           44.9500s 

          starting ci iteration  14

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     -.04702362      .17016780      .36347059     -.89782928      .00345889     -.04810859      .09955518     -.01305459
 ref:   2      .95496844     -.19657418     -.07200524     -.11642154     -.09144461     -.05669150     -.02337916     -.00124473
 ref:   3     -.08258112     -.71930370      .65387091      .13268155     -.05939063      .08177042      .04277264      .01767774
 ref:   4      .21989262      .61997102      .63603522      .36349311     -.00899926      .01847143     -.00371663     -.10927436
 ref:   5     -.00019066     -.00046902     -.00038971      .00159724      .00050291     -.05612492      .06990789     -.01365177
 ref:   6      .00022770      .00027107     -.00009624      .00089607      .00236756     -.01654900      .04907224      .02334817
 ref:   7     -.00068130      .00034705      .00125933      .00060769     -.01482179     -.06404829     -.02513056      .05351121
 ref:   8     -.00170750      .00613020      .01300451     -.03227847     -.00795786      .12171294     -.25581875      .03271953
 ref:   9     -.03123967     -.01674317      .02592391      .00219377     -.04852187     -.28047230     -.19271145     -.03649609
 ref:  10      .01030947      .03483113      .02480542      .01077931     -.00766135      .00318522      .00509408      .37664702

                ci   9         ci  10         ci  11         ci  12
 ref:   1      .00460289     -.00272506     -.00059857      .00460162
 ref:   2      .00144345      .00206994      .02141838     -.01341951
 ref:   3     -.00127418     -.00201635      .01451375     -.00166234
 ref:   4     -.00178459     -.00363624      .00242950      .00366065
 ref:   5     -.61785479      .29557291     -.00297398     -.00684384
 ref:   6     -.35317700      .23748574     -.00948459     -.00627368
 ref:   7     -.31785627     -.83641984      .02815654      .03332649
 ref:   8      .52425903     -.13582314     -.03056095     -.03187522
 ref:   9     -.18887712     -.25849167     -.07420044     -.09695904
 ref:  10     -.13981450     -.16611680     -.02295855     -.02302526

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 14  1    -93.3414566858  7.2711E-08  0.0000E+00  7.6353E-04  1.0000E-03
 mr-sdci # 14  2    -93.3414565420  7.2448E-09  0.0000E+00  7.8746E-04  1.0000E-03
 mr-sdci # 14  3    -93.3414565282  1.8004E-07  0.0000E+00  7.8991E-04  1.0000E-03
 mr-sdci # 14  4    -93.3414563481  1.6629E-07  0.0000E+00  8.3250E-04  1.0000E-03
 mr-sdci # 14  5    -93.1681953936  9.2208E-04  2.3469E-04  8.7398E-03  1.0000E-03
 mr-sdci # 14  6    -93.1547732585  3.9715E-07  0.0000E+00  8.2008E-02  1.0000E-03
 mr-sdci # 14  7    -93.1542761621  6.6590E-07  0.0000E+00  8.3673E-02  1.0000E-03
 mr-sdci # 14  8    -93.1535277763  4.6682E-08  0.0000E+00  8.5933E-02  1.0000E-03
 mr-sdci # 14  9    -93.1222142636  4.0697E-07  0.0000E+00  9.7711E-02  1.0000E-03
 mr-sdci # 14 10    -93.1197805629  1.5038E-05  0.0000E+00  9.6339E-02  1.0000E-03
 mr-sdci # 14 11    -92.9452607410  1.9862E-01  0.0000E+00  2.2259E-01  1.0000E-04
 mr-sdci # 14 12    -92.7272035487 -3.8572E-01  0.0000E+00  2.5730E-01  1.0000E-04
 
 root number  5 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .35    .13    .00    .34
    2   25    0    .19    .07    .00    .19
    3   26    0   5.13    .19    .00   5.13
    4   11    0   2.22   1.24    .00   2.21
    5   15    0   4.54   1.69    .00   4.54
    6   16    0   2.66    .92    .00   2.65
    7    1    0    .49    .27    .00    .49
    8    5    0   2.26    .83    .00   2.25
    9    6    0   8.29    .73    .00   8.29
   10    7    0   1.73    .20    .00   1.73
   11   75    0    .08    .06    .00    .08
   12   45    0    .21    .09    .00    .20
   13   46    0    .57    .09    .00    .57
   14   47    0    .24    .03    .00    .24
   15   81    0    .53    .00    .00    .53
   16   82    0   1.59    .00    .00   1.59
   17  101    0    .28    .00    .00    .27
   18   83    0   1.66    .00    .00   1.66
   19  102    0   4.13    .00    .00   4.13
   20   84    0    .43    .00    .00    .43
   21   91    0    .63    .00    .00    .63
   22   93    0   2.28    .00    .00   2.27
   23   94    0   1.15    .00    .00   1.14
   24  103    0   2.94    .00    .00   2.94
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .020000
time for cinew                          .600000
time for eigenvalue solver              .000000
time for vector access                  .000000
================================================================
time spent in mult:                    44.5800s 
time spent in multnx:                  44.5000s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.5400s 
time for vector access in mult:          .0000s 
total time per CI iteration:           45.2100s 

          starting ci iteration  15

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1      .12085776      .18997170     -.35642638     -.88974205     -.00349515      .04812035      .09955274      .01301303
 ref:   2     -.97064996      .05363124      .05937589     -.14417951      .09081619      .05672867     -.02335748      .00138089
 ref:   3     -.09680755     -.69333324     -.68311374      .11244667      .05898026     -.08174367      .04279459     -.01757512
 ref:   4     -.05669791      .67058543     -.61006080      .37988271      .00880984     -.01844337     -.00368327      .10929458
 ref:   5      .00000811     -.00056006      .00037165      .00158456     -.00121373      .05615105      .06991715      .01368448
 ref:   6     -.00018893      .00028843      .00010732      .00090000     -.00334338      .01656946      .04907919     -.02328330
 ref:   7      .00075009      .00019703     -.00124017      .00063615      .01617388      .06400191     -.02517431     -.05364974
 ref:   8      .00435684      .00683177     -.01275065     -.03198628      .00790357     -.12174283     -.25581243     -.03261357
 ref:   9      .02628388     -.02315803     -.02646673      .00239966      .04995887      .28044123     -.19274498      .03640167
 ref:  10     -.00108814      .03696882     -.02335391      .01172967      .00810281     -.00326994      .00499457     -.37665598

                ci   9         ci  10         ci  11         ci  12         ci  13
 ref:   1      .00459702      .00276076      .00067404      .00146173      .00440334
 ref:   2      .00148833     -.00278754     -.02268035      .00368394     -.01485886
 ref:   3     -.00124759      .00150298     -.01585354      .00481548     -.00276909
 ref:   4     -.00178657      .00357016     -.00260907      .00216272      .00332949
 ref:   5     -.61740478     -.29649432      .00522217     -.00699360     -.00368873
 ref:   6     -.35280035     -.23827186     -.00099938     -.02110496      .00536085
 ref:   7     -.31908923      .83520910     -.04102681      .02800555      .02493985
 ref:   8      .52406600      .13637690      .00075561     -.05019236     -.00963040
 ref:   9     -.18930803      .25887354      .03708613     -.09283555     -.06649860
 ref:  10     -.14004921      .16559812     -.00265877     -.03802085     -.00606025

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 15  1    -93.3414567516  6.5827E-08  0.0000E+00  7.4538E-04  1.0000E-03
 mr-sdci # 15  2    -93.3414565679  2.5969E-08  0.0000E+00  7.6850E-04  1.0000E-03
 mr-sdci # 15  3    -93.3414565282  3.6351E-11  0.0000E+00  7.9078E-04  1.0000E-03
 mr-sdci # 15  4    -93.3414563495  1.4209E-09  0.0000E+00  8.3234E-04  1.0000E-03
 mr-sdci # 15  5    -93.1684189772  2.2358E-04  8.3225E-05  5.2452E-03  1.0000E-03
 mr-sdci # 15  6    -93.1547733305  7.1964E-08  0.0000E+00  8.2009E-02  1.0000E-03
 mr-sdci # 15  7    -93.1542761982  3.6029E-08  0.0000E+00  8.3672E-02  1.0000E-03
 mr-sdci # 15  8    -93.1535288390  1.0628E-06  0.0000E+00  8.5924E-02  1.0000E-03
 mr-sdci # 15  9    -93.1222144733  2.0967E-07  0.0000E+00  9.7710E-02  1.0000E-03
 mr-sdci # 15 10    -93.1198444377  6.3875E-05  0.0000E+00  9.6487E-02  1.0000E-03
 mr-sdci # 15 11    -93.0521657940  1.0691E-01  0.0000E+00  1.4690E-01  1.0000E-04
 mr-sdci # 15 12    -92.8063316513  7.9128E-02  0.0000E+00  2.8434E-01  1.0000E-04
 mr-sdci # 15 13    -92.6977041325 -7.7852E-02  0.0000E+00  2.4063E-01  1.0000E-04
 
 root number  5 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .35    .13    .00    .34
    2   25    0    .19    .06    .00    .19
    3   26    0   5.18    .20    .00   5.17
    4   11    0   2.24   1.25    .00   2.23
    5   15    0   4.53   1.68    .00   4.53
    6   16    0   2.66    .93    .00   2.66
    7    1    0    .49    .28    .00    .49
    8    5    0   2.26    .83    .00   2.25
    9    6    0   8.29    .73    .00   8.29
   10    7    0   1.74    .20    .00   1.73
   11   75    0    .08    .05    .00    .08
   12   45    0    .20    .10    .00    .19
   13   46    0    .58    .09    .00    .57
   14   47    0    .24    .04    .00    .24
   15   81    0    .53    .00    .00    .53
   16   82    0   1.60    .00    .00   1.60
   17  101    0    .28    .00    .00    .27
   18   83    0   1.66    .00    .00   1.66
   19  102    0   4.12    .00    .00   4.12
   20   84    0    .43    .00    .00    .43
   21   91    0    .64    .00    .00    .63
   22   93    0   2.27    .00    .00   2.27
   23   94    0   1.17    .00    .00   1.16
   24  103    0   2.96    .00    .00   2.95
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .050000
time for cinew                          .700000
time for eigenvalue solver              .000000
time for vector access                  .000000
================================================================
time spent in mult:                    44.6900s 
time spent in multnx:                  44.5800s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.5700s 
time for vector access in mult:          .0100s 
total time per CI iteration:           45.4600s 

          starting ci iteration  16

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1      .09345428      .17627008      .38905359     -.88216159     -.00349304     -.04800777      .09959332     -.01311741
 ref:   2     -.96579322      .05869064     -.11488630     -.14125195      .09028740     -.05687769     -.02337102     -.00149127
 ref:   3     -.13944198     -.71601886      .64869437      .12822430      .05872289      .08174416      .04266150      .01721170
 ref:   4     -.09177643      .64974056      .61969194      .39342232      .00863359      .01826483     -.00391425     -.10933860
 ref:   5      .00003986     -.00054531     -.00042435      .00157602     -.00102956     -.05605022      .06998775     -.01366230
 ref:   6     -.00017233      .00029389     -.00013636      .00089924     -.00255664     -.01638636      .04920570      .02339875
 ref:   7      .00067341      .00015222      .00127950      .00065982      .01516722     -.06409615     -.02509845      .05348796
 ref:   8      .00337806      .00634156      .01391985     -.03171453      .00827797      .12149026     -.25588786      .03296370
 ref:   9      .02456675     -.02413991      .02718034      .00287995      .05028247     -.28070091     -.19241314     -.03553591
 ref:  10     -.00243563      .03615773      .02424108      .01224119      .00860939      .00382935      .00567059      .37662958

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14
 ref:   1      .00459721      .00274821      .00051718      .00215094     -.00087957      .00404816
 ref:   2      .00156795     -.00233459     -.02253228     -.00566066      .01290066     -.01101591
 ref:   3     -.00118464      .00184663     -.01648589     -.00092568      .00780727     -.00046134
 ref:   4     -.00177955      .00359951     -.00195228      .00127083      .00112750      .00368057
 ref:   5     -.61760450     -.29606142     -.00415733     -.00187741     -.00743381     -.00625518
 ref:   6     -.35300692     -.23827062      .00586859     -.03167511      .00369564      .01397452
 ref:   7     -.31842675      .83595208     -.02091032      .00761783      .02708628      .03430293
 ref:   8      .52410844      .13574214      .01417860     -.04913301     -.01812620     -.00755402
 ref:   9     -.18925469      .25830895      .03879003     -.06330530     -.05564197     -.08003234
 ref:  10     -.13992908      .16572564      .00123586     -.03285047     -.01904737     -.00747294

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 16  1    -93.3414567579  6.3466E-09  0.0000E+00  7.4675E-04  1.0000E-03
 mr-sdci # 16  2    -93.3414565680  2.0393E-11  0.0000E+00  7.6850E-04  1.0000E-03
 mr-sdci # 16  3    -93.3414565560  2.7795E-08  0.0000E+00  7.8098E-04  1.0000E-03
 mr-sdci # 16  4    -93.3414563502  6.4677E-10  0.0000E+00  8.3223E-04  1.0000E-03
 mr-sdci # 16  5    -93.1685107395  9.1762E-05  2.2582E-05  2.8572E-03  1.0000E-03
 mr-sdci # 16  6    -93.1547742070  8.7658E-07  0.0000E+00  8.1999E-02  1.0000E-03
 mr-sdci # 16  7    -93.1542766730  4.7487E-07  0.0000E+00  8.3665E-02  1.0000E-03
 mr-sdci # 16  8    -93.1535327081  3.8691E-06  0.0000E+00  8.5917E-02  1.0000E-03
 mr-sdci # 16  9    -93.1222147841  3.1084E-07  0.0000E+00  9.7707E-02  1.0000E-03
 mr-sdci # 16 10    -93.1198527886  8.3508E-06  0.0000E+00  9.6542E-02  1.0000E-03
 mr-sdci # 16 11    -93.1030793474  5.0914E-02  0.0000E+00  8.8223E-02  1.0000E-04
 mr-sdci # 16 12    -92.8296683712  2.3337E-02  0.0000E+00  2.9065E-01  1.0000E-04
 mr-sdci # 16 13    -92.7740948236  7.6391E-02  0.0000E+00  2.1394E-01  1.0000E-04
 mr-sdci # 16 14    -92.6807194553 -9.3883E-02  0.0000E+00  2.3788E-01  1.0000E-04
 
 root number  5 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .35    .13    .00    .34
    2   25    0    .19    .07    .00    .19
    3   26    0   5.14    .19    .00   5.13
    4   11    0   2.21   1.23    .00   2.20
    5   15    0   4.39   1.60    .00   4.39
    6   16    0   2.66    .92    .00   2.66
    7    1    0    .49    .27    .00    .49
    8    5    0   2.27    .85    .00   2.26
    9    6    0   8.30    .73    .00   8.30
   10    7    0   1.72    .20    .00   1.72
   11   75    0    .09    .05    .00    .08
   12   45    0    .20    .11    .00    .20
   13   46    0    .58    .08    .00    .57
   14   47    0    .23    .04    .00    .23
   15   81    0    .53    .00    .00    .52
   16   82    0   1.61    .00    .00   1.60
   17  101    0    .27    .00    .00    .26
   18   83    0   1.66    .00    .00   1.66
   19  102    0   4.13    .00    .00   4.13
   20   84    0    .43    .00    .00    .43
   21   91    0    .64    .00    .00    .62
   22   93    0   2.28    .00    .00   2.28
   23   94    0   1.16    .00    .00   1.16
   24  103    0   2.94    .00    .00   2.94
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .030000
time for cinew                          .730000
time for eigenvalue solver              .010000
time for vector access                  .000000
================================================================
time spent in mult:                    44.4700s 
time spent in multnx:                  44.3600s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.4700s 
time for vector access in mult:          .0000s 
total time per CI iteration:           45.2400s 

          starting ci iteration  17

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1      .10697197      .24005418     -.35755167     -.87889966      .00347982      .04773000     -.09972389      .01314247
 ref:   2     -.96560453      .06080623      .10983643     -.14559779     -.09011655      .05705365      .02327821      .00148012
 ref:   3     -.14307473     -.59904150     -.75755423      .12713536     -.05867145     -.08179171     -.04239308     -.01712731
 ref:   4     -.07107493      .74107150     -.50555825      .39944618     -.00861744     -.01818870      .00402676      .10934843
 ref:   5      .00001588     -.00061658      .00033686      .00157115      .00091991      .05579908     -.07017536      .01365218
 ref:   6     -.00017738      .00026312      .00018566      .00090069      .00275645      .01628626     -.04923405     -.02336929
 ref:   7      .00068452      .00033259     -.00122211      .00067260     -.01382434      .06471049      .02523904     -.05337440
 ref:   8      .00386193      .00862258     -.01278676     -.03159717     -.00823727     -.12077840      .25621710     -.03302970
 ref:   9      .02454166     -.01994453     -.03041361      .00298904     -.05135879      .28081958      .19138033      .03517720
 ref:  10     -.00140416      .03958204     -.01803210      .01253624     -.00887998     -.00409464     -.00588624     -.37664036

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15
 ref:   1     -.00458985      .00258476      .00115867     -.00099827      .00106425     -.00085273      .00470778
 ref:   2     -.00133777      .00197442     -.02137886      .01350297      .00704073      .00594017     -.01106273
 ref:   3      .00138610      .00510047     -.01555423      .00703120      .00580286      .00397469     -.00007737
 ref:   4      .00181876      .00384960     -.00095851      .00029661      .00167739      .00084499      .00397684
 ref:   5      .61593981     -.29388111     -.05788849     -.00224552     -.00576323     -.00592342     -.00580316
 ref:   6      .35173628     -.23470783     -.04850398     -.01938171     -.04156279      .04074005     -.00884306
 ref:   7      .32356099      .82367263      .13947875     -.03496673     -.00259096      .05327068      .01003703
 ref:   8     -.52343130      .13472368      .03398862      .01379903     -.05090378      .00725145     -.01629993
 ref:   9      .18974459      .23598801      .12069895      .11665666     -.02353628     -.10038801     -.03411995
 ref:  10      .14070306      .15888430      .04395381      .04174848     -.02039228     -.02246482      .00372091

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 17  1    -93.3414567637  5.7114E-09  0.0000E+00  7.3961E-04  1.0000E-03
 mr-sdci # 17  2    -93.3414565706  2.6539E-09  0.0000E+00  7.6676E-04  1.0000E-03
 mr-sdci # 17  3    -93.3414565576  1.5467E-09  0.0000E+00  7.8228E-04  1.0000E-03
 mr-sdci # 17  4    -93.3414563507  5.5002E-10  0.0000E+00  8.3223E-04  1.0000E-03
 mr-sdci # 17  5    -93.1685310297  2.0290E-05  3.7548E-06  1.1943E-03  1.0000E-03
 mr-sdci # 17  6    -93.1547765190  2.3119E-06  0.0000E+00  8.1992E-02  1.0000E-03
 mr-sdci # 17  7    -93.1542775149  8.4184E-07  0.0000E+00  8.3670E-02  1.0000E-03
 mr-sdci # 17  8    -93.1535328779  1.6975E-07  0.0000E+00  8.5909E-02  1.0000E-03
 mr-sdci # 17  9    -93.1222158839  1.0998E-06  0.0000E+00  9.7710E-02  1.0000E-03
 mr-sdci # 17 10    -93.1200620165  2.0923E-04  0.0000E+00  9.5032E-02  1.0000E-03
 mr-sdci # 17 11    -93.1160852350  1.3006E-02  0.0000E+00  5.7276E-02  1.0000E-04
 mr-sdci # 17 12    -92.8929960892  6.3328E-02  0.0000E+00  1.9953E-01  1.0000E-04
 mr-sdci # 17 13    -92.8008584643  2.6764E-02  0.0000E+00  2.8066E-01  1.0000E-04
 mr-sdci # 17 14    -92.7490792100  6.8360E-02  0.0000E+00  2.6564E-01  1.0000E-04
 mr-sdci # 17 15    -92.6626608438 -3.4588E-02  0.0000E+00  2.3197E-01  1.0000E-04
 
 root number  5 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .32    .11    .00    .31
    2   25    0    .20    .07    .00    .19
    3   26    0   5.10    .19    .00   5.10
    4   11    0   2.22   1.24    .00   2.21
    5   15    0   4.54   1.68    .00   4.53
    6   16    0   2.66    .93    .00   2.66
    7    1    0    .50    .29    .00    .49
    8    5    0   2.26    .83    .00   2.26
    9    6    0   8.29    .74    .00   8.28
   10    7    0   1.73    .21    .00   1.73
   11   75    0    .08    .06    .00    .08
   12   45    0    .21    .10    .00    .20
   13   46    0    .57    .08    .00    .57
   14   47    0    .24    .05    .00    .24
   15   81    0    .53    .00    .00    .53
   16   82    0   1.54    .00    .00   1.54
   17  101    0    .26    .00    .00    .26
   18   83    0   1.60    .00    .00   1.59
   19  102    0   4.06    .00    .00   4.05
   20   84    0    .44    .00    .00    .43
   21   91    0    .63    .00    .00    .63
   22   93    0   2.28    .00    .00   2.27
   23   94    0   1.17    .00    .00   1.17
   24  103    0   2.95    .00    .00   2.94
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .060000
time for cinew                          .760000
time for eigenvalue solver              .010000
time for vector access                  .000000
================================================================
time spent in mult:                    44.3800s 
time spent in multnx:                  44.2600s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.5800s 
time for vector access in mult:          .0000s 
total time per CI iteration:           45.2100s 

          starting ci iteration  18

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     -.11278491     -.23121794     -.36312567     -.87825647     -.00347753     -.04657896     -.10025277      .01324990
 ref:   2      .96332881     -.08316391      .11186000     -.14806238      .09007771     -.05740940      .02265386      .00132240
 ref:   3      .15848420      .61398013     -.74272550      .12507380      .05870271      .08225247     -.04142340     -.01682995
 ref:   4      .05932151     -.72939118     -.52284868      .40060416      .00861570      .01788995      .00447500      .10938014
 ref:   5     -.00000540      .00061003      .00035098      .00156944     -.00110893     -.05539972     -.07057321      .01378820
 ref:   6      .00018503     -.00026458      .00017879      .00090217     -.00255142     -.01532236     -.04965541     -.02338663
 ref:   7     -.00068589     -.00028904     -.00123222      .00067440      .01389513     -.06459462      .02419764     -.05365692
 ref:   8     -.00406529     -.00830390     -.01298748     -.03157331      .00839804      .11817727      .25735454     -.03340817
 ref:   9     -.02399322      .02115777     -.03002286      .00299968      .05161482     -.28222757      .18762457      .03387366
 ref:  10      .00071838     -.03913901     -.01896305      .01260937      .00900846      .00522621     -.00675856     -.37667156

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1     -.00459561      .00209814     -.00184405     -.00058573     -.00086318     -.00141044     -.00051879      .00471354
 ref:   2     -.00114999      .00848950      .01811631      .01800187      .00635667      .00097829      .00374019     -.01083786
 ref:   3      .00154709      .01002369      .01288536      .01052352      .00268776     -.00038006      .00261223      .00023088
 ref:   4      .00182934      .00383613     -.00065324      .00216747     -.00073245     -.00025343     -.00013267      .00397548
 ref:   5      .61608192     -.25902062      .15077550     -.01945599      .00682487     -.00562510      .00918944     -.00366220
 ref:   6      .35167580     -.21006502      .11274631      .04432322     -.04035076      .07851754     -.04431978     -.01919761
 ref:   7      .32347047      .72815163     -.40896506     -.02909343     -.02603447      .02961726      .05251356      .01925707
 ref:   8     -.52359530      .11652973     -.07604304      .00806145      .01320393      .04968547     -.03857676     -.02424808
 ref:   9      .18875576      .17729495     -.20280131      .05780365      .10376219     -.03504518     -.11508279     -.05491100
 ref:  10      .14050846      .13420246     -.09598524      .00703951      .04358558      .00099014     -.02993946     -.00135845

 trial vector basis is being transformed.  new dimension:  10

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 18  1    -93.3414567734  9.7091E-09  0.0000E+00  7.4024E-04  1.0000E-03
 mr-sdci # 18  2    -93.3414565722  1.6221E-09  0.0000E+00  7.6652E-04  1.0000E-03
 mr-sdci # 18  3    -93.3414565576  6.5505E-11  0.0000E+00  7.8221E-04  1.0000E-03
 mr-sdci # 18  4    -93.3414563509  1.3768E-10  0.0000E+00  8.3228E-04  1.0000E-03
 mr-sdci # 18  5    -93.1685341678  3.1381E-06  0.0000E+00  4.6033E-04  1.0000E-03
 mr-sdci # 18  6    -93.1547869758  1.0457E-05  1.8289E-02  8.1936E-02  1.0000E-03
 mr-sdci # 18  7    -93.1542808633  3.3484E-06  0.0000E+00  8.3659E-02  1.0000E-03
 mr-sdci # 18  8    -93.1535336538  7.7595E-07  0.0000E+00  8.5905E-02  1.0000E-03
 mr-sdci # 18  9    -93.1222163178  4.3399E-07  0.0000E+00  9.7701E-02  1.0000E-03
 mr-sdci # 18 10    -93.1201407443  7.8728E-05  0.0000E+00  8.4705E-02  1.0000E-03
 mr-sdci # 18 11    -93.1195375986  3.4524E-03  0.0000E+00  6.3382E-02  1.0000E-04
 mr-sdci # 18 12    -92.9046965721  1.1700E-02  0.0000E+00  1.6542E-01  1.0000E-04
 mr-sdci # 18 13    -92.8904183444  8.9560E-02  0.0000E+00  2.4995E-01  1.0000E-04
 mr-sdci # 18 14    -92.7732555704  2.4176E-02  0.0000E+00  3.0818E-01  1.0000E-04
 mr-sdci # 18 15    -92.6859470234  2.3286E-02  0.0000E+00  2.5215E-01  1.0000E-04
 mr-sdci # 18 16    -92.6617928849 -3.4525E-02  0.0000E+00  2.2258E-01  1.0000E-04
 
 root number  6 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .34    .12    .00    .34
    2   25    0    .19    .06    .00    .19
    3   26    0   5.15    .20    .00   5.15
    4   11    0   2.22   1.24    .00   2.22
    5   15    0   4.55   1.68    .00   4.55
    6   16    0   2.61    .90    .00   2.61
    7    1    0    .47    .26    .00    .47
    8    5    0   2.18    .79    .00   2.17
    9    6    0   8.22    .73    .00   8.22
   10    7    0   1.72    .20    .00   1.72
   11   75    0    .09    .05    .00    .08
   12   45    0    .20    .10    .00    .20
   13   46    0    .58    .09    .00    .58
   14   47    0    .23    .04    .00    .23
   15   81    0    .53    .00    .00    .52
   16   82    0   1.60    .00    .00   1.59
   17  101    0    .28    .00    .00    .27
   18   83    0   1.65    .00    .00   1.65
   19  102    0   4.12    .00    .00   4.12
   20   84    0    .43    .00    .00    .43
   21   91    0    .63    .00    .00    .62
   22   93    0   2.28    .00    .00   2.27
   23   94    0   1.17    .00    .00   1.17
   24  103    0   2.96    .00    .00   2.95
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .060000
time for cinew                         1.430000
time for eigenvalue solver              .000000
time for vector access                  .000000
================================================================
time spent in mult:                    44.4000s 
time spent in multnx:                  44.3200s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.4600s 
time for vector access in mult:          .0100s 
total time per CI iteration:           45.9000s 

          starting ci iteration  19

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     -.09502376      .14214833      .40180026      .88243560      .00358688      .04682056     -.09977478     -.01376676
 ref:   2      .97148624      .08796627     -.06156751      .11846793     -.08994722      .05604723      .02323453     -.00193330
 ref:   3      .11227152     -.87511159      .43465936     -.04482701     -.05888862     -.07964229     -.04225606      .01770257
 ref:   4      .06273422      .41903287      .78435809     -.41790257     -.00866287     -.02004337      .00430292     -.10918286
 ref:   5     -.00005737     -.00055369     -.00052505     -.00155211      .00124243      .05681048     -.07000716     -.01439456
 ref:   6      .00017716      .00022480     -.00002945     -.00093402      .00259398      .01790287     -.04950003      .02321302
 ref:   7     -.00072881     -.00026587      .00125579     -.00061752     -.01375346      .06068757      .02484964      .05295779
 ref:   8     -.00342390      .00513268      .01437933      .03172279     -.00864950     -.10865978      .25613646      .03473396
 ref:   9     -.02563177     -.03043428      .01865783      .00055683     -.05107902      .23426289      .19049479     -.03690637
 ref:  10      .00134776      .02696832      .03352828     -.01405667     -.00899346      .00551858     -.00684383      .37671052

                ci   9         ci  10         ci  11
 ref:   1      .00391853     -.00268122      .00816618
 ref:   2      .00026702     -.00916643      .01212022
 ref:   3     -.00047070     -.00904810     -.01858037
 ref:   4     -.00163913     -.00363773      .00291226
 ref:   5     -.61426038      .26404640     -.02933127
 ref:   6     -.34985794      .21312277     -.02145573
 ref:   7     -.33034075     -.72565639     -.04158813
 ref:   8      .52434812     -.11963892     -.04154119
 ref:   9     -.19556118     -.18003339      .24410834
 ref:  10     -.14127852     -.13260812     -.05435770

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 19  1    -93.3414567774  4.0858E-09  0.0000E+00  7.3688E-04  1.0000E-03
 mr-sdci # 19  2    -93.3414565896  1.7362E-08  0.0000E+00  7.6179E-04  1.0000E-03
 mr-sdci # 19  3    -93.3414565615  3.8686E-09  0.0000E+00  7.8093E-04  1.0000E-03
 mr-sdci # 19  4    -93.3414563675  1.6633E-08  0.0000E+00  8.2750E-04  1.0000E-03
 mr-sdci # 19  5    -93.1685341766  8.7579E-09  0.0000E+00  4.6278E-04  1.0000E-03
 mr-sdci # 19  6    -93.1670999637  1.2313E-02  1.1493E-03  1.9636E-02  1.0000E-03
 mr-sdci # 19  7    -93.1542809173  5.3991E-08  0.0000E+00  8.3654E-02  1.0000E-03
 mr-sdci # 19  8    -93.1535338117  1.5786E-07  0.0000E+00  8.5900E-02  1.0000E-03
 mr-sdci # 19  9    -93.1222372005  2.0883E-05  0.0000E+00  9.7625E-02  1.0000E-03
 mr-sdci # 19 10    -93.1201567336  1.5989E-05  0.0000E+00  8.4663E-02  1.0000E-03
 mr-sdci # 19 11    -92.8057754779 -3.1376E-01  0.0000E+00  2.6985E-01  1.0000E-04
 
 root number  6 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .34    .12    .00    .34
    2   25    0    .20    .06    .00    .20
    3   26    0   5.17    .20    .00   5.17
    4   11    0   2.22   1.26    .00   2.21
    5   15    0   4.56   1.70    .00   4.55
    6   16    0   2.65    .92    .00   2.65
    7    1    0    .49    .27    .00    .49
    8    5    0   2.26    .83    .00   2.25
    9    6    0   8.29    .74    .00   8.28
   10    7    0   1.72    .20    .00   1.72
   11   75    0    .08    .06    .00    .08
   12   45    0    .21    .10    .00    .20
   13   46    0    .58    .08    .00    .58
   14   47    0    .23    .04    .00    .23
   15   81    0    .53    .00    .00    .53
   16   82    0   1.61    .00    .00   1.60
   17  101    0    .27    .00    .00    .27
   18   83    0   1.66    .00    .00   1.65
   19  102    0   4.10    .00    .00   4.10
   20   84    0    .41    .00    .00    .41
   21   91    0    .60    .00    .00    .60
   22   93    0   2.21    .00    .00   2.20
   23   94    0   1.15    .00    .00   1.15
   24  103    0   2.95    .00    .00   2.95
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .040000
time for cinew                          .600000
time for eigenvalue solver              .000000
time for vector access                  .000000
================================================================
time spent in mult:                    44.4900s 
time spent in multnx:                  44.4100s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.5800s 
time for vector access in mult:          .0100s 
total time per CI iteration:           45.1600s 

          starting ci iteration  20

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1      .07542062     -.19063740      .36801663      .88989025     -.00387022      .04681617      .09950927      .01402157
 ref:   2      .86561584      .46841953      .01378031      .02127769      .08961208      .05591788     -.02352787      .00199115
 ref:   3     -.45258060      .82773083     -.10786214      .26031319      .05936238     -.07843763      .04268937     -.01776528
 ref:   4     -.09761569      .16874421      .90670274     -.33056200      .00878400     -.01999339     -.00438861      .10914013
 ref:   5     -.00052948      .00049030     -.00066397     -.00144695     -.00162930      .06191400      .07009814      .01446678
 ref:   6      .00004982      .00014520      .00014636     -.00094589     -.00270026      .01767628      .04948365     -.02311013
 ref:   7     -.00125239      .00039456      .00092065     -.00020944      .01337192      .06242314     -.02492171     -.05294845
 ref:   8      .00272622     -.00689262      .01318833      .03196829      .00931848     -.11002199     -.25566592     -.03531219
 ref:   9     -.04009670      .01222001     -.00158833      .01346469      .04957930      .24409194     -.19098944      .03670698
 ref:  10     -.00015238     -.00045888      .04346560     -.01266579      .00900403      .00039888      .00713950     -.37657140

                ci   9         ci  10         ci  11         ci  12
 ref:   1      .00437824      .00277834      .00763330     -.00840893
 ref:   2      .00083247      .00931481      .00988607     -.01244896
 ref:   3     -.00099694      .00885084     -.01224141      .01897948
 ref:   4     -.00179287      .00359282     -.00383263     -.00279118
 ref:   5     -.61688629     -.25743118     -.00147021      .02970712
 ref:   6     -.35114902     -.20905697      .04764847      .01984815
 ref:   7     -.32255801      .72914101     -.01319910      .04219982
 ref:   8      .52399775      .11351500     -.05194002      .04320112
 ref:   9     -.19513035      .18158495     -.13771561     -.23884344
 ref:  10     -.13794519      .13474694      .09523224      .05087024

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 20  1    -93.3414568479  7.0457E-08  0.0000E+00  7.0646E-04  1.0000E-03
 mr-sdci # 20  2    -93.3414567122  1.2264E-07  0.0000E+00  7.7323E-04  1.0000E-03
 mr-sdci # 20  3    -93.3414565707  9.1611E-09  0.0000E+00  7.7163E-04  1.0000E-03
 mr-sdci # 20  4    -93.3414564122  4.4717E-08  0.0000E+00  8.1857E-04  1.0000E-03
 mr-sdci # 20  5    -93.1685341978  2.1278E-08  0.0000E+00  4.6408E-04  1.0000E-03
 mr-sdci # 20  6    -93.1681207061  1.0207E-03  3.0580E-04  9.0780E-03  1.0000E-03
 mr-sdci # 20  7    -93.1542845675  3.6502E-06  0.0000E+00  8.3612E-02  1.0000E-03
 mr-sdci # 20  8    -93.1535342741  4.6244E-07  0.0000E+00  8.5887E-02  1.0000E-03
 mr-sdci # 20  9    -93.1223035504  6.6350E-05  0.0000E+00  9.7306E-02  1.0000E-03
 mr-sdci # 20 10    -93.1201643699  7.6363E-06  0.0000E+00  8.4662E-02  1.0000E-03
 mr-sdci # 20 11    -92.9495436378  1.4377E-01  0.0000E+00  2.2895E-01  1.0000E-04
 mr-sdci # 20 12    -92.8055735362 -9.9123E-02  0.0000E+00  2.6839E-01  1.0000E-04
 
 root number  6 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .34    .12    .00    .34
    2   25    0    .19    .07    .00    .19
    3   26    0   5.14    .19    .00   5.13
    4   11    0   2.21   1.26    .00   2.20
    5   15    0   4.56   1.68    .00   4.55
    6   16    0   2.67    .94    .00   2.67
    7    1    0    .49    .29    .00    .49
    8    5    0   2.27    .83    .00   2.27
    9    6    0   8.28    .75    .00   8.28
   10    7    0   1.72    .20    .00   1.71
   11   75    0    .09    .05    .00    .08
   12   45    0    .20    .10    .00    .20
   13   46    0    .57    .08    .00    .57
   14   47    0    .22    .04    .00    .22
   15   81    0    .51    .00    .00    .51
   16   82    0   1.54    .00    .00   1.53
   17  101    0    .26    .00    .00    .26
   18   83    0   1.60    .00    .00   1.59
   19  102    0   3.97    .00    .00   3.97
   20   84    0    .43    .00    .00    .43
   21   91    0    .63    .00    .00    .63
   22   93    0   2.27    .00    .00   2.27
   23   94    0   1.18    .00    .00   1.17
   24  103    0   2.95    .00    .00   2.95
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .050000
time for cinew                          .570000
time for eigenvalue solver              .000000
time for vector access                  .000000
================================================================
time spent in mult:                    44.2900s 
time spent in multnx:                  44.2100s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.6000s 
time for vector access in mult:          .0100s 
total time per CI iteration:           44.9300s 

          starting ci iteration  21

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1      .19759954     -.20961286      .36370280     -.86839451     -.00437250     -.04687619      .09911906     -.01533883
 ref:   2      .71755884      .67394124     -.01520446     -.00575921      .08901541     -.05648835     -.02381859     -.00193762
 ref:   3     -.64279324      .68248737      .02785774     -.29936380      .06018864      .07725069      .04325565      .01756773
 ref:   4     -.04709086      .07380592      .91437801      .35444669      .00900083      .02010412     -.00553452     -.10899954
 ref:   5     -.00064386      .00038029     -.00057418      .00137732     -.00217774     -.05526934      .06842330     -.01650504
 ref:   6      .00001615      .00015256      .00013367      .00094419     -.00287115     -.01660038      .04949037      .02233775
 ref:   7     -.00112583     -.00003435      .00107737      .00018192      .01278970     -.05753579     -.02556890      .05219480
 ref:   8      .00724054     -.00759876      .01305366     -.03121019      .01064957      .11895330     -.25666524      .03702444
 ref:   9     -.04143395      .00128068      .00366682     -.01504743      .04672186     -.25880975     -.18910774     -.03256063
 ref:  10      .00431216     -.00356588      .04261895      .01418386      .00894068     -.00392195      .01193421      .37686959

                ci   9         ci  10         ci  11         ci  12         ci  13
 ref:   1     -.00478793      .00283268      .00854286     -.00264727      .00859938
 ref:   2     -.00128748      .00939846      .01078257     -.00282962      .01244360
 ref:   3      .00138871      .00874473     -.01364988      .00275092     -.01896966
 ref:   4      .00200662      .00354994     -.00450184      .00269025      .00270577
 ref:   5      .61289792     -.25051813      .18363034      .10938002     -.10065676
 ref:   6      .35102924     -.20551582      .07115018     -.01835308     -.02693442
 ref:   7      .31234503      .73287240      .10387893      .07468735     -.08959809
 ref:   8     -.52858489      .10985349      .09281109      .12525437     -.11044826
 ref:   9      .20285425      .18095822     -.30476174     -.00045193      .31879456
 ref:  10      .13757324      .13574004     -.00619952     -.12931108     -.00918670

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 21  1    -93.3414569621  1.1415E-07  0.0000E+00  6.6281E-04  1.0000E-03
 mr-sdci # 21  2    -93.3414567302  1.7997E-08  0.0000E+00  7.6739E-04  1.0000E-03
 mr-sdci # 21  3    -93.3414565805  9.8158E-09  0.0000E+00  7.6969E-04  1.0000E-03
 mr-sdci # 21  4    -93.3414564150  2.7672E-09  0.0000E+00  8.1735E-04  1.0000E-03
 mr-sdci # 21  5    -93.1685342264  2.8584E-08  0.0000E+00  4.6458E-04  1.0000E-03
 mr-sdci # 21  6    -93.1683784796  2.5777E-04  9.6049E-05  5.4736E-03  1.0000E-03
 mr-sdci # 21  7    -93.1542947881  1.0221E-05  0.0000E+00  8.3573E-02  1.0000E-03
 mr-sdci # 21  8    -93.1535415249  7.2507E-06  0.0000E+00  8.5886E-02  1.0000E-03
 mr-sdci # 21  9    -93.1223794844  7.5934E-05  0.0000E+00  9.7019E-02  1.0000E-03
 mr-sdci # 21 10    -93.1201690857  4.7158E-06  0.0000E+00  8.4637E-02  1.0000E-03
 mr-sdci # 21 11    -93.0462560964  9.6712E-02  0.0000E+00  1.5672E-01  1.0000E-04
 mr-sdci # 21 12    -92.9011776545  9.5604E-02  0.0000E+00  2.5616E-01  1.0000E-04
 mr-sdci # 21 13    -92.7855232637 -1.0490E-01  0.0000E+00  2.6504E-01  1.0000E-04
 
 root number  6 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .33    .13    .00    .33
    2   25    0    .20    .07    .00    .19
    3   26    0   5.15    .19    .00   5.15
    4   11    0   2.20   1.24    .00   2.20
    5   15    0   4.55   1.66    .00   4.54
    6   16    0   2.65    .92    .00   2.65
    7    1    0    .49    .27    .00    .49
    8    5    0   2.27    .84    .00   2.26
    9    6    0   8.21    .71    .00   8.21
   10    7    0   1.68    .21    .00   1.68
   11   75    0    .08    .06    .00    .08
   12   45    0    .21    .10    .00    .20
   13   46    0    .57    .08    .00    .57
   14   47    0    .24    .04    .00    .23
   15   81    0    .53    .00    .00    .53
   16   82    0   1.61    .00    .00   1.61
   17  101    0    .27    .00    .00    .27
   18   83    0   1.65    .00    .00   1.64
   19  102    0   4.13    .00    .00   4.13
   20   84    0    .43    .00    .00    .43
   21   91    0    .63    .00    .00    .63
   22   93    0   2.28    .00    .00   2.27
   23   94    0   1.17    .00    .00   1.17
   24  103    0   2.95    .00    .00   2.95
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .060000
time for cinew                          .700000
time for eigenvalue solver              .000000
time for vector access                  .000000
================================================================
time spent in mult:                    44.4800s 
time spent in multnx:                  44.4100s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.5200s 
time for vector access in mult:          .0000s 
total time per CI iteration:           45.2400s 

          starting ci iteration  22

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1      .19769018      .20956824     -.36181532      .86917286     -.00606168     -.04672188     -.09851144     -.01705561
 ref:   2      .71735917     -.67406218      .01839450      .00701418      .08694256     -.05922888      .02424778     -.00174680
 ref:   3     -.64297338     -.68180345     -.02341882      .30091188      .06290633      .07446328     -.04403722      .01711891
 ref:   4     -.04729351     -.07896970     -.91519234     -.35119060      .00972095      .01978723      .00706293     -.10881528
 ref:   5     -.00064438     -.00038454      .00056766     -.00138178     -.00424259     -.05659449     -.06856344     -.01737916
 ref:   6      .00001600     -.00015507     -.00013604     -.00094335     -.00344949     -.01612524     -.04948988      .02139927
 ref:   7     -.00112639      .00002448     -.00108191     -.00018173      .01060810     -.05989534      .02394471      .05297301
 ref:   8      .00724333      .00759138     -.01299118      .03123352      .01486963      .11726909      .25479993      .04161563
 ref:   9     -.04143324     -.00125265     -.00360108      .01506820      .03728057     -.26186417      .18886383     -.02920220
 ref:  10      .00430527      .00333070     -.04268180     -.01404495      .00884585     -.00321414     -.01697852      .37622837

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14
 ref:   1      .00580869     -.00276625     -.01146791     -.00063999      .00284073     -.00809475
 ref:   2      .00218933     -.00932041     -.01233752     -.00009659      .00442862     -.01162497
 ref:   3     -.00251196     -.00883419      .01435151     -.00116826     -.00673377      .01819150
 ref:   4     -.00262036     -.00359177      .00653375      .00117992      .00279234     -.00178834
 ref:   5     -.60645915      .25399342     -.11921164      .22137342     -.11710553      .01203296
 ref:   6     -.34540634      .20764395     -.07976729      .02233459     -.05155931     -.00498513
 ref:   7     -.31492344     -.73123449     -.04786264      .15578126     -.10169012      .01496114
 ref:   8      .52233232     -.11291764      .06296723      .21615727     -.11066405      .02474786
 ref:   9     -.21252966     -.18078365      .15340467     -.22227587      .35137368     -.11700813
 ref:  10     -.13386829     -.13468595     -.05275392     -.12605191     -.04298160     -.00164134

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 22  1    -93.3414569621  5.7803E-12  0.0000E+00  6.6278E-04  1.0000E-03
 mr-sdci # 22  2    -93.3414567311  8.4254E-10  0.0000E+00  7.6708E-04  1.0000E-03
 mr-sdci # 22  3    -93.3414565811  6.3212E-10  0.0000E+00  7.6926E-04  1.0000E-03
 mr-sdci # 22  4    -93.3414564155  4.9642E-10  0.0000E+00  8.1710E-04  1.0000E-03
 mr-sdci # 22  5    -93.1685343097  8.3255E-08  0.0000E+00  4.7684E-04  1.0000E-03
 mr-sdci # 22  6    -93.1684889114  1.1043E-04  3.1655E-05  3.1336E-03  1.0000E-03
 mr-sdci # 22  7    -93.1543199531  2.5165E-05  0.0000E+00  8.3419E-02  1.0000E-03
 mr-sdci # 22  8    -93.1535472288  5.7039E-06  0.0000E+00  8.5842E-02  1.0000E-03
 mr-sdci # 22  9    -93.1225093283  1.2984E-04  0.0000E+00  9.6189E-02  1.0000E-03
 mr-sdci # 22 10    -93.1201700332  9.4745E-07  0.0000E+00  8.4644E-02  1.0000E-03
 mr-sdci # 22 11    -93.1031044187  5.6848E-02  0.0000E+00  9.6009E-02  1.0000E-04
 mr-sdci # 22 12    -92.9400106140  3.8833E-02  0.0000E+00  2.2695E-01  1.0000E-04
 mr-sdci # 22 13    -92.8212377224  3.5714E-02  0.0000E+00  2.7701E-01  1.0000E-04
 mr-sdci # 22 14    -92.7593535665 -1.3902E-02  0.0000E+00  2.2580E-01  1.0000E-04
 
 root number  6 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .34    .12    .00    .34
    2   25    0    .19    .06    .00    .19
    3   26    0   5.15    .19    .00   5.15
    4   11    0   2.20   1.24    .00   2.20
    5   15    0   4.56   1.66    .00   4.55
    6   16    0   2.66    .91    .00   2.66
    7    1    0    .49    .28    .00    .49
    8    5    0   2.26    .82    .00   2.26
    9    6    0   8.29    .74    .00   8.28
   10    7    0   1.73    .20    .00   1.73
   11   75    0    .08    .06    .00    .08
   12   45    0    .20    .10    .00    .20
   13   46    0    .57    .08    .00    .57
   14   47    0    .23    .04    .00    .22
   15   81    0    .52    .00    .00    .52
   16   82    0   1.61    .00    .00   1.60
   17  101    0    .28    .00    .00    .27
   18   83    0   1.66    .00    .00   1.66
   19  102    0   4.13    .00    .00   4.13
   20   84    0    .43    .00    .00    .43
   21   91    0    .63    .00    .00    .63
   22   93    0   2.28    .00    .00   2.27
   23   94    0   1.17    .00    .00   1.17
   24  103    0   2.95    .00    .00   2.95
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .080000
time for cinew                          .730000
time for eigenvalue solver              .000000
time for vector access                  .000000
================================================================
time spent in mult:                    44.6100s 
time spent in multnx:                  44.5500s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.5000s 
time for vector access in mult:          .0000s 
total time per CI iteration:           45.4300s 

          starting ci iteration  23

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     -.19843922     -.20985989     -.34240291     -.87676243      .00926792     -.04632921     -.09795009     -.01892933
 ref:   2     -.71614654      .67472976      .03280652     -.01222016     -.08268571     -.06493223      .02452543     -.00140791
 ref:   3      .64393555      .67780996      .00172278     -.30868235     -.06784544      .06975190     -.04468758      .01645860
 ref:   4      .04939603      .10316799     -.92252613      .32441571     -.01106502      .01915902      .00892231     -.10862645
 ref:   5      .00064160      .00036288      .00057023      .00136419      .00760353     -.05129015     -.06164257     -.02131123
 ref:   6     -.00001595      .00015969     -.00014692      .00092689      .00433648     -.01383865     -.04696452      .01931050
 ref:   7      .00112526     -.00001686     -.00106066      .00012828     -.00682087     -.05716912      .02717152      .05169325
 ref:   8     -.00727456     -.00762397     -.01226281     -.03153448     -.02328718      .11981850      .25742415      .04482702
 ref:   9      .04142999      .00112897     -.00312038     -.01518110     -.01857161     -.26998039      .18146631     -.02253805
 ref:  10     -.00422170     -.00220563     -.04313893      .01287951     -.00850228     -.00478511     -.02429834      .37606581

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15
 ref:   1      .00484602     -.01124001      .00480271     -.00725067     -.00597728      .00341603      .00462073
 ref:   2      .00142907     -.00844965      .01103413     -.00822662     -.00787261      .00394372      .00819119
 ref:   3     -.00142117      .01358555      .00647929      .00852839      .01056158     -.00518725     -.01504567
 ref:   4     -.00204873      .00701341      .00239504      .00391256      .00077267     -.00273074      .00521314
 ref:   5     -.62410341     -.19395655     -.21780649      .27774184     -.01481220      .05604574      .03118659
 ref:   6     -.35505916     -.11117261     -.18766806      .05113488      .03123054      .02944403      .01894420
 ref:   7     -.31705735      .04171672      .73791293      .22967799      .01968448      .04204316      .02812678
 ref:   8      .51809510     -.05952764      .11940545      .34996987      .03990490      .02231482      .06284912
 ref:   9     -.18708456      .31356752      .12976604     -.37397702     -.12289384     -.17933350      .11201065
 ref:  10     -.13400051      .01210451      .13635699     -.13509916      .05897866      .04783412     -.04557944

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 23  1    -93.3414569621  8.2075E-11  0.0000E+00  6.6240E-04  1.0000E-03
 mr-sdci # 23  2    -93.3414567334  2.2698E-09  0.0000E+00  7.6633E-04  1.0000E-03
 mr-sdci # 23  3    -93.3414565862  5.0447E-09  0.0000E+00  7.6719E-04  1.0000E-03
 mr-sdci # 23  4    -93.3414564198  4.3101E-09  0.0000E+00  8.1505E-04  1.0000E-03
 mr-sdci # 23  5    -93.1685344227  1.1307E-07  0.0000E+00  4.9080E-04  1.0000E-03
 mr-sdci # 23  6    -93.1685186806  2.9769E-05  7.8043E-06  1.6039E-03  1.0000E-03
 mr-sdci # 23  7    -93.1543542792  3.4326E-05  0.0000E+00  8.3120E-02  1.0000E-03
 mr-sdci # 23  8    -93.1535531265  5.8977E-06  0.0000E+00  8.5774E-02  1.0000E-03
 mr-sdci # 23  9    -93.1225165895  7.2612E-06  0.0000E+00  9.6898E-02  1.0000E-03
 mr-sdci # 23 10    -93.1215533020  1.3833E-03  0.0000E+00  6.3622E-02  1.0000E-03
 mr-sdci # 23 11    -93.1201271683  1.7023E-02  0.0000E+00  8.4396E-02  1.0000E-04
 mr-sdci # 23 12    -92.9769326743  3.6922E-02  0.0000E+00  1.9927E-01  1.0000E-04
 mr-sdci # 23 13    -92.9077594510  8.6522E-02  0.0000E+00  2.4204E-01  1.0000E-04
 mr-sdci # 23 14    -92.7974346509  3.8081E-02  0.0000E+00  2.1858E-01  1.0000E-04
 mr-sdci # 23 15    -92.7064483792  2.0501E-02  0.0000E+00  2.5720E-01  1.0000E-04
 
 root number  6 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .33    .12    .00    .33
    2   25    0    .19    .06    .00    .19
    3   26    0   5.01    .18    .00   5.01
    4   11    0   2.20   1.23    .00   2.19
    5   15    0   4.56   1.68    .00   4.55
    6   16    0   2.65    .92    .00   2.65
    7    1    0    .50    .28    .00    .49
    8    5    0   2.26    .83    .00   2.26
    9    6    0   8.29    .74    .00   8.28
   10    7    0   1.73    .20    .00   1.73
   11   75    0    .08    .06    .00    .08
   12   45    0    .21    .10    .00    .20
   13   46    0    .57    .09    .00    .57
   14   47    0    .24    .04    .00    .24
   15   81    0    .53    .00    .00    .53
   16   82    0   1.60    .00    .00   1.60
   17  101    0    .28    .00    .00    .27
   18   83    0   1.66    .00    .00   1.66
   19  102    0   4.14    .00    .00   4.14
   20   84    0    .43    .00    .00    .43
   21   91    0    .63    .00    .00    .63
   22   93    0   2.28    .00    .00   2.27
   23   94    0   1.18    .00    .00   1.18
   24  103    0   2.95    .00    .00   2.95
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .090000
time for cinew                          .830000
time for eigenvalue solver              .000000
time for vector access                  .000000
================================================================
time spent in mult:                    44.5000s 
time spent in multnx:                  44.4300s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.5300s 
time for vector access in mult:          .0000s 
total time per CI iteration:           45.4200s 

          starting ci iteration  24

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1      .20210663     -.21013909     -.29421410     -.89319896     -.01253837      .04557531      .09759963     -.02019409
 ref:   2      .71340484      .67724671      .04022515     -.01115289      .07786639      .07063693     -.02458175     -.00114150
 ref:   3     -.64473997      .67142059      .04257282     -.31789874      .07262075     -.06471975      .04496967      .01598064
 ref:   4     -.06208846      .12547803     -.93775381      .26533581      .01240478     -.01836471     -.01017733     -.10847000
 ref:   5     -.00064959      .00036285      .00049254      .00139577     -.01109419      .04988075      .05965122     -.02333432
 ref:   6      .00001147      .00016829     -.00018788      .00090366     -.00533034      .01361890      .04704639      .01861917
 ref:   7     -.00114271      .00000535     -.00106910      .00006792      .00284800      .05692736     -.02760277      .05124769
 ref:   8      .00740970     -.00763623     -.01052639     -.03212651      .03175282     -.11794935     -.25685357      .04792577
 ref:   9     -.04133828      .00083102     -.00165048     -.01566427     -.00104828      .27285376     -.17739188     -.01715267
 ref:  10      .00367369     -.00114829     -.04394209      .01023069      .00816892      .00522100      .02827804      .37534004

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1      .01178335      .00508238      .00379430     -.00560817     -.00893629      .00034887     -.00218172      .00435206
 ref:   2      .00747449      .00160766      .01020594     -.00531608     -.01488094     -.00259053     -.00060634      .00612205
 ref:   3     -.01081636     -.00168957      .00769677      .00519863      .01724295      .00172307      .00110665     -.01343776
 ref:   4     -.00712041     -.00219204      .00300110      .00314119      .00382111      .00173134      .00253784      .00588919
 ref:   5      .18503801     -.62015783     -.23473358      .27940998      .02301162     -.00521927     -.05167760      .04168231
 ref:   6      .08508192     -.35290520     -.19743530      .06565977     -.04993141     -.09497654      .00769157     -.01278264
 ref:   7      .04316695     -.31713983      .73867591      .21296194      .10092254      .02189054     -.05624870      .05828375
 ref:   8      .04511792      .51928100      .11387359      .35086223      .04263849     -.07146305      .00872985      .04419399
 ref:   9     -.33072836     -.19352836      .15614182     -.33325717     -.24372027      .00996447      .22773879      .03390089
 ref:  10      .01645333     -.13405537      .13703630     -.14579420      .04175605     -.02589087     -.05761955     -.02948079

 trial vector basis is being transformed.  new dimension:  10

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 24  1    -93.3414569636  1.4489E-09  0.0000E+00  6.6517E-04  1.0000E-03
 mr-sdci # 24  2    -93.3414567342  8.2130E-10  0.0000E+00  7.6670E-04  1.0000E-03
 mr-sdci # 24  3    -93.3414565974  1.1215E-08  0.0000E+00  7.6299E-04  1.0000E-03
 mr-sdci # 24  4    -93.3414564293  9.5588E-09  0.0000E+00  8.1252E-04  1.0000E-03
 mr-sdci # 24  5    -93.1685345431  1.2033E-07  0.0000E+00  4.6477E-04  1.0000E-03
 mr-sdci # 24  6    -93.1685249861  6.3056E-06  0.0000E+00  7.3080E-04  1.0000E-03
 mr-sdci # 24  7    -93.1543675849  1.3306E-05  1.9131E-02  8.2992E-02  1.0000E-03
 mr-sdci # 24  8    -93.1535602096  7.0832E-06  0.0000E+00  8.5767E-02  1.0000E-03
 mr-sdci # 24  9    -93.1278100069  5.2934E-03  0.0000E+00  4.8304E-02  1.0000E-03
 mr-sdci # 24 10    -93.1225162387  9.6294E-04  0.0000E+00  9.6768E-02  1.0000E-03
 mr-sdci # 24 11    -93.1201408559  1.3688E-05  0.0000E+00  8.4637E-02  1.0000E-04
 mr-sdci # 24 12    -92.9777921401  8.5947E-04  0.0000E+00  1.9715E-01  1.0000E-04
 mr-sdci # 24 13    -92.9585781331  5.0819E-02  0.0000E+00  1.8651E-01  1.0000E-04
 mr-sdci # 24 14    -92.8650255987  6.7591E-02  0.0000E+00  2.8023E-01  1.0000E-04
 mr-sdci # 24 15    -92.7866320366  8.0184E-02  0.0000E+00  2.2977E-01  1.0000E-04
 mr-sdci # 24 16    -92.6858621731  2.4069E-02  0.0000E+00  2.5578E-01  1.0000E-04
 
 root number  7 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .34    .12    .00    .34
    2   25    0    .19    .06    .00    .19
    3   26    0   5.15    .20    .00   5.14
    4   11    0   2.22   1.23    .00   2.22
    5   15    0   4.54   1.67    .00   4.54
    6   16    0   2.67    .91    .00   2.66
    7    1    0    .49    .28    .00    .49
    8    5    0   2.27    .83    .00   2.26
    9    6    0   8.07    .71    .00   8.07
   10    7    0   1.66    .19    .00   1.66
   11   75    0    .08    .06    .00    .08
   12   45    0    .21    .09    .00    .20
   13   46    0    .57    .09    .00    .57
   14   47    0    .24    .03    .00    .24
   15   81    0    .51    .00    .00    .51
   16   82    0   1.61    .00    .00   1.61
   17  101    0    .27    .00    .00    .27
   18   83    0   1.66    .00    .00   1.66
   19  102    0   4.13    .00    .00   4.13
   20   84    0    .43    .00    .00    .43
   21   91    0    .64    .00    .00    .63
   22   93    0   2.26    .00    .00   2.26
   23   94    0   1.17    .00    .00   1.17
   24  103    0   2.95    .00    .00   2.95
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .070000
time for cinew                         1.400000
time for eigenvalue solver              .000000
time for vector access                  .000000
================================================================
time spent in mult:                    44.3300s 
time spent in multnx:                  44.2800s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.4700s 
time for vector access in mult:          .0100s 
total time per CI iteration:           45.8100s 

          starting ci iteration  25

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     -.17371219     -.22111262      .31812954     -.88830200     -.01245052      .04602754     -.09675535     -.01951971
 ref:   2     -.71614826      .67351993     -.03516709     -.04019863      .07788769      .07048230      .02479338     -.00131126
 ref:   3      .65040550      .67142058     -.03546934     -.30703503      .07261581     -.06455235     -.04485346      .01629085
 ref:   4      .05713170      .12667306      .93041530      .29051937      .01238815     -.01841943      .01031982     -.10853716
 ref:   5      .00059794      .00038315     -.00053323      .00141366     -.01102461      .05018828     -.06475799     -.02292429
 ref:   6     -.00003996      .00017769      .00016385      .00091041     -.00529382      .01382676     -.04469450      .01894462
 ref:   7      .00113840      .00001089      .00106024      .00014382      .00286707      .05684778      .01831203      .05105197
 ref:   8     -.00639356     -.00803043      .01138553     -.03194175      .03154895     -.11895513      .21854549      .04613294
 ref:   9      .04177707      .00087546      .00188048     -.01439820     -.00101198      .27209468      .16741396     -.01837832
 ref:  10     -.00380819     -.00114780      .04366847      .01130344      .00819058      .00534886     -.02835228      .37552528

                ci   9         ci  10         ci  11
 ref:   1     -.00802248     -.00356075     -.01623838
 ref:   2     -.00839795     -.00186967      .00154831
 ref:   3      .01250147      .00219952     -.00332970
 ref:   4      .00683764      .00201698     -.00239482
 ref:   5     -.17749937      .62244335      .03173716
 ref:   6     -.07995879      .35434893     -.00522669
 ref:   7     -.04233081      .31664929      .06953247
 ref:   8     -.06201519     -.52341798      .16961954
 ref:   9      .32337881      .18682691      .15647540
 ref:  10     -.01457621      .13440088      .01866137

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 25  1    -93.3414569694  5.8045E-09  0.0000E+00  6.6318E-04  1.0000E-03
 mr-sdci # 25  2    -93.3414567344  2.2731E-10  0.0000E+00  7.6644E-04  1.0000E-03
 mr-sdci # 25  3    -93.3414565978  3.8555E-10  0.0000E+00  7.6280E-04  1.0000E-03
 mr-sdci # 25  4    -93.3414564688  3.9490E-08  0.0000E+00  7.9934E-04  1.0000E-03
 mr-sdci # 25  5    -93.1685345438  7.2451E-10  0.0000E+00  4.6524E-04  1.0000E-03
 mr-sdci # 25  6    -93.1685250215  3.5381E-08  0.0000E+00  7.2245E-04  1.0000E-03
 mr-sdci # 25  7    -93.1670326998  1.2665E-02  1.2742E-03  2.0704E-02  1.0000E-03
 mr-sdci # 25  8    -93.1535602505  4.0912E-08  0.0000E+00  8.5764E-02  1.0000E-03
 mr-sdci # 25  9    -93.1279264681  1.1646E-04  0.0000E+00  4.6789E-02  1.0000E-03
 mr-sdci # 25 10    -93.1225383704  2.2132E-05  0.0000E+00  9.6713E-02  1.0000E-03
 mr-sdci # 25 11    -92.8178907093 -3.0225E-01  0.0000E+00  2.7354E-01  1.0000E-04
 
 root number  7 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .34    .13    .00    .34
    2   25    0    .19    .07    .00    .19
    3   26    0   5.13    .19    .00   5.12
    4   11    0   2.21   1.24    .00   2.21
    5   15    0   4.56   1.67    .00   4.55
    6   16    0   2.65    .92    .00   2.65
    7    1    0    .49    .28    .00    .48
    8    5    0   2.27    .84    .00   2.26
    9    6    0   8.27    .73    .00   8.27
   10    7    0   1.73    .19    .00   1.73
   11   75    0    .08    .06    .00    .08
   12   45    0    .20    .10    .00    .19
   13   46    0    .58    .09    .00    .57
   14   47    0    .24    .04    .00    .23
   15   81    0    .53    .00    .00    .53
   16   82    0   1.61    .00    .00   1.61
   17  101    0    .28    .00    .00    .28
   18   83    0   1.65    .00    .00   1.65
   19  102    0   4.12    .00    .00   4.12
   20   84    0    .43    .00    .00    .43
   21   91    0    .64    .00    .00    .63
   22   93    0   2.27    .00    .00   2.27
   23   94    0   1.18    .00    .00   1.17
   24  103    0   2.96    .00    .00   2.96
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .050000
time for cinew                          .550000
time for eigenvalue solver              .000000
time for vector access                  .000000
================================================================
time spent in mult:                    44.6100s 
time spent in multnx:                  44.5200s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.5500s 
time for vector access in mult:          .0100s 
total time per CI iteration:           45.2200s 

          starting ci iteration  26

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     -.05493496     -.26777544      .94583945     -.00461605      .01215670      .04595737      .09612836     -.01929314
 ref:   2     -.70788112      .66558006      .14702301     -.06034829     -.07780164      .07051040     -.02462620     -.00137618
 ref:   3      .68056452      .66222426      .22639886     -.12804854     -.07276220     -.06457378      .04400047      .01640533
 ref:   4      .04534150      .12699956      .04333278      .97433144     -.01235913     -.01841054     -.01016865     -.10855668
 ref:   5      .00039193      .00046304     -.00159979     -.00000356      .01079540      .05013296      .07247211     -.02306283
 ref:   6     -.00015158      .00021766     -.00079477      .00046494      .00514307      .01379124      .04763212      .01892552
 ref:   7      .00114993      .00000945      .00011982      .00105843     -.00279284      .05686331     -.02036093      .05104490
 ref:   8     -.00212054     -.00970908      .03401405     -.00022033     -.03088270     -.11879511     -.21821161      .04565335
 ref:   9      .04326082      .00052033      .00893827     -.00241427      .00158756      .27221732     -.17052483     -.01857506
 ref:  10     -.00399829     -.00128643      .00470675      .04483614     -.00825794      .00533441      .02357386      .37568625

                ci   9         ci  10         ci  11         ci  12
 ref:   1      .00576577     -.00412080     -.01579211      .01723384
 ref:   2      .00892384     -.00189416      .00383353     -.00179325
 ref:   3     -.01342874      .00221006     -.00630965      .00372995
 ref:   4     -.00665897      .00215617      .00172607      .00229709
 ref:   5      .18794498      .61967464      .02490682     -.03413601
 ref:   6      .08513644      .35294354      .00088749      .00488027
 ref:   7      .04523339      .31544758     -.05909120     -.06563213
 ref:   8      .05818096     -.52346109      .02039317     -.17102791
 ref:   9     -.31664879      .19269795      .02476437     -.15796708
 ref:  10      .01345836      .13343632     -.06915793     -.01388629

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 26  1    -93.3414569791  9.7423E-09  0.0000E+00  6.5988E-04  1.0000E-03
 mr-sdci # 26  2    -93.3414567345  9.6282E-11  0.0000E+00  7.6560E-04  1.0000E-03
 mr-sdci # 26  3    -93.3414566998  1.0208E-07  0.0000E+00  7.6267E-04  1.0000E-03
 mr-sdci # 26  4    -93.3414565907  1.2187E-07  0.0000E+00  7.6675E-04  1.0000E-03
 mr-sdci # 26  5    -93.1685345496  5.8019E-09  0.0000E+00  4.6517E-04  1.0000E-03
 mr-sdci # 26  6    -93.1685250218  2.9338E-10  0.0000E+00  7.2104E-04  1.0000E-03
 mr-sdci # 26  7    -93.1681102412  1.0775E-03  3.2926E-04  9.1552E-03  1.0000E-03
 mr-sdci # 26  8    -93.1535610854  8.3489E-07  0.0000E+00  8.5749E-02  1.0000E-03
 mr-sdci # 26  9    -93.1283241347  3.9767E-04  0.0000E+00  4.5292E-02  1.0000E-03
 mr-sdci # 26 10    -93.1225583377  1.9967E-05  0.0000E+00  9.6466E-02  1.0000E-03
 mr-sdci # 26 11    -92.9422887811  1.2440E-01  0.0000E+00  2.2834E-01  1.0000E-04
 mr-sdci # 26 12    -92.8172643816 -1.6053E-01  0.0000E+00  2.7112E-01  1.0000E-04
 
 root number  7 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .33    .11    .00    .33
    2   25    0    .19    .05    .00    .19
    3   26    0   4.97    .18    .00   4.97
    4   11    0   2.21   1.23    .00   2.21
    5   15    0   4.55   1.66    .00   4.55
    6   16    0   2.66    .94    .00   2.65
    7    1    0    .50    .28    .00    .49
    8    5    0   2.26    .83    .00   2.26
    9    6    0   8.28    .73    .00   8.28
   10    7    0   1.71    .20    .00   1.71
   11   75    0    .09    .05    .00    .08
   12   45    0    .20    .10    .00    .20
   13   46    0    .58    .08    .00    .57
   14   47    0    .23    .04    .00    .23
   15   81    0    .53    .00    .00    .52
   16   82    0   1.61    .00    .00   1.60
   17  101    0    .28    .00    .00    .27
   18   83    0   1.66    .00    .00   1.66
   19  102    0   4.12    .00    .00   4.12
   20   84    0    .43    .00    .00    .43
   21   91    0    .63    .00    .00    .63
   22   93    0   2.28    .00    .00   2.27
   23   94    0   1.18    .00    .00   1.18
   24  103    0   2.94    .00    .00   2.94
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .060000
time for cinew                          .570000
time for eigenvalue solver              .000000
time for vector access                  .000000
================================================================
time spent in mult:                    44.4200s 
time spent in multnx:                  44.3400s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.4800s 
time for vector access in mult:          .0000s 
total time per CI iteration:           45.0500s 

          starting ci iteration  27

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1      .21322964      .95795040      .03914271     -.06850049      .01154067     -.04849306      .09447487      .01909847
 ref:   2     -.68692084      .12926041      .69043007      .06394185     -.07789836     -.06959099     -.02671291      .00142531
 ref:   3      .67206712     -.16961974      .68987902      .11394867     -.07277691      .06363622      .04512403     -.01648929
 ref:   4      .01854510     -.07878997      .12336102     -.97345510     -.01225339      .01870096     -.00946957      .10858152
 ref:   5      .00001312     -.00158999     -.00005084      .00013325      .01033833     -.05175624      .06369870      .02237361
 ref:   6     -.00033433     -.00071826     -.00003561     -.00039598      .00489732     -.01488397      .04298881     -.01932840
 ref:   7      .00109300     -.00029796      .00001469     -.00107512     -.00288495     -.05638349     -.01974955     -.05078509
 ref:   8      .00769621      .03472801      .00135458     -.00239343     -.02930063      .12527673     -.23191725     -.04682788
 ref:   9      .04411658     -.00343596      .00259538      .00140291      .00167373     -.26718753     -.18872858      .01786332
 ref:  10     -.00383003      .00261235     -.00016707     -.04504263     -.00841189     -.00603367      .02603002     -.37542666

                ci   9         ci  10         ci  11         ci  12         ci  13
 ref:   1      .00545691     -.00485045     -.01808468      .00850950      .01601862
 ref:   2      .00898427     -.00176944      .00443974     -.00227774     -.00138691
 ref:   3     -.01352504      .00202053     -.00639430      .00407389      .00343980
 ref:   4     -.00662532      .00225532      .00245372     -.00108268      .00282586
 ref:   5      .18877465      .61185317     -.20735207     -.10301154     -.10667996
 ref:   6      .08560833      .34860248     -.12229503     -.04862106     -.03406525
 ref:   7      .04728489      .31596210      .00978938      .07640460     -.04853430
 ref:   8      .05183233     -.53290457     -.27802365     -.12349180     -.28304376
 ref:   9     -.31816247      .18760308     -.21122303     -.09774647     -.24265717
 ref:  10      .01491220      .13519051      .04021119      .09315719      .01638316

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 27  1    -93.3414570136  3.4460E-08  0.0000E+00  6.4588E-04  1.0000E-03
 mr-sdci # 27  2    -93.3414568184  8.3894E-08  0.0000E+00  7.3062E-04  1.0000E-03
 mr-sdci # 27  3    -93.3414567320  3.2211E-08  0.0000E+00  7.7063E-04  1.0000E-03
 mr-sdci # 27  4    -93.3414565919  1.1725E-09  0.0000E+00  7.6550E-04  1.0000E-03
 mr-sdci # 27  5    -93.1685345554  5.8422E-09  0.0000E+00  4.6516E-04  1.0000E-03
 mr-sdci # 27  6    -93.1685251757  1.5388E-07  0.0000E+00  7.1625E-04  1.0000E-03
 mr-sdci # 27  7    -93.1683806178  2.7038E-04  9.5467E-05  5.4455E-03  1.0000E-03
 mr-sdci # 27  8    -93.1535629778  1.8924E-06  0.0000E+00  8.5744E-02  1.0000E-03
 mr-sdci # 27  9    -93.1283365409  1.2406E-05  0.0000E+00  4.5453E-02  1.0000E-03
 mr-sdci # 27 10    -93.1226397198  8.1382E-05  0.0000E+00  9.6159E-02  1.0000E-03
 mr-sdci # 27 11    -93.0365343300  9.4246E-02  0.0000E+00  1.6014E-01  1.0000E-04
 mr-sdci # 27 12    -92.9251451806  1.0788E-01  0.0000E+00  2.4001E-01  1.0000E-04
 mr-sdci # 27 13    -92.7978430945 -1.6074E-01  0.0000E+00  2.7332E-01  1.0000E-04
 
 root number  7 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .33    .12    .00    .33
    2   25    0    .20    .07    .00    .19
    3   26    0   5.03    .19    .00   5.03
    4   11    0   2.18   1.22    .00   2.17
    5   15    0   4.52   1.67    .00   4.52
    6   16    0   2.66    .94    .00   2.65
    7    1    0    .50    .28    .00    .49
    8    5    0   2.24    .83    .00   2.24
    9    6    0   8.27    .73    .00   8.26
   10    7    0   1.73    .21    .00   1.73
   11   75    0    .08    .06    .00    .08
   12   45    0    .20    .09    .00    .19
   13   46    0    .57    .09    .00    .56
   14   47    0    .24    .03    .00    .24
   15   81    0    .53    .00    .00    .53
   16   82    0   1.60    .00    .00   1.60
   17  101    0    .28    .00    .00    .27
   18   83    0   1.66    .00    .00   1.66
   19  102    0   4.13    .00    .00   4.13
   20   84    0    .43    .00    .00    .43
   21   91    0    .64    .00    .00    .63
   22   93    0   2.26    .00    .00   2.26
   23   94    0   1.17    .00    .00   1.16
   24  103    0   2.95    .00    .00   2.95
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .070000
time for cinew                          .710000
time for eigenvalue solver              .000000
time for vector access                  .000000
================================================================
time spent in mult:                    44.4000s 
time spent in multnx:                  44.3000s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.5300s 
time for vector access in mult:          .0000s 
total time per CI iteration:           45.1800s 

          starting ci iteration  28

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     -.20624838      .95935862      .02242987      .07714468      .00984492     -.05585039      .08993793      .01880165
 ref:   2      .68942624      .13675318      .68707056     -.05722250     -.07794120     -.06668740     -.03307137      .00151089
 ref:   3     -.67161582     -.15156998      .69474446     -.11247775     -.07302322      .06043535      .04873797     -.01661877
 ref:   4     -.02073284     -.08552369      .11890168      .97339770     -.01199745      .01945911     -.00789545      .10861198
 ref:   5     -.00002826     -.00159876     -.00002604     -.00014295      .00903071     -.05688924      .06118026      .02269494
 ref:   6      .00032618     -.00073024     -.00002825      .00039270      .00415115     -.01835484      .04246057     -.01920495
 ref:   7     -.00109582     -.00029595      .00001703      .00106842     -.00293732     -.05454307     -.02491322     -.05084704
 ref:   8     -.00744752      .03476684      .00074456      .00271287     -.02507919      .14341997     -.22119670     -.04653705
 ref:   9     -.04413110     -.00304828      .00275242     -.00150124      .00281377     -.25112774     -.21190926      .01736036
 ref:  10      .00376327      .00216562     -.00048979      .04506819     -.00881168     -.00789997      .02428207     -.37554484

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14
 ref:   1     -.00382608     -.00622036      .02224147      .00065302      .00603534      .01563180
 ref:   2     -.00933773     -.00161476     -.00531717     -.00033870      .00015946     -.00177352
 ref:   3      .01400326      .00184230      .00708758      .00062220     -.00031192      .00488800
 ref:   4      .00650400      .00246591     -.00257012      .00011087      .00244250      .00180695
 ref:   5     -.19654600      .60497599      .11672968     -.23009207     -.13684135      .01493324
 ref:   6     -.08981142      .34488138      .07033954     -.12491516     -.06560149      .03382713
 ref:   7     -.05067048      .31300311      .04360485      .07394095     -.05054396     -.02761458
 ref:   8     -.04225932     -.53425612      .05153384     -.32192989     -.30587018     -.05292147
 ref:   9      .31966883      .18878153      .11052742     -.20582342     -.19241305     -.13839321
 ref:  10     -.01661935      .13423573      .01097820      .10335361     -.00686311      .01974524

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 28  1    -93.3414570141  5.3683E-10  0.0000E+00  6.4582E-04  1.0000E-03
 mr-sdci # 28  2    -93.3414568222  3.7798E-09  0.0000E+00  7.2863E-04  1.0000E-03
 mr-sdci # 28  3    -93.3414567327  6.2044E-10  0.0000E+00  7.7017E-04  1.0000E-03
 mr-sdci # 28  4    -93.3414565930  1.1707E-09  0.0000E+00  7.6497E-04  1.0000E-03
 mr-sdci # 28  5    -93.1685345690  1.3559E-08  0.0000E+00  4.6853E-04  1.0000E-03
 mr-sdci # 28  6    -93.1685254788  3.0309E-07  0.0000E+00  7.7992E-04  1.0000E-03
 mr-sdci # 28  7    -93.1684892252  1.0861E-04  2.9517E-05  3.0755E-03  1.0000E-03
 mr-sdci # 28  8    -93.1535679164  4.9386E-06  0.0000E+00  8.5716E-02  1.0000E-03
 mr-sdci # 28  9    -93.1284736203  1.3708E-04  0.0000E+00  4.5613E-02  1.0000E-03
 mr-sdci # 28 10    -93.1227200131  8.0293E-05  0.0000E+00  9.5497E-02  1.0000E-03
 mr-sdci # 28 11    -93.0997141574  6.3180E-02  0.0000E+00  9.9177E-02  1.0000E-04
 mr-sdci # 28 12    -92.9652523124  4.0107E-02  0.0000E+00  2.0700E-01  1.0000E-04
 mr-sdci # 28 13    -92.8219646077  2.4122E-02  0.0000E+00  2.8403E-01  1.0000E-04
 mr-sdci # 28 14    -92.7612002787 -1.0383E-01  0.0000E+00  2.2578E-01  1.0000E-04
 
 root number  7 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .35    .13    .00    .34
    2   25    0    .19    .07    .00    .19
    3   26    0   5.10    .19    .00   5.10
    4   11    0   2.21   1.23    .00   2.21
    5   15    0   4.54   1.67    .00   4.54
    6   16    0   2.68    .92    .00   2.67
    7    1    0    .48    .26    .00    .48
    8    5    0   2.27    .84    .00   2.26
    9    6    0   8.28    .74    .00   8.28
   10    7    0   1.73    .20    .00   1.73
   11   75    0    .08    .05    .00    .08
   12   45    0    .20    .11    .00    .20
   13   46    0    .58    .08    .00    .57
   14   47    0    .23    .04    .00    .23
   15   81    0    .53    .00    .00    .52
   16   82    0   1.60    .00    .00   1.60
   17  101    0    .27    .00    .00    .27
   18   83    0   1.66    .00    .00   1.65
   19  102    0   4.13    .00    .00   4.12
   20   84    0    .42    .00    .00    .42
   21   91    0    .63    .00    .00    .62
   22   93    0   2.27    .00    .00   2.27
   23   94    0   1.18    .00    .00   1.17
   24  103    0   2.95    .00    .00   2.95
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .060000
time for cinew                          .740000
time for eigenvalue solver              .010000
time for vector access                  .000000
================================================================
time spent in mult:                    44.5600s 
time spent in multnx:                  44.4700s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.5300s 
time for vector access in mult:          .0100s 
total time per CI iteration:           45.3800s 

          starting ci iteration  29

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1      .21050406      .95912815      .00339047     -.07148770     -.00444295      .08503532      .06352777      .01863079
 ref:   2     -.68744343      .15790919     -.68410610      .06193163      .07803208      .04698203     -.05759557      .00156730
 ref:   3      .67230885     -.13638734     -.69654192      .11657965      .07358781     -.04142633      .06498764     -.01669990
 ref:   4      .02133012     -.07677044     -.12725141     -.97305879      .01115012     -.02142306     -.00057094      .10860624
 ref:   5      .00002725     -.00157374     -.00003345      .00012454     -.00519784      .07336878      .03229710      .02015951
 ref:   6     -.00032385     -.00070445     -.00000957     -.00040526     -.00205476      .03093770      .02961057     -.02118949
 ref:   7      .00109376     -.00030088     -.00002504     -.00106671      .00321639      .04180906     -.04275629     -.05064607
 ref:   8      .00761046      .03479459      .00016619     -.00252203      .01122705     -.21764031     -.15943189     -.04919102
 ref:   9      .04412404     -.00316640     -.00274353      .00152976     -.00619333      .15534007     -.29141696      .01564183
 ref:  10     -.00372142      .00242793      .00025679     -.04506032      .01005501      .01567932      .01912393     -.37558305

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15
 ref:   1     -.00070116     -.00361819     -.02025891      .01152950     -.01349090     -.00846513     -.00911121
 ref:   2     -.00995749     -.00169289      .00254572     -.00293487      .00275598      .00221743     -.00058030
 ref:   3      .01471591      .00182866     -.00287773      .00463095     -.00441419     -.00480549     -.00070931
 ref:   4      .00654901      .00208038      .00232358     -.00147880      .00136236      .00174673     -.00465431
 ref:   5     -.13589002      .63617612     -.14816795     -.28709351      .02553635     -.06763953     -.03504531
 ref:   6     -.04899239      .36549529     -.10614783     -.19027420      .05595155     -.02303947     -.08901744
 ref:   7     -.04199057      .31362738      .01842110      .09631970      .00951072     -.04135200      .08940350
 ref:   8     -.01047919     -.49999515     -.26256142     -.47861657      .14326641     -.11618892      .03416596
 ref:   9      .34451932      .18911110     -.05174759     -.19374894     -.03642387     -.06744711      .21707246
 ref:  10     -.01339487      .13390824      .01215415      .08480792      .05977941     -.01165775     -.02035345

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 29  1    -93.3414570143  1.9254E-10  0.0000E+00  6.4547E-04  1.0000E-03
 mr-sdci # 29  2    -93.3414568258  3.5746E-09  0.0000E+00  7.2562E-04  1.0000E-03
 mr-sdci # 29  3    -93.3414567343  1.6650E-09  0.0000E+00  7.6947E-04  1.0000E-03
 mr-sdci # 29  4    -93.3414565935  4.7965E-10  0.0000E+00  7.6485E-04  1.0000E-03
 mr-sdci # 29  5    -93.1685346119  4.2925E-08  0.0000E+00  4.6876E-04  1.0000E-03
 mr-sdci # 29  6    -93.1685271480  1.6692E-06  2.9721E-06  1.0221E-03  1.0000E-03
 mr-sdci # 29  7    -93.1685173928  2.8168E-05  0.0000E+00  1.3855E-03  1.0000E-03
 mr-sdci # 29  8    -93.1535738611  5.9447E-06  0.0000E+00  8.5648E-02  1.0000E-03
 mr-sdci # 29  9    -93.1289604102  4.8679E-04  0.0000E+00  4.4452E-02  1.0000E-03
 mr-sdci # 29 10    -93.1228110747  9.1062E-05  0.0000E+00  9.6575E-02  1.0000E-03
 mr-sdci # 29 11    -93.1184200894  1.8706E-02  0.0000E+00  6.6341E-02  1.0000E-04
 mr-sdci # 29 12    -92.9834552531  1.8203E-02  0.0000E+00  1.9330E-01  1.0000E-04
 mr-sdci # 29 13    -92.9331550008  1.1119E-01  0.0000E+00  2.3012E-01  1.0000E-04
 mr-sdci # 29 14    -92.7875798946  2.6380E-02  0.0000E+00  2.1923E-01  1.0000E-04
 mr-sdci # 29 15    -92.7142216261 -7.2410E-02  0.0000E+00  2.6552E-01  1.0000E-04
 
 root number  6 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .35    .13    .00    .34
    2   25    0    .19    .07    .00    .19
    3   26    0   5.14    .20    .00   5.13
    4   11    0   2.22   1.25    .00   2.22
    5   15    0   4.53   1.65    .00   4.53
    6   16    0   2.67    .91    .00   2.67
    7    1    0    .49    .29    .00    .49
    8    5    0   2.26    .83    .00   2.26
    9    6    0   8.28    .73    .00   8.28
   10    7    0   1.73    .20    .00   1.73
   11   75    0    .08    .06    .00    .08
   12   45    0    .20    .10    .00    .19
   13   46    0    .57    .09    .00    .56
   14   47    0    .24    .03    .00    .24
   15   81    0    .52    .00    .00    .52
   16   82    0   1.54    .00    .00   1.53
   17  101    0    .26    .00    .00    .26
   18   83    0   1.59    .00    .00   1.59
   19  102    0   4.03    .00    .00   4.03
   20   84    0    .43    .00    .00    .43
   21   91    0    .63    .00    .00    .63
   22   93    0   2.27    .00    .00   2.26
   23   94    0   1.17    .00    .00   1.17
   24  103    0   2.96    .00    .00   2.95
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .050000
time for cinew                         1.550000
time for eigenvalue solver              .000000
time for vector access                  .000000
================================================================
time spent in mult:                    44.3500s 
time spent in multnx:                  44.2800s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.5400s 
time for vector access in mult:          .0000s 
total time per CI iteration:           45.9600s 

          starting ci iteration  30

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1      .09632987     -.97917230      .00816938      .03518948      .00153854      .09607915     -.04526702     -.01866199
 ref:   2     -.70001378     -.07804387     -.68101843     -.09723885     -.07871196      .03231965      .06613223     -.00156340
 ref:   3      .68548977      .05846736     -.69891577     -.08697530     -.07306737     -.02968837     -.07165531      .01669884
 ref:   4     -.01213765      .03277722     -.13053794      .97525536     -.01056239     -.02141820     -.00367266     -.10859868
 ref:   5      .00026903      .00159297     -.00004118     -.00007358      .00299196      .07713715     -.01630734     -.02096041
 ref:   6     -.00024497      .00072672     -.00001363      .00041940      .00097055      .03571557     -.02254544      .02081543
 ref:   7      .00109795      .00013969     -.00003250      .00111045     -.00390619      .03195591      .05045643      .05044462
 ref:   8      .00353778     -.03548155      .00034118      .00117842     -.00373613     -.24664028      .11348650      .04846888
 ref:   9      .04419440     -.00193678     -.00288457      .00028199      .00479718      .09438902      .31642770     -.01550991
 ref:  10     -.00592244     -.00385280      .00015233      .04473838     -.01067679      .01918777     -.01572344      .37569454

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1      .00483199      .01546510     -.00889142      .01094024     -.01953080      .00337387      .00667532     -.00826529
 ref:   2      .00876894     -.00566126     -.00052698     -.00292278      .00109534      .00319535     -.00280459     -.00003052
 ref:   3     -.01334981      .00705050      .00045256      .00455929     -.00321097     -.00280707      .00518616     -.00124520
 ref:   4     -.00686584      .00010002      .00246509     -.00142536      .00179028      .00013677     -.00188965     -.00464930
 ref:   5      .20727814      .30313712      .56629803     -.28509975      .06932511     -.05068848      .08252920     -.05356517
 ref:   6      .09196809      .19121385      .32033377     -.18988314      .03101585      .05365636      .01360400     -.08721908
 ref:   7      .05560429      .07384108      .30438124      .09784570      .04267719     -.04702575      .05675749      .07776248
 ref:   8      .06590404      .12196502     -.55914741     -.47369691      .18311726      .00174142      .13150377      .01391284
 ref:   9     -.31528777      .18067787      .15743730     -.19548304     -.05256929      .01098600      .07125156      .21986909
 ref:  10      .01149399      .01357679      .13408317      .08419369     -.00219268      .10265726     -.00713968     -.00691188

 trial vector basis is being transformed.  new dimension:  10

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 30  1    -93.3414570540  3.9645E-08  0.0000E+00  6.2965E-04  1.0000E-03
 mr-sdci # 30  2    -93.3414568393  1.3553E-08  0.0000E+00  7.2594E-04  1.0000E-03
 mr-sdci # 30  3    -93.3414567343  2.2137E-11  0.0000E+00  7.6951E-04  1.0000E-03
 mr-sdci # 30  4    -93.3414566013  7.8194E-09  0.0000E+00  7.6389E-04  1.0000E-03
 mr-sdci # 30  5    -93.1685346197  7.7456E-09  0.0000E+00  4.7201E-04  1.0000E-03
 mr-sdci # 30  6    -93.1685303522  3.2042E-06  0.0000E+00  6.4285E-04  1.0000E-03
 mr-sdci # 30  7    -93.1685187963  1.4035E-06  3.7390E-06  1.0789E-03  1.0000E-03
 mr-sdci # 30  8    -93.1535745651  7.0401E-07  0.0000E+00  8.5639E-02  1.0000E-03
 mr-sdci # 30  9    -93.1294226495  4.6224E-04  0.0000E+00  4.3618E-02  1.0000E-03
 mr-sdci # 30 10    -93.1251623333  2.3513E-03  0.0000E+00  5.7883E-02  1.0000E-03
 mr-sdci # 30 11    -93.1226717709  4.2517E-03  0.0000E+00  9.3776E-02  1.0000E-04
 mr-sdci # 30 12    -92.9834731525  1.7899E-05  0.0000E+00  1.9301E-01  1.0000E-04
 mr-sdci # 30 13    -92.9667458376  3.3591E-02  0.0000E+00  2.0320E-01  1.0000E-04
 mr-sdci # 30 14    -92.8704827491  8.2903E-02  0.0000E+00  2.7969E-01  1.0000E-04
 mr-sdci # 30 15    -92.7842106046  6.9989E-02  0.0000E+00  2.2317E-01  1.0000E-04
 mr-sdci # 30 16    -92.7104632917  2.4601E-02  0.0000E+00  2.6654E-01  1.0000E-04
 
 root number  7 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .34    .13    .00    .33
    2   25    0    .20    .07    .00    .19
    3   26    0   5.15    .19    .00   5.15
    4   11    0   2.21   1.23    .00   2.21
    5   15    0   4.54   1.69    .00   4.53
    6   16    0   2.67    .94    .00   2.66
    7    1    0    .49    .29    .00    .49
    8    5    0   2.27    .83    .00   2.27
    9    6    0   8.28    .74    .00   8.28
   10    7    0   1.72    .20    .00   1.72
   11   75    0    .08    .06    .00    .08
   12   45    0    .21    .10    .00    .20
   13   46    0    .56    .09    .00    .56
   14   47    0    .24    .03    .00    .23
   15   81    0    .53    .00    .00    .53
   16   82    0   1.60    .00    .00   1.60
   17  101    0    .28    .00    .00    .27
   18   83    0   1.66    .00    .00   1.66
   19  102    0   4.13    .00    .00   4.13
   20   84    0    .43    .00    .00    .43
   21   91    0    .62    .00    .00    .61
   22   93    0   2.27    .00    .00   2.27
   23   94    0   1.15    .00    .00   1.14
   24  103    0   2.95    .00    .00   2.95
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .070000
time for cinew                         1.330000
time for eigenvalue solver              .010000
time for vector access                  .000000
================================================================
time spent in mult:                    44.5800s 
time spent in multnx:                  44.4900s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.5900s 
time for vector access in mult:          .0000s 
total time per CI iteration:           45.9900s 

          starting ci iteration  31

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     -.10997712      .97746567      .01149103     -.04119539     -.00149013     -.09998361     -.03582136      .01859723
 ref:   2      .68873073      .09030448     -.68870982      .11193665     -.07802969     -.02394812      .07037659      .00157563
 ref:   3     -.69387608     -.06720977     -.69228894      .06421728     -.07385174      .02440587     -.07284557     -.01672243
 ref:   4      .03801002     -.03536454     -.12513508     -.97520802     -.01016680      .02120273     -.00554881      .10856640
 ref:   5     -.00023603     -.00159714     -.00004159      .00006931      .00100686     -.07826147     -.00851986      .02004316
 ref:   6      .00022434     -.00072195     -.00001768     -.00040159     -.00029739     -.03781224     -.01923539     -.02060725
 ref:   7     -.00113590     -.00015601     -.00001873     -.00112567     -.00364064     -.02711287      .05274983     -.04928972
 ref:   8     -.00403308      .03541913      .00046156     -.00139738      .00399884      .25645363      .08938924     -.04874949
 ref:   9     -.04403664      .00126104     -.00240436     -.00154953      .00946184     -.06279678      .32621492      .01046995
 ref:  10      .00707849      .00380331      .00035431     -.04456933     -.01141220     -.02034301     -.01359347     -.37549315

                ci   9         ci  10         ci  11
 ref:   1      .00169294     -.01591193     -.00451055
 ref:   2     -.00928335      .00082505      .00998943
 ref:   3      .01366243      .00004048     -.01292005
 ref:   4      .00699087      .00228485      .00064454
 ref:   5     -.06528732     -.36143855     -.01238983
 ref:   6     -.02085130     -.20116787     -.13074953
 ref:   7     -.04228736     -.07431071     -.17667934
 ref:   8     -.01237209     -.13683497     -.01703363
 ref:   9      .40756128     -.06981712      .35489743
 ref:  10     -.01076823     -.01308030     -.03180679

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 31  1    -93.3414571160  6.2010E-08  0.0000E+00  6.1801E-04  1.0000E-03
 mr-sdci # 31  2    -93.3414568395  2.1880E-10  0.0000E+00  7.2636E-04  1.0000E-03
 mr-sdci # 31  3    -93.3414567346  2.1431E-10  0.0000E+00  7.6920E-04  1.0000E-03
 mr-sdci # 31  4    -93.3414566041  2.7977E-09  0.0000E+00  7.6379E-04  1.0000E-03
 mr-sdci # 31  5    -93.1685346463  2.6602E-08  0.0000E+00  4.6274E-04  1.0000E-03
 mr-sdci # 31  6    -93.1685306841  3.3189E-07  0.0000E+00  5.4465E-04  1.0000E-03
 mr-sdci # 31  7    -93.1685216886  2.8923E-06  0.0000E+00  5.8548E-04  1.0000E-03
 mr-sdci # 31  8    -93.1535833708  8.8057E-06  2.0663E-02  8.5544E-02  1.0000E-03
 mr-sdci # 31  9    -93.1309149125  1.4923E-03  0.0000E+00  4.2219E-02  1.0000E-03
 mr-sdci # 31 10    -93.1270172888  1.8550E-03  0.0000E+00  5.1774E-02  1.0000E-03
 mr-sdci # 31 11    -92.9812212954 -1.4145E-01  0.0000E+00  2.3518E-01  1.0000E-04
 
 root number  8 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .34    .12    .00    .34
    2   25    0    .20    .06    .00    .20
    3   26    0   5.15    .20    .00   5.15
    4   11    0   2.22   1.24    .00   2.21
    5   15    0   4.56   1.68    .00   4.56
    6   16    0   2.67    .92    .00   2.66
    7    1    0    .49    .28    .00    .49
    8    5    0   2.25    .84    .00   2.24
    9    6    0   8.14    .71    .00   8.14
   10    7    0   1.66    .19    .00   1.66
   11   75    0    .07    .05    .00    .07
   12   45    0    .19    .10    .00    .18
   13   46    0    .56    .08    .00    .55
   14   47    0    .23    .04    .00    .23
   15   81    0    .53    .00    .00    .53
   16   82    0   1.59    .00    .00   1.59
   17  101    0    .28    .00    .00    .27
   18   83    0   1.66    .00    .00   1.66
   19  102    0   4.13    .00    .00   4.12
   20   84    0    .43    .00    .00    .43
   21   91    0    .63    .00    .00    .62
   22   93    0   2.28    .00    .00   2.28
   23   94    0   1.18    .00    .00   1.17
   24  103    0   2.95    .00    .00   2.94
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .050000
time for cinew                         1.140000
time for eigenvalue solver              .000000
time for vector access                  .000000
================================================================
time spent in mult:                    44.3900s 
time spent in multnx:                  44.2900s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.5100s 
time for vector access in mult:          .0100s 
total time per CI iteration:           45.5900s 

          starting ci iteration  32

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     -.10466550     -.97875547      .01296893      .01664776     -.00147917      .09994231      .03592255      .01848018
 ref:   2      .68809654     -.08485171     -.68742023     -.12701449     -.07801265      .02400661     -.07037245      .00145976
 ref:   3     -.69460049      .06408795     -.69233313     -.05887395     -.07386223     -.02440976      .07278065     -.01629198
 ref:   4      .04951231      .00955042     -.13166693      .97441638     -.01022441     -.02132696      .00579394      .10730116
 ref:   5     -.00024413      .00159652     -.00004428     -.00002220      .00101762      .07823723      .00860459      .01700596
 ref:   6      .00022485      .00071293     -.00002147      .00041489     -.00028366      .03781870      .01922056     -.01558936
 ref:   7     -.00112477      .00012186     -.00002687      .00113593     -.00360655      .02718842     -.05282834     -.03973774
 ref:   8     -.00384259     -.03546267      .00051584      .00050541      .00396679     -.25635767     -.08963096     -.04088968
 ref:   9     -.04400719     -.00153646     -.00243536      .00201821      .00952077      .06295421     -.32612370      .02211734
 ref:  10      .00761834     -.00493338      .00006981      .04432932     -.01126552      .02068493      .01287743     -.30850716

                ci   9         ci  10         ci  11         ci  12
 ref:   1      .00111639      .01573656      .00459676     -.00353734
 ref:   2     -.00933226     -.00078742     -.00998802     -.00009204
 ref:   3      .01423699      .00003246      .01285222      .00288302
 ref:   4      .00317709     -.00327777     -.00018191     -.01809485
 ref:   5     -.06434040      .36152683      .01271709     -.02198174
 ref:   6     -.01887083      .20151707      .13042109      .02436807
 ref:   7     -.03957997      .07513991      .17584142      .05951246
 ref:   8     -.00956391      .13744168      .01643520      .04028281
 ref:   9      .40709912      .06746423     -.35440170     -.03194088
 ref:  10      .00625669      .01757242      .02639931      .36972242

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 32  1    -93.3414571174  1.4089E-09  0.0000E+00  6.1778E-04  1.0000E-03
 mr-sdci # 32  2    -93.3414568409  1.4022E-09  0.0000E+00  7.2554E-04  1.0000E-03
 mr-sdci # 32  3    -93.3414567346  2.4663E-11  0.0000E+00  7.6926E-04  1.0000E-03
 mr-sdci # 32  4    -93.3414566291  2.4929E-08  0.0000E+00  7.5609E-04  1.0000E-03
 mr-sdci # 32  5    -93.1685346467  4.2185E-10  0.0000E+00  4.6292E-04  1.0000E-03
 mr-sdci # 32  6    -93.1685306866  2.4505E-09  0.0000E+00  5.4544E-04  1.0000E-03
 mr-sdci # 32  7    -93.1685216993  1.0682E-08  0.0000E+00  5.8617E-04  1.0000E-03
 mr-sdci # 32  8    -93.1668909166  1.3308E-02  1.3576E-03  2.0921E-02  1.0000E-03
 mr-sdci # 32  9    -93.1309861887  7.1276E-05  0.0000E+00  4.1225E-02  1.0000E-03
 mr-sdci # 32 10    -93.1270232384  5.9496E-06  0.0000E+00  5.1744E-02  1.0000E-03
 mr-sdci # 32 11    -92.9812458299  2.4535E-05  0.0000E+00  2.3505E-01  1.0000E-04
 mr-sdci # 32 12    -92.8271711458 -1.5630E-01  0.0000E+00  2.7523E-01  1.0000E-04
 
 root number  8 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .34    .12    .00    .34
    2   25    0    .20    .06    .00    .20
    3   26    0   5.16    .19    .00   5.16
    4   11    0   2.22   1.24    .00   2.21
    5   15    0   4.57   1.70    .00   4.56
    6   16    0   2.67    .93    .00   2.66
    7    1    0    .49    .29    .00    .49
    8    5    0   2.26    .83    .00   2.26
    9    6    0   8.28    .75    .00   8.28
   10    7    0   1.72    .20    .00   1.72
   11   75    0    .08    .06    .00    .08
   12   45    0    .21    .10    .00    .20
   13   46    0    .57    .09    .00    .57
   14   47    0    .23    .04    .00    .23
   15   81    0    .53    .00    .00    .52
   16   82    0   1.61    .00    .00   1.60
   17  101    0    .27    .00    .00    .26
   18   83    0   1.65    .00    .00   1.65
   19  102    0   4.20    .00    .00   4.19
   20   84    0    .42    .00    .00    .42
   21   91    0    .63    .00    .00    .63
   22   93    0   2.25    .00    .00   2.25
   23   94    0   1.18    .00    .00   1.17
   24  103    0   2.95    .00    .00   2.94
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .080000
time for cinew                          .610000
time for eigenvalue solver              .000000
time for vector access                  .000000
================================================================
time spent in mult:                    44.6900s 
time spent in multnx:                  44.5900s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.6000s 
time for vector access in mult:          .0000s 
total time per CI iteration:           45.3900s 

          starting ci iteration  33

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1      .09677536      .21166478      .95662636      .00781082      .00146479     -.09989414      .03602171      .01843379
 ref:   2     -.68375293     -.06440818      .08913517     -.69981735      .07797578     -.02408516     -.07038462      .00141787
 ref:   3      .69245277      .06430724     -.07860903     -.69250465      .07386789      .02436117      .07269404     -.01658952
 ref:   4     -.11392170      .95722359     -.20026969     -.00230728      .01046806      .02151924      .00611496      .10625299
 ref:   5      .00025807     -.00033268     -.00156157     -.00003545     -.00102425     -.07819656      .00869935      .01893304
 ref:   6     -.00025490      .00032627     -.00078843      .00004185      .00021381     -.03785517      .01916126     -.02152471
 ref:   7      .00103361      .00129417     -.00036413      .00013195      .00345115     -.02731613     -.05299290     -.05096330
 ref:   8      .00356271      .00757723      .03468008      .00031602     -.00391615      .25624642     -.08986634     -.04113620
 ref:   9      .04375286      .00544887      .00077338     -.00209417     -.00961639     -.06303589     -.32601162      .02582423
 ref:  10     -.01054317      .04358250     -.00362953      .00585875      .01056975     -.02127989      .01187706     -.31631920

                ci   9         ci  10         ci  11         ci  12         ci  13
 ref:   1      .00098262      .01570356      .00276987     -.00429595     -.00407822
 ref:   2     -.00936605     -.00076395     -.00891122      .00449727      .00002233
 ref:   3      .01437379      .00001024      .01130863     -.00631468      .00259760
 ref:   4      .00205340     -.00346852     -.00810158     -.01339444     -.02103927
 ref:   5     -.06285310      .36179239      .02260221      .01661817     -.01733064
 ref:   6     -.01969538      .20131597      .07467496     -.13923580      .00450676
 ref:   7     -.04196831      .07479185      .08190431     -.22475985      .02451416
 ref:   8     -.00886207      .13751327      .01220428     -.01380865      .03928107
 ref:   9      .40681287      .06611705     -.31016445      .17428931     -.02396644
 ref:  10      .00659218      .01755341     -.03362771     -.13628837      .34695662

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 33  1    -93.3414571187  1.3562E-09  0.0000E+00  6.1597E-04  1.0000E-03
 mr-sdci # 33  2    -93.3414569297  8.8770E-08  0.0000E+00  6.8815E-04  1.0000E-03
 mr-sdci # 33  3    -93.3414568385  1.0391E-07  0.0000E+00  7.2808E-04  1.0000E-03
 mr-sdci # 33  4    -93.3414567334  1.0434E-07  0.0000E+00  7.6778E-04  1.0000E-03
 mr-sdci # 33  5    -93.1685346497  3.0421E-09  0.0000E+00  4.6319E-04  1.0000E-03
 mr-sdci # 33  6    -93.1685306888  2.2478E-09  0.0000E+00  5.4645E-04  1.0000E-03
 mr-sdci # 33  7    -93.1685217056  6.3071E-09  0.0000E+00  5.8720E-04  1.0000E-03
 mr-sdci # 33  8    -93.1680640614  1.1731E-03  3.4816E-04  9.5969E-03  1.0000E-03
 mr-sdci # 33  9    -93.1310562470  7.0058E-05  0.0000E+00  4.1022E-02  1.0000E-03
 mr-sdci # 33 10    -93.1270254233  2.1849E-06  0.0000E+00  5.1719E-02  1.0000E-03
 mr-sdci # 33 11    -92.9874062950  6.1605E-03  0.0000E+00  2.2430E-01  1.0000E-04
 mr-sdci # 33 12    -92.9587291340  1.3156E-01  0.0000E+00  2.3529E-01  1.0000E-04
 mr-sdci # 33 13    -92.8213749383 -1.4537E-01  0.0000E+00  2.6802E-01  1.0000E-04
 
 root number  8 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .34    .13    .00    .33
    2   25    0    .20    .07    .00    .19
    3   26    0   5.06    .18    .00   5.06
    4   11    0   2.21   1.25    .00   2.21
    5   15    0   4.55   1.67    .00   4.54
    6   16    0   2.57    .87    .00   2.57
    7    1    0    .47    .27    .00    .47
    8    5    0   2.24    .81    .00   2.23
    9    6    0   8.27    .73    .00   8.26
   10    7    0   1.71    .20    .00   1.71
   11   75    0    .09    .06    .00    .09
   12   45    0    .20    .10    .00    .20
   13   46    0    .56    .08    .00    .56
   14   47    0    .22    .04    .00    .22
   15   81    0    .53    .00    .00    .53
   16   82    0   1.59    .00    .00   1.58
   17  101    0    .27    .00    .00    .27
   18   83    0   1.66    .00    .00   1.65
   19  102    0   4.13    .00    .00   4.13
   20   84    0    .44    .00    .00    .44
   21   91    0    .61    .00    .00    .61
   22   93    0   2.28    .00    .00   2.27
   23   94    0   1.15    .00    .00   1.15
   24  103    0   2.94    .00    .00   2.94
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .040000
time for cinew                          .760000
time for eigenvalue solver              .000000
time for vector access                  .000000
================================================================
time spent in mult:                    44.2900s 
time spent in multnx:                  44.2100s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.4600s 
time for vector access in mult:          .0000s 
total time per CI iteration:           45.1000s 

          starting ci iteration  34

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1      .03424249     -.30409163     -.93561364      .01859851     -.00136697      .09963190      .03644315      .01890087
 ref:   2     -.64959226      .22509629     -.08302211      .69987479     -.07790725      .02437191     -.07034570      .00203131
 ref:   3      .67922844     -.15362254      .08849596      .69035181     -.07389872     -.02426267      .07243940     -.01761164
 ref:   4     -.29130309     -.89589326      .28155254      .05117263     -.01077278     -.02250650      .00703662      .10538403
 ref:   5      .00034036      .00037872      .00152467     -.00001169      .00110680      .07802394      .00904333      .01740382
 ref:   6     -.00027542     -.00015021      .00081837     -.00002975     -.00012449      .03794534      .01911785     -.01702995
 ref:   7      .00079941     -.00136626      .00047230     -.00006486     -.00327535      .02784831     -.05331849     -.04470762
 ref:   8      .00130719     -.01095691     -.03392824      .00063448      .00365400     -.25561757     -.09091003     -.04374496
 ref:   9      .04190060     -.01372428     -.00048725      .00219368      .00979714      .06354987     -.32556720      .03113738
 ref:  10     -.01881422     -.04075798      .00728883     -.00353597     -.00966195      .02452149      .00870544     -.33527087

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14
 ref:   1     -.00090155     -.01570740     -.00351476     -.00480232     -.00082904      .00368720
 ref:   2      .00938548      .00076507     -.00197017      .00961996      .00195596     -.00028550
 ref:   3     -.01442077     -.00001128      .00239403     -.01241554     -.00320603     -.00230262
 ref:   4     -.00164888      .00344923     -.01993301     -.00053107     -.00448421      .02018482
 ref:   5      .06354369     -.36180812     -.02693161     -.02389468      .04834432      .00138122
 ref:   6      .01846822     -.20124970      .06148158     -.10009914     -.15560799      .03498708
 ref:   7      .04034321     -.07470313      .06479664     -.13136315     -.24901885      .03395082
 ref:   8      .00910668     -.13752226     -.01565273     -.01926705     -.00297841     -.04401027
 ref:   9     -.40694403     -.06620006     -.02213591      .35134262      .05681502      .02806679
 ref:  10     -.00161274     -.01780332     -.35757633     -.08576349      .03157511     -.43263641

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 34  1    -93.3414571253  6.5620E-09  0.0000E+00  6.1164E-04  1.0000E-03
 mr-sdci # 34  2    -93.3414570311  1.0139E-07  0.0000E+00  6.6300E-04  1.0000E-03
 mr-sdci # 34  3    -93.3414568397  1.2241E-09  0.0000E+00  7.2754E-04  1.0000E-03
 mr-sdci # 34  4    -93.3414567351  1.6768E-09  0.0000E+00  7.6818E-04  1.0000E-03
 mr-sdci # 34  5    -93.1685346515  1.7519E-09  0.0000E+00  4.6372E-04  1.0000E-03
 mr-sdci # 34  6    -93.1685307123  2.3438E-08  0.0000E+00  5.5004E-04  1.0000E-03
 mr-sdci # 34  7    -93.1685217271  2.1480E-08  0.0000E+00  5.9592E-04  1.0000E-03
 mr-sdci # 34  8    -93.1683613883  2.9733E-04  1.0597E-04  5.7881E-03  1.0000E-03
 mr-sdci # 34  9    -93.1310748130  1.8566E-05  0.0000E+00  4.0971E-02  1.0000E-03
 mr-sdci # 34 10    -93.1270254664  4.3090E-08  0.0000E+00  5.1719E-02  1.0000E-03
 mr-sdci # 34 11    -93.0465659768  5.9160E-02  0.0000E+00  1.5810E-01  1.0000E-04
 mr-sdci # 34 12    -92.9817288028  2.3000E-02  0.0000E+00  2.2991E-01  1.0000E-04
 mr-sdci # 34 13    -92.9266622640  1.0529E-01  0.0000E+00  2.3965E-01  1.0000E-04
 mr-sdci # 34 14    -92.8095550573 -6.0928E-02  0.0000E+00  2.7228E-01  1.0000E-04
 
 root number  8 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .34    .12    .00    .34
    2   25    0    .19    .06    .00    .19
    3   26    0   5.14    .19    .00   5.14
    4   11    0   2.24   1.24    .00   2.23
    5   15    0   4.53   1.68    .00   4.53
    6   16    0   2.65    .93    .00   2.65
    7    1    0    .49    .28    .00    .49
    8    5    0   2.27    .83    .00   2.26
    9    6    0   8.27    .73    .00   8.27
   10    7    0   1.73    .20    .00   1.73
   11   75    0    .08    .06    .00    .08
   12   45    0    .21    .09    .00    .20
   13   46    0    .56    .08    .00    .56
   14   47    0    .24    .05    .00    .24
   15   81    0    .53    .00    .00    .53
   16   82    0   1.57    .00    .00   1.57
   17  101    0    .26    .00    .00    .26
   18   83    0   1.66    .00    .00   1.65
   19  102    0   4.08    .00    .00   4.07
   20   84    0    .44    .00    .00    .44
   21   91    0    .63    .00    .00    .63
   22   93    0   2.25    .00    .00   2.25
   23   94    0   1.19    .00    .00   1.18
   24  103    0   2.90    .00    .00   2.90
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .040000
time for cinew                          .720000
time for eigenvalue solver              .000000
time for vector access                  .000000
================================================================
time spent in mult:                    44.4500s 
time spent in multnx:                  44.3900s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.5400s 
time for vector access in mult:          .0100s 
total time per CI iteration:           45.2300s 

          starting ci iteration  35

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     -.03339265     -.30176678     -.93632479     -.02192358      .00092166     -.09839306      .03920302      .01976606
 ref:   2      .65000257      .22260408     -.07851465     -.70081049      .07740283     -.02597719     -.07005921      .00646485
 ref:   3     -.68040435     -.15208452      .08940672     -.68941645      .07412754      .02384297      .07029260     -.02453044
 ref:   4      .28771992     -.89756315      .28019063     -.04963803      .01271801      .02630388      .01373362      .10294849
 ref:   5     -.00034120      .00037699      .00152346      .00001813     -.00148479     -.07717296      .01148350      .01992664
 ref:   6      .00027112     -.00015478      .00082158      .00003128     -.00045853     -.03837204      .01835674     -.01868449
 ref:   7     -.00080891     -.00136788      .00047305      .00006541      .00197904     -.03038364     -.05612616     -.04354998
 ref:   8     -.00127764     -.01087357     -.03395193     -.00075613     -.00246374      .25255912     -.09783126     -.04796127
 ref:   9     -.04194459     -.01358352     -.00059811     -.00215452     -.01121403     -.06651067     -.32143161      .05759331
 ref:  10      .01866771     -.04083395      .00721379      .00358164      .00391548     -.03689893     -.01408234     -.33693232

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15
 ref:   1     -.00078103      .01579072     -.00357539      .00479575      .00045513      .00231435      .00320443
 ref:   2      .00940606     -.00076611     -.00130302     -.00963013      .00231335      .00013736     -.00063621
 ref:   3     -.01445490      .00001615      .00145058      .01243054     -.00358404     -.00238518     -.00092955
 ref:   4     -.00117502     -.00293616     -.02329684      .00049933      .00158023      .01373469      .01527196
 ref:   5      .06429482      .36137566      .01028016      .02362991      .06318538     -.00097949      .01494147
 ref:   6      .01949228      .20165859     -.00561836      .10079589     -.17782293      .01532252      .01525047
 ref:   7      .04185369      .07594363     -.04454271      .13239139     -.27258478      .00276388      .01640671
 ref:   8      .01007586      .13813063     -.02797623      .01928789     -.00676024     -.02661966     -.04840764
 ref:   9     -.40723687      .06683578      .01263979     -.35163934      .07196509      .01751960      .04096392
 ref:  10      .00070813      .02049930     -.19536953      .08423967      .25435767     -.48267522     -.00711896

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 35  1    -93.3414571257  3.9411E-10  0.0000E+00  6.1168E-04  1.0000E-03
 mr-sdci # 35  2    -93.3414570313  2.4988E-10  0.0000E+00  6.6240E-04  1.0000E-03
 mr-sdci # 35  3    -93.3414568406  8.2713E-10  0.0000E+00  7.2701E-04  1.0000E-03
 mr-sdci # 35  4    -93.3414567353  1.9424E-10  0.0000E+00  7.6810E-04  1.0000E-03
 mr-sdci # 35  5    -93.1685346687  1.7253E-08  0.0000E+00  4.6803E-04  1.0000E-03
 mr-sdci # 35  6    -93.1685307911  7.8821E-08  0.0000E+00  5.6326E-04  1.0000E-03
 mr-sdci # 35  7    -93.1685219405  2.1342E-07  0.0000E+00  6.3609E-04  1.0000E-03
 mr-sdci # 35  8    -93.1684864629  1.2507E-04  3.1431E-05  3.1942E-03  1.0000E-03
 mr-sdci # 35  9    -93.1310861864  1.1373E-05  0.0000E+00  4.1040E-02  1.0000E-03
 mr-sdci # 35 10    -93.1270365132  1.1047E-05  0.0000E+00  5.1768E-02  1.0000E-03
 mr-sdci # 35 11    -93.1048655054  5.8300E-02  0.0000E+00  9.3643E-02  1.0000E-04
 mr-sdci # 35 12    -92.9817292765  4.7372E-07  0.0000E+00  2.3004E-01  1.0000E-04
 mr-sdci # 35 13    -92.9592881468  3.2626E-02  0.0000E+00  2.0818E-01  1.0000E-04
 mr-sdci # 35 14    -92.8237703646  1.4215E-02  0.0000E+00  2.8756E-01  1.0000E-04
 mr-sdci # 35 15    -92.7543123261 -2.9898E-02  0.0000E+00  2.1966E-01  1.0000E-04
 
 root number  8 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .35    .13    .00    .34
    2   25    0    .20    .07    .00    .20
    3   26    0   5.15    .19    .00   5.14
    4   11    0   2.20   1.24    .00   2.20
    5   15    0   4.56   1.70    .00   4.56
    6   16    0   2.65    .93    .00   2.65
    7    1    0    .49    .28    .00    .49
    8    5    0   2.25    .82    .00   2.24
    9    6    0   8.26    .74    .00   8.26
   10    7    0   1.75    .20    .00   1.74
   11   75    0    .09    .05    .00    .08
   12   45    0    .20    .10    .00    .20
   13   46    0    .58    .08    .00    .58
   14   47    0    .24    .05    .00    .24
   15   81    0    .54    .00    .00    .54
   16   82    0   1.61    .00    .00   1.60
   17  101    0    .28    .00    .00    .28
   18   83    0   1.66    .00    .00   1.65
   19  102    0   4.14    .00    .00   4.13
   20   84    0    .44    .00    .00    .43
   21   91    0    .62    .00    .00    .62
   22   93    0   2.28    .00    .00   2.27
   23   94    0   1.17    .00    .00   1.17
   24  103    0   2.95    .00    .00   2.95
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .080000
time for cinew                          .800000
time for eigenvalue solver              .000000
time for vector access                  .000000
================================================================
time spent in mult:                    44.6600s 
time spent in multnx:                  44.5600s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.5800s 
time for vector access in mult:          .0000s 
total time per CI iteration:           45.5500s 

          starting ci iteration  36

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     -.03738913     -.28464518     -.94157797      .01931618      .00019658     -.09457259     -.05011776      .01236155
 ref:   2      .65506215      .21371976     -.07627841      .69910997      .07642383     -.02930119      .06301936      .03074659
 ref:   3     -.68311768     -.13653650      .08255796      .69084065      .07429407      .02206177     -.05399062     -.05181753
 ref:   4      .26869553     -.90775730      .26488755      .05464686      .01698175      .03644808     -.04674061      .08832856
 ref:   5     -.00033937      .00034444      .00153333     -.00001481     -.00213919     -.07450940     -.02244297      .01970760
 ref:   6      .00026809     -.00016350      .00081584     -.00002584     -.00159834     -.03921718     -.01360119     -.01993195
 ref:   7     -.00083100     -.00132297      .00043623     -.00005224     -.00051369     -.03607107      .06453605     -.01576354
 ref:   8     -.00142053     -.01024818     -.03414228      .00066175     -.00048322      .24310736      .12510590     -.03023899
 ref:   9     -.04221211     -.01270694     -.00092232      .00223489     -.01343926     -.07124701      .27555834      .17314278
 ref:  10      .01779713     -.04141003      .00657531     -.00338888     -.00934595     -.07150371      .13178416     -.31278074

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1     -.00023467     -.01596232      .00217566      .00529832      .00051639     -.00245541     -.00088925     -.00284226
 ref:   2      .00953023      .00084213      .00051436     -.00930563     -.00177550     -.00268105      .00014459      .00089244
 ref:   3     -.01440311     -.00025759      .00099617      .01095947      .00378841      .00744901      .00069893     -.00098109
 ref:   4      .00122947      .00161927      .02117551      .00576660     -.00765125     -.01699747     -.00331310     -.01346008
 ref:   5      .07105896     -.36064158     -.01625568     -.01074414      .08788767      .01542761      .01188809     -.05063761
 ref:   6      .01760420     -.19906146     -.03769476      .15207062     -.13517226      .05997813     -.00799172      .00075661
 ref:   7      .03392913     -.07156726     -.04603426      .24403762     -.26464075      .01315454     -.06623260      .09267125
 ref:   8      .01328691     -.13866671      .00853998      .02561495     -.00722471     -.00049961      .00149317      .06891785
 ref:   9     -.40105289     -.07548939      .05456009     -.35420139     -.04909911     -.05855612     -.00644727     -.05662409
 ref:  10      .04324088     -.04130936      .30678237     -.08459759      .41355309      .25246591     -.25414790      .10212990

 trial vector basis is being transformed.  new dimension:  10

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 36  1    -93.3414571260  3.2444E-10  0.0000E+00  6.1252E-04  1.0000E-03
 mr-sdci # 36  2    -93.3414570416  1.0287E-08  0.0000E+00  6.5312E-04  1.0000E-03
 mr-sdci # 36  3    -93.3414568417  1.1180E-09  0.0000E+00  7.2612E-04  1.0000E-03
 mr-sdci # 36  4    -93.3414567355  1.7608E-10  0.0000E+00  7.6810E-04  1.0000E-03
 mr-sdci # 36  5    -93.1685347008  3.2035E-08  0.0000E+00  4.7119E-04  1.0000E-03
 mr-sdci # 36  6    -93.1685309742  1.8316E-07  0.0000E+00  5.9415E-04  1.0000E-03
 mr-sdci # 36  7    -93.1685229027  9.6215E-07  0.0000E+00  8.5383E-04  1.0000E-03
 mr-sdci # 36  8    -93.1685164024  2.9940E-05  6.0805E-06  1.3788E-03  1.0000E-03
 mr-sdci # 36  9    -93.1312606177  1.7443E-04  0.0000E+00  4.0895E-02  1.0000E-03
 mr-sdci # 36 10    -93.1270583354  2.1822E-05  0.0000E+00  5.1861E-02  1.0000E-03
 mr-sdci # 36 11    -93.1226691662  1.7804E-02  0.0000E+00  5.7571E-02  1.0000E-04
 mr-sdci # 36 12    -92.9842432776  2.5140E-03  0.0000E+00  2.2607E-01  1.0000E-04
 mr-sdci # 36 13    -92.9710657885  1.1778E-02  0.0000E+00  2.0032E-01  1.0000E-04
 mr-sdci # 36 14    -92.9295473039  1.0578E-01  0.0000E+00  2.1252E-01  1.0000E-04
 mr-sdci # 36 15    -92.7803530670  2.6041E-02  0.0000E+00  2.3799E-01  1.0000E-04
 mr-sdci # 36 16    -92.7110727725  6.0948E-04  0.0000E+00  2.6795E-01  1.0000E-04
 
 root number  8 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .34    .12    .00    .34
    2   25    0    .20    .06    .00    .20
    3   26    0   5.15    .18    .00   5.15
    4   11    0   2.22   1.24    .00   2.21
    5   15    0   4.53   1.68    .00   4.52
    6   16    0   2.65    .93    .00   2.64
    7    1    0    .48    .27    .00    .48
    8    5    0   2.26    .84    .00   2.25
    9    6    0   8.29    .73    .00   8.29
   10    7    0   1.72    .20    .00   1.72
   11   75    0    .09    .05    .00    .08
   12   45    0    .20    .10    .00    .20
   13   46    0    .58    .08    .00    .58
   14   47    0    .23    .04    .00    .23
   15   81    0    .53    .00    .00    .53
   16   82    0   1.60    .00    .00   1.59
   17  101    0    .28    .00    .00    .27
   18   83    0   1.66    .00    .00   1.66
   19  102    0   4.13    .00    .00   4.13
   20   84    0    .43    .00    .00    .43
   21   91    0    .64    .00    .00    .63
   22   93    0   2.28    .00    .00   2.28
   23   94    0   1.18    .00    .00   1.18
   24  103    0   2.95    .00    .00   2.95
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .060000
time for cinew                          .840000
time for eigenvalue solver              .000000
time for vector access                  .000000
================================================================
time spent in mult:                    44.6200s 
time spent in multnx:                  44.5400s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.5200s 
time for vector access in mult:          .0000s 
total time per CI iteration:           45.5200s 

          starting ci iteration  37

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1      .00959636      .27577534      .94490736      .01920995      .00069774     -.08976186     -.05938080      .00498566
 ref:   2      .60254858     -.33397625      .07713927      .69914261     -.07565034     -.03203830      .04278230     -.05510211
 ref:   3     -.64704557      .25889393     -.08300984      .69049579     -.07429894      .01916421     -.02370115      .07181502
 ref:   4      .43304614      .84542830     -.25234357      .05849111     -.02012250      .04656627     -.07534544     -.05796124
 ref:   5     -.00039625     -.00027068     -.00153762     -.00001418      .00288754     -.07097277     -.03531021     -.01207006
 ref:   6      .00031463      .00014187     -.00081489     -.00002497      .00255198     -.03972727     -.00799795      .02139755
 ref:   7     -.00057341      .00145388     -.00042277     -.00004799      .00238111     -.04132379      .06157938     -.01489811
 ref:   8      .00026330      .00992962      .03426288      .00065803     -.00187580      .23104042      .14881311     -.01246012
 ref:   9     -.03911358      .02031184      .00089768      .00221951      .01463946     -.07223475      .16875837     -.27819730
 ref:  10      .02516391      .03748536     -.00598467     -.00321153      .01897579     -.10672853      .24288638      .22339110

                ci   9         ci  10         ci  11
 ref:   1      .00002740     -.01595927      .00084848
 ref:   2      .00938666      .00103904      .00321469
 ref:   3     -.01419992     -.00055673     -.00529896
 ref:   4      .00104468      .00168008      .00621673
 ref:   5      .07932849     -.35978628     -.04055740
 ref:   6      .01694490     -.19776092      .09588917
 ref:   7      .03587323     -.07112136     -.01568483
 ref:   8      .01747390     -.13881816     -.03970926
 ref:   9     -.40918781     -.08032089      .21017805
 ref:  10      .04617623     -.04106646     -.05739350

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 37  1    -93.3414571345  8.4421E-09  0.0000E+00  6.0836E-04  1.0000E-03
 mr-sdci # 37  2    -93.3414570652  2.3557E-08  0.0000E+00  6.4680E-04  1.0000E-03
 mr-sdci # 37  3    -93.3414568419  2.7577E-10  0.0000E+00  7.2573E-04  1.0000E-03
 mr-sdci # 37  4    -93.3414567355  4.9297E-11  0.0000E+00  7.6815E-04  1.0000E-03
 mr-sdci # 37  5    -93.1685347229  2.2127E-08  0.0000E+00  4.6634E-04  1.0000E-03
 mr-sdci # 37  6    -93.1685312191  2.4489E-07  0.0000E+00  5.5055E-04  1.0000E-03
 mr-sdci # 37  7    -93.1685243838  1.4812E-06  0.0000E+00  7.4482E-04  1.0000E-03
 mr-sdci # 37  8    -93.1685198989  3.4965E-06  0.0000E+00  5.6950E-04  1.0000E-03
 mr-sdci # 37  9    -93.1315828117  3.2219E-04  1.6728E-02  4.0557E-02  1.0000E-03
 mr-sdci # 37 10    -93.1270764817  1.8146E-05  0.0000E+00  5.1849E-02  1.0000E-03
 mr-sdci # 37 11    -92.9625431349 -1.6013E-01  0.0000E+00  2.2385E-01  1.0000E-04
 
 root number  9 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .34    .12    .00    .34
    2   25    0    .19    .07    .00    .19
    3   26    0   5.13    .19    .00   5.12
    4   11    0   2.22   1.24    .00   2.22
    5   15    0   4.54   1.66    .00   4.54
    6   16    0   2.66    .94    .00   2.65
    7    1    0    .49    .28    .00    .49
    8    5    0   2.26    .82    .00   2.25
    9    6    0   8.30    .73    .00   8.30
   10    7    0   1.73    .20    .00   1.73
   11   75    0    .08    .06    .00    .08
   12   45    0    .20    .11    .00    .20
   13   46    0    .58    .08    .00    .57
   14   47    0    .23    .03    .00    .23
   15   81    0    .53    .00    .00    .52
   16   82    0   1.59    .00    .00   1.59
   17  101    0    .27    .00    .00    .26
   18   83    0   1.60    .00    .00   1.60
   19  102    0   3.99    .00    .00   3.99
   20   84    0    .43    .00    .00    .43
   21   91    0    .63    .00    .00    .62
   22   93    0   2.28    .00    .00   2.27
   23   94    0   1.17    .00    .00   1.17
   24  103    0   2.96    .00    .00   2.95
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .050000
time for cinew                         1.180000
time for eigenvalue solver              .000000
time for vector access                  .000000
================================================================
time spent in mult:                    44.4000s 
time spent in multnx:                  44.3100s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.5300s 
time for vector access in mult:          .0100s 
total time per CI iteration:           45.6500s 

          starting ci iteration  38

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1      .00613522      .27226622      .94592287      .02065007     -.00088922     -.08944778     -.05989815     -.00437358
 ref:   2      .62238674     -.29396220      .06527283      .70094192      .07538590     -.03287438      .04389223      .05409869
 ref:   3     -.66540542      .21692146     -.07313857      .68862603      .07453797      .01958550     -.02506020     -.07099889
 ref:   4      .37308716      .87279461     -.25492921      .05850881      .02022317      .04641330     -.07384094      .05995154
 ref:   5     -.00044937     -.00028183     -.00154091     -.00001578     -.00303858     -.07076234     -.03552766      .01192402
 ref:   6      .00030458      .00016297     -.00081671     -.00002616     -.00262539     -.03954576     -.00861750     -.02144850
 ref:   7     -.00074844      .00142804     -.00041996     -.00004829     -.00254284     -.04153547      .06148997      .01276497
 ref:   8      .00015851      .00979426      .03430343      .00070951      .00236421      .23022674      .15016770      .01104741
 ref:   9     -.04012765      .01768459      .00161764      .00210570     -.01586308     -.07561486      .17498559      .27563185
 ref:  10      .02252286      .03907504     -.00619130     -.00318527     -.01903657     -.10595592      .23734312     -.22981042

                ci   9         ci  10         ci  11         ci  12
 ref:   1      .00013298     -.01595918      .00022398      .00089906
 ref:   2      .00493566      .00101966     -.01186095      .00161238
 ref:   3     -.00780184     -.00052772      .01720124     -.00302429
 ref:   4      .00007801      .00167748     -.00260590      .00588797
 ref:   5      .13118037     -.35986894      .09435875     -.01920469
 ref:   6     -.00428870     -.19780734     -.05204820      .08873200
 ref:   7      .12892181     -.07109725      .18835330      .02080632
 ref:   8     -.00455003     -.13886576     -.04781226     -.04808951
 ref:   9     -.63046079     -.07984838     -.38319948      .11871659
 ref:  10      .12324697     -.04107322      .15464248     -.02709508

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 38  1    -93.3414571603  2.5805E-08  0.0000E+00  6.0034E-04  1.0000E-03
 mr-sdci # 38  2    -93.3414570662  1.0684E-09  0.0000E+00  6.4586E-04  1.0000E-03
 mr-sdci # 38  3    -93.3414568428  8.3844E-10  0.0000E+00  7.2510E-04  1.0000E-03
 mr-sdci # 38  4    -93.3414567355  3.8622E-11  0.0000E+00  7.6819E-04  1.0000E-03
 mr-sdci # 38  5    -93.1685347268  3.9483E-09  0.0000E+00  4.6493E-04  1.0000E-03
 mr-sdci # 38  6    -93.1685312472  2.8073E-08  0.0000E+00  5.4698E-04  1.0000E-03
 mr-sdci # 38  7    -93.1685244196  3.5789E-08  0.0000E+00  7.4545E-04  1.0000E-03
 mr-sdci # 38  8    -93.1685201452  2.4634E-07  0.0000E+00  6.2193E-04  1.0000E-03
 mr-sdci # 38  9    -93.1412491120  9.6663E-03  6.4494E-03  4.6935E-02  1.0000E-03
 mr-sdci # 38 10    -93.1270764992  1.7550E-08  0.0000E+00  5.1849E-02  1.0000E-03
 mr-sdci # 38 11    -93.0885245293  1.2598E-01  0.0000E+00  1.2979E-01  1.0000E-04
 mr-sdci # 38 12    -92.9588699025 -2.5373E-02  0.0000E+00  2.2302E-01  1.0000E-04
 
 root number  9 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .34    .12    .00    .33
    2   25    0    .19    .07    .00    .18
    3   26    0   5.14    .19    .00   5.13
    4   11    0   2.22   1.26    .00   2.21
    5   15    0   4.56   1.67    .00   4.55
    6   16    0   2.66    .93    .00   2.66
    7    1    0    .48    .27    .00    .48
    8    5    0   2.15    .79    .00   2.15
    9    6    0   8.18    .73    .00   8.17
   10    7    0   1.72    .19    .00   1.72
   11   75    0    .08    .05    .00    .08
   12   45    0    .20    .10    .00    .20
   13   46    0    .57    .08    .00    .57
   14   47    0    .22    .04    .00    .22
   15   81    0    .53    .00    .00    .53
   16   82    0   1.59    .00    .00   1.58
   17  101    0    .26    .00    .00    .25
   18   83    0   1.64    .00    .00   1.64
   19  102    0   4.09    .00    .00   4.08
   20   84    0    .43    .00    .00    .43
   21   91    0    .64    .00    .00    .63
   22   93    0   2.27    .00    .00   2.27
   23   94    0   1.15    .00    .00   1.14
   24  103    0   2.84    .00    .00   2.83
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .050000
time for cinew                          .570000
time for eigenvalue solver              .000000
time for vector access                  .000000
================================================================
time spent in mult:                    44.1500s 
time spent in multnx:                  44.0300s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.4900s 
time for vector access in mult:          .0100s 
total time per CI iteration:           44.7800s 

          starting ci iteration  39

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     -.00278969     -.27315359      .94587054      .00844424      .00088145      .08910998      .06047463      .00317312
 ref:   2      .61455947      .29105658      .09216801     -.70602165     -.07539996      .03361560     -.04556566     -.05222999
 ref:   3     -.67348034     -.21545374     -.05809552     -.68264683     -.07452441     -.02042685      .02740960      .06992896
 ref:   4      .37161436     -.87385410     -.25065426     -.06950444     -.02022093     -.04617230      .07148518     -.06292155
 ref:   5     -.00043293      .00028490     -.00154592     -.00002338      .00303113      .07062526      .03556757     -.01213172
 ref:   6      .00029158     -.00016468     -.00080176     -.00002475      .00262477      .03921955      .00965083      .02180091
 ref:   7     -.00074655     -.00142495     -.00043063      .00004851      .00253397      .04185772     -.06154597     -.00982730
 ref:   8     -.00018176     -.00982749      .03431133      .00032597     -.00234281     -.22944492     -.15155731     -.00783039
 ref:   9     -.04018771     -.01755409      .00129859     -.00154036      .01579722      .07972666     -.18472926     -.27079473
 ref:  10      .02248761     -.03913915     -.00613329      .00279035      .01904119      .10474131     -.22861802      .23918794

                ci   9         ci  10         ci  11         ci  12         ci  13
 ref:   1      .00039351     -.01594114      .00054763      .00098251      .00017199
 ref:   2      .00068139      .00151279      .01341139      .00044655      .00037486
 ref:   3     -.00113578     -.00128977     -.02027915     -.00203982      .00062262
 ref:   4     -.00050393      .00173816      .00226565      .00380792      .00479192
 ref:   5      .13729425     -.35681473     -.00590775      .05193135     -.11233673
 ref:   6      .03049268     -.19909402     -.00484866     -.02362676      .22438611
 ref:   7      .14832842     -.07026963     -.09223303      .11599413     -.13028685
 ref:   8      .02869796     -.14027241     -.01086653     -.12629909      .12776308
 ref:   9     -.71187043     -.08842179      .17038075     -.01101997      .16315541
 ref:  10      .12745722     -.03929970     -.05826035      .07994910     -.17147912

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 39  1    -93.3414571644  4.0985E-09  0.0000E+00  6.0065E-04  1.0000E-03
 mr-sdci # 39  2    -93.3414570663  2.4155E-11  0.0000E+00  6.4599E-04  1.0000E-03
 mr-sdci # 39  3    -93.3414568445  1.7541E-09  0.0000E+00  7.2441E-04  1.0000E-03
 mr-sdci # 39  4    -93.3414567414  5.8443E-09  0.0000E+00  7.6567E-04  1.0000E-03
 mr-sdci # 39  5    -93.1685347269  8.0504E-12  0.0000E+00  4.6491E-04  1.0000E-03
 mr-sdci # 39  6    -93.1685312731  2.5948E-08  0.0000E+00  5.5280E-04  1.0000E-03
 mr-sdci # 39  7    -93.1685244642  4.4586E-08  0.0000E+00  7.4658E-04  1.0000E-03
 mr-sdci # 39  8    -93.1685205893  4.4409E-07  0.0000E+00  6.0781E-04  1.0000E-03
 mr-sdci # 39  9    -93.1491438099  7.8947E-03  3.9280E-03  2.7913E-02  1.0000E-03
 mr-sdci # 39 10    -93.1271013122  2.4813E-05  0.0000E+00  5.1853E-02  1.0000E-03
 mr-sdci # 39 11    -93.1118223820  2.3298E-02  0.0000E+00  6.5117E-02  1.0000E-04
 mr-sdci # 39 12    -92.9778425971  1.8973E-02  0.0000E+00  2.1076E-01  1.0000E-04
 mr-sdci # 39 13    -92.8962542230 -7.4812E-02  0.0000E+00  2.5352E-01  1.0000E-04
 
 root number  9 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .32    .11    .00    .32
    2   25    0    .20    .07    .00    .19
    3   26    0   5.10    .18    .00   5.10
    4   11    0   2.21   1.24    .00   2.21
    5   15    0   4.56   1.67    .00   4.55
    6   16    0   2.66    .93    .00   2.66
    7    1    0    .50    .28    .00    .50
    8    5    0   2.26    .83    .00   2.26
    9    6    0   8.27    .73    .00   8.27
   10    7    0   1.73    .20    .00   1.73
   11   75    0    .08    .05    .00    .08
   12   45    0    .20    .11    .00    .20
   13   46    0    .58    .08    .00    .57
   14   47    0    .23    .04    .00    .23
   15   81    0    .53    .00    .00    .52
   16   82    0   1.62    .00    .00   1.61
   17  101    0    .27    .00    .00    .27
   18   83    0   1.66    .00    .00   1.65
   19  102    0   4.13    .00    .00   4.12
   20   84    0    .43    .00    .00    .43
   21   91    0    .63    .00    .00    .63
   22   93    0   2.27    .00    .00   2.27
   23   94    0   1.18    .00    .00   1.17
   24  103    0   2.95    .00    .00   2.95
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .050000
time for cinew                          .710000
time for eigenvalue solver              .000000
time for vector access                  .000000
================================================================
time spent in mult:                    44.5700s 
time spent in multnx:                  44.4900s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.5200s 
time for vector access in mult:          .0000s 
total time per CI iteration:           45.3400s 

          starting ci iteration  40

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1      .00246036      .26550742     -.94644126      .05578057      .00088850     -.08886946     -.06090948      .00041086
 ref:   2      .62788239     -.28746919     -.11976597     -.69147892     -.07534645     -.03414399      .04826704     -.04950043
 ref:   3     -.66970004      .19964304      .01338122     -.69342833     -.07457938      .02096249     -.03111905      .06817614
 ref:   4      .35584486      .88112323      .24309496     -.08530896     -.02021767      .04600978     -.06772836     -.06706575
 ref:   5     -.00044573     -.00027853      .00154248     -.00010374      .00304452     -.07053806     -.03523816     -.01334900
 ref:   6      .00030091      .00016477      .00077764     -.00009513      .00262729     -.03909608     -.01081744      .02107103
 ref:   7     -.00077272      .00141585      .00041080      .00000522      .00256471     -.04210202      .06170735     -.00587473
 ref:   8      .00002330      .00954167     -.03434702      .00202229     -.00235850      .22881040      .15276526     -.00119038
 ref:   9     -.04039497      .01686517     -.00199434     -.00207545      .01606214     -.08241407      .19983833     -.26172867
 ref:  10      .02174602      .03957528      .00619561      .00243209      .01898550     -.10389924      .21442225      .25276183

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14
 ref:   1      .00049549      .01593188     -.00018949     -.00148997      .00130109      .00067623
 ref:   2     -.00146058     -.00118672      .01443992      .00192834     -.00032841     -.00088224
 ref:   3      .00179880      .00082197     -.02124010     -.00079260     -.00142046      .00176195
 ref:   4     -.00108831     -.00166716      .00286813      .00288975      .00300978      .00435997
 ref:   5      .13573837      .35763399      .00180176     -.00031603      .05880177     -.10832508
 ref:   6     -.00529838      .20178551      .05276530      .22024505     -.08070005      .16533358
 ref:   7      .14946337      .07016030     -.06100934      .07379317      .11134600     -.13720099
 ref:   8      .00583277      .14194663      .02766601      .12934964     -.16188013      .08208841
 ref:   9     -.75505715      .08831158      .12629440      .10052514     -.05055940      .11376586
 ref:  10      .16106047      .03704953     -.08898019     -.14730102      .12324062     -.11998173

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 40  1    -93.3414571673  2.8814E-09  0.0000E+00  6.0163E-04  1.0000E-03
 mr-sdci # 40  2    -93.3414570671  8.7073E-10  0.0000E+00  6.4550E-04  1.0000E-03
 mr-sdci # 40  3    -93.3414568484  3.8746E-09  0.0000E+00  7.2317E-04  1.0000E-03
 mr-sdci # 40  4    -93.3414567486  7.2439E-09  0.0000E+00  7.6285E-04  1.0000E-03
 mr-sdci # 40  5    -93.1685347270  1.2086E-10  0.0000E+00  4.6468E-04  1.0000E-03
 mr-sdci # 40  6    -93.1685312817  8.5099E-09  0.0000E+00  5.5171E-04  1.0000E-03
 mr-sdci # 40  7    -93.1685245404  7.6176E-08  0.0000E+00  7.3467E-04  1.0000E-03
 mr-sdci # 40  8    -93.1685211153  5.2604E-07  0.0000E+00  6.4851E-04  1.0000E-03
 mr-sdci # 40  9    -93.1518202349  2.6764E-03  1.5300E-03  2.0426E-02  1.0000E-03
 mr-sdci # 40 10    -93.1271099154  8.6032E-06  0.0000E+00  5.1842E-02  1.0000E-03
 mr-sdci # 40 11    -93.1185328026  6.7104E-03  0.0000E+00  5.0356E-02  1.0000E-04
 mr-sdci # 40 12    -93.0329028041  5.5060E-02  0.0000E+00  1.9497E-01  1.0000E-04
 mr-sdci # 40 13    -92.9752071080  7.8953E-02  0.0000E+00  2.0252E-01  1.0000E-04
 mr-sdci # 40 14    -92.8857445975 -4.3803E-02  0.0000E+00  2.6228E-01  1.0000E-04
 
 root number  9 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .34    .12    .00    .34
    2   25    0    .19    .06    .00    .19
    3   26    0   5.16    .19    .00   5.16
    4   11    0   2.22   1.25    .00   2.22
    5   15    0   4.54   1.66    .00   4.54
    6   16    0   2.65    .94    .00   2.65
    7    1    0    .48    .27    .00    .48
    8    5    0   2.27    .84    .00   2.26
    9    6    0   8.01    .70    .00   8.01
   10    7    0   1.71    .20    .00   1.71
   11   75    0    .09    .05    .00    .08
   12   45    0    .20    .10    .00    .20
   13   46    0    .58    .09    .00    .58
   14   47    0    .23    .04    .00    .23
   15   81    0    .54    .00    .00    .54
   16   82    0   1.61    .00    .00   1.61
   17  101    0    .28    .00    .00    .27
   18   83    0   1.64    .00    .00   1.64
   19  102    0   4.15    .00    .00   4.14
   20   84    0    .43    .00    .00    .43
   21   91    0    .64    .00    .00    .63
   22   93    0   2.28    .00    .00   2.28
   23   94    0   1.17    .00    .00   1.17
   24  103    0   2.96    .00    .00   2.95
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .040000
time for cinew                          .800000
time for eigenvalue solver              .000000
time for vector access                  .000000
================================================================
time spent in mult:                    44.3700s 
time spent in multnx:                  44.3100s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.5100s 
time for vector access in mult:          .0000s 
total time per CI iteration:           45.2200s 

          starting ci iteration  41

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     -.00443296      .26382827      .94644858     -.06303169      .00075084     -.08775946      .06192248      .00848583
 ref:   2     -.64308482     -.27445280      .11897256      .68288106     -.07562508     -.03646890     -.05587747      .03818118
 ref:   3      .66649445      .18269610     -.00108740      .70127346     -.07429975      .02403011      .04315463     -.06055204
 ref:   4     -.33402973      .88941586     -.24382194      .08538759     -.02021196      .04485658      .05270021      .08008522
 ref:   5      .00045180     -.00028615     -.00153789      .00012087      .00289071     -.07026939      .03385682      .01734932
 ref:   6     -.00025505      .00017771     -.00078531      .00008921      .00261611     -.03806296      .01571815     -.02015859
 ref:   7      .00078422      .00139505     -.00039646      .00001636      .00237017     -.04334990     -.06075338     -.00719947
 ref:   8     -.00006803      .00948307      .03434280     -.00229255     -.00198765      .22612779     -.15525581     -.02133759
 ref:   9      .04074242      .01591816      .00241900      .00253661      .01482604     -.09545766     -.24520666      .21849205
 ref:  10     -.02072775      .04008644     -.00633496     -.00253300      .01920356     -.09855125     -.15963129     -.29282367

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15
 ref:   1      .00004998      .00089810     -.01592455      .00083136     -.00248708      .00007378     -.00260802
 ref:   2     -.00350626      .01219248      .00134307     -.00669955      .00173352     -.00015905      .00074464
 ref:   3      .00459327     -.01732656     -.00104568      .01105925     -.00027008      .00118842     -.00229494
 ref:   4     -.00153596      .00260116      .00170048     -.00046419      .00141619     -.00532059     -.00237026
 ref:   5      .12125773      .05867834     -.35710684      .04541798     -.06656415      .00971083      .09947822
 ref:   6      .01131608      .00880537     -.20155980     -.04314029      .29674689     -.14503375      .02131708
 ref:   7      .11188969      .07256919     -.06963908      .18137347     -.08587177     -.00886738      .08775393
 ref:   8      .02515585     -.01441350     -.14201938     -.05080966      .24590879      .00062301      .01849555
 ref:   9     -.71238723     -.13546594     -.08910166     -.21473489      .19705717     -.11253420      .03609298
 ref:  10      .17293777     -.04910798     -.03780858      .02935545     -.21082721      .01476881      .08988967

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 41  1    -93.3414571847  1.7464E-08  0.0000E+00  5.9570E-04  1.0000E-03
 mr-sdci # 41  2    -93.3414570675  3.6039E-10  0.0000E+00  6.4496E-04  1.0000E-03
 mr-sdci # 41  3    -93.3414568489  5.2495E-10  0.0000E+00  7.2318E-04  1.0000E-03
 mr-sdci # 41  4    -93.3414567499  1.2711E-09  0.0000E+00  7.6253E-04  1.0000E-03
 mr-sdci # 41  5    -93.1685347279  9.0402E-10  0.0000E+00  4.6525E-04  1.0000E-03
 mr-sdci # 41  6    -93.1685313588  7.7110E-08  0.0000E+00  5.4197E-04  1.0000E-03
 mr-sdci # 41  7    -93.1685247718  2.3146E-07  0.0000E+00  7.1315E-04  1.0000E-03
 mr-sdci # 41  8    -93.1685223910  1.2757E-06  0.0000E+00  8.0444E-04  1.0000E-03
 mr-sdci # 41  9    -93.1549755738  3.1553E-03  3.5471E-03  2.9820E-02  1.0000E-03
 mr-sdci # 41 10    -93.1298625381  2.7526E-03  0.0000E+00  6.9512E-02  1.0000E-03
 mr-sdci # 41 11    -93.1271095992  8.5768E-03  0.0000E+00  5.1844E-02  1.0000E-04
 mr-sdci # 41 12    -93.0983283839  6.5426E-02  0.0000E+00  1.2235E-01  1.0000E-04
 mr-sdci # 41 13    -92.9987745376  2.3567E-02  0.0000E+00  1.7315E-01  1.0000E-04
 mr-sdci # 41 14    -92.9547289528  6.8984E-02  0.0000E+00  2.1750E-01  1.0000E-04
 mr-sdci # 41 15    -92.7734141209 -6.9389E-03  0.0000E+00  2.5846E-01  1.0000E-04
 
 root number  9 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .33    .12    .00    .32
    2   25    0    .20    .07    .00    .19
    3   26    0   5.15    .19    .00   5.15
    4   11    0   2.22   1.24    .00   2.21
    5   15    0   4.54   1.68    .00   4.54
    6   16    0   2.65    .91    .00   2.65
    7    1    0    .48    .27    .00    .48
    8    5    0   2.25    .82    .00   2.24
    9    6    0   8.27    .74    .00   8.26
   10    7    0   1.73    .20    .00   1.73
   11   75    0    .08    .05    .00    .07
   12   45    0    .21    .11    .00    .20
   13   46    0    .57    .08    .00    .57
   14   47    0    .22    .04    .00    .22
   15   81    0    .54    .00    .00    .53
   16   82    0   1.61    .00    .00   1.61
   17  101    0    .27    .00    .00    .27
   18   83    0   1.66    .00    .00   1.66
   19  102    0   4.08    .00    .00   4.08
   20   84    0    .43    .00    .00    .43
   21   91    0    .61    .00    .00    .61
   22   93    0   2.25    .00    .00   2.25
   23   94    0   1.18    .00    .00   1.17
   24  103    0   2.92    .00    .00   2.92
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .060000
time for cinew                          .770000
time for eigenvalue solver              .010000
time for vector access                  .000000
================================================================
time spent in mult:                    44.4500s 
time spent in multnx:                  44.3600s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.5200s 
time for vector access in mult:          .0000s 
total time per CI iteration:           45.2900s 

          starting ci iteration  42

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     -.00180393     -.26455625     -.94536472     -.07520266     -.00018278      .08631959      .05272363      .03711820
 ref:   2     -.64140658      .27301796     -.12952075      .68311538      .07685962      .03929262     -.06468915      .00107625
 ref:   3      .66850470     -.18230886     -.00591171      .69943427      .07293112     -.02935466      .06819759     -.02839532
 ref:   4     -.33326318     -.88972062      .24258003      .08868172      .02054860     -.04202280      .00034182      .09708866
 ref:   5      .00044023      .00028945      .00154583      .00012870     -.00215220      .07027173      .02294889      .03063568
 ref:   6     -.00025692     -.00017741      .00078395      .00009924     -.00263691      .03643106      .02603309     -.01005334
 ref:   7      .00078605     -.00139390      .00039831      .00001982     -.00164598      .04463678     -.04558371     -.03951943
 ref:   8      .00002762     -.00950944     -.03430367     -.00273381      .00045883     -.22264105     -.13205576     -.09325112
 ref:   9      .04076713     -.01586607     -.00232156      .00238030     -.00886652      .11565959     -.32225764      .05577314
 ref:  10     -.02069955     -.04010649      .00634801     -.00243200     -.02138538      .08652988      .02489060     -.33654393

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1     -.00049661     -.00017987     -.01594937     -.00001066     -.00286855     -.00092595      .00324979     -.00080325
 ref:   2      .00429056      .00385111      .00083303     -.01283663      .00157993     -.00046740      .00148203      .00183646
 ref:   3     -.00578605     -.00533897     -.00031544      .01926535     -.00035044     -.00099024     -.00061709     -.00311646
 ref:   4      .00171651      .00096618      .00159703     -.00187069      .00157959      .00458694      .00298734     -.00181326
 ref:   5     -.03303769      .15254643     -.35824697      .05305487     -.05246685      .03933178     -.15686637      .00885820
 ref:   6     -.01063942      .00260896     -.20185382     -.02295779      .30547031      .11740071      .05294815      .03092713
 ref:   7     -.03442034      .15983020     -.07126968      .10519495     -.10435622     -.02481008      .07082970      .14159683
 ref:   8     -.03695306     -.01324116     -.14155167     -.02103626      .24906572     -.01128282     -.00662739      .01337431
 ref:   9      .53883498     -.50021643     -.08582900     -.12077642      .23175190      .16120288     -.14200478     -.07159929
 ref:  10     -.13700449      .08156975     -.03532680      .07903358     -.19813037      .03757571     -.13127760      .02914789

 trial vector basis is being transformed.  new dimension:  10

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 42  1    -93.3414571855  7.5995E-10  0.0000E+00  5.9621E-04  1.0000E-03
 mr-sdci # 42  2    -93.3414570675  3.1893E-11  0.0000E+00  6.4506E-04  1.0000E-03
 mr-sdci # 42  3    -93.3414568499  9.9695E-10  0.0000E+00  7.2316E-04  1.0000E-03
 mr-sdci # 42  4    -93.3414567515  1.5721E-09  0.0000E+00  7.6193E-04  1.0000E-03
 mr-sdci # 42  5    -93.1685347397  1.1858E-08  0.0000E+00  4.6420E-04  1.0000E-03
 mr-sdci # 42  6    -93.1685314548  9.6070E-08  0.0000E+00  5.6724E-04  1.0000E-03
 mr-sdci # 42  7    -93.1685254933  7.2142E-07  0.0000E+00  9.3741E-04  1.0000E-03
 mr-sdci # 42  8    -93.1685238788  1.4878E-06  0.0000E+00  8.4073E-04  1.0000E-03
 mr-sdci # 42  9    -93.1610769210  6.1013E-03  3.5110E-03  3.2540E-02  1.0000E-03
 mr-sdci # 42 10    -93.1453923387  1.5530E-02  0.0000E+00  3.4579E-02  1.0000E-03
 mr-sdci # 42 11    -93.1271136089  4.0097E-06  0.0000E+00  5.1836E-02  1.0000E-04
 mr-sdci # 42 12    -93.1147447029  1.6416E-02  0.0000E+00  5.5609E-02  1.0000E-04
 mr-sdci # 42 13    -93.0000517141  1.2772E-03  0.0000E+00  1.6969E-01  1.0000E-04
 mr-sdci # 42 14    -92.9617990231  7.0701E-03  0.0000E+00  2.0513E-01  1.0000E-04
 mr-sdci # 42 15    -92.8938661188  1.2045E-01  0.0000E+00  2.5815E-01  1.0000E-04
 mr-sdci # 42 16    -92.7208139702  9.7412E-03  0.0000E+00  2.3976E-01  1.0000E-04
 
 root number  9 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .32    .12    .00    .32
    2   25    0    .20    .07    .00    .19
    3   26    0   5.14    .19    .00   5.14
    4   11    0   2.19   1.23    .00   2.18
    5   15    0   4.54   1.65    .00   4.54
    6   16    0   2.68    .93    .00   2.67
    7    1    0    .49    .28    .00    .49
    8    5    0   2.26    .83    .00   2.26
    9    6    0   8.30    .75    .00   8.29
   10    7    0   1.72    .20    .00   1.71
   11   75    0    .09    .05    .00    .08
   12   45    0    .20    .10    .00    .20
   13   46    0    .57    .08    .00    .57
   14   47    0    .23    .04    .00    .23
   15   81    0    .52    .00    .00    .52
   16   82    0   1.61    .00    .00   1.60
   17  101    0    .28    .00    .00    .27
   18   83    0   1.66    .00    .00   1.65
   19  102    0   4.07    .00    .00   4.06
   20   84    0    .43    .00    .00    .43
   21   91    0    .61    .00    .00    .60
   22   93    0   2.25    .00    .00   2.25
   23   94    0   1.14    .00    .00   1.13
   24  103    0   2.94    .00    .00   2.94
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .030000
time for cinew                          .810000
time for eigenvalue solver              .000000
time for vector access                  .000000
================================================================
time spent in mult:                    44.4400s 
time spent in multnx:                  44.3200s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.5200s 
time for vector access in mult:          .0100s 
total time per CI iteration:           45.3000s 

          starting ci iteration  43

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     -.01764327     -.25816044      .94644264      .08155746      .00020871      .07763748      .05369225      .05194389
 ref:   2     -.65460774      .25592161      .11616857     -.67960307      .07837591      .04967226     -.05032142     -.02241804
 ref:   3      .67041136     -.16229976      .02873316     -.70196740      .07120731     -.04522641      .06727850     -.00232758
 ref:   4     -.30181324     -.90049104     -.24350891     -.09002609      .02086587     -.03300248     -.04118286      .09162780
 ref:   5      .00050531      .00028579     -.00154994     -.00013809     -.00152707      .06681805      .02576914      .03557541
 ref:   6     -.00023755     -.00018910     -.00078595     -.00010435     -.00274721      .02983711      .03486580     -.00082914
 ref:   7      .00082218     -.00137116     -.00036272     -.00002780     -.00067996      .04909284     -.01953678     -.05359640
 ref:   8     -.00054298     -.00927585      .03434165      .00296467     -.00063764     -.20109461     -.13501349     -.13039343
 ref:   9      .04118137     -.01467656      .00344752     -.00252250     -.00148197      .18105356     -.29455363     -.06194196
 ref:  10     -.01935221     -.04073291     -.00658357      .00242503     -.02369989      .05008881      .15690965     -.30795439

                ci   9         ci  10         ci  11
 ref:   1      .00018517     -.00007677      .00155848
 ref:   2     -.00320279     -.00247827     -.00456870
 ref:   3      .00417845      .00320523      .00937128
 ref:   4     -.00105016     -.00045411     -.00120204
 ref:   5      .02942080     -.13846358     -.17706334
 ref:   6      .00681390     -.00673434      .02359131
 ref:   7      .00437302     -.16656967      .02487308
 ref:   8      .04146964      .00915923     -.01703361
 ref:   9     -.41363838      .60742530     -.24688995
 ref:  10      .12616586     -.09744973     -.05371630

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 43  1    -93.3414572059  2.0416E-08  0.0000E+00  5.9319E-04  1.0000E-03
 mr-sdci # 43  2    -93.3414570682  6.5596E-10  0.0000E+00  6.4434E-04  1.0000E-03
 mr-sdci # 43  3    -93.3414568536  3.6149E-09  0.0000E+00  7.2217E-04  1.0000E-03
 mr-sdci # 43  4    -93.3414567516  1.3369E-10  0.0000E+00  7.6165E-04  1.0000E-03
 mr-sdci # 43  5    -93.1685347455  5.8084E-09  0.0000E+00  4.6518E-04  1.0000E-03
 mr-sdci # 43  6    -93.1685317441  2.8931E-07  0.0000E+00  5.7934E-04  1.0000E-03
 mr-sdci # 43  7    -93.1685284026  2.9093E-06  0.0000E+00  8.3595E-04  1.0000E-03
 mr-sdci # 43  8    -93.1685241661  2.8725E-07  0.0000E+00  7.7570E-04  1.0000E-03
 mr-sdci # 43  9    -93.1638836593  2.8067E-03  1.3524E-03  1.8295E-02  1.0000E-03
 mr-sdci # 43 10    -93.1477622403  2.3699E-03  0.0000E+00  1.6676E-02  1.0000E-03
 mr-sdci # 43 11    -92.9567647348 -1.7035E-01  0.0000E+00  2.1991E-01  1.0000E-04
 
 root number  9 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .33    .12    .00    .33
    2   25    0    .19    .05    .00    .19
    3   26    0   5.16    .19    .00   5.16
    4   11    0   2.21   1.25    .00   2.21
    5   15    0   4.51   1.66    .00   4.50
    6   16    0   2.65    .92    .00   2.65
    7    1    0    .48    .28    .00    .48
    8    5    0   2.27    .84    .00   2.27
    9    6    0   8.28    .73    .00   8.28
   10    7    0   1.72    .19    .00   1.72
   11   75    0    .08    .06    .00    .08
   12   45    0    .20    .09    .00    .19
   13   46    0    .57    .09    .00    .57
   14   47    0    .24    .03    .00    .24
   15   81    0    .53    .00    .00    .53
   16   82    0   1.60    .00    .00   1.60
   17  101    0    .28    .00    .00    .27
   18   83    0   1.65    .00    .00   1.65
   19  102    0   4.13    .00    .00   4.13
   20   84    0    .43    .00    .00    .42
   21   91    0    .64    .00    .00    .63
   22   93    0   2.27    .00    .00   2.27
   23   94    0   1.17    .00    .00   1.17
   24  103    0   2.94    .00    .00   2.93
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .040000
time for cinew                          .560000
time for eigenvalue solver              .000000
time for vector access                  .000000
================================================================
time spent in mult:                    44.5300s 
time spent in multnx:                  44.4700s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.5000s 
time for vector access in mult:          .0000s 
total time per CI iteration:           45.1300s 

          starting ci iteration  44

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     -.01132438     -.25833275      .94636198     -.08305653      .00324765      .03490819     -.08592109      .05474680
 ref:   2     -.65382857      .23537391      .11677691      .68762811      .08734582      .05618851      .00808223     -.02822511
 ref:   3      .68194411     -.14758115      .02880359      .69405780      .05907512     -.08529783     -.02939263      .00468406
 ref:   4     -.27693785     -.90854345     -.24352315      .08903565      .02279603      .00088094      .05755258      .08822522
 ref:   5      .00046677      .00030780     -.00155031      .00012296      .00321284      .04213438     -.05758436      .03627094
 ref:   6     -.00022770     -.00019679     -.00078581      .00010780     -.00332460      .00393972     -.04575415      .00161126
 ref:   7      .00086208     -.00134994     -.00036321      .00001416      .00554528      .04716422     -.01619754     -.05625341
 ref:   8     -.00032114     -.00927812      .03433865     -.00302150     -.00899862     -.09306157      .21918557     -.13738285
 ref:   9      .04155327     -.01358098      .00343109      .00200542      .04702085      .31666836      .13025173     -.09187167
 ref:  10     -.01830984     -.04121526     -.00658550     -.00241793     -.03892981     -.07025869     -.17433914     -.29368196

                ci   9         ci  10         ci  11         ci  12
 ref:   1      .00026104      .00003627      .00124173      .00204100
 ref:   2     -.00147789     -.00165109      .00604806     -.00296587
 ref:   3      .00192961      .00199266     -.01018856      .00671206
 ref:   4     -.00001614     -.00012700      .00207040     -.00063752
 ref:   5      .00458290     -.15146325     -.08847766     -.21582852
 ref:   6      .00783973     -.00580061      .01195562      .02863992
 ref:   7     -.00362847     -.16355521      .03468701      .03521514
 ref:   8      .03898369      .00363358     -.02712169     -.02596312
 ref:   9     -.37580343      .62590145     -.08489266     -.28451835
 ref:  10      .10506074     -.11440895     -.07673681     -.08060268

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 44  1    -93.3414572186  1.2711E-08  0.0000E+00  5.9298E-04  1.0000E-03
 mr-sdci # 44  2    -93.3414570693  1.1392E-09  0.0000E+00  6.4438E-04  1.0000E-03
 mr-sdci # 44  3    -93.3414568536  1.2861E-12  0.0000E+00  7.2217E-04  1.0000E-03
 mr-sdci # 44  4    -93.3414567535  1.8736E-09  0.0000E+00  7.6107E-04  1.0000E-03
 mr-sdci # 44  5    -93.1685347866  4.1061E-08  0.0000E+00  4.7478E-04  1.0000E-03
 mr-sdci # 44  6    -93.1685334528  1.7087E-06  0.0000E+00  6.8236E-04  1.0000E-03
 mr-sdci # 44  7    -93.1685304368  2.0342E-06  0.0000E+00  6.6750E-04  1.0000E-03
 mr-sdci # 44  8    -93.1685242518  8.5735E-08  0.0000E+00  7.5065E-04  1.0000E-03
 mr-sdci # 44  9    -93.1654381810  1.5545E-03  3.4522E-04  1.1449E-02  1.0000E-03
 mr-sdci # 44 10    -93.1486967061  9.3447E-04  0.0000E+00  1.4810E-02  1.0000E-03
 mr-sdci # 44 11    -93.0345697778  7.7805E-02  0.0000E+00  1.5118E-01  1.0000E-04
 mr-sdci # 44 12    -92.9487741292 -1.6597E-01  0.0000E+00  2.2022E-01  1.0000E-04
 
 root number  9 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .34    .13    .00    .33
    2   25    0    .19    .07    .00    .19
    3   26    0   5.15    .19    .00   5.15
    4   11    0   2.22   1.26    .00   2.22
    5   15    0   4.52   1.69    .00   4.51
    6   16    0   2.58    .89    .00   2.57
    7    1    0    .50    .28    .00    .49
    8    5    0   2.27    .84    .00   2.27
    9    6    0   8.25    .74    .00   8.25
   10    7    0   1.73    .20    .00   1.72
   11   75    0    .09    .06    .00    .09
   12   45    0    .20    .10    .00    .20
   13   46    0    .57    .08    .00    .57
   14   47    0    .23    .04    .00    .23
   15   81    0    .53    .00    .00    .53
   16   82    0   1.60    .00    .00   1.59
   17  101    0    .28    .00    .00    .27
   18   83    0   1.66    .00    .00   1.66
   19  102    0   4.13    .00    .00   4.13
   20   84    0    .43    .00    .00    .43
   21   91    0    .63    .00    .00    .63
   22   93    0   2.25    .00    .00   2.25
   23   94    0   1.15    .00    .00   1.15
   24  103    0   2.90    .00    .00   2.89
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .050000
time for cinew                          .620000
time for eigenvalue solver              .000000
time for vector access                  .000000
================================================================
time spent in mult:                    44.4000s 
time spent in multnx:                  44.3200s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.5700s 
time for vector access in mult:          .0100s 
total time per CI iteration:           45.0800s 

          starting ci iteration  45

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     -.02311598     -.25752733     -.94685017     -.07735543     -.01025807     -.01930103      .08945235      .05593949
 ref:   2     -.65266033      .23156707     -.10359478      .69212744     -.10247516     -.01488569      .00395956     -.03015500
 ref:   3      .68632066     -.14394013     -.03401436      .69026179     -.01674012      .10514499      .01623153      .00717539
 ref:   4     -.26800133     -.91033272      .24689565      .08881940     -.02766896     -.00145877     -.05778739      .08665530
 ref:   5      .00047862      .00030930      .00154316      .00011010     -.01550288     -.02951115      .06298675      .03665848
 ref:   6     -.00021444     -.00019883      .00078945      .00010438      .00553456      .00149125      .04566744      .00262543
 ref:   7      .00081673     -.00134328      .00033795     -.00000010     -.02220550     -.03716799      .02465704     -.05682286
 ref:   8     -.00073666     -.00924886     -.03435477     -.00281363      .02861614      .05278775     -.22875725     -.14040597
 ref:   9      .04160344     -.01334490     -.00399048      .00178560     -.18195707     -.28280767     -.07396977     -.10243223
 ref:  10     -.01799304     -.04132303      .00677985     -.00236676      .08450436      .07877450      .16542247     -.28755724

                ci   9         ci  10         ci  11         ci  12         ci  13
 ref:   1      .00010023     -.00011037      .00079218      .00316001     -.00268026
 ref:   2     -.00082077     -.00123238     -.00684653     -.00091801     -.00176358
 ref:   3      .00112075      .00147039      .01025182      .00208624      .00698558
 ref:   4      .00040782     -.00004492     -.00148040      .00052460     -.00220577
 ref:   5      .00037112     -.15272142      .03737622     -.21666429     -.08671277
 ref:   6      .00770818     -.00579873     -.00609650      .02860171      .01180100
 ref:   7     -.01603028     -.17334089      .07185604      .10631754     -.21655981
 ref:   8      .04093170      .00472247     -.00418695     -.04742113      .04937354
 ref:   9     -.35779760      .63638428      .01568856     -.28831067     -.06439014
 ref:  10      .10155788     -.11637105      .02991848     -.11108723      .03252966

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 45  1    -93.3414572416  2.3021E-08  0.0000E+00  5.8726E-04  1.0000E-03
 mr-sdci # 45  2    -93.3414570694  3.5328E-11  0.0000E+00  6.4431E-04  1.0000E-03
 mr-sdci # 45  3    -93.3414568547  1.1183E-09  0.0000E+00  7.2165E-04  1.0000E-03
 mr-sdci # 45  4    -93.3414567538  3.2606E-10  0.0000E+00  7.6111E-04  1.0000E-03
 mr-sdci # 45  5    -93.1685349291  1.4251E-07  0.0000E+00  4.7089E-04  1.0000E-03
 mr-sdci # 45  6    -93.1685343585  9.0564E-07  0.0000E+00  4.7795E-04  1.0000E-03
 mr-sdci # 45  7    -93.1685307423  3.0554E-07  0.0000E+00  5.7688E-04  1.0000E-03
 mr-sdci # 45  8    -93.1685243149  6.3154E-08  0.0000E+00  7.3566E-04  1.0000E-03
 mr-sdci # 45  9    -93.1657770815  3.3890E-04  1.0990E-04  5.6193E-03  1.0000E-03
 mr-sdci # 45 10    -93.1490913839  3.9468E-04  0.0000E+00  1.0987E-02  1.0000E-03
 mr-sdci # 45 11    -93.0764238275  4.1854E-02  0.0000E+00  9.5941E-02  1.0000E-04
 mr-sdci # 45 12    -92.9694745246  2.0700E-02  0.0000E+00  1.9829E-01  1.0000E-04
 mr-sdci # 45 13    -92.7750225885 -2.2503E-01  0.0000E+00  2.6363E-01  1.0000E-04
 
 root number  9 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .35    .13    .00    .34
    2   25    0    .19    .07    .00    .19
    3   26    0   5.16    .19    .00   5.15
    4   11    0   2.16   1.20    .00   2.16
    5   15    0   4.45   1.65    .00   4.45
    6   16    0   2.67    .95    .00   2.67
    7    1    0    .47    .27    .00    .47
    8    5    0   2.29    .84    .00   2.28
    9    6    0   8.28    .73    .00   8.28
   10    7    0   1.72    .19    .00   1.72
   11   75    0    .08    .05    .00    .08
   12   45    0    .20    .11    .00    .20
   13   46    0    .58    .07    .00    .57
   14   47    0    .23    .04    .00    .23
   15   81    0    .53    .00    .00    .52
   16   82    0   1.60    .00    .00   1.59
   17  101    0    .28    .00    .00    .28
   18   83    0   1.65    .00    .00   1.65
   19  102    0   4.11    .00    .00   4.11
   20   84    0    .43    .00    .00    .43
   21   91    0    .63    .00    .00    .63
   22   93    0   2.28    .00    .00   2.27
   23   94    0   1.18    .00    .00   1.17
   24  103    0   2.94    .00    .00   2.94
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .040000
time for cinew                          .690000
time for eigenvalue solver              .010000
time for vector access                  .000000
================================================================
time spent in mult:                    44.4600s 
time spent in multnx:                  44.3800s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.4900s 
time for vector access in mult:          .0000s 
total time per CI iteration:           45.2000s 

          starting ci iteration  46

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1      .00821512     -.26191611      .94392929      .09845688     -.01249214     -.01291665      .09065959     -.05537744
 ref:   2      .64396933      .20402941      .12458107     -.70534792     -.10267648      .01442247      .00590158      .02934859
 ref:   3     -.70498137     -.12914072      .04060170     -.67382989      .01395081      .10602399      .01331793     -.00598782
 ref:   4      .23999106     -.91785794     -.24738670     -.09009820     -.02755977      .00572986     -.05646173     -.08738339
 ref:   5     -.00041886      .00034128     -.00155068     -.00011435     -.02148300     -.02227003      .06430985     -.03635631
 ref:   6      .00023520     -.00019633     -.00079047     -.00011097      .00680417      .00094656      .04546961     -.00209786
 ref:   7     -.00086348     -.00131794     -.00035084      .00001877     -.03172639     -.02932893      .02563654      .05652329
 ref:   8      .00025172     -.00938998      .03423818      .00360794      .03503853      .03551366     -.23206937      .13912221
 ref:   9     -.04205492     -.01207431      .00356852     -.00072084     -.25730599     -.22313473     -.06323908      .09771184
 ref:  10      .01683432     -.04180050     -.00688086      .00228824      .10457759      .05332714      .15889458      .29047020

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14
 ref:   1     -.00000386     -.00013149     -.00046596     -.00312622     -.00088008     -.00265321
 ref:   2      .00048478     -.00090550      .00713553      .00161283     -.00019877     -.00167225
 ref:   3     -.00069986      .00102753     -.01025802     -.00333210      .00041527      .00696916
 ref:   4     -.00053902      .00013565      .00282372     -.00161478      .00212559     -.00179016
 ref:   5      .00587985     -.15992519     -.09671912      .23423176      .01776943     -.10147864
 ref:   6     -.00510341     -.00938586     -.03390810      .01868376     -.09442099     -.00160601
 ref:   7      .01672302     -.17249798     -.02576183     -.14270026      .03610155     -.20788619
 ref:   8     -.03497437     -.00445917     -.09461011      .14000966     -.16060501      .01990904
 ref:   9      .35275891      .63735491     -.05051366      .25196715      .13997231     -.06218977
 ref:  10     -.09722442     -.12200352     -.06783130      .12696985     -.00648445      .02290236

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 46  1    -93.3414572604  1.8759E-08  0.0000E+00  5.8446E-04  1.0000E-03
 mr-sdci # 46  2    -93.3414570710  1.6800E-09  0.0000E+00  6.4388E-04  1.0000E-03
 mr-sdci # 46  3    -93.3414568554  6.9724E-10  0.0000E+00  7.2144E-04  1.0000E-03
 mr-sdci # 46  4    -93.3414567594  5.5470E-09  0.0000E+00  7.5899E-04  1.0000E-03
 mr-sdci # 46  5    -93.1685350610  1.3192E-07  0.0000E+00  4.5610E-04  1.0000E-03
 mr-sdci # 46  6    -93.1685345377  1.7927E-07  0.0000E+00  4.5629E-04  1.0000E-03
 mr-sdci # 46  7    -93.1685308024  6.0077E-08  0.0000E+00  5.7473E-04  1.0000E-03
 mr-sdci # 46  8    -93.1685243717  5.6705E-08  0.0000E+00  7.2865E-04  1.0000E-03
 mr-sdci # 46  9    -93.1658885791  1.1150E-04  4.2711E-05  3.2851E-03  1.0000E-03
 mr-sdci # 46 10    -93.1492827233  1.9134E-04  0.0000E+00  9.8005E-03  1.0000E-03
 mr-sdci # 46 11    -93.1032223139  2.6798E-02  0.0000E+00  7.9496E-02  1.0000E-04
 mr-sdci # 46 12    -92.9778764663  8.4019E-03  0.0000E+00  1.9139E-01  1.0000E-04
 mr-sdci # 46 13    -92.9420528942  1.6703E-01  0.0000E+00  2.0617E-01  1.0000E-04
 mr-sdci # 46 14    -92.7716559915 -1.9014E-01  0.0000E+00  2.6279E-01  1.0000E-04
 
 root number  9 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .34    .12    .00    .33
    2   25    0    .19    .06    .00    .19
    3   26    0   5.12    .20    .00   5.12
    4   11    0   2.21   1.23    .00   2.21
    5   15    0   4.56   1.68    .00   4.55
    6   16    0   2.70    .94    .00   2.70
    7    1    0    .49    .29    .00    .49
    8    5    0   2.27    .84    .00   2.27
    9    6    0   8.28    .73    .00   8.28
   10    7    0   1.73    .21    .00   1.73
   11   75    0    .08    .06    .00    .08
   12   45    0    .21    .09    .00    .20
   13   46    0    .57    .09    .00    .57
   14   47    0    .24    .03    .00    .24
   15   81    0    .53    .00    .00    .53
   16   82    0   1.60    .00    .00   1.60
   17  101    0    .27    .00    .00    .27
   18   83    0   1.66    .00    .00   1.66
   19  102    0   4.13    .00    .00   4.13
   20   84    0    .43    .00    .00    .43
   21   91    0    .64    .00    .00    .63
   22   93    0   2.27    .00    .00   2.27
   23   94    0   1.18    .00    .00   1.17
   24  103    0   2.95    .00    .00   2.95
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .030000
time for cinew                          .770000
time for eigenvalue solver              .000000
time for vector access                  .000000
================================================================
time spent in mult:                    44.6500s 
time spent in multnx:                  44.6000s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.5700s 
time for vector access in mult:          .0000s 
total time per CI iteration:           45.4500s 

          starting ci iteration  47

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     -.00943752     -.25876107      .94514788     -.09493681      .01323926     -.01022318     -.09071787      .05567048
 ref:   2     -.64527007      .19353272      .11768869      .70829419      .09906691      .02982425     -.00726855     -.02981510
 ref:   3      .70880220     -.11964960      .04178483      .67149583     -.02983363      .10282554     -.01184948      .00658038
 ref:   4     -.22470526     -.92230163     -.24590774      .08816716      .02776980      .00896166      .05653185      .08699589
 ref:   5      .00040257      .00034875     -.00154541      .00010340      .02404294     -.01842974     -.06458958      .03654071
 ref:   6     -.00023416     -.00020048     -.00078991      .00010684     -.00748772      .00030854     -.04532155      .00237385
 ref:   7      .00082833     -.00128711     -.00032825     -.00003616      .03547874     -.02404029     -.02646224     -.05650080
 ref:   8     -.00028142     -.00928029      .03427841     -.00347708     -.03734359      .02816400      .23226705     -.13987915
 ref:   9      .04221730     -.01143504      .00381777      .00057980      .28838229     -.18237349      .05675157     -.10024183
 ref:  10     -.01616089     -.04207212     -.00682160     -.00233497     -.11570077      .03963190     -.15818053     -.28900996

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15
 ref:   1     -.00002475     -.00020270     -.00097928     -.00120223     -.00341569     -.00025853     -.00194660
 ref:   2     -.00027084     -.00048578      .00796484     -.00044094      .00162920     -.00037021     -.00350011
 ref:   3      .00040425      .00044168     -.01122669      .00199931     -.00309676      .00018359      .00971865
 ref:   4      .00066489      .00027790      .00284445     -.00040781     -.00161709      .00237085     -.00178268
 ref:   5     -.00977233     -.16763187     -.12526478     -.10034702      .21918555      .03510036     -.05055653
 ref:   6      .00395339     -.01207479     -.04511891     -.03455961      .01011859     -.08841636      .00905838
 ref:   7     -.02417217     -.18690244     -.14928865     -.28606915     -.19111621      .12578967      .00591414
 ref:   8      .03573103     -.00391994     -.04832128      .08049602      .14841122     -.19047116     -.07317036
 ref:   9     -.35007304      .63711851     -.06582528     -.00191530      .25774332      .13111753     -.07217974
 ref:  10      .09793486     -.11980312     -.00964805      .11815436      .14618138     -.04173071     -.06785826

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 47  1    -93.3414572688  8.4171E-09  0.0000E+00  5.8042E-04  1.0000E-03
 mr-sdci # 47  2    -93.3414570720  9.8762E-10  0.0000E+00  6.4347E-04  1.0000E-03
 mr-sdci # 47  3    -93.3414568559  5.1284E-10  0.0000E+00  7.2136E-04  1.0000E-03
 mr-sdci # 47  4    -93.3414567597  3.1627E-10  0.0000E+00  7.5894E-04  1.0000E-03
 mr-sdci # 47  5    -93.1685351930  1.3198E-07  0.0000E+00  4.3352E-04  1.0000E-03
 mr-sdci # 47  6    -93.1685345914  5.3636E-08  0.0000E+00  4.5648E-04  1.0000E-03
 mr-sdci # 47  7    -93.1685308320  2.9615E-08  0.0000E+00  5.6840E-04  1.0000E-03
 mr-sdci # 47  8    -93.1685244003  2.8659E-08  0.0000E+00  7.1465E-04  1.0000E-03
 mr-sdci # 47  9    -93.1659291431  4.0564E-05  1.5112E-05  2.2290E-03  1.0000E-03
 mr-sdci # 47 10    -93.1494272660  1.4454E-04  0.0000E+00  9.6829E-03  1.0000E-03
 mr-sdci # 47 11    -93.1210315211  1.7809E-02  0.0000E+00  6.7207E-02  1.0000E-04
 mr-sdci # 47 12    -93.0062418865  2.8365E-02  0.0000E+00  1.5398E-01  1.0000E-04
 mr-sdci # 47 13    -92.9771357821  3.5083E-02  0.0000E+00  1.9098E-01  1.0000E-04
 mr-sdci # 47 14    -92.9383915483  1.6674E-01  0.0000E+00  2.1667E-01  1.0000E-04
 mr-sdci # 47 15    -92.6568249688 -2.3704E-01  0.0000E+00  2.2651E-01  1.0000E-04
 
 root number  9 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .34    .12    .00    .34
    2   25    0    .19    .06    .00    .19
    3   26    0   5.14    .20    .00   5.14
    4   11    0   2.22   1.23    .00   2.22
    5   15    0   4.50   1.64    .00   4.50
    6   16    0   2.68    .94    .00   2.68
    7    1    0    .49    .28    .00    .49
    8    5    0   2.27    .81    .00   2.26
    9    6    0   8.24    .73    .00   8.24
   10    7    0   1.72    .20    .00   1.72
   11   75    0    .09    .05    .00    .08
   12   45    0    .20    .10    .00    .20
   13   46    0    .59    .08    .00    .59
   14   47    0    .22    .04    .00    .22
   15   81    0    .52    .00    .00    .51
   16   82    0   1.61    .00    .00   1.61
   17  101    0    .28    .00    .00    .27
   18   83    0   1.64    .00    .00   1.64
   19  102    0   4.12    .00    .00   4.12
   20   84    0    .43    .00    .00    .43
   21   91    0    .64    .00    .00    .63
   22   93    0   2.27    .00    .00   2.27
   23   94    0   1.18    .00    .00   1.17
   24  103    0   2.95    .00    .00   2.94
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .070000
time for cinew                          .760000
time for eigenvalue solver              .000000
time for vector access                  .000000
================================================================
time spent in mult:                    44.5300s 
time spent in multnx:                  44.4600s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.4800s 
time for vector access in mult:          .0100s 
total time per CI iteration:           45.3800s 

          starting ci iteration  48

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1      .01046157     -.24765884      .94645374      .11020272     -.01278254      .00818767     -.09127752      .05519771
 ref:   2      .64645063      .19586977      .12620894     -.70510102     -.09651370     -.03750219     -.00769646     -.02939448
 ref:   3     -.70858704     -.11481658      .05599532     -.67153211      .03803522     -.10024497     -.01078029      .00575061
 ref:   4      .22192766     -.92546551     -.23352262     -.09542697     -.02690528     -.01114624      .05577930      .08750150
 ref:   5     -.00040112      .00034294     -.00152979     -.00013600     -.02482182      .01596890     -.06504432      .03610243
 ref:   6      .00023315     -.00020713     -.00078129     -.00012282      .00785710     -.00001523     -.04522388      .00194827
 ref:   7     -.00083200     -.00128797     -.00031013      .00002495     -.03755493      .02133097     -.02650796     -.05647679
 ref:   8      .00032051     -.00887175      .03433269      .00402744      .03625636     -.02262908      .23377683     -.13878619
 ref:   9     -.04223608     -.01126539      .00405899     -.00058663     -.30337239      .15999495      .05352917     -.09731972
 ref:  10      .01604112     -.04219164     -.00636170      .00207532      .11791738     -.03002939     -.15487211     -.29107443

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1      .00006232     -.00021696      .00044618      .00101909     -.00366593     -.00080910      .00075707      .00199883
 ref:   2      .00012636      .00001276     -.00709989     -.00383128      .00164515     -.00008895     -.00052254      .00347108
 ref:   3     -.00019206     -.00030400      .01032005      .00548631     -.00195707     -.00139012     -.00117312     -.00977214
 ref:   4     -.00073032      .00058936     -.00363964      .00091571     -.00220800      .00187180      .00007791      .00185697
 ref:   5      .01441744     -.18296614      .16526812     -.11720835      .13617544      .13498431      .02294036      .04894983
 ref:   6     -.00255226     -.01732302      .05987411     -.03683319      .00622490     -.05782745      .05417198     -.00854408
 ref:   7      .02686768     -.19685373      .13227370     -.04338622     -.33688139      .13969691      .04438951     -.00310182
 ref:   8     -.03393205     -.01093057      .07464993     -.03103459      .20137018     -.13179515      .07143732      .07275529
 ref:   9      .35114599      .62573301      .15615770     -.08813860      .21097237      .18267621     -.08839135      .06756776
 ref:  10     -.09563143     -.12596644      .04368278     -.03471341      .20963585     -.04867501     -.09104632      .06213068

 trial vector basis is being transformed.  new dimension:  10

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 48  1    -93.3414572689  1.4737E-10  0.0000E+00  5.7986E-04  1.0000E-03
 mr-sdci # 48  2    -93.3414570738  1.7496E-09  0.0000E+00  6.4373E-04  1.0000E-03
 mr-sdci # 48  3    -93.3414568598  3.9049E-09  0.0000E+00  7.2036E-04  1.0000E-03
 mr-sdci # 48  4    -93.3414567604  7.1532E-10  0.0000E+00  7.5885E-04  1.0000E-03
 mr-sdci # 48  5    -93.1685352587  6.5638E-08  0.0000E+00  4.0573E-04  1.0000E-03
 mr-sdci # 48  6    -93.1685346278  3.6381E-08  0.0000E+00  4.4935E-04  1.0000E-03
 mr-sdci # 48  7    -93.1685308560  2.4024E-08  0.0000E+00  5.6285E-04  1.0000E-03
 mr-sdci # 48  8    -93.1685244760  7.5709E-08  0.0000E+00  7.1250E-04  1.0000E-03
 mr-sdci # 48  9    -93.1659522374  2.3094E-05  1.0794E-05  1.7230E-03  1.0000E-03
 mr-sdci # 48 10    -93.1495549997  1.2773E-04  0.0000E+00  1.1150E-02  1.0000E-03
 mr-sdci # 48 11    -93.1368006036  1.5769E-02  0.0000E+00  5.5098E-02  1.0000E-04
 mr-sdci # 48 12    -93.0590579593  5.2816E-02  0.0000E+00  1.1775E-01  1.0000E-04
 mr-sdci # 48 13    -92.9843373692  7.2016E-03  0.0000E+00  1.8263E-01  1.0000E-04
 mr-sdci # 48 14    -92.9532647124  1.4873E-02  0.0000E+00  2.1559E-01  1.0000E-04
 mr-sdci # 48 15    -92.8655157660  2.0869E-01  0.0000E+00  2.4069E-01  1.0000E-04
 mr-sdci # 48 16    -92.6563962367 -6.4418E-02  0.0000E+00  2.2646E-01  1.0000E-04
 
 root number  9 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .35    .13    .00    .34
    2   25    0    .19    .07    .00    .19
    3   26    0   5.13    .19    .00   5.12
    4   11    0   2.16   1.20    .00   2.16
    5   15    0   4.53   1.67    .00   4.53
    6   16    0   2.59    .91    .00   2.58
    7    1    0    .47    .26    .00    .47
    8    5    0   2.16    .81    .00   2.15
    9    6    0   8.22    .73    .00   8.22
   10    7    0   1.72    .20    .00   1.72
   11   75    0    .08    .06    .00    .08
   12   45    0    .21    .10    .00    .20
   13   46    0    .57    .09    .00    .57
   14   47    0    .24    .03    .00    .23
   15   81    0    .53    .00    .00    .53
   16   82    0   1.60    .00    .00   1.60
   17  101    0    .28    .00    .00    .27
   18   83    0   1.65    .00    .00   1.65
   19  102    0   4.13    .00    .00   4.13
   20   84    0    .43    .00    .00    .43
   21   91    0    .63    .00    .00    .63
   22   93    0   2.28    .00    .00   2.27
   23   94    0   1.17    .00    .00   1.17
   24  103    0   2.95    .00    .00   2.94
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .050000
time for cinew                          .850000
time for eigenvalue solver              .010000
time for vector access                  .000000
================================================================
time spent in mult:                    44.2700s 
time spent in multnx:                  44.1800s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.4500s 
time for vector access in mult:          .0100s 
total time per CI iteration:           45.1900s 

          starting ci iteration  49

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1      .01997603     -.24590850      .94633027     -.11380977      .01277318      .00813858     -.09127993     -.05520312
 ref:   2      .64496130      .21448295      .12642575      .70099706      .09647223     -.03762021     -.00769560      .02938208
 ref:   3     -.69601232     -.15296031      .05638202      .67702265     -.03817722     -.10019583     -.01077869     -.00568476
 ref:   4      .26181139     -.91624160     -.23381256      .08163579      .02685882     -.01123008      .05577580     -.08750642
 ref:   5     -.00041199      .00032283     -.00152953      .00014590      .02483278      .01591717     -.06504605     -.03610256
 ref:   6      .00023153     -.00019024     -.00078143      .00011313     -.00785408     -.00000018     -.04522362     -.00191775
 ref:   7     -.00080028     -.00130501     -.00031054     -.00004321      .03761657      .02132575     -.02650564      .05652706
 ref:   8      .00069238     -.00883319      .03432849     -.00414203     -.03624763     -.02251685      .23378217      .13872708
 ref:   9     -.04173993     -.01303495      .00406427      .00085056      .30364132      .15964201      .05352856      .09719123
 ref:  10      .01779597     -.04144896     -.00637871     -.00276008     -.11785948     -.02971550     -.15485944      .29111370

                ci   9         ci  10         ci  11
 ref:   1     -.00007327      .00022290     -.00000268
 ref:   2     -.00015388      .00013413      .00376388
 ref:   3      .00024056      .00006253     -.00624992
 ref:   4      .00069231     -.00047877      .00245881
 ref:   5     -.01473629      .18218120     -.02279888
 ref:   6      .00199520      .01957133      .05698569
 ref:   7     -.02836021      .20109953      .10661558
 ref:   8      .03538380      .00461755     -.16345599
 ref:   9     -.35091925     -.62024818      .17512139
 ref:  10      .09578166      .12429381     -.05058757

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 49  1    -93.3414572802  1.1219E-08  0.0000E+00  5.7487E-04  1.0000E-03
 mr-sdci # 49  2    -93.3414570800  6.2484E-09  0.0000E+00  6.4242E-04  1.0000E-03
 mr-sdci # 49  3    -93.3414568598  6.7146E-13  0.0000E+00  7.2034E-04  1.0000E-03
 mr-sdci # 49  4    -93.3414567628  2.4447E-09  0.0000E+00  7.5833E-04  1.0000E-03
 mr-sdci # 49  5    -93.1685352593  6.1885E-10  0.0000E+00  4.0525E-04  1.0000E-03
 mr-sdci # 49  6    -93.1685346290  1.2435E-09  0.0000E+00  4.4938E-04  1.0000E-03
 mr-sdci # 49  7    -93.1685308560  2.5722E-12  0.0000E+00  5.6281E-04  1.0000E-03
 mr-sdci # 49  8    -93.1685245004  2.4349E-08  0.0000E+00  7.0698E-04  1.0000E-03
 mr-sdci # 49  9    -93.1659626437  1.0406E-05  7.9548E-06  1.2497E-03  1.0000E-03
 mr-sdci # 49 10    -93.1497330862  1.7809E-04  0.0000E+00  1.0042E-02  1.0000E-03
 mr-sdci # 49 11    -93.0331637881 -1.0364E-01  0.0000E+00  2.1058E-01  1.0000E-04
 
 root number  9 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .34    .12    .00    .34
    2   25    0    .19    .06    .00    .19
    3   26    0   5.15    .20    .00   5.14
    4   11    0   2.22   1.24    .00   2.21
    5   15    0   4.58   1.66    .00   4.57
    6   16    0   2.66    .94    .00   2.66
    7    1    0    .49    .28    .00    .49
    8    5    0   2.26    .83    .00   2.25
    9    6    0   8.23    .73    .00   8.23
   10    7    0   1.72    .19    .00   1.72
   11   75    0    .09    .05    .00    .08
   12   45    0    .20    .10    .00    .20
   13   46    0    .58    .08    .00    .58
   14   47    0    .23    .04    .00    .23
   15   81    0    .53    .00    .00    .52
   16   82    0   1.61    .00    .00   1.60
   17  101    0    .28    .00    .00    .28
   18   83    0   1.66    .00    .00   1.66
   19  102    0   4.13    .00    .00   4.13
   20   84    0    .42    .00    .00    .42
   21   91    0    .60    .00    .00    .60
   22   93    0   2.19    .00    .00   2.18
   23   94    0   1.17    .00    .00   1.16
   24  103    0   2.96    .00    .00   2.96
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .060000
time for cinew                          .570000
time for eigenvalue solver              .000000
time for vector access                  .000000
================================================================
time spent in mult:                    44.4900s 
time spent in multnx:                  44.4000s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.5200s 
time for vector access in mult:          .0100s 
total time per CI iteration:           45.1300s 

          starting ci iteration  50

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1      .01722299      .22687572     -.94906054      .12991725      .01278225      .00781853      .09119878     -.05538129
 ref:   2      .64178956     -.22502876     -.13776085     -.69846175      .09604331     -.03876659      .00772303      .02930073
 ref:   3     -.69594580      .15421268     -.06827798     -.67571006     -.03950580     -.09970183      .01085506     -.00523091
 ref:   4      .26985400      .91841111      .21216939     -.08978863      .02647553     -.01196214     -.05589032     -.08745989
 ref:   5     -.00041358     -.00030332      .00151582     -.00017729      .02503689      .01558150      .06499752     -.03603532
 ref:   6      .00023291      .00019556      .00076297     -.00013145     -.00780232      .00018492      .04523609     -.00177958
 ref:   7     -.00078508      .00132517      .00029872      .00003627      .03804937      .02107908      .02655146      .05636498
 ref:   8      .00059524      .00815036     -.03441564      .00472949     -.03635479     -.02174557     -.23359277      .13893781
 ref:   9     -.04167111      .01324444     -.00417159     -.00079048      .30603767      .15612835     -.05359779      .09642980
 ref:  10      .01813030      .04140870      .00544187      .00244616     -.11737959     -.02677063      .15532884      .29178185

                ci   9         ci  10         ci  11         ci  12
 ref:   1      .00009801      .00016973      .00018382      .00011025
 ref:   2      .00025378      .00182214     -.00338836      .00144120
 ref:   3     -.00039670     -.00252871      .00527092     -.00299963
 ref:   4     -.00072362     -.00139541      .00164114      .00563486
 ref:   5      .01869361      .19885508      .01281350     -.10385756
 ref:   6      .00065358      .05230118     -.06039467      .00526514
 ref:   7      .02888596      .17465069      .09579711      .15502277
 ref:   8     -.03896856     -.04701198      .10691543     -.11374082
 ref:   9      .35439560     -.45776920     -.47514469      .01644016
 ref:  10     -.08799961      .20399495     -.12076735     -.25578040

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 50  1    -93.3414572809  7.0684E-10  0.0000E+00  5.7440E-04  1.0000E-03
 mr-sdci # 50  2    -93.3414570833  3.2901E-09  0.0000E+00  6.4257E-04  1.0000E-03
 mr-sdci # 50  3    -93.3414568659  6.0960E-09  0.0000E+00  7.1783E-04  1.0000E-03
 mr-sdci # 50  4    -93.3414567634  5.3149E-10  0.0000E+00  7.5802E-04  1.0000E-03
 mr-sdci # 50  5    -93.1685352646  5.3565E-09  0.0000E+00  4.0147E-04  1.0000E-03
 mr-sdci # 50  6    -93.1685346417  1.2727E-08  0.0000E+00  4.5020E-04  1.0000E-03
 mr-sdci # 50  7    -93.1685308566  5.3819E-10  0.0000E+00  5.6419E-04  1.0000E-03
 mr-sdci # 50  8    -93.1685246207  1.2034E-07  0.0000E+00  7.2612E-04  1.0000E-03
 mr-sdci # 50  9    -93.1659856359  2.2992E-05  2.0819E-05  2.6973E-03  1.0000E-03
 mr-sdci # 50 10    -93.1511377406  1.4047E-03  0.0000E+00  3.8307E-02  1.0000E-03
 mr-sdci # 50 11    -93.1443578844  1.1119E-01  0.0000E+00  7.8560E-02  1.0000E-04
 mr-sdci # 50 12    -92.9316678209 -1.2739E-01  0.0000E+00  2.2697E-01  1.0000E-04
 
 root number  9 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .33    .12    .00    .33
    2   25    0    .20    .06    .00    .20
    3   26    0   5.14    .19    .00   5.14
    4   11    0   2.24   1.26    .00   2.23
    5   15    0   4.54   1.69    .00   4.54
    6   16    0   2.62    .90    .00   2.61
    7    1    0    .49    .27    .00    .49
    8    5    0   2.24    .83    .00   2.24
    9    6    0   8.30    .73    .00   8.30
   10    7    0   1.73    .20    .00   1.73
   11   75    0    .08    .05    .00    .08
   12   45    0    .20    .11    .00    .20
   13   46    0    .58    .08    .00    .57
   14   47    0    .23    .04    .00    .23
   15   81    0    .54    .00    .00    .53
   16   82    0   1.60    .00    .00   1.60
   17  101    0    .28    .00    .00    .27
   18   83    0   1.66    .00    .00   1.66
   19  102    0   4.13    .00    .00   4.13
   20   84    0    .43    .00    .00    .43
   21   91    0    .64    .00    .00    .63
   22   93    0   2.27    .00    .00   2.27
   23   94    0   1.18    .00    .00   1.17
   24  103    0   2.94    .00    .00   2.94
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .040000
time for cinew                          .570000
time for eigenvalue solver              .000000
time for vector access                  .000000
================================================================
time spent in mult:                    44.5900s 
time spent in multnx:                  44.5200s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.5300s 
time for vector access in mult:          .0100s 
total time per CI iteration:           45.2200s 

          starting ci iteration  51

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     -.01712347      .22688309     -.94824054      .13577327     -.01290676     -.00803934      .09062084      .05626270
 ref:   2     -.63943255     -.22329813     -.14215357     -.70029671     -.09585572      .03919148      .00806284     -.02926432
 ref:   3      .69848680      .15481573     -.07188709     -.67256780      .04008066      .09942946      .01138307      .00497910
 ref:   4     -.26888794      .91873020      .21174559     -.09042083     -.02623278      .01269913     -.05667922      .08692867
 ref:   5      .00041735     -.00030251      .00151356     -.00019868     -.02520476     -.01570854      .06463445      .03656045
 ref:   6     -.00023074      .00019634      .00076154     -.00014261      .00771911     -.00051330      .04531884      .00204458
 ref:   7      .00078322      .00132362      .00030044      .00004853     -.03828782     -.02123644      .02700983     -.05617638
 ref:   8     -.00058207      .00815343     -.03438896      .00490910      .03675487      .02250728     -.23249318     -.14060538
 ref:   9      .04168395      .01321273     -.00415486     -.00058967     -.30704099     -.15458430     -.05356789     -.09676783
 ref:  10     -.01810554      .04141854      .00545736      .00241182      .11696567      .02417675      .15857693     -.29090340

                ci   9         ci  10         ci  11         ci  12         ci  13
 ref:   1      .00017769      .00063973     -.00008306     -.00164161      .00240848
 ref:   2      .00041820      .00235266      .00037514      .00227667     -.00599060
 ref:   3     -.00067499     -.00371302     -.00087724     -.00237664      .00885866
 ref:   4     -.00078998     -.00169246      .00011007     -.00442857     -.00374923
 ref:   5      .02395641      .07633897     -.17195451      .16309516     -.05821521
 ref:   6      .00573025      .05979433     -.00778146      .04765201     -.07869584
 ref:   7      .03338397      .05216166     -.19134751     -.16158789     -.03490064
 ref:   8     -.05640204     -.16079541     -.04230583      .18725394     -.09647418
 ref:   9      .36330453      .03866783      .64930385      .03923332     -.06957842
 ref:  10     -.07262216      .18647673     -.09350019      .30066409      .01961675

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 51  1    -93.3414572814  5.3595E-10  0.0000E+00  5.7434E-04  1.0000E-03
 mr-sdci # 51  2    -93.3414570834  4.6558E-11  0.0000E+00  6.4255E-04  1.0000E-03
 mr-sdci # 51  3    -93.3414568659  5.4115E-11  0.0000E+00  7.1777E-04  1.0000E-03
 mr-sdci # 51  4    -93.3414567696  6.2069E-09  0.0000E+00  7.5626E-04  1.0000E-03
 mr-sdci # 51  5    -93.1685352658  1.1800E-09  0.0000E+00  4.0060E-04  1.0000E-03
 mr-sdci # 51  6    -93.1685346529  1.1171E-08  0.0000E+00  4.5440E-04  1.0000E-03
 mr-sdci # 51  7    -93.1685308917  3.5136E-08  0.0000E+00  5.7761E-04  1.0000E-03
 mr-sdci # 51  8    -93.1685247156  9.4859E-08  0.0000E+00  7.3183E-04  1.0000E-03
 mr-sdci # 51  9    -93.1660433847  5.7749E-05  6.5189E-05  4.1839E-03  1.0000E-03
 mr-sdci # 51 10    -93.1613194826  1.0182E-02  0.0000E+00  3.3855E-02  1.0000E-03
 mr-sdci # 51 11    -93.1492548359  4.8970E-03  0.0000E+00  1.2016E-02  1.0000E-04
 mr-sdci # 51 12    -92.9606692408  2.9001E-02  0.0000E+00  2.0752E-01  1.0000E-04
 mr-sdci # 51 13    -92.8595531790 -1.2478E-01  0.0000E+00  2.6364E-01  1.0000E-04
 
 root number  9 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .35    .13    .00    .35
    2   25    0    .19    .07    .00    .19
    3   26    0   5.09    .19    .00   5.09
    4   11    0   2.12   1.17    .00   2.12
    5   15    0   4.54   1.68    .00   4.54
    6   16    0   2.66    .92    .00   2.66
    7    1    0    .49    .27    .00    .49
    8    5    0   2.27    .84    .00   2.26
    9    6    0   8.29    .73    .00   8.29
   10    7    0   1.72    .19    .00   1.72
   11   75    0    .09    .05    .00    .08
   12   45    0    .20    .10    .00    .20
   13   46    0    .57    .08    .00    .57
   14   47    0    .24    .04    .00    .23
   15   81    0    .53    .00    .00    .53
   16   82    0   1.62    .00    .00   1.61
   17  101    0    .27    .00    .00    .27
   18   83    0   1.67    .00    .00   1.66
   19  102    0   4.12    .00    .00   4.12
   20   84    0    .43    .00    .00    .43
   21   91    0    .63    .00    .00    .63
   22   93    0   2.28    .00    .00   2.27
   23   94    0   1.17    .00    .00   1.17
   24  103    0   2.95    .00    .00   2.94
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .060000
time for cinew                          .700000
time for eigenvalue solver              .000000
time for vector access                  .000000
================================================================
time spent in mult:                    44.4900s 
time spent in multnx:                  44.4200s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.4600s 
time for vector access in mult:          .0100s 
total time per CI iteration:           45.2600s 

          starting ci iteration  52

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1      .01602138     -.22082551     -.94996067      .13385731      .01323854      .00893629      .08843407      .05944043
 ref:   2      .64297494      .21083211     -.13707385     -.70192771      .09534825     -.04015115      .00948542     -.02920387
 ref:   3     -.70183369     -.14299506     -.07323874     -.67155322     -.04157940     -.09860952      .01334313      .00414750
 ref:   4      .25120954     -.92504611      .20686527     -.08814247      .02557839     -.01526983     -.05943820      .08487021
 ref:   5     -.00041324      .00030376      .00151346     -.00019438      .02564692      .01632989      .06327574      .03846732
 ref:   6      .00023118     -.00019846      .00075882     -.00013926     -.00749247      .00168871      .04557217      .00306636
 ref:   7     -.00082001     -.00132371      .00029394      .00005097      .03887075      .02186323      .02836462     -.05494540
 ref:   8      .00054096     -.00793584     -.03444999      .00483902     -.03774759     -.02524152     -.22775049     -.14759435
 ref:   9     -.04190293     -.01241389     -.00436225     -.00051927      .30956248      .15043708     -.05348157     -.09757924
 ref:  10      .01735394     -.04174439      .00522901      .00250181     -.11580323     -.01518406      .17020830     -.28730535

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14
 ref:   1      .00030893      .00044059     -.00014159     -.00119989     -.00125881     -.00243413
 ref:   2      .00079221      .00149708      .00012457     -.00253866      .00411043      .00557758
 ref:   3     -.00122002     -.00222661     -.00043821      .00587682     -.00613520     -.00807561
 ref:   4     -.00099075     -.00122486      .00011073     -.00591136     -.00165210      .00338307
 ref:   5      .04423301      .05976552     -.17531895      .09172005      .13613581      .05750813
 ref:   6      .02696083      .06162844     -.00969491      .06120014      .02087230      .08300653
 ref:   7      .03537170      .01172794     -.19799719     -.18212264     -.08227132      .02567635
 ref:   8     -.10319607     -.13524264     -.03201793      .10116515      .16177990      .09397090
 ref:   9      .37141476     -.04206979      .64388380     -.00747415      .04718330      .06781117
 ref:  10     -.00079963      .22353823     -.09695959      .32809279      .15800392     -.00251733

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 52  1    -93.3414572846  3.2417E-09  0.0000E+00  5.7382E-04  1.0000E-03
 mr-sdci # 52  2    -93.3414570874  4.0677E-09  0.0000E+00  6.4169E-04  1.0000E-03
 mr-sdci # 52  3    -93.3414568664  4.8153E-10  0.0000E+00  7.1776E-04  1.0000E-03
 mr-sdci # 52  4    -93.3414567697  1.2362E-10  0.0000E+00  7.5624E-04  1.0000E-03
 mr-sdci # 52  5    -93.1685352679  2.0580E-09  0.0000E+00  4.0073E-04  1.0000E-03
 mr-sdci # 52  6    -93.1685346939  4.1013E-08  0.0000E+00  4.6146E-04  1.0000E-03
 mr-sdci # 52  7    -93.1685310244  1.3272E-07  0.0000E+00  5.9728E-04  1.0000E-03
 mr-sdci # 52  8    -93.1685250384  3.2287E-07  0.0000E+00  7.2842E-04  1.0000E-03
 mr-sdci # 52  9    -93.1662389191  1.9553E-04  1.6959E-04  7.9536E-03  1.0000E-03
 mr-sdci # 52 10    -93.1647541293  3.4346E-03  0.0000E+00  1.7326E-02  1.0000E-03
 mr-sdci # 52 11    -93.1494338139  1.7898E-04  0.0000E+00  9.5485E-03  1.0000E-04
 mr-sdci # 52 12    -92.9882635700  2.7594E-02  0.0000E+00  1.8924E-01  1.0000E-04
 mr-sdci # 52 13    -92.9519191707  9.2366E-02  0.0000E+00  2.1751E-01  1.0000E-04
 mr-sdci # 52 14    -92.8583780527 -9.4887E-02  0.0000E+00  2.6192E-01  1.0000E-04
 
 root number  9 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .34    .12    .00    .34
    2   25    0    .20    .06    .00    .20
    3   26    0   4.94    .19    .00   4.94
    4   11    0   2.16   1.22    .00   2.15
    5   15    0   4.55   1.69    .00   4.54
    6   16    0   2.63    .93    .00   2.62
    7    1    0    .48    .26    .00    .48
    8    5    0   2.25    .82    .00   2.25
    9    6    0   8.18    .73    .00   8.17
   10    7    0   1.71    .20    .00   1.71
   11   75    0    .08    .06    .00    .08
   12   45    0    .21    .10    .00    .20
   13   46    0    .58    .08    .00    .58
   14   47    0    .23    .04    .00    .23
   15   81    0    .54    .00    .00    .53
   16   82    0   1.59    .00    .00   1.58
   17  101    0    .28    .00    .00    .27
   18   83    0   1.66    .00    .00   1.66
   19  102    0   4.13    .00    .00   4.13
   20   84    0    .43    .00    .00    .43
   21   91    0    .64    .00    .00    .63
   22   93    0   2.27    .00    .00   2.27
   23   94    0   1.17    .00    .00   1.16
   24  103    0   2.95    .00    .00   2.95
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .050000
time for cinew                          .760000
time for eigenvalue solver              .000000
time for vector access                  .000000
================================================================
time spent in mult:                    44.2000s 
time spent in multnx:                  44.1000s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.5000s 
time for vector access in mult:          .0100s 
total time per CI iteration:           45.0200s 

          starting ci iteration  53

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1      .01738650     -.21179927     -.95386839      .11974240      .01398661     -.01116005      .08564033      .06289133
 ref:   2      .64323941      .20830926     -.12303489     -.70503291      .09449511      .04177159      .01140205     -.02902086
 ref:   3     -.70254913     -.14258939     -.06547955     -.67169256     -.04386133      .09705200      .01710199      .00359693
 ref:   4      .24842598     -.92778708      .20020508     -.08241809      .02451415      .01900711     -.06219681      .08243598
 ref:   5     -.00042080      .00027708      .00153949     -.00018129      .02643405     -.01758443      .06100375      .04103031
 ref:   6      .00022880     -.00020720      .00076080     -.00012768     -.00705708     -.00367926      .04577968      .00451087
 ref:   7     -.00082487     -.00132667      .00028354      .00005804      .03975931     -.02256978      .02939261     -.05356841
 ref:   8      .00058359     -.00762625     -.03456252      .00431388     -.03992528      .03169318     -.22230239     -.15527486
 ref:   9     -.04192856     -.01227598     -.00453611     -.00052609      .31307280     -.14238076     -.05626433     -.09830840
 ref:  10      .01724285     -.04182151      .00483283      .00269485     -.11390946      .00229093      .18354984     -.28193982

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15
 ref:   1      .00047467      .00036621     -.00008451      .00313494      .00251672     -.00212155      .00105018
 ref:   2      .00090549      .00069266      .00002257     -.00539013     -.00079626      .00053498      .00509993
 ref:   3     -.00141425     -.00103427     -.00028953      .00836200     -.00072871      .00010341     -.00850313
 ref:   4     -.00078224     -.00040985      .00010528     -.00306241      .00528147      .00204351      .00246732
 ref:   5      .04753860      .02038854     -.18012513     -.17787820     -.18425986      .03044810     -.09462685
 ref:   6      .04819020      .04279512     -.01100677     -.02103415     -.07555296      .05860022      .04254676
 ref:   7      .03082981     -.00950055     -.19929598     -.07030651      .18659875      .03707027      .00240023
 ref:   8     -.16932647     -.09581214     -.03467213     -.20947468     -.21497082      .05044099     -.09600492
 ref:   9      .31419036     -.19814458      .63988841     -.14162839     -.04561507      .04756489     -.02797128
 ref:  10      .09493496      .20535547     -.09833071      .09009165     -.34963939     -.04056879      .02081739

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 53  1    -93.3414572848  1.9618E-10  0.0000E+00  5.7389E-04  1.0000E-03
 mr-sdci # 53  2    -93.3414570887  1.2505E-09  0.0000E+00  6.4161E-04  1.0000E-03
 mr-sdci # 53  3    -93.3414568696  3.2269E-09  0.0000E+00  7.1735E-04  1.0000E-03
 mr-sdci # 53  4    -93.3414567704  7.1368E-10  0.0000E+00  7.5619E-04  1.0000E-03
 mr-sdci # 53  5    -93.1685352710  3.0905E-09  0.0000E+00  4.0301E-04  1.0000E-03
 mr-sdci # 53  6    -93.1685347513  5.7349E-08  0.0000E+00  4.6982E-04  1.0000E-03
 mr-sdci # 53  7    -93.1685313208  2.9634E-07  0.0000E+00  5.9661E-04  1.0000E-03
 mr-sdci # 53  8    -93.1685252177  1.7929E-07  0.0000E+00  7.1675E-04  1.0000E-03
 mr-sdci # 53  9    -93.1666385625  3.9964E-04  2.0321E-04  7.6662E-03  1.0000E-03
 mr-sdci # 53 10    -93.1655008334  7.4670E-04  0.0000E+00  6.6689E-03  1.0000E-03
 mr-sdci # 53 11    -93.1494700077  3.6194E-05  0.0000E+00  9.2413E-03  1.0000E-04
 mr-sdci # 53 12    -93.0683582756  8.0095E-02  0.0000E+00  1.2905E-01  1.0000E-04
 mr-sdci # 53 13    -92.9779100857  2.5991E-02  0.0000E+00  1.9212E-01  1.0000E-04
 mr-sdci # 53 14    -92.9111487239  5.2771E-02  0.0000E+00  2.2919E-01  1.0000E-04
 mr-sdci # 53 15    -92.7042644347 -1.6125E-01  0.0000E+00  2.7603E-01  1.0000E-04
 
 root number  9 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .33    .11    .00    .33
    2   25    0    .20    .07    .00    .19
    3   26    0   5.11    .19    .00   5.11
    4   11    0   2.20   1.25    .00   2.20
    5   15    0   4.56   1.67    .00   4.55
    6   16    0   2.65    .93    .00   2.65
    7    1    0    .50    .28    .00    .50
    8    5    0   2.27    .81    .00   2.27
    9    6    0   8.27    .73    .00   8.27
   10    7    0   1.71    .20    .00   1.71
   11   75    0    .08    .05    .00    .08
   12   45    0    .20    .09    .00    .20
   13   46    0    .57    .08    .00    .57
   14   47    0    .23    .03    .00    .23
   15   81    0    .51    .00    .00    .50
   16   82    0   1.64    .00    .00   1.63
   17  101    0    .27    .00    .00    .26
   18   83    0   1.69    .00    .00   1.68
   19  102    0   4.10    .00    .00   4.09
   20   84    0    .43    .00    .00    .43
   21   91    0    .64    .00    .00    .63
   22   93    0   2.29    .00    .00   2.29
   23   94    0   1.20    .00    .00   1.19
   24  103    0   2.95    .00    .00   2.95
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .050000
time for cinew                          .780000
time for eigenvalue solver              .010000
time for vector access                  .000000
================================================================
time spent in mult:                    44.6000s 
time spent in multnx:                  44.5100s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.4900s 
time for vector access in mult:          .0100s 
total time per CI iteration:           45.4500s 

          starting ci iteration  54

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1      .01462962     -.21278311     -.95267428      .12761479      .01636445      .01101859     -.08380363      .06478846
 ref:   2      .64084870      .20507178     -.13064060     -.70678856      .08976212     -.05145246     -.01135086     -.02850123
 ref:   3     -.70576997     -.14134546     -.06879634     -.66823733     -.05397441     -.09116570     -.02038630      .00268226
 ref:   4      .24563396     -.92847331      .19996038     -.08364362      .02025215     -.02386839      .06370978      .08117368
 ref:   5     -.00041312      .00028073      .00154014     -.00019001      .02919838      .01570664     -.05932605      .04248012
 ref:   6      .00021906     -.00020965      .00075516     -.00014183     -.00564075      .00555543     -.04576532      .00532797
 ref:   7     -.00083599     -.00132536      .00028465      .00005532      .04301833      .01915666     -.02902823     -.05238906
 ref:   8      .00048534     -.00766142     -.03451875      .00460035     -.04693293     -.03147590      .21823472     -.15954901
 ref:   9     -.04197941     -.01214538     -.00441231     -.00030949      .32691593      .10667735      .06313589     -.09640018
 ref:  10      .01714319     -.04186484      .00486204      .00265862     -.10665443      .01794350     -.19180258     -.27982499

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1      .00036208      .00021812     -.00010560      .00099597      .00377068     -.00402798     -.00040555      .00074806
 ref:   2      .00043084      .00023947     -.00016290     -.00687273     -.00077960     -.00148337      .00138685      .00482003
 ref:   3     -.00077559     -.00038199     -.00002430      .01015211     -.00037396      .00106915     -.00057875     -.00820479
 ref:   4     -.00052429     -.00021646      .00005413     -.00320666      .00458857      .00285358      .00109795      .00248403
 ref:   5      .04726357      .01118123     -.18163338     -.10610199     -.22832388      .09791560     -.01524493     -.08635382
 ref:   6      .04828727      .03139730     -.01390568     -.07248188     -.05006007     -.08353452      .10343778      .03225083
 ref:   7      .02206334     -.01847868     -.20196346     -.09734668      .17686069      .04869867      .02394497     -.00021162
 ref:   8     -.18400014     -.07091472     -.03485775     -.12337942     -.26469077      .11136105      .00031804     -.08758793
 ref:   9      .28082951     -.24265760      .63751992     -.11946769     -.07969561      .09579987      .00774999     -.02229724
 ref:  10      .13079456      .19309939     -.09616087      .13489443     -.33509052     -.07919015     -.01789863      .02478064

 trial vector basis is being transformed.  new dimension:  10

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 54  1    -93.3414572876  2.7931E-09  0.0000E+00  5.7266E-04  1.0000E-03
 mr-sdci # 54  2    -93.3414570888  1.4647E-10  0.0000E+00  6.4164E-04  1.0000E-03
 mr-sdci # 54  3    -93.3414568701  4.4269E-10  0.0000E+00  7.1708E-04  1.0000E-03
 mr-sdci # 54  4    -93.3414567718  1.4115E-09  0.0000E+00  7.5533E-04  1.0000E-03
 mr-sdci # 54  5    -93.1685353211  5.0147E-08  0.0000E+00  4.0133E-04  1.0000E-03
 mr-sdci # 54  6    -93.1685348156  6.4300E-08  0.0000E+00  4.7572E-04  1.0000E-03
 mr-sdci # 54  7    -93.1685314364  1.1567E-07  0.0000E+00  6.1017E-04  1.0000E-03
 mr-sdci # 54  8    -93.1685253430  1.2526E-07  0.0000E+00  7.1088E-04  1.0000E-03
 mr-sdci # 54  9    -93.1668771388  2.3858E-04  1.2618E-04  6.0018E-03  1.0000E-03
 mr-sdci # 54 10    -93.1656341312  1.3330E-04  0.0000E+00  3.9041E-03  1.0000E-03
 mr-sdci # 54 11    -93.1495090829  3.9075E-05  0.0000E+00  8.7351E-03  1.0000E-04
 mr-sdci # 54 12    -93.1076212194  3.9263E-02  0.0000E+00  9.5450E-02  1.0000E-04
 mr-sdci # 54 13    -92.9822213254  4.3112E-03  0.0000E+00  1.8806E-01  1.0000E-04
 mr-sdci # 54 14    -92.9230415957  1.1893E-02  0.0000E+00  2.1937E-01  1.0000E-04
 mr-sdci # 54 15    -92.9085069315  2.0424E-01  0.0000E+00  2.3010E-01  1.0000E-04
 mr-sdci # 54 16    -92.7026076071  4.6211E-02  0.0000E+00  2.7527E-01  1.0000E-04
 
 root number  9 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .34    .12    .00    .34
    2   25    0    .19    .06    .00    .19
    3   26    0   5.15    .20    .00   5.14
    4   11    0   2.22   1.23    .00   2.22
    5   15    0   4.52   1.65    .00   4.52
    6   16    0   2.56    .92    .00   2.55
    7    1    0    .48    .27    .00    .47
    8    5    0   2.16    .82    .00   2.16
    9    6    0   8.27    .72    .00   8.27
   10    7    0   1.73    .19    .00   1.73
   11   75    0    .08    .06    .00    .08
   12   45    0    .20    .11    .00    .20
   13   46    0    .58    .08    .00    .57
   14   47    0    .24    .03    .00    .24
   15   81    0    .53    .00    .00    .53
   16   82    0   1.60    .00    .00   1.60
   17  101    0    .28    .00    .00    .27
   18   83    0   1.66    .00    .00   1.66
   19  102    0   4.14    .00    .00   4.14
   20   84    0    .42    .00    .00    .42
   21   91    0    .63    .00    .00    .63
   22   93    0   2.27    .00    .00   2.27
   23   94    0   1.18    .00    .00   1.17
   24  103    0   2.95    .00    .00   2.95
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .070000
time for cinew                          .770000
time for eigenvalue solver              .000000
time for vector access                  .000000
================================================================
time spent in mult:                    44.3800s 
time spent in multnx:                  44.3200s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.4600s 
time for vector access in mult:          .0100s 
total time per CI iteration:           45.2400s 

          starting ci iteration  55

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     -.01507743      .19273870      .95530983      .13918616     -.01843939      .01182224     -.08217534     -.06616385
 ref:   2     -.64112220     -.20895156      .13470291     -.70463811     -.08640582     -.05695798     -.01165408      .02827748
 ref:   3      .70576903      .13787152      .08061272     -.66764300      .05958840     -.08672414     -.02385471     -.00239562
 ref:   4     -.24489457      .93249938     -.17920064     -.08796695     -.01706054     -.02728502      .06457719     -.08014234
 ref:   5      .00041518     -.00022538     -.00158665     -.00020279     -.03110360      .01499743     -.05753018     -.04362617
 ref:   6     -.00021837      .00022703     -.00075166     -.00015133      .00433762      .00719252     -.04572918     -.00597059
 ref:   7      .00083712      .00133175     -.00025851      .00004934     -.04496862      .01723533     -.02853293      .05172471
 ref:   8     -.00050003      .00696313      .03456072      .00502708      .05289547     -.03373768      .21485649      .16273002
 ref:   9      .04198518      .01202788      .00468363     -.00027841     -.33258430      .08318170      .06998127      .09600847
 ref:  10     -.01711236      .04196929     -.00399828      .00251403      .10019169      .03192891     -.19786193      .27740039

                ci   9         ci  10         ci  11
 ref:   1      .00026037     -.00018045      .00242906
 ref:   2      .00028019     -.00014798     -.00329034
 ref:   3     -.00048409      .00019609      .00853310
 ref:   4     -.00027816      .00012031     -.00151104
 ref:   5      .04113992     -.00521789     -.28001928
 ref:   6      .04906567     -.02887225     -.02130995
 ref:   7      .02068917      .01970292     -.01353299
 ref:   8     -.19410325      .06681849     -.32483519
 ref:   9      .26674543      .25664828     -.10822951
 ref:  10      .14031488     -.18776956      .07299679

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 55  1    -93.3414572876  7.4714E-12  0.0000E+00  5.7262E-04  1.0000E-03
 mr-sdci # 55  2    -93.3414570912  2.3787E-09  0.0000E+00  6.4063E-04  1.0000E-03
 mr-sdci # 55  3    -93.3414568790  8.8662E-09  0.0000E+00  7.1401E-04  1.0000E-03
 mr-sdci # 55  4    -93.3414567720  1.8744E-10  0.0000E+00  7.5519E-04  1.0000E-03
 mr-sdci # 55  5    -93.1685353468  2.5718E-08  0.0000E+00  3.8946E-04  1.0000E-03
 mr-sdci # 55  6    -93.1685348593  4.3730E-08  0.0000E+00  4.6436E-04  1.0000E-03
 mr-sdci # 55  7    -93.1685316265  1.9007E-07  0.0000E+00  5.6756E-04  1.0000E-03
 mr-sdci # 55  8    -93.1685253892  4.6264E-08  0.0000E+00  6.9747E-04  1.0000E-03
 mr-sdci # 55  9    -93.1669705023  9.3363E-05  4.9494E-05  3.2812E-03  1.0000E-03
 mr-sdci # 55 10    -93.1656693994  3.5268E-05  0.0000E+00  1.9885E-03  1.0000E-03
 mr-sdci # 55 11    -92.9859961891 -1.6351E-01  0.0000E+00  2.1529E-01  1.0000E-04
 
 root number  9 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .35    .12    .00    .34
    2   25    0    .19    .07    .00    .19
    3   26    0   5.15    .19    .00   5.14
    4   11    0   2.22   1.23    .00   2.21
    5   15    0   4.55   1.67    .00   4.55
    6   16    0   2.66    .94    .00   2.65
    7    1    0    .50    .28    .00    .49
    8    5    0   2.26    .84    .00   2.26
    9    6    0   8.24    .73    .00   8.24
   10    7    0   1.75    .19    .00   1.75
   11   75    0    .08    .05    .00    .07
   12   45    0    .20    .09    .00    .19
   13   46    0    .57    .09    .00    .57
   14   47    0    .24    .04    .00    .24
   15   81    0    .53    .00    .00    .53
   16   82    0   1.62    .00    .00   1.61
   17  101    0    .29    .00    .00    .28
   18   83    0   1.69    .00    .00   1.69
   19  102    0   4.09    .00    .00   4.09
   20   84    0    .45    .00    .00    .45
   21   91    0    .60    .00    .00    .59
   22   93    0   2.27    .00    .00   2.27
   23   94    0   1.18    .00    .00   1.17
   24  103    0   2.94    .00    .00   2.94
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .060000
time for cinew                          .590000
time for eigenvalue solver              .000000
time for vector access                  .000000
================================================================
time spent in mult:                    44.6200s 
time spent in multnx:                  44.5100s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.5300s 
time for vector access in mult:          .0000s 
total time per CI iteration:           45.2700s 

          starting ci iteration  56

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     -.01493384      .18944032     -.95576187     -.14061777      .02029804      .01139316     -.08109193     -.06702391
 ref:   2     -.64104357     -.20972767     -.13518751      .70438610      .08248017     -.06266995     -.01139100      .02801486
 ref:   3      .70576896      .13755474     -.08199249      .66754047     -.06515289     -.08195720     -.02606788     -.00205353
 ref:   4     -.24510944      .93304769      .17576487      .08848508      .01365113     -.02958160      .06513656     -.07952661
 ref:   5      .00041488     -.00021936      .00158813      .00020508      .03284971      .01340610     -.05641774     -.04432066
 ref:   6     -.00021890      .00023278      .00075710      .00015184     -.00308294      .00808928     -.04562238     -.00639297
 ref:   7      .00083640      .00133666      .00026190     -.00004948      .04670900      .01452914     -.02793925      .05115879
 ref:   8     -.00049480      .00684345     -.03457777     -.00507880     -.05828342     -.03252223      .21237031      .16474404
 ref:   9      .04198366      .01202036     -.00471644      .00027334      .33702585      .05892841      .07534076      .09514709
 ref:  10     -.01712102      .04197708      .00384605     -.00249619     -.09280694      .04282133     -.20177900      .27617210

                ci   9         ci  10         ci  11         ci  12
 ref:   1      .00008859     -.00009781      .00228106     -.00401775
 ref:   2      .00005069     -.00003560      .00484008      .00109705
 ref:   3     -.00015121      .00002464     -.00844107     -.00509208
 ref:   4     -.00009135      .00005921      .00043096      .00143112
 ref:   5      .03942969     -.00297702      .10709838      .26139332
 ref:   6      .04714807     -.02588484      .08606623     -.02287325
 ref:   7      .01680974      .02207622      .09620527     -.03804698
 ref:   8     -.19685660      .06151019      .09089987      .31858970
 ref:   9      .25727207      .26532776      .05898295      .09238792
 ref:  10      .14798734     -.18478565     -.08926051     -.03086340

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 56  1    -93.3414572876  4.0679E-12  0.0000E+00  5.7263E-04  1.0000E-03
 mr-sdci # 56  2    -93.3414570916  3.6179E-10  0.0000E+00  6.4055E-04  1.0000E-03
 mr-sdci # 56  3    -93.3414568805  1.5054E-09  0.0000E+00  7.1352E-04  1.0000E-03
 mr-sdci # 56  4    -93.3414567720  1.8495E-11  0.0000E+00  7.5521E-04  1.0000E-03
 mr-sdci # 56  5    -93.1685353927  4.5831E-08  0.0000E+00  3.8713E-04  1.0000E-03
 mr-sdci # 56  6    -93.1685348862  2.6890E-08  0.0000E+00  4.6811E-04  1.0000E-03
 mr-sdci # 56  7    -93.1685317055  7.8944E-08  0.0000E+00  5.6847E-04  1.0000E-03
 mr-sdci # 56  8    -93.1685254294  4.0127E-08  0.0000E+00  6.9870E-04  1.0000E-03
 mr-sdci # 56  9    -93.1670407419  7.0240E-05  2.9057E-05  3.1429E-03  1.0000E-03
 mr-sdci # 56 10    -93.1656918425  2.2443E-05  0.0000E+00  1.8687E-03  1.0000E-03
 mr-sdci # 56 11    -93.0959101337  1.0991E-01  0.0000E+00  1.3673E-01  1.0000E-04
 mr-sdci # 56 12    -92.9518040299 -1.5582E-01  0.0000E+00  2.1636E-01  1.0000E-04
 
 root number  9 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .34    .13    .00    .33
    2   25    0    .20    .07    .00    .19
    3   26    0   5.14    .19    .00   5.14
    4   11    0   2.21   1.25    .00   2.21
    5   15    0   4.57   1.70    .00   4.56
    6   16    0   2.66    .94    .00   2.66
    7    1    0    .49    .28    .00    .49
    8    5    0   2.25    .84    .00   2.25
    9    6    0   8.22    .70    .00   8.22
   10    7    0   1.66    .19    .00   1.66
   11   75    0    .08    .06    .00    .08
   12   45    0    .19    .09    .00    .18
   13   46    0    .56    .08    .00    .55
   14   47    0    .22    .04    .00    .22
   15   81    0    .51    .00    .00    .50
   16   82    0   1.55    .00    .00   1.54
   17  101    0    .28    .00    .00    .27
   18   83    0   1.66    .00    .00   1.66
   19  102    0   4.05    .00    .00   4.05
   20   84    0    .43    .00    .00    .42
   21   91    0    .62    .00    .00    .62
   22   93    0   2.26    .00    .00   2.25
   23   94    0   1.17    .00    .00   1.17
   24  103    0   2.95    .00    .00   2.95
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .060000
time for cinew                          .610000
time for eigenvalue solver              .000000
time for vector access                  .000000
================================================================
time spent in mult:                    44.2700s 
time spent in multnx:                  44.1700s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.5600s 
time for vector access in mult:          .0000s 
total time per CI iteration:           44.9500s 

          starting ci iteration  57

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     -.01700227      .18887721     -.95645414     -.13636784      .02123856     -.01173859     -.08021982      .06772121
 ref:   2     -.64033049     -.20883776     -.13054154      .70617313      .08071993      .06488155     -.01178740     -.02794093
 ref:   3      .70690247      .13699196     -.08057604      .66662813     -.06733675      .07960097     -.02776908      .00195261
 ref:   4     -.24356595      .93344418      .17616476      .08776725      .01203566      .03107787      .06544827      .07895991
 ref:   5      .00040714     -.00022036      .00158217      .00019104      .03359156     -.01299256     -.05538993      .04495516
 ref:   6     -.00022665      .00023217      .00075274      .00014320     -.00248579     -.00873587     -.04535177      .00683300
 ref:   7      .00083962      .00133605      .00025986     -.00005188      .04744678     -.01361692     -.02768876     -.05080337
 ref:   8     -.00058873      .00682047     -.03461325     -.00493578     -.06106271      .03359257      .21071108     -.16625813
 ref:   9      .04197526      .01197053     -.00481768      .00021081      .33812047     -.04799302      .07828245     -.09505908
 ref:  10     -.01706451      .04199781      .00385178     -.00249575     -.08903176     -.04927418     -.20453061     -.27484162

                ci   9         ci  10         ci  11         ci  12         ci  13
 ref:   1      .00007051     -.00010627     -.00031730     -.00586569      .00404986
 ref:   2     -.00004089      .00001031      .00328532     -.00103512      .00488015
 ref:   3     -.00005222     -.00002434     -.00467253     -.00010888     -.01265116
 ref:   4      .00005214      .00001964      .00038582      .00122714      .00065866
 ref:   5      .03399024      .00088814      .16478758      .25177744      .03566586
 ref:   6      .04329274     -.02256568      .12754260     -.00175469     -.09036657
 ref:   7      .01511467      .02311425      .06448434     -.07233342      .06444821
 ref:   8     -.20496304      .06216082      .19341813      .34367339     -.05222544
 ref:   9      .24785934      .27250088      .12487809      .11962988     -.08213728
 ref:  10      .15409698     -.18394064     -.10312897     -.02873216      .01706173

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 57  1    -93.3414572892  1.5959E-09  0.0000E+00  5.7172E-04  1.0000E-03
 mr-sdci # 57  2    -93.3414570916  2.8791E-11  0.0000E+00  6.4053E-04  1.0000E-03
 mr-sdci # 57  3    -93.3414568809  4.7170E-10  0.0000E+00  7.1314E-04  1.0000E-03
 mr-sdci # 57  4    -93.3414567725  5.0249E-10  0.0000E+00  7.5498E-04  1.0000E-03
 mr-sdci # 57  5    -93.1685354035  1.0880E-08  0.0000E+00  3.8903E-04  1.0000E-03
 mr-sdci # 57  6    -93.1685349069  2.0696E-08  0.0000E+00  4.6877E-04  1.0000E-03
 mr-sdci # 57  7    -93.1685317995  9.4011E-08  0.0000E+00  5.5946E-04  1.0000E-03
 mr-sdci # 57  8    -93.1685254556  2.6252E-08  0.0000E+00  6.9361E-04  1.0000E-03
 mr-sdci # 57  9    -93.1670824317  4.1690E-05  2.0765E-05  2.3670E-03  1.0000E-03
 mr-sdci # 57 10    -93.1657057722  1.3930E-05  0.0000E+00  1.4131E-03  1.0000E-03
 mr-sdci # 57 11    -93.1356484969  3.9738E-02  0.0000E+00  6.2928E-02  1.0000E-04
 mr-sdci # 57 12    -92.9658291446  1.4025E-02  0.0000E+00  2.0298E-01  1.0000E-04
 mr-sdci # 57 13    -92.8473663041 -1.3486E-01  0.0000E+00  2.6261E-01  1.0000E-04
 
 root number  9 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .32    .11    .00    .32
    2   25    0    .20    .06    .00    .20
    3   26    0   5.09    .18    .00   5.09
    4   11    0   2.16   1.21    .00   2.16
    5   15    0   4.55   1.66    .00   4.54
    6   16    0   2.64    .94    .00   2.64
    7    1    0    .46    .27    .00    .46
    8    5    0   2.19    .81    .00   2.18
    9    6    0   8.28    .73    .00   8.27
   10    7    0   1.72    .20    .00   1.72
   11   75    0    .07    .05    .00    .07
   12   45    0    .20    .09    .00    .19
   13   46    0    .55    .09    .00    .55
   14   47    0    .23    .03    .00    .23
   15   81    0    .50    .00    .00    .50
   16   82    0   1.56    .00    .00   1.55
   17  101    0    .27    .00    .00    .26
   18   83    0   1.66    .00    .00   1.66
   19  102    0   4.15    .00    .00   4.14
   20   84    0    .44    .00    .00    .44
   21   91    0    .62    .00    .00    .62
   22   93    0   2.29    .00    .00   2.28
   23   94    0   1.17    .00    .00   1.17
   24  103    0   2.93    .00    .00   2.93
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .040000
time for cinew                          .760000
time for eigenvalue solver              .010000
time for vector access                  .000000
================================================================
time spent in mult:                    44.2500s 
time spent in multnx:                  44.1700s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.4300s 
time for vector access in mult:          .0000s 
total time per CI iteration:           45.0600s 

          starting ci iteration  58

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1      .01876773      .18761664     -.95782072     -.12802939     -.02189394     -.01096898     -.08003981     -.06785418
 ref:   2      .63938475     -.20728927     -.12282765      .70886500     -.07802775      .06814310     -.01169273      .02786348
 ref:   3     -.70833780      .13665020     -.07611167      .66569913      .07050500      .07664545     -.02822640     -.00177608
 ref:   4      .24174473      .93409348      .17627192      .08565390     -.01033872      .03180211      .06549014     -.07887731
 ref:   5     -.00040540     -.00022042      .00157909      .00017035     -.03422452     -.01166088     -.05518114     -.04508984
 ref:   6      .00023325      .00023067      .00074641      .00012576      .00190636     -.00896880     -.04534964     -.00686042
 ref:   7     -.00084081      .00133476      .00025530     -.00005877     -.04814944     -.01173212     -.02752327      .05066883
 ref:   8      .00066040      .00677223     -.03466970     -.00464395      .06306219      .03144387      .21027820      .16654886
 ref:   9     -.04199035      .01190799     -.00490118      .00016194     -.33967339     -.03369144      .07956259      .09449450
 ref:  10      .01700384      .04202343      .00381037     -.00254217      .08545644     -.05386275     -.20507322      .27481138

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14
 ref:   1      .00004187     -.00009425     -.00007141     -.00527938     -.00272921      .00405032
 ref:   2     -.00008906      .00002745     -.00251806      .00160311     -.00365876      .00498866
 ref:   3      .00003018     -.00005312      .00357601     -.00348594      .00414274     -.01279780
 ref:   4      .00009099      .00001100     -.00030010      .00102853      .00070009      .00065393
 ref:   5      .03307592      .00170378     -.12858177      .29409905      .03571466      .03836570
 ref:   6      .04392115     -.02239822     -.07946657      .13927952     -.18224422     -.08421730
 ref:   7      .01422836      .02361723     -.05613974     -.02532295     -.08313865      .06608341
 ref:   8     -.20576972      .06072189     -.13407781      .41332192      .03133881     -.04793153
 ref:   9      .24202749      .27641473     -.14842747      .04123446      .14820071     -.08563194
 ref:  10      .15688677     -.18341420      .09199298     -.04829266      .01161756      .01638128

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 58  1    -93.3414572901  8.3768E-10  0.0000E+00  5.7151E-04  1.0000E-03
 mr-sdci # 58  2    -93.3414570917  9.3184E-11  0.0000E+00  6.4054E-04  1.0000E-03
 mr-sdci # 58  3    -93.3414568816  6.7024E-10  0.0000E+00  7.1241E-04  1.0000E-03
 mr-sdci # 58  4    -93.3414567739  1.3780E-09  0.0000E+00  7.5494E-04  1.0000E-03
 mr-sdci # 58  5    -93.1685354391  3.5561E-08  0.0000E+00  3.7743E-04  1.0000E-03
 mr-sdci # 58  6    -93.1685349196  1.2700E-08  0.0000E+00  4.6736E-04  1.0000E-03
 mr-sdci # 58  7    -93.1685318046  5.1080E-09  0.0000E+00  5.5776E-04  1.0000E-03
 mr-sdci # 58  8    -93.1685254640  8.4170E-09  0.0000E+00  6.9427E-04  1.0000E-03
 mr-sdci # 58  9    -93.1671039495  2.1518E-05  7.6668E-06  1.6357E-03  1.0000E-03
 mr-sdci # 58 10    -93.1657117767  6.0045E-06  0.0000E+00  1.1019E-03  1.0000E-03
 mr-sdci # 58 11    -93.1450620773  9.4136E-03  0.0000E+00  3.8888E-02  1.0000E-04
 mr-sdci # 58 12    -92.9806489086  1.4820E-02  0.0000E+00  1.8820E-01  1.0000E-04
 mr-sdci # 58 13    -92.9402647742  9.2898E-02  0.0000E+00  2.0838E-01  1.0000E-04
 mr-sdci # 58 14    -92.8472844993 -7.5757E-02  0.0000E+00  2.6238E-01  1.0000E-04
 
 root number  9 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0    .35    .13    .00    .35
    2   25    0    .20    .07    .00    .20
    3   26    0   5.13    .19    .00   5.13
    4   11    0   2.18   1.24    .00   2.18
    5   15    0   4.52   1.67    .00   4.51
    6   16    0   2.63    .91    .00   2.63
    7    1    0    .50    .28    .00    .49
    8    5    0   2.28    .85    .00   2.27
    9    6    0   8.30    .75    .00   8.29
   10    7    0   1.72    .20    .00   1.71
   11   75    0    .09    .05    .00    .08
   12   45    0    .20    .10    .00    .20
   13   46    0    .57    .08    .00    .57
   14   47    0    .22    .04    .00    .22
   15   81    0    .52    .00    .00    .52
   16   82    0   1.57    .00    .00   1.56
   17  101    0    .27    .00    .00    .26
   18   83    0   1.65    .00    .00   1.64
   19  102    0   4.16    .00    .00   4.16
   20   84    0    .41    .00    .00    .41
   21   91    0    .63    .00    .00    .63
   22   93    0   2.29    .00    .00   2.28
   23   94    0   1.17    .00    .00   1.17
   24  103    0   2.96    .00    .00   2.96
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction   .060000
time for cinew                          .760000
time for eigenvalue solver              .000000
time for vector access                  .000000
================================================================
time spent in mult:                    44.5200s 
time spent in multnx:                  44.4200s 
integral transfer time:                  .0000s 
time spent for loop construction:       6.5600s 
time for vector access in mult:          .0100s 
total time per CI iteration:           45.3600s 

          starting ci iteration  59

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:    339878 2x:    111419 4x:     25025
All internal counts: zz :     59067 yy:    375081 xx:    435436 ww:    100832
One-external counts: yz :    263356 yx:    705046 yw:    520656
Two-external counts: yy :    226736 ww:     67508 xx:    358876 xz:     44665 wz:     23975 wx:    234556
Three-ext.   counts: yx :    137672 yw:     71757

SO-0ex       counts: zz :     82216 yy:    577145 xx:    806186 ww:  148954
SO-1ex       counts: yz :    195563 yx:    650224 yw:    388093
SO-2ex       counts: yy :    102636 xx:    759570 wx:    973308
====================================================================================================


 xx2xso2= 668850


LOOPCOUNT per task:
task #   1:     20613    task #   2:     11017    task #   3:     27461    task #   4:    181187
task #   5:    222140    task #   6:    124214    task #   7:     35392    task #   8:     69112
task #   9:     52709    task #  10:     16241    task #  11:      9956    task #  12:         1
task #  13:         1    task #  14:         1    task #  15:     77631    task #  16:    287091
task #  17:     22988    task #  18:    274679    task #  19:     25066    task #  20:     72246
task #  21:     94274    task #  22:    190311    task #  23:     89873    task #  24:     19361

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1      .01858611      .18705015      .95765753     -.13008904      .02206865      .01111255      .07991970      .06791579
 ref:   2      .63918426     -.20776281      .12443579      .70862665      .07762542     -.06857235      .01185565     -.02786534
 ref:   3     -.70843593      .13654604      .07749223      .66545678     -.07093643     -.07610243      .02861236      .00176525
 ref:   4      .24200129      .93411712     -.17542726      .08640252      .01003763     -.03213234     -.06544010      .07882369
 ref:   5     -.00040567     -.00022014     -.00157729      .00017506      .03435700      .01161288      .05500977      .04514681
 ref:   6      .00023288      .00023031     -.00074429      .00012886     -.00179402      .00913028      .04528542      .00690098
 ref:   7     -.00084050      .00133503     -.00025457     -.00005770      .04826336      .01154334      .02743279     -.05063738
 ref:   8      .00065284      .00675049      .03466650     -.00471627     -.06357563     -.03187186     -.21008604     -.16668100
 ref:   9     -.04198761      .01191643      .00489493      .00014547      .33977945      .03133844     -.08007611     -.09450632
 ref:  10      .01701426      .04202118     -.00378188     -.00251407     -.08477820      .05526400      .20523096     -.27467436

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15
 ref:   1      .00002129      .00008761      .00007233      .00172253      .00542840      .00145555     -.00451086
 ref:   2     -.00009617     -.00003241      .00226335      .00410355     -.00132297      .00169479     -.00460113
 ref:   3      .00004780      .00006162     -.00313959     -.00764459      .00297794      .00020127      .01157217
 ref:   4      .00012059     -.00000152      .00017708      .00058018     -.00100408     -.00100052     -.00005310
 ref:   5      .03184419     -.00261396      .13282147      .00177882     -.29474411     -.03352822     -.05516829
 ref:   6      .04308616      .02165793      .08529928      .01258926     -.13717969      .20486057      .01477961
 ref:   7      .01379786     -.02389412      .05377845      .05804654      .02966935      .05636111     -.07312926
 ref:   8     -.20746683     -.06098317      .14212300     -.04715661     -.41711557      .00551348     -.00150728
 ref:   9      .24071758     -.27751335      .13326304      .03712977     -.04148263     -.15087478      .19769501
 ref:  10      .15728251      .18276467     -.07271906     -.10722157      .04262585      .01946052     -.09954255

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 59  1    -93.3414572901  3.8156E-11  0.0000E+00  5.7146E-04  1.0000E-03
 mr-sdci # 59  2    -93.3414570918  6.0009E-11  0.0000E+00  6.4060E-04  1.0000E-03
 mr-sdci # 59  3    -93.3414568819  2.9395E-10  0.0000E+00  7.1205E-04  1.0000E-03
 mr-sdci # 59  4    -93.3414567741  1.9474E-10  0.0000E+00  7.5496E-04  1.0000E-03
 mr-sdci # 59  5    -93.1685354405  1.4294E-09  0.0000E+00  3.7682E-04  1.0000E-03
 mr-sdci # 59  6    -93.1685349267  7.1348E-09  0.0000E+00  4.6560E-04  1.0000E-03
 mr-sdci # 59  7    -93.1685318235  1.8926E-08  0.0000E+00  5.4880E-04  1.0000E-03
 mr-sdci # 59  8    -93.1685254651  1.0353E-09  0.0000E+00  6.9319E-04  1.0000E-03
 mr-sdci # 59  9    -93.1671118464  7.8969E-06  2.2084E-06  7.6063E-04  1.0000E-03
 mr-sdci # 59 10    -93.1657147849  3.0082E-06  0.0000E+00  6.8321E-04  1.0000E-03
 mr-sdci # 59 11    -93.1482256120  3.1635E-03  0.0000E+00  2.1386E-02  1.0000E-04
 mr-sdci # 59 12    -93.0012449108  2.0596E-02  0.0000E+00  1.4845E-01  1.0000E-04
 mr-sdci # 59 13    -92.9805816302  4.0317E-02  0.0000E+00  1.8902E-01  1.0000E-04
 mr-sdci # 59 14    -92.9306718256  8.3387E-02  0.0000E+00  2.2280E-01  1.0000E-04
 mr-sdci # 59 15    -92.7500335089 -1.5847E-01  0.0000E+00  2.8407E-01  1.0000E-04
 

 mr-sdci  convergence criteria satisfied after 59 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 59  1    -93.3414572901  3.8156E-11  0.0000E+00  5.7146E-04  1.0000E-03
 mr-sdci # 59  2    -93.3414570918  6.0009E-11  0.0000E+00  6.4060E-04  1.0000E-03
 mr-sdci # 59  3    -93.3414568819  2.9395E-10  0.0000E+00  7.1205E-04  1.0000E-03
 mr-sdci # 59  4    -93.3414567741  1.9474E-10  0.0000E+00  7.5496E-04  1.0000E-03
 mr-sdci # 59  5    -93.1685354405  1.4294E-09  0.0000E+00  3.7682E-04  1.0000E-03
 mr-sdci # 59  6    -93.1685349267  7.1348E-09  0.0000E+00  4.6560E-04  1.0000E-03
 mr-sdci # 59  7    -93.1685318235  1.8926E-08  0.0000E+00  5.4880E-04  1.0000E-03
 mr-sdci # 59  8    -93.1685254651  1.0353E-09  0.0000E+00  6.9319E-04  1.0000E-03
 mr-sdci # 59  9    -93.1671118464  7.8969E-06  2.2084E-06  7.6063E-04  1.0000E-03
 mr-sdci # 59 10    -93.1657147849  3.0082E-06  0.0000E+00  6.8321E-04  1.0000E-03
 mr-sdci # 59 11    -93.1482256120  3.1635E-03  0.0000E+00  2.1386E-02  1.0000E-04
 mr-sdci # 59 12    -93.0012449108  2.0596E-02  0.0000E+00  1.4845E-01  1.0000E-04
 mr-sdci # 59 13    -92.9805816302  4.0317E-02  0.0000E+00  1.8902E-01  1.0000E-04
 mr-sdci # 59 14    -92.9306718256  8.3387E-02  0.0000E+00  2.2280E-01  1.0000E-04
 mr-sdci # 59 15    -92.7500335089 -1.5847E-01  0.0000E+00  2.8407E-01  1.0000E-04

####################CIUDGINFO####################

   ci vector at position   1 energy=  -93.341457290122
   ci vector at position   2 energy=  -93.341457091752
   ci vector at position   3 energy=  -93.341456881895
   ci vector at position   4 energy=  -93.341456774100
   ci vector at position   5 energy=  -93.168535440530
   ci vector at position   6 energy=  -93.168534926710
   ci vector at position   7 energy=  -93.168531823499
   ci vector at position   8 energy=  -93.168525465069
   ci vector at position   9 energy=  -93.167111846378
   ci vector at position  10 energy=  -93.165714784914

################END OF CIUDGINFO################

 
   10 of the  16 expansion vectors are transformed.
   10 of the  15 matrix-vector products are transformed.

   10 expansion eigenvectors written to unit nvfile (= 11)
   10 matrix-vector products written to unit nhvfil (= 10)
 bummer (warning):Biggest overlap is smaller than 0.85         0
   Overlap of CI vector # 1  with reference vector # 1 0.185861082772563156E-01
   Overlap of CI vector # 1  with reference vector # 2 0.639184264177150463
   Overlap of CI vector # 1  with reference vector # 3 -0.708435934643152576
   Overlap of CI vector # 1  with reference vector # 4 0.242001292194775980
   Overlap of CI vector # 1  with reference vector # 5 -0.405671043542337622E-03
   Overlap of CI vector # 1  with reference vector # 6 0.232884605594670373E-03
   Overlap of CI vector # 1  with reference vector # 7 -0.840500517840749657E-03
   Overlap of CI vector # 1  with reference vector # 8 0.652835742657704586E-03
   Overlap of CI vector # 1  with reference vector # 9 -0.419876079339692662E-01
   Overlap of CI vector # 1  with reference vector # 10 0.170142623167844932E-01

information on vector: 1from unit 11 written to unit 16filename civout                                                      
 maximum overlap with reference  4 (overlap= 0.934117120953129265 )

information on vector: 2from unit 11 written to unit 16filename civout                                                      
 maximum overlap with reference  1 (overlap= 0.957657531740733647 )

information on vector: 3from unit 11 written to unit 16filename civout                                                      
 bummer (warning):Biggest overlap is smaller than 0.85         0
   Overlap of CI vector # 4  with reference vector # 1 -0.130089042571127744
   Overlap of CI vector # 4  with reference vector # 2 0.708626648700148909
   Overlap of CI vector # 4  with reference vector # 3 0.665456775804564860
   Overlap of CI vector # 4  with reference vector # 4 0.864025249069250989E-01
   Overlap of CI vector # 4  with reference vector # 5 0.175060831746207086E-03
   Overlap of CI vector # 4  with reference vector # 6 0.128863214035798988E-03
   Overlap of CI vector # 4  with reference vector # 7 -0.576954901769485601E-04
   Overlap of CI vector # 4  with reference vector # 8 -0.471627386712613478E-02
   Overlap of CI vector # 4  with reference vector # 9 0.145472867228657517E-03
   Overlap of CI vector # 4  with reference vector # 10 -0.251406886932433425E-02

information on vector: 4from unit 11 written to unit 16filename civout                                                      
 bummer (warning):Biggest overlap is smaller than 0.85         0
   Overlap of CI vector # 5  with reference vector # 1 0.220686481406764182E-01
   Overlap of CI vector # 5  with reference vector # 2 0.776254237807001146E-01
   Overlap of CI vector # 5  with reference vector # 3 -0.709364334206619013E-01
   Overlap of CI vector # 5  with reference vector # 4 0.100376340241487491E-01
   Overlap of CI vector # 5  with reference vector # 5 0.343570000869977410E-01
   Overlap of CI vector # 5  with reference vector # 6 -0.179402144757791226E-02
   Overlap of CI vector # 5  with reference vector # 7 0.482633563409450850E-01
   Overlap of CI vector # 5  with reference vector # 8 -0.635756274001330474E-01
   Overlap of CI vector # 5  with reference vector # 9 0.339779449797735500
   Overlap of CI vector # 5  with reference vector # 10 -0.847782012338978014E-01

information on vector: 5from unit 11 written to unit 16filename civout                                                      
 bummer (warning):Biggest overlap is smaller than 0.85         0
   Overlap of CI vector # 6  with reference vector # 1 0.111125472589434400E-01
   Overlap of CI vector # 6  with reference vector # 2 -0.685723497066650228E-01
   Overlap of CI vector # 6  with reference vector # 3 -0.761024306242288479E-01
   Overlap of CI vector # 6  with reference vector # 4 -0.321323396345580617E-01
   Overlap of CI vector # 6  with reference vector # 5 0.116128778348374376E-01
   Overlap of CI vector # 6  with reference vector # 6 0.913028219903300187E-02
   Overlap of CI vector # 6  with reference vector # 7 0.115433432826981524E-01
   Overlap of CI vector # 6  with reference vector # 8 -0.318718608126604894E-01
   Overlap of CI vector # 6  with reference vector # 9 0.313384437505721120E-01
   Overlap of CI vector # 6  with reference vector # 10 0.552640034362224045E-01

information on vector: 6from unit 11 written to unit 16filename civout                                                      
 bummer (warning):Biggest overlap is smaller than 0.85         0
   Overlap of CI vector # 7  with reference vector # 1 0.799197012653357908E-01
   Overlap of CI vector # 7  with reference vector # 2 0.118556528704840038E-01
   Overlap of CI vector # 7  with reference vector # 3 0.286123627085671656E-01
   Overlap of CI vector # 7  with reference vector # 4 -0.654401008958310076E-01
   Overlap of CI vector # 7  with reference vector # 5 0.550097740944026359E-01
   Overlap of CI vector # 7  with reference vector # 6 0.452854247817661357E-01
   Overlap of CI vector # 7  with reference vector # 7 0.274327893029725385E-01
   Overlap of CI vector # 7  with reference vector # 8 -0.210086042387220340
   Overlap of CI vector # 7  with reference vector # 9 -0.800761129906293195E-01
   Overlap of CI vector # 7  with reference vector # 10 0.205230960364306186

information on vector: 7from unit 11 written to unit 16filename civout                                                      
 bummer (warning):Biggest overlap is smaller than 0.85         0
   Overlap of CI vector # 8  with reference vector # 1 0.679157903152657894E-01
   Overlap of CI vector # 8  with reference vector # 2 -0.278653379109963274E-01
   Overlap of CI vector # 8  with reference vector # 3 0.176524988655096349E-02
   Overlap of CI vector # 8  with reference vector # 4 0.788236928643515633E-01
   Overlap of CI vector # 8  with reference vector # 5 0.451468145032478257E-01
   Overlap of CI vector # 8  with reference vector # 6 0.690098227931519505E-02
   Overlap of CI vector # 8  with reference vector # 7 -0.506373772876897651E-01
   Overlap of CI vector # 8  with reference vector # 8 -0.166680998401341707
   Overlap of CI vector # 8  with reference vector # 9 -0.945063222716089474E-01
   Overlap of CI vector # 8  with reference vector # 10 -0.274674358153218112

information on vector: 8from unit 11 written to unit 16filename civout                                                      
 bummer (warning):Biggest overlap is smaller than 0.85         0
   Overlap of CI vector # 9  with reference vector # 1 0.212923070683830046E-04
   Overlap of CI vector # 9  with reference vector # 2 -0.961710576338644079E-04
   Overlap of CI vector # 9  with reference vector # 3 0.477987734385641427E-04
   Overlap of CI vector # 9  with reference vector # 4 0.120585755125221221E-03
   Overlap of CI vector # 9  with reference vector # 5 0.318441927629364227E-01
   Overlap of CI vector # 9  with reference vector # 6 0.430861630309225460E-01
   Overlap of CI vector # 9  with reference vector # 7 0.137978611614187219E-01
   Overlap of CI vector # 9  with reference vector # 8 -0.207466827290001460
   Overlap of CI vector # 9  with reference vector # 9 0.240717576758573998
   Overlap of CI vector # 9  with reference vector # 10 0.157282506764540919

information on vector: 9from unit 11 written to unit 16filename civout                                                      
 bummer (warning):Biggest overlap is smaller than 0.85         0
   Overlap of CI vector # 10  with reference vector # 1 0.876100431461049035E-04
   Overlap of CI vector # 10  with reference vector # 2 -0.324067467908158399E-04
   Overlap of CI vector # 10  with reference vector # 3 0.616164127735085575E-04
   Overlap of CI vector # 10  with reference vector # 4 -0.151510788863721159E-05
   Overlap of CI vector # 10  with reference vector # 5 -0.261396417632559636E-02
   Overlap of CI vector # 10  with reference vector # 6 0.216579288161101099E-01
   Overlap of CI vector # 10  with reference vector # 7 -0.238941171988158868E-01
   Overlap of CI vector # 10  with reference vector # 8 -0.609831698424503563E-01
   Overlap of CI vector # 10  with reference vector # 9 -0.277513349556947342
   Overlap of CI vector # 10  with reference vector # 10 0.182764668184948492

information on vector:10from unit 11 written to unit 16filename civout                                                      


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -93.3414572901

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7

                                          orbital    34   37   38   45   46   53   54

                                         symmetry     5    6    6    7    7    8    8

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  7  1     152   .027865                        +-   +    +    +    +    +         +  
 z*  7  5     153  -.011257                        +-   +    +    +    +    +         +  
 z*  5  4     246  -.014345                        +-   +         +    +    +    +     - 
 z*  7  3     247   .013605                        +-   +         +    +    +    +    +  
 z*  5  4     344   .014894                        +-        +    +    +    +    +     - 
 z*  7  3     345  -.014469                        +-        +    +    +    +    +    +  
 z*  7  1     527   .015153                        +    +-   +         +    +    +    +  
 z*  7  1     783  -.041988                        +    +    +-   +         +    +    +  
 z*  7  5     784   .017014                        +    +    +-   +         +    +    +  
 z*  5  4     898  -.021090                        +    +    +    +-   +    +          - 
 z*  7  3     899   .020263                        +    +    +    +-   +    +         +  
 z*  5  4     903  -.020798                        +    +    +    +-   +         +     - 
 z*  7  3     904   .020181                        +    +    +    +-   +         +    +  
 z*  5  4     950  -.012770                        +    +    +    +    +-   +          - 
 z*  7  3     951   .012433                        +    +    +    +    +-   +         +  
 z*  7  2     980   .018586                        +    +    +    +    +    +    +     - 
 z*  7  6     981   .639184                        +    +    +    +    +    +    +     - 
 z*  9  3     982  -.708436                        +    +    +    +    +    +    +    +  
 z*  9  7     983   .242001                        +    +    +    +    +    +    +    +  
 z*  5  4     995   .014535                        +    +    +    +         +    +-    - 
 z*  7  3     996  -.013824                        +    +    +    +         +    +-   +  
 z*  5  4    1018   .021793                        +    +    +         +    +-   +     - 
 z*  7  3    1019  -.020973                        +    +    +         +    +-   +    +  
 z*  5  4    1023  -.019768                        +    +    +         +    +    +-    - 
 z*  7  3    1024   .019214                        +    +    +         +    +    +-   +  
 z*  7  1    1080   .042037                        +    +         +    +-   +    +    +  
 z*  7  5    1081  -.016999                        +    +         +    +-   +    +    +  
 z*  7  1    1229  -.013224                        +         +    +-   +    +    +    +  
 z*  5  4    1394   .016357                             +-   +    +    +    +    +     - 
 z*  7  3    1395  -.015616                             +-   +    +    +    +    +    +  
 z*  5  4    1492   .012540                             +    +-   +    +    +    +     - 
 z*  7  3    1493  -.012208                             +    +-   +    +    +    +    +  
 z*  7  1    1616  -.027261                             +    +    +    +    +    +-   +  
 z*  7  5    1617   .011144                             +    +    +    +    +    +-   +  
 x   9  1   39376   .013812    1(   1)   2(   1)   +    +    +    +    +              +  
 x   9  5   39400  -.015315    1(   1)   2(   1)   +    +    +    +    +              +  
 x   9  1   39545  -.015491    2(   1)   1(   2)   +    +    +    +         +         +  
 x   9  1   39548  -.010092    1(   3)   1(   4)   +    +    +    +         +         +  
 x   9  5   39569   .017166    2(   1)   1(   2)   +    +    +    +         +         +  
 x   9  5   39572   .011176    1(   3)   1(   4)   +    +    +    +         +         +  
 x   9  1   39716   .012010    1(   3)   1(   4)   +    +    +    +              +    +  
 x   9  5   39740  -.013317    1(   3)   1(   4)   +    +    +    +              +    +  
 x   9  1   40192   .014877    1(   1)   1(   2)   +    +    +         +    +         +  
 x   9  1   40193   .017107    2(   1)   1(   2)   +    +    +         +    +         +  
 x   9  5   40216  -.016486    1(   1)   1(   2)   +    +    +         +    +         +  
 x   9  5   40217  -.018957    2(   1)   1(   2)   +    +    +         +    +         +  
 x   9  1   40360   .023185    1(   1)   1(   2)   +    +    +         +         +    +  
 x   9  5   40384  -.025695    1(   1)   1(   2)   +    +    +         +         +    +  
 x   9  1   40720  -.013787    1(   1)   2(   1)   +    +    +              +    +    +  
 x   9  5   40744   .015289    1(   1)   2(   1)   +    +    +              +    +    +  
 x   9  1   42356   .014591    1(   2)   1(   4)   +    +         +    +    +         +  
 x   9  5   42377   .010437    2(   1)   1(   3)   +    +         +    +    +         +  
 x   9  5   42380  -.016173    1(   2)   1(   4)   +    +         +    +    +         +  
 x   9  1   42521   .011980    2(   1)   1(   3)   +    +         +    +         +    +  
 x   9  5   42545  -.013284    2(   1)   1(   3)   +    +         +    +         +    +  
 x   9  1   42882  -.030681    2(   1)   1(   4)   +    +         +         +    +    +  
 x   9  5   42906   .034006    2(   1)   1(   4)   +    +         +         +    +    +  
 x   9  9   42930  -.011611    2(   1)   1(   4)   +    +         +         +    +    +  
 x   9  1   43841   .013819    1(   1)   1(   4)   +    +              +    +    +    +  
 x   9  5   43865  -.015323    1(   1)   1(   4)   +    +              +    +    +    +  
 x   9  1   49073  -.015248    2(   1)   1(   3)   +         +    +    +    +         +  
 x   9  5   49097   .016900    2(   1)   1(   3)   +         +    +    +    +         +  
 x   9  1   49240  -.028448    1(   1)   1(   3)   +         +    +    +         +    +  
 x   9  5   49264   .031526    1(   1)   1(   3)   +         +    +    +         +    +  
 x   9  9   49288  -.010767    1(   1)   1(   3)   +         +    +    +         +    +  
 x   9  1   49601   .013783    1(   1)   1(   4)   +         +    +         +    +    +  
 x   9  5   49625  -.015283    1(   1)   1(   4)   +         +    +         +    +    +  
 x   9  1   50560   .016405    1(   2)   1(   3)   +         +         +    +    +    +  
 x   9  5   50584  -.018192    1(   2)   1(   3)   +         +         +    +    +    +  
 x   9  1   53200  -.013800    1(   1)   2(   1)   +              +    +    +    +    +  
 x   9  5   53224   .015302    1(   1)   2(   1)   +              +    +    +    +    +  
 x   9  1   68512  -.020039    1(   2)   1(   3)        +    +    +    +    +         +  
 x   9  1   68514   .010138    2(   1)   1(   4)        +    +    +    +    +         +  
 x   9  5   68536   .022212    1(   2)   1(   3)        +    +    +    +    +         +  
 x   9  5   68538  -.011253    2(   1)   1(   4)        +    +    +    +    +         +  
 x   9  1   68681   .013659    1(   1)   1(   4)        +    +    +    +         +    +  
 x   9  5   68705  -.015145    1(   1)   1(   4)        +    +    +    +         +    +  
 x   9  1   69041   .012819    2(   1)   1(   3)        +    +    +         +    +    +  
 x   9  1   69044  -.011564    1(   2)   1(   4)        +    +    +         +    +    +  
 x   9  5   69065  -.014208    2(   1)   1(   3)        +    +    +         +    +    +  
 x   9  5   69068   .012813    1(   2)   1(   4)        +    +    +         +    +    +  
 x   9  1   70000  -.010861    1(   1)   1(   3)        +    +         +    +    +    +  
 x   9  1   70004  -.017029    1(   2)   1(   4)        +    +         +    +    +    +  
 x   9  5   70024   .012043    1(   1)   1(   3)        +    +         +    +    +    +  
 x   9  5   70028   .018873    1(   2)   1(   4)        +    +         +    +    +    +  
 x   9  1   72641  -.012458    2(   1)   1(   2)        +         +    +    +    +    +  
 x   9  1   72644  -.013497    1(   3)   1(   4)        +         +    +    +    +    +  
 x   9  5   72665   .013802    2(   1)   1(   2)        +         +    +    +    +    +  
 x   9  5   72668   .014954    1(   3)   1(   4)        +         +    +    +    +    +  
 x   9  1   79696   .011600    1(   1)   1(   2)             +    +    +    +    +    +  
 x   9  1   79700   .015632    1(   3)   1(   4)             +    +    +    +    +    +  
 x   9  5   79720  -.012850    1(   1)   1(   2)             +    +    +    +    +    +  
 x   9  5   79724  -.017321    1(   3)   1(   4)             +    +    +    +    +    +  

 ci coefficient statistics:
           rq > 0.1                3
      0.1> rq > 0.01              90
     0.01> rq > 0.001            190
    0.001> rq > 0.0001          1750
   0.0001> rq > 0.00001         6659
  0.00001> rq > 0.000001       17361
 0.000001> rq                  90778
           all                116831


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 2) =       -93.3414570918

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7

                                          orbital    34   37   38   45   46   53   54

                                         symmetry     5    6    6    7    7    8    8

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  7  5     153  -.027212                        +-   +    +    +    +    +         +  
 z*  7  4     168  -.017013                        +-   +    +    +         +    +    +  
 z*  7  4     192  -.022632                        +-   +    +         +    +    +    +  
 z*  7  4     489  -.016066                        +    +-   +    +    +    +         +  
 z*  7  4     494   .038348                        +    +-   +    +    +         +    +  
 z*  7  5     528  -.013070                        +    +-   +         +    +    +    +  
 z*  7  4     769   .014949                        +    +    +-   +    +    +         +  
 z*  7  1     783   .011916                        +    +    +-   +         +    +    +  
 z*  7  5     784   .042021                        +    +    +-   +         +    +    +  
 z*  7  2     980   .187050                        +    +    +    +    +    +    +     - 
 z*  7  6     981  -.207763                        +    +    +    +    +    +    +     - 
 z*  9  3     982   .136546                        +    +    +    +    +    +    +    +  
 z*  9  7     983   .934117                        +    +    +    +    +    +    +    +  
 z*  7  1    1080  -.011925                        +    +         +    +-   +    +    +  
 z*  7  5    1081  -.041822                        +    +         +    +-   +    +    +  
 z*  7  4    1099  -.017230                        +    +         +    +    +    +-   +  
 z*  7  5    1230   .015282                        +         +    +-   +    +    +    +  
 z*  7  4    1276  -.038883                        +         +    +    +    +-   +    +  
 z*  7  4    1281  -.014371                        +         +    +    +    +    +-   +  
 z*  7  4    1566  -.024160                             +    +    +-   +    +    +    +  
 z*  7  4    1599   .014638                             +    +    +    +-   +    +    +  
 z*  7  5    1617   .027793                             +    +    +    +    +    +-   +  
 x   9  9   39424   .020181    1(   1)   2(   1)   +    +    +    +    +              +  
 x   9  9   39593  -.022620    2(   1)   1(   2)   +    +    +    +         +         +  
 x   9  9   39596  -.014711    1(   3)   1(   4)   +    +    +    +         +         +  
 x   9  9   39761  -.010063    2(   1)   1(   2)   +    +    +    +              +    +  
 x   9  9   39764   .017538    1(   3)   1(   4)   +    +    +    +              +    +  
 x   9  9   40240   .021725    1(   1)   1(   2)   +    +    +         +    +         +  
 x   9  9   40241   .024982    2(   1)   1(   2)   +    +    +         +    +         +  
 x   9  9   40408   .033859    1(   1)   1(   2)   +    +    +         +         +    +  
 x   9  9   40768  -.020153    1(   1)   2(   1)   +    +    +              +    +    +  
 x   9  9   42401  -.013746    2(   1)   1(   3)   +    +         +    +    +         +  
 x   9  9   42404   .021317    1(   2)   1(   4)   +    +         +    +    +         +  
 x   9  9   42569   .017498    2(   1)   1(   3)   +    +         +    +         +    +  
 x   9  9   42572   .010123    1(   2)   1(   4)   +    +         +    +         +    +  
 x   9  9   42930  -.044831    2(   1)   1(   4)   +    +         +         +    +    +  
 x   9  9   43889   .020197    1(   1)   1(   4)   +    +              +    +    +    +  
 x   9  9   49120   .010168    1(   1)   1(   3)   +         +    +    +    +         +  
 x   9  9   49121  -.022266    2(   1)   1(   3)   +         +    +    +    +         +  
 x   9  9   49288  -.041547    1(   1)   1(   3)   +         +    +    +         +    +  
 x   9  9   49649   .020133    1(   1)   1(   4)   +         +    +         +    +    +  
 x   9  9   50608   .023970    1(   2)   1(   3)   +         +         +    +    +    +  
 x   9  9   53248  -.020159    1(   1)   2(   1)   +              +    +    +    +    +  
 x   9  9   68560  -.029281    1(   2)   1(   3)        +    +    +    +    +         +  
 x   9  9   68562   .014820    2(   1)   1(   4)        +    +    +    +    +         +  
 x   9  9   68729   .019954    1(   1)   1(   4)        +    +    +    +         +    +  
 x   9  9   69089   .018724    2(   1)   1(   3)        +    +    +         +    +    +  
 x   9  9   69092  -.016877    1(   2)   1(   4)        +    +    +         +    +    +  
 x   9  9   70048  -.015871    1(   1)   1(   3)        +    +         +    +    +    +  
 x   9  9   70052  -.024873    1(   2)   1(   4)        +    +         +    +    +    +  
 x   9  9   72689  -.018171    2(   1)   1(   2)        +         +    +    +    +    +  
 x   9  9   72692  -.019702    1(   3)   1(   4)        +         +    +    +    +    +  
 x   9  9   79744   .016911    1(   1)   1(   2)             +    +    +    +    +    +  
 x   9  9   79748   .022818    1(   3)   1(   4)             +    +    +    +    +    +  

 ci coefficient statistics:
           rq > 0.1                4
      0.1> rq > 0.01              50
     0.01> rq > 0.001            292
    0.001> rq > 0.0001          1777
   0.0001> rq > 0.00001         6679
  0.00001> rq > 0.000001       19289
 0.000001> rq                  88740
           all                116831


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 3) =       -93.3414568819

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7

                                          orbital    34   37   38   45   46   53   54

                                         symmetry     5    6    6    7    7    8    8

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  5  5     151  -.022598                        +-   +    +    +    +     -        +  
 z*  5  1     166   .012878                        +-   +    +    +         +    +     - 
 z*  5  1     190   .018933                        +-   +    +         +    +    +     - 
 z*  5  2     245  -.014754                        +-   +         +    +    +    +     - 
 z*  5  2     343   .017290                        +-        +    +    +    +    +     - 
 z*  5  1     487   .012442                        +    +-   +    +    +    +          - 
 z*  5  1     492  -.031666                        +    +-   +    +    +         +     - 
 z*  5  5     493   .011191                        +    +-   +    +    +         +     - 
 z*  7  4     494  -.010964                        +    +-   +    +    +         +    +  
 z*  5  5     526  -.011525                        +    +-   +         +    +     -   +  
 z*  5  1     767  -.013009                        +    +    +-   +    +    +          - 
 z*  5  5     782   .034667                        +    +    +-   +         +     -   +  
 z*  5  2     897  -.022659                        +    +    +    +-   +    +          - 
 z*  5  2     902  -.023646                        +    +    +    +-   +         +     - 
 z*  5  2     949  -.015123                        +    +    +    +    +-   +          - 
 z*  7  2     980   .957658                        +    +    +    +    +    +    +     - 
 z*  7  6     981   .124436                        +    +    +    +    +    +    +     - 
 z*  9  3     982   .077492                        +    +    +    +    +    +    +    +  
 z*  9  7     983  -.175427                        +    +    +    +    +    +    +    +  
 z*  5  2     994   .015002                        +    +    +    +         +    +-    - 
 z*  5  2    1017   .023519                        +    +    +         +    +-   +     - 
 z*  5  2    1022  -.022790                        +    +    +         +    +    +-    - 
 z*  5  5    1079  -.034733                        +    +         +    +-   +     -   +  
 z*  5  1    1097   .013037                        +    +         +    +    +    +-    - 
 z*  5  5    1228   .011689                        +         +    +-   +    +     -   +  
 z*  5  1    1274   .031604                        +         +    +    +    +-   +     - 
 z*  5  5    1275  -.011229                        +         +    +    +    +-   +     - 
 z*  7  4    1276   .010881                        +         +    +    +    +-   +    +  
 z*  5  1    1279   .012441                        +         +    +    +    +    +-    - 
 z*  5  2    1393   .017123                             +-   +    +    +    +    +     - 
 z*  5  2    1491   .014873                             +    +-   +    +    +    +     - 
 z*  5  1    1564   .018904                             +    +    +-   +    +    +     - 
 z*  5  1    1597  -.012808                             +    +    +    +-   +    +     - 
 z*  5  5    1615   .022612                             +    +    +    +     -   +-   +  
 x   7  4   39352   .020685    1(   1)   2(   1)   +    +    +    +    +               - 
 x   7  4   39521  -.023198    2(   1)   1(   2)   +    +    +    +         +          - 
 x   7  4   39524  -.015098    1(   3)   1(   4)   +    +    +    +         +          - 
 x   7  4   39689  -.010309    2(   1)   1(   2)   +    +    +    +              +     - 
 x   7  4   39692   .017985    1(   3)   1(   4)   +    +    +    +              +     - 
 x   7  4   40168   .022275    1(   1)   1(   2)   +    +    +         +    +          - 
 x   7  4   40169   .025617    2(   1)   1(   2)   +    +    +         +    +          - 
 x   7  4   40336   .034719    1(   1)   1(   2)   +    +    +         +         +     - 
 x   7  4   40696  -.020657    1(   1)   2(   1)   +    +    +              +    +     - 
 x   7  4   42329  -.014102    2(   1)   1(   3)   +    +         +    +    +          - 
 x   7  4   42332   .021857    1(   2)   1(   4)   +    +         +    +    +          - 
 x   7  4   42497   .017942    2(   1)   1(   3)   +    +         +    +         +     - 
 x   7  4   42500   .010371    1(   2)   1(   4)   +    +         +    +         +     - 
 x   7  4   42858  -.045972    2(   1)   1(   4)   +    +         +         +    +     - 
 x   7  4   43817   .020702    1(   1)   1(   4)   +    +              +    +    +     - 
 x   7  4   49048   .010430    1(   1)   1(   3)   +         +    +    +    +          - 
 x   7  4   49049  -.022831    2(   1)   1(   3)   +         +    +    +    +          - 
 x   7  4   49216  -.042607    1(   1)   1(   3)   +         +    +    +         +     - 
 x   7  4   49577   .020643    1(   1)   1(   4)   +         +    +         +    +     - 
 x   7  4   50536   .024566    1(   2)   1(   3)   +         +         +    +    +     - 
 x   7  4   53176  -.020669    1(   1)   2(   1)   +              +    +    +    +     - 
 x   7  4   68488  -.030019    1(   2)   1(   3)        +    +    +    +    +          - 
 x   7  4   68490   .015178    2(   1)   1(   4)        +    +    +    +    +          - 
 x   7  4   68657   .020454    1(   1)   1(   4)        +    +    +    +         +     - 
 x   7  4   69017   .019201    2(   1)   1(   3)        +    +    +         +    +     - 
 x   7  4   69020  -.017313    1(   2)   1(   4)        +    +    +         +    +     - 
 x   7  4   69976  -.016265    1(   1)   1(   3)        +    +         +    +    +     - 
 x   7  4   69980  -.025503    1(   2)   1(   4)        +    +         +    +    +     - 
 x   7  4   72617  -.018644    2(   1)   1(   2)        +         +    +    +    +     - 
 x   7  4   72620  -.020208    1(   3)   1(   4)        +         +    +    +    +     - 
 x   7  4   79672   .017350    1(   1)   1(   2)             +    +    +    +    +     - 
 x   7  4   79676   .023401    1(   3)   1(   4)             +    +    +    +    +     - 

 ci coefficient statistics:
           rq > 0.1                3
      0.1> rq > 0.01              63
     0.01> rq > 0.001            247
    0.001> rq > 0.0001          2195
   0.0001> rq > 0.00001         7975
  0.00001> rq > 0.000001       20900
 0.000001> rq                  85448
           all                116831


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 4) =       -93.3414567741

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7

                                          orbital    34   37   38   45   46   53   54

                                         symmetry     5    6    6    7    7    8    8

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  5  5     167  -.014554                        +-   +    +    +         +    +     - 
 z*  5  5     191  -.024023                        +-   +    +         +    +    +     - 
 z*  5  4     246  -.014062                        +-   +         +    +    +    +     - 
 z*  7  3     247  -.011901                        +-   +         +    +    +    +    +  
 z*  5  4     344   .018013                        +-        +    +    +    +    +     - 
 z*  7  3     345   .015072                        +-        +    +    +    +    +    +  
 z*  5  5     488  -.014244                        +    +-   +    +    +    +          - 
 z*  5  5     493   .039045                        +    +-   +    +    +         +     - 
 z*  5  5     768   .017286                        +    +    +-   +    +    +          - 
 z*  5  4     898  -.021884                        +    +    +    +-   +    +          - 
 z*  7  3     899  -.018565                        +    +    +    +-   +    +         +  
 z*  5  4     903  -.024042                        +    +    +    +-   +         +     - 
 z*  7  3     904  -.020208                        +    +    +    +-   +         +    +  
 z*  5  4     950  -.015938                        +    +    +    +    +-   +          - 
 z*  7  3     951  -.013398                        +    +    +    +    +-   +         +  
 z*  7  2     980  -.130089                        +    +    +    +    +    +    +     - 
 z*  7  6     981   .708627                        +    +    +    +    +    +    +     - 
 z*  9  3     982   .665457                        +    +    +    +    +    +    +    +  
 z*  9  7     983   .086403                        +    +    +    +    +    +    +    +  
 z*  5  4     995   .014273                        +    +    +    +         +    +-    - 
 z*  7  3     996   .012117                        +    +    +    +         +    +-   +  
 z*  5  4    1018   .022982                        +    +    +         +    +-   +     - 
 z*  7  3    1019   .019378                        +    +    +         +    +-   +    +  
 z*  5  4    1023  -.023209                        +    +    +         +    +    +-    - 
 z*  7  3    1024  -.019610                        +    +    +         +    +    +-   +  
 z*  5  5    1098  -.014850                        +    +         +    +    +    +-    - 
 z*  5  5    1275  -.038367                        +         +    +    +    +-   +     - 
 z*  5  5    1280  -.016131                        +         +    +    +    +    +-    - 
 z*  5  4    1394   .016517                             +-   +    +    +    +    +     - 
 z*  7  3    1395   .013912                             +-   +    +    +    +    +    +  
 z*  5  4    1492   .015763                             +    +-   +    +    +    +     - 
 z*  7  3    1493   .013244                             +    +-   +    +    +    +    +  
 z*  5  5    1565  -.022285                             +    +    +-   +    +    +     - 
 z*  5  5    1598   .017064                             +    +    +    +-   +    +     - 
 x   9  1   39376   .015308    1(   1)   2(   1)   +    +    +    +    +              +  
 x   9  5   39400   .014360    1(   1)   2(   1)   +    +    +    +    +              +  
 x   9  1   39545  -.017179    2(   1)   1(   2)   +    +    +    +         +         +  
 x   9  1   39548  -.011183    1(   3)   1(   4)   +    +    +    +         +         +  
 x   9  5   39569  -.016118    2(   1)   1(   2)   +    +    +    +         +         +  
 x   9  5   39572  -.010490    1(   3)   1(   4)   +    +    +    +         +         +  
 x   9  1   39716   .013321    1(   3)   1(   4)   +    +    +    +              +    +  
 x   9  5   39740   .012491    1(   3)   1(   4)   +    +    +    +              +    +  
 x   9  1   40192   .016489    1(   1)   1(   2)   +    +    +         +    +         +  
 x   9  1   40193   .018957    2(   1)   1(   2)   +    +    +         +    +         +  
 x   9  5   40216   .015471    1(   1)   1(   2)   +    +    +         +    +         +  
 x   9  5   40217   .017796    2(   1)   1(   2)   +    +    +         +    +         +  
 x   9  1   40360   .025696    1(   1)   1(   2)   +    +    +         +         +    +  
 x   9  5   40384   .024118    1(   1)   1(   2)   +    +    +         +         +    +  
 x   9  1   40720  -.015290    1(   1)   2(   1)   +    +    +              +    +    +  
 x   9  5   40744  -.014347    1(   1)   2(   1)   +    +    +              +    +    +  
 x   9  1   42353  -.010444    2(   1)   1(   3)   +    +         +    +    +         +  
 x   9  1   42356   .016185    1(   2)   1(   4)   +    +         +    +    +         +  
 x   9  5   42380   .015184    1(   2)   1(   4)   +    +         +    +    +         +  
 x   9  1   42521   .013287    2(   1)   1(   3)   +    +         +    +         +    +  
 x   9  5   42545   .012460    2(   1)   1(   3)   +    +         +    +         +    +  
 x   9  1   42882  -.034030    2(   1)   1(   4)   +    +         +         +    +    +  
 x   9  5   42906  -.031950    2(   1)   1(   4)   +    +         +         +    +    +  
 x   9  1   43841   .015325    1(   1)   1(   4)   +    +              +    +    +    +  
 x   9  5   43865   .014379    1(   1)   1(   4)   +    +              +    +    +    +  
 x   9  1   49073  -.016897    2(   1)   1(   3)   +         +    +    +    +         +  
 x   9  5   49097  -.015856    2(   1)   1(   3)   +         +    +    +    +         +  
 x   9  1   49240  -.031534    1(   1)   1(   3)   +         +    +    +         +    +  
 x   9  5   49264  -.029602    1(   1)   1(   3)   +         +    +    +         +    +  
 x   9  1   49601   .015284    1(   1)   1(   4)   +         +    +         +    +    +  
 x   9  5   49625   .014337    1(   1)   1(   4)   +         +    +         +    +    +  
 x   9  1   50560   .018183    1(   2)   1(   3)   +         +         +    +    +    +  
 x   9  5   50584   .017052    1(   2)   1(   3)   +         +         +    +    +    +  
 x   9  1   53200  -.015303    1(   1)   2(   1)   +              +    +    +    +    +  
 x   9  5   53224  -.014354    1(   1)   2(   1)   +              +    +    +    +    +  
 x   9  1   68512  -.022211    1(   2)   1(   3)        +    +    +    +    +         +  
 x   9  1   68514   .011236    2(   1)   1(   4)        +    +    +    +    +         +  
 x   9  5   68536  -.020855    1(   2)   1(   3)        +    +    +    +    +         +  
 x   9  5   68538   .010527    2(   1)   1(   4)        +    +    +    +    +         +  
 x   9  1   68681   .015138    1(   1)   1(   4)        +    +    +    +         +    +  
 x   9  5   68705   .014200    1(   1)   1(   4)        +    +    +    +         +    +  
 x   9  1   69041   .014219    2(   1)   1(   3)        +    +    +         +    +    +  
 x   9  1   69044  -.012822    1(   2)   1(   4)        +    +    +         +    +    +  
 x   9  5   69065   .013339    2(   1)   1(   3)        +    +    +         +    +    +  
 x   9  5   69068  -.012027    1(   2)   1(   4)        +    +    +         +    +    +  
 x   9  1   70000  -.012043    1(   1)   1(   3)        +    +         +    +    +    +  
 x   9  1   70004  -.018872    1(   2)   1(   4)        +    +         +    +    +    +  
 x   9  5   70024  -.011290    1(   1)   1(   3)        +    +         +    +    +    +  
 x   9  5   70028  -.017715    1(   2)   1(   4)        +    +         +    +    +    +  
 x   9  1   72641  -.013809    2(   1)   1(   2)        +         +    +    +    +    +  
 x   9  1   72644  -.014963    1(   3)   1(   4)        +         +    +    +    +    +  
 x   9  5   72665  -.012953    2(   1)   1(   2)        +         +    +    +    +    +  
 x   9  5   72668  -.014041    1(   3)   1(   4)        +         +    +    +    +    +  
 x   9  1   79696   .012847    1(   1)   1(   2)             +    +    +    +    +    +  
 x   9  1   79700   .017319    1(   3)   1(   4)             +    +    +    +    +    +  
 x   9  5   79720   .012048    1(   1)   1(   2)             +    +    +    +    +    +  
 x   9  5   79724   .016255    1(   3)   1(   4)             +    +    +    +    +    +  

 ci coefficient statistics:
           rq > 0.1                3
      0.1> rq > 0.01              88
     0.01> rq > 0.001            217
    0.001> rq > 0.0001          1832
   0.0001> rq > 0.00001         7152
  0.00001> rq > 0.000001       16533
 0.000001> rq                  91006
           all                116831


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 5) =       -93.1685354405

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7

                                          orbital    34   37   38   45   46   53   54

                                         symmetry     5    6    6    7    7    8    8

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  3  2      69   .023656                        +-   +    +-   +         +          - 
 z*  5  3      70  -.015156                        +-   +    +-   +         +         +  
 z*  5  1     105  -.023467                        +-   +     -   +         +    +    +  
 z*  5  1     119  -.023646                        +-   +    +    +-   +              +  
 z*  5  5     151   .035664                        +-   +    +    +    +     -        +  
 z*  7  1     152  -.236105                        +-   +    +    +    +    +         +  
 z*  7  5     153   .067189                        +-   +    +    +    +    +         +  
 z*  7  1     157  -.013974                        +-   +    +    +    +         +    +  
 z*  7  5     158   .011611                        +-   +    +    +    +         +    +  
 z*  5  1     165   .011585                        +-   +    +    +         +     -   +  
 z*  5  5     167  -.032130                        +-   +    +    +         +    +     - 
 z*  7  4     168   .024495                        +-   +    +    +         +    +    +  
 z*  5  1     184   .023698                        +-   +    +         +    +-        +  
 z*  5  1     189  -.018871                        +-   +    +         +    +     -   +  
 z*  5  1     190  -.046707                        +-   +    +         +    +    +     - 
 z*  5  5     191   .027154                        +-   +    +         +    +    +     - 
 z*  7  4     192  -.033818                        +-   +    +         +    +    +    +  
 z*  3  2     205  -.011342                        +-   +         +-   +    +          - 
 z*  3  2     208  -.015563                        +-   +         +-   +         +     - 
 z*  5  3     209   .010366                        +-   +         +-   +         +    +  
 z*  3  2     227  -.034171                        +-   +         +    +-   +          - 
 z*  5  3     228   .022743                        +-   +         +    +-   +         +  
 z*  5  2     245   .027752                        +-   +         +    +    +    +     - 
 z*  5  4     246   .160415                        +-   +         +    +    +    +     - 
 z*  7  3     247  -.129671                        +-   +         +    +    +    +    +  
 z*  3  2     265   .014659                        +-   +              +    +-   +     - 
 z*  3  2     268  -.014356                        +-   +              +    +    +-    - 
 z*  5  3     269   .010424                        +-   +              +    +    +-   +  
 z*  5  1     287   .022427                        +-        +-   +         +    +    +  
 z*  3  2     303   .020462                        +-        +    +-   +    +          - 
 z*  5  3     304  -.014747                        +-        +    +-   +    +         +  
 z*  3  2     306   .011838                        +-        +    +-   +         +     - 
 z*  5  2     343  -.030593                        +-        +    +    +    +    +     - 
 z*  5  4     344  -.116612                        +-        +    +    +    +    +     - 
 z*  7  3     345   .097814                        +-        +    +    +    +    +    +  
 z*  3  2     352  -.011202                        +-        +    +         +    +-    - 
 z*  3  2     363  -.015906                        +-        +         +    +-   +     - 
 z*  5  3     364   .012401                        +-        +         +    +-   +    +  
 z*  5  1     375   .010671                        +-             +-   +    +    +    +  
 z*  5  1     383  -.021924                        +-             +    +-   +    +    +  
 z*  3  2     422  -.015412                        +    +-   +-             +    +     - 
 z*  5  3     423   .010496                        +    +-   +-             +    +    +  
 z*  5  1     433   .013610                        +    +-    -   +    +    +         +  
 z*  5  1     470   .013913                        +    +-   +     -   +         +    +  
 z*  5  1     481   .011361                        +    +-   +    +     -   +         +  
 z*  5  5     488  -.024112                        +    +-   +    +    +    +          - 
 z*  7  4     489   .018730                        +    +-   +    +    +    +         +  
 z*  5  1     492   .061509                        +    +-   +    +    +         +     - 
 z*  5  5     493  -.016205                        +    +-   +    +    +         +     - 
 z*  7  4     494   .027486                        +    +-   +    +    +         +    +  
 z*  7  1     503  -.043787                        +    +-   +    +         +    +    +  
 z*  5  5     526   .017185                        +    +-   +         +    +     -   +  
 z*  7  1     527  -.166602                        +    +-   +         +    +    +    +  
 z*  7  5     528   .051738                        +    +-   +         +    +    +    +  
 z*  5  1     536  -.011899                        +    +-   +              +    +-   +  
 z*  7  2     583  -.022003                        +    +-        +    +    +    +    +  
 z*  3  2     595  -.015206                        +    +-             +-   +    +     - 
 z*  5  3     596   .010525                        +    +-             +-   +    +    +  
 z*  5  1     615   .011413                        +     -   +-   +    +    +         +  
 z*  3  2     677   .013477                        +     -   +    +    +    +     -    - 
 z*  5  4     679   .013861                        +     -   +    +    +    +     -   +  
 z*  5  1     728   .014156                        +     -        +    +    +    +-   +  
 z*  5  1     738  -.034180                        +    +    +-   +-        +         +  
 z*  5  1     741  -.030982                        +    +    +-   +-             +    +  
 z*  5  1     761  -.014812                        +    +    +-   +     -   +         +  
 z*  5  1     767   .039528                        +    +    +-   +    +    +          - 
 z*  5  5     768  -.031298                        +    +    +-   +    +    +          - 
 z*  7  4     769   .033645                        +    +    +-   +    +    +         +  
 z*  5  5     782  -.063576                        +    +    +-   +         +     -   +  
 z*  7  1     783   .339779                        +    +    +-   +         +    +    +  
 z*  7  5     784  -.084778                        +    +    +-   +         +    +    +  
 z*  7  1     807  -.016577                        +    +    +-        +    +    +    +  
 z*  7  5     808   .010138                        +    +    +-        +    +    +    +  
 z*  5  1     813  -.034658                        +    +    +-             +-   +    +  
 z*  5  1     816   .029138                        +    +    +-             +    +-   +  
 z*  3  2     851   .028411                        +    +     -   +     -   +    +     - 
 z*  5  3     852  -.018626                        +    +     -   +     -   +    +    +  
 z*  3  2     857   .015411                        +    +     -   +    +     -   +     - 
 z*  5  3     858  -.010464                        +    +     -   +    +     -   +    +  
 z*  3  2     859  -.013661                        +    +     -   +    +    +     -    - 
 z*  5  4     861   .038073                        +    +     -   +    +    +     -   +  
 z*  7  2     863   .046421                        +    +     -   +    +    +    +    +  
 z*  3  2     887  -.010293                        +    +    +    +-   +-              - 
 z*  5  2     897   .038709                        +    +    +    +-   +    +          - 
 z*  5  4     898   .210975                        +    +    +    +-   +    +          - 
 z*  7  3     899  -.179518                        +    +    +    +-   +    +         +  
 z*  5  2     902   .040375                        +    +    +    +-   +         +     - 
 z*  5  4     903   .174733                        +    +    +    +-   +         +     - 
 z*  7  3     904  -.150492                        +    +    +    +-   +         +    +  
 z*  3  2     910  -.013616                        +    +    +    +-        +     -    - 
 z*  7  2     914  -.024799                        +    +    +    +-        +    +    +  
 z*  3  2     915  -.015447                        +    +    +    +-             +-    - 
 z*  5  3     916   .010124                        +    +    +    +-             +-   +  
 z*  3  2     926   .017498                        +    +    +     -    -   +    +     - 
 z*  5  3     927  -.012527                        +    +    +     -    -   +    +    +  
 z*  3  2     929  -.019705                        +    +    +     -   +    +-         - 
 z*  5  3     930   .013621                        +    +    +     -   +    +-        +  
 z*  3  2     932  -.019038                        +    +    +     -   +     -   +     - 
 z*  5  3     933   .014035                        +    +    +     -   +     -   +    +  
 z*  3  2     934   .018558                        +    +    +     -   +    +     -    - 
 z*  5  3     935  -.013435                        +    +    +     -   +    +     -   +  
 z*  5  4     936   .024067                        +    +    +     -   +    +     -   +  
 z*  7  2     938   .056042                        +    +    +     -   +    +    +    +  
 z*  3  2     939   .016233                        +    +    +     -   +         +-    - 
 z*  5  3     940  -.011541                        +    +    +     -   +         +-   +  
 z*  5  2     949   .028595                        +    +    +    +    +-   +          - 
 z*  5  4     950   .091224                        +    +    +    +    +-   +          - 
 z*  7  3     951  -.074091                        +    +    +    +    +-   +         +  
 z*  3  2     962  -.015638                        +    +    +    +     -   +     -    - 
 z*  5  3     963   .010222                        +    +    +    +     -   +     -   +  
 z*  5  4     964   .034357                        +    +    +    +     -   +     -   +  
 z*  7  2     966   .048263                        +    +    +    +     -   +    +    +  
 z*  7  2     971   .037734                        +    +    +    +    +    +-        +  
 z*  5  4     974   .030270                        +    +    +    +    +     -    -   +  
 z*  7  2     976   .035316                        +    +    +    +    +     -   +    +  
 z*  7  2     979  -.012623                        +    +    +    +    +    +     -   +  
 z*  7  2     980   .022069                        +    +    +    +    +    +    +     - 
 z*  7  6     981   .077625                        +    +    +    +    +    +    +     - 
 z*  9  3     982  -.070936                        +    +    +    +    +    +    +    +  
 z*  9  7     983   .010038                        +    +    +    +    +    +    +    +  
 z*  7  2     986  -.028196                        +    +    +    +    +         +-   +  
 z*  5  4     990  -.023657                        +    +    +    +         +-   +     - 
 z*  7  3     991   .016055                        +    +    +    +         +-   +    +  
 z*  5  2     994  -.028204                        +    +    +    +         +    +-    - 
 z*  5  4     995  -.159757                        +    +    +    +         +    +-    - 
 z*  7  3     996   .129163                        +    +    +    +         +    +-   +  
 z*  3  2     997  -.012844                        +    +    +         +-   +-         - 
 z*  7  2    1006   .015467                        +    +    +         +-   +    +    +  
 z*  5  2    1017  -.038624                        +    +    +         +    +-   +     - 
 z*  5  4    1018  -.216821                        +    +    +         +    +-   +     - 
 z*  7  3    1019   .185714                        +    +    +         +    +-   +    +  
 z*  5  2    1022   .040439                        +    +    +         +    +    +-    - 
 z*  5  4    1023   .157106                        +    +    +         +    +    +-    - 
 z*  7  3    1024  -.134291                        +    +    +         +    +    +-   +  
 z*  3  2    1025  -.016691                        +    +    +              +-   +-    - 
 z*  5  3    1026   .010721                        +    +    +              +-   +-   +  
 z*  5  1    1029  -.036784                        +    +         +-   +-   +         +  
 z*  5  1    1032  -.030738                        +    +         +-   +-        +    +  
 z*  7  1    1047   .054136                        +    +         +-   +    +    +    +  
 z*  7  5    1048  -.013879                        +    +         +-   +    +    +    +  
 z*  5  5    1079   .063597                        +    +         +    +-   +     -   +  
 z*  7  1    1080  -.338325                        +    +         +    +-   +    +    +  
 z*  7  5    1081   .084805                        +    +         +    +-   +    +    +  
 z*  5  1    1089  -.021440                        +    +         +     -   +    +-   +  
 z*  5  5    1093  -.010074                        +    +         +    +    +-   +     - 
 z*  5  5    1098  -.030052                        +    +         +    +    +    +-    - 
 z*  7  4    1099   .022183                        +    +         +    +    +    +-   +  
 z*  5  1    1104  -.034642                        +    +              +-   +-   +    +  
 z*  5  1    1107   .029544                        +    +              +-   +    +-   +  
 z*  7  2    1157   .012507                        +         +-   +    +    +    +    +  
 z*  5  1    1204  -.010615                        +          -   +    +    +    +-   +  
 z*  5  5    1228  -.017159                        +         +    +-   +    +     -   +  
 z*  7  1    1229   .064614                        +         +    +-   +    +    +    +  
 z*  7  5    1230  -.014837                        +         +    +-   +    +    +    +  
 z*  5  1    1249   .011991                        +         +     -   +    +-   +    +  
 z*  7  1    1262   .016462                        +         +    +    +-   +    +    +  
 z*  5  1    1274  -.052346                        +         +    +    +    +-   +     - 
 z*  7  4    1276  -.012100                        +         +    +    +    +-   +    +  
 z*  5  1    1279  -.035783                        +         +    +    +    +    +-    - 
 z*  5  5    1280   .023050                        +         +    +    +    +    +-    - 
 z*  7  4    1281  -.026674                        +         +    +    +    +    +-   +  
 z*  5  1    1337   .029541                             +-   +-   +         +    +    +  
 z*  3  2    1353   .014030                             +-   +    +-   +    +          - 
 z*  3  2    1356   .017970                             +-   +    +-   +         +     - 
 z*  5  3    1357  -.013543                             +-   +    +-   +         +    +  
 z*  3  2    1375   .010134                             +-   +    +    +-   +          - 
 z*  5  2    1393  -.029830                             +-   +    +    +    +    +     - 
 z*  5  4    1394  -.177439                             +-   +    +    +    +    +     - 
 z*  7  3    1395   .147500                             +-   +    +    +    +    +    +  
 z*  3  2    1413  -.013766                             +-   +         +    +-   +     - 
 z*  3  2    1416   .025259                             +-   +         +    +    +-    - 
 z*  5  3    1417  -.017928                             +-   +         +    +    +-   +  
 z*  5  1    1433  -.027630                             +-        +    +-   +    +    +  
 z*  3  2    1451   .012081                             +    +-   +-   +    +          - 
 z*  5  2    1491  -.028222                             +    +-   +    +    +    +     - 
 z*  5  4    1492  -.089308                             +    +-   +    +    +    +     - 
 z*  7  3    1493   .071746                             +    +-   +    +    +    +    +  
 z*  3  2    1497  -.011387                             +    +-   +         +-   +     - 
 z*  3  2    1500  -.032188                             +    +-   +         +    +-    - 
 z*  5  3    1501   .021180                             +    +-   +         +    +-   +  
 z*  3  2    1511  -.012680                             +    +-        +    +-   +     - 
 z*  5  1    1531  -.016558                             +     -   +    +-   +    +    +  
 z*  5  1    1561   .011834                             +    +    +-   +     -   +    +  
 z*  5  1    1563   .023198                             +    +    +-   +    +     -   +  
 z*  5  1    1564  -.019615                             +    +    +-   +    +    +     - 
 z*  5  5    1565  -.014778                             +    +    +-   +    +    +     - 
 z*  5  1    1568   .019810                             +    +    +-   +         +-   +  
 z*  5  1    1597   .039775                             +    +    +    +-   +    +     - 
 z*  5  5    1598  -.031745                             +    +    +    +-   +    +     - 
 z*  7  4    1599   .033844                             +    +    +    +-   +    +    +  
 z*  7  1    1611   .117366                             +    +    +    +    +-   +    +  
 z*  7  5    1612  -.034546                             +    +    +    +    +-   +    +  
 z*  5  5    1615  -.035736                             +    +    +    +     -   +-   +  
 z*  7  1    1616   .208549                             +    +    +    +    +    +-   +  
 z*  7  5    1617  -.056229                             +    +    +    +    +    +-   +  
 z*  5  1    1628  -.031162                             +    +         +    +-   +-   +  
 z*  3  2    1651   .021787                             +         +    +-   +    +-    - 
 z*  5  3    1652  -.013744                             +         +    +-   +    +-   +  
 x   7  3   13458   .011016    2(   1)   1(   4)   +-   +         +         +         +  
 x   7  3   15640   .011995    1(   1)   1(   3)   +-        +    +    +              +  
 x   7  3   34672  -.011173    1(   1)   1(   3)   +     -   +         +         +    +  
 x   7  3   37456  -.012924    1(   1)   1(   2)   +    +    +-                  +    +  
 x   7  1   41046  -.010103    2(   1)   1(   4)   +    +         +-        +         +  
 x   7  3   43216  -.012869    1(   1)   1(   2)   +    +              +-        +    +  
 x   7  1   44046  -.010362    2(   1)   1(   4)   +    +                   +-   +    +  
 x   7  3   44800   .016035    1(   1)   1(   3)   +         +-   +              +    +  
 x   7  1   47644  -.012362    1(   1)   1(   3)   +         +    +-   +              +  
 x   7  5   47668   .010538    1(   1)   1(   3)   +         +    +-   +              +  
 x   7  1   50404  -.012438    1(   1)   1(   3)   +         +         +     -   +    +  
 x   7  5   50428   .010668    1(   1)   1(   3)   +         +         +     -   +    +  
 x   7  3   52576  -.015965    1(   1)   1(   3)   +              +    +-        +    +  
 x   7  1   60916   .010898    1(   1)   1(   3)         -   +    +    +         +    +  
 x   7  3   64120   .011098    1(   2)   1(   3)        +    +-   +         +         +  
 x   7  3   64916  -.010070    1(   2)   1(   4)        +    +-             +    +    +  
 x   7  3   71896  -.011018    1(   2)   1(   3)        +         +    +-   +         +  
 x   7  3   79792  -.010443    1(   1)   1(   3)             +    +    +         +-   +  
 w   7  1   92122  -.012229    2(   1)   2(   1)   +    +    +    +         +         +  
 w   7  3   92143  -.010114    2(   1)   1(   3)   +    +    +    +         +         +  
 w   7  3   92146  -.014394    1(   2)   1(   4)   +    +    +    +         +         +  
 w   7  5   92156   .010003    2(   1)   2(   1)   +    +    +    +         +         +  
 w   7  1   92742   .018018    1(   2)   1(   2)   +    +    +         +    +         +  
 w   7  5   92776  -.015400    1(   2)   1(   2)   +    +    +         +    +         +  
 w   7  1   92902   .017496    1(   1)   1(   1)   +    +    +         +         +    +  
 w   7  1   92912   .010548    1(   2)   1(   2)   +    +    +         +         +    +  
 w   7  5   92936  -.015049    1(   1)   1(   1)   +    +    +         +         +    +  
 w   7  1   93253  -.010882    2(   1)   1(   2)   +    +    +              +    +    +  
 w   7  3   93265   .013344    1(   1)   1(   4)   +    +    +              +    +    +  
 w   7  3   94591   .015026    2(   1)   1(   2)   +    +         +    +    +         +  
 w   7  3   94594  -.010909    1(   3)   1(   4)   +    +         +    +    +         +  
 w   7  3   95092  -.025502    2(   1)   2(   1)   +    +         +         +    +    +  
 w   7  3   95102  -.025843    1(   4)   1(   4)   +    +         +         +    +    +  
 w   7  1   95898   .011005    1(   2)   1(   4)   +    +              +    +    +    +  
 w   7  3   95907   .013145    1(   1)   2(   1)   +    +              +    +    +    +  
 w   7  3   99851   .011949    1(   1)   2(   1)   +         +    +         +    +    +  
 w   7  3  102547   .012125    1(   1)   1(   4)   +              +    +    +    +    +  
 w   7  3  108530   .013964    1(   2)   1(   2)        +    +    +    +    +         +  
 w   7  3  108531   .014560    1(   3)   1(   3)        +    +    +    +    +         +  
 w   7  3  109041  -.011086    2(   1)   1(   2)        +    +    +         +    +    +  
 w   7  3  109044   .014800    1(   3)   1(   4)        +    +    +         +    +    +  
 w   7  1  109834  -.012170    1(   2)   1(   3)        +    +         +    +    +    +  
 w   7  3  109856   .010769    1(   1)   1(   2)        +    +         +    +    +    +  
 w   7  5  109868   .010292    1(   2)   1(   3)        +    +         +    +    +    +  
 w   7  1  111716  -.011129    1(   4)   1(   4)        +         +    +    +    +    +  
 w   7  3  111727  -.013941    2(   1)   1(   3)        +         +    +    +    +    +  
 w   7  3  111730  -.010386    1(   2)   1(   4)        +         +    +    +    +    +  
 w   7  1  115047   .012343    1(   3)   1(   3)             +    +    +    +    +    +  
 w   7  5  115081  -.010273    1(   3)   1(   3)             +    +    +    +    +    +  

 ci coefficient statistics:
           rq > 0.1               21
      0.1> rq > 0.01             226
     0.01> rq > 0.001           2680
    0.001> rq > 0.0001         14169
   0.0001> rq > 0.00001        33949
  0.00001> rq > 0.000001       37074
 0.000001> rq                  28712
           all                116831


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 6) =       -93.1685349267

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7

                                          orbital    34   37   38   45   46   53   54

                                         symmetry     5    6    6    7    7    8    8

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  3  2      16   .010557                        +-   +-   +    +              +     - 
 z*  5  3      17   .010196                        +-   +-   +    +              +    +  
 z*  3  2      25   .020344                        +-   +-   +         +         +     - 
 z*  5  3      26   .017320                        +-   +-   +         +         +    +  
 z*  3  3      44  -.015576                        +-   +-        +    +         +     - 
 z*  3  3      97  -.013122                        +-   +     -   +    +    +          - 
 z*  3  3     100   .023374                        +-   +     -   +    +         +     - 
 z*  3  3     125   .010829                        +-   +    +    +-             +     - 
 z*  3  3     131   .016740                        +-   +    +     -   +    +          - 
 z*  3  3     145   .015553                        +-   +    +    +     -   +          - 
 z*  3  3     148   .015199                        +-   +    +    +     -        +     - 
 z*  5  5     151   .024298                        +-   +    +    +    +     -        +  
 z*  7  1     152  -.012676                        +-   +    +    +    +    +         +  
 z*  7  5     153  -.025008                        +-   +    +    +    +    +         +  
 z*  3  3     155  -.010815                        +-   +    +    +    +          -    - 
 z*  5  5     156   .021362                        +-   +    +    +    +          -   +  
 z*  7  1     157   .040011                        +-   +    +    +    +         +    +  
 z*  7  5     158   .058356                        +-   +    +    +    +         +    +  
 z*  5  5     167  -.080387                        +-   +    +    +         +    +     - 
 z*  7  4     168  -.053104                        +-   +    +    +         +    +    +  
 z*  3  3     173   .012883                        +-   +    +         +-   +          - 
 z*  5  1     190  -.013656                        +-   +    +         +    +    +     - 
 z*  5  5     191  -.218042                        +-   +    +         +    +    +     - 
 z*  7  4     192  -.100269                        +-   +    +         +    +    +    +  
 z*  3  3     200  -.022348                        +-   +    +              +    +-    - 
 z*  5  2     245   .010415                        +-   +         +    +    +    +     - 
 z*  5  4     246  -.072201                        +-   +         +    +    +    +     - 
 z*  7  3     247  -.075745                        +-   +         +    +    +    +    +  
 z*  3  2     268   .017769                        +-   +              +    +    +-    - 
 z*  5  3     269   .016160                        +-   +              +    +    +-   +  
 z*  3  3     279   .010610                        +-        +-   +    +    +          - 
 z*  3  2     303  -.013898                        +-        +    +-   +    +          - 
 z*  3  2     306  -.012798                        +-        +    +-   +         +     - 
 z*  5  2     343  -.021866                        +-        +    +    +    +    +     - 
 z*  5  4     344   .163389                        +-        +    +    +    +    +     - 
 z*  7  3     345   .152964                        +-        +    +    +    +    +    +  
 z*  3  2     349   .010375                        +-        +    +         +-   +     - 
 z*  5  3     350   .010182                        +-        +    +         +-   +    +  
 z*  3  2     352   .014450                        +-        +    +         +    +-    - 
 z*  5  3     353   .011480                        +-        +    +         +    +-   +  
 z*  3  2     363   .035134                        +-        +         +    +-   +     - 
 z*  5  3     364   .028395                        +-        +         +    +-   +    +  
 z*  3  3     389  -.015130                        +-             +    +    +-   +     - 
 z*  3  3     392  -.017723                        +-             +    +    +    +-    - 
 z*  3  2     402  -.015734                        +    +-   +-   +    +               - 
 z*  5  3     403  -.012498                        +    +-   +-   +    +              +  
 z*  3  3     449   .011972                        +    +-    -        +    +    +     - 
 z*  3  3     455  -.036872                        +    +-   +    +-   +               - 
 z*  3  3     478  -.021672                        +    +-   +    +    +-              - 
 z*  5  1     487  -.010483                        +    +-   +    +    +    +          - 
 z*  5  5     488  -.087051                        +    +-   +    +    +    +          - 
 z*  7  4     489  -.049068                        +    +-   +    +    +    +         +  
 z*  5  1     492   .026504                        +    +-   +    +    +         +     - 
 z*  5  5     493   .315447                        +    +-   +    +    +         +     - 
 z*  7  4     494   .149178                        +    +-   +    +    +         +    +  
 z*  7  1     503   .010800                        +    +-   +    +         +    +    +  
 z*  7  5     504   .017011                        +    +-   +    +         +    +    +  
 z*  3  3     506  -.015697                        +    +-   +    +              +-    - 
 z*  3  3     523  -.025759                        +    +-   +         +     -   +     - 
 z*  3  3     525  -.020639                        +    +-   +         +    +     -    - 
 z*  5  5     526   .026751                        +    +-   +         +    +     -   +  
 z*  7  1     527   .014148                        +    +-   +         +    +    +    +  
 z*  7  5     528   .017463                        +    +-   +         +    +    +    +  
 z*  3  3     530   .030567                        +    +-   +         +         +-    - 
 z*  7  2     583  -.020083                        +    +-        +    +    +    +    +  
 z*  3  2     584  -.010545                        +    +-        +    +         +-    - 
 z*  5  3     585  -.010993                        +    +-        +    +         +-   +  
 z*  3  3     631   .010246                        +     -   +-        +    +    +     - 
 z*  3  2     655   .010500                        +     -   +     -   +    +    +     - 
 z*  3  2     675  -.026391                        +     -   +    +    +     -   +     - 
 z*  5  3     676  -.022979                        +     -   +    +    +     -   +    +  
 z*  5  4     679   .017640                        +     -   +    +    +    +     -   +  
 z*  7  2     681  -.022233                        +     -   +    +    +    +    +    +  
 z*  3  2     682  -.012562                        +     -   +    +    +         +-    - 
 z*  5  3     683  -.011018                        +     -   +    +    +         +-   +  
 z*  3  3     735   .017058                        +    +    +-   +-   +               - 
 z*  5  1     767   .011720                        +    +    +-   +    +    +          - 
 z*  5  5     768   .180115                        +    +    +-   +    +    +          - 
 z*  7  4     769   .071462                        +    +    +-   +    +    +         +  
 z*  5  5     773  -.025386                        +    +    +-   +    +         +     - 
 z*  3  3     781  -.010957                        +    +    +-   +         +     -    - 
 z*  5  5     782  -.031872                        +    +    +-   +         +     -   +  
 z*  7  1     783   .031338                        +    +    +-   +         +    +    +  
 z*  7  5     784   .055264                        +    +    +-   +         +    +    +  
 z*  3  3     800  -.014639                        +    +    +-        +    +-         - 
 z*  3  3     805   .011430                        +    +    +-        +    +     -    - 
 z*  5  5     806   .013636                        +    +    +-        +    +     -   +  
 z*  7  1     807   .022811                        +    +    +-        +    +    +    +  
 z*  7  5     808   .032163                        +    +    +-        +    +    +    +  
 z*  3  2     857  -.012601                        +    +     -   +    +     -   +     - 
 z*  5  3     860  -.010014                        +    +     -   +    +    +     -   +  
 z*  5  4     861   .019233                        +    +     -   +    +    +     -   +  
 z*  5  3     862   .011616                        +    +     -   +    +    +    +     - 
 z*  3  2     887   .015604                        +    +    +    +-   +-              - 
 z*  5  3     888   .010534                        +    +    +    +-   +-             +  
 z*  5  2     897   .016656                        +    +    +    +-   +    +          - 
 z*  5  4     898  -.134086                        +    +    +    +-   +    +          - 
 z*  7  3     899  -.140251                        +    +    +    +-   +    +         +  
 z*  5  2     902   .025277                        +    +    +    +-   +         +     - 
 z*  5  4     903  -.195082                        +    +    +    +-   +         +     - 
 z*  7  3     904  -.190605                        +    +    +    +-   +         +    +  
 z*  7  2     914  -.024192                        +    +    +    +-        +    +    +  
 z*  3  2     915   .010468                        +    +    +    +-             +-    - 
 z*  3  2     929   .011018                        +    +    +     -   +    +-         - 
 z*  3  2     932   .030293                        +    +    +     -   +     -   +     - 
 z*  5  3     933   .025488                        +    +    +     -   +     -   +    +  
 z*  3  2     934  -.016319                        +    +    +     -   +    +     -    - 
 z*  5  3     935  -.013077                        +    +    +     -   +    +     -   +  
 z*  5  4     936  -.013367                        +    +    +     -   +    +     -   +  
 z*  7  2     938   .052635                        +    +    +     -   +    +    +    +  
 z*  3  2     939  -.014853                        +    +    +     -   +         +-    - 
 z*  5  3     940  -.011188                        +    +    +     -   +         +-   +  
 z*  5  2     949   .015867                        +    +    +    +    +-   +          - 
 z*  5  4     950  -.153277                        +    +    +    +    +-   +          - 
 z*  7  3     951  -.145742                        +    +    +    +    +-   +         +  
 z*  5  4     955   .021816                        +    +    +    +    +-        +     - 
 z*  7  3     956   .014874                        +    +    +    +    +-        +    +  
 z*  5  4     964   .011613                        +    +    +    +     -   +     -   +  
 z*  7  2     966   .011543                        +    +    +    +     -   +    +    +  
 z*  3  2     967  -.011949                        +    +    +    +     -        +-    - 
 z*  5  4     969  -.016737                        +    +    +    +     -        +-   +  
 z*  7  2     971   .041419                        +    +    +    +    +    +-        +  
 z*  5  4     974   .017412                        +    +    +    +    +     -    -   +  
 z*  7  2     979  -.022265                        +    +    +    +    +    +     -   +  
 z*  7  2     980   .011113                        +    +    +    +    +    +    +     - 
 z*  7  6     981  -.068572                        +    +    +    +    +    +    +     - 
 z*  9  3     982  -.076102                        +    +    +    +    +    +    +    +  
 z*  9  7     983  -.032132                        +    +    +    +    +    +    +    +  
 z*  9  8     984   .020942                        +    +    +    +    +    +    +    +  
 z*  7  2     986  -.051835                        +    +    +    +    +         +-   +  
 z*  5  2     994  -.010020                        +    +    +    +         +    +-    - 
 z*  5  4     995   .074829                        +    +    +    +         +    +-    - 
 z*  7  3     996   .078653                        +    +    +    +         +    +-   +  
 z*  3  2     997   .013216                        +    +    +         +-   +-         - 
 z*  3  2    1002  -.011136                        +    +    +         +-   +     -    - 
 z*  5  4    1004  -.013430                        +    +    +         +-   +     -   +  
 z*  7  2    1006   .034709                        +    +    +         +-   +    +    +  
 z*  5  2    1017  -.021119                        +    +    +         +    +-   +     - 
 z*  5  4    1018   .148171                        +    +    +         +    +-   +     - 
 z*  7  3    1019   .150419                        +    +    +         +    +-   +    +  
 z*  5  2    1022   .021450                        +    +    +         +    +    +-    - 
 z*  5  4    1023  -.195381                        +    +    +         +    +    +-    - 
 z*  7  3    1024  -.193685                        +    +    +         +    +    +-   +  
 z*  5  5    1046  -.012173                        +    +         +-   +    +     -   +  
 z*  7  1    1047  -.012711                        +    +         +-   +    +    +    +  
 z*  7  5    1048  -.021178                        +    +         +-   +    +    +    +  
 z*  3  3    1050  -.011405                        +    +         +-   +         +-    - 
 z*  3  3    1078  -.012088                        +    +         +    +-   +     -    - 
 z*  5  5    1079   .032066                        +    +         +    +-   +     -   +  
 z*  7  1    1080  -.032474                        +    +         +    +-   +    +    +  
 z*  7  5    1081  -.057889                        +    +         +    +-   +    +    +  
 z*  5  5    1098  -.083901                        +    +         +    +    +    +-    - 
 z*  7  4    1099  -.056036                        +    +         +    +    +    +-   +  
 z*  3  2    1148  -.016029                        +         +-   +    +    +-         - 
 z*  5  3    1149  -.013172                        +         +-   +    +    +-        +  
 z*  5  4    1155  -.012690                        +         +-   +    +    +     -   +  
 z*  7  2    1157   .030523                        +         +-   +    +    +    +    +  
 z*  3  3    1187   .012521                        +          -   +-   +    +    +     - 
 z*  3  3    1222  -.023107                        +         +    +-   +    +-         - 
 z*  3  3    1225  -.027319                        +         +    +-   +     -   +     - 
 z*  3  3    1227  -.023269                        +         +    +-   +    +     -    - 
 z*  7  1    1229   .034892                        +         +    +-   +    +    +    +  
 z*  7  5    1230   .056076                        +         +    +-   +    +    +    +  
 z*  3  3    1232  -.010280                        +         +    +-   +         +-    - 
 z*  3  3    1255  -.022185                        +         +    +    +-   +-         - 
 z*  5  5    1261  -.012023                        +         +    +    +-   +     -   +  
 z*  7  1    1262  -.020280                        +         +    +    +-   +    +    +  
 z*  7  5    1263  -.025917                        +         +    +    +-   +    +    +  
 z*  5  1    1274  -.025330                        +         +    +    +    +-   +     - 
 z*  5  5    1275  -.291574                        +         +    +    +    +-   +     - 
 z*  7  4    1276  -.145494                        +         +    +    +    +-   +    +  
 z*  5  1    1279  -.014737                        +         +    +    +    +    +-    - 
 z*  5  5    1280  -.160423                        +         +    +    +    +    +-    - 
 z*  7  4    1281  -.059884                        +         +    +    +    +    +-   +  
 z*  3  3    1283  -.013805                        +         +    +         +-   +-    - 
 z*  3  3    1292   .043979                        +         +         +    +-   +-    - 
 z*  3  3    1329   .019567                             +-   +-   +    +    +          - 
 z*  3  3    1332  -.024105                             +-   +-   +    +         +     - 
 z*  3  2    1356  -.029480                             +-   +    +-   +         +     - 
 z*  5  3    1357  -.026066                             +-   +    +-   +         +    +  
 z*  3  2    1375  -.016129                             +-   +    +    +-   +          - 
 z*  5  3    1376  -.012117                             +-   +    +    +-   +         +  
 z*  3  2    1378   .018084                             +-   +    +    +-        +     - 
 z*  5  3    1379   .013014                             +-   +    +    +-        +    +  
 z*  5  2    1393  -.015558                             +-   +    +    +    +    +     - 
 z*  5  4    1394   .094467                             +-   +    +    +    +    +     - 
 z*  7  3    1395   .095245                             +-   +    +    +    +    +    +  
 z*  3  2    1416  -.012209                             +-   +         +    +    +-    - 
 z*  3  2    1451  -.022176                             +    +-   +-   +    +          - 
 z*  5  3    1452  -.018074                             +    +-   +-   +    +         +  
 z*  3  2    1454  -.012380                             +    +-   +-   +         +     - 
 z*  5  2    1491  -.017165                             +    +-   +    +    +    +     - 
 z*  5  4    1492   .153701                             +    +-   +    +    +    +     - 
 z*  7  3    1493   .144515                             +    +-   +    +    +    +    +  
 z*  3  2    1511   .012679                             +    +-        +    +-   +     - 
 z*  3  2    1514  -.012125                             +    +-        +    +    +-    - 
 z*  3  3    1537  -.018196                             +     -   +    +    +-   +     - 
 z*  3  3    1540  -.014698                             +     -   +    +    +    +-    - 
 z*  3  3    1547   .027581                             +    +    +-   +-   +          - 
 z*  3  3    1550   .013382                             +    +    +-   +-        +     - 
 z*  5  1    1564  -.011079                             +    +    +-   +    +    +     - 
 z*  5  5    1565  -.151939                             +    +    +-   +    +    +     - 
 z*  7  4    1566  -.089174                             +    +    +-   +    +    +    +  
 z*  3  3    1588   .016082                             +    +     -   +    +    +-    - 
 z*  5  1    1597   .012469                             +    +    +    +-   +    +     - 
 z*  5  5    1598   .180161                             +    +    +    +-   +    +     - 
 z*  7  4    1599   .068681                             +    +    +    +-   +    +    +  
 z*  3  3    1604  -.011838                             +    +    +     -   +-   +     - 
 z*  3  3    1607   .014617                             +    +    +     -   +    +-    - 
 z*  5  5    1610  -.025166                             +    +    +    +    +-    -   +  
 z*  7  1    1611  -.024437                             +    +    +    +    +-   +    +  
 z*  7  5    1612  -.036916                             +    +    +    +    +-   +    +  
 z*  5  5    1615  -.014014                             +    +    +    +     -   +-   +  
 z*  7  1    1616   .025916                             +    +    +    +    +    +-   +  
 z*  7  5    1617   .047884                             +    +    +    +    +    +-   +  
 z*  3  3    1622   .014176                             +    +         +-   +-   +     - 
 z*  3  3    1625  -.013169                             +    +         +-   +    +-    - 
 z*  5  3    1652   .010710                             +         +    +-   +    +-   +  
 z*  3  3    1677  -.022415                                  +-   +    +    +-   +     - 
 z*  3  2    1694  -.014944                                  +    +-   +    +-   +     - 
 z*  5  3    1695  -.014603                                  +    +-   +    +-   +    +  
 z*  3  2    1697  -.010549                                  +    +-   +    +    +-    - 
 z*  5  3    1698  -.010547                                  +    +-   +    +    +-   +  
 z*  3  2    1704   .016534                                  +    +    +-   +-   +     - 
 z*  5  3    1705   .012430                                  +    +    +-   +-   +    +  
 x   7  2   14244  -.011828    2(   1)   1(   4)   +-   +                   +    +    +  
 x   7  2   31066  -.011156    1(   1)   1(   2)   +    +-   +         +              +  
 x   7  2   31932  -.016058    2(   1)   1(   4)   +    +-        +              +    +  
 x   7  2   34666   .010295    1(   2)   1(   3)   +     -   +         +         +    +  
 x   7  2   37956  -.012761    2(   1)   1(   4)   +    +     -   +         +         +  
 x   7  1   41166   .010071    2(   1)   1(   4)   +    +         +-             +    +  
 x   7  1   42078   .010501    2(   1)   1(   4)   +    +         +     -   +         +  
 x   7  1   44166  -.010543    2(   1)   1(   4)   +    +                   +    +-   +  
 x   7  5   44190  -.010444    2(   1)   1(   4)   +    +                   +    +-   +  
 x   7  2   50410  -.012005    1(   1)   1(   2)   +         +         +     -   +    +  
 x   7  2   53412   .014557    2(   1)   1(   4)   +              +         +-   +    +  
 x   7  2   55594   .010014    1(   2)   1(   3)        +-   +    +    +              +  
 x   7  1   66198  -.010862    2(   1)   1(   4)        +     -   +         +    +    +  
 x   7  5   66222  -.010138    2(   1)   1(   4)        +     -   +         +    +    +  
 x   7  2   72252  -.012559    2(   1)   1(   4)        +         +     -   +    +    +  
 w   7  2   91967  -.010914    2(   1)   1(   3)   +    +    +    +    +              +  
 w   7  1   92122   .010992    2(   1)   2(   1)   +    +    +    +         +         +  
 w   7  5   92156   .010883    2(   1)   2(   1)   +    +    +    +         +         +  
 w   7  2   92307   .011150    1(   1)   1(   4)   +    +    +    +              +    +  
 w   7  1   92742  -.015072    1(   2)   1(   2)   +    +    +         +    +         +  
 w   7  5   92776  -.015363    1(   2)   1(   2)   +    +    +         +    +         +  
 w   7  1   92902  -.015181    1(   1)   1(   1)   +    +    +         +         +    +  
 w   7  1   92912  -.011778    1(   2)   1(   2)   +    +    +         +         +    +  
 w   7  2   92918  -.015365    1(   2)   1(   3)   +    +    +         +         +    +  
 w   7  5   92936  -.015104    1(   1)   1(   1)   +    +    +         +         +    +  
 w   7  5   92946  -.011556    1(   2)   1(   2)   +    +    +         +         +    +  
 w   7  2   94576  -.011265    2(   1)   2(   1)   +    +         +    +    +         +  
 w   7  2   94745  -.011326    1(   1)   2(   1)   +    +         +    +         +    +  
 w   7  2   99335   .011293    1(   1)   2(   1)   +         +    +    +    +         +  
 w   7  2   99345  -.012406    1(   3)   1(   3)   +         +    +    +    +         +  
 w   7  2   99504   .023360    1(   1)   1(   1)   +         +    +    +         +    +  
 w   7  2   99515   .022680    1(   3)   1(   3)   +         +    +    +         +    +  
 w   7  6   99538   .011477    1(   1)   1(   1)   +         +    +    +         +    +  
 w   7  6   99549   .010569    1(   3)   1(   3)   +         +    +    +         +    +  
 w   7  2   99848  -.012361    1(   3)   1(   4)   +         +    +         +    +    +  
 w   7  1  100654   .010649    1(   1)   1(   3)   +         +         +    +    +    +  
 w   7  2  100660  -.015167    1(   1)   1(   2)   +         +         +    +    +    +  
 w   7  5  100688   .010398    1(   1)   1(   3)   +         +         +    +    +    +  
 w   7  2  102541   .012664    2(   1)   1(   3)   +              +    +    +    +    +  
 w   7  2  108515  -.010973    2(   1)   1(   2)        +    +    +    +    +         +  
 w   7  2  108684  -.010295    1(   1)   1(   2)        +    +    +    +         +    +  
 w   7  2  108688  -.012155    1(   3)   1(   4)        +    +    +    +         +    +  
 w   7  2  109036   .012324    1(   4)   1(   4)        +    +    +         +    +    +  
 w   7  2  109850   .014068    1(   2)   1(   2)        +    +         +    +    +    +  
 w   7  2  109852   .010528    1(   4)   1(   4)        +    +         +    +    +    +  
 w   7  1  111716   .010118    1(   4)   1(   4)        +         +    +    +    +    +  
 w   7  1  115047  -.010452    1(   3)   1(   3)             +    +    +    +    +    +  
 w   7  2  115052  -.010404    1(   2)   1(   3)             +    +    +    +    +    +  
 w   7  2  115053   .012159    1(   1)   1(   4)             +    +    +    +    +    +  
 w   7  5  115081  -.010058    1(   3)   1(   3)             +    +    +    +    +    +  

 ci coefficient statistics:
           rq > 0.1               24
      0.1> rq > 0.01             251
     0.01> rq > 0.001           2728
    0.001> rq > 0.0001         14012
   0.0001> rq > 0.00001        33390
  0.00001> rq > 0.000001       36983
 0.000001> rq                  29443
           all                116831


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 7) =       -93.1685318235

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7

                                          orbital    34   37   38   45   46   53   54

                                         symmetry     5    6    6    7    7    8    8

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  3  2      10   .015521                        +-   +-   +    +     -             +  
 z*  5  2      11   .014443                        +-   +-   +    +    +              +  
 z*  5  3      17   .013377                        +-   +-   +    +              +    +  
 z*  5  3      23  -.011428                        +-   +-   +         +    +         +  
 z*  3  2      25  -.014175                        +-   +-   +         +         +     - 
 z*  5  3      26   .017014                        +-   +-   +         +         +    +  
 z*  3  1      43  -.010414                        +-   +-        +    +         +     - 
 z*  3  2      69  -.016301                        +-   +    +-   +         +          - 
 z*  5  3      70   .020989                        +-   +    +-   +         +         +  
 z*  3  2      86  -.015934                        +-   +    +-             +     -   +  
 z*  5  2      87  -.015227                        +-   +    +-             +    +    +  
 z*  3  1      99   .011753                        +-   +     -   +    +         +     - 
 z*  3  3     104   .010800                        +-   +     -   +         +     -   +  
 z*  3  3     118   .017832                        +-   +    +    +-    -             +  
 z*  3  1     130   .013391                        +-   +    +     -   +    +          - 
 z*  5  5     151   .128742                        +-   +    +    +    +     -        +  
 z*  7  1     152   .054185                        +-   +    +    +    +    +         +  
 z*  7  5     153  -.126256                        +-   +    +    +    +    +         +  
 z*  5  5     156  -.033120                        +-   +    +    +    +          -   +  
 z*  7  1     157  -.032660                        +-   +    +    +    +         +    +  
 z*  7  5     158   .075022                        +-   +    +    +    +         +    +  
 z*  5  1     166  -.081753                        +-   +    +    +         +    +     - 
 z*  5  5     167   .049137                        +-   +    +    +         +    +     - 
 z*  7  4     168  -.115622                        +-   +    +    +         +    +    +  
 z*  3  3     183  -.012356                        +-   +    +          -        +-   +  
 z*  5  5     188   .014695                        +-   +    +         +     -   +    +  
 z*  5  1     190  -.117216                        +-   +    +         +    +    +     - 
 z*  5  5     191   .073029                        +-   +    +         +    +    +     - 
 z*  7  4     192  -.116557                        +-   +    +         +    +    +    +  
 z*  3  1     199  -.015650                        +-   +    +              +    +-    - 
 z*  3  2     227   .010213                        +-   +         +    +-   +          - 
 z*  5  3     228  -.013659                        +-   +         +    +-   +         +  
 z*  3  2     235  -.011125                        +-   +         +     -   +     -   +  
 z*  5  2     236  -.013522                        +-   +         +     -   +    +    +  
 z*  5  2     245   .084329                        +-   +         +    +    +    +     - 
 z*  5  4     246  -.010991                        +-   +         +    +    +    +     - 
 z*  7  3     247   .042820                        +-   +         +    +    +    +    +  
 z*  3  2     259  -.015967                        +-   +              +-   +     -   +  
 z*  5  2     260  -.017342                        +-   +              +-   +    +    +  
 z*  5  3     269   .010198                        +-   +              +    +    +-   +  
 z*  3  3     286  -.015449                        +-        +-   +         +     -   +  
 z*  3  2     336  -.016351                        +-        +    +     -        +-   +  
 z*  5  2     337  -.018048                        +-        +    +    +    +-        +  
 z*  5  2     343  -.124232                        +-        +    +    +    +    +     - 
 z*  5  4     344  -.052004                        +-        +    +    +    +    +     - 
 z*  5  3     350   .013078                        +-        +    +         +-   +    +  
 z*  3  2     363  -.016159                        +-        +         +    +-   +     - 
 z*  5  3     364   .017691                        +-        +         +    +-   +    +  
 z*  3  3     382   .017323                        +-             +    +-   +     -   +  
 z*  3  1     388  -.011547                        +-             +    +    +-   +     - 
 z*  3  1     391  -.011418                        +-             +    +    +    +-    - 
 z*  5  2     406   .012718                        +    +-   +-   +         +         +  
 z*  3  2     408  -.024487                        +    +-   +-   +               -   +  
 z*  5  2     409  -.026995                        +    +-   +-   +              +    +  
 z*  3  1     454  -.025729                        +    +-   +    +-   +               - 
 z*  3  3     455   .012050                        +    +-   +    +-   +               - 
 z*  3  3     469  -.013534                        +    +-   +     -   +          -   +  
 z*  3  1     477  -.010063                        +    +-   +    +    +-              - 
 z*  5  1     487  -.080589                        +    +-   +    +    +    +          - 
 z*  5  5     488   .047817                        +    +-   +    +    +    +          - 
 z*  7  4     489  -.103644                        +    +-   +    +    +    +         +  
 z*  5  1     492   .199218                        +    +-   +    +    +         +     - 
 z*  5  5     493  -.119935                        +    +-   +    +    +         +     - 
 z*  7  4     494   .211956                        +    +-   +    +    +         +    +  
 z*  7  5     504   .015723                        +    +-   +    +         +    +    +  
 z*  3  1     505  -.010737                        +    +-   +    +              +-    - 
 z*  3  1     522  -.016910                        +    +-   +         +     -   +     - 
 z*  3  1     524  -.013690                        +    +-   +         +    +     -    - 
 z*  5  5     526   .060268                        +    +-   +         +    +     -   +  
 z*  7  1     527   .019383                        +    +-   +         +    +    +    +  
 z*  7  5     528  -.034746                        +    +-   +         +    +    +    +  
 z*  3  1     529   .017617                        +    +-   +         +         +-    - 
 z*  3  2     563  -.010074                        +    +-        +    +-    -        +  
 z*  5  2     564  -.012422                        +    +-        +    +-   +         +  
 z*  3  2     566   .024854                        +    +-        +    +-         -   +  
 z*  5  2     567   .027374                        +    +-        +    +-        +    +  
 z*  5  3     582  -.026093                        +    +-        +    +    +    +     - 
 z*  7  2     583   .017252                        +    +-        +    +    +    +    +  
 z*  5  3     585  -.013613                        +    +-        +    +         +-   +  
 z*  5  3     673   .010840                        +     -   +    +    +    +-        +  
 z*  3  2     675   .018721                        +     -   +    +    +     -   +     - 
 z*  5  3     676  -.024156                        +     -   +    +    +     -   +    +  
 z*  5  4     679   .037541                        +     -   +    +    +    +     -   +  
 z*  7  2     681   .029985                        +     -   +    +    +    +    +    +  
 z*  5  3     683  -.011262                        +     -   +    +    +         +-   +  
 z*  3  3     737   .017414                        +    +    +-   +-         -        +  
 z*  3  3     740   .020208                        +    +    +-   +-              -   +  
 z*  5  1     767   .080148                        +    +    +-   +    +    +          - 
 z*  5  5     768  -.051860                        +    +    +-   +    +    +          - 
 z*  7  4     769   .064183                        +    +    +-   +    +    +         +  
 z*  5  1     772  -.010257                        +    +    +-   +    +         +     - 
 z*  5  5     782  -.210086                        +    +    +-   +         +     -   +  
 z*  7  1     783  -.080076                        +    +    +-   +         +    +    +  
 z*  7  5     784   .205231                        +    +    +-   +         +    +    +  
 z*  5  5     806  -.012464                        +    +    +-        +    +     -   +  
 z*  7  1     807  -.015214                        +    +    +-        +    +    +    +  
 z*  7  5     808   .033021                        +    +    +-        +    +    +    +  
 z*  3  3     812   .017491                        +    +    +-             +-    -   +  
 z*  3  3     815  -.022283                        +    +    +-              -   +-   +  
 z*  3  2     851  -.015473                        +    +     -   +     -   +    +     - 
 z*  5  3     852   .020968                        +    +     -   +     -   +    +    +  
 z*  3  2     859   .011205                        +    +     -   +    +    +     -    - 
 z*  5  3     860  -.016085                        +    +     -   +    +    +     -   +  
 z*  5  4     861   .067721                        +    +     -   +    +    +     -   +  
 z*  5  3     862   .032493                        +    +     -   +    +    +    +     - 
 z*  7  2     863   .040630                        +    +     -   +    +    +    +    +  
 z*  5  2     897   .147901                        +    +    +    +-   +    +          - 
 z*  7  3     899   .052095                        +    +    +    +-   +    +         +  
 z*  5  2     902   .170164                        +    +    +    +-   +         +     - 
 z*  5  4     903   .051797                        +    +    +    +-   +         +     - 
 z*  7  3     904   .016605                        +    +    +    +-   +         +    +  
 z*  5  3     913  -.032009                        +    +    +    +-        +    +     - 
 z*  7  2     914   .020100                        +    +    +    +-        +    +    +  
 z*  3  2     932  -.012684                        +    +    +     -   +     -   +     - 
 z*  5  3     933   .015808                        +    +    +     -   +     -   +    +  
 z*  5  4     936   .011526                        +    +    +     -   +    +     -   +  
 z*  5  3     937   .077877                        +    +    +     -   +    +    +     - 
 z*  7  2     938  -.015814                        +    +    +     -   +    +    +    +  
 z*  5  2     949   .110601                        +    +    +    +    +-   +          - 
 z*  5  4     950   .049125                        +    +    +    +    +-   +          - 
 z*  5  4     964   .055010                        +    +    +    +     -   +     -   +  
 z*  5  3     965   .045285                        +    +    +    +     -   +    +     - 
 z*  7  2     966   .027433                        +    +    +    +     -   +    +    +  
 z*  5  4     969  -.015022                        +    +    +    +     -        +-   +  
 z*  5  3     970   .052419                        +    +    +    +    +    +-         - 
 z*  7  2     971  -.029938                        +    +    +    +    +    +-        +  
 z*  5  4     974   .056105                        +    +    +    +    +     -    -   +  
 z*  5  3     975   .023080                        +    +    +    +    +     -   +     - 
 z*  7  2     976   .034852                        +    +    +    +    +     -   +    +  
 z*  5  3     978  -.025216                        +    +    +    +    +    +     -    - 
 z*  7  2     979   .015581                        +    +    +    +    +    +     -   +  
 z*  7  2     980   .079920                        +    +    +    +    +    +    +     - 
 z*  7  6     981   .011856                        +    +    +    +    +    +    +     - 
 z*  9  3     982   .028612                        +    +    +    +    +    +    +    +  
 z*  9  7     983  -.065440                        +    +    +    +    +    +    +    +  
 z*  9  8     984   .014029                        +    +    +    +    +    +    +    +  
 z*  5  3     985  -.055795                        +    +    +    +    +         +-    - 
 z*  7  2     986   .025146                        +    +    +    +    +         +-   +  
 z*  5  2     994  -.088078                        +    +    +    +         +    +-    - 
 z*  7  3     996  -.044358                        +    +    +    +         +    +-   +  
 z*  5  3    1005   .035190                        +    +    +         +-   +    +     - 
 z*  7  2    1006  -.015415                        +    +    +         +-   +    +    +  
 z*  5  2    1017  -.154838                        +    +    +         +    +-   +     - 
 z*  5  4    1018  -.014714                        +    +    +         +    +-   +     - 
 z*  7  3    1019  -.043425                        +    +    +         +    +-   +    +  
 z*  5  2    1022   .167357                        +    +    +         +    +    +-    - 
 z*  5  4    1023   .053364                        +    +    +         +    +    +-    - 
 z*  7  3    1024   .017573                        +    +    +         +    +    +-   +  
 z*  3  3    1028   .016918                        +    +         +-   +-    -        +  
 z*  3  3    1031   .020550                        +    +         +-   +-         -   +  
 z*  7  5    1048  -.016673                        +    +         +-   +    +    +    +  
 z*  5  5    1079   .212984                        +    +         +    +-   +     -   +  
 z*  7  1    1080   .080197                        +    +         +    +-   +    +    +  
 z*  7  5    1081  -.209861                        +    +         +    +-   +    +    +  
 z*  3  3    1088   .011421                        +    +         +     -    -   +-   +  
 z*  5  1    1092  -.011021                        +    +         +    +    +-   +     - 
 z*  7  4    1094  -.011593                        +    +         +    +    +-   +    +  
 z*  5  1    1097  -.082278                        +    +         +    +    +    +-    - 
 z*  5  5    1098   .049306                        +    +         +    +    +    +-    - 
 z*  7  4    1099  -.117488                        +    +         +    +    +    +-   +  
 z*  3  3    1103   .017940                        +    +              +-   +-    -   +  
 z*  3  3    1106  -.020401                        +    +              +-    -   +-   +  
 z*  5  3    1124  -.011207                        +         +-   +-        +    +    +  
 z*  5  3    1156   .029311                        +         +-   +    +    +    +     - 
 z*  7  2    1157  -.012668                        +         +-   +    +    +    +    +  
 z*  3  2    1161   .024090                        +         +-   +         +-    -   +  
 z*  5  2    1162   .027879                        +         +-   +         +-   +    +  
 z*  3  2    1164   .011277                        +         +-   +          -   +-   +  
 z*  5  2    1165   .010131                        +         +-   +         +    +-   +  
 z*  3  1    1221  -.015413                        +         +    +-   +    +-         - 
 z*  3  1    1224  -.015987                        +         +    +-   +     -   +     - 
 z*  3  1    1226  -.013859                        +         +    +-   +    +     -    - 
 z*  5  5    1228  -.067775                        +         +    +-   +    +     -   +  
 z*  7  1    1229  -.044712                        +         +    +-   +    +    +    +  
 z*  7  5    1230   .106490                        +         +    +-   +    +    +    +  
 z*  3  1    1254  -.011574                        +         +    +    +-   +-         - 
 z*  7  1    1262   .013879                        +         +    +    +-   +    +    +  
 z*  7  5    1263  -.025858                        +         +    +    +-   +    +    +  
 z*  5  1    1274  -.198920                        +         +    +    +    +-   +     - 
 z*  5  5    1275   .119672                        +         +    +    +    +-   +     - 
 z*  7  4    1276  -.225721                        +         +    +    +    +-   +    +  
 z*  5  1    1279  -.080657                        +         +    +    +    +    +-    - 
 z*  5  5    1280   .049139                        +         +    +    +    +    +-    - 
 z*  7  4    1281  -.063248                        +         +    +    +    +    +-   +  
 z*  3  1    1282  -.010541                        +         +    +         +-   +-    - 
 z*  3  1    1291   .024450                        +         +         +    +-   +-    - 
 z*  3  3    1292  -.011318                        +         +         +    +-   +-    - 
 z*  5  3    1297  -.011198                        +              +-   +-   +    +    +  
 z*  3  2    1312  -.024371                        +              +    +-   +-    -   +  
 z*  5  2    1313  -.028154                        +              +    +-   +-   +    +  
 z*  3  2    1315  -.011609                        +              +    +-    -   +-   +  
 z*  5  3    1319  -.012630                        +              +    +    +-   +-   +  
 z*  3  1    1328   .010547                             +-   +-   +    +    +          - 
 z*  3  1    1331  -.010801                             +-   +-   +    +         +     - 
 z*  3  3    1336  -.011439                             +-   +-   +         +     -   +  
 z*  3  2    1356   .014067                             +-   +    +-   +         +     - 
 z*  5  3    1357  -.020152                             +-   +    +-   +         +    +  
 z*  5  2    1393  -.102915                             +-   +    +    +    +    +     - 
 z*  7  3    1395  -.038701                             +-   +    +    +    +    +    +  
 z*  7  6    1396  -.015605                             +-   +    +    +    +    +    +  
 z*  5  2    1397  -.019714                             +-   +    +    +         +-   +  
 z*  3  3    1432   .012338                             +-        +    +-   +     -   +  
 z*  3  2    1459   .014706                             +    +-   +-        +     -   +  
 z*  5  2    1460   .018545                             +    +-   +-        +    +    +  
 z*  5  2    1491  -.108844                             +    +-   +    +    +    +     - 
 z*  5  4    1492  -.050190                             +    +-   +    +    +    +     - 
 z*  5  3    1501  -.015320                             +    +-   +         +    +-   +  
 z*  3  3    1530   .011644                             +     -   +    +-   +     -   +  
 z*  3  1    1536  -.012128                             +     -   +    +    +-   +     - 
 z*  3  1    1546   .014080                             +    +    +-   +-   +          - 
 z*  5  1    1564  -.117212                             +    +    +-   +    +    +     - 
 z*  5  5    1565   .070996                             +    +    +-   +    +    +     - 
 z*  7  4    1566  -.153711                             +    +    +-   +    +    +    +  
 z*  7  7    1567  -.012129                             +    +    +-   +    +    +    +  
 z*  5  1    1597   .079358                             +    +    +    +-   +    +     - 
 z*  5  5    1598  -.051300                             +    +    +    +-   +    +     - 
 z*  7  4    1599   .060754                             +    +    +    +-   +    +    +  
 z*  5  5    1610  -.022396                             +    +    +    +    +-    -   +  
 z*  7  5    1612  -.017899                             +    +    +    +    +-   +    +  
 z*  5  5    1615  -.130243                             +    +    +    +     -   +-   +  
 z*  7  1    1616  -.059847                             +    +    +    +    +    +-   +  
 z*  7  5    1617   .147641                             +    +    +    +    +    +-   +  
 z*  3  3    1627   .013347                             +    +          -   +-   +-   +  
 z*  3  2    1632   .013654                             +         +-   +-   +     -   +  
 z*  5  2    1633   .019357                             +         +-   +-   +    +    +  
 z*  3  2    1651  -.016098                             +         +    +-   +    +-    - 
 z*  5  3    1652   .023016                             +         +    +-   +    +-   +  
 z*  3  1    1676  -.011561                                  +-   +    +    +-   +     - 
 z*  3  2    1694   .013027                                  +    +-   +    +-   +     - 
 z*  5  3    1695  -.019135                                  +    +-   +    +-   +    +  
 z*  5  3    1698  -.014446                                  +    +-   +    +    +-   +  
 z*  3  2    1710   .013244                                  +    +     -   +-   +-   +  
 z*  5  2    1711   .019462                                  +    +    +    +-   +-   +  
 x   5  3   31908  -.010080    2(   1)   1(   4)   +    +-        +              +     - 
 x   7  6   31956  -.010634    2(   1)   1(   4)   +    +-        +              +    +  
 x   7  6   50434  -.010004    1(   1)   1(   2)   +         +         +     -   +    +  
 x   7  6   53436   .011067    2(   1)   1(   4)   +              +         +-   +    +  
 w   5  4   92720   .014841    1(   2)   1(   2)   +    +    +         +    +          - 
 w   5  4   92880   .014465    1(   1)   1(   1)   +    +    +         +         +     - 
 w   5  4   92890   .010353    1(   2)   1(   2)   +    +    +         +         +     - 
 w   5  2   95046   .016126    2(   1)   2(   1)   +    +         +         +    +     - 
 w   5  2   95056   .016065    1(   4)   1(   4)   +    +         +         +    +     - 
 w   7  7   95126  -.015870    2(   1)   2(   1)   +    +         +         +    +    +  
 w   7  7   95136  -.015542    1(   4)   1(   4)   +    +         +         +    +    +  
 w   7  6   99379  -.010307    1(   3)   1(   3)   +         +    +    +    +         +  
 w   5  3   99470   .015623    1(   1)   1(   1)   +         +    +    +         +     - 
 w   5  3   99481   .014232    1(   3)   1(   3)   +         +    +    +         +     - 
 w   7  6   99538   .017385    1(   1)   1(   1)   +         +    +    +         +    +  
 w   7  6   99549   .014955    1(   3)   1(   3)   +         +    +    +         +    +  
 w   7  7  108564   .010080    1(   2)   1(   2)        +    +    +    +    +         +  
 w   7  6  109884   .010849    1(   2)   1(   2)        +    +         +    +    +    +  
 w   7  6  115086  -.010229    1(   2)   1(   3)             +    +    +    +    +    +  

 ci coefficient statistics:
           rq > 0.1               30
      0.1> rq > 0.01             222
     0.01> rq > 0.001           3427
    0.001> rq > 0.0001         17669
   0.0001> rq > 0.00001        36797
  0.00001> rq > 0.000001       36299
 0.000001> rq                  22387
           all                116831


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 8) =       -93.1685254651

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7

                                          orbital    34   37   38   45   46   53   54

                                         symmetry     5    6    6    7    7    8    8

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  3  2      10   .014764                        +-   +-   +    +     -             +  
 z*  5  2      11  -.020185                        +-   +-   +    +    +              +  
 z*  5  3      17  -.014318                        +-   +-   +    +              +    +  
 z*  5  3      23   .012285                        +-   +-   +         +    +         +  
 z*  5  3      26  -.015019                        +-   +-   +         +         +    +  
 z*  3  1      43  -.011954                        +-   +-        +    +         +     - 
 z*  3  2      69  -.012897                        +-   +    +-   +         +          - 
 z*  5  3      70  -.028464                        +-   +    +-   +         +         +  
 z*  3  2      86  -.013096                        +-   +    +-             +     -   +  
 z*  5  2      87   .017802                        +-   +    +-             +    +    +  
 z*  3  1      96  -.010726                        +-   +     -   +    +    +          - 
 z*  3  3     118   .013403                        +-   +    +    +-    -             +  
 z*  3  1     130   .011506                        +-   +    +     -   +    +          - 
 z*  5  5     151   .108566                        +-   +    +    +    +     -        +  
 z*  7  1     152   .062092                        +-   +    +    +    +    +         +  
 z*  7  5     153   .177765                        +-   +    +    +    +    +         +  
 z*  5  5     156  -.020578                        +-   +    +    +    +          -   +  
 z*  7  5     158  -.072892                        +-   +    +    +    +         +    +  
 z*  5  1     166  -.072233                        +-   +    +    +         +    +     - 
 z*  5  5     167   .020944                        +-   +    +    +         +    +     - 
 z*  7  4     168   .145122                        +-   +    +    +         +    +    +  
 z*  3  3     183  -.011486                        +-   +    +          -        +-   +  
 z*  5  5     188   .010921                        +-   +    +         +     -   +    +  
 z*  5  1     190  -.103881                        +-   +    +         +    +    +     - 
 z*  5  5     191  -.026861                        +-   +    +         +    +    +     - 
 z*  7  4     192   .110160                        +-   +    +         +    +    +    +  
 z*  3  1     199  -.014257                        +-   +    +              +    +-    - 
 z*  3  2     227   .015677                        +-   +         +    +-   +          - 
 z*  5  3     228   .026777                        +-   +         +    +-   +         +  
 z*  5  2     236   .017368                        +-   +         +     -   +    +    +  
 z*  5  2     245   .095186                        +-   +         +    +    +    +     - 
 z*  5  4     246  -.063153                        +-   +         +    +    +    +     - 
 z*  7  3     247  -.043873                        +-   +         +    +    +    +    +  
 z*  3  2     259  -.012493                        +-   +              +-   +     -   +  
 z*  5  2     260   .018897                        +-   +              +-   +    +    +  
 z*  3  3     286  -.010260                        +-        +-   +         +     -   +  
 z*  5  3     304  -.014534                        +-        +    +-   +    +         +  
 z*  3  2     336  -.016011                        +-        +    +     -        +-   +  
 z*  5  2     337   .026040                        +-        +    +    +    +-        +  
 z*  5  2     343  -.084164                        +-        +    +    +    +    +     - 
 z*  5  4     344   .042497                        +-        +    +    +    +    +     - 
 z*  5  3     350  -.013519                        +-        +    +         +-   +    +  
 z*  3  3     382   .011689                        +-             +    +-   +     -   +  
 z*  3  1     388  -.012908                        +-             +    +    +-   +     - 
 z*  3  1     391  -.010204                        +-             +    +    +    +-    - 
 z*  5  2     406  -.016016                        +    +-   +-   +         +         +  
 z*  3  2     408  -.021087                        +    +-   +-   +               -   +  
 z*  5  2     409   .033620                        +    +-   +-   +              +    +  
 z*  3  1     454  -.024141                        +    +-   +    +-   +               - 
 z*  5  1     487  -.075927                        +    +-   +    +    +    +          - 
 z*  7  4     489   .128408                        +    +-   +    +    +    +         +  
 z*  5  1     492   .185635                        +    +-   +    +    +         +     - 
 z*  5  5     493   .030146                        +    +-   +    +    +         +     - 
 z*  7  4     494  -.225388                        +    +-   +    +    +         +    +  
 z*  7  5     504  -.011576                        +    +-   +    +         +    +    +  
 z*  3  1     505  -.012019                        +    +-   +    +              +-    - 
 z*  3  1     522  -.013658                        +    +-   +         +     -   +     - 
 z*  3  1     524  -.011788                        +    +-   +         +    +     -    - 
 z*  5  5     526   .057125                        +    +-   +         +    +     -   +  
 z*  7  1     527   .037048                        +    +-   +         +    +    +    +  
 z*  7  5     528   .067379                        +    +-   +         +    +    +    +  
 z*  3  1     529   .013476                        +    +-   +         +         +-    - 
 z*  5  2     564   .017051                        +    +-        +    +-   +         +  
 z*  3  2     566   .021562                        +    +-        +    +-         -   +  
 z*  5  2     567  -.034466                        +    +-        +    +-        +    +  
 z*  5  4     581   .011989                        +    +-        +    +    +     -   +  
 z*  5  3     582  -.028816                        +    +-        +    +    +    +     - 
 z*  7  2     583  -.010337                        +    +-        +    +    +    +    +  
 z*  5  3     585   .014151                        +    +-        +    +         +-   +  
 z*  5  3     673  -.011474                        +     -   +    +    +    +-        +  
 z*  5  3     676   .022575                        +     -   +    +    +     -   +    +  
 z*  5  3     678  -.013961                        +     -   +    +    +    +     -   +  
 z*  5  4     679   .039032                        +     -   +    +    +    +     -   +  
 z*  5  3     680  -.028309                        +     -   +    +    +    +    +     - 
 z*  7  2     681  -.037016                        +     -   +    +    +    +    +    +  
 z*  3  3     737   .016860                        +    +    +-   +-         -        +  
 z*  3  3     740   .014986                        +    +    +-   +-              -   +  
 z*  5  1     767   .069220                        +    +    +-   +    +    +          - 
 z*  5  5     768   .028204                        +    +    +-   +    +    +          - 
 z*  7  4     769  -.050482                        +    +    +-   +    +    +         +  
 z*  5  1     772  -.010063                        +    +    +-   +    +         +     - 
 z*  5  5     782  -.166681                        +    +    +-   +         +     -   +  
 z*  7  1     783  -.094506                        +    +    +-   +         +    +    +  
 z*  7  5     784  -.274674                        +    +    +-   +         +    +    +  
 z*  7  5     808  -.028177                        +    +    +-        +    +    +    +  
 z*  3  3     812   .015350                        +    +    +-             +-    -   +  
 z*  3  3     815  -.016202                        +    +    +-              -   +-   +  
 z*  3  2     851  -.014270                        +    +     -   +     -   +    +     - 
 z*  5  3     852  -.030545                        +    +     -   +     -   +    +    +  
 z*  5  3     860   .020532                        +    +     -   +    +    +     -   +  
 z*  5  4     861   .058982                        +    +     -   +    +    +     -   +  
 z*  5  3     862  -.010222                        +    +     -   +    +    +    +     - 
 z*  7  2     863  -.060576                        +    +     -   +    +    +    +    +  
 z*  5  2     871  -.011268                        +    +     -   +         +    +-   +  
 z*  5  2     897   .142030                        +    +    +    +-   +    +          - 
 z*  5  4     898  -.080764                        +    +    +    +-   +    +          - 
 z*  7  3     899  -.043095                        +    +    +    +-   +    +         +  
 z*  5  2     902   .126913                        +    +    +    +-   +         +     - 
 z*  5  4     903  -.064524                        +    +    +    +-   +         +     - 
 z*  5  4     912   .012365                        +    +    +    +-        +     -   +  
 z*  5  3     913  -.033888                        +    +    +    +-        +    +     - 
 z*  7  2     914  -.011065                        +    +    +    +-        +    +    +  
 z*  5  3     927  -.010964                        +    +    +     -    -   +    +    +  
 z*  5  3     937   .057394                        +    +    +     -   +    +    +     - 
 z*  5  2     946  -.010775                        +    +    +     -        +    +-   +  
 z*  5  2     949   .073313                        +    +    +    +    +-   +          - 
 z*  5  4     950  -.035213                        +    +    +    +    +-   +          - 
 z*  5  3     963   .011358                        +    +    +    +     -   +     -   +  
 z*  5  4     964   .045147                        +    +    +    +     -   +     -   +  
 z*  7  2     966  -.050637                        +    +    +    +     -   +    +    +  
 z*  5  4     969  -.022405                        +    +    +    +     -        +-   +  
 z*  5  3     970   .053253                        +    +    +    +    +    +-         - 
 z*  7  2     971   .016682                        +    +    +    +    +    +-        +  
 z*  5  4     974   .051529                        +    +    +    +    +     -    -   +  
 z*  5  3     975  -.012866                        +    +    +    +    +     -   +     - 
 z*  7  2     976  -.053998                        +    +    +    +    +     -   +    +  
 z*  7  6     977   .013067                        +    +    +    +    +     -   +    +  
 z*  5  3     978  -.025872                        +    +    +    +    +    +     -    - 
 z*  7  2     979  -.010804                        +    +    +    +    +    +     -   +  
 z*  7  2     980   .067916                        +    +    +    +    +    +    +     - 
 z*  7  6     981  -.027865                        +    +    +    +    +    +    +     - 
 z*  9  7     983   .078824                        +    +    +    +    +    +    +    +  
 z*  9  8     984   .022418                        +    +    +    +    +    +    +    +  
 z*  5  3     985  -.049547                        +    +    +    +    +         +-    - 
 z*  7  2     986  -.012450                        +    +    +    +    +         +-   +  
 z*  5  2     994  -.097711                        +    +    +    +         +    +-    - 
 z*  5  4     995   .063378                        +    +    +    +         +    +-    - 
 z*  7  3     996   .043275                        +    +    +    +         +    +-   +  
 z*  5  4    1004  -.012725                        +    +    +         +-   +     -   +  
 z*  5  3    1005   .029958                        +    +    +         +-   +    +     - 
 z*  5  2    1017  -.140684                        +    +    +         +    +-   +     - 
 z*  5  4    1018   .079833                        +    +    +         +    +-   +     - 
 z*  7  3    1019   .034272                        +    +    +         +    +-   +    +  
 z*  5  2    1022   .122666                        +    +    +         +    +    +-    - 
 z*  5  4    1023  -.059927                        +    +    +         +    +    +-    - 
 z*  7  3    1024  -.010434                        +    +    +         +    +    +-   +  
 z*  3  3    1028   .016610                        +    +         +-   +-    -        +  
 z*  3  3    1031   .015313                        +    +         +-   +-         -   +  
 z*  5  5    1046  -.011571                        +    +         +-   +    +     -   +  
 z*  7  5    1048   .010121                        +    +         +-   +    +    +    +  
 z*  5  5    1079   .169146                        +    +         +    +-   +     -   +  
 z*  7  1    1080   .095537                        +    +         +    +-   +    +    +  
 z*  7  5    1081   .282075                        +    +         +    +-   +    +    +  
 z*  5  1    1092  -.011694                        +    +         +    +    +-   +     - 
 z*  7  4    1094   .017731                        +    +         +    +    +-   +    +  
 z*  5  1    1097  -.072044                        +    +         +    +    +    +-    - 
 z*  5  5    1098   .020516                        +    +         +    +    +    +-    - 
 z*  7  4    1099   .145328                        +    +         +    +    +    +-   +  
 z*  3  3    1103   .015793                        +    +              +-   +-    -   +  
 z*  3  3    1106  -.014808                        +    +              +-    -   +-   +  
 z*  5  3    1124   .015100                        +         +-   +-        +    +    +  
 z*  5  4    1155  -.012036                        +         +-   +    +    +     -   +  
 z*  5  3    1156   .025215                        +         +-   +    +    +    +     - 
 z*  3  2    1161   .020909                        +         +-   +         +-    -   +  
 z*  5  2    1162  -.034972                        +         +-   +         +-   +    +  
 z*  5  2    1165  -.010844                        +         +-   +         +    +-   +  
 z*  3  1    1221  -.015532                        +         +    +-   +    +-         - 
 z*  3  1    1224  -.015082                        +         +    +-   +     -   +     - 
 z*  3  1    1226  -.011755                        +         +    +-   +    +     -    - 
 z*  5  5    1228  -.051921                        +         +    +-   +    +     -   +  
 z*  7  1    1229  -.023626                        +         +    +-   +    +    +    +  
 z*  7  5    1230  -.123847                        +         +    +-   +    +    +    +  
 z*  7  5    1263   .021205                        +         +    +    +-   +    +    +  
 z*  5  1    1274  -.186464                        +         +    +    +    +-   +     - 
 z*  5  5    1275  -.019152                        +         +    +    +    +-   +     - 
 z*  7  4    1276   .249917                        +         +    +    +    +-   +    +  
 z*  5  1    1279  -.073225                        +         +    +    +    +    +-    - 
 z*  5  5    1280  -.024949                        +         +    +    +    +    +-    - 
 z*  7  4    1281   .055907                        +         +    +    +    +    +-   +  
 z*  3  1    1282  -.011643                        +         +    +         +-   +-    - 
 z*  3  1    1291   .019664                        +         +         +    +-   +-    - 
 z*  5  3    1297   .015094                        +              +-   +-   +    +    +  
 z*  3  2    1312  -.021315                        +              +    +-   +-    -   +  
 z*  5  2    1313   .035689                        +              +    +-   +-   +    +  
 z*  5  2    1316   .010899                        +              +    +-   +    +-   +  
 z*  5  3    1319   .013123                        +              +    +    +-   +-   +  
 z*  3  3    1336  -.011498                             +-   +-   +         +     -   +  
 z*  5  3    1357   .011913                             +-   +    +-   +         +    +  
 z*  5  2    1393  -.104075                             +-   +    +    +    +    +     - 
 z*  5  4    1394   .065483                             +-   +    +    +    +    +     - 
 z*  7  3    1395   .035308                             +-   +    +    +    +    +    +  
 z*  7  6    1396  -.013647                             +-   +    +    +    +    +    +  
 z*  5  2    1397   .023088                             +-   +    +    +         +-   +  
 z*  5  3    1417  -.010903                             +-   +         +    +    +-   +  
 z*  3  3    1432   .012307                             +-        +    +-   +     -   +  
 z*  3  2    1459   .012236                             +    +-   +-        +     -   +  
 z*  5  2    1460  -.024299                             +    +-   +-        +    +    +  
 z*  5  2    1482   .010012                             +    +-   +     -   +    +    +  
 z*  5  2    1491  -.070976                             +    +-   +    +    +    +     - 
 z*  5  4    1492   .034545                             +    +-   +    +    +    +     - 
 z*  3  2    1500   .015336                             +    +-   +         +    +-    - 
 z*  5  3    1501   .027931                             +    +-   +         +    +-   +  
 z*  3  1    1536  -.011818                             +     -   +    +    +-   +     - 
 z*  3  1    1546   .011302                             +    +    +-   +-   +          - 
 z*  5  1    1564  -.105497                             +    +    +-   +    +    +     - 
 z*  7  4    1566   .177618                             +    +    +-   +    +    +    +  
 z*  5  1    1597   .068681                             +    +    +    +-   +    +     - 
 z*  5  5    1598   .028569                             +    +    +    +-   +    +     - 
 z*  7  4    1599  -.046146                             +    +    +    +-   +    +    +  
 z*  5  5    1610  -.027258                             +    +    +    +    +-    -   +  
 z*  7  1    1611  -.020714                             +    +    +    +    +-   +    +  
 z*  5  5    1615  -.105830                             +    +    +    +     -   +-   +  
 z*  7  1    1616  -.059022                             +    +    +    +    +    +-   +  
 z*  7  5    1617  -.193452                             +    +    +    +    +    +-   +  
 z*  3  3    1627   .012303                             +    +          -   +-   +-   +  
 z*  3  2    1632   .010739                             +         +-   +-   +     -   +  
 z*  5  2    1633  -.023936                             +         +-   +-   +    +    +  
 z*  3  2    1651  -.012751                             +         +    +-   +    +-    - 
 z*  5  3    1652  -.030268                             +         +    +-   +    +-   +  
 z*  5  3    1695   .019998                                  +    +-   +    +-   +    +  
 z*  5  3    1698   .015055                                  +    +-   +    +    +-   +  
 z*  3  2    1710   .011931                                  +    +     -   +-   +-   +  
 z*  5  2    1711  -.023978                                  +    +    +    +-   +-   +  
 x   7  6   31090   .010819    1(   1)   1(   2)   +    +-   +         +              +  
 x   7  6   31956   .011231    2(   1)   1(   4)   +    +-        +              +    +  
 x   7  7   37480   .010639    1(   1)   1(   2)   +    +    +-                  +    +  
 x   7  7   43240   .010950    1(   1)   1(   2)   +    +              +-        +    +  
 x   7  7   44824  -.012689    1(   1)   1(   3)   +         +-   +              +    +  
 x   7  6   50434   .011248    1(   1)   1(   2)   +         +         +     -   +    +  
 x   7  7   52600   .013116    1(   1)   1(   3)   +              +    +-        +    +  
 x   7  6   53436  -.012171    2(   1)   1(   4)   +              +         +-   +    +  
 w   7  7   92180   .011407    1(   2)   1(   4)   +    +    +    +         +         +  
 w   7  6   92341  -.010031    1(   1)   1(   4)   +    +    +    +              +    +  
 w   5  4   92720   .012757    1(   2)   1(   2)   +    +    +         +    +          - 
 w   5  4   92880   .011972    1(   1)   1(   1)   +    +    +         +         +     - 
 w   7  7   94625  -.011181    2(   1)   1(   2)   +    +         +    +    +         +  
 w   7  6   94779   .010114    1(   1)   2(   1)   +    +         +    +         +    +  
 w   5  2   95046   .012700    2(   1)   2(   1)   +    +         +         +    +     - 
 w   5  2   95056   .012728    1(   4)   1(   4)   +    +         +         +    +     - 
 w   7  7   95126   .021169    2(   1)   2(   1)   +    +         +         +    +    +  
 w   7  7   95136   .020876    1(   4)   1(   4)   +    +         +         +    +    +  
 w   7  6   99379   .011865    1(   3)   1(   3)   +         +    +    +    +         +  
 w   5  3   99470   .014632    1(   1)   1(   1)   +         +    +    +         +     - 
 w   5  3   99481   .013256    1(   3)   1(   3)   +         +    +    +         +     - 
 w   7  6   99538  -.018989    1(   1)   1(   1)   +         +    +    +         +    +  
 w   7  6   99549  -.015810    1(   3)   1(   3)   +         +    +    +         +    +  
 w   7  6   99882   .010634    1(   3)   1(   4)   +         +    +         +    +    +  
 w   7  7   99885  -.011184    1(   1)   2(   1)   +         +    +         +    +    +  
 w   7  6  102575  -.010801    2(   1)   1(   3)   +              +    +    +    +    +  
 w   7  7  102581  -.011419    1(   1)   1(   4)   +              +    +    +    +    +  
 w   7  7  108564  -.013230    1(   2)   1(   2)        +    +    +    +    +         +  
 w   7  7  109075   .011165    2(   1)   1(   2)        +    +    +         +    +    +  
 w   7  6  109884  -.011740    1(   2)   1(   2)        +    +         +    +    +    +  
 w   7  7  111764   .011951    1(   2)   1(   4)        +         +    +    +    +    +  
 w   7  6  115086   .011827    1(   2)   1(   3)             +    +    +    +    +    +  

 ci coefficient statistics:
           rq > 0.1               25
      0.1> rq > 0.01             220
     0.01> rq > 0.001           3275
    0.001> rq > 0.0001         17149
   0.0001> rq > 0.00001        36231
  0.00001> rq > 0.000001       36799
 0.000001> rq                  23132
           all                116831


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 9) =       -93.1671118464

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7

                                          orbital    34   37   38   45   46   53   54

                                         symmetry     5    6    6    7    7    8    8

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  3  2      16   .010002                        +-   +-   +    +              +     - 
 z*  3  2      25   .013827                        +-   +-   +         +         +     - 
 z*  3  2      69   .017072                        +-   +    +-   +         +          - 
 z*  3  3     118   .010835                        +-   +    +    +-    -             +  
 z*  5  5     151   .132441                        +-   +    +    +    +     -        +  
 z*  7  1     152  -.151230                        +-   +    +    +    +    +         +  
 z*  7  5     153  -.113559                        +-   +    +    +    +    +         +  
 z*  7  1     157   .084191                        +-   +    +    +    +         +    +  
 z*  7  5     158  -.023203                        +-   +    +    +    +         +    +  
 z*  5  1     166   .048336                        +-   +    +    +         +    +     - 
 z*  5  5     167  -.087645                        +-   +    +    +         +    +     - 
 z*  7  4     168   .021738                        +-   +    +    +         +    +    +  
 z*  5  1     190   .083149                        +-   +    +         +    +    +     - 
 z*  5  5     191  -.079245                        +-   +    +         +    +    +     - 
 z*  7  4     192   .114299                        +-   +    +         +    +    +    +  
 z*  3  2     227  -.010598                        +-   +         +    +-   +          - 
 z*  5  3     228  -.010634                        +-   +         +    +-   +         +  
 z*  5  2     245   .068335                        +-   +         +    +    +    +     - 
 z*  5  4     246   .025816                        +-   +         +    +    +    +     - 
 z*  7  3     247   .105068                        +-   +         +    +    +    +    +  
 z*  5  3     269  -.010558                        +-   +              +    +    +-   +  
 z*  5  2     343  -.039760                        +-        +    +    +    +    +     - 
 z*  5  4     344   .068223                        +-        +    +    +    +    +     - 
 z*  7  3     345  -.146610                        +-        +    +    +    +    +    +  
 z*  3  2     363   .015569                        +-        +         +    +-   +     - 
 z*  5  3     364  -.018854                        +-        +         +    +-   +    +  
 z*  3  3     382   .010049                        +-             +    +-   +     -   +  
 z*  5  1     487   .047189                        +    +-   +    +    +    +          - 
 z*  5  5     488  -.076045                        +    +-   +    +    +    +          - 
 z*  7  4     489   .026704                        +    +-   +    +    +    +         +  
 z*  5  1     492  -.131034                        +    +-   +    +    +         +     - 
 z*  5  5     493   .144235                        +    +-   +    +    +         +     - 
 z*  7  4     494  -.150004                        +    +-   +    +    +         +    +  
 z*  5  5     502   .017643                        +    +-   +    +         +     -   +  
 z*  7  1     503   .012752                        +    +-   +    +         +    +    +  
 z*  7  5     504  -.021939                        +    +-   +    +         +    +    +  
 z*  5  5     526   .081114                        +    +-   +         +    +     -   +  
 z*  7  1     527  -.047928                        +    +-   +         +    +    +    +  
 z*  7  5     528  -.086093                        +    +-   +         +    +    +    +  
 z*  5  4     581  -.010302                        +    +-        +    +    +     -   +  
 z*  5  3     582  -.014207                        +    +-        +    +    +    +     - 
 z*  3  2     584  -.010092                        +    +-        +    +         +-    - 
 z*  3  2     675  -.019206                        +     -   +    +    +     -   +     - 
 z*  5  3     676   .011501                        +     -   +    +    +     -   +    +  
 z*  5  3     680   .010109                        +     -   +    +    +    +    +     - 
 z*  7  2     681   .013413                        +     -   +    +    +    +    +    +  
 z*  3  3     737   .014243                        +    +    +-   +-         -        +  
 z*  3  3     740   .013369                        +    +    +-   +-              -   +  
 z*  5  1     767  -.061314                        +    +    +-   +    +    +          - 
 z*  5  5     768   .044527                        +    +    +-   +    +    +          - 
 z*  7  4     769  -.095342                        +    +    +-   +    +    +         +  
 z*  5  5     782  -.207467                        +    +    +-   +         +     -   +  
 z*  7  1     783   .240718                        +    +    +-   +         +    +    +  
 z*  7  5     784   .157283                        +    +    +-   +         +    +    +  
 z*  7  1     807   .037875                        +    +    +-        +    +    +    +  
 z*  7  5     808  -.018907                        +    +    +-        +    +    +    +  
 z*  3  3     812   .013847                        +    +    +-             +-    -   +  
 z*  3  3     815  -.013847                        +    +    +-              -   +-   +  
 z*  3  2     851   .016731                        +    +     -   +     -   +    +     - 
 z*  3  2     859  -.012853                        +    +     -   +    +    +     -    - 
 z*  5  4     861   .034227                        +    +     -   +    +    +     -   +  
 z*  5  3     862   .045052                        +    +     -   +    +    +    +     - 
 z*  7  2     863   .019079                        +    +     -   +    +    +    +    +  
 z*  5  2     897   .095806                        +    +    +    +-   +    +          - 
 z*  7  3     899   .161738                        +    +    +    +-   +    +         +  
 z*  5  2     902   .070401                        +    +    +    +-   +         +     - 
 z*  5  4     903  -.064486                        +    +    +    +-   +         +     - 
 z*  7  3     904   .189301                        +    +    +    +-   +         +    +  
 z*  5  4     912  -.013259                        +    +    +    +-        +     -   +  
 z*  5  3     913  -.016692                        +    +    +    +-        +    +     - 
 z*  3  2     932   .013240                        +    +    +     -   +     -   +     - 
 z*  5  3     933  -.016750                        +    +    +     -   +     -   +    +  
 z*  5  3     935   .010586                        +    +    +     -   +    +     -   +  
 z*  5  4     936   .034351                        +    +    +     -   +    +     -   +  
 z*  5  3     937   .047277                        +    +    +     -   +    +    +     - 
 z*  5  2     949   .026321                        +    +    +    +    +-   +          - 
 z*  5  4     950  -.065056                        +    +    +    +    +-   +          - 
 z*  7  3     951   .131789                        +    +    +    +    +-   +         +  
 z*  5  4     955   .013515                        +    +    +    +    +-        +     - 
 z*  7  3     956  -.017593                        +    +    +    +    +-        +    +  
 z*  5  4     964   .031844                        +    +    +    +     -   +     -   +  
 z*  5  3     965   .043086                        +    +    +    +     -   +    +     - 
 z*  7  2     966   .013798                        +    +    +    +     -   +    +    +  
 z*  5  4     969   .019961                        +    +    +    +     -        +-   +  
 z*  5  3     970   .027291                        +    +    +    +    +    +-         - 
 z*  5  4     974   .024927                        +    +    +    +    +     -    -   +  
 z*  5  3     975   .034482                        +    +    +    +    +     -   +     - 
 z*  7  2     976   .017126                        +    +    +    +    +     -   +    +  
 z*  5  3     978  -.010502                        +    +    +    +    +    +     -    - 
 z*  9  8     984  -.018476                        +    +    +    +    +    +    +    +  
 z*  5  3     985  -.022962                        +    +    +    +    +         +-    - 
 z*  7  3     991  -.010247                        +    +    +    +         +-   +    +  
 z*  5  2     994  -.070194                        +    +    +    +         +    +-    - 
 z*  5  4     995  -.025614                        +    +    +    +         +    +-    - 
 z*  7  3     996  -.104125                        +    +    +    +         +    +-   +  
 z*  5  4    1004   .011794                        +    +    +         +-   +     -   +  
 z*  5  3    1005   .012168                        +    +    +         +-   +    +     - 
 z*  5  2    1017  -.096431                        +    +    +         +    +-   +     - 
 z*  7  3    1019  -.172910                        +    +    +         +    +-   +    +  
 z*  5  2    1022   .062558                        +    +    +         +    +    +-    - 
 z*  5  4    1023  -.066139                        +    +    +         +    +    +-    - 
 z*  7  3    1024   .183417                        +    +    +         +    +    +-   +  
 z*  3  3    1028   .014298                        +    +         +-   +-    -        +  
 z*  3  3    1031   .013136                        +    +         +-   +-         -   +  
 z*  5  5    1046  -.023824                        +    +         +-   +    +     -   +  
 z*  7  1    1047  -.010217                        +    +         +-   +    +    +    +  
 z*  7  5    1048   .026941                        +    +         +-   +    +    +    +  
 z*  5  5    1079   .204284                        +    +         +    +-   +     -   +  
 z*  7  1    1080  -.248681                        +    +         +    +-   +    +    +  
 z*  7  5    1081  -.153860                        +    +         +    +-   +    +    +  
 z*  5  1    1097   .051838                        +    +         +    +    +    +-    - 
 z*  5  5    1098  -.089241                        +    +         +    +    +    +-    - 
 z*  7  4    1099   .027179                        +    +         +    +    +    +-   +  
 z*  3  3    1103   .013744                        +    +              +-   +-    -   +  
 z*  3  3    1106  -.012933                        +    +              +-    -   +-   +  
 z*  5  5    1228  -.048337                        +         +    +-   +    +     -   +  
 z*  7  1    1229   .123045                        +         +    +-   +    +    +    +  
 z*  7  5    1230   .019105                        +         +    +-   +    +    +    +  
 z*  7  1    1262  -.033104                        +         +    +    +-   +    +    +  
 z*  7  5    1263   .018810                        +         +    +    +-   +    +    +  
 z*  5  1    1274   .128975                        +         +    +    +    +-   +     - 
 z*  5  5    1275  -.154403                        +         +    +    +    +-   +     - 
 z*  7  4    1276   .133426                        +         +    +    +    +-   +    +  
 z*  5  1    1279   .055627                        +         +    +    +    +    +-    - 
 z*  5  5    1280  -.041745                        +         +    +    +    +    +-    - 
 z*  7  4    1281   .080781                        +         +    +    +    +    +-   +  
 z*  3  3    1336  -.010738                             +-   +-   +         +     -   +  
 z*  3  2    1356  -.015297                             +-   +    +-   +         +     - 
 z*  5  3    1357   .016026                             +-   +    +-   +         +    +  
 z*  5  2    1393  -.076245                             +-   +    +    +    +    +     - 
 z*  5  4    1394  -.011567                             +-   +    +    +    +    +     - 
 z*  7  3    1395  -.124012                             +-   +    +    +    +    +    +  
 z*  5  3    1417   .010210                             +-   +         +    +    +-   +  
 z*  3  3    1432   .010503                             +-        +    +-   +     -   +  
 z*  5  3    1452   .012216                             +    +-   +-   +    +         +  
 z*  5  2    1491  -.025649                             +    +-   +    +    +    +     - 
 z*  5  4    1492   .065718                             +    +-   +    +    +    +     - 
 z*  7  3    1493  -.128533                             +    +-   +    +    +    +    +  
 z*  3  2    1500  -.011470                             +    +-   +         +    +-    - 
 z*  5  1    1564   .077659                             +    +    +-   +    +    +     - 
 z*  5  5    1565  -.108867                             +    +    +-   +    +    +     - 
 z*  7  4    1566   .065292                             +    +    +-   +    +    +    +  
 z*  5  1    1597  -.062037                             +    +    +    +-   +    +     - 
 z*  5  5    1598   .042494                             +    +    +    +-   +    +     - 
 z*  7  4    1599  -.096543                             +    +    +    +-   +    +    +  
 z*  5  5    1610  -.051329                             +    +    +    +    +-    -   +  
 z*  7  5    1612   .061440                             +    +    +    +    +-   +    +  
 z*  5  5    1615  -.124068                             +    +    +    +     -   +-   +  
 z*  7  1    1616   .174036                             +    +    +    +    +    +-   +  
 z*  7  5    1617   .091728                             +    +    +    +    +    +-   +  
 z*  3  3    1627   .011520                             +    +          -   +-   +-   +  
 z*  3  2    1651   .018258                             +         +    +-   +    +-    - 
 z*  3  2    1694  -.014229                                  +    +-   +    +-   +     - 
 z*  3  2    1697  -.011120                                  +    +-   +    +    +-    - 
 x   7  3   44800   .011163    1(   1)   1(   3)   +         +-   +              +    +  
 x   7  3   52576  -.011543    1(   1)   1(   3)   +              +    +-        +    +  
 w   7  3   92146  -.010072    1(   2)   1(   4)   +    +    +    +         +         +  
 w   7  5   92156  -.011522    2(   1)   2(   1)   +    +    +    +         +         +  
 w   7  5   92776   .016323    1(   2)   1(   2)   +    +    +         +    +         +  
 w   7  5   92936   .016229    1(   1)   1(   1)   +    +    +         +         +    +  
 w   7  5   92946   .011464    1(   2)   1(   2)   +    +    +         +         +    +  
 w   5  2   95046   .015592    2(   1)   2(   1)   +    +         +         +    +     - 
 w   5  2   95056   .015746    1(   4)   1(   4)   +    +         +         +    +     - 
 w   7  3   95092  -.018702    2(   1)   2(   1)   +    +         +         +    +    +  
 w   7  3   95102  -.018484    1(   4)   1(   4)   +    +         +         +    +    +  
 w   7  7   95126  -.011807    2(   1)   2(   1)   +    +         +         +    +    +  
 w   7  7   95136  -.011978    1(   4)   1(   4)   +    +         +         +    +    +  
 w   7  5   95932   .010187    1(   2)   1(   4)   +    +              +    +    +    +  
 w   5  3   99470  -.010166    1(   1)   1(   1)   +         +    +    +         +     - 
 w   7  2   99504   .011859    1(   1)   1(   1)   +         +    +    +         +    +  
 w   7  2   99515   .010122    1(   3)   1(   3)   +         +    +    +         +    +  
 w   7  6   99538  -.010830    1(   1)   1(   1)   +         +    +    +         +    +  
 w   7  6   99549  -.010787    1(   3)   1(   3)   +         +    +    +         +    +  
 w   7  3   99851   .010350    1(   1)   2(   1)   +         +    +         +    +    +  
 w   7  5  100688  -.010413    1(   1)   1(   3)   +         +         +    +    +    +  
 w   7  3  102547   .010490    1(   1)   1(   4)   +              +    +    +    +    +  
 w   7  3  108530   .011943    1(   2)   1(   2)        +    +    +    +    +         +  
 w   7  3  109041  -.010158    2(   1)   1(   2)        +    +    +         +    +    +  
 w   7  3  111730  -.010838    1(   2)   1(   4)        +         +    +    +    +    +  
 w   7  5  111750  -.010585    1(   4)   1(   4)        +         +    +    +    +    +  
 w   7  5  115081   .011284    1(   3)   1(   3)             +    +    +    +    +    +  

 ci coefficient statistics:
           rq > 0.1               30
      0.1> rq > 0.01             151
     0.01> rq > 0.001           2935
    0.001> rq > 0.0001         11197
   0.0001> rq > 0.00001        29541
  0.00001> rq > 0.000001       38019
 0.000001> rq                  34958
           all                116831


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy(10) =       -93.1657147849

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7

                                          orbital    34   37   38   45   46   53   54

                                         symmetry     5    6    6    7    7    8    8

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  3  2      69  -.014598                        +-   +    +-   +         +          - 
 z*  5  3      70   .014993                        +-   +    +-   +         +         +  
 z*  5  5     151   .041756                        +-   +    +    +    +     -        +  
 z*  7  1     152   .172693                        +-   +    +    +    +    +         +  
 z*  7  5     153  -.127112                        +-   +    +    +    +    +         +  
 z*  7  1     157  -.081219                        +-   +    +    +    +         +    +  
 z*  5  1     166   .041651                        +-   +    +    +         +    +     - 
 z*  5  5     167  -.074203                        +-   +    +    +         +    +     - 
 z*  7  4     168   .049040                        +-   +    +    +         +    +    +  
 z*  5  1     190   .061733                        +-   +    +         +    +    +     - 
 z*  5  5     191  -.091805                        +-   +    +         +    +    +     - 
 z*  7  4     192   .113435                        +-   +    +         +    +    +    +  
 z*  3  2     227   .010274                        +-   +         +    +-   +          - 
 z*  5  3     228  -.014752                        +-   +         +    +-   +         +  
 z*  5  2     245   .036102                        +-   +         +    +    +    +     - 
 z*  5  4     246   .111312                        +-   +         +    +    +    +     - 
 z*  7  3     247  -.034706                        +-   +         +    +    +    +    +  
 z*  3  2     306   .012342                        +-        +    +-   +         +     - 
 z*  5  2     343  -.015921                        +-        +    +    +    +    +     - 
 z*  5  4     344  -.151055                        +-        +    +    +    +    +     - 
 z*  7  3     345   .102024                        +-        +    +    +    +    +    +  
 z*  3  2     366   .013992                        +-        +         +    +    +-    - 
 z*  5  1     487   .041944                        +    +-   +    +    +    +          - 
 z*  5  5     488  -.072533                        +    +-   +    +    +    +          - 
 z*  7  4     489   .052780                        +    +-   +    +    +    +         +  
 z*  5  1     492  -.105170                        +    +-   +    +    +         +     - 
 z*  5  5     493   .162290                        +    +-   +    +    +         +     - 
 z*  7  4     494  -.174082                        +    +-   +    +    +         +    +  
 z*  5  5     502   .012317                        +    +-   +    +         +     -   +  
 z*  7  5     504  -.015863                        +    +-   +    +         +    +    +  
 z*  5  5     526   .034565                        +    +-   +         +    +     -   +  
 z*  7  1     527   .060827                        +    +-   +         +    +    +    +  
 z*  7  5     528  -.080025                        +    +-   +         +    +    +    +  
 z*  5  4     581  -.011259                        +    +-        +    +    +     -   +  
 z*  7  2     583  -.019342                        +    +-        +    +    +    +    +  
 z*  5  4     679  -.013056                        +     -   +    +    +    +     -   +  
 z*  5  3     680   .010758                        +     -   +    +    +    +    +     - 
 z*  7  2     681  -.035828                        +     -   +    +    +    +    +    +  
 z*  5  1     767  -.042286                        +    +    +-   +    +    +          - 
 z*  5  5     768   .060264                        +    +    +-   +    +    +          - 
 z*  7  4     769  -.084302                        +    +    +-   +    +    +         +  
 z*  5  5     782  -.060983                        +    +    +-   +         +     -   +  
 z*  7  1     783  -.277513                        +    +    +-   +         +    +    +  
 z*  7  5     784   .182765                        +    +    +-   +         +    +    +  
 z*  7  1     807  -.036494                        +    +    +-        +    +    +    +  
 z*  3  2     851  -.019671                        +    +     -   +     -   +    +     - 
 z*  5  3     852   .019576                        +    +     -   +     -   +    +    +  
 z*  5  3     862   .025867                        +    +     -   +    +    +    +     - 
 z*  7  2     863  -.040212                        +    +     -   +    +    +    +    +  
 z*  3  2     887  -.010331                        +    +    +    +-   +-              - 
 z*  5  2     897   .047050                        +    +    +    +-   +    +          - 
 z*  5  4     898   .175062                        +    +    +    +-   +    +          - 
 z*  7  3     899  -.079573                        +    +    +    +-   +    +         +  
 z*  5  2     902   .030365                        +    +    +    +-   +         +     - 
 z*  5  4     903   .198785                        +    +    +    +-   +         +     - 
 z*  7  3     904  -.123240                        +    +    +    +-   +         +    +  
 z*  5  4     912  -.013469                        +    +    +    +-        +     -   +  
 z*  7  2     914  -.022675                        +    +    +    +-        +    +    +  
 z*  3  2     929  -.014862                        +    +    +     -   +    +-         - 
 z*  3  2     932  -.011778                        +    +    +     -   +     -   +     - 
 z*  5  4     936   .018011                        +    +    +     -   +    +     -   +  
 z*  5  3     937   .015617                        +    +    +     -   +    +    +     - 
 z*  7  2     938   .025219                        +    +    +     -   +    +    +    +  
 z*  3  2     939   .015592                        +    +    +     -   +         +-    - 
 z*  5  4     950   .131690                        +    +    +    +    +-   +          - 
 z*  7  3     951  -.094829                        +    +    +    +    +-   +         +  
 z*  5  4     955  -.015372                        +    +    +    +    +-        +     - 
 z*  7  3     956   .010526                        +    +    +    +    +-        +    +  
 z*  5  3     965   .021658                        +    +    +    +     -   +    +     - 
 z*  7  2     966  -.023894                        +    +    +    +     -   +    +    +  
 z*  5  4     969   .021456                        +    +    +    +     -        +-   +  
 z*  7  2     971   .038205                        +    +    +    +    +    +-        +  
 z*  5  3     975   .020740                        +    +    +    +    +     -   +     - 
 z*  7  2     976  -.035569                        +    +    +    +    +     -   +    +  
 z*  7  2     979  -.019831                        +    +    +    +    +    +     -   +  
 z*  9  8     984  -.019318                        +    +    +    +    +    +    +    +  
 z*  7  2     986  -.038120                        +    +    +    +    +         +-   +  
 z*  5  4     990  -.012226                        +    +    +    +         +-   +     - 
 z*  5  2     994  -.036707                        +    +    +    +         +    +-    - 
 z*  5  4     995  -.113224                        +    +    +    +         +    +-    - 
 z*  7  3     996   .038499                        +    +    +    +         +    +-   +  
 z*  5  4    1004   .011328                        +    +    +         +-   +     -   +  
 z*  7  2    1006   .022619                        +    +    +         +-   +    +    +  
 z*  5  2    1017  -.048117                        +    +    +         +    +-   +     - 
 z*  5  4    1018  -.186861                        +    +    +         +    +-   +     - 
 z*  7  3    1019   .087845                        +    +    +         +    +-   +    +  
 z*  5  2    1022   .024702                        +    +    +         +    +    +-    - 
 z*  5  4    1023   .190201                        +    +    +         +    +    +-    - 
 z*  7  3    1024  -.124205                        +    +    +         +    +    +-   +  
 z*  3  2    1025  -.010025                        +    +    +              +-   +-    - 
 z*  5  5    1046  -.015271                        +    +         +-   +    +     -   +  
 z*  7  1    1047   .010215                        +    +         +-   +    +    +    +  
 z*  7  5    1048   .019386                        +    +         +-   +    +    +    +  
 z*  5  5    1079   .058994                        +    +         +    +-   +     -   +  
 z*  7  1    1080   .273813                        +    +         +    +-   +    +    +  
 z*  7  5    1081  -.180876                        +    +         +    +-   +    +    +  
 z*  5  5    1093  -.010588                        +    +         +    +    +-   +     - 
 z*  5  1    1097   .043573                        +    +         +    +    +    +-    - 
 z*  5  5    1098  -.074856                        +    +         +    +    +    +-    - 
 z*  7  4    1099   .052485                        +    +         +    +    +    +-   +  
 z*  7  2    1157   .019338                        +         +-   +    +    +    +    +  
 z*  7  1    1229  -.125625                        +         +    +-   +    +    +    +  
 z*  7  5    1230   .048030                        +         +    +-   +    +    +    +  
 z*  7  1    1262   .026939                        +         +    +    +-   +    +    +  
 z*  5  1    1274   .105398                        +         +    +    +    +-   +     - 
 z*  5  5    1275  -.165447                        +         +    +    +    +-   +     - 
 z*  7  4    1276   .166306                        +         +    +    +    +-   +    +  
 z*  5  1    1279   .041916                        +         +    +    +    +    +-    - 
 z*  5  5    1280  -.060803                        +         +    +    +    +    +-    - 
 z*  7  4    1281   .079148                        +         +    +    +    +    +-   +  
 z*  3  2    1353   .013754                             +-   +    +-   +    +          - 
 z*  5  2    1393  -.040812                             +-   +    +    +    +    +     - 
 z*  5  4    1394  -.134404                             +-   +    +    +    +    +     - 
 z*  7  3    1395   .049718                             +-   +    +    +    +    +    +  
 z*  3  2    1413  -.010950                             +-   +         +    +-   +     - 
 z*  5  4    1492  -.131452                             +    +-   +    +    +    +     - 
 z*  7  3    1493   .096060                             +    +-   +    +    +    +    +  
 z*  3  2    1500   .010705                             +    +-   +         +    +-    - 
 z*  5  3    1501  -.014202                             +    +-   +         +    +-   +  
 z*  5  1    1564   .062956                             +    +    +-   +    +    +     - 
 z*  5  5    1565  -.101088                             +    +    +-   +    +    +     - 
 z*  7  4    1566   .090174                             +    +    +-   +    +    +    +  
 z*  5  1    1597  -.042907                             +    +    +    +-   +    +     - 
 z*  5  5    1598   .059717                             +    +    +    +-   +    +     - 
 z*  7  4    1599  -.084985                             +    +    +    +-   +    +    +  
 z*  5  5    1610  -.029208                             +    +    +    +    +-    -   +  
 z*  7  5    1612   .047320                             +    +    +    +    +-   +    +  
 z*  5  5    1615  -.034664                             +    +    +    +     -   +-   +  
 z*  7  1    1616  -.188347                             +    +    +    +    +    +-   +  
 z*  7  5    1617   .116719                             +    +    +    +    +    +-   +  
 z*  3  2    1651  -.014996                             +         +    +-   +    +-    - 
 z*  5  3    1652   .014398                             +         +    +-   +    +-   +  
 x   7  3   37456   .010768    1(   1)   1(   2)   +    +    +-                  +    +  
 x   7  1   41166  -.010220    2(   1)   1(   4)   +    +         +-             +    +  
 x   7  3   43216   .010602    1(   1)   1(   2)   +    +              +-        +    +  
 x   7  1   44166   .010189    2(   1)   1(   4)   +    +                   +    +-   +  
 x   7  3   44800  -.012848    1(   1)   1(   3)   +         +-   +              +    +  
 x   7  1   47644  -.010737    1(   1)   1(   3)   +         +    +-   +              +  
 x   7  1   50404  -.010240    1(   1)   1(   3)   +         +         +     -   +    +  
 x   7  3   52576   .012709    1(   1)   1(   3)   +              +    +-        +    +  
 w   7  1   92122  -.012207    2(   1)   2(   1)   +    +    +    +         +         +  
 w   7  3   92146   .011536    1(   2)   1(   4)   +    +    +    +         +         +  
 w   7  1   92742   .017498    1(   2)   1(   2)   +    +    +         +    +         +  
 w   7  1   92902   .017376    1(   1)   1(   1)   +    +    +         +         +    +  
 w   7  1   92912   .012015    1(   2)   1(   2)   +    +    +         +         +    +  
 w   7  1   93253  -.010694    2(   1)   1(   2)   +    +    +              +    +    +  
 w   7  3   94591  -.010923    2(   1)   1(   2)   +    +         +    +    +         +  
 w   7  3   95092   .021174    2(   1)   2(   1)   +    +         +         +    +    +  
 w   7  3   95102   .020841    1(   4)   1(   4)   +    +         +         +    +    +  
 w   7  7   95126  -.013964    2(   1)   2(   1)   +    +         +         +    +    +  
 w   7  7   95136  -.014024    1(   4)   1(   4)   +    +         +         +    +    +  
 w   7  1   95898   .010859    1(   2)   1(   4)   +    +              +    +    +    +  
 w   7  2   99504   .013086    1(   1)   1(   1)   +         +    +    +         +    +  
 w   7  2   99515   .011655    1(   3)   1(   3)   +         +    +    +         +    +  
 w   7  6   99538  -.013411    1(   1)   1(   1)   +         +    +    +         +    +  
 w   7  6   99549  -.012592    1(   3)   1(   3)   +         +    +    +         +    +  
 w   7  3   99851  -.011580    1(   1)   2(   1)   +         +    +         +    +    +  
 w   7  1  100654  -.010944    1(   1)   1(   3)   +         +         +    +    +    +  
 w   7  3  102547  -.011408    1(   1)   1(   4)   +              +    +    +    +    +  
 w   7  3  108530  -.013127    1(   2)   1(   2)        +    +    +    +    +         +  
 w   7  3  109041   .011258    2(   1)   1(   2)        +    +    +         +    +    +  
 w   7  1  109834  -.010101    1(   2)   1(   3)        +    +         +    +    +    +  
 w   7  1  111716  -.011175    1(   4)   1(   4)        +         +    +    +    +    +  
 w   7  3  111730   .011796    1(   2)   1(   4)        +         +    +    +    +    +  
 w   7  1  115047   .012059    1(   3)   1(   3)             +    +    +    +    +    +  

 ci coefficient statistics:
           rq > 0.1               30
      0.1> rq > 0.01             135
     0.01> rq > 0.001           2728
    0.001> rq > 0.0001         10489
   0.0001> rq > 0.00001        28497
  0.00001> rq > 0.000001       34825
 0.000001> rq                  40127
           all                116831
