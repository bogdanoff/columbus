
 program cipc      

 print the csf info for mrsdci wave functions

 written by: ron shepard

 version date: 06-jun-96

 This Version of Program cipc is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 workspace allocation parameters: lencor= 100000000 mem1=         1 ifirst=         1

 drt header information:
  cidrt_title                                                                    
 nmot  =   146 niot  =    13 nfct  =     4 nfvt  =     0
 nrow  =   124 nsym  =     4 ssym  =     1 lenbuf=  1600
 spnorb=     F spnodd=     F lxyzir(1:3)= 0 0 0
 nwalk,xbar:      51947     1151    11715    19281    19800
 nvalwt,nvalw:    40446      630    11283    14835    13698
 ncsft:        58607084
 map(*)=    -1 -1 -1 -1130131132  1  2  3  4  5  6  7  8  9 10 11 12 13
            14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33
            34 35 36 37 38 39 40 41 42 43 44 45 46133134135136137138 47
            48 49 50 51 52 53 54 55 56 57 58 59 60 61 62 63 64 65 66 67
            68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87
            88 89 90 91 92 93139140 94 95 96 97 98 99100101102103104105
           106107108109110111141142112113114115116117118119120121122123
           124125126127128129
 mu(*)=      2  2  2  2  2  2  2  0  0  0  0  0  0
 syml(*) =   1  1  1  2  2  2  2  2  2  3  3  4  4
 rmo(*)=     5  6  7  1  2  3  4  5  6  1  2  1  2

 indx01: 40446 indices saved in indxv(*)
===================================ROOT # 1===================================

 rdhciv: CI vector file information:
 Hermit Integral Program : SIFS version  j07             Tue Oct 19 15:27:23 2004
  cidrt_title                                                                    
 Hermit Integral Program : SIFS version  grimming        Tue Aug  7 18:24:26 2001
  title                                                                          
 mofmt: formatted orbitals label=morb    grimming        Tue Aug  7 18:38:16 2001
 SIFS file created by program tran.      j07             Tue Oct 19 15:27:34 2004
 energy computed by program ciudg.       j07             Tue Oct 19 15:41:34 2004

 lenrec =   32768 lenci =  58607084 ninfo =  6 nenrgy =  7 ntitle =  7

 Max. overlap with ref vector #        1
 Valid ci vector #        1
 Method:        0       94% overlap
 energy( 1)=  1.034429979483E+02, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -1.274375457176E+02, ietype=    6,   fcore energy of type: H1(*)   
 energy( 3)= -1.191864289308E+02, ietype=    5,   fcore energy of type: Vref(*) 
 energy( 4)= -1.554211752062E+02, ietype=-1026,   total energy of type: MRSDCI  
 energy( 5)=  4.595202324067E-04, ietype=-2055, cnvginf energy of type: CI-Resid
 energy( 6)=  4.618245217358E-07, ietype=-2056, cnvginf energy of type: CI-D.E. 
 energy( 7)=  7.177501012134E-08, ietype=-2057, cnvginf energy of type: CI-ApxDE
==================================================================================

 space is available for  49950258 coefficients.

 updated histogram parameters:
 csfmn = 0.0000E+00 csfmx = 1.0000E+00 fhist = 5.0000E-01 nhist =  20

 this program will print the csfs generated from
 the drt according to the following print options :

 1) run in batch mode: all valid roots are automatically
    analysed and csf info is printed by default contribution
    threshold 0.01 
 2) run in interactive mode

 input menu number [  0]:

================================================================================
===================================VECTOR # 1===================================
================================================================================


 rdcivnew:      64 coefficients were selected.
 workspace: ncsfmx=49950258
 ncsfmx= 49950258

 histogram parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =58607084 ncsft =58607084 ncsf =      64
 nhist =  20 fhist = 0.50000

    cmin                cmax        num  '*'=     2 csfs.
 ----------          ----------   ----- ---------|---------|---------|---------|
 5.0000E-01 <= |c| < 1.0000E+00       1 *
 2.5000E-01 <= |c| < 5.0000E-01       0
 1.2500E-01 <= |c| < 2.5000E-01       1 *
 6.2500E-02 <= |c| < 1.2500E-01       2 *
 3.1250E-02 <= |c| < 6.2500E-02       5 ***
 1.5625E-02 <= |c| < 3.1250E-02       5 ***
 7.8125E-03 <= |c| < 1.5625E-02      50 *************************
 3.9062E-03 <= |c| < 7.8125E-03       0
 1.9531E-03 <= |c| < 3.9062E-03       0
 9.7656E-04 <= |c| < 1.9531E-03       0
 4.8828E-04 <= |c| < 9.7656E-04       0
 2.4414E-04 <= |c| < 4.8828E-04       0
 1.2207E-04 <= |c| < 2.4414E-04       0
 6.1035E-05 <= |c| < 1.2207E-04       0
 3.0518E-05 <= |c| < 6.1035E-05       0
 1.5259E-05 <= |c| < 3.0518E-05       0
 7.6294E-06 <= |c| < 1.5259E-05       0
 3.8147E-06 <= |c| < 7.6294E-06       0
 0.0000E+00 <= |c| < 3.8147E-06       0
                                  ----- ---------|---------|---------|---------|
                  total read =       64 total stored =      64

 from the selected csfs,
 min(|csfvec(:)|) = 1.0025E-02    max(|csfvec(:)|) = 9.0643E-01
 norm= 0.999999999878798174
 csfs will be printed based on coefficient magnitudes.

 current csfvec(*) selection parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =58607084 ncsft =58607084 ncsf =      64

 i:slabel(i) =  1: Ag   2: Bu   3: Au   4: Bg 
 
 frozen orbital =    1    2    3    4
 symfc(*)       =    1    1    1    1
 label          =  Ag   Ag   Ag   Ag 
 rmo(*)         =    1    2    3    4
 
 internal level =    1    2    3    4    5    6    7    8    9   10
   11   12   13
 syml(*)        =    1    1    1    2    2    2    2    2    2    3
    3    4    4
 label          =  Ag   Ag   Ag   Bu   Bu   Bu   Bu   Bu   Bu   Au 
  Au   Bg   Bg 
 rmo(*)         =    5    6    7    1    2    3    4    5    6    1
    2    1    2

 printing selected csfs in sorted order from cmin = 0.01000 to cmax = 1.00000

   indcsf     c     c**2   v  lab:rmo  lab:rmo   step(*)
  ------- -------- ------- - ---- --- ---- --- ------------
        2 -0.90643 0.82161 z*                    3333333333030
        1  0.15293 0.02339 z*                    3333333333300
        8 -0.10604 0.01125 z*                    3333333331122
        6  0.10372 0.01076 z*                    3333333331212
       12  0.06237 0.00389 z*                    3333333330033
        9  0.06215 0.00386 z*                    3333333330330
        5  0.06208 0.00385 z*                    3333333331230
        3 -0.05513 0.00304 z*                    3333333333012
        4  0.05398 0.00291 z*                    3333333333003
       11 -0.02474 0.00061 z*                    3333333330303
   174532 -0.01806 0.00033 y           Bu : 18  13233333333120
 30310288 -0.01768 0.00031 w  Ag : 13  Bu : 13 123333333331020
   174530 -0.01645 0.00027 y           Bu : 16  13233333333120
 30312169 -0.01565 0.00024 w  Au :  3  Bg :  3 123333333331020
    11264 -0.01475 0.00022 y           Ag : 13  13333331332032
      884 -0.01464 0.00021 y           Au :  4  13333333330230
      794 -0.01456 0.00021 y           Bg :  4  13333333331220
   174858  0.01426 0.00020 y           Ag : 19  13233333331230
 24066343 -0.01394 0.00019 x  Bu : 13  Au :  5 112333333333020
     1039  0.01393 0.00019 y           Ag :  9  13333333323120
   107485 -0.01380 0.00019 y           Bu : 11  13323333333120
     3363 -0.01366 0.00019 y           Bu : 18  13333333231230
 30322522  0.01349 0.00018 w           Ag : 13  33333333330030
      632  0.01340 0.00018 y           Au :  4  13333333333200
    22859 -0.01339 0.00018 y           Ag : 14  13333313333220
     1042  0.01324 0.00018 y           Ag : 12  13333333323120
     3177 -0.01319 0.00017 y           Bu : 18  13333333233012
   107811  0.01308 0.00017 y           Ag : 12  13323333331230
      703 -0.01267 0.00016 y           Bg :  3  13333333332120
   174526  0.01263 0.00016 y           Bu : 12  13233333333120
     3175 -0.01242 0.00015 y           Bu : 16  13333333233012
   174672  0.01239 0.00015 y           Ag : 19  13233333333012
    10938  0.01226 0.00015 y           Bu : 13  13333331333022
      793  0.01223 0.00015 y           Bg :  3  13333333331220
   175042  0.01206 0.00015 y           Bu : 18  13233333331032
     3361 -0.01198 0.00014 y           Bu : 16  13333333231230
 44596345  0.01190 0.00014 w           Bu : 11  33303333333030
       55  0.01190 0.00014 z*                    3333330333033
      830 -0.01178 0.00014 y           Au :  4  13333333331022
   174525 -0.01144 0.00013 y           Bu : 11  13233333333120
   259079  0.01138 0.00013 y           Bu : 19  12333333331032
   107625  0.01135 0.00013 y           Ag : 12  13323333333012
   108003 -0.01125 0.00013 y           Bu : 19  13323333331032
   174671  0.01113 0.00012 y           Ag : 18  13233333333012
     3356 -0.01104 0.00012 y           Bu : 11  13333333231230
   258891 -0.01099 0.00012 y           Ag : 16  12333333331230
 24074710  0.01096 0.00012 x  Ag : 13  Au :  5 112333333332030
   107996 -0.01095 0.00012 y           Bu : 12  13323333331032
   259072  0.01094 0.00012 y           Bu : 12  12333333331032
     3027  0.01070 0.00011 y           Ag :  8  13333333233120
      831  0.01064 0.00011 y           Au :  5  13333333331022
 30307285  0.01055 0.00011 w           Bg :  3  33333333333000
 30306013  0.01048 0.00011 w           Bu : 13  33333333333000
     1046 -0.01036 0.00011 y           Ag : 16  13333333323120
    16736 -0.01031 0.00011 y           Bu : 18  13333323331230
   174667  0.01023 0.00010 y           Ag : 14  13233333333012
   174862  0.01018 0.00010 y           Ag : 23  13233333331230
     3551 -0.01018 0.00010 y           Ag : 19  13333333231032
      758 -0.01017 0.00010 y           Au :  4  13333333332012
 50210718  0.01016 0.00010 w  Bu : 11  Bu : 15 123123333333030
    16550 -0.01015 0.00010 y           Bu : 18  13333323333012
 30324711  0.01013 0.00010 w           Au :  3  33333333330030
 30577958  0.01012 0.00010 w           Bu : 11  33333333033030
     1041  0.01003 0.00010 y           Ag : 11  13333333323120
           64 csfs were printed in this range.
