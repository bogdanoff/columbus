1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      09/20/00    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 using llenci= -1
================================================================================
four external integ   76.75 MB location: distr. (dual) 
three external inte   31.25 MB location: distr. (dual) 
four external integ   74.00 MB location: distr. (dual) 
three external inte   30.75 MB location: distr. (dual) 
diagonal integrals     0.25 MB location: virtual disk  
off-diagonal integr    5.00 MB location: virtual disk  
computed file size in DP units
fil3w:     4095875
fil3x:     4030341
fil4w:    10059469
fil4x:     9699032
ofdgint:       32760
diagint:      655200
computed file size in DP units
fil3w:    32767000
fil3x:    32242728
fil4w:    80475752
fil4x:    77592256
ofdgint:      262080
diagint:     5241600
 nsubmx= 6  lenci= 58607084
global arrays:     ********   (           5812.78 MB)
vdisk:               687960   (              5.25 MB)
drt:                2492280   (              9.51 MB)
================================================================================
 Main memory management:
 global         47696145 DP per process
 vdisk            687960 DP per process
 stack                 0 DP per process
 core           51615895 DP per process
Array Handle=-1000 Name:'civect' Data Type:double
Array Dimensions:58607084x6
Process=0	 owns array section: [1:3662943,1:6] 
Process=1	 owns array section: [3662944:7325886,1:6] 
Process=2	 owns array section: [7325887:10988829,1:6] 
Process=3	 owns array section: [10988830:14651772,1:6] 
Process=4	 owns array section: [14651773:18314715,1:6] 
Process=5	 owns array section: [18314716:21977658,1:6] 
Process=6	 owns array section: [21977659:25640601,1:6] 
Process=7	 owns array section: [25640602:29303544,1:6] 
Process=8	 owns array section: [29303545:32966487,1:6] 
Process=9	 owns array section: [32966488:36629430,1:6] 
Process=10	 owns array section: [36629431:40292373,1:6] 
Process=11	 owns array section: [40292374:43955316,1:6] 
Process=12	 owns array section: [43955317:47618259,1:6] 
Process=13	 owns array section: [47618260:51281202,1:6] 
Process=14	 owns array section: [51281203:54944145,1:6] 
Process=15	 owns array section: [54944146:58607084,1:6] 
Array Handle=-999 Name:'sigvec' Data Type:double
Array Dimensions:58607084x6
Process=0	 owns array section: [1:3662943,1:6] 
Process=1	 owns array section: [3662944:7325886,1:6] 
Process=2	 owns array section: [7325887:10988829,1:6] 
Process=3	 owns array section: [10988830:14651772,1:6] 
Process=4	 owns array section: [14651773:18314715,1:6] 
Process=5	 owns array section: [18314716:21977658,1:6] 
Process=6	 owns array section: [21977659:25640601,1:6] 
Process=7	 owns array section: [25640602:29303544,1:6] 
Process=8	 owns array section: [29303545:32966487,1:6] 
Process=9	 owns array section: [32966488:36629430,1:6] 
Process=10	 owns array section: [36629431:40292373,1:6] 
Process=11	 owns array section: [40292374:43955316,1:6] 
Process=12	 owns array section: [43955317:47618259,1:6] 
Process=13	 owns array section: [47618260:51281202,1:6] 
Process=14	 owns array section: [51281203:54944145,1:6] 
Process=15	 owns array section: [54944146:58607084,1:6] 
Array Handle=-998 Name:'hdg' Data Type:double
Array Dimensions:58607084x1
Process=0	 owns array section: [1:3662943,1:1] 
Process=1	 owns array section: [3662944:7325886,1:1] 
Process=2	 owns array section: [7325887:10988829,1:1] 
Process=3	 owns array section: [10988830:14651772,1:1] 
Process=4	 owns array section: [14651773:18314715,1:1] 
Process=5	 owns array section: [18314716:21977658,1:1] 
Process=6	 owns array section: [21977659:25640601,1:1] 
Process=7	 owns array section: [25640602:29303544,1:1] 
Process=8	 owns array section: [29303545:32966487,1:1] 
Process=9	 owns array section: [32966488:36629430,1:1] 
Process=10	 owns array section: [36629431:40292373,1:1] 
Process=11	 owns array section: [40292374:43955316,1:1] 
Process=12	 owns array section: [43955317:47618259,1:1] 
Process=13	 owns array section: [47618260:51281202,1:1] 
Process=14	 owns array section: [51281203:54944145,1:1] 
Process=15	 owns array section: [54944146:58607084,1:1] 
Array Handle=-997 Name:'drt' Data Type:double
Array Dimensions:178020x7
Process=0	 owns array section: [1:11127,1:7] 
Process=1	 owns array section: [11128:22254,1:7] 
Process=2	 owns array section: [22255:33381,1:7] 
Process=3	 owns array section: [33382:44508,1:7] 
Process=4	 owns array section: [44509:55635,1:7] 
Process=5	 owns array section: [55636:66762,1:7] 
Process=6	 owns array section: [66763:77889,1:7] 
Process=7	 owns array section: [77890:89016,1:7] 
Process=8	 owns array section: [89017:100143,1:7] 
Process=9	 owns array section: [100144:111270,1:7] 
Process=10	 owns array section: [111271:122397,1:7] 
Process=11	 owns array section: [122398:133524,1:7] 
Process=12	 owns array section: [133525:144651,1:7] 
Process=13	 owns array section: [144652:155778,1:7] 
Process=14	 owns array section: [155779:166905,1:7] 
Process=15	 owns array section: [166906:178020,1:7] 
Array Handle=-996 Name:'nxttask' Data Type:integer
Array Dimensions:1x1
Process=0	 owns array section: [1:1,1:1] 
Process=1	 owns array section: [0:-1,0:-1] 
Process=2	 owns array section: [0:-1,0:-1] 
Process=3	 owns array section: [0:-1,0:-1] 
Process=4	 owns array section: [0:-1,0:-1] 
Process=5	 owns array section: [0:-1,0:-1] 
Process=6	 owns array section: [0:-1,0:-1] 
Process=7	 owns array section: [0:-1,0:-1] 
Process=8	 owns array section: [0:-1,0:-1] 
Process=9	 owns array section: [0:-1,0:-1] 
Process=10	 owns array section: [0:-1,0:-1] 
Process=11	 owns array section: [0:-1,0:-1] 
Process=12	 owns array section: [0:-1,0:-1] 
Process=13	 owns array section: [0:-1,0:-1] 
Process=14	 owns array section: [0:-1,0:-1] 
Process=15	 owns array section: [0:-1,0:-1] 
 CIUDG version 5.9.7 ( 5-Oct-2004)
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 0,
  GSET = 0,
   DAVCOR =10,
  NCOREL = 22
  NROOT = 1
  IVMODE = 3
  NBKITR = 1
  NVBKMN = 1
  NVBKMX = 6
  NVRFMX = 6
  RTOLBK = 1e-3,1e-3,
  NITER =  10
  NVCIMN = 1
  NVCIMX =  6
  RTOLCI = 1e-3,1e-3,
  IDEN  = 1
  CSFPRN = 10,
  FTCALC = 1
  MAXSEG = 20
  nsegd = 1,1,6,6
  nseg0x = 1,1,2,2
  nseg1x = 1,1,2,2
  nseg2x = 1,1,4,4
  nseg3x = 1,1,2,2
  nseg4x = 1,1,6,6,
  c2ex0ex=0
  c3ex1ex=0
  cdg4ex=1
  fileloc=1,1,4,4,4,4
  &end
 ------------------------------------------------------------------------

 ** list of control variables **
 nrfitr =   30      nvrfmx =    6      nvrfmn =    1
 lvlprt =    0      nroot  =    1      noldv  =    0      noldhv =    0
 nunitv =    1      ntype  =    0      nbkitr =    1      niter  =   10
 ivmode =    3      vout   =    0      istrt  =    0      iortls =    0
 nvbkmx =    6      ibktv  =   -1      ibkthv =   -1      frcsub =    0
 nvcimx =    6      icitv  =   -1      icithv =   -1      maxseg =   20
 iden   =    1      itran  =    0      froot  =    0      rtmode =    0
 ftcalc =    1      lrtshift=1.0000    ncouple=    1      skipso  =   F
 ncorel =   22      csfprn  =  10      ctol   = 1.00E-02  davcor  =  10


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-03    1.000E-03
 
 units and filenames:
    1: ( 6)    ciudgls                                                     
    2: ( 5)    ciudgin                                                     
    3: ( 7)    ciudgsm                                                     
    4: ( 4)    cihdiag                                                     
    5: ( 3)    cihpseu                                                     
    6: (10)    cihvfl                                                      
    7: (11)    civfl                                                       
    8: (12)    diagint                                                     
    9: (13)    ofdgint                                                     
   10: (17)    cidrtfl                                                     
   11: (20)    ciftdfl                                                     
   12: (21)    ciftofl                                                     
   13: (24)    ciftifl                                                     
   14: ( 8)    ciflind                                                     
   15: (15)    civin                                                       
   16: (16)    civout                                                      
   18: (18)    d1fl                                                        
   20: (53)    cifvfl                                                      
   21: (25)    ciscr4                                                      
   22: (41)    ciftotd                                                     
   23: (42)    flacpfd                                                     
   26: (26)    ciscr5                                                      
   27: (47)    restart                                                     
   28: (48)    civout                                                      
   29: (49)    cirefv                                                      
   31: (31)    fil4w                                                       
   32: (32)    fil4x                                                       
   33: (33)    fil3w                                                       
   34: (34)    fil3x                                                       
   36: (36)    aoints                                                      
   37: (37)    aoints2                                                     
   38: (38)    drtfil                                                      
   39: (50)    cisrtif                                                     
   42: (50)    mocoef                                                      
   45: (52)    nocoef_ci                                                   
   46: (53)    civfl_restart                                               
 ------------------------------------------------------------------------
 broadcasting INDATA    
 broadcasting ACPFINFO  
 broadcasting CFILES    

 workspace allocation information: lcore=  51615895 mem1=         1 ifirst=         1

 integral file titles:
 Hermit Integral Program : SIFS version  j07             Tue Oct 19 15:27:23 2004
  cidrt_title                                                                    
 Hermit Integral Program : SIFS version  grimming        Tue Aug  7 18:24:26 2001
  title                                                                          
 mofmt: formatted orbitals label=morb    grimming        Tue Aug  7 18:38:16 2001
 SIFS file created by program tran.      j07             Tue Oct 19 15:27:34 2004

 core energy values from the integral file:
 energy( 1)=  1.034429979483E+02, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -1.274375457176E+02, ietype=    6,   fcore energy of type: H1(*)   
 energy( 3)= -1.191864289308E+02, ietype=    5,   fcore energy of type: Vref(*) 

 total core repulsion energy = -1.431809767001E+02

 drt header information:
  cidrt_title                                                                    
 spnorb, spnodd, lxyzir,hmult F F 0 0 0 0
 nmot  =   146 niot  =    13 nfct  =     4 nfvt  =     0
 nrow  =   124 nsym  =     4 ssym  =     1 lenbuf=  1600
 nwalk,xbar:      51947     1151    11715    19281    19800
 nvalwt,nvalw:    40446      630    11283    14835    13698
 ncsft:        58607084
 total number of valid internal walks:   40446
 nvalz,nvaly,nvalx,nvalw =      630   11283   14835   13698

 cisrt info file parameters:
 file number  12 blocksize   4095
 mxbld  20475
 nd4ext,nd2ext,nd0ext 16770  3354   182
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int  9468510  3698334   597417    46410     1461        0        0        0
 minbl4,minbl3,maxbl2  7650  7650  7653
 maxbuf 32767
 number of external orbitals per symmetry block:  46  47  18  18
 nmsym   4 number of internal orbitals  13

 formula file title:
 Hermit Integral Program : SIFS version  j07             Tue Oct 19 15:27:23 2004
  cidrt_title                                                                    
 Hermit Integral Program : SIFS version  grimming        Tue Aug  7 18:24:26 2001
  title                                                                          
 mofmt: formatted orbitals label=morb    grimming        Tue Aug  7 18:38:16 2001
 SIFS file created by program tran.      j07             Tue Oct 19 15:27:34 2004
  cidrt_title                                                                    
 file nmb.  24 block size     0
 pthz,pthy,pthx,pthw:  1151 11715 19281 19800 total internal walks:   51947
 maxlp3,n3xtlp,n2xtlp,n1xtlp,n0xtlp,n2lp,n1lp,n0lp     0     0     0     0
 orbsym(*)= 1 1 1 2 2 2 2 2 2 3 3 4 4

 setref:      273 references kept,
                0 references were marked as invalid, out of
              273 total.
 limcnvrt: found 630  valid internal walksout of  1151  walks (skipping trailing invalids)
  ... adding  630  segmentation marks segtype= 1
 limcnvrt: found 11283  valid internal walksout of  11715  walks (skipping trailing invalids)
  ... adding  11283  segmentation marks segtype= 2
 limcnvrt: found 14835  valid internal walksout of  19281  walks (skipping trailing invalids)
  ... adding  14835  segmentation marks segtype= 3
 limcnvrt: found 13698  valid internal walksout of  19800  walks (skipping trailing invalids)
  ... adding  13698  segmentation marks segtype= 4
 broadcasting SOLXYZ    
 broadcasting INDATA    
 broadcasting REPNUC    
 broadcasting INF       
 broadcasting SOINF     
 broadcasting DATA      
 broadcasting MOMAP     
 broadcasting CLI       
 broadcasting CSYM      
 broadcasting SLABEL    
 broadcasting MOMAP     
 broadcasting DRT       
 broadcasting INF1      
 broadcasting DRTINFO   
loaded      142 onel-dg  integrals(   1records ) at       1 on virtual disk
loaded    16770 4ext-dg  integrals(   5records ) at    4096 on virtual disk
loaded     3354 2ext-dg  integrals(   1records ) at   24571 on virtual disk
loaded      182 0ext-dg  integrals(   1records ) at   28666 on virtual disk
loaded     2551 onel-of  integrals(   1records ) at   32761 on virtual disk
loaded   597417 2ext-og  integrals( 146records ) at   36856 on virtual disk
loaded    46410 1ext-og  integrals(  12records ) at  634726 on virtual disk
loaded     1461 0ext-og  integrals(   1records ) at  683866 on virtual disk

 number of external paths / symmetry
 vertex x    2422    2486    1674    1674
 vertex w    2551    2486    1674    1674

 lprune: l(*,*,*) pruned with nwalk=    1151 nvalwt=     630 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=   11715 nvalwt=   11283 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    2613 nvalwt=    2473 nprune=     112

 lprune: l(*,*,*) pruned with nwalk=    2673 nvalwt=    2473 nprune=     102

 lprune: l(*,*,*) pruned with nwalk=    2889 nvalwt=    2473 nprune=      89

 lprune: l(*,*,*) pruned with nwalk=    3247 nvalwt=    2473 nprune=     104

 lprune: l(*,*,*) pruned with nwalk=    3717 nvalwt=    2473 nprune=      97

 lprune: l(*,*,*) pruned with nwalk=    4142 nvalwt=    2470 nprune=     118

 lprune: l(*,*,*) pruned with nwalk=    2460 nvalwt=    2284 nprune=     128

 lprune: l(*,*,*) pruned with nwalk=    2552 nvalwt=    2284 nprune=     112

 lprune: l(*,*,*) pruned with nwalk=    3009 nvalwt=    2284 nprune=     122

 lprune: l(*,*,*) pruned with nwalk=    3470 nvalwt=    2284 nprune=     103

 lprune: l(*,*,*) pruned with nwalk=    3960 nvalwt=    2284 nprune=      95

 lprune: l(*,*,*) pruned with nwalk=    4349 nvalwt=    2278 nprune=     120



                   segmentation summary for type diagonal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1        1151|       630|         0|       630|         0|         1|
 -------------------------------------------------------------------------------
  Y 2       11715|    358912|       630|     11283|       630|      1321|
 -------------------------------------------------------------------------------
  X 3        2613|   5045742|    359542|      2473|     11913|     13326|
 -------------------------------------------------------------------------------
  X 4        2673|   5023886|   5405284|      2473|     14386|     16429|
 -------------------------------------------------------------------------------
  X 5        2874|   4954342|  10429170|      2473|     16859|     19562|
 -------------------------------------------------------------------------------
  X 6        3157|   4842182|  15383512|      2473|     19332|     22751|
 -------------------------------------------------------------------------------
  X 7        3627|   4965186|  20225694|      2473|     21805|     26044|
 -------------------------------------------------------------------------------
  X 8        4104|   5114024|  25190880|      2470|     24278|     29431|
 -------------------------------------------------------------------------------
  W 9        2460|   4741068|  30304904|      2284|     26748|     32909|
 -------------------------------------------------------------------------------
  W10        2552|   4702158|  35045972|      2284|     29032|     35797|
 -------------------------------------------------------------------------------
  W11        2924|   4464896|  39748130|      2284|     31316|     38708|
 -------------------------------------------------------------------------------
  W12        3395|   4647477|  44213026|      2284|     33600|     41792|
 -------------------------------------------------------------------------------
  W13        3870|   4782962|  48860503|      2284|     35884|     44983|
 -------------------------------------------------------------------------------
  W14        4326|   4963619|  53643465|      2278|     38168|     48265|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>  58607084


 lprune: l(*,*,*) pruned with nwalk=    1151 nvalwt=     630 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=   11715 nvalwt=   11283 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    6739 nvalwt=    6323 nprune=      68

 lprune: l(*,*,*) pruned with nwalk=    9021 nvalwt=    6510 nprune=      92

 lprune: l(*,*,*) pruned with nwalk=    3521 nvalwt=    2002 nprune=     122

 lprune: l(*,*,*) pruned with nwalk=    7331 nvalwt=    6322 nprune=      93

 lprune: l(*,*,*) pruned with nwalk=    9718 nvalwt=    6327 nprune=      91

 lprune: l(*,*,*) pruned with nwalk=    2751 nvalwt=    1049 nprune=     152



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1        1151|       630|         0|       630|         0|         1|
 -------------------------------------------------------------------------------
  Y 2       11715|    358912|       630|     11283|       630|      1321|
 -------------------------------------------------------------------------------
  X 3        6739|  12877218|    359542|      6323|     11913|     13326|
 -------------------------------------------------------------------------------
  X 4        8766|  12876404|  13236760|      6510|     18236|     20417|
 -------------------------------------------------------------------------------
  X 5        3500|   4191740|  26113164|      2002|     24746|     28270|
 -------------------------------------------------------------------------------
  W 6        7331|  12877990|  30304904|      6322|     26748|     31227|
 -------------------------------------------------------------------------------
  W 7        9523|  12876009|  43182894|      6327|     33070|     38421|
 -------------------------------------------------------------------------------
  W 8        2685|   2548181|  56058903|      1049|     39397|     46267|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>  58607084


 lprune: l(*,*,*) pruned with nwalk=    1151 nvalwt=     630 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=   11715 nvalwt=   11283 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    6739 nvalwt=    6323 nprune=      68

 lprune: l(*,*,*) pruned with nwalk=    9021 nvalwt=    6510 nprune=      92

 lprune: l(*,*,*) pruned with nwalk=    3521 nvalwt=    2002 nprune=     122

 lprune: l(*,*,*) pruned with nwalk=    7331 nvalwt=    6322 nprune=      93

 lprune: l(*,*,*) pruned with nwalk=    9718 nvalwt=    6327 nprune=      91

 lprune: l(*,*,*) pruned with nwalk=    2751 nvalwt=    1049 nprune=     152



                   segmentation summary for type one-external
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1        1151|       630|         0|       630|         0|         1|
 -------------------------------------------------------------------------------
  Y 2       11715|    358912|       630|     11283|       630|      1321|
 -------------------------------------------------------------------------------
  X 3        6739|  12877218|    359542|      6323|     11913|     13326|
 -------------------------------------------------------------------------------
  X 4        8766|  12876404|  13236760|      6510|     18236|     20417|
 -------------------------------------------------------------------------------
  X 5        3500|   4191740|  26113164|      2002|     24746|     28270|
 -------------------------------------------------------------------------------
  W 6        7331|  12877990|  30304904|      6322|     26748|     31227|
 -------------------------------------------------------------------------------
  W 7        9523|  12876009|  43182894|      6327|     33070|     38421|
 -------------------------------------------------------------------------------
  W 8        2685|   2548181|  56058903|      1049|     39397|     46267|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>  58607084


 lprune: l(*,*,*) pruned with nwalk=    1151 nvalwt=     630 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=   11715 nvalwt=   11283 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    3925 nvalwt=    3709 nprune=      96

 lprune: l(*,*,*) pruned with nwalk=    4249 nvalwt=    3709 nprune=      85

 lprune: l(*,*,*) pruned with nwalk=    4859 nvalwt=    3709 nprune=     104

 lprune: l(*,*,*) pruned with nwalk=    6248 nvalwt=    3708 nprune=      94

 lprune: l(*,*,*) pruned with nwalk=    3713 nvalwt=    3425 nprune=     120

 lprune: l(*,*,*) pruned with nwalk=    4306 nvalwt=    3425 nprune=     103

 lprune: l(*,*,*) pruned with nwalk=    5756 nvalwt=    3425 nprune=      93

 lprune: l(*,*,*) pruned with nwalk=    6025 nvalwt=    3423 nprune=      92



                   segmentation summary for type two-external
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1        1151|       630|         0|       630|         0|         1|
 -------------------------------------------------------------------------------
  Y 2       11715|    358912|       630|     11283|       630|      1321|
 -------------------------------------------------------------------------------
  X 3        3925|   7561546|    359542|      3709|     11913|     13326|
 -------------------------------------------------------------------------------
  X 4        4234|   7460750|   7921088|      3709|     15622|     17703|
 -------------------------------------------------------------------------------
  X 5        4739|   7263654|  15381838|      3709|     19331|     22190|
 -------------------------------------------------------------------------------
  X 6        6158|   7659412|  22645492|      3708|     23040|     26844|
 -------------------------------------------------------------------------------
  W 7        3713|   7096319|  30304904|      3425|     26748|     31793|
 -------------------------------------------------------------------------------
  W 8        4291|   6808455|  37401223|      3425|     30173|     35850|
 -------------------------------------------------------------------------------
  W 9        5662|   7150419|  44209678|      3425|     33598|     40123|
 -------------------------------------------------------------------------------
  W10        5935|   7246987|  51360097|      3423|     37023|     44730|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>  58607084


 lprune: l(*,*,*) pruned with nwalk=    1151 nvalwt=     630 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=   11715 nvalwt=   11283 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    6739 nvalwt=    6323 nprune=      68

 lprune: l(*,*,*) pruned with nwalk=    9021 nvalwt=    6510 nprune=      92

 lprune: l(*,*,*) pruned with nwalk=    3521 nvalwt=    2002 nprune=     122

 lprune: l(*,*,*) pruned with nwalk=    7331 nvalwt=    6322 nprune=      93

 lprune: l(*,*,*) pruned with nwalk=    9718 nvalwt=    6327 nprune=      91

 lprune: l(*,*,*) pruned with nwalk=    2751 nvalwt=    1049 nprune=     152



                   segmentation summary for type three-external
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1        1151|       630|         0|       630|         0|         1|
 -------------------------------------------------------------------------------
  Y 2       11715|    358912|       630|     11283|       630|      1321|
 -------------------------------------------------------------------------------
  X 3        6739|  12877218|    359542|      6323|     11913|     13326|
 -------------------------------------------------------------------------------
  X 4        8766|  12876404|  13236760|      6510|     18236|     20417|
 -------------------------------------------------------------------------------
  X 5        3500|   4191740|  26113164|      2002|     24746|     28270|
 -------------------------------------------------------------------------------
  W 6        7331|  12877990|  30304904|      6322|     26748|     31227|
 -------------------------------------------------------------------------------
  W 7        9523|  12876009|  43182894|      6327|     33070|     38421|
 -------------------------------------------------------------------------------
  W 8        2685|   2548181|  56058903|      1049|     39397|     46267|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>  58607084


 lprune: l(*,*,*) pruned with nwalk=    1151 nvalwt=     630 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=   11715 nvalwt=   11283 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    2613 nvalwt=    2473 nprune=     112

 lprune: l(*,*,*) pruned with nwalk=    2673 nvalwt=    2473 nprune=     102

 lprune: l(*,*,*) pruned with nwalk=    2889 nvalwt=    2473 nprune=      89

 lprune: l(*,*,*) pruned with nwalk=    3247 nvalwt=    2473 nprune=     104

 lprune: l(*,*,*) pruned with nwalk=    3717 nvalwt=    2473 nprune=      97

 lprune: l(*,*,*) pruned with nwalk=    4142 nvalwt=    2470 nprune=     118

 lprune: l(*,*,*) pruned with nwalk=    2460 nvalwt=    2284 nprune=     128

 lprune: l(*,*,*) pruned with nwalk=    2552 nvalwt=    2284 nprune=     112

 lprune: l(*,*,*) pruned with nwalk=    3009 nvalwt=    2284 nprune=     122

 lprune: l(*,*,*) pruned with nwalk=    3470 nvalwt=    2284 nprune=     103

 lprune: l(*,*,*) pruned with nwalk=    3960 nvalwt=    2284 nprune=      95

 lprune: l(*,*,*) pruned with nwalk=    4349 nvalwt=    2278 nprune=     120



                   segmentation summary for type four-external
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1        1151|       630|         0|       630|         0|         1|
 -------------------------------------------------------------------------------
  Y 2       11715|    358912|       630|     11283|       630|      1321|
 -------------------------------------------------------------------------------
  X 3        2613|   5045742|    359542|      2473|     11913|     13326|
 -------------------------------------------------------------------------------
  X 4        2673|   5023886|   5405284|      2473|     14386|     16429|
 -------------------------------------------------------------------------------
  X 5        2874|   4954342|  10429170|      2473|     16859|     19562|
 -------------------------------------------------------------------------------
  X 6        3157|   4842182|  15383512|      2473|     19332|     22751|
 -------------------------------------------------------------------------------
  X 7        3627|   4965186|  20225694|      2473|     21805|     26044|
 -------------------------------------------------------------------------------
  X 8        4104|   5114024|  25190880|      2470|     24278|     29431|
 -------------------------------------------------------------------------------
  W 9        2460|   4741068|  30304904|      2284|     26748|     32909|
 -------------------------------------------------------------------------------
  W10        2552|   4702158|  35045972|      2284|     29032|     35797|
 -------------------------------------------------------------------------------
  W11        2924|   4464896|  39748130|      2284|     31316|     38708|
 -------------------------------------------------------------------------------
  W12        3395|   4647477|  44213026|      2284|     33600|     41792|
 -------------------------------------------------------------------------------
  W13        3870|   4782962|  48860503|      2284|     35884|     44983|
 -------------------------------------------------------------------------------
  W14        4326|   4963619|  53643465|      2278|     38168|     48265|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>  58607084

 broadcasting CIVCT     
 broadcasting DATA      
 broadcasting INF       
 broadcasting CNFSPC    
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS
----------------------------------------------------------------------------------------------------
     1  2   2    21      two-ext yy   2X  2 2   11715   11715     358912     358912   11283   11283
     2  3   3    22      two-ext xx   2X  3 3    3925    3925    7561546    7561546    3709    3709
     3  4   3    22      two-ext xx   2X  3 3    4234    3925    7460750    7561546    3709    3709
     4  5   3    22      two-ext xx   2X  3 3    4739    3925    7263654    7561546    3709    3709
     5  6   3    22      two-ext xx   2X  3 3    6158    3925    7659412    7561546    3708    3709
     6  4   4    22      two-ext xx   2X  3 3    4234    4234    7460750    7460750    3709    3709
     7  5   4    22      two-ext xx   2X  3 3    4739    4234    7263654    7460750    3709    3709
     8  6   4    22      two-ext xx   2X  3 3    6158    4234    7659412    7460750    3708    3709
     9  5   5    22      two-ext xx   2X  3 3    4739    4739    7263654    7263654    3709    3709
    10  6   5    22      two-ext xx   2X  3 3    6158    4739    7659412    7263654    3708    3709
    11  6   6    22      two-ext xx   2X  3 3    6158    6158    7659412    7659412    3708    3708
    12  7   7    23      two-ext ww   2X  4 4    3713    3713    7096319    7096319    3425    3425
    13  8   7    23      two-ext ww   2X  4 4    4291    3713    6808455    7096319    3425    3425
    14  9   7    23      two-ext ww   2X  4 4    5662    3713    7150419    7096319    3425    3425
    15 10   7    23      two-ext ww   2X  4 4    5935    3713    7246987    7096319    3423    3425
    16  8   8    23      two-ext ww   2X  4 4    4291    4291    6808455    6808455    3425    3425
    17  9   8    23      two-ext ww   2X  4 4    5662    4291    7150419    6808455    3425    3425
    18 10   8    23      two-ext ww   2X  4 4    5935    4291    7246987    6808455    3423    3425
    19  9   9    23      two-ext ww   2X  4 4    5662    5662    7150419    7150419    3425    3425
    20 10   9    23      two-ext ww   2X  4 4    5935    5662    7246987    7150419    3423    3425
    21 10  10    23      two-ext ww   2X  4 4    5935    5935    7246987    7246987    3423    3423
    22  3   1    24      two-ext xz   2X  3 1    3925    1151    7561546        630    3709     630
    23  4   1    24      two-ext xz   2X  3 1    4234    1151    7460750        630    3709     630
    24  5   1    24      two-ext xz   2X  3 1    4739    1151    7263654        630    3709     630
    25  6   1    24      two-ext xz   2X  3 1    6158    1151    7659412        630    3708     630
    26  7   1    25      two-ext wz   2X  4 1    3713    1151    7096319        630    3425     630
    27  8   1    25      two-ext wz   2X  4 1    4291    1151    6808455        630    3425     630
    28  9   1    25      two-ext wz   2X  4 1    5662    1151    7150419        630    3425     630
    29 10   1    25      two-ext wz   2X  4 1    5935    1151    7246987        630    3423     630
    30  7   3    26      two-ext wx   2X  4 3    3713    3925    7096319    7561546    3425    3709
    31  8   3    26      two-ext wx   2X  4 3    4291    3925    6808455    7561546    3425    3709
    32  9   3    26      two-ext wx   2X  4 3    5662    3925    7150419    7561546    3425    3709
    33 10   3    26      two-ext wx   2X  4 3    5935    3925    7246987    7561546    3423    3709
    34  7   4    26      two-ext wx   2X  4 3    3713    4234    7096319    7460750    3425    3709
    35  8   4    26      two-ext wx   2X  4 3    4291    4234    6808455    7460750    3425    3709
    36  9   4    26      two-ext wx   2X  4 3    5662    4234    7150419    7460750    3425    3709
    37 10   4    26      two-ext wx   2X  4 3    5935    4234    7246987    7460750    3423    3709
    38  7   5    26      two-ext wx   2X  4 3    3713    4739    7096319    7263654    3425    3709
    39  8   5    26      two-ext wx   2X  4 3    4291    4739    6808455    7263654    3425    3709
    40  9   5    26      two-ext wx   2X  4 3    5662    4739    7150419    7263654    3425    3709
    41 10   5    26      two-ext wx   2X  4 3    5935    4739    7246987    7263654    3423    3709
    42  7   6    26      two-ext wx   2X  4 3    3713    6158    7096319    7659412    3425    3708
    43  8   6    26      two-ext wx   2X  4 3    4291    6158    6808455    7659412    3425    3708
    44  9   6    26      two-ext wx   2X  4 3    5662    6158    7150419    7659412    3425    3708
    45 10   6    26      two-ext wx   2X  4 3    5935    6158    7246987    7659412    3423    3708
    46  2   1    11      one-ext yz   1X  2 1   11715    1151     358912        630   11283     630
    47  3   2    13      one-ext yx   1X  3 2    6739   11715   12877218     358912    6323   11283
    48  4   2    13      one-ext yx   1X  3 2    8766   11715   12876404     358912    6510   11283
    49  5   2    13      one-ext yx   1X  3 2    3500   11715    4191740     358912    2002   11283
    50  6   2    14      one-ext yw   1X  4 2    7331   11715   12877990     358912    6322   11283
    51  7   2    14      one-ext yw   1X  4 2    9523   11715   12876009     358912    6327   11283
    52  8   2    14      one-ext yw   1X  4 2    2685   11715    2548181     358912    1049   11283
    53  3   2    31      thr-ext yx   3X  3 2    6739   11715   12877218     358912    6323   11283
    54  4   2    31      thr-ext yx   3X  3 2    8766   11715   12876404     358912    6510   11283
    55  5   2    31      thr-ext yx   3X  3 2    3500   11715    4191740     358912    2002   11283
    56  6   2    32      thr-ext yw   3X  4 2    7331   11715   12877990     358912    6322   11283
    57  7   2    32      thr-ext yw   3X  4 2    9523   11715   12876009     358912    6327   11283
    58  8   2    32      thr-ext yw   3X  4 2    2685   11715    2548181     358912    1049   11283
    59  1   1     1      allint zz    OX  1 1    1151    1151        630        630     630     630
    60  2   2     2      allint yy    OX  2 2   11715   11715     358912     358912   11283   11283
    61  3   3     3      allint xx    OX  3 3    6739    6739   12877218   12877218    6323    6323
    62  4   3     3      allint xx    OX  3 3    8766    6739   12876404   12877218    6510    6323
    63  5   3     3      allint xx    OX  3 3    3500    6739    4191740   12877218    2002    6323
    64  4   4     3      allint xx    OX  3 3    8766    8766   12876404   12876404    6510    6510
    65  5   4     3      allint xx    OX  3 3    3500    8766    4191740   12876404    2002    6510
    66  5   5     3      allint xx    OX  3 3    3500    3500    4191740    4191740    2002    2002
    67  6   6     4      allint ww    OX  4 4    7331    7331   12877990   12877990    6322    6322
    68  7   6     4      allint ww    OX  4 4    9523    7331   12876009   12877990    6327    6322
    69  8   6     4      allint ww    OX  4 4    2685    7331    2548181   12877990    1049    6322
    70  7   7     4      allint ww    OX  4 4    9523    9523   12876009   12876009    6327    6327
    71  8   7     4      allint ww    OX  4 4    2685    9523    2548181   12876009    1049    6327
    72  8   8     4      allint ww    OX  4 4    2685    2685    2548181    2548181    1049    1049
    73  1   1    75      dg-024ext z  DG  1 1    1151    1151        630        630     630     630
    74  2   2    45      4exdg024 y   DG  2 2   11715   11715     358912     358912   11283   11283
    75  3   3    46      4exdg024 x   DG  3 3    2613    2613    5045742    5045742    2473    2473
    76  4   4    46      4exdg024 x   DG  3 3    2673    2673    5023886    5023886    2473    2473
    77  5   5    46      4exdg024 x   DG  3 3    2874    2874    4954342    4954342    2473    2473
    78  6   6    46      4exdg024 x   DG  3 3    3157    3157    4842182    4842182    2473    2473
    79  7   7    46      4exdg024 x   DG  3 3    3627    3627    4965186    4965186    2473    2473
    80  8   8    46      4exdg024 x   DG  3 3    4104    4104    5114024    5114024    2470    2470
    81  9   9    47      4exdg024 w   DG  4 4    2460    2460    4741068    4741068    2284    2284
    82 10  10    47      4exdg024 w   DG  4 4    2552    2552    4702158    4702158    2284    2284
    83 11  11    47      4exdg024 w   DG  4 4    2924    2924    4464896    4464896    2284    2284
    84 12  12    47      4exdg024 w   DG  4 4    3395    3395    4647477    4647477    2284    2284
    85 13  13    47      4exdg024 w   DG  4 4    3870    3870    4782962    4782962    2284    2284
    86 14  14    47      4exdg024 w   DG  4 4    4326    4326    4963619    4963619    2278    2278
----------------------------------------------------------------------------------------------------
 broadcasting TASKLST   
 DIAGEL COUNTS: DG0X,DG2X,DG4X= 1478252 317196 39816
 diagonal elements written to file   4

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:   21154 2x:       0 4x:       0
All internal counts: zz :   63047 yy:       0 xx:       0 ww:       0
One-external counts: yz :       0 yx:       0 yw:       0
Two-external counts: yy :       0 ww:       0 xx:       0 xz:       0 wz:       0 wx:       0
Three-ext.   counts: yx :       0 yw:       0

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 reference space has dimension     273

    root           eigenvalues
    ----           ------------
       1        -155.0180228620

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt= 630

         vector  1 from unit 11 written to unit 49 filename cirefv                                                      
 ufvoutnew: ... writing  recamt= 630

         vector  1 from unit 11 written to unit 49 filename cirefv                                                      

 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=   630)...
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:          58607084
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                    1
 residual norm convergence criteria:               0.001000

          starting bk iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x: 1478252 2x:  317196 4x:   39816
All internal counts: zz :   63047 yy:       0 xx:       0 ww:       0
One-external counts: yz :  467862 yx:       0 yw:       0
Two-external counts: yy :       0 ww:       0 xx:       0 xz:   44221 wz:   42950 wx:       0
Three-ext.   counts: yx :       0 yw:       0

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 630

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -155.0180228620 -5.5067E-14  4.7766E-01  1.3806E+00  1.0000E-03
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   21    0   0.00   0.00   0.00   0.00         0.    0.0000
    2   22    0   0.00   0.00   0.00   0.00         0.    0.0000
    3   22    0   0.00   0.00   0.00   0.00         0.    0.0000
    4   22    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   22    0   0.00   0.00   0.00   0.00         0.    0.0000
    6   22    0   0.00   0.00   0.00   0.00         0.    0.0000
    7   22    0   0.00   0.00   0.00   0.00         0.    0.0000
    8   22    0   0.00   0.00   0.00   0.00         0.    0.0000
    9   22    0   0.00   0.00   0.00   0.00         0.    0.0000
   10   22    0   0.00   0.00   0.00   0.00         0.    0.0000
   11   22    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   23    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   23    0   0.00   0.00   0.00   0.00         0.    0.0000
   14   23    0   0.00   0.00   0.00   0.00         0.    0.0000
   15   23    0   0.00   0.00   0.00   0.00         0.    0.0000
   16   23    0   0.00   0.00   0.00   0.00         0.    0.0000
   17   23    0   0.00   0.00   0.00   0.00         0.    0.0000
   18   23    0   0.00   0.00   0.00   0.00         0.    0.0000
   19   23    0   0.00   0.00   0.00   0.00         0.    0.0000
   20   23    0   0.00   0.00   0.00   0.00         0.    0.0000
   21   23    0   0.00   0.00   0.00   0.00         0.    0.0000
   22   24   10   0.69   0.01   0.00   0.26         0.    0.0082
   23   24   13   0.59   0.01   0.00   0.28         0.    0.0100
   24   24    1   0.51   0.01   0.00   0.21         0.    0.0077
   25   24    4   0.52   0.01   0.00   0.22         0.    0.0067
   26   25   14   0.54   0.01   0.00   0.27         0.    0.0081
   27   25    7   0.51   0.01   0.00   0.25         0.    0.0080
   28   25    9   0.61   0.01   0.00   0.21         0.    0.0065
   29   25    5   0.53   0.01   0.00   0.22         0.    0.0068
   30   26    0   0.00   0.00   0.00   0.00         0.    0.0000
   31   26    0   0.00   0.00   0.00   0.00         0.    0.0000
   32   26    0   0.00   0.00   0.00   0.00         0.    0.0000
   33   26    0   0.00   0.00   0.00   0.00         0.    0.0000
   34   26    0   0.00   0.00   0.00   0.00         0.    0.0000
   35   26    0   0.00   0.00   0.00   0.00         0.    0.0000
   36   26    0   0.00   0.00   0.00   0.00         0.    0.0000
   37   26    0   0.00   0.00   0.00   0.00         0.    0.0000
   38   26    0   0.00   0.00   0.00   0.00         0.    0.0000
   39   26    0   0.00   0.00   0.00   0.00         0.    0.0000
   40   26    0   0.00   0.00   0.00   0.00         0.    0.0000
   41   26    0   0.00   0.00   0.00   0.00         0.    0.0000
   42   26    0   0.00   0.00   0.00   0.00         0.    0.0000
   43   26    0   0.00   0.00   0.00   0.00         0.    0.0000
   44   26    0   0.00   0.00   0.00   0.00         0.    0.0000
   45   26    0   0.00   0.00   0.00   0.00         0.    0.0000
   46   11    6   1.06   0.61   0.00   1.05         0.    0.3822
   47   13    0   0.00   0.00   0.00   0.00         0.    0.0000
   48   13    0   0.00   0.00   0.00   0.00         0.    0.0000
   49   13    0   0.00   0.00   0.00   0.00         0.    0.0000
   50   14    0   0.00   0.00   0.00   0.00         0.    0.0000
   51   14    0   0.00   0.00   0.00   0.00         0.    0.0000
   52   14    0   0.00   0.00   0.00   0.00         0.    0.0000
   53   31    0   0.00   0.00   0.00   0.00         0.    0.0000
   54   31    0   0.00   0.00   0.00   0.00         0.    0.0000
   55   31    0   0.00   0.00   0.00   0.00         0.    0.0000
   56   32    0   0.00   0.00   0.00   0.00         0.    0.0000
   57   32    0   0.00   0.00   0.00   0.00         0.    0.0000
   58   32    0   0.00   0.00   0.00   0.00         0.    0.0000
   59    1   12   0.15   0.12   0.00   0.15         0.    0.0528
   60    2    0   0.00   0.00   0.00   0.00         0.    0.0000
   61    3    0   0.00   0.00   0.00   0.00         0.    0.0000
   62    3    0   0.00   0.00   0.00   0.00         0.    0.0000
   63    3    0   0.00   0.00   0.00   0.00         0.    0.0000
   64    3    0   0.00   0.00   0.00   0.00         0.    0.0000
   65    3    0   0.00   0.00   0.00   0.00         0.    0.0000
   66    3    0   0.00   0.00   0.00   0.00         0.    0.0000
   67    4    0   0.00   0.00   0.00   0.00         0.    0.0000
   68    4    0   0.00   0.00   0.00   0.00         0.    0.0000
   69    4    0   0.00   0.00   0.00   0.00         0.    0.0000
   70    4    0   0.00   0.00   0.00   0.00         0.    0.0000
   71    4    0   0.00   0.00   0.00   0.00         0.    0.0000
   72    4    0   0.00   0.00   0.00   0.00         0.    0.0000
   73   75    0   0.02   0.02   0.00   0.02         0.    0.0167
   74   45    3   0.34   0.19   0.00   0.33         0.    0.0507
   75   46    8   1.98   0.02   0.00   1.84         0.    0.0091
   76   46    2   2.10   0.03   0.00   1.96         0.    0.0099
   77   46   15   2.54   0.03   0.00   2.40         0.    0.0101
   78   46   11   2.66   0.04   0.00   2.52         0.    0.0106
   79   46    0   2.88   0.05   0.00   2.61         0.    0.0110
   80   46   12   2.83   0.05   0.00   2.69         0.    0.0115
   81   47    3   1.91   0.02   0.00   1.67         0.    0.0083
   82   47    1   1.67   0.03   0.00   1.54         0.    0.0092
   83   47    7   2.25   0.04   0.00   2.14         0.    0.0097
   84   47    4   2.22   0.04   0.00   2.09         0.    0.0099
   85   47    5   1.92   0.04   0.00   1.78         0.    0.0102
   86   47   14   2.03   0.05   0.00   1.89         0.    0.0110
================================================================
================ TIMING STATISTICS PER NODE    ================
node# twait    ths   tcin   teig  tvect  tga   ttotal 
  1   0.08   0.04   0.33   0.00   0.00   0.16   3.54
  2   0.80   0.04   0.33   0.00   0.00   0.23   3.64
  3   0.88   0.04   0.33   0.00   0.00   0.10   3.62
  4   0.73   0.04   0.33   0.00   0.00   0.15   3.61
  5   0.25   0.04   0.33   0.00   0.00   0.24   3.66
  6   0.54   0.04   0.33   0.00   0.00   0.25   3.70
  7   1.92   0.04   0.33   0.00   0.00   0.01   3.64
  8   0.22   0.04   0.33   0.00   0.00   0.22   3.70
  9   1.00   0.04   0.33   0.00   0.00   0.10   3.66
 10   2.38   0.04   0.33   0.00   0.00   0.24   3.64
 11   2.30   0.04   0.33   0.00   0.00   0.28   3.61
 12   0.33   0.04   0.33   0.00   0.00   0.09   3.66
 13   0.00   0.04   0.33   0.00   0.00   0.09   3.66
 14   2.39   0.04   0.33   0.00   0.00   0.16   3.62
 15   0.42   0.04   0.33   0.00   0.00   0.24   3.70
 16   0.44   0.04   0.33   0.00   0.00   0.10   3.66
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                    33.0540s 
time spent in multnx:                  28.6040s 
integral transfer time:                 0.0305s 
time spent for loop construction:       1.4460s 
syncronization time in mult:           14.6980s 
total time per CI iteration:           58.3085s 
 ** parallelization degree for mult section:0.6922

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)           0.01        0.00          0.000
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.01        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:            0.02        0.00          0.000
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)         894.31        1.55        576.107
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)        894.31        1.10        811.152
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.43        0.00        110.944
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:         1789.05        2.66        672.905
========================================================


 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -155.0180228620 -5.5067E-14  4.7766E-01  1.3806E+00  1.0000E-03
 
 expansion vectors are not transformed.
 matrix-vector products are not transformed.

    2 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)

 from bk iterations: iconv=   1

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:          58607084
 number of initial trial vectors:                         2
 number of initial matrix-vector products:                1
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                   10
 residual norm convergence criteria:               0.001000

          starting ci iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x: 1478252 2x:  317196 4x:   39816
All internal counts: zz :   63047 yy: 2272259 xx: 3264807 ww: 2639385
One-external counts: yz :  467862 yx: 5844432 yw: 5256661
Two-external counts: yy :  777693 ww:  527776 xx:  884767 xz:   44221 wz:   42950 wx: 1137511
Three-ext.   counts: yx : 4881506 yw: 4629136

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 630

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.94316235    -0.33233232

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -155.4047614913  3.8674E-01  2.0341E-02  2.6469E-01  1.0000E-03
 mr-sdci #  1  2   -151.9031142753  8.7221E+00  0.0000E+00  0.0000E+00  1.0000E-03
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   21    2  11.71   0.38   0.00  11.70         0.    0.4513
    2   22    3  39.11   0.08   0.00  38.92         0.    0.1186
    3   22    5  18.19   0.03   0.00  17.78         0.    0.0401
    4   22   15   7.66   0.01   0.00   7.24         0.    0.0173
    5   22    6   6.80   0.01   0.00   6.38         0.    0.0112
    6   22    4  39.36   0.10   0.00  39.18         0.    0.1193
    7   22   10  11.80   0.02   0.00  11.38         0.    0.0218
    8   22    5   6.99   0.01   0.00   6.33         0.    0.0140
    9   22    1  27.41   0.08   0.00  27.21         0.    0.0848
   10   22   15   9.03   0.02   0.00   8.64         0.    0.0157
   11   22    8  27.82   0.08   0.00  27.64         0.    0.0799
   12   23   12  22.43   0.04   0.00  22.26         0.    0.0577
   13   23    1  11.67   0.01   0.00  11.18         0.    0.0210
   14   23   12   5.63   0.01   0.00   5.25         0.    0.0113
   15   23    3   6.50   0.01   0.00   6.17         0.    0.0113
   16   23    3  20.33   0.04   0.00  20.16         0.    0.0525
   17   23   14   6.18   0.01   0.00   5.61         0.    0.0112
   18   23   14   4.86   0.01   0.00   4.53         0.    0.0059
   19   23    6  20.24   0.05   0.00  20.03         0.    0.0503
   20   23    3   3.40   0.00   0.00   2.81         0.    0.0042
   21   23    7  22.06   0.05   0.00  21.86         0.    0.0533
   22   24   14   0.50   0.01   0.00   0.27         0.    0.0082
   23   24    6   0.59   0.01   0.00   0.31         0.    0.0100
   24   24    3   0.49   0.01   0.00   0.26         0.    0.0077
   25   24    6   0.51   0.01   0.00   0.26         0.    0.0067
   26   25   14   0.53   0.01   0.00   0.30         0.    0.0081
   27   25   14   0.49   0.01   0.00   0.28         0.    0.0080
   28   25    3   0.55   0.01   0.00   0.28         0.    0.0065
   29   25    3   0.52   0.01   0.00   0.29         0.    0.0068
   30   26    5  53.15   0.10   0.00  51.62         0.    0.1350
   31   26    3   7.86   0.01   0.00   7.51         0.    0.0186
   32   26   14   5.02   0.01   0.00   4.64         0.    0.0112
   33   26   14   6.09   0.01   0.00   5.72         0.    0.0112
   34   26    3  26.48   0.05   0.00  26.12         0.    0.0613
   35   26   13  38.93   0.08   0.00  38.41         0.    0.0961
   36   26    6   5.79   0.01   0.00   5.29         0.    0.0112
   37   26   10   4.99   0.01   0.00   4.48         0.    0.0092
   38   26   12  10.16   0.01   0.00   9.63         0.    0.0179
   39   26   14  25.96   0.06   0.00  25.62         0.    0.0627
   40   26   11  36.77   0.08   0.00  36.16         0.    0.0878
   41   26    9   3.45   0.00   0.00   2.91         0.    0.0048
   42   26    0   5.73   0.01   0.00   5.14         0.    0.0116
   43   26   15   8.00   0.01   0.00   7.44         0.    0.0107
   44   26    2  12.00   0.03   0.00  11.39         0.    0.0228
   45   26    4  44.35   0.10   0.00  43.79         0.    0.1059
   46   11    5   1.14   0.61   0.00   1.13         0.    0.3822
   47   13    7  30.22   1.27   0.00  29.77         0.    1.6074
   48   13    6  40.50   1.93   0.00  40.16         0.    1.9524
   49   13   10  12.02   0.76   0.00  11.90         0.    0.6359
   50   14    9  30.99   1.37   0.00  30.66         0.    1.6375
   51   14    8  39.24   1.96   0.00  38.75         0.    1.8853
   52   14    0   5.76   0.41   0.00   5.69         0.    0.2648
   53   31   15  20.80   0.02   0.00  20.45         0.    0.0079
   54   31   12  27.54   0.03   0.00  27.20         0.    0.0081
   55   31    0  10.10   0.01   0.00   9.99         0.    0.0021
   56   32    1  25.22   0.02   0.00  24.87         0.    0.0084
   57   32    2  27.67   0.03   0.00  27.31         0.    0.0074
   58   32   10   6.39   0.01   0.00   6.32         0.    0.0007
   59    1    5   0.15   0.12   0.00   0.15         0.    0.0528
   60    2   10   3.56   1.71   0.00   3.55         0.    1.4781
   61    3    0  28.91   0.58   0.00  28.60         0.    0.6283
   62    3   10  13.32   0.47   0.00  12.35         0.    0.4645
   63    3   14   2.90   0.13   0.00   2.41         0.    0.1106
   64    3   14  26.66   0.67   0.00  26.34         0.    0.5745
   65    3   15   7.97   0.16   0.00   7.45         0.    0.1107
   66    3    7   4.99   0.22   0.00   4.88         0.    0.1458
   67    4    9  23.89   0.55   0.00  23.56         0.    0.5650
   68    4   13  14.97   0.41   0.00  14.01         0.    0.3693
   69    4   12   1.21   0.03   0.00   0.76         0.    0.0152
   70    4   11  23.86   0.70   0.00  23.51         0.    0.5596
   71    4    5   2.83   0.12   0.00   2.38         0.    0.0409
   72    4   15   2.41   0.13   0.00   2.35         0.    0.0730
   73   75    5   0.02   0.02   0.00   0.02         0.    0.0167
   74   45    6   0.37   0.18   0.00   0.36         0.    0.0000
   75   46    2  25.37   0.02   0.00  25.22         0.    0.0000
   76   46    1  25.71   0.03   0.00  25.57         0.    0.0000
   77   46   11  24.58   0.03   0.00  24.45         0.    0.0000
   78   46   10  22.55   0.04   0.00  22.41         0.    0.0000
   79   46    0  24.67   0.04   0.00  24.53         0.    0.0000
   80   46    7  26.90   0.05   0.00  26.76         0.    0.0000
   81   47    9  24.61   0.02   0.00  24.48         0.    0.0000
   82   47   15  24.93   0.03   0.00  24.79         0.    0.0000
   83   47   13  20.95   0.04   0.00  20.82         0.    0.0000
   84   47   12  23.24   0.04   0.00  23.12         0.    0.0000
   85   47    4  25.49   0.04   0.00  25.35         0.    0.0000
   86   47    8  28.17   0.05   0.00  28.03         0.    0.0000
================================================================
================ TIMING STATISTICS PER NODE    ================
node# twait    ths   tcin   teig  tvect  tga   ttotal 
  1  34.04   0.13   0.41   0.00   0.00   0.79 109.90
  2  19.20   0.13   0.41   0.00   0.00   0.79 109.96
  3  32.45   0.13   0.41   0.00   0.00   0.73 109.98
  4   3.96   0.13   0.41   0.00   0.00   1.82 109.96
  5   0.00   0.13   0.41   0.00   0.00   0.59 109.92
  6  26.73   0.13   0.41   0.00   0.00   2.44 110.01
  7  34.40   0.13   0.41   0.00   0.00   1.32 110.02
  8  25.03   0.13   0.41   0.00   0.00   0.54 110.02
  9  13.97   0.13   0.41   0.00   0.00   0.50 109.96
 10  26.25   0.13   0.41   0.00   0.00   0.91 110.04
 11  34.59   0.13   0.41   0.00   0.00   1.47 109.94
 12  23.99   0.13   0.41   0.00   0.00   0.70 109.96
 13  18.98   0.13   0.41   0.00   0.00   1.32 109.94
 14  34.35   0.13   0.41   0.00   0.00   1.06 110.04
 15  30.00   0.13   0.41   0.00   0.00   2.36 110.01
 16  28.40   0.13   0.41   0.00   0.00   1.62 109.90
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                  1360.8387s 
time spent in multnx:                1332.8272s 
integral transfer time:                 0.1662s 
time spent for loop construction:      16.7905s 
syncronization time in mult:          386.3345s 
total time per CI iteration:         1759.5581s 
 ** parallelization degree for mult section:0.7789

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)          24.92        0.03        807.561
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)         24.92        0.03        979.257
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.09        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:           49.93        0.06        876.477
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)        6684.85       10.98        608.551
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)       6684.85        7.91        844.973
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           2.75        0.02        165.439
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:        13372.45       18.91        707.059
========================================================


          starting ci iteration   2

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x: 1478252 2x:  317196 4x:   39816
All internal counts: zz :   63047 yy: 2272259 xx: 3264807 ww: 2639385
One-external counts: yz :  467862 yx: 5844432 yw: 5256661
Two-external counts: yy :  777693 ww:  527776 xx:  884767 xz:   44221 wz:   42950 wx: 1137511
Three-ext.   counts: yx : 4881506 yw: 4629136

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 630

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.94098376    -0.04772597     0.33506983

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1   -155.4195807093  1.4819E-02  1.6847E-03  8.1846E-02  1.0000E-03
 mr-sdci #  2  2   -152.5768041273  6.7369E-01  0.0000E+00  0.0000E+00  1.0000E-03
 mr-sdci #  2  3   -151.9005965104  8.7196E+00  0.0000E+00  2.2380E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   21   14  11.82   0.38   0.00  11.81         0.    0.4513
    2   22    3  40.94   0.08   0.00  40.72         0.    0.1186
    3   22    6  19.66   0.03   0.00  19.27         0.    0.0401
    4   22   12   7.43   0.01   0.00   7.06         0.    0.0173
    5   22    2   5.07   0.01   0.00   4.68         0.    0.0112
    6   22   14  41.15   0.10   0.00  40.95         0.    0.1193
    7   22    7  11.74   0.02   0.00  11.33         0.    0.0218
    8   22   15   6.83   0.01   0.00   6.47         0.    0.0140
    9   22   10  27.22   0.08   0.00  27.01         0.    0.0848
   10   22    4   9.05   0.02   0.00   6.97         0.    0.0157
   11   22    6  27.95   0.08   0.00  27.72         0.    0.0799
   12   23    2  24.72   0.04   0.00  24.53         0.    0.0577
   13   23    5  11.32   0.01   0.00  10.96         0.    0.0210
   14   23    3   7.10   0.01   0.00   6.76         0.    0.0113
   15   23    9   5.42   0.01   0.00   5.07         0.    0.0113
   16   23   10  21.81   0.04   0.00  21.64         0.    0.0525
   17   23    8   6.06   0.01   0.00   5.69         0.    0.0112
   18   23    4   3.81   0.01   0.00   3.45         0.    0.0059
   19   23   13  22.12   0.05   0.00  21.94         0.    0.0503
   20   23    7   4.88   0.00   0.00   4.52         0.    0.0042
   21   23    8  23.80   0.05   0.00  23.61         0.    0.0533
   22   24    6   0.45   0.01   0.00   0.23         0.    0.0082
   23   24   13   0.49   0.01   0.00   0.28         0.    0.0100
   24   24   12   0.41   0.01   0.00   0.22         0.    0.0077
   25   24    8   0.43   0.01   0.00   0.22         0.    0.0067
   26   25   13   0.46   0.01   0.00   0.27         0.    0.0081
   27   25   13   0.46   0.01   0.00   0.26         0.    0.0080
   28   25   12   0.42   0.01   0.00   0.23         0.    0.0065
   29   25   12   0.42   0.01   0.00   0.24         0.    0.0068
   30   26    0  47.77   0.10   0.00  47.36         0.    0.1350
   31   26    6   7.86   0.01   0.00   7.50         0.    0.0186
   32   26    0   6.71   0.01   0.00   6.34         0.    0.0112
   33   26    7   5.02   0.01   0.00   4.65         0.    0.0112
   34   26    2  22.70   0.05   0.00  22.31         0.    0.0613
   35   26    4  34.17   0.08   0.00  33.78         0.    0.0961
   36   26   13   5.71   0.01   0.00   5.35         0.    0.0112
   37   26   11   6.50   0.01   0.00   6.15         0.    0.0092
   38   26    1   9.72   0.01   0.00   9.37         0.    0.0179
   39   26   12  23.12   0.06   0.00  22.77         0.    0.0627
   40   26   11  32.12   0.08   0.00  31.70         0.    0.0878
   41   26    2   4.95   0.00   0.00   4.59         0.    0.0048
   42   26    1   7.22   0.01   0.00   6.86         0.    0.0116
   43   26   11   5.79   0.01   0.00   5.46         0.    0.0107
   44   26    9  11.52   0.03   0.00  11.13         0.    0.0228
   45   26    5  40.86   0.10   0.00  40.44         0.    0.1059
   46   11    6   1.09   0.61   0.00   1.08         0.    0.3822
   47   13   13  28.91   1.28   0.00  28.54         0.    1.6074
   48   13    7  34.81   3.09   0.00  34.44         0.    1.9524
   49   13   11  12.23   2.49   0.00  12.12         0.    0.6359
   50   14   12  26.62   1.37   0.00  26.25         0.    1.6375
   51   14    9  35.98   1.96   0.00  35.61         0.    1.8853
   52   14   14   3.67   0.41   0.00   3.61         0.    0.2648
   53   31   12  22.40   0.02   0.00  22.07         0.    0.0079
   54   31   15  25.12   0.03   0.00  24.74         0.    0.0081
   55   31    3   9.94   0.01   0.00   9.84         0.    0.0021
   56   32   13  22.71   0.02   0.00  22.36         0.    0.0084
   57   32    8  24.84   0.03   0.00  24.46         0.    0.0074
   58   32   10   4.50   0.01   0.00   4.43         0.    0.0007
   59    1    6   0.15   0.12   0.00   0.15         0.    0.0528
   60    2   10   5.25   3.32   0.00   5.25         0.    1.4781
   61    3    1  27.58   0.57   0.00  27.22         0.    0.6283
   62    3    4   9.84   0.47   0.00   8.98         0.    0.4645
   63    3    9   4.54   0.13   0.00   4.08         0.    0.1106
   64    3   15  24.59   0.67   0.00  24.26         0.    0.5745
   65    3    0   5.30   0.15   0.00   4.85         0.    0.1107
   66    3    5   6.24   0.23   0.00   6.14         0.    0.1458
   67    4    5  24.48   0.56   0.00  24.15         0.    0.5650
   68    4    1  14.52   0.41   0.00  13.60         0.    0.3693
   69    4    8   1.16   0.03   0.00   0.71         0.    0.0152
   70    4    3  25.41   0.69   0.00  25.09         0.    0.5596
   71    4   14   2.19   0.12   0.00   1.80         0.    0.0409
   72    4   15   3.69   0.13   0.00   3.63         0.    0.0730
   73   75    4   0.02   0.02   0.00   0.02         0.    0.0167
   74   45    8   0.34   0.18   0.00   0.33         0.    0.0000
   75   46    6  23.78   0.02   0.00  23.64         0.    0.0000
   76   46   10  24.02   0.03   0.00  23.89         0.    0.0000
   77   46    9  25.44   0.03   0.00  25.32         0.    0.0000
   78   46    0  23.50   0.04   0.00  23.38         0.    0.0000
   79   46    4  23.94   0.04   0.00  23.82         0.    0.0000
   80   46    8  24.44   0.05   0.00  24.31         0.    0.0000
   81   47    7  26.75   0.02   0.00  26.63         0.    0.0000
   82   47   11  26.67   0.03   0.00  26.55         0.    0.0000
   83   47   15  22.54   0.04   0.00  22.44         0.    0.0000
   84   47   14  21.98   0.04   0.00  21.86         0.    0.0000
   85   47    1  24.18   0.04   0.00  24.06         0.    0.0000
   86   47    2  25.44   0.05   0.00  25.29         0.    0.0000
================================================================
================ TIMING STATISTICS PER NODE    ================
node# twait    ths   tcin   teig  tvect  tga   ttotal 
  1   0.11   0.14   0.54   0.00   0.00   0.91  84.23
  2   0.18   0.14   0.54   0.00   0.00   1.42  84.23
  3   0.51   0.14   0.54   0.00   0.00   1.03  84.23
  4   0.00   0.14   0.54   0.00   0.00   0.68  84.23
  5   2.56   0.14   0.54   0.00   0.00   3.13  84.23
  6   0.49   0.14   0.54   0.00   0.00   0.85  84.23
  7   2.46   0.14   0.54   0.00   0.00   0.95  84.23
  8   0.19   0.14   0.54   0.00   0.00   1.13  84.23
  9   2.31   0.14   0.54   0.00   0.00   1.20  84.23
 10   0.48   0.14   0.54   0.00   0.00   1.16  84.23
 11   0.58   0.14   0.54   0.00   0.00   0.42  84.23
 12   0.07   0.14   0.54   0.00   0.00   0.91  84.23
 13   2.56   0.14   0.54   0.00   0.00   1.39  84.23
 14   2.53   0.14   0.54   0.00   0.00   1.31  84.23
 15   2.57   0.14   0.54   0.00   0.00   0.55  84.23
 16   0.60   0.14   0.54   0.00   0.00   0.87  84.23
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                  1316.0137s 
time spent in multnx:                1290.8849s 
integral transfer time:                 0.1581s 
time spent for loop construction:      21.2736s 
syncronization time in mult:           18.2144s 
total time per CI iteration:         1347.7258s 
 ** parallelization degree for mult section:0.9863

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)          19.45        0.02        864.847
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)         19.44        0.02       1140.174
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.06        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:           38.95        0.04        974.471
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)        6690.33       10.20        655.741
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)       6690.33        7.65        874.673
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           2.78        0.01        227.650
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:        13383.43       17.86        749.191
========================================================


          starting ci iteration   3

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x: 1478252 2x:  317196 4x:   39816
All internal counts: zz :   63047 yy: 2272259 xx: 3264807 ww: 2639385
One-external counts: yz :  467862 yx: 5844432 yw: 5256661
Two-external counts: yy :  777693 ww:  527776 xx:  884767 xz:   44221 wz:   42950 wx: 1137511
Three-ext.   counts: yx : 4881506 yw: 4629136

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 630

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.94068087    -0.05434674     0.00695773     0.33483955

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1   -155.4209290326  1.3483E-03  2.0729E-04  2.5399E-02  1.0000E-03
 mr-sdci #  3  2   -152.7046666116  1.2786E-01  0.0000E+00  0.0000E+00  1.0000E-03
 mr-sdci #  3  3   -152.0947490456  1.9415E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mr-sdci #  3  4   -151.9003154193  8.7193E+00  0.0000E+00  2.2395E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   21    1  10.84   0.38   0.00  10.83         0.    0.4513
    2   22   15  43.31   0.08   0.00  43.09         0.    0.1186
    3   22    6  17.05   0.03   0.00  16.69         0.    0.0401
    4   22    1   8.60   0.01   0.00   8.24         0.    0.0173
    5   22    2   5.13   0.01   0.00   4.75         0.    0.0112
    6   22   14  43.40   0.10   0.00  43.17         0.    0.1193
    7   22   12  10.86   0.02   0.00  10.50         0.    0.0218
    8   22   13   8.05   0.01   0.00   7.66         0.    0.0140
    9   22    9  29.04   0.08   0.00  28.84         0.    0.0848
   10   22   11   8.38   0.02   0.00   8.01         0.    0.0157
   11   22    6  29.68   0.08   0.00  29.45         0.    0.0799
   12   23    9  23.81   0.04   0.00  23.63         0.    0.0577
   13   23   15  10.71   0.01   0.00  10.36         0.    0.0210
   14   23    9   6.59   0.01   0.00   6.25         0.    0.0113
   15   23   14   5.51   0.01   0.00   5.15         0.    0.0113
   16   23   13  19.18   0.04   0.00  19.01         0.    0.0525
   17   23   10   7.12   0.01   0.00   6.77         0.    0.0112
   18   23    5   3.86   0.01   0.00   3.50         0.    0.0059
   19   23    9  19.15   0.05   0.00  18.97         0.    0.0503
   20   23    7   3.38   0.00   0.00   2.99         0.    0.0042
   21   23    1  21.49   0.05   0.00  21.30         0.    0.0533
   22   24   15   0.46   0.01   0.00   0.24         0.    0.0082
   23   24    8   0.47   0.01   0.00   0.27         0.    0.0100
   24   24   10   0.40   0.01   0.00   0.21         0.    0.0077
   25   24    3   0.43   0.01   0.00   0.23         0.    0.0067
   26   25   15   0.47   0.01   0.00   0.27         0.    0.0081
   27   25    8   0.43   0.01   0.00   0.25         0.    0.0080
   28   25    8   0.42   0.01   0.00   0.22         0.    0.0065
   29   25    9   0.45   0.01   0.00   0.25         0.    0.0068
   30   26    0  51.37   0.10   0.00  50.96         0.    0.1350
   31   26    4   8.95   0.01   0.00   8.61         0.    0.0186
   32   26    7   6.19   0.01   0.00   5.82         0.    0.0112
   33   26   12   5.11   0.01   0.00   4.74         0.    0.0112
   34   26    0  22.74   0.05   0.00  22.36         0.    0.0613
   35   26   12  37.76   0.08   0.00  37.35         0.    0.0961
   36   26    0   6.82   0.01   0.00   6.43         0.    0.0112
   37   26    6   5.94   0.01   0.00   5.59         0.    0.0092
   38   26    2   9.10   0.01   0.00   8.73         0.    0.0179
   39   26   14  24.49   0.06   0.00  24.12         0.    0.0627
   40   26    2  33.54   0.08   0.00  33.12         0.    0.0878
   41   26    9   3.35   0.00   0.00   2.96         0.    0.0048
   42   26   12   6.58   0.01   0.00   6.21         0.    0.0116
   43   26    8   6.97   0.01   0.00   6.59         0.    0.0107
   44   26    3  10.81   0.03   0.00  10.45         0.    0.0228
   45   26    5  42.77   0.10   0.00  42.33         0.    0.1059
   46   11   10   1.06   0.61   0.00   1.05         0.    0.3822
   47   13   11  29.18   1.28   0.00  28.80         0.    1.6074
   48   13    1  39.25   1.93   0.00  38.86         0.    1.9524
   49   13    2  10.55   0.76   0.00  10.44         0.    0.6359
   50   14   13  29.41   1.37   0.00  29.05         0.    1.6375
   51   14    3  38.39   1.96   0.00  38.00         0.    1.8853
   52   14    1   3.87   0.41   0.00   3.81         0.    0.2648
   53   31    7  20.38   0.02   0.00  20.03         0.    0.0079
   54   31    4  26.15   0.03   0.00  25.81         0.    0.0081
   55   31   14   9.33   0.01   0.00   9.22         0.    0.0021
   56   32   10  22.57   0.02   0.00  22.24         0.    0.0084
   57   32    8  25.84   0.03   0.00  25.51         0.    0.0074
   58   32    6   4.54   0.01   0.00   4.47         0.    0.0007
   59    1    0   0.15   0.12   0.00   0.15         0.    0.0528
   60    2   15   3.78   1.71   0.00   3.77         0.    1.4781
   61    3    8  27.21   0.58   0.00  26.86         0.    0.6283
   62    3    5  11.20   0.48   0.00  10.28         0.    0.4645
   63    3    3   2.91   0.13   0.00   2.42         0.    0.1106
   64    3   11  27.44   0.67   0.00  27.11         0.    0.5745
   65    3   11   6.03   0.16   0.00   5.61         0.    0.1107
   66    3    3   5.32   0.22   0.00   5.22         0.    0.1458
   67    4   13  23.78   0.55   0.00  23.45         0.    0.5650
   68    4   11  12.37   0.41   0.00  11.50         0.    0.3693
   69    4    0   1.21   0.03   0.00   0.78         0.    0.0152
   70    4    7  27.02   0.70   0.00  26.68         0.    0.5596
   71    4   13   2.27   0.12   0.00   1.89         0.    0.0409
   72    4    4   2.24   0.13   0.00   2.15         0.    0.0730
   73   75    4   0.02   0.02   0.00   0.02         0.    0.0167
   74   45   15   0.35   0.18   0.00   0.34         0.    0.0000
   75   46    5  25.20   0.02   0.00  25.08         0.    0.0000
   76   46   12  23.33   0.03   0.00  23.20         0.    0.0000
   77   46    7  25.29   0.03   0.00  25.15         0.    0.0000
   78   46   15  23.38   0.04   0.00  23.24         0.    0.0000
   79   46    3  24.45   0.04   0.00  24.32         0.    0.0000
   80   46    6  26.36   0.05   0.00  26.22         0.    0.0000
   81   47   10  24.08   0.02   0.00  23.94         0.    0.0000
   82   47    4  25.95   0.03   0.00  25.82         0.    0.0000
   83   47    4  18.90   0.04   0.00  18.79         0.    0.0000
   84   47    8  21.04   0.04   0.00  20.93         0.    0.0000
   85   47    2  24.72   0.04   0.00  24.61         0.    0.0000
   86   47   10  27.19   0.05   0.00  27.08         0.    0.0000
================================================================
================ TIMING STATISTICS PER NODE    ================
node# twait    ths   tcin   teig  tvect  tga   ttotal 
  1   1.76   0.15   0.65   0.00   0.00   1.13  85.04
  2   0.00   0.15   0.65   0.00   0.00   0.70  85.04
  3   1.01   0.15   0.65   0.00   0.00   0.99  85.04
  4   1.73   0.15   0.65   0.00   0.00   1.14  85.04
  5   1.86   0.15   0.65   0.00   0.00   0.71  85.04
  6   1.02   0.15   0.65   0.00   0.00   1.23  85.04
  7   0.48   0.15   0.65   0.00   0.00   0.81  85.04
  8   1.78   0.15   0.65   0.00   0.00   1.13  85.04
  9   1.68   0.15   0.65   0.00   0.00   1.23  85.04
 10   1.67   0.15   0.65   0.00   0.00   1.04  85.04
 11   1.64   0.15   0.65   0.00   0.00   0.80  85.04
 12   0.65   0.15   0.65   0.00   0.00   1.61  85.04
 13   0.43   0.15   0.65   0.00   0.00   1.14  85.04
 14   1.35   0.15   0.65   0.00   0.00   1.15  85.04
 15   1.32   0.15   0.65   0.00   0.00   0.74  85.04
 16   1.60   0.15   0.65   0.00   0.00   0.80  85.04
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                  1324.9177s 
time spent in multnx:                1301.3660s 
integral transfer time:                 0.1570s 
time spent for loop construction:      16.7889s 
syncronization time in mult:           19.9877s 
total time per CI iteration:         1360.6946s 
 ** parallelization degree for mult section:0.9851

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)           0.01        0.00          0.000
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.06        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:            0.07        0.00          0.000
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)        6709.76        8.50        789.564
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)       6709.76        7.85        854.801
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           2.78        0.01        253.034
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:        13422.31       16.36        820.507
========================================================


          starting ci iteration   4

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x: 1478252 2x:  317196 4x:   39816
All internal counts: zz :   63047 yy: 2272259 xx: 3264807 ww: 2639385
One-external counts: yz :  467862 yx: 5844432 yw: 5256661
Two-external counts: yy :  777693 ww:  527776 xx:  884767 xz:   44221 wz:   42950 wx: 1137511
Three-ext.   counts: yx : 4881506 yw: 4629136

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 630

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1    -0.94015215    -0.10032633    -0.09232353     0.22851590     0.21285068

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1   -155.4211036776  1.7464E-04  4.9481E-05  1.1388E-02  1.0000E-03
 mr-sdci #  4  2   -153.6539933805  9.4933E-01  0.0000E+00  0.0000E+00  1.0000E-03
 mr-sdci #  4  3   -152.2318265455  1.3708E-01  0.0000E+00  1.8592E+00  1.0000E-04
 mr-sdci #  4  4   -152.0074850868  1.0717E-01  0.0000E+00  1.9515E+00  1.0000E-04
 mr-sdci #  4  5   -151.4513252328  8.2703E+00  0.0000E+00  1.1749E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   21   11  11.50   0.38   0.00  11.49         0.    0.4513
    2   22    4  42.97   0.08   0.00  42.76         0.    0.1186
    3   22    5  19.62   0.03   0.00  17.52         0.    0.0401
    4   22    7   8.89   0.01   0.00   8.52         0.    0.0173
    5   22    3   5.17   0.01   0.00   4.79         0.    0.0112
    6   22    2  44.00   0.10   0.00  43.79         0.    0.1193
    7   22    7  11.54   0.02   0.00  11.16         0.    0.0218
    8   22    6   8.45   0.01   0.00   8.08         0.    0.0140
    9   22    8  28.59   0.08   0.00  28.39         0.    0.0848
   10   22   11   8.81   0.02   0.00   8.43         0.    0.0157
   11   22    5  29.74   0.08   0.01  29.51         0.    0.0799
   12   23    7  26.50   0.04   0.00  26.32         0.    0.0577
   13   23   15  11.17   0.01   0.00  10.83         0.    0.0210
   14   23   12   6.95   0.01   0.00   6.61         0.    0.0113
   15   23    9   6.94   0.01   0.00   6.59         0.    0.0113
   16   23    8  21.34   0.04   0.00  21.17         0.    0.0525
   17   23    0   7.53   0.01   0.00   7.19         0.    0.0112
   18   23    7   3.83   0.01   0.00   3.48         0.    0.0059
   19   23   14  21.58   0.05   0.00  21.39         0.    0.0503
   20   23    6   3.34   0.00   0.00   2.94         0.    0.0042
   21   23    0  23.33   0.06   0.00  23.14         0.    0.0533
   22   24   10   0.44   0.01   0.00   0.23         0.    0.0082
   23   24   10   0.47   0.01   0.00   0.27         0.    0.0100
   24   24   10   0.42   0.01   0.00   0.22         0.    0.0077
   25   24    9   0.43   0.01   0.00   0.23         0.    0.0067
   26   25    9   0.45   0.01   0.00   0.25         0.    0.0081
   27   25   14   0.45   0.01   0.00   0.26         0.    0.0080
   28   25   15   0.41   0.01   0.00   0.21         0.    0.0065
   29   25   15   0.42   0.01   0.00   0.23         0.    0.0068
   30   26    0  51.84   0.10   0.00  51.43         0.    0.1350
   31   26    1   9.38   0.01   0.00   9.02         0.    0.0186
   32   26    8   6.53   0.01   0.00   6.15         0.    0.0112
   33   26    4   5.10   0.01   0.00   4.75         0.    0.0112
   34   26    2  27.23   0.05   0.00  26.83         0.    0.0613
   35   26   11  35.62   0.08   0.00  35.23         0.    0.0961
   36   26    5   7.20   0.01   0.00   6.83         0.    0.0112
   37   26   14   6.51   0.01   0.00   6.12         0.    0.0092
   38   26    2   9.60   0.01   0.00   9.24         0.    0.0179
   39   26    1  27.64   0.06   0.00  27.29         0.    0.0627
   40   26    3  33.09   0.08   0.00  32.70         0.    0.0878
   41   26   11   3.36   0.00   0.00   2.99         0.    0.0048
   42   26   15   7.01   0.01   0.00   6.63         0.    0.0116
   43   26   13   7.38   0.01   0.00   7.00         0.    0.0107
   44   26    6  11.24   0.03   0.00  10.87         0.    0.0228
   45   26   15  42.22   0.10   0.00  41.81         0.    0.1059
   46   11   14   1.11   0.61   0.00   1.10         0.    0.3822
   47   13    9  29.97   1.28   0.00  29.60         0.    1.6074
   48   13    7  35.84   3.02   0.00  35.47         0.    1.9524
   49   13    4  12.60   2.36   0.00  12.49         0.    0.6359
   50   14    1  31.60   1.37   0.00  31.22         0.    1.6375
   51   14    6  39.28   1.96   0.00  38.92         0.    1.8853
   52   14    0   3.98   0.41   0.00   3.91         0.    0.2648
   53   31   13  22.31   0.02   0.00  21.99         0.    0.0079
   54   31   10  27.67   0.03   0.00  27.34         0.    0.0081
   55   31    3   9.68   0.01   0.00   9.58         0.    0.0021
   56   32   12  24.73   0.02   0.00  24.39         0.    0.0084
   57   32    8  27.03   0.03   0.00  26.73         0.    0.0074
   58   32    1   4.63   0.01   0.00   4.54         0.    0.0007
   59    1   14   0.15   0.12   0.00   0.15         0.    0.0528
   60    2   13   3.74   1.73   0.00   3.73         0.    1.4781
   61    3   12  26.94   0.58   0.00  26.59         0.    0.6283
   62    3    3  11.42   0.47   0.00  10.59         0.    0.4645
   63    3    5   2.73   0.13   0.00   2.28         0.    0.1106
   64    3   10  25.65   0.66   0.00  25.29         0.    0.5745
   65    3   10   7.23   0.16   0.00   6.79         0.    0.1107
   66    3    2   4.84   0.22   0.00   4.73         0.    0.1458
   67    4    6  24.07   0.55   0.00  23.76         0.    0.5650
   68    4    1  12.74   0.41   0.00  12.10         0.    0.3693
   69    4   15   1.06   0.03   0.00   0.65         0.    0.0152
   70    4   13  25.64   0.70   0.00  25.28         0.    0.5596
   71    4   12   2.42   0.12   0.00   2.02         0.    0.0409
   72    4    8   2.27   0.13   0.00   2.21         0.    0.0730
   73   75   14   0.02   0.02   0.00   0.02         0.    0.0167
   74   45    9   0.35   0.18   0.00   0.34         0.    0.0000
   75   46    5  26.77   0.02   0.00  26.64         0.    0.0000
   76   46    4  25.76   0.03   0.00  25.63         0.    0.0000
   77   46   14  26.00   0.03   0.00  25.87         0.    0.0000
   78   46   15  23.33   0.04   0.00  23.21         0.    0.0000
   79   46    3  26.66   0.04   0.00  26.53         0.    0.0000
   80   46   13  27.67   0.05   0.00  27.53         0.    0.0000
   81   47   11  27.28   0.02   0.00  27.17         0.    0.0000
   82   47   12  24.81   0.03   0.00  24.67         0.    0.0000
   83   47    9  21.41   0.04   0.00  21.31         0.    0.0000
   84   47   10  23.77   0.04   0.00  23.65         0.    0.0000
   85   47    9  26.08   0.04   0.00  25.96         0.    0.0000
   86   47   14  29.72   0.05   0.00  29.57         0.    0.0000
================================================================
================ TIMING STATISTICS PER NODE    ================
node# twait    ths   tcin   teig  tvect  tga   ttotal 
  1   0.06   0.20   0.85   0.00   0.00   0.71  87.97
  2   0.74   0.21   0.85   0.00   0.00   1.29  87.97
  3   1.06   0.21   0.85   0.00   0.00   0.77  87.97
  4   0.71   0.21   0.85   0.00   0.00   1.22  87.97
  5   0.31   0.21   0.85   0.00   0.00   0.56  87.97
  6   0.67   0.21   0.85   0.00   0.00   2.81  87.97
  7   0.35   0.21   0.85   0.00   0.00   1.28  87.97
  8   0.13   0.21   0.85   0.00   0.00   1.17  87.97
  9   0.98   0.21   0.85   0.00   0.00   0.77  87.97
 10   1.10   0.21   0.85   0.00   0.00   0.96  87.97
 11   1.10   0.21   0.85   0.00   0.00   1.30  87.97
 12   0.17   0.21   0.85   0.00   0.00   0.89  87.97
 13   0.88   0.21   0.85   0.00   0.00   1.10  87.97
 14   0.00   0.21   0.85   0.00   0.00   0.87  87.97
 15   1.21   0.21   0.85   0.00   0.00   0.75  87.97
 16   1.11   0.21   0.85   0.00   0.00   1.44  87.97
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                  1377.1833s 
time spent in multnx:                1352.3997s 
integral transfer time:                 0.1574s 
time spent for loop construction:      19.4818s 
syncronization time in mult:           10.5895s 
total time per CI iteration:         1407.5407s 
 ** parallelization degree for mult section:0.9924

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)          22.18        0.03        846.237
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)         22.18        0.02       1076.365
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.05        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:           44.41        0.05        945.245
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)        6687.59        9.99        669.409
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)       6687.59        7.84        853.099
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           2.79        0.01        267.229
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:        13377.97       17.84        749.890
========================================================


          starting ci iteration   5

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x: 1478252 2x:  317196 4x:   39816
All internal counts: zz :   63047 yy: 2272259 xx: 3264807 ww: 2639385
One-external counts: yz :  467862 yx: 5844432 yw: 5256661
Two-external counts: yy :  777693 ww:  527776 xx:  884767 xz:   44221 wz:   42950 wx: 1137511
Three-ext.   counts: yx : 4881506 yw: 4629136

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 630

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.93968853     0.09982043    -0.06795985     0.17363614    -0.14799850    -0.22438747

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1   -155.4211537376  5.0060E-05  1.5654E-05  7.3159E-03  1.0000E-03
 mr-sdci #  5  2   -154.5065875331  8.5259E-01  0.0000E+00  0.0000E+00  1.0000E-03
 mr-sdci #  5  3   -152.3362381504  1.0441E-01  0.0000E+00  1.6150E+00  1.0000E-04
 mr-sdci #  5  4   -152.0199311516  1.2446E-02  0.0000E+00  1.9809E+00  1.0000E-04
 mr-sdci #  5  5   -151.9551638242  5.0384E-01  0.0000E+00  1.1292E+00  1.0000E-04
 mr-sdci #  5  6   -151.4229694353  8.2420E+00  0.0000E+00  2.6568E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   21    9  11.68   0.38   0.00  11.67         0.    0.4513
    2   22    4  42.05   0.08   0.00  41.83         0.    0.1186
    3   22    7  20.07   0.03   0.00  19.66         0.    0.0401
    4   22    8   9.00   0.01   0.00   8.64         0.    0.0173
    5   22    1   5.13   0.01   0.00   4.76         0.    0.0112
    6   22    2  42.80   0.10   0.00  42.57         0.    0.1193
    7   22    8  11.61   0.02   0.00  11.24         0.    0.0218
    8   22    0   8.53   0.01   0.00   8.15         0.    0.0140
    9   22   13  28.64   0.08   0.00  28.44         0.    0.0848
   10   22   12   8.94   0.02   0.00   8.56         0.    0.0157
   11   22    6  29.38   0.08   0.00  29.16         0.    0.0799
   12   23   13  24.38   0.04   0.00  24.20         0.    0.0577
   13   23    2  11.24   0.01   0.00  10.90         0.    0.0210
   14   23    5   7.12   0.01   0.00   6.77         0.    0.0113
   15   23   15   7.20   0.01   0.00   6.84         0.    0.0113
   16   23    6  22.15   0.04   0.00  21.97         0.    0.0525
   17   23   10   7.71   0.01   0.00   7.35         0.    0.0112
   18   23   10   3.81   0.01   0.00   3.46         0.    0.0059
   19   23   10  22.11   0.05   0.00  21.91         0.    0.0503
   20   23   13   3.26   0.00   0.00   2.89         0.    0.0042
   21   23   14  24.06   0.05   0.00  23.87         0.    0.0533
   22   24   15   0.44   0.01   0.00   0.23         0.    0.0082
   23   24    6   0.47   0.01   0.00   0.27         0.    0.0100
   24   24    6   0.43   0.01   0.00   0.23         0.    0.0077
   25   24    0   0.44   0.01   0.00   0.22         0.    0.0067
   26   25    0   0.44   0.01   0.00   0.25         0.    0.0081
   27   25    6   0.43   0.01   0.00   0.25         0.    0.0080
   28   25    5   0.37   0.01   0.00   0.20         0.    0.0065
   29   25   15   0.42   0.01   0.00   0.22         0.    0.0068
   30   26    0  51.23   0.10   0.00  50.81         0.    0.1350
   31   26    3   9.63   0.01   0.00   9.27         0.    0.0186
   32   26    6   6.77   0.01   0.00   6.38         0.    0.0112
   33   26   11   5.13   0.01   0.00   4.78         0.    0.0112
   34   26   14  24.95   0.05   0.00  24.57         0.    0.0613
   35   26    8  36.52   0.08   0.00  36.12         0.    0.0961
   36   26    9   7.35   0.01   0.00   6.98         0.    0.0112
   37   26    4   6.58   0.01   0.00   4.54         0.    0.0092
   38   26   11   9.78   0.01   0.00   9.41         0.    0.0179
   39   26   12  24.45   0.06   0.00  24.06         0.    0.0627
   40   26    3  34.51   0.08   0.00  34.10         0.    0.0878
   41   26    8   3.32   0.00   0.00   2.96         0.    0.0048
   42   26   13   7.16   0.01   0.00   6.77         0.    0.0116
   43   26   14   7.52   0.01   0.00   7.17         0.    0.0107
   44   26    4  11.42   0.02   0.00  11.05         0.    0.0228
   45   26    1  41.94   0.10   0.00  41.52         0.    0.1059
   46   11    0   1.09   0.61   0.00   1.08         0.    0.3822
   47   13   11  27.23   1.28   0.00  26.86         0.    1.6074
   48   13    9  37.47   1.93   0.00  37.09         0.    1.9524
   49   13    3  12.24   2.55   0.00  12.13         0.    0.6359
   50   14   10  28.30   1.37   0.00  27.92         0.    1.6375
   51   14    5  36.36   1.97   0.00  35.98         0.    1.8853
   52   14   14   3.98   0.41   0.00   3.91         0.    0.2648
   53   31   13  23.01   0.02   0.00  22.66         0.    0.0079
   54   31   15  26.61   0.03   0.00  26.23         0.    0.0081
   55   31    1   9.99   0.01   0.00   9.87         0.    0.0021
   56   32    4  25.86   0.02   0.00  25.51         0.    0.0084
   57   32   15  27.45   0.03   0.00  27.11         0.    0.0074
   58   32    7   4.64   0.01   0.00   4.58         0.    0.0007
   59    1    3   0.15   0.12   0.00   0.15         0.    0.0528
   60    2    9   3.65   1.72   0.00   3.64         0.    1.4781
   61    3   11  29.38   0.58   0.00  29.05         0.    0.6283
   62    3    5  11.37   0.48   0.00  10.73         0.    0.4645
   63    3   12   2.70   0.13   0.00   2.24         0.    0.1106
   64    3    8  25.99   0.66   0.00  25.67         0.    0.5745
   65    3    7   7.10   0.16   0.00   6.65         0.    0.1107
   66    3    2   4.39   0.22   0.00   4.29         0.    0.1458
   67    4    2  27.48   0.54   0.00  27.14         0.    0.5650
   68    4   11  15.10   0.41   0.00  14.42         0.    0.3693
   69    4   15   1.16   0.03   0.00   0.75         0.    0.0152
   70    4    9  26.61   2.58   0.00  26.28         0.    0.5596
   71    4    3   2.36   0.12   0.00   1.95         0.    0.0409
   72    4    5   2.06   0.13   0.00   1.99         0.    0.0730
   73   75    6   0.02   0.02   0.00   0.02         0.    0.0167
   74   45    0   0.35   0.18   0.00   0.34         0.    0.0000
   75   46    7  27.74   0.02   0.00  27.60         0.    0.0000
   76   46    5  28.59   0.03   0.00  28.47         0.    0.0000
   77   46    3  26.82   0.03   0.00  26.70         0.    0.0000
   78   46    0  23.81   0.04   0.00  23.69         0.    0.0000
   79   46   10  24.86   0.04   0.00  24.73         0.    0.0000
   80   46   14  26.36   0.05   0.00  26.21         0.    0.0000
   81   47   12  25.23   0.02   0.00  25.12         0.    0.0000
   82   47    1  29.25   0.03   0.00  29.13         0.    0.0000
   83   47   15  22.59   0.04   0.00  22.48         0.    0.0000
   84   47   12  24.56   0.04   0.00  24.45         0.    0.0000
   85   47    6  25.96   0.04   0.00  25.84         0.    0.0000
   86   47    7  27.67   0.05   0.00  27.53         0.    0.0000
================================================================
================ TIMING STATISTICS PER NODE    ================
node# twait    ths   tcin   teig  tvect  tga   ttotal 
  1   1.34   0.23   1.06   0.00   0.00   0.93  90.13
  2   0.91   0.23   1.06   0.00   0.00   0.72  90.13
  3   1.33   0.23   1.06   0.00   0.00   0.69  90.13
  4   1.52   0.23   1.06   0.00   0.00   0.98  90.13
  5   1.32   0.23   1.06   0.00   0.00   2.58  90.13
  6   1.35   0.23   1.06   0.00   0.00   1.21  90.13
  7   1.62   0.23   1.06   0.00   0.00   1.04  90.13
  8   0.00   0.23   1.06   0.00   0.00   0.81  90.13
  9   0.78   0.23   1.06   0.00   0.00   1.26  90.13
 10   0.47   0.23   1.06   0.00   0.00   0.77  90.13
 11   0.44   0.23   1.06   0.00   0.00   0.97  90.13
 12   0.60   0.23   1.06   0.00   0.00   1.47  90.13
 13   1.33   0.23   1.06   0.00   0.00   1.00  90.13
 14   0.78   0.23   1.06   0.00   0.00   1.04  90.13
 15   0.35   0.23   1.06   0.00   0.00   0.79  90.13
 16   1.35   0.23   1.06   0.00   0.00   1.42  90.13
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                  1380.0709s 
time spent in multnx:                1355.4484s 
integral transfer time:                 0.1551s 
time spent for loop construction:      20.4541s 
syncronization time in mult:           15.4930s 
total time per CI iteration:         1442.0636s 
 ** parallelization degree for mult section:0.9889

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)          24.94        0.03        836.074
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)         24.93        0.02       1039.924
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.08        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:           49.95        0.05        922.901
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)        6684.84        9.88        676.450
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)       6684.84        7.73        865.180
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           2.76        0.01        263.019
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:        13372.43       17.62        758.967
========================================================


          starting ci iteration   6

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x: 1478252 2x:  317196 4x:   39816
All internal counts: zz :   63047 yy: 2272259 xx: 3264807 ww: 2639385
One-external counts: yz :  467862 yx: 5844432 yw: 5256661
Two-external counts: yy :  777693 ww:  527776 xx:  884767 xz:   44221 wz:   42950 wx: 1137511
Three-ext.   counts: yx : 4881506 yw: 4629136

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 630

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1    -0.93969639    -0.00260704

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  6  1   -155.4211663869  1.2649E-05  5.1860E-06  3.7141E-03  1.0000E-03
 mr-sdci #  6  2   -152.7286501997 -1.7779E+00  0.0000E+00  6.0445E+00  1.0000E-03
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   21    6  11.80   0.38   0.00  11.79         0.    0.4513
    2   22   14  42.91   0.08   0.00  42.67         0.    0.1186
    3   22   10  18.14   0.03   0.00  17.79         0.    0.0401
    4   22   11   9.24   0.01   0.00   8.86         0.    0.0173
    5   22   12   7.00   0.01   0.00   4.73         0.    0.0112
    6   22   12  45.05   0.10   0.00  44.83         0.    0.1193
    7   22    8  11.67   0.02   0.00  11.32         0.    0.0218
    8   22    8   8.66   0.01   0.00   8.29         0.    0.0140
    9   22    2  28.96   0.08   0.00  28.74         0.    0.0848
   10   22    6   9.29   0.02   0.00   8.92         0.    0.0157
   11   22    5  30.41   0.09   0.00  30.17         0.    0.0799
   12   23    4  23.97   0.04   0.00  23.80         0.    0.0577
   13   23   15  11.50   0.01   0.00  11.15         0.    0.0210
   14   23    2   7.42   0.01   0.00   7.05         0.    0.0113
   15   23    3   7.35   0.01   0.00   7.00         0.    0.0113
   16   23    2  21.35   0.04   0.00  20.10         0.    0.0525
   17   23    0   7.85   0.01   0.00   7.51         0.    0.0112
   18   23    6   3.86   0.01   0.00   3.50         0.    0.0059
   19   23    5  20.10   0.05   0.00  19.94         0.    0.0503
   20   23    8   3.28   0.00   0.00   2.88         0.    0.0042
   21   23    0  22.99   0.06   0.00  22.80         0.    0.0533
   22   24   14   0.43   0.01   0.00   0.23         0.    0.0082
   23   24   14   0.47   0.01   0.00   0.27         0.    0.0100
   24   24    2   0.44   0.01   0.00   0.24         0.    0.0077
   25   24    5   0.44   0.01   0.00   0.23         0.    0.0067
   26   25   10   0.46   0.01   0.00   0.27         0.    0.0081
   27   25   14   0.44   0.01   0.00   0.26         0.    0.0080
   28   25   12   0.41   0.01   0.00   0.22         0.    0.0065
   29   25    4   0.44   0.01   0.00   0.25         0.    0.0068
   30   26    0  51.48   0.10   0.00  51.06         0.    0.1350
   31   26    4   9.69   0.01   0.00   9.34         0.    0.0186
   32   26   10   7.01   0.01   0.00   6.62         0.    0.0112
   33   26    9   7.00   0.01   0.00   4.72         0.    0.0112
   34   26    1  24.42   0.05   0.00  24.04         0.    0.0613
   35   26    6  37.24   0.08   0.00  36.84         0.    0.0961
   36   26    7   7.56   0.01   0.00   7.20         0.    0.0112
   37   26    5   6.79   0.01   0.00   6.40         0.    0.0092
   38   26    9   8.03   0.01   0.00   7.68         0.    0.0179
   39   26   12  25.19   0.06   0.00  24.83         0.    0.0627
   40   26   10  33.53   0.08   0.00  33.11         0.    0.0878
   41   26   13   3.33   0.00   0.00   2.97         0.    0.0048
   42   26    1   7.38   0.01   0.00   7.01         0.    0.0116
   43   26   13   7.72   0.01   0.00   7.36         0.    0.0107
   44   26    1  11.64   0.03   0.00  11.27         0.    0.0228
   45   26   15  42.60   0.10   0.00  42.19         0.    0.1059
   46   11    2   1.10   0.61   0.00   1.09         0.    0.3822
   47   13    9  31.22   1.28   0.00  30.88         0.    1.6074
   48   13    8  38.30   1.93   0.00  37.94         0.    1.9524
   49   13    9  11.56   0.76   0.00  11.45         0.    0.6359
   50   14   11  29.87   1.37   0.00  29.47         0.    1.6375
   51   14    1  40.42   3.24   0.00  40.02         0.    1.8853
   52   14    4   3.76   0.41   0.00   3.69         0.    0.2648
   53   31    7  22.21   0.02   0.00  21.86         0.    0.0079
   54   31    2  26.34   0.03   0.00  25.98         0.    0.0081
   55   31   12   8.21   0.01   0.00   8.10         0.    0.0021
   56   32    6  23.80   0.02   0.00  23.47         0.    0.0084
   57   32    7  26.78   0.03   0.00  26.43         0.    0.0074
   58   32   15   4.78   0.01   0.00   4.72         0.    0.0007
   59    1    5   0.15   0.12   0.00   0.15         0.    0.0528
   60    2    0   3.54   1.71   0.00   3.53         0.    1.4781
   61    3   13  28.39   0.57   0.00  28.05         0.    0.6283
   62    3   14  11.74   0.48   0.00  10.92         0.    0.4645
   63    3    7   3.11   0.13   0.00   2.68         0.    0.1106
   64    3    5  27.79   0.68   0.00  27.47         0.    0.5745
   65    3   14   7.39   0.16   0.00   6.96         0.    0.1107
   66    3   11   4.85   0.23   0.00   4.75         0.    0.1458
   67    4    4  23.18   0.55   0.00  22.87         0.    0.5650
   68    4   11  13.02   0.41   0.00  12.39         0.    0.3693
   69    4   10   1.10   0.03   0.00   0.68         0.    0.0152
   70    4    3  24.49   0.69   0.00  24.17         0.    0.5596
   71    4    3   2.57   0.12   0.00   2.19         0.    0.0409
   72    4    1   2.10   0.13   0.00   2.03         0.    0.0730
   73   75    9   0.02   0.02   0.00   0.02         0.    0.0167
   74   45   10   0.35   0.18   0.00   0.33         0.    0.0000
   75   46    7  26.53   0.02   0.00  26.38         0.    0.0000
   76   46    9  27.75   0.03   0.00  27.61         0.    0.0000
   77   46   13  25.32   0.03   0.00  25.20         0.    0.0000
   78   46    3  23.08   0.04   0.00  22.97         0.    0.0000
   79   46   15  26.91   0.04   0.00  26.78         0.    0.0000
   80   46   11  29.07   0.05   0.00  28.94         0.    0.0000
   81   47    8  24.16   0.02   0.00  24.05         0.    0.0000
   82   47    4  24.67   0.03   0.00  24.54         0.    0.0000
   83   47   13  21.13   0.04   0.00  21.02         0.    0.0000
   84   47   14  22.20   0.04   0.00  22.08         0.    0.0000
   85   47   10  25.21   0.04   0.00  25.09         0.    0.0000
   86   47    3  28.62   0.05   0.00  28.48         0.    0.0000
================================================================
================ TIMING STATISTICS PER NODE    ================
node# twait    ths   tcin   teig  tvect  tga   ttotal 
  1   0.32   0.07   0.31   0.00   0.00   0.67  86.60
  2   0.23   0.07   0.31   0.00   0.00   1.12  86.60
  3   0.57   0.07   0.31   0.00   0.00   2.02  86.60
  4   0.06   0.07   0.31   0.00   0.00   0.90  86.60
  5   0.46   0.07   0.31   0.00   0.00   0.86  86.60
  6   0.50   0.07   0.31   0.00   0.00   0.94  86.60
  7   0.18   0.07   0.31   0.00   0.00   1.03  86.60
  8   0.00   0.07   0.31   0.00   0.00   1.14  86.60
  9   0.10   0.07   0.31   0.00   0.00   1.12  86.60
 10   0.61   0.07   0.31   0.00   0.00   2.83  86.60
 11   0.38   0.07   0.31   0.00   0.00   1.34  86.60
 12   0.13   0.07   0.31   0.00   0.00   1.16  86.60
 13   0.33   0.07   0.31   0.00   0.00   2.77  86.60
 14   0.28   0.07   0.31   0.00   0.00   0.90  86.60
 15   0.60   0.07   0.31   0.00   0.00   1.47  86.60
 16   0.38   0.07   0.31   0.00   0.00   0.67  86.60
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                  1373.7181s 
time spent in multnx:                1345.8464s 
integral transfer time:                 0.1560s 
time spent for loop construction:      18.0668s 
syncronization time in mult:            5.1450s 
total time per CI iteration:         1385.6007s 
 ** parallelization degree for mult section:0.9963

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)          22.18        0.02        964.909
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)         22.18        0.02        984.267
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.04        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:           44.41        0.05        972.350
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)        6687.59       13.08        511.278
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)       6687.59        7.80        857.901
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           2.80        0.01        265.215
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:        13377.97       20.89        640.524
========================================================


          starting ci iteration   7

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x: 1478252 2x:  317196 4x:   39816
All internal counts: zz :   63047 yy: 2272259 xx: 3264807 ww: 2639385
One-external counts: yz :  467862 yx: 5844432 yw: 5256661
Two-external counts: yy :  777693 ww:  527776 xx:  884767 xz:   44221 wz:   42950 wx: 1137511
Three-ext.   counts: yx : 4881506 yw: 4629136

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 630

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1    -0.93961379    -0.03079362     0.02634196

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  7  1   -155.4211727701  6.3832E-06  2.0440E-06  2.3944E-03  1.0000E-03
 mr-sdci #  7  2   -154.4294719036  1.7008E+00  0.0000E+00  8.7179E+00  1.0000E-03
 mr-sdci #  7  3   -151.6692217003 -6.6702E-01  0.0000E+00  2.0659E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   21   15  11.35   0.38   0.00  11.34         0.    0.4513
    2   22    9  43.91   0.08   0.00  43.70         0.    0.1186
    3   22   14  17.31   0.03   0.00  16.93         0.    0.0401
    4   22   15   7.37   0.01   0.00   6.99         0.    0.0173
    5   22    4   6.17   0.01   0.00   5.79         0.    0.0112
    6   22    4  43.69   0.10   0.00  43.48         0.    0.1193
    7   22    2  10.99   0.02   0.00  10.62         0.    0.0218
    8   22    0   6.87   0.01   0.00   6.49         0.    0.0140
    9   22    8  29.19   0.08   0.00  28.99         0.    0.0848
   10   22    5   7.28   0.02   0.00   6.91         0.    0.0157
   11   22    6  30.75   0.08   0.00  30.54         0.    0.0799
   12   23   12  23.49   0.04   0.00  23.32         0.    0.0577
   13   23    4   9.56   0.01   0.00   9.21         0.    0.0210
   14   23    3   6.48   0.01   0.00   6.12         0.    0.0113
   15   23    7   6.52   0.01   0.00   6.16         0.    0.0113
   16   23    8  19.22   0.04   0.00  19.05         0.    0.0525
   17   23   14   6.15   0.01   0.00   5.79         0.    0.0112
   18   23    0   4.90   0.01   0.00   4.54         0.    0.0059
   19   23    6  19.28   0.05   0.00  19.09         0.    0.0503
   20   23    3   3.22   0.00   0.00   2.88         0.    0.0042
   21   23    3  21.51   0.05   0.00  21.34         0.    0.0533
   22   24   13   0.45   0.01   0.00   0.24         0.    0.0082
   23   24    8   0.47   0.01   0.00   0.27         0.    0.0100
   24   24   12   0.43   0.01   0.00   0.23         0.    0.0077
   25   24    8   0.42   0.01   0.00   0.21         0.    0.0067
   26   25    9   0.44   0.01   0.00   0.25         0.    0.0081
   27   25    9   0.44   0.01   0.00   0.25         0.    0.0080
   28   25    8   0.41   0.01   0.00   0.22         0.    0.0065
   29   25    7   0.45   0.01   0.00   0.26         0.    0.0068
   30   26    0  50.28   0.10   0.00  49.88         0.    0.1350
   31   26    9   7.92   0.01   0.00   7.56         0.    0.0186
   32   26   12   6.08   0.01   0.00   5.71         0.    0.0112
   33   26    9   6.13   0.01   0.00   5.76         0.    0.0112
   34   26    2  24.37   0.05   0.00  24.01         0.    0.0613
   35   26   11  36.72   0.08   0.00  36.34         0.    0.0961
   36   26    2   6.84   0.01   0.00   5.41         0.    0.0112
   37   26   13   5.99   0.01   0.00   5.60         0.    0.0092
   38   26    8   9.05   0.01   0.00   8.71         0.    0.0179
   39   26    5  25.35   0.06   0.00  25.01         0.    0.0627
   40   26   15  34.23   0.08   0.00  33.82         0.    0.0878
   41   26    2   3.40   0.00   0.00   3.03         0.    0.0048
   42   26    6   6.61   0.01   0.00   6.24         0.    0.0116
   43   26    1   5.83   0.01   0.00   5.46         0.    0.0107
   44   26   12   9.66   0.02   0.00   9.29         0.    0.0228
   45   26   12  42.62   0.10   0.00  42.21         0.    0.1059
   46   11    7   1.08   0.61   0.00   1.07         0.    0.3822
   47   13    3  28.43   1.27   0.00  28.07         0.    1.6074
   48   13   13  41.16   3.53   0.00  40.80         0.    1.9524
   49   13   13  10.09   0.76   0.00   9.98         0.    0.6359
   50   14    5  32.60   1.38   0.00  32.23         0.    1.6375
   51   14    2  38.66   1.96   0.00  38.28         0.    1.8853
   52   14    1   4.95   0.41   0.00   4.89         0.    0.2648
   53   31    1  20.53   0.02   0.00  20.21         0.    0.0079
   54   31   14  26.84   0.03   0.00  26.48         0.    0.0081
   55   31   11   9.28   0.01   0.00   9.18         0.    0.0021
   56   32    4  23.87   0.02   0.00  23.54         0.    0.0084
   57   32    7  26.63   0.03   0.00  26.28         0.    0.0074
   58   32   15   5.62   0.01   0.00   5.55         0.    0.0007
   59    1    9   0.17   0.12   0.00   0.15         0.    0.0528
   60    2   14   4.81   1.74   0.00   4.80         0.    1.4781
   61    3   10  29.62   0.58   0.00  29.26         0.    0.6283
   62    3   11  10.87   0.48   0.00  10.22         0.    0.4645
   63    3   10   2.87   0.13   0.00   2.46         0.    0.1106
   64    3    1  27.17   0.67   0.00  26.82         0.    0.5745
   65    3   10   6.45   0.16   0.00   6.02         0.    0.1107
   66    3    5   5.46   0.23   0.00   5.36         0.    0.1458
   67    4    9  24.55   0.55   0.00  24.23         0.    0.5650
   68    4    5  13.17   0.42   0.00  12.52         0.    0.3693
   69    4   12   1.13   0.03   0.00   0.70         0.    0.0152
   70    4   11  24.74   0.70   0.00  24.43         0.    0.5596
   71    4    6   2.40   0.12   0.00   2.00         0.    0.0409
   72    4   11   2.09   0.13   0.00   2.03         0.    0.0730
   73   75    7   0.02   0.02   0.00   0.02         0.    0.0167
   74   45    4   0.34   0.18   0.00   0.33         0.    0.0000
   75   46    8  24.81   0.02   0.00  24.69         0.    0.0000
   76   46    1  25.99   0.03   0.00  25.86         0.    0.0000
   77   46   10  24.16   0.03   0.00  24.03         0.    0.0000
   78   46    0  21.73   0.04   0.00  21.61         0.    0.0000
   79   46    3  24.68   0.04   0.00  24.55         0.    0.0000
   80   46   14  29.46   0.05   0.00  29.30         0.    0.0000
   81   47   13  25.86   0.02   0.00  25.75         0.    0.0000
   82   47   15  25.75   0.03   0.00  25.63         0.    0.0000
   83   47    7  19.89   0.04   0.00  19.78         0.    0.0000
   84   47   10  21.02   0.04   0.00  20.91         0.    0.0000
   85   47    6  24.77   0.04   0.00  24.66         0.    0.0000
   86   47    7  28.82   0.05   0.00  28.68         0.    0.0000
================================================================
================ TIMING STATISTICS PER NODE    ================
node# twait    ths   tcin   teig  tvect  tga   ttotal 
  1   0.79   0.11   0.46   0.00   0.00   0.87  85.17
  2   0.10   0.11   0.46   0.00   0.00   0.85  85.17
  3   0.30   0.11   0.46   0.00   0.00   2.36  85.17
  4   0.24   0.11   0.46   0.00   0.00   0.95  85.17
  5   0.93   0.11   0.46   0.00   0.00   0.89  85.17
  6   0.70   0.11   0.46   0.00   0.00   1.28  85.17
  7   0.74   0.11   0.46   0.00   0.00   0.90  85.17
  8   1.15   0.11   0.46   0.00   0.00   0.81  85.17
  9   1.01   0.11   0.46   0.00   0.00   0.99  85.17
 10   1.01   0.11   0.46   0.00   0.00   1.17  85.17
 11   0.45   0.11   0.46   0.00   0.00   0.99  85.17
 12   0.86   0.11   0.46   0.00   0.00   1.04  85.17
 13   1.15   0.11   0.46   0.00   0.00   1.37  85.17
 14   1.00   0.11   0.46   0.00   0.00   0.84  85.17
 15   0.00   0.11   0.46   0.00   0.00   0.89  85.17
 16   0.24   0.11   0.46   0.00   0.00   0.69  85.17
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                  1342.2947s 
time spent in multnx:                1318.6207s 
integral transfer time:                 0.1559s 
time spent for loop construction:      18.4277s 
syncronization time in mult:           10.6721s 
total time per CI iteration:         1362.6531s 
 ** parallelization degree for mult section:0.9921

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)          19.45        0.02        935.676
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)         19.44        0.02       1157.771
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.04        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:           38.93        0.04       1032.449
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)        6690.33        8.21        815.194
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)       6690.33        8.66        772.728
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           2.80        0.01        266.288
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:        13383.45       16.88        793.065
========================================================


          starting ci iteration   8

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x: 1478252 2x:  317196 4x:   39816
All internal counts: zz :   63047 yy: 2272259 xx: 3264807 ww: 2639385
One-external counts: yz :  467862 yx: 5844432 yw: 5256661
Two-external counts: yy :  777693 ww:  527776 xx:  884767 xz:   44221 wz:   42950 wx: 1137511
Three-ext.   counts: yx : 4881506 yw: 4629136

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 630

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1    -0.93951223     0.05571664     0.04560988     0.03162367

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  8  1   -155.4211747444  1.9743E-06  4.8284E-07  1.3280E-03  1.0000E-03
 mr-sdci #  8  2   -154.8707764669  4.4130E-01  0.0000E+00  7.9840E+00  1.0000E-03
 mr-sdci #  8  3   -152.1101040790  4.4088E-01  0.0000E+00  3.6170E+00  1.0000E-04
 mr-sdci #  8  4   -151.6642187680 -3.5571E-01  0.0000E+00  3.2071E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   21    9  11.20   0.38   0.00  11.19         0.    0.4513
    2   22    4  45.70   0.08   0.00  45.50         0.    0.1186
    3   22    2  17.66   0.03   0.00  17.28         0.    0.0401
    4   22   12   8.59   0.01   0.00   8.23         0.    0.0173
    5   22    7   6.49   0.01   0.00   6.09         0.    0.0112
    6   22    8  46.20   0.10   0.00  45.99         0.    0.1193
    7   22   12  11.16   0.02   0.00  10.80         0.    0.0218
    8   22    0   8.12   0.01   0.00   7.74         0.    0.0140
    9   22    7  31.82   1.91   0.00  31.61         0.    0.0848
   10   22   11   8.66   0.02   0.00   8.29         0.    0.0157
   11   22    2  33.06   0.08   0.00  32.84         0.    0.0799
   12   23    8  23.78   0.04   0.00  23.61         0.    0.0577
   13   23    8   9.53   0.01   0.00   9.16         0.    0.0210
   14   23   14   6.74   0.01   0.00   6.38         0.    0.0113
   15   23    1   6.73   0.01   0.00   6.37         0.    0.0113
   16   23   13  19.52   0.04   0.00  19.34         0.    0.0525
   17   23   13   7.38   0.01   0.00   7.03         0.    0.0112
   18   23    0   3.82   0.01   0.00   3.47         0.    0.0059
   19   23    7  19.90   0.05   0.00  19.70         0.    0.0503
   20   23    5   3.33   0.00   0.00   2.97         0.    0.0042
   21   23    1  21.37   0.05   0.00  21.20         0.    0.0533
   22   24    8   0.41   0.01   0.00   0.21         0.    0.0082
   23   24   10   0.48   0.01   0.00   0.28         0.    0.0100
   24   24   10   0.44   0.01   0.00   0.24         0.    0.0077
   25   24   13   0.44   0.01   0.00   0.23         0.    0.0067
   26   25    6   0.46   0.01   0.00   0.27         0.    0.0081
   27   25   15   0.41   0.01   0.00   0.24         0.    0.0080
   28   25    8   0.40   0.01   0.00   0.21         0.    0.0065
   29   25   13   0.48   0.01   0.00   0.28         0.    0.0068
   30   26    0  52.78   0.10   0.00  52.37         0.    0.1350
   31   26    9   7.85   0.01   0.00   7.50         0.    0.0186
   32   26   15   6.37   0.01   0.00   5.99         0.    0.0112
   33   26   10   6.33   0.01   0.00   5.96         0.    0.0112
   34   26    5  25.10   0.05   0.00  23.09         0.    0.0613
   35   26   11  38.85   0.08   0.00  38.46         0.    0.0961
   36   26    5   6.99   0.01   0.00   6.63         0.    0.0112
   37   26    8   6.10   0.01   0.00   5.73         0.    0.0092
   38   26    6   7.98   0.01   0.00   7.62         0.    0.0179
   39   26   13  25.74   0.06   0.00  25.38         0.    0.0627
   40   26    9  36.77   0.08   0.00  36.38         0.    0.0878
   41   26    2   3.38   0.00   0.00   3.00         0.    0.0048
   42   26    2   6.77   0.01   0.00   6.41         0.    0.0116
   43   26    4   7.13   0.01   0.00   6.78         0.    0.0107
   44   26    4   9.60   0.02   0.00   9.23         0.    0.0228
   45   26   10  45.68   0.10   0.00  45.27         0.    0.1059
   46   11    3   1.09   0.61   0.00   1.08         0.    0.3822
   47   13    3  28.84   1.27   0.00  28.47         0.    1.6074
   48   13   12  38.44   1.92   0.00  38.07         0.    1.9524
   49   13    5  10.88   0.77   0.00  10.76         0.    0.6359
   50   14    6  33.95   1.37   0.00  33.59         0.    1.6375
   51   14    5  40.80   1.97   0.00  40.42         0.    1.8853
   52   14   12   3.79   0.41   0.00   3.72         0.    0.2648
   53   31   14  20.47   0.02   0.00  20.13         0.    0.0079
   54   31    1  28.91   0.03   0.00  28.57         0.    0.0081
   55   31   10   8.25   0.01   0.00   8.14         0.    0.0021
   56   32    4  24.23   0.02   0.00  23.90         0.    0.0084
   57   32   15  28.77   0.03   0.00  28.43         0.    0.0074
   58   32    6   5.82   0.01   0.00   5.75         0.    0.0007
   59    1    9   0.15   0.12   0.00   0.15         0.    0.0528
   60    2   11   3.69   1.73   0.00   3.68         0.    1.4781
   61    3   14  31.76   0.58   0.00  31.39         0.    0.6283
   62    3   11  11.28   0.48   0.00  10.63         0.    0.4645
   63    3    1   2.81   0.13   0.00   2.38         0.    0.1106
   64    3    1  26.94   0.66   0.00  26.59         0.    0.5745
   65    3    3   6.63   0.15   0.00   6.18         0.    0.1107
   66    3    9   5.72   0.23   0.00   4.35         0.    0.1458
   67    4   11  24.66   0.55   0.00  24.34         0.    0.5650
   68    4    6  12.61   0.41   0.00  11.75         0.    0.3693
   69    4    3   1.06   0.03   0.00   0.67         0.    0.0152
   70    4    9  24.57   0.70   0.00  24.26         0.    0.5596
   71    4   14   2.36   0.12   0.00   1.96         0.    0.0409
   72    4    7   2.21   0.13   0.00   2.15         0.    0.0730
   73   75   15   0.02   0.02   0.00   0.02         0.    0.0167
   74   45   12   0.34   0.18   0.00   0.34         0.    0.0000
   75   46    2  26.07   0.02   0.00  25.95         0.    0.0000
   76   46    3  28.56   1.76   0.00  28.44         0.    0.0000
   77   46   10  25.07   0.03   0.00  24.94         0.    0.0000
   78   46    0  22.16   0.04   0.00  22.05         0.    0.0000
   79   46   12  24.08   0.04   0.00  23.96         0.    0.0000
   80   46   15  27.85   0.05   0.00  27.70         0.    0.0000
   81   47   14  25.13   0.02   0.00  25.00         0.    0.0000
   82   47    7  25.96   0.03   0.00  25.84         0.    0.0000
   83   47    3  20.17   0.04   0.00  20.07         0.    0.0000
   84   47   15  22.73   0.04   0.00  22.61         0.    0.0000
   85   47    6  25.36   0.04   0.00  25.24         0.    0.0000
   86   47   13  32.82   0.05   0.00  32.67         0.    0.0000
================================================================
================ TIMING STATISTICS PER NODE    ================
node# twait    ths   tcin   teig  tvect  tga   ttotal 
  1   0.26   0.15   0.60   0.00   0.00   0.88  87.92
  2   0.37   0.15   0.60   0.00   0.00   1.16  87.92
  3   0.21   0.15   0.60   0.00   0.00   1.03  87.92
  4   0.79   0.15   0.60   0.00   0.00   1.02  87.92
  5   0.48   0.15   0.60   0.00   0.00   0.86  87.92
  6   0.03   0.15   0.60   0.00   0.00   1.12  87.92
  7   0.96   0.15   0.60   0.00   0.00   1.32  87.92
  8   0.76   0.15   0.60   0.00   0.00   0.69  87.92
  9   0.71   0.15   0.60   0.00   0.00   1.06  87.92
 10   0.88   0.15   0.60   0.00   0.00   2.09  87.92
 11   0.88   0.15   0.60   0.00   0.00   1.01  87.92
 12   0.00   0.15   0.60   0.00   0.00   1.22  87.92
 13   0.73   0.15   0.60   0.00   0.00   0.90  87.92
 14   0.76   0.15   0.60   0.00   0.00   1.03  87.92
 15   0.67   0.15   0.60   0.00   0.00   1.13  87.92
 16   0.99   0.15   0.60   0.00   0.00   0.82  87.92
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                  1384.7450s 
time spent in multnx:                1358.8530s 
integral transfer time:                 0.1574s 
time spent for loop construction:      20.3689s 
syncronization time in mult:            9.4946s 
total time per CI iteration:         1406.7387s 
 ** parallelization degree for mult section:0.9932

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)           0.00        0.00          0.000
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.08        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:            0.08        0.00          0.000
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)        6709.77        9.59        699.404
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)       6709.77        7.74        867.449
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           2.76        0.01        268.878
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:        13422.30       17.34        774.116
========================================================


          starting ci iteration   9

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x: 1478252 2x:  317196 4x:   39816
All internal counts: zz :   63047 yy: 2272259 xx: 3264807 ww: 2639385
One-external counts: yz :  467862 yx: 5844432 yw: 5256661
Two-external counts: yy :  777693 ww:  527776 xx:  884767 xz:   44221 wz:   42950 wx: 1137511
Three-ext.   counts: yx : 4881506 yw: 4629136

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 630

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1    -0.93946830     0.06586182    -0.07000050    -0.00774958     0.02150477

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  9  1   -155.4211752062  4.6182E-07  7.1775E-08  4.5952E-04  1.0000E-03
 mr-sdci #  9  2   -154.9342919241  6.3515E-02  0.0000E+00  6.9172E+00  1.0000E-03
 mr-sdci #  9  3   -152.6164697666  5.0637E-01  0.0000E+00  7.5846E+00  1.0000E-04
 mr-sdci #  9  4   -151.9651515968  3.0093E-01  0.0000E+00  1.1103E+00  1.0000E-04
 mr-sdci #  9  5   -151.5950575731 -3.6011E-01  0.0000E+00  1.9637E+00  1.0000E-04
 
 
 total energy in cosmo calc 
e(nroot) + repnuc=    -155.4211752062
dielectric energy =       0.0000000000
deltaediel =       0.0000000000
deltaelast =       0.0000000000
 

 mr-sdci  convergence criteria satisfied after  9 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  9  1   -155.4211752062  4.6182E-07  7.1775E-08  4.5952E-04  1.0000E-03
 mr-sdci #  9  2   -154.9342919241  6.3515E-02  0.0000E+00  6.9172E+00  1.0000E-03
 mr-sdci #  9  3   -152.6164697666  5.0637E-01  0.0000E+00  7.5846E+00  1.0000E-04
 mr-sdci #  9  4   -151.9651515968  3.0093E-01  0.0000E+00  1.1103E+00  1.0000E-04
 mr-sdci #  9  5   -151.5950575731 -3.6011E-01  0.0000E+00  1.9637E+00  1.0000E-04

####################CIUDGINFO####################

   ci vector at position   1 energy= -155.421175206181

################END OF CIUDGINFO################

 
    1 of the   6 expansion vectors are transformed.
 nnormold( 1 )= 0.920875170098814255
 nnormold( 2 )= 0.558214807984581652E-05
 nnormold( 3 )= 0.266868254286840989E-05
 nnormold( 4 )= 0.113906776874674636E-05
 nnormold( 5 )= 0.141094221975834538E-06
 nnormnew( 1 )= 0.920990454410310022
    1 of the   5 matrix-vector products are transformed.
 nnormold( 1 )= 137.968117652710248
 nnormold( 2 )= 0.586087666944663452E-03
 nnormold( 3 )= 0.301112733524747263E-03
 nnormold( 4 )= 0.128418867554372379E-03
 nnormold( 5 )= 0.134294740346502066E-04
 nnormnew( 1 )= 137.985134345796268

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
 maximum overlap with reference  1 (overlap= 0.939468303173220942 )

information on vector: 1from unit 11 written to unit 16filename civout                                                      


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =      -155.4211752062

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7    8    9   10   11   12   13

                                          orbital     5    6    7   54   55   56   57   58   59  107  108  127  128

                                         symmetry   Ag   Ag   Ag   Bu   Bu   Bu   Bu   Bu   Bu   Au   Au   Bg   Bg 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1  0.152925                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-           
 z*  1  1       2 -0.906427                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +-      
 z*  1  1       3 -0.055126                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +     - 
 z*  1  1       4  0.053975                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-             +- 
 z*  1  1       5  0.062083                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +-      
 z*  1  1       6  0.103718                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +     - 
 z*  1  1       8 -0.106045                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +     -    - 
 z*  1  1       9  0.062148                        +-   +-   +-   +-   +-   +-   +-   +-   +-        +-   +-      
 z*  1  1      11 -0.024739                        +-   +-   +-   +-   +-   +-   +-   +-   +-        +-        +- 
 z*  1  1      12  0.062370                        +-   +-   +-   +-   +-   +-   +-   +-   +-             +-   +- 
 z*  1  1      55  0.011900                        +-   +-   +-   +-   +-   +-        +-   +-   +-        +-   +- 
 y   1  1     632  0.013399              2( Au )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -           
 y   1  1     703 -0.012668              1( Bg )   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +     -      
 y   1  1     758 -0.010169              2( Au )   +-   +-   +-   +-   +-   +-   +-   +-   +-    -        +     - 
 y   1  1     793  0.012232              1( Bg )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -    -      
 y   1  1     794 -0.014564              2( Bg )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -    -      
 y   1  1     830 -0.011784              2( Au )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +          -    - 
 y   1  1     831  0.010637              3( Au )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +          -    - 
 y   1  1     884 -0.014637              2( Au )   +-   +-   +-   +-   +-   +-   +-   +-   +-         -   +-      
 y   1  1    1039  0.013929              2( Ag )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +     -      
 y   1  1    1041  0.010025              4( Ag )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +     -      
 y   1  1    1042  0.013245              5( Ag )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +     -      
 y   1  1    1046 -0.010364              9( Ag )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +     -      
 y   1  1    3027  0.010702              1( Ag )   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +     -      
 y   1  1    3175 -0.012423             10( Bu )   +-   +-   +-   +-   +-   +-   +-    -   +-   +-        +     - 
 y   1  1    3177 -0.013192             12( Bu )   +-   +-   +-   +-   +-   +-   +-    -   +-   +-        +     - 
 y   1  1    3356 -0.011044              5( Bu )   +-   +-   +-   +-   +-   +-   +-    -   +-   +     -   +-      
 y   1  1    3361 -0.011978             10( Bu )   +-   +-   +-   +-   +-   +-   +-    -   +-   +     -   +-      
 y   1  1    3363 -0.013662             12( Bu )   +-   +-   +-   +-   +-   +-   +-    -   +-   +     -   +-      
 y   1  1    3551 -0.010176             12( Ag )   +-   +-   +-   +-   +-   +-   +-    -   +-   +         +-    - 
 y   1  1   10938  0.012261              7( Bu )   +-   +-   +-   +-   +-   +-   +    +-   +-   +-         -    - 
 y   1  1   11264 -0.014752              6( Ag )   +-   +-   +-   +-   +-   +-   +    +-   +-    -        +-    - 
 y   1  1   16550 -0.010149             12( Bu )   +-   +-   +-   +-   +-    -   +-   +-   +-   +-        +     - 
 y   1  1   16736 -0.010312             12( Bu )   +-   +-   +-   +-   +-    -   +-   +-   +-   +     -   +-      
 y   1  1   22859 -0.013389              7( Ag )   +-   +-   +-   +-   +-   +    +-   +-   +-   +-    -    -      
 y   1  1  107485 -0.013804              5( Bu )   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +     -      
 y   1  1  107625  0.011346              5( Ag )   +-   +-    -   +-   +-   +-   +-   +-   +-   +-        +     - 
 y   1  1  107811  0.013075              5( Ag )   +-   +-    -   +-   +-   +-   +-   +-   +-   +     -   +-      
 y   1  1  107996 -0.010952              6( Bu )   +-   +-    -   +-   +-   +-   +-   +-   +-   +         +-    - 
 y   1  1  108003 -0.011253             13( Bu )   +-   +-    -   +-   +-   +-   +-   +-   +-   +         +-    - 
 y   1  1  174525 -0.011440              5( Bu )   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +     -      
 y   1  1  174526  0.012626              6( Bu )   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +     -      
 y   1  1  174530 -0.016454             10( Bu )   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +     -      
 y   1  1  174532 -0.018058             12( Bu )   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +     -      
 y   1  1  174667  0.010226              7( Ag )   +-    -   +-   +-   +-   +-   +-   +-   +-   +-        +     - 
 y   1  1  174671  0.011131             11( Ag )   +-    -   +-   +-   +-   +-   +-   +-   +-   +-        +     - 
 y   1  1  174672  0.012393             12( Ag )   +-    -   +-   +-   +-   +-   +-   +-   +-   +-        +     - 
 y   1  1  174858  0.014256             12( Ag )   +-    -   +-   +-   +-   +-   +-   +-   +-   +     -   +-      
 y   1  1  174862  0.010184             16( Ag )   +-    -   +-   +-   +-   +-   +-   +-   +-   +     -   +-      
 y   1  1  175042  0.012058             12( Bu )   +-    -   +-   +-   +-   +-   +-   +-   +-   +         +-    - 
 y   1  1  258891 -0.010990              9( Ag )    -   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +-      
 y   1  1  259072  0.010936              6( Bu )    -   +-   +-   +-   +-   +-   +-   +-   +-   +         +-    - 
 y   1  1  259079  0.011377             13( Bu )    -   +-   +-   +-   +-   +-   +-   +-   +-   +         +-    - 
 x   1  124066343 -0.013936    7( Bu )   3( Au )   +-    -   +-        +-   +-   +-   +-    -   +-   +-        +- 
 x   1  124074710  0.010962    6( Ag )   3( Au )   +-    -   +-        +-   +-   +-   +-    -   +    +-    -   +- 
 w   1  130306013  0.010476    7( Bu )   7( Bu )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-                
 w   1  130307285  0.010550    1( Bg )   1( Bg )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-                
 w   1  130310288 -0.017675    6( Ag )   7( Bu )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +          -      
 w   1  130312169 -0.015650    1( Au )   1( Bg )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +          -      
 w   1  130322522  0.013489    6( Ag )   6( Ag )   +-   +-   +-   +-   +-   +-   +-   +-   +-             +-      
 w   1  130324711  0.010125    1( Au )   1( Au )   +-   +-   +-   +-   +-   +-   +-   +-   +-             +-      
 w   1  130577958  0.010121    5( Bu )   5( Bu )   +-   +-   +-   +-   +-   +-   +-        +-   +-        +-      
 w   1  144596345  0.011904    5( Bu )   5( Bu )   +-   +-   +    +-   +-   +-    -   +    +-   +-    -   +-      
 w   1  150210718  0.010157    5( Bu )   9( Bu )   +-   +    +-   +-   +-        +-   +-        +-   +-   +-    - 

 ci coefficient statistics:
           rq > 0.1                4
      0.1> rq > 0.01              60
     0.01> rq > 0.001          16781
    0.001> rq > 0.0001        248841
   0.0001> rq > 0.00001      1364543
  0.00001> rq > 0.000001     4248763
 0.000001> rq               52728092
           all              58607084
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:   21154 2x:       0 4x:       0
All internal counts: zz :   63047 yy:       0 xx:       0 ww:       0
One-external counts: yz :       0 yx:       0 yw:       0
Two-external counts: yy :       0 ww:       0 xx:       0 xz:       0 wz:       0 wx:       0
Three-ext.   counts: yx :       0 yw:       0

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.152925111687    -23.717558637531
     2     2     -0.906427277321    140.503165552214
     3     3     -0.055126300766      8.548625955909
     4     4      0.053975383970     -8.374337363862
     5     5      0.062083407930     -9.625634689253
     6     6      0.103717585700    -16.094350429535
     7     7     -0.004859114809      0.754130054370
     8     8     -0.106044665068     16.452876447859
     9     9      0.062147807212     -9.641046978205
    10    10      0.004841089592     -0.751931847863
    11    11     -0.024738612202      3.841546000137
    12    12      0.062369708408     -9.679016023107
    13    13      0.005411898243     -0.838461288274
    14    14      0.000357304767     -0.054928049336
    15    15     -0.001049122744      0.162674900495
    16    16      0.003994245462     -0.618487322289
    17    17     -0.000350725796      0.054250382702
    18    18     -0.000616019699      0.095509085216
    19    19     -0.000084916955      0.012834792843
    20    20     -0.000014990809      0.002288848984
    21    21      0.000190478343     -0.029480265910
    22    22     -0.001128763091      0.174576260201
    23    23      0.000069825627     -0.010626820048
    24    24      0.000077873413     -0.012038579652
    25    25      0.000198417215     -0.030715159478
    26    26     -0.000096583523      0.014910675342
    27    27      0.004971773184     -0.770353150030
    28    28      0.000213046414     -0.032460102063
    29    29     -0.000937645123      0.145428573159
    30    30      0.003783359838     -0.585926173515
    31    31     -0.000475142553      0.073551526629
    32    32     -0.000572456012      0.088763814725
    33    33     -0.000046021381      0.007133842808
    34    34     -0.000153112554      0.023825728132
    35    35      0.000411638792     -0.063416602583
    36    36      0.000775549548     -0.120368644525
    37    37     -0.004312288806      0.668898665846
    38    38     -0.000032045612      0.005393335243
    39    39      0.000302046487     -0.046903401777
    40    40     -0.000113948911      0.017667546816
    41    41     -0.000001374036      0.000248772646
    42    42     -0.002878637944      0.446513558549
    43    43      0.000401343485     -0.062284766538
    44    44      0.000605057950     -0.093949743155
    45    45     -0.002580882494      0.400483143016
    46    46      0.000396422262     -0.061624271749
    47    47      0.000368303607     -0.057179604161
    48    48      0.000027941976     -0.004336454616
    49    49      0.000357989721     -0.055427021744
    50    50     -0.000058084704      0.009002086393
    51    51     -0.000029759602      0.004624794571
    52    52      0.005357109167     -0.830066974713
    53    53     -0.000460518286      0.071656322213
    54    54     -0.002421303157      0.375636596214
    55    55      0.011899524512     -1.844829784513
    56    56     -0.000545946983      0.084229561320
    57    57     -0.001151194253      0.178594211321
    58    58     -0.000094090870      0.014601834094
    59    59      0.000022346425     -0.003466774207
    60    60     -0.000082221223      0.012761488591
    61    61      0.001249644450     -0.193616349796
    62    62     -0.000032593102      0.005114255392
    63    63     -0.000623403367      0.096669949246
    64    64      0.003085082812     -0.478013963064
    65    65     -0.000196676134      0.030443713139
    66    66     -0.000286754234      0.044458349426
    67    67      0.000175902014     -0.027276247744
    68    68     -0.000067356920      0.010399689446
    69    69     -0.005846247831      0.906645570292
    70    70     -0.000887088161      0.137535680560
    71    71      0.000486744538     -0.075463575799
    72    72     -0.000763178363      0.118182924520
    73    73      0.000486268960     -0.075730394051
    74    74      0.000422481690     -0.065594036711
    75    75      0.000029481020     -0.004576315862
    76    76      0.000109965457     -0.017374034086
    77    77      0.000049586943     -0.007597242231
    78    78      0.000017789430     -0.002759222923
    79    79      0.005138622438     -0.797039516832
    80    80      0.001630884086     -0.253031559701
    81    81      0.000543053817     -0.084280809376
    82    82     -0.004707260542      0.729735456371
    83    83     -0.000613740949      0.095842570356
    84    84     -0.000025670653      0.004030557473
    85    85     -0.000000285592      0.000047856291
    86    86      0.000011283916     -0.001754744421
    87    87      0.000018787160     -0.002913224963
    88    88     -0.000908691886      0.141458004364
    89    89      0.000228393338     -0.035590314497
    90    90     -0.000038594672      0.005990669771
    91    91      0.000053000739     -0.008227476083
    92    92     -0.000088262354      0.013704708233
    93    93      0.005173156638     -0.801016298405
    94    94     -0.000570573233      0.089011255582
    95    95     -0.001297149653      0.201184328597
    96    96      0.004797846844     -0.743149673923
    97    97      0.000040802112     -0.006724621172
    98    98     -0.000629230979      0.097511010326
    99    99     -0.000047650569      0.007381225737
   100   100     -0.000008098606      0.001255504407
   101   101     -0.000039894601      0.006181260161
   102   102      0.000037652500     -0.005835311987
   103   103      0.000043977349     -0.006812979487
   104   104     -0.000121473692      0.018821251273
   105   105      0.000001510426     -0.000265504170
   106   106      0.000003115285     -0.000476130065
   107   107     -0.000001089392      0.000170105648
   108   108      0.000005339760     -0.000833494419
   109   109      0.000000860571     -0.000096777537
   110   110     -0.000000622140      0.000098119791
   111   111     -0.000003108482      0.000498729047
   112   112      0.000001881424     -0.000289177221
   113   113     -0.000164642253      0.025649245409
   114   114     -0.000019589393      0.003098535627
   115   115      0.000031029346     -0.004838567902
   116   116     -0.000124988083      0.019490075553
   117   117      0.000018273573     -0.002867258280
   118   118      0.000019552237     -0.003053160102
   119   119      0.000001466121     -0.000228677361
   120   120      0.000008106172     -0.001293643608
   121   121     -0.000001482433      0.000233982696
   122   122     -0.000000106536      0.000016404234
   123   123      0.000249635072     -0.038740570923
   124   124      0.000030424049     -0.004775934286
   125   125     -0.000049915249      0.007768170393
   126   126      0.000204885184     -0.031939479866
   127   127     -0.000028847969      0.004504157992
   128   128     -0.000030888161      0.004810972584
   129   129     -0.000002289526      0.000356938309
   130   130      0.000000667631     -0.000104206351
   131   131     -0.000001525828      0.000237391704
   132   132     -0.000009207759      0.001460022580
   133   133      0.000002490889     -0.000402688752
   134   134     -0.000000736490      0.000114592272
   135   135      0.000000923165     -0.000143687076
   136   136     -0.000001998840      0.000312355936
   137   137      0.000438660276     -0.067859338052
   138   138      0.000061145022     -0.009439157762
   139   139     -0.000053827905      0.008331809434
   140   140      0.000138973526     -0.021428680652
   141   141     -0.000028425656      0.004417112709
   142   142     -0.000037711162      0.005833092548
   143   143     -0.000002728389      0.000421874519
   144   144      0.000000141781     -0.000021995074
   145   145     -0.000001759721      0.000270739065
   146   146      0.000002622538     -0.000406626686
   147   147      0.000000566547     -0.000084450525
   148   148     -0.000006293216      0.000972098422
   149   149      0.000026812470     -0.004220975148
   150   150      0.000000513614     -0.000055034904
   151   151      0.000000609345     -0.000094815765
   152   152     -0.000000952034      0.000148241159
   153   153      0.000001507833     -0.000234403204
   154   154     -0.000001089146      0.000168184579
   155   155     -0.000000524848      0.000082648711
   156   156      0.000003122250     -0.000483991779
   157   157      0.000214164945     -0.033311969498
   158   158      0.000029271168     -0.004542844513
   159   159     -0.000033045935      0.005147338671
   160   160      0.000114053361     -0.017760925234
   161   161     -0.000019159502      0.002986750216
   162   162     -0.000021963059      0.003420795996
   163   163     -0.000001605689      0.000250261216
   164   164      0.000000281896     -0.000044044715
   165   165     -0.000001437020      0.000223705177
   166   166      0.000001011220     -0.000157769198
   167   167      0.000000762646     -0.000118003187
   168   168     -0.000003317047      0.000516444185
   169   169     -0.000000863095      0.000134397998
   170   170      0.000000918438     -0.000143079870
   171   171      0.000000408921     -0.000063817208
   172   172     -0.000001584786      0.000245711635
   173   173      0.000058229794     -0.009054231573
   174   174     -0.000006936043      0.000998771424
   175   175     -0.000029343468      0.004559094498
   176   176      0.000149035494     -0.023146542697
   177   177     -0.000009929010      0.001509659476
   178   178     -0.000013755146      0.002144697169
   179   179      0.000003929532     -0.000620756058
   180   180     -0.000004077459      0.000612466540
   181   181      0.000001118907     -0.000083047790
   182   182     -0.000019457847      0.003064939991
   183   183     -0.000024681025      0.003816926930
   184   184      0.000140828746     -0.021712751114
   185   185     -0.000002117290      0.000237985498
   186   186     -0.000009958944      0.001520322647
   187   187     -0.000000927152      0.000142461829
   188   188     -0.000010450058      0.001629968695
   189   189      0.000000969029     -0.000159883631
   190   190      0.000001077068     -0.000167683235
   191   191     -0.000112905950      0.017469929680
   192   192      0.000033953372     -0.005269353665
   193   193      0.000099309977     -0.015425564144
   194   194     -0.000533025045      0.082713053451
   195   195      0.000016579907     -0.002490346076
   196   196      0.000043647243     -0.006776240298
   197   197      0.000003394633     -0.000528015890
   198   198     -0.000000606019      0.000093646900
   199   199      0.000003076784     -0.000478365659
   200   200     -0.000005279200      0.000844587686
   201   201      0.000008861741     -0.001362164889
   202   202     -0.000000527596      0.000082720367
   203   203      0.000001169371     -0.000182339301
   204   204     -0.000000672903      0.000107004096
   205   205      0.000092605834     -0.014143073215
   206   206     -0.000045702056      0.007166506333
   207   207     -0.000084385936      0.013077531170
   208   208      0.000436921803     -0.067585264563
   209   209     -0.000003405310      0.000377975995
   210   210     -0.000035218387      0.005436527058
   211   211     -0.000002733283      0.000422061022
   212   212     -0.000000225382      0.000035673576
   213   213     -0.000003440169      0.000532530230
   214   214      0.000000322960     -0.000048811360
   215   215      0.000004015010     -0.000623034815
   216   216     -0.000005649834      0.000873320671
   217   217     -0.000027282362      0.004298349536
   218   218      0.000000402455     -0.000112058800
   219   219     -0.000001057980      0.000164348979
   220   220      0.000001416205     -0.000220863546
   221   221     -0.000002491363      0.000387799151
   222   222      0.000000512317     -0.000080006710
   223   223      0.000002806783     -0.000434009757
   224   224     -0.000004928311      0.000763859799
   225   225      0.000012763438     -0.001981053096
   226   226      0.000004926006     -0.000762541383
   227   227      0.000005021861     -0.000781735407
   228   228     -0.000033515007      0.005215861165
   229   229     -0.000000698811      0.000111521147
   230   230      0.000001455777     -0.000226868149
   231   231      0.000000150359     -0.000023460429
   232   232     -0.000000000064      0.000000028584
   233   233      0.000000132428     -0.000020730785
   234   234      0.000000068929     -0.000010833316
   235   235     -0.000000048863      0.000007769816
   236   236     -0.000000046981      0.000007259107
   237   237      0.000000048660     -0.000007622978
   238   238     -0.000000209375      0.000032809035
   239   239      0.000000157390     -0.000024677961
   240   240      0.000000315658     -0.000048847766
   241   241     -0.000001847268      0.000289341363
   242   242      0.000000226367     -0.000036999134
   243   243     -0.000000030075      0.000004715991
   244   244      0.000000046102     -0.000007241376
   245   245     -0.000000064902      0.000010238860
   246   246      0.000000063974     -0.000009937856
   247   247      0.000000131703     -0.000020293870
   248   248     -0.000000340686      0.000052682402
   249   249      0.000000040265     -0.000006312074
   250   250      0.000000026847     -0.000004124390
   251   251     -0.000000130252      0.000020312902
   252   252      0.000000118879     -0.000018393554
   253   253      0.000088040594     -0.013697896682
   254   254     -0.000015178935      0.002359951924
   255   255     -0.000061508557      0.009571930494
   256   256      0.000317254851     -0.049327554231
   257   257     -0.000006224713      0.000943484509
   258   258     -0.000027215761      0.004234913479
   259   259     -0.000002298414      0.000357854223
   260   260      0.000000133169     -0.000020742029
   261   261     -0.000002190332      0.000341434172
   262   262      0.000000500429     -0.000077438297
   263   263      0.000001483613     -0.000231266912
   264   264     -0.000003172613      0.000493636477
   265   265     -0.000000742851      0.000115719094
   266   266      0.000002097817     -0.000327109426
   267   267     -0.000001265757      0.000197435659
   268   268     -0.000002431433      0.000377607096
   269   269     -0.000000000832      0.000000146260
   270   270      0.000000070762     -0.000011077725
   271   271     -0.000000107666      0.000016803393
   272   272     -0.000000163179      0.000025296773
   273   273     -0.000000085317      0.000013316533

 number of reference csfs (nref) is   273.  root number (iroot) is  1.

 pople ci energy extrapolation is computed with 22 correlated electrons.

 eref      =   -155.016125613887   "relaxed" cnot**2         =   0.885673512603
 eci       =   -155.421175206181   deltae = eci - eref       =  -0.405049592294
 eci+dv1   =   -155.467483103290   dv1 = (1-cnot**2)*deltae  =  -0.046307897109
 eci+dv2   =   -155.473460722740   dv2 = dv1 / cnot**2       =  -0.052285516558
 eci+dv3   =   -155.481210307887   dv3 = dv1 / (2*cnot**2-1) =  -0.060035101706
 eci+pople =   -155.474229212477   ( 22e- scaled deltae )    =  -0.458103598589
NO coefficients and occupation numbers are written to nocoef_ci.1
 entering drivercid: firstcall,firstnonref= T F

################################################################################
 one electron density matrix calculation 
################################################################################


 workspace allocation information: mxcore=51611785
 
 space required:
 
    space required for calls in multd2:
       onex         2491
       allin           0
       diagon      12755
    max.      ---------
       maxnex      12755
 
    total core space usage:
       maxnex      12755
    max k-seg    5114024
    max k-ind      11283
              ---------
       totmax   10263369
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=   13637  DYX=  142292  DYW=  133923
   D0Z=    3270  D0Y=   89925  D0X=  145845  D0W=  124571
  DDZI=    4795 DDYI=   90321 DDXI=  117976 DDWI=  108899
  DDZE=       0 DDYE=   11283 DDXE=   14835 DDWE=   13698
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   2.0000000      2.0000000      2.0000000      2.0000000      1.9755124
   1.9727744      1.9716614      0.0126323      0.0116803      0.0103737
   0.0093594      0.0064693      0.0051551      0.0043903      0.0025360
   0.0021050      0.0015828      0.0011154      0.0009764      0.0009104
   0.0007145      0.0005608      0.0004528      0.0004429      0.0004191
   0.0003277      0.0002778      0.0002514      0.0002325      0.0001552
   0.0001382      0.0001158      0.0001078      0.0000950      0.0000845
   0.0000782      0.0000696      0.0000640      0.0000597      0.0000536
   0.0000496      0.0000471      0.0000442      0.0000353      0.0000300
   0.0000247      0.0000199      0.0000174      0.0000162      0.0000100
   0.0000093      0.0000082      0.0000047


*****   symmetry block SYM2   *****

 occupation numbers of nos
   1.9995649      1.9995608      1.9848206      1.9779463      1.9753261
   1.9727599      0.0142495      0.0124423      0.0105674      0.0093325
   0.0087771      0.0066801      0.0051696      0.0039798      0.0023056
   0.0017022      0.0011494      0.0009668      0.0009244      0.0007284
   0.0006643      0.0005481      0.0004675      0.0004347      0.0003917
   0.0002773      0.0002365      0.0001838      0.0001709      0.0001370
   0.0001220      0.0001096      0.0000868      0.0000799      0.0000724
   0.0000688      0.0000633      0.0000617      0.0000594      0.0000580
   0.0000508      0.0000467      0.0000351      0.0000331      0.0000264
   0.0000217      0.0000179      0.0000158      0.0000130      0.0000091
   0.0000081      0.0000048      0.0000044


*****   symmetry block SYM3   *****

 occupation numbers of nos
   1.9287691      0.1020412      0.0061787      0.0053716      0.0033129
   0.0027496      0.0019971      0.0006941      0.0005197      0.0004517
   0.0003348      0.0002651      0.0002132      0.0001690      0.0001100
   0.0000863      0.0000656      0.0000328      0.0000276      0.0000225


*****   symmetry block SYM4   *****

 occupation numbers of nos
   1.8845132      0.0546027      0.0060504      0.0043645      0.0031998
   0.0021691      0.0009333      0.0006390      0.0005633      0.0004495
   0.0003322      0.0002614      0.0002116      0.0001724      0.0001144
   0.0000816      0.0000647      0.0000315      0.0000251      0.0000214


 total number of electrons =   30.0000000000



          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        Ag  partial gross atomic populations
   ao class       1Ag        2Ag        3Ag        4Ag        5Ag        6Ag 
    C1_ s       0.828078   0.013791   1.984577   0.742037   1.006847   0.246313
    C2_ s       0.682266   1.985503   0.013847   1.111303   0.105836   0.876828
    H1_ s       0.165622  -0.000035   0.000716   0.037003   0.491260   0.137939
    H2_ s       0.251377  -0.000068   0.000822   0.031652   0.343117  -0.003047
    H3_ s       0.072658   0.000809   0.000038   0.078005   0.028452   0.714741
 
   ao class       7Ag        8Ag        9Ag       10Ag       11Ag       12Ag 
    C1_ s       0.594600   0.003172   0.006331   0.003079   0.004214   0.002631
    C2_ s       1.088998   0.005762   0.000851   0.005755   0.001046   0.002806
    H1_ s       0.019117   0.000235   0.003551   0.000125   0.001152   0.000374
    H2_ s       0.234535   0.000770   0.000784   0.000102   0.002508   0.000295
    H3_ s       0.034411   0.002694   0.000164   0.001313   0.000439   0.000363
 
   ao class      13Ag       14Ag       15Ag       16Ag       17Ag       18Ag 
    C1_ s       0.001308   0.003213   0.000628   0.000559   0.000635   0.000391
    C2_ s       0.003155   0.000138   0.001329   0.000726   0.000582   0.000479
    H1_ s       0.000130   0.000378   0.000194   0.000245   0.000121   0.000028
    H2_ s       0.000126   0.000638   0.000021   0.000057   0.000138   0.000157
    H3_ s       0.000435   0.000023   0.000364   0.000519   0.000107   0.000059
 
   ao class      19Ag       20Ag       21Ag       22Ag       23Ag       24Ag 
    C1_ s       0.000306   0.000294   0.000165   0.000146   0.000206   0.000268
    C2_ s       0.000248   0.000364   0.000404   0.000285   0.000122   0.000066
    H1_ s       0.000147   0.000168   0.000061   0.000069   0.000055   0.000025
    H2_ s       0.000240   0.000059   0.000075  -0.000001   0.000056   0.000050
    H3_ s       0.000034   0.000026   0.000010   0.000062   0.000014   0.000034
 
   ao class      25Ag       26Ag       27Ag       28Ag       29Ag       30Ag 
    C1_ s       0.000109   0.000088   0.000099   0.000114   0.000136   0.000046
    C2_ s       0.000226   0.000158   0.000121   0.000071   0.000049   0.000084
    H1_ s       0.000031   0.000017   0.000026   0.000000   0.000018   0.000004
    H2_ s       0.000011   0.000016  -0.000004   0.000015   0.000013   0.000014
    H3_ s       0.000042   0.000050   0.000037   0.000051   0.000017   0.000007
 
   ao class      31Ag       32Ag       33Ag       34Ag       35Ag       36Ag 
    C1_ s       0.000033   0.000038   0.000032   0.000014   0.000018   0.000019
    C2_ s       0.000069   0.000059   0.000037   0.000038   0.000036   0.000019
    H1_ s       0.000015   0.000004   0.000018   0.000024   0.000003   0.000005
    H2_ s       0.000011   0.000009   0.000009   0.000005   0.000016   0.000032
    H3_ s       0.000010   0.000006   0.000012   0.000014   0.000012   0.000004
 
   ao class      37Ag       38Ag       39Ag       40Ag       41Ag       42Ag 
    C1_ s       0.000000   0.000013   0.000013   0.000012   0.000019   0.000000
    C2_ s       0.000017   0.000019   0.000015   0.000004   0.000002  -0.000004
    H1_ s       0.000020   0.000013   0.000004   0.000025   0.000011   0.000009
    H2_ s       0.000030   0.000014   0.000017   0.000008   0.000007   0.000001
    H3_ s       0.000003   0.000006   0.000011   0.000005   0.000010   0.000042
 
   ao class      43Ag       44Ag       45Ag       46Ag       47Ag       48Ag 
    C1_ s       0.000009   0.000008   0.000007   0.000007   0.000002   0.000006
    C2_ s       0.000007   0.000007   0.000006   0.000007   0.000001   0.000002
    H1_ s       0.000013   0.000003   0.000010   0.000001   0.000005   0.000002
    H2_ s       0.000010   0.000007   0.000004   0.000008   0.000006   0.000001
    H3_ s       0.000005   0.000009   0.000004   0.000002   0.000006   0.000005
 
   ao class      49Ag       50Ag       51Ag       52Ag       53Ag 
    C1_ s       0.000001   0.000000   0.000004   0.000000   0.000001
    C2_ s       0.000003   0.000001  -0.000001   0.000000   0.000000
    H1_ s       0.000002   0.000002   0.000003   0.000003   0.000002
    H2_ s       0.000005   0.000001   0.000002   0.000005   0.000001
    H3_ s       0.000005   0.000006   0.000001   0.000000   0.000001

                        Bu  partial gross atomic populations
   ao class       1Bu        2Bu        3Bu        4Bu        5Bu        6Bu 
    C1_ s       0.387350   1.611779   1.182459   0.348923   1.018328   0.709849
    C2_ s       1.611543   0.386721   0.410832   0.914363   0.294390   0.620315
    H1_ s       0.000083   0.000526   0.138244   0.211178   0.085288   0.396889
    H2_ s       0.000104   0.000475   0.224884   0.002436   0.575105   0.014464
    H3_ s       0.000485   0.000060   0.028402   0.501047   0.002215   0.231243
 
   ao class       7Bu        8Bu        9Bu       10Bu       11Bu       12Bu 
    C1_ s       0.003549   0.005192   0.002573   0.003710   0.003148   0.002073
    C2_ s       0.005711   0.006185   0.003992   0.003227   0.002338   0.003685
    H1_ s       0.001722   0.000244   0.001268   0.000520   0.001332   0.000302
    H2_ s       0.000624   0.000457   0.000223   0.001668   0.001441   0.000240
    H3_ s       0.002643   0.000365   0.002511   0.000207   0.000518   0.000380
 
   ao class      13Bu       14Bu       15Bu       16Bu       17Bu       18Bu 
    C1_ s       0.002367   0.002649   0.000785   0.000480   0.000396   0.000210
    C2_ s       0.001834   0.000464   0.000965   0.000723   0.000385   0.000478
    H1_ s       0.000143   0.000538   0.000124   0.000071   0.000230   0.000018
    H2_ s       0.000200   0.000317   0.000229   0.000037   0.000121   0.000103
    H3_ s       0.000625   0.000011   0.000204   0.000391   0.000017   0.000158
 
   ao class      19Bu       20Bu       21Bu       22Bu       23Bu       24Bu 
    C1_ s       0.000415   0.000374   0.000232   0.000090   0.000113   0.000248
    C2_ s      -0.000026   0.000194   0.000257   0.000243   0.000190   0.000037
    H1_ s       0.000285   0.000048   0.000054   0.000010   0.000014   0.000093
    H2_ s       0.000247   0.000030   0.000064   0.000074   0.000045   0.000053
    H3_ s       0.000003   0.000082   0.000058   0.000130   0.000106   0.000002
 
   ao class      25Bu       26Bu       27Bu       28Bu       29Bu       30Bu 
    C1_ s       0.000192   0.000178   0.000133   0.000096   0.000036   0.000062
    C2_ s       0.000142   0.000065   0.000064   0.000060   0.000087   0.000072
    H1_ s       0.000011   0.000030   0.000003   0.000013   0.000027  -0.000004
    H2_ s      -0.000003   0.000001   0.000020   0.000007   0.000018   0.000006
    H3_ s       0.000050   0.000003   0.000017   0.000007   0.000003   0.000000
 
   ao class      31Bu       32Bu       33Bu       34Bu       35Bu       36Bu 
    C1_ s       0.000005   0.000027   0.000040  -0.000004   0.000007   0.000027
    C2_ s       0.000109   0.000035   0.000014   0.000046   0.000020   0.000017
    H1_ s       0.000002   0.000015   0.000007   0.000018   0.000028   0.000006
    H2_ s       0.000005   0.000005   0.000015   0.000012   0.000012   0.000017
    H3_ s       0.000001   0.000028   0.000011   0.000009   0.000005   0.000002
 
   ao class      37Bu       38Bu       39Bu       40Bu       41Bu       42Bu 
    C1_ s       0.000002   0.000017   0.000009   0.000012   0.000006   0.000004
    C2_ s       0.000011   0.000021   0.000015   0.000009   0.000012   0.000003
    H1_ s       0.000008   0.000010   0.000017   0.000005   0.000009   0.000019
    H2_ s       0.000010   0.000005   0.000012   0.000009   0.000019   0.000008
    H3_ s       0.000033   0.000009   0.000006   0.000024   0.000004   0.000012
 
   ao class      43Bu       44Bu       45Bu       46Bu       47Bu       48Bu 
    C1_ s       0.000004   0.000008   0.000004   0.000001   0.000003   0.000003
    C2_ s       0.000013   0.000007   0.000007   0.000001   0.000005   0.000002
    H1_ s       0.000009   0.000002   0.000004   0.000010   0.000003   0.000003
    H2_ s       0.000003   0.000015   0.000006   0.000006   0.000004   0.000003
    H3_ s       0.000006   0.000001   0.000006   0.000004   0.000003   0.000005
 
   ao class      49Bu       50Bu       51Bu       52Bu       53Bu 
    C1_ s      -0.000001   0.000001   0.000002   0.000001   0.000002
    C2_ s       0.000008   0.000000  -0.000001   0.000000   0.000001
    H1_ s       0.000001   0.000002   0.000003   0.000002   0.000000
    H2_ s       0.000001   0.000002   0.000003   0.000001   0.000001
    H3_ s       0.000005   0.000005   0.000000   0.000001   0.000001

                        Au  partial gross atomic populations
   ao class       1Au        2Au        3Au        4Au        5Au        6Au 
    C1_ s       0.682459   0.064909   0.002638   0.002181   0.001429   0.001049
    C2_ s       1.220618   0.035560   0.003217   0.002556   0.001255   0.001487
    H1_ s       0.006993   0.000564   0.000291   0.000047   0.000139   0.000114
    H2_ s       0.005461   0.000645   0.000017   0.000206   0.000292   0.000000
    H3_ s       0.013238   0.000364   0.000016   0.000381   0.000197   0.000100
 
   ao class       7Au        8Au        9Au       10Au       11Au       12Au 
    C1_ s       0.000976   0.000312   0.000052   0.000107   0.000053   0.000106
    C2_ s       0.000733   0.000277   0.000063   0.000032   0.000104   0.000153
    H1_ s       0.000143   0.000024   0.000005   0.000302   0.000003   0.000004
    H2_ s       0.000038   0.000004   0.000270   0.000001   0.000088   0.000000
    H3_ s       0.000108   0.000078   0.000130   0.000010   0.000088   0.000002
 
   ao class      13Au       14Au       15Au       16Au       17Au       18Au 
    C1_ s       0.000139   0.000086   0.000084   0.000033   0.000008   0.000001
    C2_ s       0.000069   0.000082   0.000020   0.000044   0.000035   0.000001
    H1_ s       0.000004   0.000000   0.000001   0.000002   0.000006   0.000010
    H2_ s       0.000000  -0.000001   0.000003   0.000004   0.000001   0.000012
    H3_ s       0.000001   0.000001   0.000001   0.000002   0.000016   0.000010
 
   ao class      19Au       20Au 
    C1_ s       0.000001   0.000001
    C2_ s       0.000002   0.000003
    H1_ s       0.000017   0.000000
    H2_ s       0.000005   0.000009
    H3_ s       0.000002   0.000010

                        Bg  partial gross atomic populations
   ao class       1Bg        2Bg        3Bg        4Bg        5Bg        6Bg 
    C1_ s       1.220473   0.018515   0.002826   0.003437   0.001647   0.000809
    C2_ s       0.632001   0.035168   0.002636   0.000334   0.001195   0.000998
    H1_ s       0.012013   0.000230   0.000008   0.000543   0.000059   0.000004
    H2_ s       0.013712   0.000175   0.000280   0.000053   0.000089   0.000186
    H3_ s       0.006315   0.000514   0.000301  -0.000003   0.000210   0.000171
 
   ao class       7Bg        8Bg        9Bg       10Bg       11Bg       12Bg 
    C1_ s       0.000056   0.000170   0.000107   0.000102   0.000125   0.000109
    C2_ s       0.000696   0.000288   0.000135   0.000064   0.000042   0.000147
    H1_ s       0.000171   0.000005   0.000017   0.000171   0.000051   0.000001
    H2_ s      -0.000001   0.000060   0.000099   0.000098   0.000066   0.000001
    H3_ s       0.000012   0.000116   0.000206   0.000015   0.000048   0.000004
 
   ao class      13Bg       14Bg       15Bg       16Bg       17Bg       18Bg 
    C1_ s       0.000135   0.000059   0.000047   0.000049   0.000011   0.000000
    C2_ s       0.000056   0.000106   0.000063   0.000025   0.000037   0.000000
    H1_ s       0.000005   0.000001   0.000001   0.000002   0.000012   0.000000
    H2_ s       0.000015   0.000001   0.000002   0.000001   0.000005   0.000014
    H3_ s       0.000000   0.000006   0.000001   0.000004   0.000001   0.000017
 
   ao class      19Bg       20Bg 
    C1_ s       0.000001   0.000002
    C2_ s       0.000001   0.000001
    H1_ s       0.000020   0.000001
    H2_ s       0.000001   0.000010
    H3_ s       0.000001   0.000008


                        gross atomic populations
     ao           C1_        C2_        H1_        H2_        H3_
      s        12.738113  12.099940   1.720442   1.710524   1.730982
    total      12.738113  12.099940   1.720442   1.710524   1.730982
 

 Total number of electrons:   30.00000000

