 
 program cidrt 5.9  

 distinct row table construction, reference csf selection, and internal
 walk selection for multireference single- and double-excitation
configuration interaction.

 references:  r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).
              h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. symp. 15, 91 (1981).

 based on the initial version by  Ron Shepard

 extended for spin-orbit CI calculations ( Russ Pitzer, OSU)

 and large active spaces (Thomas Müller, FZ Juelich)

 version date: 16-jul-04


 This Version of Program CIDRT is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIDRT       **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 workspace allocation parameters: lencor= 100000000 mem1=         1 ifirst=         1
 expanded "keystrokes" are being written to file:
 cidrtky                                                                         
 Spin-Orbit CI Calculation?(y,[n])
 Spin-Free Calculation
 
 input the spin multiplicity [  0]:
 spin multiplicity, smult            :   1    singlet 
 input the total number of electrons [  0]:
 total number of electrons, nelt     :    30
 input the number of irreps (1:8) [  0]:
 point group dimension, nsym         :     4
 enter symmetry labels:(y,[n])
 enter 4 labels (a4):
 enter symmetry label, default=   1
 enter symmetry label, default=   2
 enter symmetry label, default=   3
 enter symmetry label, default=   4
 symmetry labels: (symmetry, slabel)
 ( 1,  Ag ) ( 2,  Bu ) ( 3,  Au ) ( 4,  Bg ) 
 input nmpsy(*):
 nmpsy(*)=        53  53  20  20
 
   symmetry block summary
 block(*)=         1   2   3   4
 slabel(*)=      Ag  Bu  Au  Bg 
 nmpsy(*)=        53  53  20  20
 
 total molecular orbitals            :   146
 input the molecular spatial symmetry (irrep 1:nsym) [  0]:
 state spatial symmetry label        :  Ag 
 
 input the frozen core orbitals (sym(i),rmo(i),i=1,nfct):
 total frozen core orbitals, nfct    :     4
 
 fcorb(*)=         1   2   3   4
 slabel(*)=      Ag  Ag  Ag  Ag 
 
 number of frozen core orbitals      :     4
 number of frozen core electrons     :     8
 number of internal electrons        :    22
 
 input the frozen virtual orbitals (sym(i),rmo(i),i=1,nfvt):
 total frozen virtual orbitals, nfvt :     0

 no frozen virtual orbitals entered
 
 input the internal orbitals (sym(i),rmo(i),i=1,niot):
 niot                                :    13
 
 modrt(*)=         5   6   7  54  55  56  57  58  59 107 108 127 128
 slabel(*)=      Ag  Ag  Ag  Bu  Bu  Bu  Bu  Bu  Bu  Au  Au  Bg  Bg 
 
 total number of orbitals            :   146
 number of frozen core orbitals      :     4
 number of frozen virtual orbitals   :     0
 number of internal orbitals         :    13
 number of external orbitals         :   129
 
 orbital-to-level mapping vector
 map(*)=          -1  -1  -1  -1 130 131 132   1   2   3   4   5   6   7   8
                   9  10  11  12  13  14  15  16  17  18  19  20  21  22  23
                  24  25  26  27  28  29  30  31  32  33  34  35  36  37  38
                  39  40  41  42  43  44  45  46 133 134 135 136 137 138  47
                  48  49  50  51  52  53  54  55  56  57  58  59  60  61  62
                  63  64  65  66  67  68  69  70  71  72  73  74  75  76  77
                  78  79  80  81  82  83  84  85  86  87  88  89  90  91  92
                  93 139 140  94  95  96  97  98  99 100 101 102 103 104 105
                 106 107 108 109 110 111 141 142 112 113 114 115 116 117 118
                 119 120 121 122 123 124 125 126 127 128 129
 
 input the number of ref-csf doubly-occupied orbitals [  0]:
 (ref) doubly-occupied orbitals      :     3
 
 no. of internal orbitals            :    13
 no. of doubly-occ. (ref) orbitals   :     3
 no. active (ref) orbitals           :    10
 no. of active electrons             :    16
 
 input the active-orbital, active-electron occmnr(*):
  54 55 56 57 58 59107108127128
 input the active-orbital, active-electron occmxr(*):
  54 55 56 57 58 59107108127128
 
 actmo(*) =       54  55  56  57  58  59 107 108 127 128
 occmnr(*)=        0   0   0   0   0   0   0   0   0  16
 occmxr(*)=       16  16  16  16  16  16  16  16  16  16
 reference csf cumulative electron occupations:
 modrt(*)=         5   6   7  54  55  56  57  58  59 107 108 127 128
 occmnr(*)=        2   4   6   6   6   6   6   6   6   6   6   6  22
 occmxr(*)=        2   4   6  22  22  22  22  22  22  22  22  22  22
 
 input the active-orbital bminr(*):
  54 55 56 57 58 59107108127128
 input the active-orbital bmaxr(*):
  54 55 56 57 58 59107108127128
 reference csf b-value constraints:
 modrt(*)=         5   6   7  54  55  56  57  58  59 107 108 127 128
 bminr(*)=         0   0   0   0   0   0   0   0   0   0   0   0   0
 bmaxr(*)=         0   0   0   4   4   4   4   4   4   4   4   4   4
 input the active orbital smaskr(*):
  54 55 56 57 58 59107108127128
 modrt:smaskr=
   5:1000   6:1000   7:1000  54:1111  55:1111  56:1111  57:1111  58:1111
  59:1111 107:1111 108:1111 127:1111 128:1111
 
 input the maximum excitation level from the reference csfs [  2]:
 maximum excitation from ref. csfs:  :     2
 number of internal electrons:       :    22
 
 input the internal-orbital mrsdci occmin(*):
   5  6  7 54 55 56 57 58 59107108127128
 input the internal-orbital mrsdci occmax(*):
   5  6  7 54 55 56 57 58 59107108127128
 mrsdci csf cumulative electron occupations:
 modrt(*)=         5   6   7  54  55  56  57  58  59 107 108 127 128
 occmin(*)=        0   0   0   0   0   0   0   0   0   0   0   0  20
 occmax(*)=       22  22  22  22  22  22  22  22  22  22  22  22  22
 
 input the internal-orbital mrsdci bmin(*):
   5  6  7 54 55 56 57 58 59107108127128
 input the internal-orbital mrsdci bmax(*):
   5  6  7 54 55 56 57 58 59107108127128
 mrsdci b-value constraints:
 modrt(*)=         5   6   7  54  55  56  57  58  59 107 108 127 128
 bmin(*)=          0   0   0   0   0   0   0   0   0   0   0   0   0
 bmax(*)=         22  22  22  22  22  22  22  22  22  22  22  22  22
 
 input the internal-orbital smask(*):
   5  6  7 54 55 56 57 58 59107108127128
 modrt:smask=
   5:1111   6:1111   7:1111  54:1111  55:1111  56:1111  57:1111  58:1111
  59:1111 107:1111 108:1111 127:1111 128:1111
 
 internal orbital summary:
 block(*)=         1   1   1   2   2   2   2   2   2   3   3   4   4
 slabel(*)=      Ag  Ag  Ag  Bu  Bu  Bu  Bu  Bu  Bu  Au  Au  Bg  Bg 
 rmo(*)=           5   6   7   1   2   3   4   5   6   1   2   1   2
 modrt(*)=         5   6   7  54  55  56  57  58  59 107 108 127 128
 
 reference csf info:
 occmnr(*)=        2   4   6   6   6   6   6   6   6   6   6   6  22
 occmxr(*)=        2   4   6  22  22  22  22  22  22  22  22  22  22
 
 bminr(*)=         0   0   0   0   0   0   0   0   0   0   0   0   0
 bmaxr(*)=         0   0   0   4   4   4   4   4   4   4   4   4   4
 
 
 mrsdci csf info:
 occmin(*)=        0   0   0   0   0   0   0   0   0   0   0   0  20
 occmax(*)=       22  22  22  22  22  22  22  22  22  22  22  22  22
 
 bmin(*)=          0   0   0   0   0   0   0   0   0   0   0   0   0
 bmax(*)=         22  22  22  22  22  22  22  22  22  22  22  22  22
 

 a priori removal of distinct rows:

 input the level, a, and b values for the vertices 
 to be removed (-1/ to end).

 input level, a, and b (-1/ to end):
 no vertices marked for removal
 
 impose generalized interacting space restrictions?(y,[n])
 generalized interacting space restrictions will be imposed.
 multp 0 0 0 0 0 0 0 0 0
 spnir
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  hmult 0
 lxyzir 0 0 0
 spnir
  1  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  1  0  0  0  1  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  1  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  1  0  0  0  1  0  0  0  1  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  1  0  0  0  1  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0

 number of rows in the drt : 124
     5 arcs removed due to generalized interacting space restrictions.

 manual arc removal step:


 input the level, a, b, and step values 
 for the arcs to be removed (-1/ to end).

 input the level, a, b, and step (-1/ to end):
 remarc:   0 arcs removed out of   0 specified.

 xbarz=    2280
 xbary=   12380
 xbarx=   24080
 xbarw=   25830
        --------
 nwalk=   64570
 input the range of drt levels to print (l1,l2):
 levprt(*)        -1   0

 reference-csf selection step 1:
 total number of z-walks in the drt, nzwalk=    2280

 input the list of allowed reference symmetries:
 allowed reference symmetries:             1
 allowed reference symmetry labels:      Ag 
 keep all of the z-walks as references?(y,[n])
 all z-walks are initially deleted.
 
 generate walks while applying reference drt restrictions?([y],n)
 reference drt restrictions will be imposed on the z-walks.
 
 impose additional orbital-group occupation restrictions?(y,[n])
 
 apply primary reference occupation restrictions?(y,[n])
 
 manually select individual walks?(y,[n])

 step 1 reference csf selection complete.
      273 csfs initially selected from    2280 total walks.

 beginning step-vector based selection.
 enter [internal_orbital_step_vector/disposition] pairs:

 enter internal orbital step vector, (-1/ to end):
   5  6  7 54 55 56 57 58 59107108127128

 step 2 reference csf selection complete.
      273 csfs currently selected from    2280 total walks.

 beginning numerical walk based selection.
 enter positive walk numbers to add walks,
 negative walk numbers to delete walks, and zero to end:

 input reference walk number (0 to end) [  0]:

 numerical walk-number based selection complete.
      273 reference csfs selected from    2280 total z-walks.
 
 input the reference occupations, mu(*):
 reference occupations:
 mu(*)=            2   2   2   2   2   2   2   0   0   0   0   0   0
 
 interacting space determination:
 checking diagonal loops...
 checking 2-internal loops...
 checking 3-internal loops...
 checking 4-internal loops...
 !timer: limint() required                   user=     0.310 walltime=     1.000
 
 this is an obsolete prompt.(y,[n])

 final mrsdci walk selection step:

 nvalw(*)=     630   11283   14835   13698 nvalwt=   40446

 enter positive walk numbers to add walks,
 negative walk numbers to delete walks, and zero to end.

 input mrsdci walk number (0 to end) [  0]:

 end of manual mrsdci walk selection.
 number added=   0 number removed=   0

 nvalw(*)=     630   11283   14835   13698 nvalwt=   40446


 lprune: l(*,*,*) pruned with nwalk=   64570 nvalwt=   40446
 lprune:  z-drt, nprune=   201
 lprune:  y-drt, nprune=   112
 lprune: wx-drt, nprune=    52

 xbarz=    1151
 xbary=   11715
 xbarx=   19281
 xbarw=   19800
        --------
 nwalk=   51947
 levprt(*)        -1   0

 beginning the reference csf index recomputation...

     iref   iwalk  step-vector
   ------  ------  ------------
        1       1  3333333333300
        2       2  3333333333030
        3       3  3333333333012
        4       4  3333333333003
        5       5  3333333331230
        6       6  3333333331212
        7       7  3333333331203
        8       8  3333333331122
        9       9  3333333330330
       10      10  3333333330312
       11      11  3333333330303
       12      14  3333333330033
       13      15  3333333303330
       14      16  3333333303312
       15      17  3333333303303
       16      20  3333333303033
       17      23  3333333301233
       18      24  3333333300333
       19      25  3333333123330
       20      26  3333333123312
       21      27  3333333123303
       22      30  3333333123033
       23      33  3333333121233
       24      34  3333333120333
       25      35  3333333113322
       26      40  3333333112233
       27      41  3333333033330
       28      42  3333333033312
       29      43  3333333033303
       30      46  3333333033033
       31      49  3333333031233
       32      50  3333333030333
       33      51  3333333003333
       34      52  3333331323330
       35      53  3333331323312
       36      54  3333331323303
       37      57  3333331323033
       38      60  3333331321233
       39      61  3333331320333
       40      62  3333331313322
       41      67  3333331312233
       42      68  3333331233330
       43      69  3333331233312
       44      70  3333331233303
       45      73  3333331233033
       46      76  3333331231233
       47      77  3333331230333
       48      78  3333331203333
       49      79  3333331133322
       50      84  3333331132233
       51      85  3333331023333
       52      86  3333330333330
       53      87  3333330333312
       54      88  3333330333303
       55      91  3333330333033
       56      94  3333330331233
       57      95  3333330330333
       58      96  3333330303333
       59      97  3333330123333
       60      98  3333330033333
       61      99  3333313323330
       62     100  3333313323312
       63     101  3333313323303
       64     104  3333313323033
       65     107  3333313321233
       66     108  3333313320333
       67     109  3333313313322
       68     114  3333313312233
       69     115  3333313233330
       70     116  3333313233312
       71     117  3333313233303
       72     120  3333313233033
       73     123  3333313231233
       74     124  3333313230333
       75     125  3333313203333
       76     126  3333313133322
       77     131  3333313132233
       78     132  3333313023333
       79     133  3333312333330
       80     134  3333312333312
       81     135  3333312333303
       82     138  3333312333033
       83     141  3333312331233
       84     142  3333312330333
       85     143  3333312303333
       86     144  3333312123333
       87     145  3333312033333
       88     146  3333311333322
       89     151  3333311332233
       90     152  3333311223333
       91     153  3333310323333
       92     154  3333310233333
       93     155  3333303333330
       94     156  3333303333312
       95     157  3333303333303
       96     160  3333303333033
       97     163  3333303331233
       98     164  3333303330333
       99     165  3333303303333
      100     166  3333303123333
      101     167  3333303033333
      102     168  3333301323333
      103     169  3333301233333
      104     170  3333300333333
      105     171  3333133323330
      106     172  3333133323312
      107     173  3333133323303
      108     176  3333133323033
      109     179  3333133321233
      110     180  3333133320333
      111     181  3333133313322
      112     186  3333133312233
      113     187  3333133233330
      114     188  3333133233312
      115     189  3333133233303
      116     192  3333133233033
      117     195  3333133231233
      118     196  3333133230333
      119     197  3333133203333
      120     198  3333133133322
      121     203  3333133132233
      122     204  3333133023333
      123     205  3333132333330
      124     206  3333132333312
      125     207  3333132333303
      126     210  3333132333033
      127     213  3333132331233
      128     214  3333132330333
      129     215  3333132303333
      130     216  3333132123333
      131     217  3333132033333
      132     218  3333131333322
      133     223  3333131332233
      134     224  3333131223333
      135     225  3333130323333
      136     226  3333130233333
      137     227  3333123333330
      138     228  3333123333312
      139     229  3333123333303
      140     232  3333123333033
      141     235  3333123331233
      142     236  3333123330333
      143     237  3333123303333
      144     238  3333123123333
      145     239  3333123033333
      146     240  3333121323333
      147     241  3333121233333
      148     242  3333120333333
      149     243  3333113333322
      150     248  3333113332233
      151     249  3333113223333
      152     250  3333112323333
      153     251  3333112233333
      154     252  3333103323333
      155     253  3333103233333
      156     254  3333102333333
      157     255  3333033333330
      158     256  3333033333312
      159     257  3333033333303
      160     260  3333033333033
      161     263  3333033331233
      162     264  3333033330333
      163     265  3333033303333
      164     266  3333033123333
      165     267  3333033033333
      166     268  3333031323333
      167     269  3333031233333
      168     270  3333030333333
      169     271  3333013323333
      170     272  3333013233333
      171     273  3333012333333
      172     274  3333003333333
      173     275  3331333323330
      174     276  3331333323312
      175     277  3331333323303
      176     280  3331333323033
      177     283  3331333321233
      178     284  3331333320333
      179     285  3331333313322
      180     290  3331333312233
      181     291  3331333233330
      182     292  3331333233312
      183     293  3331333233303
      184     296  3331333233033
      185     299  3331333231233
      186     300  3331333230333
      187     301  3331333203333
      188     302  3331333133322
      189     307  3331333132233
      190     308  3331333023333
      191     309  3331332333330
      192     310  3331332333312
      193     311  3331332333303
      194     314  3331332333033
      195     317  3331332331233
      196     318  3331332330333
      197     319  3331332303333
      198     320  3331332123333
      199     321  3331332033333
      200     322  3331331333322
      201     327  3331331332233
      202     328  3331331223333
      203     329  3331330323333
      204     330  3331330233333
      205     331  3331323333330
      206     332  3331323333312
      207     333  3331323333303
      208     336  3331323333033
      209     339  3331323331233
      210     340  3331323330333
      211     341  3331323303333
      212     342  3331323123333
      213     343  3331323033333
      214     344  3331321323333
      215     345  3331321233333
      216     346  3331320333333
      217     347  3331313333322
      218     352  3331313332233
      219     353  3331313223333
      220     354  3331312323333
      221     355  3331312233333
      222     356  3331303323333
      223     357  3331303233333
      224     358  3331302333333
      225     359  3331233333330
      226     360  3331233333312
      227     361  3331233333303
      228     364  3331233333033
      229     367  3331233331233
      230     368  3331233330333
      231     369  3331233303333
      232     370  3331233123333
      233     371  3331233033333
      234     372  3331231323333
      235     373  3331231233333
      236     374  3331230333333
      237     375  3331213323333
      238     376  3331213233333
      239     377  3331212333333
      240     378  3331203333333
      241     379  3331133333322
      242     384  3331133332233
      243     385  3331133223333
      244     386  3331132323333
      245     387  3331132233333
      246     388  3331123323333
      247     389  3331123233333
      248     390  3331122333333
      249     391  3331033323333
      250     392  3331033233333
      251     393  3331032333333
      252     394  3331023333333
      253     395  3330333333330
      254     396  3330333333312
      255     397  3330333333303
      256     400  3330333333033
      257     403  3330333331233
      258     404  3330333330333
      259     405  3330333303333
      260     406  3330333123333
      261     407  3330333033333
      262     408  3330331323333
      263     409  3330331233333
      264     410  3330330333333
      265     411  3330313323333
      266     412  3330313233333
      267     413  3330312333333
      268     414  3330303333333
      269     415  3330133323333
      270     416  3330133233333
      271     417  3330132333333
      272     418  3330123333333
      273     419  3330033333333
 indx01:   273 elements set in vec01(*)

 beginning the valid upper walk index recomputation...
 indx01: 40446 elements set in vec01(*)

 beginning the final csym(*) computation...

  number of valid internal walks of each symmetry:

       Ag      Bu      Au      Bg 
      ----    ----    ----    ----
 z     630       0       0       0
 y    2377    3078    2914    2914
 x    2537    3958    4152    4188
 w    3016    3358    3644    3680

 csfs grouped by internal walk symmetry:

       Ag      Bu      Au      Bg 
      ----    ----    ----    ----
 z     630       0       0       0
 y  109342  144666   52452   52452
 x 6144614 9839588 6950448 7010712
 w 7693816 8347988 6100056 6160320

 total csf counts:
 z-vertex:      630
 y-vertex:   358912
 x-vertex: 29945362
 w-vertex: 28302180
           --------
 total:    58607084
 
 input a title card, default=cidrt_title
 title card:
  cidrt_title                                                                    
 
 input a drt file name, default=cidrtfl
 drt and indexing arrays will be written to file:
 cidrtfl                                                                         
 
 write the drt file?([y],n)
 drt file is being written...
 !timer: cidrt required                      user=     0.380 walltime=     1.000
