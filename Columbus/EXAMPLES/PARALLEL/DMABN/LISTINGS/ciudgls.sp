1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      2009-03.    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


================ Computing sorting integral file structure ================

                    -----z----- -----y----- -----x----- -----w----- ---total---

                CSFs      2955      965697    28646604    31218914    60834170
      internal walks      8556       27450       15045       16149       67200
valid internal walks      2955       27450        8445        8999       47849
 getinfoarray: info=                     6 :                     1
                  8192                  6552                  8192
                  5460                     0
 icd(3)=                 93606 ci%nnlev=                 18721  l2rec=
                  8192  n2max=                  5460
 lcore1,lcore2=             524140181             524153117
 lencor,maxblo             524288000                 60000
========================================
 current settings:
 minbl3       11745
 minbl4       11745
 locmaxbl3   834704
 locmaxbuf   417352
 maxbl3       60000
 maxbl3       60000
 maxbl4       60000
 maxbuf       30006
========================================

 sorted 4-external integrals:   867 records of integral w-combinations 
                                865 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=   87.77   85.55)

 sorted 3-external integrals:   672 records of integral w-combinations 
                                672 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=   82.89   81.70)
 Orig.  diagonal integrals:  1electron:       193
                             0ext.    :       930
                             2ext.    :      9780
                             4ext.    :     26732


 Orig. off-diag. integrals:  4ext.    :  23440059
                             3ext.    :  16933662
                             2ext.    :   4795650
                             1ext.    :    629778
                             0ext.    :     32256
                             2ext. SO :         0
                             1ext. SO :         0
                             0ext. SO :         0
                             1electron:      3916


 Sorted integrals            3ext.  w :  16701354 x :  16469046
                             4ext.  w :  22805667 x :  22180725


Cycle #  1 sortfile size=  70254592(     134 records of   524288) #buckets=   2
distributed memory consumption per node=   8404132 available core 515748985
Cycle #  2 sortfile size=  70254592(     134 records of   524288) #buckets=   2
distributed memory consumption per node=   8404132 available core 515748985
 minimum size of srtscr:  69206016 WP (   132 records)
 maximum size of srtscr:  70254592 WP (   134 records)
diagi   file:      9 records  of   6144 WP each=>      55296 WP total
ofdgi   file:    891 records  of   6144 WP each=>    5474304 WP total
fil3w   file:    672 records  of  30006 WP each=>   20164032 WP total
fil3x   file:    672 records  of  30006 WP each=>   20164032 WP total
fil4w   file:    867 records  of  30006 WP each=>   26015202 WP total
fil4x   file:    865 records  of  30006 WP each=>   25955190 WP total
 compressed index vector length=                 16631
nrow=    2115valw=   95698indx=   16631maxseg=      19total=  169298
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 0,
  GSET = 0,
   DAVCOR =10,
  NCOREL = 56
  NROOT = 1
  IVMODE = 3
  NBKITR = 1
  NVBKMN = 1
  RTOLBK = 1e-3,
  NITER = 20
  NVCIMN = 3
  RTOLCI = 5e-3,
  NVCIMX = 6
  NVRFMX = 6
  NVBKMX = 6
  IDEN  = 1
  CSFPRN = 10,
  FINALV = 0
  FINALW =-1
  CDG4EX =1, C2EX0EX=0,C3EX1EX=0
  nseg4x=1,2,6,6
  nseg3x=1,3,3,3
  nseg2x=2,2,2,6
  nsegwx=1,1,4,4
  nseg1x=2,2,4,3
  nseg0x=7,2,5,5
  maxseg=19
  &end
 ------------------------------------------------------------------------
lodens (list->root)=  1
invlodens (root->list)=  1
 USING SEGMENTS OF EQUAL SIZE

****************  list of control variables  ****************
 lvlprt =    0      nroot  =    1      noldv  =   0      noldhv =   0
 nunitv =    3      nbkitr =    1      niter  =  20      davcor =  10
 csfprn =   10      ivmode =    3      istrt  =   0      vout   =   0
 iortls =    0      nvbkmx =    6      ibktv  =  -1      ibkthv =  -1
 nvcimx =    6      icitv  =   -1      icithv =  -1      frcsub =   0
 nvbkmn =    1      nvcimn =    3      maxseg =  19      nrfitr =  30
 ncorel =   56      nvrfmx =    6      nvrfmn =   3      iden   =   1
 itran  =    0      froot  =    0      rtmode =   0      ncouple=   1
 skipso =    F      dalton2=    0      molcas =   0      finalv =   0
 finalw =   -1      cosmocalc=   0    with_tsklst=   0
 nsegwx =    1     1     4     4
 nseg0x =    7     2     5     5
 nseg1x =    2     2     4     3
 nseg2x =    2     2     2     6
 nseg3x =    1     3     3     3
 nseg4x =    1     2     6     6
 no0ex  =      0    no1ex  =      0    no2ex  =     0    no3ex  =     0
 no4ex  =      0    nodiag =      0
 cdg4ex =    1      c3ex1ex=    0      c2ex0ex=   0
 fileloc=    2     2     2     2     2     2     2     1     2     2
 directhd=   1      noaqccshift_zyxw=      0
 critical_crit=-1.00000    critical_delta= 0.05000

 ctol   = 0.010000    lrtshift=1.000000    smalld =0.001000


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-03    5.000E-03
 Computing density:                    .drt1.state1
 forcing FILELOC=2,2,2,2,2,2,2,2,2,2
****************************************************************
*** using    1 nodes and   20 cores.
*** diagint:    0.021 MB/core
*** diagint:    0.422 MB/node
*** ofdgint:    2.088 MB/core
*** ofdgint:   41.766 MB/node
****************************************************************
 Main memory management:
 global         36827780 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core          487460220 DP per process
 gapointer%node_offset(*)=                     0                     0
 gapointer%node_width(*)=               5474320                 55300
  asynchronous I/O for fil3w enabled ..
  asynchronous I/O for fil3x enabled ..
  asynchronous I/O for fil4w enabled ..
  asynchronous I/O for fil4x enabled ..
 Sorting in memory ...               70254592             790844210
 changing lcore_cisrt to             523489700

********** Integral sort section *************


 workspace allocation information: lencor= 523489700

 echo of the input for program cisrt:
 ------------------------------------------------------------------------
  &input
  maxbl3=60000
  maxbl4=60000
  &end
 ------------------------------------------------------------------------
 
 ( 6) listing file:                    ciudgls             
 ( 5) input file:                      cisrtin   
 (17) cidrt file:                      cidrtfl             
 (11) transformed integrals file:      moints    
 (12) diagonal integral file:          diagint             
 (13) off-diagonal integral file:      ofdgint             
 (31) 4-external w integrals file:     fil4w               
 (32) 4-external x integrals file:     fil4x               
 (33) 3-external w integrals file:     fil3w               
 (34) 3-external x integrals file:     fil3x               
 (21) scratch da sorting file:         srtscr              
 (12) 2-e integral file [fsplit=2]:    moints2   

 input integral file header information:
 Hermit Integral Program : SIFS version  login002.cm.clust 10:44:46.492 31-Oct-22
  cidrt_title                                                                    
 MO-coefficients from mcscf.x                                                    
  with dummy occupation 1.0 for active orbitals                                  
  total ao core energy =  546.173514343                                          
 MCSCF energy =    -455.510259275                                                
 SIFS file created by program tran.      login002.cm.clust 10:44:54.899 31-Oct-22

 input energy(*) values:
 energy( 1)=  5.461735143427E+02, ietype=   -1,    core energy of type: Nuc.Rep.

 total core energy =   5.461735143427E+02

 nsym = 4 nmot= 193

 symmetry  =    1    2    3    4
 slabel(*) =  A1   B1   B2   A2 
 nmpsy(*)  =   74   35   59   25

 info(*) =          1      8192      6552      8192      5460         0

 orbital labels, i:molab(i)=
   1:tout:001   2:tout:002   3:tout:003   4:tout:004   5:tout:005   6:tout:006   7:tout:007   8:tout:008   9:tout:009  10:tout:010
  11:tout:011  12:tout:012  13:tout:013  14:tout:014  15:tout:015  16:tout:016  17:tout:017  18:tout:018  19:tout:019  20:tout:020
  21:tout:021  22:tout:022  23:tout:023  24:tout:024  25:tout:025  26:tout:026  27:tout:027  28:tout:028  29:tout:029  30:tout:030
  31:tout:031  32:tout:032  33:tout:033  34:tout:034  35:tout:035  36:tout:036  37:tout:037  38:tout:038  39:tout:039  40:tout:040
  41:tout:041  42:tout:042  43:tout:043  44:tout:044  45:tout:045  46:tout:046  47:tout:047  48:tout:048  49:tout:049  50:tout:050
  51:tout:051  52:tout:052  53:tout:053  54:tout:054  55:tout:055  56:tout:056  57:tout:057  58:tout:058  59:tout:059  60:tout:060
  61:tout:061  62:tout:062  63:tout:063  64:tout:064  65:tout:065  66:tout:066  67:tout:067  68:tout:068  69:tout:069  70:tout:070
  71:tout:071  72:tout:072  73:tout:073  74:tout:074  75:tout:075  76:tout:076  77:tout:077  78:tout:078  79:tout:079  80:tout:080
  81:tout:081  82:tout:082  83:tout:083  84:tout:084  85:tout:085  86:tout:086  87:tout:087  88:tout:088  89:tout:089  90:tout:090
  91:tout:091  92:tout:092  93:tout:093  94:tout:094  95:tout:095  96:tout:096  97:tout:097  98:tout:098  99:tout:099 100:tout:100
 101:tout:101 102:tout:102 103:tout:103 104:tout:104 105:tout:105 106:tout:106 107:tout:107 108:tout:108 109:tout:109 110:tout:110
 111:tout:111 112:tout:112 113:tout:113 114:tout:114 115:tout:115 116:tout:116 117:tout:117 118:tout:118 119:tout:119 120:tout:120
 121:tout:121 122:tout:122 123:tout:123 124:tout:124 125:tout:125 126:tout:126 127:tout:127 128:tout:128 129:tout:129 130:tout:130
 131:tout:131 132:tout:132 133:tout:133 134:tout:134 135:tout:135 136:tout:136 137:tout:137 138:tout:138 139:tout:139 140:tout:140
 141:tout:141 142:tout:142 143:tout:143 144:tout:144 145:tout:145 146:tout:146 147:tout:147 148:tout:148 149:tout:149 150:tout:150
 151:tout:151 152:tout:152 153:tout:153 154:tout:154 155:tout:155 156:tout:156 157:tout:157 158:tout:158 159:tout:159 160:tout:160
 161:tout:161 162:tout:162 163:tout:163 164:tout:164 165:tout:165 166:tout:166 167:tout:167 168:tout:168 169:tout:169 170:tout:170
 171:tout:171 172:tout:172 173:tout:173 174:tout:174 175:tout:175 176:tout:176 177:tout:177 178:tout:178 179:tout:179 180:tout:180
 181:tout:181 182:tout:182 183:tout:183 184:tout:184 185:tout:185 186:tout:186 187:tout:187 188:tout:188 189:tout:189 190:tout:190
 191:tout:191 192:tout:192 193:tout:193

 input parameters:
 prnopt=  0
 ldamin=   32767 ldamax=  524288 ldainc=    4096
 maxbuf=   30006 maxbl3=   60000 maxbl4=   60000 intmxo=    6144
  Using 32 bit compression 

 drt information:
  cidrt_title                                                                    
 nmotd = 204 nfctd =  11 nfvtc =   0 nmot  = 193
 nlevel = 193 niot  =  30 lowinl= 164
 orbital-to-level map(*)
   -1  -1  -1  -1  -1  -1  -1  -1 164 165 166 167 168 169 170 171 172 173 174 175
    1   2   3   4   5   6   7   8   9  10  11  12  13  14  15  16  17  18  19  20
   21  22  23  24  25  26  27  28  29  30  31  32  33  34  35  36  37  38  39  40
   41  42  43  44  45  46  47  48  49  50  51  52  53  54  55  56  57  58  59  60
   61  62 176 177 178 189 190 191  63  64  65  66  67  68  69  70  71  72  73  74
   75  76  77  78  79  80  81  82  83  84  85  86  87  88  89  90  91  -1  -1  -1
  179 180 181 182 183 184 185 186 187  92  93  94  95  96  97  98  99 100 101 102
  103 104 105 106 107 108 109 110 111 112 113 114 115 116 117 118 119 120 121 122
  123 124 125 126 127 128 129 130 131 132 133 134 135 136 137 138 139 140 141 188
  192 193 142 143 144 145 146 147 148 149 150 151 152 153 154 155 156 157 158 159
  160 161 162 163
 compressed map(*)
  164 165 166 167 168 169 170 171 172 173 174 175   1   2   3   4   5   6   7   8
    9  10  11  12  13  14  15  16  17  18  19  20  21  22  23  24  25  26  27  28
   29  30  31  32  33  34  35  36  37  38  39  40  41  42  43  44  45  46  47  48
   49  50  51  52  53  54  55  56  57  58  59  60  61  62 176 177 178 189 190 191
   63  64  65  66  67  68  69  70  71  72  73  74  75  76  77  78  79  80  81  82
   83  84  85  86  87  88  89  90  91 179 180 181 182 183 184 185 186 187  92  93
   94  95  96  97  98  99 100 101 102 103 104 105 106 107 108 109 110 111 112 113
  114 115 116 117 118 119 120 121 122 123 124 125 126 127 128 129 130 131 132 133
  134 135 136 137 138 139 140 141 188 192 193 142 143 144 145 146 147 148 149 150
  151 152 153 154 155 156 157 158 159 160 161 162 163
 levsym(*)
    1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1
    1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1
    1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1
    1   1   2   2   2   2   2   2   2   2   2   2   2   2   2   2   2   2   2   2
    2   2   2   2   2   2   2   2   2   2   2   3   3   3   3   3   3   3   3   3
    3   3   3   3   3   3   3   3   3   3   3   3   3   3   3   3   3   3   3   3
    3   3   3   3   3   3   3   3   3   3   3   3   3   3   3   3   3   3   3   3
    3   4   4   4   4   4   4   4   4   4   4   4   4   4   4   4   4   4   4   4
    4   4   4   1   1   1   1   1   1   1   1   1   1   1   1   2   2   2   3   3
    3   3   3   3   3   3   3   4   2   2   2   4   4
 repartitioning mu(*)=
   2.  2.  2.  2.  2.  2.  2.  2.  2.  2.  2.  2.  2.  2.  2.  2.  2.  2.  2.  2.
   2.  2.  2.  2.  2.  0.  0.  0.  0.  0.

 new core energy added to the energy(*) list.
 from the integral file: h1_core= -6.474852489814E+02

 indxdg: diagonal integral statistics.
 total number of integrals contributing to diagonal matrix elements:     37442
 number with all external indices:     26732
 number with half external - half internal indices:      9780
 number with all internal indices:       930

 indxof: off-diagonal integral statistics.
    4-external integrals: num=   23440059 strt=          1
    3-external integrals: num=   16933662 strt=   23440060
    2-external integrals: num=    4795650 strt=   40373722
    1-external integrals: num=     629778 strt=   45169372
    0-external integrals: num=      32256 strt=   45799150

 total number of off-diagonal integrals:    45831405


 indxof(2nd)  ittp=   3 numx(ittp)=     4795650
 indxof(2nd)  ittp=   4 numx(ittp)=      629778
 indxof(2nd)  ittp=   5 numx(ittp)=       32256

 intermediate da file sorting parameters:
 nbuk=   2 lendar=  524288 nipbk=  349524 nipsg= 522477153
 pro2e        1   18722   37443   56164   74885   75350   75815   94536  793584 1492632
  2016920 2025112 2030572 2052411

 pro2e:  44794126 integrals read in  8205 records.

 pro2e:         0 integrals 34-ext integrals skipped.
 pro1e        1   18722   37443   56164   74885   75350   75815   94536  793584 1492632
  2016920 2025112 2030572 2052411
 pro1e: eref =   -3.488342881411210E+02
 total size of srtscr:                   134  records of                 524288 
 WP =             562036736 Bytes
 !timer: first half-sort required        cpu_time=     1.431 walltime=     1.435

 new core energy added to the energy(*) list.
 from the hamiltonian repartitioning, eref= -3.488342881411E+02
 putdg        1   18722   37443   56164   62308  586596  936121   94536  793584 1492632
  2016920 2025112 2030572 2052411

 putf:       9 buffers of length    6144 written to file 12
 diagonal integral file completed.

 putd34:   867 records of integral w-combinations and
           865 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=   87.77   85.55
 prep4e:   868 blocks of linear combinations of 4-external integrals processed.
 number of sorted 4-external integrals   44986392
 number of original 4-external integrals 23440059


 putf34: external integral file complete. nfilw=    31 nfilx=    32 nrecw=   867 nrecx=   865 lbufp= 30006

 putd34:   672 records of integral w-combinations and
           672 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=   82.89   81.70
 prep3e:   672 blocks of linear combinations of 3-external integrals processed.
 number of sorted 3-external integrals   33170400
 number of original 3-external integrals 16933662


 putf34: external integral file complete. nfilw=    33 nfilx=    34 nrecw=   672 nrecx=   672 lbufp= 30006
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd   4795650         3  40373722  40373722   4795650  45831405
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd    629778         4  45169372  45169372    629778  45831405
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd     32256         5  45799150  45799150     32256  45831405

 putf:     891 buffers of length    6144 written to file 13
 off-diagonal files sort completed.
 !timer: second half-sort required       cpu_time=     1.047 walltime=     1.051
 !timer: cisrt complete                  cpu_time=     2.483 walltime=     2.492
 deleting integral sort shared memory
 executing brd_struct for cisrtinfo
cisrtinfo:
bufszi  6144
 diagfile 4ext:   26732 2ext:    9780 0ext:     930
 fil4w,fil4x  :23440059 fil3w,fil3x :16933662
 ofdgint  2ext: 4795650 1ext:  629778 0ext:   32256so0ext:       0so1ext:       0so2ext:       0
buffer minbl4   11745 minbl3   11745 maxbl2   11748nbas:  62  29  50  22   0   0   0   0 maxbuf 30006
 CIUDG version 5.9.7 ( 5-Oct-2004)

 workspace allocation information: lcore= 487460220

 core energy values from the integral file:
 energy( 1)=  5.461735143427E+02, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -6.474852489814E+02, ietype=    6,   fcore energy of type: H1(*)   
 energy( 3)= -3.488342881411E+02, ietype=    5,   fcore energy of type: Vref(*) 

 total core repulsion energy = -4.501460227798E+02
 nmot  =   204 niot  =    30 nfct  =    11 nfvt  =     0
 nrow  =   235 nsym  =     4 ssym  =     3 lenbuf=  1600
 nwalk,xbar:      67200     8556    27450    15045    16149
 nvalwt,nvalw:    47849     2955    27450     8445     8999
 ncsft:        60834170
 total number of valid internal walks:   47849
 nvalz,nvaly,nvalx,nvalw =     2955   27450    8445    8999

 cisrt info file parameters:
 file number  12 blocksize   6144
 mxbld  30720
 nd4ext,nd2ext,nd0ext 26732  9780   930
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int 23440059 16933662  4795650   629778    32256        0        0        0
 minbl4,minbl3,maxbl2 11745 11745 11748
 maxbuf 30006
 number of external orbitals per symmetry block:  62  29  50  22
 nmsym   4 number of internal orbitals  30
 executing brd_struct for drt
 executing brd_struct for orbinf
 executing brd_struct for momap
 calcthrxt: niot,maxw1=                    30                 15318
 block size     0
 pthz,pthy,pthx,pthw:  8556 27450 15045 16149 total internal walks:   67200
 maxlp3,n2lp,n1lp,n0lp 15318     0     0     0
 orbsym(*)= 1 1 1 1 1 1 1 1 1 1 1 1 2 2 2 3 3 3 3 3 3 3 3 3 4 2 2 2 4 4
setref: retained number of references =    22
 setref: total/valid number of walks=                  8556
                  2955
 nmb.of records onel     1
 nmb.of records 2-ext   781
 nmb.of records 1-ext   103
 nmb.of records 0-ext     6
 nmb.of records 2-int     0
 nmb.of records 1-int     0
 nmb.of records 0-int     0
 ---------memory usage in DP -----------------
 < n-ex core usage >
     routines:
    fourex            73916
    threx            151921
    twoex             68440
    onex              14081
    allin              6144
    diagon            32099
               =======
   maximum           151921
 
  __ static summary __ 
   reflst              2955
   hrfspc              2955
               -------
   static->            2955
 
  __ core required  __ 
   totstc              2955
   max n-ex          151921
               -------
   totnec->          154876
 
  __ core available __ 
   totspc         487460220
   totnec -          154876
               -------
   totvec->       487305344

 number of external paths / symmetry
 vertex x    3753    2898    3738    2814
 vertex w    3916    2898    3738    2814
segment: free space=   487305344
 reducing frespc by                 86791 to              487218553 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1         865|       424|         0|       424|         0|         1|
 -------------------------------------------------------------------------------
  Z 2        1759|       424|       424|       424|       424|      3328|
 -------------------------------------------------------------------------------
  Z 3         835|       424|       848|       424|       848|      6645|
 -------------------------------------------------------------------------------
  Z 4         841|       424|      1272|       424|      1272|      9954|
 -------------------------------------------------------------------------------
  Z 5         845|       424|      1696|       424|      1696|     13263|
 -------------------------------------------------------------------------------
  Z 6         855|       424|      2120|       424|      2120|     16574|
 -------------------------------------------------------------------------------
  Z 7         837|       411|      2544|       411|      2544|     19889|
 -------------------------------------------------------------------------------
  Y 8       13727|    518171|      2955|     13727|      2955|     23170|
 -------------------------------------------------------------------------------
  Y 9       13723|    447526|    521126|     13723|     16682|     52745|
 -------------------------------------------------------------------------------
  X10        2616|   5594922|    968652|      1691|     30405|     82312|
 -------------------------------------------------------------------------------
  X11        3025|   5523168|   6563574|      1691|     32096|     88489|
 -------------------------------------------------------------------------------
  X12        3092|   5741250|  12086742|      1691|     33787|     94960|
 -------------------------------------------------------------------------------
  X13        3110|   5864730|  17827992|      1691|     35478|    101483|
 -------------------------------------------------------------------------------
  X14        3199|   5922534|  23692722|      1681|     37169|    108018|
 -------------------------------------------------------------------------------
  W15        2835|   6153058|  29615256|      1801|     38850|    114605|
 -------------------------------------------------------------------------------
  W16        3242|   6038498|  35768314|      1801|     40651|    121080|
 -------------------------------------------------------------------------------
  W17        3312|   6226514|  41806812|      1801|     42452|    127851|
 -------------------------------------------------------------------------------
  W18        3377|   6375354|  48033326|      1801|     44253|    134674|
 -------------------------------------------------------------------------------
  W19        3379|   6425490|  54408680|      1795|     46054|    141543|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=        2308DP  conft+indsym=       54908DP  drtbuffer=       29575 DP
GA space requirements for DRT:       74203DP out of       169298 allocated.

dimension of the ci-matrix ->>>  60834170

segment: free space=   487305344
 reducing frespc by                 89327 to              487216017 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type four-external+diagonal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1        8553|      2955|         0|      2955|         0|         1|
 -------------------------------------------------------------------------------
  Y 2       13727|    518171|      2955|     13727|      2955|     10452|
 -------------------------------------------------------------------------------
  Y 3       13723|    447526|    521126|     13723|     16682|     40027|
 -------------------------------------------------------------------------------
  X 4        2128|   4619970|    968652|      1409|     30405|     69594|
 -------------------------------------------------------------------------------
  X 5        2455|   4558560|   5588622|      1409|     31814|     75055|
 -------------------------------------------------------------------------------
  X 6        2600|   4795056|  10147182|      1409|     33223|     80756|
 -------------------------------------------------------------------------------
  X 7        2601|   4854051|  14942238|      1409|     34632|     86561|
 -------------------------------------------------------------------------------
  X 8        2600|   4874571|  19796289|      1409|     36041|     92368|
 -------------------------------------------------------------------------------
  X 9        2654|   4944396|  24670860|      1400|     37450|     98173|
 -------------------------------------------------------------------------------
  W10        2308|   5087154|  29615256|      1501|     38850|    104006|
 -------------------------------------------------------------------------------
  W11        2693|   5067602|  34702410|      1501|     40351|    109715|
 -------------------------------------------------------------------------------
  W12        2730|   5130654|  39770012|      1501|     41852|    115706|
 -------------------------------------------------------------------------------
  W13        2785|   5220064|  44900666|      1501|     43353|    121723|
 -------------------------------------------------------------------------------
  W14        2787|   5321864|  50120730|      1501|     44854|    127780|
 -------------------------------------------------------------------------------
  W15        2839|   5391576|  55442594|      1494|     46355|    133837|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=        4844DP  conft+indsym=       54908DP  drtbuffer=       29575 DP
GA space requirements for DRT:       69962DP out of       169298 allocated.

dimension of the ci-matrix ->>>  60834170

segment: free space=   487305344
 reducing frespc by                 88319 to              487217025 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type one-external
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1        4845|      1479|         0|      1479|         0|         1|
 -------------------------------------------------------------------------------
  Z 2        2968|      1476|      1479|      1476|      1479|      6300|
 -------------------------------------------------------------------------------
  Y 3       13727|    518171|      2955|     13727|      2955|     12573|
 -------------------------------------------------------------------------------
  Y 4       13723|    447526|    521126|     13723|     16682|     42148|
 -------------------------------------------------------------------------------
  X 5        3323|   6991371|    968652|      2113|     30405|     71715|
 -------------------------------------------------------------------------------
  X 6        3862|   6978462|   7960023|      2113|     32518|     78942|
 -------------------------------------------------------------------------------
  X 7        3863|   7265259|  14938485|      2113|     34631|     86561|
 -------------------------------------------------------------------------------
  X 8        3994|   7411512|  22203744|      2106|     36744|     94182|
 -------------------------------------------------------------------------------
  W 9        5003|  10151942|  29615256|      3001|     38850|    101887|
 -------------------------------------------------------------------------------
  W10        5513|  10345700|  39767198|      3001|     41851|    111466|
 -------------------------------------------------------------------------------
  W11        5633|  10721272|  50112898|      2997|     44852|    121417|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=        3836DP  conft+indsym=       54908DP  drtbuffer=       29575 DP
GA space requirements for DRT:       65724DP out of       169298 allocated.

dimension of the ci-matrix ->>>  60834170

segment: free space=   487305344
 reducing frespc by                 89783 to              487215561 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type two-external
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1        4845|      1479|         0|      1479|         0|         1|
 -------------------------------------------------------------------------------
  Z 2        2968|      1476|      1479|      1476|      1479|      6300|
 -------------------------------------------------------------------------------
  Y 3       13727|    518171|      2955|     13727|      2955|     12573|
 -------------------------------------------------------------------------------
  Y 4       13723|    447526|    521126|     13723|     16682|     42148|
 -------------------------------------------------------------------------------
  X 5        7183|  13962327|    968652|      4224|     30405|     71715|
 -------------------------------------------------------------------------------
  X 6        7862|  14684277|  14930979|      4221|     34629|     84436|
 -------------------------------------------------------------------------------
  W 7        2308|   5087154|  29615256|      1501|     38850|     97647|
 -------------------------------------------------------------------------------
  W 8        2693|   5067602|  34702410|      1501|     40351|    103356|
 -------------------------------------------------------------------------------
  W 9        2730|   5130654|  39770012|      1501|     41852|    109347|
 -------------------------------------------------------------------------------
  W10        2785|   5220064|  44900666|      1501|     43353|    115364|
 -------------------------------------------------------------------------------
  W11        2787|   5321864|  50120730|      1501|     44854|    121421|
 -------------------------------------------------------------------------------
  W12        2839|   5391576|  55442594|      1494|     46355|    127478|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=        5300DP  conft+indsym=       54908DP  drtbuffer=       29575 DP
GA space requirements for DRT:       66783DP out of       169298 allocated.

dimension of the ci-matrix ->>>  60834170

segment: free space=   487305344
 reducing frespc by                 61877 to              487243467 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type three-external
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1        8553|      2955|         0|      2955|         0|         1|
 -------------------------------------------------------------------------------
  Y 2        9152|    358441|      2955|      9152|      2955|     10452|
 -------------------------------------------------------------------------------
  Y 3        9152|    313541|    361396|      9152|     12107|     30877|
 -------------------------------------------------------------------------------
  Y 4        9146|    293715|    674937|      9146|     21259|     51302|
 -------------------------------------------------------------------------------
  X 5        4581|   9174792|    968652|      2817|     30405|     71715|
 -------------------------------------------------------------------------------
  X 6        5203|   9645339|  10143444|      2817|     33222|     80754|
 -------------------------------------------------------------------------------
  X 7        5257|   9826473|  19788783|      2811|     36039|     90243|
 -------------------------------------------------------------------------------
  W 8        5003|  10151942|  29615256|      3001|     38850|     99766|
 -------------------------------------------------------------------------------
  W 9        5513|  10345700|  39767198|      3001|     41851|    109345|
 -------------------------------------------------------------------------------
  W10        5633|  10721272|  50112898|      2997|     44852|    119296|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=        4844DP  conft+indsym=       36608DP  drtbuffer=       20425 DP
GA space requirements for DRT:       64664DP out of       169298 allocated.

dimension of the ci-matrix ->>>  60834170

segment: free space=   487305344
 reducing frespc by                171665 to              487133679 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type two-external wx
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1        8553|      2955|         0|      2955|         0|         1|
 -------------------------------------------------------------------------------
  Y 2       27450|    965697|      2955|     27450|      2955|     10452|
 -------------------------------------------------------------------------------
  X 3        3323|   6991371|    968652|      2113|     30405|     67473|
 -------------------------------------------------------------------------------
  X 4        3862|   6978462|   7960023|      2113|     32518|     74700|
 -------------------------------------------------------------------------------
  X 5        3863|   7265259|  14938485|      2113|     34631|     82319|
 -------------------------------------------------------------------------------
  X 6        3994|   7411512|  22203744|      2106|     36744|     89940|
 -------------------------------------------------------------------------------
  W 7        3660|   7708778|  29615256|      2251|     38850|     97645|
 -------------------------------------------------------------------------------
  W 8        4070|   7573734|  37324034|      2251|     41101|    105294|
 -------------------------------------------------------------------------------
  W 9        4143|   7849638|  44897768|      2251|     43352|    113239|
 -------------------------------------------------------------------------------
  W10        4273|   8086764|  52747406|      2246|     45603|    121238|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=        4844DP  conft+indsym=      109800DP  drtbuffer=       57021 DP
GA space requirements for DRT:       64663DP out of       169298 allocated.

dimension of the ci-matrix ->>>  60834170

 executing brd_struct for civct
 gentasklist: ntask=                   259
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS   
----------------------------------------------------------------------------------------------------
     1  3   3    21      two-ext yy   2X  2 2   13727   13727     518171     518171   13727   13727
     2  4   3    21      two-ext yy   2X  2 2   13723   13727     447526     518171   13723   13727
     3  4   4    21      two-ext yy   2X  2 2   13723   13723     447526     447526   13723   13723
     4  5   5    22      two-ext xx*  2X  3 3    7183    7183   13962327   13962327    4224    4224
     5  5   5    28      two-ext xx+  2X  3 3    7183    7183   13962327   13962327    4224    4224
     6  6   5    22      two-ext xx*  2X  3 3    7862    7183   14684277   13962327    4221    4224
     7  6   5    28      two-ext xx+  2X  3 3    7862    7183   14684277   13962327    4221    4224
     8  6   6    22      two-ext xx*  2X  3 3    7862    7862   14684277   14684277    4221    4221
     9  6   6    28      two-ext xx+  2X  3 3    7862    7862   14684277   14684277    4221    4221
    10  7   7    23      two-ext ww*  2X  4 4    2308    2308    5087154    5087154    1501    1501
    11  7   7    29      two-ext ww+  2X  4 4    2308    2308    5087154    5087154    1501    1501
    12  8   7    23      two-ext ww*  2X  4 4    2693    2308    5067602    5087154    1501    1501
    13  8   7    29      two-ext ww+  2X  4 4    2693    2308    5067602    5087154    1501    1501
    14  9   7    23      two-ext ww*  2X  4 4    2730    2308    5130654    5087154    1501    1501
    15  9   7    29      two-ext ww+  2X  4 4    2730    2308    5130654    5087154    1501    1501
    16 10   7    23      two-ext ww*  2X  4 4    2785    2308    5220064    5087154    1501    1501
    17 10   7    29      two-ext ww+  2X  4 4    2785    2308    5220064    5087154    1501    1501
    18 11   7    23      two-ext ww*  2X  4 4    2787    2308    5321864    5087154    1501    1501
    19 11   7    29      two-ext ww+  2X  4 4    2787    2308    5321864    5087154    1501    1501
    20 12   7    23      two-ext ww*  2X  4 4    2839    2308    5391576    5087154    1494    1501
    21 12   7    29      two-ext ww+  2X  4 4    2839    2308    5391576    5087154    1494    1501
    22  8   8    23      two-ext ww*  2X  4 4    2693    2693    5067602    5067602    1501    1501
    23  8   8    29      two-ext ww+  2X  4 4    2693    2693    5067602    5067602    1501    1501
    24  9   8    23      two-ext ww*  2X  4 4    2730    2693    5130654    5067602    1501    1501
    25  9   8    29      two-ext ww+  2X  4 4    2730    2693    5130654    5067602    1501    1501
    26 10   8    23      two-ext ww*  2X  4 4    2785    2693    5220064    5067602    1501    1501
    27 10   8    29      two-ext ww+  2X  4 4    2785    2693    5220064    5067602    1501    1501
    28 11   8    23      two-ext ww*  2X  4 4    2787    2693    5321864    5067602    1501    1501
    29 11   8    29      two-ext ww+  2X  4 4    2787    2693    5321864    5067602    1501    1501
    30 12   8    23      two-ext ww*  2X  4 4    2839    2693    5391576    5067602    1494    1501
    31 12   8    29      two-ext ww+  2X  4 4    2839    2693    5391576    5067602    1494    1501
    32  9   9    23      two-ext ww*  2X  4 4    2730    2730    5130654    5130654    1501    1501
    33  9   9    29      two-ext ww+  2X  4 4    2730    2730    5130654    5130654    1501    1501
    34 10   9    23      two-ext ww*  2X  4 4    2785    2730    5220064    5130654    1501    1501
    35 10   9    29      two-ext ww+  2X  4 4    2785    2730    5220064    5130654    1501    1501
    36 11   9    23      two-ext ww*  2X  4 4    2787    2730    5321864    5130654    1501    1501
    37 11   9    29      two-ext ww+  2X  4 4    2787    2730    5321864    5130654    1501    1501
    38 12   9    23      two-ext ww*  2X  4 4    2839    2730    5391576    5130654    1494    1501
    39 12   9    29      two-ext ww+  2X  4 4    2839    2730    5391576    5130654    1494    1501
    40 10  10    23      two-ext ww*  2X  4 4    2785    2785    5220064    5220064    1501    1501
    41 10  10    29      two-ext ww+  2X  4 4    2785    2785    5220064    5220064    1501    1501
    42 11  10    23      two-ext ww*  2X  4 4    2787    2785    5321864    5220064    1501    1501
    43 11  10    29      two-ext ww+  2X  4 4    2787    2785    5321864    5220064    1501    1501
    44 12  10    23      two-ext ww*  2X  4 4    2839    2785    5391576    5220064    1494    1501
    45 12  10    29      two-ext ww+  2X  4 4    2839    2785    5391576    5220064    1494    1501
    46 11  11    23      two-ext ww*  2X  4 4    2787    2787    5321864    5321864    1501    1501
    47 11  11    29      two-ext ww+  2X  4 4    2787    2787    5321864    5321864    1501    1501
    48 12  11    23      two-ext ww*  2X  4 4    2839    2787    5391576    5321864    1494    1501
    49 12  11    29      two-ext ww+  2X  4 4    2839    2787    5391576    5321864    1494    1501
    50 12  12    23      two-ext ww*  2X  4 4    2839    2839    5391576    5391576    1494    1494
    51 12  12    29      two-ext ww+  2X  4 4    2839    2839    5391576    5391576    1494    1494
    52  5   1    24      two-ext xz   2X  3 1    7183    4845   13962327       1479    4224    1479
    53  5   2    24      two-ext xz   2X  3 1    7183    2968   13962327       1476    4224    1476
    54  6   1    24      two-ext xz   2X  3 1    7862    4845   14684277       1479    4221    1479
    55  6   2    24      two-ext xz   2X  3 1    7862    2968   14684277       1476    4221    1476
    56  7   1    25      two-ext wz   2X  4 1    2308    4845    5087154       1479    1501    1479
    57  7   2    25      two-ext wz   2X  4 1    2308    2968    5087154       1476    1501    1476
    58  8   1    25      two-ext wz   2X  4 1    2693    4845    5067602       1479    1501    1479
    59  8   2    25      two-ext wz   2X  4 1    2693    2968    5067602       1476    1501    1476
    60  9   1    25      two-ext wz   2X  4 1    2730    4845    5130654       1479    1501    1479
    61  9   2    25      two-ext wz   2X  4 1    2730    2968    5130654       1476    1501    1476
    62 10   1    25      two-ext wz   2X  4 1    2785    4845    5220064       1479    1501    1479
    63 10   2    25      two-ext wz   2X  4 1    2785    2968    5220064       1476    1501    1476
    64 11   1    25      two-ext wz   2X  4 1    2787    4845    5321864       1479    1501    1479
    65 11   2    25      two-ext wz   2X  4 1    2787    2968    5321864       1476    1501    1476
    66 12   1    25      two-ext wz   2X  4 1    2839    4845    5391576       1479    1494    1479
    67 12   2    25      two-ext wz   2X  4 1    2839    2968    5391576       1476    1494    1476
    68  7   3    26      two-ext wx*  WX  4 3    3660    3323    7708778    6991371    2251    2113
    69  7   3    27      two-ext wx+  WX  4 3    3660    3323    7708778    6991371    2251    2113
    70  8   3    26      two-ext wx*  WX  4 3    4070    3323    7573734    6991371    2251    2113
    71  8   3    27      two-ext wx+  WX  4 3    4070    3323    7573734    6991371    2251    2113
    72  9   3    26      two-ext wx*  WX  4 3    4143    3323    7849638    6991371    2251    2113
    73  9   3    27      two-ext wx+  WX  4 3    4143    3323    7849638    6991371    2251    2113
    74 10   3    26      two-ext wx*  WX  4 3    4273    3323    8086764    6991371    2246    2113
    75 10   3    27      two-ext wx+  WX  4 3    4273    3323    8086764    6991371    2246    2113
    76  7   4    26      two-ext wx*  WX  4 3    3660    3862    7708778    6978462    2251    2113
    77  7   4    27      two-ext wx+  WX  4 3    3660    3862    7708778    6978462    2251    2113
    78  8   4    26      two-ext wx*  WX  4 3    4070    3862    7573734    6978462    2251    2113
    79  8   4    27      two-ext wx+  WX  4 3    4070    3862    7573734    6978462    2251    2113
    80  9   4    26      two-ext wx*  WX  4 3    4143    3862    7849638    6978462    2251    2113
    81  9   4    27      two-ext wx+  WX  4 3    4143    3862    7849638    6978462    2251    2113
    82 10   4    26      two-ext wx*  WX  4 3    4273    3862    8086764    6978462    2246    2113
    83 10   4    27      two-ext wx+  WX  4 3    4273    3862    8086764    6978462    2246    2113
    84  7   5    26      two-ext wx*  WX  4 3    3660    3863    7708778    7265259    2251    2113
    85  7   5    27      two-ext wx+  WX  4 3    3660    3863    7708778    7265259    2251    2113
    86  8   5    26      two-ext wx*  WX  4 3    4070    3863    7573734    7265259    2251    2113
    87  8   5    27      two-ext wx+  WX  4 3    4070    3863    7573734    7265259    2251    2113
    88  9   5    26      two-ext wx*  WX  4 3    4143    3863    7849638    7265259    2251    2113
    89  9   5    27      two-ext wx+  WX  4 3    4143    3863    7849638    7265259    2251    2113
    90 10   5    26      two-ext wx*  WX  4 3    4273    3863    8086764    7265259    2246    2113
    91 10   5    27      two-ext wx+  WX  4 3    4273    3863    8086764    7265259    2246    2113
    92  7   6    26      two-ext wx*  WX  4 3    3660    3994    7708778    7411512    2251    2106
    93  7   6    27      two-ext wx+  WX  4 3    3660    3994    7708778    7411512    2251    2106
    94  8   6    26      two-ext wx*  WX  4 3    4070    3994    7573734    7411512    2251    2106
    95  8   6    27      two-ext wx+  WX  4 3    4070    3994    7573734    7411512    2251    2106
    96  9   6    26      two-ext wx*  WX  4 3    4143    3994    7849638    7411512    2251    2106
    97  9   6    27      two-ext wx+  WX  4 3    4143    3994    7849638    7411512    2251    2106
    98 10   6    26      two-ext wx*  WX  4 3    4273    3994    8086764    7411512    2246    2106
    99 10   6    27      two-ext wx+  WX  4 3    4273    3994    8086764    7411512    2246    2106
   100  3   1    11      one-ext yz   1X  2 1   13727    4845     518171       1479   13727    1479
   101  3   2    11      one-ext yz   1X  2 1   13727    2968     518171       1476   13727    1476
   102  4   1    11      one-ext yz   1X  2 1   13723    4845     447526       1479   13723    1479
   103  4   2    11      one-ext yz   1X  2 1   13723    2968     447526       1476   13723    1476
   104  5   3    13      one-ext yx   1X  3 2    3323   13727    6991371     518171    2113   13727
   105  6   3    13      one-ext yx   1X  3 2    3862   13727    6978462     518171    2113   13727
   106  7   3    13      one-ext yx   1X  3 2    3863   13727    7265259     518171    2113   13727
   107  8   3    13      one-ext yx   1X  3 2    3994   13727    7411512     518171    2106   13727
   108  5   4    13      one-ext yx   1X  3 2    3323   13723    6991371     447526    2113   13723
   109  6   4    13      one-ext yx   1X  3 2    3862   13723    6978462     447526    2113   13723
   110  7   4    13      one-ext yx   1X  3 2    3863   13723    7265259     447526    2113   13723
   111  8   4    13      one-ext yx   1X  3 2    3994   13723    7411512     447526    2106   13723
   112  9   3    14      one-ext yw   1X  4 2    5003   13727   10151942     518171    3001   13727
   113 10   3    14      one-ext yw   1X  4 2    5513   13727   10345700     518171    3001   13727
   114 11   3    14      one-ext yw   1X  4 2    5633   13727   10721272     518171    2997   13727
   115  9   4    14      one-ext yw   1X  4 2    5003   13723   10151942     447526    3001   13723
   116 10   4    14      one-ext yw   1X  4 2    5513   13723   10345700     447526    3001   13723
   117 11   4    14      one-ext yw   1X  4 2    5633   13723   10721272     447526    2997   13723
   118  5   2    31      thr-ext yx   3X  3 2    4581    9152    9174792     358441    2817    9152
   119  5   3    31      thr-ext yx   3X  3 2    4581    9152    9174792     313541    2817    9152
   120  5   4    31      thr-ext yx   3X  3 2    4581    9146    9174792     293715    2817    9146
   121  6   2    31      thr-ext yx   3X  3 2    5203    9152    9645339     358441    2817    9152
   122  6   3    31      thr-ext yx   3X  3 2    5203    9152    9645339     313541    2817    9152
   123  6   4    31      thr-ext yx   3X  3 2    5203    9146    9645339     293715    2817    9146
   124  7   2    31      thr-ext yx   3X  3 2    5257    9152    9826473     358441    2811    9152
   125  7   3    31      thr-ext yx   3X  3 2    5257    9152    9826473     313541    2811    9152
   126  7   4    31      thr-ext yx   3X  3 2    5257    9146    9826473     293715    2811    9146
   127  8   2    32      thr-ext yw   3X  4 2    5003    9152   10151942     358441    3001    9152
   128  8   3    32      thr-ext yw   3X  4 2    5003    9152   10151942     313541    3001    9152
   129  8   4    32      thr-ext yw   3X  4 2    5003    9146   10151942     293715    3001    9146
   130  9   2    32      thr-ext yw   3X  4 2    5513    9152   10345700     358441    3001    9152
   131  9   3    32      thr-ext yw   3X  4 2    5513    9152   10345700     313541    3001    9152
   132  9   4    32      thr-ext yw   3X  4 2    5513    9146   10345700     293715    3001    9146
   133 10   2    32      thr-ext yw   3X  4 2    5633    9152   10721272     358441    2997    9152
   134 10   3    32      thr-ext yw   3X  4 2    5633    9152   10721272     313541    2997    9152
   135 10   4    32      thr-ext yw   3X  4 2    5633    9146   10721272     293715    2997    9146
   136  1   1     1      allint zz    OX  1 1     865     865        424        424     424     424
   137  2   1     1      allint zz    OX  1 1    1759     865        424        424     424     424
   138  3   1     1      allint zz    OX  1 1     835     865        424        424     424     424
   139  4   1     1      allint zz    OX  1 1     841     865        424        424     424     424
   140  5   1     1      allint zz    OX  1 1     845     865        424        424     424     424
   141  6   1     1      allint zz    OX  1 1     855     865        424        424     424     424
   142  7   1     1      allint zz    OX  1 1     837     865        411        424     411     424
   143  2   2     1      allint zz    OX  1 1    1759    1759        424        424     424     424
   144  3   2     1      allint zz    OX  1 1     835    1759        424        424     424     424
   145  4   2     1      allint zz    OX  1 1     841    1759        424        424     424     424
   146  5   2     1      allint zz    OX  1 1     845    1759        424        424     424     424
   147  6   2     1      allint zz    OX  1 1     855    1759        424        424     424     424
   148  7   2     1      allint zz    OX  1 1     837    1759        411        424     411     424
   149  3   3     1      allint zz    OX  1 1     835     835        424        424     424     424
   150  4   3     1      allint zz    OX  1 1     841     835        424        424     424     424
   151  5   3     1      allint zz    OX  1 1     845     835        424        424     424     424
   152  6   3     1      allint zz    OX  1 1     855     835        424        424     424     424
   153  7   3     1      allint zz    OX  1 1     837     835        411        424     411     424
   154  4   4     1      allint zz    OX  1 1     841     841        424        424     424     424
   155  5   4     1      allint zz    OX  1 1     845     841        424        424     424     424
   156  6   4     1      allint zz    OX  1 1     855     841        424        424     424     424
   157  7   4     1      allint zz    OX  1 1     837     841        411        424     411     424
   158  5   5     1      allint zz    OX  1 1     845     845        424        424     424     424
   159  6   5     1      allint zz    OX  1 1     855     845        424        424     424     424
   160  7   5     1      allint zz    OX  1 1     837     845        411        424     411     424
   161  6   6     1      allint zz    OX  1 1     855     855        424        424     424     424
   162  7   6     1      allint zz    OX  1 1     837     855        411        424     411     424
   163  7   7     1      allint zz    OX  1 1     837     837        411        411     411     411
   164  8   8     2      allint yy    OX  2 2   13727   13727     518171     518171   13727   13727
   165  9   8     2      allint yy    OX  2 2   13723   13727     447526     518171   13723   13727
   166  9   9     2      allint yy    OX  2 2   13723   13723     447526     447526   13723   13723
   167 10  10     3      allint xx*   OX  3 3    2616    2616    5594922    5594922    1691    1691
   168 10  10     8      allint xx+   OX  3 3    2616    2616    5594922    5594922    1691    1691
   169 11  10     3      allint xx*   OX  3 3    3025    2616    5523168    5594922    1691    1691
   170 11  10     8      allint xx+   OX  3 3    3025    2616    5523168    5594922    1691    1691
   171 12  10     3      allint xx*   OX  3 3    3092    2616    5741250    5594922    1691    1691
   172 12  10     8      allint xx+   OX  3 3    3092    2616    5741250    5594922    1691    1691
   173 13  10     3      allint xx*   OX  3 3    3110    2616    5864730    5594922    1691    1691
   174 13  10     8      allint xx+   OX  3 3    3110    2616    5864730    5594922    1691    1691
   175 14  10     3      allint xx*   OX  3 3    3199    2616    5922534    5594922    1681    1691
   176 14  10     8      allint xx+   OX  3 3    3199    2616    5922534    5594922    1681    1691
   177 11  11     3      allint xx*   OX  3 3    3025    3025    5523168    5523168    1691    1691
   178 11  11     8      allint xx+   OX  3 3    3025    3025    5523168    5523168    1691    1691
   179 12  11     3      allint xx*   OX  3 3    3092    3025    5741250    5523168    1691    1691
   180 12  11     8      allint xx+   OX  3 3    3092    3025    5741250    5523168    1691    1691
   181 13  11     3      allint xx*   OX  3 3    3110    3025    5864730    5523168    1691    1691
   182 13  11     8      allint xx+   OX  3 3    3110    3025    5864730    5523168    1691    1691
   183 14  11     3      allint xx*   OX  3 3    3199    3025    5922534    5523168    1681    1691
   184 14  11     8      allint xx+   OX  3 3    3199    3025    5922534    5523168    1681    1691
   185 12  12     3      allint xx*   OX  3 3    3092    3092    5741250    5741250    1691    1691
   186 12  12     8      allint xx+   OX  3 3    3092    3092    5741250    5741250    1691    1691
   187 13  12     3      allint xx*   OX  3 3    3110    3092    5864730    5741250    1691    1691
   188 13  12     8      allint xx+   OX  3 3    3110    3092    5864730    5741250    1691    1691
   189 14  12     3      allint xx*   OX  3 3    3199    3092    5922534    5741250    1681    1691
   190 14  12     8      allint xx+   OX  3 3    3199    3092    5922534    5741250    1681    1691
   191 13  13     3      allint xx*   OX  3 3    3110    3110    5864730    5864730    1691    1691
   192 13  13     8      allint xx+   OX  3 3    3110    3110    5864730    5864730    1691    1691
   193 14  13     3      allint xx*   OX  3 3    3199    3110    5922534    5864730    1681    1691
   194 14  13     8      allint xx+   OX  3 3    3199    3110    5922534    5864730    1681    1691
   195 14  14     3      allint xx*   OX  3 3    3199    3199    5922534    5922534    1681    1681
   196 14  14     8      allint xx+   OX  3 3    3199    3199    5922534    5922534    1681    1681
   197 15  15     4      allint ww*   OX  4 4    2835    2835    6153058    6153058    1801    1801
   198 15  15     9      allint ww+   OX  4 4    2835    2835    6153058    6153058    1801    1801
   199 16  15     4      allint ww*   OX  4 4    3242    2835    6038498    6153058    1801    1801
   200 16  15     9      allint ww+   OX  4 4    3242    2835    6038498    6153058    1801    1801
   201 17  15     4      allint ww*   OX  4 4    3312    2835    6226514    6153058    1801    1801
   202 17  15     9      allint ww+   OX  4 4    3312    2835    6226514    6153058    1801    1801
   203 18  15     4      allint ww*   OX  4 4    3377    2835    6375354    6153058    1801    1801
   204 18  15     9      allint ww+   OX  4 4    3377    2835    6375354    6153058    1801    1801
   205 19  15     4      allint ww*   OX  4 4    3379    2835    6425490    6153058    1795    1801
   206 19  15     9      allint ww+   OX  4 4    3379    2835    6425490    6153058    1795    1801
   207 16  16     4      allint ww*   OX  4 4    3242    3242    6038498    6038498    1801    1801
   208 16  16     9      allint ww+   OX  4 4    3242    3242    6038498    6038498    1801    1801
   209 17  16     4      allint ww*   OX  4 4    3312    3242    6226514    6038498    1801    1801
   210 17  16     9      allint ww+   OX  4 4    3312    3242    6226514    6038498    1801    1801
   211 18  16     4      allint ww*   OX  4 4    3377    3242    6375354    6038498    1801    1801
   212 18  16     9      allint ww+   OX  4 4    3377    3242    6375354    6038498    1801    1801
   213 19  16     4      allint ww*   OX  4 4    3379    3242    6425490    6038498    1795    1801
   214 19  16     9      allint ww+   OX  4 4    3379    3242    6425490    6038498    1795    1801
   215 17  17     4      allint ww*   OX  4 4    3312    3312    6226514    6226514    1801    1801
   216 17  17     9      allint ww+   OX  4 4    3312    3312    6226514    6226514    1801    1801
   217 18  17     4      allint ww*   OX  4 4    3377    3312    6375354    6226514    1801    1801
   218 18  17     9      allint ww+   OX  4 4    3377    3312    6375354    6226514    1801    1801
   219 19  17     4      allint ww*   OX  4 4    3379    3312    6425490    6226514    1795    1801
   220 19  17     9      allint ww+   OX  4 4    3379    3312    6425490    6226514    1795    1801
   221 18  18     4      allint ww*   OX  4 4    3377    3377    6375354    6375354    1801    1801
   222 18  18     9      allint ww+   OX  4 4    3377    3377    6375354    6375354    1801    1801
   223 19  18     4      allint ww*   OX  4 4    3379    3377    6425490    6375354    1795    1801
   224 19  18     9      allint ww+   OX  4 4    3379    3377    6425490    6375354    1795    1801
   225 19  19     4      allint ww*   OX  4 4    3379    3379    6425490    6425490    1795    1795
   226 19  19     9      allint ww+   OX  4 4    3379    3379    6425490    6425490    1795    1795
   227  2   2    42      four-ext y   4X  2 2   13727   13727     518171     518171   13727   13727
   228  3   3    42      four-ext y   4X  2 2   13723   13723     447526     447526   13723   13723
   229  4   4    43      four-ext x   4X  3 3    2128    2128    4619970    4619970    1409    1409
   230  5   5    43      four-ext x   4X  3 3    2455    2455    4558560    4558560    1409    1409
   231  6   6    43      four-ext x   4X  3 3    2600    2600    4795056    4795056    1409    1409
   232  7   7    43      four-ext x   4X  3 3    2601    2601    4854051    4854051    1409    1409
   233  8   8    43      four-ext x   4X  3 3    2600    2600    4874571    4874571    1409    1409
   234  9   9    43      four-ext x   4X  3 3    2654    2654    4944396    4944396    1400    1400
   235 10  10    44      four-ext w   4X  4 4    2308    2308    5087154    5087154    1501    1501
   236 11  11    44      four-ext w   4X  4 4    2693    2693    5067602    5067602    1501    1501
   237 12  12    44      four-ext w   4X  4 4    2730    2730    5130654    5130654    1501    1501
   238 13  13    44      four-ext w   4X  4 4    2785    2785    5220064    5220064    1501    1501
   239 14  14    44      four-ext w   4X  4 4    2787    2787    5321864    5321864    1501    1501
   240 15  15    44      four-ext w   4X  4 4    2839    2839    5391576    5391576    1494    1494
   241  1   1    75      dg-024ext z  OX  1 1     865     865        424        424     424     424
   242  2   2    75      dg-024ext z  OX  1 1    1759    1759        424        424     424     424
   243  3   3    75      dg-024ext z  OX  1 1     835     835        424        424     424     424
   244  4   4    75      dg-024ext z  OX  1 1     841     841        424        424     424     424
   245  5   5    75      dg-024ext z  OX  1 1     845     845        424        424     424     424
   246  6   6    75      dg-024ext z  OX  1 1     855     855        424        424     424     424
   247  7   7    75      dg-024ext z  OX  1 1     837     837        411        411     411     411
   248  8   8    76      dg-024ext y  OX  2 2   13727   13727     518171     518171   13727   13727
   249  9   9    76      dg-024ext y  OX  2 2   13723   13723     447526     447526   13723   13723
   250 10  10    77      dg-024ext x  OX  3 3    2616    2616    5594922    5594922    1691    1691
   251 11  11    77      dg-024ext x  OX  3 3    3025    3025    5523168    5523168    1691    1691
   252 12  12    77      dg-024ext x  OX  3 3    3092    3092    5741250    5741250    1691    1691
   253 13  13    77      dg-024ext x  OX  3 3    3110    3110    5864730    5864730    1691    1691
   254 14  14    77      dg-024ext x  OX  3 3    3199    3199    5922534    5922534    1681    1681
   255 15  15    78      dg-024ext w  OX  4 4    2835    2835    6153058    6153058    1801    1801
   256 16  16    78      dg-024ext w  OX  4 4    3242    3242    6038498    6038498    1801    1801
   257 17  17    78      dg-024ext w  OX  4 4    3312    3312    6226514    6226514    1801    1801
   258 18  18    78      dg-024ext w  OX  4 4    3377    3377    6375354    6375354    1801    1801
   259 19  19    78      dg-024ext w  OX  4 4    3379    3379    6425490    6425490    1795    1795
----------------------------------------------------------------------------------------------------
REDTASK #   1 TIME= 258.000 N=  1 (task/type/sgbra)=(   1/21/0) (
REDTASK #   2 TIME= 257.000 N=  1 (task/type/sgbra)=(   2/21/0) (
REDTASK #   3 TIME= 256.000 N=  1 (task/type/sgbra)=(   3/21/0) (
REDTASK #   4 TIME= 255.000 N=  1 (task/type/sgbra)=(   4/22/1) (
REDTASK #   5 TIME= 254.000 N=  1 (task/type/sgbra)=(   5/28/2) (
REDTASK #   6 TIME= 253.000 N=  1 (task/type/sgbra)=(   6/22/1) (
REDTASK #   7 TIME= 252.000 N=  1 (task/type/sgbra)=(   7/28/2) (
REDTASK #   8 TIME= 251.000 N=  1 (task/type/sgbra)=(   8/22/1) (
REDTASK #   9 TIME= 250.000 N=  1 (task/type/sgbra)=(   9/28/2) (
REDTASK #  10 TIME= 249.000 N=  1 (task/type/sgbra)=(  10/23/1) (
REDTASK #  11 TIME= 248.000 N=  1 (task/type/sgbra)=(  11/29/2) (
REDTASK #  12 TIME= 247.000 N=  1 (task/type/sgbra)=(  12/23/1) (
REDTASK #  13 TIME= 246.000 N=  1 (task/type/sgbra)=(  13/29/2) (
REDTASK #  14 TIME= 245.000 N=  1 (task/type/sgbra)=(  14/23/1) (
REDTASK #  15 TIME= 244.000 N=  1 (task/type/sgbra)=(  15/29/2) (
REDTASK #  16 TIME= 243.000 N=  1 (task/type/sgbra)=(  16/23/1) (
REDTASK #  17 TIME= 242.000 N=  1 (task/type/sgbra)=(  17/29/2) (
REDTASK #  18 TIME= 241.000 N=  1 (task/type/sgbra)=(  18/23/1) (
REDTASK #  19 TIME= 240.000 N=  1 (task/type/sgbra)=(  19/29/2) (
REDTASK #  20 TIME= 239.000 N=  1 (task/type/sgbra)=(  20/23/1) (
REDTASK #  21 TIME= 238.000 N=  1 (task/type/sgbra)=(  21/29/2) (
REDTASK #  22 TIME= 237.000 N=  1 (task/type/sgbra)=(  22/23/1) (
REDTASK #  23 TIME= 236.000 N=  1 (task/type/sgbra)=(  23/29/2) (
REDTASK #  24 TIME= 235.000 N=  1 (task/type/sgbra)=(  24/23/1) (
REDTASK #  25 TIME= 234.000 N=  1 (task/type/sgbra)=(  25/29/2) (
REDTASK #  26 TIME= 233.000 N=  1 (task/type/sgbra)=(  26/23/1) (
REDTASK #  27 TIME= 232.000 N=  1 (task/type/sgbra)=(  27/29/2) (
REDTASK #  28 TIME= 231.000 N=  1 (task/type/sgbra)=(  28/23/1) (
REDTASK #  29 TIME= 230.000 N=  1 (task/type/sgbra)=(  29/29/2) (
REDTASK #  30 TIME= 229.000 N=  1 (task/type/sgbra)=(  30/23/1) (
REDTASK #  31 TIME= 228.000 N=  1 (task/type/sgbra)=(  31/29/2) (
REDTASK #  32 TIME= 227.000 N=  1 (task/type/sgbra)=(  32/23/1) (
REDTASK #  33 TIME= 226.000 N=  1 (task/type/sgbra)=(  33/29/2) (
REDTASK #  34 TIME= 225.000 N=  1 (task/type/sgbra)=(  34/23/1) (
REDTASK #  35 TIME= 224.000 N=  1 (task/type/sgbra)=(  35/29/2) (
REDTASK #  36 TIME= 223.000 N=  1 (task/type/sgbra)=(  36/23/1) (
REDTASK #  37 TIME= 222.000 N=  1 (task/type/sgbra)=(  37/29/2) (
REDTASK #  38 TIME= 221.000 N=  1 (task/type/sgbra)=(  38/23/1) (
REDTASK #  39 TIME= 220.000 N=  1 (task/type/sgbra)=(  39/29/2) (
REDTASK #  40 TIME= 219.000 N=  1 (task/type/sgbra)=(  40/23/1) (
REDTASK #  41 TIME= 218.000 N=  1 (task/type/sgbra)=(  41/29/2) (
REDTASK #  42 TIME= 217.000 N=  1 (task/type/sgbra)=(  42/23/1) (
REDTASK #  43 TIME= 216.000 N=  1 (task/type/sgbra)=(  43/29/2) (
REDTASK #  44 TIME= 215.000 N=  1 (task/type/sgbra)=(  44/23/1) (
REDTASK #  45 TIME= 214.000 N=  1 (task/type/sgbra)=(  45/29/2) (
REDTASK #  46 TIME= 213.000 N=  1 (task/type/sgbra)=(  46/23/1) (
REDTASK #  47 TIME= 212.000 N=  1 (task/type/sgbra)=(  47/29/2) (
REDTASK #  48 TIME= 211.000 N=  1 (task/type/sgbra)=(  48/23/1) (
REDTASK #  49 TIME= 210.000 N=  1 (task/type/sgbra)=(  49/29/2) (
REDTASK #  50 TIME= 209.000 N=  1 (task/type/sgbra)=(  50/23/1) (
REDTASK #  51 TIME= 208.000 N=  1 (task/type/sgbra)=(  51/29/2) (
REDTASK #  52 TIME= 207.000 N=  1 (task/type/sgbra)=(  52/24/0) (
REDTASK #  53 TIME= 206.000 N=  1 (task/type/sgbra)=(  53/24/0) (
REDTASK #  54 TIME= 205.000 N=  1 (task/type/sgbra)=(  54/24/0) (
REDTASK #  55 TIME= 204.000 N=  1 (task/type/sgbra)=(  55/24/0) (
REDTASK #  56 TIME= 203.000 N=  1 (task/type/sgbra)=(  56/25/0) (
REDTASK #  57 TIME= 202.000 N=  1 (task/type/sgbra)=(  57/25/0) (
REDTASK #  58 TIME= 201.000 N=  1 (task/type/sgbra)=(  58/25/0) (
REDTASK #  59 TIME= 200.000 N=  1 (task/type/sgbra)=(  59/25/0) (
REDTASK #  60 TIME= 199.000 N=  1 (task/type/sgbra)=(  60/25/0) (
REDTASK #  61 TIME= 198.000 N=  1 (task/type/sgbra)=(  61/25/0) (
REDTASK #  62 TIME= 197.000 N=  1 (task/type/sgbra)=(  62/25/0) (
REDTASK #  63 TIME= 196.000 N=  1 (task/type/sgbra)=(  63/25/0) (
REDTASK #  64 TIME= 195.000 N=  1 (task/type/sgbra)=(  64/25/0) (
REDTASK #  65 TIME= 194.000 N=  1 (task/type/sgbra)=(  65/25/0) (
REDTASK #  66 TIME= 193.000 N=  1 (task/type/sgbra)=(  66/25/0) (
REDTASK #  67 TIME= 192.000 N=  1 (task/type/sgbra)=(  67/25/0) (
REDTASK #  68 TIME= 191.000 N=  1 (task/type/sgbra)=(  68/26/1) (
REDTASK #  69 TIME= 190.000 N=  1 (task/type/sgbra)=(  69/27/2) (
REDTASK #  70 TIME= 189.000 N=  1 (task/type/sgbra)=(  70/26/1) (
REDTASK #  71 TIME= 188.000 N=  1 (task/type/sgbra)=(  71/27/2) (
REDTASK #  72 TIME= 187.000 N=  1 (task/type/sgbra)=(  72/26/1) (
REDTASK #  73 TIME= 186.000 N=  1 (task/type/sgbra)=(  73/27/2) (
REDTASK #  74 TIME= 185.000 N=  1 (task/type/sgbra)=(  74/26/1) (
REDTASK #  75 TIME= 184.000 N=  1 (task/type/sgbra)=(  75/27/2) (
REDTASK #  76 TIME= 183.000 N=  1 (task/type/sgbra)=(  76/26/1) (
REDTASK #  77 TIME= 182.000 N=  1 (task/type/sgbra)=(  77/27/2) (
REDTASK #  78 TIME= 181.000 N=  1 (task/type/sgbra)=(  78/26/1) (
REDTASK #  79 TIME= 180.000 N=  1 (task/type/sgbra)=(  79/27/2) (
REDTASK #  80 TIME= 179.000 N=  1 (task/type/sgbra)=(  80/26/1) (
REDTASK #  81 TIME= 178.000 N=  1 (task/type/sgbra)=(  81/27/2) (
REDTASK #  82 TIME= 177.000 N=  1 (task/type/sgbra)=(  82/26/1) (
REDTASK #  83 TIME= 176.000 N=  1 (task/type/sgbra)=(  83/27/2) (
REDTASK #  84 TIME= 175.000 N=  1 (task/type/sgbra)=(  84/26/1) (
REDTASK #  85 TIME= 174.000 N=  1 (task/type/sgbra)=(  85/27/2) (
REDTASK #  86 TIME= 173.000 N=  1 (task/type/sgbra)=(  86/26/1) (
REDTASK #  87 TIME= 172.000 N=  1 (task/type/sgbra)=(  87/27/2) (
REDTASK #  88 TIME= 171.000 N=  1 (task/type/sgbra)=(  88/26/1) (
REDTASK #  89 TIME= 170.000 N=  1 (task/type/sgbra)=(  89/27/2) (
REDTASK #  90 TIME= 169.000 N=  1 (task/type/sgbra)=(  90/26/1) (
REDTASK #  91 TIME= 168.000 N=  1 (task/type/sgbra)=(  91/27/2) (
REDTASK #  92 TIME= 167.000 N=  1 (task/type/sgbra)=(  92/26/1) (
REDTASK #  93 TIME= 166.000 N=  1 (task/type/sgbra)=(  93/27/2) (
REDTASK #  94 TIME= 165.000 N=  1 (task/type/sgbra)=(  94/26/1) (
REDTASK #  95 TIME= 164.000 N=  1 (task/type/sgbra)=(  95/27/2) (
REDTASK #  96 TIME= 163.000 N=  1 (task/type/sgbra)=(  96/26/1) (
REDTASK #  97 TIME= 162.000 N=  1 (task/type/sgbra)=(  97/27/2) (
REDTASK #  98 TIME= 161.000 N=  1 (task/type/sgbra)=(  98/26/1) (
REDTASK #  99 TIME= 160.000 N=  1 (task/type/sgbra)=(  99/27/2) (
REDTASK # 100 TIME= 159.000 N=  1 (task/type/sgbra)=( 100/11/0) (
REDTASK # 101 TIME= 158.000 N=  1 (task/type/sgbra)=( 101/11/0) (
REDTASK # 102 TIME= 157.000 N=  1 (task/type/sgbra)=( 102/11/0) (
REDTASK # 103 TIME= 156.000 N=  1 (task/type/sgbra)=( 103/11/0) (
REDTASK # 104 TIME= 155.000 N=  1 (task/type/sgbra)=( 104/13/0) (
REDTASK # 105 TIME= 154.000 N=  1 (task/type/sgbra)=( 105/13/0) (
REDTASK # 106 TIME= 153.000 N=  1 (task/type/sgbra)=( 106/13/0) (
REDTASK # 107 TIME= 152.000 N=  1 (task/type/sgbra)=( 107/13/0) (
REDTASK # 108 TIME= 151.000 N=  1 (task/type/sgbra)=( 108/13/0) (
REDTASK # 109 TIME= 150.000 N=  1 (task/type/sgbra)=( 109/13/0) (
REDTASK # 110 TIME= 149.000 N=  1 (task/type/sgbra)=( 110/13/0) (
REDTASK # 111 TIME= 148.000 N=  1 (task/type/sgbra)=( 111/13/0) (
REDTASK # 112 TIME= 147.000 N=  1 (task/type/sgbra)=( 112/14/0) (
REDTASK # 113 TIME= 146.000 N=  1 (task/type/sgbra)=( 113/14/0) (
REDTASK # 114 TIME= 145.000 N=  1 (task/type/sgbra)=( 114/14/0) (
REDTASK # 115 TIME= 144.000 N=  1 (task/type/sgbra)=( 115/14/0) (
REDTASK # 116 TIME= 143.000 N=  1 (task/type/sgbra)=( 116/14/0) (
REDTASK # 117 TIME= 142.000 N=  1 (task/type/sgbra)=( 117/14/0) (
REDTASK # 118 TIME= 141.000 N=  1 (task/type/sgbra)=( 118/31/0) (
REDTASK # 119 TIME= 140.000 N=  1 (task/type/sgbra)=( 119/31/0) (
REDTASK # 120 TIME= 139.000 N=  1 (task/type/sgbra)=( 120/31/0) (
REDTASK # 121 TIME= 138.000 N=  1 (task/type/sgbra)=( 121/31/0) (
REDTASK # 122 TIME= 137.000 N=  1 (task/type/sgbra)=( 122/31/0) (
REDTASK # 123 TIME= 136.000 N=  1 (task/type/sgbra)=( 123/31/0) (
REDTASK # 124 TIME= 135.000 N=  1 (task/type/sgbra)=( 124/31/0) (
REDTASK # 125 TIME= 134.000 N=  1 (task/type/sgbra)=( 125/31/0) (
REDTASK # 126 TIME= 133.000 N=  1 (task/type/sgbra)=( 126/31/0) (
REDTASK # 127 TIME= 132.000 N=  1 (task/type/sgbra)=( 127/32/0) (
REDTASK # 128 TIME= 131.000 N=  1 (task/type/sgbra)=( 128/32/0) (
REDTASK # 129 TIME= 130.000 N=  1 (task/type/sgbra)=( 129/32/0) (
REDTASK # 130 TIME= 129.000 N=  1 (task/type/sgbra)=( 130/32/0) (
REDTASK # 131 TIME= 128.000 N=  1 (task/type/sgbra)=( 131/32/0) (
REDTASK # 132 TIME= 127.000 N=  1 (task/type/sgbra)=( 132/32/0) (
REDTASK # 133 TIME= 126.000 N=  1 (task/type/sgbra)=( 133/32/0) (
REDTASK # 134 TIME= 125.000 N=  1 (task/type/sgbra)=( 134/32/0) (
REDTASK # 135 TIME= 124.000 N=  1 (task/type/sgbra)=( 135/32/0) (
REDTASK # 136 TIME= 123.000 N=  1 (task/type/sgbra)=( 136/ 1/0) (
REDTASK # 137 TIME= 122.000 N=  1 (task/type/sgbra)=( 137/ 1/0) (
REDTASK # 138 TIME= 121.000 N=  1 (task/type/sgbra)=( 138/ 1/0) (
REDTASK # 139 TIME= 120.000 N=  1 (task/type/sgbra)=( 139/ 1/0) (
REDTASK # 140 TIME= 119.000 N=  1 (task/type/sgbra)=( 140/ 1/0) (
REDTASK # 141 TIME= 118.000 N=  1 (task/type/sgbra)=( 141/ 1/0) (
REDTASK # 142 TIME= 117.000 N=  1 (task/type/sgbra)=( 142/ 1/0) (
REDTASK # 143 TIME= 116.000 N=  1 (task/type/sgbra)=( 143/ 1/0) (
REDTASK # 144 TIME= 115.000 N=  1 (task/type/sgbra)=( 144/ 1/0) (
REDTASK # 145 TIME= 114.000 N=  1 (task/type/sgbra)=( 145/ 1/0) (
REDTASK # 146 TIME= 113.000 N=  1 (task/type/sgbra)=( 146/ 1/0) (
REDTASK # 147 TIME= 112.000 N=  1 (task/type/sgbra)=( 147/ 1/0) (
REDTASK # 148 TIME= 111.000 N=  1 (task/type/sgbra)=( 148/ 1/0) (
REDTASK # 149 TIME= 110.000 N=  1 (task/type/sgbra)=( 149/ 1/0) (
REDTASK # 150 TIME= 109.000 N=  1 (task/type/sgbra)=( 150/ 1/0) (
REDTASK # 151 TIME= 108.000 N=  1 (task/type/sgbra)=( 151/ 1/0) (
REDTASK # 152 TIME= 107.000 N=  1 (task/type/sgbra)=( 152/ 1/0) (
REDTASK # 153 TIME= 106.000 N=  1 (task/type/sgbra)=( 153/ 1/0) (
REDTASK # 154 TIME= 105.000 N=  1 (task/type/sgbra)=( 154/ 1/0) (
REDTASK # 155 TIME= 104.000 N=  1 (task/type/sgbra)=( 155/ 1/0) (
REDTASK # 156 TIME= 103.000 N=  1 (task/type/sgbra)=( 156/ 1/0) (
REDTASK # 157 TIME= 102.000 N=  1 (task/type/sgbra)=( 157/ 1/0) (
REDTASK # 158 TIME= 101.000 N=  1 (task/type/sgbra)=( 158/ 1/0) (
REDTASK # 159 TIME= 100.000 N=  1 (task/type/sgbra)=( 159/ 1/0) (
REDTASK # 160 TIME=  99.000 N=  1 (task/type/sgbra)=( 160/ 1/0) (
REDTASK # 161 TIME=  98.000 N=  1 (task/type/sgbra)=( 161/ 1/0) (
REDTASK # 162 TIME=  97.000 N=  1 (task/type/sgbra)=( 162/ 1/0) (
REDTASK # 163 TIME=  96.000 N=  1 (task/type/sgbra)=( 163/ 1/0) (
REDTASK # 164 TIME=  95.000 N=  1 (task/type/sgbra)=( 164/ 2/0) (
REDTASK # 165 TIME=  94.000 N=  1 (task/type/sgbra)=( 165/ 2/0) (
REDTASK # 166 TIME=  93.000 N=  1 (task/type/sgbra)=( 166/ 2/0) (
REDTASK # 167 TIME=  92.000 N=  1 (task/type/sgbra)=( 167/ 3/1) (
REDTASK # 168 TIME=  91.000 N=  1 (task/type/sgbra)=( 168/ 8/2) (
REDTASK # 169 TIME=  90.000 N=  1 (task/type/sgbra)=( 169/ 3/1) (
REDTASK # 170 TIME=  89.000 N=  1 (task/type/sgbra)=( 170/ 8/2) (
REDTASK # 171 TIME=  88.000 N=  1 (task/type/sgbra)=( 171/ 3/1) (
REDTASK # 172 TIME=  87.000 N=  1 (task/type/sgbra)=( 172/ 8/2) (
REDTASK # 173 TIME=  86.000 N=  1 (task/type/sgbra)=( 173/ 3/1) (
REDTASK # 174 TIME=  85.000 N=  1 (task/type/sgbra)=( 174/ 8/2) (
REDTASK # 175 TIME=  84.000 N=  1 (task/type/sgbra)=( 175/ 3/1) (
REDTASK # 176 TIME=  83.000 N=  1 (task/type/sgbra)=( 176/ 8/2) (
REDTASK # 177 TIME=  82.000 N=  1 (task/type/sgbra)=( 177/ 3/1) (
REDTASK # 178 TIME=  81.000 N=  1 (task/type/sgbra)=( 178/ 8/2) (
REDTASK # 179 TIME=  80.000 N=  1 (task/type/sgbra)=( 179/ 3/1) (
REDTASK # 180 TIME=  79.000 N=  1 (task/type/sgbra)=( 180/ 8/2) (
REDTASK # 181 TIME=  78.000 N=  1 (task/type/sgbra)=( 181/ 3/1) (
REDTASK # 182 TIME=  77.000 N=  1 (task/type/sgbra)=( 182/ 8/2) (
REDTASK # 183 TIME=  76.000 N=  1 (task/type/sgbra)=( 183/ 3/1) (
REDTASK # 184 TIME=  75.000 N=  1 (task/type/sgbra)=( 184/ 8/2) (
REDTASK # 185 TIME=  74.000 N=  1 (task/type/sgbra)=( 185/ 3/1) (
REDTASK # 186 TIME=  73.000 N=  1 (task/type/sgbra)=( 186/ 8/2) (
REDTASK # 187 TIME=  72.000 N=  1 (task/type/sgbra)=( 187/ 3/1) (
REDTASK # 188 TIME=  71.000 N=  1 (task/type/sgbra)=( 188/ 8/2) (
REDTASK # 189 TIME=  70.000 N=  1 (task/type/sgbra)=( 189/ 3/1) (
REDTASK # 190 TIME=  69.000 N=  1 (task/type/sgbra)=( 190/ 8/2) (
REDTASK # 191 TIME=  68.000 N=  1 (task/type/sgbra)=( 191/ 3/1) (
REDTASK # 192 TIME=  67.000 N=  1 (task/type/sgbra)=( 192/ 8/2) (
REDTASK # 193 TIME=  66.000 N=  1 (task/type/sgbra)=( 193/ 3/1) (
REDTASK # 194 TIME=  65.000 N=  1 (task/type/sgbra)=( 194/ 8/2) (
REDTASK # 195 TIME=  64.000 N=  1 (task/type/sgbra)=( 195/ 3/1) (
REDTASK # 196 TIME=  63.000 N=  1 (task/type/sgbra)=( 196/ 8/2) (
REDTASK # 197 TIME=  62.000 N=  1 (task/type/sgbra)=( 197/ 4/1) (
REDTASK # 198 TIME=  61.000 N=  1 (task/type/sgbra)=( 198/ 9/2) (
REDTASK # 199 TIME=  60.000 N=  1 (task/type/sgbra)=( 199/ 4/1) (
REDTASK # 200 TIME=  59.000 N=  1 (task/type/sgbra)=( 200/ 9/2) (
REDTASK # 201 TIME=  58.000 N=  1 (task/type/sgbra)=( 201/ 4/1) (
REDTASK # 202 TIME=  57.000 N=  1 (task/type/sgbra)=( 202/ 9/2) (
REDTASK # 203 TIME=  56.000 N=  1 (task/type/sgbra)=( 203/ 4/1) (
REDTASK # 204 TIME=  55.000 N=  1 (task/type/sgbra)=( 204/ 9/2) (
REDTASK # 205 TIME=  54.000 N=  1 (task/type/sgbra)=( 205/ 4/1) (
REDTASK # 206 TIME=  53.000 N=  1 (task/type/sgbra)=( 206/ 9/2) (
REDTASK # 207 TIME=  52.000 N=  1 (task/type/sgbra)=( 207/ 4/1) (
REDTASK # 208 TIME=  51.000 N=  1 (task/type/sgbra)=( 208/ 9/2) (
REDTASK # 209 TIME=  50.000 N=  1 (task/type/sgbra)=( 209/ 4/1) (
REDTASK # 210 TIME=  49.000 N=  1 (task/type/sgbra)=( 210/ 9/2) (
REDTASK # 211 TIME=  48.000 N=  1 (task/type/sgbra)=( 211/ 4/1) (
REDTASK # 212 TIME=  47.000 N=  1 (task/type/sgbra)=( 212/ 9/2) (
REDTASK # 213 TIME=  46.000 N=  1 (task/type/sgbra)=( 213/ 4/1) (
REDTASK # 214 TIME=  45.000 N=  1 (task/type/sgbra)=( 214/ 9/2) (
REDTASK # 215 TIME=  44.000 N=  1 (task/type/sgbra)=( 215/ 4/1) (
REDTASK # 216 TIME=  43.000 N=  1 (task/type/sgbra)=( 216/ 9/2) (
REDTASK # 217 TIME=  42.000 N=  1 (task/type/sgbra)=( 217/ 4/1) (
REDTASK # 218 TIME=  41.000 N=  1 (task/type/sgbra)=( 218/ 9/2) (
REDTASK # 219 TIME=  40.000 N=  1 (task/type/sgbra)=( 219/ 4/1) (
REDTASK # 220 TIME=  39.000 N=  1 (task/type/sgbra)=( 220/ 9/2) (
REDTASK # 221 TIME=  38.000 N=  1 (task/type/sgbra)=( 221/ 4/1) (
REDTASK # 222 TIME=  37.000 N=  1 (task/type/sgbra)=( 222/ 9/2) (
REDTASK # 223 TIME=  36.000 N=  1 (task/type/sgbra)=( 223/ 4/1) (
REDTASK # 224 TIME=  35.000 N=  1 (task/type/sgbra)=( 224/ 9/2) (
REDTASK # 225 TIME=  34.000 N=  1 (task/type/sgbra)=( 225/ 4/1) (
REDTASK # 226 TIME=  33.000 N=  1 (task/type/sgbra)=( 226/ 9/2) (
REDTASK # 227 TIME=  32.000 N=  1 (task/type/sgbra)=( 227/42/1) (
REDTASK # 228 TIME=  31.000 N=  1 (task/type/sgbra)=( 228/42/1) (
REDTASK # 229 TIME=  30.000 N=  1 (task/type/sgbra)=( 229/43/1) (
REDTASK # 230 TIME=  29.000 N=  1 (task/type/sgbra)=( 230/43/1) (
REDTASK # 231 TIME=  28.000 N=  1 (task/type/sgbra)=( 231/43/1) (
REDTASK # 232 TIME=  27.000 N=  1 (task/type/sgbra)=( 232/43/1) (
REDTASK # 233 TIME=  26.000 N=  1 (task/type/sgbra)=( 233/43/1) (
REDTASK # 234 TIME=  25.000 N=  1 (task/type/sgbra)=( 234/43/1) (
REDTASK # 235 TIME=  24.000 N=  1 (task/type/sgbra)=( 235/44/1) (
REDTASK # 236 TIME=  23.000 N=  1 (task/type/sgbra)=( 236/44/1) (
REDTASK # 237 TIME=  22.000 N=  1 (task/type/sgbra)=( 237/44/1) (
REDTASK # 238 TIME=  21.000 N=  1 (task/type/sgbra)=( 238/44/1) (
REDTASK # 239 TIME=  20.000 N=  1 (task/type/sgbra)=( 239/44/1) (
REDTASK # 240 TIME=  19.000 N=  1 (task/type/sgbra)=( 240/44/1) (
REDTASK # 241 TIME=  18.000 N=  1 (task/type/sgbra)=( 241/75/1) (
REDTASK # 242 TIME=  17.000 N=  1 (task/type/sgbra)=( 242/75/1) (
REDTASK # 243 TIME=  16.000 N=  1 (task/type/sgbra)=( 243/75/1) (
REDTASK # 244 TIME=  15.000 N=  1 (task/type/sgbra)=( 244/75/1) (
REDTASK # 245 TIME=  14.000 N=  1 (task/type/sgbra)=( 245/75/1) (
REDTASK # 246 TIME=  13.000 N=  1 (task/type/sgbra)=( 246/75/1) (
REDTASK # 247 TIME=  12.000 N=  1 (task/type/sgbra)=( 247/75/1) (
REDTASK # 248 TIME=  11.000 N=  1 (task/type/sgbra)=( 248/76/1) (
REDTASK # 249 TIME=  10.000 N=  1 (task/type/sgbra)=( 249/76/1) (
REDTASK # 250 TIME=   9.000 N=  1 (task/type/sgbra)=( 250/77/1) (
REDTASK # 251 TIME=   8.000 N=  1 (task/type/sgbra)=( 251/77/1) (
REDTASK # 252 TIME=   7.000 N=  1 (task/type/sgbra)=( 252/77/1) (
REDTASK # 253 TIME=   6.000 N=  1 (task/type/sgbra)=( 253/77/1) (
REDTASK # 254 TIME=   5.000 N=  1 (task/type/sgbra)=( 254/77/1) (
REDTASK # 255 TIME=   4.000 N=  1 (task/type/sgbra)=( 255/78/1) (
REDTASK # 256 TIME=   3.000 N=  1 (task/type/sgbra)=( 256/78/1) (
REDTASK # 257 TIME=   2.000 N=  1 (task/type/sgbra)=( 257/78/1) (
REDTASK # 258 TIME=   1.000 N=  1 (task/type/sgbra)=( 258/78/1) (
REDTASK # 259 TIME=   0.000 N=  1 (task/type/sgbra)=( 259/78/1) (
 initializing v-file: 1:              60834170

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      3 vectors will be written to unit 11 beginning with logical record   1

            3 vectors will be created
 ========= Executing OUT-OF-CORE method ========
Wait time (non-blocking I/O):    0.000000  seconds
Wait time (non-blocking+overhead I/O):    0.000000  seconds
Wait time (sync mult):    0.431243  seconds


====================================================================================================
Diagonal     counts:  0x:       76653 2x:           0 4x:           0
All internal counts: zz :      792015 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 reference space has dimension      22
 dsyevx: computed roots 1 to    6(converged:   6)

    root           eigenvalues
    ----           ------------
       1        -455.3557136666
       2        -455.2717251450
       3        -455.2166902946
       4        -455.1333028931
       5        -455.0742656104
       6        -455.0470061675

 strefv generated    3 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt=                  2955

         vector  1 from unit 11 written to unit 49 filename cirefv              

 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=  2955)...
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:          60834170
 number of initial trial vectors:                         3
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                    1
 residual norm convergence criteria:               0.001000

          starting bk iteration   1

 ========= Executing OUT-OF-CORE method ========
Wait time (non-blocking I/O):    0.000005  seconds
Wait time (non-blocking+overhead I/O):    0.004468  seconds
Wait time (sync mult):    1.116063  seconds


====================================================================================================
Diagonal     counts:  0x:     1073670 2x:      275673 4x:       44894
All internal counts: zz :      792015 yy:           0 xx:           0 ww:           0
One-external counts: yz :     2416841 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:       21915 wz:       27811 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 ========= Executing OUT-OF-CORE method ========
Wait time (non-blocking I/O):    0.000004  seconds
Wait time (non-blocking+overhead I/O):    0.000177  seconds
Wait time (sync mult):    1.279863  seconds


====================================================================================================
Diagonal     counts:  0x:     1073670 2x:      275673 4x:       44894
All internal counts: zz :      792015 yy:           0 xx:           0 ww:           0
One-external counts: yz :     2416841 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:       21915 wz:       27811 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 ========= Executing OUT-OF-CORE method ========
Wait time (non-blocking I/O):    0.000000  seconds
Wait time (non-blocking+overhead I/O):    0.000000  seconds
Wait time (sync mult):    1.355805  seconds


====================================================================================================
Diagonal     counts:  0x:     1073670 2x:      275673 4x:       44894
All internal counts: zz :      792015 yy:           0 xx:           0 ww:           0
One-external counts: yz :     2416841 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:       21915 wz:       27811 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3
   ht   1    -5.20969089
   ht   2    -0.00000000    -5.12570237
   ht   3     0.00000000    -0.00000000    -5.07066751

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3
 ref    1   -1.00000      -9.392443E-13   2.754072E-13

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3
 ref    1    1.00000       8.821798E-25   7.584911E-26

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1    -1.00000000    -0.00000000     0.00000000
rescaling new expansion vector ... old norm=      0.8324503497

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -455.3557136666  3.0198E-14  1.7634E+00  2.2142E+00  1.0000E-03   
 mr-sdci #  1  2   -455.2717251450 -3.5527E-15  0.0000E+00  2.2189E+00  1.0000E-04   
 mr-sdci #  1  3   -455.2166902946 -1.0125E-13  0.0000E+00  7.7552E-01  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -455.3557136666  3.0198E-14  1.7634E+00  2.2142E+00  1.0000E-03   
 mr-sdci #  1  2   -455.2717251450 -3.5527E-15  0.0000E+00  2.2189E+00  1.0000E-04   
 mr-sdci #  1  3   -455.2166902946 -1.0125E-13  0.0000E+00  7.7552E-01  1.0000E-04   
 
    1 of the   4 expansion vectors are transformed.
    1 of the   3 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:          60834170
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                1
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                   20
 residual norm convergence criteria:               0.005000

          starting ci iteration   1

 Final subspace hamiltonian 

                ht   1
   ht   1    -5.20969089

          calcsovref: eigensolution overlap with references block   1

              v      1
 ref    1   -1.00000    

          calcsovref: reference weight per eigenvector block   1

              v      1
 ref    1    1.00000    

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1

          calcsovref: aa weight per eigenvector block   1

              v      1

          reference overlap matrix  block   1

                ci   1
 ref:   1    -1.00000000
rescaling new expansion vector ... old norm=      0.8324503497

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -455.3557136666 -8.8818E-16  1.7634E+00  2.2142E+00  5.0000E-03   
 
 root number  1 is used to define the new expansion vector.

          starting ci iteration   2

 ========= Executing OUT-OF-CORE method ========
Wait time (non-blocking I/O):    0.000021  seconds
Wait time (non-blocking+overhead I/O):    0.102118  seconds
Wait time (sync mult):    0.555300  seconds


====================================================================================================
Diagonal     counts:  0x:     1073670 2x:      275673 4x:       44894
All internal counts: zz :      792015 yy:    11098814 xx:     2856674 ww:     3114304
One-external counts: yz :     2416841 yx:     5047353 yw:     5176784
Two-external counts: yy :     2021086 ww:      720740 xx:      689884 xz:       21915 wz:       27811 wx:     1184168
Three-ext.   counts: yx :     1583914 yw:     1634032

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2
   ht   1    -5.20969089
   ht   2    -1.93270882    -2.81168384

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2
 ref    1   0.873833      -0.486225    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2
 ref    1   0.763585       0.236415    

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.87383339    -0.48622546
rescaling new expansion vector ... old norm=      0.0324758178

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1   -456.4311271764  1.0754E+00  8.6700E-02  5.3756E-01  5.0000E-03   
 mr-sdci #  2  2   -451.8822931092 -3.3894E+00  0.0000E+00  0.0000E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.

          starting ci iteration   3

 ========= Executing OUT-OF-CORE method ========
Wait time (non-blocking I/O):    0.000014  seconds
Wait time (non-blocking+overhead I/O):    0.117046  seconds
Wait time (sync mult):    0.037807  seconds


====================================================================================================
Diagonal     counts:  0x:     1073670 2x:      275673 4x:       44894
All internal counts: zz :      792015 yy:    11098814 xx:     2856674 ww:     3114304
One-external counts: yz :     2416841 yx:     5047353 yw:     5176784
Two-external counts: yy :     2021086 ww:      720740 xx:      689884 xz:       21915 wz:       27811 wx:     1184168
Three-ext.   counts: yx :     1583914 yw:     1634032

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3
   ht   1    -5.20969089
   ht   2    -1.93270882    -2.81168384
   ht   3     0.33776235    -1.17513532    -3.00028007

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3
 ref    1   0.876591      -0.120390       0.465933    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3
 ref    1   0.768412       1.449379E-02   0.217094    

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.87659136    -0.12039017     0.46593325
rescaling new expansion vector ... old norm=      0.0016017342

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1   -456.5032016154  7.2074E-02  4.6326E-03  1.2564E-01  5.0000E-03   
 mr-sdci #  3  2   -453.2738116457  1.3915E+00  0.0000E+00  0.0000E+00  1.0000E-04   
 mr-sdci #  3  3   -451.4331265829 -3.7836E+00  0.0000E+00  1.1861E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.

          starting ci iteration   4

 ========= Executing OUT-OF-CORE method ========
Wait time (non-blocking I/O):    0.000016  seconds
Wait time (non-blocking+overhead I/O):    0.088765  seconds
Wait time (sync mult):    0.009065  seconds


====================================================================================================
Diagonal     counts:  0x:     1073670 2x:      275673 4x:       44894
All internal counts: zz :      792015 yy:    11098814 xx:     2856674 ww:     3114304
One-external counts: yz :     2416841 yx:     5047353 yw:     5176784
Two-external counts: yy :     2021086 ww:      720740 xx:      689884 xz:       21915 wz:       27811 wx:     1184168
Three-ext.   counts: yx :     1583914 yw:     1634032

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4
   ht   1    -5.20969089
   ht   2    -1.93270882    -2.81168384
   ht   3     0.33776235    -1.17513532    -3.00028007
   ht   4    -0.10796176    -0.55778707    -0.04863251    -2.42249037

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4
 ref    1   0.877512      -0.121280       4.017830E-02  -0.462222    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4
 ref    1   0.770028       1.470893E-02   1.614296E-03   0.213649    

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.87751219    -0.12128038     0.04017830    -0.46222195
rescaling new expansion vector ... old norm=      0.0001721832

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1   -456.5067764563  3.5748E-03  4.3285E-04  3.6961E-02  5.0000E-03   
 mr-sdci #  4  2   -453.5074580007  2.3365E-01  0.0000E+00  9.2340E-01  1.0000E-04   
 mr-sdci #  4  3   -452.4319096446  9.9878E-01  0.0000E+00  3.8368E-01  1.0000E-04   
 mr-sdci #  4  4   -451.3564256517  1.2104E+00  0.0000E+00  1.2848E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.

          starting ci iteration   5

 ========= Executing OUT-OF-CORE method ========
Wait time (non-blocking I/O):    0.000016  seconds
Wait time (non-blocking+overhead I/O):    0.118553  seconds
Wait time (sync mult):    0.061224  seconds


====================================================================================================
Diagonal     counts:  0x:     1073670 2x:      275673 4x:       44894
All internal counts: zz :      792015 yy:    11098814 xx:     2856674 ww:     3114304
One-external counts: yz :     2416841 yx:     5047353 yw:     5176784
Two-external counts: yy :     2021086 ww:      720740 xx:      689884 xz:       21915 wz:       27811 wx:     1184168
Three-ext.   counts: yx :     1583914 yw:     1634032

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1    -5.20969089
   ht   2    -1.93270882    -2.81168384
   ht   3     0.33776235    -1.17513532    -3.00028007
   ht   4    -0.10796176    -0.55778707    -0.04863251    -2.42249037
   ht   5     0.23409468    -0.06917266    -0.35013970    -0.10386463    -2.75780925

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1   0.877602      -9.178759E-02  -6.875673E-02  -7.690420E-02  -0.459073    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1   0.770185       8.424961E-03   4.727488E-03   5.914256E-03   0.210748    

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     0.87760210    -0.09178759    -0.06875673    -0.07690420    -0.45907281
rescaling new expansion vector ... old norm=      0.0000180530

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1   -456.5071063377  3.2988E-04  4.8594E-05  1.2594E-02  5.0000E-03   
 mr-sdci #  5  2   -453.7942197986  2.8676E-01  0.0000E+00  9.7676E-01  1.0000E-04   
 mr-sdci #  5  3   -452.9043854974  4.7248E-01  0.0000E+00  1.0197E-01  1.0000E-04   
 mr-sdci #  5  4   -452.2748278284  9.1840E-01  0.0000E+00  1.8836E+00  1.0000E-04   
 mr-sdci #  5  5   -451.3517790821  1.2058E+00  0.0000E+00  1.3626E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.

          starting ci iteration   6

 ========= Executing OUT-OF-CORE method ========
Wait time (non-blocking I/O):    0.000013  seconds
Wait time (non-blocking+overhead I/O):    0.099260  seconds
Wait time (sync mult):    0.062282  seconds


====================================================================================================
Diagonal     counts:  0x:     1073670 2x:      275673 4x:       44894
All internal counts: zz :      792015 yy:    11098814 xx:     2856674 ww:     3114304
One-external counts: yz :     2416841 yx:     5047353 yw:     5176784
Two-external counts: yy :     2021086 ww:      720740 xx:      689884 xz:       21915 wz:       27811 wx:     1184168
Three-ext.   counts: yx :     1583914 yw:     1634032

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1    -5.20969089
   ht   2    -1.93270882    -2.81168384
   ht   3     0.33776235    -1.17513532    -3.00028007
   ht   4    -0.10796176    -0.55778707    -0.04863251    -2.42249037
   ht   5     0.23409468    -0.06917266    -0.35013970    -0.10386463    -2.75780925
   ht   6    -0.06373931    -0.27193562    -0.75882021    -0.09621175    -0.93847161    -3.27405991

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.877610      -3.206519E-02  -0.125526      -2.883010E-02   2.976762E-02  -0.459672    

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.770199       1.028176E-03   1.575689E-02   8.311745E-04   8.861109E-04   0.211298    

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.87760993    -0.03206519    -0.12552647    -0.02883010     0.02976762    -0.45967212

 trial vector basis is being transformed.  new dimension:   3
rescaling new expansion vector ... old norm=      0.0000072238

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  6  1   -456.5071549931  4.8655E-05  1.4422E-05  6.2432E-03  5.0000E-03   
 mr-sdci #  6  2   -454.5583010644  7.6408E-01  0.0000E+00  7.7799E-01  1.0000E-04   
 mr-sdci #  6  3   -453.2015262324  2.9714E-01  0.0000E+00  9.8185E-01  1.0000E-04   
 mr-sdci #  6  4   -452.6535630326  3.7874E-01  0.0000E+00  8.8675E-01  1.0000E-04   
 mr-sdci #  6  5   -451.9557242486  6.0395E-01  0.0000E+00  2.0355E+00  1.0000E-04   
 mr-sdci #  6  6   -451.3480305785  1.2020E+00  0.0000E+00  1.3644E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.

          starting ci iteration   7

 ========= Executing OUT-OF-CORE method ========
Wait time (non-blocking I/O):    0.000020  seconds
Wait time (non-blocking+overhead I/O):    0.108033  seconds
Wait time (sync mult):    0.004917  seconds


====================================================================================================
Diagonal     counts:  0x:     1073670 2x:      275673 4x:       44894
All internal counts: zz :      792015 yy:    11098814 xx:     2856674 ww:     3114304
One-external counts: yz :     2416841 yx:     5047353 yw:     5176784
Two-external counts: yy :     2021086 ww:      720740 xx:      689884 xz:       21915 wz:       27811 wx:     1184168
Three-ext.   counts: yx :     1583914 yw:     1634032

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4
   ht   1    -6.36113221
   ht   2     0.00000000    -4.41227828
   ht   3    -0.00000000     0.00000000    -3.05550345
   ht   4    -0.09560247     1.63462801    -0.68746437    -4.52327619

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4
 ref    1  -0.877549      -8.884083E-03  -9.992749E-02  -8.301589E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4
 ref    1   0.770093       7.892693E-05   9.985502E-03   6.891638E-03

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1    -0.87754930    -0.00888408    -0.09992749    -0.08301589
rescaling new expansion vector ... old norm=      0.0000056900

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  7  1   -456.5071756227  2.0630E-05  1.5676E-05  7.3053E-03  5.0000E-03   
 mr-sdci #  7  2   -455.6283668597  1.0701E+00  0.0000E+00  1.6951E+00  1.0000E-04   
 mr-sdci #  7  3   -453.7134065414  5.1188E-01  0.0000E+00  7.3822E+00  1.0000E-04   
 mr-sdci #  7  4   -452.7829378295  1.2937E-01  0.0000E+00  9.9388E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.

          starting ci iteration   8

 ========= Executing OUT-OF-CORE method ========
Wait time (non-blocking I/O):    0.000023  seconds
Wait time (non-blocking+overhead I/O):    0.127756  seconds
Wait time (sync mult):    0.005213  seconds


====================================================================================================
Diagonal     counts:  0x:     1073670 2x:      275673 4x:       44894
All internal counts: zz :      792015 yy:    11098814 xx:     2856674 ww:     3114304
One-external counts: yz :     2416841 yx:     5047353 yw:     5176784
Two-external counts: yy :     2021086 ww:      720740 xx:      689884 xz:       21915 wz:       27811 wx:     1184168
Three-ext.   counts: yx :     1583914 yw:     1634032

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1    -6.36113221
   ht   2     0.00000000    -4.41227828
   ht   3    -0.00000000     0.00000000    -3.05550345
   ht   4    -0.09560247     1.63462801    -0.68746437    -4.52327619
   ht   5     0.09407707    -0.23194259     0.48709257     1.38955499    -2.87188746

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1  -0.877449       1.761178E-02   6.132106E-02   0.114589       1.644621E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1   0.769917       3.101748E-04   3.760273E-03   1.313072E-02   2.704779E-04

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1    -0.87744891     0.01761178     0.06132106     0.11458933     0.01644621
rescaling new expansion vector ... old norm=      0.0000074753

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  8  1   -456.5072067610  3.1138E-05  1.6257E-05  6.7721E-03  5.0000E-03   
 mr-sdci #  8  2   -456.1675048313  5.3914E-01  0.0000E+00  1.8878E+00  1.0000E-04   
 mr-sdci #  8  3   -454.1668368061  4.5343E-01  0.0000E+00  3.7089E+00  1.0000E-04   
 mr-sdci #  8  4   -452.9511338196  1.6820E-01  0.0000E+00  1.1778E+01  1.0000E-04   
 mr-sdci #  8  5   -451.9850788159  2.9355E-02  0.0000E+00  1.6367E+00  1.0000E-04   
 
 root number  1 is used to define the new expansion vector.

          starting ci iteration   9

 ========= Executing OUT-OF-CORE method ========
Wait time (non-blocking I/O):    0.000016  seconds
Wait time (non-blocking+overhead I/O):    0.093251  seconds
Wait time (sync mult):    0.060332  seconds


====================================================================================================
Diagonal     counts:  0x:     1073670 2x:      275673 4x:       44894
All internal counts: zz :      792015 yy:    11098814 xx:     2856674 ww:     3114304
One-external counts: yz :     2416841 yx:     5047353 yw:     5176784
Two-external counts: yy :     2021086 ww:      720740 xx:      689884 xz:       21915 wz:       27811 wx:     1184168
Three-ext.   counts: yx :     1583914 yw:     1634032

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1    -6.36113221
   ht   2     0.00000000    -4.41227828
   ht   3    -0.00000000     0.00000000    -3.05550345
   ht   4    -0.09560247     1.63462801    -0.68746437    -4.52327619
   ht   5     0.09407707    -0.23194259     0.48709257     1.38955499    -2.87188746
   ht   6    -0.00526231     0.13849496     0.55519248     1.64585326    -1.00616000    -3.70212365

          calcsovref: eigensolution overlap with references block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.877290      -2.491929E-02  -3.518000E-02   0.114702      -4.992753E-02  -1.685682E-02

          calcsovref: reference weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.769637       6.209708E-04   1.237632E-03   1.315652E-02   2.492759E-03   2.841522E-04

          calcsovref: sovlaa in eigenvector basis block   1

               ev    1        ev    2        ev    3        ev    4        ev    5        ev    6

          calcsovref: aa weight per eigenvector block   1

              v      1       v      2       v      3       v      4       v      5       v      6

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.87728958    -0.02491929    -0.03518000     0.11470187    -0.04992753    -0.01685682

 trial vector basis is being transformed.  new dimension:   3
rescaling new expansion vector ... old norm=      0.0000025376

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  9  1   -456.5072299940  2.3233E-05  6.7186E-06  4.6644E-03  5.0000E-03   
 mr-sdci #  9  2   -456.3110711447  1.4357E-01  0.0000E+00  1.6682E+00  1.0000E-04   
 mr-sdci #  9  3   -454.6598357593  4.9300E-01  0.0000E+00  6.1058E-01  1.0000E-04   
 mr-sdci #  9  4   -453.1670039864  2.1587E-01  0.0000E+00  1.0931E+01  1.0000E-04   
 mr-sdci #  9  5   -452.6759528693  6.9087E-01  0.0000E+00  5.8464E+00  1.0000E-04   
 mr-sdci #  9  6   -451.9661985811  6.1817E-01  0.0000E+00  2.2485E+00  1.0000E-04   
 

 mr-sdci  convergence criteria satisfied after  9 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  9  1   -456.5072299940  2.3233E-05  6.7186E-06  4.6644E-03  5.0000E-03   
 mr-sdci #  9  2   -456.3110711447  1.4357E-01  0.0000E+00  1.6682E+00  1.0000E-04   
 mr-sdci #  9  3   -454.6598357593  4.9300E-01  0.0000E+00  6.1058E-01  1.0000E-04   

####################CIUDGINFO####################

   ci vector at position   1 energy= -456.507229993963

################END OF CIUDGINFO################

 
    1 of the   4 expansion vectors are transformed.
    1 of the   3 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =      -456.5072299940

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7    8    9   10   11   12   13   14   15   16   17
                                               18   19   20   21   22   23   24   25   26   27   28   29   30

                                          orbital     9   10   11   12   13   14   15   16   17   18   19   20   83   84   85  121  122
                                              123  124  125  126  127  128  129  180   86   87   88  181  182

                                         symmetry   a1   a1   a1   a1   a1   a1   a1   a1   a1   a1   a1   a1   b1   b1   b1   b2   b2 
                                              b2   b2   b2   b2   b2   b2   b2   a2   b1   b1   b1   a2   a2 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1  0.405670                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -      
 z*  1  1       3  0.033134                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    -      
 z*  1  1       5 -0.750403                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +-   +-   +-   +-   +-   +-   +         +-    - 
 z*  1  1       6 -0.066554                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +-   +-   +-   +-   +-   +-   +          -   +- 
 z*  1  1       8  0.045140                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +-   +-   +-   +-   +-   +-        +     -   +- 
 z*  1  1       9 -0.068224                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-    -      
 z*  1  1      10  0.012392                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-         - 
 z*  1  1      11 -0.031843                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +-   +-   +-   +-   +-   +    +-        +-    - 
 z*  1  1      12 -0.079837                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +-   +-   +-   +-   +-   +    +-         -   +- 
 z*  1  1      13 -0.137047                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +-   +-   +-   +-   +-   +     -   +    +-    - 
 z*  1  1      14 -0.012644                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +-   +-   +-   +-   +-   +     -   +     -   +- 
 z*  1  1      15 -0.034440                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +-   +-   +-   +-   +-   +    +     -   +-    - 
 z*  1  1      17  0.011413                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +-   +-   +-   +-   +-   +         +-   +-    - 
 z*  1  1      20 -0.026669                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +-   +-   +-   +-   +-        +-   +     -   +- 
 z*  1  1      21 -0.044814                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +-   +-   +-   +-   +-        +    +-   +-    - 
 z   1  1     570 -0.010099                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +-   +- 
                                             +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-    - 
 z   1  1     574  0.014972                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +- 
                                             +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -      
 z   1  1     577  0.024544                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +- 
                                             +-   +-   +-   +-   +-   +-   +-   +-   +-   +-         -   +- 
 z   1  1     578  0.029781                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +- 
                                             +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +    +-    - 
 z   1  1     607  0.012367                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +-   +- 
                                             +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-    - 
 z   1  1     621 -0.011276                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +-   +-   +- 
                                             +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-    - 
 y   1  1    3007  0.023619              1( b1 )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +-   +-   +-   +-   +-   +-   +-              - 
 y   1  1    3008 -0.019003              2( b1 )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +-   +-   +-   +-   +-   +-   +-              - 
 y   1  1    3058 -0.023079              1( b1 )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +     -      
 y   1  1    3059  0.018366              2( b1 )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +     -      
 y   1  1    3119  0.021557              4( a2 )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +-   +-   +-   +-   +-   +-    -        +-      
 y   1  1    3182  0.026120              1( b1 )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -    -      
 y   1  1    3183 -0.017850              2( b1 )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -    -      
 y   1  1    3408 -0.029687              1( b1 )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +-   +-   +-   +-   +-   +-             +-    - 
 y   1  1    3409  0.021720              2( b1 )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +-   +-   +-   +-   +-   +-             +-    - 
 y   1  1   10314 -0.011509              2( b2 )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +-   +-   +-    -   +-   +    +     -   +-    - 
 y   1  1  201954  0.015488              2( b1 )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +- 
                                             +-   +-   +-   +-   +-   +-   +-   +-   +-    -        +-    - 
 y   1  1  202485  0.012855              1( b1 )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +- 
                                             +-   +-   +-   +-   +-   +-   +-   +-    -   +     -   +-    - 
 y   1  1  202486  0.012377              2( b1 )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +- 
                                             +-   +-   +-   +-   +-   +-   +-   +-    -   +     -   +-    - 
 y   1  1  248766  0.013618              2( b1 )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +- 
                                             +-   +-   +-   +-   +-   +-   +-   +-   +-    -        +-    - 
 y   1  1  249298  0.010510              2( b1 )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +- 
                                             +-   +-   +-   +-   +-   +-   +-   +-    -   +     -   +-    - 
 x   1  1 1498488 -0.011446    1( b1 )   2( b2 )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +-   +-   +-    -   +-    -   +         +-    - 
 x   1  1 1498489 -0.010542    2( b1 )   2( b2 )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +-   +-   +-    -   +-    -   +         +-    - 
 w   1  129628602 -0.011456    1( b1 )   1( b1 )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +-   +-   +-   +-   +-   +-   +               - 
 w   1  129628603  0.010507    1( b1 )   2( b1 )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +-   +-   +-   +-   +-   +-   +               - 
 w   1  130255620 -0.010427    1( b1 )   2( b2 )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +-   +-   +-   +    +-    -   +         +-    - 
 w   1  130435811 -0.010848    2( b2 )   3( b2 )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +-   +-   +-        +-   +-   +-   +     -      
 w   1  130451473  0.017781    2( b2 )   2( b2 )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +-   +-   +-        +-   +-   +         +-    - 
 w   1  130451475  0.019875    2( b2 )   3( b2 )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +-   +-   +-        +-   +-   +         +-    - 
 w   1  130451476  0.011492    3( b2 )   3( b2 )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +-   +-   +-        +-   +-   +         +-    - 
 w   1  130451482  0.010306    2( b2 )   5( b2 )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +- 
                                             +-   +-   +-   +-   +-   +-        +-   +-   +         +-    - 
 w   1  136589425  0.015256    1( b1 )   2( b1 )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +- 
                                             +-   +-   +-   +-   +-   +-   +-   +-    -   +         +-    - 
 w   1  137397932  0.010931    1( b1 )   1( b1 )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +-   +- 
                                             +-   +-   +-   +-   +-   +-   +-   +-   +-   +         +-    - 
 w   1  137397934  0.010006    2( b1 )   2( b1 )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +-   +- 
                                             +-   +-   +-   +-   +-   +-   +-   +-   +-   +         +-    - 
 w   1  138847321 -0.011484    3( b1 )   1( a2 )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +-   +-   +-   +- 
                                             +-   +-   +-   +-   +-   +-   +-    -   +-   +         +-    - 

 ci coefficient statistics:
           rq > 0.1                3
      0.1> rq > 0.01              47
     0.01> rq > 0.001          27707
    0.001> rq > 0.0001       1721100
   0.0001> rq > 0.00001     10954641
  0.00001> rq > 0.000001    26377123
 0.000001> rq               21753549
           all              60834170
 ========= Executing OUT-OF-CORE method ========
Wait time (non-blocking I/O):    0.000000  seconds
Wait time (non-blocking+overhead I/O):    0.000000  seconds
Wait time (sync mult):    0.407249  seconds


====================================================================================================
Diagonal     counts:  0x:       76653 2x:           0 4x:           0
All internal counts: zz :      792015 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.405669687291   -184.724955361504
     2     2      0.005256736967     -2.393721084805
     3     3      0.033133550731    -15.088396162718
     4     4      0.001157332216     -0.524869117314
     5     5     -0.750403149011    341.697744598065
     6     6     -0.066554397003     30.310316696623
     7     7      0.001819726111     -0.829675419735
     8     8      0.045140420631    -20.559542261083
     9     9     -0.068223734959     31.067309458208
    10    10      0.012391735129     -5.644078981749
    11    11     -0.031843022606     14.498157171674
    12    12     -0.079837027838     36.356390351803
    13    13     -0.137046563926     62.409068316779
    14    14     -0.012644099776      5.759978183998
    15    15     -0.034439719213     15.679129244946
    16    16     -0.009042860419      4.117605852153
    17    17      0.011412623592     -5.196552224415
    18    18      0.004055869813     -1.847462035535
    19    19      0.006083660587     -2.769670112043
    20    20     -0.026669324793     12.146486986595
    21    21     -0.044813712052     20.406410356146
    22    22     -0.006820089237      3.105881376416

 number of reference csfs (nref) is    22.  root number (iroot) is  1.
 c0**2 =   0.77062525  c**2 (all zwalks) =   0.77388583

 pople ci energy extrapolation is computed with 56 correlated electrons.

 eref      =   -455.355357038300   "relaxed" cnot**2         =   0.770625250870
 eci       =   -456.507229993963   deltae = eci - eref       =  -1.151872955662
 eci+dv1   =   -456.771440564198   dv1 = (1-cnot**2)*deltae  =  -0.264210570235
 eci+dv2   =   -456.850082203824   dv2 = dv1 / cnot**2       =  -0.342852209861
 eci+dv3   =   -456.995378401006   dv3 = dv1 / (2*cnot**2-1) =  -0.488148407043
 eci+pople =   -456.961482767134   ( 56e- scaled deltae )    =  -1.606125728834
maximum overlap with reference    1(overlap= 0.87729)

 information on vector: 1 from unit 11 written to unit 48 filename civout              
 passed aftci ... 
 readint2: molcas,dalton2=                     0                     0
 files%faoints=aoints              
lodens (list->root)=  1
sifcfg setup: record length 4096 DP
# d1 elements per record  3272
# d2 elements per record  2730
  The MR-CISD density will be calculated.
 item #                     1 suffix=:.drt1.state1:
 read_civout: repnuc=  -450.146022779782     
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method:  0 last record  1max overlap with ref# 88% root-following 0
 MR-CISD energy:  -456.50722999    -6.36120721
 residuum:     0.00466437
 deltae:     0.00002323
 apxde:     0.00000672

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.87728958    -0.02491929    -0.03518000     0.11470187    -0.04992753    -0.01685682     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.87728958    -0.02491929    -0.03518000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 computing final density
 computing MRCISD density
 densi: densityinfo%a4den=   1.00000000000000     
 ========= Executing OUT-OF-CORE method ========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
skipping task#     8    22
skipping task#    82    26
skipping task#    83    27
skipping task#    84    26
skipping task#    85    27
skipping task#    86    26
skipping task#    88    26
skipping task#    95    27
executing task#   101    11
executing task#   144     1
executing task#   148     1
executing task#   150     1
executing task#   152     1
executing task#   155     1
executing task#   158     1
executing task#   165     2
skipping task#   170     8
executing task#   171     3
skipping task#   186     8
executing task#   187     3
skipping task#   220     9
executing task#   221     4
================================================================================
   DYZ=   39697  DYX=   82724  DYW=   85526
   D0Z=   30379  D0Y=  328974  D0X=   87327  D0W=   93464
  DDZI=   19802 DDYI=  174025 DDXI=   49400 DDWI=   52248
  DDZE=       0 DDYE=   27450 DDXE=    8445 DDWE=    8999
================================================================================
Trace of MO density:    56.000000
   56  correlated and    22  frozen core electrons

          modens reordered block   1

               a1    1        a1    2        a1    3        a1    4        a1    5        a1    6        a1    7        a1    8
  a1    1    4.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  a1    2    0.00000        4.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  a1    3    0.00000        0.00000        4.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  a1    4    0.00000        0.00000        0.00000        4.00000        0.00000        0.00000        0.00000        0.00000    
  a1    5    0.00000        0.00000        0.00000        0.00000        4.00000        0.00000        0.00000        0.00000    
  a1    6    0.00000        0.00000        0.00000        0.00000        0.00000        4.00000        0.00000        0.00000    
  a1    7    0.00000        0.00000        0.00000        0.00000        0.00000        0.00000        4.00000        0.00000    
  a1    8    0.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000        4.00000    

               a1    9        a1   10        a1   11        a1   12        a1   13        a1   14        a1   15        a1   16
  a1    9    1.99095       1.462313E-04   3.970315E-04  -4.165939E-05  -9.716809E-04   8.959574E-04   1.862264E-04  -2.563326E-04
  a1   10   1.462313E-04    1.99075       3.348433E-04   2.848125E-04   2.161561E-04  -2.360963E-06   3.266889E-04   1.231782E-04
  a1   11   3.970315E-04   3.348433E-04    1.99045      -1.559144E-05  -3.895765E-05  -6.491154E-04   4.272854E-04   1.359285E-04
  a1   12  -4.165939E-05   2.848125E-04  -1.559144E-05    1.98869      -1.714386E-04   3.980113E-04  -3.975487E-05  -8.508194E-04
  a1   13  -9.716809E-04   2.161561E-04  -3.895765E-05  -1.714386E-04    1.98733      -6.908076E-04  -7.636893E-04   9.794059E-04
  a1   14   8.959574E-04  -2.360963E-06  -6.491154E-04   3.980113E-04  -6.908076E-04    1.98561      -2.799084E-04  -3.618220E-04
  a1   15   1.862264E-04   3.266889E-04   4.272854E-04  -3.975487E-05  -7.636893E-04  -2.799084E-04    1.98518      -1.539567E-04
  a1   16  -2.563326E-04   1.231782E-04   1.359285E-04  -8.508194E-04   9.794059E-04  -3.618220E-04  -1.539567E-04    1.98329    
  a1   17   9.492916E-05   1.125604E-04  -1.172079E-05   8.852644E-04  -1.230913E-04  -6.972403E-04  -3.378732E-04   7.491679E-05
  a1   18  -2.561291E-04  -2.275322E-04   8.258362E-05  -1.442138E-04   9.423889E-04   7.351743E-06  -5.995276E-05  -1.347533E-03
  a1   19   1.236042E-04   7.099896E-04   1.119936E-04  -5.436363E-04   6.734153E-04  -7.401076E-04   1.231595E-03  -1.506821E-03
  a1   20  -1.293639E-04   1.183464E-03  -8.315462E-04  -2.209664E-04  -4.237033E-04  -1.016762E-04  -1.227686E-03  -1.659967E-04
  a1   21  -5.270649E-04  -2.409005E-04  -9.312006E-04   4.518264E-04   4.935763E-04   2.135545E-04  -2.917970E-04  -1.311746E-03
  a1   22  -6.753155E-05   4.113845E-04   1.421972E-03  -1.218354E-05  -1.095961E-03  -2.733886E-04  -6.105352E-04   5.618256E-04
  a1   23  -1.096372E-04  -2.668773E-04  -1.176473E-03   3.441779E-04  -9.459911E-05   2.081514E-04  -1.840632E-04   1.388773E-03
  a1   24  -1.177274E-04   3.878686E-04   8.892442E-04  -8.476772E-04  -4.503659E-04   2.274005E-04   5.348513E-04  -1.183844E-04
  a1   25   1.418848E-03  -4.478242E-06   1.783481E-04  -6.423276E-04  -3.638249E-04   9.023603E-04   8.817099E-04   8.268800E-04
  a1   26   9.821602E-05   8.695956E-04   7.478973E-04   1.037630E-03   9.747403E-05  -2.006255E-04  -9.185608E-04  -3.116868E-04
  a1   27  -1.274693E-04   7.487192E-05  -1.910138E-04   9.709685E-05   6.749421E-04  -1.743034E-04  -5.510743E-05  -8.598279E-04
  a1   28   1.232725E-04  -1.821088E-04   1.397029E-04  -4.908881E-05  -4.484633E-04  -2.735215E-04   7.538162E-04   6.366345E-04
  a1   29  -1.716636E-04  -1.793884E-04  -1.859180E-03   2.811019E-04   1.301777E-03   9.549484E-04   2.974973E-03  -7.296344E-04
  a1   30  -2.710052E-04   4.711051E-04  -1.416500E-04   1.110731E-03   1.303324E-03  -8.986557E-04   5.245941E-04  -2.047604E-03
  a1   31   3.481655E-04  -3.075718E-04  -6.857125E-04  -5.855495E-04  -2.719859E-04   9.611155E-05   5.369528E-06   1.226275E-03
  a1   32  -2.037376E-04   8.956724E-05  -8.970413E-04   1.265432E-03   2.937753E-04  -1.156788E-03  -2.475817E-04   1.141680E-03
  a1   33   1.778548E-04   4.532133E-04   5.831241E-04   2.797187E-04   6.885921E-04   1.935511E-03  -1.781943E-03   3.815973E-04
  a1   34  -4.795349E-04  -8.929969E-04  -2.209052E-03   1.068640E-03   5.648743E-04  -7.476165E-04   2.308457E-03  -1.539874E-04
  a1   35   4.610616E-04   5.899139E-04   8.043205E-04   7.118696E-04  -3.139801E-04   1.817595E-04  -1.341317E-03   2.843124E-04
  a1   36   1.144850E-04  -1.035702E-04  -6.293895E-04  -1.598864E-03  -6.350628E-04   1.359867E-03   8.835627E-04   1.306513E-03
  a1   37  -4.916646E-04   2.176757E-04   1.320963E-04   3.499572E-04  -5.694931E-04   7.817298E-04   1.482088E-04  -2.212731E-03
  a1   38   4.223185E-04   4.414773E-05   6.402812E-05  -3.114576E-04   4.439783E-04  -1.569022E-04  -1.540215E-04   2.707635E-04
  a1   39  -4.397235E-05   1.830998E-04  -2.868570E-04  -1.248670E-04   1.556662E-04   6.464470E-04   8.585624E-04   7.441562E-04
  a1   40   9.405848E-06   2.227625E-04   9.019707E-06  -1.014315E-04   8.265113E-05  -8.232375E-04  -3.317053E-04   2.463297E-04
  a1   41   9.400925E-07  -2.771755E-04  -4.915894E-05  -3.138060E-04  -3.788744E-04  -6.898650E-05  -6.396816E-05  -6.752900E-05
  a1   42  -1.107067E-04  -2.874680E-04   4.764792E-05   6.997356E-05   9.313023E-05   9.085115E-05   3.435271E-05   2.885697E-04
  a1   43  -5.104708E-04  -1.346975E-04  -3.414240E-05  -1.092611E-04  -2.073934E-04   8.044871E-05   4.637299E-05  -3.307640E-04
  a1   44  -1.661785E-06   6.313026E-04   7.106293E-04   8.269940E-04   2.525247E-04  -4.564567E-04  -5.198991E-04  -6.066192E-04
  a1   45   5.329056E-04   5.362928E-05   3.725818E-04  -6.228146E-04   5.541656E-04  -2.012340E-04  -3.762656E-04  -3.644203E-04
  a1   46   2.521262E-04  -3.831949E-04   1.223336E-04   1.356041E-03   5.152736E-04  -8.585877E-04   1.715695E-04  -1.300115E-03
  a1   47  -3.864941E-04  -6.538067E-04   5.171188E-04   4.449456E-04   5.748917E-04   1.057653E-03   6.672437E-04   1.163202E-03
  a1   48   2.319529E-04   2.725686E-04   1.198346E-04   8.312011E-04   3.022013E-04   1.225578E-04  -1.841357E-04  -2.830429E-06
  a1   49   2.013140E-04  -1.365784E-03   1.216658E-03   1.450747E-03   1.586372E-03   2.085878E-03   2.136003E-03   1.621768E-03
  a1   50  -1.031786E-04   1.730370E-04   8.399276E-04   4.821679E-04   5.676384E-04   7.854110E-04  -1.924029E-04   1.437662E-04
  a1   51  -4.190394E-05   6.117214E-05  -2.692732E-04  -9.667614E-04  -3.891578E-04  -7.260735E-04  -1.376580E-04  -6.022225E-04
  a1   52  -3.798237E-04  -2.959426E-04   2.091933E-04   7.199826E-04   1.289820E-03   9.676386E-05   1.619758E-03  -4.812367E-04
  a1   53  -3.144424E-04   1.471913E-04  -4.820766E-05  -1.024732E-04  -4.221103E-04   2.070933E-04   7.582917E-05  -4.740904E-04
  a1   54   1.224270E-04   2.999313E-04  -8.968176E-04  -6.931821E-04   1.015780E-04  -8.559516E-04   2.108553E-03  -8.171799E-04
  a1   55   1.814624E-04   3.323933E-04   4.111186E-04  -1.952354E-05  -1.888721E-04  -1.051169E-03  -3.166125E-04  -4.757240E-04
  a1   56   3.184384E-04   8.182259E-05   8.695752E-04  -5.168748E-04  -6.233502E-05   8.201667E-04  -2.538693E-03   5.520477E-04
  a1   57  -2.364516E-04   4.418509E-04   5.093825E-04  -6.256240E-04  -1.358320E-04   1.944533E-03  -8.237724E-04  -5.637861E-04
  a1   58  -5.131948E-04  -1.727050E-04  -8.631257E-04  -4.679809E-04  -9.026297E-04  -7.003867E-04   1.167727E-03  -5.534893E-04
  a1   59  -6.422373E-05  -5.430424E-04  -5.191533E-04  -4.766511E-04   1.358900E-04   8.329607E-04   1.020208E-03  -9.510999E-04
  a1   60   5.180712E-04   2.262032E-04  -7.863839E-04   6.421714E-04   1.108597E-03  -7.553409E-04  -2.787086E-04  -8.537893E-04
  a1   61  -4.532892E-04   1.294047E-04   3.017757E-04   1.191126E-04   8.840563E-05   5.276219E-04  -3.320016E-04  -2.252335E-03
  a1   62   5.537624E-04  -5.532376E-04   7.683583E-05  -2.863640E-04   5.788255E-04  -3.651033E-04   8.033457E-04  -5.322352E-05
  a1   63   4.385425E-04  -3.210271E-04  -3.470151E-04  -1.508700E-03   3.404302E-05   3.845734E-04   6.079110E-04   8.327680E-05
  a1   64   4.619123E-04  -8.170163E-05  -6.976632E-04   3.241279E-04  -4.576851E-04  -1.299951E-03  -9.543951E-04   2.575135E-03
  a1   65   1.472415E-04  -4.812931E-04  -5.685065E-04   2.426387E-04  -1.919432E-04  -7.682726E-04   8.610326E-04   1.109202E-03
  a1   66   5.529371E-04   3.536869E-06  -1.021502E-05   8.029201E-04   2.020135E-04  -6.339639E-04  -7.315927E-04   2.357770E-03
  a1   67  -5.548852E-05  -1.032572E-04   2.286486E-04  -1.730154E-04  -8.339299E-04  -4.757467E-04  -1.250653E-04   1.224363E-03
  a1   68   1.117759E-04   1.821245E-05   2.459793E-06   2.029533E-04  -5.466774E-04  -1.285900E-03   7.386372E-05   6.554786E-04
  a1   69  -1.235622E-03   2.070487E-04   3.248621E-04   1.708697E-04  -1.270211E-03   1.308267E-03   3.687416E-05  -6.891065E-04
  a1   70   9.127768E-06   6.930255E-04  -3.890463E-04  -1.138309E-04   4.191493E-04   1.336334E-03  -3.438339E-04   9.795003E-04
  a1   71   8.923802E-04   1.035726E-05  -3.665064E-04  -3.172851E-04   1.965784E-03  -1.532734E-04  -8.415925E-04  -1.378179E-03
  a1   72  -4.687811E-04  -9.857802E-05  -1.145209E-04   3.290604E-05  -1.434926E-03   6.043140E-04  -1.451495E-04   1.153287E-04
  a1   73   5.977293E-04   1.525647E-05   1.323391E-04   1.403347E-04   4.962367E-05   1.015296E-03   8.440322E-05  -1.710198E-04
  a1   74  -1.153941E-03   2.300910E-04   6.230538E-04  -8.595302E-04   5.340543E-04   3.654178E-04   2.617057E-04  -2.541807E-04
  a1   75   4.237778E-04  -6.948736E-04  -1.926270E-04   1.343031E-04  -2.378192E-05  -1.034668E-07  -5.342902E-04   4.541654E-04
  a1   76   3.921830E-05  -1.881024E-04   2.862526E-04   1.547743E-03   6.867832E-04  -8.239638E-04  -1.261139E-04  -1.290623E-03
  a1   77  -1.092765E-03  -1.485481E-03   4.574331E-04   5.889714E-04  -6.768513E-04   5.662947E-04  -1.275460E-03   6.068402E-04
  a1   78  -5.501376E-04  -4.203384E-04   3.765498E-04  -1.753907E-04  -2.327724E-04   3.161759E-04  -1.632245E-04  -4.070780E-04
  a1   79  -6.498085E-04   2.438873E-03  -1.086786E-03   5.188655E-04  -8.380900E-04  -8.343738E-05  -2.696713E-04   8.535640E-04
  a1   80  -1.740580E-04  -5.229172E-04   3.276198E-04   9.565208E-04  -4.655771E-04  -2.831159E-04   3.774439E-04   9.816918E-04
  a1   81   1.909590E-04  -1.585221E-03   6.245517E-04   3.849752E-05  -4.092039E-04  -7.871570E-04  -2.371528E-04  -2.925403E-04
  a1   82   5.500820E-04   3.781768E-04  -3.175595E-04  -4.532314E-04   1.942637E-04  -3.879162E-04  -1.193188E-04  -4.883111E-04

               a1   17        a1   18        a1   19        a1   20        a1   21        a1   22        a1   23        a1   24
  a1    9   9.492916E-05  -2.561291E-04   1.236042E-04  -1.293639E-04  -5.270649E-04  -6.753155E-05  -1.096372E-04  -1.177274E-04
  a1   10   1.125604E-04  -2.275322E-04   7.099896E-04   1.183464E-03  -2.409005E-04   4.113845E-04  -2.668773E-04   3.878686E-04
  a1   11  -1.172079E-05   8.258362E-05   1.119936E-04  -8.315462E-04  -9.312006E-04   1.421972E-03  -1.176473E-03   8.892442E-04
  a1   12   8.852644E-04  -1.442138E-04  -5.436363E-04  -2.209664E-04   4.518264E-04  -1.218354E-05   3.441779E-04  -8.476772E-04
  a1   13  -1.230913E-04   9.423889E-04   6.734153E-04  -4.237033E-04   4.935763E-04  -1.095961E-03  -9.459911E-05  -4.503659E-04
  a1   14  -6.972403E-04   7.351743E-06  -7.401076E-04  -1.016762E-04   2.135545E-04  -2.733886E-04   2.081514E-04   2.274005E-04
  a1   15  -3.378732E-04  -5.995276E-05   1.231595E-03  -1.227686E-03  -2.917970E-04  -6.105352E-04  -1.840632E-04   5.348513E-04
  a1   16   7.491679E-05  -1.347533E-03  -1.506821E-03  -1.659967E-04  -1.311746E-03   5.618256E-04   1.388773E-03  -1.183844E-04
  a1   17    1.98496      -3.421804E-04  -8.819369E-05  -2.130458E-04   3.797649E-04   6.571877E-04   2.399884E-04  -1.891930E-03
  a1   18  -3.421804E-04    1.98349       1.383114E-04  -8.518878E-04   1.282476E-03   6.147709E-04  -1.529618E-03   2.514674E-04
  a1   19  -8.819369E-05   1.383114E-04    1.98213       1.590678E-03  -2.235871E-04  -1.727166E-04   2.640369E-04  -3.354669E-04
  a1   20  -2.130458E-04  -8.518878E-04   1.590678E-03    1.98319       7.811495E-04  -2.008771E-04   2.655815E-04   6.489967E-04
  a1   21   3.797649E-04   1.282476E-03  -2.235871E-04   7.811495E-04   3.255873E-03  -2.218777E-04   3.993948E-04  -3.913502E-04
  a1   22   6.571877E-04   6.147709E-04  -1.727166E-04  -2.008771E-04  -2.218777E-04   3.733922E-03   2.030904E-04   7.070801E-05
  a1   23   2.399884E-04  -1.529618E-03   2.640369E-04   2.655815E-04   3.993948E-04   2.030904E-04   3.829913E-03  -3.285707E-04
  a1   24  -1.891930E-03   2.514674E-04  -3.354669E-04   6.489967E-04  -3.913502E-04   7.070801E-05  -3.285707E-04   3.309911E-03
  a1   25  -9.955656E-04  -1.625477E-03   1.683326E-03  -1.607997E-03   1.353003E-04  -9.956302E-04  -7.142303E-04  -3.730408E-04
  a1   26  -1.418951E-03  -2.156263E-03   9.878364E-04   7.863610E-04   3.367184E-04  -2.665573E-04   7.548299E-04   2.448398E-04
  a1   27  -1.557972E-03   7.508745E-04  -1.099259E-03   1.817109E-04  -4.396659E-04   1.128966E-04  -1.125774E-03   2.942466E-05
  a1   28  -2.602900E-03  -1.180645E-03  -1.542754E-03  -1.461309E-03   1.367683E-04  -1.818439E-04   1.229464E-04  -1.376045E-03
  a1   29   1.262221E-03  -3.934072E-03   5.985665E-03  -8.878449E-04   8.797923E-04  -1.812842E-03   1.789605E-03  -6.700505E-05
  a1   30  -2.481059E-03  -7.441591E-04   3.962525E-04   2.378295E-03   7.516163E-05  -9.801599E-04  -4.885412E-04   1.087840E-03
  a1   31   2.557076E-03  -9.898943E-05   8.970726E-04  -6.070525E-04  -3.847724E-04   8.152155E-05   1.148386E-03   7.049194E-04
  a1   32  -3.735236E-05  -2.010521E-03  -4.369486E-04   1.524808E-03  -4.046164E-04   6.788186E-04   1.501343E-03  -3.214757E-04
  a1   33  -4.649650E-04  -2.105922E-03   2.452986E-03  -4.019971E-04  -2.719613E-04   1.115858E-03  -3.177624E-04  -7.668467E-05
  a1   34  -1.049975E-03  -1.169114E-03  -1.025416E-03  -9.021509E-04   7.791303E-04  -8.159120E-04  -3.761441E-04  -2.158578E-03
  a1   35  -5.949007E-04  -1.767015E-03   3.885783E-04   5.221227E-04   3.945274E-04   3.272421E-04   3.034488E-04  -1.269015E-03
  a1   36   1.688558E-03   2.106040E-03   1.771692E-04   1.980830E-03   1.585185E-04  -3.174002E-04   1.786870E-04   4.694828E-04
  a1   37  -3.910040E-04   9.370972E-04   4.788171E-04   8.189699E-04   1.495114E-03  -6.135026E-04  -1.241363E-03   4.836321E-04
  a1   38  -4.550896E-04   5.022792E-04   9.238157E-04  -3.337527E-04  -6.930429E-04  -1.738010E-03  -1.411624E-03   3.569242E-04
  a1   39   7.129718E-04   9.232889E-04  -4.148912E-04  -2.169223E-03  -3.862223E-04   2.814467E-04  -3.855336E-04   2.343137E-04
  a1   40   4.513498E-04   8.800794E-04   7.025211E-04   1.259886E-03  -1.415196E-03  -7.649140E-04  -9.057775E-05  -7.663697E-04
  a1   41   9.899538E-05  -6.950961E-04   1.607790E-03   1.512769E-03  -2.036732E-04   1.104704E-03  -8.603395E-04   3.891715E-04
  a1   42  -9.468017E-04  -4.159881E-04   1.191148E-03  -7.356871E-04  -1.803369E-03  -1.196912E-04  -9.073747E-05   1.907000E-04
  a1   43   8.346002E-04  -2.172228E-04  -1.189408E-03   8.942195E-04  -2.025375E-04  -2.006135E-04  -6.040345E-04  -4.311521E-05
  a1   44  -1.782752E-03  -1.127975E-03  -8.755306E-04   3.575726E-04  -4.258182E-04  -2.911151E-05   1.300780E-04   8.168602E-04
  a1   45   8.751893E-04   2.718862E-04  -1.828080E-04   3.311489E-04  -1.200211E-04   6.237551E-04  -7.574154E-05  -3.422781E-04
  a1   46  -7.380144E-04  -3.493588E-04  -6.419167E-04   8.099179E-04   1.582232E-04  -4.521159E-04  -1.634932E-04   3.139188E-04
  a1   47   2.046418E-04  -7.770598E-04   1.408001E-03   4.795026E-04   2.303922E-04  -4.021308E-04   7.230634E-04  -2.582007E-04
  a1   48  -1.020537E-03  -2.428455E-04  -6.595064E-04  -9.800854E-04  -1.303792E-04   3.929294E-04  -5.802172E-04  -5.495357E-05
  a1   49   1.195940E-03   1.031346E-03   2.509376E-04   1.766624E-03  -9.769094E-05   2.364130E-04  -3.349887E-04  -6.741242E-05
  a1   50  -1.373665E-04  -1.360019E-03   6.373542E-04  -1.606354E-04  -9.568737E-05   1.350891E-04   4.047089E-05   1.845849E-04
  a1   51   5.755235E-04  -4.507127E-04  -3.728250E-04  -1.266935E-03  -1.278775E-04   1.492665E-04  -2.566708E-04  -3.052597E-04
  a1   52  -9.223325E-04  -7.637918E-04  -1.182965E-03  -1.849745E-04  -1.957389E-04   7.137481E-05  -5.677966E-05  -4.531788E-04
  a1   53  -4.444858E-04  -5.894787E-04   1.267963E-03  -1.354710E-03  -9.298797E-05   3.485575E-04   1.890266E-05   1.073134E-04
  a1   54   1.248072E-04  -1.214652E-03   2.282801E-04   2.538849E-04   4.361814E-05  -2.658687E-04   3.120883E-04   1.493253E-04
  a1   55  -6.974506E-04   8.220431E-04  -4.742300E-04   1.219633E-03   4.941751E-05   1.172746E-04   1.340605E-04   2.095809E-04
  a1   56  -8.833650E-04  -2.063023E-04   1.020840E-03   3.613793E-04  -9.450594E-06  -6.986928E-05   9.861002E-05  -5.831513E-05
  a1   57   1.655970E-03  -5.534867E-04   3.211325E-04  -5.374853E-04  -9.467762E-05  -1.990995E-04   4.415562E-05   2.829189E-05
  a1   58   7.789757E-04  -7.387784E-04  -1.578206E-03   1.508368E-03  -5.726682E-06  -1.551348E-04  -8.282750E-05   3.626951E-05
  a1   59  -1.806587E-04  -5.337545E-04  -4.469253E-04   1.721557E-04   4.192118E-06   1.603696E-05  -2.598401E-04  -7.414630E-05
  a1   60  -5.381030E-04  -1.133045E-03   1.356512E-03  -7.642455E-04   5.591500E-05  -1.935560E-05  -1.993748E-04  -7.193209E-06
  a1   61  -1.171869E-03  -1.630743E-03  -7.681928E-04   1.121645E-04  -2.437025E-06  -9.529858E-05   6.546750E-05   3.238037E-04
  a1   62  -3.804656E-04  -1.759307E-03  -2.484769E-03   7.685509E-04  -7.555632E-05  -5.378994E-05  -9.485138E-05   5.334643E-05
  a1   63  -5.144547E-04   1.050067E-03   1.039163E-03   9.867818E-05  -7.677939E-05   1.183806E-05   7.554174E-05   3.959733E-04
  a1   64   7.372414E-04  -7.280815E-04   1.025736E-03  -3.548762E-04  -1.783444E-04   9.788940E-05   2.748357E-04   1.779574E-04
  a1   65  -9.954951E-04   1.185185E-03   3.793956E-04   2.805165E-04  -3.279716E-05   7.406054E-05  -5.933986E-06   2.740442E-05
  a1   66  -6.527517E-04  -4.007357E-04   8.382585E-04  -8.100593E-04  -5.306055E-05   1.068462E-04   1.783439E-05  -2.214159E-05
  a1   67   3.087033E-04   2.418899E-05   8.755338E-04  -9.292867E-05   3.316870E-05   3.479752E-05   1.499132E-04  -9.695212E-05
  a1   68  -3.728498E-04   1.889900E-04   1.997041E-04  -2.085435E-05   1.441511E-05   3.705367E-05   1.968406E-04   4.090805E-05
  a1   69   2.337938E-04  -5.279230E-04  -1.944023E-04  -9.261844E-05   1.316917E-04  -7.244987E-05   4.130115E-05  -1.560179E-04
  a1   70  -1.516733E-03  -4.032509E-04  -7.206785E-04   1.399913E-04   4.963598E-06   1.673609E-04  -1.704261E-04  -2.348103E-04
  a1   71   1.224556E-03   3.931717E-04  -1.378578E-03   7.313214E-04  -1.983759E-04   1.959574E-04  -8.142899E-05   3.848307E-04
  a1   72   9.252497E-04   3.951912E-04  -9.282781E-04   7.663707E-04   2.610632E-04   1.925292E-04  -1.920563E-05   4.230337E-05
  a1   73   6.988930E-04  -9.374432E-04   1.502521E-03  -1.086646E-03   2.827337E-04  -3.700952E-05   1.445821E-04   1.293423E-05
  a1   74   8.370634E-04  -1.140187E-03  -3.393257E-04  -1.193089E-04  -1.415281E-04  -1.570606E-04   2.193452E-04  -2.904665E-04
  a1   75  -1.961132E-04  -7.791176E-04   2.681031E-04  -2.408821E-04  -4.933512E-05   3.768760E-04   2.242917E-04  -1.006323E-04
  a1   76  -1.180197E-03  -1.256203E-03  -6.888982E-04  -1.564567E-04   4.966045E-05   1.857101E-05   5.037671E-06  -8.434339E-06
  a1   77  -1.493434E-04   1.757210E-04   2.102428E-03  -6.183754E-04  -1.078165E-04   3.052840E-05  -1.458373E-04  -5.166945E-06
  a1   78  -6.711297E-04  -5.412547E-04   3.549722E-05  -1.183448E-04   4.960682E-05  -1.163486E-04  -1.828728E-04  -6.138840E-05
  a1   79  -4.250840E-04   5.241900E-04   4.472775E-04  -1.162841E-03  -2.261434E-05   3.752256E-05   2.019016E-05  -6.804598E-06
  a1   80  -3.803691E-04   6.216611E-04   1.608299E-04   5.398457E-04   5.785324E-05  -4.076402E-05   1.577677E-04   3.767866E-05
  a1   81  -1.708135E-04  -8.163640E-05   1.444290E-04   5.781061E-04  -7.487455E-06   6.633139E-05  -6.989063E-05  -1.369161E-04
  a1   82   4.219307E-04   9.560186E-05  -3.424375E-04  -2.015872E-04   2.723695E-05   3.978392E-05  -4.813770E-05  -1.009869E-04

               a1   25        a1   26        a1   27        a1   28        a1   29        a1   30        a1   31        a1   32
  a1    9   1.418848E-03   9.821602E-05  -1.274693E-04   1.232725E-04  -1.716636E-04  -2.710052E-04   3.481655E-04  -2.037376E-04
  a1   10  -4.478242E-06   8.695956E-04   7.487192E-05  -1.821088E-04  -1.793884E-04   4.711051E-04  -3.075718E-04   8.956724E-05
  a1   11   1.783481E-04   7.478973E-04  -1.910138E-04   1.397029E-04  -1.859180E-03  -1.416500E-04  -6.857125E-04  -8.970413E-04
  a1   12  -6.423276E-04   1.037630E-03   9.709685E-05  -4.908881E-05   2.811019E-04   1.110731E-03  -5.855495E-04   1.265432E-03
  a1   13  -3.638249E-04   9.747403E-05   6.749421E-04  -4.484633E-04   1.301777E-03   1.303324E-03  -2.719859E-04   2.937753E-04
  a1   14   9.023603E-04  -2.006255E-04  -1.743034E-04  -2.735215E-04   9.549484E-04  -8.986557E-04   9.611155E-05  -1.156788E-03
  a1   15   8.817099E-04  -9.185608E-04  -5.510743E-05   7.538162E-04   2.974973E-03   5.245941E-04   5.369528E-06  -2.475817E-04
  a1   16   8.268800E-04  -3.116868E-04  -8.598279E-04   6.366345E-04  -7.296344E-04  -2.047604E-03   1.226275E-03   1.141680E-03
  a1   17  -9.955656E-04  -1.418951E-03  -1.557972E-03  -2.602900E-03   1.262221E-03  -2.481059E-03   2.557076E-03  -3.735236E-05
  a1   18  -1.625477E-03  -2.156263E-03   7.508745E-04  -1.180645E-03  -3.934072E-03  -7.441591E-04  -9.898943E-05  -2.010521E-03
  a1   19   1.683326E-03   9.878364E-04  -1.099259E-03  -1.542754E-03   5.985665E-03   3.962525E-04   8.970726E-04  -4.369486E-04
  a1   20  -1.607997E-03   7.863610E-04   1.817109E-04  -1.461309E-03  -8.878449E-04   2.378295E-03  -6.070525E-04   1.524808E-03
  a1   21   1.353003E-04   3.367184E-04  -4.396659E-04   1.367683E-04   8.797923E-04   7.516163E-05  -3.847724E-04  -4.046164E-04
  a1   22  -9.956302E-04  -2.665573E-04   1.128966E-04  -1.818439E-04  -1.812842E-03  -9.801599E-04   8.152155E-05   6.788186E-04
  a1   23  -7.142303E-04   7.548299E-04  -1.125774E-03   1.229464E-04   1.789605E-03  -4.885412E-04   1.148386E-03   1.501343E-03
  a1   24  -3.730408E-04   2.448398E-04   2.942466E-05  -1.376045E-03  -6.700505E-05   1.087840E-03   7.049194E-04  -3.214757E-04
  a1   25   6.388788E-03   5.536174E-05  -6.373375E-05   1.058559E-03   3.324088E-04   2.935775E-04   2.166051E-03  -2.756054E-03
  a1   26   5.536174E-05   5.440193E-03  -1.300535E-03  -3.148464E-05   1.318197E-03   8.708579E-04  -5.308709E-04   1.041401E-04
  a1   27  -6.373375E-05  -1.300535E-03   5.779675E-03  -4.938743E-04  -5.123974E-04   9.835898E-04  -9.344213E-04   1.336413E-03
  a1   28   1.058559E-03  -3.148464E-05  -4.938743E-04   7.846787E-03  -8.629280E-04  -1.827012E-03   1.359101E-03   6.491574E-04
  a1   29   3.324088E-04   1.318197E-03  -5.123974E-04  -8.629280E-04   7.029796E-03   6.149665E-04   4.461350E-04   8.344366E-04
  a1   30   2.935775E-04   8.708579E-04   9.835898E-04  -1.827012E-03   6.149665E-04   5.450016E-03  -1.258849E-03  -2.503627E-04
  a1   31   2.166051E-03  -5.308709E-04  -9.344213E-04   1.359101E-03   4.461350E-04  -1.258849E-03   5.800282E-03  -2.561439E-04
  a1   32  -2.756054E-03   1.041401E-04   1.336413E-03   6.491574E-04   8.344366E-04  -2.503627E-04  -2.561439E-04   6.106854E-03
  a1   33   5.138368E-04   2.021507E-03  -6.674591E-04  -4.851258E-04   3.963699E-04  -3.654163E-05   2.267381E-04  -3.155942E-04
  a1   34   6.835080E-04  -7.962591E-04   1.006817E-03   4.166511E-04   3.271940E-04  -7.443516E-05  -4.588172E-04  -1.342269E-04
  a1   35   2.067778E-03   1.145726E-03   4.871783E-04  -1.298275E-04   7.747272E-04   2.854928E-04   2.054165E-04  -1.087374E-04
  a1   36   5.736275E-04  -1.454951E-03   3.637696E-04  -1.375322E-03   3.596288E-04  -1.108882E-03   7.006338E-04  -4.941129E-04
  a1   37   8.965468E-04   4.719726E-04   4.987666E-04   8.816795E-04   2.505427E-04   4.691303E-04   6.476714E-04   5.613547E-05
  a1   38  -7.937729E-04   4.880445E-04  -8.276817E-04   1.007315E-03   7.426582E-04  -1.157141E-03  -2.769285E-04   6.226878E-05
  a1   39   2.222608E-05  -4.371280E-04   7.308896E-05  -2.366581E-04   1.181894E-03   9.454829E-05   3.408771E-04   5.975357E-04
  a1   40   1.516383E-04   8.156024E-04   6.592771E-04   4.457225E-04   5.459580E-05   6.919459E-04   8.622325E-04   5.778207E-04
  a1   41  -6.069653E-05   7.988498E-05   1.019219E-03   1.465696E-04   1.285263E-03  -6.106019E-04   4.839175E-04   7.985577E-04
  a1   42   4.634326E-04  -5.695054E-04   3.239288E-04  -6.617034E-04   3.552260E-05  -2.786104E-05   1.175887E-05  -3.462310E-05
  a1   43   9.460310E-04  -6.508353E-04   7.131158E-04  -1.658220E-03  -6.870045E-04   8.225441E-04   2.217986E-05  -2.025068E-04
  a1   44   5.757777E-04   1.213662E-03   4.346305E-05   7.192829E-04  -4.866350E-04   1.307059E-03  -3.404548E-04   4.233968E-04
  a1   45  -3.543350E-04   7.017283E-04  -8.645331E-04  -6.479653E-04  -2.270024E-04  -1.295277E-04  -4.986428E-05  -6.861533E-04
  a1   46   2.635750E-04   7.919230E-06   9.106662E-04   1.513131E-04   1.190880E-04   8.854061E-04   1.203314E-04   6.511614E-04
  a1   47  -2.949417E-04   3.023141E-04  -6.959321E-04  -2.210822E-06   1.042918E-03  -4.250550E-04   2.780066E-04  -1.859385E-04
  a1   48  -6.469724E-05  -3.748935E-04  -4.510754E-04  -7.971235E-05  -6.550053E-04  -1.713408E-04  -4.299829E-04  -2.355429E-04
  a1   49   8.489236E-05  -5.674942E-04  -3.113150E-04   1.059692E-05  -5.095077E-04  -6.981781E-04   8.900599E-05  -7.855495E-04
  a1   50   4.162179E-04   9.418554E-04  -4.527857E-04   3.483940E-04  -2.092434E-04  -2.918798E-04   8.668130E-05  -1.100421E-04
  a1   51   2.979898E-04  -2.750087E-04   2.795100E-07   2.070334E-04  -2.058244E-04  -3.190333E-04  -2.336319E-04   1.803366E-04
  a1   52   1.324599E-05  -8.006458E-04   5.108364E-04   1.274882E-03  -1.423215E-04  -1.418083E-04  -4.062057E-04  -2.965166E-04
  a1   53  -3.705735E-04   4.346941E-04  -1.839193E-04   3.746402E-04   8.533806E-05   7.308340E-05   2.467790E-04  -5.844171E-04
  a1   54   6.566178E-05   4.821372E-04   7.625291E-05  -4.737548E-04   1.209227E-03   3.645657E-04  -5.603298E-04   2.440535E-04
  a1   55  -5.073274E-04   2.420574E-04   8.018858E-04  -2.084576E-04  -1.294023E-04   1.882747E-04   3.765886E-05  -1.581325E-04
  a1   56  -9.584101E-05   4.100171E-04   8.379854E-04  -1.604329E-04  -2.121415E-04  -8.366371E-04  -6.893072E-04  -1.172647E-04
  a1   57   1.756015E-04  -2.515745E-04  -1.039555E-04   1.730113E-04   3.619332E-04   8.761880E-05   6.030992E-04  -4.688295E-04
  a1   58   9.194271E-05   3.495140E-04   5.035431E-04  -1.890234E-05  -5.136161E-04  -7.582279E-04   3.323489E-04  -4.363498E-04
  a1   59   3.299159E-05   1.103452E-04   1.955465E-04  -3.678042E-04  -8.241937E-05   3.805976E-04  -5.662077E-04  -3.870999E-04
  a1   60  -3.093220E-04   2.252702E-04   6.932710E-04  -4.706261E-04   5.902301E-05   4.998948E-05  -7.774511E-05   1.419741E-04
  a1   61   4.500399E-04   5.989231E-05   4.922936E-05  -2.675611E-04   1.344735E-05  -1.737878E-04   4.606552E-04  -4.411326E-04
  a1   62  -4.093293E-04  -2.086834E-04  -9.926091E-05  -3.221049E-04  -1.792786E-04   5.219588E-04  -2.152645E-04   2.184240E-04
  a1   63   1.733437E-04  -2.305852E-04   2.682533E-04   2.981561E-04  -1.240374E-04  -3.038846E-04   5.836139E-04  -2.936185E-04
  a1   64  -2.362831E-04   6.087623E-06  -2.504145E-05   5.728631E-05  -5.157468E-05  -6.313897E-04   5.195419E-04   4.094565E-04
  a1   65  -1.129642E-04  -2.970756E-04   7.773992E-05   6.941260E-05   6.111633E-05   2.814164E-05  -5.549764E-05   2.805557E-04
  a1   66  -2.973835E-04  -1.021826E-05  -7.841286E-05  -1.917967E-05  -1.226438E-04   1.289468E-04  -1.776382E-04   3.711743E-04
  a1   67  -6.179126E-05   1.182700E-04  -1.202538E-04   1.405104E-04   9.598800E-05  -1.837525E-04   5.130864E-05   7.070607E-05
  a1   68  -2.144633E-04   2.816114E-05   4.211500E-05  -2.894798E-05   9.999640E-05  -1.491679E-04   7.737163E-05   2.630933E-04
  a1   69   5.970412E-04   2.696594E-05  -6.498123E-05   7.483999E-05   1.554086E-04  -1.945847E-04   1.375928E-05  -2.639533E-04
  a1   70  -7.272199E-05   1.676750E-04   7.825572E-05   6.073123E-05  -2.299394E-04  -7.676334E-05  -2.608004E-04   1.126298E-05
  a1   71  -2.441029E-04  -1.537368E-04   8.758963E-05  -3.805382E-04  -4.205614E-04   2.948117E-04   2.411249E-04   2.312962E-05
  a1   72   1.420945E-04  -1.910292E-05  -1.051394E-06   4.258055E-05  -5.974005E-05  -1.394517E-04   2.272941E-04   3.361823E-05
  a1   73  -1.255121E-04  -1.028783E-04  -6.583559E-05   1.053275E-05   1.851800E-04  -3.134422E-05  -1.760719E-05   7.109130E-05
  a1   74   3.721923E-04   2.774339E-05  -1.841712E-05   7.992499E-05   1.298599E-04   2.942767E-05   2.503845E-04  -1.327407E-04
  a1   75  -4.461411E-05  -2.036821E-05   4.815579E-05  -1.483423E-05  -1.044168E-04  -1.256337E-04   6.584913E-05   2.310230E-04
  a1   76  -2.276634E-05   2.083588E-05   4.652264E-05   2.400174E-04  -5.527901E-05   1.556228E-04   6.841097E-05   1.775878E-04
  a1   77   2.804455E-04  -4.307574E-05   9.500642E-06   9.217167E-05  -3.342801E-06  -7.534474E-05   2.685993E-04   1.225345E-05
  a1   78   1.327060E-04   7.582376E-06   5.781003E-05   1.599110E-04  -2.994572E-06   3.013447E-06  -7.613109E-05  -1.567786E-04
  a1   79   1.583027E-04  -1.745961E-05   1.903214E-05  -3.012166E-05   3.998481E-05  -8.082057E-05   2.164616E-04   3.088727E-05
  a1   80   2.150735E-05   1.508879E-04  -1.500291E-05  -1.765250E-04   5.048815E-05   1.658511E-04   2.913717E-06   1.642475E-04
  a1   81  -3.380246E-05  -4.252712E-04  -1.512220E-04   2.175505E-04  -5.001851E-04  -1.995710E-04  -5.451338E-05  -1.350348E-04
  a1   82  -1.042352E-04   1.375576E-04   1.004502E-04  -1.447169E-04   1.031098E-05   6.023797E-05  -1.081934E-04   6.632623E-05

               a1   33        a1   34        a1   35        a1   36        a1   37        a1   38        a1   39        a1   40
  a1    9   1.778548E-04  -4.795349E-04   4.610616E-04   1.144850E-04  -4.916646E-04   4.223185E-04  -4.397235E-05   9.405848E-06
  a1   10   4.532133E-04  -8.929969E-04   5.899139E-04  -1.035702E-04   2.176757E-04   4.414773E-05   1.830998E-04   2.227625E-04
  a1   11   5.831241E-04  -2.209052E-03   8.043205E-04  -6.293895E-04   1.320963E-04   6.402812E-05  -2.868570E-04   9.019707E-06
  a1   12   2.797187E-04   1.068640E-03   7.118696E-04  -1.598864E-03   3.499572E-04  -3.114576E-04  -1.248670E-04  -1.014315E-04
  a1   13   6.885921E-04   5.648743E-04  -3.139801E-04  -6.350628E-04  -5.694931E-04   4.439783E-04   1.556662E-04   8.265113E-05
  a1   14   1.935511E-03  -7.476165E-04   1.817595E-04   1.359867E-03   7.817298E-04  -1.569022E-04   6.464470E-04  -8.232375E-04
  a1   15  -1.781943E-03   2.308457E-03  -1.341317E-03   8.835627E-04   1.482088E-04  -1.540215E-04   8.585624E-04  -3.317053E-04
  a1   16   3.815973E-04  -1.539874E-04   2.843124E-04   1.306513E-03  -2.212731E-03   2.707635E-04   7.441562E-04   2.463297E-04
  a1   17  -4.649650E-04  -1.049975E-03  -5.949007E-04   1.688558E-03  -3.910040E-04  -4.550896E-04   7.129718E-04   4.513498E-04
  a1   18  -2.105922E-03  -1.169114E-03  -1.767015E-03   2.106040E-03   9.370972E-04   5.022792E-04   9.232889E-04   8.800794E-04
  a1   19   2.452986E-03  -1.025416E-03   3.885783E-04   1.771692E-04   4.788171E-04   9.238157E-04  -4.148912E-04   7.025211E-04
  a1   20  -4.019971E-04  -9.021509E-04   5.221227E-04   1.980830E-03   8.189699E-04  -3.337527E-04  -2.169223E-03   1.259886E-03
  a1   21  -2.719613E-04   7.791303E-04   3.945274E-04   1.585185E-04   1.495114E-03  -6.930429E-04  -3.862223E-04  -1.415196E-03
  a1   22   1.115858E-03  -8.159120E-04   3.272421E-04  -3.174002E-04  -6.135026E-04  -1.738010E-03   2.814467E-04  -7.649140E-04
  a1   23  -3.177624E-04  -3.761441E-04   3.034488E-04   1.786870E-04  -1.241363E-03  -1.411624E-03  -3.855336E-04  -9.057775E-05
  a1   24  -7.668467E-05  -2.158578E-03  -1.269015E-03   4.694828E-04   4.836321E-04   3.569242E-04   2.343137E-04  -7.663697E-04
  a1   25   5.138368E-04   6.835080E-04   2.067778E-03   5.736275E-04   8.965468E-04  -7.937729E-04   2.222608E-05   1.516383E-04
  a1   26   2.021507E-03  -7.962591E-04   1.145726E-03  -1.454951E-03   4.719726E-04   4.880445E-04  -4.371280E-04   8.156024E-04
  a1   27  -6.674591E-04   1.006817E-03   4.871783E-04   3.637696E-04   4.987666E-04  -8.276817E-04   7.308896E-05   6.592771E-04
  a1   28  -4.851258E-04   4.166511E-04  -1.298275E-04  -1.375322E-03   8.816795E-04   1.007315E-03  -2.366581E-04   4.457225E-04
  a1   29   3.963699E-04   3.271940E-04   7.747272E-04   3.596288E-04   2.505427E-04   7.426582E-04   1.181894E-03   5.459580E-05
  a1   30  -3.654163E-05  -7.443516E-05   2.854928E-04  -1.108882E-03   4.691303E-04  -1.157141E-03   9.454829E-05   6.919459E-04
  a1   31   2.267381E-04  -4.588172E-04   2.054165E-04   7.006338E-04   6.476714E-04  -2.769285E-04   3.408771E-04   8.622325E-04
  a1   32  -3.155942E-04  -1.342269E-04  -1.087374E-04  -4.941129E-04   5.613547E-05   6.226878E-05   5.975357E-04   5.778207E-04
  a1   33   5.345807E-03  -4.229441E-04   8.137535E-04  -3.390403E-04  -4.792836E-05   5.228145E-04   4.243539E-04  -2.757237E-04
  a1   34  -4.229441E-04   5.359410E-03   5.370952E-04   2.631908E-04  -4.287382E-04  -2.601422E-04  -7.007836E-06  -6.679301E-04
  a1   35   8.137535E-04   5.370952E-04   5.425878E-03   1.118698E-05   2.236351E-04  -3.216232E-04   2.199202E-04  -2.595354E-04
  a1   36  -3.390403E-04   2.631908E-04   1.118698E-05   4.969026E-03   3.639674E-05   9.407102E-06   4.603337E-04   3.412632E-04
  a1   37  -4.792836E-05  -4.287382E-04   2.236351E-04   3.639674E-05   4.256297E-03  -2.978500E-04   1.727844E-04   2.021211E-04
  a1   38   5.228145E-04  -2.601422E-04  -3.216232E-04   9.407102E-06  -2.978500E-04   4.535087E-03   2.731260E-04   1.554144E-04
  a1   39   4.243539E-04  -7.007836E-06   2.199202E-04   4.603337E-04   1.727844E-04   2.731260E-04   3.575713E-03   5.339155E-05
  a1   40  -2.757237E-04  -6.679301E-04  -2.595354E-04   3.412632E-04   2.021211E-04   1.554144E-04   5.339155E-05   4.290811E-03
  a1   41   6.073961E-04  -3.277120E-06   2.743801E-04   5.195250E-04   6.082629E-04   4.892900E-04   8.370052E-05   2.461823E-04
  a1   42   2.177727E-04   4.330602E-04   2.881581E-04  -4.086855E-04  -6.672875E-04  -9.009749E-05   8.981008E-05  -3.791917E-04
  a1   43  -5.215491E-04   5.081857E-04   3.353601E-04   1.684494E-04  -2.015361E-04  -3.240598E-04  -2.104051E-04  -2.636219E-04
  a1   44   1.910525E-04  -2.527707E-04   3.485399E-04  -6.189033E-04   9.964057E-05  -2.087866E-04  -6.338307E-04  -1.344482E-04
  a1   45   1.982762E-04  -8.403658E-04  -6.801457E-04   1.768600E-04  -1.095915E-04  -6.491222E-04  -2.030291E-04   7.573516E-04
  a1   46  -6.089061E-04  -1.149743E-04  -7.792447E-05  -5.243669E-04   5.822931E-04  -3.579372E-04   6.947502E-05   1.965611E-04
  a1   47   4.709028E-04   1.137559E-04   5.661845E-05  -2.324884E-04  -2.992060E-04   3.744603E-04   1.150832E-04  -5.959321E-04
  a1   48  -6.378280E-05   2.825131E-04   1.610833E-04  -4.771142E-04  -3.369717E-04   1.859581E-04   1.982213E-05  -9.527507E-04
  a1   49  -6.425894E-04   2.589675E-04  -5.674049E-04  -1.012427E-03  -3.674873E-04  -1.527767E-04   7.793501E-04  -6.495218E-04
  a1   50   7.497216E-06  -2.224416E-04   6.010061E-05  -4.210867E-04   7.918698E-05   1.962303E-04   2.609731E-04  -1.748585E-04
  a1   51  -3.084810E-06   4.051181E-05  -1.053094E-04   2.816915E-04   2.515207E-04  -3.924375E-06   1.124934E-04   4.690939E-04
  a1   52  -4.190959E-04   3.147417E-04  -4.421358E-04  -6.418465E-04  -1.177050E-04  -3.742387E-04   3.762396E-04  -1.937301E-04
  a1   53   4.039575E-04   1.197990E-04   1.622201E-04  -8.524839E-05  -2.112017E-04  -6.341721E-05   1.555308E-04   1.534652E-05
  a1   54  -2.181700E-04   2.011534E-04  -4.467550E-04   3.094390E-04   2.141065E-06  -2.723051E-04  -3.898457E-04   1.722793E-04
  a1   55  -5.346201E-04   1.578853E-04   5.254981E-04   1.664281E-04  -2.606846E-04  -8.373402E-05  -2.442488E-04  -2.668863E-05
  a1   56   2.668672E-04  -3.363462E-04   2.480192E-04  -8.412110E-06  -1.390139E-04   1.745875E-04  -1.708161E-04  -1.816481E-04
  a1   57   9.566548E-05  -3.417424E-04   4.763896E-04   3.551278E-04   3.178667E-04   8.167801E-05  -1.498321E-04   1.088249E-04
  a1   58  -5.813810E-04   7.901925E-04  -3.480610E-04   1.589994E-04   7.957359E-05   4.974753E-06  -6.794771E-05  -1.596045E-04
  a1   59   4.153346E-04   6.283092E-04   2.336230E-05  -1.517084E-04  -6.624782E-05  -3.285862E-04   3.087083E-05  -2.125722E-04
  a1   60   1.524060E-04   2.813647E-04  -2.221521E-04  -6.436332E-04  -4.078764E-05   1.074488E-04  -2.946094E-04  -5.636235E-05
  a1   61  -8.027179E-06  -3.620648E-04  -6.482863E-05  -7.208953E-05   2.828518E-04  -1.556335E-04  -1.460747E-04   9.927765E-05
  a1   62  -2.866544E-04   3.259071E-04   2.692065E-04   2.645883E-04   1.291057E-04   1.185580E-05   1.557857E-04   9.563076E-05
  a1   63  -1.631888E-04  -2.529199E-04  -5.683896E-04   1.249624E-04  -2.840989E-04   1.656185E-04   2.729746E-06  -1.096526E-04
  a1   64  -1.880081E-04  -8.479801E-05  -2.579912E-04  -1.065720E-04  -1.680731E-04  -1.522559E-04  -1.127432E-04   1.509105E-05
  a1   65  -2.648597E-04   2.208494E-04  -2.100242E-04   3.327197E-04  -2.733047E-04   2.500944E-05   2.684806E-04   2.600707E-06
  a1   66   1.466693E-04  -1.109310E-04   9.719051E-05  -7.974118E-05  -4.589677E-04   2.129859E-04   2.174437E-05  -2.120203E-04
  a1   67  -6.338600E-05   5.118745E-05   9.836873E-05  -9.391098E-05  -9.567609E-05   1.333194E-04  -6.997091E-05  -6.153835E-06
  a1   68  -3.155270E-04   5.299845E-05   8.421399E-05   1.315946E-06   1.482911E-05   8.353394E-05  -3.132060E-05   1.641413E-04
  a1   69   3.566162E-05   1.321634E-04   3.232744E-04   1.278846E-04   2.610672E-04  -1.495157E-04  -4.102481E-05  -2.399839E-04
  a1   70   4.394825E-04   2.403957E-04   1.704657E-04  -1.957471E-04   8.515994E-05   2.635906E-05  -1.857127E-04   1.235114E-04
  a1   71  -7.544317E-06  -3.339567E-04  -3.444405E-04  -6.890477E-05  -5.062917E-06  -1.647599E-04   1.056857E-05   4.585792E-05
  a1   72   9.927034E-05  -1.039245E-05   1.520335E-04   1.996737E-04   2.657180E-04  -3.572698E-04   8.876312E-05  -3.964695E-04
  a1   73  -2.479468E-04  -5.189216E-05  -1.139057E-06   4.475405E-05   1.664416E-04  -1.211417E-04  -8.758340E-05  -3.567648E-04
  a1   74  -6.383794E-05   1.563815E-04   3.174681E-04   2.901872E-05  -4.975848E-04  -2.039254E-04  -7.373015E-05   4.075537E-04
  a1   75   2.782542E-04   5.463117E-05   1.118562E-04  -4.487634E-05  -2.259454E-04  -4.803523E-04   1.257098E-04  -1.058226E-04
  a1   76  -1.085104E-04  -3.488704E-05   3.555969E-05  -3.509641E-04   2.246300E-04  -1.607942E-04   5.473482E-05   1.485099E-04
  a1   77   2.365644E-04   3.918718E-05   1.597590E-04   1.167546E-04   1.007097E-04   1.840431E-04   4.793238E-04   3.991914E-05
  a1   78   6.570309E-05   2.418252E-04   1.567287E-04  -4.040201E-05  -5.749751E-06   2.546485E-04   1.119646E-04  -7.311874E-05
  a1   79  -4.059282E-05   4.106246E-05   1.458825E-04  -1.623516E-04   1.973271E-05   4.699770E-05  -5.908376E-04  -9.973674E-05
  a1   80  -4.838868E-05   9.855127E-06   1.891961E-04  -2.501324E-07  -2.446432E-05  -3.359381E-05   2.318093E-04  -1.084306E-05
  a1   81  -3.122898E-04   1.881054E-04  -2.139394E-04   3.174579E-05  -1.754456E-04  -3.145355E-05  -6.310648E-05  -2.040719E-04
  a1   82   3.697584E-05  -9.740700E-06   1.707450E-06   1.096391E-05   3.115588E-05  -1.878204E-05  -5.076905E-05   2.030323E-04

               a1   41        a1   42        a1   43        a1   44        a1   45        a1   46        a1   47        a1   48
  a1    9   9.400925E-07  -1.107067E-04  -5.104708E-04  -1.661785E-06   5.329056E-04   2.521262E-04  -3.864941E-04   2.319529E-04
  a1   10  -2.771755E-04  -2.874680E-04  -1.346975E-04   6.313026E-04   5.362928E-05  -3.831949E-04  -6.538067E-04   2.725686E-04
  a1   11  -4.915894E-05   4.764792E-05  -3.414240E-05   7.106293E-04   3.725818E-04   1.223336E-04   5.171188E-04   1.198346E-04
  a1   12  -3.138060E-04   6.997356E-05  -1.092611E-04   8.269940E-04  -6.228146E-04   1.356041E-03   4.449456E-04   8.312011E-04
  a1   13  -3.788744E-04   9.313023E-05  -2.073934E-04   2.525247E-04   5.541656E-04   5.152736E-04   5.748917E-04   3.022013E-04
  a1   14  -6.898650E-05   9.085115E-05   8.044871E-05  -4.564567E-04  -2.012340E-04  -8.585877E-04   1.057653E-03   1.225578E-04
  a1   15  -6.396816E-05   3.435271E-05   4.637299E-05  -5.198991E-04  -3.762656E-04   1.715695E-04   6.672437E-04  -1.841357E-04
  a1   16  -6.752900E-05   2.885697E-04  -3.307640E-04  -6.066192E-04  -3.644203E-04  -1.300115E-03   1.163202E-03  -2.830429E-06
  a1   17   9.899538E-05  -9.468017E-04   8.346002E-04  -1.782752E-03   8.751893E-04  -7.380144E-04   2.046418E-04  -1.020537E-03
  a1   18  -6.950961E-04  -4.159881E-04  -2.172228E-04  -1.127975E-03   2.718862E-04  -3.493588E-04  -7.770598E-04  -2.428455E-04
  a1   19   1.607790E-03   1.191148E-03  -1.189408E-03  -8.755306E-04  -1.828080E-04  -6.419167E-04   1.408001E-03  -6.595064E-04
  a1   20   1.512769E-03  -7.356871E-04   8.942195E-04   3.575726E-04   3.311489E-04   8.099179E-04   4.795026E-04  -9.800854E-04
  a1   21  -2.036732E-04  -1.803369E-03  -2.025375E-04  -4.258182E-04  -1.200211E-04   1.582232E-04   2.303922E-04  -1.303792E-04
  a1   22   1.104704E-03  -1.196912E-04  -2.006135E-04  -2.911151E-05   6.237551E-04  -4.521159E-04  -4.021308E-04   3.929294E-04
  a1   23  -8.603395E-04  -9.073747E-05  -6.040345E-04   1.300780E-04  -7.574154E-05  -1.634932E-04   7.230634E-04  -5.802172E-04
  a1   24   3.891715E-04   1.907000E-04  -4.311521E-05   8.168602E-04  -3.422781E-04   3.139188E-04  -2.582007E-04  -5.495357E-05
  a1   25  -6.069653E-05   4.634326E-04   9.460310E-04   5.757777E-04  -3.543350E-04   2.635750E-04  -2.949417E-04  -6.469724E-05
  a1   26   7.988498E-05  -5.695054E-04  -6.508353E-04   1.213662E-03   7.017283E-04   7.919230E-06   3.023141E-04  -3.748935E-04
  a1   27   1.019219E-03   3.239288E-04   7.131158E-04   4.346305E-05  -8.645331E-04   9.106662E-04  -6.959321E-04  -4.510754E-04
  a1   28   1.465696E-04  -6.617034E-04  -1.658220E-03   7.192829E-04  -6.479653E-04   1.513131E-04  -2.210822E-06  -7.971235E-05
  a1   29   1.285263E-03   3.552260E-05  -6.870045E-04  -4.866350E-04  -2.270024E-04   1.190880E-04   1.042918E-03  -6.550053E-04
  a1   30  -6.106019E-04  -2.786104E-05   8.225441E-04   1.307059E-03  -1.295277E-04   8.854061E-04  -4.250550E-04  -1.713408E-04
  a1   31   4.839175E-04   1.175887E-05   2.217986E-05  -3.404548E-04  -4.986428E-05   1.203314E-04   2.780066E-04  -4.299829E-04
  a1   32   7.985577E-04  -3.462310E-05  -2.025068E-04   4.233968E-04  -6.861533E-04   6.511614E-04  -1.859385E-04  -2.355429E-04
  a1   33   6.073961E-04   2.177727E-04  -5.215491E-04   1.910525E-04   1.982762E-04  -6.089061E-04   4.709028E-04  -6.378280E-05
  a1   34  -3.277120E-06   4.330602E-04   5.081857E-04  -2.527707E-04  -8.403658E-04  -1.149743E-04   1.137559E-04   2.825131E-04
  a1   35   2.743801E-04   2.881581E-04   3.353601E-04   3.485399E-04  -6.801457E-04  -7.792447E-05   5.661845E-05   1.610833E-04
  a1   36   5.195250E-04  -4.086855E-04   1.684494E-04  -6.189033E-04   1.768600E-04  -5.243669E-04  -2.324884E-04  -4.771142E-04
  a1   37   6.082629E-04  -6.672875E-04  -2.015361E-04   9.964057E-05  -1.095915E-04   5.822931E-04  -2.992060E-04  -3.369717E-04
  a1   38   4.892900E-04  -9.009749E-05  -3.240598E-04  -2.087866E-04  -6.491222E-04  -3.579372E-04   3.744603E-04   1.859581E-04
  a1   39   8.370052E-05   8.981008E-05  -2.104051E-04  -6.338307E-04  -2.030291E-04   6.947502E-05   1.150832E-04   1.982213E-05
  a1   40   2.461823E-04  -3.791917E-04  -2.636219E-04  -1.344482E-04   7.573516E-04   1.965611E-04  -5.959321E-04  -9.527507E-04
  a1   41   3.109733E-03   1.621534E-04  -2.680918E-04  -4.006291E-04  -2.155048E-04  -1.798427E-04  -5.144636E-05  -2.153749E-04
  a1   42   1.621534E-04   3.056864E-03  -3.395630E-04  -3.358679E-05  -9.272015E-04   1.933572E-04   4.091658E-04   7.151568E-04
  a1   43  -2.680918E-04  -3.395630E-04   3.221260E-03   1.220295E-04   3.517402E-04   4.615498E-04  -6.126898E-04  -1.574790E-04
  a1   44  -4.006291E-04  -3.358679E-05   1.220295E-04   3.029529E-03  -3.508517E-04   2.879932E-04  -2.568682E-04   5.439041E-04
  a1   45  -2.155048E-04  -9.272015E-04   3.517402E-04  -3.508517E-04   2.986273E-03   1.671492E-04  -6.776673E-04  -6.239498E-04
  a1   46  -1.798427E-04   1.933572E-04   4.615498E-04   2.879932E-04   1.671492E-04   2.871722E-03  -2.408172E-05  -2.780837E-04
  a1   47  -5.144636E-05   4.091658E-04  -6.126898E-04  -2.568682E-04  -6.776673E-04  -2.408172E-05   2.312017E-03   6.102774E-05
  a1   48  -2.153749E-04   7.151568E-04  -1.574790E-04   5.439041E-04  -6.239498E-04  -2.780837E-04   6.102774E-05   2.011839E-03
  a1   49  -4.739115E-04   4.111452E-04   2.330435E-04  -9.198088E-04   4.873701E-04   8.724225E-04   6.486219E-04   2.164866E-05
  a1   50   6.059341E-05   2.775280E-04  -1.158264E-04  -2.321242E-04  -2.935806E-05   2.746092E-04   1.285650E-04  -4.872373E-06
  a1   51   2.905255E-04  -2.119062E-04   3.044754E-05   2.756494E-04   1.627726E-04   3.770526E-05  -3.724383E-05   6.089178E-05
  a1   52  -3.201469E-04   2.631919E-04  -1.063360E-04  -2.543857E-04  -1.652328E-04   3.708058E-04   3.059567E-05   2.064501E-05
  a1   53   4.020794E-04   3.435023E-05  -3.245777E-04   1.849608E-04   4.360395E-05  -2.420957E-04   1.786997E-04  -2.379075E-05
  a1   54   4.539587E-04   1.066287E-04   9.496069E-06   2.886218E-04   8.057114E-05  -4.143232E-04  -6.191443E-04   1.636603E-04
  a1   55   1.530085E-04  -1.719155E-04   1.649941E-04   7.719263E-05  -1.299548E-04   1.059678E-04  -1.464911E-04  -1.423131E-05
  a1   56  -2.215760E-04   8.054035E-05  -1.939780E-04  -1.895990E-04  -2.733116E-04  -8.190980E-05   2.401553E-04  -9.250122E-05
  a1   57  -1.150811E-04   1.466807E-04   8.236496E-05   4.462243E-06   3.014249E-04  -5.087640E-05  -1.302306E-04   1.651924E-04
  a1   58  -8.999402E-05   2.631888E-04   1.248463E-04  -2.849994E-04   8.806129E-06  -1.479447E-05   9.365807E-05  -4.128134E-05
  a1   59  -2.435286E-04   1.654004E-04   4.189446E-04   1.887976E-04   2.006510E-04   1.572037E-04  -1.118706E-04  -7.786086E-05
  a1   60   2.823059E-05  -5.566516E-05  -1.209611E-04   2.350344E-04   2.640264E-04   8.643291E-06  -1.036896E-04   2.155006E-04
  a1   61  -1.562125E-04   1.795189E-05   1.119987E-04   2.211917E-04   2.264537E-05  -2.598262E-04  -1.116386E-04  -2.814904E-04
  a1   62  -1.242329E-04   2.393525E-05  -1.172612E-04   4.130634E-05   1.687026E-04   5.856014E-05  -2.227147E-04  -2.269591E-04
  a1   63   2.639380E-05  -1.066754E-04  -2.408432E-04  -1.195435E-04   2.342933E-04   8.851208E-05  -1.325362E-04  -2.565457E-04
  a1   64  -1.806682E-05   2.050075E-05   6.853283E-05   1.086506E-04  -8.145231E-06   2.762059E-04  -1.758151E-04   1.054719E-06
  a1   65   2.435197E-04  -6.563975E-05   1.935418E-04  -1.657156E-04  -2.296644E-04  -7.121140E-05   4.473845E-05  -1.016310E-04
  a1   66  -9.342546E-05  -6.046591E-05   8.584657E-05  -1.568376E-04   3.006454E-04  -2.359176E-04  -1.559237E-04   3.155357E-05
  a1   67  -9.658176E-05  -1.403893E-04  -3.975965E-05   3.272035E-04   1.159546E-04   2.220859E-04   1.196194E-04   4.909494E-05
  a1   68   9.566539E-05  -7.495024E-05  -2.911294E-05   7.805530E-05  -1.705523E-04   1.647193E-04  -1.458053E-04  -1.318265E-04
  a1   69  -4.864376E-05  -5.049566E-05   6.848236E-04   1.071102E-04  -7.096433E-05   5.537800E-05  -1.096700E-04  -7.382924E-05
  a1   70   2.193870E-04   2.241376E-05  -3.737394E-05  -1.723335E-04   1.698138E-04  -1.787261E-05  -2.585948E-04  -1.176654E-04
  a1   71   7.447960E-07   2.437597E-04   1.025465E-04   7.509166E-05  -2.086877E-05   2.312328E-04  -2.131672E-04   1.593104E-04
  a1   72   2.311552E-04  -6.676514E-05  -1.090807E-04  -8.568733E-06   9.930335E-05  -2.386694E-05   4.802122E-05   1.115783E-05
  a1   73  -1.708999E-04  -2.357694E-04  -8.538642E-05  -8.096308E-05   7.880948E-05   2.041807E-05   9.404021E-05  -6.296362E-05
  a1   74  -1.924332E-04   2.179143E-04  -4.790089E-05  -2.589258E-05  -2.139408E-05  -1.982865E-05   8.610451E-05  -8.161380E-05
  a1   75   1.172611E-06   6.977681E-06   6.337795E-06  -3.517046E-05   8.335579E-05  -1.430381E-04   1.668776E-05   3.678055E-05
  a1   76  -4.397825E-05  -1.634049E-04  -4.843711E-05   1.015689E-04  -1.127944E-04   1.884844E-06  -1.313911E-04  -5.530020E-05
  a1   77  -1.574315E-05   1.352643E-04   2.411238E-05   3.976644E-05  -8.078489E-05  -6.133144E-06  -6.579411E-05   1.200723E-04
  a1   78   1.786314E-05  -1.200620E-05  -4.945797E-05  -2.217539E-05  -4.026387E-05  -5.398469E-06   3.464465E-05   7.995712E-05
  a1   79   3.167104E-04   2.549713E-04   1.667226E-04  -2.016863E-05  -5.625026E-05   7.804789E-05   9.197564E-05   2.944449E-05
  a1   80  -2.626996E-04  -1.286451E-04   1.662428E-04   2.258136E-04   7.256598E-06   9.020486E-06  -1.242091E-04  -1.346271E-05
  a1   81  -2.780092E-04   2.460604E-05   1.296965E-04  -1.594676E-04   4.720120E-05   1.526514E-05   2.957967E-05   1.690695E-04
  a1   82   6.193509E-05  -1.904188E-04   1.387761E-04  -7.808789E-05   2.768199E-04   5.757945E-05  -1.098602E-04  -1.407146E-04

               a1   49        a1   50        a1   51        a1   52        a1   53        a1   54        a1   55        a1   56
  a1    9   2.013140E-04  -1.031786E-04  -4.190394E-05  -3.798237E-04  -3.144424E-04   1.224270E-04   1.814624E-04   3.184384E-04
  a1   10  -1.365784E-03   1.730370E-04   6.117214E-05  -2.959426E-04   1.471913E-04   2.999313E-04   3.323933E-04   8.182259E-05
  a1   11   1.216658E-03   8.399276E-04  -2.692732E-04   2.091933E-04  -4.820766E-05  -8.968176E-04   4.111186E-04   8.695752E-04
  a1   12   1.450747E-03   4.821679E-04  -9.667614E-04   7.199826E-04  -1.024732E-04  -6.931821E-04  -1.952354E-05  -5.168748E-04
  a1   13   1.586372E-03   5.676384E-04  -3.891578E-04   1.289820E-03  -4.221103E-04   1.015780E-04  -1.888721E-04  -6.233502E-05
  a1   14   2.085878E-03   7.854110E-04  -7.260735E-04   9.676386E-05   2.070933E-04  -8.559516E-04  -1.051169E-03   8.201667E-04
  a1   15   2.136003E-03  -1.924029E-04  -1.376580E-04   1.619758E-03   7.582917E-05   2.108553E-03  -3.166125E-04  -2.538693E-03
  a1   16   1.621768E-03   1.437662E-04  -6.022225E-04  -4.812367E-04  -4.740904E-04  -8.171799E-04  -4.757240E-04   5.520477E-04
  a1   17   1.195940E-03  -1.373665E-04   5.755235E-04  -9.223325E-04  -4.444858E-04   1.248072E-04  -6.974506E-04  -8.833650E-04
  a1   18   1.031346E-03  -1.360019E-03  -4.507127E-04  -7.637918E-04  -5.894787E-04  -1.214652E-03   8.220431E-04  -2.063023E-04
  a1   19   2.509376E-04   6.373542E-04  -3.728250E-04  -1.182965E-03   1.267963E-03   2.282801E-04  -4.742300E-04   1.020840E-03
  a1   20   1.766624E-03  -1.606354E-04  -1.266935E-03  -1.849745E-04  -1.354710E-03   2.538849E-04   1.219633E-03   3.613793E-04
  a1   21  -9.769094E-05  -9.568737E-05  -1.278775E-04  -1.957389E-04  -9.298797E-05   4.361814E-05   4.941751E-05  -9.450594E-06
  a1   22   2.364130E-04   1.350891E-04   1.492665E-04   7.137481E-05   3.485575E-04  -2.658687E-04   1.172746E-04  -6.986928E-05
  a1   23  -3.349887E-04   4.047089E-05  -2.566708E-04  -5.677966E-05   1.890266E-05   3.120883E-04   1.340605E-04   9.861002E-05
  a1   24  -6.741242E-05   1.845849E-04  -3.052597E-04  -4.531788E-04   1.073134E-04   1.493253E-04   2.095809E-04  -5.831513E-05
  a1   25   8.489236E-05   4.162179E-04   2.979898E-04   1.324599E-05  -3.705735E-04   6.566178E-05  -5.073274E-04  -9.584101E-05
  a1   26  -5.674942E-04   9.418554E-04  -2.750087E-04  -8.006458E-04   4.346941E-04   4.821372E-04   2.420574E-04   4.100171E-04
  a1   27  -3.113150E-04  -4.527857E-04   2.795100E-07   5.108364E-04  -1.839193E-04   7.625291E-05   8.018858E-04   8.379854E-04
  a1   28   1.059692E-05   3.483940E-04   2.070334E-04   1.274882E-03   3.746402E-04  -4.737548E-04  -2.084576E-04  -1.604329E-04
  a1   29  -5.095077E-04  -2.092434E-04  -2.058244E-04  -1.423215E-04   8.533806E-05   1.209227E-03  -1.294023E-04  -2.121415E-04
  a1   30  -6.981781E-04  -2.918798E-04  -3.190333E-04  -1.418083E-04   7.308340E-05   3.645657E-04   1.882747E-04  -8.366371E-04
  a1   31   8.900599E-05   8.668130E-05  -2.336319E-04  -4.062057E-04   2.467790E-04  -5.603298E-04   3.765886E-05  -6.893072E-04
  a1   32  -7.855495E-04  -1.100421E-04   1.803366E-04  -2.965166E-04  -5.844171E-04   2.440535E-04  -1.581325E-04  -1.172647E-04
  a1   33  -6.425894E-04   7.497216E-06  -3.084810E-06  -4.190959E-04   4.039575E-04  -2.181700E-04  -5.346201E-04   2.668672E-04
  a1   34   2.589675E-04  -2.224416E-04   4.051181E-05   3.147417E-04   1.197990E-04   2.011534E-04   1.578853E-04  -3.363462E-04
  a1   35  -5.674049E-04   6.010061E-05  -1.053094E-04  -4.421358E-04   1.622201E-04  -4.467550E-04   5.254981E-04   2.480192E-04
  a1   36  -1.012427E-03  -4.210867E-04   2.816915E-04  -6.418465E-04  -8.524839E-05   3.094390E-04   1.664281E-04  -8.412110E-06
  a1   37  -3.674873E-04   7.918698E-05   2.515207E-04  -1.177050E-04  -2.112017E-04   2.141065E-06  -2.606846E-04  -1.390139E-04
  a1   38  -1.527767E-04   1.962303E-04  -3.924375E-06  -3.742387E-04  -6.341721E-05  -2.723051E-04  -8.373402E-05   1.745875E-04
  a1   39   7.793501E-04   2.609731E-04   1.124934E-04   3.762396E-04   1.555308E-04  -3.898457E-04  -2.442488E-04  -1.708161E-04
  a1   40  -6.495218E-04  -1.748585E-04   4.690939E-04  -1.937301E-04   1.534652E-05   1.722793E-04  -2.668863E-05  -1.816481E-04
  a1   41  -4.739115E-04   6.059341E-05   2.905255E-04  -3.201469E-04   4.020794E-04   4.539587E-04   1.530085E-04  -2.215760E-04
  a1   42   4.111452E-04   2.775280E-04  -2.119062E-04   2.631919E-04   3.435023E-05   1.066287E-04  -1.719155E-04   8.054035E-05
  a1   43   2.330435E-04  -1.158264E-04   3.044754E-05  -1.063360E-04  -3.245777E-04   9.496069E-06   1.649941E-04  -1.939780E-04
  a1   44  -9.198088E-04  -2.321242E-04   2.756494E-04  -2.543857E-04   1.849608E-04   2.886218E-04   7.719263E-05  -1.895990E-04
  a1   45   4.873701E-04  -2.935806E-05   1.627726E-04  -1.652328E-04   4.360395E-05   8.057114E-05  -1.299548E-04  -2.733116E-04
  a1   46   8.724225E-04   2.746092E-04   3.770526E-05   3.708058E-04  -2.420957E-04  -4.143232E-04   1.059678E-04  -8.190980E-05
  a1   47   6.486219E-04   1.285650E-04  -3.724383E-05   3.059567E-05   1.786997E-04  -6.191443E-04  -1.464911E-04   2.401553E-04
  a1   48   2.164866E-05  -4.872373E-06   6.089178E-05   2.064501E-05  -2.379075E-05   1.636603E-04  -1.423131E-05  -9.250122E-05
  a1   49   3.941407E-03   6.523640E-04  -8.588131E-05   9.075870E-04   4.281276E-05  -8.092645E-04  -9.076349E-05   3.894594E-04
  a1   50   6.523640E-04   1.740233E-03  -2.395329E-04   1.918029E-04  -2.258341E-04  -2.412409E-04  -2.034109E-05   1.773091E-04
  a1   51  -8.588131E-05  -2.395329E-04   1.520551E-03  -2.805115E-04  -1.568957E-04   2.389366E-04  -2.027218E-05  -7.856135E-05
  a1   52   9.075870E-04   1.918029E-04  -2.805115E-04   1.520634E-03   8.112148E-05  -1.186174E-04  -7.613351E-05   1.489723E-05
  a1   53   4.281276E-05  -2.258341E-04  -1.568957E-04   8.112148E-05   1.346165E-03   9.920579E-05   1.788125E-04  -6.634843E-05
  a1   54  -8.092645E-04  -2.412409E-04   2.389366E-04  -1.186174E-04   9.920579E-05   1.689014E-03  -1.271223E-04  -1.386469E-04
  a1   55  -9.076349E-05  -2.034109E-05  -2.027218E-05  -7.613351E-05   1.788125E-04  -1.271223E-04   1.360557E-03   1.478858E-04
  a1   56   3.894594E-04   1.773091E-04  -7.856135E-05   1.489723E-05  -6.634843E-05  -1.386469E-04   1.478858E-04   1.720165E-03
  a1   57  -3.266270E-04  -1.665516E-04   4.623226E-05  -1.601383E-04   7.490446E-05  -1.048152E-04   2.937877E-05  -1.279609E-04
  a1   58   2.618606E-04   2.039676E-04  -1.255354E-04  -7.448702E-05   4.672322E-05   8.492131E-05   2.518973E-04   9.100783E-05
  a1   59   2.419808E-04   1.046988E-05  -1.929719E-04   1.921757E-04  -1.611612E-05   1.652469E-04  -1.808403E-04  -3.561389E-05
  a1   60  -8.970429E-06  -1.883782E-04  -2.746061E-05  -6.613302E-05  -3.261073E-05   1.105605E-04   6.021611E-05  -4.971753E-05
  a1   61   2.541644E-04  -1.028089E-04   1.680364E-05  -3.916608E-05   9.866942E-05   7.682072E-05  -1.330563E-05   3.079860E-04
  a1   62   2.629767E-04  -1.407631E-04  -6.168552E-05  -9.141887E-05   3.231242E-06  -2.851167E-05   1.463773E-04  -8.759501E-05
  a1   63   1.405620E-04   1.191969E-04  -1.361350E-04   1.164221E-04   3.690014E-05  -2.818818E-06   4.523955E-05   5.930416E-05
  a1   64   7.027373E-05  -2.492448E-05   2.266079E-05  -6.514009E-06  -9.741893E-05   7.347127E-05  -5.502232E-05   2.933917E-05
  a1   65   1.595695E-04  -2.317859E-04   2.457162E-04  -6.569084E-05  -4.923838E-05   1.746409E-04   1.047832E-04  -8.145014E-06
  a1   66   7.174738E-05  -3.372874E-05  -2.284045E-04  -2.147469E-04  -6.690360E-05  -6.489815E-07  -6.102338E-05   3.717909E-05
  a1   67   1.500089E-04  -3.321308E-05   2.698197E-04  -6.921413E-05   4.459494E-05  -2.001406E-04   1.159128E-04   3.750087E-06
  a1   68  -1.486165E-04   2.782771E-04  -1.560777E-04   7.085136E-05  -1.528211E-04  -5.508363E-05   1.649212E-04  -7.844057E-05
  a1   69  -2.242713E-05   1.977174E-05   1.533791E-05   4.935823E-05  -6.782151E-05  -4.055117E-07   4.252571E-05  -2.194965E-05
  a1   70  -1.542705E-04  -1.155439E-04   2.594298E-04   3.016444E-05   3.555341E-05  -2.766950E-06  -4.343896E-05  -1.955803E-04
  a1   71   3.714469E-05  -1.603780E-04  -6.617252E-05   8.357159E-05   5.739723E-05   1.380025E-04   2.973587E-05  -9.063641E-05
  a1   72  -7.996030E-06  -2.490331E-06  -5.730720E-05  -3.128726E-05   2.594362E-04  -4.387867E-05   1.658119E-04  -1.296331E-04
  a1   73  -8.656814E-05  -2.147982E-04  -1.168731E-04   4.746844E-05  -1.839761E-04  -8.883197E-05   5.366950E-05   7.817816E-05
  a1   74  -7.178476E-05  -1.655990E-04  -1.604824E-04   5.881845E-05   1.949546E-04  -1.024418E-04   1.577905E-04  -5.821035E-05
  a1   75   1.013676E-04   1.020113E-04   7.394579E-05   6.474444E-05  -4.298099E-05   3.738264E-05  -1.018078E-05   8.151411E-05
  a1   76   4.904350E-05   1.369465E-05  -6.259788E-05   2.333455E-04   3.383810E-05  -5.730684E-05  -1.316435E-05  -6.335403E-05
  a1   77   1.257723E-04   1.208904E-04   2.266329E-06  -3.905817E-05  -6.268452E-05  -1.875710E-04   1.326635E-05  -1.265745E-04
  a1   78   7.769004E-05  -7.269116E-05  -1.067057E-04   5.502941E-05   1.481579E-04  -1.394297E-04   1.399646E-04  -1.055815E-05
  a1   79   7.307288E-05   5.671233E-05  -2.295009E-05  -1.272117E-04  -1.294659E-04  -9.746408E-05   6.340722E-05  -1.877938E-04
  a1   80  -3.175380E-04  -1.999469E-05   7.986021E-05  -2.747041E-04  -1.713907E-04   3.137391E-05   1.797758E-04  -8.594856E-05
  a1   81   6.117009E-04   6.401796E-05  -8.001030E-06   2.712226E-04  -1.535700E-04  -3.129225E-04  -4.919635E-05  -3.353336E-05
  a1   82  -1.507904E-05  -8.375502E-07   5.282913E-05  -7.551801E-05  -6.798337E-05   3.697985E-05   4.007710E-05  -1.507976E-05

               a1   57        a1   58        a1   59        a1   60        a1   61        a1   62        a1   63        a1   64
  a1    9  -2.364516E-04  -5.131948E-04  -6.422373E-05   5.180712E-04  -4.532892E-04   5.537624E-04   4.385425E-04   4.619123E-04
  a1   10   4.418509E-04  -1.727050E-04  -5.430424E-04   2.262032E-04   1.294047E-04  -5.532376E-04  -3.210271E-04  -8.170163E-05
  a1   11   5.093825E-04  -8.631257E-04  -5.191533E-04  -7.863839E-04   3.017757E-04   7.683583E-05  -3.470151E-04  -6.976632E-04
  a1   12  -6.256240E-04  -4.679809E-04  -4.766511E-04   6.421714E-04   1.191126E-04  -2.863640E-04  -1.508700E-03   3.241279E-04
  a1   13  -1.358320E-04  -9.026297E-04   1.358900E-04   1.108597E-03   8.840563E-05   5.788255E-04   3.404302E-05  -4.576851E-04
  a1   14   1.944533E-03  -7.003867E-04   8.329607E-04  -7.553409E-04   5.276219E-04  -3.651033E-04   3.845734E-04  -1.299951E-03
  a1   15  -8.237724E-04   1.167727E-03   1.020208E-03  -2.787086E-04  -3.320016E-04   8.033457E-04   6.079110E-04  -9.543951E-04
  a1   16  -5.637861E-04  -5.534893E-04  -9.510999E-04  -8.537893E-04  -2.252335E-03  -5.322352E-05   8.327680E-05   2.575135E-03
  a1   17   1.655970E-03   7.789757E-04  -1.806587E-04  -5.381030E-04  -1.171869E-03  -3.804656E-04  -5.144547E-04   7.372414E-04
  a1   18  -5.534867E-04  -7.387784E-04  -5.337545E-04  -1.133045E-03  -1.630743E-03  -1.759307E-03   1.050067E-03  -7.280815E-04
  a1   19   3.211325E-04  -1.578206E-03  -4.469253E-04   1.356512E-03  -7.681928E-04  -2.484769E-03   1.039163E-03   1.025736E-03
  a1   20  -5.374853E-04   1.508368E-03   1.721557E-04  -7.642455E-04   1.121645E-04   7.685509E-04   9.867818E-05  -3.548762E-04
  a1   21  -9.467762E-05  -5.726682E-06   4.192118E-06   5.591500E-05  -2.437025E-06  -7.555632E-05  -7.677939E-05  -1.783444E-04
  a1   22  -1.990995E-04  -1.551348E-04   1.603696E-05  -1.935560E-05  -9.529858E-05  -5.378994E-05   1.183806E-05   9.788940E-05
  a1   23   4.415562E-05  -8.282750E-05  -2.598401E-04  -1.993748E-04   6.546750E-05  -9.485138E-05   7.554174E-05   2.748357E-04
  a1   24   2.829189E-05   3.626951E-05  -7.414630E-05  -7.193209E-06   3.238037E-04   5.334643E-05   3.959733E-04   1.779574E-04
  a1   25   1.756015E-04   9.194271E-05   3.299159E-05  -3.093220E-04   4.500399E-04  -4.093293E-04   1.733437E-04  -2.362831E-04
  a1   26  -2.515745E-04   3.495140E-04   1.103452E-04   2.252702E-04   5.989231E-05  -2.086834E-04  -2.305852E-04   6.087623E-06
  a1   27  -1.039555E-04   5.035431E-04   1.955465E-04   6.932710E-04   4.922936E-05  -9.926091E-05   2.682533E-04  -2.504145E-05
  a1   28   1.730113E-04  -1.890234E-05  -3.678042E-04  -4.706261E-04  -2.675611E-04  -3.221049E-04   2.981561E-04   5.728631E-05
  a1   29   3.619332E-04  -5.136161E-04  -8.241937E-05   5.902301E-05   1.344735E-05  -1.792786E-04  -1.240374E-04  -5.157468E-05
  a1   30   8.761880E-05  -7.582279E-04   3.805976E-04   4.998948E-05  -1.737878E-04   5.219588E-04  -3.038846E-04  -6.313897E-04
  a1   31   6.030992E-04   3.323489E-04  -5.662077E-04  -7.774511E-05   4.606552E-04  -2.152645E-04   5.836139E-04   5.195419E-04
  a1   32  -4.688295E-04  -4.363498E-04  -3.870999E-04   1.419741E-04  -4.411326E-04   2.184240E-04  -2.936185E-04   4.094565E-04
  a1   33   9.566548E-05  -5.813810E-04   4.153346E-04   1.524060E-04  -8.027179E-06  -2.866544E-04  -1.631888E-04  -1.880081E-04
  a1   34  -3.417424E-04   7.901925E-04   6.283092E-04   2.813647E-04  -3.620648E-04   3.259071E-04  -2.529199E-04  -8.479801E-05
  a1   35   4.763896E-04  -3.480610E-04   2.336230E-05  -2.221521E-04  -6.482863E-05   2.692065E-04  -5.683896E-04  -2.579912E-04
  a1   36   3.551278E-04   1.589994E-04  -1.517084E-04  -6.436332E-04  -7.208953E-05   2.645883E-04   1.249624E-04  -1.065720E-04
  a1   37   3.178667E-04   7.957359E-05  -6.624782E-05  -4.078764E-05   2.828518E-04   1.291057E-04  -2.840989E-04  -1.680731E-04
  a1   38   8.167801E-05   4.974753E-06  -3.285862E-04   1.074488E-04  -1.556335E-04   1.185580E-05   1.656185E-04  -1.522559E-04
  a1   39  -1.498321E-04  -6.794771E-05   3.087083E-05  -2.946094E-04  -1.460747E-04   1.557857E-04   2.729746E-06  -1.127432E-04
  a1   40   1.088249E-04  -1.596045E-04  -2.125722E-04  -5.636235E-05   9.927765E-05   9.563076E-05  -1.096526E-04   1.509105E-05
  a1   41  -1.150811E-04  -8.999402E-05  -2.435286E-04   2.823059E-05  -1.562125E-04  -1.242329E-04   2.639380E-05  -1.806682E-05
  a1   42   1.466807E-04   2.631888E-04   1.654004E-04  -5.566516E-05   1.795189E-05   2.393525E-05  -1.066754E-04   2.050075E-05
  a1   43   8.236496E-05   1.248463E-04   4.189446E-04  -1.209611E-04   1.119987E-04  -1.172612E-04  -2.408432E-04   6.853283E-05
  a1   44   4.462243E-06  -2.849994E-04   1.887976E-04   2.350344E-04   2.211917E-04   4.130634E-05  -1.195435E-04   1.086506E-04
  a1   45   3.014249E-04   8.806129E-06   2.006510E-04   2.640264E-04   2.264537E-05   1.687026E-04   2.342933E-04  -8.145231E-06
  a1   46  -5.087640E-05  -1.479447E-05   1.572037E-04   8.643291E-06  -2.598262E-04   5.856014E-05   8.851208E-05   2.762059E-04
  a1   47  -1.302306E-04   9.365807E-05  -1.118706E-04  -1.036896E-04  -1.116386E-04  -2.227147E-04  -1.325362E-04  -1.758151E-04
  a1   48   1.651924E-04  -4.128134E-05  -7.786086E-05   2.155006E-04  -2.814904E-04  -2.269591E-04  -2.565457E-04   1.054719E-06
  a1   49  -3.266270E-04   2.618606E-04   2.419808E-04  -8.970429E-06   2.541644E-04   2.629767E-04   1.405620E-04   7.027373E-05
  a1   50  -1.665516E-04   2.039676E-04   1.046988E-05  -1.883782E-04  -1.028089E-04  -1.407631E-04   1.191969E-04  -2.492448E-05
  a1   51   4.623226E-05  -1.255354E-04  -1.929719E-04  -2.746061E-05   1.680364E-05  -6.168552E-05  -1.361350E-04   2.266079E-05
  a1   52  -1.601383E-04  -7.448702E-05   1.921757E-04  -6.613302E-05  -3.916608E-05  -9.141887E-05   1.164221E-04  -6.514009E-06
  a1   53   7.490446E-05   4.672322E-05  -1.611612E-05  -3.261073E-05   9.866942E-05   3.231242E-06   3.690014E-05  -9.741893E-05
  a1   54  -1.048152E-04   8.492131E-05   1.652469E-04   1.105605E-04   7.682072E-05  -2.851167E-05  -2.818818E-06   7.347127E-05
  a1   55   2.937877E-05   2.518973E-04  -1.808403E-04   6.021611E-05  -1.330563E-05   1.463773E-04   4.523955E-05  -5.502232E-05
  a1   56  -1.279609E-04   9.100783E-05  -3.561389E-05  -4.971753E-05   3.079860E-04  -8.759501E-05   5.930416E-05   2.933917E-05
  a1   57   1.203473E-03  -8.748808E-05  -2.384005E-05  -6.675772E-05   4.258991E-05  -1.041710E-04  -4.580286E-05  -8.859328E-05
  a1   58  -8.748808E-05   1.451077E-03   9.722160E-05   1.315899E-04   2.746292E-05  -5.087995E-05   1.262419E-04   1.664668E-04
  a1   59  -2.384005E-05   9.722160E-05   1.132487E-03   5.579466E-05   1.312255E-04   2.014593E-04   2.571814E-05  -1.145180E-05
  a1   60  -6.675772E-05   1.315899E-04   5.579466E-05   1.142659E-03   1.217191E-04  -1.107555E-04   1.118039E-04   1.596105E-04
  a1   61   4.258991E-05   2.746292E-05   1.312255E-04   1.217191E-04   1.115847E-03   1.621866E-04   4.127362E-05   6.529837E-06
  a1   62  -1.041710E-04  -5.087995E-05   2.014593E-04  -1.107555E-04   1.621866E-04   1.176959E-03  -8.606157E-05  -1.305691E-04
  a1   63  -4.580286E-05   1.262419E-04   2.571814E-05   1.118039E-04   4.127362E-05  -8.606157E-05   9.045856E-04   2.852330E-05
  a1   64  -8.859328E-05   1.664668E-04  -1.145180E-05   1.596105E-04   6.529837E-06  -1.305691E-04   2.852330E-05   7.860892E-04
  a1   65  -2.624443E-04   9.047446E-05  -8.570044E-05  -1.355123E-04  -1.423354E-05   2.200584E-04  -1.044938E-04   3.581086E-05
  a1   66  -1.062122E-04  -1.747404E-04   7.832585E-05   7.093655E-05  -1.711755E-05   1.300963E-04   1.074373E-04  -1.197504E-05
  a1   67   1.374412E-05  -1.063992E-04  -1.668394E-04   1.473502E-04   4.516750E-05  -2.377145E-05  -7.151444E-05   1.154318E-04
  a1   68  -1.683211E-04   6.600798E-05  -9.624781E-05  -1.055894E-04  -2.674881E-04   1.192594E-06   7.804992E-05   8.035547E-05
  a1   69   1.732473E-04   9.195741E-05   1.991839E-04  -1.690584E-04   9.079287E-05  -1.383013E-04  -1.289443E-04  -2.127875E-05
  a1   70   1.108122E-04  -7.548521E-06  -7.543016E-05   1.256378E-04  -1.417078E-04  -1.416696E-04   6.916505E-06   3.846602E-05
  a1   71  -1.000263E-04  -7.275870E-05   1.432260E-04   1.678766E-04   1.262703E-04   1.107829E-04   6.773896E-05   6.631951E-05
  a1   72  -8.588215E-05   5.350207E-05  -1.289240E-04  -2.213045E-05  -7.324694E-05   9.791831E-05   8.929135E-05  -5.669940E-06
  a1   73   4.918201E-05  -8.840490E-05  -8.577140E-05   1.165045E-04  -2.783127E-05   6.816697E-05   6.985896E-05   2.595252E-05
  a1   74   1.230508E-04   1.211952E-05  -3.312017E-05  -5.891463E-05   5.345267E-05   2.362834E-06   5.701380E-05  -8.343564E-05
  a1   75  -8.652186E-05  -5.955257E-05   1.447393E-04   3.237464E-05  -5.738537E-05  -1.293304E-04  -1.165113E-05   3.336821E-05
  a1   76  -1.321960E-04  -1.279827E-04  -8.227824E-05   6.917015E-05   1.312393E-04  -2.215304E-06  -1.035394E-04   7.837459E-05
  a1   77   1.054499E-04   1.238833E-04   8.558007E-06   1.039388E-04   2.069933E-05   8.789483E-05   7.798819E-05   7.509921E-06
  a1   78  -3.941118E-05   1.061752E-04   1.986042E-05   1.695483E-04  -1.551467E-04   1.003708E-04  -8.792141E-05  -1.302606E-04
  a1   79   2.574993E-05  -7.723239E-05   2.953150E-05   8.436024E-05   1.445801E-05   8.249082E-05  -5.676033E-05  -3.114813E-05
  a1   80  -2.668168E-05   5.282100E-05   5.109966E-05   1.325559E-04   6.440448E-05   6.045280E-07  -2.236183E-05   1.839567E-05
  a1   81  -8.954723E-05   6.008354E-05   8.260643E-05  -1.027451E-04  -2.161336E-05   5.421819E-05   4.787262E-05  -5.981334E-06
  a1   82  -2.202933E-05  -2.966638E-06   5.881474E-05   8.579008E-05  -2.524843E-05   1.099644E-05  -5.706928E-05  -1.588148E-05

               a1   65        a1   66        a1   67        a1   68        a1   69        a1   70        a1   71        a1   72
  a1    9   1.472415E-04   5.529371E-04  -5.548852E-05   1.117759E-04  -1.235622E-03   9.127768E-06   8.923802E-04  -4.687811E-04
  a1   10  -4.812931E-04   3.536869E-06  -1.032572E-04   1.821245E-05   2.070487E-04   6.930255E-04   1.035726E-05  -9.857802E-05
  a1   11  -5.685065E-04  -1.021502E-05   2.286486E-04   2.459793E-06   3.248621E-04  -3.890463E-04  -3.665064E-04  -1.145209E-04
  a1   12   2.426387E-04   8.029201E-04  -1.730154E-04   2.029533E-04   1.708697E-04  -1.138309E-04  -3.172851E-04   3.290604E-05
  a1   13  -1.919432E-04   2.020135E-04  -8.339299E-04  -5.466774E-04  -1.270211E-03   4.191493E-04   1.965784E-03  -1.434926E-03
  a1   14  -7.682726E-04  -6.339639E-04  -4.757467E-04  -1.285900E-03   1.308267E-03   1.336334E-03  -1.532734E-04   6.043140E-04
  a1   15   8.610326E-04  -7.315927E-04  -1.250653E-04   7.386372E-05   3.687416E-05  -3.438339E-04  -8.415925E-04  -1.451495E-04
  a1   16   1.109202E-03   2.357770E-03   1.224363E-03   6.554786E-04  -6.891065E-04   9.795003E-04  -1.378179E-03   1.153287E-04
  a1   17  -9.954951E-04  -6.527517E-04   3.087033E-04  -3.728498E-04   2.337938E-04  -1.516733E-03   1.224556E-03   9.252497E-04
  a1   18   1.185185E-03  -4.007357E-04   2.418899E-05   1.889900E-04  -5.279230E-04  -4.032509E-04   3.931717E-04   3.951912E-04
  a1   19   3.793956E-04   8.382585E-04   8.755338E-04   1.997041E-04  -1.944023E-04  -7.206785E-04  -1.378578E-03  -9.282781E-04
  a1   20   2.805165E-04  -8.100593E-04  -9.292867E-05  -2.085435E-05  -9.261844E-05   1.399913E-04   7.313214E-04   7.663707E-04
  a1   21  -3.279716E-05  -5.306055E-05   3.316870E-05   1.441511E-05   1.316917E-04   4.963598E-06  -1.983759E-04   2.610632E-04
  a1   22   7.406054E-05   1.068462E-04   3.479752E-05   3.705367E-05  -7.244987E-05   1.673609E-04   1.959574E-04   1.925292E-04
  a1   23  -5.933986E-06   1.783439E-05   1.499132E-04   1.968406E-04   4.130115E-05  -1.704261E-04  -8.142899E-05  -1.920563E-05
  a1   24   2.740442E-05  -2.214159E-05  -9.695212E-05   4.090805E-05  -1.560179E-04  -2.348103E-04   3.848307E-04   4.230337E-05
  a1   25  -1.129642E-04  -2.973835E-04  -6.179126E-05  -2.144633E-04   5.970412E-04  -7.272199E-05  -2.441029E-04   1.420945E-04
  a1   26  -2.970756E-04  -1.021826E-05   1.182700E-04   2.816114E-05   2.696594E-05   1.676750E-04  -1.537368E-04  -1.910292E-05
  a1   27   7.773992E-05  -7.841286E-05  -1.202538E-04   4.211500E-05  -6.498123E-05   7.825572E-05   8.758963E-05  -1.051394E-06
  a1   28   6.941260E-05  -1.917967E-05   1.405104E-04  -2.894798E-05   7.483999E-05   6.073123E-05  -3.805382E-04   4.258055E-05
  a1   29   6.111633E-05  -1.226438E-04   9.598800E-05   9.999640E-05   1.554086E-04  -2.299394E-04  -4.205614E-04  -5.974005E-05
  a1   30   2.814164E-05   1.289468E-04  -1.837525E-04  -1.491679E-04  -1.945847E-04  -7.676334E-05   2.948117E-04  -1.394517E-04
  a1   31  -5.549764E-05  -1.776382E-04   5.130864E-05   7.737163E-05   1.375928E-05  -2.608004E-04   2.411249E-04   2.272941E-04
  a1   32   2.805557E-04   3.711743E-04   7.070607E-05   2.630933E-04  -2.639533E-04   1.126298E-05   2.312962E-05   3.361823E-05
  a1   33  -2.648597E-04   1.466693E-04  -6.338600E-05  -3.155270E-04   3.566162E-05   4.394825E-04  -7.544317E-06   9.927034E-05
  a1   34   2.208494E-04  -1.109310E-04   5.118745E-05   5.299845E-05   1.321634E-04   2.403957E-04  -3.339567E-04  -1.039245E-05
  a1   35  -2.100242E-04   9.719051E-05   9.836873E-05   8.421399E-05   3.232744E-04   1.704657E-04  -3.444405E-04   1.520335E-04
  a1   36   3.327197E-04  -7.974118E-05  -9.391098E-05   1.315946E-06   1.278846E-04  -1.957471E-04  -6.890477E-05   1.996737E-04
  a1   37  -2.733047E-04  -4.589677E-04  -9.567609E-05   1.482911E-05   2.610672E-04   8.515994E-05  -5.062917E-06   2.657180E-04
  a1   38   2.500944E-05   2.129859E-04   1.333194E-04   8.353394E-05  -1.495157E-04   2.635906E-05  -1.647599E-04  -3.572698E-04
  a1   39   2.684806E-04   2.174437E-05  -6.997091E-05  -3.132060E-05  -4.102481E-05  -1.857127E-04   1.056857E-05   8.876312E-05
  a1   40   2.600707E-06  -2.120203E-04  -6.153835E-06   1.641413E-04  -2.399839E-04   1.235114E-04   4.585792E-05  -3.964695E-04
  a1   41   2.435197E-04  -9.342546E-05  -9.658176E-05   9.566539E-05  -4.864376E-05   2.193870E-04   7.447960E-07   2.311552E-04
  a1   42  -6.563975E-05  -6.046591E-05  -1.403893E-04  -7.495024E-05  -5.049566E-05   2.241376E-05   2.437597E-04  -6.676514E-05
  a1   43   1.935418E-04   8.584657E-05  -3.975965E-05  -2.911294E-05   6.848236E-04  -3.737394E-05   1.025465E-04  -1.090807E-04
  a1   44  -1.657156E-04  -1.568376E-04   3.272035E-04   7.805530E-05   1.071102E-04  -1.723335E-04   7.509166E-05  -8.568733E-06
  a1   45  -2.296644E-04   3.006454E-04   1.159546E-04  -1.705523E-04  -7.096433E-05   1.698138E-04  -2.086877E-05   9.930335E-05
  a1   46  -7.121140E-05  -2.359176E-04   2.220859E-04   1.647193E-04   5.537800E-05  -1.787261E-05   2.312328E-04  -2.386694E-05
  a1   47   4.473845E-05  -1.559237E-04   1.196194E-04  -1.458053E-04  -1.096700E-04  -2.585948E-04  -2.131672E-04   4.802122E-05
  a1   48  -1.016310E-04   3.155357E-05   4.909494E-05  -1.318265E-04  -7.382924E-05  -1.176654E-04   1.593104E-04   1.115783E-05
  a1   49   1.595695E-04   7.174738E-05   1.500089E-04  -1.486165E-04  -2.242713E-05  -1.542705E-04   3.714469E-05  -7.996030E-06
  a1   50  -2.317859E-04  -3.372874E-05  -3.321308E-05   2.782771E-04   1.977174E-05  -1.155439E-04  -1.603780E-04  -2.490331E-06
  a1   51   2.457162E-04  -2.284045E-04   2.698197E-04  -1.560777E-04   1.533791E-05   2.594298E-04  -6.617252E-05  -5.730720E-05
  a1   52  -6.569084E-05  -2.147469E-04  -6.921413E-05   7.085136E-05   4.935823E-05   3.016444E-05   8.357159E-05  -3.128726E-05
  a1   53  -4.923838E-05  -6.690360E-05   4.459494E-05  -1.528211E-04  -6.782151E-05   3.555341E-05   5.739723E-05   2.594362E-04
  a1   54   1.746409E-04  -6.489815E-07  -2.001406E-04  -5.508363E-05  -4.055117E-07  -2.766950E-06   1.380025E-04  -4.387867E-05
  a1   55   1.047832E-04  -6.102338E-05   1.159128E-04   1.649212E-04   4.252571E-05  -4.343896E-05   2.973587E-05   1.658119E-04
  a1   56  -8.145014E-06   3.717909E-05   3.750087E-06  -7.844057E-05  -2.194965E-05  -1.955803E-04  -9.063641E-05  -1.296331E-04
  a1   57  -2.624443E-04  -1.062122E-04   1.374412E-05  -1.683211E-04   1.732473E-04   1.108122E-04  -1.000263E-04  -8.588215E-05
  a1   58   9.047446E-05  -1.747404E-04  -1.063992E-04   6.600798E-05   9.195741E-05  -7.548521E-06  -7.275870E-05   5.350207E-05
  a1   59  -8.570044E-05   7.832585E-05  -1.668394E-04  -9.624781E-05   1.991839E-04  -7.543016E-05   1.432260E-04  -1.289240E-04
  a1   60  -1.355123E-04   7.093655E-05   1.473502E-04  -1.055894E-04  -1.690584E-04   1.256378E-04   1.678766E-04  -2.213045E-05
  a1   61  -1.423354E-05  -1.711755E-05   4.516750E-05  -2.674881E-04   9.079287E-05  -1.417078E-04   1.262703E-04  -7.324694E-05
  a1   62   2.200584E-04   1.300963E-04  -2.377145E-05   1.192594E-06  -1.383013E-04  -1.416696E-04   1.107829E-04   9.791831E-05
  a1   63  -1.044938E-04   1.074373E-04  -7.151444E-05   7.804992E-05  -1.289443E-04   6.916505E-06   6.773896E-05   8.929135E-05
  a1   64   3.581086E-05  -1.197504E-05   1.154318E-04   8.035547E-05  -2.127875E-05   3.846602E-05   6.631951E-05  -5.669940E-06
  a1   65   1.098085E-03  -8.639204E-05   1.111813E-04  -1.309197E-04  -1.738408E-05  -1.328031E-04  -5.094701E-05  -1.206350E-05
  a1   66  -8.639204E-05   8.997998E-04  -2.245697E-04  -1.924020E-05  -1.209064E-04  -3.198578E-05   4.910993E-05   1.454741E-05
  a1   67   1.111813E-04  -2.245697E-04   9.196732E-04  -2.112204E-04  -4.376034E-05   3.815674E-05  -1.030549E-04   2.106158E-05
  a1   68  -1.309197E-04  -1.924020E-05  -2.112204E-04   8.671329E-04   5.554192E-05  -3.445121E-05  -1.255649E-04  -3.208730E-06
  a1   69  -1.738408E-05  -1.209064E-04  -4.376034E-05   5.554192E-05   6.715058E-04   1.413449E-05  -1.252104E-04  -4.205962E-05
  a1   70  -1.328031E-04  -3.198578E-05   3.815674E-05  -3.445121E-05   1.413449E-05   7.245946E-04  -2.421848E-06   2.551096E-06
  a1   71  -5.094701E-05   4.910993E-05  -1.030549E-04  -1.255649E-04  -1.252104E-04  -2.421848E-06   6.955422E-04   2.851448E-05
  a1   72  -1.206350E-05   1.454741E-05   2.106158E-05  -3.208730E-06  -4.205962E-05   2.551096E-06   2.851448E-05   6.294327E-04
  a1   73  -2.754138E-05   7.212838E-05   1.162644E-05  -1.259332E-05  -1.430795E-05  -4.074860E-05  -1.251215E-05   3.739208E-05
  a1   74  -5.498352E-05  -5.021380E-05  -9.226166E-07  -1.974247E-05  -1.776391E-05  -3.059032E-06  -1.622488E-05   8.600866E-05
  a1   75   1.193494E-04   5.383321E-06  -3.562340E-05  -1.360785E-05   2.183991E-05  -3.203231E-05  -6.350441E-05  -5.661811E-05
  a1   76  -5.154284E-05  -1.300915E-04   9.471382E-06   1.206265E-04  -5.062884E-06   2.909074E-05   1.113835E-05  -2.622608E-05
  a1   77   7.621348E-05   8.562160E-06   6.304282E-05   8.618081E-05   7.504274E-05  -8.618700E-05  -8.041270E-05   4.554490E-05
  a1   78  -6.152726E-05   4.531990E-05   5.356349E-05  -1.260772E-05  -2.972188E-05   4.028354E-05  -4.183427E-05   1.198351E-04
  a1   79   2.489134E-05   8.938612E-05   5.122814E-05   6.283938E-05   9.368555E-05   5.096440E-05  -1.138003E-05  -3.750439E-06
  a1   80  -5.484376E-05   1.207780E-04   9.197536E-05   3.050277E-05   1.011635E-04   5.214974E-05   5.191001E-05  -4.363156E-05
  a1   81   5.273958E-06   9.201716E-05   3.843775E-05  -4.331927E-05   2.887191E-05  -4.479801E-05   5.212104E-05  -3.780339E-05
  a1   82  -2.541974E-06   3.541738E-05   7.761005E-07  -5.602770E-09   9.167368E-06  -1.795310E-05   5.296349E-05  -1.687260E-05

               a1   73        a1   74        a1   75        a1   76        a1   77        a1   78        a1   79        a1   80
  a1    9   5.977293E-04  -1.153941E-03   4.237778E-04   3.921830E-05  -1.092765E-03  -5.501376E-04  -6.498085E-04  -1.740580E-04
  a1   10   1.525647E-05   2.300910E-04  -6.948736E-04  -1.881024E-04  -1.485481E-03  -4.203384E-04   2.438873E-03  -5.229172E-04
  a1   11   1.323391E-04   6.230538E-04  -1.926270E-04   2.862526E-04   4.574331E-04   3.765498E-04  -1.086786E-03   3.276198E-04
  a1   12   1.403347E-04  -8.595302E-04   1.343031E-04   1.547743E-03   5.889714E-04  -1.753907E-04   5.188655E-04   9.565208E-04
  a1   13   4.962367E-05   5.340543E-04  -2.378192E-05   6.867832E-04  -6.768513E-04  -2.327724E-04  -8.380900E-04  -4.655771E-04
  a1   14   1.015296E-03   3.654178E-04  -1.034668E-07  -8.239638E-04   5.662947E-04   3.161759E-04  -8.343738E-05  -2.831159E-04
  a1   15   8.440322E-05   2.617057E-04  -5.342902E-04  -1.261139E-04  -1.275460E-03  -1.632245E-04  -2.696713E-04   3.774439E-04
  a1   16  -1.710198E-04  -2.541807E-04   4.541654E-04  -1.290623E-03   6.068402E-04  -4.070780E-04   8.535640E-04   9.816918E-04
  a1   17   6.988930E-04   8.370634E-04  -1.961132E-04  -1.180197E-03  -1.493434E-04  -6.711297E-04  -4.250840E-04  -3.803691E-04
  a1   18  -9.374432E-04  -1.140187E-03  -7.791176E-04  -1.256203E-03   1.757210E-04  -5.412547E-04   5.241900E-04   6.216611E-04
  a1   19   1.502521E-03  -3.393257E-04   2.681031E-04  -6.888982E-04   2.102428E-03   3.549722E-05   4.472775E-04   1.608299E-04
  a1   20  -1.086646E-03  -1.193089E-04  -2.408821E-04  -1.564567E-04  -6.183754E-04  -1.183448E-04  -1.162841E-03   5.398457E-04
  a1   21   2.827337E-04  -1.415281E-04  -4.933512E-05   4.966045E-05  -1.078165E-04   4.960682E-05  -2.261434E-05   5.785324E-05
  a1   22  -3.700952E-05  -1.570606E-04   3.768760E-04   1.857101E-05   3.052840E-05  -1.163486E-04   3.752256E-05  -4.076402E-05
  a1   23   1.445821E-04   2.193452E-04   2.242917E-04   5.037671E-06  -1.458373E-04  -1.828728E-04   2.019016E-05   1.577677E-04
  a1   24   1.293423E-05  -2.904665E-04  -1.006323E-04  -8.434339E-06  -5.166945E-06  -6.138840E-05  -6.804598E-06   3.767866E-05
  a1   25  -1.255121E-04   3.721923E-04  -4.461411E-05  -2.276634E-05   2.804455E-04   1.327060E-04   1.583027E-04   2.150735E-05
  a1   26  -1.028783E-04   2.774339E-05  -2.036821E-05   2.083588E-05  -4.307574E-05   7.582376E-06  -1.745961E-05   1.508879E-04
  a1   27  -6.583559E-05  -1.841712E-05   4.815579E-05   4.652264E-05   9.500642E-06   5.781003E-05   1.903214E-05  -1.500291E-05
  a1   28   1.053275E-05   7.992499E-05  -1.483423E-05   2.400174E-04   9.217167E-05   1.599110E-04  -3.012166E-05  -1.765250E-04
  a1   29   1.851800E-04   1.298599E-04  -1.044168E-04  -5.527901E-05  -3.342801E-06  -2.994572E-06   3.998481E-05   5.048815E-05
  a1   30  -3.134422E-05   2.942767E-05  -1.256337E-04   1.556228E-04  -7.534474E-05   3.013447E-06  -8.082057E-05   1.658511E-04
  a1   31  -1.760719E-05   2.503845E-04   6.584913E-05   6.841097E-05   2.685993E-04  -7.613109E-05   2.164616E-04   2.913717E-06
  a1   32   7.109130E-05  -1.327407E-04   2.310230E-04   1.775878E-04   1.225345E-05  -1.567786E-04   3.088727E-05   1.642475E-04
  a1   33  -2.479468E-04  -6.383794E-05   2.782542E-04  -1.085104E-04   2.365644E-04   6.570309E-05  -4.059282E-05  -4.838868E-05
  a1   34  -5.189216E-05   1.563815E-04   5.463117E-05  -3.488704E-05   3.918718E-05   2.418252E-04   4.106246E-05   9.855127E-06
  a1   35  -1.139057E-06   3.174681E-04   1.118562E-04   3.555969E-05   1.597590E-04   1.567287E-04   1.458825E-04   1.891961E-04
  a1   36   4.475405E-05   2.901872E-05  -4.487634E-05  -3.509641E-04   1.167546E-04  -4.040201E-05  -1.623516E-04  -2.501324E-07
  a1   37   1.664416E-04  -4.975848E-04  -2.259454E-04   2.246300E-04   1.007097E-04  -5.749751E-06   1.973271E-05  -2.446432E-05
  a1   38  -1.211417E-04  -2.039254E-04  -4.803523E-04  -1.607942E-04   1.840431E-04   2.546485E-04   4.699770E-05  -3.359381E-05
  a1   39  -8.758340E-05  -7.373015E-05   1.257098E-04   5.473482E-05   4.793238E-04   1.119646E-04  -5.908376E-04   2.318093E-04
  a1   40  -3.567648E-04   4.075537E-04  -1.058226E-04   1.485099E-04   3.991914E-05  -7.311874E-05  -9.973674E-05  -1.084306E-05
  a1   41  -1.708999E-04  -1.924332E-04   1.172611E-06  -4.397825E-05  -1.574315E-05   1.786314E-05   3.167104E-04  -2.626996E-04
  a1   42  -2.357694E-04   2.179143E-04   6.977681E-06  -1.634049E-04   1.352643E-04  -1.200620E-05   2.549713E-04  -1.286451E-04
  a1   43  -8.538642E-05  -4.790089E-05   6.337795E-06  -4.843711E-05   2.411238E-05  -4.945797E-05   1.667226E-04   1.662428E-04
  a1   44  -8.096308E-05  -2.589258E-05  -3.517046E-05   1.015689E-04   3.976644E-05  -2.217539E-05  -2.016863E-05   2.258136E-04
  a1   45   7.880948E-05  -2.139408E-05   8.335579E-05  -1.127944E-04  -8.078489E-05  -4.026387E-05  -5.625026E-05   7.256598E-06
  a1   46   2.041807E-05  -1.982865E-05  -1.430381E-04   1.884844E-06  -6.133144E-06  -5.398469E-06   7.804789E-05   9.020486E-06
  a1   47   9.404021E-05   8.610451E-05   1.668776E-05  -1.313911E-04  -6.579411E-05   3.464465E-05   9.197564E-05  -1.242091E-04
  a1   48  -6.296362E-05  -8.161380E-05   3.678055E-05  -5.530020E-05   1.200723E-04   7.995712E-05   2.944449E-05  -1.346271E-05
  a1   49  -8.656814E-05  -7.178476E-05   1.013676E-04   4.904350E-05   1.257723E-04   7.769004E-05   7.307288E-05  -3.175380E-04
  a1   50  -2.147982E-04  -1.655990E-04   1.020113E-04   1.369465E-05   1.208904E-04  -7.269116E-05   5.671233E-05  -1.999469E-05
  a1   51  -1.168731E-04  -1.604824E-04   7.394579E-05  -6.259788E-05   2.266329E-06  -1.067057E-04  -2.295009E-05   7.986021E-05
  a1   52   4.746844E-05   5.881845E-05   6.474444E-05   2.333455E-04  -3.905817E-05   5.502941E-05  -1.272117E-04  -2.747041E-04
  a1   53  -1.839761E-04   1.949546E-04  -4.298099E-05   3.383810E-05  -6.268452E-05   1.481579E-04  -1.294659E-04  -1.713907E-04
  a1   54  -8.883197E-05  -1.024418E-04   3.738264E-05  -5.730684E-05  -1.875710E-04  -1.394297E-04  -9.746408E-05   3.137391E-05
  a1   55   5.366950E-05   1.577905E-04  -1.018078E-05  -1.316435E-05   1.326635E-05   1.399646E-04   6.340722E-05   1.797758E-04
  a1   56   7.817816E-05  -5.821035E-05   8.151411E-05  -6.335403E-05  -1.265745E-04  -1.055815E-05  -1.877938E-04  -8.594856E-05
  a1   57   4.918201E-05   1.230508E-04  -8.652186E-05  -1.321960E-04   1.054499E-04  -3.941118E-05   2.574993E-05  -2.668168E-05
  a1   58  -8.840490E-05   1.211952E-05  -5.955257E-05  -1.279827E-04   1.238833E-04   1.061752E-04  -7.723239E-05   5.282100E-05
  a1   59  -8.577140E-05  -3.312017E-05   1.447393E-04  -8.227824E-05   8.558007E-06   1.986042E-05   2.953150E-05   5.109966E-05
  a1   60   1.165045E-04  -5.891463E-05   3.237464E-05   6.917015E-05   1.039388E-04   1.695483E-04   8.436024E-05   1.325559E-04
  a1   61  -2.783127E-05   5.345267E-05  -5.738537E-05   1.312393E-04   2.069933E-05  -1.551467E-04   1.445801E-05   6.440448E-05
  a1   62   6.816697E-05   2.362834E-06  -1.293304E-04  -2.215304E-06   8.789483E-05   1.003708E-04   8.249082E-05   6.045280E-07
  a1   63   6.985896E-05   5.701380E-05  -1.165113E-05  -1.035394E-04   7.798819E-05  -8.792141E-05  -5.676033E-05  -2.236183E-05
  a1   64   2.595252E-05  -8.343564E-05   3.336821E-05   7.837459E-05   7.509921E-06  -1.302606E-04  -3.114813E-05   1.839567E-05
  a1   65  -2.754138E-05  -5.498352E-05   1.193494E-04  -5.154284E-05   7.621348E-05  -6.152726E-05   2.489134E-05  -5.484376E-05
  a1   66   7.212838E-05  -5.021380E-05   5.383321E-06  -1.300915E-04   8.562160E-06   4.531990E-05   8.938612E-05   1.207780E-04
  a1   67   1.162644E-05  -9.226166E-07  -3.562340E-05   9.471382E-06   6.304282E-05   5.356349E-05   5.122814E-05   9.197536E-05
  a1   68  -1.259332E-05  -1.974247E-05  -1.360785E-05   1.206265E-04   8.618081E-05  -1.260772E-05   6.283938E-05   3.050277E-05
  a1   69  -1.430795E-05  -1.776391E-05   2.183991E-05  -5.062884E-06   7.504274E-05  -2.972188E-05   9.368555E-05   1.011635E-04
  a1   70  -4.074860E-05  -3.059032E-06  -3.203231E-05   2.909074E-05  -8.618700E-05   4.028354E-05   5.096440E-05   5.214974E-05
  a1   71  -1.251215E-05  -1.622488E-05  -6.350441E-05   1.113835E-05  -8.041270E-05  -4.183427E-05  -1.138003E-05   5.191001E-05
  a1   72   3.739208E-05   8.600866E-05  -5.661811E-05  -2.622608E-05   4.554490E-05   1.198351E-04  -3.750439E-06  -4.363156E-05
  a1   73   5.193453E-04  -4.985777E-06  -2.563505E-05  -3.558853E-06   1.208851E-05   2.866062E-05   1.349600E-05   1.597006E-05
  a1   74  -4.985777E-06   6.860353E-04  -8.558246E-05  -8.129027E-06   2.909233E-05   1.396183E-04  -1.593443E-05  -6.297847E-05
  a1   75  -2.563505E-05  -8.558246E-05   5.133722E-04   7.318473E-06   4.384333E-05  -1.078780E-04  -2.011320E-05   1.569747E-05
  a1   76  -3.558853E-06  -8.129027E-06   7.318473E-06   4.702499E-04  -2.392567E-05  -8.630365E-06  -7.866503E-05  -4.384874E-05
  a1   77   1.208851E-05   2.909233E-05   4.384333E-05  -2.392567E-05   6.381000E-04   1.048569E-04   1.646286E-06   1.062073E-04
  a1   78   2.866062E-05   1.396183E-04  -1.078780E-04  -8.630365E-06   1.048569E-04   4.631849E-04  -4.444785E-06  -1.941157E-05
  a1   79   1.349600E-05  -1.593443E-05  -2.011320E-05  -7.866503E-05   1.646286E-06  -4.444785E-06   7.612201E-04  -6.793702E-05
  a1   80   1.597006E-05  -6.297847E-05   1.569747E-05  -4.384874E-05   1.062073E-04  -1.941157E-05  -6.793702E-05   7.084601E-04
  a1   81   1.472136E-05  -4.676315E-05   2.841469E-05  -4.922232E-06   1.408130E-05  -4.554605E-07   1.066788E-04  -9.363616E-05
  a1   82   6.553787E-06  -3.031169E-05  -5.632534E-06  -1.063141E-05   6.194459E-07  -5.869496E-05   1.505308E-05   2.783967E-05

               a1   81        a1   82
  a1    9   1.909590E-04   5.500820E-04
  a1   10  -1.585221E-03   3.781768E-04
  a1   11   6.245517E-04  -3.175595E-04
  a1   12   3.849752E-05  -4.532314E-04
  a1   13  -4.092039E-04   1.942637E-04
  a1   14  -7.871570E-04  -3.879162E-04
  a1   15  -2.371528E-04  -1.193188E-04
  a1   16  -2.925403E-04  -4.883111E-04
  a1   17  -1.708135E-04   4.219307E-04
  a1   18  -8.163640E-05   9.560186E-05
  a1   19   1.444290E-04  -3.424375E-04
  a1   20   5.781061E-04  -2.015872E-04
  a1   21  -7.487455E-06   2.723695E-05
  a1   22   6.633139E-05   3.978392E-05
  a1   23  -6.989063E-05  -4.813770E-05
  a1   24  -1.369161E-04  -1.009869E-04
  a1   25  -3.380246E-05  -1.042352E-04
  a1   26  -4.252712E-04   1.375576E-04
  a1   27  -1.512220E-04   1.004502E-04
  a1   28   2.175505E-04  -1.447169E-04
  a1   29  -5.001851E-04   1.031098E-05
  a1   30  -1.995710E-04   6.023797E-05
  a1   31  -5.451338E-05  -1.081934E-04
  a1   32  -1.350348E-04   6.632623E-05
  a1   33  -3.122898E-04   3.697584E-05
  a1   34   1.881054E-04  -9.740700E-06
  a1   35  -2.139394E-04   1.707450E-06
  a1   36   3.174579E-05   1.096391E-05
  a1   37  -1.754456E-04   3.115588E-05
  a1   38  -3.145355E-05  -1.878204E-05
  a1   39  -6.310648E-05  -5.076905E-05
  a1   40  -2.040719E-04   2.030323E-04
  a1   41  -2.780092E-04   6.193509E-05
  a1   42   2.460604E-05  -1.904188E-04
  a1   43   1.296965E-04   1.387761E-04
  a1   44  -1.594676E-04  -7.808789E-05
  a1   45   4.720120E-05   2.768199E-04
  a1   46   1.526514E-05   5.757945E-05
  a1   47   2.957967E-05  -1.098602E-04
  a1   48   1.690695E-04  -1.407146E-04
  a1   49   6.117009E-04  -1.507904E-05
  a1   50   6.401796E-05  -8.375502E-07
  a1   51  -8.001030E-06   5.282913E-05
  a1   52   2.712226E-04  -7.551801E-05
  a1   53  -1.535700E-04  -6.798337E-05
  a1   54  -3.129225E-04   3.697985E-05
  a1   55  -4.919635E-05   4.007710E-05
  a1   56  -3.353336E-05  -1.507976E-05
  a1   57  -8.954723E-05  -2.202933E-05
  a1   58   6.008354E-05  -2.966638E-06
  a1   59   8.260643E-05   5.881474E-05
  a1   60  -1.027451E-04   8.579008E-05
  a1   61  -2.161336E-05  -2.524843E-05
  a1   62   5.421819E-05   1.099644E-05
  a1   63   4.787262E-05  -5.706928E-05
  a1   64  -5.981334E-06  -1.588148E-05
  a1   65   5.273958E-06  -2.541974E-06
  a1   66   9.201716E-05   3.541738E-05
  a1   67   3.843775E-05   7.761005E-07
  a1   68  -4.331927E-05  -5.602770E-09
  a1   69   2.887191E-05   9.167368E-06
  a1   70  -4.479801E-05  -1.795310E-05
  a1   71   5.212104E-05   5.296349E-05
  a1   72  -3.780339E-05  -1.687260E-05
  a1   73   1.472136E-05   6.553787E-06
  a1   74  -4.676315E-05  -3.031169E-05
  a1   75   2.841469E-05  -5.632534E-06
  a1   76  -4.922232E-06  -1.063141E-05
  a1   77   1.408130E-05   6.194459E-07
  a1   78  -4.554605E-07  -5.869496E-05
  a1   79   1.066788E-04   1.505308E-05
  a1   80  -9.363616E-05   2.783967E-05
  a1   81   6.415169E-04   7.481103E-06
  a1   82   7.481103E-06   2.894671E-04

Natural orbital populations,block 1
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000
              MO     9       MO    10       MO    11       MO    12       MO    13       MO    14       MO    15       MO    16
  occ(*)=     1.99161661     1.99111736     1.99058662     1.98912213     1.98756190     1.98608954     1.98597754     1.98504816
              MO    17       MO    18       MO    19       MO    20       MO    21       MO    22       MO    23       MO    24
  occ(*)=     1.98434864     1.98371318     1.98134111     1.97978584     0.01224968     0.01188740     0.01137256     0.00997704
              MO    25       MO    26       MO    27       MO    28       MO    29       MO    30       MO    31       MO    32
  occ(*)=     0.00941245     0.00883121     0.00809855     0.00765144     0.00698626     0.00653667     0.00596986     0.00489679
              MO    33       MO    34       MO    35       MO    36       MO    37       MO    38       MO    39       MO    40
  occ(*)=     0.00473167     0.00430373     0.00377509     0.00360671     0.00349564     0.00341393     0.00275841     0.00263195
              MO    41       MO    42       MO    43       MO    44       MO    45       MO    46       MO    47       MO    48
  occ(*)=     0.00253056     0.00222098     0.00210229     0.00161434     0.00149011     0.00145215     0.00137966     0.00117235
              MO    49       MO    50       MO    51       MO    52       MO    53       MO    54       MO    55       MO    56
  occ(*)=     0.00112602     0.00102646     0.00097821     0.00095122     0.00089626     0.00080231     0.00071728     0.00064711
              MO    57       MO    58       MO    59       MO    60       MO    61       MO    62       MO    63       MO    64
  occ(*)=     0.00058311     0.00057287     0.00055277     0.00050226     0.00048215     0.00042045     0.00037399     0.00035750
              MO    65       MO    66       MO    67       MO    68       MO    69       MO    70       MO    71       MO    72
  occ(*)=     0.00034681     0.00027251     0.00025050     0.00024251     0.00020418     0.00018726     0.00017716     0.00011174
              MO    73       MO    74       MO    75       MO    76       MO    77       MO    78       MO    79       MO    80
  occ(*)=     0.00010273     0.00009581     0.00008103     0.00007456     0.00007185     0.00007059     0.00005594     0.00004864
              MO    81       MO    82
  occ(*)=     0.00003883     0.00002497

          modens reordered block   1

               b1    1        b1    2        b1    3        b1    4        b1    5        b1    6        b1    7        b1    8
  b1    1    1.98569       4.626013E-04   2.185309E-04  -3.778999E-04   3.524164E-04   8.150715E-04  -1.521475E-04   4.010226E-05
  b1    2   4.626013E-04    1.97958       7.845072E-04   6.459229E-03   4.950433E-03   1.535167E-02   2.829899E-03  -9.739918E-04
  b1    3   2.185309E-04   7.845072E-04    1.97839      -1.277178E-03   4.894395E-04   2.505667E-03   2.695366E-03   9.112069E-04
  b1    4  -3.778999E-04   6.459229E-03  -1.277178E-03    1.92922       2.965222E-02  -1.545046E-02  -1.282222E-03   1.273501E-03
  b1    5   3.524164E-04   4.950433E-03   4.894395E-04   2.965222E-02    1.22045       1.667453E-02   7.199163E-03  -3.645239E-03
  b1    6   8.150715E-04   1.535167E-02   2.505667E-03  -1.545046E-02   1.667453E-02   0.276455       2.670941E-04   2.473681E-03
  b1    7  -1.521475E-04   2.829899E-03   2.695366E-03  -1.282222E-03   7.199163E-03   2.670941E-04   1.854265E-02   1.904843E-04
  b1    8   4.010226E-05  -9.739918E-04   9.112069E-04   1.273501E-03  -3.645239E-03   2.473681E-03   1.904843E-04   1.485014E-02
  b1    9   3.421855E-03  -1.428300E-03  -1.027507E-03  -5.802634E-05  -1.419252E-05   8.342292E-04   2.061614E-04  -4.842343E-05
  b1   10   1.940847E-03  -8.513844E-05  -1.329136E-04  -1.888651E-04   3.105012E-04   1.001058E-03   2.040995E-04  -6.047200E-05
  b1   11   1.774595E-04  -7.372520E-04   1.394382E-03   5.325610E-04   2.906349E-03   1.616654E-03   2.195780E-04   4.838410E-04
  b1   12  -7.568766E-04  -7.375332E-04   1.352988E-03  -6.488649E-04  -5.307671E-05  -2.126626E-03  -5.227097E-04   6.487953E-04
  b1   13  -1.560220E-03  -7.363091E-04   3.548668E-04  -1.406730E-03   4.165092E-05  -1.060728E-03  -6.704229E-05  -1.142478E-04
  b1   14   2.561149E-03  -6.973926E-04   4.423025E-04  -1.701539E-03  -2.401449E-03   3.151115E-05   9.945355E-04  -2.304632E-04
  b1   15   1.834618E-03  -8.560662E-04   7.995056E-05   8.533210E-04   2.762644E-03  -2.000178E-03  -7.531331E-04  -2.268674E-04
  b1   16  -1.085833E-03   6.189753E-05   2.229507E-03   1.433302E-03   8.596449E-04  -1.804552E-03   7.549479E-04  -8.486486E-04
  b1   17  -1.227676E-04   1.039045E-04  -8.841245E-04  -2.332303E-04   2.004798E-03   6.178995E-05   1.206389E-03  -8.675782E-05
  b1   18   1.442931E-04  -6.609728E-04  -5.261837E-05  -1.919600E-03   3.947997E-03   2.203562E-03  -1.986130E-04  -5.681735E-04
  b1   19  -2.566498E-04   1.888050E-04   9.013460E-04  -6.648574E-05  -5.825140E-04  -7.261919E-04  -9.164663E-04  -5.462411E-04
  b1   20   3.019040E-04  -2.752507E-04  -7.476737E-04   4.909807E-04  -3.548834E-03  -1.508711E-04  -1.627853E-04  -4.050477E-04
  b1   21   1.578368E-04   6.344799E-04   4.824272E-04  -2.976555E-04  -1.670995E-03  -3.052113E-04  -2.084519E-05   1.720031E-04
  b1   22   5.186440E-05   1.149596E-03   1.660953E-03   1.890891E-03  -1.526224E-03  -1.220741E-03  -5.892647E-04   2.282957E-04
  b1   23   3.368924E-04   6.838852E-05   1.464276E-04   2.645922E-04  -4.973223E-04  -1.763512E-04  -3.796106E-05   3.961358E-05
  b1   24   4.203688E-04  -5.741212E-04   1.366923E-05  -1.865056E-04   1.753575E-04  -4.841529E-04  -4.727620E-04  -4.159399E-04
  b1   25  -6.161989E-04   1.417021E-04   9.583939E-05  -5.134752E-04   6.334561E-04   4.467889E-04   6.377390E-04  -5.789965E-04
  b1   26   2.523857E-03  -6.851143E-04  -7.919942E-04  -8.295505E-04   5.007202E-04  -2.986054E-06  -7.246313E-05   6.333510E-05
  b1   27   1.072671E-03  -3.163949E-04  -6.930400E-05  -2.671842E-04  -9.557400E-04  -3.454430E-04  -3.245894E-04  -9.285423E-05
  b1   28  -1.254049E-03   1.424024E-04   2.403000E-04  -2.601306E-04   3.391164E-05   1.914202E-04  -2.613297E-04  -3.535506E-04
  b1   29  -4.200476E-04  -1.042252E-04  -9.485753E-05   2.542579E-04  -2.943101E-03   5.814144E-04   2.167665E-05  -3.130417E-05
  b1   30   1.109765E-03  -2.896126E-04  -2.859968E-04  -5.642073E-04  -1.412650E-04   4.124449E-04  -1.656947E-04  -3.720244E-04
  b1   31   1.096664E-04   3.450544E-04   3.666529E-04   5.255156E-04  -5.742997E-04  -2.041123E-04  -9.199383E-06   8.357264E-05
  b1   32  -9.846798E-04   2.688609E-04   7.460383E-04   5.934729E-04  -5.065226E-04  -3.754127E-04  -4.479155E-04  -1.189975E-04
  b1   33   1.529924E-03  -3.702058E-04  -3.067131E-04  -3.997095E-04   1.853577E-04   1.617599E-04   9.434018E-07  -1.392574E-04
  b1   34   7.182043E-04   6.190690E-05   7.525327E-05   1.185078E-04  -3.872696E-04  -4.168012E-04  -6.216031E-05   2.195803E-04
  b1   35   2.330258E-04  -1.358456E-05   1.046639E-04   3.393096E-04   7.256263E-05   2.896554E-05  -2.826381E-04  -2.457260E-04

               b1    9        b1   10        b1   11        b1   12        b1   13        b1   14        b1   15        b1   16
  b1    1   3.421855E-03   1.940847E-03   1.774595E-04  -7.568766E-04  -1.560220E-03   2.561149E-03   1.834618E-03  -1.085833E-03
  b1    2  -1.428300E-03  -8.513844E-05  -7.372520E-04  -7.375332E-04  -7.363091E-04  -6.973926E-04  -8.560662E-04   6.189753E-05
  b1    3  -1.027507E-03  -1.329136E-04   1.394382E-03   1.352988E-03   3.548668E-04   4.423025E-04   7.995056E-05   2.229507E-03
  b1    4  -5.802634E-05  -1.888651E-04   5.325610E-04  -6.488649E-04  -1.406730E-03  -1.701539E-03   8.533210E-04   1.433302E-03
  b1    5  -1.419252E-05   3.105012E-04   2.906349E-03  -5.307671E-05   4.165092E-05  -2.401449E-03   2.762644E-03   8.596449E-04
  b1    6   8.342292E-04   1.001058E-03   1.616654E-03  -2.126626E-03  -1.060728E-03   3.151115E-05  -2.000178E-03  -1.804552E-03
  b1    7   2.061614E-04   2.040995E-04   2.195780E-04  -5.227097E-04  -6.704229E-05   9.945355E-04  -7.531331E-04   7.549479E-04
  b1    8  -4.842343E-05  -6.047200E-05   4.838410E-04   6.487953E-04  -1.142478E-04  -2.304632E-04  -2.268674E-04  -8.486486E-04
  b1    9   3.929739E-03   6.136971E-04   1.880385E-05  -2.336070E-04  -1.460300E-03   2.241952E-03   2.022878E-03  -1.266473E-03
  b1   10   6.136971E-04   3.319121E-03   7.579125E-05  -2.994370E-04  -2.889751E-04   7.196499E-04   4.027767E-04  -4.151103E-04
  b1   11   1.880385E-05   7.579125E-05   2.851814E-03  -3.737100E-04  -1.876578E-04   1.733042E-05   8.007117E-05   9.460499E-05
  b1   12  -2.336070E-04  -2.994370E-04  -3.737100E-04   2.904547E-03   1.368780E-04  -4.530663E-04  -1.444842E-04   3.756978E-04
  b1   13  -1.460300E-03  -2.889751E-04  -1.876578E-04   1.368780E-04   2.583728E-03  -4.826228E-04  -4.639128E-04   3.846935E-04
  b1   14   2.241952E-03   7.196499E-04   1.733042E-05  -4.530663E-04  -4.826228E-04   3.382686E-03   7.639134E-04  -8.781922E-04
  b1   15   2.022878E-03   4.027767E-04   8.007117E-05  -1.444842E-04  -4.639128E-04   7.639134E-04   2.681566E-03  -2.165223E-04
  b1   16  -1.266473E-03  -4.151103E-04   9.460499E-05   3.756978E-04   3.846935E-04  -8.781922E-04  -2.165223E-04   1.892367E-03
  b1   17   8.510953E-05   3.662691E-04  -1.344938E-04  -3.459388E-04   5.036488E-05   1.320897E-04   8.197783E-05  -7.255845E-04
  b1   18   6.815150E-04   6.208710E-04   3.689157E-04  -1.438879E-04   4.784419E-04   7.730806E-04  -3.829543E-05  -7.679650E-04
  b1   19  -3.004246E-04  -1.397343E-04   3.928476E-04   5.066176E-04  -2.655977E-04   3.806967E-04  -6.794002E-04  -1.916590E-04
  b1   20  -2.865520E-04  -1.750160E-04   3.790014E-04   4.892155E-04  -9.077708E-05  -2.494214E-04  -6.580404E-06  -6.286456E-05
  b1   21  -3.342349E-04  -1.071615E-04  -3.585355E-05  -9.142159E-05  -4.742395E-05  -5.943513E-05   7.727881E-06   1.926525E-04
  b1   22  -4.847023E-04  -8.268040E-04  -3.851053E-05   1.653886E-05  -1.659272E-04  -1.103380E-06  -1.232741E-04   2.090272E-04
  b1   23  -1.416955E-04  -6.481091E-05  -7.597062E-06   2.005017E-05  -2.781683E-05  -2.438659E-05  -4.160529E-05   8.508625E-05
  b1   24  -3.144635E-05   1.182118E-04   9.458692E-05  -7.778546E-06  -3.792211E-04  -1.802253E-04  -3.000530E-04  -2.601918E-04
  b1   25   1.908970E-04  -1.265502E-05   3.850447E-05   1.954428E-04   2.413423E-04  -1.806086E-04   1.634758E-04  -5.252300E-04
  b1   26  -1.030979E-04   1.169201E-03  -8.889204E-05   1.066949E-04   1.200357E-05  -8.343765E-05  -1.149206E-04   1.076494E-04
  b1   27   1.083657E-05   1.471293E-04  -1.529961E-04   4.204717E-05  -4.362909E-05   2.216866E-04   3.423183E-05  -1.321931E-04
  b1   28   2.317477E-05  -3.037334E-04  -7.610025E-05   4.890411E-05  -3.555190E-06  -1.055756E-04  -9.334415E-05   2.141776E-05
  b1   29   3.760796E-06  -1.061148E-05  -7.665252E-05  -1.179896E-04   4.976942E-05   1.910555E-05  -1.366782E-04  -2.990578E-05
  b1   30  -7.151571E-06   2.256202E-04  -3.003945E-05  -1.273688E-04   7.577077E-05   9.462887E-05   1.069858E-04  -1.500976E-04
  b1   31  -5.321798E-05  -8.401583E-05  -1.796754E-05   1.208431E-04  -1.472739E-04  -7.403664E-05  -2.393087E-05   1.094475E-04
  b1   32  -6.600083E-05  -8.883404E-05  -7.283735E-05   4.020749E-05   2.187978E-05  -1.801216E-04  -6.568195E-05   2.945584E-04
  b1   33   2.097615E-04  -2.244949E-05   2.593762E-05  -1.576667E-04  -2.952104E-04   4.111750E-04   2.825321E-04  -9.095787E-05
  b1   34   4.027616E-04   1.802225E-05   4.491298E-05  -6.222661E-05  -2.409028E-04   4.521538E-04   3.074386E-04  -1.328535E-04
  b1   35   1.643136E-04   1.344222E-05  -5.556535E-06  -9.073955E-05  -1.558637E-04   1.106798E-05   2.488471E-04  -8.747517E-05

               b1   17        b1   18        b1   19        b1   20        b1   21        b1   22        b1   23        b1   24
  b1    1  -1.227676E-04   1.442931E-04  -2.566498E-04   3.019040E-04   1.578368E-04   5.186440E-05   3.368924E-04   4.203688E-04
  b1    2   1.039045E-04  -6.609728E-04   1.888050E-04  -2.752507E-04   6.344799E-04   1.149596E-03   6.838852E-05  -5.741212E-04
  b1    3  -8.841245E-04  -5.261837E-05   9.013460E-04  -7.476737E-04   4.824272E-04   1.660953E-03   1.464276E-04   1.366923E-05
  b1    4  -2.332303E-04  -1.919600E-03  -6.648574E-05   4.909807E-04  -2.976555E-04   1.890891E-03   2.645922E-04  -1.865056E-04
  b1    5   2.004798E-03   3.947997E-03  -5.825140E-04  -3.548834E-03  -1.670995E-03  -1.526224E-03  -4.973223E-04   1.753575E-04
  b1    6   6.178995E-05   2.203562E-03  -7.261919E-04  -1.508711E-04  -3.052113E-04  -1.220741E-03  -1.763512E-04  -4.841529E-04
  b1    7   1.206389E-03  -1.986130E-04  -9.164663E-04  -1.627853E-04  -2.084519E-05  -5.892647E-04  -3.796106E-05  -4.727620E-04
  b1    8  -8.675782E-05  -5.681735E-04  -5.462411E-04  -4.050477E-04   1.720031E-04   2.282957E-04   3.961358E-05  -4.159399E-04
  b1    9   8.510953E-05   6.815150E-04  -3.004246E-04  -2.865520E-04  -3.342349E-04  -4.847023E-04  -1.416955E-04  -3.144635E-05
  b1   10   3.662691E-04   6.208710E-04  -1.397343E-04  -1.750160E-04  -1.071615E-04  -8.268040E-04  -6.481091E-05   1.182118E-04
  b1   11  -1.344938E-04   3.689157E-04   3.928476E-04   3.790014E-04  -3.585355E-05  -3.851053E-05  -7.597062E-06   9.458692E-05
  b1   12  -3.459388E-04  -1.438879E-04   5.066176E-04   4.892155E-04  -9.142159E-05   1.653886E-05   2.005017E-05  -7.778546E-06
  b1   13   5.036488E-05   4.784419E-04  -2.655977E-04  -9.077708E-05  -4.742395E-05  -1.659272E-04  -2.781683E-05  -3.792211E-04
  b1   14   1.320897E-04   7.730806E-04   3.806967E-04  -2.494214E-04  -5.943513E-05  -1.103380E-06  -2.438659E-05  -1.802253E-04
  b1   15   8.197783E-05  -3.829543E-05  -6.794002E-04  -6.580404E-06   7.727881E-06  -1.232741E-04  -4.160529E-05  -3.000530E-04
  b1   16  -7.255845E-04  -7.679650E-04  -1.916590E-04  -6.286456E-05   1.926525E-04   2.090272E-04   8.508625E-05  -2.601918E-04
  b1   17   3.040248E-03   9.288216E-04   6.079584E-04  -2.091674E-04  -8.880905E-05  -9.776503E-05  -3.977460E-05  -7.647366E-05
  b1   18   9.288216E-04   3.057721E-03  -5.064239E-04  -3.054772E-04  -2.229454E-04  -2.433946E-04  -8.524794E-05  -8.009045E-05
  b1   19   6.079584E-04  -5.064239E-04   2.661413E-03  -1.579995E-04  -1.885473E-04   1.133634E-04   4.352073E-05  -4.360511E-04
  b1   20  -2.091674E-04  -3.054772E-04  -1.579995E-04   3.251878E-03   6.617104E-05  -1.321636E-04   6.245427E-05  -1.927927E-04
  b1   21  -8.880905E-05  -2.229454E-04  -1.885473E-04   6.617104E-05   3.079120E-03  -1.573270E-04   5.305636E-05   5.167053E-05
  b1   22  -9.776503E-05  -2.433946E-04   1.133634E-04  -1.321636E-04  -1.573270E-04   1.514285E-03   6.011620E-05   2.824262E-05
  b1   23  -3.977460E-05  -8.524794E-05   4.352073E-05   6.245427E-05   5.305636E-05   6.011620E-05   7.973122E-04  -2.755771E-05
  b1   24  -7.647366E-05  -8.009045E-05  -4.360511E-04  -1.927927E-04   5.167053E-05   2.824262E-05  -2.755771E-05   1.592062E-03
  b1   25   9.427971E-05  -4.490405E-05   1.899620E-04  -4.161499E-04   1.025157E-05   1.976840E-05  -6.800172E-05  -6.679016E-05
  b1   26   9.387438E-05   1.305185E-04  -4.926636E-05   4.033289E-05   6.556704E-05  -2.308939E-04   3.880137E-06   6.188472E-05
  b1   27   3.009378E-04   2.275275E-04  -1.347103E-04  -1.465651E-05   2.503584E-04  -6.002814E-05  -7.825033E-06   2.009837E-04
  b1   28  -6.690495E-05  -4.048891E-04  -3.861766E-05  -7.702312E-05   2.138677E-04   6.010683E-05  -5.294570E-06   2.484518E-04
  b1   29  -5.735642E-05  -1.367420E-04  -3.519999E-05   1.889110E-04   8.603948E-05  -5.280122E-05   2.940478E-06   4.027512E-05
  b1   30   1.685430E-04   4.411705E-05  -1.399753E-04  -2.720884E-04  -1.844794E-04   4.673998E-05  -2.556969E-05   7.381320E-05
  b1   31  -3.073644E-04  -5.015545E-04   2.766377E-04  -2.075056E-05  -2.716915E-05   5.170050E-06   4.447011E-06  -3.365663E-05
  b1   32  -1.010529E-05  -1.836669E-04  -2.013348E-04   6.827622E-05   1.640476E-04  -1.250067E-04   1.689885E-05   1.289153E-04
  b1   33  -1.017473E-06  -2.330720E-04   1.826376E-04  -2.318837E-04  -3.982167E-05   2.339139E-04   2.137428E-05  -9.782351E-05
  b1   34  -2.057044E-04   9.480278E-05  -7.117169E-05   2.488581E-04   4.448407E-05  -1.202198E-04  -1.918026E-05   1.364063E-05
  b1   35   1.985956E-04  -1.730791E-04  -2.423033E-04  -2.216849E-05   1.633676E-04  -5.846375E-05  -2.198334E-05   2.083161E-04

               b1   25        b1   26        b1   27        b1   28        b1   29        b1   30        b1   31        b1   32
  b1    1  -6.161989E-04   2.523857E-03   1.072671E-03  -1.254049E-03  -4.200476E-04   1.109765E-03   1.096664E-04  -9.846798E-04
  b1    2   1.417021E-04  -6.851143E-04  -3.163949E-04   1.424024E-04  -1.042252E-04  -2.896126E-04   3.450544E-04   2.688609E-04
  b1    3   9.583939E-05  -7.919942E-04  -6.930400E-05   2.403000E-04  -9.485753E-05  -2.859968E-04   3.666529E-04   7.460383E-04
  b1    4  -5.134752E-04  -8.295505E-04  -2.671842E-04  -2.601306E-04   2.542579E-04  -5.642073E-04   5.255156E-04   5.934729E-04
  b1    5   6.334561E-04   5.007202E-04  -9.557400E-04   3.391164E-05  -2.943101E-03  -1.412650E-04  -5.742997E-04  -5.065226E-04
  b1    6   4.467889E-04  -2.986054E-06  -3.454430E-04   1.914202E-04   5.814144E-04   4.124449E-04  -2.041123E-04  -3.754127E-04
  b1    7   6.377390E-04  -7.246313E-05  -3.245894E-04  -2.613297E-04   2.167665E-05  -1.656947E-04  -9.199383E-06  -4.479155E-04
  b1    8  -5.789965E-04   6.333510E-05  -9.285423E-05  -3.535506E-04  -3.130417E-05  -3.720244E-04   8.357264E-05  -1.189975E-04
  b1    9   1.908970E-04  -1.030979E-04   1.083657E-05   2.317477E-05   3.760796E-06  -7.151571E-06  -5.321798E-05  -6.600083E-05
  b1   10  -1.265502E-05   1.169201E-03   1.471293E-04  -3.037334E-04  -1.061148E-05   2.256202E-04  -8.401583E-05  -8.883404E-05
  b1   11   3.850447E-05  -8.889204E-05  -1.529961E-04  -7.610025E-05  -7.665252E-05  -3.003945E-05  -1.796754E-05  -7.283735E-05
  b1   12   1.954428E-04   1.066949E-04   4.204717E-05   4.890411E-05  -1.179896E-04  -1.273688E-04   1.208431E-04   4.020749E-05
  b1   13   2.413423E-04   1.200357E-05  -4.362909E-05  -3.555190E-06   4.976942E-05   7.577077E-05  -1.472739E-04   2.187978E-05
  b1   14  -1.806086E-04  -8.343765E-05   2.216866E-04  -1.055756E-04   1.910555E-05   9.462887E-05  -7.403664E-05  -1.801216E-04
  b1   15   1.634758E-04  -1.149206E-04   3.423183E-05  -9.334415E-05  -1.366782E-04   1.069858E-04  -2.393087E-05  -6.568195E-05
  b1   16  -5.252300E-04   1.076494E-04  -1.321931E-04   2.141776E-05  -2.990578E-05  -1.500976E-04   1.094475E-04   2.945584E-04
  b1   17   9.427971E-05   9.387438E-05   3.009378E-04  -6.690495E-05  -5.735642E-05   1.685430E-04  -3.073644E-04  -1.010529E-05
  b1   18  -4.490405E-05   1.305185E-04   2.275275E-04  -4.048891E-04  -1.367420E-04   4.411705E-05  -5.015545E-04  -1.836669E-04
  b1   19   1.899620E-04  -4.926636E-05  -1.347103E-04  -3.861766E-05  -3.519999E-05  -1.399753E-04   2.766377E-04  -2.013348E-04
  b1   20  -4.161499E-04   4.033289E-05  -1.465651E-05  -7.702312E-05   1.889110E-04  -2.720884E-04  -2.075056E-05   6.827622E-05
  b1   21   1.025157E-05   6.556704E-05   2.503584E-04   2.138677E-04   8.603948E-05  -1.844794E-04  -2.716915E-05   1.640476E-04
  b1   22   1.976840E-05  -2.308939E-04  -6.002814E-05   6.010683E-05  -5.280122E-05   4.673998E-05   5.170050E-06  -1.250067E-04
  b1   23  -6.800172E-05   3.880137E-06  -7.825033E-06  -5.294570E-06   2.940478E-06  -2.556969E-05   4.447011E-06   1.689885E-05
  b1   24  -6.679016E-05   6.188472E-05   2.009837E-04   2.484518E-04   4.027512E-05   7.381320E-05  -3.365663E-05   1.289153E-04
  b1   25   1.341444E-03  -1.157531E-04  -6.668010E-05   1.866091E-04  -5.167825E-05   3.008195E-04   6.831608E-05  -3.266264E-04
  b1   26  -1.157531E-04   1.053535E-03   1.438506E-04  -1.805934E-04  -6.375308E-05   8.342811E-05   6.950269E-06   5.795672E-05
  b1   27  -6.668010E-05   1.438506E-04   8.094930E-04   4.048694E-05   1.614392E-05   1.575392E-04  -1.352136E-04   2.084946E-04
  b1   28   1.866091E-04  -1.805934E-04   4.048694E-05   9.885819E-04   4.883215E-06   2.123336E-04   3.246775E-05   1.908124E-04
  b1   29  -5.167825E-05  -6.375308E-05   1.614392E-05   4.883215E-06   9.340959E-04   6.534626E-05   3.528097E-05   3.745486E-05
  b1   30   3.008195E-04   8.342811E-05   1.575392E-04   2.123336E-04   6.534626E-05   8.598309E-04  -7.083163E-05   5.575622E-05
  b1   31   6.831608E-05   6.950269E-06  -1.352136E-04   3.246775E-05   3.528097E-05  -7.083163E-05   4.021307E-04  -7.298084E-05
  b1   32  -3.266264E-04   5.795672E-05   2.084946E-04   1.908124E-04   3.745486E-05   5.575622E-05  -7.298084E-05   7.807031E-04
  b1   33   8.398965E-05  -1.802748E-04  -1.027654E-04   2.052752E-04   1.145349E-05   1.424098E-04   7.126112E-05  -1.341294E-04
  b1   34  -2.170738E-04  -1.290349E-04   1.062689E-04  -1.617453E-04  -5.569045E-06  -1.992310E-04   5.568163E-06   8.071115E-05
  b1   35   2.175471E-05  -4.811912E-05   1.571668E-04   1.238364E-04   2.532793E-06   1.423906E-04  -5.630559E-05   2.772802E-04

               b1   33        b1   34        b1   35
  b1    1   1.529924E-03   7.182043E-04   2.330258E-04
  b1    2  -3.702058E-04   6.190690E-05  -1.358456E-05
  b1    3  -3.067131E-04   7.525327E-05   1.046639E-04
  b1    4  -3.997095E-04   1.185078E-04   3.393096E-04
  b1    5   1.853577E-04  -3.872696E-04   7.256263E-05
  b1    6   1.617599E-04  -4.168012E-04   2.896554E-05
  b1    7   9.434018E-07  -6.216031E-05  -2.826381E-04
  b1    8  -1.392574E-04   2.195803E-04  -2.457260E-04
  b1    9   2.097615E-04   4.027616E-04   1.643136E-04
  b1   10  -2.244949E-05   1.802225E-05   1.344222E-05
  b1   11   2.593762E-05   4.491298E-05  -5.556535E-06
  b1   12  -1.576667E-04  -6.222661E-05  -9.073955E-05
  b1   13  -2.952104E-04  -2.409028E-04  -1.558637E-04
  b1   14   4.111750E-04   4.521538E-04   1.106798E-05
  b1   15   2.825321E-04   3.074386E-04   2.488471E-04
  b1   16  -9.095787E-05  -1.328535E-04  -8.747517E-05
  b1   17  -1.017473E-06  -2.057044E-04   1.985956E-04
  b1   18  -2.330720E-04   9.480278E-05  -1.730791E-04
  b1   19   1.826376E-04  -7.117169E-05  -2.423033E-04
  b1   20  -2.318837E-04   2.488581E-04  -2.216849E-05
  b1   21  -3.982167E-05   4.448407E-05   1.633676E-04
  b1   22   2.339139E-04  -1.202198E-04  -5.846375E-05
  b1   23   2.137428E-05  -1.918026E-05  -2.198334E-05
  b1   24  -9.782351E-05   1.364063E-05   2.083161E-04
  b1   25   8.398965E-05  -2.170738E-04   2.175471E-05
  b1   26  -1.802748E-04  -1.290349E-04  -4.811912E-05
  b1   27  -1.027654E-04   1.062689E-04   1.571668E-04
  b1   28   2.052752E-04  -1.617453E-04   1.238364E-04
  b1   29   1.145349E-05  -5.569045E-06   2.532793E-06
  b1   30   1.424098E-04  -1.992310E-04   1.423906E-04
  b1   31   7.126112E-05   5.568163E-06  -5.630559E-05
  b1   32  -1.341294E-04   8.071115E-05   2.772802E-04
  b1   33   6.852713E-04  -1.905352E-04   1.758165E-05
  b1   34  -1.905352E-04   6.446926E-04  -5.225767E-05
  b1   35   1.758165E-05  -5.225767E-05   6.848715E-04

Natural orbital populations,block 2
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     1.98575729     1.98073999     1.97826770     1.92970845     1.21961254     0.27598212     0.01886900     0.01504091
              MO     9       MO    10       MO    11       MO    12       MO    13       MO    14       MO    15       MO    16
  occ(*)=     0.00863067     0.00486157     0.00402086     0.00373820     0.00339407     0.00327298     0.00311844     0.00276560
              MO    17       MO    18       MO    19       MO    20       MO    21       MO    22       MO    23       MO    24
  occ(*)=     0.00249258     0.00237129     0.00198317     0.00163220     0.00150266     0.00101565     0.00088432     0.00084708
              MO    25       MO    26       MO    27       MO    28       MO    29       MO    30       MO    31       MO    32
  occ(*)=     0.00078045     0.00067707     0.00057687     0.00053354     0.00042526     0.00033692     0.00029132     0.00027107
              MO    33       MO    34       MO    35
  occ(*)=     0.00022655     0.00019507     0.00007466

          modens reordered block   1

               b2    1        b2    2        b2    3        b2    4        b2    5        b2    6        b2    7        b2    8
  b2    1    4.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  b2    2    0.00000        4.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  b2    3    0.00000        0.00000        4.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  b2    4    0.00000        0.00000        0.00000        1.98940      -4.034625E-05   1.340203E-04   9.020851E-04   9.195834E-04
  b2    5    0.00000        0.00000        0.00000      -4.034625E-05    1.98893       1.714342E-04   1.340651E-03   1.140124E-03
  b2    6    0.00000        0.00000        0.00000       1.340203E-04   1.714342E-04    1.98650      -1.575855E-04  -2.899003E-05
  b2    7    0.00000        0.00000        0.00000       9.020851E-04   1.340651E-03  -1.575855E-04    1.98272      -7.518401E-05
  b2    8    0.00000        0.00000        0.00000       9.195834E-04   1.140124E-03  -2.899003E-05  -7.518401E-05    1.98113    
  b2    9    0.00000        0.00000        0.00000      -1.164403E-03   4.971914E-04   1.396451E-04   3.076563E-04  -3.592370E-04
  b2   10    0.00000        0.00000        0.00000       1.921421E-06   7.970211E-04  -5.901686E-04  -6.577207E-04  -2.865543E-04
  b2   11    0.00000        0.00000        0.00000      -1.425713E-04  -2.193055E-04  -1.654053E-03   5.555788E-04   6.932251E-04
  b2   12    0.00000        0.00000        0.00000      -1.011884E-06  -6.443145E-05   4.890332E-04  -1.328389E-04  -2.417100E-04
  b2   13    0.00000        0.00000        0.00000      -5.993149E-04   3.357223E-04   1.724403E-03  -1.823276E-04   8.022082E-05
  b2   14    0.00000        0.00000        0.00000       3.964832E-04  -6.457081E-04   3.437757E-03  -2.205043E-03   3.291951E-03
  b2   15    0.00000        0.00000        0.00000       1.480988E-04  -1.705441E-04   1.720891E-03  -3.663254E-03   3.881821E-03
  b2   16    0.00000        0.00000        0.00000      -5.522836E-04  -3.228725E-05   1.688432E-03   1.374354E-03  -2.259823E-04
  b2   17    0.00000        0.00000        0.00000       8.846721E-04  -3.744612E-04  -3.573408E-04  -1.242166E-03   2.409964E-03
  b2   18    0.00000        0.00000        0.00000       6.351107E-04  -8.500513E-05  -1.120370E-04  -3.772889E-04   1.223627E-03
  b2   19    0.00000        0.00000        0.00000      -1.468728E-03   5.316011E-04   5.603463E-04   1.622932E-03  -1.442845E-03
  b2   20    0.00000        0.00000        0.00000       1.177444E-03   6.869356E-05  -3.210293E-03  -2.453741E-03   1.387234E-03
  b2   21    0.00000        0.00000        0.00000      -4.147393E-04  -3.790247E-04   1.655607E-03   1.091261E-03  -1.121430E-04
  b2   22    0.00000        0.00000        0.00000       4.660776E-05  -1.560501E-04   3.216302E-05  -2.842771E-03   3.585352E-03
  b2   23    0.00000        0.00000        0.00000       3.159027E-04   1.078993E-04  -3.017933E-04  -2.304668E-03   2.380727E-03
  b2   24    0.00000        0.00000        0.00000       3.830397E-05   2.104766E-04  -1.420875E-03   5.439123E-04  -7.829725E-04
  b2   25    0.00000        0.00000        0.00000      -2.251044E-04   1.123482E-04  -8.239516E-04   1.267924E-03  -2.673761E-03
  b2   26    0.00000        0.00000        0.00000       3.874332E-05   2.518346E-04  -8.805830E-04   1.267922E-03   3.673576E-04
  b2   27    0.00000        0.00000        0.00000       1.725907E-04   1.412632E-05   1.553734E-03   7.287342E-04   1.626956E-03
  b2   28    0.00000        0.00000        0.00000       2.426119E-05   4.089680E-04   1.756433E-04  -8.125011E-04   1.226313E-03
  b2   29    0.00000        0.00000        0.00000       3.857024E-04  -1.710739E-04   1.266493E-04   2.751754E-04   9.928248E-04
  b2   30    0.00000        0.00000        0.00000      -9.860057E-05   2.901580E-04  -2.108192E-03   7.045117E-04   1.218436E-03
  b2   31    0.00000        0.00000        0.00000       2.583004E-06   3.847617E-04  -5.022935E-04   8.950795E-04  -8.002069E-04
  b2   32    0.00000        0.00000        0.00000      -2.025528E-04  -1.990122E-04   8.156802E-04   8.421819E-04  -4.699549E-04
  b2   33    0.00000        0.00000        0.00000      -1.060973E-03   2.587771E-04  -7.400714E-04   1.741412E-03  -1.351342E-03
  b2   34    0.00000        0.00000        0.00000      -1.406425E-04  -3.427180E-04   1.385423E-03   8.718580E-04   1.181928E-04
  b2   35    0.00000        0.00000        0.00000       1.936066E-04   6.412503E-04  -8.356742E-04  -6.436927E-04  -1.571812E-03
  b2   36    0.00000        0.00000        0.00000       2.506617E-04   2.998531E-04  -5.193095E-04   6.594839E-05  -1.817701E-05
  b2   37    0.00000        0.00000        0.00000      -3.350269E-04  -3.951545E-04   1.785628E-04  -3.591816E-05   1.581115E-04
  b2   38    0.00000        0.00000        0.00000      -3.672554E-04  -7.571211E-04   2.148668E-06  -5.010521E-05  -6.761944E-04
  b2   39    0.00000        0.00000        0.00000      -3.755442E-05   5.822054E-05   3.584631E-04   1.620646E-03  -1.290580E-03
  b2   40    0.00000        0.00000        0.00000      -1.663346E-04   2.399899E-04   1.243480E-03   1.086682E-03  -5.985630E-04
  b2   41    0.00000        0.00000        0.00000       2.560658E-04  -5.677015E-04  -7.351343E-04   2.881224E-03  -4.216470E-03
  b2   42    0.00000        0.00000        0.00000      -3.778377E-04   5.577865E-04  -1.102861E-03  -1.837713E-05  -1.293031E-05
  b2   43    0.00000        0.00000        0.00000       1.285220E-04   2.076560E-04  -5.134471E-04  -9.343267E-04  -1.312804E-03
  b2   44    0.00000        0.00000        0.00000      -5.212634E-04   4.370404E-05  -1.163243E-04   3.033250E-03   6.106337E-04
  b2   45    0.00000        0.00000        0.00000      -3.384653E-04  -3.903113E-04   9.737899E-04   9.535089E-04   8.597849E-04
  b2   46    0.00000        0.00000        0.00000      -4.186618E-04   3.618989E-04   1.439106E-03  -6.790379E-04  -2.780926E-03
  b2   47    0.00000        0.00000        0.00000      -2.877785E-04   8.610490E-04  -1.203478E-03  -1.423718E-03  -1.766619E-03
  b2   48    0.00000        0.00000        0.00000       6.461823E-04  -4.409085E-04  -1.826516E-04  -3.850268E-04   6.272110E-04
  b2   49    0.00000        0.00000        0.00000       3.224042E-04  -2.470112E-04   8.741599E-05   4.526586E-04  -2.175831E-04
  b2   50    0.00000        0.00000        0.00000      -2.222979E-04  -4.919337E-04   1.520543E-03   4.484754E-04  -7.506817E-04
  b2   51    0.00000        0.00000        0.00000       9.654460E-04   5.594568E-04   1.681196E-04  -3.836945E-03  -2.712868E-03
  b2   52    0.00000        0.00000        0.00000       7.663560E-05   1.219718E-04   8.686073E-04  -1.585352E-04   6.808932E-04
  b2   53    0.00000        0.00000        0.00000       1.420762E-04  -5.691394E-04   2.040748E-04   9.210670E-04   3.814153E-04
  b2   54    0.00000        0.00000        0.00000      -1.017589E-03  -1.567464E-04   9.197919E-04   3.751878E-04   1.528181E-03
  b2   55    0.00000        0.00000        0.00000       6.826728E-04  -1.473350E-04   5.276920E-04   4.134056E-04  -8.712729E-04
  b2   56    0.00000        0.00000        0.00000       7.091343E-04   1.667509E-03   2.790006E-04   5.456238E-05  -3.986612E-04
  b2   57    0.00000        0.00000        0.00000       8.946178E-04  -1.004232E-04  -1.835322E-05   1.159603E-04   1.122315E-04
  b2   58    0.00000        0.00000        0.00000       8.460484E-04   2.746311E-04  -2.235864E-04  -6.823604E-04  -6.588219E-04
  b2   59    0.00000        0.00000        0.00000      -1.115960E-04   8.298642E-05  -1.287854E-03   8.100679E-05  -3.330688E-04
  b2   60    0.00000        0.00000        0.00000       3.253423E-04  -8.112945E-05  -7.807395E-04  -5.650131E-05  -4.076236E-05
  b2   61    0.00000        0.00000        0.00000       4.740233E-04  -7.815906E-04   5.640789E-04   1.976583E-04   5.025300E-04
  b2   62    0.00000        0.00000        0.00000      -6.482371E-05   6.210300E-04   7.032843E-04  -5.435773E-04  -7.798004E-04

               b2    9        b2   10        b2   11        b2   12        b2   13        b2   14        b2   15        b2   16
  b2    4  -1.164403E-03   1.921421E-06  -1.425713E-04  -1.011884E-06  -5.993149E-04   3.964832E-04   1.480988E-04  -5.522836E-04
  b2    5   4.971914E-04   7.970211E-04  -2.193055E-04  -6.443145E-05   3.357223E-04  -6.457081E-04  -1.705441E-04  -3.228725E-05
  b2    6   1.396451E-04  -5.901686E-04  -1.654053E-03   4.890332E-04   1.724403E-03   3.437757E-03   1.720891E-03   1.688432E-03
  b2    7   3.076563E-04  -6.577207E-04   5.555788E-04  -1.328389E-04  -1.823276E-04  -2.205043E-03  -3.663254E-03   1.374354E-03
  b2    8  -3.592370E-04  -2.865543E-04   6.932251E-04  -2.417100E-04   8.022082E-05   3.291951E-03   3.881821E-03  -2.259823E-04
  b2    9    1.98406      -4.065947E-04   3.595997E-04  -2.972810E-05   1.752464E-03  -1.720177E-03   7.372774E-04   7.724558E-04
  b2   10  -4.065947E-04    1.98382       1.021673E-04  -5.351275E-05   1.045646E-03  -9.532570E-04   1.938160E-04  -1.305624E-03
  b2   11   3.595997E-04   1.021673E-04    1.98144      -9.062979E-04  -8.163363E-04   1.764106E-03   1.463689E-03   3.368759E-03
  b2   12  -2.972810E-05  -5.351275E-05  -9.062979E-04    1.97678       4.988958E-04  -1.323163E-03   3.252764E-04   3.471514E-04
  b2   13   1.752464E-03   1.045646E-03  -8.163363E-04   4.988958E-04   3.331418E-03   8.601908E-04   1.009272E-03   2.025830E-04
  b2   14  -1.720177E-03  -9.532570E-04   1.764106E-03  -1.323163E-03   8.601908E-04   9.803865E-03   5.521545E-03   1.433174E-03
  b2   15   7.372774E-04   1.938160E-04   1.463689E-03   3.252764E-04   1.009272E-03   5.521545E-03   7.710789E-03   8.590765E-04
  b2   16   7.724558E-04  -1.305624E-03   3.368759E-03   3.471514E-04   2.025830E-04   1.433174E-03   8.590765E-04   4.023731E-03
  b2   17  -1.585752E-03   8.805044E-04   3.156177E-03   1.018771E-03   1.779437E-04   2.773965E-03   2.633269E-03  -4.501259E-05
  b2   18   3.404169E-04   1.763172E-05  -1.583302E-03   8.280077E-04   1.917679E-04  -7.540366E-04   7.984364E-04  -1.051146E-03
  b2   19   9.328812E-04   4.248530E-04  -1.286131E-03  -2.050577E-03  -5.785091E-04   7.172364E-06  -1.243323E-03  -3.153871E-06
  b2   20   3.511154E-04  -2.349949E-04  -4.029368E-03  -4.634836E-04  -6.165290E-04  -9.105440E-05   1.076327E-03  -1.454257E-03
  b2   21   9.993944E-04   7.143002E-04   4.370226E-03   6.059639E-03  -1.087701E-05   2.176700E-03   6.674408E-04   1.089712E-03
  b2   22  -1.732779E-03   7.177976E-05   2.294735E-03  -2.718970E-04   4.552936E-04  -4.104260E-04   2.830562E-04  -1.066384E-03
  b2   23   1.206980E-03   2.434177E-04   4.549276E-04   2.165686E-03  -9.493257E-04   1.207678E-03  -3.490829E-04  -8.982652E-04
  b2   24   4.648693E-04   1.905534E-03  -5.435868E-04   4.087313E-03   2.440932E-04   9.061614E-04   3.230435E-04  -1.585541E-03
  b2   25  -2.230551E-04   2.147560E-03  -1.437015E-03  -4.079601E-03   4.542551E-04   5.558300E-04  -3.541190E-04  -1.678617E-03
  b2   26   5.202367E-04  -5.971689E-04  -5.714472E-04  -4.957699E-03   1.165167E-03   9.208669E-04  -1.370229E-04   5.446477E-04
  b2   27  -6.677274E-04   1.906918E-03   7.603752E-04  -1.940591E-03   1.544230E-03   1.994658E-04  -6.234246E-04   1.021976E-04
  b2   28  -5.498847E-04   7.452438E-05  -2.806783E-04   7.385165E-03   1.840017E-03  -8.297678E-04   3.427828E-04  -5.226679E-04
  b2   29   1.719751E-04   4.015062E-05  -6.020512E-04   3.239299E-03  -2.986662E-04   1.900491E-05  -2.445275E-03  -4.259654E-04
  b2   30  -3.273328E-04  -1.041906E-03  -6.651315E-04   2.318223E-03  -1.264036E-04   3.215873E-04  -3.126261E-04   1.452321E-03
  b2   31   5.910819E-04  -8.597633E-04  -1.204092E-03  -1.037603E-03   8.495102E-04   3.894968E-04  -6.822182E-04   9.592399E-05
  b2   32   1.147705E-04   4.300277E-04   2.285574E-03   5.486687E-03  -4.734493E-04  -3.240737E-03  -2.456291E-03  -3.794293E-04
  b2   33  -1.145767E-05   9.871923E-05  -1.535780E-03  -8.462973E-04   3.963532E-04  -5.586579E-04  -9.827503E-07   2.481038E-04
  b2   34   3.916127E-04   3.668013E-04   1.135609E-03   7.494683E-05   2.718021E-04   1.562123E-03   1.212926E-03   6.892748E-04
  b2   35   2.312240E-04  -4.409505E-04  -1.112531E-03   4.680247E-04  -3.120751E-04  -1.697280E-04   1.254649E-04  -4.869703E-04
  b2   36   1.313956E-04  -3.427810E-04  -9.907096E-04   1.221087E-03  -2.584180E-06  -1.483471E-03  -1.164171E-03  -9.584787E-06
  b2   37   1.076783E-05  -4.080191E-04   1.242141E-03  -7.311008E-04  -5.344421E-05   2.493370E-04   3.537992E-04  -2.323675E-04
  b2   38  -1.963453E-04  -5.662941E-04  -6.014200E-04   8.381388E-04  -2.212076E-04  -2.587626E-04   6.231920E-05  -3.241852E-04
  b2   39  -5.990167E-04   1.997373E-04   1.300595E-03   1.077752E-03  -3.227505E-04  -3.709052E-04  -6.795670E-05  -9.629596E-05
  b2   40  -4.839181E-04   2.971406E-04  -4.540359E-04  -2.997523E-03   1.705700E-04   9.479598E-04   3.792317E-04   2.349831E-04
  b2   41  -1.220615E-03  -5.600488E-05  -2.674625E-04  -1.296098E-03  -6.445039E-05   1.854549E-04  -3.853040E-05   1.189853E-04
  b2   42  -4.189604E-04  -8.002062E-04  -2.100307E-03  -6.486858E-03   3.464256E-05   7.750353E-04   7.154969E-04  -2.099828E-05
  b2   43   2.964750E-03   1.461836E-03  -2.403595E-04  -1.307010E-04   1.826168E-04  -1.689323E-05   2.867244E-04   4.736311E-05
  b2   44   2.926203E-04  -1.038041E-03  -2.534924E-04   2.226107E-03   1.174470E-04  -1.027695E-04  -1.686674E-04   7.141977E-05
  b2   45   1.620256E-03  -1.977723E-03   3.982588E-04  -9.107908E-04   1.127933E-04   9.934989E-05   9.794894E-05   5.093700E-05
  b2   46  -1.078131E-03  -1.642403E-04   1.873796E-03  -1.498173E-04  -1.930941E-05  -1.891097E-06  -5.066512E-05  -2.953416E-06
  b2   47  -1.243116E-03  -8.389672E-04   5.942881E-04   2.129575E-03  -5.747787E-05  -4.104462E-05  -1.413425E-04  -2.136979E-04
  b2   48   1.055889E-03   5.990972E-04  -1.414101E-04   1.675421E-04  -1.033056E-06  -2.390635E-05   7.260529E-05   3.029200E-05
  b2   49   3.376468E-04   2.983349E-04   1.479266E-03   1.923510E-03  -4.278157E-06  -1.990158E-04  -2.365778E-04   3.740514E-07
  b2   50  -8.027158E-05   3.985094E-04  -2.203273E-03   5.410729E-04  -7.524528E-05  -1.357058E-04  -1.288559E-04  -2.635065E-05
  b2   51   9.860158E-04  -2.356270E-03   9.085776E-04  -4.632672E-04  -1.003777E-05  -1.624540E-05   1.869117E-04  -1.260363E-04
  b2   52  -6.277022E-05   7.983387E-05  -2.013421E-03   5.620135E-04   4.536380E-05  -1.514736E-04  -4.878195E-05  -4.525790E-05
  b2   53   4.497869E-04  -1.328493E-05  -4.400515E-04  -3.848555E-04   2.539147E-05  -3.478725E-05  -3.202092E-05   9.569099E-05
  b2   54  -1.099124E-03   1.156142E-03   6.685393E-04  -7.079744E-04  -1.752288E-05   1.840941E-05   9.763247E-05  -1.141571E-04
  b2   55   5.321491E-04   1.076368E-03   5.301225E-04  -2.669509E-03   6.415159E-05   5.387810E-05   2.600335E-07  -6.197613E-05
  b2   56   4.891497E-04  -3.456416E-04   2.335181E-04   4.767208E-04   2.872402E-04   1.509581E-04  -2.779292E-04   3.542032E-05
  b2   57  -1.181173E-03  -1.182964E-03  -3.779340E-05  -3.918691E-04   2.606773E-04  -2.137372E-04   2.212167E-04   2.342484E-04
  b2   58  -4.997147E-04   8.321030E-04  -3.231866E-04   6.724421E-04   2.835949E-04  -7.289436E-05   7.370324E-05  -3.934511E-04
  b2   59   2.833422E-04  -4.064840E-04  -2.730248E-03   9.592845E-04  -6.977633E-05  -1.279408E-04  -9.021137E-05  -3.694845E-05
  b2   60   4.479423E-04  -1.094108E-04  -8.581832E-04  -4.129004E-04   5.590048E-05   1.103077E-04   3.194349E-05  -8.924928E-05
  b2   61   6.439830E-04   6.256301E-04   2.670442E-04  -1.096708E-03  -3.425171E-05   5.453701E-05   7.996208E-05   1.590245E-04
  b2   62  -3.302271E-04  -2.343787E-04   1.054259E-03  -7.024182E-04  -6.352340E-06  -2.156502E-05   4.376771E-05  -5.260619E-05

               b2   17        b2   18        b2   19        b2   20        b2   21        b2   22        b2   23        b2   24
  b2    4   8.846721E-04   6.351107E-04  -1.468728E-03   1.177444E-03  -4.147393E-04   4.660776E-05   3.159027E-04   3.830397E-05
  b2    5  -3.744612E-04  -8.500513E-05   5.316011E-04   6.869356E-05  -3.790247E-04  -1.560501E-04   1.078993E-04   2.104766E-04
  b2    6  -3.573408E-04  -1.120370E-04   5.603463E-04  -3.210293E-03   1.655607E-03   3.216302E-05  -3.017933E-04  -1.420875E-03
  b2    7  -1.242166E-03  -3.772889E-04   1.622932E-03  -2.453741E-03   1.091261E-03  -2.842771E-03  -2.304668E-03   5.439123E-04
  b2    8   2.409964E-03   1.223627E-03  -1.442845E-03   1.387234E-03  -1.121430E-04   3.585352E-03   2.380727E-03  -7.829725E-04
  b2    9  -1.585752E-03   3.404169E-04   9.328812E-04   3.511154E-04   9.993944E-04  -1.732779E-03   1.206980E-03   4.648693E-04
  b2   10   8.805044E-04   1.763172E-05   4.248530E-04  -2.349949E-04   7.143002E-04   7.177976E-05   2.434177E-04   1.905534E-03
  b2   11   3.156177E-03  -1.583302E-03  -1.286131E-03  -4.029368E-03   4.370226E-03   2.294735E-03   4.549276E-04  -5.435868E-04
  b2   12   1.018771E-03   8.280077E-04  -2.050577E-03  -4.634836E-04   6.059639E-03  -2.718970E-04   2.165686E-03   4.087313E-03
  b2   13   1.779437E-04   1.917679E-04  -5.785091E-04  -6.165290E-04  -1.087701E-05   4.552936E-04  -9.493257E-04   2.440932E-04
  b2   14   2.773965E-03  -7.540366E-04   7.172364E-06  -9.105440E-05   2.176700E-03  -4.104260E-04   1.207678E-03   9.061614E-04
  b2   15   2.633269E-03   7.984364E-04  -1.243323E-03   1.076327E-03   6.674408E-04   2.830562E-04  -3.490829E-04   3.230435E-04
  b2   16  -4.501259E-05  -1.051146E-03  -3.153871E-06  -1.454257E-03   1.089712E-03  -1.066384E-03  -8.982652E-04  -1.585541E-03
  b2   17   4.454122E-03   8.643061E-05  -6.231825E-04   1.543636E-03   2.782569E-04  -6.270818E-04   3.458875E-04  -1.194267E-04
  b2   18   8.643061E-05   5.616364E-03  -7.821909E-04   9.797406E-04  -1.702440E-03   4.031384E-04   1.357849E-03   5.629160E-04
  b2   19  -6.231825E-04  -7.821909E-04   4.829784E-03  -8.685564E-04   5.975786E-04  -3.522952E-04  -1.541555E-04   2.373929E-03
  b2   20   1.543636E-03   9.797406E-04  -8.685564E-04   6.612686E-03  -5.375820E-04  -1.981206E-03   3.183707E-04   2.844258E-04
  b2   21   2.782569E-04  -1.702440E-03   5.975786E-04  -5.375820E-04   3.993793E-03  -9.333265E-04  -4.792290E-04   4.033566E-04
  b2   22  -6.270818E-04   4.031384E-04  -3.522952E-04  -1.981206E-03  -9.333265E-04   6.666443E-03   6.700403E-04   1.054027E-05
  b2   23   3.458875E-04   1.357849E-03  -1.541555E-04   3.183707E-04  -4.792290E-04   6.700403E-04   5.397328E-03  -3.660181E-04
  b2   24  -1.194267E-04   5.629160E-04   2.373929E-03   2.844258E-04   4.033566E-04   1.054027E-05  -3.660181E-04   5.932264E-03
  b2   25   4.367050E-04  -1.922536E-03  -4.702785E-04  -2.773199E-04   6.928811E-04  -2.269882E-04  -2.440121E-04  -6.161632E-05
  b2   26  -1.800938E-03   5.102189E-04  -8.017193E-04   4.431086E-04   8.341669E-04   5.346143E-05  -1.270802E-05  -2.763080E-04
  b2   27   1.687692E-03  -5.536111E-04  -2.842506E-05  -8.416452E-04   1.511242E-04   4.604109E-04  -7.575292E-05  -7.607879E-04
  b2   28  -2.113156E-04  -3.975862E-05  -1.207612E-03   7.484929E-04  -3.102061E-04  -4.915915E-04  -5.821505E-04  -6.201390E-04
  b2   29  -7.930833E-04  -1.074456E-03   9.517949E-05   5.681541E-04  -1.284494E-05  -5.511406E-04   3.345575E-04  -7.181064E-04
  b2   30   4.494552E-04  -1.078795E-03  -2.209352E-04  -7.299383E-04  -4.094747E-04  -5.293974E-05   1.212994E-04  -3.927013E-04
  b2   31   6.721110E-04   1.794191E-03   6.582218E-04   1.454420E-04  -4.778468E-04  -2.878819E-04   9.324852E-05   7.260439E-04
  b2   32  -1.012371E-03   3.250352E-04   1.389019E-04  -4.572623E-04   7.170221E-04   5.858825E-04  -1.701242E-04  -9.161289E-05
  b2   33  -8.093304E-04  -7.030332E-04   1.631277E-03  -1.868162E-04  -4.099036E-04  -2.349918E-04  -6.302440E-04   9.294645E-04
  b2   34   8.541001E-04  -6.255883E-04   5.623940E-04   8.705073E-04   1.353330E-03  -7.472445E-04  -7.578121E-04   1.182084E-04
  b2   35   4.399821E-04   5.037572E-04   3.453952E-04   5.451870E-04  -4.240043E-04  -6.074719E-04  -2.169754E-04  -9.836735E-06
  b2   36  -3.346022E-04  -4.099683E-04   5.437296E-04   3.248968E-04  -2.422664E-04  -1.257449E-04   1.343002E-04  -2.622807E-04
  b2   37   2.299948E-04   2.932524E-04   4.463301E-04  -1.963762E-04   3.453201E-04   2.636681E-04   1.796559E-05  -1.236736E-04
  b2   38  -2.464600E-04   6.581688E-04   1.602013E-04  -1.879363E-04   2.767440E-05   1.108159E-04   7.848921E-05   1.148385E-04
  b2   39   1.477400E-04   4.339810E-04   1.193244E-04  -1.004664E-04   1.638383E-04  -8.273396E-04  -1.889425E-04  -5.111638E-04
  b2   40  -1.436498E-04   8.673057E-05   4.411275E-04  -9.332573E-04   4.775316E-04   4.676838E-04   2.010232E-04  -2.636855E-04
  b2   41   1.737383E-04   1.795745E-04  -5.322927E-04  -2.389879E-05   2.049538E-04  -5.369440E-04  -2.532268E-04   4.054974E-04
  b2   42   4.890657E-04  -1.543730E-04  -5.262746E-05   7.558176E-04   2.579742E-04  -4.490668E-04   2.561097E-04   1.466089E-05
  b2   43   6.539417E-05   1.412479E-04  -9.062105E-05   1.163322E-04   1.893637E-04  -8.080795E-05   6.284737E-04  -2.201290E-05
  b2   44  -2.987702E-04  -2.996619E-05  -1.233259E-04  -7.743838E-04   2.175035E-04  -1.847185E-04   3.023478E-05   2.357343E-05
  b2   45  -4.605412E-05  -5.338779E-05  -1.045417E-05  -3.384138E-04  -1.956697E-04  -4.569046E-04   1.937173E-04   1.833497E-05
  b2   46   2.621348E-05   3.309999E-04   4.103554E-05  -5.834537E-05  -3.146875E-04  -3.880725E-04   3.067359E-04  -5.265479E-05
  b2   47  -1.943978E-05   5.463860E-05   7.917245E-05  -5.639648E-06   3.602724E-05   5.569655E-04   3.749387E-04   2.673033E-04
  b2   48  -1.892149E-05   1.087840E-04   1.289518E-05   7.413938E-05   3.219471E-05   1.329829E-04   1.334629E-05  -4.854288E-05
  b2   49  -1.695414E-04   4.744512E-05  -8.193372E-08  -4.120540E-04  -8.209030E-05   3.979124E-04   1.638932E-04  -3.835867E-05
  b2   50   1.228843E-04  -5.562163E-05  -8.939539E-05   2.902160E-04  -6.883559E-05  -4.311025E-04  -5.034732E-05  -3.882475E-04
  b2   51   1.766907E-05   3.437842E-04   6.767138E-05   1.592261E-04  -2.072566E-04  -1.770436E-05   1.079292E-04   4.085248E-04
  b2   52   4.556707E-05  -7.346540E-05  -4.804101E-05   7.799068E-05  -4.209911E-05  -5.822140E-05  -1.736549E-04  -1.650997E-04
  b2   53  -6.673168E-07  -4.031746E-05   2.773158E-05  -2.826967E-05   1.024398E-04  -8.271081E-05  -2.784116E-04  -9.465801E-05
  b2   54   2.165845E-04   1.443272E-04   1.960099E-05  -1.015669E-05  -7.451524E-05   2.034975E-04   1.751613E-04  -9.126169E-05
  b2   55   1.457085E-04  -1.276996E-04   1.451791E-04  -5.625034E-06   9.806336E-05  -1.423937E-04  -1.904315E-04   4.555503E-04
  b2   56   9.237948E-05   2.697659E-04   1.964325E-04   2.355993E-05  -6.508398E-06  -1.194830E-04   1.929842E-04   1.946414E-04
  b2   57  -5.402100E-05   2.149123E-04  -1.743849E-04  -6.368705E-06  -1.339921E-04   7.276137E-05  -5.207087E-04  -1.191436E-04
  b2   58   9.736197E-05  -5.243267E-05  -8.174857E-05  -5.920131E-05  -1.066346E-04   2.456522E-04  -2.521383E-04   3.216513E-04
  b2   59   3.128507E-05   8.427154E-05   9.978432E-05  -2.598563E-05  -6.873793E-05  -7.346548E-05   3.329962E-05   9.502346E-05
  b2   60  -1.996199E-04   4.311361E-05   1.173110E-05   4.233710E-05  -1.891428E-05   5.162513E-05  -3.041306E-05   1.545081E-04
  b2   61   1.047354E-04  -5.520119E-05   1.865143E-04  -1.389046E-04   2.726081E-05  -1.267777E-04  -2.650293E-04   8.644775E-05
  b2   62   2.327872E-04  -3.985901E-05  -1.471484E-04   1.573823E-04  -2.254398E-04   2.402341E-05   1.141285E-04  -1.151039E-04

               b2   25        b2   26        b2   27        b2   28        b2   29        b2   30        b2   31        b2   32
  b2    4  -2.251044E-04   3.874332E-05   1.725907E-04   2.426119E-05   3.857024E-04  -9.860057E-05   2.583004E-06  -2.025528E-04
  b2    5   1.123482E-04   2.518346E-04   1.412632E-05   4.089680E-04  -1.710739E-04   2.901580E-04   3.847617E-04  -1.990122E-04
  b2    6  -8.239516E-04  -8.805830E-04   1.553734E-03   1.756433E-04   1.266493E-04  -2.108192E-03  -5.022935E-04   8.156802E-04
  b2    7   1.267924E-03   1.267922E-03   7.287342E-04  -8.125011E-04   2.751754E-04   7.045117E-04   8.950795E-04   8.421819E-04
  b2    8  -2.673761E-03   3.673576E-04   1.626956E-03   1.226313E-03   9.928248E-04   1.218436E-03  -8.002069E-04  -4.699549E-04
  b2    9  -2.230551E-04   5.202367E-04  -6.677274E-04  -5.498847E-04   1.719751E-04  -3.273328E-04   5.910819E-04   1.147705E-04
  b2   10   2.147560E-03  -5.971689E-04   1.906918E-03   7.452438E-05   4.015062E-05  -1.041906E-03  -8.597633E-04   4.300277E-04
  b2   11  -1.437015E-03  -5.714472E-04   7.603752E-04  -2.806783E-04  -6.020512E-04  -6.651315E-04  -1.204092E-03   2.285574E-03
  b2   12  -4.079601E-03  -4.957699E-03  -1.940591E-03   7.385165E-03   3.239299E-03   2.318223E-03  -1.037603E-03   5.486687E-03
  b2   13   4.542551E-04   1.165167E-03   1.544230E-03   1.840017E-03  -2.986662E-04  -1.264036E-04   8.495102E-04  -4.734493E-04
  b2   14   5.558300E-04   9.208669E-04   1.994658E-04  -8.297678E-04   1.900491E-05   3.215873E-04   3.894968E-04  -3.240737E-03
  b2   15  -3.541190E-04  -1.370229E-04  -6.234246E-04   3.427828E-04  -2.445275E-03  -3.126261E-04  -6.822182E-04  -2.456291E-03
  b2   16  -1.678617E-03   5.446477E-04   1.021976E-04  -5.226679E-04  -4.259654E-04   1.452321E-03   9.592399E-05  -3.794293E-04
  b2   17   4.367050E-04  -1.800938E-03   1.687692E-03  -2.113156E-04  -7.930833E-04   4.494552E-04   6.721110E-04  -1.012371E-03
  b2   18  -1.922536E-03   5.102189E-04  -5.536111E-04  -3.975862E-05  -1.074456E-03  -1.078795E-03   1.794191E-03   3.250352E-04
  b2   19  -4.702785E-04  -8.017193E-04  -2.842506E-05  -1.207612E-03   9.517949E-05  -2.209352E-04   6.582218E-04   1.389019E-04
  b2   20  -2.773199E-04   4.431086E-04  -8.416452E-04   7.484929E-04   5.681541E-04  -7.299383E-04   1.454420E-04  -4.572623E-04
  b2   21   6.928811E-04   8.341669E-04   1.511242E-04  -3.102061E-04  -1.284494E-05  -4.094747E-04  -4.778468E-04   7.170221E-04
  b2   22  -2.269882E-04   5.346143E-05   4.604109E-04  -4.915915E-04  -5.511406E-04  -5.293974E-05  -2.878819E-04   5.858825E-04
  b2   23  -2.440121E-04  -1.270802E-05  -7.575292E-05  -5.821505E-04   3.345575E-04   1.212994E-04   9.324852E-05  -1.701242E-04
  b2   24  -6.161632E-05  -2.763080E-04  -7.607879E-04  -6.201390E-04  -7.181064E-04  -3.927013E-04   7.260439E-04  -9.161289E-05
  b2   25   4.209566E-03  -4.423416E-05  -2.971960E-05   9.213885E-04   3.840272E-04   3.020632E-04  -9.571805E-05  -6.398342E-04
  b2   26  -4.423416E-05   4.131300E-03  -9.628405E-05   5.447429E-04   6.259525E-04  -2.366030E-04  -5.028838E-05  -1.183966E-04
  b2   27  -2.971960E-05  -9.628405E-05   4.429191E-03  -5.909034E-05   2.910588E-04  -2.354419E-04  -4.594340E-06   3.571586E-04
  b2   28   9.213885E-04   5.447429E-04  -5.909034E-05   3.528198E-03   7.674433E-04   6.923718E-04   1.140545E-04  -6.004546E-05
  b2   29   3.840272E-04   6.259525E-04   2.910588E-04   7.674433E-04   3.374194E-03   3.766448E-04  -2.109232E-04  -6.823061E-05
  b2   30   3.020632E-04  -2.366030E-04  -2.354419E-04   6.923718E-04   3.766448E-04   4.516047E-03   5.654607E-04  -1.109074E-03
  b2   31  -9.571805E-05  -5.028838E-05  -4.594340E-06   1.140545E-04  -2.109232E-04   5.654607E-04   2.803271E-03  -3.069379E-04
  b2   32  -6.398342E-04  -1.183966E-04   3.571586E-04  -6.004546E-05  -6.823061E-05  -1.109074E-03  -3.069379E-04   3.333416E-03
  b2   33   1.702195E-05  -2.313685E-04  -3.962903E-04   3.192460E-04  -4.534753E-04   6.186570E-04   2.072876E-04  -1.698578E-04
  b2   34   1.367759E-05   4.027124E-04   3.116083E-04  -4.486912E-04  -3.349002E-04  -1.054928E-03  -1.046571E-04  -7.802324E-05
  b2   35   9.929324E-05  -2.008214E-05  -5.229436E-04   3.501331E-04  -9.530155E-05  -4.798966E-05   2.561252E-04  -1.634290E-04
  b2   36  -6.695636E-05  -3.797944E-05   4.481108E-06   1.483654E-04   5.673094E-04   1.212235E-05  -1.085265E-04   1.517350E-04
  b2   37   6.588456E-06  -2.830237E-04  -6.623468E-05  -1.622250E-04  -3.378019E-04  -8.793031E-05   3.160451E-04   5.558427E-05
  b2   38   1.491678E-04   2.019521E-04  -4.255004E-04   1.196124E-04   1.003852E-04  -4.869968E-04   2.796829E-04   1.144406E-04
  b2   39   3.604429E-04  -1.632450E-04   8.238608E-05   9.145311E-05   2.455670E-05   8.940043E-05   2.232379E-05  -1.049546E-05
  b2   40   8.045563E-05   1.548176E-04  -1.349330E-04  -2.645415E-05  -4.261830E-05   1.389586E-04   1.474355E-04  -1.119466E-05
  b2   41   4.552058E-04   1.255415E-04  -2.635777E-04  -4.130111E-04  -5.796581E-04   3.990531E-05   4.916330E-04   2.106813E-04
  b2   42   2.681884E-04   4.158190E-04   2.218773E-04   3.655615E-05  -4.093339E-04   1.968461E-04  -1.819433E-04   2.087649E-05
  b2   43   1.796564E-04   6.105354E-05  -4.067342E-05  -2.375103E-05  -3.478996E-04   1.717110E-05   1.234602E-04  -1.334426E-04
  b2   44   3.104428E-04   2.115081E-04   2.048846E-04   1.778242E-05  -1.710784E-05   3.123321E-04  -1.889332E-04  -1.592481E-04
  b2   45  -2.595357E-04  -6.865348E-05   1.290905E-05   1.212584E-04  -9.887168E-05  -3.201860E-04  -6.939579E-05  -3.607850E-05
  b2   46  -2.701211E-05  -3.325026E-04  -1.295747E-04   6.941668E-05  -5.908461E-05  -5.773733E-04   2.889298E-04   1.939921E-04
  b2   47  -8.456120E-05  -2.115435E-05  -2.254892E-04  -2.445154E-04  -2.967961E-04  -4.725015E-05   3.297110E-04  -3.803356E-05
  b2   48  -2.447698E-05   4.210633E-05  -3.063675E-05   1.713462E-04  -5.931009E-05   1.220671E-05   1.261359E-04  -2.595388E-04
  b2   49  -4.467318E-05   1.441068E-04   1.259974E-04  -2.168708E-04  -1.420721E-04  -6.617868E-05  -6.158325E-05   2.873828E-05
  b2   50   2.912925E-05  -5.002229E-04   2.845903E-05  -2.102513E-05   3.116002E-05  -2.073737E-05   1.869247E-04  -1.185885E-04
  b2   51  -3.779835E-04  -1.994442E-04  -9.452184E-05  -1.203973E-04  -2.929173E-04  -3.058665E-04   2.521187E-05  -6.822356E-05
  b2   52  -8.149689E-06  -1.729803E-04   4.594102E-04  -5.498895E-05   1.278178E-05   1.377015E-04  -1.139675E-04  -2.263545E-05
  b2   53   6.803928E-05  -1.615269E-05  -1.068276E-04  -4.215328E-06   5.828646E-06  -1.352579E-05   1.319606E-04   1.054402E-04
  b2   54   1.066625E-04  -1.834204E-04   1.293607E-04   7.747277E-05  -1.773530E-04  -1.111640E-04   1.733722E-04  -1.965243E-05
  b2   55   2.064471E-04  -1.462578E-04   4.634393E-05   4.528292E-05  -9.084363E-06   2.256335E-04   1.507869E-04   1.094975E-04
  b2   56  -2.624982E-04   3.394018E-04   6.253546E-04   8.311587E-05   2.869377E-04   4.030937E-05   4.868069E-04  -2.627421E-05
  b2   57  -3.135916E-04   1.697637E-04   5.669927E-06   2.455318E-04  -3.463888E-04   1.192965E-04   1.855087E-04   6.266304E-05
  b2   58   4.779849E-04  -8.974498E-05   3.485665E-04   3.390461E-04  -4.045022E-05  -3.434594E-04  -9.465719E-05  -2.006202E-05
  b2   59  -2.492276E-05  -1.954536E-04   5.151185E-05  -6.983170E-05  -1.337441E-04   1.599693E-04   9.626040E-05   9.143050E-05
  b2   60   1.113432E-04   2.104550E-04  -2.573160E-04   9.698973E-05   1.395312E-04   2.396699E-06   9.091367E-05  -3.216148E-04
  b2   61  -1.235029E-04  -1.680411E-04   7.466739E-05  -2.432074E-04  -1.192734E-04  -6.468302E-05   4.523266E-05  -1.926848E-04
  b2   62   7.626617E-05  -1.922209E-04   1.331920E-04   2.099489E-05   1.477044E-05   2.389082E-04  -1.597141E-05  -2.257846E-04

               b2   33        b2   34        b2   35        b2   36        b2   37        b2   38        b2   39        b2   40
  b2    4  -1.060973E-03  -1.406425E-04   1.936066E-04   2.506617E-04  -3.350269E-04  -3.672554E-04  -3.755442E-05  -1.663346E-04
  b2    5   2.587771E-04  -3.427180E-04   6.412503E-04   2.998531E-04  -3.951545E-04  -7.571211E-04   5.822054E-05   2.399899E-04
  b2    6  -7.400714E-04   1.385423E-03  -8.356742E-04  -5.193095E-04   1.785628E-04   2.148668E-06   3.584631E-04   1.243480E-03
  b2    7   1.741412E-03   8.718580E-04  -6.436927E-04   6.594839E-05  -3.591816E-05  -5.010521E-05   1.620646E-03   1.086682E-03
  b2    8  -1.351342E-03   1.181928E-04  -1.571812E-03  -1.817701E-05   1.581115E-04  -6.761944E-04  -1.290580E-03  -5.985630E-04
  b2    9  -1.145767E-05   3.916127E-04   2.312240E-04   1.313956E-04   1.076783E-05  -1.963453E-04  -5.990167E-04  -4.839181E-04
  b2   10   9.871923E-05   3.668013E-04  -4.409505E-04  -3.427810E-04  -4.080191E-04  -5.662941E-04   1.997373E-04   2.971406E-04
  b2   11  -1.535780E-03   1.135609E-03  -1.112531E-03  -9.907096E-04   1.242141E-03  -6.014200E-04   1.300595E-03  -4.540359E-04
  b2   12  -8.462973E-04   7.494683E-05   4.680247E-04   1.221087E-03  -7.311008E-04   8.381388E-04   1.077752E-03  -2.997523E-03
  b2   13   3.963532E-04   2.718021E-04  -3.120751E-04  -2.584180E-06  -5.344421E-05  -2.212076E-04  -3.227505E-04   1.705700E-04
  b2   14  -5.586579E-04   1.562123E-03  -1.697280E-04  -1.483471E-03   2.493370E-04  -2.587626E-04  -3.709052E-04   9.479598E-04
  b2   15  -9.827503E-07   1.212926E-03   1.254649E-04  -1.164171E-03   3.537992E-04   6.231920E-05  -6.795670E-05   3.792317E-04
  b2   16   2.481038E-04   6.892748E-04  -4.869703E-04  -9.584787E-06  -2.323675E-04  -3.241852E-04  -9.629596E-05   2.349831E-04
  b2   17  -8.093304E-04   8.541001E-04   4.399821E-04  -3.346022E-04   2.299948E-04  -2.464600E-04   1.477400E-04  -1.436498E-04
  b2   18  -7.030332E-04  -6.255883E-04   5.037572E-04  -4.099683E-04   2.932524E-04   6.581688E-04   4.339810E-04   8.673057E-05
  b2   19   1.631277E-03   5.623940E-04   3.453952E-04   5.437296E-04   4.463301E-04   1.602013E-04   1.193244E-04   4.411275E-04
  b2   20  -1.868162E-04   8.705073E-04   5.451870E-04   3.248968E-04  -1.963762E-04  -1.879363E-04  -1.004664E-04  -9.332573E-04
  b2   21  -4.099036E-04   1.353330E-03  -4.240043E-04  -2.422664E-04   3.453201E-04   2.767440E-05   1.638383E-04   4.775316E-04
  b2   22  -2.349918E-04  -7.472445E-04  -6.074719E-04  -1.257449E-04   2.636681E-04   1.108159E-04  -8.273396E-04   4.676838E-04
  b2   23  -6.302440E-04  -7.578121E-04  -2.169754E-04   1.343002E-04   1.796559E-05   7.848921E-05  -1.889425E-04   2.010232E-04
  b2   24   9.294645E-04   1.182084E-04  -9.836735E-06  -2.622807E-04  -1.236736E-04   1.148385E-04  -5.111638E-04  -2.636855E-04
  b2   25   1.702195E-05   1.367759E-05   9.929324E-05  -6.695636E-05   6.588456E-06   1.491678E-04   3.604429E-04   8.045563E-05
  b2   26  -2.313685E-04   4.027124E-04  -2.008214E-05  -3.797944E-05  -2.830237E-04   2.019521E-04  -1.632450E-04   1.548176E-04
  b2   27  -3.962903E-04   3.116083E-04  -5.229436E-04   4.481108E-06  -6.623468E-05  -4.255004E-04   8.238608E-05  -1.349330E-04
  b2   28   3.192460E-04  -4.486912E-04   3.501331E-04   1.483654E-04  -1.622250E-04   1.196124E-04   9.145311E-05  -2.645415E-05
  b2   29  -4.534753E-04  -3.349002E-04  -9.530155E-05   5.673094E-04  -3.378019E-04   1.003852E-04   2.455670E-05  -4.261830E-05
  b2   30   6.186570E-04  -1.054928E-03  -4.798966E-05   1.212235E-05  -8.793031E-05  -4.869968E-04   8.940043E-05   1.389586E-04
  b2   31   2.072876E-04  -1.046571E-04   2.561252E-04  -1.085265E-04   3.160451E-04   2.796829E-04   2.232379E-05   1.474355E-04
  b2   32  -1.698578E-04  -7.802324E-05  -1.634290E-04   1.517350E-04   5.558427E-05   1.144406E-04  -1.049546E-05  -1.119466E-05
  b2   33   2.719401E-03   2.497318E-04   9.479425E-05  -1.417911E-04  -1.812301E-04  -4.297667E-05  -1.766743E-04  -9.297286E-05
  b2   34   2.497318E-04   2.048868E-03   6.979086E-05  -2.570590E-05   6.020169E-05   7.517615E-06  -3.470849E-05  -1.662043E-04
  b2   35   9.479425E-05   6.979086E-05   1.807871E-03   1.076054E-04   3.879871E-04   4.613345E-05   3.460288E-04  -7.860056E-05
  b2   36  -1.417911E-04  -2.570590E-05   1.076054E-04   1.529026E-03   1.293144E-04  -5.443276E-05  -2.735428E-05  -3.604925E-04
  b2   37  -1.812301E-04   6.020169E-05   3.879871E-04   1.293144E-04   1.363966E-03  -5.884346E-05  -2.165872E-04   2.648858E-04
  b2   38  -4.297667E-05   7.517615E-06   4.613345E-05  -5.443276E-05  -5.884346E-05   1.063199E-03   2.491292E-04   6.620448E-05
  b2   39  -1.766743E-04  -3.470849E-05   3.460288E-04  -2.735428E-05  -2.165872E-04   2.491292E-04   1.520008E-03   6.472990E-05
  b2   40  -9.297286E-05  -1.662043E-04  -7.860056E-05  -3.604925E-04   2.648858E-04   6.620448E-05   6.472990E-05   1.305222E-03
  b2   41  -1.167637E-04   9.468636E-05  -1.473681E-04  -4.067330E-04   1.408069E-05   4.533243E-05  -3.204528E-05  -5.128187E-05
  b2   42  -2.765540E-04   6.525563E-06  -3.882001E-05  -4.319701E-04   1.161828E-04  -2.736206E-04  -1.414949E-04   4.278425E-04
  b2   43  -1.321003E-04  -5.589264E-05  -4.290506E-04   4.010133E-04   6.535958E-05   7.249491E-05  -9.070172E-06   2.089126E-05
  b2   44   2.227928E-04  -2.977575E-04   3.276777E-05  -8.979782E-05   1.130170E-05   2.410037E-05   1.967536E-04   7.497536E-05
  b2   45  -1.786706E-04  -1.272021E-04   2.556854E-04   5.462355E-05   1.987323E-04   4.928432E-05   3.774145E-05   4.397719E-05
  b2   46  -1.178939E-05   1.255065E-04   1.830706E-04  -1.270358E-04  -1.184651E-04   2.390514E-04   1.669906E-04  -9.696644E-05
  b2   47   1.758478E-04  -1.892560E-04   2.922465E-04  -8.952730E-05   1.180337E-04  -5.353355E-05  -1.737670E-04  -3.206304E-05
  b2   48  -4.234798E-04  -3.183043E-04   5.439386E-05   6.347975E-05  -1.329276E-05   9.340873E-05   1.746425E-04   7.274540E-05
  b2   49  -3.786414E-04   5.243612E-05  -2.163617E-04  -4.132750E-05   1.187520E-05   6.633478E-05  -6.428493E-05  -5.094538E-06
  b2   50  -7.285839E-05  -1.243906E-04  -9.099829E-05  -7.604113E-05   8.717845E-05  -5.688362E-05   1.850439E-04  -4.944673E-05
  b2   51   7.111871E-07  -1.374590E-04  -3.940917E-05  -5.373821E-05  -3.571360E-05  -1.050873E-04  -2.787904E-04  -5.548368E-05
  b2   52   1.379173E-04   8.072090E-05  -2.423435E-04  -5.427944E-05   9.603991E-05  -2.305248E-04  -7.173700E-05   5.204028E-05
  b2   53  -7.124671E-05   1.610529E-04   4.801764E-05   1.205300E-04   2.444080E-04   3.802743E-05  -3.711097E-05   3.522672E-05
  b2   54  -1.870062E-05  -9.069589E-05   2.334211E-04  -1.395307E-04   1.410158E-04   1.888819E-04   1.802986E-04   2.526146E-04
  b2   55   8.545203E-05  -2.544978E-04  -1.263024E-04  -3.148382E-05   8.248952E-06   2.277595E-05  -1.445218E-04   1.841395E-04
  b2   56  -9.575004E-06   2.570155E-05  -2.135294E-05   2.395897E-06   9.645531E-06   8.301383E-05  -6.496621E-05  -1.208764E-04
  b2   57   2.122350E-04  -2.542549E-05  -3.180704E-05  -2.848516E-05  -2.276995E-05   3.584275E-05  -2.026558E-05  -3.814855E-05
  b2   58  -3.402454E-05   3.207847E-05   7.355753E-05   1.687133E-05   4.779047E-05   1.011813E-05  -5.184779E-05  -1.921961E-04
  b2   59  -1.095999E-05  -2.201889E-04   2.035100E-04   6.532398E-05   7.077991E-05  -1.030550E-04   1.843055E-04  -9.947897E-05
  b2   60   1.496607E-04  -7.068994E-05   5.767661E-05  -1.136278E-05  -4.654253E-05   9.681582E-05  -2.584258E-05   7.402819E-06
  b2   61   6.145517E-05   2.009063E-04   1.202588E-04   3.184045E-05   1.421939E-05  -1.665491E-05   1.622080E-04  -6.855853E-05
  b2   62   4.417037E-05  -5.756809E-05  -5.097002E-05   5.625146E-05  -1.934387E-05  -1.193371E-04  -1.222225E-04  -1.879324E-04

               b2   41        b2   42        b2   43        b2   44        b2   45        b2   46        b2   47        b2   48
  b2    4   2.560658E-04  -3.778377E-04   1.285220E-04  -5.212634E-04  -3.384653E-04  -4.186618E-04  -2.877785E-04   6.461823E-04
  b2    5  -5.677015E-04   5.577865E-04   2.076560E-04   4.370404E-05  -3.903113E-04   3.618989E-04   8.610490E-04  -4.409085E-04
  b2    6  -7.351343E-04  -1.102861E-03  -5.134471E-04  -1.163243E-04   9.737899E-04   1.439106E-03  -1.203478E-03  -1.826516E-04
  b2    7   2.881224E-03  -1.837713E-05  -9.343267E-04   3.033250E-03   9.535089E-04  -6.790379E-04  -1.423718E-03  -3.850268E-04
  b2    8  -4.216470E-03  -1.293031E-05  -1.312804E-03   6.106337E-04   8.597849E-04  -2.780926E-03  -1.766619E-03   6.272110E-04
  b2    9  -1.220615E-03  -4.189604E-04   2.964750E-03   2.926203E-04   1.620256E-03  -1.078131E-03  -1.243116E-03   1.055889E-03
  b2   10  -5.600488E-05  -8.002062E-04   1.461836E-03  -1.038041E-03  -1.977723E-03  -1.642403E-04  -8.389672E-04   5.990972E-04
  b2   11  -2.674625E-04  -2.100307E-03  -2.403595E-04  -2.534924E-04   3.982588E-04   1.873796E-03   5.942881E-04  -1.414101E-04
  b2   12  -1.296098E-03  -6.486858E-03  -1.307010E-04   2.226107E-03  -9.107908E-04  -1.498173E-04   2.129575E-03   1.675421E-04
  b2   13  -6.445039E-05   3.464256E-05   1.826168E-04   1.174470E-04   1.127933E-04  -1.930941E-05  -5.747787E-05  -1.033056E-06
  b2   14   1.854549E-04   7.750353E-04  -1.689323E-05  -1.027695E-04   9.934989E-05  -1.891097E-06  -4.104462E-05  -2.390635E-05
  b2   15  -3.853040E-05   7.154969E-04   2.867244E-04  -1.686674E-04   9.794894E-05  -5.066512E-05  -1.413425E-04   7.260529E-05
  b2   16   1.189853E-04  -2.099828E-05   4.736311E-05   7.141977E-05   5.093700E-05  -2.953416E-06  -2.136979E-04   3.029200E-05
  b2   17   1.737383E-04   4.890657E-04   6.539417E-05  -2.987702E-04  -4.605412E-05   2.621348E-05  -1.943978E-05  -1.892149E-05
  b2   18   1.795745E-04  -1.543730E-04   1.412479E-04  -2.996619E-05  -5.338779E-05   3.309999E-04   5.463860E-05   1.087840E-04
  b2   19  -5.322927E-04  -5.262746E-05  -9.062105E-05  -1.233259E-04  -1.045417E-05   4.103554E-05   7.917245E-05   1.289518E-05
  b2   20  -2.389879E-05   7.558176E-04   1.163322E-04  -7.743838E-04  -3.384138E-04  -5.834537E-05  -5.639648E-06   7.413938E-05
  b2   21   2.049538E-04   2.579742E-04   1.893637E-04   2.175035E-04  -1.956697E-04  -3.146875E-04   3.602724E-05   3.219471E-05
  b2   22  -5.369440E-04  -4.490668E-04  -8.080795E-05  -1.847185E-04  -4.569046E-04  -3.880725E-04   5.569655E-04   1.329829E-04
  b2   23  -2.532268E-04   2.561097E-04   6.284737E-04   3.023478E-05   1.937173E-04   3.067359E-04   3.749387E-04   1.334629E-05
  b2   24   4.054974E-04   1.466089E-05  -2.201290E-05   2.357343E-05   1.833497E-05  -5.265479E-05   2.673033E-04  -4.854288E-05
  b2   25   4.552058E-04   2.681884E-04   1.796564E-04   3.104428E-04  -2.595357E-04  -2.701211E-05  -8.456120E-05  -2.447698E-05
  b2   26   1.255415E-04   4.158190E-04   6.105354E-05   2.115081E-04  -6.865348E-05  -3.325026E-04  -2.115435E-05   4.210633E-05
  b2   27  -2.635777E-04   2.218773E-04  -4.067342E-05   2.048846E-04   1.290905E-05  -1.295747E-04  -2.254892E-04  -3.063675E-05
  b2   28  -4.130111E-04   3.655615E-05  -2.375103E-05   1.778242E-05   1.212584E-04   6.941668E-05  -2.445154E-04   1.713462E-04
  b2   29  -5.796581E-04  -4.093339E-04  -3.478996E-04  -1.710784E-05  -9.887168E-05  -5.908461E-05  -2.967961E-04  -5.931009E-05
  b2   30   3.990531E-05   1.968461E-04   1.717110E-05   3.123321E-04  -3.201860E-04  -5.773733E-04  -4.725015E-05   1.220671E-05
  b2   31   4.916330E-04  -1.819433E-04   1.234602E-04  -1.889332E-04  -6.939579E-05   2.889298E-04   3.297110E-04   1.261359E-04
  b2   32   2.106813E-04   2.087649E-05  -1.334426E-04  -1.592481E-04  -3.607850E-05   1.939921E-04  -3.803356E-05  -2.595388E-04
  b2   33  -1.167637E-04  -2.765540E-04  -1.321003E-04   2.227928E-04  -1.786706E-04  -1.178939E-05   1.758478E-04  -4.234798E-04
  b2   34   9.468636E-05   6.525563E-06  -5.589264E-05  -2.977575E-04  -1.272021E-04   1.255065E-04  -1.892560E-04  -3.183043E-04
  b2   35  -1.473681E-04  -3.882001E-05  -4.290506E-04   3.276777E-05   2.556854E-04   1.830706E-04   2.922465E-04   5.439386E-05
  b2   36  -4.067330E-04  -4.319701E-04   4.010133E-04  -8.979782E-05   5.462355E-05  -1.270358E-04  -8.952730E-05   6.347975E-05
  b2   37   1.408069E-05   1.161828E-04   6.535958E-05   1.130170E-05   1.987323E-04  -1.184651E-04   1.180337E-04  -1.329276E-05
  b2   38   4.533243E-05  -2.736206E-04   7.249491E-05   2.410037E-05   4.928432E-05   2.390514E-04  -5.353355E-05   9.340873E-05
  b2   39  -3.204528E-05  -1.414949E-04  -9.070172E-06   1.967536E-04   3.774145E-05   1.669906E-04  -1.737670E-04   1.746425E-04
  b2   40  -5.128187E-05   4.278425E-04   2.089126E-05   7.497536E-05   4.397719E-05  -9.696644E-05  -3.206304E-05   7.274540E-05
  b2   41   1.447761E-03   1.427389E-04  -4.477626E-05   9.375058E-05  -1.052721E-04   1.633308E-04   1.161649E-04  -1.365407E-04
  b2   42   1.427389E-04   1.614773E-03   1.432562E-04  -2.555436E-04   9.523455E-05  -8.979006E-05  -1.582532E-04   2.342421E-05
  b2   43  -4.477626E-05   1.432562E-04   1.405566E-03   5.854351E-06   5.285009E-05  -4.200257E-05  -7.823120E-05   2.392944E-04
  b2   44   9.375058E-05  -2.555436E-04   5.854351E-06   1.067926E-03   8.306119E-06  -1.917726E-04   1.312799E-04  -5.167037E-05
  b2   45  -1.052721E-04   9.523455E-05   5.285009E-05   8.306119E-06   9.154823E-04   1.583336E-04  -5.844420E-05   5.467693E-05
  b2   46   1.633308E-04  -8.979006E-05  -4.200257E-05  -1.917726E-04   1.583336E-04   1.054182E-03  -4.926712E-05  -4.823198E-05
  b2   47   1.161649E-04  -1.582532E-04  -7.823120E-05   1.312799E-04  -5.844420E-05  -4.926712E-05   1.038884E-03  -4.102282E-05
  b2   48  -1.365407E-04   2.342421E-05   2.392944E-04  -5.167037E-05   5.467693E-05  -4.823198E-05  -4.102282E-05   7.782780E-04
  b2   49   1.275641E-04  -1.506237E-04   1.562902E-04   1.756624E-04  -5.406673E-05  -1.165097E-04   1.686650E-04   1.076989E-04
  b2   50   8.023122E-05  -6.703957E-05   3.113716E-05  -4.247344E-05   4.130208E-05   1.937314E-04  -6.918721E-05   1.188675E-04
  b2   51   1.094314E-05   4.928167E-06   9.894689E-05  -1.894340E-04   9.061913E-05   6.630085E-05   1.493354E-04  -3.443677E-06
  b2   52  -8.867500E-05   8.598304E-06   3.621517E-06   1.216114E-04  -1.177120E-04  -2.566464E-04  -7.736440E-06  -1.326699E-04
  b2   53   8.839840E-05  -6.191339E-05   3.883720E-05  -1.036048E-04   2.983341E-05  -2.894329E-05  -1.088413E-04  -5.654789E-05
  b2   54  -1.654320E-04   6.420990E-05   1.070281E-05   2.063046E-05   1.728131E-04   1.655496E-04   2.185518E-04   2.118034E-04
  b2   55   1.135659E-04   5.226244E-05  -1.252732E-05   1.431166E-05   1.738740E-05  -6.127841E-05  -8.140838E-05  -5.084556E-05
  b2   56   5.175829E-05  -4.693469E-06  -4.873843E-05  -8.109271E-05  -1.547499E-05   9.943114E-05   5.955155E-05   1.105015E-06
  b2   57  -2.005813E-05  -3.107843E-05   2.971473E-04  -2.709112E-05   3.658552E-06  -2.208115E-05  -6.129270E-05   9.660339E-05
  b2   58   4.487875E-06  -1.113491E-05   5.720420E-05   6.924283E-05   8.343295E-05   1.680548E-05  -7.827846E-05  -5.861402E-06
  b2   59   3.573500E-05   7.333485E-05  -5.020263E-06   5.287808E-05   4.263387E-05  -8.076494E-05   1.557167E-04   9.454967E-05
  b2   60  -2.776975E-05  -1.493071E-04  -9.495713E-06   9.973911E-05  -3.049330E-05  -4.570926E-05   7.418178E-05   7.285996E-05
  b2   61  -3.665579E-07  -1.787749E-04  -3.655055E-05   7.439697E-06   5.171519E-05  -4.016416E-05   4.106762E-05   2.037599E-05
  b2   62   8.631402E-06  -2.909494E-05   2.279271E-05   4.962764E-05  -1.274245E-04  -2.911126E-05   8.106130E-07  -2.855837E-05

               b2   49        b2   50        b2   51        b2   52        b2   53        b2   54        b2   55        b2   56
  b2    4   3.224042E-04  -2.222979E-04   9.654460E-04   7.663560E-05   1.420762E-04  -1.017589E-03   6.826728E-04   7.091343E-04
  b2    5  -2.470112E-04  -4.919337E-04   5.594568E-04   1.219718E-04  -5.691394E-04  -1.567464E-04  -1.473350E-04   1.667509E-03
  b2    6   8.741599E-05   1.520543E-03   1.681196E-04   8.686073E-04   2.040748E-04   9.197919E-04   5.276920E-04   2.790006E-04
  b2    7   4.526586E-04   4.484754E-04  -3.836945E-03  -1.585352E-04   9.210670E-04   3.751878E-04   4.134056E-04   5.456238E-05
  b2    8  -2.175831E-04  -7.506817E-04  -2.712868E-03   6.808932E-04   3.814153E-04   1.528181E-03  -8.712729E-04  -3.986612E-04
  b2    9   3.376468E-04  -8.027158E-05   9.860158E-04  -6.277022E-05   4.497869E-04  -1.099124E-03   5.321491E-04   4.891497E-04
  b2   10   2.983349E-04   3.985094E-04  -2.356270E-03   7.983387E-05  -1.328493E-05   1.156142E-03   1.076368E-03  -3.456416E-04
  b2   11   1.479266E-03  -2.203273E-03   9.085776E-04  -2.013421E-03  -4.400515E-04   6.685393E-04   5.301225E-04   2.335181E-04
  b2   12   1.923510E-03   5.410729E-04  -4.632672E-04   5.620135E-04  -3.848555E-04  -7.079744E-04  -2.669509E-03   4.767208E-04
  b2   13  -4.278157E-06  -7.524528E-05  -1.003777E-05   4.536380E-05   2.539147E-05  -1.752288E-05   6.415159E-05   2.872402E-04
  b2   14  -1.990158E-04  -1.357058E-04  -1.624540E-05  -1.514736E-04  -3.478725E-05   1.840941E-05   5.387810E-05   1.509581E-04
  b2   15  -2.365778E-04  -1.288559E-04   1.869117E-04  -4.878195E-05  -3.202092E-05   9.763247E-05   2.600335E-07  -2.779292E-04
  b2   16   3.740514E-07  -2.635065E-05  -1.260363E-04  -4.525790E-05   9.569099E-05  -1.141571E-04  -6.197613E-05   3.542032E-05
  b2   17  -1.695414E-04   1.228843E-04   1.766907E-05   4.556707E-05  -6.673168E-07   2.165845E-04   1.457085E-04   9.237948E-05
  b2   18   4.744512E-05  -5.562163E-05   3.437842E-04  -7.346540E-05  -4.031746E-05   1.443272E-04  -1.276996E-04   2.697659E-04
  b2   19  -8.193372E-08  -8.939539E-05   6.767138E-05  -4.804101E-05   2.773158E-05   1.960099E-05   1.451791E-04   1.964325E-04
  b2   20  -4.120540E-04   2.902160E-04   1.592261E-04   7.799068E-05  -2.826967E-05  -1.015669E-05  -5.625034E-06   2.355993E-05
  b2   21  -8.209030E-05  -6.883559E-05  -2.072566E-04  -4.209911E-05   1.024398E-04  -7.451524E-05   9.806336E-05  -6.508398E-06
  b2   22   3.979124E-04  -4.311025E-04  -1.770436E-05  -5.822140E-05  -8.271081E-05   2.034975E-04  -1.423937E-04  -1.194830E-04
  b2   23   1.638932E-04  -5.034732E-05   1.079292E-04  -1.736549E-04  -2.784116E-04   1.751613E-04  -1.904315E-04   1.929842E-04
  b2   24  -3.835867E-05  -3.882475E-04   4.085248E-04  -1.650997E-04  -9.465801E-05  -9.126169E-05   4.555503E-04   1.946414E-04
  b2   25  -4.467318E-05   2.912925E-05  -3.779835E-04  -8.149689E-06   6.803928E-05   1.066625E-04   2.064471E-04  -2.624982E-04
  b2   26   1.441068E-04  -5.002229E-04  -1.994442E-04  -1.729803E-04  -1.615269E-05  -1.834204E-04  -1.462578E-04   3.394018E-04
  b2   27   1.259974E-04   2.845903E-05  -9.452184E-05   4.594102E-04  -1.068276E-04   1.293607E-04   4.634393E-05   6.253546E-04
  b2   28  -2.168708E-04  -2.102513E-05  -1.203973E-04  -5.498895E-05  -4.215328E-06   7.747277E-05   4.528292E-05   8.311587E-05
  b2   29  -1.420721E-04   3.116002E-05  -2.929173E-04   1.278178E-05   5.828646E-06  -1.773530E-04  -9.084363E-06   2.869377E-04
  b2   30  -6.617868E-05  -2.073737E-05  -3.058665E-04   1.377015E-04  -1.352579E-05  -1.111640E-04   2.256335E-04   4.030937E-05
  b2   31  -6.158325E-05   1.869247E-04   2.521187E-05  -1.139675E-04   1.319606E-04   1.733722E-04   1.507869E-04   4.868069E-04
  b2   32   2.873828E-05  -1.185885E-04  -6.822356E-05  -2.263545E-05   1.054402E-04  -1.965243E-05   1.094975E-04  -2.627421E-05
  b2   33  -3.786414E-04  -7.285839E-05   7.111871E-07   1.379173E-04  -7.124671E-05  -1.870062E-05   8.545203E-05  -9.575004E-06
  b2   34   5.243612E-05  -1.243906E-04  -1.374590E-04   8.072090E-05   1.610529E-04  -9.069589E-05  -2.544978E-04   2.570155E-05
  b2   35  -2.163617E-04  -9.099829E-05  -3.940917E-05  -2.423435E-04   4.801764E-05   2.334211E-04  -1.263024E-04  -2.135294E-05
  b2   36  -4.132750E-05  -7.604113E-05  -5.373821E-05  -5.427944E-05   1.205300E-04  -1.395307E-04  -3.148382E-05   2.395897E-06
  b2   37   1.187520E-05   8.717845E-05  -3.571360E-05   9.603991E-05   2.444080E-04   1.410158E-04   8.248952E-06   9.645531E-06
  b2   38   6.633478E-05  -5.688362E-05  -1.050873E-04  -2.305248E-04   3.802743E-05   1.888819E-04   2.277595E-05   8.301383E-05
  b2   39  -6.428493E-05   1.850439E-04  -2.787904E-04  -7.173700E-05  -3.711097E-05   1.802986E-04  -1.445218E-04  -6.496621E-05
  b2   40  -5.094538E-06  -4.944673E-05  -5.548368E-05   5.204028E-05   3.522672E-05   2.526146E-04   1.841395E-04  -1.208764E-04
  b2   41   1.275641E-04   8.023122E-05   1.094314E-05  -8.867500E-05   8.839840E-05  -1.654320E-04   1.135659E-04   5.175829E-05
  b2   42  -1.506237E-04  -6.703957E-05   4.928167E-06   8.598304E-06  -6.191339E-05   6.420990E-05   5.226244E-05  -4.693469E-06
  b2   43   1.562902E-04   3.113716E-05   9.894689E-05   3.621517E-06   3.883720E-05   1.070281E-05  -1.252732E-05  -4.873843E-05
  b2   44   1.756624E-04  -4.247344E-05  -1.894340E-04   1.216114E-04  -1.036048E-04   2.063046E-05   1.431166E-05  -8.109271E-05
  b2   45  -5.406673E-05   4.130208E-05   9.061913E-05  -1.177120E-04   2.983341E-05   1.728131E-04   1.738740E-05  -1.547499E-05
  b2   46  -1.165097E-04   1.937314E-04   6.630085E-05  -2.566464E-04  -2.894329E-05   1.655496E-04  -6.127841E-05   9.943114E-05
  b2   47   1.686650E-04  -6.918721E-05   1.493354E-04  -7.736440E-06  -1.088413E-04   2.185518E-04  -8.140838E-05   5.955155E-05
  b2   48   1.076989E-04   1.188675E-04  -3.443677E-06  -1.326699E-04  -5.654789E-05   2.118034E-04  -5.084556E-05   1.105015E-06
  b2   49   9.008890E-04  -1.102994E-04   1.772699E-05   1.116369E-04  -2.886030E-05   2.505423E-05  -2.165750E-04   1.204073E-05
  b2   50  -1.102994E-04   7.275197E-04  -3.210579E-05   3.230540E-05   3.986673E-05  -3.733875E-05  -1.123480E-04  -1.485232E-05
  b2   51   1.772699E-05  -3.210579E-05   6.651793E-04   1.805419E-05  -8.963274E-05  -4.171898E-05   6.289842E-05   6.959675E-05
  b2   52   1.116369E-04   3.230540E-05   1.805419E-05   6.035347E-04   3.004742E-06  -7.855934E-05  -8.573150E-05   3.189017E-05
  b2   53  -2.886030E-05   3.986673E-05  -8.963274E-05   3.004742E-06   3.814492E-04  -9.253922E-05   5.258824E-05  -3.774697E-05
  b2   54   2.505423E-05  -3.733875E-05  -4.171898E-05  -7.855934E-05  -9.253922E-05   8.035790E-04  -2.771155E-05  -4.240335E-05
  b2   55  -2.165750E-04  -1.123480E-04   6.289842E-05  -8.573150E-05   5.258824E-05  -2.771155E-05   8.694278E-04  -2.924152E-05
  b2   56   1.204073E-05  -1.485232E-05   6.959675E-05   3.189017E-05  -3.774697E-05  -4.240335E-05  -2.924152E-05   5.869003E-04
  b2   57  -2.867362E-05  -1.197224E-05   4.460160E-05  -5.667517E-06   2.670848E-05   2.583849E-05   4.284229E-05  -1.064368E-05
  b2   58   3.882412E-05  -3.672188E-05   7.195056E-05   6.788121E-05   4.391810E-05  -3.002656E-05  -4.856187E-05   2.049579E-05
  b2   59  -1.062266E-04   3.059833E-05  -3.462401E-06  -6.163750E-06  -2.500901E-05   1.041811E-04   1.068071E-04  -2.421497E-05
  b2   60  -1.389427E-04   1.153518E-04   1.978277E-05  -1.972284E-05  -1.516710E-05  -4.637534E-05  -9.497733E-05   2.959129E-05
  b2   61   5.212707E-05  -7.859238E-05  -1.609867E-06   3.020569E-05   1.941314E-05   1.289815E-04  -7.217634E-05  -1.846439E-05
  b2   62   3.046251E-05   2.477578E-05   3.342190E-05  -6.468653E-06  -6.739088E-05  -1.317986E-04  -1.215442E-06   4.326289E-05

               b2   57        b2   58        b2   59        b2   60        b2   61        b2   62
  b2    4   8.946178E-04   8.460484E-04  -1.115960E-04   3.253423E-04   4.740233E-04  -6.482371E-05
  b2    5  -1.004232E-04   2.746311E-04   8.298642E-05  -8.112945E-05  -7.815906E-04   6.210300E-04
  b2    6  -1.835322E-05  -2.235864E-04  -1.287854E-03  -7.807395E-04   5.640789E-04   7.032843E-04
  b2    7   1.159603E-04  -6.823604E-04   8.100679E-05  -5.650131E-05   1.976583E-04  -5.435773E-04
  b2    8   1.122315E-04  -6.588219E-04  -3.330688E-04  -4.076236E-05   5.025300E-04  -7.798004E-04
  b2    9  -1.181173E-03  -4.997147E-04   2.833422E-04   4.479423E-04   6.439830E-04  -3.302271E-04
  b2   10  -1.182964E-03   8.321030E-04  -4.064840E-04  -1.094108E-04   6.256301E-04  -2.343787E-04
  b2   11  -3.779340E-05  -3.231866E-04  -2.730248E-03  -8.581832E-04   2.670442E-04   1.054259E-03
  b2   12  -3.918691E-04   6.724421E-04   9.592845E-04  -4.129004E-04  -1.096708E-03  -7.024182E-04
  b2   13   2.606773E-04   2.835949E-04  -6.977633E-05   5.590048E-05  -3.425171E-05  -6.352340E-06
  b2   14  -2.137372E-04  -7.289436E-05  -1.279408E-04   1.103077E-04   5.453701E-05  -2.156502E-05
  b2   15   2.212167E-04   7.370324E-05  -9.021137E-05   3.194349E-05   7.996208E-05   4.376771E-05
  b2   16   2.342484E-04  -3.934511E-04  -3.694845E-05  -8.924928E-05   1.590245E-04  -5.260619E-05
  b2   17  -5.402100E-05   9.736197E-05   3.128507E-05  -1.996199E-04   1.047354E-04   2.327872E-04
  b2   18   2.149123E-04  -5.243267E-05   8.427154E-05   4.311361E-05  -5.520119E-05  -3.985901E-05
  b2   19  -1.743849E-04  -8.174857E-05   9.978432E-05   1.173110E-05   1.865143E-04  -1.471484E-04
  b2   20  -6.368705E-06  -5.920131E-05  -2.598563E-05   4.233710E-05  -1.389046E-04   1.573823E-04
  b2   21  -1.339921E-04  -1.066346E-04  -6.873793E-05  -1.891428E-05   2.726081E-05  -2.254398E-04
  b2   22   7.276137E-05   2.456522E-04  -7.346548E-05   5.162513E-05  -1.267777E-04   2.402341E-05
  b2   23  -5.207087E-04  -2.521383E-04   3.329962E-05  -3.041306E-05  -2.650293E-04   1.141285E-04
  b2   24  -1.191436E-04   3.216513E-04   9.502346E-05   1.545081E-04   8.644775E-05  -1.151039E-04
  b2   25  -3.135916E-04   4.779849E-04  -2.492276E-05   1.113432E-04  -1.235029E-04   7.626617E-05
  b2   26   1.697637E-04  -8.974498E-05  -1.954536E-04   2.104550E-04  -1.680411E-04  -1.922209E-04
  b2   27   5.669927E-06   3.485665E-04   5.151185E-05  -2.573160E-04   7.466739E-05   1.331920E-04
  b2   28   2.455318E-04   3.390461E-04  -6.983170E-05   9.698973E-05  -2.432074E-04   2.099489E-05
  b2   29  -3.463888E-04  -4.045022E-05  -1.337441E-04   1.395312E-04  -1.192734E-04   1.477044E-05
  b2   30   1.192965E-04  -3.434594E-04   1.599693E-04   2.396699E-06  -6.468302E-05   2.389082E-04
  b2   31   1.855087E-04  -9.465719E-05   9.626040E-05   9.091367E-05   4.523266E-05  -1.597141E-05
  b2   32   6.266304E-05  -2.006202E-05   9.143050E-05  -3.216148E-04  -1.926848E-04  -2.257846E-04
  b2   33   2.122350E-04  -3.402454E-05  -1.095999E-05   1.496607E-04   6.145517E-05   4.417037E-05
  b2   34  -2.542549E-05   3.207847E-05  -2.201889E-04  -7.068994E-05   2.009063E-04  -5.756809E-05
  b2   35  -3.180704E-05   7.355753E-05   2.035100E-04   5.767661E-05   1.202588E-04  -5.097002E-05
  b2   36  -2.848516E-05   1.687133E-05   6.532398E-05  -1.136278E-05   3.184045E-05   5.625146E-05
  b2   37  -2.276995E-05   4.779047E-05   7.077991E-05  -4.654253E-05   1.421939E-05  -1.934387E-05
  b2   38   3.584275E-05   1.011813E-05  -1.030550E-04   9.681582E-05  -1.665491E-05  -1.193371E-04
  b2   39  -2.026558E-05  -5.184779E-05   1.843055E-04  -2.584258E-05   1.622080E-04  -1.222225E-04
  b2   40  -3.814855E-05  -1.921961E-04  -9.947897E-05   7.402819E-06  -6.855853E-05  -1.879324E-04
  b2   41  -2.005813E-05   4.487875E-06   3.573500E-05  -2.776975E-05  -3.665579E-07   8.631402E-06
  b2   42  -3.107843E-05  -1.113491E-05   7.333485E-05  -1.493071E-04  -1.787749E-04  -2.909494E-05
  b2   43   2.971473E-04   5.720420E-05  -5.020263E-06  -9.495713E-06  -3.655055E-05   2.279271E-05
  b2   44  -2.709112E-05   6.924283E-05   5.287808E-05   9.973911E-05   7.439697E-06   4.962764E-05
  b2   45   3.658552E-06   8.343295E-05   4.263387E-05  -3.049330E-05   5.171519E-05  -1.274245E-04
  b2   46  -2.208115E-05   1.680548E-05  -8.076494E-05  -4.570926E-05  -4.016416E-05  -2.911126E-05
  b2   47  -6.129270E-05  -7.827846E-05   1.557167E-04   7.418178E-05   4.106762E-05   8.106130E-07
  b2   48   9.660339E-05  -5.861402E-06   9.454967E-05   7.285996E-05   2.037599E-05  -2.855837E-05
  b2   49  -2.867362E-05   3.882412E-05  -1.062266E-04  -1.389427E-04   5.212707E-05   3.046251E-05
  b2   50  -1.197224E-05  -3.672188E-05   3.059833E-05   1.153518E-04  -7.859238E-05   2.477578E-05
  b2   51   4.460160E-05   7.195056E-05  -3.462401E-06   1.978277E-05  -1.609867E-06   3.342190E-05
  b2   52  -5.667517E-06   6.788121E-05  -6.163750E-06  -1.972284E-05   3.020569E-05  -6.468653E-06
  b2   53   2.670848E-05   4.391810E-05  -2.500901E-05  -1.516710E-05   1.941314E-05  -6.739088E-05
  b2   54   2.583849E-05  -3.002656E-05   1.041811E-04  -4.637534E-05   1.289815E-04  -1.317986E-04
  b2   55   4.284229E-05  -4.856187E-05   1.068071E-04  -9.497733E-05  -7.217634E-05  -1.215442E-06
  b2   56  -1.064368E-05   2.049579E-05  -2.421497E-05   2.959129E-05  -1.846439E-05   4.326289E-05
  b2   57   5.637492E-04  -3.855343E-06   1.635246E-05  -2.071517E-05  -3.145262E-05   1.643620E-05
  b2   58  -3.855343E-06   5.459653E-04  -3.968751E-05   4.390481E-05   2.343371E-06   2.455467E-06
  b2   59   1.635246E-05  -3.968751E-05   5.024604E-04  -4.833398E-05   5.163839E-06  -1.275584E-04
  b2   60  -2.071517E-05   4.390481E-05  -4.833398E-05   4.175385E-04   3.799351E-05  -1.395260E-05
  b2   61  -3.145262E-05   2.343371E-06   5.163839E-06   3.799351E-05   5.099727E-04  -6.017483E-05
  b2   62   1.643620E-05   2.455467E-06  -1.275584E-04  -1.395260E-05  -6.017483E-05   5.069282E-04

Natural orbital populations,block 3
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     2.00000000     2.00000000     1.98989888     1.98942069     1.98718962     1.98459260     1.98341601
              MO     9       MO    10       MO    11       MO    12       MO    13       MO    14       MO    15       MO    16
  occ(*)=     1.98218588     1.98169690     1.98003268     1.97675937     0.01875764     0.01129469     0.00998036     0.00971435
              MO    17       MO    18       MO    19       MO    20       MO    21       MO    22       MO    23       MO    24
  occ(*)=     0.00761863     0.00752745     0.00714029     0.00693450     0.00636967     0.00496272     0.00402927     0.00353582
              MO    25       MO    26       MO    27       MO    28       MO    29       MO    30       MO    31       MO    32
  occ(*)=     0.00340557     0.00285162     0.00257064     0.00248828     0.00215893     0.00207282     0.00172600     0.00152919
              MO    33       MO    34       MO    35       MO    36       MO    37       MO    38       MO    39       MO    40
  occ(*)=     0.00120075     0.00109743     0.00103260     0.00093088     0.00088690     0.00077936     0.00071612     0.00064106
              MO    41       MO    42       MO    43       MO    44       MO    45       MO    46       MO    47       MO    48
  occ(*)=     0.00061748     0.00059366     0.00056308     0.00049586     0.00044815     0.00035636     0.00032797     0.00029277
              MO    49       MO    50       MO    51       MO    52       MO    53       MO    54       MO    55       MO    56
  occ(*)=     0.00027791     0.00026436     0.00022978     0.00019318     0.00019014     0.00017698     0.00012349     0.00009751
              MO    57       MO    58       MO    59       MO    60       MO    61       MO    62
  occ(*)=     0.00008525     0.00007762     0.00006454     0.00005213     0.00003754     0.00003222

          modens reordered block   1

               a2    1        a2    2        a2    3        a2    4        a2    5        a2    6        a2    7        a2    8
  a2    1    1.98514       7.573949E-04   6.899535E-04  -3.420434E-03   3.073423E-03   3.564590E-04   3.046152E-04   3.285013E-03
  a2    2   7.573949E-04    1.74137       7.040729E-02   2.130721E-03   9.856025E-04   1.003497E-03   1.611775E-03  -9.748055E-04
  a2    3   6.899535E-04   7.040729E-02   0.795119       5.599265E-03   4.148710E-03   1.884640E-03   1.445993E-02  -4.674637E-03
  a2    4  -3.420434E-03   2.130721E-03   5.599265E-03   3.584431E-03  -1.695396E-03  -4.475216E-04   1.011940E-04  -3.021874E-03
  a2    5   3.073423E-03   9.856025E-04   4.148710E-03  -1.695396E-03   4.279716E-03  -8.625103E-05   5.303735E-04   1.075365E-03
  a2    6   3.564590E-04   1.003497E-03   1.884640E-03  -4.475216E-04  -8.625103E-05   3.051639E-03  -9.239329E-05   1.609560E-04
  a2    7   3.046152E-04   1.611775E-03   1.445993E-02   1.011940E-04   5.303735E-04  -9.239329E-05   3.080526E-03  -7.295104E-05
  a2    8   3.285013E-03  -9.748055E-04  -4.674637E-03  -3.021874E-03   1.075365E-03   1.609560E-04  -7.295104E-05   3.144771E-03
  a2    9  -5.216875E-04  -2.779497E-03   9.069699E-04   3.974049E-05  -2.961551E-04   2.158551E-05  -2.222099E-04   1.186633E-04
  a2   10   3.163803E-04  -2.541876E-03   9.514928E-04  -6.756960E-05  -8.106085E-05  -7.828018E-05   2.836459E-04   1.019848E-04
  a2   11  -9.907546E-05   1.085830E-03  -4.740657E-04   2.192464E-04   1.878970E-04  -5.916629E-04   1.937423E-05  -4.901957E-05
  a2   12   1.063905E-04   8.699493E-05   3.303902E-03  -1.600259E-04   1.379226E-04   9.581972E-05  -3.282619E-05  -1.330466E-04
  a2   13  -1.608746E-05  -5.186973E-04   1.181262E-03  -5.631872E-05   2.282305E-05  -1.501240E-04  -2.104623E-04   1.734044E-05
  a2   14   2.224692E-04  -7.498669E-05   1.137421E-03  -4.949637E-05   4.058878E-05  -1.759666E-05   1.708644E-04   8.559190E-06
  a2   15   1.885882E-04   1.895322E-04   5.014586E-03   6.330899E-05   3.588253E-04   5.912939E-05   7.045534E-04  -2.208942E-04
  a2   16  -6.502175E-04   1.901439E-04   2.715875E-03   5.201666E-05  -2.747224E-04   4.955883E-05   2.915698E-04  -4.657995E-05
  a2   17  -3.785367E-03   1.134767E-04  -2.842801E-04  -2.093902E-04  -9.185482E-04  -2.963444E-04   2.083933E-04   4.789272E-04
  a2   18   1.778831E-04   2.152559E-04  -1.843850E-04   7.638860E-05   1.529029E-05   5.855380E-05  -1.829002E-05  -3.208955E-05
  a2   19   6.004719E-05  -1.327727E-04   8.852865E-04  -2.885960E-05   1.337932E-04  -1.854068E-04   5.996741E-05  -1.118353E-04
  a2   20   2.586633E-04  -1.665838E-04  -8.953490E-05   7.891671E-05  -6.606386E-05   2.918462E-05  -6.584425E-05   5.623147E-05
  a2   21  -3.125533E-04   1.195142E-04  -5.725278E-04  -3.163403E-05  -5.775408E-05  -4.469260E-05  -1.860699E-04  -3.269683E-05
  a2   22   4.946246E-04  -1.644823E-04   1.898778E-04   7.354250E-05   3.274935E-06  -6.433117E-05  -9.482871E-05   1.062566E-04
  a2   23   1.256269E-04   5.902685E-04  -1.583641E-05  -6.124205E-05   7.351695E-05   4.532929E-05  -1.806525E-05   4.967591E-05
  a2   24   1.726219E-03  -1.004051E-04  -1.193050E-04  -4.870360E-04   4.133218E-04  -1.451920E-05   1.579156E-04   7.658309E-04
  a2   25  -3.597607E-05  -2.380606E-05   7.901555E-05   2.722803E-04  -3.985207E-04  -9.057955E-05   5.922956E-06  -1.329427E-04

               a2    9        a2   10        a2   11        a2   12        a2   13        a2   14        a2   15        a2   16
  a2    1  -5.216875E-04   3.163803E-04  -9.907546E-05   1.063905E-04  -1.608746E-05   2.224692E-04   1.885882E-04  -6.502175E-04
  a2    2  -2.779497E-03  -2.541876E-03   1.085830E-03   8.699493E-05  -5.186973E-04  -7.498669E-05   1.895322E-04   1.901439E-04
  a2    3   9.069699E-04   9.514928E-04  -4.740657E-04   3.303902E-03   1.181262E-03   1.137421E-03   5.014586E-03   2.715875E-03
  a2    4   3.974049E-05  -6.756960E-05   2.192464E-04  -1.600259E-04  -5.631872E-05  -4.949637E-05   6.330899E-05   5.201666E-05
  a2    5  -2.961551E-04  -8.106085E-05   1.878970E-04   1.379226E-04   2.282305E-05   4.058878E-05   3.588253E-04  -2.747224E-04
  a2    6   2.158551E-05  -7.828018E-05  -5.916629E-04   9.581972E-05  -1.501240E-04  -1.759666E-05   5.912939E-05   4.955883E-05
  a2    7  -2.222099E-04   2.836459E-04   1.937423E-05  -3.282619E-05  -2.104623E-04   1.708644E-04   7.045534E-04   2.915698E-04
  a2    8   1.186633E-04   1.019848E-04  -4.901957E-05  -1.330466E-04   1.734044E-05   8.559190E-06  -2.208942E-04  -4.657995E-05
  a2    9   3.759254E-03   4.154830E-04  -2.599770E-05  -2.166335E-04   8.521627E-05  -2.818171E-05  -2.135636E-04   3.584519E-04
  a2   10   4.154830E-04   3.523656E-03  -1.228477E-04  -9.442884E-05   2.257635E-04  -3.440816E-06   2.265373E-04  -3.440508E-04
  a2   11  -2.599770E-05  -1.228477E-04   2.737708E-03   1.414611E-05  -5.200128E-05   2.444224E-05  -1.195822E-05   2.748314E-05
  a2   12  -2.166335E-04  -9.442884E-05   1.414611E-05   2.824908E-03   1.681044E-04   4.349364E-05   1.630365E-04   2.775164E-04
  a2   13   8.521627E-05   2.257635E-04  -5.200128E-05   1.681044E-04   2.150720E-03   3.688064E-05   1.566328E-04   1.743964E-04
  a2   14  -2.818171E-05  -3.440816E-06   2.444224E-05   4.349364E-05   3.688064E-05   8.093245E-04   1.354367E-04   3.436713E-05
  a2   15  -2.135636E-04   2.265373E-04  -1.195822E-05   1.630365E-04   1.566328E-04   1.354367E-04   1.369799E-03  -8.080960E-06
  a2   16   3.584519E-04  -3.440508E-04   2.748314E-05   2.775164E-04   1.743964E-04   3.436713E-05  -8.080960E-06   1.269564E-03
  a2   17   1.631260E-04  -1.333625E-04  -4.311596E-05   8.322058E-06  -1.571538E-05   2.205822E-05  -1.536574E-04   2.046764E-04
  a2   18  -7.715223E-05   4.485486E-05  -2.705523E-04  -4.816352E-05   1.008466E-04  -6.334353E-06   1.618458E-05  -1.841583E-06
  a2   19   2.100438E-05   4.216420E-05  -3.176788E-04   8.702240E-05  -1.602648E-04   2.049962E-05   7.812396E-05  -1.666467E-04
  a2   20   2.261369E-04   1.085856E-05   2.294482E-05   3.889532E-04  -3.744731E-04  -5.304148E-05  -2.250569E-04   6.901251E-05
  a2   21  -1.946028E-04   1.299388E-04  -1.909555E-05   1.657421E-04   2.905588E-04  -2.711227E-05  -1.132314E-04  -1.636040E-04
  a2   22   2.661418E-04  -3.148856E-04  -8.961710E-05  -8.501749E-05   3.319157E-04  -1.930050E-05  -1.269843E-04   2.438422E-04
  a2   23   1.116660E-04  -2.469266E-05   2.085924E-05   4.503979E-05  -7.120137E-05   6.398498E-06   3.032476E-05  -5.606019E-05
  a2   24   6.276319E-05  -6.019487E-05  -6.296076E-06   2.097355E-05   2.766622E-06   1.121768E-05  -1.160266E-04   1.637907E-04
  a2   25   1.108995E-04  -5.769132E-05  -9.046006E-06  -3.492787E-05  -2.212509E-05  -4.995315E-05  -2.996612E-04   4.245863E-04

               a2   17        a2   18        a2   19        a2   20        a2   21        a2   22        a2   23        a2   24
  a2    1  -3.785367E-03   1.778831E-04   6.004719E-05   2.586633E-04  -3.125533E-04   4.946246E-04   1.256269E-04   1.726219E-03
  a2    2   1.134767E-04   2.152559E-04  -1.327727E-04  -1.665838E-04   1.195142E-04  -1.644823E-04   5.902685E-04  -1.004051E-04
  a2    3  -2.842801E-04  -1.843850E-04   8.852865E-04  -8.953490E-05  -5.725278E-04   1.898778E-04  -1.583641E-05  -1.193050E-04
  a2    4  -2.093902E-04   7.638860E-05  -2.885960E-05   7.891671E-05  -3.163403E-05   7.354250E-05  -6.124205E-05  -4.870360E-04
  a2    5  -9.185482E-04   1.529029E-05   1.337932E-04  -6.606386E-05  -5.775408E-05   3.274935E-06   7.351695E-05   4.133218E-04
  a2    6  -2.963444E-04   5.855380E-05  -1.854068E-04   2.918462E-05  -4.469260E-05  -6.433117E-05   4.532929E-05  -1.451920E-05
  a2    7   2.083933E-04  -1.829002E-05   5.996741E-05  -6.584425E-05  -1.860699E-04  -9.482871E-05  -1.806525E-05   1.579156E-04
  a2    8   4.789272E-04  -3.208955E-05  -1.118353E-04   5.623147E-05  -3.269683E-05   1.062566E-04   4.967591E-05   7.658309E-04
  a2    9   1.631260E-04  -7.715223E-05   2.100438E-05   2.261369E-04  -1.946028E-04   2.661418E-04   1.116660E-04   6.276319E-05
  a2   10  -1.333625E-04   4.485486E-05   4.216420E-05   1.085856E-05   1.299388E-04  -3.148856E-04  -2.469266E-05  -6.019487E-05
  a2   11  -4.311596E-05  -2.705523E-04  -3.176788E-04   2.294482E-05  -1.909555E-05  -8.961710E-05   2.085924E-05  -6.296076E-06
  a2   12   8.322058E-06  -4.816352E-05   8.702240E-05   3.889532E-04   1.657421E-04  -8.501749E-05   4.503979E-05   2.097355E-05
  a2   13  -1.571538E-05   1.008466E-04  -1.602648E-04  -3.744731E-04   2.905588E-04   3.319157E-04  -7.120137E-05   2.766622E-06
  a2   14   2.205822E-05  -6.334353E-06   2.049962E-05  -5.304148E-05  -2.711227E-05  -1.930050E-05   6.398498E-06   1.121768E-05
  a2   15  -1.536574E-04   1.618458E-05   7.812396E-05  -2.250569E-04  -1.132314E-04  -1.269843E-04   3.032476E-05  -1.160266E-04
  a2   16   2.046764E-04  -1.841583E-06  -1.666467E-04   6.901251E-05  -1.636040E-04   2.438422E-04  -5.606019E-05   1.637907E-04
  a2   17   1.095708E-03  -8.771380E-05   4.400886E-05   2.121345E-06  -5.439771E-06  -5.345870E-06  -4.787411E-06   2.587955E-04
  a2   18  -8.771380E-05   6.289676E-04  -1.611592E-04  -1.284216E-04   6.717210E-05   1.249583E-05  -1.643246E-05  -3.056119E-05
  a2   19   4.400886E-05  -1.611592E-04   9.183645E-04   2.023703E-04  -6.321393E-05  -4.356187E-05   3.415535E-05  -3.859136E-05
  a2   20   2.121345E-06  -1.284216E-04   2.023703E-04   1.065639E-03  -1.945388E-04  -3.361669E-04   3.126066E-05   8.668959E-05
  a2   21  -5.439771E-06   6.717210E-05  -6.321393E-05  -1.945388E-04   5.564053E-04   8.918929E-05   4.460352E-06  -5.882884E-05
  a2   22  -5.345870E-06   1.249583E-05  -4.356187E-05  -3.361669E-04   8.918929E-05   8.018081E-04  -1.102494E-04   1.371556E-04
  a2   23  -4.787411E-06  -1.643246E-05   3.415535E-05   3.126066E-05   4.460352E-06  -1.102494E-04   5.107425E-04   1.289686E-05
  a2   24   2.587955E-04  -3.056119E-05  -3.859136E-05   8.668959E-05  -5.882884E-05   1.371556E-04   1.289686E-05   5.510155E-04
  a2   25   9.337437E-05  -4.494904E-05  -2.158323E-05   1.713940E-04  -8.749558E-05   2.493166E-04  -1.114558E-05   1.049898E-04

               a2   25
  a2    1  -3.597607E-05
  a2    2  -2.380606E-05
  a2    3   7.901555E-05
  a2    4   2.722803E-04
  a2    5  -3.985207E-04
  a2    6  -9.057955E-05
  a2    7   5.922956E-06
  a2    8  -1.329427E-04
  a2    9   1.108995E-04
  a2   10  -5.769132E-05
  a2   11  -9.046006E-06
  a2   12  -3.492787E-05
  a2   13  -2.212509E-05
  a2   14  -4.995315E-05
  a2   15  -2.996612E-04
  a2   16   4.245863E-04
  a2   17   9.337437E-05
  a2   18  -4.494904E-05
  a2   19  -2.158323E-05
  a2   20   1.713940E-04
  a2   21  -8.749558E-05
  a2   22   2.493166E-04
  a2   23  -1.114558E-05
  a2   24   1.049898E-04
  a2   25   7.019631E-04

Natural orbital populations,block 4
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     1.98517268     1.74659908     0.79032176     0.00775766     0.00444289     0.00392352     0.00358917     0.00319008
              MO     9       MO    10       MO    11       MO    12       MO    13       MO    14       MO    15       MO    16
  occ(*)=     0.00302994     0.00293727     0.00255789     0.00233327     0.00149635     0.00103684     0.00099807     0.00084096
              MO    17       MO    18       MO    19       MO    20       MO    21       MO    22       MO    23       MO    24
  occ(*)=     0.00077093     0.00063898     0.00057390     0.00050070     0.00046872     0.00035266     0.00028830     0.00018106
              MO    25
  occ(*)=     0.00004940


 total number of electrons =   78.0000000000



          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        a1  partial gross atomic populations
   ao class       1a1        2a1        3a1        4a1        5a1        6a1 
    C1_ s       0.000000   0.000065   0.000000   0.000013   1.431265   0.559856
    C1_ p       0.000000   0.000164  -0.000000   0.000079   0.000252   0.000566
    C1_ d      -0.000000   0.000000  -0.000000  -0.000000  -0.000023  -0.000150
    C2_ s      -0.000001  -0.000000  -0.000000  -0.000159   0.005081   0.002169
    C2_ p      -0.000004  -0.000001   0.000000  -0.000258   0.001589  -0.000125
    C2_ d       0.000000  -0.000000  -0.000000   0.000000  -0.000024  -0.000032
    C3_ s       0.000102  -0.000000   0.000005   0.002331   0.000003  -0.000022
    C3_ p      -0.000159   0.000000   0.000008   0.001059   0.000037  -0.000047
    C3_ d      -0.000000  -0.000000  -0.000000  -0.000059   0.000001  -0.000000
    C4_ s       0.000179   0.000000  -0.000015   1.998916   0.000019  -0.000001
    C4_ p       0.001527   0.000000   0.000156  -0.000026   0.000018   0.000004
    C4_ d      -0.000038   0.000000   0.000000   0.000001   0.000000   0.000000
    C5_ s      -0.000000  -0.000391  -0.000000   0.000000   0.562519   1.438187
    C5_ p       0.000000   0.002239  -0.000000   0.000000  -0.000495   0.000858
    C5_ d      -0.000000  -0.000500  -0.000000  -0.000000   0.000080  -0.000206
    C6_ s       0.000344   0.000000   1.999609  -0.000020   0.000000  -0.000000
    C6_ p       0.000250   0.000000   0.000042   0.000031   0.000000  -0.000000
    C6_ d      -0.000065  -0.000000   0.000000   0.000001  -0.000000  -0.000000
    N1_ s      -0.000000   1.998418  -0.000000  -0.000000  -0.000221  -0.000669
    N1_ p      -0.000000   0.000006   0.000000   0.000000  -0.000171  -0.000408
    N1_ d      -0.000000   0.000001   0.000000  -0.000000   0.000015   0.000018
    N2_ s       1.997879   0.000000  -0.000200  -0.000704  -0.000000   0.000000
    N2_ p      -0.000000  -0.000000  -0.000054  -0.001258  -0.000000   0.000000
    N2_ d      -0.000000  -0.000000  -0.000023  -0.000050  -0.000000  -0.000000
    H1_ s      -0.000000  -0.000000   0.000000  -0.000002   0.000056   0.000001
    H1_ p      -0.000000  -0.000000  -0.000000  -0.000000   0.000000   0.000001
    H2_ s       0.000016   0.000000   0.000007   0.000112  -0.000002  -0.000000
    H2_ p       0.000000  -0.000000   0.000000   0.000000   0.000000  -0.000000
    H3_ s       0.000000  -0.000000   0.000081   0.000003   0.000000  -0.000000
    H3_ p      -0.000000  -0.000000  -0.000069  -0.000000  -0.000000  -0.000000
    H4_ s      -0.000030  -0.000000   0.000581  -0.000009  -0.000000   0.000000
    H4_ p      -0.000000  -0.000000  -0.000130   0.000000  -0.000000  -0.000000
 
   ao class       7a1        8a1        9a1       10a1       11a1       12a1 
    C1_ s       0.002567   0.004386   0.094702   0.254271   0.041304   0.239138
    C1_ p       0.000522   0.000340  -0.012231   0.010859   0.046179   0.001031
    C1_ d      -0.000020  -0.000056   0.000910   0.002446   0.001428   0.002752
    C2_ s       0.696583   1.295115   0.248588   0.195040   0.248343   0.198682
    C2_ p      -0.000027   0.000339   0.028098   0.008056   0.002974   0.077882
    C2_ d       0.000001  -0.000041   0.001910   0.002331   0.001643   0.005163
    C3_ s       1.299304   0.699161   0.248123   0.020707   0.258566   0.176943
    C3_ p      -0.000006   0.000493   0.012451   0.028356   0.026773   0.089414
    C3_ d       0.000005  -0.000050   0.003102   0.000690   0.001691   0.005424
    C4_ s       0.000697   0.000391   0.280465   0.002199   0.044340   0.266805
    C4_ p       0.000190   0.000057  -0.007851   0.003763   0.015962  -0.000596
    C4_ d      -0.000046  -0.000034   0.004378   0.000586   0.001144   0.002545
    C5_ s       0.000027   0.000026   0.017203   0.345236   0.162159   0.003097
    C5_ p       0.000058   0.000046  -0.002747   0.012039   0.062963   0.085470
    C5_ d      -0.000001  -0.000001   0.001300   0.014533   0.005650   0.000076
    C6_ s       0.000001  -0.000001   0.303196   0.177089   0.178572   0.183534
    C6_ p       0.000005  -0.000001  -0.003592  -0.001913   0.002527   0.065781
    C6_ d       0.000000   0.000000   0.003172   0.001197   0.000573   0.001105
    N1_ s       0.000000   0.000000   0.025256   0.586041   0.595006   0.110501
    N1_ p      -0.000000  -0.000000   0.000603   0.002267  -0.000646   0.003591
    N1_ d      -0.000000  -0.000000   0.000096   0.001630   0.001274   0.000087
    N2_ s      -0.000001   0.000000   0.501881   0.179349   0.089237   0.034371
    N2_ p       0.000003  -0.000002   0.002067   0.002059   0.016191   0.105141
    N2_ d      -0.000000  -0.000000   0.001996   0.000692   0.000502   0.000849
    H1_ s       0.000073  -0.000030   0.024227   0.035197   0.033118   0.063695
    H1_ p      -0.000024  -0.000077   0.001174   0.001950   0.002045   0.002899
    H2_ s       0.000148  -0.000012   0.047588   0.000218   0.028406   0.046339
    H2_ p      -0.000058  -0.000049   0.002925   0.000127   0.001759   0.002516
    H3_ s       0.000000  -0.000000   0.056704   0.046154   0.062024   0.139548
    H3_ p      -0.000000  -0.000000   0.003220   0.002196   0.002629   0.004167
    H4_ s      -0.000001   0.000001   0.096084   0.051850   0.052064   0.065614
    H4_ p      -0.000000  -0.000000   0.006617   0.003903   0.004187   0.005558
 
   ao class      13a1       14a1       15a1       16a1       17a1       18a1 
    C1_ s       0.137881   0.082087   0.095194  -0.003517   0.109481   0.033171
    C1_ p       0.000046   0.020907   0.006824   0.060469   0.002949   0.226695
    C1_ d       0.000621   0.005718   0.002227   0.002122   0.000816   0.000421
    C2_ s       0.000815   0.087208   0.073044   0.043916   0.009792   0.028411
    C2_ p       0.132629   0.435953   0.051867   0.047968   0.156538   0.258387
    C2_ d       0.002768   0.000928   0.004719   0.003965   0.014632  -0.000032
    C3_ s       0.247765   0.009248   0.000042   0.032429   0.022775   0.004664
    C3_ p       0.044164   0.256333   0.104446   0.207228   0.298839   0.242399
    C3_ d       0.001743   0.003182   0.002975   0.001274   0.014982   0.008316
    C4_ s       0.035282   0.075846  -0.005472   0.000256   0.178561   0.127390
    C4_ p       0.038175   0.074422   0.000682   0.007349   0.039038  -0.001699
    C4_ d       0.010268   0.000204   0.001814   0.003453   0.000413   0.003427
    C5_ s       0.029300   0.014864   0.089726   0.267342   0.038701  -0.000350
    C5_ p       0.065471   0.015471   0.065461   0.061880   0.062093   0.240378
    C5_ d       0.000443   0.000339   0.000585  -0.000026   0.000231   0.000596
    C6_ s       0.093335   0.001365   0.020081   0.011533   0.005587   0.001043
    C6_ p       0.191936   0.188822   0.608906   0.203191   0.093184   0.094370
    C6_ d       0.012977   0.004127   0.007447   0.003410   0.003715   0.006050
    N1_ s       0.045097   0.004295   0.052140   0.141289   0.028763   0.008998
    N1_ p       0.009045   0.002004   0.056321   0.502394   0.310574   0.433732
    N1_ d       0.000000   0.000004   0.000257   0.003581   0.002609   0.004143
    N2_ s       0.483198   0.075629   0.031710  -0.000059   0.050756   0.057568
    N2_ p       0.024071  -0.000258   0.041871   0.009779   0.057459   0.072252
    N2_ d       0.000642   0.001305   0.002703   0.001339   0.000140   0.002279
    H1_ s      -0.000024   0.411475   0.080097  -0.000492   0.168757   0.067336
    H1_ p       0.000378   0.008862   0.002082  -0.000026   0.002100   0.000601
    H2_ s       0.166458   0.058814   0.059394   0.169381   0.259431   0.007232
    H2_ p       0.004271   0.001048   0.000451   0.004041   0.004359   0.000902
    H3_ s       0.081333   0.135975   0.226002   0.093899   0.000187   0.054252
    H3_ p       0.002464   0.001518   0.002986   0.000956   0.000389   0.000224
    H4_ s       0.119609   0.006925   0.289980   0.101302   0.044803  -0.000191
    H4_ p       0.005400   0.001469   0.009414   0.003423   0.001690   0.000747
 
   ao class      19a1       20a1       21a1       22a1       23a1       24a1 
    C1_ s       0.014343   0.002520   0.000016   0.001501   0.000015   0.000336
    C1_ p       0.423972   0.033375   0.000048   0.001361   0.000040   0.000528
    C1_ d       0.017197   0.006022   0.000018   0.000073   0.000005   0.000024
    C2_ s       0.006849   0.009999   0.000123   0.000198   0.000016   0.000528
    C2_ p       0.317151   0.219036   0.000559   0.000277   0.000141   0.000299
    C2_ d       0.020416   0.005332   0.000062   0.000344   0.000044   0.000243
    C3_ s       0.042714   0.000109   0.000415   0.000037   0.000071   0.000959
    C3_ p       0.212899   0.325266   0.000527   0.000353   0.000187   0.002282
    C3_ d       0.000687   0.015727   0.000556   0.000062   0.000098   0.000733
    C4_ s       0.013781   0.000626   0.002157   0.000066   0.000431   0.001176
    C4_ p       0.117451   0.418220   0.001419   0.000010   0.000133   0.001542
    C4_ d       0.000048   0.022392   0.000241   0.000007   0.000082   0.000395
    C5_ s       0.100950   0.017011   0.000009   0.004186   0.000010   0.000102
    C5_ p       0.268002   0.026621   0.000001  -0.000157  -0.000000   0.000049
    C5_ d       0.000045   0.000015   0.000000   0.000380   0.000001   0.000033
    C6_ s       0.006268   0.009016   0.000320   0.000001   0.001033   0.000014
    C6_ p       0.065648   0.150818   0.001270   0.000003   0.003520   0.000013
    C6_ d       0.006351   0.011183   0.000033   0.000001   0.000247   0.000003
    N1_ s       0.011107   0.001500   0.000001   0.000458   0.000001   0.000029
    N1_ p       0.053477   0.002170   0.000004   0.002629   0.000006   0.000130
    N1_ d       0.000591   0.000021  -0.000000  -0.000024   0.000000   0.000004
    N2_ s       0.006792   0.003649   0.002915   0.000002   0.001001  -0.000005
    N2_ p       0.188881   0.630771   0.000984   0.000013   0.003553   0.000088
    N2_ d       0.002596   0.003793   0.000348   0.000001   0.000328   0.000014
    H1_ s       0.001563   0.020166   0.000034   0.000068   0.000012   0.000021
    H1_ p       0.001452   0.001060   0.000009   0.000001   0.000004   0.000004
    H2_ s       0.046685   0.008968   0.000089   0.000028   0.000014   0.000371
    H2_ p       0.000463   0.001701  -0.000002   0.000008   0.000002   0.000058
    H3_ s       0.032504   0.030137   0.000040   0.000001   0.000041   0.000001
    H3_ p      -0.000106  -0.000119   0.000021   0.000000   0.000064   0.000000
    H4_ s      -0.000049   0.001234  -0.000001   0.000001   0.000148   0.000003
    H4_ p       0.000613   0.001445   0.000035   0.000000   0.000123   0.000001
 
   ao class      25a1       26a1       27a1       28a1       29a1       30a1 
    C1_ s       0.001328   0.000312  -0.000002   0.000011   0.000295   0.000113
    C1_ p       0.000765   0.000157   0.000015   0.000002   0.000147   0.000208
    C1_ d       0.000543   0.000212   0.000119   0.000040   0.000160   0.000038
    C2_ s       0.000709   0.001169   0.000602   0.000504   0.000004  -0.000007
    C2_ p       0.002751   0.000861   0.000406   0.000473   0.000256   0.000102
    C2_ d       0.000581   0.000193   0.000367   0.000213   0.000048   0.000038
    C3_ s       0.000020   0.002204   0.000027   0.000009   0.000131   0.000079
    C3_ p       0.001093   0.000439   0.000473   0.000501   0.001347   0.000303
    C3_ d       0.000085   0.000304   0.000280   0.000100   0.000035   0.000058
    C4_ s       0.000441   0.000024   0.000014   0.000079   0.000029  -0.000005
    C4_ p       0.000001   0.000375   0.000370   0.000247   0.000028   0.000039
    C4_ d       0.000115  -0.000008   0.000120   0.000018   0.000012   0.000005
    C5_ s       0.000014   0.000146   0.000014   0.000001   0.000002  -0.000002
    C5_ p       0.000017   0.000578   0.000163   0.000068   0.001115   0.000574
    C5_ d      -0.000001   0.000094   0.000007   0.000002   0.000008   0.000000
    C6_ s       0.000000   0.000046   0.000002   0.000152   0.000927   0.001561
    C6_ p       0.000014   0.000029   0.001810   0.002137   0.000299   0.000778
    C6_ d       0.000015   0.000026   0.000121   0.000096   0.000026   0.000046
    N1_ s       0.000006   0.000117   0.000025   0.000009   0.000099   0.000047
    N1_ p       0.000021   0.000439   0.000078   0.000024   0.000380   0.000166
    N1_ d       0.000000   0.000038   0.000014   0.000007   0.000121   0.000073
    N2_ s       0.000025   0.000033   0.000042   0.000009   0.000000   0.000002
    N2_ p       0.000076   0.000029   0.000058   0.000406   0.000005   0.000028
    N2_ d       0.000027   0.000027   0.000244   0.000013   0.000012   0.000006
    H1_ s       0.000528   0.000003   0.001020   0.000787   0.000066   0.000013
    H1_ p       0.000062   0.000005  -0.000031  -0.000018  -0.000002  -0.000000
    H2_ s       0.000128   0.000932   0.000328   0.000099   0.000573   0.000314
    H2_ p       0.000018  -0.000020  -0.000003   0.000002   0.000036  -0.000010
    H3_ s       0.000020   0.000062   0.000986   0.001406   0.000184   0.000308
    H3_ p      -0.000001  -0.000002  -0.000009  -0.000006   0.000010   0.000032
    H4_ s       0.000011   0.000001   0.000431   0.000213   0.000640   0.001706
    H4_ p      -0.000000   0.000005   0.000010   0.000049  -0.000008  -0.000075
 
   ao class      31a1       32a1       33a1       34a1       35a1       36a1 
    C1_ s      -0.000003  -0.000028   0.000126   0.000075   0.000187   0.000276
    C1_ p       0.000633   0.000226  -0.000078   0.000016   0.000018   0.000031
    C1_ d      -0.000003   0.000186   0.000067   0.000115   0.000211   0.000258
    C2_ s       0.000054   0.000082   0.000105  -0.000026   0.000151   0.000247
    C2_ p       0.001909   0.000436   0.000184   0.000165   0.000073  -0.000010
    C2_ d       0.000020   0.000347   0.000136   0.000222   0.000331   0.000195
    C3_ s       0.000060  -0.000004  -0.000009   0.000274   0.000320   0.000024
    C3_ p       0.000845   0.000475   0.000234   0.000058   0.000012  -0.000027
    C3_ d       0.000013   0.000410   0.000189   0.000314   0.000307   0.000053
    C4_ s       0.000014   0.000021   0.000001   0.000255   0.000070   0.000113
    C4_ p       0.000209  -0.000051  -0.000017   0.000009   0.000039   0.000102
    C4_ d       0.000031   0.000220   0.000108   0.000216   0.000198   0.000132
    C5_ s       0.000012   0.000030  -0.000114   0.000003   0.000068   0.000091
    C5_ p       0.000598   0.000172   0.000337   0.000007   0.000015  -0.000005
    C5_ d       0.000002   0.000096   0.000038   0.000005   0.000124   0.000177
    C6_ s       0.000045   0.000001   0.000000   0.000002  -0.000025  -0.000025
    C6_ p       0.000039   0.000194   0.000138   0.000372   0.000117   0.000138
    C6_ d       0.000022   0.000176   0.000152   0.000778   0.000271   0.000207
    N1_ s       0.000046   0.001044   0.002031   0.000021   0.000023   0.000021
    N1_ p       0.000136   0.000330   0.000781   0.000021   0.000181   0.000230
    N1_ d       0.000090   0.000080   0.000003   0.000011   0.000207   0.000274
    N2_ s       0.000012   0.000012   0.000005   0.000015   0.000157   0.000311
    N2_ p       0.000042   0.000129   0.000106   0.000546   0.000064   0.000015
    N2_ d       0.000037   0.000172   0.000141   0.000564   0.000425   0.000649
    H1_ s       0.000713   0.000030   0.000005   0.000005   0.000023   0.000023
    H1_ p       0.000014   0.000005  -0.000001   0.000006   0.000026   0.000018
    H2_ s       0.000284   0.000022   0.000001   0.000003   0.000070   0.000015
    H2_ p       0.000010   0.000018   0.000011   0.000004   0.000023   0.000010
    H3_ s       0.000002   0.000009   0.000007   0.000031   0.000000   0.000001
    H3_ p      -0.000000   0.000030   0.000026   0.000132   0.000045   0.000025
    H4_ s       0.000081   0.000017   0.000012   0.000047   0.000015   0.000011
    H4_ p       0.000001   0.000010   0.000008   0.000036   0.000029   0.000027
 
   ao class      37a1       38a1       39a1       40a1       41a1       42a1 
    C1_ s       0.000071   0.000007   0.000221  -0.000001   0.000015   0.000001
    C1_ p      -0.000064  -0.000001   0.000057   0.000074   0.000062   0.000072
    C1_ d       0.000015   0.000006   0.000326   0.000005   0.000050   0.000269
    C2_ s       0.000280  -0.000014   0.000086   0.000021  -0.000037  -0.000003
    C2_ p       0.000347   0.000046   0.000057   0.000123   0.000018   0.000170
    C2_ d       0.000654   0.000034   0.000259   0.000170   0.000081   0.000190
    C3_ s       0.000225  -0.000009   0.000097   0.000007   0.000023  -0.000002
    C3_ p       0.000220   0.000148   0.000060   0.000011   0.000199   0.000143
    C3_ d       0.000773   0.000082   0.000111   0.000058   0.000378   0.000167
    C4_ s       0.000009   0.000018   0.000046   0.000000   0.000055   0.000003
    C4_ p      -0.000005   0.000079   0.000011   0.000009   0.000073   0.000029
    C4_ d       0.000000   0.000005   0.000160   0.000006   0.000157   0.000322
    C5_ s       0.000098   0.000005   0.000062   0.000014   0.000010   0.000006
    C5_ p       0.000000   0.000000   0.000055   0.000061   0.000005   0.000044
    C5_ d       0.000078   0.000002   0.000293   0.000719   0.000175   0.000009
    C6_ s       0.000005  -0.000014  -0.000000   0.000001   0.000015   0.000083
    C6_ p       0.000027   0.000536   0.000008   0.000027   0.000180   0.000045
    C6_ d       0.000064   0.001584   0.000018   0.000039   0.000313   0.000235
    N1_ s       0.000008   0.000000   0.000033   0.000025   0.000002   0.000020
    N1_ p       0.000096   0.000004   0.000235   0.000111   0.000013   0.000058
    N1_ d       0.000128   0.000004   0.000495   0.001029   0.000300   0.000127
    N2_ s       0.000065   0.000051   0.000009   0.000016   0.000058   0.000008
    N2_ p       0.000000   0.000008   0.000000   0.000007   0.000044   0.000029
    N2_ d       0.000109   0.000161   0.000015   0.000024   0.000134   0.000011
    H1_ s       0.000040  -0.000003   0.000002   0.000013   0.000002  -0.000000
    H1_ p       0.000086   0.000007   0.000033   0.000029   0.000008   0.000020
    H2_ s       0.000043   0.000084   0.000003   0.000002   0.000005   0.000002
    H2_ p       0.000106   0.000031  -0.000001   0.000008   0.000031   0.000015
    H3_ s       0.000000   0.000042   0.000000   0.000007   0.000050   0.000063
    H3_ p       0.000000   0.000094   0.000002   0.000006   0.000044   0.000026
    H4_ s       0.000001   0.000015   0.000000  -0.000000  -0.000004  -0.000007
    H4_ p       0.000016   0.000400   0.000003   0.000011   0.000075   0.000068
 
   ao class      43a1       44a1       45a1       46a1       47a1       48a1 
    C1_ s       0.000018   0.000010   0.000081   0.000004   0.000027   0.000153
    C1_ p       0.000000   0.000027   0.000100   0.000132  -0.000001   0.000002
    C1_ d       0.000035   0.000006   0.000277   0.000022   0.000057   0.000100
    C2_ s      -0.000025   0.000033  -0.000035   0.000001   0.000139  -0.000063
    C2_ p       0.000057   0.000047   0.000090   0.000121   0.000002   0.000034
    C2_ d       0.000033   0.000185   0.000269   0.000229   0.000091   0.000094
    C3_ s      -0.000001  -0.000022   0.000051  -0.000026   0.000023   0.000041
    C3_ p       0.000117   0.000117   0.000050   0.000166  -0.000016   0.000084
    C3_ d       0.000166   0.000227   0.000028   0.000121   0.000079   0.000167
    C4_ s       0.000005   0.000001   0.000022   0.000000   0.000028   0.000002
    C4_ p       0.000074  -0.000023  -0.000007   0.000003   0.000004   0.000001
    C4_ d       0.000049   0.000225   0.000050   0.000049   0.000054   0.000080
    C5_ s       0.000000   0.000000  -0.000011  -0.000000   0.000028   0.000056
    C5_ p       0.000006  -0.000004  -0.000004   0.000017   0.000019   0.000015
    C5_ d       0.000000   0.000002   0.000046   0.000032   0.000016   0.000019
    C6_ s       0.000196   0.000195   0.000018   0.000041   0.000079   0.000027
    C6_ p       0.000076   0.000025   0.000007   0.000018   0.000117   0.000036
    C6_ d       0.000593   0.000057   0.000023   0.000063   0.000020   0.000007
    N1_ s       0.000005   0.000002   0.000011   0.000006   0.000001   0.000000
    N1_ p       0.000010   0.000001   0.000014   0.000011   0.000003  -0.000000
    N1_ d       0.000015   0.000006   0.000129   0.000045   0.000034   0.000035
    N2_ s       0.000208   0.000037   0.000002   0.000002   0.000115   0.000048
    N2_ p      -0.000003   0.000009   0.000013   0.000021   0.000005   0.000003
    N2_ d       0.000180   0.000097   0.000046   0.000150   0.000259   0.000117
    H1_ s      -0.000007   0.000004   0.000004  -0.000010   0.000025   0.000022
    H1_ p       0.000010   0.000066   0.000150   0.000028  -0.000002   0.000016
    H2_ s      -0.000005   0.000018   0.000010   0.000015   0.000015   0.000002
    H2_ p       0.000017   0.000158   0.000007   0.000046   0.000012   0.000009
    H3_ s       0.000014   0.000015   0.000001  -0.000000   0.000053   0.000024
    H3_ p       0.000020   0.000029   0.000029   0.000082   0.000102   0.000044
    H4_ s       0.000012   0.000039   0.000011   0.000040  -0.000008  -0.000004
    H4_ p       0.000229   0.000024   0.000006   0.000024   0.000002   0.000000
 
   ao class      49a1       50a1       51a1       52a1       53a1       54a1 
    C1_ s      -0.000019   0.000000   0.000000   0.000005   0.000000   0.000017
    C1_ p       0.000012   0.000212   0.000108  -0.000011  -0.000001  -0.000002
    C1_ d       0.000144   0.000189   0.000013   0.000020   0.000027   0.000051
    C2_ s       0.000043   0.000008  -0.000003   0.000025  -0.000001   0.000001
    C2_ p       0.000075   0.000117  -0.000019   0.000040   0.000001   0.000051
    C2_ d       0.000034   0.000045   0.000082   0.000027   0.000012   0.000107
    C3_ s       0.000023   0.000015   0.000012   0.000036   0.000000   0.000011
    C3_ p       0.000091  -0.000008   0.000020   0.000018   0.000062   0.000099
    C3_ d       0.000142   0.000055   0.000040   0.000014   0.000010   0.000127
    C4_ s       0.000084   0.000061  -0.000002  -0.000003   0.000008   0.000006
    C4_ p      -0.000028   0.000014   0.000046   0.000005   0.000002   0.000015
    C4_ d       0.000154   0.000056   0.000118   0.000040   0.000095   0.000014
    C5_ s      -0.000003   0.000010  -0.000011   0.000001  -0.000001   0.000004
    C5_ p       0.000006  -0.000003  -0.000005   0.000000   0.000001   0.000000
    C5_ d       0.000049   0.000045   0.000039   0.000351   0.000034   0.000000
    C6_ s       0.000016   0.000001   0.000004   0.000029   0.000211   0.000014
    C6_ p       0.000007   0.000002   0.000024   0.000005   0.000004  -0.000002
    C6_ d       0.000009   0.000004   0.000041   0.000020   0.000108   0.000006
    N1_ s       0.000003   0.000001   0.000002   0.000000   0.000000   0.000000
    N1_ p       0.000004  -0.000000   0.000001   0.000000   0.000000   0.000001
    N1_ d       0.000036   0.000051   0.000032   0.000239   0.000021   0.000004
    N2_ s       0.000033   0.000003   0.000119   0.000019   0.000096   0.000000
    N2_ p       0.000004   0.000006   0.000046   0.000003  -0.000008   0.000075
    N2_ d       0.000154   0.000026   0.000129   0.000003   0.000004   0.000039
    H1_ s      -0.000003   0.000069   0.000001   0.000004   0.000000   0.000021
    H1_ p       0.000029   0.000002   0.000009   0.000010   0.000002   0.000012
    H2_ s      -0.000001   0.000011   0.000031   0.000001  -0.000001   0.000074
    H2_ p       0.000011   0.000013   0.000033   0.000001   0.000017   0.000036
    H3_ s       0.000002   0.000005   0.000018   0.000030   0.000103   0.000001
    H3_ p       0.000000   0.000016   0.000038   0.000009  -0.000005   0.000013
    H4_ s      -0.000001  -0.000000   0.000005   0.000004   0.000035   0.000001
    H4_ p       0.000015   0.000001   0.000009   0.000004   0.000060   0.000007
 
   ao class      55a1       56a1       57a1       58a1       59a1       60a1 
    C1_ s       0.000001   0.000006   0.000041   0.000071   0.000009   0.000003
    C1_ p       0.000019   0.000002   0.000002   0.000004  -0.000007   0.000002
    C1_ d       0.000028   0.000002  -0.000000   0.000149   0.000005   0.000025
    C2_ s      -0.000005   0.000016   0.000012  -0.000002   0.000017   0.000003
    C2_ p       0.000043  -0.000002  -0.000002   0.000047   0.000031   0.000014
    C2_ d       0.000049   0.000042   0.000054   0.000017   0.000026   0.000033
    C3_ s       0.000002   0.000002   0.000014   0.000006   0.000003   0.000001
    C3_ p       0.000032  -0.000014   0.000001   0.000014   0.000107   0.000056
    C3_ d       0.000023   0.000020   0.000080   0.000002   0.000059   0.000011
    C4_ s       0.000000   0.000012   0.000001   0.000020   0.000021   0.000004
    C4_ p       0.000053  -0.000004   0.000011   0.000034   0.000022   0.000014
    C4_ d       0.000030   0.000000   0.000018   0.000074   0.000075   0.000001
    C5_ s       0.000001   0.000000   0.000015   0.000000   0.000001   0.000006
    C5_ p       0.000001   0.000001  -0.000013  -0.000002  -0.000003  -0.000002
    C5_ d       0.000020   0.000010   0.000039   0.000016   0.000006   0.000003
    C6_ s       0.000007   0.000010   0.000003   0.000000  -0.000000   0.000011
    C6_ p       0.000008   0.000025   0.000016   0.000009   0.000028   0.000057
    C6_ d       0.000018   0.000070   0.000015   0.000018   0.000057   0.000121
    N1_ s       0.000001   0.000001   0.000008   0.000001   0.000001   0.000001
    N1_ p       0.000001   0.000001   0.000013   0.000002   0.000002   0.000001
    N1_ d       0.000019   0.000010   0.000058   0.000014   0.000007   0.000003
    N2_ s       0.000002   0.000003   0.000004   0.000000  -0.000001  -0.000001
    N2_ p       0.000210   0.000012   0.000054   0.000000   0.000001   0.000014
    N2_ d       0.000048   0.000005   0.000027   0.000015   0.000007   0.000021
    H1_ s       0.000021   0.000003   0.000009   0.000012  -0.000001   0.000002
    H1_ p       0.000015   0.000001   0.000003   0.000017   0.000005   0.000000
    H2_ s       0.000003  -0.000003   0.000016   0.000009   0.000001   0.000005
    H2_ p       0.000004   0.000002   0.000007   0.000014   0.000001   0.000009
    H3_ s       0.000017   0.000089   0.000002   0.000003   0.000007   0.000014
    H3_ p       0.000022   0.000090   0.000023   0.000000   0.000028   0.000013
    H4_ s       0.000009   0.000087   0.000005   0.000002   0.000003   0.000024
    H4_ p       0.000016   0.000149   0.000049   0.000005   0.000034   0.000030
 
   ao class      61a1       62a1       63a1       64a1       65a1       66a1 
    C1_ s      -0.000003   0.000009   0.000000   0.000000   0.000002   0.000000
    C1_ p       0.000138   0.000135   0.000002   0.000000   0.000004  -0.000002
    C1_ d       0.000006   0.000005  -0.000001   0.000001   0.000032   0.000001
    C2_ s      -0.000006  -0.000005   0.000003   0.000002   0.000028  -0.000001
    C2_ p       0.000006  -0.000001  -0.000002   0.000000   0.000035   0.000007
    C2_ d       0.000021   0.000020   0.000022   0.000002   0.000058   0.000009
    C3_ s      -0.000000  -0.000004   0.000029  -0.000000   0.000000   0.000001
    C3_ p       0.000001   0.000009   0.000027   0.000003   0.000005  -0.000000
    C3_ d       0.000029   0.000012   0.000053   0.000001   0.000030   0.000012
    C4_ s      -0.000001   0.000002   0.000004  -0.000000   0.000000   0.000003
    C4_ p       0.000005   0.000000   0.000030   0.000001  -0.000000   0.000000
    C4_ d       0.000001   0.000001   0.000020   0.000001   0.000001   0.000010
    C5_ s       0.000074   0.000000  -0.000001  -0.000000  -0.000001   0.000000
    C5_ p      -0.000068   0.000001  -0.000000   0.000000   0.000005  -0.000000
    C5_ d       0.000010   0.000172   0.000003   0.000000   0.000017   0.000000
    C6_ s       0.000000  -0.000000   0.000001   0.000024   0.000000   0.000000
    C6_ p       0.000000  -0.000000  -0.000002   0.000017   0.000002   0.000020
    C6_ d       0.000003   0.000000   0.000002   0.000051   0.000003   0.000043
    N1_ s       0.000051   0.000014   0.000000   0.000000   0.000003   0.000000
    N1_ p       0.000089   0.000024   0.000000   0.000000   0.000007  -0.000000
    N1_ d       0.000110   0.000000  -0.000000   0.000000   0.000002   0.000000
    N2_ s       0.000000   0.000000   0.000002   0.000004   0.000000  -0.000000
    N2_ p       0.000004   0.000001   0.000000   0.000002   0.000000  -0.000001
    N2_ d       0.000002   0.000000   0.000000   0.000007   0.000000   0.000017
    H1_ s      -0.000005   0.000002   0.000001   0.000000   0.000014   0.000000
    H1_ p       0.000009   0.000015   0.000059   0.000008   0.000057   0.000010
    H2_ s       0.000000   0.000002   0.000012   0.000000   0.000003   0.000001
    H2_ p       0.000004   0.000005   0.000099   0.000005   0.000031   0.000010
    H3_ s       0.000000   0.000000   0.000000   0.000004   0.000000  -0.000000
    H3_ p       0.000000   0.000000   0.000001   0.000086   0.000006   0.000055
    H4_ s       0.000001   0.000000   0.000000   0.000005   0.000000   0.000006
    H4_ p       0.000000   0.000000   0.000007   0.000132   0.000001   0.000069
 
   ao class      67a1       68a1       69a1       70a1       71a1       72a1 
    C1_ s       0.000001   0.000002   0.000000   0.000001   0.000001   0.000001
    C1_ p      -0.000002   0.000009  -0.000000   0.000000   0.000000   0.000000
    C1_ d       0.000001   0.000008   0.000001   0.000007   0.000003   0.000000
    C2_ s      -0.000001  -0.000003   0.000002   0.000013   0.000003   0.000002
    C2_ p       0.000011   0.000016  -0.000001  -0.000001  -0.000001   0.000001
    C2_ d       0.000023   0.000020   0.000001   0.000013   0.000005   0.000001
    C3_ s       0.000004   0.000003   0.000002   0.000012   0.000002   0.000000
    C3_ p       0.000003   0.000013  -0.000002  -0.000005   0.000001   0.000012
    C3_ d       0.000036   0.000005   0.000001   0.000012   0.000007  -0.000000
    C4_ s      -0.000000   0.000002  -0.000000  -0.000001   0.000006   0.000000
    C4_ p       0.000001  -0.000001   0.000004   0.000010   0.000000   0.000079
    C4_ d       0.000004   0.000007   0.000001   0.000003   0.000006   0.000002
    C5_ s       0.000000  -0.000002   0.000000   0.000002   0.000001  -0.000000
    C5_ p      -0.000000   0.000000   0.000000   0.000000  -0.000000   0.000000
    C5_ d       0.000000   0.000011   0.000001   0.000004   0.000001   0.000000
    C6_ s       0.000001  -0.000000   0.000015  -0.000000  -0.000001  -0.000001
    C6_ p       0.000002  -0.000000   0.000002   0.000005   0.000012  -0.000002
    C6_ d       0.000016   0.000001   0.000021   0.000006   0.000012   0.000001
    N1_ s       0.000000   0.000000   0.000000   0.000000   0.000000  -0.000000
    N1_ p       0.000000  -0.000000  -0.000000  -0.000000  -0.000000   0.000000
    N1_ d       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
    N2_ s       0.000001   0.000001   0.000009  -0.000000  -0.000000   0.000001
    N2_ p       0.000002   0.000002  -0.000000   0.000001   0.000000   0.000004
    N2_ d       0.000001   0.000002   0.000002   0.000001   0.000001   0.000000
    H1_ s      -0.000000   0.000007   0.000000   0.000003   0.000001   0.000002
    H1_ p       0.000028   0.000083   0.000003   0.000039   0.000009   0.000000
    H2_ s       0.000002   0.000004  -0.000000  -0.000001   0.000005  -0.000000
    H2_ p       0.000079   0.000049   0.000001   0.000020   0.000014   0.000005
    H3_ s      -0.000000   0.000000  -0.000000  -0.000001  -0.000003  -0.000000
    H3_ p       0.000011   0.000001   0.000030   0.000009   0.000045   0.000003
    H4_ s       0.000001   0.000000  -0.000001   0.000001   0.000001  -0.000002
    H4_ p       0.000027   0.000002   0.000114   0.000035   0.000044   0.000003
 
   ao class      73a1       74a1       75a1       76a1       77a1       78a1 
    C1_ s       0.000001   0.000000   0.000006   0.000001   0.000001   0.000001
    C1_ p       0.000000  -0.000002   0.000006  -0.000001   0.000004  -0.000012
    C1_ d       0.000000   0.000000   0.000001   0.000000  -0.000000   0.000000
    C2_ s       0.000001   0.000001   0.000002   0.000008   0.000000   0.000001
    C2_ p       0.000001   0.000013   0.000027   0.000003   0.000002   0.000010
    C2_ d       0.000000   0.000000   0.000000   0.000000  -0.000000   0.000000
    C3_ s      -0.000000   0.000004   0.000003   0.000001   0.000000   0.000004
    C3_ p       0.000002   0.000003   0.000002   0.000023  -0.000000  -0.000004
    C3_ d       0.000000   0.000000   0.000001  -0.000000   0.000000   0.000000
    C4_ s       0.000001  -0.000000   0.000008   0.000017   0.000000  -0.000000
    C4_ p      -0.000001  -0.000001  -0.000001   0.000001  -0.000001   0.000002
    C4_ d       0.000001   0.000000   0.000000   0.000001   0.000000   0.000000
    C5_ s      -0.000000   0.000025   0.000008   0.000003   0.000013   0.000001
    C5_ p       0.000000   0.000023   0.000002   0.000004   0.000039   0.000011
    C5_ d       0.000000   0.000001   0.000000  -0.000000   0.000000   0.000000
    C6_ s       0.000012   0.000000   0.000001   0.000001   0.000000   0.000000
    C6_ p       0.000056   0.000000   0.000001   0.000003   0.000000  -0.000000
    C6_ d       0.000002   0.000000  -0.000000   0.000001  -0.000000   0.000000
    N1_ s       0.000000   0.000016   0.000002   0.000001  -0.000000   0.000000
    N1_ p       0.000000   0.000002   0.000000   0.000000   0.000000  -0.000000
    N1_ d      -0.000000  -0.000000  -0.000000  -0.000000   0.000001   0.000000
    N2_ s       0.000006  -0.000000   0.000001  -0.000000   0.000000   0.000000
    N2_ p       0.000006   0.000000  -0.000000  -0.000000   0.000000   0.000000
    N2_ d       0.000000   0.000000   0.000000  -0.000000   0.000000   0.000000
    H1_ s       0.000001   0.000002  -0.000000  -0.000001   0.000002   0.000009
    H1_ p       0.000001   0.000007   0.000005   0.000001   0.000008   0.000001
    H2_ s      -0.000001  -0.000000   0.000002   0.000001   0.000002   0.000029
    H2_ p       0.000000   0.000001   0.000002   0.000005   0.000001   0.000013
    H3_ s       0.000004   0.000000   0.000000   0.000000   0.000000  -0.000000
    H3_ p       0.000003   0.000000   0.000000   0.000001   0.000000   0.000000
    H4_ s       0.000003   0.000000   0.000001   0.000000   0.000000   0.000000
    H4_ p       0.000004   0.000000   0.000000   0.000001   0.000000   0.000001
 
   ao class      79a1       80a1       81a1       82a1 
    C1_ s       0.000000   0.000001   0.000002   0.000002
    C1_ p      -0.000000  -0.000004   0.000001  -0.000000
    C1_ d      -0.000000   0.000000  -0.000000  -0.000000
    C2_ s       0.000000  -0.000000   0.000003   0.000004
    C2_ p       0.000000   0.000001   0.000000   0.000001
    C2_ d      -0.000000   0.000000   0.000000  -0.000000
    C3_ s       0.000000  -0.000000   0.000002   0.000004
    C3_ p      -0.000001   0.000000   0.000001   0.000001
    C3_ d      -0.000000   0.000000   0.000000  -0.000000
    C4_ s      -0.000000  -0.000000   0.000005   0.000003
    C4_ p       0.000000   0.000000   0.000000  -0.000002
    C4_ d      -0.000000   0.000000  -0.000000  -0.000000
    C5_ s       0.000000   0.000006   0.000001  -0.000000
    C5_ p       0.000000   0.000000   0.000000  -0.000000
    C5_ d       0.000000  -0.000000   0.000000  -0.000000
    C6_ s       0.000001  -0.000000  -0.000001  -0.000000
    C6_ p       0.000008   0.000002   0.000003   0.000001
    C6_ d       0.000001   0.000000   0.000000   0.000000
    N1_ s       0.000000   0.000000  -0.000000  -0.000000
    N1_ p       0.000000   0.000001   0.000000  -0.000000
    N1_ d       0.000000   0.000000   0.000000   0.000000
    N2_ s      -0.000000  -0.000000   0.000000  -0.000000
    N2_ p      -0.000000  -0.000000  -0.000001  -0.000000
    N2_ d       0.000000  -0.000000   0.000000   0.000000
    H1_ s       0.000000   0.000025  -0.000001   0.000003
    H1_ p      -0.000000   0.000004   0.000002   0.000000
    H2_ s       0.000000   0.000004   0.000000   0.000005
    H2_ p       0.000000   0.000001   0.000002   0.000000
    H3_ s       0.000033  -0.000000  -0.000001  -0.000000
    H3_ p       0.000012   0.000000   0.000000   0.000000
    H4_ s      -0.000000   0.000006   0.000015   0.000004
    H4_ p       0.000002   0.000002   0.000003   0.000001

                        b1  partial gross atomic populations
   ao class       1b1        2b1        3b1        4b1        5b1        6b1 
    C1_ p       0.001058   0.103622   0.336131   0.225394   0.254088   0.055913
    C1_ d       0.000006   0.000219   0.002763   0.007759   0.000124   0.001523
    C2_ p       0.001244   0.189270   0.129250   0.466876   0.042006   0.071991
    C2_ d       0.000011   0.003331   0.006183   0.004025   0.012278   0.003041
    C3_ p       0.007372   0.419721   0.001251   0.169780   0.306109   0.011873
    C3_ d       0.000178   0.007773   0.003558   0.005945   0.004506   0.005357
    C4_ p       0.006242   0.577699   0.083176   0.002653   0.146656   0.066007
    C4_ d       0.001561   0.000826   0.002412   0.010743   0.011453   0.000043
    C5_ p      -0.000004   0.041029   0.672620   0.191565  -0.001462   0.011948
    C5_ d       0.000011   0.000074   0.001801   0.018841   0.009464   0.003558
    C6_ p       0.944920   0.041591   0.004719   0.002271   0.000934   0.000093
    C6_ d       0.005630   0.014248   0.005507   0.011065   0.011737   0.000838
    N1_ p       0.000018   0.020878   0.507224   0.411980   0.077700   0.028088
    N1_ d       0.000000   0.000488   0.008952   0.003846   0.000211   0.000001
    N2_ p       0.239150   0.456327   0.187022   0.354349   0.290164   0.010779
    N2_ d       0.000677   0.003746   0.000664   0.000134   0.000777   0.000688
    H1_ p       0.000010   0.000865   0.000737   0.002345   0.000322   0.000660
    H2_ p       0.000108   0.001875   0.000011   0.000891   0.002237   0.000092
    H3_ p       0.005591   0.000443   0.000083   0.000079   0.000029  -0.000004
    H4_ s       0.759866   0.096205   0.024181   0.039275   0.050495   0.003499
    H4_ p       0.012108   0.000509   0.000024  -0.000109  -0.000216  -0.000007
 
   ao class       7b1        8b1        9b1       10b1       11b1       12b1 
    C1_ p       0.000045   0.002974   0.000007   0.000017   0.000014   0.000102
    C1_ d       0.000250   0.000024   0.000001   0.000003   0.000114   0.000660
    C2_ p       0.001354   0.003012   0.000017  -0.000000   0.000001   0.000078
    C2_ d       0.000188   0.000166   0.000000   0.000023   0.000194   0.000622
    C3_ p       0.004771   0.001932   0.000062   0.000025   0.000033   0.000000
    C3_ d       0.000185   0.000064   0.000004   0.000266   0.000540   0.000042
    C4_ p       0.003137   0.000837  -0.000006   0.000063   0.000018   0.000034
    C4_ d       0.000018   0.000013   0.000006   0.000338   0.000869   0.000114
    C5_ p       0.004017   0.003162   0.000001   0.000000   0.000027   0.000289
    C5_ d       0.000057   0.000007   0.000000   0.000001   0.000074   0.000655
    C6_ p      -0.000001   0.000002   0.003330   0.000830   0.000452   0.000085
    C6_ d       0.000019   0.000004   0.000353   0.000361   0.000463   0.000132
    N1_ p       0.003937   0.002424   0.000002   0.000003   0.000006   0.000078
    N1_ d       0.000087   0.000127   0.000000   0.000001   0.000049   0.000540
    N2_ p       0.000620   0.000177   0.001760   0.002678  -0.000013   0.000014
    N2_ d       0.000098   0.000035   0.000112   0.000005   0.001061   0.000164
    H1_ p       0.000012   0.000052   0.000000   0.000002   0.000015   0.000087
    H2_ p       0.000055   0.000028   0.000001   0.000030   0.000047   0.000000
    H3_ p       0.000000   0.000000   0.000047   0.000000   0.000004   0.000012
    H4_ s       0.000020   0.000002   0.002919   0.000135   0.000010   0.000011
    H4_ p       0.000001   0.000001   0.000016   0.000083   0.000042   0.000018
 
   ao class      13b1       14b1       15b1       16b1       17b1       18b1 
    C1_ p       0.000043   0.000006   0.000055  -0.000005   0.000993   0.000091
    C1_ d       0.000023   0.000036   0.000000   0.000284   0.000034   0.000057
    C2_ p       0.000010   0.000515   0.000384   0.000142   0.000341   0.000356
    C2_ d       0.000050   0.000303   0.000679   0.000400   0.000178   0.000185
    C3_ p       0.000000   0.001122   0.000604  -0.000000   0.000240   0.000008
    C3_ d       0.000208   0.000541   0.000602   0.000168   0.000214   0.000128
    C4_ p       0.000040   0.000176   0.000106   0.000028   0.000010   0.000633
    C4_ d       0.000075   0.000015   0.000045   0.000001   0.000017   0.000000
    C5_ p       0.000025   0.000001   0.000021   0.000398   0.000044   0.000117
    C5_ d       0.000069   0.000079   0.000053   0.000007   0.000033   0.000242
    C6_ p       0.000371   0.000060   0.000025   0.000002   0.000007   0.000005
    C6_ d       0.001544   0.000047   0.000126   0.000002   0.000033   0.000007
    N1_ p       0.000019   0.000005   0.000079   0.001049   0.000028   0.000358
    N1_ d       0.000077   0.000062   0.000100   0.000235   0.000031   0.000064
    N2_ p       0.000037   0.000039  -0.000006   0.000003   0.000050  -0.000017
    N2_ d       0.000040   0.000034   0.000119   0.000001   0.000149   0.000020
    H1_ p       0.000002   0.000048   0.000039   0.000034   0.000013   0.000053
    H2_ p       0.000032   0.000132   0.000010   0.000013   0.000052   0.000003
    H3_ p       0.000272   0.000008   0.000028   0.000000   0.000003   0.000002
    H4_ s       0.000175   0.000027   0.000024   0.000005   0.000017   0.000059
    H4_ p       0.000282   0.000016   0.000023  -0.000000   0.000009  -0.000001
 
   ao class      19b1       20b1       21b1       22b1       23b1       24b1 
    C1_ p       0.000357   0.000117   0.000005   0.000025   0.000022   0.000006
    C1_ d       0.000023   0.000150   0.000008   0.000167   0.000048   0.000013
    C2_ p      -0.000007   0.000113   0.000003   0.000029   0.000001   0.000015
    C2_ d       0.000240   0.000047   0.000013   0.000115   0.000496   0.000010
    C3_ p       0.000001  -0.000001   0.000002   0.000022   0.000010   0.000004
    C3_ d       0.000436   0.000046   0.000022   0.000143   0.000273   0.000111
    C4_ p       0.000418   0.000049   0.000022   0.000112   0.000002   0.000003
    C4_ d       0.000018   0.000001   0.000001   0.000039   0.000003   0.000391
    C5_ p       0.000002   0.000464   0.000014   0.000006   0.000001   0.000002
    C5_ d       0.000028   0.000037   0.000000   0.000026   0.000003   0.000004
    C6_ p      -0.000002   0.000005   0.000051   0.000007   0.000001   0.000006
    C6_ d       0.000051   0.000031   0.000424  -0.000000   0.000001   0.000023
    N1_ p       0.000071  -0.000006  -0.000000   0.000023   0.000004   0.000005
    N1_ d       0.000023   0.000449   0.000018   0.000084   0.000007   0.000007
    N2_ p       0.000017   0.000025   0.000150   0.000009   0.000000   0.000029
    N2_ d       0.000122   0.000001   0.000278   0.000076   0.000004   0.000098
    H1_ p       0.000075   0.000020   0.000006   0.000057   0.000002   0.000000
    H2_ p       0.000103   0.000004   0.000003   0.000021   0.000003   0.000080
    H3_ p       0.000004   0.000000   0.000000   0.000003   0.000000   0.000001
    H4_ s      -0.000000   0.000056   0.000149   0.000034   0.000000   0.000001
    H4_ p       0.000004   0.000025   0.000333   0.000015   0.000003   0.000037
 
   ao class      25b1       26b1       27b1       28b1       29b1       30b1 
    C1_ p       0.000000   0.000000   0.000016   0.000000   0.000000   0.000000
    C1_ d       0.000000   0.000000   0.000096   0.000000   0.000015   0.000014
    C2_ p       0.000001   0.000001  -0.000000   0.000003   0.000004   0.000014
    C2_ d       0.000000   0.000001   0.000004   0.000001   0.000016   0.000032
    C3_ p      -0.000000  -0.000001  -0.000000   0.000001   0.000000   0.000049
    C3_ d       0.000001   0.000006   0.000001   0.000001   0.000051   0.000039
    C4_ p       0.000000   0.000020  -0.000000   0.000001   0.000000   0.000038
    C4_ d       0.000003   0.000008   0.000000   0.000004   0.000018   0.000008
    C5_ p       0.000000   0.000000   0.000020   0.000001   0.000001   0.000040
    C5_ d       0.000000   0.000000   0.000251   0.000000   0.000017   0.000000
    C6_ p       0.000001   0.000008   0.000000   0.000101   0.000000   0.000003
    C6_ d       0.000001   0.000056   0.000000   0.000205   0.000001   0.000001
    N1_ p       0.000000   0.000000   0.000000   0.000001  -0.000000   0.000032
    N1_ d       0.000000   0.000000   0.000094   0.000000   0.000005   0.000002
    N2_ p       0.000000   0.000015  -0.000000  -0.000005   0.000000   0.000000
    N2_ d       0.000001   0.000004   0.000000   0.000026   0.000006  -0.000000
    H1_ p       0.000000   0.000000   0.000091   0.000000   0.000101   0.000041
    H2_ p       0.000001   0.000005   0.000002   0.000018   0.000182   0.000008
    H3_ p       0.000263   0.000067   0.000000   0.000015   0.000000   0.000000
    H4_ s       0.000001   0.000211   0.000000   0.000074   0.000000  -0.000000
    H4_ p       0.000507   0.000276   0.000000   0.000086   0.000008   0.000017
 
   ao class      31b1       32b1       33b1       34b1       35b1 
    C1_ p       0.000000   0.000015   0.000032   0.000002   0.000000
    C1_ d       0.000002   0.000004   0.000000   0.000000  -0.000000
    C2_ p       0.000000   0.000000   0.000042   0.000003   0.000001
    C2_ d       0.000006   0.000028   0.000000   0.000000   0.000000
    C3_ p      -0.000000   0.000005   0.000032   0.000003   0.000001
    C3_ d       0.000005   0.000006   0.000000   0.000000  -0.000000
    C4_ p       0.000002   0.000005   0.000019   0.000009   0.000000
    C4_ d       0.000000   0.000002   0.000000   0.000000   0.000000
    C5_ p       0.000001   0.000059   0.000018   0.000001  -0.000000
    C5_ d       0.000002   0.000010   0.000000   0.000000  -0.000000
    C6_ p       0.000017   0.000001   0.000001   0.000036   0.000011
    C6_ d       0.000059   0.000007   0.000004   0.000011   0.000004
    N1_ p       0.000001   0.000047   0.000011   0.000000   0.000000
    N1_ d       0.000001   0.000011   0.000001   0.000000   0.000000
    N2_ p       0.000007   0.000002   0.000001   0.000003   0.000002
    N2_ d       0.000003   0.000000   0.000002   0.000001  -0.000000
    H1_ p       0.000016   0.000028   0.000022   0.000002   0.000000
    H2_ p       0.000006   0.000026   0.000022   0.000002   0.000000
    H3_ p       0.000024   0.000005   0.000009   0.000054   0.000000
    H4_ s       0.000003  -0.000000  -0.000000  -0.000001   0.000029
    H4_ p       0.000135   0.000012   0.000011   0.000070   0.000027

                        b2  partial gross atomic populations
   ao class       1b2        2b2        3b2        4b2        5b2        6b2 
    C1_ p       0.000000   0.000328   0.000554   0.048062   0.060837   0.113877
    C1_ d      -0.000000  -0.000001  -0.000022   0.001030   0.002007   0.000482
    C2_ s       0.000000   0.726331   1.272293   0.208419   0.402425   0.298165
    C2_ p      -0.000000   0.000158   0.000226   0.045907   0.023607   0.186620
    C2_ d      -0.000000  -0.000006  -0.000032   0.002638   0.003245   0.003384
    C3_ s      -0.000033   1.271964   0.726638   0.354208   0.244508   0.313204
    C3_ p      -0.000049   0.000318   0.000236   0.003709   0.091671   0.140378
    C3_ d      -0.000000  -0.000003  -0.000041   0.002392   0.003464   0.002914
    C4_ p       0.000302   0.000807   0.000327  -0.000141   0.060970   0.092828
    C4_ d       0.000000  -0.000024  -0.000022   0.000569   0.002561   0.000778
    C5_ p       0.000000  -0.000003  -0.000012   0.000191   0.001100   0.005614
    C5_ d      -0.000000   0.000000  -0.000000   0.000096   0.000213   0.000205
    C6_ s       2.000167   0.000002   0.000000   0.513779   0.420382   0.026057
    C6_ p       0.000010   0.000005  -0.000001   0.043612   0.038875   0.047829
    C6_ d       0.000000   0.000000   0.000000   0.000161   0.000190   0.000081
    N1_ p      -0.000000   0.000000   0.000000   0.000062   0.000302   0.002259
    N1_ d      -0.000000  -0.000000  -0.000000   0.000005   0.000015   0.000063
    N2_ p      -0.000874  -0.000012  -0.000006   0.099196   0.052968   0.000138
    N2_ d      -0.000020   0.000000  -0.000000   0.000602   0.001791   0.000772
    H1_ s       0.000000   0.000100  -0.000001   0.062894   0.155254   0.358006
    H1_ p      -0.000000  -0.000025  -0.000073   0.002993   0.006727   0.009678
    H2_ s      -0.000021   0.000117  -0.000015   0.158866   0.065640   0.326744
    H2_ p       0.000000  -0.000054  -0.000048   0.005112   0.004185   0.008773
    H3_ s       0.000277   0.000000  -0.000000   0.088454   0.100083   0.044004
    H3_ p      -0.000065  -0.000000  -0.000000   0.006520   0.006483   0.001435
    H4_ s       0.000433   0.000000   0.000001   0.324440   0.227069   0.001856
    H4_ p      -0.000127  -0.000000   0.000000   0.016124   0.012850   0.001046
 
   ao class       7b2        8b2        9b2       10b2       11b2       12b2 
    C1_ p       0.003858   0.085570   0.111354   0.584273   0.005230  -0.001361
    C1_ d       0.000636   0.010155   0.000193   0.002783   0.000661   0.013164
    C2_ s       0.000412   0.095054   0.006634   0.012351   0.008718   0.009193
    C2_ p       0.099188   0.533066   0.305938   0.566513   0.027728   0.037620
    C2_ d       0.001592   0.006248   0.001412   0.021801   0.004999   0.000180
    C3_ s       0.066983   0.036181   0.000473   0.017788   0.021710   0.002125
    C3_ p       0.041649   0.551859   0.564327   0.167045   0.302826   0.009564
    C3_ d       0.000299   0.007221   0.004295   0.018768   0.006244   0.000265
    C4_ p       0.004541   0.028860   0.408850   0.100005   0.245346   0.000063
    C4_ d       0.002876   0.005359   0.000214   0.000540   0.021193   0.000057
    C5_ p       0.001270   0.000338   0.016031   0.041428   0.000415   0.841007
    C5_ d      -0.000022   0.000513   0.000274   0.003001   0.000080   0.027881
    C6_ s       0.004488   0.000350  -0.000161   0.002945   0.038946   0.000067
    C6_ p       0.898349   0.078285   0.180475   0.055788   0.550516   0.000128
    C6_ d       0.010314   0.001837   0.008430   0.002732   0.016256   0.000016
    N1_ p       0.000395  -0.000001   0.005639   0.009341   0.000411   1.017949
    N1_ d       0.000008   0.000004   0.000180   0.000497   0.000001   0.013703
    N2_ p       0.006504   0.014953   0.288721   0.113093   0.613029   0.000101
    N2_ d       0.001585   0.000304   0.000035   0.000003   0.009707   0.000007
    H1_ s       0.045857   0.203128   0.008859   0.107719   0.016397   0.003721
    H1_ p       0.000707   0.001103   0.001230   0.002549   0.000002   0.000314
    H2_ s       0.003321   0.244088   0.006063   0.141362   0.024992   0.000906
    H2_ p       0.000540   0.002397   0.002115   0.000297   0.000971   0.000045
    H3_ s       0.562068   0.051637   0.000750   0.007119   0.005506   0.000053
    H3_ p       0.008463   0.000481   0.000680   0.000186   0.002102   0.000000
    H4_ s       0.209473   0.023446   0.057691   0.001458   0.052766  -0.000013
    H4_ p       0.009238   0.000982   0.001483   0.000312   0.003279   0.000002
 
   ao class      13b2       14b2       15b2       16b2       17b2       18b2 
    C1_ p       0.000043   0.000032   0.000262   0.002331   0.000114   0.000015
    C1_ d       0.000541   0.000002   0.000072   0.000145  -0.000003   0.000007
    C2_ s       0.000242   0.000094   0.001437   0.000750   0.000441   0.000065
    C2_ p       0.000295   0.000273   0.000866   0.001293   0.000507   0.000235
    C2_ d       0.000042   0.000080   0.000192   0.000758   0.000158   0.000017
    C3_ s       0.000029   0.000697   0.001925  -0.000009   0.000660   0.000022
    C3_ p       0.000088   0.000769   0.000773   0.001167   0.002361   0.000074
    C3_ d       0.000010   0.000176   0.000424   0.000524   0.000182   0.000018
    C4_ p       0.000001   0.000959   0.001120   0.000888   0.000269   0.000016
    C4_ d       0.000001   0.000539   0.000076  -0.000003   0.000003   0.000027
    C5_ p       0.008216  -0.000000   0.000084   0.000204   0.000015  -0.000000
    C5_ d       0.000142   0.000000   0.000026   0.000122   0.000013   0.000001
    C6_ s       0.000000   0.000929   0.000158   0.000117   0.000106   0.000113
    C6_ p       0.000001   0.002719   0.000560   0.000173   0.000144   0.003350
    C6_ d       0.000000   0.000164   0.000110   0.000033   0.000020   0.000249
    N1_ p       0.008928   0.000000   0.000023   0.000091   0.000004   0.000001
    N1_ d       0.000161  -0.000000   0.000013   0.000036   0.000004   0.000000
    N2_ p      -0.000000   0.003069   0.001622   0.000592   0.000006   0.000139
    N2_ d       0.000000   0.000487   0.000007   0.000003   0.000008   0.000115
    H1_ s       0.000003   0.000056   0.000131   0.000126   0.000695   0.000162
    H1_ p       0.000003   0.000005   0.000000   0.000015  -0.000020  -0.000001
    H2_ s       0.000010   0.000006   0.000047   0.000288   0.001819   0.000012
    H2_ p       0.000001   0.000004  -0.000009   0.000039   0.000008  -0.000000
    H3_ s       0.000000   0.000098   0.000001   0.000004   0.000032   0.001395
    H3_ p       0.000000   0.000045   0.000017   0.000005   0.000000  -0.000037
    H4_ s       0.000000   0.000001   0.000003   0.000007   0.000060   0.001497
    H4_ p       0.000000   0.000094   0.000041   0.000017   0.000012   0.000034
 
   ao class      19b2       20b2       21b2       22b2       23b2       24b2 
    C1_ p       0.000255   0.000908   0.000016   0.000133   0.000226   0.000011
    C1_ d       0.000067   0.000059   0.000000   0.000217   0.000361   0.000026
    C2_ s       0.000499   0.000024   0.000001   0.000004   0.000051   0.000461
    C2_ p       0.002443   0.002119   0.000024   0.000176   0.000240   0.000122
    C2_ d       0.000133   0.000053   0.000007   0.000506   0.000483   0.000564
    C3_ s       0.000077   0.000047   0.000085   0.000037   0.000330   0.000531
    C3_ p       0.000922   0.001925   0.000209   0.000392   0.000049   0.000037
    C3_ d       0.000077   0.000071   0.000058   0.000803   0.000155   0.000632
    C4_ p       0.000046   0.000771  -0.000069   0.000305   0.000007  -0.000076
    C4_ d       0.000043   0.000044   0.000049   0.000535   0.000092  -0.000001
    C5_ p       0.000003   0.000018  -0.000000   0.000031   0.000233   0.000113
    C5_ d       0.000001   0.000061   0.000000   0.000101   0.000392   0.000087
    C6_ s       0.000023   0.000058   0.002056   0.000011  -0.000008   0.000002
    C6_ p       0.000134   0.000168   0.001065   0.000481   0.000242   0.000112
    C6_ d       0.000027   0.000030   0.000054   0.000211   0.000187   0.000225
    N1_ p       0.000002   0.000018   0.000000   0.000009   0.000089   0.000097
    N1_ d       0.000000   0.000012   0.000000   0.000054   0.000326   0.000143
    N2_ p       0.000002   0.000069   0.000035   0.000287   0.000131   0.000000
    N2_ d       0.000002   0.000027   0.000019   0.000360   0.000221   0.000004
    H1_ s       0.001734   0.000050   0.000001   0.000003   0.000012   0.000087
    H1_ p      -0.000020   0.000064   0.000003   0.000050   0.000073   0.000068
    H2_ s       0.000476   0.000046   0.000198   0.000005   0.000050   0.000117
    H2_ p       0.000011   0.000070  -0.000009   0.000107   0.000033   0.000067
    H3_ s       0.000167   0.000207   0.001250   0.000085   0.000007   0.000005
    H3_ p      -0.000007  -0.000006  -0.000062   0.000019   0.000019   0.000013
    H4_ s       0.000014   0.000012   0.001405   0.000020   0.000005   0.000016
    H4_ p       0.000010   0.000011  -0.000027   0.000018   0.000024   0.000069
 
   ao class      25b2       26b2       27b2       28b2       29b2       30b2 
    C1_ p      -0.000004   0.000046   0.000044   0.000015   0.000040   0.000050
    C1_ d       0.000007   0.000139   0.000097  -0.000000   0.000048   0.000009
    C2_ s       0.000109   0.000175   0.000046   0.000018   0.000032   0.000042
    C2_ p       0.000010   0.000022  -0.000001   0.000299   0.000002   0.000033
    C2_ d       0.000091   0.000325   0.000068   0.000744   0.000100   0.000157
    C3_ s      -0.000007   0.000043   0.000070   0.000062   0.000010   0.000026
    C3_ p       0.000077   0.000003   0.000082   0.000169   0.000018   0.000102
    C3_ d       0.000130   0.000167   0.000366   0.000647   0.000087   0.000236
    C4_ p       0.000028   0.000042   0.000106   0.000116   0.000008  -0.000005
    C4_ d       0.000006   0.000005   0.000055   0.000018   0.000009   0.000013
    C5_ p       0.000027   0.000328   0.000094   0.000009   0.000229   0.000115
    C5_ d       0.000030   0.000019   0.000026   0.000072   0.000287   0.000154
    C6_ s       0.000002   0.000013   0.000138  -0.000000   0.000063   0.000033
    C6_ p       0.000435   0.000077   0.000126   0.000011   0.000088   0.000108
    C6_ d       0.001503   0.000156   0.000601   0.000081   0.000547   0.000516
    N1_ p       0.000026   0.000758   0.000200   0.000008   0.000175   0.000072
    N1_ d       0.000045   0.000360   0.000009   0.000102   0.000171   0.000139
    N2_ p       0.000076   0.000016  -0.000013   0.000010   0.000004   0.000020
    N2_ d       0.000204   0.000056   0.000110   0.000000   0.000005   0.000031
    H1_ s       0.000021   0.000006  -0.000000   0.000018   0.000008   0.000018
    H1_ p       0.000008   0.000022   0.000002   0.000056   0.000018   0.000008
    H2_ s       0.000030   0.000007   0.000009   0.000002   0.000014   0.000018
    H2_ p       0.000019   0.000005   0.000046   0.000010   0.000006   0.000025
    H3_ s       0.000016   0.000007   0.000033   0.000000   0.000006   0.000002
    H3_ p       0.000039   0.000026   0.000125   0.000003   0.000107   0.000086
    H4_ s       0.000042   0.000003   0.000040   0.000000   0.000041   0.000025
    H4_ p       0.000433   0.000024   0.000092   0.000017   0.000037   0.000042
 
   ao class      31b2       32b2       33b2       34b2       35b2       36b2 
    C1_ p       0.000004  -0.000001   0.000012   0.000068   0.000001  -0.000008
    C1_ d       0.000000   0.000031   0.000250   0.000007   0.000004   0.000012
    C2_ s       0.000022   0.000041   0.000015   0.000039   0.000005   0.000033
    C2_ p       0.000117   0.000037   0.000038   0.000025   0.000024   0.000148
    C2_ d       0.000160   0.000254   0.000181   0.000312   0.000034   0.000136
    C3_ s       0.000050   0.000009  -0.000011   0.000031   0.000013   0.000099
    C3_ p       0.000052   0.000096   0.000010   0.000088   0.000026  -0.000018
    C3_ d       0.000164   0.000283   0.000059   0.000228   0.000110   0.000103
    C4_ p       0.000022   0.000012   0.000006  -0.000000  -0.000003  -0.000028
    C4_ d       0.000241   0.000054   0.000001   0.000008   0.000101   0.000022
    C5_ p       0.000020   0.000039   0.000136  -0.000002   0.000003   0.000002
    C5_ d       0.000005   0.000000   0.000047   0.000000   0.000003   0.000001
    C6_ s       0.000016   0.000004   0.000003   0.000095   0.000360   0.000013
    C6_ p       0.000065   0.000045   0.000001   0.000006   0.000003   0.000022
    C6_ d       0.000295   0.000083   0.000003   0.000036   0.000096   0.000010
    N1_ p       0.000004   0.000000  -0.000001   0.000002   0.000001   0.000006
    N1_ d       0.000017   0.000064   0.000289   0.000007   0.000010   0.000007
    N2_ p       0.000040   0.000013  -0.000001   0.000006   0.000009   0.000041
    N2_ d       0.000143   0.000163   0.000011   0.000004   0.000009   0.000103
    H1_ s       0.000028   0.000011   0.000006  -0.000001   0.000002   0.000125
    H1_ p       0.000061   0.000051   0.000129   0.000020   0.000008   0.000003
    H2_ s       0.000028   0.000014   0.000011   0.000036   0.000024   0.000044
    H2_ p       0.000016   0.000171   0.000004   0.000034   0.000012   0.000028
    H3_ s       0.000008  -0.000001  -0.000000   0.000010   0.000019   0.000006
    H3_ p       0.000042   0.000012   0.000000   0.000010   0.000046   0.000003
    H4_ s       0.000000  -0.000001   0.000001   0.000031   0.000111   0.000007
    H4_ p       0.000106   0.000045   0.000001  -0.000000   0.000002   0.000010
 
   ao class      37b2       38b2       39b2       40b2       41b2       42b2 
    C1_ p       0.000035   0.000127   0.000005   0.000097   0.000063   0.000096
    C1_ d       0.000001   0.000116   0.000000  -0.000001  -0.000000   0.000004
    C2_ s       0.000021   0.000007   0.000005   0.000009   0.000024  -0.000002
    C2_ p      -0.000020   0.000057   0.000013   0.000019   0.000017   0.000007
    C2_ d       0.000034   0.000096   0.000003   0.000036   0.000062   0.000036
    C3_ s       0.000004   0.000011   0.000015  -0.000016   0.000024   0.000037
    C3_ p       0.000111   0.000062   0.000073   0.000016   0.000060   0.000009
    C3_ d       0.000099   0.000020   0.000024   0.000039   0.000048   0.000081
    C4_ p       0.000098   0.000005  -0.000031   0.000020   0.000066   0.000081
    C4_ d       0.000000   0.000012   0.000094   0.000002   0.000001   0.000065
    C5_ p      -0.000002   0.000009   0.000002  -0.000001  -0.000002  -0.000011
    C5_ d       0.000003   0.000048   0.000012   0.000042   0.000013   0.000012
    C6_ s       0.000008   0.000021   0.000067   0.000001   0.000002   0.000000
    C6_ p       0.000036   0.000018  -0.000007   0.000010   0.000007   0.000021
    C6_ d       0.000029   0.000001   0.000098   0.000003   0.000016   0.000027
    N1_ p      -0.000000   0.000036   0.000001   0.000000   0.000000   0.000001
    N1_ d       0.000004   0.000013   0.000005   0.000016   0.000005   0.000005
    N2_ p       0.000218   0.000023   0.000059   0.000010   0.000025   0.000000
    N2_ d       0.000108   0.000020   0.000024   0.000001   0.000002   0.000013
    H1_ s       0.000021   0.000034   0.000006   0.000009  -0.000000   0.000002
    H1_ p       0.000004   0.000022   0.000005   0.000018   0.000009   0.000014
    H2_ s       0.000037  -0.000001   0.000039   0.000015   0.000002  -0.000001
    H2_ p       0.000023   0.000010   0.000052  -0.000004   0.000010   0.000010
    H3_ s       0.000008   0.000001   0.000037   0.000033   0.000006   0.000030
    H3_ p      -0.000001   0.000000   0.000028   0.000130   0.000060   0.000010
    H4_ s       0.000003   0.000004   0.000010   0.000028   0.000018   0.000024
    H4_ p       0.000006   0.000008   0.000078   0.000109   0.000081   0.000021
 
   ao class      43b2       44b2       45b2       46b2       47b2       48b2 
    C1_ p      -0.000008   0.000036   0.000006   0.000004  -0.000000   0.000000
    C1_ d       0.000020   0.000003   0.000024   0.000028   0.000000   0.000001
    C2_ s       0.000062   0.000001  -0.000011   0.000010   0.000000   0.000000
    C2_ p       0.000016  -0.000011   0.000033   0.000014   0.000000   0.000000
    C2_ d       0.000051   0.000018   0.000055   0.000022   0.000000   0.000003
    C3_ s       0.000010   0.000014   0.000005   0.000001   0.000000   0.000000
    C3_ p       0.000030  -0.000013   0.000049   0.000002   0.000003   0.000003
    C3_ d       0.000037   0.000041   0.000051   0.000026   0.000003   0.000001
    C4_ p       0.000125   0.000018  -0.000001   0.000012   0.000000   0.000000
    C4_ d       0.000001   0.000009   0.000015   0.000016   0.000001   0.000004
    C5_ p       0.000014  -0.000002   0.000005   0.000008   0.000000   0.000001
    C5_ d       0.000022   0.000028   0.000040   0.000011   0.000000   0.000000
    C6_ s       0.000010   0.000008   0.000000  -0.000000   0.000031   0.000002
    C6_ p       0.000010   0.000082   0.000036   0.000005   0.000011   0.000026
    C6_ d       0.000007   0.000106   0.000049   0.000006   0.000036   0.000038
    N1_ p       0.000007   0.000000   0.000001   0.000006   0.000000   0.000001
    N1_ d       0.000004   0.000008   0.000008   0.000001   0.000000  -0.000000
    N2_ p       0.000041   0.000027   0.000000  -0.000001   0.000003   0.000002
    N2_ d       0.000003   0.000002   0.000014   0.000002   0.000009   0.000005
    H1_ s      -0.000001   0.000006   0.000012   0.000004   0.000000   0.000000
    H1_ p       0.000027   0.000028   0.000008   0.000067   0.000001   0.000005
    H2_ s       0.000006   0.000001   0.000013   0.000005   0.000001   0.000001
    H2_ p       0.000026   0.000011  -0.000004   0.000093   0.000000   0.000013
    H3_ s       0.000006   0.000037   0.000013   0.000000   0.000000   0.000003
    H3_ p       0.000010   0.000018   0.000005   0.000003   0.000042   0.000159
    H4_ s       0.000004   0.000018   0.000005   0.000000   0.000009   0.000000
    H4_ p       0.000025   0.000002   0.000017   0.000013   0.000179   0.000024
 
   ao class      49b2       50b2       51b2       52b2       53b2       54b2 
    C1_ p      -0.000003  -0.000000   0.000002   0.000000   0.000000  -0.000000
    C1_ d       0.000000   0.000002   0.000011   0.000001   0.000002   0.000001
    C2_ s       0.000000   0.000000   0.000017  -0.000001  -0.000002   0.000000
    C2_ p       0.000024  -0.000002   0.000016   0.000005   0.000011   0.000001
    C2_ d       0.000009   0.000001   0.000019   0.000005   0.000014   0.000005
    C3_ s       0.000002  -0.000000   0.000009  -0.000001  -0.000001   0.000003
    C3_ p       0.000017   0.000031   0.000004   0.000005   0.000009   0.000001
    C3_ d       0.000000   0.000014   0.000008   0.000004   0.000012   0.000007
    C4_ p      -0.000001   0.000002   0.000003  -0.000000   0.000001   0.000003
    C4_ d       0.000001   0.000008   0.000002   0.000000   0.000001   0.000002
    C5_ p       0.000059   0.000057   0.000002   0.000000   0.000001   0.000000
    C5_ d       0.000002   0.000008   0.000013   0.000000   0.000000   0.000000
    C6_ s      -0.000000  -0.000000  -0.000000   0.000007   0.000003  -0.000000
    C6_ p      -0.000000   0.000000   0.000000   0.000006   0.000005   0.000018
    C6_ d       0.000001   0.000003   0.000001   0.000015   0.000007   0.000013
    N1_ p       0.000044   0.000046   0.000006   0.000000   0.000000   0.000000
    N1_ d       0.000006   0.000010   0.000005   0.000000   0.000000   0.000000
    N2_ p       0.000000   0.000000  -0.000000   0.000003   0.000001  -0.000002
    N2_ d       0.000001   0.000004   0.000002   0.000001   0.000000   0.000000
    H1_ s       0.000001  -0.000000   0.000005   0.000002   0.000004  -0.000000
    H1_ p       0.000055   0.000006   0.000079   0.000013   0.000033   0.000010
    H2_ s       0.000002   0.000002   0.000002   0.000004   0.000006   0.000000
    H2_ p       0.000053   0.000062   0.000019   0.000010   0.000023   0.000012
    H3_ s       0.000001   0.000002  -0.000000  -0.000000  -0.000001  -0.000001
    H3_ p       0.000001   0.000005   0.000003   0.000012   0.000028   0.000038
    H4_ s       0.000000   0.000001  -0.000000  -0.000001  -0.000001   0.000000
    H4_ p       0.000001   0.000003   0.000003   0.000105   0.000034   0.000065
 
   ao class      55b2       56b2       57b2       58b2       59b2       60b2 
    C1_ p       0.000000   0.000003   0.000002   0.000003   0.000004  -0.000002
    C1_ d       0.000000  -0.000000   0.000000   0.000000  -0.000000  -0.000000
    C2_ s       0.000000   0.000008   0.000002   0.000001   0.000016   0.000001
    C2_ p       0.000000   0.000017   0.000000   0.000005   0.000007   0.000031
    C2_ d       0.000000   0.000000   0.000000   0.000000   0.000001   0.000000
    C3_ s       0.000000   0.000009   0.000007  -0.000000   0.000012   0.000001
    C3_ p      -0.000003   0.000024   0.000007   0.000006   0.000005   0.000012
    C3_ d       0.000000   0.000000   0.000001  -0.000000   0.000001  -0.000000
    C4_ p       0.000003   0.000005   0.000009   0.000001   0.000000   0.000003
    C4_ d      -0.000001  -0.000000  -0.000000   0.000000  -0.000000  -0.000000
    C5_ p       0.000000   0.000000   0.000000  -0.000000  -0.000001   0.000001
    C5_ d      -0.000000  -0.000000  -0.000000  -0.000000  -0.000000  -0.000000
    C6_ s       0.000016   0.000000  -0.000000   0.000000   0.000000   0.000000
    C6_ p       0.000062   0.000000   0.000002   0.000000  -0.000001   0.000001
    C6_ d       0.000005   0.000000   0.000000   0.000000  -0.000000   0.000000
    N1_ p       0.000000  -0.000000   0.000001   0.000000  -0.000001   0.000000
    N1_ d       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
    N2_ p       0.000021  -0.000002  -0.000001  -0.000000   0.000000  -0.000002
    N2_ d       0.000001   0.000000   0.000000   0.000000  -0.000000   0.000000
    H1_ s       0.000000   0.000006   0.000010   0.000036   0.000004  -0.000000
    H1_ p       0.000000   0.000004   0.000002   0.000014   0.000006   0.000003
    H2_ s       0.000000   0.000015   0.000031   0.000006   0.000005  -0.000000
    H2_ p       0.000000   0.000010   0.000010   0.000005   0.000005   0.000003
    H3_ s      -0.000002  -0.000000  -0.000000   0.000000  -0.000001  -0.000000
    H3_ p       0.000000   0.000000   0.000001   0.000000   0.000000   0.000000
    H4_ s       0.000015  -0.000000  -0.000000  -0.000000   0.000001   0.000001
    H4_ p       0.000003   0.000000   0.000003   0.000000   0.000000   0.000000
 
   ao class      61b2       62b2 
    C1_ p      -0.000000   0.000000
    C1_ d      -0.000000   0.000000
    C2_ s       0.000000   0.000000
    C2_ p       0.000000   0.000000
    C2_ d       0.000000  -0.000000
    C3_ s       0.000000  -0.000000
    C3_ p       0.000001  -0.000001
    C3_ d       0.000000  -0.000000
    C4_ p      -0.000001   0.000001
    C4_ d      -0.000000   0.000000
    C5_ p       0.000000  -0.000000
    C5_ d      -0.000000  -0.000000
    C6_ s      -0.000000   0.000000
    C6_ p       0.000007   0.000003
    C6_ d       0.000001   0.000000
    N1_ p      -0.000000   0.000000
    N1_ d       0.000000   0.000000
    N2_ p       0.000000  -0.000000
    N2_ d       0.000000  -0.000000
    H1_ s       0.000000   0.000000
    H1_ p       0.000000   0.000000
    H2_ s       0.000000   0.000000
    H2_ p       0.000000   0.000000
    H3_ s       0.000000   0.000025
    H3_ p       0.000001   0.000004
    H4_ s       0.000020   0.000001
    H4_ p       0.000007   0.000000

                        a2  partial gross atomic populations
   ao class       1a2        2a2        3a2        4a2        5a2        6a2 
    C1_ d       0.000000   0.009070   0.011322   0.000000   0.000183   0.000463
    C2_ p      -0.000038   0.657769   0.461592   0.000001  -0.000013  -0.000015
    C2_ d       0.000001   0.009358   0.005627   0.000001   0.000539   0.000735
    C3_ p       0.001586   1.035999   0.286099   0.000034   0.000572   0.000730
    C3_ d       0.000043   0.005069   0.007002   0.000003   0.000868   0.000866
    C4_ d       0.000160   0.015463   0.009001   0.000003   0.000585   0.000267
    C5_ d      -0.000000   0.000030   0.000186   0.000000   0.000005   0.000026
    C6_ p       1.039505   0.000420   0.000029   0.003850   0.000270   0.000063
    C6_ d       0.012918   0.000064   0.000080   0.000332   0.000596   0.000374
    N1_ d       0.000000   0.000000   0.000000   0.000000   0.000000   0.000004
    N2_ d       0.005680   0.000012   0.000034   0.000231   0.000650   0.000262
    H1_ p       0.000000   0.004165   0.004090   0.000000   0.000005   0.000008
    H2_ p       0.000080   0.007103   0.003092   0.000002   0.000020   0.000054
    H3_ p       0.005324   0.000010   0.000011   0.000049   0.000005   0.000006
    H4_ s       0.906846   0.001984   0.002043   0.003288   0.000069   0.000026
    H4_ p       0.013068   0.000083   0.000112  -0.000036   0.000089   0.000055
 
   ao class       7a2        8a2        9a2       10a2       11a2       12a2 
    C1_ d       0.000018   0.000008   0.000252   0.000407   0.000006   0.000117
    C2_ p       0.000513   0.000099   0.000570   0.000334   0.000009   0.000894
    C2_ d       0.000634   0.000052   0.000904   0.000171   0.000198   0.000183
    C3_ p       0.000313   0.000014   0.000301   0.000367   0.000419   0.000255
    C3_ d       0.000821   0.000004   0.000295   0.000304   0.000179   0.000351
    C4_ d       0.000040   0.000002   0.000131   0.000412   0.000080   0.000002
    C5_ d       0.000023   0.000014   0.000013   0.000291   0.000650   0.000129
    C6_ p       0.000076   0.000631   0.000072   0.000114   0.000008   0.000000
    C6_ d       0.000438   0.001608   0.000048   0.000200   0.000029   0.000002
    N1_ d       0.000006   0.000006   0.000011   0.000177   0.000925   0.000353
    N2_ d       0.000389   0.000005   0.000065   0.000121   0.000018   0.000001
    H1_ p       0.000092   0.000011   0.000237   0.000006   0.000018   0.000008
    H2_ p       0.000164  -0.000000   0.000112   0.000005   0.000016   0.000039
    H3_ p       0.000019   0.000288   0.000003   0.000002   0.000000   0.000000
    H4_ s       0.000010   0.000127   0.000002   0.000000   0.000002  -0.000000
    H4_ p       0.000032   0.000322   0.000014   0.000025   0.000001   0.000000
 
   ao class      13a2       14a2       15a2       16a2       17a2       18a2 
    C1_ d       0.000001   0.000087   0.000060   0.000165   0.000002   0.000002
    C2_ p       0.000002   0.000034   0.000126   0.000006   0.000000   0.000003
    C2_ d       0.000010   0.000107   0.000249   0.000064   0.000004   0.000003
    C3_ p       0.000001  -0.000001   0.000081   0.000009   0.000003   0.000013
    C3_ d       0.000049   0.000028   0.000217   0.000147   0.000002   0.000000
    C4_ d       0.000030   0.000039   0.000090   0.000204   0.000007   0.000000
    C5_ d       0.000000   0.000405   0.000008   0.000126   0.000002   0.000000
    C6_ p       0.000036   0.000001   0.000001   0.000004   0.000002   0.000058
    C6_ d       0.000276   0.000001   0.000012   0.000003   0.000003   0.000105
    N1_ d       0.000000   0.000328   0.000010   0.000071   0.000001   0.000000
    N2_ d       0.000677   0.000004   0.000014   0.000012   0.000000   0.000002
    H1_ p       0.000003  -0.000000   0.000054   0.000001   0.000000   0.000000
    H2_ p       0.000004   0.000001   0.000046   0.000000   0.000001   0.000000
    H3_ p       0.000000   0.000000   0.000000   0.000002   0.000262   0.000040
    H4_ s       0.000054   0.000002   0.000007   0.000001   0.000001   0.000239
    H4_ p       0.000351   0.000001   0.000022   0.000026   0.000481   0.000173
 
   ao class      19a2       20a2       21a2       22a2       23a2       24a2 
    C1_ d       0.000021   0.000076   0.000002   0.000000   0.000000   0.000000
    C2_ p       0.000000   0.000000   0.000003   0.000001   0.000000   0.000000
    C2_ d       0.000029   0.000149   0.000022   0.000047   0.000000   0.000000
    C3_ p       0.000000  -0.000000   0.000004   0.000001   0.000000   0.000000
    C3_ d       0.000012   0.000137   0.000034   0.000035   0.000000   0.000000
    C4_ d       0.000000   0.000076  -0.000000   0.000000   0.000000   0.000000
    C5_ d       0.000002   0.000006   0.000000   0.000001   0.000000   0.000000
    C6_ p       0.000086   0.000010   0.000000   0.000000   0.000032   0.000013
    C6_ d       0.000162   0.000016   0.000001   0.000001   0.000042   0.000027
    N1_ d       0.000001   0.000002   0.000000   0.000000   0.000000   0.000000
    N2_ d       0.000006   0.000004   0.000000   0.000000   0.000019  -0.000000
    H1_ p      -0.000000   0.000000   0.000165   0.000162   0.000000   0.000000
    H2_ p       0.000002   0.000005   0.000236   0.000105  -0.000000  -0.000000
    H3_ p       0.000063   0.000005   0.000000   0.000000   0.000006   0.000059
    H4_ s       0.000027   0.000003   0.000000   0.000000   0.000003  -0.000002
    H4_ p       0.000161   0.000011   0.000002   0.000000   0.000186   0.000085
 
   ao class      25a2 
    C1_ d       0.000000
    C2_ p       0.000000
    C2_ d       0.000000
    C3_ p       0.000000
    C3_ d      -0.000000
    C4_ d       0.000000
    C5_ d       0.000000
    C6_ p       0.000007
    C6_ d       0.000001
    N1_ d       0.000000
    N2_ d       0.000000
    H1_ p      -0.000000
    H2_ p       0.000000
    H3_ p       0.000001
    H4_ s       0.000028
    H4_ p       0.000012


                        gross atomic populations
     ao           C1_        C2_        C3_        C4_        C5_        C6_
      s         3.104055   6.199304   6.130905   3.025600   3.090597   6.007114
      p         2.827006   5.613709   5.996285   2.547785   2.811414   5.621879
      d         0.116460   0.173511   0.166352   0.146255   0.097763   0.183178
    total       6.047521  11.986523  12.293543   5.719639   5.999775  11.812172
 
 
     ao           N1_        N2_        H1_        H2_        H3_        H4_
      s         3.611723   3.516513   1.874255   1.878427   1.826002   3.627774
      p         3.482313   3.893491   0.066530   0.068227   0.062365   0.123005
      d         0.052182   0.058021   0.000000   0.000000   0.000000   0.000000
    total       7.146218   7.468025   1.940785   1.946654   1.888367   3.750779
 

 Trace:   78.00000000

 Note:
 For the regular analysis, the Trace means the total number of electrons.  
 If the spin-density matrix was considered, the  Trace means the 2*S.  
  
