1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      09/20/00    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 using llenci= -1
================================================================================
four external integ   76.75 MB location: distr. (dual) 
three external inte   31.25 MB location: distr. (dual) 
four external integ   74.00 MB location: distr. (dual) 
three external inte   30.75 MB location: distr. (dual) 
diagonal integrals     0.25 MB location: virtual disk  
off-diagonal integr    5.00 MB location: virtual disk  
computed file size in DP units
fil3w:     4095875
fil3x:     4030341
fil4w:    10059469
fil4x:     9699032
ofdgint:       32760
diagint:      655200
computed file size in DP units
fil3w:    32767000
fil3x:    32242728
fil4w:    80475752
fil4x:    77592256
ofdgint:      262080
diagint:     5241600
 nsubmx= 6  lenci= 40481926
global arrays:     ********   (           4015.08 MB)
vdisk:               687960   (              5.25 MB)
drt:                2205973   (              8.42 MB)
================================================================================
 Main memory management:
 global         32960513 DP per process
 vdisk            687960 DP per process
 stack                 0 DP per process
 core           66351527 DP per process
Array Handle=-1000 Name:'civect' Data Type:double
Array Dimensions:40481926x6
Process=0	 owns array section: [1:2530121,1:6] 
Process=1	 owns array section: [2530122:5060242,1:6] 
Process=2	 owns array section: [5060243:7590363,1:6] 
Process=3	 owns array section: [7590364:10120484,1:6] 
Process=4	 owns array section: [10120485:12650605,1:6] 
Process=5	 owns array section: [12650606:15180726,1:6] 
Process=6	 owns array section: [15180727:17710847,1:6] 
Process=7	 owns array section: [17710848:20240968,1:6] 
Process=8	 owns array section: [20240969:22771089,1:6] 
Process=9	 owns array section: [22771090:25301210,1:6] 
Process=10	 owns array section: [25301211:27831331,1:6] 
Process=11	 owns array section: [27831332:30361452,1:6] 
Process=12	 owns array section: [30361453:32891573,1:6] 
Process=13	 owns array section: [32891574:35421694,1:6] 
Process=14	 owns array section: [35421695:37951815,1:6] 
Process=15	 owns array section: [37951816:40481926,1:6] 
Array Handle=-999 Name:'sigvec' Data Type:double
Array Dimensions:40481926x6
Process=0	 owns array section: [1:2530121,1:6] 
Process=1	 owns array section: [2530122:5060242,1:6] 
Process=2	 owns array section: [5060243:7590363,1:6] 
Process=3	 owns array section: [7590364:10120484,1:6] 
Process=4	 owns array section: [10120485:12650605,1:6] 
Process=5	 owns array section: [12650606:15180726,1:6] 
Process=6	 owns array section: [15180727:17710847,1:6] 
Process=7	 owns array section: [17710848:20240968,1:6] 
Process=8	 owns array section: [20240969:22771089,1:6] 
Process=9	 owns array section: [22771090:25301210,1:6] 
Process=10	 owns array section: [25301211:27831331,1:6] 
Process=11	 owns array section: [27831332:30361452,1:6] 
Process=12	 owns array section: [30361453:32891573,1:6] 
Process=13	 owns array section: [32891574:35421694,1:6] 
Process=14	 owns array section: [35421695:37951815,1:6] 
Process=15	 owns array section: [37951816:40481926,1:6] 
Array Handle=-998 Name:'hdg' Data Type:double
Array Dimensions:40481926x1
Process=0	 owns array section: [1:2530121,1:1] 
Process=1	 owns array section: [2530122:5060242,1:1] 
Process=2	 owns array section: [5060243:7590363,1:1] 
Process=3	 owns array section: [7590364:10120484,1:1] 
Process=4	 owns array section: [10120485:12650605,1:1] 
Process=5	 owns array section: [12650606:15180726,1:1] 
Process=6	 owns array section: [15180727:17710847,1:1] 
Process=7	 owns array section: [17710848:20240968,1:1] 
Process=8	 owns array section: [20240969:22771089,1:1] 
Process=9	 owns array section: [22771090:25301210,1:1] 
Process=10	 owns array section: [25301211:27831331,1:1] 
Process=11	 owns array section: [27831332:30361452,1:1] 
Process=12	 owns array section: [30361453:32891573,1:1] 
Process=13	 owns array section: [32891574:35421694,1:1] 
Process=14	 owns array section: [35421695:37951815,1:1] 
Process=15	 owns array section: [37951816:40481926,1:1] 
Array Handle=-997 Name:'drt' Data Type:double
Array Dimensions:157570x7
Process=0	 owns array section: [1:9849,1:7] 
Process=1	 owns array section: [9850:19698,1:7] 
Process=2	 owns array section: [19699:29547,1:7] 
Process=3	 owns array section: [29548:39396,1:7] 
Process=4	 owns array section: [39397:49245,1:7] 
Process=5	 owns array section: [49246:59094,1:7] 
Process=6	 owns array section: [59095:68943,1:7] 
Process=7	 owns array section: [68944:78792,1:7] 
Process=8	 owns array section: [78793:88641,1:7] 
Process=9	 owns array section: [88642:98490,1:7] 
Process=10	 owns array section: [98491:108339,1:7] 
Process=11	 owns array section: [108340:118188,1:7] 
Process=12	 owns array section: [118189:128037,1:7] 
Process=13	 owns array section: [128038:137886,1:7] 
Process=14	 owns array section: [137887:147735,1:7] 
Process=15	 owns array section: [147736:157570,1:7] 
Array Handle=-996 Name:'nxttask' Data Type:integer
Array Dimensions:1x1
Process=0	 owns array section: [1:1,1:1] 
Process=1	 owns array section: [0:-1,0:-1] 
Process=2	 owns array section: [0:-1,0:-1] 
Process=3	 owns array section: [0:-1,0:-1] 
Process=4	 owns array section: [0:-1,0:-1] 
Process=5	 owns array section: [0:-1,0:-1] 
Process=6	 owns array section: [0:-1,0:-1] 
Process=7	 owns array section: [0:-1,0:-1] 
Process=8	 owns array section: [0:-1,0:-1] 
Process=9	 owns array section: [0:-1,0:-1] 
Process=10	 owns array section: [0:-1,0:-1] 
Process=11	 owns array section: [0:-1,0:-1] 
Process=12	 owns array section: [0:-1,0:-1] 
Process=13	 owns array section: [0:-1,0:-1] 
Process=14	 owns array section: [0:-1,0:-1] 
Process=15	 owns array section: [0:-1,0:-1] 
 CIUDG version 5.9.7 ( 5-Oct-2004)
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 0,
  GSET = 0,
   DAVCOR =10,
  NCOREL = 22
  NROOT = 1
  IVMODE = 3
  NBKITR = 1
  NVBKMN = 1
  NVBKMX = 6
  NVRFMX = 6
  RTOLBK = 1e-3,1e-3,
  NITER =  10
  NVCIMN = 1
  NVCIMX =  6
  RTOLCI = 1e-3,1e-3,
  IDEN  = 1
  CSFPRN = 10,
  FTCALC = 1
  MAXSEG = 20
  nsegd = 1,1,6,6
  nseg0x = 1,1,2,2
  nseg1x = 1,1,2,2
  nseg2x = 1,1,4,4
  nseg3x = 1,1,2,2
  nseg4x = 1,1,6,6,
  c2ex0ex=0
  c3ex1ex=0
  cdg4ex=1
  fileloc=1,1,4,4,4,4
  &end
 ------------------------------------------------------------------------

 ** list of control variables **
 nrfitr =   30      nvrfmx =    6      nvrfmn =    1
 lvlprt =    0      nroot  =    1      noldv  =    0      noldhv =    0
 nunitv =    1      ntype  =    0      nbkitr =    1      niter  =   10
 ivmode =    3      vout   =    0      istrt  =    0      iortls =    0
 nvbkmx =    6      ibktv  =   -1      ibkthv =   -1      frcsub =    0
 nvcimx =    6      icitv  =   -1      icithv =   -1      maxseg =   20
 iden   =    1      itran  =    0      froot  =    0      rtmode =    0
 ftcalc =    1      lrtshift=1.0000    ncouple=    1      skipso  =   F
 ncorel =   22      csfprn  =  10      ctol   = 1.00E-02  davcor  =  10


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-03    1.000E-03
 
 units and filenames:
    1: ( 6)    ciudgls                                                     
    2: ( 5)    ciudgin                                                     
    3: ( 7)    ciudgsm                                                     
    4: ( 4)    cihdiag                                                     
    5: ( 3)    cihpseu                                                     
    6: (10)    cihvfl                                                      
    7: (11)    civfl                                                       
    8: (12)    diagint                                                     
    9: (13)    ofdgint                                                     
   10: (17)    cidrtfl                                                     
   11: (20)    ciftdfl                                                     
   12: (21)    ciftofl                                                     
   13: (24)    ciftifl                                                     
   14: ( 8)    ciflind                                                     
   15: (15)    civin                                                       
   16: (16)    civout                                                      
   18: (18)    d1fl                                                        
   20: (53)    cifvfl                                                      
   21: (25)    ciscr4                                                      
   22: (41)    ciftotd                                                     
   23: (42)    flacpfd                                                     
   26: (26)    ciscr5                                                      
   27: (47)    restart                                                     
   28: (48)    civout                                                      
   29: (49)    cirefv                                                      
   31: (31)    fil4w                                                       
   32: (32)    fil4x                                                       
   33: (33)    fil3w                                                       
   34: (34)    fil3x                                                       
   36: (36)    aoints                                                      
   37: (37)    aoints2                                                     
   38: (38)    drtfil                                                      
   39: (50)    cisrtif                                                     
   42: (50)    mocoef                                                      
   45: (52)    nocoef_ci                                                   
   46: (53)    civfl_restart                                               
 ------------------------------------------------------------------------
 broadcasting INDATA    
 broadcasting ACPFINFO  
 broadcasting CFILES    

 workspace allocation information: lcore=  66351527 mem1=         1 ifirst=         1

 integral file titles:
 Hermit Integral Program : SIFS version  j23             Tue Oct 19 14:53:16 2004
  cidrt_title                                                                    
 Hermit Integral Program : SIFS version  grimming        Tue Aug  7 18:24:26 2001
  title                                                                          
 mofmt: formatted orbitals label=morb    grimming        Tue Aug  7 18:38:16 2001
 SIFS file created by program tran.      j23             Tue Oct 19 14:53:26 2004

 core energy values from the integral file:
 energy( 1)=  1.034429979483E+02, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -1.274375457176E+02, ietype=    6,   fcore energy of type: H1(*)   
 energy( 3)= -1.129762209512E+02, ietype=    5,   fcore energy of type: Vref(*) 

 total core repulsion energy = -1.369707687204E+02

 drt header information:
  cidrt_title                                                                    
 spnorb, spnodd, lxyzir,hmult F F 0 0 0 0
 nmot  =   146 niot  =    13 nfct  =     4 nfvt  =     0
 nrow  =   119 nsym  =     4 ssym  =     1 lenbuf=  1600
 nwalk,xbar:      41172     1100    10410    14182    15480
 nvalwt,nvalw:    29883      579     9770     9854     9680
 ncsft:        40481926
 total number of valid internal walks:   29883
 nvalz,nvaly,nvalx,nvalw =      579    9770    9854    9680

 cisrt info file parameters:
 file number  12 blocksize   4095
 mxbld  20475
 nd4ext,nd2ext,nd0ext 16770  3354   182
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int  9468510  3698334   597417    46410     1461        0        0        0
 minbl4,minbl3,maxbl2  7650  7650  7653
 maxbuf 32767
 number of external orbitals per symmetry block:  46  47  18  18
 nmsym   4 number of internal orbitals  13

 formula file title:
 Hermit Integral Program : SIFS version  j23             Tue Oct 19 14:53:16 2004
  cidrt_title                                                                    
 Hermit Integral Program : SIFS version  grimming        Tue Aug  7 18:24:26 2001
  title                                                                          
 mofmt: formatted orbitals label=morb    grimming        Tue Aug  7 18:38:16 2001
 SIFS file created by program tran.      j23             Tue Oct 19 14:53:26 2004
  cidrt_title                                                                    
 file nmb.  24 block size     0
 pthz,pthy,pthx,pthw:  1100 10410 14182 15480 total internal walks:   41172
 maxlp3,n3xtlp,n2xtlp,n1xtlp,n0xtlp,n2lp,n1lp,n0lp     0     0     0     0
 orbsym(*)= 1 1 1 2 2 2 2 2 2 3 3 4 4

 setref:      172 references kept,
                0 references were marked as invalid, out of
              172 total.
 limcnvrt: found 579  valid internal walksout of  1100  walks (skipping trailing invalids)
  ... adding  579  segmentation marks segtype= 1
 limcnvrt: found 9770  valid internal walksout of  10410  walks (skipping trailing invalids)
  ... adding  9770  segmentation marks segtype= 2
 limcnvrt: found 9854  valid internal walksout of  14182  walks (skipping trailing invalids)
  ... adding  9854  segmentation marks segtype= 3
 limcnvrt: found 9680  valid internal walksout of  15480  walks (skipping trailing invalids)
  ... adding  9680  segmentation marks segtype= 4
 broadcasting SOLXYZ    
 broadcasting INDATA    
 broadcasting REPNUC    
 broadcasting INF       
 broadcasting SOINF     
 broadcasting DATA      
 broadcasting MOMAP     
 broadcasting CLI       
 broadcasting CSYM      
 broadcasting SLABEL    
 broadcasting MOMAP     
 broadcasting DRT       
 broadcasting INF1      
 broadcasting DRTINFO   
loaded      142 onel-dg  integrals(   1records ) at       1 on virtual disk
loaded    16770 4ext-dg  integrals(   5records ) at    4096 on virtual disk
loaded     3354 2ext-dg  integrals(   1records ) at   24571 on virtual disk
loaded      182 0ext-dg  integrals(   1records ) at   28666 on virtual disk
loaded     2551 onel-of  integrals(   1records ) at   32761 on virtual disk
loaded   597417 2ext-og  integrals( 146records ) at   36856 on virtual disk
loaded    46410 1ext-og  integrals(  12records ) at  634726 on virtual disk
loaded     1461 0ext-og  integrals(   1records ) at  683866 on virtual disk

 number of external paths / symmetry
 vertex x    2422    2486    1674    1674
 vertex w    2551    2486    1674    1674

 lprune: l(*,*,*) pruned with nwalk=    1100 nvalwt=     579 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=   10410 nvalwt=    9770 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    1711 nvalwt=    1643 nprune=     100

 lprune: l(*,*,*) pruned with nwalk=    1788 nvalwt=    1643 nprune=      87

 lprune: l(*,*,*) pruned with nwalk=    2230 nvalwt=    1643 nprune=      98

 lprune: l(*,*,*) pruned with nwalk=    2516 nvalwt=    1643 nprune=      90

 lprune: l(*,*,*) pruned with nwalk=    2826 nvalwt=    1643 nprune=      89

 lprune: l(*,*,*) pruned with nwalk=    3111 nvalwt=    1639 nprune=     106

 lprune: l(*,*,*) pruned with nwalk=    1710 nvalwt=    1614 nprune=     117

 lprune: l(*,*,*) pruned with nwalk=    2018 nvalwt=    1614 nprune=     108

 lprune: l(*,*,*) pruned with nwalk=    2479 nvalwt=    1614 nprune=      90

 lprune: l(*,*,*) pruned with nwalk=    2791 nvalwt=    1614 nprune=      88

 lprune: l(*,*,*) pruned with nwalk=    3098 nvalwt=    1614 nprune=      88

 lprune: l(*,*,*) pruned with nwalk=    3384 nvalwt=    1610 nprune=     107



                   segmentation summary for type diagonal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1        1100|       579|         0|       579|         0|         1|
 -------------------------------------------------------------------------------
  Y 2       10410|    306792|       579|      9770|       579|      1247|
 -------------------------------------------------------------------------------
  X 3        1711|   3359490|    307371|      1643|     10349|     11794|
 -------------------------------------------------------------------------------
  X 4        1773|   3330986|   3666861|      1643|     11992|     14008|
 -------------------------------------------------------------------------------
  X 5        2125|   3163374|   6997847|      1643|     13635|     16254|
 -------------------------------------------------------------------------------
  X 6        2441|   3298482|  10161221|      1643|     15278|     18624|
 -------------------------------------------------------------------------------
  X 7        2751|   3371922|  13459703|      1643|     16921|     21056|
 -------------------------------------------------------------------------------
  X 8        3071|   3458966|  16831625|      1639|     18564|     23546|
 -------------------------------------------------------------------------------
  W 9        1710|   3357561|  20290591|      1614|     20203|     26093|
 -------------------------------------------------------------------------------
  W10        2003|   3244143|  23648152|      1614|     21817|     28268|
 -------------------------------------------------------------------------------
  W11        2404|   3261383|  26892295|      1614|     23431|     30550|
 -------------------------------------------------------------------------------
  W12        2716|   3332572|  30153678|      1614|     25045|     32949|
 -------------------------------------------------------------------------------
  W13        3038|   3450124|  33486250|      1614|     26659|     35408|
 -------------------------------------------------------------------------------
  W14        3344|   3545552|  36936374|      1610|     28273|     37934|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>  40481926


 lprune: l(*,*,*) pruned with nwalk=    1100 nvalwt=     579 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=   10410 nvalwt=    9770 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    5728 nvalwt=    4928 nprune=      48

 lprune: l(*,*,*) pruned with nwalk=    8454 nvalwt=    4926 nprune=      84

 lprune: l(*,*,*) pruned with nwalk=    6206 nvalwt=    4841 nprune=      64

 lprune: l(*,*,*) pruned with nwalk=    9274 nvalwt=    4839 nprune=      80



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1        1100|       579|         0|       579|         0|         1|
 -------------------------------------------------------------------------------
  Y 2       10410|    306792|       579|      9770|       579|      1247|
 -------------------------------------------------------------------------------
  X 3        5728|   9852176|    307371|      4928|     10349|     11794|
 -------------------------------------------------------------------------------
  X 4        8274|  10131044|  10159547|      4926|     15277|     17557|
 -------------------------------------------------------------------------------
  W 5        6206|   9861413|  20290591|      4841|     20203|     23958|
 -------------------------------------------------------------------------------
  W 6        9109|  10329922|  30152004|      4839|     25044|     29745|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>  40481926


 lprune: l(*,*,*) pruned with nwalk=    1100 nvalwt=     579 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=   10410 nvalwt=    9770 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    5728 nvalwt=    4928 nprune=      48

 lprune: l(*,*,*) pruned with nwalk=    8454 nvalwt=    4926 nprune=      84

 lprune: l(*,*,*) pruned with nwalk=    6206 nvalwt=    4841 nprune=      64

 lprune: l(*,*,*) pruned with nwalk=    9274 nvalwt=    4839 nprune=      80



                   segmentation summary for type one-external
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1        1100|       579|         0|       579|         0|         1|
 -------------------------------------------------------------------------------
  Y 2       10410|    306792|       579|      9770|       579|      1247|
 -------------------------------------------------------------------------------
  X 3        5728|   9852176|    307371|      4928|     10349|     11794|
 -------------------------------------------------------------------------------
  X 4        8274|  10131044|  10159547|      4926|     15277|     17557|
 -------------------------------------------------------------------------------
  W 5        6206|   9861413|  20290591|      4841|     20203|     23958|
 -------------------------------------------------------------------------------
  W 6        9109|  10329922|  30152004|      4839|     25044|     29745|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>  40481926


 lprune: l(*,*,*) pruned with nwalk=    1100 nvalwt=     579 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=   10410 nvalwt=    9770 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    2604 nvalwt=    2464 nprune=      82

 lprune: l(*,*,*) pruned with nwalk=    3124 nvalwt=    2464 nprune=      76

 lprune: l(*,*,*) pruned with nwalk=    3613 nvalwt=    2464 nprune=      90

 lprune: l(*,*,*) pruned with nwalk=    4841 nvalwt=    2462 nprune=      86

 lprune: l(*,*,*) pruned with nwalk=    2634 nvalwt=    2421 nprune=      91

 lprune: l(*,*,*) pruned with nwalk=    3573 nvalwt=    2421 nprune=      90

 lprune: l(*,*,*) pruned with nwalk=    4215 nvalwt=    2421 nprune=      87

 lprune: l(*,*,*) pruned with nwalk=    5058 nvalwt=    2417 nprune=      83



                   segmentation summary for type two-external
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1        1100|       579|         0|       579|         0|         1|
 -------------------------------------------------------------------------------
  Y 2       10410|    306792|       579|      9770|       579|      1247|
 -------------------------------------------------------------------------------
  X 3        2604|   5030676|    307371|      2464|     10349|     11794|
 -------------------------------------------------------------------------------
  X 4        3094|   4821500|   5338047|      2464|     12813|     14865|
 -------------------------------------------------------------------------------
  X 5        3508|   4901008|  10159547|      2464|     15277|     18092|
 -------------------------------------------------------------------------------
  X 6        4766|   5230036|  15060555|      2462|     17741|     21437|
 -------------------------------------------------------------------------------
  W 7        2634|   5012242|  20290591|      2421|     20203|     25030|
 -------------------------------------------------------------------------------
  W 8        3453|   4850845|  25302833|      2421|     22624|     28044|
 -------------------------------------------------------------------------------
  W 9        4125|   5020146|  30153678|      2421|     25045|     31347|
 -------------------------------------------------------------------------------
  W10        4983|   5308102|  35173824|      2417|     27466|     34770|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>  40481926


 lprune: l(*,*,*) pruned with nwalk=    1100 nvalwt=     579 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=   10410 nvalwt=    9770 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    5728 nvalwt=    4928 nprune=      48

 lprune: l(*,*,*) pruned with nwalk=    8454 nvalwt=    4926 nprune=      84

 lprune: l(*,*,*) pruned with nwalk=    6206 nvalwt=    4841 nprune=      64

 lprune: l(*,*,*) pruned with nwalk=    9274 nvalwt=    4839 nprune=      80



                   segmentation summary for type three-external
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1        1100|       579|         0|       579|         0|         1|
 -------------------------------------------------------------------------------
  Y 2       10410|    306792|       579|      9770|       579|      1247|
 -------------------------------------------------------------------------------
  X 3        5728|   9852176|    307371|      4928|     10349|     11794|
 -------------------------------------------------------------------------------
  X 4        8274|  10131044|  10159547|      4926|     15277|     17557|
 -------------------------------------------------------------------------------
  W 5        6206|   9861413|  20290591|      4841|     20203|     23958|
 -------------------------------------------------------------------------------
  W 6        9109|  10329922|  30152004|      4839|     25044|     29745|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>  40481926


 lprune: l(*,*,*) pruned with nwalk=    1100 nvalwt=     579 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=   10410 nvalwt=    9770 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    1711 nvalwt=    1643 nprune=     100

 lprune: l(*,*,*) pruned with nwalk=    1788 nvalwt=    1643 nprune=      87

 lprune: l(*,*,*) pruned with nwalk=    2230 nvalwt=    1643 nprune=      98

 lprune: l(*,*,*) pruned with nwalk=    2516 nvalwt=    1643 nprune=      90

 lprune: l(*,*,*) pruned with nwalk=    2826 nvalwt=    1643 nprune=      89

 lprune: l(*,*,*) pruned with nwalk=    3111 nvalwt=    1639 nprune=     106

 lprune: l(*,*,*) pruned with nwalk=    1710 nvalwt=    1614 nprune=     117

 lprune: l(*,*,*) pruned with nwalk=    2018 nvalwt=    1614 nprune=     108

 lprune: l(*,*,*) pruned with nwalk=    2479 nvalwt=    1614 nprune=      90

 lprune: l(*,*,*) pruned with nwalk=    2791 nvalwt=    1614 nprune=      88

 lprune: l(*,*,*) pruned with nwalk=    3098 nvalwt=    1614 nprune=      88

 lprune: l(*,*,*) pruned with nwalk=    3384 nvalwt=    1610 nprune=     107



                   segmentation summary for type four-external
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1        1100|       579|         0|       579|         0|         1|
 -------------------------------------------------------------------------------
  Y 2       10410|    306792|       579|      9770|       579|      1247|
 -------------------------------------------------------------------------------
  X 3        1711|   3359490|    307371|      1643|     10349|     11794|
 -------------------------------------------------------------------------------
  X 4        1773|   3330986|   3666861|      1643|     11992|     14008|
 -------------------------------------------------------------------------------
  X 5        2125|   3163374|   6997847|      1643|     13635|     16254|
 -------------------------------------------------------------------------------
  X 6        2441|   3298482|  10161221|      1643|     15278|     18624|
 -------------------------------------------------------------------------------
  X 7        2751|   3371922|  13459703|      1643|     16921|     21056|
 -------------------------------------------------------------------------------
  X 8        3071|   3458966|  16831625|      1639|     18564|     23546|
 -------------------------------------------------------------------------------
  W 9        1710|   3357561|  20290591|      1614|     20203|     26093|
 -------------------------------------------------------------------------------
  W10        2003|   3244143|  23648152|      1614|     21817|     28268|
 -------------------------------------------------------------------------------
  W11        2404|   3261383|  26892295|      1614|     23431|     30550|
 -------------------------------------------------------------------------------
  W12        2716|   3332572|  30153678|      1614|     25045|     32949|
 -------------------------------------------------------------------------------
  W13        3038|   3450124|  33486250|      1614|     26659|     35408|
 -------------------------------------------------------------------------------
  W14        3344|   3545552|  36936374|      1610|     28273|     37934|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>  40481926

 broadcasting CIVCT     
 broadcasting DATA      
 broadcasting INF       
 broadcasting CNFSPC    
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS
----------------------------------------------------------------------------------------------------
     1  2   2    21      two-ext yy   2X  2 2   10410   10410     306792     306792    9770    9770
     2  3   3    22      two-ext xx   2X  3 3    2604    2604    5030676    5030676    2464    2464
     3  4   3    22      two-ext xx   2X  3 3    3094    2604    4821500    5030676    2464    2464
     4  5   3    22      two-ext xx   2X  3 3    3508    2604    4901008    5030676    2464    2464
     5  6   3    22      two-ext xx   2X  3 3    4766    2604    5230036    5030676    2462    2464
     6  4   4    22      two-ext xx   2X  3 3    3094    3094    4821500    4821500    2464    2464
     7  5   4    22      two-ext xx   2X  3 3    3508    3094    4901008    4821500    2464    2464
     8  6   4    22      two-ext xx   2X  3 3    4766    3094    5230036    4821500    2462    2464
     9  5   5    22      two-ext xx   2X  3 3    3508    3508    4901008    4901008    2464    2464
    10  6   5    22      two-ext xx   2X  3 3    4766    3508    5230036    4901008    2462    2464
    11  6   6    22      two-ext xx   2X  3 3    4766    4766    5230036    5230036    2462    2462
    12  7   7    23      two-ext ww   2X  4 4    2634    2634    5012242    5012242    2421    2421
    13  8   7    23      two-ext ww   2X  4 4    3453    2634    4850845    5012242    2421    2421
    14  9   7    23      two-ext ww   2X  4 4    4125    2634    5020146    5012242    2421    2421
    15 10   7    23      two-ext ww   2X  4 4    4983    2634    5308102    5012242    2417    2421
    16  8   8    23      two-ext ww   2X  4 4    3453    3453    4850845    4850845    2421    2421
    17  9   8    23      two-ext ww   2X  4 4    4125    3453    5020146    4850845    2421    2421
    18 10   8    23      two-ext ww   2X  4 4    4983    3453    5308102    4850845    2417    2421
    19  9   9    23      two-ext ww   2X  4 4    4125    4125    5020146    5020146    2421    2421
    20 10   9    23      two-ext ww   2X  4 4    4983    4125    5308102    5020146    2417    2421
    21 10  10    23      two-ext ww   2X  4 4    4983    4983    5308102    5308102    2417    2417
    22  3   1    24      two-ext xz   2X  3 1    2604    1100    5030676        579    2464     579
    23  4   1    24      two-ext xz   2X  3 1    3094    1100    4821500        579    2464     579
    24  5   1    24      two-ext xz   2X  3 1    3508    1100    4901008        579    2464     579
    25  6   1    24      two-ext xz   2X  3 1    4766    1100    5230036        579    2462     579
    26  7   1    25      two-ext wz   2X  4 1    2634    1100    5012242        579    2421     579
    27  8   1    25      two-ext wz   2X  4 1    3453    1100    4850845        579    2421     579
    28  9   1    25      two-ext wz   2X  4 1    4125    1100    5020146        579    2421     579
    29 10   1    25      two-ext wz   2X  4 1    4983    1100    5308102        579    2417     579
    30  7   3    26      two-ext wx   2X  4 3    2634    2604    5012242    5030676    2421    2464
    31  8   3    26      two-ext wx   2X  4 3    3453    2604    4850845    5030676    2421    2464
    32  9   3    26      two-ext wx   2X  4 3    4125    2604    5020146    5030676    2421    2464
    33 10   3    26      two-ext wx   2X  4 3    4983    2604    5308102    5030676    2417    2464
    34  7   4    26      two-ext wx   2X  4 3    2634    3094    5012242    4821500    2421    2464
    35  8   4    26      two-ext wx   2X  4 3    3453    3094    4850845    4821500    2421    2464
    36  9   4    26      two-ext wx   2X  4 3    4125    3094    5020146    4821500    2421    2464
    37 10   4    26      two-ext wx   2X  4 3    4983    3094    5308102    4821500    2417    2464
    38  7   5    26      two-ext wx   2X  4 3    2634    3508    5012242    4901008    2421    2464
    39  8   5    26      two-ext wx   2X  4 3    3453    3508    4850845    4901008    2421    2464
    40  9   5    26      two-ext wx   2X  4 3    4125    3508    5020146    4901008    2421    2464
    41 10   5    26      two-ext wx   2X  4 3    4983    3508    5308102    4901008    2417    2464
    42  7   6    26      two-ext wx   2X  4 3    2634    4766    5012242    5230036    2421    2462
    43  8   6    26      two-ext wx   2X  4 3    3453    4766    4850845    5230036    2421    2462
    44  9   6    26      two-ext wx   2X  4 3    4125    4766    5020146    5230036    2421    2462
    45 10   6    26      two-ext wx   2X  4 3    4983    4766    5308102    5230036    2417    2462
    46  2   1    11      one-ext yz   1X  2 1   10410    1100     306792        579    9770     579
    47  3   2    13      one-ext yx   1X  3 2    5728   10410    9852176     306792    4928    9770
    48  4   2    13      one-ext yx   1X  3 2    8274   10410   10131044     306792    4926    9770
    49  5   2    14      one-ext yw   1X  4 2    6206   10410    9861413     306792    4841    9770
    50  6   2    14      one-ext yw   1X  4 2    9109   10410   10329922     306792    4839    9770
    51  3   2    31      thr-ext yx   3X  3 2    5728   10410    9852176     306792    4928    9770
    52  4   2    31      thr-ext yx   3X  3 2    8274   10410   10131044     306792    4926    9770
    53  5   2    32      thr-ext yw   3X  4 2    6206   10410    9861413     306792    4841    9770
    54  6   2    32      thr-ext yw   3X  4 2    9109   10410   10329922     306792    4839    9770
    55  1   1     1      allint zz    OX  1 1    1100    1100        579        579     579     579
    56  2   2     2      allint yy    OX  2 2   10410   10410     306792     306792    9770    9770
    57  3   3     3      allint xx    OX  3 3    5728    5728    9852176    9852176    4928    4928
    58  4   3     3      allint xx    OX  3 3    8274    5728   10131044    9852176    4926    4928
    59  4   4     3      allint xx    OX  3 3    8274    8274   10131044   10131044    4926    4926
    60  5   5     4      allint ww    OX  4 4    6206    6206    9861413    9861413    4841    4841
    61  6   5     4      allint ww    OX  4 4    9109    6206   10329922    9861413    4839    4841
    62  6   6     4      allint ww    OX  4 4    9109    9109   10329922   10329922    4839    4839
    63  1   1    75      dg-024ext z  DG  1 1    1100    1100        579        579     579     579
    64  2   2    45      4exdg024 y   DG  2 2   10410   10410     306792     306792    9770    9770
    65  3   3    46      4exdg024 x   DG  3 3    1711    1711    3359490    3359490    1643    1643
    66  4   4    46      4exdg024 x   DG  3 3    1773    1773    3330986    3330986    1643    1643
    67  5   5    46      4exdg024 x   DG  3 3    2125    2125    3163374    3163374    1643    1643
    68  6   6    46      4exdg024 x   DG  3 3    2441    2441    3298482    3298482    1643    1643
    69  7   7    46      4exdg024 x   DG  3 3    2751    2751    3371922    3371922    1643    1643
    70  8   8    46      4exdg024 x   DG  3 3    3071    3071    3458966    3458966    1639    1639
    71  9   9    47      4exdg024 w   DG  4 4    1710    1710    3357561    3357561    1614    1614
    72 10  10    47      4exdg024 w   DG  4 4    2003    2003    3244143    3244143    1614    1614
    73 11  11    47      4exdg024 w   DG  4 4    2404    2404    3261383    3261383    1614    1614
    74 12  12    47      4exdg024 w   DG  4 4    2716    2716    3332572    3332572    1614    1614
    75 13  13    47      4exdg024 w   DG  4 4    3038    3038    3450124    3450124    1614    1614
    76 14  14    47      4exdg024 w   DG  4 4    3344    3344    3545552    3545552    1610    1610
----------------------------------------------------------------------------------------------------
 broadcasting TASKLST   
 DIAGEL COUNTS: DG0X,DG2X,DG4X= 1142101 239374 29304
 diagonal elements written to file   4

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:   21947 2x:       0 4x:       0
All internal counts: zz :   55360 yy:       0 xx:       0 ww:       0
One-external counts: yz :       0 yx:       0 yw:       0
Two-external counts: yy :       0 ww:       0 xx:       0 xz:       0 wz:       0 wx:       0
Three-ext.   counts: yx :       0 yw:       0

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 reference space has dimension     172

    root           eigenvalues
    ----           ------------
       1        -155.0180130005

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt= 579

         vector  1 from unit 11 written to unit 49 filename cirefv                                                      
 ufvoutnew: ... writing  recamt= 579

         vector  1 from unit 11 written to unit 49 filename cirefv                                                      

 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=   579)...
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:          40481926
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                    1
 residual norm convergence criteria:               0.001000

          starting bk iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x: 1142101 2x:  239374 4x:   29304
All internal counts: zz :   55360 yy:       0 xx:       0 ww:       0
One-external counts: yz :  391887 yx:       0 yw:       0
Two-external counts: yy :       0 ww:       0 xx:       0 xz:   30921 wz:   32232 wx:       0
Three-ext.   counts: yx :       0 yw:       0

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 579

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -155.0180130005  1.8119E-13  4.7767E-01  1.3807E+00  1.0000E-03
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   21    0   0.00   0.00   0.00   0.00         0.    0.0000
    2   22    0   0.00   0.00   0.00   0.00         0.    0.0000
    3   22    0   0.00   0.00   0.00   0.00         0.    0.0000
    4   22    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   22    0   0.00   0.00   0.00   0.00         0.    0.0000
    6   22    0   0.00   0.00   0.00   0.00         0.    0.0000
    7   22    0   0.00   0.00   0.00   0.00         0.    0.0000
    8   22    0   0.00   0.00   0.00   0.00         0.    0.0000
    9   22    0   0.00   0.00   0.00   0.00         0.    0.0000
   10   22    0   0.00   0.00   0.00   0.00         0.    0.0000
   11   22    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   23    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   23    0   0.00   0.00   0.00   0.00         0.    0.0000
   14   23    0   0.00   0.00   0.00   0.00         0.    0.0000
   15   23    0   0.00   0.00   0.00   0.00         0.    0.0000
   16   23    0   0.00   0.00   0.00   0.00         0.    0.0000
   17   23    0   0.00   0.00   0.00   0.00         0.    0.0000
   18   23    0   0.00   0.00   0.00   0.00         0.    0.0000
   19   23    0   0.00   0.00   0.00   0.00         0.    0.0000
   20   23    0   0.00   0.00   0.00   0.00         0.    0.0000
   21   23    0   0.00   0.00   0.00   0.00         0.    0.0000
   22   24    0   0.40   0.01   0.00   0.18         0.    0.0054
   23   24   13   0.41   0.01   0.00   0.19         0.    0.0071
   24   24    1   0.36   0.01   0.00   0.15         0.    0.0052
   25   24    9   0.40   0.01   0.00   0.16         0.    0.0043
   26   25    5   0.42   0.01   0.00   0.20         0.    0.0055
   27   25    2   0.40   0.01   0.00   0.18         0.    0.0067
   28   25    6   0.49   0.01   0.00   0.17         0.    0.0045
   29   25   10   0.43   0.01   0.00   0.18         0.    0.0047
   30   26    0   0.00   0.00   0.00   0.00         0.    0.0000
   31   26    0   0.00   0.00   0.00   0.00         0.    0.0000
   32   26    0   0.00   0.00   0.00   0.00         0.    0.0000
   33   26    0   0.00   0.00   0.00   0.00         0.    0.0000
   34   26    0   0.00   0.00   0.00   0.00         0.    0.0000
   35   26    0   0.00   0.00   0.00   0.00         0.    0.0000
   36   26    0   0.00   0.00   0.00   0.00         0.    0.0000
   37   26    0   0.00   0.00   0.00   0.00         0.    0.0000
   38   26    0   0.00   0.00   0.00   0.00         0.    0.0000
   39   26    0   0.00   0.00   0.00   0.00         0.    0.0000
   40   26    0   0.00   0.00   0.00   0.00         0.    0.0000
   41   26    0   0.00   0.00   0.00   0.00         0.    0.0000
   42   26    0   0.00   0.00   0.00   0.00         0.    0.0000
   43   26    0   0.00   0.00   0.00   0.00         0.    0.0000
   44   26    0   0.00   0.00   0.00   0.00         0.    0.0000
   45   26    0   0.00   0.00   0.00   0.00         0.    0.0000
   46   11    3   0.86   0.48   0.00   0.85         0.    0.3188
   47   13    0   0.00   0.00   0.00   0.00         0.    0.0000
   48   13    0   0.00   0.00   0.00   0.00         0.    0.0000
   49   14    0   0.00   0.00   0.00   0.00         0.    0.0000
   50   14    0   0.00   0.00   0.00   0.00         0.    0.0000
   51   31    0   0.00   0.00   0.00   0.00         0.    0.0000
   52   31    0   0.00   0.00   0.00   0.00         0.    0.0000
   53   32    0   0.00   0.00   0.00   0.00         0.    0.0000
   54   32    0   0.00   0.00   0.00   0.00         0.    0.0000
   55    1    4   0.13   0.10   0.00   0.13         0.    0.0452
   56    2    0   0.00   0.00   0.00   0.00         0.    0.0000
   57    3    0   0.00   0.00   0.00   0.00         0.    0.0000
   58    3    0   0.00   0.00   0.00   0.00         0.    0.0000
   59    3    0   0.00   0.00   0.00   0.00         0.    0.0000
   60    4    0   0.00   0.00   0.00   0.00         0.    0.0000
   61    4    0   0.00   0.00   0.00   0.00         0.    0.0000
   62    4    0   0.00   0.00   0.00   0.00         0.    0.0000
   63   75   14   0.02   0.01   0.00   0.02         0.    0.0163
   64   45    7   0.30   0.16   0.00   0.29         0.    0.0432
   65   46   11   1.16   0.02   0.00   1.06         0.    0.0059
   66   46   15   1.64   0.02   0.00   1.44         0.    0.0065
   67   46    8   1.73   0.03   0.00   1.55         0.    0.0066
   68   46   12   1.84   0.03   0.00   1.75         0.    0.0068
   69   46   14   1.55   0.03   0.00   1.45         0.    0.0070
   70   46    4   2.13   0.03   0.00   1.94         0.    0.0073
   71   47    7   1.24   0.01   0.00   1.12         0.    0.0056
   72   47    1   1.49   0.02   0.00   1.39         0.    0.0064
   73   47    0   1.80   0.03   0.00   1.70         0.    0.0067
   74   47    9   1.76   0.03   0.00   1.66         0.    0.0069
   75   47    2   1.39   0.03   0.00   1.29         0.    0.0070
   76   47   13   1.94   0.03   0.00   1.84         0.    0.0074
================================================================
================ TIMING STATISTICS PER NODE    ================
node# twait    ths   tcin   teig  tvect  tga   ttotal 
  1   0.15   0.04   0.26   0.00   0.00   0.18   2.81
  2   0.51   0.04   0.26   0.00   0.00   0.17   3.01
  3   0.57   0.04   0.26   0.00   0.00   0.19   3.01
  4   1.50   0.04   0.26   0.00   0.00   0.01   3.05
  5   0.10   0.04   0.26   0.00   0.00   0.12   3.01
  6   1.93   0.04   0.26   0.00   0.00   0.12   2.85
  7   1.87   0.04   0.26   0.00   0.00   0.21   2.85
  8   0.81   0.04   0.26   0.00   0.00   0.08   3.03
  9   0.62   0.04   0.26   0.00   0.00   0.10   3.07
 10   0.19   0.04   0.26   0.00   0.00   0.20   3.17
 11   1.93   0.04   0.26   0.00   0.00   0.13   3.07
 12   1.19   0.04   0.26   0.00   0.00   0.08   2.85
 13   0.52   0.04   0.26   0.00   0.00   0.06   3.05
 14   0.00   0.04   0.26   0.00   0.00   0.19   3.13
 15   0.78   0.04   0.26   0.00   0.00   0.07   2.97
 16   0.72   0.04   0.26   0.00   0.00   0.11   3.09
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                    24.3048s 
time spent in multnx:                  20.8852s 
integral transfer time:                 0.0339s 
time spent for loop construction:       1.1117s 
syncronization time in mult:           13.3724s 
total time per CI iteration:           48.0472s 
 ** parallelization degree for mult section:0.6451

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)           0.01        0.00          0.000
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.00        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:            0.02        0.00          0.000
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)         617.74        1.13        545.035
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)        617.74        0.87        709.564
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.34        0.00         93.279
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:         1235.82        2.01        615.564
========================================================


 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -155.0180130005  1.8119E-13  4.7767E-01  1.3807E+00  1.0000E-03
 
 expansion vectors are not transformed.
 matrix-vector products are not transformed.

    2 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)

 from bk iterations: iconv=   1

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:          40481926
 number of initial trial vectors:                         2
 number of initial matrix-vector products:                1
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                   10
 residual norm convergence criteria:               0.001000

          starting ci iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x: 1142101 2x:  239374 4x:   29304
All internal counts: zz :   55360 yy: 1825441 xx: 1699543 ww: 1500549
One-external counts: yz :  391887 yx: 3708744 yw: 3487646
Two-external counts: yy :  631018 ww:  333356 xx:  483913 xz:   30921 wz:   32232 wx:  652690
Three-ext.   counts: yx : 3261530 yw: 3217932

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 579

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.94316195    -0.33233346

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -155.4047600194  3.8675E-01  2.0332E-02  2.6434E-01  1.0000E-03
 mr-sdci #  1  2   -151.9030609018  1.4932E+01  0.0000E+00  0.0000E+00  1.0000E-03
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   21    6   9.69   0.31   0.00   9.68         0.    0.3634
    2   22    4  25.16   0.05   0.00  24.98         0.    0.0701
    3   22    0  14.74   0.02   0.00  14.45         0.    0.0326
    4   22   13   6.26   0.01   0.00   6.00         0.    0.0137
    5   22    5   4.02   0.01   0.00   3.72         0.    0.0084
    6   22    8  18.22   0.04   0.00  18.08         0.    0.0492
    7   22    3   5.37   0.01   0.00   5.06         0.    0.0060
    8   22    2   5.09   0.01   0.00   4.81         0.    0.0060
    9   22   12  15.10   0.05   0.00  14.97         0.    0.0478
   10   22    3   4.62   0.01   0.00   4.35         0.    0.0091
   11   22   10  16.00   0.04   0.00  14.16         0.    0.0414
   12   23   15  16.16   0.03   0.00  16.00         0.    0.0383
   13   23    5   6.21   0.01   0.00   5.95         0.    0.0122
   14   23   12   4.76   0.01   0.00   4.51         0.    0.0104
   15   23   12   3.61   0.01   0.00   3.35         0.    0.0073
   16   23    8  10.95   0.03   0.00  10.80         0.    0.0302
   17   23    1   4.41   0.01   0.00   4.09         0.    0.0070
   18   23    8   2.45   0.00   0.00   2.11         0.    0.0030
   19   23    6  10.41   0.03   0.00  10.25         0.    0.0298
   20   23   12   3.62   0.01   0.00   3.20         0.    0.0052
   21   23   14  14.34   0.03   0.00  14.20         0.    0.0302
   22   24   12   0.35   0.01   0.00   0.18         0.    0.0054
   23   24    6   0.42   0.01   0.00   0.25         0.    0.0071
   24   24    6   0.39   0.01   0.00   0.21         0.    0.0052
   25   24   12   0.36   0.01   0.00   0.18         0.    0.0043
   26   25   12   0.40   0.01   0.00   0.21         0.    0.0055
   27   25    8   0.42   0.01   0.00   0.25         0.    0.0067
   28   25    6   0.38   0.01   0.00   0.22         0.    0.0045
   29   25    8   0.45   0.01   0.00   0.23         0.    0.0047
   30   26    6  33.46   0.06   0.00  33.06         0.    0.0870
   31   26    8   7.80   0.01   0.00   7.45         0.    0.0128
   32   26   12   6.51   0.01   0.00   6.26         0.    0.0106
   33   26   13   3.66   0.01   0.00   3.27         0.    0.0079
   34   26   11  13.22   0.03   0.00  12.80         0.    0.0346
   35   26   10  19.57   0.05   0.00  19.22         0.    0.0518
   36   26    2   3.93   0.01   0.00   3.55         0.    0.0071
   37   26    5   2.26   0.00   0.00   1.88         0.    0.0035
   38   26    3   6.01   0.01   0.00   5.57         0.    0.0123
   39   26    1   7.99   0.02   0.00   7.61         0.    0.0192
   40   26   14  20.78   0.06   0.00  20.37         0.    0.0574
   41   26    7   3.63   0.01   0.00   3.21         0.    0.0057
   42   26    0   4.12   0.01   0.00   3.76         0.    0.0085
   43   26   15   3.02   0.00   0.00   2.63         0.    0.0044
   44   26   12   5.79   0.01   0.00   5.51         0.    0.0121
   45   26    8  21.30   0.05   0.01  20.99         0.    0.0530
   46   11    4   0.92   0.48   0.00   0.91         0.    0.3188
   47   13   13  28.87   1.07   0.00  28.61         0.    1.2724
   48   13    9  35.45   1.53   0.00  35.08         0.    1.4272
   49   14    2  22.96   1.05   0.00  22.66         0.    1.2155
   50   14    7  30.75   1.47   0.00  30.46         0.    1.3247
   51   31   15  19.19   0.02   0.00  18.89         0.    0.0066
   52   31    0  22.72   0.02   0.00  22.43         0.    0.0062
   53   32    3  19.98   0.02   0.00  19.64         0.    0.0066
   54   32   12  23.41   0.02   0.00  23.10         0.    0.0056
   55    1    4   0.12   0.10   0.00   0.12         0.    0.0452
   56    2    4   3.03   1.36   0.00   3.02         0.    1.1852
   57    3    5  23.87   0.42   0.00  23.58         0.    0.4367
   58    3    4   8.22   0.33   0.00   7.33         0.    0.2861
   59    3   11  20.56   0.49   0.00  20.24         0.    0.3673
   60    4    1  22.52   0.39   0.00  22.22         0.    0.3876
   61    4   10   8.49   0.27   0.00   7.56         0.    0.2123
   62    4    4  17.71   0.50   0.00  17.43         0.    0.3423
   63   75    4   0.02   0.01   0.00   0.02         0.    0.0163
   64   45    8   0.33   0.16   0.00   0.32         0.    0.0000
   65   46    1  17.95   0.02   0.00  17.85         0.    0.0000
   66   46    7  18.38   0.02   0.00  18.28         0.    0.0000
   67   46   13  16.40   0.02   0.00  16.30         0.    0.0000
   68   46   11  17.07   0.03   0.00  16.97         0.    0.0000
   69   46    5  17.76   0.03   0.00  17.66         0.    0.0000
   70   46    0  18.54   0.03   0.00  18.44         0.    0.0000
   71   47   14  18.22   0.01   0.00  18.12         0.    0.0000
   72   47    3  17.82   0.02   0.00  17.72         0.    0.0000
   73   47   10  17.29   0.03   0.00  17.19         0.    0.0000
   74   47    2  17.68   0.03   0.00  17.58         0.    0.0000
   75   47   15  19.38   0.03   0.00  19.28         0.    0.0000
   76   47    9  20.91   0.03   0.00  20.80         0.    0.0000
================================================================
================ TIMING STATISTICS PER NODE    ================
node# twait    ths   tcin   teig  tvect  tga   ttotal 
  1   3.98   0.06   0.30   0.00   0.00   0.71  64.72
  2  11.20   0.17   0.30   0.00   0.00   0.76  64.79
  3  13.61   0.17   0.30   0.00   0.00   0.71  64.80
  4   9.10   0.17   0.30   0.00   0.00   0.99  64.85
  5   8.88   0.17   0.30   0.00   0.00   0.85  64.75
  6   9.94   0.17   0.30   0.00   0.00   0.92  64.74
  7   8.17   0.17   0.30   0.00   0.00   0.77  64.72
  8  11.33   0.17   0.30   0.00   0.00   0.52  64.83
  9   0.88   0.17   0.30   0.00   0.00   1.13  64.85
 10   7.73   0.17   0.30   0.00   0.00   0.28  64.88
 11   1.80   0.17   0.30   0.00   0.00   2.72  64.88
 12  13.24   0.17   0.30   0.00   0.00   0.55  64.83
 13   0.00   0.17   0.30   0.00   0.00   1.67  64.81
 14   8.28   0.17   0.30   0.00   0.00   0.68  64.88
 15  10.73   0.17   0.30   0.00   0.00   0.43  64.78
 16   5.45   0.17   0.30   0.00   0.00   0.63  64.83
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                   893.9827s 
time spent in multnx:                 873.4160s 
integral transfer time:                 0.1776s 
time spent for loop construction:      11.1067s 
syncronization time in mult:          124.3349s 
total time per CI iteration:         1036.9377s 
 ** parallelization degree for mult section:0.8779

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)           2.35        0.00        845.369
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)          2.34        0.00        809.144
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.02        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:            4.71        0.01        813.904
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)        4316.90        8.62        500.905
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)       4316.90        5.67        761.452
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           1.91        0.01        130.950
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:         8635.71       14.30        603.808
========================================================


          starting ci iteration   2

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x: 1142101 2x:  239374 4x:   29304
All internal counts: zz :   55360 yy: 1825441 xx: 1699543 ww: 1500549
One-external counts: yz :  391887 yx: 3708744 yw: 3487646
Two-external counts: yy :  631018 ww:  333356 xx:  483913 xz:   30921 wz:   32232 wx:  652690
Three-ext.   counts: yx : 3261530 yw: 3217932

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 579

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.94098563    -0.04761293     0.33508067

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1   -155.4195718468  1.4812E-02  1.6834E-03  8.1734E-02  1.0000E-03
 mr-sdci #  2  2   -152.5778644186  6.7480E-01  0.0000E+00  0.0000E+00  1.0000E-03
 mr-sdci #  2  3   -151.9005119929  1.4930E+01  0.0000E+00  2.2054E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   21   15   9.08   0.30   0.00   9.07         0.    0.3634
    2   22   13  24.63   0.05   0.00  24.47         0.    0.0701
    3   22    5  13.54   0.02   0.00  13.27         0.    0.0326
    4   22   14   7.13   0.01   0.00   6.86         0.    0.0137
    5   22   15   3.73   0.01   0.00   3.47         0.    0.0084
    6   22   14  17.64   0.04   0.00  17.50         0.    0.0492
    7   22    9   3.41   0.01   0.00   3.15         0.    0.0060
    8   22   14   3.24   0.01   0.00   2.99         0.    0.0060
    9   22   11  15.87   0.05   0.00  15.74         0.    0.0478
   10   22   13   4.52   0.01   0.00   4.25         0.    0.0091
   11   22    8  15.58   0.04   0.00  15.44         0.    0.0414
   12   23    0  15.07   0.03   0.00  14.94         0.    0.0383
   13   23    0   5.89   0.01   0.00   5.64         0.    0.0122
   14   23    8   4.79   0.01   0.00   4.53         0.    0.0104
   15   23    0   3.56   0.01   0.00   3.31         0.    0.0073
   16   23    4  11.96   0.03   0.00  11.83         0.    0.0302
   17   23   12   3.85   0.01   0.00   3.59         0.    0.0070
   18   23    4   2.16   0.00   0.00   1.89         0.    0.0030
   19   23   10  11.76   0.03   0.00  11.62         0.    0.0298
   20   23   14   3.44   0.01   0.00   3.17         0.    0.0052
   21   23    3  13.21   0.03   0.00  13.08         0.    0.0302
   22   24    1   0.28   0.01   0.00   0.15         0.    0.0054
   23   24   11   0.33   0.01   0.00   0.20         0.    0.0071
   24   24    7   0.30   0.01   0.00   0.17         0.    0.0052
   25   24   15   0.28   0.01   0.00   0.15         0.    0.0043
   26   25    1   0.30   0.01   0.00   0.17         0.    0.0055
   27   25   15   0.35   0.01   0.00   0.22         0.    0.0067
   28   25   11   0.31   0.01   0.00   0.17         0.    0.0045
   29   25    7   0.33   0.01   0.00   0.19         0.    0.0047
   30   26    8  32.93   0.06   0.00  32.63         0.    0.0870
   31   26    9   7.08   0.01   0.00   6.81         0.    0.0128
   32   26    6   5.70   0.01   0.00   5.42         0.    0.0106
   33   26    5   3.52   0.01   0.00   3.26         0.    0.0079
   34   26    7  13.45   0.03   0.00  13.16         0.    0.0346
   35   26    7  20.67   0.05   0.00  20.38         0.    0.0518
   36   26    3   3.79   0.01   0.00   3.55         0.    0.0071
   37   26    6   2.12   0.00   0.00   1.88         0.    0.0035
   38   26    2   5.76   0.01   0.00   5.49         0.    0.0123
   39   26   12   8.62   0.02   0.00   8.37         0.    0.0192
   40   26   14  21.84   0.06   0.00  21.54         0.    0.0574
   41   26    9   3.47   0.01   0.00   3.21         0.    0.0057
   42   26    1   3.97   0.01   0.00   3.71         0.    0.0085
   43   26   10   2.87   0.00   0.00   2.61         0.    0.0044
   44   26    6   5.63   0.01   0.00   5.36         0.    0.0121
   45   26   12  21.95   0.05   0.01  21.62         0.    0.0530
   46   11   12   0.87   0.48   0.00   0.86         0.    0.3188
   47   13    4  22.86   1.04   0.00  22.56         0.    1.2724
   48   13    0  30.43   2.79   0.00  30.11         0.    1.4272
   49   14    1  22.01   1.03   0.00  21.69         0.    1.2155
   50   14    2  29.86   1.46   0.00  29.52         0.    1.3247
   51   31    3  17.74   0.02   0.00  17.46         0.    0.0066
   52   31    9  22.13   0.02   0.00  21.81         0.    0.0062
   53   32    5  19.13   0.02   0.00  18.83         0.    0.0066
   54   32    6  22.18   0.02   0.00  21.84         0.    0.0056
   55    1    8   0.12   0.10   0.00   0.12         0.    0.0452
   56    2    2   2.84   1.34   0.00   2.83         0.    1.1852
   57    3   10  19.92   0.41   0.00  19.61         0.    0.4367
   58    3    1   8.80   0.32   0.00   8.03         0.    0.2861
   59    3   11  20.11   0.49   0.00  19.81         0.    0.3673
   60    4    3  19.43   0.38   0.00  19.12         0.    0.3876
   61    4   13   8.58   0.26   0.00   7.81         0.    0.2123
   62    4    6  17.38   0.50   0.00  17.09         0.    0.3423
   63   75    7   0.02   0.01   0.00   0.02         0.    0.0163
   64   45   13   0.30   0.16   0.00   0.29         0.    0.0000
   65   46   12  16.89   0.02   0.00  16.80         0.    0.0000
   66   46   11  16.76   0.02   0.00  16.67         0.    0.0000
   67   46    2  15.84   0.02   0.00  15.75         0.    0.0000
   68   46   13  15.51   0.03   0.00  15.42         0.    0.0000
   69   46    9  17.05   0.03   0.00  16.96         0.    0.0000
   70   46   10  19.83   0.03   0.00  19.74         0.    0.0000
   71   47    7  17.40   0.01   0.00  17.31         0.    0.0000
   72   47    1  16.87   0.02   0.00  16.78         0.    0.0000
   73   47    4  16.29   0.03   0.00  16.20         0.    0.0000
   74   47   15  16.78   0.03   0.00  16.69         0.    0.0000
   75   47    5  17.91   0.03   0.00  17.82         0.    0.0000
   76   47   15  22.53   0.03   0.00  22.42         0.    0.0000
================================================================
================ TIMING STATISTICS PER NODE    ================
node# twait    ths   tcin   teig  tvect  tga   ttotal 
  1   0.00   0.07   0.38   0.00   0.00   0.66  55.70
  2   1.11   0.19   0.38   0.00   0.00   1.15  55.66
  3   0.65   0.19   0.38   0.00   0.00   0.48  55.66
  4   0.76   0.19   0.38   0.00   0.00   0.67  55.66
  5   1.11   0.19   0.38   0.00   0.00   0.55  55.66
  6   0.85   0.19   0.38   0.00   0.00   0.64  55.66
  7   0.83   0.19   0.38   0.00   0.00   0.98  55.66
  8   1.09   0.19   0.38   0.00   0.00   0.67  55.66
  9   0.99   0.19   0.38   0.00   0.00   0.49  55.66
 10   0.63   0.19   0.38   0.00   0.00   0.84  55.66
 11   0.56   0.19   0.38   0.00   0.00   0.56  55.66
 12   1.11   0.19   0.38   0.00   0.00   0.54  55.66
 13   1.11   0.19   0.38   0.00   0.00   0.65  55.66
 14   1.11   0.19   0.38   0.00   0.00   0.86  55.66
 15   0.19   0.19   0.38   0.00   0.00   0.85  55.66
 16   1.11   0.19   0.38   0.00   0.00   0.51  55.66
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                   854.2495s 
time spent in multnx:                 838.2081s 
integral transfer time:                 0.1609s 
time spent for loop construction:      12.2118s 
syncronization time in mult:           13.1781s 
total time per CI iteration:          890.6182s 
 ** parallelization degree for mult section:0.9848

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)           2.35        0.00        700.311
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)          2.34        0.00        938.920
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.07        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:            4.76        0.01        776.679
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)        4316.90        5.96        724.611
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)       4316.90        5.11        844.058
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           1.86        0.01        193.106
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:         8635.66       11.08        779.277
========================================================


          starting ci iteration   3

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x: 1142101 2x:  239374 4x:   29304
All internal counts: zz :   55360 yy: 1825441 xx: 1699543 ww: 1500549
One-external counts: yz :  391887 yx: 3708744 yw: 3487646
Two-external counts: yy :  631018 ww:  333356 xx:  483913 xz:   30921 wz:   32232 wx:  652690
Three-ext.   counts: yx : 3261530 yw: 3217932

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 579

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.94068358    -0.05416338     0.00629941     0.33487468

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1   -155.4209190628  1.3472E-03  2.0708E-04  2.5367E-02  1.0000E-03
 mr-sdci #  3  2   -152.7063355834  1.2847E-01  0.0000E+00  0.0000E+00  1.0000E-03
 mr-sdci #  3  3   -152.0949509401  1.9444E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mr-sdci #  3  4   -151.9002602715  1.4929E+01  0.0000E+00  2.2290E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   21    5  10.43   0.32   0.00  10.42         0.    0.3634
    2   22   10  24.59   0.05   0.00  24.42         0.    0.0701
    3   22   15  14.15   0.02   0.00  13.88         0.    0.0326
    4   22   12   6.15   0.01   0.00   5.87         0.    0.0137
    5   22    7   3.71   0.01   0.00   3.44         0.    0.0084
    6   22   15  18.51   0.05   0.00  18.37         0.    0.0492
    7   22    0   4.94   0.01   0.00   4.70         0.    0.0060
    8   22    8   4.82   0.01   0.00   4.55         0.    0.0060
    9   22   10  20.08   0.05   0.00  19.93         0.    0.0478
   10   22   13   4.53   0.01   0.00   4.27         0.    0.0091
   11   22    7  16.72   0.04   0.01  16.58         0.    0.0414
   12   23   11  15.91   0.03   0.00  15.77         0.    0.0383
   13   23   10   6.05   0.01   0.00   5.79         0.    0.0122
   14   23    6   4.78   0.01   0.00   4.52         0.    0.0104
   15   23   10   3.55   0.01   0.00   3.31         0.    0.0073
   16   23   14  12.80   0.03   0.00  12.69         0.    0.0302
   17   23    5   3.84   0.01   0.00   3.59         0.    0.0070
   18   23    9   3.70   0.00   0.00   3.43         0.    0.0030
   19   23    1  12.48   0.03   0.00  12.37         0.    0.0298
   20   23   15   5.01   0.01   0.00   4.75         0.    0.0052
   21   23    4  13.96   0.03   0.00  13.83         0.    0.0302
   22   24    5   1.82   1.55   0.00   1.69         0.    0.0054
   23   24   11   0.34   0.01   0.00   0.20         0.    0.0071
   24   24    7   1.85   0.01   0.00   1.71         0.    0.0052
   25   24   11   1.85   1.56   0.00   1.71         0.    0.0043
   26   25   10   0.34   0.01   0.00   0.19         0.    0.0055
   27   25    5   0.35   0.01   0.00   0.22         0.    0.0067
   28   25    7   0.33   0.01   0.00   0.17         0.    0.0045
   29   25    6   0.36   0.01   0.00   0.21         0.    0.0047
   30   26    0  33.95   0.06   0.00  33.65         0.    0.0870
   31   26    6   5.94   0.01   0.00   5.65         0.    0.0128
   32   26   12   4.51   0.01   0.00   4.25         0.    0.0106
   33   26    1   5.08   0.01   0.00   4.83         0.    0.0079
   34   26    8  14.28   0.03   0.00  14.01         0.    0.0346
   35   26   15  19.43   0.05   0.00  19.13         0.    0.0518
   36   26    0   3.73   0.01   0.00   3.49         0.    0.0071
   37   26    2   3.70   0.00   0.00   3.40         0.    0.0035
   38   26    2   5.75   0.01   0.00   5.49         0.    0.0123
   39   26    9   7.96   0.02   0.00   7.69         0.    0.0192
   40   26    5  20.65   0.06   0.00  20.32         0.    0.0574
   41   26   14   5.01   0.01   0.00   4.77         0.    0.0057
   42   26    9   3.96   0.01   0.00   3.69         0.    0.0085
   43   26   12   4.42   0.00   0.00   4.14         0.    0.0044
   44   26    3   5.64   0.01   0.00   5.36         0.    0.0121
   45   26    1  20.78   0.05   0.01  20.44         0.    0.0530
   46   11   13   2.44   2.03   0.00   2.43         0.    0.3188
   47   13   13  22.14   1.05   0.00  21.83         0.    1.2724
   48   13    7  33.67   1.53   0.00  33.36         0.    1.4272
   49   14    6  22.75   1.05   0.00  22.42         0.    1.2155
   50   14    2  30.83   1.49   0.00  30.50         0.    1.3247
   51   31    3  20.17   0.02   0.00  19.88         0.    0.0066
   52   31   12  22.41   0.02   0.00  22.08         0.    0.0062
   53   32    4  20.85   0.02   0.00  20.56         0.    0.0066
   54   32    9  20.70   0.02   0.00  20.34         0.    0.0056
   55    1   10   1.67   0.10   0.00   1.67         0.    0.0452
   56    2    4   4.40   1.34   0.00   4.39         0.    1.1852
   57    3    8  19.62   0.42   0.00  19.32         0.    0.4367
   58    3    3   9.32   0.32   0.00   6.91         0.    0.2861
   59    3   11  18.11   0.49   0.00  17.78         0.    0.3673
   60    4    4  18.11   0.39   0.00  17.79         0.    0.3876
   61    4   13   7.25   0.26   0.00   6.69         0.    0.2123
   62    4   14  19.44   0.50   0.00  19.16         0.    0.3423
   63   75    6   0.02   0.01   0.00   0.02         0.    0.0163
   64   45    3   1.87   1.71   0.00   1.86         0.    0.0000
   65   46    9  20.61   0.02   0.00  20.50         0.    0.0000
   66   46   12  19.80   0.02   0.00  19.69         0.    0.0000
   67   46    2  16.81   0.02   0.00  16.72         0.    0.0000
   68   46    0  16.13   0.03   0.00  16.05         0.    0.0000
   69   46    5  18.90   0.03   0.00  18.80         0.    0.0000
   70   46    3  19.41   0.03   0.00  19.29         0.    0.0000
   71   47    8  18.68   0.01   0.00  18.59         0.    0.0000
   72   47    1  18.61   0.02   0.00  18.52         0.    0.0000
   73   47    6  20.44   0.03   0.00  20.34         0.    0.0000
   74   47   13  19.65   0.03   0.00  19.55         0.    0.0000
   75   47   11  19.79   0.03   0.00  19.68         0.    0.0000
   76   47   14  19.86   0.03   0.00  19.74         0.    0.0000
================================================================
================ TIMING STATISTICS PER NODE    ================
node# twait    ths   tcin   teig  tvect  tga   ttotal 
  1   0.33   0.09   0.48   0.00   0.00   0.60  60.24
  2   1.83   0.21   0.48   0.00   0.00   0.56  60.24
  3   1.12   0.21   0.48   0.00   0.00   0.68  60.24
  4   2.65   0.21   0.48   0.00   0.00   2.62  60.24
  5   0.41   0.21   0.48   0.00   0.00   0.53  60.24
  6   2.64   0.21   0.48   0.00   0.00   0.66  60.24
  7   4.43   0.21   0.48   0.00   0.00   0.81  60.24
  8   2.63   0.21   0.48   0.00   0.00   0.71  60.24
  9   0.00   0.21   0.48   0.00   0.00   0.64  60.24
 10   1.11   0.21   0.48   0.00   0.00   0.89  60.24
 11   2.79   0.21   0.48   0.00   0.00   0.69  60.24
 12   2.63   0.21   0.48   0.00   0.00   0.60  60.24
 13   0.40   0.21   0.48   0.00   0.00   0.87  60.24
 14   2.38   0.21   0.48   0.00   0.00   0.86  60.24
 15   1.83   0.21   0.48   0.00   0.00   0.52  60.24
 16   1.83   0.21   0.48   0.00   0.00   0.69  60.24
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                   907.1859s 
time spent in multnx:                 889.3512s 
integral transfer time:                 0.1674s 
time spent for loop construction:      17.2659s 
syncronization time in mult:           28.9877s 
total time per CI iteration:          963.7997s 
 ** parallelization degree for mult section:0.9690

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)           0.00        0.00          0.000
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.04        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:            0.04        0.00          0.000
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)        4319.24        7.60        568.132
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)       4319.24        5.31        813.651
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           1.89        0.01        214.160
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:         8640.38       12.92        668.768
========================================================


          starting ci iteration   4

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x: 1142101 2x:  239374 4x:   29304
All internal counts: zz :   55360 yy: 1825441 xx: 1699543 ww: 1500549
One-external counts: yz :  391887 yx: 3708744 yw: 3487646
Two-external counts: yy :  631018 ww:  333356 xx:  483913 xz:   30921 wz:   32232 wx:  652690
Three-ext.   counts: yx : 3261530 yw: 3217932

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 579

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1    -0.94015536    -0.10021968    -0.09255039     0.22839193     0.21292127

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1   -155.4210935364  1.7447E-04  4.9442E-05  1.1376E-02  1.0000E-03
 mr-sdci #  4  2   -153.6545775524  9.4824E-01  0.0000E+00  0.0000E+00  1.0000E-03
 mr-sdci #  4  3   -152.2331979100  1.3825E-01  0.0000E+00  1.0627E+00  1.0000E-04
 mr-sdci #  4  4   -152.0072260388  1.0697E-01  0.0000E+00  1.7229E+00  1.0000E-04
 mr-sdci #  4  5   -151.4516985948  1.4481E+01  0.0000E+00  0.0000E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   21    5   9.08   0.30   0.00   9.07         0.    0.3634
    2   22   15  24.53   0.05   0.00  24.37         0.    0.0701
    3   22   11  14.59   0.02   0.00  14.32         0.    0.0326
    4   22   14   7.10   0.01   0.00   6.81         0.    0.0137
    5   22    6   3.77   0.01   0.00   3.49         0.    0.0084
    6   22    4  18.56   0.04   0.00  18.42         0.    0.0492
    7   22   12   3.40   0.01   0.00   3.15         0.    0.0060
    8   22    3   3.27   0.01   0.00   3.03         0.    0.0060
    9   22   12  16.50   0.05   0.00  16.36         0.    0.0478
   10   22    1   4.55   0.01   0.00   4.28         0.    0.0091
   11   22    8  15.67   0.04   0.00  15.52         0.    0.0414
   12   23   12  16.19   0.03   0.00  16.06         0.    0.0383
   13   23   15   7.02   0.01   0.00   6.76         0.    0.0122
   14   23   14   4.83   0.01   0.00   4.57         0.    0.0104
   15   23    0   3.61   0.01   0.00   3.34         0.    0.0073
   16   23    6  13.21   0.03   0.00  13.08         0.    0.0302
   17   23    2   3.91   0.01   0.00   3.66         0.    0.0070
   18   23    4   2.15   0.00   0.00   1.90         0.    0.0030
   19   23    4  11.58   0.03   0.00  11.46         0.    0.0298
   20   23    5   3.48   0.01   0.00   3.21         0.    0.0052
   21   23    9  14.71   0.03   0.00  14.56         0.    0.0302
   22   24   10   0.26   0.01   0.00   0.14         0.    0.0054
   23   24   10   0.28   0.01   0.00   0.17         0.    0.0071
   24   24    3   0.26   0.01   0.00   0.14         0.    0.0052
   25   24   12   0.25   0.01   0.00   0.13         0.    0.0043
   26   25    3   0.28   0.01   0.00   0.16         0.    0.0055
   27   25    5   0.30   0.01   0.00   0.18         0.    0.0067
   28   25   11   0.27   0.01   0.00   0.14         0.    0.0045
   29   25   12   0.28   0.01   0.00   0.15         0.    0.0047
   30   26    0  32.80   0.06   0.00  32.50         0.    0.0870
   31   26    7   6.90   0.01   0.00   6.64         0.    0.0128
   32   26   15   4.54   0.01   0.00   4.28         0.    0.0106
   33   26    0   4.64   0.01   0.00   4.38         0.    0.0079
   34   26   10  14.95   0.03   0.00  14.70         0.    0.0346
   35   26   14  21.78   0.05   0.00  21.50         0.    0.0518
   36   26    9   3.78   0.01   0.00   3.53         0.    0.0071
   37   26    8   2.15   0.00   0.00   1.88         0.    0.0035
   38   26    2   6.83   0.01   0.00   6.56         0.    0.0123
   39   26    3   8.58   0.02   0.00   8.32         0.    0.0192
   40   26    1  21.00   0.06   0.00  20.70         0.    0.0574
   41   26   13   4.57   0.01   0.00   3.24         0.    0.0057
   42   26    7   3.99   0.01   0.00   3.73         0.    0.0085
   43   26   10   2.91   0.00   0.00   2.66         0.    0.0044
   44   26    8   6.73   0.01   0.00   6.46         0.    0.0121
   45   26    3  21.60   0.05   0.00  21.29         0.    0.0530
   46   11   13   0.87   0.48   0.00   0.86         0.    0.3188
   47   13    2  23.25   1.05   0.00  22.92         0.    1.2724
   48   13    8  30.20   2.79   0.00  29.88         0.    1.4272
   49   14    4  21.97   1.03   0.00  21.65         0.    1.2155
   50   14    7  27.64   2.74   0.00  27.30         0.    1.3247
   51   31    6  18.24   0.02   0.00  17.94         0.    0.0066
   52   31   13  21.79   0.02   0.00  21.46         0.    0.0062
   53   32    9  18.92   0.02   0.00  18.61         0.    0.0066
   54   32    5  21.26   0.02   0.00  20.92         0.    0.0056
   55    1    3   0.12   0.10   0.00   0.12         0.    0.0452
   56    2   11   2.87   1.34   0.00   2.85         0.    1.1852
   57    3    6  20.39   0.41   0.00  20.14         0.    0.4367
   58    3   13   7.15   0.32   0.00   6.63         0.    0.2861
   59    3    2  20.98   1.65   0.00  20.69         0.    0.3673
   60    4   15  18.81   0.39   0.00  18.55         0.    0.3876
   61    4    1   8.17   0.26   0.00   7.62         0.    0.2123
   62    4    9  17.88   0.49   0.00  17.61         0.    0.3423
   63   75   13   0.02   0.01   0.00   0.02         0.    0.0163
   64   45    5   0.29   0.16   0.00   0.28         0.    0.0000
   65   46   10  17.66   0.02   0.00  17.55         0.    0.0000
   66   46   12  17.11   0.02   0.00  17.02         0.    0.0000
   67   46    7  16.52   0.02   0.00  16.43         0.    0.0000
   68   46    0  15.21   0.03   0.00  15.13         0.    0.0000
   69   46    5  19.28   0.03   0.00  19.19         0.    0.0000
   70   46    1  20.73   0.03   0.00  20.63         0.    0.0000
   71   47    3  19.58   0.01   0.00  19.49         0.    0.0000
   72   47   13  19.07   0.02   0.00  18.98         0.    0.0000
   73   47   11  16.75   0.03   0.00  16.65         0.    0.0000
   74   47   10  17.88   0.03   0.00  17.79         0.    0.0000
   75   47   11  19.46   0.03   0.00  19.37         0.    0.0000
   76   47   14  20.66   0.03   0.00  20.55         0.    0.0000
================================================================
================ TIMING STATISTICS PER NODE    ================
node# twait    ths   tcin   teig  tvect  tga   ttotal 
  1   0.00   0.16   0.74   0.00   0.00   0.64  57.42
  2   1.47   0.21   0.74   0.00   0.00   0.84  57.40
  3   1.19   0.21   0.74   0.00   0.00   0.80  57.40
  4   2.41   0.21   0.74   0.00   0.00   0.81  57.40
  5   1.95   0.21   0.74   0.00   0.00   0.59  57.40
  6   2.48   0.21   0.74   0.00   0.00   0.58  57.40
  7   0.54   0.21   0.74   0.00   0.00   0.68  57.40
  8   1.19   0.21   0.74   0.00   0.00   0.67  57.40
  9   1.50   0.21   0.74   0.00   0.00   0.71  57.40
 10   0.97   0.21   0.74   0.00   0.00   0.68  57.40
 11   2.29   0.21   0.74   0.00   0.00   0.66  57.40
 12   2.31   0.21   0.74   0.00   0.00   0.42  57.40
 13   2.52   0.21   0.74   0.00   0.00   0.61  57.40
 14   2.54   0.21   0.74   0.00   0.00   1.93  57.40
 15   1.20   0.21   0.74   0.00   0.00   0.65  57.40
 16   1.33   0.21   0.74   0.00   0.00   0.67  57.40
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                   872.3517s 
time spent in multnx:                 855.7832s 
integral transfer time:                 0.1586s 
time spent for loop construction:      14.6679s 
syncronization time in mult:           25.8808s 
total time per CI iteration:          918.4472s 
 ** parallelization degree for mult section:0.9712

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)           0.00        0.00          0.000
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.03        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:            0.03        0.00          0.000
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)        4319.24        6.72        642.522
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)       4319.24        5.20        829.999
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           1.90        0.01        229.899
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:         8640.39       11.93        723.983
========================================================


          starting ci iteration   5

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x: 1142101 2x:  239374 4x:   29304
All internal counts: zz :   55360 yy: 1825441 xx: 1699543 ww: 1500549
One-external counts: yz :  391887 yx: 3708744 yw: 3487646
Two-external counts: yy :  631018 ww:  333356 xx:  483913 xz:   30921 wz:   32232 wx:  652690
Three-ext.   counts: yx : 3261530 yw: 3217932

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 579

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.93969190     0.09977302    -0.06796820     0.17299171    -0.14863629    -0.22446811

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1   -155.4211435658  5.0029E-05  1.5645E-05  7.3078E-03  1.0000E-03
 mr-sdci #  5  2   -154.5069419819  8.5236E-01  0.0000E+00  0.0000E+00  1.0000E-03
 mr-sdci #  5  3   -152.3384677061  1.0527E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mr-sdci #  5  4   -152.0197793180  1.2553E-02  0.0000E+00  1.5880E+00  1.0000E-04
 mr-sdci #  5  5   -151.9553455604  5.0365E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mr-sdci #  5  6   -151.4233933655  1.4453E+01  0.0000E+00  2.5459E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   21   10  10.08   0.31   0.00  10.07         0.    0.3634
    2   22   12  25.94   0.05   0.00  25.77         0.    0.0701
    3   22    8  13.89   0.02   0.00  13.63         0.    0.0326
    4   22   11   7.79   0.01   0.00   5.74         0.    0.0137
    5   22    4   5.41   0.01   0.00   5.14         0.    0.0084
    6   22    8  18.43   0.04   0.00  18.28         0.    0.0492
    7   22    2   5.08   0.01   0.00   4.82         0.    0.0060
    8   22    8   4.96   0.01   0.00   4.68         0.    0.0060
    9   22    0  16.88   0.05   0.00  16.75         0.    0.0478
   10   22   10   4.51   0.01   0.00   4.26         0.    0.0091
   11   22    1  16.54   0.04   0.00  16.39         0.    0.0414
   12   23    5  16.56   0.03   0.00  16.43         0.    0.0383
   13   23    6   6.03   0.01   0.00   5.78         0.    0.0122
   14   23    0   4.78   0.01   0.00   4.53         0.    0.0104
   15   23    7   5.31   0.01   0.00   5.02         0.    0.0073
   16   23    2  12.78   0.03   0.00  12.66         0.    0.0302
   17   23   13   3.89   0.01   0.00   3.62         0.    0.0070
   18   23    6   3.84   0.00   0.00   3.58         0.    0.0030
   19   23    3  12.29   0.03   0.00  12.17         0.    0.0298
   20   23    3   5.14   0.01   0.00   4.86         0.    0.0052
   21   23    7  14.12   0.03   0.00  13.98         0.    0.0302
   22   24   11   0.29   0.01   0.00   0.15         0.    0.0054
   23   24    5   0.34   0.01   0.00   0.20         0.    0.0071
   24   24    5   0.29   0.01   0.00   0.16         0.    0.0052
   25   24   13   0.30   0.01   0.00   0.16         0.    0.0043
   26   25   13   0.32   0.01   0.00   0.19         0.    0.0055
   27   25   10   0.33   0.01   0.00   0.21         0.    0.0067
   28   25   10   0.29   0.01   0.00   0.15         0.    0.0045
   29   25    9   0.34   0.01   0.00   0.19         0.    0.0047
   30   26    0  35.41   0.06   0.00  35.09         0.    0.0870
   31   26   13   5.81   0.01   0.00   5.55         0.    0.0128
   32   26    5   4.51   0.01   0.00   4.26         0.    0.0106
   33   26   12   3.47   0.01   0.00   3.21         0.    0.0079
   34   26    4  14.27   0.03   0.00  13.99         0.    0.0346
   35   26    4  21.15   0.05   0.00  20.83         0.    0.0518
   36   26    1   5.43   0.01   0.00   5.16         0.    0.0071
   37   26   14   3.84   0.00   0.00   3.58         0.    0.0035
   38   26   11   5.72   0.01   0.00   5.45         0.    0.0123
   39   26   12   9.25   0.02   0.00   9.01         0.    0.0192
   40   26    2  22.78   0.06   0.00  22.43         0.    0.0574
   41   26   14   3.46   0.01   0.00   3.19         0.    0.0057
   42   26    6   3.98   0.01   0.00   3.73         0.    0.0085
   43   26   12   4.55   0.00   0.00   4.27         0.    0.0044
   44   26    9   5.62   0.01   0.00   5.32         0.    0.0121
   45   26   14  23.43   0.05   0.01  23.05         0.    0.0530
   46   11    0   0.87   0.48   0.00   0.87         0.    0.3188
   47   13    6  25.80   1.05   0.00  25.45         0.    1.2724
   48   13    1  37.82   1.53   0.00  37.48         0.    1.4272
   49   14   13  30.39   1.05   0.00  30.03         0.    1.2155
   50   14    5  36.41   1.54   0.00  36.04         0.    1.3247
   51   31   14  20.43   0.02   0.00  20.11         0.    0.0066
   52   31    8  23.21   0.02   0.00  22.85         0.    0.0062
   53   32   11  21.16   0.02   0.00  20.85         0.    0.0066
   54   32   10  23.74   0.02   0.00  23.37         0.    0.0056
   55    1    9   0.12   0.10   0.00   0.12         0.    0.0452
   56    2   15   4.51   1.34   0.00   4.50         0.    1.1852
   57    3   11  22.76   0.42   0.00  22.43         0.    0.4367
   58    3   14   8.81   0.31   0.00   6.31         0.    0.2861
   59    3    9  23.88   0.49   0.00  23.51         0.    0.3673
   60    4    2  19.24   0.39   0.00  18.96         0.    0.3876
   61    4    9   8.78   0.26   0.00   8.05         0.    0.2123
   62    4    9  19.21   0.50   0.00  18.91         0.    0.3423
   63   75   10   0.02   0.01   0.00   0.02         0.    0.0163
   64   45   11   0.30   0.16   0.00   0.29         0.    0.0000
   65   46    3  18.30   0.02   0.00  18.21         0.    0.0000
   66   46    6  21.18   0.02   0.00  21.07         0.    0.0000
   67   46   13  17.40   0.02   0.00  17.31         0.    0.0000
   68   46   15  15.92   0.03   0.00  15.83         0.    0.0000
   69   46    7  18.76   0.03   0.00  18.66         0.    0.0000
   70   46    7  21.70   0.03   0.00  21.59         0.    0.0000
   71   47   15  21.80   0.02   0.00  21.68         0.    0.0000
   72   47   15  18.27   0.02   0.00  18.17         0.    0.0000
   73   47   12  17.15   0.03   0.00  17.05         0.    0.0000
   74   47   10  18.90   0.03   0.00  18.80         0.    0.0000
   75   47    4  19.07   0.03   0.00  18.98         0.    0.0000
   76   47    3  24.16   0.03   0.00  24.04         0.    0.0000
================================================================
================ TIMING STATISTICS PER NODE    ================
node# twait    ths   tcin   teig  tvect  tga   ttotal 
  1   3.37   0.16   0.80   0.00   0.00   0.48  62.60
  2   0.00   0.23   0.80   0.00   0.00   0.51  62.58
  3   0.34   0.23   0.80   0.00   0.00   0.71  62.58
  4   0.29   0.23   0.80   0.00   0.00   0.44  62.58
  5   0.01   0.23   0.80   0.00   0.00   0.66  62.58
  6   3.19   0.23   0.80   0.00   0.00   0.70  62.58
  7   0.48   0.23   0.80   0.00   0.00   0.84  62.58
  8   0.12   0.23   0.80   0.00   0.00   0.43  62.58
  9   0.47   0.23   0.80   0.00   0.00   0.73  62.58
 10   3.33   0.23   0.80   0.00   0.00   1.26  62.58
 11   3.42   0.23   0.80   0.00   0.00   0.69  62.58
 12   3.27   0.23   0.80   0.00   0.00   0.94  62.58
 13   0.87   0.23   0.80   0.00   0.00   0.72  62.58
 14   3.18   0.23   0.80   0.00   0.00   0.88  62.58
 15   1.33   0.23   0.80   0.00   0.00   3.10  62.58
 16   0.81   0.23   0.80   0.00   0.00   0.22  62.58
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                   949.4792s 
time spent in multnx:                 929.2351s 
integral transfer time:                 0.1653s 
time spent for loop construction:      11.1045s 
syncronization time in mult:           24.4828s 
total time per CI iteration:         1001.2976s 
 ** parallelization degree for mult section:0.9749

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)           2.35        0.00        805.032
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)          2.35        0.00       1147.978
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.06        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:            4.75        0.01        904.919
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)        4316.90        7.92        545.356
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)       4316.90        5.39        801.279
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           1.87        0.01        206.845
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:         8635.67       13.31        648.699
========================================================


          starting ci iteration   6

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x: 1142101 2x:  239374 4x:   29304
All internal counts: zz :   55360 yy: 1825441 xx: 1699543 ww: 1500549
One-external counts: yz :  391887 yx: 3708744 yw: 3487646
Two-external counts: yy :  631018 ww:  333356 xx:  483913 xz:   30921 wz:   32232 wx:  652690
Three-ext.   counts: yx : 3261530 yw: 3217932

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 579

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1    -0.93969975    -0.00260777

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  6  1   -155.4211562070  1.2641E-05  5.1813E-06  3.7119E-03  1.0000E-03
 mr-sdci #  6  2   -152.7288882719 -1.7781E+00  0.0000E+00  8.0621E+00  1.0000E-03
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   21    1   9.26   0.31   0.00   9.25         0.    0.3634
    2   22    9  24.73   0.05   0.00  24.58         0.    0.0701
    3   22    7  14.14   0.02   0.00  13.87         0.    0.0326
    4   22    2   7.94   0.01   0.00   7.65         0.    0.0137
    5   22    1   3.96   0.01   0.00   3.64         0.    0.0084
    6   22    6  17.76   0.04   0.00  17.63         0.    0.0492
    7   22    9   3.53   0.01   0.00   3.27         0.    0.0060
    8   22   13   3.44   0.01   0.00   3.19         0.    0.0060
    9   22    0  16.31   0.05   0.00  16.18         0.    0.0478
   10   22   11   6.08   0.01   0.00   5.80         0.    0.0091
   11   22    4  15.92   0.04   0.00  15.78         0.    0.0414
   12   23   14  15.65   0.03   0.00  15.50         0.    0.0383
   13   23    5   7.63   0.01   0.00   7.36         0.    0.0122
   14   23    3   6.38   0.01   0.00   6.11         0.    0.0104
   15   23    2   3.87   0.01   0.00   3.55         0.    0.0073
   16   23    8  12.50   0.03   0.00  12.38         0.    0.0302
   17   23   15   5.33   0.01   0.00   5.03         0.    0.0070
   18   23   14   3.54   0.00   0.00   3.23         0.    0.0030
   19   23   15  12.42   0.03   0.00  12.29         0.    0.0298
   20   23    4   3.60   0.01   0.00   3.32         0.    0.0052
   21   23   12  13.80   0.03   0.00  13.67         0.    0.0302
   22   24   10   0.33   0.01   0.00   0.17         0.    0.0054
   23   24    4   0.38   0.01   0.00   0.23         0.    0.0071
   24   24   12   1.52   0.01   0.00   0.17         0.    0.0052
   25   24    4   0.33   0.01   0.00   0.17         0.    0.0043
   26   25   12   0.35   0.01   0.00   0.20         0.    0.0055
   27   25   10   0.42   0.01   0.00   0.26         0.    0.0067
   28   25    9   0.32   0.01   0.00   0.17         0.    0.0045
   29   25    9   0.39   0.01   0.00   0.22         0.    0.0047
   30   26    4  33.14   0.06   0.00  32.85         0.    0.0870
   31   26    9   6.09   0.01   0.00   5.80         0.    0.0128
   32   26    6   6.10   0.01   0.00   5.83         0.    0.0106
   33   26    5   4.94   0.01   0.00   4.64         0.    0.0079
   34   26   11  14.21   0.03   0.00  13.96         0.    0.0346
   35   26   15  20.07   0.05   0.00  19.82         0.    0.0518
   36   26    5   3.99   0.01   0.00   3.70         0.    0.0071
   37   26    7   3.53   0.00   0.00   3.23         0.    0.0035
   38   26   14   6.15   0.01   0.00   5.84         0.    0.0123
   39   26   13   9.26   0.02   0.00   9.02         0.    0.0192
   40   26    3  22.04   0.06   0.00  21.72         0.    0.0574
   41   26    2   4.89   0.01   0.00   4.57         0.    0.0057
   42   26    0   5.48   0.01   0.00   5.18         0.    0.0085
   43   26   12   3.02   0.00   0.00   2.76         0.    0.0044
   44   26    0   5.91   0.01   0.00   5.60         0.    0.0121
   45   26    8  21.81   0.05   0.00  21.49         0.    0.0530
   46   11    1   2.17   1.71   0.00   2.16         0.    0.3188
   47   13    5  23.35   1.04   0.00  23.03         0.    1.2724
   48   13    0  29.92   1.51   0.00  29.60         0.    1.4272
   49   14   13  22.75   1.03   0.00  22.43         0.    1.2155
   50   14   14  30.19   1.45   0.00  29.86         0.    1.3247
   51   31    7  18.45   0.02   0.00  18.18         0.    0.0066
   52   31    2  22.53   0.02   0.00  22.20         0.    0.0062
   53   32   12  18.87   0.02   0.00  18.60         0.    0.0066
   54   32    6  22.08   0.02   0.00  21.72         0.    0.0056
   55    1    4   1.34   1.32   0.00   1.34         0.    0.0452
   56    2    8   3.11   1.37   0.00   3.10         0.    1.1852
   57    3   10  19.52   0.41   0.00  19.21         0.    0.4367
   58    3    3   9.34   0.32   0.00   8.83         0.    0.2861
   59    3   15  18.96   0.49   0.00  18.65         0.    0.3673
   60    4   10  16.57   0.38   0.00  16.31         0.    0.3876
   61    4    6   9.61   0.26   0.00   7.52         0.    0.2123
   62    4   11  16.38   0.49   0.00  16.10         0.    0.3423
   63   75    9   0.03   0.01   0.00   0.02         0.    0.0163
   64   45   12   0.33   0.16   0.00   0.32         0.    0.0000
   65   46    2  17.37   0.02   0.00  17.28         0.    0.0000
   66   46   12  17.42   0.02   0.00  17.31         0.    0.0000
   67   46    5  15.24   0.02   0.00  15.15         0.    0.0000
   68   46   10  16.64   0.03   0.00  16.55         0.    0.0000
   69   46    3  17.53   0.03   0.00  17.43         0.    0.0000
   70   46   11  19.76   0.03   0.00  19.66         0.    0.0000
   71   47    7  19.14   0.01   0.00  19.03         0.    0.0000
   72   47   13  16.47   0.02   0.00  16.38         0.    0.0000
   73   47    9  17.62   0.03   0.00  17.53         0.    0.0000
   74   47    8  16.20   0.03   0.00  16.12         0.    0.0000
   75   47    1  18.34   0.03   0.00  18.26         0.    0.0000
   76   47    1  20.79   0.03   0.00  20.68         0.    0.0000
================================================================
================ TIMING STATISTICS PER NODE    ================
node# twait    ths   tcin   teig  tvect  tga   ttotal 
  1   1.13   0.08   0.26   0.00   0.00   0.73  59.12
  2   3.17   0.08   0.26   0.00   0.00   0.37  59.12
  3   0.45   0.08   0.26   0.00   0.00   0.94  59.13
  4   2.54   0.08   0.26   0.00   0.00   0.84  59.12
  5   3.29   0.08   0.26   0.00   0.00   0.72  59.12
  6   0.40   0.08   0.26   0.00   0.00   0.89  59.12
  7   2.54   0.08   0.26   0.00   0.00   1.03  59.12
  8   1.80   0.08   0.26   0.00   0.00   0.67  59.12
  9   4.59   0.08   0.26   0.00   0.00   0.37  59.12
 10   4.59   0.08   0.26   0.00   0.00   0.80  59.12
 11   4.59   0.08   0.26   0.00   0.00   0.67  59.12
 12   2.31   0.08   0.26   0.00   0.00   0.64  59.12
 13   3.14   0.08   0.26   0.00   0.00   0.75  59.12
 14   4.60   0.08   0.26   0.00   0.00   0.63  59.12
 15   1.80   0.08   0.26   0.00   0.00   0.76  59.12
 16   0.01   0.08   0.26   0.00   0.00   0.69  59.12
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                   880.4622s 
time spent in multnx:                 861.4249s 
integral transfer time:                 0.1746s 
time spent for loop construction:      13.4075s 
syncronization time in mult:           40.9130s 
total time per CI iteration:          945.9850s 
 ** parallelization degree for mult section:0.9556

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)           2.35        0.00        820.066
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)          2.34        0.00        931.083
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.05        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:            4.74        0.01        845.327
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)        4316.90        6.03        715.717
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)       4316.90        5.47        789.265
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           1.88        0.01        219.196
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:         8635.68       11.51        750.298
========================================================


          starting ci iteration   7

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x: 1142101 2x:  239374 4x:   29304
All internal counts: zz :   55360 yy: 1825441 xx: 1699543 ww: 1500549
One-external counts: yz :  391887 yx: 3708744 yw: 3487646
Two-external counts: yy :  631018 ww:  333356 xx:  483913 xz:   30921 wz:   32232 wx:  652690
Three-ext.   counts: yx : 3261530 yw: 3217932

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 579

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1    -0.93961722    -0.03078394     0.02633766

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  7  1   -155.4211625837  6.3767E-06  2.0418E-06  2.3894E-03  1.0000E-03
 mr-sdci #  7  2   -154.4292749684  1.7004E+00  0.0000E+00  1.2928E+01  1.0000E-03
 mr-sdci #  7  3   -151.6696023018 -6.6887E-01  0.0000E+00  1.1076E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   21   10  10.09   0.30   0.00  10.08         0.    0.3634
    2   22    9  25.11   0.05   0.00  24.96         0.    0.0701
    3   22    8  14.00   0.02   0.00  13.72         0.    0.0326
    4   22   13   7.91   0.01   0.00   7.65         0.    0.0137
    5   22    2   3.75   0.01   0.00   3.48         0.    0.0084
    6   22    4  17.98   0.04   0.00  17.84         0.    0.0492
    7   22    5   3.37   0.01   0.00   3.12         0.    0.0060
    8   22    0   3.24   0.01   0.00   3.00         0.    0.0060
    9   22    9  19.24   0.05   0.00  19.10         0.    0.0478
   10   22    6   4.48   0.01   0.00   4.21         0.    0.0091
   11   22    3  16.57   0.04   0.00  16.43         0.    0.0414
   12   23    0  15.76   0.03   0.00  15.63         0.    0.0383
   13   23    1   6.02   0.01   0.00   5.74         0.    0.0122
   14   23    9   4.87   0.01   0.00   4.61         0.    0.0104
   15   23   12   3.61   0.01   0.00   3.35         0.    0.0073
   16   23   12  12.77   0.03   0.00  12.63         0.    0.0302
   17   23   14   3.86   0.01   0.00   3.61         0.    0.0070
   18   23   13   2.19   0.00   0.00   1.91         0.    0.0030
   19   23    2  12.45   0.03   0.00  12.30         0.    0.0298
   20   23    9   3.46   0.01   0.00   3.19         0.    0.0052
   21   23   11  14.21   0.03   0.00  14.07         0.    0.0302
   22   24   10   0.29   0.01   0.00   0.15         0.    0.0054
   23   24    3   0.32   0.01   0.00   0.20         0.    0.0071
   24   24    8   0.27   0.01   0.00   0.15         0.    0.0052
   25   24    4   0.29   0.01   0.00   0.15         0.    0.0043
   26   25   11   0.34   0.01   0.00   0.19         0.    0.0055
   27   25    8   0.35   0.01   0.00   0.22         0.    0.0067
   28   25    8   0.29   0.01   0.00   0.16         0.    0.0045
   29   25    4   0.33   0.01   0.00   0.19         0.    0.0047
   30   26    0  34.58   0.06   0.00  34.27         0.    0.0870
   31   26    3   5.72   0.01   0.00   5.46         0.    0.0128
   32   26   13   4.48   0.01   0.00   4.22         0.    0.0106
   33   26    1   3.52   0.01   0.00   3.26         0.    0.0079
   34   26    4  14.27   0.03   0.00  13.98         0.    0.0346
   35   26   15  20.48   0.05   0.00  20.18         0.    0.0518
   36   26   10   3.78   0.01   0.00   3.54         0.    0.0071
   37   26    6   2.15   0.00   0.00   1.89         0.    0.0035
   38   26    5   5.73   0.01   0.00   5.47         0.    0.0123
   39   26   14   9.57   0.02   0.00   9.31         0.    0.0192
   40   26   14  21.72   0.06   0.01  21.40         0.    0.0574
   41   26    0   3.41   0.01   0.00   3.17         0.    0.0057
   42   26    9   3.94   0.01   0.00   3.68         0.    0.0085
   43   26    7   2.81   0.00   0.00   2.54         0.    0.0044
   44   26   15   5.62   0.01   0.00   5.35         0.    0.0121
   45   26   10  21.93   0.05   0.01  21.58         0.    0.0530
   46   11   14   0.88   0.48   0.00   0.87         0.    0.3188
   47   13    1  24.02   1.05   0.00  23.71         0.    1.2724
   48   13    3  31.68   1.51   0.00  31.36         0.    1.4272
   49   14    6  22.43   1.04   0.00  22.11         0.    1.2155
   50   14    5  27.94   1.46   0.00  27.60         0.    1.3247
   51   31   12  18.44   0.02   0.00  18.16         0.    0.0066
   52   31   13  22.57   0.02   0.00  22.24         0.    0.0062
   53   32   11  20.09   0.02   0.00  19.81         0.    0.0066
   54   32    2  21.94   0.02   0.00  21.60         0.    0.0056
   55    1   11   0.12   0.10   0.00   0.12         0.    0.0452
   56    2    1   2.83   1.34   0.00   2.83         0.    1.1852
   57    3    8  21.58   0.42   0.00  21.27         0.    0.4367
   58    3    6   8.60   0.32   0.00   8.07         0.    0.2861
   59    3   12  20.45   0.49   0.00  20.14         0.    0.3673
   60    4    6  17.75   0.39   0.00  17.47         0.    0.3876
   61    4   15   8.90   0.26   0.00   8.13         0.    0.2123
   62    4    1  19.95   0.50   0.00  19.66         0.    0.3423
   63   75    3   0.02   0.01   0.00   0.02         0.    0.0163
   64   45   15   0.30   0.16   0.00   0.29         0.    0.0000
   65   46   10  18.92   0.02   0.00  18.83         0.    0.0000
   66   46   14  19.00   0.02   0.00  18.91         0.    0.0000
   67   46    7  16.02   0.02   0.00  15.95         0.    0.0000
   68   46    2  17.40   0.03   0.00  17.31         0.    0.0000
   69   46    8  17.06   0.03   0.00  16.97         0.    0.0000
   70   46    4  20.59   0.03   0.00  20.47         0.    0.0000
   71   47   11  18.61   0.02   0.00  18.49         0.    0.0000
   72   47   13  18.18   0.02   0.00  18.09         0.    0.0000
   73   47    7  17.03   0.03   0.00  16.94         0.    0.0000
   74   47    5  20.03   0.03   0.00  19.94         0.    0.0000
   75   47   15  18.95   0.03   0.00  18.85         0.    0.0000
   76   47    7  20.98   0.03   0.00  20.87         0.    0.0000
================================================================
================ TIMING STATISTICS PER NODE    ================
node# twait    ths   tcin   teig  tvect  tga   ttotal 
  1   0.10   0.07   0.36   0.00   0.00   0.64  57.62
  2   0.42   0.16   0.36   0.00   0.00   0.80  57.62
  3   0.06   0.16   0.36   0.00   0.00   0.58  57.63
  4   2.15   0.16   0.36   0.00   0.00   0.59  57.62
  5   1.89   0.16   0.36   0.00   0.00   0.57  57.62
  6   0.00   0.16   0.36   0.00   0.00   0.65  57.62
  7   1.37   0.16   0.36   0.00   0.00   1.16  57.62
  8   0.22   0.16   0.36   0.00   0.00   0.38  57.62
  9   1.89   0.16   0.36   0.00   0.00   0.73  57.62
 10   0.35   0.16   0.36   0.00   0.00   0.75  57.62
 11   2.07   0.16   0.36   0.00   0.00   0.58  57.63
 12   2.11   0.16   0.36   0.00   0.00   0.48  57.62
 13   0.19   0.16   0.36   0.00   0.00   0.68  57.62
 14   1.60   0.16   0.36   0.00   0.00   0.84  57.62
 15   2.05   0.16   0.36   0.00   0.00   0.64  57.62
 16   1.96   0.16   0.36   0.00   0.00   0.96  57.62
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                   884.3685s 
time spent in multnx:                 868.4025s 
integral transfer time:                 0.1631s 
time spent for loop construction:      10.9673s 
syncronization time in mult:           18.4246s 
total time per CI iteration:          921.9979s 
 ** parallelization degree for mult section:0.9796

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)           0.00        0.00          0.000
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.05        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:            0.05        0.00          0.000
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)        4319.24        5.87        735.481
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)       4319.24        5.18        834.354
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           1.88        0.01        227.316
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:         8640.37       11.06        781.389
========================================================


          starting ci iteration   8

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x: 1142101 2x:  239374 4x:   29304
All internal counts: zz :   55360 yy: 1825441 xx: 1699543 ww: 1500549
One-external counts: yz :  391887 yx: 3708744 yw: 3487646
Two-external counts: yy :  631018 ww:  333356 xx:  483913 xz:   30921 wz:   32232 wx:  652690
Three-ext.   counts: yx : 3261530 yw: 3217932

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 579

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1    -0.93951574     0.05570735     0.04562789     0.03154561

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  8  1   -155.4211645558  1.9721E-06  4.8235E-07  1.3255E-03  1.0000E-03
 mr-sdci #  8  2   -154.8706395028  4.4136E-01  0.0000E+00  1.1973E+01  1.0000E-03
 mr-sdci #  8  3   -152.1117780562  4.4218E-01  0.0000E+00  4.3034E+00  1.0000E-04
 mr-sdci #  8  4   -151.6647251340 -3.5505E-01  0.0000E+00  4.0877E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   21    1   9.35   0.31   0.00   7.94         0.    0.3634
    2   22   15  24.63   0.05   0.00  24.48         0.    0.0701
    3   22   12  14.07   0.02   0.00  13.78         0.    0.0326
    4   22   14   7.63   0.01   0.00   5.90         0.    0.0137
    5   22    8   5.28   0.01   0.00   5.01         0.    0.0084
    6   22    9  18.61   0.04   0.00  18.47         0.    0.0492
    7   22   13   4.91   0.01   0.00   4.64         0.    0.0060
    8   22    4   4.77   0.01   0.00   4.49         0.    0.0060
    9   22    4  16.66   0.05   0.00  16.53         0.    0.0478
   10   22    6   6.05   0.01   0.00   5.73         0.    0.0091
   11   22   10  16.56   0.05   0.00  16.41         0.    0.0414
   12   23    4  15.81   0.02   0.00  15.69         0.    0.0383
   13   23    6   7.55   0.01   0.00   5.84         0.    0.0122
   14   23    0   4.98   0.01   0.00   4.71         0.    0.0104
   15   23    9   5.17   0.01   0.00   4.89         0.    0.0073
   16   23    2  12.62   0.03   0.00  12.49         0.    0.0302
   17   23    3   5.39   0.01   0.00   5.12         0.    0.0070
   18   23    0   3.60   0.00   0.00   3.32         0.    0.0030
   19   23    8  12.26   0.03   0.00  12.13         0.    0.0298
   20   23    2   4.97   0.01   0.00   4.66         0.    0.0052
   21   23    7  14.30   0.03   0.00  14.16         0.    0.0302
   22   24    9   0.34   0.01   0.00   0.18         0.    0.0054
   23   24    3   0.40   0.01   0.00   0.22         0.    0.0071
   24   24   11   0.32   0.01   0.00   0.17         0.    0.0052
   25   24    8   0.33   0.01   0.00   0.17         0.    0.0043
   26   25   11   0.38   0.01   0.00   0.22         0.    0.0055
   27   25   14   0.39   0.01   0.00   0.25         0.    0.0067
   28   25    1   0.33   0.01   0.00   0.18         0.    0.0045
   29   25    5   0.40   0.01   0.00   0.24         0.    0.0047
   30   26    0  33.16   0.06   0.00  32.85         0.    0.0870
   31   26   10   5.95   0.01   0.00   5.68         0.    0.0128
   32   26   14   6.08   0.01   0.00   5.77         0.    0.0106
   33   26    1   5.07   0.01   0.00   4.77         0.    0.0079
   34   26   13  14.55   0.03   0.00  14.28         0.    0.0346
   35   26   14  21.29   0.05   0.00  20.97         0.    0.0518
   36   26    5   5.30   0.01   0.00   5.05         0.    0.0071
   37   26   15   2.22   0.00   0.00   1.94         0.    0.0035
   38   26   15   6.04   0.01   0.00   5.77         0.    0.0123
   39   26    9   9.26   0.02   0.00   7.57         0.    0.0192
   40   26   12  21.24   0.06   0.00  20.95         0.    0.0574
   41   26    7   5.02   0.01   0.00   4.71         0.    0.0057
   42   26   15   5.43   0.01   0.00   5.17         0.    0.0085
   43   26   10   4.29   0.00   0.00   4.03         0.    0.0044
   44   26   11   7.25   0.01   0.00   6.95         0.    0.0121
   45   26    8  21.56   0.05   0.00  21.23         0.    0.0530
   46   11    6   0.94   0.49   0.00   0.93         0.    0.3188
   47   13    5  23.06   1.04   0.00  22.76         0.    1.2724
   48   13   11  30.54   1.50   0.00  30.21         0.    1.4272
   49   14    1  22.47   1.03   0.00  22.15         0.    1.2155
   50   14   10  30.75   1.46   0.00  30.41         0.    1.3247
   51   31    3  20.01   0.02   0.00  19.71         0.    0.0066
   52   31    9  22.13   0.02   0.00  21.80         0.    0.0062
   53   32    7  19.47   0.02   0.00  19.16         0.    0.0066
   54   32    3  21.51   0.02   0.00  21.18         0.    0.0056
   55    1   14   0.13   0.10   0.00   0.13         0.    0.0452
   56    2   12   4.36   1.35   0.00   4.35         0.    1.1852
   57    3    6  20.89   0.41   0.00  20.58         0.    0.4367
   58    3    5   8.88   0.32   0.00   6.72         0.    0.2861
   59    3    2  20.42   0.49   0.00  20.10         0.    0.3673
   60    4    1  17.92   0.39   0.00  17.65         0.    0.3876
   61    4    3   8.84   0.26   0.00   6.87         0.    0.2123
   62    4    7  17.92   2.01   0.00  17.65         0.    0.3423
   63   75    5   0.02   0.02   0.00   0.02         0.    0.0163
   64   45    2   0.31   0.16   0.00   0.30         0.    0.0000
   65   46   12  17.21   0.02   0.00  17.13         0.    0.0000
   66   46    2  18.24   0.02   0.00  18.16         0.    0.0000
   67   46    0  15.95   0.02   0.00  15.86         0.    0.0000
   68   46    5  17.06   0.03   0.00  16.97         0.    0.0000
   69   46   15  19.66   0.03   0.00  19.57         0.    0.0000
   70   46   13  19.09   0.03   0.00  18.98         0.    0.0000
   71   47   14  18.40   0.01   0.00  18.30         0.    0.0000
   72   47    8  17.26   0.02   0.00  17.16         0.    0.0000
   73   47   11  18.20   0.03   0.00  18.10         0.    0.0000
   74   47   13  18.21   0.03   0.00  18.13         0.    0.0000
   75   47    6  19.41   0.03   0.00  19.32         0.    0.0000
   76   47    4  19.65   0.03   0.00  19.54         0.    0.0000
================================================================
================ TIMING STATISTICS PER NODE    ================
node# twait    ths   tcin   teig  tvect  tga   ttotal 
  1   0.88   0.11   0.48   0.00   0.00   0.67  59.29
  2   1.59   0.20   0.48   0.00   0.00   0.73  59.28
  3   1.66   0.20   0.48   0.00   0.00   0.58  59.28
  4   1.72   0.20   0.48   0.00   0.00   2.56  59.28
  5   1.10   0.20   0.48   0.00   0.00   0.45  59.28
  6   1.75   0.20   0.48   0.00   0.00   2.45  59.28
  7   1.28   0.20   0.48   0.00   0.00   2.13  59.28
  8   1.69   0.20   0.48   0.00   0.00   0.73  59.29
  9   1.59   0.20   0.48   0.00   0.00   0.71  59.28
 10   1.58   0.20   0.48   0.00   0.00   2.23  59.28
 11   0.45   0.20   0.48   0.00   0.00   0.71  59.28
 12   1.51   0.20   0.48   0.00   0.00   0.73  59.28
 13   1.50   0.20   0.48   0.00   0.00   0.46  59.28
 14   1.77   0.20   0.48   0.00   0.00   0.53  59.28
 15   1.69   0.20   0.48   0.00   0.00   0.85  59.28
 16   0.00   0.20   0.48   0.00   0.00   0.75  59.28
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                   900.7239s 
time spent in multnx:                 875.5974s 
integral transfer time:                 0.1771s 
time spent for loop construction:      12.4892s 
syncronization time in mult:           21.7681s 
total time per CI iteration:          948.5495s 
 ** parallelization degree for mult section:0.9764

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)           0.00        0.00          0.000
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.07        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:            0.08        0.00          0.000
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)        4319.24       11.66        370.382
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)       4319.24        5.61        770.478
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           1.86        0.01        213.235
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:         8640.34       17.28        500.129
========================================================


          starting ci iteration   9

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x: 1142101 2x:  239374 4x:   29304
All internal counts: zz :   55360 yy: 1825441 xx: 1699543 ww: 1500549
One-external counts: yz :  391887 yx: 3708744 yw: 3487646
Two-external counts: yy :  631018 ww:  333356 xx:  483913 xz:   30921 wz:   32232 wx:  652690
Three-ext.   counts: yx : 3261530 yw: 3217932

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 579

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1    -0.93947183     0.06586005    -0.06998579    -0.00765356     0.02145577

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  9  1   -155.4211650171  4.6137E-07  7.1678E-08  4.5874E-04  1.0000E-03
 mr-sdci #  9  2   -154.9342091629  6.3570E-02  0.0000E+00  1.0395E+01  1.0000E-03
 mr-sdci #  9  3   -152.6186613311  5.0688E-01  0.0000E+00  1.0774E+01  1.0000E-04
 mr-sdci #  9  4   -151.9654680293  3.0074E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mr-sdci #  9  5   -151.5956150865 -3.5973E-01  0.0000E+00  6.6213E-01  1.0000E-04
 
 
 total energy in cosmo calc 
e(nroot) + repnuc=    -155.4211650171
dielectric energy =       0.0000000000
deltaediel =       0.0000000000
deltaelast =       0.0000000000
 

 mr-sdci  convergence criteria satisfied after  9 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  9  1   -155.4211650171  4.6137E-07  7.1678E-08  4.5874E-04  1.0000E-03
 mr-sdci #  9  2   -154.9342091629  6.3570E-02  0.0000E+00  1.0395E+01  1.0000E-03
 mr-sdci #  9  3   -152.6186613311  5.0688E-01  0.0000E+00  1.0774E+01  1.0000E-04
 mr-sdci #  9  4   -151.9654680293  3.0074E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mr-sdci #  9  5   -151.5956150865 -3.5973E-01  0.0000E+00  6.6213E-01  1.0000E-04

####################CIUDGINFO####################

   ci vector at position   1 energy= -155.421165017143

################END OF CIUDGINFO################

 
    1 of the   6 expansion vectors are transformed.
 nnormold( 1 )= 0.920781811503209702
 nnormold( 2 )= 0.557253620009656814E-05
 nnormold( 3 )= 0.266499892342972979E-05
 nnormold( 4 )= 0.113691481551512301E-05
 nnormold( 5 )= 0.140701175380134606E-06
 nnormnew( 1 )= 0.920897410994745758
    1 of the   5 matrix-vector products are transformed.
 nnormold( 1 )= 313.450372938242083
 nnormold( 2 )= 0.149802040160290141E-02
 nnormold( 3 )= 0.750495230595706270E-03
 nnormold( 4 )= 0.320466126514784967E-03
 nnormold( 5 )= 0.356728278169312959E-04
 nnormnew( 1 )= 313.489367916582694

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
 maximum overlap with reference  1 (overlap= 0.939471830447167311 )

information on vector: 1from unit 11 written to unit 16filename civout                                                      


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =      -155.4211650171

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7    8    9   10   11   12   13

                                          orbital     5    6    7   54   55   56   57   58   59  107  108  127  128

                                         symmetry   Ag   Ag   Ag   Bu   Bu   Bu   Bu   Bu   Bu   Au   Au   Bg   Bg 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1 -0.152928                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-           
 z*  1  1       2  0.906429                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +-      
 z*  1  1       3  0.055135                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +     - 
 z*  1  1       4 -0.053977                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-             +- 
 z*  1  1       5 -0.062085                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +-      
 z*  1  1       6 -0.103720                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +     - 
 z*  1  1       8  0.106050                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +     -    - 
 z*  1  1       9 -0.062149                        +-   +-   +-   +-   +-   +-   +-   +-   +-        +-   +-      
 z*  1  1      11  0.024740                        +-   +-   +-   +-   +-   +-   +-   +-   +-        +-        +- 
 z*  1  1      12 -0.062372                        +-   +-   +-   +-   +-   +-   +-   +-   +-             +-   +- 
 z*  1  1      55 -0.011901                        +-   +-   +-   +-   +-   +-        +-   +-   +-        +-   +- 
 y   1  1     581 -0.013398              2( Au )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -           
 y   1  1     652  0.012667              1( Bg )   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +     -      
 y   1  1     707  0.010168              2( Au )   +-   +-   +-   +-   +-   +-   +-   +-   +-    -        +     - 
 y   1  1     742 -0.012231              1( Bg )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -    -      
 y   1  1     743  0.014563              2( Bg )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -    -      
 y   1  1     779  0.011784              2( Au )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +          -    - 
 y   1  1     780 -0.010636              3( Au )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +          -    - 
 y   1  1     833  0.014635              2( Au )   +-   +-   +-   +-   +-   +-   +-   +-   +-         -   +-      
 y   1  1     988 -0.013926              2( Ag )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +     -      
 y   1  1     990 -0.010023              4( Ag )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +     -      
 y   1  1     991 -0.013242              5( Ag )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +     -      
 y   1  1     995  0.010362              9( Ag )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +     -      
 y   1  1    2976 -0.010700              1( Ag )   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +     -      
 y   1  1    3124  0.012422             10( Bu )   +-   +-   +-   +-   +-   +-   +-    -   +-   +-        +     - 
 y   1  1    3126  0.013191             12( Bu )   +-   +-   +-   +-   +-   +-   +-    -   +-   +-        +     - 
 y   1  1    3305  0.011043              5( Bu )   +-   +-   +-   +-   +-   +-   +-    -   +-   +     -   +-      
 y   1  1    3310  0.011977             10( Bu )   +-   +-   +-   +-   +-   +-   +-    -   +-   +     -   +-      
 y   1  1    3312  0.013661             12( Bu )   +-   +-   +-   +-   +-   +-   +-    -   +-   +     -   +-      
 y   1  1    3500  0.010171             12( Ag )   +-   +-   +-   +-   +-   +-   +-    -   +-   +         +-    - 
 y   1  1   10887 -0.012258              7( Bu )   +-   +-   +-   +-   +-   +-   +    +-   +-   +-         -    - 
 y   1  1   11213  0.014749              6( Ag )   +-   +-   +-   +-   +-   +-   +    +-   +-    -        +-    - 
 y   1  1   16499  0.010148             12( Bu )   +-   +-   +-   +-   +-    -   +-   +-   +-   +-        +     - 
 y   1  1   16685  0.010311             12( Bu )   +-   +-   +-   +-   +-    -   +-   +-   +-   +     -   +-      
 y   1  1   22808  0.013386              7( Ag )   +-   +-   +-   +-   +-   +    +-   +-   +-   +-    -    -      
 y   1  1  105594  0.013802              5( Bu )   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +     -      
 y   1  1  105734 -0.011346              5( Ag )   +-   +-    -   +-   +-   +-   +-   +-   +-   +-        +     - 
 y   1  1  105920 -0.013074              5( Ag )   +-   +-    -   +-   +-   +-   +-   +-   +-   +     -   +-      
 y   1  1  106105  0.010952              6( Bu )   +-   +-    -   +-   +-   +-   +-   +-   +-   +         +-    - 
 y   1  1  106112  0.011253             13( Bu )   +-   +-    -   +-   +-   +-   +-   +-   +-   +         +-    - 
 y   1  1  161030  0.011438              5( Bu )   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +     -      
 y   1  1  161031 -0.012624              6( Bu )   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +     -      
 y   1  1  161035  0.016452             10( Bu )   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +     -      
 y   1  1  161037  0.018056             12( Bu )   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +     -      
 y   1  1  161172 -0.010226              7( Ag )   +-    -   +-   +-   +-   +-   +-   +-   +-   +-        +     - 
 y   1  1  161176 -0.011131             11( Ag )   +-    -   +-   +-   +-   +-   +-   +-   +-   +-        +     - 
 y   1  1  161177 -0.012393             12( Ag )   +-    -   +-   +-   +-   +-   +-   +-   +-   +-        +     - 
 y   1  1  161363 -0.014255             12( Ag )   +-    -   +-   +-   +-   +-   +-   +-   +-   +     -   +-      
 y   1  1  161367 -0.010184             16( Ag )   +-    -   +-   +-   +-   +-   +-   +-   +-   +     -   +-      
 y   1  1  161547 -0.012057             12( Bu )   +-    -   +-   +-   +-   +-   +-   +-   +-   +         +-    - 
 y   1  1  228636  0.010989              9( Ag )    -   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +-      
 y   1  1  228817 -0.010935              6( Bu )    -   +-   +-   +-   +-   +-   +-   +-   +-   +         +-    - 
 y   1  1  228824 -0.011377             13( Bu )    -   +-   +-   +-   +-   +-   +-   +-   +-   +         +-    - 
 x   1  116293936  0.013936    7( Bu )   3( Au )   +-    -    -   +-   +-   +-   +    +-   +-    -   +-   +-      
 x   1  116302303 -0.010962    6( Ag )   3( Au )   +-    -    -   +-   +-   +-   +    +-   +-    -        +-   +- 
 w   1  120291700 -0.010476    7( Bu )   7( Bu )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-                
 w   1  120292972 -0.010550    1( Bg )   1( Bg )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-                
 w   1  120295975  0.017675    6( Ag )   7( Bu )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +          -      
 w   1  120297856  0.015650    1( Au )   1( Bg )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +          -      
 w   1  120308209 -0.013489    6( Ag )   6( Ag )   +-   +-   +-   +-   +-   +-   +-   +-   +-             +-      
 w   1  120310398 -0.010125    1( Au )   1( Au )   +-   +-   +-   +-   +-   +-   +-   +-   +-             +-      
 w   1  120563645 -0.010121    5( Bu )   5( Bu )   +-   +-   +-   +-   +-   +-   +-        +-   +-        +-      
 w   1  131522417 -0.011904    5( Bu )   5( Bu )   +-   +-   +    +-    -   +    +-   +-   +-        +-   +-    - 
 w   1  135126477 -0.010157    5( Bu )   9( Bu )   +-   +    +-   +-        +-   +-   +-   +-   +-   +-    -      

 ci coefficient statistics:
           rq > 0.1                4
      0.1> rq > 0.01              60
     0.01> rq > 0.001          16782
    0.001> rq > 0.0001        248836
   0.0001> rq > 0.00001      1364439
  0.00001> rq > 0.000001     4153784
 0.000001> rq               34698021
           all              40481926
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:   21947 2x:       0 4x:       0
All internal counts: zz :   55360 yy:       0 xx:       0 ww:       0
One-external counts: yz :       0 yx:       0 yw:       0
Two-external counts: yy :       0 ww:       0 xx:       0 xz:       0 wz:       0 wx:       0
Three-ext.   counts: yx :       0 yw:       0

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


  iref  icsf         v(icsf)             hv(icsf)
     1     1     -0.152928294659     23.718048379942
     2     2      0.906428909753   -140.503408433047
     3     3      0.055135060213     -8.549989664753
     4     4     -0.053976826438      8.374564138469
     5     5     -0.062084830569      9.625853489394
     6     6     -0.103720180108     16.094752481621
     7     7      0.004859164667     -0.754136511888
     8     8      0.106049864442    -16.453680303023
     9     9     -0.062149254488      9.641271350796
    10    10     -0.004841922552      0.752061195739
    11    11      0.024740344287     -3.841815177258
    12    12     -0.062372478750      9.679449709164
    13    13     -0.005413651267      0.838732018243
    14    14     -0.000356891389      0.054864259792
    15    15      0.001049440124     -0.162724279475
    16    16     -0.003994965224      0.618601082996
    17    17      0.000350695454     -0.054245684682
    18    18      0.000616189049     -0.095535473658
    19    19      0.000083663370     -0.012642750008
    20    20      0.000014870453     -0.002270076020
    21    21     -0.000190516974      0.029486901932
    22    22      0.001129082737     -0.174629340774
    23    23     -0.000069714577      0.010610166899
    24    24     -0.000077811078      0.012029273955
    25    25     -0.000198384542      0.030710295790
    26    26      0.000096560018     -0.014907077886
    27    27     -0.004973376942      0.770600677118
    28    28     -0.000212559326      0.032384754566
    29    29      0.000937926478     -0.145472808225
    30    30     -0.003783770499      0.585994085256
    31    31      0.000475031143     -0.073533925998
    32    32      0.000572596672     -0.088785912967
    33    33      0.000046046425     -0.007137741320
    34    34      0.000152900455     -0.023794221121
    35    35     -0.000411495691      0.063394790133
    36    36     -0.000775740442      0.120399182721
    37    37      0.004313207000     -0.669045627967
    38    38      0.000032278955     -0.005429297442
    39    39     -0.000302077264      0.046908559275
    40    40      0.000113957244     -0.017669038106
    41    41      0.000001349783     -0.000244995474
    42    42      0.002879311304     -0.446616134817
    43    43     -0.000401310308      0.062279639722
    44    44     -0.000605077332      0.093952885341
    45    45      0.002580746512     -0.400461676112
    46    46     -0.000396481110      0.061631699322
    47    47     -0.000368328396      0.057183398141
    48    48     -0.000027949152      0.004337572611
    49    49     -0.000357969656      0.055424352600
    50    50      0.000058077584     -0.009000998992
    51    51      0.000029780692     -0.004628081757
    52    52     -0.005358159412      0.830229618911
    53    53      0.000460670454     -0.071680057105
    54    54      0.002421781450     -0.375712468012
    55    55     -0.011901351912      1.845121144421
    56    56      0.000545774570     -0.084201027182
    57    57      0.001151360953     -0.178620629058
    58    58      0.000094149216     -0.014610914991
    59    59     -0.000022341047      0.003465974577
    60    60      0.000082269760     -0.012769061022
    61    61     -0.001249724790      0.193628618781
    62    62      0.000032561544     -0.005109189694
    63    63      0.000623456396     -0.096678117807
    64    64     -0.003085387140      0.478060736067
    65    65      0.000196702398     -0.030447358686
    66    66      0.000286768487     -0.044460543596
    67    67     -0.000175946783      0.027283417678
    68    68      0.000067357454     -0.010400016562
    69    69      0.005846288431     -0.906651116143
    70    70      0.000887185002     -0.137551376240
    71    71     -0.000486715228      0.075457587535
    72    72      0.000763063012     -0.118158674203
    73    73     -0.000486400857      0.075751897384
    74    74     -0.000422516555      0.065598968229
    75    75     -0.000029490694      0.004577769633
    76    76     -0.000109976829      0.017375644827
    77    77     -0.000049544862      0.007590627282
    78    78     -0.000017799323      0.002760734160
    79    79     -0.005138474140      0.797016007080
    80    80     -0.001631043825      0.253056868265
    81    81     -0.000543214303      0.084307657641
    82    82      0.004708048791     -0.729866153765
    83    83      0.000613927958     -0.095874167009
    84    84      0.000025669226     -0.004029772391
    85    85      0.000000270983     -0.000045545947
    86    86     -0.000011294766      0.001756472913
    87    87     -0.000018801401      0.002915455097
    88    88      0.000908786972     -0.141473247157
    89    89     -0.000228477283      0.035603886525
    90    90      0.000038604972     -0.005992312284
    91    91     -0.000053018872      0.008230322866
    92    92      0.000088285485     -0.013708352475
    93    93     -0.005173769818      0.801110475218
    94    94      0.000570537421     -0.089006441379
    95    95      0.001297348578     -0.201216775378
    96    96     -0.004798583914      0.743271315652
    97    97     -0.000040857276      0.006735096400
    98    98      0.000629316675     -0.097524720224
    99    99      0.000047675718     -0.007385161274
   100   100      0.000008101343     -0.001255913546
   101   101      0.000039921562     -0.006185537747
   102   102     -0.000037659575      0.005836423963
   103   103     -0.000043993884      0.006815672253
   104   104      0.000121511618     -0.018827294466
   105   105     -0.000001508007      0.000263566637
   106   106     -0.000003117526      0.000477367056
   107   107      0.000001089120     -0.000170247832
   108   108     -0.000005343196      0.000833586623
   109   109     -0.000000870438      0.000101720683
   110   110      0.000000620218     -0.000097688723
   111   111      0.000003109605     -0.000499063668
   112   112     -0.000001881541      0.000289090394
   113   113      0.000164647782     -0.025649433879
   114   114      0.000019592137     -0.003099773170
   115   115     -0.000031026173      0.004836262946
   116   116      0.000124969388     -0.019479900498
   117   117     -0.000018283249      0.002871720207
   118   118     -0.000019553057      0.003052952840
   119   119     -0.000001466480      0.000228692304
   120   120     -0.000008107714      0.001293997788
   121   121      0.000001483229     -0.000234320401
   122   122      0.000000106336     -0.000016362361
   123   123     -0.000249650248      0.038746308409
   124   124     -0.000030426863      0.004775602635
   125   125      0.000049913327     -0.007765855824
   126   126     -0.000204867952      0.031930634651
   127   127      0.000028884375     -0.004522015612
   128   128      0.000030894403     -0.004812297850
   129   129      0.000002289836     -0.000356917595
   130   130     -0.000000667559      0.000104228845
   131   131      0.000001526055     -0.000237368345
   132   132      0.000009207858     -0.001459955820
   133   133     -0.000002493598      0.000403573142
   134   134      0.000000736772     -0.000114686089
   135   135     -0.000000923674      0.000143829335
   136   136      0.000001999479     -0.000312506876
   137   137     -0.000438669827      0.067858218831
   138   138     -0.000061150738      0.009440108767
   139   139      0.000053839179     -0.008336499947
   140   140     -0.000139011495      0.021443771007
   141   141      0.000028401783     -0.004403258101
   142   142      0.000037710753     -0.005832771290
   143   143      0.000002729141     -0.000422049906
   144   144     -0.000000141674      0.000021940125
   145   145      0.000001760549     -0.000271023915
   146   146     -0.000002623193      0.000406648806
   147   147     -0.000000567422      0.000084826243
   148   148      0.000006295347     -0.000972571706
   149   149     -0.000026811568      0.004220464400
   150   150     -0.000000512336      0.000054474419
   151   151     -0.000000609278      0.000094770705
   152   152      0.000000951953     -0.000148186715
   153   153     -0.000001507758      0.000234323160
   154   154      0.000001089087     -0.000168170853
   155   155      0.000000524652     -0.000082467573
   156   156     -0.000003121902      0.000483749923
   157   157     -0.000214172040      0.033316771986
   158   158     -0.000029275356      0.004545395793
   159   159      0.000033047266     -0.005146871104
   160   160     -0.000114055444      0.017757006801
   161   161      0.000019161142     -0.002988504788
   162   162      0.000021964429     -0.003421093513
   163   163      0.000001606026     -0.000250306488
   164   164     -0.000000281846      0.000044052089
   165   165      0.000001437232     -0.000223715850
   166   166     -0.000001011556      0.000157841104
   167   167     -0.000000762637      0.000117952221
   168   168      0.000003317683     -0.000516528955
   169   169      0.000000863209     -0.000134436072
   170   170     -0.000000918406      0.000143063694
   171   171     -0.000000409199      0.000063912872
   172   172      0.000001585082     -0.000245777383

 number of reference csfs (nref) is   172.  root number (iroot) is  1.

 pople ci energy extrapolation is computed with 22 correlated electrons.

 eref      =   -155.016115522359   "relaxed" cnot**2         =   0.885680458693
 eci       =   -155.421165017143   deltae = eci - eref       =  -0.405049494784
 eci+dv1   =   -155.467470089593   dv1 = (1-cnot**2)*deltae  =  -0.046305072450
 eci+dv2   =   -155.473446934392   dv2 = dv1 / cnot**2       =  -0.052281917249
 eci+dv3   =   -155.481195375703   dv3 = dv1 / (2*cnot**2-1) =  -0.060030358561
 eci+pople =   -155.474214955429   ( 22e- scaled deltae )    =  -0.458099433070
NO coefficients and occupation numbers are written to nocoef_ci.1
 entering drivercid: firstcall,firstnonref= T F

################################################################################
 one electron density matrix calculation 
################################################################################


 workspace allocation information: mxcore=66347442
 
 space required:
 
    space required for calls in multd2:
       onex         2491
       allin           0
       diagon      12755
    max.      ---------
       maxnex      12755
 
    total core space usage:
       maxnex      12755
    max k-seg    3545552
    max k-ind       9770
              ---------
       totmax    7123399
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=   11294  DYX=   94227  DYW=   92190
   D0Z=    2871  D0Y=   73074  D0X=   79328  D0W=   74148
  DDZI=    4712 DDYI=   82266 DDXI=   79200 DDWI=   77908
  DDZE=       0 DDYE=    9770 DDXE=    9854 DDWE=    9680
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   2.0000000      2.0000000      2.0000000      2.0000000      1.9755128
   1.9727755      1.9716629      0.0126313      0.0116802      0.0103724
   0.0093592      0.0064691      0.0051549      0.0043903      0.0025360
   0.0021050      0.0015828      0.0011154      0.0009764      0.0009104
   0.0007144      0.0005608      0.0004528      0.0004429      0.0004191
   0.0003277      0.0002778      0.0002514      0.0002325      0.0001552
   0.0001382      0.0001158      0.0001078      0.0000950      0.0000845
   0.0000782      0.0000696      0.0000640      0.0000597      0.0000536
   0.0000496      0.0000471      0.0000442      0.0000353      0.0000300
   0.0000247      0.0000199      0.0000174      0.0000162      0.0000100
   0.0000093      0.0000082      0.0000047


*****   symmetry block SYM2   *****

 occupation numbers of nos
   1.9995656      1.9995609      1.9848216      1.9779477      1.9753271
   1.9727614      0.0142492      0.0124417      0.0105669      0.0093324
   0.0087768      0.0066800      0.0051695      0.0039798      0.0023056
   0.0017022      0.0011494      0.0009668      0.0009244      0.0007284
   0.0006642      0.0005481      0.0004675      0.0004347      0.0003917
   0.0002773      0.0002365      0.0001837      0.0001709      0.0001370
   0.0001220      0.0001096      0.0000868      0.0000799      0.0000724
   0.0000688      0.0000633      0.0000617      0.0000594      0.0000580
   0.0000508      0.0000467      0.0000351      0.0000331      0.0000264
   0.0000217      0.0000179      0.0000158      0.0000130      0.0000091
   0.0000081      0.0000048      0.0000044


*****   symmetry block SYM3   *****

 occupation numbers of nos
   1.9287695      0.1020400      0.0061787      0.0053715      0.0033128
   0.0027495      0.0019971      0.0006941      0.0005197      0.0004517
   0.0003348      0.0002651      0.0002132      0.0001690      0.0001100
   0.0000863      0.0000655      0.0000328      0.0000276      0.0000225


*****   symmetry block SYM4   *****

 occupation numbers of nos
   1.8845127      0.0546014      0.0060503      0.0043645      0.0031997
   0.0021690      0.0009333      0.0006390      0.0005633      0.0004495
   0.0003322      0.0002613      0.0002116      0.0001724      0.0001144
   0.0000816      0.0000647      0.0000315      0.0000251      0.0000214


 total number of electrons =   30.0000000000



          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        Ag  partial gross atomic populations
   ao class       1Ag        2Ag        3Ag        4Ag        5Ag        6Ag 
    C1_ s       0.828078   0.013791   1.984577   0.742037   1.006843   0.246309
    C2_ s       0.682266   1.985503   0.013847   1.111303   0.105841   0.876830
    H1_ s       0.165622  -0.000035   0.000716   0.037003   0.491248   0.137947
    H2_ s       0.251377  -0.000068   0.000822   0.031652   0.343123  -0.003048
    H3_ s       0.072658   0.000809   0.000038   0.078005   0.028457   0.714737
 
   ao class       7Ag        8Ag        9Ag       10Ag       11Ag       12Ag 
    C1_ s       0.594610   0.003171   0.006331   0.003079   0.004214   0.002631
    C2_ s       1.088994   0.005761   0.000851   0.005753   0.001046   0.002806
    H1_ s       0.019121   0.000235   0.003551   0.000125   0.001152   0.000374
    H2_ s       0.234530   0.000770   0.000783   0.000102   0.002508   0.000295
    H3_ s       0.034407   0.002694   0.000164   0.001313   0.000439   0.000363
 
   ao class      13Ag       14Ag       15Ag       16Ag       17Ag       18Ag 
    C1_ s       0.001308   0.003213   0.000628   0.000559   0.000635   0.000391
    C2_ s       0.003155   0.000138   0.001329   0.000726   0.000582   0.000479
    H1_ s       0.000130   0.000378   0.000194   0.000245   0.000121   0.000028
    H2_ s       0.000126   0.000638   0.000021   0.000057   0.000138   0.000157
    H3_ s       0.000435   0.000023   0.000364   0.000519   0.000107   0.000059
 
   ao class      19Ag       20Ag       21Ag       22Ag       23Ag       24Ag 
    C1_ s       0.000306   0.000294   0.000165   0.000146   0.000206   0.000268
    C2_ s       0.000248   0.000365   0.000404   0.000285   0.000122   0.000066
    H1_ s       0.000148   0.000168   0.000061   0.000069   0.000055   0.000025
    H2_ s       0.000241   0.000059   0.000075  -0.000001   0.000056   0.000050
    H3_ s       0.000034   0.000026   0.000010   0.000062   0.000014   0.000034
 
   ao class      25Ag       26Ag       27Ag       28Ag       29Ag       30Ag 
    C1_ s       0.000109   0.000088   0.000099   0.000114   0.000136   0.000046
    C2_ s       0.000226   0.000158   0.000121   0.000071   0.000049   0.000084
    H1_ s       0.000031   0.000017   0.000026   0.000000   0.000018   0.000004
    H2_ s       0.000011   0.000016  -0.000004   0.000015   0.000013   0.000014
    H3_ s       0.000042   0.000050   0.000037   0.000051   0.000017   0.000007
 
   ao class      31Ag       32Ag       33Ag       34Ag       35Ag       36Ag 
    C1_ s       0.000033   0.000038   0.000032   0.000014   0.000018   0.000019
    C2_ s       0.000069   0.000059   0.000037   0.000038   0.000036   0.000019
    H1_ s       0.000015   0.000004   0.000018   0.000024   0.000003   0.000005
    H2_ s       0.000011   0.000009   0.000009   0.000005   0.000016   0.000032
    H3_ s       0.000010   0.000006   0.000012   0.000014   0.000012   0.000004
 
   ao class      37Ag       38Ag       39Ag       40Ag       41Ag       42Ag 
    C1_ s       0.000000   0.000013   0.000013   0.000012   0.000019   0.000000
    C2_ s       0.000017   0.000019   0.000015   0.000004   0.000002  -0.000004
    H1_ s       0.000020   0.000013   0.000004   0.000025   0.000011   0.000009
    H2_ s       0.000030   0.000014   0.000017   0.000008   0.000007   0.000001
    H3_ s       0.000003   0.000006   0.000011   0.000005   0.000010   0.000042
 
   ao class      43Ag       44Ag       45Ag       46Ag       47Ag       48Ag 
    C1_ s       0.000009   0.000008   0.000007   0.000007   0.000002   0.000006
    C2_ s       0.000007   0.000007   0.000006   0.000007   0.000001   0.000002
    H1_ s       0.000013   0.000003   0.000010   0.000001   0.000005   0.000002
    H2_ s       0.000010   0.000007   0.000004   0.000008   0.000006   0.000001
    H3_ s       0.000005   0.000009   0.000004   0.000002   0.000006   0.000005
 
   ao class      49Ag       50Ag       51Ag       52Ag       53Ag 
    C1_ s       0.000001   0.000000   0.000004   0.000000   0.000001
    C2_ s       0.000003   0.000001  -0.000001   0.000000   0.000000
    H1_ s       0.000002   0.000002   0.000003   0.000003   0.000002
    H2_ s       0.000005   0.000001   0.000002   0.000005   0.000001
    H3_ s       0.000005   0.000006   0.000001   0.000000   0.000001

                        Bu  partial gross atomic populations
   ao class       1Bu        2Bu        3Bu        4Bu        5Bu        6Bu 
    C1_ s       0.293466   1.705667   1.182418   0.348886   1.018369   0.709884
    C2_ s       1.705455   0.292800   0.410929   0.914423   0.294436   0.620122
    H1_ s       0.000057   0.000553   0.138222   0.211115   0.085242   0.397021
    H2_ s       0.000078   0.000501   0.224842   0.002452   0.575091   0.014504
    H3_ s       0.000511   0.000040   0.028410   0.501072   0.002189   0.231230
 
   ao class       7Bu        8Bu        9Bu       10Bu       11Bu       12Bu 
    C1_ s       0.003549   0.005191   0.002574   0.003710   0.003147   0.002073
    C2_ s       0.005711   0.006184   0.003991   0.003227   0.002340   0.003685
    H1_ s       0.001722   0.000244   0.001268   0.000520   0.001332   0.000302
    H2_ s       0.000624   0.000457   0.000223   0.001669   0.001440   0.000240
    H3_ s       0.002643   0.000365   0.002511   0.000207   0.000519   0.000380
 
   ao class      13Bu       14Bu       15Bu       16Bu       17Bu       18Bu 
    C1_ s       0.002367   0.002649   0.000785   0.000480   0.000396   0.000210
    C2_ s       0.001834   0.000464   0.000965   0.000723   0.000385   0.000478
    H1_ s       0.000143   0.000538   0.000124   0.000071   0.000230   0.000018
    H2_ s       0.000200   0.000317   0.000229   0.000037   0.000121   0.000103
    H3_ s       0.000625   0.000011   0.000204   0.000391   0.000017   0.000158
 
   ao class      19Bu       20Bu       21Bu       22Bu       23Bu       24Bu 
    C1_ s       0.000415   0.000374   0.000232   0.000090   0.000113   0.000248
    C2_ s      -0.000026   0.000194   0.000257   0.000243   0.000190   0.000037
    H1_ s       0.000285   0.000048   0.000054   0.000010   0.000014   0.000093
    H2_ s       0.000247   0.000030   0.000064   0.000074   0.000045   0.000053
    H3_ s       0.000003   0.000082   0.000058   0.000130   0.000106   0.000002
 
   ao class      25Bu       26Bu       27Bu       28Bu       29Bu       30Bu 
    C1_ s       0.000192   0.000178   0.000133   0.000096   0.000036   0.000062
    C2_ s       0.000142   0.000065   0.000064   0.000060   0.000087   0.000072
    H1_ s       0.000011   0.000030   0.000003   0.000013   0.000027  -0.000004
    H2_ s      -0.000003   0.000001   0.000020   0.000007   0.000018   0.000006
    H3_ s       0.000050   0.000003   0.000017   0.000007   0.000003   0.000000
 
   ao class      31Bu       32Bu       33Bu       34Bu       35Bu       36Bu 
    C1_ s       0.000005   0.000027   0.000040  -0.000004   0.000007   0.000027
    C2_ s       0.000109   0.000035   0.000014   0.000046   0.000020   0.000017
    H1_ s       0.000002   0.000015   0.000007   0.000018   0.000028   0.000006
    H2_ s       0.000005   0.000005   0.000015   0.000012   0.000012   0.000017
    H3_ s       0.000001   0.000028   0.000011   0.000009   0.000005   0.000002
 
   ao class      37Bu       38Bu       39Bu       40Bu       41Bu       42Bu 
    C1_ s       0.000002   0.000017   0.000009   0.000012   0.000006   0.000004
    C2_ s       0.000011   0.000021   0.000015   0.000009   0.000012   0.000003
    H1_ s       0.000008   0.000010   0.000017   0.000005   0.000009   0.000019
    H2_ s       0.000010   0.000005   0.000012   0.000009   0.000019   0.000008
    H3_ s       0.000033   0.000009   0.000006   0.000024   0.000004   0.000012
 
   ao class      43Bu       44Bu       45Bu       46Bu       47Bu       48Bu 
    C1_ s       0.000004   0.000008   0.000004   0.000001   0.000003   0.000003
    C2_ s       0.000013   0.000007   0.000007   0.000001   0.000005   0.000002
    H1_ s       0.000009   0.000002   0.000004   0.000010   0.000003   0.000003
    H2_ s       0.000003   0.000015   0.000006   0.000006   0.000004   0.000003
    H3_ s       0.000006   0.000001   0.000006   0.000004   0.000003   0.000005
 
   ao class      49Bu       50Bu       51Bu       52Bu       53Bu 
    C1_ s      -0.000001   0.000001   0.000002   0.000001   0.000002
    C2_ s       0.000008   0.000000  -0.000001   0.000000   0.000001
    H1_ s       0.000001   0.000002   0.000003   0.000002   0.000000
    H2_ s       0.000001   0.000002   0.000003   0.000001   0.000001
    H3_ s       0.000005   0.000005   0.000000   0.000001   0.000001

                        Au  partial gross atomic populations
   ao class       1Au        2Au        3Au        4Au        5Au        6Au 
    C1_ s       0.682461   0.064908   0.002638   0.002181   0.001429   0.001049
    C2_ s       1.220617   0.035560   0.003217   0.002555   0.001255   0.001487
    H1_ s       0.006993   0.000564   0.000291   0.000047   0.000139   0.000114
    H2_ s       0.005461   0.000645   0.000017   0.000206   0.000292   0.000000
    H3_ s       0.013238   0.000364   0.000016   0.000381   0.000197   0.000100
 
   ao class       7Au        8Au        9Au       10Au       11Au       12Au 
    C1_ s       0.000976   0.000312   0.000052   0.000107   0.000053   0.000106
    C2_ s       0.000733   0.000277   0.000063   0.000032   0.000104   0.000153
    H1_ s       0.000143   0.000024   0.000005   0.000302   0.000003   0.000004
    H2_ s       0.000038   0.000004   0.000270   0.000001   0.000088   0.000000
    H3_ s       0.000108   0.000078   0.000130   0.000010   0.000088   0.000002
 
   ao class      13Au       14Au       15Au       16Au       17Au       18Au 
    C1_ s       0.000139   0.000086   0.000084   0.000033   0.000008   0.000001
    C2_ s       0.000069   0.000082   0.000020   0.000044   0.000035   0.000001
    H1_ s       0.000004   0.000000   0.000001   0.000002   0.000006   0.000010
    H2_ s       0.000000  -0.000001   0.000003   0.000004   0.000001   0.000012
    H3_ s       0.000001   0.000001   0.000001   0.000002   0.000016   0.000010
 
   ao class      19Au       20Au 
    C1_ s       0.000001   0.000001
    C2_ s       0.000002   0.000003
    H1_ s       0.000017   0.000000
    H2_ s       0.000005   0.000009
    H3_ s       0.000002   0.000010

                        Bg  partial gross atomic populations
   ao class       1Bg        2Bg        3Bg        4Bg        5Bg        6Bg 
    C1_ s       1.220459   0.018515   0.002826   0.003437   0.001647   0.000809
    C2_ s       0.632014   0.035167   0.002636   0.000334   0.001195   0.000998
    H1_ s       0.012013   0.000230   0.000008   0.000543   0.000059   0.000004
    H2_ s       0.013711   0.000175   0.000280   0.000053   0.000089   0.000186
    H3_ s       0.006315   0.000514   0.000300  -0.000003   0.000210   0.000171
 
   ao class       7Bg        8Bg        9Bg       10Bg       11Bg       12Bg 
    C1_ s       0.000056   0.000170   0.000107   0.000102   0.000125   0.000109
    C2_ s       0.000696   0.000288   0.000135   0.000064   0.000042   0.000147
    H1_ s       0.000171   0.000005   0.000017   0.000171   0.000051   0.000001
    H2_ s      -0.000001   0.000060   0.000099   0.000098   0.000066   0.000001
    H3_ s       0.000012   0.000116   0.000206   0.000015   0.000048   0.000004
 
   ao class      13Bg       14Bg       15Bg       16Bg       17Bg       18Bg 
    C1_ s       0.000135   0.000059   0.000047   0.000049   0.000011   0.000000
    C2_ s       0.000056   0.000106   0.000063   0.000025   0.000037   0.000000
    H1_ s       0.000005   0.000001   0.000001   0.000002   0.000012   0.000000
    H2_ s       0.000015   0.000001   0.000002   0.000001   0.000005   0.000014
    H3_ s       0.000000   0.000006   0.000001   0.000004   0.000001   0.000017
 
   ao class      19Bg       20Bg 
    C1_ s       0.000001   0.000002
    C2_ s       0.000001   0.000001
    H1_ s       0.000020   0.000001
    H2_ s       0.000001   0.000010
    H3_ s       0.000001   0.000008


                        gross atomic populations
     ao           C1_        C2_        H1_        H2_        H3_
      s        12.738103  12.099951   1.720441   1.710525   1.730979
    total      12.738103  12.099951   1.720441   1.710525   1.730979
 

 Total number of electrons:   30.00000000

