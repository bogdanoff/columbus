
 DALTON: user specified work memory size used,
          environment variable WRKMEM = "50000000            "

 Work memory size (LMWORK) :    50000000 =  381.47 megabytes.

 Default basis set library used :
        /sphome/kedziora/dalton/basis/                              


    $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    $$$$$$$$$$$  DALTON - An electronic structure program  $$$$$$$$$$$
    $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

               This is output from DALTON (beta-version 0.9) 

                          Principal authors:

            Trygve Helgaker,     University of Oslo,        Norway 
            Hans Joergen Jensen, University of Odense,      Denmark
            Poul Joergensen,     University of Aarhus,      Denmark
            Henrik Koch,         University of Aarhus,      Denmark
            Jeppe Olsen,         University of Lund,        Sweden 
            Hans Aagren,         University of Linkoeping,  Sweden 

                          Contributors:

            Torgeir Andersen,    University of Oslo,        Norway 
            Keld L. Bak,         University of Copenhagen,  Denmark
            Vebjoern Bakken,     University of Oslo,        Norway 
            Ove Christiansen,    University of Aarhus,      Denmark
            Paal Dahle,          University of Oslo,        Norway 
            Erik K. Dalskov,     University of Odense,      Denmark
            Thomas Enevoldsen,   University of Odense,      Denmark
            Asger Halkier,       University of Aarhus,      Denmark
            Hanne Heiberg,       University of Oslo,        Norway 
            Dan Jonsson,         University of Linkoeping,  Sweden 
            Sheela Kirpekar,     University of Odense,      Denmark
            Rika Kobayashi,      University of Aarhus,      Denmark
            Alfredo S. de Meras, Valencia University,       Spain  
            Kurt Mikkelsen,      University of Aarhus,      Denmark
            Patrick Norman,      University of Linkoeping,  Sweden 
            Martin J. Packer,    University of Sheffield,   UK     
            Kenneth Ruud,        University of Oslo,        Norway 
            Trond Saue,          University of Oslo,        Norway 
            Peter Taylor,        San Diego Superc. Center,  USA    
            Olav Vahtras,        University of Linkoeping,  Sweden

                                             Release Date:  August 1996
------------------------------------------------------------------------


      
     NOTE:
      
     This is an experimental code for the evaluation of molecular
     properties using (MC)SCF/CC wave functions. The authors accept
      no responsibility for the performance of the code or for the
     correctness of the results.
      
     The code (in whole or part) is not to be reproduced for further
     distribution without the written permission of T. Helgaker,
     H. J. Aa. Jensen or P. Taylor.
      
     If results obtained with this code are published, an
     appropriate citation would be:
      
     T. Helgaker, H. J. Aa. Jensen, P.Joergensen, H. Koch,
     J. Olsen, H. Aagren, T. Andersen, K. L. Bak, V. Bakken,
     O. Christiansen, P. Dahle, E. K. Dalskov, T. Enevoldsen,
     A. Halkier, H. Heiberg, D. Jonsson, S. Kirpekar, R. Kobayashi,
     A. S. de Meras, K. V. Mikkelsen, P. Norman, M. J. Packer,
     K. Ruud, T.Saue, P. R. Taylor, and O. Vahtras:
     DALTON, an electronic structure program"



     ******************************************
     **    PROGRAM:              DALTON      **
     **    PROGRAM VERSION:      5.4.0.0     **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************



 <<<<<<<<<< OUTPUT FROM GENERAL INPUT PROCESSING >>>>>>>>>>




 Default print level:        0

    Integral sections will be executed
    Starting in Integral Section -



 *************************************************************************
 ****************** Output from HERMIT input processing ******************
 *************************************************************************



 Default print level:        2


 Calculation of one- and two-electron Hamiltonian integrals.



 Changes of defaults for READIN:
 -------------------------------


 Maximum number of primitives per integral block :    9



 *************************************************************************
 ****************** Output from READIN input processing ******************
 *************************************************************************



  Title Cards
  -----------

                                                                          
                                                                          


  Symmetry Operations
  -------------------

  Symmetry operations: 2



                      SYMGRP:Point group information
                      ------------------------------

Point group: C2h

   * The point group was generated by:

      Rotation about the z-axis
      Reflection in the xy-plane

   * Group multiplication table

        |  E   C2z   i   Oxy
   -----+--------------------
     E  |  E 
    C2z | C2z   E 
     i  |  i   Oxy   E 
    Oxy | Oxy   i   C2z   E 

   * Character table

        |  E   C2z   i   Oxy
   -----+--------------------
    Ag  |   1    1    1    1
    Bu  |   1   -1   -1    1
    Au  |   1    1   -1   -1
    Bg  |   1   -1    1   -1

   * Direct product table

        | Ag   Bu   Au   Bg 
   -----+--------------------
    Ag  | Ag 
    Bu  | Bu   Ag 
    Au  | Au   Bg   Ag 
    Bg  | Bg   Au   Bu   Ag 


  Atoms and basis sets
  --------------------

  Number of atom types:     2
  Total number of atoms:   10

  label    atoms   charge   prim    cont     basis   
  ----------------------------------------------------------------------
  C  1        2       6      26      14      [9s4p1d|3s2p1d]                        
  C  2        2       6      26      14      [9s4p1d|3s2p1d]                        
  H  1        2       1       7       5      [4s1p|2s1p]                            
  H  2        2       1       7       5      [4s1p|2s1p]                            
  H  3        2       1       7       5      [4s1p|2s1p]                            
  ----------------------------------------------------------------------
  ----------------------------------------------------------------------
  total:     10      30     146      86

  Spherical harmonic basis used.
  Threshold for integrals:  1.00D-15


  Cartesian Coordinates
  ---------------------

  Total number of coordinates: 30


   1   C  1 1   x     -3.4807834700
   2            y      0.2386028600
   3            z      0.0000000000

   4   C  1 2   x      3.4807834700
   5            y     -0.2386028600
   6            z      0.0000000000

   7   C  2 1   x     -1.1521235500
   8            y     -0.7704587200
   9            z      0.0000000000

  10   C  2 2   x      1.1521235500
  11            y      0.7704587200
  12            z      0.0000000000

  13   H  1 1   x     -3.6994600000
  14            y      2.2943566300
  15            z      0.0000000000

  16   H  1 2   x      3.6994600000
  17            y     -2.2943566300
  18            z      0.0000000000

  19   H  2 1   x     -5.1302848600
  20            y     -1.0076267200
  21            z      0.0000000000

  22   H  2 2   x      5.1302848600
  23            y      1.0076267200
  24            z      0.0000000000

  25   H  3 1   x     -0.9334501400
  26            y     -2.8262111400
  27            z      0.0000000000

  28   H  3 2   x      0.9334501400
  29            y      2.8262111400
  30            z      0.0000000000



  Symmetry Coordinates
  --------------------

  Number of coordinates in each symmetry:  10 10  5  5


  Symmetry 1

   1   C  1  x    [ 1  -  4 ]/2
   2   C  1  y    [ 2  -  5 ]/2
   3   C  2  x    [ 7  - 10 ]/2
   4   C  2  y    [ 8  - 11 ]/2
   5   H  1  x    [13  - 16 ]/2
   6   H  1  y    [14  - 17 ]/2
   7   H  2  x    [19  - 22 ]/2
   8   H  2  y    [20  - 23 ]/2
   9   H  3  x    [25  - 28 ]/2
  10   H  3  y    [26  - 29 ]/2


  Symmetry 2

  11   C  1  x    [ 1  +  4 ]/2
  12   C  1  y    [ 2  +  5 ]/2
  13   C  2  x    [ 7  + 10 ]/2
  14   C  2  y    [ 8  + 11 ]/2
  15   H  1  x    [13  + 16 ]/2
  16   H  1  y    [14  + 17 ]/2
  17   H  2  x    [19  + 22 ]/2
  18   H  2  y    [20  + 23 ]/2
  19   H  3  x    [25  + 28 ]/2
  20   H  3  y    [26  + 29 ]/2


  Symmetry 3

  21   C  1  z    [ 3  +  6 ]/2
  22   C  2  z    [ 9  + 12 ]/2
  23   H  1  z    [15  + 18 ]/2
  24   H  2  z    [21  + 24 ]/2
  25   H  3  z    [27  + 30 ]/2


  Symmetry 4

  26   C  1  z    [ 3  -  6 ]/2
  27   C  2  z    [ 9  - 12 ]/2
  28   H  1  z    [15  - 18 ]/2
  29   H  2  z    [21  - 24 ]/2
  30   H  3  z    [27  - 30 ]/2


   Interatomic separations (in Angstroms):
   ---------------------------------------

            C  1        C  2        C  1        C  2        H  1        H  2

   C  1    0.000000
   C  2    3.692547    0.000000
   C  1    1.342991    2.467730    0.000000
   C  2    2.467730    1.342991    1.466879    0.000000
   H  1    1.093995    4.029112    2.108890    2.691016    0.000000
   H  2    4.029112    1.093995    2.691016    2.108890    4.607194    0.000000
   H  1    1.093995    4.574915    2.108890    3.455094    1.904328    4.721851
   H  2    4.574915    1.093995    3.455094    2.108890    4.721851    1.904328
   H  1    2.108888    2.707669    1.093994    2.200107    3.079748    2.467732
   H  2    2.707669    2.108888    2.200107    1.093994    2.467732    3.079748

            H  1        H  2        H  1        H  2

   H  1    0.000000
   H  2    5.533394    0.000000
   H  1    2.420409    3.796350    0.000000
   H  2    3.796350    2.420409    3.150057    0.000000




  Bond distances (angstroms):
  ---------------------------

                  atom 1     atom 2                           distance
                  ------     ------                           --------
  bond distance:    C  1       C  1                           1.342991
  bond distance:    C  2       C  2                           1.342991
  bond distance:    C  2       C  1                           1.466879
  bond distance:    H  1       C  1                           1.093995
  bond distance:    H  2       C  2                           1.093995
  bond distance:    H  1       C  1                           1.093995
  bond distance:    H  2       C  2                           1.093995
  bond distance:    H  1       C  1                           1.093994
  bond distance:    H  2       C  2                           1.093994


  Bond angles (degrees):
  ----------------------

                  atom 1     atom 2     atom 3                   angle
                  ------     ------     ------                   -----
  bond angle:       H  1       C  1       C  1                 119.500
  bond angle:       H  1       C  1       C  1                 119.500
  bond angle:       H  1       C  1       H  1                 121.000
  bond angle:       H  2       C  2       C  2                 119.500
  bond angle:       H  2       C  2       C  2                 119.500
  bond angle:       H  2       C  2       H  2                 121.000
  bond angle:       C  2       C  1       C  1                 122.800
  bond angle:       H  1       C  1       C  1                 119.500
  bond angle:       H  1       C  1       C  2                 117.700
  bond angle:       C  1       C  2       C  2                 122.800
  bond angle:       H  2       C  2       C  2                 119.500
  bond angle:       H  2       C  2       C  1                 117.700


  Nuclear repulsion energy :  103.442997948297


  Orbital exponents and contraction coefficients
  ----------------------------------------------


  C  1#1 1s    1     6665.000000    0.0007 -0.0001  0.0000
   gen. cont.  2     1000.000000    0.0053 -0.0012  0.0000
               3      228.000000    0.0271 -0.0057  0.0000
               4       64.710000    0.1017 -0.0233  0.0000
               5       21.060000    0.2747 -0.0640  0.0000
               6        7.495000    0.4486 -0.1500  0.0000
               7        2.797000    0.2851 -0.1273  0.0000
               8        0.521500    0.0152  0.5445  0.0000
               9        0.159600   -0.0032  0.5805  1.0000

  C  1#2 1s   10     6665.000000    0.0007 -0.0001  0.0000
   gen. cont. 11     1000.000000    0.0053 -0.0012  0.0000
              12      228.000000    0.0271 -0.0057  0.0000
              13       64.710000    0.1017 -0.0233  0.0000
              14       21.060000    0.2747 -0.0640  0.0000
              15        7.495000    0.4486 -0.1500  0.0000
              16        2.797000    0.2851 -0.1273  0.0000
              17        0.521500    0.0152  0.5445  0.0000
              18        0.159600   -0.0032  0.5805  1.0000

  C  1#1 2px  19        9.439000    0.0381  0.0000
   gen. cont. 20        2.002000    0.2095  0.0000
              21        0.545600    0.5086  0.0000
              22        0.151700    0.4688  1.0000

  C  1#2 2px  23        9.439000    0.0381  0.0000
   gen. cont. 24        2.002000    0.2095  0.0000
              25        0.545600    0.5086  0.0000
              26        0.151700    0.4688  1.0000

  C  1#1 2py  27        9.439000    0.0381  0.0000
   gen. cont. 28        2.002000    0.2095  0.0000
              29        0.545600    0.5086  0.0000
              30        0.151700    0.4688  1.0000

  C  1#2 2py  31        9.439000    0.0381  0.0000
   gen. cont. 32        2.002000    0.2095  0.0000
              33        0.545600    0.5086  0.0000
              34        0.151700    0.4688  1.0000

  C  1#1 2pz  35        9.439000    0.0381  0.0000
   gen. cont. 36        2.002000    0.2095  0.0000
              37        0.545600    0.5086  0.0000
              38        0.151700    0.4688  1.0000

  C  1#2 2pz  39        9.439000    0.0381  0.0000
   gen. cont. 40        2.002000    0.2095  0.0000
              41        0.545600    0.5086  0.0000
              42        0.151700    0.4688  1.0000

  C  1#1 3d2- 43        0.550000    1.0000

  C  1#2 3d2- 44        0.550000    1.0000

  C  1#1 3d1- 45        0.550000    1.0000

  C  1#2 3d1- 46        0.550000    1.0000

  C  1#1 3d0  47        0.550000    1.0000

  C  1#2 3d0  48        0.550000    1.0000

  C  1#1 3d1+ 49        0.550000    1.0000

  C  1#2 3d1+ 50        0.550000    1.0000

  C  1#1 3d2+ 51        0.550000    1.0000

  C  1#2 3d2+ 52        0.550000    1.0000

  C  2#1 1s   53     6665.000000    0.0007 -0.0001  0.0000
   gen. cont. 54     1000.000000    0.0053 -0.0012  0.0000
              55      228.000000    0.0271 -0.0057  0.0000
              56       64.710000    0.1017 -0.0233  0.0000
              57       21.060000    0.2747 -0.0640  0.0000
              58        7.495000    0.4486 -0.1500  0.0000
              59        2.797000    0.2851 -0.1273  0.0000
              60        0.521500    0.0152  0.5445  0.0000
              61        0.159600   -0.0032  0.5805  1.0000

  C  2#2 1s   62     6665.000000    0.0007 -0.0001  0.0000
   gen. cont. 63     1000.000000    0.0053 -0.0012  0.0000
              64      228.000000    0.0271 -0.0057  0.0000
              65       64.710000    0.1017 -0.0233  0.0000
              66       21.060000    0.2747 -0.0640  0.0000
              67        7.495000    0.4486 -0.1500  0.0000
              68        2.797000    0.2851 -0.1273  0.0000
              69        0.521500    0.0152  0.5445  0.0000
              70        0.159600   -0.0032  0.5805  1.0000

  C  2#1 2px  71        9.439000    0.0381  0.0000
   gen. cont. 72        2.002000    0.2095  0.0000
              73        0.545600    0.5086  0.0000
              74        0.151700    0.4688  1.0000

  C  2#2 2px  75        9.439000    0.0381  0.0000
   gen. cont. 76        2.002000    0.2095  0.0000
              77        0.545600    0.5086  0.0000
              78        0.151700    0.4688  1.0000

  C  2#1 2py  79        9.439000    0.0381  0.0000
   gen. cont. 80        2.002000    0.2095  0.0000
              81        0.545600    0.5086  0.0000
              82        0.151700    0.4688  1.0000

  C  2#2 2py  83        9.439000    0.0381  0.0000
   gen. cont. 84        2.002000    0.2095  0.0000
              85        0.545600    0.5086  0.0000
              86        0.151700    0.4688  1.0000

  C  2#1 2pz  87        9.439000    0.0381  0.0000
   gen. cont. 88        2.002000    0.2095  0.0000
              89        0.545600    0.5086  0.0000
              90        0.151700    0.4688  1.0000

  C  2#2 2pz  91        9.439000    0.0381  0.0000
   gen. cont. 92        2.002000    0.2095  0.0000
              93        0.545600    0.5086  0.0000
              94        0.151700    0.4688  1.0000

  C  2#1 3d2- 95        0.550000    1.0000

  C  2#2 3d2- 96        0.550000    1.0000

  C  2#1 3d1- 97        0.550000    1.0000

  C  2#2 3d1- 98        0.550000    1.0000

  C  2#1 3d0  99        0.550000    1.0000

  C  2#2 3d0 100        0.550000    1.0000

  C  2#1 3d1+101        0.550000    1.0000

  C  2#2 3d1+102        0.550000    1.0000

  C  2#1 3d2+103        0.550000    1.0000

  C  2#2 3d2+104        0.550000    1.0000

  H  1#1 1s  105       13.010000    0.0197  0.0000
   gen. cont.106        1.962000    0.1380  0.0000
             107        0.444600    0.4781  0.0000
             108        0.122000    0.5012  1.0000

  H  1#2 1s  109       13.010000    0.0197  0.0000
   gen. cont.110        1.962000    0.1380  0.0000
             111        0.444600    0.4781  0.0000
             112        0.122000    0.5012  1.0000

  H  1#1 2px 113        0.727000    1.0000

  H  1#2 2px 114        0.727000    1.0000

  H  1#1 2py 115        0.727000    1.0000

  H  1#2 2py 116        0.727000    1.0000

  H  1#1 2pz 117        0.727000    1.0000

  H  1#2 2pz 118        0.727000    1.0000

  H  2#1 1s  119       13.010000    0.0197  0.0000
   gen. cont.120        1.962000    0.1380  0.0000
             121        0.444600    0.4781  0.0000
             122        0.122000    0.5012  1.0000

  H  2#2 1s  123       13.010000    0.0197  0.0000
   gen. cont.124        1.962000    0.1380  0.0000
             125        0.444600    0.4781  0.0000
             126        0.122000    0.5012  1.0000

  H  2#1 2px 127        0.727000    1.0000

  H  2#2 2px 128        0.727000    1.0000

  H  2#1 2py 129        0.727000    1.0000

  H  2#2 2py 130        0.727000    1.0000

  H  2#1 2pz 131        0.727000    1.0000

  H  2#2 2pz 132        0.727000    1.0000

  H  3#1 1s  133       13.010000    0.0197  0.0000
   gen. cont.134        1.962000    0.1380  0.0000
             135        0.444600    0.4781  0.0000
             136        0.122000    0.5012  1.0000

  H  3#2 1s  137       13.010000    0.0197  0.0000
   gen. cont.138        1.962000    0.1380  0.0000
             139        0.444600    0.4781  0.0000
             140        0.122000    0.5012  1.0000

  H  3#1 2px 141        0.727000    1.0000

  H  3#2 2px 142        0.727000    1.0000

  H  3#1 2py 143        0.727000    1.0000

  H  3#2 2py 144        0.727000    1.0000

  H  3#1 2pz 145        0.727000    1.0000

  H  3#2 2pz 146        0.727000    1.0000


  Contracted Orbitals
  -------------------

   1  C  1#1  1s     1   2   3   4   5   6   7   8   9
   2  C  1#2  1s    10  11  12  13  14  15  16  17  18
   3  C  1#1  1s     1   2   3   4   5   6   7   8   9
   4  C  1#2  1s    10  11  12  13  14  15  16  17  18
   5  C  1#1  1s     9
   6  C  1#2  1s    18
   7  C  1#1  2px   19  20  21  22
   8  C  1#2  2px   23  24  25  26
   9  C  1#1  2py   27  28  29  30
  10  C  1#2  2py   31  32  33  34
  11  C  1#1  2pz   35  36  37  38
  12  C  1#2  2pz   39  40  41  42
  13  C  1#1  2px   22
  14  C  1#2  2px   26
  15  C  1#1  2py   30
  16  C  1#2  2py   34
  17  C  1#1  2pz   38
  18  C  1#2  2pz   42
  19  C  1#1  3d2-  43
  20  C  1#2  3d2-  44
  21  C  1#1  3d1-  45
  22  C  1#2  3d1-  46
  23  C  1#1  3d0   47
  24  C  1#2  3d0   48
  25  C  1#1  3d1+  49
  26  C  1#2  3d1+  50
  27  C  1#1  3d2+  51
  28  C  1#2  3d2+  52
  29  C  2#1  1s    53  54  55  56  57  58  59  60  61
  30  C  2#2  1s    62  63  64  65  66  67  68  69  70
  31  C  2#1  1s    53  54  55  56  57  58  59  60  61
  32  C  2#2  1s    62  63  64  65  66  67  68  69  70
  33  C  2#1  1s    61
  34  C  2#2  1s    70
  35  C  2#1  2px   71  72  73  74
  36  C  2#2  2px   75  76  77  78
  37  C  2#1  2py   79  80  81  82
  38  C  2#2  2py   83  84  85  86
  39  C  2#1  2pz   87  88  89  90
  40  C  2#2  2pz   91  92  93  94
  41  C  2#1  2px   74
  42  C  2#2  2px   78
  43  C  2#1  2py   82
  44  C  2#2  2py   86
  45  C  2#1  2pz   90
  46  C  2#2  2pz   94
  47  C  2#1  3d2-  95
  48  C  2#2  3d2-  96
  49  C  2#1  3d1-  97
  50  C  2#2  3d1-  98
  51  C  2#1  3d0   99
  52  C  2#2  3d0  100
  53  C  2#1  3d1+ 101
  54  C  2#2  3d1+ 102
  55  C  2#1  3d2+ 103
  56  C  2#2  3d2+ 104
  57  H  1#1  1s   105 106 107 108
  58  H  1#2  1s   109 110 111 112
  59  H  1#1  1s   108
  60  H  1#2  1s   112
  61  H  1#1  2px  113
  62  H  1#2  2px  114
  63  H  1#1  2py  115
  64  H  1#2  2py  116
  65  H  1#1  2pz  117
  66  H  1#2  2pz  118
  67  H  2#1  1s   119 120 121 122
  68  H  2#2  1s   123 124 125 126
  69  H  2#1  1s   122
  70  H  2#2  1s   126
  71  H  2#1  2px  127
  72  H  2#2  2px  128
  73  H  2#1  2py  129
  74  H  2#2  2py  130
  75  H  2#1  2pz  131
  76  H  2#2  2pz  132
  77  H  3#1  1s   133 134 135 136
  78  H  3#2  1s   137 138 139 140
  79  H  3#1  1s   136
  80  H  3#2  1s   140
  81  H  3#1  2px  141
  82  H  3#2  2px  142
  83  H  3#1  2py  143
  84  H  3#2  2py  144
  85  H  3#1  2pz  145
  86  H  3#2  2pz  146




  Symmetry Orbitals
  -----------------

  Number of orbitals in each symmetry:        32 32 11 11


  Symmetry  Ag ( 1)

    1     C  1     1s         1  +   2
    2     C  1     1s         3  +   4
    3     C  1     1s         5  +   6
    4     C  1     2px        7  -   8
    5     C  1     2py        9  -  10
    6     C  1     2px       13  -  14
    7     C  1     2py       15  -  16
    8     C  1     3d2-      19  +  20
    9     C  1     3d0       23  +  24
   10     C  1     3d2+      27  +  28
   11     C  2     1s        29  +  30
   12     C  2     1s        31  +  32
   13     C  2     1s        33  +  34
   14     C  2     2px       35  -  36
   15     C  2     2py       37  -  38
   16     C  2     2px       41  -  42
   17     C  2     2py       43  -  44
   18     C  2     3d2-      47  +  48
   19     C  2     3d0       51  +  52
   20     C  2     3d2+      55  +  56
   21     H  1     1s        57  +  58
   22     H  1     1s        59  +  60
   23     H  1     2px       61  -  62
   24     H  1     2py       63  -  64
   25     H  2     1s        67  +  68
   26     H  2     1s        69  +  70
   27     H  2     2px       71  -  72
   28     H  2     2py       73  -  74
   29     H  3     1s        77  +  78
   30     H  3     1s        79  +  80
   31     H  3     2px       81  -  82
   32     H  3     2py       83  -  84


  Symmetry  Bu ( 2)

   33     C  1     1s         1  -   2
   34     C  1     1s         3  -   4
   35     C  1     1s         5  -   6
   36     C  1     2px        7  +   8
   37     C  1     2py        9  +  10
   38     C  1     2px       13  +  14
   39     C  1     2py       15  +  16
   40     C  1     3d2-      19  -  20
   41     C  1     3d0       23  -  24
   42     C  1     3d2+      27  -  28
   43     C  2     1s        29  -  30
   44     C  2     1s        31  -  32
   45     C  2     1s        33  -  34
   46     C  2     2px       35  +  36
   47     C  2     2py       37  +  38
   48     C  2     2px       41  +  42
   49     C  2     2py       43  +  44
   50     C  2     3d2-      47  -  48
   51     C  2     3d0       51  -  52
   52     C  2     3d2+      55  -  56
   53     H  1     1s        57  -  58
   54     H  1     1s        59  -  60
   55     H  1     2px       61  +  62
   56     H  1     2py       63  +  64
   57     H  2     1s        67  -  68
   58     H  2     1s        69  -  70
   59     H  2     2px       71  +  72
   60     H  2     2py       73  +  74
   61     H  3     1s        77  -  78
   62     H  3     1s        79  -  80
   63     H  3     2px       81  +  82
   64     H  3     2py       83  +  84


  Symmetry  Au ( 3)

   65     C  1     2pz       11  +  12
   66     C  1     2pz       17  +  18
   67     C  1     3d1-      21  -  22
   68     C  1     3d1+      25  -  26
   69     C  2     2pz       39  +  40
   70     C  2     2pz       45  +  46
   71     C  2     3d1-      49  -  50
   72     C  2     3d1+      53  -  54
   73     H  1     2pz       65  +  66
   74     H  2     2pz       75  +  76
   75     H  3     2pz       85  +  86


  Symmetry  Bg ( 4)

   76     C  1     2pz       11  -  12
   77     C  1     2pz       17  -  18
   78     C  1     3d1-      21  +  22
   79     C  1     3d1+      25  +  26
   80     C  2     2pz       39  -  40
   81     C  2     2pz       45  -  46
   82     C  2     3d1-      49  +  50
   83     C  2     3d1+      53  +  54
   84     H  1     2pz       65  -  66
   85     H  2     2pz       75  -  76
   86     H  3     2pz       85  -  86

  Symmetries of electric field:  Bu (2)  Bu (2)  Au (3)

  Symmetries of magnetic field:  Bg (4)  Bg (4)  Ag (1)


 Copy of input to READIN
 -----------------------

INTGRL                                                                          
                                                                                
                                                                                
s   2    2XY  Z      0.10D-14                                                   
       6.0    2    3    1    1    1                                             
C  1  -3.480783470000000   0.238602860000000   0.000000000000000       *        
C  2  -1.152123550000000  -0.770458720000000   0.000000000000000       *        
H   9   3                                                                       
       6665.00000000         0.00069200        -0.00014600         0.00000000   
       1000.00000000         0.00532900        -0.00115400         0.00000000   
        228.00000000         0.02707700        -0.00572500         0.00000000   
         64.71000000         0.10171800        -0.02331200         0.00000000   
         21.06000000         0.27474000        -0.06395500         0.00000000   
          7.49500000         0.44856400        -0.14998100         0.00000000   
          2.79700000         0.28507400        -0.12726200         0.00000000   
          0.52150000         0.01520400         0.54452900         0.00000000   
          0.15960000        -0.00319100         0.58049600         1.00000000   
H   4   2                                                                       
          9.43900000         0.03810900         0.00000000                      
          2.00200000         0.20948000         0.00000000                      
          0.54560000         0.50855700         0.00000000                      
          0.15170000         0.46884200         1.00000000                      
H   1   1                                                                       
          0.55000000         1.00000000                                         
       1.0    3    2    1    1                                                  
H  1  -3.699460000000000   2.294356630000000   0.000000000000000       *        
H  2  -5.130284860000000  -1.007626720000000   0.000000000000000       *        
H  3  -0.933450140000000  -2.826211140000000   0.000000000000000       *        
H   4   2                                                                       
         13.01000000         0.01968500         0.00000000                      
          1.96200000         0.13797700         0.00000000                      
          0.44460000         0.47814800         0.00000000                      
          0.12200000         0.50124000         1.00000000                      
H   1   1                                                                       
          0.72700000         1.00000000                                         


 herdrv: noofopt= 0


 ************************************************************************
 ************************** Output from HERONE **************************
 ************************************************************************

 prop, itype F 1


  1186 atomic overlap integrals written in   1 buffers.
 Percentage non-zero integrals:  31.70
 prop, itype F 2


  1188 one-el. Hamil. integrals written in   1 buffers.
 Percentage non-zero integrals:  31.76
 prop, itype F 3


  1188 kinetic energy integrals written in   1 buffers.
 Percentage non-zero integrals:  31.76




 ************************************************************************
 ************************** Output from TWOINT **************************
 ************************************************************************

 calling sifew2:luinta,info,num,last,nrec
 calling sifew2: 11 2 4096 3272 4096 2730 2271 2 789070466

 Number of two-electron integrals written:   1858671 (26.6%)
 Kilobytes written:                            22313




 >>>> Total CPU  time used in HERMIT:   3.48 seconds
 >>>> Total wall time used in HERMIT:   4.00 seconds

- End of Integral Section
