1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      09/20/00    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 using llenci= -1
================================================================================
four external integ    6.75 MB location: distr. (sg)   
three external inte    5.25 MB location: distr. (sg)   
four external integ    6.75 MB location: distr. (sg)   
three external inte    5.00 MB location: distr. (sg)   
diagonal integrals     0.16 MB location: virtual disk  
off-diagonal integr    1.62 MB location: virtual disk  
computed file size in DP units
fil3w:      688107
fil3x:      655340
fil4w:      884709
fil4x:      884709
ofdgint:       20475
diagint:      212940
computed file size in DP units
fil3w:     5504856
fil3x:     5242720
fil4w:     7077672
fil4x:     7077672
ofdgint:      163800
diagint:     1703520
 nsubmx= 4  lenci= 914757
global arrays:      8232813   (             62.81 MB)
vdisk:               233415   (              1.78 MB)
drt:                1279488   (              4.88 MB)
================================================================================
 Main memory management:
 global          1478765 DP per process
 vdisk            233415 DP per process
 stack                 0 DP per process
 core           48287820 DP per process
Array Handle=-1000 Name:'civect' Data Type:double
Array Dimensions:914757x4
Process=0	 owns array section: [1:152460,1:4] 
Process=1	 owns array section: [152461:304920,1:4] 
Process=2	 owns array section: [304921:457380,1:4] 
Process=3	 owns array section: [457381:609840,1:4] 
Process=4	 owns array section: [609841:762300,1:4] 
Process=5	 owns array section: [762301:914757,1:4] 
Array Handle=-999 Name:'sigvec' Data Type:double
Array Dimensions:914757x4
Process=0	 owns array section: [1:152460,1:4] 
Process=1	 owns array section: [152461:304920,1:4] 
Process=2	 owns array section: [304921:457380,1:4] 
Process=3	 owns array section: [457381:609840,1:4] 
Process=4	 owns array section: [609841:762300,1:4] 
Process=5	 owns array section: [762301:914757,1:4] 
Array Handle=-998 Name:'hdg' Data Type:double
Array Dimensions:914757x1
Process=0	 owns array section: [1:152460,1:1] 
Process=1	 owns array section: [152461:304920,1:1] 
Process=2	 owns array section: [304921:457380,1:1] 
Process=3	 owns array section: [457381:609840,1:1] 
Process=4	 owns array section: [609841:762300,1:1] 
Process=5	 owns array section: [762301:914757,1:1] 
Array Handle=-997 Name:'drt' Data Type:double
Array Dimensions:91392x7
Process=0	 owns array section: [1:15232,1:7] 
Process=1	 owns array section: [15233:30464,1:7] 
Process=2	 owns array section: [30465:45696,1:7] 
Process=3	 owns array section: [45697:60928,1:7] 
Process=4	 owns array section: [60929:76160,1:7] 
Process=5	 owns array section: [76161:91392,1:7] 
Array Handle=-996 Name:'nxttask' Data Type:integer
Array Dimensions:1x1
Process=0	 owns array section: [1:1,1:1] 
Process=1	 owns array section: [0:-1,0:-1] 
Process=2	 owns array section: [0:-1,0:-1] 
Process=3	 owns array section: [0:-1,0:-1] 
Process=4	 owns array section: [0:-1,0:-1] 
Process=5	 owns array section: [0:-1,0:-1] 
 CIUDG version 5.9.7 ( 5-Oct-2004)
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 0,
  GSET = 0,
   DAVCOR =10,
  NCOREL = 22
  NROOT = 1
  IVMODE = 3
  NBKITR = 1
  NVBKMN = 1
  NVBKMX = 4
  RTOLBK = 1e-3,
  NITER = 10
  NVCIMN = 1
  NVCIMX = 4
  RTOLCI = 1e-3,
  IDEN  = 1
  CSFPRN = 10,
  MAXSEG = 20
  nseg0x = 1,1,2,2
  nseg2x = 1,1,2,2
  nseg1x = 1,1,2,2
  nseg3x = 1,1,2,2
  nsegd = 1,1,2,2
  nseg4x = 1,1,2,2,
  c2ex0ex=0
  c3ex1ex=0
  cdg4ex=1
  fileloc=1,1,3,3,3,3,2
  finalv=-1
  finalw=-1
  &end
  
 ------------------------------------------------------------------------

 ** list of control variables **
 nrfitr =   30      nvrfmx =    4      nvrfmn =    1
 lvlprt =    0      nroot  =    1      noldv  =    0      noldhv =    0
 nunitv =    1      ntype  =    0      nbkitr =    1      niter  =   10
 ivmode =    3      vout   =    0      istrt  =    0      iortls =    0
 nvbkmx =    4      ibktv  =   -1      ibkthv =   -1      frcsub =    0
 nvcimx =    4      icitv  =   -1      icithv =   -1      maxseg =   20
 iden   =    1      itran  =    0      froot  =    0      rtmode =    0
 ftcalc =    1      lrtshift=1.0000    ncouple=    1      skipso  =   F
 ncorel =   22      csfprn  =  10      ctol   = 1.00E-02  davcor  =  10


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-03    1.000E-03
 
 units and filenames:
    1: ( 6)    ciudgls                                                     
    2: ( 5)    ciudgin                                                     
    3: ( 7)    ciudgsm                                                     
    4: ( 4)    cihdiag                                                     
    5: ( 3)    cihpseu                                                     
    6: (10)    cihvfl                                                      
    7: (11)    civfl                                                       
    8: (12)    diagint                                                     
    9: (13)    ofdgint                                                     
   10: (17)    cidrtfl                                                     
   11: (20)    ciftdfl                                                     
   12: (21)    ciftofl                                                     
   13: (24)    ciftifl                                                     
   14: ( 8)    ciflind                                                     
   15: (15)    civin                                                       
   16: (16)    civout                                                      
   18: (18)    d1fl                                                        
   20: (53)    cifvfl                                                      
   21: (25)    ciscr4                                                      
   22: (41)    ciftotd                                                     
   23: (42)    flacpfd                                                     
   26: (26)    ciscr5                                                      
   27: (47)    restart                                                     
   28: (48)    civout                                                      
   29: (49)    cirefv                                                      
   31: (31)    fil4w                                                       
   32: (32)    fil4x                                                       
   33: (33)    fil3w                                                       
   34: (34)    fil3x                                                       
   36: (36)    aoints                                                      
   37: (37)    aoints2                                                     
   38: (38)    drtfil                                                      
   39: (50)    cisrtif                                                     
   42: (50)    mocoef                                                      
   45: (52)    nocoef_ci                                                   
   46: (53)    civfl_restart                                               
 ------------------------------------------------------------------------
 broadcasting INDATA    
 broadcasting ACPFINFO  
 broadcasting CFILES    

 workspace allocation information: lcore=  48287820 mem1=         1 ifirst=         1

 integral file titles:
 Hermit Integral Program : SIFS version  j25             Tue Oct 19 13:57:50 2004
  cidrt_title                                                                    
 Hermit Integral Program : SIFS version  zam403          Wed Dec  5 17:51:50 2001
  title                                                                          
 mofmt: formatted orbitals label=morbl   zam403          Wed Dec  5 17:52:53 2001
 SIFS file created by program tran.      j25             Tue Oct 19 13:57:54 2004

 core energy values from the integral file:
 energy( 1)=  1.034429979483E+02, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -1.274401923755E+02, ietype=    6,   fcore energy of type: H1(*)   
 energy( 3)= -1.276855608043E+02, ietype=    5,   fcore energy of type: Vref(*) 

 total core repulsion energy = -1.516827552316E+02

 drt header information:
  cidrt_title                                                                    
 spnorb, spnodd, lxyzir,hmult F F 0 0 0 0
 nmot  =    86 niot  =    13 nfct  =     4 nfvt  =     0
 nrow  =    95 nsym  =     4 ssym  =     1 lenbuf=  1600
 nwalk,xbar:       4469      680     1955      834     1000
 nvalwt,nvalw:     3657      354     1955      618      730
 ncsft:          914757
 total number of valid internal walks:    3657
 nvalz,nvaly,nvalx,nvalw =      354    1955     618     730

 cisrt info file parameters:
 file number  12 blocksize   4095
 mxbld   8190
 nd4ext,nd2ext,nd0ext  4830  1794   182
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int   822120   583389   173652    24945     1461        0        0        0
 minbl4,minbl3,maxbl2  2295  2295  2298
 maxbuf 32767
 number of external orbitals per symmetry block:  25  26   9   9
 nmsym   4 number of internal orbitals  13

 formula file title:
 Hermit Integral Program : SIFS version  j25             Tue Oct 19 13:57:50 2004
  cidrt_title                                                                    
 Hermit Integral Program : SIFS version  zam403          Wed Dec  5 17:51:50 2001
  title                                                                          
 mofmt: formatted orbitals label=morbl   zam403          Wed Dec  5 17:52:53 2001
 SIFS file created by program tran.      j25             Tue Oct 19 13:57:54 2004
  cidrt_title                                                                    
 file nmb.  24 block size     0
 pthz,pthy,pthx,pthw:   680  1955   834  1000 total internal walks:    4469
 maxlp3,n3xtlp,n2xtlp,n1xtlp,n0xtlp,n2lp,n1lp,n0lp     0     0     0     0
 orbsym(*)= 1 1 1 2 2 2 2 2 2 3 3 4 4

 setref:       12 references kept,
                0 references were marked as invalid, out of
               12 total.
 limcnvrt: found 354  valid internal walksout of  680  walks (skipping trailing invalids)
  ... adding  354  segmentation marks segtype= 1
 limcnvrt: found 1955  valid internal walksout of  1955  walks (skipping trailing invalids)
  ... adding  1955  segmentation marks segtype= 2
 limcnvrt: found 618  valid internal walksout of  834  walks (skipping trailing invalids)
  ... adding  618  segmentation marks segtype= 3
 limcnvrt: found 730  valid internal walksout of  1000  walks (skipping trailing invalids)
  ... adding  730  segmentation marks segtype= 4
 broadcasting SOLXYZ    
 broadcasting INDATA    
 broadcasting REPNUC    
 broadcasting INF       
 broadcasting SOINF     
 broadcasting DATA      
 broadcasting MOMAP     
 broadcasting CLI       
 broadcasting CSYM      
 broadcasting SLABEL    
 broadcasting MOMAP     
 broadcasting DRT       
 broadcasting INF1      
 broadcasting DRTINFO   
loaded       82 onel-dg  integrals(   1records ) at       1 on virtual disk
loaded     4830 4ext-dg  integrals(   2records ) at    4096 on virtual disk
loaded     1794 2ext-dg  integrals(   1records ) at   12286 on virtual disk
loaded      182 0ext-dg  integrals(   1records ) at   16381 on virtual disk
loaded      766 onel-of  integrals(   1records ) at   20476 on virtual disk
loaded   173652 2ext-og  integrals(  43records ) at   24571 on virtual disk
loaded    24945 1ext-og  integrals(   7records ) at  200656 on virtual disk
loaded     1461 0ext-og  integrals(   1records ) at  229321 on virtual disk

 number of external paths / symmetry
 vertex x     697     731     459     459
 vertex w     766     731     459     459

 lprune: l(*,*,*) pruned with nwalk=     680 nvalwt=     354 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    1955 nvalwt=    1955 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=     400 nvalwt=     310 nprune=      42

 lprune: l(*,*,*) pruned with nwalk=     434 nvalwt=     308 nprune=      47

 lprune: l(*,*,*) pruned with nwalk=     486 nvalwt=     366 nprune=      37

 lprune: l(*,*,*) pruned with nwalk=     514 nvalwt=     364 nprune=      40



                   segmentation summary for type diagonal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1         680|       354|         0|       354|         0|         1|
 -------------------------------------------------------------------------------
  Y 2        1955|     22797|       354|      1955|       354|       914|
 -------------------------------------------------------------------------------
  X 3         400|    186694|     23151|       310|      2309|      3298|
 -------------------------------------------------------------------------------
  X 4         434|    208692|    209845|       308|      2619|      4082|
 -------------------------------------------------------------------------------
  W 5         484|    243376|    418537|       366|      2927|      4882|
 -------------------------------------------------------------------------------
  W 6         514|    252844|    661913|       364|      3293|      5736|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>    914757


 lprune: l(*,*,*) pruned with nwalk=     680 nvalwt=     354 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    1955 nvalwt=    1955 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=     400 nvalwt=     310 nprune=      42

 lprune: l(*,*,*) pruned with nwalk=     434 nvalwt=     308 nprune=      47

 lprune: l(*,*,*) pruned with nwalk=     486 nvalwt=     366 nprune=      37

 lprune: l(*,*,*) pruned with nwalk=     514 nvalwt=     364 nprune=      40



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1         680|       354|         0|       354|         0|         1|
 -------------------------------------------------------------------------------
  Y 2        1955|     22797|       354|      1955|       354|       914|
 -------------------------------------------------------------------------------
  X 3         400|    186694|     23151|       310|      2309|      3298|
 -------------------------------------------------------------------------------
  X 4         434|    208692|    209845|       308|      2619|      4082|
 -------------------------------------------------------------------------------
  W 5         484|    243376|    418537|       366|      2927|      4882|
 -------------------------------------------------------------------------------
  W 6         514|    252844|    661913|       364|      3293|      5736|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>    914757


 lprune: l(*,*,*) pruned with nwalk=     680 nvalwt=     354 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    1955 nvalwt=    1955 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=     400 nvalwt=     310 nprune=      42

 lprune: l(*,*,*) pruned with nwalk=     434 nvalwt=     308 nprune=      47

 lprune: l(*,*,*) pruned with nwalk=     486 nvalwt=     366 nprune=      37

 lprune: l(*,*,*) pruned with nwalk=     514 nvalwt=     364 nprune=      40



                   segmentation summary for type one-external
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1         680|       354|         0|       354|         0|         1|
 -------------------------------------------------------------------------------
  Y 2        1955|     22797|       354|      1955|       354|       914|
 -------------------------------------------------------------------------------
  X 3         400|    186694|     23151|       310|      2309|      3298|
 -------------------------------------------------------------------------------
  X 4         434|    208692|    209845|       308|      2619|      4082|
 -------------------------------------------------------------------------------
  W 5         484|    243376|    418537|       366|      2927|      4882|
 -------------------------------------------------------------------------------
  W 6         514|    252844|    661913|       364|      3293|      5736|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>    914757


 lprune: l(*,*,*) pruned with nwalk=     680 nvalwt=     354 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    1955 nvalwt=    1955 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=     400 nvalwt=     310 nprune=      42

 lprune: l(*,*,*) pruned with nwalk=     434 nvalwt=     308 nprune=      47

 lprune: l(*,*,*) pruned with nwalk=     486 nvalwt=     366 nprune=      37

 lprune: l(*,*,*) pruned with nwalk=     514 nvalwt=     364 nprune=      40



                   segmentation summary for type two-external
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1         680|       354|         0|       354|         0|         1|
 -------------------------------------------------------------------------------
  Y 2        1955|     22797|       354|      1955|       354|       914|
 -------------------------------------------------------------------------------
  X 3         400|    186694|     23151|       310|      2309|      3298|
 -------------------------------------------------------------------------------
  X 4         434|    208692|    209845|       308|      2619|      4082|
 -------------------------------------------------------------------------------
  W 5         484|    243376|    418537|       366|      2927|      4882|
 -------------------------------------------------------------------------------
  W 6         514|    252844|    661913|       364|      3293|      5736|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>    914757


 lprune: l(*,*,*) pruned with nwalk=     680 nvalwt=     354 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    1955 nvalwt=    1955 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=     400 nvalwt=     310 nprune=      42

 lprune: l(*,*,*) pruned with nwalk=     434 nvalwt=     308 nprune=      47

 lprune: l(*,*,*) pruned with nwalk=     486 nvalwt=     366 nprune=      37

 lprune: l(*,*,*) pruned with nwalk=     514 nvalwt=     364 nprune=      40



                   segmentation summary for type three-external
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1         680|       354|         0|       354|         0|         1|
 -------------------------------------------------------------------------------
  Y 2        1955|     22797|       354|      1955|       354|       914|
 -------------------------------------------------------------------------------
  X 3         400|    186694|     23151|       310|      2309|      3298|
 -------------------------------------------------------------------------------
  X 4         434|    208692|    209845|       308|      2619|      4082|
 -------------------------------------------------------------------------------
  W 5         484|    243376|    418537|       366|      2927|      4882|
 -------------------------------------------------------------------------------
  W 6         514|    252844|    661913|       364|      3293|      5736|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>    914757


 lprune: l(*,*,*) pruned with nwalk=     680 nvalwt=     354 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    1955 nvalwt=    1955 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=     400 nvalwt=     310 nprune=      42

 lprune: l(*,*,*) pruned with nwalk=     434 nvalwt=     308 nprune=      47

 lprune: l(*,*,*) pruned with nwalk=     486 nvalwt=     366 nprune=      37

 lprune: l(*,*,*) pruned with nwalk=     514 nvalwt=     364 nprune=      40



                   segmentation summary for type four-external
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1         680|       354|         0|       354|         0|         1|
 -------------------------------------------------------------------------------
  Y 2        1955|     22797|       354|      1955|       354|       914|
 -------------------------------------------------------------------------------
  X 3         400|    186694|     23151|       310|      2309|      3298|
 -------------------------------------------------------------------------------
  X 4         434|    208692|    209845|       308|      2619|      4082|
 -------------------------------------------------------------------------------
  W 5         484|    243376|    418537|       366|      2927|      4882|
 -------------------------------------------------------------------------------
  W 6         514|    252844|    661913|       364|      3293|      5736|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>    914757

 broadcasting CIVCT     
 broadcasting DATA      
 broadcasting INF       
 broadcasting CNFSPC    
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS
----------------------------------------------------------------------------------------------------
     1  2   2    21      two-ext yy   2X  2 2    1955    1955      22797      22797    1955    1955
     2  3   3    22      two-ext xx   2X  3 3     400     400     186694     186694     310     310
     3  4   3    22      two-ext xx   2X  3 3     434     400     208692     186694     308     310
     4  4   4    22      two-ext xx   2X  3 3     434     434     208692     208692     308     308
     5  5   5    23      two-ext ww   2X  4 4     484     484     243376     243376     366     366
     6  6   5    23      two-ext ww   2X  4 4     514     484     252844     243376     364     366
     7  6   6    23      two-ext ww   2X  4 4     514     514     252844     252844     364     364
     8  3   1    24      two-ext xz   2X  3 1     400     680     186694        354     310     354
     9  4   1    24      two-ext xz   2X  3 1     434     680     208692        354     308     354
    10  5   1    25      two-ext wz   2X  4 1     484     680     243376        354     366     354
    11  6   1    25      two-ext wz   2X  4 1     514     680     252844        354     364     354
    12  5   3    26      two-ext wx   2X  4 3     484     400     243376     186694     366     310
    13  6   3    26      two-ext wx   2X  4 3     514     400     252844     186694     364     310
    14  5   4    26      two-ext wx   2X  4 3     484     434     243376     208692     366     308
    15  6   4    26      two-ext wx   2X  4 3     514     434     252844     208692     364     308
    16  2   1    11      one-ext yz   1X  2 1    1955     680      22797        354    1955     354
    17  3   2    13      one-ext yx   1X  3 2     400    1955     186694      22797     310    1955
    18  4   2    13      one-ext yx   1X  3 2     434    1955     208692      22797     308    1955
    19  5   2    14      one-ext yw   1X  4 2     484    1955     243376      22797     366    1955
    20  6   2    14      one-ext yw   1X  4 2     514    1955     252844      22797     364    1955
    21  3   2    31      thr-ext yx   3X  3 2     400    1955     186694      22797     310    1955
    22  4   2    31      thr-ext yx   3X  3 2     434    1955     208692      22797     308    1955
    23  5   2    32      thr-ext yw   3X  4 2     484    1955     243376      22797     366    1955
    24  6   2    32      thr-ext yw   3X  4 2     514    1955     252844      22797     364    1955
    25  1   1     1      allint zz    OX  1 1     680     680        354        354     354     354
    26  2   2     2      allint yy    OX  2 2    1955    1955      22797      22797    1955    1955
    27  3   3     3      allint xx    OX  3 3     400     400     186694     186694     310     310
    28  4   3     3      allint xx    OX  3 3     434     400     208692     186694     308     310
    29  4   4     3      allint xx    OX  3 3     434     434     208692     208692     308     308
    30  5   5     4      allint ww    OX  4 4     484     484     243376     243376     366     366
    31  6   5     4      allint ww    OX  4 4     514     484     252844     243376     364     366
    32  6   6     4      allint ww    OX  4 4     514     514     252844     252844     364     364
    33  1   1    75      dg-024ext z  DG  1 1     680     680        354        354     354     354
    34  2   2    45      4exdg024 y   DG  2 2    1955    1955      22797      22797    1955    1955
    35  3   3    46      4exdg024 x   DG  3 3     400     400     186694     186694     310     310
    36  4   4    46      4exdg024 x   DG  3 3     434     434     208692     208692     308     308
    37  5   5    47      4exdg024 w   DG  4 4     484     484     243376     243376     366     366
    38  6   6    47      4exdg024 w   DG  4 4     514     514     252844     252844     364     364
----------------------------------------------------------------------------------------------------
 broadcasting TASKLST   
 DIAGEL COUNTS: DG0X,DG2X,DG4X= 52161 15547 3303
 diagonal elements written to file   4

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:    6308 2x:       0 4x:       0
All internal counts: zz :   25449 yy:       0 xx:       0 ww:       0
One-external counts: yz :       0 yx:       0 yw:       0
Two-external counts: yy :       0 ww:       0 xx:       0 xz:       0 wz:       0 wx:       0
Three-ext.   counts: yx :       0 yw:       0

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 reference space has dimension      12

    root           eigenvalues
    ----           ------------
       1        -154.9824784923

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt= 354

         vector  1 from unit 11 written to unit 49 filename cirefv                                                      
 ufvoutnew: ... writing  recamt= 354

         vector  1 from unit 11 written to unit 49 filename cirefv                                                      

 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=   354)...
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:            914757
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               4
 number of roots to converge:                             1
 number of iterations:                                    1
 residual norm convergence criteria:               0.001000

          starting bk iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:   52161 2x:   15547 4x:    3303
All internal counts: zz :   25449 yy:       0 xx:       0 ww:       0
One-external counts: yz :   82284 yx:       0 yw:       0
Two-external counts: yy :       0 ww:       0 xx:       0 xz:    1230 wz:    1934 wx:       0
Three-ext.   counts: yx :       0 yw:       0

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 354

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -154.9824784923 -2.2204E-15  4.2087E-01  1.0947E+00  1.0000E-03
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   21    0   0.00   0.00   0.00   0.00         0.    0.0000
    2   22    0   0.00   0.00   0.00   0.00         0.    0.0000
    3   22    0   0.00   0.00   0.00   0.00         0.    0.0000
    4   22    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   23    0   0.00   0.00   0.00   0.00         0.    0.0000
    6   23    0   0.00   0.00   0.00   0.00         0.    0.0000
    7   23    0   0.00   0.00   0.00   0.00         0.    0.0000
    8   24    0   0.01   0.00   0.00   0.01         0.    0.0003
    9   24    2   0.01   0.00   0.00   0.01         0.    0.0005
   10   25    5   0.02   0.00   0.00   0.01         0.    0.0006
   11   25    1   0.02   0.00   0.00   0.01         0.    0.0006
   12   26    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   26    0   0.00   0.00   0.00   0.00         0.    0.0000
   14   26    0   0.00   0.00   0.00   0.00         0.    0.0000
   15   26    0   0.00   0.00   0.00   0.00         0.    0.0000
   16   11    3   0.15   0.10   0.00   0.15         0.    0.0696
   17   13    0   0.00   0.00   0.00   0.00         0.    0.0000
   18   13    0   0.00   0.00   0.00   0.00         0.    0.0000
   19   14    0   0.00   0.00   0.00   0.00         0.    0.0000
   20   14    0   0.00   0.00   0.00   0.00         0.    0.0000
   21   31    0   0.00   0.00   0.00   0.00         0.    0.0000
   22   31    0   0.00   0.00   0.00   0.00         0.    0.0000
   23   32    0   0.00   0.00   0.00   0.00         0.    0.0000
   24   32    0   0.00   0.00   0.00   0.00         0.    0.0000
   25    1    4   0.06   0.05   0.00   0.06         0.    0.0196
   26    2    0   0.00   0.00   0.00   0.00         0.    0.0000
   27    3    0   0.00   0.00   0.00   0.00         0.    0.0000
   28    3    0   0.00   0.00   0.00   0.00         0.    0.0000
   29    3    0   0.00   0.00   0.00   0.00         0.    0.0000
   30    4    0   0.00   0.00   0.00   0.00         0.    0.0000
   31    4    0   0.00   0.00   0.00   0.00         0.    0.0000
   32    4    0   0.00   0.00   0.00   0.00         0.    0.0000
   33   75    0   0.01   0.00   0.00   0.01         0.    0.0046
   34   45    2   0.02   0.02   0.00   0.02         0.    0.0065
   35   46    0   0.02   0.00   0.00   0.02         0.    0.0009
   36   46    5   0.02   0.00   0.00   0.02         0.    0.0009
   37   47    1   0.03   0.00   0.00   0.02         0.    0.0010
   38   47    0   0.03   0.00   0.00   0.02         0.    0.0011
================================================================
================ TIMING STATISTICS PER NODE    ================
node# twait    ths   tcin   teig  tvect  tga   ttotal 
  1   0.08   0.00   0.02   0.00   0.00   0.01   0.19
  2   0.10   0.00   0.02   0.00   0.00   0.01   0.28
  3   0.11   0.00   0.02   0.00   0.00   0.00   0.28
  4   0.00   0.00   0.02   0.00   0.00   0.00   0.31
  5   0.09   0.00   0.02   0.00   0.00   0.00   0.28
  6   0.10   0.00   0.02   0.00   0.00   0.01   0.31
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                     0.3865s 
time spent in multnx:                   0.3530s 
integral transfer time:                 0.0040s 
time spent for loop construction:       0.1814s 
syncronization time in mult:            0.4916s 
total time per CI iteration:            1.6355s 
 ** parallelization degree for mult section:0.4401

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)           0.01        0.00          0.000
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)          0.01        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.02        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:            0.03        0.00          0.000
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)          13.97        0.01       1284.643
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)         13.97        0.01       1504.287
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.05        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:           27.98        0.02       1369.820
========================================================


 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -154.9824784923 -2.2204E-15  4.2087E-01  1.0947E+00  1.0000E-03
 
 expansion vectors are not transformed.
 matrix-vector products are not transformed.

    2 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)

 from bk iterations: iconv=   1

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:            914757
 number of initial trial vectors:                         2
 number of initial matrix-vector products:                1
 maximum dimension of the subspace vectors:               4
 number of roots to converge:                             1
 number of iterations:                                   10
 residual norm convergence criteria:               0.001000

          starting ci iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:   52161 2x:   15547 4x:    3303
All internal counts: zz :   25449 yy:  198891 xx:   22385 ww:   28177
One-external counts: yz :   82284 yx:  126866 yw:  137681
Two-external counts: yy :   68919 ww:   12352 xx:   10716 xz:    1230 wz:    1934 wx:   17724
Three-ext.   counts: yx :   18602 yw:   20150

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 354

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.94544219    -0.32578991

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -155.3261768411  3.4370E-01  1.7490E-02  2.1018E-01  1.0000E-03
 mr-sdci #  1  2   -152.0879878835  4.0523E-01  0.0000E+00  6.9446E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   21    3   0.28   0.03   0.00   0.28         0.    0.0355
    2   22    2   0.24   0.00   0.00   0.23         0.    0.0023
    3   22    3   0.13   0.00   0.00   0.12         0.    0.0007
    4   22    4   0.20   0.00   0.00   0.19         0.    0.0017
    5   23    0   0.29   0.00   0.00   0.28         0.    0.0025
    6   23    3   0.17   0.00   0.00   0.15         0.    0.0007
    7   23    2   0.26   0.00   0.00   0.25         0.    0.0020
    8   24    3   0.01   0.00   0.00   0.01         0.    0.0003
    9   24    3   0.01   0.00   0.00   0.01         0.    0.0005
   10   25    3   0.01   0.00   0.00   0.01         0.    0.0006
   11   25    3   0.02   0.00   0.00   0.01         0.    0.0006
   12   26    1   0.43   0.00   0.00   0.42         0.    0.0034
   13   26    0   0.15   0.00   0.00   0.14         0.    0.0008
   14   26    3   0.14   0.00   0.00   0.13         0.    0.0007
   15   26    0   0.34   0.00   0.00   0.34         0.    0.0021
   16   11    0   0.16   0.10   0.00   0.16         0.    0.0696
   17   13    1   0.19   0.05   0.00   0.18         0.    0.0512
   18   13    4   0.20   0.05   0.00   0.20         0.    0.0488
   19   14    2   0.22   0.05   0.00   0.22         0.    0.0542
   20   14    5   0.25   0.06   0.00   0.25         0.    0.0535
   21   31    1   0.22   0.00   0.00   0.21         0.    0.0006
   22   31    4   0.21   0.00   0.00   0.21         0.    0.0006
   23   32    0   0.24   0.00   0.00   0.24         0.    0.0006
   24   32    3   0.22   0.00   0.00   0.21         0.    0.0006
   25    1    3   0.05   0.04   0.00   0.05         0.    0.0196
   26    2    5   0.25   0.13   0.00   0.25         0.    0.1416
   27    3    1   0.07   0.01   0.00   0.07         0.    0.0061
   28    3    4   0.03   0.01   0.00   0.02         0.    0.0025
   29    3    3   0.07   0.01   0.00   0.06         0.    0.0052
   30    4    4   0.14   0.01   0.00   0.13         0.    0.0076
   31    4    1   0.05   0.01   0.00   0.04         0.    0.0031
   32    4    0   0.11   0.02   0.00   0.11         0.    0.0058
   33   75    3   0.01   0.00   0.00   0.01         0.    0.0046
   34   45    4   0.02   0.01   0.00   0.02         0.    0.0000
   35   46    4   0.42   0.00   0.00   0.42         0.    0.0000
   36   46    1   0.42   0.00   0.00   0.41         0.    0.0000
   37   47    5   0.57   0.00   0.00   0.56         0.    0.0000
   38   47    2   0.53   0.00   0.00   0.52         0.    0.0000
================================================================
================ TIMING STATISTICS PER NODE    ================
node# twait    ths   tcin   teig  tvect  tga   ttotal 
  1   0.08   0.01   0.03   0.00   0.00   0.02   1.45
  2   0.00   0.01   0.03   0.00   0.00   0.03   1.42
  3   0.13   0.01   0.03   0.00   0.00   0.02   1.42
  4   0.25   0.01   0.03   0.00   0.00   0.04   1.42
  5   0.15   0.01   0.03   0.00   0.00   0.02   1.42
  6   0.30   0.01   0.03   0.00   0.00   0.01   1.42
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                     7.3189s 
time spent in multnx:                   7.1104s 
integral transfer time:                 0.0161s 
time spent for loop construction:       0.6371s 
syncronization time in mult:            0.9098s 
total time per CI iteration:            8.5637s 
 ** parallelization degree for mult section:0.8894

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)           0.35        0.00          0.000
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)          0.35        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.04        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:            0.75        0.00          0.000
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)          69.78        0.07       1003.339
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)         69.78        0.07        982.414
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.22        0.00        128.314
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:          139.77        0.14        982.562
========================================================


          starting ci iteration   2

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:   52161 2x:   15547 4x:    3303
All internal counts: zz :   25449 yy:  198891 xx:   22385 ww:   28177
One-external counts: yz :   82284 yx:  126866 yw:  137681
Two-external counts: yy :   68919 ww:   12352 xx:   10716 xz:    1230 wz:    1934 wx:   17724
Three-ext.   counts: yx :   18602 yw:   20150

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 354

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.94245833    -0.10819881     0.31633103

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1   -155.3387382526  1.2561E-02  1.4150E-03  6.4323E-02  1.0000E-03
 mr-sdci #  2  2   -152.7964485202  7.0846E-01  0.0000E+00  1.2938E+00  1.0000E-04
 mr-sdci #  2  3   -152.0758973544  3.9314E-01  0.0000E+00  1.1795E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   21    2   0.29   0.03   0.00   0.29         0.    0.0355
    2   22    1   0.22   0.00   0.00   0.21         0.    0.0023
    3   22    3   0.13   0.00   0.00   0.12         0.    0.0007
    4   22    1   0.18   0.00   0.00   0.18         0.    0.0017
    5   23    1   0.28   0.00   0.00   0.28         0.    0.0025
    6   23    5   0.15   0.00   0.00   0.14         0.    0.0007
    7   23    3   0.24   0.00   0.00   0.23         0.    0.0020
    8   24    0   0.01   0.00   0.00   0.01         0.    0.0003
    9   24    5   0.01   0.00   0.00   0.01         0.    0.0005
   10   25    4   0.01   0.00   0.00   0.01         0.    0.0006
   11   25    5   0.01   0.00   0.00   0.01         0.    0.0006
   12   26    2   0.43   0.00   0.00   0.42         0.    0.0034
   13   26    0   0.14   0.00   0.00   0.13         0.    0.0008
   14   26    4   0.14   0.00   0.00   0.13         0.    0.0007
   15   26    1   0.37   0.00   0.00   0.35         0.    0.0021
   16   11    2   0.15   0.10   0.00   0.15         0.    0.0696
   17   13    3   0.18   0.04   0.00   0.17         0.    0.0512
   18   13    4   0.18   0.05   0.00   0.18         0.    0.0488
   19   14    3   0.19   0.05   0.00   0.19         0.    0.0542
   20   14    5   0.22   0.06   0.00   0.21         0.    0.0535
   21   31    2   0.18   0.00   0.00   0.18         0.    0.0006
   22   31    0   0.18   0.00   0.00   0.18         0.    0.0006
   23   32    0   0.21   0.00   0.00   0.20         0.    0.0006
   24   32    5   0.21   0.00   0.00   0.20         0.    0.0006
   25    1    1   0.05   0.04   0.00   0.05         0.    0.0196
   26    2    4   0.23   0.13   0.00   0.23         0.    0.1416
   27    3    2   0.05   0.01   0.00   0.04         0.    0.0061
   28    3    5   0.02   0.01   0.00   0.01         0.    0.0025
   29    3    2   0.05   0.01   0.00   0.05         0.    0.0052
   30    4    5   0.08   0.01   0.00   0.08         0.    0.0076
   31    4    0   0.03   0.01   0.00   0.03         0.    0.0031
   32    4    1   0.07   0.02   0.00   0.06         0.    0.0058
   33   75    2   0.01   0.00   0.00   0.01         0.    0.0046
   34   45    4   0.02   0.01   0.00   0.02         0.    0.0000
   35   46    5   0.46   0.00   0.00   0.46         0.    0.0000
   36   46    3   0.44   0.00   0.00   0.43         0.    0.0000
   37   47    0   0.60   0.00   0.00   0.59         0.    0.0000
   38   47    4   0.58   0.00   0.00   0.58         0.    0.0000
================================================================
================ TIMING STATISTICS PER NODE    ================
node# twait    ths   tcin   teig  tvect  tga   ttotal 
  1   0.01   0.00   0.03   0.00   0.00   0.02   1.23
  2   0.00   0.00   0.03   0.00   0.00   0.02   1.23
  3   0.01   0.00   0.03   0.00   0.00   0.02   1.23
  4   0.00   0.00   0.03   0.00   0.00   0.01   1.23
  5   0.01   0.00   0.03   0.00   0.00   0.01   1.23
  6   0.01   0.00   0.03   0.00   0.00   0.02   1.23
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                     7.0011s 
time spent in multnx:                   6.8424s 
integral transfer time:                 0.0137s 
time spent for loop construction:       0.6208s 
syncronization time in mult:            0.0485s 
total time per CI iteration:            7.3596s 
 ** parallelization degree for mult section:0.9931

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)           0.35        0.00          0.000
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)          0.35        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.05        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:            0.75        0.00          0.000
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)          69.78        0.05       1331.518
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)         69.78        0.05       1401.743
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.21        0.00        183.452
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:          139.77        0.10       1352.475
========================================================


          starting ci iteration   3

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:   52161 2x:   15547 4x:    3303
All internal counts: zz :   25449 yy:  198891 xx:   22385 ww:   28177
One-external counts: yz :   82284 yx:  126866 yw:  137681
Two-external counts: yy :   68919 ww:   12352 xx:   10716 xz:    1230 wz:    1934 wx:   17724
Three-ext.   counts: yx :   18602 yw:   20150

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 354

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.94193229    -0.11401676     0.02304486     0.31501219

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1   -155.3398791814  1.1409E-03  1.7870E-04  2.1337E-02  1.0000E-03
 mr-sdci #  3  2   -152.9551202094  1.5867E-01  0.0000E+00  1.3205E+00  1.0000E-04
 mr-sdci #  3  3   -152.2955389237  2.1964E-01  0.0000E+00  1.3009E+00  1.0000E-04
 mr-sdci #  3  4   -152.0669402983  3.8419E-01  0.0000E+00  1.1447E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   21    4   0.30   0.03   0.00   0.30         0.    0.0355
    2   22    0   0.22   0.00   0.00   0.22         0.    0.0023
    3   22    2   0.14   0.00   0.00   0.13         0.    0.0007
    4   22    3   0.19   0.00   0.00   0.19         0.    0.0017
    5   23    5   0.28   0.00   0.00   0.28         0.    0.0025
    6   23    5   0.17   0.00   0.00   0.16         0.    0.0007
    7   23    2   0.25   0.00   0.00   0.24         0.    0.0020
    8   24    5   0.01   0.00   0.00   0.01         0.    0.0003
    9   24    0   0.01   0.00   0.00   0.01         0.    0.0005
   10   25    1   0.01   0.00   0.00   0.01         0.    0.0006
   11   25    3   0.02   0.00   0.00   0.01         0.    0.0006
   12   26    5   0.39   0.00   0.00   0.38         0.    0.0034
   13   26    0   0.15   0.00   0.00   0.14         0.    0.0008
   14   26    3   0.15   0.00   0.00   0.14         0.    0.0007
   15   26    4   0.34   0.00   0.00   0.33         0.    0.0021
   16   11    2   0.16   0.10   0.00   0.16         0.    0.0696
   17   13    4   0.19   0.05   0.00   0.19         0.    0.0512
   18   13    0   0.20   0.05   0.00   0.19         0.    0.0488
   19   14    2   0.23   0.05   0.00   0.23         0.    0.0542
   20   14    3   0.22   0.06   0.00   0.22         0.    0.0535
   21   31    5   0.22   0.00   0.00   0.21         0.    0.0006
   22   31    1   0.19   0.00   0.00   0.19         0.    0.0006
   23   32    4   0.25   0.00   0.00   0.24         0.    0.0006
   24   32    1   0.23   0.00   0.00   0.22         0.    0.0006
   25    1    4   0.06   0.05   0.00   0.06         0.    0.0196
   26    2    1   0.24   0.13   0.00   0.24         0.    0.1416
   27    3    0   0.06   0.01   0.00   0.06         0.    0.0061
   28    3    1   0.02   0.01   0.00   0.02         0.    0.0025
   29    3    3   0.06   0.01   0.00   0.06         0.    0.0052
   30    4    5   0.12   0.01   0.00   0.12         0.    0.0076
   31    4    4   0.05   0.01   0.00   0.04         0.    0.0031
   32    4    1   0.08   0.02   0.00   0.08         0.    0.0058
   33   75    4   0.01   0.01   0.00   0.01         0.    0.0046
   34   45    3   0.02   0.01   0.00   0.02         0.    0.0000
   35   46    1   0.41   0.00   0.00   0.41         0.    0.0000
   36   46    2   0.41   0.00   0.00   0.40         0.    0.0000
   37   47    0   0.55   0.00   0.00   0.55         0.    0.0000
   38   47    3   0.52   0.00   0.00   0.51         0.    0.0000
================================================================
================ TIMING STATISTICS PER NODE    ================
node# twait    ths   tcin   teig  tvect  tga   ttotal 
  1   0.00   0.01   0.03   0.00   0.00   0.02   1.25
  2   0.00   0.01   0.03   0.00   0.00   0.02   1.25
  3   0.00   0.01   0.03   0.00   0.00   0.02   1.25
  4   0.01   0.01   0.03   0.00   0.00   0.03   1.25
  5   0.00   0.01   0.03   0.00   0.00   0.02   1.25
  6   0.00   0.01   0.03   0.00   0.00   0.03   1.25
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                     7.1346s 
time spent in multnx:                   6.9495s 
integral transfer time:                 0.0157s 
time spent for loop construction:       0.6292s 
syncronization time in mult:            0.0217s 
total time per CI iteration:            7.4860s 
 ** parallelization degree for mult section:0.9970

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)           0.18        0.00          0.000
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)          0.18        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.03        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:            0.39        0.00          0.000
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)          69.95        0.07       1071.049
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)         69.95        0.07       1053.116
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.23        0.00        192.862
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:          140.13        0.13       1054.364
========================================================


          starting ci iteration   4

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:   52161 2x:   15547 4x:    3303
All internal counts: zz :   25449 yy:  198891 xx:   22385 ww:   28177
One-external counts: yz :   82284 yx:  126866 yw:  137681
Two-external counts: yy :   68919 ww:   12352 xx:   10716 xz:    1230 wz:    1934 wx:   17724
Three-ext.   counts: yx :   18602 yw:   20150

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 354

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.94178809    -0.02329115

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1   -155.3400055942  1.2641E-04  4.6074E-05  1.0431E-02  1.0000E-03
 mr-sdci #  4  2   -152.9385353693 -1.6585E-02  0.0000E+00  2.3193E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   21    5   0.31   0.03   0.00   0.31         0.    0.0355
    2   22    2   0.24   0.00   0.00   0.23         0.    0.0023
    3   22    3   0.14   0.00   0.00   0.13         0.    0.0007
    4   22    4   0.21   0.00   0.00   0.20         0.    0.0017
    5   23    1   0.29   0.00   0.00   0.29         0.    0.0025
    6   23    2   0.17   0.00   0.00   0.16         0.    0.0007
    7   23    2   0.26   0.00   0.00   0.25         0.    0.0020
    8   24    3   0.01   0.00   0.00   0.01         0.    0.0003
    9   24    5   0.01   0.00   0.00   0.01         0.    0.0005
   10   25    1   0.02   0.00   0.00   0.01         0.    0.0006
   11   25    0   0.02   0.00   0.00   0.01         0.    0.0006
   12   26    1   0.42   0.00   0.00   0.40         0.    0.0034
   13   26    4   0.16   0.00   0.00   0.15         0.    0.0008
   14   26    0   0.16   0.00   0.00   0.14         0.    0.0007
   15   26    5   0.37   0.00   0.00   0.35         0.    0.0021
   16   11    5   0.16   0.10   0.00   0.16         0.    0.0696
   17   13    1   0.19   0.04   0.00   0.19         0.    0.0512
   18   13    0   0.20   0.05   0.00   0.19         0.    0.0488
   19   14    0   0.22   0.04   0.00   0.21         0.    0.0542
   20   14    5   0.27   0.06   0.00   0.26         0.    0.0535
   21   31    1   0.20   0.00   0.00   0.20         0.    0.0006
   22   31    3   0.20   0.00   0.00   0.20         0.    0.0006
   23   32    3   0.22   0.00   0.00   0.22         0.    0.0006
   24   32    3   0.22   0.00   0.00   0.21         0.    0.0006
   25    1    4   0.06   0.05   0.00   0.06         0.    0.0196
   26    2    4   0.25   0.13   0.00   0.25         0.    0.1416
   27    3    0   0.08   0.01   0.00   0.07         0.    0.0061
   28    3    5   0.03   0.01   0.00   0.02         0.    0.0025
   29    3    2   0.08   0.01   0.00   0.08         0.    0.0052
   30    4    1   0.14   0.01   0.00   0.13         0.    0.0076
   31    4    2   0.05   0.01   0.00   0.04         0.    0.0031
   32    4    5   0.11   0.02   0.00   0.10         0.    0.0058
   33   75    2   0.01   0.01   0.00   0.01         0.    0.0046
   34   45    3   0.02   0.02   0.00   0.02         0.    0.0000
   35   46    3   0.43   0.00   0.00   0.43         0.    0.0000
   36   46    2   0.45   0.00   0.00   0.44         0.    0.0000
   37   47    0   0.59   0.00   0.00   0.58         0.    0.0000
   38   47    4   0.57   0.00   0.00   0.57         0.    0.0000
================================================================
================ TIMING STATISTICS PER NODE    ================
node# twait    ths   tcin   teig  tvect  tga   ttotal 
  1   0.00   0.00   0.02   0.00   0.00   0.03   1.28
  2   0.00   0.01   0.02   0.00   0.00   0.03   1.28
  3   0.01   0.01   0.02   0.00   0.00   0.03   1.28
  4   0.00   0.01   0.02   0.00   0.00   0.03   1.28
  5   0.01   0.01   0.02   0.00   0.00   0.02   1.28
  6   0.00   0.01   0.02   0.00   0.00   0.03   1.28
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                     7.5115s 
time spent in multnx:                   7.2867s 
integral transfer time:                 0.0198s 
time spent for loop construction:       0.6387s 
syncronization time in mult:            0.0169s 
total time per CI iteration:            7.6714s 
 ** parallelization degree for mult section:0.9978

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)           0.35        0.00          0.000
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)          0.35        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.04        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:            0.75        0.00          0.000
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)          69.78        0.08        911.266
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)         69.78        0.08        876.218
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.22        0.00        155.103
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:          139.78        0.16        886.882
========================================================


          starting ci iteration   5

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:   52161 2x:   15547 4x:    3303
All internal counts: zz :   25449 yy:  198891 xx:   22385 ww:   28177
One-external counts: yz :   82284 yx:  126866 yw:  137681
Two-external counts: yy :   68919 ww:   12352 xx:   10716 xz:    1230 wz:    1934 wx:   17724
Three-ext.   counts: yx :   18602 yw:   20150

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 354

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.94126292     0.08595894    -0.04212801

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1   -155.3400542824  4.8688E-05  1.4226E-05  5.5176E-03  1.0000E-03
 mr-sdci #  5  2   -154.0485354187  1.1100E+00  0.0000E+00  1.5044E+00  1.0000E-04
 mr-sdci #  5  3   -152.0731848780 -2.2235E-01  0.0000E+00  2.1491E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   21    4   0.32   0.03   0.00   0.32         0.    0.0355
    2   22    0   0.23   0.00   0.00   0.22         0.    0.0023
    3   22    5   0.14   0.00   0.00   0.14         0.    0.0007
    4   22    5   0.21   0.00   0.00   0.20         0.    0.0017
    5   23    1   0.28   0.00   0.00   0.28         0.    0.0025
    6   23    5   0.17   0.00   0.00   0.16         0.    0.0007
    7   23    5   0.26   0.00   0.00   0.25         0.    0.0020
    8   24    5   0.01   0.00   0.00   0.01         0.    0.0003
    9   24    4   0.01   0.00   0.00   0.01         0.    0.0005
   10   25    0   0.02   0.00   0.00   0.01         0.    0.0006
   11   25    3   0.02   0.00   0.00   0.01         0.    0.0006
   12   26    1   0.42   0.00   0.00   0.41         0.    0.0034
   13   26    0   0.15   0.00   0.00   0.14         0.    0.0008
   14   26    2   0.16   0.00   0.00   0.14         0.    0.0007
   15   26    4   0.37   0.00   0.00   0.35         0.    0.0021
   16   11    4   0.16   0.10   0.00   0.16         0.    0.0696
   17   13    1   0.19   0.04   0.00   0.18         0.    0.0512
   18   13    3   0.23   0.06   0.00   0.22         0.    0.0488
   19   14    1   0.21   0.05   0.00   0.20         0.    0.0542
   20   14    3   0.24   0.06   0.00   0.23         0.    0.0535
   21   31    2   0.22   0.00   0.00   0.21         0.    0.0006
   22   31    0   0.19   0.00   0.00   0.19         0.    0.0006
   23   32    3   0.23   0.00   0.00   0.23         0.    0.0006
   24   32    4   0.25   0.00   0.00   0.24         0.    0.0006
   25    1    2   0.06   0.05   0.00   0.06         0.    0.0196
   26    2    2   0.25   0.13   0.00   0.25         0.    0.1416
   27    3    0   0.06   0.01   0.00   0.06         0.    0.0061
   28    3    4   0.03   0.01   0.00   0.02         0.    0.0025
   29    3    3   0.07   0.01   0.00   0.06         0.    0.0052
   30    4    1   0.10   0.01   0.00   0.09         0.    0.0076
   31    4    1   0.04   0.01   0.00   0.03         0.    0.0031
   32    4    4   0.10   0.02   0.00   0.09         0.    0.0058
   33   75    1   0.01   0.01   0.00   0.01         0.    0.0046
   34   45    3   0.02   0.01   0.00   0.02         0.    0.0000
   35   46    3   0.44   0.00   0.00   0.43         0.    0.0000
   36   46    5   0.45   0.00   0.00   0.45         0.    0.0000
   37   47    0   0.59   0.00   0.00   0.58         0.    0.0000
   38   47    2   0.57   0.00   0.00   0.56         0.    0.0000
================================================================
================ TIMING STATISTICS PER NODE    ================
node# twait    ths   tcin   teig  tvect  tga   ttotal 
  1   0.01   0.01   0.02   0.00   0.00   0.02   1.28
  2   0.01   0.01   0.02   0.00   0.00   0.03   1.28
  3   0.00   0.01   0.02   0.00   0.00   0.02   1.28
  4   0.01   0.01   0.02   0.00   0.00   0.02   1.28
  5   0.01   0.01   0.02   0.00   0.00   0.03   1.28
  6   0.00   0.01   0.02   0.00   0.00   0.03   1.28
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                     7.4555s 
time spent in multnx:                   7.2334s 
integral transfer time:                 0.0188s 
time spent for loop construction:       0.6387s 
syncronization time in mult:            0.0475s 
total time per CI iteration:            7.6989s 
 ** parallelization degree for mult section:0.9937

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)           0.18        0.00          0.000
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)          0.18        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.03        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:            0.39        0.00          0.000
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)          69.95        0.08        903.184
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)         69.95        0.08        893.021
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.23        0.00        185.751
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:          140.13        0.16        892.573
========================================================


          starting ci iteration   6

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:   52161 2x:   15547 4x:    3303
All internal counts: zz :   25449 yy:  198891 xx:   22385 ww:   28177
One-external counts: yz :   82284 yx:  126866 yw:  137681
Two-external counts: yy :   68919 ww:   12352 xx:   10716 xz:    1230 wz:    1934 wx:   17724
Three-ext.   counts: yx :   18602 yw:   20150

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 354

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.94107867     0.07180465    -0.05601906    -0.03672817

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  6  1   -155.3400688671  1.4585E-05  2.7230E-06  2.8686E-03  1.0000E-03
 mr-sdci #  6  2   -154.6678365584  6.1930E-01  0.0000E+00  1.6325E+00  1.0000E-04
 mr-sdci #  6  3   -152.2187422674  1.4556E-01  0.0000E+00  1.3098E+00  1.0000E-04
 mr-sdci #  6  4   -152.0715608386  4.6205E-03  0.0000E+00  2.2300E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   21    1   0.29   0.03   0.00   0.28         0.    0.0355
    2   22    3   0.22   0.00   0.00   0.22         0.    0.0023
    3   22    1   0.14   0.00   0.00   0.13         0.    0.0007
    4   22    2   0.20   0.00   0.00   0.20         0.    0.0017
    5   23    4   0.29   0.00   0.00   0.29         0.    0.0025
    6   23    5   0.17   0.00   0.00   0.16         0.    0.0007
    7   23    3   0.24   0.00   0.00   0.24         0.    0.0020
    8   24    1   0.01   0.00   0.00   0.01         0.    0.0003
    9   24    4   0.01   0.00   0.00   0.01         0.    0.0005
   10   25    3   0.02   0.00   0.00   0.01         0.    0.0006
   11   25    4   0.02   0.00   0.00   0.01         0.    0.0006
   12   26    4   0.43   0.00   0.00   0.41         0.    0.0034
   13   26    2   0.16   0.00   0.00   0.15         0.    0.0008
   14   26    0   0.15   0.00   0.00   0.14         0.    0.0007
   15   26    1   0.35   0.00   0.00   0.33         0.    0.0021
   16   11    4   0.16   0.10   0.00   0.16         0.    0.0696
   17   13    3   0.19   0.04   0.00   0.18         0.    0.0512
   18   13    5   0.23   0.06   0.00   0.23         0.    0.0488
   19   14    0   0.20   0.04   0.00   0.20         0.    0.0542
   20   14    0   0.22   0.06   0.00   0.22         0.    0.0535
   21   31    4   0.22   0.00   0.00   0.22         0.    0.0006
   22   31    1   0.20   0.00   0.00   0.19         0.    0.0006
   23   32    1   0.23   0.00   0.00   0.22         0.    0.0006
   24   32    5   0.25   0.00   0.00   0.24         0.    0.0006
   25    1    2   0.06   0.05   0.00   0.06         0.    0.0196
   26    2    2   0.25   0.13   0.00   0.24         0.    0.1416
   27    3    0   0.06   0.01   0.00   0.06         0.    0.0061
   28    3    1   0.03   0.01   0.00   0.02         0.    0.0025
   29    3    5   0.08   0.01   0.00   0.08         0.    0.0052
   30    4    3   0.10   0.01   0.00   0.09         0.    0.0076
   31    4    5   0.05   0.01   0.00   0.04         0.    0.0031
   32    4    4   0.10   0.02   0.00   0.09         0.    0.0058
   33   75    0   0.01   0.01   0.00   0.01         0.    0.0046
   34   45    3   0.02   0.01   0.00   0.02         0.    0.0000
   35   46    3   0.44   0.00   0.00   0.44         0.    0.0000
   36   46    5   0.45   0.00   0.00   0.45         0.    0.0000
   37   47    0   0.58   0.00   0.00   0.58         0.    0.0000
   38   47    2   0.57   0.00   0.00   0.56         0.    0.0000
================================================================
================ TIMING STATISTICS PER NODE    ================
node# twait    ths   tcin   teig  tvect  tga   ttotal 
  1   0.00   0.01   0.03   0.00   0.00   0.02   1.27
  2   0.00   0.01   0.03   0.00   0.00   0.03   1.27
  3   0.00   0.01   0.03   0.00   0.00   0.02   1.27
  4   0.00   0.01   0.03   0.00   0.00   0.02   1.27
  5   0.00   0.01   0.03   0.00   0.00   0.03   1.27
  6   0.00   0.01   0.03   0.00   0.00   0.03   1.27
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                     7.3798s 
time spent in multnx:                   7.1657s 
integral transfer time:                 0.0173s 
time spent for loop construction:       0.6319s 
syncronization time in mult:            0.0161s 
total time per CI iteration:            7.6427s 
 ** parallelization degree for mult section:0.9978

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)           0.35        0.00          0.000
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)          0.35        0.00        325.800
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.04        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:            0.74        0.00        471.984
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)          69.78        0.07        937.884
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)         69.78        0.07        937.265
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.22        0.00        186.020
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:          139.78        0.15        931.681
========================================================


          starting ci iteration   7

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:   52161 2x:   15547 4x:    3303
All internal counts: zz :   25449 yy:  198891 xx:   22385 ww:   28177
One-external counts: yz :   82284 yx:  126866 yw:  137681
Two-external counts: yy :   68919 ww:   12352 xx:   10716 xz:    1230 wz:    1934 wx:   17724
Three-ext.   counts: yx :   18602 yw:   20150

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 354

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.94094854    -0.13695300

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  7  1   -155.3400712963  2.4292E-06  8.9144E-07  1.2394E-03  1.0000E-03
 mr-sdci #  7  2   -152.6671556936 -2.0007E+00  0.0000E+00  8.1978E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   21    2   0.31   0.03   0.00   0.31         0.    0.0355
    2   22    5   0.23   0.00   0.00   0.23         0.    0.0023
    3   22    1   0.15   0.00   0.00   0.14         0.    0.0007
    4   22    3   0.20   0.00   0.00   0.20         0.    0.0017
    5   23    4   0.30   0.00   0.00   0.29         0.    0.0025
    6   23    1   0.17   0.00   0.00   0.16         0.    0.0007
    7   23    3   0.26   0.00   0.00   0.25         0.    0.0020
    8   24    0   0.01   0.00   0.00   0.01         0.    0.0003
    9   24    4   0.02   0.00   0.00   0.01         0.    0.0005
   10   25    2   0.02   0.00   0.00   0.01         0.    0.0006
   11   25    5   0.02   0.00   0.00   0.01         0.    0.0006
   12   26    2   1.51   0.00   0.00   1.50         0.    0.0034
   13   26    3   0.16   0.00   0.00   0.15         0.    0.0008
   14   26    0   0.16   0.00   0.00   0.15         0.    0.0007
   15   26    4   1.44   0.00   0.00   1.43         0.    0.0021
   16   11    2   0.16   0.10   0.00   0.16         0.    0.0696
   17   13    5   0.20   0.04   0.00   0.19         0.    0.0512
   18   13    0   0.20   0.05   0.00   0.20         0.    0.0488
   19   14    0   0.23   0.05   0.00   0.22         0.    0.0542
   20   14    1   0.25   0.06   0.00   0.25         0.    0.0535
   21   31    2   0.22   0.00   0.00   0.22         0.    0.0006
   22   31    4   0.21   0.00   0.00   0.20         0.    0.0006
   23   32    4   0.25   0.00   0.00   0.24         0.    0.0006
   24   32    1   0.24   0.00   0.00   0.23         0.    0.0006
   25    1    0   0.06   0.05   0.00   0.06         0.    0.0196
   26    2    5   0.23   0.13   0.00   0.23         0.    0.1416
   27    3    3   0.07   0.01   0.00   0.06         0.    0.0061
   28    3    4   0.03   0.01   0.00   0.02         0.    0.0025
   29    3    2   0.08   0.01   0.00   0.07         0.    0.0052
   30    4    5   0.12   0.01   0.00   0.11         0.    0.0076
   31    4    2   0.05   0.01   0.00   0.04         0.    0.0031
   32    4    4   0.09   0.02   0.00   0.08         0.    0.0058
   33   75    5   0.01   0.01   0.00   0.01         0.    0.0046
   34   45    4   0.02   0.01   0.00   0.02         0.    0.0000
   35   46    5   1.53   0.00   0.00   1.53         0.    0.0000
   36   46    1   1.53   0.00   0.00   1.52         0.    0.0000
   37   47    0   1.68   0.00   0.00   1.68         0.    0.0000
   38   47    3   1.65   0.00   0.00   1.64         0.    0.0000
================================================================
================ TIMING STATISTICS PER NODE    ================
node# twait    ths   tcin   teig  tvect  tga   ttotal 
  1   0.00   0.00   0.02   0.00   0.00   0.02   2.37
  2   0.01   0.00   0.02   0.00   0.00   0.03   2.37
  3   0.00   0.00   0.02   0.00   0.00   0.03   2.37
  4   0.01   0.00   0.02   0.00   0.00   0.02   2.37
  5   0.00   0.00   0.02   0.00   0.00   0.03   2.37
  6   0.01   0.00   0.02   0.00   0.00   0.02   2.37
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                    14.0644s 
time spent in multnx:                  13.8376s 
integral transfer time:                 0.0197s 
time spent for loop construction:       0.6303s 
syncronization time in mult:            0.0322s 
total time per CI iteration:           14.2374s 
 ** parallelization degree for mult section:0.9977

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)           0.36        0.00          0.000
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)          0.35        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.04        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:            0.75        0.00          0.000
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)          69.78        0.08        887.479
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)         69.78        0.08        860.159
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.22        0.00        176.656
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:          139.77        0.16        868.321
========================================================


          starting ci iteration   8

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:   52161 2x:   15547 4x:    3303
All internal counts: zz :   25449 yy:  198891 xx:   22385 ww:   28177
One-external counts: yz :   82284 yx:  126866 yw:  137681
Two-external counts: yy :   68919 ww:   12352 xx:   10716 xz:    1230 wz:    1934 wx:   17724
Three-ext.   counts: yx :   18602 yw:   20150

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 354

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.94101911    -0.04255173     0.18998011

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  8  1   -155.3400721888  8.9252E-07  1.7989E-07  6.8197E-04  1.0000E-03
 mr-sdci #  8  2   -154.3334190740  1.6663E+00  0.0000E+00  7.6154E+00  1.0000E-04
 mr-sdci #  8  3   -151.9887918318 -2.2995E-01  0.0000E+00  1.4555E+01  1.0000E-04
 
 
 total energy in cosmo calc 
e(nroot) + repnuc=    -155.3400721888
dielectric energy =       0.0000000000
deltaediel =       0.0000000000
deltaelast =       0.0000000000
 

 mr-sdci  convergence criteria satisfied after  8 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  8  1   -155.3400721888  8.9252E-07  1.7989E-07  6.8197E-04  1.0000E-03
 mr-sdci #  8  2   -154.3334190740  1.6663E+00  0.0000E+00  7.6154E+00  1.0000E-04
 mr-sdci #  8  3   -151.9887918318 -2.2995E-01  0.0000E+00  1.4555E+01  1.0000E-04

####################CIUDGINFO####################

   ci vector at position   1 energy= -155.340072188840

################END OF CIUDGINFO################

 
    1 of the   4 expansion vectors are transformed.
 nnormold( 1 )= 0.922442157135331731
 nnormold( 2 )= 0.836974324808266180E-06
 nnormold( 3 )= 0.928005379491575653E-06
 nnormnew( 1 )= 0.922468418786953515
    1 of the   3 matrix-vector products are transformed.
 nnormold( 1 )= 12.3375026047234329
 nnormold( 2 )= 0.470651388466142995E-05
 nnormold( 3 )= 0.762511312801086768E-05
 nnormnew( 1 )= 12.3385700294062843

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
 maximum overlap with reference  1 (overlap= 0.941019106077081058 )

information on vector: 1from unit 11 written to unit 16filename civout                                                      


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =      -155.3400721888

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7    8    9   10   11   12   13

                                          orbital     5    6    7   33   34   35   36   37   38   65   66   76   77

                                         symmetry   ag   ag   ag   bu   bu   bu   bu   bu   bu   au   au   bg   bg 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1 -0.154339                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-           
 z*  1  1       2  0.907485                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +-      
 z*  1  1       3  0.055584                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +     - 
 z*  1  1       4 -0.054672                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-             +- 
 z*  1  1       5 -0.062757                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +-      
 z*  1  1       6 -0.104910                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +     - 
 z*  1  1       8  0.107623                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +     -    - 
 z*  1  1       9 -0.062398                        +-   +-   +-   +-   +-   +-   +-   +-   +-        +-   +-      
 z*  1  1      11  0.025271                        +-   +-   +-   +-   +-   +-   +-   +-   +-        +-        +- 
 z*  1  1      12 -0.062579                        +-   +-   +-   +-   +-   +-   +-   +-   +-             +-   +- 
 y   1  1     356  0.013193              2( au )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -           
 y   1  1     391  0.010591              1( bg )   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +     -      
 y   1  1     436 -0.013217              1( bg )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -    -      
 y   1  1     437  0.010747              2( bg )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -    -      
 y   1  1     455 -0.012956              2( au )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +          -    - 
 y   1  1     456 -0.010878              3( au )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +          -    - 
 y   1  1     482 -0.013412              2( au )   +-   +-   +-   +-   +-   +-   +-   +-   +-         -   +-      
 y   1  1     562 -0.014006              2( ag )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +     -      
 y   1  1     564 -0.012568              4( ag )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +     -      
 y   1  1     565  0.012024              5( ag )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +     -      
 y   1  1     568  0.010457              8( ag )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +     -      
 y   1  1     747  0.010248              9( bu )   +-   +-   +-   +-   +-   +-   +-   +-    -   +     -   +-      
 y   1  1    1635  0.012690              1( ag )   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +     -      
 y   1  1    1644 -0.015390             10( ag )   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +     -      
 y   1  1    1714  0.012777              4( bu )   +-   +-   +-   +-   +-   +-   +-    -   +-   +-        +     - 
 y   1  1    1715 -0.011152              5( bu )   +-   +-   +-   +-   +-   +-   +-    -   +-   +-        +     - 
 y   1  1    1720 -0.012454             10( bu )   +-   +-   +-   +-   +-   +-   +-    -   +-   +-        +     - 
 y   1  1    1816  0.010308              4( bu )   +-   +-   +-   +-   +-   +-   +-    -   +-   +     -   +-      
 y   1  1    1817 -0.012170              5( bu )   +-   +-   +-   +-   +-   +-   +-    -   +-   +     -   +-      
 y   1  1    1818 -0.010207              6( bu )   +-   +-   +-   +-   +-   +-   +-    -   +-   +     -   +-      
 y   1  1    1822 -0.013123             10( bu )   +-   +-   +-   +-   +-   +-   +-    -   +-   +     -   +-      
 y   1  1    1920 -0.010161              4( ag )   +-   +-   +-   +-   +-   +-   +-    -   +-   +         +-    - 
 y   1  1    1926  0.012803             10( ag )   +-   +-   +-   +-   +-   +-   +-    -   +-   +         +-    - 
 y   1  1    3249 -0.010838              3( bu )   +-   +-   +-   +-   +-   +-    -   +-   +-   +     -   +-      
 y   1  1    3351 -0.011094              1( ag )   +-   +-   +-   +-   +-   +-    -   +-   +-   +         +-    - 
 y   1  1    3971  0.013341              7( bu )   +-   +-   +-   +-   +-   +-   +    +-   +-   +-         -    - 
 y   1  1    4150  0.013276              6( ag )   +-   +-   +-   +-   +-   +-   +    +-   +-    -        +-    - 
 y   1  1    5048  0.010487              8( bu )   +-   +-   +-   +-   +-    -   +-   +-   +-   +     -   +-      
 y   1  1    5895  0.015777              7( ag )   +-   +-   +-   +-   +-   +    +-   +-   +-   +-    -    -      
 y   1  1    5946  0.012325              8( bu )   +-   +-   +-   +-   +-   +    +-   +-   +-   +-         -    - 
 y   1  1    6022  0.012064              8( bu )   +-   +-   +-   +-   +-   +    +-   +-   +-    -    -   +-      
 y   1  1    6125  0.011752              7( ag )   +-   +-   +-   +-   +-   +    +-   +-   +-    -        +-    - 
 y   1  1   12407  0.012091              4( bu )   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +     -      
 y   1  1   12409 -0.014133              6( bu )   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +     -      
 y   1  1   12485  0.012154              5( ag )   +-   +-    -   +-   +-   +-   +-   +-   +-   +-        +     - 
 y   1  1   12587  0.013436              5( ag )   +-   +-    -   +-   +-   +-   +-   +-   +-   +     -   +-      
 y   1  1   12687  0.013637              5( bu )   +-   +-    -   +-   +-   +-   +-   +-   +-   +         +-    - 
 y   1  1   12690 -0.010822              8( bu )   +-   +-    -   +-   +-   +-   +-   +-   +-   +         +-    - 
 y   1  1   12693  0.010771             11( bu )   +-   +-    -   +-   +-   +-   +-   +-   +-   +         +-    - 
 y   1  1   15639 -0.020581              5( bu )   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +     -      
 y   1  1   15640 -0.013180              6( bu )   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +     -      
 y   1  1   15642 -0.015237              8( bu )   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +     -      
 y   1  1   15644 -0.013068             10( bu )   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +     -      
 y   1  1   15645 -0.011111             11( bu )   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +     -      
 y   1  1   15715  0.011958              4( ag )   +-    -   +-   +-   +-   +-   +-   +-   +-   +-        +     - 
 y   1  1   15718  0.013409              7( ag )   +-    -   +-   +-   +-   +-   +-   +-   +-   +-        +     - 
 y   1  1   15721 -0.017424             10( ag )   +-    -   +-   +-   +-   +-   +-   +-   +-   +-        +     - 
 y   1  1   15817  0.010515              4( ag )   +-    -   +-   +-   +-   +-   +-   +-   +-   +     -   +-      
 y   1  1   15820  0.014265              7( ag )   +-    -   +-   +-   +-   +-   +-   +-   +-   +     -   +-      
 y   1  1   15822  0.011218              9( ag )   +-    -   +-   +-   +-   +-   +-   +-   +-   +     -   +-      
 y   1  1   15823 -0.017394             10( ag )   +-    -   +-   +-   +-   +-   +-   +-   +-   +     -   +-      
 y   1  1   15918  0.011733              5( bu )   +-    -   +-   +-   +-   +-   +-   +-   +-   +         +-    - 
 y   1  1   19226 -0.011308              1( bu )    -   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -      
 y   1  1   19228  0.011391              3( bu )    -   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -      
 y   1  1   19509 -0.013022              5( bu )    -   +-   +-   +-   +-   +-   +-   +-   +-   +         +-    - 
 y   1  1   19515 -0.010988             11( bu )    -   +-   +-   +-   +-   +-   +-   +-   +-   +         +-    - 
 x   1  1   28238 -0.010368    7( bu )   4( bg )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-         -      
 x   1  1  270287 -0.010359    7( bu )   1( au )   +-   +-    -    -   +-   +-   +-   +-   +-        +-   +     - 
 x   1  1  332645  0.011896    5( bu )   6( bu )   +-    -   +-    -   +-   +-   +-   +-   +-             +-   +- 
 x   1  1  340515  0.012391    7( bu )   3( au )   +-    -    -   +-   +-   +-   +-   +-   +-        +-   +     - 
 x   1  1  342807  0.011037    6( ag )   3( au )    -   +-   +-   +-   +-   +-   +-   +-   +-   +-              - 
 x   1  1  411184  0.010458    4( bu )   6( bu )    -   +-    -   +-   +-   +-   +-   +-   +-             +-   +- 
 w   1  1  419259 -0.011228    1( bg )   1( bg )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-                
 w   1  1  420225 -0.014646    6( ag )   7( bu )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +          -      
 w   1  1  420720  0.016355    1( au )   1( bg )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +          -      
 w   1  1  420729  0.010791    1( au )   2( bg )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +          -      
 w   1  1  424436 -0.011321    1( au )   1( au )   +-   +-   +-   +-   +-   +-   +-   +-   +-             +-      
 w   1  1  684236  0.010190    6( ag )   7( bu )   +-   +-   +-   +    +-   +-   +-    -   +-   +    +-         - 
 w   1  1  737093 -0.010260    6( bu )   6( bu )   +-   +-   +    +-   +-   +-   +-   +-    -   +-             +- 
 w   1  1  764091 -0.012929   10( ag )  10( bu )   +-   +-   +    +-   +-    -   +-   +-   +-   +    +-         - 
 w   1  1  808110 -0.010169    6( bu )   8( bu )   +-   +    +-   +-   +-   +-   +-   +-    -   +-             +- 
 w   1  1  817289 -0.010520    6( bu )   6( bu )   +-   +    +-   +-   +-   +-   +-    -   +-   +-             +- 
 w   1  1  835332  0.010481    2( ag )   3( bu )   +-   +    +-   +-   +-    -   +-   +-   +-   +    +-         - 
 w   1  1  835960  0.010964    3( au )   4( bg )   +-   +    +-   +-   +-    -   +-   +-   +-   +    +-         - 
 w   1  1  887974 -0.010186    6( ag )   7( ag )   +    +-   +-   +-   +-   +-   +-   +-    -   +-             +- 
 w   1  1  888307 -0.010176    7( bu )   8( bu )   +    +-   +-   +-   +-   +-   +-   +-    -   +-             +- 
 w   1  1  897491 -0.010128    6( bu )   7( bu )   +    +-   +-   +-   +-   +-   +-    -   +-   +-             +- 

 ci coefficient statistics:
           rq > 0.1                4
      0.1> rq > 0.01              83
     0.01> rq > 0.001          13917
    0.001> rq > 0.0001        139439
   0.0001> rq > 0.00001       354383
  0.00001> rq > 0.000001      280245
 0.000001> rq                 126686
           all                914757
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:    6308 2x:       0 4x:       0
All internal counts: zz :   25449 yy:       0 xx:       0 ww:       0
One-external counts: yz :       0 yx:       0 yw:       0
Two-external counts: yy :       0 ww:       0 xx:       0 xz:       0 wz:       0 wx:       0
Three-ext.   counts: yx :       0 yw:       0

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


  iref  icsf         v(icsf)             hv(icsf)
     1     1     -0.154338682417     23.931138376766
     2     2      0.907485453841   -140.634962294422
     3     3      0.055583790392     -8.617490595105
     4     4     -0.054671973616      8.480567154462
     5     5     -0.062757361337      9.728242090295
     6     6     -0.104909794252     16.275028213647
     7     7      0.005128193797     -0.795700468794
     8     8      0.107622694692    -16.693367283742
     9     9     -0.062397673935      9.677749376441
    10    10     -0.005078511383      0.788349069450
    11    11      0.025270563332     -3.923020061900
    12    12     -0.062579089552      9.709499189113

 number of reference csfs (nref) is    12.  root number (iroot) is  1.

 pople ci energy extrapolation is computed with 22 correlated electrons.

 eref      =   -154.980646191345   "relaxed" cnot**2         =   0.888456359204
 eci       =   -155.340072188840   deltae = eci - eref       =  -0.359425997496
 eci+dv1   =   -155.380163873198   dv1 = (1-cnot**2)*deltae  =  -0.040091684357
 eci+dv2   =   -155.385197291432   dv2 = dv1 / cnot**2       =  -0.045125102592
 eci+dv3   =   -155.391676034945   dv3 = dv1 / (2*cnot**2-1) =  -0.051603846105
 eci+pople =   -155.385717581457   ( 22e- scaled deltae )    =  -0.405071390112
NO coefficients and occupation numbers are written to nocoef_ci.1
 entering drivercid: firstcall,firstnonref= T F

################################################################################
 one electron density matrix calculation 
################################################################################


 workspace allocation information: mxcore=48285942
 
 space required:
 
    space required for calls in multd2:
       onex          832
       allin           0
       diagon       3830
    max.      ---------
       maxnex       3830
 
    total core space usage:
       maxnex       3830
    max k-seg     252844
    max k-ind       1955
              ---------
       totmax     513428
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=    3080  DYX=    4955  DYW=    5417
   D0Z=    1488  D0Y=   10060  D0X=    2506  D0W=    3050
  DDZI=    1922 DDYI=    9867 DDXI=    2640 DDWI=    3040
  DDZE=       0 DDYE=    1955 DDXE=     618 DDWE=     730
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   2.0000000      2.0000000      2.0000000      2.0000000      1.9755868
   1.9725886      1.9714671      0.0130509      0.0121593      0.0106615
   0.0096755      0.0064666      0.0047366      0.0043390      0.0025180
   0.0021240      0.0014801      0.0011070      0.0009564      0.0008857
   0.0006509      0.0005037      0.0003949      0.0003703      0.0003339
   0.0002338      0.0001862      0.0001848      0.0001313      0.0000854
   0.0000691      0.0000458


*****   symmetry block SYM2   *****

 occupation numbers of nos
   1.9998909      1.9998852      1.9855003      1.9785121      1.9757212
   1.9729405      0.0148537      0.0128140      0.0109308      0.0096199
   0.0090358      0.0066582      0.0050813      0.0037464      0.0022771
   0.0016577      0.0010808      0.0009952      0.0009274      0.0006899
   0.0006332      0.0005105      0.0003899      0.0003498      0.0003213
   0.0001913      0.0001663      0.0000989      0.0000715      0.0000652
   0.0000542      0.0000489


*****   symmetry block SYM3   *****

 occupation numbers of nos
   1.9300454      0.1029383      0.0061128      0.0048018      0.0031333
   0.0027523      0.0020210      0.0007145      0.0004841      0.0004129
   0.0002875


*****   symmetry block SYM4   *****

 occupation numbers of nos
   1.8850055      0.0548455      0.0057194      0.0040135      0.0031151
   0.0021451      0.0008902      0.0006057      0.0005395      0.0004186
   0.0002853


 total number of electrons =   30.0000000000



          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        Ag  partial gross atomic populations
   ao class       1Ag        2Ag        3Ag        4Ag        5Ag        6Ag 
    C1_ s       0.742206   0.021422   1.979297   0.509169   0.913412   0.267819
    C2_ s       0.682281   1.978433   0.020586   1.262129   0.127655   0.793905
    H1_ s       0.188687   0.000016   0.000086   0.057805   0.539271   0.150783
    H2_ s       0.300374   0.000033   0.000043   0.037214   0.363396   0.000584
    H3_ s       0.086452   0.000096  -0.000012   0.133683   0.031853   0.759498
 
   ao class       7Ag        8Ag        9Ag       10Ag       11Ag       12Ag 
    C1_ s       0.559998   0.003246   0.006412   0.003582   0.004118   0.002835
    C2_ s       1.086632   0.005591   0.001210   0.005669   0.001020   0.002788
    H1_ s       0.011346   0.000215   0.003677   0.000100   0.001336   0.000262
    H2_ s       0.255229   0.000865   0.000713   0.000073   0.002844   0.000231
    H3_ s       0.058263   0.003135   0.000147   0.001237   0.000358   0.000351
 
   ao class      13Ag       14Ag       15Ag       16Ag       17Ag       18Ag 
    C1_ s       0.001557   0.003134   0.000672   0.000509   0.000625   0.000352
    C2_ s       0.002493   0.000289   0.001271   0.000758   0.000394   0.000456
    H1_ s       0.000168   0.000380   0.000113   0.000240   0.000174   0.000043
    H2_ s       0.000047   0.000450   0.000032   0.000079   0.000201   0.000149
    H3_ s       0.000472   0.000085   0.000429   0.000537   0.000085   0.000107
 
   ao class      19Ag       20Ag       21Ag       22Ag       23Ag       24Ag 
    C1_ s       0.000128   0.000308   0.000166   0.000157   0.000125   0.000112
    C2_ s       0.000156   0.000381   0.000378   0.000220   0.000116   0.000027
    H1_ s       0.000285   0.000132   0.000052   0.000058   0.000076   0.000038
    H2_ s       0.000382   0.000037   0.000057   0.000006   0.000058   0.000128
    H3_ s       0.000007   0.000028  -0.000003   0.000063   0.000020   0.000066
 
   ao class      25Ag       26Ag       27Ag       28Ag       29Ag       30Ag 
    C1_ s       0.000052   0.000019   0.000045   0.000047   0.000015   0.000033
    C2_ s       0.000093   0.000075   0.000028   0.000031   0.000015   0.000016
    H1_ s       0.000081   0.000004   0.000038   0.000036   0.000023   0.000019
    H2_ s       0.000005   0.000025   0.000046   0.000053   0.000007   0.000011
    H3_ s       0.000103   0.000111   0.000029   0.000019   0.000071   0.000007
 
   ao class      31Ag       32Ag 
    C1_ s       0.000015   0.000005
    C2_ s       0.000003   0.000002
    H1_ s       0.000005   0.000023
    H2_ s       0.000046   0.000004
    H3_ s       0.000000   0.000011

                        Bu  partial gross atomic populations
   ao class       1Bu        2Bu        3Bu        4Bu        5Bu        6Bu 
    C1_ s       0.774189   1.226164   1.030132   0.356151   0.932613   0.671747
    C2_ s       1.226087   0.774694   0.488253   0.832811   0.331765   0.566103
    H1_ s      -0.000111  -0.000311   0.183472   0.218750   0.085637   0.447307
    H2_ s      -0.000003  -0.000389   0.250853   0.007452   0.627288   0.024989
    H3_ s      -0.000271  -0.000273   0.032790   0.563349  -0.001581   0.262794
 
   ao class       7Bu        8Bu        9Bu       10Bu       11Bu       12Bu 
    C1_ s       0.003546   0.005389   0.002823   0.003836   0.003060   0.002705
    C2_ s       0.006100   0.006425   0.003695   0.003514   0.002310   0.003235
    H1_ s       0.001817   0.000247   0.001493   0.000394   0.001407   0.000199
    H2_ s       0.000683   0.000398   0.000196   0.001702   0.001737   0.000097
    H3_ s       0.002707   0.000355   0.002723   0.000174   0.000522   0.000422
 
   ao class      13Bu       14Bu       15Bu       16Bu       17Bu       18Bu 
    C1_ s       0.002429   0.002636   0.000789   0.000434   0.000239   0.000282
    C2_ s       0.001975   0.000355   0.001014   0.000802   0.000425   0.000437
    H1_ s       0.000159   0.000439   0.000167   0.000015   0.000265   0.000015
    H2_ s       0.000214   0.000287   0.000133   0.000064   0.000125   0.000138
    H3_ s       0.000303   0.000030   0.000174   0.000343   0.000026   0.000123
 
   ao class      19Bu       20Bu       21Bu       22Bu       23Bu       24Bu 
    C1_ s       0.000191   0.000330   0.000216   0.000089   0.000031   0.000125
    C2_ s       0.000077   0.000095   0.000318   0.000270   0.000069   0.000037
    H1_ s       0.000343   0.000054   0.000030   0.000007   0.000051   0.000074
    H2_ s       0.000291   0.000056   0.000050   0.000047   0.000042   0.000111
    H3_ s       0.000025   0.000155   0.000019   0.000097   0.000198   0.000003
 
   ao class      25Bu       26Bu       27Bu       28Bu       29Bu       30Bu 
    C1_ s       0.000068   0.000034   0.000034   0.000004   0.000008   0.000017
    C2_ s       0.000077   0.000025   0.000038   0.000059   0.000000   0.000020
    H1_ s       0.000087   0.000061   0.000026   0.000003   0.000034   0.000000
    H2_ s       0.000007   0.000064   0.000050   0.000009   0.000022   0.000000
    H3_ s       0.000081   0.000007   0.000018   0.000025   0.000007   0.000028
 
   ao class      31Bu       32Bu 
    C1_ s       0.000017   0.000009
    C2_ s      -0.000001   0.000013
    H1_ s       0.000020   0.000003
    H2_ s       0.000008   0.000019
    H3_ s       0.000011   0.000005

                        Au  partial gross atomic populations
   ao class       1Au        2Au        3Au        4Au        5Au        6Au 
    C1_ s       0.678215   0.065629   0.002733   0.001724   0.001369   0.001011
    C2_ s       1.238162   0.036124   0.003174   0.002423   0.001158   0.001353
    H1_ s       0.003715   0.000455   0.000145   0.000134   0.000130   0.000182
    H2_ s       0.003280   0.000490   0.000029   0.000146   0.000345   0.000002
    H3_ s       0.006674   0.000241   0.000032   0.000375   0.000131   0.000205
 
   ao class       7Au        8Au        9Au       10Au       11Au 
    C1_ s       0.001057   0.000283   0.000030   0.000106   0.000047
    C2_ s       0.000751   0.000269   0.000041   0.000031   0.000062
    H1_ s       0.000102   0.000055   0.000011   0.000265   0.000012
    H2_ s       0.000021   0.000026   0.000256   0.000000   0.000078
    H3_ s       0.000090   0.000082   0.000145   0.000012   0.000088

                        Bg  partial gross atomic populations
   ao class       1Bg        2Bg        3Bg        4Bg        5Bg        6Bg 
    C1_ s       1.234834   0.018724   0.002894   0.003007   0.001661   0.000753
    C2_ s       0.631945   0.035374   0.002396   0.000399   0.001039   0.001021
    H1_ s       0.006961   0.000195   0.000019   0.000449   0.000126   0.000002
    H2_ s       0.007572   0.000148   0.000172   0.000137   0.000059   0.000210
    H3_ s       0.003693   0.000404   0.000237   0.000021   0.000230   0.000158
 
   ao class       7Bg        8Bg        9Bg       10Bg       11Bg 
    C1_ s       0.000044   0.000190   0.000028   0.000095   0.000064
    C2_ s       0.000711   0.000323   0.000060   0.000044   0.000042
    H1_ s       0.000133   0.000048   0.000026   0.000150   0.000057
    H2_ s       0.000000   0.000038   0.000103   0.000121   0.000077
    H3_ s       0.000002   0.000006   0.000323   0.000009   0.000046


                        gross atomic populations
     ao           C1_        C2_        H1_        H2_        H3_
      s        12.056430  12.183096   1.911099   1.893471   1.955905
    total      12.056430  12.183096   1.911099   1.893471   1.955905
 

 Total number of electrons:   30.00000000

