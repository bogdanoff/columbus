 
 program cidrt 5.9  

 distinct row table construction, reference csf selection, and internal
 walk selection for multireference single- and double-excitation
configuration interaction.

 references:  r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).
              h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. symp. 15, 91 (1981).

 based on the initial version by  Ron Shepard

 extended for spin-orbit CI calculations ( Russ Pitzer, OSU)

 and large active spaces (Thomas Müller, FZ Juelich)

 version date: 16-jul-04


 This Version of Program CIDRT is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIDRT       **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 workspace allocation parameters: lencor=  50000000 mem1=         1 ifirst=         1
 expanded "keystrokes" are being written to file:
 cidrtky                                                                         
 Spin-Orbit CI Calculation?(y,[n])
 Spin-Free Calculation
 
 input the spin multiplicity [  0]:
 spin multiplicity, smult            :   1    singlet 
 input the total number of electrons [  0]:
 total number of electrons, nelt     :    30
 input the number of irreps (1:8) [  0]:
 point group dimension, nsym         :     4
 enter symmetry labels:(y,[n])
 enter 4 labels (a4):
 enter symmetry label, default=   1
 enter symmetry label, default=   2
 enter symmetry label, default=   3
 enter symmetry label, default=   4
 symmetry labels: (symmetry, slabel)
 ( 1,  Ag ) ( 2,  Bu ) ( 3,  Au ) ( 4,  Bg ) 
 input nmpsy(*):
 nmpsy(*)=        53  53  20  20
 
   symmetry block summary
 block(*)=         1   2   3   4
 slabel(*)=      Ag  Bu  Au  Bg 
 nmpsy(*)=        53  53  20  20
 
 total molecular orbitals            :   146
 input the molecular spatial symmetry (irrep 1:nsym) [  0]:
 state spatial symmetry label        :  Ag 
 
 input the frozen core orbitals (sym(i),rmo(i),i=1,nfct):
 total frozen core orbitals, nfct    :     4
 
 fcorb(*)=         1   2   3   4
 slabel(*)=      Ag  Ag  Ag  Ag 
 
 number of frozen core orbitals      :     4
 number of frozen core electrons     :     8
 number of internal electrons        :    22
 
 input the frozen virtual orbitals (sym(i),rmo(i),i=1,nfvt):
 total frozen virtual orbitals, nfvt :     0

 no frozen virtual orbitals entered
 
 input the internal orbitals (sym(i),rmo(i),i=1,niot):
 niot                                :    13
 
 modrt(*)=         5   6   7  54  55  56  57  58  59 107 108 127 128
 slabel(*)=      Ag  Ag  Ag  Bu  Bu  Bu  Bu  Bu  Bu  Au  Au  Bg  Bg 
 
 total number of orbitals            :   146
 number of frozen core orbitals      :     4
 number of frozen virtual orbitals   :     0
 number of internal orbitals         :    13
 number of external orbitals         :   129
 
 orbital-to-level mapping vector
 map(*)=          -1  -1  -1  -1 130 131 132   1   2   3   4   5   6   7   8
                   9  10  11  12  13  14  15  16  17  18  19  20  21  22  23
                  24  25  26  27  28  29  30  31  32  33  34  35  36  37  38
                  39  40  41  42  43  44  45  46 133 134 135 136 137 138  47
                  48  49  50  51  52  53  54  55  56  57  58  59  60  61  62
                  63  64  65  66  67  68  69  70  71  72  73  74  75  76  77
                  78  79  80  81  82  83  84  85  86  87  88  89  90  91  92
                  93 139 140  94  95  96  97  98  99 100 101 102 103 104 105
                 106 107 108 109 110 111 141 142 112 113 114 115 116 117 118
                 119 120 121 122 123 124 125 126 127 128 129
 
 input the number of ref-csf doubly-occupied orbitals [  0]:
 (ref) doubly-occupied orbitals      :     6
 
 no. of internal orbitals            :    13
 no. of doubly-occ. (ref) orbitals   :     6
 no. active (ref) orbitals           :     7
 no. of active electrons             :    10
 
 input the active-orbital, active-electron occmnr(*):
  57 58 59107108127128
 input the active-orbital, active-electron occmxr(*):
  57 58 59107108127128
 
 actmo(*) =       57  58  59 107 108 127 128
 occmnr(*)=        0   0   0   0   0   0  10
 occmxr(*)=       10  10  10  10  10  10  10
 reference csf cumulative electron occupations:
 modrt(*)=         5   6   7  54  55  56  57  58  59 107 108 127 128
 occmnr(*)=        2   4   6   8  10  12  12  12  12  12  12  12  22
 occmxr(*)=        2   4   6   8  10  12  22  22  22  22  22  22  22
 
 input the active-orbital bminr(*):
  57 58 59107108127128
 input the active-orbital bmaxr(*):
  57 58 59107108127128
 reference csf b-value constraints:
 modrt(*)=         5   6   7  54  55  56  57  58  59 107 108 127 128
 bminr(*)=         0   0   0   0   0   0   0   0   0   0   0   0   0
 bmaxr(*)=         0   0   0   0   0   0   4   4   4   4   4   4   4
 input the active orbital smaskr(*):
  57 58 59107108127128
 modrt:smaskr=
   5:1000   6:1000   7:1000  54:1000  55:1000  56:1000  57:1111  58:1111
  59:1111 107:1111 108:1111 127:1111 128:1111
 
 input the maximum excitation level from the reference csfs [  2]:
 maximum excitation from ref. csfs:  :     2
 number of internal electrons:       :    22
 
 input the internal-orbital mrsdci occmin(*):
   5  6  7 54 55 56 57 58 59107108127128
 input the internal-orbital mrsdci occmax(*):
   5  6  7 54 55 56 57 58 59107108127128
 mrsdci csf cumulative electron occupations:
 modrt(*)=         5   6   7  54  55  56  57  58  59 107 108 127 128
 occmin(*)=        0   0   0   0   0   0   0   0   0   0   0   0  20
 occmax(*)=       22  22  22  22  22  22  22  22  22  22  22  22  22
 
 input the internal-orbital mrsdci bmin(*):
   5  6  7 54 55 56 57 58 59107108127128
 input the internal-orbital mrsdci bmax(*):
   5  6  7 54 55 56 57 58 59107108127128
 mrsdci b-value constraints:
 modrt(*)=         5   6   7  54  55  56  57  58  59 107 108 127 128
 bmin(*)=          0   0   0   0   0   0   0   0   0   0   0   0   0
 bmax(*)=         22  22  22  22  22  22  22  22  22  22  22  22  22
 
 input the internal-orbital smask(*):
   5  6  7 54 55 56 57 58 59107108127128
 modrt:smask=
   5:1111   6:1111   7:1111  54:1111  55:1111  56:1111  57:1111  58:1111
  59:1111 107:1111 108:1111 127:1111 128:1111
 
 internal orbital summary:
 block(*)=         1   1   1   2   2   2   2   2   2   3   3   4   4
 slabel(*)=      Ag  Ag  Ag  Bu  Bu  Bu  Bu  Bu  Bu  Au  Au  Bg  Bg 
 rmo(*)=           5   6   7   1   2   3   4   5   6   1   2   1   2
 modrt(*)=         5   6   7  54  55  56  57  58  59 107 108 127 128
 
 reference csf info:
 occmnr(*)=        2   4   6   8  10  12  12  12  12  12  12  12  22
 occmxr(*)=        2   4   6   8  10  12  22  22  22  22  22  22  22
 
 bminr(*)=         0   0   0   0   0   0   0   0   0   0   0   0   0
 bmaxr(*)=         0   0   0   0   0   0   4   4   4   4   4   4   4
 
 
 mrsdci csf info:
 occmin(*)=        0   0   0   0   0   0   0   0   0   0   0   0  20
 occmax(*)=       22  22  22  22  22  22  22  22  22  22  22  22  22
 
 bmin(*)=          0   0   0   0   0   0   0   0   0   0   0   0   0
 bmax(*)=         22  22  22  22  22  22  22  22  22  22  22  22  22
 

 a priori removal of distinct rows:

 input the level, a, and b values for the vertices 
 to be removed (-1/ to end).

 input level, a, and b (-1/ to end):
 no vertices marked for removal
 
 impose generalized interacting space restrictions?(y,[n])
 generalized interacting space restrictions will be imposed.
 multp 0 0 0 0 0 0 0 0 0
 spnir
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  hmult 0
 lxyzir 0 0 0
 spnir
  1  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  1  0  0  0  1  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  1  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  1  0  0  0  1  0  0  0  1  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  1  0  0  0  1  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0

 number of rows in the drt : 109
    11 arcs removed due to generalized interacting space restrictions.

 manual arc removal step:


 input the level, a, b, and step values 
 for the arcs to be removed (-1/ to end).

 input the level, a, b, and step (-1/ to end):
 remarc:   0 arcs removed out of   0 specified.

 xbarz=    1771
 xbary=   10073
 xbarx=   18788
 xbarw=   22946
        --------
 nwalk=   53578
 input the range of drt levels to print (l1,l2):
 levprt(*)        -1   0

 reference-csf selection step 1:
 total number of z-walks in the drt, nzwalk=    1771

 input the list of allowed reference symmetries:
 allowed reference symmetries:             1
 allowed reference symmetry labels:      Ag 
 keep all of the z-walks as references?(y,[n])
 all z-walks are initially deleted.
 
 generate walks while applying reference drt restrictions?([y],n)
 reference drt restrictions will be imposed on the z-walks.
 
 impose additional orbital-group occupation restrictions?(y,[n])
 
 apply primary reference occupation restrictions?(y,[n])
 
 manually select individual walks?(y,[n])

 step 1 reference csf selection complete.
       60 csfs initially selected from    1771 total walks.

 beginning step-vector based selection.
 enter [internal_orbital_step_vector/disposition] pairs:

 enter internal orbital step vector, (-1/ to end):
   5  6  7 54 55 56 57 58 59107108127128

 step 2 reference csf selection complete.
       60 csfs currently selected from    1771 total walks.

 beginning numerical walk based selection.
 enter positive walk numbers to add walks,
 negative walk numbers to delete walks, and zero to end:

 input reference walk number (0 to end) [  0]:

 numerical walk-number based selection complete.
       60 reference csfs selected from    1771 total z-walks.
 
 input the reference occupations, mu(*):
 reference occupations:
 mu(*)=            2   2   2   2   2   2   0   0   0   0   0   0   0
 
 interacting space determination:
 checking diagonal loops...
 checking 2-internal loops...
 checking 3-internal loops...
 checking 4-internal loops...
 !timer: limint() required                   user=     0.040 walltime=     0.000
 
 this is an obsolete prompt.(y,[n])

 final mrsdci walk selection step:

 nvalw(*)=     474    6094    3546    3820 nvalwt=   13934

 enter positive walk numbers to add walks,
 negative walk numbers to delete walks, and zero to end.

 input mrsdci walk number (0 to end) [  0]:

 end of manual mrsdci walk selection.
 number added=   0 number removed=   0

 nvalw(*)=     474    6094    3546    3820 nvalwt=   13934


 lprune: l(*,*,*) pruned with nwalk=   53578 nvalwt=   13934
 lprune:  z-drt, nprune=   164
 lprune:  y-drt, nprune=   103
 lprune: wx-drt, nprune=    85

 xbarz=     905
 xbary=    6958
 xbarx=    6456
 xbarw=    7546
        --------
 nwalk=   21865
 levprt(*)        -1   0

 beginning the reference csf index recomputation...

     iref   iwalk  step-vector
   ------  ------  ------------
        1       1  3333333333300
        2       2  3333333333030
        3       3  3333333333012
        4       4  3333333333003
        5       5  3333333331230
        6       6  3333333331212
        7       7  3333333331203
        8       8  3333333331122
        9       9  3333333330330
       10      10  3333333330312
       11      11  3333333330303
       12      14  3333333330033
       13      15  3333333303330
       14      16  3333333303312
       15      17  3333333303303
       16      20  3333333303033
       17      23  3333333301233
       18      24  3333333300333
       19      25  3333333123330
       20      26  3333333123312
       21      27  3333333123303
       22      30  3333333123033
       23      33  3333333121233
       24      34  3333333120333
       25      35  3333333113322
       26      40  3333333112233
       27      41  3333333033330
       28      42  3333333033312
       29      43  3333333033303
       30      46  3333333033033
       31      49  3333333031233
       32      50  3333333030333
       33      51  3333333003333
       34      52  3333331323330
       35      53  3333331323312
       36      54  3333331323303
       37      57  3333331323033
       38      60  3333331321233
       39      61  3333331320333
       40      62  3333331313322
       41      67  3333331312233
       42      68  3333331233330
       43      69  3333331233312
       44      70  3333331233303
       45      73  3333331233033
       46      76  3333331231233
       47      77  3333331230333
       48      78  3333331203333
       49      79  3333331133322
       50      84  3333331132233
       51      85  3333331023333
       52      86  3333330333330
       53      87  3333330333312
       54      88  3333330333303
       55      91  3333330333033
       56      94  3333330331233
       57      95  3333330330333
       58      96  3333330303333
       59      97  3333330123333
       60      98  3333330033333
 indx01:    60 elements set in vec01(*)

 beginning the valid upper walk index recomputation...
 indx01: 13934 elements set in vec01(*)

 beginning the final csym(*) computation...

  number of valid internal walks of each symmetry:

       Ag      Bu      Au      Bg 
      ----    ----    ----    ----
 z     474       0       0       0
 y    1310    1320    1732    1732
 x     815     991     870     870
 w    1177     955     844     844

 csfs grouped by internal walk symmetry:

       Ag      Bu      Au      Bg 
      ----    ----    ----    ----
 z     474       0       0       0
 y   60260   62040   31176   31176
 x 1973930 2463626 1456380 1456380
 w 3002527 2374130 1412856 1412856

 total csf counts:
 z-vertex:      474
 y-vertex:   184652
 x-vertex:  7350316
 w-vertex:  8202369
           --------
 total:    15737811
 
 input a title card, default=cidrt_title
 title card:
  cidrt_title                                                                    
 
 input a drt file name, default=cidrtfl
 drt and indexing arrays will be written to file:
 cidrtfl                                                                         
 
 write the drt file?([y],n)
 drt file is being written...
 !timer: cidrt required                      user=     0.050 walltime=     0.000
