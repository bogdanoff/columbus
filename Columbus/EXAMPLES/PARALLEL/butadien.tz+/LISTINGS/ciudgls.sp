1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      09/20/00    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 using llenci= -1
================================================================================
four external integ   76.75 MB location: distr. (dual) 
three external inte   31.25 MB location: distr. (dual) 
four external integ   74.00 MB location: distr. (dual) 
three external inte   30.75 MB location: distr. (dual) 
diagonal integrals     0.25 MB location: virtual disk  
off-diagonal integr    5.00 MB location: virtual disk  
computed file size in DP units
fil3w:     4095875
fil3x:     4030341
fil4w:    10059469
fil4x:     9699032
ofdgint:       32760
diagint:      655200
computed file size in DP units
fil3w:    32767000
fil3x:    32242728
fil4w:    80475752
fil4x:    77592256
ofdgint:      262080
diagint:     5241600
 nsubmx= 6  lenci= 15737811
global arrays:     ********   (           1560.91 MB)
vdisk:               687960   (              5.25 MB)
drt:                1721538   (              6.57 MB)
================================================================================
 Main memory management:
 global         34242059 DP per process
 vdisk            687960 DP per process
 stack                 0 DP per process
 core           15069981 DP per process
Array Handle=-1000 Name:'civect' Data Type:double
Array Dimensions:15737811x6
Process=0	 owns array section: [1:2622969,1:6] 
Process=1	 owns array section: [2622970:5245938,1:6] 
Process=2	 owns array section: [5245939:7868907,1:6] 
Process=3	 owns array section: [7868908:10491876,1:6] 
Process=4	 owns array section: [10491877:13114845,1:6] 
Process=5	 owns array section: [13114846:15737811,1:6] 
Array Handle=-999 Name:'sigvec' Data Type:double
Array Dimensions:15737811x6
Process=0	 owns array section: [1:2622969,1:6] 
Process=1	 owns array section: [2622970:5245938,1:6] 
Process=2	 owns array section: [5245939:7868907,1:6] 
Process=3	 owns array section: [7868908:10491876,1:6] 
Process=4	 owns array section: [10491877:13114845,1:6] 
Process=5	 owns array section: [13114846:15737811,1:6] 
Array Handle=-998 Name:'hdg' Data Type:double
Array Dimensions:15737811x1
Process=0	 owns array section: [1:2622969,1:1] 
Process=1	 owns array section: [2622970:5245938,1:1] 
Process=2	 owns array section: [5245939:7868907,1:1] 
Process=3	 owns array section: [7868908:10491876,1:1] 
Process=4	 owns array section: [10491877:13114845,1:1] 
Process=5	 owns array section: [13114846:15737811,1:1] 
Array Handle=-997 Name:'drt' Data Type:double
Array Dimensions:122967x7
Process=0	 owns array section: [1:20495,1:7] 
Process=1	 owns array section: [20496:40990,1:7] 
Process=2	 owns array section: [40991:61485,1:7] 
Process=3	 owns array section: [61486:81980,1:7] 
Process=4	 owns array section: [81981:102475,1:7] 
Process=5	 owns array section: [102476:122967,1:7] 
Array Handle=-996 Name:'nxttask' Data Type:integer
Array Dimensions:1x1
Process=0	 owns array section: [1:1,1:1] 
Process=1	 owns array section: [0:-1,0:-1] 
Process=2	 owns array section: [0:-1,0:-1] 
Process=3	 owns array section: [0:-1,0:-1] 
Process=4	 owns array section: [0:-1,0:-1] 
Process=5	 owns array section: [0:-1,0:-1] 
 CIUDG version 5.9.7 ( 5-Oct-2004)
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 0,
  GSET = 0,
   DAVCOR =10,
  NCOREL = 22
  NROOT = 1
  IVMODE = 3
  NBKITR = 1
  NVBKMN = 1
  NVBKMX = 6
  NVRFMX = 6
  RTOLBK = 1e-3,1e-3,
  NITER =  10
  NVCIMN = 1
  NVCIMX =  6
  RTOLCI = 1e-3,1e-3,
  IDEN  = 1
  CSFPRN = 10,
  FTCALC = 1
  MAXSEG = 20
  nsegd = 1,1,2,2
  nseg0x = 1,1,2,2
  nseg1x = 1,1,2,2
  nseg2x = 1,1,2,2
  nseg3x = 1,1,2,2
  nseg4x = 1,1,2,2,
  c2ex0ex=0
  c3ex1ex=0
  cdg4ex=1
  fileloc=1,1,4,4,4,4
  &end
 ------------------------------------------------------------------------

 ** list of control variables **
 nrfitr =   30      nvrfmx =    6      nvrfmn =    1
 lvlprt =    0      nroot  =    1      noldv  =    0      noldhv =    0
 nunitv =    1      ntype  =    0      nbkitr =    1      niter  =   10
 ivmode =    3      vout   =    0      istrt  =    0      iortls =    0
 nvbkmx =    6      ibktv  =   -1      ibkthv =   -1      frcsub =    0
 nvcimx =    6      icitv  =   -1      icithv =   -1      maxseg =   20
 iden   =    1      itran  =    0      froot  =    0      rtmode =    0
 ftcalc =    1      lrtshift=1.0000    ncouple=    1      skipso  =   F
 ncorel =   22      csfprn  =  10      ctol   = 1.00E-02  davcor  =  10


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-03    1.000E-03
 
 units and filenames:
    1: ( 6)    ciudgls                                                     
    2: ( 5)    ciudgin                                                     
    3: ( 7)    ciudgsm                                                     
    4: ( 4)    cihdiag                                                     
    5: ( 3)    cihpseu                                                     
    6: (10)    cihvfl                                                      
    7: (11)    civfl                                                       
    8: (12)    diagint                                                     
    9: (13)    ofdgint                                                     
   10: (17)    cidrtfl                                                     
   11: (20)    ciftdfl                                                     
   12: (21)    ciftofl                                                     
   13: (24)    ciftifl                                                     
   14: ( 8)    ciflind                                                     
   15: (15)    civin                                                       
   16: (16)    civout                                                      
   18: (18)    d1fl                                                        
   20: (53)    cifvfl                                                      
   21: (25)    ciscr4                                                      
   22: (41)    ciftotd                                                     
   23: (42)    flacpfd                                                     
   26: (26)    ciscr5                                                      
   27: (47)    restart                                                     
   28: (48)    civout                                                      
   29: (49)    cirefv                                                      
   31: (31)    fil4w                                                       
   32: (32)    fil4x                                                       
   33: (33)    fil3w                                                       
   34: (34)    fil3x                                                       
   36: (36)    aoints                                                      
   37: (37)    aoints2                                                     
   38: (38)    drtfil                                                      
   39: (50)    cisrtif                                                     
   42: (50)    mocoef                                                      
   45: (52)    nocoef_ci                                                   
   46: (53)    civfl_restart                                               
 ------------------------------------------------------------------------
 broadcasting INDATA    
 broadcasting ACPFINFO  
 broadcasting CFILES    

 workspace allocation information: lcore=  15069981 mem1=         1 ifirst=         1

 integral file titles:
 Hermit Integral Program : SIFS version  j25             Tue Oct 19 14:03:34 2004
  cidrt_title                                                                    
 Hermit Integral Program : SIFS version  grimming        Tue Aug  7 18:24:26 2001
  title                                                                          
 mofmt: formatted orbitals label=morb    grimming        Tue Aug  7 18:38:16 2001
 SIFS file created by program tran.      j25             Tue Oct 19 14:03:45 2004

 core energy values from the integral file:
 energy( 1)=  1.034429979483E+02, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -1.274375457176E+02, ietype=    6,   fcore energy of type: H1(*)   
 energy( 3)= -1.129762209512E+02, ietype=    5,   fcore energy of type: Vref(*) 

 total core repulsion energy = -1.369707687204E+02

 drt header information:
  cidrt_title                                                                    
 spnorb, spnodd, lxyzir,hmult F F 0 0 0 0
 nmot  =   146 niot  =    13 nfct  =     4 nfvt  =     0
 nrow  =   109 nsym  =     4 ssym  =     1 lenbuf=  1600
 nwalk,xbar:      21865      905     6958     6456     7546
 nvalwt,nvalw:    13934      474     6094     3546     3820
 ncsft:        15737811
 total number of valid internal walks:   13934
 nvalz,nvaly,nvalx,nvalw =      474    6094    3546    3820

 cisrt info file parameters:
 file number  12 blocksize   4095
 mxbld  20475
 nd4ext,nd2ext,nd0ext 16770  3354   182
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int  9468510  3698334   597417    46410     1461        0        0        0
 minbl4,minbl3,maxbl2  7650  7650  7653
 maxbuf 32767
 number of external orbitals per symmetry block:  46  47  18  18
 nmsym   4 number of internal orbitals  13

 formula file title:
 Hermit Integral Program : SIFS version  j25             Tue Oct 19 14:03:34 2004
  cidrt_title                                                                    
 Hermit Integral Program : SIFS version  grimming        Tue Aug  7 18:24:26 2001
  title                                                                          
 mofmt: formatted orbitals label=morb    grimming        Tue Aug  7 18:38:16 2001
 SIFS file created by program tran.      j25             Tue Oct 19 14:03:45 2004
  cidrt_title                                                                    
 file nmb.  24 block size     0
 pthz,pthy,pthx,pthw:   905  6958  6456  7546 total internal walks:   21865
 maxlp3,n3xtlp,n2xtlp,n1xtlp,n0xtlp,n2lp,n1lp,n0lp     0     0     0     0
 orbsym(*)= 1 1 1 2 2 2 2 2 2 3 3 4 4

 setref:       60 references kept,
                0 references were marked as invalid, out of
               60 total.
 limcnvrt: found 474  valid internal walksout of  905  walks (skipping trailing invalids)
  ... adding  474  segmentation marks segtype= 1
 limcnvrt: found 6094  valid internal walksout of  6958  walks (skipping trailing invalids)
  ... adding  6094  segmentation marks segtype= 2
 limcnvrt: found 3546  valid internal walksout of  6456  walks (skipping trailing invalids)
  ... adding  3546  segmentation marks segtype= 3
 limcnvrt: found 3820  valid internal walksout of  7546  walks (skipping trailing invalids)
  ... adding  3820  segmentation marks segtype= 4
 broadcasting SOLXYZ    
 broadcasting INDATA    
 broadcasting REPNUC    
 broadcasting INF       
 broadcasting SOINF     
 broadcasting DATA      
 broadcasting MOMAP     
 broadcasting CLI       
 broadcasting CSYM      
 broadcasting SLABEL    
 broadcasting MOMAP     
 broadcasting DRT       
 broadcasting INF1      
 broadcasting DRTINFO   
loaded      142 onel-dg  integrals(   1records ) at       1 on virtual disk
loaded    16770 4ext-dg  integrals(   5records ) at    4096 on virtual disk
loaded     3354 2ext-dg  integrals(   1records ) at   24571 on virtual disk
loaded      182 0ext-dg  integrals(   1records ) at   28666 on virtual disk
loaded     2551 onel-of  integrals(   1records ) at   32761 on virtual disk
loaded   597417 2ext-og  integrals( 146records ) at   36856 on virtual disk
loaded    46410 1ext-og  integrals(  12records ) at  634726 on virtual disk
loaded     1461 0ext-og  integrals(   1records ) at  683866 on virtual disk

 number of external paths / symmetry
 vertex x    2422    2486    1674    1674
 vertex w    2551    2486    1674    1674

 lprune: l(*,*,*) pruned with nwalk=     905 nvalwt=     474 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    6958 nvalwt=    6094 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    2603 nvalwt=    1774 nprune=      44

 lprune: l(*,*,*) pruned with nwalk=    3790 nvalwt=    1749 nprune=      66

 lprune: l(*,*,*) pruned with nwalk=      63 nvalwt=      23 nprune=     148

 lprune: l(*,*,*) pruned with nwalk=    2891 nvalwt=    1802 nprune=      52

 lprune: l(*,*,*) pruned with nwalk=    3747 nvalwt=    1738 nprune=      64

 lprune: l(*,*,*) pruned with nwalk=     908 nvalwt=     280 nprune=     126



                   segmentation summary for type diagonal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1         905|       474|         0|       474|         0|         1|
 -------------------------------------------------------------------------------
  Y 2        6958|    184652|       474|      6094|       474|      1097|
 -------------------------------------------------------------------------------
  X 3        2599|   3546860|    185126|      1774|      6568|      8007|
 -------------------------------------------------------------------------------
  X 4        3653|   3747750|   3731986|      1749|      8342|     10498|
 -------------------------------------------------------------------------------
  X 5          23|     55706|   7479736|        23|     10091|     13209|
 -------------------------------------------------------------------------------
  W 6        2891|   3746728|   7535442|      1802|     10114|     13724|
 -------------------------------------------------------------------------------
  W 7        3612|   3747861|  11282170|      1738|     11916|     16295|
 -------------------------------------------------------------------------------
  W 8         280|    707780|  15030031|       280|     13654|     18990|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>  15737811


 lprune: l(*,*,*) pruned with nwalk=     905 nvalwt=     474 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    6958 nvalwt=    6094 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    2603 nvalwt=    1774 nprune=      44

 lprune: l(*,*,*) pruned with nwalk=    3790 nvalwt=    1749 nprune=      66

 lprune: l(*,*,*) pruned with nwalk=      63 nvalwt=      23 nprune=     148

 lprune: l(*,*,*) pruned with nwalk=    2891 nvalwt=    1802 nprune=      52

 lprune: l(*,*,*) pruned with nwalk=    3747 nvalwt=    1738 nprune=      64

 lprune: l(*,*,*) pruned with nwalk=     908 nvalwt=     280 nprune=     126



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1         905|       474|         0|       474|         0|         1|
 -------------------------------------------------------------------------------
  Y 2        6958|    184652|       474|      6094|       474|      1097|
 -------------------------------------------------------------------------------
  X 3        2599|   3546860|    185126|      1774|      6568|      8007|
 -------------------------------------------------------------------------------
  X 4        3653|   3747750|   3731986|      1749|      8342|     10498|
 -------------------------------------------------------------------------------
  X 5          23|     55706|   7479736|        23|     10091|     13209|
 -------------------------------------------------------------------------------
  W 6        2891|   3746728|   7535442|      1802|     10114|     13724|
 -------------------------------------------------------------------------------
  W 7        3612|   3747861|  11282170|      1738|     11916|     16295|
 -------------------------------------------------------------------------------
  W 8         280|    707780|  15030031|       280|     13654|     18990|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>  15737811


 lprune: l(*,*,*) pruned with nwalk=     905 nvalwt=     474 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    6958 nvalwt=    6094 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    2603 nvalwt=    1774 nprune=      44

 lprune: l(*,*,*) pruned with nwalk=    3790 nvalwt=    1749 nprune=      66

 lprune: l(*,*,*) pruned with nwalk=      63 nvalwt=      23 nprune=     148

 lprune: l(*,*,*) pruned with nwalk=    2891 nvalwt=    1802 nprune=      52

 lprune: l(*,*,*) pruned with nwalk=    3747 nvalwt=    1738 nprune=      64

 lprune: l(*,*,*) pruned with nwalk=     908 nvalwt=     280 nprune=     126



                   segmentation summary for type one-external
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1         905|       474|         0|       474|         0|         1|
 -------------------------------------------------------------------------------
  Y 2        6958|    184652|       474|      6094|       474|      1097|
 -------------------------------------------------------------------------------
  X 3        2599|   3546860|    185126|      1774|      6568|      8007|
 -------------------------------------------------------------------------------
  X 4        3653|   3747750|   3731986|      1749|      8342|     10498|
 -------------------------------------------------------------------------------
  X 5          23|     55706|   7479736|        23|     10091|     13209|
 -------------------------------------------------------------------------------
  W 6        2891|   3746728|   7535442|      1802|     10114|     13724|
 -------------------------------------------------------------------------------
  W 7        3612|   3747861|  11282170|      1738|     11916|     16295|
 -------------------------------------------------------------------------------
  W 8         280|    707780|  15030031|       280|     13654|     18990|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>  15737811


 lprune: l(*,*,*) pruned with nwalk=     905 nvalwt=     474 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    6958 nvalwt=    6094 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    2603 nvalwt=    1774 nprune=      44

 lprune: l(*,*,*) pruned with nwalk=    3790 nvalwt=    1749 nprune=      66

 lprune: l(*,*,*) pruned with nwalk=      63 nvalwt=      23 nprune=     148

 lprune: l(*,*,*) pruned with nwalk=    2891 nvalwt=    1802 nprune=      52

 lprune: l(*,*,*) pruned with nwalk=    3747 nvalwt=    1738 nprune=      64

 lprune: l(*,*,*) pruned with nwalk=     908 nvalwt=     280 nprune=     126



                   segmentation summary for type two-external
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1         905|       474|         0|       474|         0|         1|
 -------------------------------------------------------------------------------
  Y 2        6958|    184652|       474|      6094|       474|      1097|
 -------------------------------------------------------------------------------
  X 3        2599|   3546860|    185126|      1774|      6568|      8007|
 -------------------------------------------------------------------------------
  X 4        3653|   3747750|   3731986|      1749|      8342|     10498|
 -------------------------------------------------------------------------------
  X 5          23|     55706|   7479736|        23|     10091|     13209|
 -------------------------------------------------------------------------------
  W 6        2891|   3746728|   7535442|      1802|     10114|     13724|
 -------------------------------------------------------------------------------
  W 7        3612|   3747861|  11282170|      1738|     11916|     16295|
 -------------------------------------------------------------------------------
  W 8         280|    707780|  15030031|       280|     13654|     18990|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>  15737811


 lprune: l(*,*,*) pruned with nwalk=     905 nvalwt=     474 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    6958 nvalwt=    6094 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    2603 nvalwt=    1774 nprune=      44

 lprune: l(*,*,*) pruned with nwalk=    3790 nvalwt=    1749 nprune=      66

 lprune: l(*,*,*) pruned with nwalk=      63 nvalwt=      23 nprune=     148

 lprune: l(*,*,*) pruned with nwalk=    2891 nvalwt=    1802 nprune=      52

 lprune: l(*,*,*) pruned with nwalk=    3747 nvalwt=    1738 nprune=      64

 lprune: l(*,*,*) pruned with nwalk=     908 nvalwt=     280 nprune=     126



                   segmentation summary for type three-external
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1         905|       474|         0|       474|         0|         1|
 -------------------------------------------------------------------------------
  Y 2        6958|    184652|       474|      6094|       474|      1097|
 -------------------------------------------------------------------------------
  X 3        2599|   3546860|    185126|      1774|      6568|      8007|
 -------------------------------------------------------------------------------
  X 4        3653|   3747750|   3731986|      1749|      8342|     10498|
 -------------------------------------------------------------------------------
  X 5          23|     55706|   7479736|        23|     10091|     13209|
 -------------------------------------------------------------------------------
  W 6        2891|   3746728|   7535442|      1802|     10114|     13724|
 -------------------------------------------------------------------------------
  W 7        3612|   3747861|  11282170|      1738|     11916|     16295|
 -------------------------------------------------------------------------------
  W 8         280|    707780|  15030031|       280|     13654|     18990|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>  15737811


 lprune: l(*,*,*) pruned with nwalk=     905 nvalwt=     474 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    6958 nvalwt=    6094 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    2603 nvalwt=    1774 nprune=      44

 lprune: l(*,*,*) pruned with nwalk=    3790 nvalwt=    1749 nprune=      66

 lprune: l(*,*,*) pruned with nwalk=      63 nvalwt=      23 nprune=     148

 lprune: l(*,*,*) pruned with nwalk=    2891 nvalwt=    1802 nprune=      52

 lprune: l(*,*,*) pruned with nwalk=    3747 nvalwt=    1738 nprune=      64

 lprune: l(*,*,*) pruned with nwalk=     908 nvalwt=     280 nprune=     126



                   segmentation summary for type four-external
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1         905|       474|         0|       474|         0|         1|
 -------------------------------------------------------------------------------
  Y 2        6958|    184652|       474|      6094|       474|      1097|
 -------------------------------------------------------------------------------
  X 3        2599|   3546860|    185126|      1774|      6568|      8007|
 -------------------------------------------------------------------------------
  X 4        3653|   3747750|   3731986|      1749|      8342|     10498|
 -------------------------------------------------------------------------------
  X 5          23|     55706|   7479736|        23|     10091|     13209|
 -------------------------------------------------------------------------------
  W 6        2891|   3746728|   7535442|      1802|     10114|     13724|
 -------------------------------------------------------------------------------
  W 7        3612|   3747861|  11282170|      1738|     11916|     16295|
 -------------------------------------------------------------------------------
  W 8         280|    707780|  15030031|       280|     13654|     18990|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>  15737811

 broadcasting CIVCT     
 broadcasting DATA      
 broadcasting INF       
 broadcasting CNFSPC    
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS
----------------------------------------------------------------------------------------------------
     1  2   2    21      two-ext yy   2X  2 2    6958    6958     184652     184652    6094    6094
     2  3   3    22      two-ext xx   2X  3 3    2599    2599    3546860    3546860    1774    1774
     3  4   3    22      two-ext xx   2X  3 3    3653    2599    3747750    3546860    1749    1774
     4  5   3    22      two-ext xx   2X  3 3      23    2599      55706    3546860      23    1774
     5  4   4    22      two-ext xx   2X  3 3    3653    3653    3747750    3747750    1749    1749
     6  5   4    22      two-ext xx   2X  3 3      23    3653      55706    3747750      23    1749
     7  5   5    22      two-ext xx   2X  3 3      23      23      55706      55706      23      23
     8  6   6    23      two-ext ww   2X  4 4    2891    2891    3746728    3746728    1802    1802
     9  7   6    23      two-ext ww   2X  4 4    3612    2891    3747861    3746728    1738    1802
    10  8   6    23      two-ext ww   2X  4 4     280    2891     707780    3746728     280    1802
    11  7   7    23      two-ext ww   2X  4 4    3612    3612    3747861    3747861    1738    1738
    12  8   7    23      two-ext ww   2X  4 4     280    3612     707780    3747861     280    1738
    13  8   8    23      two-ext ww   2X  4 4     280     280     707780     707780     280     280
    14  3   1    24      two-ext xz   2X  3 1    2599     905    3546860        474    1774     474
    15  4   1    24      two-ext xz   2X  3 1    3653     905    3747750        474    1749     474
    16  5   1    24      two-ext xz   2X  3 1      23     905      55706        474      23     474
    17  6   1    25      two-ext wz   2X  4 1    2891     905    3746728        474    1802     474
    18  7   1    25      two-ext wz   2X  4 1    3612     905    3747861        474    1738     474
    19  8   1    25      two-ext wz   2X  4 1     280     905     707780        474     280     474
    20  6   3    26      two-ext wx   2X  4 3    2891    2599    3746728    3546860    1802    1774
    21  7   3    26      two-ext wx   2X  4 3    3612    2599    3747861    3546860    1738    1774
    22  8   3    26      two-ext wx   2X  4 3     280    2599     707780    3546860     280    1774
    23  6   4    26      two-ext wx   2X  4 3    2891    3653    3746728    3747750    1802    1749
    24  7   4    26      two-ext wx   2X  4 3    3612    3653    3747861    3747750    1738    1749
    25  8   4    26      two-ext wx   2X  4 3     280    3653     707780    3747750     280    1749
    26  6   5    26      two-ext wx   2X  4 3    2891      23    3746728      55706    1802      23
    27  7   5    26      two-ext wx   2X  4 3    3612      23    3747861      55706    1738      23
    28  8   5    26      two-ext wx   2X  4 3     280      23     707780      55706     280      23
    29  2   1    11      one-ext yz   1X  2 1    6958     905     184652        474    6094     474
    30  3   2    13      one-ext yx   1X  3 2    2599    6958    3546860     184652    1774    6094
    31  4   2    13      one-ext yx   1X  3 2    3653    6958    3747750     184652    1749    6094
    32  5   2    13      one-ext yx   1X  3 2      23    6958      55706     184652      23    6094
    33  6   2    14      one-ext yw   1X  4 2    2891    6958    3746728     184652    1802    6094
    34  7   2    14      one-ext yw   1X  4 2    3612    6958    3747861     184652    1738    6094
    35  8   2    14      one-ext yw   1X  4 2     280    6958     707780     184652     280    6094
    36  3   2    31      thr-ext yx   3X  3 2    2599    6958    3546860     184652    1774    6094
    37  4   2    31      thr-ext yx   3X  3 2    3653    6958    3747750     184652    1749    6094
    38  5   2    31      thr-ext yx   3X  3 2      23    6958      55706     184652      23    6094
    39  6   2    32      thr-ext yw   3X  4 2    2891    6958    3746728     184652    1802    6094
    40  7   2    32      thr-ext yw   3X  4 2    3612    6958    3747861     184652    1738    6094
    41  8   2    32      thr-ext yw   3X  4 2     280    6958     707780     184652     280    6094
    42  1   1     1      allint zz    OX  1 1     905     905        474        474     474     474
    43  2   2     2      allint yy    OX  2 2    6958    6958     184652     184652    6094    6094
    44  3   3     3      allint xx    OX  3 3    2599    2599    3546860    3546860    1774    1774
    45  4   3     3      allint xx    OX  3 3    3653    2599    3747750    3546860    1749    1774
    46  5   3     3      allint xx    OX  3 3      23    2599      55706    3546860      23    1774
    47  4   4     3      allint xx    OX  3 3    3653    3653    3747750    3747750    1749    1749
    48  5   4     3      allint xx    OX  3 3      23    3653      55706    3747750      23    1749
    49  5   5     3      allint xx    OX  3 3      23      23      55706      55706      23      23
    50  6   6     4      allint ww    OX  4 4    2891    2891    3746728    3746728    1802    1802
    51  7   6     4      allint ww    OX  4 4    3612    2891    3747861    3746728    1738    1802
    52  8   6     4      allint ww    OX  4 4     280    2891     707780    3746728     280    1802
    53  7   7     4      allint ww    OX  4 4    3612    3612    3747861    3747861    1738    1738
    54  8   7     4      allint ww    OX  4 4     280    3612     707780    3747861     280    1738
    55  8   8     4      allint ww    OX  4 4     280     280     707780     707780     280     280
    56  1   1    75      dg-024ext z  DG  1 1     905     905        474        474     474     474
    57  2   2    45      4exdg024 y   DG  2 2    6958    6958     184652     184652    6094    6094
    58  3   3    46      4exdg024 x   DG  3 3    2599    2599    3546860    3546860    1774    1774
    59  4   4    46      4exdg024 x   DG  3 3    3653    3653    3747750    3747750    1749    1749
    60  5   5    46      4exdg024 x   DG  3 3      23      23      55706      55706      23      23
    61  6   6    47      4exdg024 w   DG  4 4    2891    2891    3746728    3746728    1802    1802
    62  7   7    47      4exdg024 w   DG  4 4    3612    3612    3747861    3747861    1738    1738
    63  8   8    47      4exdg024 w   DG  4 4     280     280     707780     707780     280     280
----------------------------------------------------------------------------------------------------
 broadcasting TASKLST   
 DIAGEL COUNTS: DG0X,DG2X,DG4X= 435270 98716 13460
 diagonal elements written to file   4

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:   16556 2x:       0 4x:       0
All internal counts: zz :   39465 yy:       0 xx:       0 ww:       0
One-external counts: yz :       0 yx:       0 yw:       0
Two-external counts: yy :       0 ww:       0 xx:       0 xz:       0 wz:       0 wx:       0
Three-ext.   counts: yx :       0 yw:       0

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 reference space has dimension      60

    root           eigenvalues
    ----           ------------
       1        -155.0177463101

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt= 474

         vector  1 from unit 11 written to unit 49 filename cirefv                                                      
 ufvoutnew: ... writing  recamt= 474

         vector  1 from unit 11 written to unit 49 filename cirefv                                                      

 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=   474)...
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:          15737811
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                    1
 residual norm convergence criteria:               0.001000

          starting bk iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:  435270 2x:   98716 4x:   13460
All internal counts: zz :   39465 yy:       0 xx:       0 ww:       0
One-external counts: yz :  232066 yx:       0 yw:       0
Two-external counts: yy :       0 ww:       0 xx:       0 xz:   10881 wz:   12834 wx:       0
Three-ext.   counts: yx :       0 yw:       0

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 474

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -155.0177463101 -2.8422E-14  4.7796E-01  1.3809E+00  1.0000E-03
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   21    0   0.00   0.00   0.00   0.00         0.    0.0000
    2   22    0   0.00   0.00   0.00   0.00         0.    0.0000
    3   22    0   0.00   0.00   0.00   0.00         0.    0.0000
    4   22    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   22    0   0.00   0.00   0.00   0.00         0.    0.0000
    6   22    0   0.00   0.00   0.00   0.00         0.    0.0000
    7   22    0   0.00   0.00   0.00   0.00         0.    0.0000
    8   23    0   0.00   0.00   0.00   0.00         0.    0.0000
    9   23    0   0.00   0.00   0.00   0.00         0.    0.0000
   10   23    0   0.00   0.00   0.00   0.00         0.    0.0000
   11   23    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   23    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   23    0   0.00   0.00   0.00   0.00         0.    0.0000
   14   24    0   0.16   0.00   0.00   0.08         0.    0.0038
   15   24    0   0.15   0.00   0.00   0.08         0.    0.0032
   16   24    0   0.01   0.00   0.00   0.01         0.    0.0000
   17   25    0   0.18   0.00   0.00   0.11         0.    0.0041
   18   25    5   0.28   0.00   0.00   0.11         0.    0.0034
   19   25    1   0.04   0.00   0.00   0.02         0.    0.0004
   20   26    0   0.00   0.00   0.00   0.00         0.    0.0000
   21   26    0   0.00   0.00   0.00   0.00         0.    0.0000
   22   26    0   0.00   0.00   0.00   0.00         0.    0.0000
   23   26    0   0.00   0.00   0.00   0.00         0.    0.0000
   24   26    0   0.00   0.00   0.00   0.00         0.    0.0000
   25   26    0   0.00   0.00   0.00   0.00         0.    0.0000
   26   26    0   0.00   0.00   0.00   0.00         0.    0.0000
   27   26    0   0.00   0.00   0.00   0.00         0.    0.0000
   28   26    0   0.00   0.00   0.00   0.00         0.    0.0000
   29   11    3   0.46   0.26   0.00   0.45         0.    0.1883
   30   13    0   0.00   0.00   0.00   0.00         0.    0.0000
   31   13    0   0.00   0.00   0.00   0.00         0.    0.0000
   32   13    0   0.00   0.00   0.00   0.00         0.    0.0000
   33   14    0   0.00   0.00   0.00   0.00         0.    0.0000
   34   14    0   0.00   0.00   0.00   0.00         0.    0.0000
   35   14    0   0.00   0.00   0.00   0.00         0.    0.0000
   36   31    0   0.00   0.00   0.00   0.00         0.    0.0000
   37   31    0   0.00   0.00   0.00   0.00         0.    0.0000
   38   31    0   0.00   0.00   0.00   0.00         0.    0.0000
   39   32    0   0.00   0.00   0.00   0.00         0.    0.0000
   40   32    0   0.00   0.00   0.00   0.00         0.    0.0000
   41   32    0   0.00   0.00   0.00   0.00         0.    0.0000
   42    1    2   0.08   0.06   0.00   0.08         0.    0.0306
   43    2    0   0.00   0.00   0.00   0.00         0.    0.0000
   44    3    0   0.00   0.00   0.00   0.00         0.    0.0000
   45    3    0   0.00   0.00   0.00   0.00         0.    0.0000
   46    3    0   0.00   0.00   0.00   0.00         0.    0.0000
   47    3    0   0.00   0.00   0.00   0.00         0.    0.0000
   48    3    0   0.00   0.00   0.00   0.00         0.    0.0000
   49    3    0   0.00   0.00   0.00   0.00         0.    0.0000
   50    4    0   0.00   0.00   0.00   0.00         0.    0.0000
   51    4    0   0.00   0.00   0.00   0.00         0.    0.0000
   52    4    0   0.00   0.00   0.00   0.00         0.    0.0000
   53    4    0   0.00   0.00   0.00   0.00         0.    0.0000
   54    4    0   0.00   0.00   0.00   0.00         0.    0.0000
   55    4    0   0.00   0.00   0.00   0.00         0.    0.0000
   56   75    4   0.01   0.01   0.00   0.01         0.    0.0114
   57   45    4   0.14   0.08   0.00   0.14         0.    0.0250
   58   46    1   0.84   0.02   0.00   0.76         0.    0.0064
   59   46    2   1.04   0.02   0.00   0.87         0.    0.0066
   60   46    4   0.01   0.00   0.00   0.01         0.    0.0001
   61   47    4   1.03   0.02   0.00   0.94         0.    0.0064
   62   47    0   1.36   0.02   0.00   1.27         0.    0.0066
   63   47    5   0.20   0.00   0.00   0.18         0.    0.0011
================================================================
================ TIMING STATISTICS PER NODE    ================
node# twait    ths   tcin   teig  tvect  tga   ttotal 
  1   0.00   0.05   0.25   0.00   0.00   0.23   2.26
  2   0.66   0.05   0.25   0.00   0.00   0.07   2.38
  3   0.42   0.05   0.25   0.00   0.00   0.10   2.40
  4   1.09   0.05   0.25   0.00   0.00   0.00   2.40
  5   0.35   0.05   0.25   0.00   0.00   0.07   2.38
  6   1.06   0.05   0.25   0.00   0.00   0.11   2.37
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                     6.0045s 
time spent in multnx:                   5.1191s 
integral transfer time:                 0.0180s 
time spent for loop construction:       0.5229s 
syncronization time in mult:            3.5746s 
total time per CI iteration:           14.1809s 
 ** parallelization degree for mult section:0.6268

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)           5.42        0.01        918.025
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)          5.41        0.01       1019.192
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.06        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:           10.89        0.01        947.704
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)         234.75        0.32        722.865
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)        234.75        0.24        960.539
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.12        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:          469.62        0.57        824.124
========================================================


 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -155.0177463101 -2.8422E-14  4.7796E-01  1.3809E+00  1.0000E-03
 
 expansion vectors are not transformed.
 matrix-vector products are not transformed.

    2 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)

 from bk iterations: iconv=   1

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:          15737811
 number of initial trial vectors:                         2
 number of initial matrix-vector products:                1
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                   10
 residual norm convergence criteria:               0.001000

          starting ci iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:  435270 2x:   98716 4x:   13460
All internal counts: zz :   39465 yy:  841978 xx:  317959 ww:  327023
One-external counts: yz :  232066 yx: 1092730 yw: 1098261
Two-external counts: yy :  315474 ww:   98211 xx:  111842 xz:   10881 wz:   12834 wx:  163856
Three-ext.   counts: yx : 1144321 yw: 1190216

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 474

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.94312697    -0.33243272

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -155.4046727609  3.8693E-01  2.0009E-02  2.6093E-01  1.0000E-03
 mr-sdci #  1  2   -151.9034407031  1.4933E+01  0.0000E+00  0.0000E+00  1.0000E-03
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   21    3   4.08   0.14   0.00   4.07         0.    0.1794
    2   22    1  10.49   0.03   0.00  10.41         0.    0.0334
    3   22    2   4.07   0.01   0.00   3.84         0.    0.0078
    4   22    3   0.12   0.00   0.00   0.01         0.    0.0000
    5   22    0   9.68   0.02   0.00   9.58         0.    0.0213
    6   22    3   0.31   0.00   0.00   0.19         0.    0.0004
    7   22    5   0.05   0.00   0.00   0.05         0.    0.0001
    8   23    5  10.43   0.02   0.00  10.32         0.    0.0224
    9   23    5   4.05   0.01   0.00   3.85         0.    0.0069
   10   23    3   0.37   0.00   0.00   0.27         0.    0.0006
   11   23    0   6.10   0.01   0.00   6.00         0.    0.0157
   12   23    3   2.98   0.00   0.00   2.86         0.    0.0022
   13   23    3   0.71   0.00   0.00   0.69         0.    0.0011
   14   24    5   0.27   0.00   0.00   0.15         0.    0.0038
   15   24    5   0.25   0.00   0.00   0.13         0.    0.0032
   16   24    5   0.01   0.00   0.00   0.01         0.    0.0000
   17   25    5   0.31   0.00   0.00   0.18         0.    0.0041
   18   25    3   0.27   0.00   0.00   0.16         0.    0.0034
   19   25    5   0.05   0.00   0.00   0.03         0.    0.0004
   20   26    3  18.11   0.03   0.00  17.83         0.    0.0466
   21   26    5   4.13   0.01   0.00   3.87         0.    0.0076
   22   26    5   0.38   0.00   0.00   0.27         0.    0.0006
   23   26    5   4.03   0.01   0.00   3.85         0.    0.0070
   24   26    1  13.30   0.02   0.01  13.04         0.    0.0279
   25   26    4   1.63   0.00   0.00   1.50         0.    0.0022
   26   26    2   0.11   0.00   0.00   0.01         0.    0.0000
   27   26    2   0.26   0.00   0.00   0.16         0.    0.0003
   28   26    2   0.07   0.00   0.00   0.05         0.    0.0000
   29   11    5   0.56   0.27   0.00   0.56         0.    0.1883
   30   13    2   6.95   0.33   0.00   6.86         0.    0.4109
   31   13    5   8.75   0.40   0.00   8.65         0.    0.4020
   32   13    4   0.07   0.01   0.00   0.06         0.    0.0056
   33   14    4   7.46   0.32   0.00   7.36         0.    0.3850
   34   14    3   8.73   0.38   0.00   8.62         0.    0.3802
   35   14    2   0.88   0.07   0.00   0.85         0.    0.0532
   36   31    2   8.52   0.01   0.00   8.41         0.    0.0026
   37   31    4   9.06   0.01   0.00   8.95         0.    0.0024
   38   31    5   0.29   0.00   0.00   0.28         0.    0.0000
   39   32    0   8.47   0.01   0.00   8.35         0.    0.0026
   40   32    5   8.60   0.01   0.00   8.49         0.    0.0023
   41   32    1   3.07   0.00   0.00   3.04         0.    0.0003
   42    1    5   0.08   0.06   0.00   0.08         0.    0.0306
   43    2    1   1.20   0.59   0.00   1.20         0.    0.5640
   44    3    3   2.23   0.09   0.00   2.16         0.    0.0905
   45    3    1   0.95   0.06   0.00   0.79         0.    0.0554
   46    3    2   0.08   0.00   0.00   0.01         0.    0.0001
   47    3    1   2.30   0.09   0.00   2.22         0.    0.0674
   48    3    2   0.08   0.00   0.00   0.02         0.    0.0006
   49    3    2   0.01   0.00   0.00   0.01         0.    0.0003
   50    4    2   2.46   0.09   0.00   2.39         0.    0.0856
   51    4    3   1.08   0.06   0.00   0.91         0.    0.0506
   52    4    4   0.19   0.00   0.00   0.09         0.    0.0011
   53    4    4   2.26   0.08   0.00   2.17         0.    0.0629
   54    4    0   0.45   0.01   0.00   0.32         0.    0.0055
   55    4    3   0.21   0.01   0.00   0.19         0.    0.0061
   56   75    5   0.01   0.01   0.00   0.01         0.    0.0114
   57   45    5   0.16   0.08   0.00   0.16         0.    0.0000
   58   46    1  18.65   0.02   0.00  18.55         0.    0.0000
   59   46    4  20.19   0.02   0.00  20.08         0.    0.0000
   60   46    5   0.87   0.00   0.00   0.86         0.    0.0000
   61   47    2  20.33   0.02   0.00  20.22         0.    0.0000
   62   47    0  20.75   0.02   0.00  20.65         0.    0.0000
   63   47    3   4.76   0.00   0.00   4.74         0.    0.0000
================================================================
================ TIMING STATISTICS PER NODE    ================
node# twait    ths   tcin   teig  tvect  tga   ttotal 
  1   4.53   0.09   0.32   0.00   0.00   0.38  50.49
  2   0.00   0.10   0.32   0.00   0.00   0.51  50.51
  3   6.16   0.10   0.32   0.00   0.00   0.71  50.53
  4   6.02   0.10   0.32   0.00   0.00   0.89  50.53
  5   9.11   0.10   0.32   0.00   0.00   0.47  50.51
  6   6.70   0.10   0.32   0.00   0.00   1.03  50.53
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                   267.3331s 
time spent in multnx:                 261.6845s 
integral transfer time:                 0.0967s 
time spent for loop construction:       3.4799s 
syncronization time in mult:           32.5231s 
total time per CI iteration:          303.1042s 
 ** parallelization degree for mult section:0.8915

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)          14.76        0.02        919.394
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)         14.76        0.01       1078.212
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.08        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:           29.60        0.03        974.618
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)        1550.36        2.10        739.344
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)       1550.36        1.85        839.230
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.96        0.01        181.179
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:         3101.69        3.95        785.313
========================================================


          starting ci iteration   2

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:  435270 2x:   98716 4x:   13460
All internal counts: zz :   39465 yy:  841978 xx:  317959 ww:  327023
One-external counts: yz :  232066 yx: 1092730 yw: 1098261
Two-external counts: yy :  315474 ww:   98211 xx:  111842 xz:   10881 wz:   12834 wx:  163856
Three-ext.   counts: yx : 1144321 yw: 1190216

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 474

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.94103954    -0.04343026     0.33549723

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1   -155.4192080417  1.4535E-02  1.6509E-03  8.0894E-02  1.0000E-03
 mr-sdci #  2  2   -152.6013938503  6.9795E-01  0.0000E+00  0.0000E+00  1.0000E-03
 mr-sdci #  2  3   -151.8997298403  1.4929E+01  0.0000E+00  2.1942E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   21    0   4.06   0.15   0.00   4.05         0.    0.1794
    2   22    2  10.28   0.03   0.00  10.21         0.    0.0334
    3   22    5   4.21   0.01   0.00   4.00         0.    0.0078
    4   22    1   0.09   0.00   0.00   0.01         0.    0.0000
    5   22    4   8.89   0.02   0.00   8.80         0.    0.0213
    6   22    2   0.26   0.00   0.00   0.18         0.    0.0004
    7   22    5   0.05   0.00   0.00   0.05         0.    0.0001
    8   23    3   8.05   0.02   0.00   7.97         0.    0.0224
    9   23    4   4.01   0.01   0.00   3.79         0.    0.0069
   10   23    4   0.39   0.00   0.00   0.27         0.    0.0006
   11   23    1   6.82   0.01   0.01   6.73         0.    0.0157
   12   23    2   1.27   0.00   0.00   1.17         0.    0.0022
   13   23    3   0.68   0.00   0.00   0.66         0.    0.0011
   14   24    1   0.19   0.00   0.00   0.10         0.    0.0038
   15   24    2   0.17   0.00   0.00   0.09         0.    0.0032
   16   24    3   0.02   0.00   0.00   0.01         0.    0.0000
   17   25    3   0.20   0.00   0.00   0.12         0.    0.0041
   18   25    3   0.18   0.00   0.00   0.11         0.    0.0034
   19   25    4   0.04   0.00   0.00   0.02         0.    0.0004
   20   26    4  18.74   0.04   0.00  18.48         0.    0.0466
   21   26    3   4.17   0.01   0.00   3.96         0.    0.0076
   22   26    0   0.36   0.00   0.00   0.26         0.    0.0006
   23   26    1   5.22   0.01   0.00   4.99         0.    0.0070
   24   26    2  12.91   0.02   0.01  12.70         0.    0.0279
   25   26    4   2.78   0.00   0.00   2.66         0.    0.0022
   26   26    2   0.07   0.00   0.00   0.01         0.    0.0000
   27   26    0   0.25   0.00   0.00   0.16         0.    0.0003
   28   26    3   0.06   0.00   0.00   0.05         0.    0.0000
   29   11    1   0.47   0.26   0.00   0.47         0.    0.1883
   30   13    5   6.64   0.35   0.00   6.55         0.    0.4109
   31   13    5   7.49   1.63   0.00   7.39         0.    0.4020
   32   13    2   0.07   0.01   0.00   0.06         0.    0.0056
   33   14    0   6.31   0.33   0.00   6.21         0.    0.3850
   34   14    1   7.12   0.37   0.00   7.04         0.    0.3802
   35   14    0   0.66   0.07   0.00   0.65         0.    0.0532
   36   31    3   7.46   0.01   0.00   7.35         0.    0.0026
   37   31    0   8.30   0.01   0.00   8.20         0.    0.0024
   38   31    5   0.34   0.00   0.00   0.33         0.    0.0000
   39   32    4   7.38   0.01   0.00   7.28         0.    0.0026
   40   32    2   7.90   0.01   0.00   7.81         0.    0.0023
   41   32    2   1.29   0.00   0.00   1.27         0.    0.0003
   42    1    0   0.08   0.06   0.00   0.08         0.    0.0306
   43    2    1   1.22   0.59   0.00   1.21         0.    0.5640
   44    3    5   3.53   0.09   0.00   3.45         0.    0.0905
   45    3    2   0.93   0.06   0.00   0.77         0.    0.0554
   46    3    4   0.09   0.00   0.00   0.01         0.    0.0001
   47    3    0   3.47   0.09   0.00   3.39         0.    0.0674
   48    3    1   0.10   0.00   0.00   0.02         0.    0.0006
   49    3    2   0.02   0.00   0.00   0.01         0.    0.0003
   50    4    3   3.46   0.09   0.00   3.39         0.    0.0856
   51    4    3   1.05   0.05   0.00   0.85         0.    0.0506
   52    4    5   0.20   0.00   0.00   0.10         0.    0.0011
   53    4    2   3.18   0.08   0.00   3.11         0.    0.0629
   54    4    1   0.37   0.01   0.00   0.27         0.    0.0055
   55    4    4   0.21   0.01   0.00   0.18         0.    0.0061
   56   75    0   0.01   0.01   0.00   0.01         0.    0.0114
   57   45    3   0.15   0.08   0.00   0.15         0.    0.0000
   58   46    3  17.63   0.02   0.00  17.53         0.    0.0000
   59   46    5  20.65   0.02   0.00  20.54         0.    0.0000
   60   46    4   0.60   0.00   0.00   0.60         0.    0.0000
   61   47    1  21.52   0.02   0.00  21.41         0.    0.0000
   62   47    0  19.59   0.02   0.00  19.49         0.    0.0000
   63   47    2   4.76   0.00   0.00   4.74         0.    0.0000
================================================================
================ TIMING STATISTICS PER NODE    ================
node# twait    ths   tcin   teig  tvect  tga   ttotal 
  1   0.02   0.08   0.37   0.00   0.00   0.43  43.67
  2   0.00   0.08   0.37   0.00   0.00   0.63  43.67
  3   0.01   0.08   0.37   0.00   0.00   0.72  43.67
  4   0.01   0.08   0.37   0.00   0.00   0.69  43.67
  5   0.00   0.08   0.37   0.00   0.00   0.69  43.67
  6   0.01   0.08   0.37   0.00   0.00   0.49  43.67
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                   258.6479s 
time spent in multnx:                 253.5257s 
integral transfer time:                 0.0893s 
time spent for loop construction:       4.7005s 
syncronization time in mult:            0.0620s 
total time per CI iteration:          262.0162s 
 ** parallelization degree for mult section:0.9998

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)          12.75        0.01        977.158
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)         12.74        0.02        618.363
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.18        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:           25.67        0.03        749.977
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)        1552.38        1.87        827.998
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)       1552.38        1.73        897.400
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.86        0.00        225.221
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:         3105.62        3.61        860.630
========================================================


          starting ci iteration   3

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:  435270 2x:   98716 4x:   13460
All internal counts: zz :   39465 yy:  841978 xx:  317959 ww:  327023
One-external counts: yz :  232066 yx: 1092730 yw: 1098261
Two-external counts: yy :  315474 ww:   98211 xx:  111842 xz:   10881 wz:   12834 wx:  163856
Three-ext.   counts: yx : 1144321 yw: 1190216

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 474

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.94076995    -0.04789982    -0.01269594     0.33540471

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1   -155.4205305043  1.3225E-03  2.0186E-04  2.4913E-02  1.0000E-03
 mr-sdci #  3  2   -152.7379189710  1.3653E-01  0.0000E+00  0.0000E+00  1.0000E-03
 mr-sdci #  3  3   -152.0984968910  1.9877E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mr-sdci #  3  4   -151.8995818614  1.4929E+01  0.0000E+00  2.2590E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   21    4   5.76   1.87   0.00   5.76         0.    0.1794
    2   22    3  10.60   0.03   0.00  10.50         0.    0.0334
    3   22    1   6.21   0.01   0.00   6.03         0.    0.0078
    4   22    1   0.08   0.00   0.00   0.01         0.    0.0000
    5   22    1   7.55   0.02   0.00   7.48         0.    0.0213
    6   22    2   0.28   0.00   0.00   0.18         0.    0.0004
    7   22    4   0.05   0.00   0.00   0.05         0.    0.0001
    8   23    2   7.95   0.02   0.00   7.87         0.    0.0224
    9   23    2   6.04   0.01   0.00   5.80         0.    0.0069
   10   23    3   0.39   0.00   0.00   0.28         0.    0.0006
   11   23    0   7.96   0.01   0.00   7.89         0.    0.0157
   12   23    3   1.52   0.00   0.00   1.37         0.    0.0022
   13   23    4   0.69   0.00   0.00   0.68         0.    0.0011
   14   24    2   0.18   0.00   0.00   0.10         0.    0.0038
   15   24    3   0.17   0.00   0.00   0.09         0.    0.0032
   16   24    3   0.01   0.00   0.00   0.01         0.    0.0000
   17   25    1   0.22   0.00   0.00   0.13         0.    0.0041
   18   25    0   0.20   0.00   0.00   0.11         0.    0.0034
   19   25    2   0.03   0.00   0.00   0.02         0.    0.0004
   20   26    4  19.05   0.04   0.00  18.86         0.    0.0466
   21   26    0   6.28   0.01   0.00   6.01         0.    0.0076
   22   26    3   0.37   0.00   0.00   0.26         0.    0.0006
   23   26    3   4.13   0.01   0.00   3.91         0.    0.0070
   24   26    3  12.87   0.02   0.00  12.65         0.    0.0279
   25   26    0   1.87   0.00   0.00   1.72         0.    0.0022
   26   26    5   0.09   0.00   0.00   0.01         0.    0.0000
   27   26    0   0.26   0.00   0.00   0.17         0.    0.0003
   28   26    1   0.07   0.00   0.00   0.05         0.    0.0000
   29   11    0   0.50   0.26   0.00   0.50         0.    0.1883
   30   13    5   7.37   0.33   0.00   7.28         0.    0.4109
   31   13    0   5.88   0.39   0.00   5.80         0.    0.4020
   32   13    3   0.07   0.01   0.00   0.06         0.    0.0056
   33   14    2   7.00   0.32   0.00   6.89         0.    0.3850
   34   14    1   7.69   0.37   0.00   7.62         0.    0.3802
   35   14    2   0.72   0.07   0.00   0.70         0.    0.0532
   36   31    3   8.13   0.01   0.00   8.06         0.    0.0026
   37   31    4   7.09   0.01   0.00   7.00         0.    0.0024
   38   31    5   0.36   0.00   0.00   0.36         0.    0.0000
   39   32    4   8.42   0.01   0.00   8.33         0.    0.0026
   40   32    5   6.65   0.01   0.00   6.57         0.    0.0023
   41   32    2   1.49   0.00   0.00   1.46         0.    0.0003
   42    1    0   0.08   0.06   0.00   0.08         0.    0.0306
   43    2    2   1.29   0.60   0.00   1.28         0.    0.5640
   44    3    3   5.37   0.09   0.00   5.25         0.    0.0905
   45    3    0   1.24   0.06   0.00   1.03         0.    0.0554
   46    3    2   0.09   0.00   0.00   0.01         0.    0.0001
   47    3    1   3.73   0.09   0.00   3.62         0.    0.0674
   48    3    4   0.10   0.00   0.00   0.02         0.    0.0006
   49    3    0   0.01   0.00   0.00   0.01         0.    0.0003
   50    4    5   4.07   0.09   0.00   3.96         0.    0.0856
   51    4    3   1.30   0.06   0.00   1.09         0.    0.0506
   52    4    4   0.20   0.00   0.00   0.09         0.    0.0011
   53    4    4   3.00   0.08   0.00   2.89         0.    0.0629
   54    4    4   0.38   0.01   0.00   0.27         0.    0.0055
   55    4    4   0.20   0.01   0.00   0.18         0.    0.0061
   56   75    3   0.01   0.01   0.00   0.01         0.    0.0114
   57   45    5   0.15   0.08   0.00   0.15         0.    0.0000
   58   46    1  18.69   0.02   0.00  18.60         0.    0.0000
   59   46    5  19.88   0.02   0.00  19.79         0.    0.0000
   60   46    1   0.71   0.00   0.00   0.71         0.    0.0000
   61   47    0  20.65   0.02   0.00  20.56         0.    0.0000
   62   47    2  19.87   0.02   0.00  19.77         0.    0.0000
   63   47    5   6.36   0.00   0.00   6.34         0.    0.0000
================================================================
================ TIMING STATISTICS PER NODE    ================
node# twait    ths   tcin   teig  tvect  tga   ttotal 
  1   0.01   0.10   0.49   0.00   0.00   0.71  45.62
  2   0.00   0.10   0.49   0.00   0.00   0.51  45.62
  3   0.01   0.10   0.49   0.00   0.00   0.62  45.62
  4   0.01   0.10   0.49   0.00   0.00   0.99  45.62
  5   0.00   0.10   0.49   0.00   0.00   0.57  45.62
  6   0.01   0.10   0.49   0.00   0.00   0.34  45.62
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                   269.6301s 
time spent in multnx:                 264.3360s 
integral transfer time:                 0.0940s 
time spent for loop construction:       5.1954s 
syncronization time in mult:            0.0440s 
total time per CI iteration:          273.7153s 
 ** parallelization degree for mult section:0.9998

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)           9.08        0.01       1083.467
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)          9.08        0.01        884.574
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.17        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:           18.33        0.02        946.656
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)        1556.04        1.91        815.425
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)       1556.04        1.81        857.352
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.87        0.00        240.283
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:         3112.96        3.73        835.285
========================================================


          starting ci iteration   4

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:  435270 2x:   98716 4x:   13460
All internal counts: zz :   39465 yy:  841978 xx:  317959 ww:  327023
One-external counts: yz :  232066 yx: 1092730 yw: 1098261
Two-external counts: yy :  315474 ww:   98211 xx:  111842 xz:   10881 wz:   12834 wx:  163856
Three-ext.   counts: yx : 1144321 yw: 1190216

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 474

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1    -0.94025826    -0.09693564    -0.10067189     0.22364312     0.21532954

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1   -155.4207002416  1.6974E-04  4.8218E-05  1.1176E-02  1.0000E-03
 mr-sdci #  4  2   -153.6655798258  9.2766E-01  0.0000E+00  0.0000E+00  1.0000E-03
 mr-sdci #  4  3   -152.2637859309  1.6529E-01  0.0000E+00  6.9055E-01  1.0000E-04
 mr-sdci #  4  4   -152.0005707221  1.0099E-01  0.0000E+00  1.7710E+00  1.0000E-04
 mr-sdci #  4  5   -151.4651753867  1.4494E+01  0.0000E+00  0.0000E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   21    4   5.35   0.15   0.00   5.35         0.    0.1794
    2   22    1  12.61   0.03   0.00  12.52         0.    0.0334
    3   22    5   4.22   0.01   0.00   4.06         0.    0.0078
    4   22    4   0.09   0.00   0.00   0.01         0.    0.0000
    5   22    1   9.18   0.02   0.00   9.09         0.    0.0213
    6   22    5   0.33   0.00   0.00   0.21         0.    0.0004
    7   22    1   0.05   0.00   0.00   0.05         0.    0.0001
    8   23    3   9.36   0.02   0.00   9.26         0.    0.0224
    9   23    1   5.57   0.01   0.00   5.36         0.    0.0069
   10   23    5   0.46   0.00   0.00   0.32         0.    0.0006
   11   23    5   7.58   0.02   0.00   7.47         0.    0.0157
   12   23    0   1.55   0.00   0.00   1.41         0.    0.0022
   13   23    2   0.77   0.00   0.00   0.75         0.    0.0011
   14   24    3   0.20   0.00   0.00   0.10         0.    0.0038
   15   24    0   0.20   0.00   0.00   0.10         0.    0.0032
   16   24    2   0.01   0.00   0.00   0.01         0.    0.0000
   17   25    3   0.29   0.00   0.00   0.17         0.    0.0041
   18   25    1   0.27   0.00   0.00   0.16         0.    0.0034
   19   25    5   0.03   0.00   0.00   0.02         0.    0.0004
   20   26    2  21.47   0.04   0.00  21.26         0.    0.0466
   21   26    2   4.00   0.01   0.00   3.82         0.    0.0076
   22   26    3   0.42   0.00   0.00   0.29         0.    0.0006
   23   26    2   5.64   0.01   0.00   5.43         0.    0.0070
   24   26    1  14.11   0.02   0.00  13.92         0.    0.0279
   25   26    0   1.85   0.00   0.00   1.70         0.    0.0022
   26   26    1   0.10   0.00   0.00   0.01         0.    0.0000
   27   26    2   0.31   0.00   0.00   0.18         0.    0.0003
   28   26    0   0.06   0.00   0.00   0.05         0.    0.0000
   29   11    0   0.55   0.27   0.00   0.54         0.    0.1883
   30   13    2   6.72   0.33   0.00   6.63         0.    0.4109
   31   13    3   8.47   0.40   0.00   8.37         0.    0.4020
   32   13    3   0.07   0.01   0.00   0.06         0.    0.0056
   33   14    5   5.00   0.32   0.00   4.92         0.    0.3850
   34   14    0   7.02   0.37   0.00   6.95         0.    0.3802
   35   14    1   0.99   0.07   0.00   0.96         0.    0.0532
   36   31    2   6.41   0.01   0.00   6.29         0.    0.0026
   37   31    4   8.35   0.01   0.00   8.25         0.    0.0024
   38   31    4   0.43   0.00   0.00   0.42         0.    0.0000
   39   32    4   6.71   0.01   0.00   6.59         0.    0.0026
   40   32    0   6.64   0.01   0.00   6.56         0.    0.0023
   41   32    2   1.54   0.00   0.00   1.51         0.    0.0003
   42    1    2   0.08   0.06   0.00   0.08         0.    0.0306
   43    2    3   1.37   0.62   0.00   1.37         0.    0.5640
   44    3    0   4.08   0.09   0.00   3.99         0.    0.0905
   45    3    4   1.60   0.06   0.00   1.36         0.    0.0554
   46    3    5   0.10   0.00   0.00   0.01         0.    0.0001
   47    3    1   3.66   0.09   0.00   3.54         0.    0.0674
   48    3    5   0.11   0.00   0.00   0.02         0.    0.0006
   49    3    4   0.01   0.00   0.00   0.01         0.    0.0003
   50    4    5   5.11   0.09   0.00   5.02         0.    0.0856
   51    4    5   1.59   0.06   0.00   1.36         0.    0.0506
   52    4    0   0.31   0.00   0.00   0.14         0.    0.0011
   53    4    4   3.05   0.09   0.00   2.93         0.    0.0629
   54    4    1   0.59   0.01   0.00   0.44         0.    0.0055
   55    4    4   0.24   0.01   0.00   0.22         0.    0.0061
   56   75    2   0.01   0.01   0.00   0.01         0.    0.0114
   57   45    2   0.15   0.08   0.00   0.15         0.    0.0000
   58   46    4  21.30   0.02   0.00  21.20         0.    0.0000
   59   46    5  22.60   0.02   0.00  22.49         0.    0.0000
   60   46    0   0.78   0.00   0.00   0.78         0.    0.0000
   61   47    0  24.09   0.02   0.00  23.99         0.    0.0000
   62   47    3  22.81   0.02   0.00  22.71         0.    0.0000
   63   47    3   4.12   0.00   0.00   4.10         0.    0.0000
================================================================
================ TIMING STATISTICS PER NODE    ================
node# twait    ths   tcin   teig  tvect  tga   ttotal 
  1   0.01   0.12   0.58   0.00   0.00   0.67  47.94
  2   0.00   0.12   0.58   0.00   0.00   0.78  47.94
  3   0.02   0.12   0.58   0.00   0.00   0.74  47.94
  4   0.02   0.12   0.58   0.00   0.00   0.49  47.94
  5   0.02   0.12   0.58   0.00   0.00   0.55  47.94
  6   0.01   0.12   0.58   0.00   0.00   0.88  47.94
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                   282.7349s 
time spent in multnx:                 277.0177s 
integral transfer time:                 0.0986s 
time spent for loop construction:       3.5142s 
syncronization time in mult:            0.0743s 
total time per CI iteration:          287.6510s 
 ** parallelization degree for mult section:0.9997

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)          15.89        0.02        951.592
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)         15.88        0.02        667.622
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.18        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:           31.95        0.04        775.704
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)        1549.24        2.04        761.069
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)       1549.24        2.03        762.242
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.86        0.00        226.845
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:         3099.33        4.07        761.158
========================================================


          starting ci iteration   5

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:  435270 2x:   98716 4x:   13460
All internal counts: zz :   39465 yy:  841978 xx:  317959 ww:  327023
One-external counts: yz :  232066 yx: 1092730 yw: 1098261
Two-external counts: yy :  315474 ww:   98211 xx:  111842 xz:   10881 wz:   12834 wx:  163856
Three-ext.   counts: yx : 1144321 yw: 1190216

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 474

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.93979922     0.09828021    -0.07326425     0.14652890    -0.16906810    -0.22736763

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1   -155.4207492527  4.9011E-05  1.5333E-05  7.2194E-03  1.0000E-03
 mr-sdci #  5  2   -154.5172745973  8.5169E-01  0.0000E+00  0.0000E+00  1.0000E-03
 mr-sdci #  5  3   -152.3803274423  1.1654E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mr-sdci #  5  4   -152.0178669320  1.7296E-02  0.0000E+00  1.1702E+00  1.0000E-04
 mr-sdci #  5  5   -151.9629188858  4.9774E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mr-sdci #  5  6   -151.4348473847  1.4464E+01  0.0000E+00  2.5304E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   21    3   7.01   0.14   0.00   7.00         0.    0.1794
    2   22    2  13.07   0.03   0.00  12.97         0.    0.0334
    3   22    5   4.14   0.01   0.00   3.97         0.    0.0078
    4   22    5   0.12   0.00   0.00   0.01         0.    0.0000
    5   22    5   8.69   0.02   0.00   8.60         0.    0.0213
    6   22    5   0.32   0.00   0.00   0.20         0.    0.0004
    7   22    5   0.06   0.00   0.00   0.05         0.    0.0001
    8   23    1   9.32   0.02   0.00   9.21         0.    0.0224
    9   23    2   6.86   0.01   0.00   6.68         0.    0.0069
   10   23    0   0.42   0.00   0.00   0.30         0.    0.0006
   11   23    0   6.96   0.01   0.00   6.86         0.    0.0157
   12   23    2   3.11   0.00   0.00   3.03         0.    0.0022
   13   23    0   0.68   0.00   0.00   0.67         0.    0.0011
   14   24    2   0.25   0.00   0.00   0.13         0.    0.0038
   15   24    5   0.27   0.00   0.00   0.14         0.    0.0032
   16   24    3   0.02   0.00   0.00   0.01         0.    0.0000
   17   25    0   0.31   0.00   0.00   0.18         0.    0.0041
   18   25    3   0.26   0.00   0.00   0.14         0.    0.0034
   19   25    1   0.05   0.00   0.00   0.03         0.    0.0004
   20   26    5  19.73   0.04   0.00  19.54         0.    0.0466
   21   26    4   4.11   0.01   0.00   3.94         0.    0.0076
   22   26    4   0.40   0.00   0.00   0.28         0.    0.0006
   23   26    4   6.92   0.01   0.00   3.84         0.    0.0070
   24   26    2  12.44   0.02   0.00  12.25         0.    0.0279
   25   26    3   3.39   0.00   0.00   1.47         0.    0.0022
   26   26    4   0.12   0.00   0.00   0.01         0.    0.0000
   27   26    2   0.30   0.00   0.00   0.17         0.    0.0003
   28   26    2   0.07   0.00   0.00   0.05         0.    0.0000
   29   11    3   0.51   0.26   0.00   0.50         0.    0.1883
   30   13    3   5.15   0.33   0.00   5.08         0.    0.4109
   31   13    3   7.28   0.39   0.00   7.18         0.    0.4020
   32   13    4   0.08   0.01   0.00   0.07         0.    0.0056
   33   14    2   5.09   0.32   0.00   5.01         0.    0.3850
   34   14    2   5.16   0.37   0.00   5.08         0.    0.3802
   35   14    1   2.61   1.89   0.00   2.59         0.    0.0532
   36   31    0   9.33   0.01   0.00   9.25         0.    0.0026
   37   31    4   7.91   0.01   0.00   7.80         0.    0.0024
   38   31    1   0.36   0.00   0.00   0.35         0.    0.0000
   39   32    5   9.39   0.01   0.00   9.31         0.    0.0026
   40   32    1   9.48   0.01   0.00   9.39         0.    0.0023
   41   32    4   3.08   0.00   0.00   3.06         0.    0.0003
   42    1    2   0.11   0.06   0.00   0.08         0.    0.0306
   43    2    5   3.01   2.41   0.00   3.00         0.    0.5640
   44    3    0   2.84   0.09   0.00   2.75         0.    0.0905
   45    3    5   1.07   0.06   0.00   0.89         0.    0.0554
   46    3    0   0.16   0.00   0.00   0.01         0.    0.0001
   47    3    3   2.24   0.09   0.00   2.15         0.    0.0674
   48    3    1   0.16   0.00   0.00   0.03         0.    0.0006
   49    3    0   0.01   0.00   0.00   0.01         0.    0.0003
   50    4    4   2.54   0.09   0.00   2.47         0.    0.0856
   51    4    1   1.13   0.06   0.00   0.94         0.    0.0506
   52    4    1   0.30   0.00   0.00   0.14         0.    0.0011
   53    4    0   3.86   0.08   0.00   3.78         0.    0.0629
   54    4    2   0.36   0.01   0.00   0.25         0.    0.0055
   55    4    4   0.27   0.01   0.00   0.25         0.    0.0061
   56   75    3   0.01   0.01   0.00   0.01         0.    0.0114
   57   45    3   0.16   0.08   0.00   0.16         0.    0.0000
   58   46    1  19.14   0.02   0.00  19.04         0.    0.0000
   59   46    3  20.14   0.02   0.00  20.04         0.    0.0000
   60   46    3   0.62   0.00   0.00   0.62         0.    0.0000
   61   47    0  22.23   0.02   0.00  22.13         0.    0.0000
   62   47    4  21.38   0.02   0.00  21.28         0.    0.0000
   63   47    1   4.26   0.00   0.00   4.25         0.    0.0000
================================================================
================ TIMING STATISTICS PER NODE    ================
node# twait    ths   tcin   teig  tvect  tga   ttotal 
  1   0.03   0.20   0.93   0.00   0.00   0.60  48.08
  2   0.02   0.20   0.93   0.00   0.00   0.58  48.08
  3   0.00   0.20   0.93   0.00   0.00   0.83  48.08
  4   0.04   0.20   0.93   0.00   0.00   2.28  48.08
  5   0.01   0.20   0.93   0.00   0.00   3.55  48.08
  6   0.02   0.20   0.93   0.00   0.00   0.77  48.08
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                   280.8152s 
time spent in multnx:                 270.7173s 
integral transfer time:                 0.0977s 
time spent for loop construction:       7.0901s 
syncronization time in mult:            0.1083s 
total time per CI iteration:          288.4993s 
 ** parallelization degree for mult section:0.9996

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)           2.27        0.00        856.174
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)          2.26        0.01        201.487
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.12        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:            4.65        0.01        324.085
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)        1562.86        1.88        832.800
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)       1562.86        6.73        232.311
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.92        0.00        251.026
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:         3126.64        8.61        363.236
========================================================


          starting ci iteration   6

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:  435270 2x:   98716 4x:   13460
All internal counts: zz :   39465 yy:  841978 xx:  317959 ww:  327023
One-external counts: yz :  232066 yx: 1092730 yw: 1098261
Two-external counts: yy :  315474 ww:   98211 xx:  111842 xz:   10881 wz:   12834 wx:  163856
Three-ext.   counts: yx : 1144321 yw: 1190216

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 474

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1    -0.93980651    -0.00240144

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  6  1   -155.4207615997  1.2347E-05  5.0044E-06  3.6522E-03  1.0000E-03
 mr-sdci #  6  2   -152.7260146708 -1.7913E+00  0.0000E+00  0.0000E+00  1.0000E-03
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   21    3   4.10   0.14   0.00   4.10         0.    0.1794
    2   22    4  12.45   0.03   0.00  12.36         0.    0.0334
    3   22    0   4.16   0.01   0.00   3.99         0.    0.0078
    4   22    3   0.12   0.00   0.00   0.01         0.    0.0000
    5   22    0   9.48   0.02   0.00   9.37         0.    0.0213
    6   22    4   0.32   0.00   0.00   0.21         0.    0.0004
    7   22    2   0.07   0.00   0.00   0.05         0.    0.0001
    8   23    5  10.13   0.02   0.00  10.02         0.    0.0224
    9   23    5   5.42   0.01   0.00   5.23         0.    0.0069
   10   23    5   1.80   0.00   0.00   1.68         0.    0.0006
   11   23    2   7.53   0.01   0.00   7.46         0.    0.0157
   12   23    0   1.39   0.00   0.00   1.28         0.    0.0022
   13   23    3   0.67   0.00   0.00   0.66         0.    0.0011
   14   24    2   0.24   0.00   0.00   0.13         0.    0.0038
   15   24    5   0.27   0.00   0.00   0.15         0.    0.0032
   16   24    3   0.03   0.00   0.00   0.01         0.    0.0000
   17   25    4   0.31   0.00   0.00   0.18         0.    0.0041
   18   25    4   0.28   0.00   0.00   0.15         0.    0.0034
   19   25    5   0.05   0.00   0.00   0.03         0.    0.0004
   20   26    3  20.13   0.04   0.00  19.93         0.    0.0466
   21   26    5   3.99   0.01   0.00   3.80         0.    0.0076
   22   26    2   0.42   0.00   0.00   0.29         0.    0.0006
   23   26    0   5.57   0.01   0.00   5.37         0.    0.0070
   24   26    4  14.87   0.02   0.00  14.68         0.    0.0279
   25   26    1   1.66   0.00   0.00   1.54         0.    0.0022
   26   26    2   0.14   0.00   0.00   0.01         0.    0.0000
   27   26    2   0.30   0.00   0.00   0.18         0.    0.0003
   28   26    4   0.08   0.00   0.00   0.06         0.    0.0000
   29   11    4   1.91   0.26   0.00   1.91         0.    0.1883
   30   13    4   5.31   0.33   0.00   5.23         0.    0.4109
   31   13    1   7.86   0.40   0.00   7.78         0.    0.4020
   32   13    3   0.08   0.01   0.00   0.07         0.    0.0056
   33   14    2   5.06   0.32   0.00   4.99         0.    0.3850
   34   14    3   6.88   0.37   0.00   6.79         0.    0.3802
   35   14    3   0.72   0.07   0.00   0.70         0.    0.0532
   36   31    2   8.37   0.01   0.00   8.27         0.    0.0026
   37   31    4   8.40   0.01   0.00   8.32         0.    0.0024
   38   31    5   0.38   0.00   0.00   0.37         0.    0.0000
   39   32    3   8.64   0.01   0.00   8.53         0.    0.0026
   40   32    1   8.75   0.01   0.00   8.65         0.    0.0023
   41   32    4   1.37   0.00   0.00   1.35         0.    0.0003
   42    1    5   0.08   0.06   0.00   0.08         0.    0.0306
   43    2    5   1.19   0.58   0.00   1.18         0.    0.5640
   44    3    2   3.88   0.09   0.00   3.78         0.    0.0905
   45    3    5   1.11   0.06   0.00   0.92         0.    0.0554
   46    3    5   0.12   0.00   0.00   0.01         0.    0.0001
   47    3    0   4.22   0.09   0.00   4.13         0.    0.0674
   48    3    4   0.15   0.00   0.00   0.03         0.    0.0006
   49    3    3   0.01   0.00   0.00   0.01         0.    0.0003
   50    4    1   4.50   0.09   0.00   4.41         0.    0.0856
   51    4    4   1.11   0.05   0.00   0.92         0.    0.0506
   52    4    5   0.26   0.00   0.00   0.13         0.    0.0011
   53    4    3   2.20   0.08   0.00   2.13         0.    0.0629
   54    4    3   0.49   0.01   0.00   0.35         0.    0.0055
   55    4    3   0.25   0.01   0.00   0.23         0.    0.0061
   56   75    5   0.01   0.01   0.00   0.01         0.    0.0114
   57   45    3   0.19   0.08   0.00   0.16         0.    0.0000
   58   46    1  19.86   0.02   0.00  19.76         0.    0.0000
   59   46    2  20.53   0.02   0.00  20.43         0.    0.0000
   60   46    3   2.06   0.00   0.00   2.05         0.    0.0000
   61   47    0  21.76   0.02   0.00  21.64         0.    0.0000
   62   47    5  21.75   0.02   0.00  21.64         0.    0.0000
   63   47    1   4.17   0.00   0.00   4.16         0.    0.0000
================================================================
================ TIMING STATISTICS PER NODE    ================
node# twait    ths   tcin   teig  tvect  tga   ttotal 
  1   0.24   0.07   0.27   0.00   0.00   0.57  47.18
  2   0.00   0.07   0.27   0.00   0.00   0.36  47.18
  3   0.26   0.07   0.27   0.00   0.00   0.72  47.18
  4   0.25   0.07   0.27   0.00   0.00   0.61  47.18
  5   0.25   0.07   0.27   0.00   0.00   0.84  47.18
  6   0.25   0.07   0.27   0.00   0.00   0.92  47.18
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                   279.6189s 
time spent in multnx:                 274.0237s 
integral transfer time:                 0.0991s 
time spent for loop construction:       3.4606s 
syncronization time in mult:            1.2488s 
total time per CI iteration:          283.0997s 
 ** parallelization degree for mult section:0.9956

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)          17.48        0.02        834.660
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)         17.47        0.06        315.459
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.08        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:           35.04        0.08        456.085
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)        1547.65        1.95        793.799
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)       1547.65        1.99        776.053
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.96        0.00        236.027
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:         3096.25        3.95        784.261
========================================================


          starting ci iteration   7

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:  435270 2x:   98716 4x:   13460
All internal counts: zz :   39465 yy:  841978 xx:  317959 ww:  327023
One-external counts: yz :  232066 yx: 1092730 yw: 1098261
Two-external counts: yy :  315474 ww:   98211 xx:  111842 xz:   10881 wz:   12834 wx:  163856
Three-ext.   counts: yx : 1144321 yw: 1190216

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 474

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1    -0.93972410    -0.03144582     0.02649807

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  7  1   -155.4207677475  6.1479E-06  1.9678E-06  2.3254E-03  1.0000E-03
 mr-sdci #  7  2   -154.4199800107  1.6940E+00  0.0000E+00  6.6182E+00  1.0000E-03
 mr-sdci #  7  3   -151.6820918636 -6.9824E-01  0.0000E+00  0.0000E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   21    0   4.01   0.15   0.00   4.00         0.    0.1794
    2   22    4  11.72   0.03   0.00  11.65         0.    0.0334
    3   22    1   4.26   0.01   0.00   4.07         0.    0.0078
    4   22    2   0.06   0.00   0.00   0.01         0.    0.0000
    5   22    1   7.74   0.02   0.00   7.65         0.    0.0213
    6   22    1   2.26   0.00   0.00   0.20         0.    0.0004
    7   22    5   0.05   0.00   0.00   0.05         0.    0.0001
    8   23    5   8.41   0.02   0.00   8.33         0.    0.0224
    9   23    0   5.54   0.01   0.00   5.32         0.    0.0069
   10   23    3   0.36   0.00   0.00   0.26         0.    0.0006
   11   23    2   7.73   0.01   0.00   7.64         0.    0.0157
   12   23    5   1.27   0.00   0.00   1.19         0.    0.0022
   13   23    2   2.66   0.00   0.00   2.65         0.    0.0011
   14   24    4   0.17   0.00   0.00   0.10         0.    0.0038
   15   24    4   0.18   0.00   0.00   0.09         0.    0.0032
   16   24    2   0.01   0.00   0.00   0.01         0.    0.0000
   17   25    1   0.21   0.00   0.00   0.12         0.    0.0041
   18   25    3   0.18   0.00   0.00   0.10         0.    0.0034
   19   25    3   0.03   0.00   0.00   0.02         0.    0.0004
   20   26    1  19.45   0.04   0.00  19.24         0.    0.0466
   21   26    2   3.94   0.01   0.00   3.77         0.    0.0076
   22   26    0   2.33   0.00   0.00   0.27         0.    0.0006
   23   26    5   5.60   0.01   0.00   5.38         0.    0.0070
   24   26    4  12.69   0.02   0.00  12.48         0.    0.0279
   25   26    1   1.56   0.00   0.00   1.46         0.    0.0022
   26   26    5   0.10   0.00   0.00   0.01         0.    0.0000
   27   26    0   0.25   0.00   0.00   0.16         0.    0.0003
   28   26    0   0.06   0.00   0.00   0.05         0.    0.0000
   29   11    1   0.47   0.26   0.00   0.47         0.    0.1883
   30   13    4   5.80   0.34   0.00   5.69         0.    0.4109
   31   13    3   8.49   0.40   0.00   8.37         0.    0.4020
   32   13    1   0.07   0.01   0.00   0.06         0.    0.0056
   33   14    5   5.41   0.32   0.00   5.29         0.    0.3850
   34   14    1   8.25   0.37   0.00   8.14         0.    0.3802
   35   14    5   2.70   0.07   0.00   2.69         0.    0.0532
   36   31    4   8.03   0.01   0.00   7.94         0.    0.0026
   37   31    0   7.32   0.01   0.00   7.20         0.    0.0024
   38   31    4   2.32   0.00   0.00   2.31         0.    0.0000
   39   32    2   6.35   0.01   0.00   6.24         0.    0.0026
   40   32    3   6.76   0.01   0.00   6.67         0.    0.0023
   41   32    3   1.24   0.00   0.00   1.21         0.    0.0003
   42    1    4   0.08   0.06   0.00   0.08         0.    0.0306
   43    2    4   1.19   0.58   0.00   1.19         0.    0.5640
   44    3    4   2.30   0.09   0.00   2.21         0.    0.0905
   45    3    2   0.94   0.06   0.00   0.79         0.    0.0554
   46    3    3   0.07   0.00   0.00   0.01         0.    0.0001
   47    3    2   2.23   0.09   0.00   2.16         0.    0.0674
   48    3    2   0.10   0.00   0.00   0.02         0.    0.0006
   49    3    3   0.01   0.00   0.00   0.01         0.    0.0003
   50    4    0   2.58   0.09   0.00   2.50         0.    0.0856
   51    4    0   1.00   0.06   0.00   0.84         0.    0.0506
   52    4    3   0.16   0.00   0.00   0.08         0.    0.0011
   53    4    5   1.88   0.09   0.00   1.79         0.    0.0629
   54    4    3   2.38   0.01   0.00   0.30         0.    0.0055
   55    4    1   0.19   0.01   0.00   0.18         0.    0.0061
   56   75    2   0.01   0.01   0.00   0.01         0.    0.0114
   57   45    0   0.15   0.08   0.00   0.14         0.    0.0000
   58   46    5  19.02   0.02   0.00  18.92         0.    0.0000
   59   46    3  19.93   0.02   0.00  19.83         0.    0.0000
   60   46    3   0.60   0.00   0.00   0.60         0.    0.0000
   61   47    0  21.22   0.02   0.00  21.10         0.    0.0000
   62   47    2  20.42   0.02   0.00  20.30         0.    0.0000
   63   47    3   4.26   0.00   0.00   4.25         0.    0.0000
================================================================
================ TIMING STATISTICS PER NODE    ================
node# twait    ths   tcin   teig  tvect  tga   ttotal 
  1   0.00   0.07   0.32   0.00   0.00   2.60  44.89
  2   0.01   0.07   0.32   0.00   0.00   2.61  44.89
  3   0.00   0.07   0.32   0.00   0.00   0.63  44.89
  4   0.01   0.07   0.32   0.00   0.00   2.54  44.89
  5   0.00   0.07   0.32   0.00   0.00   0.51  44.89
  6   0.01   0.07   0.32   0.00   0.00   0.59  44.89
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                   266.7650s 
time spent in multnx:                 255.8550s 
integral transfer time:                 0.0913s 
time spent for loop construction:       3.4477s 
syncronization time in mult:            0.0355s 
total time per CI iteration:          269.3223s 
 ** parallelization degree for mult section:0.9999

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)          16.30        0.02       1005.935
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)         16.30        0.02        756.833
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.18        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:           32.78        0.04        854.724
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)        1548.82        1.84        840.400
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)       1548.82        7.60        203.796
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.86        0.00        257.396
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:         3098.50        9.45        328.018
========================================================


          starting ci iteration   8

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:  435270 2x:   98716 4x:   13460
All internal counts: zz :   39465 yy:  841978 xx:  317959 ww:  327023
One-external counts: yz :  232066 yx: 1092730 yw: 1098261
Two-external counts: yy :  315474 ww:   98211 xx:  111842 xz:   10881 wz:   12834 wx:  163856
Three-ext.   counts: yx : 1144321 yw: 1190216

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 474

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1    -0.93962532     0.05593700     0.04427292     0.03055066

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  8  1   -155.4207696428  1.8953E-06  4.6193E-07  1.2910E-03  1.0000E-03
 mr-sdci #  8  2   -154.8662050803  4.4623E-01  0.0000E+00  6.7661E+00  1.0000E-03
 mr-sdci #  8  3   -152.1424712948  4.6038E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mr-sdci #  8  4   -151.6788312099 -3.3904E-01  0.0000E+00  2.9788E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   21    5   4.17   0.15   0.00   4.16         0.    0.1794
    2   22    1  10.50   0.03   0.00  10.42         0.    0.0334
    3   22    2   6.20   0.01   0.00   6.01         0.    0.0078
    4   22    2   0.10   0.00   0.00   0.01         0.    0.0000
    5   22    0   7.57   0.02   0.00   7.49         0.    0.0213
    6   22    1   0.26   0.00   0.00   0.18         0.    0.0004
    7   22    4   0.06   0.00   0.00   0.05         0.    0.0001
    8   23    5   8.03   0.02   0.00   7.96         0.    0.0224
    9   23    3   5.66   0.01   0.00   5.48         0.    0.0069
   10   23    5   0.42   0.00   0.00   0.29         0.    0.0006
   11   23    3   5.96   0.01   0.00   5.89         0.    0.0157
   12   23    0   1.36   0.00   0.00   1.26         0.    0.0022
   13   23    3   0.75   0.00   0.00   0.73         0.    0.0011
   14   24    4   1.81   0.00   0.00   1.72         0.    0.0038
   15   24    2   1.85   0.00   0.00   0.12         0.    0.0032
   16   24    5   0.03   0.00   0.00   0.01         0.    0.0000
   17   25    4   0.25   0.00   0.00   0.15         0.    0.0041
   18   25    5   1.85   0.00   0.00   1.73         0.    0.0034
   19   25    0   0.05   0.00   0.00   0.03         0.    0.0004
   20   26    5  18.92   0.04   0.00  18.74         0.    0.0466
   21   26    4   4.18   0.01   0.00   4.00         0.    0.0076
   22   26    2   0.32   0.00   0.00   0.25         0.    0.0006
   23   26    0   5.81   0.01   0.00   3.93         0.    0.0070
   24   26    1  13.21   0.02   0.00  13.03         0.    0.0279
   25   26    1   1.64   0.00   0.00   1.54         0.    0.0022
   26   26    3   0.12   0.00   0.00   0.01         0.    0.0000
   27   26    2   0.27   0.00   0.00   0.16         0.    0.0003
   28   26    3   0.08   0.00   0.00   0.06         0.    0.0000
   29   11    2   0.48   0.26   0.00   0.48         0.    0.1883
   30   13    5   7.18   0.33   0.00   5.43         0.    0.4109
   31   13    3   5.61   0.39   0.00   5.51         0.    0.4020
   32   13    1   0.10   0.01   0.00   0.07         0.    0.0056
   33   14    1   7.56   0.33   0.00   7.47         0.    0.3850
   34   14    2   5.29   0.37   0.00   5.21         0.    0.3802
   35   14    3   0.79   0.07   0.00   0.77         0.    0.0532
   36   31    4   6.05   0.01   0.00   5.98         0.    0.0026
   37   31    1   7.07   0.01   0.00   6.99         0.    0.0024
   38   31    2   0.31   0.00   0.00   0.31         0.    0.0000
   39   32    4   8.16   0.01   0.00   8.08         0.    0.0026
   40   32    2   6.66   0.01   0.00   6.55         0.    0.0023
   41   32    3   1.34   0.00   0.00   1.32         0.    0.0003
   42    1    4   0.09   0.07   0.00   0.09         0.    0.0306
   43    2    0   1.24   0.59   0.00   1.23         0.    0.5640
   44    3    5   2.62   0.09   0.00   2.53         0.    0.0905
   45    3    3   2.67   0.06   0.00   0.87         0.    0.0554
   46    3    5   0.12   0.00   0.00   0.01         0.    0.0001
   47    3    2   2.16   0.09   0.00   2.09         0.    0.0674
   48    3    2   0.15   0.00   0.00   0.03         0.    0.0006
   49    3    3   0.01   0.00   0.00   0.01         0.    0.0003
   50    4    3   2.43   0.09   0.00   2.34         0.    0.0856
   51    4    1   1.22   0.06   0.00   1.02         0.    0.0506
   52    4    0   1.84   0.00   0.00   0.13         0.    0.0011
   53    4    4   2.11   0.08   0.00   2.03         0.    0.0629
   54    4    2   0.38   0.01   0.00   0.26         0.    0.0055
   55    4    0   0.24   0.01   0.00   0.22         0.    0.0061
   56   75    3   0.01   0.01   0.00   0.01         0.    0.0114
   57   45    1   1.76   1.67   0.00   1.75         0.    0.0000
   58   46    3  17.91   0.02   0.00  17.82         0.    0.0000
   59   46    2  19.18   0.02   0.00  19.10         0.    0.0000
   60   46    4   0.63   0.00   0.00   0.63         0.    0.0000
   61   47    0  20.75   0.02   0.00  20.66         0.    0.0000
   62   47    4  19.97   0.02   0.00  19.89         0.    0.0000
   63   47    0   4.48   0.00   0.00   4.46         0.    0.0000
================================================================
================ TIMING STATISTICS PER NODE    ================
node# twait    ths   tcin   teig  tvect  tga   ttotal 
  1   0.00   0.14   0.46   0.00   0.00   2.13  43.98
  2   0.04   0.14   0.46   0.00   0.00   0.61  43.98
  3   0.00   0.14   0.46   0.00   0.00   2.46  43.98
  4   0.03   0.14   0.46   0.00   0.00   2.25  43.98
  5   0.03   0.14   0.46   0.00   0.00   0.51  43.98
  6   0.01   0.14   0.46   0.00   0.00   2.26  43.98
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                   259.9953s 
time spent in multnx:                 246.7783s 
integral transfer time:                 0.0975s 
time spent for loop construction:       5.0379s 
syncronization time in mult:            0.1104s 
total time per CI iteration:          263.8572s 
 ** parallelization degree for mult section:0.9996

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)           8.52        0.01        845.329
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)          8.51        0.01        801.350
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.11        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:           17.13        0.02        810.148
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)        1556.61        1.80        866.728
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)       1556.61        8.40        185.323
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.94        0.00        254.531
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:         3114.15       10.20        305.337
========================================================


          starting ci iteration   9

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:  435270 2x:   98716 4x:   13460
All internal counts: zz :   39465 yy:  841978 xx:  317959 ww:  327023
One-external counts: yz :  232066 yx: 1092730 yw: 1098261
Two-external counts: yy :  315474 ww:   98211 xx:  111842 xz:   10881 wz:   12834 wx:  163856
Three-ext.   counts: yx : 1144321 yw: 1190216

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 474

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1    -0.93958252     0.06605877    -0.06829471    -0.00542935     0.02103757

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  9  1   -155.4207700848  4.4197E-07  6.7699E-08  4.4363E-04  1.0000E-03
 mr-sdci #  9  2   -154.9312428191  6.5038E-02  0.0000E+00  5.9802E+00  1.0000E-03
 mr-sdci #  9  3   -152.6369110713  4.9444E-01  0.0000E+00  2.5506E+00  1.0000E-04
 mr-sdci #  9  4   -151.9799560064  3.0112E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mr-sdci #  9  5   -151.6164271169 -3.4649E-01  0.0000E+00  0.0000E+00  1.0000E-04
 
 
 total energy in cosmo calc 
e(nroot) + repnuc=    -155.4207700848
dielectric energy =       0.0000000000
deltaediel =       0.0000000000
deltaelast =       0.0000000000
 

 mr-sdci  convergence criteria satisfied after  9 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  9  1   -155.4207700848  4.4197E-07  6.7699E-08  4.4363E-04  1.0000E-03
 mr-sdci #  9  2   -154.9312428191  6.5038E-02  0.0000E+00  5.9802E+00  1.0000E-03
 mr-sdci #  9  3   -152.6369110713  4.9444E-01  0.0000E+00  2.5506E+00  1.0000E-04
 mr-sdci #  9  4   -151.9799560064  3.0112E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mr-sdci #  9  5   -151.6164271169 -3.4649E-01  0.0000E+00  0.0000E+00  1.0000E-04

####################CIUDGINFO####################

   ci vector at position   1 energy= -155.420770084767

################END OF CIUDGINFO################

 
    1 of the   6 expansion vectors are transformed.
 nnormold( 1 )= 0.920907142933461431
 nnormold( 2 )= 0.546876083585224937E-05
 nnormold( 3 )= 0.256103683912617152E-05
 nnormold( 4 )= 0.111032101664606399E-05
 nnormold( 5 )= 0.134929718037005322E-06
 nnormnew( 1 )= 0.921033120907733927
    1 of the   5 matrix-vector products are transformed.
 nnormold( 1 )= 313.480237105571007
 nnormold( 2 )= 0.146944478290016124E-02
 nnormold( 3 )= 0.721133972747507431E-03
 nnormold( 4 )= 0.313370624653528762E-03
 nnormold( 5 )= 0.341973733002699798E-04
 nnormnew( 1 )= 313.522126659839330

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
 maximum overlap with reference  1 (overlap= 0.939582519870707400 )

information on vector: 1from unit 11 written to unit 16filename civout                                                      


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =      -155.4207700848

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7    8    9   10   11   12   13

                                          orbital     5    6    7   54   55   56   57   58   59  107  108  127  128

                                         symmetry   Ag   Ag   Ag   Bu   Bu   Bu   Bu   Bu   Bu   Au   Au   Bg   Bg 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1 -0.152992                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-           
 z*  1  1       2  0.906541                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +-      
 z*  1  1       3  0.055340                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +     - 
 z*  1  1       4 -0.054038                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-             +- 
 z*  1  1       5 -0.062421                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +-      
 z*  1  1       6 -0.103803                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +     - 
 z*  1  1       8  0.106204                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +     -    - 
 z*  1  1       9 -0.062192                        +-   +-   +-   +-   +-   +-   +-   +-   +-        +-   +-      
 z*  1  1      11  0.024810                        +-   +-   +-   +-   +-   +-   +-   +-   +-        +-        +- 
 z*  1  1      12 -0.062444                        +-   +-   +-   +-   +-   +-   +-   +-   +-             +-   +- 
 z*  1  1      55 -0.011912                        +-   +-   +-   +-   +-   +-        +-   +-   +-        +-   +- 
 y   1  1     476 -0.013403              2( Au )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -           
 y   1  1     547  0.012672              1( Bg )   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +     -      
 y   1  1     602  0.010162              2( Au )   +-   +-   +-   +-   +-   +-   +-   +-   +-    -        +     - 
 y   1  1     637 -0.012240              1( Bg )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -    -      
 y   1  1     638  0.014562              2( Bg )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -    -      
 y   1  1     674  0.011786              2( Au )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +          -    - 
 y   1  1     675 -0.010627              3( Au )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +          -    - 
 y   1  1     728  0.014640              2( Au )   +-   +-   +-   +-   +-   +-   +-   +-   +-         -   +-      
 y   1  1     883 -0.013807              2( Ag )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +     -      
 y   1  1     886 -0.013135              5( Ag )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +     -      
 y   1  1     890  0.010275              9( Ag )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +     -      
 y   1  1    2871 -0.010594              1( Ag )   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +     -      
 y   1  1    3019  0.012383             10( Bu )   +-   +-   +-   +-   +-   +-   +-    -   +-   +-        +     - 
 y   1  1    3021  0.013148             12( Bu )   +-   +-   +-   +-   +-   +-   +-    -   +-   +-        +     - 
 y   1  1    3200  0.011012              5( Bu )   +-   +-   +-   +-   +-   +-   +-    -   +-   +     -   +-      
 y   1  1    3205  0.011940             10( Bu )   +-   +-   +-   +-   +-   +-   +-    -   +-   +     -   +-      
 y   1  1    3207  0.013630             12( Bu )   +-   +-   +-   +-   +-   +-   +-    -   +-   +     -   +-      
 y   1  1    3395  0.010105             12( Ag )   +-   +-   +-   +-   +-   +-   +-    -   +-   +         +-    - 
 y   1  1   10782 -0.012152              7( Bu )   +-   +-   +-   +-   +-   +-   +    +-   +-   +-         -    - 
 y   1  1   11108  0.014644              6( Ag )   +-   +-   +-   +-   +-   +-   +    +-   +-    -        +-    - 
 y   1  1   16394  0.010079             12( Bu )   +-   +-   +-   +-   +-    -   +-   +-   +-   +-        +     - 
 y   1  1   16580  0.010240             12( Bu )   +-   +-   +-   +-   +-    -   +-   +-   +-   +     -   +-      
 y   1  1   22703  0.013113              7( Ag )   +-   +-   +-   +-   +-   +    +-   +-   +-   +-    -    -      
 y   1  1   78401  0.013770              5( Bu )   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +     -      
 y   1  1   78541 -0.011331              5( Ag )   +-   +-    -   +-   +-   +-   +-   +-   +-   +-        +     - 
 y   1  1   78727 -0.013067              5( Ag )   +-   +-    -   +-   +-   +-   +-   +-   +-   +     -   +-      
 y   1  1   78912  0.010933              6( Bu )   +-   +-    -   +-   +-   +-   +-   +-   +-   +         +-    - 
 y   1  1   78919  0.011234             13( Bu )   +-   +-    -   +-   +-   +-   +-   +-   +-   +         +-    - 
 y   1  1  108969  0.011413              5( Bu )   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +     -      
 y   1  1  108970 -0.012600              6( Bu )   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +     -      
 y   1  1  108974  0.016430             10( Bu )   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +     -      
 y   1  1  108976  0.018042             12( Bu )   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +     -      
 y   1  1  109111 -0.010219              7( Ag )   +-    -   +-   +-   +-   +-   +-   +-   +-   +-        +     - 
 y   1  1  109115 -0.011123             11( Ag )   +-    -   +-   +-   +-   +-   +-   +-   +-   +-        +     - 
 y   1  1  109116 -0.012386             12( Ag )   +-    -   +-   +-   +-   +-   +-   +-   +-   +-        +     - 
 y   1  1  109302 -0.014249             12( Ag )   +-    -   +-   +-   +-   +-   +-   +-   +-   +     -   +-      
 y   1  1  109306 -0.010180             16( Ag )   +-    -   +-   +-   +-   +-   +-   +-   +-   +     -   +-      
 y   1  1  109486 -0.012055             12( Bu )   +-    -   +-   +-   +-   +-   +-   +-   +-   +         +-    - 
 y   1  1  144891  0.010986              9( Ag )    -   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +-      
 y   1  1  145072 -0.010911              6( Bu )    -   +-   +-   +-   +-   +-   +-   +-   +-   +         +-    - 
 y   1  1  145079 -0.011365             13( Bu )    -   +-   +-   +-   +-   +-   +-   +-   +-   +         +-    - 
 x   1  1 6132959  0.013936    7( Bu )   3( Au )   +-    -   +-   +-   +-   +-         -   +-        +-   +-   +- 
 x   1  1 6141326 -0.010964    6( Ag )   3( Au )   +-    -   +-   +-   +-   +-         -        +-   +-   +-   +- 
 w   1  1 7536551 -0.010481    7( Bu )   7( Bu )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-                
 w   1  1 7537823 -0.010551    1( Bg )   1( Bg )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-                
 w   1  1 7540826  0.017677    6( Ag )   7( Bu )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +          -      
 w   1  1 7542707  0.015653    1( Au )   1( Bg )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +          -      
 w   1  1 7553060 -0.013494    6( Ag )   6( Ag )   +-   +-   +-   +-   +-   +-   +-   +-   +-             +-      
 w   1  1 7555249 -0.010128    1( Au )   1( Au )   +-   +-   +-   +-   +-   +-   +-   +-   +-             +-      
 w   1  1 7808496 -0.010123    5( Bu )   5( Bu )   +-   +-   +-   +-   +-   +-   +-        +-   +-        +-      
 w   1  112595862 -0.011905    5( Bu )   5( Bu )   +-   +-   +    +-   +-   +-    -   +    +-        +-    -   +- 
 w   1  113862566 -0.010156    5( Bu )   9( Bu )   +-   +    +-   +-   +-   +-    -   +    +-        +-    -   +- 

 ci coefficient statistics:
           rq > 0.1                4
      0.1> rq > 0.01              59
     0.01> rq > 0.001          16765
    0.001> rq > 0.0001        248681
   0.0001> rq > 0.00001      1233668
  0.00001> rq > 0.000001     2957281
 0.000001> rq               11281353
           all              15737811
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:   16556 2x:       0 4x:       0
All internal counts: zz :   39465 yy:       0 xx:       0 ww:       0
One-external counts: yz :       0 yx:       0 yw:       0
Two-external counts: yy :       0 ww:       0 xx:       0 xz:       0 wz:       0 wx:       0
Three-ext.   counts: yx :       0 yw:       0

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


  iref  icsf         v(icsf)             hv(icsf)
     1     1     -0.152991531762     23.727875637505
     2     2      0.906540613162   -140.520411783514
     3     3      0.055340338058     -8.581862850850
     4     4     -0.054037898427      8.384162855333
     5     5     -0.062421228655      9.678107790019
     6     6     -0.103803265042     16.107614826353
     7     7      0.004886701990     -0.758406925097
     8     8      0.106203796582    -16.477512260234
     9     9     -0.062191563893      9.647864678462
    10    10     -0.004863899178      0.755405033387
    11    11      0.024810353489     -3.852671224927
    12    12     -0.062444026249      9.690625133285
    13    13     -0.005482815585      0.849822890346
    14    14     -0.000347166652      0.053395684409
    15    15      0.001058410556     -0.164166653911
    16    16     -0.004016129020      0.622061590871
    17    17      0.000352764589     -0.054598752545
    18    18      0.000622022444     -0.096475133601
    19    19      0.000062272634     -0.009220409505
    20    20      0.000014646620     -0.002230225764
    21    21     -0.000196850086      0.030497252311
    22    22      0.001171774807     -0.181487331558
    23    23     -0.000061488246      0.009275433589
    24    24     -0.000077860644      0.012041226807
    25    25     -0.000202342194      0.031351243212
    26    26      0.000093112096     -0.014352832155
    27    27     -0.005094668598      0.790028146785
    28    28     -0.000235003195      0.036119286266
    29    29      0.000950074781     -0.147396467877
    30    30     -0.003785672413      0.586389640973
    31    31      0.000475691738     -0.073661557545
    32    32      0.000578644368     -0.089780143717
    33    33      0.000046986173     -0.007290267485
    34    34      0.000103390458     -0.015810858147
    35    35     -0.000418045649      0.064505312231
    36    36     -0.000773837550      0.120095447399
    37    37      0.004323585445     -0.670669157303
    38    38      0.000029945290     -0.005019037913
    39    39     -0.000300133263      0.046590242626
    40    40      0.000121849294     -0.018937180553
    41    41      0.000004934458     -0.000831134624
    42    42      0.002970504650     -0.461209240229
    43    43     -0.000349328193      0.053852445188
    44    44     -0.000605915926      0.094040307711
    45    45      0.002535782545     -0.393251559546
    46    46     -0.000418026856      0.065108068518
    47    47     -0.000372368325      0.057855700415
    48    48     -0.000028422753      0.004412297093
    49    49     -0.000367656249      0.057001801179
    50    50      0.000052907650     -0.008159844524
    51    51      0.000030702326     -0.004774420261
    52    52     -0.005400732677      0.837090843824
    53    53      0.000437965747     -0.067950589658
    54    54      0.002430191559     -0.377045491334
    55    55     -0.011911778907      1.846854459266
    56    56      0.000566476359     -0.087594251916
    57    57      0.001157555713     -0.179638159255
    58    58      0.000096010621     -0.014908086321
    59    59     -0.000022524133      0.003495331851
    60    60      0.000084233612     -0.013083802428

 number of reference csfs (nref) is    60.  root number (iroot) is  1.

 pople ci energy extrapolation is computed with 22 correlated electrons.

 eref      =   -155.015842200856   "relaxed" cnot**2         =   0.885891691419
 eci       =   -155.420770084767   deltae = eci - eref       =  -0.404927883911
 eci+dv1   =   -155.466975720697   dv1 = (1-cnot**2)*deltae  =  -0.046205635930
 eci+dv2   =   -155.472927291317   dv2 = dv1 / cnot**2       =  -0.052157206550
 eci+dv3   =   -155.480638743458   dv3 = dv1 / (2*cnot**2-1) =  -0.059868658691
 eci+pople =   -155.473680871708   ( 22e- scaled deltae )    =  -0.457838670852
NO coefficients and occupation numbers are written to nocoef_ci.1
 entering drivercid: firstcall,firstnonref= T F

################################################################################
 one electron density matrix calculation 
################################################################################


 workspace allocation information: mxcore=15065949
 
 space required:
 
    space required for calls in multd2:
       onex         2491
       allin           0
       diagon      12755
    max.      ---------
       maxnex      12755
 
    total core space usage:
       maxnex      12755
    max k-seg    3747861
    max k-ind       6094
              ---------
       totmax    7520665
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=    7211  DYX=   32299  DYW=   33383
   D0Z=    2091  D0Y=   35614  D0X=   17449  D0W=   18833
  DDZI=    3701 DDYI=   47388 DDXI=   24732 DDWI=   26596
  DDZE=       0 DDYE=    6094 DDXE=    3546 DDWE=    3820
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   2.0000000      2.0000000      2.0000000      2.0000000      1.9755256
   1.9727899      1.9716903      0.0125967      0.0116655      0.0103433
   0.0093195      0.0064605      0.0051421      0.0043859      0.0025335
   0.0021035      0.0015781      0.0011135      0.0009759      0.0009093
   0.0007123      0.0005588      0.0004524      0.0004424      0.0004183
   0.0003272      0.0002772      0.0002512      0.0002321      0.0001549
   0.0001380      0.0001155      0.0001076      0.0000948      0.0000843
   0.0000780      0.0000695      0.0000639      0.0000596      0.0000535
   0.0000495      0.0000470      0.0000441      0.0000352      0.0000300
   0.0000247      0.0000199      0.0000174      0.0000162      0.0000100
   0.0000093      0.0000082      0.0000046


*****   symmetry block SYM2   *****

 occupation numbers of nos
   1.9995656      1.9995616      1.9851114      1.9780247      1.9754032
   1.9728387      0.0142368      0.0124106      0.0105520      0.0093242
   0.0087394      0.0066734      0.0051629      0.0039746      0.0023049
   0.0017009      0.0011474      0.0009661      0.0009243      0.0007259
   0.0006632      0.0005468      0.0004672      0.0004340      0.0003914
   0.0002770      0.0002362      0.0001833      0.0001706      0.0001367
   0.0001219      0.0001095      0.0000865      0.0000797      0.0000723
   0.0000687      0.0000632      0.0000616      0.0000594      0.0000580
   0.0000508      0.0000467      0.0000351      0.0000331      0.0000263
   0.0000217      0.0000178      0.0000158      0.0000130      0.0000091
   0.0000081      0.0000048      0.0000044


*****   symmetry block SYM3   *****

 occupation numbers of nos
   1.9287984      0.1018725      0.0061698      0.0053612      0.0033025
   0.0027431      0.0019938      0.0006933      0.0005192      0.0004511
   0.0003344      0.0002647      0.0002129      0.0001689      0.0001098
   0.0000862      0.0000655      0.0000328      0.0000276      0.0000225


*****   symmetry block SYM4   *****

 occupation numbers of nos
   1.8845733      0.0544855      0.0060413      0.0043534      0.0031923
   0.0021637      0.0009326      0.0006378      0.0005631      0.0004491
   0.0003316      0.0002610      0.0002113      0.0001722      0.0001143
   0.0000816      0.0000646      0.0000314      0.0000251      0.0000214


 total number of electrons =   30.0000000000



          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        Ag  partial gross atomic populations
   ao class       1Ag        2Ag        3Ag        4Ag        5Ag        6Ag 
    C1_ s       0.828078   0.013791   1.984577   0.742037   1.006942   0.247855
    C2_ s       0.682266   1.985503   0.013847   1.111303   0.105880   0.875764
    H1_ s       0.165622  -0.000035   0.000716   0.037003   0.491341   0.138121
    H2_ s       0.251377  -0.000068   0.000822   0.031652   0.342927  -0.002977
    H3_ s       0.072658   0.000809   0.000038   0.078005   0.028436   0.714027
 
   ao class       7Ag        8Ag        9Ag       10Ag       11Ag       12Ag 
    C1_ s       0.593090   0.003120   0.006324   0.003090   0.004200   0.002629
    C2_ s       1.090126   0.005742   0.000860   0.005712   0.001066   0.002801
    H1_ s       0.018801   0.000234   0.003539   0.000127   0.001153   0.000375
    H2_ s       0.234600   0.000787   0.000778   0.000103   0.002486   0.000294
    H3_ s       0.035073   0.002714   0.000164   0.001312   0.000415   0.000362
 
   ao class      13Ag       14Ag       15Ag       16Ag       17Ag       18Ag 
    C1_ s       0.001300   0.003210   0.000627   0.000558   0.000634   0.000389
    C2_ s       0.003152   0.000137   0.001327   0.000726   0.000579   0.000479
    H1_ s       0.000129   0.000377   0.000195   0.000244   0.000121   0.000029
    H2_ s       0.000126   0.000638   0.000021   0.000057   0.000139   0.000156
    H3_ s       0.000435   0.000023   0.000364   0.000519   0.000106   0.000060
 
   ao class      19Ag       20Ag       21Ag       22Ag       23Ag       24Ag 
    C1_ s       0.000310   0.000291   0.000163   0.000146   0.000205   0.000267
    C2_ s       0.000240   0.000371   0.000405   0.000283   0.000123   0.000067
    H1_ s       0.000149   0.000165   0.000061   0.000069   0.000055   0.000025
    H2_ s       0.000243   0.000056   0.000074  -0.000001   0.000056   0.000049
    H3_ s       0.000033   0.000026   0.000010   0.000062   0.000014   0.000034
 
   ao class      25Ag       26Ag       27Ag       28Ag       29Ag       30Ag 
    C1_ s       0.000111   0.000087   0.000098   0.000115   0.000135   0.000046
    C2_ s       0.000224   0.000158   0.000120   0.000071   0.000049   0.000084
    H1_ s       0.000031   0.000017   0.000025   0.000000   0.000018   0.000004
    H2_ s       0.000011   0.000016  -0.000004   0.000015   0.000012   0.000014
    H3_ s       0.000042   0.000050   0.000037   0.000051   0.000017   0.000007
 
   ao class      31Ag       32Ag       33Ag       34Ag       35Ag       36Ag 
    C1_ s       0.000033   0.000038   0.000032   0.000015   0.000018   0.000019
    C2_ s       0.000069   0.000059   0.000036   0.000037   0.000036   0.000019
    H1_ s       0.000015   0.000004   0.000018   0.000024   0.000003   0.000004
    H2_ s       0.000011   0.000009   0.000009   0.000005   0.000016   0.000032
    H3_ s       0.000010   0.000006   0.000012   0.000014   0.000012   0.000004
 
   ao class      37Ag       38Ag       39Ag       40Ag       41Ag       42Ag 
    C1_ s       0.000000   0.000012   0.000013   0.000012   0.000019   0.000000
    C2_ s       0.000017   0.000019   0.000015   0.000004   0.000002  -0.000004
    H1_ s       0.000020   0.000013   0.000004   0.000024   0.000011   0.000009
    H2_ s       0.000030   0.000014   0.000017   0.000008   0.000007   0.000001
    H3_ s       0.000003   0.000006   0.000011   0.000005   0.000010   0.000042
 
   ao class      43Ag       44Ag       45Ag       46Ag       47Ag       48Ag 
    C1_ s       0.000008   0.000008   0.000007   0.000007   0.000002   0.000006
    C2_ s       0.000007   0.000007   0.000006   0.000007   0.000001   0.000002
    H1_ s       0.000013   0.000003   0.000010   0.000001   0.000005   0.000002
    H2_ s       0.000010   0.000007   0.000004   0.000008   0.000006   0.000001
    H3_ s       0.000005   0.000009   0.000004   0.000002   0.000006   0.000005
 
   ao class      49Ag       50Ag       51Ag       52Ag       53Ag 
    C1_ s       0.000001   0.000000   0.000004   0.000000   0.000001
    C2_ s       0.000003   0.000001  -0.000001   0.000000   0.000000
    H1_ s       0.000002   0.000002   0.000003   0.000003   0.000002
    H2_ s       0.000005   0.000001   0.000002   0.000005   0.000001
    H3_ s       0.000005   0.000006   0.000001   0.000000   0.000001

                        Bu  partial gross atomic populations
   ao class       1Bu        2Bu        3Bu        4Bu        5Bu        6Bu 
    C1_ s       0.385653   1.613468   1.180114   0.350769   1.020641   0.708255
    C2_ s       1.613234   0.385019   0.415338   0.914073   0.292164   0.618666
    H1_ s       0.000084   0.000532   0.137866   0.210117   0.085831   0.397756
    H2_ s       0.000104   0.000482   0.222644   0.003375   0.575488   0.015366
    H3_ s       0.000491   0.000061   0.029150   0.499691   0.001279   0.232796
 
   ao class       7Bu        8Bu        9Bu       10Bu       11Bu       12Bu 
    C1_ s       0.003536   0.005159   0.002601   0.003696   0.003132   0.002072
    C2_ s       0.005701   0.006176   0.003953   0.003250   0.002334   0.003680
    H1_ s       0.001724   0.000237   0.001284   0.000521   0.001312   0.000301
    H2_ s       0.000625   0.000461   0.000225   0.001654   0.001439   0.000242
    H3_ s       0.002650   0.000376   0.002489   0.000203   0.000522   0.000379
 
   ao class      13Bu       14Bu       15Bu       16Bu       17Bu       18Bu 
    C1_ s       0.002365   0.002643   0.000784   0.000478   0.000395   0.000210
    C2_ s       0.001829   0.000466   0.000965   0.000723   0.000383   0.000478
    H1_ s       0.000144   0.000538   0.000124   0.000071   0.000230   0.000018
    H2_ s       0.000200   0.000317   0.000228   0.000037   0.000122   0.000103
    H3_ s       0.000625   0.000011   0.000204   0.000391   0.000017   0.000157
 
   ao class      19Bu       20Bu       21Bu       22Bu       23Bu       24Bu 
    C1_ s       0.000415   0.000373   0.000231   0.000090   0.000113   0.000248
    C2_ s      -0.000026   0.000191   0.000258   0.000243   0.000190   0.000038
    H1_ s       0.000285   0.000048   0.000053   0.000010   0.000014   0.000093
    H2_ s       0.000247   0.000030   0.000064   0.000074   0.000045   0.000053
    H3_ s       0.000003   0.000083   0.000058   0.000129   0.000106   0.000002
 
   ao class      25Bu       26Bu       27Bu       28Bu       29Bu       30Bu 
    C1_ s       0.000192   0.000178   0.000133   0.000096   0.000036   0.000062
    C2_ s       0.000141   0.000065   0.000064   0.000060   0.000086   0.000072
    H1_ s       0.000012   0.000030   0.000003   0.000013   0.000027  -0.000004
    H2_ s      -0.000003   0.000001   0.000020   0.000007   0.000018   0.000006
    H3_ s       0.000050   0.000003   0.000017   0.000007   0.000004   0.000000
 
   ao class      31Bu       32Bu       33Bu       34Bu       35Bu       36Bu 
    C1_ s       0.000005   0.000027   0.000040  -0.000004   0.000007   0.000027
    C2_ s       0.000109   0.000035   0.000013   0.000046   0.000019   0.000017
    H1_ s       0.000002   0.000015   0.000007   0.000018   0.000029   0.000006
    H2_ s       0.000005   0.000005   0.000015   0.000012   0.000012   0.000017
    H3_ s       0.000001   0.000028   0.000011   0.000009   0.000005   0.000002
 
   ao class      37Bu       38Bu       39Bu       40Bu       41Bu       42Bu 
    C1_ s       0.000002   0.000017   0.000009   0.000012   0.000006   0.000004
    C2_ s       0.000011   0.000021   0.000015   0.000009   0.000012   0.000003
    H1_ s       0.000008   0.000010   0.000017   0.000005   0.000009   0.000019
    H2_ s       0.000010   0.000005   0.000012   0.000009   0.000019   0.000008
    H3_ s       0.000033   0.000009   0.000006   0.000024   0.000004   0.000012
 
   ao class      43Bu       44Bu       45Bu       46Bu       47Bu       48Bu 
    C1_ s       0.000004   0.000008   0.000004   0.000001   0.000003   0.000003
    C2_ s       0.000013   0.000007   0.000007   0.000001   0.000005   0.000002
    H1_ s       0.000009   0.000002   0.000004   0.000010   0.000003   0.000003
    H2_ s       0.000003   0.000015   0.000006   0.000006   0.000004   0.000003
    H3_ s       0.000006   0.000001   0.000006   0.000004   0.000003   0.000005
 
   ao class      49Bu       50Bu       51Bu       52Bu       53Bu 
    C1_ s      -0.000001   0.000001   0.000002   0.000001   0.000002
    C2_ s       0.000008   0.000000  -0.000001   0.000000   0.000001
    H1_ s       0.000001   0.000002   0.000003   0.000002   0.000000
    H2_ s       0.000001   0.000002   0.000003   0.000001   0.000001
    H3_ s       0.000005   0.000005   0.000000   0.000001   0.000001

                        Au  partial gross atomic populations
   ao class       1Au        2Au        3Au        4Au        5Au        6Au 
    C1_ s       0.683016   0.064775   0.002632   0.002171   0.001429   0.001046
    C2_ s       1.220091   0.035532   0.003214   0.002559   0.001247   0.001483
    H1_ s       0.006997   0.000561   0.000289   0.000048   0.000139   0.000114
    H2_ s       0.005464   0.000642   0.000018   0.000204   0.000292   0.000000
    H3_ s       0.013230   0.000363   0.000016   0.000380   0.000196   0.000101
 
   ao class       7Au        8Au        9Au       10Au       11Au       12Au 
    C1_ s       0.000975   0.000311   0.000052   0.000108   0.000053   0.000106
    C2_ s       0.000731   0.000276   0.000063   0.000032   0.000104   0.000153
    H1_ s       0.000143   0.000024   0.000005   0.000301   0.000003   0.000004
    H2_ s       0.000038   0.000004   0.000270   0.000001   0.000088   0.000000
    H3_ s       0.000108   0.000078   0.000130   0.000010   0.000087   0.000002
 
   ao class      13Au       14Au       15Au       16Au       17Au       18Au 
    C1_ s       0.000139   0.000086   0.000084   0.000033   0.000008   0.000001
    C2_ s       0.000069   0.000082   0.000020   0.000044   0.000035   0.000001
    H1_ s       0.000004   0.000000   0.000001   0.000002   0.000006   0.000010
    H2_ s       0.000000  -0.000001   0.000003   0.000004   0.000001   0.000012
    H3_ s       0.000001   0.000001   0.000001   0.000002   0.000016   0.000010
 
   ao class      19Au       20Au 
    C1_ s       0.000001   0.000001
    C2_ s       0.000002   0.000003
    H1_ s       0.000017   0.000000
    H2_ s       0.000005   0.000009
    H3_ s       0.000002   0.000010

                        Bg  partial gross atomic populations
   ao class       1Bg        2Bg        3Bg        4Bg        5Bg        6Bg 
    C1_ s       1.220180   0.018487   0.002821   0.003427   0.001644   0.000806
    C2_ s       0.632365   0.035086   0.002634   0.000336   0.001191   0.000996
    H1_ s       0.012005   0.000228   0.000008   0.000541   0.000060   0.000004
    H2_ s       0.013704   0.000174   0.000278   0.000053   0.000089   0.000186
    H3_ s       0.006319   0.000511   0.000300  -0.000003   0.000209   0.000171
 
   ao class       7Bg        8Bg        9Bg       10Bg       11Bg       12Bg 
    C1_ s       0.000056   0.000170   0.000106   0.000103   0.000124   0.000108
    C2_ s       0.000695   0.000288   0.000135   0.000063   0.000042   0.000147
    H1_ s       0.000170   0.000006   0.000017   0.000170   0.000051   0.000001
    H2_ s      -0.000001   0.000060   0.000098   0.000098   0.000066   0.000001
    H3_ s       0.000012   0.000114   0.000207   0.000014   0.000048   0.000004
 
   ao class      13Bg       14Bg       15Bg       16Bg       17Bg       18Bg 
    C1_ s       0.000135   0.000059   0.000047   0.000049   0.000011   0.000000
    C2_ s       0.000055   0.000106   0.000063   0.000025   0.000036   0.000000
    H1_ s       0.000005   0.000001   0.000001   0.000002   0.000012   0.000000
    H2_ s       0.000015   0.000001   0.000002   0.000001   0.000005   0.000014
    H3_ s       0.000000   0.000006   0.000001   0.000004   0.000001   0.000017
 
   ao class      19Bg       20Bg 
    C1_ s       0.000001   0.000002
    C2_ s       0.000001   0.000001
    H1_ s       0.000020   0.000001
    H2_ s       0.000001   0.000010
    H3_ s       0.000001   0.000008


                        gross atomic populations
     ao           C1_        C2_        H1_        H2_        H3_
      s        12.738363  12.099976   1.720330   1.710427   1.730904
    total      12.738363  12.099976   1.720330   1.710427   1.730904
 

 Total number of electrons:   30.00000000

