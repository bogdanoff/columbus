
 program cipc      

 print the csf info for mrsdci wave functions

 written by: ron shepard

 version date: 06-jun-96

 This Version of Program cipc is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 workspace allocation parameters: lencor=  50000000 mem1=         1 ifirst=         1

 drt header information:
  cidrt_title                                                                    
 nmot  =   146 niot  =    13 nfct  =     4 nfvt  =     0
 nrow  =   109 nsym  =     4 ssym  =     1 lenbuf=  1600
 spnorb=     F spnodd=     F lxyzir(1:3)= 0 0 0
 nwalk,xbar:      21865      905     6958     6456     7546
 nvalwt,nvalw:    13934      474     6094     3546     3820
 ncsft:        15737811
 map(*)=    -1 -1 -1 -1130131132  1  2  3  4  5  6  7  8  9 10 11 12 13
            14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33
            34 35 36 37 38 39 40 41 42 43 44 45 46133134135136137138 47
            48 49 50 51 52 53 54 55 56 57 58 59 60 61 62 63 64 65 66 67
            68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87
            88 89 90 91 92 93139140 94 95 96 97 98 99100101102103104105
           106107108109110111141142112113114115116117118119120121122123
           124125126127128129
 mu(*)=      2  2  2  2  2  2  0  0  0  0  0  0  0
 syml(*) =   1  1  1  2  2  2  2  2  2  3  3  4  4
 rmo(*)=     5  6  7  1  2  3  4  5  6  1  2  1  2

 indx01: 13934 indices saved in indxv(*)
===================================ROOT # 1===================================

 rdhciv: CI vector file information:
 Hermit Integral Program : SIFS version  j25             Tue Oct 19 14:03:34 2004
  cidrt_title                                                                    
 Hermit Integral Program : SIFS version  grimming        Tue Aug  7 18:24:26 2001
  title                                                                          
 mofmt: formatted orbitals label=morb    grimming        Tue Aug  7 18:38:16 2001
 SIFS file created by program tran.      j25             Tue Oct 19 14:03:45 2004
 energy computed by program ciudg.       j25             Tue Oct 19 14:11:20 2004

 lenrec =   32768 lenci =  15737811 ninfo =  6 nenrgy =  7 ntitle =  7

 Max. overlap with ref vector #        1
 Valid ci vector #        1
 Method:        0       94% overlap
 energy( 1)=  1.034429979483E+02, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -1.274375457176E+02, ietype=    6,   fcore energy of type: H1(*)   
 energy( 3)= -1.129762209512E+02, ietype=    5,   fcore energy of type: Vref(*) 
 energy( 4)= -1.554207700848E+02, ietype=-1026,   total energy of type: MRSDCI  
 energy( 5)=  4.436325292813E-04, ietype=-2055, cnvginf energy of type: CI-Resid
 energy( 6)=  4.419718671045E-07, ietype=-2056, cnvginf energy of type: CI-D.E. 
 energy( 7)=  6.769931335983E-08, ietype=-2057, cnvginf energy of type: CI-ApxDE
==================================================================================

 space is available for  24970204 coefficients.

 updated histogram parameters:
 csfmn = 0.0000E+00 csfmx = 1.0000E+00 fhist = 5.0000E-01 nhist =  20

 this program will print the csfs generated from
 the drt according to the following print options :

 1) run in batch mode: all valid roots are automatically
    analysed and csf info is printed by default contribution
    threshold 0.01 
 2) run in interactive mode

 input menu number [  0]:

================================================================================
===================================VECTOR # 1===================================
================================================================================


 rdcivnew:      63 coefficients were selected.
 workspace: ncsfmx=15737811
 ncsfmx= 15737811

 histogram parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =15737811 ncsft =15737811 ncsf =      63
 nhist =  20 fhist = 0.50000

    cmin                cmax        num  '*'=     2 csfs.
 ----------          ----------   ----- ---------|---------|---------|---------|
 5.0000E-01 <= |c| < 1.0000E+00       1 *
 2.5000E-01 <= |c| < 5.0000E-01       0
 1.2500E-01 <= |c| < 2.5000E-01       1 *
 6.2500E-02 <= |c| < 1.2500E-01       2 *
 3.1250E-02 <= |c| < 6.2500E-02       5 ***
 1.5625E-02 <= |c| < 3.1250E-02       5 ***
 7.8125E-03 <= |c| < 1.5625E-02      49 *************************
 3.9062E-03 <= |c| < 7.8125E-03       0
 1.9531E-03 <= |c| < 3.9062E-03       0
 9.7656E-04 <= |c| < 1.9531E-03       0
 4.8828E-04 <= |c| < 9.7656E-04       0
 2.4414E-04 <= |c| < 4.8828E-04       0
 1.2207E-04 <= |c| < 2.4414E-04       0
 6.1035E-05 <= |c| < 1.2207E-04       0
 3.0518E-05 <= |c| < 6.1035E-05       0
 1.5259E-05 <= |c| < 3.0518E-05       0
 7.6294E-06 <= |c| < 1.5259E-05       0
 3.8147E-06 <= |c| < 7.6294E-06       0
 0.0000E+00 <= |c| < 3.8147E-06       0
                                  ----- ---------|---------|---------|---------|
                  total read =       63 total stored =      63

 from the selected csfs,
 min(|csfvec(:)|) = 1.0079E-02    max(|csfvec(:)|) = 9.0654E-01
 norm= 0.999999999982707388
 csfs will be printed based on coefficient magnitudes.

 current csfvec(*) selection parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =15737811 ncsft =15737811 ncsf =      63

 i:slabel(i) =  1: Ag   2: Bu   3: Au   4: Bg 
 
 frozen orbital =    1    2    3    4
 symfc(*)       =    1    1    1    1
 label          =  Ag   Ag   Ag   Ag 
 rmo(*)         =    1    2    3    4
 
 internal level =    1    2    3    4    5    6    7    8    9   10
   11   12   13
 syml(*)        =    1    1    1    2    2    2    2    2    2    3
    3    4    4
 label          =  Ag   Ag   Ag   Bu   Bu   Bu   Bu   Bu   Bu   Au 
  Au   Bg   Bg 
 rmo(*)         =    5    6    7    1    2    3    4    5    6    1
    2    1    2

 printing selected csfs in sorted order from cmin = 0.01000 to cmax = 1.00000

   indcsf     c     c**2   v  lab:rmo  lab:rmo   step(*)
  ------- -------- ------- - ---- --- ---- --- ------------
        2  0.90654 0.82182 z*                    3333333333030
        1 -0.15299 0.02341 z*                    3333333333300
        8  0.10620 0.01128 z*                    3333333331122
        6 -0.10380 0.01078 z*                    3333333331212
       12 -0.06244 0.00390 z*                    3333333330033
        5 -0.06242 0.00390 z*                    3333333331230
        9 -0.06219 0.00387 z*                    3333333330330
        3  0.05534 0.00306 z*                    3333333333012
        4 -0.05404 0.00292 z*                    3333333333003
       11  0.02481 0.00062 z*                    3333333330303
   108976  0.01804 0.00033 y           Bu : 18  13233333333120
  7540826  0.01768 0.00031 w  Ag : 13  Bu : 13 123333333331020
   108974  0.01643 0.00027 y           Bu : 16  13233333333120
  7542707  0.01565 0.00025 w  Au :  3  Bg :  3 123333333331020
    11108  0.01464 0.00021 y           Ag : 13  13333331332032
      728  0.01464 0.00021 y           Au :  4  13333333330230
      638  0.01456 0.00021 y           Bg :  4  13333333331220
   109302 -0.01425 0.00020 y           Ag : 19  13233333331230
  6132959  0.01394 0.00019 x  Bu : 13  Au :  5 112333333333020
      883 -0.01381 0.00019 y           Ag :  9  13333333323120
    78401  0.01377 0.00019 y           Bu : 11  13323333333120
     3207  0.01363 0.00019 y           Bu : 18  13333333231230
  7553060 -0.01349 0.00018 w           Ag : 13  33333333330030
      476 -0.01340 0.00018 y           Au :  4  13333333333200
     3021  0.01315 0.00017 y           Bu : 18  13333333233012
      886 -0.01313 0.00017 y           Ag : 12  13333333323120
    22703  0.01311 0.00017 y           Ag : 14  13333313333220
    78727 -0.01307 0.00017 y           Ag : 12  13323333331230
      547  0.01267 0.00016 y           Bg :  3  13333333332120
   108970 -0.01260 0.00016 y           Bu : 12  13233333333120
   109116 -0.01239 0.00015 y           Ag : 19  13233333333012
     3019  0.01238 0.00015 y           Bu : 16  13333333233012
      637 -0.01224 0.00015 y           Bg :  3  13333333331220
    10782 -0.01215 0.00015 y           Bu : 13  13333331333022
   109486 -0.01206 0.00015 y           Bu : 18  13233333331032
     3205  0.01194 0.00014 y           Bu : 16  13333333231230
       55 -0.01191 0.00014 z*                    3333330333033
 12595862 -0.01191 0.00014 w           Bu : 11  33303333333030
      674  0.01179 0.00014 y           Au :  4  13333333331022
   108969  0.01141 0.00013 y           Bu : 11  13233333333120
   145079 -0.01136 0.00013 y           Bu : 19  12333333331032
    78541 -0.01133 0.00013 y           Ag : 12  13323333333012
    78919  0.01123 0.00013 y           Bu : 19  13323333331032
   109115 -0.01112 0.00012 y           Ag : 18  13233333333012
     3200  0.01101 0.00012 y           Bu : 11  13333333231230
   144891  0.01099 0.00012 y           Ag : 16  12333333331230
  6141326 -0.01096 0.00012 x  Ag : 13  Au :  5 112333333332030
    78912  0.01093 0.00012 y           Bu : 12  13323333331032
   145072 -0.01091 0.00012 y           Bu : 12  12333333331032
      675 -0.01063 0.00011 y           Au :  5  13333333331022
     2871 -0.01059 0.00011 y           Ag :  8  13333333233120
  7537823 -0.01055 0.00011 w           Bg :  3  33333333333000
  7536551 -0.01048 0.00011 w           Bu : 13  33333333333000
      890  0.01027 0.00011 y           Ag : 16  13333333323120
    16580  0.01024 0.00010 y           Bu : 18  13333323331230
   109111 -0.01022 0.00010 y           Ag : 14  13233333333012
   109306 -0.01018 0.00010 y           Ag : 23  13233333331230
      602  0.01016 0.00010 y           Au :  4  13333333332012
 13862566 -0.01016 0.00010 w  Bu : 11  Bu : 15 123123333333030
  7555249 -0.01013 0.00010 w           Au :  3  33333333330030
  7808496 -0.01012 0.00010 w           Bu : 11  33333333033030
     3395  0.01011 0.00010 y           Ag : 19  13333333231032
    16394  0.01008 0.00010 y           Bu : 18  13333323333012
           63 csfs were printed in this range.
