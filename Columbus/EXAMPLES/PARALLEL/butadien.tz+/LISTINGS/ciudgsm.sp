1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=   474)...
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -155.0177463101 -2.8422E-14  4.7796E-01  1.3809E+00  1.0000E-03

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  1  1   -155.0177463101 -2.8422E-14  4.7796E-01  1.3809E+00  1.0000E-03

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -155.4046727609  3.8693E-01  2.0009E-02  2.6093E-01  1.0000E-03
 mr-sdci #  2  1   -155.4192080417  1.4535E-02  1.6509E-03  8.0894E-02  1.0000E-03
 mr-sdci #  3  1   -155.4205305043  1.3225E-03  2.0186E-04  2.4913E-02  1.0000E-03
 mr-sdci #  4  1   -155.4207002416  1.6974E-04  4.8218E-05  1.1176E-02  1.0000E-03
 mr-sdci #  5  1   -155.4207492527  4.9011E-05  1.5333E-05  7.2194E-03  1.0000E-03
 mr-sdci #  6  1   -155.4207615997  1.2347E-05  5.0044E-06  3.6522E-03  1.0000E-03
 mr-sdci #  7  1   -155.4207677475  6.1479E-06  1.9678E-06  2.3254E-03  1.0000E-03
 mr-sdci #  8  1   -155.4207696428  1.8953E-06  4.6193E-07  1.2910E-03  1.0000E-03
 mr-sdci #  9  1   -155.4207700848  4.4197E-07  6.7699E-08  4.4363E-04  1.0000E-03

 mr-sdci  convergence criteria satisfied after  9 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  9  1   -155.4207700848  4.4197E-07  6.7699E-08  4.4363E-04  1.0000E-03

 number of reference csfs (nref) is    60.  root number (iroot) is  1.

 eref      =   -155.015842200856   "relaxed" cnot**2         =   0.885891691419
 eci       =   -155.420770084767   deltae = eci - eref       =  -0.404927883911
 eci+dv1   =   -155.466975720697   dv1 = (1-cnot**2)*deltae  =  -0.046205635930
 eci+dv2   =   -155.472927291317   dv2 = dv1 / cnot**2       =  -0.052157206550
 eci+dv3   =   -155.480638743458   dv3 = dv1 / (2*cnot**2-1) =  -0.059868658691
 eci+pople =   -155.473680871708   ( 22e- scaled deltae )    =  -0.457838670852
