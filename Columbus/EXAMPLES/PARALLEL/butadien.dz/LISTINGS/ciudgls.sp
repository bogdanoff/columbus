1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      09/20/00    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 using llenci= -1
================================================================================
four external integ    6.75 MB location: distr. (sg)   
three external inte    5.25 MB location: distr. (sg)   
four external integ    6.75 MB location: distr. (sg)   
three external inte    5.00 MB location: distr. (sg)   
diagonal integrals     0.16 MB location: virtual disk  
off-diagonal integr    1.62 MB location: virtual disk  
computed file size in DP units
fil3w:      688107
fil3x:      655340
fil4w:      884709
fil4x:      884709
ofdgint:       20475
diagint:      212940
computed file size in DP units
fil3w:     5504856
fil3x:     5242720
fil4w:     7077672
fil4x:     7077672
ofdgint:      163800
diagint:     1703520
 nsubmx= 4  lenci= 2551014
global arrays:     22959126   (            175.16 MB)
vdisk:               233415   (              1.78 MB)
drt:                1531782   (              5.84 MB)
================================================================================
 Main memory management:
 global          3954170 DP per process
 vdisk            233415 DP per process
 stack                 0 DP per process
 core           45812415 DP per process
Array Handle=-1000 Name:'civect' Data Type:double
Array Dimensions:2551014x4
Process=0	 owns array section: [1:425169,1:4] 
Process=1	 owns array section: [425170:850338,1:4] 
Process=2	 owns array section: [850339:1275507,1:4] 
Process=3	 owns array section: [1275508:1700676,1:4] 
Process=4	 owns array section: [1700677:2125845,1:4] 
Process=5	 owns array section: [2125846:2551014,1:4] 
Array Handle=-999 Name:'sigvec' Data Type:double
Array Dimensions:2551014x4
Process=0	 owns array section: [1:425169,1:4] 
Process=1	 owns array section: [425170:850338,1:4] 
Process=2	 owns array section: [850339:1275507,1:4] 
Process=3	 owns array section: [1275508:1700676,1:4] 
Process=4	 owns array section: [1700677:2125845,1:4] 
Process=5	 owns array section: [2125846:2551014,1:4] 
Array Handle=-998 Name:'hdg' Data Type:double
Array Dimensions:2551014x1
Process=0	 owns array section: [1:425169,1:1] 
Process=1	 owns array section: [425170:850338,1:1] 
Process=2	 owns array section: [850339:1275507,1:1] 
Process=3	 owns array section: [1275508:1700676,1:1] 
Process=4	 owns array section: [1700677:2125845,1:1] 
Process=5	 owns array section: [2125846:2551014,1:1] 
Array Handle=-997 Name:'drt' Data Type:double
Array Dimensions:109413x7
Process=0	 owns array section: [1:18236,1:7] 
Process=1	 owns array section: [18237:36472,1:7] 
Process=2	 owns array section: [36473:54708,1:7] 
Process=3	 owns array section: [54709:72944,1:7] 
Process=4	 owns array section: [72945:91180,1:7] 
Process=5	 owns array section: [91181:109413,1:7] 
Array Handle=-996 Name:'nxttask' Data Type:integer
Array Dimensions:1x1
Process=0	 owns array section: [1:1,1:1] 
Process=1	 owns array section: [0:-1,0:-1] 
Process=2	 owns array section: [0:-1,0:-1] 
Process=3	 owns array section: [0:-1,0:-1] 
Process=4	 owns array section: [0:-1,0:-1] 
Process=5	 owns array section: [0:-1,0:-1] 
 CIUDG version 5.9.7 ( 5-Oct-2004)
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 0,
  GSET = 0,
   DAVCOR =10,
  NCOREL = 22
  NROOT = 1
  IVMODE = 3
  NBKITR = 1
  NVBKMN = 1
  NVBKMX = 4
  RTOLBK = 1e-3,
  NITER = 10
  NVCIMN = 1
  NVCIMX = 4
  RTOLCI = 1e-3,
  IDEN  = 1
  CSFPRN = 10,
  MAXSEG = 20
  nseg0x = 1,1,2,2
  nseg2x = 1,1,2,2
  nseg1x = 1,1,2,2
  nseg3x = 1,1,2,2
  nsegd = 1,1,2,2
  nseg4x = 1,1,2,2,
  c2ex0ex=0
  c3ex1ex=0
  cdg4ex=1
  fileloc=1,1,3,3,3,3,2
  finalv=-1
  finalw=-1
  &end
  
 ------------------------------------------------------------------------

 ** list of control variables **
 nrfitr =   30      nvrfmx =    4      nvrfmn =    1
 lvlprt =    0      nroot  =    1      noldv  =    0      noldhv =    0
 nunitv =    1      ntype  =    0      nbkitr =    1      niter  =   10
 ivmode =    3      vout   =    0      istrt  =    0      iortls =    0
 nvbkmx =    4      ibktv  =   -1      ibkthv =   -1      frcsub =    0
 nvcimx =    4      icitv  =   -1      icithv =   -1      maxseg =   20
 iden   =    1      itran  =    0      froot  =    0      rtmode =    0
 ftcalc =    1      lrtshift=1.0000    ncouple=    1      skipso  =   F
 ncorel =   22      csfprn  =  10      ctol   = 1.00E-02  davcor  =  10


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-03    1.000E-03
 
 units and filenames:
    1: ( 6)    ciudgls                                                     
    2: ( 5)    ciudgin                                                     
    3: ( 7)    ciudgsm                                                     
    4: ( 4)    cihdiag                                                     
    5: ( 3)    cihpseu                                                     
    6: (10)    cihvfl                                                      
    7: (11)    civfl                                                       
    8: (12)    diagint                                                     
    9: (13)    ofdgint                                                     
   10: (17)    cidrtfl                                                     
   11: (20)    ciftdfl                                                     
   12: (21)    ciftofl                                                     
   13: (24)    ciftifl                                                     
   14: ( 8)    ciflind                                                     
   15: (15)    civin                                                       
   16: (16)    civout                                                      
   18: (18)    d1fl                                                        
   20: (53)    cifvfl                                                      
   21: (25)    ciscr4                                                      
   22: (41)    ciftotd                                                     
   23: (42)    flacpfd                                                     
   26: (26)    ciscr5                                                      
   27: (47)    restart                                                     
   28: (48)    civout                                                      
   29: (49)    cirefv                                                      
   31: (31)    fil4w                                                       
   32: (32)    fil4x                                                       
   33: (33)    fil3w                                                       
   34: (34)    fil3x                                                       
   36: (36)    aoints                                                      
   37: (37)    aoints2                                                     
   38: (38)    drtfil                                                      
   39: (50)    cisrtif                                                     
   42: (50)    mocoef                                                      
   45: (52)    nocoef_ci                                                   
   46: (53)    civfl_restart                                               
 ------------------------------------------------------------------------
 broadcasting INDATA    
 broadcasting ACPFINFO  
 broadcasting CFILES    

 workspace allocation information: lcore=  45812415 mem1=         1 ifirst=         1

 integral file titles:
 Hermit Integral Program : SIFS version  j25             Tue Oct 19 13:58:15 2004
  cidrt_title                                                                    
 Hermit Integral Program : SIFS version  zam403          Wed Dec  5 17:51:50 2001
  title                                                                          
 mofmt: formatted orbitals label=morbl   zam403          Wed Dec  5 17:52:53 2001
 SIFS file created by program tran.      j25             Tue Oct 19 13:58:18 2004

 core energy values from the integral file:
 energy( 1)=  1.034429979483E+02, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -1.274401923755E+02, ietype=    6,   fcore energy of type: H1(*)   
 energy( 3)= -1.191523566419E+02, ietype=    5,   fcore energy of type: Vref(*) 

 total core repulsion energy = -1.431495510692E+02

 drt header information:
  cidrt_title                                                                    
 spnorb, spnodd, lxyzir,hmult F F 0 0 0 0
 nmot  =    86 niot  =    13 nfct  =     4 nfvt  =     0
 nrow  =   104 nsym  =     4 ssym  =     1 lenbuf=  1600
 nwalk,xbar:      14155      800     5110     3772     4473
 nvalwt,nvalw:     8735      420     4326     1882     2107
 ncsft:         2551014
 total number of valid internal walks:    8735
 nvalz,nvaly,nvalx,nvalw =      420    4326    1882    2107

 cisrt info file parameters:
 file number  12 blocksize   4095
 mxbld   8190
 nd4ext,nd2ext,nd0ext  4830  1794   182
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int   822120   583389   173652    24945     1461        0        0        0
 minbl4,minbl3,maxbl2  2295  2295  2298
 maxbuf 32767
 number of external orbitals per symmetry block:  25  26   9   9
 nmsym   4 number of internal orbitals  13

 formula file title:
 Hermit Integral Program : SIFS version  j25             Tue Oct 19 13:58:15 2004
  cidrt_title                                                                    
 Hermit Integral Program : SIFS version  zam403          Wed Dec  5 17:51:50 2001
  title                                                                          
 mofmt: formatted orbitals label=morbl   zam403          Wed Dec  5 17:52:53 2001
 SIFS file created by program tran.      j25             Tue Oct 19 13:58:18 2004
  cidrt_title                                                                    
 file nmb.  24 block size     0
 pthz,pthy,pthx,pthw:   800  5110  3772  4473 total internal walks:   14155
 maxlp3,n3xtlp,n2xtlp,n1xtlp,n0xtlp,n2lp,n1lp,n0lp     0     0     0     0
 orbsym(*)= 1 1 1 2 2 2 2 2 2 3 3 4 4

 setref:       33 references kept,
                0 references were marked as invalid, out of
               33 total.
 limcnvrt: found 420  valid internal walksout of  800  walks (skipping trailing invalids)
  ... adding  420  segmentation marks segtype= 1
 limcnvrt: found 4326  valid internal walksout of  5110  walks (skipping trailing invalids)
  ... adding  4326  segmentation marks segtype= 2
 limcnvrt: found 1882  valid internal walksout of  3772  walks (skipping trailing invalids)
  ... adding  1882  segmentation marks segtype= 3
 limcnvrt: found 2107  valid internal walksout of  4473  walks (skipping trailing invalids)
  ... adding  2107  segmentation marks segtype= 4
 broadcasting SOLXYZ    
 broadcasting INDATA    
 broadcasting REPNUC    
 broadcasting INF       
 broadcasting SOINF     
 broadcasting DATA      
 broadcasting MOMAP     
 broadcasting CLI       
 broadcasting CSYM      
 broadcasting SLABEL    
 broadcasting MOMAP     
 broadcasting DRT       
 broadcasting INF1      
 broadcasting DRTINFO   
loaded       82 onel-dg  integrals(   1records ) at       1 on virtual disk
loaded     4830 4ext-dg  integrals(   2records ) at    4096 on virtual disk
loaded     1794 2ext-dg  integrals(   1records ) at   12286 on virtual disk
loaded      182 0ext-dg  integrals(   1records ) at   16381 on virtual disk
loaded      766 onel-of  integrals(   1records ) at   20476 on virtual disk
loaded   173652 2ext-og  integrals(  43records ) at   24571 on virtual disk
loaded    24945 1ext-og  integrals(   7records ) at  200656 on virtual disk
loaded     1461 0ext-og  integrals(   1records ) at  229321 on virtual disk

 number of external paths / symmetry
 vertex x     697     731     459     459
 vertex w     766     731     459     459

 lprune: l(*,*,*) pruned with nwalk=     800 nvalwt=     420 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    5110 nvalwt=    4326 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    1598 nvalwt=     942 nprune=      44

 lprune: l(*,*,*) pruned with nwalk=    2174 nvalwt=     940 nprune=      58

 lprune: l(*,*,*) pruned with nwalk=    1934 nvalwt=    1054 nprune=      43

 lprune: l(*,*,*) pruned with nwalk=    2539 nvalwt=    1053 nprune=      54



                   segmentation summary for type diagonal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1         800|       420|         0|       420|         0|         1|
 -------------------------------------------------------------------------------
  Y 2        5110|     67426|       420|      4326|       420|      1021|
 -------------------------------------------------------------------------------
  X 3        1598|    538254|     67846|       942|      4746|      6111|
 -------------------------------------------------------------------------------
  X 4        2084|    598468|    606100|       940|      5688|      7681|
 -------------------------------------------------------------------------------
  W 5        1874|    653692|   1204568|      1054|      6628|      9361|
 -------------------------------------------------------------------------------
  W 6        2449|    692754|   1858260|      1053|      7682|     11089|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>   2551014


 lprune: l(*,*,*) pruned with nwalk=     800 nvalwt=     420 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    5110 nvalwt=    4326 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    1598 nvalwt=     942 nprune=      44

 lprune: l(*,*,*) pruned with nwalk=    2174 nvalwt=     940 nprune=      58

 lprune: l(*,*,*) pruned with nwalk=    1934 nvalwt=    1054 nprune=      43

 lprune: l(*,*,*) pruned with nwalk=    2539 nvalwt=    1053 nprune=      54



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1         800|       420|         0|       420|         0|         1|
 -------------------------------------------------------------------------------
  Y 2        5110|     67426|       420|      4326|       420|      1021|
 -------------------------------------------------------------------------------
  X 3        1598|    538254|     67846|       942|      4746|      6111|
 -------------------------------------------------------------------------------
  X 4        2084|    598468|    606100|       940|      5688|      7681|
 -------------------------------------------------------------------------------
  W 5        1874|    653692|   1204568|      1054|      6628|      9361|
 -------------------------------------------------------------------------------
  W 6        2449|    692754|   1858260|      1053|      7682|     11089|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>   2551014


 lprune: l(*,*,*) pruned with nwalk=     800 nvalwt=     420 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    5110 nvalwt=    4326 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    1598 nvalwt=     942 nprune=      44

 lprune: l(*,*,*) pruned with nwalk=    2174 nvalwt=     940 nprune=      58

 lprune: l(*,*,*) pruned with nwalk=    1934 nvalwt=    1054 nprune=      43

 lprune: l(*,*,*) pruned with nwalk=    2539 nvalwt=    1053 nprune=      54



                   segmentation summary for type one-external
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1         800|       420|         0|       420|         0|         1|
 -------------------------------------------------------------------------------
  Y 2        5110|     67426|       420|      4326|       420|      1021|
 -------------------------------------------------------------------------------
  X 3        1598|    538254|     67846|       942|      4746|      6111|
 -------------------------------------------------------------------------------
  X 4        2084|    598468|    606100|       940|      5688|      7681|
 -------------------------------------------------------------------------------
  W 5        1874|    653692|   1204568|      1054|      6628|      9361|
 -------------------------------------------------------------------------------
  W 6        2449|    692754|   1858260|      1053|      7682|     11089|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>   2551014


 lprune: l(*,*,*) pruned with nwalk=     800 nvalwt=     420 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    5110 nvalwt=    4326 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    1598 nvalwt=     942 nprune=      44

 lprune: l(*,*,*) pruned with nwalk=    2174 nvalwt=     940 nprune=      58

 lprune: l(*,*,*) pruned with nwalk=    1934 nvalwt=    1054 nprune=      43

 lprune: l(*,*,*) pruned with nwalk=    2539 nvalwt=    1053 nprune=      54



                   segmentation summary for type two-external
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1         800|       420|         0|       420|         0|         1|
 -------------------------------------------------------------------------------
  Y 2        5110|     67426|       420|      4326|       420|      1021|
 -------------------------------------------------------------------------------
  X 3        1598|    538254|     67846|       942|      4746|      6111|
 -------------------------------------------------------------------------------
  X 4        2084|    598468|    606100|       940|      5688|      7681|
 -------------------------------------------------------------------------------
  W 5        1874|    653692|   1204568|      1054|      6628|      9361|
 -------------------------------------------------------------------------------
  W 6        2449|    692754|   1858260|      1053|      7682|     11089|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>   2551014


 lprune: l(*,*,*) pruned with nwalk=     800 nvalwt=     420 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    5110 nvalwt=    4326 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    1598 nvalwt=     942 nprune=      44

 lprune: l(*,*,*) pruned with nwalk=    2174 nvalwt=     940 nprune=      58

 lprune: l(*,*,*) pruned with nwalk=    1934 nvalwt=    1054 nprune=      43

 lprune: l(*,*,*) pruned with nwalk=    2539 nvalwt=    1053 nprune=      54



                   segmentation summary for type three-external
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1         800|       420|         0|       420|         0|         1|
 -------------------------------------------------------------------------------
  Y 2        5110|     67426|       420|      4326|       420|      1021|
 -------------------------------------------------------------------------------
  X 3        1598|    538254|     67846|       942|      4746|      6111|
 -------------------------------------------------------------------------------
  X 4        2084|    598468|    606100|       940|      5688|      7681|
 -------------------------------------------------------------------------------
  W 5        1874|    653692|   1204568|      1054|      6628|      9361|
 -------------------------------------------------------------------------------
  W 6        2449|    692754|   1858260|      1053|      7682|     11089|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>   2551014


 lprune: l(*,*,*) pruned with nwalk=     800 nvalwt=     420 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    5110 nvalwt=    4326 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=    1598 nvalwt=     942 nprune=      44

 lprune: l(*,*,*) pruned with nwalk=    2174 nvalwt=     940 nprune=      58

 lprune: l(*,*,*) pruned with nwalk=    1934 nvalwt=    1054 nprune=      43

 lprune: l(*,*,*) pruned with nwalk=    2539 nvalwt=    1053 nprune=      54



                   segmentation summary for type four-external
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1         800|       420|         0|       420|         0|         1|
 -------------------------------------------------------------------------------
  Y 2        5110|     67426|       420|      4326|       420|      1021|
 -------------------------------------------------------------------------------
  X 3        1598|    538254|     67846|       942|      4746|      6111|
 -------------------------------------------------------------------------------
  X 4        2084|    598468|    606100|       940|      5688|      7681|
 -------------------------------------------------------------------------------
  W 5        1874|    653692|   1204568|      1054|      6628|      9361|
 -------------------------------------------------------------------------------
  W 6        2449|    692754|   1858260|      1053|      7682|     11089|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>   2551014

 broadcasting CIVCT     
 broadcasting DATA      
 broadcasting INF       
 broadcasting CNFSPC    
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS
----------------------------------------------------------------------------------------------------
     1  2   2    21      two-ext yy   2X  2 2    5110    5110      67426      67426    4326    4326
     2  3   3    22      two-ext xx   2X  3 3    1598    1598     538254     538254     942     942
     3  4   3    22      two-ext xx   2X  3 3    2084    1598     598468     538254     940     942
     4  4   4    22      two-ext xx   2X  3 3    2084    2084     598468     598468     940     940
     5  5   5    23      two-ext ww   2X  4 4    1874    1874     653692     653692    1054    1054
     6  6   5    23      two-ext ww   2X  4 4    2449    1874     692754     653692    1053    1054
     7  6   6    23      two-ext ww   2X  4 4    2449    2449     692754     692754    1053    1053
     8  3   1    24      two-ext xz   2X  3 1    1598     800     538254        420     942     420
     9  4   1    24      two-ext xz   2X  3 1    2084     800     598468        420     940     420
    10  5   1    25      two-ext wz   2X  4 1    1874     800     653692        420    1054     420
    11  6   1    25      two-ext wz   2X  4 1    2449     800     692754        420    1053     420
    12  5   3    26      two-ext wx   2X  4 3    1874    1598     653692     538254    1054     942
    13  6   3    26      two-ext wx   2X  4 3    2449    1598     692754     538254    1053     942
    14  5   4    26      two-ext wx   2X  4 3    1874    2084     653692     598468    1054     940
    15  6   4    26      two-ext wx   2X  4 3    2449    2084     692754     598468    1053     940
    16  2   1    11      one-ext yz   1X  2 1    5110     800      67426        420    4326     420
    17  3   2    13      one-ext yx   1X  3 2    1598    5110     538254      67426     942    4326
    18  4   2    13      one-ext yx   1X  3 2    2084    5110     598468      67426     940    4326
    19  5   2    14      one-ext yw   1X  4 2    1874    5110     653692      67426    1054    4326
    20  6   2    14      one-ext yw   1X  4 2    2449    5110     692754      67426    1053    4326
    21  3   2    31      thr-ext yx   3X  3 2    1598    5110     538254      67426     942    4326
    22  4   2    31      thr-ext yx   3X  3 2    2084    5110     598468      67426     940    4326
    23  5   2    32      thr-ext yw   3X  4 2    1874    5110     653692      67426    1054    4326
    24  6   2    32      thr-ext yw   3X  4 2    2449    5110     692754      67426    1053    4326
    25  1   1     1      allint zz    OX  1 1     800     800        420        420     420     420
    26  2   2     2      allint yy    OX  2 2    5110    5110      67426      67426    4326    4326
    27  3   3     3      allint xx    OX  3 3    1598    1598     538254     538254     942     942
    28  4   3     3      allint xx    OX  3 3    2084    1598     598468     538254     940     942
    29  4   4     3      allint xx    OX  3 3    2084    2084     598468     598468     940     940
    30  5   5     4      allint ww    OX  4 4    1874    1874     653692     653692    1054    1054
    31  6   5     4      allint ww    OX  4 4    2449    1874     692754     653692    1053    1054
    32  6   6     4      allint ww    OX  4 4    2449    2449     692754     692754    1053    1053
    33  1   1    75      dg-024ext z  DG  1 1     800     800        420        420     420     420
    34  2   2    45      4exdg024 y   DG  2 2    5110    5110      67426      67426    4326    4326
    35  3   3    46      4exdg024 x   DG  3 3    1598    1598     538254     538254     942     942
    36  4   4    46      4exdg024 x   DG  3 3    2084    2084     598468     598468     940     940
    37  5   5    47      4exdg024 w   DG  4 4    1874    1874     653692     653692    1054    1054
    38  6   6    47      4exdg024 w   DG  4 4    2449    2449     692754     692754    1053    1053
----------------------------------------------------------------------------------------------------
 broadcasting TASKLST   
 DIAGEL COUNTS: DG0X,DG2X,DG4X= 214466 53393 8315
 diagonal elements written to file   4

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:   11918 2x:       0 4x:       0
All internal counts: zz :   32118 yy:       0 xx:       0 ww:       0
One-external counts: yz :       0 yx:       0 yw:       0
Two-external counts: yy :       0 ww:       0 xx:       0 xz:       0 wz:       0 wx:       0
Three-ext.   counts: yx :       0 yw:       0

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 reference space has dimension      33

    root           eigenvalues
    ----           ------------
       1        -154.9825941390

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt= 420

         vector  1 from unit 11 written to unit 49 filename cirefv                                                      
 ufvoutnew: ... writing  recamt= 420

         vector  1 from unit 11 written to unit 49 filename cirefv                                                      

 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=   420)...
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:           2551014
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               4
 number of roots to converge:                             1
 number of iterations:                                    1
 residual norm convergence criteria:               0.001000

          starting bk iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:  214466 2x:   53393 4x:    8315
All internal counts: zz :   32118 yy:       0 xx:       0 ww:       0
One-external counts: yz :  161582 yx:       0 yw:       0
Two-external counts: yy :       0 ww:       0 xx:       0 xz:    5228 wz:    6717 wx:       0
Three-ext.   counts: yx :       0 yw:       0

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 420

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -154.9825941390 -1.1546E-13  4.2073E-01  1.0946E+00  1.0000E-03
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   21    0   0.00   0.00   0.00   0.00         0.    0.0000
    2   22    0   0.00   0.00   0.00   0.00         0.    0.0000
    3   22    0   0.00   0.00   0.00   0.00         0.    0.0000
    4   22    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   23    0   0.00   0.00   0.00   0.00         0.    0.0000
    6   23    0   0.00   0.00   0.00   0.00         0.    0.0000
    7   23    0   0.00   0.00   0.00   0.00         0.    0.0000
    8   24    0   0.03   0.00   0.00   0.02         0.    0.0016
    9   24    4   0.03   0.00   0.00   0.02         0.    0.0015
   10   25    5   0.04   0.00   0.00   0.02         0.    0.0021
   11   25    1   0.06   0.00   0.00   0.03         0.    0.0018
   12   26    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   26    0   0.00   0.00   0.00   0.00         0.    0.0000
   14   26    0   0.00   0.00   0.00   0.00         0.    0.0000
   15   26    0   0.00   0.00   0.00   0.00         0.    0.0000
   16   11    2   0.29   0.18   0.00   0.29         0.    0.1309
   17   13    0   0.00   0.00   0.00   0.00         0.    0.0000
   18   13    0   0.00   0.00   0.00   0.00         0.    0.0000
   19   14    0   0.00   0.00   0.00   0.00         0.    0.0000
   20   14    0   0.00   0.00   0.00   0.00         0.    0.0000
   21   31    0   0.00   0.00   0.00   0.00         0.    0.0000
   22   31    0   0.00   0.00   0.00   0.00         0.    0.0000
   23   32    0   0.00   0.00   0.00   0.00         0.    0.0000
   24   32    0   0.00   0.00   0.00   0.00         0.    0.0000
   25    1    3   0.07   0.05   0.00   0.07         0.    0.0244
   26    2    0   0.00   0.00   0.00   0.00         0.    0.0000
   27    3    0   0.00   0.00   0.00   0.00         0.    0.0000
   28    3    0   0.00   0.00   0.00   0.00         0.    0.0000
   29    3    0   0.00   0.00   0.00   0.00         0.    0.0000
   30    4    0   0.00   0.00   0.00   0.00         0.    0.0000
   31    4    0   0.00   0.00   0.00   0.00         0.    0.0000
   32    4    0   0.00   0.00   0.00   0.00         0.    0.0000
   33   75    0   0.01   0.01   0.00   0.01         0.    0.0082
   34   45    4   0.07   0.05   0.00   0.07         0.    0.0168
   35   46    0   0.11   0.01   0.00   0.10         0.    0.0032
   36   46    5   0.14   0.01   0.00   0.14         0.    0.0033
   37   47    1   0.13   0.01   0.00   0.12         0.    0.0035
   38   47    3   0.16   0.01   0.00   0.14         0.    0.0037
================================================================
================ TIMING STATISTICS PER NODE    ================
node# twait    ths   tcin   teig  tvect  tga   ttotal 
  1   0.15   0.00   0.05   0.00   0.00   0.01   0.40
  2   0.10   0.01   0.05   0.00   0.00   0.02   0.51
  3   0.00   0.01   0.05   0.00   0.00   0.00   0.55
  4   0.07   0.01   0.05   0.00   0.00   0.01   0.55
  5   0.19   0.01   0.05   0.00   0.00   0.01   0.55
  6   0.11   0.01   0.05   0.00   0.00   0.02   0.55
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                     1.1362s 
time spent in multnx:                   1.0179s 
integral transfer time:                 0.0045s 
time spent for loop construction:       0.3364s 
syncronization time in mult:            0.6199s 
total time per CI iteration:            3.1060s 
 ** parallelization degree for mult section:0.6470

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)           0.01        0.00          0.000
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)          0.01        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.02        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:            0.04        0.00          0.000
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)          38.93        0.05        862.247
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)         38.93        0.03       1265.569
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.10        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:           77.97        0.08       1020.152
========================================================


 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -154.9825941390 -1.1546E-13  4.2073E-01  1.0946E+00  1.0000E-03
 
 expansion vectors are not transformed.
 matrix-vector products are not transformed.

    2 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)

 from bk iterations: iconv=   1

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:           2551014
 number of initial trial vectors:                         2
 number of initial matrix-vector products:                1
 maximum dimension of the subspace vectors:               4
 number of roots to converge:                             1
 number of iterations:                                   10
 residual norm convergence criteria:               0.001000

          starting ci iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:  214466 2x:   53393 4x:    8315
All internal counts: zz :   32118 yy:  490036 xx:  110475 ww:  123428
One-external counts: yz :  161582 yx:  486981 yw:  504573
Two-external counts: yy :  191370 ww:   45189 xx:   45699 xz:    5228 wz:    6717 wx:   69923
Three-ext.   counts: yx :   59965 yw:   63497

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 420

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.94546073    -0.32573609

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -155.3262379997  3.4364E-01  1.7658E-02  2.1192E-01  1.0000E-03
 mr-sdci #  1  2   -152.0874924680  8.9379E+00  0.0000E+00  0.0000E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   21    4   0.94   0.09   0.00   0.94         0.    0.1070
    2   22    2   0.93   0.01   0.00   0.92         0.    0.0126
    3   22    3   0.46   0.00   0.00   0.42         0.    0.0033
    4   22    2   0.72   0.01   0.00   0.71         0.    0.0085
    5   23    0   0.95   0.01   0.00   0.93         0.    0.0106
    6   23    1   0.50   0.00   0.00   0.45         0.    0.0031
    7   23    3   0.84   0.01   0.00   0.82         0.    0.0080
    8   24    2   0.02   0.00   0.00   0.02         0.    0.0016
    9   24    2   0.03   0.00   0.00   0.02         0.    0.0015
   10   25    2   0.03   0.00   0.00   0.03         0.    0.0021
   11   25    2   0.05   0.00   0.00   0.02         0.    0.0018
   12   26    4   1.59   0.01   0.00   1.56         0.    0.0186
   13   26    0   0.43   0.00   0.00   0.40         0.    0.0032
   14   26    0   0.45   0.00   0.00   0.43         0.    0.0031
   15   26    5   1.23   0.01   0.00   1.19         0.    0.0112
   16   11    0   0.30   0.18   0.00   0.30         0.    0.1309
   17   13    0   0.90   0.15   0.00   0.89         0.    0.1882
   18   13    2   0.92   0.18   0.00   0.91         0.    0.1800
   19   14    3   1.00   0.16   0.00   0.99         0.    0.1908
   20   14    1   1.11   0.19   0.00   1.09         0.    0.1882
   21   31    0   0.63   0.00   0.00   0.61         0.    0.0014
   22   31    4   0.69   0.00   0.00   0.67         0.    0.0013
   23   32    2   0.65   0.00   0.00   0.64         0.    0.0015
   24   32    5   0.75   0.00   0.00   0.73         0.    0.0014
   25    1    2   0.07   0.05   0.00   0.06         0.    0.0244
   26    2    1   0.67   0.35   0.00   0.67         0.    0.3369
   27    3    0   0.33   0.03   0.00   0.32         0.    0.0313
   28    3    2   0.16   0.02   0.00   0.12         0.    0.0197
   29    3    4   0.35   0.04   0.00   0.33         0.    0.0238
   30    4    2   0.45   0.04   0.00   0.44         0.    0.0349
   31    4    0   0.19   0.02   0.00   0.16         0.    0.0190
   32    4    5   0.38   0.04   0.00   0.36         0.    0.0258
   33   75    2   0.01   0.01   0.00   0.01         0.    0.0082
   34   45    2   0.07   0.05   0.00   0.07         0.    0.0000
   35   46    5   1.11   0.01   0.00   1.10         0.    0.0000
   36   46    4   1.17   0.01   0.00   1.16         0.    0.0000
   37   47    1   1.42   0.01   0.00   1.41         0.    0.0000
   38   47    3   1.41   0.01   0.00   1.40         0.    0.0000
================================================================
================ TIMING STATISTICS PER NODE    ================
node# twait    ths   tcin   teig  tvect  tga   ttotal 
  1   0.57   0.01   0.06   0.00   0.00   0.10   4.87
  2   1.05   0.01   0.06   0.00   0.00   0.05   4.89
  3   0.65   0.01   0.06   0.00   0.00   0.10   4.93
  4   1.04   0.01   0.06   0.00   0.00   0.06   4.93
  5   0.00   0.01   0.06   0.00   0.00   0.06   4.87
  6   1.28   0.01   0.06   0.00   0.00   0.06   4.89
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                    23.8996s 
time spent in multnx:                  23.3000s 
integral transfer time:                 0.0164s 
time spent for loop construction:       1.7230s 
syncronization time in mult:            4.5821s 
total time per CI iteration:           29.3838s 
 ** parallelization degree for mult section:0.8391

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)           1.55        0.00       1433.880
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)          1.55        0.00       1128.647
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.13        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:            3.22        0.00       1042.577
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)         194.10        0.21        914.204
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)        194.10        0.19       1013.761
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.39        0.00        167.312
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:          388.59        0.41        956.807
========================================================


          starting ci iteration   2

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:  214466 2x:   53393 4x:    8315
All internal counts: zz :   32118 yy:  490036 xx:  110475 ww:  123428
One-external counts: yz :  161582 yx:  486981 yw:  504573
Two-external counts: yy :  191370 ww:   45189 xx:   45699 xz:    5228 wz:    6717 wx:   69923
Three-ext.   counts: yx :   59965 yw:   63497

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 420

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.94242155    -0.11178499     0.31519160

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1   -155.3389342592  1.2696E-02  1.4337E-03  6.4796E-02  1.0000E-03
 mr-sdci #  2  2   -152.7831941528  6.9570E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mr-sdci #  2  3   -152.0735312724  8.9240E+00  0.0000E+00  1.0087E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   21    2   0.87   0.09   0.00   0.87         0.    0.1070
    2   22    5   1.02   0.01   0.00   1.01         0.    0.0126
    3   22    5   0.48   0.00   0.00   0.44         0.    0.0033
    4   22    1   0.79   0.01   0.00   0.78         0.    0.0085
    5   23    3   1.04   0.01   0.00   1.02         0.    0.0106
    6   23    1   0.52   0.00   0.00   0.49         0.    0.0031
    7   23    4   0.86   0.01   0.00   0.85         0.    0.0080
    8   24    5   0.04   0.00   0.00   0.02         0.    0.0016
    9   24    3   0.04   0.00   0.00   0.03         0.    0.0015
   10   25    1   0.06   0.00   0.00   0.04         0.    0.0021
   11   25    0   0.05   0.00   0.00   0.03         0.    0.0018
   12   26    0   1.64   0.01   0.00   1.60         0.    0.0186
   13   26    1   0.48   0.00   0.00   0.45         0.    0.0032
   14   26    0   0.51   0.00   0.00   0.48         0.    0.0031
   15   26    3   1.23   0.01   0.00   1.19         0.    0.0112
   16   11    4   0.33   0.18   0.00   0.33         0.    0.1309
   17   13    3   1.00   0.15   0.00   0.99         0.    0.1882
   18   13    0   1.16   0.18   0.00   1.14         0.    0.1800
   19   14    4   1.09   0.16   0.00   1.08         0.    0.1908
   20   14    1   1.23   0.19   0.00   1.21         0.    0.1882
   21   31    4   0.66   0.00   0.00   0.65         0.    0.0014
   22   31    5   0.73   0.00   0.00   0.71         0.    0.0013
   23   32    2   0.67   0.00   0.00   0.65         0.    0.0015
   24   32    2   0.71   0.00   0.00   0.69         0.    0.0014
   25    1    4   0.07   0.06   0.00   0.07         0.    0.0244
   26    2    0   0.65   0.35   0.00   0.65         0.    0.3369
   27    3    3   0.37   0.03   0.00   0.36         0.    0.0313
   28    3    2   0.16   0.02   0.00   0.13         0.    0.0197
   29    3    2   0.35   0.04   0.00   0.34         0.    0.0238
   30    4    3   0.51   0.04   0.00   0.50         0.    0.0349
   31    4    0   0.19   0.02   0.00   0.15         0.    0.0190
   32    4    5   0.42   0.04   0.00   0.41         0.    0.0258
   33   75    3   0.01   0.01   0.00   0.01         0.    0.0082
   34   45    5   0.08   0.05   0.00   0.07         0.    0.0000
   35   46    1   1.12   0.01   0.00   1.11         0.    0.0000
   36   46    4   1.19   0.01   0.00   1.17         0.    0.0000
   37   47    5   1.47   0.01   0.00   1.45         0.    0.0000
   38   47    2   1.46   0.01   0.00   1.44         0.    0.0000
================================================================
================ TIMING STATISTICS PER NODE    ================
node# twait    ths   tcin   teig  tvect  tga   ttotal 
  1   0.03   0.02   0.09   0.00   0.00   0.10   4.38
  2   0.02   0.02   0.09   0.00   0.00   0.10   4.38
  3   0.00   0.02   0.09   0.00   0.00   0.08   4.38
  4   0.02   0.02   0.09   0.00   0.00   0.09   4.38
  5   0.02   0.02   0.09   0.00   0.00   0.05   4.38
  6   0.00   0.02   0.09   0.00   0.00   0.08   4.38
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                    25.2949s 
time spent in multnx:                  24.5993s 
integral transfer time:                 0.0216s 
time spent for loop construction:       1.7464s 
syncronization time in mult:            0.0898s 
total time per CI iteration:           26.3055s 
 ** parallelization degree for mult section:0.9965

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)           1.04        0.00        742.374
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)          1.03        0.00        877.855
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.10        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:            2.16        0.00        725.721
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)         194.61        0.25        786.141
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)        194.61        0.25        765.961
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.43        0.00        207.717
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:          389.65        0.50        773.612
========================================================


          starting ci iteration   3

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:  214466 2x:   53393 4x:    8315
All internal counts: zz :   32118 yy:  490036 xx:  110475 ww:  123428
One-external counts: yz :  161582 yx:  486981 yw:  504573
Two-external counts: yy :  191370 ww:   45189 xx:   45699 xz:    5228 wz:    6717 wx:   69923
Three-ext.   counts: yx :   59965 yw:   63497

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 420

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.94187155    -0.11828406     0.02812267     0.31320277

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1   -155.3400920432  1.1578E-03  1.8239E-04  2.1553E-02  1.0000E-03
 mr-sdci #  3  2   -152.9443074650  1.6111E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mr-sdci #  3  3   -152.2930017408  2.1947E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mr-sdci #  3  4   -152.0619933313  8.9124E+00  0.0000E+00  4.5629E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   21    1   0.93   0.09   0.00   0.92         0.    0.1070
    2   22    4   1.00   0.01   0.00   0.99         0.    0.0126
    3   22    3   0.44   0.00   0.00   0.41         0.    0.0033
    4   22    2   0.77   0.01   0.00   0.75         0.    0.0085
    5   23    3   0.99   0.01   0.00   0.98         0.    0.0106
    6   23    3   2.36   0.00   0.00   2.32         0.    0.0031
    7   23    5   0.85   0.01   0.00   0.84         0.    0.0080
    8   24    2   0.03   0.00   0.00   0.02         0.    0.0016
    9   24    4   0.03   0.00   0.00   0.02         0.    0.0015
   10   25    3   0.04   0.00   0.00   0.03         0.    0.0021
   11   25    1   0.04   0.00   0.00   0.03         0.    0.0018
   12   26    0   1.66   0.01   0.00   1.62         0.    0.0186
   13   26    4   2.29   0.00   0.00   2.26         0.    0.0032
   14   26    0   2.36   0.00   0.00   2.32         0.    0.0031
   15   26    5   1.29   0.01   0.00   1.25         0.    0.0112
   16   11    0   0.33   0.18   0.00   0.33         0.    0.1309
   17   13    0   0.92   0.15   0.00   0.91         0.    0.1882
   18   13    1   1.11   0.18   0.00   1.09         0.    0.1800
   19   14    5   1.05   0.16   0.00   1.03         0.    0.1908
   20   14    1   1.21   0.19   0.00   1.19         0.    0.1882
   21   31    2   2.46   0.00   0.00   2.44         0.    0.0014
   22   31    3   0.66   0.00   0.00   0.65         0.    0.0013
   23   32    0   0.75   0.00   0.00   0.74         0.    0.0015
   24   32    4   0.78   0.00   0.00   0.76         0.    0.0014
   25    1    2   0.07   0.05   0.00   0.07         0.    0.0244
   26    2    5   2.51   2.19   0.00   2.51         0.    0.3369
   27    3    2   0.31   0.03   0.00   0.29         0.    0.0313
   28    3    5   0.13   0.02   0.00   0.11         0.    0.0197
   29    3    4   0.30   0.04   0.00   0.28         0.    0.0238
   30    4    1   2.37   0.04   0.00   2.36         0.    0.0349
   31    4    5   0.18   0.02   0.00   0.14         0.    0.0190
   32    4    1   0.38   0.04   0.00   0.37         0.    0.0258
   33   75    5   0.01   0.01   0.00   0.01         0.    0.0082
   34   45    4   0.07   0.05   0.00   0.07         0.    0.0000
   35   46    2   1.16   0.01   0.00   1.14         0.    0.0000
   36   46    2   1.27   0.01   0.00   1.25         0.    0.0000
   37   47    4   1.58   0.01   0.00   1.56         0.    0.0000
   38   47    3   1.54   0.01   0.00   1.52         0.    0.0000
================================================================
================ TIMING STATISTICS PER NODE    ================
node# twait    ths   tcin   teig  tvect  tga   ttotal 
  1   0.03   0.02   0.08   0.00   0.00   0.07   6.19
  2   0.01   0.02   0.08   0.00   0.00   0.06   6.19
  3   0.00   0.02   0.08   0.00   0.00   0.07   6.19
  4   0.02   0.02   0.08   0.00   0.00   0.10   6.19
  5   0.00   0.02   0.08   0.00   0.00   0.08   6.19
  6   0.02   0.02   0.08   0.00   0.00   0.09   6.19
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                    36.2204s 
time spent in multnx:                  35.5778s 
integral transfer time:                 0.0204s 
time spent for loop construction:       3.5797s 
syncronization time in mult:            0.0755s 
total time per CI iteration:           37.1648s 
 ** parallelization degree for mult section:0.9979

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)           1.55        0.00       1018.135
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)          1.55        0.00        604.561
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.10        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:            3.20        0.00        720.409
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)         194.10        0.24        818.965
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)        194.10        0.22        892.990
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.42        0.00        185.830
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:          388.62        0.46        851.068
========================================================


          starting ci iteration   4

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:  214466 2x:   53393 4x:    8315
All internal counts: zz :   32118 yy:  490036 xx:  110475 ww:  123428
One-external counts: yz :  161582 yx:  486981 yw:  504573
Two-external counts: yy :  191370 ww:   45189 xx:   45699 xz:    5228 wz:    6717 wx:   69923
Three-ext.   counts: yx :   59965 yw:   63497

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 420

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.94172291    -0.02371625

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1   -155.3402211189  1.2908E-04  4.7242E-05  1.0565E-02  1.0000E-03
 mr-sdci #  4  2   -152.9419007657 -2.4067E-03  0.0000E+00  4.8819E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   21    5   0.92   0.08   0.00   0.92         0.    0.1070
    2   22    1   0.93   0.01   0.00   0.92         0.    0.0126
    3   22    4   0.43   0.00   0.00   0.41         0.    0.0033
    4   22    1   0.76   0.01   0.00   0.75         0.    0.0085
    5   23    3   0.93   0.01   0.00   0.92         0.    0.0106
    6   23    4   0.47   0.00   0.00   0.44         0.    0.0031
    7   23    2   0.83   0.01   0.00   0.81         0.    0.0080
    8   24    2   0.02   0.00   0.00   0.02         0.    0.0016
    9   24    5   0.02   0.00   0.00   0.02         0.    0.0015
   10   25    3   0.03   0.00   0.00   0.03         0.    0.0021
   11   25    3   0.03   0.00   0.00   0.02         0.    0.0018
   12   26    5   1.57   0.01   0.00   1.55         0.    0.0186
   13   26    1   0.44   0.00   0.00   0.41         0.    0.0032
   14   26    3   0.47   0.00   0.00   0.45         0.    0.0031
   15   26    4   1.19   0.01   0.00   1.16         0.    0.0112
   16   11    3   0.31   0.18   0.00   0.31         0.    0.1309
   17   13    0   0.96   0.15   0.00   0.95         0.    0.1882
   18   13    0   1.02   0.18   0.00   1.00         0.    0.1800
   19   14    2   0.97   0.16   0.00   0.96         0.    0.1908
   20   14    0   1.01   0.19   0.00   1.00         0.    0.1882
   21   31    2   0.58   0.00   0.00   0.57         0.    0.0014
   22   31    5   0.72   0.00   0.00   0.70         0.    0.0013
   23   32    3   0.74   0.00   0.00   0.72         0.    0.0015
   24   32    4   0.77   0.00   0.00   0.75         0.    0.0014
   25    1    1   0.07   0.05   0.00   0.07         0.    0.0244
   26    2    0   0.68   0.36   0.00   0.68         0.    0.3369
   27    3    1   0.30   0.03   0.00   0.29         0.    0.0313
   28    3    0   0.11   0.02   0.00   0.10         0.    0.0197
   29    3    5   0.28   0.04   0.00   0.27         0.    0.0238
   30    4    5   0.43   0.04   0.00   0.42         0.    0.0349
   31    4    0   0.15   0.02   0.00   0.12         0.    0.0190
   32    4    2   0.32   0.04   0.00   0.31         0.    0.0258
   33   75    0   0.01   0.01   0.00   0.01         0.    0.0082
   34   45    2   0.07   0.05   0.00   0.07         0.    0.0000
   35   46    4   1.11   0.01   0.00   1.10         0.    0.0000
   36   46    2   1.16   0.01   0.00   1.15         0.    0.0000
   37   47    1   1.44   0.01   0.00   1.43         0.    0.0000
   38   47    3   1.43   0.01   0.00   1.41         0.    0.0000
================================================================
================ TIMING STATISTICS PER NODE    ================
node# twait    ths   tcin   teig  tvect  tga   ttotal 
  1   0.03   0.01   0.03   0.00   0.00   0.06   4.01
  2   0.03   0.01   0.03   0.00   0.00   0.05   4.01
  3   0.02   0.01   0.03   0.00   0.00   0.05   4.01
  4   0.03   0.01   0.03   0.00   0.00   0.06   4.01
  5   0.00   0.01   0.03   0.00   0.00   0.07   4.01
  6   0.03   0.01   0.03   0.00   0.00   0.05   4.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                    23.6785s 
time spent in multnx:                  23.2004s 
integral transfer time:                 0.0166s 
time spent for loop construction:       1.7323s 
syncronization time in mult:            0.1248s 
total time per CI iteration:           24.0729s 
 ** parallelization degree for mult section:0.9948

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)           2.06        0.00       1339.949
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)          2.06        0.00       1219.349
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.13        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:            4.25        0.00       1195.309
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)         193.59        0.17       1140.839
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)        193.59        0.17       1119.915
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.39        0.00        264.920
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:          387.56        0.34       1126.545
========================================================


          starting ci iteration   5

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:  214466 2x:   53393 4x:    8315
All internal counts: zz :   32118 yy:  490036 xx:  110475 ww:  123428
One-external counts: yz :  161582 yx:  486981 yw:  504573
Two-external counts: yy :  191370 ww:   45189 xx:   45699 xz:    5228 wz:    6717 wx:   69923
Three-ext.   counts: yx :   59965 yw:   63497

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 420

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.94118300     0.08705875    -0.04279459

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1   -155.3402711099  4.9991E-05  1.4653E-05  5.6210E-03  1.0000E-03
 mr-sdci #  5  2   -154.0539454892  1.1120E+00  0.0000E+00  2.3323E+00  1.0000E-04
 mr-sdci #  5  3   -152.0659021481 -2.2710E-01  0.0000E+00  4.1694E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   21    4   2.67   0.08   0.00   2.67         0.    0.1070
    2   22    2   0.94   0.01   0.00   0.93         0.    0.0126
    3   22    4   0.44   0.00   0.00   0.42         0.    0.0033
    4   22    1   2.49   0.01   0.00   2.48         0.    0.0085
    5   23    0   2.71   0.01   0.00   2.70         0.    0.0106
    6   23    4   0.46   0.00   0.00   0.43         0.    0.0031
    7   23    3   2.55   0.01   0.00   2.54         0.    0.0080
    8   24    4   0.03   0.00   0.00   0.02         0.    0.0016
    9   24    2   0.04   0.00   0.00   0.02         0.    0.0015
   10   25    5   0.05   0.00   0.00   0.03         0.    0.0021
   11   25    3   0.05   0.00   0.00   0.03         0.    0.0018
   12   26    0   1.53   0.01   0.00   1.51         0.    0.0186
   13   26    2   0.41   0.00   0.00   0.39         0.    0.0032
   14   26    1   0.45   0.00   0.00   0.43         0.    0.0031
   15   26    5   1.14   0.01   0.00   1.12         0.    0.0112
   16   11    2   0.31   0.18   0.00   0.31         0.    0.1309
   17   13    1   0.83   0.15   0.00   0.82         0.    0.1882
   18   13    4   0.89   0.17   0.00   0.89         0.    0.1800
   19   14    3   0.93   0.16   0.00   0.93         0.    0.1908
   20   14    5   0.94   0.18   0.00   0.93         0.    0.1882
   21   31    3   0.55   0.00   0.00   0.55         0.    0.0014
   22   31    0   0.61   0.00   0.00   0.60         0.    0.0013
   23   32    2   2.39   0.00   0.00   2.38         0.    0.0015
   24   32    5   2.43   0.00   0.00   2.42         0.    0.0014
   25    1    1   0.07   0.06   0.00   0.07         0.    0.0244
   26    2    5   0.62   0.34   0.00   0.62         0.    0.3369
   27    3    1   0.34   0.03   0.00   0.33         0.    0.0313
   28    3    0   0.15   0.02   0.00   0.13         0.    0.0197
   29    3    3   0.33   0.04   0.00   0.31         0.    0.0238
   30    4    0   0.38   0.04   0.00   0.37         0.    0.0349
   31    4    0   0.19   0.02   0.00   0.16         0.    0.0190
   32    4    5   0.38   0.04   0.00   0.36         0.    0.0258
   33   75    2   0.01   0.01   0.00   0.01         0.    0.0082
   34   45    2   0.07   0.05   0.00   0.07         0.    0.0000
   35   46    4   1.08   0.01   0.00   1.07         0.    0.0000
   36   46    3   1.14   0.01   0.00   1.14         0.    0.0000
   37   47    2   1.39   0.01   0.00   1.38         0.    0.0000
   38   47    1   1.39   0.01   0.00   1.38         0.    0.0000
================================================================
================ TIMING STATISTICS PER NODE    ================
node# twait    ths   tcin   teig  tvect  tga   ttotal 
  1   0.02   0.01   0.06   0.00   0.00   0.08   5.66
  2   0.01   0.01   0.06   0.00   0.00   0.04   5.66
  3   0.02   0.01   0.06   0.00   0.00   0.05   5.66
  4   0.02   0.01   0.06   0.00   0.00   0.05   5.66
  5   0.00   0.01   0.06   0.00   0.00   0.06   5.66
  6   0.02   0.01   0.06   0.00   0.00   0.05   5.66
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                    33.4019s 
time spent in multnx:                  32.9499s 
integral transfer time:                 0.0159s 
time spent for loop construction:       1.7014s 
syncronization time in mult:            0.0824s 
total time per CI iteration:           33.9691s 
 ** parallelization degree for mult section:0.9975

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)           0.52        0.00          0.000
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)          0.51        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.08        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:            1.11        0.00        993.424
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)         195.13        0.16       1246.942
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)        195.13        0.17       1116.137
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.44        0.00        298.133
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:          390.71        0.33       1173.993
========================================================


          starting ci iteration   6

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:  214466 2x:   53393 4x:    8315
All internal counts: zz :   32118 yy:  490036 xx:  110475 ww:  123428
One-external counts: yz :  161582 yx:  486981 yw:  504573
Two-external counts: yy :  191370 ww:   45189 xx:   45699 xz:    5228 wz:    6717 wx:   69923
Three-ext.   counts: yx :   59965 yw:   63497

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 420

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.94099665     0.07239640    -0.05859312    -0.03529374

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  6  1   -155.3402861170  1.5007E-05  2.8018E-06  2.9131E-03  1.0000E-03
 mr-sdci #  6  2   -154.6679258758  6.1398E-01  0.0000E+00  5.0670E+00  1.0000E-04
 mr-sdci #  6  3   -152.2104414180  1.4454E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mr-sdci #  6  4   -152.0629968930  1.0036E-03  0.0000E+00  4.6945E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   21    4   0.94   0.09   0.00   0.94         0.    0.1070
    2   22    0   0.95   0.01   0.00   0.94         0.    0.0126
    3   22    4   0.45   0.00   0.00   0.42         0.    0.0033
    4   22    2   0.75   0.01   0.00   0.73         0.    0.0085
    5   23    0   0.96   0.01   0.00   0.94         0.    0.0106
    6   23    3   0.49   0.00   0.00   0.46         0.    0.0031
    7   23    3   0.82   0.01   0.00   0.80         0.    0.0080
    8   24    1   0.03   0.00   0.00   0.02         0.    0.0016
    9   24    2   0.03   0.00   0.00   0.02         0.    0.0015
   10   25    4   0.05   0.00   0.00   0.03         0.    0.0021
   11   25    1   0.04   0.00   0.00   0.03         0.    0.0018
   12   26    5   1.57   0.01   0.00   1.54         0.    0.0186
   13   26    1   0.45   0.00   0.00   0.42         0.    0.0032
   14   26    2   0.50   0.00   0.00   0.47         0.    0.0031
   15   26    4   1.19   0.01   0.00   1.16         0.    0.0112
   16   11    5   0.32   0.18   0.00   0.32         0.    0.1309
   17   13    2   0.89   0.15   0.00   0.88         0.    0.1882
   18   13    1   0.99   0.18   0.00   0.97         0.    0.1800
   19   14    4   0.95   0.16   0.00   0.93         0.    0.1908
   20   14    3   1.03   0.19   0.00   1.01         0.    0.1882
   21   31    0   0.63   0.00   0.00   0.62         0.    0.0014
   22   31    5   0.71   0.00   0.00   0.69         0.    0.0013
   23   32    5   0.69   0.00   0.00   0.67         0.    0.0015
   24   32    1   0.73   0.00   0.00   0.71         0.    0.0014
   25    1    5   0.07   0.06   0.00   0.07         0.    0.0244
   26    2    5   0.64   0.34   0.00   0.64         0.    0.3369
   27    3    2   0.33   0.03   0.00   0.32         0.    0.0313
   28    3    0   0.15   0.02   0.00   0.12         0.    0.0197
   29    3    1   0.33   0.04   0.00   0.31         0.    0.0238
   30    4    3   0.47   0.04   0.00   0.46         0.    0.0349
   31    4    0   0.20   0.02   0.00   0.16         0.    0.0190
   32    4    4   0.40   0.04   0.00   0.38         0.    0.0258
   33   75    4   0.01   0.01   0.00   0.01         0.    0.0082
   34   45    2   0.07   0.05   0.00   0.07         0.    0.0000
   35   46    0   1.11   0.01   0.00   1.10         0.    0.0000
   36   46    3   1.17   0.01   0.00   1.16         0.    0.0000
   37   47    1   1.44   0.01   0.00   1.42         0.    0.0000
   38   47    2   1.42   0.01   0.00   1.41         0.    0.0000
================================================================
================ TIMING STATISTICS PER NODE    ================
node# twait    ths   tcin   teig  tvect  tga   ttotal 
  1   0.01   0.02   0.08   0.00   0.00   0.08   4.11
  2   0.00   0.02   0.08   0.00   0.00   0.08   4.11
  3   0.01   0.02   0.08   0.00   0.00   0.07   4.11
  4   0.02   0.02   0.08   0.00   0.00   0.07   4.11
  5   0.02   0.02   0.08   0.00   0.00   0.08   4.11
  6   0.01   0.02   0.08   0.00   0.00   0.05   4.11
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                    23.9597s 
time spent in multnx:                  23.3872s 
integral transfer time:                 0.0184s 
time spent for loop construction:       1.7214s 
syncronization time in mult:            0.0671s 
total time per CI iteration:           24.6350s 
 ** parallelization degree for mult section:0.9972

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)           0.52        0.00          0.000
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)          0.51        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.07        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:            1.10        0.00        827.472
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)         195.13        0.21        931.078
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)        195.13        0.20        971.751
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.45        0.00        244.121
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:          390.71        0.41        947.816
========================================================


          starting ci iteration   7

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:  214466 2x:   53393 4x:    8315
All internal counts: zz :   32118 yy:  490036 xx:  110475 ww:  123428
One-external counts: yz :  161582 yx:  486981 yw:  504573
Two-external counts: yy :  191370 ww:   45189 xx:   45699 xz:    5228 wz:    6717 wx:   69923
Three-ext.   counts: yx :   59965 yw:   63497

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 420

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.94086542    -0.13615566

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  7  1   -155.3402886185  2.5015E-06  9.1471E-07  1.2589E-03  1.0000E-03
 mr-sdci #  7  2   -152.6654481569 -2.0025E+00  0.0000E+00  2.6293E+01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   21    0   2.62   0.09   0.00   2.62         0.    0.1070
    2   22    5   2.68   0.01   0.00   2.67         0.    0.0126
    3   22    0   0.48   0.00   0.00   0.45         0.    0.0033
    4   22    3   0.79   0.01   0.00   0.77         0.    0.0085
    5   23    4   2.66   0.01   0.00   2.65         0.    0.0106
    6   23    5   0.52   0.00   0.00   0.48         0.    0.0031
    7   23    2   0.87   0.01   0.00   0.85         0.    0.0080
    8   24    1   0.04   0.00   0.00   0.02         0.    0.0016
    9   24    3   0.04   0.00   0.00   0.02         0.    0.0015
   10   25    0   0.06   0.00   0.00   0.04         0.    0.0021
   11   25    5   0.05   0.00   0.00   0.03         0.    0.0018
   12   26    0   1.57   0.01   0.00   1.54         0.    0.0186
   13   26    3   0.48   0.00   0.00   0.44         0.    0.0032
   14   26    4   0.53   0.00   0.00   0.49         0.    0.0031
   15   26    4   1.18   0.01   0.00   1.15         0.    0.0112
   16   11    5   0.33   0.18   0.00   0.33         0.    0.1309
   17   13    4   1.05   0.16   0.00   1.03         0.    0.1882
   18   13    2   2.70   0.18   0.00   2.68         0.    0.1800
   19   14    1   2.74   0.16   0.00   2.72         0.    0.1908
   20   14    3   2.75   0.19   0.00   2.74         0.    0.1882
   21   31    2   0.67   0.00   0.00   0.65         0.    0.0014
   22   31    1   0.73   0.00   0.00   0.71         0.    0.0013
   23   32    0   0.75   0.00   0.00   0.73         0.    0.0015
   24   32    5   0.79   0.00   0.00   0.76         0.    0.0014
   25    1    2   0.07   0.06   0.00   0.07         0.    0.0244
   26    2    3   0.70   0.36   0.00   0.69         0.    0.3369
   27    3    2   0.37   0.03   0.00   0.36         0.    0.0313
   28    3    1   0.17   0.02   0.00   0.14         0.    0.0197
   29    3    0   0.40   0.04   0.00   0.38         0.    0.0238
   30    4    1   0.55   0.04   0.00   0.53         0.    0.0349
   31    4    1   0.22   0.02   0.00   0.18         0.    0.0190
   32    4    4   0.47   0.04   0.00   0.45         0.    0.0258
   33   75    2   0.01   0.01   0.00   0.01         0.    0.0082
   34   45    5   0.07   0.05   0.00   0.07         0.    0.0000
   35   46    3   1.13   0.01   0.00   1.12         0.    0.0000
   36   46    2   1.18   0.01   0.00   1.17         0.    0.0000
   37   47    1   1.44   0.01   0.00   1.43         0.    0.0000
   38   47    5   1.44   0.01   0.00   1.42         0.    0.0000
================================================================
================ TIMING STATISTICS PER NODE    ================
node# twait    ths   tcin   teig  tvect  tga   ttotal 
  1   0.01   0.01   0.04   0.00   0.00   0.08   5.95
  2   0.00   0.01   0.04   0.00   0.00   0.11   5.95
  3   0.03   0.01   0.04   0.00   0.00   0.06   5.95
  4   0.01   0.01   0.04   0.00   0.00   0.07   5.95
  5   0.01   0.01   0.04   0.00   0.00   0.08   5.95
  6   0.01   0.01   0.04   0.00   0.00   0.07   5.95
================================================================
================ TIMING STATISTICS FOR JOB     ================
time spent in mult:                    35.2673s 
time spent in multnx:                  34.6208s 
integral transfer time:                 0.0208s 
time spent for loop construction:       1.7618s 
syncronization time in mult:            0.0678s 
total time per CI iteration:           35.6924s 
 ** parallelization degree for mult section:0.9981

==================== LOCAL GA TRANSFER ====================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)           1.04        0.00          0.000
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)          1.03        0.00        912.266
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.09        0.00          0.000
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:            2.15        0.00        924.028
========================================================


==================== NON-LOCAL GA TRANSFER ================
       type           volume        time          rate   
                       [MB]         [s]          [MB/s] 
    v  (read)         194.61        0.23        852.563
    v  (write)          0.00        0.00          0.000
    w  (read)           0.00        0.00          0.000
    w  (write)        194.61        0.24        814.453
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    drt(read)           0.43        0.00        236.328
    drt(write)          0.00        0.00          0.000
    3x (read)           0.00        0.00          0.000
    3x (write)          0.00        0.00          0.000
    4x (read)           0.00        0.00          0.000
    4x (write)          0.00        0.00          0.000
    dg (read)           0.00        0.00          0.000
    dg (write)          0.00        0.00          0.000
    ofdg(rd)            0.00        0.00          0.000
    ofdg (wt)           0.00        0.00          0.000
      total:          389.66        0.47        830.741
========================================================


          starting ci iteration   8

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:  214466 2x:   53393 4x:    8315
All internal counts: zz :   32118 yy:  490036 xx:  110475 ww:  123428
One-external counts: yz :  161582 yx:  486981 yw:  504573
Two-external counts: yy :  191370 ww:   45189 xx:   45699 xz:    5228 wz:    6717 wx:   69923
Three-ext.   counts: yx :   59965 yw:   63497

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


 calctciref: ... reading cirefv                                                       recamt= 420

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.94093722    -0.04272296     0.18993257

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  8  1   -155.3402895352  9.1676E-07  1.8477E-07  6.9194E-04  1.0000E-03
 mr-sdci #  8  2   -154.3352480968  1.6698E+00  0.0000E+00  2.5148E+01  1.0000E-04
 mr-sdci #  8  3   -151.9712898798 -2.3915E-01  0.0000E+00  4.8229E+01  1.0000E-04
 
 
 total energy in cosmo calc 
e(nroot) + repnuc=    -155.3402895352
dielectric energy =       0.0000000000
deltaediel =       0.0000000000
deltaelast =       0.0000000000
 

 mr-sdci  convergence criteria satisfied after  8 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  8  1   -155.3402895352  9.1676E-07  1.8477E-07  6.9194E-04  1.0000E-03
 mr-sdci #  8  2   -154.3352480968  1.6698E+00  0.0000E+00  2.5148E+01  1.0000E-04
 mr-sdci #  8  3   -151.9712898798 -2.3915E-01  0.0000E+00  4.8229E+01  1.0000E-04

####################CIUDGINFO####################

   ci vector at position   1 energy= -155.340289535218

################END OF CIUDGINFO################

 
    1 of the   4 expansion vectors are transformed.
 nnormold( 1 )= 0.922407638844894850
 nnormold( 2 )= 0.860103425571503256E-06
 nnormold( 3 )= 0.946742113462637810E-06
 nnormnew( 1 )= 0.922430559457150023
    1 of the   3 matrix-vector products are transformed.
 nnormold( 1 )= 137.079127799829621
 nnormold( 2 )= 0.948874908036847505E-04
 nnormold( 3 )= 0.120073680379369273E-03
 nnormnew( 1 )= 137.085071305807617

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
 maximum overlap with reference  1 (overlap= 0.940937215418346073 )

information on vector: 1from unit 11 written to unit 16filename civout                                                      


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =      -155.3402895352

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7    8    9   10   11   12   13

                                          orbital     5    6    7   33   34   35   36   37   38   65   66   76   77

                                         symmetry   Ag   Ag   Ag   Bu   Bu   Bu   Bu   Bu   Bu   Au   Au   Bg   Bg 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1 -0.154132                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-           
 z*  1  1       2  0.907436                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +-      
 z*  1  1       3  0.055542                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-        +     - 
 z*  1  1       4 -0.054637                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +-             +- 
 z*  1  1       5 -0.062647                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +-      
 z*  1  1       6 -0.104819                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -   +     - 
 z*  1  1       8  0.107482                        +-   +-   +-   +-   +-   +-   +-   +-   +-   +    +     -    - 
 z*  1  1       9 -0.062357                        +-   +-   +-   +-   +-   +-   +-   +-   +-        +-   +-      
 z*  1  1      11  0.025218                        +-   +-   +-   +-   +-   +-   +-   +-   +-        +-        +- 
 z*  1  1      12 -0.062509                        +-   +-   +-   +-   +-   +-   +-   +-   +-             +-   +- 
 y   1  1     422  0.013190              2( Au )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-    -           
 y   1  1     457  0.010579              1( Bg )   +-   +-   +-   +-   +-   +-   +-   +-   +-    -   +     -      
 y   1  1     502 -0.013210              1( Bg )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -    -      
 y   1  1     503  0.010744              2( Bg )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -    -      
 y   1  1     521 -0.012955              2( Au )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +          -    - 
 y   1  1     522 -0.010876              3( Au )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +          -    - 
 y   1  1     548 -0.013407              2( Au )   +-   +-   +-   +-   +-   +-   +-   +-   +-         -   +-      
 y   1  1     628 -0.014229              2( Ag )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +     -      
 y   1  1     630 -0.012727              4( Ag )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +     -      
 y   1  1     631  0.012129              5( Ag )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +     -      
 y   1  1     634  0.010619              8( Ag )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-   +     -      
 y   1  1     813  0.010288              9( Bu )   +-   +-   +-   +-   +-   +-   +-   +-    -   +     -   +-      
 y   1  1    1701  0.012870              1( Ag )   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +     -      
 y   1  1    1710 -0.015566             10( Ag )   +-   +-   +-   +-   +-   +-   +-    -   +-   +-   +     -      
 y   1  1    1780  0.012840              4( Bu )   +-   +-   +-   +-   +-   +-   +-    -   +-   +-        +     - 
 y   1  1    1781 -0.011195              5( Bu )   +-   +-   +-   +-   +-   +-   +-    -   +-   +-        +     - 
 y   1  1    1786 -0.012491             10( Bu )   +-   +-   +-   +-   +-   +-   +-    -   +-   +-        +     - 
 y   1  1    1882  0.010369              4( Bu )   +-   +-   +-   +-   +-   +-   +-    -   +-   +     -   +-      
 y   1  1    1883 -0.012229              5( Bu )   +-   +-   +-   +-   +-   +-   +-    -   +-   +     -   +-      
 y   1  1    1884 -0.010261              6( Bu )   +-   +-   +-   +-   +-   +-   +-    -   +-   +     -   +-      
 y   1  1    1888 -0.013170             10( Bu )   +-   +-   +-   +-   +-   +-   +-    -   +-   +     -   +-      
 y   1  1    1986 -0.010272              4( Ag )   +-   +-   +-   +-   +-   +-   +-    -   +-   +         +-    - 
 y   1  1    1992  0.012924             10( Ag )   +-   +-   +-   +-   +-   +-   +-    -   +-   +         +-    - 
 y   1  1    4203 -0.010840              3( Bu )   +-   +-   +-   +-   +-   +-    -   +-   +-   +     -   +-      
 y   1  1    4305 -0.011097              1( Ag )   +-   +-   +-   +-   +-   +-    -   +-   +-   +         +-    - 
 y   1  1    5943  0.013343              7( Bu )   +-   +-   +-   +-   +-   +-   +    +-   +-   +-         -    - 
 y   1  1    6122  0.013279              6( Ag )   +-   +-   +-   +-   +-   +-   +    +-   +-    -        +-    - 
 y   1  1    8640  0.010491              8( Bu )   +-   +-   +-   +-   +-    -   +-   +-   +-   +     -   +-      
 y   1  1   11117  0.015781              7( Ag )   +-   +-   +-   +-   +-   +    +-   +-   +-   +-    -    -      
 y   1  1   11168  0.012325              8( Bu )   +-   +-   +-   +-   +-   +    +-   +-   +-   +-         -    - 
 y   1  1   11244  0.012064              8( Bu )   +-   +-   +-   +-   +-   +    +-   +-   +-    -    -   +-      
 y   1  1   11347  0.011752              7( Ag )   +-   +-   +-   +-   +-   +    +-   +-   +-    -        +-    - 
 y   1  1   31257  0.012112              4( Bu )   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +     -      
 y   1  1   31259 -0.014147              6( Bu )   +-   +-    -   +-   +-   +-   +-   +-   +-   +-   +     -      
 y   1  1   31335  0.012158              5( Ag )   +-   +-    -   +-   +-   +-   +-   +-   +-   +-        +     - 
 y   1  1   31437  0.013438              5( Ag )   +-   +-    -   +-   +-   +-   +-   +-   +-   +     -   +-      
 y   1  1   31537  0.013644              5( Bu )   +-   +-    -   +-   +-   +-   +-   +-   +-   +         +-    - 
 y   1  1   31540 -0.010827              8( Bu )   +-   +-    -   +-   +-   +-   +-   +-   +-   +         +-    - 
 y   1  1   31543  0.010776             11( Bu )   +-   +-    -   +-   +-   +-   +-   +-   +-   +         +-    - 
 y   1  1   41880 -0.020607              5( Bu )   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +     -      
 y   1  1   41881 -0.013193              6( Bu )   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +     -      
 y   1  1   41883 -0.015256              8( Bu )   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +     -      
 y   1  1   41885 -0.013066             10( Bu )   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +     -      
 y   1  1   41886 -0.011106             11( Bu )   +-    -   +-   +-   +-   +-   +-   +-   +-   +-   +     -      
 y   1  1   41956  0.011966              4( Ag )   +-    -   +-   +-   +-   +-   +-   +-   +-   +-        +     - 
 y   1  1   41959  0.013413              7( Ag )   +-    -   +-   +-   +-   +-   +-   +-   +-   +-        +     - 
 y   1  1   41962 -0.017435             10( Ag )   +-    -   +-   +-   +-   +-   +-   +-   +-   +-        +     - 
 y   1  1   42058  0.010521              4( Ag )   +-    -   +-   +-   +-   +-   +-   +-   +-   +     -   +-      
 y   1  1   42061  0.014269              7( Ag )   +-    -   +-   +-   +-   +-   +-   +-   +-   +     -   +-      
 y   1  1   42063  0.011223              9( Ag )   +-    -   +-   +-   +-   +-   +-   +-   +-   +     -   +-      
 y   1  1   42064 -0.017398             10( Ag )   +-    -   +-   +-   +-   +-   +-   +-   +-   +     -   +-      
 y   1  1   42159  0.011746              5( Bu )   +-    -   +-   +-   +-   +-   +-   +-   +-   +         +-    - 
 y   1  1   54082 -0.011309              1( Bu )    -   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -      
 y   1  1   54084  0.011410              3( Bu )    -   +-   +-   +-   +-   +-   +-   +-   +-   +-   +     -      
 y   1  1   54365 -0.013026              5( Bu )    -   +-   +-   +-   +-   +-   +-   +-   +-   +         +-    - 
 y   1  1   54371 -0.010991             11( Bu )    -   +-   +-   +-   +-   +-   +-   +-   +-   +         +-    - 
 x   1  1   72933 -0.010370    7( Bu )   4( Bg )   +-   +-   +-   +-   +-   +-   +-   +-    -   +-         -      
 x   1  1  789095 -0.010358    7( Bu )   1( Au )   +-   +-    -   +-   +-   +-   +-   +    +     -   +-    -    - 
 x   1  1  963109  0.011893    5( Bu )   6( Bu )   +-    -   +-   +-   +-   +-   +-   +    +     -    -    -   +- 
 x   1  1  985616  0.012389    7( Bu )   3( Au )   +-    -   +-   +-   +-   +-    -   +    +-   +-    -   +-      
 x   1  1  987908  0.011036    6( Ag )   3( Au )   +-    -   +-   +-   +-   +-    -   +    +-   +-         -   +- 
 x   1  1 1182578  0.010457    4( Bu )   6( Bu )    -   +-   +-   +-   +-   +-    -   +    +-   +-    -        +- 
 w   1  1 1205290 -0.011228    1( Bg )   1( Bg )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +-                
 w   1  1 1206256 -0.014645    6( Ag )   7( Bu )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +          -      
 w   1  1 1206751  0.016352    1( Au )   1( Bg )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +          -      
 w   1  1 1206760  0.010792    1( Au )   2( Bg )   +-   +-   +-   +-   +-   +-   +-   +-   +-   +          -      
 w   1  1 1210467 -0.011320    1( Au )   1( Au )   +-   +-   +-   +-   +-   +-   +-   +-   +-             +-      
 w   1  1 1889009  0.010192    6( Ag )   7( Bu )   +-   +-   +    +-   +-   +-   +-   +-   +-        +-         - 
 w   1  1 2053522 -0.010260    6( Bu )   6( Bu )   +-   +-   +     -   +-   +-   +-        +-   +-   +     -   +- 
 w   1  1 2104868 -0.012915   10( Ag )  10( Bu )   +-   +    +-   +-   +-   +-   +-   +-         -        +-   +- 
 w   1  1 2252281 -0.010169    6( Bu )   8( Bu )   +-   +    +-    -   +-   +-   +-        +-   +-   +     -   +- 
 w   1  1 2277546 -0.010519    6( Bu )   6( Bu )   +-   +     -   +-   +-   +-   +-        +-   +-   +     -   +- 
 w   1  1 2311675  0.010497    2( Ag )   3( Bu )   +    +-   +-   +-   +-   +-   +-   +-   +-        +-         - 
 w   1  1 2312303  0.010961    3( Au )   4( Bg )   +    +-   +-   +-   +-   +-   +-   +-   +-        +-         - 
 w   1  1 2475973 -0.010187    6( Ag )   7( Ag )   +    +-   +-    -   +-   +-   +-        +-   +-   +     -   +- 
 w   1  1 2476306 -0.010175    7( Bu )   8( Bu )   +    +-   +-    -   +-   +-   +-        +-   +-   +     -   +- 
 w   1  1 2501576 -0.010128    6( Bu )   7( Bu )   +    +-    -   +-   +-   +-   +-        +-   +-   +     -   +- 

 ci coefficient statistics:
           rq > 0.1                4
      0.1> rq > 0.01              83
     0.01> rq > 0.001          13923
    0.001> rq > 0.0001        139970
   0.0001> rq > 0.00001       417958
  0.00001> rq > 0.000001      660118
 0.000001> rq                1318958
           all               2551014
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:   11918 2x:       0 4x:       0
All internal counts: zz :   32118 yy:       0 xx:       0 ww:       0
One-external counts: yz :       0 yx:       0 yw:       0
Two-external counts: yy :       0 ww:       0 xx:       0 xz:       0 wz:       0 wx:       0
Three-ext.   counts: yx :       0 yw:       0

SO-0ex       counts: zz :       0 yy:       0 xx:       0 ww:       0
SO-1ex       counts: yz :       0 yx:       0 yw:       0
SO-2ex       counts: yy :       0 xx:               0 wx:       0
====================================================================================================


  iref  icsf         v(icsf)             hv(icsf)
     1     1     -0.154132396389     23.899188087638
     2     2      0.907436164537   -140.627425314530
     3     3      0.055542355691     -8.611107858013
     4     4     -0.054636660359      8.475070537001
     5     5     -0.062646987462      9.711157295864
     6     6     -0.104819130240     16.261002864989
     7     7      0.005110002509     -0.792877082295
     8     8      0.107482192540    -16.671654231336
     9     9     -0.062356626823      9.671356541593
    10    10     -0.005067284239      0.786617691069
    11    11      0.025218206277     -3.914906054026
    12    12     -0.062509354540      9.698694994193
    13    13     -0.005567871408      0.863024943672
    14    14     -0.000370467133      0.057002221642
    15    15      0.001085141113     -0.168285602906
    16    16     -0.004071662279      0.630689837004
    17    17      0.000397897772     -0.061791547271
    18    18      0.000635452952     -0.098571642658
    19    19      0.000055806834     -0.008343283278
    20    20      0.000013358820     -0.002137635386
    21    21     -0.000206090817      0.031992456539
    22    22      0.001182704179     -0.183439027738
    23    23     -0.000057985720      0.008777984546
    24    24     -0.000081824450      0.012698819965
    25    25     -0.000198321000      0.030701209096
    26    26      0.000100645263     -0.015538597915
    27    27     -0.005092916013      0.789674868029
    28    28     -0.000243292956      0.037428532159
    29    29      0.000991576734     -0.153894756618
    30    30     -0.003938791923      0.610382763468
    31    31      0.000489694708     -0.075749367609
    32    32      0.000591753468     -0.091829496364
    33    33      0.000048588282     -0.007541023041

 number of reference csfs (nref) is    33.  root number (iroot) is  1.

 pople ci energy extrapolation is computed with 22 correlated electrons.

 eref      =   -154.980757874610   "relaxed" cnot**2         =   0.888308986034
 eci       =   -155.340289535218   deltae = eci - eref       =  -0.359531660608
 eci+dv1   =   -155.380445990944   dv1 = (1-cnot**2)*deltae  =  -0.040156455726
 eci+dv2   =   -155.385495039561   dv2 = dv1 / cnot**2       =  -0.045205504343
 eci+dv3   =   -155.391996368140   dv3 = dv1 / (2*cnot**2-1) =  -0.051706832922
 eci+pople =   -155.386023799493   ( 22e- scaled deltae )    =  -0.405265924883
NO coefficients and occupation numbers are written to nocoef_ci.1
 entering drivercid: firstcall,firstnonref= T F

################################################################################
 one electron density matrix calculation 
################################################################################


 workspace allocation information: mxcore=45810504
 
 space required:
 
    space required for calls in multd2:
       onex          832
       allin           0
       diagon       3830
    max.      ---------
       maxnex       3830
 
    total core space usage:
       maxnex       3830
    max k-seg     692754
    max k-ind       4326
              ---------
       totmax    1397990
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=    5435  DYX=   16031  DYW=   16939
   D0Z=    1746  D0Y=   22064  D0X=    7405  D0W=    8533
  DDZI=    2933 DDYI=   29580 DDXI=   11282 DDWI=   12531
  DDZE=       0 DDYE=    4326 DDXE=    1882 DDWE=    2107
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   2.0000000      2.0000000      2.0000000      2.0000000      1.9755815
   1.9725794      1.9714561      0.0130735      0.0121778      0.0106822
   0.0096895      0.0064795      0.0047380      0.0043438      0.0025181
   0.0021253      0.0014815      0.0011082      0.0009573      0.0008863
   0.0006515      0.0005043      0.0003953      0.0003712      0.0003343
   0.0002340      0.0001868      0.0001851      0.0001317      0.0000858
   0.0000696      0.0000460


*****   symmetry block SYM2   *****

 occupation numbers of nos
   1.9998909      1.9998852      1.9854675      1.9784929      1.9755563
   1.9727490      0.0148663      0.0128360      0.0109393      0.0096303
   0.0090486      0.0066646      0.0050854      0.0037479      0.0022779
   0.0016585      0.0010817      0.0009960      0.0009280      0.0006902
   0.0006339      0.0005107      0.0003904      0.0003505      0.0003216
   0.0001916      0.0001665      0.0000992      0.0000717      0.0000653
   0.0000544      0.0000490


*****   symmetry block SYM3   *****

 occupation numbers of nos
   1.9300855      0.1029428      0.0061144      0.0048059      0.0031388
   0.0027544      0.0020219      0.0007141      0.0004850      0.0004136
   0.0002879


*****   symmetry block SYM4   *****

 occupation numbers of nos
   1.8851192      0.0548967      0.0057229      0.0040187      0.0031180
   0.0021484      0.0008907      0.0006050      0.0005399      0.0004195
   0.0002857


 total number of electrons =   30.0000000000



          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        Ag  partial gross atomic populations
   ao class       1Ag        2Ag        3Ag        4Ag        5Ag        6Ag 
    C1_ s       0.742206   0.021422   1.979297   0.509169   0.912954   0.265946
    C2_ s       0.682281   1.978433   0.020586   1.262129   0.128498   0.794875
    H1_ s       0.188687   0.000016   0.000086   0.057805   0.539119   0.150850
    H2_ s       0.300374   0.000033   0.000043   0.037214   0.362870   0.000345
    H3_ s       0.086452   0.000096  -0.000012   0.133683   0.032140   0.760563
 
   ao class       7Ag        8Ag        9Ag       10Ag       11Ag       12Ag 
    C1_ s       0.562196   0.003287   0.006432   0.003557   0.004126   0.002834
    C2_ s       1.084770   0.005591   0.001194   0.005689   0.001022   0.002799
    H1_ s       0.011471   0.000213   0.003684   0.000100   0.001341   0.000263
    H2_ s       0.256073   0.000869   0.000723   0.000072   0.002842   0.000232
    H3_ s       0.056946   0.003112   0.000145   0.001264   0.000358   0.000352
 
   ao class      13Ag       14Ag       15Ag       16Ag       17Ag       18Ag 
    C1_ s       0.001558   0.003137   0.000672   0.000508   0.000626   0.000354
    C2_ s       0.002492   0.000289   0.001271   0.000759   0.000394   0.000455
    H1_ s       0.000168   0.000381   0.000113   0.000241   0.000174   0.000042
    H2_ s       0.000047   0.000451   0.000032   0.000079   0.000202   0.000150
    H3_ s       0.000473   0.000085   0.000429   0.000537   0.000085   0.000107
 
   ao class      19Ag       20Ag       21Ag       22Ag       23Ag       24Ag 
    C1_ s       0.000126   0.000309   0.000167   0.000157   0.000125   0.000113
    C2_ s       0.000158   0.000381   0.000378   0.000220   0.000116   0.000027
    H1_ s       0.000285   0.000132   0.000052   0.000058   0.000076   0.000038
    H2_ s       0.000382   0.000037   0.000057   0.000006   0.000058   0.000128
    H3_ s       0.000007   0.000028  -0.000003   0.000063   0.000020   0.000065
 
   ao class      25Ag       26Ag       27Ag       28Ag       29Ag       30Ag 
    C1_ s       0.000052   0.000019   0.000044   0.000048   0.000015   0.000032
    C2_ s       0.000093   0.000076   0.000031   0.000028   0.000015   0.000016
    H1_ s       0.000081   0.000004   0.000036   0.000038   0.000023   0.000019
    H2_ s       0.000005   0.000024   0.000043   0.000055   0.000007   0.000012
    H3_ s       0.000103   0.000111   0.000033   0.000016   0.000071   0.000007
 
   ao class      31Ag       32Ag 
    C1_ s       0.000015   0.000005
    C2_ s       0.000004   0.000002
    H1_ s       0.000005   0.000023
    H2_ s       0.000046   0.000004
    H3_ s       0.000000   0.000012

                        Bu  partial gross atomic populations
   ao class       1Bu        2Bu        3Bu        4Bu        5Bu        6Bu 
    C1_ s       0.775024   1.225331   1.029623   0.356811   0.933000   0.671039
    C2_ s       1.225254   0.775528   0.493342   0.831383   0.328031   0.565944
    H1_ s      -0.000112  -0.000310   0.182432   0.221034   0.083472   0.448243
    H2_ s      -0.000003  -0.000390   0.247523   0.006748   0.632322   0.023980
    H3_ s      -0.000272  -0.000274   0.032547   0.562517  -0.001270   0.263542
 
   ao class       7Bu        8Bu        9Bu       10Bu       11Bu       12Bu 
    C1_ s       0.003558   0.005406   0.002819   0.003844   0.003059   0.002705
    C2_ s       0.006104   0.006427   0.003700   0.003510   0.002321   0.003239
    H1_ s       0.001817   0.000252   0.001492   0.000394   0.001410   0.000199
    H2_ s       0.000681   0.000403   0.000200   0.001707   0.001733   0.000097
    H3_ s       0.002706   0.000348   0.002728   0.000175   0.000526   0.000424
 
   ao class      13Bu       14Bu       15Bu       16Bu       17Bu       18Bu 
    C1_ s       0.002432   0.002637   0.000789   0.000434   0.000239   0.000284
    C2_ s       0.001976   0.000355   0.001014   0.000803   0.000426   0.000436
    H1_ s       0.000159   0.000439   0.000167   0.000015   0.000265   0.000015
    H2_ s       0.000215   0.000286   0.000133   0.000064   0.000126   0.000138
    H3_ s       0.000303   0.000030   0.000174   0.000342   0.000026   0.000123
 
   ao class      19Bu       20Bu       21Bu       22Bu       23Bu       24Bu 
    C1_ s       0.000191   0.000330   0.000217   0.000089   0.000031   0.000125
    C2_ s       0.000078   0.000095   0.000318   0.000270   0.000068   0.000037
    H1_ s       0.000343   0.000054   0.000030   0.000007   0.000051   0.000075
    H2_ s       0.000291   0.000057   0.000050   0.000047   0.000042   0.000111
    H3_ s       0.000025   0.000155   0.000018   0.000097   0.000198   0.000003
 
   ao class      25Bu       26Bu       27Bu       28Bu       29Bu       30Bu 
    C1_ s       0.000068   0.000034   0.000034   0.000004   0.000008   0.000017
    C2_ s       0.000077   0.000025   0.000038   0.000059   0.000000   0.000020
    H1_ s       0.000087   0.000061   0.000026   0.000003   0.000034   0.000000
    H2_ s       0.000007   0.000063   0.000050   0.000009   0.000022   0.000000
    H3_ s       0.000082   0.000007   0.000018   0.000025   0.000007   0.000027
 
   ao class      31Bu       32Bu 
    C1_ s       0.000017   0.000009
    C2_ s      -0.000002   0.000013
    H1_ s       0.000020   0.000003
    H2_ s       0.000008   0.000019
    H3_ s       0.000011   0.000005

                        Au  partial gross atomic populations
   ao class       1Au        2Au        3Au        4Au        5Au        6Au 
    C1_ s       0.678053   0.065636   0.002735   0.001732   0.001366   0.001010
    C2_ s       1.238364   0.036112   0.003173   0.002416   0.001164   0.001355
    H1_ s       0.003714   0.000458   0.000146   0.000135   0.000130   0.000182
    H2_ s       0.003279   0.000495   0.000029   0.000148   0.000345   0.000002
    H3_ s       0.006676   0.000242   0.000032   0.000374   0.000133   0.000204
 
   ao class       7Au        8Au        9Au       10Au       11Au 
    C1_ s       0.001057   0.000284   0.000031   0.000106   0.000047
    C2_ s       0.000750   0.000269   0.000040   0.000031   0.000062
    H1_ s       0.000102   0.000054   0.000012   0.000265   0.000012
    H2_ s       0.000021   0.000026   0.000258   0.000000   0.000078
    H3_ s       0.000091   0.000082   0.000145   0.000012   0.000089

                        Bg  partial gross atomic populations
   ao class       1Bg        2Bg        3Bg        4Bg        5Bg        6Bg 
    C1_ s       1.234976   0.018739   0.002893   0.003013   0.001662   0.000754
    C2_ s       0.631915   0.035403   0.002399   0.000396   0.001040   0.001023
    H1_ s       0.006962   0.000197   0.000019   0.000451   0.000125   0.000002
    H2_ s       0.007573   0.000150   0.000173   0.000138   0.000060   0.000211
    H3_ s       0.003693   0.000407   0.000238   0.000021   0.000231   0.000158
 
   ao class       7Bg        8Bg        9Bg       10Bg       11Bg 
    C1_ s       0.000043   0.000189   0.000029   0.000095   0.000064
    C2_ s       0.000711   0.000322   0.000060   0.000044   0.000042
    H1_ s       0.000135   0.000047   0.000026   0.000150   0.000057
    H2_ s       0.000000   0.000039   0.000103   0.000121   0.000076
    H3_ s       0.000002   0.000007   0.000322   0.000010   0.000046


                        gross atomic populations
     ao           C1_        C2_        H1_        H2_        H3_
      s        12.056233  12.183058   1.911186   1.893583   1.955940
    total      12.056233  12.183058   1.911186   1.893583   1.955940
 

 Total number of electrons:   30.00000000

