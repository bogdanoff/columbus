 total ao core energy =  455.754838005
 MCSCF calculation performed for  1 DRT.

 DRT  first state   no.of aver. states   weights
  1   ground state          1             1.000

 DRT file header:
  title                                                                          
 Molecular symmetry group:    ag 
 Total number of electrons:    10
 Spin multiplicity:            1
 Number of active orbitals:    8
 Number of active electrons:   8
 Total number of CSFs:       468

 Number of active-double rotations:         2
 Number of active-active rotations:         0
 Number of double-virtual rotations:        3
 Number of active-virtual rotations:       22
 
 iter     emc (average)    demc       wnorm      knorm      apxde  qcoupl
    1   -383.2916830268  3.833E+02  7.559E-02  3.757E-01  1.145E-02  F   *not conv.*     
    2   -383.3068280013  1.514E-02  2.587E-02  2.750E-01  2.723E-03  F   *not conv.*     
    3   -383.3108243322  3.996E-03  1.301E-02  1.227E-01  5.830E-04  F   *not conv.*     
    4   -383.3116585063  8.342E-04  6.980E-03  4.977E-02  1.049E-04  F   *not conv.*     
    5   -383.3118078107  1.493E-04  3.380E-03  2.068E-02  1.886E-05  F   *not conv.*     
    6   -383.3118346359  2.683E-05  1.521E-03  8.669E-03  3.381E-06  F   *not conv.*     
    7   -383.3118394450  4.809E-06  6.610E-04  3.648E-03  6.043E-07  F   *not conv.*     
    8   -383.3118403045  8.595E-07  2.826E-04  2.657E-03  1.864E-07  T   *not conv.*     
    9   -383.3118404911  1.866E-07  9.091E-07  2.875E-07  1.308E-13  T   *not conv.*     

 final mcscf convergence values:
   10   -383.3118404911  4.547E-13  4.651E-07  2.512E-07  5.835E-14  T   *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 wt 1.000 total energy=     -383.311840491, rel. (eV)=   0.000000
   ------------------------------------------------------------


