1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      09/20/00    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

================================================================================
four external integ    0.25 MB location: local disk    
three external inte    0.25 MB location: local disk    
four external integ    0.25 MB location: local disk    
three external inte    0.25 MB location: local disk    
diagonal integrals     0.12 MB location: local disk    
off-diagonal integr    0.12 MB location: local disk    
 nsubmx= 16 lenci= 1311
global arrays:        43263   (    0.33 MB)
vdisk:                    0   (    0.00 MB)
drt:                 252658   (    0.96 MB)
================================================================================
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core            2999999 DP per process
 CIUDG version 5.9.3 (05-Dec-2002)
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
   NTYPE = 3,
   GSET  = 3,
   RTOLCI=0.00010000,
   VOUT  = 1,
   NBKITR= 1,
   NITER= 20,
   iden=1
   IVMODE=   3,
 /&end
 ------------------------------------------------------------------------
 bummer (warning):2:changed keyword: nbkitr=         0
 bummer (warning):changed keyword: davcor=         0

 ** list of control variables **
 nrfitr =   30      nvrfmx =   16      nvrfmn =    1
 lvlprt =    0      nroot  =    1      noldv  =    0      noldhv =    0
 nunitv =    1      ntype  =    3      nbkitr =    0      niter  =   20
 ivmode =    3      vout   =    1      istrt  =    0      iortls =    0
 nvbkmx =   16      ibktv  =   -1      ibkthv =   -1      frcsub =    0
 nvcimx =   16      icitv  =   -1      icithv =   -1      maxseg =    4
 iden   =    1      itran  =    0      froot  =    0      rtmode =    0
 ftcalc =    1      lrtshift=1.0000    ncouple=    1      skipso  =   F
 ncorel =    0      csfprn  =   1      ctol   = 1.00E-02  davcor  =   0


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-04    1.000E-04
 This is a AQCC calculation USING the mrci-code /  March 1997 (tm)
 =============================================
 ==========================================

 units and filenames:
    1: ( 6)    ciudgls                                                     
    2: ( 5)    ciudgin                                                     
    3: ( 7)    ciudgsm                                                     
    4: ( 4)    cihdiag                                                     
    5: ( 3)    cihpseu                                                     
    6: (10)    cihvfl                                                      
    7: (11)    civfl                                                       
    8: (12)    diagint                                                     
    9: (13)    ofdgint                                                     
   10: (17)    cidrtfl                                                     
   11: (20)    ciftdfl                                                     
   12: (21)    ciftofl                                                     
   13: (24)    ciftifl                                                     
   14: ( 8)    ciflind                                                     
   15: (15)    civin                                                       
   16: (16)    civout                                                      
   18: (18)    d1fl                                                        
   20: (53)    cifvfl                                                      
   21: (25)    ciscr4                                                      
   22: (41)    ciftotd                                                     
   23: (42)    flacpfd                                                     
   26: (26)    ciscr5                                                      
   27: (47)    restart                                                     
   28: (48)    civout                                                      
   29: (49)    cirefv                                                      
   31: (31)    fil4w                                                       
   32: (32)    fil4x                                                       
   33: (33)    fil3w                                                       
   34: (34)    fil3x                                                       
   36: (36)    aoints                                                      
   37: (37)    aoints2                                                     
   38: (38)    drtfil                                                      
   39: (50)    cisrtif                                                     
   42: (50)    mocoef                                                      
   45: (52)    nocoef_ci                                                   
   46: (53)    civfl_restart                                               
 ------------------------------------------------------------------------

 workspace allocation information: lcore=   2999999 mem1=1074180104 ifirst=-268380122

 integral file titles:
 Hermit Integral Program : SIFS version  hochtor2        Thu Jun  5 14:27:23 2003
 h2o pvdz                                                                        
 mo coefficients generated by scfpq                                              
 SIFS file created by program tran.      hochtor2        Thu Jun  5 14:27:24 2003

 core energy values from the integral file:
 energy( 1)=  9.187211027960E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -8.346680954729E+01, ietype=    5,   fcore energy of type: Vref(*) 

 total core repulsion energy = -7.427959851933E+01

 drt header information:
 h2o pvdz                                                                        
 spnorb, spnodd, lxyzir,hmult F F 0 0 0 0
 nmot  =    24 niot  =     5 nfct  =     0 nfvt  =     0
 nrow  =    20 nsym  =     4 ssym  =     1 lenbuf=  1600
 nwalk,xbar:         31        1        5       10       15
 nvalwt,nvalw:       31        1        5       10       15
 ncsft:            1311
 total number of valid internal walks:      31
 nvalz,nvaly,nvalx,nvalw =        1       5      10      15

 cisrt info file parameters:
 file number  12 blocksize   4095
 mxbld   4095
 nd4ext,nd2ext,nd0ext   380   190    30
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int     5955     5445     2496      618       66        0        0        0
 minbl4,minbl3,maxbl2   195   186   198
 maxbuf 32767
 number of external orbitals per symmetry block:   8   6   3   2
 nmsym   4 number of internal orbitals   5

 formula file title:
 Hermit Integral Program : SIFS version  hochtor2        Thu Jun  5 14:27:23 2003
 h2o pvdz                                                                        
 mo coefficients generated by scfpq                                              
 SIFS file created by program tran.      hochtor2        Thu Jun  5 14:27:24 2003
 h2o pvdz                                                                        
 file nmb.  24 block size     0
 pthz,pthy,pthx,pthw:     1     5    10    15 total internal walks:      31
 maxlp3,n3xtlp,n2xtlp,n1xtlp,n0xtlp,n2lp,n1lp,n0lp     0     0     0     0
 orbsym(*)= 1 1 2 1 3

 setref:        1 references kept,
                0 references were marked as invalid, out of
                1 total.
 limcnvrt: found 1 valid internal walksout of  1
  walks (skipping trailing invalids)
  ... adding  1 segmentation marks segtype= 1
 limcnvrt: found 5 valid internal walksout of  5
  walks (skipping trailing invalids)
  ... adding  5 segmentation marks segtype= 2
 limcnvrt: found 10 valid internal walksout of  10
  walks (skipping trailing invalids)
  ... adding  10 segmentation marks segtype= 3
 limcnvrt: found 15 valid internal walksout of  15
  walks (skipping trailing invalids)
  ... adding  15 segmentation marks segtype= 4

 number of external paths / symmetry
 vertex x      47      54      36      34
 vertex w      66      54      36      34



                   segmentation summary for type diagonal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1           1|         1|         0|         1|         0|         1|
 -------------------------------------------------------------------------------
  Y 2           5|        33|         1|         5|         1|         2|
 -------------------------------------------------------------------------------
  X 3          10|       445|        34|        10|         6|         3|
 -------------------------------------------------------------------------------
  W 4          15|       832|       479|        15|        16|         4|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>      1311


 297 dimension of the ci-matrix ->>>      1311


 297 dimension of the ci-matrix ->>>         7

                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      10       1        445          1      10       1
     2  4   1    25      two-ext wz   2X  4 1      15       1        832          1      15       1
     3  4   3    26      two-ext wx   2X  4 3      15      10        832        445      15      10
     4  2   1    11      one-ext yz   1X  2 1       5       1         33          1       5       1
     5  3   2    15      1ex3ex  yx   3X  3 2      10       5        445         33      10       5
     6  4   2    16      1ex3ex  yw   3X  4 2      15       5        832         33      15       5
     7  1   1     1      allint zz    OX  1 1       1       1          1          1       1       1
     8  2   2     5      0ex2ex yy    OX  2 2       5       5         33         33       5       5
     9  3   3     6      0ex2ex xx    OX  3 3      10      10        445        445      10      10
    10  4   4     7      0ex2ex ww    OX  4 4      15      15        832        832      15      15
    11  1   1    75      dg-024ext z  DG  1 1       1       1          1          1       1       1
    12  2   2    45      4exdg024 y   DG  2 2       5       5         33         33       5       5
    13  3   3    46      4exdg024 x   DG  3 3      10      10        445        445      10      10
    14  4   4    47      4exdg024 w   DG  4 4      15      15        832        832      15      15
----------------------------------------------------------------------------------------------------
 DIAGEL COUNTS: DG0X,DG2X,DG4X= 122 69 30
 diagonal elements written to file   4

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:         1 2x:         0 4x:         0
All internal counts: zz :         0 yy:         0 xx:         0 ww:         0
One-external counts: yz :         0 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         0    task #   2:         0    task #   3:         0    task #   4:         0
task #   5:         0    task #   6:         0    task #   7:        -1    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:         0    task #  12:         0
task #  13:         0    task #  14:         0    task #
 reference space has dimension       1

    root           eigenvalues
    ----           ------------
       1         -76.0253553484

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------


         vector  1 from unit 11 written to unit 49 filename cirefv                                                      

         vector  1 from unit 11 written to unit 49 filename cirefv                                                      

    ------------------------------------------------------------

 setopt2: mraqcc energy evaluation      gvalue=  0.377777778

    ------------------------------------------------------------


    reference energies:

  reference state used is:   1with energy      -76.0253553484

  ### active excitation selection ###

 Inactive orbitals: 1 2 3 4 5
    there are    1 all-active excitations of which    1 are references.

  ### end active excitation selection ###


################################################################################
 one electron density matrix calculation 
################################################################################


 workspace allocation information: mxcore= 2999996
 the acpf density matrix will be calculated

 space required:

    space required for calls in multd2:
       onex          112
       allin           0
       diagon        330
    max.      ---------
       maxnex        330

    total core space usage:
       maxnex        330
    max k-seg        832
    max k-ind         15
              ---------
       totmax       2024
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       0  DYX=       0  DYW=       0
   D0Z=       0  D0Y=       0  D0X=       0  D0W=       0
  DDZI=       1 DDYI=       0 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              1311
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:              16
 number of roots to converge:                             1
 number of iterations:                                   20
 residual norm convergence criteria:               0.000100

          starting ci iteration   1

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       122 2x:        69 4x:        30
All internal counts: zz :         0 yy:         6 xx:        24 ww:        55
One-external counts: yz :         9 yx:        82 yw:       111
Two-external counts: yy :        19 ww:        84 xx:        56 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:         8
task #   5:        75    task #   6:       104    task #   7:        -1    task #   8:         5
task #   9:        21    task #  10:        52    task #  11:         0    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  1  1    -76.0253553484  4.6629E-15  2.5320E-01  9.9888E-01  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.02   0.00   0.00   0.02
    2   25    0   0.00   0.00   0.00   0.00
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.02   0.00   0.00   0.02
    7    1    0   0.00   0.00   0.00   0.00
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.01   0.00   0.00   0.01
   10    7    0   0.02   0.00   0.00   0.02
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.01   0.00   0.00   0.01
   14   47    0   0.01   0.00   0.00   0.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1400s 
time spent in multnx:                   0.1400s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1400s 

          starting ci iteration   2

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       122 2x:        69 4x:        30
All internal counts: zz :         0 yy:         6 xx:        24 ww:        55
One-external counts: yz :         9 yx:        82 yw:       111
Two-external counts: yy :        19 ww:        84 xx:        56 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:         8
task #   5:        75    task #   6:       104    task #   7:        -1    task #   8:         5
task #   9:        21    task #  10:        52    task #  11:         0    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97618283    -0.21694949

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  2  1    -76.2243425458  1.9899E-01  6.4740E-03  1.5886E-01  1.0000E-04
 mraqcc  #  2  2    -71.9966063991 -2.2830E+00  0.0000E+00  1.7209E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.00   0.00   0.00   0.00
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.02   0.00   0.00   0.01
    7    1    0   0.00   0.00   0.00   0.00
    8    5    0   0.04   0.00   0.00   0.04
    9    6    0   0.01   0.00   0.00   0.01
   10    7    0   0.02   0.00   0.00   0.02
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.01   0.00   0.00   0.01
   14   47    0   0.00   0.00   0.00   0.00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1400s 
time spent in multnx:                   0.1300s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1500s 

          starting ci iteration   3

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       122 2x:        69 4x:        30
All internal counts: zz :         0 yy:         6 xx:        24 ww:        55
One-external counts: yz :         9 yx:        82 yw:       111
Two-external counts: yy :        19 ww:        84 xx:        56 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:         8
task #   5:        75    task #   6:       104    task #   7:        -1    task #   8:         5
task #   9:        21    task #  10:        52    task #  11:         0    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.97387612     0.03592314    -0.22422048

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  3  1    -76.2365496271  1.2207E-02  2.9859E-04  3.1494E-02  1.0000E-04
 mraqcc  #  3  2    -73.2333039766  1.2367E+00  0.0000E+00  1.8246E+00  1.0000E-04
 mraqcc  #  3  3    -72.1128419468 -2.1668E+00  0.0000E+00  1.9064E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.00   0.00   0.00   0.00
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.01   0.00   0.00   0.00
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.02   0.00   0.00   0.02
    7    1    0   0.00   0.00   0.00   0.00
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.01   0.00   0.00   0.01
   10    7    0   0.03   0.00   0.00   0.03
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.00   0.00   0.00   0.00
   14   47    0   0.01   0.00   0.00   0.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1300s 
time spent in multnx:                   0.1200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1400s 

          starting ci iteration   4

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       122 2x:        69 4x:        30
All internal counts: zz :         0 yy:         6 xx:        24 ww:        55
One-external counts: yz :         9 yx:        82 yw:       111
Two-external counts: yy :        19 ww:        84 xx:        56 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:         8
task #   5:        75    task #   6:       104    task #   7:        -1    task #   8:         5
task #   9:        21    task #  10:        52    task #  11:         0    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.97312619     0.10572133     0.04263237     0.20007725

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  4  1    -76.2372509188  7.0129E-04  2.2818E-05  7.5434E-03  1.0000E-04
 mraqcc  #  4  2    -74.1356364077  9.0233E-01  0.0000E+00  1.2046E+00  1.0000E-04
 mraqcc  #  4  3    -73.0516993227  9.3886E-01  0.0000E+00  1.6549E+00  1.0000E-04
 mraqcc  #  4  4    -71.6753748967 -2.6042E+00  0.0000E+00  2.2474E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.00   0.00   0.00   0.00
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.01   0.00   0.00   0.01
   10    7    0   0.02   0.00   0.00   0.02
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.01   0.00   0.00   0.01
   14   47    0   0.00   0.00   0.00   0.00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

          starting ci iteration   5

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       122 2x:        69 4x:        30
All internal counts: zz :         0 yy:         6 xx:        24 ww:        55
One-external counts: yz :         9 yx:        82 yw:       111
Two-external counts: yy :        19 ww:        84 xx:        56 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:         8
task #   5:        75    task #   6:       104    task #   7:        -1    task #   8:         5
task #   9:        21    task #  10:        52    task #  11:         0    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     0.97301068    -0.05509645     0.09419517    -0.08176401     0.18616261

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  5  1    -76.2372935088  4.2590E-05  1.4154E-06  2.1330E-03  1.0000E-04
 mraqcc  #  5  2    -74.8576147160  7.2198E-01  0.0000E+00  9.9618E-01  1.0000E-04
 mraqcc  #  5  3    -74.0473696789  9.9567E-01  0.0000E+00  1.3795E+00  1.0000E-04
 mraqcc  #  5  4    -72.6267927369  9.5142E-01  0.0000E+00  1.4150E+00  1.0000E-04
 mraqcc  #  5  5    -71.4998900234 -2.7797E+00  0.0000E+00  2.3947E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.00   0.00   0.00   0.00
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.02   0.00   0.00   0.01
    7    1    0   0.00   0.00   0.00   0.00
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.01   0.00   0.00   0.01
   10    7    0   0.02   0.00   0.00   0.01
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.01   0.00   0.00   0.01
   14   47    0   0.01   0.00   0.00   0.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1300s 
time spent in multnx:                   0.1100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

          starting ci iteration   6

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       122 2x:        69 4x:        30
All internal counts: zz :         0 yy:         6 xx:        24 ww:        55
One-external counts: yz :         9 yx:        82 yw:       111
Two-external counts: yy :        19 ww:        84 xx:        56 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:         8
task #   5:        75    task #   6:       104    task #   7:        -1    task #   8:         5
task #   9:        21    task #  10:        52    task #  11:         0    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.97299986     0.06099431     0.05103907    -0.09222398    -0.09993989    -0.16867938

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  6  1    -76.2372963377  2.8288E-06  6.9760E-08  4.3826E-04  1.0000E-04
 mraqcc  #  6  2    -74.9094378548  5.1823E-02  0.0000E+00  8.9070E-01  1.0000E-04
 mraqcc  #  6  3    -74.3766176507  3.2925E-01  0.0000E+00  1.1089E+00  1.0000E-04
 mraqcc  #  6  4    -73.1743588870  5.4757E-01  0.0000E+00  1.8386E+00  1.0000E-04
 mraqcc  #  6  5    -72.6113078070  1.1114E+00  0.0000E+00  1.3012E+00  1.0000E-04
 mraqcc  #  6  6    -71.3208435007 -2.9588E+00  0.0000E+00  2.3127E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.01   0.00   0.00   0.01
    9    6    0   0.02   0.00   0.00   0.02
   10    7    0   0.01   0.00   0.00   0.01
   11   75    0   0.01   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.00   0.00   0.00   0.00
   14   47    0   0.01   0.00   0.00   0.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1300s 
time spent in multnx:                   0.1200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1400s 

          starting ci iteration   7

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       122 2x:        69 4x:        30
All internal counts: zz :         0 yy:         6 xx:        24 ww:        55
One-external counts: yz :         9 yx:        82 yw:       111
Two-external counts: yy :        19 ww:        84 xx:        56 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:         8
task #   5:        75    task #   6:       104    task #   7:        -1    task #   8:         5
task #   9:        21    task #  10:        52    task #  11:         0    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.97300019     0.02965243     0.08163995     0.01659018     0.10274126     0.08247805    -0.16760866

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  7  1    -76.2372965023  1.6461E-07  6.4464E-09  1.3466E-04  1.0000E-04
 mraqcc  #  7  2    -75.2121448227  3.0271E-01  0.0000E+00  7.9061E-01  1.0000E-04
 mraqcc  #  7  3    -74.4856193995  1.0900E-01  0.0000E+00  9.5261E-01  1.0000E-04
 mraqcc  #  7  4    -74.1990461274  1.0247E+00  0.0000E+00  1.2859E+00  1.0000E-04
 mraqcc  #  7  5    -72.9543092207  3.4300E-01  0.0000E+00  1.8230E+00  1.0000E-04
 mraqcc  #  7  6    -72.5139450441  1.1931E+00  0.0000E+00  1.5518E+00  1.0000E-04
 mraqcc  #  7  7    -71.2957683988 -2.9838E+00  0.0000E+00  2.3670E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.00   0.00   0.00   0.00
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.01   0.00   0.00   0.01
   10    7    0   0.02   0.00   0.00   0.02
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.01   0.00   0.00   0.01
   14   47    0   0.00   0.00   0.00   0.00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.1300s 

          starting ci iteration   8

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       122 2x:        69 4x:        30
All internal counts: zz :         0 yy:         6 xx:        24 ww:        55
One-external counts: yz :         9 yx:        82 yw:       111
Two-external counts: yy :        19 ww:        84 xx:        56 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:         8
task #   5:        75    task #   6:       104    task #   7:        -1    task #   8:         5
task #   9:        21    task #  10:        52    task #  11:         0    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.97300018    -0.01832812    -0.08685596     0.01193084    -0.01329193    -0.12118810    -0.06395924     0.16215550

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  8  1    -76.2372965143  1.2012E-08  4.7244E-10  3.6168E-05  1.0000E-04
 mraqcc  #  8  2    -75.3972629317  1.8512E-01  0.0000E+00  5.9892E-01  1.0000E-04
 mraqcc  #  8  3    -74.5425371037  5.6918E-02  0.0000E+00  8.3677E-01  1.0000E-04
 mraqcc  #  8  4    -74.2095881216  1.0542E-02  0.0000E+00  1.2278E+00  1.0000E-04
 mraqcc  #  8  5    -73.7429911201  7.8868E-01  0.0000E+00  1.2896E+00  1.0000E-04
 mraqcc  #  8  6    -72.7076342519  1.9369E-01  0.0000E+00  1.8617E+00  1.0000E-04
 mraqcc  #  8  7    -72.5061990194  1.2104E+00  0.0000E+00  1.7094E+00  1.0000E-04
 mraqcc  #  8  8    -71.2536216033 -3.0260E+00  0.0000E+00  2.3150E+00  1.0000E-04


 mraqcc   convergence criteria satisfied after  8 iterations.

 final mraqcc   convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  8  1    -76.2372965143  1.2012E-08  4.7244E-10  3.6168E-05  1.0000E-04
 mraqcc  #  8  2    -75.3972629317  1.8512E-01  0.0000E+00  5.9892E-01  1.0000E-04
 mraqcc  #  8  3    -74.5425371037  5.6918E-02  0.0000E+00  8.3677E-01  1.0000E-04
 mraqcc  #  8  4    -74.2095881216  1.0542E-02  0.0000E+00  1.2278E+00  1.0000E-04
 mraqcc  #  8  5    -73.7429911201  7.8868E-01  0.0000E+00  1.2896E+00  1.0000E-04
 mraqcc  #  8  6    -72.7076342519  1.9369E-01  0.0000E+00  1.8617E+00  1.0000E-04
 mraqcc  #  8  7    -72.5061990194  1.2104E+00  0.0000E+00  1.7094E+00  1.0000E-04
 mraqcc  #  8  8    -71.2536216033 -3.0260E+00  0.0000E+00  2.3150E+00  1.0000E-04

####################CIUDGINFO####################

   aqcc vector at position   1 energy=  -76.237296514276

################END OF CIUDGINFO################


 a4den factor =  1.034282513 for root   1
    1 of the   9 expansion vectors are transformed.
    1 of the   8 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
 maximum overlap with reference  1(overlap=  0.973000183)
 reference energy= -76.0253553
 total aqcc energy= -76.2372965
 diagonal element shift (lrtshift) =  -0.211941166

information on vector: 1from unit 11 written to unit 16filename civout                                                      


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -76.2372965143

                                                       internal orbitals

                                          level       1    2    3    4    5

                                          orbital     1    2   12    3   19

                                         symmetry   a1   a1   b1   a1   b2 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1  0.973000                        +-   +-   +-   +-   +- 
 y   1  1       5 -0.010002              1( a1 )   +-   +-   +-    -   +- 
 x   1  1      35 -0.014733    1( a1 )   1( b2 )   +-   +-   +-    -    - 
 x   1  1      36  0.018437    2( a1 )   1( b2 )   +-   +-   +-    -    - 
 x   1  1      37 -0.032765    3( a1 )   1( b2 )   +-   +-   +-    -    - 
 x   1  1      46 -0.011548    4( a1 )   2( b2 )   +-   +-   +-    -    - 
 x   1  1      47 -0.011942    5( a1 )   2( b2 )   +-   +-   +-    -    - 
 x   1  1      54 -0.010628    4( a1 )   3( b2 )   +-   +-   +-    -    - 
 x   1  1      55 -0.010691    5( a1 )   3( b2 )   +-   +-   +-    -    - 
 x   1  1      57 -0.014059    7( a1 )   3( b2 )   +-   +-   +-    -    - 
 x   1  1      60 -0.015221    2( b1 )   1( a2 )   +-   +-   +-    -    - 
 x   1  1      71  0.026869    1( b1 )   1( b2 )   +-   +-    -   +-    - 
 x   1  1      72  0.015039    2( b1 )   1( b2 )   +-   +-    -   +-    - 
 x   1  1      73 -0.027377    3( b1 )   1( b2 )   +-   +-    -   +-    - 
 x   1  1      75 -0.010046    5( b1 )   1( b2 )   +-   +-    -   +-    - 
 x   1  1      78  0.014260    2( b1 )   2( b2 )   +-   +-    -   +-    - 
 x   1  1      90 -0.020879    2( a1 )   1( a2 )   +-   +-    -   +-    - 
 x   1  1      93  0.011659    5( a1 )   1( a2 )   +-   +-    -   +-    - 
 x   1  1      94  0.011186    6( a1 )   1( a2 )   +-   +-    -   +-    - 
 x   1  1      98 -0.010384    2( a1 )   2( a2 )   +-   +-    -   +-    - 
 x   1  1     102  0.011355    6( a1 )   2( a2 )   +-   +-    -   +-    - 
 x   1  1     104  0.012256    8( a1 )   2( a2 )   +-   +-    -   +-    - 
 x   1  1     105  0.010410    1( a1 )   1( b1 )   +-   +-    -    -   +- 
 x   1  1     107  0.025367    3( a1 )   1( b1 )   +-   +-    -    -   +- 
 x   1  1     115  0.023092    3( a1 )   2( b1 )   +-   +-    -    -   +- 
 x   1  1     116  0.020747    4( a1 )   2( b1 )   +-   +-    -    -   +- 
 x   1  1     119  0.011719    7( a1 )   2( b1 )   +-   +-    -    -   +- 
 x   1  1     122  0.013190    2( a1 )   3( b1 )   +-   +-    -    -   +- 
 x   1  1     123 -0.018661    3( a1 )   3( b1 )   +-   +-    -    -   +- 
 x   1  1     139 -0.012446    3( a1 )   5( b1 )   +-   +-    -    -   +- 
 x   1  1     159 -0.015374    1( a1 )   1( b2 )   +-    -   +-   +-    - 
 x   1  1     163  0.015302    5( a1 )   1( b2 )   +-    -   +-   +-    - 
 x   1  1     196  0.011016    1( a1 )   3( a1 )   +-    -   +-    -   +- 
 x   1  1     242 -0.011062    1( a1 )   1( b1 )   +-    -    -   +-   +- 
 x   1  1     258  0.010517    1( a1 )   3( b1 )   +-    -    -   +-   +- 
 w   1  1     480 -0.011813    1( a1 )   1( a1 )   +-   +-   +-   +-      
 w   1  1     481  0.010977    1( a1 )   2( a1 )   +-   +-   +-   +-      
 w   1  1     494 -0.013246    5( a1 )   5( a1 )   +-   +-   +-   +-      
 w   1  1     496  0.011014    2( a1 )   6( a1 )   +-   +-   +-   +-      
 w   1  1     513 -0.010210    6( a1 )   8( a1 )   +-   +-   +-   +-      
 w   1  1     537 -0.049436    1( b2 )   1( b2 )   +-   +-   +-   +-      
 w   1  1     541 -0.010155    2( b2 )   3( b2 )   +-   +-   +-   +-      
 w   1  1     542 -0.012684    3( b2 )   3( b2 )   +-   +-   +-   +-      
 w   1  1     544 -0.011102    1( a2 )   2( a2 )   +-   +-   +-   +-      
 w   1  1     545 -0.012371    2( a2 )   2( a2 )   +-   +-   +-   +-      
 w   1  1     547 -0.013897    2( a1 )   1( b2 )   +-   +-   +-   +     - 
 w   1  1     548  0.035914    3( a1 )   1( b2 )   +-   +-   +-   +     - 
 w   1  1     550  0.010015    5( a1 )   1( b2 )   +-   +-   +-   +     - 
 w   1  1     571  0.012584    2( b1 )   1( a2 )   +-   +-   +-   +     - 
 w   1  1     577  0.010365    2( b1 )   2( a2 )   +-   +-   +-   +     - 
 w   1  1     582 -0.015940    1( a1 )   1( a1 )   +-   +-   +-        +- 
 w   1  1     583  0.016636    1( a1 )   2( a1 )   +-   +-   +-        +- 
 w   1  1     584 -0.015329    2( a1 )   2( a1 )   +-   +-   +-        +- 
 w   1  1     585 -0.012768    1( a1 )   3( a1 )   +-   +-   +-        +- 
 w   1  1     586  0.017671    2( a1 )   3( a1 )   +-   +-   +-        +- 
 w   1  1     587 -0.034154    3( a1 )   3( a1 )   +-   +-   +-        +- 
 w   1  1     591 -0.014957    4( a1 )   4( a1 )   +-   +-   +-        +- 
 w   1  1     594 -0.016580    3( a1 )   5( a1 )   +-   +-   +-        +- 
 w   1  1     609 -0.011498    7( a1 )   7( a1 )   +-   +-   +-        +- 
 w   1  1     619 -0.018304    1( b1 )   2( b1 )   +-   +-   +-        +- 
 w   1  1     620 -0.025094    2( b1 )   2( b1 )   +-   +-   +-        +- 
 w   1  1     629  0.013170    2( b1 )   5( b1 )   +-   +-   +-        +- 
 w   1  1     644 -0.010519    3( b2 )   3( b2 )   +-   +-   +-        +- 
 w   1  1     648 -0.025898    1( b1 )   1( b2 )   +-   +-   +    +-    - 
 w   1  1     649 -0.015591    2( b1 )   1( b2 )   +-   +-   +    +-    - 
 w   1  1     650  0.028009    3( b1 )   1( b2 )   +-   +-   +    +-    - 
 w   1  1     652  0.011528    5( b1 )   1( b2 )   +-   +-   +    +-    - 
 w   1  1     655 -0.011171    2( b1 )   2( b2 )   +-   +-   +    +-    - 
 w   1  1     682  0.021189    1( a1 )   1( b1 )   +-   +-   +     -   +- 
 w   1  1     683 -0.023424    2( a1 )   1( b1 )   +-   +-   +     -   +- 
 w   1  1     684  0.019736    3( a1 )   1( b1 )   +-   +-   +     -   +- 
 w   1  1     690  0.019490    1( a1 )   2( b1 )   +-   +-   +     -   +- 
 w   1  1     691 -0.036923    2( a1 )   2( b1 )   +-   +-   +     -   +- 
 w   1  1     692  0.010802    3( a1 )   2( b1 )   +-   +-   +     -   +- 
 w   1  1     694  0.010127    5( a1 )   2( b1 )   +-   +-   +     -   +- 
 w   1  1     695  0.012745    6( a1 )   2( b1 )   +-   +-   +     -   +- 
 w   1  1     699  0.011921    2( a1 )   3( b1 )   +-   +-   +     -   +- 
 w   1  1     700 -0.020352    3( a1 )   3( b1 )   +-   +-   +     -   +- 
 w   1  1     731  0.013638    2( b2 )   1( a2 )   +-   +-   +     -   +- 
 w   1  1     736 -0.014400    1( a1 )   1( a1 )   +-   +-        +-   +- 
 w   1  1     737  0.019656    1( a1 )   2( a1 )   +-   +-        +-   +- 
 w   1  1     738 -0.027834    2( a1 )   2( a1 )   +-   +-        +-   +- 
 w   1  1     772 -0.028436    1( b1 )   1( b1 )   +-   +-        +-   +- 
 w   1  1     773 -0.028849    1( b1 )   2( b1 )   +-   +-        +-   +- 
 w   1  1     774 -0.035255    2( b1 )   2( b1 )   +-   +-        +-   +- 
 w   1  1     775  0.026135    1( b1 )   3( b1 )   +-   +-        +-   +- 
 w   1  1     776  0.012453    2( b1 )   3( b1 )   +-   +-        +-   +- 
 w   1  1     777 -0.021401    3( b1 )   3( b1 )   +-   +-        +-   +- 
 w   1  1     783  0.014945    2( b1 )   5( b1 )   +-   +-        +-   +- 
 w   1  1     786 -0.010244    5( b1 )   5( b1 )   +-   +-        +-   +- 
 w   1  1     799 -0.014763    1( a2 )   1( a2 )   +-   +-        +-   +- 
 w   1  1     802  0.026169    1( a1 )   1( b2 )   +-   +    +-   +-    - 
 w   1  1     803 -0.018204    2( a1 )   1( b2 )   +-   +    +-   +-    - 
 w   1  1     805  0.014175    4( a1 )   1( b2 )   +-   +    +-   +-    - 
 w   1  1     806 -0.021304    5( a1 )   1( b2 )   +-   +    +-   +-    - 
 w   1  1     807  0.016891    6( a1 )   1( b2 )   +-   +    +-   +-    - 
 w   1  1     819  0.011012    2( a1 )   3( b2 )   +-   +    +-   +-    - 
 w   1  1     832 -0.011401    1( b1 )   2( a2 )   +-   +    +-   +-    - 
 w   1  1     838 -0.016225    1( a1 )   1( a1 )   +-   +    +-    -   +- 
 w   1  1     839  0.021648    1( a1 )   2( a1 )   +-   +    +-    -   +- 
 w   1  1     840 -0.018980    2( a1 )   2( a1 )   +-   +    +-    -   +- 
 w   1  1     841 -0.016162    1( a1 )   3( a1 )   +-   +    +-    -   +- 
 w   1  1     846 -0.012540    3( a1 )   4( a1 )   +-   +    +-    -   +- 
 w   1  1     850  0.019844    3( a1 )   5( a1 )   +-   +    +-    -   +- 
 w   1  1     904  0.021006    1( a1 )   1( b1 )   +-   +     -   +-   +- 
 w   1  1     905 -0.012017    2( a1 )   1( b1 )   +-   +     -   +-   +- 
 w   1  1     907  0.012849    4( a1 )   1( b1 )   +-   +     -   +-   +- 
 w   1  1     908 -0.012075    5( a1 )   1( b1 )   +-   +     -   +-   +- 
 w   1  1     912  0.012534    1( a1 )   2( b1 )   +-   +     -   +-   +- 
 w   1  1     913 -0.021028    2( a1 )   2( b1 )   +-   +     -   +-   +- 
 w   1  1     915  0.014098    4( a1 )   2( b1 )   +-   +     -   +-   +- 
 w   1  1     919 -0.010390    8( a1 )   2( b1 )   +-   +     -   +-   +- 
 w   1  1     920 -0.021832    1( a1 )   3( b1 )   +-   +     -   +-   +- 
 w   1  1     921  0.014660    2( a1 )   3( b1 )   +-   +     -   +-   +- 
 w   1  1     924  0.017635    5( a1 )   3( b1 )   +-   +     -   +-   +- 
 w   1  1     953  0.012208    2( b2 )   1( a2 )   +-   +     -   +-   +- 
 w   1  1     958 -0.012228    1( a1 )   1( a1 )   +-        +-   +-   +- 
 w   1  1     959  0.014717    1( a1 )   2( a1 )   +-        +-   +-   +- 
 w   1  1     960 -0.013481    2( a1 )   2( a1 )   +-        +-   +-   +- 

 ci coefficient statistics:
           rq > 0.1                1
      0.1> rq > 0.01             118
     0.01> rq > 0.001            585
    0.001> rq > 0.0001           420
   0.0001> rq > 0.00001          140
  0.00001> rq > 0.000001          43
 0.000001> rq                      4
           all                  1311
NO coefficients and occupation numbers are written to nocoef_ci.1
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=       3  D0X=       9  D0W=      15
  DDZI=       1 DDYI=       9 DDXI=      26 DDWI=      34
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================
 root #  1: Scaling(2) with   1.03428251corresponding to ref #  1


*****   symmetry block  A1    *****

 occupation numbers of nos
   1.9999182      1.9851103      1.9673672      0.0242152      0.0110288
   0.0056059      0.0045176      0.0011480      0.0006067      0.0004983
   0.0000583


*****   symmetry block  B1    *****

 occupation numbers of nos
   1.9646970      0.0262128      0.0058625      0.0011741      0.0006751
   0.0005046      0.0000403


*****   symmetry block  B2    *****

 occupation numbers of nos
   1.9737129      0.0162070      0.0044129      0.0005501


*****   symmetry block  A2    *****

 occupation numbers of nos
   0.0051303      0.0007455


 total number of electrons =   10.0000000000



          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!


                        A1  partial gross atomic populations
   ao class       1A1        2A1        3A1        4A1        5A1        6A1 
    O1_ s       2.000573   1.689517  -0.051155   0.001806   0.005458   0.001317
    O1_ p       0.000089   0.041632   1.453298   0.010736   0.004390   0.000039
    O1_ d       0.000000   0.000187   0.002017   0.000304   0.000343   0.002815
    H1_ s      -0.000187   0.201194   0.520301   0.011810   0.000392   0.000295
    H1_ p      -0.000556   0.052580   0.042906  -0.000441   0.000446   0.001140

   ao class       7A1        8A1        9A1       10A1       11A1 
    O1_ s       0.000203   0.000221   0.000015   0.000011   0.000005
    O1_ p       0.000263   0.000184  -0.000001   0.000047   0.000005
    O1_ d       0.003587   0.000444   0.000015   0.000117   0.000001
    H1_ s       0.000046   0.000347   0.000128   0.000006   0.000031
    H1_ p       0.000418  -0.000047   0.000450   0.000317   0.000017

                        B1  partial gross atomic populations
   ao class       1B1        2B1        3B1        4B1        5B1        6B1 
    O1_ p       1.126662   0.013340   0.002034   0.000323   0.000003   0.000070
    O1_ d       0.008344   0.000493   0.002781   0.000422   0.000086   0.000000
    H1_ s       0.802601   0.012816   0.000626   0.000147   0.000292   0.000009
    H1_ p       0.027090  -0.000436   0.000421   0.000283   0.000294   0.000425

   ao class       7B1 
    O1_ p       0.000006
    O1_ d       0.000001
    H1_ s       0.000017
    H1_ p       0.000017

                        B2  partial gross atomic populations
   ao class       1B2        2B2        3B2        4B2 
    O1_ p       1.925633   0.015385   0.000106   0.000028
    O1_ d       0.001139   0.000046   0.003779   0.000077
    H1_ p       0.046941   0.000776   0.000528   0.000445

                        A2  partial gross atomic populations
   ao class       1A2        2A2 
    O1_ d       0.003819   0.000191
    H1_ p       0.001311   0.000555


                        gross atomic populations
     ao           O1_        H1_
      s         3.647971   1.550871
      p         4.594273   0.175880
      d         0.031006   0.000000
    total       8.273249   1.726751


 Total number of electrons:   10.00000000

