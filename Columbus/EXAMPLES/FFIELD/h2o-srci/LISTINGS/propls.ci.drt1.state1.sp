

     ******************************************
     **    PROGRAM:              EXPTVL      **
     **    PROGRAM VERSION:      5.5.2b      **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

        Calculation of expectation values of one-electron properties.

 This Version of Program EXPTVL   is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at

 workspace allocation information: lcore=   3000000 mem1= 1074180104 ifirst= -268379950

echo of the input file:
 ------------------------------------------------------------------------
  &input
    quadrup=1
    lvlprt=1
    moment=2
    mofilen='mocoef'
    nofilen='mocoef_prop'
 /&end
  
 ------------------------------------------------------------------------

     Molecular geometry:

 O     8.0    0.00000000    0.00000000    0.00000000
 H     1.0    1.43145000    0.00000000    1.10834700
 H     1.0   -1.43145000    0.00000000    1.10834700

  Charge of molecule  0.00

Header information of AO-integral file:

Hermit Integral Program : SIFS version  hochtor2        Thu Jun  5 14:27:21 2003

irrep                  A1     B1     B2     A2 
basis functions        11      7      4      2
One electron integral record length =   4096 real*8 words
Number of 1-e integrals per record  =   3272

 energy( 1)=  9.187211027960E+00, ietype=   -1,    core energy of type: Nuc.Rep.

nonzero typea=0 expectation values:
type             fcore              <type>               total
--------      ----------          ----------          ----------
S1(*)   :  0.000000000000E+00  9.999999999998E+00  9.999999999998E+00
T1(*)   :  0.000000000000E+00  7.614785419698E+01  7.614785419698E+01
V1(*)   :  0.000000000000E+00 -1.990995774659E+02 -1.990995774659E+02
H1(*)   :  0.000000000000E+00  1.437674809657E-03  1.437674809657E-03

total <h1> = -1.229502855941E+02

Component X   (itypeb= 0) is not totally symmetric: non-vanishing off-diagonal integrals: 5.4346E+01
Component Y   (itypeb= 1) is not totally symmetric: non-vanishing off-diagonal integrals: 2.6034E+01
Component XY  (itypeb= 4) is not totally symmetric: non-vanishing off-diagonal integrals: 2.2307E+01
Component XZ  (itypeb= 5) is not totally symmetric: non-vanishing off-diagonal integrals: 5.4243E+01
Component YZ  (itypeb= 7) is not totally symmetric: non-vanishing off-diagonal integrals: 1.9382E+01

################################################################################

          O U T P U T:

          Diagonal dipole moment integrals in the NO basis.

   Symmetry:  A1 
               X              Y              Z   

    1     0.00000000     0.00000000     0.00073340
    2     0.00000000     0.00000000    -0.13296991
    3     0.00000000     0.00000000     0.37218716
    4     0.00000000     0.00000000     0.87689662
    5     0.00000000     0.00000000    -0.69923139
    6     0.00000000     0.00000000     0.31205312
    7     0.00000000     0.00000000     0.07095214
    8     0.00000000     0.00000000     0.65652027
    9     0.00000000     0.00000000     1.26566578
   10     0.00000000     0.00000000     0.61733337
   11     0.00000000     0.00000000     1.26043278

   Symmetry:  B1 
               X              Y              Z   

    1     0.00000000     0.00000000     0.42662814
    2     0.00000000     0.00000000     0.57878137
    3     0.00000000     0.00000000    -0.08606785
    4     0.00000000     0.00000000     0.38200512
    5     0.00000000     0.00000000     0.90597671
    6     0.00000000     0.00000000     0.84405019
    7     0.00000000     0.00000000     1.35795342

   Symmetry:  B2 
               X              Y              Z   

    1     0.00000000     0.00000000     0.04742046
    2     0.00000000     0.00000000     0.09408060
    3     0.00000000     0.00000000     0.02455796
    4     0.00000000     0.00000000     0.78626572

   Symmetry:  A2 
               X              Y              Z   

    1     0.00000000     0.00000000     0.24663270
    2     0.00000000     0.00000000     0.90248664


          Diagonal second moment integrals in the NO basis.

   Symmetry:  A1 
               XX             XY             XZ             YY             YZ             ZZ  

    1     0.01740199     0.00000000     0.00000000     0.01738219     0.00000000     0.01742096
    2     0.63167313     0.00000000     0.00000000     0.49947228     0.00000000     0.58018919
    3     0.74524413     0.00000000     0.00000000     0.41930304     0.00000000     1.45637939
    4     1.56835304     0.00000000     0.00000000     0.42134941     0.00000000     1.55370890
    5     1.21106265     0.00000000     0.00000000     0.80667904     0.00000000     1.61150363
    6     0.92951696     0.00000000     0.00000000     0.76525534     0.00000000     1.04107072
    7     0.46211820     0.00000000     0.00000000     0.83781680     0.00000000     0.78547553
    8     1.42985526     0.00000000     0.00000000     1.21924626     0.00000000     1.57107954
    9     3.69861077     0.00000000     0.00000000     0.96769295     0.00000000     2.53118193
   10     2.23592003     0.00000000     0.00000000     0.59420573     0.00000000     2.18148274
   11     6.04727353     0.00000000     0.00000000     2.81982923     0.00000000     5.28762478

   Symmetry:  B1 
               XX             XY             XZ             YY             YZ             ZZ  

    1     1.75752378     0.00000000     0.00000000     0.42633739     0.00000000     0.75771255
    2     1.88289415     0.00000000     0.00000000     0.35641967     0.00000000     1.15594179
    3     1.93945398     0.00000000     0.00000000     0.63352672     0.00000000     0.92241260
    4     2.16420334     0.00000000     0.00000000     0.57289555     0.00000000     1.63346965
    5     4.69434292     0.00000000     0.00000000     1.07900551     0.00000000     2.53649516
    6     3.10056090     0.00000000     0.00000000     0.56174317     0.00000000     1.95177919
    7     8.36225260     0.00000000     0.00000000     2.23313290     0.00000000     4.47366399

   Symmetry:  B2 
               XX             XY             XZ             YY             YZ             ZZ  

    1     0.45537072     0.00000000     0.00000000     1.25364089     0.00000000     0.44088270
    2     0.86320112     0.00000000     0.00000000     2.14602750     0.00000000     0.80703885
    3     0.37078443     0.00000000     0.00000000     0.76561830     0.00000000     0.73573218
    4     2.20159082     0.00000000     0.00000000     1.48181452     0.00000000     1.54749352

   Symmetry:  A2 
               XX             XY             XZ             YY             YZ             ZZ  

    1     0.99151675     0.00000000     0.00000000     0.72163698     0.00000000     0.45067887
    2     2.29363389     0.00000000     0.00000000     0.95757904     0.00000000     1.47159689


  The following moments are calculated in a.u. relative to the point:      0.00000000      0.00000000      0.00000000

           Dipole moments:

                     X               Y               Z   
   nuclear       0.00000000      0.00000000      2.21669400
   electronic    0.00000000      0.00000000     -1.43767481
   total         0.00000000      0.00000000      0.77901919
   total Dipole moment =   1.98007125    Debye

           Second moments:

                     XX              XY              XZ              YY  
   nuclear       4.09809820      0.00000000      0.00000000      0.00000000
   electronic   -7.25744270      0.00000000      0.00000000     -5.24934032
   total        -3.15934449      0.00000000      0.00000000     -5.24934032

                     YZ              ZZ  
   nuclear       0.00000000      2.45686614
   electronic    0.00000000     -6.53748582
   total         0.00000000     -4.08061968

           Quadrupole moment:  

                    QXX             QXY             QXZ             QYY  
   nuclear       2.86966513      0.00000000      0.00000000     -3.27748217
   electronic   -1.36402963      0.00000000      0.00000000      1.64812394
   total         1.50563550      0.00000000      0.00000000     -1.62935823
  
                    QYZ             QZZ  
   nuclear       0.00000000      0.40781704
   electronic    0.00000000     -0.28409431
   total         0.00000000      0.12372273

          Diagonal dipole moment integrals in the MO basis.

   Symmetry:  A1 
               X              Y              Z   

    1     0.00000000     0.00000000     0.00024412
    2     0.00000000     0.00000000     0.32189765
    3     0.00000000     0.00000000    -0.08838425
    4     0.00000000     0.00000000     1.37399310
    5     0.00000000     0.00000000     1.11177321
    6     0.00000000     0.00000000    -0.04402160
    7     0.00000000     0.00000000     0.33658164
    8     0.00000000     0.00000000     0.34294604
    9     0.00000000     0.00000000     0.91233752
   10     0.00000000     0.00000000     0.12924748
   11     0.00000000     0.00000000     0.20395842

   Symmetry:  B1 
               X              Y              Z   

    1     0.00000000     0.00000000     0.42231292
    2     0.00000000     0.00000000     1.39008233
    3     0.00000000     0.00000000     0.68808914
    4     0.00000000     0.00000000    -0.01144820
    5     0.00000000     0.00000000     0.97935659
    6     0.00000000     0.00000000     0.88018506
    7     0.00000000     0.00000000     0.06074925

   Symmetry:  B2 
               X              Y              Z   

    1     0.00000000     0.00000000     0.04503437
    2     0.00000000     0.00000000     0.00805434
    3     0.00000000     0.00000000     0.76926711
    4     0.00000000     0.00000000     0.12996891

   Symmetry:  A2 
               X              Y              Z   

    1     0.00000000     0.00000000     1.02692667
    2     0.00000000     0.00000000     0.12219266


          Diagonal second moment integrals in the MO basis.

   Symmetry:  A1 
               XX             XY             XZ             YY             YZ             ZZ  

    1     0.01773368     0.00000000     0.00000000     0.01773223     0.00000000     0.01773368
    2     0.74118718     0.00000000     0.00000000     0.48115491     0.00000000     0.67683847
    3     0.61960993     0.00000000     0.00000000     0.43803957     0.00000000     1.35215049
    4     5.16423819     0.00000000     0.00000000     2.65436573     0.00000000     4.73074001
    5     3.37563536     0.00000000     0.00000000     1.14176961     0.00000000     2.35217612
    6     1.11788232     0.00000000     0.00000000     0.83620904     0.00000000     2.45128647
    7     1.94505256     0.00000000     0.00000000     0.65990313     0.00000000     2.37020495
    8     1.46661975     0.00000000     0.00000000     1.69884240     0.00000000     1.23548216
    9     2.36366592     0.00000000     0.00000000     0.41074991     0.00000000     1.55330974
   10     0.97016134     0.00000000     0.00000000     0.33114298     0.00000000     1.04472274
   11     1.19524345     0.00000000     0.00000000     0.69832276     0.00000000     0.83247247

   Symmetry:  B1 
               XX             XY             XZ             YY             YZ             ZZ  

    1     1.74868475     0.00000000     0.00000000     0.42800383     0.00000000     0.75008125
    2     8.39281824     0.00000000     0.00000000     2.28507479     0.00000000     4.13125414
    3     3.46473020     0.00000000     0.00000000     0.89067483     0.00000000     2.04723913
    4     3.49865445     0.00000000     0.00000000     1.03479298     0.00000000     1.64053356
    5     2.79015433     0.00000000     0.00000000     0.47558085     0.00000000     2.21402183
    6     2.63180691     0.00000000     0.00000000     0.40083336     0.00000000     1.63466285
    7     1.37438279     0.00000000     0.00000000     0.34810029     0.00000000     1.01368217

   Symmetry:  B2 
               XX             XY             XZ             YY             YZ             ZZ  

    1     0.45153710     0.00000000     0.00000000     1.25045675     0.00000000     0.43811870
    2     0.78601677     0.00000000     0.00000000     2.32002631     0.00000000     0.78094268
    3     2.05752875     0.00000000     0.00000000     1.32570774     0.00000000     1.54512873
    4     0.59586446     0.00000000     0.00000000     0.75091041     0.00000000     0.76695714

   Symmetry:  A2 
               XX             XY             XZ             YY             YZ             ZZ  

    1     2.34145660     0.00000000     0.00000000     1.00234608     0.00000000     1.44607012
    2     0.94369404     0.00000000     0.00000000     0.67686994     0.00000000     0.47620564


 !timer: exptvl required                 user+sys=     0.020 walltime=     0.000
