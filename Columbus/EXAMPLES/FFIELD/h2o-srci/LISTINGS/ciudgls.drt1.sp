1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      09/20/00    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

================================================================================
four external integ    0.25 MB location: local disk    
three external inte    0.25 MB location: local disk    
four external integ    0.25 MB location: local disk    
three external inte    0.25 MB location: local disk    
diagonal integrals     0.12 MB location: local disk    
off-diagonal integr    0.12 MB location: local disk    
 nsubmx= 16 lenci= 1311
global arrays:        43263   (    0.33 MB)
vdisk:                    0   (    0.00 MB)
drt:                 252658   (    0.96 MB)
================================================================================
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core            2999999 DP per process
 CIUDG version 5.9.3 (05-Dec-2002)
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
   NTYPE = 0,
   RTOLCI=0.00010000,
   VOUT  = 1,
   NBKITR= 1,
   NITER= 20,
  IDEN=   1,
   IVMODE=   3,
 /&end
 ------------------------------------------------------------------------

 ** list of control variables **
 nrfitr =   30      nvrfmx =   16      nvrfmn =    1
 lvlprt =    0      nroot  =    1      noldv  =    0      noldhv =    0
 nunitv =    1      ntype  =    0      nbkitr =    1      niter  =   20
 ivmode =    3      vout   =    1      istrt  =    0      iortls =    0
 nvbkmx =   16      ibktv  =   -1      ibkthv =   -1      frcsub =    0
 nvcimx =   16      icitv  =   -1      icithv =   -1      maxseg =    4
 iden   =    1      itran  =    0      froot  =    0      rtmode =    0
 ftcalc =    1      lrtshift=1.0000    ncouple=    1      skipso  =   F
 ncorel =    0      csfprn  =   1      ctol   = 1.00E-02  davcor  =   1


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-04    1.000E-04

 units and filenames:
    1: ( 6)    ciudgls                                                     
    2: ( 5)    ciudgin                                                     
    3: ( 7)    ciudgsm                                                     
    4: ( 4)    cihdiag                                                     
    5: ( 3)    cihpseu                                                     
    6: (10)    cihvfl                                                      
    7: (11)    civfl                                                       
    8: (12)    diagint                                                     
    9: (13)    ofdgint                                                     
   10: (17)    cidrtfl                                                     
   11: (20)    ciftdfl                                                     
   12: (21)    ciftofl                                                     
   13: (24)    ciftifl                                                     
   14: ( 8)    ciflind                                                     
   15: (15)    civin                                                       
   16: (16)    civout                                                      
   18: (18)    d1fl                                                        
   20: (53)    cifvfl                                                      
   21: (25)    ciscr4                                                      
   22: (41)    ciftotd                                                     
   23: (42)    flacpfd                                                     
   26: (26)    ciscr5                                                      
   27: (47)    restart                                                     
   28: (48)    civout                                                      
   29: (49)    cirefv                                                      
   31: (31)    fil4w                                                       
   32: (32)    fil4x                                                       
   33: (33)    fil3w                                                       
   34: (34)    fil3x                                                       
   36: (36)    aoints                                                      
   37: (37)    aoints2                                                     
   38: (38)    drtfil                                                      
   39: (50)    cisrtif                                                     
   42: (50)    mocoef                                                      
   45: (52)    nocoef_ci                                                   
   46: (53)    civfl_restart                                               
 ------------------------------------------------------------------------

 workspace allocation information: lcore=   2999999 mem1=1074180104 ifirst=-268379578

 integral file titles:
 Hermit Integral Program : SIFS version  hochtor2        Thu Jun  5 14:27:21 2003
  h2o pvdz                                                                       
 mo coefficients generated by scfpq                                              
 SIFS file created by program tran.      hochtor2        Thu Jun  5 14:27:21 2003

 core energy values from the integral file:
 energy( 1)=  9.187211027960E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -8.521256637634E+01, ietype=    5,   fcore energy of type: Vref(*) 

 total core repulsion energy = -7.602535534838E+01

 drt header information:
  h2o pvdz                                                                       
 spnorb, spnodd, lxyzir,hmult F F 0 0 0 0
 nmot  =    24 niot  =     5 nfct  =     0 nfvt  =     0
 nrow  =    20 nsym  =     4 ssym  =     1 lenbuf=  1600
 nwalk,xbar:         31        1        5       10       15
 nvalwt,nvalw:       31        1        5       10       15
 ncsft:            1311
 total number of valid internal walks:      31
 nvalz,nvaly,nvalx,nvalw =        1       5      10      15

 cisrt info file parameters:
 file number  12 blocksize   4095
 mxbld   4095
 nd4ext,nd2ext,nd0ext   380   190    30
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int     5955     5445     2496      618       66        0        0        0
 minbl4,minbl3,maxbl2   195   186   198
 maxbuf 32767
 number of external orbitals per symmetry block:   8   6   3   2
 nmsym   4 number of internal orbitals   5

 formula file title:
 Hermit Integral Program : SIFS version  hochtor2        Thu Jun  5 14:27:21 2003
  h2o pvdz                                                                       
 mo coefficients generated by scfpq                                              
 SIFS file created by program tran.      hochtor2        Thu Jun  5 14:27:21 2003
  h2o pvdz                                                                       
 file nmb.  24 block size     0
 pthz,pthy,pthx,pthw:     1     5    10    15 total internal walks:      31
 maxlp3,n3xtlp,n2xtlp,n1xtlp,n0xtlp,n2lp,n1lp,n0lp     0     0     0     0
 orbsym(*)= 1 1 2 1 3

 setref:        1 references kept,
                0 references were marked as invalid, out of
                1 total.
 limcnvrt: found 1 valid internal walksout of  1
  walks (skipping trailing invalids)
  ... adding  1 segmentation marks segtype= 1
 limcnvrt: found 5 valid internal walksout of  5
  walks (skipping trailing invalids)
  ... adding  5 segmentation marks segtype= 2
 limcnvrt: found 10 valid internal walksout of  10
  walks (skipping trailing invalids)
  ... adding  10 segmentation marks segtype= 3
 limcnvrt: found 15 valid internal walksout of  15
  walks (skipping trailing invalids)
  ... adding  15 segmentation marks segtype= 4

 number of external paths / symmetry
 vertex x      47      54      36      34
 vertex w      66      54      36      34



                   segmentation summary for type diagonal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1           1|         1|         0|         1|         0|         1|
 -------------------------------------------------------------------------------
  Y 2           5|        33|         1|         5|         1|         2|
 -------------------------------------------------------------------------------
  X 3          10|       445|        34|        10|         6|         3|
 -------------------------------------------------------------------------------
  W 4          15|       832|       479|        15|        16|         4|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>      1311


 297 dimension of the ci-matrix ->>>      1311


 297 dimension of the ci-matrix ->>>         7

                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      10       1        445          1      10       1
     2  4   1    25      two-ext wz   2X  4 1      15       1        832          1      15       1
     3  4   3    26      two-ext wx   2X  4 3      15      10        832        445      15      10
     4  2   1    11      one-ext yz   1X  2 1       5       1         33          1       5       1
     5  3   2    15      1ex3ex  yx   3X  3 2      10       5        445         33      10       5
     6  4   2    16      1ex3ex  yw   3X  4 2      15       5        832         33      15       5
     7  1   1     1      allint zz    OX  1 1       1       1          1          1       1       1
     8  2   2     5      0ex2ex yy    OX  2 2       5       5         33         33       5       5
     9  3   3     6      0ex2ex xx    OX  3 3      10      10        445        445      10      10
    10  4   4     7      0ex2ex ww    OX  4 4      15      15        832        832      15      15
    11  1   1    75      dg-024ext z  DG  1 1       1       1          1          1       1       1
    12  2   2    45      4exdg024 y   DG  2 2       5       5         33         33       5       5
    13  3   3    46      4exdg024 x   DG  3 3      10      10        445        445      10      10
    14  4   4    47      4exdg024 w   DG  4 4      15      15        832        832      15      15
----------------------------------------------------------------------------------------------------
 DIAGEL COUNTS: DG0X,DG2X,DG4X= 70 50 30
 diagonal elements written to file   4

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:         0 2x:         0 4x:         0
All internal counts: zz :         0 yy:         0 xx:         0 ww:         0
One-external counts: yz :         0 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         0    task #   2:         0    task #   3:         0    task #   4:         0
task #   5:         0    task #   6:         0    task #   7:        -1    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:        -1    task #  12:         0
task #  13:         0    task #  14:         0    task #
 reference space has dimension       1

    root           eigenvalues
    ----           ------------
       1         -76.0253553484

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------


         vector  1 from unit 11 written to unit 49 filename cirefv                                                      

         vector  1 from unit 11 written to unit 49 filename cirefv                                                      

 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=     1)...
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              1311
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:              16
 number of roots to converge:                             1
 number of iterations:                                    1
 residual norm convergence criteria:               0.000100

          starting bk iteration   1

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        70 2x:        50 4x:        30
All internal counts: zz :         0 yy:         0 xx:         0 ww:         0
One-external counts: yz :         5 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:        10 wz:        15 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:         0    task #   4:         4
task #   5:         0    task #   6:         0    task #   7:        -1    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:        -1    task #  12:         9
task #  13:        23    task #  14:        33    task #

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -76.0253553484  0.0000E+00  2.5320E-01  9.9888E-01  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.00   0.00   0.00   0.00
    4   11    0   0.00   0.00   0.00   0.00
    5   15    0   0.00   0.00   0.00   0.00
    6   16    0   0.00   0.00   0.00   0.00
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.00   0.00   0.00   0.00
    9    6    0   0.00   0.00   0.00   0.00
   10    7    0   0.00   0.00   0.00   0.00
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.01   0.00   0.00   0.01
   14   47    0   0.00   0.00   0.00   0.00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0400s 
time spent in multnx:                   0.0400s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.0400s 

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -76.0253553484  0.0000E+00  2.5320E-01  9.9888E-01  1.0000E-04

 expansion vectors are not transformed.
 matrix-vector products are not transformed.

    2 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)

 from bk iterations: iconv=   1

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              1311
 number of initial trial vectors:                         2
 number of initial matrix-vector products:                1
 maximum dimension of the subspace vectors:              16
 number of roots to converge:                             1
 number of iterations:                                   20
 residual norm convergence criteria:               0.000100

          starting ci iteration   1

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        70 2x:        50 4x:        30
All internal counts: zz :         0 yy:         3 xx:        18 ww:        43
One-external counts: yz :         5 yx:        70 yw:        95
Two-external counts: yy :        15 ww:        75 xx:        50 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:         4
task #   5:        63    task #   6:        88    task #   7:        -1    task #   8:         2
task #   9:        15    task #  10:        40    task #  11:        -1    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97618283    -0.21694949

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -76.2243425458  1.9899E-01  6.3980E-03  1.5657E-01  1.0000E-04
 mr-sdci #  1  2    -71.9966063991 -4.0287E+00  0.0000E+00  1.7209E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.01   0.00   0.00   0.01
    9    6    0   0.02   0.00   0.00   0.02
   10    7    0   0.02   0.01   0.00   0.02
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.00   0.00   0.00   0.00
   14   47    0   0.01   0.00   0.00   0.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1300s 
time spent in multnx:                   0.1300s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

          starting ci iteration   2

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        70 2x:        50 4x:        30
All internal counts: zz :         0 yy:         3 xx:        18 ww:        43
One-external counts: yz :         5 yx:        70 yw:        95
Two-external counts: yy :        15 ww:        75 xx:        50 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:         4
task #   5:        63    task #   6:        88    task #   7:        -1    task #   8:         2
task #   9:        15    task #  10:        40    task #  11:        -1    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.97560185     0.00988953    -0.21932449

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1    -76.2302857565  5.9432E-03  2.3177E-04  2.8767E-02  1.0000E-04
 mr-sdci #  2  2    -72.9340821885  9.3748E-01  0.0000E+00  1.7054E+00  1.0000E-04
 mr-sdci #  2  3    -71.9767665346 -4.0486E+00  0.0000E+00  1.8935E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.00   0.00   0.00   0.00
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.02   0.01   0.00   0.02
    9    6    0   0.01   0.00   0.00   0.01
   10    7    0   0.02   0.00   0.00   0.02
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.01   0.00   0.00   0.01
   14   47    0   0.00   0.00   0.00   0.00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

          starting ci iteration   3

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        70 2x:        50 4x:        30
All internal counts: zz :         0 yy:         3 xx:        18 ww:        43
One-external counts: yz :         5 yx:        70 yw:        95
Two-external counts: yy :        15 ww:        75 xx:        50 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:         4
task #   5:        63    task #   6:        88    task #   7:        -1    task #   8:         2
task #   9:        15    task #  10:        40    task #  11:        -1    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.97511580     0.09644373     0.05012155     0.19322426

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1    -76.2305262980  2.4054E-04  1.4783E-05  6.5008E-03  1.0000E-04
 mr-sdci #  3  2    -73.9203693533  9.8629E-01  0.0000E+00  1.2839E+00  1.0000E-04
 mr-sdci #  3  3    -72.8250953290  8.4833E-01  0.0000E+00  1.4867E+00  1.0000E-04
 mr-sdci #  3  4    -71.5398775452 -4.4855E+00  0.0000E+00  2.1625E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.01   0.00   0.01
    2   25    0   0.00   0.00   0.00   0.00
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.02   0.00   0.00   0.02
    7    1    0   0.00   0.00   0.00   0.00
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.01   0.00   0.00   0.01
   10    7    0   0.02   0.00   0.00   0.02
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.01   0.00   0.00   0.01
   14   47    0   0.01   0.00   0.00   0.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1300s 
time spent in multnx:                   0.1300s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

          starting ci iteration   4

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        70 2x:        50 4x:        30
All internal counts: zz :         0 yy:         3 xx:        18 ww:        43
One-external counts: yz :         5 yx:        70 yw:        95
Two-external counts: yy :        15 ww:        75 xx:        50 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:         4
task #   5:        63    task #   6:        88    task #   7:        -1    task #   8:         2
task #   9:        15    task #  10:        40    task #  11:        -1    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     0.97504340     0.05630379     0.08263066    -0.09104432     0.17607772

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1    -76.2305392723  1.2974E-05  7.9710E-07  1.6330E-03  1.0000E-04
 mr-sdci #  4  2    -74.5147010033  5.9433E-01  0.0000E+00  1.0568E+00  1.0000E-04
 mr-sdci #  4  3    -73.8216462068  9.9655E-01  0.0000E+00  1.4880E+00  1.0000E-04
 mr-sdci #  4  4    -72.4626401322  9.2276E-01  0.0000E+00  1.2282E+00  1.0000E-04
 mr-sdci #  4  5    -71.3257604008 -4.6996E+00  0.0000E+00  2.3086E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.00   0.00   0.00   0.00
    5   15    0   0.02   0.00   0.00   0.01
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.01   0.00   0.00   0.01
    9    6    0   0.02   0.00   0.00   0.02
   10    7    0   0.01   0.00   0.00   0.01
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.01   0.00   0.00   0.01
   13   46    0   0.00   0.00   0.00   0.00
   14   47    0   0.01   0.00   0.00   0.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1300s 
time spent in multnx:                   0.1200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

          starting ci iteration   5

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        70 2x:        50 4x:        30
All internal counts: zz :         0 yy:         3 xx:        18 ww:        43
One-external counts: yz :         5 yx:        70 yw:        95
Two-external counts: yy :        15 ww:        75 xx:        50 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:         4
task #   5:        63    task #   6:        88    task #   7:        -1    task #   8:         2
task #   9:        15    task #  10:        40    task #  11:        -1    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.97503698     0.06646342     0.02774296     0.09707927     0.09141210     0.16228147

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1    -76.2305400583  7.8593E-07  2.9843E-08  3.0692E-04  1.0000E-04
 mr-sdci #  5  2    -74.5641792407  4.9478E-02  0.0000E+00  8.5842E-01  1.0000E-04
 mr-sdci #  5  3    -74.2901018536  4.6846E-01  0.0000E+00  1.2541E+00  1.0000E-04
 mr-sdci #  5  4    -72.8494497631  3.8681E-01  0.0000E+00  1.8533E+00  1.0000E-04
 mr-sdci #  5  5    -72.4626351413  1.1369E+00  0.0000E+00  1.2249E+00  1.0000E-04
 mr-sdci #  5  6    -71.1810279822 -4.8443E+00  0.0000E+00  2.1661E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.00
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.01   0.00   0.00   0.01
    9    6    0   0.02   0.00   0.00   0.02
   10    7    0   0.02   0.00   0.00   0.02
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.01   0.00   0.00   0.01
   14   47    0   0.00   0.00   0.00   0.00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1300s 
time spent in multnx:                   0.1200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1400s 

          starting ci iteration   6

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        70 2x:        50 4x:        30
All internal counts: zz :         0 yy:         3 xx:        18 ww:        43
One-external counts: yz :         5 yx:        70 yw:        95
Two-external counts: yy :        15 ww:        75 xx:        50 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        59 yw:        76

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:         4
task #   5:        63    task #   6:        88    task #   7:        -1    task #   8:         2
task #   9:        15    task #  10:        40    task #  11:        -1    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.97503796     0.04157704     0.04657248     0.05776328    -0.10852167    -0.06800401     0.16020373

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  6  1    -76.2305400879  2.9615E-08  1.9074E-09  7.7364E-05  1.0000E-04
 mr-sdci #  6  2    -74.7229784775  1.5880E-01  0.0000E+00  8.6641E-01  1.0000E-04
 mr-sdci #  6  3    -74.3062426455  1.6141E-02  0.0000E+00  1.1751E+00  1.0000E-04
 mr-sdci #  6  4    -74.0048219632  1.1554E+00  0.0000E+00  1.3267E+00  1.0000E-04
 mr-sdci #  6  5    -72.6797791595  2.1714E-01  0.0000E+00  1.5962E+00  1.0000E-04
 mr-sdci #  6  6    -72.3434585935  1.1624E+00  0.0000E+00  1.6593E+00  1.0000E-04
 mr-sdci #  6  7    -71.1191338015 -4.9062E+00  0.0000E+00  2.2759E+00  1.0000E-04


 mr-sdci  convergence criteria satisfied after  6 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  6  1    -76.2305400879  2.9615E-08  1.9074E-09  7.7364E-05  1.0000E-04
 mr-sdci #  6  2    -74.7229784775  1.5880E-01  0.0000E+00  8.6641E-01  1.0000E-04
 mr-sdci #  6  3    -74.3062426455  1.6141E-02  0.0000E+00  1.1751E+00  1.0000E-04
 mr-sdci #  6  4    -74.0048219632  1.1554E+00  0.0000E+00  1.3267E+00  1.0000E-04
 mr-sdci #  6  5    -72.6797791595  2.1714E-01  0.0000E+00  1.5962E+00  1.0000E-04
 mr-sdci #  6  6    -72.3434585935  1.1624E+00  0.0000E+00  1.6593E+00  1.0000E-04
 mr-sdci #  6  7    -71.1191338015 -4.9062E+00  0.0000E+00  2.2759E+00  1.0000E-04

####################CIUDGINFO####################

   ci vector at position   1 energy=  -76.230540087876

################END OF CIUDGINFO################


    1 of the   8 expansion vectors are transformed.
    1 of the   7 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
 maximum overlap with reference  1(overlap=  0.97503796)

information on vector: 1from unit 11 written to unit 16filename civout                                                      


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -76.2305400879

                                                       internal orbitals

                                          level       1    2    3    4    5

                                          orbital     1    2   12    3   19

                                         symmetry   a1   a1   b1   a1   b2 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1  0.975038                        +-   +-   +-   +-   +- 
 x   1  1      35 -0.014154    1( a1 )   1( b2 )   +-   +-   +-    -    - 
 x   1  1      36  0.017987    2( a1 )   1( b2 )   +-   +-   +-    -    - 
 x   1  1      37 -0.032065    3( a1 )   1( b2 )   +-   +-   +-    -    - 
 x   1  1      46 -0.011239    4( a1 )   2( b2 )   +-   +-   +-    -    - 
 x   1  1      47 -0.011648    5( a1 )   2( b2 )   +-   +-   +-    -    - 
 x   1  1      54 -0.010436    4( a1 )   3( b2 )   +-   +-   +-    -    - 
 x   1  1      55 -0.010508    5( a1 )   3( b2 )   +-   +-   +-    -    - 
 x   1  1      57 -0.013894    7( a1 )   3( b2 )   +-   +-   +-    -    - 
 x   1  1      60 -0.014684    2( b1 )   1( a2 )   +-   +-   +-    -    - 
 x   1  1      71  0.026070    1( b1 )   1( b2 )   +-   +-    -   +-    - 
 x   1  1      72  0.014795    2( b1 )   1( b2 )   +-   +-    -   +-    - 
 x   1  1      73 -0.026821    3( b1 )   1( b2 )   +-   +-    -   +-    - 
 x   1  1      78  0.013816    2( b1 )   2( b2 )   +-   +-    -   +-    - 
 x   1  1      90 -0.020186    2( a1 )   1( a2 )   +-   +-    -   +-    - 
 x   1  1      93  0.011353    5( a1 )   1( a2 )   +-   +-    -   +-    - 
 x   1  1      94  0.010942    6( a1 )   1( a2 )   +-   +-    -   +-    - 
 x   1  1      98 -0.010187    2( a1 )   2( a2 )   +-   +-    -   +-    - 
 x   1  1     102  0.011205    6( a1 )   2( a2 )   +-   +-    -   +-    - 
 x   1  1     104  0.012127    8( a1 )   2( a2 )   +-   +-    -   +-    - 
 x   1  1     107  0.024560    3( a1 )   1( b1 )   +-   +-    -    -   +- 
 x   1  1     115  0.022496    3( a1 )   2( b1 )   +-   +-    -    -   +- 
 x   1  1     116  0.020032    4( a1 )   2( b1 )   +-   +-    -    -   +- 
 x   1  1     119  0.011501    7( a1 )   2( b1 )   +-   +-    -    -   +- 
 x   1  1     122  0.012976    2( a1 )   3( b1 )   +-   +-    -    -   +- 
 x   1  1     123 -0.018318    3( a1 )   3( b1 )   +-   +-    -    -   +- 
 x   1  1     139 -0.012236    3( a1 )   5( b1 )   +-   +-    -    -   +- 
 x   1  1     159 -0.015030    1( a1 )   1( b2 )   +-    -   +-   +-    - 
 x   1  1     163  0.015106    5( a1 )   1( b2 )   +-    -   +-   +-    - 
 x   1  1     196  0.010836    1( a1 )   3( a1 )   +-    -   +-    -   +- 
 x   1  1     242 -0.010763    1( a1 )   1( b1 )   +-    -    -   +-   +- 
 x   1  1     258  0.010311    1( a1 )   3( b1 )   +-    -    -   +-   +- 
 w   1  1     480 -0.010469    1( a1 )   1( a1 )   +-   +-   +-   +-      
 w   1  1     481  0.010022    1( a1 )   2( a1 )   +-   +-   +-   +-      
 w   1  1     494 -0.012888    5( a1 )   5( a1 )   +-   +-   +-   +-      
 w   1  1     496  0.010638    2( a1 )   6( a1 )   +-   +-   +-   +-      
 w   1  1     513 -0.010041    6( a1 )   8( a1 )   +-   +-   +-   +-      
 w   1  1     537 -0.047518    1( b2 )   1( b2 )   +-   +-   +-   +-      
 w   1  1     542 -0.012505    3( b2 )   3( b2 )   +-   +-   +-   +-      
 w   1  1     544 -0.010853    1( a2 )   2( a2 )   +-   +-   +-   +-      
 w   1  1     545 -0.012197    2( a2 )   2( a2 )   +-   +-   +-   +-      
 w   1  1     547 -0.013319    2( a1 )   1( b2 )   +-   +-   +-   +     - 
 w   1  1     548  0.034537    3( a1 )   1( b2 )   +-   +-   +-   +     - 
 w   1  1     571  0.012075    2( b1 )   1( a2 )   +-   +-   +-   +     - 
 w   1  1     577  0.010102    2( b1 )   2( a2 )   +-   +-   +-   +     - 
 w   1  1     582 -0.014259    1( a1 )   1( a1 )   +-   +-   +-        +- 
 w   1  1     583  0.015294    1( a1 )   2( a1 )   +-   +-   +-        +- 
 w   1  1     584 -0.014542    2( a1 )   2( a1 )   +-   +-   +-        +- 
 w   1  1     585 -0.011951    1( a1 )   3( a1 )   +-   +-   +-        +- 
 w   1  1     586  0.016917    2( a1 )   3( a1 )   +-   +-   +-        +- 
 w   1  1     587 -0.032906    3( a1 )   3( a1 )   +-   +-   +-        +- 
 w   1  1     591 -0.014524    4( a1 )   4( a1 )   +-   +-   +-        +- 
 w   1  1     594 -0.016021    3( a1 )   5( a1 )   +-   +-   +-        +- 
 w   1  1     609 -0.011379    7( a1 )   7( a1 )   +-   +-   +-        +- 
 w   1  1     619 -0.016927    1( b1 )   2( b1 )   +-   +-   +-        +- 
 w   1  1     620 -0.023737    2( b1 )   2( b1 )   +-   +-   +-        +- 
 w   1  1     629  0.012677    2( b1 )   5( b1 )   +-   +-   +-        +- 
 w   1  1     644 -0.010399    3( b2 )   3( b2 )   +-   +-   +-        +- 
 w   1  1     648 -0.024552    1( b1 )   1( b2 )   +-   +-   +    +-    - 
 w   1  1     649 -0.014901    2( b1 )   1( b2 )   +-   +-   +    +-    - 
 w   1  1     650  0.026994    3( b1 )   1( b2 )   +-   +-   +    +-    - 
 w   1  1     652  0.011220    5( b1 )   1( b2 )   +-   +-   +    +-    - 
 w   1  1     655 -0.010762    2( b1 )   2( b2 )   +-   +-   +    +-    - 
 w   1  1     682  0.019091    1( a1 )   1( b1 )   +-   +-   +     -   +- 
 w   1  1     683 -0.021741    2( a1 )   1( b1 )   +-   +-   +     -   +- 
 w   1  1     684  0.018681    3( a1 )   1( b1 )   +-   +-   +     -   +- 
 w   1  1     690  0.017888    1( a1 )   2( b1 )   +-   +-   +     -   +- 
 w   1  1     691 -0.034900    2( a1 )   2( b1 )   +-   +-   +     -   +- 
 w   1  1     692  0.010227    3( a1 )   2( b1 )   +-   +-   +     -   +- 
 w   1  1     695  0.012290    6( a1 )   2( b1 )   +-   +-   +     -   +- 
 w   1  1     699  0.011360    2( a1 )   3( b1 )   +-   +-   +     -   +- 
 w   1  1     700 -0.019709    3( a1 )   3( b1 )   +-   +-   +     -   +- 
 w   1  1     731  0.013274    2( b2 )   1( a2 )   +-   +-   +     -   +- 
 w   1  1     736 -0.013147    1( a1 )   1( a1 )   +-   +-        +-   +- 
 w   1  1     737  0.018332    1( a1 )   2( a1 )   +-   +-        +-   +- 
 w   1  1     738 -0.026561    2( a1 )   2( a1 )   +-   +-        +-   +- 
 w   1  1     772 -0.026264    1( b1 )   1( b1 )   +-   +-        +-   +- 
 w   1  1     773 -0.026934    1( b1 )   2( b1 )   +-   +-        +-   +- 
 w   1  1     774 -0.033505    2( b1 )   2( b1 )   +-   +-        +-   +- 
 w   1  1     775  0.024809    1( b1 )   3( b1 )   +-   +-        +-   +- 
 w   1  1     776  0.011855    2( b1 )   3( b1 )   +-   +-        +-   +- 
 w   1  1     777 -0.020772    3( b1 )   3( b1 )   +-   +-        +-   +- 
 w   1  1     783  0.014470    2( b1 )   5( b1 )   +-   +-        +-   +- 
 w   1  1     786 -0.010122    5( b1 )   5( b1 )   +-   +-        +-   +- 
 w   1  1     799 -0.014378    1( a2 )   1( a2 )   +-   +-        +-   +- 
 w   1  1     802  0.024972    1( a1 )   1( b2 )   +-   +    +-   +-    - 
 w   1  1     803 -0.017526    2( a1 )   1( b2 )   +-   +    +-   +-    - 
 w   1  1     805  0.013733    4( a1 )   1( b2 )   +-   +    +-   +-    - 
 w   1  1     806 -0.020704    5( a1 )   1( b2 )   +-   +    +-   +-    - 
 w   1  1     807  0.016472    6( a1 )   1( b2 )   +-   +    +-   +-    - 
 w   1  1     819  0.010728    2( a1 )   3( b2 )   +-   +    +-   +-    - 
 w   1  1     832 -0.011062    1( b1 )   2( a2 )   +-   +    +-   +-    - 
 w   1  1     838 -0.014952    1( a1 )   1( a1 )   +-   +    +-    -   +- 
 w   1  1     839  0.020336    1( a1 )   2( a1 )   +-   +    +-    -   +- 
 w   1  1     840 -0.018116    2( a1 )   2( a1 )   +-   +    +-    -   +- 
 w   1  1     841 -0.015461    1( a1 )   3( a1 )   +-   +    +-    -   +- 
 w   1  1     846 -0.012151    3( a1 )   4( a1 )   +-   +    +-    -   +- 
 w   1  1     850  0.019322    3( a1 )   5( a1 )   +-   +    +-    -   +- 
 w   1  1     904  0.019693    1( a1 )   1( b1 )   +-   +     -   +-   +- 
 w   1  1     905 -0.011315    2( a1 )   1( b1 )   +-   +     -   +-   +- 
 w   1  1     907  0.012331    4( a1 )   1( b1 )   +-   +     -   +-   +- 
 w   1  1     908 -0.011642    5( a1 )   1( b1 )   +-   +     -   +-   +- 
 w   1  1     912  0.011835    1( a1 )   2( b1 )   +-   +     -   +-   +- 
 w   1  1     913 -0.020102    2( a1 )   2( b1 )   +-   +     -   +-   +- 
 w   1  1     915  0.013632    4( a1 )   2( b1 )   +-   +     -   +-   +- 
 w   1  1     919 -0.010137    8( a1 )   2( b1 )   +-   +     -   +-   +- 
 w   1  1     920 -0.020842    1( a1 )   3( b1 )   +-   +     -   +-   +- 
 w   1  1     921  0.014075    2( a1 )   3( b1 )   +-   +     -   +-   +- 
 w   1  1     924  0.017197    5( a1 )   3( b1 )   +-   +     -   +-   +- 
 w   1  1     953  0.011926    2( b2 )   1( a2 )   +-   +     -   +-   +- 
 w   1  1     958 -0.011760    1( a1 )   1( a1 )   +-        +-   +-   +- 
 w   1  1     959  0.014190    1( a1 )   2( a1 )   +-        +-   +-   +- 
 w   1  1     960 -0.013072    2( a1 )   2( a1 )   +-        +-   +-   +- 

 ci coefficient statistics:
           rq > 0.1                1
      0.1> rq > 0.01             112
     0.01> rq > 0.001            586
    0.001> rq > 0.0001           425
   0.0001> rq > 0.00001          142
  0.00001> rq > 0.000001          41
 0.000001> rq                      4
           all                  1311
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:         0 2x:         0 4x:         0
All internal counts: zz :         0 yy:         0 xx:         0 ww:         0
One-external counts: yz :         0 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         0    task #   2:         0    task #   3:         0    task #   4:         0
task #   5:         0    task #   6:         0    task #   7:        -1    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:        -1    task #  12:         0
task #  13:         0    task #  14:         0    task #
  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.975037959958    -74.127607383949

 number of reference csfs (nref) is     1.  root number (iroot) is  1.

 pople ci energy extrapolation is computed with 10 correlated electrons.

 eref      =    -76.025355348382   "relaxed" cnot**2         =   0.950699023359
 eci       =    -76.230540087876   deltae = eci - eref       =  -0.205184739493
 eci+dv1   =    -76.240655895925   dv1 = (1-cnot**2)*deltae  =  -0.010115808049
 eci+dv2   =    -76.241180477526   dv2 = dv1 / cnot**2       =  -0.010640389651
 eci+dv3   =    -76.241762441957   dv3 = dv1 / (2*cnot**2-1) =  -0.011222354082
 eci+pople =    -76.239321738767   ( 10e- scaled deltae )    =  -0.213966390385
NO coefficients and occupation numbers are written to nocoef_ci.1

################################################################################
 one electron density matrix calculation 
################################################################################


 workspace allocation information: mxcore= 2999634

 space required:

    space required for calls in multd2:
       onex          112
       allin           0
       diagon        330
    max.      ---------
       maxnex        330

    total core space usage:
       maxnex        330
    max k-seg        832
    max k-ind         15
              ---------
       totmax       2024
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=       3  D0X=       9  D0W=      15
  DDZI=       0 DDYI=       5 DDXI=      20 DDWI=      25
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================


*****   symmetry block  A1    *****

 occupation numbers of nos
   1.9999209      1.9863871      1.9709540      0.0212517      0.0100282
   0.0051354      0.0041807      0.0010532      0.0005414      0.0004583
   0.0000507


*****   symmetry block  B1    *****

 occupation numbers of nos
   1.9686350      0.0230421      0.0053457      0.0010698      0.0006112
   0.0004642      0.0000364


*****   symmetry block  B2    *****

 occupation numbers of nos
   1.9761798      0.0146383      0.0040929      0.0005066


*****   symmetry block  A2    *****

 occupation numbers of nos
   0.0047310      0.0006856


 total number of electrons =   10.0000000000



          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!


                        A1  partial gross atomic populations
   ao class       1A1        2A1        3A1        4A1        5A1        6A1 
    O1_ s       2.000566   1.689478  -0.049562   0.001586   0.005055   0.001195
    O1_ p       0.000093   0.039475   1.461300   0.009639   0.003886   0.000036
    O1_ d       0.000000   0.000183   0.002021   0.000268   0.000322   0.002630
    H1_ s      -0.000184   0.204493   0.514051   0.010233   0.000368   0.000250
    H1_ p      -0.000554   0.052758   0.043144  -0.000474   0.000398   0.001025

   ao class       7A1        8A1        9A1       10A1       11A1 
    O1_ s       0.000186   0.000206   0.000011   0.000010   0.000004
    O1_ p       0.000244   0.000173  -0.000006   0.000045   0.000004
    O1_ d       0.003342   0.000398   0.000016   0.000103   0.000001
    H1_ s       0.000037   0.000320   0.000101   0.000006   0.000028
    H1_ p       0.000372  -0.000044   0.000419   0.000295   0.000014

                        B1  partial gross atomic populations
   ao class       1B1        2B1        3B1        4B1        5B1        6B1 
    O1_ p       1.131582   0.011923   0.001822   0.000291   0.000002   0.000065
    O1_ d       0.008400   0.000434   0.002600   0.000383   0.000072   0.000000
    H1_ s       0.801089   0.011142   0.000546   0.000146   0.000251   0.000009
    H1_ p       0.027564  -0.000458   0.000378   0.000250   0.000286   0.000390

   ao class       7B1 
    O1_ p       0.000006
    O1_ d       0.000001
    H1_ s       0.000016
    H1_ p       0.000014

                        B2  partial gross atomic populations
   ao class       1B2        2B2        3B2        4B2 
    O1_ p       1.928483   0.013889   0.000098   0.000026
    O1_ d       0.001132   0.000045   0.003521   0.000069
    H1_ p       0.046565   0.000705   0.000474   0.000412

                        A2  partial gross atomic populations
   ao class       1A2        2A2 
    O1_ d       0.003554   0.000170
    H1_ p       0.001176   0.000515


                        gross atomic populations
     ao           O1_        H1_
      s         3.648735   1.542902
      p         4.603075   0.175624
      d         0.029664   0.000000
    total       8.281473   1.718527


 Total number of electrons:   10.00000000

