
 program cipc      

 print the csf info for mrsdci wave functions

 written by: ron shepard

 version date: 06-jun-96

 This Version of Program cipc is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 workspace allocation parameters: lencor=  32768000 mem1=         0 ifirst=         1

 drt header information:
  cidrt_title                                                                    
 nmot  =    44 niot  =     6 nfct  =     0 nfvt  =     0
 nrow  =    34 nsym  =     4 ssym  =     1 lenbuf=  1600
 spnorb=     F spnodd=     F lxyzir(1:3)= 0 0 0
 nwalk,xbar:        143        8       40       45       50
 nvalwt,nvalw:      137        8       40       42       47
 ncsft:           17157
 map(*)=    39 40 41 42  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16
            43 17 18 19 20 21 22 23 24 25 26 27 28 29 44 30 31 32 33 34
            35 36 37 38
 mu(*)=      2  0  0  0  0  0
 syml(*) =   1  1  1  1  2  4
 rmo(*)=     1  2  3  4  1  1

 indx01:   137 indices saved in indxv(*)
===================================ROOT # 1===================================

 rdhciv: CI vector file information:
  cidrt_title                                                                    
 energy computed by program ciudg.       zam216            14:03:23.992 23-Dec-08

 lenrec =   32768 lenci =     17157 ninfo =  6 nenrgy =  8 ntitle =  2

 Max. overlap with ref vector #        1
 Valid ci vector #        1
 Method:        3       98% overlap
 energy( 1)=  5.220720459423E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -7.631761899821E+01, ietype=    5,   fcore energy of type: Vref(*) 
 energy( 3)= -1.003312990012E+02, ietype=-1038,   total energy of type: MR-AQCC 
 energy( 4)=  3.731947768201E-05, ietype=-2055, cnvginf energy of type: CI-Resid
 energy( 5)=  2.514891406236E-09, ietype=-2056, cnvginf energy of type: CI-D.E. 
 energy( 6)=  2.492178665590E-10, ietype=-2057, cnvginf energy of type: CI-ApxDE
 energy( 7)=  1.022895636879E+00, ietype=-1039,   total energy of type: a4den   
 energy( 8)= -1.000823305449E+02, ietype=-1041,   total energy of type: E(ref)  
==================================================================================

 space is available for  10911278 coefficients.

 updated histogram parameters:
 csfmn = 0.0000E+00 csfmx = 1.0000E+00 fhist = 5.0000E-01 nhist =  20

 this program will print the csfs generated from
 the drt according to the following print options :

 1) run in batch mode: all valid roots are automatically
    analysed and csf info is printed by default contribution
    threshold 0.01 
 2) run in interactive mode

 input menu number [  0]:
================================================================================
===================================VECTOR # 1===================================
================================================================================


 rdcivnew:      86 coefficients were selected.
 workspace: ncsfmx=   17157
 ncsfmx=                    17157

 histogram parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =   17157 ncsft =   17157 ncsf =      86
 nhist =  20 fhist = 0.50000

    cmin                cmax        num  '*'=     2 csfs.
 ----------          ----------   ----- ---------|---------|---------|---------|
 5.0000E-01 <= |c| < 1.0000E+00       1 *
 2.5000E-01 <= |c| < 5.0000E-01       0
 1.2500E-01 <= |c| < 2.5000E-01       0
 6.2500E-02 <= |c| < 1.2500E-01       1 *
 3.1250E-02 <= |c| < 6.2500E-02       2 *
 1.5625E-02 <= |c| < 3.1250E-02      23 ************
 7.8125E-03 <= |c| < 1.5625E-02      59 ******************************
 3.9063E-03 <= |c| < 7.8125E-03       0
 1.9531E-03 <= |c| < 3.9063E-03       0
 9.7656E-04 <= |c| < 1.9531E-03       0
 4.8828E-04 <= |c| < 9.7656E-04       0
 2.4414E-04 <= |c| < 4.8828E-04       0
 1.2207E-04 <= |c| < 2.4414E-04       0
 6.1035E-05 <= |c| < 1.2207E-04       0
 3.0518E-05 <= |c| < 6.1035E-05       0
 1.5259E-05 <= |c| < 3.0518E-05       0
 7.6294E-06 <= |c| < 1.5259E-05       0
 3.8147E-06 <= |c| < 7.6294E-06       0
 0.0000E+00 <= |c| < 3.8147E-06       0
                                  ----- ---------|---------|---------|---------|
                  total read =       86 total stored =      86

 from the selected csfs,
 min(|csfvec(:)|) = 1.0049E-02    max(|csfvec(:)|) = 9.7521E-01
 norm=   0.9999999999999975     
 csfs will be printed based on coefficient magnitudes.

 current csfvec(*) selection parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =   17157 ncsft =   17157 ncsf =      86

 i:slabel(i) =  1: a1   2: b1   3: a2   4: b2 

 internal level =    1    2    3    4    5    6
 syml(*)        =    1    1    1    1    2    4
 label          =  a1   a1   a1   a1   b1   b2 
 rmo(*)         =    1    2    3    4    1    1

 printing selected csfs in sorted order from cmin = 0.01000 to cmax = 1.00000

   indcsf     c     c**2   v  lab:rmo  lab:rmo   step(*)
  ------- -------- ------- - ---- --- ---- --- ------------
        3 -0.97521 0.95103 z*                    333033
        5  0.08189 0.00671 z*                    330333
      168 -0.04962 0.00246 y           b1 :  2  1331223
      159  0.04962 0.00246 y           b2 :  2  1331232
      801  0.03075 0.00095 x  a1 :  9  a2 :  1 11333022
      160 -0.02993 0.00090 y           b2 :  3  1331232
      169  0.02993 0.00090 y           b1 :  3  1331223
     8959  0.02488 0.00062 w           b1 :  3  3333003
     8633  0.02488 0.00062 w           b2 :  3  3333030
     8958 -0.02385 0.00057 w  b1 :  2  b1 :  3 12333003
     8632 -0.02385 0.00057 w  b2 :  2  b2 :  3 12333030
     8957  0.02294 0.00053 w           b1 :  2  3333003
     8631  0.02294 0.00053 w           b2 :  2  3333030
      871  0.02272 0.00052 x  b1 :  3  b2 :  3 11333022
     8750 -0.02226 0.00050 w  b1 :  3  b2 :  3 12333012
      861  0.02146 0.00046 x  b1 :  2  b2 :  2 11333022
     8740 -0.02067 0.00043 w  b1 :  2  b2 :  2 12333012
      302 -0.01991 0.00040 y           a1 :  8  1321233
    12783 -0.01817 0.00033 w  a1 :  8  b1 :  3 12313023
    12639  0.01817 0.00033 w  a1 :  8  b2 :  3 12313032
      301  0.01667 0.00028 y           a1 :  7  1321233
      130 -0.01624 0.00026 y           b1 :  2  1332123
      121  0.01624 0.00026 y           b2 :  2  1332132
     8454  0.01613 0.00026 w           a1 :  9  3333030
     8835  0.01613 0.00026 w           a1 :  9  3333003
     8621  0.01613 0.00026 w           a2 :  1  3333030
     9002  0.01613 0.00026 w           a2 :  1  3333003
      346 -0.01557 0.00024 y           b1 :  4  1313223
      337  0.01557 0.00024 y           b2 :  4  1313232
     1896 -0.01524 0.00023 x  a1 :  6  b2 :  3 11332032
     2040  0.01524 0.00023 x  a1 :  6  b1 :  3 11332023
      374 -0.01512 0.00023 y           a1 :  8  1312233
     8749  0.01486 0.00022 w  b1 :  2  b2 :  3 12333012
     8741  0.01486 0.00022 w  b1 :  3  b2 :  2 12333012
      300  0.01483 0.00022 y           a1 :  6  1321233
      870 -0.01459 0.00021 x  b1 :  2  b2 :  3 11333022
      862 -0.01459 0.00021 x  b1 :  3  b2 :  2 11333022
    12767  0.01385 0.00019 w  a1 :  8  b1 :  2 12313023
    12623 -0.01385 0.00019 w  a1 :  8  b2 :  2 12313032
     8636  0.01338 0.00018 w           b2 :  4  3333030
     8962  0.01338 0.00018 w           b1 :  4  3333003
      173 -0.01276 0.00016 y           b1 :  7  1331223
      164  0.01276 0.00016 y           b2 :  7  1331232
        2  0.01269 0.00016 z*                    333303
        1  0.01269 0.00016 z*                    333330
    16099  0.01251 0.00016 w           a1 :  8  3303033
      881  0.01209 0.00015 x  b1 :  4  b2 :  4 11333022
     8760 -0.01171 0.00014 w  b1 :  4  b2 :  4 12333012
    12637 -0.01164 0.00014 w  a1 :  6  b2 :  3 12313032
    12781  0.01164 0.00014 w  a1 :  6  b1 :  3 12313023
       67  0.01164 0.00014 y           b2 :  2  1333032
       76 -0.01164 0.00014 y           b1 :  2  1333023
      377 -0.01162 0.00014 y           a1 : 11  1312233
      172  0.01151 0.00013 y           b1 :  6  1331223
      163 -0.01151 0.00013 y           b2 :  6  1331232
     1845  0.01148 0.00013 x  b1 :  4  a2 :  1 11332032
     2175  0.01148 0.00013 x  a2 :  1  b2 :  4 11332023
     1915  0.01148 0.00013 x  a1 :  9  b2 :  4 11332032
     2059  0.01148 0.00013 x  a1 :  9  b1 :  4 11332023
     1917  0.01133 0.00013 x  a1 : 11  b2 :  4 11332032
     2061 -0.01133 0.00013 x  a1 : 11  b1 :  4 11332023
    10036 -0.01120 0.00013 w  b1 :  4  a2 :  1 12331032
    10366  0.01120 0.00013 w  a2 :  1  b2 :  4 12331023
    10106  0.01120 0.00013 w  a1 :  9  b2 :  4 12331032
    10250  0.01120 0.00013 w  a1 :  9  b1 :  4 12331023
     3937 -0.01118 0.00013 x  a1 :  8  b1 :  2 11323023
     3793  0.01118 0.00013 x  a1 :  8  b2 :  2 11323032
    11373  0.01113 0.00012 w           a1 :  6  3330033
        8  0.01104 0.00012 z*                    303333
    11391  0.01090 0.00012 w           a1 : 10  3330033
    13928  0.01090 0.00012 w  a1 :  6  a1 :  8 12312033
    10231 -0.01061 0.00011 w  a1 :  6  b1 :  3 12331023
    10087  0.01061 0.00011 w  a1 :  6  b2 :  3 12331032
    12847  0.01057 0.00011 w  a1 :  8  b1 :  7 12313023
    12703 -0.01057 0.00011 w  a1 :  8  b2 :  7 12313032
    11512  0.01042 0.00011 w           b1 :  4  3330033
    11567  0.01042 0.00011 w           b2 :  4  3330033
     8973 -0.01031 0.00011 w  b1 :  3  b1 :  7 12333003
     8647 -0.01031 0.00011 w  b2 :  3  b2 :  7 12333030
     3809 -0.01028 0.00011 x  a1 :  8  b2 :  3 11323032
     3953  0.01028 0.00011 x  a1 :  8  b1 :  3 11323023
     8977  0.01027 0.00011 w           b1 :  7  3333003
     8651  0.01027 0.00011 w           b2 :  7  3333030
    12765 -0.01020 0.00010 w  a1 :  6  b1 :  2 12313023
    12621  0.01020 0.00010 w  a1 :  6  b2 :  2 12313032
     8682  0.01005 0.00010 w  a1 : 11  a2 :  1 12333012
           86 csfs were printed in this range.
