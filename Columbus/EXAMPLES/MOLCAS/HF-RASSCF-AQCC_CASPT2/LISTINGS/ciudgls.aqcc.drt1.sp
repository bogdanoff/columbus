
======= Computing sorting integral file structure =========

 nmpsy=                       20                       10 
                        4                       10
                    -----z----- -----y----- -----x----- -----w----- ---total---

                CSFs         8         428        7407        9314       17157
      internal walks         8          40          45          50         143
valid internal walks         8          40          42          47         137
 found l2rec=                     4096 n2max=                     2730

 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg=  32583894
 minimum size of srtscr:                   229369  WP(  
                        7  records)
 maximum size of srtscr:                   360437  WP(  
                        9  records)

sorted 4-external integrals:     3 records of integral w-combinations and
             2 records of integral x-combinations of length= 32767 have been written.
 wstat,xstat=  100.00  100.00

sorted 3-external integrals:     2 records of integral w-combinations and
             2 records of integral x-combinations of length= 32767 have been written.
 wstat,xstat=  100.00  100.00
 Orig.  diagonal integrals:  1electron:        44
                             0ext.    :        42
                             2ext.    :       456
                             4ext.    :      1482


 Orig. off-diag. integrals:  4ext.    :     79122
                             3ext.    :     47394
                             2ext.    :     13251
                             1ext.    :      2040
                             0ext.    :       138
                             2ext. SO :         0
                             1ext. SO :         0
                             0ext. SO :         0
                             1electron:       236


 Sorted integrals            3ext.  w :     44360 x :     41326
                             4ext.  w :     70390 x :     62163


1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      -varseg3    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 compressed index vector length=                       25
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
    NTYPE=3
    GSET=3
   DAVCOR =10,
  NCOREL = 8
  NROOT = 1
  IVMODE = 3
  NBKITR = 1
  NVBKMN = 1
  NVBKMX = 6
  RTOLBK = 1e-4
  NITER = 30
  NVCIMN = 1
  NVCIMX = 6
  RTOLCI = 1e-4
  IDEN  = 1
  CSFPRN = 10,
    MOLCAS=1
 /&end
 ------------------------------------------------------------------------
 Ame=                        0 ivmode,nbkitr=                        3 
                        1 ntype=                        3
 me=                        0 ivmode,nbkitr=                        3 
                        1 ntype=                        3
 bummer (warning):2:changed keyword: nbkitr=         0
 ntotseg(*)=                        4                        4 
                        4                        4                        4 
                        4
 iexplseg(*)=                        0                        0 
                        0                        0                        0 
                        0
 resetting explseg from                         0  to                         0
 USING SEGMENTS OF EQUAL SIZE

****************  list of control variables  ****************
 lvlprt =    0      nroot  =    1      noldv  =   0      noldhv =   0
 nunitv =    1      nbkitr =    0      niter  =  30      davcor =  10
 csfprn =   10      ivmode =    3      istrt  =   0      vout   =   1
 iortls =    0      nvbkmx =    6      ibktv  =  -1      ibkthv =  -1
 nvcimx =    6      icitv  =   -1      icithv =  -1      frcsub =   0
 nvbkmn =    1      nvcimn =    1      maxseg =   4      nrfitr =  30
 ncorel =    8      nvrfmx =    6      nvrfmn =   1      iden   =   1
 itran  =    0      froot  =    0      rtmode =   0      ncouple=   1
 skipso =    F      dalton2=    0      molcas =   1      finalv =   0
 finalw =    0      cosmocalc=   0    with_tsklst=   0
 nsegwx =    1     1     1     1
 nseg0x =    1     1     1     1
 nseg1x =    1     1     1     1
 nseg2x =    1     1     1     1
 nseg3x =    1     1     1     1
 nseg4x =    1     1     1     1
 no0ex  =      0    no1ex  =      0    no2ex  =     0    no3ex  =     0
 no4ex  =      0    nodiag =      0
 cdg4ex =    1      c3ex1ex=    1      c2ex0ex=   1
 fileloc=    0     0     0     0     0     0     0     1     1     1

 ctol   = 0.010000    lrtshift=1.000000    smalld =0.001000


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-04    1.000E-04
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core           32767999 DP per process

********** Integral sort section *************


 workspace allocation information: lencor=  32767999

 echo of the input for program cisrt:
 ------------------------------------------------------------------------
  &input
  maxbl3=60000
  maxbl4=60000
  &end
 ------------------------------------------------------------------------

 ( 6) listing file:                    ciudgls             
 ( 5) input file:                      cisrtin   
 (17) cidrt file:                      cidrtfl             
 (11) transformed integrals file:      moints    
 (12) diagonal integral file:          diagint             
 (13) off-diagonal integral file:      ofdgint             
 (31) 4-external w integrals file:     fil4w               
 (32) 4-external x integrals file:     fil4x               
 (33) 3-external w integrals file:     fil3w               
 (34) 3-external x integrals file:     fil3x               
 (21) scratch da sorting file:         srtscr              
 (12) 2-e integral file [fsplit=2]:    moints2   

 input integral file header information:
 SEWARD INTEGRALS                                                                
  cidrt_title                                                                    
 SIFS file created by program tran.      zam216            14:03:22.058 23-Dec-08

 input energy(*) values:
 energy( 1)=  5.220720459423E+00, ietype=   -1,    core energy of type: Nuc.Rep.

 total core energy =   5.220720459423E+00

 nsym = 4 nmot=  44

 symmetry  =    1    2    3    4
 slabel(*) = a1   b1   a2   b2  
 nmpsy(*)  =   20   10    4   10

 info(*) =          1      4096      3272      4096      2730

 orbital labels, i:molab(i)=
   1:tout:001   2:tout:002   3:tout:003   4:tout:004   5:tout:005   6:tout:006   7:tout:007   8:tout:008   9:tout:009  10:tout:010
  11:tout:011  12:tout:012  13:tout:013  14:tout:014  15:tout:015  16:tout:016  17:tout:017  18:tout:018  19:tout:019  20:tout:020
  21:tout:021  22:tout:022  23:tout:023  24:tout:024  25:tout:025  26:tout:026  27:tout:027  28:tout:028  29:tout:029  30:tout:030
  31:tout:031  32:tout:032  33:tout:033  34:tout:034  35:tout:035  36:tout:036  37:tout:037  38:tout:038  39:tout:039  40:tout:040
  41:tout:041  42:tout:042  43:tout:043  44:tout:044

 input parameters:
 prnopt=  0
 ldamin=    4095 ldamax=   32767 ldainc=      64
 maxbuf=   32767 maxbl3=   60000 maxbl4=   60000 intmxo=     766

 drt information:
  cidrt_title                                                                    
 nmotd =  44 nfctd =   0 nfvtc =   0 nmot  =  44
 nlevel =  44 niot  =   6 lowinl=  39
 orbital-to-level map(*)
   39  40  41  42   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15  16
   43  17  18  19  20  21  22  23  24  25  26  27  28  29  44  30  31  32  33  34
   35  36  37  38
 compressed map(*)
   39  40  41  42   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15  16
   43  17  18  19  20  21  22  23  24  25  26  27  28  29  44  30  31  32  33  34
   35  36  37  38
 levsym(*)
    1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   2   2   2   2
    2   2   2   2   2   3   3   3   3   4   4   4   4   4   4   4   4   4   1   1
    1   1   2   4
 repartitioning mu(*)=
   2.  0.  0.  0.  0.  0.

 indxdg: diagonal integral statistics.
 total number of integrals contributing to diagonal matrix elements:      1980
 number with all external indices:      1482
 number with half external - half internal indices:       456
 number with all internal indices:        42

 indxof: off-diagonal integral statistics.
    4-external integrals: num=      79122 strt=          1
    3-external integrals: num=      47394 strt=      79123
    2-external integrals: num=      13251 strt=     126517
    1-external integrals: num=       2040 strt=     139768
    0-external integrals: num=        138 strt=     141808

 total number of off-diagonal integrals:      141945



 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg=  32586861
 !timer: setup required                  user+sys=     0.000 walltime=     0.000
 pro2e        1     991    1981    2971    3961    3982    4003    4993   48681   92369
   125136  129232  131962  142881

 pro2e:    108786 integrals read in    40 records.
 pro1e        1     991    1981    2971    3961    3982    4003    4993   48681   92369
   125136  129232  131962  142881
 pro1e: eref =   -7.631761899820656E+01
 total size of srtscr:                        8  records of  
                    32767 WP =                  2097088 Bytes
 !timer: first half-sort required        user+sys=     0.010 walltime=     0.000

 new core energy added to the energy(*) list.
 from the hamiltonian repartitioning, eref= -7.631761899821E+01
 putdg        1     991    1981    2971    3737   36504   58349    4993   48681   92369
   125136  129232  131962  142881

 putf:       5 buffers of length     766 written to file 12
 diagonal integral file completed.

 putd34:     3 records of integral w-combinations and
             2 records of integral x-combinations of length= 32767 have been written.
 wstat,xstat=  100.00  100.00
 prep4e:    21 blocks of linear combinations of 4-external integrals processed.
 number of sorted 4-external integrals     132553
 number of original 4-external integrals    79122


 putf34: external integral file complete. nfilw=    31 nfilx=    32 nrecw=     3 nrecx=     2 lbufp= 32767

 putd34:     2 records of integral w-combinations and
             2 records of integral x-combinations of length= 32767 have been written.
 wstat,xstat=  100.00  100.00
 prep3e:    21 blocks of linear combinations of 3-external integrals processed.
 number of sorted 3-external integrals      85686
 number of original 3-external integrals    47394


 putf34: external integral file complete. nfilw=    33 nfilx=    34 nrecw=     2 nrecx=     2 lbufp= 32767

 putf:      23 buffers of length     766 written to file 13
 off-diagonal files sort completed.
 !timer: second half-sort required       user+sys=     0.020 walltime=     0.000
 !timer: cisrt complete                  user+sys=     0.030 walltime=     0.000
 executing brd_struct for cisrtinfo
cisrtinfo:
bufszi   766
 diagfile 4ext:    1482 2ext:     456 0ext:      42
 fil4w,fil4x  :   79122 fil3w,fil3x :   47394
 ofdgint  2ext:   13251 1ext:    2040 0ext:     138so0ext:       0so1ext:       0so2ext:       0
buffer minbl4     705 minbl3     705 maxbl2     708nbas:  16   9   4   9   0   0   0   0 maxbuf 32767
 CIUDG version 5.9.7 ( 5-Oct-2004)

 workspace allocation information: lcore=  32767999

 core energy values from the integral file:
 energy( 1)=  5.220720459423E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -7.631761899821E+01, ietype=    5,   fcore energy of type: Vref(*) 

 total core repulsion energy = -7.109689853878E+01
 nmot  =    44 niot  =     6 nfct  =     0 nfvt  =     0
 nrow  =    34 nsym  =     4 ssym  =     1 lenbuf=  1600
 nwalk,xbar:        143        8       40       45       50
 nvalwt,nvalw:      137        8       40       42       47
 ncsft:           17157
 total number of valid internal walks:     137
 nvalz,nvaly,nvalx,nvalw =        8      40      42      47

 cisrt info file parameters:
 file number  12 blocksize    766
 mxbld   1532
 nd4ext,nd2ext,nd0ext  1482   456    42
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int    79122    47394    13251     2040      138        0        0        0
 minbl4,minbl3,maxbl2   705   705   708
 maxbuf 32767
 number of external orbitals per symmetry block:  16   9   4   9
 nmsym   4 number of internal orbitals   6
 executing brd_struct for drt
 executing brd_struct for orbinf
 executing brd_struct for momap
 calcthrxt: niot,maxw1=                        6                       39
 block size     0
 pthz,pthy,pthx,pthw:     8    40    45    50 total internal walks:     143
 maxlp3,n2lp,n1lp,n0lp    39     0     0     0
 orbsym(*)= 1 1 1 1 2 4

 setref:        8 references kept,
                0 references were marked as invalid, out of
                8 total.
 limcnvrt: found                        8  valid internal walksout of  
                        8  walks (skipping trailing invalids)
  ... adding                         8  segmentation marks segtype= 
                        1
 limcnvrt: found                       40  valid internal walksout of  
                       40  walks (skipping trailing invalids)
  ... adding                        40  segmentation marks segtype= 
                        2
 limcnvrt: found                       42  valid internal walksout of  
                       45  walks (skipping trailing invalids)
  ... adding                        42  segmentation marks segtype= 
                        3
 limcnvrt: found                       47  valid internal walksout of  
                       50  walks (skipping trailing invalids)
  ... adding                        47  segmentation marks segtype= 
                        4

 norbsm(*)=  20  10   4  10   0   0   0   0
 noxt(*)=  16   9   4   9   0   0   0   0
 intorb=   6 nmsym=   4 lowdoc=   5
 ivout(*)=   6   7   8   9  10  11  12  13  14  15  16  17  18  19  20  22  23  24  25  26
  27  28  29  30  31  32  33  34  36  37  38  39  40  41  42  43  44   1   2   3
   4  21  35   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0
 post-tapin: reflst,limvec,indsym=                        1 
                        9                      152
 ---------memory usage in DP -----------------
 < n-ex core usage >
     routines:
    fourex            66792
    threx             33002
    twoex              4642
    onex               1343
    allin               766
    diagon             2125
               =======
   maximum            66792

  __ static summary __ 
   reflst                 8
   hrfspc                 8
               -------
   static->              16

  __ core required  __ 
   totstc                16
   max n-ex           66792
               -------
   totnec->           66808

  __ core available __ 
   totspc          32767999
   totnec -           66808
               -------
   totvec->        32701191

 number of external paths / symmetry
 vertex x     198     180     145     180
 vertex w     236     180     145     180
segment: free space=    32701191
 reducing frespc by                      598 to                  32700593 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1           8|         8|         0|         8|         0|         1|
 -------------------------------------------------------------------------------
  Y 2          40|       428|         8|        40|         8|         2|
 -------------------------------------------------------------------------------
  X 3          42|      7407|       436|        42|        48|         3|
 -------------------------------------------------------------------------------
  W 4          47|      9314|      7843|        47|        90|         4|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=           4DP  conft+indsym=         188DP  drtbuffer=         406 DP

dimension of the ci-matrix ->>>     17157

 executing brd_struct for civct
 gentasklist: ntask=                       20
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS   
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      42       8       7407          8      42       8
     2  4   1    25      two-ext wz   2X  4 1      47       8       9314          8      47       8
     3  4   3    26      two-ext wx*  WX  4 3      47      42       9314       7407      47      42
     4  4   3    27      two-ext wx+  WX  4 3      47      42       9314       7407      47      42
     5  2   1    11      one-ext yz   1X  2 1      40       8        428          8      40       8
     6  3   2    15      1ex3ex yx    3X  3 2      42      40       7407        428      42      40
     7  4   2    16      1ex3ex yw    3X  4 2      47      40       9314        428      47      40
     8  1   1     1      allint zz    OX  1 1       8       8          8          8       8       8
     9  2   2     5      0ex2ex yy    OX  2 2      40      40        428        428      40      40
    10  3   3     6      0ex2ex xx*   OX  3 3      42      42       7407       7407      42      42
    11  3   3    18      0ex2ex xx+   OX  3 3      42      42       7407       7407      42      42
    12  4   4     7      0ex2ex ww*   OX  4 4      47      47       9314       9314      47      47
    13  4   4    19      0ex2ex ww+   OX  4 4      47      47       9314       9314      47      47
    14  2   2    42      four-ext y   4X  2 2      40      40        428        428      40      40
    15  3   3    43      four-ext x   4X  3 3      42      42       7407       7407      42      42
    16  4   4    44      four-ext w   4X  4 4      47      47       9314       9314      47      47
    17  1   1    75      dg-024ext z  OX  1 1       8       8          8          8       8       8
    18  2   2    76      dg-024ext y  OX  2 2      40      40        428        428      40      40
    19  3   3    77      dg-024ext x  OX  3 3      42      42       7407       7407      42      42
    20  4   4    78      dg-024ext w  OX  4 4      47      47       9314       9314      47      47
----------------------------------------------------------------------------------------------------
REDTASK #   1 TIME=  19.000 N=   1 indiv tasks=   1
REDTASK #   2 TIME=  18.000 N=   1 indiv tasks=   2
REDTASK #   3 TIME=  17.000 N=   1 indiv tasks=   3
REDTASK #   4 TIME=  16.000 N=   1 indiv tasks=   4
REDTASK #   5 TIME=  15.000 N=   1 indiv tasks=   5
REDTASK #   6 TIME=  14.000 N=   1 indiv tasks=   6
REDTASK #   7 TIME=  13.000 N=   1 indiv tasks=   7
REDTASK #   8 TIME=  12.000 N=   1 indiv tasks=   8
REDTASK #   9 TIME=  11.000 N=   1 indiv tasks=   9
REDTASK #  10 TIME=  10.000 N=   1 indiv tasks=  10
REDTASK #  11 TIME=   9.000 N=   1 indiv tasks=  11
REDTASK #  12 TIME=   8.000 N=   1 indiv tasks=  12
REDTASK #  13 TIME=   7.000 N=   1 indiv tasks=  13
REDTASK #  14 TIME=   6.000 N=   1 indiv tasks=  14
REDTASK #  15 TIME=   5.000 N=   1 indiv tasks=  15
REDTASK #  16 TIME=   4.000 N=   1 indiv tasks=  16
REDTASK #  17 TIME=   3.000 N=   1 indiv tasks=  17
REDTASK #  18 TIME=   2.000 N=   1 indiv tasks=  18
REDTASK #  19 TIME=   1.000 N=   1 indiv tasks=  19
REDTASK #  20 TIME=   0.000 N=   1 indiv tasks=  20
 initializing v-file: 1:                    17157

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:          95 2x:           0 4x:           0
All internal counts: zz :          55 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:           0    task #     2:           0    task #     3:           0    task #     4:           0
task #     5:           0    task #     6:           0    task #     7:           0    task #     8:          54
task #     9:           0    task #    10:           0    task #    11:           0    task #    12:           0
task #    13:           0    task #    14:           0    task #    15:           0    task #    16:           0
task #    17:          82    task #    18:           0    task #    19:           0    task #    20:           0
 reference space has dimension       8

    root           eigenvalues
    ----           ------------
       1        -100.0823305449

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt=                        8

         vector  1 from unit 11 written to unit 49 filename cirefv              
 ncorel,neli=                        8                       10
  This is a  mraqcc energy evaluation      

    ------------------------------------------------------------

 setopt2: mraqcc energy evaluation      gvalue=   0.4642857142857143      
 ncorel=                        8

    ------------------------------------------------------------


    reference energies:

  reference state used is:   1with energy     -100.0823305449

  ### active excitation selection ###

 Inactive orbitals:                        1
    there are        8 all-active excitations of which      8 are references.

    the      8 reference all-active excitation csfs

    ------------------------------------------------
         1     2     3     4     5     6     7     8
    ------------------------------------------------
  2:     2     2     2     2     2     1     1     0
  3:     2     2     2     1     0     2    -1     2
  4:     2     2     0    -1     2    -1     2     2
  5:     2     0     2     2     2     2     2     2
  6:     0     2     2     2     2     2     2     2

  ### end active excitation selection ###


 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:             17157
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                   30
 residual norm convergence criteria:               0.000100

          starting ci iteration   1

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    1.124793
ci vector #   2dasum_wr=    0.000000
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:        1540 2x:         545 4x:         129
All internal counts: zz :          55 yy:         391 xx:         830 ww:         824
One-external counts: yz :         349 yx:        1619 yw:        1632
Two-external counts: yy :         470 ww:         852 xx:         968 xz:          68 wz:          93 wx:        1100
Three-ext.   counts: yx :         426 yw:         455

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         127    task #     2:         163    task #     3:         899    task #     4:         449
task #     5:         332    task #     6:        1463    task #     7:        1441    task #     8:          54
task #     9:         669    task #    10:         741    task #    11:         741    task #    12:         595
task #    13:         595    task #    14:         297    task #    15:         299    task #    16:         299
task #    17:          82    task #    18:         581    task #    19:         579    task #    20:         529

          calcsovref: tciref block   1

              civs   1
 refs   1    1.00000    

          calcsovref: scrb block   1

              civs   1
 civs   1    1.00000    
 eci,repnuc=   -28.98543200610800        -71.09689853878315     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  1  1   -100.0823305449 -2.4869E-14  2.8280E-01  1.4291E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0001
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0002
    3   26    0     0.00     0.00     0.00     0.00         0.    0.0009
    4   27    0     0.01     0.01     0.00     0.01         0.    0.0005
    5   11    0     0.00     0.00     0.00     0.00         0.    0.0003
    6   15    0     0.01     0.00     0.01     0.01         0.    0.0015
    7   16    0     0.00     0.00     0.00     0.00         0.    0.0014
    8    1    0     0.01     0.00     0.01     0.00         0.    0.0001
    9    5    0     0.00     0.00     0.00     0.00         0.    0.0007
   10    6    0     0.01     0.00     0.00     0.01         0.    0.0007
   11   18    0     0.00     0.00     0.00     0.00         0.    0.0007
   12    7    0     0.01     0.00     0.00     0.01         0.    0.0006
   13   19    0     0.01     0.00     0.01     0.00         0.    0.0006
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0003
   15   43    0     0.00     0.00     0.00     0.00         0.    0.0003
   16   44    0     0.01     0.00     0.00     0.01         0.    0.0003
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0001
   18   76    0     0.00     0.00     0.00     0.00         0.    0.0006
   19   77    0     0.00     0.00     0.00     0.00         0.    0.0006
   20   78    0     0.00     0.00     0.00     0.00         0.    0.0001
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0700s 
time spent in multnx:                   0.0500s 
integral transfer time:                 0.0300s 
time spent for loop construction:       0.0100s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.0900s 

          starting ci iteration   2

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.000000
ci vector #   2dasum_wr=    0.915615
ci vector #   3dasum_wr=    2.200899
ci vector #   4dasum_wr=    4.165199


====================================================================================================
Diagonal     counts:  0x:        1540 2x:         545 4x:         129
All internal counts: zz :          55 yy:         391 xx:         830 ww:         824
One-external counts: yz :         349 yx:        1619 yw:        1632
Two-external counts: yy :         470 ww:         852 xx:         968 xz:          68 wz:          93 wx:        1100
Three-ext.   counts: yx :         426 yw:         455

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         127    task #     2:         163    task #     3:         899    task #     4:         449
task #     5:         332    task #     6:        1463    task #     7:        1441    task #     8:          54
task #     9:         669    task #    10:         741    task #    11:         741    task #    12:         595
task #    13:         595    task #    14:         297    task #    15:         299    task #    16:         299
task #    17:          82    task #    18:         581    task #    19:         579    task #    20:         529

          calcsovref: tciref block   1

              civs   1       civs   2
 refs   1    1.00000      -2.882599E-13

          calcsovref: scrb block   1

              civs   1       civs   2
 civs   1   0.980979      -0.194112    
 civs   2   0.809451        4.09070    
 eci,repnuc=   -29.21877908724286        -71.09689853878315     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  2  1   -100.3156776260  2.3335E-01  1.2672E-02  2.7847E-01  1.0000E-04
 mraqcc  #  2  2    -94.1227199041  2.3026E+01  0.0000E+00  3.7122E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0001
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0002
    3   26    0     0.00     0.00     0.00     0.00         0.    0.0009
    4   27    0     0.02     0.00     0.01     0.01         0.    0.0005
    5   11    0     0.00     0.00     0.00     0.00         0.    0.0003
    6   15    0     0.00     0.00     0.00     0.00         0.    0.0015
    7   16    0     0.01     0.01     0.00     0.01         0.    0.0014
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0001
    9    5    0     0.00     0.00     0.00     0.00         0.    0.0007
   10    6    0     0.01     0.00     0.00     0.01         0.    0.0007
   11   18    0     0.00     0.00     0.00     0.00         0.    0.0007
   12    7    0     0.01     0.00     0.00     0.01         0.    0.0006
   13   19    0     0.01     0.00     0.00     0.01         0.    0.0006
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0003
   15   43    0     0.00     0.00     0.00     0.00         0.    0.0003
   16   44    0     0.00     0.00     0.00     0.00         0.    0.0003
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0001
   18   76    0     0.00     0.00     0.00     0.00         0.    0.0006
   19   77    0     0.01     0.00     0.00     0.01         0.    0.0006
   20   78    0     0.00     0.00     0.00     0.00         0.    0.0001
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0700s 
time spent in multnx:                   0.0600s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.0100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.0800s 

          starting ci iteration   3

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.051924
ci vector #   2dasum_wr=    0.297572
ci vector #   3dasum_wr=    0.691145
ci vector #   4dasum_wr=    1.029280


====================================================================================================
Diagonal     counts:  0x:        1540 2x:         545 4x:         129
All internal counts: zz :          55 yy:         391 xx:         830 ww:         824
One-external counts: yz :         349 yx:        1619 yw:        1632
Two-external counts: yy :         470 ww:         852 xx:         968 xz:          68 wz:          93 wx:        1100
Three-ext.   counts: yx :         426 yw:         455

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         127    task #     2:         163    task #     3:         899    task #     4:         449
task #     5:         332    task #     6:        1463    task #     7:        1441    task #     8:          54
task #     9:         669    task #    10:         741    task #    11:         741    task #    12:         595
task #    13:         595    task #    14:         297    task #    15:         299    task #    16:         299
task #    17:          82    task #    18:         581    task #    19:         579    task #    20:         529

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3
 refs   1    1.00000      -2.882599E-13   1.704962E-02

          calcsovref: scrb block   1

              civs   1       civs   2       civs   3
 civs   1   0.966664       0.289280      -0.273891    
 civs   2   0.842468       0.492147        4.08943    
 civs   3   0.757018       -17.3651        4.27607    
 eci,repnuc=   -29.23328741610625        -71.09689853878315     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  3  1   -100.3301859549  1.4508E-02  7.4372E-04  6.6877E-02  1.0000E-04
 mraqcc  #  3  2    -94.9417287858  8.1901E-01  0.0000E+00  2.4683E+00  1.0000E-04
 mraqcc  #  3  3    -94.2005597691  2.3104E+01  0.0000E+00  3.1482E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0001
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0002
    3   26    0     0.01     0.00     0.00     0.01         0.    0.0009
    4   27    0     0.00     0.00     0.00     0.00         0.    0.0005
    5   11    0     0.01     0.00     0.01     0.00         0.    0.0003
    6   15    0     0.00     0.00     0.00     0.00         0.    0.0015
    7   16    0     0.01     0.00     0.00     0.01         0.    0.0014
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0001
    9    5    0     0.01     0.00     0.00     0.01         0.    0.0007
   10    6    0     0.00     0.00     0.00     0.00         0.    0.0007
   11   18    0     0.00     0.00     0.00     0.00         0.    0.0007
   12    7    0     0.01     0.00     0.00     0.01         0.    0.0006
   13   19    0     0.01     0.00     0.00     0.01         0.    0.0006
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0003
   15   43    0     0.00     0.00     0.00     0.00         0.    0.0003
   16   44    0     0.01     0.00     0.00     0.01         0.    0.0003
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0001
   18   76    0     0.00     0.00     0.00     0.00         0.    0.0006
   19   77    0     0.00     0.00     0.00     0.00         0.    0.0006
   20   78    0     0.00     0.00     0.01     0.00         0.    0.0001
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0700s 
time spent in multnx:                   0.0600s 
integral transfer time:                 0.0200s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.0800s 

          starting ci iteration   4

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.012701
ci vector #   2dasum_wr=    0.073212
ci vector #   3dasum_wr=    0.189419
ci vector #   4dasum_wr=    0.255413


====================================================================================================
Diagonal     counts:  0x:        1540 2x:         545 4x:         129
All internal counts: zz :          55 yy:         391 xx:         830 ww:         824
One-external counts: yz :         349 yx:        1619 yw:        1632
Two-external counts: yy :         470 ww:         852 xx:         968 xz:          68 wz:          93 wx:        1100
Three-ext.   counts: yx :         426 yw:         455

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         127    task #     2:         163    task #     3:         899    task #     4:         449
task #     5:         332    task #     6:        1463    task #     7:        1441    task #     8:          54
task #     9:         669    task #    10:         741    task #    11:         741    task #    12:         595
task #    13:         595    task #    14:         297    task #    15:         299    task #    16:         299
task #    17:          82    task #    18:         581    task #    19:         579    task #    20:         529

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4
 refs   1    1.00000      -2.882599E-13   1.704962E-02  -3.527939E-03

          calcsovref: scrb block   1

              civs   1       civs   2       civs   3       civs   4
 civs   1   0.968527       4.948485E-03   0.256655      -0.373270    
 civs   2   0.843322       -1.20589        1.16581        3.82425    
 civs   3   0.812997       -7.90309       -15.7500        4.13058    
 civs   4   0.956030       -66.2982        9.68105       -37.9289    
 eci,repnuc=   -29.23431114135198        -71.09689853878315     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  4  1   -100.3312096801  1.0237E-03  6.0370E-05  1.8320E-02  1.0000E-04
 mraqcc  #  4  2    -96.1594237231  1.2177E+00  0.0000E+00  2.0931E+00  1.0000E-04
 mraqcc  #  4  3    -94.9116807203  7.1112E-01  0.0000E+00  2.1699E+00  1.0000E-04
 mraqcc  #  4  4    -93.6049351570  2.2508E+01  0.0000E+00  3.2540E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.01     0.00     0.00     0.00         0.    0.0001
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0002
    3   26    0     0.00     0.00     0.00     0.00         0.    0.0009
    4   27    0     0.01     0.00     0.00     0.01         0.    0.0005
    5   11    0     0.00     0.00     0.00     0.00         0.    0.0003
    6   15    0     0.01     0.00     0.00     0.01         0.    0.0015
    7   16    0     0.01     0.00     0.00     0.01         0.    0.0014
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0001
    9    5    0     0.00     0.00     0.00     0.00         0.    0.0007
   10    6    0     0.01     0.00     0.00     0.01         0.    0.0007
   11   18    0     0.00     0.00     0.00     0.00         0.    0.0007
   12    7    0     0.01     0.00     0.00     0.01         0.    0.0006
   13   19    0     0.01     0.01     0.00     0.01         0.    0.0006
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0003
   15   43    0     0.00     0.00     0.00     0.00         0.    0.0003
   16   44    0     0.00     0.00     0.00     0.00         0.    0.0003
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0001
   18   76    0     0.00     0.00     0.00     0.00         0.    0.0006
   19   77    0     0.00     0.00     0.00     0.00         0.    0.0006
   20   78    0     0.01     0.00     0.00     0.00         0.    0.0001
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0800s 
time spent in multnx:                   0.0600s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.0800s 

          starting ci iteration   5

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.004512
ci vector #   2dasum_wr=    0.025307
ci vector #   3dasum_wr=    0.045932
ci vector #   4dasum_wr=    0.069278


====================================================================================================
Diagonal     counts:  0x:        1540 2x:         545 4x:         129
All internal counts: zz :          55 yy:         391 xx:         830 ww:         824
One-external counts: yz :         349 yx:        1619 yw:        1632
Two-external counts: yy :         470 ww:         852 xx:         968 xz:          68 wz:          93 wx:        1100
Three-ext.   counts: yx :         426 yw:         455

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         127    task #     2:         163    task #     3:         899    task #     4:         449
task #     5:         332    task #     6:        1463    task #     7:        1441    task #     8:          54
task #     9:         669    task #    10:         741    task #    11:         741    task #    12:         595
task #    13:         595    task #    14:         297    task #    15:         299    task #    16:         299
task #    17:          82    task #    18:         581    task #    19:         579    task #    20:         529

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1    1.00000      -2.882599E-13   1.704962E-02  -3.527939E-03   1.355044E-03

          calcsovref: scrb block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 civs   1   0.967157      -0.274138      -0.337123      -0.240312      -0.183049    
 civs   2   0.843188       0.609166       -1.21383        2.09597        3.46182    
 civs   3   0.817662        5.82606        9.05548       -10.1135        11.5197    
 civs   4    1.03567        40.6562       -43.8055       -47.7786       -8.71263    
 civs   5   0.982089        165.292        65.8149        115.192       -141.090    
 eci,repnuc=   -29.23439311034084        -71.09689853878315     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  5  1   -100.3312916491  8.1969E-05  5.1787E-06  5.3826E-03  1.0000E-04
 mraqcc  #  5  2    -97.7430044268  1.5836E+00  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  5  3    -95.1374397265  2.2576E-01  0.0000E+00  2.2897E+00  1.0000E-04
 mraqcc  #  5  4    -94.1977673978  5.9283E-01  0.0000E+00  2.5488E+00  1.0000E-04
 mraqcc  #  5  5    -92.9800765923  2.1883E+01  0.0000E+00  3.4791E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0001
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0002
    3   26    0     0.00     0.00     0.00     0.00         0.    0.0009
    4   27    0     0.01     0.00     0.00     0.00         0.    0.0005
    5   11    0     0.00     0.00     0.00     0.00         0.    0.0003
    6   15    0     0.01     0.00     0.00     0.01         0.    0.0015
    7   16    0     0.01     0.00     0.00     0.01         0.    0.0014
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0001
    9    5    0     0.00     0.00     0.00     0.00         0.    0.0007
   10    6    0     0.01     0.00     0.00     0.01         0.    0.0007
   11   18    0     0.00     0.00     0.00     0.00         0.    0.0007
   12    7    0     0.01     0.00     0.00     0.01         0.    0.0006
   13   19    0     0.01     0.00     0.00     0.01         0.    0.0006
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0003
   15   43    0     0.00     0.00     0.00     0.00         0.    0.0003
   16   44    0     0.01     0.00     0.01     0.01         0.    0.0003
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0001
   18   76    0     0.00     0.00     0.00     0.00         0.    0.0006
   19   77    0     0.01     0.01     0.00     0.01         0.    0.0006
   20   78    0     0.00     0.00     0.00     0.00         0.    0.0001
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0800s 
time spent in multnx:                   0.0700s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.0100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.0800s 

          starting ci iteration   6

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.001404
ci vector #   2dasum_wr=    0.007317
ci vector #   3dasum_wr=    0.015016
ci vector #   4dasum_wr=    0.021541


====================================================================================================
Diagonal     counts:  0x:        1540 2x:         545 4x:         129
All internal counts: zz :          55 yy:         391 xx:         830 ww:         824
One-external counts: yz :         349 yx:        1619 yw:        1632
Two-external counts: yy :         470 ww:         852 xx:         968 xz:          68 wz:          93 wx:        1100
Three-ext.   counts: yx :         426 yw:         455

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         127    task #     2:         163    task #     3:         899    task #     4:         449
task #     5:         332    task #     6:        1463    task #     7:        1441    task #     8:          54
task #     9:         669    task #    10:         741    task #    11:         741    task #    12:         595
task #    13:         595    task #    14:         297    task #    15:         299    task #    16:         299
task #    17:          82    task #    18:         581    task #    19:         579    task #    20:         529

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1    1.00000      -2.882599E-13   1.704962E-02  -3.527939E-03   1.355044E-03  -1.066067E-04

          calcsovref: scrb block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 civs   1   0.967134       0.203189      -0.327594      -0.259815      -0.190067      -0.170827    
 civs   2   0.843162      -0.392050       0.885655       -1.57803        1.93515        3.32397    
 civs   3   0.818033       -4.82691        1.71734        9.82454       -9.27660        12.4283    
 civs   4    1.04206       -35.2629       -3.48967       -39.2134       -59.4298       -2.94840    
 civs   5    1.06115       -148.704        116.351        35.9538        83.6203       -148.310    
 civs   6   0.921558       -342.742       -638.561        138.144        445.077       -66.3622    

 trial vector basis is being transformed.  new dimension:   1
 eci,repnuc=   -29.23439971649177        -71.09689853878315     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  6  1   -100.3312982553  6.6062E-06  5.1972E-07  1.6746E-03  1.0000E-04
 mraqcc  #  6  2    -98.1891686964  4.4616E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  6  3    -95.7822785980  6.4484E-01  0.0000E+00  2.1200E+00  1.0000E-04
 mraqcc  #  6  4    -95.1024112020  9.0464E-01  0.0000E+00  2.1930E+00  1.0000E-04
 mraqcc  #  6  5    -93.5467927868  5.6672E-01  0.0000E+00  2.7628E+00  1.0000E-04
 mraqcc  #  6  6    -92.9717564002  2.1875E+01  0.0000E+00  3.4839E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.01     0.00     0.00     0.01         0.    0.0001
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0002
    3   26    0     0.00     0.00     0.00     0.00         0.    0.0009
    4   27    0     0.01     0.00     0.00     0.01         0.    0.0005
    5   11    0     0.00     0.00     0.00     0.00         0.    0.0003
    6   15    0     0.01     0.00     0.00     0.01         0.    0.0015
    7   16    0     0.00     0.00     0.00     0.00         0.    0.0014
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0001
    9    5    0     0.01     0.00     0.00     0.01         0.    0.0007
   10    6    0     0.00     0.00     0.00     0.00         0.    0.0007
   11   18    0     0.01     0.00     0.00     0.01         0.    0.0007
   12    7    0     0.00     0.00     0.00     0.00         0.    0.0006
   13   19    0     0.01     0.00     0.00     0.01         0.    0.0006
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0003
   15   43    0     0.01     0.00     0.00     0.01         0.    0.0003
   16   44    0     0.00     0.00     0.00     0.00         0.    0.0003
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0001
   18   76    0     0.01     0.00     0.01     0.00         0.    0.0006
   19   77    0     0.00     0.00     0.00     0.00         0.    0.0006
   20   78    0     0.00     0.00     0.00     0.00         0.    0.0001
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0800s 
time spent in multnx:                   0.0700s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.0900s 

          starting ci iteration   7

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.000286
ci vector #   2dasum_wr=    0.003017
ci vector #   3dasum_wr=    0.004769
ci vector #   4dasum_wr=    0.006491


====================================================================================================
Diagonal     counts:  0x:        1540 2x:         545 4x:         129
All internal counts: zz :          55 yy:         391 xx:         830 ww:         824
One-external counts: yz :         349 yx:        1619 yw:        1632
Two-external counts: yy :         470 ww:         852 xx:         968 xz:          68 wz:          93 wx:        1100
Three-ext.   counts: yx :         426 yw:         455

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         127    task #     2:         163    task #     3:         899    task #     4:         449
task #     5:         332    task #     6:        1463    task #     7:        1441    task #     8:          54
task #     9:         669    task #    10:         741    task #    11:         741    task #    12:         595
task #    13:         595    task #    14:         297    task #    15:         299    task #    16:         299
task #    17:          82    task #    18:         581    task #    19:         579    task #    20:         529

          calcsovref: tciref block   1

              civs   1       civs   2
 refs   1   0.978745       9.113073E-06

          calcsovref: scrb block   1

              civs   1       civs   2
 civs   1   0.999997      -7.503161E-03
 civs   2   0.871215        2498.82    
 eci,repnuc=   -29.23440031717470        -71.09689853878315     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  7  1   -100.3312988560  6.0068E-07  1.0337E-07  6.3083E-04  1.0000E-04
 mraqcc  #  7  2    -96.6063952330 -1.5828E+00  0.0000E+00  2.2222E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0001
    2   25    0     0.01     0.00     0.00     0.01         0.    0.0002
    3   26    0     0.00     0.00     0.00     0.00         0.    0.0009
    4   27    0     0.01     0.00     0.00     0.01         0.    0.0005
    5   11    0     0.00     0.00     0.00     0.00         0.    0.0003
    6   15    0     0.01     0.00     0.00     0.01         0.    0.0015
    7   16    0     0.01     0.00     0.00     0.01         0.    0.0014
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0001
    9    5    0     0.00     0.00     0.00     0.00         0.    0.0007
   10    6    0     0.01     0.00     0.00     0.01         0.    0.0007
   11   18    0     0.00     0.00     0.00     0.00         0.    0.0007
   12    7    0     0.01     0.00     0.00     0.01         0.    0.0006
   13   19    0     0.00     0.00     0.00     0.00         0.    0.0006
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0003
   15   43    0     0.00     0.00     0.00     0.00         0.    0.0003
   16   44    0     0.01     0.00     0.00     0.01         0.    0.0003
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0001
   18   76    0     0.00     0.00     0.00     0.00         0.    0.0006
   19   77    0     0.00     0.00     0.00     0.00         0.    0.0006
   20   78    0     0.01     0.00     0.00     0.01         0.    0.0001
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0800s 
time spent in multnx:                   0.0800s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.0800s 

          starting ci iteration   8

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.000284
ci vector #   2dasum_wr=    0.001113
ci vector #   3dasum_wr=    0.001613
ci vector #   4dasum_wr=    0.002450


====================================================================================================
Diagonal     counts:  0x:        1540 2x:         545 4x:         129
All internal counts: zz :          55 yy:         391 xx:         830 ww:         824
One-external counts: yz :         349 yx:        1619 yw:        1632
Two-external counts: yy :         470 ww:         852 xx:         968 xz:          68 wz:          93 wx:        1100
Three-ext.   counts: yx :         426 yw:         455

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         127    task #     2:         163    task #     3:         899    task #     4:         449
task #     5:         332    task #     6:        1463    task #     7:        1441    task #     8:          54
task #     9:         669    task #    10:         741    task #    11:         741    task #    12:         595
task #    13:         595    task #    14:         297    task #    15:         299    task #    16:         299
task #    17:          82    task #    18:         581    task #    19:         579    task #    20:         529

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3
 refs   1   0.978745       9.113073E-06   1.370361E-05

          calcsovref: scrb block   1

              civs   1       civs   2       civs   3
 civs   1   0.999981       6.905801E-02  -3.243197E-02
 civs   2    1.08484       -1634.54       -1934.42    
 civs   3    1.07403       -4400.72        2627.38    
 eci,repnuc=   -29.23440044164103        -71.09689853878315     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  8  1   -100.3312989804  1.2447E-07  1.5379E-08  2.9173E-04  1.0000E-04
 mraqcc  #  8  2    -98.1235328677  1.5171E+00  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  8  3    -96.0656119680  2.8333E-01  0.0000E+00  2.4816E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0001
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0002
    3   26    0     0.01     0.00     0.00     0.01         0.    0.0009
    4   27    0     0.01     0.00     0.00     0.01         0.    0.0005
    5   11    0     0.00     0.00     0.00     0.00         0.    0.0003
    6   15    0     0.00     0.00     0.00     0.00         0.    0.0015
    7   16    0     0.01     0.00     0.00     0.01         0.    0.0014
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0001
    9    5    0     0.01     0.00     0.00     0.01         0.    0.0007
   10    6    0     0.00     0.00     0.00     0.00         0.    0.0007
   11   18    0     0.01     0.00     0.00     0.01         0.    0.0007
   12    7    0     0.01     0.00     0.01     0.01         0.    0.0006
   13   19    0     0.00     0.00     0.00     0.00         0.    0.0006
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0003
   15   43    0     0.01     0.00     0.00     0.01         0.    0.0003
   16   44    0     0.00     0.00     0.00     0.00         0.    0.0003
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0001
   18   76    0     0.01     0.00     0.01     0.00         0.    0.0006
   19   77    0     0.00     0.00     0.00     0.00         0.    0.0006
   20   78    0     0.00     0.00     0.00     0.00         0.    0.0001
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0800s 
time spent in multnx:                   0.0700s 
integral transfer time:                 0.0200s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.0900s 

          starting ci iteration   9

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.000076
ci vector #   2dasum_wr=    0.000456
ci vector #   3dasum_wr=    0.000834
ci vector #   4dasum_wr=    0.001099


====================================================================================================
Diagonal     counts:  0x:        1540 2x:         545 4x:         129
All internal counts: zz :          55 yy:         391 xx:         830 ww:         824
One-external counts: yz :         349 yx:        1619 yw:        1632
Two-external counts: yy :         470 ww:         852 xx:         968 xz:          68 wz:          93 wx:        1100
Three-ext.   counts: yx :         426 yw:         455

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         127    task #     2:         163    task #     3:         899    task #     4:         449
task #     5:         332    task #     6:        1463    task #     7:        1441    task #     8:          54
task #     9:         669    task #    10:         741    task #    11:         741    task #    12:         595
task #    13:         595    task #    14:         297    task #    15:         299    task #    16:         299
task #    17:          82    task #    18:         581    task #    19:         579    task #    20:         529

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4
 refs   1   0.978745       9.113073E-06   1.370361E-05  -4.518571E-06

          calcsovref: scrb block   1

              civs   1       civs   2       civs   3       civs   4
 civs   1   0.999981      -4.665681E-02  -5.649181E-02   3.970782E-02
 civs   2    1.11461        1336.14       -1198.24       -1873.29    
 civs   3    1.22371        3892.12        3307.98       -434.999    
 civs   4    1.00607        6467.19       -5456.80        12682.4    
 eci,repnuc=   -29.23440045989903        -71.09689853878315     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  9  1   -100.3312989987  1.8258E-08  2.0975E-09  1.0963E-04  1.0000E-04
 mraqcc  #  9  2    -98.6773261985  5.5379E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  9  3    -96.1871608996  1.2155E-01  0.0000E+00  2.3542E+00  1.0000E-04
 mraqcc  #  9  4    -95.4493352144  3.4692E-01  0.0000E+00  2.7798E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0001
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0002
    3   26    0     0.00     0.00     0.00     0.00         0.    0.0009
    4   27    0     0.01     0.00     0.00     0.01         0.    0.0005
    5   11    0     0.00     0.00     0.00     0.00         0.    0.0003
    6   15    0     0.00     0.00     0.00     0.00         0.    0.0015
    7   16    0     0.02     0.01     0.01     0.02         0.    0.0014
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0001
    9    5    0     0.00     0.00     0.00     0.00         0.    0.0007
   10    6    0     0.01     0.00     0.00     0.01         0.    0.0007
   11   18    0     0.00     0.00     0.00     0.00         0.    0.0007
   12    7    0     0.01     0.00     0.00     0.01         0.    0.0006
   13   19    0     0.00     0.00     0.00     0.00         0.    0.0006
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0003
   15   43    0     0.01     0.00     0.01     0.01         0.    0.0003
   16   44    0     0.01     0.00     0.00     0.00         0.    0.0003
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0001
   18   76    0     0.00     0.00     0.00     0.00         0.    0.0006
   19   77    0     0.00     0.00     0.00     0.00         0.    0.0006
   20   78    0     0.00     0.00     0.00     0.00         0.    0.0001
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0700s 
time spent in multnx:                   0.0600s 
integral transfer time:                 0.0200s 
time spent for loop construction:       0.0100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.0800s 

          starting ci iteration  10

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.000018
ci vector #   2dasum_wr=    0.000175
ci vector #   3dasum_wr=    0.000326
ci vector #   4dasum_wr=    0.000442


====================================================================================================
Diagonal     counts:  0x:        1540 2x:         545 4x:         129
All internal counts: zz :          55 yy:         391 xx:         830 ww:         824
One-external counts: yz :         349 yx:        1619 yw:        1632
Two-external counts: yy :         470 ww:         852 xx:         968 xz:          68 wz:          93 wx:        1100
Three-ext.   counts: yx :         426 yw:         455

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         127    task #     2:         163    task #     3:         899    task #     4:         449
task #     5:         332    task #     6:        1463    task #     7:        1441    task #     8:          54
task #     9:         669    task #    10:         741    task #    11:         741    task #    12:         595
task #    13:         595    task #    14:         297    task #    15:         299    task #    16:         299
task #    17:          82    task #    18:         581    task #    19:         579    task #    20:         529

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1   0.978745       9.113073E-06   1.370361E-05  -4.518571E-06   3.118858E-06

          calcsovref: scrb block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 civs   1   0.999978       7.910430E-02  -5.900275E-02  -7.557273E-02   0.100700    
 civs   2    1.11866       -1164.37       -1231.80       -1590.03       -1205.96    
 civs   3    1.24408       -3519.01        3252.07       -1943.14        659.774    
 civs   4    1.14303       -7072.85       -5343.29        7570.62        10086.2    
 civs   5    1.00417       -11223.9        1038.60        35392.7       -23918.8    
 eci,repnuc=   -29.23440046241392        -71.09689853878315     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  # 10  1   -100.3312990012  2.5149E-09  2.4922E-10  3.7319E-05  1.0000E-04
 mraqcc  # 10  2    -98.8843552398  2.0703E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 10  3    -96.1874253832  2.6448E-04  0.0000E+00  2.3750E+00  1.0000E-04
 mraqcc  # 10  4    -95.8288553062  3.7952E-01  0.0000E+00  2.2138E+00  1.0000E-04
 mraqcc  # 10  5    -95.2779804515  1.7312E+00  0.0000E+00  2.4860E+00  1.0000E-04

======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0001
    2   25    0     0.01     0.00     0.00     0.00         0.    0.0002
    3   26    0     0.00     0.00     0.00     0.00         0.    0.0009
    4   27    0     0.00     0.00     0.00     0.00         0.    0.0005
    5   11    0     0.00     0.00     0.00     0.00         0.    0.0003
    6   15    0     0.01     0.00     0.00     0.01         0.    0.0015
    7   16    0     0.01     0.01     0.00     0.01         0.    0.0014
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0001
    9    5    0     0.00     0.00     0.00     0.00         0.    0.0007
   10    6    0     0.01     0.00     0.00     0.01         0.    0.0007
   11   18    0     0.01     0.00     0.00     0.01         0.    0.0007
   12    7    0     0.00     0.00     0.00     0.00         0.    0.0006
   13   19    0     0.01     0.00     0.00     0.01         0.    0.0006
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0003
   15   43    0     0.00     0.00     0.00     0.00         0.    0.0003
   16   44    0     0.01     0.00     0.00     0.01         0.    0.0003
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0001
   18   76    0     0.00     0.00     0.00     0.00         0.    0.0006
   19   77    0     0.00     0.00     0.00     0.00         0.    0.0006
   20   78    0     0.00     0.00     0.00     0.00         0.    0.0001
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0700s 
time spent in multnx:                   0.0600s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.0800s 

 mraqcc   convergence criteria satisfied after 10 iterations.

 final mraqcc   convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  # 10  1   -100.3312990012  2.5149E-09  2.4922E-10  3.7319E-05  1.0000E-04
 mraqcc  # 10  2    -98.8843552398  2.0703E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 10  3    -96.1874253832  2.6448E-04  0.0000E+00  2.3750E+00  1.0000E-04
 mraqcc  # 10  4    -95.8288553062  3.7952E-01  0.0000E+00  2.2138E+00  1.0000E-04
 mraqcc  # 10  5    -95.2779804515  1.7312E+00  0.0000E+00  2.4860E+00  1.0000E-04

####################CIUDGINFO####################

   aqcc vector at position   1 energy= -100.331299001197

################END OF CIUDGINFO################


 a4den factor =  1.022895637 for root   1
    1 of the   6 expansion vectors are transformed.
    1 of the   5 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
 maximum overlap with reference                         1 (overlap= 
   0.9787480598185176      )
 reference energy=   -100.0823305448912     
 total aqcc energy=   -100.3312990011971     
 diagonal element shift (lrtshift) =   -0.2489684563058923     

information on vector: 1from unit 11 written to unit 48filename civout              


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =      -100.3312990012

                                                       internal orbitals

                                          level       1    2    3    4    5    6

                                          orbital     1    2    3    4   21   35

                                         symmetry   a1   a1   a1   a1   b1   b2 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1  0.012690                        +-   +-   +-   +-   +-      
 z*  1  1       2  0.012690                        +-   +-   +-   +-        +- 
 z*  1  1       3 -0.975208                        +-   +-   +-        +-   +- 
 z*  1  1       5  0.081889                        +-   +-        +-   +-   +- 
 z*  1  1       8  0.011038                        +-        +-   +-   +-   +- 
 y   1  1      67  0.011637              1( b2 )   +-   +-   +-        +-    - 
 y   1  1      76 -0.011637              1( b1 )   +-   +-   +-         -   +- 
 y   1  1     121  0.016241              1( b2 )   +-   +-    -   +    +-    - 
 y   1  1     130 -0.016241              1( b1 )   +-   +-    -   +     -   +- 
 y   1  1     159  0.049625              1( b2 )   +-   +-   +     -   +-    - 
 y   1  1     160 -0.029930              2( b2 )   +-   +-   +     -   +-    - 
 y   1  1     163 -0.011511              5( b2 )   +-   +-   +     -   +-    - 
 y   1  1     164  0.012758              6( b2 )   +-   +-   +     -   +-    - 
 y   1  1     168 -0.049625              1( b1 )   +-   +-   +     -    -   +- 
 y   1  1     169  0.029930              2( b1 )   +-   +-   +     -    -   +- 
 y   1  1     172  0.011511              5( b1 )   +-   +-   +     -    -   +- 
 y   1  1     173 -0.012758              6( b1 )   +-   +-   +     -    -   +- 
 y   1  1     300  0.014832              2( a1 )   +-    -   +     -   +-   +- 
 y   1  1     301  0.016665              3( a1 )   +-    -   +     -   +-   +- 
 y   1  1     302 -0.019909              4( a1 )   +-    -   +     -   +-   +- 
 y   1  1     337  0.015573              3( b2 )   +-   +    +-    -   +-    - 
 y   1  1     346 -0.015573              3( b1 )   +-   +    +-    -    -   +- 
 y   1  1     374 -0.015125              4( a1 )   +-   +     -    -   +-   +- 
 y   1  1     377 -0.011624              7( a1 )   +-   +     -    -   +-   +- 
 x   1  1     801  0.030754    5( a1 )   1( a2 )   +-   +-   +-         -    - 
 x   1  1     861  0.021461    1( b1 )   1( b2 )   +-   +-   +-         -    - 
 x   1  1     862 -0.014594    2( b1 )   1( b2 )   +-   +-   +-         -    - 
 x   1  1     870 -0.014594    1( b1 )   2( b2 )   +-   +-   +-         -    - 
 x   1  1     871  0.022720    2( b1 )   2( b2 )   +-   +-   +-         -    - 
 x   1  1     881  0.012086    3( b1 )   3( b2 )   +-   +-   +-         -    - 
 x   1  1    1845  0.011479    3( b1 )   1( a2 )   +-   +-    -        +-    - 
 x   1  1    1896 -0.015241    2( a1 )   2( b2 )   +-   +-    -        +-    - 
 x   1  1    1915  0.011478    5( a1 )   3( b2 )   +-   +-    -        +-    - 
 x   1  1    1917  0.011326    7( a1 )   3( b2 )   +-   +-    -        +-    - 
 x   1  1    2040  0.015241    2( a1 )   2( b1 )   +-   +-    -         -   +- 
 x   1  1    2059  0.011478    5( a1 )   3( b1 )   +-   +-    -         -   +- 
 x   1  1    2061 -0.011326    7( a1 )   3( b1 )   +-   +-    -         -   +- 
 x   1  1    2175  0.011479    1( a2 )   3( b2 )   +-   +-    -         -   +- 
 x   1  1    3793  0.011182    4( a1 )   1( b2 )   +-    -   +-        +-    - 
 x   1  1    3809 -0.010284    4( a1 )   2( b2 )   +-    -   +-        +-    - 
 x   1  1    3937 -0.011182    4( a1 )   1( b1 )   +-    -   +-         -   +- 
 x   1  1    3953  0.010284    4( a1 )   2( b1 )   +-    -   +-         -   +- 
 w   1  1    8454  0.016134    5( a1 )   5( a1 )   +-   +-   +-        +-      
 w   1  1    8621  0.016134    1( a2 )   1( a2 )   +-   +-   +-        +-      
 w   1  1    8631  0.022938    1( b2 )   1( b2 )   +-   +-   +-        +-      
 w   1  1    8632 -0.023850    1( b2 )   2( b2 )   +-   +-   +-        +-      
 w   1  1    8633  0.024884    2( b2 )   2( b2 )   +-   +-   +-        +-      
 w   1  1    8636  0.013376    3( b2 )   3( b2 )   +-   +-   +-        +-      
 w   1  1    8647 -0.010313    2( b2 )   6( b2 )   +-   +-   +-        +-      
 w   1  1    8651  0.010274    6( b2 )   6( b2 )   +-   +-   +-        +-      
 w   1  1    8682  0.010049    7( a1 )   1( a2 )   +-   +-   +-        +     - 
 w   1  1    8740 -0.020672    1( b1 )   1( b2 )   +-   +-   +-        +     - 
 w   1  1    8741  0.014863    2( b1 )   1( b2 )   +-   +-   +-        +     - 
 w   1  1    8749  0.014863    1( b1 )   2( b2 )   +-   +-   +-        +     - 
 w   1  1    8750 -0.022259    2( b1 )   2( b2 )   +-   +-   +-        +     - 
 w   1  1    8760 -0.011713    3( b1 )   3( b2 )   +-   +-   +-        +     - 
 w   1  1    8835  0.016134    5( a1 )   5( a1 )   +-   +-   +-             +- 
 w   1  1    8957  0.022938    1( b1 )   1( b1 )   +-   +-   +-             +- 
 w   1  1    8958 -0.023850    1( b1 )   2( b1 )   +-   +-   +-             +- 
 w   1  1    8959  0.024884    2( b1 )   2( b1 )   +-   +-   +-             +- 
 w   1  1    8962  0.013376    3( b1 )   3( b1 )   +-   +-   +-             +- 
 w   1  1    8973 -0.010313    2( b1 )   6( b1 )   +-   +-   +-             +- 
 w   1  1    8977  0.010274    6( b1 )   6( b1 )   +-   +-   +-             +- 
 w   1  1    9002  0.016134    1( a2 )   1( a2 )   +-   +-   +-             +- 
 w   1  1   10036 -0.011205    3( b1 )   1( a2 )   +-   +-   +         +-    - 
 w   1  1   10087  0.010605    2( a1 )   2( b2 )   +-   +-   +         +-    - 
 w   1  1   10106  0.011205    5( a1 )   3( b2 )   +-   +-   +         +-    - 
 w   1  1   10231 -0.010605    2( a1 )   2( b1 )   +-   +-   +          -   +- 
 w   1  1   10250  0.011205    5( a1 )   3( b1 )   +-   +-   +          -   +- 
 w   1  1   10366  0.011205    1( a2 )   3( b2 )   +-   +-   +          -   +- 
 w   1  1   11373  0.011130    2( a1 )   2( a1 )   +-   +-             +-   +- 
 w   1  1   11391  0.010903    6( a1 )   6( a1 )   +-   +-             +-   +- 
 w   1  1   11512  0.010417    3( b1 )   3( b1 )   +-   +-             +-   +- 
 w   1  1   11567  0.010417    3( b2 )   3( b2 )   +-   +-             +-   +- 
 w   1  1   12621  0.010195    2( a1 )   1( b2 )   +-   +    +-        +-    - 
 w   1  1   12623 -0.013854    4( a1 )   1( b2 )   +-   +    +-        +-    - 
 w   1  1   12637 -0.011639    2( a1 )   2( b2 )   +-   +    +-        +-    - 
 w   1  1   12639  0.018171    4( a1 )   2( b2 )   +-   +    +-        +-    - 
 w   1  1   12703 -0.010568    4( a1 )   6( b2 )   +-   +    +-        +-    - 
 w   1  1   12765 -0.010195    2( a1 )   1( b1 )   +-   +    +-         -   +- 
 w   1  1   12767  0.013854    4( a1 )   1( b1 )   +-   +    +-         -   +- 
 w   1  1   12781  0.011639    2( a1 )   2( b1 )   +-   +    +-         -   +- 
 w   1  1   12783 -0.018171    4( a1 )   2( b1 )   +-   +    +-         -   +- 
 w   1  1   12847  0.010568    4( a1 )   6( b1 )   +-   +    +-         -   +- 
 w   1  1   13928  0.010902    2( a1 )   4( a1 )   +-   +     -        +-   +- 
 w   1  1   16099  0.012507    4( a1 )   4( a1 )   +-        +-        +-   +- 

 ci coefficient statistics:
           rq > 0.1                1
      0.1> rq > 0.01              85
     0.01> rq > 0.001           1632
    0.001> rq > 0.0001          4096
   0.0001> rq > 0.00001         7006
  0.00001> rq > 0.000001        2103
 0.000001> rq                   2234
           all                 17157
  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.012689999955
     2     2      0.012689999955
     3     3     -0.975208359646
     4     4     -0.006031537481
     5     5      0.081888732923
     6     6     -0.000472424176
     7     7     -0.000695066107
     8     8      0.011037833461

 number of reference csfs (nref) is     8.  root number (iroot) is  1.
 c0**2 =   0.95821810  c**2 (all zwalks) =   0.95821810
 passed aftci ... 
 readint2: molcas,dalton2=                        1                        0
 files%faoints=aoints              
molcasbasis:  20  10   4  10

 Integral file header information:
 Using SEWARD integrals


   ******  Basis set information:  ******

 Number of irreps:                  4
 Total number of basis functions:  44

 irrep no.              1    2    3    4
 irrep label           a1   b1   a2   b2 
 no. of bas.fcions.    20   10    4   10
 input basis function labels, i:bfnlab(i)=
H   1s     H   *s     H   *s     H   *pz    H   *pz    H   *d0    H   *d2+   F   1s     F   2s     F   *s  
F   *s     F   2pz    F   *pz    F   *pz    F   *d0    F   *d0    F   *d2+   F   *d2+   F   *f0    F   *f2+
H   *px    H   *px    H   *d1+   F   2px    F   *px    F   *px    F   *d1+   F   *d1+   F   *f1+   F   *f3+
H   *d2-   F   *d2-   F   *d2-   F   *f2-   H   *py    H   *py    H   *d1-   F   2py    F   *py    F   *py 
F   *d1-   F   *d1-   F   *f3-   F   *f1-
 map-vector 1 , imtype=                        3
   1   1   1   1   1   1   1   2   2   2   2   2   2   2   2   2   2   2   2   2
  1   1   1   2   2   2   2   2   2   2   1   2   2   2   1   1   1   2   2   2
  2   2   2   2
 map-vector 2 , imtype=                        4
   1   1   1   2   2   4   4   1   1   1   1   2   2   2   4   4   4   4   6   6
  2   2   4   2   2   2   4   4   6   6   4   4   4   6   2   2   4   2   2   2
  4   4   6   6
 <<< Entering RdOne >>>
 rc on entry:            0
 Label on entry:  MLTPL  0
 Comp on entry:          1
 SymLab on entry:        1
 Option on entry:        6
 picked LabelMLTPL  0 Component                        1 currop= 
                        1
 Computed Symlab=                        1
  I will close the file for you!

          overlap matrix SAO basis block   1

               sao   1        sao   2        sao   3        sao   4        sao   5        sao   6        sao   7        sao   8
  sao   1     1.00000000
  sao   2     0.97447731     1.00000000
  sao   3     0.86984181     0.78887809     1.00000000
  sao   4     0.00000000     0.00000000     0.00000000     1.00000000
  sao   5     0.00000000     0.00000000     0.00000000     0.61492356     1.00000000
  sao   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
  sao   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
  sao   8     0.05911140     0.06743507     0.05561031     0.04104207     0.13108980     0.12746029     0.00000000     1.00000000
  sao   9     0.49084273     0.49592760     0.47131392     0.28485591     0.58719620     0.27993617     0.00000000     0.00000029
  sao  10     0.21122260     0.23217693     0.19467288     0.18392315     0.40778364     0.35651489     0.00000000     0.55834817
  sao  11     0.64136164     0.62605044     0.64465485     0.23110630     0.56227706     0.13648451     0.00000000     0.16285663
  sao  12    -0.37254426    -0.42058338    -0.18140325    -0.31000965    -0.13871174    -0.16898404     0.00000000     0.00000000
  sao  13    -0.31114088    -0.35063667    -0.11851283    -0.41707795    -0.22046505    -0.20036660     0.00000000     0.00000000
  sao  14    -0.56255659    -0.62833307    -0.33645097    -0.07884573     0.03561921     0.09260922     0.00000000     0.00000000
  sao  15     0.04234456     0.03588607     0.00339274     0.19083866     0.00219189     0.12247687     0.00000000     0.00000000
  sao  16     0.20723129     0.18729841     0.02501434     0.25343197    -0.08891297    -0.02767215     0.00000000     0.00000000
  sao  17     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.05903711     0.00000000
  sao  18     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.24067146     0.00000000
  sao  19    -0.05225636    -0.02226505    -0.00080503    -0.16729315     0.03349404    -0.03157112     0.00000000     0.00000000
  sao  20     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.19224851     0.00000000

               sao   9        sao  10        sao  11        sao  12        sao  13        sao  14        sao  15        sao  16
  sao   9     1.00000000
  sao  10     0.66714794     1.00000000
  sao  11     0.89221651     0.52037470     1.00000000
  sao  12     0.00000000     0.00000000     0.00000000     1.00000000
  sao  13     0.00000000     0.00000000     0.00000000     0.93861578     1.00000000
  sao  14     0.00000000     0.00000000     0.00000000     0.72541428     0.64084785     1.00000000
  sao  15     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
  sao  16     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.50517544     1.00000000
  sao  17     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
  sao  19     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
  sao  20     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

               sao  17        sao  18        sao  19        sao  20
  sao  17     1.00000000
  sao  18     0.50517544     1.00000000
  sao  19     0.00000000     0.00000000     1.00000000
  sao  20     0.00000000     0.00000000     0.00000000     1.00000000

          overlap matrix SAO basis block   2

               sao  21        sao  22        sao  23        sao  24        sao  25        sao  26        sao  27        sao  28
  sao  21     1.00000000
  sao  22     0.61492356     1.00000000
  sao  23     0.00000000     0.00000000     1.00000000
  sao  24     0.18446324     0.41505806     0.32427560     1.00000000
  sao  25     0.18201399     0.35646190     0.38055058     0.93861578     1.00000000
  sao  26     0.23558499     0.59841627     0.21903729     0.72541428     0.64084785     1.00000000
  sao  27    -0.08800157    -0.07574290    -0.21771107     0.00000000     0.00000000     0.00000000     1.00000000
  sao  28    -0.37809183    -0.37224003    -0.43546125     0.00000000     0.00000000     0.00000000     0.50517544     1.00000000
  sao  29     0.22479575     0.07582204     0.24920595     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
  sao  30     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

               sao  29        sao  30
  sao  29     1.00000000
  sao  30     0.00000000     1.00000000

          overlap matrix SAO basis block   3

               sao  31        sao  32        sao  33        sao  34
  sao  31     1.00000000
  sao  32     0.05903711     1.00000000
  sao  33     0.24067146     0.50517544     1.00000000
  sao  34    -0.19224851     0.00000000     0.00000000     1.00000000

          overlap matrix SAO basis block   4

               sao  35        sao  36        sao  37        sao  38        sao  39        sao  40        sao  41        sao  42
  sao  35     1.00000000
  sao  36     0.61492356     1.00000000
  sao  37     0.00000000     0.00000000     1.00000000
  sao  38     0.18446324     0.41505806     0.32427560     1.00000000
  sao  39     0.18201399     0.35646190     0.38055058     0.93861578     1.00000000
  sao  40     0.23558499     0.59841627     0.21903729     0.72541428     0.64084785     1.00000000
  sao  41    -0.08800157    -0.07574290    -0.21771107     0.00000000     0.00000000     0.00000000     1.00000000
  sao  42    -0.37809183    -0.37224003    -0.43546125     0.00000000     0.00000000     0.00000000     0.50517544     1.00000000
  sao  43     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
  sao  44     0.22479575     0.07582204     0.24920595     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

               sao  43        sao  44
  sao  43     1.00000000
  sao  44     0.00000000     1.00000000
NO coefficients and occupation numbers are written to nocoef_ci.1

################################################################################
 one electron density matrix calculation 
################################################################################


 workspace allocation information: mxcore=       32766638
 the acpf density matrix will be calculated
 rdcirefv: extracting reference vector # 1(blocksize=     8 #zcsf=       8)
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
 1-DENSITY: executing task                         1                        1
 1-DENSITY: executing task                        71                        1
================================================================================
   DYZ=       0  DYX=       0  DYW=       0
   D0Z=       9  D0Y=       0  D0X=       0  D0W=       0
  DDZI=      35 DDYI=       0 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
 Trace of MO density:     10.00000000000001     
                       10 correlated and                         0 
 frozen core electrons
 root #                         1 : Scaling(2) with     1.022895636878909      
 corresponding to ref #                         1
 computing final density
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
 1-DENSITY: executing task                        11                        1
 1-DENSITY: executing task                        13                        3
 1-DENSITY: executing task                        14                        4
 1-DENSITY: executing task                         1                        1
 1-DENSITY: executing task                         2                        2
 1-DENSITY: executing task                         3                        3
 1-DENSITY: executing task                         4                        4
 1-DENSITY: executing task                        71                        1
 1-DENSITY: executing task                        52                        2
 1-DENSITY: executing task                        72                        2
 1-DENSITY: executing task                        53                        3
 1-DENSITY: executing task                        73                        3
 1-DENSITY: executing task                        54                        4
 1-DENSITY: executing task                        74                        4
================================================================================
   DYZ=      43  DYX=     167  DYW=     172
   D0Z=       9  D0Y=      68  D0X=      75  D0W=      74
  DDZI=      35 DDYI=     180 DDXI=     180 DDWI=     185
  DDZE=       0 DDYE=      40 DDXE=      42 DDWE=      47
================================================================================
 Trace of MO density:     10.00000000000001     
                       10 correlated and                         0 
 frozen core electrons


*****   symmetry block  a1    *****

               no    1        no    2        no    3        no    4        no    5        no    6        no    7        no    8

      occ    2.00000        1.98762        1.96490       2.715697E-02   8.116575E-03   5.009426E-03   4.615859E-03   1.139748E-03

  mo    1    1.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  mo    2    0.00000       0.987463       0.157822       2.376421E-04   1.334084E-03  -1.079279E-03    0.00000      -4.769094E-04
  mo    3    0.00000      -0.157823       0.987445      -5.680074E-03   2.836414E-04  -1.674054E-03    0.00000      -1.052270E-03
  mo    4    0.00000      -1.056144E-03   5.596273E-03   0.993641      -7.473570E-02  -3.331560E-02    0.00000      -5.471053E-02
  mo    5    0.00000       1.436507E-03  -1.651526E-03  -4.032380E-03  -0.217661      -0.239678        0.00000       1.308034E-02
  mo    6    0.00000       1.812406E-03   1.948503E-03  -5.509708E-02  -0.506291       0.465490        0.00000      -0.305407    
  mo    7    0.00000       9.297501E-04   1.181066E-03  -1.779174E-02  -0.353525      -8.220665E-02    0.00000       0.499685    
  mo    8    0.00000      -1.055644E-04   1.038388E-03   6.323085E-02   0.677298       0.412471        0.00000       4.045442E-02
  mo    9    0.00000        0.00000        0.00000        0.00000        0.00000        0.00000       0.928923        0.00000    
  mo   10    0.00000      -2.680045E-04  -7.229795E-04   8.695170E-03   8.610760E-02  -0.496722        0.00000       5.619805E-02
  mo   11    0.00000       4.025117E-04  -1.056996E-04   4.994154E-02  -0.152202       0.473858        0.00000       0.280475    
  mo   12    0.00000        0.00000        0.00000        0.00000        0.00000        0.00000       0.151897        0.00000    
  mo   13    0.00000       1.449417E-04   8.173622E-04  -2.399484E-03   6.244938E-02  -9.395714E-02    0.00000       0.247660    
  mo   14    0.00000      -1.185866E-04   1.203491E-03   2.424001E-02  -6.404150E-02   8.235514E-02    0.00000       0.573188    
  mo   15    0.00000       8.235922E-04   1.357095E-03   1.264751E-02   0.200680      -7.479450E-02    0.00000       0.289528    
  mo   16    0.00000        0.00000        0.00000        0.00000        0.00000        0.00000      -5.224524E-02    0.00000    
  mo   17    0.00000       7.340862E-04   7.813976E-04   3.472484E-02   0.117799       0.140699        0.00000       0.154782    
  mo   18    0.00000        0.00000        0.00000        0.00000        0.00000        0.00000       0.333615        0.00000    
  mo   19    0.00000       1.704022E-04   2.037318E-04   2.584213E-02  -2.497826E-02   0.188063        0.00000       0.218997    
  mo   20    0.00000       8.488611E-04  -1.559941E-04  -9.037698E-03   0.116788       1.504455E-02    0.00000      -0.157453    

               no    9        no   10        no   11        no   12        no   13        no   14        no   15        no   16

      occ   7.791041E-04   3.705471E-04   3.624790E-04   3.062037E-04   2.288001E-04   1.337775E-04   8.470146E-05   4.872724E-05

  mo    2   4.257547E-04    0.00000      -1.243831E-03    0.00000      -1.978201E-03   4.081817E-04  -3.980271E-04    0.00000    
  mo    3   2.425439E-03    0.00000      -1.134911E-03    0.00000      -1.163970E-04  -3.459347E-04   2.549565E-04    0.00000    
  mo    4  -4.776445E-02    0.00000       1.943259E-02    0.00000      -3.011112E-03   1.096153E-02   1.255194E-02    0.00000    
  mo    5   0.318376        0.00000      -1.113225E-02    0.00000       0.386149      -0.115899      -3.733978E-02    0.00000    
  mo    6  -0.257515        0.00000       0.358933        0.00000       0.180246      -4.493984E-02  -0.202429        0.00000    
  mo    7  -0.200632        0.00000       3.623546E-02    0.00000      -6.194991E-02  -0.255613       0.565334        0.00000    
  mo    8  -6.288603E-02    0.00000       9.160054E-02    0.00000      -9.174932E-02  -0.232520       0.105908        0.00000    
  mo    9    0.00000      -0.245419        0.00000      -0.262597        0.00000        0.00000        0.00000      -8.895712E-02
  mo   10   0.303277        0.00000       0.222406        0.00000      -2.091466E-02  -0.294176      -0.194591        0.00000    
  mo   11   0.536405        0.00000       4.775708E-02    0.00000      -0.101398      -0.430243      -4.113162E-02    0.00000    
  mo   12    0.00000       0.405667        0.00000      -0.143248        0.00000        0.00000        0.00000       0.889855    
  mo   13  -0.472257        0.00000      -0.126018        0.00000       0.131728      -8.751207E-02  -6.652835E-02    0.00000    
  mo   14  -0.186444        0.00000      -1.765400E-02    0.00000      -0.134737       0.176683      -0.306118        0.00000    
  mo   15  -3.211030E-02    0.00000       0.752347        0.00000       0.290313       0.158088      -0.160973        0.00000    
  mo   16    0.00000       0.644723        0.00000      -0.655094        0.00000        0.00000        0.00000      -0.390454    
  mo   17  -1.955714E-02    0.00000      -0.425125        0.00000       0.733596      -0.135509      -0.190688        0.00000    
  mo   18    0.00000       0.599613        0.00000       0.693812        0.00000        0.00000        0.00000      -0.218610    
  mo   19   0.383058        0.00000      -7.134012E-02    0.00000       8.236055E-02   0.714855       0.189646        0.00000    
  mo   20  -1.362206E-02    0.00000       0.202916        0.00000       0.361494      -1.130716E-02   0.625076        0.00000    

               no   17        no   18        no   19        no   20

      occ   3.443027E-05   1.162147E-05   8.161034E-06   3.797420E-06

  mo    2  -1.183882E-04  -3.892073E-04  -6.838430E-04   6.751523E-05
  mo    3  -1.171382E-03   3.339232E-04   2.060466E-04   2.788340E-04
  mo    4   2.148048E-03  -2.006038E-03   4.781551E-04   1.259019E-03
  mo    5  -0.423928       0.315451       0.589430       5.392559E-02
  mo    6   0.276939       0.238830       9.803539E-02  -0.129205    
  mo    7   0.278743      -0.194978       0.267321      -2.720612E-02
  mo    8   0.122633       0.226039       0.450647      -0.101187    
  mo   10   0.530834       0.329011      -0.159158      -0.246922    
  mo   11  -0.152722       2.782405E-02  -0.325059       0.232976    
  mo   13   1.520869E-03   0.534393      -0.212863       0.571575    
  mo   14  -0.302365       0.208061      -7.406274E-02  -0.584493    
  mo   15  -7.924254E-02  -0.331235      -9.596822E-04   0.227825    
  mo   17   0.248489      -0.277905      -7.823575E-02  -0.148533    
  mo   19   0.350293       0.260627       5.371423E-02   0.127571    
  mo   20  -0.252152       0.248144      -0.421450      -0.309190    

               no    1        no    2        no    3        no    4        no    5        no    6        no    7        no    8

      occ    2.00000        1.98762        1.96490       2.715697E-02   8.116575E-03   5.009426E-03   4.615859E-03   1.139748E-03

  ao    1   2.656011E-03   0.471490      -0.616332      -8.428354E-02   6.536619E-02   0.789107       2.692603E-16   -1.68073    
  ao    2  -7.244295E-04  -0.143553       0.105237       0.161047        1.07976       0.244761       3.511017E-16  -0.194559    
  ao    3  -1.536984E-03  -0.171925       0.223908        1.40351       7.527666E-02  -0.162963      -9.886536E-17   0.239201    
  ao    4   6.668593E-04   1.754355E-02  -1.460151E-02  -1.834875E-02  -8.030590E-02   7.029045E-02   1.467072E-16   0.128711    
  ao    5   2.803823E-04   1.801195E-02  -1.556994E-02   0.116241       0.564476       0.490197       1.913339E-16  -0.496591    
  ao    6   1.548623E-04   3.311946E-03  -2.600644E-03   1.454311E-02   5.191408E-03   5.637026E-02   6.721429E-17   8.347198E-03
  ao    7   4.433440E-19   4.746970E-20  -7.245014E-20  -2.207396E-19   7.748377E-18   2.031534E-18   2.932450E-02  -3.715420E-18
  ao    8   0.999444      -7.542618E-03  -2.158273E-03   4.683015E-02  -6.155171E-02  -6.335558E-02   5.081156E-17  -3.372369E-02
  ao    9  -1.173097E-03   0.876888       0.371026      -1.693854E-02   0.939536      -7.978593E-02   6.507074E-17  -0.675479    
  ao   10   6.796289E-04   8.791911E-05   5.918524E-03  -1.520610E-02   0.566223       0.147134      -5.314042E-17  -0.281966    
  ao   11   4.268120E-04  -3.107671E-02   0.117508      -0.848360       -2.49610      -0.904183      -5.181072E-16    2.14646    
  ao   12  -3.300168E-03  -0.214618       0.796997       0.349998       0.924367      -0.344691      -8.273526E-17    2.12361    
  ao   13   2.863689E-03  -8.577145E-03  -8.714191E-03  -1.562825E-02  -0.195810       0.357157       2.236121E-16   -1.62156    
  ao   14   9.269381E-04   2.688421E-02  -3.101962E-02   0.120747      -0.201326       0.798356       2.552422E-16   -1.74931    
  ao   15  -7.957979E-06   4.337536E-03  -6.617566E-03   4.752107E-02  -2.881723E-02   0.304808       2.051885E-17   0.396552    
  ao   16  -5.320522E-04   1.098848E-02  -1.919658E-02   3.004132E-02  -2.578442E-02   0.582373      -1.600113E-16   0.178230    
  ao   17  -2.487475E-21  -2.117742E-19   6.669449E-19   1.607681E-19   3.727838E-19   2.590608E-20   0.382239      -9.971995E-19
  ao   18  -1.353278E-21  -1.538119E-19   4.748201E-19   8.530584E-20   2.528173E-19  -7.747883E-20   0.743816      -6.373614E-19
  ao   19   1.497144E-04  -2.263834E-03   3.403077E-03  -1.051534E-02  -2.989796E-03  -3.716902E-02   3.076629E-17   2.313515E-02
  ao   20   1.063864E-31  -3.570494E-31  -1.307846E-31   8.452305E-33  -1.323390E-30  -7.602724E-31  -2.124099E-02   9.315885E-31

               no    9        no   10        no   11        no   12        no   13        no   14        no   15        no   16

      occ   7.791041E-04   3.705471E-04   3.624790E-04   3.062037E-04   2.288001E-04   1.337775E-04   8.470146E-05   4.872724E-05

  ao    1   -3.77301      -1.269017E-15   -1.33588       1.257618E-15   -1.06885        4.98849        4.52759       7.192274E-16
  ao    2    2.10581      -3.671545E-15    1.51200       3.446524E-15   -1.09059       -3.62689       -2.10216       1.964151E-15
  ao    3    1.41223       4.492196E-16   0.524653      -3.847291E-16   0.598507       -1.78401       -1.93267      -2.552992E-16
  ao    4  -0.185745      -6.970093E-16  -0.296962       5.971701E-16  -0.397762       0.131340       0.720023       3.943107E-16
  ao    5   0.144457      -2.154491E-15   0.395344       2.017582E-15  -0.468290      -0.798091       0.732599       1.148578E-15
  ao    6   2.281915E-02  -6.548130E-16  -7.677649E-02   5.448090E-16  -0.304575       9.735169E-02   0.230442       3.512450E-16
  ao    7  -7.481594E-19  -0.230392       5.690422E-18   8.029796E-03   3.344503E-17  -2.814032E-18   2.613043E-17   -1.02824    
  ao    8   4.864124E-03   4.328565E-16  -0.357988      -3.861675E-17   -1.63015       0.188067       -1.70607      -2.049992E-16
  ao    9   0.264804       1.105090E-15  -0.723865      -2.518080E-16   -3.25526       0.604137       -4.38494      -5.415403E-16
  ao   10   1.077396E-02  -5.248436E-16   0.525198       9.051583E-17    2.34558      -0.113386        2.08908       2.510469E-16
  ao   11  -0.140610       3.501398E-15  -0.104404      -3.879222E-15    3.31883       6.749451E-02    2.24696      -1.925331E-15
  ao   12   -1.57282       6.910394E-16    1.06056      -6.184482E-16   0.582059       9.014507E-02   -1.36758      -3.740618E-16
  ao   13    1.30305      -1.767835E-15   -1.38575       1.557808E-15   -1.40525      -0.182868        2.56458       9.600677E-16
  ao   14  -0.111157      -2.196111E-15   0.568675       2.152350E-15  -0.462645       8.487203E-02  -0.223671       1.187489E-15
  ao   15   0.384978      -6.119561E-17  -0.345492       1.132515E-16   0.273125       0.749569      -4.543019E-02   3.695147E-17
  ao   16   0.171256       9.167171E-16   0.370571      -9.382754E-16   0.134946      -0.962500      -0.543515      -5.103435E-16
  ao   17   3.301914E-18   0.642048       4.521627E-18   0.863176      -5.906168E-18   4.428769E-18   8.014135E-18  -0.216975    
  ao   18   2.928690E-18  -0.438629       4.886192E-18  -0.713970      -5.201609E-18   4.406776E-18   6.848902E-18   0.419875    
  ao   19   0.241594      -3.375779E-16   0.671354       2.905435E-16  -0.417766       0.510158       0.485515       1.800681E-16
  ao   20  -1.786126E-30   0.728766      -1.957238E-30  -0.606497      -2.406657E-30  -3.314864E-30  -5.230009E-30  -0.376407    

               no   17        no   18        no   19        no   20

      occ   3.443027E-05   1.162147E-05   8.161034E-06   3.797420E-06

  ao    1   -1.82201        2.54346       0.704274       0.511163    
  ao    2    5.68820        4.03999      -0.531857       -7.58021    
  ao    3   0.229157      -0.644605        1.13447      -0.109504    
  ao    4  -0.489769        1.88764      -0.971573      -0.607145    
  ao    5    1.94205        2.25669        1.76714       -4.31295    
  ao    6  -0.400666       0.543746      -0.163190       -2.04643    
  ao    7  -5.898788E-18   4.199533E-18  -2.155365E-17  -1.611924E-17
  ao    8   0.485845      -0.219655        1.29468        1.35063    
  ao    9    1.17529      -0.497450        3.16625        3.80329    
  ao   10  -0.475792       0.465610       -1.63616       -1.29775    
  ao   11   -4.25021       -5.34236       -3.81259        4.37265    
  ao   12  -0.213360      -0.930928       0.143201       0.463292    
  ao   13   0.292045        2.32210      -0.420143       -2.76068    
  ao   14    2.49892        3.04199       0.485355       -2.79268    
  ao   15   0.546564       0.215016       0.101673       0.112744    
  ao   16  -0.752910       -1.77018       0.160705        1.00753    
  ao   17  -1.594476E-18   8.146890E-18  -2.156675E-18  -5.492034E-18
  ao   18  -1.659184E-18   7.004859E-18  -1.927442E-18  -4.074824E-18
  ao   19  -0.211912       0.510086      -0.163455      -9.213355E-02
  ao   20   4.536661E-31  -3.047527E-30   2.380467E-30   5.590228E-31


*****   symmetry block  b1    *****

               no    1        no    2        no    3        no    4        no    5        no    6        no    7        no    8

      occ    1.97467       1.498037E-02   5.012460E-03   7.854090E-04   6.075185E-04   3.339454E-04   3.219807E-04   1.017229E-04

  mo    1   0.999966       4.888232E-03  -1.931309E-03  -5.772414E-03   2.292588E-03   1.497501E-04   5.770745E-07   8.135917E-04
  mo    2  -7.251250E-03   0.705191      -0.306583      -0.482144      -8.588784E-02   0.152585       4.784437E-04   0.160379    
  mo    3   1.632334E-03  -0.607881      -0.455353      -0.155826      -0.461677       0.205506       6.318180E-04   0.195721    
  mo    4   8.736874E-05  -0.121026       0.780847      -0.226536      -8.151724E-02  -8.141837E-03  -6.304590E-05   0.342712    
  mo    5   1.066816E-05  -6.610701E-02  -8.561444E-03   0.111179       0.349709       0.217330       7.507716E-04  -0.442196    
  mo    6  -1.849777E-03  -0.212015      -9.258239E-02  -0.152943       0.608219       0.316006       9.858012E-04  -7.730220E-02
  mo    7   3.173128E-03   0.262057       7.035498E-02   0.712652      -0.303528       0.439688       1.336976E-03   7.001830E-03
  mo    8   2.276623E-08   2.931178E-06   4.702860E-07   4.962406E-05   7.046487E-06  -3.143730E-03   0.999995       1.330441E-04
  mo    9  -2.634774E-04  -2.246150E-02   0.171767      -0.212131       4.077457E-03   0.767487       2.415955E-03   0.143208    
  mo   10  -1.590053E-04   4.027795E-03  -0.214095       0.321370       0.434115      -7.038816E-02  -3.353378E-04   0.772301    

               no    9        no   10

      occ   4.831130E-05   8.126962E-06

  mo    1   1.368268E-03  -2.828565E-04
  mo    2   0.346111       4.869078E-03
  mo    3   0.321724       4.072140E-02
  mo    4   0.447334      -6.491104E-03
  mo    5   0.468977       0.631023    
  mo    6   0.193315      -0.640271    
  mo    7   0.252496      -0.263123    
  mo    8   7.713933E-06  -3.186897E-05
  mo    9  -0.504761       0.246254    
  mo   10   2.726417E-02   0.245490    

               no    1        no    2        no    3        no    4        no    5        no    6        no    7        no    8

      occ    1.97467       1.498037E-02   5.012460E-03   7.854090E-04   6.075185E-04   3.339454E-04   3.219807E-04   1.017229E-04

  ao    1   1.127784E-02  -3.745870E-02   6.492273E-02   0.199929       0.327562       0.191872       6.968556E-04  -0.787633    
  ao    2   3.790093E-02  -0.167802       0.182428       0.598511       0.343091      -0.320120      -9.941510E-04  -0.518572    
  ao    3   6.446815E-03  -3.517532E-02   4.541255E-02   0.151394       0.191380       0.127967       3.893127E-04  -0.347528    
  ao    4   0.967873        1.63847       0.297114        2.03092       -1.62954       0.423269       1.235076E-03  -0.101549    
  ao    5  -1.475162E-02  -0.573728      -0.170000       -2.16719        1.77681      -0.546470      -1.614240E-03   0.183990    
  ao    6   3.316718E-02   -1.32806      -0.374174      -0.489405      -0.426398       0.201596       6.065686E-04   0.562775    
  ao    7  -3.708526E-03   1.731002E-02  -0.332363       0.435294       0.426822      -0.519167      -1.753216E-03   0.744730    
  ao    8  -1.290162E-02   5.441908E-02  -0.658102       0.127010       8.390808E-02   0.436109       1.498169E-03   -1.24376    
  ao    9   2.807411E-03   3.114676E-03   2.921758E-02   9.539459E-02   0.269390       0.769671       2.352100E-03   0.582640    
  ao   10   2.276623E-08   2.931178E-06   4.702860E-07   4.962406E-05   7.046487E-06  -3.143730E-03   0.999995       1.330441E-04

               no    9        no   10

      occ   4.831130E-05   8.126962E-06

  ao    1    1.01957       0.278778    
  ao    2   -1.19531       -1.07551    
  ao    3   0.474740       -1.46785    
  ao    4   0.487786      -0.347005    
  ao    5  -0.775873       0.797975    
  ao    6   0.549932       0.645081    
  ao    7   0.304155       0.152021    
  ao    8  -0.127887       -1.02182    
  ao    9  -0.390020       0.427126    
  ao   10   7.713933E-06  -3.186897E-05


*****   symmetry block  a2    *****

               no    1        no    2        no    3        no    4

      occ   4.614561E-03   3.704390E-04   3.060574E-04   4.872072E-05

  mo    1   0.928924      -0.245663      -0.262368      -8.895141E-02
  mo    2   0.151897       0.405629      -0.143679       0.889803    
  mo    3  -5.220950E-02   0.644157      -0.655571      -0.390592    
  mo    4   0.333618       0.600147       0.693359      -0.218578    

               no    1        no    2        no    3        no    4

      occ   4.614561E-03   3.704390E-04   3.060574E-04   4.872072E-05

  ao    1   2.933430E-02  -0.230481       8.339077E-03   -1.02821    
  ao    2   0.382239       0.642718       0.862689      -0.216926    
  ao    3   0.743814      -0.439187      -0.713649       0.419842    
  ao    4  -2.120442E-02   0.728240      -0.607043      -0.376547    


*****   symmetry block  b2    *****

               no    1        no    2        no    3        no    4        no    5        no    6        no    7        no    8

      occ    1.97467       1.498037E-02   5.012460E-03   7.854090E-04   6.075185E-04   3.339454E-04   3.219807E-04   1.017229E-04

  mo    1   0.999966       4.888232E-03  -1.931309E-03  -5.772414E-03   2.292588E-03   1.497501E-04  -5.770745E-07   8.135917E-04
  mo    2  -7.251250E-03   0.705191      -0.306583      -0.482144      -8.588784E-02   0.152585      -4.784437E-04   0.160379    
  mo    3   1.632334E-03  -0.607881      -0.455353      -0.155826      -0.461677       0.205506      -6.318180E-04   0.195721    
  mo    4   8.736874E-05  -0.121026       0.780847      -0.226536      -8.151724E-02  -8.141837E-03   6.304591E-05   0.342712    
  mo    5   1.066816E-05  -6.610701E-02  -8.561444E-03   0.111179       0.349709       0.217330      -7.507716E-04  -0.442196    
  mo    6  -1.849777E-03  -0.212015      -9.258239E-02  -0.152943       0.608219       0.316006      -9.858012E-04  -7.730220E-02
  mo    7   3.173128E-03   0.262057       7.035498E-02   0.712652      -0.303528       0.439688      -1.336976E-03   7.001830E-03
  mo    8  -2.276623E-08  -2.931178E-06  -4.702860E-07  -4.962406E-05  -7.046490E-06   3.143730E-03   0.999995      -1.330441E-04
  mo    9  -2.634774E-04  -2.246150E-02   0.171767      -0.212131       4.077457E-03   0.767487      -2.415955E-03   0.143208    
  mo   10  -1.590053E-04   4.027795E-03  -0.214095       0.321370       0.434115      -7.038816E-02   3.353378E-04   0.772301    

               no    9        no   10

      occ   4.831130E-05   8.126962E-06

  mo    1   1.368268E-03  -2.828565E-04
  mo    2   0.346111       4.869078E-03
  mo    3   0.321724       4.072140E-02
  mo    4   0.447334      -6.491104E-03
  mo    5   0.468977       0.631023    
  mo    6   0.193315      -0.640271    
  mo    7   0.252496      -0.263123    
  mo    8  -7.713934E-06   3.186897E-05
  mo    9  -0.504761       0.246254    
  mo   10   2.726417E-02   0.245490    

               no    1        no    2        no    3        no    4        no    5        no    6        no    7        no    8

      occ    1.97467       1.498037E-02   5.012460E-03   7.854090E-04   6.075185E-04   3.339454E-04   3.219807E-04   1.017229E-04

  ao    1   1.127784E-02  -3.745870E-02   6.492273E-02   0.199929       0.327562       0.191872      -6.968556E-04  -0.787633    
  ao    2   3.790093E-02  -0.167802       0.182428       0.598511       0.343091      -0.320120       9.941511E-04  -0.518572    
  ao    3   6.446815E-03  -3.517532E-02   4.541255E-02   0.151394       0.191380       0.127967      -3.893127E-04  -0.347528    
  ao    4   0.967873        1.63847       0.297114        2.03092       -1.62954       0.423269      -1.235076E-03  -0.101549    
  ao    5  -1.475162E-02  -0.573728      -0.170000       -2.16719        1.77681      -0.546470       1.614240E-03   0.183990    
  ao    6   3.316718E-02   -1.32806      -0.374174      -0.489405      -0.426398       0.201596      -6.065686E-04   0.562775    
  ao    7  -3.708526E-03   1.731002E-02  -0.332363       0.435294       0.426822      -0.519167       1.753216E-03   0.744730    
  ao    8  -1.290162E-02   5.441908E-02  -0.658102       0.127010       8.390808E-02   0.436109      -1.498169E-03   -1.24376    
  ao    9  -2.276623E-08  -2.931178E-06  -4.702860E-07  -4.962406E-05  -7.046490E-06   3.143730E-03   0.999995      -1.330441E-04
  ao   10   2.807411E-03   3.114676E-03   2.921758E-02   9.539459E-02   0.269390       0.769671      -2.352100E-03   0.582640    

               no    9        no   10

      occ   4.831130E-05   8.126962E-06

  ao    1    1.01957       0.278778    
  ao    2   -1.19531       -1.07551    
  ao    3   0.474740       -1.46785    
  ao    4   0.487786      -0.347005    
  ao    5  -0.775873       0.797975    
  ao    6   0.549932       0.645081    
  ao    7   0.304155       0.152021    
  ao    8  -0.127887       -1.02182    
  ao    9  -7.713934E-06   3.186897E-05
  ao   10  -0.390020       0.427126    


 total number of electrons =   10.0000000000

 test slabel:                        4
  a1  b1  a2  b2 


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!


                        a1  partial gross atomic populations
   ao class       1a1        2a1        3a1        4a1        5a1        6a1 
    H _ s       0.000046   0.240690   0.286942   0.031153   0.000195   0.000019
    H _ p       0.000129   0.031893  -0.000739  -0.001224  -0.000842   0.000034
    H _ d       0.000040   0.001872   0.000087  -0.000057  -0.000001   0.000011
    F _ s       1.999784   1.593522   0.295393  -0.003427   0.007886   0.000273
    F _ p       0.000001   0.117485   1.377646   0.000552   0.000897   0.000979
    F _ d       0.000000   0.002041   0.005331   0.000159  -0.000019   0.003678
    F _ f       0.000000   0.000117   0.000235   0.000001   0.000000   0.000015

   ao class       7a1        8a1        9a1       10a1       11a1       12a1 
    H _ s       0.000000  -0.000154   0.000339   0.000000   0.000013   0.000000
    H _ p       0.000000  -0.000096   0.000007   0.000000   0.000018   0.000000
    H _ d       0.000032  -0.000001   0.000002   0.000037   0.000002   0.000000
    F _ s       0.000000   0.000299   0.000015   0.000000   0.000006   0.000000
    F _ p       0.000000   0.000878   0.000235   0.000000   0.000093   0.000000
    F _ d       0.004581   0.000212   0.000102   0.000124   0.000042   0.000193
    F _ f       0.000003   0.000002   0.000080   0.000209   0.000188   0.000113

   ao class      13a1       14a1       15a1       16a1       17a1       18a1 
    H _ s       0.000079   0.000033   0.000024   0.000000   0.000037  -0.000011
    H _ p      -0.000029   0.000017   0.000005   0.000000   0.000000   0.000017
    H _ d      -0.000003   0.000005  -0.000009   0.000043   0.000006  -0.000001
    F _ s       0.000119  -0.000001   0.000022   0.000000  -0.000013   0.000004
    F _ p       0.000033   0.000001   0.000036   0.000000  -0.000002   0.000000
    F _ d       0.000003   0.000060  -0.000002   0.000002   0.000005   0.000003
    F _ f       0.000027   0.000019   0.000008   0.000003   0.000001   0.000000

   ao class      19a1       20a1 
    H _ s       0.000000   0.000001
    H _ p       0.000006   0.000000
    H _ d       0.000000   0.000004
    F _ s       0.000002   0.000000
    F _ p       0.000000   0.000000
    F _ d       0.000000   0.000000
    F _ f       0.000000   0.000000

                        b1  partial gross atomic populations
   ao class       1b1        2b1        3b1        4b1        5b1        6b1 
    H _ p       0.039900   0.001491   0.000434   0.000258   0.000132   0.000022
    H _ d       0.004189   0.000021   0.000082  -0.000030   0.000021   0.000009
    F _ p       1.929493   0.013325   0.000234   0.000404   0.000291   0.000012
    F _ d       0.001029   0.000144   0.004252   0.000136   0.000095   0.000079
    F _ f       0.000054  -0.000001   0.000010   0.000017   0.000068   0.000211

   ao class       7b1        8b1        9b1       10b1 
    H _ p       0.000000   0.000044   0.000033   0.000001
    H _ d       0.000000  -0.000012   0.000008   0.000007
    F _ p       0.000000   0.000002   0.000002   0.000000
    F _ d       0.000000   0.000051   0.000003   0.000000
    F _ f       0.000322   0.000017   0.000003   0.000000

                        a2  partial gross atomic populations
   ao class       1a2        2a2        3a2        4a2 
    H _ d       0.000032   0.000037   0.000000   0.000043
    F _ d       0.004580   0.000125   0.000193   0.000002
    F _ f       0.000003   0.000208   0.000113   0.000003

                        b2  partial gross atomic populations
   ao class       1b2        2b2        3b2        4b2        5b2        6b2 
    H _ p       0.039900   0.001491   0.000434   0.000258   0.000132   0.000022
    H _ d       0.004189   0.000021   0.000082  -0.000030   0.000021   0.000009
    F _ p       1.929493   0.013325   0.000234   0.000404   0.000291   0.000012
    F _ d       0.001029   0.000144   0.004252   0.000136   0.000095   0.000079
    F _ f       0.000054  -0.000001   0.000010   0.000017   0.000068   0.000211

   ao class       7b2        8b2        9b2       10b2 
    H _ p       0.000000   0.000044   0.000033   0.000001
    H _ d       0.000000  -0.000012   0.000008   0.000007
    F _ p       0.000000   0.000002   0.000002   0.000000
    F _ d       0.000000   0.000051   0.000003   0.000000
    F _ f       0.000322   0.000017   0.000003   0.000000


                        gross atomic populations
     ao           H _        F _
      s         0.559406   3.893885
      p         0.113827   5.386364
      d         0.010775   0.032995
      f         0.000000   0.002748
    total       0.684008   9.315992


 Total number of electrons:   10.00000000

========================GLOBAL TIMING PER NODE========================
   process    vectrn  integral   segment    diagon     dmain    davidc   finalvw   driver  
--------------------------------------------------------------------------------
   #   1         0.0       0.0       0.0       0.8       0.8       0.0       0.0       1.8
--------------------------------------------------------------------------------
       1         0.0       0.0       0.0       0.8       0.8       0.0       0.0       1.8
