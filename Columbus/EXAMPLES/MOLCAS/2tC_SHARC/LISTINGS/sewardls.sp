   This run of MOLCAS is using the pymolcas driver

                             OPE
                             OPE          NMOL  CASOP  ENMOLC A   SO
                 OPE        NMOLC        AS  OP EN  MO LC     AS  OP
                OPENM       OL CA        SO  PE NM  OL CA     SOP EN
                OP EN      MO   LC       AS  OP ENMOL  CASO   PENMOL
                OP  EN     MO   LC       AS  OP EN     MO     LC ASO
               OP   E  NMOL  C  AS       OP  EN MO     LC     AS  OP
               OP  E   NMO   LC AS        OPEN  MO     LCASOP EN   M
               O  PEN   MO  LCA  SO
            OPE   NMO   L   CAS    OP
        OPENMOL  CASOP     ENMOL   CASOPE
     OPENMOLCA   SOPENMOLCASOPEN   MOLCASOPE
    OPENMOLCAS   OP           EN   MOL    CAS
    OPENMOLCAS       OP  ENM        O     LCA
    OPENMOLCAS    OPEN  MOLCASO     P  E  NMO
    OPENMOLCAS     OP               E  N  MOL
    OPENMOLCA   SO           PENM   O  L  CAS    OPEN    MO    LCAS
    OPENMOLCA   SOP           ENM   O  L  CAS   OP  EN  MOLC  AS   O
    OPENMOLCA   SOPE           NM      O  LCA   S      OP  EN MO
    OPENMOLC                AS         O  PEN   M      OL  CA  SOPE
    OPENMO        LCASOPE  NMOL        C  ASO   P      ENMOLC     AS
    OPE     NMO      LCA  SO     P     E   NM   OL  CA SO  PE N   MO
          OPENMOLCA            SOPE   NMO        LCAS  O    P  ENMO
     OPENMOLCASOPENMOLCASOPENMOLCASOPENMOLCA
        OPENMOLCASOPENMOLCASOPENMOLCASOPE
            OPENMOLCASOPENMOLCASOPENM
               OPENMOLCASOPENMOLCA        version v18.09-1-g93456996
                   OPENMOLCASO
                       OPE                tag

 OpenMolcas is free software; you can redistribute it and/or modify it
 under the terms of the GNU Lesser General Public License version 2.1.
 OpenMolcas is distributed in the hope that it will be useful, but it
 is provided "as is" and without any express or implied warranties.
 For more details see the full text of the license in the file
 LICENSE or in <http://www.gnu.org/licenses/>.

                 Copyright (C) The OpenMolcas Authors
           For the author list and the recommended citation,
                   consult the file CONTRIBUTORS.md

           *************************************************
           * pymolcas version py2.03                       *
           *   build b1746ecf88d668b0cc2a7c456d933324      *
           *   (after the EMIL interpreter by V. Veryazov) *
           *************************************************

configuration info
------------------
C Compiler ID: GNU
C flags: -std=gnu99
Fortran Compiler ID: GNU
Fortran flags: -cpp -fno-aggressive-loop-optimizations -fdefault-integer-8
Definitions: _MOLCAS_;_I8_;_LINUX_;_MKL_
Parallel: OFF (GA=OFF)


   -------------------------------------------------------------------------------
  |
  |           Project: molcas
  |    Submitted from: /home/lunet/cmfp2/programs/Columbus/Test/2tC/RUN3/WORK
  |      Scratch area: /home/lunet/cmfp2/programs/Columbus/Test/2tC/RUN3/WORK
  |   Save outputs to: WORKDIR
  |            Molcas: /home/lunet/cmfp2/programs/Molcas/OM_v18.09-col/build-local
  |
  | Scratch area is NOT empty
  |
  |        MOLCASMEM = 400
  |    MOLCAS_DRIVER = /PhotoChem/bin/pymolcas
  |    MOLCAS_NPROCS = 1
  |    MOLCAS_SOURCE = /home/lunet/cmfp2/programs/Molcas/OM_v18.09-col
  | MOLCAS_STRUCTURE = 0
  |
   -------------------------------------------------------------------------------

++ ---------   Input file   ---------

&SEWARD
ANGMOM
 0 0 0
RELINT
DOUGLAS-KROLL
AMFI
Basis set
N.3-21g
N1        1.09092200     0.03380900     3.05870400
End of Basis
Basis set
C.3-21g
C2        2.91200100     0.49729900     1.22981900
End of Basis
Basis set
N.3-21g
N3        2.11022600     0.45828700    -1.25969300
End of Basis
Basis set
C.3-21g
C4       -0.26473600    -0.03122100    -1.77867500
C5       -2.12432700    -0.51212900     0.10188700
C6       -1.34964300    -0.45457400     2.51592400
End of Basis
Basis set
S.3-21g
S7        5.87442700     1.07973500     2.09595300
End of Basis
Basis set
N.3-21g
N8       -0.91700400    -0.14986200    -4.28378400
End of Basis
Basis set
H.3-21g
H9        1.69328700     0.08137400     4.84904500
H10      -2.59210100    -0.78921700     4.10425100
H11      -4.06324100    -0.92214500    -0.38222300
H12      -2.70137500     0.29029300    -4.71735500
H13       0.36990200     0.60541700    -5.44618100
End of Basis
End of input

-- ----------------------------------

--- Start Module: seward at Thu Jan 21 17:54:34 2021 ---

()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()

                                              &SEWARD

                                   only a single process is used
                       available to each process: 400 MB of memory, 1 thread?
()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()

 ###############################################################################
 ###############################################################################
 ###                                                                         ###
 ###                                                                         ###
 ###    WARNING: you requested the DK-option for                             ###
 ###    a non-relativistic basis.                                            ###
 ###    This request will be ignored                                         ###
 ###                                                                         ###
 ###                                                                         ###
 ###############################################################################
 ###############################################################################
               SEWARD will generate:
                  Multipole Moment integrals up to order  2
                  Kinetic Energy integrals
                  Nuclear Attraction integrals (point charge)
                  One-Electron Hamiltonian integrals
                  Mass-Velocity integrals
                  Darwin One-Electron Contact Term integrals
                  Velocity integrals
                  Orbital angular momentum around ( 0.0000  0.0000  0.0000 )
                  Atomic mean-field integrals
                  Two-Electron Repulsion integrals

                   Integrals are discarded if absolute value <: 0.10E-13
                   Integral cutoff threshold is set to       <: 0.10E-15

++    Symmetry information:
      ---------------------

                    Character Table for C1 

                             E  
                    a        1  x, y, xy, Rz, z, xz, Ry, yz, Rx, I
--


++    Basis set information:
      ----------------------


      Basis set label: N.3-21G.........

      Electronic valence basis set:
      ------------------
      Associated Effective Charge   7.000000 au
      Associated Actual Charge      7.000000 au
      Nuclear Model: Point charge

      Shell  nPrim  nBasis  Cartesian Spherical Contaminant
         s       6       3        X                  
         p       3       2        X                  


      Basis set label: C.3-21G.........

      Electronic valence basis set:
      ------------------
      Associated Effective Charge   6.000000 au
      Associated Actual Charge      6.000000 au
      Nuclear Model: Point charge

      Shell  nPrim  nBasis  Cartesian Spherical Contaminant
         s       6       3        X                  
         p       3       2        X                  


      Basis set label: N.3-21G.........

      Electronic valence basis set:
      ------------------
      Associated Effective Charge   7.000000 au
      Associated Actual Charge      7.000000 au
      Nuclear Model: Point charge

      Shell  nPrim  nBasis  Cartesian Spherical Contaminant
         s       6       3        X                  
         p       3       2        X                  


      Basis set label: C.3-21G.........

      Electronic valence basis set:
      ------------------
      Associated Effective Charge   6.000000 au
      Associated Actual Charge      6.000000 au
      Nuclear Model: Point charge

      Shell  nPrim  nBasis  Cartesian Spherical Contaminant
         s       6       3        X                  
         p       3       2        X                  


      Basis set label: S.3-21G.........

      Electronic valence basis set:
      ------------------
      Associated Effective Charge  16.000000 au
      Associated Actual Charge     16.000000 au
      Nuclear Model: Point charge

      Shell  nPrim  nBasis  Cartesian Spherical Contaminant
         s       9       4        X                  
         p       6       3        X                  


      Basis set label: N.3-21G.........

      Electronic valence basis set:
      ------------------
      Associated Effective Charge   7.000000 au
      Associated Actual Charge      7.000000 au
      Nuclear Model: Point charge

      Shell  nPrim  nBasis  Cartesian Spherical Contaminant
         s       6       3        X                  
         p       3       2        X                  


      Basis set label: H.3-21G.........

      Electronic valence basis set:
      ------------------
      Associated Effective Charge   1.000000 au
      Associated Actual Charge      1.000000 au
      Nuclear Model: Point charge

      Shell  nPrim  nBasis  Cartesian Spherical Contaminant
         s       3       2        X                  
--


++    Molecular structure info:
      -------------------------

                    ************************************************ 
                    **** Cartesian Coordinates / Bohr, Angstrom **** 
                    ************************************************ 

     Center  Label                x              y              z                     x              y              z
        1      N1               1.090922       0.033809       3.058704              0.577291       0.017891       1.618596
        2      C2               2.912001       0.497299       1.229819              1.540965       0.263159       0.650792
        3      N3               2.110226       0.458287      -1.259693              1.116684       0.242515      -0.666601
        4      C4              -0.264736      -0.031221      -1.778675             -0.140092      -0.016521      -0.941234
        5      C5              -2.124327      -0.512129       0.101887             -1.124145      -0.271007       0.053916
        6      C6              -1.349643      -0.454574       2.515924             -0.714200      -0.240550       1.331370
        7      S7               5.874427       1.079735       2.095953              3.108613       0.571371       1.109131
        8      N8              -0.917004      -0.149862      -4.283784             -0.485258      -0.079304      -2.266881
        9      H9               1.693287       0.081374       4.849045              0.896049       0.043061       2.566004
       10      H10             -2.592101      -0.789217       4.104251             -1.371681      -0.417636       2.171876
       11      H11             -4.063241      -0.922145      -0.382223             -2.150175      -0.487978      -0.202264
       12      H12             -2.701375       0.290293      -4.717355             -1.429506       0.153616      -2.496317
       13      H13              0.369902       0.605417      -5.446181              0.195744       0.320373      -2.881995

                    *************************************** 
                    *    InterNuclear Distances / Bohr    * 
                    *************************************** 

               1 N1            2 C2            3 N3            4 C4            5 C5            6 C6    
    1 N1       0.000000
    2 C2       2.622207        0.000000
    3 N3       4.457321        2.615728        0.000000
    4 C4       5.024169        4.407043        2.479799        0.000000
    5 C5       4.402118        5.258876        4.552697        2.688097        0.000000
    6 C6       2.547447        4.552114        5.201855        4.449700        2.535946        0.000000
    7 S7       4.990268        3.140921        5.080922        7.344133        8.395856        7.397139
    8 N8       7.614304        6.743880        4.321932        2.591351        4.563220        6.820268
    9 H9       1.889557        3.841491        6.134540        6.911816        6.120615        3.871707
   10 H10      3.916018        6.341342        7.241550        6.371813        4.039122        2.044131
   11 H11      6.270504        7.298459        6.386489        4.143967        2.040064        3.997686
   12 H12      8.655310        8.180570        5.927491        3.830982        4.919549        7.396102
   13 H13      8.554512        7.144434        4.536194        3.776066        6.184751        8.214349

               7 S7            8 N8            9 H9           10 H10          11 H11          12 H12   
    7 S7       0.000000
    8 N8       9.398749        0.000000
    9 H9       5.104721        9.501350        0.000000
   10 H10      8.899907        8.577520        4.435899        0.000000
   11 H11     10.435810        5.071233        7.842884        4.723386        0.000000
   12 H12     10.981292        1.888306       10.529611        8.888083        4.702984        0.000000
   13 H13      9.349254        1.891492       10.393154       10.096000        6.901440        3.172260

              13 H13   
   13 H13      0.000000

                    ******************************************* 
                    *    InterNuclear Distances / Angstrom    * 
                    ******************************************* 

               1 N1            2 C2            3 N3            4 C4            5 C5            6 C6    
    1 N1       0.000000
    2 C2       1.387612        0.000000
    3 N3       2.358713        1.384184        0.000000
    4 C4       2.658676        2.332107        1.312253        0.000000
    5 C5       2.329500        2.782877        2.409183        1.422480        0.000000
    6 C6       1.348051        2.408875        2.752703        2.354680        1.341965        0.000000
    7 S7       2.640736        1.662104        2.688708        3.886348        4.442896        3.914397
    8 N8       4.029316        3.568707        2.287068        1.371284        2.414752        3.609130
    9 H9       0.999911        2.032830        3.246259        3.657576        3.238890        2.048819
   10 H10      2.072268        3.355694        3.832063        3.371818        2.137411        1.081707
   11 H11      3.318208        3.862178        3.379584        2.192893        1.079556        2.115484
   12 H12      4.580193        4.328971        3.136693        2.027268        2.603313        3.913849
   13 H13      4.526853        3.780671        2.400451        1.998208        3.272829        4.346846

               7 S7            8 N8            9 H9           10 H10          11 H11          12 H12   
    7 S7       0.000000
    8 N8       4.973604        0.000000
    9 H9       2.701302        5.027898        0.000000
   10 H10      4.709628        4.539028        2.347376        0.000000
   11 H11      5.522393        2.683581        4.150275        2.499508        0.000000
   12 H12      5.811049        0.999249        5.572030        4.703371        2.488712        0.000000
   13 H13      4.947412        1.000934        5.499820        5.342573        3.652085        1.678688

              13 H13   
   13 H13      0.000000

                    ************************************** 
                    *    Valence Bond Angles / Degree    * 
                    ************************************** 
                          Atom centers                 Phi
                      2 C2       1 N1       6 C6       123.41
                      2 C2       1 N1       9 H9       115.79
                      6 C6       1 N1       9 H9       120.80
                      1 N1       2 C2       3 N3       116.63
                      2 C2       3 N3       4 C4       119.72
                      3 N3       4 C4       5 C5       123.47
                      3 N3       4 C4       8 N8       116.90
                      5 C5       4 C4       8 N8       119.60
                      4 C4       5 C5       6 C6       116.78
                      4 C4       5 C5      11 H11      121.83
                      6 C6       5 C5      11 H11      121.39
                      1 N1       6 C6       5 C5       119.99
                      1 N1       6 C6      10 H10      116.62
                      5 C5       6 C6      10 H10      123.39
                      4 C4       8 N8      12 H12      116.69
                      4 C4       8 N8      13 H13      113.86
                     12 H12      8 N8      13 H13      114.13

           ***************************************************************
           *              Valence Dihedral Angles / Degree               *
           ***************************************************************
                    Atom centers                       Phi1     Phi2     Theta 
           2 C2       1 N1       6 C6       5 C5        123.41   119.99     0.05
           2 C2       1 N1       6 C6      10 H10       123.41   116.62   179.90
           9 H9       1 N1       6 C6      10 H10       120.80   116.62    -0.12
           3 N3       2 C2       1 N1       6 C6        116.63   123.41     0.45
           3 N3       2 C2       1 N1       9 H9        116.63   115.79  -179.53
           1 N1       2 C2       3 N3       4 C4        116.63   119.72    -0.79
           2 C2       3 N3       4 C4       5 C5        119.72   123.47     0.68
           2 C2       3 N3       4 C4       8 N8        119.72   116.90  -177.26
           3 N3       4 C4       5 C5       6 C6        123.47   116.78    -0.16
           3 N3       4 C4       5 C5      11 H11       123.47   121.83  -179.58
           8 N8       4 C4       5 C5      11 H11       119.60   121.83    -1.69
           3 N3       4 C4       8 N8      12 H12       116.90   116.69  -152.05
           3 N3       4 C4       8 N8      13 H13       116.90   113.86   -15.78
           5 C5       4 C4       8 N8      12 H12       119.60   116.69    29.93
           5 C5       4 C4       8 N8      13 H13       119.60   113.86   166.20
           6 C6       5 C5       4 C4       8 N8        116.78   119.60   177.73
           4 C4       5 C5       6 C6      10 H10       116.78   123.39   179.95
           5 C5       6 C6       1 N1       9 H9        119.99   120.80  -179.97
           1 N1       6 C6       5 C5       4 C4        119.99   116.78    -0.21
           1 N1       6 C6       5 C5      11 H11       119.99   121.39   179.22
          10 H10      6 C6       5 C5      11 H11       123.39   121.39    -0.62
--


            Nuclear Potential Energy            412.53257492 au


      Basis set specifications :
      Symmetry species         a  
      Basis functions           86


  Input file to MOLDEN was generated!

 ###############################################################################
 ###############################################################################
 ###                                                                         ###
 ###                                                                         ###
 ###    There were warnings during the execution                             ###
 ###    Please, check the output with care!                                  ###
 ###                                                                         ###
 ###                                                                         ###
 ###############################################################################
 ###############################################################################
--- Stop Module: seward at Thu Jan 21 17:54:37 2021 /rc=_RC_ALL_IS_WELL_ ---
--- Module seward spent 3 seconds ---

    Timing: Wall=3.08 User=2.43 System=0.63
