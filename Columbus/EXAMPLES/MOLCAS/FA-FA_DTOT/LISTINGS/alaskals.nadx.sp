   This run of MOLCAS is using the pymolcas driver

                             OPE
                             OPE          NMOL  CASOP  ENMOLC A   SO
                 OPE        NMOLC        AS  OP EN  MO LC     AS  OP
                OPENM       OL CA        SO  PE NM  OL CA     SOP EN
                OP EN      MO   LC       AS  OP ENMOL  CASO   PENMOL
                OP  EN     MO   LC       AS  OP EN     MO     LC ASO
               OP   E  NMOL  C  AS       OP  EN MO     LC     AS  OP
               OP  E   NMO   LC AS        OPEN  MO     LCASOP EN   M
               O  PEN   MO  LCA  SO
            OPE   NMO   L   CAS    OP
        OPENMOL  CASOP     ENMOL   CASOPE
     OPENMOLCA   SOPENMOLCASOPEN   MOLCASOPE
    OPENMOLCAS   OP           EN   MOL    CAS
    OPENMOLCAS       OP  ENM        O     LCA
    OPENMOLCAS    OPEN  MOLCASO     P  E  NMO
    OPENMOLCAS     OP               E  N  MOL
    OPENMOLCA   SO           PENM   O  L  CAS    OPEN    MO    LCAS
    OPENMOLCA   SOP           ENM   O  L  CAS   OP  EN  MOLC  AS   O
    OPENMOLCA   SOPE           NM      O  LCA   S      OP  EN MO
    OPENMOLC                AS         O  PEN   M      OL  CA  SOPE
    OPENMO        LCASOPE  NMOL        C  ASO   P      ENMOLC     AS
    OPE     NMO      LCA  SO     P     E   NM   OL  CA SO  PE N   MO
          OPENMOLCA            SOPE   NMO        LCAS  O    P  ENMO
     OPENMOLCASOPENMOLCASOPENMOLCASOPENMOLCA
        OPENMOLCASOPENMOLCASOPENMOLCASOPE
            OPENMOLCASOPENMOLCASOPENM
               OPENMOLCASOPENMOLCA        version v18.09-1-g9345699-dirty
                   OPENMOLCASO
                       OPE                tag

 OpenMolcas is free software; you can redistribute it and/or modify it
 under the terms of the GNU Lesser General Public License version 2.1.
 OpenMolcas is distributed in the hope that it will be useful, but it
 is provided "as is" and without any express or implied warranties.
 For more details see the full text of the license in the file
 LICENSE or in <http://www.gnu.org/licenses/>.

                 Copyright (C) The OpenMolcas Authors
           For the author list and the recommended citation,
                   consult the file CONTRIBUTORS.md

           *************************************************
           * pymolcas version py2.03                       *
           *   build cc5afe975c0d8f3d2e37f599b10b09cf      *
           *   (after the EMIL interpreter by V. Veryazov) *
           *************************************************

configuration info
------------------
C Compiler ID: GNU
C flags: -std=gnu99
Fortran Compiler ID: Intel
Fortran flags: -fpp -i8 -r8 -heap-arrays
Definitions: _MOLCAS_;_I8_;_LINUX_;_MKL_
Parallel: OFF (GA=OFF)


   --------------------------------------------------------------------------------------------------------------------------
  |
  |           Project: molcas
  |    Submitted from: /mnt/gpfs01/home/cm/cmfp2/programs/Columbus/Test/NAC_OpenMolcas/EXAMPLES_Yafu/FA-FA_DTOT/RUN6.min/WORK
  |      Scratch area: /mnt/gpfs01/home/cm/cmfp2/programs/Columbus/Test/NAC_OpenMolcas/EXAMPLES_Yafu/FA-FA_DTOT/RUN6.min/WORK
  |   Save outputs to: WORKDIR
  |            Molcas: /home/cm/cmfp2/programs/Molcas/OpenMolcas/build-col
  |
  | Scratch area is NOT empty
  |
  |        MOLCASMEM = 2000
  |    MOLCAS_DRIVER = /mnt/gpfs01/home/cm/cmfp2/bin/pymolcas
  |    MOLCAS_NPROCS = 1
  |    MOLCAS_SOURCE = /home/cm/cmfp2/programs/Molcas/OpenMolcas
  | MOLCAS_STRUCTURE = 0
  |
   --------------------------------------------------------------------------------------------------------------------------

++ ---------   Input file   ---------

&ALASKA
VERBOSE
SHOW GRADIENT CONTRIBUTIONS
NONUC
End of Input

-- ----------------------------------

--- Start Module: alaska at Thu Apr  8 14:30:15 2021 ---
 
()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()

                                              &ALASKA

                                   only a single process is used
                       available to each process: 2.0 GB of memory, 1 thread?
()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()
 
 
                     Threshold for contributions to the gradient: .100E-06
 
 
                    ********************************************
                    * Symmetry Adapted Cartesian Displacements *
                    ********************************************
 
 
           Irreducible representation : a  
           Basis function(s) of irrep: x, y, xy, Rz, z, xz, Ry, yz, Rx, I                                              
 
 Basis Label        Type   Center Phase
   1   C1           x         1     1
   2   C1           y         1     1
   3   C1           z         1     1
   4   C2           x         2     1
   5   C2           y         2     1
   6   C2           z         2     1
   7   O3           x         3     1
   8   O3           y         3     1
   9   O3           z         3     1
  10   O4           x         4     1
  11   O4           y         4     1
  12   O4           z         4     1
  13   H5           x         5     1
  14   H5           y         5     1
  15   H5           z         5     1
  16   H6           x         6     1
  17   H6           y         6     1
  18   H6           z         6     1
  19   H7           x         7     1
  20   H7           y         7     1
  21   H7           z         7     1
  22   H8           x         8     1
  23   H8           y         8     1
  24   H8           z         8     1
 
                     No automatic utilization of translational and rotational invariance of the energy is employed.
 
 Skipping Nuclear Charge Contribution
 
 **************************************************************************************************************
 *                                                                                                            *
 *                                      The Renormalization Contribution                                      *
 *                                                                                                            *
 **************************************************************************************************************
 
  Irreducible representation: a  
 ---------------------------------------------------------
                     X             Y             Z        
 ---------------------------------------------------------
  C1                0.00134623    0.00000000   -0.05607214
  C2                0.00134623   -0.00000000    0.05607214
  O3               -0.00107433   -0.00000000    0.04415810
  O4               -0.00107433    0.00000000   -0.04415810
  H5               -0.00013583    0.00388068    0.00587341
  H6               -0.00013581   -0.00388067   -0.00587341
  H7               -0.00013607   -0.00388068    0.00587339
  H8               -0.00013609    0.00388067   -0.00587339
 ---------------------------------------------------------
 
 
 **************************************************************************************************************
 *                                                                                                            *
 *                                      The Kinetic Energy Contribution                                       *
 *                                                                                                            *
 **************************************************************************************************************
 
  Irreducible representation: a  
 ---------------------------------------------------------
                     X             Y             Z        
 ---------------------------------------------------------
  C1               -0.00187960   -0.00000004    0.13417657
  C2               -0.00187960    0.00000003   -0.13417654
  O3                0.00128286    0.00000003   -0.13480483
  O4                0.00128287   -0.00000003    0.13480481
  H5                0.00029816    0.00412413    0.00028384
  H6                0.00029815   -0.00412412   -0.00028384
  H7                0.00029858   -0.00412412    0.00028385
  H8                0.00029859    0.00412412   -0.00028385
 ---------------------------------------------------------
 
 
 **************************************************************************************************************
 *                                                                                                            *
 *                                    The Nuclear Attraction Contribution                                     *
 *                                                                                                            *
 **************************************************************************************************************
 
  Irreducible representation: a  
 ---------------------------------------------------------
                     X             Y             Z        
 ---------------------------------------------------------
  C1                0.10415387    0.00000026   -1.85942177
  C2                0.10415392   -0.00000023    1.85942145
  O3               -0.06777693   -0.00000031    1.60403121
  O4               -0.06777696    0.00000029   -1.60403097
  H5               -0.01818537    0.09738003    0.13338922
  H6               -0.01818503   -0.09738003   -0.13338920
  H7               -0.01819158   -0.09737983    0.13338894
  H8               -0.01819193    0.09737982   -0.13338888
 ---------------------------------------------------------
 
 Conventional ERI gradients!
 
 **************************************************************************************************************
 *                                                                                                            *
 *                                         Two-electron Contribution                                          *
 *                                                                                                            *
 **************************************************************************************************************
 
  Irreducible representation: a  
 ---------------------------------------------------------
                     X             Y             Z        
 ---------------------------------------------------------
  C1               -0.10061076   -0.00000019    1.59974663
  C2               -0.10061080    0.00000015   -1.59974636
  O3                0.06558748    0.00000025   -1.36017799
  O4                0.06558750   -0.00000023    1.36017780
  H5                0.01750895   -0.09835734   -0.12566863
  H6                0.01750865    0.09835733    0.12566861
  H7                0.01751434    0.09835713   -0.12566838
  H8                0.01751464   -0.09835711    0.12566831
 ---------------------------------------------------------
 
 
 **************************************************
 *                                                *
 *              Molecular gradients               *
 *                                                *
 **************************************************
 
  Irreducible representation: a  
 ---------------------------------------------------------
                     X             Y             Z        
 ---------------------------------------------------------
  C1                0.00031729    0.00000004   -0.06942642
  C2                0.00031729   -0.00000003    0.06942640
  O3                0.00016775   -0.00000003    0.06489028
  O4                0.00016775    0.00000003   -0.06489026
  H5               -0.00024243   -0.00073386    0.00213102
  H6               -0.00024242    0.00073385   -0.00213102
  H7               -0.00024260    0.00073385    0.00213102
  H8               -0.00024261   -0.00073384   -0.00213102
 ---------------------------------------------------------
 
 
 **************************************************************************************************************
 *                                                                                                            *
 *                                            The CSF Contribution                                            *
 *                                                                                                            *
 **************************************************************************************************************
 
  Irreducible representation: a  
 ---------------------------------------------------------
                     X             Y             Z        
 ---------------------------------------------------------
  C1               -0.00076889    0.00000002   -0.00000984
  C2               -0.00076889   -0.00000002    0.00000984
  O3               -0.00013611    0.00000001   -0.00000336
  O4               -0.00013611   -0.00000001    0.00000335
  H5                0.00024635   -0.00004381   -0.00003885
  H6                0.00024637    0.00004381    0.00003885
  H7                0.00024595    0.00004381   -0.00003885
  H8                0.00024593   -0.00004381    0.00003885
 ---------------------------------------------------------
 
 
 ******************************************************
 *                                                    *
 *              CSF derivative coupling               *
 *                                                    *
 ******************************************************
 
  Irreducible representation: a  
 ---------------------------------------------------------
                     X             Y             Z        
 ---------------------------------------------------------
  C1               -0.00076889    0.00000002   -0.00000984
  C2               -0.00076889   -0.00000002    0.00000984
  O3               -0.00013611    0.00000001   -0.00000336
  O4               -0.00013611   -0.00000001    0.00000335
  H5                0.00024635   -0.00004381   -0.00003885
  H6                0.00024637    0.00004381    0.00003885
  H7                0.00024595    0.00004381   -0.00003885
  H8                0.00024593   -0.00004381    0.00003885
 ---------------------------------------------------------
 
---------------------------------------------------------------------------------------------
  Nr.	 Label		Type		Offset		Length	   Atime	  Address
---------------------------------------------------------------------------------------------
  1	DENS        	REAL	       1043494	        7920          31	[0x3756bf0]
---------------------------------------------------------------------------------------------
Maximal available memory for Molcas = 1999992080
MEMORY WARNING: some memory allocations are not released!
--- Stop Module: alaska at Thu Apr  8 14:30:15 2021 /rc=_RC_ALL_IS_WELL_ ---

    Timing: Wall=0.33 User=0.26 System=0.04
