

     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 This program allows the csf mixing coefficient and orbital expansion coefficient
 optimization using the graphical unitary group approach and the exponential
 operator mcscf method.
 references:  r. shepard and j. simons, ' int. j. quantum chem. symp. 14, 211 (1980).
              r. shepard, i. shavitt, and j. simons, j. chem. phys. 76, 543 (1982).
              r. shepard in "ab initio methods in quantum chemistry ii" advances in chemical
                  physics 69, edited by k. p. lawley (wiley, new york, 1987) pp. 63-200.
 Original autor: Ron Shepard, ANL
 Later revisions: Michal Dallos, University Vienna

 This Version of Program MCSCF is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.4.0.2     **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 Workspace allocation information:
       262144000 of real*8 words ( 2000.00 MB) of work space has been allocated.

 user input information:

 ======== echo of the mcscf input ========
 ------------------------------------------------------------------------
  &input
   niter=100,
   nmiter=50,
   nciitr=300,
   tol(3)=1.e-8,
   tol(2)=1.e-8,
   tol(1)=1.e-10,
   NSTATE=0,
   npath=1,3,9,10,13,17,19,21,-11,12, 2,26
   ncoupl=5,
   tol(9)=1.e-3,
   FCIORB=  1,13,20,1,14,20,1,15,20,1,16,20,1,17,20,1,18,20
   NAVST(1) = 3,
   WAVST(1,1)=1 ,
   WAVST(1,2)=1 ,
   WAVST(1,3)=1 ,
  &end
 ------------------------------------------------------------------------


 ***  Integral file informations  ***



 Integral file header information:
 SEWARD INTEGRALS                                                                
 total ao core energy =  100.585551835


   ******  Basis set information:  ******

 Number of irreps:                  1
 Total number of basis functions:  44

 irrep no.              1
 irrep label           a  
 no. of bas.fcions.    44
 map-vector 1 , imtype=                     3
  1   1   1   1   1   1   1   1   1   2   2   2   2   2   2   2   2   2   3   3
  3   3   3   3   3   3   3   4   4   4   4   4   4   4   4   4   5   5   6   6
  7   7   8   8
 map-vector 2 , imtype=                     4
  1   1   1   2   2   2   2   2   2   1   1   1   2   2   2   2   2   2   1   1
  1   2   2   2   2   2   2   1   1   1   2   2   2   2   2   2   1   1   1   1
  1   1   1   1
 using sifcfg values:                  4096                  3272
                  4096                  2730                     0
 warning: resetting tol(6) due to possible inconsistency.


 ***  MCSCF optimization procedure parmeters:  ***


 maximum number of mcscf iterations:        niter=   100

 maximum number of psci micro-iterations:   nmiter=   50
 maximum r,s subspace dimension allowed:    nvrsmx=   30

AO integrals are read from Seward integral files 
 ... RUNFILE, ONEINT , ORDINT 

 tol(1)=  1.0000E-10. . . . delta-emc convergence criterion.
 tol(2)=  1.0000E-08. . . . wnorm convergence criterion.
 tol(3)=  1.0000E-08. . . . knorm convergence criterion.
 tol(4)=  1.0000E-08. . . . apxde convergence criterion.
 tol(5)=  1.0000E-04. . . . small diagonal matrix element tolerance.
 tol(6)=  1.2500E-09. . . . minimum ci-psci residual norm.
 tol(7)=  1.0000E-05. . . . maximum ci-psci residual norm.
 tol(8)=  1.0000E+00. . . . maximum abs(k(xy)) allowed.
 tol(9)=  1.0000E-03. . . . wnorm coupling tolerance.
 tol(10)= 0.0000E+00. . . . maximum psci emergency shift parameter.
 tol(11)= 0.0000E+00. . . . minimum psci emergency shift parameter.
 tol(12)= 0.0000E+00. . . . increment of psci emergency shift parameter.


 *** State averaging informations: ***


 MCSCF calculation performed for  1 DRT.

 DRT  first state   no.of aver.states   weights
  1   ground state          3             0.333 0.333 0.333

 The number of hmc(*) eigenvalues and eigenvectors calculated each iteration per DRT:
 DRT.   no.of eigenv.(=ncol)
    1        4

 orbital coefficients are optimized for the ground state (nstate=0).

 Orbitals included in invariant subspaces:
   symmetry   orbital   mask
       1      13( 13)    20
       1      14( 14)    20
       1      15( 15)    20
       1      16( 16)    20
       1      17( 17)    20
       1      18( 18)    20

 npath(*) options:
  2:  orbital-state coupling terms will be included beginning on iteration ncoupl=  5
  3:  print intermediate timing information.
  9:  suppress the drt listing.
 10:  suppress the hmc(*) eigenvector listing.
 12:  diagonalize the hmc(*) matrix iteratively.
        nunitv= 1 nciitr=** mxvadd=20 nvcimx=20
       rtolci(*),wnorm=     1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 0.0000E+00
   noldv =   0
 13:  get initial orbitals from the formatted file, mocoef.
 17:  print the final natural orbitals and occupations.
 19:  transform the virtual orbitals to diagonalize qvv(*).
 21:  write out the one- and two- electron density for further use (files:mcd1fl, mcd2fl).
 26:  Read AO integrals in MOLCAS native format 


   ******  DRT info section  ******


 Informations for the DRT no.  1

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a  
 Total number of electrons:   32
 Spin multiplicity:            1
 Number of active orbitals:    6
 Number of active electrons:   8
 Total number of CSFs:       105
 

 faar:   0 active-active rotations allowed out of:  15 possible.


 Number of active-double rotations:        72
 Number of active-active rotations:         0
 Number of double-virtual rotations:      312
 Number of active-virtual rotations:      156
 lenbfsdef=                131071  lenbfs=                   676
  number of integrals per class 1:11 (cf adda 
 class  1 (pq|rs):         #         231
 class  2 (pq|ri):         #        1512
 class  3 (pq|ia):         #        6552
 class  4 (pi|qa):         #       11232
 class  5 (pq|ra):         #        3276
 class  6 (pq|ij)/(pi|qj): #        4662
 class  7 (pq|ab):         #        7371
 class  8 (pa|qb):         #       14196
 class  9 p(bp,ai)         #       48672
 class 10p(ai,jp):        #       22464
 class 11p(ai,bj):        #       52728

 Size of orbital-Hessian matrix B:                   152316
 Size of the orbital-state Hessian matrix C:         170100
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:         322416


 Source of the initial MO coeficients:

 Input MO coefficient file: /mnt/gpfs01/home/cm/cmfp2/programs/Columbus/Test/NAC_OpenMol
 

               starting mcscf iteration...   1
 !timer:                                 cpu_time=     0.018 walltime=     0.974

 orbital-state coupling will not be calculated this iteration.

 *** Starting integral transformation ***

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     1, naopsy(1) =    44, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     1, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 64959, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 65000

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore= 262135325

 input file header information:
 SEWARD INTEGRALS                                                                

 total ao core energy =   1.005855518353E+02

 nsym = 1 nbft=  44

 symmetry  =    1
 SLABEL(*) = a   
 NBPSY(*)  =   44
 INFOAO(*) = unset

 inoutp: segmentation information:
 in-core transformation space,   avcinc = 261964025
 address segment size,           sizesg = 261812539
 number of in-core blocks,       nincbk =         1
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 processing ORDINT  
 twoaofs: molcasbuflen determined to                 980101
 twoaofs: remaining lcore              261964228
 total read=    980100
 total abs(val)>1d-15 =    660488
 total sym1cmp =    330689
 total sym2cmp =    330689

     330689 array elements were read by twoaof,
     330689 array elements were written into     8 da records of length   64957.
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: Im(SO:x) array: ITYPEA= 3 ITYPEB=   0 nummot=  694 nrecmo=  1 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)   694
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: Im(SO:y) array: ITYPEA= 3 ITYPEB=   0 nummot=  738 nrecmo=  2 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)   738
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: Im(SO:z) array: ITYPEA= 3 ITYPEB=   0 nummot=  588 nrecmo=  3 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)   588
 L**2             component                      1  not on file ... skipping
 L**2             component                      2  not on file ... skipping
 L**2             component                      3  not on file ... skipping
 L**2             component                      4  not on file ... skipping
 L**2             component                      5  not on file ... skipping
 L**2             component                      6  not on file ... skipping
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: Im(lx)   array: ITYPEA= 3 ITYPEB=   0 nummot=  945 nrecmo=  4 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)   945
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: Im(ly)   array: ITYPEA= 3 ITYPEB=   0 nummot=  946 nrecmo=  5 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)   946
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: Im(lz)   array: ITYPEA= 3 ITYPEB=   0 nummot=  946 nrecmo=  6 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)   946
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: X(*)     array: ITYPEA= 3 ITYPEB=   0 nummot=  990 nrecmo=  7 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)   990
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: Y(*)     array: ITYPEA= 3 ITYPEB=   0 nummot=  985 nrecmo=  8 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)   985
 fcoreao=   4.27816800000000       fcore=  0.000000000000000E+000

 tran1e: Z(*)     array: ITYPEA= 3 ITYPEB=   0 nummot=  990 nrecmo=  9 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.427816800000E+01
         kntmo(*)   990
 fcoreao=   349.964200972800       fcore=  0.000000000000000E+000

 tran1e: XX(*)    array: ITYPEA= 3 ITYPEB=   0 nummot=  990 nrecmo= 10 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.349964200973E+03
         kntmo(*)   990
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: XY(*)    array: ITYPEA= 3 ITYPEB=   0 nummot=  988 nrecmo= 11 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)   988
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: XZ(*)    array: ITYPEA= 3 ITYPEB=   0 nummot=  990 nrecmo= 12 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)   990
 fcoreao=   12.2193452019240       fcore=  0.000000000000000E+000

 tran1e: YY(*)    array: ITYPEA= 3 ITYPEB=   0 nummot=  990 nrecmo= 13 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.122193452019E+02
         kntmo(*)   990
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: YZ(*)    array: ITYPEA= 3 ITYPEB=   0 nummot=  985 nrecmo= 14 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)   985
 fcoreao=   54.8930844069655       fcore=  0.000000000000000E+000

 tran1e: ZZ(*)    array: ITYPEA= 3 ITYPEB=   0 nummot=  990 nrecmo= 15 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.548930844070E+02
         kntmo(*)   990
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: S1(*)    array: ITYPEA= 3 ITYPEB=   0 nummot=   44 nrecmo= 16 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)    44
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: T1(*)    array: ITYPEA= 3 ITYPEB=   0 nummot=  990 nrecmo= 17 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)   990
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: V1(*)    array: ITYPEA= 3 ITYPEB=   0 nummot=  990 nrecmo= 18 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)   990

 tran1e: transformation of the input 1-e arrays is complete.
 trmain:     263385 transformed 1/r12    array elements were written in      49 records.

 !timer: 2-e transformation              cpu_time=     0.122 walltime=     0.775

 mosort: allocated sort2 space, avc2is=   261997178 available sort2 space, avcisx=   261997430

 trial vectors are generated internally.

 trial vector  1 is unit matrix column     1
 ciiter=  52 noldhv= 18 noldv= 18

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -227.6831283086     -328.2686801439        0.0000000004        0.0000000013
    2      -227.5324466876     -328.1179985229        0.0000000000        0.0000000013
    3      -227.5320208555     -328.1175726908        0.0000000007        0.0000000013
    4      -227.4089739422     -327.9945257774        0.0003487910        0.0100000000
 
  tol(10)=  0.000000000000000E+000  eshsci=  9.921916634877780E-008
 Total number of micro iterations:    7

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 1.00000000 pnorm= 0.0000E+00 rznorm= 6.3198E-10 rpnorm= 0.0000E+00 noldr=  7 nnewr=  7 nolds=  0 nnews=  0
 

 fdd(*) eigenvalues. symmetry block  1
   -41.222529  -41.222522  -22.610593  -22.610567   -2.901461   -2.899636   -1.725318   -1.717514   -1.332982   -1.327002
    -1.319598   -1.315689

 qvv(*) eigenvalues. symmetry block  1
     0.480238    0.531546    0.650378    0.690873    0.700701    0.804992    1.318126    1.580707    1.595981    1.688007
     1.729763    1.965927    2.001911    2.147856    2.291084    2.297641    2.346782    2.386135    2.388179    2.450726
     2.474051    2.577502    2.718732    2.784167    3.746411    3.865262

 restrt: restart information saved on the restart file (unit= 13).
 !timer: mcscf iteration                 cpu_time=     0.210 walltime=     1.256

 not all mcscf convergence criteria are satisfied.
 iter=    1 emc=   -227.5825319506 demc= 2.2758E+02 wnorm= 7.9375E-07 knorm= 7.5482E-07 apxde= 1.2417E-13    *not conv.*     

               starting mcscf iteration...   2
 !timer:                                 cpu_time=     0.228 walltime=     2.229

 orbital-state coupling will not be calculated this iteration.

 *** Starting integral transformation ***

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     1, naopsy(1) =    44, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     1, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 64959, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 65000

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore= 262135325

 input file header information:
 SEWARD INTEGRALS                                                                

 total ao core energy =   1.005855518353E+02

 nsym = 1 nbft=  44

 symmetry  =    1
 SLABEL(*) = a   
 NBPSY(*)  =   44
 INFOAO(*) = unset

 inoutp: segmentation information:
 in-core transformation space,   avcinc = 261964025
 address segment size,           sizesg = 261812539
 number of in-core blocks,       nincbk =         1
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: Im(SO:x) array: ITYPEA= 3 ITYPEB=   0 nummot=  427 nrecmo=  1 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)   427
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: Im(SO:y) array: ITYPEA= 3 ITYPEB=   0 nummot=  494 nrecmo=  2 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)   494
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: Im(SO:z) array: ITYPEA= 3 ITYPEB=   0 nummot=  346 nrecmo=  3 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)   346
 L**2             component                      1  not on file ... skipping
 L**2             component                      2  not on file ... skipping
 L**2             component                      3  not on file ... skipping
 L**2             component                      4  not on file ... skipping
 L**2             component                      5  not on file ... skipping
 L**2             component                      6  not on file ... skipping
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: Im(lx)   array: ITYPEA= 3 ITYPEB=   0 nummot=  938 nrecmo=  4 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)   938
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: Im(ly)   array: ITYPEA= 3 ITYPEB=   0 nummot=  945 nrecmo=  5 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)   945
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: Im(lz)   array: ITYPEA= 3 ITYPEB=   0 nummot=  942 nrecmo=  6 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)   942
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: X(*)     array: ITYPEA= 3 ITYPEB=   0 nummot=  990 nrecmo=  7 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)   990
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: Y(*)     array: ITYPEA= 3 ITYPEB=   0 nummot=  969 nrecmo=  8 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)   969
 fcoreao=   4.27816800000000       fcore=  0.000000000000000E+000

 tran1e: Z(*)     array: ITYPEA= 3 ITYPEB=   0 nummot=  987 nrecmo=  9 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.427816800000E+01
         kntmo(*)   987
 fcoreao=   349.964200972800       fcore=  0.000000000000000E+000

 tran1e: XX(*)    array: ITYPEA= 3 ITYPEB=   0 nummot=  990 nrecmo= 10 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.349964200973E+03
         kntmo(*)   990
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: XY(*)    array: ITYPEA= 3 ITYPEB=   0 nummot=  976 nrecmo= 11 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)   976
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: XZ(*)    array: ITYPEA= 3 ITYPEB=   0 nummot=  990 nrecmo= 12 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)   990
 fcoreao=   12.2193452019240       fcore=  0.000000000000000E+000

 tran1e: YY(*)    array: ITYPEA= 3 ITYPEB=   0 nummot=  978 nrecmo= 13 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.122193452019E+02
         kntmo(*)   978
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: YZ(*)    array: ITYPEA= 3 ITYPEB=   0 nummot=  971 nrecmo= 14 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)   971
 fcoreao=   54.8930844069655       fcore=  0.000000000000000E+000

 tran1e: ZZ(*)    array: ITYPEA= 3 ITYPEB=   0 nummot=  988 nrecmo= 15 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.548930844070E+02
         kntmo(*)   988
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: S1(*)    array: ITYPEA= 3 ITYPEB=   0 nummot=   44 nrecmo= 16 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)    44
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: T1(*)    array: ITYPEA= 3 ITYPEB=   0 nummot=  990 nrecmo= 17 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)   990
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: V1(*)    array: ITYPEA= 3 ITYPEB=   0 nummot=  990 nrecmo= 18 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)   990

 tran1e: transformation of the input 1-e arrays is complete.
 trmain:     250819 transformed 1/r12    array elements were written in      46 records.


 mosort: allocated sort2 space, avc2is=   261997178 available sort2 space, avcisx=   261997430

   4 trial vectors read from nvfile (unit= 29).
 ciiter=  20 noldhv= 17 noldv= 17

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -227.6831283036     -328.2686801388        0.0000000003        0.0000000013
    2      -227.5324466901     -328.1179985253        0.0000000003        0.0000000013
    3      -227.5320208581     -328.1175726934        0.0000000003        0.0000000013
    4      -227.4089740078     -327.9945258431        0.0002753484        0.0100000000
 
  tol(10)=  0.000000000000000E+000  eshsci=  5.983597981529749E-009
 Total number of micro iterations:    4

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 1.00000000 pnorm= 0.0000E+00 rznorm= 9.9907E-10 rpnorm= 0.0000E+00 noldr=  4 nnewr=  4 nolds=  0 nnews=  0
 

 fdd(*) eigenvalues. symmetry block  1
   -41.222529  -41.222522  -22.610593  -22.610567   -2.901461   -2.899636   -1.725318   -1.717514   -1.332982   -1.327002
    -1.319599   -1.315689

 qvv(*) eigenvalues. symmetry block  1
     0.480238    0.531546    0.650378    0.690873    0.700701    0.804992    1.318126    1.580707    1.595981    1.688007
     1.729763    1.965927    2.001911    2.147856    2.291084    2.297641    2.346782    2.386135    2.388179    2.450726
     2.474051    2.577502    2.718732    2.784167    3.746411    3.865262

 restrt: restart information saved on the restart file (unit= 13).

 not all mcscf convergence criteria are satisfied.
 iter=    2 emc=   -227.5825319506 demc=-1.1369E-13 wnorm= 4.7869E-08 knorm= 7.1030E-08 apxde= 7.0611E-16    *not conv.*     

               starting mcscf iteration...   3
 !timer:                                 cpu_time=     0.346 walltime=     2.674

 orbital-state coupling will not be calculated this iteration.

 *** Starting integral transformation ***

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     1, naopsy(1) =    44, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     1, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 64959, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 65000

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore= 262135325

 input file header information:
 SEWARD INTEGRALS                                                                

 total ao core energy =   1.005855518353E+02

 nsym = 1 nbft=  44

 symmetry  =    1
 SLABEL(*) = a   
 NBPSY(*)  =   44
 INFOAO(*) = unset

 inoutp: segmentation information:
 in-core transformation space,   avcinc = 261964025
 address segment size,           sizesg = 261812539
 number of in-core blocks,       nincbk =         1
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: Im(SO:x) array: ITYPEA= 3 ITYPEB=   0 nummot=  269 nrecmo=  1 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)   269
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: Im(SO:y) array: ITYPEA= 3 ITYPEB=   0 nummot=  339 nrecmo=  2 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)   339
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: Im(SO:z) array: ITYPEA= 3 ITYPEB=   0 nummot=  233 nrecmo=  3 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)   233
 L**2             component                      1  not on file ... skipping
 L**2             component                      2  not on file ... skipping
 L**2             component                      3  not on file ... skipping
 L**2             component                      4  not on file ... skipping
 L**2             component                      5  not on file ... skipping
 L**2             component                      6  not on file ... skipping
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: Im(lx)   array: ITYPEA= 3 ITYPEB=   0 nummot=  931 nrecmo=  4 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)   931
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: Im(ly)   array: ITYPEA= 3 ITYPEB=   0 nummot=  946 nrecmo=  5 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)   946
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: Im(lz)   array: ITYPEA= 3 ITYPEB=   0 nummot=  936 nrecmo=  6 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)   936
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: X(*)     array: ITYPEA= 3 ITYPEB=   0 nummot=  987 nrecmo=  7 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)   987
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: Y(*)     array: ITYPEA= 3 ITYPEB=   0 nummot=  936 nrecmo=  8 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)   936
 fcoreao=   4.27816800000000       fcore=  0.000000000000000E+000

 tran1e: Z(*)     array: ITYPEA= 3 ITYPEB=   0 nummot=  975 nrecmo=  9 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.427816800000E+01
         kntmo(*)   975
 fcoreao=   349.964200972800       fcore=  0.000000000000000E+000

 tran1e: XX(*)    array: ITYPEA= 3 ITYPEB=   0 nummot=  990 nrecmo= 10 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.349964200973E+03
         kntmo(*)   990
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: XY(*)    array: ITYPEA= 3 ITYPEB=   0 nummot=  963 nrecmo= 11 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)   963
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: XZ(*)    array: ITYPEA= 3 ITYPEB=   0 nummot=  989 nrecmo= 12 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)   989
 fcoreao=   12.2193452019240       fcore=  0.000000000000000E+000

 tran1e: YY(*)    array: ITYPEA= 3 ITYPEB=   0 nummot=  967 nrecmo= 13 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.122193452019E+02
         kntmo(*)   967
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: YZ(*)    array: ITYPEA= 3 ITYPEB=   0 nummot=  947 nrecmo= 14 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)   947
 fcoreao=   54.8930844069655       fcore=  0.000000000000000E+000

 tran1e: ZZ(*)    array: ITYPEA= 3 ITYPEB=   0 nummot=  982 nrecmo= 15 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.548930844070E+02
         kntmo(*)   982
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: S1(*)    array: ITYPEA= 3 ITYPEB=   0 nummot=   44 nrecmo= 16 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)    44
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: T1(*)    array: ITYPEA= 3 ITYPEB=   0 nummot=  987 nrecmo= 17 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)   987
 fcoreao=  0.000000000000000E+000  fcore=  0.000000000000000E+000

 tran1e: V1(*)    array: ITYPEA= 3 ITYPEB=   0 nummot=  990 nrecmo= 18 symmetry=  1
          fcore=  0.000000000000E+00 fcormo=  0.000000000000E+00
         kntmo(*)   990

 tran1e: transformation of the input 1-e arrays is complete.
 trmain:     218185 transformed 1/r12    array elements were written in      40 records.


 mosort: allocated sort2 space, avc2is=   261997178 available sort2 space, avcisx=   261997430
 !timer: mosrt1                          cpu_time=     0.027 walltime=     1.784
 !timer: mosort                          cpu_time=     0.037 walltime=     1.840

   4 trial vectors read from nvfile (unit= 29).
 ciiter=  20 noldhv= 17 noldv= 17

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -227.6831283034     -328.2686801386        0.0000000003        0.0000000013
    2      -227.5324466902     -328.1179985254        0.0000000003        0.0000000013
    3      -227.5320208582     -328.1175726935        0.0000000004        0.0000000013
    4      -227.4089740081     -327.9945258433        0.0002753440        0.0100000000
 
  tol(10)=  0.000000000000000E+000  eshsci=  1.070487264342325E-009
 Total number of micro iterations:    3

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 1.00000000 pnorm= 0.0000E+00 rznorm= 3.9709E-10 rpnorm= 0.0000E+00 noldr=  3 nnewr=  3 nolds=  0 nnews=  0
 

 fdd(*) eigenvalues. symmetry block  1
   -41.222529  -41.222522  -22.610593  -22.610567   -2.901461   -2.899636   -1.725318   -1.717514   -1.332982   -1.327002
    -1.319599   -1.315689

 qvv(*) eigenvalues. symmetry block  1
     0.480238    0.531546    0.650378    0.690873    0.700701    0.804992    1.318126    1.580707    1.595981    1.688007
     1.729763    1.965927    2.001911    2.147856    2.291084    2.297641    2.346782    2.386135    2.388179    2.450726
     2.474051    2.577502    2.718732    2.784167    3.746411    3.865262

 restrt: restart information saved on the restart file (unit= 13).
 !timer: mcscf iteration                 cpu_time=     0.106 walltime=     1.913

 all mcscf convergence criteria are satisfied.

 final mcscf convergence values:
 iter=    3 emc=   -227.5825319506 demc= 5.6843E-14 wnorm= 8.5639E-09 knorm= 7.1187E-09 apxde= 2.1707E-16    *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 wt 0.333 total energy=     -227.683128303, rel. (eV)=   0.000000
   DRT #1 state # 2 wt 0.333 total energy=     -227.532446690, rel. (eV)=   4.100257
   DRT #1 state # 3 wt 0.333 total energy=     -227.532020858, rel. (eV)=   4.111845
   ------------------------------------------------------------


 MO-coefficient print-out skipped (no flag 32)
 They may be found in the MOCOEF directory.

          natural orbitals of the final iteration,block  1    -  a  
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000
               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     1.90765254     1.90096539     1.66823938     1.66394868
               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
  occ(*)=     0.46382221     0.39537180     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   25        MO   26        MO   27        MO   28        MO   29        MO   30        MO   31        MO   32
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   33        MO   34        MO   35        MO   36        MO   37        MO   38        MO   39        MO   40
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   41        MO   42        MO   43        MO   44
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000
 d1(*), fmc(*), and qmc(*) written to the 1-particle density matrix file.
        845 d2(*) elements written to the 2-particle density matrix file: mcd2fl                                                      


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        a   partial gross atomic populations
   ao class       1a         2a         3a         4a         5a         6a  
    C1_ s       0.000362   0.000315   0.999682   0.999811   0.141015   0.136058
    C1_ p       0.000382   0.000356   0.000003   0.000003   0.065819   0.063852
    C2_ s       0.000363   0.000315   0.999552   0.999942   0.141015   0.136057
    C2_ p       0.000383   0.000356   0.000003   0.000003   0.065820   0.063852
    O3_ s       0.998548   1.000031   0.000019  -0.000079   0.722083   0.727728
    O3_ p       0.000002   0.000002  -0.000181  -0.000270   0.069502   0.070541
    O4_ s       0.999959   0.998619   0.000019  -0.000079   0.722084   0.727728
    O4_ p       0.000002   0.000002  -0.000181  -0.000270   0.069502   0.070541
    H5_ s      -0.000000   0.000001   0.000271   0.000235   0.000790   0.000911
    H6_ s      -0.000000   0.000001   0.000271   0.000235   0.000790   0.000911
    H7_ s      -0.000000   0.000001   0.000271   0.000235   0.000790   0.000911
    H8_ s      -0.000000   0.000001   0.000271   0.000235   0.000790   0.000911
 
   ao class       7a         8a         9a        10a        11a        12a  
    C1_ s       0.441915   0.446169   0.000000   0.000000   0.021159   0.020669
    C1_ p       0.089058   0.085114   0.539729   0.544347   0.227418   0.231284
    C2_ s       0.441915   0.446169   0.000000   0.000000   0.021159   0.020669
    C2_ p       0.089058   0.085114   0.539729   0.544347   0.227418   0.231284
    O3_ s       0.125827   0.127719   0.000000   0.000000   0.119255   0.119271
    O3_ p       0.087077   0.088916   0.059125   0.059718   0.532784   0.528198
    O4_ s       0.125827   0.127719   0.000000   0.000000   0.119255   0.119271
    O4_ p       0.087077   0.088916   0.059125   0.059718   0.532784   0.528198
    H5_ s       0.128062   0.126041   0.200573   0.197968   0.049692   0.050289
    H6_ s       0.128062   0.126041   0.200573   0.197968   0.049692   0.050289
    H7_ s       0.128062   0.126041   0.200573   0.197968   0.049692   0.050289
    H8_ s       0.128062   0.126041   0.200573   0.197968   0.049692   0.050289
 
   ao class      13a        14a        15a        16a        17a        18a  
    C1_ s       0.000151   0.000199   0.000000  -0.000000   0.000007  -0.000023
    C1_ p       0.340131   0.321063   0.002752   0.003037   0.150942   0.131695
    C2_ s       0.000151   0.000199   0.000000   0.000000   0.000007  -0.000023
    C2_ p       0.340131   0.321063   0.002752   0.003037   0.150942   0.131695
    O3_ s      -0.000032   0.000003  -0.000000   0.000000   0.000001   0.000001
    O3_ p       0.613537   0.629187   0.742108   0.741350   0.080908   0.065963
    O4_ s      -0.000032   0.000003   0.000000  -0.000000   0.000001   0.000001
    O4_ p       0.613537   0.629187   0.742108   0.741350   0.080908   0.065963
    H5_ s       0.000020   0.000015   0.044630   0.043794   0.000026   0.000025
    H6_ s       0.000020   0.000015   0.044630   0.043794   0.000026   0.000025
    H7_ s       0.000020   0.000015   0.044630   0.043794   0.000026   0.000025
    H8_ s       0.000020   0.000015   0.044630   0.043794   0.000026   0.000025
 
   ao class      19a        20a        21a        22a        23a        24a  
 
   ao class      25a        26a        27a        28a        29a        30a  
 
   ao class      31a        32a        33a        34a        35a        36a  
 
   ao class      37a        38a        39a        40a        41a        42a  
 
   ao class      43a        44a  


                        gross atomic populations
     ao           C1_        C2_        O3_        O4_        H5_        H6_
      s         3.207490   3.207490   3.940375   3.940375   0.843340   0.843340
      p         2.796986   2.796986   4.368468   4.368468   0.000000   0.000000
    total       6.004476   6.004476   8.308843   8.308843   0.843340   0.843340
 
 
     ao           H7_        H8_
      s         0.843340   0.843340
    total       0.843340   0.843340
 

 Total number of electrons:   32.00000000

 !timer: mcscf                           cpu_time=     0.479 walltime=     5.235
 *** cpu_time / walltime =      0.091
 bummer (warning):timer: cpu_time << walltime.  If possible, increase core memory. event =1
