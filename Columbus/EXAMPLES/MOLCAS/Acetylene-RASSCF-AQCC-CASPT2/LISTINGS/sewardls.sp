License is going to expire in 85 days on Saturday December 18th, 2010
Everything is fine. Going to run MOLCAS!
                                                                                                   
                                              ^^^^^            M O L C A S                         
                                             ^^^^^^^           version 7.5 patchlevel 565          
                               ^^^^^         ^^^^^^^                                               
                              ^^^^^^^        ^^^ ^^^                                               
                              ^^^^^^^       ^^^^ ^^^                                               
                              ^^^ ^^^       ^^^^ ^^^                                               
                              ^^^ ^^^^      ^^^  ^^^                                               
                              ^^^  ^^ ^^^^^  ^^  ^^^^                                              
                             ^^^^      ^^^^   ^   ^^^                                              
                             ^   ^^^   ^^^^   ^^^^  ^                                              
                            ^   ^^^^    ^^    ^^^^   ^                                             
                        ^^^^^   ^^^^^   ^^   ^^^^^   ^^^^^                                         
                     ^^^^^^^^   ^^^^^        ^^^^^    ^^^^^^^                                      
                 ^^^^^^^^^^^    ^^^^^^      ^^^^^^^   ^^^^^^^^^^^                                  
               ^^^^^^^^^^^^^   ^^^^^^^^^^^^^^^^^^^^   ^^^^^^^^^^^^^                                
               ^^^^^^^^^^^^^   ^^^^             ^^^   ^^^      ^^^^                                
               ^^^^^^^^^^^^^                          ^^^      ^^^^                                
               ^^^^^^^^^^^^^       ^^^^^^^^^^^^        ^^      ^^^^                                
               ^^^^^^^^^^^^      ^^^^^^^^^^^^^^^^      ^^      ^^^^                                
               ^^^^^^^^^^^^    ^^^^^^^^^^^^^^^^^^^^    ^^      ^^^^                                
               ^^^^^^^^^^^^    ^^^^^^^^^^^^^^^^^^^^    ^^      ^^^^    ^^^^^     ^^^    ^^^^^^     
               ^^^^^^^^^^^^   ^^^^^^^^^^^^^^^^^^^^^^   ^^      ^^^^   ^^^^^^^    ^^^   ^^^  ^^^    
               ^^^^^^^^^^^    ^^^^^^^^^^^^^^^^^^^^^^   ^       ^^^^  ^^^    ^^   ^^^   ^^    ^^    
               ^^^^^^^^^^^     ^^^^^^^^^^^^^^^^^^^^            ^^^^  ^^         ^^ ^^  ^^^^^       
               ^^^^^^^^^^      ^^^^^^^^^^^^^^^^^^^^        ^   ^^^^  ^^         ^^ ^^   ^^^^^^     
               ^^^^^^^^          ^^^^^^^^^^^^^^^^          ^   ^^^^  ^^        ^^^^^^^     ^^^^    
               ^^^^^^      ^^^     ^^^^^^^^^^^^     ^      ^   ^^^^  ^^^   ^^^ ^^^^^^^ ^^    ^^    
                         ^^^^^^                    ^^      ^   ^^^^   ^^^^^^^  ^^   ^^ ^^^  ^^^    
                      ^^^^^^^^^^^^             ^^^^^^      ^           ^^^^^  ^^     ^^ ^^^^^^     
               ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                                
                 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                                  
                     ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                                      
                        ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                                         
                            ^^^^^^^^^^^^^^^^^^^^^^^^^^                                             
                               ^^^^^^^^^^^^^^^^^^^^                                                
                                   ^^^^^^^^^^^^                                                    
                                       ^^^^^^                                                      

                           Copyright, all rights, reserved:                                        
                         Permission is hereby granted to use                                       
                but not to reproduce or distribute any part of this                                
             program. The use is restricted to research purposes only.                             
                            Lund University Sweden, 2008.                                          
                                                                                                   
 For the author list and the recommended citation consult section 1.5 of the MOLCAS user's guide.  



   -------------------------------------------------------------------
  |                                                                   
  |   Project         = molcas
  |   Submitted from  = /local/Columbus_C70_beta/test/MOLCAS/Acetylene-RASSCF-AQCC-CASPT2/WORK
  |   Scratch area    = /local/Columbus_C70_beta/test/MOLCAS/Acetylene-RASSCF-AQCC-CASPT2/WORK
  |   Save outputs to = WORKDIR
  |                                                                   
  |   Scratch area is NOT empty
  |                                                                   
  |                                                                   
   -------------------------------------------------------------------
 
 
()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()
                                  MOLCAS executing module SEWARD with 250 MB of memory                                  
                                              at 12:32:13 Fri Sep 24 2010                                               
()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()
 
 
               SEWARD will generate:
                  Multipole Moment integrals up to order  2
                  Kinetic Energy integrals
                  Nuclear Attraction integrals (point charge)
                  One-Electron Hamiltonian integrals
                  Two-Electron Repulsion integrals
 
 Title:
                                     Acetylene Monomer D2h                              
 
 
                   Integrals are discarded if absolute value <: 0.10E-12
                   Integral cutoff threshold is set to       <: 0.10E-11
 
 
++      Symmetry information:
      ---------------------
 
 

                    --- Group Generators ---
                    Reflection in the yz-plane  
                    Reflection in the xz-plane  
                    Reflection in the xy-plane  
 
 
                    Character Table for D2h
 
                             E   s(yz) s(xz) C2(z) s(xy) C2(y) C2(x)   i  
                    ag       1     1     1     1     1     1     1     1  
                    b3u      1    -1     1    -1     1    -1     1    -1  x
                    b2u      1     1    -1    -1     1     1    -1    -1  y
                    b1g      1    -1    -1     1     1    -1    -1     1  xy, Rz
                    b1u      1     1     1     1    -1    -1    -1    -1  z
                    b2g      1    -1     1    -1    -1     1    -1     1  xz, Ry
                    b3g      1     1    -1    -1    -1    -1     1     1  yz, Rx
                    au       1    -1    -1     1    -1     1     1    -1  I
--
 
                    Symmetry adaptation a la MOLECULE.
 
 
      Basis set label:H.CC-PVTZ.DUNNING.5S2P1D.3S2P1D......                                           
 
      Valence basis set:
      ==================
      Associated Effective Charge  1.000000 au
      Associated Actual Charge     1.000000 au
      Nuclear Model: Point charge
 
 
      Shell  nPrim  nBasis  Cartesian Spherical Contaminant
         s       5       3        X                  
         p       2       2        X                  
         d       1       1                 X         
      Basis set label:C.CC-PVTZ.DUNNING.10S5P2D1F.4S3P2D1F......                                      
 
      Valence basis set:
      ==================
      Associated Effective Charge  6.000000 au
      Associated Actual Charge     6.000000 au
      Nuclear Model: Point charge
 
 
      Shell  nPrim  nBasis  Cartesian Spherical Contaminant
         s      10       4        X                  
         p       5       3        X                  
         d       2       2                 X         
         f       1       1                 X         
 
 
++       Molecular structure info:
       -------------------------
 
 
                    ************************************************ 
                    **** Cartesian Coordinates / Bohr, Angstrom **** 
                    ************************************************ 
 
     Center  Label                x              y              z                     x              y              z
        1      H              0.000000       0.000000       2.139200              0.000000       0.000000       1.132016
        2      H              0.000000       0.000000      -2.139200              0.000000       0.000000      -1.132016
        3      C              0.000000       0.000000       1.135700              0.000000       0.000000       0.600987
        4      C              0.000000       0.000000      -1.135700              0.000000       0.000000      -0.600987
 
                    *************************************** 
                    *    InterNuclear Distances / Bohr    * 
                    *************************************** 
 
             1 H             2 H             3 C             4 C   
    1 H      0.000000
    2 H      4.278400        0.000000
    3 C      1.003500        3.274900        0.000000
    4 C      3.274900        1.003500        2.271400        0.000000
 
                    ******************************************* 
                    *    InterNuclear Distances / Angstrom    * 
                    ******************************************* 
 
             1 H             2 H             3 C             4 C   
    1 H      0.000000
    2 H      2.264032        0.000000
    3 C      0.531029        1.733002        0.000000
    4 C      1.733002        0.531029        1.201973        0.000000
 
                    ************************************** 
                    *    Valence Bond Angles / Degree    * 
                    ************************************** 
                          Atom centers          Phi       
                      1 H      3 C      4 C      180.00
                      2 H      4 C      3 C      180.00
--
 
      Basis set specifications :
      Symmetry species        ag  b3u b2u b1g b1u b2g b3g au 
      Basis functions          20  10  10   4  20  10  10   4
 
 
            Nuclear Potential Energy             31.70536871 au
 
--- Stop Module: seward at Fri Sep 24 12:32:14 2010 /rc=0 ---

     Happy landing!

--- Stop Module: auto at Fr Sep 24 12:32:14 2010 /rc=0 ---
