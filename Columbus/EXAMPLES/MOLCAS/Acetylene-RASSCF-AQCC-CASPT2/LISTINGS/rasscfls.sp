License is going to expire in 85 days on Saturday December 18th, 2010
Everything is fine. Going to run MOLCAS!
                                                                                                   
                                              ^^^^^            M O L C A S                         
                                             ^^^^^^^           version 7.5 patchlevel 565          
                               ^^^^^         ^^^^^^^                                               
                              ^^^^^^^        ^^^ ^^^                                               
                              ^^^^^^^       ^^^^ ^^^                                               
                              ^^^ ^^^       ^^^^ ^^^                                               
                              ^^^ ^^^^      ^^^  ^^^                                               
                              ^^^  ^^ ^^^^^  ^^  ^^^^                                              
                             ^^^^      ^^^^   ^   ^^^                                              
                             ^   ^^^   ^^^^   ^^^^  ^                                              
                            ^   ^^^^    ^^    ^^^^   ^                                             
                        ^^^^^   ^^^^^   ^^   ^^^^^   ^^^^^                                         
                     ^^^^^^^^   ^^^^^        ^^^^^    ^^^^^^^                                      
                 ^^^^^^^^^^^    ^^^^^^      ^^^^^^^   ^^^^^^^^^^^                                  
               ^^^^^^^^^^^^^   ^^^^^^^^^^^^^^^^^^^^   ^^^^^^^^^^^^^                                
               ^^^^^^^^^^^^^   ^^^^             ^^^   ^^^      ^^^^                                
               ^^^^^^^^^^^^^                          ^^^      ^^^^                                
               ^^^^^^^^^^^^^       ^^^^^^^^^^^^        ^^      ^^^^                                
               ^^^^^^^^^^^^      ^^^^^^^^^^^^^^^^      ^^      ^^^^                                
               ^^^^^^^^^^^^    ^^^^^^^^^^^^^^^^^^^^    ^^      ^^^^                                
               ^^^^^^^^^^^^    ^^^^^^^^^^^^^^^^^^^^    ^^      ^^^^    ^^^^^     ^^^    ^^^^^^     
               ^^^^^^^^^^^^   ^^^^^^^^^^^^^^^^^^^^^^   ^^      ^^^^   ^^^^^^^    ^^^   ^^^  ^^^    
               ^^^^^^^^^^^    ^^^^^^^^^^^^^^^^^^^^^^   ^       ^^^^  ^^^    ^^   ^^^   ^^    ^^    
               ^^^^^^^^^^^     ^^^^^^^^^^^^^^^^^^^^            ^^^^  ^^         ^^ ^^  ^^^^^       
               ^^^^^^^^^^      ^^^^^^^^^^^^^^^^^^^^        ^   ^^^^  ^^         ^^ ^^   ^^^^^^     
               ^^^^^^^^          ^^^^^^^^^^^^^^^^          ^   ^^^^  ^^        ^^^^^^^     ^^^^    
               ^^^^^^      ^^^     ^^^^^^^^^^^^     ^      ^   ^^^^  ^^^   ^^^ ^^^^^^^ ^^    ^^    
                         ^^^^^^                    ^^      ^   ^^^^   ^^^^^^^  ^^   ^^ ^^^  ^^^    
                      ^^^^^^^^^^^^             ^^^^^^      ^           ^^^^^  ^^     ^^ ^^^^^^     
               ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                                
                 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                                  
                     ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                                      
                        ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                                         
                            ^^^^^^^^^^^^^^^^^^^^^^^^^^                                             
                               ^^^^^^^^^^^^^^^^^^^^                                                
                                   ^^^^^^^^^^^^                                                    
                                       ^^^^^^                                                      

                           Copyright, all rights, reserved:                                        
                         Permission is hereby granted to use                                       
                but not to reproduce or distribute any part of this                                
             program. The use is restricted to research purposes only.                             
                            Lund University Sweden, 2008.                                          
                                                                                                   
 For the author list and the recommended citation consult section 1.5 of the MOLCAS user's guide.  



   -------------------------------------------------------------------
  |                                                                   
  |   Project         = molcas
  |   Submitted from  = /local/Columbus_C70_beta/test/MOLCAS/Acetylene-RASSCF-AQCC-CASPT2/WORK
  |   Scratch area    = /local/Columbus_C70_beta/test/MOLCAS/Acetylene-RASSCF-AQCC-CASPT2/WORK
  |   Save outputs to = WORKDIR
  |                                                                   
  |   Scratch area is NOT empty
  |                                                                   
  |                                                                   
   -------------------------------------------------------------------
 
 
()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()
                                  MOLCAS executing module RASSCF with 250 MB of memory                                  
                                              at 12:32:14 Fri Sep 24 2010                                               
()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()
 
 
 
      Header of the ONEINT file:
      --------------------------
      Acetylene Monomer D2h                                                                                                   
      Integrals generated by seward 4.2.0  , Fri Sep 24 12:32:13 2010                                                         
 
 
      OrdInt status: non-squared
 
 
      Cartesian coordinates in Angstrom:
      -----------------------------------------
      No.  Label     X         Y         Z     
      -----------------------------------------
       1   H        0.00000   0.00000   1.13202
       2   C        0.00000   0.00000   0.60099
       3   H        0.00000   0.00000  -1.13202
       4   C        0.00000   0.00000  -0.60099
      -----------------------------------------
      Nuclear repulsion energy =   31.705369
 
 
 
      Wave function specifications:
      -----------------------------
 
      Number of closed shell electrons           4
      Number of electrons in active shells      10
      Max number of holes in RAS1 space          0
      Max nr of electrons in RAS3 space          0
      Number of inactive orbitals                2
      Number of active orbitals                  8
      Number of secondary orbitals              78
      Spin quantum number                      0.0
      State symmetry                             1
 
 
      Orbital specifications:
      -----------------------
 
      Symmetry species                           1   2   3   4   5   6   7   8
                                                ag b3u b2u b1g b1u b2g b3g  au
      Frozen orbitals                            0   0   0   0   0   0   0   0
      Inactive orbitals                          1   0   0   0   1   0   0   0
      Active orbitals                            2   1   1   0   2   1   1   0
      RAS1 orbitals                              0   0   0   0   0   0   0   0
      RAS2 orbitals                              2   1   1   0   2   1   1   0
      RAS3 orbitals                              0   0   0   0   0   0   0   0
      Secondary orbitals                        17   9   9   4  17   9   9   4
      Deleted orbitals                           0   0   0   0   0   0   0   0
      Number of basis functions                 20  10  10   4  20  10  10   4
 
 
      CI expansion specifications:
      ----------------------------
 
      Number of configuration state fnc.       176
      Number of determinants                   226
      Number of root(s) required                 1
      Root chosen for geometry opt.              1
      CI root used                               1
      highest root included in the CI            1
      max. size of the explicit Hamiltonian    100
 
 
      Optimization specifications:
      ----------------------------
 
      RASSCF algorithm: Conventional
      Maximum number of macro iterations        50
      Maximum number of SX iterations           25
      Threshold for RASSCF energy            0.100E-08
      Threshold for max MO rotation          0.100E-05
      Threshold for max BLB element          0.100E-05
      Level shift parameter                  0.500E+00
      Make Quasi-Newton update
      The MO-coefficients are taken from the file:
      INPORB                                                                                                                          
      Title:mocoef_scfpq                                                                   
 
      Total molecular charge    0.00
 
      ************************************************************************************************************************
      *                                                                                                                      *
      *                                            Wave function  control section                                            *
      *                                                                                                                      *
      ************************************************************************************************************************
 
 
                          RASSCF iterations: Energy and convergence statistics
                          ----------------------------------------------------
 
      Iter CI   SX   CI       RASSCF       Energy    max BLB   max BLB  max ROT   Level Ln srch  Step   QN    Time(min)
          iter iter root      energy       change    element    value    param    shift minimum  type update     CPU
 ###############################################################################
 ###############################################################################
 ###                                                                         ###
 ###                                                                         ###
 ###    Large orbital rotation.                                              ###
 ###                                                                         ###
 ###                                                                         ###
 ###############################################################################
 ###############################################################################
      Molecular orbital   3 of symmetry 5 MO space 2  weight is    0.082243
        1   1   25    1   -75.41925533    0.00E+00    1   2 6 -0.26E-01*  0.54E+00*  0.00   0.00     SX     NO      0.00
        2   4   11    1   -75.48025249   -0.61E-01*   1   2 2  0.19E-01* -0.41E+00*  0.00   0.00     SX     NO      0.00
        3   3   10    1   -75.49665968   -0.16E-01*   3  12 1 -0.88E-02* -0.45E-01*  0.00   0.00     SX     NO      0.00
        4   3   10    1   -75.49792964   -0.13E-02*   3   6 1  0.27E-02* -0.20E-01*  0.00   0.00     SX     NO      0.00
        5   3    9    1   -75.49813166   -0.20E-03*   3   6 1  0.13E-02* -0.13E-01*  0.00   1.68     LS    YES      0.00
        6   3    9    1   -75.49817137   -0.40E-04*   3  10 1 -0.64E-03*  0.20E-02*  0.00   1.05     QN    YES      0.00
        7   3    8    1   -75.49817328   -0.19E-05*   2   9 1  0.17E-03* -0.52E-03*  0.00   1.00     QN    YES      0.00
        8   3    6    1   -75.49817340   -0.12E-06*   1   3 5 -0.22E-04*  0.68E-04*  0.00   0.99     QN    YES      0.00
        9   1    4    1   -75.49817340   -0.21E-08*   3   6 1 -0.35E-05* -0.14E-04*  0.00   1.19     QN    YES      0.00
       10   1    3    1   -75.49817340   -0.11E-09    1   3 5  0.89E-06   0.10E-05*  0.00   1.01     QN    YES      0.00
       11   1    2    1   -75.49817340   -0.15E-11    3   8 1  0.12E-06   0.23E-06   0.00   1.08     QN    YES      0.00
      Convergence after 11 iterations
       12   1    2    1   -75.49817340    0.43E-13    1   5 1 -0.10E-06   0.23E-06   0.00   1.08     QN    YES      0.00
 
      ************************************************************************************************************************
                                                      Wave function printout:
                       occupation of active orbitals, and spin coupling of open shells (u,d: Spin up or down)
      ************************************************************************************************************************
 
      Note: transformation to natural orbitals
      has been made, which may change the order of the CSFs.
 
      printout of CI-coefficients larger than  0.05 for root  1
      energy=     -75.498173
      conf/sym  11 2 3 55 6 7     Coeff  Weight
             1  22 2 2 20 0 0   0.96233 0.92607
            10  22 2 0 20 0 2  -0.12849 0.01651
            15  22 0 2 20 2 0  -0.12849 0.01651
            46  2u d 2 2u d 0   0.05567 0.00310
            54  2u 2 d 2u 0 d  -0.05567 0.00310
            62  22 u d 20 u d   0.11503 0.01323
            85  22 u u 20 d d  -0.08302 0.00689
 
      Natural orbitals and occupation numbers for root  1
      sym 1:   1.997088   1.982373
      sym 2:   1.934544
      sym 3:   1.934544
      sym 5:   1.997800   0.020136
      sym 6:   0.066757
      sym 7:   0.066757
 
      ************************************************************************************************************************
      *                                                                                                                      *
      *                                                    Final results                                                    *
      *                                                                                                                      *
      ************************************************************************************************************************
 
 
      Wave function specifications:
      -----------------------------
 
      Number of closed shell electrons           4
      Number of electrons in active shells      10
      Max number of holes in RAS1 space          0
      Max nr of electrons in RAS3 space          0
      Number of inactive orbitals                2
      Number of active orbitals                  8
      Number of secondary orbitals              78
      Spin quantum number                      0.0
      State symmetry                             1
 
 
      Orbital specifications:
      -----------------------
 
      Symmetry species                           1   2   3   4   5   6   7   8
                                                ag b3u b2u b1g b1u b2g b3g  au
      Frozen orbitals                            0   0   0   0   0   0   0   0
      Inactive orbitals                          1   0   0   0   1   0   0   0
      Active orbitals                            2   1   1   0   2   1   1   0
      RAS1 orbitals                              0   0   0   0   0   0   0   0
      RAS2 orbitals                              2   1   1   0   2   1   1   0
      RAS3 orbitals                              0   0   0   0   0   0   0   0
      Secondary orbitals                        17   9   9   4  17   9   9   4
      Deleted orbitals                           0   0   0   0   0   0   0   0
      Number of basis functions                 20  10  10   4  20  10  10   4
 
 
      CI expansion specifications:
      ----------------------------
 
      Number of configuration state fnc.       176
      Number of determinants                   226
      Number of root(s) required                 1
      CI root used                               1
      highest root included in the CI            1
      Root passed to geometry opt.               1
 
 
      Final optimization conditions:
      ------------------------------
 
      Average CI energy                             -75.49817340
      RASSCF energy for state  1                    -75.49817340
      Super-CI energy                                 0.00000000
      RASSCF energy change                            0.00000000
      Max change in MO coefficients              -0.156E-05
      Max non-diagonal density matrix element     0.233E-06
      Maximum BLB matrix element                 -0.104E-06
      (orbital pair   1,   5 in symmetry   1)
      Norm of electronic gradient            0.174E-05
 
 
      Final state energy(ies):
      ------------------------
 
::    RASSCF root number  1 Total energy =        -75.49817340                                                          
 
 
++       Molecular orbitals:
      Molecular orbitals:
      -------------------
 
      All orbitals are eigenfunctions of the PT2 Fock matrix
 
 
 
 
      Molecular orbitals for symmetry species 1: ag 
 
 
      Orbital          1         2         3         4
      Energy    -11.1555   -1.1256   -0.8625    0.1421
 
    1 H   1s     -0.0557    0.7728   -0.6849    0.2920
    2 H   *s      0.0335   -0.4381    0.2471   -1.3183
    3 H   *s      0.0234   -0.3543    0.2705   -5.0999
    4 H   *pz    -0.0012   -0.0209    0.0150    0.0037
    5 H   *pz    -0.0028    0.0261   -0.0003    0.0458
    6 H   *d0     0.0015    0.0086   -0.0054   -0.0026
    7 H   *d2+    0.0000    0.0000    0.0000    0.0000
    8 C   1s      0.7024    0.0347   -0.0172    0.0170
    9 C   2s     -0.0463    0.6999    0.2706    0.2492
   10 C   *s      0.0013   -0.0549    0.0325    0.0225
   11 C   *s      0.0064   -0.0827   -0.0817    5.2724
   12 C   2pz    -0.0357    0.1823   -0.7771    0.0970
   13 C   *pz     0.0003   -0.0244    0.1810    0.2571
   14 C   *pz     0.0080   -0.0190    0.2069    1.7556
   15 C   *d0     0.0007    0.0127    0.0121   -0.0122
   16 C   *d0    -0.0026    0.0299   -0.0022    0.2774
   17 C   *d2+    0.0000    0.0000    0.0000    0.0000
   18 C   *d2+    0.0000    0.0000    0.0000    0.0000
   19 C   *f0    -0.0007   -0.0093    0.0024   -0.0178
   20 C   *f2+    0.0000    0.0000    0.0000    0.0000
 
 
 
      Molecular orbitals for symmetry species 2: b3u
 
 
      Orbital          1
      Energy     -0.4061
 
    1 H   *px     0.0152
    2 H   *px     0.0156
    3 H   *d1+   -0.0055
    4 C   2px     0.6449
    5 C   *px    -0.0325
    6 C   *px    -0.0521
    7 C   *d1+   -0.0143
    8 C   *d1+   -0.0189
    9 C   *f1+    0.0074
   10 C   *f3+    0.0000
 
 
 
      Molecular orbitals for symmetry species 3: b2u
 
 
      Orbital          1
      Energy     -0.4061
 
    1 H   *py     0.0152
    2 H   *py     0.0156
    3 H   *d1-   -0.0055
    4 C   2py     0.6449
    5 C   *py    -0.0325
    6 C   *py    -0.0521
    7 C   *d1-   -0.0143
    8 C   *d1-   -0.0189
    9 C   *f3-    0.0000
   10 C   *f1-    0.0074
 
 
 
      Molecular orbitals for symmetry species 5: b1u
 
 
      Orbital          1         2         3
      Energy    -11.1926   -1.0140    0.9691
 
    1 H   1s     -0.0335    0.9330   -0.6327
    2 H   *s      0.0276   -0.2532    0.6789
    3 H   *s      0.0170   -0.3153    0.3922
    4 H   *pz    -0.0021   -0.0223    0.0337
    5 H   *pz    -0.0050   -0.0472   -0.0664
    6 H   *d0     0.0017    0.0099   -0.0217
    7 H   *d2+    0.0000    0.0000    0.0000
    8 C   1s      0.7046    0.0247    0.0484
    9 C   2s     -0.0307    0.5775    0.8780
   10 C   *s     -0.0016   -0.0780    0.1488
   11 C   *s      0.0138    0.0977    0.0363
   12 C   2pz    -0.0178    0.4580   -1.0703
   13 C   *pz    -0.0094   -0.2992   -0.0941
   14 C   *pz    -0.0020   -0.2527    0.1129
   15 C   *d0     0.0022    0.0274   -0.0632
   16 C   *d0     0.0010    0.0789   -0.0326
   17 C   *d2+    0.0000    0.0000    0.0000
   18 C   *d2+    0.0000    0.0000    0.0000
   19 C   *f0     0.0000   -0.0066    0.0187
   20 C   *f2+    0.0000    0.0000    0.0000
 
 
 
      Molecular orbitals for symmetry species 6: b2g
 
 
      Orbital          1
      Energy      0.3105
 
    1 H   *px     0.0416
    2 H   *px     0.0716
    3 H   *d1+   -0.0132
    4 C   2px     0.9868
    5 C   *px    -0.0738
    6 C   *px    -0.2205
    7 C   *d1+   -0.0002
    8 C   *d1+   -0.0404
    9 C   *f1+   -0.0199
   10 C   *f3+    0.0000
 
 
 
      Molecular orbitals for symmetry species 7: b3g
 
 
      Orbital          1
      Energy      0.3105
 
    1 H   *py     0.0416
    2 H   *py     0.0716
    3 H   *d1-   -0.0132
    4 C   2py     0.9868
    5 C   *py    -0.0738
    6 C   *py    -0.2205
    7 C   *d1-   -0.0002
    8 C   *d1-   -0.0404
    9 C   *f3-    0.0000
   10 C   *f1-   -0.0199
--

      Mulliken population Analysis for root number: 1
      -----------------------------------------------
 
 
 
++       Molecular Charges:
      ------------------
 
 
 
      Mulliken charges per center and basis function type
      ---------------------------------------------------
 
               H       C   
      1s     2.2955  1.9915
      2s     0.0000  1.7804
      2px    0.0000  1.0790
      2pz    0.0000  1.6868
      2py    0.0000  1.0790
      *s    -1.5323 -0.2161
      *px    0.0294 -0.1275
      *pz   -0.0046 -1.0090
      *py    0.0294 -0.1275
      *d2+   0.0000  0.0000
      *d1+   0.0032  0.0138
      *d0   -0.0025  0.0099
      *d1-   0.0032  0.0138
      *d2-   0.0000  0.0000
      *f3+   0.0000  0.0000
      *f2+   0.0000  0.0000
      *f1+   0.0000  0.0028
      *f0    0.0000 -0.0009
      *f1-   0.0000  0.0028
      *f2-   0.0000  0.0000
      *f3-   0.0000  0.0000
      Total  0.8214  6.1786
 
      N-E    0.1786 -0.1786
 
      Total electronic charge=   14.000000
 
      Total            charge=    0.000000
--
 

      Expectation values of various properties for root number: 1
      -----------------------------------------------------------
 
 
 
++       Moleculer Properties:
      ---------------------
 
 
      Dipole Moment (Debye):                                                          
      Origin of the operator (Ang)=    0.0000    0.0000    0.0000
                     X=    0.0000               Y=    0.0000               Z=    0.0000           Total=    0.0000
      Quadrupole Moment (Debye*Ang):                                                  
      Origin of the operator (Ang)=    0.0000    0.0000    0.0000
                    XX=  -11.9601              XY=    0.0000              XZ=    0.0000              YY=  -11.9601
                    YZ=    0.0000              ZZ=   -8.6498
      In traceless form (Debye*Ang)
                    XX=   -1.6551              XY=    0.0000              XZ=    0.0000              YY=   -1.6551
                    YZ=    0.0000              ZZ=    3.3103
--
      Canonical orbitals are written to the RASORB           file
      Natural orbitals for root   1 are written to the RASORB.1         file
      Spin density orbitals for root   1 are written to the SPDORB.1         file
 
--- Stop Module: rasscf at Fri Sep 24 12:32:16 2010 /rc=0 ---

     Happy landing!

--- Stop Module: auto at Fr Sep 24 12:32:16 2010 /rc=0 ---
