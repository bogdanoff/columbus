 
 program cidrt 5.9  

 distinct row table construction, reference csf selection, and internal
 walk selection for multireference single- and double-excitation
configuration interaction.

 references:  r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).
              h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. symp. 15, 91 (1981).

 based on the initial version by  Ron Shepard

 extended for spin-orbit CI calculations ( Russ Pitzer, OSU)

 and large active spaces (Thomas Müller, FZ Juelich)

 version date: 16-jul-04


 This Version of Program CIDRT is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIDRT       **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 workspace allocation parameters: lencor=  32768000 mem1=         0 ifirst=         1
 expanded "keystrokes" are being written to file:
 /local/Columbus_C70_beta/test/MOLCAS/Acetylene-RASSCF-AQCC-CASPT2/WORK/cidrtky  
 Spin-Orbit CI Calculation?(y,[n]) Spin-Free Calculation
 
 input the spin multiplicity [  0]: spin multiplicity, smult            :   1    singlet 
 input the total number of electrons [  0]: total number of electrons, nelt     :    14
 input the number of irreps (1:8) [  0]: point group dimension, nsym         :     8
 enter symmetry labels:(y,[n]) enter 8 labels (a4):
 enter symmetry label, default=   1
 enter symmetry label, default=   2
 enter symmetry label, default=   3
 enter symmetry label, default=   4
 enter symmetry label, default=   5
 enter symmetry label, default=   6
 enter symmetry label, default=   7
 enter symmetry label, default=   8
 symmetry labels: (symmetry, slabel)
 ( 1,  ag ) ( 2,  b3u) ( 3,  b2u) ( 4,  b1g) ( 5,  b1u) ( 6,  b2g) ( 7,  b3g) ( 8,  au ) 
 input nmpsy(*):
 nmpsy(*)=        20  10  10   4  20  10  10   4
 
   symmetry block summary
 block(*)=         1   2   3   4   5   6   7   8
 slabel(*)=      ag  b3u b2u b1g b1u b2g b3g au 
 nmpsy(*)=        20  10  10   4  20  10  10   4
 
 total molecular orbitals            :    88
 input the molecular spatial symmetry (irrep 1:nsym) [  0]: state spatial symmetry label        :  ag 
 
 input the frozen core orbitals (sym(i),rmo(i),i=1,nfct):
 total frozen core orbitals, nfct    :     0
 no frozen core orbitals entered
 
 number of frozen core orbitals      :     0
 number of frozen core electrons     :     0
 number of internal electrons        :    14
 
 input the frozen virtual orbitals (sym(i),rmo(i),i=1,nfvt):
 total frozen virtual orbitals, nfvt :     0

 no frozen virtual orbitals entered
 
 input the internal orbitals (sym(i),rmo(i),i=1,niot):
 niot                                :    10
 
 modrt(*)=         1  45   2   3  21  31  46  47  65  75
 slabel(*)=      ag  b1u ag  ag  b3u b2u b1u b1u b2g b3g
 
 total number of orbitals            :    88
 number of frozen core orbitals      :     0
 number of frozen virtual orbitals   :     0
 number of internal orbitals         :    10
 number of external orbitals         :    78
 
 orbital-to-level mapping vector
 map(*)=          79  81  82   1   2   3   4   5   6   7   8   9  10  11  12
                  13  14  15  16  17  83  18  19  20  21  22  23  24  25  26
                  84  27  28  29  30  31  32  33  34  35  36  37  38  39  80
                  85  86  40  41  42  43  44  45  46  47  48  49  50  51  52
                  53  54  55  56  87  57  58  59  60  61  62  63  64  65  88
                  66  67  68  69  70  71  72  73  74  75  76  77  78
 
 input the number of ref-csf doubly-occupied orbitals [  0]: (ref) doubly-occupied orbitals      :     2
 
 no. of internal orbitals            :    10
 no. of doubly-occ. (ref) orbitals   :     2
 no. active (ref) orbitals           :     8
 no. of active electrons             :    10
 
 input the active-orbital, active-electron occmnr(*):
   2  3 21 31 46 47 65 75
 input the active-orbital, active-electron occmxr(*):
   2  3 21 31 46 47 65 75
 
 actmo(*) =        2   3  21  31  46  47  65  75
 occmnr(*)=        0   0   0   0   0   0   0  10
 occmxr(*)=       10  10  10  10  10  10  10  10
 reference csf cumulative electron occupations:
 modrt(*)=         1  45   2   3  21  31  46  47  65  75
 occmnr(*)=        2   4   4   4   4   4   4   4   4  14
 occmxr(*)=        2   4  14  14  14  14  14  14  14  14
 
 input the active-orbital bminr(*):
   2  3 21 31 46 47 65 75
 input the active-orbital bmaxr(*):
   2  3 21 31 46 47 65 75
 reference csf b-value constraints:
 modrt(*)=         1  45   2   3  21  31  46  47  65  75
 bminr(*)=         0   0   0   0   0   0   0   0   0   0
 bmaxr(*)=         0   0  10  10  10  10  10  10  10  10
 input the active orbital smaskr(*):
   2  3 21 31 46 47 65 75
 modrt:smaskr=
   1:1000  45:1000   2:1111   3:1111  21:1111  31:1111  46:1111  47:1111
  65:1111  75:1111
 
 input the maximum excitation level from the reference csfs [  2]: maximum excitation from ref. csfs:  :     2
 number of internal electrons:       :    14
 
 input the internal-orbital mrsdci occmin(*):
   1 45  2  3 21 31 46 47 65 75
 input the internal-orbital mrsdci occmax(*):
   1 45  2  3 21 31 46 47 65 75
 mrsdci csf cumulative electron occupations:
 modrt(*)=         1  45   2   3  21  31  46  47  65  75
 occmin(*)=        0   0   0   0   0   0   0   0   0  12
 occmax(*)=       14  14  14  14  14  14  14  14  14  14
 
 input the internal-orbital mrsdci bmin(*):
   1 45  2  3 21 31 46 47 65 75
 input the internal-orbital mrsdci bmax(*):
   1 45  2  3 21 31 46 47 65 75
 mrsdci b-value constraints:
 modrt(*)=         1  45   2   3  21  31  46  47  65  75
 bmin(*)=          0   0   0   0   0   0   0   0   0   0
 bmax(*)=         14  14  14  14  14  14  14  14  14  14
 
 input the internal-orbital smask(*):
   1 45  2  3 21 31 46 47 65 75
 modrt:smask=
   1:1000  45:1000   2:1111   3:1111  21:1111  31:1111  46:1111  47:1111
  65:1111  75:1111
 
 internal orbital summary:
 block(*)=         1   5   1   1   2   3   5   5   6   7
 slabel(*)=      ag  b1u ag  ag  b3u b2u b1u b1u b2g b3g
 rmo(*)=           1   1   2   3   1   1   2   3   1   1
 modrt(*)=         1  45   2   3  21  31  46  47  65  75
 
 reference csf info:
 occmnr(*)=        2   4   4   4   4   4   4   4   4  14
 occmxr(*)=        2   4  14  14  14  14  14  14  14  14
 
 bminr(*)=         0   0   0   0   0   0   0   0   0   0
 bmaxr(*)=         0   0  10  10  10  10  10  10  10  10
 
 
 mrsdci csf info:
 occmin(*)=        0   0   0   0   0   0   0   0   0  12
 occmax(*)=       14  14  14  14  14  14  14  14  14  14
 
 bmin(*)=          0   0   0   0   0   0   0   0   0   0
 bmax(*)=         14  14  14  14  14  14  14  14  14  14
 

 a priori removal of distinct rows:

 input the level, a, and b values for the vertices 
 to be removed (-1/ to end).

 input level, a, and b (-1/ to end):
 no vertices marked for removal
 
 impose generalized interacting space restrictions?(y,[n]) generalized interacting space restrictions will be imposed.
 multp                     0                     0                     0
                     0                     0                     0
                     0                     0                     0
 spnir
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  hmult                     0
 lxyzir                     0                     0                     0
 spnir
  1  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  1  0  0  0  1  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  1  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  1  0  0  0  1  0  0  0  1  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  1  0  0  0  1  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0

 number of rows in the drt :  87
     0 arcs removed due to generalized interacting space restrictions.

 manual arc removal step:


 input the level, a, b, and step values 
 for the arcs to be removed (-1/ to end).

 input the level, a, b, and step (-1/ to end):
 remarc:   0 arcs removed out of   0 specified.

 xbarz=        1176
 xbary=        2352
 xbarx=        2352
 xbarw=        1764
        --------
 nwalk=        7644
 input the range of drt levels to print (l1,l2):
 levprt(*)        -1   0

 reference-csf selection step 1:
 total number of z-walks in the drt, nzwalk=    1176

 input the list of allowed reference symmetries:
 allowed reference symmetries:             1
 allowed reference symmetry labels:      ag 
 keep all of the z-walks as references?(y,[n]) all z-walks are initially deleted.
 
 generate walks while applying reference drt restrictions?([y],n) reference drt restrictions will be imposed on the z-walks.
 
 impose additional orbital-group occupation restrictions?(y,[n]) 
 apply primary reference occupation restrictions?(y,[n]) 
 manually select individual walks?(y,[n])
 step 1 reference csf selection complete.
      176 csfs initially selected from    1176 total walks.

 beginning step-vector based selection.
 enter [internal_orbital_step_vector/disposition] pairs:

 enter internal orbital step vector, (-1/ to end):
   1 45  2  3 21 31 46 47 65 75

 step 2 reference csf selection complete.
      176 csfs currently selected from    1176 total walks.

 beginning numerical walk based selection.
 enter positive walk numbers to add walks,
 negative walk numbers to delete walks, and zero to end:

 input reference walk number (0 to end) [  0]:
 numerical walk-number based selection complete.
      176 reference csfs selected from    1176 total z-walks.
 
 input the reference occupations, mu(*):
 reference occupations:
 mu(*)=            2   2   0   0   0   0   0   0   0   0
 
 interacting space determination:
 checking diagonal loops...
 checking 2-internal loops...
 checking 3-internal loops...
 checking 4-internal loops...
 !timer: limint() required               cpu_time=     0.024 walltime=     0.026
 
 this is an obsolete prompt.(y,[n])
 final mrsdci walk selection step:

 nvalw(*)=     176    2352    2139    1674 nvalwt=    6341

 enter positive walk numbers to add walks,
 negative walk numbers to delete walks, and zero to end.

 input mrsdci walk number (0 to end) [  0]:
 end of manual mrsdci walk selection.
 number added=   0 number removed=   0

 nvalw(*)=     176    2352    2139    1674 nvalwt=    6341


 lprune: l(*,*,*) pruned with nwalk=    7644 nvalwt=    6341
 lprune:  z-drt, nprune=   123
 lprune:  y-drt, nprune=    60
 lprune: wx-drt, nprune=    28

 xbarz=         595
 xbary=        2352
 xbarx=        2352
 xbarw=        1764
        --------
 nwalk=        7063
 levprt(*)        -1   0

 beginning the reference csf index recomputation...

     iref   iwalk  step-vector
   ------  ------  ------------
        1       1  3333333000
        2       2  3333331200
        3       3  3333330300
        4       6  3333330030
        5       8  3333330003
        6       9  3333303300
        7      12  3333303030
        8      14  3333303003
        9      17  3333301230
       10      19  3333301203
       11      23  3333300330
       12      25  3333300303
       13      28  3333300033
       14      33  3333123012
       15      38  3333121212
       16      40  3333121122
       17      44  3333120312
       18      51  3333113022
       19      55  3333112212
       20      57  3333112122
       21      60  3333111222
       22      61  3333110322
       23      64  3333033300
       24      67  3333033030
       25      69  3333033003
       26      72  3333031230
       27      74  3333031203
       28      78  3333030330
       29      80  3333030303
       30      83  3333030033
       31      84  3333003330
       32      86  3333003303
       33      89  3333003033
       34      92  3333001233
       35      93  3333000333
       36      96  3331323102
       37     101  3331321302
       38     106  3331321032
       39     111  3331320132
       40     115  3331313202
       41     118  3331312302
       42     123  3331312032
       43     127  3331310232
       44     130  3331233120
       45     135  3331231320
       46     142  3331231023
       47     147  3331230123
       48     153  3331203123
       49     156  3331201323
       50     159  3331133220
       51     162  3331132320
       52     169  3331132023
       53     173  3331130223
       54     176  3331103223
       55     178  3331102323
       56     183  3331023132
       57     186  3331021332
       58     191  3331013232
       59     193  3331012332
       60     196  3330333300
       61     199  3330333030
       62     201  3330333003
       63     204  3330331230
       64     206  3330331203
       65     210  3330330330
       66     212  3330330303
       67     215  3330330033
       68     216  3330303330
       69     218  3330303303
       70     221  3330303033
       71     224  3330301233
       72     225  3330300333
       73     227  3330123312
       74     236  3330113322
       75     242  3330033330
       76     244  3330033303
       77     247  3330033033
       78     250  3330031233
       79     251  3330030333
       80     252  3330003333
       81     255  3313323102
       82     260  3313321302
       83     265  3313321032
       84     270  3313320132
       85     274  3313313202
       86     277  3313312302
       87     282  3313312032
       88     286  3313310232
       89     289  3313233120
       90     294  3313231320
       91     301  3313231023
       92     306  3313230123
       93     312  3313203123
       94     315  3313201323
       95     318  3313133220
       96     321  3313132320
       97     328  3313132023
       98     332  3313130223
       99     335  3313103223
      100     337  3313102323
      101     342  3313023132
      102     345  3313021332
      103     350  3313013232
      104     352  3313012332
      105     355  3312333300
      106     358  3312333030
      107     360  3312333003
      108     363  3312331230
      109     365  3312331203
      110     369  3312330330
      111     371  3312330303
      112     374  3312330033
      113     375  3312303330
      114     377  3312303303
      115     380  3312303033
      116     383  3312301233
      117     384  3312300333
      118     386  3312123312
      119     395  3312113322
      120     401  3312033330
      121     403  3312033303
      122     406  3312033033
      123     409  3312031233
      124     410  3312030333
      125     411  3312003333
      126     417  3311332230
      127     419  3311332203
      128     432  3311302233
      129     434  3311223312
      130     443  3311213322
      131     449  3311123322
      132     460  3311032233
      133     464  3310323132
      134     467  3310321332
      135     472  3310313232
      136     474  3310312332
      137     481  3310233123
      138     484  3310231323
      139     490  3310133223
      140     492  3310132323
      141     494  3303333300
      142     497  3303333030
      143     499  3303333003
      144     502  3303331230
      145     504  3303331203
      146     508  3303330330
      147     510  3303330303
      148     513  3303330033
      149     514  3303303330
      150     516  3303303303
      151     519  3303303033
      152     522  3303301233
      153     523  3303300333
      154     525  3303123312
      155     534  3303113322
      156     540  3303033330
      157     542  3303033303
      158     545  3303033033
      159     548  3303031233
      160     549  3303030333
      161     550  3303003333
      162     554  3301323132
      163     557  3301321332
      164     562  3301313232
      165     564  3301312332
      166     571  3301233123
      167     574  3301231323
      168     580  3301133223
      169     582  3301132323
      170     584  3300333330
      171     586  3300333303
      172     589  3300333033
      173     592  3300331233
      174     593  3300330333
      175     594  3300303333
      176     595  3300033333
 indx01:   176 elements set in vec01(*)

 beginning the valid upper walk index recomputation...
 indx01:  6341 elements set in vec01(*)

 beginning the final csym(*) computation...

  number of valid internal walks of each symmetry:

       ag      b3u     b2u     b1g     b1u     b2g     b3g     au 
      ----    ----    ----    ----    ----    ----    ----    ----
 z     176       0       0       0       0       0       0       0
 y     308     294     294     280     308     294     294     280
 x     214     278     288     235     300     286     290     248
 w     260     210     212     158     228     214     214     178

 csfs grouped by internal walk symmetry:

       ag      b3u     b2u     b1g     b1u     b2g     b3g     au 
      ----    ----    ----    ----    ----    ----    ----    ----
 z     176       0       0       0       0       0       0       0
 y    5236    2646    2646    1120    5236    2646    2646    1120
 x   91592  105084  108864   70030  140100  108108  109620   73904
 w  131560   79380   80136   47084  106476   80892   80892   53044

 total csf counts:
 z-vertex:      176
 y-vertex:    23296
 x-vertex:   807302
 w-vertex:   659464
           --------
 total:     1490238
 
 input a title card, default=cidrt_title
 title card:
  cidrt_title                                                                   
  
 
 input a drt file name, default=cidrtfl
 drt and indexing arrays will be written to file:
 /local/Columbus_C70_beta/test/MOLCAS/Acetylene-RASSCF-AQCC-CASPT2/WORK/cidrtfl  
 
 write the drt file?([y],n) drt file is being written...
 wrtstr:  ag  b3u b2u b1g b1u b2g b3g au 
nwalk=    7063 cpos=    1512 maxval=    9 cmprfactor=   78.59 %.
nwalk=    7063 cpos=     911 maxval=   99 cmprfactor=   74.20 %.
 compressed with: nwalk=    7063 cpos=    1522 maxval=    9 cmprfactor=   78.45 %.
initial index vector length:      7063
compressed index vector length:      1522reduction:  78.45%
nwalk=     595 cpos=     311 maxval=    9 cmprfactor=   47.73 %.
nwalk=     595 cpos=     309 maxval=   99 cmprfactor=   -3.87 %.
 compressed with: nwalk=     595 cpos=     311 maxval=    9 cmprfactor=   47.73 %.
initial ref vector length:       595
compressed ref vector length:       311reduction:  47.73%
 !timer: cidrt required                  cpu_time=     0.032 walltime=     0.031
