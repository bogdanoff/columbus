 
 program cidrt 5.9  

 distinct row table construction, reference csf selection, and internal
 walk selection for multireference single- and double-excitation
configuration interaction.

 references:  r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).
              h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. symp. 15, 91 (1981).

 based on the initial version by  Ron Shepard

 extended for spin-orbit CI calculations ( Russ Pitzer, OSU)

 and large active spaces (Thomas Müller, FZ Juelich)

 version date: 16-jul-04


 This Version of Program CIDRT is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIDRT       **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 workspace allocation parameters: lencor=  32768000 mem1=         0 ifirst=         1
 expanded "keystrokes" are being written to file:
 cidrtky                                                                         
 Spin-Orbit CI Calculation?(y,[n]) Spin-Free Calculation

 input the spin multiplicity [  0]: spin multiplicity, smult            :   1    singlet 
 input the total number of electrons [  0]: total number of electrons, nelt     :    10
 input the number of irreps (1:8) [  0]: point group dimension, nsym         :     4
 enter symmetry labels:(y,[n]) enter 4 labels (a4):
 enter symmetry label, default=   1
 enter symmetry label, default=   2
 enter symmetry label, default=   3
 enter symmetry label, default=   4
 symmetry labels: (symmetry, slabel)
 ( 1,  a1 ) ( 2,  b1 ) ( 3,  a2 ) ( 4,  b2 ) 
 input nmpsy(*):
 nmpsy(*)=        20  10   4  10

   symmetry block summary
 block(*)=         1   2   3   4
 slabel(*)=      a1  b1  a2  b2 
 nmpsy(*)=        20  10   4  10

 total molecular orbitals            :    44
 input the molecular spatial symmetry (irrep 1:nsym) [  0]: state spatial symmetry label        :  a1 

 input the frozen core orbitals (sym(i),rmo(i),i=1,nfct):
 total frozen core orbitals, nfct    :     0
 no frozen core orbitals entered

 number of frozen core orbitals      :     0
 number of frozen core electrons     :     0
 number of internal electrons        :    10

 input the frozen virtual orbitals (sym(i),rmo(i),i=1,nfvt):
 total frozen virtual orbitals, nfvt :     0

 no frozen virtual orbitals entered

 input the internal orbitals (sym(i),rmo(i),i=1,niot):
 niot                                :     6

 modrt(*)=         1   2   3   4  21  35
 slabel(*)=      a1  a1  a1  a1  b1  b2 

 total number of orbitals            :    44
 number of frozen core orbitals      :     0
 number of frozen virtual orbitals   :     0
 number of internal orbitals         :     6
 number of external orbitals         :    38

 orbital-to-level mapping vector
 map(*)=          39  40  41  42   1   2   3   4   5   6   7   8   9  10  11
                  12  13  14  15  16  43  17  18  19  20  21  22  23  24  25
                  26  27  28  29  44  30  31  32  33  34  35  36  37  38

 input the number of ref-csf doubly-occupied orbitals [  0]: (ref) doubly-occupied orbitals      :     1

 no. of internal orbitals            :     6
 no. of doubly-occ. (ref) orbitals   :     1
 no. active (ref) orbitals           :     5
 no. of active electrons             :     8

 input the active-orbital, active-electron occmnr(*):
   2  3  4 21 35
 input the active-orbital, active-electron occmxr(*):
   2  3  4 21 35

 actmo(*) =        2   3   4  21  35
 occmnr(*)=        0   0   0   0   8
 occmxr(*)=        8   8   8   8   8
 reference csf cumulative electron occupations:
 modrt(*)=         1   2   3   4  21  35
 occmnr(*)=        2   2   2   2   2  10
 occmxr(*)=        2  10  10  10  10  10

 input the active-orbital bminr(*):
   2  3  4 21 35
 input the active-orbital bmaxr(*):
   2  3  4 21 35
 reference csf b-value constraints:
 modrt(*)=         1   2   3   4  21  35
 bminr(*)=         0   0   0   0   0   0
 bmaxr(*)=         0   8   8   8   8   8
 input the active orbital smaskr(*):
   2  3  4 21 35
 modrt:smaskr=
   1:1000   2:1111   3:1111   4:1111  21:1111  35:1111

 input the maximum excitation level from the reference csfs [  2]: maximum excitation from ref. csfs:  :     2
 number of internal electrons:       :    10

 input the internal-orbital mrsdci occmin(*):
   1  2  3  4 21 35
 input the internal-orbital mrsdci occmax(*):
   1  2  3  4 21 35
 mrsdci csf cumulative electron occupations:
 modrt(*)=         1   2   3   4  21  35
 occmin(*)=        0   0   0   0   0   8
 occmax(*)=       10  10  10  10  10  10

 input the internal-orbital mrsdci bmin(*):
   1  2  3  4 21 35
 input the internal-orbital mrsdci bmax(*):
   1  2  3  4 21 35
 mrsdci b-value constraints:
 modrt(*)=         1   2   3   4  21  35
 bmin(*)=          0   0   0   0   0   0
 bmax(*)=         10  10  10  10  10  10

 input the internal-orbital smask(*):
   1  2  3  4 21 35
 modrt:smask=
   1:1000   2:1111   3:1111   4:1111  21:1111  35:1111

 internal orbital summary:
 block(*)=         1   1   1   1   2   4
 slabel(*)=      a1  a1  a1  a1  b1  b2 
 rmo(*)=           1   2   3   4   1   1
 modrt(*)=         1   2   3   4  21  35

 reference csf info:
 occmnr(*)=        2   2   2   2   2  10
 occmxr(*)=        2  10  10  10  10  10

 bminr(*)=         0   0   0   0   0   0
 bmaxr(*)=         0   8   8   8   8   8


 mrsdci csf info:
 occmin(*)=        0   0   0   0   0   8
 occmax(*)=       10  10  10  10  10  10

 bmin(*)=          0   0   0   0   0   0
 bmax(*)=         10  10  10  10  10  10


 a priori removal of distinct rows:

 input the level, a, and b values for the vertices 
 to be removed (-1/ to end).

 input level, a, and b (-1/ to end):
 no vertices marked for removal

 impose generalized interacting space restrictions?(y,[n]) generalized interacting space restrictions will be imposed.
 multp                        0                        0 
                        0                        0                        0 
                        0                        0                        0 
                        0
 spnir
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  hmult                        0
 lxyzir                        0                        0 
                        0
 spnir
  1  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  1  0  0  0  1  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  1  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  1  0  0  0  1  0  0  0  1  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  1  0  0  0  1  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0

 number of rows in the drt :  34
     0 arcs removed due to generalized interacting space restrictions.

 manual arc removal step:


 input the level, a, b, and step values 
 for the arcs to be removed (-1/ to end).

 input the level, a, b, and step (-1/ to end):
 remarc:   0 arcs removed out of   0 specified.

 xbarz=          15
 xbary=          40
 xbarx=          45
 xbarw=          50
        --------
 nwalk=         150
 input the range of drt levels to print (l1,l2):
 levprt(*)        -1   0

 reference-csf selection step 1:
 total number of z-walks in the drt, nzwalk=      15

 input the list of allowed reference symmetries:
 allowed reference symmetries:             1
 allowed reference symmetry labels:      a1 
 keep all of the z-walks as references?(y,[n]) all z-walks are initially deleted.

 generate walks while applying reference drt restrictions?([y],n) reference drt restrictions will be imposed on the z-walks.

 impose additional orbital-group occupation restrictions?(y,[n])
 apply primary reference occupation restrictions?(y,[n])
 manually select individual walks?(y,[n])
 step 1 reference csf selection complete.
        8 csfs initially selected from      15 total walks.

 beginning step-vector based selection.
 enter [internal_orbital_step_vector/disposition] pairs:

 enter internal orbital step vector, (-1/ to end):
   1  2  3  4 21 35

 step 2 reference csf selection complete.
        8 csfs currently selected from      15 total walks.

 beginning numerical walk based selection.
 enter positive walk numbers to add walks,
 negative walk numbers to delete walks, and zero to end:

 input reference walk number (0 to end) [  0]:
 numerical walk-number based selection complete.
        8 reference csfs selected from      15 total z-walks.

 input the reference occupations, mu(*):
 reference occupations:
 mu(*)=            2   0   0   0   0   0

 interacting space determination:
 checking diagonal loops...
 checking 2-internal loops...
 checking 3-internal loops...
 checking 4-internal loops...
 !timer: limint() required               user+sys=     0.000 walltime=     0.000

 this is an obsolete prompt.(y,[n])
 final mrsdci walk selection step:

 nvalw(*)=       8      40      42      47 nvalwt=     137

 enter positive walk numbers to add walks,
 negative walk numbers to delete walks, and zero to end.

 input mrsdci walk number (0 to end) [  0]:
 end of manual mrsdci walk selection.
 number added=   0 number removed=   0

 nvalw(*)=       8      40      42      47 nvalwt=     137


 lprune: l(*,*,*) pruned with nwalk=     150 nvalwt=     137
 lprune:  z-drt, nprune=    51
 lprune:  y-drt, nprune=    30
 lprune: wx-drt, nprune=    12

 xbarz=           8
 xbary=          40
 xbarx=          45
 xbarw=          50
        --------
 nwalk=         143
 levprt(*)        -1   0

 beginning the reference csf index recomputation...

     iref   iwalk  step-vector
   ------  ------  ------------
        1       1  333330
        2       2  333303
        3       3  333033
        4       4  331233
        5       5  330333
        6       6  313233
        7       7  312333
        8       8  303333
 indx01:     8 elements set in vec01(*)

 beginning the valid upper walk index recomputation...
 indx01:   137 elements set in vec01(*)

 beginning the final csym(*) computation...

  number of valid internal walks of each symmetry:

       a1      b1      a2      b2 
      ----    ----    ----    ----
 z       8       0       0       0
 y      14      10       6      10
 x       9      12       9      12
 w      19      11       6      11

 csfs grouped by internal walk symmetry:

       a1      b1      a2      b2 
      ----    ----    ----    ----
 z       8       0       0       0
 y     224      90      24      90
 x    1782    2160    1305    2160
 w    4484    1980     870    1980

 total csf counts:
 z-vertex:        8
 y-vertex:      428
 x-vertex:     7407
 w-vertex:     9314
           --------
 total:       17157

 input a title card, default=cidrt_title
 title card:
  cidrt_title                                                                    

 input a drt file name, default=cidrtfl
 drt and indexing arrays will be written to file:
 cidrtfl                                                                         

 write the drt file?([y],n) drt file is being written...
 wrtstr:  a1  b1  a2  b2 
nwalk=     143 cpos=      25 maxval=    9 cmprfactor=   82.52 %.
nwalk=     143 cpos=      13 maxval=   99 cmprfactor=   81.82 %.
 compressed with: nwalk=     143 cpos=      25 maxval=    9 cmprfactor=   82.52 %.
initial index vector length:       143
compressed index vector length:        25reduction:  82.52%
nwalk=       8 cpos=       1 maxval=    9 cmprfactor=   87.50 %.
nwalk=       8 cpos=       1 maxval=   99 cmprfactor=   75.00 %.
 compressed with: nwalk=       8 cpos=       1 maxval=    9 cmprfactor=   87.50 %.
initial ref vector length:         8
compressed ref vector length:         1reduction:  87.50%
 !timer: cidrt required                  user+sys=     0.000 walltime=     0.000
