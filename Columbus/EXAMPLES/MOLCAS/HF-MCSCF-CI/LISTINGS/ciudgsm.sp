1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=     8)...
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -100.0823305449 -3.5527E-15  2.8280E-01  1.4291E+00  1.0000E-04

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  1  1   -100.0823305449 -3.5527E-15  2.8280E-01  1.4291E+00  1.0000E-04

 from bk iterations: iconv=   1

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -100.3156785027  2.3335E-01  1.2824E-02  2.7742E-01  1.0000E-04
 mr-sdci #  2  1   -100.3252326736  9.5542E-03  6.4858E-04  6.3800E-02  1.0000E-04
 mr-sdci #  3  1   -100.3258400319  6.0736E-04  4.8453E-05  1.6614E-02  1.0000E-04
 mr-sdci #  4  1   -100.3258878210  4.7789E-05  3.8882E-06  4.7806E-03  1.0000E-04
 mr-sdci #  5  1   -100.3258914807  3.6597E-06  3.3639E-07  1.3891E-03  1.0000E-04
 mr-sdci #  6  1   -100.3258917893  3.0861E-07  6.2670E-08  4.9498E-04  1.0000E-04
 mr-sdci #  7  1   -100.3258918574  6.8051E-08  9.2096E-09  2.3140E-04  1.0000E-04
 mr-sdci #  8  1   -100.3258918664  9.0645E-09  1.4226E-09  8.2505E-05  1.0000E-04

 mr-sdci  convergence criteria satisfied after  8 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  8  1   -100.3258918664  9.0645E-09  1.4226E-09  8.2505E-05  1.0000E-04

 number of reference csfs (nref) is     8.  root number (iroot) is  1.
 c0**2 =   0.96063956  c**2 (all zwalks) =   0.96063956

 eref      =   -100.081742218220   "relaxed" cnot**2         =   0.960639556578
 eci       =   -100.325891866419   deltae = eci - eref       =  -0.244149648199
 eci+dv1   =   -100.335501704834   dv1 = (1-cnot**2)*deltae  =  -0.009609838414
 eci+dv2   =   -100.335895450332   dv2 = dv1 / cnot**2       =  -0.010003583913
 eci+dv3   =   -100.336322840352   dv3 = dv1 / (2*cnot**2-1) =  -0.010430973933
 eci+pople =   -100.334094118814   ( 10e- scaled deltae )    =  -0.252351900594
