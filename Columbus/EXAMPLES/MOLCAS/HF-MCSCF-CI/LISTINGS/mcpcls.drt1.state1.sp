

     ******************************************
     **    PROGRAM:              MCPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 original author: Daniel Robertson, FSU
 later revisions: Ron Shepard, ANL;
                  Michal Dallos, University Vienna



 This Version of Program mcpc is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



   ******  File header section  ******

 Headers form the restart file:
    SEWARD INTEGRALS                                                                
     title                                                                          


   ******  DRT info section  ******

 Informations for the DRT no.  1
 Header form the DRT file: 
     title                                                                          
 Molecular symmetry group:   sym1
 Total number of electrons:   10
 Spin multiplicity:            1
 Number of active orbitals:    5
 Number of active electrons:   8
 Total number of CSFs:         8

   ***  Informations from the DRT number:   1


 Symmetry orbital summary:
 Symm.blocks:         1     2     3     4
 Symm.labels:         a1    b1    a2    b2 

 List of doubly occupied orbitals:
  1 a1 

 List of active orbitals:
  2 a1   3 a1   4 a1   1 b1   1 b2 


   ******  MCSCF convergence information:  ******

 MCSCF convergence criteria were satisfied.

 mcscf energy=  -100.0823305449    nuclear repulsion=     5.2207204594
 demc=             0.0000000000    wnorm=                 0.0000006989
 knorm=            0.0000000313    apxde=                 0.0000000000


 MCSCF calculation performmed for   1 symmetry.

 State averaging:
 No,  ssym, navst, wavst
  1    a1     1   1.0000

 Input the DRT No of interest: [  1]:
In the DRT No.: 1 there are  1 states.

 Which one to take? [  1]:
 The CSFs for the state No  1 of the symmetry  a1  will be printed
 according to the following print options :

 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 0) end.

 input menu number [  0]: csfs will be printed based on coefficient magnitudes.

 input the coefficient threshold (end with 0.) [ 0.0000]:
 List of active orbitals:
  2 a1   3 a1   4 a1   1 b1   1 b2 

   csf       coeff       coeff**2    step(*)
  -----  ------------  ------------  ------------
      3 -0.9949486424  0.9899228009  33033
      5  0.0987678145  0.0097550812  30333
      2  0.0105432038  0.0001111591  33303
      1  0.0105432038  0.0001111591  33330
      8  0.0099899751  0.0000997996  03333

 input the coefficient threshold (end with 0.) [ 0.0000]:
 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 0) end.

 input menu number [  0]: