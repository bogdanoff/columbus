License is going to expire in 128 days on Thursday April 30th, 2009
Everything is fine. Going to run MOLCAS!
                                                                                                   
                                              ^^^^^            M O L C A S                         
                                             ^^^^^^^           version 7.3 patchlevel 337          
                               ^^^^^         ^^^^^^^                                               
                              ^^^^^^^        ^^^ ^^^                              ^                
                              ^^^^^^^       ^^^^ ^^^                             ^o^               
                              ^^^ ^^^       ^^^^ ^^^                            ^^^o^              
                              ^^^ ^^^^      ^^^  ^^^                           ^^^^^o^             
                              ^^^  ^^ ^^^^^  ^^  ^^^^                            ^^o               
                             ^^^^      ^^^^   ^   ^^^                           ^^o^^              
                             ^   ^^^   ^^^^   ^^^^  ^                          ^^o^^^^             
                            ^   ^^^^    ^^    ^^^^   ^                        ^^o^^^^^^            
                        ^^^^^   ^^^^^   ^^   ^^^^^   ^^^^^                   ^^^o^^^^^^^           
                     ^^^^^^^^   ^^^^^        ^^^^^    ^^^^^^^                   ^o^^^              
                 ^^^^^^^^^^^    ^^^^^^      ^^^^^^^   ^^^^^^^^^^^              ^^^o^^^             
               ^^^^^^^^^^^^^   ^^^^^^^^^^^^^^^^^^^^   ^^^^^^^^^^^^^           ^^^^^o^^^            
               ^^^^^^^^^^^^^   ^^^^             ^^^   ^^^      ^^^^          ^^^^^^^o^^^           
               ^^^^^^^^^^^^^                          ^^^      ^^^^         ^^^^^^^^^o^^^          
               ^^^^^^^^^^^^^       ^^^^^^^^^^^^        ^^      ^^^^        ^^^^^^^^^^^o^^^         
               ^^^^^^^^^^^^      ^^^^^^^^^^^^^^^^      ^^      ^^^^               ^                
               ^^^^^^^^^^^^    ^^^^^^^^^^^^^^^^^^^^    ^^      ^^^^               ^                
               ^^^^^^^^^^^^    ^^^^^^^^^^^^^^^^^^^^    ^^      ^^^^    ^^^^^     ^^^    ^^^^^^     
               ^^^^^^^^^^^^   ^^^^^^^^^^^^^^^^^^^^^^   ^^      ^^^^   ^^^^^^^    ^^^   ^^^  ^^^    
               ^^^^^^^^^^^    ^^^^^^^^^^^^^^^^^^^^^^   ^       ^^^^  ^^^    ^^   ^^^   ^^    ^^    
               ^^^^^^^^^^^     ^^^^^^^^^^^^^^^^^^^^            ^^^^  ^^         ^^ ^^  ^^^^^       
               ^^^^^^^^^^      ^^^^^^^^^^^^^^^^^^^^        ^   ^^^^  ^^         ^^ ^^   ^^^^^^     
               ^^^^^^^^          ^^^^^^^^^^^^^^^^          ^   ^^^^  ^^        ^^^^^^^     ^^^^    
               ^^^^^^      ^^^     ^^^^^^^^^^^^     ^      ^   ^^^^  ^^^   ^^^ ^^^^^^^ ^^    ^^    
                         ^^^^^^                    ^^      ^   ^^^^   ^^^^^^^  ^^   ^^ ^^^  ^^^    
                      ^^^^^^^^^^^^             ^^^^^^      ^           ^^^^^  ^^     ^^ ^^^^^^     
               ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                                
                 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                                  
                     ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                                      
                        ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                                         
                            ^^^^^^^^^^^^^^^^^^^^^^^^^^                                             
                               ^^^^^^^^^^^^^^^^^^^^                                                
                                   ^^^^^^^^^^^^                                                    
                                       ^^^^^^                                                      

                           Copyright, all rights, reserved:                                        
                         Permission is hereby granted to use                                       
                but not to reproduce or distribute any part of this                                
             program. The use is restricted to research purposes only.                             
                            Lund University Sweden, 2008.                                          
                                                                                                   
 For the author list and the recommended citation consult section 1.5 of the MOLCAS user's guide.  



()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()
                                  MOLCAS executing module SEWARD with 250 MB of memory                                  
                                              at 14:17:16 Tue Dec 23 2008                                               
()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()


               SEWARD will generate:
                  Multipole Moment integrals up to order  2
                  Kinetic Energy integrals
                  Nuclear Attraction integrals (point charge)
                  One-Electron Hamiltonian integrals
                  Two-Electron Repulsion integrals

 Title:
                                         HF Monomer C2v                                 


                   Integrals are discarded if absolute value <: 0.10E-12
                   Integral cutoff threshold is set to       <: 0.10E-11

                    --- Group Generators ---
                    Rotation around the z-axis  
                    Reflection in the xz-plane  


                    Character Table for C2v

                             E   C2(z) s(xz) s(yz)
                    a1       1     1     1     1  z
                    b1       1    -1     1    -1  x, xz, Ry
                    a2       1     1    -1    -1  xy, Rz, I
                    b2       1    -1    -1     1  y, yz, Rx

                    Symmetry adaptation a la MOLECULE.



 Basis set label:H.CC-PVTZ.DUNNING.5S2P1D.3S2P1D......                                           

 Valence basis set:
 ==================
 Associated Effective Charge  1.000000 au
 Associated Actual Charge     1.000000 au
 Nuclear Model: Point charge
  Shell  nPrim  nBasis  Cartesian Spherical Contaminant
    s       5       3        X                  
    p       2       2        X                  
    d       1       1                 X         


 Basis set label:F.CC-PVTZ.DUNNING.10S5P2D1F.4S3P2D1F......                                      

 Valence basis set:
 ==================
 Associated Effective Charge  9.000000 au
 Associated Actual Charge     9.000000 au
 Nuclear Model: Point charge
  Shell  nPrim  nBasis  Cartesian Spherical Contaminant
    s      10       4        X                  
    p       5       3        X                  
    d       2       2                 X         
    f       1       1                 X         

                    ************************************************ 
                    **** Cartesian Coordinates / Bohr, Angstrom **** 
                    ************************************************ 

     Center  Label                x              y              z                     x              y              z
        1      H              0.000000       0.000000       0.000000              0.000000       0.000000       0.000000
        2      F              0.000000       0.000000       1.723900              0.000000       0.000000       0.912249

                    *************************************** 
                    *    InterNuclear Distances / Bohr    * 
                    *************************************** 

             1 H             2 F   
    1 H      0.000000
    2 F      1.723900        0.000000

                    ******************************************* 
                    *    InterNuclear Distances / Angstrom    * 
                    ******************************************* 

             1 H             2 F   
    1 H      0.000000
    2 F      0.912249        0.000000

  Basis set specifications :
  Symmetry species            a1  b1  a2  b2 
  Basis functions              20  10   4  10




                    Nuclear Potential Energy                      5.22072046 au
 Molden_Interface: Unsupported normalization,Molwgh=1!
--- Stop Module: seward at Tue Dec 23 14:17:16 2008 /rc=0 ---

     Happy landing!

