
======= Computing sorting integral file structure =========

 nmpsy=                       20                       10 
                        4                       10
                    -----z----- -----y----- -----x----- -----w----- ---total---

                CSFs         8         428        7407        9314       17157
      internal walks         8          40          45          50         143
valid internal walks         8          40          42          47         137
 found l2rec=                     4096 n2max=                     2730

 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg=  32583894
 minimum size of srtscr:                   229369  WP(  
                        7  records)
 maximum size of srtscr:                   360437  WP(  
                        9  records)

sorted 4-external integrals:     3 records of integral w-combinations and
             2 records of integral x-combinations of length= 32767 have been written.
 wstat,xstat=  100.00  100.00

sorted 3-external integrals:     2 records of integral w-combinations and
             2 records of integral x-combinations of length= 32767 have been written.
 wstat,xstat=  100.00  100.00
 Orig.  diagonal integrals:  1electron:        44
                             0ext.    :        42
                             2ext.    :       456
                             4ext.    :      1482


 Orig. off-diag. integrals:  4ext.    :     79122
                             3ext.    :     47394
                             2ext.    :     13251
                             1ext.    :      2040
                             0ext.    :       138
                             2ext. SO :         0
                             1ext. SO :         0
                             0ext. SO :         0
                             1electron:       236


 Sorted integrals            3ext.  w :     44360 x :     41326
                             4ext.  w :     70390 x :     62163


1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      -varseg3    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 compressed index vector length=                       25
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 0,
  GSET = 0,
   DAVCOR =10,
  NCOREL = 8
  NROOT = 1
  IVMODE = 3
  NBKITR = 1
  NVBKMN = 1
  NVBKMX = 6
  RTOLBK = 1e-4
  NITER = 30
  NVCIMN = 1
  NVCIMX = 6
  RTOLCI = 1e-4
  IDEN  = 1
  CSFPRN = 10,
    MOLCAS=1
 /&end
 ------------------------------------------------------------------------
 Ame=                        0 ivmode,nbkitr=                        3 
                        1 ntype=                        0
 me=                        0 ivmode,nbkitr=                        3 
                        1 ntype=                        0
 ntotseg(*)=                        4                        4 
                        4                        4                        4 
                        4
 iexplseg(*)=                        0                        0 
                        0                        0                        0 
                        0
 resetting explseg from                         0  to                         0
 USING SEGMENTS OF EQUAL SIZE

****************  list of control variables  ****************
 lvlprt =    0      nroot  =    1      noldv  =   0      noldhv =   0
 nunitv =    1      nbkitr =    1      niter  =  30      davcor =  10
 csfprn =   10      ivmode =    3      istrt  =   0      vout   =   0
 iortls =    0      nvbkmx =    6      ibktv  =  -1      ibkthv =  -1
 nvcimx =    6      icitv  =   -1      icithv =  -1      frcsub =   0
 nvbkmn =    1      nvcimn =    1      maxseg =   4      nrfitr =  30
 ncorel =    8      nvrfmx =    6      nvrfmn =   1      iden   =   1
 itran  =    0      froot  =    0      rtmode =   0      ncouple=   1
 skipso =    F      dalton2=    0      molcas =   1      finalv =   0
 finalw =    0      cosmocalc=   0    with_tsklst=   0
 nsegwx =    1     1     1     1
 nseg0x =    1     1     1     1
 nseg1x =    1     1     1     1
 nseg2x =    1     1     1     1
 nseg3x =    1     1     1     1
 nseg4x =    1     1     1     1
 no0ex  =      0    no1ex  =      0    no2ex  =     0    no3ex  =     0
 no4ex  =      0    nodiag =      0
 cdg4ex =    1      c3ex1ex=    1      c2ex0ex=   1
 fileloc=    0     0     0     0     0     0     0     1     1     1

 ctol   = 0.010000    lrtshift=1.000000    smalld =0.001000


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-04    1.000E-04
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core           32767999 DP per process

********** Integral sort section *************


 workspace allocation information: lencor=  32767999

 echo of the input for program cisrt:
 ------------------------------------------------------------------------
  &input
  maxbl3=60000
  maxbl4=60000
  &end
 ------------------------------------------------------------------------

 ( 6) listing file:                    ciudgls             
 ( 5) input file:                      cisrtin   
 (17) cidrt file:                      cidrtfl             
 (11) transformed integrals file:      moints    
 (12) diagonal integral file:          diagint             
 (13) off-diagonal integral file:      ofdgint             
 (31) 4-external w integrals file:     fil4w               
 (32) 4-external x integrals file:     fil4x               
 (33) 3-external w integrals file:     fil3w               
 (34) 3-external x integrals file:     fil3x               
 (21) scratch da sorting file:         srtscr              
 (12) 2-e integral file [fsplit=2]:    moints2   

 input integral file header information:
 SEWARD INTEGRALS                                                                
  cidrt_title                                                                    
  title                                                                          
 mofmt: formatted orbitals label=morbl   zam216            14:17:17.268 23-Dec-08
 SIFS file created by program tran.      zam216            14:17:17.312 23-Dec-08

 input energy(*) values:
 energy( 1)=  5.220720459423E+00, ietype=   -1,    core energy of type: Nuc.Rep.

 total core energy =   5.220720459423E+00

 nsym = 4 nmot=  44

 symmetry  =    1    2    3    4
 slabel(*) = a1   b1   a2   b2  
 nmpsy(*)  =   20   10    4   10

 info(*) =          1      4096      3272      4096      2730

 orbital labels, i:molab(i)=
   1:tout:001   2:tout:002   3:tout:003   4:tout:004   5:tout:005   6:tout:006   7:tout:007   8:tout:008   9:tout:009  10:tout:010
  11:tout:011  12:tout:012  13:tout:013  14:tout:014  15:tout:015  16:tout:016  17:tout:017  18:tout:018  19:tout:019  20:tout:020
  21:tout:021  22:tout:022  23:tout:023  24:tout:024  25:tout:025  26:tout:026  27:tout:027  28:tout:028  29:tout:029  30:tout:030
  31:tout:031  32:tout:032  33:tout:033  34:tout:034  35:tout:035  36:tout:036  37:tout:037  38:tout:038  39:tout:039  40:tout:040
  41:tout:041  42:tout:042  43:tout:043  44:tout:044

 input parameters:
 prnopt=  0
 ldamin=    4095 ldamax=   32767 ldainc=      64
 maxbuf=   32767 maxbl3=   60000 maxbl4=   60000 intmxo=     766

 drt information:
  cidrt_title                                                                    
 nmotd =  44 nfctd =   0 nfvtc =   0 nmot  =  44
 nlevel =  44 niot  =   6 lowinl=  39
 orbital-to-level map(*)
   39  40  41  42   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15  16
   43  17  18  19  20  21  22  23  24  25  26  27  28  29  44  30  31  32  33  34
   35  36  37  38
 compressed map(*)
   39  40  41  42   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15  16
   43  17  18  19  20  21  22  23  24  25  26  27  28  29  44  30  31  32  33  34
   35  36  37  38
 levsym(*)
    1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   2   2   2   2
    2   2   2   2   2   3   3   3   3   4   4   4   4   4   4   4   4   4   1   1
    1   1   2   4
 repartitioning mu(*)=
   2.  0.  0.  0.  0.  0.

 indxdg: diagonal integral statistics.
 total number of integrals contributing to diagonal matrix elements:      1980
 number with all external indices:      1482
 number with half external - half internal indices:       456
 number with all internal indices:        42

 indxof: off-diagonal integral statistics.
    4-external integrals: num=      79122 strt=          1
    3-external integrals: num=      47394 strt=      79123
    2-external integrals: num=      13251 strt=     126517
    1-external integrals: num=       2040 strt=     139768
    0-external integrals: num=        138 strt=     141808

 total number of off-diagonal integrals:      141945



 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg=  32586861
 !timer: setup required                  user+sys=     0.000 walltime=     0.000
 pro2e        1     991    1981    2971    3961    3982    4003    4993   48681   92369
   125136  129232  131962  142881

 pro2e:    108786 integrals read in    40 records.
 pro1e        1     991    1981    2971    3961    3982    4003    4993   48681   92369
   125136  129232  131962  142881
 pro1e: eref =   -7.631778201443808E+01
 total size of srtscr:                        8  records of  
                    32767 WP =                  2097088 Bytes
 !timer: first half-sort required        user+sys=     0.010 walltime=     0.000

 new core energy added to the energy(*) list.
 from the hamiltonian repartitioning, eref= -7.631778201444E+01
 putdg        1     991    1981    2971    3737   36504   58349    4993   48681   92369
   125136  129232  131962  142881

 putf:       5 buffers of length     766 written to file 12
 diagonal integral file completed.

 putd34:     3 records of integral w-combinations and
             2 records of integral x-combinations of length= 32767 have been written.
 wstat,xstat=  100.00  100.00
 prep4e:    21 blocks of linear combinations of 4-external integrals processed.
 number of sorted 4-external integrals     132553
 number of original 4-external integrals    79122


 putf34: external integral file complete. nfilw=    31 nfilx=    32 nrecw=     3 nrecx=     2 lbufp= 32767

 putd34:     2 records of integral w-combinations and
             2 records of integral x-combinations of length= 32767 have been written.
 wstat,xstat=  100.00  100.00
 prep3e:    21 blocks of linear combinations of 3-external integrals processed.
 number of sorted 3-external integrals      85686
 number of original 3-external integrals    47394


 putf34: external integral file complete. nfilw=    33 nfilx=    34 nrecw=     2 nrecx=     2 lbufp= 32767

 putf:      23 buffers of length     766 written to file 13
 off-diagonal files sort completed.
 !timer: second half-sort required       user+sys=     0.010 walltime=     0.000
 !timer: cisrt complete                  user+sys=     0.020 walltime=     0.000
 executing brd_struct for cisrtinfo
cisrtinfo:
bufszi   766
 diagfile 4ext:    1482 2ext:     456 0ext:      42
 fil4w,fil4x  :   79122 fil3w,fil3x :   47394
 ofdgint  2ext:   13251 1ext:    2040 0ext:     138so0ext:       0so1ext:       0so2ext:       0
buffer minbl4     705 minbl3     705 maxbl2     708nbas:  16   9   4   9   0   0   0   0 maxbuf 32767
 CIUDG version 5.9.7 ( 5-Oct-2004)

 workspace allocation information: lcore=  32767999

 core energy values from the integral file:
 energy( 1)=  5.220720459423E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -7.631778201444E+01, ietype=    5,   fcore energy of type: Vref(*) 

 total core repulsion energy = -7.109706155501E+01
 nmot  =    44 niot  =     6 nfct  =     0 nfvt  =     0
 nrow  =    34 nsym  =     4 ssym  =     1 lenbuf=  1600
 nwalk,xbar:        143        8       40       45       50
 nvalwt,nvalw:      137        8       40       42       47
 ncsft:           17157
 total number of valid internal walks:     137
 nvalz,nvaly,nvalx,nvalw =        8      40      42      47

 cisrt info file parameters:
 file number  12 blocksize    766
 mxbld   1532
 nd4ext,nd2ext,nd0ext  1482   456    42
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int    79122    47394    13251     2040      138        0        0        0
 minbl4,minbl3,maxbl2   705   705   708
 maxbuf 32767
 number of external orbitals per symmetry block:  16   9   4   9
 nmsym   4 number of internal orbitals   6
 executing brd_struct for drt
 executing brd_struct for orbinf
 executing brd_struct for momap
 calcthrxt: niot,maxw1=                        6                       39
 block size     0
 pthz,pthy,pthx,pthw:     8    40    45    50 total internal walks:     143
 maxlp3,n2lp,n1lp,n0lp    39     0     0     0
 orbsym(*)= 1 1 1 1 2 4

 setref:        8 references kept,
                0 references were marked as invalid, out of
                8 total.
 limcnvrt: found                        8  valid internal walksout of  
                        8  walks (skipping trailing invalids)
  ... adding                         8  segmentation marks segtype= 
                        1
 limcnvrt: found                       40  valid internal walksout of  
                       40  walks (skipping trailing invalids)
  ... adding                        40  segmentation marks segtype= 
                        2
 limcnvrt: found                       42  valid internal walksout of  
                       45  walks (skipping trailing invalids)
  ... adding                        42  segmentation marks segtype= 
                        3
 limcnvrt: found                       47  valid internal walksout of  
                       50  walks (skipping trailing invalids)
  ... adding                        47  segmentation marks segtype= 
                        4

 norbsm(*)=  20  10   4  10   0   0   0   0
 noxt(*)=  16   9   4   9   0   0   0   0
 intorb=   6 nmsym=   4 lowdoc=   5
 ivout(*)=   6   7   8   9  10  11  12  13  14  15  16  17  18  19  20  22  23  24  25  26
  27  28  29  30  31  32  33  34  36  37  38  39  40  41  42  43  44   1   2   3
   4  21  35   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0
 post-tapin: reflst,limvec,indsym=                        1 
                        9                      152
 ---------memory usage in DP -----------------
 < n-ex core usage >
     routines:
    fourex            66792
    threx             33002
    twoex              4642
    onex               1343
    allin               766
    diagon             2125
               =======
   maximum            66792

  __ static summary __ 
   reflst                 8
   hrfspc                 8
               -------
   static->               8

  __ core required  __ 
   totstc                 8
   max n-ex           66792
               -------
   totnec->           66800

  __ core available __ 
   totspc          32767999
   totnec -           66800
               -------
   totvec->        32701199

 number of external paths / symmetry
 vertex x     198     180     145     180
 vertex w     236     180     145     180
segment: free space=    32701199
 reducing frespc by                      598 to                  32700601 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1           8|         8|         0|         8|         0|         1|
 -------------------------------------------------------------------------------
  Y 2          40|       428|         8|        40|         8|         2|
 -------------------------------------------------------------------------------
  X 3          42|      7407|       436|        42|        48|         3|
 -------------------------------------------------------------------------------
  W 4          47|      9314|      7843|        47|        90|         4|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=           4DP  conft+indsym=         188DP  drtbuffer=         406 DP

dimension of the ci-matrix ->>>     17157

 executing brd_struct for civct
 gentasklist: ntask=                       20
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS   
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      42       8       7407          8      42       8
     2  4   1    25      two-ext wz   2X  4 1      47       8       9314          8      47       8
     3  4   3    26      two-ext wx*  WX  4 3      47      42       9314       7407      47      42
     4  4   3    27      two-ext wx+  WX  4 3      47      42       9314       7407      47      42
     5  2   1    11      one-ext yz   1X  2 1      40       8        428          8      40       8
     6  3   2    15      1ex3ex yx    3X  3 2      42      40       7407        428      42      40
     7  4   2    16      1ex3ex yw    3X  4 2      47      40       9314        428      47      40
     8  1   1     1      allint zz    OX  1 1       8       8          8          8       8       8
     9  2   2     5      0ex2ex yy    OX  2 2      40      40        428        428      40      40
    10  3   3     6      0ex2ex xx*   OX  3 3      42      42       7407       7407      42      42
    11  3   3    18      0ex2ex xx+   OX  3 3      42      42       7407       7407      42      42
    12  4   4     7      0ex2ex ww*   OX  4 4      47      47       9314       9314      47      47
    13  4   4    19      0ex2ex ww+   OX  4 4      47      47       9314       9314      47      47
    14  2   2    42      four-ext y   4X  2 2      40      40        428        428      40      40
    15  3   3    43      four-ext x   4X  3 3      42      42       7407       7407      42      42
    16  4   4    44      four-ext w   4X  4 4      47      47       9314       9314      47      47
    17  1   1    75      dg-024ext z  OX  1 1       8       8          8          8       8       8
    18  2   2    76      dg-024ext y  OX  2 2      40      40        428        428      40      40
    19  3   3    77      dg-024ext x  OX  3 3      42      42       7407       7407      42      42
    20  4   4    78      dg-024ext w  OX  4 4      47      47       9314       9314      47      47
----------------------------------------------------------------------------------------------------
REDTASK #   1 TIME=  19.000 N=   1 indiv tasks=   1
REDTASK #   2 TIME=  18.000 N=   1 indiv tasks=   2
REDTASK #   3 TIME=  17.000 N=   1 indiv tasks=   3
REDTASK #   4 TIME=  16.000 N=   1 indiv tasks=   4
REDTASK #   5 TIME=  15.000 N=   1 indiv tasks=   5
REDTASK #   6 TIME=  14.000 N=   1 indiv tasks=   6
REDTASK #   7 TIME=  13.000 N=   1 indiv tasks=   7
REDTASK #   8 TIME=  12.000 N=   1 indiv tasks=   8
REDTASK #   9 TIME=  11.000 N=   1 indiv tasks=   9
REDTASK #  10 TIME=  10.000 N=   1 indiv tasks=  10
REDTASK #  11 TIME=   9.000 N=   1 indiv tasks=  11
REDTASK #  12 TIME=   8.000 N=   1 indiv tasks=  12
REDTASK #  13 TIME=   7.000 N=   1 indiv tasks=  13
REDTASK #  14 TIME=   6.000 N=   1 indiv tasks=  14
REDTASK #  15 TIME=   5.000 N=   1 indiv tasks=  15
REDTASK #  16 TIME=   4.000 N=   1 indiv tasks=  16
REDTASK #  17 TIME=   3.000 N=   1 indiv tasks=  17
REDTASK #  18 TIME=   2.000 N=   1 indiv tasks=  18
REDTASK #  19 TIME=   1.000 N=   1 indiv tasks=  19
REDTASK #  20 TIME=   0.000 N=   1 indiv tasks=  20
 initializing v-file: 1:                    17157

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:          95 2x:           0 4x:           0
All internal counts: zz :          55 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:           0    task #     2:           0    task #     3:           0    task #     4:           0
task #     5:           0    task #     6:           0    task #     7:           0    task #     8:          54
task #     9:           0    task #    10:           0    task #    11:           0    task #    12:           0
task #    13:           0    task #    14:           0    task #    15:           0    task #    16:           0
task #    17:          82    task #    18:           0    task #    19:           0    task #    20:           0
 reference space has dimension       8

    root           eigenvalues
    ----           ------------
       1        -100.0823305449

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt=                        8

         vector  1 from unit 11 written to unit 49 filename cirefv              

 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=     8)...
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:             17157
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                    1
 residual norm convergence criteria:               0.000100

          starting bk iteration   1

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    1.124793
ci vector #   2dasum_wr=    0.000000
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:        1540 2x:         545 4x:         129
All internal counts: zz :          55 yy:           0 xx:           0 ww:           0
One-external counts: yz :         349 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:          68 wz:          93 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         127    task #     2:         163    task #     3:           0    task #     4:           0
task #     5:         332    task #     6:           0    task #     7:           0    task #     8:          54
task #     9:           0    task #    10:           0    task #    11:           0    task #    12:           0
task #    13:           0    task #    14:           0    task #    15:          56    task #    16:          56
task #    17:          82    task #    18:         581    task #    19:         579    task #    20:         529

          calcsovref: tciref block   1

              civs   1
 refs   1    1.00000    

          calcsovref: scrb block   1

              civs   1
 civs   1    1.00000    
 eci,repnuc=   -28.98526898986131        -71.09706155501468     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -100.0823305449 -3.5527E-15  2.8280E-01  1.4291E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0001
    2   25    0     0.01     0.00     0.01     0.01         0.    0.0002
    3   26    0     0.00     0.00     0.00     0.00         0.    0.0000
    4   27    0     0.00     0.00     0.00     0.00         0.    0.0000
    5   11    0     0.00     0.00     0.00     0.00         0.    0.0003
    6   15    0     0.00     0.00     0.00     0.00         0.    0.0000
    7   16    0     0.00     0.00     0.00     0.00         0.    0.0000
    8    1    0     0.01     0.00     0.00     0.01         0.    0.0001
    9    5    0     0.00     0.00     0.00     0.00         0.    0.0000
   10    6    0     0.00     0.00     0.00     0.00         0.    0.0000
   11   18    0     0.00     0.00     0.00     0.00         0.    0.0000
   12    7    0     0.00     0.00     0.00     0.00         0.    0.0000
   13   19    0     0.00     0.00     0.00     0.00         0.    0.0000
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0000
   15   43    0     0.00     0.00     0.00     0.00         0.    0.0001
   16   44    0     0.00     0.00     0.00     0.00         0.    0.0001
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0001
   18   76    0     0.00     0.00     0.00     0.00         0.    0.0006
   19   77    0     0.00     0.00     0.00     0.00         0.    0.0006
   20   78    0     0.00     0.00     0.00     0.00         0.    0.0001
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0200s 
time spent in multnx:                   0.0200s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.0300s 

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -100.0823305449 -3.5527E-15  2.8280E-01  1.4291E+00  1.0000E-04

 expansion vectors are not transformed.
 matrix-vector products are not transformed.

    2 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:             17157
 number of initial trial vectors:                         2
 number of initial matrix-vector products:                1
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                   30
 residual norm convergence criteria:               0.000100

          starting ci iteration   1

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.000000
ci vector #   2dasum_wr=    0.915621
ci vector #   3dasum_wr=    2.200915
ci vector #   4dasum_wr=    4.165218


====================================================================================================
Diagonal     counts:  0x:        1540 2x:         545 4x:         129
All internal counts: zz :          55 yy:         391 xx:         830 ww:         824
One-external counts: yz :         349 yx:        1619 yw:        1632
Two-external counts: yy :         470 ww:         852 xx:         968 xz:          68 wz:          93 wx:        1100
Three-ext.   counts: yx :         426 yw:         455

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         127    task #     2:         163    task #     3:         899    task #     4:         449
task #     5:         332    task #     6:        1463    task #     7:        1441    task #     8:          54
task #     9:         669    task #    10:         741    task #    11:         741    task #    12:         595
task #    13:         595    task #    14:         297    task #    15:         299    task #    16:         299
task #    17:          82    task #    18:         581    task #    19:         579    task #    20:         529

          calcsovref: tciref block   1

              civs   1       civs   2
 refs   1    1.00000      -4.317642E-13

          calcsovref: scrb block   1

              civs   1       civs   2
 civs   1   0.980979      -0.194112    
 civs   2   0.809449        4.09069    
 eci,repnuc=   -29.21861694766799        -71.09706155501468     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -100.3156785027  2.3335E-01  1.2824E-02  2.7742E-01  1.0000E-04
 mr-sdci #  1  2    -94.1227354590  2.3026E+01  0.0000E+00  3.7122E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0001
    2   25    0     0.01     0.00     0.00     0.01         0.    0.0002
    3   26    0     0.00     0.00     0.00     0.00         0.    0.0009
    4   27    0     0.01     0.00     0.00     0.01         0.    0.0005
    5   11    0     0.00     0.00     0.00     0.00         0.    0.0003
    6   15    0     0.01     0.00     0.01     0.01         0.    0.0015
    7   16    0     0.01     0.00     0.00     0.01         0.    0.0014
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0001
    9    5    0     0.00     0.00     0.00     0.00         0.    0.0007
   10    6    0     0.01     0.01     0.00     0.01         0.    0.0007
   11   18    0     0.01     0.00     0.00     0.01         0.    0.0007
   12    7    0     0.00     0.00     0.00     0.00         0.    0.0006
   13   19    0     0.01     0.00     0.00     0.01         0.    0.0006
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0003
   15   43    0     0.00     0.00     0.00     0.00         0.    0.0003
   16   44    0     0.01     0.00     0.00     0.01         0.    0.0003
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0001
   18   76    0     0.00     0.00     0.00     0.00         0.    0.0006
   19   77    0     0.00     0.00     0.00     0.00         0.    0.0006
   20   78    0     0.00     0.01     0.00     0.00         0.    0.0001
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0800s 
time spent in multnx:                   0.0800s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.0200s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.0900s 

          starting ci iteration   2

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.051924
ci vector #   2dasum_wr=    0.296308
ci vector #   3dasum_wr=    0.682817
ci vector #   4dasum_wr=    0.980946


====================================================================================================
Diagonal     counts:  0x:        1540 2x:         545 4x:         129
All internal counts: zz :          55 yy:         391 xx:         830 ww:         824
One-external counts: yz :         349 yx:        1619 yw:        1632
Two-external counts: yy :         470 ww:         852 xx:         968 xz:          68 wz:          93 wx:        1100
Three-ext.   counts: yx :         426 yw:         455

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         127    task #     2:         163    task #     3:         899    task #     4:         449
task #     5:         332    task #     6:        1463    task #     7:        1441    task #     8:          54
task #     9:         669    task #    10:         741    task #    11:         741    task #    12:         595
task #    13:         595    task #    14:         297    task #    15:         299    task #    16:         299
task #    17:          82    task #    18:         581    task #    19:         579    task #    20:         529

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3
 refs   1    1.00000      -4.317642E-13   1.704964E-02

          calcsovref: scrb block   1

              civs   1       civs   2       civs   3
 civs   1   0.967844      -0.236465       0.321130    
 civs   2   0.840329      -0.871196       -4.10594    
 civs   3   0.744406        16.4591       -7.61431    
 eci,repnuc=   -29.22817111854091        -71.09706155501468     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1   -100.3252326736  9.5542E-03  6.4858E-04  6.3800E-02  1.0000E-04
 mr-sdci #  2  2    -94.7692721384  6.4654E-01  0.0000E+00  3.1661E+00  1.0000E-04
 mr-sdci #  2  3    -93.9843940873  2.2887E+01  0.0000E+00  4.0866E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0001
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0002
    3   26    0     0.01     0.00     0.00     0.01         0.    0.0009
    4   27    0     0.00     0.00     0.00     0.00         0.    0.0005
    5   11    0     0.01     0.00     0.00     0.01         0.    0.0003
    6   15    0     0.00     0.00     0.00     0.00         0.    0.0015
    7   16    0     0.01     0.00     0.00     0.01         0.    0.0014
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0001
    9    5    0     0.00     0.00     0.00     0.00         0.    0.0007
   10    6    0     0.01     0.00     0.00     0.01         0.    0.0007
   11   18    0     0.01     0.00     0.00     0.01         0.    0.0007
   12    7    0     0.00     0.00     0.00     0.00         0.    0.0006
   13   19    0     0.00     0.00     0.00     0.00         0.    0.0006
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0003
   15   43    0     0.01     0.00     0.00     0.01         0.    0.0003
   16   44    0     0.00     0.00     0.00     0.00         0.    0.0003
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0001
   18   76    0     0.01     0.00     0.01     0.00         0.    0.0006
   19   77    0     0.00     0.00     0.00     0.00         0.    0.0006
   20   78    0     0.00     0.00     0.00     0.00         0.    0.0001
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0700s 
time spent in multnx:                   0.0600s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.0800s 

          starting ci iteration   3

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.012521
ci vector #   2dasum_wr=    0.066534
ci vector #   3dasum_wr=    0.179870
ci vector #   4dasum_wr=    0.239031


====================================================================================================
Diagonal     counts:  0x:        1540 2x:         545 4x:         129
All internal counts: zz :          55 yy:         391 xx:         830 ww:         824
One-external counts: yz :         349 yx:        1619 yw:        1632
Two-external counts: yy :         470 ww:         852 xx:         968 xz:          68 wz:          93 wx:        1100
Three-ext.   counts: yx :         426 yw:         455

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         127    task #     2:         163    task #     3:         899    task #     4:         449
task #     5:         332    task #     6:        1463    task #     7:        1441    task #     8:          54
task #     9:         669    task #    10:         741    task #    11:         741    task #    12:         595
task #    13:         595    task #    14:         297    task #    15:         299    task #    16:         299
task #    17:          82    task #    18:         581    task #    19:         579    task #    20:         529

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4
 refs   1    1.00000      -4.317642E-13   1.704964E-02  -3.532659E-03

          calcsovref: scrb block   1

              civs   1       civs   2       civs   3       civs   4
 civs   1  -0.970018      -6.175344E-02  -0.233070      -0.395375    
 civs   2  -0.842308       -1.41405      -0.994680        3.83923    
 civs   3  -0.791525       -6.79750        16.0916        5.78720    
 civs   4  -0.936391       -75.9844       -2.67114       -36.2983    
 eci,repnuc=   -29.22877847684310        -71.09706155501468     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1   -100.3258400319  6.0736E-04  4.8453E-05  1.6614E-02  1.0000E-04
 mr-sdci #  3  2    -95.6937782448  9.2451E-01  0.0000E+00  3.4447E+00  1.0000E-04
 mr-sdci #  3  3    -94.7678793663  7.8349E-01  0.0000E+00  3.1381E+00  1.0000E-04
 mr-sdci #  3  4    -93.5953701955  2.2498E+01  0.0000E+00  4.0132E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0001
    2   25    0     0.01     0.00     0.01     0.00         0.    0.0002
    3   26    0     0.00     0.00     0.00     0.00         0.    0.0009
    4   27    0     0.00     0.00     0.00     0.00         0.    0.0005
    5   11    0     0.00     0.00     0.00     0.00         0.    0.0003
    6   15    0     0.01     0.00     0.00     0.01         0.    0.0015
    7   16    0     0.01     0.01     0.00     0.01         0.    0.0014
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0001
    9    5    0     0.00     0.00     0.00     0.00         0.    0.0007
   10    6    0     0.01     0.00     0.00     0.01         0.    0.0007
   11   18    0     0.00     0.00     0.00     0.00         0.    0.0007
   12    7    0     0.01     0.00     0.00     0.01         0.    0.0006
   13   19    0     0.00     0.00     0.00     0.00         0.    0.0006
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0003
   15   43    0     0.01     0.00     0.01     0.01         0.    0.0003
   16   44    0     0.01     0.00     0.01     0.01         0.    0.0003
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0001
   18   76    0     0.00     0.00     0.00     0.00         0.    0.0006
   19   77    0     0.01     0.00     0.00     0.01         0.    0.0006
   20   78    0     0.00     0.00     0.00     0.00         0.    0.0001
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0800s 
time spent in multnx:                   0.0700s 
integral transfer time:                 0.0300s 
time spent for loop construction:       0.0100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.0800s 

          starting ci iteration   4

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.004179
ci vector #   2dasum_wr=    0.021954
ci vector #   3dasum_wr=    0.040919
ci vector #   4dasum_wr=    0.062210


====================================================================================================
Diagonal     counts:  0x:        1540 2x:         545 4x:         129
All internal counts: zz :          55 yy:         391 xx:         830 ww:         824
One-external counts: yz :         349 yx:        1619 yw:        1632
Two-external counts: yy :         470 ww:         852 xx:         968 xz:          68 wz:          93 wx:        1100
Three-ext.   counts: yx :         426 yw:         455

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         127    task #     2:         163    task #     3:         899    task #     4:         449
task #     5:         332    task #     6:        1463    task #     7:        1441    task #     8:          54
task #     9:         669    task #    10:         741    task #    11:         741    task #    12:         595
task #    13:         595    task #    14:         297    task #    15:         299    task #    16:         299
task #    17:          82    task #    18:         581    task #    19:         579    task #    20:         529

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1    1.00000      -4.317642E-13   1.704964E-02  -3.532659E-03  -1.315721E-03

          calcsovref: scrb block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 civs   1   0.968725      -0.292952      -0.356729      -0.288566      -0.126400    
 civs   2   0.842446       0.846868       -1.06642        1.99022        3.58771    
 civs   3   0.795242        5.58579        10.1344       -8.73956        12.1501    
 civs   4    1.01005        43.5637       -45.3457       -56.0686      -0.748954    
 civs   5  -0.986298       -200.135       -61.3671       -109.696        169.449    
 eci,repnuc=   -29.22882626596244        -71.09706155501468     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1   -100.3258878210  4.7789E-05  3.8882E-06  4.7806E-03  1.0000E-04
 mr-sdci #  4  2    -97.4473814152  1.7536E+00  0.0000E+00  2.0697E+00  1.0000E-04
 mr-sdci #  4  3    -94.9455842715  1.7770E-01  0.0000E+00  3.2078E+00  1.0000E-04
 mr-sdci #  4  4    -94.2263217626  6.3095E-01  0.0000E+00  3.4286E+00  1.0000E-04
 mr-sdci #  4  5    -92.7052975158  2.1608E+01  0.0000E+00  4.2495E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0001
    2   25    0     0.01     0.00     0.00     0.01         0.    0.0002
    3   26    0     0.00     0.00     0.00     0.00         0.    0.0009
    4   27    0     0.01     0.00     0.00     0.01         0.    0.0005
    5   11    0     0.00     0.00     0.00     0.00         0.    0.0003
    6   15    0     0.01     0.00     0.00     0.01         0.    0.0015
    7   16    0     0.01     0.00     0.00     0.01         0.    0.0014
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0001
    9    5    0     0.00     0.00     0.00     0.00         0.    0.0007
   10    6    0     0.01     0.00     0.01     0.01         0.    0.0007
   11   18    0     0.00     0.00     0.00     0.00         0.    0.0007
   12    7    0     0.01     0.00     0.00     0.01         0.    0.0006
   13   19    0     0.00     0.00     0.00     0.00         0.    0.0006
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0003
   15   43    0     0.01     0.00     0.00     0.01         0.    0.0003
   16   44    0     0.01     0.00     0.00     0.01         0.    0.0003
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0001
   18   76    0     0.00     0.00     0.00     0.00         0.    0.0006
   19   77    0     0.00     0.00     0.00     0.00         0.    0.0006
   20   78    0     0.00     0.00     0.00     0.00         0.    0.0001
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0800s 
time spent in multnx:                   0.0800s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.0900s 

          starting ci iteration   5

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.001288
ci vector #   2dasum_wr=    0.006228
ci vector #   3dasum_wr=    0.012950
ci vector #   4dasum_wr=    0.018755


====================================================================================================
Diagonal     counts:  0x:        1540 2x:         545 4x:         129
All internal counts: zz :          55 yy:         391 xx:         830 ww:         824
One-external counts: yz :         349 yx:        1619 yw:        1632
Two-external counts: yy :         470 ww:         852 xx:         968 xz:          68 wz:          93 wx:        1100
Three-ext.   counts: yx :         426 yw:         455

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         127    task #     2:         163    task #     3:         899    task #     4:         449
task #     5:         332    task #     6:        1463    task #     7:        1441    task #     8:          54
task #     9:         669    task #    10:         741    task #    11:         741    task #    12:         595
task #    13:         595    task #    14:         297    task #    15:         299    task #    16:         299
task #    17:          82    task #    18:         581    task #    19:         579    task #    20:         529

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1    1.00000      -4.317642E-13   1.704964E-02  -3.532659E-03  -1.315721E-03  -1.225645E-04

          calcsovref: scrb block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 civs   1   0.968731      -0.196600       0.362659       0.319418      -0.183752      -0.110753    
 civs   2   0.842449       0.564241       -1.17507        1.22649        2.18820        3.27600    
 civs   3   0.795524        4.63572       0.143654       -10.5568       -6.62869        14.0018    
 civs   4    1.01569        36.9290        3.82168        42.7940       -64.3681        14.3797    
 civs   5   -1.06183       -173.648        146.096        46.5343       -32.3198        175.446    
 civs   6   0.941226        440.782        719.036       -59.9337        550.613       -224.107    

 trial vector basis is being transformed.  new dimension:   1
 eci,repnuc=   -29.22882992567507        -71.09706155501468     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1   -100.3258914807  3.6597E-06  3.3639E-07  1.3891E-03  1.0000E-04
 mr-sdci #  5  2    -97.9982883352  5.5091E-01  0.0000E+00  1.4711E+00  1.0000E-04
 mr-sdci #  5  3    -95.4846659093  5.3908E-01  0.0000E+00  3.1297E+00  1.0000E-04
 mr-sdci #  5  4    -94.9410186487  7.1470E-01  0.0000E+00  3.1757E+00  1.0000E-04
 mr-sdci #  5  5    -93.5311716667  8.2587E-01  0.0000E+00  3.6290E+00  1.0000E-04
 mr-sdci #  5  6    -92.6203772724  2.1523E+01  0.0000E+00  4.2594E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0001
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0002
    3   26    0     0.00     0.00     0.00     0.00         0.    0.0009
    4   27    0     0.01     0.00     0.00     0.01         0.    0.0005
    5   11    0     0.00     0.00     0.00     0.00         0.    0.0003
    6   15    0     0.01     0.00     0.00     0.01         0.    0.0015
    7   16    0     0.00     0.00     0.00     0.00         0.    0.0014
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0001
    9    5    0     0.01     0.00     0.00     0.01         0.    0.0007
   10    6    0     0.01     0.00     0.00     0.01         0.    0.0007
   11   18    0     0.00     0.00     0.00     0.00         0.    0.0007
   12    7    0     0.01     0.00     0.00     0.01         0.    0.0006
   13   19    0     0.00     0.00     0.00     0.00         0.    0.0006
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0003
   15   43    0     0.01     0.00     0.00     0.01         0.    0.0003
   16   44    0     0.01     0.00     0.00     0.01         0.    0.0003
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0001
   18   76    0     0.00     0.00     0.00     0.00         0.    0.0006
   19   77    0     0.00     0.00     0.00     0.00         0.    0.0006
   20   78    0     0.01     0.00     0.01     0.00         0.    0.0001
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0800s 
time spent in multnx:                   0.0700s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.0800s 

          starting ci iteration   6

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.000223
ci vector #   2dasum_wr=    0.002433
ci vector #   3dasum_wr=    0.003915
ci vector #   4dasum_wr=    0.005377


====================================================================================================
Diagonal     counts:  0x:        1540 2x:         545 4x:         129
All internal counts: zz :          55 yy:         391 xx:         830 ww:         824
One-external counts: yz :         349 yx:        1619 yw:        1632
Two-external counts: yy :         470 ww:         852 xx:         968 xz:          68 wz:          93 wx:        1100
Three-ext.   counts: yx :         426 yw:         455

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         127    task #     2:         163    task #     3:         899    task #     4:         449
task #     5:         332    task #     6:        1463    task #     7:        1441    task #     8:          54
task #     9:         669    task #    10:         741    task #    11:         741    task #    12:         595
task #    13:         595    task #    14:         297    task #    15:         299    task #    16:         299
task #    17:          82    task #    18:         581    task #    19:         579    task #    20:         529

          calcsovref: tciref block   1

              civs   1       civs   2
 refs   1   0.979988       2.515792E-05

          calcsovref: scrb block   1

              civs   1       civs   2
 civs   1   0.999981      -6.959293E-02
 civs   2   0.917426        3275.86    
 eci,repnuc=   -29.22883023428934        -71.09706155501468     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  6  1   -100.3258917893  3.0861E-07  6.2670E-08  4.9498E-04  1.0000E-04
 mr-sdci #  6  2    -96.3910700353 -1.6072E+00  0.0000E+00  3.2862E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.01     0.00     0.00     0.01         0.    0.0001
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0002
    3   26    0     0.01     0.00     0.00     0.01         0.    0.0009
    4   27    0     0.00     0.00     0.00     0.00         0.    0.0005
    5   11    0     0.00     0.00     0.00     0.00         0.    0.0003
    6   15    0     0.01     0.00     0.00     0.01         0.    0.0015
    7   16    0     0.01     0.00     0.00     0.01         0.    0.0014
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0001
    9    5    0     0.00     0.00     0.00     0.00         0.    0.0007
   10    6    0     0.00     0.00     0.00     0.00         0.    0.0007
   11   18    0     0.00     0.00     0.00     0.00         0.    0.0007
   12    7    0     0.02     0.00     0.01     0.01         0.    0.0006
   13   19    0     0.00     0.00     0.00     0.00         0.    0.0006
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0003
   15   43    0     0.01     0.00     0.00     0.01         0.    0.0003
   16   44    0     0.00     0.00     0.00     0.00         0.    0.0003
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0001
   18   76    0     0.00     0.00     0.00     0.00         0.    0.0006
   19   77    0     0.01     0.00     0.00     0.00         0.    0.0006
   20   78    0     0.00     0.00     0.00     0.00         0.    0.0001
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0800s 
time spent in multnx:                   0.0600s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.0800s 

          starting ci iteration   7

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.000230
ci vector #   2dasum_wr=    0.000798
ci vector #   3dasum_wr=    0.001285
ci vector #   4dasum_wr=    0.001889


====================================================================================================
Diagonal     counts:  0x:        1540 2x:         545 4x:         129
All internal counts: zz :          55 yy:         391 xx:         830 ww:         824
One-external counts: yz :         349 yx:        1619 yw:        1632
Two-external counts: yy :         470 ww:         852 xx:         968 xz:          68 wz:          93 wx:        1100
Three-ext.   counts: yx :         426 yw:         455

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         127    task #     2:         163    task #     3:         899    task #     4:         449
task #     5:         332    task #     6:        1463    task #     7:        1441    task #     8:          54
task #     9:         669    task #    10:         741    task #    11:         741    task #    12:         595
task #    13:         595    task #    14:         297    task #    15:         299    task #    16:         299
task #    17:          82    task #    18:         581    task #    19:         579    task #    20:         529

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3
 refs   1   0.979988       2.515792E-05  -7.425276E-06

          calcsovref: scrb block   1

              civs   1       civs   2       civs   3
 civs   1   0.999982       1.062712E-02   7.480709E-02
 civs   2    1.11972       -2044.63       -2593.83    
 civs   3    1.08585       -5770.09        3456.77    
 eci,repnuc=   -29.22883030233999        -71.09706155501468     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  7  1   -100.3258918574  6.8051E-08  9.2096E-09  2.3140E-04  1.0000E-04
 mr-sdci #  7  2    -98.0595729400  1.6685E+00  0.0000E+00  2.1314E+00  1.0000E-04
 mr-sdci #  7  3    -95.7922422886  3.0758E-01  0.0000E+00  3.2647E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0001
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0002
    3   26    0     0.01     0.00     0.00     0.01         0.    0.0009
    4   27    0     0.01     0.01     0.00     0.01         0.    0.0005
    5   11    0     0.00     0.00     0.00     0.00         0.    0.0003
    6   15    0     0.01     0.00     0.01     0.01         0.    0.0015
    7   16    0     0.00     0.00     0.00     0.00         0.    0.0014
    8    1    0     0.01     0.00     0.01     0.00         0.    0.0001
    9    5    0     0.00     0.00     0.00     0.00         0.    0.0007
   10    6    0     0.01     0.00     0.00     0.01         0.    0.0007
   11   18    0     0.00     0.00     0.00     0.00         0.    0.0007
   12    7    0     0.01     0.00     0.00     0.01         0.    0.0006
   13   19    0     0.00     0.00     0.00     0.00         0.    0.0006
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0003
   15   43    0     0.00     0.00     0.00     0.00         0.    0.0003
   16   44    0     0.01     0.00     0.00     0.01         0.    0.0003
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0001
   18   76    0     0.00     0.00     0.00     0.00         0.    0.0006
   19   77    0     0.00     0.00     0.00     0.00         0.    0.0006
   20   78    0     0.00     0.00     0.00     0.00         0.    0.0001
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0700s 
time spent in multnx:                   0.0600s 
integral transfer time:                 0.0200s 
time spent for loop construction:       0.0100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.0800s 

          starting ci iteration   8

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.000074
ci vector #   2dasum_wr=    0.000337
ci vector #   3dasum_wr=    0.000662
ci vector #   4dasum_wr=    0.000874


====================================================================================================
Diagonal     counts:  0x:        1540 2x:         545 4x:         129
All internal counts: zz :          55 yy:         391 xx:         830 ww:         824
One-external counts: yz :         349 yx:        1619 yw:        1632
Two-external counts: yy :         470 ww:         852 xx:         968 xz:          68 wz:          93 wx:        1100
Three-ext.   counts: yx :         426 yw:         455

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         127    task #     2:         163    task #     3:         899    task #     4:         449
task #     5:         332    task #     6:        1463    task #     7:        1441    task #     8:          54
task #     9:         669    task #    10:         741    task #    11:         741    task #    12:         595
task #    13:         595    task #    14:         297    task #    15:         299    task #    16:         299
task #    17:          82    task #    18:         581    task #    19:         579    task #    20:         529

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4
 refs   1   0.979988       2.515792E-05  -7.425276E-06   1.709177E-05

          calcsovref: scrb block   1

              civs   1       civs   2       civs   3       civs   4
 civs   1   0.999966      -0.149703       0.184376      -0.253707    
 civs   2    1.14667        1692.20       -1650.47       -2449.64    
 civs   3    1.23049        5109.38        4343.76       -525.320    
 civs   4   0.984247        8194.13       -7149.26        17377.4    
 eci,repnuc=   -29.22883031140450        -71.09706155501468     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  8  1   -100.3258918664  9.0645E-09  1.4226E-09  8.2505E-05  1.0000E-04
 mr-sdci #  8  2    -98.6164670791  5.5689E-01  0.0000E+00  1.2865E+00  1.0000E-04
 mr-sdci #  8  3    -95.9497476251  1.5751E-01  0.0000E+00  3.0431E+00  1.0000E-04
 mr-sdci #  8  4    -94.9252119188 -1.5807E-02  0.0000E+00  3.8264E+00  1.0000E-04

======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.01     0.00     0.00     0.01         0.    0.0001
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0002
    3   26    0     0.00     0.00     0.00     0.00         0.    0.0009
    4   27    0     0.01     0.00     0.00     0.01         0.    0.0005
    5   11    0     0.00     0.00     0.00     0.00         0.    0.0003
    6   15    0     0.01     0.00     0.01     0.01         0.    0.0015
    7   16    0     0.01     0.00     0.00     0.01         0.    0.0014
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0001
    9    5    0     0.00     0.00     0.00     0.00         0.    0.0007
   10    6    0     0.00     0.00     0.00     0.00         0.    0.0007
   11   18    0     0.01     0.00     0.00     0.01         0.    0.0007
   12    7    0     0.01     0.00     0.01     0.00         0.    0.0006
   13   19    0     0.01     0.00     0.00     0.01         0.    0.0006
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0003
   15   43    0     0.00     0.00     0.00     0.00         0.    0.0003
   16   44    0     0.00     0.00     0.00     0.00         0.    0.0003
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0001
   18   76    0     0.00     0.00     0.00     0.00         0.    0.0006
   19   77    0     0.01     0.00     0.01     0.00         0.    0.0006
   20   78    0     0.00     0.00     0.00     0.00         0.    0.0001
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0800s 
time spent in multnx:                   0.0600s 
integral transfer time:                 0.0300s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.0900s 

 mr-sdci  convergence criteria satisfied after  8 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  8  1   -100.3258918664  9.0645E-09  1.4226E-09  8.2505E-05  1.0000E-04
 mr-sdci #  8  2    -98.6164670791  5.5689E-01  0.0000E+00  1.2865E+00  1.0000E-04
 mr-sdci #  8  3    -95.9497476251  1.5751E-01  0.0000E+00  3.0431E+00  1.0000E-04
 mr-sdci #  8  4    -94.9252119188 -1.5807E-02  0.0000E+00  3.8264E+00  1.0000E-04

####################CIUDGINFO####################

   ci vector at position   1 energy= -100.325891866419

################END OF CIUDGINFO################


    1 of the   5 expansion vectors are transformed.
    1 of the   4 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
 maximum overlap with reference                         1 (overlap= 
   0.9799910016705295      )

information on vector: 1from unit 11 written to unit 48filename civout              


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =      -100.3258918664

                                                       internal orbitals

                                          level       1    2    3    4    5    6

                                          orbital     1    2    3    4   21   35

                                         symmetry   a1   a1   a1   a1   b1   b2 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1  0.012600                        +-   +-   +-   +-   +-      
 z*  1  1       2  0.012600                        +-   +-   +-   +-        +- 
 z*  1  1       3 -0.976408                        +-   +-   +-        +-   +- 
 z*  1  1       5  0.082412                        +-   +-        +-   +-   +- 
 z*  1  1       8  0.011029                        +-        +-   +-   +-   +- 
 y   1  1     121  0.015639              1( b2 )   +-   +-    -   +    +-    - 
 y   1  1     130 -0.015639              1( b1 )   +-   +-    -   +     -   +- 
 y   1  1     159  0.047194              1( b2 )   +-   +-   +     -   +-    - 
 y   1  1     160 -0.028746              2( b2 )   +-   +-   +     -   +-    - 
 y   1  1     163 -0.011331              5( b2 )   +-   +-   +     -   +-    - 
 y   1  1     164  0.012596              6( b2 )   +-   +-   +     -   +-    - 
 y   1  1     168 -0.047194              1( b1 )   +-   +-   +     -    -   +- 
 y   1  1     169  0.028746              2( b1 )   +-   +-   +     -    -   +- 
 y   1  1     172  0.011331              5( b1 )   +-   +-   +     -    -   +- 
 y   1  1     173 -0.012596              6( b1 )   +-   +-   +     -    -   +- 
 y   1  1     300  0.014444              2( a1 )   +-    -   +     -   +-   +- 
 y   1  1     301  0.016203              3( a1 )   +-    -   +     -   +-   +- 
 y   1  1     302 -0.019475              4( a1 )   +-    -   +     -   +-   +- 
 y   1  1     337  0.015154              3( b2 )   +-   +    +-    -   +-    - 
 y   1  1     346 -0.015154              3( b1 )   +-   +    +-    -    -   +- 
 y   1  1     374 -0.014598              4( a1 )   +-   +     -    -   +-   +- 
 y   1  1     377 -0.011382              7( a1 )   +-   +     -    -   +-   +- 
 x   1  1     801  0.030115    5( a1 )   1( a2 )   +-   +-   +-         -    - 
 x   1  1     861  0.020723    1( b1 )   1( b2 )   +-   +-   +-         -    - 
 x   1  1     862 -0.014210    2( b1 )   1( b2 )   +-   +-   +-         -    - 
 x   1  1     870 -0.014210    1( b1 )   2( b2 )   +-   +-   +-         -    - 
 x   1  1     871  0.022038    2( b1 )   2( b2 )   +-   +-   +-         -    - 
 x   1  1     881  0.011880    3( b1 )   3( b2 )   +-   +-   +-         -    - 
 x   1  1    1845  0.011262    3( b1 )   1( a2 )   +-   +-    -        +-    - 
 x   1  1    1896 -0.014716    2( a1 )   2( b2 )   +-   +-    -        +-    - 
 x   1  1    1915  0.011262    5( a1 )   3( b2 )   +-   +-    -        +-    - 
 x   1  1    1917  0.011147    7( a1 )   3( b2 )   +-   +-    -        +-    - 
 x   1  1    2040  0.014716    2( a1 )   2( b1 )   +-   +-    -         -   +- 
 x   1  1    2059  0.011262    5( a1 )   3( b1 )   +-   +-    -         -   +- 
 x   1  1    2061 -0.011147    7( a1 )   3( b1 )   +-   +-    -         -   +- 
 x   1  1    2175  0.011262    1( a2 )   3( b2 )   +-   +-    -         -   +- 
 x   1  1    3793  0.010991    4( a1 )   1( b2 )   +-    -   +-        +-    - 
 x   1  1    3809 -0.010141    4( a1 )   2( b2 )   +-    -   +-        +-    - 
 x   1  1    3937 -0.010991    4( a1 )   1( b1 )   +-    -   +-         -   +- 
 x   1  1    3953  0.010141    4( a1 )   2( b1 )   +-    -   +-         -   +- 
 w   1  1    8454  0.015781    5( a1 )   5( a1 )   +-   +-   +-        +-      
 w   1  1    8621  0.015780    1( a2 )   1( a2 )   +-   +-   +-        +-      
 w   1  1    8631  0.021854    1( b2 )   1( b2 )   +-   +-   +-        +-      
 w   1  1    8632 -0.022772    1( b2 )   2( b2 )   +-   +-   +-        +-      
 w   1  1    8633  0.023864    2( b2 )   2( b2 )   +-   +-   +-        +-      
 w   1  1    8636  0.013119    3( b2 )   3( b2 )   +-   +-   +-        +-      
 w   1  1    8647 -0.010147    2( b2 )   6( b2 )   +-   +-   +-        +-      
 w   1  1    8651  0.010202    6( b2 )   6( b2 )   +-   +-   +-        +-      
 w   1  1    8740 -0.019686    1( b1 )   1( b2 )   +-   +-   +-        +     - 
 w   1  1    8741  0.014194    2( b1 )   1( b2 )   +-   +-   +-        +     - 
 w   1  1    8749  0.014194    1( b1 )   2( b2 )   +-   +-   +-        +     - 
 w   1  1    8750 -0.021330    2( b1 )   2( b2 )   +-   +-   +-        +     - 
 w   1  1    8760 -0.011481    3( b1 )   3( b2 )   +-   +-   +-        +     - 
 w   1  1    8835  0.015781    5( a1 )   5( a1 )   +-   +-   +-             +- 
 w   1  1    8957  0.021854    1( b1 )   1( b1 )   +-   +-   +-             +- 
 w   1  1    8958 -0.022772    1( b1 )   2( b1 )   +-   +-   +-             +- 
 w   1  1    8959  0.023864    2( b1 )   2( b1 )   +-   +-   +-             +- 
 w   1  1    8962  0.013119    3( b1 )   3( b1 )   +-   +-   +-             +- 
 w   1  1    8973 -0.010147    2( b1 )   6( b1 )   +-   +-   +-             +- 
 w   1  1    8977  0.010202    6( b1 )   6( b1 )   +-   +-   +-             +- 
 w   1  1    9002  0.015780    1( a2 )   1( a2 )   +-   +-   +-             +- 
 w   1  1   10036 -0.010971    3( b1 )   1( a2 )   +-   +-   +         +-    - 
 w   1  1   10087  0.010159    2( a1 )   2( b2 )   +-   +-   +         +-    - 
 w   1  1   10106  0.010971    5( a1 )   3( b2 )   +-   +-   +         +-    - 
 w   1  1   10231 -0.010159    2( a1 )   2( b1 )   +-   +-   +          -   +- 
 w   1  1   10250  0.010971    5( a1 )   3( b1 )   +-   +-   +          -   +- 
 w   1  1   10366  0.010971    1( a2 )   3( b2 )   +-   +-   +          -   +- 
 w   1  1   11373  0.010646    2( a1 )   2( a1 )   +-   +-             +-   +- 
 w   1  1   11391  0.010689    6( a1 )   6( a1 )   +-   +-             +-   +- 
 w   1  1   11512  0.010228    3( b1 )   3( b1 )   +-   +-             +-   +- 
 w   1  1   11567  0.010228    3( b2 )   3( b2 )   +-   +-             +-   +- 
 w   1  1   12623 -0.013427    4( a1 )   1( b2 )   +-   +    +-        +-    - 
 w   1  1   12637 -0.011201    2( a1 )   2( b2 )   +-   +    +-        +-    - 
 w   1  1   12639  0.017618    4( a1 )   2( b2 )   +-   +    +-        +-    - 
 w   1  1   12703 -0.010413    4( a1 )   6( b2 )   +-   +    +-        +-    - 
 w   1  1   12767  0.013427    4( a1 )   1( b1 )   +-   +    +-         -   +- 
 w   1  1   12781  0.011201    2( a1 )   2( b1 )   +-   +    +-         -   +- 
 w   1  1   12783 -0.017618    4( a1 )   2( b1 )   +-   +    +-         -   +- 
 w   1  1   12847  0.010413    4( a1 )   6( b1 )   +-   +    +-         -   +- 
 w   1  1   13928  0.010549    2( a1 )   4( a1 )   +-   +     -        +-   +- 
 w   1  1   16099  0.012248    4( a1 )   4( a1 )   +-        +-        +-   +- 

 ci coefficient statistics:
           rq > 0.1                1
      0.1> rq > 0.01              80
     0.01> rq > 0.001           1615
    0.001> rq > 0.0001          4039
   0.0001> rq > 0.00001         7041
  0.00001> rq > 0.000001        2149
 0.000001> rq                   2232
           all                 17157
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:          95 2x:           0 4x:           0
All internal counts: zz :          55 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:         127    task #     2:         163    task #     3:         899    task #     4:         449
task #     5:         332    task #     6:        1463    task #     7:        1441    task #     8:          54
task #     9:         669    task #    10:         741    task #    11:         741    task #    12:         595
task #    13:         595    task #    14:         297    task #    15:         299    task #    16:         299
task #    17:          82    task #    18:         581    task #    19:         579    task #    20:         100
  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.012600067850     -1.256646005852
     2     2      0.012600067850     -1.256646005855
     3     3     -0.976407659150     97.717956930708
     4     4     -0.006000258718      0.592957345106
     5     5      0.082412022835     -8.281602275367
     6     6     -0.000420183388      0.039377901909
     7     7     -0.000743173839      0.067333323939
     8     8      0.011029181173     -1.099188845026

 number of reference csfs (nref) is     8.  root number (iroot) is  1.
 c0**2 =   0.96063956  c**2 (all zwalks) =   0.96063956

 pople ci energy extrapolation is computed with 10 correlated electrons.

 eref      =   -100.081742218220   "relaxed" cnot**2         =   0.960639556578
 eci       =   -100.325891866419   deltae = eci - eref       =  -0.244149648199
 eci+dv1   =   -100.335501704834   dv1 = (1-cnot**2)*deltae  =  -0.009609838414
 eci+dv2   =   -100.335895450332   dv2 = dv1 / cnot**2       =  -0.010003583913
 eci+dv3   =   -100.336322840352   dv3 = dv1 / (2*cnot**2-1) =  -0.010430973933
 eci+pople =   -100.334094118814   ( 10e- scaled deltae )    =  -0.252351900594
 passed aftci ... 
 readint2: molcas,dalton2=                        1                        0
 files%faoints=aoints              
molcasbasis:  20  10   4  10

 Integral file header information:
 Using SEWARD integrals


   ******  Basis set information:  ******

 Number of irreps:                  4
 Total number of basis functions:  44

 irrep no.              1    2    3    4
 irrep label           a1   b1   a2   b2 
 no. of bas.fcions.    20   10    4   10
 input basis function labels, i:bfnlab(i)=
H   1s     H   *s     H   *s     H   *pz    H   *pz    H   *d0    H   *d2+   F   1s     F   2s     F   *s  
F   *s     F   2pz    F   *pz    F   *pz    F   *d0    F   *d0    F   *d2+   F   *d2+   F   *f0    F   *f2+
H   *px    H   *px    H   *d1+   F   2px    F   *px    F   *px    F   *d1+   F   *d1+   F   *f1+   F   *f3+
H   *d2-   F   *d2-   F   *d2-   F   *f2-   H   *py    H   *py    H   *d1-   F   2py    F   *py    F   *py 
F   *d1-   F   *d1-   F   *f3-   F   *f1-
 map-vector 1 , imtype=                        3
   1   1   1   1   1   1   1   2   2   2   2   2   2   2   2   2   2   2   2   2
  1   1   1   2   2   2   2   2   2   2   1   2   2   2   1   1   1   2   2   2
  2   2   2   2
 map-vector 2 , imtype=                        4
   1   1   1   2   2   4   4   1   1   1   1   2   2   2   4   4   4   4   6   6
  2   2   4   2   2   2   4   4   6   6   4   4   4   6   2   2   4   2   2   2
  4   4   6   6
 <<< Entering RdOne >>>
 rc on entry:            0
 Label on entry:  MLTPL  0
 Comp on entry:          1
 SymLab on entry:        1
 Option on entry:        6
 picked LabelMLTPL  0 Component                        1 currop= 
                        1
 Computed Symlab=                        1
  I will close the file for you!

          overlap matrix SAO basis block   1

               sao   1        sao   2        sao   3        sao   4        sao   5        sao   6        sao   7        sao   8
  sao   1     1.00000000
  sao   2     0.97447731     1.00000000
  sao   3     0.86984181     0.78887809     1.00000000
  sao   4     0.00000000     0.00000000     0.00000000     1.00000000
  sao   5     0.00000000     0.00000000     0.00000000     0.61492356     1.00000000
  sao   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
  sao   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
  sao   8     0.05911140     0.06743507     0.05561031     0.04104207     0.13108980     0.12746029     0.00000000     1.00000000
  sao   9     0.49084273     0.49592760     0.47131392     0.28485591     0.58719620     0.27993617     0.00000000     0.00000029
  sao  10     0.21122260     0.23217693     0.19467288     0.18392315     0.40778364     0.35651489     0.00000000     0.55834817
  sao  11     0.64136164     0.62605044     0.64465485     0.23110630     0.56227706     0.13648451     0.00000000     0.16285663
  sao  12    -0.37254426    -0.42058338    -0.18140325    -0.31000965    -0.13871174    -0.16898404     0.00000000     0.00000000
  sao  13    -0.31114088    -0.35063667    -0.11851283    -0.41707795    -0.22046505    -0.20036660     0.00000000     0.00000000
  sao  14    -0.56255659    -0.62833307    -0.33645097    -0.07884573     0.03561921     0.09260922     0.00000000     0.00000000
  sao  15     0.04234456     0.03588607     0.00339274     0.19083866     0.00219189     0.12247687     0.00000000     0.00000000
  sao  16     0.20723129     0.18729841     0.02501434     0.25343197    -0.08891297    -0.02767215     0.00000000     0.00000000
  sao  17     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.05903711     0.00000000
  sao  18     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.24067146     0.00000000
  sao  19    -0.05225636    -0.02226505    -0.00080503    -0.16729315     0.03349404    -0.03157112     0.00000000     0.00000000
  sao  20     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.19224851     0.00000000

               sao   9        sao  10        sao  11        sao  12        sao  13        sao  14        sao  15        sao  16
  sao   9     1.00000000
  sao  10     0.66714794     1.00000000
  sao  11     0.89221651     0.52037470     1.00000000
  sao  12     0.00000000     0.00000000     0.00000000     1.00000000
  sao  13     0.00000000     0.00000000     0.00000000     0.93861578     1.00000000
  sao  14     0.00000000     0.00000000     0.00000000     0.72541428     0.64084785     1.00000000
  sao  15     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
  sao  16     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.50517544     1.00000000
  sao  17     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
  sao  19     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
  sao  20     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

               sao  17        sao  18        sao  19        sao  20
  sao  17     1.00000000
  sao  18     0.50517544     1.00000000
  sao  19     0.00000000     0.00000000     1.00000000
  sao  20     0.00000000     0.00000000     0.00000000     1.00000000

          overlap matrix SAO basis block   2

               sao  21        sao  22        sao  23        sao  24        sao  25        sao  26        sao  27        sao  28
  sao  21     1.00000000
  sao  22     0.61492356     1.00000000
  sao  23     0.00000000     0.00000000     1.00000000
  sao  24     0.18446324     0.41505806     0.32427560     1.00000000
  sao  25     0.18201399     0.35646190     0.38055058     0.93861578     1.00000000
  sao  26     0.23558499     0.59841627     0.21903729     0.72541428     0.64084785     1.00000000
  sao  27    -0.08800157    -0.07574290    -0.21771107     0.00000000     0.00000000     0.00000000     1.00000000
  sao  28    -0.37809183    -0.37224003    -0.43546125     0.00000000     0.00000000     0.00000000     0.50517544     1.00000000
  sao  29     0.22479575     0.07582204     0.24920595     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
  sao  30     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

               sao  29        sao  30
  sao  29     1.00000000
  sao  30     0.00000000     1.00000000

          overlap matrix SAO basis block   3

               sao  31        sao  32        sao  33        sao  34
  sao  31     1.00000000
  sao  32     0.05903711     1.00000000
  sao  33     0.24067146     0.50517544     1.00000000
  sao  34    -0.19224851     0.00000000     0.00000000     1.00000000

          overlap matrix SAO basis block   4

               sao  35        sao  36        sao  37        sao  38        sao  39        sao  40        sao  41        sao  42
  sao  35     1.00000000
  sao  36     0.61492356     1.00000000
  sao  37     0.00000000     0.00000000     1.00000000
  sao  38     0.18446324     0.41505806     0.32427560     1.00000000
  sao  39     0.18201399     0.35646190     0.38055058     0.93861578     1.00000000
  sao  40     0.23558499     0.59841627     0.21903729     0.72541428     0.64084785     1.00000000
  sao  41    -0.08800157    -0.07574290    -0.21771107     0.00000000     0.00000000     0.00000000     1.00000000
  sao  42    -0.37809183    -0.37224003    -0.43546125     0.00000000     0.00000000     0.00000000     0.50517544     1.00000000
  sao  43     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
  sao  44     0.22479575     0.07582204     0.24920595     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

               sao  43        sao  44
  sao  43     1.00000000
  sao  44     0.00000000     1.00000000
NO coefficients and occupation numbers are written to nocoef_ci.1

################################################################################
 one electron density matrix calculation 
################################################################################


 workspace allocation information: mxcore=       32766638
 computing final density
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
 1-DENSITY: executing task                        11                        1
 1-DENSITY: executing task                        13                        3
 1-DENSITY: executing task                        14                        4
 1-DENSITY: executing task                         1                        1
 1-DENSITY: executing task                         2                        2
 1-DENSITY: executing task                         3                        3
 1-DENSITY: executing task                         4                        4
 1-DENSITY: executing task                        71                        1
 1-DENSITY: executing task                        52                        2
 1-DENSITY: executing task                        72                        2
 1-DENSITY: executing task                        53                        3
 1-DENSITY: executing task                        73                        3
 1-DENSITY: executing task                        54                        4
 1-DENSITY: executing task                        74                        4
================================================================================
   DYZ=      43  DYX=     167  DYW=     172
   D0Z=       9  D0Y=      68  D0X=      75  D0W=      74
  DDZI=      35 DDYI=     180 DDXI=     180 DDWI=     185
  DDZE=       0 DDYE=      40 DDXE=      42 DDWE=      47
================================================================================
 Trace of MO density:     9.999999999999998     
                       10 correlated and                         0 
 frozen core electrons


*****   symmetry block SYM1   *****

               no    1        no    2        no    3        no    4        no    5        no    6        no    7        no    8

      occ    2.00000        1.98812        1.96606       2.636714E-02   7.699992E-03   4.776238E-03   4.441133E-03   1.104396E-03

  mo    1    1.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  mo    2    0.00000       0.988297       0.152511       2.166049E-04   1.251420E-03  -9.665618E-04    0.00000      -4.631586E-04
  mo    3    0.00000      -0.152512       0.988282      -5.395755E-03   2.371623E-04  -1.410102E-03    0.00000      -9.658835E-04
  mo    4    0.00000      -9.691613E-04   5.308184E-03   0.994081      -7.122879E-02  -3.374641E-02    0.00000      -5.483661E-02
  mo    5    0.00000       1.285907E-03  -1.301833E-03  -5.339480E-03  -0.214738      -0.234535        0.00000       1.125004E-02
  mo    6    0.00000       1.674180E-03   1.695981E-03  -5.275549E-02  -0.503370       0.459238        0.00000      -0.303010    
  mo    7    0.00000       8.936464E-04   9.820053E-04  -1.460806E-02  -0.352369      -8.033406E-02    0.00000       0.495800    
  mo    8    0.00000      -1.611041E-04   8.990258E-04   6.100076E-02   0.678572       0.411182        0.00000       3.572935E-02
  mo    9    0.00000        0.00000        0.00000        0.00000        0.00000        0.00000       0.926888        0.00000    
  mo   10    0.00000      -2.597281E-04  -6.550584E-04   7.544865E-03   8.626006E-02  -0.496968        0.00000       6.109960E-02
  mo   11    0.00000       3.853208E-04  -1.003633E-04   4.818372E-02  -0.153036       0.479647        0.00000       0.273082    
  mo   12    0.00000        0.00000        0.00000        0.00000        0.00000        0.00000       0.152835        0.00000    
  mo   13    0.00000       1.419097E-04   7.515824E-04  -1.299087E-03   6.374716E-02  -9.521727E-02    0.00000       0.251257    
  mo   14    0.00000      -9.607792E-05   1.123917E-03   2.497431E-02  -6.447531E-02   8.464495E-02    0.00000       0.577112    
  mo   15    0.00000       7.842019E-04   1.279490E-03   1.283270E-02   0.204949      -7.584889E-02    0.00000       0.294150    
  mo   16    0.00000        0.00000        0.00000        0.00000        0.00000        0.00000      -5.271391E-02    0.00000    
  mo   17    0.00000       7.020081E-04   7.366269E-04   3.362471E-02   0.121279       0.144336        0.00000       0.153929    
  mo   18    0.00000        0.00000        0.00000        0.00000        0.00000        0.00000       0.338735        0.00000    
  mo   19    0.00000       1.659831E-04   1.921879E-04   2.542281E-02  -2.514531E-02   0.193271        0.00000       0.217388    
  mo   20    0.00000       8.162865E-04  -1.505293E-04  -9.068645E-03   0.119917       1.481094E-02    0.00000      -0.161195    

               no    9        no   10        no   11        no   12        no   13        no   14        no   15        no   16

      occ   7.444930E-04   3.624208E-04   3.518526E-04   3.001588E-04   2.187580E-04   1.296001E-04   8.075965E-05   4.725083E-05

  mo    2   3.926719E-04    0.00000      -1.146380E-03    0.00000      -1.859871E-03   3.789217E-04  -3.778332E-04    0.00000    
  mo    3   2.090471E-03    0.00000      -1.032603E-03    0.00000      -2.136176E-04  -3.354468E-04   3.146247E-04    0.00000    
  mo    4  -4.397128E-02    0.00000       1.932786E-02    0.00000      -1.809773E-03   1.076454E-02   1.096434E-02    0.00000    
  mo    5   0.310454        0.00000      -1.337199E-02    0.00000       0.375281      -0.116406      -3.582950E-02    0.00000    
  mo    6  -0.261226        0.00000       0.362088        0.00000       0.180754      -4.551908E-02  -0.205694        0.00000    
  mo    7  -0.194195        0.00000       3.772130E-02    0.00000      -6.126436E-02  -0.250156       0.566940        0.00000    
  mo    8  -6.472640E-02    0.00000       9.125478E-02    0.00000      -0.105898      -0.224356       0.106310        0.00000    
  mo    9    0.00000      -0.244727        0.00000      -0.269896        0.00000        0.00000        0.00000      -9.024322E-02
  mo   10   0.311497        0.00000       0.209852        0.00000      -1.754206E-02  -0.297490      -0.191477        0.00000    
  mo   11   0.531717        0.00000       4.539960E-02    0.00000      -0.102529      -0.436384      -3.485580E-02    0.00000    
  mo   12    0.00000       0.403526        0.00000      -0.139050        0.00000        0.00000        0.00000       0.891332    
  mo   13  -0.474048        0.00000      -0.123406        0.00000       0.123413      -8.548951E-02  -5.557649E-02    0.00000    
  mo   14  -0.185485        0.00000      -1.661432E-02    0.00000      -0.137139       0.175399      -0.294847        0.00000    
  mo   15  -2.360779E-02    0.00000       0.749932        0.00000       0.295774       0.148731      -0.167950        0.00000    
  mo   16    0.00000       0.655485        0.00000      -0.645518        0.00000        0.00000        0.00000      -0.388416    
  mo   17  -2.823196E-02    0.00000      -0.432122        0.00000       0.732151      -0.151241      -0.194141        0.00000    
  mo   18    0.00000       0.589589        0.00000       0.700805        0.00000        0.00000        0.00000      -0.215675    
  mo   19   0.388732        0.00000      -7.695793E-02    0.00000       9.689156E-02   0.713560       0.183411        0.00000    
  mo   20  -1.390394E-02    0.00000       0.204775        0.00000       0.365711      -1.341416E-02   0.629383        0.00000    

               no   17        no   18        no   19        no   20

      occ   3.240680E-05   1.111039E-05   7.268943E-06   3.693592E-06

  mo    2   1.005815E-04  -2.929824E-04  -6.331546E-04   4.266316E-05
  mo    3   9.419832E-04   2.522526E-04   1.539663E-04   2.453272E-04
  mo    4  -1.988561E-03  -1.697810E-03   2.602365E-04   1.148055E-03
  mo    5   0.415450       0.272664       0.627949       7.230296E-02
  mo    6  -0.283876       0.232161       0.115042      -0.130368    
  mo    7  -0.287142      -0.225465       0.250348      -2.244386E-02
  mo    8  -0.136546       0.185742       0.467469      -9.181074E-02
  mo   10  -0.525311       0.341875      -0.133933      -0.253936    
  mo   11   0.159958       5.915433E-02  -0.318932       0.227765    
  mo   13  -7.302162E-03   0.550214      -0.178866       0.568663    
  mo   14   0.306995       0.217097      -4.172820E-02  -0.583685    
  mo   15   7.870107E-02  -0.326299      -3.096608E-02   0.226189    
  mo   17  -0.231664      -0.265331      -9.315675E-02  -0.150831    
  mo   19  -0.352096       0.246973       6.203338E-02   0.126741    
  mo   20   0.256990       0.279833      -0.379958      -0.313752    

               no    1        no    2        no    3        no    4        no    5        no    6        no    7        no    8

      occ    2.00000        1.98812        1.96606       2.636714E-02   7.699992E-03   4.776238E-03   4.441133E-03   1.104396E-03

  ao    1  -4.149729E-03   0.181966      -0.771401        1.52631      -0.336289       0.701128      -2.496930E-11   -1.97685    
  ao    2   5.412851E-03  -9.103124E-02   0.152689      -3.861039E-02   0.252127      -0.212185       2.498066E-11   0.325795    
  ao    3   1.314584E-03  -6.786344E-02   0.271669      -0.560722       0.175813      -0.347905       6.079814E-12   0.790203    
  ao    4  -2.935299E-05   1.031132E-02  -1.844950E-02   3.441910E-02  -5.152332E-02   8.424021E-02   2.791404E-12   0.105357    
  ao    5  -4.679854E-05   8.285659E-03  -1.737074E-02   0.117176      -2.958857E-02   2.719583E-02  -5.920924E-13   0.163102    
  ao    6  -6.748724E-06   2.025267E-03  -3.013201E-03   9.776072E-03  -3.065094E-02   4.840319E-02   6.975114E-13   9.107712E-02
  ao    7  -2.945984E-15   9.791932E-14   9.374127E-14  -1.636480E-13   2.267515E-14   1.223705E-12   2.678704E-02  -7.226339E-13
  ao    8   0.996184       7.633680E-02   1.066333E-02  -1.473505E-02  -4.265397E-02  -1.880887E-02  -1.048966E-12   5.429627E-02
  ao    9  -8.169733E-02   0.952143      -1.447814E-02  -0.366626        1.14378       0.193643      -3.926292E-12  -0.230059    
  ao   10   8.135892E-04   2.617864E-03   5.716126E-03  -4.085087E-02   0.575612       0.139332       8.665053E-13  -0.310246    
  ao   11  -2.318781E-03   1.862043E-02   0.130739      -0.223423       -1.69153      -0.492256      -7.733933E-13   0.816150    
  ao   12  -2.192866E-02   0.116001       0.821443        1.25181       0.653306      -0.485276      -1.054502E-12    1.86933    
  ao   13   3.344221E-03  -7.270305E-03  -1.087124E-02  -0.175628      -0.352039       0.285552       2.883339E-12   -1.53457    
  ao   14   7.305681E-04   1.207532E-02  -5.115301E-02  -0.491818      -0.515309       0.601666       1.775490E-12   -1.20906    
  ao   15   2.149093E-05   1.126566E-03  -8.128654E-03   2.334615E-02  -3.079518E-02   0.311059       4.516398E-13   0.393892    
  ao   16  -3.950033E-04   1.809671E-03  -2.156669E-02   4.613269E-02  -1.559913E-02   0.591535      -1.457613E-12   0.170165    
  ao   17  -2.050180E-14   3.880372E-14   1.863764E-14   1.110957E-13  -1.129560E-13  -3.003031E-13   0.388306      -1.338488E-13
  ao   18  -9.449420E-15   4.985184E-13   5.312248E-14  -3.657016E-13   8.961618E-14   1.343000E-12   0.739550      -1.884774E-13
  ao   19   1.467698E-04  -5.543350E-04   4.440862E-03   7.596333E-03  -3.612188E-03  -3.556699E-02   2.956330E-13   3.811688E-02
  ao   20   3.731361E-15  -1.207271E-14  -7.422838E-15   6.551011E-15  -2.733529E-14   4.612866E-14  -2.100734E-02  -2.661719E-14

               no    9        no   10        no   11        no   12        no   13        no   14        no   15        no   16

      occ   7.444930E-04   3.624208E-04   3.518526E-04   3.001588E-04   2.187580E-04   1.296001E-04   8.075965E-05   4.725083E-05

  ao    1   -3.38565       1.095882E-11   -1.23087       1.115376E-11  -0.888249        4.95351        4.72651       6.646574E-12
  ao    2    2.11357      -1.309552E-11    1.47660      -9.649773E-12  -0.736340       -3.92827       -1.73830      -1.382844E-11
  ao    3    1.47235      -3.044337E-12   0.393010      -3.296002E-12   0.648540       -1.99289       -1.67147      -1.683973E-12
  ao    4  -0.253478      -8.746743E-13  -0.301112      -1.365890E-13  -0.378445       0.133955       0.811012      -1.333857E-12
  ao    5   9.761668E-02  -2.065056E-12   0.274097      -4.675612E-13  -0.615449      -0.968416        1.37300      -4.142360E-12
  ao    6  -1.729155E-02  -6.722564E-13  -9.078705E-02  -7.288635E-14  -0.305907       6.699729E-02   0.302367      -1.299656E-12
  ao    7  -7.357123E-13  -0.225779      -6.279775E-14   5.890953E-03  -7.756033E-15   5.076596E-13   6.147903E-13   -1.02934    
  ao    8  -2.975229E-02   2.611154E-13  -0.330049      -1.326386E-13   -1.65264       0.146761       -1.60725       6.712164E-13
  ao    9  -3.534843E-02   1.486594E-12  -0.610922       2.054128E-13   -3.45439       0.376030       -3.88897       2.427259E-12
  ao   10   1.275117E-02   9.036310E-14   0.514171       4.758559E-13    2.36407      -0.132794        2.07114      -4.845476E-13
  ao   11  -1.164617E-02   3.057541E-12  -0.107678       8.411433E-13    3.27892       0.756596       0.794151       6.130768E-12
  ao   12   -1.36538      -1.213569E-13    1.02956       2.330089E-13   0.773571       8.741486E-02   -1.51415      -5.988218E-13
  ao   13    1.34520      -1.340924E-12   -1.35902      -6.264516E-13   -1.40424      -0.190871        2.66126      -1.876069E-12
  ao   14  -0.136987      -1.583006E-12   0.570929      -2.957874E-13  -0.303718      -0.197403       0.311302      -3.195112E-12
  ao   15   0.388707       4.431434E-13  -0.360590       6.478073E-13   0.294362       0.735359      -4.860556E-02  -2.958328E-13
  ao   16   0.184990       5.003189E-13   0.395909      -6.077273E-13   0.103206      -0.968681      -0.630960       2.135496E-12
  ao   17   1.092466E-14   0.629527       2.552736E-13   0.870488      -3.497041E-13  -9.635756E-13  -6.254277E-14  -0.213654    
  ao   18  -2.812141E-12  -0.434099      -1.909099E-13  -0.723406       4.721755E-14   1.908210E-12   2.012260E-12   0.415949    
  ao   19   0.241454       8.617158E-14   0.672576       1.610585E-13  -0.397591       0.520795       0.491709      -2.352955E-13
  ao   20  -8.232066E-14   0.738818      -6.105180E-14  -0.596012       1.382429E-14  -1.604354E-14  -3.398485E-15  -0.373563    

               no   17        no   18        no   19        no   20

      occ   3.240680E-05   1.111039E-05   7.268943E-06   3.693592E-06

  ao    1    1.63570        2.19315        1.11999       0.547743    
  ao    2   -5.56414        4.31550       0.493242       -7.58334    
  ao    3  -2.471657E-02  -0.636964        1.53830      -3.976134E-02
  ao    4   0.452462        1.98381      -0.698297      -0.580410    
  ao    5   -2.24414        1.67811        1.89829       -4.26541    
  ao    6   0.397431       0.578609      -3.094204E-02   -2.03286    
  ao    7   6.259874E-13  -9.628380E-13   5.851876E-13   6.952296E-13
  ao    8  -0.567732      -0.330981        1.34734        1.35268    
  ao    9   -1.57479      -0.861786        3.44080        3.75148    
  ao   10   0.525492       0.591271       -1.54994       -1.32054    
  ao   11    4.78512       -4.66428       -5.42192        4.34697    
  ao   12   0.312404      -0.809798       0.100171       0.501016    
  ao   13  -0.330556        2.27315      -0.198760       -2.76044    
  ao   14   -2.53185        2.96055        1.44043       -2.78161    
  ao   15  -0.545488       0.199857       0.112089       0.113999    
  ao   16   0.776809       -1.73169      -7.242019E-02   0.996889    
  ao   17  -2.424511E-14  -2.743117E-13   3.489203E-14  -6.374771E-14
  ao   18   1.733774E-13  -1.646177E-12   2.095624E-12   9.908753E-14
  ao   19   0.199391       0.526889      -0.106777      -9.290518E-02
  ao   20   6.556618E-14   2.962193E-14  -3.176298E-14   3.152266E-14


*****   symmetry block SYM2   *****

               no    1        no    2        no    3        no    4        no    5        no    6        no    7        no    8

      occ    1.97612       1.395767E-02   4.789111E-03   7.458495E-04   5.838681E-04   3.250565E-04   3.167216E-04   9.886909E-05

  mo    1   0.999974       4.003764E-03  -1.673970E-03  -5.177783E-03   2.162643E-03   9.261549E-05   5.510561E-07   6.863577E-04
  mo    2  -6.178838E-03   0.699844      -0.302365      -0.483517      -8.106696E-02   0.156010       6.620730E-04   0.158278    
  mo    3   1.411067E-03  -0.608004      -0.448967      -0.143692      -0.463524       0.209448       8.767700E-04   0.193882    
  mo    4   9.953461E-05  -0.122703       0.783839      -0.212509      -7.389115E-02  -1.548366E-02  -1.003385E-04   0.334997    
  mo    5   8.250208E-06  -6.738257E-02  -9.279749E-03   0.115132       0.351481       0.209623       9.516974E-04  -0.453010    
  mo    6  -1.728583E-03  -0.217536      -9.346218E-02  -0.147140       0.608737       0.309864       1.308078E-03  -8.394020E-02
  mo    7   2.997166E-03   0.270255       7.012551E-02   0.719537      -0.294686       0.430881       1.787948E-03  -1.662457E-04
  mo    8  -4.348654E-08   2.618868E-06   2.784213E-07   4.760018E-05   9.187536E-06  -4.247177E-03   0.999991       1.229778E-04
  mo    9  -2.489042E-04  -2.364757E-02   0.175718      -0.206259       1.202308E-02   0.774048       3.289070E-03   0.160158    
  mo   10  -1.517542E-04   4.255456E-03  -0.219049       0.324338       0.438206      -8.226520E-02  -4.563318E-04   0.766303    

               no    9        no   10

      occ   4.533601E-05   7.945511E-06

  mo    1  -1.170762E-03  -2.569033E-04
  mo    2  -0.359137       4.906298E-03
  mo    3  -0.331746       4.172412E-02
  mo    4  -0.455431      -6.395559E-03
  mo    5  -0.457787       0.632337    
  mo    6  -0.195637      -0.640618    
  mo    7  -0.251143      -0.262160    
  mo    8  -8.982453E-06  -2.896172E-05
  mo    9   0.491143       0.244629    
  mo   10  -3.754941E-02   0.243683    

               no    1        no    2        no    3        no    4        no    5        no    6        no    7        no    8

      occ    1.97612       1.395767E-02   4.789111E-03   7.458495E-04   5.838681E-04   3.250565E-04   3.167216E-04   9.886909E-05

  ao    1   1.125533E-02  -3.801180E-02   6.376500E-02   0.209106       0.324364       0.176747       8.370859E-04  -0.812370    
  ao    2   3.561922E-02  -0.143395       0.174195       0.576073       0.341041      -0.314550      -1.324136E-03  -0.506887    
  ao    3   6.338117E-03  -3.507654E-02   4.330165E-02   0.159333       0.192697       0.122278       5.085622E-04  -0.366922    
  ao    4   0.968787        1.66666       0.293357        2.02422       -1.61728       0.407193       1.642789E-03  -0.109524    
  ao    5  -1.341452E-02  -0.602950      -0.175495       -2.18286        1.76321      -0.524738      -2.134793E-03   0.204671    
  ao    6   3.177641E-02   -1.34704      -0.354819      -0.452069      -0.424807       0.199231       8.205743E-04   0.555428    
  ao    7  -3.700443E-03   1.787884E-02  -0.339552       0.434986       0.426217      -0.535292      -2.386151E-03   0.729182    
  ao    8  -1.254125E-02   5.636243E-02  -0.659286       0.118472       7.386182E-02   0.445910       2.012536E-03   -1.24438    
  ao    9   2.850727E-03   3.098884E-03   2.963448E-02   0.104910       0.280884       0.766588       3.190921E-03   0.593507    
  ao   10  -4.348655E-08   2.618868E-06   2.784213E-07   4.760018E-05   9.187536E-06  -4.247177E-03   0.999991       1.229778E-04

               no    9        no   10

      occ   4.533601E-05   7.945511E-06

  ao    1  -0.999499       0.287537    
  ao    2    1.21593       -1.07749    
  ao    3  -0.471802       -1.46361    
  ao    4  -0.475913      -0.343197    
  ao    5   0.755445       0.791843    
  ao    6  -0.558543       0.644730    
  ao    7  -0.307856       0.150630    
  ao    8   0.146235       -1.01529    
  ao    9   0.371208       0.424796    
  ao   10  -8.982453E-06  -2.896172E-05


*****   symmetry block SYM3   *****

               no    1        no    2        no    3        no    4

      occ   4.439968E-03   3.623208E-04   3.000226E-04   4.724498E-05

  mo    1   0.926889      -0.244969      -0.269674      -9.023792E-02
  mo    2   0.152835       0.403491      -0.139463       0.891284    
  mo    3  -5.267987E-02   0.654943      -0.645993      -0.388546    
  mo    4   0.338738       0.590114       0.700371      -0.215645    

               no    1        no    2        no    3        no    4

      occ   4.439968E-03   3.623208E-04   3.000226E-04   4.724498E-05

  ao    1   2.679671E-02  -0.225865       6.183160E-03   -1.02932    
  ao    2   0.388307       0.630185       0.870023      -0.213607    
  ao    3   0.739548      -0.434650      -0.723095       0.415918    
  ao    4  -2.097255E-02   0.738315      -0.596553      -0.373694    


*****   symmetry block SYM4   *****

               no    1        no    2        no    3        no    4        no    5        no    6        no    7        no    8

      occ    1.97612       1.395767E-02   4.789111E-03   7.458495E-04   5.838681E-04   3.250565E-04   3.167216E-04   9.886909E-05

  mo    1   0.999974       4.003764E-03  -1.673970E-03  -5.177783E-03   2.162643E-03   9.261549E-05  -5.510561E-07   6.863577E-04
  mo    2  -6.178838E-03   0.699844      -0.302365      -0.483517      -8.106696E-02   0.156010      -6.620730E-04   0.158278    
  mo    3   1.411067E-03  -0.608004      -0.448967      -0.143692      -0.463524       0.209448      -8.767700E-04   0.193882    
  mo    4   9.953461E-05  -0.122703       0.783839      -0.212509      -7.389115E-02  -1.548366E-02   1.003385E-04   0.334997    
  mo    5   8.250208E-06  -6.738257E-02  -9.279749E-03   0.115132       0.351481       0.209623      -9.516974E-04  -0.453010    
  mo    6  -1.728583E-03  -0.217536      -9.346218E-02  -0.147140       0.608737       0.309864      -1.308078E-03  -8.394020E-02
  mo    7   2.997166E-03   0.270255       7.012551E-02   0.719537      -0.294686       0.430881      -1.787948E-03  -1.662457E-04
  mo    8   4.348654E-08  -2.618868E-06  -2.784220E-07  -4.760018E-05  -9.187536E-06   4.247177E-03   0.999991      -1.229778E-04
  mo    9  -2.489042E-04  -2.364757E-02   0.175718      -0.206259       1.202308E-02   0.774048      -3.289070E-03   0.160158    
  mo   10  -1.517542E-04   4.255456E-03  -0.219049       0.324338       0.438206      -8.226520E-02   4.563318E-04   0.766303    

               no    9        no   10

      occ   4.533601E-05   7.945511E-06

  mo    1  -1.170762E-03  -2.569033E-04
  mo    2  -0.359137       4.906298E-03
  mo    3  -0.331746       4.172412E-02
  mo    4  -0.455431      -6.395559E-03
  mo    5  -0.457787       0.632337    
  mo    6  -0.195637      -0.640618    
  mo    7  -0.251143      -0.262160    
  mo    8   8.982453E-06   2.896172E-05
  mo    9   0.491143       0.244629    
  mo   10  -3.754941E-02   0.243683    

               no    1        no    2        no    3        no    4        no    5        no    6        no    7        no    8

      occ    1.97612       1.395767E-02   4.789111E-03   7.458495E-04   5.838681E-04   3.250565E-04   3.167216E-04   9.886909E-05

  ao    1   1.125533E-02  -3.801180E-02   6.376500E-02   0.209106       0.324364       0.176747      -8.370859E-04  -0.812370    
  ao    2   3.561922E-02  -0.143395       0.174195       0.576073       0.341041      -0.314550       1.324136E-03  -0.506887    
  ao    3   6.338117E-03  -3.507654E-02   4.330165E-02   0.159333       0.192697       0.122278      -5.085622E-04  -0.366922    
  ao    4   0.968787        1.66666       0.293357        2.02422       -1.61728       0.407193      -1.642789E-03  -0.109524    
  ao    5  -1.341452E-02  -0.602950      -0.175495       -2.18286        1.76321      -0.524738       2.134793E-03   0.204671    
  ao    6   3.177641E-02   -1.34704      -0.354819      -0.452069      -0.424807       0.199231      -8.205743E-04   0.555428    
  ao    7  -3.700443E-03   1.787884E-02  -0.339552       0.434986       0.426217      -0.535292       2.386151E-03   0.729182    
  ao    8  -1.254125E-02   5.636243E-02  -0.659286       0.118472       7.386182E-02   0.445910      -2.012536E-03   -1.24438    
  ao    9   4.348654E-08  -2.618868E-06  -2.784220E-07  -4.760018E-05  -9.187536E-06   4.247177E-03   0.999991      -1.229778E-04
  ao   10   2.850727E-03   3.098884E-03   2.963448E-02   0.104910       0.280884       0.766588      -3.190921E-03   0.593507    

               no    9        no   10

      occ   4.533601E-05   7.945511E-06

  ao    1  -0.999499       0.287537    
  ao    2    1.21593       -1.07749    
  ao    3  -0.471802       -1.46361    
  ao    4  -0.475913      -0.343197    
  ao    5   0.755445       0.791843    
  ao    6  -0.558543       0.644730    
  ao    7  -0.307856       0.150630    
  ao    8   0.146235       -1.01529    
  ao    9   8.982453E-06   2.896172E-05
  ao   10   0.371208       0.424796    


 total number of electrons =   10.0000000000

 test slabel:                        4
  a1  b1  a2  b2 


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!


                        a1  partial gross atomic populations
   ao class       1a1        2a1        3a1        4a1        5a1        6a1 
    H _ s       0.000218   0.023579   0.526303   0.015838  -0.000115   0.000166
    H _ p      -0.000009   0.014931   0.011699  -0.001455   0.000054   0.000100
    H _ d      -0.000001   0.001066   0.000757  -0.000093  -0.000040   0.000043
    F _ s       1.999088   1.925867  -0.027314   0.000050   0.006694   0.000302
    F _ p       0.000704   0.022561   1.446956   0.011543   0.001086   0.000743
    F _ d       0.000000   0.000102   0.007282   0.000498   0.000021   0.003409
    F _ f       0.000000   0.000010   0.000382  -0.000015   0.000000   0.000014

   ao class       7a1        8a1        9a1       10a1       11a1       12a1 
    H _ s       0.000000   0.000163   0.000277   0.000000   0.000015   0.000000
    H _ p       0.000000   0.000145   0.000033   0.000000   0.000014   0.000000
    H _ d       0.000028  -0.000004   0.000001   0.000036   0.000002   0.000000
    F _ s       0.000000   0.000054  -0.000001   0.000000   0.000008   0.000000
    F _ p       0.000000   0.000539   0.000248   0.000000   0.000082   0.000000
    F _ d       0.004411   0.000202   0.000112   0.000117   0.000049   0.000193
    F _ f       0.000002   0.000005   0.000075   0.000209   0.000181   0.000107

   ao class      13a1       14a1       15a1       16a1       17a1       18a1 
    H _ s      -0.000001   0.000035   0.000003   0.000000   0.000004   0.000004
    H _ p       0.000024   0.000013   0.000050   0.000000   0.000015   0.000005
    H _ d       0.000002   0.000003  -0.000010   0.000042   0.000005  -0.000001
    F _ s       0.000145  -0.000003   0.000016   0.000000   0.000001   0.000000
    F _ p       0.000015  -0.000001   0.000015   0.000000   0.000001   0.000001
    F _ d       0.000009   0.000062  -0.000001   0.000002   0.000005   0.000001
    F _ f       0.000025   0.000020   0.000007   0.000003   0.000000   0.000000

   ao class      19a1       20a1 
    H _ s       0.000004   0.000000
    H _ p       0.000001  -0.000001
    H _ d       0.000000   0.000004
    F _ s       0.000001   0.000000
    F _ p       0.000001   0.000000
    F _ d       0.000000   0.000000
    F _ f       0.000000   0.000000

                        b1  partial gross atomic populations
   ao class       1b1        2b1        3b1        4b1        5b1        6b1 
    H _ p       0.037614   0.001179   0.000399   0.000242   0.000127   0.000021
    H _ d       0.004125   0.000023   0.000075  -0.000028   0.000022   0.000008
    F _ p       1.933361   0.012623   0.000202   0.000387   0.000276   0.000011
    F _ d       0.000970   0.000134   0.004103   0.000127   0.000089   0.000082
    F _ f       0.000054  -0.000001   0.000010   0.000018   0.000070   0.000203

   ao class       7b1        8b1        9b1       10b1 
    H _ p       0.000000   0.000045   0.000031   0.000001
    H _ d       0.000000  -0.000012   0.000008   0.000007
    F _ p       0.000000   0.000001   0.000002   0.000000
    F _ d       0.000000   0.000048   0.000003   0.000000
    F _ f       0.000317   0.000016   0.000002   0.000000

                        a2  partial gross atomic populations
   ao class       1a2        2a2        3a2        4a2 
    H _ d       0.000028   0.000036   0.000000   0.000042
    F _ d       0.004410   0.000118   0.000193   0.000002
    F _ f       0.000002   0.000209   0.000107   0.000003

                        b2  partial gross atomic populations
   ao class       1b2        2b2        3b2        4b2        5b2        6b2 
    H _ p       0.037614   0.001179   0.000399   0.000242   0.000127   0.000021
    H _ d       0.004125   0.000023   0.000075  -0.000028   0.000022   0.000008
    F _ p       1.933361   0.012623   0.000202   0.000387   0.000276   0.000011
    F _ d       0.000970   0.000134   0.004103   0.000127   0.000089   0.000082
    F _ f       0.000054  -0.000001   0.000010   0.000018   0.000070   0.000203

   ao class       7b2        8b2        9b2       10b2 
    H _ p       0.000000   0.000045   0.000031   0.000001
    H _ d       0.000000  -0.000012   0.000008   0.000007
    F _ p       0.000000   0.000001   0.000002   0.000000
    F _ d       0.000000   0.000048   0.000003   0.000000
    F _ f       0.000317   0.000016   0.000002   0.000000


                        gross atomic populations
     ao           H _        F _
      s         0.566497   3.904909
      p         0.104936   5.378220
      d         0.010401   0.032311
      f         0.000000   0.002727
    total       0.681833   9.318167


 Total number of electrons:   10.00000000

========================GLOBAL TIMING PER NODE========================
   process    vectrn  integral   segment    diagon     dmain    davidc   finalvw   driver  
--------------------------------------------------------------------------------
   #   1         0.0       0.0       0.0       0.7       0.7       0.0       0.0       1.6
--------------------------------------------------------------------------------
       1         0.0       0.0       0.0       0.7       0.7       0.0       0.0       1.6
