License is going to expire in 252 days on Thursday April 30th, 2009
Everything is fine. Going to run MOLCAS!
                                                                                                   
                                              ^^^^^            M O L C A S                         
                                             ^^^^^^^           version 7.0 patchlevel 266          
                               ^^^^^         ^^^^^^^                                               
                              ^^^^^^^        ^^^ ^^^                                               
                              ^^^^^^^       ^^^^ ^^^                                               
                              ^^^ ^^^       ^^^^ ^^^                                               
                              ^^^ ^^^^      ^^^  ^^^                                               
                              ^^^  ^^ ^^^^^  ^^  ^^^^                                              
                             ^^^^      ^^^^   ^   ^^^                                              
                             ^   ^^^   ^^^^   ^^^^  ^                                              
                            ^   ^^^^    ^^    ^^^^   ^                                             
                        ^^^^^   ^^^^^   ^^   ^^^^^   ^^^^^                                         
                     ^^^^^^^^   ^^^^^        ^^^^^    ^^^^^^^                                      
                 ^^^^^^^^^^^    ^^^^^^      ^^^^^^^   ^^^^^^^^^^^                                  
               ^^^^^^^^^^^^^   ^^^^^^^^^^^^^^^^^^^^   ^^^^^^^^^^^^^                                
               ^^^^^^^^^^^^^   ^^^^             ^^^   ^^^      ^^^^                                
               ^^^^^^^^^^^^^                          ^^^      ^^^^                                
               ^^^^^^^^^^^^^       ^^^^^^^^^^^^        ^^      ^^^^                                
               ^^^^^^^^^^^^      ^^^^^^^^^^^^^^^^      ^^      ^^^^                                
               ^^^^^^^^^^^^    ^^^^^^^^^^^^^^^^^^^^    ^^      ^^^^                                
               ^^^^^^^^^^^^    ^^^^^^^^^^^^^^^^^^^^    ^^      ^^^^    ^^^^^     ^^^    ^^^^^^     
               ^^^^^^^^^^^^   ^^^^^^^^^^^^^^^^^^^^^^   ^^      ^^^^   ^^^^^^^    ^^^   ^^^  ^^^    
               ^^^^^^^^^^^    ^^^^^^^^^^^^^^^^^^^^^^   ^       ^^^^  ^^^    ^^   ^^^   ^^    ^^    
               ^^^^^^^^^^^     ^^^^^^^^^^^^^^^^^^^^            ^^^^  ^^         ^^ ^^  ^^^^^       
               ^^^^^^^^^^      ^^^^^^^^^^^^^^^^^^^^        ^   ^^^^  ^^         ^^ ^^   ^^^^^^     
               ^^^^^^^^          ^^^^^^^^^^^^^^^^          ^   ^^^^  ^^        ^^^^^^^     ^^^^    
               ^^^^^^      ^^^     ^^^^^^^^^^^^     ^      ^   ^^^^  ^^^   ^^^ ^^^^^^^ ^^    ^^    
                         ^^^^^^                    ^^      ^   ^^^^   ^^^^^^^  ^^   ^^ ^^^  ^^^    
                      ^^^^^^^^^^^^             ^^^^^^      ^           ^^^^^  ^^     ^^ ^^^^^^     
               ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                                
                 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                                  
                     ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                                      
                        ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                                         
                            ^^^^^^^^^^^^^^^^^^^^^^^^^^                                             
                               ^^^^^^^^^^^^^^^^^^^^                                                
                                   ^^^^^^^^^^^^                                                    
                                       ^^^^^^                                                      

                           Copyright, all rights, reserved:                                        
                         Permission is hereby granted to use                                       
                but not to reproduce or distribute any part of this                                
             program. The use is restricted to research purposes only.                             
                            Lund University Sweden, 2007.                                          
                                                                                                   
 For the author list and the recommended citation consult section 1.5 of the MOLCAS user's guide.  

 COLUMBUS MR-CISD gradients using ALASKA 


()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()
                                  MOLCAS executing module ALASKA with 500 MB of memory                                  
                                              at 17:05:21 Thu Aug 21 2008                                               
()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()



                     Threshold for contributions to the gradient: .100E-06


                    ********************************************
                    * Symmetry Adapted Cartesian Displacements *
                    ********************************************


           Irreducible representation : ag 
           Basis function(s) of irrep:                                                                                 

 Basis Label        Type   Center Phase
   1   C1           x         1     1      2    -1
   2   C2           x         3     1      4    -1      5     1      6    -1
   3   C2           z         3     1      4     1      5    -1      6    -1
   4   H2           x         7     1      8    -1      9     1     10    -1
   5   H2           z         7     1      8     1      9    -1     10    -1

           Irreducible representation : b3u
           Basis function(s) of irrep: x                                                                               

 Basis Label        Type   Center Phase
   6   C1           x         1     1      2     1
   7   C2           x         3     1      4     1      5     1      6     1
   8   C2           z         3     1      4    -1      5    -1      6     1
   9   H2           x         7     1      8     1      9     1     10     1
  10   H2           z         7     1      8    -1      9    -1     10     1

           Irreducible representation : b2u
           Basis function(s) of irrep: y                                                                               

 Basis Label        Type   Center Phase
  11   C1           y         1     1      2     1
  12   C2           y         3     1      4     1      5     1      6     1
  13   H2           y         7     1      8     1      9     1     10     1

           Irreducible representation : b1g
           Basis function(s) of irrep: xy, Rz                                                                          

 Basis Label        Type   Center Phase
  14   C1           y         1     1      2    -1
  15   C2           y         3     1      4    -1      5     1      6    -1
  16   H2           y         7     1      8    -1      9     1     10    -1

           Irreducible representation : b1u
           Basis function(s) of irrep: z                                                                               

 Basis Label        Type   Center Phase
  17   C1           z         1     1      2     1
  18   C2           x         3     1      4    -1      5    -1      6     1
  19   C2           z         3     1      4     1      5     1      6     1
  20   H2           x         7     1      8    -1      9    -1     10     1
  21   H2           z         7     1      8     1      9     1     10     1

           Irreducible representation : b2g
           Basis function(s) of irrep: xz, Ry                                                                          

 Basis Label        Type   Center Phase
  22   C1           z         1     1      2    -1
  23   C2           x         3     1      4     1      5    -1      6    -1
  24   C2           z         3     1      4    -1      5     1      6    -1
  25   H2           x         7     1      8     1      9    -1     10    -1
  26   H2           z         7     1      8    -1      9     1     10    -1

           Irreducible representation : b3g
           Basis function(s) of irrep: yz, Rx                                                                          

 Basis Label        Type   Center Phase
  27   C2           y         3     1      4     1      5    -1      6    -1
  28   H2           y         7     1      8     1      9    -1     10    -1

           Irreducible representation : au 
           Basis function(s) of irrep: I                                                                               

 Basis Label        Type   Center Phase
  29   C2           y         3     1      4    -1      5    -1      6     1
  30   H2           y         7     1      8    -1      9    -1     10     1

                     No automatic utilization of translational and rotational invariance of the energy is employed.


 ******************************************************************
 *                                                                *
 *              The Nuclear repulsion  contribution               *
 *                                                                *
 ******************************************************************

                Irreducible representation: ag 

                C1       x                 0.9547412E+01
                C2       x                 0.4290366E+01
                C2       z                -0.7176905E+01
                H2       x                 0.1242505E+01
                H2       z                -0.2100315E+01


 *********************************************************
 *                                                       *
 *              Total Nuclear contribution               *
 *                                                       *
 *********************************************************

                Irreducible representation: ag 

                C1       x                 0.9547412E+01
                C2       x                 0.4290366E+01
                C2       z                -0.7176905E+01
                H2       x                 0.1242505E+01
                H2       z                -0.2100315E+01


 **************************************************************************************************************
 *                                                                                                            *
 *                                      The Renormalization Contribution                                      *
 *                                                                                                            *
 **************************************************************************************************************

                Irreducible representation: ag 

                C1       x                -0.3015690E+00
                C2       x                 0.8560306E-03
                C2       z                 0.6012409E-01
                H2       x                -0.1114669E+00
                H2       z                 0.2049268E+00


 **************************************************************************************************************
 *                                                                                                            *
 *                                      The Kinetic Energy Contribution                                       *
 *                                                                                                            *
 **************************************************************************************************************

                Irreducible representation: ag 

                C1       x                 0.6519679E+00
                C2       x                 0.9514819E-01
                C2       z                -0.2491798E+00
                H2       x                 0.2263919E+00
                H2       z                -0.3991154E+00


 **************************************************************************************************************
 *                                                                                                            *
 *                                    The Nuclear Attraction Contribution                                     *
 *                                                                                                            *
 **************************************************************************************************************

                Irreducible representation: ag 

                C1       x                -0.2225147E+02
                C2       x                -0.8545984E+01
                C2       z                 0.1511104E+02
                H2       x                -0.3819890E+01
                H2       z                 0.6709391E+01

 Conventional ERI gradients!
 A total of 43584011. entities were prescreened and 28425831. were kept.

 ********************************************************
 *                                                      *
 *              Two-electron contribution               *
 *                                                      *
 ********************************************************

                Irreducible representation: ag 

                C1       x                 0.1175053E+02
                C2       x                 0.4161303E+01
                C2       z                -0.7624838E+01
                H2       x                 0.2239523E+01
                H2       z                -0.4005031E+01


 **************************************************
 *                                                *
 *              Molecular gradients               *
 *                                                *
 **************************************************

                Irreducible representation: ag 

                C1       x                 0.5844254E-05
                C2       x                -0.2258430E-04
                C2       z                -0.2076319E-05
                H2       x                -0.2486001E-05
                H2       z                 0.3403007E-05


  CGrad                                                       
  mat. size =     3x   10
    0.00000584 -0.00000584 -0.00002258  0.00002258 -0.00002258  0.00002258 -0.00000249  0.00000249 -0.00000249  0.00000249
    0.00000000  0.00000000  0.00000000  0.00000000  0.00000000  0.00000000  0.00000000  0.00000000  0.00000000  0.00000000
    0.00000000  0.00000000 -0.00000208 -0.00000208  0.00000208  0.00000208  0.00000340  0.00000340 -0.00000340 -0.00000340
--- Stop Module: alaska at Thu Aug 21 17:05:26 2008 /rc=                        0 ---

     Happy landing!

--- Stop Module: auto at Do Aug 21 17:05:26 2008 /rc=0 ---
