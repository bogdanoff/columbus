

     ******************************************
     **    PROGRAM:              MCPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 original author: Daniel Robertson, FSU
 later revisions: Ron Shepard, ANL;
                  Michal Dallos, University Vienna



 This Version of Program mcpc is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



   ******  File header section  ******

 Headers form the restart file:
    SEWARD INTEGRALS                                                                
     title                                                                          
     title                                                                          


   ******  DRT info section  ******

 Informations for the DRT no.  1
 Header form the DRT file: 
     title                                                                          
 Molecular symmetry group:   sym1
 Total number of electrons:    7
 Spin multiplicity:            2
 Number of active orbitals:    6
 Number of active electrons:   5
 Total number of CSFs:       115

   ***  Informations from the DRT number:   1


 Symmetry orbital summary:
 Symm.blocks:         1     2
 Symm.labels:         a'    a" 

 List of doubly occupied orbitals:
  1 a' 

 List of active orbitals:
  2 a'   3 a'   4 a'   5 a'   6 a'   1 a" 

 Informations for the DRT no.  2
 Header form the DRT file: 
     title                                                                          
 Molecular symmetry group:    a" 
 Total number of electrons:    7
 Spin multiplicity:            2
 Number of active orbitals:    6
 Number of active electrons:   5
 Total number of CSFs:        95

   ***  Informations from the DRT number:   2


 Symmetry orbital summary:
 Symm.blocks:         1     2
 Symm.labels:         a'    a" 

 List of doubly occupied orbitals:
  1 a' 

 List of active orbitals:
  2 a'   3 a'   4 a'   5 a'   6 a'   1 a" 


   ******  MCSCF convergence information:  ******

 MCSCF convergence criteria were satisfied.

 mcscf energy=   -25.6530063490    nuclear repulsion=     4.1965378929
 demc=             0.0000000000    wnorm=                 0.0000000151
 knorm=            0.0000000028    apxde=                 0.0000000000


 MCSCF calculation performmed for   2 symmetries.

 State averaging:
 No,  ssym, navst, wavst
  1    a'     2   0.3333 0.3333
  2    a"     1   0.3333

 Input the DRT No of interest: [  1]:
In the DRT No.: 1 there are  2 states.

 Which one to take? [  1]:
 The CSFs for the state No  1 of the symmetry  a'  will be printed
 according to the following print options :

 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 0) end.

 input menu number [  0]: csfs will be printed based on coefficient magnitudes.

 input the coefficient threshold (end with 0.) [ 0.0000]:
 List of active orbitals:
  2 a'   3 a'   4 a'   5 a'   6 a'   1 a" 

   csf       coeff       coeff**2    step(*)
  -----  ------------  ------------  ------------
      1  0.9592987030  0.9202540016  331000
      5  0.1181966182  0.0139704406  312100
     16  0.1053158447  0.0110914271  301300
      7 -0.1015853797  0.0103195894  311200
     39  0.0828500913  0.0068641376  121120
     35 -0.0756441201  0.0057220329  123100
     25  0.0741827597  0.0055030818  133000
      3  0.0708870306  0.0050249711  330010
     34 -0.0675154081  0.0045583303  130003
     81  0.0519528274  0.0026990963  031030
     38  0.0498277749  0.0024828072  121210
     15 -0.0432237830  0.0018682954  303010
     82  0.0399530101  0.0015962430  031003
     19  0.0394884627  0.0015593387  301030
     78  0.0380383878  0.0014469189  031300
     29 -0.0305421825  0.0009328249  131020
     11  0.0292550990  0.0008558608  310120
     77 -0.0233903147  0.0005471068  033010
     10  0.0204297960  0.0004173766  310210
     20  0.0190485170  0.0003628460  301003
     46 -0.0153651932  0.0002360892  113200
     49 -0.0149939316  0.0002248180  112210
     33  0.0147165673  0.0002165774  130030
    109  0.0129064180  0.0001665756  001330
    105  0.0100125854  0.0001002519  003310
     67 -0.0099385887  0.0000987755  101320
     43 -0.0094321205  0.0000889649  120130
     27 -0.0091370093  0.0000834849  132010
     21 -0.0090168236  0.0000813031  300310
     44 -0.0080183623  0.0000642941  120103
     72  0.0079125208  0.0000626080  100303
     93  0.0073786722  0.0000544448  012130
     56  0.0071897336  0.0000516923  110203
     24 -0.0071709691  0.0000514228  300013
     58 -0.0069874937  0.0000488251  103300
     88 -0.0063541291  0.0000403750  013210
    110  0.0052918561  0.0000280037  001303
    102  0.0052659851  0.0000277306  010213
     61 -0.0051193774  0.0000262080  103030
     53 -0.0050743333  0.0000257489  111220
     97 -0.0050192723  0.0000251931  011230
     50 -0.0046901858  0.0000219978  112120
    103  0.0045111164  0.0000203502  010123
     71 -0.0041534674  0.0000172513  100330
    113  0.0029020830  0.0000084221  001033
     75  0.0028440634  0.0000080887  100033
     55 -0.0028084046  0.0000078871  110230
     66 -0.0027416056  0.0000075164  102013
     83 -0.0024368134  0.0000059381  030310
     89  0.0021792156  0.0000047490  013120
     98 -0.0021608630  0.0000046693  011203
     86 -0.0021396847  0.0000045783  030013
    108 -0.0017945375  0.0000032204  003013
    114  0.0015446959  0.0000023861  000313
     30  0.0015352629  0.0000023570  130300

 input the coefficient threshold (end with 0.) [ 0.0000]:
 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 0) end.

 input menu number [  0]: