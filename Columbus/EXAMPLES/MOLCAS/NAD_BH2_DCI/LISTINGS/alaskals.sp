License is going to expire in 22 days on Saturday November 01st, 2008
Everything is fine. Going to run MOLCAS!
                                                                                                   
                                              ^^^^^            M O L C A S                         
                                             ^^^^^^^           version 7.3 patchlevel 320          
                               ^^^^^         ^^^^^^^                                               
                              ^^^^^^^        ^^^ ^^^                                               
                              ^^^^^^^       ^^^^ ^^^                                               
                              ^^^ ^^^       ^^^^ ^^^                                               
                              ^^^ ^^^^      ^^^  ^^^                                               
                              ^^^  ^^ ^^^^^  ^^  ^^^^                                              
                             ^^^^      ^^^^   ^   ^^^                                              
                             ^   ^^^   ^^^^   ^^^^  ^                                              
                            ^   ^^^^    ^^    ^^^^   ^                                             
                        ^^^^^   ^^^^^   ^^   ^^^^^   ^^^^^                                         
                     ^^^^^^^^   ^^^^^        ^^^^^    ^^^^^^^                                      
                 ^^^^^^^^^^^    ^^^^^^      ^^^^^^^   ^^^^^^^^^^^                                  
               ^^^^^^^^^^^^^   ^^^^^^^^^^^^^^^^^^^^   ^^^^^^^^^^^^^                                
               ^^^^^^^^^^^^^   ^^^^             ^^^   ^^^      ^^^^                                
               ^^^^^^^^^^^^^                          ^^^      ^^^^                                
               ^^^^^^^^^^^^^       ^^^^^^^^^^^^        ^^      ^^^^                                
               ^^^^^^^^^^^^      ^^^^^^^^^^^^^^^^      ^^      ^^^^                                
               ^^^^^^^^^^^^    ^^^^^^^^^^^^^^^^^^^^    ^^      ^^^^                                
               ^^^^^^^^^^^^    ^^^^^^^^^^^^^^^^^^^^    ^^      ^^^^    ^^^^^     ^^^    ^^^^^^     
               ^^^^^^^^^^^^   ^^^^^^^^^^^^^^^^^^^^^^   ^^      ^^^^   ^^^^^^^    ^^^   ^^^  ^^^    
               ^^^^^^^^^^^    ^^^^^^^^^^^^^^^^^^^^^^   ^       ^^^^  ^^^    ^^   ^^^   ^^    ^^    
               ^^^^^^^^^^^     ^^^^^^^^^^^^^^^^^^^^            ^^^^  ^^         ^^ ^^  ^^^^^       
               ^^^^^^^^^^      ^^^^^^^^^^^^^^^^^^^^        ^   ^^^^  ^^         ^^ ^^   ^^^^^^     
               ^^^^^^^^          ^^^^^^^^^^^^^^^^          ^   ^^^^  ^^        ^^^^^^^     ^^^^    
               ^^^^^^      ^^^     ^^^^^^^^^^^^     ^      ^   ^^^^  ^^^   ^^^ ^^^^^^^ ^^    ^^    
                         ^^^^^^                    ^^      ^   ^^^^   ^^^^^^^  ^^   ^^ ^^^  ^^^    
                      ^^^^^^^^^^^^             ^^^^^^      ^           ^^^^^  ^^     ^^ ^^^^^^     
               ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                                
                 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                                  
                     ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                                      
                        ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                                         
                            ^^^^^^^^^^^^^^^^^^^^^^^^^^                                             
                               ^^^^^^^^^^^^^^^^^^^^                                                
                                   ^^^^^^^^^^^^                                                    
                                       ^^^^^^                                                      

                           Copyright, all rights, reserved:                                        
                         Permission is hereby granted to use                                       
                but not to reproduce or distribute any part of this                                
             program. The use is restricted to research purposes only.                             
                            Lund University Sweden, 2008.                                          
                                                                                                   
 For the author list and the recommended citation consult section 1.5 of the MOLCAS user's guide.  



()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()
                                  MOLCAS executing module ALASKA with 100 MB of memory                                  
                                              at 13:32:59 Fri Oct 10 2008                                               
()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()



                     Threshold for contributions to the gradient: .100E-06


                    ********************************************
                    * Symmetry Adapted Cartesian Displacements *
                    ********************************************


           Irreducible representation : a' 
           Basis function(s) of irrep: x, y, xy, Rz                                                                    

 Basis Label        Type   Center Phase
   1   B1           x         1     1
   2   B1           y         1     1
   3   H1           x         2     1
   4   H1           y         2     1
   5   H2           x         3     1
   6   H2           y         3     1

           Irreducible representation : a" 
           Basis function(s) of irrep: z, xz, Ry, yz, Rx, I                                                            

 Basis Label        Type   Center Phase
   7   B1           z         1     1
   8   H1           z         2     1
   9   H2           z         3     1

                     No automatic utilization of translational and rotational invariance of the energy is employed.

 Skipping Nuclear Charge Contribution

 **************************************************************************************************************
 *                                                                                                            *
 *                                      The Renormalization Contribution                                      *
 *                                                                                                            *
 **************************************************************************************************************

                Irreducible representation: a' 

                B1       x                -0.2433912E-05
                B1       y                -0.2147208E+02
                H1       x                 0.1335478E+02
                H1       y                 0.1073604E+02
                H2       x                -0.1335478E+02
                H2       y                 0.1073604E+02


 **************************************************************************************************************
 *                                                                                                            *
 *                                      The Kinetic Energy Contribution                                       *
 *                                                                                                            *
 **************************************************************************************************************

                Irreducible representation: a' 

                B1       x                 0.1721527E-05
                B1       y                 0.9494627E+02
                H1       x                -0.7823618E+02
                H1       y                -0.4747313E+02
                H2       x                 0.7823618E+02
                H2       y                -0.4747314E+02


 **************************************************************************************************************
 *                                                                                                            *
 *                                    The Nuclear Attraction Contribution                                     *
 *                                                                                                            *
 **************************************************************************************************************

                Irreducible representation: a' 

                B1       x                -0.2671707E-04
                B1       y                -0.4867089E+03
                H1       x                 0.3516720E+03
                H1       y                 0.2433544E+03
                H2       x                -0.3516720E+03
                H2       y                 0.2433545E+03

 Conventional ERI gradients!
 A total of 355514. entities were prescreened and 355037. were kept.

 ********************************************************
 *                                                      *
 *              Two-electron contribution               *
 *                                                      *
 ********************************************************

                Irreducible representation: a' 

                B1       x                 0.1936189E-04
                B1       y                 0.3233800E+03
                H1       x                -0.2243413E+03
                H1       y                -0.1616900E+03
                H2       x                 0.2243413E+03
                H2       y                -0.1616900E+03


 **************************************************
 *                                                *
 *              Molecular gradients               *
 *                                                *
 **************************************************

                Irreducible representation: a' 

                B1       x                -0.3199737E-05
                B1       y                -0.4691054E+02
                H1       x                 0.3573975E+02
                H1       y                 0.2345527E+02
                H2       x                -0.3573974E+02
                H2       y                 0.2345527E+02

--- Stop Module: alaska at Fri Oct 10 13:32:59 2008 /rc=0 ---

     Happy landing!

