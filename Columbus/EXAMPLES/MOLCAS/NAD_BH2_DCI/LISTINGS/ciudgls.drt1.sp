
======= Computing sorting integral file structure =========

 nmpsy=                       18                        6
                    -----z----- -----y----- -----x----- -----w----- ---total---

                CSFs       115        1890        6280        5685       13970
      internal walks       210         210          90          70         580
valid internal walks       115         210          90          70         485
 found l2rec=                     4096 n2max=                     2730

 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg=  12953685
 minimum size of srtscr:                    65534  WP(  
                        2  records)
 maximum size of srtscr:                   196602  WP(  
                        4  records)

sorted 4-external integrals:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 32767 have been written.
 wstat,xstat=  100.00  100.00

sorted 3-external integrals:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 32767 have been written.
 wstat,xstat=  100.00  100.00
 Orig.  diagonal integrals:  1electron:        24
                             0ext.    :        56
                             2ext.    :       238
                             4ext.    :       306


 Orig. off-diag. integrals:  4ext.    :      7536
                             3ext.    :     10836
                             2ext.    :      7218
                             1ext.    :      2562
                             0ext.    :       378
                             2ext. SO :         0
                             1ext. SO :         0
                             0ext. SO :         0
                             1electron:        93


 Sorted integrals            3ext.  w :      9604 x :      8372
                             4ext.  w :      6048 x :      4620


1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      -varseg3    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 compressed index vector length=                      189
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 0,
  GSET = 0,
   DAVCOR =10,
  NCOREL = 7
  NROOT = 2
  IVMODE = 3
  NBKITR = 1
  NVBKMN = 2
  NVBKMX = 7
  RTOLBK = 1e-3,1e-3,
  NITER = 400
  NVCIMN = 4
  NVCIMX = 16
  RTOLCI = 1e-5,1e-5,
  IDEN  = 1
  CSFPRN = 10,
   MOLCAS=1
 /&end
 ------------------------------------------------------------------------
 Ame=                        0 ivmode,nbkitr=                        3 
                        1 ntype=                        0
 me=                        0 ivmode,nbkitr=                        3 
                        1 ntype=                        0
 ntotseg(*)=                        4                        4 
                        4                        4                        4 
                        4
 iexplseg(*)=                        0                        0 
                        0                        0                        0 
                        0
 resetting explseg from                         0  to                         0
 USING SEGMENTS OF EQUAL SIZE

****************  list of control variables  ****************
 lvlprt =    0      nroot  =    2      noldv  =   0      noldhv =   0
 nunitv =    2      nbkitr =    1      niter  = 400      davcor =  10
 csfprn =   10      ivmode =    3      istrt  =   0      vout   =   0
 iortls =    0      nvbkmx =    7      ibktv  =  -1      ibkthv =  -1
 nvcimx =    7      icitv  =   -1      icithv =  -1      frcsub =   0
 nvbkmn =    2      nvcimn =    4      maxseg =   4      nrfitr =  30
 ncorel =    7      nvrfmx =    7      nvrfmn =   4      iden   =   1
 itran  =    0      froot  =    0      rtmode =   0      ncouple=   1
 skipso =    0      dalton2=    0      molcas =   1      finalv =   0
 finalw =    0      cosmocalc=   0    with_tsklst=   0
 nsegwx =    1     1     1     1
 nseg0x =    1     1     1     1
 nseg1x =    1     1     1     1
 nseg2x =    1     1     1     1
 nseg3x =    1     1     1     1
 nseg4x =    1     1     1     1
 no0ex  =      0    no1ex  =      0    no2ex  =     0    no3ex  =     0
 no4ex  =      0    nodiag =      0
 cdg4ex =    1      c3ex1ex=    1      c2ex0ex=   1
 fileloc=    0     0     0     0     0     0     0

 ctol   = 0.010000    lrtshift=1.000000    smalld =0.001000


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-03    1.000E-05
    2        1.000E-03    1.000E-05
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core           13107199 DP per process

********** Integral sort section *************


 workspace allocation information: lencor=  13107199

 echo of the input for program cisrt:
 ------------------------------------------------------------------------
  &input
  maxbl3=32768
  maxbl4=32768
 /&end
 ------------------------------------------------------------------------

 ( 6) listing file:                    ciudgls             
 ( 5) input file:                      cisrtin   
 (17) cidrt file:                      cidrtfl             
 (11) transformed integrals file:      moints    
 (12) diagonal integral file:          diagint             
 (13) off-diagonal integral file:      ofdgint             
 (31) 4-external w integrals file:     fil4w               
 (32) 4-external x integrals file:     fil4x               
 (33) 3-external w integrals file:     fil3w               
 (34) 3-external x integrals file:     fil3x               
 (21) scratch da sorting file:         srtscr              
 (12) 2-e integral file [fsplit=2]:    moints2   

 input integral file header information:
 SEWARD INTEGRALS                                                                
  cidrt_title DRT#1                                                              
  title                                                                          
 mofmt: formatted orbitals label=morbl   zam216            13:32:53.866 10-Oct-08
 SIFS file created by program tran.      zam216            13:32:53.935 10-Oct-08

 input energy(*) values:
 energy( 1)=  4.196537892870E+00, ietype=   -1,    core energy of type: Nuc.Rep.

 total core energy =   4.196537892870E+00

 nsym = 2 nmot=  24

 symmetry  =    1    2
 slabel(*) = a'   a"  
 nmpsy(*)  =   18    6

 info(*) =          1      4096      3272      4096      2730

 orbital labels, i:molab(i)=
   1:tout:001   2:tout:002   3:tout:003   4:tout:004   5:tout:005   6:tout:006   7:tout:007   8:tout:008   9:tout:009  10:tout:010
  11:tout:011  12:tout:012  13:tout:013  14:tout:014  15:tout:015  16:tout:016  17:tout:017  18:tout:018  19:tout:019  20:tout:020
  21:tout:021  22:tout:022  23:tout:023  24:tout:024

 input parameters:
 prnopt=  0
 ldamin=    4095 ldamax=   32767 ldainc=      64
 maxbuf=   32767 maxbl3=   32768 maxbl4=   32768 intmxo=     766

 drt information:
  cidrt_title DRT#1                                                              
 nmotd =  24 nfctd =   0 nfvtc =   0 nmot  =  24
 nlevel =  24 niot  =   7 lowinl=  18
 orbital-to-level map(*)
   18  19  20  21  22  23   1   2   3   4   5   6   7   8   9  10  11  12  24  13
   14  15  16  17
 compressed map(*)
   18  19  20  21  22  23   1   2   3   4   5   6   7   8   9  10  11  12  24  13
   14  15  16  17
 levsym(*)
    1   1   1   1   1   1   1   1   1   1   1   1   2   2   2   2   2   1   1   1
    1   1   1   2
 repartitioning mu(*)=
   0.  0.  0.  0.  0.  0.  0.

 indxdg: diagonal integral statistics.
 total number of integrals contributing to diagonal matrix elements:       600
 number with all external indices:       306
 number with half external - half internal indices:       238
 number with all internal indices:        56

 indxof: off-diagonal integral statistics.
    4-external integrals: num=       7536 strt=          1
    3-external integrals: num=      10836 strt=       7537
    2-external integrals: num=       7218 strt=      18373
    1-external integrals: num=       2562 strt=      25591
    0-external integrals: num=        378 strt=      28153

 total number of off-diagonal integrals:       28530



 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg=  12953985
 !timer: setup required                  user+sys=     0.000 walltime=     0.000
 pro2e        1     301     601     901    1201    1229    1257    1557   45245   88933
   121700  125796  128526  139445

 pro2e:     23367 integrals read in     9 records.
 pro1e        1     301     601     901    1201    1229    1257    1557   45245   88933
   121700  125796  128526  139445
 pro1e: eref =    0.000000000000000E+00
 total size of srtscr:                        4  records of  
                    32767 WP =                  1048544 Bytes
 !timer: first half-sort required        user+sys=     0.000 walltime=     0.000
 putdg        1     301     601     901    1667   34434   56279    1557   45245   88933
   121700  125796  128526  139445

 putf:       4 buffers of length     766 written to file 12
 diagonal integral file completed.

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 32767 have been written.
 wstat,xstat=  100.00  100.00
 prep4e:     2 blocks of linear combinations of 4-external integrals processed.
 number of sorted 4-external integrals      10668
 number of original 4-external integrals     7536


 putf34: external integral file complete. nfilw=    31 nfilx=    32 nrecw=     1 nrecx=     1 lbufp= 32767

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 32767 have been written.
 wstat,xstat=  100.00  100.00
 prep3e:    13 blocks of linear combinations of 3-external integrals processed.
 number of sorted 3-external integrals      17976
 number of original 3-external integrals    10836


 putf34: external integral file complete. nfilw=    33 nfilx=    34 nrecw=     1 nrecx=     1 lbufp= 32767

 putf:      16 buffers of length     766 written to file 13
 off-diagonal files sort completed.
 !timer: second half-sort required       user+sys=     0.010 walltime=     0.000
 !timer: cisrt complete                  user+sys=     0.010 walltime=     0.000
 deleting integral sort shared memory
 executing brd_struct for cisrtinfo
cisrtinfo:
bufszi   766
 diagfile 4ext:     306 2ext:     238 0ext:      56
 fil4w,fil4x  :    7536 fil3w,fil3x :   10836
 ofdgint  2ext:    7218 1ext:    2562 0ext:     378so0ext:       0so1ext:       0so2ext:       0
buffer minbl4     276 minbl3     276 maxbl2     279nbas:  12   5   0   0   0   0   0   0 maxbuf 32767
 CIUDG version 5.9.7 ( 5-Oct-2004)

 workspace allocation information: lcore=  13107199

 core energy values from the integral file:
 energy( 1)=  4.196537892870E+00, ietype=   -1,    core energy of type: Nuc.Rep.

 total core repulsion energy =  4.196537892870E+00
 nmot  =    24 niot  =     7 nfct  =     0 nfvt  =     0
 nrow  =    47 nsym  =     2 ssym  =     1 lenbuf=  1600
 nwalk,xbar:        580      210      210       90       70
 nvalwt,nvalw:      485      115      210       90       70
 ncsft:           13970
 total number of valid internal walks:     485
 nvalz,nvaly,nvalx,nvalw =      115     210      90      70

 cisrt info file parameters:
 file number  12 blocksize    766
 mxbld    766
 nd4ext,nd2ext,nd0ext   306   238    56
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int     7536    10836     7218     2562      378        0        0        0
 minbl4,minbl3,maxbl2   276   276   279
 maxbuf 32767
 number of external orbitals per symmetry block:  12   5
 nmsym   2 number of internal orbitals   7
 executing brd_struct for drt
 executing brd_struct for orbinf
 executing brd_struct for momap
 calcthrxt: niot,maxw1=                        7                      216
 block size     0
 pthz,pthy,pthx,pthw:   210   210    90    70 total internal walks:     580
 maxlp3,n2lp,n1lp,n0lp   216     0     0     0
 orbsym(*)= 1 1 1 1 1 1 2

 setref:      115 references kept,
               95 references were marked as invalid, out of
              210 total.
 limcnvrt: found                      115  valid internal walksout of  
                      210  walks (skipping trailing invalids)
  ... adding                       115  segmentation marks segtype= 
                        1
 limcnvrt: found                      210  valid internal walksout of  
                      210  walks (skipping trailing invalids)
  ... adding                       210  segmentation marks segtype= 
                        2
 limcnvrt: found                       90  valid internal walksout of  
                       90  walks (skipping trailing invalids)
  ... adding                        90  segmentation marks segtype= 
                        3
 limcnvrt: found                       70  valid internal walksout of  
                       70  walks (skipping trailing invalids)
  ... adding                        70  segmentation marks segtype= 
                        4

 norbsm(*)=  18   6   0   0   0   0   0   0
 noxt(*)=  12   5   0   0   0   0   0   0
 intorb=   7 nmsym=   2 lowdoc=   7
 ivout(*)=   8   9  10  11  12  13  14  15  16  17  18  20  21  22  23  24   1   2   3   4
   5   6  19   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0
 initvdsk=    0.000000000000000       seconds.
 ---------memory usage in DP -----------------
 < n-ex core usage >
     routines:
    fourex            66537
    threx             34064
    twoex              2509
    onex               1103
    allin               766
    diagon             1318
               =======
   maximum            66537

  __ static summary __ 
   reflst               115
   hrfspc               115
               -------
   static->             115

  __ core required  __ 
   totstc               115
   max n-ex           66537
               -------
   totnec->           66652

  __ core available __ 
   totspc          13107199
   totnec -           66652
               -------
   totvec->        13040547

 number of external paths / symmetry
 vertex x      76      60
 vertex w      93      60
segment: free space=    13040547
 reducing frespc by                     1693 to                  13038854 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1         115|       115|         0|       115|         0|         1|
 -------------------------------------------------------------------------------
  Y 2         210|      1890|       115|       210|       115|         2|
 -------------------------------------------------------------------------------
  X 3          90|      6280|      2005|        90|       325|         3|
 -------------------------------------------------------------------------------
  W 4          70|      5685|      8285|        70|       415|         4|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=           4DP  conft+indsym=         840DP  drtbuffer=         849 DP

dimension of the ci-matrix ->>>     13970

 executing brd_struct for civct
 gentasklist: ntask=                       17
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS   
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      90     115       6280        115      90     115
     2  4   1    25      two-ext wz   2X  4 1      70     115       5685        115      70     115
     3  4   3    26      two-ext wx*  WX  4 3      70      90       5685       6280      70      90
     4  4   3    27      two-ext wx+  WX  4 3      70      90       5685       6280      70      90
     5  2   1    11      one-ext yz   1X  2 1     210     115       1890        115     210     115
     6  3   2    15      1ex3ex yx    3X  3 2      90     210       6280       1890      90     210
     7  4   2    16      1ex3ex yw    3X  4 2      70     210       5685       1890      70     210
     8  1   1     1      allint zz    OX  1 1     115     115        115        115     115     115
     9  2   2     5      0ex2ex yy    OX  2 2     210     210       1890       1890     210     210
    10  3   3     6      0ex2ex xx*   OX  3 3      90      90       6280       6280      90      90
    11  3   3    18      0ex2ex xx+   OX  3 3      90      90       6280       6280      90      90
    12  4   4     7      0ex2ex ww*   OX  4 4      70      70       5685       5685      70      70
    13  4   4    19      0ex2ex ww+   OX  4 4      70      70       5685       5685      70      70
    14  1   1    75      dg-024ext z  4X  1 1     115     115        115        115     115     115
    15  2   2    45      4exdg024 y   4X  2 2     210     210       1890       1890     210     210
    16  3   3    46      4exdg024 x   4X  3 3      90      90       6280       6280      90      90
    17  4   4    47      4exdg024 w   4X  4 4      70      70       5685       5685      70      70
----------------------------------------------------------------------------------------------------
 DIAGEL COUNTS: DG0X,DG2X,DG4X=                     5280 
                     1480                      370
 diagonal elements written to file   4(cihdiag             )

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      2 vectors will be written to unit 11 beginning with logical record   1

            2 vectors will be created
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        1505 2x:           0 4x:           0
All internal counts: zz :        5629 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:           0    task #     2:           0    task #     3:           0    task #     4:           0
task #     5:           0    task #     6:           0    task #     7:           0    task #     8:        4962
task #     9:           0    task #    10:           0    task #    11:           0    task #    12:           0
task #    13:           0    task #    14:         865    task #    15:           0    task #    16:           0
task #    17:           0    task #
 reference space has dimension     115

    root           eigenvalues
    ----           ------------
       1         -25.7042693745
       2         -25.6559723480

 strefv generated    2 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt=                      115

         vector  1 from unit 11 written to unit 49 filename cirefv              
 ufvoutnew: ... writing  recamt=                      115

         vector  2 from unit 11 written to unit 49 filename cirefv              

 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=   115)...
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:             13970
 number of initial trial vectors:                         2
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               7
 number of roots to converge:                             2
 number of iterations:                                    1
 residual norm convergence criteria:               0.001000  0.001000

          starting bk iteration   1

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        5280 2x:        1480 4x:         370
All internal counts: zz :        5629 yy:           0 xx:           0 ww:           0
One-external counts: yz :        9682 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:         818 wz:         810 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:           0    task #     4:           0
task #     5:        8526    task #     6:           0    task #     7:           0    task #     8:        4962
task #     9:           0    task #    10:           0    task #    11:           0    task #    12:           0
task #    13:           0    task #    14:         865    task #    15:        1326    task #    16:         450
task #    17:         332    task #
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        5280 2x:        1480 4x:         370
All internal counts: zz :        5629 yy:           0 xx:           0 ww:           0
One-external counts: yz :        9682 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:         818 wz:         810 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:           0    task #     4:           0
task #     5:        8526    task #     6:           0    task #     7:           0    task #     8:        4962
task #     9:           0    task #    10:           0    task #    11:           0    task #    12:           0
task #    13:           0    task #    14:         865    task #    15:        1326    task #    16:         450
task #    17:         332    task #

          calcsovref: tciref block   1

              civs   1       civs   2
 refs   1    1.00000       1.338424E-16
 refs   2    0.00000        1.00000    

          calcsovref: scrb block   1

              civs   1       civs   2
 civs   1    1.00000      -1.338424E-16
 civs   2    0.00000        1.00000    
 eci,repnuc=   -29.85251024087975         4.196537892870333     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -25.7042693745 -3.9080E-14  5.4110E-02  3.3283E-01  1.0000E-03
 mr-sdci #  1  2    -25.6559723480 -7.1054E-15  0.0000E+00  3.3458E-01  1.0000E-03

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0014
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0013
    3   26    0     0.00     0.00     0.00     0.00         0.    0.0000
    4   27    0     0.00     0.00     0.00     0.00         0.    0.0000
    5   11    0     0.01     0.01     0.00     0.01         0.    0.0085
    6   15    0     0.00     0.00     0.00     0.00         0.    0.0000
    7   16    0     0.00     0.00     0.00     0.00         0.    0.0000
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0050
    9    5    0     0.00     0.00     0.00     0.00         0.    0.0000
   10    6    0     0.00     0.00     0.00     0.00         0.    0.0000
   11   18    0     0.00     0.00     0.00     0.00         0.    0.0000
   12    7    0     0.00     0.00     0.00     0.00         0.    0.0000
   13   19    0     0.00     0.00     0.00     0.00         0.    0.0000
   14   75    0     0.00     0.00     0.00     0.00         0.    0.0009
   15   45    0     0.00     0.00     0.00     0.00         0.    0.0013
   16   46    0     0.01     0.00     0.00     0.01         0.    0.0005
   17   47    0     0.00     0.00     0.00     0.00         0.    0.0003
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0200s 
time spent in multnx:                   0.0200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.0600s 

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -25.7042693745 -3.9080E-14  5.4110E-02  3.3283E-01  1.0000E-03
 mr-sdci #  1  2    -25.6559723480 -7.1054E-15  0.0000E+00  3.3458E-01  1.0000E-03

 expansion vectors are not transformed.
 matrix-vector products are not transformed.

    3 expansion eigenvectors written to unit nvfile (= 11)
    2 matrix-vector products written to unit nhvfil (= 10)

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:             13970
 number of initial trial vectors:                         3
 number of initial matrix-vector products:                2
 maximum dimension of the subspace vectors:               7
 number of roots to converge:                             2
 number of iterations:                                  400
 residual norm convergence criteria:               0.000010  0.000010

          starting ci iteration   1

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        5280 2x:        1480 4x:         370
All internal counts: zz :        5629 yy:       10577 xx:        5740 ww:        3548
One-external counts: yz :        9682 yx:       10444 yw:        8199
Two-external counts: yy :        4346 ww:        1530 xx:        2750 xz:         818 wz:         810 wx:        2990
Three-ext.   counts: yx :        1415 yw:        1165

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:        2479    task #     4:        1239
task #     5:        8526    task #     6:        9200    task #     7:        7175    task #     8:        4962
task #     9:       11867    task #    10:        2061    task #    11:        2061    task #    12:        1071
task #    13:        1071    task #    14:         865    task #    15:        1326    task #    16:         450
task #    17:         332    task #

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3
 refs   1    1.00000       1.338424E-16   1.531111E-13
 refs   2    0.00000        1.00000      -2.545873E-15

          calcsovref: scrb block   1

              civs   1       civs   2       civs   3
 civs   1  -0.985981       1.019793E-09  -0.166856    
 civs   2  -9.786336E-10   -1.00000      -3.288983E-10
 civs   3  -0.943235      -9.101897E-10    5.57373    
 eci,repnuc=   -29.85251024087976         4.196537892870333     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -25.7560331435  5.1764E-02  2.6026E-03  7.2622E-02  1.0000E-05
 mr-sdci #  1  2    -25.6559723480  3.5527E-15  0.0000E+00  3.3458E-01  1.0000E-05
 mr-sdci #  1  3    -23.8967718643  2.8093E+01  0.0000E+00  7.9335E-01  1.0000E-04

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0014
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0013
    3   26    0     0.01     0.01     0.00     0.01         0.    0.0025
    4   27    0     0.01     0.00     0.00     0.01         0.    0.0012
    5   11    0     0.01     0.00     0.00     0.00         0.    0.0085
    6   15    0     0.02     0.01     0.00     0.02         0.    0.0092
    7   16    0     0.02     0.00     0.00     0.02         0.    0.0072
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0050
    9    5    0     0.01     0.00     0.00     0.01         0.    0.0119
   10    6    0     0.01     0.01     0.00     0.01         0.    0.0021
   11   18    0     0.01     0.01     0.00     0.01         0.    0.0021
   12    7    0     0.01     0.00     0.00     0.01         0.    0.0011
   13   19    0     0.00     0.00     0.00     0.00         0.    0.0011
   14   75    0     0.00     0.00     0.00     0.00         0.    0.0009
   15   45    0     0.00     0.00     0.00     0.00         0.    0.0013
   16   46    0     0.01     0.00     0.00     0.01         0.    0.0005
   17   47    0     0.00     0.00     0.00     0.00         0.    0.0003
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0400s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

          starting ci iteration   2

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        5280 2x:        1480 4x:         370
All internal counts: zz :        5629 yy:       10577 xx:        5740 ww:        3548
One-external counts: yz :        9682 yx:       10444 yw:        8199
Two-external counts: yy :        4346 ww:        1530 xx:        2750 xz:         818 wz:         810 wx:        2990
Three-ext.   counts: yx :        1415 yw:        1165

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:        2479    task #     4:        1239
task #     5:        8526    task #     6:        9200    task #     7:        7175    task #     8:        4962
task #     9:       11867    task #    10:        2061    task #    11:        2061    task #    12:        1071
task #    13:        1071    task #    14:         865    task #    15:        1326    task #    16:         450
task #    17:         332    task #

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4
 refs   1    1.00000       1.338424E-16   1.531111E-13  -2.591906E-02
 refs   2    0.00000        1.00000      -2.545873E-15  -1.801123E-09

          calcsovref: scrb block   1

              civs   1       civs   2       civs   3       civs   4
 civs   1   0.963462      -3.993506E-10   0.560031       0.405580    
 civs   2  -7.960496E-10    1.00000       2.825567E-08   3.348886E-08
 civs   3   0.980572       6.375794E-10   -4.57194        3.21427    
 civs   4  -0.822424       1.184009E-08    15.7345        18.8859    
 eci,repnuc=   -29.85251024087976         4.196537892870333     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1    -25.7581747841  2.1416E-03  2.2698E-04  2.0055E-02  1.0000E-05
 mr-sdci #  2  2    -25.6559723480  0.0000E+00  0.0000E+00  3.3458E-01  1.0000E-05
 mr-sdci #  2  3    -24.0756208115  1.7885E-01  0.0000E+00  8.7525E-01  1.0000E-04
 mr-sdci #  2  4    -23.6391737009  2.7836E+01  0.0000E+00  1.0219E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0014
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0013
    3   26    0     0.01     0.00     0.00     0.01         0.    0.0025
    4   27    0     0.00     0.00     0.00     0.00         0.    0.0012
    5   11    0     0.01     0.00     0.00     0.01         0.    0.0085
    6   15    0     0.02     0.01     0.00     0.02         0.    0.0092
    7   16    0     0.01     0.00     0.00     0.01         0.    0.0072
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0050
    9    5    0     0.02     0.01     0.01     0.01         0.    0.0119
   10    6    0     0.01     0.00     0.00     0.01         0.    0.0021
   11   18    0     0.01     0.01     0.00     0.01         0.    0.0021
   12    7    0     0.01     0.00     0.00     0.01         0.    0.0011
   13   19    0     0.00     0.00     0.00     0.00         0.    0.0011
   14   75    0     0.00     0.00     0.00     0.00         0.    0.0009
   15   45    0     0.00     0.00     0.00     0.00         0.    0.0013
   16   46    0     0.01     0.00     0.00     0.01         0.    0.0005
   17   47    0     0.00     0.00     0.00     0.00         0.    0.0003
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1100s 
time spent in multnx:                   0.1000s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.0300s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1100s 

          starting ci iteration   3

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        5280 2x:        1480 4x:         370
All internal counts: zz :        5629 yy:       10577 xx:        5740 ww:        3548
One-external counts: yz :        9682 yx:       10444 yw:        8199
Two-external counts: yy :        4346 ww:        1530 xx:        2750 xz:         818 wz:         810 wx:        2990
Three-ext.   counts: yx :        1415 yw:        1165

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:        2479    task #     4:        1239
task #     5:        8526    task #     6:        9200    task #     7:        7175    task #     8:        4962
task #     9:       11867    task #    10:        2061    task #    11:        2061    task #    12:        1071
task #    13:        1071    task #    14:         865    task #    15:        1326    task #    16:         450
task #    17:         332    task #

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1    1.00000       1.338424E-16   1.531111E-13  -2.591906E-02  -6.370183E-03
 refs   2    0.00000        1.00000      -2.545873E-15  -1.801123E-09  -1.251770E-09

          calcsovref: scrb block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 civs   1   0.966632      -5.725494E-10   8.728931E-02   0.512481       0.607457    
 civs   2  -1.031246E-10    1.00000      -4.868501E-08   5.692848E-08   6.618015E-08
 civs   3   0.983770       4.112712E-10   -1.98067       -4.76763        2.14257    
 civs   4  -0.894320       2.100442E-08    14.5844        4.90185        19.3283    
 civs   5   0.827834      -1.038478E-07   -58.7604        37.8800        25.2043    
 eci,repnuc=   -29.85251024087975         4.196537892870333     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1    -25.7583626944  1.8791E-04  2.9481E-05  7.5543E-03  1.0000E-05
 mr-sdci #  3  2    -25.6559723480 -7.1054E-15  0.0000E+00  3.3458E-01  1.0000E-05
 mr-sdci #  3  3    -24.4193156786  3.4369E-01  0.0000E+00  9.5114E-01  1.0000E-04
 mr-sdci #  3  4    -23.9122156527  2.7304E-01  0.0000E+00  8.7281E-01  1.0000E-04
 mr-sdci #  3  5    -23.5735574477  2.7770E+01  0.0000E+00  1.1092E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.01     0.00     0.00     0.01         0.    0.0014
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0013
    3   26    0     0.01     0.00     0.00     0.01         0.    0.0025
    4   27    0     0.00     0.00     0.00     0.00         0.    0.0012
    5   11    0     0.01     0.01     0.00     0.01         0.    0.0085
    6   15    0     0.02     0.00     0.00     0.02         0.    0.0092
    7   16    0     0.01     0.01     0.00     0.01         0.    0.0072
    8    1    0     0.01     0.01     0.00     0.01         0.    0.0050
    9    5    0     0.01     0.01     0.00     0.01         0.    0.0119
   10    6    0     0.02     0.00     0.01     0.01         0.    0.0021
   11   18    0     0.01     0.00     0.00     0.01         0.    0.0021
   12    7    0     0.00     0.00     0.00     0.00         0.    0.0011
   13   19    0     0.01     0.00     0.00     0.01         0.    0.0011
   14   75    0     0.00     0.00     0.00     0.00         0.    0.0009
   15   45    0     0.00     0.00     0.00     0.00         0.    0.0013
   16   46    0     0.01     0.00     0.00     0.01         0.    0.0005
   17   47    0     0.00     0.00     0.00     0.00         0.    0.0003
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1300s 
time spent in multnx:                   0.1200s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.0400s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

          starting ci iteration   4

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        5280 2x:        1480 4x:         370
All internal counts: zz :        5629 yy:       10577 xx:        5740 ww:        3548
One-external counts: yz :        9682 yx:       10444 yw:        8199
Two-external counts: yy :        4346 ww:        1530 xx:        2750 xz:         818 wz:         810 wx:        2990
Three-ext.   counts: yx :        1415 yw:        1165

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:        2479    task #     4:        1239
task #     5:        8526    task #     6:        9200    task #     7:        7175    task #     8:        4962
task #     9:       11867    task #    10:        2061    task #    11:        2061    task #    12:        1071
task #    13:        1071    task #    14:         865    task #    15:        1326    task #    16:         450
task #    17:         332    task #

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1    1.00000       1.338424E-16   1.531111E-13  -2.591906E-02  -6.370183E-03   2.427591E-03
 refs   2    0.00000        1.00000      -2.545873E-15  -1.801123E-09  -1.251770E-09   4.535120E-10

          calcsovref: scrb block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 civs   1  -0.964788       1.928549E-09  -0.359812      -0.650471      -0.300190      -0.315673    
 civs   2   6.662614E-10    1.00000      -8.308078E-09  -9.793165E-08  -7.317337E-08   5.450392E-09
 civs   3  -0.984178      -7.215908E-11    1.34541        3.10226       -4.48995      -9.543791E-02
 civs   4   0.904536       3.557936E-08   -11.9851       -2.90147       -1.95844       -22.0548    
 civs   5  -0.944944      -2.489480E-07    49.8927       -52.2395       -16.8272       -13.7145    
 civs   6  -0.901992      -1.064462E-06    120.261        60.6186        111.065       -140.752    
 eci,repnuc=   -29.85251024087976         4.196537892870333     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1    -25.7583892866  2.6592E-05  3.8323E-06  2.6993E-03  1.0000E-05
 mr-sdci #  4  2    -25.6559723480  7.1054E-15  0.0000E+00  3.3458E-01  1.0000E-05
 mr-sdci #  4  3    -24.7372844732  3.1797E-01  0.0000E+00  7.6538E-01  1.0000E-04
 mr-sdci #  4  4    -23.9425344776  3.0319E-02  0.0000E+00  1.0328E+00  1.0000E-04
 mr-sdci #  4  5    -23.7950547968  2.2150E-01  0.0000E+00  1.0431E+00  1.0000E-04
 mr-sdci #  4  6    -23.3198226477  2.7516E+01  0.0000E+00  1.0485E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0014
    2   25    0     0.01     0.00     0.00     0.01         0.    0.0013
    3   26    0     0.00     0.00     0.00     0.00         0.    0.0025
    4   27    0     0.01     0.00     0.00     0.01         0.    0.0012
    5   11    0     0.01     0.01     0.00     0.01         0.    0.0085
    6   15    0     0.01     0.01     0.00     0.01         0.    0.0092
    7   16    0     0.02     0.01     0.00     0.02         0.    0.0072
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0050
    9    5    0     0.01     0.01     0.00     0.01         0.    0.0119
   10    6    0     0.02     0.00     0.00     0.01         0.    0.0021
   11   18    0     0.01     0.00     0.00     0.01         0.    0.0021
   12    7    0     0.01     0.00     0.00     0.01         0.    0.0011
   13   19    0     0.00     0.00     0.00     0.00         0.    0.0011
   14   75    0     0.00     0.00     0.00     0.00         0.    0.0009
   15   45    0     0.00     0.00     0.00     0.00         0.    0.0013
   16   46    0     0.01     0.00     0.00     0.01         0.    0.0005
   17   47    0     0.00     0.00     0.00     0.00         0.    0.0003
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0400s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

          starting ci iteration   5

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        5280 2x:        1480 4x:         370
All internal counts: zz :        5629 yy:       10577 xx:        5740 ww:        3548
One-external counts: yz :        9682 yx:       10444 yw:        8199
Two-external counts: yy :        4346 ww:        1530 xx:        2750 xz:         818 wz:         810 wx:        2990
Three-ext.   counts: yx :        1415 yw:        1165

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:        2479    task #     4:        1239
task #     5:        8526    task #     6:        9200    task #     7:        7175    task #     8:        4962
task #     9:       11867    task #    10:        2061    task #    11:        2061    task #    12:        1071
task #    13:        1071    task #    14:         865    task #    15:        1326    task #    16:         450
task #    17:         332    task #

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1    1.00000       1.338424E-16   1.531111E-13  -2.591906E-02  -6.370183E-03   2.427591E-03   4.189545E-04
 refs   2    0.00000        1.00000      -2.545873E-15  -1.801123E-09  -1.251770E-09   4.535120E-10   1.791046E-10

          calcsovref: scrb block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 civs   1   0.964919       1.622015E-09   0.126851      -0.742058      -0.313139       4.917055E-02  -0.299082    
 civs   2  -6.566528E-10    1.00000      -4.996019E-08  -1.126883E-07  -7.501523E-08   1.048193E-08   1.828558E-08
 civs   3   0.984221      -3.852412E-10  -0.606666        3.04285       -4.40741       -1.56218       0.340607    
 civs   4  -0.905933       4.482171E-08    6.87112       -11.2732       -1.79760       -13.9451       -16.7107    
 civs   5   0.961177      -3.365179E-07   -37.5223       -7.91688       -18.9503        53.8193       -41.4158    
 civs   6    1.02701      -1.606155E-06   -107.664        73.9758        112.842       -72.2052       -124.145    
 civs   7  -0.961834       3.821400E-06    295.047        271.679       -7.27999        351.366       -249.052    

 trial vector basis is being transformed.  new dimension:   4
 eci,repnuc=   -29.85251024087976         4.196537892870333     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1    -25.7583929726  3.6860E-06  6.2756E-07  1.0565E-03  1.0000E-05
 mr-sdci #  5  2    -25.6559723480  7.1054E-15  0.0000E+00  3.3458E-01  1.0000E-05
 mr-sdci #  5  3    -25.0550870038  3.1780E-01  0.0000E+00  5.8428E-01  1.0000E-04
 mr-sdci #  5  4    -24.1496743615  2.0714E-01  0.0000E+00  9.7814E-01  1.0000E-04
 mr-sdci #  5  5    -23.7951786468  1.2385E-04  0.0000E+00  1.0540E+00  1.0000E-04
 mr-sdci #  5  6    -23.5886015973  2.6878E-01  0.0000E+00  1.0395E+00  1.0000E-04
 mr-sdci #  5  7    -23.2161941465  2.7413E+01  0.0000E+00  1.1423E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0014
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0013
    3   26    0     0.00     0.00     0.00     0.00         0.    0.0025
    4   27    0     0.02     0.00     0.01     0.01         0.    0.0012
    5   11    0     0.01     0.01     0.00     0.01         0.    0.0085
    6   15    0     0.02     0.00     0.00     0.02         0.    0.0092
    7   16    0     0.01     0.00     0.00     0.01         0.    0.0072
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0050
    9    5    0     0.02     0.00     0.00     0.02         0.    0.0119
   10    6    0     0.01     0.00     0.00     0.01         0.    0.0021
   11   18    0     0.00     0.00     0.00     0.00         0.    0.0021
   12    7    0     0.01     0.00     0.00     0.01         0.    0.0011
   13   19    0     0.01     0.00     0.00     0.01         0.    0.0011
   14   75    0     0.00     0.00     0.00     0.00         0.    0.0009
   15   45    0     0.00     0.00     0.00     0.00         0.    0.0013
   16   46    0     0.00     0.00     0.00     0.00         0.    0.0005
   17   47    0     0.00     0.00     0.00     0.00         0.    0.0003
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1100s 
time spent in multnx:                   0.1000s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.0100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1200s 

          starting ci iteration   6

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        5280 2x:        1480 4x:         370
All internal counts: zz :        5629 yy:       10577 xx:        5740 ww:        3548
One-external counts: yz :        9682 yx:       10444 yw:        8199
Two-external counts: yy :        4346 ww:        1530 xx:        2750 xz:         818 wz:         810 wx:        2990
Three-ext.   counts: yx :        1415 yw:        1165

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:        2479    task #     4:        1239
task #     5:        8526    task #     6:        9200    task #     7:        7175    task #     8:        4962
task #     9:       11867    task #    10:        2061    task #    11:        2061    task #    12:        1071
task #    13:        1071    task #    14:         865    task #    15:        1326    task #    16:         450
task #    17:         332    task #

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1   0.984367       3.058652E-10   5.002871E-02  -0.106030       3.681174E-04
 refs   2   6.536419E-11    1.00000      -1.134930E-08  -2.657564E-10   3.663425E-11

          calcsovref: scrb block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 civs   1   0.999692       5.495264E-09  -0.155458       0.234582      -0.421425    
 civs   2  -1.139021E-10    1.00000      -6.759954E-09   1.880057E-08  -3.449287E-08
 civs   3  -2.487029E-04   5.173664E-09  -0.939880      -0.185148       0.289559    
 civs   4  -6.061834E-05   1.033119E-09  -2.301769E-02  -0.805511      -0.608526    
 civs   5   0.851226      -1.492293E-05    428.070       -647.092        1162.61    
 eci,repnuc=   -29.85251024087976         4.196537892870333     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  6  1    -25.7583935068  5.3419E-07  1.5451E-07  5.2101E-04  1.0000E-05
 mr-sdci #  6  2    -25.6559723480 -3.5527E-15  0.0000E+00  3.3458E-01  1.0000E-05
 mr-sdci #  6  3    -25.1562719388  1.0118E-01  0.0000E+00  4.8554E-01  1.0000E-04
 mr-sdci #  6  4    -24.1845454521  3.4871E-02  0.0000E+00  1.0445E+00  1.0000E-04
 mr-sdci #  6  5    -24.0387917799  2.4361E-01  0.0000E+00  1.1101E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.01     0.00     0.01     0.00         0.    0.0014
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0013
    3   26    0     0.01     0.00     0.00     0.01         0.    0.0025
    4   27    0     0.00     0.00     0.00     0.00         0.    0.0012
    5   11    0     0.01     0.00     0.00     0.01         0.    0.0085
    6   15    0     0.02     0.01     0.01     0.02         0.    0.0092
    7   16    0     0.02     0.01     0.00     0.02         0.    0.0072
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0050
    9    5    0     0.01     0.00     0.00     0.01         0.    0.0119
   10    6    0     0.01     0.01     0.00     0.01         0.    0.0021
   11   18    0     0.01     0.00     0.00     0.01         0.    0.0021
   12    7    0     0.01     0.00     0.00     0.01         0.    0.0011
   13   19    0     0.00     0.00     0.00     0.00         0.    0.0011
   14   75    0     0.00     0.00     0.00     0.00         0.    0.0009
   15   45    0     0.00     0.00     0.00     0.00         0.    0.0013
   16   46    0     0.01     0.00     0.00     0.01         0.    0.0005
   17   47    0     0.00     0.00     0.00     0.00         0.    0.0003
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1100s 
integral transfer time:                 0.0200s 
time spent for loop construction:       0.0300s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

          starting ci iteration   7

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        5280 2x:        1480 4x:         370
All internal counts: zz :        5629 yy:       10577 xx:        5740 ww:        3548
One-external counts: yz :        9682 yx:       10444 yw:        8199
Two-external counts: yy :        4346 ww:        1530 xx:        2750 xz:         818 wz:         810 wx:        2990
Three-ext.   counts: yx :        1415 yw:        1165

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:        2479    task #     4:        1239
task #     5:        8526    task #     6:        9200    task #     7:        7175    task #     8:        4962
task #     9:       11867    task #    10:        2061    task #    11:        2061    task #    12:        1071
task #    13:        1071    task #    14:         865    task #    15:        1326    task #    16:         450
task #    17:         332    task #

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1   0.984367       3.058652E-10   5.002871E-02  -0.106030       3.681174E-04  -2.438681E-04
 refs   2   6.536419E-11    1.00000      -1.134930E-08  -2.657564E-10   3.663425E-11  -3.286870E-11

          calcsovref: scrb block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 civs   1   0.999824      -2.743590E-10   1.766945E-02  -0.183050      -0.294218       0.710021    
 civs   2  -1.471396E-10    1.00000      -2.964094E-08  -4.920148E-08  -1.966896E-08   9.504046E-08
 civs   3  -2.898814E-04   8.453357E-09   0.786019      -0.585081       0.301779       0.163880    
 civs   4  -9.074629E-05   2.700896E-09   7.653251E-02   0.525327       0.610586       0.625719    
 civs   5    1.07976      -2.644670E-05   -585.644       -649.476        953.717       -570.921    
 civs   6   0.928201      -4.307719E-05   -839.429       -1800.66        221.634        2164.46    
 eci,repnuc=   -29.85251024087975         4.196537892870333     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  7  1    -25.7583936502  1.4342E-07  2.5360E-08  2.1071E-04  1.0000E-05
 mr-sdci #  7  2    -25.6559723480 -7.1054E-15  0.0000E+00  3.3458E-01  1.0000E-05
 mr-sdci #  7  3    -25.2480888340  9.1817E-02  0.0000E+00  4.2007E-01  1.0000E-04
 mr-sdci #  7  4    -24.4884960654  3.0395E-01  0.0000E+00  8.8661E-01  1.0000E-04
 mr-sdci #  7  5    -24.1743653940  1.3557E-01  0.0000E+00  1.0989E+00  1.0000E-04
 mr-sdci #  7  6    -23.4643102230 -1.2429E-01  0.0000E+00  1.0798E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0014
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0013
    3   26    0     0.00     0.00     0.00     0.00         0.    0.0025
    4   27    0     0.02     0.00     0.01     0.01         0.    0.0012
    5   11    0     0.01     0.01     0.00     0.01         0.    0.0085
    6   15    0     0.02     0.00     0.00     0.02         0.    0.0092
    7   16    0     0.01     0.00     0.00     0.01         0.    0.0072
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0050
    9    5    0     0.02     0.00     0.00     0.02         0.    0.0119
   10    6    0     0.01     0.00     0.00     0.01         0.    0.0021
   11   18    0     0.00     0.00     0.00     0.00         0.    0.0021
   12    7    0     0.01     0.00     0.00     0.01         0.    0.0011
   13   19    0     0.01     0.00     0.00     0.01         0.    0.0011
   14   75    0     0.00     0.00     0.00     0.00         0.    0.0009
   15   45    0     0.00     0.00     0.00     0.00         0.    0.0013
   16   46    0     0.00     0.00     0.00     0.00         0.    0.0005
   17   47    0     0.00     0.00     0.00     0.00         0.    0.0003
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1100s 
time spent in multnx:                   0.1000s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.0100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1200s 

          starting ci iteration   8

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        5280 2x:        1480 4x:         370
All internal counts: zz :        5629 yy:       10577 xx:        5740 ww:        3548
One-external counts: yz :        9682 yx:       10444 yw:        8199
Two-external counts: yy :        4346 ww:        1530 xx:        2750 xz:         818 wz:         810 wx:        2990
Three-ext.   counts: yx :        1415 yw:        1165

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:        2479    task #     4:        1239
task #     5:        8526    task #     6:        9200    task #     7:        7175    task #     8:        4962
task #     9:       11867    task #    10:        2061    task #    11:        2061    task #    12:        1071
task #    13:        1071    task #    14:         865    task #    15:        1326    task #    16:         450
task #    17:         332    task #

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1   0.984367       3.058652E-10   5.002871E-02  -0.106030       3.681174E-04  -2.438681E-04   1.202061E-04
 refs   2   6.536419E-11    1.00000      -1.134930E-08  -2.657564E-10   3.663425E-11  -3.286870E-11   8.100193E-12

          calcsovref: scrb block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 civs   1   0.999746       9.917042E-09   0.169986       0.281629      -0.473232       0.755086      -0.321614    
 civs   2  -1.724097E-10    1.00000      -3.811122E-08  -1.400166E-08  -5.085876E-08   9.819273E-08  -1.702360E-08
 civs   3  -3.038006E-04   1.131462E-08   0.653959      -0.734339      -7.170287E-02   0.210381      -0.253216    
 civs   4  -9.936770E-05   4.007762E-09   8.849627E-02   0.145342       0.775590       0.644196      -3.941090E-02
 civs   5    1.11390      -3.258636E-05   -621.719       -448.176        221.601       -379.937       -1244.31    
 civs   6    1.06684      -6.337863E-05   -1069.60       -1189.09       -934.993        2259.55       -381.936    
 civs   7   0.844689      -1.083416E-04   -1649.67       -3386.96        1503.59       -789.460        5856.06    

 trial vector basis is being transformed.  new dimension:   4
 eci,repnuc=   -29.85251024087977         4.196537892870333     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  8  1    -25.7583936716  2.1421E-08  4.3006E-09  8.6648E-05  1.0000E-05
 mr-sdci #  8  2    -25.6559723480  1.4211E-14  0.0000E+00  3.3458E-01  1.0000E-05
 mr-sdci #  8  3    -25.3085691493  6.0480E-02  0.0000E+00  3.5815E-01  1.0000E-04
 mr-sdci #  8  4    -24.7947726557  3.0628E-01  0.0000E+00  6.3052E-01  1.0000E-04
 mr-sdci #  8  5    -24.3160809182  1.4172E-01  0.0000E+00  9.8533E-01  1.0000E-04
 mr-sdci #  8  6    -23.4691789854  4.8688E-03  0.0000E+00  1.0651E+00  1.0000E-04
 mr-sdci #  8  7    -23.2206820560  4.4879E-03  0.0000E+00  1.2220E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.01     0.00     0.01     0.00         0.    0.0014
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0013
    3   26    0     0.01     0.00     0.00     0.01         0.    0.0025
    4   27    0     0.00     0.00     0.00     0.00         0.    0.0012
    5   11    0     0.01     0.00     0.00     0.01         0.    0.0085
    6   15    0     0.02     0.00     0.00     0.02         0.    0.0092
    7   16    0     0.01     0.00     0.00     0.01         0.    0.0072
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0050
    9    5    0     0.02     0.01     0.00     0.02         0.    0.0119
   10    6    0     0.00     0.00     0.00     0.00         0.    0.0021
   11   18    0     0.01     0.01     0.00     0.01         0.    0.0021
   12    7    0     0.01     0.00     0.00     0.01         0.    0.0011
   13   19    0     0.01     0.00     0.00     0.01         0.    0.0011
   14   75    0     0.00     0.00     0.00     0.00         0.    0.0009
   15   45    0     0.00     0.00     0.00     0.00         0.    0.0013
   16   46    0     0.00     0.00     0.00     0.00         0.    0.0005
   17   47    0     0.01     0.00     0.00     0.01         0.    0.0003
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1100s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.0200s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

          starting ci iteration   9

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        5280 2x:        1480 4x:         370
All internal counts: zz :        5629 yy:       10577 xx:        5740 ww:        3548
One-external counts: yz :        9682 yx:       10444 yw:        8199
Two-external counts: yy :        4346 ww:        1530 xx:        2750 xz:         818 wz:         810 wx:        2990
Three-ext.   counts: yx :        1415 yw:        1165

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:        2479    task #     4:        1239
task #     5:        8526    task #     6:        9200    task #     7:        7175    task #     8:        4962
task #     9:       11867    task #    10:        2061    task #    11:        2061    task #    12:        1071
task #    13:        1071    task #    14:         865    task #    15:        1326    task #    16:         450
task #    17:         332    task #

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1   0.984363       6.460907E-10   2.433675E-02  -5.705488E-02  -3.990082E-05
 refs   2  -9.100453E-11    1.00000      -4.652811E-08  -1.045735E-08  -5.965791E-12

          calcsovref: scrb block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 civs   1    1.00004      -1.374425E-08   0.130660       0.247047       0.585031    
 civs   2  -7.595457E-12    1.00000       3.773855E-08   5.363597E-08   1.103692E-07
 civs   3  -1.614216E-05   8.764640E-09  -0.947407       0.235126       0.250238    
 civs   4  -1.733495E-05   7.462068E-09  -0.112748      -0.897632       0.426556    
 civs   5   0.942102      -3.599711E-04    3419.24        6464.11        15306.6    
 eci,repnuc=   -29.85251024087976         4.196537892870333     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  9  1    -25.7583936757  4.0516E-09  1.1811E-09  4.5509E-05  1.0000E-05
 mr-sdci #  9  2    -25.6559723480 -7.1054E-15  0.0000E+00  3.3458E-01  1.0000E-05
 mr-sdci #  9  3    -25.3472688548  3.8700E-02  0.0000E+00  3.2044E-01  1.0000E-04
 mr-sdci #  9  4    -24.9295355604  1.3476E-01  0.0000E+00  5.7992E-01  1.0000E-04
 mr-sdci #  9  5    -24.0874168505 -2.2866E-01  0.0000E+00  1.0475E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0014
    2   25    0     0.01     0.00     0.00     0.01         0.    0.0013
    3   26    0     0.01     0.00     0.00     0.01         0.    0.0025
    4   27    0     0.00     0.00     0.00     0.00         0.    0.0012
    5   11    0     0.01     0.01     0.00     0.01         0.    0.0085
    6   15    0     0.02     0.00     0.01     0.02         0.    0.0092
    7   16    0     0.01     0.00     0.00     0.01         0.    0.0072
    8    1    0     0.01     0.00     0.00     0.01         0.    0.0050
    9    5    0     0.01     0.00     0.00     0.01         0.    0.0119
   10    6    0     0.01     0.00     0.00     0.01         0.    0.0021
   11   18    0     0.00     0.00     0.00     0.00         0.    0.0021
   12    7    0     0.01     0.00     0.00     0.01         0.    0.0011
   13   19    0     0.01     0.01     0.00     0.01         0.    0.0011
   14   75    0     0.00     0.00     0.00     0.00         0.    0.0009
   15   45    0     0.00     0.00     0.00     0.00         0.    0.0013
   16   46    0     0.00     0.00     0.00     0.00         0.    0.0005
   17   47    0     0.01     0.00     0.00     0.01         0.    0.0003
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1200s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.0200s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1200s 

          starting ci iteration  10

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        5280 2x:        1480 4x:         370
All internal counts: zz :        5629 yy:       10577 xx:        5740 ww:        3548
One-external counts: yz :        9682 yx:       10444 yw:        8199
Two-external counts: yy :        4346 ww:        1530 xx:        2750 xz:         818 wz:         810 wx:        2990
Three-ext.   counts: yx :        1415 yw:        1165

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:        2479    task #     4:        1239
task #     5:        8526    task #     6:        9200    task #     7:        7175    task #     8:        4962
task #     9:       11867    task #    10:        2061    task #    11:        2061    task #    12:        1071
task #    13:        1071    task #    14:         865    task #    15:        1326    task #    16:         450
task #    17:         332    task #

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1   0.984363       6.460907E-10   2.433675E-02  -5.705488E-02  -3.990082E-05   2.767225E-05
 refs   2  -9.100453E-11    1.00000      -4.652811E-08  -1.045735E-08  -5.965791E-12   2.744614E-12

          calcsovref: scrb block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 civs   1    1.00002      -1.294891E-09   3.788214E-02   2.479545E-02   0.131487       -1.01982    
 civs   2  -1.654760E-11    1.00000       5.525081E-08   4.876444E-08  -3.566941E-08  -1.227483E-07
 civs   3  -2.040493E-05   1.537356E-08  -0.862471       0.446305      -0.303136      -2.790907E-02
 civs   4  -1.776172E-05   9.173773E-09  -0.146613      -0.695587      -0.749530       8.020855E-02
 civs   5    1.21188      -6.116757E-04    5216.23        7800.31       -10752.6       -9589.47    
 civs   6   0.982342      -8.280035E-04    6059.62        10258.7       -20360.9        24521.4    
 eci,repnuc=   -29.85251024087977         4.196537892870333     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 10  1    -25.7583936769  1.1602E-09  1.6466E-10  1.6900E-05  1.0000E-05
 mr-sdci # 10  2    -25.6559723480  1.0658E-14  0.0000E+00  3.3458E-01  1.0000E-05
 mr-sdci # 10  3    -25.3802982073  3.3029E-02  0.0000E+00  2.7494E-01  1.0000E-04
 mr-sdci # 10  4    -25.0229104661  9.3375E-02  0.0000E+00  5.1337E-01  1.0000E-04
 mr-sdci # 10  5    -24.3330711492  2.4565E-01  0.0000E+00  8.7471E-01  1.0000E-04
 mr-sdci # 10  6    -23.7585701755  2.8939E-01  0.0000E+00  1.1488E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.01     0.00     0.01     0.00         0.    0.0014
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0013
    3   26    0     0.01     0.00     0.00     0.01         0.    0.0025
    4   27    0     0.00     0.00     0.00     0.00         0.    0.0012
    5   11    0     0.01     0.01     0.00     0.01         0.    0.0085
    6   15    0     0.02     0.01     0.00     0.02         0.    0.0092
    7   16    0     0.01     0.00     0.00     0.01         0.    0.0072
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0050
    9    5    0     0.02     0.01     0.00     0.02         0.    0.0119
   10    6    0     0.01     0.00     0.00     0.01         0.    0.0021
   11   18    0     0.01     0.00     0.00     0.01         0.    0.0021
   12    7    0     0.00     0.00     0.00     0.00         0.    0.0011
   13   19    0     0.01     0.00     0.00     0.01         0.    0.0011
   14   75    0     0.01     0.00     0.01     0.00         0.    0.0009
   15   45    0     0.00     0.00     0.00     0.00         0.    0.0013
   16   46    0     0.00     0.00     0.00     0.00         0.    0.0005
   17   47    0     0.01     0.00     0.00     0.01         0.    0.0003
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1300s 
time spent in multnx:                   0.1100s 
integral transfer time:                 0.0200s 
time spent for loop construction:       0.0300s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

          starting ci iteration  11

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        5280 2x:        1480 4x:         370
All internal counts: zz :        5629 yy:       10577 xx:        5740 ww:        3548
One-external counts: yz :        9682 yx:       10444 yw:        8199
Two-external counts: yy :        4346 ww:        1530 xx:        2750 xz:         818 wz:         810 wx:        2990
Three-ext.   counts: yx :        1415 yw:        1165

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:        2479    task #     4:        1239
task #     5:        8526    task #     6:        9200    task #     7:        7175    task #     8:        4962
task #     9:       11867    task #    10:        2061    task #    11:        2061    task #    12:        1071
task #    13:        1071    task #    14:         865    task #    15:        1326    task #    16:         450
task #    17:         332    task #

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1   0.984363       6.460907E-10   2.433675E-02  -5.705488E-02  -3.990082E-05   2.767225E-05  -1.165518E-05
 refs   2  -9.100453E-11    1.00000      -4.652811E-08  -1.045735E-08  -5.965791E-12   2.744614E-12  -1.639705E-12

          calcsovref: scrb block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 civs   1    1.00003      -1.762334E-08   0.129266       0.235888       0.343319       -1.16133       2.138045E-02
 civs   2  -1.760542E-11    1.00000       8.731131E-08   9.543181E-08   8.534076E-08  -1.281292E-07  -5.102662E-09
 civs   3  -2.006041E-05   1.606979E-08  -0.794149       0.592353       0.176210       8.417174E-02  -0.133771    
 civs   4  -1.924510E-05   1.298715E-08  -0.171669      -0.521085       0.746066       0.358626      -0.335116    
 civs   5    1.24512      -7.113040E-04    5840.69        7522.49        3182.99       -1357.61       -14976.2    
 civs   6    1.10339      -1.121778E-03    7862.91        12435.4        9965.99        28902.0        3017.94    
 civs   7   0.868267      -1.849133E-03    10500.6        25414.2        44265.7       -30841.1        61258.0    

 trial vector basis is being transformed.  new dimension:   4
 eci,repnuc=   -29.85251024087978         4.196537892870333     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 11  1    -25.7583936770  1.4296E-10  0.0000E+00  7.9296E-06  1.0000E-05
 mr-sdci # 11  2    -25.6559723480  7.1054E-15  5.9626E-02  3.3458E-01  1.0000E-05
 mr-sdci # 11  3    -25.3945720232  1.4274E-02  0.0000E+00  2.7127E-01  1.0000E-04
 mr-sdci # 11  4    -25.0960852772  7.3175E-02  0.0000E+00  4.1808E-01  1.0000E-04
 mr-sdci # 11  5    -24.6872480947  3.5418E-01  0.0000E+00  6.5315E-01  1.0000E-04
 mr-sdci # 11  6    -23.8338246132  7.5254E-02  0.0000E+00  1.1441E+00  1.0000E-04
 mr-sdci # 11  7    -23.5132998486  2.9262E-01  0.0000E+00  1.1414E+00  1.0000E-04

 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0014
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0013
    3   26    0     0.01     0.00     0.00     0.01         0.    0.0025
    4   27    0     0.01     0.00     0.00     0.01         0.    0.0012
    5   11    0     0.00     0.00     0.00     0.00         0.    0.0085
    6   15    0     0.02     0.00     0.00     0.01         0.    0.0092
    7   16    0     0.01     0.00     0.00     0.01         0.    0.0072
    8    1    0     0.01     0.00     0.00     0.01         0.    0.0050
    9    5    0     0.01     0.00     0.00     0.01         0.    0.0119
   10    6    0     0.01     0.00     0.00     0.01         0.    0.0021
   11   18    0     0.01     0.00     0.00     0.01         0.    0.0021
   12    7    0     0.01     0.00     0.00     0.01         0.    0.0011
   13   19    0     0.00     0.00     0.00     0.00         0.    0.0011
   14   75    0     0.01     0.00     0.00     0.00         0.    0.0009
   15   45    0     0.00     0.00     0.00     0.00         0.    0.0013
   16   46    0     0.00     0.00     0.00     0.00         0.    0.0005
   17   47    0     0.01     0.00     0.00     0.01         0.    0.0003
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

          starting ci iteration  12

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        5280 2x:        1480 4x:         370
All internal counts: zz :        5629 yy:       10577 xx:        5740 ww:        3548
One-external counts: yz :        9682 yx:       10444 yw:        8199
Two-external counts: yy :        4346 ww:        1530 xx:        2750 xz:         818 wz:         810 wx:        2990
Three-ext.   counts: yx :        1415 yw:        1165

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:        2479    task #     4:        1239
task #     5:        8526    task #     6:        9200    task #     7:        7175    task #     8:        4962
task #     9:       11867    task #    10:        2061    task #    11:        2061    task #    12:        1071
task #    13:        1071    task #    14:         865    task #    15:        1326    task #    16:         450
task #    17:         332    task #

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1   0.984362       1.839870E-09  -2.013792E-02   2.409951E-02  -6.607017E-08
 refs   2  -1.133013E-10    1.00000       9.556341E-08   2.087931E-08  -3.488661E-13

          calcsovref: scrb block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 civs   1    1.00000      -6.053711E-08  -2.069808E-14   5.124788E-15   3.242598E-07
 civs   2  -4.720053E-12  -0.983018       7.413901E-08  -8.234803E-09  -0.183508    
 civs   3   1.863991E-16  -4.815125E-08   -1.00000       8.164921E-13  -1.460722E-07
 civs   4  -1.562849E-16  -4.217907E-09   8.195724E-13    1.00000      -2.227977E-08
 civs   5  -8.107722E-12  -0.942051      -3.250233E-07   7.732413E-08    5.04638    
 eci,repnuc=   -29.90965170919508         4.196537892870333     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 12  1    -25.7583936770 -3.5527E-15  0.0000E+00  7.9296E-06  1.0000E-05
 mr-sdci # 12  2    -25.7131138163  5.7141E-02  3.4924E-03  7.8109E-02  1.0000E-05
 mr-sdci # 12  3    -25.3945720232  1.4211E-14  0.0000E+00  2.7127E-01  1.0000E-04
 mr-sdci # 12  4    -25.0960852772  2.8422E-14  0.0000E+00  4.1808E-01  1.0000E-04
 mr-sdci # 12  5    -24.0162774862 -6.7097E-01  0.0000E+00  7.5282E-01  1.0000E-04

 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0014
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0013
    3   26    0     0.01     0.00     0.01     0.00         0.    0.0025
    4   27    0     0.01     0.00     0.00     0.01         0.    0.0012
    5   11    0     0.01     0.01     0.00     0.01         0.    0.0085
    6   15    0     0.02     0.00     0.00     0.02         0.    0.0092
    7   16    0     0.01     0.00     0.00     0.01         0.    0.0072
    8    1    0     0.01     0.01     0.00     0.01         0.    0.0050
    9    5    0     0.01     0.01     0.00     0.01         0.    0.0119
   10    6    0     0.01     0.00     0.00     0.01         0.    0.0021
   11   18    0     0.01     0.00     0.00     0.01         0.    0.0021
   12    7    0     0.01     0.00     0.00     0.01         0.    0.0011
   13   19    0     0.00     0.00     0.00     0.00         0.    0.0011
   14   75    0     0.00     0.00     0.00     0.00         0.    0.0009
   15   45    0     0.00     0.00     0.00     0.00         0.    0.0013
   16   46    0     0.00     0.00     0.00     0.00         0.    0.0005
   17   47    0     0.01     0.00     0.00     0.01         0.    0.0003
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1100s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.0300s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1200s 

          starting ci iteration  13

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        5280 2x:        1480 4x:         370
All internal counts: zz :        5629 yy:       10577 xx:        5740 ww:        3548
One-external counts: yz :        9682 yx:       10444 yw:        8199
Two-external counts: yy :        4346 ww:        1530 xx:        2750 xz:         818 wz:         810 wx:        2990
Three-ext.   counts: yx :        1415 yw:        1165

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:        2479    task #     4:        1239
task #     5:        8526    task #     6:        9200    task #     7:        7175    task #     8:        4962
task #     9:       11867    task #    10:        2061    task #    11:        2061    task #    12:        1071
task #    13:        1071    task #    14:         865    task #    15:        1326    task #    16:         450
task #    17:         332    task #

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1   0.984362       1.839870E-09  -2.013792E-02   2.409951E-02  -6.607017E-08  -1.090946E-07
 refs   2  -1.133013E-10    1.00000       9.556341E-08   2.087931E-08  -3.488661E-13  -3.851704E-02

          calcsovref: scrb block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 civs   1    1.00000       2.048451E-08   1.355996E-13   2.300862E-13   1.518594E-06   1.048420E-06
 civs   2  -3.406811E-12  -0.950517       1.518515E-07   1.149272E-07   0.727991       0.124558    
 civs   3  -1.483997E-16  -5.707278E-08   -1.00000      -1.130852E-14   2.073324E-07  -8.014905E-08
 civs   4   2.833942E-17  -8.528775E-09   4.518224E-14   -1.00000       1.908404E-07   3.578034E-08
 civs   5  -3.475106E-12  -0.985055      -4.061590E-07  -2.384213E-07   -2.39977        4.44648    
 civs   6  -3.636524E-11   0.791953       1.529854E-06   2.319442E-06    15.8121        7.20952    
 eci,repnuc=   -29.91242040360415         4.196537892870333     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 13  1    -25.7583936770  3.5527E-15  0.0000E+00  7.9296E-06  1.0000E-05
 mr-sdci # 13  2    -25.7158825107  2.7687E-03  4.3482E-04  2.6143E-02  1.0000E-05
 mr-sdci # 13  3    -25.3945720232  3.5527E-15  0.0000E+00  2.7127E-01  1.0000E-04
 mr-sdci # 13  4    -25.0960852772 -7.1054E-15  0.0000E+00  4.1808E-01  1.0000E-04
 mr-sdci # 13  5    -24.4467260404  4.3045E-01  0.0000E+00  9.8712E-01  1.0000E-04
 mr-sdci # 13  6    -23.9268486843  9.3024E-02  0.0000E+00  8.5281E-01  1.0000E-04

 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.01     0.01     0.00     0.01         0.    0.0014
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0013
    3   26    0     0.00     0.00     0.00     0.00         0.    0.0025
    4   27    0     0.01     0.00     0.00     0.01         0.    0.0012
    5   11    0     0.01     0.00     0.00     0.01         0.    0.0085
    6   15    0     0.01     0.00     0.00     0.01         0.    0.0092
    7   16    0     0.02     0.00     0.00     0.02         0.    0.0072
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0050
    9    5    0     0.01     0.01     0.00     0.01         0.    0.0119
   10    6    0     0.02     0.00     0.00     0.02         0.    0.0021
   11   18    0     0.00     0.00     0.00     0.00         0.    0.0021
   12    7    0     0.01     0.00     0.00     0.01         0.    0.0011
   13   19    0     0.01     0.00     0.01     0.00         0.    0.0011
   14   75    0     0.00     0.00     0.00     0.00         0.    0.0009
   15   45    0     0.01     0.01     0.00     0.01         0.    0.0013
   16   46    0     0.00     0.00     0.00     0.00         0.    0.0005
   17   47    0     0.00     0.00     0.00     0.00         0.    0.0003
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1100s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.0300s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

          starting ci iteration  14

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        5280 2x:        1480 4x:         370
All internal counts: zz :        5629 yy:       10577 xx:        5740 ww:        3548
One-external counts: yz :        9682 yx:       10444 yw:        8199
Two-external counts: yy :        4346 ww:        1530 xx:        2750 xz:         818 wz:         810 wx:        2990
Three-ext.   counts: yx :        1415 yw:        1165

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:        2479    task #     4:        1239
task #     5:        8526    task #     6:        9200    task #     7:        7175    task #     8:        4962
task #     9:       11867    task #    10:        2061    task #    11:        2061    task #    12:        1071
task #    13:        1071    task #    14:         865    task #    15:        1326    task #    16:         450
task #    17:         332    task #

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1   0.984362       1.839870E-09  -2.013792E-02   2.409951E-02  -6.607017E-08  -1.090946E-07   1.099985E-07
 refs   2  -1.133013E-10    1.00000       9.556341E-08   2.087931E-08  -3.488661E-13  -3.851704E-02   1.138346E-02

          calcsovref: scrb block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 civs   1    1.00000      -6.016414E-08  -3.192661E-13   8.345717E-13  -1.782612E-06   4.831576E-06   1.413420E-08
 civs   2  -6.648379E-12  -0.955813      -1.620816E-07   1.474949E-07   0.364020       0.725476       3.488493E-02
 civs   3  -1.521315E-14  -5.234314E-08    1.00000       8.129278E-14   2.280577E-07   3.609923E-08  -6.126058E-08
 civs   4   2.058023E-15  -6.561941E-09   6.319953E-14   -1.00000       4.128787E-08   2.396084E-07   8.924182E-09
 civs   5  -6.855399E-12  -0.990813       3.904241E-07  -1.669006E-07   -2.34671      -0.703678        4.41779    
 civs   6  -9.855556E-12   0.897951      -1.262124E-06   1.451501E-06    15.3208        6.31966        7.86229    
 civs   7   2.016483E-10   0.857834       2.119444E-06  -6.466609E-06    30.4974       -39.4573        10.3279    

 trial vector basis is being transformed.  new dimension:   4
 eci,repnuc=   -29.91279345932493         4.196537892870333     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 14  1    -25.7583936770  3.5527E-15  0.0000E+00  7.9296E-06  1.0000E-05
 mr-sdci # 14  2    -25.7162555665  3.7306E-04  7.4632E-05  1.1041E-02  1.0000E-05
 mr-sdci # 14  3    -25.3945720232 -1.0658E-14  0.0000E+00  2.7127E-01  1.0000E-04
 mr-sdci # 14  4    -25.0960852772  0.0000E+00  0.0000E+00  4.1808E-01  1.0000E-04
 mr-sdci # 14  5    -24.4959089283  4.9183E-02  0.0000E+00  8.9284E-01  1.0000E-04
 mr-sdci # 14  6    -24.3635333838  4.3668E-01  0.0000E+00  1.0769E+00  1.0000E-04
 mr-sdci # 14  7    -23.9063363042  3.9304E-01  0.0000E+00  8.5471E-01  1.0000E-04

 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0014
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0013
    3   26    0     0.01     0.00     0.00     0.01         0.    0.0025
    4   27    0     0.00     0.00     0.00     0.00         0.    0.0012
    5   11    0     0.01     0.01     0.00     0.01         0.    0.0085
    6   15    0     0.02     0.01     0.00     0.02         0.    0.0092
    7   16    0     0.01     0.01     0.00     0.01         0.    0.0072
    8    1    0     0.01     0.01     0.00     0.01         0.    0.0050
    9    5    0     0.01     0.00     0.00     0.01         0.    0.0119
   10    6    0     0.01     0.00     0.00     0.01         0.    0.0021
   11   18    0     0.01     0.00     0.00     0.01         0.    0.0021
   12    7    0     0.01     0.00     0.00     0.01         0.    0.0011
   13   19    0     0.00     0.00     0.00     0.00         0.    0.0011
   14   75    0     0.00     0.00     0.00     0.00         0.    0.0009
   15   45    0     0.01     0.00     0.00     0.01         0.    0.0013
   16   46    0     0.00     0.00     0.00     0.00         0.    0.0005
   17   47    0     0.00     0.00     0.00     0.00         0.    0.0003
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1100s 
time spent in multnx:                   0.1100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0400s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1200s 

          starting ci iteration  15

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        5280 2x:        1480 4x:         370
All internal counts: zz :        5629 yy:       10577 xx:        5740 ww:        3548
One-external counts: yz :        9682 yx:       10444 yw:        8199
Two-external counts: yy :        4346 ww:        1530 xx:        2750 xz:         818 wz:         810 wx:        2990
Three-ext.   counts: yx :        1415 yw:        1165

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:        2479    task #     4:        1239
task #     5:        8526    task #     6:        9200    task #     7:        7175    task #     8:        4962
task #     9:       11867    task #    10:        2061    task #    11:        2061    task #    12:        1071
task #    13:        1071    task #    14:         865    task #    15:        1326    task #    16:         450
task #    17:         332    task #

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1   0.984362       1.776150E-09  -2.013792E-02  -2.409951E-02  -1.140256E-07
 refs   2  -1.172746E-10  -0.980635       6.221691E-09  -2.904280E-09  -2.272326E-03

          calcsovref: scrb block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 civs   1    1.00000      -9.912516E-08  -2.646307E-12   6.947111E-12   1.251351E-05
 civs   2   8.771304E-13  -0.997757       6.461748E-08  -1.625921E-07  -0.287172    
 civs   3  -3.866395E-16   5.834045E-09    1.00000       1.598788E-13  -3.534536E-08
 civs   4  -4.131461E-17  -1.678717E-09  -1.398264E-13    1.00000      -7.426956E-07
 civs   5  -1.259971E-09  -0.896545      -2.393878E-05   6.283467E-05    113.182    
 eci,repnuc=   -29.91286037221197         4.196537892870333     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 15  1    -25.7583936770  7.1054E-15  0.0000E+00  7.9296E-06  1.0000E-05
 mr-sdci # 15  2    -25.7163224793  6.6913E-05  2.5044E-05  5.5673E-03  1.0000E-05
 mr-sdci # 15  3    -25.3945720232  5.3291E-14  0.0000E+00  2.7127E-01  1.0000E-04
 mr-sdci # 15  4    -25.0960852772  1.2790E-13  0.0000E+00  4.1808E-01  1.0000E-04
 mr-sdci # 15  5    -24.6498563787  1.5395E-01  0.0000E+00  1.1550E+00  1.0000E-04

 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.01     0.00     0.00     0.01         0.    0.0014
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0013
    3   26    0     0.00     0.00     0.00     0.00         0.    0.0025
    4   27    0     0.01     0.00     0.00     0.01         0.    0.0012
    5   11    0     0.01     0.01     0.00     0.01         0.    0.0085
    6   15    0     0.02     0.01     0.00     0.02         0.    0.0092
    7   16    0     0.01     0.00     0.00     0.01         0.    0.0072
    8    1    0     0.01     0.01     0.00     0.01         0.    0.0050
    9    5    0     0.01     0.01     0.00     0.01         0.    0.0119
   10    6    0     0.02     0.00     0.01     0.01         0.    0.0021
   11   18    0     0.00     0.00     0.00     0.00         0.    0.0021
   12    7    0     0.01     0.01     0.00     0.01         0.    0.0011
   13   19    0     0.01     0.00     0.00     0.01         0.    0.0011
   14   75    0     0.00     0.00     0.00     0.00         0.    0.0009
   15   45    0     0.00     0.00     0.00     0.00         0.    0.0013
   16   46    0     0.00     0.00     0.00     0.00         0.    0.0005
   17   47    0     0.01     0.00     0.00     0.01         0.    0.0003
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1300s 
time spent in multnx:                   0.1200s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.0500s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

          starting ci iteration  16

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        5280 2x:        1480 4x:         370
All internal counts: zz :        5629 yy:       10577 xx:        5740 ww:        3548
One-external counts: yz :        9682 yx:       10444 yw:        8199
Two-external counts: yy :        4346 ww:        1530 xx:        2750 xz:         818 wz:         810 wx:        2990
Three-ext.   counts: yx :        1415 yw:        1165

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:        2479    task #     4:        1239
task #     5:        8526    task #     6:        9200    task #     7:        7175    task #     8:        4962
task #     9:       11867    task #    10:        2061    task #    11:        2061    task #    12:        1071
task #    13:        1071    task #    14:         865    task #    15:        1326    task #    16:         450
task #    17:         332    task #

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1   0.984362       1.776150E-09  -2.013792E-02  -2.409951E-02  -1.140256E-07  -1.602247E-07
 refs   2  -1.172746E-10  -0.980635       6.221691E-09  -2.904280E-09  -2.272326E-03  -3.920715E-03

          calcsovref: scrb block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 civs   1    1.00000      -2.879831E-08   1.335017E-11   1.181291E-05  -3.162932E-11   2.711504E-05
 civs   2   1.728418E-11    1.00087      -3.207318E-07  -0.289753       7.906151E-07  -0.641966    
 civs   3  -8.243634E-15   1.443837E-09   -1.00000       2.006914E-07  -1.527186E-13  -3.023581E-07
 civs   4  -1.818727E-15  -6.022784E-10   6.978793E-13   3.971013E-06    1.00000      -9.662030E-07
 civs   5   2.652347E-10    1.25953      -2.099177E-05   -86.1114       3.979391E-04    74.9557    
 civs   6  -4.672418E-09   -1.08192       1.009432E-04    137.345      -4.868904E-04    121.214    
 eci,repnuc=   -29.91288746839146         4.196537892870333     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 16  1    -25.7583936770  0.0000E+00  0.0000E+00  7.9296E-06  1.0000E-05
 mr-sdci # 16  2    -25.7163495755  2.7096E-05  8.2335E-06  3.4782E-03  1.0000E-05
 mr-sdci # 16  3    -25.3945720232  1.0303E-13  0.0000E+00  2.7127E-01  1.0000E-04
 mr-sdci # 16  4    -25.1462321067  5.0147E-02  0.0000E+00  6.3476E-01  1.0000E-04
 mr-sdci # 16  5    -25.0960852772  4.4623E-01  0.0000E+00  4.1808E-01  1.0000E-04
 mr-sdci # 16  6    -24.2632422189 -1.0029E-01  0.0000E+00  1.2064E+00  1.0000E-04

 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.01     0.01     0.00     0.01         0.    0.0014
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0013
    3   26    0     0.00     0.00     0.00     0.00         0.    0.0025
    4   27    0     0.01     0.00     0.00     0.01         0.    0.0012
    5   11    0     0.01     0.00     0.00     0.01         0.    0.0085
    6   15    0     0.01     0.00     0.00     0.01         0.    0.0092
    7   16    0     0.02     0.00     0.00     0.02         0.    0.0072
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0050
    9    5    0     0.01     0.01     0.00     0.01         0.    0.0119
   10    6    0     0.02     0.00     0.00     0.02         0.    0.0021
   11   18    0     0.00     0.00     0.00     0.00         0.    0.0021
   12    7    0     0.01     0.00     0.00     0.01         0.    0.0011
   13   19    0     0.00     0.00     0.00     0.00         0.    0.0011
   14   75    0     0.00     0.00     0.00     0.00         0.    0.0009
   15   45    0     0.01     0.01     0.00     0.01         0.    0.0013
   16   46    0     0.00     0.00     0.00     0.00         0.    0.0005
   17   47    0     0.00     0.00     0.00     0.00         0.    0.0003
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1100s 
time spent in multnx:                   0.1100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0300s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1200s 

          starting ci iteration  17

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        5280 2x:        1480 4x:         370
All internal counts: zz :        5629 yy:       10577 xx:        5740 ww:        3548
One-external counts: yz :        9682 yx:       10444 yw:        8199
Two-external counts: yy :        4346 ww:        1530 xx:        2750 xz:         818 wz:         810 wx:        2990
Three-ext.   counts: yx :        1415 yw:        1165

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:        2479    task #     4:        1239
task #     5:        8526    task #     6:        9200    task #     7:        7175    task #     8:        4962
task #     9:       11867    task #    10:        2061    task #    11:        2061    task #    12:        1071
task #    13:        1071    task #    14:         865    task #    15:        1326    task #    16:         450
task #    17:         332    task #

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1   0.984362       1.776150E-09  -2.013792E-02  -2.409951E-02  -1.140256E-07  -1.602247E-07  -1.930461E-07
 refs   2  -1.172746E-10  -0.980635       6.221691E-09  -2.904280E-09  -2.272326E-03  -3.920715E-03  -2.532336E-03

          calcsovref: scrb block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 civs   1    1.00000       9.163079E-08  -6.556629E-11   1.126038E-05  -7.849690E-11   4.876425E-05   4.110085E-05
 civs   2   3.857083E-11   0.999649       6.702438E-07  -1.511498E-02   1.431407E-06  -0.882031      -0.341710    
 civs   3  -7.696306E-14  -6.140082E-09    1.00000       1.924631E-06  -1.306860E-12  -4.663657E-07  -6.401775E-07
 civs   4   1.473377E-14   2.117686E-09  -3.906343E-12   1.093381E-06   -1.00000      -2.016701E-06  -1.027931E-06
 civs   5  -1.267632E-09    1.34911      -1.445871E-04    72.6529       1.073312E-04    27.3199       -97.3973    
 civs   6   2.896743E-10   -1.34891       2.098261E-04   -126.944      -3.121221E-04    127.892       -49.7087    
 civs   7  -1.600877E-08   0.812198      -4.395129E-04    122.614      -2.238834E-04    138.286        318.431    

 trial vector basis is being transformed.  new dimension:   4
 eci,repnuc=   -29.91289415565703         4.196537892870333     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 17  1    -25.7583936770 -1.0658E-14  0.0000E+00  7.9296E-06  1.0000E-05
 mr-sdci # 17  2    -25.7163562628  6.6873E-06  8.1351E-07  1.2511E-03  1.0000E-05
 mr-sdci # 17  3    -25.3945720232  7.3541E-13  0.0000E+00  2.7127E-01  1.0000E-04
 mr-sdci # 17  4    -25.3149089466  1.6868E-01  0.0000E+00  3.4414E-01  1.0000E-04
 mr-sdci # 17  5    -25.0960852772  2.0570E-12  0.0000E+00  4.1808E-01  1.0000E-04
 mr-sdci # 17  6    -24.4162893413  1.5305E-01  0.0000E+00  1.0454E+00  1.0000E-04
 mr-sdci # 17  7    -23.5350404285 -3.7130E-01  0.0000E+00  1.0973E+00  1.0000E-04

 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0014
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0013
    3   26    0     0.02     0.00     0.01     0.01         0.    0.0025
    4   27    0     0.00     0.00     0.00     0.00         0.    0.0012
    5   11    0     0.01     0.01     0.00     0.01         0.    0.0085
    6   15    0     0.02     0.00     0.00     0.02         0.    0.0092
    7   16    0     0.01     0.00     0.00     0.01         0.    0.0072
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0050
    9    5    0     0.02     0.01     0.00     0.02         0.    0.0119
   10    6    0     0.01     0.00     0.01     0.00         0.    0.0021
   11   18    0     0.01     0.01     0.00     0.01         0.    0.0021
   12    7    0     0.01     0.01     0.00     0.01         0.    0.0011
   13   19    0     0.00     0.00     0.00     0.00         0.    0.0011
   14   75    0     0.01     0.01     0.00     0.01         0.    0.0009
   15   45    0     0.00     0.00     0.00     0.00         0.    0.0013
   16   46    0     0.00     0.00     0.00     0.00         0.    0.0005
   17   47    0     0.01     0.00     0.00     0.01         0.    0.0003
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1300s 
time spent in multnx:                   0.1100s 
integral transfer time:                 0.0200s 
time spent for loop construction:       0.0500s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

          starting ci iteration  18

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        5280 2x:        1480 4x:         370
All internal counts: zz :        5629 yy:       10577 xx:        5740 ww:        3548
One-external counts: yz :        9682 yx:       10444 yw:        8199
Two-external counts: yy :        4346 ww:        1530 xx:        2750 xz:         818 wz:         810 wx:        2990
Three-ext.   counts: yx :        1415 yw:        1165

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:        2479    task #     4:        1239
task #     5:        8526    task #     6:        9200    task #     7:        7175    task #     8:        4962
task #     9:       11867    task #    10:        2061    task #    11:        2061    task #    12:        1071
task #    13:        1071    task #    14:         865    task #    15:        1326    task #    16:         450
task #    17:         332    task #

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1   0.984362      -2.449708E-09  -2.013792E-02  -5.957485E-07   1.391829E-07
 refs   2  -1.128142E-10  -0.980125      -3.216713E-08   3.694230E-02   8.649031E-04

          calcsovref: scrb block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 civs   1    1.00000       1.206917E-07  -4.463442E-10   3.059743E-05  -1.745904E-04
 civs   2   1.206606E-10   -1.00071       2.624570E-06  -0.179978        1.02929    
 civs   3   1.010588E-15  -6.521554E-09    1.00000       7.066111E-06   2.958306E-06
 civs   4   3.739265E-11  -2.639870E-04   6.964522E-06  -0.950289      -0.370617    
 civs   5   1.479918E-07  -0.894423       3.307841E-03   -226.757        1293.89    
 eci,repnuc=   -29.91289488328016         4.196537892870333     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 18  1    -25.7583936770 -3.5527E-15  0.0000E+00  7.9296E-06  1.0000E-05
 mr-sdci # 18  2    -25.7163569904  7.2762E-07  1.6741E-07  4.8429E-04  1.0000E-05
 mr-sdci # 18  3    -25.3945720232  4.3379E-12  0.0000E+00  2.7127E-01  1.0000E-04
 mr-sdci # 18  4    -25.3563282327  4.1419E-02  0.0000E+00  2.4725E-01  1.0000E-04
 mr-sdci # 18  5    -23.9663368663 -1.1297E+00  0.0000E+00  1.0825E+00  1.0000E-04

 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0014
    2   25    0     0.01     0.01     0.00     0.01         0.    0.0013
    3   26    0     0.00     0.00     0.00     0.00         0.    0.0025
    4   27    0     0.01     0.00     0.00     0.01         0.    0.0012
    5   11    0     0.00     0.00     0.00     0.00         0.    0.0085
    6   15    0     0.02     0.00     0.00     0.02         0.    0.0092
    7   16    0     0.02     0.01     0.00     0.02         0.    0.0072
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0050
    9    5    0     0.02     0.01     0.00     0.02         0.    0.0119
   10    6    0     0.00     0.00     0.00     0.00         0.    0.0021
   11   18    0     0.01     0.01     0.00     0.01         0.    0.0021
   12    7    0     0.01     0.00     0.00     0.01         0.    0.0011
   13   19    0     0.00     0.00     0.00     0.00         0.    0.0011
   14   75    0     0.01     0.00     0.01     0.00         0.    0.0009
   15   45    0     0.00     0.00     0.00     0.00         0.    0.0013
   16   46    0     0.01     0.00     0.00     0.01         0.    0.0005
   17   47    0     0.00     0.00     0.00     0.00         0.    0.0003
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1100s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.0400s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

          starting ci iteration  19

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        5280 2x:        1480 4x:         370
All internal counts: zz :        5629 yy:       10577 xx:        5740 ww:        3548
One-external counts: yz :        9682 yx:       10444 yw:        8199
Two-external counts: yy :        4346 ww:        1530 xx:        2750 xz:         818 wz:         810 wx:        2990
Three-ext.   counts: yx :        1415 yw:        1165

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:        2479    task #     4:        1239
task #     5:        8526    task #     6:        9200    task #     7:        7175    task #     8:        4962
task #     9:       11867    task #    10:        2061    task #    11:        2061    task #    12:        1071
task #    13:        1071    task #    14:         865    task #    15:        1326    task #    16:         450
task #    17:         332    task #

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1   0.984362      -2.449708E-09  -2.013792E-02  -5.957485E-07   1.391829E-07   1.857280E-07
 refs   2  -1.128142E-10  -0.980125      -3.216713E-08   3.694230E-02   8.649031E-04   4.402866E-04

          calcsovref: scrb block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 civs   1    1.00000       1.820190E-08   2.639177E-09  -1.818305E-05  -3.180629E-04  -3.167400E-04
 civs   2   3.318412E-10    1.00048      -3.786885E-06  -9.663554E-02   0.403221        1.26961    
 civs   3   8.219273E-15  -1.957200E-09   -1.00000      -1.261926E-05   1.459173E-06   4.903771E-06
 civs   4   2.098708E-11   2.918530E-04   1.039579E-05  -0.915357       0.389575      -0.266246    
 civs   5  -2.058761E-08    1.08206       4.798941E-03   -287.759       -686.165        1111.61    
 civs   6   8.378008E-07  -0.911839      -1.825081E-02    316.581        2280.30        925.919    
 eci,repnuc=   -29.91289503592803         4.196537892870333     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 19  1    -25.7583936770  3.5527E-15  0.0000E+00  7.9296E-06  1.0000E-05
 mr-sdci # 19  2    -25.7163571431  1.5265E-07  2.9620E-08  2.0926E-04  1.0000E-05
 mr-sdci # 19  3    -25.3945720233  2.9733E-11  0.0000E+00  2.7127E-01  1.0000E-04
 mr-sdci # 19  4    -25.3696359580  1.3308E-02  0.0000E+00  2.4033E-01  1.0000E-04
 mr-sdci # 19  5    -24.6089938817  6.4266E-01  0.0000E+00  7.7651E-01  1.0000E-04
 mr-sdci # 19  6    -23.8613043525 -5.5498E-01  0.0000E+00  1.0651E+00  1.0000E-04

 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0014
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0013
    3   26    0     0.01     0.00     0.00     0.01         0.    0.0025
    4   27    0     0.00     0.00     0.00     0.00         0.    0.0012
    5   11    0     0.01     0.00     0.00     0.01         0.    0.0085
    6   15    0     0.02     0.01     0.00     0.02         0.    0.0092
    7   16    0     0.02     0.00     0.00     0.01         0.    0.0072
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0050
    9    5    0     0.02     0.00     0.00     0.02         0.    0.0119
   10    6    0     0.01     0.00     0.00     0.01         0.    0.0021
   11   18    0     0.00     0.00     0.00     0.00         0.    0.0021
   12    7    0     0.01     0.00     0.01     0.01         0.    0.0011
   13   19    0     0.01     0.00     0.00     0.01         0.    0.0011
   14   75    0     0.00     0.00     0.00     0.00         0.    0.0009
   15   45    0     0.00     0.00     0.00     0.00         0.    0.0013
   16   46    0     0.00     0.00     0.00     0.00         0.    0.0005
   17   47    0     0.00     0.00     0.00     0.00         0.    0.0003
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1100s 
time spent in multnx:                   0.1000s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.0100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1200s 

          starting ci iteration  20

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        5280 2x:        1480 4x:         370
All internal counts: zz :        5629 yy:       10577 xx:        5740 ww:        3548
One-external counts: yz :        9682 yx:       10444 yw:        8199
Two-external counts: yy :        4346 ww:        1530 xx:        2750 xz:         818 wz:         810 wx:        2990
Three-ext.   counts: yx :        1415 yw:        1165

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:        2479    task #     4:        1239
task #     5:        8526    task #     6:        9200    task #     7:        7175    task #     8:        4962
task #     9:       11867    task #    10:        2061    task #    11:        2061    task #    12:        1071
task #    13:        1071    task #    14:         865    task #    15:        1326    task #    16:         450
task #    17:         332    task #

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1   0.984362      -2.449708E-09  -2.013792E-02  -5.957485E-07   1.391829E-07   1.857280E-07   1.984503E-07
 refs   2  -1.128142E-10  -0.980125      -3.216713E-08   3.694230E-02   8.649031E-04   4.402866E-04   9.095324E-05

          calcsovref: scrb block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 civs   1    1.00000       1.183670E-07  -2.142850E-08   7.866408E-05  -4.996891E-04  -8.488427E-04  -2.603723E-04
 civs   2   5.758239E-10   -1.00052       1.627907E-05  -0.124883       0.117664       0.899193      -0.974020    
 civs   3   5.399043E-14  -6.645623E-09    1.00000       5.525596E-05  -3.413673E-06   7.780320E-06   3.156767E-06
 civs   4   1.264428E-10  -3.117051E-04   4.844217E-05  -0.893853      -0.364280       0.184676       0.329514    
 civs   5   1.418641E-07   -1.11212       2.092422E-02   -317.240        511.768       -172.017       -1230.65    
 civs   6  -1.663950E-08    1.05789      -2.868062E-02    439.991       -1522.21        1858.95       -619.363    
 civs   7   4.969759E-06  -0.825437       0.123547       -598.211        3663.07        2792.59        2796.15    

 trial vector basis is being transformed.  new dimension:   4
 eci,repnuc=   -29.91289506037722         4.196537892870333     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 20  1    -25.7583936770  0.0000E+00  0.0000E+00  7.9296E-06  1.0000E-05
 mr-sdci # 20  2    -25.7163571675  2.4449E-08  6.4160E-09  1.0540E-04  1.0000E-05
 mr-sdci # 20  3    -25.3945720235  2.4218E-10  0.0000E+00  2.7127E-01  1.0000E-04
 mr-sdci # 20  4    -25.3778389241  8.2030E-03  0.0000E+00  2.0910E-01  1.0000E-04
 mr-sdci # 20  5    -24.9426888454  3.3369E-01  0.0000E+00  7.1051E-01  1.0000E-04
 mr-sdci # 20  6    -24.3731856045  5.1188E-01  0.0000E+00  9.3180E-01  1.0000E-04
 mr-sdci # 20  7    -23.5808656228  4.5825E-02  0.0000E+00  1.1138E+00  1.0000E-04

 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0014
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0013
    3   26    0     0.02     0.00     0.01     0.02         0.    0.0025
    4   27    0     0.00     0.00     0.00     0.00         0.    0.0012
    5   11    0     0.01     0.01     0.00     0.01         0.    0.0085
    6   15    0     0.01     0.01     0.00     0.01         0.    0.0092
    7   16    0     0.02     0.01     0.00     0.02         0.    0.0072
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0050
    9    5    0     0.02     0.00     0.01     0.01         0.    0.0119
   10    6    0     0.01     0.01     0.00     0.01         0.    0.0021
   11   18    0     0.01     0.00     0.00     0.01         0.    0.0021
   12    7    0     0.01     0.00     0.00     0.01         0.    0.0011
   13   19    0     0.00     0.00     0.00     0.00         0.    0.0011
   14   75    0     0.00     0.00     0.00     0.00         0.    0.0009
   15   45    0     0.00     0.00     0.00     0.00         0.    0.0013
   16   46    0     0.00     0.00     0.00     0.00         0.    0.0005
   17   47    0     0.01     0.00     0.00     0.01         0.    0.0003
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1100s 
integral transfer time:                 0.0200s 
time spent for loop construction:       0.0400s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

          starting ci iteration  21

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        5280 2x:        1480 4x:         370
All internal counts: zz :        5629 yy:       10577 xx:        5740 ww:        3548
One-external counts: yz :        9682 yx:       10444 yw:        8199
Two-external counts: yy :        4346 ww:        1530 xx:        2750 xz:         818 wz:         810 wx:        2990
Three-ext.   counts: yx :        1415 yw:        1165

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:        2479    task #     4:        1239
task #     5:        8526    task #     6:        9200    task #     7:        7175    task #     8:        4962
task #     9:       11867    task #    10:        2061    task #    11:        2061    task #    12:        1071
task #    13:        1071    task #    14:         865    task #    15:        1326    task #    16:         450
task #    17:         332    task #

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1   0.984362      -2.830433E-09  -2.013792E-02  -4.296754E-06   1.777695E-07
 refs   2  -1.051340E-10   0.980053       2.508636E-06  -4.568848E-02   3.953962E-05

          calcsovref: scrb block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 civs   1    1.00000       1.309353E-07  -4.830673E-08   1.363152E-04  -2.328384E-03
 civs   2  -6.664994E-10  -0.999972      -1.030798E-05   2.908715E-02  -0.496631    
 civs   3   1.131088E-13  -6.326617E-09    1.00000       8.693055E-05   3.718231E-05
 civs   4   1.232106E-10  -6.456966E-06   8.694023E-05  -0.992605      -0.155560    
 civs   5   1.820807E-05  -0.759661       0.280272       -790.890        13509.1    
 eci,repnuc=   -29.91289506525121         4.196537892870333     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 21  1    -25.7583936770  0.0000E+00  0.0000E+00  7.9296E-06  1.0000E-05
 mr-sdci # 21  2    -25.7163571724  4.8740E-09  1.6761E-09  4.4270E-05  1.0000E-05
 mr-sdci # 21  3    -25.3945720239  3.9952E-10  0.0000E+00  2.7127E-01  1.0000E-04
 mr-sdci # 21  4    -25.3820464469  4.2075E-03  0.0000E+00  2.0635E-01  1.0000E-04
 mr-sdci # 21  5    -24.1502620127 -7.9243E-01  0.0000E+00  1.1855E+00  1.0000E-04

 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0014
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0013
    3   26    0     0.01     0.01     0.00     0.01         0.    0.0025
    4   27    0     0.01     0.00     0.00     0.01         0.    0.0012
    5   11    0     0.00     0.00     0.00     0.00         0.    0.0085
    6   15    0     0.03     0.00     0.01     0.02         0.    0.0092
    7   16    0     0.01     0.00     0.00     0.01         0.    0.0072
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0050
    9    5    0     0.01     0.01     0.00     0.01         0.    0.0119
   10    6    0     0.01     0.01     0.00     0.01         0.    0.0021
   11   18    0     0.01     0.00     0.00     0.01         0.    0.0021
   12    7    0     0.01     0.00     0.01     0.00         0.    0.0011
   13   19    0     0.01     0.01     0.00     0.01         0.    0.0011
   14   75    0     0.00     0.00     0.00     0.00         0.    0.0009
   15   45    0     0.00     0.00     0.00     0.00         0.    0.0013
   16   46    0     0.01     0.01     0.00     0.01         0.    0.0005
   17   47    0     0.00     0.00     0.00     0.00         0.    0.0003
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1000s 
integral transfer time:                 0.0200s 
time spent for loop construction:       0.0500s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1200s 

          starting ci iteration  22

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        5280 2x:        1480 4x:         370
All internal counts: zz :        5629 yy:       10577 xx:        5740 ww:        3548
One-external counts: yz :        9682 yx:       10444 yw:        8199
Two-external counts: yy :        4346 ww:        1530 xx:        2750 xz:         818 wz:         810 wx:        2990
Three-ext.   counts: yx :        1415 yw:        1165

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:        2479    task #     4:        1239
task #     5:        8526    task #     6:        9200    task #     7:        7175    task #     8:        4962
task #     9:       11867    task #    10:        2061    task #    11:        2061    task #    12:        1071
task #    13:        1071    task #    14:         865    task #    15:        1326    task #    16:         450
task #    17:         332    task #

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1   0.984362      -2.830433E-09  -2.013792E-02  -4.296754E-06   1.777695E-07   1.824483E-07
 refs   2  -1.051340E-10   0.980053       2.508636E-06  -4.568848E-02   3.953962E-05   2.771496E-05

          calcsovref: scrb block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 civs   1    1.00000       8.640783E-09   3.053566E-07  -1.233988E-04  -2.393559E-03  -3.029626E-03
 civs   2  -1.986948E-09   0.999992       4.259584E-05  -6.658120E-03  -0.318580      -0.596003    
 civs   3   3.083593E-13  -1.810292E-09   -1.00000      -2.140975E-04  -3.948372E-05   4.638241E-05
 civs   4  -1.305560E-10   1.076745E-05   2.049050E-04  -0.985607       0.128915      -0.146703    
 civs   5  -1.326651E-07    1.03620       0.612413       -1362.88       -6425.00        12264.8    
 civs   6   7.103620E-05   -1.05858       -2.32318        2025.73        19793.5        5177.16    
 eci,repnuc=   -29.91289506702547         4.196537892870333     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 22  1    -25.7583936770  3.5527E-15  0.0000E+00  7.9296E-06  1.0000E-05
 mr-sdci # 22  2    -25.7163571742  1.7743E-09  4.7192E-10  2.6429E-05  1.0000E-05
 mr-sdci # 22  3    -25.3945720272  3.2579E-09  0.0000E+00  2.7127E-01  1.0000E-04
 mr-sdci # 22  4    -25.3852989015  3.2525E-03  0.0000E+00  1.9771E-01  1.0000E-04
 mr-sdci # 22  5    -25.0663425565  9.1608E-01  0.0000E+00  7.0386E-01  1.0000E-04
 mr-sdci # 22  6    -24.0880733714 -2.8511E-01  0.0000E+00  1.1913E+00  1.0000E-04

 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0014
    2   25    0     0.01     0.00     0.00     0.01         0.    0.0013
    3   26    0     0.01     0.00     0.00     0.01         0.    0.0025
    4   27    0     0.00     0.00     0.00     0.00         0.    0.0012
    5   11    0     0.00     0.00     0.00     0.00         0.    0.0085
    6   15    0     0.03     0.01     0.01     0.02         0.    0.0092
    7   16    0     0.01     0.00     0.00     0.01         0.    0.0072
    8    1    0     0.01     0.00     0.00     0.01         0.    0.0050
    9    5    0     0.01     0.00     0.00     0.01         0.    0.0119
   10    6    0     0.01     0.00     0.00     0.01         0.    0.0021
   11   18    0     0.00     0.00     0.00     0.00         0.    0.0021
   12    7    0     0.01     0.00     0.00     0.01         0.    0.0011
   13   19    0     0.01     0.01     0.00     0.01         0.    0.0011
   14   75    0     0.00     0.00     0.00     0.00         0.    0.0009
   15   45    0     0.00     0.00     0.00     0.00         0.    0.0013
   16   46    0     0.00     0.00     0.00     0.00         0.    0.0005
   17   47    0     0.01     0.00     0.00     0.01         0.    0.0003
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1100s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.0200s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

          starting ci iteration  23

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        5280 2x:        1480 4x:         370
All internal counts: zz :        5629 yy:       10577 xx:        5740 ww:        3548
One-external counts: yz :        9682 yx:       10444 yw:        8199
Two-external counts: yy :        4346 ww:        1530 xx:        2750 xz:         818 wz:         810 wx:        2990
Three-ext.   counts: yx :        1415 yw:        1165

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:        2479    task #     4:        1239
task #     5:        8526    task #     6:        9200    task #     7:        7175    task #     8:        4962
task #     9:       11867    task #    10:        2061    task #    11:        2061    task #    12:        1071
task #    13:        1071    task #    14:         865    task #    15:        1326    task #    16:         450
task #    17:         332    task #

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1   0.984362      -2.830433E-09  -2.013792E-02  -4.296754E-06   1.777695E-07   1.824483E-07   2.036846E-07
 refs   2  -1.051340E-10   0.980053       2.508636E-06  -4.568848E-02   3.953962E-05   2.771496E-05   2.023388E-05

          calcsovref: scrb block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 civs   1    1.00000      -1.425396E-07  -1.598069E-06   4.036408E-04  -1.716149E-03  -7.998228E-03  -4.337057E-03
 civs   2  -6.928392E-09   0.999978      -1.504178E-04   3.565788E-02  -9.577148E-02  -0.971436      -0.167689    
 civs   3   3.890973E-12   7.651231E-09    1.00000       8.646385E-04  -1.243401E-04   7.515054E-05   5.624380E-05
 civs   4   1.406351E-10   1.167902E-05   7.974113E-04  -0.961269      -0.254913      -6.052891E-02   0.135012    
 civs   5   2.274999E-05    1.10554        2.65275       -2042.47        5748.10        3948.87       -12517.7    
 civs   6  -2.073518E-05   -1.32402       -5.47388        4148.03       -17386.4        10299.4       -688.234    
 civs   7   3.456669E-04   0.942756        10.6798       -3976.55        19245.7        27830.4        33503.8    

 trial vector basis is being transformed.  new dimension:   4
 eci,repnuc=   -29.91289506747038         4.196537892870333     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 23  1    -25.7583936770 -7.1054E-15  0.0000E+00  7.9296E-06  1.0000E-05
 mr-sdci # 23  2    -25.7163571746  4.4491E-10  1.0720E-10  1.4175E-05  1.0000E-05
 mr-sdci # 23  3    -25.3945720437  1.6475E-08  0.0000E+00  2.7127E-01  1.0000E-04
 mr-sdci # 23  4    -25.3889827778  3.6839E-03  0.0000E+00  1.9716E-01  1.0000E-04
 mr-sdci # 23  5    -25.2544657627  1.8812E-01  0.0000E+00  4.8645E-01  1.0000E-04
 mr-sdci # 23  6    -24.4339341599  3.4586E-01  0.0000E+00  9.6762E-01  1.0000E-04
 mr-sdci # 23  7    -23.6511551165  7.0289E-02  0.0000E+00  1.1527E+00  1.0000E-04

 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0014
    2   25    0     0.01     0.00     0.00     0.01         0.    0.0013
    3   26    0     0.00     0.00     0.00     0.00         0.    0.0025
    4   27    0     0.01     0.00     0.00     0.01         0.    0.0012
    5   11    0     0.01     0.00     0.00     0.01         0.    0.0085
    6   15    0     0.01     0.00     0.00     0.01         0.    0.0092
    7   16    0     0.02     0.01     0.00     0.02         0.    0.0072
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0050
    9    5    0     0.01     0.01     0.00     0.01         0.    0.0119
   10    6    0     0.01     0.01     0.00     0.01         0.    0.0021
   11   18    0     0.01     0.00     0.00     0.01         0.    0.0021
   12    7    0     0.00     0.00     0.00     0.00         0.    0.0011
   13   19    0     0.01     0.00     0.00     0.01         0.    0.0011
   14   75    0     0.00     0.00     0.00     0.00         0.    0.0009
   15   45    0     0.00     0.00     0.00     0.00         0.    0.0013
   16   46    0     0.01     0.01     0.00     0.01         0.    0.0005
   17   47    0     0.00     0.00     0.00     0.00         0.    0.0003
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1100s 
time spent in multnx:                   0.1100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0400s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1200s 

          starting ci iteration  24

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        5280 2x:        1480 4x:         370
All internal counts: zz :        5629 yy:       10577 xx:        5740 ww:        3548
One-external counts: yz :        9682 yx:       10444 yw:        8199
Two-external counts: yy :        4346 ww:        1530 xx:        2750 xz:         818 wz:         810 wx:        2990
Three-ext.   counts: yx :        1415 yw:        1165

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:        2479    task #     4:        1239
task #     5:        8526    task #     6:        9200    task #     7:        7175    task #     8:        4962
task #     9:       11867    task #    10:        2061    task #    11:        2061    task #    12:        1071
task #    13:        1071    task #    14:         865    task #    15:        1326    task #    16:         450
task #    17:         332    task #

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1   0.984362       3.644533E-09  -2.013784E-02  -3.220267E-05  -2.132389E-07
 refs   2   4.172798E-10   0.980058      -1.206754E-05   3.260845E-02  -9.532163E-06

          calcsovref: scrb block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 civs   1    1.00000      -1.722861E-07   6.175942E-06   1.095123E-03   2.298464E-02
 civs   2  -1.306993E-08   -1.00001       2.597330E-04   4.605610E-02   0.966665    
 civs   3   1.388944E-11   8.147998E-09  -0.999996       2.912056E-03  -4.441382E-04
 civs   4   1.308613E-09   8.844728E-07  -2.911578E-03  -0.995615       0.115759    
 civs   5  -1.502703E-03  -0.833315        29.8725        5297.01        111175.    
 eci,repnuc=   -29.91289506755972         4.196537892870333     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 24  1    -25.7583936770  0.0000E+00  0.0000E+00  7.9296E-06  1.0000E-05
 mr-sdci # 24  2    -25.7163571747  8.9333E-11  2.5237E-11  5.5303E-06  1.0000E-05
 mr-sdci # 24  3    -25.3945720881  4.4460E-08  0.0000E+00  2.7127E-01  1.0000E-04
 mr-sdci # 24  4    -25.3918899064  2.9071E-03  0.0000E+00  1.8555E-01  1.0000E-04
 mr-sdci # 24  5    -24.1083982499 -1.1461E+00  0.0000E+00  1.1683E+00  1.0000E-04

======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0014
    2   25    0     0.01     0.00     0.01     0.00         0.    0.0013
    3   26    0     0.01     0.00     0.00     0.01         0.    0.0025
    4   27    0     0.01     0.00     0.00     0.01         0.    0.0012
    5   11    0     0.01     0.00     0.00     0.01         0.    0.0085
    6   15    0     0.01     0.00     0.00     0.01         0.    0.0092
    7   16    0     0.02     0.01     0.00     0.02         0.    0.0072
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0050
    9    5    0     0.02     0.01     0.00     0.02         0.    0.0119
   10    6    0     0.00     0.00     0.00     0.00         0.    0.0021
   11   18    0     0.02     0.00     0.00     0.01         0.    0.0021
   12    7    0     0.00     0.00     0.00     0.00         0.    0.0011
   13   19    0     0.01     0.00     0.00     0.01         0.    0.0011
   14   75    0     0.00     0.00     0.00     0.00         0.    0.0009
   15   45    0     0.00     0.00     0.00     0.00         0.    0.0013
   16   46    0     0.01     0.00     0.00     0.01         0.    0.0005
   17   47    0     0.00     0.00     0.00     0.00         0.    0.0003
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1300s 
time spent in multnx:                   0.1100s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.0200s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

 mr-sdci  convergence criteria satisfied after 24 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 24  1    -25.7583936770  0.0000E+00  0.0000E+00  7.9296E-06  1.0000E-05
 mr-sdci # 24  2    -25.7163571747  8.9333E-11  2.5237E-11  5.5303E-06  1.0000E-05
 mr-sdci # 24  3    -25.3945720881  4.4460E-08  0.0000E+00  2.7127E-01  1.0000E-04
 mr-sdci # 24  4    -25.3918899064  2.9071E-03  0.0000E+00  1.8555E-01  1.0000E-04
 mr-sdci # 24  5    -24.1083982499 -1.1461E+00  0.0000E+00  1.1683E+00  1.0000E-04

####################CIUDGINFO####################

   ci vector at position   1 energy=  -25.758393676995
   ci vector at position   2 energy=  -25.716357174689

################END OF CIUDGINFO################


    2 of the   6 expansion vectors are transformed.
    2 of the   5 matrix-vector products are transformed.

    2 expansion eigenvectors written to unit nvfile (= 11)
    2 matrix-vector products written to unit nhvfil (= 10)
 maximum overlap with reference                         1 (overlap= 
   0.9843621500697308      )

information on vector: 1from unit 11 written to unit 48filename civout              
 maximum overlap with reference                         2 (overlap= 
   0.9800568776731955      )

information on vector: 2from unit 11 written to unit 48filename civout              


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -25.7583936770

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7

                                          orbital     1    2    3    4    5    6   19

                                         symmetry   a'   a'   a'   a'   a'   a'   a" 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  2  1       1  0.947322                        +-   +-   +-   +                 
 z*  2  1       3  0.074011                        +-   +-   +-             +       
 z*  2  1       5  0.119429                        +-   +-   +     -   +            
 z*  2  1       7 -0.101932                        +-   +-   +    +     -           
 z*  2  1      10  0.024485                        +-   +-   +          -   +       
 z*  2  1      11  0.024445                        +-   +-   +         +     -      
 z*  2  1      15 -0.036145                        +-   +-        +-        +       
 z*  2  1      16  0.095674                        +-   +-        +    +-           
 z*  2  1      19  0.036853                        +-   +-        +         +-      
 z*  2  1      20  0.029346                        +-   +-        +              +- 
 z*  2  1      25  0.073295                        +-   +    +-   +-                
 z*  2  1      27 -0.012190                        +-   +    +-    -        +       
 z*  2  1      29 -0.029154                        +-   +    +-   +          -      
 z*  2  1      33  0.012176                        +-   +    +-             +-      
 z*  2  1      34 -0.057484                        +-   +    +-                  +- 
 z*  2  1      35 -0.067485                        +-   +     -   +-   +            
 z*  2  1      38  0.043797                        +-   +     -   +     -   +       
 z*  2  1      39  0.071382                        +-   +     -   +    +     -      
 z*  2  1      46 -0.017010                        +-   +    +    +-    -           
 z*  2  1      49 -0.016131                        +-   +    +     -    -   +       
 z*  2  1      77 -0.019738                        +-        +-   +-        +       
 z*  2  1      78  0.033776                        +-        +-   +    +-           
 z*  2  1      81  0.047732                        +-        +-   +         +-      
 z*  2  1      82  0.040638                        +-        +-   +              +- 
 z*  2  1     109  0.010454                        +-             +    +-   +-      
 y   2  1     120  0.031779              5( a' )   +-   +-   +-                     
 y   2  1     121  0.011689              6( a' )   +-   +-   +-                     
 y   2  1     122  0.023080              7( a' )   +-   +-   +-                     
 y   2  1     124 -0.011966              9( a' )   +-   +-   +-                     
 y   2  1     129 -0.016389              2( a' )   +-   +-    -   +                 
 y   2  1     145  0.038553              6( a' )   +-   +-    -        +            
 y   2  1     166  0.027165              3( a" )   +-   +-    -                  +  
 y   2  1     172  0.012966              4( a' )   +-   +-   +     -                
 y   2  1     176 -0.025002              8( a' )   +-   +-   +     -                
 y   2  1     181  0.015923              1( a' )   +-   +-   +          -           
 y   2  1     185  0.019648              5( a' )   +-   +-   +          -           
 y   2  1     207 -0.014084              3( a" )   +-   +-   +                    - 
 y   2  1     215 -0.010255              6( a' )   +-   +-        +-                
 y   2  1     362  0.010774              3( a' )   +-    -   +-   +                 
 y   2  1     365  0.011245              6( a' )   +-    -   +-   +                 
 y   2  1     373  0.010748              2( a' )   +-    -   +-        +            
 y   2  1     397 -0.010817              2( a" )   +-    -   +-                  +  
 y   2  1     418 -0.012843              6( a' )   +-    -   +     -   +            
 y   2  1     447  0.014623              6( a' )   +-    -   +    +     -           
 y   2  1     455 -0.010060              2( a' )   +-    -   +    +          -      
 y   2  1     732 -0.015408              5( a' )   +-   +    +-    -                
 y   2  1     747  0.013443              8( a' )   +-   +    +-         -           
 y   2  1     752  0.012862              1( a' )   +-   +    +-              -      
 y   2  1     764 -0.011413              1( a" )   +-   +    +-                   - 
 y   2  1     765 -0.020431              2( a" )   +-   +    +-                   - 
 y   2  1     794 -0.011867              2( a' )   +-   +     -    -        +       
 y   2  1     800 -0.012375              8( a' )   +-   +     -    -        +       
 y   2  1     807 -0.020960              3( a" )   +-   +     -    -             +  
 y   2  1     823  0.023194              2( a' )   +-   +     -   +          -      
 y   2  1     829  0.017465              8( a' )   +-   +     -   +          -      
 y   2  1     836  0.035720              3( a" )   +-   +     -   +               - 
 y   2  1    1176 -0.010962              3( a' )   +-        +-   +-                
 y   2  1    1211  0.013967              2( a" )   +-        +-    -             +  
 y   2  1    1229 -0.013658              3( a' )   +-        +-   +          -      
 y   2  1    1240 -0.023234              2( a" )   +-        +-   +               - 
 x   2  1    2006 -0.022324    1( a' )   2( a' )   +-   +-    -                     
 x   2  1    2013  0.014303    2( a' )   5( a' )   +-   +-    -                     
 x   2  1    2032  0.013439    6( a' )   8( a' )   +-   +-    -                     
 x   2  1    2074  0.012443    2( a" )   3( a" )   +-   +-    -                     
 x   2  1    2448  0.017464    2( a' )   3( a' )   +-    -    -   +                 
 x   2  1    2457  0.010670    2( a' )   6( a' )   +-    -    -   +                 
 w   2  1    8287 -0.016876    1( a' )   2( a' )   +-   +-   +                      
 w   2  1    8297 -0.015305    2( a' )   5( a' )   +-   +-   +                      
 w   2  1    8381 -0.027512    2( a' )   2( a' )   +-   +-        +                 
 w   2  1    8384 -0.013973    3( a' )   3( a' )   +-   +-        +                 
 w   2  1    8386 -0.018041    2( a' )   4( a' )   +-   +-        +                 
 w   2  1    8388 -0.014294    4( a' )   4( a' )   +-   +-        +                 
 w   2  1    8399 -0.027763    6( a' )   6( a' )   +-   +-        +                 
 w   2  1    8414 -0.010476    8( a' )   8( a' )   +-   +-        +                 
 w   2  1    8462 -0.019053    3( a" )   3( a" )   +-   +-        +                 
 w   2  1    8815  0.027641    2( a' )   3( a' )   +-   +     -   +                 
 w   2  1    8819  0.024349    3( a' )   4( a' )   +-   +     -   +                 
 w   2  1    8844 -0.010922    6( a' )   8( a' )   +-   +     -   +                 
 w   2  1    8861  0.012689    6( a' )  10( a' )   +-   +     -   +                 
 w   2  1    8879 -0.011446    3( a' )  12( a' )   +-   +     -   +                 
 w   2  1    8893  0.019162    2( a" )   3( a" )   +-   +     -   +                 
 w   2  1    8896  0.014148    2( a" )   4( a" )   +-   +     -   +                 
 w   2  1    8902  0.011852    4( a" )   5( a" )   +-   +     -   +                 
 w   2  1   10784 -0.022422    3( a' )   3( a' )   +-        +-   +                 
 w   2  1   10799 -0.013632    6( a' )   6( a' )   +-        +-   +                 
 w   2  1   10859 -0.018197    2( a" )   2( a" )   +-        +-   +                 

 ci coefficient statistics:
           rq > 0.1                3
      0.1> rq > 0.01              83
     0.01> rq > 0.001            819
    0.001> rq > 0.0001          3106
   0.0001> rq > 0.00001         2462
  0.00001> rq > 0.000001         476
 0.000001> rq                   7021
           all                 13970
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        1505 2x:           0 4x:           0
All internal counts: zz :        5629 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:        2479    task #     4:        1239
task #     5:        8526    task #     6:        9200    task #     7:        7175    task #     8:        4962
task #     9:       11867    task #    10:        2061    task #    11:        2061    task #    12:        1071
task #    13:        1071    task #    14:         865    task #    15:        1326    task #    16:         450
task #    17:         332    task #
  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.947322234807    -24.347474718964
     2     2      0.000000046515     -0.000001194295
     3     3      0.074011110693     -1.898146747344
     4     4     -0.000000243672      0.000006264426
     5     5      0.119429192169     -3.068163798990
     6     6     -0.000000036655      0.000000943072
     7     7     -0.101932170507      2.619065027795
     8     8     -0.000000000286      0.000000008028
     9     9      0.000000027834     -0.000000713692
    10    10      0.024484598497     -0.623820807493
    11    11      0.024444762044     -0.631247717874
    12    12      0.000000010336     -0.000000265518
    13    13      0.000000009059     -0.000000231212
    14    14     -0.000000045204      0.000001161141
    15    15     -0.036145168078      0.932408992570
    16    16      0.095673850381     -2.464695846458
    17    17     -0.000000005113      0.000000131143
    18    18     -0.000000004540      0.000000118362
    19    19      0.036853123619     -0.948890581242
    20    20      0.029345555398     -0.750159995874
    21    21     -0.008237109562      0.213728678528
    22    22     -0.000000000208      0.000000004730
    23    23      0.000000001733     -0.000000042972
    24    24     -0.004966690381      0.129129063842
    25    25      0.073294695205     -1.883144545107
    26    26     -0.000000013569      0.000000352647
    27    27     -0.012189513968      0.309979125458
    28    28     -0.000000002224      0.000000058705
    29    29     -0.029153899378      0.750189675887
    30    30     -0.001709840191      0.040250052154
    31    31      0.000000002947     -0.000000077223
    32    32      0.000000013102     -0.000000337318
    33    33      0.012176175963     -0.315167524454
    34    34     -0.057484254409      1.482352867919
    35    35     -0.067484544583      1.739358439214
    36    36      0.000000004238     -0.000000106109
    37    37     -0.000000001721      0.000000045383
    38    38      0.043797376533     -1.132432697162
    39    39      0.071381719885     -1.846771147863
    40    40     -0.000000001460      0.000000039006
    41    41      0.000000009430     -0.000000244512
    42    42      0.000000000477     -0.000000011535
    43    43     -0.008492400761      0.219581322548
    44    44     -0.005974242946      0.154927200176
    45    45      0.000000000167     -0.000000003829
    46    46     -0.017010423604      0.436325182622
    47    47      0.000000007851     -0.000000201752
    48    48     -0.000000002160      0.000000056309
    49    49     -0.016131300214      0.414352728762
    50    50     -0.001854558186      0.050459049298
    51    51     -0.000000006289      0.000000161098
    52    52      0.000000020266     -0.000000521187
    53    53     -0.007672833412      0.195177604517
    54    54     -0.000000001807      0.000000047901
    55    55     -0.003161176618      0.080866832232
    56    56      0.004695522414     -0.122576501687
    57    57     -0.000000000959      0.000000023152
    58    58     -0.007203872736      0.185043984063
    59    59     -0.000000003889      0.000000099910
    60    60     -0.000000014236      0.000000364877
    61    61     -0.004184405641      0.108322377022
    62    62     -0.000116577243      0.002140391321
    63    63     -0.000378102767      0.009140196773
    64    64      0.000000002207     -0.000000056410
    65    65      0.000000000615     -0.000000017181
    66    66     -0.001340266752      0.034895646279
    67    67     -0.008492354571      0.220217480310
    68    68      0.000000000912     -0.000000022868
    69    69     -0.000000001727      0.000000044176
    70    70     -0.000063888009      0.001434374720
    71    71     -0.003405549317      0.088103138406
    72    72      0.005916979270     -0.153454110463
    73    73     -0.000000000389      0.000000008988
    74    74      0.000000001452     -0.000000035313
    75    75      0.002099809569     -0.054385556759
    76    76      0.000000001528     -0.000000038090
    77    77     -0.019738353326      0.511208336708
    78    78      0.033776066904     -0.873410603602
    79    79     -0.000000001292      0.000000034165
    80    80     -0.000000000452      0.000000011177
    81    81      0.047732207588     -1.231417289918
    82    82      0.040637960669     -1.041780339272
    83    83     -0.001939331065      0.050624132063
    84    84      0.000000002498     -0.000000064617
    85    85      0.000000000517     -0.000000013273
    86    86     -0.001455237783      0.037835509205
    87    87     -0.000000009661      0.000000248400
    88    88     -0.004619118956      0.120655778182
    89    89      0.001313834023     -0.034378934207
    90    90     -0.000000013459      0.000000345102
    91    91     -0.000000010284      0.000000264054
    92    92      0.000000003614     -0.000000093456
    93    93      0.006192167051     -0.160714935287
    94    94      0.000911675457     -0.022320642186
    95    95     -0.000000000249      0.000000005824
    96    96      0.000000000140     -0.000000003459
    97    97     -0.004490658749      0.116548789264
    98    98     -0.002485453284      0.063075259871
    99    99      0.000000000336     -0.000000007890
   100   100      0.000000002590     -0.000000068041
   101   101      0.000000001573     -0.000000040435
   102   102      0.004288529148     -0.111251539069
   103   103      0.003447768416     -0.089379461407
   104   104      0.000000001729     -0.000000042953
   105   105      0.007756073602     -0.201568942497
   106   106     -0.000000002513      0.000000064244
   107   107     -0.000000000219      0.000000005782
   108   108     -0.001813492601      0.046571655369
   109   109      0.010454146857     -0.271411966355
   110   110      0.005309099532     -0.136684324104
   111   111     -0.000000001080      0.000000028077
   112   112     -0.000000000681      0.000000017945
   113   113      0.003164563652     -0.081439705207
   114   114      0.001055307869     -0.027492540962
   115   115     -0.000000000598      0.000000013548

 number of reference csfs (nref) is   115.  root number (iroot) is  1.
 c0**2 =   0.96965664  c**2 (all zwalks) =   0.96965664

 pople ci energy extrapolation is computed with  7 correlated electrons.

 eref      =    -25.703701602666   "relaxed" cnot**2         =   0.969656643738
 eci       =    -25.758393676995   deltae = eci - eref       =  -0.054692074329
 eci+dv1   =    -25.760053218091   dv1 = (1-cnot**2)*deltae  =  -0.001659541096
 eci+dv2   =    -25.760105149924   dv2 = dv1 / cnot**2       =  -0.001711472929
 eci+dv3   =    -25.760160436944   dv3 = dv1 / (2*cnot**2-1) =  -0.001766759949
 eci+pople =    -25.759632521201   (  7e- scaled deltae )    =  -0.055930918534


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 2) =       -25.7163571747

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7

                                          orbital     1    2    3    4    5    6   19

                                         symmetry   a'   a'   a'   a'   a'   a'   a" 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  2  1       2 -0.034306                        +-   +-   +-        +            
 z*  2  1       4 -0.938105                        +-   +-   +    +-                
 z*  2  1       6  0.090526                        +-   +-   +     -        +       
 z*  2  1       8  0.048118                        +-   +-   +    +          -      
 z*  2  1       9  0.083171                        +-   +-   +         +-           
 z*  2  1      12  0.039232                        +-   +-   +              +-      
 z*  2  1      13  0.177860                        +-   +-   +                   +- 
 z*  2  1      14  0.033593                        +-   +-        +-   +            
 z*  2  1      18  0.050524                        +-   +-        +    +     -      
 z*  2  1      26  0.092421                        +-   +    +-    -   +            
 z*  2  1      28  0.044599                        +-   +    +-   +     -           
 z*  2  1      36  0.045274                        +-   +     -   +-        +       
 z*  2  1      40  0.010078                        +-   +     -   +         +-      
 z*  2  1      41 -0.027876                        +-   +     -   +              +- 
 z*  2  1      45  0.012326                        +-   +     -             +    +- 
 z*  2  1      47  0.017649                        +-   +    +    +-         -      
 z*  2  1      51 -0.018353                        +-   +    +     -        +-      
 z*  2  1      52  0.060267                        +-   +    +     -             +- 
 z*  2  1      59 -0.013400                        +-   +         +-    -   +       
 z*  2  1      60 -0.052636                        +-   +         +-   +     -      
 z*  2  1      64 -0.013512                        +-   +          -   +    +-      
 z*  2  1      74  0.013084                        +-   +              +     -   +- 
 z*  2  1      76  0.028034                        +-        +-   +-   +            
 z*  2  1      85  0.011395                        +-        +-        +         +- 
 z*  2  1      87 -0.031213                        +-        +    +-   +-           
 z*  2  1      90 -0.043965                        +-        +    +-        +-      
 z*  2  1      91 -0.035093                        +-        +    +-             +- 
 z*  2  1     104  0.011445                        +-        +              +-   +- 
 y   2  1     123  0.021656              8( a' )   +-   +-   +-                     
 y   2  1     128 -0.014822              1( a' )   +-   +-    -   +                 
 y   2  1     132 -0.024114              5( a' )   +-   +-    -   +                 
 y   2  1     134 -0.015063              7( a' )   +-   +-    -   +                 
 y   2  1     147  0.018543              8( a' )   +-   +-    -        +            
 y   2  1     156 -0.013761              5( a' )   +-   +-    -             +       
 y   2  1     157 -0.010273              6( a' )   +-   +-    -             +       
 y   2  1     165 -0.025276              2( a" )   +-   +-    -                  +  
 y   2  1     173  0.034313              5( a' )   +-   +-   +     -                
 y   2  1     174  0.034271              6( a' )   +-   +-   +     -                
 y   2  1     175  0.017474              7( a' )   +-   +-   +     -                
 y   2  1     177 -0.011524              9( a' )   +-   +-   +     -                
 y   2  1     188 -0.028005              8( a' )   +-   +-   +          -           
 y   2  1     197  0.024751              5( a' )   +-   +-   +               -      
 y   2  1     198  0.015271              6( a' )   +-   +-   +               -      
 y   2  1     199  0.014601              7( a' )   +-   +-   +               -      
 y   2  1     205  0.012136              1( a" )   +-   +-   +                    - 
 y   2  1     206  0.037378              2( a" )   +-   +-   +                    - 
 y   2  1     211 -0.013110              2( a' )   +-   +-        +-                
 y   2  1     217 -0.014345              8( a' )   +-   +-        +-                
 y   2  1     222 -0.011053              1( a' )   +-   +-         -   +            
 y   2  1     226 -0.027839              5( a' )   +-   +-         -   +            
 y   2  1     227 -0.012466              6( a' )   +-   +-         -   +            
 y   2  1     230 -0.010680              9( a' )   +-   +-         -   +            
 y   2  1     256 -0.028842              6( a' )   +-   +-        +     -           
 y   2  1     277 -0.026380              3( a" )   +-   +-        +               - 
 y   2  1     403  0.011773              3( a' )   +-    -   +    +-                
 y   2  1     406  0.010427              6( a' )   +-    -   +    +-                
 y   2  1     443 -0.010818              2( a' )   +-    -   +    +     -           
 y   2  1     454 -0.011056              1( a' )   +-    -   +    +          -      
 y   2  1     458 -0.012351              5( a' )   +-    -   +    +          -      
 y   2  1     467  0.012393              2( a" )   +-    -   +    +               - 
 y   2  1     556  0.010610              6( a' )   +-    -        +-   +            
 y   2  1     771  0.017090              3( a' )   +-   +     -   +-                
 y   2  1     773 -0.010037              5( a' )   +-   +     -   +-                
 y   2  1     774 -0.044963              6( a' )   +-   +     -   +-                
 y   2  1     775 -0.015500              7( a' )   +-   +     -   +-                
 y   2  1     788 -0.010737              8( a' )   +-   +     -    -   +            
 y   2  1     806  0.010555              2( a" )   +-   +     -    -             +  
 y   2  1     926 -0.013042              8( a' )   +-   +    +     -    -           
 y   2  1     931 -0.011028              1( a' )   +-   +    +     -         -      
 y   2  1     935 -0.014573              5( a' )   +-   +    +     -         -      
 y   2  1     943  0.015706              1( a" )   +-   +    +     -              - 
 y   2  1     944  0.015996              2( a" )   +-   +    +     -              - 
 y   2  1     983 -0.014132              2( a' )   +-   +         +-         -      
 y   2  1     989 -0.018361              8( a' )   +-   +         +-         -      
 y   2  1     996 -0.025853              3( a" )   +-   +         +-              - 
 y   2  1    1175 -0.018810              2( a' )   +-        +-   +-                
 y   2  1    1177 -0.021993              4( a' )   +-        +-   +-                
 y   2  1    1349 -0.013266              2( a" )   +-         -   +-             +  
 y   2  1    1515  0.013819              3( a' )   +-        +    +-         -      
 y   2  1    1526  0.022969              2( a" )   +-        +    +-              - 
 x   2  1    2082  0.013407    1( a' )   2( a' )   +-   +-         -                
 x   2  1    2089 -0.012502    2( a' )   5( a' )   +-   +-         -                
 x   2  1    3024 -0.010071    2( a' )   3( a' )   +-    -        +-                
 w   2  1    8286 -0.029729    1( a' )   1( a' )   +-   +-   +                      
 w   2  1    8296 -0.032836    1( a' )   5( a' )   +-   +-   +                      
 w   2  1    8300 -0.025378    5( a' )   5( a' )   +-   +-   +                      
 w   2  1    8306 -0.010087    6( a' )   6( a' )   +-   +-   +                      
 w   2  1    8321 -0.013871    8( a' )   8( a' )   +-   +-   +                      
 w   2  1    8366 -0.013439    2( a" )   2( a" )   +-   +-   +                      
 w   2  1    8380 -0.013206    1( a' )   2( a' )   +-   +-        +                 
 w   2  1    8387  0.010388    3( a' )   4( a' )   +-   +-        +                 
 w   2  1    8390 -0.014249    2( a' )   5( a' )   +-   +-        +                 
 w   2  1    9162 -0.010084    3( a' )   5( a' )   +-   +    +     -                
 w   2  1    9493 -0.018258    2( a' )   3( a' )   +-   +         +-                
 w   2  1    9497 -0.019482    3( a' )   4( a' )   +-   +         +-                
 w   2  1    9571 -0.013738    2( a" )   3( a" )   +-   +         +-                
 w   2  1    9574 -0.011921    2( a" )   4( a" )   +-   +         +-                
 w   2  1   11123  0.021917    3( a' )   3( a' )   +-        +    +-                
 w   2  1   11138  0.014712    6( a' )   6( a' )   +-        +    +-                
 w   2  1   11198  0.018074    2( a" )   2( a" )   +-        +    +-                

 ci coefficient statistics:
           rq > 0.1                2
      0.1> rq > 0.01              98
     0.01> rq > 0.001            946
    0.001> rq > 0.0001          3289
   0.0001> rq > 0.00001         2200
  0.00001> rq > 0.000001         390
 0.000001> rq                   7045
           all                 13970
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        1505 2x:           0 4x:           0
All internal counts: zz :        5629 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:        2479    task #     4:        1239
task #     5:        8526    task #     6:        9200    task #     7:        7175    task #     8:        4962
task #     9:       11867    task #    10:        2061    task #    11:        2061    task #    12:        1071
task #    13:        1071    task #    14:         865    task #    15:        1326    task #    16:         450
task #    17:         332    task #
  iref  icsf         v(icsf)             hv(icsf)
     1     1     -0.000000226733      0.000005827598
     2     2     -0.034305833990      0.877695594708
     3     3      0.000000017493     -0.000000463002
     4     4     -0.938104771381     24.063327723386
     5     5      0.000000001684     -0.000000056952
     6     6      0.090526346819     -2.322605309951
     7     7      0.000000017963     -0.000000447034
     8     8      0.048117528825     -1.228109274272
     9     9      0.083170692675     -2.128630813459
    10    10     -0.000000007110      0.000000169888
    11    11      0.000000009417     -0.000000245546
    12    12      0.039231888495     -0.997385223000
    13    13      0.177859541725     -4.568977433789
    14    14      0.033593492755     -0.868586449964
    15    15     -0.000000026485      0.000000683084
    16    16     -0.000000040999      0.000001031589
    17    17     -0.005541020597      0.140798492544
    18    18      0.050524373863     -1.297613826478
    19    19     -0.000000017939      0.000000446721
    20    20     -0.000000049909      0.000001279617
    21    21     -0.000000000647      0.000000012918
    22    22      0.005459306456     -0.138899896452
    23    23     -0.001068310264      0.028287336780
    24    24      0.000000000088     -0.000000003098
    25    25     -0.000000012008      0.000000301743
    26    26      0.092421463483     -2.378879903529
    27    27      0.000000015465     -0.000000395562
    28    28      0.044598836414     -1.146647670731
    29    29      0.000000012139     -0.000000303853
    30    30     -0.000000000003      0.000000000952
    31    31      0.006533184461     -0.171006604469
    32    32      0.006318957610     -0.166593939155
    33    33     -0.000000006680      0.000000166541
    34    34      0.000000023139     -0.000000590208
    35    35     -0.000000014455      0.000000380510
    36    36      0.045273994814     -1.172855875305
    37    37     -0.000513087498      0.009121667196
    38    38     -0.000000010228      0.000000256864
    39    39     -0.000000022703      0.000000570978
    40    40      0.010077691962     -0.260460836564
    41    41     -0.027876389167      0.717614651062
    42    42     -0.005566915873      0.142352925770
    43    43     -0.000000003350      0.000000083585
    44    44     -0.000000000470      0.000000011525
    45    45      0.012325834797     -0.317652274555
    46    46     -0.000000006639      0.000000177530
    47    47      0.017648851977     -0.458948546944
    48    48     -0.000707257577      0.021279868871
    49    49     -0.000000003836      0.000000103512
    50    50     -0.000000019590      0.000000502710
    51    51     -0.018352958950      0.473009099443
    52    52      0.060267077253     -1.552597745611
    53    53      0.000000003142     -0.000000072266
    54    54     -0.003060957903      0.080943895683
    55    55     -0.000000000963      0.000000023069
    56    56     -0.000000000606      0.000000015772
    57    57     -0.000523906568      0.009471247939
    58    58      0.000000001139     -0.000000030852
    59    59     -0.013399963949      0.348628523585
    60    60     -0.052635934935      1.359710755097
    61    61      0.000000003666     -0.000000095262
    62    62     -0.000000013871      0.000000357828
    63    63      0.000000001526     -0.000000039293
    64    64     -0.013511919867      0.348809789991
    65    65     -0.003059986977      0.078754267197
    66    66     -0.000000002958      0.000000076104
    67    67      0.000000001130     -0.000000032193
    68    68     -0.003842707267      0.098330719232
    69    69     -0.000715071426      0.017043300903
    70    70     -0.000000000168      0.000000005299
    71    71     -0.000000000086      0.000000000345
    72    72     -0.000000000406      0.000000011778
    73    73      0.001849627200     -0.048054878911
    74    74      0.013084209440     -0.339211654363
    75    75      0.000000000053     -0.000000000866
    76    76      0.028033750994     -0.724376986870
    77    77      0.000000003054     -0.000000074357
    78    78     -0.000000013160      0.000000331833
    79    79      0.001482972685     -0.039061239059
    80    80     -0.008047178396      0.207114238512
    81    81     -0.000000019891      0.000000496037
    82    82     -0.000000016477      0.000000414032
    83    83     -0.000000001710      0.000000044020
    84    84     -0.003197953858      0.082796724717
    85    85      0.011395396113     -0.294980755761
    86    86      0.000000002154     -0.000000055897
    87    87     -0.031213181283      0.805206025612
    88    88      0.000000000214     -0.000000006113
    89    89      0.000000002415     -0.000000061954
    90    90     -0.043964622106      1.131989497921
    91    91     -0.035092895258      0.896850635162
    92    92     -0.005645414504      0.145987807593
    93    93      0.000000003798     -0.000000096281
    94    94     -0.000000003013      0.000000078509
    95    95      0.005823573977     -0.149893944453
    96    96     -0.002668807714      0.068665065526
    97    97      0.000000000104     -0.000000004553
    98    98     -0.000000000261      0.000000006236
    99    99      0.000832735949     -0.020554421040
   100   100      0.006115107025     -0.157694998106
   101   101      0.009923043597     -0.256101876392
   102   102     -0.000000000056      0.000000002499
   103   103      0.000000000721     -0.000000017934
   104   104      0.011445279240     -0.295510411055
   105   105      0.000000001627     -0.000000040132
   106   106      0.007565921095     -0.195979443707
   107   107      0.002900729179     -0.074774127078
   108   108     -0.000000001936      0.000000049595
   109   109     -0.000000001999      0.000000055290
   110   110     -0.000000002863      0.000000075116
   111   111      0.000559443033     -0.014688603711
   112   112      0.003891543642     -0.100549100001
   113   113     -0.000000003054      0.000000079847
   114   114     -0.000000000231      0.000000005611
   115   115      0.002252244215     -0.058240342405

 number of reference csfs (nref) is   115.  root number (iroot) is  2.
 c0**2 =   0.96246688  c**2 (all zwalks) =   0.96246688

 pople ci energy extrapolation is computed with  7 correlated electrons.

 eref      =    -25.654790727398   "relaxed" cnot**2         =   0.962466880261
 eci       =    -25.716357174689   deltae = eci - eref       =  -0.061566447291
 eci+dv1   =    -25.718667955527   dv1 = (1-cnot**2)*deltae  =  -0.002310780838
 eci+dv2   =    -25.718758068565   dv2 = dv1 / cnot**2       =  -0.002400893875
 eci+dv3   =    -25.718855495041   dv3 = dv1 / (2*cnot**2-1) =  -0.002498320352
 eci+pople =    -25.718100687934   (  7e- scaled deltae )    =  -0.063309960536
 passed aftci ... 
 readint2: molcas,dalton2=                        1                        0
 files%faoints=aoints              
molcasbasis:  18   6

 Integral file header information:
 Using SEWARD integrals


   ******  Basis set information:  ******

 Number of irreps:                  2
 Total number of basis functions:  24

 irrep no.              1    2
 irrep label           a'   a" 
 no. of bas.fcions.    18    6
 input basis function labels, i:bfnlab(i)=
B1  1s     B1  2s     B1  *s     B1  2px    B1  *px    B1  2py    B1  *py    B1  *d2-   B1  *d0    B1  *d2+
H1  1s     H1  *s     H1  *px    H1  *py    H2  1s     H2  *s     H2  *px    H2  *py    B1  2pz    B1  *pz 
B1  *d1-   B1  *d1+   H1  *pz    H2  *pz 
 map-vector 1 , imtype=                        3
   1   1   1   1   1   1   1   1   1   1   2   2   2   2   3   3   3   3   1   1
  1   1   2   3
 map-vector 2 , imtype=                        4
   1   1   1   2   2   2   2   4   4   4   1   1   2   2   1   1   2   2   2   2
  4   4   2   2
 <<< Entering RdOne >>>
 rc on entry:            0
 Label on entry:  MLTPL  0
 Comp on entry:          1
 SymLab on entry:        1
 Option on entry:        6
 picked LabelMLTPL  0 Component                        1 currop= 
                        1
 Computed Symlab=                        1
  I will close the file for you!

          overlap matrix SAO basis block   1

               sao   1        sao   2        sao   3        sao   4        sao   5        sao   6        sao   7        sao   8
  sao   1     1.00000000
  sao   2    -0.00000120     1.00000000
  sao   3     0.18219354     0.94106828     1.00000000
  sao   4     0.00000000     0.00000000     0.00000000     1.00000000
  sao   5     0.00000000     0.00000000     0.00000000     0.83507774     1.00000000
  sao   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
  sao   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.83507774     1.00000000
  sao   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     1.00000000
  sao  11     0.06306161     0.49736838     0.55008417     0.44164400     0.49413895     0.28984200     0.32429337     0.27580617
  sao  12     0.09223998     0.60878906     0.68063452     0.42057119     0.51882874     0.27601234     0.34049678     0.13154356
  sao  13    -0.03497853    -0.18618989    -0.13536409    -0.11433429     0.03770702    -0.19732958    -0.09705900    -0.23773874
  sao  14    -0.02295570    -0.12219265    -0.08883671    -0.19732958    -0.09705900     0.05684139     0.12190190     0.01549226
  sao  15     0.06306161     0.49736838     0.55008417     0.44164400     0.49413895    -0.28984200    -0.32429337    -0.27580617
  sao  16     0.09223998     0.60878906     0.68063452     0.42057119     0.51882874    -0.27601234    -0.34049678    -0.13154356
  sao  17    -0.03497853    -0.18618989    -0.13536409    -0.11433429     0.03770702     0.19732958     0.09705900     0.23773874
  sao  18     0.02295570     0.12219265     0.08883671     0.19732958     0.09705900     0.05684139     0.12190190     0.01549226

               sao   9        sao  10        sao  11        sao  12        sao  13        sao  14        sao  15        sao  16
  sao   9     1.00000000
  sao  10     0.00000000     1.00000000
  sao  11    -0.17356969     0.11962552     1.00000000
  sao  12    -0.08278268     0.05705444     0.90368690     1.00000000
  sao  13     0.10010096     0.11240356     0.00000000     0.00000000     1.00000000
  sao  14     0.06569423    -0.32167417     0.00000000     0.00000000     0.00000000     1.00000000
  sao  15    -0.17356969     0.11962552     0.37716799     0.48659392     0.00000000    -0.18287858     1.00000000
  sao  16    -0.08278268     0.05705444     0.48659392     0.60860937     0.00000000    -0.17552552     0.90368690     1.00000000
  sao  17     0.10010096     0.11240356     0.00000000     0.00000000     0.05186452     0.00000000     0.00000000     0.00000000
  sao  18    -0.06569423     0.32167417     0.18287858     0.17552552     0.00000000    -0.25508220     0.00000000     0.00000000

               sao  17        sao  18
  sao  17     1.00000000
  sao  18     0.00000000     1.00000000

          overlap matrix SAO basis block   2

               sao  19        sao  20        sao  21        sao  22        sao  23        sao  24
  sao  19     1.00000000
  sao  20     0.83507774     1.00000000
  sao  21     0.00000000     0.00000000     1.00000000
  sao  22     0.00000000     0.00000000     0.00000000     1.00000000
  sao  23     0.18634480     0.18559976     0.19772118     0.30127577     1.00000000
  sao  24     0.18634480     0.18559976    -0.19772118     0.30127577     0.05186452     1.00000000
NO coefficients and occupation numbers are written to nocoef_ci.1

################################################################################
 one electron density matrix calculation 
################################################################################


 workspace allocation information: mxcore=       13105869
sifcfg setup: record length 4096 DP
# d1 elements per record  3272
# d2 elements per record  2730
 computing final density
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
 1-DENSITY: executing task                        11                        1
 1-DENSITY: executing task                        13                        3
 1-DENSITY: executing task                        14                        4
 1-DENSITY: executing task                         1                        1
 1-DENSITY: executing task                         2                        2
 1-DENSITY: executing task                         3                        3
 1-DENSITY: executing task                         4                        4
 1-DENSITY: executing task                        71                        1
 1-DENSITY: executing task                        52                        2
 1-DENSITY: executing task                        72                        2
 1-DENSITY: executing task                        53                        3
 1-DENSITY: executing task                        73                        3
 1-DENSITY: executing task                        54                        4
 1-DENSITY: executing task                        74                        4
================================================================================
   DYZ=     692  DYX=     815  DYW=     670
   D0Z=     740  D0Y=    1405  D0X=     475  D0W=     310
  DDZI=     530 DDYI=     900 DDXI=     330 DDWI=     250
  DDZE=       0 DDYE=     210 DDXE=      90 DDWE=      70
================================================================================
 Trace of MO density:     6.999999999999992     
                        7 correlated and                         0 
 frozen core electrons


*****   symmetry block SYM1   *****

               no    1        no    2        no    3        no    4        no    5        no    6        no    7        no    8

      occ    2.00000        1.94815        1.92556       0.993203       4.978766E-02   2.557525E-02   8.681156E-03   6.869268E-03

  mo    1    1.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  mo    2    0.00000       0.997880       4.383204E-10   6.373229E-02  -4.606572E-10  -8.241899E-03  -2.462518E-10   8.581932E-03
  mo    3    0.00000      -1.698056E-08   0.995664       2.569653E-07   9.035506E-02  -1.687957E-08   1.847860E-02   4.090867E-09
  mo    4    0.00000      -6.422235E-02  -2.567322E-07   0.995262      -2.900462E-08  -5.875793E-02  -5.598322E-09   1.478734E-02
  mo    5    0.00000       1.876022E-11  -9.188908E-02   3.535955E-09   0.991512      -3.305043E-08   8.672080E-02   2.130587E-09
  mo    6    0.00000       4.554082E-03  -1.216259E-09   6.208338E-02   3.297339E-08   0.981095      -1.285239E-09   3.335398E-02
  mo    7    0.00000       3.768208E-03  -6.507003E-10  -9.894167E-05  -6.786185E-09  -1.036279E-02   3.195503E-08  -0.194689    
  mo    8    0.00000      -7.199031E-11  -1.264610E-02   1.030958E-09  -8.704397E-02   4.787256E-09   0.803548       3.052189E-08
  mo    9    0.00000      -6.604918E-03   7.027469E-10  -6.546174E-03   4.822926E-09   0.106967      -2.866838E-08   0.517729    
  mo   10    0.00000      -3.966611E-10   1.018102E-03  -3.781555E-10  -3.216803E-02   1.308164E-09   0.517796       2.213700E-08
  mo   11    0.00000       1.375321E-03  -1.179154E-09  -2.902991E-02  -7.216674E-09   4.692683E-02   1.402858E-08   0.194419    
  mo   12    0.00000      -5.167277E-03   5.686969E-10  -4.620833E-03  -2.743813E-09  -0.137917      -2.425323E-08   0.755551    
  mo   13    0.00000       5.154514E-05  -1.028885E-09  -2.165500E-02  -3.632786E-10   1.872881E-02  -7.613368E-09   0.273132    
  mo   14    0.00000      -1.781385E-11  -6.113782E-03  -3.724570E-10  -9.995151E-03  -1.158064E-09   0.189835       1.335629E-08
  mo   15    0.00000       1.412073E-03  -1.384900E-10   1.212787E-02  -7.784422E-10  -1.412856E-02   7.722619E-10   6.692614E-02
  mo   16    0.00000       9.491108E-11   3.109441E-03  -2.441769E-11   4.225671E-03   6.370042E-10   4.623909E-02  -4.237334E-09
  mo   17    0.00000      -1.207239E-03   1.301923E-10   3.898338E-03   2.394502E-09   2.467314E-02  -7.959775E-09   7.007849E-02
  mo   18    0.00000      -8.662010E-12  -1.105906E-03  -9.465721E-11  -3.234356E-03   2.402104E-09  -0.200401      -3.922401E-09

               no    9        no   10        no   11        no   12        no   13        no   14        no   15        no   16

      occ   4.648980E-03   3.981806E-03   2.516270E-03   1.128239E-03   9.818620E-04   6.098940E-04   4.795451E-04   4.090658E-04

  mo    2   5.219896E-03   4.502345E-04  -1.988904E-10  -1.477885E-03   4.989083E-10  -1.492886E-10   1.449911E-03  -2.322178E-05
  mo    3  -1.618550E-09   8.025959E-09   5.317629E-03   7.224803E-10  -8.680139E-03  -6.205905E-03   9.603663E-09   9.845231E-10
  mo    4  -1.439515E-03   2.660302E-02  -6.724269E-10   2.396473E-03   5.010061E-09   2.187582E-09   3.021730E-02   3.972172E-03
  mo    5  -3.886922E-09   7.824614E-09  -3.242254E-03  -2.274137E-09  -2.466399E-02  -1.768043E-02   3.736755E-09  -1.479332E-09
  mo    6  -0.122854      -0.117926      -1.560492E-09  -3.706438E-02   6.823443E-10  -1.264238E-10  -4.215836E-02   9.336491E-03
  mo    7  -0.387774       0.523836       4.724483E-08  -0.115873      -2.182703E-08   2.038112E-09  -0.366389       0.354017    
  mo    8   1.935246E-08  -3.553918E-09  -7.220156E-02  -4.280352E-08  -0.529352      -0.153948       3.418414E-08  -2.655428E-08
  mo    9   0.654876       0.426663       1.690543E-08  -2.228149E-02   5.068658E-09  -1.206434E-09  -0.133942      -0.151227    
  mo   10   1.006061E-08  -1.096649E-08  -0.209585       5.956216E-08   0.628145      -1.999057E-02  -5.072171E-08   2.314019E-08
  mo   11  -0.350322       0.638643       3.653395E-08  -0.146913       5.524669E-08   3.718985E-09   0.347687      -0.174668    
  mo   12  -0.409660      -0.335485      -7.746474E-09  -0.281682       2.171107E-09  -3.939957E-09  -0.202583       8.089292E-02
  mo   13  -0.124489       1.917547E-02  -3.041771E-08   0.656026      -4.199332E-08   2.731907E-08   0.527169       0.360110    
  mo   14   2.430619E-08  -6.373312E-08   0.968172       2.009907E-08   9.894651E-02   0.100006       2.462814E-08   1.931989E-08
  mo   15  -0.219799       6.916645E-02   2.289789E-08   0.663646      -8.151498E-08   2.766448E-08  -0.544435      -0.424944    
  mo   16   8.583447E-09   1.444068E-09  -9.767629E-02  -6.200351E-08  -0.285237       0.909223       1.673664E-08  -1.427306E-08
  mo   17   0.226474       5.807692E-02  -3.368561E-09   0.113889      -6.444851E-08   2.780626E-09  -0.329976       0.710204    
  mo   18  -7.986202E-09   6.545461E-09   6.264201E-02  -3.211660E-08  -0.483100      -0.372641       3.570728E-08  -1.985782E-08

               no   17        no   18

      occ   1.071447E-04   5.286131E-05

  mo    2   2.024780E-10  -2.339122E-05
  mo    3  -2.280942E-03  -1.200854E-09
  mo    4   1.262157E-09  -3.775351E-03
  mo    5   2.858318E-03  -7.098977E-10
  mo    6  -2.237688E-09   1.505357E-02
  mo    7   3.226827E-09   0.514010    
  mo    8  -0.193513      -2.251415E-09
  mo    9   4.374575E-10   0.261184    
  mo   10   0.540333      -1.474421E-09
  mo   11   2.532017E-09  -0.505554    
  mo   12  -3.692818E-09   5.266379E-02
  mo   13  -7.625954E-10   0.266003    
  mo   14   8.170849E-02  -5.344267E-10
  mo   15   1.875144E-10  -0.157047    
  mo   16   0.283279      -1.390079E-10
  mo   17   4.924174E-11  -0.559961    
  mo   18   0.763974       5.803031E-10

               no    1        no    2        no    3        no    4        no    5        no    6        no    7        no    8

      occ    2.00000        1.94815        1.92556       0.993203       4.978766E-02   2.557525E-02   8.681156E-03   6.869268E-03

  ao    1    1.00081      -2.635491E-04  -5.982498E-10  -1.521681E-02  -1.321364E-09   0.155970       1.296462E-08   0.130402    
  ao    2   6.820240E-04   0.666736      -7.539739E-09  -0.710128       5.763472E-09    1.22074       6.779428E-08   0.633891    
  ao    3  -4.667026E-03  -0.147928       4.326636E-09  -5.715417E-02   2.307071E-08  -0.422529      -8.180292E-08  -0.673265    
  ao    4   1.139236E-03   0.273284       3.157961E-09   0.699848       3.510614E-08    1.19409      -3.724864E-08   0.161367    
  ao    5  -2.216494E-03  -8.111746E-02  -1.015312E-09  -9.771940E-02  -1.238350E-08  -0.295964       4.312106E-08  -0.323412    
  ao    6   1.755151E-11   1.897481E-09   0.660896      -4.204369E-09   -1.19591       3.553406E-08   0.753989       2.860224E-08
  ao    7  -8.861160E-12  -7.365261E-10  -0.110033      -7.993063E-10   0.316066      -1.355490E-08  -0.878186      -3.323026E-08
  ao    8   4.491487E-12   1.242194E-10   3.878979E-02  -7.963310E-10   3.577269E-02   8.205990E-10   0.264578       1.623406E-08
  ao    9  -4.976591E-04  -1.330340E-02   8.801858E-10   1.679751E-03   9.057642E-10  -6.826939E-02  -2.039670E-09   3.997245E-02
  ao   10   3.826727E-04   6.279004E-03  -7.603987E-11  -1.228003E-02  -1.438451E-09  -0.100597      -2.117655E-08   0.648837    
  ao   11  -3.868983E-03   0.453939       0.501552       0.234130       0.892364      -0.940902       0.710193       0.158186    
  ao   12   3.081472E-03  -0.144104       1.127217E-02  -2.483255E-02  -0.108978       0.193378       -1.04069      -0.197860    
  ao   13   4.570031E-04  -1.012122E-02  -1.211714E-02   5.387352E-03   1.875358E-03  -3.888810E-02  -0.148123       5.018808E-02
  ao   14   4.724148E-04  -1.464202E-02   1.482137E-03  -1.282158E-02   1.512922E-03  -1.295628E-02  -0.130164      -0.264028    
  ao   15  -3.868983E-03   0.453939      -0.501552       0.234130      -0.892364      -0.940902      -0.710193       0.158186    
  ao   16   3.081472E-03  -0.144104      -1.127217E-02  -2.483254E-02   0.108978       0.193378        1.04069      -0.197860    
  ao   17   4.570031E-04  -1.012122E-02   1.211714E-02   5.387352E-03  -1.875362E-03  -3.888811E-02   0.148123       5.018809E-02
  ao   18  -4.724148E-04   1.464202E-02   1.482136E-03   1.282158E-02   1.512925E-03   1.295629E-02  -0.130164       0.264028    

               no    9        no   10        no   11        no   12        no   13        no   14        no   15        no   16

      occ   4.648980E-03   3.981806E-03   2.516270E-03   1.128239E-03   9.818620E-04   6.098940E-04   4.795451E-04   4.090658E-04

  ao    1  -9.395411E-02   0.520731       3.627564E-08  -0.251270       6.517004E-08  -4.653175E-09   0.143541      -0.388931    
  ao    2  -0.670468        2.54203       1.765138E-07   -1.22347       3.223605E-07  -2.295282E-08   0.805246       -1.88484    
  ao    3   0.710809       -3.00582      -1.993579E-07    1.51953      -4.225399E-07   1.905638E-08   -1.22740        2.17599    
  ao    4   0.273740      -0.876582      -6.239343E-08  -0.198980       1.081552E-07  -2.390164E-08   0.880037       -1.17336    
  ao    5  -0.630021       0.704193       5.485328E-08   0.263066      -1.556219E-07   1.966929E-08  -0.954969        1.62008    
  ao    6   1.799774E-08  -6.392746E-09  -7.432053E-04  -1.082094E-07   -1.23133      -0.131233       8.012106E-08  -4.486459E-08
  ao    7  -7.194693E-09  -3.316983E-08   5.484223E-02   1.658038E-07    1.85858       0.363509      -1.296950E-07   8.494283E-08
  ao    8   3.145597E-08  -7.316110E-08    1.00586      -1.220172E-08  -0.221786       0.346347       4.728217E-08   1.725329E-08
  ao    9  -0.130732      -0.225697       3.098569E-08  -0.553069      -5.349511E-09  -1.587704E-08  -0.865496      -3.486649E-02
  ao   10  -0.149993      -0.233431      -3.227928E-08  -0.587906       7.651951E-08  -2.078714E-08   0.574974       0.301446    
  ao   11    1.12659       0.438882       -1.34677      -0.162209       0.790477      -0.362621       -1.27521      -0.238523    
  ao   12   -1.05176      -0.182179        1.19501      -0.100051       -1.61042       0.458049        1.42876      -0.172583    
  ao   13  -0.291938      -4.573284E-02  -0.118187       0.151276      -0.482151       0.273492       8.338365E-02  -0.687751    
  ao   14   8.228669E-02  -3.835873E-02   0.135446      -0.525963      -0.122203      -0.778894       0.387699      -0.150610    
  ao   15    1.12659       0.438882        1.34677      -0.162209      -0.790477       0.362621       -1.27521      -0.238523    
  ao   16   -1.05176      -0.182179       -1.19501      -0.100051        1.61042      -0.458049        1.42876      -0.172583    
  ao   17  -0.291938      -4.573286E-02   0.118187       0.151276       0.482152      -0.273492       8.338360E-02  -0.687751    
  ao   18  -8.228670E-02   3.835873E-02   0.135446       0.525963      -0.122203      -0.778894      -0.387699       0.150610    

               no   17        no   18

      occ   1.071447E-04   5.286131E-05

  ao    1   9.373250E-09  -0.381854    
  ao    2   4.878734E-08   -1.25134    
  ao    3  -4.633176E-08    4.87575    
  ao    4   5.375750E-09   0.479790    
  ao    5  -2.589710E-10    2.38933    
  ao    6   0.494156      -9.420322E-09
  ao    7    1.05037       8.312698E-09
  ao    8    1.09595      -4.172996E-09
  ao    9  -7.033001E-09  -0.745743    
  ao   10   2.041627E-09   0.556630    
  ao   11  -0.247119      -0.360935    
  ao   12   -1.70210       -2.21395    
  ao   13   0.767681       0.499983    
  ao   14   0.381521       0.376827    
  ao   15   0.247119      -0.360935    
  ao   16    1.70210       -2.21395    
  ao   17  -0.767681       0.499983    
  ao   18   0.381521      -0.376827    


*****   symmetry block SYM2   *****

               no    1        no    2        no    3        no    4        no    5        no    6

      occ   1.795177E-02   5.041309E-03   3.161300E-03   6.676299E-04   3.479872E-04   8.911542E-05

  mo    1   0.971495      -4.369168E-11  -0.183876       1.593217E-09   4.377261E-02   0.143077    
  mo    2  -0.127277      -3.892181E-09   0.113792      -6.149515E-08   0.467171       0.867527    
  mo    3   0.183569      -1.141657E-07   0.943589       2.676479E-08  -0.271151       4.917951E-02
  mo    4   2.312908E-08   0.967264       1.081364E-07  -0.253773      -4.078936E-08  -2.474582E-09
  mo    5  -6.650282E-11   0.253773       3.708293E-08   0.967264       1.178746E-07   1.353249E-09
  mo    6   7.937645E-02  -2.331593E-08   0.250758      -1.052454E-07   0.840424      -0.473822    

               no    1        no    2        no    3        no    4        no    5        no    6

      occ   1.795177E-02   5.041309E-03   3.161300E-03   6.676299E-04   3.479872E-04   8.911542E-05

  ao    1   -1.16100      -1.413233E-08   0.434070      -1.167922E-07   0.831797        1.05023    
  ao    2   0.273335       2.435963E-09  -0.169116       1.007403E-07  -0.764483       -1.62516    
  ao    3  -2.093058E-08  -0.810026      -8.438624E-08   0.658584       9.081524E-08   2.151234E-09
  ao    4  -8.645128E-02   8.433148E-08  -0.688425      -9.447448E-08   0.786203      -0.358949    
  ao    5  -0.101848      -0.289193      -0.327082      -0.700796      -0.606636       0.378967    
  ao    6  -0.101848       0.289193      -0.327082       0.700796      -0.606636       0.378967    


 total number of electrons =    7.0000000000

 test slabel:                        2
  a'  a" 


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!


                        a'  partial gross atomic populations
   ao class       1a'        2a'        3a'        4a'        5a'        6a' 
    B1_ s       1.999928   0.826255   0.000000   0.423651   0.000000   0.004424
    B1_ p       0.000004   0.191839   0.943586   0.494152   0.025505   0.009148
    B1_ d      -0.000001   0.005204   0.024217  -0.000741   0.000888   0.000404
    H1_ s       0.000086   0.453960   0.475258   0.036636   0.011670   0.005362
    H1_ p      -0.000052   0.008467   0.003618   0.001434   0.000027   0.000437
    H2_ s       0.000086   0.453960   0.475258   0.036636   0.011670   0.005362
    H2_ p      -0.000052   0.008467   0.003618   0.001434   0.000027   0.000437

   ao class       7a'        8a'        9a'       10a'       11a'       12a' 
    B1_ s       0.000000   0.000369   0.000012   0.002707   0.000000   0.000109
    B1_ p       0.003164   0.000358   0.001086   0.000719   0.000007  -0.000008
    B1_ d       0.001031   0.003765   0.000323   0.000451   0.001614   0.000500
    H1_ s       0.001663   0.000163   0.001068   0.000045   0.000312  -0.000007
    H1_ p       0.000580   0.001026   0.000546   0.000008   0.000135   0.000270
    H2_ s       0.001663   0.000163   0.001068   0.000045   0.000312  -0.000007
    H2_ p       0.000580   0.001026   0.000546   0.000008   0.000135   0.000270

   ao class      13a'       14a'       15a'       16a'       17a'       18a' 
    B1_ s       0.000000   0.000000  -0.000006   0.000035   0.000000   0.000004
    B1_ p       0.000486   0.000018   0.000025   0.000115   0.000011   0.000013
    B1_ d      -0.000003   0.000024   0.000301   0.000022   0.000019   0.000007
    H1_ s       0.000079   0.000007   0.000054  -0.000008   0.000012   0.000009
    H1_ p       0.000171   0.000277   0.000026   0.000126   0.000027   0.000006
    H2_ s       0.000079   0.000007   0.000054  -0.000008   0.000012   0.000009
    H2_ p       0.000171   0.000277   0.000026   0.000126   0.000027   0.000006

                        a"  partial gross atomic populations
   ao class       1a"        2a"        3a"        4a"        5a"        6a" 
    B1_ p       0.016630   0.000000   0.000196   0.000000   0.000069   0.000072
    B1_ d       0.000229   0.003775   0.001927   0.000168   0.000115   0.000004
    H1_ p       0.000546   0.000633   0.000519   0.000250   0.000082   0.000006
    H2_ p       0.000546   0.000633   0.000519   0.000250   0.000082   0.000006


                        gross atomic populations
     ao           B1_        H1_        H2_
      s         3.257487   0.986370   0.986370
      p         1.687196   0.019167   0.019167
      d         0.044243   0.000000   0.000000
    total       4.988926   1.005537   1.005537


 Total number of electrons:    7.00000000

NO coefficients and occupation numbers are written to nocoef_ci.2

################################################################################
 one electron density matrix calculation 
################################################################################


 workspace allocation information: mxcore=       13105869
sifcfg setup: record length 4096 DP
# d1 elements per record  3272
# d2 elements per record  2730
 computing final density
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    2
--------------------------------------------------------------------------------
 1-DENSITY: executing task                        11                        1
 1-DENSITY: executing task                        13                        3
 1-DENSITY: executing task                        14                        4
 1-DENSITY: executing task                         1                        1
 1-DENSITY: executing task                         2                        2
 1-DENSITY: executing task                         3                        3
 1-DENSITY: executing task                         4                        4
 1-DENSITY: executing task                        71                        1
 1-DENSITY: executing task                        52                        2
 1-DENSITY: executing task                        72                        2
 1-DENSITY: executing task                        53                        3
 1-DENSITY: executing task                        73                        3
 1-DENSITY: executing task                        54                        4
 1-DENSITY: executing task                        74                        4
================================================================================
   DYZ=     692  DYX=     815  DYW=     670
   D0Z=     740  D0Y=    1405  D0X=     475  D0W=     310
  DDZI=     530 DDYI=     900 DDXI=     330 DDWI=     250
  DDZE=       0 DDYE=     210 DDXE=      90 DDWE=      70
================================================================================
 Trace of MO density:     6.999999999999999     
                        7 correlated and                         0 
 frozen core electrons


*****   symmetry block SYM1   *****

               no    1        no    2        no    3        no    4        no    5        no    6        no    7        no    8

      occ    2.00000        1.94556        1.85766       0.999009       4.214991E-02   2.723264E-02   9.772409E-03   7.136800E-03

  mo    1    1.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  mo    2    0.00000       0.987605      -0.156419      -1.411254E-08  -2.574906E-10   4.818058E-03  -3.374345E-03  -8.330639E-03
  mo    3    0.00000       5.149099E-08   2.356348E-07   0.999357      -3.003988E-02   7.629307E-09  -6.405749E-09  -7.588856E-09
  mo    4    0.00000       0.155486       0.985210      -2.409829E-07   2.438770E-10   6.550447E-02  -1.742226E-02  -1.864115E-02
  mo    5    0.00000      -8.737631E-10  -4.924673E-09   2.918757E-02   0.992315       1.749830E-07   3.464267E-09   1.612444E-08
  mo    6    0.00000      -1.392132E-02  -5.853498E-02   3.572123E-09  -1.763261E-07   0.972558       0.208991       4.464398E-04
  mo    7    0.00000      -2.546833E-03   1.913326E-03   2.111287E-09   1.770628E-08  -0.111408       0.542859      -0.412928    
  mo    8    0.00000      -3.536366E-10  -2.318641E-09   1.574335E-02   1.646392E-02   6.707398E-09  -1.107615E-07   1.387039E-07
  mo    9    0.00000       6.135928E-03  -8.824946E-03  -8.968639E-10  -1.739149E-08   7.932050E-02  -5.525024E-03   0.118633    
  mo   10    0.00000      -1.808374E-10  -2.551998E-09   6.636955E-03  -2.305616E-02  -2.611360E-08  -4.678349E-08   1.271113E-07
  mo   11    0.00000       9.375508E-03   2.715646E-02   2.530486E-09   3.218660E-08  -0.154538       0.745980      -7.120631E-02
  mo   12    0.00000       9.569161E-03   1.954569E-02   5.209760E-10  -2.123397E-09  -6.685516E-02   0.257316       0.820654    
  mo   13    0.00000       6.307992E-03   1.429603E-02   1.346771E-09  -8.546394E-11  -2.463090E-02   0.170130       0.362962    
  mo   14    0.00000       5.967806E-10   2.627753E-09   1.085467E-02   0.107575       5.173201E-08  -5.898881E-09  -2.771909E-08
  mo   15    0.00000      -1.645095E-03  -6.359488E-03   5.090690E-10   5.795841E-09  -1.858419E-02   5.236756E-02   6.752226E-02
  mo   16    0.00000       6.083789E-11   4.545144E-10  -4.611669E-03   4.408095E-02   1.286051E-08   5.673648E-10   1.957170E-08
  mo   17    0.00000       9.142089E-04  -4.767178E-03  -3.073001E-10  -1.137405E-08   4.254104E-02  -8.304410E-02  -1.247109E-02
  mo   18    0.00000       1.182973E-10   4.236645E-10  -1.396004E-03   9.505425E-03  -7.615031E-10   1.815526E-08  -4.989128E-08

               no    9        no   10        no   11        no   12        no   13        no   14        no   15        no   16

      occ   4.973822E-03   4.642210E-03   3.197482E-03   1.250609E-03   9.194754E-04   8.016773E-04   5.976946E-04   3.682015E-04

  mo    2  -2.784099E-09  -7.919016E-03  -7.877797E-10   3.750574E-04  -1.402910E-05   4.812555E-11   9.821660E-04  -1.491803E-10
  mo    3  -1.652864E-02   9.519292E-09  -7.086612E-03   3.505746E-09  -1.634105E-09   1.953507E-03   6.670677E-10   7.336956E-03
  mo    4   8.627669E-09  -3.644516E-04  -5.445253E-09   1.387170E-02  -4.690670E-03  -2.133445E-09   1.772159E-03  -3.705046E-09
  mo    5  -8.621567E-03   8.162632E-09  -0.110232      -8.457659E-10  -7.328991E-09   1.918885E-02   8.589714E-09  -4.270989E-02
  mo    6  -1.100628E-08  -8.023502E-02  -3.588110E-08  -1.560461E-02   1.826733E-03   4.219863E-09  -1.213015E-02  -8.158108E-09
  mo    7   9.223983E-08  -8.528167E-02  -1.477831E-08   0.281755      -0.230383      -1.149211E-07   0.534074      -5.529102E-08
  mo    8   0.720820      -3.907123E-07   6.792997E-02  -4.659140E-08   1.670865E-07  -0.640159      -1.053480E-08  -0.189294    
  mo    9   4.462855E-07   0.913418       1.080928E-08   0.223662      -0.142112      -8.093134E-08  -7.357597E-02   2.110783E-09
  mo   10   0.625371      -3.012958E-07  -0.189976       5.144328E-08  -1.502441E-07   0.542528      -2.096143E-08  -2.523281E-03
  mo   11   1.890222E-07   0.168168       1.909797E-09  -0.233455       0.114174       2.442937E-08  -0.441079       3.602933E-08
  mo   12  -2.238233E-07  -0.187574       1.535295E-08   6.671409E-02  -0.456517      -1.292660E-07   8.138410E-02   1.355648E-08
  mo   13   1.764262E-09   0.104081       8.130854E-08  -0.101295       0.780563       2.138561E-07   0.440886      -9.259216E-08
  mo   14   0.102686      -7.004451E-08   0.966996       3.058391E-08  -1.032747E-07   0.190893      -2.655771E-08   7.078915E-02
  mo   15  -1.240845E-07  -0.192038      -3.473553E-09   0.864381       0.294950       1.792277E-08  -0.346986      -4.251143E-08
  mo   16   4.953788E-02  -2.874877E-08  -3.065123E-02   3.527204E-08   1.377752E-07  -0.282306       7.420123E-08   0.929164    
  mo   17   9.368325E-08   0.201789       9.137546E-09   0.231917      -9.478375E-02  -2.569409E-08   0.439598      -4.630232E-08
  mo   18  -0.275653       1.395567E-07   0.105227      -5.531512E-08   6.427326E-08  -0.423498      -7.197011E-08  -0.306473    

               no   17        no   18

      occ   1.330242E-04   1.121284E-04

  mo    2   9.087092E-10  -1.391746E-03
  mo    3   1.182987E-03  -6.484050E-10
  mo    4   4.060428E-09  -5.134521E-03
  mo    5  -6.416848E-03  -5.096153E-09
  mo    6   9.329236E-09   1.243405E-03
  mo    7   1.986460E-07  -0.312094    
  mo    8  -0.172190      -8.472466E-08
  mo    9   1.348329E-07  -0.263671    
  mo   10   0.527164       2.923358E-07
  mo   11  -2.321666E-07   0.351236    
  mo   12  -6.292383E-11  -2.656573E-02
  mo   13   1.073867E-07  -0.117093    
  mo   14   3.511392E-02   1.575147E-08
  mo   15  -2.533915E-08   2.967738E-02
  mo   16   0.227155       1.379613E-07
  mo   17  -4.633277E-07   0.833295    
  mo   18   0.799733       4.725269E-07

               no    1        no    2        no    3        no    4        no    5        no    6        no    7        no    8

      occ    2.00000        1.94556        1.85766       0.999009       4.214991E-02   2.723264E-02   9.772409E-03   7.136800E-03

  ao    1    1.00081       2.634468E-03   3.125321E-03   1.576445E-09   3.110141E-09   1.101586E-02   0.484191      -0.149684    
  ao    2   6.820240E-04   0.510932      -0.796048      -1.504138E-08  -5.792103E-08   0.408547        2.62432      -0.731883    
  ao    3  -4.667026E-03  -0.218378      -0.232269      -8.601890E-09  -1.343973E-07   0.482120       -2.82928       0.812410    
  ao    4   1.139236E-03   0.384696       0.471960       9.806899E-09  -2.545253E-07    1.39595      -0.462112       0.438814    
  ao    5  -2.216494E-03  -0.112061      -7.428004E-02   2.464692E-09   4.806053E-08  -0.363044       0.717392      -0.572173    
  ao    6   1.755151E-11   9.316405E-09  -7.668349E-09   0.557583       -1.10059      -1.987070E-07  -1.226177E-07   1.091662E-07
  ao    7  -8.861160E-12  -1.392770E-09   2.935584E-09  -0.132824       0.160756       5.936492E-09   1.388236E-07  -1.108614E-07
  ao    8   4.491487E-12   1.349814E-09   7.788335E-10   6.148041E-02   0.198942       6.679260E-08  -1.160142E-08  -1.287522E-08
  ao    9  -4.976591E-04  -1.966456E-02  -2.841556E-02  -1.208367E-09  -3.848142E-09   1.517818E-02  -7.817741E-02   2.488550E-02
  ao   10   3.826727E-04   2.585490E-02   3.616242E-02   2.569089E-10   1.656544E-10  -7.018798E-02   0.249247       0.686926    
  ao   11  -3.868983E-03   0.521452       0.173059       0.608816       0.696979      -0.807860      -0.716544      -0.300475    
  ao   12   3.081472E-03  -0.138607       8.932936E-02  -9.406558E-03  -2.676810E-02  -9.035795E-03   0.682210       0.201200    
  ao   13   4.570031E-04  -9.790350E-03   1.158762E-02  -1.980800E-02   2.126462E-02  -4.811688E-02   0.113841       0.122543    
  ao   14   4.724148E-04  -1.931367E-02  -4.240507E-03   7.704012E-03  -8.114749E-03  -1.660804E-02  -7.009464E-02  -0.236309    
  ao   15  -3.868983E-03   0.521452       0.173059      -0.608816      -0.696978      -0.807860      -0.716544      -0.300476    
  ao   16   3.081472E-03  -0.138607       8.932934E-02   9.406573E-03   2.676813E-02  -9.035945E-03   0.682209       0.201201    
  ao   17   4.570031E-04  -9.790350E-03   1.158762E-02   1.980801E-02  -2.126460E-02  -4.811688E-02   0.113841       0.122543    
  ao   18  -4.724148E-04   1.931367E-02   4.240507E-03   7.704013E-03  -8.114761E-03   1.660803E-02   7.009466E-02   0.236309    

               no    9        no   10        no   11        no   12        no   13        no   14        no   15        no   16

      occ   4.973822E-03   4.642210E-03   3.197482E-03   1.250609E-03   9.194754E-04   8.016773E-04   5.976946E-04   3.682015E-04

  ao    1   2.233752E-07   0.250786      -2.350917E-08  -0.121269      -7.992698E-02  -2.782752E-08  -0.575649       5.775033E-08
  ao    2   1.074323E-06    1.13788      -1.125866E-07  -0.684064      -0.294706      -1.227469E-07   -2.56872       2.510126E-07
  ao    3  -1.160232E-06   -1.18517       1.411691E-07    1.14734       0.270565       2.251158E-09    4.36685      -4.530252E-07
  ao    4  -1.577280E-07  -7.954845E-02  -3.844277E-08  -0.789655       0.281169       1.364562E-07  -0.954758       1.014718E-07
  ao    5   7.985889E-08  -0.158161       4.032842E-08   0.884822      -0.274169      -2.100868E-07    2.53046      -2.692209E-07
  ao    6   0.660299      -3.801521E-07   0.356681      -9.293395E-08   3.373053E-07   -1.35243      -4.848490E-08  -7.700837E-02
  ao    7  -0.607069       3.748900E-07  -0.216531       1.689398E-07  -5.344864E-07    1.99264       2.541896E-09   0.440801    
  ao    8   0.140355      -8.797132E-08    1.07934       3.146733E-08  -3.374408E-08  -9.491048E-02  -4.171633E-08   0.386287    
  ao    9  -1.787055E-07  -0.315824      -6.725821E-08   0.390946      -0.914786      -2.497212E-07  -0.304547       7.293448E-08
  ao   10  -1.883713E-08   0.131890       4.058867E-08  -0.751737      -0.315599      -4.691873E-08   0.391621       2.326543E-08
  ao   11   0.838966       0.896979       -1.44965        1.06616      -0.878290       0.573371      -0.285156      -0.388478    
  ao   12   -1.36379      -0.891512        1.18520       -1.30300       0.777538       -1.36531       -1.15852       0.364680    
  ao   13  -0.182870      -0.225954      -5.095442E-02   0.121069       0.177618      -0.445220      -0.404238       0.339883    
  ao   14  -0.182352      -1.110748E-02   0.124545      -0.596737      -0.218529      -8.269787E-02  -4.907139E-02  -0.746386    
  ao   15  -0.838965       0.896980        1.44965        1.06616      -0.878290      -0.573371      -0.285156       0.388478    
  ao   16    1.36379      -0.891514       -1.18520       -1.30300       0.777538        1.36531       -1.15852      -0.364680    
  ao   17   0.182870      -0.225954       5.095442E-02   0.121069       0.177618       0.445220      -0.404238      -0.339883    
  ao   18  -0.182353       1.110767E-02   0.124545       0.596737       0.218529      -8.269781E-02   4.907121E-02  -0.746386    

               no   17        no   18

      occ   1.330242E-04   1.121284E-04

  ao    1  -1.305275E-07   0.173284    
  ao    2  -2.934480E-07   0.289525    
  ao    3   2.211942E-06   -3.41438    
  ao    4   5.505615E-07  -0.966801    
  ao    5   9.565930E-07   -1.46207    
  ao    6   0.540976       3.569701E-07
  ao    7   0.975765       5.131518E-07
  ao    8    1.03371       6.082219E-07
  ao    9  -4.518919E-07   0.704602    
  ao   10   2.714654E-07  -0.457010    
  ao   11  -0.204917       0.347054    
  ao   12   -1.72076        1.80209    
  ao   13   0.762915      -0.743715    
  ao   14   0.435477      -0.449806    
  ao   15   0.204917       0.347055    
  ao   16    1.72076        1.80210    
  ao   17  -0.762914      -0.743716    
  ao   18   0.435477       0.449807    


*****   symmetry block SYM2   *****

               no    1        no    2        no    3        no    4        no    5        no    6

      occ   8.598519E-02   4.280974E-03   2.648373E-03   7.381740E-04   5.835137E-04   2.393024E-04

  mo    1   0.986495       0.124471       4.021465E-09   3.956864E-02  -2.968699E-08  -9.883504E-02
  mo    2  -8.277190E-02  -3.330919E-02   2.496183E-08   0.842868      -2.904636E-07  -0.530672    
  mo    3  -0.136069       0.974411      -1.262053E-07  -7.663686E-02  -8.839170E-09  -0.161661    
  mo    4  -1.977306E-08   1.179068E-07   0.972270      -9.132152E-08  -0.233860       2.437422E-08
  mo    5   1.840121E-09   4.282340E-08   0.233860       2.642533E-07   0.972270      -1.044329E-07
  mo    6   3.822485E-02   0.184173      -7.304128E-09   0.531163      -6.205679E-08   0.826126    

               no    1        no    2        no    3        no    4        no    5        no    6

      occ   8.598519E-02   4.280974E-03   2.648373E-03   7.381740E-04   5.835137E-04   2.393024E-04

  ao    1   -1.12589      -0.114785       2.814400E-08    1.33538      -4.189830E-07  -0.516868    
  ao    2   0.182610       0.101655      -4.847874E-08   -1.47832       5.207169E-07    1.04871    
  ao    3   1.992383E-08  -9.110059E-08  -0.823378       2.017863E-07   0.641814      -6.693206E-08
  ao    4   0.173110      -0.749826       1.053813E-07   0.406024      -3.267373E-08   0.686513    
  ao    5  -2.290663E-02  -0.286162      -0.274743      -0.385641      -0.706586      -0.630751    
  ao    6  -2.290663E-02  -0.286162       0.274743      -0.385641       0.706586      -0.630751    


 total number of electrons =    7.0000000000

 test slabel:                        2
  a'  a" 


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!


                        a'  partial gross atomic populations
   ao class       1a'        2a'        3a'        4a'        5a'        6a' 
    B1_ s       1.999928   0.381937   1.379467   0.000000   0.000000   0.000558
    B1_ p       0.000004   0.355474   0.477685   0.350877   0.024963   0.014849
    B1_ d      -0.000001   0.014267   0.011613   0.024844   0.004746   0.000624
    H1_ s       0.000086   0.586591  -0.008751   0.308046   0.006090   0.005032
    H1_ p      -0.000052   0.010351   0.003201   0.003598   0.000130   0.000569
    H2_ s       0.000086   0.586591  -0.008751   0.308046   0.006090   0.005032
    H2_ p      -0.000052   0.010351   0.003201   0.003598   0.000130   0.000569

   ao class       7a'        8a'        9a'       10a'       11a'       12a' 
    B1_ s       0.005523   0.000329   0.000000   0.000835   0.000000  -0.000041
    B1_ p       0.002119   0.000756   0.001154   0.000246   0.000035   0.000055
    B1_ d       0.000497   0.004024   0.000227   0.000895   0.002138   0.000309
    H1_ s       0.000557   0.000012   0.001311   0.001002   0.000436   0.000143
    H1_ p       0.000259   0.001002   0.000485   0.000332   0.000076   0.000321
    H2_ s       0.000557   0.000012   0.001311   0.001002   0.000436   0.000143
    H2_ p       0.000259   0.001002   0.000485   0.000332   0.000076   0.000321

   ao class      13a'       14a'       15a'       16a'       17a'       18a' 
    B1_ s       0.000010   0.000000   0.000174   0.000000   0.000000  -0.000002
    B1_ p       0.000005   0.000532   0.000320   0.000019   0.000013   0.000006
    B1_ d       0.000690  -0.000005   0.000049   0.000012   0.000016   0.000009
    H1_ s       0.000056   0.000019  -0.000047   0.000004   0.000015   0.000017
    H1_ p       0.000051   0.000119   0.000074   0.000165   0.000037   0.000033
    H2_ s       0.000056   0.000019  -0.000047   0.000004   0.000015   0.000017
    H2_ p       0.000051   0.000119   0.000074   0.000165   0.000037   0.000033

                        a"  partial gross atomic populations
   ao class       1a"        2a"        3a"        4a"        5a"        6a" 
    B1_ p       0.083031   0.000023   0.000000   0.000510   0.000000   0.000081
    B1_ d       0.002371   0.002960   0.002032   0.000052   0.000136   0.000050
    H1_ p       0.000291   0.000649   0.000308   0.000088   0.000224   0.000054
    H2_ p       0.000291   0.000649   0.000308   0.000088   0.000224   0.000054


                        gross atomic populations
     ao           B1_        H1_        H2_
      s         3.768717   0.900618   0.900618
      p         1.312758   0.022366   0.022366
      d         0.072555   0.000000   0.000000
    total       5.154031   0.922984   0.922984


 Total number of electrons:    7.00000000

========================GLOBAL TIMING PER NODE========================
   process    vectrn  integral   segment    diagon     dmain    davidc   finalvw   driver  
--------------------------------------------------------------------------------
   #   1         0.0       0.0       0.0       3.1       3.1       0.0       0.0       3.6
--------------------------------------------------------------------------------
       1         0.0       0.0       0.0       3.1       3.1       0.0       0.0       3.6
