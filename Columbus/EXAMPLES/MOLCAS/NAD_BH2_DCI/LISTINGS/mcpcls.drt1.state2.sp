

     ******************************************
     **    PROGRAM:              MCPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 original author: Daniel Robertson, FSU
 later revisions: Ron Shepard, ANL;
                  Michal Dallos, University Vienna



 This Version of Program mcpc is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



   ******  File header section  ******

 Headers form the restart file:
    SEWARD INTEGRALS                                                                
     title                                                                          
     title                                                                          


   ******  DRT info section  ******

 Informations for the DRT no.  1
 Header form the DRT file: 
     title                                                                          
 Molecular symmetry group:   sym1
 Total number of electrons:    7
 Spin multiplicity:            2
 Number of active orbitals:    6
 Number of active electrons:   5
 Total number of CSFs:       115

   ***  Informations from the DRT number:   1


 Symmetry orbital summary:
 Symm.blocks:         1     2
 Symm.labels:         a'    a" 

 List of doubly occupied orbitals:
  1 a' 

 List of active orbitals:
  2 a'   3 a'   4 a'   5 a'   6 a'   1 a" 

 Informations for the DRT no.  2
 Header form the DRT file: 
     title                                                                          
 Molecular symmetry group:    a" 
 Total number of electrons:    7
 Spin multiplicity:            2
 Number of active orbitals:    6
 Number of active electrons:   5
 Total number of CSFs:        95

   ***  Informations from the DRT number:   2


 Symmetry orbital summary:
 Symm.blocks:         1     2
 Symm.labels:         a'    a" 

 List of doubly occupied orbitals:
  1 a' 

 List of active orbitals:
  2 a'   3 a'   4 a'   5 a'   6 a'   1 a" 


   ******  MCSCF convergence information:  ******

 MCSCF convergence criteria were satisfied.

 mcscf energy=   -25.6530063490    nuclear repulsion=     4.1965378929
 demc=             0.0000000000    wnorm=                 0.0000000151
 knorm=            0.0000000028    apxde=                 0.0000000000


 MCSCF calculation performmed for   2 symmetries.

 State averaging:
 No,  ssym, navst, wavst
  1    a'     2   0.3333 0.3333
  2    a"     1   0.3333

 Input the DRT No of interest: [  1]:
In the DRT No.: 1 there are  2 states.

 Which one to take? [  1]:
 The CSFs for the state No  2 of the symmetry  a'  will be printed
 according to the following print options :

 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 0) end.

 input menu number [  0]: csfs will be printed based on coefficient magnitudes.

 input the coefficient threshold (end with 0.) [ 0.0000]:
 List of active orbitals:
  2 a'   3 a'   4 a'   5 a'   6 a'   1 a" 

   csf       coeff       coeff**2    step(*)
  -----  ------------  ------------  ------------
      4  0.9480928024  0.8988799621  313000
     13 -0.2040071214  0.0416189056  310003
     26 -0.1065197190  0.0113464505  132100
      6 -0.0938316097  0.0088043710  312010
      9 -0.0773169550  0.0059779115  310300
     52 -0.0712727453  0.0050798042  112003
     60  0.0614628794  0.0037776855  103120
     36 -0.0597605640  0.0035713250  123010
     18 -0.0526970508  0.0027769792  301120
     14 -0.0500284193  0.0025028427  303100
     28 -0.0489731835  0.0023983727  131200
     90  0.0481573207  0.0023191275  013030
      8 -0.0358898802  0.0012880835  311020
     87  0.0353964774  0.0012529106  013300
     91  0.0327503540  0.0010725857  013003
     76 -0.0318907506  0.0010170200  033100
     12 -0.0315603426  0.0009960552  310030
     41  0.0308643055  0.0009526054  121003
     47 -0.0269573880  0.0007267008  113020
      2  0.0248282525  0.0006164421  330100
     51  0.0201717205  0.0004068983  112030
     59  0.0179691415  0.0003228900  103210
     74 -0.0176982789  0.0003132291  100123
     45 -0.0162337096  0.0002635333  120013
     64  0.0161126281  0.0002596168  102130
     85 -0.0159885108  0.0002556325  030103
    104 -0.0139709591  0.0001951877  010033
    101 -0.0118179257  0.0001396634  010303
     40 -0.0116091521  0.0001347724  121030
    106 -0.0102146200  0.0001043385  003130
     32 -0.0101771449  0.0001035743  130120
     31 -0.0090153093  0.0000812758  130210
     80  0.0085167857  0.0000725356  031120
     92  0.0070594726  0.0000498362  012310
     95 -0.0065033631  0.0000422937  012013
    100 -0.0064549798  0.0000416668  010330
     22 -0.0060090444  0.0000361086  300130
     42  0.0053080843  0.0000281758  120310
     54  0.0052386080  0.0000274430  110320
     17  0.0049409550  0.0000244130  301210
    112 -0.0046352734  0.0000214858  001123
     57 -0.0037943459  0.0000143971  110023
     84  0.0037761189  0.0000142591  030130
     68  0.0037138804  0.0000137929  101230
    107 -0.0031631535  0.0000100055  003103
    115 -0.0030751671  0.0000094567  000133
     96  0.0028668602  0.0000082189  011320
     73 -0.0026704929  0.0000071315  100213
     65  0.0025290417  0.0000063961  102103
     37 -0.0024823054  0.0000061618  121300
     48  0.0021442058  0.0000045976  112300
     79 -0.0015433248  0.0000023819  031210

 input the coefficient threshold (end with 0.) [ 0.0000]:
 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 0) end.

 input menu number [  0]: