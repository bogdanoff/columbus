
 program "mcdrt 4.1 a3"

 distinct row table specification and csf
 selection for mcscf wavefunction optimization.

 programmed by: ron shepard

 version date: 17-oct-91


 This Version of Program mcdrt is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              MCDRT       **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 expanded keystroke file:
 mcdrtky                                                                         

 input the spin multiplicity [  0]: spin multiplicity:    2    doublet 
 input the total number of electrons [  0]: nelt:      7
 input the number of irreps (1-8) [  0]: nsym:      2
 enter symmetry labels:(y,[n]) enter 2 labels (a4):
 enter symmetry label, default=   1
 enter symmetry label, default=   2
 input the molecular spatial symmetry (irrep 1:nsym) [  0]: spatial symmetry is irrep number:      1

 input the list of doubly-occupied orbitals (sym(i),rmo(i),i=1,ndot):
 number of doubly-occupied orbitals:      1
 number of inactive electrons:      2
 number of active electrons:      5
 level(*)        1
 symd(*)         1
 slabel(*)     a' 
 doub(*)         1

 input the active orbitals (sym(i),rmo(i),i=1,nact):
 nact:      6
 level(*)        1   2   3   4   5   6
 syml(*)         1   1   1   1   1   2
 slabel(*)     a'  a'  a'  a'  a'  a" 
 modrt(*)        2   3   4   5   6   1
 input the minimum cumulative occupation for each active level:
  a'  a'  a'  a'  a'  a" 
    2   3   4   5   6   1
 input the maximum cumulative occupation for each active level:
  a'  a'  a'  a'  a'  a" 
    2   3   4   5   6   1
 slabel(*)     a'  a'  a'  a'  a'  a" 
 modrt(*)        2   3   4   5   6   1
 occmin(*)       0   0   0   0   0   5
 occmax(*)       5   5   5   5   5   5
 input the minimum b value for each active level:
  a'  a'  a'  a'  a'  a" 
    2   3   4   5   6   1
 input the maximum b value for each active level:
  a'  a'  a'  a'  a'  a" 
    2   3   4   5   6   1
 slabel(*)     a'  a'  a'  a'  a'  a" 
 modrt(*)        2   3   4   5   6   1
 bmin(*)         0   0   0   0   0   0
 bmax(*)         5   5   5   5   5   5
 input the step masks for each active level:
 modrt:smask=
   2:1111   3:1111   4:1111   5:1111   6:1111   1:1111
 input the number of vertices to be deleted [  0]: number of vertices to be removed (a priori):      0
 number of rows in the drt:     32
 are any arcs to be manually removed?(y,[n])
 nwalk=     115
 input the range of drt levels to print (l1,l2):
 levprt(*)       0   6

 level  0 through level  6 of the drt:

 row lev a b syml lab rmo  l0  l1  l2  l3 isym xbar   y0    y1    y2    xp     z

  32   6 2 1   2 a"    1    0   0   0   0   1     1     0     0     0   115     0
                                            2     0     0     0     0    95     0
 ........................................

  28   5 2 1   1 a'    6   32   0   0   0   1     1     0     0     0    75     0
                                            2     0     0     0     0     0     0

  29   5 2 0   1 a'    6    0  32   0   0   1     0     0     0     0    50     0
                                            2     1     1     0     0     0    75

  30   5 1 2   1 a'    6    0   0  32   0   1     0     0     0     0    45     0
                                            2     1     1     1     0     0    75

  31   5 1 1   1 a'    6    0   0   0  32   1     1     1     1     1    40    75
                                            2     0     0     0     0     0     0
 ........................................

  20   4 2 1   1 a'    5   28   0   0   0   1     1     0     0     0    20     0
                                            2     0     0     0     0     0     0

  21   4 2 0   1 a'    5   29  28   0   0   1     1     1     0     0    20    20
                                            2     1     0     0     0     0    75

  22   4 1 2   1 a'    5   30   0  28   0   1     1     1     1     0    15    40
                                            2     1     0     0     0     0    75

  23   4 1 1   1 a'    5   31  30  29  28   1     2     1     1     1    20    55
                                            2     2     2     1     0     0    75

  24   4 1 0   1 a'    5    0  31   0  29   1     1     1     0     0    10    95
                                            2     1     1     1     1     0    75

  25   4 0 3   1 a'    5    0   0  30   0   1     0     0     0     0     4     0
                                            2     1     1     1     0     0    75

  26   4 0 2   1 a'    5    0   0  31  30   1     1     1     1     0     6   105
                                            2     1     1     1     1     0    75

  27   4 0 1   1 a'    5    0   0   0  31   1     1     1     1     1     4   111
                                            2     0     0     0     0     0     0
 ........................................

  11   3 2 1   1 a'    4   20   0   0   0   1     1     0     0     0     3     0
                                            2     0     0     0     0     0     0

  12   3 2 0   1 a'    4   21  20   0   0   1     2     1     0     0     6     3
                                            2     1     0     0     0     0    75

  13   3 1 2   1 a'    4   22   0  20   0   1     2     1     1     0     3     9
                                            2     1     0     0     0     0    75

  14   3 1 1   1 a'    4   23  22  21  20   1     5     3     2     1     8    12
                                            2     4     2     1     0     0    75

  15   3 1 0   1 a'    4   24  23   0  21   1     4     3     1     1     6    34
                                            2     4     3     1     1     0    75

  16   3 0 3   1 a'    4   25   0  22   0   1     1     1     1     0     1    51
                                            2     2     1     1     0     0    75

  17   3 0 2   1 a'    4   26  25  23  22   1     4     3     3     1     3    52
                                            2     5     4     3     1     0    75

  18   3 0 1   1 a'    4   27  26  24  23   1     5     4     3     2     3    72
                                            2     4     4     3     2     0    75

  19   3 0 0   1 a'    4    0  27   0  24   1     2     2     1     1     1   104
                                            2     1     1     1     1     0    75
 ........................................

   5   2 2 0   1 a'    3   12  11   0   0   1     3     1     0     0     1     0
                                            2     1     0     0     0     0    75

   6   2 1 1   1 a'    3   14  13  12  11   1    10     5     3     1     2     1
                                            2     6     2     1     0     0    75

   7   2 1 0   1 a'    3   15  14   0  12   1    11     7     2     2     3     6
                                            2     9     5     1     1     0    75

   8   2 0 2   1 a'    3   17  16  14  13   1    12     8     7     2     1    11
                                            2    12     7     5     1     0    75

   9   2 0 1   1 a'    3   18  17  15  14   1    18    13     9     5     2    18
                                            2    17    13     8     4     0    75

  10   2 0 0   1 a'    3   19  18   0  15   1    11     9     4     4     1    39
                                            2     9     8     4     4     0    75
 ........................................

   2   1 1 0   1 a'    2    7   6   0   5   1    24    13     3     3     1     0
                                            2    16     7     1     1     0    75

   3   1 0 1   1 a'    2    9   8   7   6   1    51    33    21    10     1     2
                                            2    44    27    15     6     0    75

   4   1 0 0   1 a'    2   10   9   0   7   1    40    29    11    11     1     8
                                            2    35    26     9     9     0    75
 ........................................

   1   0 0 0   0       0    4   3   0   2   1   115    75    24    24     1     0
                                            2    95    60    16    16     0    75
 ........................................

 initial csf selection step:
 total number of walks in the drt, nwalk=     115
 keep all of these walks?(y,[n]) individual walks will be generated from the drt.
 apply orbital-group occupation restrictions?(y,[n]) apply reference occupation restrictions?(y,[n]) manually select individual walks?(y,[n])
 step-vector based csf selection complete.
      115 csfs selected from     115 total walks.

 beginning step-vector based csf selection.
 enter [step_vector/disposition] pairs:

 enter the active orbital step vector, (-1/ to end):

 step-vector based csf selection complete.
      115 csfs selected from     115 total walks.

 beginning numerical walk selection:
 enter positive walk numbers to add walks,
 negative walk numbers to delete walks, and zero to end.

 input walk number (0 to end) [  0]:
 final csf selection complete.
      115 csfs selected from     115 total walks.
  drt construction and csf selection complete.

 input a title card, default=mdrt2_title
  title                                                                          
 input a drt file name, default=mcdrtfl
 drt and indexing arrays written to file:
 mcdrtfl                                                                         

 write the drt file?([y],n) include step(*) vectors?([y],n) drt file is being written...


   List of selected configurations (step vectors)


   CSF#     1    3 3 1 0 0 0
   CSF#     2    3 3 0 1 0 0
   CSF#     3    3 3 0 0 1 0
   CSF#     4    3 1 3 0 0 0
   CSF#     5    3 1 2 1 0 0
   CSF#     6    3 1 2 0 1 0
   CSF#     7    3 1 1 2 0 0
   CSF#     8    3 1 1 0 2 0
   CSF#     9    3 1 0 3 0 0
   CSF#    10    3 1 0 2 1 0
   CSF#    11    3 1 0 1 2 0
   CSF#    12    3 1 0 0 3 0
   CSF#    13    3 1 0 0 0 3
   CSF#    14    3 0 3 1 0 0
   CSF#    15    3 0 3 0 1 0
   CSF#    16    3 0 1 3 0 0
   CSF#    17    3 0 1 2 1 0
   CSF#    18    3 0 1 1 2 0
   CSF#    19    3 0 1 0 3 0
   CSF#    20    3 0 1 0 0 3
   CSF#    21    3 0 0 3 1 0
   CSF#    22    3 0 0 1 3 0
   CSF#    23    3 0 0 1 0 3
   CSF#    24    3 0 0 0 1 3
   CSF#    25    1 3 3 0 0 0
   CSF#    26    1 3 2 1 0 0
   CSF#    27    1 3 2 0 1 0
   CSF#    28    1 3 1 2 0 0
   CSF#    29    1 3 1 0 2 0
   CSF#    30    1 3 0 3 0 0
   CSF#    31    1 3 0 2 1 0
   CSF#    32    1 3 0 1 2 0
   CSF#    33    1 3 0 0 3 0
   CSF#    34    1 3 0 0 0 3
   CSF#    35    1 2 3 1 0 0
   CSF#    36    1 2 3 0 1 0
   CSF#    37    1 2 1 3 0 0
   CSF#    38    1 2 1 2 1 0
   CSF#    39    1 2 1 1 2 0
   CSF#    40    1 2 1 0 3 0
   CSF#    41    1 2 1 0 0 3
   CSF#    42    1 2 0 3 1 0
   CSF#    43    1 2 0 1 3 0
   CSF#    44    1 2 0 1 0 3
   CSF#    45    1 2 0 0 1 3
   CSF#    46    1 1 3 2 0 0
   CSF#    47    1 1 3 0 2 0
   CSF#    48    1 1 2 3 0 0
   CSF#    49    1 1 2 2 1 0
   CSF#    50    1 1 2 1 2 0
   CSF#    51    1 1 2 0 3 0
   CSF#    52    1 1 2 0 0 3
   CSF#    53    1 1 1 2 2 0
   CSF#    54    1 1 0 3 2 0
   CSF#    55    1 1 0 2 3 0
   CSF#    56    1 1 0 2 0 3
   CSF#    57    1 1 0 0 2 3
   CSF#    58    1 0 3 3 0 0
   CSF#    59    1 0 3 2 1 0
   CSF#    60    1 0 3 1 2 0
   CSF#    61    1 0 3 0 3 0
   CSF#    62    1 0 3 0 0 3
   CSF#    63    1 0 2 3 1 0
   CSF#    64    1 0 2 1 3 0
   CSF#    65    1 0 2 1 0 3
   CSF#    66    1 0 2 0 1 3
   CSF#    67    1 0 1 3 2 0
   CSF#    68    1 0 1 2 3 0
   CSF#    69    1 0 1 2 0 3
   CSF#    70    1 0 1 0 2 3
   CSF#    71    1 0 0 3 3 0
   CSF#    72    1 0 0 3 0 3
   CSF#    73    1 0 0 2 1 3
   CSF#    74    1 0 0 1 2 3
   CSF#    75    1 0 0 0 3 3
   CSF#    76    0 3 3 1 0 0
   CSF#    77    0 3 3 0 1 0
   CSF#    78    0 3 1 3 0 0
   CSF#    79    0 3 1 2 1 0
   CSF#    80    0 3 1 1 2 0
   CSF#    81    0 3 1 0 3 0
   CSF#    82    0 3 1 0 0 3
   CSF#    83    0 3 0 3 1 0
   CSF#    84    0 3 0 1 3 0
   CSF#    85    0 3 0 1 0 3
   CSF#    86    0 3 0 0 1 3
   CSF#    87    0 1 3 3 0 0
   CSF#    88    0 1 3 2 1 0
   CSF#    89    0 1 3 1 2 0
   CSF#    90    0 1 3 0 3 0
   CSF#    91    0 1 3 0 0 3
   CSF#    92    0 1 2 3 1 0
   CSF#    93    0 1 2 1 3 0
   CSF#    94    0 1 2 1 0 3
   CSF#    95    0 1 2 0 1 3
   CSF#    96    0 1 1 3 2 0
   CSF#    97    0 1 1 2 3 0
   CSF#    98    0 1 1 2 0 3
   CSF#    99    0 1 1 0 2 3
   CSF#   100    0 1 0 3 3 0
   CSF#   101    0 1 0 3 0 3
   CSF#   102    0 1 0 2 1 3
   CSF#   103    0 1 0 1 2 3
   CSF#   104    0 1 0 0 3 3
   CSF#   105    0 0 3 3 1 0
   CSF#   106    0 0 3 1 3 0
   CSF#   107    0 0 3 1 0 3
   CSF#   108    0 0 3 0 1 3
   CSF#   109    0 0 1 3 3 0
   CSF#   110    0 0 1 3 0 3
   CSF#   111    0 0 1 2 1 3
   CSF#   112    0 0 1 1 2 3
   CSF#   113    0 0 1 0 3 3
   CSF#   114    0 0 0 3 1 3
   CSF#   115    0 0 0 1 3 3
