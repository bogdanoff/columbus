License is going to expire in 22 days on Saturday November 01st, 2008
Everything is fine. Going to run MOLCAS!
                                                                                                   
                                              ^^^^^            M O L C A S                         
                                             ^^^^^^^           version 7.3 patchlevel 320          
                               ^^^^^         ^^^^^^^                                               
                              ^^^^^^^        ^^^ ^^^                                               
                              ^^^^^^^       ^^^^ ^^^                                               
                              ^^^ ^^^       ^^^^ ^^^                                               
                              ^^^ ^^^^      ^^^  ^^^                                               
                              ^^^  ^^ ^^^^^  ^^  ^^^^                                              
                             ^^^^      ^^^^   ^   ^^^                                              
                             ^   ^^^   ^^^^   ^^^^  ^                                              
                            ^   ^^^^    ^^    ^^^^   ^                                             
                        ^^^^^   ^^^^^   ^^   ^^^^^   ^^^^^                                         
                     ^^^^^^^^   ^^^^^        ^^^^^    ^^^^^^^                                      
                 ^^^^^^^^^^^    ^^^^^^      ^^^^^^^   ^^^^^^^^^^^                                  
               ^^^^^^^^^^^^^   ^^^^^^^^^^^^^^^^^^^^   ^^^^^^^^^^^^^                                
               ^^^^^^^^^^^^^   ^^^^             ^^^   ^^^      ^^^^                                
               ^^^^^^^^^^^^^                          ^^^      ^^^^                                
               ^^^^^^^^^^^^^       ^^^^^^^^^^^^        ^^      ^^^^                                
               ^^^^^^^^^^^^      ^^^^^^^^^^^^^^^^      ^^      ^^^^                                
               ^^^^^^^^^^^^    ^^^^^^^^^^^^^^^^^^^^    ^^      ^^^^                                
               ^^^^^^^^^^^^    ^^^^^^^^^^^^^^^^^^^^    ^^      ^^^^    ^^^^^     ^^^    ^^^^^^     
               ^^^^^^^^^^^^   ^^^^^^^^^^^^^^^^^^^^^^   ^^      ^^^^   ^^^^^^^    ^^^   ^^^  ^^^    
               ^^^^^^^^^^^    ^^^^^^^^^^^^^^^^^^^^^^   ^       ^^^^  ^^^    ^^   ^^^   ^^    ^^    
               ^^^^^^^^^^^     ^^^^^^^^^^^^^^^^^^^^            ^^^^  ^^         ^^ ^^  ^^^^^       
               ^^^^^^^^^^      ^^^^^^^^^^^^^^^^^^^^        ^   ^^^^  ^^         ^^ ^^   ^^^^^^     
               ^^^^^^^^          ^^^^^^^^^^^^^^^^          ^   ^^^^  ^^        ^^^^^^^     ^^^^    
               ^^^^^^      ^^^     ^^^^^^^^^^^^     ^      ^   ^^^^  ^^^   ^^^ ^^^^^^^ ^^    ^^    
                         ^^^^^^                    ^^      ^   ^^^^   ^^^^^^^  ^^   ^^ ^^^  ^^^    
                      ^^^^^^^^^^^^             ^^^^^^      ^           ^^^^^  ^^     ^^ ^^^^^^     
               ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                                
                 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                                  
                     ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                                      
                        ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                                         
                            ^^^^^^^^^^^^^^^^^^^^^^^^^^                                             
                               ^^^^^^^^^^^^^^^^^^^^                                                
                                   ^^^^^^^^^^^^                                                    
                                       ^^^^^^                                                      

                           Copyright, all rights, reserved:                                        
                         Permission is hereby granted to use                                       
                but not to reproduce or distribute any part of this                                
             program. The use is restricted to research purposes only.                             
                            Lund University Sweden, 2008.                                          
                                                                                                   
 For the author list and the recommended citation consult section 1.5 of the MOLCAS user's guide.  



()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()
                                  MOLCAS executing module SEWARD with 100 MB of memory                                  
                                              at 13:32:53 Fri Oct 10 2008                                               
()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()


               SEWARD will generate:
                  Multipole Moment integrals up to order  2
                  Kinetic Energy integrals
                  Nuclear Attraction integrals (point charge)
                  One-Electron Hamiltonian integrals
                  Orbital angular momentum around ( 0.0000  0.0000  0.0000 )
                  Two-Electron Repulsion integrals

 Title:
                                             BH2 Cs                                     


                   Integrals are discarded if absolute value <: 0.10E-12
                   Integral cutoff threshold is set to       <: 0.10E-11

                    --- Group Generators ---
                    Reflection in the xy-plane  


                    Character Table for Cs 

                             E   s(xy)
                    a'       1     1  x, y, xy, Rz
                    a"       1    -1  z, xz, Ry, yz, Rx, I

                    Unitary symmetry adaptation



 Basis set label:B.CC-PVDZ.DUNNING.9S4P1D.3S2P1D......                                           

 Valence basis set:
 ==================
 Associated Effective Charge  5.000000 au
 Associated Actual Charge     5.000000 au
 Nuclear Model: Point charge
  Shell  nPrim  nBasis  Cartesian Spherical Contaminant
    s       9       3        X                  
    p       4       2        X                  
    d       1       1                 X         


 Basis set label:H.CC-PVDZ.DUNNING.4S1P.2S1P......                                               

 Valence basis set:
 ==================
 Associated Effective Charge  1.000000 au
 Associated Actual Charge     1.000000 au
 Nuclear Model: Point charge
  Shell  nPrim  nBasis  Cartesian Spherical Contaminant
    s       4       2        X                  
    p       1       1        X                  

                    ************************************************ 
                    **** Cartesian Coordinates / Bohr, Angstrom **** 
                    ************************************************ 

     Center  Label                x              y              z                     x              y              z
        1      B1            -0.336394       0.000000       0.000000             -0.178012       0.000000       0.000000
        2      H1             1.837358       1.426590       0.000000              0.972288       0.754919       0.000000
        3      H2             1.837358      -1.426590       0.000000              0.972288      -0.754919       0.000000

                    *************************************** 
                    *    InterNuclear Distances / Bohr    * 
                    --------------------------------------- 

             1 B1            2 H1            3 H2  
    1 B1     0.000000
    2 H1     2.600069        0.000000
    3 H2     2.600069        2.853180        0.000000

                    ******************************************* 
                    *    InterNuclear Distances / Angstrom    * 
                    ------------------------------------------- 

             1 B1            2 H1            3 H2  
    1 B1     0.000000
    2 H1     1.375897        0.000000
    3 H2     1.375897        1.509838        0.000000

                    ************************************** 
                    *    Valence Bond Angles / Degree    * 
                    -------------------------------------- 
                          Atom centers          Phi       
                      2 H1     1 B1     3 H2      66.55
                      1 B1     2 H1     3 H2      56.72
                      1 B1     3 H2     2 H1      56.72

  Basis set specifications :
  Symmetry species            a'  a" 
  Basis functions              18   6




                    Nuclear Potential Energy                      4.19653789 au
--- Stop Module: seward at Fri Oct 10 13:32:53 2008 /rc=0 ---

     Happy landing!

