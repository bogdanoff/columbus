

     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 This program allows the csf mixing coefficient and orbital expansion coefficient
 optimization using the graphical unitary group approach and the exponential
 operator mcscf method.
 references:  r. shepard and j. simons,                    ' int. j. quantum chem. symp. 14, 211 (1980).
              r. shepard, i. shavitt, and j. simons, j. chem. phys. 76, 543 (1982).
              r. shepard in "ab initio methods in quantum chemistry ii" advances in chemical
                  physics 69, edited by k. p. lawley (wiley, new york, 1987) pp. 63-200.
 Original autor: Ron Shepard, ANL
 Later revisions: Michal Dallos, University Vienna

 This Version of Program MCSCF is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.4.0.2     **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 Workspace allocation information:
        13107200 of real*8 words (  100.00 MB) of work space has been allocated.

 user input information:

 ======== echo of the mcscf input ========
 ------------------------------------------------------------------------
  &input
   niter=200,
   nmiter=30,
   nciitr=30,
   tol(3)=1.e-6,
   tol(2)=1.e-6,
   tol(1)=1.e-8,
   NSTATE=0,
   npath=1,3,9,10,13,17,19,21,11, 2,23,26
   ncoupl=5,
   FCIORB=
   1,2,20,1,3,20,1,4,20,1,5,20,1,6,20,2,1,20
   NAVST(1) = 2,
   WAVST(1,1)=1 ,
   WAVST(1,2)=1 ,
   NAVST(2) = 1,
   WAVST(2,1)=1 ,
 /&end
 ------------------------------------------------------------------------


 ***  Integral file informations  ***



 Integral file header information:
 SEWARD INTEGRALS                                                                
 total ao core energy =    4.196537893


   ******  Basis set information:  ******

 Number of irreps:                  2
 Total number of basis functions:  24

 irrep no.              1    2
 irrep label           a'   a" 
 no. of bas.fcions.    18    6
 map-vector 1 , imtype=                        3
  1   1   1   1   1   1   1   1   1   1   2   2   2   2   3   3   3   3   1   1
  1   1   2   3
 map-vector 2 , imtype=                        4
  1   1   1   2   2   2   2   4   4   4   1   1   2   2   1   1   2   2   2   2
  4   4   2   2
 using sifcfg values:                     4096                     3272 
                     4096                     2730
 warning: resetting tol(6) due to possible inconsistency.


 ***  MCSCF optimization procedure parmeters:  ***


 maximum number of mcscf iterations:        niter=   200

 maximum number of psci micro-iterations:   nmiter=   30
 maximum r,s subspace dimension allowed:    nvrsmx=   30

AO integrals are read from Seward integral files 
 ... RUNFILE, ONEINT , ORDINT 

 tol(1)=  1.0000E-08. . . . delta-emc convergence criterion.
 tol(2)=  1.0000E-06. . . . wnorm convergence criterion.
 tol(3)=  1.0000E-06. . . . knorm convergence criterion.
 tol(4)=  1.0000E-08. . . . apxde convergence criterion.
 tol(5)=  1.0000E-04. . . . small diagonal matrix element tolerance.
 tol(6)=  1.2500E-07. . . . minimum ci-psci residual norm.
 tol(7)=  1.0000E-05. . . . maximum ci-psci residual norm.
 tol(8)=  1.0000E+00. . . . maximum abs(k(xy)) allowed.
 tol(9)=  1.0000E+00. . . . wnorm coupling tolerance.
 tol(10)= 0.0000E+00. . . . maximum psci emergency shift parameter.
 tol(11)= 0.0000E+00. . . . minimum psci emergency shift parameter.
 tol(12)= 0.0000E+00. . . . increment of psci emergency shift parameter.


 *** State averaging informations: ***


 MCSCF calculation performed for  2 DRTs.

 DRT  first state   no.of aver.states   weights
  1   ground state          2             0.333 0.333
  2   ground state          1             0.333

 The number of hmc(*) eigenvalues and eigenvectors calculated each iteration per symmetry:
 Symm.   no.of eigenv.(=ncol)
  a'         3
  a"         2

 Orbitals included in invariant subspaces:
   symmetry   orbital   mask
       1       2(  2)    20
       1       3(  3)    20
       1       4(  4)    20
       1       5(  5)    20
       1       6(  6)    20
       2       1( 19)    20

 npath(*) options:
  2:  orbital-state coupling terms will be included beginning on iteration ncoupl=  5
  3:  print intermediate timing information.
  9:  suppress the drt listing.
 10:  suppress the hmc(*) eigenvector listing.
 11:  construct the hmc(*) matrix explicitly.
 13:  get initial orbitals from the formatted file, mocoef.
 17:  print the final natural orbitals and occupations.
 19:  transform the virtual orbitals to diagonalize qvv(*).
 21:  write out the one- and two- electron density for further use (files:mcd1fl, mcd2fl).
 23:  use the old integral transformation.
 26:  Read AO integrals in MOLCAS native format 


   ******  DRT info section  ******


 Informations for the DRT no.  1

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a' 
 Total number of electrons:    7
 Spin multiplicity:            2
 Number of active orbitals:    6
 Number of active electrons:   5
 Total number of CSFs:       115

 Informations for the DRT no.  2

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a" 
 Total number of electrons:    7
 Spin multiplicity:            2
 Number of active orbitals:    6
 Number of active electrons:   5
 Total number of CSFs:        95

 !timer: initialization                  user+sys=     0.000 walltime=     0.000

 faar:   0 active-active rotations allowed out of:  10 possible.


 Number of active-double rotations:         5
 Number of active-active rotations:         0
 Number of double-virtual rotations:       12
 Number of active-virtual rotations:       65

 Size of orbital-Hessian matrix B:                     3809
 Size of the orbital-state Hessian matrix C:          26650
 Total size of the state Hessian matrix M:            31125
 Size of HESSIAN-matrix for quadratic conv.:          61584



   ****** Integral transformation section ******


 number of blocks to be transformed in-core is   4
 number of blocks to be transformed out of core is    0

 in-core ao list requires    1 segments.
 out of core ao list requires    0 segments.
 each segment has length ****** working precision words.

               ao integral sort statistics
 length of records:      1610
 number of buckets:   1
 scratch file used is da1:
 amount of core needed for in-core arrays:  2767

 twoao_o processed      21669 two electron ao integrals.

      21669 ao integrals were written into   21 records

 srtinc_o read in      24414 integrals and wrote out      24414 integrals.

 Source of the initial MO coeficients:

 Input MO coefficient file: mocoef                                                      


               starting mcscf iteration...   1
 !timer:                                 user+sys=     0.010 walltime=     0.000

 orbital-state coupling will not be calculated this iteration.

 number of transformed integrals put on file:      8490
 number of records written:     4
 !timer: 2-e transformation              user+sys=     0.010 walltime=     0.000

 Size of orbital-Hessian matrix B:                     3809
 Total size of the state Hessian matrix M:            31125
 Total size of HESSIAN-matrix for linear con          34934

 !timer: mosrt1                          user+sys=     0.000 walltime=     0.000
 !timer: mosrt2                          user+sys=     0.000 walltime=     0.000
 !timer: mosort                          user+sys=     0.000 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000
 !timer: hmc(*) diagonalization          user+sys=     0.010 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy
    1*      -25.7042693742      -29.9008072671
    2       -25.6559723484      -29.8525102412
    3       -25.4338900402      -29.6304279330
 !timer: hmcvec                          user+sys=     0.010 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000
 !timer: hmc(*) diagonalization          user+sys=     0.010 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy
    1*      -25.5987773245      -29.7953152173
    2       -25.5937550811      -29.7902929740
 !timer: hmcvec                          user+sys=     0.010 walltime=     0.000
 !timer: rdft                            user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.000 walltime=     0.000
 !timer: mfad                            user+sys=     0.000 walltime=     0.000
 !timer: mqad                            user+sys=     0.000 walltime=     0.000
 !timer: mqva                            user+sys=     0.000 walltime=     0.000
 !timer: mfva                            user+sys=     0.000 walltime=     0.000
 !timer: mfvd                            user+sys=     0.000 walltime=     0.000
 !timer: bvaad                           user+sys=     0.000 walltime=     0.000
 !timer: bvaadf                          user+sys=     0.000 walltime=     0.000
 !timer: mqaa                            user+sys=     0.000 walltime=     0.000
 !timer: mfaa                            user+sys=     0.000 walltime=     0.000
 !timer: badad                           user+sys=     0.000 walltime=     0.000
 !timer: badadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvava                           user+sys=     0.000 walltime=     0.000
 !timer: bvavaf                          user+sys=     0.000 walltime=     0.000
 !timer: bvavd                           user+sys=     0.000 walltime=     0.000
 !timer: bvavdf                          user+sys=     0.000 walltime=     0.000
 !timer: bvdad                           user+sys=     0.000 walltime=     0.000
 !timer: bvdadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvdvd                           user+sys=     0.000 walltime=     0.000
 !timer: hbcon                           user+sys=     0.000 walltime=     0.000

  tol(10)=    0.000000000000000       eshsci=   1.1955138244805124E-008
 Total number of micro iterations:    2

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 1.00000000 pnorm= 0.0000E+00 rznorm= 1.1792E-08 rpnorm= 0.0000E+00 noldr=  2 nnewr=  2 nolds=  0 nnews=  0

 !timer: Direct Bxr time contribution    user+sys=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  user+sys=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    user+sys=     0.000 walltime=     0.000
 !timer: solvek total                    user+sys=     0.000 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
   -15.321852

 qvv(*) eigenvalues. symmetry block  1
     0.811757    0.857800    0.957074    1.114145    1.248810    1.694346    2.443428    2.462132    3.151879    4.036750
     4.418300    4.486704

 qvv(*) eigenvalues. symmetry block  2
     0.942491    1.693244    1.762825    3.431461    3.563663
 !timer: motran                          user+sys=     0.010 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 user+sys=     0.040 walltime=     0.000

 all mcscf convergence criteria are not satisfied.
 iter=    1 emc=  -25.6530063490 demc= 2.5653E+01 wnorm= 9.5641E-08 knorm= 1.7028E-07 apxde= 3.0926E-15    *not converged* 

               starting mcscf iteration...   2
 !timer:                                 user+sys=     0.050 walltime=     0.000

 orbital-state coupling will not be calculated this iteration.

 number of transformed integrals put on file:      8500
 number of records written:     4
 !timer: 2-e transformation              user+sys=     0.000 walltime=     0.000

 Size of orbital-Hessian matrix B:                     3809
 Total size of the state Hessian matrix M:            31125
 Total size of HESSIAN-matrix for linear con          34934

 !timer: mosrt1                          user+sys=     0.000 walltime=     0.000
 !timer: mosrt2                          user+sys=     0.000 walltime=     0.000
 !timer: mosort                          user+sys=     0.000 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.010 walltime=     0.000
 !timer: hmc(*) diagonalization          user+sys=     0.010 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy
    1*      -25.7042693745      -29.9008072674
    2       -25.6559723480      -29.8525102409
    3       -25.4338900393      -29.6304279322
 !timer: hmcvec                          user+sys=     0.020 walltime=     0.000
 !timer: hdiag(*) construction           user+sys=     0.000 walltime=     0.000
 !timer: hmcft                           user+sys=     0.000 walltime=     0.000
 !timer: hmc(*) diagonalization          user+sys=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy
    1*      -25.5987773245      -29.7953152173
    2       -25.5937550808      -29.7902929737
 !timer: hmcvec                          user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.000 walltime=     0.000
 !timer: rdft                            user+sys=     0.000 walltime=     0.000
 !timer: mfad                            user+sys=     0.000 walltime=     0.000
 !timer: mqad                            user+sys=     0.000 walltime=     0.000
 !timer: mqva                            user+sys=     0.000 walltime=     0.000
 !timer: mfva                            user+sys=     0.000 walltime=     0.000
 !timer: mfvd                            user+sys=     0.000 walltime=     0.000
 !timer: bvaad                           user+sys=     0.000 walltime=     0.000
 !timer: bvaadf                          user+sys=     0.000 walltime=     0.000
 !timer: mqaa                            user+sys=     0.000 walltime=     0.000
 !timer: mfaa                            user+sys=     0.000 walltime=     0.000
 !timer: badad                           user+sys=     0.000 walltime=     0.000
 !timer: badadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvava                           user+sys=     0.000 walltime=     0.000
 !timer: bvavaf                          user+sys=     0.000 walltime=     0.000
 !timer: bvavd                           user+sys=     0.000 walltime=     0.000
 !timer: bvavdf                          user+sys=     0.000 walltime=     0.000
 !timer: bvdad                           user+sys=     0.000 walltime=     0.000
 !timer: bvdadf                          user+sys=     0.000 walltime=     0.000
 !timer: bvdvd                           user+sys=     0.000 walltime=     0.000
 !timer: hbcon                           user+sys=     0.000 walltime=     0.000

  tol(10)=    0.000000000000000       eshsci=   1.8832216011395907E-009
 Total number of micro iterations:    1

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 1.00000000 pnorm= 0.0000E+00 rznorm= 2.1144E-08 rpnorm= 0.0000E+00 noldr=  1 nnewr=  1 nolds=  0 nnews=  0

 !timer: Direct Bxr time contribution    user+sys=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  user+sys=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    user+sys=     0.000 walltime=     0.000
 !timer: solvek total                    user+sys=     0.000 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
   -15.321852

 qvv(*) eigenvalues. symmetry block  1
     0.811757    0.857800    0.957074    1.114145    1.248810    1.694346    2.443428    2.462132    3.151879    4.036750
     4.418300    4.486704

 qvv(*) eigenvalues. symmetry block  2
     0.942491    1.693244    1.762825    3.431461    3.563663
 !timer: motran                          user+sys=     0.000 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 user+sys=     0.020 walltime=     0.000

 all mcscf convergence criteria are satisfied.

 final mcscf convergence values:
 iter=    2 emc=  -25.6530063490 demc= 7.1054E-15 wnorm= 1.5066E-08 knorm= 2.8155E-09 apxde=-1.1230E-17    *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 weight 0.333333 total energy=      -25.704269375
   DRT #1 state # 2 weight 0.333333 total energy=      -25.655972348
   DRT #2 state # 1 weight 0.333333 total energy=      -25.598777324
   ------------------------------------------------------------



          mcscf orbitals of the final iteration,  a'  block   1

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
 B1  1s       1.00081063    -0.00105773     0.00000000    -0.00474368     0.00000000     0.10044140    -0.07523425     0.00000000
 B1  2s       0.00068202     0.61613667    -0.00000019    -0.72470797    -0.00000001     0.93531065    -0.00561521     0.00000000
 B1  *s      -0.00466703    -0.15538560    -0.00000004    -0.15671062     0.00000000    -0.08494004     1.83487019     0.00000000
 B1  2px      0.00113924     0.31147114     0.00000016     0.60713446    -0.00000001     1.25787827    -1.07631679     0.00000001
 B1  *px     -0.00221649    -0.09234809    -0.00000002    -0.09060991     0.00000000    -0.23163068     2.80000649    -0.00000001
 B1  2py      0.00000000     0.00000000     0.57427729    -0.00000015    -1.14700112    -0.00000001     0.00000001     1.27804163
 B1  *py      0.00000000     0.00000000    -0.11771822     0.00000003     0.19789517     0.00000000    -0.00000001    -1.97880708
 B1  *d2-     0.00000000     0.00000000     0.04936762    -0.00000001     0.05406695     0.00000000     0.00000000    -0.01162536
 B1  *d0     -0.00049766    -0.01346573    -0.00000001    -0.02348883     0.00000000     0.02251025    -0.08912726     0.00000000
 B1  *d2+     0.00038267     0.01267490     0.00000000     0.01194748     0.00000000    -0.02308832     0.06091812     0.00000000
 H1  1s      -0.00386898     0.48149334     0.58192123     0.23303938     0.89086612    -1.03928532    -0.01026701     0.26910050
 H1  *s       0.00308147    -0.15196950     0.00351906     0.01752542    -0.17645788     0.24015587    -1.36259137     0.19815606
 H1  *px      0.00045700    -0.01064807    -0.01452389     0.00650527    -0.00023865    -0.00294855     0.04528386    -0.04593218
 H1  *py      0.00047241    -0.01584083     0.00495167    -0.00766851     0.00751255    -0.02055003     0.05875170    -0.00375375
 H2  1s      -0.00386898     0.48149334    -0.58192111     0.23303968    -0.89086610    -1.03928534    -0.01026701    -0.26910048
 H2  *s       0.00308147    -0.15196950    -0.00351905     0.01752542     0.17645788     0.24015587    -1.36259138    -0.19815608
 H2  *px      0.00045700    -0.01064807     0.01452389     0.00650526     0.00023865    -0.00294854     0.04528386     0.04593218
 H2  *py     -0.00047241     0.01584083     0.00495168     0.00766850     0.00751255     0.02055003    -0.05875170    -0.00375375

               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
 B1  1s       0.19040117     0.00000001     0.74639278    -0.06899439    -0.27025251     0.00000000     0.04334689     0.00000000
 B1  2s       0.98213703     0.00000005     3.48195046    -0.30460560    -1.04611791     0.00000001     0.08781109     0.00000000
 B1  *s      -0.13445171    -0.00000007    -5.81294863     0.52125850     2.09366036    -0.00000001    -0.41785772     0.00000000
 B1  2px      0.19946392     0.00000000    -0.29065481    -0.05735196     0.03888923     0.00000000    -0.30590717     0.00000000
 B1  *px      0.19104995    -0.00000001    -1.26519134     0.19542701     0.88829141     0.00000000    -0.20075661     0.00000000
 B1  2py     -0.00000001    -0.07410779     0.00000000     0.00000000     0.00000000     0.05574354     0.00000000     0.40382183
 B1  *py      0.00000000     1.25124570    -0.00000002     0.00000000     0.00000000     0.18997753     0.00000000     0.05295495
 B1  *d2-     0.00000000     0.37101317    -0.00000001     0.00000000     0.00000001     1.12572080     0.00000000     0.60288512
 B1  *d0     -0.22969467     0.00000000     0.06959376     0.45798190    -1.00846687     0.00000001     0.25286190     0.00000000
 B1  *d2+     0.16325980     0.00000000    -0.02251986     0.74670875     0.36384275     0.00000000    -0.85718393     0.00000000
 H1  1s       1.01797114     0.99205964    -0.32935988    -0.09717603    -1.07186307    -1.15931582     0.55475414    -0.45546337
 H1  *s      -1.58843024    -2.72624331     1.88411235    -0.07513946     0.11404492     0.70782073    -0.22113836     0.22837968
 H1  *px      0.03121464     0.05447557    -0.04504578     0.08944155     0.07685767    -0.10011892     0.33369590     0.60832649
 H1  *py     -0.01939148     0.04912494    -0.05704328    -0.14113079    -0.17772106     0.04758920    -0.59371230    -0.58449237
 H2  1s       1.01797116    -0.99205963    -0.32935986    -0.09717602    -1.07186305     1.15931584     0.55475414     0.45546337
 H2  *s      -1.58843029     2.72624333     1.88411227    -0.07513947     0.11404491    -0.70782073    -0.22113836    -0.22837967
 H2  *px      0.03121464    -0.05447557    -0.04504578     0.08944156     0.07685767     0.10011892     0.33369590    -0.60832649
 H2  *py      0.01939148     0.04912495     0.05704328     0.14113079     0.17772106     0.04758920     0.59371230    -0.58449237

               MO   17        MO   18
 B1  1s      -0.11648624     0.00000000
 B1  2s      -0.97621085     0.00000001
 B1  *s      -0.67799950     0.00000001
 B1  2px     -1.36078618     0.00000002
 B1  *px      0.02569444     0.00000000
 B1  2py      0.00000001     0.87326912
 B1  *py      0.00000000    -0.05235341
 B1  *d2-     0.00000001     0.82518860
 B1  *d0      0.57385279    -0.00000001
 B1  *d2+    -0.35888097     0.00000001
 H1  1s       0.70389053    -0.66567368
 H1  *s       0.37651325    -0.40930240
 H1  *px     -0.84488217     0.73978955
 H1  *py     -0.50825106     0.67531899
 H2  1s       0.70389055     0.66567366
 H2  *s       0.37651326     0.40930239
 H2  *px     -0.84488219    -0.73978953
 H2  *py      0.50825108     0.67531898

          mcscf orbitals of the final iteration,  a"  block   2

               MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
 B1  2pz     -1.02104719     1.49685527     0.02256798     0.00000000     0.00000000     0.21813136
 B1  *pz      0.03065307    -1.82105013     0.01796518     0.00000000     0.00000000     0.10683667
 B1  *d1-     0.00000000     0.00000000     0.00000000    -0.95064017     0.43146160     0.00000000
 B1  *d1+     0.02565483    -0.01144061    -0.89629286     0.00000000     0.00000000     0.65133129
 H1  *pz     -0.01113513     0.02110488    -0.14420036    -0.10188238    -0.75124355    -0.77949722
 H2  *pz     -0.01113513     0.02110488    -0.14420036     0.10188238     0.75124356    -0.77949721

          natural orbitals of the final iteration, block  1

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8

  occ(*)=     2.00000000     1.95860103     1.30516998     1.28888515     0.04578139     0.03031733     0.00000000     0.00000000

 B1  1s       1.00081063    -0.00105773     0.00000000    -0.00474368     0.00000000     0.10044140    -0.07523422     0.00000000
 B1  2s       0.00068202     0.61613667    -0.00000019    -0.72470797     0.00000000     0.93531065    -0.00561504     0.00000000
 B1  *s      -0.00466703    -0.15538560    -0.00000004    -0.15671062     0.00000000    -0.08494004     1.83487033     0.00000001
 B1  2px      0.00113924     0.31147115     0.00000016     0.60713447    -0.00000001     1.25787827    -1.07631673    -0.00000001
 B1  *px     -0.00221649    -0.09234810    -0.00000002    -0.09060991     0.00000000    -0.23163068     2.80000658     0.00000002
 B1  2py      0.00000000     0.00000000     0.57427728    -0.00000015    -1.14700113     0.00000000    -0.00000001     1.27804163
 B1  *py      0.00000000     0.00000000    -0.11771822     0.00000003     0.19789517     0.00000000     0.00000001    -1.97880710
 B1  *d2-     0.00000000     0.00000000     0.04936762    -0.00000001     0.05406695     0.00000000     0.00000000    -0.01162538
 B1  *d0     -0.00049766    -0.01346573    -0.00000001    -0.02348883     0.00000000     0.02251025    -0.08912730     0.00000000
 B1  *d2+     0.00038267     0.01267490     0.00000000     0.01194748     0.00000000    -0.02308832     0.06091816     0.00000000
 H1  1s      -0.00386898     0.48149334     0.58192124     0.23303937     0.89086611    -1.03928533    -0.01026666     0.26910049
 H1  *s       0.00308147    -0.15196950     0.00351905     0.01752542    -0.17645788     0.24015587    -1.36259193     0.19815609
 H1  *px      0.00045700    -0.01064807    -0.01452389     0.00650527    -0.00023865    -0.00294855     0.04528386    -0.04593218
 H1  *py      0.00047241    -0.01584083     0.00495167    -0.00766851     0.00751255    -0.02055003     0.05875170    -0.00375375
 H2  1s      -0.00386898     0.48149334    -0.58192113     0.23303967    -0.89086610    -1.03928533    -0.01026665    -0.26910049
 H2  *s       0.00308147    -0.15196950    -0.00351904     0.01752543     0.17645788     0.24015587    -1.36259192    -0.19815611
 H2  *px      0.00045700    -0.01064807     0.01452389     0.00650526     0.00023865    -0.00294854     0.04528386     0.04593218
 H2  *py     -0.00047241     0.01584083     0.00495168     0.00766850     0.00751255     0.02055003    -0.05875170    -0.00375375

               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16

  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

 B1  1s       0.19040120     0.00000000     0.74639277    -0.06899437    -0.27025252     0.00000000     0.04334689     0.00000000
 B1  2s       0.98213707     0.00000002     3.48195045    -0.30460553    -1.04611792     0.00000000     0.08781109     0.00000000
 B1  *s      -0.13445234    -0.00000003    -5.81294854     0.52125833     2.09366049     0.00000000    -0.41785769     0.00000000
 B1  2px      0.19946423     0.00000000    -0.29065486    -0.05735197     0.03888922     0.00000000    -0.30590716     0.00000000
 B1  *px      0.19104909    -0.00000001    -1.26519122     0.19542695     0.88829151     0.00000000    -0.20075661     0.00000000
 B1  2py      0.00000000    -0.07410777     0.00000000     0.00000000     0.00000000     0.05574355     0.00000000     0.40382183
 B1  *py     -0.00000001     1.25124567    -0.00000001     0.00000000     0.00000000     0.18997747     0.00000000     0.05295495
 B1  *d2-     0.00000000     0.37101320     0.00000000     0.00000000     0.00000000     1.12572078     0.00000000     0.60288513
 B1  *d0     -0.22969461     0.00000000     0.06959373     0.45798192    -1.00846688     0.00000000     0.25286190     0.00000000
 B1  *d2+     0.16325979     0.00000000    -0.02251988     0.74670874     0.36384276     0.00000000    -0.85718393     0.00000000
 H1  1s       1.01797118     0.99205961    -0.32935989    -0.09717604    -1.07186304    -1.15931584     0.55475415    -0.45546337
 H1  *s      -1.58842984    -2.72624330     1.88411228    -0.07513938     0.11404481     0.70782080    -0.22113838     0.22837969
 H1  *px      0.03121462     0.05447557    -0.04504578     0.08944155     0.07685768    -0.10011893     0.33369590     0.60832648
 H1  *py     -0.01939150     0.04912495    -0.05704328    -0.14113079    -0.17772106     0.04758920    -0.59371229    -0.58449237
 H2  1s       1.01797118    -0.99205961    -0.32935988    -0.09717603    -1.07186304     1.15931584     0.55475415     0.45546337
 H2  *s      -1.58842986     2.72624330     1.88411225    -0.07513939     0.11404481    -0.70782081    -0.22113838    -0.22837969
 H2  *px      0.03121462    -0.05447557    -0.04504578     0.08944155     0.07685768     0.10011893     0.33369590    -0.60832649
 H2  *py      0.01939150     0.04912495     0.05704328     0.14113079     0.17772106     0.04758920     0.59371230    -0.58449237

               MO   17        MO   18

  occ(*)=     0.00000000     0.00000000

 B1  1s      -0.11648625     0.00000000
 B1  2s      -0.97621087     0.00000001
 B1  *s      -0.67799948     0.00000001
 B1  2px     -1.36078617     0.00000002
 B1  *px      0.02569442     0.00000000
 B1  2py      0.00000001     0.87326913
 B1  *py      0.00000000    -0.05235340
 B1  *d2-     0.00000001     0.82518861
 B1  *d0      0.57385278    -0.00000001
 B1  *d2+    -0.35888097     0.00000000
 H1  1s       0.70389052    -0.66567369
 H1  *s       0.37651327    -0.40930240
 H1  *px     -0.84488217     0.73978955
 H1  *py     -0.50825107     0.67531899
 H2  1s       0.70389053     0.66567367
 H2  *s       0.37651328     0.40930239
 H2  *px     -0.84488219    -0.73978953
 H2  *py      0.50825108     0.67531897

          natural orbitals of the final iteration, block  2

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6

  occ(*)=     0.37124512     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

 B1  2pz     -1.02104719     1.49685527     0.02256798     0.00000000     0.00000000     0.21813136
 B1  *pz      0.03065307    -1.82105013     0.01796518     0.00000000     0.00000000     0.10683667
 B1  *d1-     0.00000000     0.00000000     0.00000000    -0.95064017     0.43146160     0.00000000
 B1  *d1+     0.02565483    -0.01144061    -0.89629286     0.00000000     0.00000000     0.65133129
 H1  *pz     -0.01113513     0.02110488    -0.14420036    -0.10188238    -0.75124355    -0.77949722
 H2  *pz     -0.01113513     0.02110488    -0.14420036     0.10188238     0.75124356    -0.77949721
 d1(*), fmc(*), and qmc(*) written to the 1-particle density matrix file.
        165 d2(*) elements written to the 2-particle density matrix file.
 !timer: writing the mc density files requser+sys=     0.000 walltime=     0.000


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!


                        a'  partial gross atomic populations
   ao class       1a'        2a'        3a'        4a'        5a'        6a' 
    B1_ s       1.999928   0.702109   0.000000   0.691789   0.000000   0.003399
    B1_ p       0.000004   0.241437   0.501199   0.511823   0.026778   0.013310
    B1_ d      -0.000001   0.007148   0.024378   0.004411   0.001236   0.000394
    H1_ s       0.000086   0.494742   0.386543   0.038999   0.008852   0.006451
    H1_ p      -0.000052   0.009211   0.003254   0.001432   0.000031   0.000156
    H2_ s       0.000086   0.494742   0.386543   0.038999   0.008852   0.006451
    H2_ p      -0.000052   0.009211   0.003254   0.001432   0.000031   0.000156

   ao class       7a'        8a'        9a'       10a'       11a'       12a' 

   ao class      13a'       14a'       15a'       16a'       17a'       18a' 

                        a"  partial gross atomic populations
   ao class       1a"        2a"        3a"        4a"        5a"        6a" 
    B1_ p       0.369506   0.000000   0.000000   0.000000   0.000000   0.000000
    B1_ d       0.000180   0.000000   0.000000   0.000000   0.000000   0.000000
    H1_ p       0.000779   0.000000   0.000000   0.000000   0.000000   0.000000
    H2_ p       0.000779   0.000000   0.000000   0.000000   0.000000   0.000000


                        gross atomic populations
     ao           B1_        H1_        H2_
      s         3.397225   0.935673   0.935673
      p         1.664058   0.014812   0.014812
      d         0.037748   0.000000   0.000000
    total       5.099031   0.950485   0.950485


 Total number of electrons:    7.00000000

 !timer: mcscf                           user+sys=     0.070 walltime=     0.000
