 
 program cidrt 5.9  

 distinct row table construction, reference csf selection, and internal
 walk selection for multireference single- and double-excitation
configuration interaction.

 references:  r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).
              h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. symp. 15, 91 (1981).

 programmed by: Thomas Müller (FZ Juelich)
 based on the cidrt code for single DRTs.

 version date: 16-jul-04


 This Version of Program CIDRT-MS is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIDRT-MS    **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 workspace allocation parameters: lencor=  13107200 mem1=         0 ifirst=         1
 expanded "keystrokes" are being written to files:
                       
 cidrtky                                                                         

 number of DRTs to be constructed [  0]: DRT infos are being written to files:
 cidrtfl.1                                                                       
 starting with the input common to all drts ... 
******************** ALL   DRTs    ********************
 Spin-Orbit CI Calculation?(y,[n]) Spin-Free Calculation

 input the spin multiplicity [  0]: spin multiplicity, smult            :   2    doublet 
 input the total number of electrons [  0]: total number of electrons, nelt     :     7
 input the number of irreps (1:8) [  0]: point group dimension, nsym         :     2
 enter symmetry labels:(y,[n]) enter 2 labels (a4):
 enter symmetry label, default=   1
 enter symmetry label, default=   2
 symmetry labels: (symmetry, slabel)
 ( 1,  a' ) ( 2,  a" ) 
 input nmpsy(*):
 nmpsy(*)=        18   6

   symmetry block summary
 block(*)=         1   2
 slabel(*)=      a'  a" 
 nmpsy(*)=        18   6

 total molecular orbitals            :    24

 input the frozen core orbitals (sym(i),rmo(i),i=1,nfct):
 total frozen core orbitals, nfct    :     0
 no frozen core orbitals entered

 number of frozen core orbitals      :     0
 number of frozen core electrons     :     0
 number of internal electrons        :     7

 input the frozen virtual orbitals (sym(i),rmo(i),i=1,nfvt):
 total frozen virtual orbitals, nfvt :     0

 no frozen virtual orbitals entered

 input the internal orbitals (sym(i),rmo(i),i=1,niot):
 niot                                :     7

 modrt(*)=         1   2   3   4   5   6  19
 slabel(*)=      a'  a'  a'  a'  a'  a'  a" 

 total number of orbitals            :    24
 number of frozen core orbitals      :     0
 number of frozen virtual orbitals   :     0
 number of internal orbitals         :     7
 number of external orbitals         :    17

 orbital-to-level mapping vector
 map(*)=          18  19  20  21  22  23   1   2   3   4   5   6   7   8   9
                  10  11  12  24  13  14  15  16  17
==================== START DRT#   1====================
================================================================================
  Input for DRT number                         1
 input the molecular spatial symmetry (irrep 1:nsym) [  0]: state spatial symmetry label        :  a' 

 input the number of ref-csf doubly-occupied orbitals [  0]: (ref) doubly-occupied orbitals      :     0

 no. of internal orbitals            :     7
 no. of doubly-occ. (ref) orbitals   :     0
 no. active (ref) orbitals           :     7
 no. of active electrons             :     7

 input the active-orbital, active-electron occmnr(*):
   1  2  3  4  5  6 19
 input the active-orbital, active-electron occmxr(*):
   1  2  3  4  5  6 19

 actmo(*) =        1   2   3   4   5   6  19
 occmnr(*)=        0   0   0   0   0   0   7
 occmxr(*)=        7   7   7   7   7   7   7
 reference csf cumulative electron occupations:
 modrt(*)=         1   2   3   4   5   6  19
 occmnr(*)=        0   0   0   0   0   0   7
 occmxr(*)=        7   7   7   7   7   7   7

 input the active-orbital bminr(*):
   1  2  3  4  5  6 19
 input the active-orbital bmaxr(*):
   1  2  3  4  5  6 19
 reference csf b-value constraints:
 modrt(*)=         1   2   3   4   5   6  19
 bminr(*)=         0   0   0   0   0   0   0
 bmaxr(*)=         7   7   7   7   7   7   7
 input the active orbital smaskr(*):
   1  2  3  4  5  6 19
 modrt:smaskr=
   1:1000   2:1111   3:1111   4:1111   5:1111   6:1111  19:1111

 input the maximum excitation level from the reference csfs [  2]: maximum excitation from ref. csfs:  :     2
 number of internal electrons:       :     7

 input the internal-orbital mrsdci occmin(*):
   1  2  3  4  5  6 19
 input the internal-orbital mrsdci occmax(*):
   1  2  3  4  5  6 19
 mrsdci csf cumulative electron occupations:
 modrt(*)=         1   2   3   4   5   6  19
 occmin(*)=        0   0   0   0   0   0   7
 occmax(*)=        7   7   7   7   7   7   7

 input the internal-orbital mrsdci bmin(*):
   1  2  3  4  5  6 19
 input the internal-orbital mrsdci bmax(*):
   1  2  3  4  5  6 19
 mrsdci b-value constraints:
 modrt(*)=         1   2   3   4   5   6  19
 bmin(*)=          0   0   0   0   0   0   0
 bmax(*)=          7   7   7   7   7   7   7

 input the internal-orbital smask(*):
   1  2  3  4  5  6 19
 modrt:smask=
   1:1000   2:1111   3:1111   4:1111   5:1111   6:1111  19:1111

 internal orbital summary:
 block(*)=         1   1   1   1   1   1   2
 slabel(*)=      a'  a'  a'  a'  a'  a'  a" 
 rmo(*)=           1   2   3   4   5   6   1
 modrt(*)=         1   2   3   4   5   6  19

 reference csf info:
 occmnr(*)=        0   0   0   0   0   0   7
 occmxr(*)=        7   7   7   7   7   7   7

 bminr(*)=         0   0   0   0   0   0   0
 bmaxr(*)=         7   7   7   7   7   7   7


 mrsdci csf info:
 occmin(*)=        0   0   0   0   0   0   7
 occmax(*)=        7   7   7   7   7   7   7

 bmin(*)=          0   0   0   0   0   0   0
 bmax(*)=          7   7   7   7   7   7   7

******************** END   DRT#   1********************
******************** ALL   DRTs    ********************

 impose generalized interacting space restrictions?(y,[n]) generalized interacting space restrictions will not be imposed.

 a priori removal of distinct rows:

 input the level, a, and b values for the vertices 
 to be removed (-1/ to end).

 input level, a, and b (-1/ to end):
 no vertices marked for removal

 number of rows in the drt :  47

 manual arc removal step:


 input the level, a, b, and step values 
 for the arcs to be removed (-1/ to end).

 input the level, a, b, and step (-1/ to end):
 remarc:   0 arcs removed out of   0 specified.

 xbarz=     210
 xbary=     210
 xbarx=      90
 xbarw=      70
        --------
 nwalk=     580
------------------------------------------------------------------------
Core address array: 
             1             5
------------------------------------------------------------------------
 input the range of drt levels to print (l1,l2):
 levprt(*)        -1   0
==================== START DRT#   1====================
  INPUT FOR DRT #                         1

 reference-csf selection step 1:
 total number of z-walks in the drt, nzwalk=     210

 input the list of allowed reference symmetries:
 allowed reference symmetries:             1    2
 allowed reference symmetry labels:      a'   a" 
 keep all of the z-walks as references?(y,[n]) all z-walks are initially deleted.

 generate walks while applying reference drt restrictions?([y],n) reference drt restrictions will be imposed on the z-walks.

 impose additional orbital-group occupation restrictions?(y,[n])
 apply primary reference occupation restrictions?(y,[n])
 manually select individual walks?(y,[n])
 step 1 reference csf selection complete.
      210 csfs initially selected from     210 total walks.

 beginning step-vector based selection.
 enter [internal_orbital_step_vector/disposition] pairs:

 enter internal orbital step vector, (-1/ to end):
   1  2  3  4  5  6 19

 step 2 reference csf selection complete.
      210 csfs currently selected from     210 total walks.

 beginning numerical walk based selection.
 enter positive walk numbers to add walks,
 negative walk numbers to delete walks, and zero to end:

 input reference walk number (0 to end) [  0]:
 numerical walk-number based selection complete.
      210 reference csfs selected from     210 total z-walks.

 input the reference occupations, mu(*):
 reference occupations:
 mu(*)=            0   0   0   0   0   0   0

------------------------------------------------------------------------
Core address array: 
             1             5          1685          3365          3575

------------------------------------------------------------------------
      1: 3331000
      2: 3330100
      3: 3330010
      4: 3330001
      5: 3313000
      6: 3312100
      7: 3312010
      8: 3312001
      9: 3311200
     10: 3311020
     11: 3311002
     12: 3310300
     13: 3310210
     14: 3310201
     15: 3310120
     16: 3310102
     17: 3310030
     18: 3310021
     19: 3310012
     20: 3310003
     21: 3303100
     22: 3303010
     23: 3303001
     24: 3301300
     25: 3301210
     26: 3301201
     27: 3301120
     28: 3301102
     29: 3301030
     30: 3301021
     31: 3301012
     32: 3301003
     33: 3300310
     34: 3300301
     35: 3300130
     36: 3300121
     37: 3300112
     38: 3300103
     39: 3300031
     40: 3300013
     41: 3133000
     42: 3132100
     43: 3132010
     44: 3132001
     45: 3131200
     46: 3131020
     47: 3131002
     48: 3130300
     49: 3130210
     50: 3130201
     51: 3130120
     52: 3130102
     53: 3130030
     54: 3130021
     55: 3130012
     56: 3130003
     57: 3123100
     58: 3123010
     59: 3123001
     60: 3121300
     61: 3121210
     62: 3121201
     63: 3121120
     64: 3121102
     65: 3121030
     66: 3121021
     67: 3121012
     68: 3121003
     69: 3120310
     70: 3120301
     71: 3120130
     72: 3120121
     73: 3120112
     74: 3120103
     75: 3120031
     76: 3120013
     77: 3113200
     78: 3113020
     79: 3113002
     80: 3112300
     81: 3112210
     82: 3112201
     83: 3112120
     84: 3112102
     85: 3112030
     86: 3112021
     87: 3112012
     88: 3112003
     89: 3111220
     90: 3111202
     91: 3111022
     92: 3110320
     93: 3110302
     94: 3110230
     95: 3110221
     96: 3110212
     97: 3110203
     98: 3110122
     99: 3110032
    100: 3110023
    101: 3103300
    102: 3103210
    103: 3103201
    104: 3103120
    105: 3103102
    106: 3103030
    107: 3103021
    108: 3103012
    109: 3103003
    110: 3102310
    111: 3102301
    112: 3102130
    113: 3102121
    114: 3102112
    115: 3102103
    116: 3102031
    117: 3102013
    118: 3101320
    119: 3101302
    120: 3101230
    121: 3101221
    122: 3101212
    123: 3101203
    124: 3101122
    125: 3101032
    126: 3101023
    127: 3100330
    128: 3100321
    129: 3100312
    130: 3100303
    131: 3100231
    132: 3100213
    133: 3100132
    134: 3100123
    135: 3100033
    136: 3033100
    137: 3033010
    138: 3033001
    139: 3031300
    140: 3031210
    141: 3031201
    142: 3031120
    143: 3031102
    144: 3031030
    145: 3031021
    146: 3031012
    147: 3031003
    148: 3030310
    149: 3030301
    150: 3030130
    151: 3030121
    152: 3030112
    153: 3030103
    154: 3030031
    155: 3030013
    156: 3013300
    157: 3013210
    158: 3013201
    159: 3013120
    160: 3013102
    161: 3013030
    162: 3013021
    163: 3013012
    164: 3013003
    165: 3012310
    166: 3012301
    167: 3012130
    168: 3012121
    169: 3012112
    170: 3012103
    171: 3012031
    172: 3012013
    173: 3011320
    174: 3011302
    175: 3011230
    176: 3011221
    177: 3011212
    178: 3011203
    179: 3011122
    180: 3011032
    181: 3011023
    182: 3010330
    183: 3010321
    184: 3010312
    185: 3010303
    186: 3010231
    187: 3010213
    188: 3010132
    189: 3010123
    190: 3010033
    191: 3003310
    192: 3003301
    193: 3003130
    194: 3003121
    195: 3003112
    196: 3003103
    197: 3003031
    198: 3003013
    199: 3001330
    200: 3001321
    201: 3001312
    202: 3001303
    203: 3001231
    204: 3001213
    205: 3001132
    206: 3001123
    207: 3001033
    208: 3000331
    209: 3000313
    210: 3000133
 number of step vectors saved:    210

 exlimw: beginning excitation-based walk selection...

  number of valid internal walks of each symmetry:

       a'      a" 
      ----    ----
 z     115      95
 y     120      90
 x      55      35
 w      45      25
 nviwsm(*) error: z-vertex, isym=                        2

 csfs grouped by internal walk symmetry:

       a'      a" 
      ----    ----
 z     115       0
 y    1440     450
 x    4180    2100
 w    4185    1500

 total csf counts:
 z-vertex:      115
 y-vertex:     1890
 x-vertex:     6280
 w-vertex:     5685
           --------
 total:       13970
******************** END   DRT#   1********************
******************** ALL   DRTs    ********************
 Generating 3-DRT at exlimw level ... 

 xbarz=     210
 xbary=     210
 xbarx=      90
 xbarw=      70
        --------
 nwalk=     580
 levprt(*)        -1   0
------------------------------------------------------------------------
Core address array: 
             1             5          1685          3365          3575
          3785
------------------------------------------------------------------------
==================== START DRT#   1====================
 Converting reference vector for DRT #                        1

 beginning the reference csf index recomputation...

     iref   iwalk  step-vector
   ------  ------  ------------
        1       1  3331000
        2       2  3330100
        3       3  3330010
        4       4  3330001
        5       5  3313000
        6       6  3312100
        7       7  3312010
        8       8  3312001
        9       9  3311200
       10      10  3311020
       11      11  3311002
       12      12  3310300
       13      13  3310210
       14      14  3310201
       15      15  3310120
       16      16  3310102
       17      17  3310030
       18      18  3310021
       19      19  3310012
       20      20  3310003
       21      21  3303100
       22      22  3303010
       23      23  3303001
       24      24  3301300
       25      25  3301210
       26      26  3301201
       27      27  3301120
       28      28  3301102
       29      29  3301030
       30      30  3301021
       31      31  3301012
       32      32  3301003
       33      33  3300310
       34      34  3300301
       35      35  3300130
       36      36  3300121
       37      37  3300112
       38      38  3300103
       39      39  3300031
       40      40  3300013
       41      41  3133000
       42      42  3132100
       43      43  3132010
       44      44  3132001
       45      45  3131200
       46      46  3131020
       47      47  3131002
       48      48  3130300
       49      49  3130210
       50      50  3130201
       51      51  3130120
       52      52  3130102
       53      53  3130030
       54      54  3130021
       55      55  3130012
       56      56  3130003
       57      57  3123100
       58      58  3123010
       59      59  3123001
       60      60  3121300
       61      61  3121210
       62      62  3121201
       63      63  3121120
       64      64  3121102
       65      65  3121030
       66      66  3121021
       67      67  3121012
       68      68  3121003
       69      69  3120310
       70      70  3120301
       71      71  3120130
       72      72  3120121
       73      73  3120112
       74      74  3120103
       75      75  3120031
       76      76  3120013
       77      77  3113200
       78      78  3113020
       79      79  3113002
       80      80  3112300
       81      81  3112210
       82      82  3112201
       83      83  3112120
       84      84  3112102
       85      85  3112030
       86      86  3112021
       87      87  3112012
       88      88  3112003
       89      89  3111220
       90      90  3111202
       91      91  3111022
       92      92  3110320
       93      93  3110302
       94      94  3110230
       95      95  3110221
       96      96  3110212
       97      97  3110203
       98      98  3110122
       99      99  3110032
      100     100  3110023
      101     101  3103300
      102     102  3103210
      103     103  3103201
      104     104  3103120
      105     105  3103102
      106     106  3103030
      107     107  3103021
      108     108  3103012
      109     109  3103003
      110     110  3102310
      111     111  3102301
      112     112  3102130
      113     113  3102121
      114     114  3102112
      115     115  3102103
      116     116  3102031
      117     117  3102013
      118     118  3101320
      119     119  3101302
      120     120  3101230
      121     121  3101221
      122     122  3101212
      123     123  3101203
      124     124  3101122
      125     125  3101032
      126     126  3101023
      127     127  3100330
      128     128  3100321
      129     129  3100312
      130     130  3100303
      131     131  3100231
      132     132  3100213
      133     133  3100132
      134     134  3100123
      135     135  3100033
      136     136  3033100
      137     137  3033010
      138     138  3033001
      139     139  3031300
      140     140  3031210
      141     141  3031201
      142     142  3031120
      143     143  3031102
      144     144  3031030
      145     145  3031021
      146     146  3031012
      147     147  3031003
      148     148  3030310
      149     149  3030301
      150     150  3030130
      151     151  3030121
      152     152  3030112
      153     153  3030103
      154     154  3030031
      155     155  3030013
      156     156  3013300
      157     157  3013210
      158     158  3013201
      159     159  3013120
      160     160  3013102
      161     161  3013030
      162     162  3013021
      163     163  3013012
      164     164  3013003
      165     165  3012310
      166     166  3012301
      167     167  3012130
      168     168  3012121
      169     169  3012112
      170     170  3012103
      171     171  3012031
      172     172  3012013
      173     173  3011320
      174     174  3011302
      175     175  3011230
      176     176  3011221
      177     177  3011212
      178     178  3011203
      179     179  3011122
      180     180  3011032
      181     181  3011023
      182     182  3010330
      183     183  3010321
      184     184  3010312
      185     185  3010303
      186     186  3010231
      187     187  3010213
      188     188  3010132
      189     189  3010123
      190     190  3010033
      191     191  3003310
      192     192  3003301
      193     193  3003130
      194     194  3003121
      195     195  3003112
      196     196  3003103
      197     197  3003031
      198     198  3003013
      199     199  3001330
      200     200  3001321
      201     201  3001312
      202     202  3001303
      203     203  3001231
      204     204  3001213
      205     205  3001132
      206     206  3001123
      207     207  3001033
      208     208  3000331
      209     209  3000313
      210     210  3000133
 indx01:   210 elements set in vec01(*)
******************** END   DRT#   1********************
******************** ALL   DRTs    ********************
==================== START DRT#   1====================
------------------------------------------------------------------------
Core address array: 
             1             5          1685          3365          3575

------------------------------------------------------------------------
      1: 3331000
      2: 3330100
      3: 3330010
      4: 3330001
      5: 3313000
      6: 3312100
      7: 3312010
      8: 3312001
      9: 3311200
     10: 3311020
     11: 3311002
     12: 3310300
     13: 3310210
     14: 3310201
     15: 3310120
     16: 3310102
     17: 3310030
     18: 3310021
     19: 3310012
     20: 3310003
     21: 3303100
     22: 3303010
     23: 3303001
     24: 3301300
     25: 3301210
     26: 3301201
     27: 3301120
     28: 3301102
     29: 3301030
     30: 3301021
     31: 3301012
     32: 3301003
     33: 3300310
     34: 3300301
     35: 3300130
     36: 3300121
     37: 3300112
     38: 3300103
     39: 3300031
     40: 3300013
     41: 3133000
     42: 3132100
     43: 3132010
     44: 3132001
     45: 3131200
     46: 3131020
     47: 3131002
     48: 3130300
     49: 3130210
     50: 3130201
     51: 3130120
     52: 3130102
     53: 3130030
     54: 3130021
     55: 3130012
     56: 3130003
     57: 3123100
     58: 3123010
     59: 3123001
     60: 3121300
     61: 3121210
     62: 3121201
     63: 3121120
     64: 3121102
     65: 3121030
     66: 3121021
     67: 3121012
     68: 3121003
     69: 3120310
     70: 3120301
     71: 3120130
     72: 3120121
     73: 3120112
     74: 3120103
     75: 3120031
     76: 3120013
     77: 3113200
     78: 3113020
     79: 3113002
     80: 3112300
     81: 3112210
     82: 3112201
     83: 3112120
     84: 3112102
     85: 3112030
     86: 3112021
     87: 3112012
     88: 3112003
     89: 3111220
     90: 3111202
     91: 3111022
     92: 3110320
     93: 3110302
     94: 3110230
     95: 3110221
     96: 3110212
     97: 3110203
     98: 3110122
     99: 3110032
    100: 3110023
    101: 3103300
    102: 3103210
    103: 3103201
    104: 3103120
    105: 3103102
    106: 3103030
    107: 3103021
    108: 3103012
    109: 3103003
    110: 3102310
    111: 3102301
    112: 3102130
    113: 3102121
    114: 3102112
    115: 3102103
    116: 3102031
    117: 3102013
    118: 3101320
    119: 3101302
    120: 3101230
    121: 3101221
    122: 3101212
    123: 3101203
    124: 3101122
    125: 3101032
    126: 3101023
    127: 3100330
    128: 3100321
    129: 3100312
    130: 3100303
    131: 3100231
    132: 3100213
    133: 3100132
    134: 3100123
    135: 3100033
    136: 3033100
    137: 3033010
    138: 3033001
    139: 3031300
    140: 3031210
    141: 3031201
    142: 3031120
    143: 3031102
    144: 3031030
    145: 3031021
    146: 3031012
    147: 3031003
    148: 3030310
    149: 3030301
    150: 3030130
    151: 3030121
    152: 3030112
    153: 3030103
    154: 3030031
    155: 3030013
    156: 3013300
    157: 3013210
    158: 3013201
    159: 3013120
    160: 3013102
    161: 3013030
    162: 3013021
    163: 3013012
    164: 3013003
    165: 3012310
    166: 3012301
    167: 3012130
    168: 3012121
    169: 3012112
    170: 3012103
    171: 3012031
    172: 3012013
    173: 3011320
    174: 3011302
    175: 3011230
    176: 3011221
    177: 3011212
    178: 3011203
    179: 3011122
    180: 3011032
    181: 3011023
    182: 3010330
    183: 3010321
    184: 3010312
    185: 3010303
    186: 3010231
    187: 3010213
    188: 3010132
    189: 3010123
    190: 3010033
    191: 3003310
    192: 3003301
    193: 3003130
    194: 3003121
    195: 3003112
    196: 3003103
    197: 3003031
    198: 3003013
    199: 3001330
    200: 3001321
    201: 3001312
    202: 3001303
    203: 3001231
    204: 3001213
    205: 3001132
    206: 3001123
    207: 3001033
    208: 3000331
    209: 3000313
    210: 3000133
 number of step vectors saved:    210

 exlimw: beginning excitation-based walk selection...

  number of valid internal walks of each symmetry:

       a'      a" 
      ----    ----
 z     115       0
 y     120      90
 x      55      35
 w      45      25

 csfs grouped by internal walk symmetry:

       a'      a" 
      ----    ----
 z     115       0
 y    1440     450
 x    4180    2100
 w    4185    1500

 total csf counts:
 z-vertex:      115
 y-vertex:     1890
 x-vertex:     6280
 w-vertex:     5685
           --------
 total:       13970

 final mrsdci walk selection step:

 nvalw(*)=     115     210      90      70 nvalwt=     485

 enter positive walk numbers to add walks,
 negative walk numbers to delete walks, and zero to end.

 input mrsdci walk number (0 to end) [  0]:
 end of manual mrsdci walk selection.
 number added=   0 number removed=   0

 nvalw(*)=     115     210      90      70 nvalwt=     485


 beginning the final csym(*) computation...

  number of valid internal walks of each symmetry:

       a'      a" 
      ----    ----
 z     115       0
 y     120      90
 x      55      35
 w      45      25

 csfs grouped by internal walk symmetry:

       a'      a" 
      ----    ----
 z     115       0
 y    1440     450
 x    4180    2100
 w    4185    1500

 total csf counts:
 z-vertex:      115
 y-vertex:     1890
 x-vertex:     6280
 w-vertex:     5685
           --------
 total:       13970
 drt file is being written...
------------------------------------------------------------------------
Core address array: 
             1             5          1685          3365          3945
          4430          6030
------------------------------------------------------------------------

 input a title card, default=cidrt_title
 title card for DRT #                         1
  cidrt_title DRT#1                                                              
 drt file is being written... for DRT #                        1
 CI version:                        6
 wrtstr:  a'  a" 
nwalk=     580 cpos=     189 maxval=    9 cmprfactor=   67.41 %.
nwalk=     580 cpos=     146 maxval=   99 cmprfactor=   49.66 %.
 compressed with: nwalk=     580 cpos=     189 maxval=    9 cmprfactor=   67.41 %.
initial index vector length:       580
compressed index vector length:       189reduction:  67.41%
nwalk=     210 cpos=      27 maxval=    9 cmprfactor=   87.14 %.
nwalk=     210 cpos=       3 maxval=   99 cmprfactor=   97.14 %.
nwalk=     210 cpos=       1 maxval=  999 cmprfactor=   98.57 %.
nwalk=     210 cpos=       1 maxval= 9999 cmprfactor=   98.10 %.
 compressed with: nwalk=     210 cpos=       1 maxval=  999 cmprfactor=   98.57 %.
initial ref vector length:       210
compressed ref vector length:         1reduction:  99.52%
******************** END   DRT#   1********************
 !timer: cidrt required                  user+sys=     0.000 walltime=     0.000
