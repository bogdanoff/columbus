 total ao core energy =    4.196537893
 MCSCF calculation performed for  2 DRTs.

 DRT  first state   no.of aver. states   weights
  1   ground state          2             0.333 0.333
  2   ground state          1             0.333

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a' 
 Total number of electrons:     7
 Spin multiplicity:            2
 Number of active orbitals:    6
 Number of active electrons:   5
 Total number of CSFs:       115

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a" 
 Total number of electrons:     7
 Spin multiplicity:            2
 Number of active orbitals:    6
 Number of active electrons:   5
 Total number of CSFs:        95

 Number of active-double rotations:         5
 Number of active-active rotations:         0
 Number of double-virtual rotations:       12
 Number of active-virtual rotations:       65

 iter=    1 emc=  -25.6530063490 demc= 2.5653E+01 wnorm= 9.5641E-08 knorm= 1.7028E-07 apxde= 3.0926E-15    *not converged* 

 final mcscf convergence values:
 iter=    2 emc=  -25.6530063490 demc= 7.1054E-15 wnorm= 1.5066E-08 knorm= 2.8155E-09 apxde=-1.1230E-17    *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 weight 0.333333 total energy=      -25.704269375
   DRT #1 state # 2 weight 0.333333 total energy=      -25.655972348
   DRT #2 state # 1 weight 0.333333 total energy=      -25.598777324
   ------------------------------------------------------------


