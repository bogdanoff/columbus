1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=    24)...
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.2740792789 -3.9968E-15  5.7057E-02  2.2790E-01  1.0000E-03

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  1  1    -46.2740792789 -3.9968E-15  5.7057E-02  2.2790E-01  1.0000E-03

 from bk iterations: iconv=   1

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.3229910521  4.8912E-02  2.4700E-03  4.5414E-02  1.0000E-03
 mr-sdci #  2  1    -46.3255607754  2.5697E-03  3.8611E-04  1.7379E-02  1.0000E-03
 mr-sdci #  3  1    -46.3258918922  3.3112E-04  7.3991E-05  7.9963E-03  1.0000E-03
 mr-sdci #  4  1    -46.3259515186  5.9626E-05  7.0173E-06  2.4399E-03  1.0000E-03
 mr-sdci #  5  1    -46.3259581397  6.6212E-06  7.2812E-07  7.9836E-04  1.0000E-03

 mr-sdci  convergence criteria satisfied after  5 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  5  1    -46.3259581397  6.6212E-06  7.2812E-07  7.9836E-04  1.0000E-03

 number of reference csfs (nref) is    24.  root number (iroot) is  1.
 c0**2 =   0.92261790  c**2 (all zwalks) =   0.92261790

 eref      =    -46.274037490723   "relaxed" cnot**2         =   0.922617895351
 eci       =    -46.325958139748   deltae = eci - eref       =  -0.051920649025
 eci+dv1   =    -46.329975868844   dv1 = (1-cnot**2)*deltae  =  -0.004017729096
 eci+dv2   =    -46.330312845110   dv2 = dv1 / cnot**2       =  -0.004354705362
 eci+dv3   =    -46.330711522343   dv3 = dv1 / (2*cnot**2-1) =  -0.004753382595
 eci+pople =    -46.328131676656   (  4e- scaled deltae )    =  -0.054094185933
