

     ******************************************
     **    PROGRAM:              MCPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 original author: Daniel Robertson, FSU
 later revisions: Ron Shepard, ANL;
                  Michal Dallos, University Vienna



 This Version of Program mcpc is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



   ******  File header section  ******

 Headers form the restart file:
                                                                                    
    aoints SIFS file created by argos.      zam792            17:53:52.086 17-Dec-13
     title                                                                          
     title                                                                          
     title                                                                          
     title                                                                          


   ******  DRT info section  ******

 Informations for the DRT no.  1
 Header form the DRT file: 
     title                                                                          
 Molecular symmetry group:   sym1
 Total number of electrons:   12
 Spin multiplicity:            3
 Number of active orbitals:    6
 Number of active electrons:   4
 Total number of CSFs:        21

   ***  Informations from the DRT number:   1

 
 Symmetry orbital summary:
 Symm.blocks:         1     2     3     4
 Symm.labels:         a1    a2    b1    b2 

 List of doubly occupied orbitals:
  1 a1   2 a1   1 b1   1 b2 

 List of active orbitals:
  3 a1   4 a1   5 a1   1 a2   2 b1   2 b2 

 Informations for the DRT no.  2
 Header form the DRT file: 
     title                                                                          
 Molecular symmetry group:    a2 
 Total number of electrons:   12
 Spin multiplicity:            3
 Number of active orbitals:    6
 Number of active electrons:   4
 Total number of CSFs:        28

   ***  Informations from the DRT number:   2

 
 Symmetry orbital summary:
 Symm.blocks:         1     2     3     4
 Symm.labels:         a1    a2    b1    b2 

 List of doubly occupied orbitals:
  1 a1   2 a1   1 b1   1 b2 

 List of active orbitals:
  3 a1   4 a1   5 a1   1 a2   2 b1   2 b2 

 Informations for the DRT no.  3
 Header form the DRT file: 
     title                                                                          
 Molecular symmetry group:    b1 
 Total number of electrons:   12
 Spin multiplicity:            3
 Number of active orbitals:    6
 Number of active electrons:   4
 Total number of CSFs:        28

   ***  Informations from the DRT number:   3

 
 Symmetry orbital summary:
 Symm.blocks:         1     2     3     4
 Symm.labels:         a1    a2    b1    b2 

 List of doubly occupied orbitals:
  1 a1   2 a1   1 b1   1 b2 

 List of active orbitals:
  3 a1   4 a1   5 a1   1 a2   2 b1   2 b2 

 Informations for the DRT no.  4
 Header form the DRT file: 
     title                                                                          
 Molecular symmetry group:    b2 
 Total number of electrons:   12
 Spin multiplicity:            3
 Number of active orbitals:    6
 Number of active electrons:   4
 Total number of CSFs:        28

   ***  Informations from the DRT number:   4

 
 Symmetry orbital summary:
 Symm.blocks:         1     2     3     4
 Symm.labels:         a1    a2    b1    b2 

 List of doubly occupied orbitals:
  1 a1   2 a1   1 b1   1 b2 

 List of active orbitals:
  3 a1   4 a1   5 a1   1 a2   2 b1   2 b2 


   ******  MCSCF convergence information:  ******

 MCSCF convergence criteria were satisfied.

 mcscf energy=   -46.3018785757    nuclear repulsion=     0.0000000000
 demc=             0.0000000000    wnorm=                 0.0000008053
 knorm=            0.0000000310    apxde=                 0.0000000000


 MCSCF calculation performmed for   4 symmetries.

 State averaging:
 No,  ssym, navst, wavst
  1    a1     1   0.1429
  2    a2     2   0.1429 0.1429
  3    b1     2   0.1429 0.1429
  4    b2     2   0.1429 0.1429

 Input the DRT No of interest: [  1]:
In the DRT No.: 2 there are  2 states.

 Which one to take? [  1]:
 The CSFs for the state No  1 of the symmetry  a2  will be printed
 according to the following print options :

 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 9) export wave function files for cioverlap (all states).
 0) end.

 input menu number [  0]: csfs will be printed based on coefficient magnitudes.

 input the coefficient threshold (end with 0.) [ 0.0000]:
 List of active orbitals:
  3 a1   4 a1   5 a1   1 a2   2 b1   2 b2 

   csf       coeff       coeff**2    step(*)
  -----  ------------  ------------  ------------
      2 -0.7480365766  0.5595587199  301100
      3 -0.5742369142  0.3297480337  300011
      1  0.2959378198  0.0875791932  310100
     10 -0.0724826337  0.0052537322  110012
     11  0.0599740813  0.0035968904  103100
     16  0.0485088523  0.0023531088  100103
     14  0.0472873614  0.0022360945  101012
      7  0.0428744946  0.0018382223  112100
     26  0.0352038049  0.0012393079  001130
     17  0.0348488305  0.0012144410  031100
     28  0.0266598864  0.0007107495  000311
     18  0.0264975751  0.0007021215  030011
      8  0.0263736468  0.0006955692  111200
     27  0.0244425331  0.0005974374  001103
      6  0.0232778825  0.0005418598  120011
     12 -0.0224813157  0.0005054096  102011
     25  0.0193825050  0.0003756815  003011
     24 -0.0169833259  0.0002884334  010103
      5  0.0162520681  0.0002641297  121100
      9 -0.0160462043  0.0002574807  110021
     23 -0.0115960961  0.0001344694  010130
     15  0.0098729937  0.0000974760  100130
     19  0.0088044860  0.0000775190  013100
     13 -0.0075240869  0.0000566119  101021
     22 -0.0062270961  0.0000387767  011012
     20  0.0048396565  0.0000234223  012011
     21  0.0035458659  0.0000125732  011021
      4 -0.0015922815  0.0000025354  130100

 input the coefficient threshold (end with 0.) [ 0.0000]:
 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 9) export wave function files for cioverlap (all states).
 0) end.

 input menu number [  0]: