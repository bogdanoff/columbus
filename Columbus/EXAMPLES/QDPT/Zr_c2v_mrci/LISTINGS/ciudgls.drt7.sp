1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      2009-03.    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


================ Computing sorting integral file structure ================

                    -----z----- -----y----- -----x----- -----w----- ---total---

                CSFs        28         630        3416        1526        5600
      internal walks       105          90          36          15         246
valid internal walks        28          90          36          15         169
 getinfoarray: info=                     6 :                     1
                  8192                  6552                  8192
                  5460                     0
 icd(3)=                  2976 ci%nnlev=                   595  l2rec=
                  8192  n2max=                  5460
 lcore1,lcore2=              13068137              13044821
 lencor,maxblo              13107200                 60000
========================================
 current settings:
 minbl3         381
 minbl4         381
 locmaxbl3     4288
 locmaxbuf     2144
 maxbl3       60000
 maxbl3       60000
 maxbl4       60000
 maxbuf       30006
========================================

 sorted 4-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)

 sorted 3-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)
 Orig.  diagonal integrals:  1electron:        34
                             0ext.    :        42
                             2ext.    :       336
                             4ext.    :       812


 Orig. off-diag. integrals:  4ext.    :     24411
                             3ext.    :     18720
                             2ext.    :      6792
                             1ext.    :      1272
                             0ext.    :        99
                             2ext. SO :         0
                             1ext. SO :         0
                             0ext. SO :         0
                             1electron:       128


 Sorted integrals            3ext.  w :     17370 x :     16020
                             4ext.  w :     20955 x :     17777


Cycle #  1 sortfile size=    163835(       5 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13068137
Cycle #  2 sortfile size=    163835(       5 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13068137
 minimum size of srtscr:     98301 WP (     3 records)
 maximum size of srtscr:    163835 WP (     5 records)
diagi   file:      4 records  of   1536 WP each=>       6144 WP total
ofdgi   file:      8 records  of   1536 WP each=>      12288 WP total
fil3w   file:      1 records  of  30006 WP each=>      30006 WP total
fil3x   file:      1 records  of  30006 WP each=>      30006 WP total
fil4w   file:      1 records  of  30006 WP each=>      30006 WP total
fil4x   file:      1 records  of  30006 WP each=>      30006 WP total
 compressed index vector length=                    75
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 0,
  GSET = 0,
   DAVCOR =10,
  NCOREL = 4
   nroot=1
  IVMODE = 3
  NBKITR = 1
   nvbkmn=2
  RTOLBK = 1e-3
   niter=30
   nvcimn=2
   rtolci=1e-3
  NVCIMX = 6
  NVRFMX = 6
  NVBKMX = 6
  IDEN  = 1
  CSFPRN = 10,
  update_mode=1
  nvrfmn=2
 /&end
 ------------------------------------------------------------------------
lodens (list->root)=  1
invlodens (root->list)=  1
 USING SEGMENTS OF EQUAL SIZE

****************  list of control variables  ****************
 lvlprt =    0      nroot  =    1      noldv  =   0      noldhv =   0
 nunitv =    1      nbkitr =    1      niter  =  30      davcor =  10
 csfprn =   10      ivmode =    3      istrt  =   0      vout   =   0
 iortls =    0      nvbkmx =    6      ibktv  =  -1      ibkthv =  -1
 nvcimx =    6      icitv  =   -1      icithv =  -1      frcsub =   0
 nvbkmn =    2      nvcimn =    2      maxseg =   4      nrfitr =  30
 ncorel =    4      nvrfmx =    6      nvrfmn =   2      iden   =   1
 itran  =    0      froot  =    0      rtmode =   0      ncouple=   1
 skipso =    F      dalton2=    0      molcas =   0      finalv =   0
 finalw =    0      cosmocalc=   0    with_tsklst=   0
 nsegwx =    1     1     1     1
 nseg0x =    1     1     1     1
 nseg1x =    1     1     1     1
 nseg2x =    1     1     1     1
 nseg3x =    1     1     1     1
 nseg4x =    1     1     1     1
 no0ex  =      0    no1ex  =      0    no2ex  =     0    no3ex  =     0
 no4ex  =      0    nodiag =      0
 cdg4ex =    1      c3ex1ex=    1      c2ex0ex=   1
 fileloc=    0     0     0     0     0     0     0     1     1     1
 directhd=   1      noaqccshift_zyxw=      0
 critical_crit=-1.00000    critical_delta= 0.05000

 ctol   = 0.010000    lrtshift=1.000000    smalld =0.001000


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-03    1.000E-03
 Computing density:                    .drt1.state1
 using                      1  nodes and                      1  cores.
 szdg/szodg per processor=                  6144                 12288
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core           13107199 DP per process

********** Integral sort section *************


 workspace allocation information: lencor=  13107199

 echo of the input for program cisrt:
 ------------------------------------------------------------------------
  &input
  maxbl3=60000
  maxbl4=60000
  &end
 ------------------------------------------------------------------------
 
 ( 6) listing file:                    ciudgls             
 ( 5) input file:                      cisrtin   
 (17) cidrt file:                      cidrtfl             
 (11) transformed integrals file:      moints    
 (12) diagonal integral file:          diagint             
 (13) off-diagonal integral file:      ofdgint             
 (31) 4-external w integrals file:     fil4w               
 (32) 4-external x integrals file:     fil4x               
 (33) 3-external w integrals file:     fil3w               
 (34) 3-external x integrals file:     fil3x               
 (21) scratch da sorting file:         srtscr              
 (12) 2-e integral file [fsplit=2]:    moints2   

 input integral file header information:
                                                                                 
 aoints SIFS file created by argos.      zam792            17:53:52.086 17-Dec-13
  cidrt_title                                                                    
 MO-coefficients from mcscf.x                                                    
  with dummy occupation 1.0 for active orbitals                                  
  total ao core energy =    0.000000000                                          
 MCSCF energy =     -46.301878576                                                
 SIFS file created by program tran.      zam792            17:53:52.407 17-Dec-13

 input energy(*) values:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.

 total core energy =   0.000000000000E+00

 nsym = 4 nmot=  34

 symmetry  =    1    2    3    4
 slabel(*) =   a1   a2   b1   b2
 nmpsy(*)  =   14    4    8    8

 info(*) =          1      8192      6552      8192      5460         0

 orbital labels, i:molab(i)=
   1:tout:001   2:tout:002   3:tout:003   4:tout:004   5:tout:005   6:tout:006   7:tout:007   8:tout:008   9:tout:009  10:tout:010
  11:tout:011  12:tout:012  13:tout:013  14:tout:014  15:tout:015  16:tout:016  17:tout:017  18:tout:018  19:tout:019  20:tout:020
  21:tout:021  22:tout:022  23:tout:023  24:tout:024  25:tout:025  26:tout:026  27:tout:027  28:tout:028  29:tout:029  30:tout:030
  31:tout:031  32:tout:032  33:tout:033  34:tout:034

 input parameters:
 prnopt=  0
 ldamin=    4095 ldamax=   32767 ldainc=      64
 maxbuf=   30006 maxbl3=   60000 maxbl4=   60000 intmxo=    1536
  Using 32 bit compression 

 drt information:
  cidrt_title                                                                    
 nmotd =  38 nfctd =   4 nfvtc =   0 nmot  =  34
 nlevel =  34 niot  =   6 lowinl=  29
 orbital-to-level map(*)
   -1  -1  29  30  31   1   2   3   4   5   6   7   8   9  10  11  32  12  13  14
   -1  33  15  16  17  18  19  20  21  -1  34  22  23  24  25  26  27  28
 compressed map(*)
   29  30  31   1   2   3   4   5   6   7   8   9  10  11  32  12  13  14  33  15
   16  17  18  19  20  21  34  22  23  24  25  26  27  28
 levsym(*)
    1   1   1   1   1   1   1   1   1   1   1   2   2   2   3   3   3   3   3   3
    3   4   4   4   4   4   4   4   1   1   1   2   3   4
 repartitioning mu(*)=
   0.  0.  0.  0.  0.  0.

 new core energy added to the energy(*) list.
 from the integral file: h1_core= -4.349858765627E+01

 indxdg: diagonal integral statistics.
 total number of integrals contributing to diagonal matrix elements:      1190
 number with all external indices:       812
 number with half external - half internal indices:       336
 number with all internal indices:        42

 indxof: off-diagonal integral statistics.
    4-external integrals: num=      24411 strt=          1
    3-external integrals: num=      18720 strt=      24412
    2-external integrals: num=       6792 strt=      43132
    1-external integrals: num=       1272 strt=      49924
    0-external integrals: num=         99 strt=      51196

 total number of off-diagonal integrals:       51294


 indxof(2nd)  ittp=   3 numx(ittp)=        6792
 indxof(2nd)  ittp=   4 numx(ittp)=        1272
 indxof(2nd)  ittp=   5 numx(ittp)=          99

 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg=  12931980
 pro2e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652

 pro2e:     25126 integrals read in     5 records.

 pro2e:         0 integrals 34-ext integrals skipped.
 pro1e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652
 pro1e: eref =    0.000000000000000E+00
 total size of srtscr:                     4  records of                  32767 
 WP =               1048544 Bytes
 putdg        1     596    1191    1786    3322   36089   57934    3018   46706   90394
   123161  131353  136813  158652

 putf:       4 buffers of length    1536 written to file 12
 diagonal integral file completed.

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep4e:    17 blocks of linear combinations of 4-external integrals processed.
 number of sorted 4-external integrals      38732
 number of original 4-external integrals    24411


 putf34: external integral file complete. nfilw=    31 nfilx=    32 nrecw=     1 nrecx=     1 lbufp= 30006

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep3e:    19 blocks of linear combinations of 3-external integrals processed.
 number of sorted 3-external integrals      33390
 number of original 3-external integrals    18720


 putf34: external integral file complete. nfilw=    33 nfilx=    34 nrecw=     1 nrecx=     1 lbufp= 30006
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      6792         3     43132     43132      6792     51294
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      1272         4     49924     49924      1272     51294
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd        99         5     51196     51196        99     51294

 putf:       8 buffers of length    1536 written to file 13
 off-diagonal files sort completed.
 executing brd_struct for cisrtinfo
cisrtinfo:
bufszi  1536
 diagfile 4ext:     812 2ext:     336 0ext:      42
 fil4w,fil4x  :   24411 fil3w,fil3x :   18720
 ofdgint  2ext:    6792 1ext:    1272 0ext:      99so0ext:       0so1ext:       0so2ext:       0
buffer minbl4     381 minbl3     381 maxbl2     384nbas:  11   3   7   7   0   0   0   0 maxbuf 30006
 CIUDG version 5.9.7 ( 5-Oct-2004)

 workspace allocation information: lcore=  13107199

 core energy values from the integral file:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -4.349858765627E+01, ietype=    6,   fcore energy of type: H1(*)   

 total core repulsion energy = -4.349858765627E+01
 nmot  =    38 niot  =     6 nfct  =     4 nfvt  =     0
 nrow  =    36 nsym  =     4 ssym  =     3 lenbuf=  1600
 nwalk,xbar:        246      105       90       36       15
 nvalwt,nvalw:      169       28       90       36       15
 ncsft:            5600
 total number of valid internal walks:     169
 nvalz,nvaly,nvalx,nvalw =       28      90      36      15

 cisrt info file parameters:
 file number  12 blocksize   1536
 mxbld   1536
 nd4ext,nd2ext,nd0ext   812   336    42
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int    24411    18720     6792     1272       99        0        0        0
 minbl4,minbl3,maxbl2   381   381   384
 maxbuf 30006
 number of external orbitals per symmetry block:  11   3   7   7
 nmsym   4 number of internal orbitals   6
 executing brd_struct for drt
 executing brd_struct for orbinf
 executing brd_struct for momap
 calcthrxt: niot,maxw1=                     6                    66
 block size     0
 pthz,pthy,pthx,pthw:   105    90    36    15 total internal walks:     246
 maxlp3,n2lp,n1lp,n0lp    66     0     0     0
 orbsym(*)= 1 1 1 2 3 4

 setref:       28 references kept,
               77 references were marked as invalid, out of
              105 total.
 nmb.of records onel     1
 nmb.of records 2-ext     5
 nmb.of records 1-ext     1
 nmb.of records 0-ext     1
 nmb.of records 2-int     0
 nmb.of records 1-int     0
 nmb.of records 0-int     0
 ---------memory usage in DP -----------------
 < n-ex core usage >
     routines:
    fourex            61797
    threx             60409
    twoex              3549
    onex               1823
    allin              1536
    diagon             2109
               =======
   maximum            61797
 
  __ static summary __ 
   reflst                28
   hrfspc                28
               -------
   static->              28
 
  __ core required  __ 
   totstc                28
   max n-ex           61797
               -------
   totnec->           61825
 
  __ core available __ 
   totspc          13107199
   totnec -           61825
               -------
   totvec->        13045374

 number of external paths / symmetry
 vertex x     100      82      98      98
 vertex w     128      82      98      98
segment: free space=    13045374
 reducing frespc by                   946 to               13044428 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1          55|        28|         0|        28|         0|         1|
 -------------------------------------------------------------------------------
  Y 2          90|       630|        28|        90|        28|         2|
 -------------------------------------------------------------------------------
  X 3          36|      3416|       658|        36|       118|         3|
 -------------------------------------------------------------------------------
  W 4          15|      1526|      4074|        15|       154|         4|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=          76DP  conft+indsym=         360DP  drtbuffer=         510 DP

dimension of the ci-matrix ->>>      5600

 executing brd_struct for civct
 gentasklist: ntask=                    20
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS   
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      36      55       3416         28      36      28
     2  4   1    25      two-ext wz   2X  4 1      15      55       1526         28      15      28
     3  4   3    26      two-ext wx*  WX  4 3      15      36       1526       3416      15      36
     4  4   3    27      two-ext wx+  WX  4 3      15      36       1526       3416      15      36
     5  2   1    11      one-ext yz   1X  2 1      90      55        630         28      90      28
     6  3   2    15      1ex3ex yx    3X  3 2      36      90       3416        630      36      90
     7  4   2    16      1ex3ex yw    3X  4 2      15      90       1526        630      15      90
     8  1   1     1      allint zz    OX  1 1      55      55         28         28      28      28
     9  2   2     5      0ex2ex yy    OX  2 2      90      90        630        630      90      90
    10  3   3     6      0ex2ex xx*   OX  3 3      36      36       3416       3416      36      36
    11  3   3    18      0ex2ex xx+   OX  3 3      36      36       3416       3416      36      36
    12  4   4     7      0ex2ex ww*   OX  4 4      15      15       1526       1526      15      15
    13  4   4    19      0ex2ex ww+   OX  4 4      15      15       1526       1526      15      15
    14  2   2    42      four-ext y   4X  2 2      90      90        630        630      90      90
    15  3   3    43      four-ext x   4X  3 3      36      36       3416       3416      36      36
    16  4   4    44      four-ext w   4X  4 4      15      15       1526       1526      15      15
    17  1   1    75      dg-024ext z  OX  1 1      55      55         28         28      28      28
    18  2   2    76      dg-024ext y  OX  2 2      90      90        630        630      90      90
    19  3   3    77      dg-024ext x  OX  3 3      36      36       3416       3416      36      36
    20  4   4    78      dg-024ext w  OX  4 4      15      15       1526       1526      15      15
----------------------------------------------------------------------------------------------------
REDTASK #   1 TIME=  19.000 N=  1 (task/type/sgbra)=(   1/24/0) (
REDTASK #   2 TIME=  18.000 N=  1 (task/type/sgbra)=(   2/25/0) (
REDTASK #   3 TIME=  17.000 N=  1 (task/type/sgbra)=(   3/26/1) (
REDTASK #   4 TIME=  16.000 N=  1 (task/type/sgbra)=(   4/27/2) (
REDTASK #   5 TIME=  15.000 N=  1 (task/type/sgbra)=(   5/11/0) (
REDTASK #   6 TIME=  14.000 N=  1 (task/type/sgbra)=(   6/15/0) (
REDTASK #   7 TIME=  13.000 N=  1 (task/type/sgbra)=(   7/16/0) (
REDTASK #   8 TIME=  12.000 N=  1 (task/type/sgbra)=(   8/ 1/0) (
REDTASK #   9 TIME=  11.000 N=  1 (task/type/sgbra)=(   9/ 5/0) (
REDTASK #  10 TIME=  10.000 N=  1 (task/type/sgbra)=(  10/ 6/1) (
REDTASK #  11 TIME=   9.000 N=  1 (task/type/sgbra)=(  11/18/2) (
REDTASK #  12 TIME=   8.000 N=  1 (task/type/sgbra)=(  12/ 7/1) (
REDTASK #  13 TIME=   7.000 N=  1 (task/type/sgbra)=(  13/19/2) (
REDTASK #  14 TIME=   6.000 N=  1 (task/type/sgbra)=(  14/42/1) (
REDTASK #  15 TIME=   5.000 N=  1 (task/type/sgbra)=(  15/43/1) (
REDTASK #  16 TIME=   4.000 N=  1 (task/type/sgbra)=(  16/44/1) (
REDTASK #  17 TIME=   3.000 N=  1 (task/type/sgbra)=(  17/75/1) (
REDTASK #  18 TIME=   2.000 N=  1 (task/type/sgbra)=(  18/76/1) (
REDTASK #  19 TIME=   1.000 N=  1 (task/type/sgbra)=(  19/77/1) (
REDTASK #  20 TIME=   0.000 N=  1 (task/type/sgbra)=(  20/78/1) (
 initializing v-file: 1:                  5600

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         216 2x:           0 4x:           0
All internal counts: zz :         356 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 reference space has dimension      28
 dsyevx: computed roots 1 to    2(converged:   2)

    root           eigenvalues
    ----           ------------
       1         -46.3018785769
       2         -46.3018785739

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt=                    28

         vector  1 from unit 11 written to unit 49 filename cirefv              

 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=    28)...
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              5600
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                    1
 residual norm convergence criteria:               0.001000

          starting bk iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         807 2x:         336 4x:         141
All internal counts: zz :         356 yy:           0 xx:           0 ww:           0
One-external counts: yz :        1495 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:         157 wz:         100 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1
   ht   1    -2.80329092

          calcsovref: tciref block   1

              civs   1
 refs   1    1.00000    

          calcsovref: scrb block   1

                ci   1
 civs   1    1.00000    

          calcsovref: sovref block   1

              v      1
 ref    1    1.00000    

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.3018785769 -1.3323E-15  5.1853E-02  2.1139E-01  1.0000E-03
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.3018785769 -1.3323E-15  5.1853E-02  2.1139E-01  1.0000E-03
 
diagon:itrnv=   2
 expansion vectors are not transformed.
 matrix-vector products are not transformed.

    2 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              5600
 number of initial trial vectors:                         2
 number of initial matrix-vector products:                1
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                   30
 residual norm convergence criteria:               0.001000

          starting ci iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         807 2x:         336 4x:         141
All internal counts: zz :         356 yy:         930 xx:         352 ww:          66
One-external counts: yz :        1495 yx:        2405 yw:        1220
Two-external counts: yy :        1285 ww:         180 xx:         682 xz:         157 wz:         100 wx:         510
Three-ext.   counts: yx :         657 yw:         358

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2
   ht   1    -2.80329092
   ht   2    -0.05185312    -0.17921068

          calcsovref: tciref block   1

              civs   1       civs   2
 refs   1    1.00000      -3.377880E-14

          calcsovref: scrb block   1

                ci   1         ci   2
 civs   1   0.971229      -0.238147    
 civs   2   0.819038        3.34026    

          calcsovref: sovref block   1

              v      1       v      2
 ref    1   0.971229      -0.238147    

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97122902    -0.23814742

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.3456063269  4.3728E-02  1.8440E-03  3.9976E-02  1.0000E-03
 mr-sdci #  1  2    -45.5745868355  2.0760E+00  0.0000E+00  5.0151E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   2

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         807 2x:         336 4x:         141
All internal counts: zz :         356 yy:         930 xx:         352 ww:          66
One-external counts: yz :        1495 yx:        2405 yw:        1220
Two-external counts: yy :        1285 ww:         180 xx:         682 xz:         157 wz:         100 wx:         510
Three-ext.   counts: yx :         657 yw:         358

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3
   ht   1    -2.80329092
   ht   2    -0.05185312    -0.17921068
   ht   3     0.00147517    -0.00240782    -0.00636708

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3
 refs   1    1.00000      -3.377880E-14  -5.252391E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3
 civs   1   0.967510      -0.168224       0.189003    
 civs   2   0.853046        1.66384       -2.88657    
 civs   3    1.09067        16.0121        9.64278    

          calcsovref: sovref block   1

              v      1       v      2       v      3
 ref    1   0.966937      -0.176634       0.183939    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.96693689    -0.17663426     0.18393855

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1    -46.3476210581  2.0147E-03  2.4297E-04  1.4156E-02  1.0000E-03
 mr-sdci #  2  2    -45.8136904662  2.3910E-01  0.0000E+00  4.4906E-01  1.0000E-04
 mr-sdci #  2  3    -45.4879961126  1.9894E+00  0.0000E+00  5.1031E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   3

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         807 2x:         336 4x:         141
All internal counts: zz :         356 yy:         930 xx:         352 ww:          66
One-external counts: yz :        1495 yx:        2405 yw:        1220
Two-external counts: yy :        1285 ww:         180 xx:         682 xz:         157 wz:         100 wx:         510
Three-ext.   counts: yx :         657 yw:         358

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4
   ht   1    -2.80329092
   ht   2    -0.05185312    -0.17921068
   ht   3     0.00147517    -0.00240782    -0.00636708
   ht   4    -0.00275537    -0.00222531    -0.00046532    -0.00087996

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4
 refs   1    1.00000      -3.377880E-14  -5.252391E-04   9.951308E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4
 civs   1   0.965088       0.176487       0.195758      -4.217724E-02
 civs   2   0.856460       -1.01428       -3.20302      -3.788870E-02
 civs   3    1.21137       -13.1665        4.51854       -12.5513    
 civs   4   0.923424       -26.6522        15.0987        39.4066    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4
 ref    1   0.965371       0.156880       0.208410       3.629922E-03

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.96537069     0.15688046     0.20841013     0.00362992

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1    -46.3478454693  2.2441E-04  4.1291E-05  6.0931E-03  1.0000E-03
 mr-sdci #  3  2    -45.9726880352  1.5900E-01  0.0000E+00  2.9733E-01  1.0000E-04
 mr-sdci #  3  3    -45.5024124184  1.4416E-02  0.0000E+00  5.1349E-01  1.0000E-04
 mr-sdci #  3  4    -45.3981294798  1.8995E+00  0.0000E+00  5.7355E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   4

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         807 2x:         336 4x:         141
All internal counts: zz :         356 yy:         930 xx:         352 ww:          66
One-external counts: yz :        1495 yx:        2405 yw:        1220
Two-external counts: yy :        1285 ww:         180 xx:         682 xz:         157 wz:         100 wx:         510
Three-ext.   counts: yx :         657 yw:         358

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1    -2.80329092
   ht   2    -0.05185312    -0.17921068
   ht   3     0.00147517    -0.00240782    -0.00636708
   ht   4    -0.00275537    -0.00222531    -0.00046532    -0.00087996
   ht   5     0.00260236    -0.00038701    -0.00027007     0.00002444    -0.00015227

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1    1.00000      -3.377880E-14  -5.252391E-04   9.951308E-04  -9.258620E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 civs   1  -0.965242       0.132275      -0.250454       3.407337E-02  -2.127662E-03
 civs   2  -0.856873      -0.781965        3.07227        1.14195       0.129437    
 civs   3   -1.22970       -11.2787      -0.828816       -3.27687       -15.1934    
 civs   4   -1.06425       -27.6330       -7.49291       -24.7569        33.5846    
 civs   5  -0.829475       -40.5761       -56.4304        87.3254        48.6229    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1  -0.964887       0.148268      -0.205228      -6.969311E-02  -5.744583E-03

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1    -0.96488747     0.14826834    -0.20522802    -0.06969311    -0.00574458

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1    -46.3478797198  3.4251E-05  3.1121E-06  1.6365E-03  1.0000E-03
 mr-sdci #  4  2    -46.0368916407  6.4204E-02  0.0000E+00  1.7398E-01  1.0000E-04
 mr-sdci #  4  3    -45.5224771233  2.0065E-02  0.0000E+00  5.2093E-01  1.0000E-04
 mr-sdci #  4  4    -45.4485814842  5.0452E-02  0.0000E+00  6.5728E-01  1.0000E-04
 mr-sdci #  4  5    -45.3849463844  1.8864E+00  0.0000E+00  5.6202E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   5

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         807 2x:         336 4x:         141
All internal counts: zz :         356 yy:         930 xx:         352 ww:          66
One-external counts: yz :        1495 yx:        2405 yw:        1220
Two-external counts: yy :        1285 ww:         180 xx:         682 xz:         157 wz:         100 wx:         510
Three-ext.   counts: yx :         657 yw:         358

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1    -2.80329092
   ht   2    -0.05185312    -0.17921068
   ht   3     0.00147517    -0.00240782    -0.00636708
   ht   4    -0.00275537    -0.00222531    -0.00046532    -0.00087996
   ht   5     0.00260236    -0.00038701    -0.00027007     0.00002444    -0.00015227
   ht   6     0.00060293    -0.00009053     0.00001775     0.00001795     0.00000041    -0.00001261

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1    1.00000      -3.377880E-14  -5.252391E-04   9.951308E-04  -9.258620E-04  -2.145058E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1  -0.965049       0.133414      -7.202378E-02  -0.225082      -8.626179E-02  -8.629699E-02
 civs   2  -0.856891      -0.611398        1.19376        3.00313      -0.493470       0.601322    
 civs   3   -1.23140       -9.81146        7.18804       -7.34336      -4.877812E-02    12.9787    
 civs   4   -1.07744       -26.1403        5.06084       -5.06477        27.3356       -33.4527    
 civs   5  -0.907184       -47.1339       -29.9194       -10.1818       -85.5613       -65.5679    
 civs   6    1.03117        126.883        359.501       -127.919       -46.7136       -145.286    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.964855       0.128977      -0.120177      -0.189398       3.020464E-02  -3.453212E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.96485539     0.12897696    -0.12017689    -0.18939837     0.03020464    -0.03453212

 trial vector basis is being transformed.  new dimension:   2

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1    -46.3478829290  3.2091E-06  2.7199E-07  4.6991E-04  1.0000E-03
 mr-sdci #  5  2    -46.0641680807  2.7276E-02  0.0000E+00  1.4012E-01  1.0000E-04
 mr-sdci #  5  3    -45.7915154296  2.6904E-01  0.0000E+00  4.4394E-01  1.0000E-04
 mr-sdci #  5  4    -45.4772051296  2.8624E-02  0.0000E+00  5.0548E-01  1.0000E-04
 mr-sdci #  5  5    -45.4457367786  6.0790E-02  0.0000E+00  6.6720E-01  1.0000E-04
 mr-sdci #  5  6    -45.3469030994  1.8483E+00  0.0000E+00  5.7782E-01  1.0000E-04
 
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mr-sdci  convergence criteria satisfied after  5 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1    -46.3478829290  3.2091E-06  2.7199E-07  4.6991E-04  1.0000E-03
 mr-sdci #  5  2    -46.0641680807  2.7276E-02  0.0000E+00  1.4012E-01  1.0000E-04

####################CIUDGINFO####################

   ci vector at position   1 energy=  -46.347882928965

################END OF CIUDGINFO################

 
diagon:itrnv=   0
    1 of the   3 expansion vectors are transformed.
    1 of the   2 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
maximum overlap with reference    1(overlap= 0.96486)

 information on vector: 1 from unit 11 written to unit 48 filename civout              


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -46.3478829290

                                                       internal orbitals

                                          level       1    2    3    4    5    6

                                          orbital     3    4    5   17   22   31

                                         symmetry   a1   a1   a1   a2   b1   b2 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  3  1       1  0.152287                        +-   +              +       
 z*  3  1       2 -0.649710                        +-        +         +       
 z*  3  1       3  0.681733                        +-             +         +  
 z*  3  1       4 -0.017114                        +    +-             +       
 z*  3  1       5  0.023856                        +     -   +         +       
 z*  3  1       6 -0.012776                        +     -        +         +  
 z*  3  1       7  0.057406                        +    +     -        +       
 z*  3  1       8  0.030138                        +    +    +          -      
 z*  3  1       9  0.014949                        +    +          -        +  
 z*  3  1      10  0.045459                        +    +         +          - 
 z*  3  1      11 -0.025737                        +         +-        +       
 z*  3  1      12 -0.034940                        +          -   +         +  
 z*  3  1      13 -0.015970                        +         +     -        +  
 z*  3  1      14  0.061113                        +         +    +          - 
 z*  3  1      16 -0.043589                        +                   +    +- 
 z*  3  1      17  0.030722                             +-   +         +       
 z*  3  1      18 -0.033985                             +-        +         +  
 z*  3  1      19  0.012432                             +    +-        +       
 z*  3  1      25 -0.023795                                  +-   +         +  
 z*  3  1      26 -0.031774                                  +    +-   +       
 z*  3  1      27  0.024227                                  +         +    +- 
 z*  3  1      28  0.033340                                       +    +-   +  
 y   3  1     103  0.014621              5( a1 )    -        +         +       
 y   3  1     120 -0.015362              5( a1 )    -             +         +  
 y   3  1     215  0.020302              5( a1 )   +          -        +       
 y   3  1     236 -0.014358              5( a1 )   +         +          -      
 y   3  1     260 -0.021330              5( a1 )   +               -        +  
 y   3  1     274  0.015088              5( a1 )   +              +          - 
 y   3  1     350 -0.011018              7( a1 )         -   +         +       
 y   3  1     366  0.010003              6( a1 )         -        +         +  
 y   3  1     541 -0.011451              2( a2 )             +     -   +       
 y   3  1     627  0.011874              4( b1 )                  +     -   +  
 x   3  1     697 -0.015629    6( a1 )   4( b1 )   +-                          
 x   3  1     722 -0.014727    9( a1 )   6( b1 )   +-                          
 x   3  1     734 -0.014766   10( a1 )   7( b1 )   +-                          
 x   3  1     740  0.010605    2( a2 )   2( b2 )   +-                          
 x   3  1     745  0.010606    1( a2 )   4( b2 )   +-                          
 x   3  1     746 -0.018626    2( a2 )   4( b2 )   +-                          
 x   3  1     756  0.013846    3( a2 )   7( b2 )   +-                          
 x   3  1     863  0.010665    9( a1 )   1( b1 )    -        +                 
 x   3  1     870 -0.013865    5( a1 )   2( b1 )    -        +                 
 x   3  1     885  0.011080    9( a1 )   3( b1 )    -        +                 
 x   3  1     892  0.016165    5( a1 )   4( b1 )    -        +                 
 x   3  1     910  0.013059    1( a1 )   6( b1 )    -        +                 
 x   3  1     913  0.013568    4( a1 )   6( b1 )    -        +                 
 x   3  1     934 -0.010224    3( a2 )   1( b2 )    -        +                 
 x   3  1     940 -0.010610    3( a2 )   3( b2 )    -        +                 
 x   3  1     955  0.011041    3( a2 )   1( b1 )    -             +            
 x   3  1     961  0.011528    3( a2 )   3( b1 )    -             +            
 x   3  1     983  0.012207   10( a1 )   1( b2 )    -             +            
 x   3  1     989  0.014565    5( a1 )   2( b2 )    -             +            
 x   3  1    1005  0.012699   10( a1 )   3( b2 )    -             +            
 x   3  1    1011 -0.016983    5( a1 )   4( b2 )    -             +            
 x   3  1    1029 -0.014098    1( a1 )   6( b2 )    -             +            
 x   3  1    1032 -0.014620    4( a1 )   6( b2 )    -             +            
 x   3  1    1058 -0.012991    2( a1 )   5( a1 )    -                  +       
 x   3  1    1065  0.014255    5( a1 )   6( a1 )    -                  +       
 x   3  1    1082  0.010339    4( a1 )   9( a1 )    -                  +       
 x   3  1    1140  0.011754    1( b2 )   6( b2 )    -                  +       
 x   3  1    1142  0.012146    3( b2 )   6( b2 )    -                  +       
 x   3  1    1155 -0.014565    5( a1 )   1( a2 )    -                       +  
 x   3  1    1166  0.016983    5( a1 )   2( a2 )    -                       +  
 x   3  1    1173  0.010884    1( a1 )   3( a2 )    -                       +  
 x   3  1    1176  0.011282    4( a1 )   3( a2 )    -                       +  
 x   3  1    1190  0.013333    7( b1 )   1( b2 )    -                       +  
 x   3  1    1204  0.013873    7( b1 )   3( b2 )    -                       +  
 x   3  1    1226  0.013044    1( b1 )   7( b2 )    -                       +  
 x   3  1    1228  0.013570    3( b1 )   7( b2 )    -                       +  
 w   3  1    4173  0.036694    1( a1 )   1( b1 )   +         +                 
 w   3  1    4176  0.028088    4( a1 )   1( b1 )   +         +                 
 w   3  1    4195  0.028207    1( a1 )   3( b1 )   +         +                 
 w   3  1    4198  0.023075    4( a1 )   3( b1 )   +         +                 
 w   3  1    4231 -0.010356    4( a1 )   6( b1 )   +         +                 
 w   3  1    4279  0.010040    3( a2 )   3( b1 )   +              +            
 w   3  1    4292 -0.038522    1( a1 )   1( b2 )   +              +            
 w   3  1    4295 -0.029555    4( a1 )   1( b2 )   +              +            
 w   3  1    4314 -0.029550    1( a1 )   3( b2 )   +              +            
 w   3  1    4317 -0.024229    4( a1 )   3( b2 )   +              +            
 w   3  1    4347  0.012105    1( a1 )   6( b2 )   +              +            
 w   3  1    4350  0.012831    4( a1 )   6( b2 )   +              +            
 w   3  1    4369  0.026769    1( a1 )   1( a1 )   +                   +       
 w   3  1    4375  0.029045    1( a1 )   4( a1 )   +                   +       
 w   3  1    4378  0.016848    4( a1 )   4( a1 )   +                   +       
 w   3  1    4469 -0.026422    1( b2 )   1( b2 )   +                   +       
 w   3  1    4472 -0.028667    1( b2 )   3( b2 )   +                   +       
 w   3  1    4474 -0.016623    3( b2 )   3( b2 )   +                   +       
 w   3  1    4484 -0.010971    1( b2 )   6( b2 )   +                   +       
 w   3  1    4486 -0.011579    3( b2 )   6( b2 )   +                   +       
 w   3  1    4530  0.038532    1( b1 )   1( b2 )   +                        +  
 w   3  1    4532  0.029622    3( b1 )   1( b2 )   +                        +  
 w   3  1    4544  0.029496    1( b1 )   3( b2 )   +                        +  
 w   3  1    4546  0.024233    3( b1 )   3( b2 )   +                        +  
 w   3  1    4550  0.010127    7( b1 )   3( b2 )   +                        +  
 w   3  1    4572 -0.010385    1( b1 )   7( b2 )   +                        +  
 w   3  1    4574 -0.011055    3( b1 )   7( b2 )   +                        +  
 w   3  1    4847 -0.012204    1( b1 )   1( b1 )        +              +       
 w   3  1    4850 -0.010402    1( b1 )   3( b1 )        +              +       
 w   3  1    4875 -0.015801    1( b2 )   1( b2 )        +              +       
 w   3  1    4878 -0.013830    1( b2 )   3( b2 )        +              +       
 w   3  1    5083  0.047499    1( a1 )   1( a1 )             +         +       
 w   3  1    5089  0.040431    1( a1 )   4( a1 )             +         +       
 w   3  1    5092  0.020530    4( a1 )   4( a1 )             +         +       
 w   3  1    5097  0.016317    5( a1 )   5( a1 )             +         +       
 w   3  1    5155  0.052476    1( b1 )   1( b1 )             +         +       
 w   3  1    5158  0.044825    1( b1 )   3( b1 )             +         +       
 w   3  1    5160  0.022652    3( b1 )   3( b1 )             +         +       
 w   3  1    5183  0.051848    1( b2 )   1( b2 )             +         +       
 w   3  1    5186  0.044381    1( b2 )   3( b2 )             +         +       
 w   3  1    5188  0.022473    3( b2 )   3( b2 )             +         +       
 w   3  1    5375 -0.055244    1( a1 )   1( a1 )                  +         +  
 w   3  1    5381 -0.047341    1( a1 )   4( a1 )                  +         +  
 w   3  1    5384 -0.023976    4( a1 )   4( a1 )                  +         +  
 w   3  1    5389 -0.017147    5( a1 )   5( a1 )                  +         +  
 w   3  1    5447 -0.055106    1( b1 )   1( b1 )                  +         +  
 w   3  1    5450 -0.047059    1( b1 )   3( b1 )                  +         +  
 w   3  1    5452 -0.023791    3( b1 )   3( b1 )                  +         +  
 w   3  1    5475 -0.049170    1( b2 )   1( b2 )                  +         +  
 w   3  1    5478 -0.041762    1( b2 )   3( b2 )                  +         +  
 w   3  1    5480 -0.021223    3( b2 )   3( b2 )                  +         +  

 ci coefficient statistics:
           rq > 0.1                3
      0.1> rq > 0.01             116
     0.01> rq > 0.001            781
    0.001> rq > 0.0001          1322
   0.0001> rq > 0.00001          460
  0.00001> rq > 0.000001          68
 0.000001> rq                   2850
           all                  5600
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         216 2x:           0 4x:           0
All internal counts: zz :         356 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.152286510138     -7.051149461010
     2     2     -0.649709712742     30.082778601266
     3     3      0.681732776286    -31.565510333755
     4     4     -0.017113965356      0.792532155840
     5     5      0.023855816198     -1.104699530117
     6     6     -0.012775543498      0.591582920398
     7     7      0.057405854758     -2.658428264455
     8     8      0.030138134242     -1.395661176857
     9     9      0.014949475428     -0.692269727729
    10    10      0.045458941433     -2.105149318344
    11    11     -0.025737467914      1.191883011201
    12    12     -0.034939759207      1.617997657436
    13    13     -0.015970389654      0.739571649755
    14    14      0.061113203287     -2.830092988190
    15    15     -0.000667713645      0.030917986834
    16    16     -0.043589422707      2.018572110114
    17    17      0.030721644240     -1.421842272969
    18    18     -0.033984983007      1.572906175221
    19    19      0.012431618725     -0.575467115699
    20    20      0.000727177713     -0.033671566494
    21    21      0.001980996725     -0.091727902290
    22    22     -0.005887756451      0.272602756326
    23    23      0.007271221076     -0.336534852673
    24    24     -0.000534306499      0.024607887256
    25    25     -0.023794724019      1.101070847809
    26    26     -0.031773685200      1.470559059826
    27    27      0.024226751362     -1.121134485575
    28    28      0.033339795378     -1.543037107421

 number of reference csfs (nref) is    28.  root number (iroot) is  1.
 c0**2 =   0.93098583  c**2 (all zwalks) =   0.93098583

 pople ci energy extrapolation is computed with  4 correlated electrons.

 eref      =    -46.301867338747   "relaxed" cnot**2         =   0.930985830614
 eci       =    -46.347882928965   deltae = eci - eref       =  -0.046015590218
 eci+dv1   =    -46.351058656703   dv1 = (1-cnot**2)*deltae  =  -0.003175727738
 eci+dv2   =    -46.351294074047   dv2 = dv1 / cnot**2       =  -0.003411145082
 eci+dv3   =    -46.351567188994   dv3 = dv1 / (2*cnot**2-1) =  -0.003684260029
 eci+pople =    -46.349586164769   (  4e- scaled deltae )    =  -0.047718826022
 passed aftci ... 
 readint2: molcas,dalton2=                     0                     0
 files%faoints=aoints              
lodens (list->root)=  1
sifcfg setup: record length 4096 DP
# d1 elements per record  3272
# d2 elements per record  2730
  The MR-CISD density will be calculated.
 item #                     1 suffix=:.drt1.state1:
 read_civout: repnuc=  -43.4985876562748     
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method:  0 last record  1max overlap with ref# 96% root-following 0
 MR-CISD energy:   -46.34788293    -2.84929527
 residuum:     0.00046991
 deltae:     0.00000321
 apxde:     0.00000027

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.96485539     0.12897696    -0.12017689    -0.18939837     0.03020464    -0.03453212     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.96485539     0.12897696     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 computing final density
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=     137  DYX=     275  DYW=     150
   D0Z=      37  D0Y=     125  D0X=      38  D0W=      12
  DDZI=      96 DDYI=     240 DDXI=      66 DDWI=      30
  DDZE=       0 DDYE=      90 DDXE=      36 DDWE=      15
================================================================================
Trace of MO density:     4.000000
    4  correlated and     8  frozen core electrons

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     1.86973802    -0.03544007    -0.00848321     0.00000000     0.00314665    -0.00707408
   MO   4     0.00000000     0.00000000    -0.03544007     0.03870922    -0.10500508     0.00000000    -0.00163790     0.00109798
   MO   5     0.00000000     0.00000000    -0.00848321    -0.10500508     0.46248553     0.00000000     0.00394822     0.00221554
   MO   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.02361177     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000     0.00314665    -0.00163790     0.00394822     0.00000000     0.00078089     0.00017543
   MO   8     0.00000000     0.00000000    -0.00707408     0.00109798     0.00221554     0.00000000     0.00017543     0.00046949
   MO   9     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.01659611     0.00000000     0.00000000
   MO  10     0.00000000     0.00000000    -0.02929601    -0.00056212    -0.00013356     0.00000000     0.00001758    -0.00003897
   MO  11     0.00000000     0.00000000     0.00478879    -0.00206108     0.00368918     0.00000000     0.00088173     0.00014540
   MO  12     0.00000000     0.00000000     0.00806916    -0.00136734    -0.00283035     0.00000000    -0.00031582    -0.00056236
   MO  13     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00349835     0.00000000     0.00000000
   MO  14     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00063988     0.00000000     0.00000000
   MO  15     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00042101     0.00000000     0.00000000
   MO  16     0.00000000     0.00000000    -0.00022823     0.00004213     0.00001004     0.00000000    -0.00000297     0.00000665

                MO   9         MO  10         MO  11         MO  12         MO  13         MO  14         MO  15         MO  16
   MO   3     0.00000000    -0.02929601     0.00478879     0.00806916     0.00000000     0.00000000     0.00000000    -0.00022823
   MO   4     0.00000000    -0.00056212    -0.00206108    -0.00136734     0.00000000     0.00000000     0.00000000     0.00004213
   MO   5     0.00000000    -0.00013356     0.00368918    -0.00283035     0.00000000     0.00000000     0.00000000     0.00001004
   MO   6     0.01659611     0.00000000     0.00000000     0.00000000     0.00349835     0.00063988     0.00042101     0.00000000
   MO   7     0.00000000     0.00001758     0.00088173    -0.00031582     0.00000000     0.00000000     0.00000000    -0.00000297
   MO   8     0.00000000    -0.00003897     0.00014540    -0.00056236     0.00000000     0.00000000     0.00000000     0.00000665
   MO   9     0.01204173     0.00000000     0.00000000     0.00000000     0.00281066     0.00051287     0.00033340     0.00000000
   MO  10     0.00000000     0.00552299     0.00002925     0.00004936     0.00000000     0.00000000     0.00000000    -0.00025886
   MO  11     0.00000000     0.00002925     0.00111526    -0.00033979     0.00000000     0.00000000     0.00000000    -0.00000518
   MO  12     0.00000000     0.00004936    -0.00033979     0.00075556     0.00000000     0.00000000     0.00000000    -0.00000873
   MO  13     0.00281066     0.00000000     0.00000000     0.00000000     0.00087024     0.00014972     0.00009296     0.00000000
   MO  14     0.00051287     0.00000000     0.00000000     0.00000000     0.00014972     0.00125102    -0.00019975     0.00000000
   MO  15     0.00033340     0.00000000     0.00000000     0.00000000     0.00009296    -0.00019975     0.00138263     0.00000000
   MO  16     0.00000000    -0.00025886    -0.00000518    -0.00000873     0.00000000     0.00000000     0.00000000     0.00001731

Natural orbital populations,block 1
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     2.00000000     1.87099279     0.48716757     0.03600220     0.01408566     0.00492212     0.00203370
              MO     9       MO    10       MO    11       MO    12       MO    13       MO    14       MO    15       MO    16
  occ(*)=     0.00152715     0.00108824     0.00052906     0.00032090     0.00005538     0.00001265     0.00001075     0.00000352

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4
   MO   1     0.50473386     0.00439795    -0.00432678     0.00000000
   MO   2     0.00439795     0.00083416    -0.00094647     0.00000000
   MO   3    -0.00432678    -0.00094647     0.00121940     0.00000000
   MO   4     0.00000000     0.00000000     0.00000000     0.00176179

Natural orbital populations,block 2
              MO     1       MO     2       MO     3       MO     4       MO
  occ(*)=     0.50480956     0.00191788     0.00176179     0.00005999

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     0.48409507     0.00000000     0.00427626     0.00000000    -0.00423861     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.02278203     0.00000000     0.01571102     0.00000000     0.00314027    -0.00056322
   MO   4     0.00000000     0.00427626     0.00000000     0.00081433     0.00000000    -0.00092405     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.01571102     0.00000000     0.01118095     0.00000000     0.00249367    -0.00053881
   MO   6     0.00000000    -0.00423861     0.00000000    -0.00092405     0.00000000     0.00118928     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000     0.00314027     0.00000000     0.00249367     0.00000000     0.00076831    -0.00024917
   MO   8     0.00000000     0.00000000    -0.00056322     0.00000000    -0.00053881     0.00000000    -0.00024917     0.00131806
   MO   9     0.00000000     0.00000000     0.00071641     0.00000000     0.00068138     0.00000000     0.00031147     0.00046910

                MO   9
   MO   2     0.00000000
   MO   3     0.00071641
   MO   4     0.00000000
   MO   5     0.00068138
   MO   6     0.00000000
   MO   7     0.00031147
   MO   8     0.00046910
   MO   9     0.00120274

Natural orbital populations,block 3
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     0.48417024     0.03425729     0.00187036     0.00173304     0.00095724     0.00029445     0.00005808
              MO     9       MO
  occ(*)=     0.00001007

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     0.50768361     0.00000000     0.00475952     0.00000000    -0.00482979     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.02363000     0.00000000     0.01661395     0.00000000    -0.00350569     0.00023234
   MO   4     0.00000000     0.00475952     0.00000000     0.00087937     0.00000000    -0.00102907     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.01661395     0.00000000     0.01205837     0.00000000    -0.00281700     0.00018415
   MO   6     0.00000000    -0.00482979     0.00000000    -0.00102907     0.00000000     0.00134830     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000    -0.00350569     0.00000000    -0.00281700     0.00000000     0.00087235    -0.00005145
   MO   8     0.00000000     0.00000000     0.00023234     0.00000000     0.00018415     0.00000000    -0.00005145     0.00168068
   MO   9     0.00000000     0.00000000    -0.00080636     0.00000000    -0.00064977     0.00000000     0.00019325     0.00017752

                MO   9
   MO   2     0.00000000
   MO   3    -0.00080636
   MO   4     0.00000000
   MO   5    -0.00064977
   MO   6     0.00000000
   MO   7     0.00019325
   MO   8     0.00017752
   MO   9     0.00119566

Natural orbital populations,block 4
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     0.50777455     0.03604455     0.00207938     0.00173871     0.00111704     0.00052599     0.00005736
              MO     9       MO
  occ(*)=     0.00001077


 total number of electrons =   12.0000000000

 test slabel:                     4
  a1  a2  b1  b2 


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        a1  partial gross atomic populations
   ao class       1a1        2a1        3a1        4a1        5a1        6a1 
     3_ s       0.000000   2.000000   1.870192   0.000000   0.000000   0.000243
     3_ p       2.000000   0.000000   0.000000   0.000000   0.035973   0.000000
     3_ d       0.000000   0.000000   0.000800   0.487168   0.000000   0.013842
     3_ f       0.000000   0.000000   0.000000   0.000000   0.000029   0.000000
 
   ao class       7a1        8a1        9a1       10a1       11a1       12a1 
     3_ s       0.004839   0.000000   0.000000   0.000000   0.000000   0.000000
     3_ p       0.000000   0.000000   0.000000   0.000020   0.000520   0.000000
     3_ d       0.000083   0.002034   0.000000   0.000000   0.000000   0.000321
     3_ f       0.000000   0.000000   0.001527   0.001068   0.000009   0.000000
 
   ao class      13a1       14a1       15a1       16a1 
     3_ s       0.000000   0.000000   0.000000   0.000003
     3_ p       0.000000   0.000000   0.000011   0.000000
     3_ d       0.000055   0.000012   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000000

                        a2  partial gross atomic populations
   ao class       1a2        2a2        3a2        4a2 
     3_ d       0.504810   0.001918   0.000000   0.000060
     3_ f       0.000000   0.000000   0.001762   0.000000

                        b1  partial gross atomic populations
   ao class       1b1        2b1        3b1        4b1        5b1        6b1 
     3_ p       2.000000   0.000000   0.034207   0.000000   0.000000   0.000316
     3_ d       0.000000   0.484170   0.000000   0.001870   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000050   0.000000   0.001733   0.000642
 
   ao class       7b1        8b1        9b1 
     3_ p       0.000198   0.000000   0.000010
     3_ d       0.000000   0.000058   0.000000
     3_ f       0.000096   0.000000   0.000000

                        b2  partial gross atomic populations
   ao class       1b2        2b2        3b2        4b2        5b2        6b2 
     3_ p       2.000000   0.000000   0.036009   0.000000   0.000000   0.000027
     3_ d       0.000000   0.507775   0.000000   0.002079   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000035   0.000000   0.001739   0.001090
 
   ao class       7b2        8b2        9b2 
     3_ p       0.000514   0.000000   0.000011
     3_ d       0.000000   0.000057   0.000000
     3_ f       0.000012   0.000000   0.000000


                        gross atomic populations
     ao            3_
      s         3.875278
      p         6.107816
      d         2.007113
      f         0.009793
    total      12.000000
 

 Total number of electrons:   12.00000000

 DA ...
