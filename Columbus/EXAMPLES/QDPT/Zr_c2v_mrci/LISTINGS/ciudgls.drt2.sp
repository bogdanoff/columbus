1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      2009-03.    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


================ Computing sorting integral file structure ================

                    -----z----- -----y----- -----x----- -----w----- ---total---

                CSFs        24         482        1430        2034        3970
      internal walks       105          70          15          21         211
valid internal walks        24          70          15          21         130
 getinfoarray: info=                     6 :                     1
                  8192                  6552                  8192
                  5460                     0
 icd(3)=                  2976 ci%nnlev=                   595  l2rec=
                  8192  n2max=                  5460
 lcore1,lcore2=              13068137              13044821
 lencor,maxblo              13107200                 60000
========================================
 current settings:
 minbl3         381
 minbl4         381
 locmaxbl3     4288
 locmaxbuf     2144
 maxbl3       60000
 maxbl3       60000
 maxbl4       60000
 maxbuf       30006
========================================

 sorted 4-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)

 sorted 3-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)
 Orig.  diagonal integrals:  1electron:        34
                             0ext.    :        42
                             2ext.    :       336
                             4ext.    :       812


 Orig. off-diag. integrals:  4ext.    :     24411
                             3ext.    :     18720
                             2ext.    :      6792
                             1ext.    :      1272
                             0ext.    :        99
                             2ext. SO :         0
                             1ext. SO :         0
                             0ext. SO :         0
                             1electron:       128


 Sorted integrals            3ext.  w :     17370 x :     16020
                             4ext.  w :     20955 x :     17777


Cycle #  1 sortfile size=    163835(       5 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13068137
Cycle #  2 sortfile size=    163835(       5 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13068137
 minimum size of srtscr:     98301 WP (     3 records)
 maximum size of srtscr:    163835 WP (     5 records)
diagi   file:      4 records  of   1536 WP each=>       6144 WP total
ofdgi   file:      8 records  of   1536 WP each=>      12288 WP total
fil3w   file:      1 records  of  30006 WP each=>      30006 WP total
fil3x   file:      1 records  of  30006 WP each=>      30006 WP total
fil4w   file:      1 records  of  30006 WP each=>      30006 WP total
fil4x   file:      1 records  of  30006 WP each=>      30006 WP total
 compressed index vector length=                    61
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 0,
  GSET = 0,
   DAVCOR =10,
  NCOREL = 4
   nroot=1
  IVMODE = 3
  NBKITR = 1
   nvbkmn=2
  RTOLBK = 1e-3
   niter=30
   nvcimn=2
   rtolci=1e-3
  NVCIMX = 6
  NVRFMX = 6
  NVBKMX = 6
  IDEN  = 1
  CSFPRN = 10,
  update_mode=1
  nvrfmn=2
 /&end
 ------------------------------------------------------------------------
lodens (list->root)=  1
invlodens (root->list)=  1
 USING SEGMENTS OF EQUAL SIZE

****************  list of control variables  ****************
 lvlprt =    0      nroot  =    1      noldv  =   0      noldhv =   0
 nunitv =    1      nbkitr =    1      niter  =  30      davcor =  10
 csfprn =   10      ivmode =    3      istrt  =   0      vout   =   0
 iortls =    0      nvbkmx =    6      ibktv  =  -1      ibkthv =  -1
 nvcimx =    6      icitv  =   -1      icithv =  -1      frcsub =   0
 nvbkmn =    2      nvcimn =    2      maxseg =   4      nrfitr =  30
 ncorel =    4      nvrfmx =    6      nvrfmn =   2      iden   =   1
 itran  =    0      froot  =    0      rtmode =   0      ncouple=   1
 skipso =    F      dalton2=    0      molcas =   0      finalv =   0
 finalw =    0      cosmocalc=   0    with_tsklst=   0
 nsegwx =    1     1     1     1
 nseg0x =    1     1     1     1
 nseg1x =    1     1     1     1
 nseg2x =    1     1     1     1
 nseg3x =    1     1     1     1
 nseg4x =    1     1     1     1
 no0ex  =      0    no1ex  =      0    no2ex  =     0    no3ex  =     0
 no4ex  =      0    nodiag =      0
 cdg4ex =    1      c3ex1ex=    1      c2ex0ex=   1
 fileloc=    0     0     0     0     0     0     0     1     1     1
 directhd=   1      noaqccshift_zyxw=      0
 critical_crit=-1.00000    critical_delta= 0.05000

 ctol   = 0.010000    lrtshift=1.000000    smalld =0.001000


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-03    1.000E-03
 Computing density:                    .drt1.state1
 using                      1  nodes and                      1  cores.
 szdg/szodg per processor=                  6144                 12288
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core           13107199 DP per process

********** Integral sort section *************


 workspace allocation information: lencor=  13107199

 echo of the input for program cisrt:
 ------------------------------------------------------------------------
  &input
  maxbl3=60000
  maxbl4=60000
  &end
 ------------------------------------------------------------------------
 
 ( 6) listing file:                    ciudgls             
 ( 5) input file:                      cisrtin   
 (17) cidrt file:                      cidrtfl             
 (11) transformed integrals file:      moints    
 (12) diagonal integral file:          diagint             
 (13) off-diagonal integral file:      ofdgint             
 (31) 4-external w integrals file:     fil4w               
 (32) 4-external x integrals file:     fil4x               
 (33) 3-external w integrals file:     fil3w               
 (34) 3-external x integrals file:     fil3x               
 (21) scratch da sorting file:         srtscr              
 (12) 2-e integral file [fsplit=2]:    moints2   

 input integral file header information:
                                                                                 
 aoints SIFS file created by argos.      zam792            17:53:52.086 17-Dec-13
  cidrt_title                                                                    
 MO-coefficients from mcscf.x                                                    
  with dummy occupation 1.0 for active orbitals                                  
  total ao core energy =    0.000000000                                          
 MCSCF energy =     -46.301878576                                                
 SIFS file created by program tran.      zam792            17:53:52.407 17-Dec-13

 input energy(*) values:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.

 total core energy =   0.000000000000E+00

 nsym = 4 nmot=  34

 symmetry  =    1    2    3    4
 slabel(*) =   a1   a2   b1   b2
 nmpsy(*)  =   14    4    8    8

 info(*) =          1      8192      6552      8192      5460         0

 orbital labels, i:molab(i)=
   1:tout:001   2:tout:002   3:tout:003   4:tout:004   5:tout:005   6:tout:006   7:tout:007   8:tout:008   9:tout:009  10:tout:010
  11:tout:011  12:tout:012  13:tout:013  14:tout:014  15:tout:015  16:tout:016  17:tout:017  18:tout:018  19:tout:019  20:tout:020
  21:tout:021  22:tout:022  23:tout:023  24:tout:024  25:tout:025  26:tout:026  27:tout:027  28:tout:028  29:tout:029  30:tout:030
  31:tout:031  32:tout:032  33:tout:033  34:tout:034

 input parameters:
 prnopt=  0
 ldamin=    4095 ldamax=   32767 ldainc=      64
 maxbuf=   30006 maxbl3=   60000 maxbl4=   60000 intmxo=    1536
  Using 32 bit compression 

 drt information:
  cidrt_title                                                                    
 nmotd =  38 nfctd =   4 nfvtc =   0 nmot  =  34
 nlevel =  34 niot  =   6 lowinl=  29
 orbital-to-level map(*)
   -1  -1  29  30  31   1   2   3   4   5   6   7   8   9  10  11  32  12  13  14
   -1  33  15  16  17  18  19  20  21  -1  34  22  23  24  25  26  27  28
 compressed map(*)
   29  30  31   1   2   3   4   5   6   7   8   9  10  11  32  12  13  14  33  15
   16  17  18  19  20  21  34  22  23  24  25  26  27  28
 levsym(*)
    1   1   1   1   1   1   1   1   1   1   1   2   2   2   3   3   3   3   3   3
    3   4   4   4   4   4   4   4   1   1   1   2   3   4
 repartitioning mu(*)=
   0.  0.  0.  0.  0.  0.

 new core energy added to the energy(*) list.
 from the integral file: h1_core= -4.349858765627E+01

 indxdg: diagonal integral statistics.
 total number of integrals contributing to diagonal matrix elements:      1190
 number with all external indices:       812
 number with half external - half internal indices:       336
 number with all internal indices:        42

 indxof: off-diagonal integral statistics.
    4-external integrals: num=      24411 strt=          1
    3-external integrals: num=      18720 strt=      24412
    2-external integrals: num=       6792 strt=      43132
    1-external integrals: num=       1272 strt=      49924
    0-external integrals: num=         99 strt=      51196

 total number of off-diagonal integrals:       51294


 indxof(2nd)  ittp=   3 numx(ittp)=        6792
 indxof(2nd)  ittp=   4 numx(ittp)=        1272
 indxof(2nd)  ittp=   5 numx(ittp)=          99

 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg=  12931980
 pro2e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652

 pro2e:     25126 integrals read in     5 records.

 pro2e:         0 integrals 34-ext integrals skipped.
 pro1e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652
 pro1e: eref =    0.000000000000000E+00
 total size of srtscr:                     4  records of                  32767 
 WP =               1048544 Bytes
 putdg        1     596    1191    1786    3322   36089   57934    3018   46706   90394
   123161  131353  136813  158652

 putf:       4 buffers of length    1536 written to file 12
 diagonal integral file completed.

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep4e:    17 blocks of linear combinations of 4-external integrals processed.
 number of sorted 4-external integrals      38732
 number of original 4-external integrals    24411


 putf34: external integral file complete. nfilw=    31 nfilx=    32 nrecw=     1 nrecx=     1 lbufp= 30006

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep3e:    19 blocks of linear combinations of 3-external integrals processed.
 number of sorted 3-external integrals      33390
 number of original 3-external integrals    18720


 putf34: external integral file complete. nfilw=    33 nfilx=    34 nrecw=     1 nrecx=     1 lbufp= 30006
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      6792         3     43132     43132      6792     51294
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      1272         4     49924     49924      1272     51294
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd        99         5     51196     51196        99     51294

 putf:       8 buffers of length    1536 written to file 13
 off-diagonal files sort completed.
 executing brd_struct for cisrtinfo
cisrtinfo:
bufszi  1536
 diagfile 4ext:     812 2ext:     336 0ext:      42
 fil4w,fil4x  :   24411 fil3w,fil3x :   18720
 ofdgint  2ext:    6792 1ext:    1272 0ext:      99so0ext:       0so1ext:       0so2ext:       0
buffer minbl4     381 minbl3     381 maxbl2     384nbas:  11   3   7   7   0   0   0   0 maxbuf 30006
 CIUDG version 5.9.7 ( 5-Oct-2004)

 workspace allocation information: lcore=  13107199

 core energy values from the integral file:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -4.349858765627E+01, ietype=    6,   fcore energy of type: H1(*)   

 total core repulsion energy = -4.349858765627E+01
 nmot  =    38 niot  =     6 nfct  =     4 nfvt  =     0
 nrow  =    32 nsym  =     4 ssym  =     2 lenbuf=  1600
 nwalk,xbar:        211      105       70       15       21
 nvalwt,nvalw:      130       24       70       15       21
 ncsft:            3970
 total number of valid internal walks:     130
 nvalz,nvaly,nvalx,nvalw =       24      70      15      21

 cisrt info file parameters:
 file number  12 blocksize   1536
 mxbld   1536
 nd4ext,nd2ext,nd0ext   812   336    42
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int    24411    18720     6792     1272       99        0        0        0
 minbl4,minbl3,maxbl2   381   381   384
 maxbuf 30006
 number of external orbitals per symmetry block:  11   3   7   7
 nmsym   4 number of internal orbitals   6
 executing brd_struct for drt
 executing brd_struct for orbinf
 executing brd_struct for momap
 calcthrxt: niot,maxw1=                     6                    31
 block size     0
 pthz,pthy,pthx,pthw:   105    70    15    21 total internal walks:     211
 maxlp3,n2lp,n1lp,n0lp    31     0     0     0
 orbsym(*)= 1 1 1 2 3 4

 setref:       24 references kept,
               81 references were marked as invalid, out of
              105 total.
 nmb.of records onel     1
 nmb.of records 2-ext     5
 nmb.of records 1-ext     1
 nmb.of records 0-ext     1
 nmb.of records 2-int     0
 nmb.of records 1-int     0
 nmb.of records 0-int     0
 ---------memory usage in DP -----------------
 < n-ex core usage >
     routines:
    fourex            61797
    threx             60199
    twoex              3549
    onex               1823
    allin              1536
    diagon             2109
               =======
   maximum            61797
 
  __ static summary __ 
   reflst                24
   hrfspc                24
               -------
   static->              24
 
  __ core required  __ 
   totstc                24
   max n-ex           61797
               -------
   totnec->           61821
 
  __ core available __ 
   totspc          13107199
   totnec -           61821
               -------
   totvec->        13045378

 number of external paths / symmetry
 vertex x     100      82      98      98
 vertex w     128      82      98      98
segment: free space=    13045378
 reducing frespc by                   782 to               13044596 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1          40|        24|         0|        24|         0|         1|
 -------------------------------------------------------------------------------
  Y 2          70|       482|        24|        70|        24|         2|
 -------------------------------------------------------------------------------
  X 3          15|      1430|       506|        15|        94|         3|
 -------------------------------------------------------------------------------
  W 4          21|      2034|      1936|        21|       109|         4|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=          68DP  conft+indsym=         280DP  drtbuffer=         434 DP

dimension of the ci-matrix ->>>      3970

 executing brd_struct for civct
 gentasklist: ntask=                    20
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS   
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      15      40       1430         24      15      24
     2  4   1    25      two-ext wz   2X  4 1      21      40       2034         24      21      24
     3  4   3    26      two-ext wx*  WX  4 3      21      15       2034       1430      21      15
     4  4   3    27      two-ext wx+  WX  4 3      21      15       2034       1430      21      15
     5  2   1    11      one-ext yz   1X  2 1      70      40        482         24      70      24
     6  3   2    15      1ex3ex yx    3X  3 2      15      70       1430        482      15      70
     7  4   2    16      1ex3ex yw    3X  4 2      21      70       2034        482      21      70
     8  1   1     1      allint zz    OX  1 1      40      40         24         24      24      24
     9  2   2     5      0ex2ex yy    OX  2 2      70      70        482        482      70      70
    10  3   3     6      0ex2ex xx*   OX  3 3      15      15       1430       1430      15      15
    11  3   3    18      0ex2ex xx+   OX  3 3      15      15       1430       1430      15      15
    12  4   4     7      0ex2ex ww*   OX  4 4      21      21       2034       2034      21      21
    13  4   4    19      0ex2ex ww+   OX  4 4      21      21       2034       2034      21      21
    14  2   2    42      four-ext y   4X  2 2      70      70        482        482      70      70
    15  3   3    43      four-ext x   4X  3 3      15      15       1430       1430      15      15
    16  4   4    44      four-ext w   4X  4 4      21      21       2034       2034      21      21
    17  1   1    75      dg-024ext z  OX  1 1      40      40         24         24      24      24
    18  2   2    76      dg-024ext y  OX  2 2      70      70        482        482      70      70
    19  3   3    77      dg-024ext x  OX  3 3      15      15       1430       1430      15      15
    20  4   4    78      dg-024ext w  OX  4 4      21      21       2034       2034      21      21
----------------------------------------------------------------------------------------------------
REDTASK #   1 TIME=  19.000 N=  1 (task/type/sgbra)=(   1/24/0) (
REDTASK #   2 TIME=  18.000 N=  1 (task/type/sgbra)=(   2/25/0) (
REDTASK #   3 TIME=  17.000 N=  1 (task/type/sgbra)=(   3/26/1) (
REDTASK #   4 TIME=  16.000 N=  1 (task/type/sgbra)=(   4/27/2) (
REDTASK #   5 TIME=  15.000 N=  1 (task/type/sgbra)=(   5/11/0) (
REDTASK #   6 TIME=  14.000 N=  1 (task/type/sgbra)=(   6/15/0) (
REDTASK #   7 TIME=  13.000 N=  1 (task/type/sgbra)=(   7/16/0) (
REDTASK #   8 TIME=  12.000 N=  1 (task/type/sgbra)=(   8/ 1/0) (
REDTASK #   9 TIME=  11.000 N=  1 (task/type/sgbra)=(   9/ 5/0) (
REDTASK #  10 TIME=  10.000 N=  1 (task/type/sgbra)=(  10/ 6/1) (
REDTASK #  11 TIME=   9.000 N=  1 (task/type/sgbra)=(  11/18/2) (
REDTASK #  12 TIME=   8.000 N=  1 (task/type/sgbra)=(  12/ 7/1) (
REDTASK #  13 TIME=   7.000 N=  1 (task/type/sgbra)=(  13/19/2) (
REDTASK #  14 TIME=   6.000 N=  1 (task/type/sgbra)=(  14/42/1) (
REDTASK #  15 TIME=   5.000 N=  1 (task/type/sgbra)=(  15/43/1) (
REDTASK #  16 TIME=   4.000 N=  1 (task/type/sgbra)=(  16/44/1) (
REDTASK #  17 TIME=   3.000 N=  1 (task/type/sgbra)=(  17/75/1) (
REDTASK #  18 TIME=   2.000 N=  1 (task/type/sgbra)=(  18/76/1) (
REDTASK #  19 TIME=   1.000 N=  1 (task/type/sgbra)=(  19/77/1) (
REDTASK #  20 TIME=   0.000 N=  1 (task/type/sgbra)=(  20/78/1) (
 initializing v-file: 1:                  3970

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         176 2x:           0 4x:           0
All internal counts: zz :         271 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 reference space has dimension      24
 dsyevx: computed roots 1 to    2(converged:   2)

    root           eigenvalues
    ----           ------------
       1         -46.2740792789
       2         -46.2533198520

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt=                    24

         vector  1 from unit 11 written to unit 49 filename cirefv              

 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=    24)...
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              3970
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                    1
 residual norm convergence criteria:               0.001000

          starting bk iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         602 2x:         246 4x:         106
All internal counts: zz :         271 yy:           0 xx:           0 ww:           0
One-external counts: yz :        1062 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:          72 wz:         104 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1
   ht   1    -2.77549162

          calcsovref: tciref block   1

              civs   1
 refs   1    1.00000    

          calcsovref: scrb block   1

                ci   1
 civs   1    1.00000    

          calcsovref: sovref block   1

              v      1
 ref    1    1.00000    

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.2740792789 -3.9968E-15  5.7057E-02  2.2790E-01  1.0000E-03
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.2740792789 -3.9968E-15  5.7057E-02  2.2790E-01  1.0000E-03
 
diagon:itrnv=   2
 expansion vectors are not transformed.
 matrix-vector products are not transformed.

    2 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              3970
 number of initial trial vectors:                         2
 number of initial matrix-vector products:                1
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                   30
 residual norm convergence criteria:               0.001000

          starting ci iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         602 2x:         246 4x:         106
All internal counts: zz :         271 yy:         620 xx:          66 ww:         132
One-external counts: yz :        1062 yx:        1000 yw:        1210
Two-external counts: yy :         865 ww:         252 xx:         180 xz:          72 wz:         104 wx:         360
Three-ext.   counts: yx :         320 yw:         395

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2
   ht   1    -2.77549162
   ht   2    -0.05705693    -0.18534557

          calcsovref: tciref block   1

              civs   1       civs   2
 refs   1    1.00000       6.067981E-15

          calcsovref: scrb block   1

                ci   1         ci   2
 civs   1   0.968757      -0.248013    
 civs   2   0.830462        3.24385    

          calcsovref: sovref block   1

              v      1       v      2
 ref    1   0.968757      -0.248013    

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.96875677    -0.24801272

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.3229910521  4.8912E-02  2.4700E-03  4.5414E-02  1.0000E-03
 mr-sdci #  1  2    -45.5278109946  2.0292E+00  0.0000E+00  5.3099E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   2

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         602 2x:         246 4x:         106
All internal counts: zz :         271 yy:         620 xx:          66 ww:         132
One-external counts: yz :        1062 yx:        1000 yw:        1210
Two-external counts: yy :         865 ww:         252 xx:         180 xz:          72 wz:         104 wx:         360
Three-ext.   counts: yx :         320 yw:         395

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3
   ht   1    -2.77549162
   ht   2    -0.05705693    -0.18534557
   ht   3     0.00709525    -0.00285024    -0.00871139

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3
 refs   1    1.00000       6.067981E-15  -2.556261E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3
 civs   1   0.966146      -0.152782       0.211829    
 civs   2   0.869349        1.60848       -2.80522    
 civs   3    1.03807        13.6184        8.13803    

          calcsovref: sovref block   1

              v      1       v      2       v      3
 ref    1   0.963492      -0.187594       0.191026    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.96349229    -0.18759443     0.19102601

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1    -46.3255607754  2.5697E-03  3.8611E-04  1.7379E-02  1.0000E-03
 mr-sdci #  2  2    -45.7848859388  2.5707E-01  0.0000E+00  4.5288E-01  1.0000E-04
 mr-sdci #  2  3    -45.4361825176  1.9376E+00  0.0000E+00  5.2941E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   3

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         602 2x:         246 4x:         106
All internal counts: zz :         271 yy:         620 xx:          66 ww:         132
One-external counts: yz :        1062 yx:        1000 yw:        1210
Two-external counts: yy :         865 ww:         252 xx:         180 xz:          72 wz:         104 wx:         360
Three-ext.   counts: yx :         320 yw:         395

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4
   ht   1    -2.77549162
   ht   2    -0.05705693    -0.18534557
   ht   3     0.00709525    -0.00285024    -0.00871139
   ht   4    -0.00043713    -0.00328794    -0.00047001    -0.00145057

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4
 refs   1    1.00000       6.067981E-15  -2.556261E-03   1.785755E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4
 civs   1  -0.964178       0.151023       0.220815       2.246502E-02
 civs   2  -0.873865       -1.05347       -3.00157      -0.754201    
 civs   3   -1.17075       -11.3988        6.52627       -8.88752    
 civs   4  -0.857322       -20.4684        5.65648        31.9912    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4
 ref    1  -0.961338       0.176506       0.205143       5.089669E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1    -0.96133842     0.17650630     0.20514261     0.05089669

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1    -46.3258918922  3.3112E-04  7.3991E-05  7.9963E-03  1.0000E-03
 mr-sdci #  3  2    -45.9403460684  1.5546E-01  0.0000E+00  3.1322E-01  1.0000E-04
 mr-sdci #  3  3    -45.4376190522  1.4365E-03  0.0000E+00  5.3473E-01  1.0000E-04
 mr-sdci #  3  4    -45.3918869587  1.8933E+00  0.0000E+00  5.7305E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   4

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         602 2x:         246 4x:         106
All internal counts: zz :         271 yy:         620 xx:          66 ww:         132
One-external counts: yz :        1062 yx:        1000 yw:        1210
Two-external counts: yy :         865 ww:         252 xx:         180 xz:          72 wz:         104 wx:         360
Three-ext.   counts: yx :         320 yw:         395

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1    -2.77549162
   ht   2    -0.05705693    -0.18534557
   ht   3     0.00709525    -0.00285024    -0.00871139
   ht   4    -0.00043713    -0.00328794    -0.00047001    -0.00145057
   ht   5    -0.00393529     0.00075123     0.00041537    -0.00002959    -0.00028395

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1    1.00000       6.067981E-15  -2.556261E-03   1.785755E-04   1.412486E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 civs   1  -0.964543       0.102924       0.270289       3.255917E-02  -5.943484E-03
 civs   2  -0.874506      -0.792216       -2.79360       -1.20770       0.963195    
 civs   3   -1.19449       -9.50032      -0.996397        12.1486        4.82032    
 civs   4   -1.01153       -21.5466        6.19152       -6.82210       -30.9936    
 civs   5   0.805822        31.8168       -54.4208        55.4168       -28.1615    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1  -0.960532       0.168302       0.197073       7.856128E-02  -6.357793E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1    -0.96053245     0.16830198     0.19707314     0.07856128    -0.06357793

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1    -46.3259515186  5.9626E-05  7.0173E-06  2.4399E-03  1.0000E-03
 mr-sdci #  4  2    -46.0158427724  7.5497E-02  0.0000E+00  1.8457E-01  1.0000E-04
 mr-sdci #  4  3    -45.4593076854  2.1689E-02  0.0000E+00  5.7474E-01  1.0000E-04
 mr-sdci #  4  4    -45.4125254033  2.0638E-02  0.0000E+00  5.7517E-01  1.0000E-04
 mr-sdci #  4  5    -45.3878063533  1.8892E+00  0.0000E+00  6.1195E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   5

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         602 2x:         246 4x:         106
All internal counts: zz :         271 yy:         620 xx:          66 ww:         132
One-external counts: yz :        1062 yx:        1000 yw:        1210
Two-external counts: yy :         865 ww:         252 xx:         180 xz:          72 wz:         104 wx:         360
Three-ext.   counts: yx :         320 yw:         395

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1    -2.77549162
   ht   2    -0.05705693    -0.18534557
   ht   3     0.00709525    -0.00285024    -0.00871139
   ht   4    -0.00043713    -0.00328794    -0.00047001    -0.00145057
   ht   5    -0.00393529     0.00075123     0.00041537    -0.00002959    -0.00028395
   ht   6     0.00075563    -0.00013971     0.00003458     0.00002290     0.00000929    -0.00002741

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1    1.00000       6.067981E-15  -2.556261E-03   1.785755E-04   1.412486E-03  -2.712473E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1  -0.964346       0.104769      -6.921855E-02  -0.251156      -1.479268E-02   0.100045    
 civs   2  -0.874548      -0.674192        1.19405        2.31065      -0.841978       -1.70813    
 civs   3   -1.19710       -8.73252        4.66069       -4.26589        11.2390       -4.63388    
 civs   4   -1.02864       -20.9689        1.58112        5.51611       -4.30864        32.0360    
 civs   5   0.895287        36.0156        22.4334        42.5303        65.7192        8.63131    
 civs   6   0.943551        69.0764        254.543       -87.7576       -21.0070        70.0065    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.960461       0.155482      -0.118207      -0.155389       5.423369E-02   0.110814    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.96046119     0.15548172    -0.11820735    -0.15538915     0.05423369     0.11081373

 trial vector basis is being transformed.  new dimension:   2

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1    -46.3259581397  6.6212E-06  7.2812E-07  7.9836E-04  1.0000E-03
 mr-sdci #  5  2    -46.0368058527  2.0963E-02  0.0000E+00  1.4981E-01  1.0000E-04
 mr-sdci #  5  3    -45.7016939059  2.4239E-01  0.0000E+00  4.9449E-01  1.0000E-04
 mr-sdci #  5  4    -45.4220370405  9.5116E-03  0.0000E+00  6.0462E-01  1.0000E-04
 mr-sdci #  5  5    -45.4120367674  2.4230E-02  0.0000E+00  6.0607E-01  1.0000E-04
 mr-sdci #  5  6    -45.3770815393  1.8785E+00  0.0000E+00  5.7136E-01  1.0000E-04
 
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mr-sdci  convergence criteria satisfied after  5 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1    -46.3259581397  6.6212E-06  7.2812E-07  7.9836E-04  1.0000E-03
 mr-sdci #  5  2    -46.0368058527  2.0963E-02  0.0000E+00  1.4981E-01  1.0000E-04

####################CIUDGINFO####################

   ci vector at position   1 energy=  -46.325958139748

################END OF CIUDGINFO################

 
diagon:itrnv=   0
    1 of the   3 expansion vectors are transformed.
    1 of the   2 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
maximum overlap with reference    1(overlap= 0.96046)

 information on vector: 1 from unit 11 written to unit 48 filename civout              


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -46.3259581397

                                                       internal orbitals

                                          level       1    2    3    4    5    6

                                          orbital     3    4    5   17   22   31

                                         symmetry   a1   a1   a1   a2   b1   b2 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1 -0.468669                        +-   +          -           
 z*  1  1       2  0.511536                        +-        +     -           
 z*  1  1       3  0.601497                        +-                  +     - 
 z*  1  1       4  0.022557                        +    +-         -           
 z*  1  1       5 -0.074448                        +     -   +     -           
 z*  1  1       6 -0.087431                        +     -             +     - 
 z*  1  1       7 -0.128904                        +    +     -    -           
 z*  1  1       8  0.095527                        +    +               -    - 
 z*  1  1       9  0.040996                        +         +-    -           
 z*  1  1      10  0.095417                        +          -        +     - 
 z*  1  1      11  0.087517                        +         +          -    - 
 z*  1  1      12 -0.084699                        +               -   +-      
 z*  1  1      13 -0.084700                        +               -        +- 
 z*  1  1      14 -0.033593                             +-   +     -           
 z*  1  1      15 -0.028811                             +-             +     - 
 z*  1  1      16 -0.030790                             +    +-    -           
 z*  1  1      17 -0.036422                             +     -        +     - 
 z*  1  1      18 -0.018318                             +    +          -    - 
 z*  1  1      20  0.025185                             +          -        +- 
 z*  1  1      21 -0.024296                                  +-        +     - 
 z*  1  1      22 -0.025813                                  +     -   +-      
 z*  1  1      24 -0.026533                                       +-   +     - 
 y   1  1      25  0.013027              1( a2 )   +-    -                     
 y   1  1      26 -0.010267              2( a2 )   +-    -                     
 y   1  1      28 -0.014221              1( a2 )   +-         -                
 y   1  1      29  0.011207              2( a2 )   +-         -                
 y   1  1      32 -0.019172              2( a1 )   +-              -           
 y   1  1      35 -0.058582              5( a1 )   +-              -           
 y   1  1      36 -0.015161              6( a1 )   +-              -           
 y   1  1      43 -0.016729              2( b2 )   +-                   -      
 y   1  1      45  0.013176              4( b2 )   +-                   -      
 y   1  1      50 -0.016730              2( b1 )   +-                        - 
 y   1  1      52  0.013176              4( b1 )   +-                        - 
 y   1  1      59  0.016048              1( a2 )    -   +     -                
 y   1  1      60 -0.016138              2( a2 )    -   +     -                
 y   1  1      66  0.015504              5( a1 )    -   +          -           
 y   1  1      81  0.013634              2( b1 )    -   +                    - 
 y   1  1      83 -0.013708              4( b1 )    -   +                    - 
 y   1  1      94 -0.016920              5( a1 )    -        +     -           
 y   1  1     102 -0.013315              2( b2 )    -        +          -      
 y   1  1     104  0.013388              4( b2 )    -        +          -      
 y   1  1     133  0.010303              2( a2 )    -                  +-      
 y   1  1     136  0.013879              2( a1 )    -                  +     - 
 y   1  1     139 -0.019901              5( a1 )    -                  +     - 
 y   1  1     140  0.014022              6( a1 )    -                  +     - 
 y   1  1     147  0.010304              2( a2 )    -                       +- 
 y   1  1     157  0.011427              6( a1 )   +     -         -           
 y   1  1     166 -0.011862              4( b2 )   +     -              -      
 y   1  1     182 -0.012774              6( a1 )   +          -    -           
 y   1  1     198  0.012143              4( b1 )   +          -              - 
 y   1  1     205 -0.010951              4( b1 )   +               -    -      
 y   1  1     212 -0.010951              4( b2 )   +               -         - 
 y   1  1     222  0.012433              7( a1 )   +                    -    - 
 y   1  1     260 -0.010633              3( a1 )         -   +     -           
 y   1  1     264  0.012587              7( a1 )         -   +     -           
 y   1  1     305 -0.010637              3( a1 )         -             +     - 
 y   1  1     309  0.012663              7( a1 )         -             +     - 
 y   1  1     418  0.011782              7( a1 )              -        +     - 
 y   1  1     477  0.011203              2( a2 )                   -   +     - 
 x   1  1     511  0.011340    5( a1 )   1( a2 )    -    -                     
 x   1  1     522 -0.011485    5( a1 )   2( a2 )    -    -                     
 x   1  1     546 -0.010636    7( b1 )   1( b2 )    -    -                     
 x   1  1     560 -0.010815    7( b1 )   3( b2 )    -    -                     
 x   1  1     593 -0.012377    5( a1 )   1( a2 )    -         -                
 x   1  1     604  0.012536    5( a1 )   2( a2 )    -         -                
 x   1  1     642  0.010019    7( b1 )   3( b2 )    -         -                
 x   1  1     664  0.011288    1( b1 )   7( b2 )    -         -                
 x   1  1     666  0.011527    3( b1 )   7( b2 )    -         -                
 x   1  1     678  0.016711    2( a1 )   5( a1 )    -              -           
 x   1  1     685 -0.016996    5( a1 )   6( a1 )    -              -           
 x   1  1     699 -0.014253    1( a1 )   9( a1 )    -              -           
 x   1  1     702 -0.014676    4( a1 )   9( a1 )    -              -           
 x   1  1     739 -0.011795    1( b1 )   6( b1 )    -              -           
 x   1  1     741 -0.012217    3( b1 )   6( b1 )    -              -           
 x   1  1     760 -0.011522    1( b2 )   6( b2 )    -              -           
 x   1  1     762 -0.011992    3( b2 )   6( b2 )    -              -           
 x   1  1     801 -0.012729   10( a1 )   1( b2 )    -                   -      
 x   1  1     807 -0.014558    5( a1 )   2( b2 )    -                   -      
 x   1  1     823 -0.013100   10( a1 )   3( b2 )    -                   -      
 x   1  1     829  0.014741    5( a1 )   4( b2 )    -                   -      
 x   1  1     847  0.011820    1( a1 )   6( b2 )    -                   -      
 x   1  1     850  0.012040    4( a1 )   6( b2 )    -                   -      
 x   1  1     884 -0.014558    5( a1 )   2( b1 )    -                        - 
 x   1  1     906  0.014741    5( a1 )   4( b1 )    -                        - 
 x   1  1     924  0.011970    1( a1 )   6( b1 )    -                        - 
 x   1  1     927  0.012249    4( a1 )   6( b1 )    -                        - 
 x   1  1    1000 -0.010970    1( b1 )   1( b2 )         -    -                
 x   1  1    1247  0.010764    1( a1 )   1( b1 )         -                   - 
 x   1  1    1466 -0.010530    1( a1 )   1( b2 )              -         -      
 w   1  1    1938 -0.011101    2( a1 )   1( a2 )   +-                          
 w   1  1    1942 -0.014381    6( a1 )   1( a2 )   +-                          
 w   1  1    1949  0.014322    2( a1 )   2( a2 )   +-                          
 w   1  1    1953  0.024559    6( a1 )   2( a2 )   +-                          
 w   1  1    1967  0.024968    9( a1 )   3( a2 )   +-                          
 w   1  1    1970 -0.027663    1( b1 )   1( b2 )   +-                          
 w   1  1    1972 -0.025651    3( b1 )   1( b2 )   +-                          
 w   1  1    1974 -0.010365    5( b1 )   1( b2 )   +-                          
 w   1  1    1980  0.012478    4( b1 )   2( b2 )   +-                          
 w   1  1    1984 -0.025651    1( b1 )   3( b2 )   +-                          
 w   1  1    1986 -0.025040    3( b1 )   3( b2 )   +-                          
 w   1  1    1988 -0.011812    5( b1 )   3( b2 )   +-                          
 w   1  1    1992  0.012478    2( b1 )   4( b2 )   +-                          
 w   1  1    1994 -0.021306    4( b1 )   4( b2 )   +-                          
 w   1  1    1998  0.010365    1( b1 )   5( b2 )   +-                          
 w   1  1    2000  0.011812    3( b1 )   5( b2 )   +-                          
 w   1  1    2010 -0.020566    6( b1 )   6( b2 )   +-                          
 w   1  1    2011 -0.014086    7( b1 )   6( b2 )   +-                          
 w   1  1    2017  0.011072    6( b1 )   7( b2 )   +-                          
 w   1  1    2052 -0.023524    1( b1 )   1( b2 )   +     -                     
 w   1  1    2054 -0.017156    3( b1 )   1( b2 )   +     -                     
 w   1  1    2066 -0.018046    1( b1 )   3( b2 )   +     -                     
 w   1  1    2068 -0.014231    3( b1 )   3( b2 )   +     -                     
 w   1  1    2134  0.025679    1( b1 )   1( b2 )   +          -                
 w   1  1    2136  0.019621    3( b1 )   1( b2 )   +          -                
 w   1  1    2148  0.018805    1( b1 )   3( b2 )   +          -                
 w   1  1    2150  0.015534    3( b1 )   3( b2 )   +          -                
 w   1  1    2183 -0.019735    1( a1 )   1( a1 )   +               -           
 w   1  1    2189 -0.024381    1( a1 )   4( a1 )   +               -           
 w   1  1    2192 -0.015175    4( a1 )   4( a1 )   +               -           
 w   1  1    2194  0.010203    2( a1 )   5( a1 )   +               -           
 w   1  1    2219  0.010068    1( a1 )   9( a1 )   +               -           
 w   1  1    2222  0.010813    4( a1 )   9( a1 )   +               -           
 w   1  1    2255  0.022786    1( b1 )   1( b1 )   +               -           
 w   1  1    2258  0.020645    1( b1 )   3( b1 )   +               -           
 w   1  1    2260  0.010569    3( b1 )   3( b1 )   +               -           
 w   1  1    2283  0.022785    1( b2 )   1( b2 )   +               -           
 w   1  1    2286  0.020645    1( b2 )   3( b2 )   +               -           
 w   1  1    2288  0.010569    3( b2 )   3( b2 )   +               -           
 w   1  1    2332  0.030190    1( a1 )   1( b2 )   +                    -      
 w   1  1    2335  0.022286    4( a1 )   1( b2 )   +                    -      
 w   1  1    2354  0.022886    1( a1 )   3( b2 )   +                    -      
 w   1  1    2357  0.018262    4( a1 )   3( b2 )   +                    -      
 w   1  1    2409  0.030193    1( a1 )   1( b1 )   +                         - 
 w   1  1    2412  0.022289    4( a1 )   1( b1 )   +                         - 
 w   1  1    2431  0.022888    1( a1 )   3( b1 )   +                         - 
 w   1  1    2434  0.018264    4( a1 )   3( b1 )   +                         - 
 w   1  1    2622 -0.015565    1( b1 )   1( b2 )        +     -                
 w   1  1    2624 -0.010089    3( b1 )   1( b2 )        +     -                
 w   1  1    2636 -0.010018    1( b1 )   3( b2 )        +     -                
 w   1  1    2671  0.029996    1( a1 )   1( a1 )        +          -           
 w   1  1    2677  0.025846    1( a1 )   4( a1 )        +          -           
 w   1  1    2680  0.013351    4( a1 )   4( a1 )        +          -           
 w   1  1    2685  0.011477    5( a1 )   5( a1 )        +          -           
 w   1  1    2743  0.036561    1( b1 )   1( b1 )        +          -           
 w   1  1    2746  0.031495    1( b1 )   3( b1 )        +          -           
 w   1  1    2748  0.016093    3( b1 )   3( b1 )        +          -           
 w   1  1    2771  0.041461    1( b2 )   1( b2 )        +          -           
 w   1  1    2774  0.036247    1( b2 )   3( b2 )        +          -           
 w   1  1    2776  0.018563    3( b2 )   3( b2 )        +          -           
 w   1  1    2897 -0.013228    1( a1 )   1( b1 )        +                    - 
 w   1  1    3077 -0.032739    1( a1 )   1( a1 )             +     -           
 w   1  1    3083 -0.028211    1( a1 )   4( a1 )             +     -           
 w   1  1    3086 -0.014572    4( a1 )   4( a1 )             +     -           
 w   1  1    3091 -0.012528    5( a1 )   5( a1 )             +     -           
 w   1  1    3149 -0.044826    1( b1 )   1( b1 )             +     -           
 w   1  1    3152 -0.039149    1( b1 )   3( b1 )             +     -           
 w   1  1    3154 -0.020046    3( b1 )   3( b1 )             +     -           
 w   1  1    3177 -0.040335    1( b2 )   1( b2 )             +     -           
 w   1  1    3180 -0.034794    1( b2 )   3( b2 )             +     -           
 w   1  1    3182 -0.017783    3( b2 )   3( b2 )             +     -           
 w   1  1    3226  0.012919    1( a1 )   1( b2 )             +          -      
 w   1  1    3712  0.011790    1( b1 )   1( b2 )                       +-      
 w   1  1    3761 -0.035194    1( a1 )   1( a1 )                       +     - 
 w   1  1    3767 -0.029973    1( a1 )   4( a1 )                       +     - 
 w   1  1    3770 -0.015473    4( a1 )   4( a1 )                       +     - 
 w   1  1    3775 -0.014730    5( a1 )   5( a1 )                       +     - 
 w   1  1    3833 -0.051721    1( b1 )   1( b1 )                       +     - 
 w   1  1    3836 -0.045082    1( b1 )   3( b1 )                       +     - 
 w   1  1    3838 -0.023077    3( b1 )   3( b1 )                       +     - 
 w   1  1    3861 -0.051719    1( b2 )   1( b2 )                       +     - 
 w   1  1    3864 -0.045080    1( b2 )   3( b2 )                       +     - 
 w   1  1    3866 -0.023076    3( b2 )   3( b2 )                       +     - 
 w   1  1    3922  0.011788    1( b1 )   1( b2 )                            +- 

 ci coefficient statistics:
           rq > 0.1                4
      0.1> rq > 0.01             169
     0.01> rq > 0.001            875
    0.001> rq > 0.0001           711
   0.0001> rq > 0.00001          197
  0.00001> rq > 0.000001          30
 0.000001> rq                   1984
           all                  3970
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         176 2x:           0 4x:           0
All internal counts: zz :         271 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


  iref  icsf         v(icsf)             hv(icsf)
     1     1     -0.468668744895     21.687392886145
     2     2      0.511536455616    -23.671072456651
     3     3      0.601496928629    -27.833929235311
     4     4      0.022556636438     -1.042728447120
     5     5     -0.074447704049      3.445155949028
     6     6     -0.087431086600      4.045984829921
     7     7     -0.128903536453      5.965170025528
     8     8      0.095526594715     -4.420611716724
     9     9      0.040995770565     -1.896023993162
    10    10      0.095417421453     -4.415563788362
    11    11      0.087516811212     -4.049949107303
    12    12     -0.084698589024      3.918420320055
    13    13     -0.084699981730      3.918484616569
    14    14     -0.033592895417      1.553340729172
    15    15     -0.028811053160      1.332151419690
    16    16     -0.030789738789      1.423718906340
    17    17     -0.036422214289      1.684441141311
    18    18     -0.018318014291      0.847154843412
    19    19      0.006099916724     -0.281882183230
    20    20      0.025185345286     -1.164532515466
    21    21     -0.024295517615      1.123319001411
    22    22     -0.025813331489      1.193555973331
    23    23     -0.008333335362      0.385149989526
    24    24     -0.026532530634      1.226779166355

 number of reference csfs (nref) is    24.  root number (iroot) is  1.
 c0**2 =   0.92261790  c**2 (all zwalks) =   0.92261790

 pople ci energy extrapolation is computed with  4 correlated electrons.

 eref      =    -46.274037490723   "relaxed" cnot**2         =   0.922617895351
 eci       =    -46.325958139748   deltae = eci - eref       =  -0.051920649025
 eci+dv1   =    -46.329975868844   dv1 = (1-cnot**2)*deltae  =  -0.004017729096
 eci+dv2   =    -46.330312845110   dv2 = dv1 / cnot**2       =  -0.004354705362
 eci+dv3   =    -46.330711522343   dv3 = dv1 / (2*cnot**2-1) =  -0.004753382595
 eci+pople =    -46.328131676656   (  4e- scaled deltae )    =  -0.054094185933
 passed aftci ... 
 readint2: molcas,dalton2=                     0                     0
 files%faoints=aoints              
lodens (list->root)=  1
sifcfg setup: record length 4096 DP
# d1 elements per record  3272
# d2 elements per record  2730
  The MR-CISD density will be calculated.
 item #                     1 suffix=:.drt1.state1:
 read_civout: repnuc=  -43.4985876562748     
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method:  0 last record  1max overlap with ref# 96% root-following 0
 MR-CISD energy:   -46.32595814    -2.82737048
 residuum:     0.00079836
 deltae:     0.00000662
 apxde:     0.00000073

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.96046119     0.15548172    -0.11820735    -0.15538915     0.05423369     0.11081373     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.96046119     0.15548172     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 computing final density
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=     105  DYX=     130  DYW=     160
   D0Z=      34  D0Y=     103  D0X=      12  D0W=      18
  DDZI=      80 DDYI=     180 DDXI=      30 DDWI=      36
  DDZE=       0 DDYE=      70 DDXE=      15 DDWE=      21
================================================================================
Trace of MO density:     4.000000
    4  correlated and     8  frozen core electrons

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     1.81031319    -0.13947115     0.15218794     0.00000000     0.02144242    -0.00196263
   MO   4     0.00000000     0.00000000    -0.13947115     0.28341207    -0.26221342     0.00000000    -0.01198024     0.00419522
   MO   5     0.00000000     0.00000000     0.15218794    -0.26221342     0.32936669     0.00000000     0.01364005     0.00158962
   MO   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.01568372     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000     0.02144242    -0.01198024     0.01364005     0.00000000     0.00169480    -0.00009466
   MO   8     0.00000000     0.00000000    -0.00196263     0.00419522     0.00158962     0.00000000    -0.00009466     0.00066996
   MO   9     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.01124258     0.00000000     0.00000000
   MO  10     0.00000000     0.00000000    -0.03929238    -0.02338268     0.02552152     0.00000000     0.00128051    -0.00011736
   MO  11     0.00000000     0.00000000     0.01762556    -0.01073628     0.01151071     0.00000000     0.00180569    -0.00018609
   MO  12     0.00000000     0.00000000    -0.00046334    -0.00365365    -0.00390832     0.00000000    -0.00011929    -0.00078048
   MO  13     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00239498     0.00000000     0.00000000
   MO  14     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00129700     0.00000000     0.00000000
   MO  15     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00038023     0.00000000     0.00000000
   MO  16     0.00000000     0.00000000     0.00036550     0.00105969    -0.00115660     0.00000000    -0.00006914     0.00000633

                MO   9         MO  10         MO  11         MO  12         MO  13         MO  14         MO  15         MO  16
   MO   3     0.00000000    -0.03929238     0.01762556    -0.00046334     0.00000000     0.00000000     0.00000000     0.00036550
   MO   4     0.00000000    -0.02338268    -0.01073628    -0.00365365     0.00000000     0.00000000     0.00000000     0.00105969
   MO   5     0.00000000     0.02552152     0.01151071    -0.00390832     0.00000000     0.00000000     0.00000000    -0.00115660
   MO   6     0.01124258     0.00000000     0.00000000     0.00000000     0.00239498     0.00129700    -0.00038023     0.00000000
   MO   7     0.00000000     0.00128051     0.00180569    -0.00011929     0.00000000     0.00000000     0.00000000    -0.00006914
   MO   8     0.00000000    -0.00011736    -0.00018609    -0.00078048     0.00000000     0.00000000     0.00000000     0.00000633
   MO   9     0.00845479     0.00000000     0.00000000     0.00000000     0.00206698     0.00083276    -0.00024414     0.00000000
   MO  10     0.00000000     0.00849675     0.00110073    -0.00002880     0.00000000     0.00000000     0.00000000    -0.00037989
   MO  11     0.00000000     0.00110073     0.00216965    -0.00003128     0.00000000     0.00000000     0.00000000    -0.00006572
   MO  12     0.00000000    -0.00002880    -0.00003128     0.00097995     0.00000000     0.00000000     0.00000000     0.00000172
   MO  13     0.00206698     0.00000000     0.00000000     0.00000000     0.00070326     0.00021374    -0.00006265     0.00000000
   MO  14     0.00083276     0.00000000     0.00000000     0.00000000     0.00021374     0.00197218    -0.00031502     0.00000000
   MO  15    -0.00024414     0.00000000     0.00000000     0.00000000    -0.00006265    -0.00031502     0.00098989     0.00000000
   MO  16     0.00000000    -0.00037989    -0.00006572     0.00000172     0.00000000     0.00000000     0.00000000     0.00002207

Natural orbital populations,block 1
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     2.00000000     1.84489174     0.53995143     0.04424990     0.02441712     0.00506933     0.00233673
              MO     9       MO    10       MO    11       MO    12       MO    13       MO    14       MO    15       MO    16
  occ(*)=     0.00195289     0.00089755     0.00054707     0.00052589     0.00006131     0.00001460     0.00001040     0.00000302

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4
   MO   1     0.55739260     0.01758215    -0.01535064     0.00000000
   MO   2     0.01758215     0.00178494    -0.00189164     0.00000000
   MO   3    -0.01535064    -0.00189164     0.00230129     0.00000000
   MO   4     0.00000000     0.00000000     0.00000000     0.00207261

Natural orbital populations,block 2
              MO     1       MO     2       MO     3       MO     4       MO
  occ(*)=     0.55837507     0.00299359     0.00207261     0.00011016

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     0.43891540     0.00000000     0.01477034     0.00000000    -0.01318246     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.02609059     0.00000000     0.01831127     0.00000000     0.00397996     0.00143684
   MO   4     0.00000000     0.01477034     0.00000000     0.00144829     0.00000000    -0.00156357     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.01831127     0.00000000     0.01327703     0.00000000     0.00321569     0.00099459
   MO   6     0.00000000    -0.01318246     0.00000000    -0.00156357     0.00000000     0.00187714     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000     0.00397996     0.00000000     0.00321569     0.00000000     0.00106323     0.00028965
   MO   8     0.00000000     0.00000000     0.00143684     0.00000000     0.00099459     0.00000000     0.00028965     0.00183833
   MO   9     0.00000000     0.00000000     0.00176468     0.00000000     0.00149663     0.00000000     0.00055952     0.00049003

                MO   9
   MO   2     0.00000000
   MO   3     0.00176468
   MO   4     0.00000000
   MO   5     0.00149663
   MO   6     0.00000000
   MO   7     0.00055952
   MO   8     0.00049003
   MO   9     0.00125008

Natural orbital populations,block 3
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     0.43981307     0.03998615     0.00236241     0.00195257     0.00109515     0.00047147     0.00006535
              MO     9       MO
  occ(*)=     0.00001391

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     0.43891703     0.00000000     0.01476963     0.00000000    -0.01318196     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.02608909     0.00000000     0.01831029     0.00000000    -0.00397986     0.00168444
   MO   4     0.00000000     0.01476963     0.00000000     0.00144816     0.00000000    -0.00156349     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.01831029     0.00000000     0.01327640     0.00000000    -0.00321561     0.00120709
   MO   6     0.00000000    -0.01318196     0.00000000    -0.00156349     0.00000000     0.00187711     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000    -0.00397986     0.00000000    -0.00321561     0.00000000     0.00106321    -0.00037002
   MO   8     0.00000000     0.00000000     0.00168444     0.00000000     0.00120709     0.00000000    -0.00037002     0.00197003
   MO   9     0.00000000     0.00000000    -0.00153013     0.00000000    -0.00133118     0.00000000     0.00050992    -0.00038123

                MO   9
   MO   2     0.00000000
   MO   3    -0.00153013
   MO   4     0.00000000
   MO   5    -0.00133118
   MO   6     0.00000000
   MO   7     0.00050992
   MO   8    -0.00038123
   MO   9     0.00111848

Natural orbital populations,block 4
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     0.43981462     0.03998405     0.00236235     0.00195258     0.00109516     0.00047150     0.00006533
              MO     9       MO
  occ(*)=     0.00001391


 total number of electrons =   12.0000000000

 test slabel:                     4
  a1  a2  b1  b2 


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        a1  partial gross atomic populations
   ao class       1a1        2a1        3a1        4a1        5a1        6a1 
     3_ s       2.000000   0.000000   1.797381   0.016647   0.000000   0.000000
     3_ p       0.000000   2.000000   0.000000   0.000000   0.000000   0.024290
     3_ d       0.000000   0.000000   0.047511   0.523304   0.044250   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000000   0.000000   0.000127
 
   ao class       7a1        8a1        9a1       10a1       11a1       12a1 
     3_ s       0.004600   0.000200   0.000000   0.000000   0.000000   0.000000
     3_ p       0.000000   0.000000   0.000017   0.000000   0.000000   0.000525
     3_ d       0.000470   0.002136   0.000000   0.000000   0.000547   0.000000
     3_ f       0.000000   0.000000   0.001936   0.000898   0.000000   0.000001
 
   ao class      13a1       14a1       15a1       16a1 
     3_ s       0.000001   0.000000   0.000000   0.000003
     3_ p       0.000000   0.000000   0.000010   0.000000
     3_ d       0.000061   0.000015   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000000

                        a2  partial gross atomic populations
   ao class       1a2        2a2        3a2        4a2 
     3_ d       0.558375   0.002994   0.000000   0.000110
     3_ f       0.000000   0.000000   0.002073   0.000000

                        b1  partial gross atomic populations
   ao class       1b1        2b1        3b1        4b1        5b1        6b1 
     3_ p       2.000000   0.000000   0.039747   0.000000   0.000044   0.000283
     3_ d       0.000000   0.439813   0.000000   0.002362   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000239   0.000000   0.001908   0.000812
 
   ao class       7b1        8b1        9b1 
     3_ p       0.000342   0.000000   0.000014
     3_ d       0.000000   0.000065   0.000000
     3_ f       0.000129   0.000000   0.000000

                        b2  partial gross atomic populations
   ao class       1b2        2b2        3b2        4b2        5b2        6b2 
     3_ p       2.000000   0.000000   0.039745   0.000000   0.000044   0.000283
     3_ d       0.000000   0.439815   0.000000   0.002362   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000239   0.000000   0.001908   0.000812
 
   ao class       7b2        8b2        9b2 
     3_ p       0.000342   0.000000   0.000014
     3_ d       0.000000   0.000065   0.000000
     3_ f       0.000129   0.000000   0.000000


                        gross atomic populations
     ao            3_
      s         3.818832
      p         6.105701
      d         2.064255
      f         0.011212
    total      12.000000
 

 Total number of electrons:   12.00000000

 DA ...
