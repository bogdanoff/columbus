1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      2009-03.    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


================ Computing sorting integral file structure ================

                    -----z----- -----y----- -----x----- -----w----- ---total---

                CSFs        21         622        3424        1496        5563
      internal walks       105          90          36          15         246
valid internal walks        21          90          36          15         162
 getinfoarray: info=                     6 :                     1
                  8192                  6552                  8192
                  5460                     0
 icd(3)=                  2976 ci%nnlev=                   595  l2rec=
                  8192  n2max=                  5460
 lcore1,lcore2=              13068137              13044821
 lencor,maxblo              13107200                 60000
========================================
 current settings:
 minbl3         381
 minbl4         381
 locmaxbl3     4288
 locmaxbuf     2144
 maxbl3       60000
 maxbl3       60000
 maxbl4       60000
 maxbuf       30006
========================================

 sorted 4-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)

 sorted 3-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)
 Orig.  diagonal integrals:  1electron:        34
                             0ext.    :        42
                             2ext.    :       336
                             4ext.    :       812


 Orig. off-diag. integrals:  4ext.    :     24411
                             3ext.    :     18720
                             2ext.    :      6792
                             1ext.    :      1272
                             0ext.    :        99
                             2ext. SO :         0
                             1ext. SO :         0
                             0ext. SO :         0
                             1electron:       128


 Sorted integrals            3ext.  w :     17370 x :     16020
                             4ext.  w :     20955 x :     17777


Cycle #  1 sortfile size=    163835(       5 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13068137
Cycle #  2 sortfile size=    163835(       5 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13068137
 minimum size of srtscr:     98301 WP (     3 records)
 maximum size of srtscr:    163835 WP (     5 records)
diagi   file:      4 records  of   1536 WP each=>       6144 WP total
ofdgi   file:      8 records  of   1536 WP each=>      12288 WP total
fil3w   file:      1 records  of  30006 WP each=>      30006 WP total
fil3x   file:      1 records  of  30006 WP each=>      30006 WP total
fil4w   file:      1 records  of  30006 WP each=>      30006 WP total
fil4x   file:      1 records  of  30006 WP each=>      30006 WP total
 compressed index vector length=                    57
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 0,
  GSET = 0,
   DAVCOR =10,
  NCOREL = 4
   nroot=1
  IVMODE = 3
  NBKITR = 1
   nvbkmn=2
  RTOLBK = 1e-3
   niter=30
   nvcimn=2
   rtolci=1e-3
  NVCIMX = 6
  NVRFMX = 6
  NVBKMX = 6
  IDEN  = 1
  CSFPRN = 10,
  update_mode=1
  nvrfmn=2
 /&end
 ------------------------------------------------------------------------
lodens (list->root)=  1
invlodens (root->list)=  1
 USING SEGMENTS OF EQUAL SIZE

****************  list of control variables  ****************
 lvlprt =    0      nroot  =    1      noldv  =   0      noldhv =   0
 nunitv =    1      nbkitr =    1      niter  =  30      davcor =  10
 csfprn =   10      ivmode =    3      istrt  =   0      vout   =   0
 iortls =    0      nvbkmx =    6      ibktv  =  -1      ibkthv =  -1
 nvcimx =    6      icitv  =   -1      icithv =  -1      frcsub =   0
 nvbkmn =    2      nvcimn =    2      maxseg =   4      nrfitr =  30
 ncorel =    4      nvrfmx =    6      nvrfmn =   2      iden   =   1
 itran  =    0      froot  =    0      rtmode =   0      ncouple=   1
 skipso =    F      dalton2=    0      molcas =   0      finalv =   0
 finalw =    0      cosmocalc=   0    with_tsklst=   0
 nsegwx =    1     1     1     1
 nseg0x =    1     1     1     1
 nseg1x =    1     1     1     1
 nseg2x =    1     1     1     1
 nseg3x =    1     1     1     1
 nseg4x =    1     1     1     1
 no0ex  =      0    no1ex  =      0    no2ex  =     0    no3ex  =     0
 no4ex  =      0    nodiag =      0
 cdg4ex =    1      c3ex1ex=    1      c2ex0ex=   1
 fileloc=    0     0     0     0     0     0     0     1     1     1
 directhd=   1      noaqccshift_zyxw=      0
 critical_crit=-1.00000    critical_delta= 0.05000

 ctol   = 0.010000    lrtshift=1.000000    smalld =0.001000


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-03    1.000E-03
 Computing density:                    .drt1.state1
 using                      1  nodes and                      1  cores.
 szdg/szodg per processor=                  6144                 12288
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core           13107199 DP per process

********** Integral sort section *************


 workspace allocation information: lencor=  13107199

 echo of the input for program cisrt:
 ------------------------------------------------------------------------
  &input
  maxbl3=60000
  maxbl4=60000
  &end
 ------------------------------------------------------------------------
 
 ( 6) listing file:                    ciudgls             
 ( 5) input file:                      cisrtin   
 (17) cidrt file:                      cidrtfl             
 (11) transformed integrals file:      moints    
 (12) diagonal integral file:          diagint             
 (13) off-diagonal integral file:      ofdgint             
 (31) 4-external w integrals file:     fil4w               
 (32) 4-external x integrals file:     fil4x               
 (33) 3-external w integrals file:     fil3w               
 (34) 3-external x integrals file:     fil3x               
 (21) scratch da sorting file:         srtscr              
 (12) 2-e integral file [fsplit=2]:    moints2   

 input integral file header information:
                                                                                 
 aoints SIFS file created by argos.      zam792            17:53:52.086 17-Dec-13
  cidrt_title                                                                    
 MO-coefficients from mcscf.x                                                    
  with dummy occupation 1.0 for active orbitals                                  
  total ao core energy =    0.000000000                                          
 MCSCF energy =     -46.301878576                                                
 SIFS file created by program tran.      zam792            17:53:52.407 17-Dec-13

 input energy(*) values:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.

 total core energy =   0.000000000000E+00

 nsym = 4 nmot=  34

 symmetry  =    1    2    3    4
 slabel(*) =   a1   a2   b1   b2
 nmpsy(*)  =   14    4    8    8

 info(*) =          1      8192      6552      8192      5460         0

 orbital labels, i:molab(i)=
   1:tout:001   2:tout:002   3:tout:003   4:tout:004   5:tout:005   6:tout:006   7:tout:007   8:tout:008   9:tout:009  10:tout:010
  11:tout:011  12:tout:012  13:tout:013  14:tout:014  15:tout:015  16:tout:016  17:tout:017  18:tout:018  19:tout:019  20:tout:020
  21:tout:021  22:tout:022  23:tout:023  24:tout:024  25:tout:025  26:tout:026  27:tout:027  28:tout:028  29:tout:029  30:tout:030
  31:tout:031  32:tout:032  33:tout:033  34:tout:034

 input parameters:
 prnopt=  0
 ldamin=    4095 ldamax=   32767 ldainc=      64
 maxbuf=   30006 maxbl3=   60000 maxbl4=   60000 intmxo=    1536
  Using 32 bit compression 

 drt information:
  cidrt_title                                                                    
 nmotd =  38 nfctd =   4 nfvtc =   0 nmot  =  34
 nlevel =  34 niot  =   6 lowinl=  29
 orbital-to-level map(*)
   -1  -1  29  30  31   1   2   3   4   5   6   7   8   9  10  11  32  12  13  14
   -1  33  15  16  17  18  19  20  21  -1  34  22  23  24  25  26  27  28
 compressed map(*)
   29  30  31   1   2   3   4   5   6   7   8   9  10  11  32  12  13  14  33  15
   16  17  18  19  20  21  34  22  23  24  25  26  27  28
 levsym(*)
    1   1   1   1   1   1   1   1   1   1   1   2   2   2   3   3   3   3   3   3
    3   4   4   4   4   4   4   4   1   1   1   2   3   4
 repartitioning mu(*)=
   0.  0.  0.  0.  0.  0.

 new core energy added to the energy(*) list.
 from the integral file: h1_core= -4.349858765627E+01

 indxdg: diagonal integral statistics.
 total number of integrals contributing to diagonal matrix elements:      1190
 number with all external indices:       812
 number with half external - half internal indices:       336
 number with all internal indices:        42

 indxof: off-diagonal integral statistics.
    4-external integrals: num=      24411 strt=          1
    3-external integrals: num=      18720 strt=      24412
    2-external integrals: num=       6792 strt=      43132
    1-external integrals: num=       1272 strt=      49924
    0-external integrals: num=         99 strt=      51196

 total number of off-diagonal integrals:       51294


 indxof(2nd)  ittp=   3 numx(ittp)=        6792
 indxof(2nd)  ittp=   4 numx(ittp)=        1272
 indxof(2nd)  ittp=   5 numx(ittp)=          99

 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg=  12931980
 pro2e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652

 pro2e:     25126 integrals read in     5 records.

 pro2e:         0 integrals 34-ext integrals skipped.
 pro1e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652
 pro1e: eref =    0.000000000000000E+00
 total size of srtscr:                     4  records of                  32767 
 WP =               1048544 Bytes
 putdg        1     596    1191    1786    3322   36089   57934    3018   46706   90394
   123161  131353  136813  158652

 putf:       4 buffers of length    1536 written to file 12
 diagonal integral file completed.

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep4e:    17 blocks of linear combinations of 4-external integrals processed.
 number of sorted 4-external integrals      38732
 number of original 4-external integrals    24411


 putf34: external integral file complete. nfilw=    31 nfilx=    32 nrecw=     1 nrecx=     1 lbufp= 30006

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep3e:    19 blocks of linear combinations of 3-external integrals processed.
 number of sorted 3-external integrals      33390
 number of original 3-external integrals    18720


 putf34: external integral file complete. nfilw=    33 nfilx=    34 nrecw=     1 nrecx=     1 lbufp= 30006
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      6792         3     43132     43132      6792     51294
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      1272         4     49924     49924      1272     51294
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd        99         5     51196     51196        99     51294

 putf:       8 buffers of length    1536 written to file 13
 off-diagonal files sort completed.
 executing brd_struct for cisrtinfo
cisrtinfo:
bufszi  1536
 diagfile 4ext:     812 2ext:     336 0ext:      42
 fil4w,fil4x  :   24411 fil3w,fil3x :   18720
 ofdgint  2ext:    6792 1ext:    1272 0ext:      99so0ext:       0so1ext:       0so2ext:       0
buffer minbl4     381 minbl3     381 maxbl2     384nbas:  11   3   7   7   0   0   0   0 maxbuf 30006
 CIUDG version 5.9.7 ( 5-Oct-2004)

 workspace allocation information: lcore=  13107199

 core energy values from the integral file:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -4.349858765627E+01, ietype=    6,   fcore energy of type: H1(*)   

 total core repulsion energy = -4.349858765627E+01
 nmot  =    38 niot  =     6 nfct  =     4 nfvt  =     0
 nrow  =    36 nsym  =     4 ssym  =     1 lenbuf=  1600
 nwalk,xbar:        246      105       90       36       15
 nvalwt,nvalw:      162       21       90       36       15
 ncsft:            5563
 total number of valid internal walks:     162
 nvalz,nvaly,nvalx,nvalw =       21      90      36      15

 cisrt info file parameters:
 file number  12 blocksize   1536
 mxbld   1536
 nd4ext,nd2ext,nd0ext   812   336    42
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int    24411    18720     6792     1272       99        0        0        0
 minbl4,minbl3,maxbl2   381   381   384
 maxbuf 30006
 number of external orbitals per symmetry block:  11   3   7   7
 nmsym   4 number of internal orbitals   6
 executing brd_struct for drt
 executing brd_struct for orbinf
 executing brd_struct for momap
 calcthrxt: niot,maxw1=                     6                    66
 block size     0
 pthz,pthy,pthx,pthw:   105    90    36    15 total internal walks:     246
 maxlp3,n2lp,n1lp,n0lp    66     0     0     0
 orbsym(*)= 1 1 1 2 3 4

 setref:       21 references kept,
               84 references were marked as invalid, out of
              105 total.
 nmb.of records onel     1
 nmb.of records 2-ext     5
 nmb.of records 1-ext     1
 nmb.of records 0-ext     1
 nmb.of records 2-int     0
 nmb.of records 1-int     0
 nmb.of records 0-int     0
 ---------memory usage in DP -----------------
 < n-ex core usage >
     routines:
    fourex            61797
    threx             60409
    twoex              3549
    onex               1823
    allin              1536
    diagon             2109
               =======
   maximum            61797
 
  __ static summary __ 
   reflst                21
   hrfspc                21
               -------
   static->              21
 
  __ core required  __ 
   totstc                21
   max n-ex           61797
               -------
   totnec->           61818
 
  __ core available __ 
   totspc          13107199
   totnec -           61818
               -------
   totvec->        13045381

 number of external paths / symmetry
 vertex x     100      82      98      98
 vertex w     128      82      98      98
segment: free space=    13045381
 reducing frespc by                   906 to               13044475 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1          32|        21|         0|        21|         0|         1|
 -------------------------------------------------------------------------------
  Y 2          90|       622|        21|        90|        21|         2|
 -------------------------------------------------------------------------------
  X 3          36|      3424|       643|        36|       111|         3|
 -------------------------------------------------------------------------------
  W 4          15|      1496|      4067|        15|       147|         4|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=          36DP  conft+indsym=         360DP  drtbuffer=         510 DP

dimension of the ci-matrix ->>>      5563

 executing brd_struct for civct
 gentasklist: ntask=                    20
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS   
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      36      32       3424         21      36      21
     2  4   1    25      two-ext wz   2X  4 1      15      32       1496         21      15      21
     3  4   3    26      two-ext wx*  WX  4 3      15      36       1496       3424      15      36
     4  4   3    27      two-ext wx+  WX  4 3      15      36       1496       3424      15      36
     5  2   1    11      one-ext yz   1X  2 1      90      32        622         21      90      21
     6  3   2    15      1ex3ex yx    3X  3 2      36      90       3424        622      36      90
     7  4   2    16      1ex3ex yw    3X  4 2      15      90       1496        622      15      90
     8  1   1     1      allint zz    OX  1 1      32      32         21         21      21      21
     9  2   2     5      0ex2ex yy    OX  2 2      90      90        622        622      90      90
    10  3   3     6      0ex2ex xx*   OX  3 3      36      36       3424       3424      36      36
    11  3   3    18      0ex2ex xx+   OX  3 3      36      36       3424       3424      36      36
    12  4   4     7      0ex2ex ww*   OX  4 4      15      15       1496       1496      15      15
    13  4   4    19      0ex2ex ww+   OX  4 4      15      15       1496       1496      15      15
    14  2   2    42      four-ext y   4X  2 2      90      90        622        622      90      90
    15  3   3    43      four-ext x   4X  3 3      36      36       3424       3424      36      36
    16  4   4    44      four-ext w   4X  4 4      15      15       1496       1496      15      15
    17  1   1    75      dg-024ext z  OX  1 1      32      32         21         21      21      21
    18  2   2    76      dg-024ext y  OX  2 2      90      90        622        622      90      90
    19  3   3    77      dg-024ext x  OX  3 3      36      36       3424       3424      36      36
    20  4   4    78      dg-024ext w  OX  4 4      15      15       1496       1496      15      15
----------------------------------------------------------------------------------------------------
REDTASK #   1 TIME=  19.000 N=  1 (task/type/sgbra)=(   1/24/0) (
REDTASK #   2 TIME=  18.000 N=  1 (task/type/sgbra)=(   2/25/0) (
REDTASK #   3 TIME=  17.000 N=  1 (task/type/sgbra)=(   3/26/1) (
REDTASK #   4 TIME=  16.000 N=  1 (task/type/sgbra)=(   4/27/2) (
REDTASK #   5 TIME=  15.000 N=  1 (task/type/sgbra)=(   5/11/0) (
REDTASK #   6 TIME=  14.000 N=  1 (task/type/sgbra)=(   6/15/0) (
REDTASK #   7 TIME=  13.000 N=  1 (task/type/sgbra)=(   7/16/0) (
REDTASK #   8 TIME=  12.000 N=  1 (task/type/sgbra)=(   8/ 1/0) (
REDTASK #   9 TIME=  11.000 N=  1 (task/type/sgbra)=(   9/ 5/0) (
REDTASK #  10 TIME=  10.000 N=  1 (task/type/sgbra)=(  10/ 6/1) (
REDTASK #  11 TIME=   9.000 N=  1 (task/type/sgbra)=(  11/18/2) (
REDTASK #  12 TIME=   8.000 N=  1 (task/type/sgbra)=(  12/ 7/1) (
REDTASK #  13 TIME=   7.000 N=  1 (task/type/sgbra)=(  13/19/2) (
REDTASK #  14 TIME=   6.000 N=  1 (task/type/sgbra)=(  14/42/1) (
REDTASK #  15 TIME=   5.000 N=  1 (task/type/sgbra)=(  15/43/1) (
REDTASK #  16 TIME=   4.000 N=  1 (task/type/sgbra)=(  16/44/1) (
REDTASK #  17 TIME=   3.000 N=  1 (task/type/sgbra)=(  17/75/1) (
REDTASK #  18 TIME=   2.000 N=  1 (task/type/sgbra)=(  18/76/1) (
REDTASK #  19 TIME=   1.000 N=  1 (task/type/sgbra)=(  19/77/1) (
REDTASK #  20 TIME=   0.000 N=  1 (task/type/sgbra)=(  20/78/1) (
 initializing v-file: 1:                  5563

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         162 2x:           0 4x:           0
All internal counts: zz :         210 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 reference space has dimension      21
 dsyevx: computed roots 1 to    2(converged:   2)

    root           eigenvalues
    ----           ------------
       1         -46.3018785760
       2         -46.1888399386

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt=                    21

         vector  1 from unit 11 written to unit 49 filename cirefv              

 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=    21)...
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              5563
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                    1
 residual norm convergence criteria:               0.001000

          starting bk iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         753 2x:         336 4x:         141
All internal counts: zz :         210 yy:           0 xx:           0 ww:           0
One-external counts: yz :        1183 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:         129 wz:          75 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1
   ht   1    -2.80329092

          calcsovref: tciref block   1

              civs   1
 refs   1    1.00000    

          calcsovref: scrb block   1

                ci   1
 civs   1    1.00000    

          calcsovref: sovref block   1

              v      1
 ref    1    1.00000    

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.3018785760  8.8818E-15  5.2032E-02  2.1139E-01  1.0000E-03
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.3018785760  8.8818E-15  5.2032E-02  2.1139E-01  1.0000E-03
 
diagon:itrnv=   2
 expansion vectors are not transformed.
 matrix-vector products are not transformed.

    2 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              5563
 number of initial trial vectors:                         2
 number of initial matrix-vector products:                1
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                   30
 residual norm convergence criteria:               0.001000

          starting ci iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         753 2x:         336 4x:         141
All internal counts: zz :         210 yy:         930 xx:         352 ww:          66
One-external counts: yz :        1183 yx:        2405 yw:        1220
Two-external counts: yy :        1285 ww:         180 xx:         682 xz:         129 wz:          75 wx:         510
Three-ext.   counts: yx :         676 yw:         361

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2
   ht   1    -2.80329092
   ht   2    -0.05203205    -0.18103659

          calcsovref: tciref block   1

              civs   1       civs   2
 refs   1    1.00000       2.685278E-13

          calcsovref: scrb block   1

                ci   1         ci   2
 civs   1   0.971259      -0.238026    
 civs   2   0.814596        3.32394    

          calcsovref: sovref block   1

              v      1       v      2
 ref    1   0.971259      -0.238026    

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97125881    -0.23802589

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.3455178994  4.3639E-02  1.8920E-03  4.0410E-02  1.0000E-03
 mr-sdci #  1  2    -45.5752716061  2.0767E+00  0.0000E+00  5.0090E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   2

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         753 2x:         336 4x:         141
All internal counts: zz :         210 yy:         930 xx:         352 ww:          66
One-external counts: yz :        1183 yx:        2405 yw:        1220
Two-external counts: yy :        1285 ww:         180 xx:         682 xz:         129 wz:          75 wx:         510
Three-ext.   counts: yx :         676 yw:         361

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3
   ht   1    -2.80329092
   ht   2    -0.05203205    -0.18103659
   ht   3    -0.00022651    -0.00185851    -0.00650660

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3
 refs   1    1.00000       2.685278E-13   7.828601E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3
 civs   1   0.966947      -0.174763       0.185670    
 civs   2   0.849605        1.63987       -2.88138    
 civs   3    1.09865        16.0293        9.22140    

          calcsovref: sovref block   1

              v      1       v      2       v      3
 ref    1   0.967033      -0.173508       0.186392    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.96703312    -0.17350832     0.18639155

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1    -46.3476003123  2.0824E-03  2.5496E-04  1.4525E-02  1.0000E-03
 mr-sdci #  2  2    -45.8094588302  2.3419E-01  0.0000E+00  4.5129E-01  1.0000E-04
 mr-sdci #  2  3    -45.4978774034  1.9993E+00  0.0000E+00  5.0871E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   3

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         753 2x:         336 4x:         141
All internal counts: zz :         210 yy:         930 xx:         352 ww:          66
One-external counts: yz :        1183 yx:        2405 yw:        1220
Two-external counts: yy :        1285 ww:         180 xx:         682 xz:         129 wz:          75 wx:         510
Three-ext.   counts: yx :         676 yw:         361

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4
   ht   1    -2.80329092
   ht   2    -0.05203205    -0.18103659
   ht   3    -0.00022651    -0.00185851    -0.00650660
   ht   4    -0.00183236    -0.00224653    -0.00052374    -0.00088697

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4
 refs   1    1.00000       2.685278E-13   7.828601E-05   6.660812E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4
 civs   1   0.964641       0.172914       0.201478      -7.963246E-03
 civs   2   0.853269      -0.977536       -3.18503      -0.291547    
 civs   3    1.22587       -13.0464        5.00008       -12.2310    
 civs   4   0.951997       -26.5186        11.6719        40.3384    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4
 ref    1   0.965371       0.154230       0.209644       1.794791E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.96537067     0.15422953     0.20964397     0.01794791

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1    -46.3478430852  2.4277E-04  4.3662E-05  6.2846E-03  1.0000E-03
 mr-sdci #  3  2    -45.9774873356  1.6803E-01  0.0000E+00  2.8932E-01  1.0000E-04
 mr-sdci #  3  3    -45.5096042759  1.1727E-02  0.0000E+00  5.0928E-01  1.0000E-04
 mr-sdci #  3  4    -45.3735165943  1.8749E+00  0.0000E+00  5.5927E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   4

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         753 2x:         336 4x:         141
All internal counts: zz :         210 yy:         930 xx:         352 ww:          66
One-external counts: yz :        1183 yx:        2405 yw:        1220
Two-external counts: yy :        1285 ww:         180 xx:         682 xz:         129 wz:          75 wx:         510
Three-ext.   counts: yx :         676 yw:         361

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1    -2.80329092
   ht   2    -0.05203205    -0.18103659
   ht   3    -0.00022651    -0.00185851    -0.00650660
   ht   4    -0.00183236    -0.00224653    -0.00052374    -0.00088697
   ht   5     0.00368953    -0.00033298    -0.00024498     0.00000163    -0.00014836

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1    1.00000       2.685278E-13   7.828601E-05   6.660812E-04  -1.313875E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 civs   1  -0.965148       0.115360       0.258636       9.380041E-02   6.948972E-02
 civs   2  -0.853705      -0.759390       -3.21223       0.427504       0.415725    
 civs   3   -1.24465       -11.1900        1.28940       -13.6365        6.81424    
 civs   4   -1.09321       -27.7157        11.0582        13.0400       -37.9766    
 civs   5  -0.825432       -38.1419        38.4175        95.1067        52.4400    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1  -0.964889       0.146136       0.215627      -2.353981E-02  -2.417193E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1    -0.96488904     0.14613632     0.21562680    -0.02353981    -0.02417193

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1    -46.3478791263  3.6041E-05  3.8025E-06  1.7823E-03  1.0000E-03
 mr-sdci #  4  2    -46.0392285864  6.1741E-02  0.0000E+00  1.6922E-01  1.0000E-04
 mr-sdci #  4  3    -45.5238681105  1.4264E-02  0.0000E+00  5.0717E-01  1.0000E-04
 mr-sdci #  4  4    -45.4060250870  3.2508E-02  0.0000E+00  6.1644E-01  1.0000E-04
 mr-sdci #  4  5    -45.3640420423  1.8655E+00  0.0000E+00  5.9930E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.001000
time for vector access                 0.000000

          starting ci iteration   5

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         753 2x:         336 4x:         141
All internal counts: zz :         210 yy:         930 xx:         352 ww:          66
One-external counts: yz :        1183 yx:        2405 yw:        1220
Two-external counts: yy :        1285 ww:         180 xx:         682 xz:         129 wz:          75 wx:         510
Three-ext.   counts: yx :         676 yw:         361

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1    -2.80329092
   ht   2    -0.05203205    -0.18103659
   ht   3    -0.00022651    -0.00185851    -0.00650660
   ht   4    -0.00183236    -0.00224653    -0.00052374    -0.00088697
   ht   5     0.00368953    -0.00033298    -0.00024498     0.00000163    -0.00014836
   ht   6     0.00160980    -0.00008974     0.00002536     0.00000941     0.00000213    -0.00001559

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1    1.00000       2.685278E-13   7.828601E-05   6.660812E-04  -1.313875E-03  -5.735779E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1  -0.964657       0.147626       1.116420E-02   0.228333      -6.326747E-02  -0.255368    
 civs   2  -0.853729      -0.616527        1.15971       -3.03547       0.467748       0.369686    
 civs   3   -1.24657       -9.99966        6.36613        6.46296        13.0037        3.01438    
 civs   4   -1.10773       -26.3128        6.48209        11.2011       -39.5364        9.36422    
 civs   5  -0.910365       -45.5170       -39.5669       -11.2401       -22.0311       -102.361    
 civs   6   0.975380        102.816        318.493        98.9615       -54.9243       -164.189    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.964856       0.130147      -0.114715       0.194306      -2.813436E-02  -2.023075E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.96485570     0.13014748    -0.11471476     0.19430596    -0.02813436    -0.02023075

 trial vector basis is being transformed.  new dimension:   2

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1    -46.3478828351  3.7089E-06  3.7007E-07  5.6412E-04  1.0000E-03
 mr-sdci #  5  2    -46.0626292495  2.3401E-02  0.0000E+00  1.3694E-01  1.0000E-04
 mr-sdci #  5  3    -45.7805043690  2.5664E-01  0.0000E+00  4.5049E-01  1.0000E-04
 mr-sdci #  5  4    -45.4866552097  8.0630E-02  0.0000E+00  5.0799E-01  1.0000E-04
 mr-sdci #  5  5    -45.3758935404  1.1851E-02  0.0000E+00  5.5905E-01  1.0000E-04
 mr-sdci #  5  6    -45.3185906649  1.8200E+00  0.0000E+00  6.6075E-01  1.0000E-04
 
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mr-sdci  convergence criteria satisfied after  5 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1    -46.3478828351  3.7089E-06  3.7007E-07  5.6412E-04  1.0000E-03
 mr-sdci #  5  2    -46.0626292495  2.3401E-02  0.0000E+00  1.3694E-01  1.0000E-04

####################CIUDGINFO####################

   ci vector at position   1 energy=  -46.347882835135

################END OF CIUDGINFO################

 
diagon:itrnv=   0
    1 of the   3 expansion vectors are transformed.
    1 of the   2 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
maximum overlap with reference    1(overlap= 0.96486)

 information on vector: 1 from unit 11 written to unit 48 filename civout              


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -46.3478828351

                                                       internal orbitals

                                          level       1    2    3    4    5    6

                                          orbital     3    4    5   17   22   31

                                         symmetry   a1   a1   a1   a2   b1   b2 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  3  1       1 -0.953998                        +-   +    +                 
 z*  3  1       4  0.052586                        +    +         +-           
 z*  3  1       5 -0.067988                        +    +              +-      
 z*  3  1       6  0.015414                        +    +                   +- 
 z*  3  1       7  0.048176                        +         +    +-           
 z*  3  1       8  0.021434                        +         +         +-      
 z*  3  1       9 -0.069589                        +         +              +- 
 z*  3  1      13  0.042439                             +    +    +-           
 z*  3  1      14  0.042448                             +    +         +-      
 z*  3  1      15  0.042448                             +    +              +- 
 y   3  1      65  0.021532              5( a1 )    -   +    +                 
 y   3  1      78  0.012707              4( b1 )    -   +              +       
 y   3  1     102  0.013010              4( b2 )    -        +              +  
 y   3  1     138  0.029814              5( a1 )   +     -   +                 
 y   3  1     166 -0.021084              5( a1 )   +    +     -                
 y   3  1     343 -0.010251              4( b2 )         -   +              +  
 y   3  1     392  0.011275              1( a2 )        +    +     -           
 y   3  1     393 -0.014158              2( a2 )        +    +     -           
 y   3  1     396  0.011277              2( b1 )        +    +          -      
 y   3  1     398 -0.014157              4( b1 )        +    +          -      
 y   3  1     403  0.011278              2( b2 )        +    +               - 
 y   3  1     405 -0.014157              4( b2 )        +    +               - 
 x   3  1     646 -0.010736    2( a1 )   3( a1 )   +-                          
 x   3  1     656  0.014731    3( a1 )   6( a1 )   +-                          
 x   3  1     660  0.014731    2( a1 )   7( a1 )   +-                          
 x   3  1     664  0.026039    6( a1 )   7( a1 )   +-                          
 x   3  1     688  0.014790    9( a1 )  10( a1 )   +-                          
 x   3  1     722 -0.014789    6( b1 )   7( b1 )   +-                          
 x   3  1     743 -0.014790    6( b2 )   7( b2 )   +-                          
 x   3  1     751  0.016204    2( a1 )   5( a1 )    -   +                      
 x   3  1     752  0.012329    3( a1 )   5( a1 )    -   +                      
 x   3  1     758 -0.017079    5( a1 )   6( a1 )    -   +                      
 x   3  1     763  0.016492    5( a1 )   7( a1 )    -   +                      
 x   3  1     772 -0.011917    1( a1 )   9( a1 )    -   +                      
 x   3  1     775 -0.012376    4( a1 )   9( a1 )    -   +                      
 x   3  1     780  0.013830    1( a1 )  10( a1 )    -   +                      
 x   3  1     783  0.014379    4( a1 )  10( a1 )    -   +                      
 x   3  1     817 -0.012784    1( b1 )   7( b1 )    -   +                      
 x   3  1     819 -0.013292    3( b1 )   7( b1 )    -   +                      
 x   3  1     833 -0.014328    1( b2 )   6( b2 )    -   +                      
 x   3  1     835 -0.014887    3( b2 )   6( b2 )    -   +                      
 x   3  1     838 -0.014396    1( b2 )   7( b2 )    -   +                      
 x   3  1     840 -0.014951    3( b2 )   7( b2 )    -   +                      
 x   3  1     851  0.012329    2( a1 )   5( a1 )    -        +                 
 x   3  1     852 -0.016205    3( a1 )   5( a1 )    -        +                 
 x   3  1     858 -0.016492    5( a1 )   6( a1 )    -        +                 
 x   3  1     863 -0.017079    5( a1 )   7( a1 )    -        +                 
 x   3  1     872 -0.016520    1( a1 )   9( a1 )    -        +                 
 x   3  1     875 -0.017165    4( a1 )   9( a1 )    -        +                 
 x   3  1     912 -0.015690    1( b1 )   6( b1 )    -        +                 
 x   3  1     914 -0.016300    3( b1 )   6( b1 )    -        +                 
 x   3  1     917  0.013260    1( b1 )   7( b1 )    -        +                 
 x   3  1     919  0.013771    3( b1 )   7( b1 )    -        +                 
 x   3  1     938  0.012766    1( b2 )   7( b2 )    -        +                 
 x   3  1     940  0.013272    3( b2 )   7( b2 )    -        +                 
 x   3  1    2479  0.010184    1( a1 )   1( b2 )        +                    - 
 w   3  1    4068 -0.032411    1( a1 )   1( a1 )   +    +                      
 w   3  1    4074 -0.035161    1( a1 )   4( a1 )   +    +                      
 w   3  1    4077 -0.020395    4( a1 )   4( a1 )   +    +                      
 w   3  1    4079  0.010607    2( a1 )   5( a1 )   +    +                      
 w   3  1    4104  0.010500    1( a1 )   9( a1 )   +    +                      
 w   3  1    4107  0.011128    4( a1 )   9( a1 )   +    +                      
 w   3  1    4143 -0.010316    1( b1 )   3( b1 )   +    +                      
 w   3  1    4168  0.041941    1( b2 )   1( b2 )   +    +                      
 w   3  1    4171  0.045504    1( b2 )   3( b2 )   +    +                      
 w   3  1    4173  0.026388    3( b2 )   3( b2 )   +    +                      
 w   3  1    4178 -0.011209    1( b2 )   5( b2 )   +    +                      
 w   3  1    4180 -0.010727    3( b2 )   5( b2 )   +    +                      
 w   3  1    4183  0.011018    1( b2 )   6( b2 )   +    +                      
 w   3  1    4185  0.011741    3( b2 )   6( b2 )   +    +                      
 w   3  1    4189  0.012457    1( b2 )   7( b2 )   +    +                      
 w   3  1    4191  0.013211    3( b2 )   7( b2 )   +    +                      
 w   3  1    4196 -0.029693    1( a1 )   1( a1 )   +         +                 
 w   3  1    4202 -0.032212    1( a1 )   4( a1 )   +         +                 
 w   3  1    4205 -0.018685    4( a1 )   4( a1 )   +         +                 
 w   3  1    4208 -0.010609    3( a1 )   5( a1 )   +         +                 
 w   3  1    4232  0.012989    1( a1 )   9( a1 )   +         +                 
 w   3  1    4235  0.013828    4( a1 )   9( a1 )   +         +                 
 w   3  1    4268  0.042939    1( b1 )   1( b1 )   +         +                 
 w   3  1    4271  0.046588    1( b1 )   3( b1 )   +         +                 
 w   3  1    4273  0.027016    3( b1 )   3( b1 )   +         +                 
 w   3  1    4278  0.011476    1( b1 )   5( b1 )   +         +                 
 w   3  1    4280  0.010983    3( b1 )   5( b1 )   +         +                 
 w   3  1    4283  0.012448    1( b1 )   6( b1 )   +         +                 
 w   3  1    4285  0.013247    3( b1 )   6( b1 )   +         +                 
 w   3  1    4289 -0.011453    1( b1 )   7( b1 )   +         +                 
 w   3  1    4291 -0.012147    3( b1 )   7( b1 )   +         +                 
 w   3  1    4296 -0.013219    1( b2 )   1( b2 )   +         +                 
 w   3  1    4299 -0.014340    1( b2 )   3( b2 )   +         +                 
 w   3  1    4602  0.074358    1( a1 )   1( a1 )        +    +                 
 w   3  1    4608  0.063480    1( a1 )   4( a1 )        +    +                 
 w   3  1    4611  0.032161    4( a1 )   4( a1 )        +    +                 
 w   3  1    4616  0.023966    5( a1 )   5( a1 )        +    +                 
 w   3  1    4674  0.074365    1( b1 )   1( b1 )        +    +                 
 w   3  1    4677  0.063488    1( b1 )   3( b1 )        +    +                 
 w   3  1    4679  0.032166    3( b1 )   3( b1 )        +    +                 
 w   3  1    4702  0.074364    1( b2 )   1( b2 )        +    +                 
 w   3  1    4705  0.063486    1( b2 )   3( b2 )        +    +                 
 w   3  1    4707  0.032165    3( b2 )   3( b2 )        +    +                 

 ci coefficient statistics:
           rq > 0.1                1
      0.1> rq > 0.01              98
     0.01> rq > 0.001            598
    0.001> rq > 0.0001          1200
   0.0001> rq > 0.00001          551
  0.00001> rq > 0.000001         120
 0.000001> rq                   2995
           all                  5563
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         162 2x:           0 4x:           0
All internal counts: zz :         210 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


  iref  icsf         v(icsf)             hv(icsf)
     1     1     -0.953997729374     44.171883066192
     2     2      0.000009312755     -0.000430450150
     3     3     -0.000001916058      0.000088717917
     4     4      0.052585988456     -2.435203154340
     5     5     -0.067988112055      3.148466480394
     6     6      0.015414073776     -0.713812439850
     7     7      0.048176143912     -2.230987952123
     8     8      0.021433874757     -0.992583323566
     9     9     -0.069589417462      3.222623626713
    10    10      0.002715067943     -0.125693987482
    11    11     -0.001603760556      0.074240987420
    12    12      0.001142708452     -0.052896763151
    13    13      0.042439232115     -1.964110203950
    14    14      0.042448256410     -1.964524576823
    15    15      0.042448001318     -1.964512559175
    16    16      0.004000145861     -0.185204700890
    17    17      0.000252847314     -0.011706390475
    18    18     -0.009427344180      0.436487775679
    19    19     -0.004365629545      0.202126619033
    20    20     -0.009032382399      0.418199319402
    21    21     -0.002090804872      0.096804495020

 number of reference csfs (nref) is    21.  root number (iroot) is  1.
 c0**2 =   0.93098595  c**2 (all zwalks) =   0.93098595

 pople ci energy extrapolation is computed with  4 correlated electrons.

 eref      =    -46.301867251409   "relaxed" cnot**2         =   0.930985948996
 eci       =    -46.347882835135   deltae = eci - eref       =  -0.046015583727
 eci+dv1   =    -46.351058556978   dv1 = (1-cnot**2)*deltae  =  -0.003175721842
 eci+dv2   =    -46.351293973451   dv2 = dv1 / cnot**2       =  -0.003411138316
 eci+dv3   =    -46.351567087313   dv3 = dv1 / (2*cnot**2-1) =  -0.003684252178
 eci+pople =    -46.349586067570   (  4e- scaled deltae )    =  -0.047718816161
 passed aftci ... 
 readint2: molcas,dalton2=                     0                     0
 files%faoints=aoints              
lodens (list->root)=  1
sifcfg setup: record length 4096 DP
# d1 elements per record  3272
# d2 elements per record  2730
  The MR-CISD density will be calculated.
 item #                     1 suffix=:.drt1.state1:
 read_civout: repnuc=  -43.4985876562748     
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method:  0 last record  1max overlap with ref# 96% root-following 0
 MR-CISD energy:   -46.34788284    -2.84929518
 residuum:     0.00056412
 deltae:     0.00000371
 apxde:     0.00000037

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.96485570     0.13014748    -0.11471476     0.19430596    -0.02813436    -0.02023075     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.96485570     0.13014748     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 computing final density
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=     114  DYX=     275  DYW=     150
   D0Z=      21  D0Y=     125  D0X=      38  D0W=      12
  DDZI=      72 DDYI=     240 DDXI=      66 DDWI=      30
  DDZE=       0 DDYE=      90 DDXE=      36 DDWE=      15
================================================================================
Trace of MO density:     4.000000
    4  correlated and     8  frozen core electrons

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     1.86977586    -0.00000636     0.00000030     0.00000000     0.00001184    -0.00000400
   MO   4     0.00000000     0.00000000    -0.00000636     0.97475366     0.00000190     0.00000000    -0.00420991     0.00553929
   MO   5     0.00000000     0.00000000     0.00000030     0.00000190     0.97475249     0.00000000     0.00553331     0.00421457
   MO   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.02333206     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000     0.00001184    -0.00420991     0.00553331     0.00000000     0.00125751    -0.00000002
   MO   8     0.00000000     0.00000000    -0.00000400     0.00553929     0.00421457     0.00000000    -0.00000002     0.00125806
   MO   9     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.01630114     0.00000000     0.00000000
   MO  10     0.00000000     0.00000000    -0.02935737    -0.00000240     0.00000271     0.00000000     0.00000072    -0.00000026
   MO  11     0.00000000     0.00000000     0.00000774    -0.00435668     0.00451125     0.00000000     0.00141437    -0.00016705
   MO  12     0.00000000     0.00000000     0.00000094    -0.00451312    -0.00435720     0.00000000    -0.00016701    -0.00141451
   MO  13     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00338050     0.00000000     0.00000000
   MO  14     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00241672     0.00000000     0.00000000
   MO  15     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00070850     0.00000000     0.00000000
   MO  16     0.00000000     0.00000000    -0.00022589     0.00000017    -0.00000016     0.00000000    -0.00000005     0.00000002

                MO   9         MO  10         MO  11         MO  12         MO  13         MO  14         MO  15         MO  16
   MO   3     0.00000000    -0.02935737     0.00000774     0.00000094     0.00000000     0.00000000     0.00000000    -0.00022589
   MO   4     0.00000000    -0.00000240    -0.00435668    -0.00451312     0.00000000     0.00000000     0.00000000     0.00000017
   MO   5     0.00000000     0.00000271     0.00451125    -0.00435720     0.00000000     0.00000000     0.00000000    -0.00000016
   MO   6     0.01630114     0.00000000     0.00000000     0.00000000     0.00338050    -0.00241672     0.00070850     0.00000000
   MO   7     0.00000000     0.00000072     0.00141437    -0.00016701     0.00000000     0.00000000     0.00000000    -0.00000005
   MO   8     0.00000000    -0.00000026    -0.00016705    -0.00141451     0.00000000     0.00000000     0.00000000     0.00000002
   MO   9     0.01175676     0.00000000     0.00000000     0.00000000     0.00270652    -0.00211479     0.00061999     0.00000000
   MO  10     0.00000000     0.00552188     0.00000071     0.00000013     0.00000000     0.00000000     0.00000000    -0.00025879
   MO  11     0.00000000     0.00000071     0.00185788     0.00000001     0.00000000     0.00000000     0.00000000    -0.00000005
   MO  12     0.00000000     0.00000013     0.00000001     0.00185794     0.00000000     0.00000000     0.00000000    -0.00000001
   MO  13     0.00270652     0.00000000     0.00000000     0.00000000     0.00083676    -0.00080320     0.00023547     0.00000000
   MO  14    -0.00211479     0.00000000     0.00000000     0.00000000    -0.00080320     0.00204503    -0.00027225     0.00000000
   MO  15     0.00061999     0.00000000     0.00000000     0.00000000     0.00023547    -0.00027225     0.00119600     0.00000000
   MO  16     0.00000000    -0.00025879    -0.00000005    -0.00000001     0.00000000     0.00000000     0.00000000     0.00001730

Natural orbital populations,block 1
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     2.00000000     1.87023808     0.97484540     0.97484130     0.03572889     0.00507330     0.00292547
              MO     9       MO    10       MO    11       MO    12       MO    13       MO    14       MO    15       MO    16
  occ(*)=     0.00292519     0.00197939     0.00111620     0.00033174     0.00010019     0.00009999     0.00001039     0.00000367

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4
   MO   1     0.01606033     0.00196177    -0.00263203     0.00000000
   MO   2     0.00196177     0.00042101    -0.00050234     0.00000000
   MO   3    -0.00263203    -0.00050234     0.00063832     0.00000000
   MO   4     0.00000000     0.00000000     0.00000000     0.00006570

Natural orbital populations,block 2
              MO     1       MO     2       MO     3       MO     4       MO
  occ(*)=     0.01674652     0.00036027     0.00006570     0.00001288

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     0.01605662     0.00000000     0.00196437     0.00000000    -0.00263365     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.02334185     0.00000000     0.01630877     0.00000000     0.00338257     0.00165109
   MO   4     0.00000000     0.00196437     0.00000000     0.00042183     0.00000000    -0.00050320     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.01630877     0.00000000     0.01176260     0.00000000     0.00270809     0.00144445
   MO   6     0.00000000    -0.00263365     0.00000000    -0.00050320     0.00000000     0.00063912     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000     0.00338257     0.00000000     0.00270809     0.00000000     0.00083722     0.00054851
   MO   8     0.00000000     0.00000000     0.00165109     0.00000000     0.00144445     0.00000000     0.00054851     0.00154862
   MO   9     0.00000000     0.00000000    -0.00190542     0.00000000    -0.00166713     0.00000000    -0.00063308    -0.00049948

                MO   9
   MO   2     0.00000000
   MO   3    -0.00190542
   MO   4     0.00000000
   MO   5    -0.00166713
   MO   6     0.00000000
   MO   7    -0.00063308
   MO   8    -0.00049948
   MO   9     0.00169261

Natural orbital populations,block 3
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     0.03574551     0.01674417     0.00197934     0.00111597     0.00036053     0.00033169     0.00001287
              MO     9       MO
  occ(*)=     0.00001039

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     0.01605424     0.00000000     0.00196502     0.00000000    -0.00263384     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.02333972     0.00000000     0.01630712     0.00000000    -0.00338213     0.00134758
   MO   4     0.00000000     0.00196502     0.00000000     0.00042209     0.00000000    -0.00050343     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.01630712     0.00000000     0.01176133     0.00000000    -0.00270774     0.00117892
   MO   6     0.00000000    -0.00263384     0.00000000    -0.00050343     0.00000000     0.00063928     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000    -0.00338213     0.00000000    -0.00270774     0.00000000     0.00083711    -0.00044767
   MO   8     0.00000000     0.00000000     0.00134758     0.00000000     0.00117892     0.00000000    -0.00044767     0.00140422
   MO   9     0.00000000     0.00000000     0.00213028     0.00000000     0.00186391     0.00000000    -0.00070783     0.00045581

                MO   9
   MO   2     0.00000000
   MO   3     0.00213028
   MO   4     0.00000000
   MO   5     0.00186391
   MO   6     0.00000000
   MO   7    -0.00070783
   MO   8     0.00045581
   MO   9     0.00183700

Natural orbital populations,block 4
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     0.03574192     0.01674213     0.00197934     0.00111604     0.00036061     0.00033169     0.00001286
              MO     9       MO
  occ(*)=     0.00001039


 total number of electrons =   12.0000000000

 test slabel:                     4
  a1  a2  b1  b2 


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        a1  partial gross atomic populations
   ao class       1a1        2a1        3a1        4a1        5a1        6a1 
     3_ s       2.000000   0.000000   1.870238   0.000000   0.000000   0.000000
     3_ p       0.000000   2.000000   0.000000   0.000000   0.000000   0.035365
     3_ d       0.000000   0.000000   0.000000   0.974845   0.974841   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000000   0.000000   0.000364
 
   ao class       7a1        8a1        9a1       10a1       11a1       12a1 
     3_ s       0.005073   0.000000   0.000000   0.000000   0.000000   0.000000
     3_ p       0.000000   0.000000   0.000000   0.000258   0.000000   0.000292
     3_ d       0.000000   0.002925   0.002925   0.000000   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000000   0.001721   0.001116   0.000040
 
   ao class      13a1       14a1       15a1       16a1 
     3_ s       0.000000   0.000000   0.000000   0.000004
     3_ p       0.000000   0.000000   0.000010   0.000000
     3_ d       0.000100   0.000100   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000000

                        a2  partial gross atomic populations
   ao class       1a2        2a2        3a2        4a2 
     3_ d       0.016747   0.000360   0.000000   0.000013
     3_ f       0.000000   0.000000   0.000066   0.000000

                        b1  partial gross atomic populations
   ao class       1b1        2b1        3b1        4b1        5b1        6b1 
     3_ p       2.000000   0.035381   0.000000   0.000258   0.000000   0.000000
     3_ d       0.000000   0.000000   0.016744   0.000000   0.000000   0.000361
     3_ f       0.000000   0.000364   0.000000   0.001721   0.001116   0.000000
 
   ao class       7b1        8b1        9b1 
     3_ p       0.000292   0.000000   0.000010
     3_ d       0.000000   0.000013   0.000000
     3_ f       0.000040   0.000000   0.000000

                        b2  partial gross atomic populations
   ao class       1b2        2b2        3b2        4b2        5b2        6b2 
     3_ p       2.000000   0.035378   0.000000   0.000258   0.000000   0.000000
     3_ d       0.000000   0.000000   0.016742   0.000000   0.000000   0.000361
     3_ f       0.000000   0.000364   0.000000   0.001721   0.001116   0.000000
 
   ao class       7b2        8b2        9b2 
     3_ p       0.000292   0.000000   0.000010
     3_ d       0.000000   0.000013   0.000000
     3_ f       0.000040   0.000000   0.000000


                        gross atomic populations
     ao            3_
      s         3.875315
      p         6.107805
      d         2.007090
      f         0.009789
    total      12.000000
 

 Total number of electrons:   12.00000000

 DA ...
