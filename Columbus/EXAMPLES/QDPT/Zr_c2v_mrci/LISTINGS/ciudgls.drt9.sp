1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      2009-03.    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


================ Computing sorting integral file structure ================

                    -----z----- -----y----- -----x----- -----w----- ---total---

                CSFs         3         124        1412           0        1539
      internal walks        15          20          15           0          50
valid internal walks         3          20          15           0          38
 getinfoarray: info=                     6 :                     1
                  8192                  6552                  8192
                  5460                     0
 icd(3)=                  2976 ci%nnlev=                   595  l2rec=
                  8192  n2max=                  5460
 lcore1,lcore2=              13068137              13044821
 lencor,maxblo              13107200                 60000
========================================
 current settings:
 minbl3         381
 minbl4         381
 locmaxbl3     4288
 locmaxbuf     2144
 maxbl3       60000
 maxbl3       60000
 maxbl4       60000
 maxbuf       30006
========================================

 sorted 4-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)

 sorted 3-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)
 Orig.  diagonal integrals:  1electron:        34
                             0ext.    :        42
                             2ext.    :       336
                             4ext.    :       812


 Orig. off-diag. integrals:  4ext.    :     24411
                             3ext.    :     18720
                             2ext.    :      6792
                             1ext.    :      1272
                             0ext.    :        99
                             2ext. SO :         0
                             1ext. SO :         0
                             0ext. SO :         0
                             1electron:       128


 Sorted integrals            3ext.  w :     17370 x :     16020
                             4ext.  w :     20955 x :     17777


Cycle #  1 sortfile size=    163835(       5 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13068137
Cycle #  2 sortfile size=    163835(       5 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13068137
 minimum size of srtscr:     98301 WP (     3 records)
 maximum size of srtscr:    163835 WP (     5 records)
diagi   file:      4 records  of   1536 WP each=>       6144 WP total
ofdgi   file:      8 records  of   1536 WP each=>      12288 WP total
fil3w   file:      1 records  of  30006 WP each=>      30006 WP total
fil3x   file:      1 records  of  30006 WP each=>      30006 WP total
fil4w   file:      1 records  of  30006 WP each=>      30006 WP total
fil4x   file:      1 records  of  30006 WP each=>      30006 WP total
 compressed index vector length=                     9
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 0,
  GSET = 0,
   DAVCOR =10,
  NCOREL = 4
   nroot=1
  IVMODE = 3
  NBKITR = 1
   nvbkmn=2
  RTOLBK = 1e-3
   niter=30
   nvcimn=2
   rtolci=1e-3
  NVCIMX = 6
  NVRFMX = 6
  NVBKMX = 6
  IDEN  = 1
  CSFPRN = 10,
  update_mode=1
  nvrfmn=2
 /&end
 ------------------------------------------------------------------------
lodens (list->root)=  1
invlodens (root->list)=  1
 USING SEGMENTS OF EQUAL SIZE

****************  list of control variables  ****************
 lvlprt =    0      nroot  =    1      noldv  =   0      noldhv =   0
 nunitv =    1      nbkitr =    1      niter  =  30      davcor =  10
 csfprn =   10      ivmode =    3      istrt  =   0      vout   =   0
 iortls =    0      nvbkmx =    6      ibktv  =  -1      ibkthv =  -1
 nvcimx =    6      icitv  =   -1      icithv =  -1      frcsub =   0
 nvbkmn =    2      nvcimn =    2      maxseg =   4      nrfitr =  30
 ncorel =    4      nvrfmx =    6      nvrfmn =   2      iden   =   1
 itran  =    0      froot  =    0      rtmode =   0      ncouple=   1
 skipso =    F      dalton2=    0      molcas =   0      finalv =   0
 finalw =    0      cosmocalc=   0    with_tsklst=   0
 nsegwx =    1     1     1     1
 nseg0x =    1     1     1     1
 nseg1x =    1     1     1     1
 nseg2x =    1     1     1     1
 nseg3x =    1     1     1     1
 nseg4x =    1     1     1     1
 no0ex  =      0    no1ex  =      0    no2ex  =     0    no3ex  =     0
 no4ex  =      0    nodiag =      0
 cdg4ex =    1      c3ex1ex=    1      c2ex0ex=   1
 fileloc=    0     0     0     0     0     0     0     1     1     1
 directhd=   1      noaqccshift_zyxw=      0
 critical_crit=-1.00000    critical_delta= 0.05000

 ctol   = 0.010000    lrtshift=1.000000    smalld =0.001000


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-03    1.000E-03
 Computing density:                    .drt1.state1
 using                      1  nodes and                      1  cores.
 szdg/szodg per processor=                  6144                 12288
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core           13107199 DP per process

********** Integral sort section *************


 workspace allocation information: lencor=  13107199

 echo of the input for program cisrt:
 ------------------------------------------------------------------------
  &input
  maxbl3=60000
  maxbl4=60000
  &end
 ------------------------------------------------------------------------
 
 ( 6) listing file:                    ciudgls             
 ( 5) input file:                      cisrtin   
 (17) cidrt file:                      cidrtfl             
 (11) transformed integrals file:      moints    
 (12) diagonal integral file:          diagint             
 (13) off-diagonal integral file:      ofdgint             
 (31) 4-external w integrals file:     fil4w               
 (32) 4-external x integrals file:     fil4x               
 (33) 3-external w integrals file:     fil3w               
 (34) 3-external x integrals file:     fil3x               
 (21) scratch da sorting file:         srtscr              
 (12) 2-e integral file [fsplit=2]:    moints2   

 input integral file header information:
                                                                                 
 aoints SIFS file created by argos.      zam792            17:53:52.086 17-Dec-13
  cidrt_title                                                                    
 MO-coefficients from mcscf.x                                                    
  with dummy occupation 1.0 for active orbitals                                  
  total ao core energy =    0.000000000                                          
 MCSCF energy =     -46.301878576                                                
 SIFS file created by program tran.      zam792            17:53:52.407 17-Dec-13

 input energy(*) values:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.

 total core energy =   0.000000000000E+00

 nsym = 4 nmot=  34

 symmetry  =    1    2    3    4
 slabel(*) =   a1   a2   b1   b2
 nmpsy(*)  =   14    4    8    8

 info(*) =          1      8192      6552      8192      5460         0

 orbital labels, i:molab(i)=
   1:tout:001   2:tout:002   3:tout:003   4:tout:004   5:tout:005   6:tout:006   7:tout:007   8:tout:008   9:tout:009  10:tout:010
  11:tout:011  12:tout:012  13:tout:013  14:tout:014  15:tout:015  16:tout:016  17:tout:017  18:tout:018  19:tout:019  20:tout:020
  21:tout:021  22:tout:022  23:tout:023  24:tout:024  25:tout:025  26:tout:026  27:tout:027  28:tout:028  29:tout:029  30:tout:030
  31:tout:031  32:tout:032  33:tout:033  34:tout:034

 input parameters:
 prnopt=  0
 ldamin=    4095 ldamax=   32767 ldainc=      64
 maxbuf=   30006 maxbl3=   60000 maxbl4=   60000 intmxo=    1536
  Using 32 bit compression 

 drt information:
  cidrt_title                                                                    
 nmotd =  38 nfctd =   4 nfvtc =   0 nmot  =  34
 nlevel =  34 niot  =   6 lowinl=  29
 orbital-to-level map(*)
   -1  -1  29  30  31   1   2   3   4   5   6   7   8   9  10  11  32  12  13  14
   -1  33  15  16  17  18  19  20  21  -1  34  22  23  24  25  26  27  28
 compressed map(*)
   29  30  31   1   2   3   4   5   6   7   8   9  10  11  32  12  13  14  33  15
   16  17  18  19  20  21  34  22  23  24  25  26  27  28
 levsym(*)
    1   1   1   1   1   1   1   1   1   1   1   2   2   2   3   3   3   3   3   3
    3   4   4   4   4   4   4   4   1   1   1   2   3   4
 repartitioning mu(*)=
   0.  0.  0.  0.  0.  0.

 new core energy added to the energy(*) list.
 from the integral file: h1_core= -4.349858765627E+01

 indxdg: diagonal integral statistics.
 total number of integrals contributing to diagonal matrix elements:      1190
 number with all external indices:       812
 number with half external - half internal indices:       336
 number with all internal indices:        42

 indxof: off-diagonal integral statistics.
    4-external integrals: num=      24411 strt=          1
    3-external integrals: num=      18720 strt=      24412
    2-external integrals: num=       6792 strt=      43132
    1-external integrals: num=       1272 strt=      49924
    0-external integrals: num=         99 strt=      51196

 total number of off-diagonal integrals:       51294


 indxof(2nd)  ittp=   3 numx(ittp)=        6792
 indxof(2nd)  ittp=   4 numx(ittp)=        1272
 indxof(2nd)  ittp=   5 numx(ittp)=          99

 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg=  12931980
 pro2e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652

 pro2e:     25126 integrals read in     5 records.

 pro2e:         0 integrals 34-ext integrals skipped.
 pro1e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652
 pro1e: eref =    0.000000000000000E+00
 total size of srtscr:                     4  records of                  32767 
 WP =               1048544 Bytes
 putdg        1     596    1191    1786    3322   36089   57934    3018   46706   90394
   123161  131353  136813  158652

 putf:       4 buffers of length    1536 written to file 12
 diagonal integral file completed.

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep4e:    17 blocks of linear combinations of 4-external integrals processed.
 number of sorted 4-external integrals      38732
 number of original 4-external integrals    24411


 putf34: external integral file complete. nfilw=    31 nfilx=    32 nrecw=     1 nrecx=     1 lbufp= 30006

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep3e:    19 blocks of linear combinations of 3-external integrals processed.
 number of sorted 3-external integrals      33390
 number of original 3-external integrals    18720


 putf34: external integral file complete. nfilw=    33 nfilx=    34 nrecw=     1 nrecx=     1 lbufp= 30006
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      6792         3     43132     43132      6792     51294
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      1272         4     49924     49924      1272     51294
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd        99         5     51196     51196        99     51294

 putf:       8 buffers of length    1536 written to file 13
 off-diagonal files sort completed.
 executing brd_struct for cisrtinfo
cisrtinfo:
bufszi  1536
 diagfile 4ext:     812 2ext:     336 0ext:      42
 fil4w,fil4x  :   24411 fil3w,fil3x :   18720
 ofdgint  2ext:    6792 1ext:    1272 0ext:      99so0ext:       0so1ext:       0so2ext:       0
buffer minbl4     381 minbl3     381 maxbl2     384nbas:  11   3   7   7   0   0   0   0 maxbuf 30006
 CIUDG version 5.9.7 ( 5-Oct-2004)

 workspace allocation information: lcore=  13107199

 core energy values from the integral file:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -4.349858765627E+01, ietype=    6,   fcore energy of type: H1(*)   

 total core repulsion energy = -4.349858765627E+01
 nmot  =    38 niot  =     6 nfct  =     4 nfvt  =     0
 nrow  =    23 nsym  =     4 ssym  =     1 lenbuf=  1600
 nwalk,xbar:         50       15       20       15        0
 nvalwt,nvalw:       38        3       20       15        0
 ncsft:            1539
 total number of valid internal walks:      38
 nvalz,nvaly,nvalx,nvalw =        3      20      15       0

 cisrt info file parameters:
 file number  12 blocksize   1536
 mxbld   1536
 nd4ext,nd2ext,nd0ext   812   336    42
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int    24411    18720     6792     1272       99        0        0        0
 minbl4,minbl3,maxbl2   381   381   384
 maxbuf 30006
 number of external orbitals per symmetry block:  11   3   7   7
 nmsym   4 number of internal orbitals   6
 executing brd_struct for drt
 executing brd_struct for orbinf
 executing brd_struct for momap
 calcthrxt: niot,maxw1=                     6                    11
 block size     0
 pthz,pthy,pthx,pthw:    15    20    15     0 total internal walks:      50
 maxlp3,n2lp,n1lp,n0lp    11     0     0     0
 orbsym(*)= 1 1 1 2 3 4

 setref:        3 references kept,
               12 references were marked as invalid, out of
               15 total.
 nmb.of records onel     1
 nmb.of records 2-ext     5
 nmb.of records 1-ext     1
 nmb.of records 0-ext     1
 nmb.of records 2-int     0
 nmb.of records 1-int     0
 nmb.of records 0-int     0
 ---------memory usage in DP -----------------
 < n-ex core usage >
     routines:
    fourex            61797
    threx             60079
    twoex              3549
    onex               1823
    allin              1536
    diagon             2109
               =======
   maximum            61797
 
  __ static summary __ 
   reflst                 3
   hrfspc                 3
               -------
   static->               3
 
  __ core required  __ 
   totstc                 3
   max n-ex           61797
               -------
   totnec->           61800
 
  __ core available __ 
   totspc          13107199
   totnec -           61800
               -------
   totvec->        13045399

 number of external paths / symmetry
 vertex x     100      82      98      98
 vertex w     128      82      98      98
segment: free space=    13045399
 reducing frespc by                   337 to               13045062 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1           3|         3|         0|         3|         0|         1|
 -------------------------------------------------------------------------------
  Y 2          20|       124|         3|        20|         3|         2|
 -------------------------------------------------------------------------------
  X 3          15|      1412|       127|        15|        23|         3|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=           4DP  conft+indsym=          80DP  drtbuffer=         253 DP

dimension of the ci-matrix ->>>      1539

 executing brd_struct for civct
 gentasklist: ntask=                    12
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS   
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      15       3       1412          3      15       3
     2  2   1    11      one-ext yz   1X  2 1      20       3        124          3      20       3
     3  3   2    15      1ex3ex yx    3X  3 2      15      20       1412        124      15      20
     4  1   1     1      allint zz    OX  1 1       3       3          3          3       3       3
     5  2   2     5      0ex2ex yy    OX  2 2      20      20        124        124      20      20
     6  3   3     6      0ex2ex xx*   OX  3 3      15      15       1412       1412      15      15
     7  3   3    18      0ex2ex xx+   OX  3 3      15      15       1412       1412      15      15
     8  2   2    42      four-ext y   4X  2 2      20      20        124        124      20      20
     9  3   3    43      four-ext x   4X  3 3      15      15       1412       1412      15      15
    10  1   1    75      dg-024ext z  OX  1 1       3       3          3          3       3       3
    11  2   2    76      dg-024ext y  OX  2 2      20      20        124        124      20      20
    12  3   3    77      dg-024ext x  OX  3 3      15      15       1412       1412      15      15
----------------------------------------------------------------------------------------------------
REDTASK #   1 TIME=  11.000 N=  1 (task/type/sgbra)=(   1/24/0) (
REDTASK #   2 TIME=  10.000 N=  1 (task/type/sgbra)=(   2/11/0) (
REDTASK #   3 TIME=   9.000 N=  1 (task/type/sgbra)=(   3/15/0) (
REDTASK #   4 TIME=   8.000 N=  1 (task/type/sgbra)=(   4/ 1/0) (
REDTASK #   5 TIME=   7.000 N=  1 (task/type/sgbra)=(   5/ 5/0) (
REDTASK #   6 TIME=   6.000 N=  1 (task/type/sgbra)=(   6/ 6/1) (
REDTASK #   7 TIME=   5.000 N=  1 (task/type/sgbra)=(   7/18/2) (
REDTASK #   8 TIME=   4.000 N=  1 (task/type/sgbra)=(   8/42/1) (
REDTASK #   9 TIME=   3.000 N=  1 (task/type/sgbra)=(   9/43/1) (
REDTASK #  10 TIME=   2.000 N=  1 (task/type/sgbra)=(  10/75/1) (
REDTASK #  11 TIME=   1.000 N=  1 (task/type/sgbra)=(  11/76/1) (
REDTASK #  12 TIME=   0.000 N=  1 (task/type/sgbra)=(  12/77/1) (
 initializing v-file: 1:                  1539

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:          30 2x:           0 4x:           0
All internal counts: zz :          12 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 reference space has dimension       3
 dsyevx: computed roots 1 to    2(converged:   2)

    root           eigenvalues
    ----           ------------
       1         -46.2453833560
       2         -46.0438684033

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt=                     3

         vector  1 from unit 11 written to unit 49 filename cirefv              

 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=     3)...
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              1539
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                    1
 residual norm convergence criteria:               0.001000

          starting bk iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         195 2x:          90 4x:          35
All internal counts: zz :          12 yy:           0 xx:           0 ww:           0
One-external counts: yz :          84 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:          18 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1
   ht   1    -2.74679570

          calcsovref: tciref block   1

              civs   1
 refs   1    1.00000    

          calcsovref: scrb block   1

                ci   1
 civs   1    1.00000    

          calcsovref: sovref block   1

              v      1
 ref    1    1.00000    

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.2453833560  2.2204E-15  5.4652E-02  2.1660E-01  1.0000E-03
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.2453833560  2.2204E-15  5.4652E-02  2.1660E-01  1.0000E-03
 
diagon:itrnv=   2
 expansion vectors are not transformed.
 matrix-vector products are not transformed.

    2 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              1539
 number of initial trial vectors:                         2
 number of initial matrix-vector products:                1
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                   30
 residual norm convergence criteria:               0.001000

          starting ci iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         195 2x:          90 4x:          35
All internal counts: zz :          12 yy:          72 xx:          66 ww:           0
One-external counts: yz :          84 yx:         360 yw:           0
Two-external counts: yy :         150 ww:           0 xx:         180 xz:          18 wz:           0 wx:           0
Three-ext.   counts: yx :         147 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2
   ht   1    -2.74679570
   ht   2    -0.05465212    -0.24652792

          calcsovref: tciref block   1

              civs   1       civs   2
 refs   1    1.00000      -8.881784E-13

          calcsovref: scrb block   1

                ci   1         ci   2
 civs   1   0.964859      -0.262768    
 civs   2   0.783663        2.87753    

          calcsovref: sovref block   1

              v      1       v      2
 ref    1   0.964859      -0.262768    

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.96485897    -0.26276828

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.2897720606  4.4389E-02  1.2904E-03  2.8693E-02  1.0000E-03
 mr-sdci #  1  2    -45.6468969750  2.1483E+00  0.0000E+00  5.1909E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   2

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         195 2x:          90 4x:          35
All internal counts: zz :          12 yy:          72 xx:          66 ww:           0
One-external counts: yz :          84 yx:         360 yw:           0
Two-external counts: yy :         150 ww:           0 xx:         180 xz:          18 wz:           0 wx:           0
Three-ext.   counts: yx :         147 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3
   ht   1    -2.74679570
   ht   2    -0.05465212    -0.24652792
   ht   3    -0.00019647     0.01094909    -0.00671346

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3
 refs   1    1.00000      -8.881784E-13   3.240613E-11

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3
 civs   1   0.965163       3.649600E-02   0.259092    
 civs   2   0.805051      -0.316711       -2.95435    
 civs   3    1.09933        18.1720       -6.65494    

          calcsovref: sovref block   1

              v      1       v      2       v      3
 ref    1   0.965163       3.649600E-02   0.259092    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.96516293     0.03649600     0.25909182

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1    -46.2911929720  1.4209E-03  1.2079E-04  9.5844E-03  1.0000E-03
 mr-sdci #  2  2    -45.8689378451  2.2204E-01  0.0000E+00  3.2855E-01  1.0000E-04
 mr-sdci #  2  3    -45.6171552208  2.1186E+00  0.0000E+00  5.3438E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   3

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         195 2x:          90 4x:          35
All internal counts: zz :          12 yy:          72 xx:          66 ww:           0
One-external counts: yz :          84 yx:         360 yw:           0
Two-external counts: yy :         150 ww:           0 xx:         180 xz:          18 wz:           0 wx:           0
Three-ext.   counts: yx :         147 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4
   ht   1    -2.74679570
   ht   2    -0.05465212    -0.24652792
   ht   3    -0.00019647     0.01094909    -0.00671346
   ht   4     0.00005710    -0.00354949    -0.00032294    -0.00054714

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4
 refs   1    1.00000      -8.881784E-13   3.240613E-11  -5.024099E-11

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4
 civs   1   0.962971      -0.105750       0.174546       0.176173    
 civs   2   0.807530       0.382650       -1.52088       -2.67747    
 civs   3    1.24037        11.6385        11.3206       -11.0099    
 civs   4    1.51943        46.8176       -24.9619        44.5290    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4
 ref    1   0.962971      -0.105750       0.174546       0.176173    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.96297128    -0.10575027     0.17454584     0.17617307

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1    -46.2913765571  1.8359E-04  9.1230E-06  3.0340E-03  1.0000E-03
 mr-sdci #  3  2    -46.0512872779  1.8235E-01  0.0000E+00  2.0184E-01  1.0000E-04
 mr-sdci #  3  3    -45.7900877647  1.7293E-01  0.0000E+00  4.0047E-01  1.0000E-04
 mr-sdci #  3  4    -45.3880705494  1.8895E+00  0.0000E+00  5.0303E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   4

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         195 2x:          90 4x:          35
All internal counts: zz :          12 yy:          72 xx:          66 ww:           0
One-external counts: yz :          84 yx:         360 yw:           0
Two-external counts: yy :         150 ww:           0 xx:         180 xz:          18 wz:           0 wx:           0
Three-ext.   counts: yx :         147 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1    -2.74679570
   ht   2    -0.05465212    -0.24652792
   ht   3    -0.00019647     0.01094909    -0.00671346
   ht   4     0.00005710    -0.00354949    -0.00032294    -0.00054714
   ht   5     0.00000488    -0.00030202    -0.00005749    -0.00005449    -0.00002977

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1    1.00000      -8.881784E-13   3.240613E-11  -5.024099E-11   7.544313E-11

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 civs   1   0.962643       0.110330       0.133199       0.178349      -0.107684    
 civs   2   0.807617      -0.316960       -1.11611       -2.22699        1.82599    
 civs   3    1.24866       -8.93451        14.1091       -7.18489        7.56079    
 civs   4    1.60967       -43.4084       -13.8778       -5.20509       -55.8723    
 civs   5    1.19660       -82.3997       -74.5629        202.829        169.973    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1   0.962643       0.110330       0.133199       0.178349      -0.107684    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     0.96264284     0.11032983     0.13319904     0.17834889    -0.10768375

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1    -46.2913874738  1.0917E-05  4.6602E-07  5.7168E-04  1.0000E-03
 mr-sdci #  4  2    -46.0985773606  4.7290E-02  0.0000E+00  1.0286E-01  1.0000E-04
 mr-sdci #  4  3    -45.8169278402  2.6840E-02  0.0000E+00  3.4375E-01  1.0000E-04
 mr-sdci #  4  4    -45.5511622248  1.6309E-01  0.0000E+00  5.0408E-01  1.0000E-04
 mr-sdci #  4  5    -45.2829247152  1.7843E+00  0.0000E+00  4.8709E-01  1.0000E-04
 
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mr-sdci  convergence criteria satisfied after  4 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1    -46.2913874738  1.0917E-05  4.6602E-07  5.7168E-04  1.0000E-03
 mr-sdci #  4  2    -46.0985773606  4.7290E-02  0.0000E+00  1.0286E-01  1.0000E-04
 mr-sdci #  4  3    -45.8169278402  2.6840E-02  0.0000E+00  3.4375E-01  1.0000E-04
 mr-sdci #  4  4    -45.5511622248  1.6309E-01  0.0000E+00  5.0408E-01  1.0000E-04
 mr-sdci #  4  5    -45.2829247152  1.7843E+00  0.0000E+00  4.8709E-01  1.0000E-04

####################CIUDGINFO####################

   ci vector at position   1 energy=  -46.291387473751

################END OF CIUDGINFO################

 
diagon:itrnv=   0
    1 of the   6 expansion vectors are transformed.
    1 of the   5 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
maximum overlap with reference    1(overlap= 0.96264)

 information on vector: 1 from unit 11 written to unit 48 filename civout              


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -46.2913874738

                                                       internal orbitals

                                          level       1    2    3    4    5    6

                                          orbital     3    4    5   17   22   31

                                         symmetry   a1   a1   a1   a2   b1   b2 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  5  1       1  0.962643                        +              +    +    +  
 y   5  1      15 -0.012995              1( a2 )   +    +         +            
 y   5  1      19  0.016766              2( b1 )   +    +              +       
 y   5  1      32 -0.011910              1( a2 )   +         +    +            
 y   5  1      43  0.017155              2( b2 )   +         +              +  
 y   5  1      50 -0.100178              2( b2 )   +              +    +       
 y   5  1      52  0.074757              4( b2 )   +              +    +       
 y   5  1      57  0.100172              2( b1 )   +              +         +  
 y   5  1      59 -0.074757              4( b1 )   +              +         +  
 y   5  1      63 -0.100153              1( a2 )   +                   +    +  
 y   5  1      64  0.074755              2( a2 )   +                   +    +  
 y   5  1     121  0.012785              5( a1 )                  +    +    +  
 x   5  1     359  0.024223   10( a1 )   3( a2 )   +              +            
 x   5  1     361 -0.031943    1( b1 )   1( b2 )   +              +            
 x   5  1     363 -0.024030    3( b1 )   1( b2 )   +              +            
 x   5  1     366  0.010177    6( b1 )   1( b2 )   +              +            
 x   5  1     369 -0.016227    2( b1 )   2( b2 )   +              +            
 x   5  1     371  0.015188    4( b1 )   2( b2 )   +              +            
 x   5  1     375 -0.024029    1( b1 )   3( b2 )   +              +            
 x   5  1     377 -0.019154    3( b1 )   3( b2 )   +              +            
 x   5  1     380  0.010671    6( b1 )   3( b2 )   +              +            
 x   5  1     383  0.015187    2( b1 )   4( b2 )   +              +            
 x   5  1     385 -0.022054    4( b1 )   4( b2 )   +              +            
 x   5  1     396  0.010099    1( b1 )   6( b2 )   +              +            
 x   5  1     398  0.010592    3( b1 )   6( b2 )   +              +            
 x   5  1     401 -0.019728    6( b1 )   6( b2 )   +              +            
 x   5  1     410 -0.031945    1( a1 )   1( b1 )   +                   +       
 x   5  1     413 -0.024029    4( a1 )   1( b1 )   +                   +       
 x   5  1     432 -0.024032    1( a1 )   3( b1 )   +                   +       
 x   5  1     435 -0.019151    4( a1 )   3( b1 )   +                   +       
 x   5  1     476 -0.010036    1( a1 )   7( b1 )   +                   +       
 x   5  1     479 -0.010522    4( a1 )   7( b1 )   +                   +       
 x   5  1     485  0.018246   10( a1 )   7( b1 )   +                   +       
 x   5  1     490  0.016226    1( a2 )   2( b2 )   +                   +       
 x   5  1     491 -0.015188    2( a2 )   2( b2 )   +                   +       
 x   5  1     496 -0.015187    1( a2 )   4( b2 )   +                   +       
 x   5  1     497  0.022055    2( a2 )   4( b2 )   +                   +       
 x   5  1     504  0.021337    3( a2 )   6( b2 )   +                   +       
 x   5  1     507 -0.013491    3( a2 )   7( b2 )   +                   +       
 x   5  1     511 -0.016226    1( a2 )   2( b1 )   +                        +  
 x   5  1     512  0.015188    2( a2 )   2( b1 )   +                        +  
 x   5  1     517  0.015187    1( a2 )   4( b1 )   +                        +  
 x   5  1     518 -0.022055    2( a2 )   4( b1 )   +                        +  
 x   5  1     525 -0.019082    3( a2 )   6( b1 )   +                        +  
 x   5  1     528 -0.016528    3( a2 )   7( b1 )   +                        +  
 x   5  1     529  0.031945    1( a1 )   1( b2 )   +                        +  
 x   5  1     532  0.024030    4( a1 )   1( b2 )   +                        +  
 x   5  1     551  0.024032    1( a1 )   3( b2 )   +                        +  
 x   5  1     554  0.019150    4( a1 )   3( b2 )   +                        +  
 x   5  1     598 -0.010135    4( a1 )   7( b2 )   +                        +  
 x   5  1     603 -0.014906    9( a1 )   7( b2 )   +                        +  
 x   5  1     604 -0.011479   10( a1 )   7( b2 )   +                        +  
 x   5  1    1264 -0.016039    3( a2 )   1( b1 )                  +    +       
 x   5  1    1270 -0.014698    3( a2 )   3( b1 )                  +    +       
 x   5  1    1292 -0.017931   10( a1 )   1( b2 )                  +    +       
 x   5  1    1298 -0.022215    5( a1 )   2( b2 )                  +    +       
 x   5  1    1314 -0.016568   10( a1 )   3( b2 )                  +    +       
 x   5  1    1320  0.016795    5( a1 )   4( b2 )                  +    +       
 x   5  1    1338  0.019183    1( a1 )   6( b2 )                  +    +       
 x   5  1    1341  0.017713    4( a1 )   6( b2 )                  +    +       
 x   5  1    1368 -0.015635    9( a1 )   1( b1 )                  +         +  
 x   5  1    1369 -0.011278   10( a1 )   1( b1 )                  +         +  
 x   5  1    1375  0.022215    5( a1 )   2( b1 )                  +         +  
 x   5  1    1390 -0.014401    9( a1 )   3( b1 )                  +         +  
 x   5  1    1391 -0.010448   10( a1 )   3( b1 )                  +         +  
 x   5  1    1397 -0.016795    5( a1 )   4( b1 )                  +         +  
 x   5  1    1415 -0.019247    1( a1 )   6( b1 )                  +         +  
 x   5  1    1418 -0.017764    4( a1 )   6( b1 )                  +         +  
 x   5  1    1439  0.016038    3( a2 )   1( b2 )                  +         +  
 x   5  1    1445  0.014698    3( a2 )   3( b2 )                  +         +  
 x   5  1    1462 -0.022215    5( a1 )   1( a2 )                       +    +  
 x   5  1    1473  0.016795    5( a1 )   2( a2 )                       +    +  
 x   5  1    1480  0.016039    1( a1 )   3( a2 )                       +    +  
 x   5  1    1483  0.014699    4( a1 )   3( a2 )                       +    +  
 x   5  1    1497  0.018904    7( b1 )   1( b2 )                       +    +  
 x   5  1    1511  0.017440    7( b1 )   3( b2 )                       +    +  
 x   5  1    1533  0.018131    1( b1 )   7( b2 )                       +    +  
 x   5  1    1535  0.016718    3( b1 )   7( b2 )                       +    +  

 ci coefficient statistics:
           rq > 0.1                4
      0.1> rq > 0.01              74
     0.01> rq > 0.001            132
    0.001> rq > 0.0001           252
   0.0001> rq > 0.00001          182
  0.00001> rq > 0.000001          41
 0.000001> rq                    854
           all                  1539
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:          30 2x:           0 4x:           0
All internal counts: zz :          12 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.962642835778    -44.517786975469
     2     2     -0.000156902336      0.007224395383
     3     3      0.000122599238     -0.005644919306

 number of reference csfs (nref) is     3.  root number (iroot) is  1.
 c0**2 =   0.92668127  c**2 (all zwalks) =   0.92668127

 pople ci energy extrapolation is computed with  4 correlated electrons.

 eref      =    -46.245383347381   "relaxed" cnot**2         =   0.926681268924
 eci       =    -46.291387473751   deltae = eci - eref       =  -0.046004126370
 eci+dv1   =    -46.294760437921   dv1 = (1-cnot**2)*deltae  =  -0.003372964170
 eci+dv2   =    -46.295027305787   dv2 = dv1 / cnot**2       =  -0.003639832036
 eci+dv3   =    -46.295340030865   dv3 = dv1 / (2*cnot**2-1) =  -0.003952557114
 eci+pople =    -46.293204550515   (  4e- scaled deltae )    =  -0.047821203134
 passed aftci ... 
 readint2: molcas,dalton2=                     0                     0
 files%faoints=aoints              
lodens (list->root)=  1
sifcfg setup: record length 4096 DP
# d1 elements per record  3272
# d2 elements per record  2730
  The MR-CISD density will be calculated.
 item #                     1 suffix=:.drt1.state1:
 read_civout: repnuc=  -43.4985876562748     
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method:  0 last record  1max overlap with ref# 96% root-following 0
 MR-CISD energy:   -46.29138747    -2.79279982
 residuum:     0.00057168
 deltae:     0.00001092
 apxde:     0.00000047

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.96264284     0.11032983     0.13319904     0.17834889    -0.10768375     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.96264284     0.11032983     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 computing final density
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=      12  DYX=      60  DYW=       0
   D0Z=       3  D0Y=      18  D0X=      12  D0W=       0
  DDZI=      12 DDYI=      60 DDXI=      30 DDWI=       0
  DDZE=       0 DDYE=      20 DDXE=      15 DDWE=       0
================================================================================
Trace of MO density:     4.000000
    4  correlated and     8  frozen core electrons

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.99110517    -0.00012487     0.00009777     0.00000000    -0.00005273     0.00001434
   MO   4     0.00000000     0.00000000    -0.00012487     0.00063914     0.00000033     0.00000000     0.00003221    -0.00004239
   MO   5     0.00000000     0.00000000     0.00009777     0.00000033     0.00063891     0.00000000    -0.00004233    -0.00003224
   MO   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00457213     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000    -0.00005273     0.00003221    -0.00004233     0.00000000     0.00000645     0.00000000
   MO   8     0.00000000     0.00000000     0.00001434    -0.00004239    -0.00003224     0.00000000     0.00000000     0.00000645
   MO   9     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00370230     0.00000000     0.00000000
   MO  10     0.00000000     0.00000000     0.00182367    -0.00000682     0.00000533     0.00000000    -0.00000177     0.00000048
   MO  11     0.00000000     0.00000000    -0.00003722     0.00003100    -0.00003208     0.00000000     0.00000821    -0.00000097
   MO  12     0.00000000     0.00000000    -0.00000518     0.00003215     0.00003102     0.00000000    -0.00000097    -0.00000821
   MO  13     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00122109     0.00000000     0.00000000
   MO  14     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00089769     0.00000000     0.00000000
   MO  15     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00026312     0.00000000     0.00000000
   MO  16     0.00000000     0.00000000    -0.00049953     0.00000039    -0.00000031     0.00000000     0.00000012    -0.00000003

                MO   9         MO  10         MO  11         MO  12         MO  13         MO  14         MO  15         MO  16
   MO   3     0.00000000     0.00182367    -0.00003722    -0.00000518     0.00000000     0.00000000     0.00000000    -0.00049953
   MO   4     0.00000000    -0.00000682     0.00003100     0.00003215     0.00000000     0.00000000     0.00000000     0.00000039
   MO   5     0.00000000     0.00000533    -0.00003208     0.00003102     0.00000000     0.00000000     0.00000000    -0.00000031
   MO   6     0.00370230     0.00000000     0.00000000     0.00000000     0.00122109     0.00089769    -0.00026312     0.00000000
   MO   7     0.00000000    -0.00000177     0.00000821    -0.00000097     0.00000000     0.00000000     0.00000000     0.00000012
   MO   8     0.00000000     0.00000048    -0.00000097    -0.00000821     0.00000000     0.00000000     0.00000000    -0.00000003
   MO   9     0.00310288     0.00000000     0.00000000     0.00000000     0.00108900     0.00095180    -0.00027896     0.00000000
   MO  10     0.00000000     0.00249337    -0.00000221    -0.00000031     0.00000000     0.00000000     0.00000000    -0.00012320
   MO  11     0.00000000    -0.00000221     0.00001765     0.00000000     0.00000000     0.00000000     0.00000000     0.00000014
   MO  12     0.00000000    -0.00000031     0.00000000     0.00001766     0.00000000     0.00000000     0.00000000     0.00000002
   MO  13     0.00108900     0.00000000     0.00000000     0.00000000     0.00042721     0.00048170    -0.00014118     0.00000000
   MO  14     0.00095180     0.00000000     0.00000000     0.00000000     0.00048170     0.00117772     0.00034245     0.00000000
   MO  15    -0.00027896     0.00000000     0.00000000     0.00000000    -0.00014118     0.00034245     0.00224549     0.00000000
   MO  16     0.00000000    -0.00012320     0.00000014     0.00000002     0.00000000     0.00000000     0.00000000     0.00000767

Natural orbital populations,block 1
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     2.00000000     0.99110882     0.00825123     0.00249606     0.00234588     0.00088341     0.00064707
              MO     9       MO    10       MO    11       MO    12       MO    13       MO    14       MO    15       MO    16
  occ(*)=     0.00064630     0.00004316     0.00001588     0.00001587     0.00000174     0.00000141     0.00000053     0.00000053

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4
   MO   1     0.96975793     0.09062102    -0.06539880     0.00000000
   MO   2     0.09062102     0.01186319    -0.00918706     0.00000000
   MO   3    -0.06539880    -0.00918706     0.00740173     0.00000000
   MO   4     0.00000000     0.00000000     0.00000000     0.00342800

Natural orbital populations,block 2
              MO     1       MO     2       MO     3       MO     4       MO
  occ(*)=     0.98271804     0.00619405     0.00342800     0.00011077

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     0.96975161     0.00000000     0.09063867     0.00000000    -0.06540016     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.00457190     0.00000000     0.00370233     0.00000000     0.00122108    -0.00061290
   MO   4     0.00000000     0.09063867     0.00000000     0.01186632     0.00000000    -0.00918929     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.00370233     0.00000000     0.00310307     0.00000000     0.00108904    -0.00064989
   MO   6     0.00000000    -0.06540016     0.00000000    -0.00918929     0.00000000     0.00740276     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000     0.00122108     0.00000000     0.00108904     0.00000000     0.00042721    -0.00032888
   MO   8     0.00000000     0.00000000    -0.00061290     0.00000000    -0.00064989     0.00000000    -0.00032888     0.00180202
   MO   9     0.00000000     0.00000000     0.00070741     0.00000000     0.00075003     0.00000000     0.00037954     0.00062804

                MO   9
   MO   2     0.00000000
   MO   3     0.00070741
   MO   4     0.00000000
   MO   5     0.00075003
   MO   6     0.00000000
   MO   7     0.00037954
   MO   8     0.00062804
   MO   9     0.00162099

Natural orbital populations,block 3
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     0.98271532     0.00825150     0.00619515     0.00234603     0.00088288     0.00011022     0.00004302
              MO     9       MO
  occ(*)=     0.00000175

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     0.96974991     0.00000000     0.09064425     0.00000000    -0.06540030     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.00457195     0.00000000     0.00370234     0.00000000    -0.00122109    -0.00050030
   MO   4     0.00000000     0.09064425     0.00000000     0.01186729     0.00000000    -0.00918986     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.00370234     0.00000000     0.00310307     0.00000000    -0.00108905    -0.00053048
   MO   6     0.00000000    -0.06540030     0.00000000    -0.00918986     0.00000000     0.00740295     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000    -0.00122109     0.00000000    -0.00108905     0.00000000     0.00042722     0.00026845
   MO   8     0.00000000     0.00000000    -0.00050030     0.00000000    -0.00053048     0.00000000     0.00026845     0.00198353
   MO   9     0.00000000     0.00000000    -0.00079099     0.00000000    -0.00083869     0.00000000     0.00042441    -0.00057311

                MO   9
   MO   2     0.00000000
   MO   3    -0.00079099
   MO   4     0.00000000
   MO   5    -0.00083869
   MO   6     0.00000000
   MO   7     0.00042441
   MO   8    -0.00057311
   MO   9     0.00143946

Natural orbital populations,block 4
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     0.98271471     0.00825151     0.00619535     0.00234589     0.00088302     0.00011010     0.00004305
              MO     9       MO
  occ(*)=     0.00000175


 total number of electrons =   12.0000000000

 test slabel:                     4
  a1  a2  b1  b2 


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        a1  partial gross atomic populations
   ao class       1a1        2a1        3a1        4a1        5a1        6a1 
     3_ s       0.000000   2.000000   0.991109   0.000000   0.002496   0.000000
     3_ p       2.000000   0.000000   0.000000   0.007936   0.000000   0.000000
     3_ d       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000315   0.000000   0.002346
 
   ao class       7a1        8a1        9a1       10a1       11a1       12a1 
     3_ s       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     3_ p       0.000126   0.000000   0.000000   0.000039   0.000000   0.000000
     3_ d       0.000000   0.000647   0.000646   0.000000   0.000016   0.000016
     3_ f       0.000758   0.000000   0.000000   0.000004   0.000000   0.000000
 
   ao class      13a1       14a1       15a1       16a1 
     3_ s       0.000000   0.000001   0.000000   0.000000
     3_ p       0.000002   0.000000   0.000000   0.000000
     3_ d       0.000000   0.000000   0.000001   0.000001
     3_ f       0.000000   0.000000   0.000000   0.000000

                        a2  partial gross atomic populations
   ao class       1a2        2a2        3a2        4a2 
     3_ d       0.982718   0.006194   0.000000   0.000111
     3_ f       0.000000   0.000000   0.003428   0.000000

                        b1  partial gross atomic populations
   ao class       1b1        2b1        3b1        4b1        5b1        6b1 
     3_ p       2.000000   0.000000   0.007936   0.000000   0.000000   0.000126
     3_ d       0.000000   0.982715   0.000000   0.006195   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000316   0.000000   0.002346   0.000757
 
   ao class       7b1        8b1        9b1 
     3_ p       0.000000   0.000039   0.000002
     3_ d       0.000110   0.000000   0.000000
     3_ f       0.000000   0.000004   0.000000

                        b2  partial gross atomic populations
   ao class       1b2        2b2        3b2        4b2        5b2        6b2 
     3_ p       2.000000   0.000000   0.007936   0.000000   0.000000   0.000126
     3_ d       0.000000   0.982715   0.000000   0.006195   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000316   0.000000   0.002346   0.000757
 
   ao class       7b2        8b2        9b2 
     3_ p       0.000000   0.000039   0.000002
     3_ d       0.000110   0.000000   0.000000
     3_ f       0.000000   0.000004   0.000000


                        gross atomic populations
     ao            3_
      s         2.993606
      p         6.024307
      d         2.968390
      f         0.013697
    total      12.000000
 

 Total number of electrons:   12.00000000

 DA ...
