

     ******************************************
     **    PROGRAM:              MCPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 original author: Daniel Robertson, FSU
 later revisions: Ron Shepard, ANL;
                  Michal Dallos, University Vienna



 This Version of Program mcpc is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



   ******  File header section  ******

 Headers form the restart file:
                                                                                    
    aoints SIFS file created by argos.      zam792            17:53:52.086 17-Dec-13
     title                                                                          
     title                                                                          
     title                                                                          
     title                                                                          


   ******  DRT info section  ******

 Informations for the DRT no.  1
 Header form the DRT file: 
     title                                                                          
 Molecular symmetry group:   sym1
 Total number of electrons:   12
 Spin multiplicity:            3
 Number of active orbitals:    6
 Number of active electrons:   4
 Total number of CSFs:        21

   ***  Informations from the DRT number:   1

 
 Symmetry orbital summary:
 Symm.blocks:         1     2     3     4
 Symm.labels:         a1    a2    b1    b2 

 List of doubly occupied orbitals:
  1 a1   2 a1   1 b1   1 b2 

 List of active orbitals:
  3 a1   4 a1   5 a1   1 a2   2 b1   2 b2 

 Informations for the DRT no.  2
 Header form the DRT file: 
     title                                                                          
 Molecular symmetry group:    a2 
 Total number of electrons:   12
 Spin multiplicity:            3
 Number of active orbitals:    6
 Number of active electrons:   4
 Total number of CSFs:        28

   ***  Informations from the DRT number:   2

 
 Symmetry orbital summary:
 Symm.blocks:         1     2     3     4
 Symm.labels:         a1    a2    b1    b2 

 List of doubly occupied orbitals:
  1 a1   2 a1   1 b1   1 b2 

 List of active orbitals:
  3 a1   4 a1   5 a1   1 a2   2 b1   2 b2 

 Informations for the DRT no.  3
 Header form the DRT file: 
     title                                                                          
 Molecular symmetry group:    b1 
 Total number of electrons:   12
 Spin multiplicity:            3
 Number of active orbitals:    6
 Number of active electrons:   4
 Total number of CSFs:        28

   ***  Informations from the DRT number:   3

 
 Symmetry orbital summary:
 Symm.blocks:         1     2     3     4
 Symm.labels:         a1    a2    b1    b2 

 List of doubly occupied orbitals:
  1 a1   2 a1   1 b1   1 b2 

 List of active orbitals:
  3 a1   4 a1   5 a1   1 a2   2 b1   2 b2 

 Informations for the DRT no.  4
 Header form the DRT file: 
     title                                                                          
 Molecular symmetry group:    b2 
 Total number of electrons:   12
 Spin multiplicity:            3
 Number of active orbitals:    6
 Number of active electrons:   4
 Total number of CSFs:        28

   ***  Informations from the DRT number:   4

 
 Symmetry orbital summary:
 Symm.blocks:         1     2     3     4
 Symm.labels:         a1    a2    b1    b2 

 List of doubly occupied orbitals:
  1 a1   2 a1   1 b1   1 b2 

 List of active orbitals:
  3 a1   4 a1   5 a1   1 a2   2 b1   2 b2 


   ******  MCSCF convergence information:  ******

 MCSCF convergence criteria were satisfied.

 mcscf energy=   -46.3018785757    nuclear repulsion=     0.0000000000
 demc=             0.0000000000    wnorm=                 0.0000008053
 knorm=            0.0000000310    apxde=                 0.0000000000


 MCSCF calculation performmed for   4 symmetries.

 State averaging:
 No,  ssym, navst, wavst
  1    a1     1   0.1429
  2    a2     2   0.1429 0.1429
  3    b1     2   0.1429 0.1429
  4    b2     2   0.1429 0.1429

 Input the DRT No of interest: [  1]:
In the DRT No.: 4 there are  2 states.

 Which one to take? [  1]:
 The CSFs for the state No  2 of the symmetry  b2  will be printed
 according to the following print options :

 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 9) export wave function files for cioverlap (all states).
 0) end.

 input menu number [  0]: csfs will be printed based on coefficient magnitudes.

 input the coefficient threshold (end with 0.) [ 0.0000]:
 List of active orbitals:
  3 a1   4 a1   5 a1   1 a2   2 b1   2 b2 

   csf       coeff       coeff**2    step(*)
  -----  ------------  ------------  ------------
      3 -0.8008181834  0.6413097628  300110
      2  0.4814998763  0.2318421309  301001
      1  0.3220777402  0.1037340707  310001
      9 -0.0760768382  0.0057876853  110210
     11 -0.0507146195  0.0025719726  103001
      6 -0.0442177412  0.0019552086  120110
     15  0.0432020505  0.0018664172  100301
      7 -0.0382506056  0.0014631088  112001
     28  0.0371793994  0.0013823077  000113
      8  0.0367802702  0.0013527883  111002
     25  0.0359294846  0.0012909279  003110
      5 -0.0339398431  0.0011519130  121001
     14  0.0314521054  0.0009892349  101120
     18  0.0280537211  0.0007870113  030110
     17 -0.0270277128  0.0007304973  031001
     27  0.0206036178  0.0004245091  001031
      4  0.0181913268  0.0003309244  130001
     24  0.0165569864  0.0002741338  010031
     26  0.0131934357  0.0001740667  001301
     19  0.0120655841  0.0001455783  013001
     23  0.0120636701  0.0001455321  010301
     16 -0.0106784266  0.0001140288  100031
     12 -0.0086709520  0.0000751854  102110
     20 -0.0054166838  0.0000293405  012110
     21 -0.0053976349  0.0000291345  011210
     13  0.0050198621  0.0000251990  101210
     10 -0.0039307961  0.0000154512  110120
     22 -0.0013707488  0.0000018790  011120

 input the coefficient threshold (end with 0.) [ 0.0000]:
 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 9) export wave function files for cioverlap (all states).
 0) end.

 input menu number [  0]: