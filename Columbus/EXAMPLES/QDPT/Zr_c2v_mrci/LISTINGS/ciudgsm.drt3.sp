1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=    24)...
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.2740792781 -7.1054E-15  5.7198E-02  2.2790E-01  1.0000E-03

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  1  1    -46.2740792781 -7.1054E-15  5.7198E-02  2.2790E-01  1.0000E-03

 from bk iterations: iconv=   1

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.3229786118  4.8899E-02  2.4855E-03  4.5539E-02  1.0000E-03
 mr-sdci #  2  1    -46.3255608524  2.5822E-03  3.8521E-04  1.7388E-02  1.0000E-03
 mr-sdci #  3  1    -46.3258929574  3.3211E-04  7.2397E-05  7.9523E-03  1.0000E-03
 mr-sdci #  4  1    -46.3259516593  5.8702E-05  6.9288E-06  2.4110E-03  1.0000E-03
 mr-sdci #  5  1    -46.3259581403  6.4810E-06  7.3399E-07  7.9951E-04  1.0000E-03

 mr-sdci  convergence criteria satisfied after  5 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  5  1    -46.3259581403  6.4810E-06  7.3399E-07  7.9951E-04  1.0000E-03

 number of reference csfs (nref) is    24.  root number (iroot) is  1.
 c0**2 =   0.92261740  c**2 (all zwalks) =   0.92261740

 eref      =    -46.274037333461   "relaxed" cnot**2         =   0.922617399951
 eci       =    -46.325958140317   deltae = eci - eref       =  -0.051920806857
 eci+dv1   =    -46.329975907348   dv1 = (1-cnot**2)*deltae  =  -0.004017767031
 eci+dv2   =    -46.330312889134   dv2 = dv1 / cnot**2       =  -0.004354748817
 eci+dv3   =    -46.330711573365   dv3 = dv1 / (2*cnot**2-1) =  -0.004753433048
 eci+pople =    -46.328131698861   (  4e- scaled deltae )    =  -0.054094365401
