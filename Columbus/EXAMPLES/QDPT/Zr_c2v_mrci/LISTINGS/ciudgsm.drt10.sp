1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=     4)...
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.2453833591 -4.4409E-16  5.4086E-02  2.1660E-01  1.0000E-03

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  1  1    -46.2453833591 -4.4409E-16  5.4086E-02  2.1660E-01  1.0000E-03

 from bk iterations: iconv=   1

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.2898721893  4.4489E-02  1.1853E-03  2.7818E-02  1.0000E-03
 mr-sdci #  2  1    -46.2912065912  1.3344E-03  1.1453E-04  9.3806E-03  1.0000E-03
 mr-sdci #  3  1    -46.2913775056  1.7091E-04  8.6895E-06  2.7892E-03  1.0000E-03
 mr-sdci #  4  1    -46.2913869657  9.4600E-06  1.2736E-06  8.9501E-04  1.0000E-03

 mr-sdci  convergence criteria satisfied after  4 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  4  1    -46.2913869657  9.4600E-06  1.2736E-06  8.9501E-04  1.0000E-03

 number of reference csfs (nref) is     4.  root number (iroot) is  1.
 c0**2 =   0.92674391  c**2 (all zwalks) =   0.92674391

 eref      =    -46.245383353754   "relaxed" cnot**2         =   0.926743909126
 eci       =    -46.291386965653   deltae = eci - eref       =  -0.046003611899
 eci+dv1   =    -46.294757010427   dv1 = (1-cnot**2)*deltae  =  -0.003370044774
 eci+dv2   =    -46.295023401502   dv2 = dv1 / cnot**2       =  -0.003636435849
 eci+dv3   =    -46.295335522037   dv3 = dv1 / (2*cnot**2-1) =  -0.003948556384
 eci+pople =    -46.293202352184   (  4e- scaled deltae )    =  -0.047818998429
