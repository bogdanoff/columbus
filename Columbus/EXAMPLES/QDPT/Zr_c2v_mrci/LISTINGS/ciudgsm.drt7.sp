1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=    28)...
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.3018785769 -1.3323E-15  5.1853E-02  2.1139E-01  1.0000E-03

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  1  1    -46.3018785769 -1.3323E-15  5.1853E-02  2.1139E-01  1.0000E-03

 from bk iterations: iconv=   1

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.3456063269  4.3728E-02  1.8440E-03  3.9976E-02  1.0000E-03
 mr-sdci #  2  1    -46.3476210581  2.0147E-03  2.4297E-04  1.4156E-02  1.0000E-03
 mr-sdci #  3  1    -46.3478454693  2.2441E-04  4.1291E-05  6.0931E-03  1.0000E-03
 mr-sdci #  4  1    -46.3478797198  3.4251E-05  3.1121E-06  1.6365E-03  1.0000E-03
 mr-sdci #  5  1    -46.3478829290  3.2091E-06  2.7199E-07  4.6991E-04  1.0000E-03

 mr-sdci  convergence criteria satisfied after  5 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  5  1    -46.3478829290  3.2091E-06  2.7199E-07  4.6991E-04  1.0000E-03

 number of reference csfs (nref) is    28.  root number (iroot) is  1.
 c0**2 =   0.93098583  c**2 (all zwalks) =   0.93098583

 eref      =    -46.301867338747   "relaxed" cnot**2         =   0.930985830614
 eci       =    -46.347882928965   deltae = eci - eref       =  -0.046015590218
 eci+dv1   =    -46.351058656703   dv1 = (1-cnot**2)*deltae  =  -0.003175727738
 eci+dv2   =    -46.351294074047   dv2 = dv1 / cnot**2       =  -0.003411145082
 eci+dv3   =    -46.351567188994   dv3 = dv1 / (2*cnot**2-1) =  -0.003684260029
 eci+pople =    -46.349586164769   (  4e- scaled deltae )    =  -0.047718826022
