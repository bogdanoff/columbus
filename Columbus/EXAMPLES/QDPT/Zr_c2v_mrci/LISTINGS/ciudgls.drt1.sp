1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      2009-03.    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


================ Computing sorting integral file structure ================

                    -----z----- -----y----- -----x----- -----w----- ---total---

                CSFs        33         498        1412        2264        4207
      internal walks       105          70          15          21         211
valid internal walks        33          70          15          21         139
 getinfoarray: info=                     6 :                     1
                  8192                  6552                  8192
                  5460                     0
 icd(3)=                  2976 ci%nnlev=                   595  l2rec=
                  8192  n2max=                  5460
 lcore1,lcore2=              13068137              13044821
 lencor,maxblo              13107200                 60000
========================================
 current settings:
 minbl3         381
 minbl4         381
 locmaxbl3     4288
 locmaxbuf     2144
 maxbl3       60000
 maxbl3       60000
 maxbl4       60000
 maxbuf       30006
========================================

 sorted 4-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)

 sorted 3-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)
 Orig.  diagonal integrals:  1electron:        34
                             0ext.    :        42
                             2ext.    :       336
                             4ext.    :       812


 Orig. off-diag. integrals:  4ext.    :     24411
                             3ext.    :     18720
                             2ext.    :      6792
                             1ext.    :      1272
                             0ext.    :        99
                             2ext. SO :         0
                             1ext. SO :         0
                             0ext. SO :         0
                             1electron:       128


 Sorted integrals            3ext.  w :     17370 x :     16020
                             4ext.  w :     20955 x :     17777


Cycle #  1 sortfile size=    163835(       5 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13068137
Cycle #  2 sortfile size=    163835(       5 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13068137
 minimum size of srtscr:     98301 WP (     3 records)
 maximum size of srtscr:    163835 WP (     5 records)
diagi   file:      4 records  of   1536 WP each=>       6144 WP total
ofdgi   file:      8 records  of   1536 WP each=>      12288 WP total
fil3w   file:      1 records  of  30006 WP each=>      30006 WP total
fil3x   file:      1 records  of  30006 WP each=>      30006 WP total
fil4w   file:      1 records  of  30006 WP each=>      30006 WP total
fil4x   file:      1 records  of  30006 WP each=>      30006 WP total
 compressed index vector length=                    75
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 0,
  GSET = 0,
   DAVCOR =10,
  NCOREL = 4
   nroot=1
  IVMODE = 3
  NBKITR = 1
   nvbkmn=2
  RTOLBK = 1e-3
   niter=30
   nvcimn=2
   rtolci=1e-3
  NVCIMX = 6
  NVRFMX = 6
  NVBKMX = 6
  IDEN  = 1
  CSFPRN = 10,
  update_mode=1
  nvrfmn=2
 /&end
 ------------------------------------------------------------------------
lodens (list->root)=  1
invlodens (root->list)=  1
 USING SEGMENTS OF EQUAL SIZE

****************  list of control variables  ****************
 lvlprt =    0      nroot  =    1      noldv  =   0      noldhv =   0
 nunitv =    1      nbkitr =    1      niter  =  30      davcor =  10
 csfprn =   10      ivmode =    3      istrt  =   0      vout   =   0
 iortls =    0      nvbkmx =    6      ibktv  =  -1      ibkthv =  -1
 nvcimx =    6      icitv  =   -1      icithv =  -1      frcsub =   0
 nvbkmn =    2      nvcimn =    2      maxseg =   4      nrfitr =  30
 ncorel =    4      nvrfmx =    6      nvrfmn =   2      iden   =   1
 itran  =    0      froot  =    0      rtmode =   0      ncouple=   1
 skipso =    F      dalton2=    0      molcas =   0      finalv =   0
 finalw =    0      cosmocalc=   0    with_tsklst=   0
 nsegwx =    1     1     1     1
 nseg0x =    1     1     1     1
 nseg1x =    1     1     1     1
 nseg2x =    1     1     1     1
 nseg3x =    1     1     1     1
 nseg4x =    1     1     1     1
 no0ex  =      0    no1ex  =      0    no2ex  =     0    no3ex  =     0
 no4ex  =      0    nodiag =      0
 cdg4ex =    1      c3ex1ex=    1      c2ex0ex=   1
 fileloc=    0     0     0     0     0     0     0     1     1     1
 directhd=   1      noaqccshift_zyxw=      0
 critical_crit=-1.00000    critical_delta= 0.05000

 ctol   = 0.010000    lrtshift=1.000000    smalld =0.001000


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-03    1.000E-03
 Computing density:                    .drt1.state1
 using                      1  nodes and                      1  cores.
 szdg/szodg per processor=                  6144                 12288
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core           13107199 DP per process

********** Integral sort section *************


 workspace allocation information: lencor=  13107199

 echo of the input for program cisrt:
 ------------------------------------------------------------------------
  &input
  maxbl3=60000
  maxbl4=60000
  &end
 ------------------------------------------------------------------------
 
 ( 6) listing file:                    ciudgls             
 ( 5) input file:                      cisrtin   
 (17) cidrt file:                      cidrtfl             
 (11) transformed integrals file:      moints    
 (12) diagonal integral file:          diagint             
 (13) off-diagonal integral file:      ofdgint             
 (31) 4-external w integrals file:     fil4w               
 (32) 4-external x integrals file:     fil4x               
 (33) 3-external w integrals file:     fil3w               
 (34) 3-external x integrals file:     fil3x               
 (21) scratch da sorting file:         srtscr              
 (12) 2-e integral file [fsplit=2]:    moints2   

 input integral file header information:
                                                                                 
 aoints SIFS file created by argos.      zam792            17:53:52.086 17-Dec-13
  cidrt_title                                                                    
 MO-coefficients from mcscf.x                                                    
  with dummy occupation 1.0 for active orbitals                                  
  total ao core energy =    0.000000000                                          
 MCSCF energy =     -46.301878576                                                
 SIFS file created by program tran.      zam792            17:53:52.407 17-Dec-13

 input energy(*) values:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.

 total core energy =   0.000000000000E+00

 nsym = 4 nmot=  34

 symmetry  =    1    2    3    4
 slabel(*) =   a1   a2   b1   b2
 nmpsy(*)  =   14    4    8    8

 info(*) =          1      8192      6552      8192      5460         0

 orbital labels, i:molab(i)=
   1:tout:001   2:tout:002   3:tout:003   4:tout:004   5:tout:005   6:tout:006   7:tout:007   8:tout:008   9:tout:009  10:tout:010
  11:tout:011  12:tout:012  13:tout:013  14:tout:014  15:tout:015  16:tout:016  17:tout:017  18:tout:018  19:tout:019  20:tout:020
  21:tout:021  22:tout:022  23:tout:023  24:tout:024  25:tout:025  26:tout:026  27:tout:027  28:tout:028  29:tout:029  30:tout:030
  31:tout:031  32:tout:032  33:tout:033  34:tout:034

 input parameters:
 prnopt=  0
 ldamin=    4095 ldamax=   32767 ldainc=      64
 maxbuf=   30006 maxbl3=   60000 maxbl4=   60000 intmxo=    1536
  Using 32 bit compression 

 drt information:
  cidrt_title                                                                    
 nmotd =  38 nfctd =   4 nfvtc =   0 nmot  =  34
 nlevel =  34 niot  =   6 lowinl=  29
 orbital-to-level map(*)
   -1  -1  29  30  31   1   2   3   4   5   6   7   8   9  10  11  32  12  13  14
   -1  33  15  16  17  18  19  20  21  -1  34  22  23  24  25  26  27  28
 compressed map(*)
   29  30  31   1   2   3   4   5   6   7   8   9  10  11  32  12  13  14  33  15
   16  17  18  19  20  21  34  22  23  24  25  26  27  28
 levsym(*)
    1   1   1   1   1   1   1   1   1   1   1   2   2   2   3   3   3   3   3   3
    3   4   4   4   4   4   4   4   1   1   1   2   3   4
 repartitioning mu(*)=
   0.  0.  0.  0.  0.  0.

 new core energy added to the energy(*) list.
 from the integral file: h1_core= -4.349858765627E+01

 indxdg: diagonal integral statistics.
 total number of integrals contributing to diagonal matrix elements:      1190
 number with all external indices:       812
 number with half external - half internal indices:       336
 number with all internal indices:        42

 indxof: off-diagonal integral statistics.
    4-external integrals: num=      24411 strt=          1
    3-external integrals: num=      18720 strt=      24412
    2-external integrals: num=       6792 strt=      43132
    1-external integrals: num=       1272 strt=      49924
    0-external integrals: num=         99 strt=      51196

 total number of off-diagonal integrals:       51294


 indxof(2nd)  ittp=   3 numx(ittp)=        6792
 indxof(2nd)  ittp=   4 numx(ittp)=        1272
 indxof(2nd)  ittp=   5 numx(ittp)=          99

 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg=  12931980
 pro2e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652

 pro2e:     25126 integrals read in     5 records.

 pro2e:         0 integrals 34-ext integrals skipped.
 pro1e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652
 pro1e: eref =    0.000000000000000E+00
 total size of srtscr:                     4  records of                  32767 
 WP =               1048544 Bytes
 putdg        1     596    1191    1786    3322   36089   57934    3018   46706   90394
   123161  131353  136813  158652

 putf:       4 buffers of length    1536 written to file 12
 diagonal integral file completed.

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep4e:    17 blocks of linear combinations of 4-external integrals processed.
 number of sorted 4-external integrals      38732
 number of original 4-external integrals    24411


 putf34: external integral file complete. nfilw=    31 nfilx=    32 nrecw=     1 nrecx=     1 lbufp= 30006

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep3e:    19 blocks of linear combinations of 3-external integrals processed.
 number of sorted 3-external integrals      33390
 number of original 3-external integrals    18720


 putf34: external integral file complete. nfilw=    33 nfilx=    34 nrecw=     1 nrecx=     1 lbufp= 30006
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      6792         3     43132     43132      6792     51294
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      1272         4     49924     49924      1272     51294
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd        99         5     51196     51196        99     51294

 putf:       8 buffers of length    1536 written to file 13
 off-diagonal files sort completed.
 executing brd_struct for cisrtinfo
cisrtinfo:
bufszi  1536
 diagfile 4ext:     812 2ext:     336 0ext:      42
 fil4w,fil4x  :   24411 fil3w,fil3x :   18720
 ofdgint  2ext:    6792 1ext:    1272 0ext:      99so0ext:       0so1ext:       0so2ext:       0
buffer minbl4     381 minbl3     381 maxbl2     384nbas:  11   3   7   7   0   0   0   0 maxbuf 30006
 CIUDG version 5.9.7 ( 5-Oct-2004)

 workspace allocation information: lcore=  13107199

 core energy values from the integral file:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -4.349858765627E+01, ietype=    6,   fcore energy of type: H1(*)   

 total core repulsion energy = -4.349858765627E+01
 nmot  =    38 niot  =     6 nfct  =     4 nfvt  =     0
 nrow  =    32 nsym  =     4 ssym  =     1 lenbuf=  1600
 nwalk,xbar:        211      105       70       15       21
 nvalwt,nvalw:      139       33       70       15       21
 ncsft:            4207
 total number of valid internal walks:     139
 nvalz,nvaly,nvalx,nvalw =       33      70      15      21

 cisrt info file parameters:
 file number  12 blocksize   1536
 mxbld   1536
 nd4ext,nd2ext,nd0ext   812   336    42
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int    24411    18720     6792     1272       99        0        0        0
 minbl4,minbl3,maxbl2   381   381   384
 maxbuf 30006
 number of external orbitals per symmetry block:  11   3   7   7
 nmsym   4 number of internal orbitals   6
 executing brd_struct for drt
 executing brd_struct for orbinf
 executing brd_struct for momap
 calcthrxt: niot,maxw1=                     6                    31
 block size     0
 pthz,pthy,pthx,pthw:   105    70    15    21 total internal walks:     211
 maxlp3,n2lp,n1lp,n0lp    31     0     0     0
 orbsym(*)= 1 1 1 2 3 4

 setref:       33 references kept,
               72 references were marked as invalid, out of
              105 total.
 nmb.of records onel     1
 nmb.of records 2-ext     5
 nmb.of records 1-ext     1
 nmb.of records 0-ext     1
 nmb.of records 2-int     0
 nmb.of records 1-int     0
 nmb.of records 0-int     0
 ---------memory usage in DP -----------------
 < n-ex core usage >
     routines:
    fourex            61797
    threx             60199
    twoex              3549
    onex               1823
    allin              1536
    diagon             2109
               =======
   maximum            61797
 
  __ static summary __ 
   reflst                33
   hrfspc                33
               -------
   static->              33
 
  __ core required  __ 
   totstc                33
   max n-ex           61797
               -------
   totnec->           61830
 
  __ core available __ 
   totspc          13107199
   totnec -           61830
               -------
   totvec->        13045369

 number of external paths / symmetry
 vertex x     100      82      98      98
 vertex w     128      82      98      98
segment: free space=    13045369
 reducing frespc by                   770 to               13044599 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1          46|        33|         0|        33|         0|         1|
 -------------------------------------------------------------------------------
  Y 2          70|       498|        33|        70|        33|         2|
 -------------------------------------------------------------------------------
  X 3          15|      1412|       531|        15|       103|         3|
 -------------------------------------------------------------------------------
  W 4          21|      2264|      1943|        21|       118|         4|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=          56DP  conft+indsym=         280DP  drtbuffer=         434 DP

dimension of the ci-matrix ->>>      4207

 executing brd_struct for civct
 gentasklist: ntask=                    20
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS   
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      15      46       1412         33      15      33
     2  4   1    25      two-ext wz   2X  4 1      21      46       2264         33      21      33
     3  4   3    26      two-ext wx*  WX  4 3      21      15       2264       1412      21      15
     4  4   3    27      two-ext wx+  WX  4 3      21      15       2264       1412      21      15
     5  2   1    11      one-ext yz   1X  2 1      70      46        498         33      70      33
     6  3   2    15      1ex3ex yx    3X  3 2      15      70       1412        498      15      70
     7  4   2    16      1ex3ex yw    3X  4 2      21      70       2264        498      21      70
     8  1   1     1      allint zz    OX  1 1      46      46         33         33      33      33
     9  2   2     5      0ex2ex yy    OX  2 2      70      70        498        498      70      70
    10  3   3     6      0ex2ex xx*   OX  3 3      15      15       1412       1412      15      15
    11  3   3    18      0ex2ex xx+   OX  3 3      15      15       1412       1412      15      15
    12  4   4     7      0ex2ex ww*   OX  4 4      21      21       2264       2264      21      21
    13  4   4    19      0ex2ex ww+   OX  4 4      21      21       2264       2264      21      21
    14  2   2    42      four-ext y   4X  2 2      70      70        498        498      70      70
    15  3   3    43      four-ext x   4X  3 3      15      15       1412       1412      15      15
    16  4   4    44      four-ext w   4X  4 4      21      21       2264       2264      21      21
    17  1   1    75      dg-024ext z  OX  1 1      46      46         33         33      33      33
    18  2   2    76      dg-024ext y  OX  2 2      70      70        498        498      70      70
    19  3   3    77      dg-024ext x  OX  3 3      15      15       1412       1412      15      15
    20  4   4    78      dg-024ext w  OX  4 4      21      21       2264       2264      21      21
----------------------------------------------------------------------------------------------------
REDTASK #   1 TIME=  19.000 N=  1 (task/type/sgbra)=(   1/24/0) (
REDTASK #   2 TIME=  18.000 N=  1 (task/type/sgbra)=(   2/25/0) (
REDTASK #   3 TIME=  17.000 N=  1 (task/type/sgbra)=(   3/26/1) (
REDTASK #   4 TIME=  16.000 N=  1 (task/type/sgbra)=(   4/27/2) (
REDTASK #   5 TIME=  15.000 N=  1 (task/type/sgbra)=(   5/11/0) (
REDTASK #   6 TIME=  14.000 N=  1 (task/type/sgbra)=(   6/15/0) (
REDTASK #   7 TIME=  13.000 N=  1 (task/type/sgbra)=(   7/16/0) (
REDTASK #   8 TIME=  12.000 N=  1 (task/type/sgbra)=(   8/ 1/0) (
REDTASK #   9 TIME=  11.000 N=  1 (task/type/sgbra)=(   9/ 5/0) (
REDTASK #  10 TIME=  10.000 N=  1 (task/type/sgbra)=(  10/ 6/1) (
REDTASK #  11 TIME=   9.000 N=  1 (task/type/sgbra)=(  11/18/2) (
REDTASK #  12 TIME=   8.000 N=  1 (task/type/sgbra)=(  12/ 7/1) (
REDTASK #  13 TIME=   7.000 N=  1 (task/type/sgbra)=(  13/19/2) (
REDTASK #  14 TIME=   6.000 N=  1 (task/type/sgbra)=(  14/42/1) (
REDTASK #  15 TIME=   5.000 N=  1 (task/type/sgbra)=(  15/43/1) (
REDTASK #  16 TIME=   4.000 N=  1 (task/type/sgbra)=(  16/44/1) (
REDTASK #  17 TIME=   3.000 N=  1 (task/type/sgbra)=(  17/75/1) (
REDTASK #  18 TIME=   2.000 N=  1 (task/type/sgbra)=(  18/76/1) (
REDTASK #  19 TIME=   1.000 N=  1 (task/type/sgbra)=(  19/77/1) (
REDTASK #  20 TIME=   0.000 N=  1 (task/type/sgbra)=(  20/78/1) (
 initializing v-file: 1:                  4207

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         177 2x:           0 4x:           0
All internal counts: zz :         345 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 reference space has dimension      33
 dsyevx: computed roots 1 to    2(converged:   2)

    root           eigenvalues
    ----           ------------
       1         -46.2740792806
       2         -46.2740792722

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt=                    33

         vector  1 from unit 11 written to unit 49 filename cirefv              

 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=    33)...
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              4207
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                    1
 residual norm convergence criteria:               0.001000

          starting bk iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         603 2x:         246 4x:         106
All internal counts: zz :         345 yy:           0 xx:           0 ww:           0
One-external counts: yz :        1106 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:          69 wz:         123 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1
   ht   1    -2.77549162

          calcsovref: tciref block   1

              civs   1
 refs   1    1.00000    

          calcsovref: scrb block   1

                ci   1
 civs   1    1.00000    

          calcsovref: sovref block   1

              v      1
 ref    1    1.00000    

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.2740792806  6.2172E-15  5.5935E-02  2.2790E-01  1.0000E-03
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000999
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.2740792806  6.2172E-15  5.5935E-02  2.2790E-01  1.0000E-03
 
diagon:itrnv=   2
 expansion vectors are not transformed.
 matrix-vector products are not transformed.

    2 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              4207
 number of initial trial vectors:                         2
 number of initial matrix-vector products:                1
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                   30
 residual norm convergence criteria:               0.001000

          starting ci iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         603 2x:         246 4x:         106
All internal counts: zz :         345 yy:         620 xx:          66 ww:         132
One-external counts: yz :        1106 yx:        1000 yw:        1210
Two-external counts: yy :         865 ww:         252 xx:         180 xz:          69 wz:         123 wx:         360
Three-ext.   counts: yx :         319 yw:         409

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2
   ht   1    -2.77549162
   ht   2    -0.05593494    -0.17611796

          calcsovref: tciref block   1

              civs   1       civs   2
 refs   1    1.00000      -6.864011E-15

          calcsovref: scrb block   1

                ci   1         ci   2
 civs   1   0.968958      -0.247227    
 civs   2   0.848075        3.32386    

          calcsovref: sovref block   1

              v      1       v      2
 ref    1   0.968958      -0.247227    

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.96895753    -0.24722722

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.3230360383  4.8957E-02  2.3161E-03  4.4693E-02  1.0000E-03
 mr-sdci #  1  2    -45.5220589925  2.0235E+00  0.0000E+00  5.3477E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   2

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         603 2x:         246 4x:         106
All internal counts: zz :         345 yy:         620 xx:          66 ww:         132
One-external counts: yz :        1106 yx:        1000 yw:        1210
Two-external counts: yy :         865 ww:         252 xx:         180 xz:          69 wz:         123 wx:         360
Three-ext.   counts: yx :         319 yw:         409

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3
   ht   1    -2.77549162
   ht   2    -0.05593494    -0.17611796
   ht   3     0.00529531    -0.00361640    -0.00796271

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3
 refs   1    1.00000      -6.864011E-15  -1.901808E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3
 civs   1   0.965451      -0.161146       0.207229    
 civs   2   0.887434        1.59175       -2.90699    
 civs   3    1.09868        14.2877        8.54679    

          calcsovref: sovref block   1

              v      1       v      2       v      3
 ref    1   0.963361      -0.188319       0.190975    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.96336115    -0.18831886     0.19097463

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1    -46.3255865141  2.5505E-03  3.3928E-04  1.6793E-02  1.0000E-03
 mr-sdci #  2  2    -45.8025711422  2.8051E-01  0.0000E+00  4.4072E-01  1.0000E-04
 mr-sdci #  2  3    -45.4218893486  1.9233E+00  0.0000E+00  5.4035E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   3

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         603 2x:         246 4x:         106
All internal counts: zz :         345 yy:         620 xx:          66 ww:         132
One-external counts: yz :        1106 yx:        1000 yw:        1210
Two-external counts: yy :         865 ww:         252 xx:         180 xz:          69 wz:         123 wx:         360
Three-ext.   counts: yx :         319 yw:         409

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4
   ht   1    -2.77549162
   ht   2    -0.05593494    -0.17611796
   ht   3     0.00529531    -0.00361640    -0.00796271
   ht   4    -0.00006926    -0.00310184    -0.00039008    -0.00131337

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4
 refs   1    1.00000      -6.864011E-15  -1.901808E-03   4.490314E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4
 civs   1   0.963449       0.156755       4.643113E-02  -0.214593    
 civs   2   0.891799       -1.03383      -0.925098        3.05280    
 civs   3    1.23397       -11.6316       -9.29203       -7.43731    
 civs   4   0.933142       -22.8170        33.7077       -4.73119    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4
 ref    1   0.961144       0.177851       6.561637E-02  -0.200661    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.96114434     0.17785114     0.06561637    -0.20066148

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1    -46.3259032066  3.1669E-04  6.1580E-05  7.2353E-03  1.0000E-03
 mr-sdci #  3  2    -45.9571422882  1.5457E-01  0.0000E+00  2.8318E-01  1.0000E-04
 mr-sdci #  3  3    -45.4594197912  3.7530E-02  0.0000E+00  5.8701E-01  1.0000E-04
 mr-sdci #  3  4    -45.4211730088  1.9226E+00  0.0000E+00  5.4143E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   4

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         603 2x:         246 4x:         106
All internal counts: zz :         345 yy:         620 xx:          66 ww:         132
One-external counts: yz :        1106 yx:        1000 yw:        1210
Two-external counts: yy :         865 ww:         252 xx:         180 xz:          69 wz:         123 wx:         360
Three-ext.   counts: yx :         319 yw:         409

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1    -2.77549162
   ht   2    -0.05593494    -0.17611796
   ht   3     0.00529531    -0.00361640    -0.00796271
   ht   4    -0.00006926    -0.00310184    -0.00039008    -0.00131337
   ht   5     0.00234798    -0.00039570    -0.00042612     0.00009313    -0.00026874

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1    1.00000      -6.864011E-15  -1.901808E-03   4.490314E-05  -8.430987E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 civs   1   0.963507       0.122994       0.138924      -0.195125      -7.231842E-02
 civs   2   0.892304      -0.803908      -0.999115        2.76192        1.42983    
 civs   3    1.25476       -10.1739       0.316692        3.63886       -13.2499    
 civs   4    1.07727       -23.2727       -15.5419       -28.3108        13.4650    
 civs   5   0.795180       -32.5835        70.2054       -19.1453        49.3953    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1   0.960499       0.168769       7.843389E-02  -0.187175      -8.816019E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     0.96049902     0.16876879     0.07843389    -0.18717542    -0.08816019

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1    -46.3259521761  4.8969E-05  6.3229E-06  2.2811E-03  1.0000E-03
 mr-sdci #  4  2    -46.0178299371  6.0688E-02  0.0000E+00  1.7679E-01  1.0000E-04
 mr-sdci #  4  3    -45.5653340878  1.0591E-01  0.0000E+00  6.3329E-01  1.0000E-04
 mr-sdci #  4  4    -45.4398521227  1.8679E-02  0.0000E+00  5.6229E-01  1.0000E-04
 mr-sdci #  4  5    -45.3772841859  1.8787E+00  0.0000E+00  5.7105E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000999
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   5

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         603 2x:         246 4x:         106
All internal counts: zz :         345 yy:         620 xx:          66 ww:         132
One-external counts: yz :        1106 yx:        1000 yw:        1210
Two-external counts: yy :         865 ww:         252 xx:         180 xz:          69 wz:         123 wx:         360
Three-ext.   counts: yx :         319 yw:         409

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1    -2.77549162
   ht   2    -0.05593494    -0.17611796
   ht   3     0.00529531    -0.00361640    -0.00796271
   ht   4    -0.00006926    -0.00310184    -0.00039008    -0.00131337
   ht   5     0.00234798    -0.00039570    -0.00042612     0.00009313    -0.00026874
   ht   6    -0.00029257     0.00018654    -0.00001594    -0.00003361     0.00001497    -0.00002704

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1    1.00000      -6.864011E-15  -1.901808E-03   4.490314E-05  -8.430987E-04   1.041646E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1  -0.963452       0.112643       9.964238E-02  -0.128077      -0.176591       8.916274E-02
 civs   2  -0.892341      -0.674250       -1.14493       0.854557        2.47172       -1.66745    
 civs   3   -1.25729       -9.32817       -5.03960      -0.651531        3.55392        12.9770    
 civs   4   -1.09495       -22.9012       0.491539        16.6055       -29.4978       -10.7598    
 civs   5  -0.892775       -37.5515        19.8459       -68.4083       -18.4474       -47.8933    
 civs   6  -0.950707       -73.4314        259.939        14.2062        118.778       -11.3267    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.960456       0.153365       0.119593      -6.693795E-02  -0.156749       0.103199    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.96045590     0.15336533     0.11959314    -0.06693795    -0.15674929     0.10319879

 trial vector basis is being transformed.  new dimension:   2

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1    -46.3259581873  6.0112E-06  6.4156E-07  7.6901E-04  1.0000E-03
 mr-sdci #  5  2    -46.0377562149  1.9926E-02  0.0000E+00  1.4279E-01  1.0000E-04
 mr-sdci #  5  3    -45.7452646410  1.7993E-01  0.0000E+00  4.7325E-01  1.0000E-04
 mr-sdci #  5  4    -45.5646846577  1.2483E-01  0.0000E+00  6.4212E-01  1.0000E-04
 mr-sdci #  5  5    -45.3784646339  1.1804E-03  0.0000E+00  5.6227E-01  1.0000E-04
 mr-sdci #  5  6    -45.3772736219  1.8787E+00  0.0000E+00  5.6873E-01  1.0000E-04
 
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mr-sdci  convergence criteria satisfied after  5 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1    -46.3259581873  6.0112E-06  6.4156E-07  7.6901E-04  1.0000E-03
 mr-sdci #  5  2    -46.0377562149  1.9926E-02  0.0000E+00  1.4279E-01  1.0000E-04

####################CIUDGINFO####################

   ci vector at position   1 energy=  -46.325958187267

################END OF CIUDGINFO################

 
diagon:itrnv=   0
    1 of the   3 expansion vectors are transformed.
    1 of the   2 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
maximum overlap with reference    1(overlap= 0.96046)

 information on vector: 1 from unit 11 written to unit 48 filename civout              


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -46.3259581873

                                                       internal orbitals

                                          level       1    2    3    4    5    6

                                          orbital     3    4    5   17   22   31

                                         symmetry   a1   a1   a1   a2   b1   b2 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1  0.460011                        +-   +-                     
 z*  1  1       2  0.241490                        +-   +     -                
 z*  1  1       3 -0.459993                        +-        +-                
 z*  1  1       4  0.209939                        +-             +-           
 z*  1  1       5  0.279316                        +-                  +-      
 z*  1  1       6 -0.489203                        +-                       +- 
 z*  1  1       7 -0.040575                        +    +-    -                
 z*  1  1       8  0.131613                        +     -   +-                
 z*  1  1      10 -0.034340                        +     -             +-      
 z*  1  1      11  0.135293                        +     -                  +- 
 z*  1  1      12 -0.088488                        +          -   +-           
 z*  1  1      13  0.092999                        +          -        +-      
 z*  1  1      14  0.023882                        +          -             +- 
 z*  1  1      15  0.055428                        +               -   +     - 
 z*  1  1      16  0.117304                        +              +     -    - 
 z*  1  1      18 -0.034114                             +-        +-           
 z*  1  1      19 -0.026664                             +-             +-      
 z*  1  1      22 -0.025594                             +     -        +-      
 z*  1  1      25 -0.016968                             +         +     -    - 
 z*  1  1      26  0.024656                                  +-   +-           
 z*  1  1      27  0.014040                                  +-        +-      
 z*  1  1      28  0.022181                                  +-             +- 
 z*  1  1      29 -0.016941                                  +     -   +     - 
 z*  1  1      31 -0.042608                                       +-   +-      
 z*  1  1      32  0.024349                                       +-        +- 
 z*  1  1      33  0.018273                                            +-   +- 
 y   1  1      36 -0.018430              3( a1 )   +-    -                     
 y   1  1      38  0.055933              5( a1 )   +-    -                     
 y   1  1      40  0.013913              7( a1 )   +-    -                     
 y   1  1      46  0.018438              2( a1 )   +-         -                
 y   1  1      49  0.017272              5( a1 )   +-         -                
 y   1  1      50  0.013919              6( a1 )   +-         -                
 y   1  1      60 -0.010992              2( b1 )   +-                   -      
 y   1  1      67  0.019243              2( b2 )   +-                        - 
 y   1  1      69 -0.015180              4( b2 )   +-                        - 
 y   1  1      77 -0.015195              5( a1 )    -   +-                     
 y   1  1     116  0.015194              5( a1 )    -        +-                
 y   1  1     118 -0.012389              7( a1 )    -        +-                
 y   1  1     152 -0.013916              2( b2 )    -             +     -      
 y   1  1     154  0.014001              4( b2 )    -             +     -      
 y   1  1     166  0.010816              2( a1 )    -                  +-      
 y   1  1     181  0.011504              3( a1 )    -                       +- 
 y   1  1     183  0.016162              5( a1 )    -                       +- 
 y   1  1     185 -0.013328              7( a1 )    -                       +- 
 y   1  1     191 -0.010574              2( a1 )   +     -    -                
 y   1  1     195 -0.015341              6( a1 )   +     -    -                
 y   1  1     212 -0.011061              2( b2 )   +     -                   - 
 y   1  1     214  0.016867              4( b2 )   +     -                   - 
 y   1  1     219 -0.010118              2( a2 )   +          -    -           
 y   1  1     224  0.011231              4( b1 )   +          -         -      
 y   1  1     245  0.010221              4( b1 )   +               -         - 
 y   1  1     250  0.011242              2( a2 )   +                    -    - 
 y   1  1     263  0.010021              1( a2 )        +-         -           
 y   1  1     264 -0.011225              2( a2 )        +-         -           
 y   1  1     393  0.010206              2( a2 )             +-    -           
 y   1  1     477  0.010196              2( b1 )                  +-    -      
 y   1  1     479 -0.010979              4( b1 )                  +-    -      
 y   1  1     490 -0.010540              1( a2 )                   -   +-      
 y   1  1     491  0.011754              2( a2 )                   -   +-      
 y   1  1     505 -0.010379              2( a2 )                   -        +- 
 x   1  1     540  0.016069    3( a1 )   5( a1 )    -    -                     
 x   1  1     551  0.015576    5( a1 )   7( a1 )    -    -                     
 x   1  1     605 -0.010995    1( b1 )   7( b1 )    -    -                     
 x   1  1     607 -0.011684    3( b1 )   7( b1 )    -    -                     
 x   1  1     626 -0.013213    1( b2 )   7( b2 )    -    -                     
 x   1  1     628 -0.013488    3( b2 )   7( b2 )    -    -                     
 x   1  1     639 -0.016074    2( a1 )   5( a1 )    -         -                
 x   1  1     646  0.015579    5( a1 )   6( a1 )    -         -                
 x   1  1     660  0.011742    1( a1 )   9( a1 )    -         -                
 x   1  1     663  0.012297    4( a1 )   9( a1 )    -         -                
 x   1  1     700  0.010965    1( b1 )   6( b1 )    -         -                
 x   1  1     702  0.011343    3( b1 )   6( b1 )    -         -                
 x   1  1     721  0.010643    1( b2 )   6( b2 )    -         -                
 x   1  1     723  0.010867    3( b2 )   6( b2 )    -         -                
 x   1  1     914  0.013260    3( a2 )   1( b1 )    -                        - 
 x   1  1     920  0.013643    3( a2 )   3( b1 )    -                        - 
 x   1  1     942  0.011967   10( a1 )   1( b2 )    -                        - 
 x   1  1     948  0.016755    5( a1 )   2( b2 )    -                        - 
 x   1  1     964  0.012539   10( a1 )   3( b2 )    -                        - 
 x   1  1     970 -0.016963    5( a1 )   4( b2 )    -                        - 
 x   1  1     988 -0.012894    1( a1 )   6( b2 )    -                        - 
 x   1  1     991 -0.013369    4( a1 )   6( b2 )    -                        - 
 x   1  1    1143 -0.010206    1( b1 )   1( b2 )         -         -           
 x   1  1    1589  0.011070    1( a1 )   1( b2 )              -              - 
 x   1  1    1687 -0.010865    1( a1 )   1( b2 )                   -    -      
 w   1  1    1950  0.012674    1( a1 )   4( a1 )   +-                          
 w   1  1    1961  0.012563    3( a1 )   6( a1 )   +-                          
 w   1  1    1966 -0.012564    2( a1 )   7( a1 )   +-                          
 w   1  1    1970 -0.022719    6( a1 )   7( a1 )   +-                          
 w   1  1    1997 -0.022496    9( a1 )  10( a1 )   +-                          
 w   1  1    2016 -0.022504    1( b1 )   1( b1 )   +-                          
 w   1  1    2019 -0.029496    1( b1 )   3( b1 )   +-                          
 w   1  1    2021 -0.020370    3( b1 )   3( b1 )   +-                          
 w   1  1    2026 -0.011919    1( b1 )   5( b1 )   +-                          
 w   1  1    2028 -0.013588    3( b1 )   5( b1 )   +-                          
 w   1  1    2042  0.011776    6( b1 )   7( b1 )   +-                          
 w   1  1    2044  0.012843    1( b2 )   1( b2 )   +-                          
 w   1  1    2047  0.016832    1( b2 )   3( b2 )   +-                          
 w   1  1    2049  0.011626    3( b2 )   3( b2 )   +-                          
 w   1  1    2051 -0.014345    2( b2 )   4( b2 )   +-                          
 w   1  1    2053  0.017334    4( b2 )   4( b2 )   +-                          
 w   1  1    2064  0.016356    6( b2 )   6( b2 )   +-                          
 w   1  1    2070  0.015152    6( b2 )   7( b2 )   +-                          
 w   1  1    2144 -0.036381    1( b1 )   1( b1 )   +     -                     
 w   1  1    2147 -0.035149    1( b1 )   3( b1 )   +     -                     
 w   1  1    2149 -0.018923    3( b1 )   3( b1 )   +     -                     
 w   1  1    2167  0.010304    3( b1 )   7( b1 )   +     -                     
 w   1  1    2175  0.013042    1( b2 )   3( b2 )   +     -                     
 w   1  1    2200  0.023751    1( a1 )   1( a1 )   +          -                
 w   1  1    2206  0.026164    1( a1 )   4( a1 )   +          -                
 w   1  1    2209  0.015323    4( a1 )   4( a1 )   +          -                
 w   1  1    2300 -0.024963    1( b2 )   1( b2 )   +          -                
 w   1  1    2303 -0.025385    1( b2 )   3( b2 )   +          -                
 w   1  1    2305 -0.014150    3( b2 )   3( b2 )   +          -                
 w   1  1    2361  0.014898    1( b1 )   1( b2 )   +               -           
 w   1  1    2363  0.011693    3( b1 )   1( b2 )   +               -           
 w   1  1    2375  0.010601    1( b1 )   3( b2 )   +               -           
 w   1  1    2410  0.019834    1( a1 )   1( b1 )   +                    -      
 w   1  1    2413  0.014341    4( a1 )   1( b1 )   +                    -      
 w   1  1    2432  0.015340    1( a1 )   3( b1 )   +                    -      
 w   1  1    2435  0.011994    4( a1 )   3( b1 )   +                    -      
 w   1  1    2529 -0.034733    1( a1 )   1( b2 )   +                         - 
 w   1  1    2532 -0.025939    4( a1 )   1( b2 )   +                         - 
 w   1  1    2544 -0.010227    5( a1 )   2( b2 )   +                         - 
 w   1  1    2551 -0.026036    1( a1 )   3( b2 )   +                         - 
 w   1  1    2554 -0.021003    4( a1 )   3( b2 )   +                         - 
 w   1  1    2560  0.010777   10( a1 )   3( b2 )   +                         - 
 w   1  1    2584  0.010560    1( a1 )   6( b2 )   +                         - 
 w   1  1    2587  0.011315    4( a1 )   6( b2 )   +                         - 
 w   1  1    2606 -0.033268    1( a1 )   1( a1 )        +-                     
 w   1  1    2612 -0.029224    1( a1 )   4( a1 )        +-                     
 w   1  1    2615 -0.015185    4( a1 )   4( a1 )        +-                     
 w   1  1    2620 -0.011266    5( a1 )   5( a1 )        +-                     
 w   1  1    2678 -0.033027    1( b1 )   1( b1 )        +-                     
 w   1  1    2681 -0.027504    1( b1 )   3( b1 )        +-                     
 w   1  1    2683 -0.013755    3( b1 )   3( b1 )        +-                     
 w   1  1    2706 -0.039747    1( b2 )   1( b2 )        +-                     
 w   1  1    2709 -0.035149    1( b2 )   3( b2 )        +-                     
 w   1  1    2711 -0.018179    3( b2 )   3( b2 )        +-                     
 w   1  1    2734 -0.023054    1( a1 )   1( a1 )        +     -                
 w   1  1    2740 -0.020098    1( a1 )   4( a1 )        +     -                
 w   1  1    2743 -0.010234    4( a1 )   4( a1 )        +     -                
 w   1  1    2806 -0.022946    1( b1 )   1( b1 )        +     -                
 w   1  1    2809 -0.020000    1( b1 )   3( b1 )        +     -                
 w   1  1    2811 -0.010188    3( b1 )   3( b1 )        +     -                
 w   1  1    3063  0.010746    1( a1 )   1( b2 )        +                    - 
 w   1  1    3140  0.026566    1( a1 )   1( a1 )             +-                
 w   1  1    3146  0.022357    1( a1 )   4( a1 )             +-                
 w   1  1    3149  0.011434    4( a1 )   4( a1 )             +-                
 w   1  1    3154  0.011270    5( a1 )   5( a1 )             +-                
 w   1  1    3212  0.048635    1( b1 )   1( b1 )             +-                
 w   1  1    3215  0.043511    1( b1 )   3( b1 )             +-                
 w   1  1    3217  0.022501    3( b1 )   3( b1 )             +-                
 w   1  1    3240  0.030872    1( b2 )   1( b2 )             +-                
 w   1  1    3243  0.026035    1( b2 )   3( b2 )             +-                
 w   1  1    3245  0.013203    3( b2 )   3( b2 )             +-                
 w   1  1    3546 -0.022173    1( a1 )   1( a1 )                  +-           
 w   1  1    3552 -0.019805    1( a1 )   4( a1 )                  +-           
 w   1  1    3555 -0.010233    4( a1 )   4( a1 )                  +-           
 w   1  1    3618 -0.016174    1( b1 )   1( b1 )                  +-           
 w   1  1    3621 -0.013290    1( b1 )   3( b1 )                  +-           
 w   1  1    3646 -0.010050    1( b2 )   1( b2 )                  +-           
 w   1  1    3695  0.013535    1( a1 )   1( b2 )                  +     -      
 w   1  1    3870 -0.014667    1( a1 )   1( a1 )                       +-      
 w   1  1    3876 -0.012691    1( a1 )   4( a1 )                       +-      
 w   1  1    3942 -0.020260    1( b1 )   1( b1 )                       +-      
 w   1  1    3945 -0.016777    1( b1 )   3( b1 )                       +-      
 w   1  1    3970 -0.029507    1( b2 )   1( b2 )                       +-      
 w   1  1    3973 -0.026363    1( b2 )   3( b2 )                       +-      
 w   1  1    3975 -0.013620    3( b2 )   3( b2 )                       +-      
 w   1  1    4080  0.030297    1( a1 )   1( a1 )                            +- 
 w   1  1    4086  0.025590    1( a1 )   4( a1 )                            +- 
 w   1  1    4089  0.013062    4( a1 )   4( a1 )                            +- 
 w   1  1    4094  0.011990    5( a1 )   5( a1 )                            +- 
 w   1  1    4152  0.051689    1( b1 )   1( b1 )                            +- 
 w   1  1    4155  0.046177    1( b1 )   3( b1 )                            +- 
 w   1  1    4157  0.023859    3( b1 )   3( b1 )                            +- 
 w   1  1    4180  0.030849    1( b2 )   1( b2 )                            +- 
 w   1  1    4183  0.025993    1( b2 )   3( b2 )                            +- 
 w   1  1    4185  0.013226    3( b2 )   3( b2 )                            +- 

 ci coefficient statistics:
           rq > 0.1                9
      0.1> rq > 0.01             171
     0.01> rq > 0.001            979
    0.001> rq > 0.0001           764
   0.0001> rq > 0.00001          228
  0.00001> rq > 0.000001          20
 0.000001> rq                   2036
           all                  4207
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         177 2x:           0 4x:           0
All internal counts: zz :         345 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.460011240890    -21.286773305482
     2     2      0.241490046893    -11.174813386397
     3     3     -0.459993094036     21.285937335099
     4     4      0.209938741950     -9.714802838510
     5     5      0.279316091489    -12.925196587255
     6     6     -0.489203290568     22.637620540976
     7     7     -0.040574783381      1.877304527613
     8     8      0.131613095751     -6.089463337890
     9     9     -0.009406253625      0.436358215040
    10    10     -0.034340217410      1.590219850984
    11    11      0.135293047670     -6.259763081401
    12    12     -0.088487944376      4.095220510097
    13    13      0.092998882785     -4.303291373291
    14    14      0.023881971421     -1.104830205496
    15    15      0.055428365689     -2.565008857124
    16    16      0.117303557857     -5.428336798023
    17    17      0.000007708506     -0.000353120674
    18    18     -0.034113827105      1.577375978379
    19    19     -0.026663836691      1.232790796162
    20    20     -0.000051442229      0.002395766823
    21    21     -0.003110799609      0.143752435342
    22    22     -0.025594018847      1.183545051412
    23    23     -0.003237982235      0.149643607942
    24    24     -0.006894825281      0.318873120187
    25    25     -0.016967812516      0.784713454963
    26    26      0.024655713320     -1.140150074766
    27    27      0.014039807658     -0.649220331809
    28    28      0.022181234749     -1.025371116470
    29    29     -0.016941334959      0.783498932449
    30    30      0.006905129686     -0.319346970401
    31    31     -0.042607503137      1.970274116371
    32    32      0.024349403286     -1.125965125733
    33    33      0.018272914632     -0.844984882674

 number of reference csfs (nref) is    33.  root number (iroot) is  1.
 c0**2 =   0.92261427  c**2 (all zwalks) =   0.92261427

 pople ci energy extrapolation is computed with  4 correlated electrons.

 eref      =    -46.274036773044   "relaxed" cnot**2         =   0.922614273433
 eci       =    -46.325958187267   deltae = eci - eref       =  -0.051921414223
 eci+dv1   =    -46.329976163631   dv1 = (1-cnot**2)*deltae  =  -0.004017976364
 eci+dv2   =    -46.330313177732   dv2 = dv1 / cnot**2       =  -0.004354990465
 eci+dv3   =    -46.330711903145   dv3 = dv1 / (2*cnot**2-1) =  -0.004753715878
 eci+pople =    -46.328131866090   (  4e- scaled deltae )    =  -0.054095093046
 passed aftci ... 
 readint2: molcas,dalton2=                     0                     0
 files%faoints=aoints              
lodens (list->root)=  1
sifcfg setup: record length 4096 DP
# d1 elements per record  3272
# d2 elements per record  2730
  The MR-CISD density will be calculated.
 item #                     1 suffix=:.drt1.state1:
 read_civout: repnuc=  -43.4985876562748     
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method:  0 last record  1max overlap with ref# 96% root-following 0
 MR-CISD energy:   -46.32595819    -2.82737053
 residuum:     0.00076901
 deltae:     0.00000601
 apxde:     0.00000064

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.96045590     0.15336533     0.11959314    -0.06693795    -0.15674929     0.10319879     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.96045590     0.15336533     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 computing final density
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=     113  DYX=     130  DYW=     160
   D0Z=      42  D0Y=     103  D0X=      12  D0W=      18
  DDZI=      90 DDYI=     180 DDXI=      30 DDWI=      36
  DDZE=       0 DDYE=      70 DDXE=      15 DDWE=      21
================================================================================
Trace of MO density:     4.000000
    4  correlated and     8  frozen core electrons

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     1.81011333    -0.20639191    -0.01148566     0.00000000     0.01208281    -0.01785653
   MO   4     0.00000000     0.00000000    -0.20639191     0.55878481    -0.00344316     0.00000000    -0.01081336     0.01391582
   MO   5     0.00000000     0.00000000    -0.01148566    -0.00344316     0.56893072     0.00000000     0.01460633     0.01085329
   MO   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.01823183     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000     0.01208281    -0.01081336     0.01460633     0.00000000     0.00171537    -0.00002436
   MO   8     0.00000000     0.00000000    -0.01785653     0.01391582     0.01085329     0.00000000    -0.00002436     0.00177279
   MO   9     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.01297110     0.00000000     0.00000000
   MO  10     0.00000000     0.00000000    -0.03922430    -0.03453392    -0.00193865     0.00000000     0.00071933    -0.00106602
   MO  11     0.00000000     0.00000000     0.01153953    -0.01076774     0.01141419     0.00000000     0.00181677    -0.00024353
   MO  12     0.00000000     0.00000000     0.01334661    -0.01098554    -0.01084100     0.00000000    -0.00019186    -0.00186458
   MO  13     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00278138     0.00000000     0.00000000
   MO  14     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00049602     0.00000000     0.00000000
   MO  15     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00002631     0.00000000     0.00000000
   MO  16     0.00000000     0.00000000     0.00036522     0.00156645     0.00008779     0.00000000    -0.00003887     0.00005754

                MO   9         MO  10         MO  11         MO  12         MO  13         MO  14         MO  15         MO  16
   MO   3     0.00000000    -0.03922430     0.01153953     0.01334661     0.00000000     0.00000000     0.00000000     0.00036522
   MO   4     0.00000000    -0.03453392    -0.01076774    -0.01098554     0.00000000     0.00000000     0.00000000     0.00156645
   MO   5     0.00000000    -0.00193865     0.01141419    -0.01084100     0.00000000     0.00000000     0.00000000     0.00008779
   MO   6     0.01297110     0.00000000     0.00000000     0.00000000     0.00278138     0.00049602     0.00002631     0.00000000
   MO   7     0.00000000     0.00071933     0.00181677    -0.00019186     0.00000000     0.00000000     0.00000000    -0.00003887
   MO   8     0.00000000    -0.00106602    -0.00024353    -0.00186458     0.00000000     0.00000000     0.00000000     0.00005754
   MO   9     0.00963210     0.00000000     0.00000000     0.00000000     0.00234642     0.00020172    -0.00008582     0.00000000
   MO  10     0.00000000     0.00848900     0.00071963     0.00083472     0.00000000     0.00000000     0.00000000    -0.00037968
   MO  11     0.00000000     0.00071963     0.00219883     0.00005088     0.00000000     0.00000000     0.00000000    -0.00004299
   MO  12     0.00000000     0.00083472     0.00005088     0.00227322     0.00000000     0.00000000     0.00000000    -0.00004980
   MO  13     0.00234642     0.00000000     0.00000000     0.00000000     0.00079060    -0.00000457    -0.00007156     0.00000000
   MO  14     0.00020172     0.00000000     0.00000000     0.00000000    -0.00000457     0.00179524    -0.00038253     0.00000000
   MO  15    -0.00008582     0.00000000     0.00000000     0.00000000    -0.00007156    -0.00038253     0.00208699     0.00000000
   MO  16     0.00000000    -0.00037968    -0.00004299    -0.00004980     0.00000000     0.00000000     0.00000000     0.00002207

Natural orbital populations,block 1
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     2.00000000     1.84449779     0.57049710     0.52879549     0.02808988     0.00504736     0.00279235
              MO     9       MO    10       MO    11       MO    12       MO    13       MO    14       MO    15       MO    16
  occ(*)=     0.00249637     0.00234658     0.00155802     0.00052896     0.00009269     0.00007781     0.00001333     0.00000318

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4
   MO   1     0.13962002     0.00675560    -0.00724701     0.00000000
   MO   2     0.00675560     0.00085132    -0.00097572     0.00000000
   MO   3    -0.00724701    -0.00097572     0.00119664     0.00000000
   MO   4     0.00000000     0.00000000     0.00000000     0.00089768

Natural orbital populations,block 2
              MO     1       MO     2       MO     3       MO     4       MO
  occ(*)=     0.14032966     0.00130822     0.00089768     0.00003011

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     0.21393085     0.00000000     0.00875295     0.00000000    -0.00872868     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.02948787     0.00000000     0.02061532     0.00000000     0.00449403     0.00228082
   MO   4     0.00000000     0.00875295     0.00000000     0.00100038     0.00000000    -0.00112253     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.02061532     0.00000000     0.01484670     0.00000000     0.00358785     0.00173813
   MO   6     0.00000000    -0.00872868     0.00000000    -0.00112253     0.00000000     0.00136655     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000     0.00449403     0.00000000     0.00358785     0.00000000     0.00117964     0.00057797
   MO   8     0.00000000     0.00000000     0.00228082     0.00000000     0.00173813     0.00000000     0.00057797     0.00139083
   MO   9     0.00000000     0.00000000    -0.00258158     0.00000000    -0.00201588     0.00000000    -0.00069001    -0.00047750

                MO   9
   MO   2     0.00000000
   MO   3    -0.00258158
   MO   4     0.00000000
   MO   5    -0.00201588
   MO   6     0.00000000
   MO   7    -0.00069001
   MO   8    -0.00047750
   MO   9     0.00135817

Natural orbital populations,block 3
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     0.21465045     0.04525143     0.00160822     0.00155827     0.00090759     0.00053442     0.00003911
              MO     9       MO
  occ(*)=     0.00001149

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     0.56691947     0.00000000     0.01819971     0.00000000    -0.01574731     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.02017221     0.00000000     0.01428923     0.00000000    -0.00307674     0.00027939
   MO   4     0.00000000     0.01819971     0.00000000     0.00170268     0.00000000    -0.00181465     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.01428923     0.00000000     0.01053156     0.00000000    -0.00256053     0.00012356
   MO   6     0.00000000    -0.01574731     0.00000000    -0.00181465     0.00000000     0.00216877     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000    -0.00307674     0.00000000    -0.00256053     0.00000000     0.00085766    -0.00000467
   MO   8     0.00000000     0.00000000     0.00027939     0.00000000     0.00012356     0.00000000    -0.00000467     0.00221601
   MO   9     0.00000000     0.00000000     0.00007166     0.00000000     0.00025521     0.00000000    -0.00016635     0.00034426

                MO   9
   MO   2     0.00000000
   MO   3     0.00007166
   MO   4     0.00000000
   MO   5     0.00025521
   MO   6     0.00000000
   MO   7    -0.00016635
   MO   8     0.00034426
   MO   9     0.00146809

Natural orbital populations,block 4
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     0.56794598     0.03096671     0.00276457     0.00234599     0.00139569     0.00052281     0.00008037
              MO     9       MO
  occ(*)=     0.00001432


 total number of electrons =   12.0000000000

 test slabel:                     4
  a1  a2  b1  b2 


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        a1  partial gross atomic populations
   ao class       1a1        2a1        3a1        4a1        5a1        6a1 
     3_ s       2.000000   0.000000   1.797647   0.000083   0.016152   0.000000
     3_ p       0.000000   2.000000   0.000000   0.000000   0.000000   0.028079
     3_ d       0.000000   0.000000   0.046851   0.570414   0.512643   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000000   0.000000   0.000011
 
   ao class       7a1        8a1        9a1       10a1       11a1       12a1 
     3_ s       0.004470   0.000010   0.000258   0.000000   0.000000   0.000000
     3_ p       0.000000   0.000000   0.000000   0.000000   0.000049   0.000513
     3_ d       0.000577   0.002782   0.002239   0.000000   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000000   0.002346   0.001509   0.000016
 
   ao class      13a1       14a1       15a1       16a1 
     3_ s       0.000000   0.000000   0.000000   0.000003
     3_ p       0.000000   0.000000   0.000013   0.000000
     3_ d       0.000092   0.000078   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000000

                        a2  partial gross atomic populations
   ao class       1a2        2a2        3a2        4a2 
     3_ d       0.140330   0.001308   0.000000   0.000030
     3_ f       0.000000   0.000000   0.000898   0.000000

                        b1  partial gross atomic populations
   ao class       1b1        2b1        3b1        4b1        5b1        6b1 
     3_ p       2.000000   0.000000   0.044785   0.000000   0.000249   0.000031
     3_ d       0.000000   0.214650   0.000000   0.001608   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000466   0.000000   0.001309   0.000876
 
   ao class       7b1        8b1        9b1 
     3_ p       0.000438   0.000000   0.000011
     3_ d       0.000000   0.000039   0.000000
     3_ f       0.000097   0.000000   0.000000

                        b2  partial gross atomic populations
   ao class       1b2        2b2        3b2        4b2        5b2        6b2 
     3_ p       2.000000   0.000000   0.030961   0.000000   0.000001   0.000099
     3_ d       0.000000   0.567946   0.000000   0.002765   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000005   0.000000   0.002345   0.001297
 
   ao class       7b2        8b2        9b2 
     3_ p       0.000486   0.000000   0.000014
     3_ d       0.000000   0.000080   0.000000
     3_ f       0.000037   0.000000   0.000000


                        gross atomic populations
     ao            3_
      s         3.818624
      p         6.105730
      d         2.064432
      f         0.011213
    total      12.000000
 

 Total number of electrons:   12.00000000

 DA ...
