1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      2009-03.    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


================ Computing sorting integral file structure ================

                    -----z----- -----y----- -----x----- -----w----- ---total---

                CSFs        28         638        3352        1542        5560
      internal walks       105          90          36          15         246
valid internal walks        28          90          36          15         169
 getinfoarray: info=                     6 :                     1
                  8192                  6552                  8192
                  5460                     0
 icd(3)=                  2976 ci%nnlev=                   595  l2rec=
                  8192  n2max=                  5460
 lcore1,lcore2=              13068137              13044821
 lencor,maxblo              13107200                 60000
========================================
 current settings:
 minbl3         381
 minbl4         381
 locmaxbl3     4288
 locmaxbuf     2144
 maxbl3       60000
 maxbl3       60000
 maxbl4       60000
 maxbuf       30006
========================================

 sorted 4-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)

 sorted 3-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)
 Orig.  diagonal integrals:  1electron:        34
                             0ext.    :        42
                             2ext.    :       336
                             4ext.    :       812


 Orig. off-diag. integrals:  4ext.    :     24411
                             3ext.    :     18720
                             2ext.    :      6792
                             1ext.    :      1272
                             0ext.    :        99
                             2ext. SO :         0
                             1ext. SO :         0
                             0ext. SO :         0
                             1electron:       128


 Sorted integrals            3ext.  w :     17370 x :     16020
                             4ext.  w :     20955 x :     17777


Cycle #  1 sortfile size=    163835(       5 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13068137
Cycle #  2 sortfile size=    163835(       5 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13068137
 minimum size of srtscr:     98301 WP (     3 records)
 maximum size of srtscr:    163835 WP (     5 records)
diagi   file:      4 records  of   1536 WP each=>       6144 WP total
ofdgi   file:      8 records  of   1536 WP each=>      12288 WP total
fil3w   file:      1 records  of  30006 WP each=>      30006 WP total
fil3x   file:      1 records  of  30006 WP each=>      30006 WP total
fil4w   file:      1 records  of  30006 WP each=>      30006 WP total
fil4x   file:      1 records  of  30006 WP each=>      30006 WP total
 compressed index vector length=                    67
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 0,
  GSET = 0,
   DAVCOR =10,
  NCOREL = 4
   nroot=1
  IVMODE = 3
  NBKITR = 1
   nvbkmn=2
  RTOLBK = 1e-3
   niter=30
   nvcimn=2
   rtolci=1e-3
  NVCIMX = 6
  NVRFMX = 6
  NVBKMX = 6
  IDEN  = 1
  CSFPRN = 10,
  update_mode=1
  nvrfmn=2
 /&end
 ------------------------------------------------------------------------
lodens (list->root)=  1
invlodens (root->list)=  1
 USING SEGMENTS OF EQUAL SIZE

****************  list of control variables  ****************
 lvlprt =    0      nroot  =    1      noldv  =   0      noldhv =   0
 nunitv =    1      nbkitr =    1      niter  =  30      davcor =  10
 csfprn =   10      ivmode =    3      istrt  =   0      vout   =   0
 iortls =    0      nvbkmx =    6      ibktv  =  -1      ibkthv =  -1
 nvcimx =    6      icitv  =   -1      icithv =  -1      frcsub =   0
 nvbkmn =    2      nvcimn =    2      maxseg =   4      nrfitr =  30
 ncorel =    4      nvrfmx =    6      nvrfmn =   2      iden   =   1
 itran  =    0      froot  =    0      rtmode =   0      ncouple=   1
 skipso =    F      dalton2=    0      molcas =   0      finalv =   0
 finalw =    0      cosmocalc=   0    with_tsklst=   0
 nsegwx =    1     1     1     1
 nseg0x =    1     1     1     1
 nseg1x =    1     1     1     1
 nseg2x =    1     1     1     1
 nseg3x =    1     1     1     1
 nseg4x =    1     1     1     1
 no0ex  =      0    no1ex  =      0    no2ex  =     0    no3ex  =     0
 no4ex  =      0    nodiag =      0
 cdg4ex =    1      c3ex1ex=    1      c2ex0ex=   1
 fileloc=    0     0     0     0     0     0     0     1     1     1
 directhd=   1      noaqccshift_zyxw=      0
 critical_crit=-1.00000    critical_delta= 0.05000

 ctol   = 0.010000    lrtshift=1.000000    smalld =0.001000


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-03    1.000E-03
 Computing density:                    .drt1.state1
 using                      1  nodes and                      1  cores.
 szdg/szodg per processor=                  6144                 12288
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core           13107199 DP per process

********** Integral sort section *************


 workspace allocation information: lencor=  13107199

 echo of the input for program cisrt:
 ------------------------------------------------------------------------
  &input
  maxbl3=60000
  maxbl4=60000
  &end
 ------------------------------------------------------------------------
 
 ( 6) listing file:                    ciudgls             
 ( 5) input file:                      cisrtin   
 (17) cidrt file:                      cidrtfl             
 (11) transformed integrals file:      moints    
 (12) diagonal integral file:          diagint             
 (13) off-diagonal integral file:      ofdgint             
 (31) 4-external w integrals file:     fil4w               
 (32) 4-external x integrals file:     fil4x               
 (33) 3-external w integrals file:     fil3w               
 (34) 3-external x integrals file:     fil3x               
 (21) scratch da sorting file:         srtscr              
 (12) 2-e integral file [fsplit=2]:    moints2   

 input integral file header information:
                                                                                 
 aoints SIFS file created by argos.      zam792            17:53:52.086 17-Dec-13
  cidrt_title                                                                    
 MO-coefficients from mcscf.x                                                    
  with dummy occupation 1.0 for active orbitals                                  
  total ao core energy =    0.000000000                                          
 MCSCF energy =     -46.301878576                                                
 SIFS file created by program tran.      zam792            17:53:52.407 17-Dec-13

 input energy(*) values:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.

 total core energy =   0.000000000000E+00

 nsym = 4 nmot=  34

 symmetry  =    1    2    3    4
 slabel(*) =   a1   a2   b1   b2
 nmpsy(*)  =   14    4    8    8

 info(*) =          1      8192      6552      8192      5460         0

 orbital labels, i:molab(i)=
   1:tout:001   2:tout:002   3:tout:003   4:tout:004   5:tout:005   6:tout:006   7:tout:007   8:tout:008   9:tout:009  10:tout:010
  11:tout:011  12:tout:012  13:tout:013  14:tout:014  15:tout:015  16:tout:016  17:tout:017  18:tout:018  19:tout:019  20:tout:020
  21:tout:021  22:tout:022  23:tout:023  24:tout:024  25:tout:025  26:tout:026  27:tout:027  28:tout:028  29:tout:029  30:tout:030
  31:tout:031  32:tout:032  33:tout:033  34:tout:034

 input parameters:
 prnopt=  0
 ldamin=    4095 ldamax=   32767 ldainc=      64
 maxbuf=   30006 maxbl3=   60000 maxbl4=   60000 intmxo=    1536
  Using 32 bit compression 

 drt information:
  cidrt_title                                                                    
 nmotd =  38 nfctd =   4 nfvtc =   0 nmot  =  34
 nlevel =  34 niot  =   6 lowinl=  29
 orbital-to-level map(*)
   -1  -1  29  30  31   1   2   3   4   5   6   7   8   9  10  11  32  12  13  14
   -1  33  15  16  17  18  19  20  21  -1  34  22  23  24  25  26  27  28
 compressed map(*)
   29  30  31   1   2   3   4   5   6   7   8   9  10  11  32  12  13  14  33  15
   16  17  18  19  20  21  34  22  23  24  25  26  27  28
 levsym(*)
    1   1   1   1   1   1   1   1   1   1   1   2   2   2   3   3   3   3   3   3
    3   4   4   4   4   4   4   4   1   1   1   2   3   4
 repartitioning mu(*)=
   0.  0.  0.  0.  0.  0.

 new core energy added to the energy(*) list.
 from the integral file: h1_core= -4.349858765627E+01

 indxdg: diagonal integral statistics.
 total number of integrals contributing to diagonal matrix elements:      1190
 number with all external indices:       812
 number with half external - half internal indices:       336
 number with all internal indices:        42

 indxof: off-diagonal integral statistics.
    4-external integrals: num=      24411 strt=          1
    3-external integrals: num=      18720 strt=      24412
    2-external integrals: num=       6792 strt=      43132
    1-external integrals: num=       1272 strt=      49924
    0-external integrals: num=         99 strt=      51196

 total number of off-diagonal integrals:       51294


 indxof(2nd)  ittp=   3 numx(ittp)=        6792
 indxof(2nd)  ittp=   4 numx(ittp)=        1272
 indxof(2nd)  ittp=   5 numx(ittp)=          99

 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg=  12931980
 pro2e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652

 pro2e:     25126 integrals read in     5 records.

 pro2e:         0 integrals 34-ext integrals skipped.
 pro1e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652
 pro1e: eref =    0.000000000000000E+00
 total size of srtscr:                     4  records of                  32767 
 WP =               1048544 Bytes
 putdg        1     596    1191    1786    3322   36089   57934    3018   46706   90394
   123161  131353  136813  158652

 putf:       4 buffers of length    1536 written to file 12
 diagonal integral file completed.

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep4e:    17 blocks of linear combinations of 4-external integrals processed.
 number of sorted 4-external integrals      38732
 number of original 4-external integrals    24411


 putf34: external integral file complete. nfilw=    31 nfilx=    32 nrecw=     1 nrecx=     1 lbufp= 30006

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep3e:    19 blocks of linear combinations of 3-external integrals processed.
 number of sorted 3-external integrals      33390
 number of original 3-external integrals    18720


 putf34: external integral file complete. nfilw=    33 nfilx=    34 nrecw=     1 nrecx=     1 lbufp= 30006
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      6792         3     43132     43132      6792     51294
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      1272         4     49924     49924      1272     51294
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd        99         5     51196     51196        99     51294

 putf:       8 buffers of length    1536 written to file 13
 off-diagonal files sort completed.
 executing brd_struct for cisrtinfo
cisrtinfo:
bufszi  1536
 diagfile 4ext:     812 2ext:     336 0ext:      42
 fil4w,fil4x  :   24411 fil3w,fil3x :   18720
 ofdgint  2ext:    6792 1ext:    1272 0ext:      99so0ext:       0so1ext:       0so2ext:       0
buffer minbl4     381 minbl3     381 maxbl2     384nbas:  11   3   7   7   0   0   0   0 maxbuf 30006
 CIUDG version 5.9.7 ( 5-Oct-2004)

 workspace allocation information: lcore=  13107199

 core energy values from the integral file:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -4.349858765627E+01, ietype=    6,   fcore energy of type: H1(*)   

 total core repulsion energy = -4.349858765627E+01
 nmot  =    38 niot  =     6 nfct  =     4 nfvt  =     0
 nrow  =    36 nsym  =     4 ssym  =     2 lenbuf=  1600
 nwalk,xbar:        246      105       90       36       15
 nvalwt,nvalw:      169       28       90       36       15
 ncsft:            5560
 total number of valid internal walks:     169
 nvalz,nvaly,nvalx,nvalw =       28      90      36      15

 cisrt info file parameters:
 file number  12 blocksize   1536
 mxbld   1536
 nd4ext,nd2ext,nd0ext   812   336    42
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int    24411    18720     6792     1272       99        0        0        0
 minbl4,minbl3,maxbl2   381   381   384
 maxbuf 30006
 number of external orbitals per symmetry block:  11   3   7   7
 nmsym   4 number of internal orbitals   6
 executing brd_struct for drt
 executing brd_struct for orbinf
 executing brd_struct for momap
 calcthrxt: niot,maxw1=                     6                    66
 block size     0
 pthz,pthy,pthx,pthw:   105    90    36    15 total internal walks:     246
 maxlp3,n2lp,n1lp,n0lp    66     0     0     0
 orbsym(*)= 1 1 1 2 3 4

 setref:       28 references kept,
               77 references were marked as invalid, out of
              105 total.
 nmb.of records onel     1
 nmb.of records 2-ext     5
 nmb.of records 1-ext     1
 nmb.of records 0-ext     1
 nmb.of records 2-int     0
 nmb.of records 1-int     0
 nmb.of records 0-int     0
 ---------memory usage in DP -----------------
 < n-ex core usage >
     routines:
    fourex            61797
    threx             60409
    twoex              3549
    onex               1823
    allin              1536
    diagon             2109
               =======
   maximum            61797
 
  __ static summary __ 
   reflst                28
   hrfspc                28
               -------
   static->              28
 
  __ core required  __ 
   totstc                28
   max n-ex           61797
               -------
   totnec->           61825
 
  __ core available __ 
   totspc          13107199
   totnec -           61825
               -------
   totvec->        13045374

 number of external paths / symmetry
 vertex x     100      82      98      98
 vertex w     128      82      98      98
segment: free space=    13045374
 reducing frespc by                   910 to               13044464 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1          40|        28|         0|        28|         0|         1|
 -------------------------------------------------------------------------------
  Y 2          90|       638|        28|        90|        28|         2|
 -------------------------------------------------------------------------------
  X 3          36|      3352|       666|        36|       118|         3|
 -------------------------------------------------------------------------------
  W 4          15|      1542|      4018|        15|       154|         4|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=          40DP  conft+indsym=         360DP  drtbuffer=         510 DP

dimension of the ci-matrix ->>>      5560

 executing brd_struct for civct
 gentasklist: ntask=                    20
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS   
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      36      40       3352         28      36      28
     2  4   1    25      two-ext wz   2X  4 1      15      40       1542         28      15      28
     3  4   3    26      two-ext wx*  WX  4 3      15      36       1542       3352      15      36
     4  4   3    27      two-ext wx+  WX  4 3      15      36       1542       3352      15      36
     5  2   1    11      one-ext yz   1X  2 1      90      40        638         28      90      28
     6  3   2    15      1ex3ex yx    3X  3 2      36      90       3352        638      36      90
     7  4   2    16      1ex3ex yw    3X  4 2      15      90       1542        638      15      90
     8  1   1     1      allint zz    OX  1 1      40      40         28         28      28      28
     9  2   2     5      0ex2ex yy    OX  2 2      90      90        638        638      90      90
    10  3   3     6      0ex2ex xx*   OX  3 3      36      36       3352       3352      36      36
    11  3   3    18      0ex2ex xx+   OX  3 3      36      36       3352       3352      36      36
    12  4   4     7      0ex2ex ww*   OX  4 4      15      15       1542       1542      15      15
    13  4   4    19      0ex2ex ww+   OX  4 4      15      15       1542       1542      15      15
    14  2   2    42      four-ext y   4X  2 2      90      90        638        638      90      90
    15  3   3    43      four-ext x   4X  3 3      36      36       3352       3352      36      36
    16  4   4    44      four-ext w   4X  4 4      15      15       1542       1542      15      15
    17  1   1    75      dg-024ext z  OX  1 1      40      40         28         28      28      28
    18  2   2    76      dg-024ext y  OX  2 2      90      90        638        638      90      90
    19  3   3    77      dg-024ext x  OX  3 3      36      36       3352       3352      36      36
    20  4   4    78      dg-024ext w  OX  4 4      15      15       1542       1542      15      15
----------------------------------------------------------------------------------------------------
REDTASK #   1 TIME=  19.000 N=  1 (task/type/sgbra)=(   1/24/0) (
REDTASK #   2 TIME=  18.000 N=  1 (task/type/sgbra)=(   2/25/0) (
REDTASK #   3 TIME=  17.000 N=  1 (task/type/sgbra)=(   3/26/1) (
REDTASK #   4 TIME=  16.000 N=  1 (task/type/sgbra)=(   4/27/2) (
REDTASK #   5 TIME=  15.000 N=  1 (task/type/sgbra)=(   5/11/0) (
REDTASK #   6 TIME=  14.000 N=  1 (task/type/sgbra)=(   6/15/0) (
REDTASK #   7 TIME=  13.000 N=  1 (task/type/sgbra)=(   7/16/0) (
REDTASK #   8 TIME=  12.000 N=  1 (task/type/sgbra)=(   8/ 1/0) (
REDTASK #   9 TIME=  11.000 N=  1 (task/type/sgbra)=(   9/ 5/0) (
REDTASK #  10 TIME=  10.000 N=  1 (task/type/sgbra)=(  10/ 6/1) (
REDTASK #  11 TIME=   9.000 N=  1 (task/type/sgbra)=(  11/18/2) (
REDTASK #  12 TIME=   8.000 N=  1 (task/type/sgbra)=(  12/ 7/1) (
REDTASK #  13 TIME=   7.000 N=  1 (task/type/sgbra)=(  13/19/2) (
REDTASK #  14 TIME=   6.000 N=  1 (task/type/sgbra)=(  14/42/1) (
REDTASK #  15 TIME=   5.000 N=  1 (task/type/sgbra)=(  15/43/1) (
REDTASK #  16 TIME=   4.000 N=  1 (task/type/sgbra)=(  16/44/1) (
REDTASK #  17 TIME=   3.000 N=  1 (task/type/sgbra)=(  17/75/1) (
REDTASK #  18 TIME=   2.000 N=  1 (task/type/sgbra)=(  18/76/1) (
REDTASK #  19 TIME=   1.000 N=  1 (task/type/sgbra)=(  19/77/1) (
REDTASK #  20 TIME=   0.000 N=  1 (task/type/sgbra)=(  20/78/1) (
 initializing v-file: 1:                  5560

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         216 2x:           0 4x:           0
All internal counts: zz :         356 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 reference space has dimension      28
 dsyevx: computed roots 1 to    2(converged:   2)

    root           eigenvalues
    ----           ------------
       1         -46.3018785770
       2         -46.3018785757

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt=                    28

         vector  1 from unit 11 written to unit 49 filename cirefv              

 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=    28)...
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              5560
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                    1
 residual norm convergence criteria:               0.001000

          starting bk iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         807 2x:         336 4x:         141
All internal counts: zz :         356 yy:           0 xx:           0 ww:           0
One-external counts: yz :        1509 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:         159 wz:         100 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1
   ht   1    -2.80329092

          calcsovref: tciref block   1

              civs   1
 refs   1    1.00000    

          calcsovref: scrb block   1

                ci   1
 civs   1    1.00000    

          calcsovref: sovref block   1

              v      1
 ref    1    1.00000    

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.3018785770  7.5495E-15  5.1510E-02  2.1139E-01  1.0000E-03
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.3018785770  7.5495E-15  5.1510E-02  2.1139E-01  1.0000E-03
 
diagon:itrnv=   2
 expansion vectors are not transformed.
 matrix-vector products are not transformed.

    2 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              5560
 number of initial trial vectors:                         2
 number of initial matrix-vector products:                1
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                   30
 residual norm convergence criteria:               0.001000

          starting ci iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         807 2x:         336 4x:         141
All internal counts: zz :         356 yy:         930 xx:         352 ww:          66
One-external counts: yz :        1509 yx:        2405 yw:        1220
Two-external counts: yy :        1285 ww:         180 xx:         682 xz:         159 wz:         100 wx:         510
Three-ext.   counts: yx :         657 yw:         358

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2
   ht   1    -2.80329092
   ht   2    -0.05150986    -0.17507202

          calcsovref: tciref block   1

              civs   1       civs   2
 refs   1    1.00000       1.430610E-13

          calcsovref: scrb block   1

                ci   1         ci   2
 civs   1   0.971427      -0.237338    
 civs   2   0.824796        3.37590    

          calcsovref: sovref block   1

              v      1       v      2
 ref    1   0.971427      -0.237338    

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97142720    -0.23733774

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.3456133303  4.3735E-02  1.8243E-03  3.9854E-02  1.0000E-03
 mr-sdci #  1  2    -45.5691998471  2.0706E+00  0.0000E+00  5.0329E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   2

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         807 2x:         336 4x:         141
All internal counts: zz :         356 yy:         930 xx:         352 ww:          66
One-external counts: yz :        1509 yx:        2405 yw:        1220
Two-external counts: yy :        1285 ww:         180 xx:         682 xz:         159 wz:         100 wx:         510
Three-ext.   counts: yx :         657 yw:         358

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3
   ht   1    -2.80329092
   ht   2    -0.05150986    -0.17507202
   ht   3     0.00186286    -0.00307009    -0.00623456

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3
 refs   1    1.00000       1.430610E-13  -6.597244E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3
 civs   1   0.967655      -0.169592       0.187183    
 civs   2   0.858952        1.69154       -2.91253    
 civs   3    1.09857        15.9831        10.0635    

          calcsovref: sovref block   1

              v      1       v      2       v      3
 ref    1   0.966931      -0.180137       0.180543    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.96693062    -0.18013676     0.18054339

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1    -46.3476209660  2.0076E-03  2.4487E-04  1.4283E-02  1.0000E-03
 mr-sdci #  2  2    -45.8193799809  2.5018E-01  0.0000E+00  4.4462E-01  1.0000E-04
 mr-sdci #  2  3    -45.4701685979  1.9716E+00  0.0000E+00  5.1418E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000999
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   3

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         807 2x:         336 4x:         141
All internal counts: zz :         356 yy:         930 xx:         352 ww:          66
One-external counts: yz :        1509 yx:        2405 yw:        1220
Two-external counts: yy :        1285 ww:         180 xx:         682 xz:         159 wz:         100 wx:         510
Three-ext.   counts: yx :         657 yw:         358

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4
   ht   1    -2.80329092
   ht   2    -0.05150986    -0.17507202
   ht   3     0.00186286    -0.00307009    -0.00623456
   ht   4    -0.00319932    -0.00214816    -0.00047020    -0.00088189

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4
 refs   1    1.00000       1.430610E-13  -6.597244E-04   1.153024E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4
 civs   1   0.965120       0.180904       0.186018      -6.911786E-02
 civs   2   0.862375       -1.03871       -3.21534       0.321519    
 civs   3    1.22025       -13.3247        3.54637       -12.9852    
 civs   4   0.913748       -26.0742        19.0626        37.8416    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4
 ref    1   0.965369       0.159630       0.205658      -1.691890E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.96536867     0.15963050     0.20565792    -0.01691890

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1    -46.3478447599  2.2379E-04  4.2256E-05  6.1555E-03  1.0000E-03
 mr-sdci #  3  2    -45.9732086273  1.5383E-01  0.0000E+00  2.9568E-01  1.0000E-04
 mr-sdci #  3  3    -45.4932670474  2.3098E-02  0.0000E+00  5.2039E-01  1.0000E-04
 mr-sdci #  3  4    -45.3863495287  1.8878E+00  0.0000E+00  5.7726E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   4

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         807 2x:         336 4x:         141
All internal counts: zz :         356 yy:         930 xx:         352 ww:          66
One-external counts: yz :        1509 yx:        2405 yw:        1220
Two-external counts: yy :        1285 ww:         180 xx:         682 xz:         159 wz:         100 wx:         510
Three-ext.   counts: yx :         657 yw:         358

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1    -2.80329092
   ht   2    -0.05150986    -0.17507202
   ht   3     0.00186286    -0.00307009    -0.00623456
   ht   4    -0.00319932    -0.00214816    -0.00047020    -0.00088189
   ht   5     0.00240320    -0.00036438    -0.00028172     0.00004169    -0.00017178

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1    1.00000       1.430610E-13  -6.597244E-04   1.153024E-03  -8.549667E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 civs   1  -0.965191       0.138824       0.226189       8.804716E-02  -3.252285E-02
 civs   2  -0.862797      -0.796688       -2.17071       -2.46918       0.384488    
 civs   3   -1.23909       -11.4434       -1.93914        3.26037       -15.2463    
 civs   4   -1.05581       -27.0079       -2.34042        26.1039        33.9875    
 civs   5  -0.823992       -40.5839        89.2727       -45.8515        42.7288    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1  -0.964887       0.149931       0.148445       0.155196      -1.980785E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1    -0.96488691     0.14993068     0.14844478     0.15519612    -0.01980785

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1    -46.3478795796  3.4820E-05  3.3796E-06  1.6706E-03  1.0000E-03
 mr-sdci #  4  2    -46.0368842639  6.3676E-02  0.0000E+00  1.7512E-01  1.0000E-04
 mr-sdci #  4  3    -45.5454487056  5.2182E-02  0.0000E+00  6.0162E-01  1.0000E-04
 mr-sdci #  4  4    -45.4784443342  9.2095E-02  0.0000E+00  5.9963E-01  1.0000E-04
 mr-sdci #  4  5    -45.3621522826  1.8636E+00  0.0000E+00  5.6219E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   5

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         807 2x:         336 4x:         141
All internal counts: zz :         356 yy:         930 xx:         352 ww:          66
One-external counts: yz :        1509 yx:        2405 yw:        1220
Two-external counts: yy :        1285 ww:         180 xx:         682 xz:         159 wz:         100 wx:         510
Three-ext.   counts: yx :         657 yw:         358

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1    -2.80329092
   ht   2    -0.05150986    -0.17507202
   ht   3     0.00186286    -0.00307009    -0.00623456
   ht   4    -0.00319932    -0.00214816    -0.00047020    -0.00088189
   ht   5     0.00240320    -0.00036438    -0.00028172     0.00004169    -0.00017178
   ht   6     0.00041280    -0.00008826     0.00000830     0.00003342    -0.00001226    -0.00002157

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1    1.00000       1.430610E-13  -6.597244E-04   1.153024E-03  -8.549667E-04  -1.467151E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1  -0.965072      -0.131477      -5.320527E-02   0.226290      -9.273398E-02   9.515801E-03
 civs   2  -0.862818       0.596743       0.791363       -2.17378        2.42464       0.189275    
 civs   3   -1.24078        9.59310        6.81211       -1.94162       -7.67370        13.3238    
 civs   4   -1.06868        25.5146        5.76220       -2.30260       -17.9024       -40.3681    
 civs   5  -0.898661        43.8520        3.79394        89.2376        58.5042       -26.5736    
 civs   6   0.933758       -136.709        298.511      -0.230385       -82.8265       -78.1395    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.964854      -0.125821      -9.809517E-02   0.148655      -0.146181      -1.163583E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.96485440    -0.12582115    -0.09809517     0.14865466    -0.14618062    -0.01163583

 trial vector basis is being transformed.  new dimension:   2

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1    -46.3478827354  3.1558E-06  6.2265E-07  6.3216E-04  1.0000E-03
 mr-sdci #  5  2    -46.0658904656  2.9006E-02  0.0000E+00  1.6663E-01  1.0000E-04
 mr-sdci #  5  3    -45.8940765256  3.4863E-01  0.0000E+00  4.8111E-01  1.0000E-04
 mr-sdci #  5  4    -45.5454484317  6.7004E-02  0.0000E+00  6.0134E-01  1.0000E-04
 mr-sdci #  5  5    -45.4444932050  8.2341E-02  0.0000E+00  5.7984E-01  1.0000E-04
 mr-sdci #  5  6    -45.3401411021  1.8416E+00  0.0000E+00  5.6659E-01  1.0000E-04
 
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mr-sdci  convergence criteria satisfied after  5 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1    -46.3478827354  3.1558E-06  6.2265E-07  6.3216E-04  1.0000E-03
 mr-sdci #  5  2    -46.0658904656  2.9006E-02  0.0000E+00  1.6663E-01  1.0000E-04

####################CIUDGINFO####################

   ci vector at position   1 energy=  -46.347882735374

################END OF CIUDGINFO################

 
diagon:itrnv=   0
    1 of the   3 expansion vectors are transformed.
    1 of the   2 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
maximum overlap with reference    1(overlap= 0.96485)

 information on vector: 1 from unit 11 written to unit 48 filename civout              


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -46.3478827354

                                                       internal orbitals

                                          level       1    2    3    4    5    6

                                          orbital     3    4    5   17   22   31

                                         symmetry   a1   a1   a1   a2   b1   b2 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  3  1       1  0.284151                        +-   +         +            
 z*  3  1       2 -0.721808                        +-        +    +            
 z*  3  1       3 -0.555255                        +-                  +    +  
 z*  3  1       5  0.015346                        +     -   +    +            
 z*  3  1       6  0.022026                        +     -             +    +  
 z*  3  1       7  0.039656                        +    +     -   +            
 z*  3  1       8  0.024533                        +    +    +     -           
 z*  3  1       9 -0.015031                        +    +               -   +  
 z*  3  1      10 -0.067355                        +    +              +     - 
 z*  3  1      11  0.055708                        +         +-   +            
 z*  3  1      12 -0.020868                        +          -        +    +  
 z*  3  1      14  0.043839                        +         +         +     - 
 z*  3  1      16  0.045163                        +              +         +- 
 z*  3  1      17  0.035425                             +-   +    +            
 z*  3  1      18  0.027066                             +-             +    +  
 z*  3  1      23 -0.011864                             +         +    +-      
 z*  3  1      24 -0.017122                             +         +         +- 
 z*  3  1      25  0.020105                                  +-        +    +  
 z*  3  1      26  0.035754                                  +    +    +-      
 z*  3  1      27  0.025234                                  +    +         +- 
 z*  3  1      28  0.027206                                       +-   +    +  
 y   3  1      77  0.010559              4( b2 )    -   +              +       
 y   3  1      92  0.016322              5( a1 )    -        +    +            
 y   3  1      94 -0.010064              7( a1 )    -        +    +            
 y   3  1     131  0.012542              5( a1 )    -                  +    +  
 y   3  1     204  0.022549              5( a1 )   +          -   +            
 y   3  1     229 -0.015944              5( a1 )   +         +     -           
 y   3  1     288  0.017401              5( a1 )   +                    -   +  
 y   3  1     299 -0.012306              5( a1 )   +                   +     - 
 y   3  1     339  0.011166              3( a1 )         -   +    +            
 y   3  1     343 -0.012340              7( a1 )         -   +    +            
 y   3  1     569 -0.011805              4( b1 )             +    +     -      
 y   3  1     623 -0.011403              2( a2 )                   -   +    +  
 x   3  1     672  0.011138    6( a1 )   1( a2 )   +-                          
 x   3  1     679 -0.011598    2( a1 )   2( a2 )   +-                          
 x   3  1     683 -0.019566    6( a1 )   2( a2 )   +-                          
 x   3  1     697 -0.013491    9( a1 )   3( a2 )   +-                          
 x   3  1     724  0.015186    4( b1 )   4( b2 )   +-                          
 x   3  1     740  0.011216    6( b1 )   6( b2 )   +-                          
 x   3  1     748 -0.011160    7( b1 )   7( b2 )   +-                          
 x   3  1     835 -0.015377    5( a1 )   1( a2 )    -        +                 
 x   3  1     846  0.017942    5( a1 )   2( a2 )    -        +                 
 x   3  1     853  0.011513    1( a1 )   3( a2 )    -        +                 
 x   3  1     856  0.011935    4( a1 )   3( a2 )    -        +                 
 x   3  1     870  0.014004    7( b1 )   1( b2 )    -        +                 
 x   3  1     884  0.014579    7( b1 )   3( b2 )    -        +                 
 x   3  1     906  0.013839    1( b1 )   7( b2 )    -        +                 
 x   3  1     908  0.014406    3( b1 )   7( b2 )    -        +                 
 x   3  1     920 -0.015915    2( a1 )   5( a1 )    -             +            
 x   3  1     927  0.017836    5( a1 )   6( a1 )    -             +            
 x   3  1     941  0.013767    1( a1 )   9( a1 )    -             +            
 x   3  1     944  0.014321    4( a1 )   9( a1 )    -             +            
 x   3  1     981  0.010971    1( b1 )   6( b1 )    -             +            
 x   3  1     983  0.011440    3( b1 )   6( b1 )    -             +            
 x   3  1    1002  0.013094    1( b2 )   6( b2 )    -             +            
 x   3  1    1004  0.013575    3( b2 )   6( b2 )    -             +            
 x   3  1    1043 -0.010681   10( a1 )   1( b2 )    -                  +       
 x   3  1    1049 -0.011861    5( a1 )   2( b2 )    -                  +       
 x   3  1    1065 -0.011089   10( a1 )   3( b2 )    -                  +       
 x   3  1    1071  0.013840    5( a1 )   4( b2 )    -                  +       
 x   3  1    1089  0.010574    1( a1 )   6( b2 )    -                  +       
 x   3  1    1092  0.010984    4( a1 )   6( b2 )    -                  +       
 x   3  1    1126  0.011860    5( a1 )   2( b1 )    -                       +  
 x   3  1    1148 -0.013840    5( a1 )   4( b1 )    -                       +  
 x   3  1    1166 -0.011182    1( a1 )   6( b1 )    -                       +  
 x   3  1    1169 -0.011636    4( a1 )   6( b1 )    -                       +  
 w   3  1    4052 -0.016069    1( b1 )   1( b2 )   +    +                      
 w   3  1    4054 -0.012332    3( b1 )   1( b2 )   +    +                      
 w   3  1    4066 -0.012331    1( b1 )   3( b2 )   +    +                      
 w   3  1    4068 -0.010115    3( b1 )   3( b2 )   +    +                      
 w   3  1    4105  0.010063    5( a1 )   1( a2 )   +         +                 
 w   3  1    4134  0.040702    1( b1 )   1( b2 )   +         +                 
 w   3  1    4136  0.031288    3( b1 )   1( b2 )   +         +                 
 w   3  1    4148  0.031174    1( b1 )   3( b2 )   +         +                 
 w   3  1    4150  0.025609    3( b1 )   3( b2 )   +         +                 
 w   3  1    4154  0.010311    7( b1 )   3( b2 )   +         +                 
 w   3  1    4176 -0.011323    1( b1 )   7( b2 )   +         +                 
 w   3  1    4178 -0.012045    3( b1 )   7( b2 )   +         +                 
 w   3  1    4183  0.033329    1( a1 )   1( a1 )   +              +            
 w   3  1    4189  0.036168    1( a1 )   4( a1 )   +              +            
 w   3  1    4192  0.020978    4( a1 )   4( a1 )   +              +            
 w   3  1    4194 -0.010415    2( a1 )   5( a1 )   +              +            
 w   3  1    4219 -0.011070    1( a1 )   9( a1 )   +              +            
 w   3  1    4222 -0.011780    4( a1 )   9( a1 )   +              +            
 w   3  1    4283 -0.027684    1( b2 )   1( b2 )   +              +            
 w   3  1    4286 -0.030045    1( b2 )   3( b2 )   +              +            
 w   3  1    4288 -0.017433    3( b2 )   3( b2 )   +              +            
 w   3  1    4332  0.031378    1( a1 )   1( b2 )   +                   +       
 w   3  1    4335  0.024088    4( a1 )   1( b2 )   +                   +       
 w   3  1    4354  0.024067    1( a1 )   3( b2 )   +                   +       
 w   3  1    4357  0.019748    4( a1 )   3( b2 )   +                   +       
 w   3  1    4409 -0.031386    1( a1 )   1( b1 )   +                        +  
 w   3  1    4412 -0.024020    4( a1 )   1( b1 )   +                        +  
 w   3  1    4431 -0.024145    1( a1 )   3( b1 )   +                        +  
 w   3  1    4434 -0.019751    4( a1 )   3( b1 )   +                        +  
 w   3  1    4589 -0.019740    1( a1 )   1( a1 )        +         +            
 w   3  1    4595 -0.016608    1( a1 )   4( a1 )        +         +            
 w   3  1    4661 -0.021654    1( b1 )   1( b1 )        +         +            
 w   3  1    4664 -0.018405    1( b1 )   3( b1 )        +         +            
 w   3  1    4689 -0.025301    1( b2 )   1( b2 )        +         +            
 w   3  1    4692 -0.021864    1( b2 )   3( b2 )        +         +            
 w   3  1    4694 -0.011075    3( b2 )   3( b2 )        +         +            
 w   3  1    4913  0.058503    1( a1 )   1( a1 )             +    +            
 w   3  1    4919  0.050119    1( a1 )   4( a1 )             +    +            
 w   3  1    4922  0.025389    4( a1 )   4( a1 )             +    +            
 w   3  1    4927  0.018120    5( a1 )   5( a1 )             +    +            
 w   3  1    4985  0.058618    1( b1 )   1( b1 )             +    +            
 w   3  1    4988  0.050080    1( b1 )   3( b1 )             +    +            
 w   3  1    4990  0.025327    3( b1 )   3( b1 )             +    +            
 w   3  1    5013  0.051572    1( b2 )   1( b2 )             +    +            
 w   3  1    5016  0.043704    1( b2 )   3( b2 )             +    +            
 w   3  1    5018  0.022209    3( b2 )   3( b2 )             +    +            
 w   3  1    5433  0.040103    1( a1 )   1( a1 )                       +    +  
 w   3  1    5439  0.034040    1( a1 )   4( a1 )                       +    +  
 w   3  1    5442  0.017308    4( a1 )   4( a1 )                       +    +  
 w   3  1    5447  0.013982    5( a1 )   5( a1 )                       +    +  
 w   3  1    5505  0.044916    1( b1 )   1( b1 )                       +    +  
 w   3  1    5508  0.038300    1( b1 )   3( b1 )                       +    +  
 w   3  1    5510  0.019365    3( b1 )   3( b1 )                       +    +  
 w   3  1    5533  0.045080    1( b2 )   1( b2 )                       +    +  
 w   3  1    5536  0.038651    1( b2 )   3( b2 )                       +    +  
 w   3  1    5538  0.019593    3( b2 )   3( b2 )                       +    +  

 ci coefficient statistics:
           rq > 0.1                3
      0.1> rq > 0.01             119
     0.01> rq > 0.001            834
    0.001> rq > 0.0001          1259
   0.0001> rq > 0.00001          473
  0.00001> rq > 0.000001          78
 0.000001> rq                   2794
           all                  5560
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         216 2x:           0 4x:           0
All internal counts: zz :         356 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.284150736137    -13.156734335854
     2     2     -0.721807806416     33.421040095725
     3     3     -0.555254659106     25.709350680011
     4     4     -0.001488344935      0.068944851113
     5     5      0.015346049392     -0.710620975837
     6     6      0.022026325863     -1.019967582377
     7     7      0.039655594873     -1.836427127533
     8     8      0.024533461787     -1.136125675558
     9     9     -0.015031334609      0.696058338081
    10    10     -0.067354736498      3.119119670252
    11    11      0.055708219926     -2.579797747584
    12    12     -0.020867642831      0.966364112105
    13    13     -0.007284962157      0.337330216948
    14    14      0.043839314874     -2.030160242465
    15    15      0.009031124368     -0.418236963032
    16    16      0.045162563931     -2.091411266924
    17    17      0.035425190960     -1.639551299600
    18    18      0.027065718625     -1.252626504625
    19    19      0.009136439931     -0.422740113265
    20    20      0.004731274752     -0.219074731584
    21    21      0.003464711269     -0.160419535694
    22    22     -0.006084776978      0.281730922872
    23    23     -0.011864267186      0.549047785460
    24    24     -0.017121635332      0.792471146486
    25    25      0.020104877678     -0.930333451243
    26    26      0.035754454640     -1.654796920318
    27    27      0.025233634841     -1.167671818543
    28    28      0.027206241950     -1.259145778832

 number of reference csfs (nref) is    28.  root number (iroot) is  1.
 c0**2 =   0.93098666  c**2 (all zwalks) =   0.93098666

 pople ci energy extrapolation is computed with  4 correlated electrons.

 eref      =    -46.301866959168   "relaxed" cnot**2         =   0.930986659322
 eci       =    -46.347882735374   deltae = eci - eref       =  -0.046015776205
 eci+dv1   =    -46.351058437814   dv1 = (1-cnot**2)*deltae  =  -0.003175702440
 eci+dv2   =    -46.351293850246   dv2 = dv1 / cnot**2       =  -0.003411114873
 eci+dv3   =    -46.351566958970   dv3 = dv1 / (2*cnot**2-1) =  -0.003684223596
 eci+pople =    -46.349585956154   (  4e- scaled deltae )    =  -0.047718996986
 passed aftci ... 
 readint2: molcas,dalton2=                     0                     0
 files%faoints=aoints              
lodens (list->root)=  1
sifcfg setup: record length 4096 DP
# d1 elements per record  3272
# d2 elements per record  2730
  The MR-CISD density will be calculated.
 item #                     1 suffix=:.drt1.state1:
 read_civout: repnuc=  -43.4985876562748     
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method:  0 last record  1max overlap with ref# 96% root-following 0
 MR-CISD energy:   -46.34788274    -2.84929508
 residuum:     0.00063216
 deltae:     0.00000316
 apxde:     0.00000062

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.96485440    -0.12582115    -0.09809517     0.14865466    -0.14618062    -0.01163583     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.96485440    -0.12582115     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 computing final density
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=     139  DYX=     275  DYW=     150
   D0Z=      37  D0Y=     125  D0X=      38  D0W=      12
  DDZI=      96 DDYI=     240 DDXI=      66 DDWI=      30
  DDZE=       0 DDYE=      90 DDXE=      36 DDWE=      15
================================================================================
Trace of MO density:     4.000000
    4  correlated and     8  frozen core electrons

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     1.86968733    -0.03210945    -0.01261593     0.00000000     0.00199074    -0.00709062
   MO   4     0.00000000     0.00000000    -0.03210945     0.09938417    -0.21658213     0.00000000    -0.00226013     0.00102828
   MO   5     0.00000000     0.00000000    -0.01261593    -0.21658213     0.56662072     0.00000000     0.00471412     0.00208557
   MO   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.02353314     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000     0.00199074    -0.00226013     0.00471412     0.00000000     0.00094640     0.00016854
   MO   8     0.00000000     0.00000000    -0.00709062     0.00102828     0.00208557     0.00000000     0.00016854     0.00044606
   MO   9     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.01650751     0.00000000     0.00000000
   MO  10     0.00000000     0.00000000    -0.02940157    -0.00050789    -0.00018992     0.00000000     0.00001056    -0.00003808
   MO  11     0.00000000     0.00000000     0.00340902    -0.00249119     0.00424423     0.00000000     0.00106108     0.00014120
   MO  12     0.00000000     0.00000000     0.00822312    -0.00127730    -0.00283544     0.00000000    -0.00033035    -0.00053912
   MO  13     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00346535     0.00000000     0.00000000
   MO  14     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00027618     0.00000000     0.00000000
   MO  15     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00075227     0.00000000     0.00000000
   MO  16     0.00000000     0.00000000    -0.00023067     0.00003815     0.00001490     0.00000000    -0.00000185     0.00000665

                MO   9         MO  10         MO  11         MO  12         MO  13         MO  14         MO  15         MO  16
   MO   3     0.00000000    -0.02940157     0.00340902     0.00822312     0.00000000     0.00000000     0.00000000    -0.00023067
   MO   4     0.00000000    -0.00050789    -0.00249119    -0.00127730     0.00000000     0.00000000     0.00000000     0.00003815
   MO   5     0.00000000    -0.00018992     0.00424423    -0.00283544     0.00000000     0.00000000     0.00000000     0.00001490
   MO   6     0.01650751     0.00000000     0.00000000     0.00000000     0.00346535    -0.00027618     0.00075227     0.00000000
   MO   7     0.00000000     0.00001056     0.00106108    -0.00033035     0.00000000     0.00000000     0.00000000    -0.00000185
   MO   8     0.00000000    -0.00003808     0.00014120    -0.00053912     0.00000000     0.00000000     0.00000000     0.00000665
   MO   9     0.01195514     0.00000000     0.00000000     0.00000000     0.00278117    -0.00027836     0.00062000     0.00000000
   MO  10     0.00000000     0.00552587     0.00002039     0.00004957     0.00000000     0.00000000     0.00000000    -0.00025880
   MO  11     0.00000000     0.00002039     0.00135073    -0.00036215     0.00000000     0.00000000     0.00000000    -0.00000367
   MO  12     0.00000000     0.00004957    -0.00036215     0.00072964     0.00000000     0.00000000     0.00000000    -0.00000890
   MO  13     0.00278117     0.00000000     0.00000000     0.00000000     0.00086095    -0.00014102     0.00019852     0.00000000
   MO  14    -0.00027836     0.00000000     0.00000000     0.00000000    -0.00014102     0.00148276    -0.00046217     0.00000000
   MO  15     0.00062000     0.00000000     0.00000000     0.00000000     0.00019852    -0.00046217     0.00116403     0.00000000
   MO  16     0.00000000    -0.00025880    -0.00000367    -0.00000890     0.00000000     0.00000000     0.00000000     0.00001731

Natural orbital populations,block 1
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     2.00000000     1.87086363     0.65165936     0.03583365     0.01447773     0.00494420     0.00235146
              MO     9       MO    10       MO    11       MO    12       MO    13       MO    14       MO    15       MO    16
  occ(*)=     0.00180654     0.00082628     0.00051865     0.00032541     0.00007003     0.00001288     0.00001090     0.00000354

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4
   MO   1     0.64923778     0.00516946    -0.00493758     0.00000000
   MO   2     0.00516946     0.00096161    -0.00109084     0.00000000
   MO   3    -0.00493758    -0.00109084     0.00141339     0.00000000
   MO   4     0.00000000     0.00000000     0.00000000     0.00154534

Natural orbital populations,block 2
              MO     1       MO     2       MO     3       MO     4       MO
  occ(*)=     0.64931676     0.00222377     0.00154534     0.00007225

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     0.33974060     0.00000000     0.00351207     0.00000000    -0.00367136     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.02282538     0.00000000     0.01575029     0.00000000     0.00315865    -0.00007285
   MO   4     0.00000000     0.00351207     0.00000000     0.00068585     0.00000000    -0.00078131     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.01575029     0.00000000     0.01121746     0.00000000     0.00250924    -0.00010148
   MO   6     0.00000000    -0.00367136     0.00000000    -0.00078131     0.00000000     0.00100043     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000     0.00315865     0.00000000     0.00250924     0.00000000     0.00077329    -0.00007503
   MO   8     0.00000000     0.00000000    -0.00007285     0.00000000    -0.00010148     0.00000000    -0.00007503     0.00134181
   MO   9     0.00000000     0.00000000     0.00111634     0.00000000     0.00103276     0.00000000     0.00044593     0.00056740

                MO   9
   MO   2     0.00000000
   MO   3     0.00111634
   MO   4     0.00000000
   MO   5     0.00103276
   MO   6     0.00000000
   MO   7     0.00044593
   MO   8     0.00056740
   MO   9     0.00140038

Natural orbital populations,block 3
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     0.33981693     0.03436380     0.00193218     0.00156427     0.00094222     0.00030971     0.00004568
              MO     9       MO
  occ(*)=     0.00001039

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     0.34277792     0.00000000     0.00388854     0.00000000    -0.00419309     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.02367280     0.00000000     0.01665592     0.00000000    -0.00352561     0.00086705
   MO   4     0.00000000     0.00388854     0.00000000     0.00073296     0.00000000    -0.00086690     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.01665592     0.00000000     0.01209938     0.00000000    -0.00283437     0.00074860
   MO   6     0.00000000    -0.00419309     0.00000000    -0.00086690     0.00000000     0.00113369     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000    -0.00352561     0.00000000    -0.00283437     0.00000000     0.00087787    -0.00027434
   MO   8     0.00000000     0.00000000     0.00086705     0.00000000     0.00074860     0.00000000    -0.00027434     0.00151727
   MO   9     0.00000000     0.00000000    -0.00109980     0.00000000    -0.00090530     0.00000000     0.00028891     0.00002127

                MO   9
   MO   2     0.00000000
   MO   3    -0.00109980
   MO   4     0.00000000
   MO   5    -0.00090530
   MO   6     0.00000000
   MO   7     0.00028891
   MO   8     0.00002127
   MO   9     0.00134054

Natural orbital populations,block 4
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     0.34287381     0.03619681     0.00172777     0.00151576     0.00130123     0.00048314     0.00004299
              MO     9       MO
  occ(*)=     0.00001093


 total number of electrons =   12.0000000000

 test slabel:                     4
  a1  a2  b1  b2 


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        a1  partial gross atomic populations
   ao class       1a1        2a1        3a1        4a1        5a1        6a1 
     3_ s       0.000000   2.000000   1.870146   0.000000   0.000000   0.000206
     3_ p       2.000000   0.000000   0.000000   0.000000   0.035799   0.000000
     3_ d       0.000000   0.000000   0.000718   0.651659   0.000000   0.014272
     3_ f       0.000000   0.000000   0.000000   0.000000   0.000035   0.000000
 
   ao class       7a1        8a1        9a1       10a1       11a1       12a1 
     3_ s       0.004875   0.000000   0.000000   0.000000   0.000000   0.000000
     3_ p       0.000000   0.000000   0.000026   0.000004   0.000509   0.000000
     3_ d       0.000069   0.002351   0.000000   0.000000   0.000000   0.000325
     3_ f       0.000000   0.000000   0.001780   0.000823   0.000009   0.000000
 
   ao class      13a1       14a1       15a1       16a1 
     3_ s       0.000000   0.000000   0.000000   0.000004
     3_ p       0.000000   0.000000   0.000011   0.000000
     3_ d       0.000070   0.000013   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000000

                        a2  partial gross atomic populations
   ao class       1a2        2a2        3a2        4a2 
     3_ d       0.649317   0.002224   0.000000   0.000072
     3_ f       0.000000   0.000000   0.001545   0.000000

                        b1  partial gross atomic populations
   ao class       1b1        2b1        3b1        4b1        5b1        6b1 
     3_ p       2.000000   0.000000   0.034288   0.000037   0.000000   0.000262
     3_ d       0.000000   0.339817   0.000000   0.000000   0.001564   0.000000
     3_ f       0.000000   0.000000   0.000076   0.001895   0.000000   0.000680
 
   ao class       7b1        8b1        9b1 
     3_ p       0.000219   0.000000   0.000010
     3_ d       0.000000   0.000046   0.000000
     3_ f       0.000091   0.000000   0.000000

                        b2  partial gross atomic populations
   ao class       1b2        2b2        3b2        4b2        5b2        6b2 
     3_ p       2.000000   0.000000   0.036094   0.000000   0.000028   0.000066
     3_ d       0.000000   0.342874   0.000000   0.001728   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000103   0.000000   0.001488   0.001235
 
   ao class       7b2        8b2        9b2 
     3_ p       0.000451   0.000000   0.000011
     3_ d       0.000000   0.000043   0.000000
     3_ f       0.000032   0.000000   0.000000


                        gross atomic populations
     ao            3_
      s         3.875231
      p         6.107815
      d         2.007162
      f         0.009792
    total      12.000000
 

 Total number of electrons:   12.00000000

 DA ...
