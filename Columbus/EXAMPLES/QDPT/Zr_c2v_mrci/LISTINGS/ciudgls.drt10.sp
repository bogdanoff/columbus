1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      2009-03.    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


================ Computing sorting integral file structure ================

                    -----z----- -----y----- -----x----- -----w----- ---total---

                CSFs         4         156        1430           0        1590
      internal walks        15          20          15           0          50
valid internal walks         4          20          15           0          39
 getinfoarray: info=                     6 :                     1
                  8192                  6552                  8192
                  5460                     0
 icd(3)=                  2976 ci%nnlev=                   595  l2rec=
                  8192  n2max=                  5460
 lcore1,lcore2=              13068137              13044821
 lencor,maxblo              13107200                 60000
========================================
 current settings:
 minbl3         381
 minbl4         381
 locmaxbl3     4288
 locmaxbuf     2144
 maxbl3       60000
 maxbl3       60000
 maxbl4       60000
 maxbuf       30006
========================================

 sorted 4-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)

 sorted 3-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)
 Orig.  diagonal integrals:  1electron:        34
                             0ext.    :        42
                             2ext.    :       336
                             4ext.    :       812


 Orig. off-diag. integrals:  4ext.    :     24411
                             3ext.    :     18720
                             2ext.    :      6792
                             1ext.    :      1272
                             0ext.    :        99
                             2ext. SO :         0
                             1ext. SO :         0
                             0ext. SO :         0
                             1electron:       128


 Sorted integrals            3ext.  w :     17370 x :     16020
                             4ext.  w :     20955 x :     17777


Cycle #  1 sortfile size=    163835(       5 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13068137
Cycle #  2 sortfile size=    163835(       5 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13068137
 minimum size of srtscr:     98301 WP (     3 records)
 maximum size of srtscr:    163835 WP (     5 records)
diagi   file:      4 records  of   1536 WP each=>       6144 WP total
ofdgi   file:      8 records  of   1536 WP each=>      12288 WP total
fil3w   file:      1 records  of  30006 WP each=>      30006 WP total
fil3x   file:      1 records  of  30006 WP each=>      30006 WP total
fil4w   file:      1 records  of  30006 WP each=>      30006 WP total
fil4x   file:      1 records  of  30006 WP each=>      30006 WP total
 compressed index vector length=                    13
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 0,
  GSET = 0,
   DAVCOR =10,
  NCOREL = 4
   nroot=1
  IVMODE = 3
  NBKITR = 1
   nvbkmn=2
  RTOLBK = 1e-3
   niter=30
   nvcimn=2
   rtolci=1e-3
  NVCIMX = 6
  NVRFMX = 6
  NVBKMX = 6
  IDEN  = 1
  CSFPRN = 10,
  update_mode=1
  nvrfmn=2
 /&end
 ------------------------------------------------------------------------
lodens (list->root)=  1
invlodens (root->list)=  1
 USING SEGMENTS OF EQUAL SIZE

****************  list of control variables  ****************
 lvlprt =    0      nroot  =    1      noldv  =   0      noldhv =   0
 nunitv =    1      nbkitr =    1      niter  =  30      davcor =  10
 csfprn =   10      ivmode =    3      istrt  =   0      vout   =   0
 iortls =    0      nvbkmx =    6      ibktv  =  -1      ibkthv =  -1
 nvcimx =    6      icitv  =   -1      icithv =  -1      frcsub =   0
 nvbkmn =    2      nvcimn =    2      maxseg =   4      nrfitr =  30
 ncorel =    4      nvrfmx =    6      nvrfmn =   2      iden   =   1
 itran  =    0      froot  =    0      rtmode =   0      ncouple=   1
 skipso =    F      dalton2=    0      molcas =   0      finalv =   0
 finalw =    0      cosmocalc=   0    with_tsklst=   0
 nsegwx =    1     1     1     1
 nseg0x =    1     1     1     1
 nseg1x =    1     1     1     1
 nseg2x =    1     1     1     1
 nseg3x =    1     1     1     1
 nseg4x =    1     1     1     1
 no0ex  =      0    no1ex  =      0    no2ex  =     0    no3ex  =     0
 no4ex  =      0    nodiag =      0
 cdg4ex =    1      c3ex1ex=    1      c2ex0ex=   1
 fileloc=    0     0     0     0     0     0     0     1     1     1
 directhd=   1      noaqccshift_zyxw=      0
 critical_crit=-1.00000    critical_delta= 0.05000

 ctol   = 0.010000    lrtshift=1.000000    smalld =0.001000


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-03    1.000E-03
 Computing density:                    .drt1.state1
 using                      1  nodes and                      1  cores.
 szdg/szodg per processor=                  6144                 12288
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core           13107199 DP per process

********** Integral sort section *************


 workspace allocation information: lencor=  13107199

 echo of the input for program cisrt:
 ------------------------------------------------------------------------
  &input
  maxbl3=60000
  maxbl4=60000
  &end
 ------------------------------------------------------------------------
 
 ( 6) listing file:                    ciudgls             
 ( 5) input file:                      cisrtin   
 (17) cidrt file:                      cidrtfl             
 (11) transformed integrals file:      moints    
 (12) diagonal integral file:          diagint             
 (13) off-diagonal integral file:      ofdgint             
 (31) 4-external w integrals file:     fil4w               
 (32) 4-external x integrals file:     fil4x               
 (33) 3-external w integrals file:     fil3w               
 (34) 3-external x integrals file:     fil3x               
 (21) scratch da sorting file:         srtscr              
 (12) 2-e integral file [fsplit=2]:    moints2   

 input integral file header information:
                                                                                 
 aoints SIFS file created by argos.      zam792            17:53:52.086 17-Dec-13
  cidrt_title                                                                    
 MO-coefficients from mcscf.x                                                    
  with dummy occupation 1.0 for active orbitals                                  
  total ao core energy =    0.000000000                                          
 MCSCF energy =     -46.301878576                                                
 SIFS file created by program tran.      zam792            17:53:52.407 17-Dec-13

 input energy(*) values:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.

 total core energy =   0.000000000000E+00

 nsym = 4 nmot=  34

 symmetry  =    1    2    3    4
 slabel(*) =   a1   a2   b1   b2
 nmpsy(*)  =   14    4    8    8

 info(*) =          1      8192      6552      8192      5460         0

 orbital labels, i:molab(i)=
   1:tout:001   2:tout:002   3:tout:003   4:tout:004   5:tout:005   6:tout:006   7:tout:007   8:tout:008   9:tout:009  10:tout:010
  11:tout:011  12:tout:012  13:tout:013  14:tout:014  15:tout:015  16:tout:016  17:tout:017  18:tout:018  19:tout:019  20:tout:020
  21:tout:021  22:tout:022  23:tout:023  24:tout:024  25:tout:025  26:tout:026  27:tout:027  28:tout:028  29:tout:029  30:tout:030
  31:tout:031  32:tout:032  33:tout:033  34:tout:034

 input parameters:
 prnopt=  0
 ldamin=    4095 ldamax=   32767 ldainc=      64
 maxbuf=   30006 maxbl3=   60000 maxbl4=   60000 intmxo=    1536
  Using 32 bit compression 

 drt information:
  cidrt_title                                                                    
 nmotd =  38 nfctd =   4 nfvtc =   0 nmot  =  34
 nlevel =  34 niot  =   6 lowinl=  29
 orbital-to-level map(*)
   -1  -1  29  30  31   1   2   3   4   5   6   7   8   9  10  11  32  12  13  14
   -1  33  15  16  17  18  19  20  21  -1  34  22  23  24  25  26  27  28
 compressed map(*)
   29  30  31   1   2   3   4   5   6   7   8   9  10  11  32  12  13  14  33  15
   16  17  18  19  20  21  34  22  23  24  25  26  27  28
 levsym(*)
    1   1   1   1   1   1   1   1   1   1   1   2   2   2   3   3   3   3   3   3
    3   4   4   4   4   4   4   4   1   1   1   2   3   4
 repartitioning mu(*)=
   0.  0.  0.  0.  0.  0.

 new core energy added to the energy(*) list.
 from the integral file: h1_core= -4.349858765627E+01

 indxdg: diagonal integral statistics.
 total number of integrals contributing to diagonal matrix elements:      1190
 number with all external indices:       812
 number with half external - half internal indices:       336
 number with all internal indices:        42

 indxof: off-diagonal integral statistics.
    4-external integrals: num=      24411 strt=          1
    3-external integrals: num=      18720 strt=      24412
    2-external integrals: num=       6792 strt=      43132
    1-external integrals: num=       1272 strt=      49924
    0-external integrals: num=         99 strt=      51196

 total number of off-diagonal integrals:       51294


 indxof(2nd)  ittp=   3 numx(ittp)=        6792
 indxof(2nd)  ittp=   4 numx(ittp)=        1272
 indxof(2nd)  ittp=   5 numx(ittp)=          99

 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg=  12931980
 pro2e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652

 pro2e:     25126 integrals read in     5 records.

 pro2e:         0 integrals 34-ext integrals skipped.
 pro1e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652
 pro1e: eref =    0.000000000000000E+00
 total size of srtscr:                     4  records of                  32767 
 WP =               1048544 Bytes
 putdg        1     596    1191    1786    3322   36089   57934    3018   46706   90394
   123161  131353  136813  158652

 putf:       4 buffers of length    1536 written to file 12
 diagonal integral file completed.

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep4e:    17 blocks of linear combinations of 4-external integrals processed.
 number of sorted 4-external integrals      38732
 number of original 4-external integrals    24411


 putf34: external integral file complete. nfilw=    31 nfilx=    32 nrecw=     1 nrecx=     1 lbufp= 30006

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep3e:    19 blocks of linear combinations of 3-external integrals processed.
 number of sorted 3-external integrals      33390
 number of original 3-external integrals    18720


 putf34: external integral file complete. nfilw=    33 nfilx=    34 nrecw=     1 nrecx=     1 lbufp= 30006
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      6792         3     43132     43132      6792     51294
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      1272         4     49924     49924      1272     51294
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd        99         5     51196     51196        99     51294

 putf:       8 buffers of length    1536 written to file 13
 off-diagonal files sort completed.
 executing brd_struct for cisrtinfo
cisrtinfo:
bufszi  1536
 diagfile 4ext:     812 2ext:     336 0ext:      42
 fil4w,fil4x  :   24411 fil3w,fil3x :   18720
 ofdgint  2ext:    6792 1ext:    1272 0ext:      99so0ext:       0so1ext:       0so2ext:       0
buffer minbl4     381 minbl3     381 maxbl2     384nbas:  11   3   7   7   0   0   0   0 maxbuf 30006
 CIUDG version 5.9.7 ( 5-Oct-2004)

 workspace allocation information: lcore=  13107199

 core energy values from the integral file:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -4.349858765627E+01, ietype=    6,   fcore energy of type: H1(*)   

 total core repulsion energy = -4.349858765627E+01
 nmot  =    38 niot  =     6 nfct  =     4 nfvt  =     0
 nrow  =    23 nsym  =     4 ssym  =     2 lenbuf=  1600
 nwalk,xbar:         50       15       20       15        0
 nvalwt,nvalw:       39        4       20       15        0
 ncsft:            1590
 total number of valid internal walks:      39
 nvalz,nvaly,nvalx,nvalw =        4      20      15       0

 cisrt info file parameters:
 file number  12 blocksize   1536
 mxbld   1536
 nd4ext,nd2ext,nd0ext   812   336    42
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int    24411    18720     6792     1272       99        0        0        0
 minbl4,minbl3,maxbl2   381   381   384
 maxbuf 30006
 number of external orbitals per symmetry block:  11   3   7   7
 nmsym   4 number of internal orbitals   6
 executing brd_struct for drt
 executing brd_struct for orbinf
 executing brd_struct for momap
 calcthrxt: niot,maxw1=                     6                    11
 block size     0
 pthz,pthy,pthx,pthw:    15    20    15     0 total internal walks:      50
 maxlp3,n2lp,n1lp,n0lp    11     0     0     0
 orbsym(*)= 1 1 1 2 3 4

 setref:        4 references kept,
               11 references were marked as invalid, out of
               15 total.
 nmb.of records onel     1
 nmb.of records 2-ext     5
 nmb.of records 1-ext     1
 nmb.of records 0-ext     1
 nmb.of records 2-int     0
 nmb.of records 1-int     0
 nmb.of records 0-int     0
 ---------memory usage in DP -----------------
 < n-ex core usage >
     routines:
    fourex            61797
    threx             60079
    twoex              3549
    onex               1823
    allin              1536
    diagon             2109
               =======
   maximum            61797
 
  __ static summary __ 
   reflst                 4
   hrfspc                 4
               -------
   static->               4
 
  __ core required  __ 
   totstc                 4
   max n-ex           61797
               -------
   totnec->           61801
 
  __ core available __ 
   totspc          13107199
   totnec -           61801
               -------
   totvec->        13045398

 number of external paths / symmetry
 vertex x     100      82      98      98
 vertex w     128      82      98      98
segment: free space=    13045398
 reducing frespc by                   337 to               13045061 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1           4|         4|         0|         4|         0|         1|
 -------------------------------------------------------------------------------
  Y 2          20|       156|         4|        20|         4|         2|
 -------------------------------------------------------------------------------
  X 3          15|      1430|       160|        15|        24|         3|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=           4DP  conft+indsym=          80DP  drtbuffer=         253 DP

dimension of the ci-matrix ->>>      1590

 executing brd_struct for civct
 gentasklist: ntask=                    12
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS   
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      15       4       1430          4      15       4
     2  2   1    11      one-ext yz   1X  2 1      20       4        156          4      20       4
     3  3   2    15      1ex3ex yx    3X  3 2      15      20       1430        156      15      20
     4  1   1     1      allint zz    OX  1 1       4       4          4          4       4       4
     5  2   2     5      0ex2ex yy    OX  2 2      20      20        156        156      20      20
     6  3   3     6      0ex2ex xx*   OX  3 3      15      15       1430       1430      15      15
     7  3   3    18      0ex2ex xx+   OX  3 3      15      15       1430       1430      15      15
     8  2   2    42      four-ext y   4X  2 2      20      20        156        156      20      20
     9  3   3    43      four-ext x   4X  3 3      15      15       1430       1430      15      15
    10  1   1    75      dg-024ext z  OX  1 1       4       4          4          4       4       4
    11  2   2    76      dg-024ext y  OX  2 2      20      20        156        156      20      20
    12  3   3    77      dg-024ext x  OX  3 3      15      15       1430       1430      15      15
----------------------------------------------------------------------------------------------------
REDTASK #   1 TIME=  11.000 N=  1 (task/type/sgbra)=(   1/24/0) (
REDTASK #   2 TIME=  10.000 N=  1 (task/type/sgbra)=(   2/11/0) (
REDTASK #   3 TIME=   9.000 N=  1 (task/type/sgbra)=(   3/15/0) (
REDTASK #   4 TIME=   8.000 N=  1 (task/type/sgbra)=(   4/ 1/0) (
REDTASK #   5 TIME=   7.000 N=  1 (task/type/sgbra)=(   5/ 5/0) (
REDTASK #   6 TIME=   6.000 N=  1 (task/type/sgbra)=(   6/ 6/1) (
REDTASK #   7 TIME=   5.000 N=  1 (task/type/sgbra)=(   7/18/2) (
REDTASK #   8 TIME=   4.000 N=  1 (task/type/sgbra)=(   8/42/1) (
REDTASK #   9 TIME=   3.000 N=  1 (task/type/sgbra)=(   9/43/1) (
REDTASK #  10 TIME=   2.000 N=  1 (task/type/sgbra)=(  10/75/1) (
REDTASK #  11 TIME=   1.000 N=  1 (task/type/sgbra)=(  11/76/1) (
REDTASK #  12 TIME=   0.000 N=  1 (task/type/sgbra)=(  12/77/1) (
 initializing v-file: 1:                  1590

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:          40 2x:           0 4x:           0
All internal counts: zz :          15 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 reference space has dimension       4
 dsyevx: computed roots 1 to    2(converged:   2)

    root           eigenvalues
    ----           ------------
       1         -46.2453833591
       2         -46.2453833530

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt=                     4

         vector  1 from unit 11 written to unit 49 filename cirefv              

 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=     4)...
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              1590
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                    1
 residual norm convergence criteria:               0.001000

          starting bk iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         205 2x:          90 4x:          35
All internal counts: zz :          15 yy:           0 xx:           0 ww:           0
One-external counts: yz :         112 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:          24 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1
   ht   1    -2.74679570

          calcsovref: tciref block   1

              civs   1
 refs   1    1.00000    

          calcsovref: scrb block   1

                ci   1
 civs   1    1.00000    

          calcsovref: sovref block   1

              v      1
 ref    1    1.00000    

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.2453833591 -4.4409E-16  5.4086E-02  2.1660E-01  1.0000E-03
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.2453833591 -4.4409E-16  5.4086E-02  2.1660E-01  1.0000E-03
 
diagon:itrnv=   2
 expansion vectors are not transformed.
 matrix-vector products are not transformed.

    2 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              1590
 number of initial trial vectors:                         2
 number of initial matrix-vector products:                1
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                   30
 residual norm convergence criteria:               0.001000

          starting ci iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         205 2x:          90 4x:          35
All internal counts: zz :          15 yy:          72 xx:          66 ww:           0
One-external counts: yz :         112 yx:         360 yw:           0
Two-external counts: yy :         150 ww:           0 xx:         180 xz:          24 wz:           0 wx:           0
Three-ext.   counts: yx :         151 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2
   ht   1    -2.74679570
   ht   2    -0.05408649    -0.23845420

          calcsovref: tciref block   1

              civs   1       civs   2
 refs   1    1.00000      -5.008900E-14

          calcsovref: scrb block   1

                ci   1         ci   2
 civs   1   0.965052      -0.262058    
 civs   2   0.793804        2.92326    

          calcsovref: sovref block   1

              v      1       v      2
 ref    1   0.965052      -0.262058    

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.96505221    -0.26205768

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.2898721893  4.4489E-02  1.1853E-03  2.7818E-02  1.0000E-03
 mr-sdci #  1  2    -45.6420479490  2.1435E+00  0.0000E+00  5.2225E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   2

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         205 2x:          90 4x:          35
All internal counts: zz :          15 yy:          72 xx:          66 ww:           0
One-external counts: yz :         112 yx:         360 yw:           0
Two-external counts: yy :         150 ww:           0 xx:         180 xz:          24 wz:           0 wx:           0
Three-ext.   counts: yx :         151 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3
   ht   1    -2.74679570
   ht   2    -0.05408649    -0.23845420
   ht   3    -0.00022600     0.00903242    -0.00594384

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3
 refs   1    1.00000      -5.008900E-14   2.235799E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3
 civs   1   0.965037       2.311576E-02   0.261094    
 civs   2   0.814200      -0.226127       -2.99059    
 civs   3    1.12408        19.5466       -5.92112    

          calcsovref: sovref block   1

              v      1       v      2       v      3
 ref    1   0.965062       2.355278E-02   0.260961    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.96506193     0.02355278     0.26096119

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1    -46.2912065912  1.3344E-03  1.1453E-04  9.3806E-03  1.0000E-03
 mr-sdci #  2  2    -45.8627029079  2.2065E-01  0.0000E+00  3.4045E-01  1.0000E-04
 mr-sdci #  2  3    -45.6218228876  2.1232E+00  0.0000E+00  5.3233E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   3

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         205 2x:          90 4x:          35
All internal counts: zz :          15 yy:          72 xx:          66 ww:           0
One-external counts: yz :         112 yx:         360 yw:           0
Two-external counts: yy :         150 ww:           0 xx:         180 xz:          24 wz:           0 wx:           0
Three-ext.   counts: yx :         151 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4
   ht   1    -2.74679570
   ht   2    -0.05408649    -0.23845420
   ht   3    -0.00022600     0.00903242    -0.00594384
   ht   4     0.00013030    -0.00339114    -0.00034118    -0.00051719

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4
 refs   1    1.00000      -5.008900E-14   2.235799E-05  -2.755659E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4
 civs   1   0.962950       0.106889       0.176024       0.174133    
 civs   2   0.816514      -0.394928       -1.60879       -2.65912    
 civs   3    1.26711       -12.3129        11.9789       -11.6559    
 civs   4    1.49191       -47.2125       -25.2650        47.1097    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4
 ref    1   0.962937       0.107915       0.176988       0.172574    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.96293708     0.10791520     0.17698762     0.17257427

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1    -46.2913775056  1.7091E-04  8.6895E-06  2.7892E-03  1.0000E-03
 mr-sdci #  3  2    -46.0525585958  1.8986E-01  0.0000E+00  1.9317E-01  1.0000E-04
 mr-sdci #  3  3    -45.7726792898  1.5086E-01  0.0000E+00  4.1847E-01  1.0000E-04
 mr-sdci #  3  4    -45.3859641287  1.8874E+00  0.0000E+00  4.9558E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   4

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         205 2x:          90 4x:          35
All internal counts: zz :          15 yy:          72 xx:          66 ww:           0
One-external counts: yz :         112 yx:         360 yw:           0
Two-external counts: yy :         150 ww:           0 xx:         180 xz:          24 wz:           0 wx:           0
Three-ext.   counts: yx :         151 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1    -2.74679570
   ht   2    -0.05408649    -0.23845420
   ht   3    -0.00022600     0.00903242    -0.00594384
   ht   4     0.00013030    -0.00339114    -0.00034118    -0.00051719
   ht   5    -0.00012383    -0.00020360    -0.00006981    -0.00003750    -0.00004576

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1    1.00000      -5.008900E-14   2.235799E-05  -2.755659E-05   4.626631E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 civs   1   0.962638       0.109829       8.613011E-02   0.161806       0.166704    
 civs   2   0.816584      -0.312192      -0.693170       -1.65577       -2.55072    
 civs   3    1.27494       -9.55976        14.7468        2.40650       -10.9840    
 civs   4    1.57440       -42.9578       -2.59472       -28.8752        50.1636    
 civs   5    1.08866       -85.3124       -144.122        154.830       -41.1549    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1   0.962674       0.106852       7.986332E-02   0.169819       0.163172    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     0.96267389     0.10685176     0.07986332     0.16981920     0.16317162

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1    -46.2913869657  9.4600E-06  1.2736E-06  8.9501E-04  1.0000E-03
 mr-sdci #  4  2    -46.0971800170  4.4621E-02  0.0000E+00  1.2453E-01  1.0000E-04
 mr-sdci #  4  3    -45.8139721057  4.1293E-02  0.0000E+00  3.7099E-01  1.0000E-04
 mr-sdci #  4  4    -45.7266862300  3.4072E-01  0.0000E+00  5.0959E-01  1.0000E-04
 mr-sdci #  4  5    -45.3728469275  1.8743E+00  0.0000E+00  5.0447E-01  1.0000E-04
 
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mr-sdci  convergence criteria satisfied after  4 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1    -46.2913869657  9.4600E-06  1.2736E-06  8.9501E-04  1.0000E-03
 mr-sdci #  4  2    -46.0971800170  4.4621E-02  0.0000E+00  1.2453E-01  1.0000E-04
 mr-sdci #  4  3    -45.8139721057  4.1293E-02  0.0000E+00  3.7099E-01  1.0000E-04
 mr-sdci #  4  4    -45.7266862300  3.4072E-01  0.0000E+00  5.0959E-01  1.0000E-04
 mr-sdci #  4  5    -45.3728469275  1.8743E+00  0.0000E+00  5.0447E-01  1.0000E-04

####################CIUDGINFO####################

   ci vector at position   1 energy=  -46.291386965653

################END OF CIUDGINFO################

 
diagon:itrnv=   0
    1 of the   6 expansion vectors are transformed.
    1 of the   5 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
maximum overlap with reference    1(overlap= 0.96267)

 information on vector: 1 from unit 11 written to unit 48 filename civout              


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -46.2913869657

                                                       internal orbitals

                                          level       1    2    3    4    5    6

                                          orbital     3    4    5   17   22   31

                                         symmetry   a1   a1   a1   a2   b1   b2 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  5  1       1  0.113390                        +    +    +    +            
 z*  5  1       2  0.741701                        +    +              +    +  
 z*  5  1       3  0.603130                        +         +         +    +  
 y   5  1       5 -0.012507              1( a2 )   +    +    +                 
 y   5  1      20 -0.065807              2( b2 )   +    +              +       
 y   5  1      22  0.052692              4( b2 )   +    +              +       
 y   5  1      27  0.087163              2( b1 )   +    +                   +  
 y   5  1      29 -0.061874              4( b1 )   +    +                   +  
 y   5  1      35 -0.021532              3( a1 )   +         +    +            
 y   5  1      39  0.011469              7( a1 )   +         +    +            
 y   5  1      45 -0.073998              2( b2 )   +         +         +       
 y   5  1      47  0.051676              4( b2 )   +         +         +       
 y   5  1      52  0.053211              2( b1 )   +         +              +  
 y   5  1      54 -0.042745              4( b1 )   +         +              +  
 y   5  1      74 -0.099497              3( a1 )   +                   +    +  
 y   5  1      78  0.073974              7( a1 )   +                   +    +  
 x   5  1     192  0.017254   10( a1 )   3( a2 )   +    +                      
 x   5  1     194 -0.019468    1( b1 )   1( b2 )   +    +                      
 x   5  1     196 -0.014596    3( b1 )   1( b2 )   +    +                      
 x   5  1     202 -0.012387    2( b1 )   2( b2 )   +    +                      
 x   5  1     204  0.011210    4( b1 )   2( b2 )   +    +                      
 x   5  1     208 -0.014825    1( b1 )   3( b2 )   +    +                      
 x   5  1     210 -0.011714    3( b1 )   3( b2 )   +    +                      
 x   5  1     216  0.011986    2( b1 )   4( b2 )   +    +                      
 x   5  1     218 -0.016816    4( b1 )   4( b2 )   +    +                      
 x   5  1     234 -0.015043    6( b1 )   6( b2 )   +    +                      
 x   5  1     274  0.015942   10( a1 )   3( a2 )   +         +                 
 x   5  1     276 -0.025372    1( b1 )   1( b2 )   +         +                 
 x   5  1     278 -0.019291    3( b1 )   1( b2 )   +         +                 
 x   5  1     284 -0.010321    2( b1 )   2( b2 )   +         +                 
 x   5  1     286  0.010021    4( b1 )   2( b2 )   +         +                 
 x   5  1     290 -0.019014    1( b1 )   3( b2 )   +         +                 
 x   5  1     292 -0.015244    3( b1 )   3( b2 )   +         +                 
 x   5  1     300 -0.014000    4( b1 )   4( b2 )   +         +                 
 x   5  1     316 -0.012986    6( b1 )   6( b2 )   +         +                 
 x   5  1     445  0.015052    3( a2 )   7( b1 )   +                   +       
 x   5  1     446 -0.028386    1( a1 )   1( b2 )   +                   +       
 x   5  1     449 -0.021372    4( a1 )   1( b2 )   +                   +       
 x   5  1     459  0.015932    3( a1 )   2( b2 )   +                   +       
 x   5  1     463 -0.014989    7( a1 )   2( b2 )   +                   +       
 x   5  1     468 -0.021322    1( a1 )   3( b2 )   +                   +       
 x   5  1     471 -0.017013    4( a1 )   3( b2 )   +                   +       
 x   5  1     481 -0.014973    3( a1 )   4( b2 )   +                   +       
 x   5  1     485  0.021765    7( a1 )   4( b2 )   +                   +       
 x   5  1     510  0.017152   10( a1 )   6( b2 )   +                   +       
 x   5  1     512 -0.010039    1( a1 )   7( b2 )   +                   +       
 x   5  1     515 -0.010546    4( a1 )   7( b2 )   +                   +       
 x   5  1     521  0.019573   10( a1 )   7( b2 )   +                   +       
 x   5  1     523 -0.034817    1( a1 )   1( b1 )   +                        +  
 x   5  1     526 -0.026159    4( a1 )   1( b1 )   +                        +  
 x   5  1     536 -0.016348    3( a1 )   2( b1 )   +                        +  
 x   5  1     540  0.015081    7( a1 )   2( b1 )   +                        +  
 x   5  1     545 -0.026191    1( a1 )   3( b1 )   +                        +  
 x   5  1     548 -0.020864    4( a1 )   3( b1 )   +                        +  
 x   5  1     558  0.015246    3( a1 )   4( b1 )   +                        +  
 x   5  1     562 -0.021877    7( a1 )   4( b1 )   +                        +  
 x   5  1     586 -0.013247    9( a1 )   6( b1 )   +                        +  
 x   5  1     587 -0.018556   10( a1 )   6( b1 )   +                        +  
 x   5  1     592  0.010218    4( a1 )   7( b1 )   +                        +  
 x   5  1     597  0.014681    9( a1 )   7( b1 )   +                        +  
 x   5  1     598  0.010551   10( a1 )   7( b1 )   +                        +  
 x   5  1     620 -0.015427    3( a2 )   7( b2 )   +                        +  
 x   5  1     805 -0.011471    3( a2 )   1( b1 )        +              +       
 x   5  1     811 -0.010694    3( a2 )   3( b1 )        +              +       
 x   5  1     833 -0.014695   10( a1 )   1( b2 )        +              +       
 x   5  1     839 -0.016438    5( a1 )   2( b2 )        +              +       
 x   5  1     855 -0.013390   10( a1 )   3( b2 )        +              +       
 x   5  1     861  0.013319    5( a1 )   4( b2 )        +              +       
 x   5  1     879  0.014492    1( a1 )   6( b2 )        +              +       
 x   5  1     882  0.013454    4( a1 )   6( b2 )        +              +       
 x   5  1     909 -0.012053    9( a1 )   1( b1 )        +                   +  
 x   5  1     916  0.017692    5( a1 )   2( b1 )        +                   +  
 x   5  1     931 -0.011137    9( a1 )   3( b1 )        +                   +  
 x   5  1     938 -0.012760    5( a1 )   4( b1 )        +                   +  
 x   5  1     956 -0.015076    1( a1 )   6( b1 )        +                   +  
 x   5  1     959 -0.013902    4( a1 )   6( b1 )        +                   +  
 x   5  1     980  0.011965    3( a2 )   1( b2 )        +                   +  
 x   5  1     986  0.011017    3( a2 )   3( b2 )        +                   +  
 x   5  1    1129 -0.011438   10( a1 )   1( b2 )             +         +       
 x   5  1    1135 -0.014582    5( a1 )   2( b2 )             +         +       
 x   5  1    1151 -0.010628   10( a1 )   3( b2 )             +         +       
 x   5  1    1157  0.010333    5( a1 )   4( b2 )             +         +       
 x   5  1    1175  0.012231    1( a1 )   6( b2 )             +         +       
 x   5  1    1178  0.011262    4( a1 )   6( b2 )             +         +       
 x   5  1    1205 -0.010100    9( a1 )   1( b1 )             +              +  
 x   5  1    1212  0.013361    5( a1 )   2( b1 )             +              +  
 x   5  1    1234 -0.010870    5( a1 )   4( b1 )             +              +  
 x   5  1    1252 -0.011872    1( a1 )   6( b1 )             +              +  
 x   5  1    1255 -0.011016    4( a1 )   6( b1 )             +              +  
 x   5  1    1499  0.022084    3( a1 )   5( a1 )                       +    +  
 x   5  1    1510  0.016725    5( a1 )   7( a1 )                       +    +  
 x   5  1    1527  0.014916    1( a1 )  10( a1 )                       +    +  
 x   5  1    1530  0.013693    4( a1 )  10( a1 )                       +    +  
 x   5  1    1564 -0.018153    1( b1 )   7( b1 )                       +    +  
 x   5  1    1566 -0.016774    3( b1 )   7( b1 )                       +    +  
 x   5  1    1585 -0.018766    1( b2 )   7( b2 )                       +    +  
 x   5  1    1587 -0.017349    3( b2 )   7( b2 )                       +    +  

 ci coefficient statistics:
           rq > 0.1                3
      0.1> rq > 0.01              94
     0.01> rq > 0.001            255
    0.001> rq > 0.0001           281
   0.0001> rq > 0.00001           94
  0.00001> rq > 0.000001          26
 0.000001> rq                    837
           all                  1590
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:          40 2x:           0 4x:           0
All internal counts: zz :          15 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.113389848791     -5.243751052305
     2     2      0.741701166974    -34.300262875946
     3     3      0.603130190767    -27.891978077729
     4     4     -0.000056665303      0.002609074560

 number of reference csfs (nref) is     4.  root number (iroot) is  1.
 c0**2 =   0.92674391  c**2 (all zwalks) =   0.92674391

 pople ci energy extrapolation is computed with  4 correlated electrons.

 eref      =    -46.245383353754   "relaxed" cnot**2         =   0.926743909126
 eci       =    -46.291386965653   deltae = eci - eref       =  -0.046003611899
 eci+dv1   =    -46.294757010427   dv1 = (1-cnot**2)*deltae  =  -0.003370044774
 eci+dv2   =    -46.295023401502   dv2 = dv1 / cnot**2       =  -0.003636435849
 eci+dv3   =    -46.295335522037   dv3 = dv1 / (2*cnot**2-1) =  -0.003948556384
 eci+pople =    -46.293202352184   (  4e- scaled deltae )    =  -0.047818998429
 passed aftci ... 
 readint2: molcas,dalton2=                     0                     0
 files%faoints=aoints              
lodens (list->root)=  1
sifcfg setup: record length 4096 DP
# d1 elements per record  3272
# d2 elements per record  2730
  The MR-CISD density will be calculated.
 item #                     1 suffix=:.drt1.state1:
 read_civout: repnuc=  -43.4985876562748     
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method:  0 last record  1max overlap with ref# 96% root-following 0
 MR-CISD energy:   -46.29138697    -2.79279931
 residuum:     0.00089501
 deltae:     0.00000946
 apxde:     0.00000127

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.96267389     0.10685176     0.07986332     0.16981920     0.16317162     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.96267389     0.10685176     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 computing final density
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=      16  DYX=      60  DYW=       0
   D0Z=       3  D0Y=      18  D0X=      12  D0W=       0
  DDZI=      16 DDYI=      60 DDXI=      30 DDWI=       0
  DDZE=       0 DDYE=      20 DDXE=      15 DDWE=       0
================================================================================
Trace of MO density:     4.000000
    4  correlated and     8  frozen core electrons

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.99109640    -0.00006614     0.00000248     0.00000000     0.00000637     0.00062662
   MO   4     0.00000000     0.00000000    -0.00006614     0.58813630     0.46788522     0.00000000     0.00036038     0.07174255
   MO   5     0.00000000     0.00000000     0.00000248     0.46788522     0.39572044     0.00000000     0.00128257     0.05578849
   MO   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00453090     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000     0.00000637     0.00036038     0.00128257     0.00000000     0.00008981     0.00003142
   MO   8     0.00000000     0.00000000     0.00062662     0.07174255     0.05578849     0.00000000     0.00003142     0.01191585
   MO   9     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00367169     0.00000000     0.00000000
   MO  10     0.00000000     0.00000000     0.00171516     0.00000861     0.00001023     0.00000000    -0.00000043     0.00000296
   MO  11     0.00000000     0.00000000    -0.00006638    -0.00541819    -0.00342384     0.00000000     0.00007663    -0.00099904
   MO  12     0.00000000     0.00000000    -0.00061777    -0.05111800    -0.04055515     0.00000000    -0.00009159    -0.00917157
   MO  13     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00121290     0.00000000     0.00000000
   MO  14     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00087861     0.00000000     0.00000000
   MO  15     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00024362     0.00000000     0.00000000
   MO  16     0.00000000     0.00000000    -0.00050284     0.00000116     0.00000069     0.00000000     0.00000003    -0.00000008

                MO   9         MO  10         MO  11         MO  12         MO  13         MO  14         MO  15         MO  16
   MO   3     0.00000000     0.00171516    -0.00006638    -0.00061777     0.00000000     0.00000000     0.00000000    -0.00050284
   MO   4     0.00000000     0.00000861    -0.00541819    -0.05111800     0.00000000     0.00000000     0.00000000     0.00000116
   MO   5     0.00000000     0.00001023    -0.00342384    -0.04055515     0.00000000     0.00000000     0.00000000     0.00000069
   MO   6     0.00367169     0.00000000     0.00000000     0.00000000     0.00121290     0.00087861    -0.00024362     0.00000000
   MO   7     0.00000000    -0.00000043     0.00007663    -0.00009159     0.00000000     0.00000000     0.00000000     0.00000003
   MO   8     0.00000000     0.00000296    -0.00099904    -0.00917157     0.00000000     0.00000000     0.00000000    -0.00000008
   MO   9     0.00308121     0.00000000     0.00000000     0.00000000     0.00108339     0.00092539    -0.00027738     0.00000000
   MO  10     0.00000000     0.00250291    -0.00000099    -0.00000450     0.00000000     0.00000000     0.00000000    -0.00012411
   MO  11     0.00000000    -0.00000099     0.00016655     0.00073620     0.00000000     0.00000000     0.00000000     0.00000005
   MO  12     0.00000000    -0.00000450     0.00073620     0.00735404     0.00000000     0.00000000     0.00000000     0.00000021
   MO  13     0.00108339     0.00000000     0.00000000     0.00000000     0.00042557     0.00046383    -0.00015086     0.00000000
   MO  14     0.00092539     0.00000000     0.00000000     0.00000000     0.00046383     0.00136884     0.00076618     0.00000000
   MO  15    -0.00027738     0.00000000     0.00000000     0.00000000    -0.00015086     0.00076618     0.00311086     0.00000000
   MO  16     0.00000000    -0.00012411     0.00000005     0.00000021     0.00000000     0.00000000     0.00000000     0.00000771

Natural orbital populations,block 1
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     2.00000000     0.99110073     0.98263148     0.01490178     0.00817580     0.00567473     0.00339769
              MO     9       MO    10       MO    11       MO    12       MO    13       MO    14       MO    15       MO    16
  occ(*)=     0.00250601     0.00089231     0.00011069     0.00005907     0.00004968     0.00000413     0.00000191     0.00000138

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4
   MO   1     0.01398514     0.00128814    -0.00089267     0.00000000
   MO   2     0.00128814     0.00018412    -0.00014247     0.00000000
   MO   3    -0.00089267    -0.00014247     0.00012235     0.00000000
   MO   4     0.00000000     0.00000000     0.00000000     0.00232508

Natural orbital populations,block 2
              MO     1       MO     2       MO     3       MO     4       MO
  occ(*)=     0.01416229     0.00232508     0.00012415     0.00000517

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     0.95535922     0.00000000     0.09102293     0.00000000    -0.06521265     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.00491024     0.00000000     0.00397700     0.00000000     0.00131248    -0.00044322
   MO   4     0.00000000     0.09102293     0.00000000     0.01200543     0.00000000    -0.00925482     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.00397700     0.00000000     0.00332776     0.00000000     0.00116526    -0.00048061
   MO   6     0.00000000    -0.06521265     0.00000000    -0.00925482     0.00000000     0.00741178     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000     0.00131248     0.00000000     0.00116526     0.00000000     0.00045439    -0.00024820
   MO   8     0.00000000     0.00000000    -0.00044322     0.00000000    -0.00048061     0.00000000    -0.00024820     0.00200261
   MO   9     0.00000000     0.00000000    -0.00084352     0.00000000    -0.00087000     0.00000000    -0.00042634    -0.00069600

                MO   9
   MO   2     0.00000000
   MO   3    -0.00084352
   MO   4     0.00000000
   MO   5    -0.00087000
   MO   6     0.00000000
   MO   7    -0.00042634
   MO   8    -0.00069600
   MO   9     0.00149903

Natural orbital populations,block 3
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     0.96856594     0.00882058     0.00611109     0.00248724     0.00083565     0.00009941     0.00004855
              MO     9       MO
  occ(*)=     0.00000200

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     0.95743312     0.00000000     0.08772834     0.00000000    -0.06375207     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.00421106     0.00000000     0.00341870     0.00000000    -0.00113134    -0.00066865
   MO   4     0.00000000     0.08772834     0.00000000     0.01138788     0.00000000    -0.00887488     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.00341870     0.00000000     0.00287913     0.00000000    -0.00101676    -0.00070901
   MO   6     0.00000000    -0.06375207     0.00000000    -0.00887488     0.00000000     0.00719852     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000    -0.00113134     0.00000000    -0.00101676     0.00000000     0.00040234     0.00035670
   MO   8     0.00000000     0.00000000    -0.00066865     0.00000000    -0.00070901     0.00000000     0.00035670     0.00172221
   MO   9     0.00000000     0.00000000     0.00057691     0.00000000     0.00063556     0.00000000    -0.00033630     0.00050150

                MO   9
   MO   2     0.00000000
   MO   3     0.00057691
   MO   4     0.00000000
   MO   5     0.00063556
   MO   6     0.00000000
   MO   7    -0.00033630
   MO   8     0.00050150
   MO   9     0.00167119

Natural orbital populations,block 4
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     0.96979431     0.00764837     0.00611358     0.00219684     0.00098796     0.00011163     0.00005096
              MO     9       MO
  occ(*)=     0.00000179


 total number of electrons =   12.0000000000

 test slabel:                     4
  a1  a2  b1  b2 


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        a1  partial gross atomic populations
   ao class       1a1        2a1        3a1        4a1        5a1        6a1 
     3_ s       2.000000   0.000000   0.991063   0.000036   0.000000   0.000000
     3_ p       0.000000   2.000000   0.000000   0.000000   0.000000   0.007870
     3_ d       0.000000   0.000000   0.000037   0.982595   0.014902   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000000   0.000000   0.000306
 
   ao class       7a1        8a1        9a1       10a1       11a1       12a1 
     3_ s       0.000000   0.000000   0.002506   0.000000   0.000000   0.000000
     3_ p       0.000000   0.000001   0.000000   0.000119   0.000000   0.000000
     3_ d       0.005675   0.000000   0.000000   0.000000   0.000111   0.000059
     3_ f       0.000000   0.003396   0.000000   0.000773   0.000000   0.000000
 
   ao class      13a1       14a1       15a1       16a1 
     3_ s       0.000000   0.000000   0.000000   0.000001
     3_ p       0.000045   0.000000   0.000002   0.000000
     3_ d       0.000000   0.000004   0.000000   0.000000
     3_ f       0.000005   0.000000   0.000000   0.000000

                        a2  partial gross atomic populations
   ao class       1a2        2a2        3a2        4a2 
     3_ d       0.014162   0.000000   0.000124   0.000005
     3_ f       0.000000   0.002325   0.000000   0.000000

                        b1  partial gross atomic populations
   ao class       1b1        2b1        3b1        4b1        5b1        6b1 
     3_ p       2.000000   0.000000   0.008528   0.000000   0.000001   0.000118
     3_ d       0.000000   0.968566   0.000000   0.006111   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000293   0.000000   0.002486   0.000718
 
   ao class       7b1        8b1        9b1 
     3_ p       0.000000   0.000043   0.000002
     3_ d       0.000099   0.000000   0.000000
     3_ f       0.000000   0.000005   0.000000

                        b2  partial gross atomic populations
   ao class       1b2        2b2        3b2        4b2        5b2        6b2 
     3_ p       2.000000   0.000000   0.007320   0.000000   0.000001   0.000123
     3_ d       0.000000   0.969794   0.000000   0.006114   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000328   0.000000   0.002196   0.000865
 
   ao class       7b2        8b2        9b2 
     3_ p       0.000000   0.000047   0.000002
     3_ d       0.000112   0.000000   0.000000
     3_ f       0.000000   0.000004   0.000000


                        gross atomic populations
     ao            3_
      s         2.993607
      p         6.024223
      d         2.968471
      f         0.013700
    total      12.000000
 

 Total number of electrons:   12.00000000

 DA ...
