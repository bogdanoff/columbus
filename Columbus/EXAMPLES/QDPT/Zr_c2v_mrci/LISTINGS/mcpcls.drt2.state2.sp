

     ******************************************
     **    PROGRAM:              MCPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 original author: Daniel Robertson, FSU
 later revisions: Ron Shepard, ANL;
                  Michal Dallos, University Vienna



 This Version of Program mcpc is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



   ******  File header section  ******

 Headers form the restart file:
                                                                                    
    aoints SIFS file created by argos.      zam792            17:53:52.086 17-Dec-13
     title                                                                          
     title                                                                          
     title                                                                          
     title                                                                          


   ******  DRT info section  ******

 Informations for the DRT no.  1
 Header form the DRT file: 
     title                                                                          
 Molecular symmetry group:   sym1
 Total number of electrons:   12
 Spin multiplicity:            3
 Number of active orbitals:    6
 Number of active electrons:   4
 Total number of CSFs:        21

   ***  Informations from the DRT number:   1

 
 Symmetry orbital summary:
 Symm.blocks:         1     2     3     4
 Symm.labels:         a1    a2    b1    b2 

 List of doubly occupied orbitals:
  1 a1   2 a1   1 b1   1 b2 

 List of active orbitals:
  3 a1   4 a1   5 a1   1 a2   2 b1   2 b2 

 Informations for the DRT no.  2
 Header form the DRT file: 
     title                                                                          
 Molecular symmetry group:    a2 
 Total number of electrons:   12
 Spin multiplicity:            3
 Number of active orbitals:    6
 Number of active electrons:   4
 Total number of CSFs:        28

   ***  Informations from the DRT number:   2

 
 Symmetry orbital summary:
 Symm.blocks:         1     2     3     4
 Symm.labels:         a1    a2    b1    b2 

 List of doubly occupied orbitals:
  1 a1   2 a1   1 b1   1 b2 

 List of active orbitals:
  3 a1   4 a1   5 a1   1 a2   2 b1   2 b2 

 Informations for the DRT no.  3
 Header form the DRT file: 
     title                                                                          
 Molecular symmetry group:    b1 
 Total number of electrons:   12
 Spin multiplicity:            3
 Number of active orbitals:    6
 Number of active electrons:   4
 Total number of CSFs:        28

   ***  Informations from the DRT number:   3

 
 Symmetry orbital summary:
 Symm.blocks:         1     2     3     4
 Symm.labels:         a1    a2    b1    b2 

 List of doubly occupied orbitals:
  1 a1   2 a1   1 b1   1 b2 

 List of active orbitals:
  3 a1   4 a1   5 a1   1 a2   2 b1   2 b2 

 Informations for the DRT no.  4
 Header form the DRT file: 
     title                                                                          
 Molecular symmetry group:    b2 
 Total number of electrons:   12
 Spin multiplicity:            3
 Number of active orbitals:    6
 Number of active electrons:   4
 Total number of CSFs:        28

   ***  Informations from the DRT number:   4

 
 Symmetry orbital summary:
 Symm.blocks:         1     2     3     4
 Symm.labels:         a1    a2    b1    b2 

 List of doubly occupied orbitals:
  1 a1   2 a1   1 b1   1 b2 

 List of active orbitals:
  3 a1   4 a1   5 a1   1 a2   2 b1   2 b2 


   ******  MCSCF convergence information:  ******

 MCSCF convergence criteria were satisfied.

 mcscf energy=   -46.3018785757    nuclear repulsion=     0.0000000000
 demc=             0.0000000000    wnorm=                 0.0000008053
 knorm=            0.0000000310    apxde=                 0.0000000000


 MCSCF calculation performmed for   4 symmetries.

 State averaging:
 No,  ssym, navst, wavst
  1    a1     1   0.1429
  2    a2     2   0.1429 0.1429
  3    b1     2   0.1429 0.1429
  4    b2     2   0.1429 0.1429

 Input the DRT No of interest: [  1]:
In the DRT No.: 2 there are  2 states.

 Which one to take? [  1]:
 The CSFs for the state No  2 of the symmetry  a2  will be printed
 according to the following print options :

 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 9) export wave function files for cioverlap (all states).
 0) end.

 input menu number [  0]: csfs will be printed based on coefficient magnitudes.

 input the coefficient threshold (end with 0.) [ 0.0000]:
 List of active orbitals:
  3 a1   4 a1   5 a1   1 a2   2 b1   2 b2 

   csf       coeff       coeff**2    step(*)
  -----  ------------  ------------  ------------
      1  0.6814940786  0.4644341791  310100
      3  0.6721315254  0.4517607875  300011
      2 -0.2463554058  0.0606909860  301100
     13  0.0688789669  0.0047443121  101021
      4  0.0661312328  0.0043733400  130100
     15  0.0475505915  0.0022610588  100130
      9 -0.0393893545  0.0015515212  110021
     19  0.0329766896  0.0010874621  013100
     14  0.0329173091  0.0010835492  101012
     24 -0.0317952603  0.0010109386  010103
     28 -0.0312048006  0.0009737396  000311
      8 -0.0308697741  0.0009529430  111200
     25 -0.0292326819  0.0008545497  003011
     12  0.0290175128  0.0008420161  102011
     18 -0.0244689321  0.0005987286  030011
      6 -0.0242952728  0.0005902603  120011
      5  0.0226125310  0.0005113266  121100
      7  0.0219304040  0.0004809426  112100
     23 -0.0213173891  0.0004544311  010130
     11 -0.0162523060  0.0002641374  103100
     26  0.0137248127  0.0001883705  001130
     27  0.0109438037  0.0001197668  001103
     21  0.0065799932  0.0000432963  011021
     20 -0.0064760911  0.0000419398  012011
     17  0.0064519071  0.0000416271  031100
     10  0.0061871257  0.0000382805  110012
     16  0.0023281606  0.0000054203  100103

 input the coefficient threshold (end with 0.) [ 0.0000]:
 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 9) export wave function files for cioverlap (all states).
 0) end.

 input menu number [  0]: