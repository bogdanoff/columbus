1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=     3)...
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.2453833560  2.2204E-15  5.4652E-02  2.1660E-01  1.0000E-03

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  1  1    -46.2453833560  2.2204E-15  5.4652E-02  2.1660E-01  1.0000E-03

 from bk iterations: iconv=   1

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.2897720606  4.4389E-02  1.2904E-03  2.8693E-02  1.0000E-03
 mr-sdci #  2  1    -46.2911929720  1.4209E-03  1.2079E-04  9.5844E-03  1.0000E-03
 mr-sdci #  3  1    -46.2913765571  1.8359E-04  9.1230E-06  3.0340E-03  1.0000E-03
 mr-sdci #  4  1    -46.2913874738  1.0917E-05  4.6602E-07  5.7168E-04  1.0000E-03

 mr-sdci  convergence criteria satisfied after  4 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  4  1    -46.2913874738  1.0917E-05  4.6602E-07  5.7168E-04  1.0000E-03

 number of reference csfs (nref) is     3.  root number (iroot) is  1.
 c0**2 =   0.92668127  c**2 (all zwalks) =   0.92668127

 eref      =    -46.245383347381   "relaxed" cnot**2         =   0.926681268924
 eci       =    -46.291387473751   deltae = eci - eref       =  -0.046004126370
 eci+dv1   =    -46.294760437921   dv1 = (1-cnot**2)*deltae  =  -0.003372964170
 eci+dv2   =    -46.295027305787   dv2 = dv1 / cnot**2       =  -0.003639832036
 eci+dv3   =    -46.295340030865   dv3 = dv1 / (2*cnot**2-1) =  -0.003952557114
 eci+pople =    -46.293204550515   (  4e- scaled deltae )    =  -0.047821203134
