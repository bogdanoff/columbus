1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=    33)...
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.2740792806  6.2172E-15  5.5935E-02  2.2790E-01  1.0000E-03

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  1  1    -46.2740792806  6.2172E-15  5.5935E-02  2.2790E-01  1.0000E-03

 from bk iterations: iconv=   1

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.3230360383  4.8957E-02  2.3161E-03  4.4693E-02  1.0000E-03
 mr-sdci #  2  1    -46.3255865141  2.5505E-03  3.3928E-04  1.6793E-02  1.0000E-03
 mr-sdci #  3  1    -46.3259032066  3.1669E-04  6.1580E-05  7.2353E-03  1.0000E-03
 mr-sdci #  4  1    -46.3259521761  4.8969E-05  6.3229E-06  2.2811E-03  1.0000E-03
 mr-sdci #  5  1    -46.3259581873  6.0112E-06  6.4156E-07  7.6901E-04  1.0000E-03

 mr-sdci  convergence criteria satisfied after  5 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  5  1    -46.3259581873  6.0112E-06  6.4156E-07  7.6901E-04  1.0000E-03

 number of reference csfs (nref) is    33.  root number (iroot) is  1.
 c0**2 =   0.92261427  c**2 (all zwalks) =   0.92261427

 eref      =    -46.274036773044   "relaxed" cnot**2         =   0.922614273433
 eci       =    -46.325958187267   deltae = eci - eref       =  -0.051921414223
 eci+dv1   =    -46.329976163631   dv1 = (1-cnot**2)*deltae  =  -0.004017976364
 eci+dv2   =    -46.330313177732   dv2 = dv1 / cnot**2       =  -0.004354990465
 eci+dv3   =    -46.330711903145   dv3 = dv1 / (2*cnot**2-1) =  -0.004753715878
 eci+pople =    -46.328131866090   (  4e- scaled deltae )    =  -0.054095093046
