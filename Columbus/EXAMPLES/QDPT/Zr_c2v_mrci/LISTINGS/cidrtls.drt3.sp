 
 program cidrt 7.0  

 distinct row table construction, reference csf selection, and internal
 walk selection for multireference single- and double-excitation
configuration interaction.

 references:  r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).
              h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. symp. 15, 91 (1981).

 based on the initial version by  Ron Shepard

 extended for spin-orbit CI calculations ( Russ Pitzer, OSU)

 and large active spaces (Thomas Müller, FZ(21 Juelich)

 This Version of Program CIDRT is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de

*********************** File revision status: ***********************
* cidrt1.F9 Revision: 1.1.6.2           Date: 2013/04/11 14:37:29   * 
* cidrt2.F9 Revision: 1.1.6.2           Date: 2013/04/11 14:37:29   * 
* cidrt3.F9 Revision: 1.1.6.2           Date: 2013/04/11 14:37:29   * 
* cidrt4.F9 Revision: 1.1.6.2           Date: 2013/04/11 14:37:29   * 
********************************************************************

 workspace allocation parameters: lencor=  13107200 mem1=         0 ifirst=         1
 expanded "keystrokes" are being written to file:
 /bigscratch/Columbus_C70/tests/Zr_c2v/scr/WORK/cidrtky                          
 Spin-Orbit CI Calculation?(y,[n]) Spin-Free Calculation
 
 input the spin multiplicity [  0]: spin multiplicity, smult            :   1    singlet 
 input the total number of electrons [  0]: total number of electrons, nelt     :    12
 input the number of irreps (1:8) [  0]: point group dimension, nsym         :     4
 enter symmetry labels:(y,[n]) enter 4 labels (a4):
 enter symmetry label, default=   1
 enter symmetry label, default=   2
 enter symmetry label, default=   3
 enter symmetry label, default=   4
 symmetry labels: (symmetry, slabel)
 ( 1,  a1 ) ( 2,  a2 ) ( 3,  b1 ) ( 4,  b2 ) 
 input nmpsy(*):
 nmpsy(*)=        16   4   9   9
 
   symmetry block summary
 block(*)=         1   2   3   4
 slabel(*)=      a1  a2  b1  b2 
 nmpsy(*)=        16   4   9   9
 
 total molecular orbitals            :    38
 input the molecular spatial symmetry (irrep 1:nsym) [  0]: state spatial symmetry label        :  b1 
 
 input the frozen core orbitals (sym(i),rmo(i),i=1,nfct):
 total frozen core orbitals, nfct    :     1
 
 fcorb(*)=         0
 0 < fcorb(i) <= nmot
 
 input the frozen core orbitals (sym(i),rmo(i),i=1,nfct):
 total frozen core orbitals, nfct    :     4
 
 fcorb(*)=         1   2  21  30
 slabel(*)=      a1  a1  b1  b2 
 
 number of frozen core orbitals      :     4
 number of frozen core electrons     :     8
 number of internal electrons        :     4
 
 input the frozen virtual orbitals (sym(i),rmo(i),i=1,nfvt):
 total frozen virtual orbitals, nfvt :     0

 no frozen virtual orbitals entered
 
 input the internal orbitals (sym(i),rmo(i),i=1,niot):
 niot                                :     6
 
 modrt(*)=         3   4   5  17  22  31
 slabel(*)=      a1  a1  a1  a2  b1  b2 
 
 total number of orbitals            :    38
 number of frozen core orbitals      :     4
 number of frozen virtual orbitals   :     0
 number of internal orbitals         :     6
 number of external orbitals         :    28
 
 orbital-to-level mapping vector
 map(*)=          -1  -1  29  30  31   1   2   3   4   5   6   7   8   9  10
                  11  32  12  13  14  -1  33  15  16  17  18  19  20  21  -1
                  34  22  23  24  25  26  27  28
 
 input the number of ref-csf doubly-occupied orbitals [  0]: (ref) doubly-occupied orbitals      :     0
 
 no. of internal orbitals            :     6
 no. of doubly-occ. (ref) orbitals   :     0
 no. active (ref) orbitals           :     6
 no. of active electrons             :     4
 
 input the active-orbital, active-electron occmnr(*):
   3  4  5 17 22 31
 input the active-orbital, active-electron occmxr(*):
   3  4  5 17 22 31
 
 actmo(*) =        3   4   5  17  22  31
 occmnr(*)=        0   0   0   0   0   4
 occmxr(*)=        4   4   4   4   4   4
 reference csf cumulative electron occupations:
 modrt(*)=         3   4   5  17  22  31
 occmnr(*)=        0   0   0   0   0   4
 occmxr(*)=        4   4   4   4   4   4
 
 input the active-orbital bminr(*):
   3  4  5 17 22 31
 input the active-orbital bmaxr(*):
   3  4  5 17 22 31
 reference csf b-value constraints:
 modrt(*)=         3   4   5  17  22  31
 bminr(*)=         0   0   0   0   0   0
 bmaxr(*)=         4   4   4   4   4   4
 input the active orbital smaskr(*):
   3  4  5 17 22 31
 modrt:smaskr=
   3:1111   4:1111   5:1111  17:1111  22:1111  31:1111
 
 input the maximum excitation level from the reference csfs [  2]: maximum excitation from ref. csfs:  :     2
 number of internal electrons:       :     4
 
 input the internal-orbital mrsdci occmin(*):
   3  4  5 17 22 31
 input the internal-orbital mrsdci occmax(*):
   3  4  5 17 22 31
 mrsdci csf cumulative electron occupations:
 modrt(*)=         3   4   5  17  22  31
 occmin(*)=        0   0   0   0   0   2
 occmax(*)=        4   4   4   4   4   4
 
 input the internal-orbital mrsdci bmin(*):
   3  4  5 17 22 31
 input the internal-orbital mrsdci bmax(*):
   3  4  5 17 22 31
 mrsdci b-value constraints:
 modrt(*)=         3   4   5  17  22  31
 bmin(*)=          0   0   0   0   0   0
 bmax(*)=          4   4   4   4   4   4
 
 input the internal-orbital smask(*):
   3  4  5 17 22 31
 modrt:smask=
   3:1111   4:1111   5:1111  17:1111  22:1111  31:1111
 
 internal orbital summary:
 block(*)=         1   1   1   2   3   4
 slabel(*)=      a1  a1  a1  a2  b1  b2 
 rmo(*)=           3   4   5   1   2   2
 modrt(*)=         3   4   5  17  22  31
 
 reference csf info:
 occmnr(*)=        0   0   0   0   0   4
 occmxr(*)=        4   4   4   4   4   4
 
 bminr(*)=         0   0   0   0   0   0
 bmaxr(*)=         4   4   4   4   4   4
 
 
 mrsdci csf info:
 occmin(*)=        0   0   0   0   0   2
 occmax(*)=        4   4   4   4   4   4
 
 bmin(*)=          0   0   0   0   0   0
 bmax(*)=          4   4   4   4   4   4
 

 a priori removal of distinct rows:

 input the level, a, and b values for the vertices 
 to be removed (-1/ to end).

 input level, a, and b (-1/ to end):
 no vertices marked for removal
 
 impose generalized interacting space restrictions?(y,[n]) generalized interacting space restrictions will not be imposed.
 multp(*)=
  hmult                     0
 lxyzir   0   0   0
 symmetry of spin functions (spnir)
       --------------------------Ms ----------------------------
   S     1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19
   1     1  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   2     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   3     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   4     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   5     1  0  0  0  1  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   6     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   7     0  0  0  1  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   8     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   9     1  0  0  0  1  0  0  0  1  0  0  0  0  0  0  0  0  0  0
  10     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  11     0  0  0  1  0  0  0  1  0  0  0  0  0  0  0  0  0  0  0
  12     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  13     1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  0  0  0
  14     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  15     0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  0  0  0  0
  16     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  17     1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0
  18     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  19     0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0

 number of rows in the drt :  32

 manual arc removal step:


 input the level, a, b, and step values 
 for the arcs to be removed (-1/ to end).

 input the level, a, b, and step (-1/ to end):
 remarc:   0 arcs removed out of   0 specified.

 xbarz=         105
 xbary=          70
 xbarx=          15
 xbarw=          21
        --------
 nwalk=         211
 input the range of drt levels to print (l1,l2):
 levprt(*)        -1   0

 reference-csf selection step 1:
 total number of z-walks in the drt, nzwalk=     105

 input the list of allowed reference symmetries:
 allowed reference symmetries:             1    2    3    4
 allowed reference symmetry labels:      a1   a2   b1   b2 
 keep all of the z-walks as references?(y,[n]) y or n required
 keep all of the z-walks as references?(y,[n]) all z-walks are initially deleted.
 
 generate walks while applying reference drt restrictions?([y],n) reference drt restrictions will be imposed on the z-walks.
 
 impose additional orbital-group occupation restrictions?(y,[n]) 
 apply primary reference occupation restrictions?(y,[n]) 
 manually select individual walks?(y,[n])
 step 1 reference csf selection complete.
      105 csfs initially selected from     105 total walks.

 beginning step-vector based selection.
 enter [internal_orbital_step_vector/disposition] pairs:

 enter internal orbital step vector, (-1/ to end):
   3  4  5 17 22 31

 step 2 reference csf selection complete.
      105 csfs currently selected from     105 total walks.

 beginning numerical walk based selection.
 enter positive walk numbers to add walks,
 negative walk numbers to delete walks, and zero to end:

 input reference walk number (0 to end) [  0]:
 numerical walk-number based selection complete.
      105 reference csfs selected from     105 total z-walks.
 
 input the reference occupations, mu(*):
 reference occupations:
 mu(*)=            0   0   0   0   0   0
 
 number of step vectors saved:    105

 exlimw: beginning excitation-based walk selection...
 exlimw: nref=                   105

  number of valid internal walks of each symmetry:

       a1      a2      b1      b2 
      ----    ----    ----    ----
 z       0       0      24       0
 y      19      17      17      17
 x       3       4       4       4
 w       9       4       4       4

 csfs grouped by internal walk symmetry:

       a1      a2      b1      b2 
      ----    ----    ----    ----
 z       0       0      24       0
 y     133     119     187      51
 x     294     392     400     328
 w     882     392     512     328

 total csf counts:
 z-vertex:       24
 y-vertex:      490
 x-vertex:     1414
 w-vertex:     2114
           --------
 total:        4042
 
 this is an obsolete prompt.(y,[n])
 final mrsdci walk selection step:

 nvalw(*)=      24      70      15      21 nvalwt=     130

 enter positive walk numbers to add walks,
 negative walk numbers to delete walks, and zero to end.

 input mrsdci walk number (0 to end) [  0]:
 end of manual mrsdci walk selection.
 number added=   0 number removed=   0

 nvalw(*)=      24      70      15      21 nvalwt=     130

 lprune input numv1,nwalk=                   130                   211
 lprune input xbar(1,1),nref=                   105                   105

 lprune: l(*,*,*) pruned with nwalk=     211 nvalwt=     130=  24  70  15  21
 lprune:  z-drt, nprune=    14
 lprune:  y-drt, nprune=    19
 lprune: wx-drt, nprune=    31

 xbarz=         105
 xbary=          70
 xbarx=          15
 xbarw=          21
        --------
 nwalk=         211
 levprt(*)        -1   0

 beginning the reference csf index recomputation...

     iref   iwalk  step-vector
   ------  ------  ------------
        1       1  330000
        2       2  312000
        3       3  310200
        4       4  310020
        5       5  310002
        6       6  303000
        7       7  301200
        8       8  301020
        9       9  301002
       10      10  300300
       11      11  300120
       12      12  300102
       13      13  300030
       14      14  300012
       15      15  300003
       16      16  132000
       17      17  130200
       18      18  130020
       19      19  130002
       20      20  123000
       21      21  121200
       22      22  121020
       23      23  121002
       24      24  120300
       25      25  120120
       26      26  120102
       27      27  120030
       28      28  120012
       29      29  120003
       30      30  112200
       31      31  112020
       32      32  112002
       33      33  110220
       34      34  110202
       35      35  110022
       36      36  103200
       37      37  103020
       38      38  103002
       39      39  102300
       40      40  102120
       41      41  102102
       42      42  102030
       43      43  102012
       44      44  102003
       45      45  101220
       46      46  101202
       47      47  101022
       48      48  100320
       49      49  100302
       50      50  100230
       51      51  100212
       52      52  100203
       53      53  100122
       54      54  100032
       55      55  100023
       56      56  033000
       57      57  031200
       58      58  031020
       59      59  031002
       60      60  030300
       61      61  030120
       62      62  030102
       63      63  030030
       64      64  030012
       65      65  030003
       66      66  013200
       67      67  013020
       68      68  013002
       69      69  012300
       70      70  012120
       71      71  012102
       72      72  012030
       73      73  012012
       74      74  012003
       75      75  011220
       76      76  011202
       77      77  011022
       78      78  010320
       79      79  010302
       80      80  010230
       81      81  010212
       82      82  010203
       83      83  010122
       84      84  010032
       85      85  010023
       86      86  003300
       87      87  003120
       88      88  003102
       89      89  003030
       90      90  003012
       91      91  003003
       92      92  001320
       93      93  001302
       94      94  001230
       95      95  001212
       96      96  001203
       97      97  001122
       98      98  001032
       99      99  001023
      100     100  000330
      101     101  000312
      102     102  000303
      103     103  000132
      104     104  000123
      105     105  000033
 indx01:   105 elements set in vec01(*)

 beginning the valid upper walk index recomputation...
 indx01:   130 elements set in vec01(*)

 beginning the final csym(*) computation...

  number of valid internal walks of each symmetry:

       a1      a2      b1      b2 
      ----    ----    ----    ----
 z       0       0      24       0
 y      19      17      17      17
 x       3       4       4       4
 w       9       4       4       4

 csfs grouped by internal walk symmetry:

       a1      a2      b1      b2 
      ----    ----    ----    ----
 z       0       0      24       0
 y     133     119     187      51
 x     294     392     400     328
 w     882     392     512     328

 total csf counts:
 z-vertex:       24
 y-vertex:      490
 x-vertex:     1414
 w-vertex:     2114
           --------
 total:        4042
 
 input a title card, default=cidrt_title
 title card:
  cidrt_title                                                                   
  
 
 input a drt file name, default=cidrtfl
 drt and indexing arrays will be written to file:
 /bigscratch/Columbus_C70/tests/Zr_c2v/scr/WORK/cidrtfl                          
 
 write the drt file?([y],n) drt file is being written...
 wrtstr:  a1  a2  b1  b2 
nwalk=     211 cpos=      63 maxval=    9 cmprfactor=   70.14 %.
nwalk=     211 cpos=      51 maxval=   99 cmprfactor=   51.66 %.
 compressed with: nwalk=     211 cpos=      63 maxval=    9 cmprfactor=   70.14 %.
initial index vector length:       211
compressed index vector length:        63reduction:  70.14%
nwalk=     105 cpos=      13 maxval=    9 cmprfactor=   87.62 %.
nwalk=     105 cpos=       2 maxval=   99 cmprfactor=   96.19 %.
nwalk=     105 cpos=       1 maxval=  999 cmprfactor=   97.14 %.
 compressed with: nwalk=     105 cpos=       2 maxval=   99 cmprfactor=   96.19 %.
initial ref vector length:       105
compressed ref vector length:         2reduction:  98.10%
