1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      2009-03.    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


================ Computing sorting integral file structure ================

                    -----z----- -----y----- -----x----- -----w----- ---total---

                CSFs         4         140        1414           0        1558
      internal walks        15          20          15           0          50
valid internal walks         4          20          15           0          39
 getinfoarray: info=                     6 :                     1
                  8192                  6552                  8192
                  5460                     0
 icd(3)=                  2976 ci%nnlev=                   595  l2rec=
                  8192  n2max=                  5460
 lcore1,lcore2=              13068137              13044821
 lencor,maxblo              13107200                 60000
========================================
 current settings:
 minbl3         381
 minbl4         381
 locmaxbl3     4288
 locmaxbuf     2144
 maxbl3       60000
 maxbl3       60000
 maxbl4       60000
 maxbuf       30006
========================================

 sorted 4-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)

 sorted 3-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)
 Orig.  diagonal integrals:  1electron:        34
                             0ext.    :        42
                             2ext.    :       336
                             4ext.    :       812


 Orig. off-diag. integrals:  4ext.    :     24411
                             3ext.    :     18720
                             2ext.    :      6792
                             1ext.    :      1272
                             0ext.    :        99
                             2ext. SO :         0
                             1ext. SO :         0
                             0ext. SO :         0
                             1electron:       128


 Sorted integrals            3ext.  w :     17370 x :     16020
                             4ext.  w :     20955 x :     17777


Cycle #  1 sortfile size=    163835(       5 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13068137
Cycle #  2 sortfile size=    163835(       5 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13068137
 minimum size of srtscr:     98301 WP (     3 records)
 maximum size of srtscr:    163835 WP (     5 records)
diagi   file:      4 records  of   1536 WP each=>       6144 WP total
ofdgi   file:      8 records  of   1536 WP each=>      12288 WP total
fil3w   file:      1 records  of  30006 WP each=>      30006 WP total
fil3x   file:      1 records  of  30006 WP each=>      30006 WP total
fil4w   file:      1 records  of  30006 WP each=>      30006 WP total
fil4x   file:      1 records  of  30006 WP each=>      30006 WP total
 compressed index vector length=                    14
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 0,
  GSET = 0,
   DAVCOR =10,
  NCOREL = 4
   nroot=1
  IVMODE = 3
  NBKITR = 1
   nvbkmn=2
  RTOLBK = 1e-3
   niter=30
   nvcimn=2
   rtolci=1e-3
  NVCIMX = 6
  NVRFMX = 6
  NVBKMX = 6
  IDEN  = 1
  CSFPRN = 10,
  update_mode=1
  nvrfmn=2
 /&end
 ------------------------------------------------------------------------
lodens (list->root)=  1
invlodens (root->list)=  1
 USING SEGMENTS OF EQUAL SIZE

****************  list of control variables  ****************
 lvlprt =    0      nroot  =    1      noldv  =   0      noldhv =   0
 nunitv =    1      nbkitr =    1      niter  =  30      davcor =  10
 csfprn =   10      ivmode =    3      istrt  =   0      vout   =   0
 iortls =    0      nvbkmx =    6      ibktv  =  -1      ibkthv =  -1
 nvcimx =    6      icitv  =   -1      icithv =  -1      frcsub =   0
 nvbkmn =    2      nvcimn =    2      maxseg =   4      nrfitr =  30
 ncorel =    4      nvrfmx =    6      nvrfmn =   2      iden   =   1
 itran  =    0      froot  =    0      rtmode =   0      ncouple=   1
 skipso =    F      dalton2=    0      molcas =   0      finalv =   0
 finalw =    0      cosmocalc=   0    with_tsklst=   0
 nsegwx =    1     1     1     1
 nseg0x =    1     1     1     1
 nseg1x =    1     1     1     1
 nseg2x =    1     1     1     1
 nseg3x =    1     1     1     1
 nseg4x =    1     1     1     1
 no0ex  =      0    no1ex  =      0    no2ex  =     0    no3ex  =     0
 no4ex  =      0    nodiag =      0
 cdg4ex =    1      c3ex1ex=    1      c2ex0ex=   1
 fileloc=    0     0     0     0     0     0     0     1     1     1
 directhd=   1      noaqccshift_zyxw=      0
 critical_crit=-1.00000    critical_delta= 0.05000

 ctol   = 0.010000    lrtshift=1.000000    smalld =0.001000


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-03    1.000E-03
 Computing density:                    .drt1.state1
 using                      1  nodes and                      1  cores.
 szdg/szodg per processor=                  6144                 12288
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core           13107199 DP per process

********** Integral sort section *************


 workspace allocation information: lencor=  13107199

 echo of the input for program cisrt:
 ------------------------------------------------------------------------
  &input
  maxbl3=60000
  maxbl4=60000
  &end
 ------------------------------------------------------------------------
 
 ( 6) listing file:                    ciudgls             
 ( 5) input file:                      cisrtin   
 (17) cidrt file:                      cidrtfl             
 (11) transformed integrals file:      moints    
 (12) diagonal integral file:          diagint             
 (13) off-diagonal integral file:      ofdgint             
 (31) 4-external w integrals file:     fil4w               
 (32) 4-external x integrals file:     fil4x               
 (33) 3-external w integrals file:     fil3w               
 (34) 3-external x integrals file:     fil3x               
 (21) scratch da sorting file:         srtscr              
 (12) 2-e integral file [fsplit=2]:    moints2   

 input integral file header information:
                                                                                 
 aoints SIFS file created by argos.      zam792            17:53:52.086 17-Dec-13
  cidrt_title                                                                    
 MO-coefficients from mcscf.x                                                    
  with dummy occupation 1.0 for active orbitals                                  
  total ao core energy =    0.000000000                                          
 MCSCF energy =     -46.301878576                                                
 SIFS file created by program tran.      zam792            17:53:52.407 17-Dec-13

 input energy(*) values:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.

 total core energy =   0.000000000000E+00

 nsym = 4 nmot=  34

 symmetry  =    1    2    3    4
 slabel(*) =   a1   a2   b1   b2
 nmpsy(*)  =   14    4    8    8

 info(*) =          1      8192      6552      8192      5460         0

 orbital labels, i:molab(i)=
   1:tout:001   2:tout:002   3:tout:003   4:tout:004   5:tout:005   6:tout:006   7:tout:007   8:tout:008   9:tout:009  10:tout:010
  11:tout:011  12:tout:012  13:tout:013  14:tout:014  15:tout:015  16:tout:016  17:tout:017  18:tout:018  19:tout:019  20:tout:020
  21:tout:021  22:tout:022  23:tout:023  24:tout:024  25:tout:025  26:tout:026  27:tout:027  28:tout:028  29:tout:029  30:tout:030
  31:tout:031  32:tout:032  33:tout:033  34:tout:034

 input parameters:
 prnopt=  0
 ldamin=    4095 ldamax=   32767 ldainc=      64
 maxbuf=   30006 maxbl3=   60000 maxbl4=   60000 intmxo=    1536
  Using 32 bit compression 

 drt information:
  cidrt_title                                                                    
 nmotd =  38 nfctd =   4 nfvtc =   0 nmot  =  34
 nlevel =  34 niot  =   6 lowinl=  29
 orbital-to-level map(*)
   -1  -1  29  30  31   1   2   3   4   5   6   7   8   9  10  11  32  12  13  14
   -1  33  15  16  17  18  19  20  21  -1  34  22  23  24  25  26  27  28
 compressed map(*)
   29  30  31   1   2   3   4   5   6   7   8   9  10  11  32  12  13  14  33  15
   16  17  18  19  20  21  34  22  23  24  25  26  27  28
 levsym(*)
    1   1   1   1   1   1   1   1   1   1   1   2   2   2   3   3   3   3   3   3
    3   4   4   4   4   4   4   4   1   1   1   2   3   4
 repartitioning mu(*)=
   0.  0.  0.  0.  0.  0.

 new core energy added to the energy(*) list.
 from the integral file: h1_core= -4.349858765627E+01

 indxdg: diagonal integral statistics.
 total number of integrals contributing to diagonal matrix elements:      1190
 number with all external indices:       812
 number with half external - half internal indices:       336
 number with all internal indices:        42

 indxof: off-diagonal integral statistics.
    4-external integrals: num=      24411 strt=          1
    3-external integrals: num=      18720 strt=      24412
    2-external integrals: num=       6792 strt=      43132
    1-external integrals: num=       1272 strt=      49924
    0-external integrals: num=         99 strt=      51196

 total number of off-diagonal integrals:       51294


 indxof(2nd)  ittp=   3 numx(ittp)=        6792
 indxof(2nd)  ittp=   4 numx(ittp)=        1272
 indxof(2nd)  ittp=   5 numx(ittp)=          99

 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg=  12931980
 pro2e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652

 pro2e:     25126 integrals read in     5 records.

 pro2e:         0 integrals 34-ext integrals skipped.
 pro1e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652
 pro1e: eref =    0.000000000000000E+00
 total size of srtscr:                     4  records of                  32767 
 WP =               1048544 Bytes
 putdg        1     596    1191    1786    3322   36089   57934    3018   46706   90394
   123161  131353  136813  158652

 putf:       4 buffers of length    1536 written to file 12
 diagonal integral file completed.

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep4e:    17 blocks of linear combinations of 4-external integrals processed.
 number of sorted 4-external integrals      38732
 number of original 4-external integrals    24411


 putf34: external integral file complete. nfilw=    31 nfilx=    32 nrecw=     1 nrecx=     1 lbufp= 30006

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep3e:    19 blocks of linear combinations of 3-external integrals processed.
 number of sorted 3-external integrals      33390
 number of original 3-external integrals    18720


 putf34: external integral file complete. nfilw=    33 nfilx=    34 nrecw=     1 nrecx=     1 lbufp= 30006
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      6792         3     43132     43132      6792     51294
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      1272         4     49924     49924      1272     51294
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd        99         5     51196     51196        99     51294

 putf:       8 buffers of length    1536 written to file 13
 off-diagonal files sort completed.
 executing brd_struct for cisrtinfo
cisrtinfo:
bufszi  1536
 diagfile 4ext:     812 2ext:     336 0ext:      42
 fil4w,fil4x  :   24411 fil3w,fil3x :   18720
 ofdgint  2ext:    6792 1ext:    1272 0ext:      99so0ext:       0so1ext:       0so2ext:       0
buffer minbl4     381 minbl3     381 maxbl2     384nbas:  11   3   7   7   0   0   0   0 maxbuf 30006
 CIUDG version 5.9.7 ( 5-Oct-2004)

 workspace allocation information: lcore=  13107199

 core energy values from the integral file:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -4.349858765627E+01, ietype=    6,   fcore energy of type: H1(*)   

 total core repulsion energy = -4.349858765627E+01
 nmot  =    38 niot  =     6 nfct  =     4 nfvt  =     0
 nrow  =    23 nsym  =     4 ssym  =     3 lenbuf=  1600
 nwalk,xbar:         50       15       20       15        0
 nvalwt,nvalw:       39        4       20       15        0
 ncsft:            1558
 total number of valid internal walks:      39
 nvalz,nvaly,nvalx,nvalw =        4      20      15       0

 cisrt info file parameters:
 file number  12 blocksize   1536
 mxbld   1536
 nd4ext,nd2ext,nd0ext   812   336    42
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int    24411    18720     6792     1272       99        0        0        0
 minbl4,minbl3,maxbl2   381   381   384
 maxbuf 30006
 number of external orbitals per symmetry block:  11   3   7   7
 nmsym   4 number of internal orbitals   6
 executing brd_struct for drt
 executing brd_struct for orbinf
 executing brd_struct for momap
 calcthrxt: niot,maxw1=                     6                    11
 block size     0
 pthz,pthy,pthx,pthw:    15    20    15     0 total internal walks:      50
 maxlp3,n2lp,n1lp,n0lp    11     0     0     0
 orbsym(*)= 1 1 1 2 3 4

 setref:        4 references kept,
               11 references were marked as invalid, out of
               15 total.
 nmb.of records onel     1
 nmb.of records 2-ext     5
 nmb.of records 1-ext     1
 nmb.of records 0-ext     1
 nmb.of records 2-int     0
 nmb.of records 1-int     0
 nmb.of records 0-int     0
 ---------memory usage in DP -----------------
 < n-ex core usage >
     routines:
    fourex            61797
    threx             60079
    twoex              3549
    onex               1823
    allin              1536
    diagon             2109
               =======
   maximum            61797
 
  __ static summary __ 
   reflst                 4
   hrfspc                 4
               -------
   static->               4
 
  __ core required  __ 
   totstc                 4
   max n-ex           61797
               -------
   totnec->           61801
 
  __ core available __ 
   totspc          13107199
   totnec -           61801
               -------
   totvec->        13045398

 number of external paths / symmetry
 vertex x     100      82      98      98
 vertex w     128      82      98      98
segment: free space=    13045398
 reducing frespc by                   349 to               13045049 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1           8|         4|         0|         4|         0|         1|
 -------------------------------------------------------------------------------
  Y 2          20|       140|         4|        20|         4|         2|
 -------------------------------------------------------------------------------
  X 3          15|      1414|       144|        15|        24|         3|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=          16DP  conft+indsym=          80DP  drtbuffer=         253 DP

dimension of the ci-matrix ->>>      1558

 executing brd_struct for civct
 gentasklist: ntask=                    12
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS   
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      15       8       1414          4      15       4
     2  2   1    11      one-ext yz   1X  2 1      20       8        140          4      20       4
     3  3   2    15      1ex3ex yx    3X  3 2      15      20       1414        140      15      20
     4  1   1     1      allint zz    OX  1 1       8       8          4          4       4       4
     5  2   2     5      0ex2ex yy    OX  2 2      20      20        140        140      20      20
     6  3   3     6      0ex2ex xx*   OX  3 3      15      15       1414       1414      15      15
     7  3   3    18      0ex2ex xx+   OX  3 3      15      15       1414       1414      15      15
     8  2   2    42      four-ext y   4X  2 2      20      20        140        140      20      20
     9  3   3    43      four-ext x   4X  3 3      15      15       1414       1414      15      15
    10  1   1    75      dg-024ext z  OX  1 1       8       8          4          4       4       4
    11  2   2    76      dg-024ext y  OX  2 2      20      20        140        140      20      20
    12  3   3    77      dg-024ext x  OX  3 3      15      15       1414       1414      15      15
----------------------------------------------------------------------------------------------------
REDTASK #   1 TIME=  11.000 N=  1 (task/type/sgbra)=(   1/24/0) (
REDTASK #   2 TIME=  10.000 N=  1 (task/type/sgbra)=(   2/11/0) (
REDTASK #   3 TIME=   9.000 N=  1 (task/type/sgbra)=(   3/15/0) (
REDTASK #   4 TIME=   8.000 N=  1 (task/type/sgbra)=(   4/ 1/0) (
REDTASK #   5 TIME=   7.000 N=  1 (task/type/sgbra)=(   5/ 5/0) (
REDTASK #   6 TIME=   6.000 N=  1 (task/type/sgbra)=(   6/ 6/1) (
REDTASK #   7 TIME=   5.000 N=  1 (task/type/sgbra)=(   7/18/2) (
REDTASK #   8 TIME=   4.000 N=  1 (task/type/sgbra)=(   8/42/1) (
REDTASK #   9 TIME=   3.000 N=  1 (task/type/sgbra)=(   9/43/1) (
REDTASK #  10 TIME=   2.000 N=  1 (task/type/sgbra)=(  10/75/1) (
REDTASK #  11 TIME=   1.000 N=  1 (task/type/sgbra)=(  11/76/1) (
REDTASK #  12 TIME=   0.000 N=  1 (task/type/sgbra)=(  12/77/1) (
 initializing v-file: 1:                  1558

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:          40 2x:           0 4x:           0
All internal counts: zz :          15 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 reference space has dimension       4
 dsyevx: computed roots 1 to    2(converged:   2)

    root           eigenvalues
    ----           ------------
       1         -46.2453833562
       2         -46.2453833445

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt=                     4

         vector  1 from unit 11 written to unit 49 filename cirefv              

 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=     4)...
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              1558
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                    1
 residual norm convergence criteria:               0.001000

          starting bk iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         205 2x:          90 4x:          35
All internal counts: zz :          15 yy:           0 xx:           0 ww:           0
One-external counts: yz :         112 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:          24 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1
   ht   1    -2.74679570

          calcsovref: tciref block   1

              civs   1
 refs   1    1.00000    

          calcsovref: scrb block   1

                ci   1
 civs   1    1.00000    

          calcsovref: sovref block   1

              v      1
 ref    1    1.00000    

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.2453833562  3.1086E-15  5.4533E-02  2.1660E-01  1.0000E-03
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.2453833562  3.1086E-15  5.4533E-02  2.1660E-01  1.0000E-03
 
diagon:itrnv=   2
 expansion vectors are not transformed.
 matrix-vector products are not transformed.

    2 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              1558
 number of initial trial vectors:                         2
 number of initial matrix-vector products:                1
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                   30
 residual norm convergence criteria:               0.001000

          starting ci iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         205 2x:          90 4x:          35
All internal counts: zz :          15 yy:          72 xx:          66 ww:           0
One-external counts: yz :         112 yx:         360 yw:           0
Two-external counts: yy :         150 ww:           0 xx:         180 xz:          24 wz:           0 wx:           0
Three-ext.   counts: yx :         151 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2
   ht   1    -2.74679570
   ht   2    -0.05453269    -0.24562244

          calcsovref: tciref block   1

              civs   1       civs   2
 refs   1    1.00000      -2.363593E-13

          calcsovref: scrb block   1

                ci   1         ci   2
 civs   1   0.964785      -0.263040    
 civs   2   0.786062        2.88314    

          calcsovref: sovref block   1

              v      1       v      2
 ref    1   0.964785      -0.263040    

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.96478492    -0.26304005

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.2898140783  4.4431E-02  1.2553E-03  2.8562E-02  1.0000E-03
 mr-sdci #  1  2    -45.6476594446  2.1491E+00  0.0000E+00  5.1940E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   2

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         205 2x:          90 4x:          35
All internal counts: zz :          15 yy:          72 xx:          66 ww:           0
One-external counts: yz :         112 yx:         360 yw:           0
Two-external counts: yy :         150 ww:           0 xx:         180 xz:          24 wz:           0 wx:           0
Three-ext.   counts: yx :         151 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3
   ht   1    -2.74679570
   ht   2    -0.05453269    -0.24562244
   ht   3    -0.00069296     0.01057110    -0.00645134

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3
 refs   1    1.00000      -2.363593E-13   1.830602E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3
 civs   1   0.964878       3.420706E-02   0.260487    
 civs   2   0.806956      -0.326559       -2.95665    
 civs   3    1.10069        18.5079       -6.78145    

          calcsovref: sovref block   1

              v      1       v      2       v      3
 ref    1   0.965079       3.759512E-02   0.259246    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.96507935     0.03759512     0.25924592

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1    -46.2911980096  1.3839E-03  1.2451E-04  9.5671E-03  1.0000E-03
 mr-sdci #  2  2    -45.8653045767  2.1765E-01  0.0000E+00  3.3769E-01  1.0000E-04
 mr-sdci #  2  3    -45.6184745549  2.1199E+00  0.0000E+00  5.3434E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000999
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   3

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         205 2x:          90 4x:          35
All internal counts: zz :          15 yy:          72 xx:          66 ww:           0
One-external counts: yz :         112 yx:         360 yw:           0
Two-external counts: yy :         150 ww:           0 xx:         180 xz:          24 wz:           0 wx:           0
Three-ext.   counts: yx :         151 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4
   ht   1    -2.74679570
   ht   2    -0.05453269    -0.24562244
   ht   3    -0.00069296     0.01057110    -0.00645134
   ht   4     0.00083025    -0.00339220    -0.00023600    -0.00075463

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4
 refs   1    1.00000      -2.363593E-13   1.830602E-04  -2.822987E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4
 civs   1   0.963398      -8.640356E-02   0.136777       0.214452    
 civs   2   0.809178       0.349011       -1.24218       -2.77037    
 civs   3    1.22982        10.7714        13.2050       -10.1011    
 civs   4    1.31250        44.6004       -22.0552        30.7482    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4
 ref    1   0.963252      -9.702238E-02   0.145421       0.203923    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.96325226    -0.09702238     0.14542081     0.20392286

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1    -46.2913614786  1.6347E-04  3.7279E-05  4.4912E-03  1.0000E-03
 mr-sdci #  3  2    -46.0533124978  1.8801E-01  0.0000E+00  2.4040E-01  1.0000E-04
 mr-sdci #  3  3    -45.8051487912  1.8667E-01  0.0000E+00  4.0413E-01  1.0000E-04
 mr-sdci #  3  4    -45.4868497643  1.9883E+00  0.0000E+00  5.4748E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   4

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         205 2x:          90 4x:          35
All internal counts: zz :          15 yy:          72 xx:          66 ww:           0
One-external counts: yz :         112 yx:         360 yw:           0
Two-external counts: yy :         150 ww:           0 xx:         180 xz:          24 wz:           0 wx:           0
Three-ext.   counts: yx :         151 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1    -2.74679570
   ht   2    -0.05453269    -0.24562244
   ht   3    -0.00069296     0.01057110    -0.00645134
   ht   4     0.00083025    -0.00339220    -0.00023600    -0.00075463
   ht   5    -0.00091518    -0.00082474    -0.00027852     0.00027906    -0.00070527

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1    1.00000      -2.363593E-13   1.830602E-04  -2.822987E-04   3.379219E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 civs   1   0.962776      -0.108355       1.155866E-02  -0.166798      -0.184032    
 civs   2   0.809444       0.338950      -0.129595        1.48755        2.71162    
 civs   3    1.24749        9.98217       -4.40457       -12.2392        11.7461    
 civs   4    1.49228        38.8568       -25.0768        21.2760       -39.2308    
 civs   5   0.601142        40.8020        43.9752        13.7772       -25.4326    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1   0.962787      -0.103709       3.269170E-02  -0.170390      -0.179402    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     0.96278666    -0.10370921     0.03269170    -0.17038952    -0.17940155

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1    -46.2913838897  2.2411E-05  3.6371E-06  1.8151E-03  1.0000E-03
 mr-sdci #  4  2    -46.0745203042  2.1208E-02  0.0000E+00  2.0780E-01  1.0000E-04
 mr-sdci #  4  3    -46.0281311477  2.2298E-01  0.0000E+00  4.3199E-01  1.0000E-04
 mr-sdci #  4  4    -45.7908527001  3.0400E-01  0.0000E+00  3.9717E-01  1.0000E-04
 mr-sdci #  4  5    -45.3948468448  1.8963E+00  0.0000E+00  4.9952E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   5

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         205 2x:          90 4x:          35
All internal counts: zz :          15 yy:          72 xx:          66 ww:           0
One-external counts: yz :         112 yx:         360 yw:           0
Two-external counts: yy :         150 ww:           0 xx:         180 xz:          24 wz:           0 wx:           0
Three-ext.   counts: yx :         151 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1    -2.74679570
   ht   2    -0.05453269    -0.24562244
   ht   3    -0.00069296     0.01057110    -0.00645134
   ht   4     0.00083025    -0.00339220    -0.00023600    -0.00075463
   ht   5    -0.00091518    -0.00082474    -0.00027852     0.00027906    -0.00070527
   ht   6    -0.00004875    -0.00006047     0.00000748    -0.00008486     0.00009228    -0.00002933

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1    1.00000      -2.363593E-13   1.830602E-04  -2.822987E-04   3.379219E-04   1.809897E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1   0.962600       0.101609       5.062299E-02  -0.131958       0.144504      -0.153243    
 civs   2   0.809464      -0.284432      -0.127032        1.04564       -1.89373        2.21923    
 civs   3    1.25044       -8.26693       -3.74185       -14.4672       -4.14917        9.91401    
 civs   4    1.52284       -40.5576       -5.57183        8.67550       -17.6698       -51.6909    
 civs   5   0.703333       -22.9716       -50.9756        32.3175        43.4576       -1.03609    
 civs   6    1.04771       -119.243        62.1061        117.056        292.457        193.853    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.962656       0.101624       3.540921E-02  -0.124016       0.168711      -0.133678    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.96265552     0.10162383     0.03540921    -0.12401564     0.16871120    -0.13367793

 trial vector basis is being transformed.  new dimension:   2

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1    -46.2913877003  3.8107E-06  2.3749E-07  3.9816E-04  1.0000E-03
 mr-sdci #  5  2    -46.1032387475  2.8718E-02  0.0000E+00  1.4344E-01  1.0000E-04
 mr-sdci #  5  3    -46.0553413796  2.7210E-02  0.0000E+00  4.2176E-01  1.0000E-04
 mr-sdci #  5  4    -45.8159069178  2.5054E-02  0.0000E+00  3.4270E-01  1.0000E-04
 mr-sdci #  5  5    -45.6293168595  2.3447E-01  0.0000E+00  5.0745E-01  1.0000E-04
 mr-sdci #  5  6    -45.3060584512  1.8075E+00  0.0000E+00  4.9436E-01  1.0000E-04
 
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mr-sdci  convergence criteria satisfied after  5 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1    -46.2913877003  3.8107E-06  2.3749E-07  3.9816E-04  1.0000E-03
 mr-sdci #  5  2    -46.1032387475  2.8718E-02  0.0000E+00  1.4344E-01  1.0000E-04

####################CIUDGINFO####################

   ci vector at position   1 energy=  -46.291387700345

################END OF CIUDGINFO################

 
diagon:itrnv=   0
    1 of the   3 expansion vectors are transformed.
    1 of the   2 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
maximum overlap with reference    1(overlap= 0.96266)

 information on vector: 1 from unit 11 written to unit 48 filename civout              


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -46.2913877003

                                                       internal orbitals

                                          level       1    2    3    4    5    6

                                          orbital     3    4    5   17   22   31

                                         symmetry   a1   a1   a1   a2   b1   b2 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  5  1       1  0.733175                        +    +    +         +       
 z*  5  1       2 -0.591968                        +    +         +         +  
 z*  5  1       3 -0.196808                        +         +    +         +  
 y   5  1       6 -0.082930              2( b1 )   +    +    +                 
 y   5  1       8  0.059811              4( b1 )   +    +    +                 
 y   5  1      13  0.051620              2( b2 )   +    +         +            
 y   5  1      15 -0.041679              4( b2 )   +    +         +            
 y   5  1      20  0.047947              2( a1 )   +    +              +       
 y   5  1      21  0.039732              3( a1 )   +    +              +       
 y   5  1      24  0.035840              6( a1 )   +    +              +       
 y   5  1      25 -0.036161              7( a1 )   +    +              +       
 y   5  1      30 -0.067305              1( a2 )   +    +                   +  
 y   5  1      31  0.048397              2( a2 )   +    +                   +  
 y   5  1      36 -0.010109              4( b2 )   +         +    +            
 y   5  1      41  0.055926              2( a1 )   +         +         +       
 y   5  1      42 -0.062647              3( a1 )   +         +         +       
 y   5  1      45  0.043820              6( a1 )   +         +         +       
 y   5  1      46  0.041329              7( a1 )   +         +         +       
 y   5  1      51 -0.020037              1( a2 )   +         +              +  
 y   5  1      52  0.015093              2( a2 )   +         +              +  
 y   5  1      58 -0.013218              2( a1 )   +              +         +  
 y   5  1      59  0.072251              3( a1 )   +              +         +  
 y   5  1      62 -0.018128              6( a1 )   +              +         +  
 y   5  1      63 -0.048653              7( a1 )   +              +         +  
 y   5  1      69  0.010355              2( b2 )   +                   +    +  
 x   5  1     154  0.010065   10( a1 )   1( b1 )   +    +                      
 x   5  1     176  0.010557   10( a1 )   3( b1 )   +    +                      
 x   5  1     183  0.011261    6( a1 )   4( b1 )   +    +                      
 x   5  1     184 -0.011292    7( a1 )   4( b1 )   +    +                      
 x   5  1     208  0.012319    9( a1 )   6( b1 )   +    +                      
 x   5  1     220  0.015743   10( a1 )   7( b1 )   +    +                      
 x   5  1     232  0.013083    2( a2 )   4( b2 )   +    +                      
 x   5  1     242 -0.016346    3( a2 )   7( b2 )   +    +                      
 x   5  1     243  0.039957    1( a1 )   1( b1 )   +         +                 
 x   5  1     246  0.030033    4( a1 )   1( b1 )   +         +                 
 x   5  1     256  0.010504    3( a1 )   2( b1 )   +         +                 
 x   5  1     265  0.030232    1( a1 )   3( b1 )   +         +                 
 x   5  1     268  0.023865    4( a1 )   3( b1 )   +         +                 
 x   5  1     281  0.013354    6( a1 )   4( b1 )   +         +                 
 x   5  1     282  0.012745    7( a1 )   4( b1 )   +         +                 
 x   5  1     306  0.024750    9( a1 )   6( b1 )   +         +                 
 x   5  1     307  0.013381   10( a1 )   6( b1 )   +         +                 
 x   5  1     401 -0.012444    7( a1 )   4( b2 )   +              +            
 x   5  1     437 -0.010382   10( a1 )   7( b2 )   +              +            
 x   5  1     441  0.011788    2( a1 )   3( a1 )   +                   +       
 x   5  1     451 -0.010997    3( a1 )   6( a1 )   +                   +       
 x   5  1     455 -0.011032    2( a1 )   7( a1 )   +                   +       
 x   5  1     459 -0.016043    6( a1 )   7( a1 )   +                   +       
 x   5  1     483 -0.011649    9( a1 )  10( a1 )   +                   +       
 x   5  1     517  0.012096    6( b1 )   7( b1 )   +                   +       
 x   5  1     538  0.012337    6( b2 )   7( b2 )   +                   +       
 x   5  1     541  0.011312    3( a1 )   1( a2 )   +                        +  
 x   5  1     552 -0.010615    3( a1 )   2( a2 )   +                        +  
 x   5  1     556  0.014685    7( a1 )   2( a2 )   +                        +  
 x   5  1     572 -0.037760    1( b1 )   1( b2 )   +                        +  
 x   5  1     574 -0.028643    3( b1 )   1( b2 )   +                        +  
 x   5  1     586 -0.028390    1( b1 )   3( b2 )   +                        +  
 x   5  1     588 -0.022585    3( b1 )   3( b2 )   +                        +  
 x   5  1     620 -0.023248    7( b1 )   7( b2 )   +                        +  
 x   5  1     629  0.012209    9( a1 )   1( b1 )        +    +                 
 x   5  1     636 -0.017313    5( a1 )   2( b1 )        +    +                 
 x   5  1     651  0.011261    9( a1 )   3( b1 )        +    +                 
 x   5  1     658  0.012607    5( a1 )   4( b1 )        +    +                 
 x   5  1     676  0.014992    1( a1 )   6( b1 )        +    +                 
 x   5  1     679  0.013815    4( a1 )   6( b1 )        +    +                 
 x   5  1     700 -0.011555    3( a2 )   1( b2 )        +    +                 
 x   5  1     706 -0.010632    3( a2 )   3( b2 )        +    +                 
 x   5  1     749  0.010614   10( a1 )   1( b2 )        +         +            
 x   5  1     755  0.013079    5( a1 )   2( b2 )        +         +            
 x   5  1     777 -0.010539    5( a1 )   4( b2 )        +         +            
 x   5  1     795 -0.012443    1( a1 )   6( b2 )        +         +            
 x   5  1     798 -0.011323    4( a1 )   6( b2 )        +         +            
 x   5  1     824 -0.012721    2( a1 )   5( a1 )        +              +       
 x   5  1     906  0.011539    1( b2 )   6( b2 )        +              +       
 x   5  1     908  0.010461    3( b2 )   6( b2 )        +              +       
 x   5  1     911  0.011338    1( b2 )   7( b2 )        +              +       
 x   5  1     913  0.010361    3( b2 )   7( b2 )        +              +       
 x   5  1     921 -0.013976    5( a1 )   1( a2 )        +                   +  
 x   5  1     932  0.010138    5( a1 )   2( a2 )        +                   +  
 x   5  1     956  0.012025    7( b1 )   1( b2 )        +                   +  
 x   5  1     970  0.011068    7( b1 )   3( b2 )        +                   +  
 x   5  1     992  0.011365    1( b1 )   7( b2 )        +                   +  
 x   5  1     994  0.010499    3( b1 )   7( b2 )        +                   +  
 x   5  1    1104 -0.010810    2( a1 )   5( a1 )             +         +       
 x   5  1    1105  0.013588    3( a1 )   5( a1 )             +         +       
 x   5  1    1125  0.012912    1( a1 )   9( a1 )             +         +       
 x   5  1    1128  0.011876    4( a1 )   9( a1 )             +         +       
 x   5  1    1165  0.012231    1( b1 )   6( b1 )             +         +       
 x   5  1    1167  0.011274    3( b1 )   6( b1 )             +         +       
 x   5  1    1191 -0.010028    1( b2 )   7( b2 )             +         +       
 x   5  1    1369 -0.014223    3( a1 )   5( a1 )                  +         +  
 x   5  1    1434  0.010972    1( b1 )   7( b1 )                  +         +  
 x   5  1    1436  0.010095    3( b1 )   7( b1 )                  +         +  
 x   5  1    1455  0.010914    1( b2 )   7( b2 )                  +         +  
 x   5  1    1457  0.010104    3( b2 )   7( b2 )                  +         +  

 ci coefficient statistics:
           rq > 0.1                3
      0.1> rq > 0.01              92
     0.01> rq > 0.001            311
    0.001> rq > 0.0001           250
   0.0001> rq > 0.00001           76
  0.00001> rq > 0.000001          23
 0.000001> rq                    803
           all                  1558
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:          40 2x:           0 4x:           0
All internal counts: zz :          15 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.733175333938    -33.905960394895
     2     2     -0.591968407396     27.375814350869
     3     3     -0.196808420419      9.101507535112
     4     4     -0.000021295390      0.000980535481

 number of reference csfs (nref) is     4.  root number (iroot) is  1.
 c0**2 =   0.92670622  c**2 (all zwalks) =   0.92670622

 pople ci energy extrapolation is computed with  4 correlated electrons.

 eref      =    -46.245383330736   "relaxed" cnot**2         =   0.926706220452
 eci       =    -46.291387700345   deltae = eci - eref       =  -0.046004369609
 eci+dv1   =    -46.294759534469   dv1 = (1-cnot**2)*deltae  =  -0.003371834124
 eci+dv2   =    -46.295026214957   dv2 = dv1 / cnot**2       =  -0.003638514612
 eci+dv3   =    -46.295338702185   dv3 = dv1 / (2*cnot**2-1) =  -0.003951001840
 eci+pople =    -46.293204121502   (  4e- scaled deltae )    =  -0.047820790766
 passed aftci ... 
 readint2: molcas,dalton2=                     0                     0
 files%faoints=aoints              
lodens (list->root)=  1
sifcfg setup: record length 4096 DP
# d1 elements per record  3272
# d2 elements per record  2730
  The MR-CISD density will be calculated.
 item #                     1 suffix=:.drt1.state1:
 read_civout: repnuc=  -43.4985876562748     
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method:  0 last record  1max overlap with ref# 96% root-following 0
 MR-CISD energy:   -46.29138770    -2.79280004
 residuum:     0.00039816
 deltae:     0.00000381
 apxde:     0.00000024

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.96265552     0.10162383     0.03540921    -0.12401564     0.16871120    -0.13367793     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.96265552     0.10162383     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 computing final density
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=      16  DYX=      60  DYW=       0
   D0Z=       3  D0Y=      18  D0X=      12  D0W=       0
  DDZI=      16 DDYI=      60 DDXI=      30 DDWI=       0
  DDZE=       0 DDYE=      20 DDXE=      15 DDWE=       0
================================================================================
Trace of MO density:     4.000000
    4  correlated and     8  frozen core electrons

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.99110210    -0.00020992    -0.00008500     0.00000000    -0.00085013     0.00252890
   MO   4     0.00000000     0.00000000    -0.00020992     0.92433041     0.12016079     0.00000000    -0.04603464     0.08383065
   MO   5     0.00000000     0.00000000    -0.00008500     0.12016079     0.60825314     0.00000000     0.03011310     0.04057922
   MO   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00388541     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000    -0.00085013    -0.04603464     0.03011310     0.00000000     0.00652019    -0.00279886
   MO   8     0.00000000     0.00000000     0.00252890     0.08383065     0.04057922     0.00000000    -0.00279886     0.01221575
   MO   9     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00315141     0.00000000     0.00000000
   MO  10     0.00000000     0.00000000     0.00204492     0.00004750     0.00000861     0.00000000    -0.00000458     0.00001238
   MO  11     0.00000000     0.00000000    -0.00113234    -0.03911898     0.02023707     0.00000000     0.00539011    -0.00304106
   MO  12     0.00000000     0.00000000    -0.00239320    -0.05408935    -0.03260124     0.00000000     0.00133997    -0.00902914
   MO  13     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00104048     0.00000000     0.00000000
   MO  14     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00003855     0.00000000     0.00000000
   MO  15     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00019202     0.00000000     0.00000000
   MO  16     0.00000000     0.00000000    -0.00050893     0.00000571     0.00000150     0.00000000     0.00000010    -0.00000035

                MO   9         MO  10         MO  11         MO  12         MO  13         MO  14         MO  15         MO  16
   MO   3     0.00000000     0.00204492    -0.00113234    -0.00239320     0.00000000     0.00000000     0.00000000    -0.00050893
   MO   4     0.00000000     0.00004750    -0.03911898    -0.05408935     0.00000000     0.00000000     0.00000000     0.00000571
   MO   5     0.00000000     0.00000861     0.02023707    -0.03260124     0.00000000     0.00000000     0.00000000     0.00000150
   MO   6     0.00315141     0.00000000     0.00000000     0.00000000     0.00104048     0.00003855     0.00019202     0.00000000
   MO   7     0.00000000    -0.00000458     0.00539011     0.00133997     0.00000000     0.00000000     0.00000000     0.00000010
   MO   8     0.00000000     0.00001238    -0.00304106    -0.00902914     0.00000000     0.00000000     0.00000000    -0.00000035
   MO   9     0.00265914     0.00000000     0.00000000     0.00000000     0.00094080    -0.00002597     0.00014327     0.00000000
   MO  10     0.00000000     0.00249543    -0.00000935    -0.00001856     0.00000000     0.00000000     0.00000000    -0.00012327
   MO  11     0.00000000    -0.00000935     0.00468886     0.00170379     0.00000000     0.00000000     0.00000000     0.00000041
   MO  12     0.00000000    -0.00001856     0.00170379     0.00701604     0.00000000     0.00000000     0.00000000     0.00000086
   MO  13     0.00094080     0.00000000     0.00000000     0.00000000     0.00037480    -0.00005136     0.00003673     0.00000000
   MO  14    -0.00002597     0.00000000     0.00000000     0.00000000    -0.00005136     0.00228178     0.00047325     0.00000000
   MO  15     0.00014327     0.00000000     0.00000000     0.00000000     0.00003673     0.00047325     0.00195569     0.00000000
   MO  16     0.00000000    -0.00012327     0.00000041     0.00000086     0.00000000     0.00000000     0.00000000     0.00000766

Natural orbital populations,block 1
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     2.00000000     0.99112590     0.97987316     0.57341967     0.00680113     0.00609304     0.00347284
              MO     9       MO    10       MO    11       MO    12       MO    13       MO    14       MO    15       MO    16
  occ(*)=     0.00261635     0.00249719     0.00161116     0.00012583     0.00008138     0.00006501     0.00000234     0.00000139

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4
   MO   1     0.40546908     0.04131912    -0.02887872     0.00000000
   MO   2     0.04131912     0.00560034    -0.00424018     0.00000000
   MO   3    -0.02887872    -0.00424018     0.00332825     0.00000000
   MO   4     0.00000000     0.00000000     0.00000000     0.00142650

Natural orbital populations,block 2
              MO     1       MO     2       MO     3       MO     4       MO
  occ(*)=     0.41177587     0.00258990     0.00142650     0.00003191

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     0.55976319     0.00000000     0.05740446     0.00000000    -0.04007511     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.00631596     0.00000000     0.00509280     0.00000000     0.00166903     0.00038703
   MO   4     0.00000000     0.05740446     0.00000000     0.00780188     0.00000000    -0.00589993     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.00509280     0.00000000     0.00421905     0.00000000     0.00145600     0.00034369
   MO   6     0.00000000    -0.04007511     0.00000000    -0.00589993     0.00000000     0.00462097     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000     0.00166903     0.00000000     0.00145600     0.00000000     0.00055518     0.00013451
   MO   8     0.00000000     0.00000000     0.00038703     0.00000000     0.00034369     0.00000000     0.00013451     0.00231931
   MO   9     0.00000000     0.00000000    -0.00067929     0.00000000    -0.00062929     0.00000000    -0.00026476    -0.00062633

                MO   9
   MO   2     0.00000000
   MO   3    -0.00067929
   MO   4     0.00000000
   MO   5    -0.00062929
   MO   6     0.00000000
   MO   7    -0.00026476
   MO   8    -0.00062633
   MO   9     0.00229511

Natural orbital populations,block 3
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     0.56857409     0.01108527     0.00357057     0.00280147     0.00167045     0.00014470     0.00004138
              MO     9       MO
  occ(*)=     0.00000272

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     0.41271661     0.00000000     0.02973875     0.00000000    -0.02378827     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.00355182     0.00000000     0.00288728     0.00000000    -0.00095547     0.00009409
   MO   4     0.00000000     0.02973875     0.00000000     0.00342298     0.00000000    -0.00290422     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.00288728     0.00000000     0.00244868     0.00000000    -0.00087168     0.00007796
   MO   6     0.00000000    -0.02378827     0.00000000    -0.00290422     0.00000000     0.00258140     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000    -0.00095547     0.00000000    -0.00087168     0.00000000     0.00035074    -0.00002647
   MO   8     0.00000000     0.00000000     0.00009409     0.00000000     0.00007796     0.00000000    -0.00002647     0.00116701
   MO   9     0.00000000     0.00000000     0.00006583     0.00000000     0.00016417     0.00000000    -0.00013731    -0.00027062

                MO   9
   MO   2     0.00000000
   MO   3     0.00006583
   MO   4     0.00000000
   MO   5     0.00016417
   MO   6     0.00000000
   MO   7    -0.00013731
   MO   8    -0.00027062
   MO   9     0.00225955

Natural orbital populations,block 4
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     0.41625109     0.00623471     0.00241535     0.00232473     0.00109935     0.00011675     0.00005456
              MO     9       MO
  occ(*)=     0.00000225


 total number of electrons =   12.0000000000

 test slabel:                     4
  a1  a2  b1  b2 


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        a1  partial gross atomic populations
   ao class       1a1        2a1        3a1        4a1        5a1        6a1 
     3_ s       2.000000   0.000000   0.990675   0.000432   0.000000   0.000000
     3_ p       0.000000   2.000000   0.000000   0.000000   0.000000   0.006784
     3_ d       0.000000   0.000000   0.000451   0.979441   0.573420   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000000   0.000000   0.000017
 
   ao class       7a1        8a1        9a1       10a1       11a1       12a1 
     3_ s       0.000000   0.000000   0.000000   0.002497   0.000000   0.000000
     3_ p       0.000000   0.000000   0.000005   0.000000   0.000003   0.000126
     3_ d       0.006093   0.003473   0.000000   0.000000   0.000000   0.000000
     3_ f       0.000000   0.000000   0.002611   0.000000   0.001608   0.000000
 
   ao class      13a1       14a1       15a1       16a1 
     3_ s       0.000000   0.000000   0.000000   0.000001
     3_ p       0.000000   0.000000   0.000002   0.000000
     3_ d       0.000081   0.000065   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000000

                        a2  partial gross atomic populations
   ao class       1a2        2a2        3a2        4a2 
     3_ d       0.411776   0.002590   0.000000   0.000032
     3_ f       0.000000   0.000000   0.001427   0.000000

                        b1  partial gross atomic populations
   ao class       1b1        2b1        3b1        4b1        5b1        6b1 
     3_ p       2.000000   0.000000   0.010892   0.000000   0.000048   0.000004
     3_ d       0.000000   0.568574   0.000000   0.003571   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000194   0.000000   0.002754   0.001667
 
   ao class       7b1        8b1        9b1 
     3_ p       0.000144   0.000000   0.000003
     3_ d       0.000000   0.000041   0.000000
     3_ f       0.000000   0.000000   0.000000

                        b2  partial gross atomic populations
   ao class       1b2        2b2        3b2        4b2        5b2        6b2 
     3_ p       2.000000   0.000000   0.006219   0.000000   0.000011   0.000002
     3_ d       0.000000   0.416251   0.000000   0.002415   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000015   0.000000   0.002314   0.001097
 
   ao class       7b2        8b2        9b2 
     3_ p       0.000116   0.000000   0.000002
     3_ d       0.000000   0.000055   0.000000
     3_ f       0.000000   0.000000   0.000000


                        gross atomic populations
     ao            3_
      s         2.993605
      p         6.024361
      d         2.968329
      f         0.013705
    total      12.000000
 

 Total number of electrons:   12.00000000

 DA ...
