1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      2009-03.    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


================ Computing sorting integral file structure ================

                    -----z----- -----y----- -----x----- -----w----- ---total---

                CSFs        15         560        5670           0        6245
      internal walks        15          20          15           0          50
valid internal walks        15          20          15           0          50
 getinfoarray: info=                     6 :                     1
                  8192                  6552                  8192
                  5460                     0
 icd(3)=                  1191 ci%nnlev=                   595  l2rec=
                  8192  n2max=                  5460
 lcore1,lcore2=              13069922              13046606
 lencor,maxblo              13107200                 60000
========================================
 current settings:
 minbl3        1215
 minbl4        1215
 locmaxbl3    22684
 locmaxbuf    11342
 maxbl3       60000
 maxbl3       60000
 maxbl4       60000
 maxbuf       30006
========================================

 sorted 4-external integrals:     3 records of integral w-combinations 
                                  3 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)

 sorted 3-external integrals:     3 records of integral w-combinations 
                                  3 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)
 Orig.  diagonal integrals:  1electron:        34
                             0ext.    :        42
                             2ext.    :       336
                             4ext.    :       812


 Orig. off-diag. integrals:  4ext.    :     93177
                             3ext.    :     72576
                             2ext.    :     25578
                             1ext.    :      4704
                             0ext.    :       315
                             2ext. SO :         0
                             1ext. SO :         0
                             0ext. SO :         0
                             1electron:       406


 Sorted integrals            3ext.  w :     68040 x :     63504
                             4ext.  w :     82215 x :     71253


Cycle #  1 sortfile size=    393204(      12 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13069922
Cycle #  2 sortfile size=    393204(      12 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13069922
 minimum size of srtscr:    327670 WP (    10 records)
 maximum size of srtscr:    393204 WP (    12 records)
diagi   file:      4 records  of   1536 WP each=>       6144 WP total
ofdgi   file:     23 records  of   1536 WP each=>      35328 WP total
fil3w   file:      3 records  of  30006 WP each=>      90018 WP total
fil3x   file:      3 records  of  30006 WP each=>      90018 WP total
fil4w   file:      3 records  of  30006 WP each=>      90018 WP total
fil4x   file:      3 records  of  30006 WP each=>      90018 WP total
 compressed index vector length=                     1
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 3,
  GSET = 3,
   DAVCOR =10,
  NCOREL = 4
   nroot=1
  IVMODE = 3
  NBKITR = 1
  RTOLBK = 1e-4,1e-4,1e-4,1e-4,1e-4,1e-4,1e-4,1e-4,1e-4,
  NITER = 200
   rtolci=1e-4
  NVCIMX = 25
  NVRFMX = 25
  NVBKMX = 25
  IDEN  = 1
  CSFPRN = 10,
  update_mode=1
  nvcimn=2
  nvrfmn=2
  nvbkmn=2
   lrtshift=-0.0524795077
 /&end
 ------------------------------------------------------------------------
lodens (list->root)=  1
invlodens (root->list)=  1
 LRT calculation: diagonal shift= -5.247950770000000E-002
 bummer (warning):2:changed keyword: nbkitr=                      0
 USING SEGMENTS OF EQUAL SIZE

****************  list of control variables  ****************
 lvlprt =    0      nroot  =    1      noldv  =   0      noldhv =   0
 nunitv =    1      nbkitr =    0      niter  = 200      davcor =  10
 csfprn =   10      ivmode =    3      istrt  =   0      vout   =   0
 iortls =    0      nvbkmx =   25      ibktv  =  -1      ibkthv =  -1
 nvcimx =   25      icitv  =   -1      icithv =  -1      frcsub =   0
 nvbkmn =    2      nvcimn =    2      maxseg =   4      nrfitr =  30
 ncorel =    4      nvrfmx =   25      nvrfmn =   2      iden   =   1
 itran  =    0      froot  =    0      rtmode =   0      ncouple=   1
 skipso =    F      dalton2=    0      molcas =   0      finalv =   0
 finalw =    0      cosmocalc=   0    with_tsklst=   0
 nsegwx =    1     1     1     1
 nseg0x =    1     1     1     1
 nseg1x =    1     1     1     1
 nseg2x =    1     1     1     1
 nseg3x =    1     1     1     1
 nseg4x =    1     1     1     1
 no0ex  =      0    no1ex  =      0    no2ex  =     0    no3ex  =     0
 no4ex  =      0    nodiag =      0
 cdg4ex =    1      c3ex1ex=    1      c2ex0ex=   1
 fileloc=    0     0     0     0     0     0     0     1     1     1
 directhd=   1      noaqccshift_zyxw=      0
 critical_crit=-1.00000    critical_delta= 0.05000

 ctol   = 0.010000    lrtshift=-.052480    smalld =0.001000


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-04    1.000E-04
 Computing density:                    .drt1.state1
 using                      1  nodes and                      1  cores.
 szdg/szodg per processor=                  6144                 35328
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core           13107199 DP per process

********** Integral sort section *************


 workspace allocation information: lencor=  13107199

 echo of the input for program cisrt:
 ------------------------------------------------------------------------
  &input
  maxbl3=60000
  maxbl4=60000
  &end
 ------------------------------------------------------------------------
 
 ( 6) listing file:                    ciudgls             
 ( 5) input file:                      cisrtin   
 (17) cidrt file:                      cidrtfl             
 (11) transformed integrals file:      moints    
 (12) diagonal integral file:          diagint             
 (13) off-diagonal integral file:      ofdgint             
 (31) 4-external w integrals file:     fil4w               
 (32) 4-external x integrals file:     fil4x               
 (33) 3-external w integrals file:     fil3w               
 (34) 3-external x integrals file:     fil3x               
 (21) scratch da sorting file:         srtscr              
 (12) 2-e integral file [fsplit=2]:    moints2   

 input integral file header information:
                                                                                 
 aoints SIFS file created by argos.      zam792            16:59:01.676 17-Dec-13
  cidrt_title                                                                    
 MO-coefficients from mcscf.x                                                    
  with dummy occupation 1.0 for active orbitals                                  
  total ao core energy =    0.000000000                                          
 MCSCF energy =     -46.297255533                                                
 SIFS file created by program tran.      zam792            16:59:02.163 17-Dec-13

 input energy(*) values:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.

 total core energy =   0.000000000000E+00

 nsym = 1 nmot=  34

 symmetry  =    1
 slabel(*) =    a
 nmpsy(*)  =   34

 info(*) =          1      8192      6552      8192      5460         0

 orbital labels, i:molab(i)=
   1:tout:001   2:tout:002   3:tout:003   4:tout:004   5:tout:005   6:tout:006   7:tout:007   8:tout:008   9:tout:009  10:tout:010
  11:tout:011  12:tout:012  13:tout:013  14:tout:014  15:tout:015  16:tout:016  17:tout:017  18:tout:018  19:tout:019  20:tout:020
  21:tout:021  22:tout:022  23:tout:023  24:tout:024  25:tout:025  26:tout:026  27:tout:027  28:tout:028  29:tout:029  30:tout:030
  31:tout:031  32:tout:032  33:tout:033  34:tout:034

 input parameters:
 prnopt=  0
 ldamin=    4095 ldamax=   32767 ldainc=      64
 maxbuf=   30006 maxbl3=   60000 maxbl4=   60000 intmxo=    1536
  Using 32 bit compression 

 drt information:
  cidrt_title                                                                    
 nmotd =  38 nfctd =   4 nfvtc =   0 nmot  =  34
 nlevel =  34 niot  =   6 lowinl=  29
 orbital-to-level map(*)
   -1  -1  -1  -1  29  30  31  32  33  34   1   2   3   4   5   6   7   8   9  10
   11  12  13  14  15  16  17  18  19  20  21  22  23  24  25  26  27  28
 compressed map(*)
   29  30  31  32  33  34   1   2   3   4   5   6   7   8   9  10  11  12  13  14
   15  16  17  18  19  20  21  22  23  24  25  26  27  28
 levsym(*)
    1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1
    1   1   1   1   1   1   1   1   1   1   1   1   1   1
 repartitioning mu(*)=
   2.  0.  0.  0.  0.  0.

 new core energy added to the energy(*) list.
 from the integral file: h1_core= -4.355613553355E+01

 indxdg: diagonal integral statistics.
 total number of integrals contributing to diagonal matrix elements:      1190
 number with all external indices:       812
 number with half external - half internal indices:       336
 number with all internal indices:        42

 indxof: off-diagonal integral statistics.
    4-external integrals: num=      93177 strt=          1
    3-external integrals: num=      72576 strt=      93178
    2-external integrals: num=      25578 strt=     165754
    1-external integrals: num=       4704 strt=     191332
    0-external integrals: num=        315 strt=     196036

 total number of off-diagonal integrals:      196350


 indxof(2nd)  ittp=   3 numx(ittp)=       25578
 indxof(2nd)  ittp=   4 numx(ittp)=        4704
 indxof(2nd)  ittp=   5 numx(ittp)=         315

 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg=  12931980
 pro2e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652

 pro2e:     90013 integrals read in    17 records.

 pro2e:         0 integrals 34-ext integrals skipped.
 pro1e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652
 pro1e: eref =   -1.721733008562787E+00
 total size of srtscr:                     7  records of                  32767 
 WP =               1834952 Bytes

 new core energy added to the energy(*) list.
 from the hamiltonian repartitioning, eref= -1.721733008563E+00
 putdg        1     596    1191    1786    3322   36089   57934    3018   46706   90394
   123161  131353  136813  158652

 putf:       4 buffers of length    1536 written to file 12
 diagonal integral file completed.

 putd34:     3 records of integral w-combinations and
             3 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep4e:     3 blocks of linear combinations of 4-external integrals processed.
 number of sorted 4-external integrals     153468
 number of original 4-external integrals    93177


 putf34: external integral file complete. nfilw=    31 nfilx=    32 nrecw=     3 nrecx=     3 lbufp= 30006

 putd34:     3 records of integral w-combinations and
             3 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep3e:     6 blocks of linear combinations of 3-external integrals processed.
 number of sorted 3-external integrals     131544
 number of original 3-external integrals    72576


 putf34: external integral file complete. nfilw=    33 nfilx=    34 nrecw=     3 nrecx=     3 lbufp= 30006
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd     25578         3    165754    165754     25578    196350
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      4704         4    191332    191332      4704    196350
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd       315         5    196036    196036       315    196350

 putf:      23 buffers of length    1536 written to file 13
 off-diagonal files sort completed.
 executing brd_struct for cisrtinfo
cisrtinfo:
bufszi  1536
 diagfile 4ext:     812 2ext:     336 0ext:      42
 fil4w,fil4x  :   93177 fil3w,fil3x :   72576
 ofdgint  2ext:   25578 1ext:    4704 0ext:     315so0ext:       0so1ext:       0so2ext:       0
buffer minbl4    1215 minbl3    1215 maxbl2    1218nbas:  28   0   0   0   0   0   0   0 maxbuf 30006
 CIUDG version 5.9.7 ( 5-Oct-2004)

 workspace allocation information: lcore=  13107199

 core energy values from the integral file:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -4.355613553355E+01, ietype=    6,   fcore energy of type: H1(*)   
 energy( 3)= -1.721733008563E+00, ietype=    5,   fcore energy of type: Vref(*) 

 total core repulsion energy = -4.527786854211E+01
 nmot  =    38 niot  =     6 nfct  =     4 nfvt  =     0
 nrow  =    23 nsym  =     1 ssym  =     1 lenbuf=  1600
 nwalk,xbar:         50       15       20       15        0
 nvalwt,nvalw:       50       15       20       15        0
 ncsft:            6245
 total number of valid internal walks:      50
 nvalz,nvaly,nvalx,nvalw =       15      20      15       0

 cisrt info file parameters:
 file number  12 blocksize   1536
 mxbld   1536
 nd4ext,nd2ext,nd0ext   812   336    42
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int    93177    72576    25578     4704      315        0        0        0
 minbl4,minbl3,maxbl2  1215  1215  1218
 maxbuf 30006
 number of external orbitals per symmetry block:  28
 nmsym   1 number of internal orbitals   6
 executing brd_struct for drt
 executing brd_struct for orbinf
 executing brd_struct for momap
 calcthrxt: niot,maxw1=                     6                    11
 block size     0
 pthz,pthy,pthx,pthw:    15    20    15     0 total internal walks:      50
 maxlp3,n2lp,n1lp,n0lp    11     0     0     0
 orbsym(*)= 1 1 1 1 1 1

 setref:       15 references kept,
                0 references were marked as invalid, out of
               15 total.
 nmb.of records onel     1
 nmb.of records 2-ext    17
 nmb.of records 1-ext     4
 nmb.of records 0-ext     1
 nmb.of records 2-int     0
 nmb.of records 1-int     0
 nmb.of records 0-int     0
 ---------memory usage in DP -----------------
 < n-ex core usage >
     routines:
    fourex            62738
    threx             60079
    twoex              9922
    onex               3217
    allin              1536
    diagon             2109
               =======
   maximum            62738
 
  __ static summary __ 
   reflst                15
   hrfspc                15
               -------
   static->              30
 
  __ core required  __ 
   totstc                30
   max n-ex           62738
               -------
   totnec->           62768
 
  __ core available __ 
   totspc          13107199
   totnec -           62768
               -------
   totvec->        13044431

 number of external paths / symmetry
 vertex x     378
 vertex w     406
segment: free space=    13044431
 reducing frespc by                   337 to               13044094 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1          15|        15|         0|        15|         0|         1|
 -------------------------------------------------------------------------------
  Y 2          20|       560|        15|        20|        15|         2|
 -------------------------------------------------------------------------------
  X 3          15|      5670|       575|        15|        35|         3|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=           4DP  conft+indsym=          80DP  drtbuffer=         253 DP

dimension of the ci-matrix ->>>      6245

 executing brd_struct for civct
 gentasklist: ntask=                    12
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS   
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      15      15       5670         15      15      15
     2  2   1    11      one-ext yz   1X  2 1      20      15        560         15      20      15
     3  3   2    15      1ex3ex yx    3X  3 2      15      20       5670        560      15      20
     4  1   1     1      allint zz    OX  1 1      15      15         15         15      15      15
     5  2   2     5      0ex2ex yy    OX  2 2      20      20        560        560      20      20
     6  3   3     6      0ex2ex xx*   OX  3 3      15      15       5670       5670      15      15
     7  3   3    18      0ex2ex xx+   OX  3 3      15      15       5670       5670      15      15
     8  2   2    42      four-ext y   4X  2 2      20      20        560        560      20      20
     9  3   3    43      four-ext x   4X  3 3      15      15       5670       5670      15      15
    10  1   1    75      dg-024ext z  OX  1 1      15      15         15         15      15      15
    11  2   2    76      dg-024ext y  OX  2 2      20      20        560        560      20      20
    12  3   3    77      dg-024ext x  OX  3 3      15      15       5670       5670      15      15
----------------------------------------------------------------------------------------------------
REDTASK #   1 TIME=  11.000 N=  1 (task/type/sgbra)=(   1/24/0) (
REDTASK #   2 TIME=  10.000 N=  1 (task/type/sgbra)=(   2/11/0) (
REDTASK #   3 TIME=   9.000 N=  1 (task/type/sgbra)=(   3/15/0) (
REDTASK #   4 TIME=   8.000 N=  1 (task/type/sgbra)=(   4/ 1/0) (
REDTASK #   5 TIME=   7.000 N=  1 (task/type/sgbra)=(   5/ 5/0) (
REDTASK #   6 TIME=   6.000 N=  1 (task/type/sgbra)=(   6/ 6/1) (
REDTASK #   7 TIME=   5.000 N=  1 (task/type/sgbra)=(   7/18/2) (
REDTASK #   8 TIME=   4.000 N=  1 (task/type/sgbra)=(   8/42/1) (
REDTASK #   9 TIME=   3.000 N=  1 (task/type/sgbra)=(   9/43/1) (
REDTASK #  10 TIME=   2.000 N=  1 (task/type/sgbra)=(  10/75/1) (
REDTASK #  11 TIME=   1.000 N=  1 (task/type/sgbra)=(  11/76/1) (
REDTASK #  12 TIME=   0.000 N=  1 (task/type/sgbra)=(  12/77/1) (
 initializing v-file: 1:                  6245

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         175 2x:           0 4x:           0
All internal counts: zz :         295 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 reference space has dimension      15
 dsyevx: computed roots 1 to    2(converged:   2)

    root           eigenvalues
    ----           ------------
       1         -46.2748581090
       2         -46.2748580985

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt=                    15

         vector  1 from unit 11 written to unit 49 filename cirefv              
 ncorel,neli=                     4                     4
  This is a  mraqcc energy evaluation      

    ------------------------------------------------------------

 setopt2: mraqcc energy evaluation      gvalue=  0.833333333333333      ncorel=
                     4

    ------------------------------------------------------------


    reference energies:

  reference state used is:   1with energy      -46.2748581090

  ### begin active excitation selection ###

 inactive(1:nintern)=AAAAAA
 There are         15 reference CSFs out of         15 valid zwalks.
 There are    0 inactive and    6 active orbitals.
      15 active-active  and        0 inactive excitations.

  ### end active excitation selection ###


 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              6245
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:              25
 number of roots to converge:                             1
 number of iterations:                                  200
 residual norm convergence criteria:               0.000100

          starting ci iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         410 2x:         110 4x:          35
All internal counts: zz :         295 yy:         390 xx:         390 ww:           0
One-external counts: yz :         440 yx:         390 yw:           0
Two-external counts: yy :         160 ww:           0 xx:         200 xz:          90 wz:           0 wx:           0
Three-ext.   counts: yx :          60 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1
   ht   1    -0.99698957
calca4: root=   1 anorm**2=  0.00000000 scale:  1.00000000

          calcsovref: tciref block   1

              civs   1
 refs   1    1.00000    

          calcsovref: scrb block   1

                ci   1
 civs   1    1.00000    

          calcsovref: sovref block   1

              v      1
 ref    1    1.00000    

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000
 fact= -8.746584616666664E-003
 factd= -8.746584616666664E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  1  1    -46.2748581090 -1.0769E-14  6.1608E-02  2.2388E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   2

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         410 2x:         110 4x:          35
All internal counts: zz :         295 yy:         390 xx:         390 ww:           0
One-external counts: yz :         440 yx:         390 yw:           0
Two-external counts: yy :         160 ww:           0 xx:         200 xz:          90 wz:           0 wx:           0
Three-ext.   counts: yx :          60 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2
   ht   1    -0.99698957
   ht   2    -0.06160829    -0.06622191
calca4: root=   1 anorm**2=  0.07991867 scale:  1.03919135
calca4: root=   2 anorm**2=  0.92008133 scale:  1.38566999

          calcsovref: tciref block   1

              civs   1       civs   2
 refs   1    1.00000       1.653273E-13

          calcsovref: scrb block   1

                ci   1         ci   2
 civs   1   0.959209      -0.282699    
 civs   2   0.762694        2.58785    

          calcsovref: sovref block   1

              v      1       v      2
 ref    1   0.959209      -0.282699    

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.95920870    -0.28269891
 fact= -8.746584616666664E-003
 factd= -8.746584616666664E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  2  1    -46.3238446165  4.8987E-02  1.5502E-03  3.1238E-02  1.0000E-04
 mraqcc  #  2  2    -45.7108901434  4.3302E-01  0.0000E+00  4.9699E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   3

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         410 2x:         110 4x:          35
All internal counts: zz :         295 yy:         390 xx:         390 ww:           0
One-external counts: yz :         440 yx:         390 yw:           0
Two-external counts: yy :         160 ww:           0 xx:         200 xz:          90 wz:           0 wx:           0
Three-ext.   counts: yx :          60 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3
   ht   1    -0.99698957
   ht   2    -0.06160829    -0.06622191
   ht   3    -0.00159576     0.00433913    -0.00227756
calca4: root=   1 anorm**2=  0.07873859 scale:  1.03862341
calca4: root=   2 anorm**2=  0.98075125 scale:  1.40739165
calca4: root=   3 anorm**2=  0.92078754 scale:  1.38592480

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3
 refs   1    1.00000       1.653273E-13   1.314325E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3
 civs   1   0.958362       2.913084E-02   0.284976    
 civs   2   0.784308      -0.363999       -2.65909    
 civs   3    1.08130        15.9598       -6.65206    

          calcsovref: sovref block   1

              v      1       v      2       v      3
 ref    1   0.959784       5.010723E-02   0.276233    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.95978361     0.05010723     0.27623304
 fact= -8.746584616666664E-003
 factd= -8.746584616666664E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  3  1    -46.3255241914  1.6796E-03  1.5993E-04  1.0708E-02  1.0000E-04
 mraqcc  #  3  2    -45.9182113031  2.0732E-01  0.0000E+00  3.1661E-01  1.0000E-04
 mraqcc  #  3  3    -45.6749294753  3.9706E-01  0.0000E+00  5.1372E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   4

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         410 2x:         110 4x:          35
All internal counts: zz :         295 yy:         390 xx:         390 ww:           0
One-external counts: yz :         440 yx:         390 yw:           0
Two-external counts: yy :         160 ww:           0 xx:         200 xz:          90 wz:           0 wx:           0
Three-ext.   counts: yx :          60 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4
   ht   1    -0.99698957
   ht   2    -0.06160829    -0.06622191
   ht   3    -0.00159576     0.00433913    -0.00227756
   ht   4     0.00213174    -0.00119252    -0.00015664    -0.00031662
calca4: root=   1 anorm**2=  0.08185276 scale:  1.04012151
calca4: root=   2 anorm**2=  0.82236364 scale:  1.34994950
calca4: root=   3 anorm**2=  0.84792143 scale:  1.35938274
calca4: root=   4 anorm**2=  0.82025213 scale:  1.34916720

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4
 refs   1    1.00000       1.653273E-13   1.314325E-03  -2.077700E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4
 civs   1   0.958986      -1.693997E-02   9.051286E-02   0.287331    
 civs   2   0.786417       0.258195       -1.06044       -2.49287    
 civs   3    1.19813        9.94463        11.2263       -8.63558    
 civs   4    1.14346        37.5451       -20.5065        21.7021    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4
 ref    1   0.958185      -8.187690E-02   0.147874       0.230891    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.95818515    -0.08187690     0.14787429     0.23089085
 fact= -8.746584616666664E-003
 factd= -8.746584616666664E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  4  1    -46.3257071273  1.8294E-04  5.7166E-05  5.6724E-03  1.0000E-04
 mraqcc  #  4  2    -46.0735643582  1.5535E-01  0.0000E+00  2.4636E-01  1.0000E-04
 mraqcc  #  4  3    -45.8630769225  1.8815E-01  0.0000E+00  4.0176E-01  1.0000E-04
 mraqcc  #  4  4    -45.5933487548  3.1548E-01  0.0000E+00  5.3552E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   5

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         410 2x:         110 4x:          35
All internal counts: zz :         295 yy:         390 xx:         390 ww:           0
One-external counts: yz :         440 yx:         390 yw:           0
Two-external counts: yy :         160 ww:           0 xx:         200 xz:          90 wz:           0 wx:           0
Three-ext.   counts: yx :          60 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1    -0.99698957
   ht   2    -0.06160829    -0.06622191
   ht   3    -0.00159576     0.00433913    -0.00227756
   ht   4     0.00213174    -0.00119252    -0.00015664    -0.00031662
   ht   5    -0.00244373    -0.00074737    -0.00016776     0.00012826    -0.00024175
calca4: root=   1 anorm**2=  0.08338451 scale:  1.04085758
calca4: root=   2 anorm**2=  0.98101731 scale:  1.40748617
calca4: root=   3 anorm**2=  0.16732776 scale:  1.08042943
calca4: root=   4 anorm**2=  0.95929896 scale:  1.39974961
calca4: root=   5 anorm**2=  0.96416042 scale:  1.40148508

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1    1.00000       1.653273E-13   1.314325E-03  -2.077700E-03   2.478456E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 civs   1   0.956957       0.120443      -0.148786       0.179293      -0.190831    
 civs   2   0.786817      -0.303780       3.014401E-02   -1.40125        2.40011    
 civs   3    1.22345       -9.88100       -1.26268        10.0200        10.7410    
 civs   4    1.39162       -36.2848       -17.2593       -18.6106       -31.6461    
 civs   5   0.695195       -30.7939        44.1889       -16.4654       -31.5662    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1   0.957397       0.106524      -5.066109E-03   0.190321      -0.189198    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     0.95739699     0.10652352    -0.00506611     0.19032084    -0.18919845
 fact= -8.746584616666664E-003
 factd= -8.746584616666664E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  5  1    -46.3257468714  3.9744E-05  4.2270E-06  1.9209E-03  1.0000E-04
 mraqcc  #  5  2    -46.1011116502  2.7547E-02  0.0000E+00  1.2876E-01  1.0000E-04
 mraqcc  #  5  3    -46.0117922210  1.4872E-01  0.0000E+00  4.7825E-01  1.0000E-04
 mraqcc  #  5  4    -45.8445427712  2.5119E-01  0.0000E+00  3.8974E-01  1.0000E-04
 mraqcc  #  5  5    -45.4624806618  1.8461E-01  0.0000E+00  4.8886E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   6

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         410 2x:         110 4x:          35
All internal counts: zz :         295 yy:         390 xx:         390 ww:           0
One-external counts: yz :         440 yx:         390 yw:           0
Two-external counts: yy :         160 ww:           0 xx:         200 xz:          90 wz:           0 wx:           0
Three-ext.   counts: yx :          60 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1    -0.99698957
   ht   2    -0.06160829    -0.06622191
   ht   3    -0.00159576     0.00433913    -0.00227756
   ht   4     0.00213174    -0.00119252    -0.00015664    -0.00031662
   ht   5    -0.00244373    -0.00074737    -0.00016776     0.00012826    -0.00024175
   ht   6     0.00031181    -0.00008907     0.00001182    -0.00003808     0.00001930    -0.00000671
calca4: root=   1 anorm**2=  0.08384512 scale:  1.04107882
calca4: root=   2 anorm**2=  0.94606876 scale:  1.39501568
calca4: root=   3 anorm**2=  0.18435679 scale:  1.08828158
calca4: root=   4 anorm**2=  0.96754844 scale:  1.40269328
calca4: root=   5 anorm**2=  0.95472594 scale:  1.39811514
calca4: root=   6 anorm**2=  0.98633323 scale:  1.40937335

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1    1.00000       1.653273E-13   1.314325E-03  -2.077700E-03   2.478456E-03  -3.077799E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1   0.956932       8.009259E-02   0.164095      -0.140812       0.200223      -0.113064    
 civs   2   0.786853      -0.263132      -7.638556E-02   0.807330       -2.09861        1.64196    
 civs   3    1.22662       -7.00647       -4.17071       -12.1531       -3.16980        9.53547    
 civs   4    1.42345       -33.8491        2.96344       -1.32540       -15.3490       -46.7458    
 civs   5   0.784391       -27.2432       -43.1909        30.4030        22.7932       -18.7434    
 civs   6    1.20681       -124.564        76.7638        134.589        249.706        212.773    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.957159       0.112029       2.178276E-02  -0.120103       0.207585      -0.115349    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.95715905     0.11202911     0.02178276    -0.12010251     0.20758495    -0.11534913
 fact= -8.746584616666664E-003
 factd= -8.746584616666664E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  6  1    -46.3257519726  5.1012E-06  2.0905E-07  3.9556E-04  1.0000E-04
 mraqcc  #  6  2    -46.1426575754  4.1546E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  6  3    -46.0325749057  2.0783E-02  0.0000E+00  4.5481E-01  1.0000E-04
 mraqcc  #  6  4    -45.9013895987  5.6847E-02  0.0000E+00  3.1112E-01  1.0000E-04
 mraqcc  #  6  5    -45.6431705441  1.8069E-01  0.0000E+00  4.8968E-01  1.0000E-04
 mraqcc  #  6  6    -45.3545616923  7.6693E-02  0.0000E+00  4.5272E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   7

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         410 2x:         110 4x:          35
All internal counts: zz :         295 yy:         390 xx:         390 ww:           0
One-external counts: yz :         440 yx:         390 yw:           0
Two-external counts: yy :         160 ww:           0 xx:         200 xz:          90 wz:           0 wx:           0
Three-ext.   counts: yx :          60 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1    -0.99698957
   ht   2    -0.06160829    -0.06622191
   ht   3    -0.00159576     0.00433913    -0.00227756
   ht   4     0.00213174    -0.00119252    -0.00015664    -0.00031662
   ht   5    -0.00244373    -0.00074737    -0.00016776     0.00012826    -0.00024175
   ht   6     0.00031181    -0.00008907     0.00001182    -0.00003808     0.00001930    -0.00000671
   ht   7     0.00002489     0.00003962     0.00000471    -0.00000227     0.00000229    -0.00000062    -0.00000031
calca4: root=   1 anorm**2=  0.08380161 scale:  1.04105793
calca4: root=   2 anorm**2=  0.89018905 scale:  1.37484146
calca4: root=   3 anorm**2=  0.42625074 scale:  1.19425740
calca4: root=   4 anorm**2=  0.82307649 scale:  1.35021350
calca4: root=   5 anorm**2=  0.93404766 scale:  1.39070042
calca4: root=   6 anorm**2=  0.97948606 scale:  1.40694210
calca4: root=   7 anorm**2=  0.98264526 scale:  1.40806437

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1    1.00000       1.653273E-13   1.314325E-03  -2.077700E-03   2.478456E-03  -3.077799E-04  -2.670919E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1   0.956992       4.570794E-02  -0.185891       9.685324E-02   0.149790      -0.144127      -0.129467    
 civs   2   0.786852      -0.212315       0.227441      -0.219530       -1.26584        1.68269        1.87957    
 civs   3    1.22676       -6.05000        6.05403        8.78889        7.47005        3.27742        10.2136    
 civs   4    1.42483       -32.0971        7.16385        8.91006       -9.59654        16.4901       -45.4825    
 civs   5   0.788248       -23.8952        35.4813       -44.6183        4.72405       -12.9784       -19.9255    
 civs   6    1.25901       -140.939       -73.8889       -112.797       -32.9312       -281.175        175.914    
 civs   7    1.05566       -261.732       -568.975       -638.269        836.153        812.990        221.349    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.957182       9.559014E-02  -6.694149E-02   3.107213E-02   0.179058      -0.141421      -0.130983    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.95718205     0.09559014    -0.06694149     0.03107213     0.17905819    -0.14142128    -0.13098321
 fact= -8.746584616666664E-003
 factd= -8.746584616666664E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  7  1    -46.3257521933  2.2069E-07  2.1535E-08  1.0468E-04  1.0000E-04
 mraqcc  #  7  2    -46.1495486856  6.8911E-03  0.0000E+00  4.0184E-02  1.0000E-04
 mraqcc  #  7  3    -46.0699686769  3.7394E-02  0.0000E+00  3.5789E-01  1.0000E-04
 mraqcc  #  7  4    -45.9243598053  2.2970E-02  0.0000E+00  3.4035E-01  1.0000E-04
 mraqcc  #  7  5    -45.8627860399  2.1962E-01  0.0000E+00  4.0370E-01  1.0000E-04
 mraqcc  #  7  6    -45.5198723499  1.6531E-01  0.0000E+00  4.8199E-01  1.0000E-04
 mraqcc  #  7  7    -45.3471690053  6.9300E-02  0.0000E+00  4.5417E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   8

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         410 2x:         110 4x:          35
All internal counts: zz :         295 yy:         390 xx:         390 ww:           0
One-external counts: yz :         440 yx:         390 yw:           0
Two-external counts: yy :         160 ww:           0 xx:         200 xz:          90 wz:           0 wx:           0
Three-ext.   counts: yx :          60 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1    -0.99698957
   ht   2    -0.06160829    -0.06622191
   ht   3    -0.00159576     0.00433913    -0.00227756
   ht   4     0.00213174    -0.00119252    -0.00015664    -0.00031662
   ht   5    -0.00244373    -0.00074737    -0.00016776     0.00012826    -0.00024175
   ht   6     0.00031181    -0.00008907     0.00001182    -0.00003808     0.00001930    -0.00000671
   ht   7     0.00002489     0.00003962     0.00000471    -0.00000227     0.00000229    -0.00000062    -0.00000031
   ht   8     0.00003403     0.00000610     0.00000099    -0.00000246     0.00000308    -0.00000050    -0.00000004    -0.00000007
calca4: root=   1 anorm**2=  0.08379101 scale:  1.04105284
calca4: root=   2 anorm**2=  0.29863912 scale:  1.13957848
calca4: root=   3 anorm**2=  0.92925953 scale:  1.38897787
calca4: root=   4 anorm**2=  0.94327493 scale:  1.39401396
calca4: root=   5 anorm**2=  0.93142484 scale:  1.38975711
calca4: root=   6 anorm**2=  0.91695553 scale:  1.38454163
calca4: root=   7 anorm**2=  0.96878033 scale:  1.40313233
calca4: root=   8 anorm**2=  0.98907902 scale:  1.41034713

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1    1.00000       1.653273E-13   1.314325E-03  -2.077700E-03   2.478456E-03  -3.077799E-04  -2.670919E-05  -3.431547E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1   0.957045      -0.125877       0.154981      -5.792315E-02   0.148143      -4.673587E-02   0.176612       9.297952E-02
 civs   2   0.786850       4.266780E-04  -0.293574       0.159586       -1.26243       1.994249E-02   -2.07904       -1.44809    
 civs   3    1.22678      -0.870528       -7.58027       -9.69577        7.43274       0.304813       -5.48303       -9.15453    
 civs   4    1.42500       -10.1742       -31.2380       -6.00454       -9.79669       -21.5630       -1.89990        43.7772    
 civs   5   0.788713        2.35571       -37.1701        30.8957        5.48124        44.2876        3.03037        29.3834    
 civs   6    1.26534       -86.0263       -89.1676        138.284       -32.7959        33.7270        256.865       -252.416    
 civs   7    1.18351       -445.389        66.8321        769.882        836.355       -317.753       -780.566       -94.4167    
 civs   8    1.24114       -1967.18        760.676       -690.281        29.6733        3164.50       -1319.07        1335.67    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.957188       5.834674E-03   0.117353      -2.105388E-02   0.178589      -2.253700E-03   0.167919       9.719322E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.95718784     0.00583467     0.11735297    -0.02105388     0.17858926    -0.00225370     0.16791870     0.09719322
 fact= -8.746584616666664E-003
 factd= -8.746584616666664E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  8  1    -46.3257522200  2.6728E-08  7.5763E-09  6.7966E-05  1.0000E-04
 mraqcc  #  8  2    -46.2093282870  5.9780E-02  0.0000E+00  2.1367E-01  1.0000E-04
 mraqcc  #  8  3    -46.1329260096  6.2957E-02  0.0000E+00  7.8723E-02  1.0000E-04
 mraqcc  #  8  4    -45.9369316043  1.2572E-02  0.0000E+00  2.5239E-01  1.0000E-04
 mraqcc  #  8  5    -45.8628020636  1.6024E-05  0.0000E+00  4.0506E-01  1.0000E-04
 mraqcc  #  8  6    -45.7129919087  1.9312E-01  0.0000E+00  4.0560E-01  1.0000E-04
 mraqcc  #  8  7    -45.4858371835  1.3867E-01  0.0000E+00  5.0235E-01  1.0000E-04
 mraqcc  #  8  8    -45.3083491563  3.0481E-02  0.0000E+00  4.3388E-01  1.0000E-04
 
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mraqcc   convergence criteria satisfied after  8 iterations.

 final mraqcc   convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  8  1    -46.3257522200  2.6728E-08  7.5763E-09  6.7966E-05  1.0000E-04
 mraqcc  #  8  2    -46.2093282870  5.9780E-02  0.0000E+00  2.1367E-01  1.0000E-04
 mraqcc  #  8  3    -46.1329260096  6.2957E-02  0.0000E+00  7.8723E-02  1.0000E-04
 mraqcc  #  8  4    -45.9369316043  1.2572E-02  0.0000E+00  2.5239E-01  1.0000E-04
 mraqcc  #  8  5    -45.8628020636  1.6024E-05  0.0000E+00  4.0506E-01  1.0000E-04
 mraqcc  #  8  6    -45.7129919087  1.9312E-01  0.0000E+00  4.0560E-01  1.0000E-04
 mraqcc  #  8  7    -45.4858371835  1.3867E-01  0.0000E+00  5.0235E-01  1.0000E-04
 mraqcc  #  8  8    -45.3083491563  3.0481E-02  0.0000E+00  4.3388E-01  1.0000E-04

####################CIUDGINFO####################

   aqcc(lrt) vector at position   1 energy=  -46.325752220048

################END OF CIUDGINFO################

 
 a4den factor for root 1  =  1.014162956
diagon:itrnv=   0
    1 of the   9 expansion vectors are transformed.
    1 of the   8 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
maximum overlap with reference    1(overlap= 0.95719)
reference energy =                         -46.2748581090
total aqcc energy =                        -46.3257522200
diagonal element shift (lrtshift) =         -0.0508941110

 information on vector: 1 from unit 11 written to unit 48 filename civout              


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -46.3257522200

                                                       internal orbitals

                                          level       1    2    3    4    5    6

                                          orbital     5    6    7    8    9   10

                                         symmetry   a    a    a    a    a    a  

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  5  1       3 -0.856431                        +    +    +              +  
 z*  5  1      10 -0.427471                        +              +    +    +  
 y   5  1      23  0.081270              8( a  )   +    +    +                 
 y   5  1      32  0.066247             17( a  )   +    +    +                 
 y   5  1     104 -0.105856              5( a  )   +    +                   +  
 y   5  1     112 -0.076566             13( a  )   +    +                   +  
 y   5  1     187 -0.105857              4( a  )   +         +              +  
 y   5  1     197  0.076567             14( a  )   +         +              +  
 y   5  1     219  0.065069              8( a  )   +              +    +       
 y   5  1     228  0.043348             17( a  )   +              +    +       
 y   5  1     246 -0.040516              7( a  )   +              +         +  
 y   5  1     255  0.033038             16( a  )   +              +         +  
 y   5  1     273  0.040558              6( a  )   +                   +    +  
 y   5  1     282 -0.033055             15( a  )   +                   +    +  
 y   5  1     363 -0.014149             12( a  )        +    +              +  
 x   5  1     601  0.012763    5( a  )   8( a  )   +    +                      
 x   5  1     649 -0.011483    8( a  )  13( a  )   +    +                      
 x   5  1     700  0.012269    5( a  )  17( a  )   +    +                      
 x   5  1     708  0.017717   13( a  )  17( a  )   +    +                      
 x   5  1     873 -0.013529   22( a  )  25( a  )   +    +                      
 x   5  1     896 -0.013528   21( a  )  26( a  )   +    +                      
 x   5  1     924  0.014394   24( a  )  27( a  )   +    +                      
 x   5  1     978  0.012763    4( a  )   8( a  )   +         +                 
 x   5  1    1039  0.011483    8( a  )  14( a  )   +         +                 
 x   5  1    1077  0.012269    4( a  )  17( a  )   +         +                 
 x   5  1    1087 -0.017717   14( a  )  17( a  )   +         +                 
 x   5  1    1250 -0.013529   21( a  )  25( a  )   +         +                 
 x   5  1    1275  0.013528   22( a  )  26( a  )   +         +                 
 x   5  1    1301  0.014394   23( a  )  27( a  )   +         +                 
 x   5  1    1333  0.025436    1( a  )   3( a  )   +              +            
 x   5  1    1370  0.019085    3( a  )  10( a  )   +              +            
 x   5  1    1377 -0.018769    1( a  )  11( a  )   +              +            
 x   5  1    1386  0.014668   10( a  )  11( a  )   +              +            
 x   5  1    1467 -0.010136   16( a  )  17( a  )   +              +            
 x   5  1    1682  0.014700   26( a  )  27( a  )   +              +            
 x   5  1    1712  0.025546    2( a  )   3( a  )   +                   +       
 x   5  1    1740 -0.019167    3( a  )   9( a  )   +                   +       
 x   5  1    1756 -0.018851    2( a  )  11( a  )   +                   +       
 x   5  1    1763 -0.014731    9( a  )  11( a  )   +                   +       
 x   5  1    1844  0.010135   15( a  )  17( a  )   +                   +       
 x   5  1    2059  0.014689   25( a  )  27( a  )   +                   +       
 x   5  1    2088 -0.044330    1( a  )   2( a  )   +                        +  
 x   5  1    2097 -0.014648    4( a  )   5( a  )   +                        +  
 x   5  1    2116 -0.032966    1( a  )   9( a  )   +                        +  
 x   5  1    2125 -0.032966    2( a  )  10( a  )   +                        +  
 x   5  1    2132 -0.025559    9( a  )  10( a  )   +                        +  
 x   5  1    2157 -0.013540    4( a  )  13( a  )   +                        +  
 x   5  1    2170 -0.013540    5( a  )  14( a  )   +                        +  
 x   5  1    2178 -0.020308   13( a  )  14( a  )   +                        +  
 x   5  1    2318 -0.035197   21( a  )  22( a  )   +                        +  
 x   5  1    2528 -0.021779    8( a  )  12( a  )        +    +                 
 x   5  1    2597  0.017865   12( a  )  17( a  )        +    +                 
 x   5  1    2743  0.015295    2( a  )  25( a  )        +    +                 
 x   5  1    2750  0.014052    9( a  )  25( a  )        +    +                 
 x   5  1    2766  0.015299    1( a  )  26( a  )        +    +                 
 x   5  1    2775 -0.014056   10( a  )  26( a  )        +    +                 
 x   5  1    2793  0.020676    3( a  )  27( a  )        +    +                 
 x   5  1    2801 -0.018547   11( a  )  27( a  )        +    +                 
 x   5  1    3659  0.023417    5( a  )  12( a  )        +                   +  
 x   5  1    3677 -0.017179   12( a  )  13( a  )        +                   +  
 x   5  1    3790  0.017738    1( a  )  21( a  )        +                   +  
 x   5  1    3799 -0.016140   10( a  )  21( a  )        +                   +  
 x   5  1    3811  0.017742    2( a  )  22( a  )        +                   +  
 x   5  1    3818  0.016143    9( a  )  22( a  )        +                   +  
 x   5  1    3855 -0.014447    3( a  )  24( a  )        +                   +  
 x   5  1    3863  0.013105   11( a  )  24( a  )        +                   +  
 x   5  1    4792  0.023417    4( a  )  12( a  )             +              +  
 x   5  1    4823  0.017179   12( a  )  14( a  )             +              +  
 x   5  1    4925  0.017742    2( a  )  21( a  )             +              +  
 x   5  1    4932  0.016143    9( a  )  21( a  )             +              +  
 x   5  1    4944 -0.017738    1( a  )  22( a  )             +              +  
 x   5  1    4953  0.016140   10( a  )  22( a  )             +              +  
 x   5  1    4967 -0.014447    3( a  )  23( a  )             +              +  
 x   5  1    4975  0.013105   11( a  )  23( a  )             +              +  
 x   5  1    5174 -0.012507    8( a  )  12( a  )                  +    +       
 x   5  1    5551  0.010863    7( a  )  12( a  )                  +         +  
 x   5  1    5928 -0.010866    6( a  )  12( a  )                       +    +  

 ci coefficient statistics:
           rq > 0.1                4
      0.1> rq > 0.01              73
     0.01> rq > 0.001            250
    0.001> rq > 0.0001           256
   0.0001> rq > 0.00001          181
  0.00001> rq > 0.000001         318
 0.000001> rq                   5163
           all                  6245
  iref  icsf         v(icsf)             hv(icsf)
     1     1     -0.000002321731
     2     2      0.000000235524
     3     3     -0.856430987554
     4     4      0.000002085236
     5     5     -0.000002643055
     6     6     -0.000000764638
     7     7     -0.001924640064
     8     8     -0.000000552049
     9     9      0.000001586653
    10    10     -0.427470761904
    11    11      0.000000239611
    12    12      0.000000000096
    13    13      0.000000000145
    14    14      0.000000000324
    15    15      0.000000037321

 number of reference csfs (nref) is    15.  root number (iroot) is  1.
 c0**2 =   0.91620899  c0**2(renormalized) =   0.99297907
 c**2 (all zwalks) =   0.91620899  c**2(all zwalks, renormalized) =   0.99297907
 passed aftci ... 
 readint2: molcas,dalton2=                     0                     0
 files%faoints=aoints              
lodens (list->root)=  1
sifcfg setup: record length 4096 DP
# d1 elements per record  3272
# d2 elements per record  2730
  The AQCC density will be calculated.
 item #                     1 suffix=:.drt1.state1:
 read_civout: repnuc=  -45.2778685421096     
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method: 30 last record  1max overlap with ref# 96% root-following 0
 MR-CISD energy:   -46.32575222    -1.04788368
 residuum:     0.00006797
 deltae:     0.00000003
 apxde:     0.00000001
 a4den:     1.01416296
 reference energy:   -46.27485811    -0.99698957

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.95718784     0.00583467     0.11735297    -0.02105388     0.17858926    -0.00225370     0.16791870     0.09719322

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.95718784     0.00583467     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 rdcirefv: extracting reference vector # 1(blocksize=    15 #zcsf=      15)
 =========== Executing IN-CORE method ==========
================================================================================
   DYZ=       0  DYX=       0  DYW=       0
   D0Z=      60  D0Y=       0  D0X=       0  D0W=       0
  DDZI=      65 DDYI=       0 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
 root #                      1 : Scaling(1) with    1.01416295589178      
 corresponding to ref #                      1
 computing final density
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=      60  DYX=      60  DYW=       0
   D0Z=      60  D0Y=      90  D0X=      60  D0W=       0
  DDZI=      65 DDYI=      70 DDXI=      40 DDWI=       0
  DDZE=       0 DDYE=      20 DDXE=      15 DDWE=       0
================================================================================
Trace of MO density:     4.000000
    4  correlated and     8  frozen core electrons

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   4     0.00000000     0.00000000     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.00000000     0.00000000     0.98925100     0.00000000    -0.00000142     0.00000000
   MO   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.76878356     0.00000000    -0.00000015
   MO   7     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000142     0.00000000     0.76878746     0.00000166
   MO   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000015     0.00000166     0.19548773
   MO   9     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000024     0.00000044     0.00000002
   MO  10     0.00000000     0.00000000     0.00000000     0.00000000    -0.00032194    -0.00000091     0.00085604     0.00000210
   MO  11     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  12     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  13     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  14     0.00000000     0.00000000     0.00000000     0.00000000     0.00000003    -0.08710891     0.00061481     0.00000051
   MO  15     0.00000000     0.00000000     0.00000000     0.00000000     0.00000150     0.00061482     0.08710814     0.00000010
   MO  16     0.00000000     0.00000000     0.00000000     0.00000000     0.00000003     0.00000257     0.00000066     0.01636915
   MO  17     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000009    -0.00000008     0.00000231
   MO  18     0.00000000     0.00000000     0.00000000     0.00000000     0.00295139     0.00000016     0.00044904     0.00000008
   MO  19     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  20     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  21     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  22     0.00000000     0.00000000     0.00000000     0.00000000     0.00197752     0.00000000     0.00000042     0.00000000
   MO  23     0.00000000     0.00000000     0.00000000     0.00000000     0.00002207    -0.00027029     0.06109708     0.00000016
   MO  24     0.00000000     0.00000000     0.00000000     0.00000000     0.00000007     0.06109706     0.00027029    -0.00000008
   MO  25     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000002    -0.00000031    -0.00000001    -0.01291009
   MO  26     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000001    -0.00000010     0.00000016    -0.00004970
   MO  27     0.00000000     0.00000000     0.00000000     0.00000000     0.00273276     0.00000031    -0.00014981     0.00000011
   MO  28     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  29     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  30     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  31     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  32     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  33     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  34     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  35     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  36     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  37     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  38     0.00000000     0.00000000     0.00000000     0.00000000    -0.00045646     0.00000000     0.00000002     0.00000000

                MO   9         MO  10         MO  11         MO  12         MO  13         MO  14         MO  15         MO  16
   MO   5     0.00000000    -0.00032194     0.00000000     0.00000000     0.00000000     0.00000003     0.00000150     0.00000003
   MO   6     0.00000024    -0.00000091     0.00000000     0.00000000     0.00000000    -0.08710891     0.00061482     0.00000257
   MO   7     0.00000044     0.00085604     0.00000000     0.00000000     0.00000000     0.00061481     0.08710814     0.00000066
   MO   8     0.00000002     0.00000210     0.00000000     0.00000000     0.00000000     0.00000051     0.00000010     0.01636915
   MO   9     0.19551717    -0.00000021     0.00000000     0.00000000     0.00000000    -0.00000003     0.00000007    -0.00000237
   MO  10    -0.00000021     0.97047812     0.00000000     0.00000000     0.00000000    -0.00000138    -0.00026142     0.00000092
   MO  11     0.00000000     0.00000000     0.00573386     0.00000000    -0.00000004     0.00000000     0.00000000     0.00000000
   MO  12     0.00000000     0.00000000     0.00000000     0.00574352    -0.00000003     0.00000000     0.00000000     0.00000000
   MO  13     0.00000000     0.00000000    -0.00000004    -0.00000003     0.00346795     0.00000000     0.00000000     0.00000000
   MO  14    -0.00000003    -0.00000138     0.00000000     0.00000000     0.00000000     0.01266365     0.00000000    -0.00000031
   MO  15     0.00000007    -0.00026142     0.00000000     0.00000000     0.00000000     0.00000000     0.01266356     0.00000009
   MO  16    -0.00000237     0.00000092     0.00000000     0.00000000     0.00000000    -0.00000031     0.00000009     0.00207051
   MO  17     0.01632344    -0.00000005     0.00000000     0.00000000     0.00000000    -0.00000001    -0.00000001    -0.00000001
   MO  18    -0.00000002     0.09280755     0.00000000     0.00000000     0.00000000     0.00000014     0.00001615     0.00000011
   MO  19     0.00000000     0.00000000    -0.00000736     0.00455963    -0.00000003     0.00000000     0.00000000     0.00000000
   MO  20     0.00000000     0.00000000    -0.00455204    -0.00000737     0.00000012     0.00000000     0.00000000     0.00000000
   MO  21     0.00000000     0.00000000    -0.00000012     0.00000000    -0.00277167     0.00000000     0.00000000     0.00000000
   MO  22     0.00000000     0.00009541     0.00000000     0.00000000     0.00000000     0.00000000     0.00000001     0.00000000
   MO  23     0.00000008     0.00032310     0.00000000     0.00000000     0.00000000     0.00010898     0.00949117     0.00000007
   MO  24     0.00000001     0.00000089     0.00000000     0.00000000     0.00000000    -0.00949143     0.00010898     0.00000027
   MO  25     0.00004966    -0.00000038     0.00000000     0.00000000     0.00000000     0.00000000     0.00000001    -0.00171229
   MO  26    -0.01289038    -0.00000022     0.00000000     0.00000000     0.00000000     0.00000002     0.00000002    -0.00000635
   MO  27    -0.00000006     0.06941737     0.00000000     0.00000000     0.00000000    -0.00000045    -0.00006187     0.00000009
   MO  28     0.00000000     0.00000000     0.00000139    -0.00148383     0.00000002     0.00000000     0.00000000     0.00000000
   MO  29     0.00000000     0.00000000    -0.00148138    -0.00000139     0.00000003     0.00000000     0.00000000     0.00000000
   MO  30     0.00000000     0.00000000     0.00000002     0.00000001     0.00090798     0.00000000     0.00000000     0.00000000
   MO  31     0.00000000     0.00000000     0.00000151    -0.00000032     0.00000000     0.00000000     0.00000000     0.00000000
   MO  32     0.00000000     0.00000000     0.00000032     0.00000149     0.00000000     0.00000000     0.00000000     0.00000000
   MO  33     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  34     0.00000000     0.00000000     0.00000000     0.00000000     0.00000075     0.00000000     0.00000000     0.00000000
   MO  35     0.00000000     0.00000000     0.00000159    -0.00074820     0.00000001     0.00000000     0.00000000     0.00000000
   MO  36     0.00000000     0.00000000    -0.00074660    -0.00000160     0.00000001     0.00000000     0.00000000     0.00000000
   MO  37     0.00000000     0.00000000     0.00000001     0.00000000     0.00049529     0.00000000     0.00000000     0.00000000
   MO  38     0.00000000     0.00000371     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                MO  17         MO  18         MO  19         MO  20         MO  21         MO  22         MO  23         MO  24
   MO   5     0.00000000     0.00295139     0.00000000     0.00000000     0.00000000     0.00197752     0.00002207     0.00000007
   MO   6     0.00000009     0.00000016     0.00000000     0.00000000     0.00000000     0.00000000    -0.00027029     0.06109706
   MO   7    -0.00000008     0.00044904     0.00000000     0.00000000     0.00000000     0.00000042     0.06109708     0.00027029
   MO   8     0.00000231     0.00000008     0.00000000     0.00000000     0.00000000     0.00000000     0.00000016    -0.00000008
   MO   9     0.01632344    -0.00000002     0.00000000     0.00000000     0.00000000     0.00000000     0.00000008     0.00000001
   MO  10    -0.00000005     0.09280755     0.00000000     0.00000000     0.00000000     0.00009541     0.00032310     0.00000089
   MO  11     0.00000000     0.00000000    -0.00000736    -0.00455204    -0.00000012     0.00000000     0.00000000     0.00000000
   MO  12     0.00000000     0.00000000     0.00455963    -0.00000737     0.00000000     0.00000000     0.00000000     0.00000000
   MO  13     0.00000000     0.00000000    -0.00000003     0.00000012    -0.00277167     0.00000000     0.00000000     0.00000000
   MO  14    -0.00000001     0.00000014     0.00000000     0.00000000     0.00000000     0.00000000     0.00010898    -0.00949143
   MO  15    -0.00000001     0.00001615     0.00000000     0.00000000     0.00000000     0.00000001     0.00949117     0.00010898
   MO  16    -0.00000001     0.00000011     0.00000000     0.00000000     0.00000000     0.00000000     0.00000007     0.00000027
   MO  17     0.00206120    -0.00000001     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000001
   MO  18    -0.00000001     0.01252285     0.00000000     0.00000000     0.00000000     0.00001989     0.00008466     0.00000033
   MO  19     0.00000000     0.00000000     0.00375637    -0.00000001     0.00000001     0.00000000     0.00000000     0.00000000
   MO  20     0.00000000     0.00000000    -0.00000001     0.00375037     0.00000002     0.00000000     0.00000000     0.00000000
   MO  21     0.00000000     0.00000000     0.00000001     0.00000002     0.00234376     0.00000000     0.00000000     0.00000000
   MO  22     0.00000000     0.00001989     0.00000000     0.00000000     0.00000000     0.00339840     0.00000020     0.00000000
   MO  23     0.00000000     0.00008466     0.00000000     0.00000000     0.00000000     0.00000020     0.00732529     0.00000000
   MO  24     0.00000001     0.00000033     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00732521
   MO  25     0.00000633    -0.00000004     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000003
   MO  26    -0.00170666    -0.00000003     0.00000000     0.00000000     0.00000000     0.00000000     0.00000001    -0.00000001
   MO  27    -0.00000001     0.00986285     0.00000000     0.00000000     0.00000000     0.00002504     0.00001127     0.00000003
   MO  28     0.00000000     0.00000000    -0.00130365     0.00000088    -0.00000001     0.00000000     0.00000000     0.00000000
   MO  29     0.00000000     0.00000000     0.00000088     0.00130168     0.00000002     0.00000000     0.00000000     0.00000000
   MO  30     0.00000000     0.00000000     0.00000000     0.00000001    -0.00083997     0.00000000     0.00000000     0.00000000
   MO  31     0.00000000     0.00000000    -0.00000027    -0.00000124     0.00000000     0.00000000     0.00000000     0.00000000
   MO  32     0.00000000     0.00000000     0.00000124    -0.00000027     0.00000000     0.00000000     0.00000000     0.00000000
   MO  33     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  34     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000133     0.00000000     0.00000000     0.00000000
   MO  35     0.00000000     0.00000000    -0.00073109    -0.00000038     0.00000000     0.00000000     0.00000000     0.00000000
   MO  36     0.00000000     0.00000000    -0.00000038     0.00072953     0.00000001     0.00000000     0.00000000     0.00000000
   MO  37     0.00000000     0.00000000     0.00000000     0.00000001    -0.00065229     0.00000000     0.00000000     0.00000000
   MO  38     0.00000000    -0.00000060     0.00000000     0.00000000     0.00000000    -0.00016542    -0.00000001     0.00000000

                MO  25         MO  26         MO  27         MO  28         MO  29         MO  30         MO  31         MO  32
   MO   5    -0.00000002    -0.00000001     0.00273276     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   6    -0.00000031    -0.00000010     0.00000031     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   7    -0.00000001     0.00000016    -0.00014981     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   8    -0.01291009    -0.00004970     0.00000011     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   9     0.00004966    -0.01289038    -0.00000006     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  10    -0.00000038    -0.00000022     0.06941737     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  11     0.00000000     0.00000000     0.00000000     0.00000139    -0.00148138     0.00000002     0.00000151     0.00000032
   MO  12     0.00000000     0.00000000     0.00000000    -0.00148383    -0.00000139     0.00000001    -0.00000032     0.00000149
   MO  13     0.00000000     0.00000000     0.00000000     0.00000002     0.00000003     0.00090798     0.00000000     0.00000000
   MO  14     0.00000000     0.00000002    -0.00000045     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  15     0.00000001     0.00000002    -0.00006187     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  16    -0.00171229    -0.00000635     0.00000009     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  17     0.00000633    -0.00170666    -0.00000001     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  18    -0.00000004    -0.00000003     0.00986285     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  19     0.00000000     0.00000000     0.00000000    -0.00130365     0.00000088     0.00000000    -0.00000027     0.00000124
   MO  20     0.00000000     0.00000000     0.00000000     0.00000088     0.00130168     0.00000001    -0.00000124    -0.00000027
   MO  21     0.00000000     0.00000000     0.00000000    -0.00000001     0.00000002    -0.00083997     0.00000000     0.00000000
   MO  22     0.00000000     0.00000000     0.00002504     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  23     0.00000000     0.00000001     0.00001127     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  24    -0.00000003    -0.00000001     0.00000003     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  25     0.00148169     0.00000001    -0.00000004     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  26     0.00000001     0.00147863    -0.00000002     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  27    -0.00000004    -0.00000002     0.00806169     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  28     0.00000000     0.00000000     0.00000000     0.00050598     0.00000000     0.00000000     0.00000009    -0.00000043
   MO  29     0.00000000     0.00000000     0.00000000     0.00000000     0.00050530     0.00000000    -0.00000043    -0.00000009
   MO  30     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00034499     0.00000000     0.00000000
   MO  31     0.00000000     0.00000000     0.00000000     0.00000009    -0.00000043     0.00000000     0.00308666     0.00000000
   MO  32     0.00000000     0.00000000     0.00000000    -0.00000043    -0.00000009     0.00000000     0.00000000     0.00308666
   MO  33     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000001     0.00000000
   MO  34     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000095     0.00000000     0.00000000
   MO  35     0.00000000     0.00000000     0.00000000     0.00033099    -0.00000039     0.00000000     0.00000022    -0.00000103
   MO  36     0.00000000     0.00000000     0.00000000     0.00000040     0.00033029     0.00000000    -0.00000100    -0.00000022
   MO  37     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00039394     0.00000000     0.00000000
   MO  38     0.00000000     0.00000000    -0.00000099     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                MO  33         MO  34         MO  35         MO  36         MO  37         MO  38
   MO   5     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00045646
   MO   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000002
   MO   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   9     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  10     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000371
   MO  11     0.00000000     0.00000000     0.00000159    -0.00074660     0.00000001     0.00000000
   MO  12     0.00000000     0.00000000    -0.00074820    -0.00000160     0.00000000     0.00000000
   MO  13     0.00000000     0.00000075     0.00000001     0.00000001     0.00049529     0.00000000
   MO  14     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  15     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  16     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  17     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  18     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000060
   MO  19     0.00000000     0.00000000    -0.00073109    -0.00000038     0.00000000     0.00000000
   MO  20     0.00000000     0.00000000    -0.00000038     0.00072953     0.00000001     0.00000000
   MO  21     0.00000000    -0.00000133     0.00000000     0.00000001    -0.00065229     0.00000000
   MO  22     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00016542
   MO  23     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000001
   MO  24     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  25     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  26     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  27     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000099
   MO  28     0.00000000     0.00000000     0.00033099     0.00000040     0.00000000     0.00000000
   MO  29     0.00000000     0.00000000    -0.00000039     0.00033029     0.00000000     0.00000000
   MO  30     0.00000000     0.00000095     0.00000000     0.00000000     0.00039394     0.00000000
   MO  31    -0.00000001     0.00000000     0.00000022    -0.00000100     0.00000000     0.00000000
   MO  32     0.00000000     0.00000000    -0.00000103    -0.00000022     0.00000000     0.00000000
   MO  33     0.00115596     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  34     0.00000000     0.00115599     0.00000000     0.00000000     0.00000405     0.00000000
   MO  35     0.00000000     0.00000000     0.00196053     0.00000000     0.00000000     0.00000000
   MO  36     0.00000000     0.00000000     0.00000000     0.00195904     0.00000000     0.00000000
   MO  37     0.00000000     0.00000405     0.00000000     0.00000000     0.00207528     0.00000000
   MO  38     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00001008

Natural orbital populations,block 1
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     0.98927686     0.98441019     0.78360640     0.78360629
              MO     9       MO    10       MO    11       MO    12       MO    13       MO    14       MO    15       MO    16
  occ(*)=     0.19774433     0.19772556     0.00998154     0.00996492     0.00649914     0.00618499     0.00510898     0.00510897
              MO    17       MO    18       MO    19       MO    20       MO    21       MO    22       MO    23       MO    24
  occ(*)=     0.00340236     0.00308666     0.00308666     0.00193817     0.00182574     0.00182455     0.00128216     0.00128052
              MO    25       MO    26       MO    27       MO    28       MO    29       MO    30       MO    31       MO    32
  occ(*)=     0.00115597     0.00115596     0.00015582     0.00015579     0.00013548     0.00010633     0.00005715     0.00005715
              MO    33       MO    34       MO    35       MO    36       MO    37       MO    38       MO
  occ(*)=     0.00003220     0.00003214     0.00000331     0.00000330     0.00000249     0.00000189


 total number of electrons =   12.0000000000

 test slabel:                     1
  a  


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        a   partial gross atomic populations
   ao class       1a         2a         3a         4a         5a         6a  
     3_ s       0.000000   2.000000   0.000000   0.000000   0.988215   0.001040
     3_ p       2.000000   0.000000   2.000000   2.000000   0.000000   0.000000
     3_ d       0.000000   0.000000   0.000000   0.000000   0.001062   0.983370
     3_ f       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
 
   ao class       7a         8a         9a        10a        11a        12a  
     3_ s       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     3_ p       0.000000   0.000000   0.000000   0.000000   0.009801   0.009785
     3_ d       0.783606   0.783606   0.197744   0.197726   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000000   0.000180   0.000180
 
   ao class      13a        14a        15a        16a        17a        18a  
     3_ s       0.000000   0.000000   0.000000   0.000000   0.003402   0.000000
     3_ p       0.000000   0.005922   0.000000   0.000000   0.000000   0.000000
     3_ d       0.006499   0.000000   0.005109   0.005109   0.000000   0.000000
     3_ f       0.000000   0.000263   0.000000   0.000000   0.000000   0.003087
 
   ao class      19a        20a        21a        22a        23a        24a  
     3_ s       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     3_ p       0.000000   0.000128   0.000047   0.000047   0.000000   0.000000
     3_ d       0.000000   0.000000   0.000000   0.000000   0.001282   0.001281
     3_ f       0.003087   0.001810   0.001779   0.001778   0.000000   0.000000
 
   ao class      25a        26a        27a        28a        29a        30a  
     3_ s       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     3_ p       0.000000   0.000000   0.000155   0.000155   0.000000   0.000104
     3_ d       0.000000   0.000000   0.000000   0.000000   0.000135   0.000000
     3_ f       0.001156   0.001156   0.000001   0.000001   0.000000   0.000002
 
   ao class      31a        32a        33a        34a        35a        36a  
     3_ s       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     3_ p       0.000000   0.000000   0.000000   0.000000   0.000003   0.000003
     3_ d       0.000057   0.000057   0.000032   0.000032   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
 
   ao class      37a        38a  
     3_ s       0.000000   0.000002
     3_ p       0.000002   0.000000
     3_ d       0.000000   0.000000
     3_ f       0.000000   0.000000


                        gross atomic populations
     ao            3_
      s         2.992659
      p         6.026152
      d         2.966708
      f         0.014480
    total      12.000000
 

 Total number of electrons:   12.00000000

 DA ...
