echo of the argos input file:
 ------------------------------------------------------------------------
  
    0   1   4   4   4  20   9   0   0   0   1   0   0   0   0
   1  1  a
   0
     1    1
     3    1    1    1
     5    1    1    1    1    1
     7    1    1    1    1    1    1    1
     1    1    1
     1
     3    3    2
     0    0    1
     1    0    0
     0    1    0
     5    6    3
    -1   -1    4    0    0    0
     0    0    0    0    1    0
     0    0    0    0    0    1
     1   -1    0    0    0    0
     0    0    0    1    0    0
     7   10    4
     0    0    4    0   -9    0   -9    0    0    0
    -1    0    0    0    0   -1    0   16    0    0
     0   -1    0   -1    0    0    0    0   16    0
     0    0    0    0    1    0   -1    0    0    0
     0    0    0    0    0    0    0    0    0    1
    -1    0    0    0    0    9    0    0    0    0
     0   -1    0    9    0    0    0    0    0    0
     8    1    4
      53.4460000        0.0014860  -0.0003080   0.0028110   0.0000000
      17.0981000       -0.0365920   0.0096310  -0.0412920   0.0000000
      10.6875000        0.1321550  -0.0390430   0.1148910   0.0000000
       3.3913400       -0.5464540   0.1847170  -0.4348090   0.0000000
       0.7884750        0.8022910  -0.3584690   1.4174320   0.0000000
       0.3599820        0.4583040  -0.3208280  -0.7286190   0.0000000
       0.0777920        0.0203880   0.6560480  -1.6769590   0.0000000
       0.0302340       -0.0033540   0.5421000   1.6841700   1.0000000
     7    2    4
       9.9011600        0.0214360  -0.0058310  -0.0078290   0.0000000
       4.4181900       -0.1604130   0.0481520   0.0660080   0.0000000
       1.0931000        0.4543260  -0.1600800  -0.2113740   0.0000000
       0.5187860        0.4991190  -0.2201350  -0.3575490   0.0000000
       0.2363630        0.1777830   0.0355150   0.1703870   0.0000000
       0.0847070        0.0104040   0.6181780   0.8518400   0.0000000
       0.0311380        0.0002790   0.4720570   0.1460400   1.0000000
     6    3    3
       4.3948700       -0.0082920   0.0084760   0.0000000
       1.5708900        0.1278490  -0.1579890   0.0000000
       0.6923540        0.3219090  -0.3979000   0.0000000
       0.2904140        0.3998190  -0.2555990   0.0000000
       0.1158780        0.3194760   0.4914180   0.0000000
       0.0429250        0.1190920   0.5446950   1.0000000
     1    4    1
       0.3272000        1.0000000
     4    3
     1
    2      1.0000000      0.0000000
     2
    2      8.6365280    150.2429900
    2      3.7176390     18.7800360
     4
    2      7.6267280     33.1927910
    2      7.4532070     66.3890390
    2      3.3583890      4.6207260
    2      3.2297380      9.2602700
     4
    2      5.9380860     13.9933830
    2      5.8255440     20.9958820
    2      2.2050190      2.2851660
    2      2.2062920      3.4412600
     2
    2      4.8002150     -5.2393200
    2      4.7989920     -6.9874240
     4
    2      7.62672800    -66.38558200
    2      7.45320700     66.38903900
    2      3.35838900     -9.24145200
    2      3.22973800      9.26027000
     4
    2      5.93808600    -13.99338300
    2      5.82554400     13.99725500
    2      2.20501900     -2.28516600
    2      2.20629200      2.29417400
     2
    2      4.80021500      3.49288000
    2      4.79899200     -3.49371200
 Zr   4  112.
     0.00000000    0.00000000    0.00000000
    1   1
    2   2
    3   3
    4   4
    1
 ------------------------------------------------------------------------
                              program "argos" 5.9
                            columbus program system

             this program computes integrals over symmetry orbitals
               of generally contracted gaussian atomic orbitals.
                        programmed by russell m. pitzer

                           version date: 20-aug-2001

references:

    symmetry analysis (equal contributions):
    r. m. pitzer, j. chem. phys. 58, 3111 (1973).

    ao integral evaluation (hondo):
    m. dupuis, j. rys, and h. f. king, j. chem. phys. 65, 111 (1976).

    general contraction of gaussian orbitals:
    r. c. raffenetti, j. chem. phys. 58, 4452 (1973).

    core potential ao integrals (meldps):
    l. e. mcmurchie and e. r. davidson, j. comput. phys. 44, 289 (1981)

    spin-orbit and core potential integrals:
    r. m. pitzer and n. w. winter, int. j. quantum chem. 40, 773 (1991)

 This Version of Program ARGOS is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de

*********************** File revision status: ***********************
* argos1.f  Revision: 2.15.12.1         Date: 2013/04/11 14:37:29   * 
* argos2.f  Revision: 2.4.12.1          Date: 2013/04/11 14:37:29   * 
* argos3.f  Revision: 2.3.12.1           Date: 2013/04/11 14:37:29  * 
* argos4.f  Revision: 2.4.6.1           Date: 2013/04/11 14:37:29   * 
* argos5.f  Revision: 2.2.20.1          Date: 2013/04/11 14:37:29   * 
* argos6.f  Revision: 2.22.6.1          Date: 2013/04/11 14:37:29   * 
********************************************************************

 workspace allocation parameters: lcore=  13107200 mem1=         0 ifirst=         1

 filenames and unit numbers:
 unit description                   filename
 ---- -----------                   ----------
   6  listing file:                 argosls                                                     
   4  1-e integral file:            aoints                                                      
   8  2-e integral file [fsplit=2]: aoints2                                                     
   5  input file:                   argosin                                                     

 argos input parameters and titles:
 ngen   =  0 ns     =  1 naords =  4 ncons  =  4 ngcs   =  4 itol   = 20
 icut   =  9 aoints =  4 only1e =  0 inrm   =  0 ncrs   =  1
 l1rec  =         0      l2rec  =         0      aoint2 =  8 fsplit =  2

                                                                                
aoints SIFS file created by argos.      zam792            16:59:01.676 17-Dec-13


irrep            1
degeneracy       1
label             a


direct product table
   (  a) (  a) =   a


                     nuclear repulsion energy    0.00000000


primitive ao integrals neglected if exponential factor below 10**(-20)
contracted ao and so integrals neglected if value below 10**(- 9)
symmetry orbital integrals written on units  4  8


                                    Zr  atoms

                              nuclear charge  12.00

           center            x               y               z
             1             0.00000000      0.00000000      0.00000000

                    1s orbitals

 orbital exponents  contraction coefficients
    53.44600       1.4860000E-03  -3.0799990E-04   2.8110001E-03    0.000000    
    17.09810      -3.6592001E-02   9.6309969E-03  -4.1292002E-02    0.000000    
    10.68750       0.1321550      -3.9042987E-02   0.1148910        0.000000    
    3.391340      -0.5464540       0.1847169      -0.4348090        0.000000    
   0.7884750       0.8022910      -0.3584689        1.417432        0.000000    
   0.3599820       0.4583040      -0.3208279      -0.7286190        0.000000    
   7.7792000E-02   2.0388001E-02   0.6560478       -1.676959        0.000000    
   3.0234000E-02  -3.3540001E-03   0.5420998        1.684170        1.000000    

                     symmetry orbital labels
                     1  a1           2  a1           3  a1           4  a1

           symmetry orbitals
 ctr, ao     a1
  1, 000  1.000

                    2p orbitals

 orbital exponents  contraction coefficients
    9.901160       2.1435993E-02  -5.8310008E-03  -7.8289992E-03    0.000000    
    4.418190      -0.1604129       4.8152006E-02   6.6007994E-02    0.000000    
    1.093100       0.4543259      -0.1600800      -0.2113740        0.000000    
   0.5187860       0.4991188      -0.2201350      -0.3575490        0.000000    
   0.2363630       0.1777829       3.5515005E-02   0.1703870        0.000000    
   8.4707000E-02   1.0403997E-02   0.6181781       0.8518399        0.000000    
   3.1138000E-02   2.7899991E-04   0.4720571       0.1460400        1.000000    

                     symmetry orbital labels
                     5  a1           8  a1          11  a1          14  a1
                     6  a1           9  a1          12  a1          15  a1
                     7  a1          10  a1          13  a1          16  a1

           symmetry orbitals
 ctr, ao     a1     a1     a1
  1, 100  0.000  1.000  0.000
  1, 010  0.000  0.000  1.000
  1, 001  1.000  0.000  0.000

                    3d orbitals

 orbital exponents  contraction coefficients
    4.394870      -8.2920053E-03   8.4759984E-03    0.000000    
    1.570890       0.1278491      -0.1579890        0.000000    
   0.6923540       0.3219092      -0.3978999        0.000000    
   0.2904140       0.3998193      -0.2555990        0.000000    
   0.1158780       0.3194762       0.4914179        0.000000    
   4.2925000E-02   0.1190921       0.5446949        1.000000    

                     symmetry orbital labels
                    17  a1          22  a1          27  a1
                    18  a1          23  a1          28  a1
                    19  a1          24  a1          29  a1
                    20  a1          25  a1          30  a1
                    21  a1          26  a1          31  a1

           symmetry orbitals
 ctr, ao     a1     a1     a1     a1     a1
  1, 200 -1.000  0.000  0.000  1.000  0.000
  1, 020 -1.000  0.000  0.000 -1.000  0.000
  1, 002  2.000  0.000  0.000  0.000  0.000
  1, 110  0.000  0.000  0.000  0.000  1.000
  1, 101  0.000  1.000  0.000  0.000  0.000
  1, 011  0.000  0.000  1.000  0.000  0.000

                    4f orbitals

 orbital exponents  contraction coefficients
   0.3272000        1.000000    

                     symmetry orbital labels
                    32  a1
                    33  a1
                    34  a1
                    35  a1
                    36  a1
                    37  a1
                    38  a1

           symmetry orbitals
 ctr, ao     a1     a1     a1     a1     a1     a1     a1
  1, 300  0.000 -1.000  0.000  0.000  0.000 -1.000  0.000
  1, 030  0.000  0.000 -1.000  0.000  0.000  0.000 -1.000
  1, 003  2.000  0.000  0.000  0.000  0.000  0.000  0.000
  1, 210  0.000  0.000 -1.000  0.000  0.000  0.000  3.000
  1, 201 -3.000  0.000  0.000  1.000  0.000  0.000  0.000
  1, 120  0.000 -1.000  0.000  0.000  0.000  3.000  0.000
  1, 021 -3.000  0.000  0.000 -1.000  0.000  0.000  0.000
  1, 102  0.000  4.000  0.000  0.000  0.000  0.000  0.000
  1, 012  0.000  0.000  4.000  0.000  0.000  0.000  0.000
  1, 111  0.000  0.000  0.000  0.000  1.000  0.000  0.000


                               Zr  core potential

                                   g potential
                   powers      exponentials    coefficients
                     2           1.000000        0.000000    

                                 s - g potential
                   powers      exponentials    coefficients
                     2           8.636528        150.2430    
                     2           3.717639        18.78004    

                                 p - g potential
                   powers      exponentials    coefficients
                     2           7.626728        33.19279    
                     2           7.453207        66.38904    
                     2           3.358389        4.620726    
                     2           3.229738        9.260270    

                                 d - g potential
                   powers      exponentials    coefficients
                     2           5.938086        13.99338    
                     2           5.825544        20.99588    
                     2           2.205019        2.285166    
                     2           2.206292        3.441260    

                                 f - g potential
                   powers      exponentials    coefficients
                     2           4.800215       -5.239320    
                     2           4.798992       -6.987424    


                            Zr  spin-orbit potential

                                   p potential
                   powers      exponentials    coefficients
                     2           7.626728       -66.38558    
                     2           7.453207        66.38904    
                     2           3.358389       -9.241452    
                     2           3.229738        9.260270    

                                   d potential
                   powers      exponentials    coefficients
                     2           5.938086       -13.99338    
                     2           5.825544        13.99725    
                     2           2.205019       -2.285166    
                     2           2.206292        2.294174    

                                   f potential
                   powers      exponentials    coefficients
                     2           4.800215        3.492880    
                     2           4.798992       -3.493712    

lx:   a          ly:   a          lz:   a

output SIFS file header information:
                                                                                
aoints SIFS file created by argos.      zam792            16:59:01.676 17-Dec-13

output energy(*) values:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.

total core energy =  0.000000000000E+00

nsym = 1 nbft=  38

symmetry  =    1
slabel(*) =    a
nbpsy(*)  =   38

info(*) =         2      4096      3272      4096      2700         0

output orbital labels, i:bfnlab(i)=
   1:  1Zr_1s   2:  2Zr_1s   3:  3Zr_1s   4:  4Zr_1s   5:  5Zr_2p   6:  6Zr_2p
   7:  7Zr_2p   8:  8Zr_2p   9:  9Zr_2p  10: 10Zr_2p  11: 11Zr_2p  12: 12Zr_2p
  13: 13Zr_2p  14: 14Zr_2p  15: 15Zr_2p  16: 16Zr_2p  17: 17Zr_3d  18: 18Zr_3d
  19: 19Zr_3d  20: 20Zr_3d  21: 21Zr_3d  22: 22Zr_3d  23: 23Zr_3d  24: 24Zr_3d
  25: 25Zr_3d  26: 26Zr_3d  27: 27Zr_3d  28: 28Zr_3d  29: 29Zr_3d  30: 30Zr_3d
  31: 31Zr_3d  32: 32Zr_4f  33: 33Zr_4f  34: 34Zr_4f  35: 35Zr_4f  36: 36Zr_4f
  37: 37Zr_4f  38: 38Zr_4f

bfn_to_center map(*), i:map(i)
   1:  1   2:  1   3:  1   4:  1   5:  1   6:  1   7:  1   8:  1   9:  1  10:  1
  11:  1  12:  1  13:  1  14:  1  15:  1  16:  1  17:  1  18:  1  19:  1  20:  1
  21:  1  22:  1  23:  1  24:  1  25:  1  26:  1  27:  1  28:  1  29:  1  30:  1
  31:  1  32:  1  33:  1  34:  1  35:  1  36:  1  37:  1  38:  1

bfn_to_orbital_type map(*), i:map(i)
   1:  1   2:  1   3:  1   4:  1   5:  2   6:  2   7:  2   8:  2   9:  2  10:  2
  11:  2  12:  2  13:  2  14:  2  15:  2  16:  2  17:  4  18:  4  19:  4  20:  4
  21:  4  22:  4  23:  4  24:  4  25:  4  26:  4  27:  4  28:  4  29:  4  30:  4
  31:  4  32:  6  33:  6  34:  6  35:  6  36:  6  37:  6  38:  6


       38 symmetry orbitals,        a:  38

 socfpd: mcxu=     7875 mcxu2=     5983 left= 13099325
 
oneint:    77 S1(*)    integrals were written in  1 records.
oneint:    77 T1(*)    integrals were written in  1 records.
oneint:    77 V1(*)    integrals were written in  1 records.
oneint:    88 X(*)     integrals were written in  1 records.
oneint:    88 Y(*)     integrals were written in  1 records.
oneint:    67 Z(*)     integrals were written in  1 records.
oneint:    88 Im(px)   integrals were written in  1 records.
oneint:    88 Im(py)   integrals were written in  1 records.
oneint:    67 Im(pz)   integrals were written in  1 records.
oneint:    48 Im(lx)   integrals were written in  1 records.
oneint:    48 Im(ly)   integrals were written in  1 records.
oneint:    37 Im(lz)   integrals were written in  1 records.
oneint:   137 XX(*)    integrals were written in  1 records.
oneint:    70 XY(*)    integrals were written in  1 records.
oneint:    76 XZ(*)    integrals were written in  1 records.
oneint:   137 YY(*)    integrals were written in  1 records.
oneint:    76 YZ(*)    integrals were written in  1 records.
oneint:   101 ZZ(*)    integrals were written in  1 records.
oneint:    77 Veff(*)  integrals were written in  1 records.
oneint:    48 Im(SO:x) integrals were written in  1 records.
oneint:    48 Im(SO:y) integrals were written in  1 records.
oneint:    37 Im(SO:z) integrals were written in  1 records.
 

twoint:       30298 1/r12    integrals and       17 pk flags
                                 were written in    12 records.

 twoint: maximum mblu needed =    103486
 
driver: 1-e  integral workspace high-water mark =     34579
driver: 2-e  integral workspace high-water mark =    132510
driver: overall argos workspace high-water mark =    132510
