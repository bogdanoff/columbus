1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      2009-03.    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


================ Computing sorting integral file structure ================

                    -----z----- -----y----- -----x----- -----w----- ---total---

                CSFs       105        2520       13608        6090       22323
      internal walks       105          90          36          15         246
valid internal walks       105          90          36          15         246
 getinfoarray: info=                     6 :                     1
                  8192                  6552                  8192
                  5460                     0
 icd(3)=                  1191 ci%nnlev=                   595  l2rec=
                  8192  n2max=                  5460
 lcore1,lcore2=              13069922              13046606
 lencor,maxblo              13107200                 60000
========================================
 current settings:
 minbl3        1215
 minbl4        1215
 locmaxbl3    22684
 locmaxbuf    11342
 maxbl3       60000
 maxbl3       60000
 maxbl4       60000
 maxbuf       30006
========================================

 sorted 4-external integrals:     3 records of integral w-combinations 
                                  3 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)

 sorted 3-external integrals:     3 records of integral w-combinations 
                                  3 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)
 Orig.  diagonal integrals:  1electron:        34
                             0ext.    :        42
                             2ext.    :       336
                             4ext.    :       812


 Orig. off-diag. integrals:  4ext.    :     93177
                             3ext.    :     72576
                             2ext.    :     25578
                             1ext.    :      4704
                             0ext.    :       315
                             2ext. SO :         0
                             1ext. SO :         0
                             0ext. SO :         0
                             1electron:       406


 Sorted integrals            3ext.  w :     68040 x :     63504
                             4ext.  w :     82215 x :     71253


Cycle #  1 sortfile size=    393204(      12 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13069922
Cycle #  2 sortfile size=    393204(      12 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13069922
 minimum size of srtscr:    327670 WP (    10 records)
 maximum size of srtscr:    393204 WP (    12 records)
diagi   file:      4 records  of   1536 WP each=>       6144 WP total
ofdgi   file:     23 records  of   1536 WP each=>      35328 WP total
fil3w   file:      3 records  of  30006 WP each=>      90018 WP total
fil3x   file:      3 records  of  30006 WP each=>      90018 WP total
fil4w   file:      3 records  of  30006 WP each=>      90018 WP total
fil4x   file:      3 records  of  30006 WP each=>      90018 WP total
 compressed index vector length=                     1
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 3,
  GSET = 3,
   DAVCOR =10,
  NCOREL = 4
   nroot=1
  IVMODE = 3
  NBKITR = 1
  RTOLBK = 1e-4,1e-4,1e-4,1e-4,1e-4,1e-4,1e-4,1e-4,1e-4,
  NITER = 200
   rtolci=1e-4
  NVCIMX = 25
  NVRFMX = 25
  NVBKMX = 25
  IDEN  = 1
  CSFPRN = 10,
  update_mode=1
  nvcimn=2
  nvrfmn=2
  nvbkmn=2
   lrtshift=-0.0524795077
 /&end
 ------------------------------------------------------------------------
lodens (list->root)=  1
invlodens (root->list)=  1
 LRT calculation: diagonal shift= -5.247950770000000E-002
 bummer (warning):2:changed keyword: nbkitr=                      0
 USING SEGMENTS OF EQUAL SIZE

****************  list of control variables  ****************
 lvlprt =    0      nroot  =    1      noldv  =   0      noldhv =   0
 nunitv =    1      nbkitr =    0      niter  = 200      davcor =  10
 csfprn =   10      ivmode =    3      istrt  =   0      vout   =   0
 iortls =    0      nvbkmx =   25      ibktv  =  -1      ibkthv =  -1
 nvcimx =   25      icitv  =   -1      icithv =  -1      frcsub =   0
 nvbkmn =    2      nvcimn =    2      maxseg =   4      nrfitr =  30
 ncorel =    4      nvrfmx =   25      nvrfmn =   2      iden   =   1
 itran  =    0      froot  =    0      rtmode =   0      ncouple=   1
 skipso =    F      dalton2=    0      molcas =   0      finalv =   0
 finalw =    0      cosmocalc=   0    with_tsklst=   0
 nsegwx =    1     1     1     1
 nseg0x =    1     1     1     1
 nseg1x =    1     1     1     1
 nseg2x =    1     1     1     1
 nseg3x =    1     1     1     1
 nseg4x =    1     1     1     1
 no0ex  =      0    no1ex  =      0    no2ex  =     0    no3ex  =     0
 no4ex  =      0    nodiag =      0
 cdg4ex =    1      c3ex1ex=    1      c2ex0ex=   1
 fileloc=    0     0     0     0     0     0     0     1     1     1
 directhd=   1      noaqccshift_zyxw=      0
 critical_crit=-1.00000    critical_delta= 0.05000

 ctol   = 0.010000    lrtshift=-.052480    smalld =0.001000


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-04    1.000E-04
 Computing density:                    .drt1.state1
 using                      1  nodes and                      1  cores.
 szdg/szodg per processor=                  6144                 35328
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core           13107199 DP per process

********** Integral sort section *************


 workspace allocation information: lencor=  13107199

 echo of the input for program cisrt:
 ------------------------------------------------------------------------
  &input
  maxbl3=60000
  maxbl4=60000
  &end
 ------------------------------------------------------------------------
 
 ( 6) listing file:                    ciudgls             
 ( 5) input file:                      cisrtin   
 (17) cidrt file:                      cidrtfl             
 (11) transformed integrals file:      moints    
 (12) diagonal integral file:          diagint             
 (13) off-diagonal integral file:      ofdgint             
 (31) 4-external w integrals file:     fil4w               
 (32) 4-external x integrals file:     fil4x               
 (33) 3-external w integrals file:     fil3w               
 (34) 3-external x integrals file:     fil3x               
 (21) scratch da sorting file:         srtscr              
 (12) 2-e integral file [fsplit=2]:    moints2   

 input integral file header information:
                                                                                 
 aoints SIFS file created by argos.      zam792            16:59:01.676 17-Dec-13
  cidrt_title                                                                    
 MO-coefficients from mcscf.x                                                    
  with dummy occupation 1.0 for active orbitals                                  
  total ao core energy =    0.000000000                                          
 MCSCF energy =     -46.297255533                                                
 SIFS file created by program tran.      zam792            16:59:02.163 17-Dec-13

 input energy(*) values:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.

 total core energy =   0.000000000000E+00

 nsym = 1 nmot=  34

 symmetry  =    1
 slabel(*) =    a
 nmpsy(*)  =   34

 info(*) =          1      8192      6552      8192      5460         0

 orbital labels, i:molab(i)=
   1:tout:001   2:tout:002   3:tout:003   4:tout:004   5:tout:005   6:tout:006   7:tout:007   8:tout:008   9:tout:009  10:tout:010
  11:tout:011  12:tout:012  13:tout:013  14:tout:014  15:tout:015  16:tout:016  17:tout:017  18:tout:018  19:tout:019  20:tout:020
  21:tout:021  22:tout:022  23:tout:023  24:tout:024  25:tout:025  26:tout:026  27:tout:027  28:tout:028  29:tout:029  30:tout:030
  31:tout:031  32:tout:032  33:tout:033  34:tout:034

 input parameters:
 prnopt=  0
 ldamin=    4095 ldamax=   32767 ldainc=      64
 maxbuf=   30006 maxbl3=   60000 maxbl4=   60000 intmxo=    1536
  Using 32 bit compression 

 drt information:
  cidrt_title                                                                    
 nmotd =  38 nfctd =   4 nfvtc =   0 nmot  =  34
 nlevel =  34 niot  =   6 lowinl=  29
 orbital-to-level map(*)
   -1  -1  -1  -1  29  30  31  32  33  34   1   2   3   4   5   6   7   8   9  10
   11  12  13  14  15  16  17  18  19  20  21  22  23  24  25  26  27  28
 compressed map(*)
   29  30  31  32  33  34   1   2   3   4   5   6   7   8   9  10  11  12  13  14
   15  16  17  18  19  20  21  22  23  24  25  26  27  28
 levsym(*)
    1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1
    1   1   1   1   1   1   1   1   1   1   1   1   1   1
 repartitioning mu(*)=
   2.  0.  0.  0.  0.  0.

 new core energy added to the energy(*) list.
 from the integral file: h1_core= -4.355613553355E+01

 indxdg: diagonal integral statistics.
 total number of integrals contributing to diagonal matrix elements:      1190
 number with all external indices:       812
 number with half external - half internal indices:       336
 number with all internal indices:        42

 indxof: off-diagonal integral statistics.
    4-external integrals: num=      93177 strt=          1
    3-external integrals: num=      72576 strt=      93178
    2-external integrals: num=      25578 strt=     165754
    1-external integrals: num=       4704 strt=     191332
    0-external integrals: num=        315 strt=     196036

 total number of off-diagonal integrals:      196350


 indxof(2nd)  ittp=   3 numx(ittp)=       25578
 indxof(2nd)  ittp=   4 numx(ittp)=        4704
 indxof(2nd)  ittp=   5 numx(ittp)=         315

 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg=  12931980
 pro2e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652

 pro2e:     90013 integrals read in    17 records.

 pro2e:         0 integrals 34-ext integrals skipped.
 pro1e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652
 pro1e: eref =   -1.721733008562787E+00
 total size of srtscr:                     7  records of                  32767 
 WP =               1834952 Bytes

 new core energy added to the energy(*) list.
 from the hamiltonian repartitioning, eref= -1.721733008563E+00
 putdg        1     596    1191    1786    3322   36089   57934    3018   46706   90394
   123161  131353  136813  158652

 putf:       4 buffers of length    1536 written to file 12
 diagonal integral file completed.

 putd34:     3 records of integral w-combinations and
             3 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep4e:     3 blocks of linear combinations of 4-external integrals processed.
 number of sorted 4-external integrals     153468
 number of original 4-external integrals    93177


 putf34: external integral file complete. nfilw=    31 nfilx=    32 nrecw=     3 nrecx=     3 lbufp= 30006

 putd34:     3 records of integral w-combinations and
             3 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep3e:     6 blocks of linear combinations of 3-external integrals processed.
 number of sorted 3-external integrals     131544
 number of original 3-external integrals    72576


 putf34: external integral file complete. nfilw=    33 nfilx=    34 nrecw=     3 nrecx=     3 lbufp= 30006
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd     25578         3    165754    165754     25578    196350
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      4704         4    191332    191332      4704    196350
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd       315         5    196036    196036       315    196350

 putf:      23 buffers of length    1536 written to file 13
 off-diagonal files sort completed.
 executing brd_struct for cisrtinfo
cisrtinfo:
bufszi  1536
 diagfile 4ext:     812 2ext:     336 0ext:      42
 fil4w,fil4x  :   93177 fil3w,fil3x :   72576
 ofdgint  2ext:   25578 1ext:    4704 0ext:     315so0ext:       0so1ext:       0so2ext:       0
buffer minbl4    1215 minbl3    1215 maxbl2    1218nbas:  28   0   0   0   0   0   0   0 maxbuf 30006
 CIUDG version 5.9.7 ( 5-Oct-2004)

 workspace allocation information: lcore=  13107199

 core energy values from the integral file:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -4.355613553355E+01, ietype=    6,   fcore energy of type: H1(*)   
 energy( 3)= -1.721733008563E+00, ietype=    5,   fcore energy of type: Vref(*) 

 total core repulsion energy = -4.527786854211E+01
 nmot  =    38 niot  =     6 nfct  =     4 nfvt  =     0
 nrow  =    36 nsym  =     1 ssym  =     1 lenbuf=  1600
 nwalk,xbar:        246      105       90       36       15
 nvalwt,nvalw:      246      105       90       36       15
 ncsft:           22323
 total number of valid internal walks:     246
 nvalz,nvaly,nvalx,nvalw =      105      90      36      15

 cisrt info file parameters:
 file number  12 blocksize   1536
 mxbld   1536
 nd4ext,nd2ext,nd0ext   812   336    42
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int    93177    72576    25578     4704      315        0        0        0
 minbl4,minbl3,maxbl2  1215  1215  1218
 maxbuf 30006
 number of external orbitals per symmetry block:  28
 nmsym   1 number of internal orbitals   6
 executing brd_struct for drt
 executing brd_struct for orbinf
 executing brd_struct for momap
 calcthrxt: niot,maxw1=                     6                    66
 block size     0
 pthz,pthy,pthx,pthw:   105    90    36    15 total internal walks:     246
 maxlp3,n2lp,n1lp,n0lp    66     0     0     0
 orbsym(*)= 1 1 1 1 1 1

 setref:      105 references kept,
                0 references were marked as invalid, out of
              105 total.
 nmb.of records onel     1
 nmb.of records 2-ext    17
 nmb.of records 1-ext     4
 nmb.of records 0-ext     1
 nmb.of records 2-int     0
 nmb.of records 1-int     0
 nmb.of records 0-int     0
 ---------memory usage in DP -----------------
 < n-ex core usage >
     routines:
    fourex            62738
    threx             60409
    twoex              9922
    onex               3217
    allin              1536
    diagon             2109
               =======
   maximum            62738
 
  __ static summary __ 
   reflst               105
   hrfspc               105
               -------
   static->             210
 
  __ core required  __ 
   totstc               210
   max n-ex           62738
               -------
   totnec->           62948
 
  __ core available __ 
   totspc          13107199
   totnec -           62948
               -------
   totvec->        13044251

 number of external paths / symmetry
 vertex x     378
 vertex w     406
segment: free space=    13044251
 reducing frespc by                   964 to               13043287 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1         105|       105|         0|       105|         0|         1|
 -------------------------------------------------------------------------------
  Y 2          90|      2520|       105|        90|       105|         2|
 -------------------------------------------------------------------------------
  X 3          36|     13608|      2625|        36|       195|         3|
 -------------------------------------------------------------------------------
  W 4          15|      6090|     16233|        15|       231|         4|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=           4DP  conft+indsym=         420DP  drtbuffer=         540 DP

dimension of the ci-matrix ->>>     22323

 executing brd_struct for civct
 gentasklist: ntask=                    20
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS   
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      36     105      13608        105      36     105
     2  4   1    25      two-ext wz   2X  4 1      15     105       6090        105      15     105
     3  4   3    26      two-ext wx*  WX  4 3      15      36       6090      13608      15      36
     4  4   3    27      two-ext wx+  WX  4 3      15      36       6090      13608      15      36
     5  2   1    11      one-ext yz   1X  2 1      90     105       2520        105      90     105
     6  3   2    15      1ex3ex yx    3X  3 2      36      90      13608       2520      36      90
     7  4   2    16      1ex3ex yw    3X  4 2      15      90       6090       2520      15      90
     8  1   1     1      allint zz    OX  1 1     105     105        105        105     105     105
     9  2   2     5      0ex2ex yy    OX  2 2      90      90       2520       2520      90      90
    10  3   3     6      0ex2ex xx*   OX  3 3      36      36      13608      13608      36      36
    11  3   3    18      0ex2ex xx+   OX  3 3      36      36      13608      13608      36      36
    12  4   4     7      0ex2ex ww*   OX  4 4      15      15       6090       6090      15      15
    13  4   4    19      0ex2ex ww+   OX  4 4      15      15       6090       6090      15      15
    14  2   2    42      four-ext y   4X  2 2      90      90       2520       2520      90      90
    15  3   3    43      four-ext x   4X  3 3      36      36      13608      13608      36      36
    16  4   4    44      four-ext w   4X  4 4      15      15       6090       6090      15      15
    17  1   1    75      dg-024ext z  OX  1 1     105     105        105        105     105     105
    18  2   2    76      dg-024ext y  OX  2 2      90      90       2520       2520      90      90
    19  3   3    77      dg-024ext x  OX  3 3      36      36      13608      13608      36      36
    20  4   4    78      dg-024ext w  OX  4 4      15      15       6090       6090      15      15
----------------------------------------------------------------------------------------------------
REDTASK #   1 TIME=  19.000 N=  1 (task/type/sgbra)=(   1/24/0) (
REDTASK #   2 TIME=  18.000 N=  1 (task/type/sgbra)=(   2/25/0) (
REDTASK #   3 TIME=  17.000 N=  1 (task/type/sgbra)=(   3/26/1) (
REDTASK #   4 TIME=  16.000 N=  1 (task/type/sgbra)=(   4/27/2) (
REDTASK #   5 TIME=  15.000 N=  1 (task/type/sgbra)=(   5/11/0) (
REDTASK #   6 TIME=  14.000 N=  1 (task/type/sgbra)=(   6/15/0) (
REDTASK #   7 TIME=  13.000 N=  1 (task/type/sgbra)=(   7/16/0) (
REDTASK #   8 TIME=  12.000 N=  1 (task/type/sgbra)=(   8/ 1/0) (
REDTASK #   9 TIME=  11.000 N=  1 (task/type/sgbra)=(   9/ 5/0) (
REDTASK #  10 TIME=  10.000 N=  1 (task/type/sgbra)=(  10/ 6/1) (
REDTASK #  11 TIME=   9.000 N=  1 (task/type/sgbra)=(  11/18/2) (
REDTASK #  12 TIME=   8.000 N=  1 (task/type/sgbra)=(  12/ 7/1) (
REDTASK #  13 TIME=   7.000 N=  1 (task/type/sgbra)=(  13/19/2) (
REDTASK #  14 TIME=   6.000 N=  1 (task/type/sgbra)=(  14/42/1) (
REDTASK #  15 TIME=   5.000 N=  1 (task/type/sgbra)=(  15/43/1) (
REDTASK #  16 TIME=   4.000 N=  1 (task/type/sgbra)=(  16/44/1) (
REDTASK #  17 TIME=   3.000 N=  1 (task/type/sgbra)=(  17/75/1) (
REDTASK #  18 TIME=   2.000 N=  1 (task/type/sgbra)=(  18/76/1) (
REDTASK #  19 TIME=   1.000 N=  1 (task/type/sgbra)=(  19/77/1) (
REDTASK #  20 TIME=   0.000 N=  1 (task/type/sgbra)=(  20/78/1) (
 initializing v-file: 1:                 22323

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         975 2x:           0 4x:           0
All internal counts: zz :        6259 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 reference space has dimension     105
 dsyevx: computed roots 1 to    2(converged:   2)

    root           eigenvalues
    ----           ------------
       1         -46.3008935272
       2         -46.3008935272

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt=                   105

         vector  1 from unit 11 written to unit 49 filename cirefv              
 ncorel,neli=                     4                     4
  This is a  mraqcc energy evaluation      

    ------------------------------------------------------------

 setopt2: mraqcc energy evaluation      gvalue=  0.833333333333333      ncorel=
                     4

    ------------------------------------------------------------


    reference energies:

  reference state used is:   1with energy      -46.3008935272

  ### begin active excitation selection ###

 inactive(1:nintern)=AAAAAA
 There are        105 reference CSFs out of        105 valid zwalks.
 There are    0 inactive and    6 active orbitals.
     105 active-active  and        0 inactive excitations.

  ### end active excitation selection ###


 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:             22323
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:              25
 number of roots to converge:                             1
 number of iterations:                                  200
 residual norm convergence criteria:               0.000100

          starting ci iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:        1835 2x:         415 4x:         141
All internal counts: zz :        6259 yy:        4555 xx:        1820 ww:         390
One-external counts: yz :        5873 yx:        2550 yw:        1300
Two-external counts: yy :        1330 ww:         200 xx:         730 xz:         600 wz:         375 wx:         510
Three-ext.   counts: yx :         275 yw:         150

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1
   ht   1    -1.02302499
calca4: root=   1 anorm**2=  0.00000000 scale:  1.00000000

          calcsovref: tciref block   1

              civs   1
 refs   1    1.00000    

          calcsovref: scrb block   1

                ci   1
 civs   1    1.00000    

          calcsovref: sovref block   1

              v      1
 ref    1    1.00000    

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000
 fact= -8.746584616666664E-003
 factd= -8.746584616666664E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  1  1    -46.3008935272  5.3291E-15  6.0428E-02  2.1537E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   2

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:        1835 2x:         415 4x:         141
All internal counts: zz :        6259 yy:        4555 xx:        1820 ww:         390
One-external counts: yz :        5873 yx:        2550 yw:        1300
Two-external counts: yy :        1330 ww:         200 xx:         730 xz:         600 wz:         375 wx:         510
Three-ext.   counts: yx :         275 yw:         150

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2
   ht   1    -1.02302499
   ht   2    -0.06042799    -0.05774476
calca4: root=   1 anorm**2=  0.07546320 scale:  1.03704542
calca4: root=   2 anorm**2=  0.92453680 scale:  1.38727676

          calcsovref: tciref block   1

              civs   1       civs   2
 refs   1    1.00000      -4.286266E-14

          calcsovref: scrb block   1

                ci   1         ci   2
 civs   1   0.961528      -0.274706    
 civs   2   0.783116        2.74107    

          calcsovref: sovref block   1

              v      1       v      2
 ref    1   0.961528      -0.274706    

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.96152837    -0.27470566
 fact= -8.746584616666664E-003
 factd= -8.746584616666664E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  2  1    -46.3501090388  4.9216E-02  2.6248E-03  4.5041E-02  1.0000E-04
 mraqcc  #  2  2    -45.6979300542  4.2006E-01  0.0000E+00  4.7677E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   3

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:        1835 2x:         415 4x:         141
All internal counts: zz :        6259 yy:        4555 xx:        1820 ww:         390
One-external counts: yz :        5873 yx:        2550 yw:        1300
Two-external counts: yy :        1330 ww:         200 xx:         730 xz:         600 wz:         375 wx:         510
Three-ext.   counts: yx :         275 yw:         150

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3
   ht   1    -1.02302499
   ht   2    -0.06042799    -0.05774476
   ht   3     0.00371718    -0.00178085    -0.00253257
calca4: root=   1 anorm**2=  0.08634847 scale:  1.04228042
calca4: root=   2 anorm**2=  0.93770104 scale:  1.39201331
calca4: root=   3 anorm**2=  0.94739325 scale:  1.39549032

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3
 refs   1    1.00000      -4.286266E-14  -3.690763E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3
 civs   1   0.959730      -0.155246       0.240213    
 civs   2   0.822059        1.41332       -2.33951    
 civs   3    1.07306        12.6404        7.13741    

          calcsovref: sovref block   1

              v      1       v      2       v      3
 ref    1   0.955770      -0.201899       0.213870    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.95577002    -0.20189900     0.21387021
 fact= -8.746584616666664E-003
 factd= -8.746584616666664E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  3  1    -46.3529334911  2.8245E-03  4.1026E-04  1.7280E-02  1.0000E-04
 mraqcc  #  3  2    -45.8742218062  1.7629E-01  0.0000E+00  4.1131E-01  1.0000E-04
 mraqcc  #  3  3    -45.6418313922  3.6396E-01  0.0000E+00  4.7664E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   4

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:        1835 2x:         415 4x:         141
All internal counts: zz :        6259 yy:        4555 xx:        1820 ww:         390
One-external counts: yz :        5873 yx:        2550 yw:        1300
Two-external counts: yy :        1330 ww:         200 xx:         730 xz:         600 wz:         375 wx:         510
Three-ext.   counts: yx :         275 yw:         150

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4
   ht   1    -1.02302499
   ht   2    -0.06042799    -0.05774476
   ht   3     0.00371718    -0.00178085    -0.00253257
   ht   4    -0.00277115    -0.00180316    -0.00046415    -0.00031133
calca4: root=   1 anorm**2=  0.09144429 scale:  1.04472211
calca4: root=   2 anorm**2=  0.94680146 scale:  1.39527827
calca4: root=   3 anorm**2=  0.94677355 scale:  1.39526827
calca4: root=   4 anorm**2=  0.77963545 scale:  1.33402978

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4
 refs   1    1.00000      -4.286266E-14  -3.690763E-03   2.782004E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4
 civs   1  -0.955162       0.201885       0.241372      -4.943476E-02
 civs   2  -0.826501      -0.908640       -2.46731      -0.859743    
 civs   3   -1.20958       -10.6294        6.07682       -7.81632    
 civs   4  -0.884573       -18.6757        2.71731        31.2538    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4
 ref    1  -0.953158       0.189159       0.226504       6.636167E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1    -0.95315841     0.18915928     0.22650374     0.06636167
 fact= -8.746584616666664E-003
 factd= -8.746584616666664E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  4  1    -46.3532965252  3.6303E-04  8.2594E-05  8.2869E-03  1.0000E-04
 mraqcc  #  4  2    -46.0147276103  1.4051E-01  0.0000E+00  2.7629E-01  1.0000E-04
 mraqcc  #  4  3    -45.6433235069  1.4921E-03  0.0000E+00  4.8145E-01  1.0000E-04
 mraqcc  #  4  4    -45.4758484172  1.9798E-01  0.0000E+00  5.2858E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.002000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   5

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:        1835 2x:         415 4x:         141
All internal counts: zz :        6259 yy:        4555 xx:        1820 ww:         390
One-external counts: yz :        5873 yx:        2550 yw:        1300
Two-external counts: yy :        1330 ww:         200 xx:         730 xz:         600 wz:         375 wx:         510
Three-ext.   counts: yx :         275 yw:         150

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1    -1.02302499
   ht   2    -0.06042799    -0.05774476
   ht   3     0.00371718    -0.00178085    -0.00253257
   ht   4    -0.00277115    -0.00180316    -0.00046415    -0.00031133
   ht   5    -0.00224832     0.00024336     0.00017642     0.00005510    -0.00005234
calca4: root=   1 anorm**2=  0.09325417 scale:  1.04558795
calca4: root=   2 anorm**2=  0.95437910 scale:  1.39799110
calca4: root=   3 anorm**2=  0.93412510 scale:  1.39072826
calca4: root=   4 anorm**2=  0.97275042 scale:  1.40454634
calca4: root=   5 anorm**2=  0.47209556 scale:  1.21329945

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1    1.00000      -4.286266E-14  -3.690763E-03   2.782004E-03   2.180778E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 civs   1  -0.955606       0.142268       0.268261      -9.488643E-02  -0.155985    
 civs   2  -0.827134      -0.678048       -2.57680       0.673735      -0.431360    
 civs   3   -1.23484       -8.82407        4.59064        10.8180       -1.79362    
 civs   4   -1.04941       -20.0649        3.47672       -17.4953        25.0710    
 civs   5   0.820183        28.0015       -10.5470        57.7041        50.7410    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1  -0.952179       0.180080       0.237990      -5.764494E-02   3.103703E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1    -0.95217944     0.18008044     0.23798975    -0.05764494     0.03103703
 fact= -8.746584616666664E-003
 factd= -8.746584616666664E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  5  1    -46.3533642720  6.7747E-05  7.5432E-06  2.3627E-03  1.0000E-04
 mraqcc  #  5  2    -46.0815373345  6.6810E-02  0.0000E+00  1.2152E-01  1.0000E-04
 mraqcc  #  5  3    -45.6458862348  2.5627E-03  0.0000E+00  4.8092E-01  1.0000E-04
 mraqcc  #  5  4    -45.5435619046  6.7713E-02  0.0000E+00  5.3810E-01  1.0000E-04
 mraqcc  #  5  5    -45.4254874912  1.4762E-01  0.0000E+00  6.1530E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   6

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:        1835 2x:         415 4x:         141
All internal counts: zz :        6259 yy:        4555 xx:        1820 ww:         390
One-external counts: yz :        5873 yx:        2550 yw:        1300
Two-external counts: yy :        1330 ww:         200 xx:         730 xz:         600 wz:         375 wx:         510
Three-ext.   counts: yx :         275 yw:         150

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1    -1.02302499
   ht   2    -0.06042799    -0.05774476
   ht   3     0.00371718    -0.00178085    -0.00253257
   ht   4    -0.00277115    -0.00180316    -0.00046415    -0.00031133
   ht   5    -0.00224832     0.00024336     0.00017642     0.00005510    -0.00005234
   ht   6     0.00017892    -0.00012080     0.00002687     0.00001093    -0.00000566    -0.00000808
calca4: root=   1 anorm**2=  0.09337567 scale:  1.04564605
calca4: root=   2 anorm**2=  0.96412590 scale:  1.40147276
calca4: root=   3 anorm**2=  0.94602278 scale:  1.39499920
calca4: root=   4 anorm**2=  0.96596447 scale:  1.40212855
calca4: root=   5 anorm**2=  0.95738721 scale:  1.39906655
calca4: root=   6 anorm**2=  0.43445577 scale:  1.19768768

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1    1.00000      -4.286266E-14  -3.690763E-03   2.782004E-03   2.180778E-03  -1.689899E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1   0.955531       0.119949      -0.160580      -0.185975       0.142182      -0.177592    
 civs   2   0.827148      -0.503558        1.22485        1.98378       -1.43848      -0.218416    
 civs   3    1.23777       -7.68106        4.69933       -9.29918       -7.20594       -1.35104    
 civs   4    1.06872       -18.8567        4.30650        1.40757        21.9060        22.4745    
 civs   5  -0.916353        32.5473        17.0846       -17.2781       -45.7330        55.5475    
 civs   6   -1.05335        80.5982        214.085       -74.6653        101.847       -27.7860    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.952116       0.153196      -0.164864      -0.172800       0.112776       1.575055E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.95211583     0.15319636    -0.16486384    -0.17279988     0.11277566     0.01575055
 fact= -8.746584616666664E-003
 factd= -8.746584616666664E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  6  1    -46.3533722177  7.9457E-06  7.4911E-07  7.7739E-04  1.0000E-04
 mraqcc  #  6  2    -46.1090513028  2.7514E-02  0.0000E+00  6.7755E-02  1.0000E-04
 mraqcc  #  6  3    -45.8582839580  2.1240E-01  0.0000E+00  3.8369E-01  1.0000E-04
 mraqcc  #  6  4    -45.6100505745  6.6489E-02  0.0000E+00  4.9259E-01  1.0000E-04
 mraqcc  #  6  5    -45.4993417429  7.3854E-02  0.0000E+00  5.1334E-01  1.0000E-04
 mraqcc  #  6  6    -45.4227885115  1.4492E-01  0.0000E+00  6.2636E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   7

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:        1835 2x:         415 4x:         141
All internal counts: zz :        6259 yy:        4555 xx:        1820 ww:         390
One-external counts: yz :        5873 yx:        2550 yw:        1300
Two-external counts: yy :        1330 ww:         200 xx:         730 xz:         600 wz:         375 wx:         510
Three-ext.   counts: yx :         275 yw:         150

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1    -1.02302499
   ht   2    -0.06042799    -0.05774476
   ht   3     0.00371718    -0.00178085    -0.00253257
   ht   4    -0.00277115    -0.00180316    -0.00046415    -0.00031133
   ht   5    -0.00224832     0.00024336     0.00017642     0.00005510    -0.00005234
   ht   6     0.00017892    -0.00012080     0.00002687     0.00001093    -0.00000566    -0.00000808
   ht   7     0.00004653     0.00001363     0.00000893    -0.00000303     0.00000335     0.00000053    -0.00000083
calca4: root=   1 anorm**2=  0.09336489 scale:  1.04564090
calca4: root=   2 anorm**2=  0.96428706 scale:  1.40153026
calca4: root=   3 anorm**2=  0.93378450 scale:  1.39060580
calca4: root=   4 anorm**2=  0.81341343 scale:  1.34663040
calca4: root=   5 anorm**2=  0.87696914 scale:  1.37002523
calca4: root=   6 anorm**2=  0.63771341 scale:  1.27973177
calca4: root=   7 anorm**2=  0.79036559 scale:  1.33804544

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1    1.00000      -4.286266E-14  -3.690763E-03   2.782004E-03   2.180778E-03  -1.689899E-04  -4.597272E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1   0.955577       9.329762E-02   0.162168      -0.101412       0.223167      -9.231098E-02   0.151826    
 civs   2   0.827150      -0.404743      -0.865137        1.27467       -2.10086      -0.768498      -0.550486    
 civs   3    1.23801       -6.63321       -6.83451       -3.65233        5.11267       -6.63260       -7.20734    
 civs   4    1.07035       -17.6074       -7.25426        4.63910        6.45134        30.7266        3.44581    
 civs   5  -0.924499        33.3878       -9.12972       -6.63303       -5.31527        14.3760       -75.6402    
 civs   6   -1.14258        101.834       -143.541        130.033        143.490        2.22441        24.5936    
 civs   7   0.898579       -168.465        443.980        434.047        212.264       -175.515       -422.681    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.952121       0.132143       0.151147      -0.131420       0.176647       5.669378E-02   3.833420E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.95212069     0.13214268     0.15114707    -0.13142024     0.17664681     0.05669378     0.03833420
 fact= -8.746584616666664E-003
 factd= -8.746584616666664E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  7  1    -46.3533728908  6.7314E-07  1.4226E-07  2.9879E-04  1.0000E-04
 mraqcc  #  7  2    -46.1219280551  1.2877E-02  0.0000E+00  3.2830E-02  1.0000E-04
 mraqcc  #  7  3    -45.9621308014  1.0385E-01  0.0000E+00  2.9023E-01  1.0000E-04
 mraqcc  #  7  4    -45.7242562094  1.1421E-01  0.0000E+00  4.5579E-01  1.0000E-04
 mraqcc  #  7  5    -45.5763554222  7.7014E-02  0.0000E+00  5.1931E-01  1.0000E-04
 mraqcc  #  7  6    -45.4369292352  1.4141E-02  0.0000E+00  5.5604E-01  1.0000E-04
 mraqcc  #  7  7    -45.3717852141  9.3917E-02  0.0000E+00  6.4974E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.002000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   8

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:        1835 2x:         415 4x:         141
All internal counts: zz :        6259 yy:        4555 xx:        1820 ww:         390
One-external counts: yz :        5873 yx:        2550 yw:        1300
Two-external counts: yy :        1330 ww:         200 xx:         730 xz:         600 wz:         375 wx:         510
Three-ext.   counts: yx :         275 yw:         150

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1    -1.02302499
   ht   2    -0.06042799    -0.05774476
   ht   3     0.00371718    -0.00178085    -0.00253257
   ht   4    -0.00277115    -0.00180316    -0.00046415    -0.00031133
   ht   5    -0.00224832     0.00024336     0.00017642     0.00005510    -0.00005234
   ht   6     0.00017892    -0.00012080     0.00002687     0.00001093    -0.00000566    -0.00000808
   ht   7     0.00004653     0.00001363     0.00000893    -0.00000303     0.00000335     0.00000053    -0.00000083
   ht   8     0.00006000     0.00001251     0.00000188     0.00000074     0.00000082     0.00000044     0.00000004    -0.00000034
calca4: root=   1 anorm**2=  0.09334697 scale:  1.04563233
calca4: root=   2 anorm**2=  0.91189890 scale:  1.38271432
calca4: root=   3 anorm**2=  0.78483928 scale:  1.33597877
calca4: root=   4 anorm**2=  0.46797289 scale:  1.21159931
calca4: root=   5 anorm**2=  0.87208413 scale:  1.36824126
calca4: root=   6 anorm**2=  0.88842171 scale:  1.37419857
calca4: root=   7 anorm**2=  0.64763233 scale:  1.28360131
calca4: root=   8 anorm**2=  0.85144128 scale:  1.36067677

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1    1.00000      -4.286266E-14  -3.690763E-03   2.782004E-03   2.180778E-03  -1.689899E-04  -4.597272E-05  -5.905554E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1  -0.955641      -2.816053E-02   0.200376      -3.483251E-02  -0.103523       0.236045      -8.830966E-02  -0.130703    
 civs   2  -0.827151       0.264685      -0.647928       0.656017        1.41961       -2.01641      -0.782413       0.495512    
 civs   3   -1.23805        4.99198       -7.00593        3.87275       -3.57290        4.90162       -6.70017        7.63241    
 civs   4   -1.07064        14.6556       -12.0731        1.38364        4.44501        6.59826        30.8144       -2.55037    
 civs   5   0.925938       -31.5015        7.17521        9.53495       -6.35563       -8.54123        13.2912        77.0753    
 civs   6    1.15834       -115.209       -55.1636        99.1507        137.923        160.830        4.29589       -2.00157    
 civs   7   -1.05729        274.557        307.307       -328.169        398.194        199.901       -179.868        448.841    
 civs   8  -0.835793        462.288        878.474        1035.69        207.519        231.515        16.3829        269.146    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.952129      -9.496413E-02   0.151609      -8.731463E-02  -0.145700       0.167643       5.770547E-02  -3.407443E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.95212872    -0.09496413     0.15160918    -0.08731463    -0.14569967     0.16764290     0.05770547    -0.03407443
 fact= -8.746584616666664E-003
 factd= -8.746584616666664E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  8  1    -46.3533730097  1.1890E-07  2.6650E-08  1.3443E-04  1.0000E-04
 mraqcc  #  8  2    -46.1359004038  1.3972E-02  0.0000E+00  6.9610E-02  1.0000E-04
 mraqcc  #  8  3    -46.0523347956  9.0204E-02  0.0000E+00  2.4763E-01  1.0000E-04
 mraqcc  #  8  4    -45.8474287159  1.2317E-01  0.0000E+00  5.1594E-01  1.0000E-04
 mraqcc  #  8  5    -45.7203560862  1.4400E-01  0.0000E+00  4.3925E-01  1.0000E-04
 mraqcc  #  8  6    -45.5673192682  1.3039E-01  0.0000E+00  5.1136E-01  1.0000E-04
 mraqcc  #  8  7    -45.4368597444  6.5075E-02  0.0000E+00  5.5437E-01  1.0000E-04
 mraqcc  #  8  8    -45.3539892956  7.6121E-02  0.0000E+00  6.5028E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.001999
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   9

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:        1835 2x:         415 4x:         141
All internal counts: zz :        6259 yy:        4555 xx:        1820 ww:         390
One-external counts: yz :        5873 yx:        2550 yw:        1300
Two-external counts: yy :        1330 ww:         200 xx:         730 xz:         600 wz:         375 wx:         510
Three-ext.   counts: yx :         275 yw:         150

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1    -1.02302499
   ht   2    -0.06042799    -0.05774476
   ht   3     0.00371718    -0.00178085    -0.00253257
   ht   4    -0.00277115    -0.00180316    -0.00046415    -0.00031133
   ht   5    -0.00224832     0.00024336     0.00017642     0.00005510    -0.00005234
   ht   6     0.00017892    -0.00012080     0.00002687     0.00001093    -0.00000566    -0.00000808
   ht   7     0.00004653     0.00001363     0.00000893    -0.00000303     0.00000335     0.00000053    -0.00000083
   ht   8     0.00006000     0.00001251     0.00000188     0.00000074     0.00000082     0.00000044     0.00000004    -0.00000034
   ht   9    -0.00003059    -0.00000855     0.00000157    -0.00000134    -0.00000037     0.00000002     0.00000010    -0.00000003

                ht   9
   ht   9    -0.00000007
calca4: root=   1 anorm**2=  0.09333337 scale:  1.04562583
calca4: root=   2 anorm**2=  0.75610982 scale:  1.32518294
calca4: root=   3 anorm**2=  0.84558376 scale:  1.35852264
calca4: root=   4 anorm**2=  0.36364891 scale:  1.16775379
calca4: root=   5 anorm**2=  0.62301499 scale:  1.27397605
calca4: root=   6 anorm**2=  0.89736913 scale:  1.37745023
calca4: root=   7 anorm**2=  0.92816356 scale:  1.38858329
calca4: root=   8 anorm**2=  0.56253149 scale:  1.25001259
calca4: root=   9 anorm**2=  0.94769668 scale:  1.39559904

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1    1.00000      -4.286266E-14  -3.690763E-03   2.782004E-03   2.180778E-03  -1.689899E-04  -4.597272E-05  -5.905554E-05

              civs   9
 refs   1   3.020881E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1   0.955688      -8.523009E-02   0.225463       2.508401E-03  -6.980519E-03   0.126596       0.206837       0.151961    
 civs   2   0.827150      -7.785417E-02  -0.587315      -0.285519       0.798430       -1.39224       -2.12902       6.340782E-02
 civs   3    1.23806       -2.76371       -7.14563       -3.70054        5.13989        4.82310        2.90655       0.517347    
 civs   4    1.07070       -8.77179       -16.8213       -1.60405       0.191334       -5.66023        11.9215       -21.9103    
 civs   5  -0.926245        24.1296        21.1402       -1.36016        10.5302        7.60687       -10.1354       -60.5326    
 civs   6   -1.16170        105.084        22.9087       -62.9410        134.690       -115.567        155.161        8.58415    
 civs   7    1.09118       -345.532        142.462        70.7761       -288.166       -401.840        186.800       -65.3163    
 civs   8    1.01425       -758.193        555.394        589.743        1026.36       -109.108        168.295       -241.495    
 civs   9  -0.952569        1346.32       -1476.61        2328.91       -1002.43       -425.462        428.678        751.935    

                ci   9
 civs   1   5.346016E-02
 civs   2  -0.568503    
 civs   3   -9.96492    
 civs   4    20.1211    
 civs   5   -49.0668    
 civs   6   -6.44581    
 civs   7   -557.175    
 civs   8   -101.085    
 civs   9   -1120.47    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.952135       3.676147E-02   0.163315       5.164613E-02  -0.102862       0.141231       0.175375      -4.382826E-03

              v      9
 ref    1   3.803732E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.95213466     0.03676147     0.16331456     0.05164613    -0.10286237     0.14123104     0.17537472    -0.00438283

                ci   9
 ref:   1     0.03803732
 fact= -8.746584616666664E-003
 factd= -8.746584616666664E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  9  1    -46.3533730351  2.5386E-08  7.6965E-09  7.3765E-05  1.0000E-04
 mraqcc  #  9  2    -46.1558714443  1.9971E-02  0.0000E+00  1.1272E-01  1.0000E-04
 mraqcc  #  9  3    -46.0947795304  4.2445E-02  0.0000E+00  1.5566E-01  1.0000E-04
 mraqcc  #  9  4    -45.9632738589  1.1585E-01  0.0000E+00  4.8117E-01  1.0000E-04
 mraqcc  #  9  5    -45.8294556763  1.0910E-01  0.0000E+00  4.6668E-01  1.0000E-04
 mraqcc  #  9  6    -45.7152691559  1.4795E-01  0.0000E+00  4.2494E-01  1.0000E-04
 mraqcc  #  9  7    -45.5570130454  1.2015E-01  0.0000E+00  5.0493E-01  1.0000E-04
 mraqcc  #  9  8    -45.3799479242  2.5959E-02  0.0000E+00  6.1601E-01  1.0000E-04
 mraqcc  #  9  9    -45.3221302589  4.4262E-02  0.0000E+00  6.0188E-01  1.0000E-04
 
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001999
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mraqcc   convergence criteria satisfied after  9 iterations.

 final mraqcc   convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  9  1    -46.3533730351  2.5386E-08  7.6965E-09  7.3765E-05  1.0000E-04
 mraqcc  #  9  2    -46.1558714443  1.9971E-02  0.0000E+00  1.1272E-01  1.0000E-04
 mraqcc  #  9  3    -46.0947795304  4.2445E-02  0.0000E+00  1.5566E-01  1.0000E-04
 mraqcc  #  9  4    -45.9632738589  1.1585E-01  0.0000E+00  4.8117E-01  1.0000E-04
 mraqcc  #  9  5    -45.8294556763  1.0910E-01  0.0000E+00  4.6668E-01  1.0000E-04
 mraqcc  #  9  6    -45.7152691559  1.4795E-01  0.0000E+00  4.2494E-01  1.0000E-04
 mraqcc  #  9  7    -45.5570130454  1.2015E-01  0.0000E+00  5.0493E-01  1.0000E-04
 mraqcc  #  9  8    -45.3799479242  2.5959E-02  0.0000E+00  6.1601E-01  1.0000E-04
 mraqcc  #  9  9    -45.3221302589  4.4262E-02  0.0000E+00  6.0188E-01  1.0000E-04

####################CIUDGINFO####################

   aqcc(lrt) vector at position   1 energy=  -46.353373035071

################END OF CIUDGINFO################

 
 a4den factor for root 1  =  1.015801361
diagon:itrnv=   0
    1 of the  10 expansion vectors are transformed.
    1 of the   9 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
maximum overlap with reference    1(overlap= 0.95213)
reference energy =                         -46.3008935272
total aqcc energy =                        -46.3533730351
diagonal element shift (lrtshift) =         -0.0524795079

 information on vector: 1 from unit 11 written to unit 48 filename civout              


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -46.3533730351

                                                       internal orbitals

                                          level       1    2    3    4    5    6

                                          orbital     5    6    7    8    9   10

                                         symmetry   a    a    a    a    a    a  

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  3  1       2 -0.576880                        +-   +         +            
 z*  3  1       3  0.332709                        +-   +              +       
 z*  3  1       5 -0.332444                        +-        +    +            
 z*  3  1       6 -0.575587                        +-        +         +       
 z*  3  1      12  0.021636                        +    +-        +            
 z*  3  1      13 -0.037490                        +    +-             +       
 z*  3  1      15 -0.026503                        +     -   +    +            
 z*  3  1      16 -0.015281                        +     -   +         +       
 z*  3  1      19  0.028034                        +     -        +         +  
 z*  3  1      20 -0.016201                        +     -             +    +  
 z*  3  1      22 -0.046008                        +    +     -   +            
 z*  3  1      23 -0.026507                        +    +     -        +       
 z*  3  1      30  0.036872                        +    +          -        +  
 z*  3  1      32 -0.026090                        +    +         +          - 
 z*  3  1      34 -0.021301                        +    +               -   +  
 z*  3  1      35  0.015085                        +    +              +     - 
 z*  3  1      37 -0.021671                        +         +-   +            
 z*  3  1      38  0.037467                        +         +-        +       
 z*  3  1      41  0.016157                        +          -   +         +  
 z*  3  1      42  0.028030                        +          -        +    +  
 z*  3  1      45  0.021280                        +         +     -        +  
 z*  3  1      47 -0.015056                        +         +    +          - 
 z*  3  1      49  0.036906                        +         +          -   +  
 z*  3  1      50 -0.026134                        +         +         +     - 
 z*  3  1      61  0.012116                             +-   +    +            
 z*  3  1      62  0.020980                             +-   +         +       
 z*  3  1      67 -0.021036                             +    +-   +            
 z*  3  1      68  0.012134                             +    +-        +       
 z*  3  1      82  0.017640                             +         +-   +       
 z*  3  1      85  0.030587                             +         +    +-      
 z*  3  1      88  0.030587                             +         +         +- 
 z*  3  1      90 -0.017638                             +              +    +- 
 z*  3  1      94 -0.030525                                  +    +-   +       
 z*  3  1      97  0.017631                                  +    +    +-      
 z*  3  1     100  0.017636                                  +    +         +- 
 z*  3  1     102  0.030530                                  +         +    +- 
 y   3  1     285  0.022005             12( a  )    -   +         +            
 y   3  1     313 -0.012691             12( a  )    -   +              +       
 y   3  1     369  0.012680             12( a  )    -        +    +            
 y   3  1     397  0.021953             12( a  )    -        +         +       
 y   3  1     593  0.017875             12( a  )   +     -        +            
 y   3  1     621 -0.010309             12( a  )   +     -             +       
 y   3  1     705 -0.012640             12( a  )   +    +          -           
 y   3  1     817  0.010304             12( a  )   +          -   +            
 y   3  1     845  0.017840             12( a  )   +          -        +       
 y   3  1     929 -0.012617             12( a  )   +         +          -      
 y   3  1    2155  0.010246              6( a  )             +     -   +       
 y   3  1    2164 -0.010727             15( a  )             +     -   +       
 x   3  1    2730 -0.015537   14( a  )  15( a  )   +-                          
 x   3  1    2743 -0.015504   13( a  )  16( a  )   +-                          
 x   3  1    2924  0.012563   23( a  )  25( a  )   +-                          
 x   3  1    2949  0.012561   24( a  )  26( a  )   +-                          
 x   3  1    2971  0.013818   21( a  )  27( a  )   +-                          
 x   3  1    3064  0.014301    6( a  )  12( a  )    -   +                      
 x   3  1    3106  0.016996   12( a  )  15( a  )    -   +                      
 x   3  1    3265 -0.010012    9( a  )  24( a  )    -   +                      
 x   3  1    3282  0.011998    3( a  )  25( a  )    -   +                      
 x   3  1    3290 -0.012775   11( a  )  25( a  )    -   +                      
 x   3  1    3443  0.014271    7( a  )  12( a  )    -        +                 
 x   3  1    3498  0.016961   12( a  )  16( a  )    -        +                 
 x   3  1    3684 -0.011972    3( a  )  26( a  )    -        +                 
 x   3  1    3692  0.012748   11( a  )  26( a  )    -        +                 
 x   3  1    3818  0.014241    4( a  )  12( a  )    -             +            
 x   3  1    3849  0.017001   12( a  )  14( a  )    -             +            
 x   3  1    3951  0.012256    2( a  )  21( a  )    -             +            
 x   3  1    3958  0.012928    9( a  )  21( a  )    -             +            
 x   3  1    3970 -0.013171    1( a  )  22( a  )    -             +            
 x   3  1    3979  0.013990   10( a  )  22( a  )    -             +            
 x   3  1    4001  0.010213   11( a  )  23( a  )    -             +            
 x   3  1    4197 -0.014210    5( a  )  12( a  )    -                  +       
 x   3  1    4215  0.016966   12( a  )  13( a  )    -                  +       
 x   3  1    4328 -0.012231    1( a  )  21( a  )    -                  +       
 x   3  1    4337  0.012901   10( a  )  21( a  )    -                  +       
 x   3  1    4349 -0.013152    2( a  )  22( a  )    -                  +       
 x   3  1    4356 -0.013969    9( a  )  22( a  )    -                  +       
 x   3  1    4401 -0.010193   11( a  )  24( a  )    -                  +       
 x   3  1    7918  0.011879    1( a  )   2( a  )         -             +       
 x   3  1   10564 -0.011900    1( a  )   2( a  )              -   +            
 w   3  1   16237 -0.021389    1( a  )   3( a  )   +    +                      
 w   3  1   16238 -0.037192    2( a  )   3( a  )   +    +                      
 w   3  1   16272 -0.027414    3( a  )   9( a  )   +    +                      
 w   3  1   16281  0.015825    3( a  )  10( a  )   +    +                      
 w   3  1   16289  0.015899    1( a  )  11( a  )   +    +                      
 w   3  1   16290  0.027645    2( a  )  11( a  )   +    +                      
 w   3  1   16297  0.021641    9( a  )  11( a  )   +    +                      
 w   3  1   16298 -0.012492   10( a  )  11( a  )   +    +                      
 w   3  1   16544  0.010120   11( a  )  25( a  )   +    +                      
 w   3  1   16643  0.037115    1( a  )   3( a  )   +         +                 
 w   3  1   16644 -0.021376    2( a  )   3( a  )   +         +                 
 w   3  1   16678 -0.015816    3( a  )   9( a  )   +         +                 
 w   3  1   16687 -0.027358    3( a  )  10( a  )   +         +                 
 w   3  1   16695 -0.027588    1( a  )  11( a  )   +         +                 
 w   3  1   16696  0.015889    2( a  )  11( a  )   +         +                 
 w   3  1   16703  0.012485    9( a  )  11( a  )   +         +                 
 w   3  1   16704  0.021597   10( a  )  11( a  )   +         +                 
 w   3  1   16975 -0.010098   11( a  )  26( a  )   +         +                 
 w   3  1   17046 -0.015082    1( a  )   1( a  )   +              +            
 w   3  1   17047 -0.037213    1( a  )   2( a  )   +              +            
 w   3  1   17048  0.015086    2( a  )   2( a  )   +              +            
 w   3  1   17082 -0.027532    1( a  )   9( a  )   +              +            
 w   3  1   17083  0.015845    2( a  )   9( a  )   +              +            
 w   3  1   17091  0.015840    1( a  )  10( a  )   +              +            
 w   3  1   17092  0.027533    2( a  )  10( a  )   +              +            
 w   3  1   17099  0.021633    9( a  )  10( a  )   +              +            
 w   3  1   17277  0.011646    1( a  )  22( a  )   +              +            
 w   3  1   17286 -0.012462   10( a  )  22( a  )   +              +            
 w   3  1   17452 -0.026274    1( a  )   1( a  )   +                   +       
 w   3  1   17453  0.021347    1( a  )   2( a  )   +                   +       
 w   3  1   17454  0.026242    2( a  )   2( a  )   +                   +       
 w   3  1   17488  0.015853    1( a  )   9( a  )   +                   +       
 w   3  1   17489  0.027458    2( a  )   9( a  )   +                   +       
 w   3  1   17496  0.015255    9( a  )   9( a  )   +                   +       
 w   3  1   17497  0.027492    1( a  )  10( a  )   +                   +       
 w   3  1   17498 -0.015853    2( a  )  10( a  )   +                   +       
 w   3  1   17505 -0.012503    9( a  )  10( a  )   +                   +       
 w   3  1   17506 -0.015274   10( a  )  10( a  )   +                   +       
 w   3  1   17684  0.011631    2( a  )  22( a  )   +                   +       
 w   3  1   17691  0.012446    9( a  )  22( a  )   +                   +       
 w   3  1   18670  0.060426    1( a  )   1( a  )        +         +            
 w   3  1   18672  0.052761    2( a  )   2( a  )        +         +            
 w   3  1   18675  0.060332    3( a  )   3( a  )        +         +            
 w   3  1   18707  0.042077    2( a  )   9( a  )        +         +            
 w   3  1   18714  0.019732    9( a  )   9( a  )        +         +            
 w   3  1   18715 -0.048598    1( a  )  10( a  )        +         +            
 w   3  1   18724  0.022668   10( a  )  10( a  )        +         +            
 w   3  1   18727 -0.048315    3( a  )  11( a  )        +         +            
 w   3  1   18735  0.022489   11( a  )  11( a  )        +         +            
 w   3  1   18747  0.016136   12( a  )  12( a  )        +         +            
 w   3  1   19076 -0.030442    1( a  )   1( a  )        +              +       
 w   3  1   19078 -0.034837    2( a  )   2( a  )        +              +       
 w   3  1   19081 -0.034796    3( a  )   3( a  )        +              +       
 w   3  1   19113 -0.028027    2( a  )   9( a  )        +              +       
 w   3  1   19120 -0.013075    9( a  )   9( a  )        +              +       
 w   3  1   19121  0.024273    1( a  )  10( a  )        +              +       
 w   3  1   19130 -0.011378   10( a  )  10( a  )        +              +       
 w   3  1   19133  0.027865    3( a  )  11( a  )        +              +       
 w   3  1   19141 -0.012970   11( a  )  11( a  )        +              +       
 w   3  1   19888  0.034816    1( a  )   1( a  )             +    +            
 w   3  1   19890  0.030422    2( a  )   2( a  )             +    +            
 w   3  1   19893  0.034778    3( a  )   3( a  )             +    +            
 w   3  1   19925  0.024256    2( a  )   9( a  )             +    +            
 w   3  1   19932  0.011371    9( a  )   9( a  )             +    +            
 w   3  1   19933 -0.028009    1( a  )  10( a  )             +    +            
 w   3  1   19942  0.013067   10( a  )  10( a  )             +    +            
 w   3  1   19945 -0.027849    3( a  )  11( a  )             +    +            
 w   3  1   19953  0.012963   11( a  )  11( a  )             +    +            
 w   3  1   20294  0.052654    1( a  )   1( a  )             +         +       
 w   3  1   20296  0.060299    2( a  )   2( a  )             +         +       
 w   3  1   20299  0.060214    3( a  )   3( a  )             +         +       
 w   3  1   20331  0.048494    2( a  )   9( a  )             +         +       
 w   3  1   20338  0.022621    9( a  )   9( a  )             +         +       
 w   3  1   20339 -0.041990    1( a  )  10( a  )             +         +       
 w   3  1   20348  0.019692   10( a  )  10( a  )             +         +       
 w   3  1   20351 -0.048218    3( a  )  11( a  )             +         +       
 w   3  1   20359  0.022444   11( a  )  11( a  )             +         +       
 w   3  1   20371  0.016105   12( a  )  12( a  )             +         +       

 ci coefficient statistics:
           rq > 0.1                4
      0.1> rq > 0.01             152
     0.01> rq > 0.001           1149
    0.001> rq > 0.0001          2227
   0.0001> rq > 0.00001         1168
  0.00001> rq > 0.000001         438
 0.000001> rq                  17185
           all                 22323
  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.000000075908
     2     2     -0.576880019544
     3     3      0.332708643117
     4     4     -0.000010692548
     5     5     -0.332443778414
     6     6     -0.575586666836
     7     7      0.000011630209
     8     8     -0.000000222738
     9     9     -0.000184679079
    10    10     -0.001012107307
    11    11     -0.000000851777
    12    12      0.021635875892
    13    13     -0.037490237253
    14    14      0.000000039218
    15    15     -0.026502605973
    16    16     -0.015281207764
    17    17      0.000000006640
    18    18     -0.000000036366
    19    19      0.028034434166
    20    20     -0.016200714697
    21    21     -0.000000782176
    22    22     -0.046008470473
    23    23     -0.026507434102
    24    24      0.000000007833
    25    25     -0.000078133723
    26    26     -0.000031910821
    27    27     -0.000000001324
    28    28      0.000000389174
    29    29      0.000000054095
    30    30      0.036871516270
    31    31     -0.000000025896
    32    32     -0.026089942417
    33    33      0.000000392285
    34    34     -0.021300728916
    35    35      0.015085162613
    36    36      0.000000000689
    37    37     -0.021670515404
    38    38      0.037467461693
    39    39     -0.000000013719
    40    40     -0.000000032785
    41    41      0.016157453838
    42    42      0.028030498113
    43    43     -0.000000420508
    44    44      0.000000046214
    45    45      0.021280224213
    46    46     -0.000000030101
    47    47     -0.015055812169
    48    48     -0.000000431993
    49    49      0.036905915618
    50    50     -0.026133743480
    51    51      0.000000000294
    52    52      0.000041244662
    53    53      0.000000724446
    54    54     -0.000000480073
    55    55      0.000001418812
    56    56     -0.000000828924
    57    57      0.000000002352
    58    58     -0.000036480922
    59    59     -0.000000750712
    60    60      0.000018958411
    61    61      0.012115558725
    62    62      0.020980320429
    63    63     -0.000000549076
    64    64      0.000000011752
    65    65      0.003179658371
    66    66     -0.005453004809
    67    67     -0.021035703133
    68    68      0.012134299082
    69    69     -0.000000504878
    70    70     -0.000000005592
    71    71     -0.007785644170
    72    72     -0.004498128693
    73    73     -0.000000102934
    74    74     -0.000000085048
    75    75      0.000001491649
    76    76     -0.000000127123
    77    77      0.000011516290
    78    78      0.000000095423
    79    79     -0.000003787747
    80    80      0.000010409947
    81    81     -0.000000003275
    82    82      0.017640351216
    83    83     -0.000000509417
    84    84     -0.000000131813
    85    85      0.030587475299
    86    86      0.000000077573
    87    87      0.000000000016
    88    88      0.030587140197
    89    89     -0.000000509071
    90    90     -0.017638499882
    91    91      0.000000009660
    92    92     -0.003166366084
    93    93      0.005559554263
    94    94     -0.030525069085
    95    95      0.000000555087
    96    96     -0.000000121550
    97    97      0.017630940644
    98    98      0.000000070500
    99    99     -0.000000000130
   100   100      0.017635544572
   101   101      0.000000552810
   102   102      0.030529962222
   103   103      0.000038075500
   104   104     -0.000011642513
   105   105      0.000000010323

 number of reference csfs (nref) is   105.  root number (iroot) is  1.
 c0**2 =   0.90666663  c0**2(renormalized) =   0.99128888
 c**2 (all zwalks) =   0.90666663  c**2(all zwalks, renormalized) =   0.99128888
 passed aftci ... 
 readint2: molcas,dalton2=                     0                     0
 files%faoints=aoints              
lodens (list->root)=  1
sifcfg setup: record length 4096 DP
# d1 elements per record  3272
# d2 elements per record  2730
  The AQCC density will be calculated.
 item #                     1 suffix=:.drt1.state1:
 read_civout: repnuc=  -45.2778685421096     
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method: 30 last record  1max overlap with ref# 95% root-following 0
 MR-CISD energy:   -46.35337304    -1.07550449
 residuum:     0.00007377
 deltae:     0.00000003
 apxde:     0.00000001
 a4den:     1.01580136
 reference energy:   -46.30089353    -1.02302499

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.95213466     0.03676147     0.16331456     0.05164613    -0.10286237     0.14123104     0.17537472    -0.00438283

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1     0.03803732     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.95213466     0.03676147     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 rdcirefv: extracting reference vector # 1(blocksize=   105 #zcsf=     105)
 =========== Executing IN-CORE method ==========
================================================================================
   DYZ=       0  DYX=       0  DYW=       0
   D0Z=     906  D0Y=       0  D0X=       0  D0W=       0
  DDZI=     395 DDYI=       0 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
 root #                      1 : Scaling(1) with    1.01580136093376      
 corresponding to ref #                      1
 computing final density
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=     525  DYX=     275  DYW=     150
   D0Z=     906  D0Y=     760  D0X=     220  D0W=      60
  DDZI=     395 DDYI=     285 DDXI=      90 DDWI=      40
  DDZE=       0 DDYE=      90 DDXE=      36 DDWE=      15
================================================================================
Trace of MO density:     4.000000
    4  correlated and     8  frozen core electrons

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   4     0.00000000     0.00000000     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.00000000     0.00000000     1.82475207    -0.00003908     0.00008034    -0.00000019
   MO   6     0.00000000     0.00000000     0.00000000     0.00000000    -0.00003908     0.49685128     0.00029920    -0.00000002
   MO   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00008034     0.00029920     0.49504338     0.00000008
   MO   8     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000019    -0.00000002     0.00000008     0.49402317
   MO   9     0.00000000     0.00000000     0.00000000     0.00000000     0.00000086    -0.00000009    -0.00000010    -0.00063303
   MO  10     0.00000000     0.00000000     0.00000000     0.00000000     0.03732460     0.00025542    -0.00069833     0.00000248
   MO  11     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  12     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  13     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  14     0.00000000     0.00000000     0.00000000     0.00000000     0.00001135    -0.00982926     0.00006488     0.00000027
   MO  15     0.00000000     0.00000000     0.00000000     0.00000000    -0.00002215     0.00007365     0.00980201    -0.00000005
   MO  16     0.00000000     0.00000000     0.00000000     0.00000000     0.00000003     0.00000030     0.00000005     0.00940722
   MO  17     0.00000000     0.00000000     0.00000000     0.00000000     0.00000025     0.00000000    -0.00000002    -0.00000824
   MO  18     0.00000000     0.00000000     0.00000000     0.00000000     0.01094751     0.00000232     0.00002947    -0.00000004
   MO  19     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  20     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  21     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  22     0.00000000     0.00000000     0.00000000     0.00000000    -0.05017215     0.00000100    -0.00000044     0.00000000
   MO  23     0.00000000     0.00000000     0.00000000     0.00000000     0.00006642    -0.00004096     0.01018403     0.00000002
   MO  24     0.00000000     0.00000000     0.00000000     0.00000000    -0.00001296     0.01021056     0.00004927    -0.00000004
   MO  25     0.00000000     0.00000000     0.00000000     0.00000000     0.00000002    -0.00000006     0.00000002    -0.00960743
   MO  26     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000032    -0.00000001     0.00000004    -0.00002738
   MO  27     0.00000000     0.00000000     0.00000000     0.00000000     0.01204613     0.00000255    -0.00004624     0.00000000
   MO  28     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  29     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  30     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  31     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  32     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  33     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  34     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  35     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  36     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  37     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  38     0.00000000     0.00000000     0.00000000     0.00000000     0.00021943    -0.00000002    -0.00000003     0.00000000

                MO   9         MO  10         MO  11         MO  12         MO  13         MO  14         MO  15         MO  16
   MO   5     0.00000086     0.03732460     0.00000000     0.00000000     0.00000000     0.00001135    -0.00002215     0.00000003
   MO   6    -0.00000009     0.00025542     0.00000000     0.00000000     0.00000000    -0.00982926     0.00007365     0.00000030
   MO   7    -0.00000010    -0.00069833     0.00000000     0.00000000     0.00000000     0.00006488     0.00980201     0.00000005
   MO   8    -0.00063303     0.00000248     0.00000000     0.00000000     0.00000000     0.00000027    -0.00000005     0.00940722
   MO   9     0.49259700    -0.00001106     0.00000000     0.00000000     0.00000000     0.00000001     0.00000001    -0.00001094
   MO  10    -0.00001106     0.01583462     0.00000000     0.00000000     0.00000000    -0.00000370    -0.00001997     0.00000006
   MO  11     0.00000000     0.00000000     0.03455523    -0.00000024    -0.00000001     0.00000000     0.00000000     0.00000000
   MO  12     0.00000000     0.00000000    -0.00000024     0.03455412    -0.00000001     0.00000000     0.00000000     0.00000000
   MO  13     0.00000000     0.00000000    -0.00000001    -0.00000001     0.03422591     0.00000000     0.00000000     0.00000000
   MO  14     0.00000001    -0.00000370     0.00000000     0.00000000     0.00000000     0.00124006    -0.00000032     0.00000000
   MO  15     0.00000001    -0.00001997     0.00000000     0.00000000     0.00000000    -0.00000032     0.00123820     0.00000000
   MO  16    -0.00001094     0.00000006     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00114733
   MO  17     0.00938578    -0.00000016     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000070
   MO  18    -0.00000016     0.00251812     0.00000000     0.00000000     0.00000000    -0.00000018     0.00000165     0.00000000
   MO  19     0.00000000     0.00000000    -0.00003632     0.02250493    -0.00000012     0.00000000     0.00000000     0.00000000
   MO  20     0.00000000     0.00000000    -0.02250508    -0.00003643     0.00000073     0.00000000     0.00000000     0.00000000
   MO  21     0.00000000     0.00000000    -0.00000071    -0.00000011    -0.02180198     0.00000000     0.00000000     0.00000000
   MO  22     0.00000000     0.00003337     0.00000000     0.00000000     0.00000000    -0.00000020     0.00000006     0.00000000
   MO  23     0.00000002    -0.00000001     0.00000000     0.00000000     0.00000000     0.00001572     0.00139876     0.00000001
   MO  24    -0.00000001     0.00000365     0.00000000     0.00000000     0.00000000    -0.00140088     0.00001643     0.00000004
   MO  25     0.00004653    -0.00000005     0.00000000     0.00000000     0.00000000    -0.00000003     0.00000001    -0.00124288
   MO  26    -0.00958603     0.00000015     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000379
   MO  27    -0.00000019     0.00279013     0.00000000     0.00000000     0.00000000    -0.00000030    -0.00000846     0.00000000
   MO  28     0.00000000     0.00000000     0.00000392    -0.00432198     0.00000005     0.00000000     0.00000000     0.00000000
   MO  29     0.00000000     0.00000000    -0.00432171    -0.00000419     0.00000008     0.00000000     0.00000000     0.00000000
   MO  30     0.00000000     0.00000000     0.00000007     0.00000004     0.00394506     0.00000000     0.00000000     0.00000000
   MO  31     0.00000000     0.00000000     0.00000277    -0.00000102     0.00000000     0.00000000     0.00000000     0.00000000
   MO  32     0.00000000     0.00000000    -0.00000008    -0.00000591     0.00000000     0.00000000     0.00000000     0.00000000
   MO  33     0.00000000     0.00000000     0.00000000    -0.00000002    -0.00000242     0.00000000     0.00000000     0.00000000
   MO  34     0.00000000     0.00000000     0.00000002     0.00000000    -0.00000494     0.00000000     0.00000000     0.00000000
   MO  35     0.00000000     0.00000000     0.00000008     0.00098934    -0.00000002     0.00000000     0.00000000     0.00000000
   MO  36     0.00000000     0.00000000     0.00098396     0.00000429    -0.00000006     0.00000000     0.00000000     0.00000000
   MO  37     0.00000000     0.00000000    -0.00000006    -0.00000002    -0.00101047     0.00000000     0.00000000     0.00000000
   MO  38     0.00000000    -0.00002817     0.00000000     0.00000000     0.00000000    -0.00000001     0.00000002     0.00000000

                MO  17         MO  18         MO  19         MO  20         MO  21         MO  22         MO  23         MO  24
   MO   5     0.00000025     0.01094751     0.00000000     0.00000000     0.00000000    -0.05017215     0.00006642    -0.00001296
   MO   6     0.00000000     0.00000232     0.00000000     0.00000000     0.00000000     0.00000100    -0.00004096     0.01021056
   MO   7    -0.00000002     0.00002947     0.00000000     0.00000000     0.00000000    -0.00000044     0.01018403     0.00004927
   MO   8    -0.00000824    -0.00000004     0.00000000     0.00000000     0.00000000     0.00000000     0.00000002    -0.00000004
   MO   9     0.00938578    -0.00000016     0.00000000     0.00000000     0.00000000     0.00000000     0.00000002    -0.00000001
   MO  10    -0.00000016     0.00251812     0.00000000     0.00000000     0.00000000     0.00003337    -0.00000001     0.00000365
   MO  11     0.00000000     0.00000000    -0.00003632    -0.02250508    -0.00000071     0.00000000     0.00000000     0.00000000
   MO  12     0.00000000     0.00000000     0.02250493    -0.00003643    -0.00000011     0.00000000     0.00000000     0.00000000
   MO  13     0.00000000     0.00000000    -0.00000012     0.00000073    -0.02180198     0.00000000     0.00000000     0.00000000
   MO  14     0.00000000    -0.00000018     0.00000000     0.00000000     0.00000000    -0.00000020     0.00001572    -0.00140088
   MO  15     0.00000000     0.00000165     0.00000000     0.00000000     0.00000000     0.00000006     0.00139876     0.00001643
   MO  16    -0.00000070     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000001     0.00000004
   MO  17     0.00114576    -0.00000001     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  18    -0.00000001     0.00063981     0.00000000     0.00000000     0.00000000    -0.00005277     0.00000697     0.00000026
   MO  19     0.00000000     0.00000000     0.01517608    -0.00000020     0.00000001     0.00000000     0.00000000     0.00000000
   MO  20     0.00000000     0.00000000    -0.00000020     0.01517577     0.00000000     0.00000000     0.00000000     0.00000000
   MO  21     0.00000000     0.00000000     0.00000001     0.00000000     0.01436256     0.00000000     0.00000000     0.00000000
   MO  22     0.00000000    -0.00005277     0.00000000     0.00000000     0.00000000     0.00747696    -0.00000044     0.00000024
   MO  23     0.00000000     0.00000697     0.00000000     0.00000000     0.00000000    -0.00000044     0.00172183     0.00000046
   MO  24     0.00000000     0.00000026     0.00000000     0.00000000     0.00000000     0.00000024     0.00000046     0.00172461
   MO  25     0.00000542     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  26    -0.00124103     0.00000001     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  27    -0.00000002     0.00064025     0.00000000     0.00000000     0.00000000    -0.00006392    -0.00000500     0.00000032
   MO  28     0.00000000     0.00000000    -0.00325050     0.00000233    -0.00000002     0.00000000     0.00000000     0.00000000
   MO  29     0.00000000     0.00000000     0.00000207     0.00325022     0.00000004     0.00000000     0.00000000     0.00000000
   MO  30     0.00000000     0.00000000     0.00000001     0.00000004    -0.00291699     0.00000000     0.00000000     0.00000000
   MO  31     0.00000000     0.00000000    -0.00000082    -0.00000233     0.00000000     0.00000000     0.00000000     0.00000000
   MO  32     0.00000000     0.00000000    -0.00000469     0.00000011     0.00000000     0.00000000     0.00000000     0.00000000
   MO  33     0.00000000     0.00000000    -0.00000001     0.00000000     0.00000203     0.00000000     0.00000000     0.00000000
   MO  34     0.00000000     0.00000000     0.00000000    -0.00000002     0.00000426     0.00000000     0.00000000     0.00000000
   MO  35     0.00000000     0.00000000     0.00074566    -0.00000138     0.00000001     0.00000000     0.00000000     0.00000000
   MO  36     0.00000000     0.00000000     0.00000215    -0.00074131     0.00000003     0.00000000     0.00000000     0.00000000
   MO  37     0.00000000     0.00000000    -0.00000001     0.00000002     0.00090649     0.00000000     0.00000000     0.00000000
   MO  38     0.00000000    -0.00000809     0.00000000     0.00000000     0.00000000    -0.00033345    -0.00000006     0.00000001

                MO  25         MO  26         MO  27         MO  28         MO  29         MO  30         MO  31         MO  32
   MO   5     0.00000002    -0.00000032     0.01204613     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   6    -0.00000006    -0.00000001     0.00000255     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   7     0.00000002     0.00000004    -0.00004624     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   8    -0.00960743    -0.00002738     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   9     0.00004653    -0.00958603    -0.00000019     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  10    -0.00000005     0.00000015     0.00279013     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  11     0.00000000     0.00000000     0.00000000     0.00000392    -0.00432171     0.00000007     0.00000277    -0.00000008
   MO  12     0.00000000     0.00000000     0.00000000    -0.00432198    -0.00000419     0.00000004    -0.00000102    -0.00000591
   MO  13     0.00000000     0.00000000     0.00000000     0.00000005     0.00000008     0.00394506     0.00000000     0.00000000
   MO  14    -0.00000003     0.00000000    -0.00000030     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  15     0.00000001     0.00000000    -0.00000846     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  16    -0.00124288    -0.00000379     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  17     0.00000542    -0.00124103    -0.00000002     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  18     0.00000000     0.00000001     0.00064025     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  19     0.00000000     0.00000000     0.00000000    -0.00325050     0.00000207     0.00000001    -0.00000082    -0.00000469
   MO  20     0.00000000     0.00000000     0.00000000     0.00000233     0.00325022     0.00000004    -0.00000233     0.00000011
   MO  21     0.00000000     0.00000000     0.00000000    -0.00000002     0.00000004    -0.00291699     0.00000000     0.00000000
   MO  22     0.00000000     0.00000000    -0.00006392     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  23     0.00000000     0.00000000    -0.00000500     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  24     0.00000000     0.00000000     0.00000032     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  25     0.00149321    -0.00000105     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  26    -0.00000105     0.00149077     0.00000002     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  27     0.00000000     0.00000002     0.00067489     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  28     0.00000000     0.00000000     0.00000000     0.00093859     0.00000005     0.00000000     0.00000027     0.00000156
   MO  29     0.00000000     0.00000000     0.00000000     0.00000005     0.00093849     0.00000000    -0.00000092     0.00000007
   MO  30     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00083104     0.00000000     0.00000000
   MO  31     0.00000000     0.00000000     0.00000000     0.00000027    -0.00000092     0.00000000     0.00159145     0.00007671
   MO  32     0.00000000     0.00000000     0.00000000     0.00000156     0.00000007     0.00000000     0.00007671     0.00180604
   MO  33     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000077     0.00000000     0.00000000
   MO  34     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000172     0.00000000     0.00000000
   MO  35     0.00000000     0.00000000     0.00000000    -0.00020492    -0.00000037     0.00000000     0.00000046     0.00000176
   MO  36     0.00000000     0.00000000     0.00000000    -0.00000086    -0.00020341    -0.00000002     0.00000018    -0.00000013
   MO  37     0.00000000     0.00000000     0.00000000     0.00000000     0.00000001    -0.00039677     0.00000000     0.00000000
   MO  38     0.00000000     0.00000000    -0.00001017     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                MO  33         MO  34         MO  35         MO  36         MO  37         MO  38
   MO   5     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00021943
   MO   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000002
   MO   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000003
   MO   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   9     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  10     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00002817
   MO  11     0.00000000     0.00000002     0.00000008     0.00098396    -0.00000006     0.00000000
   MO  12    -0.00000002     0.00000000     0.00098934     0.00000429    -0.00000002     0.00000000
   MO  13    -0.00000242    -0.00000494    -0.00000002    -0.00000006    -0.00101047     0.00000000
   MO  14     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000001
   MO  15     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000002
   MO  16     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  17     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  18     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000809
   MO  19    -0.00000001     0.00000000     0.00074566     0.00000215    -0.00000001     0.00000000
   MO  20     0.00000000    -0.00000002    -0.00000138    -0.00074131     0.00000002     0.00000000
   MO  21     0.00000203     0.00000426     0.00000001     0.00000003     0.00090649     0.00000000
   MO  22     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00033345
   MO  23     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000006
   MO  24     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000001
   MO  25     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  26     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  27     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00001017
   MO  28     0.00000000     0.00000000    -0.00020492    -0.00000086     0.00000000     0.00000000
   MO  29     0.00000000     0.00000000    -0.00000037    -0.00020341     0.00000001     0.00000000
   MO  30    -0.00000077    -0.00000172     0.00000000    -0.00000002    -0.00039677     0.00000000
   MO  31     0.00000000     0.00000000     0.00000046     0.00000018     0.00000000     0.00000000
   MO  32     0.00000000     0.00000000     0.00000176    -0.00000013     0.00000000     0.00000000
   MO  33     0.00181130    -0.00000042     0.00000001     0.00000000     0.00000053     0.00000000
   MO  34    -0.00000042     0.00180896     0.00000000    -0.00000001    -0.00000132     0.00000000
   MO  35     0.00000001     0.00000000     0.00115430     0.00000083     0.00000000     0.00000000
   MO  36     0.00000000    -0.00000001     0.00000083     0.00115231    -0.00000001     0.00000000
   MO  37     0.00000053    -0.00000132     0.00000000    -0.00000001     0.00080044     0.00000000
   MO  38     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00002245

Natural orbital populations,block 1
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     1.82705120     0.49730590     0.49540189     0.49463133
              MO     9       MO    10       MO    11       MO    12       MO    13       MO    14       MO    15       MO    16
  occ(*)=     0.49272347     0.04999387     0.04999343     0.04879136     0.01598760     0.00598625     0.00249997     0.00249598
              MO    17       MO    18       MO    19       MO    20       MO    21       MO    22       MO    23       MO    24
  occ(*)=     0.00220953     0.00220514     0.00183065     0.00181137     0.00180889     0.00156685     0.00114610     0.00114354
              MO    25       MO    26       MO    27       MO    28       MO    29       MO    30       MO    31       MO    32
  occ(*)=     0.00104543     0.00066750     0.00066709     0.00036838     0.00035754     0.00006396     0.00006381     0.00005848
              MO    33       MO    34       MO    35       MO    36       MO    37       MO    38       MO
  occ(*)=     0.00005831     0.00001668     0.00001668     0.00001479     0.00001243     0.00000460


 total number of electrons =   12.0000000000

 test slabel:                     1
  a  


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        a   partial gross atomic populations
   ao class       1a         2a         3a         4a         5a         6a  
     3_ s       0.000000   2.000000   0.000000   0.000000   1.826128   0.000000
     3_ p       2.000000   0.000000   2.000000   2.000000   0.000000   0.000000
     3_ d       0.000000   0.000000   0.000000   0.000000   0.000923   0.497306
     3_ f       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
 
   ao class       7a         8a         9a        10a        11a        12a  
     3_ s       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     3_ p       0.000000   0.000000   0.000000   0.049961   0.049961   0.048751
     3_ d       0.495402   0.494631   0.492723   0.000000   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000033   0.000033   0.000040
 
   ao class      13a        14a        15a        16a        17a        18a  
     3_ s       0.000210   0.005908   0.000000   0.000000   0.000000   0.000000
     3_ p       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     3_ d       0.015778   0.000078   0.002500   0.002496   0.002210   0.002205
     3_ f       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
 
   ao class      19a        20a        21a        22a        23a        24a  
     3_ s       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     3_ p       0.000000   0.000000   0.000000   0.000000   0.000057   0.000056
     3_ d       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     3_ f       0.001831   0.001811   0.001809   0.001567   0.001089   0.001087
 
   ao class      25a        26a        27a        28a        29a        30a  
     3_ s       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     3_ p       0.000438   0.000635   0.000634   0.000216   0.000000   0.000000
     3_ d       0.000000   0.000000   0.000000   0.000000   0.000357   0.000064
     3_ f       0.000608   0.000032   0.000033   0.000153   0.000000   0.000000
 
   ao class      31a        32a        33a        34a        35a        36a  
     3_ s       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     3_ p       0.000000   0.000000   0.000000   0.000017   0.000017   0.000015
     3_ d       0.000064   0.000058   0.000058   0.000000   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
 
   ao class      37a        38a  
     3_ s       0.000000   0.000004
     3_ p       0.000000   0.000000
     3_ d       0.000012   0.000000
     3_ f       0.000000   0.000000


                        gross atomic populations
     ao            3_
      s         3.832251
      p         6.150758
      d         2.006866
      f         0.010125
    total      12.000000
 

 Total number of electrons:   12.00000000

 DA ...
