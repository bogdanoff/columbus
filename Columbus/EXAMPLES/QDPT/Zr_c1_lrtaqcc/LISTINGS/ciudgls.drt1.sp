1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      2009-03.    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


================ Computing sorting integral file structure ================

                    -----z----- -----y----- -----x----- -----w----- ---total---

                CSFs       105        1960        5670        8526       16261
      internal walks       105          70          15          21         211
valid internal walks       105          70          15          21         211
 getinfoarray: info=                     6 :                     1
                  8192                  6552                  8192
                  5460                     0
 icd(3)=                  1191 ci%nnlev=                   595  l2rec=
                  8192  n2max=                  5460
 lcore1,lcore2=              13069922              13046606
 lencor,maxblo              13107200                 60000
========================================
 current settings:
 minbl3        1215
 minbl4        1215
 locmaxbl3    22684
 locmaxbuf    11342
 maxbl3       60000
 maxbl3       60000
 maxbl4       60000
 maxbuf       30006
========================================

 sorted 4-external integrals:     3 records of integral w-combinations 
                                  3 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)

 sorted 3-external integrals:     3 records of integral w-combinations 
                                  3 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)
 Orig.  diagonal integrals:  1electron:        34
                             0ext.    :        42
                             2ext.    :       336
                             4ext.    :       812


 Orig. off-diag. integrals:  4ext.    :     93177
                             3ext.    :     72576
                             2ext.    :     25578
                             1ext.    :      4704
                             0ext.    :       315
                             2ext. SO :         0
                             1ext. SO :         0
                             0ext. SO :         0
                             1electron:       406


 Sorted integrals            3ext.  w :     68040 x :     63504
                             4ext.  w :     82215 x :     71253


Cycle #  1 sortfile size=    393204(      12 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13069922
Cycle #  2 sortfile size=    393204(      12 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13069922
 minimum size of srtscr:    327670 WP (    10 records)
 maximum size of srtscr:    393204 WP (    12 records)
diagi   file:      4 records  of   1536 WP each=>       6144 WP total
ofdgi   file:     23 records  of   1536 WP each=>      35328 WP total
fil3w   file:      3 records  of  30006 WP each=>      90018 WP total
fil3x   file:      3 records  of  30006 WP each=>      90018 WP total
fil4w   file:      3 records  of  30006 WP each=>      90018 WP total
fil4x   file:      3 records  of  30006 WP each=>      90018 WP total
 compressed index vector length=                     1
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 3,
  GSET = 3,
   DAVCOR =10,
  NCOREL = 4
   nroot=1
  IVMODE = 3
  NBKITR = 1
  RTOLBK = 1e-4,1e-4,1e-4,1e-4,1e-4,1e-4,1e-4,1e-4,1e-4,
  NITER = 200
   rtolci=1e-4
  NVCIMX = 25
  NVRFMX = 25
  NVBKMX = 25
  IDEN  = 1
  CSFPRN = 10,
  update_mode=1
  nvcimn=2
  nvrfmn=2
  nvbkmn=2
   lrtshift=-0.0524795077
 /&end
 ------------------------------------------------------------------------
lodens (list->root)=  1
invlodens (root->list)=  1
 LRT calculation: diagonal shift= -5.247950770000000E-002
 bummer (warning):2:changed keyword: nbkitr=                      0
 USING SEGMENTS OF EQUAL SIZE

****************  list of control variables  ****************
 lvlprt =    0      nroot  =    1      noldv  =   0      noldhv =   0
 nunitv =    1      nbkitr =    0      niter  = 200      davcor =  10
 csfprn =   10      ivmode =    3      istrt  =   0      vout   =   0
 iortls =    0      nvbkmx =   25      ibktv  =  -1      ibkthv =  -1
 nvcimx =   25      icitv  =   -1      icithv =  -1      frcsub =   0
 nvbkmn =    2      nvcimn =    2      maxseg =   4      nrfitr =  30
 ncorel =    4      nvrfmx =   25      nvrfmn =   2      iden   =   1
 itran  =    0      froot  =    0      rtmode =   0      ncouple=   1
 skipso =    F      dalton2=    0      molcas =   0      finalv =   0
 finalw =    0      cosmocalc=   0    with_tsklst=   0
 nsegwx =    1     1     1     1
 nseg0x =    1     1     1     1
 nseg1x =    1     1     1     1
 nseg2x =    1     1     1     1
 nseg3x =    1     1     1     1
 nseg4x =    1     1     1     1
 no0ex  =      0    no1ex  =      0    no2ex  =     0    no3ex  =     0
 no4ex  =      0    nodiag =      0
 cdg4ex =    1      c3ex1ex=    1      c2ex0ex=   1
 fileloc=    0     0     0     0     0     0     0     1     1     1
 directhd=   1      noaqccshift_zyxw=      0
 critical_crit=-1.00000    critical_delta= 0.05000

 ctol   = 0.010000    lrtshift=-.052480    smalld =0.001000


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-04    1.000E-04
 Computing density:                    .drt1.state1
 using                      1  nodes and                      1  cores.
 szdg/szodg per processor=                  6144                 35328
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core           13107199 DP per process

********** Integral sort section *************


 workspace allocation information: lencor=  13107199

 echo of the input for program cisrt:
 ------------------------------------------------------------------------
  &input
  maxbl3=60000
  maxbl4=60000
  &end
 ------------------------------------------------------------------------
 
 ( 6) listing file:                    ciudgls             
 ( 5) input file:                      cisrtin   
 (17) cidrt file:                      cidrtfl             
 (11) transformed integrals file:      moints    
 (12) diagonal integral file:          diagint             
 (13) off-diagonal integral file:      ofdgint             
 (31) 4-external w integrals file:     fil4w               
 (32) 4-external x integrals file:     fil4x               
 (33) 3-external w integrals file:     fil3w               
 (34) 3-external x integrals file:     fil3x               
 (21) scratch da sorting file:         srtscr              
 (12) 2-e integral file [fsplit=2]:    moints2   

 input integral file header information:
                                                                                 
 aoints SIFS file created by argos.      zam792            16:59:01.676 17-Dec-13
  cidrt_title                                                                    
 MO-coefficients from mcscf.x                                                    
  with dummy occupation 1.0 for active orbitals                                  
  total ao core energy =    0.000000000                                          
 MCSCF energy =     -46.297255533                                                
 SIFS file created by program tran.      zam792            16:59:02.163 17-Dec-13

 input energy(*) values:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.

 total core energy =   0.000000000000E+00

 nsym = 1 nmot=  34

 symmetry  =    1
 slabel(*) =    a
 nmpsy(*)  =   34

 info(*) =          1      8192      6552      8192      5460         0

 orbital labels, i:molab(i)=
   1:tout:001   2:tout:002   3:tout:003   4:tout:004   5:tout:005   6:tout:006   7:tout:007   8:tout:008   9:tout:009  10:tout:010
  11:tout:011  12:tout:012  13:tout:013  14:tout:014  15:tout:015  16:tout:016  17:tout:017  18:tout:018  19:tout:019  20:tout:020
  21:tout:021  22:tout:022  23:tout:023  24:tout:024  25:tout:025  26:tout:026  27:tout:027  28:tout:028  29:tout:029  30:tout:030
  31:tout:031  32:tout:032  33:tout:033  34:tout:034

 input parameters:
 prnopt=  0
 ldamin=    4095 ldamax=   32767 ldainc=      64
 maxbuf=   30006 maxbl3=   60000 maxbl4=   60000 intmxo=    1536
  Using 32 bit compression 

 drt information:
  cidrt_title                                                                    
 nmotd =  38 nfctd =   4 nfvtc =   0 nmot  =  34
 nlevel =  34 niot  =   6 lowinl=  29
 orbital-to-level map(*)
   -1  -1  -1  -1  29  30  31  32  33  34   1   2   3   4   5   6   7   8   9  10
   11  12  13  14  15  16  17  18  19  20  21  22  23  24  25  26  27  28
 compressed map(*)
   29  30  31  32  33  34   1   2   3   4   5   6   7   8   9  10  11  12  13  14
   15  16  17  18  19  20  21  22  23  24  25  26  27  28
 levsym(*)
    1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1
    1   1   1   1   1   1   1   1   1   1   1   1   1   1
 repartitioning mu(*)=
   2.  0.  0.  0.  0.  0.

 new core energy added to the energy(*) list.
 from the integral file: h1_core= -4.355613553355E+01

 indxdg: diagonal integral statistics.
 total number of integrals contributing to diagonal matrix elements:      1190
 number with all external indices:       812
 number with half external - half internal indices:       336
 number with all internal indices:        42

 indxof: off-diagonal integral statistics.
    4-external integrals: num=      93177 strt=          1
    3-external integrals: num=      72576 strt=      93178
    2-external integrals: num=      25578 strt=     165754
    1-external integrals: num=       4704 strt=     191332
    0-external integrals: num=        315 strt=     196036

 total number of off-diagonal integrals:      196350


 indxof(2nd)  ittp=   3 numx(ittp)=       25578
 indxof(2nd)  ittp=   4 numx(ittp)=        4704
 indxof(2nd)  ittp=   5 numx(ittp)=         315

 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg=  12931980
 pro2e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652

 pro2e:     90013 integrals read in    17 records.

 pro2e:         0 integrals 34-ext integrals skipped.
 pro1e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652
 pro1e: eref =   -1.721733008562787E+00
 total size of srtscr:                     7  records of                  32767 
 WP =               1834952 Bytes

 new core energy added to the energy(*) list.
 from the hamiltonian repartitioning, eref= -1.721733008563E+00
 putdg        1     596    1191    1786    3322   36089   57934    3018   46706   90394
   123161  131353  136813  158652

 putf:       4 buffers of length    1536 written to file 12
 diagonal integral file completed.

 putd34:     3 records of integral w-combinations and
             3 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep4e:     3 blocks of linear combinations of 4-external integrals processed.
 number of sorted 4-external integrals     153468
 number of original 4-external integrals    93177


 putf34: external integral file complete. nfilw=    31 nfilx=    32 nrecw=     3 nrecx=     3 lbufp= 30006

 putd34:     3 records of integral w-combinations and
             3 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep3e:     6 blocks of linear combinations of 3-external integrals processed.
 number of sorted 3-external integrals     131544
 number of original 3-external integrals    72576


 putf34: external integral file complete. nfilw=    33 nfilx=    34 nrecw=     3 nrecx=     3 lbufp= 30006
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd     25578         3    165754    165754     25578    196350
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      4704         4    191332    191332      4704    196350
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd       315         5    196036    196036       315    196350

 putf:      23 buffers of length    1536 written to file 13
 off-diagonal files sort completed.
 executing brd_struct for cisrtinfo
cisrtinfo:
bufszi  1536
 diagfile 4ext:     812 2ext:     336 0ext:      42
 fil4w,fil4x  :   93177 fil3w,fil3x :   72576
 ofdgint  2ext:   25578 1ext:    4704 0ext:     315so0ext:       0so1ext:       0so2ext:       0
buffer minbl4    1215 minbl3    1215 maxbl2    1218nbas:  28   0   0   0   0   0   0   0 maxbuf 30006
 CIUDG version 5.9.7 ( 5-Oct-2004)

 workspace allocation information: lcore=  13107199

 core energy values from the integral file:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -4.355613553355E+01, ietype=    6,   fcore energy of type: H1(*)   
 energy( 3)= -1.721733008563E+00, ietype=    5,   fcore energy of type: Vref(*) 

 total core repulsion energy = -4.527786854211E+01
 nmot  =    38 niot  =     6 nfct  =     4 nfvt  =     0
 nrow  =    32 nsym  =     1 ssym  =     1 lenbuf=  1600
 nwalk,xbar:        211      105       70       15       21
 nvalwt,nvalw:      211      105       70       15       21
 ncsft:           16261
 total number of valid internal walks:     211
 nvalz,nvaly,nvalx,nvalw =      105      70      15      21

 cisrt info file parameters:
 file number  12 blocksize   1536
 mxbld   1536
 nd4ext,nd2ext,nd0ext   812   336    42
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int    93177    72576    25578     4704      315        0        0        0
 minbl4,minbl3,maxbl2  1215  1215  1218
 maxbuf 30006
 number of external orbitals per symmetry block:  28
 nmsym   1 number of internal orbitals   6
 executing brd_struct for drt
 executing brd_struct for orbinf
 executing brd_struct for momap
 calcthrxt: niot,maxw1=                     6                    31
 block size     0
 pthz,pthy,pthx,pthw:   105    70    15    21 total internal walks:     211
 maxlp3,n2lp,n1lp,n0lp    31     0     0     0
 orbsym(*)= 1 1 1 1 1 1

 setref:      105 references kept,
                0 references were marked as invalid, out of
              105 total.
 nmb.of records onel     1
 nmb.of records 2-ext    17
 nmb.of records 1-ext     4
 nmb.of records 0-ext     1
 nmb.of records 2-int     0
 nmb.of records 1-int     0
 nmb.of records 0-int     0
 ---------memory usage in DP -----------------
 < n-ex core usage >
     routines:
    fourex            62738
    threx             60199
    twoex              9922
    onex               3217
    allin              1536
    diagon             2109
               =======
   maximum            62738
 
  __ static summary __ 
   reflst               105
   hrfspc               105
               -------
   static->             210
 
  __ core required  __ 
   totstc               210
   max n-ex           62738
               -------
   totnec->           62948
 
  __ core available __ 
   totspc          13107199
   totnec -           62948
               -------
   totvec->        13044251

 number of external paths / symmetry
 vertex x     378
 vertex w     406
segment: free space=    13044251
 reducing frespc by                   928 to               13043323 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1         105|       105|         0|       105|         0|         1|
 -------------------------------------------------------------------------------
  Y 2          70|      1960|       105|        70|       105|         2|
 -------------------------------------------------------------------------------
  X 3          15|      5670|      2065|        15|       175|         3|
 -------------------------------------------------------------------------------
  W 4          21|      8526|      7735|        21|       190|         4|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=           4DP  conft+indsym=         420DP  drtbuffer=         504 DP

dimension of the ci-matrix ->>>     16261

 executing brd_struct for civct
 gentasklist: ntask=                    20
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS   
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      15     105       5670        105      15     105
     2  4   1    25      two-ext wz   2X  4 1      21     105       8526        105      21     105
     3  4   3    26      two-ext wx*  WX  4 3      21      15       8526       5670      21      15
     4  4   3    27      two-ext wx+  WX  4 3      21      15       8526       5670      21      15
     5  2   1    11      one-ext yz   1X  2 1      70     105       1960        105      70     105
     6  3   2    15      1ex3ex yx    3X  3 2      15      70       5670       1960      15      70
     7  4   2    16      1ex3ex yw    3X  4 2      21      70       8526       1960      21      70
     8  1   1     1      allint zz    OX  1 1     105     105        105        105     105     105
     9  2   2     5      0ex2ex yy    OX  2 2      70      70       1960       1960      70      70
    10  3   3     6      0ex2ex xx*   OX  3 3      15      15       5670       5670      15      15
    11  3   3    18      0ex2ex xx+   OX  3 3      15      15       5670       5670      15      15
    12  4   4     7      0ex2ex ww*   OX  4 4      21      21       8526       8526      21      21
    13  4   4    19      0ex2ex ww+   OX  4 4      21      21       8526       8526      21      21
    14  2   2    42      four-ext y   4X  2 2      70      70       1960       1960      70      70
    15  3   3    43      four-ext x   4X  3 3      15      15       5670       5670      15      15
    16  4   4    44      four-ext w   4X  4 4      21      21       8526       8526      21      21
    17  1   1    75      dg-024ext z  OX  1 1     105     105        105        105     105     105
    18  2   2    76      dg-024ext y  OX  2 2      70      70       1960       1960      70      70
    19  3   3    77      dg-024ext x  OX  3 3      15      15       5670       5670      15      15
    20  4   4    78      dg-024ext w  OX  4 4      21      21       8526       8526      21      21
----------------------------------------------------------------------------------------------------
REDTASK #   1 TIME=  19.000 N=  1 (task/type/sgbra)=(   1/24/0) (
REDTASK #   2 TIME=  18.000 N=  1 (task/type/sgbra)=(   2/25/0) (
REDTASK #   3 TIME=  17.000 N=  1 (task/type/sgbra)=(   3/26/1) (
REDTASK #   4 TIME=  16.000 N=  1 (task/type/sgbra)=(   4/27/2) (
REDTASK #   5 TIME=  15.000 N=  1 (task/type/sgbra)=(   5/11/0) (
REDTASK #   6 TIME=  14.000 N=  1 (task/type/sgbra)=(   6/15/0) (
REDTASK #   7 TIME=  13.000 N=  1 (task/type/sgbra)=(   7/16/0) (
REDTASK #   8 TIME=  12.000 N=  1 (task/type/sgbra)=(   8/ 1/0) (
REDTASK #   9 TIME=  11.000 N=  1 (task/type/sgbra)=(   9/ 5/0) (
REDTASK #  10 TIME=  10.000 N=  1 (task/type/sgbra)=(  10/ 6/1) (
REDTASK #  11 TIME=   9.000 N=  1 (task/type/sgbra)=(  11/18/2) (
REDTASK #  12 TIME=   8.000 N=  1 (task/type/sgbra)=(  12/ 7/1) (
REDTASK #  13 TIME=   7.000 N=  1 (task/type/sgbra)=(  13/19/2) (
REDTASK #  14 TIME=   6.000 N=  1 (task/type/sgbra)=(  14/42/1) (
REDTASK #  15 TIME=   5.000 N=  1 (task/type/sgbra)=(  15/43/1) (
REDTASK #  16 TIME=   4.000 N=  1 (task/type/sgbra)=(  16/44/1) (
REDTASK #  17 TIME=   3.000 N=  1 (task/type/sgbra)=(  17/75/1) (
REDTASK #  18 TIME=   2.000 N=  1 (task/type/sgbra)=(  18/76/1) (
REDTASK #  19 TIME=   1.000 N=  1 (task/type/sgbra)=(  19/77/1) (
REDTASK #  20 TIME=   0.000 N=  1 (task/type/sgbra)=(  20/78/1) (
 initializing v-file: 1:                 16261

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         865 2x:           0 4x:           0
All internal counts: zz :        5410 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 reference space has dimension     105
 dsyevx: computed roots 1 to    2(converged:   2)

    root           eigenvalues
    ----           ------------
       1         -46.2712888828
       2         -46.2712888476

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt=                   105

         vector  1 from unit 11 written to unit 49 filename cirefv              
 ncorel,neli=                     4                     4
  This is a  mraqcc energy evaluation      

    ------------------------------------------------------------

 setopt2: mraqcc energy evaluation      gvalue=  0.833333333333333      ncorel=
                     4

    ------------------------------------------------------------


    reference energies:

  reference state used is:   1with energy      -46.2712888828

  ### begin active excitation selection ###

 inactive(1:nintern)=AAAAAA
 There are        105 reference CSFs out of        105 valid zwalks.
 There are    0 inactive and    6 active orbitals.
     105 active-active  and        0 inactive excitations.

  ### end active excitation selection ###


 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:             16261
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:              25
 number of roots to converge:                             1
 number of iterations:                                  200
 residual norm convergence criteria:               0.000100

          starting ci iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:        1490 2x:         305 4x:         106
All internal counts: zz :        5410 yy:        2859 xx:         390 ww:         640
One-external counts: yz :        4477 yx:        1070 yw:        1295
Two-external counts: yy :         900 ww:         280 xx:         200 xz:         285 wz:         435 wx:         360
Three-ext.   counts: yx :         130 yw:         160

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1
   ht   1    -0.99342034
calca4: root=   1 anorm**2=  0.00000000 scale:  1.00000000

          calcsovref: tciref block   1

              civs   1
 refs   1    1.00000    

          calcsovref: scrb block   1

                ci   1
 civs   1    1.00000    

          calcsovref: sovref block   1

              v      1
 ref    1    1.00000    

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000
 fact= -8.746584616666664E-003
 factd= -8.746584616666664E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  1  1    -46.2712888828 -7.7716E-15  6.5636E-02  2.3403E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   2

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:        1490 2x:         305 4x:         106
All internal counts: zz :        5410 yy:        2859 xx:         390 ww:         640
One-external counts: yz :        4477 yx:        1070 yw:        1295
Two-external counts: yy :         900 ww:         280 xx:         200 xz:         285 wz:         435 wx:         360
Three-ext.   counts: yx :         130 yw:         160

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2
   ht   1    -0.99342034
   ht   2    -0.06563569    -0.05041005
calca4: root=   1 anorm**2=  0.08101710 scale:  1.03971972
calca4: root=   2 anorm**2=  0.91898290 scale:  1.38527358

          calcsovref: tciref block   1

              civs   1       civs   2
 refs   1    1.00000      -2.344327E-14

          calcsovref: scrb block   1

                ci   1         ci   2
 civs   1   0.958636      -0.284635    
 civs   2   0.816725        2.75069    

          calcsovref: sovref block   1

              v      1       v      2
 ref    1   0.958636      -0.284635    

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.95863596    -0.28463503
 fact= -8.746584616666664E-003
 factd= -8.746584616666664E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  2  1    -46.3272082534  5.5919E-02  3.1920E-03  4.9570E-02  1.0000E-04
 mraqcc  #  2  2    -45.6369913928  3.5912E-01  0.0000E+00  5.0752E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   3

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:        1490 2x:         305 4x:         106
All internal counts: zz :        5410 yy:        2859 xx:         390 ww:         640
One-external counts: yz :        4477 yx:        1070 yw:        1295
Two-external counts: yy :         900 ww:         280 xx:         200 xz:         285 wz:         435 wx:         360
Three-ext.   counts: yx :         130 yw:         160

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3
   ht   1    -0.99342034
   ht   2    -0.06563569    -0.05041005
   ht   3     0.00566899    -0.00304803    -0.00331730
calca4: root=   1 anorm**2=  0.09545129 scale:  1.04663809
calca4: root=   2 anorm**2=  0.85621354 scale:  1.36242928
calca4: root=   3 anorm**2=  0.92108046 scale:  1.38603047

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3
 refs   1    1.00000      -2.344327E-14  -5.730981E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3
 civs   1   0.956887      -0.142145       0.263842    
 civs   2   0.860445        1.21075       -2.45543    
 civs   3    1.09862        11.4546        5.79152    

          calcsovref: sovref block   1

              v      1       v      2       v      3
 ref    1   0.950591      -0.207791       0.230651    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.95059099    -0.20779067     0.23065085
 fact= -8.746584616666664E-003
 factd= -8.746584616666664E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  3  1    -46.3307282905  3.5200E-03  5.4927E-04  2.0951E-02  1.0000E-04
 mraqcc  #  3  2    -45.8875048939  2.5051E-01  0.0000E+00  4.0952E-01  1.0000E-04
 mraqcc  #  3  3    -45.5731623458  2.9529E-01  0.0000E+00  5.1292E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   4

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:        1490 2x:         305 4x:         106
All internal counts: zz :        5410 yy:        2859 xx:         390 ww:         640
One-external counts: yz :        4477 yx:        1070 yw:        1295
Two-external counts: yy :         900 ww:         280 xx:         200 xz:         285 wz:         435 wx:         360
Three-ext.   counts: yx :         130 yw:         160

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4
   ht   1    -0.99342034
   ht   2    -0.06563569    -0.05041005
   ht   3     0.00566899    -0.00304803    -0.00331730
   ht   4    -0.00128490    -0.00198045    -0.00046230    -0.00044176
calca4: root=   1 anorm**2=  0.10233337 scale:  1.04992065
calca4: root=   2 anorm**2=  0.91730361 scale:  1.38466733
calca4: root=   3 anorm**2=  0.85643130 scale:  1.36250919
calca4: root=   4 anorm**2=  0.79793379 scale:  1.34087054

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4
 refs   1    1.00000      -2.344327E-14  -5.730981E-03   1.394390E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4
 civs   1   0.952972       0.175912      -0.231818       0.120056    
 civs   2   0.865829      -0.860248        1.83633       -1.88625    
 civs   3    1.25311       -9.74607       -7.81786       -2.87983    
 civs   4   0.913004       -15.7065        10.2280        24.5770    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4
 ref    1   0.947064       0.209866      -0.172752       0.170830    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.94706391     0.20986556    -0.17275239     0.17083034
 fact= -8.746584616666664E-003
 factd= -8.746584616666664E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  4  1    -46.3312300441  5.0175E-04  1.2000E-04  9.3542E-03  1.0000E-04
 mraqcc  #  4  2    -46.0086258576  1.2112E-01  0.0000E+00  2.6193E-01  1.0000E-04
 mraqcc  #  4  3    -45.5806306073  7.4683E-03  0.0000E+00  5.1389E-01  1.0000E-04
 mraqcc  #  4  4    -45.5317202674  2.5385E-01  0.0000E+00  5.7924E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   5

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:        1490 2x:         305 4x:         106
All internal counts: zz :        5410 yy:        2859 xx:         390 ww:         640
One-external counts: yz :        4477 yx:        1070 yw:        1295
Two-external counts: yy :         900 ww:         280 xx:         200 xz:         285 wz:         435 wx:         360
Three-ext.   counts: yx :         130 yw:         160

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1    -0.99342034
   ht   2    -0.06563569    -0.05041005
   ht   3     0.00566899    -0.00304803    -0.00331730
   ht   4    -0.00128490    -0.00198045    -0.00046230    -0.00044176
   ht   5     0.00248243    -0.00019293    -0.00044011    -0.00000108    -0.00017404
calca4: root=   1 anorm**2=  0.10447552 scale:  1.05094030
calca4: root=   2 anorm**2=  0.79980400 scale:  1.34156774
calca4: root=   3 anorm**2=  0.45423502 scale:  1.20591667
calca4: root=   4 anorm**2=  0.87266948 scale:  1.36845514
calca4: root=   5 anorm**2=  0.97772290 scale:  1.40631536

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1    1.00000      -2.344327E-14  -5.730981E-03   1.394390E-03  -2.479592E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 civs   1  -0.953467       0.108244       0.252507       0.187311       2.704008E-02
 civs   2  -0.866412      -0.568467       -1.25353       -2.37385      -0.459283    
 civs   3   -1.28273       -7.75603       -1.88442        4.53777       -9.78020    
 civs   4   -1.08977       -15.7207       -10.5141        11.7321        22.5560    
 civs   5  -0.811314       -25.1143        42.7925       -15.1592        28.7150    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1  -0.945624       0.193046       0.142538       0.215253       4.334065E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1    -0.94562369     0.19304586     0.14253778     0.21525266     0.04334065
 fact= -8.746584616666664E-003
 factd= -8.746584616666664E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  5  1    -46.3313274135  9.7369E-05  1.6848E-05  3.3947E-03  1.0000E-04
 mraqcc  #  5  2    -46.0876952272  7.9069E-02  0.0000E+00  1.6060E-01  1.0000E-04
 mraqcc  #  5  3    -45.6953925668  1.1476E-01  0.0000E+00  5.8931E-01  1.0000E-04
 mraqcc  #  5  4    -45.5438366117  1.2116E-02  0.0000E+00  5.5424E-01  1.0000E-04
 mraqcc  #  5  5    -45.5053680904  2.2750E-01  0.0000E+00  5.3910E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   6

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:        1490 2x:         305 4x:         106
All internal counts: zz :        5410 yy:        2859 xx:         390 ww:         640
One-external counts: yz :        4477 yx:        1070 yw:        1295
Two-external counts: yy :         900 ww:         280 xx:         200 xz:         285 wz:         435 wx:         360
Three-ext.   counts: yx :         130 yw:         160

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1    -0.99342034
   ht   2    -0.06563569    -0.05041005
   ht   3     0.00566899    -0.00304803    -0.00331730
   ht   4    -0.00128490    -0.00198045    -0.00046230    -0.00044176
   ht   5     0.00248243    -0.00019293    -0.00044011    -0.00000108    -0.00017404
   ht   6    -0.00054835    -0.00017877     0.00005205     0.00003024     0.00001452    -0.00002516
calca4: root=   1 anorm**2=  0.10473351 scale:  1.05106304
calca4: root=   2 anorm**2=  0.69799707 scale:  1.30307216
calca4: root=   3 anorm**2=  0.77067909 scale:  1.33066866
calca4: root=   4 anorm**2=  0.31197602 scale:  1.14541522
calca4: root=   5 anorm**2=  0.94738238 scale:  1.39548643
calca4: root=   6 anorm**2=  0.95529529 scale:  1.39831874

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1    1.00000      -2.344327E-14  -5.730981E-03   1.394390E-03  -2.479592E-03   5.596373E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1   0.954127      -3.516549E-02  -0.244566       0.158075      -0.163489      -7.879846E-02
 civs   2   0.866389       0.349787        1.03264      -0.659774        2.07801        1.35741    
 civs   3    1.28794        5.92889        6.27476       0.302671       -8.13045        6.42731    
 civs   4    1.12140        13.8199        6.72981       -13.5311       -2.17933       -24.4628    
 civs   5   0.956743        28.7362       -11.9041        37.3046        22.2169       -25.9887    
 civs   6   -1.03646       -62.5112        124.991        46.8507       -17.5384       -53.0607    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.945357      -0.156111      -0.171676       7.119207E-02  -0.184836      -0.114997    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.94535680    -0.15611123    -0.17167585     0.07119207    -0.18483645    -0.11499733
 fact= -8.746584616666664E-003
 factd= -8.746584616666664E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  6  1    -46.3313448758  1.7462E-05  3.9606E-06  1.8500E-03  1.0000E-04
 mraqcc  #  6  2    -46.1326637829  4.4969E-02  0.0000E+00  1.4538E-01  1.0000E-04
 mraqcc  #  6  3    -45.8837380847  1.8835E-01  0.0000E+00  3.9043E-01  1.0000E-04
 mraqcc  #  6  4    -45.6671412898  1.2330E-01  0.0000E+00  6.3235E-01  1.0000E-04
 mraqcc  #  6  5    -45.5348501571  2.9482E-02  0.0000E+00  5.3231E-01  1.0000E-04
 mraqcc  #  6  6    -45.4660031685  1.8813E-01  0.0000E+00  5.4730E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001999
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   7

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:        1490 2x:         305 4x:         106
All internal counts: zz :        5410 yy:        2859 xx:         390 ww:         640
One-external counts: yz :        4477 yx:        1070 yw:        1295
Two-external counts: yy :         900 ww:         280 xx:         200 xz:         285 wz:         435 wx:         360
Three-ext.   counts: yx :         130 yw:         160

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1    -0.99342034
   ht   2    -0.06563569    -0.05041005
   ht   3     0.00566899    -0.00304803    -0.00331730
   ht   4    -0.00128490    -0.00198045    -0.00046230    -0.00044176
   ht   5     0.00248243    -0.00019293    -0.00044011    -0.00000108    -0.00017404
   ht   6    -0.00054835    -0.00017877     0.00005205     0.00003024     0.00001452    -0.00002516
   ht   7     0.00006872     0.00005124     0.00000141     0.00000788    -0.00001637     0.00000182    -0.00000383
calca4: root=   1 anorm**2=  0.10467705 scale:  1.05103618
calca4: root=   2 anorm**2=  0.63131764 scale:  1.27723046
calca4: root=   3 anorm**2=  0.92162261 scale:  1.38622603
calca4: root=   4 anorm**2=  0.27467473 scale:  1.12901494
calca4: root=   5 anorm**2=  0.88679443 scale:  1.37360636
calca4: root=   6 anorm**2=  0.88776549 scale:  1.37395979
calca4: root=   7 anorm**2=  0.89584567 scale:  1.37689712

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1    1.00000      -2.344327E-14  -5.730981E-03   1.394390E-03  -2.479592E-03   5.596373E-04  -7.168678E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1   0.954391       1.528863E-02   0.221723       6.906862E-03   0.238160       8.255132E-02  -7.432477E-02
 civs   2   0.866367       0.201509      -0.840328      -0.260563       -1.97131       -1.75068       0.243666    
 civs   3    1.28919        4.27301       -7.39624      -0.947592        6.16498       -1.86134        8.27035    
 civs   4    1.12906        11.0335       -11.5181        7.74997       -8.77740        24.5255       -5.43834    
 civs   5   0.992077        28.0392        2.10928       -20.5285        6.41731        2.23416       -51.3795    
 civs   6   -1.28833       -82.0395       -78.4472       -93.5360        3.11450        58.1242       -2.05517    
 civs   7    1.07153        105.110        166.785       -137.085       -122.206        82.0715        247.750    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.945319      -0.116788       0.186962       3.152734E-02   0.185180       0.148522      -2.081556E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.94531902    -0.11678811     0.18696181     0.03152734     0.18518032     0.14852188    -0.02081556
 fact= -8.746584616666664E-003
 factd= -8.746584616666664E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  7  1    -46.3313491198  4.2440E-06  1.3148E-06  8.8658E-04  1.0000E-04
 mraqcc  #  7  2    -46.1636404891  3.0977E-02  0.0000E+00  1.3201E-01  1.0000E-04
 mraqcc  #  7  3    -45.9866993531  1.0296E-01  0.0000E+00  2.4930E-01  1.0000E-04
 mraqcc  #  7  4    -45.7276121800  6.0471E-02  0.0000E+00  6.1267E-01  1.0000E-04
 mraqcc  #  7  5    -45.5756973365  4.0847E-02  0.0000E+00  5.0116E-01  1.0000E-04
 mraqcc  #  7  6    -45.4788629963  1.2860E-02  0.0000E+00  5.5847E-01  1.0000E-04
 mraqcc  #  7  7    -45.3893760171  1.1151E-01  0.0000E+00  6.2858E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   8

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:        1490 2x:         305 4x:         106
All internal counts: zz :        5410 yy:        2859 xx:         390 ww:         640
One-external counts: yz :        4477 yx:        1070 yw:        1295
Two-external counts: yy :         900 ww:         280 xx:         200 xz:         285 wz:         435 wx:         360
Three-ext.   counts: yx :         130 yw:         160

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1    -0.99342034
   ht   2    -0.06563569    -0.05041005
   ht   3     0.00566899    -0.00304803    -0.00331730
   ht   4    -0.00128490    -0.00198045    -0.00046230    -0.00044176
   ht   5     0.00248243    -0.00019293    -0.00044011    -0.00000108    -0.00017404
   ht   6    -0.00054835    -0.00017877     0.00005205     0.00003024     0.00001452    -0.00002516
   ht   7     0.00006872     0.00005124     0.00000141     0.00000788    -0.00001637     0.00000182    -0.00000383
   ht   8     0.00036471     0.00002452    -0.00000634    -0.00000149    -0.00000562     0.00000465    -0.00000093    -0.00000302
calca4: root=   1 anorm**2=  0.10456462 scale:  1.05098269
calca4: root=   2 anorm**2=  0.35289195 scale:  1.16313883
calca4: root=   3 anorm**2=  0.93061707 scale:  1.38946647
calca4: root=   4 anorm**2=  0.66953314 scale:  1.29210415
calca4: root=   5 anorm**2=  0.45137688 scale:  1.20473104
calca4: root=   6 anorm**2=  0.83771794 scale:  1.35562456
calca4: root=   7 anorm**2=  0.92410823 scale:  1.38712228
calca4: root=   8 anorm**2=  0.88289595 scale:  1.37218656

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1    1.00000      -2.344327E-14  -5.730981E-03   1.394390E-03  -2.479592E-03   5.596373E-04  -7.168678E-05  -3.671513E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1  -0.954924      -0.122581       0.209703      -7.388222E-02  -3.287742E-02   0.232217       0.105940      -8.601454E-02
 civs   2  -0.866334      -4.769746E-02  -0.612881       0.890613       0.650419       -1.67017       -1.82723       0.320526    
 civs   3   -1.28964       -1.84203       -7.56902        2.69101        1.58955        6.95098      -0.712903        8.06787    
 civs   4   -1.13200       -5.80104       -14.5589        4.78147       -6.88517       -11.1329        23.3726       -6.05435    
 civs   5   -1.00571       -18.5754       -17.5759       -25.3463        12.4624        1.97026       -1.39093       -50.5796    
 civs   6    1.38562        72.5766       -5.68533        59.6243        116.802        18.4881        71.2446       -9.56809    
 civs   7   -1.48549       -151.039        95.3895       -110.265        117.084       -126.592        78.7203        247.904    
 civs   8   -1.24700       -226.570        216.496        379.492        141.165        101.668        73.0751       -32.1427    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.945278       6.057577E-02   0.186854      -0.117847      -7.734484E-02   0.154066       0.153463      -2.660165E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.94527830     0.06057577     0.18685441    -0.11784685    -0.07734484     0.15406564     0.15346322    -0.02660165
 fact= -8.746584616666664E-003
 factd= -8.746584616666664E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  8  1    -46.3313507594  1.6396E-06  4.0733E-07  5.5804E-04  1.0000E-04
 mraqcc  #  8  2    -46.2189398270  5.5299E-02  0.0000E+00  1.2039E-01  1.0000E-04
 mraqcc  #  8  3    -46.0661681432  7.9469E-02  0.0000E+00  1.4571E-01  1.0000E-04
 mraqcc  #  8  4    -45.7807665535  5.3154E-02  0.0000E+00  4.7705E-01  1.0000E-04
 mraqcc  #  8  5    -45.7206561206  1.4496E-01  0.0000E+00  5.8645E-01  1.0000E-04
 mraqcc  #  8  6    -45.5649071982  8.6044E-02  0.0000E+00  5.2057E-01  1.0000E-04
 mraqcc  #  8  7    -45.4723299828  8.2954E-02  0.0000E+00  5.4719E-01  1.0000E-04
 mraqcc  #  8  8    -45.3878622477  1.0999E-01  0.0000E+00  6.3166E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   9

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:        1490 2x:         305 4x:         106
All internal counts: zz :        5410 yy:        2859 xx:         390 ww:         640
One-external counts: yz :        4477 yx:        1070 yw:        1295
Two-external counts: yy :         900 ww:         280 xx:         200 xz:         285 wz:         435 wx:         360
Three-ext.   counts: yx :         130 yw:         160

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1    -0.99342034
   ht   2    -0.06563569    -0.05041005
   ht   3     0.00566899    -0.00304803    -0.00331730
   ht   4    -0.00128490    -0.00198045    -0.00046230    -0.00044176
   ht   5     0.00248243    -0.00019293    -0.00044011    -0.00000108    -0.00017404
   ht   6    -0.00054835    -0.00017877     0.00005205     0.00003024     0.00001452    -0.00002516
   ht   7     0.00006872     0.00005124     0.00000141     0.00000788    -0.00001637     0.00000182    -0.00000383
   ht   8     0.00036471     0.00002452    -0.00000634    -0.00000149    -0.00000562     0.00000465    -0.00000093    -0.00000302
   ht   9    -0.00004167    -0.00000312     0.00000588    -0.00000574     0.00000133     0.00000013     0.00000037     0.00000040

                ht   9
   ht   9    -0.00000039
calca4: root=   1 anorm**2=  0.10451687 scale:  1.05095998
calca4: root=   2 anorm**2=  0.27432508 scale:  1.12886008
calca4: root=   3 anorm**2=  0.95199375 scale:  1.39713770
calca4: root=   4 anorm**2=  0.92527293 scale:  1.38754205
calca4: root=   5 anorm**2=  0.24543440 scale:  1.11599032
calca4: root=   6 anorm**2=  0.92139101 scale:  1.38614249
calca4: root=   7 anorm**2=  0.80196127 scale:  1.34237151
calca4: root=   8 anorm**2=  0.68472592 scale:  1.29796992
calca4: root=   9 anorm**2=  0.97092832 scale:  1.40389755

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1    1.00000      -2.344327E-14  -5.730981E-03   1.394390E-03  -2.479592E-03   5.596373E-04  -7.168678E-05  -3.671513E-04

              civs   9
 refs   1   4.196552E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1   0.955109      -0.145397       0.177069       0.102455      -3.335481E-02   0.155336       0.217241       1.197739E-02
 civs   2   0.866321      -2.044709E-02  -0.492427      -0.854958       0.271908       -1.37703       -1.88924        1.01801    
 civs   3    1.28977       -1.27108       -6.76229       -5.60428      -0.151636        4.99561        2.24384       -1.07067    
 civs   4    1.13278       -4.11250       -14.7883       -1.74006        8.42638       -7.39442        5.17756       -20.4311    
 civs   5    1.00938       -14.7624       -21.9229        10.4511       -25.4042       -4.91503        12.8471        24.3932    
 civs   6   -1.41181        62.7140        22.1672       -97.8088       -33.5452       -66.6016        88.8424       -23.2296    
 civs   7    1.59691       -149.097        52.4861        86.5172       -136.268       -128.556       -56.7752       -177.611    
 civs   8    1.58265       -267.129        205.213       -191.786        204.001       -144.054        234.263        71.2390    
 civs   9   -1.08345        231.783       -282.274        606.456        187.741       -421.818        407.430        346.463    

                ci   9
 civs   1   5.378224E-02
 civs   2  -0.535056    
 civs   3   -8.70484    
 civs   4    15.7378    
 civs   5    42.2351    
 civs   6   -4.87591    
 civs   7   -204.029    
 civs   8   -82.9017    
 civs   9   -538.051    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.945263       4.634630E-02   0.171016       0.141157      -3.376920E-02   0.135714       0.164623      -8.274449E-02

              v      9
 ref    1   4.064362E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.94526321     0.04634630     0.17101553     0.14115703    -0.03376920     0.13571411     0.16462270    -0.08274449

                ci   9
 ref:   1     0.04064362
 fact= -8.746584616666664E-003
 factd= -8.746584616666664E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  9  1    -46.3313512007  4.4133E-07  7.8949E-08  2.5360E-04  1.0000E-04
 mraqcc  #  9  2    -46.2381402354  1.9200E-02  0.0000E+00  5.5138E-02  1.0000E-04
 mraqcc  #  9  3    -46.0923013021  2.6133E-02  0.0000E+00  7.9651E-02  1.0000E-04
 mraqcc  #  9  4    -45.8792921549  9.8526E-02  0.0000E+00  3.1006E-01  1.0000E-04
 mraqcc  #  9  5    -45.7560246311  3.5369E-02  0.0000E+00  5.8963E-01  1.0000E-04
 mraqcc  #  9  6    -45.6137675873  4.8860E-02  0.0000E+00  4.5673E-01  1.0000E-04
 mraqcc  #  9  7    -45.5087444331  3.6414E-02  0.0000E+00  5.6968E-01  1.0000E-04
 mraqcc  #  9  8    -45.4482375884  6.0375E-02  0.0000E+00  6.4093E-01  1.0000E-04
 mraqcc  #  9  9    -45.3318607472  5.3992E-02  0.0000E+00  6.0940E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000999
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  10

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:        1490 2x:         305 4x:         106
All internal counts: zz :        5410 yy:        2859 xx:         390 ww:         640
One-external counts: yz :        4477 yx:        1070 yw:        1295
Two-external counts: yy :         900 ww:         280 xx:         200 xz:         285 wz:         435 wx:         360
Three-ext.   counts: yx :         130 yw:         160

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1    -0.99342034
   ht   2    -0.06563569    -0.05041005
   ht   3     0.00566899    -0.00304803    -0.00331730
   ht   4    -0.00128490    -0.00198045    -0.00046230    -0.00044176
   ht   5     0.00248243    -0.00019293    -0.00044011    -0.00000108    -0.00017404
   ht   6    -0.00054835    -0.00017877     0.00005205     0.00003024     0.00001452    -0.00002516
   ht   7     0.00006872     0.00005124     0.00000141     0.00000788    -0.00001637     0.00000182    -0.00000383
   ht   8     0.00036471     0.00002452    -0.00000634    -0.00000149    -0.00000562     0.00000465    -0.00000093    -0.00000302
   ht   9    -0.00004167    -0.00000312     0.00000588    -0.00000574     0.00000133     0.00000013     0.00000037     0.00000040
   ht  10     0.00001435     0.00000071    -0.00000313     0.00000071    -0.00000011     0.00000000    -0.00000002    -0.00000023

                ht   9         ht  10
   ht   9    -0.00000039
   ht  10     0.00000005    -0.00000009
calca4: root=   1 anorm**2=  0.10450907 scale:  1.05095626
calca4: root=   2 anorm**2=  0.25489583 scale:  1.12022133
calca4: root=   3 anorm**2=  0.95311926 scale:  1.39754043
calca4: root=   4 anorm**2=  0.91076835 scale:  1.38230545
calca4: root=   5 anorm**2=  0.22590323 scale:  1.10720514
calca4: root=   6 anorm**2=  0.90043845 scale:  1.37856391
calca4: root=   7 anorm**2=  0.64453680 scale:  1.28239495
calca4: root=   8 anorm**2=  0.87569997 scale:  1.36956196
calca4: root=   9 anorm**2=  0.66027557 scale:  1.28851681
calca4: root=  10 anorm**2=  0.98385330 scale:  1.40849327

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1    1.00000      -2.344327E-14  -5.730981E-03   1.394390E-03  -2.479592E-03   5.596373E-04  -7.168678E-05  -3.671513E-04

              civs   9       civs  10
 refs   1   4.196552E-05  -1.442764E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1  -0.955146      -0.148932      -0.151442      -0.126613      -2.244663E-02   0.173123       1.269751E-02   0.211227    
 civs   2  -0.866317      -1.438750E-02   0.417204       0.718134       0.148552       -1.66468       6.011853E-03   -1.85890    
 civs   3   -1.28979       -1.12589        6.04715        6.15790      -0.491228        3.18371        3.13462        2.19031    
 civs   4   -1.13290       -3.59474        14.2428        4.62310        8.29222       -6.01681       -4.70178        8.11182    
 civs   5   -1.00994       -13.3393        23.4316       -6.08773       -23.9328        3.98934       -11.6054        10.7553    
 civs   6    1.41577        58.1067       -35.7503        80.2933       -42.9736       -64.5154       -44.1222        93.1045    
 civs   7   -1.61376       -143.644       -24.4049       -112.859       -131.584       -74.5977       -95.9192       -80.1346    
 civs   8   -1.63343       -269.616       -181.653        62.1600        170.662       -223.554       -19.0085        280.374    
 civs   9    1.24737        277.468        339.099       -462.425        248.552       -12.8483       -658.438        128.912    
 civs  10  -0.845757       -273.381       -447.689        1034.46        183.715        1218.70       -1360.24       -944.950    

                ci   9         ci  10
 civs   1  -3.286868E-02   3.115782E-02
 civs   2  -0.847934      -0.424901    
 civs   3    1.00762       -9.31163    
 civs   4    19.4099        15.9806    
 civs   5   -26.1971        41.9821    
 civs   6    13.8151       -18.9970    
 civs   7    192.606       -186.683    
 civs   8   -104.251       -157.071    
 civs   9   -331.544       -525.460    
 civs  10    197.411        615.098    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.945258       4.297816E-02  -0.155213      -0.144490      -1.822088E-02   0.169794      -1.890344E-03   0.157269    

              v      9       v     10
 ref    1   6.881816E-02   3.220202E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.94525781     0.04297816    -0.15521308    -0.14448970    -0.01822088     0.16979402    -0.00189034     0.15726948

                ci   9         ci  10
 ref:   1     0.06881816     0.03220202
 fact= -8.746584616666664E-003
 factd= -8.746584616666664E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  # 10  1    -46.3313512675  6.6771E-08  1.4318E-08  1.0086E-04  1.0000E-04
 mraqcc  # 10  2    -46.2442342663  6.0940E-03  0.0000E+00  3.5780E-02  1.0000E-04
 mraqcc  # 10  3    -46.1046810763  1.2380E-02  0.0000E+00  5.5941E-02  1.0000E-04
 mraqcc  # 10  4    -45.9526433035  7.3351E-02  0.0000E+00  2.6565E-01  1.0000E-04
 mraqcc  # 10  5    -45.7576169421  1.5923E-03  0.0000E+00  5.9359E-01  1.0000E-04
 mraqcc  # 10  6    -45.6330152617  1.9248E-02  0.0000E+00  4.9562E-01  1.0000E-04
 mraqcc  # 10  7    -45.5883257357  7.9581E-02  0.0000E+00  5.3732E-01  1.0000E-04
 mraqcc  # 10  8    -45.4836716817  3.5434E-02  0.0000E+00  5.6829E-01  1.0000E-04
 mraqcc  # 10  9    -45.4473953148  1.1553E-01  0.0000E+00  6.4893E-01  1.0000E-04
 mraqcc  # 10 10    -45.3134136824  3.5545E-02  0.0000E+00  6.1478E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.002000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration  11

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:        1490 2x:         305 4x:         106
All internal counts: zz :        5410 yy:        2859 xx:         390 ww:         640
One-external counts: yz :        4477 yx:        1070 yw:        1295
Two-external counts: yy :         900 ww:         280 xx:         200 xz:         285 wz:         435 wx:         360
Three-ext.   counts: yx :         130 yw:         160

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1    -0.99342034
   ht   2    -0.06563569    -0.05041005
   ht   3     0.00566899    -0.00304803    -0.00331730
   ht   4    -0.00128490    -0.00198045    -0.00046230    -0.00044176
   ht   5     0.00248243    -0.00019293    -0.00044011    -0.00000108    -0.00017404
   ht   6    -0.00054835    -0.00017877     0.00005205     0.00003024     0.00001452    -0.00002516
   ht   7     0.00006872     0.00005124     0.00000141     0.00000788    -0.00001637     0.00000182    -0.00000383
   ht   8     0.00036471     0.00002452    -0.00000634    -0.00000149    -0.00000562     0.00000465    -0.00000093    -0.00000302
   ht   9    -0.00004167    -0.00000312     0.00000588    -0.00000574     0.00000133     0.00000013     0.00000037     0.00000040
   ht  10     0.00001435     0.00000071    -0.00000313     0.00000071    -0.00000011     0.00000000    -0.00000002    -0.00000023
   ht  11    -0.00000500     0.00000125    -0.00000022     0.00000034     0.00000004     0.00000006     0.00000001     0.00000002

                ht   9         ht  10         ht  11
   ht   9    -0.00000039
   ht  10     0.00000005    -0.00000009
   ht  11    -0.00000003     0.00000001    -0.00000002
calca4: root=   1 anorm**2=  0.10450787 scale:  1.05095569
calca4: root=   2 anorm**2=  0.23110347 scale:  1.10955102
calca4: root=   3 anorm**2=  0.90476529 scale:  1.38013234
calca4: root=   4 anorm**2=  0.83195610 scale:  1.35349773
calca4: root=   5 anorm**2=  0.73284163 scale:  1.31637443
calca4: root=   6 anorm**2=  0.22721052 scale:  1.10779534
calca4: root=   7 anorm**2=  0.91872880 scale:  1.38518186
calca4: root=   8 anorm**2=  0.51946189 scale:  1.23266455
calca4: root=   9 anorm**2=  0.91293690 scale:  1.38308962
calca4: root=  10 anorm**2=  0.70445392 scale:  1.30554736
calca4: root=  11 anorm**2=  0.94827022 scale:  1.39580451

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1    1.00000      -2.344327E-14  -5.730981E-03   1.394390E-03  -2.479592E-03   5.596373E-04  -7.168678E-05  -3.671513E-04

              civs   9       civs  10       civs  11
 refs   1   4.196552E-05  -1.442764E-05   4.944591E-06

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1  -0.955157      -0.149656       0.114357       0.150444      -6.527663E-02   2.033820E-02  -0.172825       2.068572E-02
 civs   2  -0.866316      -1.282638E-02  -0.310773      -0.597760       0.721680      -0.123375        1.57752      -0.187762    
 civs   3   -1.28979       -1.05669       -4.95828       -6.05137        4.37431       0.630020       -4.33773       -1.47989    
 civs   4   -1.13292       -3.37812       -12.3972       -8.88874       -2.44524       -8.39899        7.96596        1.21016    
 civs   5   -1.01004       -12.6275       -22.9996       -3.57073       -11.9071        23.5439       -1.05318        11.0939    
 civs   6    1.41653        55.4383        47.1472       -48.0021        66.1105        45.1160        61.1224        48.5602    
 civs   7   -1.61700       -139.680       -6.18102        106.594       -64.8501        129.661        97.4428        65.6114    
 civs   8   -1.64320       -267.283        137.946        81.4612        220.130       -162.997        183.633        53.7548    
 civs   9    1.27890        291.897       -358.217        199.131       -389.173       -260.935        129.892        651.855    
 civs  10   -1.00842       -363.282        734.583       -976.789       -204.265       -197.345       -913.747        1363.27    
 civs  11   0.896875        455.865       -1327.03        2467.56        3840.79        130.973       -674.051        963.388    

                ci   9         ci  10         ci  11
 civs   1  -0.202610       6.638299E-02  -1.187375E-02
 civs   2    1.97180       0.494551       0.438018    
 civs   3   -1.81003       -2.60741        8.69517    
 civs   4   -11.8685       -14.6545       -18.1343    
 civs   5   -8.03448        34.4299       -35.7835    
 civs   6   -89.8071       -10.7518        12.9296    
 civs   7    56.1692       -231.399        148.280    
 civs   8   -246.039        97.0246        162.352    
 civs   9   -43.7695        247.532        603.962    
 civs  10    942.496       -353.498       -790.187    
 civs  11    364.096       -971.285       -1461.53    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1  -0.945255       4.191701E-02   0.126504       0.151820      -9.779852E-02   1.497954E-02  -0.159146       1.853273E-02

              v      9       v     10       v     11
 ref    1  -0.166450      -3.884635E-02  -3.174544E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.94525549     0.04191701     0.12650450     0.15182022    -0.09779852     0.01497954    -0.15914560     0.01853273

                ci   9         ci  10         ci  11
 ref:   1    -0.16645034    -0.03884635    -0.03174544
 fact= -8.746584616666664E-003
 factd= -8.746584616666664E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  # 11  1    -46.3313512803  1.2842E-08  3.8376E-09  5.1605E-05  1.0000E-04
 mraqcc  # 11  2    -46.2468509170  2.6167E-03  0.0000E+00  3.4822E-02  1.0000E-04
 mraqcc  # 11  3    -46.1185644370  1.3883E-02  0.0000E+00  9.0997E-02  1.0000E-04
 mraqcc  # 11  4    -46.0238191413  7.1176E-02  0.0000E+00  2.5229E-01  1.0000E-04
 mraqcc  # 11  5    -45.7843569484  2.6740E-02  0.0000E+00  4.4713E-01  1.0000E-04
 mraqcc  # 11  6    -45.7575864938  1.2457E-01  0.0000E+00  5.9239E-01  1.0000E-04
 mraqcc  # 11  7    -45.6278958110  3.9570E-02  0.0000E+00  4.7502E-01  1.0000E-04
 mraqcc  # 11  8    -45.5780576332  9.4386E-02  0.0000E+00  5.8403E-01  1.0000E-04
 mraqcc  # 11  9    -45.4809199190  3.3525E-02  0.0000E+00  5.5405E-01  1.0000E-04
 mraqcc  # 11 10    -45.4312525807  1.1784E-01  0.0000E+00  6.4613E-01  1.0000E-04
 mraqcc  # 11 11    -45.2753323967 -2.5361E-03  0.0000E+00  6.2107E-01  1.0000E-04
 
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.002000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mraqcc   convergence criteria satisfied after 11 iterations.

 final mraqcc   convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  # 11  1    -46.3313512803  1.2842E-08  3.8376E-09  5.1605E-05  1.0000E-04
 mraqcc  # 11  2    -46.2468509170  2.6167E-03  0.0000E+00  3.4822E-02  1.0000E-04
 mraqcc  # 11  3    -46.1185644370  1.3883E-02  0.0000E+00  9.0997E-02  1.0000E-04
 mraqcc  # 11  4    -46.0238191413  7.1176E-02  0.0000E+00  2.5229E-01  1.0000E-04
 mraqcc  # 11  5    -45.7843569484  2.6740E-02  0.0000E+00  4.4713E-01  1.0000E-04
 mraqcc  # 11  6    -45.7575864938  1.2457E-01  0.0000E+00  5.9239E-01  1.0000E-04
 mraqcc  # 11  7    -45.6278958110  3.9570E-02  0.0000E+00  4.7502E-01  1.0000E-04
 mraqcc  # 11  8    -45.5780576332  9.4386E-02  0.0000E+00  5.8403E-01  1.0000E-04
 mraqcc  # 11  9    -45.4809199190  3.3525E-02  0.0000E+00  5.5405E-01  1.0000E-04
 mraqcc  # 11 10    -45.4312525807  1.1784E-01  0.0000E+00  6.4613E-01  1.0000E-04
 mraqcc  # 11 11    -45.2753323967 -2.5361E-03  0.0000E+00  6.2107E-01  1.0000E-04

####################CIUDGINFO####################

   aqcc(lrt) vector at position   1 energy=  -46.331351280296

################END OF CIUDGINFO################

 
 a4den factor for root 1  =  1.017726742
diagon:itrnv=   0
    1 of the  12 expansion vectors are transformed.
    1 of the  11 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
maximum overlap with reference    1(overlap= 0.94526)
reference energy =                         -46.2712888828
total aqcc energy =                        -46.3313512803
diagonal element shift (lrtshift) =         -0.0600623974

 information on vector: 1 from unit 11 written to unit 48 filename civout              


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -46.3313512803

                                                       internal orbitals

                                          level       1    2    3    4    5    6

                                          orbital     5    6    7    8    9   10

                                         symmetry   a    a    a    a    a    a  

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1 -0.475524                        +-   +-                     
 z*  1  1       6 -0.474973                        +-        +-                
 z*  1  1      10  0.238051                        +-             +-           
 z*  1  1      13  0.237460                        +-                  +-      
 z*  1  1      15  0.475048                        +-                       +- 
 z*  1  1      19  0.158603                        +    +-                   - 
 z*  1  1      25  0.148629                        +     -        +     -      
 z*  1  1      38  0.158404                        +         +-              - 
 z*  1  1      39  0.104999                        +          -   +-           
 z*  1  1      42 -0.104823                        +          -        +-      
 z*  1  1      49 -0.022971                        +              +-         - 
 z*  1  1      54 -0.023279                        +                   +-    - 
 z*  1  1      56  0.019167                             +-   +-                
 z*  1  1      60  0.025313                             +-        +-           
 z*  1  1      63  0.025376                             +-             +-      
 z*  1  1      75  0.023738                             +    +     -    -      
 z*  1  1      81 -0.011902                             +          -   +     - 
 z*  1  1      83 -0.020543                             +         +     -    - 
 z*  1  1      86  0.025299                                  +-   +-           
 z*  1  1      89  0.025293                                  +-        +-      
 z*  1  1      93 -0.016679                                  +    +-         - 
 z*  1  1      98  0.016840                                  +         +-    - 
 z*  1  1     100 -0.050674                                       +-   +-      
 z*  1  1     102 -0.034898                                       +-        +- 
 z*  1  1     105 -0.034868                                            +-   +- 
 y   1  1     109 -0.026229              4( a  )   +-    -                     
 y   1  1     119  0.022772             14( a  )   +-    -                     
 y   1  1     138  0.026182              5( a  )   +-         -                
 y   1  1     146  0.022735             13( a  )   +-         -                
 y   1  1     167 -0.013127              6( a  )   +-              -           
 y   1  1     176  0.011398             15( a  )   +-              -           
 y   1  1     196 -0.013095              7( a  )   +-                   -      
 y   1  1     205  0.011369             16( a  )   +-                   -      
 y   1  1     225 -0.026184              8( a  )   +-                        - 
 y   1  1     229 -0.056804             12( a  )   +-                        - 
 y   1  1     234 -0.022738             17( a  )   +-                        - 
 y   1  1     253 -0.018460              8( a  )    -   +-                     
 y   1  1     257  0.025580             12( a  )    -   +-                     
 y   1  1     262 -0.021047             17( a  )    -   +-                     
 y   1  1     361 -0.012595              4( a  )    -   +                    - 
 y   1  1     393 -0.018434              8( a  )    -        +-                
 y   1  1     397  0.025537             12( a  )    -        +-                
 y   1  1     402 -0.021015             17( a  )    -        +-                
 y   1  1     474  0.012587              5( a  )    -        +               - 
 y   1  1     502  0.013837              5( a  )    -             +-           
 y   1  1     509 -0.012805             12( a  )    -             +-           
 y   1  1     510  0.013100             13( a  )    -             +-           
 y   1  1     529 -0.019607              4( a  )    -             +     -      
 y   1  1     539  0.018520             14( a  )    -             +     -      
 y   1  1     586 -0.013857              5( a  )    -                  +-      
 y   1  1     593 -0.012773             12( a  )    -                  +-      
 y   1  1     594 -0.013066             13( a  )    -                  +-      
 y   1  1     649 -0.025541             12( a  )    -                       +- 
 y   1  1     700 -0.013850              7( a  )   +     -         -           
 y   1  1     709  0.015305             16( a  )   +     -         -           
 y   1  1     727 -0.013838              6( a  )   +     -              -      
 y   1  1     736  0.015292             15( a  )   +     -              -      
 y   1  1     753  0.021466              4( a  )   +     -                   - 
 y   1  1     763 -0.024461             14( a  )   +     -                   - 
 y   1  1     783 -0.013822              6( a  )   +          -    -           
 y   1  1     792  0.015283             15( a  )   +          -    -           
 y   1  1     812  0.013799              7( a  )   +          -         -      
 y   1  1     821 -0.015257             16( a  )   +          -         -      
 y   1  1     838 -0.021429              5( a  )   +          -              - 
 y   1  1     846 -0.024429             13( a  )   +          -              - 
 y   1  1     988  0.010696             15( a  )        +-         -           
 y   1  1    1017  0.010711             16( a  )        +-              -      
 y   1  1    1520  0.010688             15( a  )             +-    -           
 y   1  1    1549  0.010684             16( a  )             +-         -      
 y   1  1    1848  0.015837              7( a  )                  +-    -      
 y   1  1    1857 -0.014389             16( a  )                  +-    -      
 y   1  1    1877  0.010533              8( a  )                  +-         - 
 y   1  1    1903 -0.015834              6( a  )                   -   +-      
 y   1  1    1912  0.014383             15( a  )                   -   +-      
 y   1  1    1959 -0.012710              6( a  )                   -        +- 
 y   1  1    1968  0.011666             15( a  )                   -        +- 
 y   1  1    2017  0.010538              8( a  )                       +-    - 
 y   1  1    2044 -0.012700              7( a  )                        -   +- 
 y   1  1    2053  0.011660             16( a  )                        -   +- 
 x   1  1    2124  0.019199    4( a  )  12( a  )    -    -                     
 x   1  1    2155  0.019514   12( a  )  14( a  )    -    -                     
 x   1  1    2257  0.011843    2( a  )  21( a  )    -    -                     
 x   1  1    2264  0.012461    9( a  )  21( a  )    -    -                     
 x   1  1    2276 -0.011837    1( a  )  22( a  )    -    -                     
 x   1  1    2285  0.012455   10( a  )  22( a  )    -    -                     
 x   1  1    2299 -0.013634    3( a  )  23( a  )    -    -                     
 x   1  1    2307  0.014252   11( a  )  23( a  )    -    -                     
 x   1  1    2503 -0.019172    5( a  )  12( a  )    -         -                
 x   1  1    2521  0.019487   12( a  )  13( a  )    -         -                
 x   1  1    2634 -0.011822    1( a  )  21( a  )    -         -                
 x   1  1    2643  0.012439   10( a  )  21( a  )    -         -                
 x   1  1    2655 -0.011828    2( a  )  22( a  )    -         -                
 x   1  1    2662 -0.012445    9( a  )  22( a  )    -         -                
 x   1  1    2699  0.013616    3( a  )  24( a  )    -         -                
 x   1  1    2707 -0.014233   11( a  )  24( a  )    -         -                
 x   1  1    3640  0.019174    8( a  )  12( a  )    -                        - 
 x   1  1    3709 -0.019488   12( a  )  17( a  )    -                        - 
 x   1  1    3855 -0.013082    2( a  )  25( a  )    -                        - 
 x   1  1    3862 -0.013485    9( a  )  25( a  )    -                        - 
 x   1  1    3878 -0.013074    1( a  )  26( a  )    -                        - 
 x   1  1    3887  0.013476   10( a  )  26( a  )    -                        - 
 x   1  1    3905 -0.012731    3( a  )  27( a  )    -                        - 
 x   1  1    3913  0.013969   11( a  )  27( a  )    -                        - 
 x   1  1    3956  0.014701    1( a  )   2( a  )         -    -                
 x   1  1    6602  0.018279    1( a  )   2( a  )                   -    -      
 x   1  1    6630  0.011130    1( a  )   9( a  )                   -    -      
 x   1  1    6639  0.011127    2( a  )  10( a  )                   -    -      
 x   1  1    6982 -0.013769    2( a  )   3( a  )                   -         - 
 x   1  1    7359  0.013746    1( a  )   3( a  )                        -    - 
 w   1  1    7736  0.011305    1( a  )   1( a  )   +-                          
 w   1  1    7738  0.011277    2( a  )   2( a  )   +-                          
 w   1  1    7741 -0.022580    3( a  )   3( a  )   +-                          
 w   1  1    7773  0.014527    2( a  )   9( a  )   +-                          
 w   1  1    7781 -0.014563    1( a  )  10( a  )   +-                          
 w   1  1    7793  0.029088    3( a  )  11( a  )   +-                          
 w   1  1    7801 -0.019811   11( a  )  11( a  )   +-                          
 w   1  1    7818  0.013453    5( a  )  13( a  )   +-                          
 w   1  1    7826  0.016369   13( a  )  13( a  )   +-                          
 w   1  1    7830 -0.013474    4( a  )  14( a  )   +-                          
 w   1  1    7840  0.016394   14( a  )  14( a  )   +-                          
 w   1  1    7879 -0.013455    8( a  )  17( a  )   +-                          
 w   1  1    7888 -0.016369   17( a  )  17( a  )   +-                          
 w   1  1    7928 -0.011613    3( a  )  20( a  )   +-                          
 w   1  1    7936  0.013136   11( a  )  20( a  )   +-                          
 w   1  1    7966  0.019905   21( a  )  21( a  )   +-                          
 w   1  1    7988  0.019905   22( a  )  22( a  )   +-                          
 w   1  1    8060 -0.011949   25( a  )  25( a  )   +-                          
 w   1  1    8086 -0.011930   26( a  )  26( a  )   +-                          
 w   1  1    8113 -0.015917   27( a  )  27( a  )   +-                          
 w   1  1    8143 -0.037588    1( a  )   2( a  )   +     -                     
 w   1  1    8178 -0.026909    1( a  )   9( a  )   +     -                     
 w   1  1    8188  0.026910    2( a  )  10( a  )   +     -                     
 w   1  1    8195  0.020621    9( a  )  10( a  )   +     -                     
 w   1  1    8211  0.011188    4( a  )  12( a  )   +     -                     
 w   1  1    8360 -0.010589    9( a  )  21( a  )   +     -                     
 w   1  1    8382 -0.010589   10( a  )  22( a  )   +     -                     
 w   1  1    8548 -0.026568    1( a  )   1( a  )   +          -                
 w   1  1    8550  0.026524    2( a  )   2( a  )   +          -                
 w   1  1    8585  0.026854    2( a  )   9( a  )   +          -                
 w   1  1    8592  0.014551    9( a  )   9( a  )   +          -                
 w   1  1    8593  0.026900    1( a  )  10( a  )   +          -                
 w   1  1    8602 -0.014576   10( a  )  10( a  )   +          -                
 w   1  1    8618 -0.011172    5( a  )  12( a  )   +          -                
 w   1  1    8767 -0.010577   10( a  )  21( a  )   +          -                
 w   1  1    8787  0.010577    9( a  )  22( a  )   +          -                
 w   1  1    8958 -0.018814    2( a  )   3( a  )   +               -           
 w   1  1    8992 -0.012544    3( a  )   9( a  )   +               -           
 w   1  1    9010  0.014394    2( a  )  11( a  )   +               -           
 w   1  1    9017  0.010321    9( a  )  11( a  )   +               -           
 w   1  1    9363  0.018767    1( a  )   3( a  )   +                    -      
 w   1  1    9407 -0.012510    3( a  )  10( a  )   +                    -      
 w   1  1    9415 -0.014361    1( a  )  11( a  )   +                    -      
 w   1  1    9424  0.010295   10( a  )  11( a  )   +                    -      
 w   1  1    9771  0.040346    3( a  )   3( a  )   +                         - 
 w   1  1    9823 -0.037564    3( a  )  11( a  )   +                         - 
 w   1  1    9831  0.019064   11( a  )  11( a  )   +                         - 
 w   1  1    9839  0.011173    8( a  )  12( a  )   +                         - 
 w   1  1   10119  0.011581    3( a  )  27( a  )   +                         - 
 w   1  1   10127 -0.012731   11( a  )  27( a  )   +                         - 
 w   1  1   10172  0.036507    1( a  )   1( a  )        +-                     
 w   1  1   10174  0.036500    2( a  )   2( a  )        +-                     
 w   1  1   10177  0.066465    3( a  )   3( a  )        +-                     
 w   1  1   10209  0.029124    2( a  )   9( a  )        +-                     
 w   1  1   10216  0.013755    9( a  )   9( a  )        +-                     
 w   1  1   10217 -0.029129    1( a  )  10( a  )        +-                     
 w   1  1   10226  0.013757   10( a  )  10( a  )        +-                     
 w   1  1   10229 -0.056194    3( a  )  11( a  )        +-                     
 w   1  1   10237  0.026961   11( a  )  11( a  )        +-                     
 w   1  1   10249  0.012906   12( a  )  12( a  )        +-                     
 w   1  1   11797 -0.015234    1( a  )   2( a  )        +                    - 
 w   1  1   12202  0.036448    1( a  )   1( a  )             +-                
 w   1  1   12204  0.036473    2( a  )   2( a  )             +-                
 w   1  1   12207  0.066380    3( a  )   3( a  )             +-                
 w   1  1   12239  0.029102    2( a  )   9( a  )             +-                
 w   1  1   12246  0.013744    9( a  )   9( a  )             +-                
 w   1  1   12247 -0.029079    1( a  )  10( a  )             +-                
 w   1  1   12256  0.013733   10( a  )  10( a  )             +-                
 w   1  1   12259 -0.056120    3( a  )  11( a  )             +-                
 w   1  1   12267  0.026926   11( a  )  11( a  )             +-                
 w   1  1   12279  0.012890   12( a  )  12( a  )             +-                
 w   1  1   13420 -0.010902    1( a  )   1( a  )             +               - 
 w   1  1   13422  0.010620    2( a  )   2( a  )             +               - 
 w   1  1   13826 -0.033267    1( a  )   1( a  )                  +-           
 w   1  1   13828 -0.013911    2( a  )   2( a  )                  +-           
 w   1  1   13831 -0.022632    3( a  )   3( a  )                  +-           
 w   1  1   13863 -0.011686    2( a  )   9( a  )                  +-           
 w   1  1   13871  0.028126    1( a  )  10( a  )                  +-           
 w   1  1   13880 -0.013495   10( a  )  10( a  )                  +-           
 w   1  1   13883  0.017472    3( a  )  11( a  )                  +-           
 w   1  1   14233 -0.019377    1( a  )   2( a  )                  +     -      
 w   1  1   14268 -0.011638    1( a  )   9( a  )                  +     -      
 w   1  1   14278  0.011639    2( a  )  10( a  )                  +     -      
 w   1  1   15044 -0.013862    1( a  )   1( a  )                       +-      
 w   1  1   15046 -0.033184    2( a  )   2( a  )                       +-      
 w   1  1   15049 -0.022590    3( a  )   3( a  )                       +-      
 w   1  1   15081 -0.028056    2( a  )   9( a  )                       +-      
 w   1  1   15088 -0.013461    9( a  )   9( a  )                       +-      
 w   1  1   15089  0.011647    1( a  )  10( a  )                       +-      
 w   1  1   15101  0.017438    3( a  )  11( a  )                       +-      
 w   1  1   15856 -0.047622    1( a  )   1( a  )                            +- 
 w   1  1   15858 -0.047620    2( a  )   2( a  )                            +- 
 w   1  1   15861 -0.044063    3( a  )   3( a  )                            +- 
 w   1  1   15893 -0.039857    2( a  )   9( a  )                            +- 
 w   1  1   15900 -0.019203    9( a  )   9( a  )                            +- 
 w   1  1   15901  0.039861    1( a  )  10( a  )                            +- 
 w   1  1   15910 -0.019205   10( a  )  10( a  )                            +- 
 w   1  1   15913  0.034590    3( a  )  11( a  )                            +- 
 w   1  1   15921 -0.015997   11( a  )  11( a  )                            +- 
 w   1  1   15933 -0.012890   12( a  )  12( a  )                            +- 

 ci coefficient statistics:
           rq > 0.1               10
      0.1> rq > 0.01             199
     0.01> rq > 0.001            878
    0.001> rq > 0.0001           719
   0.0001> rq > 0.00001          436
  0.00001> rq > 0.000001         774
 0.000001> rq                  13245
           all                 16261
  iref  icsf         v(icsf)             hv(icsf)
     1     1     -0.475523658299
     2     2     -0.000000605913
     3     3     -0.000020956406
     4     4     -0.000000026437
     5     5     -0.000011628093
     6     6     -0.474972560887
     7     7     -0.000000028921
     8     8      0.000021346903
     9     9      0.001487859077
    10    10      0.238050800116
    11    11     -0.000010691508
    12    12     -0.000000038558
    13    13      0.237459806312
    14    14      0.000012388773
    15    15      0.475048456746
    16    16      0.000178847107
    17    17     -0.000000020153
    18    18      0.000003677725
    19    19      0.158602796302
    20    20      0.000001452737
    21    21      0.000004682641
    22    22     -0.000000006972
    23    23      0.000000107495
    24    24      0.000010611128
    25    25      0.148629218175
    26    26     -0.000002816035
    27    27     -0.000014173438
    28    28      0.000000018532
    29    29     -0.000002974318
    30    30     -0.000002673607
    31    31      0.000000003606
    32    32      0.000000184666
    33    33     -0.000106821282
    34    34      0.000004624888
    35    35      0.000000004939
    36    36     -0.000000020608
    37    37      0.000003641191
    38    38      0.158404183557
    39    39      0.104998731941
    40    40     -0.000017130364
    41    41      0.000000019074
    42    42     -0.104823211991
    43    43      0.000002757632
    44    44     -0.000004031073
    45    45     -0.000002715234
    46    46      0.000000005415
    47    47     -0.000004705667
    48    48      0.000003550611
    49    49     -0.022970669726
    50    50     -0.000000009568
    51    51     -0.000001331713
    52    52     -0.000000002876
    53    53     -0.000002308491
    54    54     -0.023278837181
    55    55      0.000000858622
    56    56      0.019167144329
    57    57      0.000000000694
    58    58     -0.000000420379
    59    59     -0.000057981034
    60    60      0.025312668046
    61    61      0.000000542785
    62    62      0.000000004403
    63    63      0.025375732162
    64    64     -0.000000974681
    65    65      0.000027058838
    66    66     -0.000000433364
    67    67     -0.000000000862
    68    68     -0.000000942048
    69    69     -0.000000242809
    70    70      0.000034826808
    71    71     -0.000000005373
    72    72      0.000000348536
    73    73      0.000000000039
    74    74      0.000000035001
    75    75      0.023738261336
    76    76     -0.000001737197
    77    77     -0.000000001299
    78    78     -0.000000003853
    79    79     -0.000002357961
    80    80      0.000001000761
    81    81     -0.011901565310
    82    82      0.000001648463
    83    83     -0.020542507544
    84    84      0.000001591228
    85    85     -0.000000000499
    86    86      0.025299222623
    87    87      0.000001134214
    88    88      0.000000004462
    89    89      0.025292738283
    90    90     -0.000000966992
    91    91     -0.000001389961
    92    92      0.000001052977
    93    93     -0.016678756312
    94    94      0.000000004218
    95    95      0.000001006165
    96    96     -0.000000000400
    97    97      0.000002587196
    98    98      0.016839718523
    99    99     -0.000001665673
   100   100     -0.050674001016
   101   101      0.000000119870
   102   102     -0.034898149135
   103   103      0.000000002986
   104   104     -0.000000105256
   105   105     -0.034868057341

 number of reference csfs (nref) is   105.  root number (iroot) is  1.
 c0**2 =   0.89549213  c0**2(renormalized) =   0.98907811
 c**2 (all zwalks) =   0.89549213  c**2(all zwalks, renormalized) =   0.98907811
 passed aftci ... 
 readint2: molcas,dalton2=                     0                     0
 files%faoints=aoints              
lodens (list->root)=  1
sifcfg setup: record length 4096 DP
# d1 elements per record  3272
# d2 elements per record  2730
  The AQCC density will be calculated.
 item #                     1 suffix=:.drt1.state1:
 read_civout: repnuc=  -45.2778685421096     
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method: 30 last record  1max overlap with ref# 95% root-following 0
 MR-CISD energy:   -46.33135128    -1.05348274
 residuum:     0.00005160
 deltae:     0.00000001
 apxde:     0.00000000
 a4den:     1.01772674
 reference energy:   -46.27128888    -0.99342034

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.94525549     0.04191701     0.12650450     0.15182022    -0.09779852     0.01497954    -0.15914560     0.01853273

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1    -0.16645034    -0.03884635    -0.03174544     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.94525549     0.04191701     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 rdcirefv: extracting reference vector # 1(blocksize=   105 #zcsf=     105)
 =========== Executing IN-CORE method ==========
================================================================================
   DYZ=       0  DYX=       0  DYW=       0
   D0Z=     828  D0Y=       0  D0X=       0  D0W=       0
  DDZI=     365 DDYI=       0 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
 root #                      1 : Scaling(1) with    1.01772674208469      
 corresponding to ref #                      1
 computing final density
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=     430  DYX=     130  DYW=     160
   D0Z=     828  D0Y=     515  D0X=      60  D0W=      90
  DDZI=     365 DDYI=     215 DDXI=      40 DDWI=      50
  DDZE=       0 DDYE=      70 DDXE=      15 DDWE=      21
================================================================================
Trace of MO density:     4.000000
    4  correlated and     8  frozen core electrons

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   4     0.00000000     0.00000000     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.00000000     0.00000000     1.73573086    -0.00000848     0.00051405    -0.00000003
   MO   6     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000848     0.57274556     0.00000106     0.00000797
   MO   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00051405     0.00000106     0.57134429     0.00000001
   MO   8     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000003     0.00000797     0.00000001     0.18886562
   MO   9     0.00000000     0.00000000     0.00000000     0.00000000     0.00000867     0.00000001    -0.00000810    -0.00000815
   MO  10     0.00000000     0.00000000     0.00000000     0.00000000     0.23498746    -0.00000034    -0.00000031    -0.00000004
   MO  11     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  12     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  13     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  14     0.00000000     0.00000000     0.00000000     0.00000000     0.00000081    -0.02721287     0.00019145     0.00000007
   MO  15     0.00000000     0.00000000     0.00000000     0.00000000    -0.00005253     0.00019212     0.02713441    -0.00000007
   MO  16     0.00000000     0.00000000     0.00000000     0.00000000     0.00000023     0.00000111     0.00000015     0.01262883
   MO  17     0.00000000     0.00000000     0.00000000     0.00000000     0.00000110     0.00000002    -0.00000035     0.00000150
   MO  18     0.00000000     0.00000000     0.00000000     0.00000000     0.02993654     0.00000006     0.00010686    -0.00000010
   MO  19     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  20     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  21     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  22     0.00000000     0.00000000     0.00000000     0.00000000    -0.06253132    -0.00000100     0.00006078     0.00000000
   MO  23     0.00000000     0.00000000     0.00000000     0.00000000     0.00013735    -0.00010697     0.02412773     0.00000002
   MO  24     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000040     0.02419098     0.00010679     0.00000019
   MO  25     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000007    -0.00000038     0.00000005    -0.01212730
   MO  26     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000095    -0.00000003     0.00000033    -0.00004645
   MO  27     0.00000000     0.00000000     0.00000000     0.00000000     0.02360996     0.00000014    -0.00008765    -0.00000004
   MO  28     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  29     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  30     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  31     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  32     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  33     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  34     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  35     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  36     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  37     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  38     0.00000000     0.00000000     0.00000000     0.00000000     0.00093181     0.00000005    -0.00000292     0.00000000

                MO   9         MO  10         MO  11         MO  12         MO  13         MO  14         MO  15         MO  16
   MO   5     0.00000867     0.23498746     0.00000000     0.00000000     0.00000000     0.00000081    -0.00005253     0.00000023
   MO   6     0.00000001    -0.00000034     0.00000000     0.00000000     0.00000000    -0.02721287     0.00019212     0.00000111
   MO   7    -0.00000810    -0.00000031     0.00000000     0.00000000     0.00000000     0.00019145     0.02713441     0.00000015
   MO   8    -0.00000815    -0.00000004     0.00000000     0.00000000     0.00000000     0.00000007    -0.00000007     0.01262883
   MO   9     0.18822883     0.00001349     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000029    -0.00000212
   MO  10     0.00001349     0.55288900     0.00000000     0.00000000     0.00000000    -0.00000058    -0.00009956     0.00000019
   MO  11     0.00000000     0.00000000     0.02673416    -0.00000032     0.00000064     0.00000000     0.00000000     0.00000000
   MO  12     0.00000000     0.00000000    -0.00000032     0.02670651     0.00000021     0.00000000     0.00000000     0.00000000
   MO  13     0.00000000     0.00000000     0.00000064     0.00000021     0.04334482     0.00000000     0.00000000     0.00000000
   MO  14     0.00000000    -0.00000058     0.00000000     0.00000000     0.00000000     0.00284035    -0.00000007    -0.00000005
   MO  15    -0.00000029    -0.00009956     0.00000000     0.00000000     0.00000000    -0.00000007     0.00283191     0.00000001
   MO  16    -0.00000212     0.00000019     0.00000000     0.00000000     0.00000000    -0.00000005     0.00000001     0.00179640
   MO  17     0.01260469     0.00000046     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000002    -0.00000002
   MO  18     0.00000047     0.02525662     0.00000000     0.00000000     0.00000000     0.00000000     0.00000046     0.00000001
   MO  19     0.00000000     0.00000000    -0.00002864     0.01757766    -0.00000001     0.00000000     0.00000000     0.00000000
   MO  20     0.00000000     0.00000000    -0.01759508    -0.00002821     0.00000051     0.00000000     0.00000000     0.00000000
   MO  21     0.00000000     0.00000000    -0.00000098    -0.00000023    -0.02805367     0.00000000     0.00000000     0.00000000
   MO  22     0.00000103     0.02780264     0.00000000     0.00000000     0.00000000     0.00000003    -0.00000217     0.00000001
   MO  23    -0.00000022     0.00008380     0.00000000     0.00000000     0.00000000     0.00003266     0.00284221     0.00000002
   MO  24    -0.00000001     0.00000020     0.00000000     0.00000000     0.00000000    -0.00285026     0.00003270     0.00000010
   MO  25     0.00004688    -0.00000007     0.00000000     0.00000000     0.00000000    -0.00000002     0.00000002    -0.00181498
   MO  26    -0.01210732    -0.00000048     0.00000000     0.00000000     0.00000000     0.00000000     0.00000003    -0.00000671
   MO  27     0.00000037     0.02308333     0.00000000     0.00000000     0.00000000    -0.00000016    -0.00002118     0.00000002
   MO  28     0.00000000     0.00000000     0.00000325    -0.00341405     0.00000003     0.00000000     0.00000000     0.00000000
   MO  29     0.00000000     0.00000000    -0.00341745    -0.00000316     0.00000002     0.00000000     0.00000000     0.00000000
   MO  30     0.00000000     0.00000000     0.00000014     0.00000006     0.00545530     0.00000000     0.00000000     0.00000000
   MO  31     0.00000000     0.00000000    -0.00000014     0.00000133     0.00000000     0.00000000     0.00000000     0.00000000
   MO  32     0.00000000     0.00000000     0.00000006    -0.00000656     0.00000000     0.00000000     0.00000000     0.00000000
   MO  33     0.00000000     0.00000000     0.00000000    -0.00000001     0.00000001     0.00000000     0.00000000     0.00000000
   MO  34     0.00000000     0.00000000     0.00000002     0.00000000     0.00000882     0.00000000     0.00000000     0.00000000
   MO  35     0.00000000     0.00000000    -0.00000090     0.00044979    -0.00000001     0.00000000     0.00000000     0.00000000
   MO  36     0.00000000     0.00000000     0.00044557     0.00000101     0.00000021     0.00000000     0.00000000     0.00000000
   MO  37     0.00000000     0.00000000     0.00000014     0.00000006     0.00432323     0.00000000     0.00000000     0.00000000
   MO  38    -0.00000005    -0.00133432     0.00000000     0.00000000     0.00000000     0.00000000     0.00000014     0.00000000

                MO  17         MO  18         MO  19         MO  20         MO  21         MO  22         MO  23         MO  24
   MO   5     0.00000110     0.02993654     0.00000000     0.00000000     0.00000000    -0.06253132     0.00013735    -0.00000040
   MO   6     0.00000002     0.00000006     0.00000000     0.00000000     0.00000000    -0.00000100    -0.00010697     0.02419098
   MO   7    -0.00000035     0.00010686     0.00000000     0.00000000     0.00000000     0.00006078     0.02412773     0.00010679
   MO   8     0.00000150    -0.00000010     0.00000000     0.00000000     0.00000000     0.00000000     0.00000002     0.00000019
   MO   9     0.01260469     0.00000047     0.00000000     0.00000000     0.00000000     0.00000103    -0.00000022    -0.00000001
   MO  10     0.00000046     0.02525662     0.00000000     0.00000000     0.00000000     0.02780264     0.00008380     0.00000020
   MO  11     0.00000000     0.00000000    -0.00002864    -0.01759508    -0.00000098     0.00000000     0.00000000     0.00000000
   MO  12     0.00000000     0.00000000     0.01757766    -0.00002821    -0.00000023     0.00000000     0.00000000     0.00000000
   MO  13     0.00000000     0.00000000    -0.00000001     0.00000051    -0.02805367     0.00000000     0.00000000     0.00000000
   MO  14     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000003     0.00003266    -0.00285026
   MO  15    -0.00000002     0.00000046     0.00000000     0.00000000     0.00000000    -0.00000217     0.00284221     0.00003270
   MO  16    -0.00000002     0.00000001     0.00000000     0.00000000     0.00000000     0.00000001     0.00000002     0.00000010
   MO  17     0.00179467     0.00000003     0.00000000     0.00000000     0.00000000     0.00000005    -0.00000002     0.00000000
   MO  18     0.00000003     0.00271471     0.00000000     0.00000000     0.00000000     0.00123962     0.00002120     0.00000008
   MO  19     0.00000000     0.00000000     0.01206282     0.00000015    -0.00000005     0.00000000     0.00000000     0.00000000
   MO  20     0.00000000     0.00000000     0.00000015     0.01207391     0.00000004     0.00000000     0.00000000     0.00000000
   MO  21     0.00000000     0.00000000    -0.00000005     0.00000004     0.01872657     0.00000000     0.00000000     0.00000000
   MO  22     0.00000005     0.00123962     0.00000000     0.00000000     0.00000000     0.01093764     0.00000698    -0.00000002
   MO  23    -0.00000002     0.00002120     0.00000000     0.00000000     0.00000000     0.00000698     0.00307361    -0.00000003
   MO  24     0.00000000     0.00000008     0.00000000     0.00000000     0.00000000    -0.00000002    -0.00000003     0.00308174
   MO  25     0.00000674     0.00000001     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000003
   MO  26    -0.00181327    -0.00000004     0.00000000     0.00000000     0.00000000    -0.00000005     0.00000003     0.00000000
   MO  27     0.00000003     0.00275431     0.00000000     0.00000000     0.00000000     0.00119988     0.00000013     0.00000000
   MO  28     0.00000000     0.00000000    -0.00266999     0.00000178    -0.00000001     0.00000000     0.00000000     0.00000000
   MO  29     0.00000000     0.00000000     0.00000184     0.00267231     0.00000007     0.00000000     0.00000000     0.00000000
   MO  30     0.00000000     0.00000000     0.00000002     0.00000003    -0.00406202     0.00000000     0.00000000     0.00000000
   MO  31     0.00000000     0.00000000     0.00000099    -0.00000062     0.00000000     0.00000000     0.00000000     0.00000000
   MO  32     0.00000000     0.00000000    -0.00000486    -0.00000018     0.00000000     0.00000000     0.00000000     0.00000000
   MO  33     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  34     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000600     0.00000000     0.00000000     0.00000000
   MO  35     0.00000000     0.00000000     0.00010223     0.00000001     0.00000000     0.00000000     0.00000000     0.00000000
   MO  36     0.00000000     0.00000000     0.00000009    -0.00009913    -0.00000016     0.00000000     0.00000000     0.00000000
   MO  37     0.00000000     0.00000000     0.00000002    -0.00000001    -0.00308408     0.00000000     0.00000000     0.00000000
   MO  38     0.00000000    -0.00008105     0.00000000     0.00000000     0.00000000    -0.00046637    -0.00000048     0.00000000

                MO  25         MO  26         MO  27         MO  28         MO  29         MO  30         MO  31         MO  32
   MO   5    -0.00000007    -0.00000095     0.02360996     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   6    -0.00000038    -0.00000003     0.00000014     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   7     0.00000005     0.00000033    -0.00008765     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   8    -0.01212730    -0.00004645    -0.00000004     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   9     0.00004688    -0.01210732     0.00000037     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  10    -0.00000007    -0.00000048     0.02308333     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  11     0.00000000     0.00000000     0.00000000     0.00000325    -0.00341745     0.00000014    -0.00000014     0.00000006
   MO  12     0.00000000     0.00000000     0.00000000    -0.00341405    -0.00000316     0.00000006     0.00000133    -0.00000656
   MO  13     0.00000000     0.00000000     0.00000000     0.00000003     0.00000002     0.00545530     0.00000000     0.00000000
   MO  14    -0.00000002     0.00000000    -0.00000016     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  15     0.00000002     0.00000003    -0.00002118     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  16    -0.00181498    -0.00000671     0.00000002     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  17     0.00000674    -0.00181327     0.00000003     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  18     0.00000001    -0.00000004     0.00275431     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  19     0.00000000     0.00000000     0.00000000    -0.00266999     0.00000184     0.00000002     0.00000099    -0.00000486
   MO  20     0.00000000     0.00000000     0.00000000     0.00000178     0.00267231     0.00000003    -0.00000062    -0.00000018
   MO  21     0.00000000     0.00000000     0.00000000    -0.00000001     0.00000007    -0.00406202     0.00000000     0.00000000
   MO  22     0.00000000    -0.00000005     0.00119988     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  23     0.00000000     0.00000003     0.00000013     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  24    -0.00000003     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  25     0.00191450    -0.00000002     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  26    -0.00000002     0.00191257    -0.00000005     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  27     0.00000000    -0.00000005     0.00310969     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  28     0.00000000     0.00000000     0.00000000     0.00083692    -0.00000001     0.00000000    -0.00000033     0.00000162
   MO  29     0.00000000     0.00000000     0.00000000    -0.00000001     0.00083756    -0.00000001    -0.00000058    -0.00000014
   MO  30     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000001     0.00122367     0.00000000     0.00000000
   MO  31     0.00000000     0.00000000     0.00000000    -0.00000033    -0.00000058     0.00000000     0.00241902     0.00000000
   MO  32     0.00000000     0.00000000     0.00000000     0.00000162    -0.00000014     0.00000000     0.00000000     0.00241901
   MO  33     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000001    -0.00000003
   MO  34     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000169     0.00000004     0.00000001
   MO  35     0.00000000     0.00000000     0.00000000     0.00006283    -0.00000009     0.00000000    -0.00000030     0.00000143
   MO  36     0.00000000     0.00000000     0.00000000     0.00000006     0.00006380     0.00000005    -0.00000024    -0.00000005
   MO  37     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000002     0.00093142     0.00000000     0.00000000
   MO  38     0.00000000     0.00000000    -0.00008213     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                MO  33         MO  34         MO  35         MO  36         MO  37         MO  38
   MO   5     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00093181
   MO   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000005
   MO   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000292
   MO   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   9     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000005
   MO  10     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00133432
   MO  11     0.00000000     0.00000002    -0.00000090     0.00044557     0.00000014     0.00000000
   MO  12    -0.00000001     0.00000000     0.00044979     0.00000101     0.00000006     0.00000000
   MO  13     0.00000001     0.00000882    -0.00000001     0.00000021     0.00432323     0.00000000
   MO  14     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  15     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000014
   MO  16     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  17     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  18     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00008105
   MO  19     0.00000000     0.00000000     0.00010223     0.00000009     0.00000002     0.00000000
   MO  20     0.00000000     0.00000000     0.00000001    -0.00009913    -0.00000001     0.00000000
   MO  21     0.00000000    -0.00000600     0.00000000    -0.00000016    -0.00308408     0.00000000
   MO  22     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00046637
   MO  23     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000048
   MO  24     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  25     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  26     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  27     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00008213
   MO  28     0.00000000     0.00000000     0.00006283     0.00000006     0.00000000     0.00000000
   MO  29     0.00000000     0.00000000    -0.00000009     0.00006380    -0.00000002     0.00000000
   MO  30     0.00000000     0.00000169     0.00000000     0.00000005     0.00093142     0.00000000
   MO  31     0.00000001     0.00000004    -0.00000030    -0.00000024     0.00000000     0.00000000
   MO  32    -0.00000003     0.00000001     0.00000143    -0.00000005     0.00000000     0.00000000
   MO  33     0.00097620     0.00000000    -0.00000002     0.00000000     0.00000002     0.00000000
   MO  34     0.00000000     0.00097472     0.00000000     0.00000002     0.00000246     0.00000000
   MO  35    -0.00000002     0.00000000     0.00147777     0.00000002     0.00000000     0.00000000
   MO  36     0.00000000     0.00000002     0.00000002     0.00147603     0.00000002     0.00000000
   MO  37     0.00000002     0.00000246     0.00000000     0.00000002     0.00187111     0.00000000
   MO  38     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00002726

Natural orbital populations,block 1
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     1.78356571     0.57507439     0.57366591     0.51235567
              MO     9       MO    10       MO    11       MO    12       MO    13       MO    14       MO    15       MO    16
  occ(*)=     0.19050649     0.18986921     0.06289780     0.03896051     0.03892156     0.00646828     0.00350985     0.00350065
              MO    17       MO    18       MO    19       MO    20       MO    21       MO    22       MO    23       MO    24
  occ(*)=     0.00291894     0.00241902     0.00241902     0.00203453     0.00203125     0.00160634     0.00151882     0.00151749
              MO    25       MO    26       MO    27       MO    28       MO    29       MO    30       MO    31       MO    32
  occ(*)=     0.00097620     0.00097471     0.00064713     0.00062549     0.00062546     0.00009646     0.00008341     0.00008319
              MO    33       MO    34       MO    35       MO    36       MO    37       MO    38       MO
  occ(*)=     0.00003559     0.00003551     0.00001818     0.00001817     0.00001490     0.00000416


 total number of electrons =   12.0000000000

 test slabel:                     1
  a  


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        a   partial gross atomic populations
   ao class       1a         2a         3a         4a         5a         6a  
     3_ s       0.000000   2.000000   0.000000   0.000000   1.719866   0.000000
     3_ p       2.000000   0.000000   2.000000   2.000000   0.000000   0.000000
     3_ d       0.000000   0.000000   0.000000   0.000000   0.063700   0.575074
     3_ f       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
 
   ao class       7a         8a         9a        10a        11a        12a  
     3_ s       0.000000   0.020726   0.000000   0.000000   0.000000   0.000000
     3_ p       0.000000   0.000000   0.000000   0.000000   0.062413   0.038956
     3_ d       0.573666   0.491629   0.190506   0.189869   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000000   0.000484   0.000005
 
   ao class      13a        14a        15a        16a        17a        18a  
     3_ s       0.000000   0.005827   0.000000   0.000000   0.000270   0.000000
     3_ p       0.038917   0.000000   0.000000   0.000000   0.000000   0.000000
     3_ d       0.000000   0.000641   0.003510   0.003501   0.002649   0.000000
     3_ f       0.000005   0.000000   0.000000   0.000000   0.000000   0.002419
 
   ao class      19a        20a        21a        22a        23a        24a  
     3_ s       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     3_ p       0.000000   0.000000   0.000000   0.000356   0.000077   0.000078
     3_ d       0.000000   0.002035   0.002031   0.000000   0.000000   0.000000
     3_ f       0.002419   0.000000   0.000000   0.001251   0.001442   0.001440
 
   ao class      25a        26a        27a        28a        29a        30a  
     3_ s       0.000000   0.000000   0.000000   0.000000   0.000000   0.000001
     3_ p       0.000000   0.000000   0.000511   0.000594   0.000594   0.000000
     3_ d       0.000000   0.000000   0.000000   0.000000   0.000000   0.000096
     3_ f       0.000976   0.000975   0.000136   0.000031   0.000031   0.000000
 
   ao class      31a        32a        33a        34a        35a        36a  
     3_ s       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     3_ p       0.000000   0.000000   0.000000   0.000000   0.000018   0.000018
     3_ d       0.000083   0.000083   0.000036   0.000036   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
 
   ao class      37a        38a  
     3_ s       0.000000   0.000004
     3_ p       0.000015   0.000000
     3_ d       0.000000   0.000000
     3_ f       0.000000   0.000000


                        gross atomic populations
     ao            3_
      s         3.746694
      p         6.142547
      d         2.099145
      f         0.011614
    total      12.000000
 

 Total number of electrons:   12.00000000

 DA ...
