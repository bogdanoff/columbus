1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.

  This is a  mraqcc energy evaluation      

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  1  1    -46.2712888828 -7.7716E-15  6.5636E-02  2.3403E-01  1.0000E-04
 mraqcc  #  2  1    -46.3272082534  5.5919E-02  3.1920E-03  4.9570E-02  1.0000E-04
 mraqcc  #  3  1    -46.3307282905  3.5200E-03  5.4927E-04  2.0951E-02  1.0000E-04
 mraqcc  #  4  1    -46.3312300441  5.0175E-04  1.2000E-04  9.3542E-03  1.0000E-04
 mraqcc  #  5  1    -46.3313274135  9.7369E-05  1.6848E-05  3.3947E-03  1.0000E-04
 mraqcc  #  6  1    -46.3313448758  1.7462E-05  3.9606E-06  1.8500E-03  1.0000E-04
 mraqcc  #  7  1    -46.3313491198  4.2440E-06  1.3148E-06  8.8658E-04  1.0000E-04
 mraqcc  #  8  1    -46.3313507594  1.6396E-06  4.0733E-07  5.5804E-04  1.0000E-04
 mraqcc  #  9  1    -46.3313512007  4.4133E-07  7.8949E-08  2.5360E-04  1.0000E-04
 mraqcc  # 10  1    -46.3313512675  6.6771E-08  1.4318E-08  1.0086E-04  1.0000E-04
 mraqcc  # 11  1    -46.3313512803  1.2842E-08  3.8376E-09  5.1605E-05  1.0000E-04

 mraqcc   convergence criteria satisfied after 11 iterations.

 final mraqcc   convergence information:
 mraqcc  # 11  1    -46.3313512803  1.2842E-08  3.8376E-09  5.1605E-05  1.0000E-04

 number of reference csfs (nref) is   105.  root number (iroot) is  1.
 c0**2 =   0.89549213  c0**2(renormalized) =   0.98907811
 c**2 (all zwalks) =   0.89549213  c**2(all zwalks, renormalized) =   0.98907811
