1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      2009-03.    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


================ Computing sorting integral file structure ================

                    -----z----- -----y----- -----x----- -----w----- ---total---

                CSFs        24         490        1414        2114        4042
      internal walks       105          70          15          21         211
valid internal walks        24          70          15          21         130
 getinfoarray: info=                     6 :                     1
                  8192                  6552                  8192
                  5460                     0
 icd(3)=                  2976 ci%nnlev=                   595  l2rec=
                  8192  n2max=                  5460
 lcore1,lcore2=              13068137              13044821
 lencor,maxblo              13107200                 60000
========================================
 current settings:
 minbl3         381
 minbl4         381
 locmaxbl3     4288
 locmaxbuf     2144
 maxbl3       60000
 maxbl3       60000
 maxbl4       60000
 maxbuf       30006
========================================

 sorted 4-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)

 sorted 3-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)
 Orig.  diagonal integrals:  1electron:        34
                             0ext.    :        42
                             2ext.    :       336
                             4ext.    :       812


 Orig. off-diag. integrals:  4ext.    :     24411
                             3ext.    :     18720
                             2ext.    :      6792
                             1ext.    :      1272
                             0ext.    :        99
                             2ext. SO :         0
                             1ext. SO :         0
                             0ext. SO :         0
                             1electron:       128


 Sorted integrals            3ext.  w :     17370 x :     16020
                             4ext.  w :     20955 x :     17777


Cycle #  1 sortfile size=    163835(       5 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13068137
Cycle #  2 sortfile size=    163835(       5 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13068137
 minimum size of srtscr:     98301 WP (     3 records)
 maximum size of srtscr:    163835 WP (     5 records)
diagi   file:      4 records  of   1536 WP each=>       6144 WP total
ofdgi   file:      8 records  of   1536 WP each=>      12288 WP total
fil3w   file:      1 records  of  30006 WP each=>      30006 WP total
fil3x   file:      1 records  of  30006 WP each=>      30006 WP total
fil4w   file:      1 records  of  30006 WP each=>      30006 WP total
fil4x   file:      1 records  of  30006 WP each=>      30006 WP total
 compressed index vector length=                    61
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 3,
  GSET = 3,
   DAVCOR =10,
  NCOREL = 4
   nroot=1
  IVMODE = 3
  NBKITR = 1
   nvbkmn=2
  RTOLBK = 1e-3
   niter=30
   nvcimn=2
   rtolci=1e-3
  NVCIMX = 6
  NVRFMX = 6
  NVBKMX = 6
  IDEN  = 1
  CSFPRN = 10,
  update_mode=1
  nvrfmn=2
   lrtshift=-0.0465482970
 /&end
 ------------------------------------------------------------------------
lodens (list->root)=  1
invlodens (root->list)=  1
 LRT calculation: diagonal shift= -4.654829700000000E-002
 bummer (warning):2:changed keyword: nbkitr=                      0
 USING SEGMENTS OF EQUAL SIZE

****************  list of control variables  ****************
 lvlprt =    0      nroot  =    1      noldv  =   0      noldhv =   0
 nunitv =    1      nbkitr =    0      niter  =  30      davcor =  10
 csfprn =   10      ivmode =    3      istrt  =   0      vout   =   0
 iortls =    0      nvbkmx =    6      ibktv  =  -1      ibkthv =  -1
 nvcimx =    6      icitv  =   -1      icithv =  -1      frcsub =   0
 nvbkmn =    2      nvcimn =    2      maxseg =   4      nrfitr =  30
 ncorel =    4      nvrfmx =    6      nvrfmn =   2      iden   =   1
 itran  =    0      froot  =    0      rtmode =   0      ncouple=   1
 skipso =    F      dalton2=    0      molcas =   0      finalv =   0
 finalw =    0      cosmocalc=   0    with_tsklst=   0
 nsegwx =    1     1     1     1
 nseg0x =    1     1     1     1
 nseg1x =    1     1     1     1
 nseg2x =    1     1     1     1
 nseg3x =    1     1     1     1
 nseg4x =    1     1     1     1
 no0ex  =      0    no1ex  =      0    no2ex  =     0    no3ex  =     0
 no4ex  =      0    nodiag =      0
 cdg4ex =    1      c3ex1ex=    1      c2ex0ex=   1
 fileloc=    0     0     0     0     0     0     0     1     1     1
 directhd=   1      noaqccshift_zyxw=      0
 critical_crit=-1.00000    critical_delta= 0.05000

 ctol   = 0.010000    lrtshift=-.046548    smalld =0.001000


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-03    1.000E-03
 Computing density:                    .drt1.state1
 using                      1  nodes and                      1  cores.
 szdg/szodg per processor=                  6144                 12288
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core           13107199 DP per process

********** Integral sort section *************


 workspace allocation information: lencor=  13107199

 echo of the input for program cisrt:
 ------------------------------------------------------------------------
  &input
  maxbl3=60000
  maxbl4=60000
  &end
 ------------------------------------------------------------------------
 
 ( 6) listing file:                    ciudgls             
 ( 5) input file:                      cisrtin   
 (17) cidrt file:                      cidrtfl             
 (11) transformed integrals file:      moints    
 (12) diagonal integral file:          diagint             
 (13) off-diagonal integral file:      ofdgint             
 (31) 4-external w integrals file:     fil4w               
 (32) 4-external x integrals file:     fil4x               
 (33) 3-external w integrals file:     fil3w               
 (34) 3-external x integrals file:     fil3x               
 (21) scratch da sorting file:         srtscr              
 (12) 2-e integral file [fsplit=2]:    moints2   

 input integral file header information:
                                                                                 
 aoints SIFS file created by argos.      zam792            17:56:38.564 17-Dec-13
  cidrt_title                                                                    
 MO-coefficients from mcscf.x                                                    
  with dummy occupation 1.0 for active orbitals                                  
  total ao core energy =    0.000000000                                          
 MCSCF energy =     -46.301878576                                                
 SIFS file created by program tran.      zam792            17:56:38.888 17-Dec-13

 input energy(*) values:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.

 total core energy =   0.000000000000E+00

 nsym = 4 nmot=  34

 symmetry  =    1    2    3    4
 slabel(*) =   a1   a2   b1   b2
 nmpsy(*)  =   14    4    8    8

 info(*) =          1      8192      6552      8192      5460         0

 orbital labels, i:molab(i)=
   1:tout:001   2:tout:002   3:tout:003   4:tout:004   5:tout:005   6:tout:006   7:tout:007   8:tout:008   9:tout:009  10:tout:010
  11:tout:011  12:tout:012  13:tout:013  14:tout:014  15:tout:015  16:tout:016  17:tout:017  18:tout:018  19:tout:019  20:tout:020
  21:tout:021  22:tout:022  23:tout:023  24:tout:024  25:tout:025  26:tout:026  27:tout:027  28:tout:028  29:tout:029  30:tout:030
  31:tout:031  32:tout:032  33:tout:033  34:tout:034

 input parameters:
 prnopt=  0
 ldamin=    4095 ldamax=   32767 ldainc=      64
 maxbuf=   30006 maxbl3=   60000 maxbl4=   60000 intmxo=    1536
  Using 32 bit compression 

 drt information:
  cidrt_title                                                                    
 nmotd =  38 nfctd =   4 nfvtc =   0 nmot  =  34
 nlevel =  34 niot  =   6 lowinl=  29
 orbital-to-level map(*)
   -1  -1  29  30  31   1   2   3   4   5   6   7   8   9  10  11  32  12  13  14
   -1  33  15  16  17  18  19  20  21  -1  34  22  23  24  25  26  27  28
 compressed map(*)
   29  30  31   1   2   3   4   5   6   7   8   9  10  11  32  12  13  14  33  15
   16  17  18  19  20  21  34  22  23  24  25  26  27  28
 levsym(*)
    1   1   1   1   1   1   1   1   1   1   1   2   2   2   3   3   3   3   3   3
    3   4   4   4   4   4   4   4   1   1   1   2   3   4
 repartitioning mu(*)=
   0.  0.  0.  0.  0.  0.

 new core energy added to the energy(*) list.
 from the integral file: h1_core= -4.349858765627E+01

 indxdg: diagonal integral statistics.
 total number of integrals contributing to diagonal matrix elements:      1190
 number with all external indices:       812
 number with half external - half internal indices:       336
 number with all internal indices:        42

 indxof: off-diagonal integral statistics.
    4-external integrals: num=      24411 strt=          1
    3-external integrals: num=      18720 strt=      24412
    2-external integrals: num=       6792 strt=      43132
    1-external integrals: num=       1272 strt=      49924
    0-external integrals: num=         99 strt=      51196

 total number of off-diagonal integrals:       51294


 indxof(2nd)  ittp=   3 numx(ittp)=        6792
 indxof(2nd)  ittp=   4 numx(ittp)=        1272
 indxof(2nd)  ittp=   5 numx(ittp)=          99

 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg=  12931980
 pro2e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652

 pro2e:     25126 integrals read in     5 records.

 pro2e:         0 integrals 34-ext integrals skipped.
 pro1e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652
 pro1e: eref =    0.000000000000000E+00
 total size of srtscr:                     4  records of                  32767 
 WP =               1048544 Bytes
 putdg        1     596    1191    1786    3322   36089   57934    3018   46706   90394
   123161  131353  136813  158652

 putf:       4 buffers of length    1536 written to file 12
 diagonal integral file completed.

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep4e:    17 blocks of linear combinations of 4-external integrals processed.
 number of sorted 4-external integrals      38732
 number of original 4-external integrals    24411


 putf34: external integral file complete. nfilw=    31 nfilx=    32 nrecw=     1 nrecx=     1 lbufp= 30006

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep3e:    19 blocks of linear combinations of 3-external integrals processed.
 number of sorted 3-external integrals      33390
 number of original 3-external integrals    18720


 putf34: external integral file complete. nfilw=    33 nfilx=    34 nrecw=     1 nrecx=     1 lbufp= 30006
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      6792         3     43132     43132      6792     51294
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      1272         4     49924     49924      1272     51294
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd        99         5     51196     51196        99     51294

 putf:       8 buffers of length    1536 written to file 13
 off-diagonal files sort completed.
 executing brd_struct for cisrtinfo
cisrtinfo:
bufszi  1536
 diagfile 4ext:     812 2ext:     336 0ext:      42
 fil4w,fil4x  :   24411 fil3w,fil3x :   18720
 ofdgint  2ext:    6792 1ext:    1272 0ext:      99so0ext:       0so1ext:       0so2ext:       0
buffer minbl4     381 minbl3     381 maxbl2     384nbas:  11   3   7   7   0   0   0   0 maxbuf 30006
 CIUDG version 5.9.7 ( 5-Oct-2004)

 workspace allocation information: lcore=  13107199

 core energy values from the integral file:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -4.349858765627E+01, ietype=    6,   fcore energy of type: H1(*)   

 total core repulsion energy = -4.349858765627E+01
 nmot  =    38 niot  =     6 nfct  =     4 nfvt  =     0
 nrow  =    32 nsym  =     4 ssym  =     4 lenbuf=  1600
 nwalk,xbar:        211      105       70       15       21
 nvalwt,nvalw:      130       24       70       15       21
 ncsft:            4042
 total number of valid internal walks:     130
 nvalz,nvaly,nvalx,nvalw =       24      70      15      21

 cisrt info file parameters:
 file number  12 blocksize   1536
 mxbld   1536
 nd4ext,nd2ext,nd0ext   812   336    42
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int    24411    18720     6792     1272       99        0        0        0
 minbl4,minbl3,maxbl2   381   381   384
 maxbuf 30006
 number of external orbitals per symmetry block:  11   3   7   7
 nmsym   4 number of internal orbitals   6
 executing brd_struct for drt
 executing brd_struct for orbinf
 executing brd_struct for momap
 calcthrxt: niot,maxw1=                     6                    31
 block size     0
 pthz,pthy,pthx,pthw:   105    70    15    21 total internal walks:     211
 maxlp3,n2lp,n1lp,n0lp    31     0     0     0
 orbsym(*)= 1 1 1 2 3 4

 setref:       24 references kept,
               81 references were marked as invalid, out of
              105 total.
 nmb.of records onel     1
 nmb.of records 2-ext     5
 nmb.of records 1-ext     1
 nmb.of records 0-ext     1
 nmb.of records 2-int     0
 nmb.of records 1-int     0
 nmb.of records 0-int     0
 ---------memory usage in DP -----------------
 < n-ex core usage >
     routines:
    fourex            61797
    threx             60199
    twoex              3549
    onex               1823
    allin              1536
    diagon             2109
               =======
   maximum            61797
 
  __ static summary __ 
   reflst                24
   hrfspc                24
               -------
   static->              48
 
  __ core required  __ 
   totstc                48
   max n-ex           61797
               -------
   totnec->           61845
 
  __ core available __ 
   totspc          13107199
   totnec -           61845
               -------
   totvec->        13045354

 number of external paths / symmetry
 vertex x     100      82      98      98
 vertex w     128      82      98      98
segment: free space=    13045354
 reducing frespc by                   774 to               13044580 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1          47|        24|         0|        24|         0|         1|
 -------------------------------------------------------------------------------
  Y 2          70|       490|        24|        70|        24|         2|
 -------------------------------------------------------------------------------
  X 3          15|      1414|       514|        15|        94|         3|
 -------------------------------------------------------------------------------
  W 4          21|      2114|      1928|        21|       109|         4|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=          60DP  conft+indsym=         280DP  drtbuffer=         434 DP

dimension of the ci-matrix ->>>      4042

 executing brd_struct for civct
 gentasklist: ntask=                    20
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS   
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      15      47       1414         24      15      24
     2  4   1    25      two-ext wz   2X  4 1      21      47       2114         24      21      24
     3  4   3    26      two-ext wx*  WX  4 3      21      15       2114       1414      21      15
     4  4   3    27      two-ext wx+  WX  4 3      21      15       2114       1414      21      15
     5  2   1    11      one-ext yz   1X  2 1      70      47        490         24      70      24
     6  3   2    15      1ex3ex yx    3X  3 2      15      70       1414        490      15      70
     7  4   2    16      1ex3ex yw    3X  4 2      21      70       2114        490      21      70
     8  1   1     1      allint zz    OX  1 1      47      47         24         24      24      24
     9  2   2     5      0ex2ex yy    OX  2 2      70      70        490        490      70      70
    10  3   3     6      0ex2ex xx*   OX  3 3      15      15       1414       1414      15      15
    11  3   3    18      0ex2ex xx+   OX  3 3      15      15       1414       1414      15      15
    12  4   4     7      0ex2ex ww*   OX  4 4      21      21       2114       2114      21      21
    13  4   4    19      0ex2ex ww+   OX  4 4      21      21       2114       2114      21      21
    14  2   2    42      four-ext y   4X  2 2      70      70        490        490      70      70
    15  3   3    43      four-ext x   4X  3 3      15      15       1414       1414      15      15
    16  4   4    44      four-ext w   4X  4 4      21      21       2114       2114      21      21
    17  1   1    75      dg-024ext z  OX  1 1      47      47         24         24      24      24
    18  2   2    76      dg-024ext y  OX  2 2      70      70        490        490      70      70
    19  3   3    77      dg-024ext x  OX  3 3      15      15       1414       1414      15      15
    20  4   4    78      dg-024ext w  OX  4 4      21      21       2114       2114      21      21
----------------------------------------------------------------------------------------------------
REDTASK #   1 TIME=  19.000 N=  1 (task/type/sgbra)=(   1/24/0) (
REDTASK #   2 TIME=  18.000 N=  1 (task/type/sgbra)=(   2/25/0) (
REDTASK #   3 TIME=  17.000 N=  1 (task/type/sgbra)=(   3/26/1) (
REDTASK #   4 TIME=  16.000 N=  1 (task/type/sgbra)=(   4/27/2) (
REDTASK #   5 TIME=  15.000 N=  1 (task/type/sgbra)=(   5/11/0) (
REDTASK #   6 TIME=  14.000 N=  1 (task/type/sgbra)=(   6/15/0) (
REDTASK #   7 TIME=  13.000 N=  1 (task/type/sgbra)=(   7/16/0) (
REDTASK #   8 TIME=  12.000 N=  1 (task/type/sgbra)=(   8/ 1/0) (
REDTASK #   9 TIME=  11.000 N=  1 (task/type/sgbra)=(   9/ 5/0) (
REDTASK #  10 TIME=  10.000 N=  1 (task/type/sgbra)=(  10/ 6/1) (
REDTASK #  11 TIME=   9.000 N=  1 (task/type/sgbra)=(  11/18/2) (
REDTASK #  12 TIME=   8.000 N=  1 (task/type/sgbra)=(  12/ 7/1) (
REDTASK #  13 TIME=   7.000 N=  1 (task/type/sgbra)=(  13/19/2) (
REDTASK #  14 TIME=   6.000 N=  1 (task/type/sgbra)=(  14/42/1) (
REDTASK #  15 TIME=   5.000 N=  1 (task/type/sgbra)=(  15/43/1) (
REDTASK #  16 TIME=   4.000 N=  1 (task/type/sgbra)=(  16/44/1) (
REDTASK #  17 TIME=   3.000 N=  1 (task/type/sgbra)=(  17/75/1) (
REDTASK #  18 TIME=   2.000 N=  1 (task/type/sgbra)=(  18/76/1) (
REDTASK #  19 TIME=   1.000 N=  1 (task/type/sgbra)=(  19/77/1) (
REDTASK #  20 TIME=   0.000 N=  1 (task/type/sgbra)=(  20/78/1) (
 initializing v-file: 1:                  4042

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         176 2x:           0 4x:           0
All internal counts: zz :         271 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 reference space has dimension      24
 dsyevx: computed roots 1 to    2(converged:   2)

    root           eigenvalues
    ----           ------------
       1         -46.2740792724
       2         -46.2533198521

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt=                    24

         vector  1 from unit 11 written to unit 49 filename cirefv              
 ncorel,neli=                     4                     4
  This is a  mraqcc energy evaluation      

    ------------------------------------------------------------

 setopt2: mraqcc energy evaluation      gvalue=  0.833333333333333      ncorel=
                     4

    ------------------------------------------------------------


    reference energies:

  reference state used is:   1with energy      -46.2740792724

  ### begin active excitation selection ###

 inactive(1:nintern)=AAAAAA
 There are         24 reference CSFs out of         24 valid zwalks.
 There are    0 inactive and    6 active orbitals.
      24 active-active  and        0 inactive excitations.

  ### end active excitation selection ###


 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              4042
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                   30
 residual norm convergence criteria:               0.001000

          starting ci iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         602 2x:         246 4x:         106
All internal counts: zz :         271 yy:         620 xx:          66 ww:         132
One-external counts: yz :        1072 yx:        1000 yw:        1210
Two-external counts: yy :         865 ww:         252 xx:         180 xz:          72 wz:         104 wx:         360
Three-ext.   counts: yx :         328 yw:         393

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1
   ht   1    -2.77549162
calca4: root=   1 anorm**2=  0.00000000 scale:  1.00000000

          calcsovref: tciref block   1

              civs   1
 refs   1    1.00000    

          calcsovref: scrb block   1

                ci   1
 civs   1    1.00000    

          calcsovref: sovref block   1

              v      1
 ref    1    1.00000    

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  1  1    -46.2740792724  2.2204E-15  5.7941E-02  2.2790E-01  1.0000E-03
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   2

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         602 2x:         246 4x:         106
All internal counts: zz :         271 yy:         620 xx:          66 ww:         132
One-external counts: yz :        1072 yx:        1000 yw:        1210
Two-external counts: yy :         865 ww:         252 xx:         180 xz:          72 wz:         104 wx:         360
Three-ext.   counts: yx :         328 yw:         393

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2
   ht   1    -2.77549162
   ht   2    -0.05794112    -0.19443240
calca4: root=   1 anorm**2=  0.06320422 scale:  1.03111795
calca4: root=   2 anorm**2=  0.93679578 scale:  1.39168810

          calcsovref: tciref block   1

              civs   1       civs   2
 refs   1    1.00000       1.494216E-14

          calcsovref: scrb block   1

                ci   1         ci   2
 civs   1   0.967882      -0.251405    
 civs   2   0.824846        3.17557    

          calcsovref: sovref block   1

              v      1       v      2
 ref    1   0.967882      -0.251405    

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.96788211    -0.25140450
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  2  1    -46.3234577145  4.9378E-02  2.5849E-03  4.6126E-02  1.0000E-03
 mraqcc  #  2  2    -45.5422054730  2.0436E+00  0.0000E+00  4.9927E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   3

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         602 2x:         246 4x:         106
All internal counts: zz :         271 yy:         620 xx:          66 ww:         132
One-external counts: yz :        1072 yx:        1000 yw:        1210
Two-external counts: yy :         865 ww:         252 xx:         180 xz:          72 wz:         104 wx:         360
Three-ext.   counts: yx :         328 yw:         393

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3
   ht   1    -2.77549162
   ht   2    -0.05794112    -0.19443240
   ht   3     0.00692062    -0.00264623    -0.00931877
calca4: root=   1 anorm**2=  0.07357594 scale:  1.03613510
calca4: root=   2 anorm**2=  0.93401297 scale:  1.39068795
calca4: root=   3 anorm**2=  0.95198130 scale:  1.39713324

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3
 refs   1    1.00000       1.494216E-14  -2.495593E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3
 civs   1   0.964999      -0.157293       0.213340    
 civs   2   0.864650        1.58090       -2.74193    
 civs   3    1.03560        13.2196        7.83771    

          calcsovref: sovref block   1

              v      1       v      2       v      3
 ref    1   0.962414      -0.190283       0.193781    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.96241431    -0.19028340     0.19378058
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  3  1    -46.3261409093  2.6832E-03  4.1110E-04  1.7868E-02  1.0000E-03
 mraqcc  #  3  2    -45.7922891925  2.5008E-01  0.0000E+00  4.1424E-01  1.0000E-04
 mraqcc  #  3  3    -45.4544696118  1.9559E+00  0.0000E+00  4.9720E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   4

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         602 2x:         246 4x:         106
All internal counts: zz :         271 yy:         620 xx:          66 ww:         132
One-external counts: yz :        1072 yx:        1000 yw:        1210
Two-external counts: yy :         865 ww:         252 xx:         180 xz:          72 wz:         104 wx:         360
Three-ext.   counts: yx :         328 yw:         393

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4
   ht   1    -2.77549162
   ht   2    -0.05794112    -0.19443240
   ht   3     0.00692062    -0.00264623    -0.00931877
   ht   4    -0.00062829    -0.00360715    -0.00052681    -0.00154803
calca4: root=   1 anorm**2=  0.07822067 scale:  1.03837405
calca4: root=   2 anorm**2=  0.94886397 scale:  1.39601718
calca4: root=   3 anorm**2=  0.95513996 scale:  1.39826319
calca4: root=   4 anorm**2=  0.76735811 scale:  1.32942022

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4
 refs   1    1.00000       1.494216E-14  -2.495593E-03   2.497232E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4
 civs   1  -0.962767       0.157564       0.220356       3.578748E-02
 civs   2  -0.869394       -1.02850       -2.88285      -0.943677    
 civs   3   -1.17196       -11.0233        6.80979       -8.25720    
 civs   4  -0.866591       -19.7922        3.51748        31.3697    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4
 ref    1  -0.960059       0.180131       0.204240       6.422782E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1    -0.96005908     0.18013130     0.20424022     0.06422782
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  4  1    -46.3264972830  3.5637E-04  7.8142E-05  8.2538E-03  1.0000E-03
 mraqcc  #  4  2    -45.9500135489  1.5772E-01  0.0000E+00  2.4610E-01  1.0000E-04
 mraqcc  #  4  3    -45.4553194873  8.4988E-04  0.0000E+00  5.0092E-01  1.0000E-04
 mraqcc  #  4  4    -45.3903596760  1.8918E+00  0.0000E+00  5.5047E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000999
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   5

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         602 2x:         246 4x:         106
All internal counts: zz :         271 yy:         620 xx:          66 ww:         132
One-external counts: yz :        1072 yx:        1000 yw:        1210
Two-external counts: yy :         865 ww:         252 xx:         180 xz:          72 wz:         104 wx:         360
Three-ext.   counts: yx :         328 yw:         393

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1    -2.77549162
   ht   2    -0.05794112    -0.19443240
   ht   3     0.00692062    -0.00264623    -0.00931877
   ht   4    -0.00062829    -0.00360715    -0.00052681    -0.00154803
   ht   5    -0.00448450     0.00080305     0.00043903    -0.00001022    -0.00029581
calca4: root=   1 anorm**2=  0.07981391 scale:  1.03914095
calca4: root=   2 anorm**2=  0.93900073 scale:  1.39248006
calca4: root=   3 anorm**2=  0.88073570 scale:  1.37139918
calca4: root=   4 anorm**2=  0.96677678 scale:  1.40241819
calca4: root=   5 anorm**2=  0.51148171 scale:  1.22942333

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1    1.00000       1.494216E-14  -2.495593E-03   2.497232E-04   1.609881E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 civs   1  -0.963223       0.104909       0.280741      -1.785567E-02  -2.515752E-02
 civs   2  -0.870053      -0.768995       -2.95657      -0.569173      -0.854997    
 civs   3   -1.19612       -9.13703       0.521724        12.4700       -2.61074    
 civs   4   -1.02103       -20.8010        6.90564       -12.3092        27.9350    
 civs   5   0.813751        30.7242       -41.6164        55.2103        42.2105    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1  -0.959182       0.171979       0.214166       3.683232E-02   5.628772E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1    -0.95918247     0.17197909     0.21416641     0.03683232     0.05628772
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  5  1    -46.3265608748  6.3592E-05  7.6560E-06  2.5149E-03  1.0000E-03
 mraqcc  #  5  2    -46.0254356622  7.5422E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  5  3    -45.4689599799  1.3640E-02  0.0000E+00  5.3133E-01  1.0000E-04
 mraqcc  #  5  4    -45.4267744675  3.6415E-02  0.0000E+00  5.4177E-01  1.0000E-04
 mraqcc  #  5  5    -45.3737435700  1.8752E+00  0.0000E+00  6.1888E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.001000
time for vector access                 0.000000

          starting ci iteration   6

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         602 2x:         246 4x:         106
All internal counts: zz :         271 yy:         620 xx:          66 ww:         132
One-external counts: yz :        1072 yx:        1000 yw:        1210
Two-external counts: yy :         865 ww:         252 xx:         180 xz:          72 wz:         104 wx:         360
Three-ext.   counts: yx :         328 yw:         393

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1    -2.77549162
   ht   2    -0.05794112    -0.19443240
   ht   3     0.00692062    -0.00264623    -0.00931877
   ht   4    -0.00062829    -0.00360715    -0.00052681    -0.00154803
   ht   5    -0.00448450     0.00080305     0.00043903    -0.00001022    -0.00029581
   ht   6     0.00114092    -0.00014602     0.00003886     0.00001995     0.00001013    -0.00003179
calca4: root=   1 anorm**2=  0.07998359 scale:  1.03922259
calca4: root=   2 anorm**2=  0.96138418 scale:  1.40049426
calca4: root=   3 anorm**2=  0.85007162 scale:  1.36017338
calca4: root=   4 anorm**2=  0.91494356 scale:  1.38381486
calca4: root=   5 anorm**2=  0.80667049 scale:  1.34412443
calca4: root=   6 anorm**2=  0.56780819 scale:  1.25212148

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1    1.00000       1.494216E-14  -2.495593E-03   2.497232E-04   1.609881E-03  -4.099573E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1  -0.962911       0.113888      -3.958696E-02   0.223898      -0.188026       2.686952E-03
 civs   2  -0.870095      -0.658221        1.09949       -2.55062        1.06812       -1.05972    
 civs   3   -1.19881       -8.43113        4.20297        8.64680        8.75975       -2.76762    
 civs   4   -1.03838       -20.2145        2.44806       -2.49548       -12.4155        29.0124    
 civs   5   0.905222        34.8011        21.1607        1.02879        67.6633        37.7031    
 civs   6   0.933865        64.2814        241.516        65.7628       -71.0734        19.3406    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.959104       0.159554      -0.114410       0.176392      -7.492037E-02   6.960751E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.95910401     0.15955418    -0.11440970     0.17639193    -0.07492037     0.06960751

 trial vector basis is being transformed.  new dimension:   2
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  6  1    -46.3265680245  7.1497E-06  8.2274E-07  8.4352E-04  1.0000E-03
 mraqcc  #  6  2    -46.0452606131  1.9825E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  6  3    -45.7219294615  2.5297E-01  0.0000E+00  4.5748E-01  1.0000E-04
 mraqcc  #  6  4    -45.4364028130  9.6283E-03  0.0000E+00  5.0891E-01  1.0000E-04
 mraqcc  #  6  5    -45.4189617047  4.5218E-02  0.0000E+00  6.0264E-01  1.0000E-04
 mraqcc  #  6  6    -45.3726881349  1.8741E+00  0.0000E+00  6.0325E-01  1.0000E-04
 
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000999
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mraqcc   convergence criteria satisfied after  6 iterations.

 final mraqcc   convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  6  1    -46.3265680245  7.1497E-06  8.2274E-07  8.4352E-04  1.0000E-03
 mraqcc  #  6  2    -46.0452606131  1.9825E-02  0.0000E+00  0.0000E+00  1.0000E-04

####################CIUDGINFO####################

   aqcc(lrt) vector at position   1 energy=  -46.326568024534

################END OF CIUDGINFO################

 
 a4den factor for root 1  =  1.013510703
diagon:itrnv=   0
    1 of the   3 expansion vectors are transformed.
    1 of the   2 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
maximum overlap with reference    1(overlap= 0.95910)
reference energy =                         -46.2740792724
total aqcc energy =                        -46.3265680245
diagonal element shift (lrtshift) =         -0.0524887522

 information on vector: 1 from unit 11 written to unit 48 filename civout              


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -46.3265680245

                                                       internal orbitals

                                          level       1    2    3    4    5    6

                                          orbital     3    4    5   17   22   31

                                         symmetry   a1   a1   a1   a2   b1   b2 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1 -0.676520                        +-   +                    - 
 z*  1  1       2 -0.149810                        +-        +               - 
 z*  1  1       3 -0.600491                        +-             +     -      
 z*  1  1       4 -0.127503                        +    +-                   - 
 z*  1  1       5 -0.031478                        +     -   +               - 
 z*  1  1       6 -0.126170                        +     -        +     -      
 z*  1  1       7 -0.054623                        +    +     -              - 
 z*  1  1       8 -0.027978                        +    +          -    -      
 z*  1  1       9  0.063664                        +         +-              - 
 z*  1  1      10 -0.027976                        +          -   +     -      
 z*  1  1      11  0.126323                        +         +     -    -      
 z*  1  1      12 -0.084658                        +              +-         - 
 z*  1  1      13 -0.084644                        +                   +-    - 
 z*  1  1      16 -0.044429                             +    +-              - 
 z*  1  1      17 -0.015434                             +     -   +     -      
 z*  1  1      18  0.018293                             +    +     -    -      
 z*  1  1      19 -0.025313                             +         +-         - 
 z*  1  1      20 -0.019735                             +              +-    - 
 z*  1  1      21  0.049932                                  +-   +     -      
 z*  1  1      23 -0.017609                                  +         +-    - 
 z*  1  1      24  0.026477                                       +     -   +- 
 y   1  1      26  0.019549              2( b2 )   +-    -                     
 y   1  1      28 -0.015147              4( b2 )   +-    -                     
 y   1  1      40  0.017359              2( b1 )   +-              -           
 y   1  1      42 -0.013445              4( b1 )   +-              -           
 y   1  1      46  0.017363              1( a2 )   +-                   -      
 y   1  1      47 -0.013447              2( a2 )   +-                   -      
 y   1  1      51  0.018149              3( a1 )   +-                        - 
 y   1  1      53  0.059699              5( a1 )   +-                        - 
 y   1  1      55 -0.013204              7( a1 )   +-                        - 
 y   1  1      61  0.012330              2( b2 )    -   +-                     
 y   1  1      63 -0.015093              4( b2 )    -   +-                     
 y   1  1      88  0.022886              5( a1 )    -   +                    - 
 y   1  1     103 -0.010611              2( b1 )    -        +     -           
 y   1  1     105  0.010475              4( b1 )    -        +     -           
 y   1  1     109  0.013721              1( a2 )    -        +          -      
 y   1  1     110 -0.013540              2( a2 )    -        +          -      
 y   1  1     126 -0.010395              4( b2 )    -             +-           
 y   1  1     132 -0.013023              3( a1 )    -             +     -      
 y   1  1     134  0.020328              5( a1 )    -             +     -      
 y   1  1     136  0.012082              7( a1 )    -             +     -      
 y   1  1     147 -0.010392              4( b2 )    -                  +-      
 y   1  1     180 -0.011907              2( a2 )   +     -              -      
 y   1  1     184  0.010745              3( a1 )   +     -                   - 
 y   1  1     188 -0.015240              7( a1 )   +     -                   - 
 y   1  1     219 -0.010651              6( a1 )   +               -    -      
 y   1  1     226 -0.011022              2( a2 )   +               -         - 
 y   1  1     231 -0.011018              4( b1 )   +                    -    - 
 y   1  1     348  0.010039              6( a1 )        +     -              - 
 y   1  1     366 -0.010004              2( a2 )        +          -         - 
 y   1  1     404  0.012489              2( a1 )              -   +     -      
 y   1  1     408  0.013864              6( a1 )              -   +     -      
 x   1  1     517  0.010133    3( a2 )   1( b1 )    -    -                     
 x   1  1     523  0.010107    3( a2 )   3( b1 )    -    -                     
 x   1  1     545  0.012135   10( a1 )   1( b2 )    -    -                     
 x   1  1     551  0.016614    5( a1 )   2( b2 )    -    -                     
 x   1  1     567  0.012639   10( a1 )   3( b2 )    -    -                     
 x   1  1     573 -0.016668    5( a1 )   4( b2 )    -    -                     
 x   1  1     591 -0.013107    1( a1 )   6( b2 )    -    -                     
 x   1  1     594 -0.013818    4( a1 )   6( b2 )    -    -                     
 x   1  1     719 -0.010142    9( a1 )   1( b1 )    -              -           
 x   1  1     726  0.014753    5( a1 )   2( b1 )    -              -           
 x   1  1     741 -0.010597    9( a1 )   3( b1 )    -              -           
 x   1  1     748 -0.014803    5( a1 )   4( b1 )    -              -           
 x   1  1     766 -0.012635    1( a1 )   6( b1 )    -              -           
 x   1  1     769 -0.013023    4( a1 )   6( b1 )    -              -           
 x   1  1     813  0.014754    5( a1 )   1( a2 )    -                   -      
 x   1  1     824 -0.014803    5( a1 )   2( a2 )    -                   -      
 x   1  1     848 -0.012070    7( b1 )   1( b2 )    -                   -      
 x   1  1     862 -0.012442    7( b1 )   3( b2 )    -                   -      
 x   1  1     884 -0.011512    1( b1 )   7( b2 )    -                   -      
 x   1  1     886 -0.011906    3( b1 )   7( b2 )    -                   -      
 x   1  1     899 -0.015443    3( a1 )   5( a1 )    -                        - 
 x   1  1     910 -0.014555    5( a1 )   7( a1 )    -                        - 
 x   1  1     919 -0.010454    1( a1 )   9( a1 )    -                        - 
 x   1  1     922 -0.010631    4( a1 )   9( a1 )    -                        - 
 x   1  1     961 -0.010046    3( b1 )   6( b1 )    -                        - 
 x   1  1     964  0.011246    1( b1 )   7( b1 )    -                        - 
 x   1  1     966  0.011596    3( b1 )   7( b1 )    -                        - 
 x   1  1     985  0.011586    1( b2 )   7( b2 )    -                        - 
 x   1  1     987  0.011904    3( b2 )   7( b2 )    -                        - 
 x   1  1    1012 -0.011493    1( a1 )   1( b2 )         -    -                
 x   1  1    1500  0.010928    1( b1 )   1( b2 )              -         -      
 x   1  1    1782 -0.010002    1( b1 )   1( b2 )                   -         - 
 x   1  1    1831  0.010004    1( a1 )   1( b1 )                        -    - 
 w   1  1    1933 -0.012587    2( a2 )   2( b1 )   +-                          
 w   1  1    1938 -0.012587    1( a2 )   4( b1 )   +-                          
 w   1  1    1939  0.021368    2( a2 )   4( b1 )   +-                          
 w   1  1    1946  0.017053    3( a2 )   6( b1 )   +-                          
 w   1  1    1949 -0.019684    3( a2 )   7( b1 )   +-                          
 w   1  1    1950  0.028391    1( a1 )   1( b2 )   +-                          
 w   1  1    1953  0.026191    4( a1 )   1( b2 )   +-                          
 w   1  1    1957  0.010502    8( a1 )   1( b2 )   +-                          
 w   1  1    1963  0.010271    3( a1 )   2( b2 )   +-                          
 w   1  1    1967 -0.012374    7( a1 )   2( b2 )   +-                          
 w   1  1    1972  0.026190    1( a1 )   3( b2 )   +-                          
 w   1  1    1975  0.025427    4( a1 )   3( b2 )   +-                          
 w   1  1    1979  0.011900    8( a1 )   3( b2 )   +-                          
 w   1  1    1985 -0.013174    3( a1 )   4( b2 )   +-                          
 w   1  1    1988  0.012879    6( a1 )   4( b2 )   +-                          
 w   1  1    1989  0.021006    7( a1 )   4( b2 )   +-                          
 w   1  1    1994 -0.010502    1( a1 )   5( b2 )   +-                          
 w   1  1    1997 -0.011900    4( a1 )   5( b2 )   +-                          
 w   1  1    2013  0.012880    9( a1 )   6( b2 )   +-                          
 w   1  1    2014  0.014799   10( a1 )   6( b2 )   +-                          
 w   1  1    2025 -0.018428   10( a1 )   7( b2 )   +-                          
 w   1  1    2048 -0.034933    1( a1 )   1( b2 )   +     -                     
 w   1  1    2051 -0.025868    4( a1 )   1( b2 )   +     -                     
 w   1  1    2063 -0.010157    5( a1 )   2( b2 )   +     -                     
 w   1  1    2070 -0.026145    1( a1 )   3( b2 )   +     -                     
 w   1  1    2073 -0.020907    4( a1 )   3( b2 )   +     -                     
 w   1  1    2079  0.010209   10( a1 )   3( b2 )   +     -                     
 w   1  1    2103  0.010081    1( a1 )   6( b2 )   +     -                     
 w   1  1    2106  0.010977    4( a1 )   6( b2 )   +     -                     
 w   1  1    2223 -0.031006    1( a1 )   1( b1 )   +               -           
 w   1  1    2226 -0.023403    4( a1 )   1( b1 )   +               -           
 w   1  1    2245 -0.022764    1( a1 )   3( b1 )   +               -           
 w   1  1    2248 -0.018557    4( a1 )   3( b1 )   +               -           
 w   1  1    2354 -0.031016    1( b1 )   1( b2 )   +                    -      
 w   1  1    2356 -0.022771    3( b1 )   1( b2 )   +                    -      
 w   1  1    2368 -0.023411    1( b1 )   3( b2 )   +                    -      
 w   1  1    2370 -0.018562    3( b1 )   3( b2 )   +                    -      
 w   1  1    2403 -0.023497    1( a1 )   1( a1 )   +                         - 
 w   1  1    2409 -0.021147    1( a1 )   4( a1 )   +                         - 
 w   1  1    2412 -0.010724    4( a1 )   4( a1 )   +                         - 
 w   1  1    2475  0.020184    1( b1 )   1( b1 )   +                         - 
 w   1  1    2478  0.024880    1( b1 )   3( b1 )   +                         - 
 w   1  1    2480  0.015443    3( b1 )   3( b1 )   +                         - 
 w   1  1    2503 -0.023492    1( b2 )   1( b2 )   +                         - 
 w   1  1    2506 -0.021138    1( b2 )   3( b2 )   +                         - 
 w   1  1    2508 -0.010719    3( b2 )   3( b2 )   +                         - 
 w   1  1    2552 -0.016789    1( a1 )   1( b2 )        +-                     
 w   1  1    2555 -0.011541    4( a1 )   1( b2 )        +-                     
 w   1  1    2574 -0.011289    1( a1 )   3( b2 )        +-                     
 w   1  1    2907  0.056851    1( a1 )   1( a1 )        +                    - 
 w   1  1    2913  0.049097    1( a1 )   4( a1 )        +                    - 
 w   1  1    2916  0.024950    4( a1 )   4( a1 )        +                    - 
 w   1  1    2921  0.016661    5( a1 )   5( a1 )        +                    - 
 w   1  1    2979  0.044089    1( b1 )   1( b1 )        +                    - 
 w   1  1    2982  0.037890    1( b1 )   3( b1 )        +                    - 
 w   1  1    2984  0.019467    3( b1 )   3( b1 )        +                    - 
 w   1  1    3007  0.058333    1( b2 )   1( b2 )        +                    - 
 w   1  1    3010  0.050528    1( b2 )   3( b2 )        +                    - 
 w   1  1    3012  0.025690    3( b2 )   3( b2 )        +                    - 
 w   1  1    3133  0.010329    1( a1 )   1( b1 )             +     -           
 w   1  1    3264 -0.013354    1( b1 )   1( b2 )             +          -      
 w   1  1    3313  0.016094    1( a1 )   1( a1 )             +               - 
 w   1  1    3319  0.014251    1( a1 )   4( a1 )             +               - 
 w   1  1    3462 -0.012158    1( a1 )   1( b2 )                  +-           
 w   1  1    3539  0.052824    1( a1 )   1( a1 )                  +     -      
 w   1  1    3545  0.045855    1( a1 )   4( a1 )                  +     -      
 w   1  1    3548  0.023327    4( a1 )   4( a1 )                  +     -      
 w   1  1    3553  0.014796    5( a1 )   5( a1 )                  +     -      
 w   1  1    3611  0.035771    1( b1 )   1( b1 )                  +     -      
 w   1  1    3614  0.030383    1( b1 )   3( b1 )                  +     -      
 w   1  1    3616  0.015604    3( b1 )   3( b1 )                  +     -      
 w   1  1    3639  0.052829    1( b2 )   1( b2 )                  +     -      
 w   1  1    3642  0.045858    1( b2 )   3( b2 )                  +     -      
 w   1  1    3644  0.023327    3( b2 )   3( b2 )                  +     -      
 w   1  1    3770 -0.012151    1( a1 )   1( b2 )                       +-      

 ci coefficient statistics:
           rq > 0.1                6
      0.1> rq > 0.01             154
     0.01> rq > 0.001            865
    0.001> rq > 0.0001           738
   0.0001> rq > 0.00001          204
  0.00001> rq > 0.000001          27
 0.000001> rq                   2048
           all                  4042
  iref  icsf         v(icsf)             hv(icsf)
     1     1     -0.676519950874
     2     2     -0.149810357461
     3     3     -0.600491255959
     4     4     -0.127503037355
     5     5     -0.031478081057
     6     6     -0.126170084024
     7     7     -0.054622510171
     8     8     -0.027977986024
     9     9      0.063663905389
    10    10     -0.027976315699
    11    11      0.126322841254
    12    12     -0.084658498010
    13    13     -0.084643698380
    14    14      0.009843360922
    15    15      0.003098413735
    16    16     -0.044429348178
    17    17     -0.015434164784
    18    18      0.018292828160
    19    19     -0.025313424878
    20    20     -0.019734591539
    21    21      0.049931818256
    22    22      0.007643305291
    23    23     -0.017609052996
    24    24      0.026477430702

 number of reference csfs (nref) is    24.  root number (iroot) is  1.
 c0**2 =   0.92001641  c0**2(renormalized) =   0.99360263
 c**2 (all zwalks) =   0.92001641  c**2(all zwalks, renormalized) =   0.99360263
 passed aftci ... 
 readint2: molcas,dalton2=                     0                     0
 files%faoints=aoints              
lodens (list->root)=  1
sifcfg setup: record length 4096 DP
# d1 elements per record  3272
# d2 elements per record  2730
  The AQCC density will be calculated.
 item #                     1 suffix=:.drt1.state1:
 read_civout: repnuc=  -43.4985876562748     
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method: 30 last record  1max overlap with ref# 96% root-following 0
 MR-CISD energy:   -46.32656802    -2.82798037
 residuum:     0.00084352
 deltae:     0.00000715
 apxde:     0.00000082
 a4den:     1.01351070
 reference energy:   -46.27407927    -2.77549162

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.95910401     0.15955418    -0.11440970     0.17639193    -0.07492037     0.06960751     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.95910401     0.15955418     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 rdcirefv: extracting reference vector # 1(blocksize=    24 #zcsf=      24)
 =========== Executing IN-CORE method ==========
================================================================================
   DYZ=       0  DYX=       0  DYW=       0
   D0Z=      34  D0Y=       0  D0X=       0  D0W=       0
  DDZI=      80 DDYI=       0 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
 root #                      1 : Scaling(1) with    1.01351070348743      
 corresponding to ref #                      1
 computing final density
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=     106  DYX=     130  DYW=     160
   D0Z=      34  D0Y=     103  D0X=      12  D0W=      18
  DDZI=      80 DDYI=     180 DDXI=      30 DDWI=      36
  DDZE=       0 DDYE=      70 DDXE=      15 DDWE=      21
================================================================================
Trace of MO density:     4.000000
    4  correlated and     8  frozen core electrons

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     1.80557773     0.20073042     0.04456300     0.00000000    -0.00944923     0.02048454
   MO   4     0.00000000     0.00000000     0.20073042     0.54458140     0.11102689     0.00000000    -0.00865907     0.01653929
   MO   5     0.00000000     0.00000000     0.04456300     0.11102689     0.06783174     0.00000000     0.00207527     0.00550752
   MO   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.02760784     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000    -0.00944923    -0.00865907     0.00207527     0.00000000     0.00088217    -0.00042453
   MO   8     0.00000000     0.00000000     0.02048454     0.01653929     0.00550752     0.00000000    -0.00042453     0.00160569
   MO   9     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.01926498     0.00000000     0.00000000
   MO  10     0.00000000     0.00000000    -0.04074729     0.03484178     0.00771342     0.00000000    -0.00057104     0.00123754
   MO  11     0.00000000     0.00000000    -0.00945814    -0.00926957     0.00267344     0.00000000     0.00104251    -0.00060780
   MO  12     0.00000000     0.00000000    -0.01542986    -0.01287370    -0.00574972     0.00000000     0.00029085    -0.00164153
   MO  13     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00415669     0.00000000     0.00000000
   MO  14     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00026119     0.00000000     0.00000000
   MO  15     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00235305     0.00000000     0.00000000
   MO  16     0.00000000     0.00000000     0.00037491    -0.00155610    -0.00034457     0.00000000     0.00003042    -0.00006593

                MO   9         MO  10         MO  11         MO  12         MO  13         MO  14         MO  15         MO  16
   MO   3     0.00000000    -0.04074729    -0.00945814    -0.01542986     0.00000000     0.00000000     0.00000000     0.00037491
   MO   4     0.00000000     0.03484178    -0.00926957    -0.01287370     0.00000000     0.00000000     0.00000000    -0.00155610
   MO   5     0.00000000     0.00771342     0.00267344    -0.00574972     0.00000000     0.00000000     0.00000000    -0.00034457
   MO   6     0.01926498     0.00000000     0.00000000     0.00000000     0.00415669    -0.00026119    -0.00235305     0.00000000
   MO   7     0.00000000    -0.00057104     0.00104251     0.00029085     0.00000000     0.00000000     0.00000000     0.00003042
   MO   8     0.00000000     0.00123754    -0.00060780    -0.00164153     0.00000000     0.00000000     0.00000000    -0.00006593
   MO   9     0.01388648     0.00000000     0.00000000     0.00000000     0.00333655    -0.00002452    -0.00185423     0.00000000
   MO  10     0.00000000     0.00886197    -0.00060181    -0.00098170     0.00000000     0.00000000     0.00000000    -0.00039091
   MO  11     0.00000000    -0.00060181     0.00133498     0.00054959     0.00000000     0.00000000     0.00000000     0.00003530
   MO  12     0.00000000    -0.00098170     0.00054959     0.00189368     0.00000000     0.00000000     0.00000000     0.00005758
   MO  13     0.00333655     0.00000000     0.00000000     0.00000000     0.00109231     0.00006195    -0.00063966     0.00000000
   MO  14    -0.00002452     0.00000000     0.00000000     0.00000000     0.00006195     0.00124252     0.00047931     0.00000000
   MO  15    -0.00185423     0.00000000     0.00000000     0.00000000    -0.00063966     0.00047931     0.00190026     0.00000000
   MO  16     0.00000000    -0.00039091     0.00003530     0.00005758     0.00000000     0.00000000     0.00000000     0.00002246

Natural orbital populations,block 1
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     2.00000000     1.84010232     0.53992022     0.04436188     0.04212380     0.00519470     0.00238172
              MO     9       MO    10       MO    11       MO    12       MO    13       MO    14       MO    15       MO    16
  occ(*)=     0.00198625     0.00111945     0.00055118     0.00048557     0.00006230     0.00001451     0.00001434     0.00000300

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4
   MO   1     0.43827942     0.01544272    -0.01353427     0.00000000
   MO   2     0.01544272     0.00152794    -0.00162537     0.00000000
   MO   3    -0.01353427    -0.00162537     0.00192615     0.00000000
   MO   4     0.00000000     0.00000000     0.00000000     0.00210877

Natural orbital populations,block 2
              MO     1       MO     2       MO     3       MO     4       MO
  occ(*)=     0.43924667     0.00241888     0.00210877     0.00006797

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     0.43827632     0.00000000     0.01543874     0.00000000    -0.01353195     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.01650779     0.00000000     0.01178114     0.00000000     0.00249672    -0.00091847
   MO   4     0.00000000     0.01543874     0.00000000     0.00152723     0.00000000    -0.00162491     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.01178114     0.00000000     0.00881888     0.00000000     0.00214255    -0.00058391
   MO   6     0.00000000    -0.01353195     0.00000000    -0.00162491     0.00000000     0.00192589     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000     0.00249672     0.00000000     0.00214255     0.00000000     0.00072253    -0.00014743
   MO   8     0.00000000     0.00000000    -0.00091847     0.00000000    -0.00058391     0.00000000    -0.00014743     0.00142310
   MO   9     0.00000000     0.00000000     0.00106063     0.00000000     0.00067429     0.00000000     0.00017026    -0.00058726

                MO   9
   MO   2     0.00000000
   MO   3     0.00106063
   MO   4     0.00000000
   MO   5     0.00067429
   MO   6     0.00000000
   MO   7     0.00017026
   MO   8    -0.00058726
   MO   9     0.00159218

Natural orbital populations,block 3
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     0.43924314     0.02560934     0.00241839     0.00198805     0.00091433     0.00054207     0.00006791
              MO     9       MO
  occ(*)=     0.00001069

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     0.55706103     0.00000000     0.01841269     0.00000000    -0.01578341     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.02761134     0.00000000     0.01926706     0.00000000    -0.00415662    -0.00219047
   MO   4     0.00000000     0.01841269     0.00000000     0.00188398     0.00000000    -0.00196804     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.01926706     0.00000000     0.01388765     0.00000000    -0.00333644    -0.00177601
   MO   6     0.00000000    -0.01578341     0.00000000    -0.00196804     0.00000000     0.00236363     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000    -0.00415662     0.00000000    -0.00333644     0.00000000     0.00109222     0.00063211
   MO   8     0.00000000     0.00000000    -0.00219047     0.00000000    -0.00177601     0.00000000     0.00063211     0.00159707
   MO   9     0.00000000     0.00000000     0.00089794     0.00000000     0.00053323     0.00000000    -0.00011615    -0.00058085

                MO   9
   MO   2     0.00000000
   MO   3     0.00089794
   MO   4     0.00000000
   MO   5     0.00053323
   MO   6     0.00000000
   MO   7    -0.00011615
   MO   8    -0.00058085
   MO   9     0.00154562

Natural orbital populations,block 4
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     0.55812249     0.04212832     0.00307169     0.00198639     0.00111947     0.00048538     0.00011446
              MO     9       MO
  occ(*)=     0.00001434


 total number of electrons =   12.0000000000

 test slabel:                     4
  a1  a2  b1  b2 


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        a1  partial gross atomic populations
   ao class       1a1        2a1        3a1        4a1        5a1        6a1 
     3_ s       0.000000   2.000000   1.792725   0.016820   0.000000   0.000000
     3_ p       2.000000   0.000000   0.000000   0.000000   0.000000   0.041881
     3_ d       0.000000   0.000000   0.047377   0.523100   0.044362   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000000   0.000000   0.000243
 
   ao class       7a1        8a1        9a1       10a1       11a1       12a1 
     3_ s       0.004706   0.000207   0.000000   0.000000   0.000000   0.000000
     3_ p       0.000000   0.000000   0.000044   0.000299   0.000000   0.000348
     3_ d       0.000489   0.002175   0.000000   0.000000   0.000551   0.000000
     3_ f       0.000000   0.000000   0.001942   0.000820   0.000000   0.000137
 
   ao class      13a1       14a1       15a1       16a1 
     3_ s       0.000001   0.000000   0.000000   0.000003
     3_ p       0.000000   0.000000   0.000014   0.000000
     3_ d       0.000061   0.000015   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000000

                        a2  partial gross atomic populations
   ao class       1a2        2a2        3a2        4a2 
     3_ d       0.439247   0.002419   0.000000   0.000068
     3_ f       0.000000   0.000000   0.002109   0.000000

                        b1  partial gross atomic populations
   ao class       1b1        2b1        3b1        4b1        5b1        6b1 
     3_ p       2.000000   0.000000   0.025480   0.000000   0.000018   0.000000
     3_ d       0.000000   0.439243   0.000000   0.002418   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000129   0.000000   0.001970   0.000914
 
   ao class       7b1        8b1        9b1 
     3_ p       0.000541   0.000000   0.000011
     3_ d       0.000000   0.000068   0.000000
     3_ f       0.000001   0.000000   0.000000

                        b2  partial gross atomic populations
   ao class       1b2        2b2        3b2        4b2        5b2        6b2 
     3_ p       2.000000   0.000000   0.041886   0.000000   0.000044   0.000299
     3_ d       0.000000   0.558122   0.000000   0.003072   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000243   0.000000   0.001943   0.000820
 
   ao class       7b2        8b2        9b2 
     3_ p       0.000348   0.000000   0.000014
     3_ d       0.000000   0.000114   0.000000
     3_ f       0.000137   0.000000   0.000000


                        gross atomic populations
     ao            3_
      s         3.814462
      p         6.111227
      d         2.062901
      f         0.011410
    total      12.000000
 

 Total number of electrons:   12.00000000

 DA ...
