 total ao core energy =    0.000000000
 MCSCF calculation performed for  4 DRTs.

 DRT  first state   no.of aver. states   weights
  1   ground state          1             0.143
  2   ground state          2             0.143 0.143
  3   ground state          2             0.143 0.143
  4   ground state          2             0.143 0.143

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a1 
 Total number of electrons:    12
 Spin multiplicity:            3
 Number of active orbitals:    6
 Number of active electrons:   4
 Total number of CSFs:        21

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a2 
 Total number of electrons:    12
 Spin multiplicity:            3
 Number of active orbitals:    6
 Number of active electrons:   4
 Total number of CSFs:        28

 DRT file header:
  title                                                                          
 Molecular symmetry group:    b1 
 Total number of electrons:    12
 Spin multiplicity:            3
 Number of active orbitals:    6
 Number of active electrons:   4
 Total number of CSFs:        28

 DRT file header:
  title                                                                          
 Molecular symmetry group:    b2 
 Total number of electrons:    12
 Spin multiplicity:            3
 Number of active orbitals:    6
 Number of active electrons:   4
 Total number of CSFs:        28

 Number of active-double rotations:         8
 Number of active-active rotations:         0
 Number of double-virtual rotations:       36
 Number of active-virtual rotations:       50
 
 iter     emc (average)    demc       wnorm      knorm      apxde  qcoupl
    1    -46.3018785757  4.630E+01  6.435E-07  5.432E-08  1.793E-14  F   *not conv.*     

 final mcscf convergence values:
    2    -46.3018785757  3.055E-13  8.053E-07  3.104E-08  1.278E-14  F   *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 wt 0.143 total energy=      -46.301878576, rel. (eV)=   0.000000
   DRT #2 state # 1 wt 0.143 total energy=      -46.301878577, rel. (eV)=   0.000000
   DRT #2 state # 2 wt 0.143 total energy=      -46.301878576, rel. (eV)=   0.000000
   DRT #3 state # 1 wt 0.143 total energy=      -46.301878577, rel. (eV)=   0.000000
   DRT #3 state # 2 wt 0.143 total energy=      -46.301878574, rel. (eV)=   0.000000
   DRT #4 state # 1 wt 0.143 total energy=      -46.301878576, rel. (eV)=   0.000000
   DRT #4 state # 2 wt 0.143 total energy=      -46.301878574, rel. (eV)=   0.000000
   ------------------------------------------------------------


