1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.

  This is a  mraqcc energy evaluation      

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  1  1    -46.2740792806  7.1054E-15  5.6605E-02  2.2790E-01  1.0000E-03
 mraqcc  #  2  1    -46.3235208343  4.9442E-02  2.4014E-03  4.5231E-02  1.0000E-03
 mraqcc  #  3  1    -46.3261690664  2.6482E-03  3.6164E-04  1.7236E-02  1.0000E-03
 mraqcc  #  4  1    -46.3265083671  3.3930E-04  6.6589E-05  7.5033E-03  1.0000E-03
 mraqcc  #  5  1    -46.3265614279  5.3061E-05  6.9997E-06  2.3861E-03  1.0000E-03
 mraqcc  #  6  1    -46.3265680804  6.6525E-06  7.1308E-07  8.0670E-04  1.0000E-03

 mraqcc   convergence criteria satisfied after  6 iterations.

 final mraqcc   convergence information:
 mraqcc  #  6  1    -46.3265680804  6.6525E-06  7.1308E-07  8.0670E-04  1.0000E-03

 number of reference csfs (nref) is    33.  root number (iroot) is  1.
 c0**2 =   0.92001337  c0**2(renormalized) =   0.99360214
 c**2 (all zwalks) =   0.92001337  c**2(all zwalks, renormalized) =   0.99360214
