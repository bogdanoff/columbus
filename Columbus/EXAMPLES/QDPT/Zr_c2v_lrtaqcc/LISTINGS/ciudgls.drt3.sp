1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      2009-03.    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


================ Computing sorting integral file structure ================

                    -----z----- -----y----- -----x----- -----w----- ---total---

                CSFs        24         490        1414        2114        4042
      internal walks       105          70          15          21         211
valid internal walks        24          70          15          21         130
 getinfoarray: info=                     6 :                     1
                  8192                  6552                  8192
                  5460                     0
 icd(3)=                  2976 ci%nnlev=                   595  l2rec=
                  8192  n2max=                  5460
 lcore1,lcore2=              13068137              13044821
 lencor,maxblo              13107200                 60000
========================================
 current settings:
 minbl3         381
 minbl4         381
 locmaxbl3     4288
 locmaxbuf     2144
 maxbl3       60000
 maxbl3       60000
 maxbl4       60000
 maxbuf       30006
========================================

 sorted 4-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)

 sorted 3-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)
 Orig.  diagonal integrals:  1electron:        34
                             0ext.    :        42
                             2ext.    :       336
                             4ext.    :       812


 Orig. off-diag. integrals:  4ext.    :     24411
                             3ext.    :     18720
                             2ext.    :      6792
                             1ext.    :      1272
                             0ext.    :        99
                             2ext. SO :         0
                             1ext. SO :         0
                             0ext. SO :         0
                             1electron:       128


 Sorted integrals            3ext.  w :     17370 x :     16020
                             4ext.  w :     20955 x :     17777


Cycle #  1 sortfile size=    163835(       5 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13068137
Cycle #  2 sortfile size=    163835(       5 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13068137
 minimum size of srtscr:     98301 WP (     3 records)
 maximum size of srtscr:    163835 WP (     5 records)
diagi   file:      4 records  of   1536 WP each=>       6144 WP total
ofdgi   file:      8 records  of   1536 WP each=>      12288 WP total
fil3w   file:      1 records  of  30006 WP each=>      30006 WP total
fil3x   file:      1 records  of  30006 WP each=>      30006 WP total
fil4w   file:      1 records  of  30006 WP each=>      30006 WP total
fil4x   file:      1 records  of  30006 WP each=>      30006 WP total
 compressed index vector length=                    63
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 3,
  GSET = 3,
   DAVCOR =10,
  NCOREL = 4
   nroot=1
  IVMODE = 3
  NBKITR = 1
   nvbkmn=2
  RTOLBK = 1e-3
   niter=30
   nvcimn=2
   rtolci=1e-3
  NVCIMX = 6
  NVRFMX = 6
  NVBKMX = 6
  IDEN  = 1
  CSFPRN = 10,
  update_mode=1
  nvrfmn=2
   lrtshift=-0.0465482970
 /&end
 ------------------------------------------------------------------------
lodens (list->root)=  1
invlodens (root->list)=  1
 LRT calculation: diagonal shift= -4.654829700000000E-002
 bummer (warning):2:changed keyword: nbkitr=                      0
 USING SEGMENTS OF EQUAL SIZE

****************  list of control variables  ****************
 lvlprt =    0      nroot  =    1      noldv  =   0      noldhv =   0
 nunitv =    1      nbkitr =    0      niter  =  30      davcor =  10
 csfprn =   10      ivmode =    3      istrt  =   0      vout   =   0
 iortls =    0      nvbkmx =    6      ibktv  =  -1      ibkthv =  -1
 nvcimx =    6      icitv  =   -1      icithv =  -1      frcsub =   0
 nvbkmn =    2      nvcimn =    2      maxseg =   4      nrfitr =  30
 ncorel =    4      nvrfmx =    6      nvrfmn =   2      iden   =   1
 itran  =    0      froot  =    0      rtmode =   0      ncouple=   1
 skipso =    F      dalton2=    0      molcas =   0      finalv =   0
 finalw =    0      cosmocalc=   0    with_tsklst=   0
 nsegwx =    1     1     1     1
 nseg0x =    1     1     1     1
 nseg1x =    1     1     1     1
 nseg2x =    1     1     1     1
 nseg3x =    1     1     1     1
 nseg4x =    1     1     1     1
 no0ex  =      0    no1ex  =      0    no2ex  =     0    no3ex  =     0
 no4ex  =      0    nodiag =      0
 cdg4ex =    1      c3ex1ex=    1      c2ex0ex=   1
 fileloc=    0     0     0     0     0     0     0     1     1     1
 directhd=   1      noaqccshift_zyxw=      0
 critical_crit=-1.00000    critical_delta= 0.05000

 ctol   = 0.010000    lrtshift=-.046548    smalld =0.001000


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-03    1.000E-03
 Computing density:                    .drt1.state1
 using                      1  nodes and                      1  cores.
 szdg/szodg per processor=                  6144                 12288
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core           13107199 DP per process

********** Integral sort section *************


 workspace allocation information: lencor=  13107199

 echo of the input for program cisrt:
 ------------------------------------------------------------------------
  &input
  maxbl3=60000
  maxbl4=60000
  &end
 ------------------------------------------------------------------------
 
 ( 6) listing file:                    ciudgls             
 ( 5) input file:                      cisrtin   
 (17) cidrt file:                      cidrtfl             
 (11) transformed integrals file:      moints    
 (12) diagonal integral file:          diagint             
 (13) off-diagonal integral file:      ofdgint             
 (31) 4-external w integrals file:     fil4w               
 (32) 4-external x integrals file:     fil4x               
 (33) 3-external w integrals file:     fil3w               
 (34) 3-external x integrals file:     fil3x               
 (21) scratch da sorting file:         srtscr              
 (12) 2-e integral file [fsplit=2]:    moints2   

 input integral file header information:
                                                                                 
 aoints SIFS file created by argos.      zam792            17:56:38.564 17-Dec-13
  cidrt_title                                                                    
 MO-coefficients from mcscf.x                                                    
  with dummy occupation 1.0 for active orbitals                                  
  total ao core energy =    0.000000000                                          
 MCSCF energy =     -46.301878576                                                
 SIFS file created by program tran.      zam792            17:56:38.888 17-Dec-13

 input energy(*) values:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.

 total core energy =   0.000000000000E+00

 nsym = 4 nmot=  34

 symmetry  =    1    2    3    4
 slabel(*) =   a1   a2   b1   b2
 nmpsy(*)  =   14    4    8    8

 info(*) =          1      8192      6552      8192      5460         0

 orbital labels, i:molab(i)=
   1:tout:001   2:tout:002   3:tout:003   4:tout:004   5:tout:005   6:tout:006   7:tout:007   8:tout:008   9:tout:009  10:tout:010
  11:tout:011  12:tout:012  13:tout:013  14:tout:014  15:tout:015  16:tout:016  17:tout:017  18:tout:018  19:tout:019  20:tout:020
  21:tout:021  22:tout:022  23:tout:023  24:tout:024  25:tout:025  26:tout:026  27:tout:027  28:tout:028  29:tout:029  30:tout:030
  31:tout:031  32:tout:032  33:tout:033  34:tout:034

 input parameters:
 prnopt=  0
 ldamin=    4095 ldamax=   32767 ldainc=      64
 maxbuf=   30006 maxbl3=   60000 maxbl4=   60000 intmxo=    1536
  Using 32 bit compression 

 drt information:
  cidrt_title                                                                    
 nmotd =  38 nfctd =   4 nfvtc =   0 nmot  =  34
 nlevel =  34 niot  =   6 lowinl=  29
 orbital-to-level map(*)
   -1  -1  29  30  31   1   2   3   4   5   6   7   8   9  10  11  32  12  13  14
   -1  33  15  16  17  18  19  20  21  -1  34  22  23  24  25  26  27  28
 compressed map(*)
   29  30  31   1   2   3   4   5   6   7   8   9  10  11  32  12  13  14  33  15
   16  17  18  19  20  21  34  22  23  24  25  26  27  28
 levsym(*)
    1   1   1   1   1   1   1   1   1   1   1   2   2   2   3   3   3   3   3   3
    3   4   4   4   4   4   4   4   1   1   1   2   3   4
 repartitioning mu(*)=
   0.  0.  0.  0.  0.  0.

 new core energy added to the energy(*) list.
 from the integral file: h1_core= -4.349858765627E+01

 indxdg: diagonal integral statistics.
 total number of integrals contributing to diagonal matrix elements:      1190
 number with all external indices:       812
 number with half external - half internal indices:       336
 number with all internal indices:        42

 indxof: off-diagonal integral statistics.
    4-external integrals: num=      24411 strt=          1
    3-external integrals: num=      18720 strt=      24412
    2-external integrals: num=       6792 strt=      43132
    1-external integrals: num=       1272 strt=      49924
    0-external integrals: num=         99 strt=      51196

 total number of off-diagonal integrals:       51294


 indxof(2nd)  ittp=   3 numx(ittp)=        6792
 indxof(2nd)  ittp=   4 numx(ittp)=        1272
 indxof(2nd)  ittp=   5 numx(ittp)=          99

 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg=  12931980
 pro2e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652

 pro2e:     25126 integrals read in     5 records.

 pro2e:         0 integrals 34-ext integrals skipped.
 pro1e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652
 pro1e: eref =    0.000000000000000E+00
 total size of srtscr:                     4  records of                  32767 
 WP =               1048544 Bytes
 putdg        1     596    1191    1786    3322   36089   57934    3018   46706   90394
   123161  131353  136813  158652

 putf:       4 buffers of length    1536 written to file 12
 diagonal integral file completed.

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep4e:    17 blocks of linear combinations of 4-external integrals processed.
 number of sorted 4-external integrals      38732
 number of original 4-external integrals    24411


 putf34: external integral file complete. nfilw=    31 nfilx=    32 nrecw=     1 nrecx=     1 lbufp= 30006

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep3e:    19 blocks of linear combinations of 3-external integrals processed.
 number of sorted 3-external integrals      33390
 number of original 3-external integrals    18720


 putf34: external integral file complete. nfilw=    33 nfilx=    34 nrecw=     1 nrecx=     1 lbufp= 30006
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      6792         3     43132     43132      6792     51294
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      1272         4     49924     49924      1272     51294
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd        99         5     51196     51196        99     51294

 putf:       8 buffers of length    1536 written to file 13
 off-diagonal files sort completed.
 executing brd_struct for cisrtinfo
cisrtinfo:
bufszi  1536
 diagfile 4ext:     812 2ext:     336 0ext:      42
 fil4w,fil4x  :   24411 fil3w,fil3x :   18720
 ofdgint  2ext:    6792 1ext:    1272 0ext:      99so0ext:       0so1ext:       0so2ext:       0
buffer minbl4     381 minbl3     381 maxbl2     384nbas:  11   3   7   7   0   0   0   0 maxbuf 30006
 CIUDG version 5.9.7 ( 5-Oct-2004)

 workspace allocation information: lcore=  13107199

 core energy values from the integral file:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -4.349858765627E+01, ietype=    6,   fcore energy of type: H1(*)   

 total core repulsion energy = -4.349858765627E+01
 nmot  =    38 niot  =     6 nfct  =     4 nfvt  =     0
 nrow  =    32 nsym  =     4 ssym  =     3 lenbuf=  1600
 nwalk,xbar:        211      105       70       15       21
 nvalwt,nvalw:      130       24       70       15       21
 ncsft:            4042
 total number of valid internal walks:     130
 nvalz,nvaly,nvalx,nvalw =       24      70      15      21

 cisrt info file parameters:
 file number  12 blocksize   1536
 mxbld   1536
 nd4ext,nd2ext,nd0ext   812   336    42
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int    24411    18720     6792     1272       99        0        0        0
 minbl4,minbl3,maxbl2   381   381   384
 maxbuf 30006
 number of external orbitals per symmetry block:  11   3   7   7
 nmsym   4 number of internal orbitals   6
 executing brd_struct for drt
 executing brd_struct for orbinf
 executing brd_struct for momap
 calcthrxt: niot,maxw1=                     6                    31
 block size     0
 pthz,pthy,pthx,pthw:   105    70    15    21 total internal walks:     211
 maxlp3,n2lp,n1lp,n0lp    31     0     0     0
 orbsym(*)= 1 1 1 2 3 4

 setref:       24 references kept,
               81 references were marked as invalid, out of
              105 total.
 nmb.of records onel     1
 nmb.of records 2-ext     5
 nmb.of records 1-ext     1
 nmb.of records 0-ext     1
 nmb.of records 2-int     0
 nmb.of records 1-int     0
 nmb.of records 0-int     0
 ---------memory usage in DP -----------------
 < n-ex core usage >
     routines:
    fourex            61797
    threx             60199
    twoex              3549
    onex               1823
    allin              1536
    diagon             2109
               =======
   maximum            61797
 
  __ static summary __ 
   reflst                24
   hrfspc                24
               -------
   static->              48
 
  __ core required  __ 
   totstc                48
   max n-ex           61797
               -------
   totnec->           61845
 
  __ core available __ 
   totspc          13107199
   totnec -           61845
               -------
   totvec->        13045354

 number of external paths / symmetry
 vertex x     100      82      98      98
 vertex w     128      82      98      98
segment: free space=    13045354
 reducing frespc by                   774 to               13044580 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1          47|        24|         0|        24|         0|         1|
 -------------------------------------------------------------------------------
  Y 2          70|       490|        24|        70|        24|         2|
 -------------------------------------------------------------------------------
  X 3          15|      1414|       514|        15|        94|         3|
 -------------------------------------------------------------------------------
  W 4          21|      2114|      1928|        21|       109|         4|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=          60DP  conft+indsym=         280DP  drtbuffer=         434 DP

dimension of the ci-matrix ->>>      4042

 executing brd_struct for civct
 gentasklist: ntask=                    20
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS   
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      15      47       1414         24      15      24
     2  4   1    25      two-ext wz   2X  4 1      21      47       2114         24      21      24
     3  4   3    26      two-ext wx*  WX  4 3      21      15       2114       1414      21      15
     4  4   3    27      two-ext wx+  WX  4 3      21      15       2114       1414      21      15
     5  2   1    11      one-ext yz   1X  2 1      70      47        490         24      70      24
     6  3   2    15      1ex3ex yx    3X  3 2      15      70       1414        490      15      70
     7  4   2    16      1ex3ex yw    3X  4 2      21      70       2114        490      21      70
     8  1   1     1      allint zz    OX  1 1      47      47         24         24      24      24
     9  2   2     5      0ex2ex yy    OX  2 2      70      70        490        490      70      70
    10  3   3     6      0ex2ex xx*   OX  3 3      15      15       1414       1414      15      15
    11  3   3    18      0ex2ex xx+   OX  3 3      15      15       1414       1414      15      15
    12  4   4     7      0ex2ex ww*   OX  4 4      21      21       2114       2114      21      21
    13  4   4    19      0ex2ex ww+   OX  4 4      21      21       2114       2114      21      21
    14  2   2    42      four-ext y   4X  2 2      70      70        490        490      70      70
    15  3   3    43      four-ext x   4X  3 3      15      15       1414       1414      15      15
    16  4   4    44      four-ext w   4X  4 4      21      21       2114       2114      21      21
    17  1   1    75      dg-024ext z  OX  1 1      47      47         24         24      24      24
    18  2   2    76      dg-024ext y  OX  2 2      70      70        490        490      70      70
    19  3   3    77      dg-024ext x  OX  3 3      15      15       1414       1414      15      15
    20  4   4    78      dg-024ext w  OX  4 4      21      21       2114       2114      21      21
----------------------------------------------------------------------------------------------------
REDTASK #   1 TIME=  19.000 N=  1 (task/type/sgbra)=(   1/24/0) (
REDTASK #   2 TIME=  18.000 N=  1 (task/type/sgbra)=(   2/25/0) (
REDTASK #   3 TIME=  17.000 N=  1 (task/type/sgbra)=(   3/26/1) (
REDTASK #   4 TIME=  16.000 N=  1 (task/type/sgbra)=(   4/27/2) (
REDTASK #   5 TIME=  15.000 N=  1 (task/type/sgbra)=(   5/11/0) (
REDTASK #   6 TIME=  14.000 N=  1 (task/type/sgbra)=(   6/15/0) (
REDTASK #   7 TIME=  13.000 N=  1 (task/type/sgbra)=(   7/16/0) (
REDTASK #   8 TIME=  12.000 N=  1 (task/type/sgbra)=(   8/ 1/0) (
REDTASK #   9 TIME=  11.000 N=  1 (task/type/sgbra)=(   9/ 5/0) (
REDTASK #  10 TIME=  10.000 N=  1 (task/type/sgbra)=(  10/ 6/1) (
REDTASK #  11 TIME=   9.000 N=  1 (task/type/sgbra)=(  11/18/2) (
REDTASK #  12 TIME=   8.000 N=  1 (task/type/sgbra)=(  12/ 7/1) (
REDTASK #  13 TIME=   7.000 N=  1 (task/type/sgbra)=(  13/19/2) (
REDTASK #  14 TIME=   6.000 N=  1 (task/type/sgbra)=(  14/42/1) (
REDTASK #  15 TIME=   5.000 N=  1 (task/type/sgbra)=(  15/43/1) (
REDTASK #  16 TIME=   4.000 N=  1 (task/type/sgbra)=(  16/44/1) (
REDTASK #  17 TIME=   3.000 N=  1 (task/type/sgbra)=(  17/75/1) (
REDTASK #  18 TIME=   2.000 N=  1 (task/type/sgbra)=(  18/76/1) (
REDTASK #  19 TIME=   1.000 N=  1 (task/type/sgbra)=(  19/77/1) (
REDTASK #  20 TIME=   0.000 N=  1 (task/type/sgbra)=(  20/78/1) (
 initializing v-file: 1:                  4042

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         176 2x:           0 4x:           0
All internal counts: zz :         271 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 reference space has dimension      24
 dsyevx: computed roots 1 to    2(converged:   2)

    root           eigenvalues
    ----           ------------
       1         -46.2740792781
       2         -46.2533198525

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt=                    24

         vector  1 from unit 11 written to unit 49 filename cirefv              
 ncorel,neli=                     4                     4
  This is a  mraqcc energy evaluation      

    ------------------------------------------------------------

 setopt2: mraqcc energy evaluation      gvalue=  0.833333333333333      ncorel=
                     4

    ------------------------------------------------------------


    reference energies:

  reference state used is:   1with energy      -46.2740792781

  ### begin active excitation selection ###

 inactive(1:nintern)=AAAAAA
 There are         24 reference CSFs out of         24 valid zwalks.
 There are    0 inactive and    6 active orbitals.
      24 active-active  and        0 inactive excitations.

  ### end active excitation selection ###


 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              4042
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                   30
 residual norm convergence criteria:               0.001000

          starting ci iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         602 2x:         246 4x:         106
All internal counts: zz :         271 yy:         620 xx:          66 ww:         132
One-external counts: yz :        1072 yx:        1000 yw:        1210
Two-external counts: yy :         865 ww:         252 xx:         180 xz:          72 wz:         104 wx:         360
Three-ext.   counts: yx :         320 yw:         395

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1
   ht   1    -2.77549162
calca4: root=   1 anorm**2=  0.00000000 scale:  1.00000000

          calcsovref: tciref block   1

              civs   1
 refs   1    1.00000    

          calcsovref: scrb block   1

                ci   1
 civs   1    1.00000    

          calcsovref: sovref block   1

              v      1
 ref    1    1.00000    

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  1  1    -46.2740792781 -7.1054E-15  5.7906E-02  2.2790E-01  1.0000E-03
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   2

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         602 2x:         246 4x:         106
All internal counts: zz :         271 yy:         620 xx:          66 ww:         132
One-external counts: yz :        1072 yx:        1000 yw:        1210
Two-external counts: yy :         865 ww:         252 xx:         180 xz:          72 wz:         104 wx:         360
Three-ext.   counts: yx :         320 yw:         395

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2
   ht   1    -2.77549162
   ht   2    -0.05790565    -0.19411268
calca4: root=   1 anorm**2=  0.06319157 scale:  1.03111181
calca4: root=   2 anorm**2=  0.93680843 scale:  1.39169265

          calcsovref: tciref block   1

              civs   1       civs   2
 refs   1    1.00000      -3.196956E-14

          calcsovref: scrb block   1

                ci   1         ci   2
 civs   1   0.967889      -0.251379    
 civs   2   0.825404        3.17806    

          calcsovref: sovref block   1

              v      1       v      2
 ref    1   0.967889      -0.251379    

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.96788865    -0.25137934
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  2  1    -46.3234605324  4.9381E-02  2.5791E-03  4.6094E-02  1.0000E-03
 mraqcc  #  2  2    -45.5420073459  2.0434E+00  0.0000E+00  4.9951E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   3

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         602 2x:         246 4x:         106
All internal counts: zz :         271 yy:         620 xx:          66 ww:         132
One-external counts: yz :        1072 yx:        1000 yw:        1210
Two-external counts: yy :         865 ww:         252 xx:         180 xz:          72 wz:         104 wx:         360
Three-ext.   counts: yx :         320 yw:         395

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3
   ht   1    -2.77549162
   ht   2    -0.05790565    -0.19411268
   ht   3     0.00693460    -0.00268932    -0.00929740
calca4: root=   1 anorm**2=  0.07359428 scale:  1.03614395
calca4: root=   2 anorm**2=  0.93427771 scale:  1.39078313
calca4: root=   3 anorm**2=  0.95207134 scale:  1.39716547

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3
 refs   1    1.00000      -3.196956E-14  -2.500305E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3
 civs   1   0.964999      -0.157208       0.213423    
 civs   2   0.865208        1.57941       -2.74564    
 civs   3    1.03731        13.2375        7.84752    

          calcsovref: sovref block   1

              v      1       v      2       v      3
 ref    1   0.962406      -0.190306       0.193801    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.96240560    -0.19030617     0.19380149
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  3  1    -46.3261421245  2.6816E-03  4.0980E-04  1.7831E-02  1.0000E-03
 mraqcc  #  3  2    -45.7931547164  2.5115E-01  0.0000E+00  4.1322E-01  1.0000E-04
 mraqcc  #  3  3    -45.4539164460  1.9553E+00  0.0000E+00  4.9782E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   4

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         602 2x:         246 4x:         106
All internal counts: zz :         271 yy:         620 xx:          66 ww:         132
One-external counts: yz :        1072 yx:        1000 yw:        1210
Two-external counts: yy :         865 ww:         252 xx:         180 xz:          72 wz:         104 wx:         360
Three-ext.   counts: yx :         320 yw:         395

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4
   ht   1    -2.77549162
   ht   2    -0.05790565    -0.19411268
   ht   3     0.00693460    -0.00268932    -0.00929740
   ht   4    -0.00056377    -0.00359118    -0.00052043    -0.00155028
calca4: root=   1 anorm**2=  0.07822510 scale:  1.03837619
calca4: root=   2 anorm**2=  0.94823287 scale:  1.39579113
calca4: root=   3 anorm**2=  0.95519780 scale:  1.39828388
calca4: root=   4 anorm**2=  0.76701123 scale:  1.32928975

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4
 refs   1    1.00000      -3.196956E-14  -2.500305E-03   2.263818E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4
 civs   1  -0.962795       0.157044       0.220492       3.638058E-02
 civs   2  -0.869938       -1.02855       -2.88680      -0.939052    
 civs   3   -1.17350       -11.0359        6.81615       -8.27233    
 civs   4  -0.866283       -19.8148        3.56055        31.3396    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4
 ref    1  -0.960057       0.180151       0.204256       6.415865E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1    -0.96005671     0.18015085     0.20425583     0.06415865
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  4  1    -46.3264972451  3.5512E-04  7.8388E-05  8.2484E-03  1.0000E-03
 mraqcc  #  4  2    -45.9502681692  1.5711E-01  0.0000E+00  2.4546E-01  1.0000E-04
 mraqcc  #  4  3    -45.4547237406  8.0729E-04  0.0000E+00  5.0153E-01  1.0000E-04
 mraqcc  #  4  4    -45.3943748947  1.8958E+00  0.0000E+00  5.5008E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   5

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         602 2x:         246 4x:         106
All internal counts: zz :         271 yy:         620 xx:          66 ww:         132
One-external counts: yz :        1072 yx:        1000 yw:        1210
Two-external counts: yy :         865 ww:         252 xx:         180 xz:          72 wz:         104 wx:         360
Three-ext.   counts: yx :         320 yw:         395

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1    -2.77549162
   ht   2    -0.05790565    -0.19411268
   ht   3     0.00693460    -0.00268932    -0.00929740
   ht   4    -0.00056377    -0.00359118    -0.00052043    -0.00155028
   ht   5    -0.00436416     0.00079828     0.00043960    -0.00001402    -0.00029807
calca4: root=   1 anorm**2=  0.07981336 scale:  1.03914068
calca4: root=   2 anorm**2=  0.93918757 scale:  1.39254715
calca4: root=   3 anorm**2=  0.87537371 scale:  1.36944285
calca4: root=   4 anorm**2=  0.97008552 scale:  1.40359735
calca4: root=   5 anorm**2=  0.51146238 scale:  1.22941546

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1    1.00000      -3.196956E-14  -2.500305E-03   2.263818E-04   1.566570E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 civs   1  -0.963217       0.105910       0.279319      -9.368917E-03  -1.927772E-02
 civs   2  -0.870597      -0.770148       -2.93537      -0.628289      -0.892314    
 civs   3   -1.19771       -9.16256       0.300776        12.5040       -2.50966    
 civs   4   -1.02123       -20.8380        6.91711       -12.4561        27.8531    
 civs   5   0.811267        30.5847       -43.0692        53.8438        41.9368    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1  -0.959183       0.172015       0.212662       4.089741E-02   5.899955E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1    -0.95918299     0.17201500     0.21266238     0.04089741     0.05899955
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  5  1    -46.3265608423  6.3597E-05  7.6720E-06  2.5212E-03  1.0000E-03
 mraqcc  #  5  2    -46.0252505084  7.4982E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  5  3    -45.4694446927  1.4721E-02  0.0000E+00  5.3362E-01  1.0000E-04
 mraqcc  #  5  4    -45.4269514233  3.2577E-02  0.0000E+00  5.3916E-01  1.0000E-04
 mraqcc  #  5  5    -45.3791069121  1.8805E+00  0.0000E+00  6.1826E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   6

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         602 2x:         246 4x:         106
All internal counts: zz :         271 yy:         620 xx:          66 ww:         132
One-external counts: yz :        1072 yx:        1000 yw:        1210
Two-external counts: yy :         865 ww:         252 xx:         180 xz:          72 wz:         104 wx:         360
Three-ext.   counts: yx :         320 yw:         395

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1    -2.77549162
   ht   2    -0.05790565    -0.19411268
   ht   3     0.00693460    -0.00268932    -0.00929740
   ht   4    -0.00056377    -0.00359118    -0.00052043    -0.00155028
   ht   5    -0.00436416     0.00079828     0.00043960    -0.00001402    -0.00029807
   ht   6     0.00105702    -0.00014952     0.00003997     0.00002061     0.00000983    -0.00003147
calca4: root=   1 anorm**2=  0.07998321 scale:  1.03922241
calca4: root=   2 anorm**2=  0.96132278 scale:  1.40047234
calca4: root=   3 anorm**2=  0.85951027 scale:  1.36363861
calca4: root=   4 anorm**2=  0.91635957 scale:  1.38432639
calca4: root=   5 anorm**2=  0.78634795 scale:  1.33654328
calca4: root=   6 anorm**2=  0.58451300 scale:  1.25877441

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1    1.00000      -3.196956E-14  -2.500305E-03   2.263818E-04   1.566570E-03  -3.797159E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1  -0.962929       0.113034      -4.866733E-02   0.214898      -0.190009       1.533910E-02
 civs   2  -0.870639      -0.657331        1.12577       -2.47903        1.11796       -1.15376    
 civs   3   -1.20042       -8.44388        4.26399        9.06786        8.33367       -2.72658    
 civs   4   -1.03873       -20.2424        2.37451       -3.68652       -11.7202        29.1809    
 civs   5   0.902968        34.6863        21.5464        2.58370        68.0835        35.9090    
 civs   6   0.937194        64.6580        241.111        64.4341       -75.4715        23.5233    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.959104       0.159351      -0.116591       0.170972      -7.818327E-02   7.608411E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.95910429     0.15935087    -0.11659064     0.17097214    -0.07818327     0.07608411

 trial vector basis is being transformed.  new dimension:   2
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  6  1    -46.3265680325  7.1902E-06  8.1026E-07  8.3855E-04  1.0000E-03
 mraqcc  #  6  2    -46.0454031543  2.0153E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  6  3    -45.7195530874  2.5011E-01  0.0000E+00  4.5554E-01  1.0000E-04
 mraqcc  #  6  4    -45.4351946957  8.2433E-03  0.0000E+00  5.0865E-01  1.0000E-04
 mraqcc  #  6  5    -45.4187893886  3.9682E-02  0.0000E+00  6.0734E-01  1.0000E-04
 mraqcc  #  6  6    -45.3777046004  1.8791E+00  0.0000E+00  5.9841E-01  1.0000E-04
 
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mraqcc   convergence criteria satisfied after  6 iterations.

 final mraqcc   convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  6  1    -46.3265680325  7.1902E-06  8.1026E-07  8.3855E-04  1.0000E-03
 mraqcc  #  6  2    -46.0454031543  2.0153E-02  0.0000E+00  0.0000E+00  1.0000E-04

####################CIUDGINFO####################

   aqcc(lrt) vector at position   1 energy=  -46.326568032544

################END OF CIUDGINFO################

 
 a4den factor for root 1  =  1.013510640
diagon:itrnv=   0
    1 of the   3 expansion vectors are transformed.
    1 of the   2 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
maximum overlap with reference    1(overlap= 0.95910)
reference energy =                         -46.2740792781
total aqcc energy =                        -46.3265680325
diagonal element shift (lrtshift) =         -0.0524887545

 information on vector: 1 from unit 11 written to unit 48 filename civout              


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -46.3265680325

                                                       internal orbitals

                                          level       1    2    3    4    5    6

                                          orbital     3    4    5   17   22   31

                                         symmetry   a1   a1   a1   a2   b1   b2 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1 -0.208322                        +-   +               -      
 z*  1  1       2 -0.660834                        +-        +          -      
 z*  1  1       3  0.600506                        +-             +          - 
 z*  1  1       4 -0.054471                        +    +-              -      
 z*  1  1       5  0.042873                        +     -   +          -      
 z*  1  1       6 -0.038898                        +     -        +          - 
 z*  1  1       7  0.074113                        +    +     -         -      
 z*  1  1       8  0.123391                        +    +          -         - 
 z*  1  1       9  0.118284                        +         +-         -      
 z*  1  1      10 -0.123250                        +          -   +          - 
 z*  1  1      11 -0.038899                        +         +     -         - 
 z*  1  1      12  0.084659                        +              +-    -      
 z*  1  1      13 -0.084649                        +                    -   +- 
 z*  1  1      14  0.043387                             +-   +          -      
 z*  1  1      15 -0.047673                             +-        +          - 
 z*  1  1      16 -0.013690                             +    +-         -      
 z*  1  1      17  0.020958                             +     -   +          - 
 z*  1  1      18  0.018294                             +    +     -         - 
 z*  1  1      20  0.019268                             +               -   +- 
 z*  1  1      22 -0.025892                                  +    +-    -      
 z*  1  1      23  0.018136                                  +          -   +- 
 z*  1  1      24  0.026483                                       +    +-    - 
 y   1  1      33  0.019098              2( b1 )   +-         -                
 y   1  1      35 -0.014797              4( b1 )   +-         -                
 y   1  1      40 -0.017359              2( b2 )   +-              -           
 y   1  1      42  0.013444              4( b2 )   +-              -           
 y   1  1      47  0.011534              2( a1 )   +-                   -      
 y   1  1      48  0.016326              3( a1 )   +-                   -      
 y   1  1      50 -0.059697              5( a1 )   +-                   -      
 y   1  1      52 -0.013609              7( a1 )   +-                   -      
 y   1  1      57 -0.017363              1( a2 )   +-                        - 
 y   1  1      58  0.013447              2( a2 )   +-                        - 
 y   1  1      92  0.014046              1( a2 )    -   +                    - 
 y   1  1      93 -0.013859              2( a2 )    -   +                    - 
 y   1  1      96 -0.011304              2( b1 )    -        +-                
 y   1  1      98  0.014081              4( b1 )    -        +-                
 y   1  1     103  0.010591              2( b2 )    -        +     -           
 y   1  1     105 -0.010454              4( b2 )    -        +     -           
 y   1  1     113  0.022352              5( a1 )    -        +          -      
 y   1  1     126  0.010394              4( b1 )    -             +-           
 y   1  1     135 -0.011717              3( a1 )    -             +          - 
 y   1  1     137 -0.020324              5( a1 )    -             +          - 
 y   1  1     139  0.012455              7( a1 )    -             +          - 
 y   1  1     161  0.010392              4( b1 )    -                       +- 
 y   1  1     202 -0.010052              3( a1 )   +          -         -      
 y   1  1     206  0.015571              7( a1 )   +          -         -      
 y   1  1     212 -0.012190              2( a2 )   +          -              - 
 y   1  1     215  0.011022              2( a2 )   +               -    -      
 y   1  1     222  0.010976              6( a1 )   +               -         - 
 y   1  1     231 -0.011018              4( b2 )   +                    -    - 
 y   1  1     282 -0.012709              6( a1 )         -   +          -      
 y   1  1     302  0.010682              2( a1 )         -        +          - 
 y   1  1     306  0.013807              6( a1 )         -        +          - 
 y   1  1     439 -0.010008              2( a2 )             +     -    -      
 x   1  1     621 -0.012474    9( a1 )   1( b1 )    -         -                
 x   1  1     628  0.016230    5( a1 )   2( b1 )    -         -                
 x   1  1     643 -0.012927    9( a1 )   3( b1 )    -         -                
 x   1  1     650 -0.016283    5( a1 )   4( b1 )    -         -                
 x   1  1     668 -0.012974    1( a1 )   6( b1 )    -         -                
 x   1  1     671 -0.013708    4( a1 )   6( b1 )    -         -                
 x   1  1     741 -0.010861   10( a1 )   1( b2 )    -              -           
 x   1  1     747 -0.014753    5( a1 )   2( b2 )    -              -           
 x   1  1     763 -0.010976   10( a1 )   3( b2 )    -              -           
 x   1  1     769  0.014802    5( a1 )   4( b2 )    -              -           
 x   1  1     787  0.012952    1( a1 )   6( b2 )    -              -           
 x   1  1     790  0.013347    4( a1 )   6( b2 )    -              -           
 x   1  1     817 -0.013892    3( a1 )   5( a1 )    -                   -      
 x   1  1     828 -0.015003    5( a1 )   7( a1 )    -                   -      
 x   1  1     845 -0.010338    1( a1 )  10( a1 )    -                   -      
 x   1  1     848 -0.010896    4( a1 )  10( a1 )    -                   -      
 x   1  1     882  0.011830    1( b1 )   7( b1 )    -                   -      
 x   1  1     884  0.012215    3( b1 )   7( b1 )    -                   -      
 x   1  1     903  0.012577    1( b2 )   7( b2 )    -                   -      
 x   1  1     905  0.012969    3( b2 )   7( b2 )    -                   -      
 x   1  1     913 -0.014754    5( a1 )   1( a2 )    -                        - 
 x   1  1     924  0.014803    5( a1 )   2( a2 )    -                        - 
 x   1  1     948  0.011885    7( b1 )   1( b2 )    -                        - 
 x   1  1     962  0.012233    7( b1 )   3( b2 )    -                        - 
 x   1  1     984  0.011211    1( b1 )   7( b2 )    -                        - 
 x   1  1     986  0.011558    3( b1 )   7( b2 )    -                        - 
 x   1  1     991 -0.011495    1( a1 )   1( b1 )         -    -                
 x   1  1    1320 -0.011203    1( b1 )   1( b2 )         -                   - 
 x   1  1    1682 -0.010004    1( b1 )   1( b2 )                   -    -      
 x   1  1    1852  0.010005    1( a1 )   1( b2 )                        -    - 
 w   1  1    1929 -0.028393    1( a1 )   1( b1 )   +-                          
 w   1  1    1932 -0.026193    4( a1 )   1( b1 )   +-                          
 w   1  1    1936 -0.010503    8( a1 )   1( b1 )   +-                          
 w   1  1    1946 -0.012755    7( a1 )   2( b1 )   +-                          
 w   1  1    1951 -0.026192    1( a1 )   3( b1 )   +-                          
 w   1  1    1954 -0.025428    4( a1 )   3( b1 )   +-                          
 w   1  1    1958 -0.011900    8( a1 )   3( b1 )   +-                          
 w   1  1    1964 -0.011851    3( a1 )   4( b1 )   +-                          
 w   1  1    1967 -0.011760    6( a1 )   4( b1 )   +-                          
 w   1  1    1968  0.021653    7( a1 )   4( b1 )   +-                          
 w   1  1    1973 -0.010502    1( a1 )   5( b1 )   +-                          
 w   1  1    1976 -0.011900    4( a1 )   5( b1 )   +-                          
 w   1  1    1993  0.016504   10( a1 )   6( b1 )   +-                          
 w   1  1    2004  0.020695   10( a1 )   7( b1 )   +-                          
 w   1  1    2010  0.012586    2( a2 )   2( b2 )   +-                          
 w   1  1    2015  0.012586    1( a2 )   4( b2 )   +-                          
 w   1  1    2016 -0.021366    2( a2 )   4( b2 )   +-                          
 w   1  1    2023 -0.013919    3( a2 )   6( b2 )   +-                          
 w   1  1    2026 -0.022012    3( a2 )   7( b2 )   +-                          
 w   1  1    2027 -0.010748    1( a1 )   1( b1 )   +     -                     
 w   1  1    2125 -0.034127    1( a1 )   1( b1 )   +          -                
 w   1  1    2128 -0.025213    4( a1 )   1( b1 )   +          -                
 w   1  1    2147 -0.025599    1( a1 )   3( b1 )   +          -                
 w   1  1    2150 -0.020424    4( a1 )   3( b1 )   +          -                
 w   1  1    2183  0.010699    4( a1 )   6( b1 )   +          -                
 w   1  1    2244  0.031005    1( a1 )   1( b2 )   +               -           
 w   1  1    2247  0.023402    4( a1 )   1( b2 )   +               -           
 w   1  1    2266  0.022763    1( a1 )   3( b2 )   +               -           
 w   1  1    2269  0.018557    4( a1 )   3( b2 )   +               -           
 w   1  1    2321  0.023499    1( a1 )   1( a1 )   +                    -      
 w   1  1    2327  0.021147    1( a1 )   4( a1 )   +                    -      
 w   1  1    2330  0.010725    4( a1 )   4( a1 )   +                    -      
 w   1  1    2393  0.023496    1( b1 )   1( b1 )   +                    -      
 w   1  1    2396  0.021139    1( b1 )   3( b1 )   +                    -      
 w   1  1    2398  0.010720    3( b1 )   3( b1 )   +                    -      
 w   1  1    2421 -0.020185    1( b2 )   1( b2 )   +                    -      
 w   1  1    2424 -0.024885    1( b2 )   3( b2 )   +                    -      
 w   1  1    2426 -0.015445    3( b2 )   3( b2 )   +                    -      
 w   1  1    2482  0.031017    1( b1 )   1( b2 )   +                         - 
 w   1  1    2484  0.023411    3( b1 )   1( b2 )   +                         - 
 w   1  1    2496  0.022772    1( b1 )   3( b2 )   +                         - 
 w   1  1    2498  0.018562    3( b1 )   3( b2 )   +                         - 
 w   1  1    2825  0.020993    1( a1 )   1( a1 )        +               -      
 w   1  1    2831  0.018479    1( a1 )   4( a1 )        +               -      
 w   1  1    2897  0.014477    1( b1 )   1( b1 )        +               -      
 w   1  1    2900  0.012187    1( b1 )   3( b1 )        +               -      
 w   1  1    2925  0.013579    1( b2 )   1( b2 )        +               -      
 w   1  1    2928  0.011666    1( b2 )   3( b2 )        +               -      
 w   1  1    2986 -0.013670    1( b1 )   1( b2 )        +                    - 
 w   1  1    3035  0.015792    1( a1 )   1( b1 )             +-                
 w   1  1    3038  0.010947    4( a1 )   1( b1 )             +-                
 w   1  1    3057  0.010604    1( a1 )   3( b1 )             +-                
 w   1  1    3154 -0.010304    1( a1 )   1( b2 )             +     -           
 w   1  1    3231  0.055229    1( a1 )   1( a1 )             +          -      
 w   1  1    3237  0.047665    1( a1 )   4( a1 )             +          -      
 w   1  1    3240  0.024220    4( a1 )   4( a1 )             +          -      
 w   1  1    3245  0.016276    5( a1 )   5( a1 )             +          -      
 w   1  1    3303  0.057289    1( b1 )   1( b1 )             +          -      
 w   1  1    3306  0.049653    1( b1 )   3( b1 )             +          -      
 w   1  1    3308  0.025248    3( b1 )   3( b1 )             +          -      
 w   1  1    3331  0.043068    1( b2 )   1( b2 )             +          -      
 w   1  1    3334  0.037013    1( b2 )   3( b2 )             +          -      
 w   1  1    3336  0.019017    3( b2 )   3( b2 )             +          -      
 w   1  1    3441  0.012160    1( a1 )   1( b1 )                  +-           
 w   1  1    3621 -0.052822    1( a1 )   1( a1 )                  +          - 
 w   1  1    3627 -0.045854    1( a1 )   4( a1 )                  +          - 
 w   1  1    3630 -0.023326    4( a1 )   4( a1 )                  +          - 
 w   1  1    3635 -0.014795    5( a1 )   5( a1 )                  +          - 
 w   1  1    3693 -0.052829    1( b1 )   1( b1 )                  +          - 
 w   1  1    3696 -0.045859    1( b1 )   3( b1 )                  +          - 
 w   1  1    3698 -0.023327    3( b1 )   3( b1 )                  +          - 
 w   1  1    3721 -0.035767    1( b2 )   1( b2 )                  +          - 
 w   1  1    3724 -0.030382    1( b2 )   3( b2 )                  +          - 
 w   1  1    3726 -0.015603    3( b2 )   3( b2 )                  +          - 
 w   1  1    3945  0.012152    1( a1 )   1( b1 )                            +- 

 ci coefficient statistics:
           rq > 0.1                6
      0.1> rq > 0.01             153
     0.01> rq > 0.001            885
    0.001> rq > 0.0001           737
   0.0001> rq > 0.00001          188
  0.00001> rq > 0.000001          29
 0.000001> rq                   2044
           all                  4042
  iref  icsf         v(icsf)             hv(icsf)
     1     1     -0.208321513403
     2     2     -0.660834321314
     3     3      0.600505885186
     4     4     -0.054470729014
     5     5      0.042872842172
     6     6     -0.038898382001
     7     7      0.074112937801
     8     8      0.123390760474
     9     9      0.118283777550
    10    10     -0.123249528417
    11    11     -0.038899336692
    12    12      0.084658712313
    13    13     -0.084648682208
    14    14      0.043387372167
    15    15     -0.047673181463
    16    16     -0.013689918308
    17    17      0.020958044612
    18    18      0.018294375422
    19    19      0.005398374203
    20    20      0.019268013907
    21    21     -0.005349440812
    22    22     -0.025892094850
    23    23      0.018135749328
    24    24      0.026483454743

 number of reference csfs (nref) is    24.  root number (iroot) is  1.
 c0**2 =   0.92001679  c0**2(renormalized) =   0.99360269
 c**2 (all zwalks) =   0.92001679  c**2(all zwalks, renormalized) =   0.99360269
 passed aftci ... 
 readint2: molcas,dalton2=                     0                     0
 files%faoints=aoints              
lodens (list->root)=  1
sifcfg setup: record length 4096 DP
# d1 elements per record  3272
# d2 elements per record  2730
  The AQCC density will be calculated.
 item #                     1 suffix=:.drt1.state1:
 read_civout: repnuc=  -43.4985876562748     
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method: 30 last record  1max overlap with ref# 96% root-following 0
 MR-CISD energy:   -46.32656803    -2.82798038
 residuum:     0.00083855
 deltae:     0.00000719
 apxde:     0.00000081
 a4den:     1.01351064
 reference energy:   -46.27407928    -2.77549162

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.95910429     0.15935087    -0.11659064     0.17097214    -0.07818327     0.07608411     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.95910429     0.15935087     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 rdcirefv: extracting reference vector # 1(blocksize=    24 #zcsf=      24)
 =========== Executing IN-CORE method ==========
================================================================================
   DYZ=       0  DYX=       0  DYW=       0
   D0Z=      34  D0Y=       0  D0X=       0  D0W=       0
  DDZI=      80 DDYI=       0 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
 root #                      1 : Scaling(1) with    1.01351063973760      
 corresponding to ref #                      1
 computing final density
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=     106  DYX=     130  DYW=     160
   D0Z=      34  D0Y=     103  D0X=      12  D0W=      18
  DDZI=      80 DDYI=     180 DDXI=      30 DDWI=      36
  DDZE=       0 DDYE=      70 DDXE=      15 DDWE=      21
================================================================================
Trace of MO density:     4.000000
    4  correlated and     8  frozen core electrons

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     1.80557364    -0.06195299    -0.19607138     0.00000000    -0.01301148    -0.01842883
   MO   4     0.00000000     0.00000000    -0.06195299     0.09079072     0.15080893     0.00000000    -0.00002989     0.00705418
   MO   5     0.00000000     0.00000000    -0.19607138     0.15080893     0.52160537     0.00000000     0.01156382     0.01412913
   MO   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.02760745     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000    -0.01301148    -0.00002989     0.01156382     0.00000000     0.00105737     0.00052575
   MO   8     0.00000000     0.00000000    -0.01842883     0.00705418     0.01412913     0.00000000     0.00052575     0.00143021
   MO   9     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.01926488     0.00000000     0.00000000
   MO  10     0.00000000     0.00000000    -0.04074236    -0.01072646    -0.03403390     0.00000000    -0.00078615    -0.00111335
   MO  11     0.00000000     0.00000000    -0.00863622    -0.00221021     0.00880745     0.00000000     0.00110314     0.00032587
   MO  12     0.00000000     0.00000000     0.01590449    -0.00674513    -0.01279969     0.00000000    -0.00064286    -0.00158059
   MO  13     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00415688     0.00000000     0.00000000
   MO  14     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00105068     0.00000000     0.00000000
   MO  15     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00212185     0.00000000     0.00000000
   MO  16     0.00000000     0.00000000     0.00037487     0.00047917     0.00152006     0.00000000     0.00004188     0.00005931

                MO   9         MO  10         MO  11         MO  12         MO  13         MO  14         MO  15         MO  16
   MO   3     0.00000000    -0.04074236    -0.00863622     0.01590449     0.00000000     0.00000000     0.00000000     0.00037487
   MO   4     0.00000000    -0.01072646    -0.00221021    -0.00674513     0.00000000     0.00000000     0.00000000     0.00047917
   MO   5     0.00000000    -0.03403390     0.00880745    -0.01279969     0.00000000     0.00000000     0.00000000     0.00152006
   MO   6     0.01926488     0.00000000     0.00000000     0.00000000     0.00415688     0.00105068     0.00212185     0.00000000
   MO   7     0.00000000    -0.00078615     0.00110314    -0.00064286     0.00000000     0.00000000     0.00000000     0.00004188
   MO   8     0.00000000    -0.00111335     0.00032587    -0.00158059     0.00000000     0.00000000     0.00000000     0.00005931
   MO   9     0.01388652     0.00000000     0.00000000     0.00000000     0.00333670     0.00098050     0.00157412     0.00000000
   MO  10     0.00000000     0.00886159    -0.00054938     0.00101184     0.00000000     0.00000000     0.00000000    -0.00039091
   MO  11     0.00000000    -0.00054938     0.00127885    -0.00051727     0.00000000     0.00000000     0.00000000     0.00003223
   MO  12     0.00000000     0.00101184    -0.00051727     0.00194956     0.00000000     0.00000000     0.00000000    -0.00005935
   MO  13     0.00333670     0.00000000     0.00000000     0.00000000     0.00109236     0.00039748     0.00050506     0.00000000
   MO  14     0.00098050     0.00000000     0.00000000     0.00000000     0.00039748     0.00099871     0.00009909     0.00000000
   MO  15     0.00157412     0.00000000     0.00000000     0.00000000     0.00050506     0.00009909     0.00214432     0.00000000
   MO  16     0.00000000    -0.00039091     0.00003223    -0.00005935     0.00000000     0.00000000     0.00000000     0.00002246

Natural orbital populations,block 1
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     2.00000000     1.84010041     0.53989785     0.04436426     0.04212356     0.00519466     0.00238159
              MO     9       MO    10       MO    11       MO    12       MO    13       MO    14       MO    15       MO    16
  occ(*)=     0.00198636     0.00111946     0.00055118     0.00048565     0.00006232     0.00001451     0.00001434     0.00000300

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4
   MO   1     0.43829653     0.01544310    -0.01353441     0.00000000
   MO   2     0.01544310     0.00152795    -0.00162530     0.00000000
   MO   3    -0.01353441    -0.00162530     0.00192601     0.00000000
   MO   4     0.00000000     0.00000000     0.00000000     0.00210863

Natural orbital populations,block 2
              MO     1       MO     2       MO     3       MO     4       MO
  occ(*)=     0.43926377     0.00241873     0.00210863     0.00006798

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     0.55704446     0.00000000     0.01841346     0.00000000    -0.01578323     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.02761257     0.00000000     0.01926790     0.00000000     0.00415683    -0.00203171
   MO   4     0.00000000     0.01841346     0.00000000     0.00188405     0.00000000    -0.00196803     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.01926790     0.00000000     0.01388824     0.00000000     0.00333660    -0.00167641
   MO   6     0.00000000    -0.01578323     0.00000000    -0.00196803     0.00000000     0.00236359     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000     0.00415683     0.00000000     0.00333660     0.00000000     0.00109230    -0.00060773
   MO   8     0.00000000     0.00000000    -0.00203171     0.00000000    -0.00167641     0.00000000    -0.00060773     0.00142437
   MO   9     0.00000000     0.00000000    -0.00121529     0.00000000    -0.00079274     0.00000000    -0.00020940     0.00056255

                MO   9
   MO   2     0.00000000
   MO   3    -0.00121529
   MO   4     0.00000000
   MO   5    -0.00079274
   MO   6     0.00000000
   MO   7    -0.00020940
   MO   8     0.00056255
   MO   9     0.00171844

Natural orbital populations,block 3
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     0.55810599     0.04213017     0.00307163     0.00198649     0.00111950     0.00048540     0.00011448
              MO     9       MO
  occ(*)=     0.00001435

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     0.43829628     0.00000000     0.01543815     0.00000000    -0.01353169     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.01650767     0.00000000     0.01178124     0.00000000    -0.00249673    -0.00074954
   MO   4     0.00000000     0.01543815     0.00000000     0.00152703     0.00000000    -0.00162471     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.01178124     0.00000000     0.00881910     0.00000000    -0.00214259    -0.00047650
   MO   6     0.00000000    -0.01353169     0.00000000    -0.00162471     0.00000000     0.00192569     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000    -0.00249673     0.00000000    -0.00214259     0.00000000     0.00072255     0.00012031
   MO   8     0.00000000     0.00000000    -0.00074954     0.00000000    -0.00047650     0.00000000     0.00012031     0.00125327
   MO   9     0.00000000     0.00000000    -0.00118587     0.00000000    -0.00075384     0.00000000     0.00019033     0.00053613

                MO   9
   MO   2     0.00000000
   MO   3    -0.00118587
   MO   4     0.00000000
   MO   5    -0.00075384
   MO   6     0.00000000
   MO   7     0.00019033
   MO   8     0.00053613
   MO   9     0.00176212

Natural orbital populations,block 4
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     0.43926300     0.02560941     0.00241810     0.00198827     0.00091425     0.00054208     0.00006791
              MO     9       MO
  occ(*)=     0.00001069


 total number of electrons =   12.0000000000

 test slabel:                     4
  a1  a2  b1  b2 


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        a1  partial gross atomic populations
   ao class       1a1        2a1        3a1        4a1        5a1        6a1 
     3_ s       0.000000   2.000000   1.792721   0.016821   0.000000   0.000000
     3_ p       2.000000   0.000000   0.000000   0.000000   0.000000   0.041881
     3_ d       0.000000   0.000000   0.047379   0.523077   0.044364   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000000   0.000000   0.000243
 
   ao class       7a1        8a1        9a1       10a1       11a1       12a1 
     3_ s       0.004706   0.000207   0.000000   0.000000   0.000000   0.000000
     3_ p       0.000000   0.000000   0.000044   0.000299   0.000000   0.000349
     3_ d       0.000489   0.002175   0.000000   0.000000   0.000551   0.000000
     3_ f       0.000000   0.000000   0.001943   0.000821   0.000000   0.000137
 
   ao class      13a1       14a1       15a1       16a1 
     3_ s       0.000001   0.000000   0.000000   0.000003
     3_ p       0.000000   0.000000   0.000014   0.000000
     3_ d       0.000061   0.000015   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000000

                        a2  partial gross atomic populations
   ao class       1a2        2a2        3a2        4a2 
     3_ d       0.439264   0.002419   0.000000   0.000068
     3_ f       0.000000   0.000000   0.002109   0.000000

                        b1  partial gross atomic populations
   ao class       1b1        2b1        3b1        4b1        5b1        6b1 
     3_ p       2.000000   0.000000   0.041887   0.000000   0.000044   0.000299
     3_ d       0.000000   0.558106   0.000000   0.003072   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000243   0.000000   0.001943   0.000820
 
   ao class       7b1        8b1        9b1 
     3_ p       0.000348   0.000000   0.000014
     3_ d       0.000000   0.000114   0.000000
     3_ f       0.000137   0.000000   0.000000

                        b2  partial gross atomic populations
   ao class       1b2        2b2        3b2        4b2        5b2        6b2 
     3_ p       2.000000   0.000000   0.025480   0.000000   0.000018   0.000000
     3_ d       0.000000   0.439263   0.000000   0.002418   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000129   0.000000   0.001971   0.000914
 
   ao class       7b2        8b2        9b2 
     3_ p       0.000541   0.000000   0.000011
     3_ d       0.000000   0.000068   0.000000
     3_ f       0.000001   0.000000   0.000000


                        gross atomic populations
     ao            3_
      s         3.814458
      p         6.111229
      d         2.062904
      f         0.011410
    total      12.000000
 

 Total number of electrons:   12.00000000

 DA ...
