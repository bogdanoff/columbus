1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      2009-03.    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


================ Computing sorting integral file structure ================

                    -----z----- -----y----- -----x----- -----w----- ---total---

                CSFs         4         156        1430           0        1590
      internal walks        15          20          15           0          50
valid internal walks         4          20          15           0          39
 getinfoarray: info=                     6 :                     1
                  8192                  6552                  8192
                  5460                     0
 icd(3)=                  2976 ci%nnlev=                   595  l2rec=
                  8192  n2max=                  5460
 lcore1,lcore2=              13068137              13044821
 lencor,maxblo              13107200                 60000
========================================
 current settings:
 minbl3         381
 minbl4         381
 locmaxbl3     4288
 locmaxbuf     2144
 maxbl3       60000
 maxbl3       60000
 maxbl4       60000
 maxbuf       30006
========================================

 sorted 4-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)

 sorted 3-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)
 Orig.  diagonal integrals:  1electron:        34
                             0ext.    :        42
                             2ext.    :       336
                             4ext.    :       812


 Orig. off-diag. integrals:  4ext.    :     24411
                             3ext.    :     18720
                             2ext.    :      6792
                             1ext.    :      1272
                             0ext.    :        99
                             2ext. SO :         0
                             1ext. SO :         0
                             0ext. SO :         0
                             1electron:       128


 Sorted integrals            3ext.  w :     17370 x :     16020
                             4ext.  w :     20955 x :     17777


Cycle #  1 sortfile size=    163835(       5 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13068137
Cycle #  2 sortfile size=    163835(       5 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13068137
 minimum size of srtscr:     98301 WP (     3 records)
 maximum size of srtscr:    163835 WP (     5 records)
diagi   file:      4 records  of   1536 WP each=>       6144 WP total
ofdgi   file:      8 records  of   1536 WP each=>      12288 WP total
fil3w   file:      1 records  of  30006 WP each=>      30006 WP total
fil3x   file:      1 records  of  30006 WP each=>      30006 WP total
fil4w   file:      1 records  of  30006 WP each=>      30006 WP total
fil4x   file:      1 records  of  30006 WP each=>      30006 WP total
 compressed index vector length=                    13
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 3,
  GSET = 3,
   DAVCOR =10,
  NCOREL = 4
   nroot=1
  IVMODE = 3
  NBKITR = 1
   nvbkmn=2
  RTOLBK = 1e-3
   niter=30
   nvcimn=2
   rtolci=1e-3
  NVCIMX = 6
  NVRFMX = 6
  NVBKMX = 6
  IDEN  = 1
  CSFPRN = 10,
  update_mode=1
  nvrfmn=2
   lrtshift=-0.0465482970
 /&end
 ------------------------------------------------------------------------
lodens (list->root)=  1
invlodens (root->list)=  1
 LRT calculation: diagonal shift= -4.654829700000000E-002
 bummer (warning):2:changed keyword: nbkitr=                      0
 USING SEGMENTS OF EQUAL SIZE

****************  list of control variables  ****************
 lvlprt =    0      nroot  =    1      noldv  =   0      noldhv =   0
 nunitv =    1      nbkitr =    0      niter  =  30      davcor =  10
 csfprn =   10      ivmode =    3      istrt  =   0      vout   =   0
 iortls =    0      nvbkmx =    6      ibktv  =  -1      ibkthv =  -1
 nvcimx =    6      icitv  =   -1      icithv =  -1      frcsub =   0
 nvbkmn =    2      nvcimn =    2      maxseg =   4      nrfitr =  30
 ncorel =    4      nvrfmx =    6      nvrfmn =   2      iden   =   1
 itran  =    0      froot  =    0      rtmode =   0      ncouple=   1
 skipso =    F      dalton2=    0      molcas =   0      finalv =   0
 finalw =    0      cosmocalc=   0    with_tsklst=   0
 nsegwx =    1     1     1     1
 nseg0x =    1     1     1     1
 nseg1x =    1     1     1     1
 nseg2x =    1     1     1     1
 nseg3x =    1     1     1     1
 nseg4x =    1     1     1     1
 no0ex  =      0    no1ex  =      0    no2ex  =     0    no3ex  =     0
 no4ex  =      0    nodiag =      0
 cdg4ex =    1      c3ex1ex=    1      c2ex0ex=   1
 fileloc=    0     0     0     0     0     0     0     1     1     1
 directhd=   1      noaqccshift_zyxw=      0
 critical_crit=-1.00000    critical_delta= 0.05000

 ctol   = 0.010000    lrtshift=-.046548    smalld =0.001000


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-03    1.000E-03
 Computing density:                    .drt1.state1
 using                      1  nodes and                      1  cores.
 szdg/szodg per processor=                  6144                 12288
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core           13107199 DP per process

********** Integral sort section *************


 workspace allocation information: lencor=  13107199

 echo of the input for program cisrt:
 ------------------------------------------------------------------------
  &input
  maxbl3=60000
  maxbl4=60000
  &end
 ------------------------------------------------------------------------
 
 ( 6) listing file:                    ciudgls             
 ( 5) input file:                      cisrtin   
 (17) cidrt file:                      cidrtfl             
 (11) transformed integrals file:      moints    
 (12) diagonal integral file:          diagint             
 (13) off-diagonal integral file:      ofdgint             
 (31) 4-external w integrals file:     fil4w               
 (32) 4-external x integrals file:     fil4x               
 (33) 3-external w integrals file:     fil3w               
 (34) 3-external x integrals file:     fil3x               
 (21) scratch da sorting file:         srtscr              
 (12) 2-e integral file [fsplit=2]:    moints2   

 input integral file header information:
                                                                                 
 aoints SIFS file created by argos.      zam792            17:56:38.564 17-Dec-13
  cidrt_title                                                                    
 MO-coefficients from mcscf.x                                                    
  with dummy occupation 1.0 for active orbitals                                  
  total ao core energy =    0.000000000                                          
 MCSCF energy =     -46.301878576                                                
 SIFS file created by program tran.      zam792            17:56:38.888 17-Dec-13

 input energy(*) values:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.

 total core energy =   0.000000000000E+00

 nsym = 4 nmot=  34

 symmetry  =    1    2    3    4
 slabel(*) =   a1   a2   b1   b2
 nmpsy(*)  =   14    4    8    8

 info(*) =          1      8192      6552      8192      5460         0

 orbital labels, i:molab(i)=
   1:tout:001   2:tout:002   3:tout:003   4:tout:004   5:tout:005   6:tout:006   7:tout:007   8:tout:008   9:tout:009  10:tout:010
  11:tout:011  12:tout:012  13:tout:013  14:tout:014  15:tout:015  16:tout:016  17:tout:017  18:tout:018  19:tout:019  20:tout:020
  21:tout:021  22:tout:022  23:tout:023  24:tout:024  25:tout:025  26:tout:026  27:tout:027  28:tout:028  29:tout:029  30:tout:030
  31:tout:031  32:tout:032  33:tout:033  34:tout:034

 input parameters:
 prnopt=  0
 ldamin=    4095 ldamax=   32767 ldainc=      64
 maxbuf=   30006 maxbl3=   60000 maxbl4=   60000 intmxo=    1536
  Using 32 bit compression 

 drt information:
  cidrt_title                                                                    
 nmotd =  38 nfctd =   4 nfvtc =   0 nmot  =  34
 nlevel =  34 niot  =   6 lowinl=  29
 orbital-to-level map(*)
   -1  -1  29  30  31   1   2   3   4   5   6   7   8   9  10  11  32  12  13  14
   -1  33  15  16  17  18  19  20  21  -1  34  22  23  24  25  26  27  28
 compressed map(*)
   29  30  31   1   2   3   4   5   6   7   8   9  10  11  32  12  13  14  33  15
   16  17  18  19  20  21  34  22  23  24  25  26  27  28
 levsym(*)
    1   1   1   1   1   1   1   1   1   1   1   2   2   2   3   3   3   3   3   3
    3   4   4   4   4   4   4   4   1   1   1   2   3   4
 repartitioning mu(*)=
   0.  0.  0.  0.  0.  0.

 new core energy added to the energy(*) list.
 from the integral file: h1_core= -4.349858765627E+01

 indxdg: diagonal integral statistics.
 total number of integrals contributing to diagonal matrix elements:      1190
 number with all external indices:       812
 number with half external - half internal indices:       336
 number with all internal indices:        42

 indxof: off-diagonal integral statistics.
    4-external integrals: num=      24411 strt=          1
    3-external integrals: num=      18720 strt=      24412
    2-external integrals: num=       6792 strt=      43132
    1-external integrals: num=       1272 strt=      49924
    0-external integrals: num=         99 strt=      51196

 total number of off-diagonal integrals:       51294


 indxof(2nd)  ittp=   3 numx(ittp)=        6792
 indxof(2nd)  ittp=   4 numx(ittp)=        1272
 indxof(2nd)  ittp=   5 numx(ittp)=          99

 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg=  12931980
 pro2e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652

 pro2e:     25126 integrals read in     5 records.

 pro2e:         0 integrals 34-ext integrals skipped.
 pro1e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652
 pro1e: eref =    0.000000000000000E+00
 total size of srtscr:                     4  records of                  32767 
 WP =               1048544 Bytes
 putdg        1     596    1191    1786    3322   36089   57934    3018   46706   90394
   123161  131353  136813  158652

 putf:       4 buffers of length    1536 written to file 12
 diagonal integral file completed.

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep4e:    17 blocks of linear combinations of 4-external integrals processed.
 number of sorted 4-external integrals      38732
 number of original 4-external integrals    24411


 putf34: external integral file complete. nfilw=    31 nfilx=    32 nrecw=     1 nrecx=     1 lbufp= 30006

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep3e:    19 blocks of linear combinations of 3-external integrals processed.
 number of sorted 3-external integrals      33390
 number of original 3-external integrals    18720


 putf34: external integral file complete. nfilw=    33 nfilx=    34 nrecw=     1 nrecx=     1 lbufp= 30006
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      6792         3     43132     43132      6792     51294
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      1272         4     49924     49924      1272     51294
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd        99         5     51196     51196        99     51294

 putf:       8 buffers of length    1536 written to file 13
 off-diagonal files sort completed.
 executing brd_struct for cisrtinfo
cisrtinfo:
bufszi  1536
 diagfile 4ext:     812 2ext:     336 0ext:      42
 fil4w,fil4x  :   24411 fil3w,fil3x :   18720
 ofdgint  2ext:    6792 1ext:    1272 0ext:      99so0ext:       0so1ext:       0so2ext:       0
buffer minbl4     381 minbl3     381 maxbl2     384nbas:  11   3   7   7   0   0   0   0 maxbuf 30006
 CIUDG version 5.9.7 ( 5-Oct-2004)

 workspace allocation information: lcore=  13107199

 core energy values from the integral file:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -4.349858765627E+01, ietype=    6,   fcore energy of type: H1(*)   

 total core repulsion energy = -4.349858765627E+01
 nmot  =    38 niot  =     6 nfct  =     4 nfvt  =     0
 nrow  =    23 nsym  =     4 ssym  =     2 lenbuf=  1600
 nwalk,xbar:         50       15       20       15        0
 nvalwt,nvalw:       39        4       20       15        0
 ncsft:            1590
 total number of valid internal walks:      39
 nvalz,nvaly,nvalx,nvalw =        4      20      15       0

 cisrt info file parameters:
 file number  12 blocksize   1536
 mxbld   1536
 nd4ext,nd2ext,nd0ext   812   336    42
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int    24411    18720     6792     1272       99        0        0        0
 minbl4,minbl3,maxbl2   381   381   384
 maxbuf 30006
 number of external orbitals per symmetry block:  11   3   7   7
 nmsym   4 number of internal orbitals   6
 executing brd_struct for drt
 executing brd_struct for orbinf
 executing brd_struct for momap
 calcthrxt: niot,maxw1=                     6                    11
 block size     0
 pthz,pthy,pthx,pthw:    15    20    15     0 total internal walks:      50
 maxlp3,n2lp,n1lp,n0lp    11     0     0     0
 orbsym(*)= 1 1 1 2 3 4

 setref:        4 references kept,
               11 references were marked as invalid, out of
               15 total.
 nmb.of records onel     1
 nmb.of records 2-ext     5
 nmb.of records 1-ext     1
 nmb.of records 0-ext     1
 nmb.of records 2-int     0
 nmb.of records 1-int     0
 nmb.of records 0-int     0
 ---------memory usage in DP -----------------
 < n-ex core usage >
     routines:
    fourex            61797
    threx             60079
    twoex              3549
    onex               1823
    allin              1536
    diagon             2109
               =======
   maximum            61797
 
  __ static summary __ 
   reflst                 4
   hrfspc                 4
               -------
   static->               8
 
  __ core required  __ 
   totstc                 8
   max n-ex           61797
               -------
   totnec->           61805
 
  __ core available __ 
   totspc          13107199
   totnec -           61805
               -------
   totvec->        13045394

 number of external paths / symmetry
 vertex x     100      82      98      98
 vertex w     128      82      98      98
segment: free space=    13045394
 reducing frespc by                   337 to               13045057 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1           4|         4|         0|         4|         0|         1|
 -------------------------------------------------------------------------------
  Y 2          20|       156|         4|        20|         4|         2|
 -------------------------------------------------------------------------------
  X 3          15|      1430|       160|        15|        24|         3|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=           4DP  conft+indsym=          80DP  drtbuffer=         253 DP

dimension of the ci-matrix ->>>      1590

 executing brd_struct for civct
 gentasklist: ntask=                    12
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS   
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      15       4       1430          4      15       4
     2  2   1    11      one-ext yz   1X  2 1      20       4        156          4      20       4
     3  3   2    15      1ex3ex yx    3X  3 2      15      20       1430        156      15      20
     4  1   1     1      allint zz    OX  1 1       4       4          4          4       4       4
     5  2   2     5      0ex2ex yy    OX  2 2      20      20        156        156      20      20
     6  3   3     6      0ex2ex xx*   OX  3 3      15      15       1430       1430      15      15
     7  3   3    18      0ex2ex xx+   OX  3 3      15      15       1430       1430      15      15
     8  2   2    42      four-ext y   4X  2 2      20      20        156        156      20      20
     9  3   3    43      four-ext x   4X  3 3      15      15       1430       1430      15      15
    10  1   1    75      dg-024ext z  OX  1 1       4       4          4          4       4       4
    11  2   2    76      dg-024ext y  OX  2 2      20      20        156        156      20      20
    12  3   3    77      dg-024ext x  OX  3 3      15      15       1430       1430      15      15
----------------------------------------------------------------------------------------------------
REDTASK #   1 TIME=  11.000 N=  1 (task/type/sgbra)=(   1/24/0) (
REDTASK #   2 TIME=  10.000 N=  1 (task/type/sgbra)=(   2/11/0) (
REDTASK #   3 TIME=   9.000 N=  1 (task/type/sgbra)=(   3/15/0) (
REDTASK #   4 TIME=   8.000 N=  1 (task/type/sgbra)=(   4/ 1/0) (
REDTASK #   5 TIME=   7.000 N=  1 (task/type/sgbra)=(   5/ 5/0) (
REDTASK #   6 TIME=   6.000 N=  1 (task/type/sgbra)=(   6/ 6/1) (
REDTASK #   7 TIME=   5.000 N=  1 (task/type/sgbra)=(   7/18/2) (
REDTASK #   8 TIME=   4.000 N=  1 (task/type/sgbra)=(   8/42/1) (
REDTASK #   9 TIME=   3.000 N=  1 (task/type/sgbra)=(   9/43/1) (
REDTASK #  10 TIME=   2.000 N=  1 (task/type/sgbra)=(  10/75/1) (
REDTASK #  11 TIME=   1.000 N=  1 (task/type/sgbra)=(  11/76/1) (
REDTASK #  12 TIME=   0.000 N=  1 (task/type/sgbra)=(  12/77/1) (
 initializing v-file: 1:                  1590

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:          40 2x:           0 4x:           0
All internal counts: zz :          15 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 reference space has dimension       4
 dsyevx: computed roots 1 to    2(converged:   2)

    root           eigenvalues
    ----           ------------
       1         -46.2453833591
       2         -46.2453833530

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt=                     4

         vector  1 from unit 11 written to unit 49 filename cirefv              
 ncorel,neli=                     4                     4
  This is a  mraqcc energy evaluation      

    ------------------------------------------------------------

 setopt2: mraqcc energy evaluation      gvalue=  0.833333333333333      ncorel=
                     4

    ------------------------------------------------------------


    reference energies:

  reference state used is:   1with energy      -46.2453833591

  ### begin active excitation selection ###

 inactive(1:nintern)=AAAAAA
 There are          4 reference CSFs out of          4 valid zwalks.
 There are    0 inactive and    6 active orbitals.
       4 active-active  and        0 inactive excitations.

  ### end active excitation selection ###


 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              1590
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                   30
 residual norm convergence criteria:               0.001000

          starting ci iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         205 2x:          90 4x:          35
All internal counts: zz :          15 yy:          72 xx:          66 ww:           0
One-external counts: yz :         112 yx:         360 yw:           0
Two-external counts: yy :         150 ww:           0 xx:         180 xz:          24 wz:           0 wx:           0
Three-ext.   counts: yx :         151 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1
   ht   1    -2.74679570
calca4: root=   1 anorm**2=  0.00000000 scale:  1.00000000

          calcsovref: tciref block   1

              civs   1
 refs   1    1.00000    

          calcsovref: scrb block   1

                ci   1
 civs   1    1.00000    

          calcsovref: sovref block   1

              v      1
 ref    1    1.00000    

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  1  1    -46.2453833591 -4.4409E-16  5.4956E-02  2.1660E-01  1.0000E-03
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   2

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         205 2x:          90 4x:          35
All internal counts: zz :          15 yy:          72 xx:          66 ww:           0
One-external counts: yz :         112 yx:         360 yw:           0
Two-external counts: yy :         150 ww:           0 xx:         180 xz:          24 wz:           0 wx:           0
Three-ext.   counts: yx :         151 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2
   ht   1    -2.74679570
   ht   2    -0.05495634    -0.25496074
calca4: root=   1 anorm**2=  0.07171788 scale:  1.03523808
calca4: root=   2 anorm**2=  0.92828212 scale:  1.38862598

          calcsovref: tciref block   1

              civs   1       civs   2
 refs   1    1.00000      -5.008900E-14

          calcsovref: scrb block   1

                ci   1         ci   2
 civs   1   0.963474      -0.267802    
 civs   2   0.788394        2.83641    

          calcsovref: sovref block   1

              v      1       v      2
 ref    1   0.963474      -0.267802    

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.96347398    -0.26780194
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  2  1    -46.2903531922  4.4970E-02  1.2548E-03  2.8340E-02  1.0000E-03
 mraqcc  #  2  2    -45.6633152487  2.1647E+00  0.0000E+00  4.8704E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   3

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         205 2x:          90 4x:          35
All internal counts: zz :          15 yy:          72 xx:          66 ww:           0
One-external counts: yz :         112 yx:         360 yw:           0
Two-external counts: yy :         150 ww:           0 xx:         180 xz:          24 wz:           0 wx:           0
Three-ext.   counts: yx :         151 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3
   ht   1    -2.74679570
   ht   2    -0.05495634    -0.25496074
   ht   3    -0.00025492     0.01048662    -0.00653147
calca4: root=   1 anorm**2=  0.07134652 scale:  1.03505870
calca4: root=   2 anorm**2=  0.99857724 scale:  1.41371045
calca4: root=   3 anorm**2=  0.92967316 scale:  1.38912676

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3
 refs   1    1.00000      -5.008900E-14   2.333602E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3
 civs   1   0.963640       3.217713E-02   0.265261    
 civs   2   0.809437      -0.282774       -2.90747    
 civs   3    1.12482        18.5448       -6.36974    

          calcsovref: sovref block   1

              v      1       v      2       v      3
 ref    1   0.963666       3.260989E-02   0.265112    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.96366600     0.03260989     0.26511213
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  3  1    -46.2917669809  1.4138E-03  1.2530E-04  9.7043E-03  1.0000E-03
 mraqcc  #  3  2    -45.8773508635  2.1404E-01  0.0000E+00  2.7302E-01  1.0000E-04
 mraqcc  #  3  3    -45.6380955520  2.1395E+00  0.0000E+00  4.9986E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   4

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         205 2x:          90 4x:          35
All internal counts: zz :          15 yy:          72 xx:          66 ww:           0
One-external counts: yz :         112 yx:         360 yw:           0
Two-external counts: yy :         150 ww:           0 xx:         180 xz:          24 wz:           0 wx:           0
Three-ext.   counts: yx :         151 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4
   ht   1    -2.74679570
   ht   2    -0.05495634    -0.25496074
   ht   3    -0.00025492     0.01048662    -0.00653147
   ht   4     0.00014195    -0.00381572    -0.00035951    -0.00058601
calca4: root=   1 anorm**2=  0.07594050 scale:  1.03727552
calca4: root=   2 anorm**2=  0.98524701 scale:  1.40898794
calca4: root=   3 anorm**2=  0.96533251 scale:  1.40190317
calca4: root=   4 anorm**2=  0.96420793 scale:  1.40150203

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4
 refs   1    1.00000      -5.008900E-14   2.333602E-05  -2.904811E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4
 civs   1   0.961294       0.110421       0.180180       0.176806    
 civs   2   0.811933      -0.381595       -1.54549       -2.61359    
 civs   3    1.27467       -11.7520        11.4266       -11.3278    
 civs   4    1.51360       -44.8568       -23.4116        44.4205    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4
 ref    1   0.961280       0.111450       0.181127       0.175251    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.96127986     0.11144994     0.18112674     0.17525139
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  4  1    -46.2919567037  1.8972E-04  1.0126E-05  2.9720E-03  1.0000E-03
 mraqcc  #  4  2    -46.0621157528  1.8476E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  4  3    -45.7965323728  1.5844E-01  0.0000E+00  3.6411E-01  1.0000E-04
 mraqcc  #  4  4    -45.3977070897  1.8991E+00  0.0000E+00  4.6159E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   5

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         205 2x:          90 4x:          35
All internal counts: zz :          15 yy:          72 xx:          66 ww:           0
One-external counts: yz :         112 yx:         360 yw:           0
Two-external counts: yy :         150 ww:           0 xx:         180 xz:          24 wz:           0 wx:           0
Three-ext.   counts: yx :         151 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1    -2.74679570
   ht   2    -0.05495634    -0.25496074
   ht   3    -0.00025492     0.01048662    -0.00653147
   ht   4     0.00014195    -0.00381572    -0.00035951    -0.00058601
   ht   5    -0.00013375    -0.00023038    -0.00008407    -0.00004241    -0.00005436
calca4: root=   1 anorm**2=  0.07653140 scale:  1.03756031
calca4: root=   2 anorm**2=  0.96809780 scale:  1.40288909
calca4: root=   3 anorm**2=  0.92147920 scale:  1.38617430
calca4: root=   4 anorm**2=  0.80486328 scale:  1.34345200
calca4: root=   5 anorm**2=  0.94792570 scale:  1.39568109

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1    1.00000      -5.008900E-14   2.333602E-05  -2.904811E-05   5.004737E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 civs   1   0.960933       0.113993       0.106679       0.152434       0.170646    
 civs   2   0.812008      -0.302168      -0.833940       -1.48929       -2.52455    
 civs   3    1.28330       -9.17293        14.2175       0.862074       -10.7126    
 civs   4    1.60164       -40.8085       -4.93862       -27.0900        46.9558    
 civs   5    1.09123       -77.3290       -117.838        156.103       -33.8649    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1   0.960971       0.111094       0.101256       0.161053       0.167337    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     0.96097111     0.11109402     0.10125644     0.16105317     0.16733697
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  5  1    -46.2919677533  1.1050E-05  1.4692E-06  9.6224E-04  1.0000E-03
 mraqcc  #  5  2    -46.1055758293  4.3460E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  5  3    -45.8269246644  3.0392E-02  0.0000E+00  3.0989E-01  1.0000E-04
 mraqcc  #  5  4    -45.7450613286  3.4735E-01  0.0000E+00  4.7300E-01  1.0000E-04
 mraqcc  #  5  5    -45.3873709092  1.8888E+00  0.0000E+00  4.6898E-01  1.0000E-04
 
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mraqcc   convergence criteria satisfied after  5 iterations.

 final mraqcc   convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  5  1    -46.2919677533  1.1050E-05  1.4692E-06  9.6224E-04  1.0000E-03
 mraqcc  #  5  2    -46.1055758293  4.3460E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  5  3    -45.8269246644  3.0392E-02  0.0000E+00  3.0989E-01  1.0000E-04
 mraqcc  #  5  4    -45.7450613286  3.4735E-01  0.0000E+00  4.7300E-01  1.0000E-04
 mraqcc  #  5  5    -45.3873709092  1.8888E+00  0.0000E+00  4.6898E-01  1.0000E-04

####################CIUDGINFO####################

   aqcc(lrt) vector at position   1 energy=  -46.291967753253

################END OF CIUDGINFO################

 
 a4den factor for root 1  =  1.012920031
diagon:itrnv=   0
    1 of the   6 expansion vectors are transformed.
    1 of the   5 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
maximum overlap with reference    1(overlap= 0.96097)
reference energy =                         -46.2453833591
total aqcc energy =                        -46.2919677533
diagonal element shift (lrtshift) =         -0.0465843942

 information on vector: 1 from unit 11 written to unit 48 filename civout              


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -46.2919677533

                                                       internal orbitals

                                          level       1    2    3    4    5    6

                                          orbital     3    4    5   17   22   31

                                         symmetry   a1   a1   a1   a2   b1   b2 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  5  1       1  0.113235                        +    +    +    +            
 z*  5  1       2  0.740447                        +    +              +    +  
 z*  5  1       3  0.601984                        +         +         +    +  
 y   5  1       5 -0.012921              1( a2 )   +    +    +                 
 y   5  1      20 -0.067786              2( b2 )   +    +              +       
 y   5  1      22  0.053106              4( b2 )   +    +              +       
 y   5  1      27  0.090116              2( b1 )   +    +                   +  
 y   5  1      29 -0.062489              4( b1 )   +    +                   +  
 y   5  1      35 -0.022423              3( a1 )   +         +    +            
 y   5  1      39  0.011634              7( a1 )   +         +    +            
 y   5  1      45 -0.076561              2( b2 )   +         +         +       
 y   5  1      47  0.052215              4( b2 )   +         +         +       
 y   5  1      52  0.054817              2( b1 )   +         +              +  
 y   5  1      54 -0.043087              4( b1 )   +         +              +  
 y   5  1      74 -0.102705              3( a1 )   +                   +    +  
 y   5  1      78  0.074652              7( a1 )   +                   +    +  
 x   5  1     192  0.017261   10( a1 )   3( a2 )   +    +                      
 x   5  1     194 -0.020504    1( b1 )   1( b2 )   +    +                      
 x   5  1     196 -0.015211    3( b1 )   1( b2 )   +    +                      
 x   5  1     202 -0.012606    2( b1 )   2( b2 )   +    +                      
 x   5  1     204  0.011256    4( b1 )   2( b2 )   +    +                      
 x   5  1     208 -0.015451    1( b1 )   3( b2 )   +    +                      
 x   5  1     210 -0.012076    3( b1 )   3( b2 )   +    +                      
 x   5  1     216  0.012072    2( b1 )   4( b2 )   +    +                      
 x   5  1     218 -0.016785    4( b1 )   4( b2 )   +    +                      
 x   5  1     234 -0.015051    6( b1 )   6( b2 )   +    +                      
 x   5  1     274  0.015946   10( a1 )   3( a2 )   +         +                 
 x   5  1     276 -0.026726    1( b1 )   1( b2 )   +         +                 
 x   5  1     278 -0.020107    3( b1 )   1( b2 )   +         +                 
 x   5  1     284 -0.010501    2( b1 )   2( b2 )   +         +                 
 x   5  1     286  0.010093    4( b1 )   2( b2 )   +         +                 
 x   5  1     290 -0.019816    1( b1 )   3( b2 )   +         +                 
 x   5  1     292 -0.015716    3( b1 )   3( b2 )   +         +                 
 x   5  1     300 -0.013970    4( b1 )   4( b2 )   +         +                 
 x   5  1     316 -0.012994    6( b1 )   6( b2 )   +         +                 
 x   5  1     445  0.015058    3( a2 )   7( b1 )   +                   +       
 x   5  1     446 -0.029911    1( a1 )   1( b2 )   +                   +       
 x   5  1     449 -0.022271    4( a1 )   1( b2 )   +                   +       
 x   5  1     459  0.016216    3( a1 )   2( b2 )   +                   +       
 x   5  1     463 -0.015073    7( a1 )   2( b2 )   +                   +       
 x   5  1     468 -0.022217    1( a1 )   3( b2 )   +                   +       
 x   5  1     471 -0.017540    4( a1 )   3( b2 )   +                   +       
 x   5  1     481 -0.015063    3( a1 )   4( b2 )   +                   +       
 x   5  1     485  0.021724    7( a1 )   4( b2 )   +                   +       
 x   5  1     510  0.017156   10( a1 )   6( b2 )   +                   +       
 x   5  1     512 -0.010154    1( a1 )   7( b2 )   +                   +       
 x   5  1     515 -0.010621    4( a1 )   7( b2 )   +                   +       
 x   5  1     521  0.019583   10( a1 )   7( b2 )   +                   +       
 x   5  1     523 -0.036689    1( a1 )   1( b1 )   +                        +  
 x   5  1     526 -0.027259    4( a1 )   1( b1 )   +                        +  
 x   5  1     536 -0.016634    3( a1 )   2( b1 )   +                        +  
 x   5  1     540  0.015164    7( a1 )   2( b1 )   +                        +  
 x   5  1     545 -0.027291    1( a1 )   3( b1 )   +                        +  
 x   5  1     548 -0.021510    4( a1 )   3( b1 )   +                        +  
 x   5  1     558  0.015328    3( a1 )   4( b1 )   +                        +  
 x   5  1     562 -0.021832    7( a1 )   4( b1 )   +                        +  
 x   5  1     586 -0.013236    9( a1 )   6( b1 )   +                        +  
 x   5  1     587 -0.018565   10( a1 )   6( b1 )   +                        +  
 x   5  1     592  0.010293    4( a1 )   7( b1 )   +                        +  
 x   5  1     597  0.014695    9( a1 )   7( b1 )   +                        +  
 x   5  1     598  0.010550   10( a1 )   7( b1 )   +                        +  
 x   5  1     620 -0.015431    3( a2 )   7( b2 )   +                        +  
 x   5  1     805 -0.011523    3( a2 )   1( b1 )        +              +       
 x   5  1     811 -0.010738    3( a2 )   3( b1 )        +              +       
 x   5  1     833 -0.014772   10( a1 )   1( b2 )        +              +       
 x   5  1     839 -0.016707    5( a1 )   2( b2 )        +              +       
 x   5  1     855 -0.013451   10( a1 )   3( b2 )        +              +       
 x   5  1     861  0.013372    5( a1 )   4( b2 )        +              +       
 x   5  1     879  0.014547    1( a1 )   6( b2 )        +              +       
 x   5  1     882  0.013503    4( a1 )   6( b2 )        +              +       
 x   5  1     909 -0.012108    9( a1 )   1( b1 )        +                   +  
 x   5  1     916  0.018043    5( a1 )   2( b1 )        +                   +  
 x   5  1     931 -0.011182    9( a1 )   3( b1 )        +                   +  
 x   5  1     938 -0.012796    5( a1 )   4( b1 )        +                   +  
 x   5  1     956 -0.015149    1( a1 )   6( b1 )        +                   +  
 x   5  1     959 -0.013960    4( a1 )   6( b1 )        +                   +  
 x   5  1     980  0.012021    3( a2 )   1( b2 )        +                   +  
 x   5  1     986  0.011062    3( a2 )   3( b2 )        +                   +  
 x   5  1    1129 -0.011501   10( a1 )   1( b2 )             +         +       
 x   5  1    1135 -0.014881    5( a1 )   2( b2 )             +         +       
 x   5  1    1151 -0.010678   10( a1 )   3( b2 )             +         +       
 x   5  1    1157  0.010365    5( a1 )   4( b2 )             +         +       
 x   5  1    1175  0.012291    1( a1 )   6( b2 )             +         +       
 x   5  1    1178  0.011311    4( a1 )   6( b2 )             +         +       
 x   5  1    1205 -0.010139    9( a1 )   1( b1 )             +              +  
 x   5  1    1212  0.013580    5( a1 )   2( b1 )             +              +  
 x   5  1    1234 -0.010918    5( a1 )   4( b1 )             +              +  
 x   5  1    1252 -0.011919    1( a1 )   6( b1 )             +              +  
 x   5  1    1255 -0.011057    4( a1 )   6( b1 )             +              +  
 x   5  1    1499  0.022494    3( a1 )   5( a1 )                       +    +  
 x   5  1    1510  0.016782    5( a1 )   7( a1 )                       +    +  
 x   5  1    1527  0.015001    1( a1 )  10( a1 )                       +    +  
 x   5  1    1530  0.013759    4( a1 )  10( a1 )                       +    +  
 x   5  1    1564 -0.018229    1( b1 )   7( b1 )                       +    +  
 x   5  1    1566 -0.016839    3( b1 )   7( b1 )                       +    +  
 x   5  1    1585 -0.018846    1( b2 )   7( b2 )                       +    +  
 x   5  1    1587 -0.017417    3( b2 )   7( b2 )                       +    +  

 ci coefficient statistics:
           rq > 0.1                4
      0.1> rq > 0.01              93
     0.01> rq > 0.001            256
    0.001> rq > 0.0001           282
   0.0001> rq > 0.00001           97
  0.00001> rq > 0.000001          22
 0.000001> rq                    836
           all                  1590
  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.113234544787
     2     2      0.740446972102
     3     3      0.601984067522
     4     4     -0.000058606747

 number of reference csfs (nref) is     4.  root number (iroot) is  1.
 c0**2 =   0.92346860  c0**2(renormalized) =   0.99414295
 c**2 (all zwalks) =   0.92346860  c**2(all zwalks, renormalized) =   0.99414295
 passed aftci ... 
 readint2: molcas,dalton2=                     0                     0
 files%faoints=aoints              
lodens (list->root)=  1
sifcfg setup: record length 4096 DP
# d1 elements per record  3272
# d2 elements per record  2730
  The AQCC density will be calculated.
 item #                     1 suffix=:.drt1.state1:
 read_civout: repnuc=  -43.4985876562748     
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method: 30 last record  1max overlap with ref# 96% root-following 0
 MR-CISD energy:   -46.29196775    -2.79338010
 residuum:     0.00096224
 deltae:     0.00001105
 apxde:     0.00000147
 a4den:     1.01292003
 reference energy:   -46.24538336    -2.74679570

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.96097111     0.11109402     0.10125644     0.16105317     0.16733697     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.96097111     0.11109402     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 rdcirefv: extracting reference vector # 1(blocksize=     4 #zcsf=       4)
 =========== Executing IN-CORE method ==========
================================================================================
   DYZ=       0  DYX=       0  DYW=       0
   D0Z=       3  D0Y=       0  D0X=       0  D0W=       0
  DDZI=      16 DDYI=       0 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
 root #                      1 : Scaling(1) with    1.01292003107185      
 corresponding to ref #                      1
 computing final density
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=      16  DYX=      60  DYW=       0
   D0Z=       3  D0Y=      18  D0X=      12  D0W=       0
  DDZI=      16 DDYI=      60 DDXI=      30 DDWI=       0
  DDZE=       0 DDYE=      20 DDXE=      15 DDWE=       0
================================================================================
Trace of MO density:     4.000000
    4  correlated and     8  frozen core electrons

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.99087385    -0.00006947     0.00000164     0.00000000     0.00000651     0.00064399
   MO   4     0.00000000     0.00000000    -0.00006947     0.58706104     0.46696987     0.00000000     0.00036298     0.07489451
   MO   5     0.00000000     0.00000000     0.00000164     0.46696987     0.39504884     0.00000000     0.00131900     0.05818301
   MO   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00493319     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000     0.00000651     0.00036298     0.00131900     0.00000000     0.00009472     0.00002923
   MO   8     0.00000000     0.00000000     0.00064399     0.07489451     0.05818301     0.00000000     0.00002923     0.01281440
   MO   9     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00395431     0.00000000     0.00000000
   MO  10     0.00000000     0.00000000     0.00101373     0.00000585     0.00000817     0.00000000    -0.00000049     0.00000237
   MO  11     0.00000000     0.00000000    -0.00006826    -0.00552706    -0.00349565     0.00000000     0.00007962    -0.00105210
   MO  12     0.00000000     0.00000000    -0.00062893    -0.05210623    -0.04131048     0.00000000    -0.00009381    -0.00963788
   MO  13     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00128841     0.00000000     0.00000000
   MO  14     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00093768     0.00000000     0.00000000
   MO  15     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00025931     0.00000000     0.00000000
   MO  16     0.00000000     0.00000000    -0.00048568     0.00000125     0.00000075     0.00000000     0.00000003    -0.00000006

                MO   9         MO  10         MO  11         MO  12         MO  13         MO  14         MO  15         MO  16
   MO   3     0.00000000     0.00101373    -0.00006826    -0.00062893     0.00000000     0.00000000     0.00000000    -0.00048568
   MO   4     0.00000000     0.00000585    -0.00552706    -0.05210623     0.00000000     0.00000000     0.00000000     0.00000125
   MO   5     0.00000000     0.00000817    -0.00349565    -0.04131048     0.00000000     0.00000000     0.00000000     0.00000075
   MO   6     0.00395431     0.00000000     0.00000000     0.00000000     0.00128841     0.00093768    -0.00025931     0.00000000
   MO   7     0.00000000    -0.00000049     0.00007962    -0.00009381     0.00000000     0.00000000     0.00000000     0.00000003
   MO   8     0.00000000     0.00000237    -0.00105210    -0.00963788     0.00000000     0.00000000     0.00000000    -0.00000006
   MO   9     0.00327844     0.00000000     0.00000000     0.00000000     0.00113569     0.00096615    -0.00028853     0.00000000
   MO  10     0.00000000     0.00258963    -0.00000098    -0.00000392     0.00000000     0.00000000     0.00000000    -0.00012651
   MO  11     0.00000000    -0.00000098     0.00017051     0.00075697     0.00000000     0.00000000     0.00000000     0.00000005
   MO  12     0.00000000    -0.00000392     0.00075697     0.00755837     0.00000000     0.00000000     0.00000000     0.00000019
   MO  13     0.00113569     0.00000000     0.00000000     0.00000000     0.00043939     0.00047555    -0.00015428     0.00000000
   MO  14     0.00096615     0.00000000     0.00000000     0.00000000     0.00047555     0.00139571     0.00077865     0.00000000
   MO  15    -0.00028853     0.00000000     0.00000000     0.00000000    -0.00015428     0.00077865     0.00316719     0.00000000
   MO  16     0.00000000    -0.00012651     0.00000005     0.00000019     0.00000000     0.00000000     0.00000000     0.00000779

Natural orbital populations,block 1
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     2.00000000     0.99087628     0.98178307     0.01504101     0.00879071     0.00574641     0.00345854
              MO     9       MO    10       MO    11       MO    12       MO    13       MO    14       MO    15       MO    16
  occ(*)=     0.00259472     0.00090959     0.00011397     0.00005803     0.00005321     0.00000423     0.00000187     0.00000141

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4
   MO   1     0.01402681     0.00134611    -0.00090962     0.00000000
   MO   2     0.00134611     0.00019772    -0.00014940     0.00000000
   MO   3    -0.00090962    -0.00014940     0.00012526     0.00000000
   MO   4     0.00000000     0.00000000     0.00000000     0.00236859

Natural orbital populations,block 2
              MO     1       MO     2       MO     3       MO     4       MO
  occ(*)=     0.01421666     0.00236859     0.00012785     0.00000528

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     0.95348435     0.00000000     0.09502583     0.00000000    -0.06646269     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.00535272     0.00000000     0.00428833     0.00000000     0.00139563    -0.00047246
   MO   4     0.00000000     0.09502583     0.00000000     0.01291618     0.00000000    -0.00972847     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.00428833     0.00000000     0.00354498     0.00000000     0.00122268    -0.00050118
   MO   6     0.00000000    -0.06646269     0.00000000    -0.00972847     0.00000000     0.00761827     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000     0.00139563     0.00000000     0.00122268     0.00000000     0.00046945    -0.00025428
   MO   8     0.00000000     0.00000000    -0.00047246     0.00000000    -0.00050118     0.00000000    -0.00025428     0.00204020
   MO   9     0.00000000     0.00000000    -0.00090094     0.00000000    -0.00090913     0.00000000    -0.00043741    -0.00070756

                MO   9
   MO   2     0.00000000
   MO   3    -0.00090094
   MO   4     0.00000000
   MO   5    -0.00090913
   MO   6     0.00000000
   MO   7    -0.00043741
   MO   8    -0.00070756
   MO   9     0.00152840

Natural orbital populations,block 3
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     0.96767868     0.00949722     0.00623774     0.00253288     0.00085168     0.00010239     0.00005201
              MO     9       MO
  occ(*)=     0.00000195

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     0.95573841     0.00000000     0.09154345     0.00000000    -0.06494935     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.00457807     0.00000000     0.00367713     0.00000000    -0.00120060    -0.00071330
   MO   4     0.00000000     0.09154345     0.00000000     0.01224530     0.00000000    -0.00932510     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.00367713     0.00000000     0.00306006     0.00000000    -0.00106498    -0.00074014
   MO   6     0.00000000    -0.06494935     0.00000000    -0.00932510     0.00000000     0.00739772     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000    -0.00120060     0.00000000    -0.00106498     0.00000000     0.00041521     0.00036563
   MO   8     0.00000000     0.00000000    -0.00071330     0.00000000    -0.00074014     0.00000000     0.00036563     0.00175571
   MO   9     0.00000000     0.00000000     0.00061480     0.00000000     0.00066189     0.00000000    -0.00034430     0.00051037

                MO   9
   MO   2     0.00000000
   MO   3     0.00061480
   MO   4     0.00000000
   MO   5     0.00066189
   MO   6     0.00000000
   MO   7    -0.00034430
   MO   8     0.00051037
   MO   9     0.00170353

Natural orbital populations,block 4
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     0.96900596     0.00821071     0.00625887     0.00223857     0.00100713     0.00011661     0.00005441
              MO     9       MO
  occ(*)=     0.00000176


 total number of electrons =   12.0000000000

 test slabel:                     4
  a1  a2  b1  b2 


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        a1  partial gross atomic populations
   ao class       1a1        2a1        3a1        4a1        5a1        6a1 
     3_ s       2.000000   0.000000   0.990840   0.000035   0.000000   0.000000
     3_ p       0.000000   2.000000   0.000000   0.000000   0.000000   0.008480
     3_ d       0.000000   0.000000   0.000036   0.981748   0.015041   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000000   0.000000   0.000310
 
   ao class       7a1        8a1        9a1       10a1       11a1       12a1 
     3_ s       0.000000   0.000000   0.002595   0.000000   0.000000   0.000000
     3_ p       0.000000   0.000001   0.000000   0.000119   0.000000   0.000000
     3_ d       0.005746   0.000000   0.000000   0.000000   0.000114   0.000058
     3_ f       0.000000   0.003457   0.000000   0.000790   0.000000   0.000000
 
   ao class      13a1       14a1       15a1       16a1 
     3_ s       0.000000   0.000000   0.000000   0.000001
     3_ p       0.000048   0.000000   0.000002   0.000000
     3_ d       0.000000   0.000004   0.000000   0.000000
     3_ f       0.000005   0.000000   0.000000   0.000000

                        a2  partial gross atomic populations
   ao class       1a2        2a2        3a2        4a2 
     3_ d       0.014217   0.000000   0.000128   0.000005
     3_ f       0.000000   0.002369   0.000000   0.000000

                        b1  partial gross atomic populations
   ao class       1b1        2b1        3b1        4b1        5b1        6b1 
     3_ p       2.000000   0.000000   0.009199   0.000000   0.000001   0.000118
     3_ d       0.000000   0.967679   0.000000   0.006238   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000298   0.000000   0.002532   0.000733
 
   ao class       7b1        8b1        9b1 
     3_ p       0.000000   0.000046   0.000002
     3_ d       0.000102   0.000000   0.000000
     3_ f       0.000000   0.000006   0.000000

                        b2  partial gross atomic populations
   ao class       1b2        2b2        3b2        4b2        5b2        6b2 
     3_ p       2.000000   0.000000   0.007878   0.000000   0.000001   0.000123
     3_ d       0.000000   0.969006   0.000000   0.006259   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000332   0.000000   0.002238   0.000884
 
   ao class       7b2        8b2        9b2 
     3_ p       0.000000   0.000050   0.000002
     3_ d       0.000117   0.000000   0.000000
     3_ f       0.000000   0.000004   0.000000


                        gross atomic populations
     ao            3_
      s         2.993471
      p         6.026072
      d         2.966498
      f         0.013959
    total      12.000000
 

 Total number of electrons:   12.00000000

 DA ...
