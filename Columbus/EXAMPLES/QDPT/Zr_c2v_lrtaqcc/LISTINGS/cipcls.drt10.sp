
 program cipc      

 print the csf info for mrsdci wave functions

 written by: ron shepard

 version date: 06-jun-96

 This Version of Program cipc is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 workspace allocation parameters: lencor=  13107200 mem1=         0 ifirst=         1

 drt header information:
  cidrt_title                                                                    
 nmot  =    38 niot  =     6 nfct  =     4 nfvt  =     0
 nrow  =    23 nsym  =     4 ssym  =     2 lenbuf=  1600
 spnorb=     F spnodd=     F lxyzir(1:3)= 0 0 0
 nwalk,xbar:         50       15       20       15        0
 nvalwt,nvalw:       39        4       20       15        0
 ncsft:            1590
 map(*)=    -1 -1 29 30 31  1  2  3  4  5  6  7  8  9 10 11 32 12 13 14
            -1 33 15 16 17 18 19 20 21 -1 34 22 23 24 25 26 27 28
 mu(*)=      0  0  0  0  0  0
 syml(*) =   1  1  1  2  3  4
 rmo(*)=     3  4  5  1  2  2

 indx01:    39 indices saved in indxv(*)
 test nroots froot                      1                     1
===================================ROOT # 1===================================

 rdhciv: CI vector file information:
  cidrt_title                                                                    
 energy computed by program ciudg.       zam792            17:56:40.031 17-Dec-13

 lenrec =   32768 lenci =      1590 ninfo =  6 nenrgy =  8 ntitle =  2

 Max. overlap with ref vector #        1
 Valid ci vector #        1
 Method:       30       96% overlap
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -4.349858765627E+01, ietype=    6,   fcore energy of type: H1(*)   
 energy( 3)= -4.629196775325E+01, ietype=-1026,   total energy of type: MRSDCI  
 energy( 4)=  9.622354930485E-04, ietype=-2055, cnvginf energy of type: CI-Resid
 energy( 5)=  1.104957267595E-05, ietype=-2056, cnvginf energy of type: CI-D.E. 
 energy( 6)=  1.469206041038E-06, ietype=-2057, cnvginf energy of type: CI-ApxDE
 energy( 7)=  1.012920031072E+00, ietype=-1039,   total energy of type: a4den   
 energy( 8)= -4.624538335908E+01, ietype=-1041,   total energy of type: E(ref)  
==================================================================================
space sufficient for valid walk range           1         39
               respectively csf range           1       1590

 space is available for   4357921 coefficients.

 updated histogram parameters:
 csfmn = 0.0000E+00 csfmx = 1.0000E+00 fhist = 5.0000E-01 nhist =  20

 this program will print the csfs generated from
 the drt according to the following print options :

 1) run in batch mode: all valid roots are automatically
    analysed and csf info is printed by default contribution
    threshold 0.01 
 2) run in interactive mode
 3) generate files for cioverlap without symmetry
 4) generate files for cioverlap with symmetry

 input menu number [  1]:
================================================================================
===================================VECTOR # 1===================================
================================================================================


 rdcivnew:      97 coefficients were selected.
 workspace: ncsfmx=    1590
 ncsfmx=                  1590

 histogram parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =    1590 ncsft =    1590 ncsf =      97
 nhist =  20 fhist = 0.50000

    cmin                cmax        num  '*'=     2 csfs.
 ----------          ----------   ----- ---------|---------|---------|---------|
 5.0000E-01 <= |c| < 1.0000E+00       2 *
 2.5000E-01 <= |c| < 5.0000E-01       0
 1.2500E-01 <= |c| < 2.5000E-01       0
 6.2500E-02 <= |c| < 1.2500E-01       6 ***
 3.1250E-02 <= |c| < 6.2500E-02       6 ***
 1.5625E-02 <= |c| < 3.1250E-02      31 ****************
 7.8125E-03 <= |c| < 1.5625E-02      52 **************************
 3.9062E-03 <= |c| < 7.8125E-03       0
 1.9531E-03 <= |c| < 3.9062E-03       0
 9.7656E-04 <= |c| < 1.9531E-03       0
 4.8828E-04 <= |c| < 9.7656E-04       0
 2.4414E-04 <= |c| < 4.8828E-04       0
 1.2207E-04 <= |c| < 2.4414E-04       0
 6.1035E-05 <= |c| < 1.2207E-04       0
 3.0518E-05 <= |c| < 6.1035E-05       0
 1.5259E-05 <= |c| < 3.0518E-05       0
 7.6294E-06 <= |c| < 1.5259E-05       0
 3.8147E-06 <= |c| < 7.6294E-06       0
 0.0000E+00 <= |c| < 3.8147E-06       0
                                  ----- ---------|---------|---------|---------|
                  total read =       97 total stored =      97

 from the selected csfs,
 min(|csfvec(:)|) = 1.0093E-02    max(|csfvec(:)|) = 7.4045E-01
 norm=   1.00000000000000     
 csfs will be printed based on coefficient magnitudes.

 current csfvec(*) selection parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =    1590 ncsft =    1590 ncsf =      97

 i:slabel(i) =  1: a1   2: a2   3: b1   4: b2 
 
 frozen orbital =    1    2    3    4
 symfc(*)       =    1    1    3    4
 label          =  a1   a1   b1   b2 
 rmo(*)         =    1    2    1    1
 
 internal level =    1    2    3    4    5    6
 syml(*)        =    1    1    1    2    3    4
 label          =  a1   a1   a1   a2   b1   b2 
 rmo(*)         =    3    4    5    1    2    2

 printing selected csfs in sorted order from cmin = 0.00000 to cmax = 1.00000

   indcsf     c     c**2   v  lab:rmo  lab:rmo   step(*)
  ------- -------- ------- - ---- --- ---- --- ------------
          2  0.74045 0.54826 z*                    110011
          3  0.60198 0.36238 z*                    101011
          1  0.11323 0.01282 z*                    111100
         74 -0.10270 0.01055 y           a1 :  8  1100011
         27  0.09012 0.00812 y           b1 :  4  1110001
         45 -0.07656 0.00586 y           b2 :  4  1101010
         78  0.07465 0.00557 y           a1 : 12  1100011
         20 -0.06779 0.00459 y           b2 :  4  1110010
         29 -0.06249 0.00390 y           b1 :  6  1110001
         52  0.05482 0.00300 y           b1 :  4  1101001
         22  0.05311 0.00282 y           b2 :  6  1110010
         47  0.05222 0.00273 y           b2 :  6  1101010
         54 -0.04309 0.00186 y           b1 :  6  1101001
        523 -0.03669 0.00135 x  a1 :  6  b1 :  3 11100001
        446 -0.02991 0.00089 x  a1 :  6  b2 :  3 11100010
        545 -0.02729 0.00074 x  a1 :  6  b1 :  5 11100001
        526 -0.02726 0.00074 x  a1 :  9  b1 :  3 11100001
        276 -0.02673 0.00071 x  b1 :  3  b2 :  3 11101000
       1499  0.02249 0.00051 x  a1 :  8  a1 : 10 11000011
         35 -0.02242 0.00050 y           a1 :  8  1101100
        449 -0.02227 0.00050 x  a1 :  9  b2 :  3 11100010
        468 -0.02222 0.00049 x  a1 :  6  b2 :  5 11100010
        562 -0.02183 0.00048 x  a1 : 12  b1 :  6 11100001
        485  0.02172 0.00047 x  a1 : 12  b2 :  6 11100010
        548 -0.02151 0.00046 x  a1 :  9  b1 :  5 11100001
        194 -0.02050 0.00042 x  b1 :  3  b2 :  3 11110000
        278 -0.02011 0.00040 x  b1 :  5  b2 :  3 11101000
        290 -0.01982 0.00039 x  b1 :  3  b2 :  5 11101000
        521  0.01958 0.00038 x  a1 : 15  b2 :  9 11100010
       1585 -0.01885 0.00036 x  b2 :  3  b2 :  9 11000011
        587 -0.01857 0.00034 x  a1 : 15  b1 :  8 11100001
       1564 -0.01823 0.00033 x  b1 :  3  b1 :  9 11000011
        916  0.01804 0.00033 x  a1 : 10  b1 :  4 11010001
        471 -0.01754 0.00031 x  a1 :  9  b2 :  5 11100010
       1587 -0.01742 0.00030 x  b2 :  5  b2 :  9 11000011
        192  0.01726 0.00030 x  a1 : 15  a2 :  4 11110000
        510  0.01716 0.00029 x  a1 : 15  b2 :  8 11100010
       1566 -0.01684 0.00028 x  b1 :  5  b1 :  9 11000011
        218 -0.01678 0.00028 x  b1 :  6  b2 :  6 11110000
       1510  0.01678 0.00028 x  a1 : 10  a1 : 12 11000011
        839 -0.01671 0.00028 x  a1 : 10  b2 :  4 11010010
        536 -0.01663 0.00028 x  a1 :  8  b1 :  4 11100001
        459  0.01622 0.00026 x  a1 :  8  b2 :  4 11100010
        274  0.01595 0.00025 x  a1 : 15  a2 :  4 11101000
        292 -0.01572 0.00025 x  b1 :  5  b2 :  5 11101000
        208 -0.01545 0.00024 x  b1 :  3  b2 :  5 11110000
        620 -0.01543 0.00024 x  a2 :  4  b2 :  9 11100001
        558  0.01533 0.00023 x  a1 :  8  b1 :  6 11100001
        196 -0.01521 0.00023 x  b1 :  5  b2 :  3 11110000
        540  0.01516 0.00023 x  a1 : 12  b1 :  4 11100001
        956 -0.01515 0.00023 x  a1 :  6  b1 :  8 11010001
        463 -0.01507 0.00023 x  a1 : 12  b2 :  4 11100010
        481 -0.01506 0.00023 x  a1 :  8  b2 :  6 11100010
        445  0.01506 0.00023 x  a2 :  4  b1 :  9 11100010
        234 -0.01505 0.00023 x  b1 :  8  b2 :  8 11110000
       1527  0.01500 0.00023 x  a1 :  6  a1 : 15 11000011
       1135 -0.01488 0.00022 x  a1 : 10  b2 :  4 11001010
        833 -0.01477 0.00022 x  a1 : 15  b2 :  3 11010010
        597  0.01469 0.00022 x  a1 : 14  b1 :  9 11100001
        879  0.01455 0.00021 x  a1 :  6  b2 :  8 11010010
        300 -0.01397 0.00020 x  b1 :  6  b2 :  6 11101000
        959 -0.01396 0.00019 x  a1 :  9  b1 :  8 11010001
       1530  0.01376 0.00019 x  a1 :  9  a1 : 15 11000011
       1212  0.01358 0.00018 x  a1 : 10  b1 :  4 11001001
        882  0.01350 0.00018 x  a1 :  9  b2 :  8 11010010
        855 -0.01345 0.00018 x  a1 : 15  b2 :  5 11010010
        861  0.01337 0.00018 x  a1 : 10  b2 :  6 11010010
        586 -0.01324 0.00018 x  a1 : 14  b1 :  8 11100001
        316 -0.01299 0.00017 x  b1 :  8  b2 :  8 11101000
          5 -0.01292 0.00017 y           a2 :  2  1111000
        938 -0.01280 0.00016 x  a1 : 10  b1 :  6 11010001
        202 -0.01261 0.00016 x  b1 :  4  b2 :  4 11110000
       1175  0.01229 0.00015 x  a1 :  6  b2 :  8 11001010
        909 -0.01211 0.00015 x  a1 : 14  b1 :  3 11010001
        210 -0.01208 0.00015 x  b1 :  5  b2 :  5 11110000
        216  0.01207 0.00015 x  b1 :  4  b2 :  6 11110000
        980  0.01202 0.00014 x  a2 :  4  b2 :  3 11010001
       1252 -0.01192 0.00014 x  a1 :  6  b1 :  8 11001001
         39  0.01163 0.00014 y           a1 : 12  1101100
        805 -0.01152 0.00013 x  a2 :  4  b1 :  3 11010010
       1129 -0.01150 0.00013 x  a1 : 15  b2 :  3 11001010
       1178  0.01131 0.00013 x  a1 :  9  b2 :  8 11001010
        204  0.01126 0.00013 x  b1 :  6  b2 :  4 11110000
        931 -0.01118 0.00013 x  a1 : 14  b1 :  5 11010001
        986  0.01106 0.00012 x  a2 :  4  b2 :  5 11010001
       1255 -0.01106 0.00012 x  a1 :  9  b1 :  8 11001001
       1234 -0.01092 0.00012 x  a1 : 10  b1 :  6 11001001
        811 -0.01074 0.00012 x  a2 :  4  b1 :  5 11010010
       1151 -0.01068 0.00011 x  a1 : 15  b2 :  5 11001010
        515 -0.01062 0.00011 x  a1 :  9  b2 :  9 11100010
        598  0.01055 0.00011 x  a1 : 15  b1 :  9 11100001
        284 -0.01050 0.00011 x  b1 :  4  b2 :  4 11101000
       1157  0.01036 0.00011 x  a1 : 10  b2 :  6 11001010
        592  0.01029 0.00011 x  a1 :  9  b1 :  9 11100001
        512 -0.01015 0.00010 x  a1 :  6  b2 :  9 11100010
       1205 -0.01014 0.00010 x  a1 : 14  b1 :  3 11001001
        286  0.01009 0.00010 x  b1 :  6  b2 :  4 11101000
           97 csfs were printed in this range.
