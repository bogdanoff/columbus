
 program cipc      

 print the csf info for mrsdci wave functions

 written by: ron shepard

 version date: 06-jun-96

 This Version of Program cipc is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 workspace allocation parameters: lencor=  13107200 mem1=         0 ifirst=         1

 drt header information:
  cidrt_title                                                                    
 nmot  =    38 niot  =     6 nfct  =     4 nfvt  =     0
 nrow  =    36 nsym  =     4 ssym  =     3 lenbuf=  1600
 spnorb=     F spnodd=     F lxyzir(1:3)= 0 0 0
 nwalk,xbar:        246      105       90       36       15
 nvalwt,nvalw:      169       28       90       36       15
 ncsft:            5600
 map(*)=    -1 -1 29 30 31  1  2  3  4  5  6  7  8  9 10 11 32 12 13 14
            -1 33 15 16 17 18 19 20 21 -1 34 22 23 24 25 26 27 28
 mu(*)=      0  0  0  0  0  0
 syml(*) =   1  1  1  2  3  4
 rmo(*)=     3  4  5  1  2  2

 indx01:   169 indices saved in indxv(*)
 test nroots froot                      1                     1
===================================ROOT # 1===================================

 rdhciv: CI vector file information:
  cidrt_title                                                                    
 energy computed by program ciudg.       zam792            17:56:39.793 17-Dec-13

 lenrec =   32768 lenci =      5600 ninfo =  6 nenrgy =  8 ntitle =  2

 Max. overlap with ref vector #        1
 Valid ci vector #        1
 Method:       30       96% overlap
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -4.349858765627E+01, ietype=    6,   fcore energy of type: H1(*)   
 energy( 3)= -4.634842693365E+01, ietype=-1026,   total energy of type: MRSDCI  
 energy( 4)=  4.978277447724E-04, ietype=-2055, cnvginf energy of type: CI-Resid
 energy( 5)=  3.596433464814E-06, ietype=-2056, cnvginf energy of type: CI-D.E. 
 energy( 6)=  3.057379334698E-07, ietype=-2057, cnvginf energy of type: CI-ApxDE
 energy( 7)=  1.012026049969E+00, ietype=-1039,   total energy of type: a4den   
 energy( 8)= -4.630187857691E+01, ietype=-1041,   total energy of type: E(ref)  
==================================================================================
space sufficient for valid walk range           1        169
               respectively csf range           1       5600

 space is available for   4357761 coefficients.

 updated histogram parameters:
 csfmn = 0.0000E+00 csfmx = 1.0000E+00 fhist = 5.0000E-01 nhist =  20

 this program will print the csfs generated from
 the drt according to the following print options :

 1) run in batch mode: all valid roots are automatically
    analysed and csf info is printed by default contribution
    threshold 0.01 
 2) run in interactive mode
 3) generate files for cioverlap without symmetry
 4) generate files for cioverlap with symmetry

 input menu number [  1]:
================================================================================
===================================VECTOR # 1===================================
================================================================================


 rdcivnew:     119 coefficients were selected.
 workspace: ncsfmx=    5600
 ncsfmx=                  5600

 histogram parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =    5600 ncsft =    5600 ncsf =     119
 nhist =  20 fhist = 0.50000

    cmin                cmax        num  '*'=     2 csfs.
 ----------          ----------   ----- ---------|---------|---------|---------|
 5.0000E-01 <= |c| < 1.0000E+00       2 *
 2.5000E-01 <= |c| < 5.0000E-01       0
 1.2500E-01 <= |c| < 2.5000E-01       1 *
 6.2500E-02 <= |c| < 1.2500E-01       0
 3.1250E-02 <= |c| < 6.2500E-02      23 ************
 1.5625E-02 <= |c| < 3.1250E-02      40 ********************
 7.8125E-03 <= |c| < 1.5625E-02      53 ***************************
 3.9062E-03 <= |c| < 7.8125E-03       0
 1.9531E-03 <= |c| < 3.9062E-03       0
 9.7656E-04 <= |c| < 1.9531E-03       0
 4.8828E-04 <= |c| < 9.7656E-04       0
 2.4414E-04 <= |c| < 4.8828E-04       0
 1.2207E-04 <= |c| < 2.4414E-04       0
 6.1035E-05 <= |c| < 1.2207E-04       0
 3.0518E-05 <= |c| < 6.1035E-05       0
 1.5259E-05 <= |c| < 3.0518E-05       0
 7.6294E-06 <= |c| < 1.5259E-05       0
 3.8147E-06 <= |c| < 7.6294E-06       0
 0.0000E+00 <= |c| < 3.8147E-06       0
                                  ----- ---------|---------|---------|---------|
                  total read =      119 total stored =     119

 from the selected csfs,
 min(|csfvec(:)|) = 1.0006E-02    max(|csfvec(:)|) = 6.8090E-01
 norm=  0.999999999999999     
 csfs will be printed based on coefficient magnitudes.

 current csfvec(*) selection parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =    5600 ncsft =    5600 ncsf =     119

 i:slabel(i) =  1: a1   2: a2   3: b1   4: b2 
 
 frozen orbital =    1    2    3    4
 symfc(*)       =    1    1    3    4
 label          =  a1   a1   b1   b2 
 rmo(*)         =    1    2    1    1
 
 internal level =    1    2    3    4    5    6
 syml(*)        =    1    1    1    2    3    4
 label          =  a1   a1   a1   a2   b1   b2 
 rmo(*)         =    3    4    5    1    2    2

 printing selected csfs in sorted order from cmin = 0.00000 to cmax = 1.00000

   indcsf     c     c**2   v  lab:rmo  lab:rmo   step(*)
  ------- -------- ------- - ---- --- ---- --- ------------
          3  0.68090 0.46363 z*                    300101
          2 -0.64895 0.42113 z*                    301010
          1  0.15208 0.02313 z*                    310010
         14  0.06098 0.00372 z*                    101102
          7  0.05729 0.00328 z*                    112010
       5375 -0.05641 0.00318 w           a1 :  6  3000101
       5447 -0.05626 0.00317 w           b1 :  3  3000101
       5155  0.05358 0.00287 w           b1 :  3  3001010
       5183  0.05293 0.00280 w           b2 :  3  3001010
       5475 -0.05014 0.00251 w           b2 :  3  3000101
       5083  0.04845 0.00235 w           a1 :  6  3001010
       5381 -0.04815 0.00232 w  a1 :  6  a1 :  9 12000101
       5450 -0.04786 0.00229 w  b1 :  3  b1 :  5 12000101
       5158  0.04559 0.00208 w  b1 :  3  b1 :  5 12001010
         10  0.04537 0.00206 z*                    110102
       5186  0.04514 0.00204 w  b2 :  3  b2 :  5 12001010
         16 -0.04350 0.00189 z*                    100013
       5478 -0.04244 0.00180 w  b2 :  3  b2 :  5 12000101
       5089  0.04109 0.00169 w  a1 :  6  a1 :  9 12001010
       4530  0.03960 0.00157 w  b1 :  3  b2 :  3 12100001
       4292 -0.03959 0.00157 w  a1 :  6  b2 :  3 12100100
       4173  0.03771 0.00142 w  a1 :  6  b1 :  3 12101000
         12 -0.03487 0.00122 z*                    102101
         18 -0.03389 0.00115 z*                    030101
         28  0.03325 0.00111 z*                    000131
         26 -0.03169 0.00100 z*                    001310
         17  0.03064 0.00094 z*                    031010
       4532  0.03030 0.00092 w  b1 :  5  b2 :  3 12100001
       4295 -0.03023 0.00091 w  a1 :  9  b2 :  3 12100100
       4314 -0.03022 0.00091 w  a1 :  6  b2 :  5 12100100
       4544  0.03017 0.00091 w  b1 :  3  b2 :  5 12100001
          8  0.03006 0.00090 z*                    111020
       4375  0.02971 0.00088 w  a1 :  6  a1 :  9 12100010
       4472 -0.02932 0.00086 w  b2 :  3  b2 :  5 12100010
       4195  0.02886 0.00083 w  a1 :  6  b1 :  5 12101000
       4176  0.02873 0.00083 w  a1 :  9  b1 :  3 12101000
       4369  0.02751 0.00076 w           a1 :  6  3100010
       4469 -0.02715 0.00074 w           b2 :  3  3100010
         11 -0.02568 0.00066 z*                    103010
       4546  0.02464 0.00061 w  b1 :  5  b2 :  5 12100001
       4317 -0.02464 0.00061 w  a1 :  9  b2 :  5 12100100
       5384 -0.02423 0.00059 w           a1 :  9  3000101
         27  0.02417 0.00058 z*                    001013
       5452 -0.02404 0.00058 w           b1 :  5  3000101
          5  0.02380 0.00057 z*                    121010
         25 -0.02374 0.00056 z*                    003101
       4198  0.02347 0.00055 w  a1 :  9  b1 :  5 12101000
       5160  0.02289 0.00052 w           b1 :  5  3001010
       5188  0.02271 0.00052 w           b2 :  5  3001010
        260 -0.02169 0.00047 y           a1 : 10  1100201
       5480 -0.02143 0.00046 w           b2 :  5  3000101
       5092  0.02073 0.00043 w           a1 :  9  3001010
        215  0.02064 0.00043 y           a1 : 10  1102010
        746 -0.01870 0.00035 x  a2 :  3  b2 :  6 11300000
       5389 -0.01724 0.00030 w           a1 : 10  3000101
       4378  0.01713 0.00029 w           a1 :  9  3100010
          4 -0.01708 0.00029 z*                    130010
       1011 -0.01707 0.00029 x  a1 : 10  b2 :  6 11200100
       1166  0.01707 0.00029 x  a1 : 10  a2 :  3 11200001
       4474 -0.01690 0.00029 w           b2 :  5  3100010
       5097  0.01640 0.00027 w           a1 : 10  3001010
        892  0.01625 0.00026 x  a1 : 10  b1 :  6 11201000
       4875 -0.01617 0.00026 w           b2 :  3  3010010
         13 -0.01594 0.00025 z*                    101201
        120 -0.01571 0.00025 y           a1 : 10  1200101
        697 -0.01569 0.00025 x  a1 : 11  b1 :  6 11300000
        274  0.01534 0.00024 y           a1 : 10  1100102
        103  0.01495 0.00022 y           a1 : 10  1201010
          9  0.01491 0.00022 z*                    110201
        734 -0.01480 0.00022 x  a1 : 15  b1 :  9 11300000
        722 -0.01476 0.00022 x  a1 : 14  b1 :  8 11300000
        989  0.01472 0.00022 x  a1 : 10  b2 :  4 11200100
       1155 -0.01472 0.00022 x  a1 : 10  a2 :  2 11200001
       1032 -0.01468 0.00022 x  a1 :  9  b2 :  8 11200100
        236 -0.01460 0.00021 y           a1 : 10  1101020
       1065  0.01433 0.00021 x  a1 : 10  a1 : 11 11200010
       1029 -0.01413 0.00020 x  a1 :  6  b2 :  8 11200100
       4878 -0.01409 0.00020 w  b2 :  3  b2 :  5 12010010
        870 -0.01401 0.00020 x  a1 : 10  b1 :  4 11201000
       1204  0.01392 0.00019 x  b1 :  9  b2 :  5 11200001
        756  0.01388 0.00019 x  a2 :  4  b2 :  9 11300000
       1228  0.01362 0.00019 x  b1 :  5  b2 :  9 11200001
        913  0.01362 0.00019 x  a1 :  9  b1 :  8 11201000
       1190  0.01336 0.00018 x  b1 :  9  b2 :  3 11200001
       1058 -0.01313 0.00017 x  a1 :  7  a1 : 10 11200010
        910  0.01309 0.00017 x  a1 :  6  b1 :  8 11201000
       1226  0.01307 0.00017 x  b1 :  3  b2 :  9 11200001
       4350  0.01286 0.00017 w  a1 :  9  b2 :  8 12100100
       1005  0.01274 0.00016 x  a1 : 15  b2 :  5 11200100
          6 -0.01274 0.00016 z*                    120101
       4847 -0.01246 0.00016 w           b1 :  3  3010010
         19  0.01239 0.00015 z*                    013010
        983  0.01223 0.00015 x  a1 : 15  b2 :  3 11200100
       1142  0.01220 0.00015 x  b2 :  5  b2 :  8 11200010
       4347  0.01213 0.00015 w  a1 :  6  b2 :  8 12100100
        627  0.01188 0.00014 y           b1 :  6  1000121
       1140  0.01179 0.00014 x  b2 :  3  b2 :  8 11200010
       4486 -0.01162 0.00013 w  b2 :  5  b2 :  8 12100010
        961  0.01157 0.00013 x  a2 :  4  b1 :  5 11200100
        541 -0.01146 0.00013 y           a2 :  3  1001210
       1176  0.01132 0.00013 x  a1 :  9  a2 :  4 11200001
        885  0.01112 0.00012 x  a1 : 14  b1 :  5 11201000
       4574 -0.01107 0.00012 w  b1 :  5  b2 :  9 12100001
        955  0.01106 0.00012 x  a2 :  4  b1 :  3 11200100
        350 -0.01103 0.00012 y           a1 : 12  1021010
       4484 -0.01101 0.00012 w  b2 :  3  b2 :  8 12100010
       1173  0.01091 0.00012 x  a1 :  6  a2 :  4 11200001
        745  0.01069 0.00011 x  a2 :  2  b2 :  6 11300000
        740  0.01069 0.00011 x  a2 :  3  b2 :  4 11300000
        863  0.01069 0.00011 x  a1 : 14  b1 :  3 11201000
        940 -0.01065 0.00011 x  a2 :  4  b2 :  5 11201000
       4850 -0.01058 0.00011 w  b1 :  3  b1 :  5 12010010
       4572 -0.01040 0.00011 w  b1 :  3  b2 :  9 12100001
       1082  0.01037 0.00011 x  a1 :  9  a1 : 14 11200010
       4231 -0.01037 0.00011 w  a1 :  9  b1 :  8 12101000
        934 -0.01025 0.00010 x  a2 :  4  b2 :  3 11201000
       4550  0.01014 0.00010 w  b1 :  9  b2 :  5 12100001
       4279  0.01006 0.00010 w  a2 :  4  b1 :  5 12100100
        366  0.01001 0.00010 y           a1 : 11  1020101
          119 csfs were printed in this range.
