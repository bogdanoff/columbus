1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      2009-03.    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


================ Computing sorting integral file structure ================

                    -----z----- -----y----- -----x----- -----w----- ---total---

                CSFs        28         630        3416        1526        5600
      internal walks       105          90          36          15         246
valid internal walks        28          90          36          15         169
 getinfoarray: info=                     6 :                     1
                  8192                  6552                  8192
                  5460                     0
 icd(3)=                  2976 ci%nnlev=                   595  l2rec=
                  8192  n2max=                  5460
 lcore1,lcore2=              13068137              13044821
 lencor,maxblo              13107200                 60000
========================================
 current settings:
 minbl3         381
 minbl4         381
 locmaxbl3     4288
 locmaxbuf     2144
 maxbl3       60000
 maxbl3       60000
 maxbl4       60000
 maxbuf       30006
========================================

 sorted 4-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)

 sorted 3-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)
 Orig.  diagonal integrals:  1electron:        34
                             0ext.    :        42
                             2ext.    :       336
                             4ext.    :       812


 Orig. off-diag. integrals:  4ext.    :     24411
                             3ext.    :     18720
                             2ext.    :      6792
                             1ext.    :      1272
                             0ext.    :        99
                             2ext. SO :         0
                             1ext. SO :         0
                             0ext. SO :         0
                             1electron:       128


 Sorted integrals            3ext.  w :     17370 x :     16020
                             4ext.  w :     20955 x :     17777


Cycle #  1 sortfile size=    163835(       5 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13068137
Cycle #  2 sortfile size=    163835(       5 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13068137
 minimum size of srtscr:     98301 WP (     3 records)
 maximum size of srtscr:    163835 WP (     5 records)
diagi   file:      4 records  of   1536 WP each=>       6144 WP total
ofdgi   file:      8 records  of   1536 WP each=>      12288 WP total
fil3w   file:      1 records  of  30006 WP each=>      30006 WP total
fil3x   file:      1 records  of  30006 WP each=>      30006 WP total
fil4w   file:      1 records  of  30006 WP each=>      30006 WP total
fil4x   file:      1 records  of  30006 WP each=>      30006 WP total
 compressed index vector length=                    75
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 3,
  GSET = 3,
   DAVCOR =10,
  NCOREL = 4
   nroot=1
  IVMODE = 3
  NBKITR = 1
   nvbkmn=2
  RTOLBK = 1e-3
   niter=30
   nvcimn=2
   rtolci=1e-3
  NVCIMX = 6
  NVRFMX = 6
  NVBKMX = 6
  IDEN  = 1
  CSFPRN = 10,
  update_mode=1
  nvrfmn=2
   lrtshift=-0.0465482970
 /&end
 ------------------------------------------------------------------------
lodens (list->root)=  1
invlodens (root->list)=  1
 LRT calculation: diagonal shift= -4.654829700000000E-002
 bummer (warning):2:changed keyword: nbkitr=                      0
 USING SEGMENTS OF EQUAL SIZE

****************  list of control variables  ****************
 lvlprt =    0      nroot  =    1      noldv  =   0      noldhv =   0
 nunitv =    1      nbkitr =    0      niter  =  30      davcor =  10
 csfprn =   10      ivmode =    3      istrt  =   0      vout   =   0
 iortls =    0      nvbkmx =    6      ibktv  =  -1      ibkthv =  -1
 nvcimx =    6      icitv  =   -1      icithv =  -1      frcsub =   0
 nvbkmn =    2      nvcimn =    2      maxseg =   4      nrfitr =  30
 ncorel =    4      nvrfmx =    6      nvrfmn =   2      iden   =   1
 itran  =    0      froot  =    0      rtmode =   0      ncouple=   1
 skipso =    F      dalton2=    0      molcas =   0      finalv =   0
 finalw =    0      cosmocalc=   0    with_tsklst=   0
 nsegwx =    1     1     1     1
 nseg0x =    1     1     1     1
 nseg1x =    1     1     1     1
 nseg2x =    1     1     1     1
 nseg3x =    1     1     1     1
 nseg4x =    1     1     1     1
 no0ex  =      0    no1ex  =      0    no2ex  =     0    no3ex  =     0
 no4ex  =      0    nodiag =      0
 cdg4ex =    1      c3ex1ex=    1      c2ex0ex=   1
 fileloc=    0     0     0     0     0     0     0     1     1     1
 directhd=   1      noaqccshift_zyxw=      0
 critical_crit=-1.00000    critical_delta= 0.05000

 ctol   = 0.010000    lrtshift=-.046548    smalld =0.001000


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-03    1.000E-03
 Computing density:                    .drt1.state1
 using                      1  nodes and                      1  cores.
 szdg/szodg per processor=                  6144                 12288
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core           13107199 DP per process

********** Integral sort section *************


 workspace allocation information: lencor=  13107199

 echo of the input for program cisrt:
 ------------------------------------------------------------------------
  &input
  maxbl3=60000
  maxbl4=60000
  &end
 ------------------------------------------------------------------------
 
 ( 6) listing file:                    ciudgls             
 ( 5) input file:                      cisrtin   
 (17) cidrt file:                      cidrtfl             
 (11) transformed integrals file:      moints    
 (12) diagonal integral file:          diagint             
 (13) off-diagonal integral file:      ofdgint             
 (31) 4-external w integrals file:     fil4w               
 (32) 4-external x integrals file:     fil4x               
 (33) 3-external w integrals file:     fil3w               
 (34) 3-external x integrals file:     fil3x               
 (21) scratch da sorting file:         srtscr              
 (12) 2-e integral file [fsplit=2]:    moints2   

 input integral file header information:
                                                                                 
 aoints SIFS file created by argos.      zam792            17:56:38.564 17-Dec-13
  cidrt_title                                                                    
 MO-coefficients from mcscf.x                                                    
  with dummy occupation 1.0 for active orbitals                                  
  total ao core energy =    0.000000000                                          
 MCSCF energy =     -46.301878576                                                
 SIFS file created by program tran.      zam792            17:56:38.888 17-Dec-13

 input energy(*) values:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.

 total core energy =   0.000000000000E+00

 nsym = 4 nmot=  34

 symmetry  =    1    2    3    4
 slabel(*) =   a1   a2   b1   b2
 nmpsy(*)  =   14    4    8    8

 info(*) =          1      8192      6552      8192      5460         0

 orbital labels, i:molab(i)=
   1:tout:001   2:tout:002   3:tout:003   4:tout:004   5:tout:005   6:tout:006   7:tout:007   8:tout:008   9:tout:009  10:tout:010
  11:tout:011  12:tout:012  13:tout:013  14:tout:014  15:tout:015  16:tout:016  17:tout:017  18:tout:018  19:tout:019  20:tout:020
  21:tout:021  22:tout:022  23:tout:023  24:tout:024  25:tout:025  26:tout:026  27:tout:027  28:tout:028  29:tout:029  30:tout:030
  31:tout:031  32:tout:032  33:tout:033  34:tout:034

 input parameters:
 prnopt=  0
 ldamin=    4095 ldamax=   32767 ldainc=      64
 maxbuf=   30006 maxbl3=   60000 maxbl4=   60000 intmxo=    1536
  Using 32 bit compression 

 drt information:
  cidrt_title                                                                    
 nmotd =  38 nfctd =   4 nfvtc =   0 nmot  =  34
 nlevel =  34 niot  =   6 lowinl=  29
 orbital-to-level map(*)
   -1  -1  29  30  31   1   2   3   4   5   6   7   8   9  10  11  32  12  13  14
   -1  33  15  16  17  18  19  20  21  -1  34  22  23  24  25  26  27  28
 compressed map(*)
   29  30  31   1   2   3   4   5   6   7   8   9  10  11  32  12  13  14  33  15
   16  17  18  19  20  21  34  22  23  24  25  26  27  28
 levsym(*)
    1   1   1   1   1   1   1   1   1   1   1   2   2   2   3   3   3   3   3   3
    3   4   4   4   4   4   4   4   1   1   1   2   3   4
 repartitioning mu(*)=
   0.  0.  0.  0.  0.  0.

 new core energy added to the energy(*) list.
 from the integral file: h1_core= -4.349858765627E+01

 indxdg: diagonal integral statistics.
 total number of integrals contributing to diagonal matrix elements:      1190
 number with all external indices:       812
 number with half external - half internal indices:       336
 number with all internal indices:        42

 indxof: off-diagonal integral statistics.
    4-external integrals: num=      24411 strt=          1
    3-external integrals: num=      18720 strt=      24412
    2-external integrals: num=       6792 strt=      43132
    1-external integrals: num=       1272 strt=      49924
    0-external integrals: num=         99 strt=      51196

 total number of off-diagonal integrals:       51294


 indxof(2nd)  ittp=   3 numx(ittp)=        6792
 indxof(2nd)  ittp=   4 numx(ittp)=        1272
 indxof(2nd)  ittp=   5 numx(ittp)=          99

 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg=  12931980
 pro2e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652

 pro2e:     25126 integrals read in     5 records.

 pro2e:         0 integrals 34-ext integrals skipped.
 pro1e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652
 pro1e: eref =    0.000000000000000E+00
 total size of srtscr:                     4  records of                  32767 
 WP =               1048544 Bytes
 putdg        1     596    1191    1786    3322   36089   57934    3018   46706   90394
   123161  131353  136813  158652

 putf:       4 buffers of length    1536 written to file 12
 diagonal integral file completed.

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep4e:    17 blocks of linear combinations of 4-external integrals processed.
 number of sorted 4-external integrals      38732
 number of original 4-external integrals    24411


 putf34: external integral file complete. nfilw=    31 nfilx=    32 nrecw=     1 nrecx=     1 lbufp= 30006

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep3e:    19 blocks of linear combinations of 3-external integrals processed.
 number of sorted 3-external integrals      33390
 number of original 3-external integrals    18720


 putf34: external integral file complete. nfilw=    33 nfilx=    34 nrecw=     1 nrecx=     1 lbufp= 30006
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      6792         3     43132     43132      6792     51294
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      1272         4     49924     49924      1272     51294
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd        99         5     51196     51196        99     51294

 putf:       8 buffers of length    1536 written to file 13
 off-diagonal files sort completed.
 executing brd_struct for cisrtinfo
cisrtinfo:
bufszi  1536
 diagfile 4ext:     812 2ext:     336 0ext:      42
 fil4w,fil4x  :   24411 fil3w,fil3x :   18720
 ofdgint  2ext:    6792 1ext:    1272 0ext:      99so0ext:       0so1ext:       0so2ext:       0
buffer minbl4     381 minbl3     381 maxbl2     384nbas:  11   3   7   7   0   0   0   0 maxbuf 30006
 CIUDG version 5.9.7 ( 5-Oct-2004)

 workspace allocation information: lcore=  13107199

 core energy values from the integral file:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -4.349858765627E+01, ietype=    6,   fcore energy of type: H1(*)   

 total core repulsion energy = -4.349858765627E+01
 nmot  =    38 niot  =     6 nfct  =     4 nfvt  =     0
 nrow  =    36 nsym  =     4 ssym  =     3 lenbuf=  1600
 nwalk,xbar:        246      105       90       36       15
 nvalwt,nvalw:      169       28       90       36       15
 ncsft:            5600
 total number of valid internal walks:     169
 nvalz,nvaly,nvalx,nvalw =       28      90      36      15

 cisrt info file parameters:
 file number  12 blocksize   1536
 mxbld   1536
 nd4ext,nd2ext,nd0ext   812   336    42
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int    24411    18720     6792     1272       99        0        0        0
 minbl4,minbl3,maxbl2   381   381   384
 maxbuf 30006
 number of external orbitals per symmetry block:  11   3   7   7
 nmsym   4 number of internal orbitals   6
 executing brd_struct for drt
 executing brd_struct for orbinf
 executing brd_struct for momap
 calcthrxt: niot,maxw1=                     6                    66
 block size     0
 pthz,pthy,pthx,pthw:   105    90    36    15 total internal walks:     246
 maxlp3,n2lp,n1lp,n0lp    66     0     0     0
 orbsym(*)= 1 1 1 2 3 4

 setref:       28 references kept,
               77 references were marked as invalid, out of
              105 total.
 nmb.of records onel     1
 nmb.of records 2-ext     5
 nmb.of records 1-ext     1
 nmb.of records 0-ext     1
 nmb.of records 2-int     0
 nmb.of records 1-int     0
 nmb.of records 0-int     0
 ---------memory usage in DP -----------------
 < n-ex core usage >
     routines:
    fourex            61797
    threx             60409
    twoex              3549
    onex               1823
    allin              1536
    diagon             2109
               =======
   maximum            61797
 
  __ static summary __ 
   reflst                28
   hrfspc                28
               -------
   static->              56
 
  __ core required  __ 
   totstc                56
   max n-ex           61797
               -------
   totnec->           61853
 
  __ core available __ 
   totspc          13107199
   totnec -           61853
               -------
   totvec->        13045346

 number of external paths / symmetry
 vertex x     100      82      98      98
 vertex w     128      82      98      98
segment: free space=    13045346
 reducing frespc by                   946 to               13044400 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1          55|        28|         0|        28|         0|         1|
 -------------------------------------------------------------------------------
  Y 2          90|       630|        28|        90|        28|         2|
 -------------------------------------------------------------------------------
  X 3          36|      3416|       658|        36|       118|         3|
 -------------------------------------------------------------------------------
  W 4          15|      1526|      4074|        15|       154|         4|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=          76DP  conft+indsym=         360DP  drtbuffer=         510 DP

dimension of the ci-matrix ->>>      5600

 executing brd_struct for civct
 gentasklist: ntask=                    20
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS   
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      36      55       3416         28      36      28
     2  4   1    25      two-ext wz   2X  4 1      15      55       1526         28      15      28
     3  4   3    26      two-ext wx*  WX  4 3      15      36       1526       3416      15      36
     4  4   3    27      two-ext wx+  WX  4 3      15      36       1526       3416      15      36
     5  2   1    11      one-ext yz   1X  2 1      90      55        630         28      90      28
     6  3   2    15      1ex3ex yx    3X  3 2      36      90       3416        630      36      90
     7  4   2    16      1ex3ex yw    3X  4 2      15      90       1526        630      15      90
     8  1   1     1      allint zz    OX  1 1      55      55         28         28      28      28
     9  2   2     5      0ex2ex yy    OX  2 2      90      90        630        630      90      90
    10  3   3     6      0ex2ex xx*   OX  3 3      36      36       3416       3416      36      36
    11  3   3    18      0ex2ex xx+   OX  3 3      36      36       3416       3416      36      36
    12  4   4     7      0ex2ex ww*   OX  4 4      15      15       1526       1526      15      15
    13  4   4    19      0ex2ex ww+   OX  4 4      15      15       1526       1526      15      15
    14  2   2    42      four-ext y   4X  2 2      90      90        630        630      90      90
    15  3   3    43      four-ext x   4X  3 3      36      36       3416       3416      36      36
    16  4   4    44      four-ext w   4X  4 4      15      15       1526       1526      15      15
    17  1   1    75      dg-024ext z  OX  1 1      55      55         28         28      28      28
    18  2   2    76      dg-024ext y  OX  2 2      90      90        630        630      90      90
    19  3   3    77      dg-024ext x  OX  3 3      36      36       3416       3416      36      36
    20  4   4    78      dg-024ext w  OX  4 4      15      15       1526       1526      15      15
----------------------------------------------------------------------------------------------------
REDTASK #   1 TIME=  19.000 N=  1 (task/type/sgbra)=(   1/24/0) (
REDTASK #   2 TIME=  18.000 N=  1 (task/type/sgbra)=(   2/25/0) (
REDTASK #   3 TIME=  17.000 N=  1 (task/type/sgbra)=(   3/26/1) (
REDTASK #   4 TIME=  16.000 N=  1 (task/type/sgbra)=(   4/27/2) (
REDTASK #   5 TIME=  15.000 N=  1 (task/type/sgbra)=(   5/11/0) (
REDTASK #   6 TIME=  14.000 N=  1 (task/type/sgbra)=(   6/15/0) (
REDTASK #   7 TIME=  13.000 N=  1 (task/type/sgbra)=(   7/16/0) (
REDTASK #   8 TIME=  12.000 N=  1 (task/type/sgbra)=(   8/ 1/0) (
REDTASK #   9 TIME=  11.000 N=  1 (task/type/sgbra)=(   9/ 5/0) (
REDTASK #  10 TIME=  10.000 N=  1 (task/type/sgbra)=(  10/ 6/1) (
REDTASK #  11 TIME=   9.000 N=  1 (task/type/sgbra)=(  11/18/2) (
REDTASK #  12 TIME=   8.000 N=  1 (task/type/sgbra)=(  12/ 7/1) (
REDTASK #  13 TIME=   7.000 N=  1 (task/type/sgbra)=(  13/19/2) (
REDTASK #  14 TIME=   6.000 N=  1 (task/type/sgbra)=(  14/42/1) (
REDTASK #  15 TIME=   5.000 N=  1 (task/type/sgbra)=(  15/43/1) (
REDTASK #  16 TIME=   4.000 N=  1 (task/type/sgbra)=(  16/44/1) (
REDTASK #  17 TIME=   3.000 N=  1 (task/type/sgbra)=(  17/75/1) (
REDTASK #  18 TIME=   2.000 N=  1 (task/type/sgbra)=(  18/76/1) (
REDTASK #  19 TIME=   1.000 N=  1 (task/type/sgbra)=(  19/77/1) (
REDTASK #  20 TIME=   0.000 N=  1 (task/type/sgbra)=(  20/78/1) (
 initializing v-file: 1:                  5600

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         216 2x:           0 4x:           0
All internal counts: zz :         356 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 reference space has dimension      28
 dsyevx: computed roots 1 to    2(converged:   2)

    root           eigenvalues
    ----           ------------
       1         -46.3018785769
       2         -46.3018785739

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt=                    28

         vector  1 from unit 11 written to unit 49 filename cirefv              
 ncorel,neli=                     4                     4
  This is a  mraqcc energy evaluation      

    ------------------------------------------------------------

 setopt2: mraqcc energy evaluation      gvalue=  0.833333333333333      ncorel=
                     4

    ------------------------------------------------------------


    reference energies:

  reference state used is:   1with energy      -46.3018785769

  ### begin active excitation selection ###

 inactive(1:nintern)=AAAAAA
 There are         28 reference CSFs out of         28 valid zwalks.
 There are    0 inactive and    6 active orbitals.
      28 active-active  and        0 inactive excitations.

  ### end active excitation selection ###


 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              5600
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                   30
 residual norm convergence criteria:               0.001000

          starting ci iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         807 2x:         336 4x:         141
All internal counts: zz :         356 yy:         930 xx:         352 ww:          66
One-external counts: yz :        1495 yx:        2405 yw:        1220
Two-external counts: yy :        1285 ww:         180 xx:         682 xz:         157 wz:         100 wx:         510
Three-ext.   counts: yx :         657 yw:         358

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1
   ht   1    -2.80329092
calca4: root=   1 anorm**2=  0.00000000 scale:  1.00000000

          calcsovref: tciref block   1

              civs   1
 refs   1    1.00000    

          calcsovref: scrb block   1

                ci   1
 civs   1    1.00000    

          calcsovref: sovref block   1

              v      1
 ref    1    1.00000    

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  1  1    -46.3018785769 -1.3323E-15  5.2520E-02  2.1139E-01  1.0000E-03
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000999
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   2

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         807 2x:         336 4x:         141
All internal counts: zz :         356 yy:         930 xx:         352 ww:          66
One-external counts: yz :        1495 yx:        2405 yw:        1220
Two-external counts: yy :        1285 ww:         180 xx:         682 xz:         157 wz:         100 wx:         510
Three-ext.   counts: yx :         657 yw:         358

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2
   ht   1    -2.80329092
   ht   2    -0.05252009    -0.18650662
calca4: root=   1 anorm**2=  0.05824660 scale:  1.02871114
calca4: root=   2 anorm**2=  0.94175340 scale:  1.39346812

          calcsovref: tciref block   1

              civs   1       civs   2
 refs   1    1.00000      -3.377880E-14

          calcsovref: scrb block   1

                ci   1         ci   2
 civs   1   0.970440      -0.241343    
 civs   2   0.816222        3.28202    

          calcsovref: sovref block   1

              v      1       v      2
 ref    1   0.970440      -0.241343    

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.97043980    -0.24134333
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  2  1    -46.3460524430  4.4174E-02  1.9116E-03  4.0445E-02  1.0000E-03
 mraqcc  #  2  2    -45.5876585665  2.0891E+00  0.0000E+00  4.6796E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   3

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         807 2x:         336 4x:         141
All internal counts: zz :         356 yy:         930 xx:         352 ww:          66
One-external counts: yz :        1495 yx:        2405 yw:        1220
Two-external counts: yy :        1285 ww:         180 xx:         682 xz:         157 wz:         100 wx:         510
Three-ext.   counts: yx :         657 yw:         358

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3
   ht   1    -2.80329092
   ht   2    -0.05252009    -0.18650662
   ht   3     0.00143999    -0.00240865    -0.00674854
calca4: root=   1 anorm**2=  0.06685379 scale:  1.03288614
calca4: root=   2 anorm**2=  0.94531588 scale:  1.39474581
calca4: root=   3 anorm**2=  0.95713506 scale:  1.39897643

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3
 refs   1    1.00000      -3.377880E-14  -5.131692E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3
 civs   1   0.966499      -0.171377       0.191305    
 civs   2   0.850965        1.63263       -2.83696    
 civs   3    1.09300        15.6120        9.35562    

          calcsovref: sovref block   1

              v      1       v      2       v      3
 ref    1   0.965938      -0.179389       0.186504    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.96593784    -0.17938898     0.18650383
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  3  1    -46.3481457353  2.0933E-03  2.5892E-04  1.4528E-02  1.0000E-03
 mraqcc  #  3  2    -45.8240141940  2.3636E-01  0.0000E+00  4.0601E-01  1.0000E-04
 mraqcc  #  3  3    -45.5029104488  2.0043E+00  0.0000E+00  4.7791E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   4

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         807 2x:         336 4x:         141
All internal counts: zz :         356 yy:         930 xx:         352 ww:          66
One-external counts: yz :        1495 yx:        2405 yw:        1220
Two-external counts: yy :        1285 ww:         180 xx:         682 xz:         157 wz:         100 wx:         510
Three-ext.   counts: yx :         657 yw:         358

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4
   ht   1    -2.80329092
   ht   2    -0.05252009    -0.18650662
   ht   3     0.00143999    -0.00240865    -0.00674854
   ht   4    -0.00296170    -0.00241527    -0.00050021    -0.00095436
calca4: root=   1 anorm**2=  0.07024042 scale:  1.03452425
calca4: root=   2 anorm**2=  0.95741470 scale:  1.39907637
calca4: root=   3 anorm**2=  0.94817330 scale:  1.39576979
calca4: root=   4 anorm**2=  0.76405748 scale:  1.32817826

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4
 refs   1    1.00000      -3.377880E-14  -5.131692E-04   1.069925E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4
 civs   1   0.963866       0.181210       0.198844      -3.741047E-02
 civs   2   0.854545      -0.993412       -3.14650      -0.140730    
 civs   3    1.21765       -12.8150        4.74658       -12.1003    
 civs   4   0.928132       -25.6242        13.4496        38.3945    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4
 ref    1   0.964234       0.160371       0.210799       9.878232E-03

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.96423420     0.16037058     0.21079873     0.00987823
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  4  1    -46.3483861013  2.4037E-04  4.4832E-05  6.3332E-03  1.0000E-03
 mraqcc  #  4  2    -45.9826672246  1.5865E-01  0.0000E+00  2.2259E-01  1.0000E-04
 mraqcc  #  4  3    -45.5155035216  1.2593E-02  0.0000E+00  4.8118E-01  1.0000E-04
 mraqcc  #  4  4    -45.4092197035  1.9106E+00  0.0000E+00  5.5020E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   5

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         807 2x:         336 4x:         141
All internal counts: zz :         356 yy:         930 xx:         352 ww:          66
One-external counts: yz :        1495 yx:        2405 yw:        1220
Two-external counts: yy :        1285 ww:         180 xx:         682 xz:         157 wz:         100 wx:         510
Three-ext.   counts: yx :         657 yw:         358

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1    -2.80329092
   ht   2    -0.05252009    -0.18650662
   ht   3     0.00143999    -0.00240865    -0.00674854
   ht   4    -0.00296170    -0.00241527    -0.00050021    -0.00095436
   ht   5     0.00270982    -0.00041380    -0.00028892     0.00002275    -0.00016599
calca4: root=   1 anorm**2=  0.07123030 scale:  1.03500256
calca4: root=   2 anorm**2=  0.97114249 scale:  1.40397382
calca4: root=   3 anorm**2=  0.90366632 scale:  1.37973415
calca4: root=   4 anorm**2=  0.51014336 scale:  1.22887890
calca4: root=   5 anorm**2=  0.94805936 scale:  1.39572897

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1    1.00000      -3.377880E-14  -5.131692E-04   1.069925E-03  -9.639999E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 civs   1  -0.963994       0.137689      -0.250764       4.557394E-02  -3.310994E-03
 civs   2  -0.854982      -0.764765        3.06887       0.990977       4.194732E-02
 civs   3   -1.23685       -10.9680       -1.07810       -3.84592       -14.6294    
 civs   4   -1.07179       -26.6216       -8.02918       -21.8863        33.4396    
 civs   5  -0.830535       -38.3971       -49.7379        88.5818        42.2178    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1  -0.963706       0.151849      -0.210854      -6.126197E-02  -7.236905E-04

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1    -0.96370587     0.15184925    -0.21085363    -0.06126197    -0.00072369
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  5  1    -46.3484233372  3.7236E-05  3.4812E-06  1.7164E-03  1.0000E-03
 mraqcc  #  5  2    -46.0459485944  6.3281E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  5  3    -45.5342790051  1.8775E-02  0.0000E+00  4.8638E-01  1.0000E-04
 mraqcc  #  5  4    -45.4495511088  4.0331E-02  0.0000E+00  6.4890E-01  1.0000E-04
 mraqcc  #  5  5    -45.4009930231  1.9024E+00  0.0000E+00  5.3172E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   6

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         807 2x:         336 4x:         141
All internal counts: zz :         356 yy:         930 xx:         352 ww:          66
One-external counts: yz :        1495 yx:        2405 yw:        1220
Two-external counts: yy :        1285 ww:         180 xx:         682 xz:         157 wz:         100 wx:         510
Three-ext.   counts: yx :         657 yw:         358

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1    -2.80329092
   ht   2    -0.05252009    -0.18650662
   ht   3     0.00143999    -0.00240865    -0.00674854
   ht   4    -0.00296170    -0.00241527    -0.00050021    -0.00095436
   ht   5     0.00270982    -0.00041380    -0.00028892     0.00002275    -0.00016599
   ht   6     0.00064756    -0.00010619     0.00002093     0.00001936     0.00000075    -0.00001433
calca4: root=   1 anorm**2=  0.07129886 scale:  1.03503568
calca4: root=   2 anorm**2=  0.97178437 scale:  1.40420240
calca4: root=   3 anorm**2=  0.91079238 scale:  1.38231414
calca4: root=   4 anorm**2=  0.94423568 scale:  1.39435852
calca4: root=   5 anorm**2=  0.40157582 scale:  1.18388168
calca4: root=   6 anorm**2=  0.96159056 scale:  1.40056794

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1    1.00000      -3.377880E-14  -5.131692E-04   1.069925E-03  -9.639999E-04  -2.303224E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1  -0.963785       0.138241      -7.624175E-02  -0.219021      -9.673098E-02  -9.500493E-02
 civs   2  -0.855000      -0.596643        1.19055        2.94852      -0.347925       0.674273    
 civs   3   -1.23869       -9.55272        6.92468       -7.53445      -0.926704        12.4232    
 civs   4   -1.08566       -25.1716        4.96838       -5.54525        27.0242       -31.4170    
 civs   5  -0.910742       -44.7480       -29.1966       -2.99812       -80.4937       -65.1900    
 civs   6    1.03309        118.282        336.409       -117.795       -49.6179       -140.680    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.963671       0.132106      -0.123816      -0.191066       2.168242E-02  -3.974921E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.96367059     0.13210594    -0.12381641    -0.19106642     0.02168242    -0.03974921

 trial vector basis is being transformed.  new dimension:   2
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  6  1    -46.3484269336  3.5964E-06  3.0574E-07  4.9783E-04  1.0000E-03
 mraqcc  #  6  2    -46.0727703000  2.6822E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  6  3    -45.8018586840  2.6758E-01  0.0000E+00  3.9845E-01  1.0000E-04
 mraqcc  #  6  4    -45.4889597279  3.9409E-02  0.0000E+00  4.7422E-01  1.0000E-04
 mraqcc  #  6  5    -45.4442874769  4.3294E-02  0.0000E+00  6.5493E-01  1.0000E-04
 mraqcc  #  6  6    -45.3627681947  1.8642E+00  0.0000E+00  5.5118E-01  1.0000E-04
 
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mraqcc   convergence criteria satisfied after  6 iterations.

 final mraqcc   convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  6  1    -46.3484269336  3.5964E-06  3.0574E-07  4.9783E-04  1.0000E-03
 mraqcc  #  6  2    -46.0727703000  2.6822E-02  0.0000E+00  0.0000E+00  1.0000E-04

####################CIUDGINFO####################

   aqcc(lrt) vector at position   1 energy=  -46.348426933649

################END OF CIUDGINFO################

 
 a4den factor for root 1  =  1.012026050
diagon:itrnv=   0
    1 of the   3 expansion vectors are transformed.
    1 of the   2 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
maximum overlap with reference    1(overlap= 0.96367)
reference energy =                         -46.3018785769
total aqcc energy =                        -46.3484269336
diagonal element shift (lrtshift) =         -0.0465483567

 information on vector: 1 from unit 11 written to unit 48 filename civout              


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -46.3484269336

                                                       internal orbitals

                                          level       1    2    3    4    5    6

                                          orbital     3    4    5   17   22   31

                                         symmetry   a1   a1   a1   a2   b1   b2 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  3  1       1  0.152075                        +-   +              +       
 z*  3  1       2 -0.648946                        +-        +         +       
 z*  3  1       3  0.680902                        +-             +         +  
 z*  3  1       4 -0.017080                        +    +-             +       
 z*  3  1       5  0.023798                        +     -   +         +       
 z*  3  1       6 -0.012741                        +     -        +         +  
 z*  3  1       7  0.057285                        +    +     -        +       
 z*  3  1       8  0.030065                        +    +    +          -      
 z*  3  1       9  0.014914                        +    +          -        +  
 z*  3  1      10  0.045369                        +    +         +          - 
 z*  3  1      11 -0.025679                        +         +-        +       
 z*  3  1      12 -0.034866                        +          -   +         +  
 z*  3  1      13 -0.015935                        +         +     -        +  
 z*  3  1      14  0.060982                        +         +    +          - 
 z*  3  1      16 -0.043497                        +                   +    +- 
 z*  3  1      17  0.030640                             +-   +         +       
 z*  3  1      18 -0.033890                             +-        +         +  
 z*  3  1      19  0.012389                             +    +-        +       
 z*  3  1      25 -0.023742                                  +-   +         +  
 z*  3  1      26 -0.031686                                  +    +-   +       
 z*  3  1      27  0.024169                                  +         +    +- 
 z*  3  1      28  0.033246                                       +    +-   +  
 y   3  1     103  0.014948              5( a1 )    -        +         +       
 y   3  1     120 -0.015706              5( a1 )    -             +         +  
 y   3  1     215  0.020645              5( a1 )   +          -        +       
 y   3  1     236 -0.014601              5( a1 )   +         +          -      
 y   3  1     260 -0.021690              5( a1 )   +               -        +  
 y   3  1     274  0.015342              5( a1 )   +              +          - 
 y   3  1     350 -0.011025              7( a1 )         -   +         +       
 y   3  1     366  0.010006              6( a1 )         -        +         +  
 y   3  1     541 -0.011456              2( a2 )             +     -   +       
 y   3  1     627  0.011882              4( b1 )                  +     -   +  
 x   3  1     697 -0.015694    6( a1 )   4( b1 )   +-                          
 x   3  1     722 -0.014759    9( a1 )   6( b1 )   +-                          
 x   3  1     734 -0.014799   10( a1 )   7( b1 )   +-                          
 x   3  1     740  0.010691    2( a2 )   2( b2 )   +-                          
 x   3  1     745  0.010691    1( a2 )   4( b2 )   +-                          
 x   3  1     746 -0.018705    2( a2 )   4( b2 )   +-                          
 x   3  1     756  0.013877    3( a2 )   7( b2 )   +-                          
 x   3  1     863  0.010688    9( a1 )   1( b1 )    -        +                 
 x   3  1     870 -0.014014    5( a1 )   2( b1 )    -        +                 
 x   3  1     885  0.011122    9( a1 )   3( b1 )    -        +                 
 x   3  1     892  0.016246    5( a1 )   4( b1 )    -        +                 
 x   3  1     910  0.013087    1( a1 )   6( b1 )    -        +                 
 x   3  1     913  0.013620    4( a1 )   6( b1 )    -        +                 
 x   3  1     934 -0.010247    3( a2 )   1( b2 )    -        +                 
 x   3  1     940 -0.010651    3( a2 )   3( b2 )    -        +                 
 x   3  1     955  0.011061    3( a2 )   1( b1 )    -             +            
 x   3  1     961  0.011570    3( a2 )   3( b1 )    -             +            
 x   3  1     983  0.012232   10( a1 )   1( b2 )    -             +            
 x   3  1     989  0.014721    5( a1 )   2( b2 )    -             +            
 x   3  1    1005  0.012745   10( a1 )   3( b2 )    -             +            
 x   3  1    1011 -0.017068    5( a1 )   4( b2 )    -             +            
 x   3  1    1029 -0.014131    1( a1 )   6( b2 )    -             +            
 x   3  1    1032 -0.014677    4( a1 )   6( b2 )    -             +            
 x   3  1    1058 -0.013129    2( a1 )   5( a1 )    -                  +       
 x   3  1    1065  0.014326    5( a1 )   6( a1 )    -                  +       
 x   3  1    1082  0.010375    4( a1 )   9( a1 )    -                  +       
 x   3  1    1140  0.011785    1( b2 )   6( b2 )    -                  +       
 x   3  1    1142  0.012195    3( b2 )   6( b2 )    -                  +       
 x   3  1    1155 -0.014721    5( a1 )   1( a2 )    -                       +  
 x   3  1    1166  0.017068    5( a1 )   2( a2 )    -                       +  
 x   3  1    1173  0.010910    1( a1 )   3( a2 )    -                       +  
 x   3  1    1176  0.011325    4( a1 )   3( a2 )    -                       +  
 x   3  1    1190  0.013360    7( b1 )   1( b2 )    -                       +  
 x   3  1    1204  0.013925    7( b1 )   3( b2 )    -                       +  
 x   3  1    1226  0.013071    1( b1 )   7( b2 )    -                       +  
 x   3  1    1228  0.013621    3( b1 )   7( b2 )    -                       +  
 w   3  1    4173  0.037714    1( a1 )   1( b1 )   +         +                 
 w   3  1    4176  0.028727    4( a1 )   1( b1 )   +         +                 
 w   3  1    4195  0.028855    1( a1 )   3( b1 )   +         +                 
 w   3  1    4198  0.023466    4( a1 )   3( b1 )   +         +                 
 w   3  1    4231 -0.010370    4( a1 )   6( b1 )   +         +                 
 w   3  1    4279  0.010062    3( a2 )   3( b1 )   +              +            
 w   3  1    4292 -0.039590    1( a1 )   1( b2 )   +              +            
 w   3  1    4295 -0.030230    4( a1 )   1( b2 )   +              +            
 w   3  1    4314 -0.030224    1( a1 )   3( b2 )   +              +            
 w   3  1    4317 -0.024639    4( a1 )   3( b2 )   +              +            
 w   3  1    4347  0.012133    1( a1 )   6( b2 )   +              +            
 w   3  1    4350  0.012863    4( a1 )   6( b2 )   +              +            
 w   3  1    4369  0.027508    1( a1 )   1( a1 )   +                   +       
 w   3  1    4375  0.029705    1( a1 )   4( a1 )   +                   +       
 w   3  1    4378  0.017131    4( a1 )   4( a1 )   +                   +       
 w   3  1    4469 -0.027154    1( b2 )   1( b2 )   +                   +       
 w   3  1    4472 -0.029321    1( b2 )   3( b2 )   +                   +       
 w   3  1    4474 -0.016904    3( b2 )   3( b2 )   +                   +       
 w   3  1    4484 -0.011009    1( b2 )   6( b2 )   +                   +       
 w   3  1    4486 -0.011615    3( b2 )   6( b2 )   +                   +       
 w   3  1    4530  0.039601    1( b1 )   1( b2 )   +                        +  
 w   3  1    4532  0.030302    3( b1 )   1( b2 )   +                        +  
 w   3  1    4544  0.030166    1( b1 )   3( b2 )   +                        +  
 w   3  1    4546  0.024643    3( b1 )   3( b2 )   +                        +  
 w   3  1    4550  0.010136    7( b1 )   3( b2 )   +                        +  
 w   3  1    4572 -0.010398    1( b1 )   7( b2 )   +                        +  
 w   3  1    4574 -0.011075    3( b1 )   7( b2 )   +                        +  
 w   3  1    4847 -0.012460    1( b1 )   1( b1 )        +              +       
 w   3  1    4850 -0.010577    1( b1 )   3( b1 )        +              +       
 w   3  1    4875 -0.016172    1( b2 )   1( b2 )        +              +       
 w   3  1    4878 -0.014094    1( b2 )   3( b2 )        +              +       
 w   3  1    5083  0.048445    1( a1 )   1( a1 )             +         +       
 w   3  1    5089  0.041091    1( a1 )   4( a1 )             +         +       
 w   3  1    5092  0.020735    4( a1 )   4( a1 )             +         +       
 w   3  1    5097  0.016405    5( a1 )   5( a1 )             +         +       
 w   3  1    5155  0.053579    1( b1 )   1( b1 )             +         +       
 w   3  1    5158  0.045590    1( b1 )   3( b1 )             +         +       
 w   3  1    5160  0.022892    3( b1 )   3( b1 )             +         +       
 w   3  1    5183  0.052933    1( b2 )   1( b2 )             +         +       
 w   3  1    5186  0.045139    1( b2 )   3( b2 )             +         +       
 w   3  1    5188  0.022712    3( b2 )   3( b2 )             +         +       
 w   3  1    5375 -0.056409    1( a1 )   1( a1 )                  +         +  
 w   3  1    5381 -0.048154    1( a1 )   4( a1 )                  +         +  
 w   3  1    5384 -0.024234    4( a1 )   4( a1 )                  +         +  
 w   3  1    5389 -0.017239    5( a1 )   5( a1 )                  +         +  
 w   3  1    5447 -0.056264    1( b1 )   1( b1 )                  +         +  
 w   3  1    5450 -0.047861    1( b1 )   3( b1 )                  +         +  
 w   3  1    5452 -0.024042    3( b1 )   3( b1 )                  +         +  
 w   3  1    5475 -0.050140    1( b2 )   1( b2 )                  +         +  
 w   3  1    5478 -0.042435    1( b2 )   3( b2 )                  +         +  
 w   3  1    5480 -0.021432    3( b2 )   3( b2 )                  +         +  

 ci coefficient statistics:
           rq > 0.1                3
      0.1> rq > 0.01             116
     0.01> rq > 0.001            785
    0.001> rq > 0.0001          1326
   0.0001> rq > 0.00001          452
  0.00001> rq > 0.000001          72
 0.000001> rq                   2846
           all                  5600
  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.152075388368
     2     2     -0.648945740724
     3     3      0.680901708106
     4     4     -0.017080488423
     5     5      0.023798412192
     6     6     -0.012740769353
     7     7      0.057285338097
     8     8      0.030064940017
     9     9      0.014914399848
    10    10      0.045368565871
    11    11     -0.025678966259
    12    12     -0.034865924204
    13    13     -0.015935121926
    14    14      0.060982266994
    15    15     -0.000665005874
    16    16     -0.043496645454
    17    17      0.030639931894
    18    18     -0.033890018706
    19    19      0.012389482852
    20    20      0.000724030477
    21    21      0.001973114498
    22    22     -0.005865507934
    23    23      0.007249708776
    24    24     -0.000538179420
    25    25     -0.023742063079
    26    26     -0.031685991633
    27    27      0.024168755487
    28    28      0.033245854473

 number of reference csfs (nref) is    28.  root number (iroot) is  1.
 c0**2 =   0.92870114  c0**2(renormalized) =   0.99491647
 c**2 (all zwalks) =   0.92870114  c**2(all zwalks, renormalized) =   0.99491647
 passed aftci ... 
 readint2: molcas,dalton2=                     0                     0
 files%faoints=aoints              
lodens (list->root)=  1
sifcfg setup: record length 4096 DP
# d1 elements per record  3272
# d2 elements per record  2730
  The AQCC density will be calculated.
 item #                     1 suffix=:.drt1.state1:
 read_civout: repnuc=  -43.4985876562748     
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method: 30 last record  1max overlap with ref# 96% root-following 0
 MR-CISD energy:   -46.34842693    -2.84983928
 residuum:     0.00049783
 deltae:     0.00000360
 apxde:     0.00000031
 a4den:     1.01202605
 reference energy:   -46.30187858    -2.80329092

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.96367059     0.13210594    -0.12381641    -0.19106642     0.02168242    -0.03974921     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.96367059     0.13210594     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 rdcirefv: extracting reference vector # 1(blocksize=    28 #zcsf=      28)
 =========== Executing IN-CORE method ==========
================================================================================
   DYZ=       0  DYX=       0  DYW=       0
   D0Z=      37  D0Y=       0  D0X=       0  D0W=       0
  DDZI=      96 DDYI=       0 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
 root #                      1 : Scaling(1) with    1.01202604996855      
 corresponding to ref #                      1
 computing final density
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=     137  DYX=     275  DYW=     150
   D0Z=      37  D0Y=     125  D0X=      38  D0W=      12
  DDZI=      96 DDYI=     240 DDXI=      66 DDWI=      30
  DDZE=       0 DDYE=      90 DDXE=      36 DDWE=      15
================================================================================
Trace of MO density:     4.000000
    4  correlated and     8  frozen core electrons

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     1.86505506    -0.03522611    -0.00843883     0.00000000     0.00326459    -0.00733950
   MO   4     0.00000000     0.00000000    -0.03522611     0.03869214    -0.10490378     0.00000000    -0.00170895     0.00110046
   MO   5     0.00000000     0.00000000    -0.00843883    -0.10490378     0.46219366     0.00000000     0.00419512     0.00233535
   MO   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.02491652     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000     0.00326459    -0.00170895     0.00419512     0.00000000     0.00080931     0.00018362
   MO   8     0.00000000     0.00000000    -0.00733950     0.00110046     0.00233535     0.00000000     0.00018362     0.00048350
   MO   9     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.01742589     0.00000000     0.00000000
   MO  10     0.00000000     0.00000000    -0.03031932    -0.00057578    -0.00013678     0.00000000     0.00001803    -0.00003990
   MO  11     0.00000000     0.00000000     0.00488233    -0.00210765     0.00383221     0.00000000     0.00090666     0.00015145
   MO  12     0.00000000     0.00000000     0.00822726    -0.00136803    -0.00292315     0.00000000    -0.00032640    -0.00057590
   MO  13     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00365163     0.00000000     0.00000000
   MO  14     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00066884     0.00000000     0.00000000
   MO  15     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00044024     0.00000000     0.00000000
   MO  16     0.00000000     0.00000000    -0.00023487     0.00004300     0.00001025     0.00000000    -0.00000311     0.00000696

                MO   9         MO  10         MO  11         MO  12         MO  13         MO  14         MO  15         MO  16
   MO   3     0.00000000    -0.03031932     0.00488233     0.00822726     0.00000000     0.00000000     0.00000000    -0.00023487
   MO   4     0.00000000    -0.00057578    -0.00210765    -0.00136803     0.00000000     0.00000000     0.00000000     0.00004300
   MO   5     0.00000000    -0.00013678     0.00383221    -0.00292315     0.00000000     0.00000000     0.00000000     0.00001025
   MO   6     0.01742589     0.00000000     0.00000000     0.00000000     0.00365163     0.00066884     0.00044024     0.00000000
   MO   7     0.00000000     0.00001803     0.00090666    -0.00032640     0.00000000     0.00000000     0.00000000    -0.00000311
   MO   8     0.00000000    -0.00003990     0.00015145    -0.00057590     0.00000000     0.00000000     0.00000000     0.00000696
   MO   9     0.01257702     0.00000000     0.00000000     0.00000000     0.00291541     0.00053245     0.00034636     0.00000000
   MO  10     0.00000000     0.00571156     0.00002927     0.00004940     0.00000000     0.00000000     0.00000000    -0.00026465
   MO  11     0.00000000     0.00002927     0.00113817    -0.00034916     0.00000000     0.00000000     0.00000000    -0.00000530
   MO  12     0.00000000     0.00004940    -0.00034916     0.00076865     0.00000000     0.00000000     0.00000000    -0.00000893
   MO  13     0.00291541     0.00000000     0.00000000     0.00000000     0.00089430     0.00015401     0.00009573     0.00000000
   MO  14     0.00053245     0.00000000     0.00000000     0.00000000     0.00015401     0.00127168    -0.00020266     0.00000000
   MO  15     0.00034636     0.00000000     0.00000000     0.00000000     0.00009573    -0.00020266     0.00140525     0.00000000
   MO  16     0.00000000    -0.00026465    -0.00000530    -0.00000893     0.00000000     0.00000000     0.00000000     0.00001755

Natural orbital populations,block 1
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     2.00000000     1.86634157     0.48685410     0.03785109     0.01413012     0.00506588     0.00208357
              MO     9       MO    10       MO    11       MO    12       MO    13       MO    14       MO    15       MO    16
  occ(*)=     0.00155185     0.00110635     0.00054447     0.00032119     0.00005712     0.00001251     0.00001099     0.00000355

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4
   MO   1     0.50435279     0.00467816    -0.00449899     0.00000000
   MO   2     0.00467816     0.00086332    -0.00097207     0.00000000
   MO   3    -0.00449899    -0.00097207     0.00124417     0.00000000
   MO   4     0.00000000     0.00000000     0.00000000     0.00179235

Natural orbital populations,block 2
              MO     1       MO     2       MO     3       MO     4       MO
  occ(*)=     0.50443663     0.00196161     0.00179235     0.00006202

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     0.48377222     0.00000000     0.00454574     0.00000000    -0.00440476     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.02402154     0.00000000     0.01647968     0.00000000     0.00327261    -0.00058473
   MO   4     0.00000000     0.00454574     0.00000000     0.00084265     0.00000000    -0.00094892     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.01647968     0.00000000     0.01166429     0.00000000     0.00258251    -0.00055481
   MO   6     0.00000000    -0.00440476     0.00000000    -0.00094892     0.00000000     0.00121327     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000     0.00327261     0.00000000     0.00258251     0.00000000     0.00078827    -0.00025412
   MO   8     0.00000000     0.00000000    -0.00058473     0.00000000    -0.00055481     0.00000000    -0.00025412     0.00134043
   MO   9     0.00000000     0.00000000     0.00074382     0.00000000     0.00070169     0.00000000     0.00031767     0.00047775

                MO   9
   MO   2     0.00000000
   MO   3     0.00074382
   MO   4     0.00000000
   MO   5     0.00070169
   MO   6     0.00000000
   MO   7     0.00031767
   MO   8     0.00047775
   MO   9     0.00122301

Natural orbital populations,block 3
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     0.48385536     0.03598539     0.00191274     0.00176308     0.00097609     0.00030272     0.00006003
              MO     9       MO
  occ(*)=     0.00001028

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     0.50730166     0.00000000     0.00505322     0.00000000    -0.00501129     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.02493638     0.00000000     0.01744507     0.00000000    -0.00365945     0.00024294
   MO   4     0.00000000     0.00505322     0.00000000     0.00091231     0.00000000    -0.00105920     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.01744507     0.00000000     0.01259472     0.00000000    -0.00292209     0.00019129
   MO   6     0.00000000    -0.00501129     0.00000000    -0.00105920     0.00000000     0.00137760     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000    -0.00365945     0.00000000    -0.00292209     0.00000000     0.00089651    -0.00005298
   MO   8     0.00000000     0.00000000     0.00024294     0.00000000     0.00019129     0.00000000    -0.00005298     0.00171195
   MO   9     0.00000000     0.00000000    -0.00084253     0.00000000    -0.00067426     0.00000000     0.00019865     0.00018163

                MO   9
   MO   2     0.00000000
   MO   3    -0.00084253
   MO   4     0.00000000
   MO   5    -0.00067426
   MO   6     0.00000000
   MO   7     0.00019865
   MO   8     0.00018163
   MO   9     0.00121622

Natural orbital populations,block 4
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     0.50740191     0.03789637     0.00213047     0.00177137     0.00113572     0.00054129     0.00005919
              MO     9       MO
  occ(*)=     0.00001102


 total number of electrons =   12.0000000000

 test slabel:                     4
  a1  a2  b1  b2 


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        a1  partial gross atomic populations
   ao class       1a1        2a1        3a1        4a1        5a1        6a1 
     3_ s       0.000000   2.000000   1.865543   0.000000   0.000000   0.000264
     3_ p       2.000000   0.000000   0.000000   0.000000   0.037821   0.000000
     3_ d       0.000000   0.000000   0.000798   0.486854   0.000000   0.013866
     3_ f       0.000000   0.000000   0.000000   0.000000   0.000030   0.000000
 
   ao class       7a1        8a1        9a1       10a1       11a1       12a1 
     3_ s       0.004973   0.000000   0.000000   0.000000   0.000000   0.000000
     3_ p       0.000000   0.000000   0.000000   0.000021   0.000534   0.000000
     3_ d       0.000093   0.002084   0.000000   0.000000   0.000000   0.000321
     3_ f       0.000000   0.000000   0.001552   0.001085   0.000010   0.000000
 
   ao class      13a1       14a1       15a1       16a1 
     3_ s       0.000000   0.000000   0.000000   0.000003
     3_ p       0.000000   0.000000   0.000011   0.000000
     3_ d       0.000057   0.000012   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000000

                        a2  partial gross atomic populations
   ao class       1a2        2a2        3a2        4a2 
     3_ d       0.504437   0.001962   0.000000   0.000062
     3_ f       0.000000   0.000000   0.001792   0.000000

                        b1  partial gross atomic populations
   ao class       1b1        2b1        3b1        4b1        5b1        6b1 
     3_ p       2.000000   0.000000   0.035934   0.000000   0.000000   0.000327
     3_ d       0.000000   0.483855   0.000000   0.001913   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000051   0.000000   0.001763   0.000649
 
   ao class       7b1        8b1        9b1 
     3_ p       0.000202   0.000000   0.000010
     3_ d       0.000000   0.000060   0.000000
     3_ f       0.000100   0.000000   0.000000

                        b2  partial gross atomic populations
   ao class       1b2        2b2        3b2        4b2        5b2        6b2 
     3_ p       2.000000   0.000000   0.037860   0.000000   0.000000   0.000028
     3_ d       0.000000   0.507402   0.000000   0.002130   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000036   0.000000   0.001771   0.001108
 
   ao class       7b2        8b2        9b2 
     3_ p       0.000528   0.000000   0.000011
     3_ d       0.000000   0.000059   0.000000
     3_ f       0.000013   0.000000   0.000000


                        gross atomic populations
     ao            3_
      s         3.870784
      p         6.113290
      d         2.005965
      f         0.009961
    total      12.000000
 

 Total number of electrons:   12.00000000

 DA ...
