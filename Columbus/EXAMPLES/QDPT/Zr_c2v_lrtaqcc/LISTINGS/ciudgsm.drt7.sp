1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.

  This is a  mraqcc energy evaluation      

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  1  1    -46.3018785769 -1.3323E-15  5.2520E-02  2.1139E-01  1.0000E-03
 mraqcc  #  2  1    -46.3460524430  4.4174E-02  1.9116E-03  4.0445E-02  1.0000E-03
 mraqcc  #  3  1    -46.3481457353  2.0933E-03  2.5892E-04  1.4528E-02  1.0000E-03
 mraqcc  #  4  1    -46.3483861013  2.4037E-04  4.4832E-05  6.3332E-03  1.0000E-03
 mraqcc  #  5  1    -46.3484233372  3.7236E-05  3.4812E-06  1.7164E-03  1.0000E-03
 mraqcc  #  6  1    -46.3484269336  3.5964E-06  3.0574E-07  4.9783E-04  1.0000E-03

 mraqcc   convergence criteria satisfied after  6 iterations.

 final mraqcc   convergence information:
 mraqcc  #  6  1    -46.3484269336  3.5964E-06  3.0574E-07  4.9783E-04  1.0000E-03

 number of reference csfs (nref) is    28.  root number (iroot) is  1.
 c0**2 =   0.92870114  c0**2(renormalized) =   0.99491647
 c**2 (all zwalks) =   0.92870114  c**2(all zwalks, renormalized) =   0.99491647
