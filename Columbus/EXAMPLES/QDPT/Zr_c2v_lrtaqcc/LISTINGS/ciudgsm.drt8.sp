1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.

  This is a  mraqcc energy evaluation      

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  1  1    -46.3018785761  1.9096E-14  5.2649E-02  2.1139E-01  1.0000E-03
 mraqcc  #  2  1    -46.3460306766  4.4152E-02  1.9251E-03  4.0568E-02  1.0000E-03
 mraqcc  #  3  1    -46.3481430361  2.1124E-03  2.5783E-04  1.4522E-02  1.0000E-03
 mraqcc  #  4  1    -46.3483864246  2.4339E-04  4.4604E-05  6.3068E-03  1.0000E-03
 mraqcc  #  5  1    -46.3484231931  3.6768E-05  3.6684E-06  1.7541E-03  1.0000E-03
 mraqcc  #  6  1    -46.3484269185  3.7254E-06  3.2002E-07  5.1347E-04  1.0000E-03

 mraqcc   convergence criteria satisfied after  6 iterations.

 final mraqcc   convergence information:
 mraqcc  #  6  1    -46.3484269185  3.7254E-06  3.2002E-07  5.1347E-04  1.0000E-03

 number of reference csfs (nref) is    28.  root number (iroot) is  1.
 c0**2 =   0.92870161  c0**2(renormalized) =   0.99491654
 c**2 (all zwalks) =   0.92870161  c**2(all zwalks, renormalized) =   0.99491654
