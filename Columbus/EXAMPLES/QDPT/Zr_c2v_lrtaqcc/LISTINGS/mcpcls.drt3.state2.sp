

     ******************************************
     **    PROGRAM:              MCPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 original author: Daniel Robertson, FSU
 later revisions: Ron Shepard, ANL;
                  Michal Dallos, University Vienna



 This Version of Program mcpc is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



   ******  File header section  ******

 Headers form the restart file:
                                                                                    
    aoints SIFS file created by argos.      zam792            17:56:38.564 17-Dec-13
     title                                                                          
     title                                                                          
     title                                                                          
     title                                                                          


   ******  DRT info section  ******

 Informations for the DRT no.  1
 Header form the DRT file: 
     title                                                                          
 Molecular symmetry group:   sym1
 Total number of electrons:   12
 Spin multiplicity:            3
 Number of active orbitals:    6
 Number of active electrons:   4
 Total number of CSFs:        21

   ***  Informations from the DRT number:   1

 
 Symmetry orbital summary:
 Symm.blocks:         1     2     3     4
 Symm.labels:         a1    a2    b1    b2 

 List of doubly occupied orbitals:
  1 a1   2 a1   1 b1   1 b2 

 List of active orbitals:
  3 a1   4 a1   5 a1   1 a2   2 b1   2 b2 

 Informations for the DRT no.  2
 Header form the DRT file: 
     title                                                                          
 Molecular symmetry group:    a2 
 Total number of electrons:   12
 Spin multiplicity:            3
 Number of active orbitals:    6
 Number of active electrons:   4
 Total number of CSFs:        28

   ***  Informations from the DRT number:   2

 
 Symmetry orbital summary:
 Symm.blocks:         1     2     3     4
 Symm.labels:         a1    a2    b1    b2 

 List of doubly occupied orbitals:
  1 a1   2 a1   1 b1   1 b2 

 List of active orbitals:
  3 a1   4 a1   5 a1   1 a2   2 b1   2 b2 

 Informations for the DRT no.  3
 Header form the DRT file: 
     title                                                                          
 Molecular symmetry group:    b1 
 Total number of electrons:   12
 Spin multiplicity:            3
 Number of active orbitals:    6
 Number of active electrons:   4
 Total number of CSFs:        28

   ***  Informations from the DRT number:   3

 
 Symmetry orbital summary:
 Symm.blocks:         1     2     3     4
 Symm.labels:         a1    a2    b1    b2 

 List of doubly occupied orbitals:
  1 a1   2 a1   1 b1   1 b2 

 List of active orbitals:
  3 a1   4 a1   5 a1   1 a2   2 b1   2 b2 

 Informations for the DRT no.  4
 Header form the DRT file: 
     title                                                                          
 Molecular symmetry group:    b2 
 Total number of electrons:   12
 Spin multiplicity:            3
 Number of active orbitals:    6
 Number of active electrons:   4
 Total number of CSFs:        28

   ***  Informations from the DRT number:   4

 
 Symmetry orbital summary:
 Symm.blocks:         1     2     3     4
 Symm.labels:         a1    a2    b1    b2 

 List of doubly occupied orbitals:
  1 a1   2 a1   1 b1   1 b2 

 List of active orbitals:
  3 a1   4 a1   5 a1   1 a2   2 b1   2 b2 


   ******  MCSCF convergence information:  ******

 MCSCF convergence criteria were satisfied.

 mcscf energy=   -46.3018785757    nuclear repulsion=     0.0000000000
 demc=             0.0000000000    wnorm=                 0.0000008053
 knorm=            0.0000000310    apxde=                 0.0000000000


 MCSCF calculation performmed for   4 symmetries.

 State averaging:
 No,  ssym, navst, wavst
  1    a1     1   0.1429
  2    a2     2   0.1429 0.1429
  3    b1     2   0.1429 0.1429
  4    b2     2   0.1429 0.1429

 Input the DRT No of interest: [  1]:
In the DRT No.: 3 there are  2 states.

 Which one to take? [  1]:
 The CSFs for the state No  2 of the symmetry  b1  will be printed
 according to the following print options :

 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 9) export wave function files for cioverlap (all states).
 0) end.

 input menu number [  0]: csfs will be printed based on coefficient magnitudes.

 input the coefficient threshold (end with 0.) [ 0.0000]:
 List of active orbitals:
  3 a1   4 a1   5 a1   1 a2   2 b1   2 b2 

   csf       coeff       coeff**2    step(*)
  -----  ------------  ------------  ------------
      2 -0.6738790539  0.4541129794  301010
      3 -0.5308771017  0.2818304971  300101
      1 -0.4908589413  0.2409425002  310010
     13  0.0771629992  0.0059541284  101201
      4 -0.0735913623  0.0054156886  130010
     15  0.0485595219  0.0023580272  100310
     14  0.0425767922  0.0018127832  101102
     27  0.0291809974  0.0008515306  001013
     12  0.0291042338  0.0008470564  102101
     26 -0.0289495124  0.0008380743  001310
     17  0.0269964255  0.0007288070  031010
     19 -0.0250422361  0.0006271136  013010
     24  0.0247181906  0.0006109889  010013
     28 -0.0246474531  0.0006074969  000131
      8 -0.0243831310  0.0005945371  111020
     18  0.0220655972  0.0004868906  030101
     25  0.0203495641  0.0004141048  003101
      7 -0.0128616476  0.0001654220  112010
     16 -0.0128412937  0.0001648988  100013
     23 -0.0122476772  0.0001500056  010310
     11  0.0121908624  0.0001486171  103010
      9  0.0103044098  0.0001061809  110201
     10 -0.0090852195  0.0000825412  110102
     21  0.0071941529  0.0000517558  011201
      6  0.0070000321  0.0000490004  120101
     20 -0.0067430395  0.0000454686  012101
     22 -0.0016486013  0.0000027179  011102

 input the coefficient threshold (end with 0.) [ 0.0000]:
 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 9) export wave function files for cioverlap (all states).
 0) end.

 input menu number [  0]: