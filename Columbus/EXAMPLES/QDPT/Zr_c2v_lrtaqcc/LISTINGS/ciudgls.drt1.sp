1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      2009-03.    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


================ Computing sorting integral file structure ================

                    -----z----- -----y----- -----x----- -----w----- ---total---

                CSFs        33         498        1412        2264        4207
      internal walks       105          70          15          21         211
valid internal walks        33          70          15          21         139
 getinfoarray: info=                     6 :                     1
                  8192                  6552                  8192
                  5460                     0
 icd(3)=                  2976 ci%nnlev=                   595  l2rec=
                  8192  n2max=                  5460
 lcore1,lcore2=              13068137              13044821
 lencor,maxblo              13107200                 60000
========================================
 current settings:
 minbl3         381
 minbl4         381
 locmaxbl3     4288
 locmaxbuf     2144
 maxbl3       60000
 maxbl3       60000
 maxbl4       60000
 maxbuf       30006
========================================

 sorted 4-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)

 sorted 3-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)
 Orig.  diagonal integrals:  1electron:        34
                             0ext.    :        42
                             2ext.    :       336
                             4ext.    :       812


 Orig. off-diag. integrals:  4ext.    :     24411
                             3ext.    :     18720
                             2ext.    :      6792
                             1ext.    :      1272
                             0ext.    :        99
                             2ext. SO :         0
                             1ext. SO :         0
                             0ext. SO :         0
                             1electron:       128


 Sorted integrals            3ext.  w :     17370 x :     16020
                             4ext.  w :     20955 x :     17777


Cycle #  1 sortfile size=    163835(       5 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13068137
Cycle #  2 sortfile size=    163835(       5 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13068137
 minimum size of srtscr:     98301 WP (     3 records)
 maximum size of srtscr:    163835 WP (     5 records)
diagi   file:      4 records  of   1536 WP each=>       6144 WP total
ofdgi   file:      8 records  of   1536 WP each=>      12288 WP total
fil3w   file:      1 records  of  30006 WP each=>      30006 WP total
fil3x   file:      1 records  of  30006 WP each=>      30006 WP total
fil4w   file:      1 records  of  30006 WP each=>      30006 WP total
fil4x   file:      1 records  of  30006 WP each=>      30006 WP total
 compressed index vector length=                    75
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 3,
  GSET = 3,
   DAVCOR =10,
  NCOREL = 4
   nroot=1
  IVMODE = 3
  NBKITR = 1
   nvbkmn=2
  RTOLBK = 1e-3
   niter=30
   nvcimn=2
   rtolci=1e-3
  NVCIMX = 6
  NVRFMX = 6
  NVBKMX = 6
  IDEN  = 1
  CSFPRN = 10,
  update_mode=1
  nvrfmn=2
   lrtshift=-0.0465482970
 /&end
 ------------------------------------------------------------------------
lodens (list->root)=  1
invlodens (root->list)=  1
 LRT calculation: diagonal shift= -4.654829700000000E-002
 bummer (warning):2:changed keyword: nbkitr=                      0
 USING SEGMENTS OF EQUAL SIZE

****************  list of control variables  ****************
 lvlprt =    0      nroot  =    1      noldv  =   0      noldhv =   0
 nunitv =    1      nbkitr =    0      niter  =  30      davcor =  10
 csfprn =   10      ivmode =    3      istrt  =   0      vout   =   0
 iortls =    0      nvbkmx =    6      ibktv  =  -1      ibkthv =  -1
 nvcimx =    6      icitv  =   -1      icithv =  -1      frcsub =   0
 nvbkmn =    2      nvcimn =    2      maxseg =   4      nrfitr =  30
 ncorel =    4      nvrfmx =    6      nvrfmn =   2      iden   =   1
 itran  =    0      froot  =    0      rtmode =   0      ncouple=   1
 skipso =    F      dalton2=    0      molcas =   0      finalv =   0
 finalw =    0      cosmocalc=   0    with_tsklst=   0
 nsegwx =    1     1     1     1
 nseg0x =    1     1     1     1
 nseg1x =    1     1     1     1
 nseg2x =    1     1     1     1
 nseg3x =    1     1     1     1
 nseg4x =    1     1     1     1
 no0ex  =      0    no1ex  =      0    no2ex  =     0    no3ex  =     0
 no4ex  =      0    nodiag =      0
 cdg4ex =    1      c3ex1ex=    1      c2ex0ex=   1
 fileloc=    0     0     0     0     0     0     0     1     1     1
 directhd=   1      noaqccshift_zyxw=      0
 critical_crit=-1.00000    critical_delta= 0.05000

 ctol   = 0.010000    lrtshift=-.046548    smalld =0.001000


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-03    1.000E-03
 Computing density:                    .drt1.state1
 using                      1  nodes and                      1  cores.
 szdg/szodg per processor=                  6144                 12288
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core           13107199 DP per process

********** Integral sort section *************


 workspace allocation information: lencor=  13107199

 echo of the input for program cisrt:
 ------------------------------------------------------------------------
  &input
  maxbl3=60000
  maxbl4=60000
  &end
 ------------------------------------------------------------------------
 
 ( 6) listing file:                    ciudgls             
 ( 5) input file:                      cisrtin   
 (17) cidrt file:                      cidrtfl             
 (11) transformed integrals file:      moints    
 (12) diagonal integral file:          diagint             
 (13) off-diagonal integral file:      ofdgint             
 (31) 4-external w integrals file:     fil4w               
 (32) 4-external x integrals file:     fil4x               
 (33) 3-external w integrals file:     fil3w               
 (34) 3-external x integrals file:     fil3x               
 (21) scratch da sorting file:         srtscr              
 (12) 2-e integral file [fsplit=2]:    moints2   

 input integral file header information:
                                                                                 
 aoints SIFS file created by argos.      zam792            17:56:38.564 17-Dec-13
  cidrt_title                                                                    
 MO-coefficients from mcscf.x                                                    
  with dummy occupation 1.0 for active orbitals                                  
  total ao core energy =    0.000000000                                          
 MCSCF energy =     -46.301878576                                                
 SIFS file created by program tran.      zam792            17:56:38.888 17-Dec-13

 input energy(*) values:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.

 total core energy =   0.000000000000E+00

 nsym = 4 nmot=  34

 symmetry  =    1    2    3    4
 slabel(*) =   a1   a2   b1   b2
 nmpsy(*)  =   14    4    8    8

 info(*) =          1      8192      6552      8192      5460         0

 orbital labels, i:molab(i)=
   1:tout:001   2:tout:002   3:tout:003   4:tout:004   5:tout:005   6:tout:006   7:tout:007   8:tout:008   9:tout:009  10:tout:010
  11:tout:011  12:tout:012  13:tout:013  14:tout:014  15:tout:015  16:tout:016  17:tout:017  18:tout:018  19:tout:019  20:tout:020
  21:tout:021  22:tout:022  23:tout:023  24:tout:024  25:tout:025  26:tout:026  27:tout:027  28:tout:028  29:tout:029  30:tout:030
  31:tout:031  32:tout:032  33:tout:033  34:tout:034

 input parameters:
 prnopt=  0
 ldamin=    4095 ldamax=   32767 ldainc=      64
 maxbuf=   30006 maxbl3=   60000 maxbl4=   60000 intmxo=    1536
  Using 32 bit compression 

 drt information:
  cidrt_title                                                                    
 nmotd =  38 nfctd =   4 nfvtc =   0 nmot  =  34
 nlevel =  34 niot  =   6 lowinl=  29
 orbital-to-level map(*)
   -1  -1  29  30  31   1   2   3   4   5   6   7   8   9  10  11  32  12  13  14
   -1  33  15  16  17  18  19  20  21  -1  34  22  23  24  25  26  27  28
 compressed map(*)
   29  30  31   1   2   3   4   5   6   7   8   9  10  11  32  12  13  14  33  15
   16  17  18  19  20  21  34  22  23  24  25  26  27  28
 levsym(*)
    1   1   1   1   1   1   1   1   1   1   1   2   2   2   3   3   3   3   3   3
    3   4   4   4   4   4   4   4   1   1   1   2   3   4
 repartitioning mu(*)=
   0.  0.  0.  0.  0.  0.

 new core energy added to the energy(*) list.
 from the integral file: h1_core= -4.349858765627E+01

 indxdg: diagonal integral statistics.
 total number of integrals contributing to diagonal matrix elements:      1190
 number with all external indices:       812
 number with half external - half internal indices:       336
 number with all internal indices:        42

 indxof: off-diagonal integral statistics.
    4-external integrals: num=      24411 strt=          1
    3-external integrals: num=      18720 strt=      24412
    2-external integrals: num=       6792 strt=      43132
    1-external integrals: num=       1272 strt=      49924
    0-external integrals: num=         99 strt=      51196

 total number of off-diagonal integrals:       51294


 indxof(2nd)  ittp=   3 numx(ittp)=        6792
 indxof(2nd)  ittp=   4 numx(ittp)=        1272
 indxof(2nd)  ittp=   5 numx(ittp)=          99

 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg=  12931980
 pro2e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652

 pro2e:     25126 integrals read in     5 records.

 pro2e:         0 integrals 34-ext integrals skipped.
 pro1e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652
 pro1e: eref =    0.000000000000000E+00
 total size of srtscr:                     4  records of                  32767 
 WP =               1048544 Bytes
 putdg        1     596    1191    1786    3322   36089   57934    3018   46706   90394
   123161  131353  136813  158652

 putf:       4 buffers of length    1536 written to file 12
 diagonal integral file completed.

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep4e:    17 blocks of linear combinations of 4-external integrals processed.
 number of sorted 4-external integrals      38732
 number of original 4-external integrals    24411


 putf34: external integral file complete. nfilw=    31 nfilx=    32 nrecw=     1 nrecx=     1 lbufp= 30006

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep3e:    19 blocks of linear combinations of 3-external integrals processed.
 number of sorted 3-external integrals      33390
 number of original 3-external integrals    18720


 putf34: external integral file complete. nfilw=    33 nfilx=    34 nrecw=     1 nrecx=     1 lbufp= 30006
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      6792         3     43132     43132      6792     51294
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      1272         4     49924     49924      1272     51294
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd        99         5     51196     51196        99     51294

 putf:       8 buffers of length    1536 written to file 13
 off-diagonal files sort completed.
 executing brd_struct for cisrtinfo
cisrtinfo:
bufszi  1536
 diagfile 4ext:     812 2ext:     336 0ext:      42
 fil4w,fil4x  :   24411 fil3w,fil3x :   18720
 ofdgint  2ext:    6792 1ext:    1272 0ext:      99so0ext:       0so1ext:       0so2ext:       0
buffer minbl4     381 minbl3     381 maxbl2     384nbas:  11   3   7   7   0   0   0   0 maxbuf 30006
 CIUDG version 5.9.7 ( 5-Oct-2004)

 workspace allocation information: lcore=  13107199

 core energy values from the integral file:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -4.349858765627E+01, ietype=    6,   fcore energy of type: H1(*)   

 total core repulsion energy = -4.349858765627E+01
 nmot  =    38 niot  =     6 nfct  =     4 nfvt  =     0
 nrow  =    32 nsym  =     4 ssym  =     1 lenbuf=  1600
 nwalk,xbar:        211      105       70       15       21
 nvalwt,nvalw:      139       33       70       15       21
 ncsft:            4207
 total number of valid internal walks:     139
 nvalz,nvaly,nvalx,nvalw =       33      70      15      21

 cisrt info file parameters:
 file number  12 blocksize   1536
 mxbld   1536
 nd4ext,nd2ext,nd0ext   812   336    42
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int    24411    18720     6792     1272       99        0        0        0
 minbl4,minbl3,maxbl2   381   381   384
 maxbuf 30006
 number of external orbitals per symmetry block:  11   3   7   7
 nmsym   4 number of internal orbitals   6
 executing brd_struct for drt
 executing brd_struct for orbinf
 executing brd_struct for momap
 calcthrxt: niot,maxw1=                     6                    31
 block size     0
 pthz,pthy,pthx,pthw:   105    70    15    21 total internal walks:     211
 maxlp3,n2lp,n1lp,n0lp    31     0     0     0
 orbsym(*)= 1 1 1 2 3 4

 setref:       33 references kept,
               72 references were marked as invalid, out of
              105 total.
 nmb.of records onel     1
 nmb.of records 2-ext     5
 nmb.of records 1-ext     1
 nmb.of records 0-ext     1
 nmb.of records 2-int     0
 nmb.of records 1-int     0
 nmb.of records 0-int     0
 ---------memory usage in DP -----------------
 < n-ex core usage >
     routines:
    fourex            61797
    threx             60199
    twoex              3549
    onex               1823
    allin              1536
    diagon             2109
               =======
   maximum            61797
 
  __ static summary __ 
   reflst                33
   hrfspc                33
               -------
   static->              66
 
  __ core required  __ 
   totstc                66
   max n-ex           61797
               -------
   totnec->           61863
 
  __ core available __ 
   totspc          13107199
   totnec -           61863
               -------
   totvec->        13045336

 number of external paths / symmetry
 vertex x     100      82      98      98
 vertex w     128      82      98      98
segment: free space=    13045336
 reducing frespc by                   770 to               13044566 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1          46|        33|         0|        33|         0|         1|
 -------------------------------------------------------------------------------
  Y 2          70|       498|        33|        70|        33|         2|
 -------------------------------------------------------------------------------
  X 3          15|      1412|       531|        15|       103|         3|
 -------------------------------------------------------------------------------
  W 4          21|      2264|      1943|        21|       118|         4|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=          56DP  conft+indsym=         280DP  drtbuffer=         434 DP

dimension of the ci-matrix ->>>      4207

 executing brd_struct for civct
 gentasklist: ntask=                    20
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS   
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      15      46       1412         33      15      33
     2  4   1    25      two-ext wz   2X  4 1      21      46       2264         33      21      33
     3  4   3    26      two-ext wx*  WX  4 3      21      15       2264       1412      21      15
     4  4   3    27      two-ext wx+  WX  4 3      21      15       2264       1412      21      15
     5  2   1    11      one-ext yz   1X  2 1      70      46        498         33      70      33
     6  3   2    15      1ex3ex yx    3X  3 2      15      70       1412        498      15      70
     7  4   2    16      1ex3ex yw    3X  4 2      21      70       2264        498      21      70
     8  1   1     1      allint zz    OX  1 1      46      46         33         33      33      33
     9  2   2     5      0ex2ex yy    OX  2 2      70      70        498        498      70      70
    10  3   3     6      0ex2ex xx*   OX  3 3      15      15       1412       1412      15      15
    11  3   3    18      0ex2ex xx+   OX  3 3      15      15       1412       1412      15      15
    12  4   4     7      0ex2ex ww*   OX  4 4      21      21       2264       2264      21      21
    13  4   4    19      0ex2ex ww+   OX  4 4      21      21       2264       2264      21      21
    14  2   2    42      four-ext y   4X  2 2      70      70        498        498      70      70
    15  3   3    43      four-ext x   4X  3 3      15      15       1412       1412      15      15
    16  4   4    44      four-ext w   4X  4 4      21      21       2264       2264      21      21
    17  1   1    75      dg-024ext z  OX  1 1      46      46         33         33      33      33
    18  2   2    76      dg-024ext y  OX  2 2      70      70        498        498      70      70
    19  3   3    77      dg-024ext x  OX  3 3      15      15       1412       1412      15      15
    20  4   4    78      dg-024ext w  OX  4 4      21      21       2264       2264      21      21
----------------------------------------------------------------------------------------------------
REDTASK #   1 TIME=  19.000 N=  1 (task/type/sgbra)=(   1/24/0) (
REDTASK #   2 TIME=  18.000 N=  1 (task/type/sgbra)=(   2/25/0) (
REDTASK #   3 TIME=  17.000 N=  1 (task/type/sgbra)=(   3/26/1) (
REDTASK #   4 TIME=  16.000 N=  1 (task/type/sgbra)=(   4/27/2) (
REDTASK #   5 TIME=  15.000 N=  1 (task/type/sgbra)=(   5/11/0) (
REDTASK #   6 TIME=  14.000 N=  1 (task/type/sgbra)=(   6/15/0) (
REDTASK #   7 TIME=  13.000 N=  1 (task/type/sgbra)=(   7/16/0) (
REDTASK #   8 TIME=  12.000 N=  1 (task/type/sgbra)=(   8/ 1/0) (
REDTASK #   9 TIME=  11.000 N=  1 (task/type/sgbra)=(   9/ 5/0) (
REDTASK #  10 TIME=  10.000 N=  1 (task/type/sgbra)=(  10/ 6/1) (
REDTASK #  11 TIME=   9.000 N=  1 (task/type/sgbra)=(  11/18/2) (
REDTASK #  12 TIME=   8.000 N=  1 (task/type/sgbra)=(  12/ 7/1) (
REDTASK #  13 TIME=   7.000 N=  1 (task/type/sgbra)=(  13/19/2) (
REDTASK #  14 TIME=   6.000 N=  1 (task/type/sgbra)=(  14/42/1) (
REDTASK #  15 TIME=   5.000 N=  1 (task/type/sgbra)=(  15/43/1) (
REDTASK #  16 TIME=   4.000 N=  1 (task/type/sgbra)=(  16/44/1) (
REDTASK #  17 TIME=   3.000 N=  1 (task/type/sgbra)=(  17/75/1) (
REDTASK #  18 TIME=   2.000 N=  1 (task/type/sgbra)=(  18/76/1) (
REDTASK #  19 TIME=   1.000 N=  1 (task/type/sgbra)=(  19/77/1) (
REDTASK #  20 TIME=   0.000 N=  1 (task/type/sgbra)=(  20/78/1) (
 initializing v-file: 1:                  4207

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         177 2x:           0 4x:           0
All internal counts: zz :         345 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 reference space has dimension      33
 dsyevx: computed roots 1 to    2(converged:   2)

    root           eigenvalues
    ----           ------------
       1         -46.2740792806
       2         -46.2740792722

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt=                    33

         vector  1 from unit 11 written to unit 49 filename cirefv              
 ncorel,neli=                     4                     4
  This is a  mraqcc energy evaluation      

    ------------------------------------------------------------

 setopt2: mraqcc energy evaluation      gvalue=  0.833333333333333      ncorel=
                     4

    ------------------------------------------------------------


    reference energies:

  reference state used is:   1with energy      -46.2740792806

  ### begin active excitation selection ###

 inactive(1:nintern)=AAAAAA
 There are         33 reference CSFs out of         33 valid zwalks.
 There are    0 inactive and    6 active orbitals.
      33 active-active  and        0 inactive excitations.

  ### end active excitation selection ###


 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              4207
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                   30
 residual norm convergence criteria:               0.001000

          starting ci iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         603 2x:         246 4x:         106
All internal counts: zz :         345 yy:         620 xx:          66 ww:         132
One-external counts: yz :        1106 yx:        1000 yw:        1210
Two-external counts: yy :         865 ww:         252 xx:         180 xz:          69 wz:         123 wx:         360
Three-ext.   counts: yx :         319 yw:         409

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1
   ht   1    -2.77549162
calca4: root=   1 anorm**2=  0.00000000 scale:  1.00000000

          calcsovref: tciref block   1

              civs   1
 refs   1    1.00000    

          calcsovref: scrb block   1

                ci   1
 civs   1    1.00000    

          calcsovref: sovref block   1

              v      1
 ref    1    1.00000    

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  1  1    -46.2740792806  7.1054E-15  5.6605E-02  2.2790E-01  1.0000E-03
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   2

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         603 2x:         246 4x:         106
All internal counts: zz :         345 yy:         620 xx:          66 ww:         132
One-external counts: yz :        1106 yx:        1000 yw:        1210
Two-external counts: yy :         865 ww:         252 xx:         180 xz:          69 wz:         123 wx:         360
Three-ext.   counts: yx :         319 yw:         409

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2
   ht   1    -2.77549162
   ht   2    -0.05660475    -0.18299796
calca4: root=   1 anorm**2=  0.06272560 scale:  1.03088583
calca4: root=   2 anorm**2=  0.93727440 scale:  1.39186005

          calcsovref: tciref block   1

              civs   1       civs   2
 refs   1    1.00000      -4.385146E-14

          calcsovref: scrb block   1

                ci   1         ci   2
 civs   1   0.968129      -0.250451    
 civs   2   0.845615        3.26876    

          calcsovref: sovref block   1

              v      1       v      2
 ref    1   0.968129      -0.250451    

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.96812933    -0.25045079
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  2  1    -46.3235208343  4.9442E-02  2.4014E-03  4.5231E-02  1.0000E-03
 mraqcc  #  2  2    -45.5353011275  2.0367E+00  0.0000E+00  5.0425E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   3

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         603 2x:         246 4x:         106
All internal counts: zz :         345 yy:         620 xx:          66 ww:         132
One-external counts: yz :        1106 yx:        1000 yw:        1210
Two-external counts: yy :         865 ww:         252 xx:         180 xz:          69 wz:         123 wx:         360
Three-ext.   counts: yx :         319 yw:         409

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3
   ht   1    -2.77549162
   ht   2    -0.05660475    -0.18299796
   ht   3     0.00537089    -0.00367572    -0.00843645
calca4: root=   1 anorm**2=  0.07385063 scale:  1.03626764
calca4: root=   2 anorm**2=  0.93027887 scale:  1.38934476
calca4: root=   3 anorm**2=  0.95083406 scale:  1.39672262

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3
 refs   1    1.00000      -4.385146E-14  -1.929271E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3
 civs   1   0.964382      -0.164650       0.209383    
 civs   2   0.885822        1.56556       -2.85796    
 civs   3    1.10015        13.9193        8.31122    

          calcsovref: sovref block   1

              v      1       v      2       v      3
 ref    1   0.962259      -0.191504       0.193348    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.96225928    -0.19150355     0.19334805
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  3  1    -46.3261690664  2.6482E-03  3.6164E-04  1.7236E-02  1.0000E-03
 mraqcc  #  3  2    -45.8125100661  2.7721E-01  0.0000E+00  3.9823E-01  1.0000E-04
 mraqcc  #  3  3    -45.4366834722  1.9381E+00  0.0000E+00  5.1129E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   4

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         603 2x:         246 4x:         106
All internal counts: zz :         345 yy:         620 xx:          66 ww:         132
One-external counts: yz :        1106 yx:        1000 yw:        1210
Two-external counts: yy :         865 ww:         252 xx:         180 xz:          69 wz:         123 wx:         360
Three-ext.   counts: yx :         319 yw:         409

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4
   ht   1    -2.77549162
   ht   2    -0.05660475    -0.18299796
   ht   3     0.00537089    -0.00367572    -0.00843645
   ht   4    -0.00019493    -0.00334764    -0.00041868    -0.00142386
calca4: root=   1 anorm**2=  0.07861502 scale:  1.03856392
calca4: root=   2 anorm**2=  0.94786962 scale:  1.39566100
calca4: root=   3 anorm**2=  0.76298544 scale:  1.32777462
calca4: root=   4 anorm**2=  0.95590161 scale:  1.39853552

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4
 refs   1    1.00000      -4.385146E-14  -1.929271E-03   9.197901E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4
 civs   1   0.962159       0.162008       3.569431E-02  -0.218472    
 civs   2   0.890401       -1.01355      -0.776167        3.04169    
 civs   3    1.23985       -11.3214       -9.38561       -6.80556    
 civs   4   0.937918       -21.9367        32.2332       -6.11334    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4
 ref    1   0.959853       0.181832       5.676647E-02  -0.205905    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.95985305     0.18183209     0.05676647    -0.20590479
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  4  1    -46.3265083671  3.3930E-04  6.6589E-05  7.5033E-03  1.0000E-03
 mraqcc  #  4  2    -45.9668500549  1.5434E-01  0.0000E+00  2.0530E-01  1.0000E-04
 mraqcc  #  4  3    -45.4685314725  3.1848E-02  0.0000E+00  5.6163E-01  1.0000E-04
 mraqcc  #  4  4    -45.4355689170  1.9370E+00  0.0000E+00  5.1353E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000999
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   5

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         603 2x:         246 4x:         106
All internal counts: zz :         345 yy:         620 xx:          66 ww:         132
One-external counts: yz :        1106 yx:        1000 yw:        1210
Two-external counts: yy :         865 ww:         252 xx:         180 xz:          69 wz:         123 wx:         360
Three-ext.   counts: yx :         319 yw:         409

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1    -2.77549162
   ht   2    -0.05660475    -0.18299796
   ht   3     0.00537089    -0.00367572    -0.00843645
   ht   4    -0.00019493    -0.00334764    -0.00041868    -0.00142386
   ht   5     0.00241728    -0.00042266    -0.00045676     0.00009568    -0.00029164
calca4: root=   1 anorm**2=  0.07987137 scale:  1.03916860
calca4: root=   2 anorm**2=  0.94052720 scale:  1.39302807
calca4: root=   3 anorm**2=  0.43835462 scale:  1.19931423
calca4: root=   4 anorm**2=  0.91477666 scale:  1.38375455
calca4: root=   5 anorm**2=  0.97671868 scale:  1.40595828

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1    1.00000      -4.385146E-14  -1.929271E-03   9.197901E-05  -8.678438E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 civs   1   0.962174       0.129020       0.145823      -0.192100      -7.272496E-02
 civs   2   0.890931      -0.786814       -1.06848        2.68900        1.40051    
 civs   3    1.26155       -9.89352       0.491038        3.66107       -12.8753    
 civs   4    1.08441       -22.4226       -14.5332       -27.6674        12.4043    
 civs   5   0.796800       -30.8921        67.1511       -16.8468        48.4483    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1   0.959149       0.172854       8.526213E-02  -0.187088      -8.878958E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     0.95914873     0.17285416     0.08526213    -0.18708806    -0.08878958
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  5  1    -46.3265614279  5.3061E-05  6.9997E-06  2.3861E-03  1.0000E-03
 mraqcc  #  5  2    -46.0267124010  5.9862E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  5  3    -45.5681578744  9.9626E-02  0.0000E+00  6.1728E-01  1.0000E-04
 mraqcc  #  5  4    -45.4511224159  1.5553E-02  0.0000E+00  5.3629E-01  1.0000E-04
 mraqcc  #  5  5    -45.3919983448  1.8934E+00  0.0000E+00  5.4458E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   6

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         603 2x:         246 4x:         106
All internal counts: zz :         345 yy:         620 xx:          66 ww:         132
One-external counts: yz :        1106 yx:        1000 yw:        1210
Two-external counts: yy :         865 ww:         252 xx:         180 xz:          69 wz:         123 wx:         360
Three-ext.   counts: yx :         319 yw:         409

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1    -2.77549162
   ht   2    -0.05660475    -0.18299796
   ht   3     0.00537089    -0.00367572    -0.00843645
   ht   4    -0.00019493    -0.00334764    -0.00041868    -0.00142386
   ht   5     0.00241728    -0.00042266    -0.00045676     0.00009568    -0.00029164
   ht   6    -0.00034145     0.00020600    -0.00002052    -0.00003578     0.00001575    -0.00003034
calca4: root=   1 anorm**2=  0.07998663 scale:  1.03922405
calca4: root=   2 anorm**2=  0.96120592 scale:  1.40043062
calca4: root=   3 anorm**2=  0.82204393 scale:  1.34983107
calca4: root=   4 anorm**2=  0.38127279 scale:  1.17527562
calca4: root=   5 anorm**2=  0.98136214 scale:  1.40760866
calca4: root=   6 anorm**2=  0.93919462 scale:  1.39254968

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1    1.00000      -4.385146E-14  -1.929271E-03   9.197901E-05  -8.678438E-04   1.216295E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1  -0.962099       0.119036       0.102938      -0.127314      -1.609381E-02  -0.198668    
 civs   2  -0.890968      -0.658214       -1.15805       0.823735      -0.130041        2.92455    
 civs   3   -1.26425       -9.06811       -4.88118       -1.03183        12.6136       -3.55305    
 civs   4   -1.10276       -22.0174       1.476138E-02    16.3191       -23.1251       -19.1224    
 civs   5  -0.896680       -35.7386        20.6813       -64.2890       -49.5781        9.02859    
 civs   6  -0.950396       -69.0757        244.771        21.4809        47.9715        102.415    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.959099       0.157119       0.124180      -6.541649E-02   6.304867E-03  -0.188951    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.95909915     0.15711945     0.12418019    -0.06541649     0.00630487    -0.18895110

 trial vector basis is being transformed.  new dimension:   2
 fact= -7.758049499999998E-003
 factd= -7.758049499999998E-003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  6  1    -46.3265680804  6.6525E-06  7.1308E-07  8.0670E-04  1.0000E-03
 mraqcc  #  6  2    -46.0465917268  1.9879E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  6  3    -45.7530417082  1.8488E-01  0.0000E+00  4.3307E-01  1.0000E-04
 mraqcc  #  6  4    -45.5663974030  1.1527E-01  0.0000E+00  6.3332E-01  1.0000E-04
 mraqcc  #  6  5    -45.3924848669  4.8652E-04  0.0000E+00  5.5074E-01  1.0000E-04
 mraqcc  #  6  6    -45.3898603562  1.8913E+00  0.0000E+00  5.2589E-01  1.0000E-04
 
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000999
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mraqcc   convergence criteria satisfied after  6 iterations.

 final mraqcc   convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  6  1    -46.3265680804  6.6525E-06  7.1308E-07  8.0670E-04  1.0000E-03
 mraqcc  #  6  2    -46.0465917268  1.9879E-02  0.0000E+00  0.0000E+00  1.0000E-04

####################CIUDGINFO####################

   aqcc(lrt) vector at position   1 energy=  -46.326568080361

################END OF CIUDGINFO################

 
 a4den factor for root 1  =  1.013511225
diagon:itrnv=   0
    1 of the   3 expansion vectors are transformed.
    1 of the   2 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
maximum overlap with reference    1(overlap= 0.95910)
reference energy =                         -46.2740792806
total aqcc energy =                        -46.3265680804
diagonal element shift (lrtshift) =         -0.0524887998

 information on vector: 1 from unit 11 written to unit 48 filename civout              


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -46.3265680804

                                                       internal orbitals

                                          level       1    2    3    4    5    6

                                          orbital     3    4    5   17   22   31

                                         symmetry   a1   a1   a1   a2   b1   b2 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1  0.459327                        +-   +-                     
 z*  1  1       2  0.241175                        +-   +     -                
 z*  1  1       3 -0.459311                        +-        +-                
 z*  1  1       4  0.209649                        +-             +-           
 z*  1  1       5  0.278910                        +-                  +-      
 z*  1  1       6 -0.488508                        +-                       +- 
 z*  1  1       7 -0.040544                        +    +-    -                
 z*  1  1       8  0.131532                        +     -   +-                
 z*  1  1      10 -0.034206                        +     -             +-      
 z*  1  1      11  0.135206                        +     -                  +- 
 z*  1  1      12 -0.088345                        +          -   +-           
 z*  1  1      13  0.092904                        +          -        +-      
 z*  1  1      14  0.023883                        +          -             +- 
 z*  1  1      15  0.055360                        +               -   +     - 
 z*  1  1      16  0.117157                        +              +     -    - 
 z*  1  1      18 -0.034058                             +-        +-           
 z*  1  1      19 -0.026616                             +-             +-      
 z*  1  1      22 -0.025562                             +     -        +-      
 z*  1  1      25 -0.016952                             +         +     -    - 
 z*  1  1      26  0.024623                                  +-   +-           
 z*  1  1      27  0.014019                                  +-        +-      
 z*  1  1      28  0.022130                                  +-             +- 
 z*  1  1      29 -0.016924                                  +     -   +     - 
 z*  1  1      31 -0.042550                                       +-   +-      
 z*  1  1      32  0.024317                                       +-        +- 
 z*  1  1      33  0.018249                                            +-   +- 
 y   1  1      36 -0.019131              3( a1 )   +-    -                     
 y   1  1      38  0.056999              5( a1 )   +-    -                     
 y   1  1      40  0.014199              7( a1 )   +-    -                     
 y   1  1      46  0.019140              2( a1 )   +-         -                
 y   1  1      49  0.017599              5( a1 )   +-         -                
 y   1  1      50  0.014206              6( a1 )   +-         -                
 y   1  1      60 -0.011411              2( b1 )   +-                   -      
 y   1  1      67  0.019978              2( b2 )   +-                        - 
 y   1  1      69 -0.015495              4( b2 )   +-                        - 
 y   1  1      77 -0.015519              5( a1 )    -   +-                     
 y   1  1     114  0.010163              3( a1 )    -        +-                
 y   1  1     116  0.015517              5( a1 )    -        +-                
 y   1  1     118 -0.012494              7( a1 )    -        +-                
 y   1  1     152 -0.014327              2( b2 )    -             +     -      
 y   1  1     154  0.014148              4( b2 )    -             +     -      
 y   1  1     166  0.011145              2( a1 )    -                  +-      
 y   1  1     181  0.011814              3( a1 )    -                       +- 
 y   1  1     183  0.016507              5( a1 )    -                       +- 
 y   1  1     185 -0.013454              7( a1 )    -                       +- 
 y   1  1     191 -0.010765              2( a1 )   +     -    -                
 y   1  1     195 -0.015428              6( a1 )   +     -    -                
 y   1  1     212 -0.011261              2( b2 )   +     -                   - 
 y   1  1     214  0.016963              4( b2 )   +     -                   - 
 y   1  1     219 -0.010151              2( a2 )   +          -    -           
 y   1  1     224  0.011285              4( b1 )   +          -         -      
 y   1  1     245  0.010262              4( b1 )   +               -         - 
 y   1  1     250  0.011288              2( a2 )   +                    -    - 
 y   1  1     263  0.010153              1( a2 )        +-         -           
 y   1  1     264 -0.011256              2( a2 )        +-         -           
 y   1  1     393  0.010214              2( a2 )             +-    -           
 y   1  1     477  0.010380              2( b1 )                  +-    -      
 y   1  1     479 -0.011029              4( b1 )                  +-    -      
 y   1  1     490 -0.010713              1( a2 )                   -   +-      
 y   1  1     491  0.011800              2( a2 )                   -   +-      
 y   1  1     505 -0.010384              2( a2 )                   -        +- 
 x   1  1     540  0.016288    3( a1 )   5( a1 )    -    -                     
 x   1  1     551  0.015643    5( a1 )   7( a1 )    -    -                     
 x   1  1     605 -0.010996    1( b1 )   7( b1 )    -    -                     
 x   1  1     607 -0.011713    3( b1 )   7( b1 )    -    -                     
 x   1  1     626 -0.013247    1( b2 )   7( b2 )    -    -                     
 x   1  1     628 -0.013539    3( b2 )   7( b2 )    -    -                     
 x   1  1     639 -0.016293    2( a1 )   5( a1 )    -         -                
 x   1  1     646  0.015645    5( a1 )   6( a1 )    -         -                
 x   1  1     660  0.011757    1( a1 )   9( a1 )    -         -                
 x   1  1     663  0.012335    4( a1 )   9( a1 )    -         -                
 x   1  1     700  0.010999    1( b1 )   6( b1 )    -         -                
 x   1  1     702  0.011390    3( b1 )   6( b1 )    -         -                
 x   1  1     721  0.010648    1( b2 )   6( b2 )    -         -                
 x   1  1     723  0.010895    3( b2 )   6( b2 )    -         -                
 x   1  1     914  0.013309    3( a2 )   1( b1 )    -                        - 
 x   1  1     920  0.013704    3( a2 )   3( b1 )    -                        - 
 x   1  1     942  0.011974   10( a1 )   1( b2 )    -                        - 
 x   1  1     948  0.016985    5( a1 )   2( b2 )    -                        - 
 x   1  1     964  0.012574   10( a1 )   3( b2 )    -                        - 
 x   1  1     970 -0.017036    5( a1 )   4( b2 )    -                        - 
 x   1  1     988 -0.012905    1( a1 )   6( b2 )    -                        - 
 x   1  1     991 -0.013406    4( a1 )   6( b2 )    -                        - 
 x   1  1    1143 -0.010679    1( b1 )   1( b2 )         -         -           
 x   1  1    1589  0.011553    1( a1 )   1( b2 )              -              - 
 x   1  1    1687 -0.011413    1( a1 )   1( b2 )                   -    -      
 w   1  1    1950  0.012943    1( a1 )   4( a1 )   +-                          
 w   1  1    1961  0.012673    3( a1 )   6( a1 )   +-                          
 w   1  1    1966 -0.012674    2( a1 )   7( a1 )   +-                          
 w   1  1    1970 -0.022783    6( a1 )   7( a1 )   +-                          
 w   1  1    1997 -0.022519    9( a1 )  10( a1 )   +-                          
 w   1  1    2016 -0.023099    1( b1 )   1( b1 )   +-                          
 w   1  1    2019 -0.030120    1( b1 )   3( b1 )   +-                          
 w   1  1    2021 -0.020688    3( b1 )   3( b1 )   +-                          
 w   1  1    2026 -0.012077    1( b1 )   5( b1 )   +-                          
 w   1  1    2028 -0.013691    3( b1 )   5( b1 )   +-                          
 w   1  1    2042  0.011787    6( b1 )   7( b1 )   +-                          
 w   1  1    2044  0.013182    1( b2 )   1( b2 )   +-                          
 w   1  1    2047  0.017187    1( b2 )   3( b2 )   +-                          
 w   1  1    2049  0.011807    3( b2 )   3( b2 )   +-                          
 w   1  1    2051 -0.014471    2( b2 )   4( b2 )   +-                          
 w   1  1    2053  0.017385    4( b2 )   4( b2 )   +-                          
 w   1  1    2064  0.016373    6( b2 )   6( b2 )   +-                          
 w   1  1    2070  0.015166    6( b2 )   7( b2 )   +-                          
 w   1  1    2144 -0.037460    1( b1 )   1( b1 )   +     -                     
 w   1  1    2147 -0.035965    1( b1 )   3( b1 )   +     -                     
 w   1  1    2149 -0.019213    3( b1 )   3( b1 )   +     -                     
 w   1  1    2167  0.010329    3( b1 )   7( b1 )   +     -                     
 w   1  1    2175  0.013301    1( b2 )   3( b2 )   +     -                     
 w   1  1    2200  0.024374    1( a1 )   1( a1 )   +          -                
 w   1  1    2206  0.026738    1( a1 )   4( a1 )   +          -                
 w   1  1    2209  0.015581    4( a1 )   4( a1 )   +          -                
 w   1  1    2300 -0.025670    1( b2 )   1( b2 )   +          -                
 w   1  1    2303 -0.025959    1( b2 )   3( b2 )   +          -                
 w   1  1    2305 -0.014375    3( b2 )   3( b2 )   +          -                
 w   1  1    2361  0.015308    1( b1 )   1( b2 )   +               -           
 w   1  1    2363  0.011978    3( b1 )   1( b2 )   +               -           
 w   1  1    2375  0.010817    1( b1 )   3( b2 )   +               -           
 w   1  1    2410  0.020379    1( a1 )   1( b1 )   +                    -      
 w   1  1    2413  0.014642    4( a1 )   1( b1 )   +                    -      
 w   1  1    2432  0.015704    1( a1 )   3( b1 )   +                    -      
 w   1  1    2435  0.012192    4( a1 )   3( b1 )   +                    -      
 w   1  1    2529 -0.035688    1( a1 )   1( b2 )   +                         - 
 w   1  1    2532 -0.026519    4( a1 )   1( b2 )   +                         - 
 w   1  1    2538  0.010010   10( a1 )   1( b2 )   +                         - 
 w   1  1    2544 -0.010374    5( a1 )   2( b2 )   +                         - 
 w   1  1    2551 -0.026623    1( a1 )   3( b2 )   +                         - 
 w   1  1    2554 -0.021350    4( a1 )   3( b2 )   +                         - 
 w   1  1    2560  0.010806   10( a1 )   3( b2 )   +                         - 
 w   1  1    2584  0.010583    1( a1 )   6( b2 )   +                         - 
 w   1  1    2587  0.011347    4( a1 )   6( b2 )   +                         - 
 w   1  1    2606 -0.033887    1( a1 )   1( a1 )        +-                     
 w   1  1    2612 -0.029678    1( a1 )   4( a1 )        +-                     
 w   1  1    2615 -0.015339    4( a1 )   4( a1 )        +-                     
 w   1  1    2620 -0.011317    5( a1 )   5( a1 )        +-                     
 w   1  1    2678 -0.033707    1( b1 )   1( b1 )        +-                     
 w   1  1    2681 -0.027950    1( b1 )   3( b1 )        +-                     
 w   1  1    2683 -0.013883    3( b1 )   3( b1 )        +-                     
 w   1  1    2706 -0.040585    1( b2 )   1( b2 )        +-                     
 w   1  1    2709 -0.035755    1( b2 )   3( b2 )        +-                     
 w   1  1    2711 -0.018382    3( b2 )   3( b2 )        +-                     
 w   1  1    2734 -0.023590    1( a1 )   1( a1 )        +     -                
 w   1  1    2740 -0.020472    1( a1 )   4( a1 )        +     -                
 w   1  1    2743 -0.010353    4( a1 )   4( a1 )        +     -                
 w   1  1    2806 -0.023476    1( b1 )   1( b1 )        +     -                
 w   1  1    2809 -0.020370    1( b1 )   3( b1 )        +     -                
 w   1  1    2811 -0.010306    3( b1 )   3( b1 )        +     -                
 w   1  1    3063  0.011129    1( a1 )   1( b2 )        +                    - 
 w   1  1    3140  0.026989    1( a1 )   1( a1 )             +-                
 w   1  1    3146  0.022651    1( a1 )   4( a1 )             +-                
 w   1  1    3149  0.011525    4( a1 )   4( a1 )             +-                
 w   1  1    3154  0.011321    5( a1 )   5( a1 )             +-                
 w   1  1    3212  0.049768    1( b1 )   1( b1 )             +-                
 w   1  1    3215  0.044329    1( b1 )   3( b1 )             +-                
 w   1  1    3217  0.022778    3( b1 )   3( b1 )             +-                
 w   1  1    3240  0.031454    1( b2 )   1( b2 )             +-                
 w   1  1    3243  0.026431    1( b2 )   3( b2 )             +-                
 w   1  1    3245  0.013322    3( b2 )   3( b2 )             +-                
 w   1  1    3546 -0.022693    1( a1 )   1( a1 )                  +-           
 w   1  1    3552 -0.020179    1( a1 )   4( a1 )                  +-           
 w   1  1    3555 -0.010359    4( a1 )   4( a1 )                  +-           
 w   1  1    3618 -0.016509    1( b1 )   1( b1 )                  +-           
 w   1  1    3621 -0.013503    1( b1 )   3( b1 )                  +-           
 w   1  1    3646 -0.010175    1( b2 )   1( b2 )                  +-           
 w   1  1    3695  0.013978    1( a1 )   1( b2 )                  +     -      
 w   1  1    3870 -0.014877    1( a1 )   1( a1 )                       +-      
 w   1  1    3876 -0.012852    1( a1 )   4( a1 )                       +-      
 w   1  1    3942 -0.020662    1( b1 )   1( b1 )                       +-      
 w   1  1    3945 -0.017038    1( b1 )   3( b1 )                       +-      
 w   1  1    3970 -0.030195    1( b2 )   1( b2 )                       +-      
 w   1  1    3973 -0.026859    1( b2 )   3( b2 )                       +-      
 w   1  1    3975 -0.013787    3( b2 )   3( b2 )                       +-      
 w   1  1    4080  0.030824    1( a1 )   1( a1 )                            +- 
 w   1  1    4086  0.025955    1( a1 )   4( a1 )                            +- 
 w   1  1    4089  0.013174    4( a1 )   4( a1 )                            +- 
 w   1  1    4094  0.012045    5( a1 )   5( a1 )                            +- 
 w   1  1    4152  0.052898    1( b1 )   1( b1 )                            +- 
 w   1  1    4155  0.047048    1( b1 )   3( b1 )                            +- 
 w   1  1    4157  0.024153    3( b1 )   3( b1 )                            +- 
 w   1  1    4180  0.031395    1( b2 )   1( b2 )                            +- 
 w   1  1    4183  0.026367    1( b2 )   3( b2 )                            +- 
 w   1  1    4185  0.013340    3( b2 )   3( b2 )                            +- 

 ci coefficient statistics:
           rq > 0.1                9
      0.1> rq > 0.01             173
     0.01> rq > 0.001            979
    0.001> rq > 0.0001           765
   0.0001> rq > 0.00001          217
  0.00001> rq > 0.000001          28
 0.000001> rq                   2036
           all                  4207
  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.459326915714
     2     2      0.241175387178
     3     3     -0.459310609491
     4     4      0.209649486235
     5     5      0.278909954992
     6     6     -0.488508067945
     7     7     -0.040544398392
     8     8      0.131531504155
     9     9     -0.009300032061
    10    10     -0.034205552371
    11    11      0.135206374892
    12    12     -0.088344996889
    13    13      0.092904029519
    14    14      0.023883220345
    15    15      0.055360089205
    16    16      0.117156772552
    17    17      0.000008095368
    18    18     -0.034058417161
    19    19     -0.026615704106
    20    20     -0.000050326094
    21    21     -0.003099870951
    22    22     -0.025562131755
    23    23     -0.003226672567
    24    24     -0.006887038195
    25    25     -0.016951886303
    26    26      0.024622877803
    27    27      0.014018587152
    28    28      0.022130487304
    29    29     -0.016924347009
    30    30      0.006897688491
    31    31     -0.042550175929
    32    32      0.024316778995
    33    33      0.018248713776

 number of reference csfs (nref) is    33.  root number (iroot) is  1.
 c0**2 =   0.92001337  c0**2(renormalized) =   0.99360214
 c**2 (all zwalks) =   0.92001337  c**2(all zwalks, renormalized) =   0.99360214
 passed aftci ... 
 readint2: molcas,dalton2=                     0                     0
 files%faoints=aoints              
lodens (list->root)=  1
sifcfg setup: record length 4096 DP
# d1 elements per record  3272
# d2 elements per record  2730
  The AQCC density will be calculated.
 item #                     1 suffix=:.drt1.state1:
 read_civout: repnuc=  -43.4985876562748     
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method: 30 last record  1max overlap with ref# 96% root-following 0
 MR-CISD energy:   -46.32656808    -2.82798042
 residuum:     0.00080670
 deltae:     0.00000665
 apxde:     0.00000071
 a4den:     1.01351123
 reference energy:   -46.27407928    -2.77549162

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.95909915     0.15711945     0.12418019    -0.06541649     0.00630487    -0.18895110     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.95909915     0.15711945     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 rdcirefv: extracting reference vector # 1(blocksize=    33 #zcsf=      33)
 =========== Executing IN-CORE method ==========
================================================================================
   DYZ=       0  DYX=       0  DYW=       0
   D0Z=      42  D0Y=       0  D0X=       0  D0W=       0
  DDZI=      90 DDYI=       0 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
 root #                      1 : Scaling(1) with    1.01351122533359      
 corresponding to ref #                      1
 computing final density
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=     113  DYX=     130  DYW=     160
   D0Z=      42  D0Y=     103  D0X=      12  D0W=      18
  DDZI=      90 DDYI=     180 DDXI=      30 DDWI=      36
  DDZE=       0 DDYE=      70 DDXE=      15 DDWE=      21
================================================================================
Trace of MO density:     4.000000
    4  correlated and     8  frozen core electrons

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     1.80534536    -0.20557360    -0.01146175     0.00000000     0.01265543    -0.01870676
   MO   4     0.00000000     0.00000000    -0.20557360     0.55824929    -0.00340041     0.00000000    -0.01132314     0.01457257
   MO   5     0.00000000     0.00000000    -0.01146175    -0.00340041     0.56827740     0.00000000     0.01529624     0.01136545
   MO   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.01922931     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000     0.01265543    -0.01132314     0.01529624     0.00000000     0.00181303    -0.00002473
   MO   8     0.00000000     0.00000000    -0.01870676     0.01457257     0.01136545     0.00000000    -0.00002473     0.00187134
   MO   9     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.01361384     0.00000000     0.00000000
   MO  10     0.00000000     0.00000000    -0.04067314    -0.03559736    -0.00200171     0.00000000     0.00076216    -0.00112979
   MO  11     0.00000000     0.00000000     0.01184542    -0.01107048     0.01173744     0.00000000     0.00189097    -0.00025292
   MO  12     0.00000000     0.00000000     0.01370232    -0.01129363    -0.01114636     0.00000000    -0.00020018    -0.00193975
   MO  13     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00290195     0.00000000     0.00000000
   MO  14     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00051275     0.00000000     0.00000000
   MO  15     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00002544     0.00000000     0.00000000
   MO  16     0.00000000     0.00000000     0.00037432     0.00159130     0.00008933     0.00000000    -0.00004063     0.00006016

                MO   9         MO  10         MO  11         MO  12         MO  13         MO  14         MO  15         MO  16
   MO   3     0.00000000    -0.04067314     0.01184542     0.01370232     0.00000000     0.00000000     0.00000000     0.00037432
   MO   4     0.00000000    -0.03559736    -0.01107048    -0.01129363     0.00000000     0.00000000     0.00000000     0.00159130
   MO   5     0.00000000    -0.00200171     0.01173744    -0.01114636     0.00000000     0.00000000     0.00000000     0.00008933
   MO   6     0.01361384     0.00000000     0.00000000     0.00000000     0.00290195     0.00051275     0.00002544     0.00000000
   MO   7     0.00000000     0.00076216     0.00189097    -0.00020018     0.00000000     0.00000000     0.00000000    -0.00004063
   MO   8     0.00000000    -0.00112979    -0.00025292    -0.00193975     0.00000000     0.00000000     0.00000000     0.00006016
   MO   9     0.01005818     0.00000000     0.00000000     0.00000000     0.00243351     0.00020464    -0.00009060     0.00000000
   MO  10     0.00000000     0.00885398     0.00075197     0.00087244     0.00000000     0.00000000     0.00000000    -0.00039068
   MO  11     0.00000000     0.00075197     0.00225844     0.00005201     0.00000000     0.00000000     0.00000000    -0.00004415
   MO  12     0.00000000     0.00087244     0.00005201     0.00233446     0.00000000     0.00000000     0.00000000    -0.00005115
   MO  13     0.00243351     0.00000000     0.00000000     0.00000000     0.00081244    -0.00000645    -0.00007402     0.00000000
   MO  14     0.00020464     0.00000000     0.00000000     0.00000000    -0.00000645     0.00182688    -0.00038977     0.00000000
   MO  15    -0.00009060     0.00000000     0.00000000     0.00000000    -0.00007402    -0.00038977     0.00212389     0.00000000
   MO  16     0.00000000    -0.00039068    -0.00004415    -0.00005115     0.00000000     0.00000000     0.00000000     0.00002245

Natural orbital populations,block 1
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     2.00000000     1.83967215     0.56992031     0.52867536     0.02951649     0.00517229     0.00286235
              MO     9       MO    10       MO    11       MO    12       MO    13       MO    14       MO    15       MO    16
  occ(*)=     0.00254432     0.00238847     0.00158710     0.00054484     0.00009522     0.00008058     0.00001379     0.00000318

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4
   MO   1     0.13961049     0.00700295    -0.00739256     0.00000000
   MO   2     0.00700295     0.00088898    -0.00100761     0.00000000
   MO   3    -0.00739256    -0.00100761     0.00122229     0.00000000
   MO   4     0.00000000     0.00000000     0.00000000     0.00091463

Natural orbital populations,block 2
              MO     1       MO     2       MO     3       MO     4       MO
  occ(*)=     0.14036030     0.00133063     0.00091463     0.00003083

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     0.21379250     0.00000000     0.00910712     0.00000000    -0.00892646     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.03123297     0.00000000     0.02170482     0.00000000     0.00469535     0.00237146
   MO   4     0.00000000     0.00910712     0.00000000     0.00104850     0.00000000    -0.00116186     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.02170482     0.00000000     0.01553603     0.00000000     0.00372353     0.00179204
   MO   6     0.00000000    -0.00892646     0.00000000    -0.00116186     0.00000000     0.00139803     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000     0.00469535     0.00000000     0.00372353     0.00000000     0.00121196     0.00058875
   MO   8     0.00000000     0.00000000     0.00237146     0.00000000     0.00179204     0.00000000     0.00058875     0.00141547
   MO   9     0.00000000     0.00000000    -0.00268509     0.00000000    -0.00207939     0.00000000    -0.00070338    -0.00048481

                MO   9
   MO   2     0.00000000
   MO   3    -0.00268509
   MO   4     0.00000000
   MO   5    -0.00207939
   MO   6     0.00000000
   MO   7    -0.00070338
   MO   8    -0.00048481
   MO   9     0.00138176

Natural orbital populations,block 3
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     0.21455894     0.04770204     0.00163976     0.00158461     0.00092516     0.00055461     0.00004033
              MO     9       MO
  occ(*)=     0.00001177

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     0.56631167     0.00000000     0.01906154     0.00000000    -0.01619451     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     0.02129722     0.00000000     0.01500813     0.00000000    -0.00321106     0.00028865
   MO   4     0.00000000     0.01906154     0.00000000     0.00180025     0.00000000    -0.00188922     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.01500813     0.00000000     0.01100248     0.00000000    -0.00265588     0.00012547
   MO   6     0.00000000    -0.01619451     0.00000000    -0.00188922     0.00000000     0.00222794     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000    -0.00321106     0.00000000    -0.00265588     0.00000000     0.00088127    -0.00000377
   MO   8     0.00000000     0.00000000     0.00028865     0.00000000     0.00012547     0.00000000    -0.00000377     0.00225558
   MO   9     0.00000000     0.00000000     0.00007782     0.00000000     0.00026688     0.00000000    -0.00017147     0.00035052

                MO   9
   MO   2     0.00000000
   MO   3     0.00007782
   MO   4     0.00000000
   MO   5     0.00026688
   MO   6     0.00000000
   MO   7    -0.00017147
   MO   8     0.00035052
   MO   9     0.00149351

Natural orbital populations,block 4
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     0.56742173     0.03256655     0.00283427     0.00238786     0.00142267     0.00053815     0.00008386
              MO     9       MO
  occ(*)=     0.00001484


 total number of electrons =   12.0000000000

 test slabel:                     4
  a1  a2  b1  b2 


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        a1  partial gross atomic populations
   ao class       1a1        2a1        3a1        4a1        5a1        6a1 
     3_ s       2.000000   0.000000   1.792963   0.000083   0.016324   0.000000
     3_ p       0.000000   2.000000   0.000000   0.000000   0.000000   0.029506
     3_ d       0.000000   0.000000   0.046709   0.569837   0.512352   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000000   0.000000   0.000011
 
   ao class       7a1        8a1        9a1       10a1       11a1       12a1 
     3_ s       0.004570   0.000010   0.000267   0.000000   0.000000   0.000000
     3_ p       0.000000   0.000000   0.000000   0.000000   0.000053   0.000527
     3_ d       0.000602   0.002852   0.002277   0.000000   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000000   0.002388   0.001534   0.000017
 
   ao class      13a1       14a1       15a1       16a1 
     3_ s       0.000000   0.000000   0.000000   0.000003
     3_ p       0.000000   0.000000   0.000014   0.000000
     3_ d       0.000095   0.000080   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000000

                        a2  partial gross atomic populations
   ao class       1a2        2a2        3a2        4a2 
     3_ d       0.140360   0.001331   0.000000   0.000031
     3_ f       0.000000   0.000000   0.000915   0.000000

                        b1  partial gross atomic populations
   ao class       1b1        2b1        3b1        4b1        5b1        6b1 
     3_ p       2.000000   0.000000   0.047228   0.000000   0.000255   0.000034
     3_ d       0.000000   0.214559   0.000000   0.001640   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000474   0.000000   0.001330   0.000891
 
   ao class       7b1        8b1        9b1 
     3_ p       0.000452   0.000000   0.000012
     3_ d       0.000000   0.000040   0.000000
     3_ f       0.000102   0.000000   0.000000

                        b2  partial gross atomic populations
   ao class       1b2        2b2        3b2        4b2        5b2        6b2 
     3_ p       2.000000   0.000000   0.032561   0.000000   0.000001   0.000106
     3_ d       0.000000   0.567422   0.000000   0.002834   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000005   0.000000   0.002387   0.001317
 
   ao class       7b2        8b2        9b2 
     3_ p       0.000499   0.000000   0.000015
     3_ d       0.000000   0.000084   0.000000
     3_ f       0.000040   0.000000   0.000000


                        gross atomic populations
     ao            3_
      s         3.814222
      p         6.111262
      d         2.063105
      f         0.011412
    total      12.000000
 

 Total number of electrons:   12.00000000

 DA ...
