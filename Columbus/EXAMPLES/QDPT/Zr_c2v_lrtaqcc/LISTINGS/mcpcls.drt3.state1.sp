

     ******************************************
     **    PROGRAM:              MCPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 original author: Daniel Robertson, FSU
 later revisions: Ron Shepard, ANL;
                  Michal Dallos, University Vienna



 This Version of Program mcpc is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



   ******  File header section  ******

 Headers form the restart file:
                                                                                    
    aoints SIFS file created by argos.      zam792            17:56:38.564 17-Dec-13
     title                                                                          
     title                                                                          
     title                                                                          
     title                                                                          


   ******  DRT info section  ******

 Informations for the DRT no.  1
 Header form the DRT file: 
     title                                                                          
 Molecular symmetry group:   sym1
 Total number of electrons:   12
 Spin multiplicity:            3
 Number of active orbitals:    6
 Number of active electrons:   4
 Total number of CSFs:        21

   ***  Informations from the DRT number:   1

 
 Symmetry orbital summary:
 Symm.blocks:         1     2     3     4
 Symm.labels:         a1    a2    b1    b2 

 List of doubly occupied orbitals:
  1 a1   2 a1   1 b1   1 b2 

 List of active orbitals:
  3 a1   4 a1   5 a1   1 a2   2 b1   2 b2 

 Informations for the DRT no.  2
 Header form the DRT file: 
     title                                                                          
 Molecular symmetry group:    a2 
 Total number of electrons:   12
 Spin multiplicity:            3
 Number of active orbitals:    6
 Number of active electrons:   4
 Total number of CSFs:        28

   ***  Informations from the DRT number:   2

 
 Symmetry orbital summary:
 Symm.blocks:         1     2     3     4
 Symm.labels:         a1    a2    b1    b2 

 List of doubly occupied orbitals:
  1 a1   2 a1   1 b1   1 b2 

 List of active orbitals:
  3 a1   4 a1   5 a1   1 a2   2 b1   2 b2 

 Informations for the DRT no.  3
 Header form the DRT file: 
     title                                                                          
 Molecular symmetry group:    b1 
 Total number of electrons:   12
 Spin multiplicity:            3
 Number of active orbitals:    6
 Number of active electrons:   4
 Total number of CSFs:        28

   ***  Informations from the DRT number:   3

 
 Symmetry orbital summary:
 Symm.blocks:         1     2     3     4
 Symm.labels:         a1    a2    b1    b2 

 List of doubly occupied orbitals:
  1 a1   2 a1   1 b1   1 b2 

 List of active orbitals:
  3 a1   4 a1   5 a1   1 a2   2 b1   2 b2 

 Informations for the DRT no.  4
 Header form the DRT file: 
     title                                                                          
 Molecular symmetry group:    b2 
 Total number of electrons:   12
 Spin multiplicity:            3
 Number of active orbitals:    6
 Number of active electrons:   4
 Total number of CSFs:        28

   ***  Informations from the DRT number:   4

 
 Symmetry orbital summary:
 Symm.blocks:         1     2     3     4
 Symm.labels:         a1    a2    b1    b2 

 List of doubly occupied orbitals:
  1 a1   2 a1   1 b1   1 b2 

 List of active orbitals:
  3 a1   4 a1   5 a1   1 a2   2 b1   2 b2 


   ******  MCSCF convergence information:  ******

 MCSCF convergence criteria were satisfied.

 mcscf energy=   -46.3018785757    nuclear repulsion=     0.0000000000
 demc=             0.0000000000    wnorm=                 0.0000008053
 knorm=            0.0000000310    apxde=                 0.0000000000


 MCSCF calculation performmed for   4 symmetries.

 State averaging:
 No,  ssym, navst, wavst
  1    a1     1   0.1429
  2    a2     2   0.1429 0.1429
  3    b1     2   0.1429 0.1429
  4    b2     2   0.1429 0.1429

 Input the DRT No of interest: [  1]:
In the DRT No.: 3 there are  2 states.

 Which one to take? [  1]:
 The CSFs for the state No  1 of the symmetry  b1  will be printed
 according to the following print options :

 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 9) export wave function files for cioverlap (all states).
 0) end.

 input menu number [  0]: csfs will be printed based on coefficient magnitudes.

 input the coefficient threshold (end with 0.) [ 0.0000]:
 List of active orbitals:
  3 a1   4 a1   5 a1   1 a2   2 b1   2 b2 

   csf       coeff       coeff**2    step(*)
  -----  ------------  ------------  ------------
      3  0.7068792206  0.4996782325  300101
      2 -0.6723617515  0.4520703249  301010
      1  0.1585476634  0.0251373616  310010
     14  0.0657865838  0.0043278746  101102
      7  0.0620497947  0.0038501770  112010
     10  0.0488719611  0.0023884686  110102
     16 -0.0468363471  0.0021936434  100013
     12 -0.0374518089  0.0014026380  102101
     18 -0.0334600203  0.0011195730  030101
     28  0.0328177755  0.0010770064  000131
      8  0.0324658460  0.0010540312  111020
     26 -0.0312501900  0.0009765744  001310
     17  0.0301480398  0.0009089043  031010
     11 -0.0278018919  0.0007729452  103010
      5  0.0253590476  0.0006430813  121010
     27  0.0235369260  0.0005539869  001013
     25 -0.0230181744  0.0005298364  003101
      4 -0.0183106256  0.0003352790  130010
     13 -0.0172667391  0.0002981403  101201
      9  0.0158588241  0.0002515023  110201
      6 -0.0134483917  0.0001808592  120101
     19  0.0124987944  0.0001562199  013010
     23  0.0072037109  0.0000518935  010310
     22 -0.0060125611  0.0000361509  011102
     21  0.0020269597  0.0000041086  011201

 input the coefficient threshold (end with 0.) [ 0.0000]:
 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 9) export wave function files for cioverlap (all states).
 0) end.

 input menu number [  0]: