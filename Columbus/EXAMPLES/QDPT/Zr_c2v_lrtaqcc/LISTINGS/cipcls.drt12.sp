
 program cipc      

 print the csf info for mrsdci wave functions

 written by: ron shepard

 version date: 06-jun-96

 This Version of Program cipc is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 workspace allocation parameters: lencor=  13107200 mem1=         0 ifirst=         1

 drt header information:
  cidrt_title                                                                    
 nmot  =    38 niot  =     6 nfct  =     4 nfvt  =     0
 nrow  =    23 nsym  =     4 ssym  =     4 lenbuf=  1600
 spnorb=     F spnodd=     F lxyzir(1:3)= 0 0 0
 nwalk,xbar:         50       15       20       15        0
 nvalwt,nvalw:       39        4       20       15        0
 ncsft:            1558
 map(*)=    -1 -1 29 30 31  1  2  3  4  5  6  7  8  9 10 11 32 12 13 14
            -1 33 15 16 17 18 19 20 21 -1 34 22 23 24 25 26 27 28
 mu(*)=      0  0  0  0  0  0
 syml(*) =   1  1  1  2  3  4
 rmo(*)=     3  4  5  1  2  2

 indx01:    39 indices saved in indxv(*)
 test nroots froot                      1                     1
===================================ROOT # 1===================================

 rdhciv: CI vector file information:
  cidrt_title                                                                    
 energy computed by program ciudg.       zam792            17:56:40.151 17-Dec-13

 lenrec =   32768 lenci =      1558 ninfo =  6 nenrgy =  8 ntitle =  2

 Max. overlap with ref vector #        1
 Valid ci vector #        1
 Method:       30       96% overlap
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -4.349858765627E+01, ietype=    6,   fcore energy of type: H1(*)   
 energy( 3)= -4.629196871995E+01, ietype=-1026,   total energy of type: MRSDCI  
 energy( 4)=  3.273646991965E-04, ietype=-2055, cnvginf energy of type: CI-Resid
 energy( 5)=  3.584083528185E-06, ietype=-2056, cnvginf energy of type: CI-D.E. 
 energy( 6)=  1.346650715450E-07, ietype=-2057, cnvginf energy of type: CI-ApxDE
 energy( 7)=  1.012929508767E+00, ietype=-1039,   total energy of type: a4den   
 energy( 8)= -4.624538335615E+01, ietype=-1041,   total energy of type: E(ref)  
==================================================================================
space sufficient for valid walk range           1         39
               respectively csf range           1       1558

 space is available for   4357921 coefficients.

 updated histogram parameters:
 csfmn = 0.0000E+00 csfmx = 1.0000E+00 fhist = 5.0000E-01 nhist =  20

 this program will print the csfs generated from
 the drt according to the following print options :

 1) run in batch mode: all valid roots are automatically
    analysed and csf info is printed by default contribution
    threshold 0.01 
 2) run in interactive mode
 3) generate files for cioverlap without symmetry
 4) generate files for cioverlap with symmetry

 input menu number [  1]:
================================================================================
===================================VECTOR # 1===================================
================================================================================


 rdcivnew:      95 coefficients were selected.
 workspace: ncsfmx=    1558
 ncsfmx=                  1558

 histogram parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =    1558 ncsft =    1558 ncsf =      95
 nhist =  20 fhist = 0.50000

    cmin                cmax        num  '*'=     2 csfs.
 ----------          ----------   ----- ---------|---------|---------|---------|
 5.0000E-01 <= |c| < 1.0000E+00       1 *
 2.5000E-01 <= |c| < 5.0000E-01       1 *
 1.2500E-01 <= |c| < 2.5000E-01       0
 6.2500E-02 <= |c| < 1.2500E-01       6 ***
 3.1250E-02 <= |c| < 6.2500E-02       9 *****
 1.5625E-02 <= |c| < 3.1250E-02      29 ***************
 7.8125E-03 <= |c| < 1.5625E-02      49 *************************
 3.9062E-03 <= |c| < 7.8125E-03       0
 1.9531E-03 <= |c| < 3.9062E-03       0
 9.7656E-04 <= |c| < 1.9531E-03       0
 4.8828E-04 <= |c| < 9.7656E-04       0
 2.4414E-04 <= |c| < 4.8828E-04       0
 1.2207E-04 <= |c| < 2.4414E-04       0
 6.1035E-05 <= |c| < 1.2207E-04       0
 3.0518E-05 <= |c| < 6.1035E-05       0
 1.5259E-05 <= |c| < 3.0518E-05       0
 7.6294E-06 <= |c| < 1.5259E-05       0
 3.8147E-06 <= |c| < 7.6294E-06       0
 0.0000E+00 <= |c| < 3.8147E-06       0
                                  ----- ---------|---------|---------|---------|
                  total read =       95 total stored =      95

 from the selected csfs,
 min(|csfvec(:)|) = 1.0117E-02    max(|csfvec(:)|) = 8.6431E-01
 norm=   1.00000000000000     
 csfs will be printed based on coefficient magnitudes.

 current csfvec(*) selection parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =    1558 ncsft =    1558 ncsf =      95

 i:slabel(i) =  1: a1   2: a2   3: b1   4: b2 
 
 frozen orbital =    1    2    3    4
 symfc(*)       =    1    1    3    4
 label          =  a1   a1   b1   b2 
 rmo(*)         =    1    2    1    1
 
 internal level =    1    2    3    4    5    6
 syml(*)        =    1    1    1    2    3    4
 label          =  a1   a1   a1   a2   b1   b2 
 rmo(*)         =    3    4    5    1    2    2

 printing selected csfs in sorted order from cmin = 0.00000 to cmax = 1.00000

   indcsf     c     c**2   v  lab:rmo  lab:rmo   step(*)
  ------- -------- ------- - ---- --- ---- --- ------------
          3 -0.86431 0.74704 z*                    101110
          1  0.41935 0.17586 z*                    111001
         40 -0.09838 0.00968 y           a2 :  2  1101010
         34  0.08573 0.00735 y           b1 :  4  1101100
         41  0.07011 0.00492 y           a2 :  3  1101010
         55  0.06914 0.00478 y           a1 :  7  1100110
         56  0.06551 0.00429 y           a1 :  8  1100110
         36 -0.06481 0.00420 y           b1 :  6  1101100
         60 -0.05124 0.00263 y           a1 : 12  1100110
          6 -0.04910 0.00241 y           b2 :  4  1111000
         59  0.04579 0.00210 y           a1 : 11  1100110
         23  0.04331 0.00188 y           a1 :  7  1110001
        472 -0.04211 0.00177 x  b1 :  3  b2 :  3 11100010
         45 -0.03505 0.00123 y           a1 :  8  1101001
          8  0.03460 0.00120 y           b2 :  6  1111000
        486 -0.03153 0.00099 x  b1 :  3  b2 :  5 11100010
        474 -0.03138 0.00098 x  b1 :  5  b2 :  3 11100010
         24  0.02880 0.00083 y           a1 :  8  1110001
        166  0.02797 0.00078 x  a1 :  6  b2 :  3 11110000
        520 -0.02684 0.00072 x  b1 :  9  b2 :  9 11100010
         27  0.02669 0.00071 y           a1 : 11  1110001
        488 -0.02462 0.00061 x  b1 :  5  b2 :  5 11100010
         49  0.02411 0.00058 y           a1 : 12  1101001
        264 -0.02391 0.00057 x  a1 :  6  b2 :  3 11101000
         28 -0.02387 0.00057 y           a1 : 12  1110001
          2 -0.02285 0.00052 z*                    110110
        188  0.02101 0.00044 x  a1 :  6  b2 :  5 11110000
        169  0.02072 0.00043 x  a1 :  9  b2 :  3 11110000
       1101 -0.02067 0.00043 x  a1 : 10  a2 :  2 11001010
       1014  0.01992 0.00040 x  a1 : 10  b1 :  4 11001100
        253  0.01962 0.00039 x  a2 :  3  b1 :  6 11101000
        263  0.01911 0.00037 x  a2 :  4  b1 :  9 11101000
       1054 -0.01814 0.00033 x  a1 :  6  b1 :  8 11001100
        286 -0.01787 0.00032 x  a1 :  6  b2 :  5 11101000
        267 -0.01786 0.00032 x  a1 :  9  b2 :  3 11101000
       1136  0.01748 0.00031 x  b1 :  9  b2 :  3 11001010
        404  0.01673 0.00028 x  a1 : 14  b1 :  8 11100100
       1172  0.01673 0.00028 x  b1 :  3  b2 :  9 11001010
        341  0.01670 0.00028 x  a1 :  6  b1 :  3 11100100
       1057 -0.01655 0.00027 x  a1 :  9  b1 :  8 11001100
        191  0.01633 0.00027 x  a1 :  9  b2 :  5 11110000
       1150  0.01608 0.00026 x  b1 :  9  b2 :  5 11001010
        456  0.01595 0.00025 x  a1 : 12  a2 :  3 11100010
         48  0.01593 0.00025 y           a1 : 11  1101001
        339  0.01587 0.00025 x  a1 : 15  b2 :  9 11101000
       1286 -0.01567 0.00025 x  a1 :  7  a1 : 10 11000110
       1174  0.01540 0.00024 x  b1 :  5  b2 :  9 11001010
       1036 -0.01527 0.00023 x  a1 : 10  b1 :  6 11001100
       1112  0.01495 0.00022 x  a1 : 10  a2 :  3 11001010
        246  0.01473 0.00022 x  a2 :  2  b1 :  4 11101000
         13 -0.01428 0.00020 y           b1 :  4  1110100
        379  0.01411 0.00020 x  a1 : 11  b1 :  6 11100100
        289 -0.01399 0.00020 x  a1 :  9  b2 :  5 11101000
        252 -0.01387 0.00019 x  a2 :  2  b1 :  6 11101000
       1078  0.01384 0.00019 x  a2 :  4  b2 :  3 11001100
       1119  0.01369 0.00019 x  a1 :  6  a2 :  4 11001010
       1007 -0.01362 0.00019 x  a1 : 14  b1 :  3 11001100
        247 -0.01339 0.00018 x  a2 :  3  b1 :  4 11101000
        455 -0.01320 0.00017 x  a1 : 11  a2 :  3 11100010
        230 -0.01320 0.00017 x  a1 : 15  b2 :  8 11110000
       1373  0.01318 0.00017 x  b2 :  3  b2 :  9 11000110
       1287 -0.01316 0.00017 x  a1 :  8  a1 : 10 11000110
       1315 -0.01308 0.00017 x  a1 :  6  a1 : 15 11000110
        380 -0.01304 0.00017 x  a1 : 12  b1 :  6 11100100
       1084  0.01281 0.00016 x  a2 :  4  b2 :  5 11001100
       1368  0.01264 0.00016 x  b2 :  3  b2 :  8 11000110
       1029 -0.01263 0.00016 x  a1 : 14  b1 :  5 11001100
       1122  0.01262 0.00016 x  a1 :  9  a2 :  4 11001010
        363  0.01256 0.00016 x  a1 :  6  b1 :  5 11100100
       1352  0.01253 0.00016 x  b1 :  3  b1 :  9 11000110
        344  0.01241 0.00015 x  a1 :  9  b1 :  3 11100100
        470  0.01233 0.00015 x  a1 : 15  a2 :  4 11100010
       1375  0.01215 0.00015 x  b2 :  5  b2 :  9 11000110
        294 -0.01209 0.00015 x  a1 : 14  b2 :  5 11101000
       1318 -0.01200 0.00014 x  a1 :  9  a1 : 15 11000110
        353 -0.01168 0.00014 x  a1 :  7  b1 :  4 11100100
        260  0.01166 0.00014 x  a2 :  4  b1 :  8 11101000
       1370  0.01164 0.00014 x  b2 :  5  b2 :  8 11000110
        272 -0.01157 0.00013 x  a1 : 14  b2 :  3 11101000
       1354  0.01152 0.00013 x  b1 :  5  b1 :  9 11000110
         69  0.01148 0.00013 y           b1 :  4  1100011
        121 -0.01147 0.00013 y           a1 : 10  1001110
        338  0.01140 0.00013 x  a1 : 14  b2 :  9 11101000
         19 -0.01136 0.00013 y           a2 :  2  1110010
        440  0.01124 0.00013 x  a1 :  7  a2 :  2 11100010
        445 -0.01110 0.00012 x  a1 : 12  a2 :  2 11100010
        441  0.01081 0.00012 x  a1 :  8  a2 :  2 11100010
       1293  0.01068 0.00011 x  a1 : 10  a1 : 11 11000110
         44  0.01067 0.00011 y           a1 :  7  1101001
        375  0.01065 0.00011 x  a1 :  7  b1 :  6 11100100
       1298 -0.01061 0.00011 x  a1 : 10  a1 : 12 11000110
        435 -0.01061 0.00011 x  a2 :  4  b2 :  8 11100100
        451 -0.01037 0.00011 x  a1 :  7  a2 :  3 11100010
       1008 -0.01027 0.00011 x  a1 : 15  b1 :  3 11001100
        657 -0.01012 0.00010 x  a1 : 10  b2 :  4 11011000
           95 csfs were printed in this range.
