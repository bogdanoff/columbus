1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=   105)...
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.2712888828 -7.7716E-15  6.4597E-02  2.3403E-01  1.0000E-04

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  1  1    -46.2712888828 -7.7716E-15  6.4597E-02  2.3403E-01  1.0000E-04

 from bk iterations: iconv=   1

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.3265203915  5.5232E-02  3.0513E-03  4.8844E-02  1.0000E-04
 mr-sdci #  2  1    -46.3298822594  3.3619E-03  5.1000E-04  2.0342E-02  1.0000E-04
 mr-sdci #  3  1    -46.3303450618  4.6280E-04  1.1004E-04  8.9651E-03  1.0000E-04
 mr-sdci #  4  1    -46.3304343783  8.9317E-05  1.5355E-05  3.2516E-03  1.0000E-04
 mr-sdci #  5  1    -46.3304504496  1.6071E-05  3.7448E-06  1.8107E-03  1.0000E-04
 mr-sdci #  6  1    -46.3304545055  4.0559E-06  1.2648E-06  8.7608E-04  1.0000E-04
 mr-sdci #  7  1    -46.3304560760  1.5705E-06  3.6742E-07  5.3435E-04  1.0000E-04
 mr-sdci #  8  1    -46.3304564653  3.8935E-07  6.7790E-08  2.3498E-04  1.0000E-04
 mr-sdci #  9  1    -46.3304565219  5.6569E-08  1.2144E-08  9.3035E-05  1.0000E-04

 mr-sdci  convergence criteria satisfied after  9 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  9  1    -46.3304565219  5.6569E-08  1.2144E-08  9.3035E-05  1.0000E-04

 number of reference csfs (nref) is   105.  root number (iroot) is  1.
 c0**2 =   0.89986404  c**2 (all zwalks) =   0.89986404

 eref      =    -46.271011526023   "relaxed" cnot**2         =   0.899864036453
 eci       =    -46.330456521916   deltae = eci - eref       =  -0.059444995893
 eci+dv1   =    -46.336409103858   dv1 = (1-cnot**2)*deltae  =  -0.005952581942
 eci+dv2   =    -46.337071501181   dv2 = dv1 / cnot**2       =  -0.006614979264
 eci+dv3   =    -46.337899779373   dv3 = dv1 / (2*cnot**2-1) =  -0.007443257456
 eci+pople =    -46.333753835285   (  4e- scaled deltae )    =  -0.062742309262
