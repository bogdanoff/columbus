1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      2009-03.    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


================ Computing sorting integral file structure ================

                    -----z----- -----y----- -----x----- -----w----- ---total---

                CSFs        15         560        5670           0        6245
      internal walks        15          20          15           0          50
valid internal walks        15          20          15           0          50
 getinfoarray: info=                     6 :                     1
                  8192                  6552                  8192
                  5460                     0
 icd(3)=                  1191 ci%nnlev=                   595  l2rec=
                  8192  n2max=                  5460
 lcore1,lcore2=              13069922              13046606
 lencor,maxblo              13107200                 60000
========================================
 current settings:
 minbl3        1215
 minbl4        1215
 locmaxbl3    22684
 locmaxbuf    11342
 maxbl3       60000
 maxbl3       60000
 maxbl4       60000
 maxbuf       30006
========================================

 sorted 4-external integrals:     3 records of integral w-combinations 
                                  3 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)

 sorted 3-external integrals:     3 records of integral w-combinations 
                                  3 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)
 Orig.  diagonal integrals:  1electron:        34
                             0ext.    :        42
                             2ext.    :       336
                             4ext.    :       812


 Orig. off-diag. integrals:  4ext.    :     93177
                             3ext.    :     72576
                             2ext.    :     25578
                             1ext.    :      4704
                             0ext.    :       315
                             2ext. SO :         0
                             1ext. SO :         0
                             0ext. SO :         0
                             1electron:       406


 Sorted integrals            3ext.  w :     68040 x :     63504
                             4ext.  w :     82215 x :     71253


Cycle #  1 sortfile size=    393204(      12 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13069922
Cycle #  2 sortfile size=    393204(      12 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13069922
 minimum size of srtscr:    327670 WP (    10 records)
 maximum size of srtscr:    393204 WP (    12 records)
diagi   file:      4 records  of   1536 WP each=>       6144 WP total
ofdgi   file:     23 records  of   1536 WP each=>      35328 WP total
fil3w   file:      3 records  of  30006 WP each=>      90018 WP total
fil3x   file:      3 records  of  30006 WP each=>      90018 WP total
fil4w   file:      3 records  of  30006 WP each=>      90018 WP total
fil4x   file:      3 records  of  30006 WP each=>      90018 WP total
 compressed index vector length=                     1
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 0,
  GSET = 0,
   DAVCOR =10,
  NCOREL = 4
   nroot=1
  IVMODE = 3
  NBKITR = 1
  RTOLBK = 1e-4,1e-4,1e-4,1e-4,1e-4,1e-4,1e-4,1e-4,1e-4,
  NITER = 200
   rtolci=1e-4
  NVCIMX = 25
  NVRFMX = 25
  NVBKMX = 25
  IDEN  = 1
  CSFPRN = 10,
  update_mode=1
  nvcimn=2
  nvrfmn=2
  nvbkmn=2
 /&end
 ------------------------------------------------------------------------
lodens (list->root)=  1
invlodens (root->list)=  1
 USING SEGMENTS OF EQUAL SIZE

****************  list of control variables  ****************
 lvlprt =    0      nroot  =    1      noldv  =   0      noldhv =   0
 nunitv =    1      nbkitr =    1      niter  = 200      davcor =  10
 csfprn =   10      ivmode =    3      istrt  =   0      vout   =   0
 iortls =    0      nvbkmx =   25      ibktv  =  -1      ibkthv =  -1
 nvcimx =   25      icitv  =   -1      icithv =  -1      frcsub =   0
 nvbkmn =    2      nvcimn =    2      maxseg =   4      nrfitr =  30
 ncorel =    4      nvrfmx =   25      nvrfmn =   2      iden   =   1
 itran  =    0      froot  =    0      rtmode =   0      ncouple=   1
 skipso =    F      dalton2=    0      molcas =   0      finalv =   0
 finalw =    0      cosmocalc=   0    with_tsklst=   0
 nsegwx =    1     1     1     1
 nseg0x =    1     1     1     1
 nseg1x =    1     1     1     1
 nseg2x =    1     1     1     1
 nseg3x =    1     1     1     1
 nseg4x =    1     1     1     1
 no0ex  =      0    no1ex  =      0    no2ex  =     0    no3ex  =     0
 no4ex  =      0    nodiag =      0
 cdg4ex =    1      c3ex1ex=    1      c2ex0ex=   1
 fileloc=    0     0     0     0     0     0     0     1     1     1
 directhd=   1      noaqccshift_zyxw=      0
 critical_crit=-1.00000    critical_delta= 0.05000

 ctol   = 0.010000    lrtshift=1.000000    smalld =0.001000


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-04    1.000E-04
 Computing density:                    .drt1.state1
 using                      1  nodes and                      1  cores.
 szdg/szodg per processor=                  6144                 35328
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core           13107199 DP per process

********** Integral sort section *************


 workspace allocation information: lencor=  13107199

 echo of the input for program cisrt:
 ------------------------------------------------------------------------
  &input
  maxbl3=60000
  maxbl4=60000
  &end
 ------------------------------------------------------------------------
 
 ( 6) listing file:                    ciudgls             
 ( 5) input file:                      cisrtin   
 (17) cidrt file:                      cidrtfl             
 (11) transformed integrals file:      moints    
 (12) diagonal integral file:          diagint             
 (13) off-diagonal integral file:      ofdgint             
 (31) 4-external w integrals file:     fil4w               
 (32) 4-external x integrals file:     fil4x               
 (33) 3-external w integrals file:     fil3w               
 (34) 3-external x integrals file:     fil3x               
 (21) scratch da sorting file:         srtscr              
 (12) 2-e integral file [fsplit=2]:    moints2   

 input integral file header information:
                                                                                 
 aoints SIFS file created by argos.      zam792            16:57:17.826 17-Dec-13
  cidrt_title                                                                    
 MO-coefficients from mcscf.x                                                    
  with dummy occupation 1.0 for active orbitals                                  
  total ao core energy =    0.000000000                                          
 MCSCF energy =     -46.297255533                                                
 SIFS file created by program tran.      zam792            16:57:18.312 17-Dec-13

 input energy(*) values:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.

 total core energy =   0.000000000000E+00

 nsym = 1 nmot=  34

 symmetry  =    1
 slabel(*) =    a
 nmpsy(*)  =   34

 info(*) =          1      8192      6552      8192      5460         0

 orbital labels, i:molab(i)=
   1:tout:001   2:tout:002   3:tout:003   4:tout:004   5:tout:005   6:tout:006   7:tout:007   8:tout:008   9:tout:009  10:tout:010
  11:tout:011  12:tout:012  13:tout:013  14:tout:014  15:tout:015  16:tout:016  17:tout:017  18:tout:018  19:tout:019  20:tout:020
  21:tout:021  22:tout:022  23:tout:023  24:tout:024  25:tout:025  26:tout:026  27:tout:027  28:tout:028  29:tout:029  30:tout:030
  31:tout:031  32:tout:032  33:tout:033  34:tout:034

 input parameters:
 prnopt=  0
 ldamin=    4095 ldamax=   32767 ldainc=      64
 maxbuf=   30006 maxbl3=   60000 maxbl4=   60000 intmxo=    1536
  Using 32 bit compression 

 drt information:
  cidrt_title                                                                    
 nmotd =  38 nfctd =   4 nfvtc =   0 nmot  =  34
 nlevel =  34 niot  =   6 lowinl=  29
 orbital-to-level map(*)
   -1  -1  -1  -1  29  30  31  32  33  34   1   2   3   4   5   6   7   8   9  10
   11  12  13  14  15  16  17  18  19  20  21  22  23  24  25  26  27  28
 compressed map(*)
   29  30  31  32  33  34   1   2   3   4   5   6   7   8   9  10  11  12  13  14
   15  16  17  18  19  20  21  22  23  24  25  26  27  28
 levsym(*)
    1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1
    1   1   1   1   1   1   1   1   1   1   1   1   1   1
 repartitioning mu(*)=
   2.  0.  0.  0.  0.  0.

 new core energy added to the energy(*) list.
 from the integral file: h1_core= -4.355613553355E+01

 indxdg: diagonal integral statistics.
 total number of integrals contributing to diagonal matrix elements:      1190
 number with all external indices:       812
 number with half external - half internal indices:       336
 number with all internal indices:        42

 indxof: off-diagonal integral statistics.
    4-external integrals: num=      93177 strt=          1
    3-external integrals: num=      72576 strt=      93178
    2-external integrals: num=      25578 strt=     165754
    1-external integrals: num=       4704 strt=     191332
    0-external integrals: num=        315 strt=     196036

 total number of off-diagonal integrals:      196350


 indxof(2nd)  ittp=   3 numx(ittp)=       25578
 indxof(2nd)  ittp=   4 numx(ittp)=        4704
 indxof(2nd)  ittp=   5 numx(ittp)=         315

 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg=  12931980
 pro2e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652

 pro2e:     90013 integrals read in    17 records.

 pro2e:         0 integrals 34-ext integrals skipped.
 pro1e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652
 pro1e: eref =   -1.721733008562787E+00
 total size of srtscr:                     7  records of                  32767 
 WP =               1834952 Bytes

 new core energy added to the energy(*) list.
 from the hamiltonian repartitioning, eref= -1.721733008563E+00
 putdg        1     596    1191    1786    3322   36089   57934    3018   46706   90394
   123161  131353  136813  158652

 putf:       4 buffers of length    1536 written to file 12
 diagonal integral file completed.

 putd34:     3 records of integral w-combinations and
             3 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep4e:     3 blocks of linear combinations of 4-external integrals processed.
 number of sorted 4-external integrals     153468
 number of original 4-external integrals    93177


 putf34: external integral file complete. nfilw=    31 nfilx=    32 nrecw=     3 nrecx=     3 lbufp= 30006

 putd34:     3 records of integral w-combinations and
             3 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep3e:     6 blocks of linear combinations of 3-external integrals processed.
 number of sorted 3-external integrals     131544
 number of original 3-external integrals    72576


 putf34: external integral file complete. nfilw=    33 nfilx=    34 nrecw=     3 nrecx=     3 lbufp= 30006
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd     25578         3    165754    165754     25578    196350
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      4704         4    191332    191332      4704    196350
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd       315         5    196036    196036       315    196350

 putf:      23 buffers of length    1536 written to file 13
 off-diagonal files sort completed.
 executing brd_struct for cisrtinfo
cisrtinfo:
bufszi  1536
 diagfile 4ext:     812 2ext:     336 0ext:      42
 fil4w,fil4x  :   93177 fil3w,fil3x :   72576
 ofdgint  2ext:   25578 1ext:    4704 0ext:     315so0ext:       0so1ext:       0so2ext:       0
buffer minbl4    1215 minbl3    1215 maxbl2    1218nbas:  28   0   0   0   0   0   0   0 maxbuf 30006
 CIUDG version 5.9.7 ( 5-Oct-2004)

 workspace allocation information: lcore=  13107199

 core energy values from the integral file:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -4.355613553355E+01, ietype=    6,   fcore energy of type: H1(*)   
 energy( 3)= -1.721733008563E+00, ietype=    5,   fcore energy of type: Vref(*) 

 total core repulsion energy = -4.527786854211E+01
 nmot  =    38 niot  =     6 nfct  =     4 nfvt  =     0
 nrow  =    23 nsym  =     1 ssym  =     1 lenbuf=  1600
 nwalk,xbar:         50       15       20       15        0
 nvalwt,nvalw:       50       15       20       15        0
 ncsft:            6245
 total number of valid internal walks:      50
 nvalz,nvaly,nvalx,nvalw =       15      20      15       0

 cisrt info file parameters:
 file number  12 blocksize   1536
 mxbld   1536
 nd4ext,nd2ext,nd0ext   812   336    42
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int    93177    72576    25578     4704      315        0        0        0
 minbl4,minbl3,maxbl2  1215  1215  1218
 maxbuf 30006
 number of external orbitals per symmetry block:  28
 nmsym   1 number of internal orbitals   6
 executing brd_struct for drt
 executing brd_struct for orbinf
 executing brd_struct for momap
 calcthrxt: niot,maxw1=                     6                    11
 block size     0
 pthz,pthy,pthx,pthw:    15    20    15     0 total internal walks:      50
 maxlp3,n2lp,n1lp,n0lp    11     0     0     0
 orbsym(*)= 1 1 1 1 1 1

 setref:       15 references kept,
                0 references were marked as invalid, out of
               15 total.
 nmb.of records onel     1
 nmb.of records 2-ext    17
 nmb.of records 1-ext     4
 nmb.of records 0-ext     1
 nmb.of records 2-int     0
 nmb.of records 1-int     0
 nmb.of records 0-int     0
 ---------memory usage in DP -----------------
 < n-ex core usage >
     routines:
    fourex            62738
    threx             60079
    twoex              9922
    onex               3217
    allin              1536
    diagon             2109
               =======
   maximum            62738
 
  __ static summary __ 
   reflst                15
   hrfspc                15
               -------
   static->              15
 
  __ core required  __ 
   totstc                15
   max n-ex           62738
               -------
   totnec->           62753
 
  __ core available __ 
   totspc          13107199
   totnec -           62753
               -------
   totvec->        13044446

 number of external paths / symmetry
 vertex x     378
 vertex w     406
segment: free space=    13044446
 reducing frespc by                   337 to               13044109 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1          15|        15|         0|        15|         0|         1|
 -------------------------------------------------------------------------------
  Y 2          20|       560|        15|        20|        15|         2|
 -------------------------------------------------------------------------------
  X 3          15|      5670|       575|        15|        35|         3|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=           4DP  conft+indsym=          80DP  drtbuffer=         253 DP

dimension of the ci-matrix ->>>      6245

 executing brd_struct for civct
 gentasklist: ntask=                    12
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS   
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      15      15       5670         15      15      15
     2  2   1    11      one-ext yz   1X  2 1      20      15        560         15      20      15
     3  3   2    15      1ex3ex yx    3X  3 2      15      20       5670        560      15      20
     4  1   1     1      allint zz    OX  1 1      15      15         15         15      15      15
     5  2   2     5      0ex2ex yy    OX  2 2      20      20        560        560      20      20
     6  3   3     6      0ex2ex xx*   OX  3 3      15      15       5670       5670      15      15
     7  3   3    18      0ex2ex xx+   OX  3 3      15      15       5670       5670      15      15
     8  2   2    42      four-ext y   4X  2 2      20      20        560        560      20      20
     9  3   3    43      four-ext x   4X  3 3      15      15       5670       5670      15      15
    10  1   1    75      dg-024ext z  OX  1 1      15      15         15         15      15      15
    11  2   2    76      dg-024ext y  OX  2 2      20      20        560        560      20      20
    12  3   3    77      dg-024ext x  OX  3 3      15      15       5670       5670      15      15
----------------------------------------------------------------------------------------------------
REDTASK #   1 TIME=  11.000 N=  1 (task/type/sgbra)=(   1/24/0) (
REDTASK #   2 TIME=  10.000 N=  1 (task/type/sgbra)=(   2/11/0) (
REDTASK #   3 TIME=   9.000 N=  1 (task/type/sgbra)=(   3/15/0) (
REDTASK #   4 TIME=   8.000 N=  1 (task/type/sgbra)=(   4/ 1/0) (
REDTASK #   5 TIME=   7.000 N=  1 (task/type/sgbra)=(   5/ 5/0) (
REDTASK #   6 TIME=   6.000 N=  1 (task/type/sgbra)=(   6/ 6/1) (
REDTASK #   7 TIME=   5.000 N=  1 (task/type/sgbra)=(   7/18/2) (
REDTASK #   8 TIME=   4.000 N=  1 (task/type/sgbra)=(   8/42/1) (
REDTASK #   9 TIME=   3.000 N=  1 (task/type/sgbra)=(   9/43/1) (
REDTASK #  10 TIME=   2.000 N=  1 (task/type/sgbra)=(  10/75/1) (
REDTASK #  11 TIME=   1.000 N=  1 (task/type/sgbra)=(  11/76/1) (
REDTASK #  12 TIME=   0.000 N=  1 (task/type/sgbra)=(  12/77/1) (
 initializing v-file: 1:                  6245

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         175 2x:           0 4x:           0
All internal counts: zz :         295 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 reference space has dimension      15
 dsyevx: computed roots 1 to    2(converged:   2)

    root           eigenvalues
    ----           ------------
       1         -46.2748581090
       2         -46.2748580985

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt=                    15

         vector  1 from unit 11 written to unit 49 filename cirefv              

 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=    15)...
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              6245
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:              25
 number of roots to converge:                             1
 number of iterations:                                    1
 residual norm convergence criteria:               0.000100

          starting bk iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         410 2x:         110 4x:          35
All internal counts: zz :         295 yy:           0 xx:           0 ww:           0
One-external counts: yz :         440 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:          90 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1
   ht   1    -0.99698957

          calcsovref: tciref block   1

              civs   1
 refs   1    1.00000    

          calcsovref: scrb block   1

                ci   1
 civs   1    1.00000    

          calcsovref: sovref block   1

              v      1
 ref    1    1.00000    

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.2748581090 -1.0769E-14  6.0447E-02  2.2388E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.2748581090 -1.0769E-14  6.0447E-02  2.2388E-01  1.0000E-04
 
diagon:itrnv=   2
 expansion vectors are not transformed.
 matrix-vector products are not transformed.

    2 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:              6245
 number of initial trial vectors:                         2
 number of initial matrix-vector products:                1
 maximum dimension of the subspace vectors:              25
 number of roots to converge:                             1
 number of iterations:                                  200
 residual norm convergence criteria:               0.000100

          starting ci iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         410 2x:         110 4x:          35
All internal counts: zz :         295 yy:         390 xx:         390 ww:           0
One-external counts: yz :         440 yx:         390 yw:           0
Two-external counts: yy :         160 ww:           0 xx:         200 xz:          90 wz:           0 wx:           0
Three-ext.   counts: yx :          60 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2
   ht   1    -0.99698957
   ht   2    -0.06044657    -0.05879348

          calcsovref: tciref block   1

              civs   1       civs   2
 refs   1    1.00000       1.645985E-13

          calcsovref: scrb block   1

                ci   1         ci   2
 civs   1   0.961201      -0.275848    
 civs   2   0.769658        2.68190    

          calcsovref: sovref block   1

              v      1       v      2
 ref    1   0.961201      -0.275848    

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.96120132    -0.27584782

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.3232592101  4.8401E-02  1.4538E-03  3.0575E-02  1.0000E-04
 mr-sdci #  1  2    -45.6871727614  4.0930E-01  0.0000E+00  5.0878E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000999
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   2

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         410 2x:         110 4x:          35
All internal counts: zz :         295 yy:         390 xx:         390 ww:           0
One-external counts: yz :         440 yx:         390 yw:           0
Two-external counts: yy :         160 ww:           0 xx:         200 xz:          90 wz:           0 wx:           0
Three-ext.   counts: yx :          60 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3
   ht   1    -0.99698957
   ht   2    -0.06044657    -0.05879348
   ht   3    -0.00149991     0.00358015    -0.00201476

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3
 refs   1    1.00000       1.645985E-13   1.261002E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3
 civs   1   0.960143       1.760990E-02   0.279889    
 civs   2   0.790602      -0.308889       -2.75042    
 civs   3    1.08089        16.9706       -6.25474    

          calcsovref: sovref block   1

              v      1       v      2       v      3
 ref    1   0.961506       3.900982E-02   0.272002    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.96150569     0.03900982     0.27200191

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1    -46.3248334033  1.5742E-03  1.4503E-04  1.0316E-02  1.0000E-04
 mr-sdci #  2  2    -45.9018127035  2.1464E-01  0.0000E+00  3.4146E-01  1.0000E-04
 mr-sdci #  2  3    -45.6580559719  3.8019E-01  0.0000E+00  5.2240E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   3

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         410 2x:         110 4x:          35
All internal counts: zz :         295 yy:         390 xx:         390 ww:           0
One-external counts: yz :         440 yx:         390 yw:           0
Two-external counts: yy :         160 ww:           0 xx:         200 xz:          90 wz:           0 wx:           0
Three-ext.   counts: yx :          60 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4
   ht   1    -0.99698957
   ht   2    -0.06044657    -0.05879348
   ht   3    -0.00149991     0.00358015    -0.00201476
   ht   4     0.00204321    -0.00103523    -0.00014716    -0.00027823

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4
 refs   1    1.00000       1.645985E-13   1.261002E-03  -1.997071E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4
 civs   1   0.960841      -1.364355E-02   7.743900E-02   0.285786    
 civs   2   0.792566       0.271583       -1.04871       -2.58566    
 civs   3    1.19316        10.3202        12.1404       -8.59349    
 civs   4    1.13537        39.9166       -22.1377        22.3581    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4
 ref    1   0.960078      -8.034602E-02   0.136959       0.230299    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.96007771    -0.08034602     0.13695862     0.23029905

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1    -46.3249981079  1.6470E-04  4.9649E-05  5.3173E-03  1.0000E-04
 mr-sdci #  3  2    -46.0652714220  1.6346E-01  0.0000E+00  2.7253E-01  1.0000E-04
 mr-sdci #  3  3    -45.8419537398  1.8390E-01  0.0000E+00  4.2214E-01  1.0000E-04
 mr-sdci #  3  4    -45.5820814534  3.0421E-01  0.0000E+00  5.4266E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   4

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         410 2x:         110 4x:          35
All internal counts: zz :         295 yy:         390 xx:         390 ww:           0
One-external counts: yz :         440 yx:         390 yw:           0
Two-external counts: yy :         160 ww:           0 xx:         200 xz:          90 wz:           0 wx:           0
Three-ext.   counts: yx :          60 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1    -0.99698957
   ht   2    -0.06044657    -0.05879348
   ht   3    -0.00149991     0.00358015    -0.00201476
   ht   4     0.00204321    -0.00103523    -0.00014716    -0.00027823
   ht   5    -0.00232388    -0.00064795    -0.00014944     0.00011667    -0.00021114

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1    1.00000       1.645985E-13   1.261002E-03  -1.997071E-03   2.353865E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 civs   1   0.958994       0.118748      -0.147738      -0.174489      -0.188556    
 civs   2   0.792925      -0.317025       1.642971E-02    1.46271        2.45320    
 civs   3    1.21640       -10.4168       -1.43929       -10.5877        11.0328    
 civs   4    1.37062       -38.1876       -18.5727        20.2543       -33.8201    
 civs   5   0.687979       -33.6712        46.9048        18.4385       -33.7677    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1   0.959410       0.102618      -2.053894E-03  -0.184888      -0.186587    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     0.95940972     0.10261829    -0.00205389    -0.18488807    -0.18658685

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1    -46.3250322671  3.4159E-05  3.5629E-06  1.7789E-03  1.0000E-04
 mr-sdci #  4  2    -46.0903986666  2.5127E-02  0.0000E+00  1.7999E-01  1.0000E-04
 mr-sdci #  4  3    -46.0127444053  1.7079E-01  0.0000E+00  4.8046E-01  1.0000E-04
 mr-sdci #  4  4    -45.8185403902  2.3646E-01  0.0000E+00  4.1251E-01  1.0000E-04
 mr-sdci #  4  5    -45.4521723860  1.7430E-01  0.0000E+00  4.9643E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   5

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         410 2x:         110 4x:          35
All internal counts: zz :         295 yy:         390 xx:         390 ww:           0
One-external counts: yz :         440 yx:         390 yw:           0
Two-external counts: yy :         160 ww:           0 xx:         200 xz:          90 wz:           0 wx:           0
Three-ext.   counts: yx :          60 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1    -0.99698957
   ht   2    -0.06044657    -0.05879348
   ht   3    -0.00149991     0.00358015    -0.00201476
   ht   4     0.00204321    -0.00103523    -0.00014716    -0.00027823
   ht   5    -0.00232388    -0.00064795    -0.00014944     0.00011667    -0.00021114
   ht   6     0.00029340    -0.00007609     0.00001098    -0.00003348     0.00001739    -0.00000556

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1    1.00000       1.645985E-13   1.261002E-03  -1.997071E-03   2.353865E-03  -2.900320E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1   0.958994       6.934072E-02   0.169387      -0.130472       0.203953      -0.102902    
 civs   2   0.792957      -0.270938      -9.686961E-02   0.795840       -2.25760        1.56710    
 civs   3    1.21925       -7.29600       -4.43325       -12.8979       -3.28253        9.73012    
 civs   4    1.40007       -35.9572        1.69380       -1.17048       -14.2313       -50.8020    
 civs   5   0.774120       -28.0296       -47.5217        33.3699        24.9448       -18.3079    
 civs   6    1.20081       -139.576        77.9195        156.222        261.198        249.545    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.959209       0.106453       2.595543E-02  -0.111160       0.211196      -0.104648    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.95920943     0.10645304     0.02595543    -0.11115983     0.21119587    -0.10464768

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1    -46.3250365455  4.2784E-06  1.6811E-07  3.5804E-04  1.0000E-04
 mr-sdci #  5  2    -46.1335787870  4.3180E-02  0.0000E+00  1.2711E-01  1.0000E-04
 mr-sdci #  5  3    -46.0336035302  2.0859E-02  0.0000E+00  4.5588E-01  1.0000E-04
 mr-sdci #  5  4    -45.8825375763  6.3997E-02  0.0000E+00  3.3352E-01  1.0000E-04
 mr-sdci #  5  5    -45.6205035974  1.6833E-01  0.0000E+00  4.9926E-01  1.0000E-04
 mr-sdci #  5  6    -45.3278853017  5.0017E-02  0.0000E+00  4.5741E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   6

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         410 2x:         110 4x:          35
All internal counts: zz :         295 yy:         390 xx:         390 ww:           0
One-external counts: yz :         440 yx:         390 yw:           0
Two-external counts: yy :         160 ww:           0 xx:         200 xz:          90 wz:           0 wx:           0
Three-ext.   counts: yx :          60 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1    -0.99698957
   ht   2    -0.06044657    -0.05879348
   ht   3    -0.00149991     0.00358015    -0.00201476
   ht   4     0.00204321    -0.00103523    -0.00014716    -0.00027823
   ht   5    -0.00232388    -0.00064795    -0.00014944     0.00011667    -0.00021114
   ht   6     0.00029340    -0.00007609     0.00001098    -0.00003348     0.00001739    -0.00000556
   ht   7     0.00002537     0.00003499     0.00000401    -0.00000211     0.00000215    -0.00000056    -0.00000025

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1    1.00000       1.645985E-13   1.261002E-03  -1.997071E-03   2.353865E-03  -2.900320E-04  -2.696233E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1   0.959053       2.463169E-02   0.190721       8.071269E-02  -0.149020      -0.154526      -0.114472    
 civs   2   0.792956      -0.202873      -0.255632      -0.123138        1.31069        1.92535        1.73916    
 civs   3    1.21937       -5.96500       -6.62243        8.77595       -8.57698        4.06430        10.2196    
 civs   4    1.40131       -33.2450       -10.5643        9.79690        9.64386        13.1523       -50.1821    
 civs   5   0.777740       -22.7306       -40.3431       -48.7634       -1.95983       -15.2182       -19.2161    
 civs   6    1.25129       -159.336        64.0909       -124.778        51.7013       -292.912        223.967    
 civs   7    1.07001       -342.837        611.884       -784.827       -895.197        942.679        156.205    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.959231       8.545381E-02   7.341974E-02   1.478187E-02  -0.174567      -0.151951      -0.115769    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.95923063     0.08545381     0.07341974     0.01478187    -0.17456704    -0.15195097    -0.11576932

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  6  1    -46.3250367254  1.7988E-07  1.7301E-08  9.4186E-05  1.0000E-04
 mr-sdci #  6  2    -46.1426298429  9.0511E-03  0.0000E+00  1.4364E-01  1.0000E-04
 mr-sdci #  6  3    -46.0714839767  3.7880E-02  0.0000E+00  3.5843E-01  1.0000E-04
 mr-sdci #  6  4    -45.9119229476  2.9385E-02  0.0000E+00  3.6016E-01  1.0000E-04
 mr-sdci #  6  5    -45.8445638263  2.2406E-01  0.0000E+00  4.2006E-01  1.0000E-04
 mr-sdci #  6  6    -45.4871328405  1.5925E-01  0.0000E+00  4.8774E-01  1.0000E-04
 mr-sdci #  6  7    -45.3251231499  4.7255E-02  0.0000E+00  4.5762E-01  1.0000E-04
 
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mr-sdci  convergence criteria satisfied after  6 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  6  1    -46.3250367254  1.7988E-07  1.7301E-08  9.4186E-05  1.0000E-04
 mr-sdci #  6  2    -46.1426298429  9.0511E-03  0.0000E+00  1.4364E-01  1.0000E-04
 mr-sdci #  6  3    -46.0714839767  3.7880E-02  0.0000E+00  3.5843E-01  1.0000E-04
 mr-sdci #  6  4    -45.9119229476  2.9385E-02  0.0000E+00  3.6016E-01  1.0000E-04
 mr-sdci #  6  5    -45.8445638263  2.2406E-01  0.0000E+00  4.2006E-01  1.0000E-04
 mr-sdci #  6  6    -45.4871328405  1.5925E-01  0.0000E+00  4.8774E-01  1.0000E-04
 mr-sdci #  6  7    -45.3251231499  4.7255E-02  0.0000E+00  4.5762E-01  1.0000E-04

####################CIUDGINFO####################

   ci vector at position   1 energy=  -46.325036725358

################END OF CIUDGINFO################

 
diagon:itrnv=   0
    1 of the   8 expansion vectors are transformed.
    1 of the   7 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
maximum overlap with reference    1(overlap= 0.95923)

 information on vector: 1 from unit 11 written to unit 48 filename civout              


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -46.3250367254

                                                       internal orbitals

                                          level       1    2    3    4    5    6

                                          orbital     5    6    7    8    9   10

                                         symmetry   a    a    a    a    a    a  

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  5  1       3 -0.858364                        +    +    +              +  
 z*  5  1      10 -0.428173                        +              +    +    +  
 y   5  1      23  0.078716              8( a  )   +    +    +                 
 y   5  1      32  0.065698             17( a  )   +    +    +                 
 y   5  1     104 -0.102079              5( a  )   +    +                   +  
 y   5  1     112 -0.075776             13( a  )   +    +                   +  
 y   5  1     187 -0.102079              4( a  )   +         +              +  
 y   5  1     197  0.075776             14( a  )   +         +              +  
 y   5  1     219  0.062533              8( a  )   +              +    +       
 y   5  1     228  0.042801             17( a  )   +              +    +       
 y   5  1     246 -0.039215              7( a  )   +              +         +  
 y   5  1     255  0.032739             16( a  )   +              +         +  
 y   5  1     273  0.039255              6( a  )   +                   +    +  
 y   5  1     282 -0.032756             15( a  )   +                   +    +  
 y   5  1     363 -0.014454             12( a  )        +    +              +  
 x   5  1     601  0.012511    5( a  )   8( a  )   +    +                      
 x   5  1     649 -0.011432    8( a  )  13( a  )   +    +                      
 x   5  1     700  0.012177    5( a  )  17( a  )   +    +                      
 x   5  1     708  0.017748   13( a  )  17( a  )   +    +                      
 x   5  1     873 -0.013523   22( a  )  25( a  )   +    +                      
 x   5  1     896 -0.013521   21( a  )  26( a  )   +    +                      
 x   5  1     924  0.014379   24( a  )  27( a  )   +    +                      
 x   5  1     978  0.012511    4( a  )   8( a  )   +         +                 
 x   5  1    1039  0.011432    8( a  )  14( a  )   +         +                 
 x   5  1    1077  0.012177    4( a  )  17( a  )   +         +                 
 x   5  1    1087 -0.017748   14( a  )  17( a  )   +         +                 
 x   5  1    1250 -0.013523   21( a  )  25( a  )   +         +                 
 x   5  1    1275  0.013521   22( a  )  26( a  )   +         +                 
 x   5  1    1301  0.014379   23( a  )  27( a  )   +         +                 
 x   5  1    1333  0.023959    1( a  )   3( a  )   +              +            
 x   5  1    1370  0.018215    3( a  )  10( a  )   +              +            
 x   5  1    1377 -0.017921    1( a  )  11( a  )   +              +            
 x   5  1    1386  0.014164   10( a  )  11( a  )   +              +            
 x   5  1    1467 -0.010188   16( a  )  17( a  )   +              +            
 x   5  1    1682  0.014692   26( a  )  27( a  )   +              +            
 x   5  1    1712  0.024063    2( a  )   3( a  )   +                   +       
 x   5  1    1740 -0.018293    3( a  )   9( a  )   +                   +       
 x   5  1    1756 -0.017999    2( a  )  11( a  )   +                   +       
 x   5  1    1763 -0.014225    9( a  )  11( a  )   +                   +       
 x   5  1    1844  0.010187   15( a  )  17( a  )   +                   +       
 x   5  1    2059  0.014681   25( a  )  27( a  )   +                   +       
 x   5  1    2088 -0.041778    1( a  )   2( a  )   +                        +  
 x   5  1    2097 -0.014387    4( a  )   5( a  )   +                        +  
 x   5  1    2116 -0.031535    1( a  )   9( a  )   +                        +  
 x   5  1    2125 -0.031535    2( a  )  10( a  )   +                        +  
 x   5  1    2132 -0.024694    9( a  )  10( a  )   +                        +  
 x   5  1    2157 -0.013507    4( a  )  13( a  )   +                        +  
 x   5  1    2170 -0.013506    5( a  )  14( a  )   +                        +  
 x   5  1    2178 -0.020425   13( a  )  14( a  )   +                        +  
 x   5  1    2318 -0.035185   21( a  )  22( a  )   +                        +  
 x   5  1    2528 -0.021396    8( a  )  12( a  )        +    +                 
 x   5  1    2597  0.017798   12( a  )  17( a  )        +    +                 
 x   5  1    2743  0.015229    2( a  )  25( a  )        +    +                 
 x   5  1    2750  0.013997    9( a  )  25( a  )        +    +                 
 x   5  1    2766  0.015233    1( a  )  26( a  )        +    +                 
 x   5  1    2775 -0.014001   10( a  )  26( a  )        +    +                 
 x   5  1    2793  0.020559    3( a  )  27( a  )        +    +                 
 x   5  1    2801 -0.018458   11( a  )  27( a  )        +    +                 
 x   5  1    3659  0.022925    5( a  )  12( a  )        +                   +  
 x   5  1    3677 -0.017132   12( a  )  13( a  )        +                   +  
 x   5  1    3790  0.017641    1( a  )  21( a  )        +                   +  
 x   5  1    3799 -0.016067   10( a  )  21( a  )        +                   +  
 x   5  1    3811  0.017645    2( a  )  22( a  )        +                   +  
 x   5  1    3818  0.016070    9( a  )  22( a  )        +                   +  
 x   5  1    3855 -0.014379    3( a  )  24( a  )        +                   +  
 x   5  1    3863  0.013052   11( a  )  24( a  )        +                   +  
 x   5  1    4792  0.022925    4( a  )  12( a  )             +              +  
 x   5  1    4823  0.017132   12( a  )  14( a  )             +              +  
 x   5  1    4925  0.017645    2( a  )  21( a  )             +              +  
 x   5  1    4932  0.016070    9( a  )  21( a  )             +              +  
 x   5  1    4944 -0.017641    1( a  )  22( a  )             +              +  
 x   5  1    4953  0.016067   10( a  )  22( a  )             +              +  
 x   5  1    4967 -0.014379    3( a  )  23( a  )             +              +  
 x   5  1    4975  0.013052   11( a  )  23( a  )             +              +  
 x   5  1    5174 -0.012188    8( a  )  12( a  )                  +    +       
 x   5  1    5551  0.010660    7( a  )  12( a  )                  +         +  
 x   5  1    5928 -0.010663    6( a  )  12( a  )                       +    +  

 ci coefficient statistics:
           rq > 0.1                4
      0.1> rq > 0.01              73
     0.01> rq > 0.001            251
    0.001> rq > 0.0001           254
   0.0001> rq > 0.00001          183
  0.00001> rq > 0.000001         315
 0.000001> rq                   5165
           all                  6245
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         175 2x:           0 4x:           0
All internal counts: zz :         295 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


  iref  icsf         v(icsf)             hv(icsf)
     1     1     -0.000002295896      0.000106242632
     2     2      0.000000219482     -0.000010156231
     3     3     -0.858363864992     39.720649092054
     4     4      0.000002086219     -0.000096539478
     5     5     -0.000002648749      0.000122570279
     6     6     -0.000000725237      0.000033560391
     7     7     -0.001908212041      0.088302266818
     8     8     -0.000000522001      0.000024155653
     9     9      0.000001587863     -0.000073477918
    10    10     -0.428172911392     19.813674653824
    11    11     -0.000000104415      0.000004814693
    12    12     -0.000000000034      0.000000001585
    13    13     -0.000000000104      0.000000004803
    14    14     -0.000000000185      0.000000008520
    15    15     -0.000000011110      0.000000512682

 number of reference csfs (nref) is    15.  root number (iroot) is  1.
 c0**2 =   0.92012421  c**2 (all zwalks) =   0.92012421

 pople ci energy extrapolation is computed with  4 correlated electrons.

 eref      =    -46.274858071990   "relaxed" cnot**2         =   0.920124208068
 eci       =    -46.325036725358   deltae = eci - eref       =  -0.050178653369
 eci+dv1   =    -46.329044785034   dv1 = (1-cnot**2)*deltae  =  -0.004008059676
 eci+dv2   =    -46.329392723864   dv2 = dv1 / cnot**2       =  -0.004355998506
 eci+dv3   =    -46.329806814297   dv3 = dv1 / (2*cnot**2-1) =  -0.004770088939
 eci+pople =    -46.327210636682   (  4e- scaled deltae )    =  -0.052352564692
 passed aftci ... 
 readint2: molcas,dalton2=                     0                     0
 files%faoints=aoints              
lodens (list->root)=  1
sifcfg setup: record length 4096 DP
# d1 elements per record  3272
# d2 elements per record  2730
  The MR-CISD density will be calculated.
 item #                     1 suffix=:.drt1.state1:
 read_civout: repnuc=  -45.2778685421096     
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method:  0 last record  1max overlap with ref# 96% root-following 0
 MR-CISD energy:   -46.32503673    -1.04716818
 residuum:     0.00009419
 deltae:     0.00000018
 apxde:     0.00000002

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.95923063     0.08545381     0.07341974     0.01478187    -0.17456704    -0.15195097    -0.11576932     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.95923063     0.08545381     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 computing final density
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=      60  DYX=      60  DYW=       0
   D0Z=      60  D0Y=      90  D0X=      60  D0W=       0
  DDZI=      65 DDYI=      70 DDXI=      40 DDWI=       0
  DDZE=       0 DDYE=      20 DDXE=      15 DDWE=       0
================================================================================
Trace of MO density:     4.000000
    4  correlated and     8  frozen core electrons

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   4     0.00000000     0.00000000     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.00000000     0.00000000     0.98954738     0.00000000    -0.00000131     0.00000000
   MO   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.77104569     0.00000000    -0.00000014
   MO   7     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000131     0.00000000     0.77104933     0.00000166
   MO   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000014     0.00000166     0.19546191
   MO   9     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000023     0.00000042     0.00000002
   MO  10     0.00000000     0.00000000     0.00000000     0.00000000    -0.00029507    -0.00000091     0.00084993     0.00000208
   MO  11     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  12     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  13     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  14     0.00000000     0.00000000     0.00000000     0.00000000     0.00000003    -0.08301407     0.00058601     0.00000049
   MO  15     0.00000000     0.00000000     0.00000000     0.00000000     0.00000143     0.00058601     0.08301411     0.00000009
   MO  16     0.00000000     0.00000000     0.00000000     0.00000000     0.00000003     0.00000245     0.00000063     0.01565576
   MO  17     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000008    -0.00000007     0.00000223
   MO  18     0.00000000     0.00000000     0.00000000     0.00000000     0.00283651     0.00000015     0.00042636     0.00000007
   MO  19     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  20     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  21     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  22     0.00000000     0.00000000     0.00000000     0.00000000     0.00281667     0.00000000     0.00000046     0.00000000
   MO  23     0.00000000     0.00000000     0.00000000     0.00000000     0.00002158    -0.00026466     0.05981173     0.00000016
   MO  24     0.00000000     0.00000000     0.00000000     0.00000000     0.00000006     0.05981145     0.00026466    -0.00000008
   MO  25     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000002    -0.00000030     0.00000000    -0.01266028
   MO  26     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000001    -0.00000010     0.00000015    -0.00004874
   MO  27     0.00000000     0.00000000     0.00000000     0.00000000     0.00267467     0.00000030    -0.00014777     0.00000011
   MO  28     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  29     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  30     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  31     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  32     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  33     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  34     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  35     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  36     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  37     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  38     0.00000000     0.00000000     0.00000000     0.00000000    -0.00047751     0.00000000     0.00000001     0.00000000

                MO   9         MO  10         MO  11         MO  12         MO  13         MO  14         MO  15         MO  16
   MO   5     0.00000000    -0.00029507     0.00000000     0.00000000     0.00000000     0.00000003     0.00000143     0.00000003
   MO   6     0.00000023    -0.00000091     0.00000000     0.00000000     0.00000000    -0.08301407     0.00058601     0.00000245
   MO   7     0.00000042     0.00084993     0.00000000     0.00000000     0.00000000     0.00058601     0.08301411     0.00000063
   MO   8     0.00000002     0.00000208     0.00000000     0.00000000     0.00000000     0.00000049     0.00000009     0.01565576
   MO   9     0.19548883    -0.00000020     0.00000000     0.00000000     0.00000000    -0.00000003     0.00000007    -0.00000229
   MO  10    -0.00000020     0.97217332     0.00000000     0.00000000     0.00000000    -0.00000132    -0.00024911     0.00000088
   MO  11     0.00000000     0.00000000     0.00521262     0.00000000    -0.00000003     0.00000000     0.00000000     0.00000000
   MO  12     0.00000000     0.00000000     0.00000000     0.00522125    -0.00000002     0.00000000     0.00000000     0.00000000
   MO  13     0.00000000     0.00000000    -0.00000003    -0.00000002     0.00318693     0.00000000     0.00000000     0.00000000
   MO  14    -0.00000003    -0.00000132     0.00000000     0.00000000     0.00000000     0.01166034     0.00000000    -0.00000029
   MO  15     0.00000007    -0.00024911     0.00000000     0.00000000     0.00000000     0.00000000     0.01166043     0.00000008
   MO  16    -0.00000229     0.00000088     0.00000000     0.00000000     0.00000000    -0.00000029     0.00000008     0.00191804
   MO  17     0.01561318    -0.00000004     0.00000000     0.00000000     0.00000000    -0.00000001    -0.00000001    -0.00000001
   MO  18    -0.00000001     0.08864570     0.00000000     0.00000000     0.00000000     0.00000013     0.00001485     0.00000010
   MO  19     0.00000000     0.00000000    -0.00000678     0.00419673    -0.00000003     0.00000000     0.00000000     0.00000000
   MO  20     0.00000000     0.00000000    -0.00418986    -0.00000678     0.00000011     0.00000000     0.00000000     0.00000000
   MO  21     0.00000000     0.00000000    -0.00000011     0.00000000    -0.00257689     0.00000000     0.00000000     0.00000000
   MO  22     0.00000000     0.00011066     0.00000000     0.00000000     0.00000000     0.00000000     0.00000001     0.00000000
   MO  23     0.00000007     0.00031731     0.00000000     0.00000000     0.00000000     0.00010302     0.00897086     0.00000007
   MO  24     0.00000001     0.00000087     0.00000000     0.00000000     0.00000000    -0.00897102     0.00010302     0.00000026
   MO  25     0.00004870    -0.00000038     0.00000000     0.00000000     0.00000000     0.00000000     0.00000001    -0.00162490
   MO  26    -0.01264139    -0.00000022     0.00000000     0.00000000     0.00000000     0.00000001     0.00000002    -0.00000602
   MO  27    -0.00000006     0.06805088     0.00000000     0.00000000     0.00000000    -0.00000042    -0.00005848     0.00000009
   MO  28     0.00000000     0.00000000     0.00000130    -0.00138615     0.00000001     0.00000000     0.00000000     0.00000000
   MO  29     0.00000000     0.00000000    -0.00138389    -0.00000130     0.00000003     0.00000000     0.00000000     0.00000000
   MO  30     0.00000000     0.00000000     0.00000002     0.00000001     0.00085586     0.00000000     0.00000000     0.00000000
   MO  31     0.00000000     0.00000000     0.00000140    -0.00000029     0.00000000     0.00000000     0.00000000     0.00000000
   MO  32     0.00000000     0.00000000     0.00000030     0.00000137     0.00000000     0.00000000     0.00000000     0.00000000
   MO  33     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  34     0.00000000     0.00000000     0.00000000     0.00000000     0.00000071     0.00000000     0.00000000     0.00000000
   MO  35     0.00000000     0.00000000     0.00000148    -0.00069310     0.00000001     0.00000000     0.00000000     0.00000000
   MO  36     0.00000000     0.00000000    -0.00069162    -0.00000148     0.00000001     0.00000000     0.00000000     0.00000000
   MO  37     0.00000000     0.00000000     0.00000001     0.00000000     0.00046229     0.00000000     0.00000000     0.00000000
   MO  38     0.00000000     0.00000340     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                MO  17         MO  18         MO  19         MO  20         MO  21         MO  22         MO  23         MO  24
   MO   5     0.00000000     0.00283651     0.00000000     0.00000000     0.00000000     0.00281667     0.00002158     0.00000006
   MO   6     0.00000008     0.00000015     0.00000000     0.00000000     0.00000000     0.00000000    -0.00026466     0.05981145
   MO   7    -0.00000007     0.00042636     0.00000000     0.00000000     0.00000000     0.00000046     0.05981173     0.00026466
   MO   8     0.00000223     0.00000007     0.00000000     0.00000000     0.00000000     0.00000000     0.00000016    -0.00000008
   MO   9     0.01561318    -0.00000001     0.00000000     0.00000000     0.00000000     0.00000000     0.00000007     0.00000001
   MO  10    -0.00000004     0.08864570     0.00000000     0.00000000     0.00000000     0.00011066     0.00031731     0.00000087
   MO  11     0.00000000     0.00000000    -0.00000678    -0.00418986    -0.00000011     0.00000000     0.00000000     0.00000000
   MO  12     0.00000000     0.00000000     0.00419673    -0.00000678     0.00000000     0.00000000     0.00000000     0.00000000
   MO  13     0.00000000     0.00000000    -0.00000003     0.00000011    -0.00257689     0.00000000     0.00000000     0.00000000
   MO  14    -0.00000001     0.00000013     0.00000000     0.00000000     0.00000000     0.00000000     0.00010302    -0.00897102
   MO  15    -0.00000001     0.00001485     0.00000000     0.00000000     0.00000000     0.00000001     0.00897086     0.00010302
   MO  16    -0.00000001     0.00000010     0.00000000     0.00000000     0.00000000     0.00000000     0.00000007     0.00000026
   MO  17     0.00190954    -0.00000001     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000001
   MO  18    -0.00000001     0.01156803     0.00000000     0.00000000     0.00000000     0.00002214     0.00008022     0.00000031
   MO  19     0.00000000     0.00000000     0.00350567    -0.00000001     0.00000001     0.00000000     0.00000000     0.00000000
   MO  20     0.00000000     0.00000000    -0.00000001     0.00350016     0.00000002     0.00000000     0.00000000     0.00000000
   MO  21     0.00000000     0.00000000     0.00000001     0.00000002     0.00220717     0.00000000     0.00000000     0.00000000
   MO  22     0.00000000     0.00002214     0.00000000     0.00000000     0.00000000     0.00327500     0.00000023     0.00000000
   MO  23     0.00000000     0.00008022     0.00000000     0.00000000     0.00000000     0.00000023     0.00710444     0.00000000
   MO  24     0.00000001     0.00000031     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00710431
   MO  25     0.00000601    -0.00000004     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000003
   MO  26    -0.00161961    -0.00000003     0.00000000     0.00000000     0.00000000     0.00000000     0.00000001    -0.00000001
   MO  27    -0.00000001     0.00934405     0.00000000     0.00000000     0.00000000     0.00002784     0.00001105     0.00000003
   MO  28     0.00000000     0.00000000    -0.00123664     0.00000084    -0.00000001     0.00000000     0.00000000     0.00000000
   MO  29     0.00000000     0.00000000     0.00000084     0.00123480     0.00000002     0.00000000     0.00000000     0.00000000
   MO  30     0.00000000     0.00000000     0.00000000     0.00000001    -0.00080269     0.00000000     0.00000000     0.00000000
   MO  31     0.00000000     0.00000000    -0.00000025    -0.00000116     0.00000000     0.00000000     0.00000000     0.00000000
   MO  32     0.00000000     0.00000000     0.00000117    -0.00000025     0.00000000     0.00000000     0.00000000     0.00000000
   MO  33     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  34     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000130     0.00000000     0.00000000     0.00000000
   MO  35     0.00000000     0.00000000    -0.00069316    -0.00000036     0.00000000     0.00000000     0.00000000     0.00000000
   MO  36     0.00000000     0.00000000    -0.00000036     0.00069168     0.00000001     0.00000000     0.00000000     0.00000000
   MO  37     0.00000000     0.00000000     0.00000000     0.00000001    -0.00062656     0.00000000     0.00000000     0.00000000
   MO  38     0.00000000    -0.00000065     0.00000000     0.00000000     0.00000000    -0.00016206    -0.00000001     0.00000000

                MO  25         MO  26         MO  27         MO  28         MO  29         MO  30         MO  31         MO  32
   MO   5    -0.00000002    -0.00000001     0.00267467     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   6    -0.00000030    -0.00000010     0.00000030     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   7     0.00000000     0.00000015    -0.00014777     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   8    -0.01266028    -0.00004874     0.00000011     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   9     0.00004870    -0.01264139    -0.00000006     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  10    -0.00000038    -0.00000022     0.06805088     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  11     0.00000000     0.00000000     0.00000000     0.00000130    -0.00138389     0.00000002     0.00000140     0.00000030
   MO  12     0.00000000     0.00000000     0.00000000    -0.00138615    -0.00000130     0.00000001    -0.00000029     0.00000137
   MO  13     0.00000000     0.00000000     0.00000000     0.00000001     0.00000003     0.00085586     0.00000000     0.00000000
   MO  14     0.00000000     0.00000001    -0.00000042     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  15     0.00000001     0.00000002    -0.00005848     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  16    -0.00162490    -0.00000602     0.00000009     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  17     0.00000601    -0.00161961    -0.00000001     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  18    -0.00000004    -0.00000003     0.00934405     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  19     0.00000000     0.00000000     0.00000000    -0.00123664     0.00000084     0.00000000    -0.00000025     0.00000117
   MO  20     0.00000000     0.00000000     0.00000000     0.00000084     0.00123480     0.00000001    -0.00000116    -0.00000025
   MO  21     0.00000000     0.00000000     0.00000000    -0.00000001     0.00000002    -0.00080269     0.00000000     0.00000000
   MO  22     0.00000000     0.00000000     0.00002784     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  23     0.00000000     0.00000001     0.00001105     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  24    -0.00000003    -0.00000001     0.00000003     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  25     0.00143857     0.00000001    -0.00000004     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  26     0.00000001     0.00143561    -0.00000002     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  27    -0.00000004    -0.00000002     0.00782546     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  28     0.00000000     0.00000000     0.00000000     0.00048833     0.00000000     0.00000000     0.00000009    -0.00000041
   MO  29     0.00000000     0.00000000     0.00000000     0.00000000     0.00048767     0.00000000    -0.00000041    -0.00000009
   MO  30     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00033454     0.00000000     0.00000000
   MO  31     0.00000000     0.00000000     0.00000000     0.00000009    -0.00000041     0.00000000     0.00302704     0.00000000
   MO  32     0.00000000     0.00000000     0.00000000    -0.00000041    -0.00000009     0.00000000     0.00000000     0.00302704
   MO  33     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  34     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000094     0.00000000     0.00000000
   MO  35     0.00000000     0.00000000     0.00000000     0.00032073    -0.00000038     0.00000000     0.00000022    -0.00000102
   MO  36     0.00000000     0.00000000     0.00000000     0.00000038     0.00032004     0.00000000    -0.00000101    -0.00000022
   MO  37     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00038502     0.00000000     0.00000000
   MO  38     0.00000000     0.00000000    -0.00000107     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                MO  33         MO  34         MO  35         MO  36         MO  37         MO  38
   MO   5     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00047751
   MO   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000001
   MO   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   9     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  10     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000340
   MO  11     0.00000000     0.00000000     0.00000148    -0.00069162     0.00000001     0.00000000
   MO  12     0.00000000     0.00000000    -0.00069310    -0.00000148     0.00000000     0.00000000
   MO  13     0.00000000     0.00000071     0.00000001     0.00000001     0.00046229     0.00000000
   MO  14     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  15     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  16     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  17     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  18     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000065
   MO  19     0.00000000     0.00000000    -0.00069316    -0.00000036     0.00000000     0.00000000
   MO  20     0.00000000     0.00000000    -0.00000036     0.00069168     0.00000001     0.00000000
   MO  21     0.00000000    -0.00000130     0.00000000     0.00000001    -0.00062656     0.00000000
   MO  22     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00016206
   MO  23     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000001
   MO  24     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  25     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  26     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  27     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000107
   MO  28     0.00000000     0.00000000     0.00032073     0.00000038     0.00000000     0.00000000
   MO  29     0.00000000     0.00000000    -0.00000038     0.00032004     0.00000000     0.00000000
   MO  30     0.00000000     0.00000094     0.00000000     0.00000000     0.00038502     0.00000000
   MO  31     0.00000000     0.00000000     0.00000022    -0.00000101     0.00000000     0.00000000
   MO  32     0.00000000     0.00000000    -0.00000102    -0.00000022     0.00000000     0.00000000
   MO  33     0.00112942     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  34     0.00000000     0.00112944     0.00000000     0.00000000     0.00000400     0.00000000
   MO  35     0.00000000     0.00000000     0.00191835     0.00000000     0.00000000     0.00000000
   MO  36     0.00000000     0.00000000     0.00000000     0.00191686     0.00000000     0.00000000
   MO  37     0.00000000     0.00000400     0.00000000     0.00000000     0.00203128     0.00000000
   MO  38     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000999

Natural orbital populations,block 1
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     0.98957642     0.98510195     0.78471140     0.78471127
              MO     9       MO    10       MO    11       MO    12       MO    13       MO    14       MO    15       MO    16
  occ(*)=     0.19756635     0.19754905     0.00919272     0.00917765     0.00632074     0.00576323     0.00504081     0.00504081
              MO    17       MO    18       MO    19       MO    20       MO    21       MO    22       MO    23       MO    24
  occ(*)=     0.00327483     0.00302704     0.00302704     0.00189328     0.00178751     0.00178631     0.00123946     0.00123768
              MO    25       MO    26       MO    27       MO    28       MO    29       MO    30       MO    31       MO    32
  occ(*)=     0.00112942     0.00112942     0.00015008     0.00015005     0.00012714     0.00010096     0.00005827     0.00005827
              MO    33       MO    34       MO    35       MO    36       MO    37       MO    38       MO
  occ(*)=     0.00003002     0.00002995     0.00000329     0.00000329     0.00000246     0.00000185


 total number of electrons =   12.0000000000

 test slabel:                     1
  a  


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        a   partial gross atomic populations
   ao class       1a         2a         3a         4a         5a         6a  
     3_ s       0.000000   2.000000   0.000000   0.000000   0.988402   0.001154
     3_ p       2.000000   0.000000   2.000000   2.000000   0.000000   0.000000
     3_ d       0.000000   0.000000   0.000000   0.000000   0.001175   0.983948
     3_ f       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
 
   ao class       7a         8a         9a        10a        11a        12a  
     3_ s       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     3_ p       0.000000   0.000000   0.000000   0.000000   0.009015   0.009000
     3_ d       0.784711   0.784711   0.197566   0.197549   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000000   0.000178   0.000177
 
   ao class      13a        14a        15a        16a        17a        18a  
     3_ s       0.000000   0.000000   0.000000   0.000000   0.003275   0.000000
     3_ p       0.000000   0.005495   0.000000   0.000000   0.000000   0.000000
     3_ d       0.006321   0.000000   0.005041   0.005041   0.000000   0.000000
     3_ f       0.000000   0.000268   0.000000   0.000000   0.000000   0.003027
 
   ao class      19a        20a        21a        22a        23a        24a  
     3_ s       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     3_ p       0.000000   0.000133   0.000048   0.000048   0.000000   0.000000
     3_ d       0.000000   0.000000   0.000000   0.000000   0.001239   0.001238
     3_ f       0.003027   0.001760   0.001739   0.001738   0.000000   0.000000
 
   ao class      25a        26a        27a        28a        29a        30a  
     3_ s       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     3_ p       0.000000   0.000000   0.000149   0.000149   0.000000   0.000099
     3_ d       0.000000   0.000000   0.000000   0.000000   0.000127   0.000000
     3_ f       0.001129   0.001129   0.000001   0.000001   0.000000   0.000002
 
   ao class      31a        32a        33a        34a        35a        36a  
     3_ s       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     3_ p       0.000000   0.000000   0.000000   0.000000   0.000003   0.000003
     3_ d       0.000058   0.000058   0.000030   0.000030   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
 
   ao class      37a        38a  
     3_ s       0.000000   0.000002
     3_ p       0.000002   0.000000
     3_ d       0.000000   0.000000
     3_ f       0.000000   0.000000


                        gross atomic populations
     ao            3_
      s         2.992832
      p         6.024144
      d         2.968844
      f         0.014179
    total      12.000000
 

 Total number of electrons:   12.00000000

 DA ...
