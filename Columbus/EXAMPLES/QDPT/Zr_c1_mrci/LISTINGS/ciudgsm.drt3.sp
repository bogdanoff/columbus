1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=    15)...
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.2748581090 -1.0769E-14  6.0447E-02  2.2388E-01  1.0000E-04

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  1  1    -46.2748581090 -1.0769E-14  6.0447E-02  2.2388E-01  1.0000E-04

 from bk iterations: iconv=   1

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.3232592101  4.8401E-02  1.4538E-03  3.0575E-02  1.0000E-04
 mr-sdci #  2  1    -46.3248334033  1.5742E-03  1.4503E-04  1.0316E-02  1.0000E-04
 mr-sdci #  3  1    -46.3249981079  1.6470E-04  4.9649E-05  5.3173E-03  1.0000E-04
 mr-sdci #  4  1    -46.3250322671  3.4159E-05  3.5629E-06  1.7789E-03  1.0000E-04
 mr-sdci #  5  1    -46.3250365455  4.2784E-06  1.6811E-07  3.5804E-04  1.0000E-04
 mr-sdci #  6  1    -46.3250367254  1.7988E-07  1.7301E-08  9.4186E-05  1.0000E-04

 mr-sdci  convergence criteria satisfied after  6 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  6  1    -46.3250367254  1.7988E-07  1.7301E-08  9.4186E-05  1.0000E-04

 number of reference csfs (nref) is    15.  root number (iroot) is  1.
 c0**2 =   0.92012421  c**2 (all zwalks) =   0.92012421

 eref      =    -46.274858071990   "relaxed" cnot**2         =   0.920124208068
 eci       =    -46.325036725358   deltae = eci - eref       =  -0.050178653369
 eci+dv1   =    -46.329044785034   dv1 = (1-cnot**2)*deltae  =  -0.004008059676
 eci+dv2   =    -46.329392723864   dv2 = dv1 / cnot**2       =  -0.004355998506
 eci+dv3   =    -46.329806814297   dv3 = dv1 / (2*cnot**2-1) =  -0.004770088939
 eci+pople =    -46.327210636682   (  4e- scaled deltae )    =  -0.052352564692
