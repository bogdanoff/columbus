
 program cipc      

 print the csf info for mrsdci wave functions

 written by: ron shepard

 version date: 06-jun-96

 This Version of Program cipc is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 workspace allocation parameters: lencor=  13107200 mem1=         0 ifirst=         1

 drt header information:
  cidrt_title                                                                    
 nmot  =    38 niot  =     6 nfct  =     4 nfvt  =     0
 nrow  =    23 nsym  =     1 ssym  =     1 lenbuf=  1600
 spnorb=     F spnodd=     F lxyzir(1:3)= 0 0 0
 nwalk,xbar:         50       15       20       15        0
 nvalwt,nvalw:       50       15       20       15        0
 ncsft:            6245
 map(*)=    -1 -1 -1 -1 29 30 31 32 33 34  1  2  3  4  5  6  7  8  9 10
            11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28
 mu(*)=      2  0  0  0  0  0
 syml(*) =   1  1  1  1  1  1
 rmo(*)=     5  6  7  8  9 10

 indx01:    50 indices saved in indxv(*)
 test nroots froot                      1                     1
===================================ROOT # 1===================================

 rdhciv: CI vector file information:
  cidrt_title                                                                    
 energy computed by program ciudg.       zam792            16:57:19.165 17-Dec-13

 lenrec =   32768 lenci =      6245 ninfo =  6 nenrgy =  7 ntitle =  2

 Max. overlap with ref vector #        1
 Valid ci vector #        1
 Method:        0       96% overlap
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -4.355613553355E+01, ietype=    6,   fcore energy of type: H1(*)   
 energy( 3)= -1.721733008563E+00, ietype=    5,   fcore energy of type: Vref(*) 
 energy( 4)= -4.632503672536E+01, ietype=-1026,   total energy of type: MRSDCI  
 energy( 5)=  9.418611544077E-05, ietype=-2055, cnvginf energy of type: CI-Resid
 energy( 6)=  1.798823365817E-07, ietype=-2056, cnvginf energy of type: CI-D.E. 
 energy( 7)=  1.730128491564E-08, ietype=-2057, cnvginf energy of type: CI-ApxDE
==================================================================================
space sufficient for valid walk range           1         50
               respectively csf range           1       6245

 space is available for   4357817 coefficients.

 updated histogram parameters:
 csfmn = 0.0000E+00 csfmx = 1.0000E+00 fhist = 5.0000E-01 nhist =  20

 this program will print the csfs generated from
 the drt according to the following print options :

 1) run in batch mode: all valid roots are automatically
    analysed and csf info is printed by default contribution
    threshold 0.01 
 2) run in interactive mode
 3) generate files for cioverlap without symmetry
 4) generate files for cioverlap with symmetry

 input menu number [  1]:
================================================================================
===================================VECTOR # 1===================================
================================================================================


 rdcivnew:      77 coefficients were selected.
 workspace: ncsfmx=    6245
 ncsfmx=                  6245

 histogram parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =    6245 ncsft =    6245 ncsf =      77
 nhist =  20 fhist = 0.50000

    cmin                cmax        num  '*'=     1 csfs.
 ----------          ----------   ----- ---------|---------|---------|---------|
 5.0000E-01 <= |c| < 1.0000E+00       1 *
 2.5000E-01 <= |c| < 5.0000E-01       1 *
 1.2500E-01 <= |c| < 2.5000E-01       0
 6.2500E-02 <= |c| < 1.2500E-01       7 *******
 3.1250E-02 <= |c| < 6.2500E-02       9 *********
 1.5625E-02 <= |c| < 3.1250E-02      26 **************************
 7.8125E-03 <= |c| < 1.5625E-02      33 *********************************
 3.9062E-03 <= |c| < 7.8125E-03       0
 1.9531E-03 <= |c| < 3.9062E-03       0
 9.7656E-04 <= |c| < 1.9531E-03       0
 4.8828E-04 <= |c| < 9.7656E-04       0
 2.4414E-04 <= |c| < 4.8828E-04       0
 1.2207E-04 <= |c| < 2.4414E-04       0
 6.1035E-05 <= |c| < 1.2207E-04       0
 3.0518E-05 <= |c| < 6.1035E-05       0
 1.5259E-05 <= |c| < 3.0518E-05       0
 7.6294E-06 <= |c| < 1.5259E-05       0
 3.8147E-06 <= |c| < 7.6294E-06       0
 0.0000E+00 <= |c| < 3.8147E-06       0
                                  ----- ---------|---------|---------|---------|
                  total read =       77 total stored =      77

 from the selected csfs,
 min(|csfvec(:)|) = 1.0187E-02    max(|csfvec(:)|) = 8.5836E-01
 norm=  0.999999999999991     
 csfs will be printed based on coefficient magnitudes.

 current csfvec(*) selection parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =    6245 ncsft =    6245 ncsf =      77

 i:slabel(i) =  1: a  
 
 frozen orbital =    1    2    3    4
 symfc(*)       =    1    1    1    1
 label          =  a    a    a    a  
 rmo(*)         =    1    2    3    4
 
 internal level =    1    2    3    4    5    6
 syml(*)        =    1    1    1    1    1    1
 label          =  a    a    a    a    a    a  
 rmo(*)         =    5    6    7    8    9   10

 printing selected csfs in sorted order from cmin = 0.00000 to cmax = 1.00000

   indcsf     c     c**2   v  lab:rmo  lab:rmo   step(*)
  ------- -------- ------- - ---- --- ---- --- ------------
          3 -0.85836 0.73679 z*                    111001
         10 -0.42817 0.18333 z*                    100111
        104 -0.10208 0.01042 y           a  : 15  1110001
        187 -0.10208 0.01042 y           a  : 14  1101001
         23  0.07872 0.00620 y           a  : 18  1111000
        197  0.07578 0.00574 y           a  : 24  1101001
        112 -0.07578 0.00574 y           a  : 23  1110001
         32  0.06570 0.00432 y           a  : 27  1111000
        219  0.06253 0.00391 y           a  : 18  1100110
        228  0.04280 0.00183 y           a  : 27  1100110
       2088 -0.04178 0.00175 x  a  : 11  a  : 12 11100001
        273  0.03925 0.00154 y           a  : 16  1100011
        246 -0.03921 0.00154 y           a  : 17  1100101
       2318 -0.03519 0.00124 x  a  : 31  a  : 32 11100001
        282 -0.03276 0.00107 y           a  : 25  1100011
        255  0.03274 0.00107 y           a  : 26  1100101
       2116 -0.03154 0.00099 x  a  : 11  a  : 19 11100001
       2125 -0.03153 0.00099 x  a  : 12  a  : 20 11100001
       2132 -0.02469 0.00061 x  a  : 19  a  : 20 11100001
       1712  0.02406 0.00058 x  a  : 12  a  : 13 11100010
       1333  0.02396 0.00057 x  a  : 11  a  : 13 11100100
       4792  0.02293 0.00053 x  a  : 14  a  : 22 11001001
       3659  0.02292 0.00053 x  a  : 15  a  : 22 11010001
       2528 -0.02140 0.00046 x  a  : 18  a  : 22 11011000
       2793  0.02056 0.00042 x  a  : 13  a  : 37 11011000
       2178 -0.02043 0.00042 x  a  : 23  a  : 24 11100001
       2801 -0.01846 0.00034 x  a  : 21  a  : 37 11011000
       1740 -0.01829 0.00033 x  a  : 13  a  : 19 11100010
       1370  0.01821 0.00033 x  a  : 13  a  : 20 11100100
       1756 -0.01800 0.00032 x  a  : 12  a  : 21 11100010
       1377 -0.01792 0.00032 x  a  : 11  a  : 21 11100100
       2597  0.01780 0.00032 x  a  : 22  a  : 27 11011000
        708  0.01775 0.00031 x  a  : 23  a  : 27 11110000
       1087 -0.01775 0.00031 x  a  : 24  a  : 27 11101000
       4925  0.01764 0.00031 x  a  : 12  a  : 31 11001001
       3811  0.01764 0.00031 x  a  : 12  a  : 32 11010001
       4944 -0.01764 0.00031 x  a  : 11  a  : 32 11001001
       3790  0.01764 0.00031 x  a  : 11  a  : 31 11010001
       4823  0.01713 0.00029 x  a  : 22  a  : 24 11001001
       3677 -0.01713 0.00029 x  a  : 22  a  : 23 11010001
       4932  0.01607 0.00026 x  a  : 19  a  : 31 11001001
       3818  0.01607 0.00026 x  a  : 19  a  : 32 11010001
       4953  0.01607 0.00026 x  a  : 20  a  : 32 11001001
       3799 -0.01607 0.00026 x  a  : 20  a  : 31 11010001
       2766  0.01523 0.00023 x  a  : 11  a  : 36 11011000
       2743  0.01523 0.00023 x  a  : 12  a  : 35 11011000
       1682  0.01469 0.00022 x  a  : 36  a  : 37 11100100
       2059  0.01468 0.00022 x  a  : 35  a  : 37 11100010
        363 -0.01445 0.00021 y           a  : 22  1011001
       2097 -0.01439 0.00021 x  a  : 14  a  : 15 11100001
        924  0.01438 0.00021 x  a  : 34  a  : 37 11110000
       1301  0.01438 0.00021 x  a  : 33  a  : 37 11101000
       3855 -0.01438 0.00021 x  a  : 13  a  : 34 11010001
       4967 -0.01438 0.00021 x  a  : 13  a  : 33 11001001
       1763 -0.01423 0.00020 x  a  : 19  a  : 21 11100010
       1386  0.01416 0.00020 x  a  : 20  a  : 21 11100100
       2775 -0.01400 0.00020 x  a  : 20  a  : 36 11011000
       2750  0.01400 0.00020 x  a  : 19  a  : 35 11011000
       1250 -0.01352 0.00018 x  a  : 31  a  : 35 11101000
        873 -0.01352 0.00018 x  a  : 32  a  : 35 11110000
       1275  0.01352 0.00018 x  a  : 32  a  : 36 11101000
        896 -0.01352 0.00018 x  a  : 31  a  : 36 11110000
       2157 -0.01351 0.00018 x  a  : 14  a  : 23 11100001
       2170 -0.01351 0.00018 x  a  : 15  a  : 24 11100001
       3863  0.01305 0.00017 x  a  : 21  a  : 34 11010001
       4975  0.01305 0.00017 x  a  : 21  a  : 33 11001001
        601  0.01251 0.00016 x  a  : 15  a  : 18 11110000
        978  0.01251 0.00016 x  a  : 14  a  : 18 11101000
       5174 -0.01219 0.00015 x  a  : 18  a  : 22 11000110
       1077  0.01218 0.00015 x  a  : 14  a  : 27 11101000
        700  0.01218 0.00015 x  a  : 15  a  : 27 11110000
       1039  0.01143 0.00013 x  a  : 18  a  : 24 11101000
        649 -0.01143 0.00013 x  a  : 18  a  : 23 11110000
       5928 -0.01066 0.00011 x  a  : 16  a  : 22 11000011
       5551  0.01066 0.00011 x  a  : 17  a  : 22 11000101
       1467 -0.01019 0.00010 x  a  : 26  a  : 27 11100100
       1844  0.01019 0.00010 x  a  : 25  a  : 27 11100010
           77 csfs were printed in this range.
