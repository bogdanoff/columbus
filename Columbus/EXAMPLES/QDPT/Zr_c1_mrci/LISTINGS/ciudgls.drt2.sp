1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      2009-03.    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


================ Computing sorting integral file structure ================

                    -----z----- -----y----- -----x----- -----w----- ---total---

                CSFs       105        2520       13608        6090       22323
      internal walks       105          90          36          15         246
valid internal walks       105          90          36          15         246
 getinfoarray: info=                     6 :                     1
                  8192                  6552                  8192
                  5460                     0
 icd(3)=                  1191 ci%nnlev=                   595  l2rec=
                  8192  n2max=                  5460
 lcore1,lcore2=              13069922              13046606
 lencor,maxblo              13107200                 60000
========================================
 current settings:
 minbl3        1215
 minbl4        1215
 locmaxbl3    22684
 locmaxbuf    11342
 maxbl3       60000
 maxbl3       60000
 maxbl4       60000
 maxbuf       30006
========================================

 sorted 4-external integrals:     3 records of integral w-combinations 
                                  3 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)

 sorted 3-external integrals:     3 records of integral w-combinations 
                                  3 records of integral x-combinations of length= 30006
                                        have been written (wstat,xstat=  100.00  100.00)
 Orig.  diagonal integrals:  1electron:        34
                             0ext.    :        42
                             2ext.    :       336
                             4ext.    :       812


 Orig. off-diag. integrals:  4ext.    :     93177
                             3ext.    :     72576
                             2ext.    :     25578
                             1ext.    :      4704
                             0ext.    :       315
                             2ext. SO :         0
                             1ext. SO :         0
                             0ext. SO :         0
                             1electron:       406


 Sorted integrals            3ext.  w :     68040 x :     63504
                             4ext.  w :     82215 x :     71253


Cycle #  1 sortfile size=    393204(      12 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13069922
Cycle #  2 sortfile size=    393204(      12 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core  13069922
 minimum size of srtscr:    327670 WP (    10 records)
 maximum size of srtscr:    393204 WP (    12 records)
diagi   file:      4 records  of   1536 WP each=>       6144 WP total
ofdgi   file:     23 records  of   1536 WP each=>      35328 WP total
fil3w   file:      3 records  of  30006 WP each=>      90018 WP total
fil3x   file:      3 records  of  30006 WP each=>      90018 WP total
fil4w   file:      3 records  of  30006 WP each=>      90018 WP total
fil4x   file:      3 records  of  30006 WP each=>      90018 WP total
 compressed index vector length=                     1
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 0,
  GSET = 0,
   DAVCOR =10,
  NCOREL = 4
   nroot=1
  IVMODE = 3
  NBKITR = 1
  RTOLBK = 1e-4,1e-4,1e-4,1e-4,1e-4,1e-4,1e-4,1e-4,1e-4,
  NITER = 200
   rtolci=1e-4
  NVCIMX = 25
  NVRFMX = 25
  NVBKMX = 25
  IDEN  = 1
  CSFPRN = 10,
  update_mode=1
  nvcimn=2
  nvrfmn=2
  nvbkmn=2
 /&end
 ------------------------------------------------------------------------
lodens (list->root)=  1
invlodens (root->list)=  1
 USING SEGMENTS OF EQUAL SIZE

****************  list of control variables  ****************
 lvlprt =    0      nroot  =    1      noldv  =   0      noldhv =   0
 nunitv =    1      nbkitr =    1      niter  = 200      davcor =  10
 csfprn =   10      ivmode =    3      istrt  =   0      vout   =   0
 iortls =    0      nvbkmx =   25      ibktv  =  -1      ibkthv =  -1
 nvcimx =   25      icitv  =   -1      icithv =  -1      frcsub =   0
 nvbkmn =    2      nvcimn =    2      maxseg =   4      nrfitr =  30
 ncorel =    4      nvrfmx =   25      nvrfmn =   2      iden   =   1
 itran  =    0      froot  =    0      rtmode =   0      ncouple=   1
 skipso =    F      dalton2=    0      molcas =   0      finalv =   0
 finalw =    0      cosmocalc=   0    with_tsklst=   0
 nsegwx =    1     1     1     1
 nseg0x =    1     1     1     1
 nseg1x =    1     1     1     1
 nseg2x =    1     1     1     1
 nseg3x =    1     1     1     1
 nseg4x =    1     1     1     1
 no0ex  =      0    no1ex  =      0    no2ex  =     0    no3ex  =     0
 no4ex  =      0    nodiag =      0
 cdg4ex =    1      c3ex1ex=    1      c2ex0ex=   1
 fileloc=    0     0     0     0     0     0     0     1     1     1
 directhd=   1      noaqccshift_zyxw=      0
 critical_crit=-1.00000    critical_delta= 0.05000

 ctol   = 0.010000    lrtshift=1.000000    smalld =0.001000


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-04    1.000E-04
 Computing density:                    .drt1.state1
 using                      1  nodes and                      1  cores.
 szdg/szodg per processor=                  6144                 35328
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core           13107199 DP per process

********** Integral sort section *************


 workspace allocation information: lencor=  13107199

 echo of the input for program cisrt:
 ------------------------------------------------------------------------
  &input
  maxbl3=60000
  maxbl4=60000
  &end
 ------------------------------------------------------------------------
 
 ( 6) listing file:                    ciudgls             
 ( 5) input file:                      cisrtin   
 (17) cidrt file:                      cidrtfl             
 (11) transformed integrals file:      moints    
 (12) diagonal integral file:          diagint             
 (13) off-diagonal integral file:      ofdgint             
 (31) 4-external w integrals file:     fil4w               
 (32) 4-external x integrals file:     fil4x               
 (33) 3-external w integrals file:     fil3w               
 (34) 3-external x integrals file:     fil3x               
 (21) scratch da sorting file:         srtscr              
 (12) 2-e integral file [fsplit=2]:    moints2   

 input integral file header information:
                                                                                 
 aoints SIFS file created by argos.      zam792            16:57:17.826 17-Dec-13
  cidrt_title                                                                    
 MO-coefficients from mcscf.x                                                    
  with dummy occupation 1.0 for active orbitals                                  
  total ao core energy =    0.000000000                                          
 MCSCF energy =     -46.297255533                                                
 SIFS file created by program tran.      zam792            16:57:18.312 17-Dec-13

 input energy(*) values:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.

 total core energy =   0.000000000000E+00

 nsym = 1 nmot=  34

 symmetry  =    1
 slabel(*) =    a
 nmpsy(*)  =   34

 info(*) =          1      8192      6552      8192      5460         0

 orbital labels, i:molab(i)=
   1:tout:001   2:tout:002   3:tout:003   4:tout:004   5:tout:005   6:tout:006   7:tout:007   8:tout:008   9:tout:009  10:tout:010
  11:tout:011  12:tout:012  13:tout:013  14:tout:014  15:tout:015  16:tout:016  17:tout:017  18:tout:018  19:tout:019  20:tout:020
  21:tout:021  22:tout:022  23:tout:023  24:tout:024  25:tout:025  26:tout:026  27:tout:027  28:tout:028  29:tout:029  30:tout:030
  31:tout:031  32:tout:032  33:tout:033  34:tout:034

 input parameters:
 prnopt=  0
 ldamin=    4095 ldamax=   32767 ldainc=      64
 maxbuf=   30006 maxbl3=   60000 maxbl4=   60000 intmxo=    1536
  Using 32 bit compression 

 drt information:
  cidrt_title                                                                    
 nmotd =  38 nfctd =   4 nfvtc =   0 nmot  =  34
 nlevel =  34 niot  =   6 lowinl=  29
 orbital-to-level map(*)
   -1  -1  -1  -1  29  30  31  32  33  34   1   2   3   4   5   6   7   8   9  10
   11  12  13  14  15  16  17  18  19  20  21  22  23  24  25  26  27  28
 compressed map(*)
   29  30  31  32  33  34   1   2   3   4   5   6   7   8   9  10  11  12  13  14
   15  16  17  18  19  20  21  22  23  24  25  26  27  28
 levsym(*)
    1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1
    1   1   1   1   1   1   1   1   1   1   1   1   1   1
 repartitioning mu(*)=
   2.  0.  0.  0.  0.  0.

 new core energy added to the energy(*) list.
 from the integral file: h1_core= -4.355613553355E+01

 indxdg: diagonal integral statistics.
 total number of integrals contributing to diagonal matrix elements:      1190
 number with all external indices:       812
 number with half external - half internal indices:       336
 number with all internal indices:        42

 indxof: off-diagonal integral statistics.
    4-external integrals: num=      93177 strt=          1
    3-external integrals: num=      72576 strt=      93178
    2-external integrals: num=      25578 strt=     165754
    1-external integrals: num=       4704 strt=     191332
    0-external integrals: num=        315 strt=     196036

 total number of off-diagonal integrals:      196350


 indxof(2nd)  ittp=   3 numx(ittp)=       25578
 indxof(2nd)  ittp=   4 numx(ittp)=        4704
 indxof(2nd)  ittp=   5 numx(ittp)=         315

 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg=  12931980
 pro2e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652

 pro2e:     90013 integrals read in    17 records.

 pro2e:         0 integrals 34-ext integrals skipped.
 pro1e        1     596    1191    1786    2381    2402    2423    3018   46706   90394
   123161  131353  136813  158652
 pro1e: eref =   -1.721733008562787E+00
 total size of srtscr:                     7  records of                  32767 
 WP =               1834952 Bytes

 new core energy added to the energy(*) list.
 from the hamiltonian repartitioning, eref= -1.721733008563E+00
 putdg        1     596    1191    1786    3322   36089   57934    3018   46706   90394
   123161  131353  136813  158652

 putf:       4 buffers of length    1536 written to file 12
 diagonal integral file completed.

 putd34:     3 records of integral w-combinations and
             3 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep4e:     3 blocks of linear combinations of 4-external integrals processed.
 number of sorted 4-external integrals     153468
 number of original 4-external integrals    93177


 putf34: external integral file complete. nfilw=    31 nfilx=    32 nrecw=     3 nrecx=     3 lbufp= 30006

 putd34:     3 records of integral w-combinations and
             3 records of integral x-combinations of length= 30006 have been written.
 wstat,xstat=  100.00  100.00
 prep3e:     6 blocks of linear combinations of 3-external integrals processed.
 number of sorted 3-external integrals     131544
 number of original 3-external integrals    72576


 putf34: external integral file complete. nfilw=    33 nfilx=    34 nrecw=     3 nrecx=     3 lbufp= 30006
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd     25578         3    165754    165754     25578    196350
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      4704         4    191332    191332      4704    196350
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd       315         5    196036    196036       315    196350

 putf:      23 buffers of length    1536 written to file 13
 off-diagonal files sort completed.
 executing brd_struct for cisrtinfo
cisrtinfo:
bufszi  1536
 diagfile 4ext:     812 2ext:     336 0ext:      42
 fil4w,fil4x  :   93177 fil3w,fil3x :   72576
 ofdgint  2ext:   25578 1ext:    4704 0ext:     315so0ext:       0so1ext:       0so2ext:       0
buffer minbl4    1215 minbl3    1215 maxbl2    1218nbas:  28   0   0   0   0   0   0   0 maxbuf 30006
 CIUDG version 5.9.7 ( 5-Oct-2004)

 workspace allocation information: lcore=  13107199

 core energy values from the integral file:
 energy( 1)=  0.000000000000E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -4.355613553355E+01, ietype=    6,   fcore energy of type: H1(*)   
 energy( 3)= -1.721733008563E+00, ietype=    5,   fcore energy of type: Vref(*) 

 total core repulsion energy = -4.527786854211E+01
 nmot  =    38 niot  =     6 nfct  =     4 nfvt  =     0
 nrow  =    36 nsym  =     1 ssym  =     1 lenbuf=  1600
 nwalk,xbar:        246      105       90       36       15
 nvalwt,nvalw:      246      105       90       36       15
 ncsft:           22323
 total number of valid internal walks:     246
 nvalz,nvaly,nvalx,nvalw =      105      90      36      15

 cisrt info file parameters:
 file number  12 blocksize   1536
 mxbld   1536
 nd4ext,nd2ext,nd0ext   812   336    42
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int    93177    72576    25578     4704      315        0        0        0
 minbl4,minbl3,maxbl2  1215  1215  1218
 maxbuf 30006
 number of external orbitals per symmetry block:  28
 nmsym   1 number of internal orbitals   6
 executing brd_struct for drt
 executing brd_struct for orbinf
 executing brd_struct for momap
 calcthrxt: niot,maxw1=                     6                    66
 block size     0
 pthz,pthy,pthx,pthw:   105    90    36    15 total internal walks:     246
 maxlp3,n2lp,n1lp,n0lp    66     0     0     0
 orbsym(*)= 1 1 1 1 1 1

 setref:      105 references kept,
                0 references were marked as invalid, out of
              105 total.
 nmb.of records onel     1
 nmb.of records 2-ext    17
 nmb.of records 1-ext     4
 nmb.of records 0-ext     1
 nmb.of records 2-int     0
 nmb.of records 1-int     0
 nmb.of records 0-int     0
 ---------memory usage in DP -----------------
 < n-ex core usage >
     routines:
    fourex            62738
    threx             60409
    twoex              9922
    onex               3217
    allin              1536
    diagon             2109
               =======
   maximum            62738
 
  __ static summary __ 
   reflst               105
   hrfspc               105
               -------
   static->             105
 
  __ core required  __ 
   totstc               105
   max n-ex           62738
               -------
   totnec->           62843
 
  __ core available __ 
   totspc          13107199
   totnec -           62843
               -------
   totvec->        13044356

 number of external paths / symmetry
 vertex x     378
 vertex w     406
segment: free space=    13044356
 reducing frespc by                   964 to               13043392 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1         105|       105|         0|       105|         0|         1|
 -------------------------------------------------------------------------------
  Y 2          90|      2520|       105|        90|       105|         2|
 -------------------------------------------------------------------------------
  X 3          36|     13608|      2625|        36|       195|         3|
 -------------------------------------------------------------------------------
  W 4          15|      6090|     16233|        15|       231|         4|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=           4DP  conft+indsym=         420DP  drtbuffer=         540 DP

dimension of the ci-matrix ->>>     22323

 executing brd_struct for civct
 gentasklist: ntask=                    20
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS   
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      36     105      13608        105      36     105
     2  4   1    25      two-ext wz   2X  4 1      15     105       6090        105      15     105
     3  4   3    26      two-ext wx*  WX  4 3      15      36       6090      13608      15      36
     4  4   3    27      two-ext wx+  WX  4 3      15      36       6090      13608      15      36
     5  2   1    11      one-ext yz   1X  2 1      90     105       2520        105      90     105
     6  3   2    15      1ex3ex yx    3X  3 2      36      90      13608       2520      36      90
     7  4   2    16      1ex3ex yw    3X  4 2      15      90       6090       2520      15      90
     8  1   1     1      allint zz    OX  1 1     105     105        105        105     105     105
     9  2   2     5      0ex2ex yy    OX  2 2      90      90       2520       2520      90      90
    10  3   3     6      0ex2ex xx*   OX  3 3      36      36      13608      13608      36      36
    11  3   3    18      0ex2ex xx+   OX  3 3      36      36      13608      13608      36      36
    12  4   4     7      0ex2ex ww*   OX  4 4      15      15       6090       6090      15      15
    13  4   4    19      0ex2ex ww+   OX  4 4      15      15       6090       6090      15      15
    14  2   2    42      four-ext y   4X  2 2      90      90       2520       2520      90      90
    15  3   3    43      four-ext x   4X  3 3      36      36      13608      13608      36      36
    16  4   4    44      four-ext w   4X  4 4      15      15       6090       6090      15      15
    17  1   1    75      dg-024ext z  OX  1 1     105     105        105        105     105     105
    18  2   2    76      dg-024ext y  OX  2 2      90      90       2520       2520      90      90
    19  3   3    77      dg-024ext x  OX  3 3      36      36      13608      13608      36      36
    20  4   4    78      dg-024ext w  OX  4 4      15      15       6090       6090      15      15
----------------------------------------------------------------------------------------------------
REDTASK #   1 TIME=  19.000 N=  1 (task/type/sgbra)=(   1/24/0) (
REDTASK #   2 TIME=  18.000 N=  1 (task/type/sgbra)=(   2/25/0) (
REDTASK #   3 TIME=  17.000 N=  1 (task/type/sgbra)=(   3/26/1) (
REDTASK #   4 TIME=  16.000 N=  1 (task/type/sgbra)=(   4/27/2) (
REDTASK #   5 TIME=  15.000 N=  1 (task/type/sgbra)=(   5/11/0) (
REDTASK #   6 TIME=  14.000 N=  1 (task/type/sgbra)=(   6/15/0) (
REDTASK #   7 TIME=  13.000 N=  1 (task/type/sgbra)=(   7/16/0) (
REDTASK #   8 TIME=  12.000 N=  1 (task/type/sgbra)=(   8/ 1/0) (
REDTASK #   9 TIME=  11.000 N=  1 (task/type/sgbra)=(   9/ 5/0) (
REDTASK #  10 TIME=  10.000 N=  1 (task/type/sgbra)=(  10/ 6/1) (
REDTASK #  11 TIME=   9.000 N=  1 (task/type/sgbra)=(  11/18/2) (
REDTASK #  12 TIME=   8.000 N=  1 (task/type/sgbra)=(  12/ 7/1) (
REDTASK #  13 TIME=   7.000 N=  1 (task/type/sgbra)=(  13/19/2) (
REDTASK #  14 TIME=   6.000 N=  1 (task/type/sgbra)=(  14/42/1) (
REDTASK #  15 TIME=   5.000 N=  1 (task/type/sgbra)=(  15/43/1) (
REDTASK #  16 TIME=   4.000 N=  1 (task/type/sgbra)=(  16/44/1) (
REDTASK #  17 TIME=   3.000 N=  1 (task/type/sgbra)=(  17/75/1) (
REDTASK #  18 TIME=   2.000 N=  1 (task/type/sgbra)=(  18/76/1) (
REDTASK #  19 TIME=   1.000 N=  1 (task/type/sgbra)=(  19/77/1) (
REDTASK #  20 TIME=   0.000 N=  1 (task/type/sgbra)=(  20/78/1) (
 initializing v-file: 1:                 22323

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         975 2x:           0 4x:           0
All internal counts: zz :        6259 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 reference space has dimension     105
 dsyevx: computed roots 1 to    2(converged:   2)

    root           eigenvalues
    ----           ------------
       1         -46.3008935272
       2         -46.3008935272

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt=                   105

         vector  1 from unit 11 written to unit 49 filename cirefv              

 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=   105)...
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:             22323
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:              25
 number of roots to converge:                             1
 number of iterations:                                    1
 residual norm convergence criteria:               0.000100

          starting bk iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:        1835 2x:         415 4x:         141
All internal counts: zz :        6259 yy:           0 xx:           0 ww:           0
One-external counts: yz :        5873 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:         600 wz:         375 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1
   ht   1    -1.02302499

          calcsovref: tciref block   1

              civs   1
 refs   1    1.00000    

          calcsovref: scrb block   1

                ci   1
 civs   1    1.00000    

          calcsovref: sovref block   1

              v      1
 ref    1    1.00000    

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.3008935272 -4.4409E-16  5.9377E-02  2.1537E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.3008935272 -4.4409E-16  5.9377E-02  2.1537E-01  1.0000E-04
 
diagon:itrnv=   2
 expansion vectors are not transformed.
 matrix-vector products are not transformed.

    2 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:             22323
 number of initial trial vectors:                         2
 number of initial matrix-vector products:                1
 maximum dimension of the subspace vectors:              25
 number of roots to converge:                             1
 number of iterations:                                  200
 residual norm convergence criteria:               0.000100

          starting ci iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:        1835 2x:         415 4x:         141
All internal counts: zz :        6259 yy:        4555 xx:        1820 ww:         390
One-external counts: yz :        5873 yx:        2550 yw:        1300
Two-external counts: yy :        1330 ww:         200 xx:         730 xz:         600 wz:         375 wx:         510
Three-ext.   counts: yx :         275 yw:         150

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2
   ht   1    -1.02302499
   ht   2    -0.05937708    -0.05301544

          calcsovref: tciref block   1

              civs   1       civs   2
 refs   1    1.00000      -2.226600E-14

          calcsovref: scrb block   1

                ci   1         ci   2
 civs   1   0.962934      -0.269738    
 civs   2   0.787963        2.81293    

          calcsovref: sovref block   1

              v      1       v      2
 ref    1   0.962934      -0.269738    

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.96293362    -0.26973846

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -46.3494814752  4.8588E-02  2.5017E-03  4.4348E-02  1.0000E-04
 mr-sdci #  1  2    -45.6816870245  4.0382E-01  0.0000E+00  4.8739E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   2

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:        1835 2x:         415 4x:         141
All internal counts: zz :        6259 yy:        4555 xx:        1820 ww:         390
One-external counts: yz :        5873 yx:        2550 yw:        1300
Two-external counts: yy :        1330 ww:         200 xx:         730 xz:         600 wz:         375 wx:         510
Three-ext.   counts: yx :         275 yw:         150

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3
   ht   1    -1.02302499
   ht   2    -0.05937708    -0.05301544
   ht   3     0.00374076    -0.00183356    -0.00230040

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3
 refs   1    1.00000      -2.226600E-14  -3.703659E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3
 civs   1   0.961436      -0.149970       0.237213    
 civs   2   0.826005        1.45092       -2.40048    
 civs   3    1.07275        13.0635        7.46318    

          calcsovref: sovref block   1

              v      1       v      2       v      3
 ref    1   0.957463      -0.198353       0.209572    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.95746310    -0.19835308     0.20957212

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1    -46.3521721955  2.6907E-03  3.7925E-04  1.6751E-02  1.0000E-04
 mr-sdci #  2  2    -45.8633861705  1.8170E-01  0.0000E+00  4.2650E-01  1.0000E-04
 mr-sdci #  2  3    -45.6224920202  3.4462E-01  0.0000E+00  4.8728E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   3

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:        1835 2x:         415 4x:         141
All internal counts: zz :        6259 yy:        4555 xx:        1820 ww:         390
One-external counts: yz :        5873 yx:        2550 yw:        1300
Two-external counts: yy :        1330 ww:         200 xx:         730 xz:         600 wz:         375 wx:         510
Three-ext.   counts: yx :         275 yw:         150

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4
   ht   1    -1.02302499
   ht   2    -0.05937708    -0.05301544
   ht   3     0.00374076    -0.00183356    -0.00230040
   ht   4    -0.00267489    -0.00162545    -0.00043137    -0.00027085

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4
 refs   1    1.00000      -2.226600E-14  -3.703659E-03   2.679493E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4
 civs   1  -0.957217       0.195671       0.237424      -5.807204E-02
 civs   2  -0.830202      -0.935522       -2.55259      -0.808911    
 civs   3   -1.20429       -11.0350        6.14334       -8.19752    
 civs   4  -0.877882       -19.5094        3.49614        32.7959    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4
 ref    1  -0.955109       0.184266       0.224039       6.016515E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1    -0.95510865     0.18426593     0.22403947     0.06016515

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1    -46.3525052417  3.3305E-04  7.4730E-05  7.9042E-03  1.0000E-04
 mr-sdci #  3  2    -46.0035692690  1.4018E-01  0.0000E+00  3.0111E-01  1.0000E-04
 mr-sdci #  3  3    -45.6246691869  2.1772E-03  0.0000E+00  4.9264E-01  1.0000E-04
 mr-sdci #  3  4    -45.4598720527  1.8200E-01  0.0000E+00  5.3383E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001999
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   4

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:        1835 2x:         415 4x:         141
All internal counts: zz :        6259 yy:        4555 xx:        1820 ww:         390
One-external counts: yz :        5873 yx:        2550 yw:        1300
Two-external counts: yy :        1330 ww:         200 xx:         730 xz:         600 wz:         375 wx:         510
Three-ext.   counts: yx :         275 yw:         150

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1    -1.02302499
   ht   2    -0.05937708    -0.05301544
   ht   3     0.00374076    -0.00183356    -0.00230040
   ht   4    -0.00267489    -0.00162545    -0.00043137    -0.00027085
   ht   5    -0.00219241     0.00021327     0.00016314     0.00004746    -0.00004683

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1    1.00000      -2.226600E-14  -3.703659E-03   2.679493E-03   2.127943E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 civs   1  -0.957742       0.133185       0.269801      -9.263869E-02  -0.155133    
 civs   2  -0.830799      -0.698186       -2.67233       0.536749      -0.465541    
 civs   3   -1.22830       -9.16536        4.28132        11.2870       -2.43452    
 civs   4   -1.03923       -20.9639        4.33341       -16.8614        27.3282    
 civs   5   0.820060        29.9463       -13.7178        63.4529        49.6289    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1  -0.954232       0.174681       0.236365      -4.459767E-02   3.271714E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1    -0.95423189     0.17468141     0.23636528    -0.04459767     0.03271714

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1    -46.3525665286  6.1287E-05  6.5577E-06  2.2293E-03  1.0000E-04
 mr-sdci #  4  2    -46.0716508105  6.8082E-02  0.0000E+00  1.7077E-01  1.0000E-04
 mr-sdci #  4  3    -45.6283390145  3.6698E-03  0.0000E+00  4.9166E-01  1.0000E-04
 mr-sdci #  4  4    -45.5269912097  6.7119E-02  0.0000E+00  5.5347E-01  1.0000E-04
 mr-sdci #  4  5    -45.4205080142  1.4264E-01  0.0000E+00  6.1112E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   5

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:        1835 2x:         415 4x:         141
All internal counts: zz :        6259 yy:        4555 xx:        1820 ww:         390
One-external counts: yz :        5873 yx:        2550 yw:        1300
Two-external counts: yy :        1330 ww:         200 xx:         730 xz:         600 wz:         375 wx:         510
Three-ext.   counts: yx :         275 yw:         150

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1    -1.02302499
   ht   2    -0.05937708    -0.05301544
   ht   3     0.00374076    -0.00183356    -0.00230040
   ht   4    -0.00267489    -0.00162545    -0.00043137    -0.00027085
   ht   5    -0.00219241     0.00021327     0.00016314     0.00004746    -0.00004683
   ht   6     0.00014769    -0.00010352     0.00002237     0.00001002    -0.00000502    -0.00000669

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1    1.00000      -2.226600E-14  -3.703659E-03   2.679493E-03   2.127943E-03  -1.393905E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1   0.957698       0.110091      -0.159458      -0.187792       0.145919       0.172426    
 civs   2   0.830815      -0.519762        1.24203        2.10612       -1.36348       0.302446    
 civs   3    1.23098       -7.96028        5.05481       -9.31481       -7.61758        1.98687    
 civs   4    1.05740       -19.7155        4.32944       0.705185        21.3880       -25.2601    
 civs   5  -0.912537        34.7240        18.3627       -16.7784       -53.1491       -54.3285    
 civs   6   -1.05414        87.8794        232.576       -79.9060        112.716        22.0373    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.954177       0.148387      -0.159922      -0.175969       0.102631      -2.129665E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.95417716     0.14838717    -0.15992248    -0.17596941     0.10263142    -0.02129665

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1    -46.3525734414  6.9128E-06  6.4199E-07  7.2349E-04  1.0000E-04
 mr-sdci #  5  2    -46.0997438023  2.8093E-02  0.0000E+00  1.3893E-01  1.0000E-04
 mr-sdci #  5  3    -45.8456380226  2.1730E-01  0.0000E+00  4.0351E-01  1.0000E-04
 mr-sdci #  5  4    -45.5922091768  6.5218E-02  0.0000E+00  5.0089E-01  1.0000E-04
 mr-sdci #  5  5    -45.4810647649  6.0557E-02  0.0000E+00  5.3047E-01  1.0000E-04
 mr-sdci #  5  6    -45.4192485466  1.4138E-01  0.0000E+00  6.2140E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   6

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:        1835 2x:         415 4x:         141
All internal counts: zz :        6259 yy:        4555 xx:        1820 ww:         390
One-external counts: yz :        5873 yx:        2550 yw:        1300
Two-external counts: yy :        1330 ww:         200 xx:         730 xz:         600 wz:         375 wx:         510
Three-ext.   counts: yx :         275 yw:         150

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1    -1.02302499
   ht   2    -0.05937708    -0.05301544
   ht   3     0.00374076    -0.00183356    -0.00230040
   ht   4    -0.00267489    -0.00162545    -0.00043137    -0.00027085
   ht   5    -0.00219241     0.00021327     0.00016314     0.00004746    -0.00004683
   ht   6     0.00014769    -0.00010352     0.00002237     0.00001002    -0.00000502    -0.00000669
   ht   7     0.00005176     0.00001422     0.00000744    -0.00000230     0.00000298     0.00000043    -0.00000071

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1    1.00000      -2.226600E-14  -3.703659E-03   2.679493E-03   2.127943E-03  -1.393905E-04  -5.109012E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1   0.957750       8.159105E-02   0.165393      -8.816999E-02  -0.225334      -9.989221E-02   0.147032    
 civs   2   0.830817      -0.417238      -0.881332        1.26214        2.17613      -0.770414      -0.605474    
 civs   3    1.23120       -6.87189       -7.09222       -3.15952       -5.77005       -6.41066       -7.77593    
 civs   4    1.05890       -18.3968       -7.61672        4.23379       -5.55908        32.3860        5.41023    
 civs   5  -0.920171        35.6374       -9.66828       -6.76206        4.91739        18.3209       -79.0270    
 civs   6   -1.14118        110.805       -153.112        149.827       -148.712        10.6520        32.6282    
 civs   7   0.889094       -184.383        487.504        483.149       -211.554       -134.987       -455.147    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.954183       0.127557       0.147113      -0.125082      -0.176858       5.502625E-02   4.086874E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.95418268     0.12755738     0.14711312    -0.12508171    -0.17685779     0.05502625     0.04086874

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  6  1    -46.3525740122  5.7079E-07  1.2417E-07  2.7852E-04  1.0000E-04
 mr-sdci #  6  2    -46.1128050889  1.3061E-02  0.0000E+00  1.2749E-01  1.0000E-04
 mr-sdci #  6  3    -45.9522541883  1.0662E-01  0.0000E+00  3.1482E-01  1.0000E-04
 mr-sdci #  6  4    -45.7101683937  1.1796E-01  0.0000E+00  4.8272E-01  1.0000E-04
 mr-sdci #  6  5    -45.5656841106  8.4619E-02  0.0000E+00  5.2425E-01  1.0000E-04
 mr-sdci #  6  6    -45.4278442500  8.5957E-03  0.0000E+00  5.6043E-01  1.0000E-04
 mr-sdci #  6  7    -45.3636023559  8.5734E-02  0.0000E+00  6.4721E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.001000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   7

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:        1835 2x:         415 4x:         141
All internal counts: zz :        6259 yy:        4555 xx:        1820 ww:         390
One-external counts: yz :        5873 yx:        2550 yw:        1300
Two-external counts: yy :        1330 ww:         200 xx:         730 xz:         600 wz:         375 wx:         510
Three-ext.   counts: yx :         275 yw:         150

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1    -1.02302499
   ht   2    -0.05937708    -0.05301544
   ht   3     0.00374076    -0.00183356    -0.00230040
   ht   4    -0.00267489    -0.00162545    -0.00043137    -0.00027085
   ht   5    -0.00219241     0.00021327     0.00016314     0.00004746    -0.00004683
   ht   6     0.00014769    -0.00010352     0.00002237     0.00001002    -0.00000502    -0.00000669
   ht   7     0.00005176     0.00001422     0.00000744    -0.00000230     0.00000298     0.00000043    -0.00000071
   ht   8     0.00005993     0.00001151     0.00000161     0.00000068     0.00000075     0.00000040     0.00000006    -0.00000031

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1    1.00000      -2.226600E-14  -3.703659E-03   2.679493E-03   2.127943E-03  -1.393905E-04  -5.109012E-05  -5.893317E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1   0.957815      -8.371801E-03   0.201904      -4.095834E-02  -9.080691E-02   0.240141      -9.109412E-02   0.123594    
 civs   2   0.830818       0.258254      -0.658295       0.641900        1.47190       -2.06255      -0.803427      -0.526727    
 civs   3    1.23124        4.98334       -7.30661        4.18032       -3.22906        5.41695       -6.58230       -8.15981    
 civs   4    1.05918        14.9067       -13.0917        1.75038        3.89453        5.66785        32.6301        3.83869    
 civs   5  -0.921562       -33.1905        9.09736        10.2437       -6.88870       -9.46218        15.7314       -80.8879    
 civs   6   -1.15704       -125.647       -52.0119        103.618        158.023        171.546        15.7666        3.61813    
 civs   7    1.05112        304.963        309.218       -391.682        430.527        206.817       -144.853       -493.057    
 civs   8   0.837736        541.175        959.243        1028.42        290.590        257.624        34.7488       -311.182    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.954190      -8.747330E-02   0.148165      -8.499308E-02  -0.144219       0.165469       5.734709E-02   3.500072E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.95419009    -0.08747330     0.14816525    -0.08499308    -0.14421900     0.16546922     0.05734709     0.03500072

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  7  1    -46.3525741162  1.0402E-07  2.3218E-08  1.2540E-04  1.0000E-04
 mr-sdci #  7  2    -46.1286703452  1.5865E-02  0.0000E+00  1.4453E-01  1.0000E-04
 mr-sdci #  7  3    -46.0467294896  9.4475E-02  0.0000E+00  2.7337E-01  1.0000E-04
 mr-sdci #  7  4    -45.8537244767  1.4356E-01  0.0000E+00  5.1633E-01  1.0000E-04
 mr-sdci #  7  5    -45.7018125853  1.3613E-01  0.0000E+00  4.6135E-01  1.0000E-04
 mr-sdci #  7  6    -45.5551212910  1.2728E-01  0.0000E+00  5.1778E-01  1.0000E-04
 mr-sdci #  7  7    -45.4275489153  6.3947E-02  0.0000E+00  5.5628E-01  1.0000E-04
 mr-sdci #  7  8    -45.3420970066  6.4228E-02  0.0000E+00  6.4957E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.001000
time for vector access                 0.000000

          starting ci iteration   8

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:        1835 2x:         415 4x:         141
All internal counts: zz :        6259 yy:        4555 xx:        1820 ww:         390
One-external counts: yz :        5873 yx:        2550 yw:        1300
Two-external counts: yy :        1330 ww:         200 xx:         730 xz:         600 wz:         375 wx:         510
Three-ext.   counts: yx :         275 yw:         150

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7         ht   8
   ht   1    -1.02302499
   ht   2    -0.05937708    -0.05301544
   ht   3     0.00374076    -0.00183356    -0.00230040
   ht   4    -0.00267489    -0.00162545    -0.00043137    -0.00027085
   ht   5    -0.00219241     0.00021327     0.00016314     0.00004746    -0.00004683
   ht   6     0.00014769    -0.00010352     0.00002237     0.00001002    -0.00000502    -0.00000669
   ht   7     0.00005176     0.00001422     0.00000744    -0.00000230     0.00000298     0.00000043    -0.00000071
   ht   8     0.00005993     0.00001151     0.00000161     0.00000068     0.00000075     0.00000040     0.00000006    -0.00000031
   ht   9     0.00003092     0.00000760    -0.00000144     0.00000119     0.00000036    -0.00000003    -0.00000009     0.00000004

                ht   9
   ht   9    -0.00000006

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7       civs   8
 refs   1    1.00000      -2.226600E-14  -3.703659E-03   2.679493E-03   2.127943E-03  -1.393905E-04  -5.109012E-05  -5.893317E-05

              civs   9
 refs   1  -3.048504E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 civs   1   0.957861      -0.106264       0.223352      -5.585403E-03   2.335282E-05   0.121636       0.205548       0.148199    
 civs   2   0.830817      -7.134611E-02  -0.604453       0.263227       0.804168       -1.45538       -2.17113       7.696714E-02
 civs   3    1.23125       -2.71438       -7.45624        3.47048        5.59652        4.79795        3.16617       0.672698    
 civs   4    1.05923       -8.75162       -17.8199        2.02732      -2.293990E-02   -5.53558        11.9302       -23.5465    
 civs   5  -0.921855        25.2196        22.9927       0.106141        11.2275        8.33230       -11.2054       -62.6534    
 civs   6   -1.16038        112.501        28.5529        58.9138        148.414       -129.209        168.424        4.50499    
 civs   7    1.08520       -378.596        148.334       -53.6090       -328.329       -437.514        205.854       -78.4382    
 civs   8    1.01397       -835.908        570.126       -701.352        1033.22       -157.382        181.640       -282.544    
 civs   9   0.942445       -1486.30        1589.20        2332.95        1283.60        522.145       -492.760       -836.528    

                ci   9
 civs   1   5.379614E-02
 civs   2  -0.583492    
 civs   3   -10.3307    
 civs   4    20.9723    
 civs   5   -53.1045    
 civs   6   -2.47529    
 civs   7   -593.564    
 civs   8   -121.416    
 civs   9    1157.78    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7       v      8
 ref    1   0.954195       3.223868E-02   0.158542      -4.804103E-02  -0.100809       0.140485       0.172267      -5.175462E-03

              v      9
 ref    1   3.778027E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.95419519     0.03223868     0.15854211    -0.04804103    -0.10080885     0.14048492     0.17226749    -0.00517546

                ci   9
 ref:   1     0.03778027

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  8  1    -46.3525741381  2.1882E-08  6.7867E-09  6.9917E-05  1.0000E-04
 mr-sdci #  8  2    -46.1501786982  2.1508E-02  0.0000E+00  1.5925E-01  1.0000E-04
 mr-sdci #  8  3    -46.0854594254  3.8730E-02  0.0000E+00  1.9216E-01  1.0000E-04
 mr-sdci #  8  4    -45.9741807515  1.2046E-01  0.0000E+00  4.9015E-01  1.0000E-04
 mr-sdci #  8  5    -45.8248768716  1.2306E-01  0.0000E+00  4.6259E-01  1.0000E-04
 mr-sdci #  8  6    -45.6945993685  1.3948E-01  0.0000E+00  4.4659E-01  1.0000E-04
 mr-sdci #  8  7    -45.5431043997  1.1556E-01  0.0000E+00  5.1253E-01  1.0000E-04
 mr-sdci #  8  8    -45.3719542039  2.9857E-02  0.0000E+00  6.1524E-01  1.0000E-04
 mr-sdci #  8  9    -45.3113341753  3.3466E-02  0.0000E+00  6.0416E-01  1.0000E-04
 
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000999
time for cinew                         0.001000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mr-sdci  convergence criteria satisfied after  8 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  8  1    -46.3525741381  2.1882E-08  6.7867E-09  6.9917E-05  1.0000E-04
 mr-sdci #  8  2    -46.1501786982  2.1508E-02  0.0000E+00  1.5925E-01  1.0000E-04
 mr-sdci #  8  3    -46.0854594254  3.8730E-02  0.0000E+00  1.9216E-01  1.0000E-04
 mr-sdci #  8  4    -45.9741807515  1.2046E-01  0.0000E+00  4.9015E-01  1.0000E-04
 mr-sdci #  8  5    -45.8248768716  1.2306E-01  0.0000E+00  4.6259E-01  1.0000E-04
 mr-sdci #  8  6    -45.6945993685  1.3948E-01  0.0000E+00  4.4659E-01  1.0000E-04
 mr-sdci #  8  7    -45.5431043997  1.1556E-01  0.0000E+00  5.1253E-01  1.0000E-04
 mr-sdci #  8  8    -45.3719542039  2.9857E-02  0.0000E+00  6.1524E-01  1.0000E-04
 mr-sdci #  8  9    -45.3113341753  3.3466E-02  0.0000E+00  6.0416E-01  1.0000E-04

####################CIUDGINFO####################

   ci vector at position   1 energy=  -46.352574138102

################END OF CIUDGINFO################

 
diagon:itrnv=   0
    1 of the  10 expansion vectors are transformed.
    1 of the   9 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
maximum overlap with reference    1(overlap= 0.95420)

 information on vector: 1 from unit 11 written to unit 48 filename civout              


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -46.3525741381

                                                       internal orbitals

                                          level       1    2    3    4    5    6

                                          orbital     5    6    7    8    9   10

                                         symmetry   a    a    a    a    a    a  

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  3  1       2 -0.468731                        +-   +         +            
 z*  3  1       3  0.474852                        +-   +              +       
 z*  3  1       5 -0.474506                        +-        +    +            
 z*  3  1       6 -0.467710                        +-        +         +       
 z*  3  1      12  0.030890                        +    +-        +            
 z*  3  1      13 -0.030471                        +    +-             +       
 z*  3  1      15 -0.021541                        +     -   +    +            
 z*  3  1      16 -0.021818                        +     -   +         +       
 z*  3  1      19  0.022754                        +     -        +         +  
 z*  3  1      20 -0.023097                        +     -             +    +  
 z*  3  1      22 -0.037392                        +    +     -   +            
 z*  3  1      23 -0.037843                        +    +     -        +       
 z*  3  1      30  0.029982                        +    +          -        +  
 z*  3  1      32 -0.021214                        +    +         +          - 
 z*  3  1      34 -0.030424                        +    +               -   +  
 z*  3  1      35  0.021545                        +    +              +     - 
 z*  3  1      37 -0.030938                        +         +-   +            
 z*  3  1      38  0.030451                        +         +-        +       
 z*  3  1      41  0.023037                        +          -   +         +  
 z*  3  1      42  0.022753                        +          -        +    +  
 z*  3  1      45  0.030395                        +         +     -        +  
 z*  3  1      47 -0.021503                        +         +    +          - 
 z*  3  1      49  0.030010                        +         +          -   +  
 z*  3  1      50 -0.021249                        +         +         +     - 
 z*  3  1      61  0.017330                             +-   +    +            
 z*  3  1      62  0.017085                             +-   +         +       
 z*  3  1      67 -0.017129                             +    +-   +            
 z*  3  1      68  0.017355                             +    +-        +       
 z*  3  1      82  0.025257                             +         +-   +       
 z*  3  1      85  0.024932                             +         +    +-      
 z*  3  1      88  0.024931                             +         +         +- 
 z*  3  1      90 -0.025253                             +              +    +- 
 z*  3  1      94 -0.024882                                  +    +-   +       
 z*  3  1      97  0.025245                                  +    +    +-      
 z*  3  1     100  0.025250                                  +    +         +- 
 z*  3  1     102  0.024885                                  +         +    +- 
 y   3  1     285  0.017408             12( a  )    -   +         +            
 y   3  1     313 -0.017635             12( a  )    -   +              +       
 y   3  1     369  0.017620             12( a  )    -        +    +            
 y   3  1     397  0.017367             12( a  )    -        +         +       
 y   3  1     593  0.014214             12( a  )   +     -        +            
 y   3  1     621 -0.014399             12( a  )   +     -             +       
 y   3  1     705 -0.010051             12( a  )   +    +          -           
 y   3  1     733  0.010182             12( a  )   +    +               -      
 y   3  1     817  0.014393             12( a  )   +          -   +            
 y   3  1     845  0.014187             12( a  )   +          -        +       
 y   3  1     901 -0.010179             12( a  )   +         +     -           
 y   3  1     929 -0.010033             12( a  )   +         +          -      
 x   3  1    2729 -0.012543   13( a  )  15( a  )   +-                          
 x   3  1    2730 -0.012595   14( a  )  15( a  )   +-                          
 x   3  1    2743 -0.012570   13( a  )  16( a  )   +-                          
 x   3  1    2744  0.012550   14( a  )  16( a  )   +-                          
 x   3  1    2924  0.010164   23( a  )  25( a  )   +-                          
 x   3  1    2925 -0.010322   24( a  )  25( a  )   +-                          
 x   3  1    2948  0.010323   23( a  )  26( a  )   +-                          
 x   3  1    2949  0.010162   24( a  )  26( a  )   +-                          
 x   3  1    2971  0.012123   21( a  )  27( a  )   +-                          
 x   3  1    3064  0.011443    6( a  )  12( a  )    -   +                      
 x   3  1    3065 -0.011589    7( a  )  12( a  )    -   +                      
 x   3  1    3106  0.013729   12( a  )  15( a  )    -   +                      
 x   3  1    3120 -0.013802   12( a  )  16( a  )    -   +                      
 x   3  1    3290 -0.010320   11( a  )  25( a  )    -   +                      
 x   3  1    3314 -0.010474   11( a  )  26( a  )    -   +                      
 x   3  1    3442  0.011583    6( a  )  12( a  )    -        +                 
 x   3  1    3443  0.011420    7( a  )  12( a  )    -        +                 
 x   3  1    3484  0.013794   12( a  )  15( a  )    -        +                 
 x   3  1    3498  0.013702   12( a  )  16( a  )    -        +                 
 x   3  1    3668 -0.010468   11( a  )  25( a  )    -        +                 
 x   3  1    3692  0.010299   11( a  )  26( a  )    -        +                 
 x   3  1    3818  0.011359    4( a  )  12( a  )    -             +            
 x   3  1    3819 -0.011664    5( a  )  12( a  )    -             +            
 x   3  1    3837  0.013786   12( a  )  13( a  )    -             +            
 x   3  1    3849  0.013737   12( a  )  14( a  )    -             +            
 x   3  1    3951  0.010756    2( a  )  21( a  )    -             +            
 x   3  1    3958  0.011324    9( a  )  21( a  )    -             +            
 x   3  1    3970 -0.011568    1( a  )  22( a  )    -             +            
 x   3  1    3979  0.012252   10( a  )  22( a  )    -             +            
 x   3  1    4196 -0.011671    4( a  )  12( a  )    -                  +       
 x   3  1    4197 -0.011336    5( a  )  12( a  )    -                  +       
 x   3  1    4215  0.013710   12( a  )  13( a  )    -                  +       
 x   3  1    4227 -0.013794   12( a  )  14( a  )    -                  +       
 x   3  1    4328 -0.010737    1( a  )  21( a  )    -                  +       
 x   3  1    4337  0.011303   10( a  )  21( a  )    -                  +       
 x   3  1    4349 -0.011553    2( a  )  22( a  )    -                  +       
 x   3  1    4356 -0.012237    9( a  )  22( a  )    -                  +       
 w   3  1   16237 -0.029491    1( a  )   3( a  )   +    +                      
 w   3  1   16238 -0.029182    2( a  )   3( a  )   +    +                      
 w   3  1   16272 -0.021631    3( a  )   9( a  )   +    +                      
 w   3  1   16281  0.021930    3( a  )  10( a  )   +    +                      
 w   3  1   16289  0.022049    1( a  )  11( a  )   +    +                      
 w   3  1   16290  0.021818    2( a  )  11( a  )   +    +                      
 w   3  1   16297  0.017198    9( a  )  11( a  )   +    +                      
 w   3  1   16298 -0.017436   10( a  )  11( a  )   +    +                      
 w   3  1   16643  0.029124    1( a  )   3( a  )   +         +                 
 w   3  1   16644 -0.029474    2( a  )   3( a  )   +         +                 
 w   3  1   16678 -0.021918    3( a  )   9( a  )   +         +                 
 w   3  1   16687 -0.021587    3( a  )  10( a  )   +         +                 
 w   3  1   16695 -0.021774    1( a  )  11( a  )   +         +                 
 w   3  1   16696  0.022037    2( a  )  11( a  )   +         +                 
 w   3  1   16703  0.017427    9( a  )  11( a  )   +         +                 
 w   3  1   16704  0.017164   10( a  )  11( a  )   +         +                 
 w   3  1   17046 -0.020814    1( a  )   1( a  )   +              +            
 w   3  1   17047 -0.029213    1( a  )   2( a  )   +              +            
 w   3  1   17048  0.020819    2( a  )   2( a  )   +              +            
 w   3  1   17082 -0.021730    1( a  )   9( a  )   +              +            
 w   3  1   17083  0.021972    2( a  )   9( a  )   +              +            
 w   3  1   17090  0.012329    9( a  )   9( a  )   +              +            
 w   3  1   17091  0.021966    1( a  )  10( a  )   +              +            
 w   3  1   17092  0.021730    2( a  )  10( a  )   +              +            
 w   3  1   17099  0.017188    9( a  )  10( a  )   +              +            
 w   3  1   17100 -0.012326   10( a  )  10( a  )   +              +            
 w   3  1   17277  0.010225    1( a  )  22( a  )   +              +            
 w   3  1   17286 -0.010931   10( a  )  22( a  )   +              +            
 w   3  1   17452 -0.020627    1( a  )   1( a  )   +                   +       
 w   3  1   17453  0.029457    1( a  )   2( a  )   +                   +       
 w   3  1   17454  0.020602    2( a  )   2( a  )   +                   +       
 w   3  1   17488  0.021982    1( a  )   9( a  )   +                   +       
 w   3  1   17489  0.021672    2( a  )   9( a  )   +                   +       
 w   3  1   17496  0.012122    9( a  )   9( a  )   +                   +       
 w   3  1   17497  0.021699    1( a  )  10( a  )   +                   +       
 w   3  1   17498 -0.021982    2( a  )  10( a  )   +                   +       
 w   3  1   17505 -0.017444    9( a  )  10( a  )   +                   +       
 w   3  1   17506 -0.012136   10( a  )  10( a  )   +                   +       
 w   3  1   17684  0.010214    2( a  )  22( a  )   +                   +       
 w   3  1   17691  0.010919    9( a  )  22( a  )   +                   +       
 w   3  1   18670  0.047624    1( a  )   1( a  )        +         +            
 w   3  1   18672  0.041672    2( a  )   2( a  )        +         +            
 w   3  1   18675  0.047552    3( a  )   3( a  )        +         +            
 w   3  1   18707  0.033428    2( a  )   9( a  )        +         +            
 w   3  1   18714  0.015827    9( a  )   9( a  )        +         +            
 w   3  1   18715 -0.038536    1( a  )  10( a  )        +         +            
 w   3  1   18724  0.018149   10( a  )  10( a  )        +         +            
 w   3  1   18727 -0.038321    3( a  )  11( a  )        +         +            
 w   3  1   18735  0.018012   11( a  )  11( a  )        +         +            
 w   3  1   18747  0.013026   12( a  )  12( a  )        +         +            
 w   3  1   19076 -0.042231    1( a  )   1( a  )        +              +       
 w   3  1   19078 -0.048231    2( a  )   2( a  )        +              +       
 w   3  1   19081 -0.048173    3( a  )   3( a  )        +              +       
 w   3  1   19113 -0.039038    2( a  )   9( a  )        +              +       
 w   3  1   19120 -0.018388    9( a  )   9( a  )        +              +       
 w   3  1   19121  0.033871    1( a  )  10( a  )        +              +       
 w   3  1   19130 -0.016031   10( a  )  10( a  )        +              +       
 w   3  1   19133  0.038821    3( a  )  11( a  )        +              +       
 w   3  1   19141 -0.018247   11( a  )  11( a  )        +              +       
 w   3  1   19153 -0.013196   12( a  )  12( a  )        +              +       
 w   3  1   19888  0.048205    1( a  )   1( a  )             +    +            
 w   3  1   19890  0.042206    2( a  )   2( a  )             +    +            
 w   3  1   19893  0.048150    3( a  )   3( a  )             +    +            
 w   3  1   19925  0.033850    2( a  )   9( a  )             +    +            
 w   3  1   19932  0.016022    9( a  )   9( a  )             +    +            
 w   3  1   19933 -0.039015    1( a  )  10( a  )             +    +            
 w   3  1   19942  0.018379   10( a  )  10( a  )             +    +            
 w   3  1   19945 -0.038801    3( a  )  11( a  )             +    +            
 w   3  1   19953  0.018239   11( a  )  11( a  )             +    +            
 w   3  1   19965  0.013190   12( a  )  12( a  )             +    +            
 w   3  1   20294  0.041590    1( a  )   1( a  )             +         +       
 w   3  1   20296  0.047527    2( a  )   2( a  )             +         +       
 w   3  1   20299  0.047461    3( a  )   3( a  )             +         +       
 w   3  1   20331  0.038456    2( a  )   9( a  )             +         +       
 w   3  1   20338  0.018113    9( a  )   9( a  )             +         +       
 w   3  1   20339 -0.033361    1( a  )  10( a  )             +         +       
 w   3  1   20348  0.015796   10( a  )  10( a  )             +         +       
 w   3  1   20351 -0.038245    3( a  )  11( a  )             +         +       
 w   3  1   20359  0.017977   11( a  )  11( a  )             +         +       
 w   3  1   20371  0.013001   12( a  )  12( a  )             +         +       

 ci coefficient statistics:
           rq > 0.1                4
      0.1> rq > 0.01             161
     0.01> rq > 0.001           1196
    0.001> rq > 0.0001          2268
   0.0001> rq > 0.00001         1101
  0.00001> rq > 0.000001         408
 0.000001> rq                  17185
           all                 22323
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         975 2x:           0 4x:           0
All internal counts: zz :        6259 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.000000247459     -0.000011457636
     2     2     -0.468731455676     21.702819120761
     3     3      0.474852042966    -21.986209582248
     4     4     -0.000013463007      0.000623353149
     5     5     -0.474505695997     21.970176793847
     6     6     -0.467710104572     21.655532900594
     7     7      0.000008382514     -0.000388120289
     8     8     -0.000000038367      0.000001776434
     9     9     -0.000273649913      0.012667507679
    10    10     -0.000813261607      0.037657855913
    11    11     -0.000000614281      0.000028438678
    12    12      0.030889629780     -1.430060423792
    13    13     -0.030470776855      1.410669220798
    14    14      0.000000033899     -0.000001569349
    15    15     -0.021541352526      0.997273456203
    16    16     -0.021818087284      1.010084980737
    17    17      0.000000003502     -0.000000162138
    18    18     -0.000000026877      0.000001244070
    19    19      0.022754160805     -1.053433963257
    20    20     -0.023097366274      1.069323188683
    21    21     -0.000000985292      0.000045614871
    22    22     -0.037392209612      1.731099335093
    23    23     -0.037842699308      1.751955138402
    24    24      0.000000020751     -0.000000960715
    25    25     -0.000061323760      0.002839227690
    26    26     -0.000043223421      0.002001247034
    27    27      0.000000021586     -0.000000999349
    28    28      0.000000481422     -0.000022287835
    29    29      0.000000039620     -0.000001834081
    30    30      0.029981503880     -1.388007182604
    31    31     -0.000000018031      0.000000834628
    32    32     -0.021213907042      0.982106678215
    33    33      0.000000503371     -0.000023303974
    34    34     -0.030423850221      1.408485849893
    35    35      0.021545315621     -0.997449413882
    36    36      0.000000000623     -0.000000028784
    37    37     -0.030937649987      1.432281280211
    38    38      0.030450934966     -1.409748491968
    39    39     -0.000000013377      0.000000619252
    40    40     -0.000000042324      0.000001959062
    41    41      0.023037179096     -1.066537244259
    42    42      0.022752741477     -1.053368894265
    43    43     -0.000000303064      0.000014030473
    44    44      0.000000066125     -0.000003061133
    45    45      0.030394520712     -1.407127489178
    46    46     -0.000000027190      0.000001258651
    47    47     -0.021502615863      0.995470260287
    48    48     -0.000000311994      0.000014444088
    49    49      0.030009547803     -1.389305044644
    50    50     -0.021248619456      0.983711438464
    51    51      0.000000000336     -0.000000015534
    52    52      0.000033250906     -0.001540943853
    53    53      0.000000521257     -0.000024132064
    54    54     -0.000000598253      0.000027696550
    55    55      0.000001745363     -0.000082509148
    56    56     -0.000001048110      0.000048523069
    57    57     -0.000000021114      0.000000977472
    58    58     -0.000050735957      0.002348812404
    59    59     -0.000000542627      0.000025121314
    60    60      0.000014142307     -0.000654664165
    61    61      0.017330130349     -0.801786373920
    62    62      0.017084962031     -0.790443571737
    63    63     -0.000000396898      0.000018363773
    64    64      0.000000002545     -0.000000117776
    65    65      0.004564222294     -0.211222197191
    66    66     -0.004456191326      0.206223245679
    67    67     -0.017128564881      0.792460850386
    68    68      0.017355402299     -0.802955596905
    69    69     -0.000000637528      0.000029497276
    70    70     -0.000000005030      0.000000232745
    71    71     -0.006361808999      0.294408929703
    72    72     -0.006456398542      0.298786303753
    73    73     -0.000000136035      0.000006295336
    74    74     -0.000000061332      0.000002838342
    75    75      0.000001400884     -0.000065542630
    76    76     -0.000000092387      0.000004275481
    77    77      0.000008828851     -0.000407755051
    78    78      0.000000115122     -0.000005327746
    79    79     -0.000005286552      0.000244046827
    80    80      0.000014315995     -0.000661785167
    81    81     -0.000000014419      0.000000667165
    82    82      0.025256753671     -1.168610819055
    83    83     -0.000000644671      0.000029827931
    84    84     -0.000000095501      0.000004419605
    85    85      0.024932120328     -1.153590140617
    86    86      0.000000056331     -0.000002606900
    87    87      0.000000000052     -0.000000002413
    88    88      0.024931149713     -1.153545386734
    89    89     -0.000000641426      0.000029677745
    90    90     -0.025253486336      1.168459643924
    91    91     -0.000000001625      0.000000075201
    92    92     -0.004543813672      0.210277529494
    93    93      0.004541665375     -0.210177635026
    94    94     -0.024882252004      1.151281534557
    95    95      0.000000401356     -0.000018570051
    96    96     -0.000000152984      0.000007079758
    97    97      0.025244512930     -1.168042947431
    98    98      0.000000090911     -0.000004207156
    99    99     -0.000000000100      0.000000004604
   100   100      0.025250210881     -1.168306715516
   101   101      0.000000399458     -0.000018482254
   102   102      0.024885469822     -1.151430425570
   103   103      0.000030839564     -0.001426852286
   104   104     -0.000016993247      0.000786585386
   105   105      0.000000004553     -0.000000210678

 number of reference csfs (nref) is   105.  root number (iroot) is  1.
 c0**2 =   0.91059980  c**2 (all zwalks) =   0.91059980

 pople ci energy extrapolation is computed with  4 correlated electrons.

 eref      =    -46.300864681998   "relaxed" cnot**2         =   0.910599797521
 eci       =    -46.352574138102   deltae = eci - eref       =  -0.051709456104
 eci+dv1   =    -46.357196973948   dv1 = (1-cnot**2)*deltae  =  -0.004622835846
 eci+dv2   =    -46.357650831352   dv2 = dv1 / cnot**2       =  -0.005076693250
 eci+dv3   =    -46.358203507471   dv3 = dv1 / (2*cnot**2-1) =  -0.005629369369
 eci+pople =    -46.355106397390   (  4e- scaled deltae )    =  -0.054241715392
 passed aftci ... 
 readint2: molcas,dalton2=                     0                     0
 files%faoints=aoints              
lodens (list->root)=  1
sifcfg setup: record length 4096 DP
# d1 elements per record  3272
# d2 elements per record  2730
  The MR-CISD density will be calculated.
 item #                     1 suffix=:.drt1.state1:
 read_civout: repnuc=  -45.2778685421096     
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method:  0 last record  1max overlap with ref# 95% root-following 0
 MR-CISD energy:   -46.35257414    -1.07470560
 residuum:     0.00006992
 deltae:     0.00000002
 apxde:     0.00000001

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.95419519     0.03223868     0.15854211    -0.04804103    -0.10080885     0.14048492     0.17226749    -0.00517546

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16
 ref:   1     0.03778027     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.95419519     0.03223868     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 computing final density
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=     525  DYX=     275  DYW=     150
   D0Z=     906  D0Y=     760  D0X=     220  D0W=      60
  DDZI=     395 DDYI=     285 DDXI=      90 DDWI=      40
  DDZE=       0 DDYE=      90 DDXE=      36 DDWE=      15
================================================================================
Trace of MO density:     4.000000
    4  correlated and     8  frozen core electrons

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   2     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   3     0.00000000     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   4     0.00000000     0.00000000     0.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   5     0.00000000     0.00000000     0.00000000     0.00000000     1.83322640    -0.00004227     0.00004477    -0.00000020
   MO   6     0.00000000     0.00000000     0.00000000     0.00000000    -0.00004227     0.49712706     0.00034679     0.00000014
   MO   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00004477     0.00034679     0.49575544    -0.00000012
   MO   8     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000020     0.00000014    -0.00000012     0.49416423
   MO   9     0.00000000     0.00000000     0.00000000     0.00000000     0.00000087     0.00000011     0.00000006    -0.00069017
   MO  10     0.00000000     0.00000000     0.00000000     0.00000000     0.03764496     0.00028064    -0.00054947     0.00000251
   MO  11     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  12     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  13     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  14     0.00000000     0.00000000     0.00000000     0.00000000     0.00001181    -0.00927950     0.00006072     0.00000026
   MO  15     0.00000000     0.00000000     0.00000000     0.00000000    -0.00002900     0.00007011     0.00925974    -0.00000005
   MO  16     0.00000000     0.00000000     0.00000000     0.00000000     0.00000003     0.00000028     0.00000005     0.00887723
   MO  17     0.00000000     0.00000000     0.00000000     0.00000000     0.00000024     0.00000001    -0.00000002    -0.00000864
   MO  18     0.00000000     0.00000000     0.00000000     0.00000000     0.01044571     0.00000240     0.00002910    -0.00000004
   MO  19     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  20     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  21     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  22     0.00000000     0.00000000     0.00000000     0.00000000    -0.04822355     0.00000106    -0.00000003     0.00000000
   MO  23     0.00000000     0.00000000     0.00000000     0.00000000     0.00005657    -0.00003896     0.00984123     0.00000002
   MO  24     0.00000000     0.00000000     0.00000000     0.00000000    -0.00001392     0.00986096     0.00004821    -0.00000004
   MO  25     0.00000000     0.00000000     0.00000000     0.00000000     0.00000002    -0.00000006     0.00000002    -0.00927115
   MO  26     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000031    -0.00000001     0.00000004    -0.00002551
   MO  27     0.00000000     0.00000000     0.00000000     0.00000000     0.01176644     0.00000271    -0.00004334     0.00000000
   MO  28     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  29     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  30     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  31     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  32     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  33     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  34     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  35     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  36     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  37     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  38     0.00000000     0.00000000     0.00000000     0.00000000     0.00022116    -0.00000002    -0.00000003     0.00000000

                MO   9         MO  10         MO  11         MO  12         MO  13         MO  14         MO  15         MO  16
   MO   5     0.00000087     0.03764496     0.00000000     0.00000000     0.00000000     0.00001181    -0.00002900     0.00000003
   MO   6     0.00000011     0.00028064     0.00000000     0.00000000     0.00000000    -0.00927950     0.00007011     0.00000028
   MO   7     0.00000006    -0.00054947     0.00000000     0.00000000     0.00000000     0.00006072     0.00925974     0.00000005
   MO   8    -0.00069017     0.00000251     0.00000000     0.00000000     0.00000000     0.00000026    -0.00000005     0.00887723
   MO   9     0.49348970    -0.00001109     0.00000000     0.00000000     0.00000000     0.00000000     0.00000001    -0.00001119
   MO  10    -0.00001109     0.01574408     0.00000000     0.00000000     0.00000000    -0.00000381    -0.00001716     0.00000005
   MO  11     0.00000000     0.00000000     0.03220440    -0.00000025    -0.00000001     0.00000000     0.00000000     0.00000000
   MO  12     0.00000000     0.00000000    -0.00000025     0.03220452     0.00000000     0.00000000     0.00000000     0.00000000
   MO  13     0.00000000     0.00000000    -0.00000001     0.00000000     0.03191085     0.00000000     0.00000000     0.00000000
   MO  14     0.00000000    -0.00000381     0.00000000     0.00000000     0.00000000     0.00117848    -0.00000034     0.00000000
   MO  15     0.00000001    -0.00001716     0.00000000     0.00000000     0.00000000    -0.00000034     0.00117713     0.00000000
   MO  16    -0.00001119     0.00000005     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00109483
   MO  17     0.00886807    -0.00000015     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000073
   MO  18    -0.00000015     0.00246348     0.00000000     0.00000000     0.00000000    -0.00000019     0.00000163     0.00000000
   MO  19     0.00000000     0.00000000    -0.00003410     0.02113364    -0.00000012     0.00000000     0.00000000     0.00000000
   MO  20     0.00000000     0.00000000    -0.02113333    -0.00003422     0.00000069     0.00000000     0.00000000     0.00000000
   MO  21     0.00000000     0.00000000    -0.00000067    -0.00000010    -0.02048867     0.00000000     0.00000000     0.00000000
   MO  22     0.00000000     0.00003150     0.00000000     0.00000000     0.00000000    -0.00000020     0.00000013     0.00000000
   MO  23     0.00000002     0.00000233     0.00000000     0.00000000     0.00000000     0.00001507     0.00134566     0.00000001
   MO  24    -0.00000001     0.00000389     0.00000000     0.00000000     0.00000000    -0.00134722     0.00001585     0.00000004
   MO  25     0.00004585    -0.00000004     0.00000000     0.00000000     0.00000000    -0.00000003     0.00000001    -0.00119931
   MO  26    -0.00926186     0.00000014     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000359
   MO  27    -0.00000018     0.00275823     0.00000000     0.00000000     0.00000000    -0.00000031    -0.00000805     0.00000000
   MO  28     0.00000000     0.00000000     0.00000370    -0.00408914     0.00000005     0.00000000     0.00000000     0.00000000
   MO  29     0.00000000     0.00000000    -0.00408897    -0.00000398     0.00000008     0.00000000     0.00000000     0.00000000
   MO  30     0.00000000     0.00000000     0.00000007     0.00000004     0.00373773     0.00000000     0.00000000     0.00000000
   MO  31     0.00000000     0.00000000     0.00000223    -0.00000156     0.00000000     0.00000000     0.00000000     0.00000000
   MO  32     0.00000000     0.00000000    -0.00000032    -0.00000425     0.00000000     0.00000000     0.00000000     0.00000000
   MO  33     0.00000000     0.00000000     0.00000000    -0.00000002    -0.00000257     0.00000000     0.00000000     0.00000000
   MO  34     0.00000000     0.00000000     0.00000002     0.00000000    -0.00000341     0.00000000     0.00000000     0.00000000
   MO  35     0.00000000     0.00000000     0.00000031     0.00093601    -0.00000002     0.00000000     0.00000000     0.00000000
   MO  36     0.00000000     0.00000000     0.00093336     0.00000430    -0.00000006     0.00000000     0.00000000     0.00000000
   MO  37     0.00000000     0.00000000    -0.00000006    -0.00000002    -0.00096443     0.00000000     0.00000000     0.00000000
   MO  38     0.00000000    -0.00002710     0.00000000     0.00000000     0.00000000    -0.00000001     0.00000002     0.00000000

                MO  17         MO  18         MO  19         MO  20         MO  21         MO  22         MO  23         MO  24
   MO   5     0.00000024     0.01044571     0.00000000     0.00000000     0.00000000    -0.04822355     0.00005657    -0.00001392
   MO   6     0.00000001     0.00000240     0.00000000     0.00000000     0.00000000     0.00000106    -0.00003896     0.00986096
   MO   7    -0.00000002     0.00002910     0.00000000     0.00000000     0.00000000    -0.00000003     0.00984123     0.00004821
   MO   8    -0.00000864    -0.00000004     0.00000000     0.00000000     0.00000000     0.00000000     0.00000002    -0.00000004
   MO   9     0.00886807    -0.00000015     0.00000000     0.00000000     0.00000000     0.00000000     0.00000002    -0.00000001
   MO  10    -0.00000015     0.00246348     0.00000000     0.00000000     0.00000000     0.00003150     0.00000233     0.00000389
   MO  11     0.00000000     0.00000000    -0.00003410    -0.02113333    -0.00000067     0.00000000     0.00000000     0.00000000
   MO  12     0.00000000     0.00000000     0.02113364    -0.00003422    -0.00000010     0.00000000     0.00000000     0.00000000
   MO  13     0.00000000     0.00000000    -0.00000012     0.00000069    -0.02048867     0.00000000     0.00000000     0.00000000
   MO  14     0.00000000    -0.00000019     0.00000000     0.00000000     0.00000000    -0.00000020     0.00001507    -0.00134722
   MO  15     0.00000000     0.00000163     0.00000000     0.00000000     0.00000000     0.00000013     0.00134566     0.00001585
   MO  16    -0.00000073     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000001     0.00000004
   MO  17     0.00109415    -0.00000001     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  18    -0.00000001     0.00061893     0.00000000     0.00000000     0.00000000    -0.00004656     0.00000687     0.00000028
   MO  19     0.00000000     0.00000000     0.01436288    -0.00000020     0.00000001     0.00000000     0.00000000     0.00000000
   MO  20     0.00000000     0.00000000    -0.00000020     0.01436251     0.00000000     0.00000000     0.00000000     0.00000000
   MO  21     0.00000000     0.00000000     0.00000001     0.00000000     0.01360691     0.00000000     0.00000000     0.00000000
   MO  22     0.00000000    -0.00004656     0.00000000     0.00000000     0.00000000     0.00717386    -0.00000028     0.00000025
   MO  23     0.00000000     0.00000687     0.00000000     0.00000000     0.00000000    -0.00000028     0.00167547     0.00000052
   MO  24     0.00000000     0.00000028     0.00000000     0.00000000     0.00000000     0.00000025     0.00000052     0.00167754
   MO  25     0.00000530     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  26    -0.00119849     0.00000001     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  27    -0.00000002     0.00062511     0.00000000     0.00000000     0.00000000    -0.00005764    -0.00000465     0.00000035
   MO  28     0.00000000     0.00000000    -0.00310341     0.00000224    -0.00000002     0.00000000     0.00000000     0.00000000
   MO  29     0.00000000     0.00000000     0.00000197     0.00310325     0.00000004     0.00000000     0.00000000     0.00000000
   MO  30     0.00000000     0.00000000     0.00000001     0.00000004    -0.00278916     0.00000000     0.00000000     0.00000000
   MO  31     0.00000000     0.00000000    -0.00000126    -0.00000188     0.00000000     0.00000000     0.00000000     0.00000000
   MO  32     0.00000000     0.00000000    -0.00000341     0.00000032     0.00000000     0.00000000     0.00000000     0.00000000
   MO  33     0.00000000     0.00000000    -0.00000001     0.00000000     0.00000218     0.00000000     0.00000000     0.00000000
   MO  34     0.00000000     0.00000000     0.00000000    -0.00000002     0.00000299     0.00000000     0.00000000     0.00000000
   MO  35     0.00000000     0.00000000     0.00071212    -0.00000152     0.00000001     0.00000000     0.00000000     0.00000000
   MO  36     0.00000000     0.00000000     0.00000225    -0.00070995     0.00000003     0.00000000     0.00000000     0.00000000
   MO  37     0.00000000     0.00000000    -0.00000001     0.00000002     0.00087529     0.00000000     0.00000000     0.00000000
   MO  38     0.00000000    -0.00000761     0.00000000     0.00000000     0.00000000    -0.00032481    -0.00000005     0.00000001

                MO  25         MO  26         MO  27         MO  28         MO  29         MO  30         MO  31         MO  32
   MO   5     0.00000002    -0.00000031     0.01176644     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   6    -0.00000006    -0.00000001     0.00000271     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   7     0.00000002     0.00000004    -0.00004334     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   8    -0.00927115    -0.00002551     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   9     0.00004585    -0.00926186    -0.00000018     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  10    -0.00000004     0.00000014     0.00275823     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  11     0.00000000     0.00000000     0.00000000     0.00000370    -0.00408897     0.00000007     0.00000223    -0.00000032
   MO  12     0.00000000     0.00000000     0.00000000    -0.00408914    -0.00000398     0.00000004    -0.00000156    -0.00000425
   MO  13     0.00000000     0.00000000     0.00000000     0.00000005     0.00000008     0.00373773     0.00000000     0.00000000
   MO  14    -0.00000003     0.00000000    -0.00000031     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  15     0.00000001     0.00000000    -0.00000805     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  16    -0.00119931    -0.00000359     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  17     0.00000530    -0.00119849    -0.00000002     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  18     0.00000000     0.00000001     0.00062511     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  19     0.00000000     0.00000000     0.00000000    -0.00310341     0.00000197     0.00000001    -0.00000126    -0.00000341
   MO  20     0.00000000     0.00000000     0.00000000     0.00000224     0.00310325     0.00000004    -0.00000188     0.00000032
   MO  21     0.00000000     0.00000000     0.00000000    -0.00000002     0.00000004    -0.00278916     0.00000000     0.00000000
   MO  22     0.00000000     0.00000000    -0.00005764     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  23     0.00000000     0.00000000    -0.00000465     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  24     0.00000000     0.00000000     0.00000035     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  25     0.00145532    -0.00000113     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  26    -0.00000113     0.00145419     0.00000002     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  27     0.00000000     0.00000002     0.00066599     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  28     0.00000000     0.00000000     0.00000000     0.00090722     0.00000005     0.00000000     0.00000042     0.00000116
   MO  29     0.00000000     0.00000000     0.00000000     0.00000005     0.00090716     0.00000000    -0.00000073     0.00000015
   MO  30     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00080460     0.00000000     0.00000000
   MO  31     0.00000000     0.00000000     0.00000000     0.00000042    -0.00000073     0.00000000     0.00161498     0.00011649
   MO  32     0.00000000     0.00000000     0.00000000     0.00000116     0.00000015     0.00000000     0.00011649     0.00171693
   MO  33     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000084     0.00000000     0.00000000
   MO  34     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000124     0.00000000     0.00000000
   MO  35     0.00000000     0.00000000     0.00000000    -0.00019796    -0.00000042     0.00000000     0.00000065     0.00000127
   MO  36     0.00000000     0.00000000     0.00000000    -0.00000090    -0.00019719    -0.00000002     0.00000010    -0.00000015
   MO  37     0.00000000     0.00000000     0.00000000     0.00000000     0.00000001    -0.00038788     0.00000000     0.00000000
   MO  38     0.00000000     0.00000000    -0.00000988     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                MO  33         MO  34         MO  35         MO  36         MO  37         MO  38
   MO   5     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00022116
   MO   6     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000002
   MO   7     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000003
   MO   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO   9     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  10     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00002710
   MO  11     0.00000000     0.00000002     0.00000031     0.00093336    -0.00000006     0.00000000
   MO  12    -0.00000002     0.00000000     0.00093601     0.00000430    -0.00000002     0.00000000
   MO  13    -0.00000257    -0.00000341    -0.00000002    -0.00000006    -0.00096443     0.00000000
   MO  14     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000001
   MO  15     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000002
   MO  16     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  17     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  18     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000761
   MO  19    -0.00000001     0.00000000     0.00071212     0.00000225    -0.00000001     0.00000000
   MO  20     0.00000000    -0.00000002    -0.00000152    -0.00070995     0.00000002     0.00000000
   MO  21     0.00000218     0.00000299     0.00000001     0.00000003     0.00087529     0.00000000
   MO  22     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00032481
   MO  23     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000005
   MO  24     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000001
   MO  25     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  26     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
   MO  27     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -0.00000988
   MO  28     0.00000000     0.00000000    -0.00019796    -0.00000090     0.00000000     0.00000000
   MO  29     0.00000000     0.00000000    -0.00000042    -0.00019719     0.00000001     0.00000000
   MO  30    -0.00000084    -0.00000124     0.00000000    -0.00000002    -0.00038788     0.00000000
   MO  31     0.00000000     0.00000000     0.00000065     0.00000010     0.00000000     0.00000000
   MO  32     0.00000000     0.00000000     0.00000127    -0.00000015     0.00000000     0.00000000
   MO  33     0.00177665    -0.00000047     0.00000001     0.00000000     0.00000057     0.00000000
   MO  34    -0.00000047     0.00177491     0.00000000    -0.00000001    -0.00000160     0.00000000
   MO  35     0.00000001     0.00000000     0.00113258     0.00000090     0.00000000     0.00000000
   MO  36     0.00000000    -0.00000001     0.00000090     0.00113159    -0.00000001     0.00000000
   MO  37     0.00000057    -0.00000160     0.00000000    -0.00000001     0.00078643     0.00000000
   MO  38     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00002206

Natural orbital populations,block 1
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     1.83541350     0.49758044     0.49604361     0.49493018
              MO     9       MO    10       MO    11       MO    12       MO    13       MO    14       MO    15       MO    16
  occ(*)=     0.49339325     0.04682596     0.04682520     0.04571996     0.01585944     0.00580410     0.00242938     0.00242624
              MO    17       MO    18       MO    19       MO    20       MO    21       MO    22       MO    23       MO    24
  occ(*)=     0.00215502     0.00215159     0.00179312     0.00177677     0.00177479     0.00153880     0.00112403     0.00112200
              MO    25       MO    26       MO    27       MO    28       MO    29       MO    30       MO    31       MO    32
  occ(*)=     0.00101926     0.00064205     0.00064175     0.00035633     0.00035536     0.00006124     0.00006113     0.00005620
              MO    33       MO    34       MO    35       MO    36       MO    37       MO    38       MO
  occ(*)=     0.00005607     0.00001594     0.00001594     0.00001420     0.00001256     0.00000456


 total number of electrons =   12.0000000000

 test slabel:                     1
  a  


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        a   partial gross atomic populations
   ao class       1a         2a         3a         4a         5a         6a  
     3_ s       0.000000   2.000000   0.000000   0.000000   1.834491   0.000000
     3_ p       2.000000   0.000000   2.000000   2.000000   0.000000   0.000000
     3_ d       0.000000   0.000000   0.000000   0.000000   0.000922   0.497580
     3_ f       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
 
   ao class       7a         8a         9a        10a        11a        12a  
     3_ s       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     3_ p       0.000000   0.000000   0.000000   0.046794   0.046794   0.045681
     3_ d       0.496044   0.494930   0.493393   0.000000   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000032   0.000031   0.000039
 
   ao class      13a        14a        15a        16a        17a        18a  
     3_ s       0.000191   0.005735   0.000000   0.000000   0.000000   0.000000
     3_ p       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     3_ d       0.015668   0.000069   0.002429   0.002426   0.002155   0.002152
     3_ f       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
 
   ao class      19a        20a        21a        22a        23a        24a  
     3_ s       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     3_ p       0.000000   0.000000   0.000000   0.000000   0.000052   0.000051
     3_ d       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     3_ f       0.001793   0.001777   0.001775   0.001539   0.001072   0.001071
 
   ao class      25a        26a        27a        28a        29a        30a  
     3_ s       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     3_ p       0.000416   0.000613   0.000613   0.000000   0.000212   0.000000
     3_ d       0.000000   0.000000   0.000000   0.000356   0.000000   0.000061
     3_ f       0.000603   0.000029   0.000029   0.000000   0.000144   0.000000
 
   ao class      31a        32a        33a        34a        35a        36a  
     3_ s       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
     3_ p       0.000000   0.000000   0.000000   0.000016   0.000016   0.000014
     3_ d       0.000061   0.000056   0.000056   0.000000   0.000000   0.000000
     3_ f       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
 
   ao class      37a        38a  
     3_ s       0.000000   0.000004
     3_ p       0.000000   0.000000
     3_ d       0.000012   0.000000
     3_ f       0.000000   0.000000


                        gross atomic populations
     ao            3_
      s         3.840422
      p         6.141271
      d         2.008373
      f         0.009934
    total      12.000000
 

 Total number of electrons:   12.00000000

 DA ...
