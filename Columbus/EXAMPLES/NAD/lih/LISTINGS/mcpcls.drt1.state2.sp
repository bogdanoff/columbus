

     ******************************************
     **    PROGRAM:              MCPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 original author: Daniel Robertson, FSU
 later revisions: Ron Shepard, ANL;
                  Michal Dallos, University Vienna



 This Version of Program mcpc is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



   ******  File header section  ******

 Headers form the restart file:
    Hermit Integral Program : SIFS version  visnu             16:35:23.428 21-May-09
     title                                                                          


   ******  DRT info section  ******

 Informations for the DRT no.  1
 Header form the DRT file: 
     title                                                                          
 Molecular symmetry group:   sym1
 Total number of electrons:    4
 Spin multiplicity:            1
 Number of active orbitals:    7
 Number of active electrons:   2
 Total number of CSFs:        12

   ***  Informations from the DRT number:   1

 
 Symmetry orbital summary:
 Symm.blocks:         1     2     3     4
 Symm.labels:         a1    b1    b2    a2 

 List of doubly occupied orbitals:
  1 a1 

 List of active orbitals:
  2 a1   3 a1   4 a1   1 b1   2 b1   1 b2   2 b2 


   ******  MCSCF convergence information:  ******

 MCSCF convergence criteria were satisfied.

 mcscf energy=    -7.9366872782    nuclear repulsion=     0.7500000000
 demc=             0.0000000000    wnorm=                 0.0000000000
 knorm=            0.0000000000    apxde=                 0.0000000000


 MCSCF calculation performmed for   1 symmetry.

 State averaging:
 No,  ssym, navst, wavst
  1    a1     2   0.5000 0.5000

 Input the DRT No of interest: [  1]:
In the DRT No.: 1 there are  2 states.

 Which one to take? [  1]:
 The CSFs for the state No  2 of the symmetry  a1  will be printed
 according to the following print options :

 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 0) end.

 input menu number [  0]: csfs will be printed based on coefficient magnitudes.

 input the coefficient threshold (end with 0.) [ 0.0000]:
 List of active orbitals:
  2 a1   3 a1   4 a1   1 b1   2 b1   1 b2   2 b2 

   csf       coeff       coeff**2    step(*)
  -----  ------------  ------------  ------------
      2  0.9163731373  0.8397397268  1200000
      1 -0.3474095660  0.1206934065  3000000
      5 -0.1811141870  0.0328023487  0120000
      3 -0.0564871662  0.0031907999  1020000
      6  0.0554788779  0.0030779059  0030000
     11 -0.0145250394  0.0002109768  0000012
      8 -0.0145250328  0.0002109766  0001200
      7  0.0043982967  0.0000193450  0003000
     10  0.0043982843  0.0000193449  0000030
      4  0.0035680987  0.0000127313  0300000
      9 -0.0033494493  0.0000112188  0000300
     12 -0.0033494369  0.0000112187  0000003

 input the coefficient threshold (end with 0.) [ 0.0000]:
 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 0) end.

 input menu number [  0]: