

     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 This program allows the csf mixing coefficient and orbital expansion coefficient
 optimization using the graphical unitary group approach and the exponential
 operator mcscf method.
 references:  r. shepard and j. simons, ' int. j. quantum chem. symp. 14, 211 (1980).
              r. shepard, i. shavitt, and j. simons, j. chem. phys. 76, 543 (1982).
              r. shepard in "ab initio methods in quantum chemistry ii" advances in chemical
                  physics 69, edited by k. p. lawley (wiley, new york, 1987) pp. 63-200.
 Original autor: Ron Shepard, ANL
 Later revisions: Michal Dallos, University Vienna

 This Version of Program MCSCF is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.4.0.2     **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 Workspace allocation information:
        13107200 of real*8 words (  100.00 MB) of work space has been allocated.

 user input information:

 ======== echo of the mcscf input ========
 ------------------------------------------------------------------------
  &input
   niter=600,
   nmiter=30,
   nciitr=30,
   tol(3)=1.e-10,
   tol(2)=1.e-10,
   tol(1)=1.e-10,
   NSTATE=0,
   npath=1,3,9,10,13,17,19,21,23,11,12, 2,
   ncoupl=5,
   FCIORB=  1,2,20,1,3,20,1,4,20,2,1,20,2,2,20,3,1,20,3,2,20,
   NAVST(1) = 2,
   WAVST(1,1)=1 ,
   WAVST(1,2)=1 ,
  &end
 ------------------------------------------------------------------------


 ***  Integral file informations  ***


 input integral file : /home/szalay/lih_mcscf/WORK/aoints                       
    

 Integral file header information:
 Hermit Integral Program : SIFS version  visnu             16:35:23.428 21-May-09

 Core type energy values:
 energy( 1)=  7.500000000000E-01, ietype=   -1,    core energy of type: Nuc.Rep.
 total ao core energy =    0.750000000


   ******  Basis set information:  ******

 Number of irreps:                  4
 Total number of basis functions:  14

 irrep no.              1    2    3    4
 irrep label           A1   B1   B2   A2 
 no. of bas.fcions.     8    3    3    0
 warning: resetting tol(6) due to possible inconsistency.


 ***  MCSCF optimization procedure parmeters:  ***


 maximum number of mcscf iterations:        niter=   600

 maximum number of psci micro-iterations:   nmiter=   30
 maximum r,s subspace dimension allowed:    nvrsmx=   30

 tol(1)=  1.0000E-10. . . . delta-emc convergence criterion.
 tol(2)=  1.0000E-10. . . . wnorm convergence criterion.
 tol(3)=  1.0000E-10. . . . knorm convergence criterion.
 tol(4)=  1.0000E-08. . . . apxde convergence criterion.
 tol(5)=  1.0000E-04. . . . small diagonal matrix element tolerance.
 tol(6)=  1.2500E-11. . . . minimum ci-psci residual norm.
 tol(7)=  1.0000E-05. . . . maximum ci-psci residual norm.
 tol(8)=  1.0000E+00. . . . maximum abs(k(xy)) allowed.
 tol(9)=  1.0000E+00. . . . wnorm coupling tolerance.
 tol(10)= 0.0000E+00. . . . maximum psci emergency shift parameter.
 tol(11)= 0.0000E+00. . . . minimum psci emergency shift parameter.
 tol(12)= 0.0000E+00. . . . increment of psci emergency shift parameter.


 *** State averaging informations: ***


 MCSCF calculation performed for  1 DRT.

 DRT  first state   no.of aver.states   weights
  1   ground state          2             0.500 0.500

 The number of hmc(*) eigenvalues and eigenvectors calculated each iteration per symmetry:
 Symm.   no.of eigenv.(=ncol)
  A1         3

 orbital coefficients are optimized for the ground state (nstate=0).

 Orbitals included in invariant subspaces:
   symmetry   orbital   mask
       1       2(  2)    20
       1       3(  3)    20
       1       4(  4)    20
       2       1(  9)    20
       2       2( 10)    20
       3       1( 12)    20
       3       2( 13)    20

 npath(*) options:
  2:  orbital-state coupling terms will be included beginning on iteration ncoupl=  5
  3:  print intermediate timing information.
  9:  suppress the drt listing.
 10:  suppress the hmc(*) eigenvector listing.
 11:  construct the hmc(*) matrix explicitly.
 12:  diagonalize the hmc(*) matrix iteratively.
        nunitv= 1 nciitr=30 mxvadd=20 nvcimx=20
       rtolci(*),wnorm=     1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 0.0000E+00
   noldv =   0
 13:  get initial orbitals from the formatted file, mocoef.
 17:  print the final natural orbitals and occupations.
 19:  transform the virtual orbitals to diagonalize qvv(*).
 21:  write out the one- and two- electron density for further use (files:mcd1fl, mcd2fl).
 23:  use the old integral transformation.


   ******  DRT info section  ******


 Informations for the DRT no.  1

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a1 
 Total number of electrons:    4
 Spin multiplicity:            1
 Number of active orbitals:    7
 Number of active electrons:   2
 Total number of CSFs:        12
 
 !timer: initialization                  cpu_time=     0.004 walltime=     0.013

 faar:   0 active-active rotations allowed out of:   5 possible.


 Number of active-double rotations:         3
 Number of active-active rotations:         0
 Number of double-virtual rotations:        4
 Number of active-virtual rotations:       16

 Size of orbital-Hessian matrix B:                      300
 Size of the orbital-state Hessian matrix C:            552
 Total size of the state Hessian matrix M:              300
 Size of HESSIAN-matrix for quadratic conv.:           1152



   ****** Integral transformation section ******


 number of blocks to be transformed in-core is   9
 number of blocks to be transformed out of core is    0

 in-core ao list requires    1 segments.
 out of core ao list requires    0 segments.
 each segment has length ****** working precision words.

               ao integral sort statistics
 length of records:      1610
 number of buckets:   1
 scratch file used is da1:
 amount of core needed for in-core arrays:  1045

 twoao_o processed       1667 two electron ao integrals.

       1667 ao integrals were written into    2 records

 srtinc_o read in       1821 integrals and wrote out       1821 integrals.

 Source of the initial MO coeficients:

 Input MO coefficient file: /home/szalay/lih_mcscf/WORK/mocoef                          
 

               starting mcscf iteration...   1
 !timer:                                 cpu_time=     0.004 walltime=     0.022

 orbital-state coupling will not be calculated this iteration.

 number of transformed integrals put on file:      1382
 number of records written:     1
 !timer: 2-e transformation              cpu_time=     0.004 walltime=     0.013

 Size of orbital-Hessian matrix B:                      300
 Total size of the state Hessian matrix M:              300
 Total size of HESSIAN-matrix for linear con            600

 !timer: mosrt1                          cpu_time=     0.000 walltime=     0.001
 !timer: mosrt2                          cpu_time=     0.004 walltime=     0.001
 !timer: mosort                          cpu_time=     0.004 walltime=     0.001
 !timer: hdiag(*) construction           cpu_time=     0.000 walltime=     0.000
 !timer: hmcft                           cpu_time=     0.000 walltime=     0.000

 trial vectors are generated internally.

 trial vector  1 is unit matrix column     1
 *** hmc(*) convergence not reached ***
 ciiter=  30 noldhv=  3 noldv=  6
 !timer: hmc(*) diagonalization          cpu_time=     0.000 walltime=     0.001

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*      -78.2842321799      -79.0342321799      222.7176826120        0.0000000000
    2       -63.6441358549      -64.3941358549      157.5366568321        0.0000000000
    3       -61.4931456952      -62.2431456952      152.6061131349        0.0100000000
 !timer: hmcvec                          cpu_time=     0.000 walltime=     0.006
 !timer: rdft                            cpu_time=     0.000 walltime=     0.000
 !timer: mqva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfvd                            cpu_time=     0.000 walltime=     0.000
 !timer: bvaad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvaadf                          cpu_time=     0.000 walltime=     0.000
 !timer: mfad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqaa                            cpu_time=     0.000 walltime=     0.000
 !timer: mfaa                            cpu_time=     0.000 walltime=     0.000
 !timer: badad                           cpu_time=     0.000 walltime=     0.001
 !timer: badadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvava                           cpu_time=     0.000 walltime=     0.000
 !timer: bvavaf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvavd                           cpu_time=     0.000 walltime=     0.000
 !timer: bvavdf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvdadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdvd                           cpu_time=     0.000 walltime=     0.000
 !timer: hbcon                           cpu_time=     0.000 walltime=     0.003
 
  tol(10)=  0.000000000000000E+000  eshsci=  0.172611174303443     
 Total number of micro iterations:   18

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0=-0.00119577 pnorm= 0.0000E+00 rznorm= 2.2322E-06 rpnorm= 0.0000E+00 noldr= 18 nnewr= 18 nolds=  0 nnews=  0
 *** warning *** small z0.
 
 !timer: Direct Bxr time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  cpu_time=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: solvek total                    cpu_time=     0.000 walltime=     0.002

 fdd(*) eigenvalues. symmetry block  1
     3.683394

 qvv(*) eigenvalues. symmetry block  1
     7.085239    9.681534   12.641583   16.399731

 qvv(*) eigenvalues. symmetry block  2
    16.247883

 qvv(*) eigenvalues. symmetry block  3
    16.247980
 *** warning *** large active-orbital occupation. i=  2 nocc= 4.9520E+00
 *** warning *** large active-orbital occupation. i=  3 nocc= 1.2843E+01
 !timer: motran                          cpu_time=     0.000 walltime=     0.001

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 cpu_time=     0.008 walltime=     0.033

 all mcscf convergence criteria are not satisfied.
 iter=    1 emc=  -70.9641840174 demc= 7.0964E+01 wnorm= 1.3809E+00 knorm= 1.0000E+00 apxde= 2.2334E+01    *not converged* 

               starting mcscf iteration...   2
 !timer:                                 cpu_time=     0.012 walltime=     0.055

 orbital-state coupling will not be calculated this iteration.

 number of transformed integrals put on file:      1382
 number of records written:     1
 !timer: 2-e transformation              cpu_time=     0.000 walltime=     0.001

 Size of orbital-Hessian matrix B:                      300
 Total size of the state Hessian matrix M:              300
 Total size of HESSIAN-matrix for linear con            600

 !timer: mosrt1                          cpu_time=     0.000 walltime=     0.000
 !timer: mosrt2                          cpu_time=     0.000 walltime=     0.000
 !timer: mosort                          cpu_time=     0.000 walltime=     0.001
 !timer: hdiag(*) construction           cpu_time=     0.000 walltime=     0.000
 !timer: hmcft                           cpu_time=     0.000 walltime=     0.000

   3 trial vectors read from nvfile (unit= 63).
 ciiter=   4 noldhv=  3 noldv=  3
 !timer: hmc(*) diagonalization          cpu_time=     0.000 walltime=     0.017

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*       -7.9413340715       -8.6913340715        0.0000000000        0.0000100000
    2        -6.3422018300       -7.0922018300        0.0000000000        0.0000100000
    3        -6.1708010837       -6.9208010837        0.0000000000        0.0100000000
 !timer: hmcvec                          cpu_time=     0.000 walltime=     0.017
 !timer: rdft                            cpu_time=     0.000 walltime=     0.000
 !timer: mqva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfvd                            cpu_time=     0.000 walltime=     0.000
 !timer: bvaad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvaadf                          cpu_time=     0.000 walltime=     0.000
 !timer: mfad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqaa                            cpu_time=     0.000 walltime=     0.000
 !timer: mfaa                            cpu_time=     0.000 walltime=     0.000
 !timer: badad                           cpu_time=     0.004 walltime=     0.000
 !timer: badadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvava                           cpu_time=     0.000 walltime=     0.000
 !timer: bvavaf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvavd                           cpu_time=     0.000 walltime=     0.000
 !timer: bvavdf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvdadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdvd                           cpu_time=     0.000 walltime=     0.000
 !timer: hbcon                           cpu_time=     0.004 walltime=     0.001
 
  tol(10)=  0.000000000000000E+000  eshsci=  0.138791993847567     
 Total number of micro iterations:   11

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0=-0.56155928 pnorm= 0.0000E+00 rznorm= 5.0574E-06 rpnorm= 0.0000E+00 noldr= 11 nnewr= 11 nolds=  0 nnews=  0
 *** warning *** small z0.
 
 !timer: Direct Bxr time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  cpu_time=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: solvek total                    cpu_time=     0.000 walltime=     0.001

 fdd(*) eigenvalues. symmetry block  1
    -2.066846

 qvv(*) eigenvalues. symmetry block  1
     0.293236    1.044837    1.597283    3.556243

 qvv(*) eigenvalues. symmetry block  2
     3.448821

 qvv(*) eigenvalues. symmetry block  3
     3.448820
 *** warning *** small active-orbital occupation. i=  1 nocc= 3.7633E-05
 *** warning *** small active-orbital occupation. i=  1 nocc= 8.7132E-06
 *** warning *** small active-orbital occupation. i=  2 nocc= 1.8098E-04
 *** warning *** small active-orbital occupation. i=  1 nocc= 8.7134E-06
 *** warning *** small active-orbital occupation. i=  2 nocc= 1.8098E-04
 !timer: motran                          cpu_time=     0.000 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 cpu_time=     0.004 walltime=     0.021

 all mcscf convergence criteria are not satisfied.
 iter=    2 emc=   -7.1417679507 demc=-6.3822E+01 wnorm= 1.1103E+00 knorm= 8.2744E-01 apxde= 7.1182E-01    *not converged* 

               starting mcscf iteration...   3
 !timer:                                 cpu_time=     0.016 walltime=     0.076

 orbital-state coupling will not be calculated this iteration.

 number of transformed integrals put on file:      1382
 number of records written:     1
 !timer: 2-e transformation              cpu_time=     0.000 walltime=     0.001

 Size of orbital-Hessian matrix B:                      300
 Total size of the state Hessian matrix M:              300
 Total size of HESSIAN-matrix for linear con            600

 !timer: mosrt1                          cpu_time=     0.000 walltime=     0.001
 !timer: mosrt2                          cpu_time=     0.000 walltime=     0.000
 !timer: mosort                          cpu_time=     0.000 walltime=     0.001
 !timer: hdiag(*) construction           cpu_time=     0.000 walltime=     0.000
 !timer: hmcft                           cpu_time=     0.000 walltime=     0.000

   3 trial vectors read from nvfile (unit= 63).
 ciiter=   4 noldhv=  3 noldv=  3
 !timer: hmc(*) diagonalization          cpu_time=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*       -7.9537703738       -8.7037703738        0.0000000000        0.0000100000
    2        -7.7943791925       -8.5443791925        0.0000000000        0.0000100000
    3        -7.6911607742       -8.4411607742        0.0000000000        0.0100000000
 !timer: hmcvec                          cpu_time=     0.000 walltime=     0.001
 !timer: rdft                            cpu_time=     0.000 walltime=     0.000
 !timer: mqva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfvd                            cpu_time=     0.000 walltime=     0.000
 !timer: bvaad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvaadf                          cpu_time=     0.000 walltime=     0.000
 !timer: mfad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqaa                            cpu_time=     0.000 walltime=     0.000
 !timer: mfaa                            cpu_time=     0.000 walltime=     0.000
 !timer: badad                           cpu_time=     0.004 walltime=     0.000
 !timer: badadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvava                           cpu_time=     0.000 walltime=     0.000
 !timer: bvavaf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvavd                           cpu_time=     0.000 walltime=     0.000
 !timer: bvavdf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvdadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdvd                           cpu_time=     0.000 walltime=     0.000
 !timer: hbcon                           cpu_time=     0.004 walltime=     0.001
 
  tol(10)=  0.000000000000000E+000  eshsci=  5.089707177880351E-002
 Total number of micro iterations:    9

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0=-0.98103426 pnorm= 0.0000E+00 rznorm= 7.9800E-06 rpnorm= 0.0000E+00 noldr=  9 nnewr=  9 nolds=  0 nnews=  0
 
 !timer: Direct Bxr time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  cpu_time=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: solvek total                    cpu_time=     0.000 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
    -4.873331

 qvv(*) eigenvalues. symmetry block  1
     0.323030    1.258455    1.543717    3.503218

 qvv(*) eigenvalues. symmetry block  2
     3.379439

 qvv(*) eigenvalues. symmetry block  3
     3.379438
 *** warning *** small active-orbital occupation. i=  1 nocc= 9.7448E-06
 *** warning *** small active-orbital occupation. i=  2 nocc= 9.2779E-04
 *** warning *** small active-orbital occupation. i=  1 nocc= 9.7453E-06
 *** warning *** small active-orbital occupation. i=  2 nocc= 9.2772E-04
 !timer: motran                          cpu_time=     0.000 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 cpu_time=     0.004 walltime=     0.004

 all mcscf convergence criteria are not satisfied.
 iter=    3 emc=   -7.8740747832 demc= 7.3231E-01 wnorm= 4.0718E-01 knorm= 1.9383E-01 apxde= 3.5627E-02    *not converged* 

               starting mcscf iteration...   4
 !timer:                                 cpu_time=     0.020 walltime=     0.080

 orbital-state coupling will not be calculated this iteration.

 number of transformed integrals put on file:      1382
 number of records written:     1
 !timer: 2-e transformation              cpu_time=     0.000 walltime=     0.001

 Size of orbital-Hessian matrix B:                      300
 Total size of the state Hessian matrix M:              300
 Total size of HESSIAN-matrix for linear con            600

 !timer: mosrt1                          cpu_time=     0.000 walltime=     0.000
 !timer: mosrt2                          cpu_time=     0.000 walltime=     0.000
 !timer: mosort                          cpu_time=     0.000 walltime=     0.001
 !timer: hdiag(*) construction           cpu_time=     0.000 walltime=     0.000
 !timer: hmcft                           cpu_time=     0.000 walltime=     0.000

   3 trial vectors read from nvfile (unit= 63).
 ciiter=   4 noldhv=  3 noldv=  3
 !timer: hmc(*) diagonalization          cpu_time=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*       -7.9548668582       -8.7048668582        0.0000000000        0.0000100000
    2        -7.8679973812       -8.6179973812        0.0000000000        0.0000100000
    3        -7.7666914397       -8.5166914397        0.0000000000        0.0100000000
 !timer: hmcvec                          cpu_time=     0.000 walltime=     0.001
 !timer: rdft                            cpu_time=     0.000 walltime=     0.000
 !timer: mqva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfvd                            cpu_time=     0.000 walltime=     0.000
 !timer: bvaad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvaadf                          cpu_time=     0.000 walltime=     0.000
 !timer: mfad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqaa                            cpu_time=     0.000 walltime=     0.000
 !timer: mfaa                            cpu_time=     0.000 walltime=     0.000
 !timer: badad                           cpu_time=     0.000 walltime=     0.000
 !timer: badadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvava                           cpu_time=     0.000 walltime=     0.000
 !timer: bvavaf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvavd                           cpu_time=     0.000 walltime=     0.000
 !timer: bvavdf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvdadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdvd                           cpu_time=     0.000 walltime=     0.000
 !timer: hbcon                           cpu_time=     0.000 walltime=     0.001
 
  tol(10)=  0.000000000000000E+000  eshsci=  4.654854183897807E-003
 Total number of micro iterations:    5

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.80042806 pnorm= 0.0000E+00 rznorm= 8.2843E-06 rpnorm= 0.0000E+00 noldr=  5 nnewr=  5 nolds=  0 nnews=  0
 
 !timer: Direct Bxr time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  cpu_time=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: solvek total                    cpu_time=     0.000 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
    -4.998752

 qvv(*) eigenvalues. symmetry block  1
     0.330984    1.263512    1.564294    3.531571

 qvv(*) eigenvalues. symmetry block  2
     3.408271

 qvv(*) eigenvalues. symmetry block  3
     3.408272
 *** warning *** small active-orbital occupation. i=  1 nocc= 3.1088E-06
 *** warning *** small active-orbital occupation. i=  2 nocc= 8.2706E-04
 *** warning *** small active-orbital occupation. i=  1 nocc= 3.1092E-06
 *** warning *** small active-orbital occupation. i=  2 nocc= 8.2697E-04
 !timer: motran                          cpu_time=     0.000 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 cpu_time=     0.004 walltime=     0.003

 all mcscf convergence criteria are not satisfied.
 iter=    4 emc=   -7.9114321197 demc= 3.7357E-02 wnorm= 3.7239E-02 knorm= 5.9943E-01 apxde= 3.9051E-03    *not converged* 

               starting mcscf iteration...   5
 !timer:                                 cpu_time=     0.024 walltime=     0.084

 orbital-state coupling will be calculated this iteration.

 number of transformed integrals put on file:      1382
 number of records written:     1
 !timer: 2-e transformation              cpu_time=     0.000 walltime=     0.001

 Size of orbital-Hessian matrix B:                      300
 Size of the orbital-state Hessian matrix C:            552
 Total size of the state Hessian matrix M:              300
 Size of HESSIAN-matrix for quadratic conv.:           1152

 !timer: mosrt1                          cpu_time=     0.000 walltime=     0.000
 !timer: mosrt2                          cpu_time=     0.000 walltime=     0.000
 !timer: mosort                          cpu_time=     0.000 walltime=     0.001
 !timer: hdiag(*) construction           cpu_time=     0.000 walltime=     0.000
 !timer: hmcft                           cpu_time=     0.000 walltime=     0.000

   3 trial vectors read from nvfile (unit= 63).
 ciiter=   4 noldhv=  3 noldv=  3
 !timer: hmc(*) diagonalization          cpu_time=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*       -7.9679692397       -8.7179692397        0.0000000000        0.0000002404
    2        -7.8797284382       -8.6297284382        0.0000000000        0.0000002404
    3        -7.5863645847       -8.3363645847        0.0000000000        0.0100000000
 !timer: hmcvec                          cpu_time=     0.000 walltime=     0.001
 !timer: cadu                            cpu_time=     0.000 walltime=     0.000
 !timer: cvdu                            cpu_time=     0.000 walltime=     0.000
 !timer: rdft                            cpu_time=     0.000 walltime=     0.000
 !timer: mqva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfvd                            cpu_time=     0.000 walltime=     0.000
 !timer: bvaad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvaadf                          cpu_time=     0.000 walltime=     0.000
 !timer: mfad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqaa                            cpu_time=     0.000 walltime=     0.000
 !timer: mfaa                            cpu_time=     0.000 walltime=     0.000
 !timer: badad                           cpu_time=     0.000 walltime=     0.000
 !timer: badadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvava                           cpu_time=     0.000 walltime=     0.000
 !timer: bvavaf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvavd                           cpu_time=     0.000 walltime=     0.000
 !timer: bvavdf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvdadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdvd                           cpu_time=     0.000 walltime=     0.000
 !timer: hbcon                           cpu_time=     0.000 walltime=     0.001
 
  tol(10)=  0.000000000000000E+000  eshsci=  7.185268668639614E-003
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 Total number of micro iterations:    9

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.81181021 pnorm= 3.2070E-01 rznorm= 5.1370E-06 rpnorm= 6.0933E-06 noldr=  9 nnewr=  9 nolds=  8 nnews=  8
 
 !timer: Direct Bxr time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  cpu_time=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: solvek total                    cpu_time=     0.004 walltime=     0.001

 fdd(*) eigenvalues. symmetry block  1
    -4.999318

 qvv(*) eigenvalues. symmetry block  1
     0.300987    1.238207    1.314818    3.522197

 qvv(*) eigenvalues. symmetry block  2
     3.396306

 qvv(*) eigenvalues. symmetry block  3
     3.396316
 *** warning *** small active-orbital occupation. i=  1 nocc= 3.9091E-06
 *** warning *** small active-orbital occupation. i=  2 nocc= 6.7160E-04
 *** warning *** small active-orbital occupation. i=  1 nocc= 3.9093E-06
 *** warning *** small active-orbital occupation. i=  2 nocc= 6.7151E-04
 !timer: motran                          cpu_time=     0.000 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 cpu_time=     0.004 walltime=     0.004

 all mcscf convergence criteria are not satisfied.
 iter=    5 emc=   -7.9238488390 demc= 1.2417E-02 wnorm= 5.7482E-02 knorm= 5.5603E-01 apxde= 8.9770E-03    *not converged* 

               starting mcscf iteration...   6
 !timer:                                 cpu_time=     0.028 walltime=     0.088

 orbital-state coupling will be calculated this iteration.

 number of transformed integrals put on file:      1382
 number of records written:     1
 !timer: 2-e transformation              cpu_time=     0.000 walltime=     0.001

 Size of orbital-Hessian matrix B:                      300
 Size of the orbital-state Hessian matrix C:            552
 Total size of the state Hessian matrix M:              300
 Size of HESSIAN-matrix for quadratic conv.:           1152

 !timer: mosrt1                          cpu_time=     0.000 walltime=     0.000
 !timer: mosrt2                          cpu_time=     0.000 walltime=     0.000
 !timer: mosort                          cpu_time=     0.000 walltime=     0.001
 !timer: hdiag(*) construction           cpu_time=     0.000 walltime=     0.000
 !timer: hmcft                           cpu_time=     0.000 walltime=     0.000

   4 trial vectors read from nvfile (unit= 63).
 ciiter=   4 noldhv=  3 noldv=  3
 !timer: hmc(*) diagonalization          cpu_time=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*       -7.9792612477       -8.7292612477        0.0000000000        0.0000013647
    2        -7.8876594606       -8.6376594606        0.0000000000        0.0000013647
    3        -7.5220485758       -8.2720485758        0.0000000000        0.0100000000
 !timer: hmcvec                          cpu_time=     0.000 walltime=     0.001
 !timer: cadu                            cpu_time=     0.000 walltime=     0.000
 !timer: cvdu                            cpu_time=     0.000 walltime=     0.000
 !timer: rdft                            cpu_time=     0.000 walltime=     0.000
 !timer: mqva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfvd                            cpu_time=     0.000 walltime=     0.000
 !timer: bvaad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvaadf                          cpu_time=     0.000 walltime=     0.000
 !timer: mfad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqaa                            cpu_time=     0.000 walltime=     0.000
 !timer: mfaa                            cpu_time=     0.000 walltime=     0.000
 !timer: badad                           cpu_time=     0.000 walltime=     0.000
 !timer: badadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvava                           cpu_time=     0.000 walltime=     0.000
 !timer: bvavaf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvavd                           cpu_time=     0.000 walltime=     0.000
 !timer: bvavdf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvdadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdvd                           cpu_time=     0.000 walltime=     0.000
 !timer: hbcon                           cpu_time=     0.000 walltime=     0.001
 
  tol(10)=  0.000000000000000E+000  eshsci=  2.324945342520424E-003
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 Total number of micro iterations:    9

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0=-0.91806071 pnorm= 6.2124E-02 rznorm= 4.9314E-06 rpnorm= 4.8517E-06 noldr=  9 nnewr=  9 nolds=  8 nnews=  8
 
 !timer: Direct Bxr time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  cpu_time=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: solvek total                    cpu_time=     0.000 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
    -4.991209

 qvv(*) eigenvalues. symmetry block  1
     0.208224    1.214334    1.308093    3.538913

 qvv(*) eigenvalues. symmetry block  2
     3.408051

 qvv(*) eigenvalues. symmetry block  3
     3.408068
 *** warning *** small active-orbital occupation. i=  1 nocc= 1.8056E-06
 *** warning *** small active-orbital occupation. i=  2 nocc= 4.7568E-04
 *** warning *** small active-orbital occupation. i=  1 nocc= 1.8060E-06
 *** warning *** small active-orbital occupation. i=  2 nocc= 4.7559E-04
 !timer: motran                          cpu_time=     0.004 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 cpu_time=     0.004 walltime=     0.004

 all mcscf convergence criteria are not satisfied.
 iter=    6 emc=   -7.9334603542 demc= 9.6115E-03 wnorm= 1.8600E-02 knorm= 3.9568E-01 apxde= 1.1138E-03    *not converged* 

               starting mcscf iteration...   7
 !timer:                                 cpu_time=     0.032 walltime=     0.091

 orbital-state coupling will be calculated this iteration.

 number of transformed integrals put on file:      1382
 number of records written:     1
 !timer: 2-e transformation              cpu_time=     0.000 walltime=     0.001

 Size of orbital-Hessian matrix B:                      300
 Size of the orbital-state Hessian matrix C:            552
 Total size of the state Hessian matrix M:              300
 Size of HESSIAN-matrix for quadratic conv.:           1152

 !timer: mosrt1                          cpu_time=     0.000 walltime=     0.000
 !timer: mosrt2                          cpu_time=     0.000 walltime=     0.000
 !timer: mosort                          cpu_time=     0.000 walltime=     0.000
 !timer: hdiag(*) construction           cpu_time=     0.000 walltime=     0.000
 !timer: hmcft                           cpu_time=     0.000 walltime=     0.000

   4 trial vectors read from nvfile (unit= 63).
 ciiter=   4 noldhv=  3 noldv=  3
 !timer: hmc(*) diagonalization          cpu_time=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*       -7.9819227474       -8.7319227474        0.0000000000        0.0000000150
    2        -7.8879452714       -8.6379452714        0.0000000000        0.0000000150
    3        -7.5120280262       -8.2620280262        0.0000000000        0.0100000000
 !timer: hmcvec                          cpu_time=     0.000 walltime=     0.000
 !timer: cadu                            cpu_time=     0.000 walltime=     0.000
 !timer: cvdu                            cpu_time=     0.000 walltime=     0.000
 !timer: rdft                            cpu_time=     0.000 walltime=     0.000
 !timer: mqva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfvd                            cpu_time=     0.000 walltime=     0.000
 !timer: bvaad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvaadf                          cpu_time=     0.000 walltime=     0.000
 !timer: mfad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqaa                            cpu_time=     0.000 walltime=     0.000
 !timer: mfaa                            cpu_time=     0.000 walltime=     0.000
 !timer: badad                           cpu_time=     0.000 walltime=     0.000
 !timer: badadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvava                           cpu_time=     0.000 walltime=     0.000
 !timer: bvavaf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvavd                           cpu_time=     0.000 walltime=     0.000
 !timer: bvavdf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvdadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdvd                           cpu_time=     0.000 walltime=     0.000
 !timer: hbcon                           cpu_time=     0.000 walltime=     0.000
 
  tol(10)=  0.000000000000000E+000  eshsci=  5.100526795764204E-004
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 Total number of micro iterations:   10

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0=-0.57471615 pnorm= 1.2203E-01 rznorm= 9.2069E-07 rpnorm= 7.4226E-07 noldr= 10 nnewr= 10 nolds=  9 nnews=  9
 *** warning *** small z0.
 
 !timer: Direct Bxr time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  cpu_time=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: solvek total                    cpu_time=     0.000 walltime=     0.001

 fdd(*) eigenvalues. symmetry block  1
    -4.984136

 qvv(*) eigenvalues. symmetry block  1
     0.185482    1.223036    1.329361    3.538702

 qvv(*) eigenvalues. symmetry block  2
     3.142938

 qvv(*) eigenvalues. symmetry block  3
     3.143232
 *** warning *** small active-orbital occupation. i=  1 nocc= 2.5972E-06
 *** warning *** small active-orbital occupation. i=  2 nocc= 9.2381E-04
 *** warning *** small active-orbital occupation. i=  1 nocc= 2.5980E-06
 *** warning *** small active-orbital occupation. i=  2 nocc= 9.2353E-04
 !timer: motran                          cpu_time=     0.000 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 cpu_time=     0.000 walltime=     0.003

 all mcscf convergence criteria are not satisfied.
 iter=    7 emc=   -7.9349340094 demc= 1.4737E-03 wnorm= 4.0804E-03 knorm= 8.1233E-01 apxde= 1.2472E-03    *not converged* 

               starting mcscf iteration...   8
 !timer:                                 cpu_time=     0.032 walltime=     0.094

 orbital-state coupling will be calculated this iteration.

 number of transformed integrals put on file:      1382
 number of records written:     1
 !timer: 2-e transformation              cpu_time=     0.000 walltime=     0.000

 Size of orbital-Hessian matrix B:                      300
 Size of the orbital-state Hessian matrix C:            552
 Total size of the state Hessian matrix M:              300
 Size of HESSIAN-matrix for quadratic conv.:           1152

 !timer: mosrt1                          cpu_time=     0.000 walltime=     0.000
 !timer: mosrt2                          cpu_time=     0.000 walltime=     0.000
 !timer: mosort                          cpu_time=     0.000 walltime=     0.000
 !timer: hdiag(*) construction           cpu_time=     0.000 walltime=     0.000
 !timer: hmcft                           cpu_time=     0.000 walltime=     0.000

   4 trial vectors read from nvfile (unit= 63).
 ciiter=   4 noldhv=  3 noldv=  3
 !timer: hmc(*) diagonalization          cpu_time=     0.004 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*       -7.9840201696       -8.7340201696        0.0000000000        0.0000000000
    2        -7.8884860525       -8.6384860525        0.0000000000        0.0000000000
    3        -7.5096428358       -8.2596428358        0.0000000000        0.0100000000
 !timer: hmcvec                          cpu_time=     0.004 walltime=     0.000
 !timer: cadu                            cpu_time=     0.000 walltime=     0.000
 !timer: cvdu                            cpu_time=     0.000 walltime=     0.000
 !timer: rdft                            cpu_time=     0.000 walltime=     0.000
 !timer: mqva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfvd                            cpu_time=     0.000 walltime=     0.000
 !timer: bvaad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvaadf                          cpu_time=     0.000 walltime=     0.000
 !timer: mfad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqaa                            cpu_time=     0.000 walltime=     0.000
 !timer: mfaa                            cpu_time=     0.000 walltime=     0.000
 !timer: badad                           cpu_time=     0.000 walltime=     0.000
 !timer: badadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvava                           cpu_time=     0.000 walltime=     0.000
 !timer: bvavaf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvavd                           cpu_time=     0.000 walltime=     0.000
 !timer: bvavdf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvdadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdvd                           cpu_time=     0.000 walltime=     0.000
 !timer: hbcon                           cpu_time=     0.000 walltime=     0.000
 
  tol(10)=  0.000000000000000E+000  eshsci=  3.373407558474470E-004
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 Total number of micro iterations:   10

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0=-0.94518344 pnorm= 2.8537E-02 rznorm= 4.0226E-07 rpnorm= 5.4812E-07 noldr= 10 nnewr= 10 nolds=  8 nnews=  8
 
 !timer: Direct Bxr time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  cpu_time=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: solvek total                    cpu_time=     0.000 walltime=     0.001

 fdd(*) eigenvalues. symmetry block  1
    -4.984953

 qvv(*) eigenvalues. symmetry block  1
     0.183265    1.221767    1.329882    3.544000

 qvv(*) eigenvalues. symmetry block  2
     1.523793

 qvv(*) eigenvalues. symmetry block  3
     1.521569
 *** warning *** small active-orbital occupation. i=  1 nocc= 3.1489E-06
 *** warning *** small active-orbital occupation. i=  2 nocc= 9.5691E-04
 *** warning *** small active-orbital occupation. i=  1 nocc= 3.1494E-06
 *** warning *** small active-orbital occupation. i=  2 nocc= 9.5642E-04
 !timer: motran                          cpu_time=     0.000 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 cpu_time=     0.004 walltime=     0.003

 all mcscf convergence criteria are not satisfied.
 iter=    8 emc=   -7.9362531111 demc= 1.3191E-03 wnorm= 2.6987E-03 knorm= 3.2641E-01 apxde= 1.7181E-04    *not converged* 

               starting mcscf iteration...   9
 !timer:                                 cpu_time=     0.036 walltime=     0.097

 orbital-state coupling will be calculated this iteration.

 number of transformed integrals put on file:      1382
 number of records written:     1
 !timer: 2-e transformation              cpu_time=     0.000 walltime=     0.000

 Size of orbital-Hessian matrix B:                      300
 Size of the orbital-state Hessian matrix C:            552
 Total size of the state Hessian matrix M:              300
 Size of HESSIAN-matrix for quadratic conv.:           1152

 !timer: mosrt1                          cpu_time=     0.000 walltime=     0.000
 !timer: mosrt2                          cpu_time=     0.000 walltime=     0.000
 !timer: mosort                          cpu_time=     0.000 walltime=     0.000
 !timer: hdiag(*) construction           cpu_time=     0.000 walltime=     0.000
 !timer: hmcft                           cpu_time=     0.000 walltime=     0.000

   4 trial vectors read from nvfile (unit= 63).
 ciiter=   4 noldhv=  3 noldv=  3
 !timer: hmc(*) diagonalization          cpu_time=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*       -7.9842136603       -8.7342136603        0.0000000000        0.0000000000
    2        -7.8887057924       -8.6387057924        0.0000000000        0.0000000000
    3        -7.5079976521       -8.2579976521        0.0000000000        0.0100000000
 !timer: hmcvec                          cpu_time=     0.000 walltime=     0.000
 !timer: cadu                            cpu_time=     0.000 walltime=     0.000
 !timer: cvdu                            cpu_time=     0.000 walltime=     0.000
 !timer: rdft                            cpu_time=     0.000 walltime=     0.000
 !timer: mqva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfvd                            cpu_time=     0.000 walltime=     0.000
 !timer: bvaad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvaadf                          cpu_time=     0.000 walltime=     0.000
 !timer: mfad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqaa                            cpu_time=     0.000 walltime=     0.000
 !timer: mfaa                            cpu_time=     0.000 walltime=     0.000
 !timer: badad                           cpu_time=     0.000 walltime=     0.000
 !timer: badadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvava                           cpu_time=     0.000 walltime=     0.000
 !timer: bvavaf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvavd                           cpu_time=     0.000 walltime=     0.000
 !timer: bvavdf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvdadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdvd                           cpu_time=     0.000 walltime=     0.000
 !timer: hbcon                           cpu_time=     0.000 walltime=     0.000
 
  tol(10)=  0.000000000000000E+000  eshsci=  3.643910120487327E-005
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 Total number of micro iterations:   14

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.45013260 pnorm= 2.5636E-02 rznorm= 3.7571E-09 rpnorm= 1.4133E-09 noldr= 14 nnewr= 14 nolds= 13 nnews= 13
 *** warning *** small z0.
 
 !timer: Direct Bxr time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  cpu_time=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: solvek total                    cpu_time=     0.004 walltime=     0.001

 fdd(*) eigenvalues. symmetry block  1
    -4.985137

 qvv(*) eigenvalues. symmetry block  1
     0.181532    1.217556    1.328152    3.546357

 qvv(*) eigenvalues. symmetry block  2
     0.856158

 qvv(*) eigenvalues. symmetry block  3
     0.855709
 *** warning *** small active-orbital occupation. i=  1 nocc= 3.6953E-06
 *** warning *** small active-orbital occupation. i=  2 nocc= 8.0435E-04
 *** warning *** small active-orbital occupation. i=  1 nocc= 3.6934E-06
 *** warning *** small active-orbital occupation. i=  2 nocc= 8.0425E-04
 !timer: motran                          cpu_time=     0.000 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 cpu_time=     0.004 walltime=     0.003

 all mcscf convergence criteria are not satisfied.
 iter=    9 emc=   -7.9364597264 demc= 2.0662E-04 wnorm= 2.9151E-04 knorm= 8.9267E-01 apxde= 4.2549E-05    *not converged* 

               starting mcscf iteration...  10
 !timer:                                 cpu_time=     0.040 walltime=     0.100

 orbital-state coupling will be calculated this iteration.

 number of transformed integrals put on file:      1382
 number of records written:     1
 !timer: 2-e transformation              cpu_time=     0.000 walltime=     0.000

 Size of orbital-Hessian matrix B:                      300
 Size of the orbital-state Hessian matrix C:            552
 Total size of the state Hessian matrix M:              300
 Size of HESSIAN-matrix for quadratic conv.:           1152

 !timer: mosrt1                          cpu_time=     0.000 walltime=     0.000
 !timer: mosrt2                          cpu_time=     0.000 walltime=     0.000
 !timer: mosort                          cpu_time=     0.000 walltime=     0.000
 !timer: hdiag(*) construction           cpu_time=     0.000 walltime=     0.000
 !timer: hmcft                           cpu_time=     0.000 walltime=     0.000

   4 trial vectors read from nvfile (unit= 63).
 ciiter=   4 noldhv=  3 noldv=  3
 !timer: hmc(*) diagonalization          cpu_time=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*       -7.9842609475       -8.7342609475        0.0000000000        0.0000000000
    2        -7.8888210422       -8.6388210422        0.0000000000        0.0000000000
    3        -7.5100187670       -8.2600187670        0.0000000000        0.0100000000
 !timer: hmcvec                          cpu_time=     0.000 walltime=     0.000
 !timer: cadu                            cpu_time=     0.000 walltime=     0.000
 !timer: cvdu                            cpu_time=     0.000 walltime=     0.000
 !timer: rdft                            cpu_time=     0.000 walltime=     0.000
 !timer: mqva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfvd                            cpu_time=     0.000 walltime=     0.000
 !timer: bvaad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvaadf                          cpu_time=     0.000 walltime=     0.000
 !timer: mfad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqaa                            cpu_time=     0.000 walltime=     0.000
 !timer: mfaa                            cpu_time=     0.000 walltime=     0.000
 !timer: badad                           cpu_time=     0.000 walltime=     0.000
 !timer: badadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvava                           cpu_time=     0.000 walltime=     0.000
 !timer: bvavaf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvavd                           cpu_time=     0.000 walltime=     0.000
 !timer: bvavdf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvdadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdvd                           cpu_time=     0.000 walltime=     0.000
 !timer: hbcon                           cpu_time=     0.000 walltime=     0.000
 
  tol(10)=  0.000000000000000E+000  eshsci=  3.808273662112029E-005
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 Total number of micro iterations:   16

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0=-0.54127762 pnorm= 4.4530E-02 rznorm= 1.7274E-09 rpnorm= 7.9525E-10 noldr= 15 nnewr= 15 nolds= 15 nnews= 15
 *** warning *** small z0.
 
 !timer: Direct Bxr time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  cpu_time=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: solvek total                    cpu_time=     0.000 walltime=     0.002

 fdd(*) eigenvalues. symmetry block  1
    -4.984986

 qvv(*) eigenvalues. symmetry block  1
     0.181485    1.217224    1.328008    3.546483

 qvv(*) eigenvalues. symmetry block  2
     1.021881

 qvv(*) eigenvalues. symmetry block  3
     1.019383
 *** warning *** small active-orbital occupation. i=  1 nocc= 4.3891E-05
 *** warning *** small active-orbital occupation. i=  2 nocc= 7.8878E-04
 *** warning *** small active-orbital occupation. i=  1 nocc= 4.3205E-05
 *** warning *** small active-orbital occupation. i=  2 nocc= 7.8855E-04
 !timer: motran                          cpu_time=     0.004 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 cpu_time=     0.004 walltime=     0.004

 all mcscf convergence criteria are not satisfied.
 iter=   10 emc=   -7.9365409948 demc= 8.1268E-05 wnorm= 3.0466E-04 knorm= 8.4001E-01 apxde= 1.0510E-04    *not converged* 

               starting mcscf iteration...  11
 !timer:                                 cpu_time=     0.044 walltime=     0.103

 orbital-state coupling will be calculated this iteration.

 number of transformed integrals put on file:      1382
 number of records written:     1
 !timer: 2-e transformation              cpu_time=     0.000 walltime=     0.000

 Size of orbital-Hessian matrix B:                      300
 Size of the orbital-state Hessian matrix C:            552
 Total size of the state Hessian matrix M:              300
 Size of HESSIAN-matrix for quadratic conv.:           1152

 !timer: mosrt1                          cpu_time=     0.000 walltime=     0.000
 !timer: mosrt2                          cpu_time=     0.000 walltime=     0.000
 !timer: mosort                          cpu_time=     0.000 walltime=     0.000
 !timer: hdiag(*) construction           cpu_time=     0.000 walltime=     0.000
 !timer: hmcft                           cpu_time=     0.000 walltime=     0.000

   4 trial vectors read from nvfile (unit= 63).
 ciiter=   4 noldhv=  3 noldv=  3
 !timer: hmc(*) diagonalization          cpu_time=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*       -7.9844032381       -8.7344032381        0.0000000000        0.0000000000
    2        -7.8889489329       -8.6389489329        0.0000000000        0.0000000000
    3        -7.5221920461       -8.2721920461        0.0000000000        0.0100000000
 !timer: hmcvec                          cpu_time=     0.000 walltime=     0.000
 !timer: cadu                            cpu_time=     0.000 walltime=     0.000
 !timer: cvdu                            cpu_time=     0.000 walltime=     0.000
 !timer: rdft                            cpu_time=     0.000 walltime=     0.000
 !timer: mqva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfvd                            cpu_time=     0.000 walltime=     0.000
 !timer: bvaad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvaadf                          cpu_time=     0.000 walltime=     0.000
 !timer: mfad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqaa                            cpu_time=     0.000 walltime=     0.000
 !timer: mfaa                            cpu_time=     0.000 walltime=     0.000
 !timer: badad                           cpu_time=     0.000 walltime=     0.000
 !timer: badadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvava                           cpu_time=     0.000 walltime=     0.000
 !timer: bvavaf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvavd                           cpu_time=     0.000 walltime=     0.000
 !timer: bvavdf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvdadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdvd                           cpu_time=     0.000 walltime=     0.000
 !timer: hbcon                           cpu_time=     0.000 walltime=     0.000
 
  tol(10)=  0.000000000000000E+000  eshsci=  4.764213271220476E-005
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 Total number of micro iterations:   14

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0=-0.98366968 pnorm= 5.8845E-03 rznorm= 1.1209E-08 rpnorm= 1.3270E-08 noldr= 13 nnewr= 13 nolds= 13 nnews= 13
 
 !timer: Direct Bxr time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  cpu_time=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: solvek total                    cpu_time=     0.000 walltime=     0.001

 fdd(*) eigenvalues. symmetry block  1
    -4.984720

 qvv(*) eigenvalues. symmetry block  1
     0.181528    1.216308    1.327180    3.546090

 qvv(*) eigenvalues. symmetry block  2
     1.301512

 qvv(*) eigenvalues. symmetry block  3
     1.299688
 *** warning *** small active-orbital occupation. i=  1 nocc= 2.1083E-04
 *** warning *** small active-orbital occupation. i=  2 nocc= 8.9230E-04
 *** warning *** small active-orbital occupation. i=  1 nocc= 2.0891E-04
 *** warning *** small active-orbital occupation. i=  2 nocc= 8.9060E-04
 !timer: motran                          cpu_time=     0.000 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 cpu_time=     0.000 walltime=     0.003

 all mcscf convergence criteria are not satisfied.
 iter=   11 emc=   -7.9366760855 demc= 1.3509E-04 wnorm= 3.8114E-04 knorm= 1.7998E-01 apxde= 1.0564E-05    *not converged* 

               starting mcscf iteration...  12
 !timer:                                 cpu_time=     0.044 walltime=     0.106

 orbital-state coupling will be calculated this iteration.

 number of transformed integrals put on file:      1382
 number of records written:     1
 !timer: 2-e transformation              cpu_time=     0.000 walltime=     0.000

 Size of orbital-Hessian matrix B:                      300
 Size of the orbital-state Hessian matrix C:            552
 Total size of the state Hessian matrix M:              300
 Size of HESSIAN-matrix for quadratic conv.:           1152

 !timer: mosrt1                          cpu_time=     0.000 walltime=     0.000
 !timer: mosrt2                          cpu_time=     0.004 walltime=     0.000
 !timer: mosort                          cpu_time=     0.004 walltime=     0.000
 !timer: hdiag(*) construction           cpu_time=     0.000 walltime=     0.000
 !timer: hmcft                           cpu_time=     0.000 walltime=     0.000

   4 trial vectors read from nvfile (unit= 63).
 ciiter=   4 noldhv=  3 noldv=  3
 !timer: hmc(*) diagonalization          cpu_time=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*       -7.9844189791       -8.7344189791        0.0000000000        0.0000000000
    2        -7.8889555050       -8.6389555050        0.0000000000        0.0000000000
    3        -7.5246629019       -8.2746629019        0.0000000000        0.0100000000
 !timer: hmcvec                          cpu_time=     0.000 walltime=     0.000
 !timer: cadu                            cpu_time=     0.000 walltime=     0.000
 !timer: cvdu                            cpu_time=     0.000 walltime=     0.000
 !timer: rdft                            cpu_time=     0.000 walltime=     0.000
 !timer: mqva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfvd                            cpu_time=     0.000 walltime=     0.000
 !timer: bvaad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvaadf                          cpu_time=     0.000 walltime=     0.000
 !timer: mfad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqaa                            cpu_time=     0.000 walltime=     0.000
 !timer: mfaa                            cpu_time=     0.000 walltime=     0.000
 !timer: badad                           cpu_time=     0.000 walltime=     0.000
 !timer: badadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvava                           cpu_time=     0.000 walltime=     0.000
 !timer: bvavaf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvavd                           cpu_time=     0.000 walltime=     0.000
 !timer: bvavdf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvdadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdvd                           cpu_time=     0.000 walltime=     0.000
 !timer: hbcon                           cpu_time=     0.000 walltime=     0.000
 
  tol(10)=  0.000000000000000E+000  eshsci=  4.136077508862949E-006
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 Total number of micro iterations:   16

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0=-0.99994959 pnorm= 1.1299E-04 rznorm= 3.1218E-11 rpnorm= 1.8724E-11 noldr= 14 nnewr= 14 nolds= 15 nnews= 15
 
 !timer: Direct Bxr time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  cpu_time=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: solvek total                    cpu_time=     0.000 walltime=     0.001

 fdd(*) eigenvalues. symmetry block  1
    -4.984834

 qvv(*) eigenvalues. symmetry block  1
     0.181472    1.215721    1.326703    3.546161

 qvv(*) eigenvalues. symmetry block  2
     1.297949

 qvv(*) eigenvalues. symmetry block  3
     1.298106
 *** warning *** small active-orbital occupation. i=  1 nocc= 2.2324E-04
 *** warning *** small active-orbital occupation. i=  2 nocc= 8.9940E-04
 *** warning *** small active-orbital occupation. i=  1 nocc= 2.2328E-04
 *** warning *** small active-orbital occupation. i=  2 nocc= 8.9950E-04
 !timer: motran                          cpu_time=     0.000 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 cpu_time=     0.004 walltime=     0.003

 all mcscf convergence criteria are not satisfied.
 iter=   12 emc=   -7.9366872420 demc= 1.1157E-05 wnorm= 3.3089E-05 knorm= 1.0041E-02 apxde= 3.5940E-08    *not converged* 

               starting mcscf iteration...  13
 !timer:                                 cpu_time=     0.048 walltime=     0.110

 orbital-state coupling will be calculated this iteration.

 number of transformed integrals put on file:      1382
 number of records written:     1
 !timer: 2-e transformation              cpu_time=     0.000 walltime=     0.000

 Size of orbital-Hessian matrix B:                      300
 Size of the orbital-state Hessian matrix C:            552
 Total size of the state Hessian matrix M:              300
 Size of HESSIAN-matrix for quadratic conv.:           1152

 !timer: mosrt1                          cpu_time=     0.000 walltime=     0.000
 !timer: mosrt2                          cpu_time=     0.000 walltime=     0.000
 !timer: mosort                          cpu_time=     0.000 walltime=     0.000
 !timer: hdiag(*) construction           cpu_time=     0.000 walltime=     0.000
 !timer: hmcft                           cpu_time=     0.000 walltime=     0.000

   4 trial vectors read from nvfile (unit= 63).
 ciiter=   4 noldhv=  3 noldv=  3
 !timer: hmc(*) diagonalization          cpu_time=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*       -7.9844188992       -8.7344188992        0.0000000000        0.0000000000
    2        -7.8889556571       -8.6389556571        0.0000000000        0.0000000000
    3        -7.5247608446       -8.2747608446        0.0000000000        0.0100000000
 !timer: hmcvec                          cpu_time=     0.000 walltime=     0.000
 !timer: cadu                            cpu_time=     0.000 walltime=     0.000
 !timer: cvdu                            cpu_time=     0.000 walltime=     0.000
 !timer: rdft                            cpu_time=     0.000 walltime=     0.000
 !timer: mqva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfvd                            cpu_time=     0.000 walltime=     0.000
 !timer: bvaad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvaadf                          cpu_time=     0.000 walltime=     0.000
 !timer: mfad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqaa                            cpu_time=     0.000 walltime=     0.000
 !timer: mfaa                            cpu_time=     0.000 walltime=     0.000
 !timer: badad                           cpu_time=     0.000 walltime=     0.000
 !timer: badadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvava                           cpu_time=     0.000 walltime=     0.000
 !timer: bvavaf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvavd                           cpu_time=     0.000 walltime=     0.000
 !timer: bvavdf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvdadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdvd                           cpu_time=     0.000 walltime=     0.000
 !timer: hbcon                           cpu_time=     0.004 walltime=     0.000
 
  tol(10)=  0.000000000000000E+000  eshsci=  1.028681833594783E-008
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 *** psci convergence not reached ***
 Total number of micro iterations:   30

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0=-1.00000000 pnorm= 1.3429E-06 rznorm= 1.7723E-12 rpnorm= 7.4035E-08 noldr= 14 nnewr= 14 nolds= 22 nnews= 23
 
 !timer: Direct Bxr time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  cpu_time=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: solvek total                    cpu_time=     0.004 walltime=     0.005

 fdd(*) eigenvalues. symmetry block  1
    -4.984843

 qvv(*) eigenvalues. symmetry block  1
     0.181466    1.215768    1.326740    3.546149

 qvv(*) eigenvalues. symmetry block  2
     1.297781

 qvv(*) eigenvalues. symmetry block  3
     1.297782
 *** warning *** small active-orbital occupation. i=  1 nocc= 2.2335E-04
 *** warning *** small active-orbital occupation. i=  2 nocc= 8.9952E-04
 *** warning *** small active-orbital occupation. i=  1 nocc= 2.2335E-04
 *** warning *** small active-orbital occupation. i=  2 nocc= 8.9952E-04
 !timer: motran                          cpu_time=     0.000 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 cpu_time=     0.008 walltime=     0.007

 all mcscf convergence criteria are not satisfied.
 iter=   13 emc=   -7.9366872782 demc= 3.6158E-08 wnorm= 8.2295E-08 knorm= 7.6050E-05 apxde= 1.6382E-12    *not converged* 

               starting mcscf iteration...  14
 !timer:                                 cpu_time=     0.056 walltime=     0.117

 orbital-state coupling will be calculated this iteration.

 number of transformed integrals put on file:      1382
 number of records written:     1
 !timer: 2-e transformation              cpu_time=     0.000 walltime=     0.000

 Size of orbital-Hessian matrix B:                      300
 Size of the orbital-state Hessian matrix C:            552
 Total size of the state Hessian matrix M:              300
 Size of HESSIAN-matrix for quadratic conv.:           1152

 !timer: mosrt1                          cpu_time=     0.000 walltime=     0.000
 !timer: mosrt2                          cpu_time=     0.000 walltime=     0.000
 !timer: mosort                          cpu_time=     0.000 walltime=     0.000
 !timer: hdiag(*) construction           cpu_time=     0.000 walltime=     0.000
 !timer: hmcft                           cpu_time=     0.000 walltime=     0.000

   4 trial vectors read from nvfile (unit= 63).
 ciiter=   4 noldhv=  3 noldv=  3
 !timer: hmc(*) diagonalization          cpu_time=     0.000 walltime=     0.000

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*       -7.9844188989       -8.7344188989        0.0000000000        0.0000000000
    2        -7.8889556575       -8.6389556575        0.0000000000        0.0000000000
    3        -7.5247615462       -8.2747615462        0.0000000000        0.0100000000
 !timer: hmcvec                          cpu_time=     0.000 walltime=     0.000
 !timer: cadu                            cpu_time=     0.000 walltime=     0.000
 !timer: cvdu                            cpu_time=     0.000 walltime=     0.000
 !timer: rdft                            cpu_time=     0.000 walltime=     0.000
 !timer: mqva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfvd                            cpu_time=     0.000 walltime=     0.000
 !timer: bvaad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvaadf                          cpu_time=     0.000 walltime=     0.000
 !timer: mfad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqaa                            cpu_time=     0.000 walltime=     0.000
 !timer: mfaa                            cpu_time=     0.000 walltime=     0.000
 !timer: badad                           cpu_time=     0.000 walltime=     0.000
 !timer: badadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvava                           cpu_time=     0.000 walltime=     0.000
 !timer: bvavaf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvavd                           cpu_time=     0.000 walltime=     0.000
 !timer: bvavdf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvdadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdvd                           cpu_time=     0.000 walltime=     0.000
 !timer: hbcon                           cpu_time=     0.000 walltime=     0.000
 
  tol(10)=  0.000000000000000E+000  eshsci=  8.916324036190251E-013
 performing all-state projection
 Total number of micro iterations:    1

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 1.00000000 pnorm= 0.0000E+00 rznorm= 7.5564E-12 rpnorm= 9.5382E-13 noldr=  1 nnewr=  1 nolds=  0 nnews=  0
 
 !timer: Direct Bxr time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  cpu_time=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: solvek total                    cpu_time=     0.000 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
    -4.984843

 qvv(*) eigenvalues. symmetry block  1
     0.181466    1.215768    1.326740    3.546149

 qvv(*) eigenvalues. symmetry block  2
     1.297783

 qvv(*) eigenvalues. symmetry block  3
     1.297783
 *** warning *** small active-orbital occupation. i=  1 nocc= 2.2335E-04
 *** warning *** small active-orbital occupation. i=  2 nocc= 8.9952E-04
 *** warning *** small active-orbital occupation. i=  1 nocc= 2.2335E-04
 *** warning *** small active-orbital occupation. i=  2 nocc= 8.9952E-04
 !timer: motran                          cpu_time=     0.000 walltime=     0.000

 restrt: restart information saved on the restart file (unit= 17).
 !timer: mcscf iteration                 cpu_time=     0.000 walltime=     0.002

 all mcscf convergence criteria are satisfied.

 final mcscf convergence values:
 iter=   14 emc=   -7.9366872782 demc= 1.6351E-12 wnorm= 7.1331E-12 knorm= 2.8648E-12 apxde= 4.9365E-17    *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 weight 0.500000 total energy=       -7.984418899
   DRT #1 state # 2 weight 0.500000 total energy=       -7.888955657
   ------------------------------------------------------------



          mcscf orbitals of the final iteration,  A1  block   1

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
   1Lis       0.99627517    -0.07437783    -0.19198116    -0.12183383     0.20076545    -0.04110535     0.02175361    -0.03317768
   2Lis       0.02017703     0.15631380     0.63479614     0.54961052    -1.82648051     0.54147347    -0.44294207     0.31274190
   3Lis      -0.00913252    -0.00326612     0.28705818     0.01271910     1.94432631     0.12742035    -0.17048762    -0.11473736
   4Lipz     -0.00090315     0.06099524    -0.07527254     0.14711379     0.00332717    -0.76904470    -0.71758996     0.18504893
   5Lipz      0.00076906     0.20039824    -0.46425220     0.82423153     0.07713588     1.18306743    -0.35277626     0.14863543
   6H1s       0.00061057     0.46366396    -0.01215013    -0.79147149     0.01459734     0.67874877    -0.90659205     0.13747295
   7H1s      -0.00051606     0.41537033    -0.06101050    -0.14879018    -0.02487495    -1.51921166     1.54167786    -0.33064166
   8H1pz     -0.00072416    -0.01365713     0.00183189     0.00196431     0.00638069     0.04815730     0.09819023     1.01079885

          mcscf orbitals of the final iteration,  B1  block   2

               MO    9        MO   10        MO   11
   9Lipx      0.07039876     0.18950329     1.05850872
  10Lipx      0.52173409     0.74889372    -0.57862890
  11H1px      0.79450333    -0.60779565     0.07378257

          mcscf orbitals of the final iteration,  B2  block   3

               MO   12        MO   13        MO   14
  12Lipy      0.07039888     0.18950325     1.05850872
  13Lipy      0.52173454     0.74889341    -0.57862890
  14H1py      0.79450296    -0.60779613     0.07378257

          mcscf orbitals of the final iteration,  A2  block   4

          natural orbitals of the final iteration, block  1

               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8

  occ(*)=     2.00000000     1.44730096     0.49976881     0.05068450     0.00000000     0.00000000     0.00000000     0.00000000
 
   1Lis       0.99627517    -0.07437783    -0.19198115    -0.12183385     0.20076544    -0.04110533     0.02175370    -0.03317768
   2Lis       0.02017703     0.15631382     0.63479608     0.54961058    -1.82648043     0.54147288    -0.44294312     0.31274191
   3Lis      -0.00913252    -0.00326611     0.28705818     0.01271913     1.94432633     0.12741987    -0.17048770    -0.11473736
   4Lipz     -0.00090315     0.06099524    -0.07527255     0.14711378     0.00332715    -0.76904592    -0.71758864     0.18504893
   5Lipz      0.00076906     0.20039824    -0.46425229     0.82423148     0.07713602     1.18306682    -0.35277828     0.14863544
   6H1s       0.00061057     0.46366395    -0.01215006    -0.79147150     0.01459747     0.67874721    -0.90659321     0.13747296
   7H1s      -0.00051606     0.41537033    -0.06101049    -0.14879020    -0.02487520    -1.51920902     1.54168045    -0.33064168
   8H1pz     -0.00072416    -0.01365713     0.00183189     0.00196431     0.00638069     0.04815747     0.09819015     1.01079885

          natural orbitals of the final iteration, block  2

               MO    1        MO    2        MO    3

  occ(*)=     0.00089952     0.00022335     0.00000000
 
   9Lipx      0.07039944     0.18950304     1.05850872
  10Lipx      0.52173675     0.74889186    -0.57862890
  11H1px      0.79450117    -0.60779848     0.07378257

          natural orbitals of the final iteration, block  3

               MO    1        MO    2        MO    3

  occ(*)=     0.00089952     0.00022335     0.00000000
 
  12Lipy      0.07039944     0.18950304     1.05850872
  13Lipy      0.52173675     0.74889187    -0.57862890
  14H1py      0.79450117    -0.60779848     0.07378257
 d1(*), fmc(*), and qmc(*) written to the 1-particle density matrix file.
        110 d2(*) elements written to the 2-particle density matrix file.
 !timer: writing the mc density files reqcpu_time=     0.004 walltime=     0.032


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        A1  partial gross atomic populations
   ao class       1A1        2A1        3A1        4A1        5A1        6A1 
     1_ s       2.000061   0.096970   0.366974   0.008466   0.000000   0.000000
     1_ p       0.000002   0.210665   0.133988   0.023925   0.000000   0.000000
     1_ s      -0.000070   1.138849  -0.001182   0.018304   0.000000   0.000000
     1_ p       0.000008   0.000817  -0.000011  -0.000010   0.000000   0.000000
 
   ao class       7A1        8A1 

                        B1  partial gross atomic populations
   ao class       1B1        2B1        3B1 
     1_ p       0.000303   0.000149   0.000000
     1_ p       0.000597   0.000074   0.000000

                        B2  partial gross atomic populations
   ao class       1B2        2B2        3B2 
     1_ p       0.000303   0.000149   0.000000
     1_ p       0.000597   0.000074   0.000000


                        gross atomic populations
     ao            1_         1_
      s         2.472471   1.155901
      p         0.369483   0.002146
    total       2.841953   1.158047
 

 Total number of electrons:    4.00000000

 !timer: mcscf                           cpu_time=     0.064 walltime=     0.210
