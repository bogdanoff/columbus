
 DALTON: user specified work memory size used,
          environment variable WRKMEM = "13107200            "

 Work memory size (LMWORK) :    13107200 =  100.00 megabytes.

 Default basis set library used :
        /sphome/kedziora/dalton/basis/                              


    $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    $$$$$$$$$$$  DALTON - An electronic structure program  $$$$$$$$$$$
    $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

               This is output from DALTON (beta-version 0.9) 

                          Principal authors:

            Trygve Helgaker,     University of Oslo,        Norway 
            Hans Joergen Jensen, University of Odense,      Denmark
            Poul Joergensen,     University of Aarhus,      Denmark
            Henrik Koch,         University of Aarhus,      Denmark
            Jeppe Olsen,         University of Lund,        Sweden 
            Hans Aagren,         University of Linkoeping,  Sweden 

                          Contributors:

            Torgeir Andersen,    University of Oslo,        Norway 
            Keld L. Bak,         University of Copenhagen,  Denmark
            Vebjoern Bakken,     University of Oslo,        Norway 
            Ove Christiansen,    University of Aarhus,      Denmark
            Paal Dahle,          University of Oslo,        Norway 
            Erik K. Dalskov,     University of Odense,      Denmark
            Thomas Enevoldsen,   University of Odense,      Denmark
            Asger Halkier,       University of Aarhus,      Denmark
            Hanne Heiberg,       University of Oslo,        Norway 
            Dan Jonsson,         University of Linkoeping,  Sweden 
            Sheela Kirpekar,     University of Odense,      Denmark
            Rika Kobayashi,      University of Aarhus,      Denmark
            Alfredo S. de Meras, Valencia University,       Spain  
            Kurt Mikkelsen,      University of Aarhus,      Denmark
            Patrick Norman,      University of Linkoeping,  Sweden 
            Martin J. Packer,    University of Sheffield,   UK     
            Kenneth Ruud,        University of Oslo,        Norway 
            Trond Saue,          University of Oslo,        Norway 
            Peter Taylor,        San Diego Superc. Center,  USA    
            Olav Vahtras,        University of Linkoeping,  Sweden

                                             Release Date:  August 1996
------------------------------------------------------------------------


      
     NOTE:
      
     This is an experimental code for the evaluation of molecular
     properties using (MC)SCF/CC wave functions. The authors accept
      no responsibility for the performance of the code or for the
     correctness of the results.
      
     The code (in whole or part) is not to be reproduced for further
     distribution without the written permission of T. Helgaker,
     H. J. Aa. Jensen or P. Taylor.
      
     If results obtained with this code are published, an
     appropriate citation would be:
      
     T. Helgaker, H. J. Aa. Jensen, P.Joergensen, H. Koch,
     J. Olsen, H. Aagren, T. Andersen, K. L. Bak, V. Bakken,
     O. Christiansen, P. Dahle, E. K. Dalskov, T. Enevoldsen,
     A. Halkier, H. Heiberg, D. Jonsson, S. Kirpekar, R. Kobayashi,
     A. S. de Meras, K. V. Mikkelsen, P. Norman, M. J. Packer,
     K. Ruud, T.Saue, P. R. Taylor, and O. Vahtras:
     DALTON, an electronic structure program"



     ******************************************
     **    PROGRAM:              DALTON      **
     **    PROGRAM VERSION:      5.4.0.0     **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************



 <<<<<<<<<< OUTPUT FROM GENERAL INPUT PROCESSING >>>>>>>>>>




 Default print level:        0

    Integral sections will be executed
    Starting in Integral Section -



 *************************************************************************
 ****************** Output from HERMIT input processing ******************
 *************************************************************************



 Default print level:        2


 Calculation of one- and two-electron Hamiltonian integrals.


 The following one-electron property integrals are calculated:

          - overlap integrals
          - Cartesian multipole moment integrals of orders 4 and lower
          - electronic angular momentum around the origin


 Changes of defaults for READIN:
 -------------------------------


 Maximum number of primitives per integral block :    7



 *************************************************************************
 ****************** Output from READIN input processing ******************
 *************************************************************************



  Title Cards
  -----------

                                                                          
                                                                          


  Symmetry Operations
  -------------------

  Symmetry operations: 2



                      SYMGRP:Point group information
                      ------------------------------

Point group: C2v

   * The point group was generated by:

      Reflection in the yz-plane
      Reflection in the xz-plane

   * Group multiplication table

        |  E   C2z  Oxz  Oyz
   -----+--------------------
     E  |  E 
    C2z | C2z   E 
    Oxz | Oxz  Oyz   E 
    Oyz | Oyz  Oxz  C2z   E 

   * Character table

        |  E   C2z  Oxz  Oyz
   -----+--------------------
    A1  |   1    1    1    1
    B1  |   1   -1    1   -1
    B2  |   1   -1   -1    1
    A2  |   1    1   -1   -1

   * Direct product table

        | A1   B1   B2   A2 
   -----+--------------------
    A1  | A1 
    B1  | B1   A1 
    B2  | B2   A2   A1 
    A2  | A2   B2   B1   A1 


  Atoms and basis sets
  --------------------

  Number of atom types:     2
  Total number of atoms:    2

  label    atoms   charge   prim    cont     basis   
  ----------------------------------------------------------------------
  Li 1        1       3      13       9      [7s2p|3s2p]                            
  H  1        1       1       7       5      [4s1p|2s1p]                            
  ----------------------------------------------------------------------
  ----------------------------------------------------------------------
  total:      2       4      20      14

  Threshold for integrals:  1.00D-15


  Cartesian Coordinates
  ---------------------

  Total number of coordinates:  6


   1   Li 1     x      0.0000000000
   2            y      0.0000000000
   3            z      0.0000000000

   4   H  1     x      0.0000000000
   5            y      0.0000000000
   6            z      4.0000000000



  Symmetry Coordinates
  --------------------

  Number of coordinates in each symmetry:   2  2  2  0


  Symmetry 1

   1   Li 1  z    3
   2   H  1  z    6


  Symmetry 2

   3   Li 1  x    1
   4   H  1  x    4


  Symmetry 3

   5   Li 1  y    2
   6   H  1  y    5


   Interatomic separations (in Angstroms):
   ---------------------------------------

            Li 1        H  1

   Li 1    0.000000
   H  1    2.116708    0.000000




  Bond distances (angstroms):
  ---------------------------

                  atom 1     atom 2                           distance
                  ------     ------                           --------
  bond distance:    H  1       Li 1                           2.116708


  Nuclear repulsion energy :    0.750000000000


  Orbital exponents and contraction coefficients
  ----------------------------------------------


  Li 1   1s    1      266.277855    0.0065  0.0000  0.0000
   seg. cont.  2       40.069783    0.0477  0.0000  0.0000
               3        9.055994    0.2027  0.0000  0.0000
               4        2.450301    0.4861  0.0000  0.0000
               5        0.722096    0.4363  0.0000  0.0000
               6        0.052811    0.0000  1.0000  0.0000
               7        0.020961    0.0000  0.0000  1.0000

  Li 1   2px   8        0.400000    1.0000  0.0000
   seg. cont.  9        0.060000    0.0000  1.0000

  Li 1   2py  10        0.400000    1.0000  0.0000
   seg. cont. 11        0.060000    0.0000  1.0000

  Li 1   2pz  12        0.400000    1.0000  0.0000
   seg. cont. 13        0.060000    0.0000  1.0000

  H  1   1s   14       13.010701    0.0197  0.0000
   seg. cont. 15        1.962257    0.1380  0.0000
              16        0.444538    0.4783  0.0000
              17        0.121950    0.0000  1.0000

  H  1   2px  18        0.800000    1.0000

  H  1   2py  19        0.800000    1.0000

  H  1   2pz  20        0.800000    1.0000


  Contracted Orbitals
  -------------------

   1  Li 1    1s       1     2     3     4     5
   2  Li 1    1s       6
   3  Li 1    1s       7
   4  Li 1    2px      8
   5  Li 1    2py     10
   6  Li 1    2pz     12
   7  Li 1    2px      9
   8  Li 1    2py     11
   9  Li 1    2pz     13
  10  H  1    1s      14    15    16
  11  H  1    1s      17
  12  H  1    2px     18
  13  H  1    2py     19
  14  H  1    2pz     20




  Symmetry Orbitals
  -----------------

  Number of orbitals in each symmetry:         8  3  3  0


  Symmetry  A1 ( 1)

    1     Li 1     1s         1
    2     Li 1     1s         2
    3     Li 1     1s         3
    4     Li 1     2pz        6
    5     Li 1     2pz        9
    6     H  1     1s        10
    7     H  1     1s        11
    8     H  1     2pz       14


  Symmetry  B1 ( 2)

    9     Li 1     2px        4
   10     Li 1     2px        7
   11     H  1     2px       12


  Symmetry  B2 ( 3)

   12     Li 1     2py        5
   13     Li 1     2py        8
   14     H  1     2py       13


  No orbitals in symmetry  A2 ( 4)

  Symmetries of electric field:  B1 (2)  B2 (3)  A1 (1)

  Symmetries of magnetic field:  B2 (3)  B1 (2)  A2 (4)


 Copy of input to READIN
 -----------------------

INTGRL                                                                          
                                                                                
                                                                                
s   2    2X   Y      0.10D-14                                                   
       3.0    1    2    3    2                                                  
Li 1   0.000000000000000   0.000000000000000   0.000000000000000       *        
H   5   1                                                                       
        266.27785516         0.00649202                                         
         40.06978345         0.04774786                                         
          9.05599444         0.20268796                                         
          2.45030091         0.48606575                                         
          0.72209572         0.43626978                                         
H   1   1                                                                       
          0.05281088         1.00000000                                         
H   1   1                                                                       
          0.02096095         1.00000000                                         
H   1   1                                                                       
          0.40000000         1.00000000                                         
H   1   1                                                                       
          0.06000000         1.00000000                                         
       1.0    1    2    2    1                                                  
H  1   0.000000000000000   0.000000000000000   4.000000000000000       *        
H   3   1                                                                       
         13.01070100         0.01968216                                         
          1.96225720         0.13796524                                         
          0.44453796         0.47831935                                         
H   1   1                                                                       
          0.12194962         1.00000000                                         
H   1   1                                                                       
          0.80000000         1.00000000                                         




 ************************************************************************
 ************************** Output from HERONE **************************
 ************************************************************************



    40 atomic overlap integrals written in   1 buffers.
 Percentage non-zero integrals:  38.10


    48 one-el. Hamil. integrals written in   1 buffers.
 Percentage non-zero integrals:  45.71


    40 kinetic energy integrals written in   1 buffers.
 Percentage non-zero integrals:  38.10


















 ************************************************************************
 ************************** Output from TWOINT **************************
 ************************************************************************


 Number of two-electron integrals written:      1667 (30.0%)
 Kilobytes written:                               44




 >>>> Total CPU  time used in HERMIT:   0.02 seconds
 >>>> Total wall time used in HERMIT:   0.00 seconds

- End of Integral Section
