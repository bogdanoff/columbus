
======= Computing sorting integral file structure =========

 nmpsy=                     8                     3                     3
                     0
                    -----z----- -----y----- -----x----- -----w----- ---total---

                CSFs        12           0           0           0          12
      internal walks        12           0           0           0          12
valid internal walks        12           0           0           0          12
 found l2rec=                  4096 n2max=                  2730

 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg=  12926634
 minimum size of srtscr:                 32767  WP(                      1 
  records)
 maximum size of srtscr:                163835  WP(                      3 
  records)

sorted 4-external integrals:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 32767 have been written.
 wstat,xstat=  100.00  100.00

sorted 3-external integrals:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 32767 have been written.
 wstat,xstat=  100.00  100.00
 Orig.  diagonal integrals:  1electron:        14
                             0ext.    :        72
                             2ext.    :        96
                             4ext.    :        42


 Orig. off-diag. integrals:  4ext.    :       138
                             3ext.    :       420
                             2ext.    :       780
                             1ext.    :       708
                             0ext.    :       294
                             2ext. SO :         0
                             1ext. SO :         0
                             0ext. SO :         0
                             1electron:        12


 Sorted integrals            3ext.  w :       320 x :       220
                             4ext.  w :        78 x :        27


1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      -varseg3    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 compressed index vector length=                     2
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 0,
  GSET = 0,
   DAVCOR =10,
  NCOREL = 4
  NROOT = 2
  IVMODE = 3
  NBKITR = 1
  NVBKMN = 2
  RTOLBK = 1e-5,1e-5,
  NITER = 60
  NVCIMN = 4
  RTOLCI = 1e-5,1e-5,
  NVCIMX = 7
  NVRFMX = 7
  NVBKMX = 7
  IDEN  = 1
  CSFPRN = 10,
 /&end
 ------------------------------------------------------------------------
 Ame=                     0 ivmode,nbkitr=                     3
                     1 ntype=                     0
 me=                     0 ivmode,nbkitr=                     3
                     1 ntype=                     0
 ntotseg(*)=                     4                     4                     4
                     4                     4                     4
 iexplseg(*)=                     0                     0                     0
                     0                     0                     0
 resetting explseg from                      0  to                      0
 USING SEGMENTS OF EQUAL SIZE

****************  list of control variables  ****************
 lvlprt =    0      nroot  =    2      noldv  =   0      noldhv =   0
 nunitv =    2      nbkitr =    1      niter  =  60      davcor =  10
 csfprn =   10      ivmode =    3      istrt  =   0      vout   =   0
 iortls =    0      nvbkmx =    7      ibktv  =  -1      ibkthv =  -1
 nvcimx =    7      icitv  =   -1      icithv =  -1      frcsub =   0
 nvbkmn =    2      nvcimn =    4      maxseg =   4      nrfitr =  30
 ncorel =    4      nvrfmx =    7      nvrfmn =   4      iden   =   1
 itran  =    0      froot  =    0      rtmode =   0      ncouple=   1
 skipso =    0      dalton2=    0      molcas =   0      finalv =   0
 finalw =    0      cosmocalc=   0    with_tsklst=   0
 nsegwx =    1     1     1     1
 nseg0x =    1     1     1     1
 nseg1x =    1     1     1     1
 nseg2x =    1     1     1     1
 nseg3x =    1     1     1     1
 nseg4x =    1     1     1     1
 no0ex  =      0    no1ex  =      0    no2ex  =     0    no3ex  =     0
 no4ex  =      0    nodiag =      0
 cdg4ex =    1      c3ex1ex=    1      c2ex0ex=   1
 fileloc=    0     0     0     0     0     0     0

 ctol   = 0.010000    lrtshift=1.000000    smalld =0.001000


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-05    1.000E-05
    2        1.000E-05    1.000E-05
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core           13107199 DP per process

********** Integral sort section *************


 workspace allocation information: lencor=  13107199

 echo of the input for program cisrt:
 ------------------------------------------------------------------------
  &input
  maxbl3=60000
  maxbl4=60000
  &end
 ------------------------------------------------------------------------
 
 ( 6) listing file:                    ciudgls             
 ( 5) input file:                      cisrtin   
 (17) cidrt file:                      cidrtfl             
 (11) transformed integrals file:      moints    
 (12) diagonal integral file:          diagint             
 (13) off-diagonal integral file:      ofdgint             
 (31) 4-external w integrals file:     fil4w               
 (32) 4-external x integrals file:     fil4x               
 (33) 3-external w integrals file:     fil3w               
 (34) 3-external x integrals file:     fil3x               
 (21) scratch da sorting file:         srtscr              
 (12) 2-e integral file [fsplit=2]:    moints2   

 input integral file header information:
 Hermit Integral Program : SIFS version  visnu             16:35:23.428 21-May-09
  cidrt_title DRT#1                                                              
  title                                                                          
 mofmt: formatted orbitals label=morbl   visnu             16:35:24.225 21-May-09
 SIFS file created by program tran.      visnu             16:35:24.709 21-May-09

 input energy(*) values:
 energy( 1)=  7.500000000000E-01, ietype=   -1,    core energy of type: Nuc.Rep.

 total core energy =   7.500000000000E-01

 nsym = 4 nmot=  14

 symmetry  =    1    2    3    4
 slabel(*) =  A1   B1   B2   A2 
 nmpsy(*)  =    8    3    3    0

 info(*) =          1      4096      3272      4096      2730

 orbital labels, i:molab(i)=
   1:tout:001   2:tout:002   3:tout:003   4:tout:004   5:tout:005   6:tout:006   7:tout:007   8:tout:008   9:tout:009  10:tout:010
  11:tout:011  12:tout:012  13:tout:013  14:tout:014

 input parameters:
 prnopt=  0
 ldamin=    4095 ldamax=   32767 ldainc=      64
 maxbuf=   32767 maxbl3=   60000 maxbl4=   60000 intmxo=     766

 drt information:
  cidrt_title DRT#1                                                              
 nmotd =  14 nfctd =   0 nfvtc =   0 nmot  =  14
 nlevel =  14 niot  =   8 lowinl=   7
 orbital-to-level map(*)
    7   8   9  10   1   2   3   4  11  12   5  13  14   6
 compressed map(*)
    7   8   9  10   1   2   3   4  11  12   5  13  14   6
 levsym(*)
    1   1   1   1   2   3   1   1   1   1   2   2   3   3
 repartitioning mu(*)=
   0.  0.  0.  0.  0.  0.  0.  0.

 indxdg: diagonal integral statistics.
 total number of integrals contributing to diagonal matrix elements:       210
 number with all external indices:        42
 number with half external - half internal indices:        96
 number with all internal indices:        72

 indxof: off-diagonal integral statistics.
    4-external integrals: num=        138 strt=          1
    3-external integrals: num=        420 strt=        139
    2-external integrals: num=        780 strt=        559
    1-external integrals: num=        708 strt=       1339
    0-external integrals: num=        294 strt=       2047

 total number of off-diagonal integrals:        2340



 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg=  12926946
 !timer: setup required                  cpu_time=     0.000 walltime=     0.001
 pro2e        1     106     211     316     421     457     493     598   44286   87974
   120741  124837  127567  138486

 pro2e:      1821 integrals read in     1 records.
 pro1e        1     106     211     316     421     457     493     598   44286   87974
   120741  124837  127567  138486
 pro1e: eref =    0.000000000000000E+00
 total size of srtscr:                     3  records of                  32767 
 WP =                786408 Bytes
 !timer: first half-sort required        cpu_time=     0.004 walltime=     0.002
 putdg        1     106     211     316    1082   33849   55694     598   44286   87974
   120741  124837  127567  138486

 putf:       4 buffers of length     766 written to file 12
 diagonal integral file completed.

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 32767 have been written.
 wstat,xstat=  100.00  100.00
 prep4e:     3 blocks of linear combinations of 4-external integrals processed.
 number of sorted 4-external integrals        105
 number of original 4-external integrals      138


 putf34: external integral file complete. nfilw=    31 nfilx=    32 nrecw=     1 nrecx=     1 lbufp= 32767

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 32767 have been written.
 wstat,xstat=  100.00  100.00
 prep3e:    18 blocks of linear combinations of 3-external integrals processed.
 number of sorted 3-external integrals        540
 number of original 3-external integrals      420


 putf34: external integral file complete. nfilw=    33 nfilx=    34 nrecw=     1 nrecx=     1 lbufp= 32767

 putf:       5 buffers of length     766 written to file 13
 off-diagonal files sort completed.
 !timer: second half-sort required       cpu_time=     0.004 walltime=     0.003
 !timer: cisrt complete                  cpu_time=     0.008 walltime=     0.006
 executing brd_struct for cisrtinfo
cisrtinfo:
bufszi   766
 diagfile 4ext:      42 2ext:      96 0ext:      72
 fil4w,fil4x  :     138 fil3w,fil3x :     420
 ofdgint  2ext:     780 1ext:     708 0ext:     294so0ext:       0so1ext:       0so2ext:       0
buffer minbl4      33 minbl3      33 maxbl2      36nbas:   4   1   1   0   0   0   0   0 maxbuf 32767
 CIUDG version 5.9.7 ( 5-Oct-2004)

 workspace allocation information: lcore=  13107199

 core energy values from the integral file:
 energy( 1)=  7.500000000000E-01, ietype=   -1,    core energy of type: Nuc.Rep.

 total core repulsion energy =  7.500000000000E-01
 nmot  =    14 niot  =     8 nfct  =     0 nfvt  =     0
 nrow  =    26 nsym  =     4 ssym  =     1 lenbuf=  1600
 nwalk,xbar:         12       12        0        0        0
 nvalwt,nvalw:       12       12        0        0        0
 ncsft:              12
 total number of valid internal walks:      12
 nvalz,nvaly,nvalx,nvalw =       12       0       0       0

 cisrt info file parameters:
 file number  12 blocksize    766
 mxbld    766
 nd4ext,nd2ext,nd0ext    42    96    72
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int      138      420      780      708      294        0        0        0
 minbl4,minbl3,maxbl2    33    33    36
 maxbuf 32767
 number of external orbitals per symmetry block:   4   1   1   0
 nmsym   4 number of internal orbitals   8
 executing brd_struct for drt
 executing brd_struct for orbinf
 executing brd_struct for momap
 calcthrxt: niot,maxw1=                     8                     1
 block size     0
 pthz,pthy,pthx,pthw:    12     0     0     0 total internal walks:      12
 maxlp3,n2lp,n1lp,n0lp     1     0     0     0
 orbsym(*)= 1 1 1 1 2 2 3 3

 setref:       12 references kept,
                0 references were marked as invalid, out of
               12 total.
 limcnvrt: found                    12  valid internal walksout of 
                    12  walks (skipping trailing invalids)
  ... adding                     12  segmentation marks segtype=
                     1

 norbsm(*)=   8   3   3   0   0   0   0   0
 noxt(*)=   4   1   1   0   0   0   0   0
 intorb=   8 nmsym=   4 lowdoc=   5
 ivout(*)=   6   7   8  11  14   1   2   3   4   9  10  12  13   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
   0   0   0   0   0   0   0   0   0   0
 initvdsk=  0.000000000000000E+000  seconds.
 ---------memory usage in DP -----------------
 < n-ex core usage >
     routines:
    fourex            66328
    threx             32774
    twoex               978
    onex                815
    allin               766
    diagon             1297
               =======
   maximum            66328
 
  __ static summary __ 
   reflst                12
   hrfspc                12
               -------
   static->              12
 
  __ core required  __ 
   totstc                12
   max n-ex           66328
               -------
   totnec->           66340
 
  __ core available __ 
   totspc          13107199
   totnec -           66340
               -------
   totvec->        13040859

 number of external paths / symmetry
 vertex x       6       4       4       1
 vertex w      12       4       4       1
segment: free space=    13040859
 reducing frespc by                   316 to               13040543 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1          12|        12|         0|        12|         0|         1|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=           4DP  conft+indsym=          48DP  drtbuffer=         264 DP

dimension of the ci-matrix ->>>        12

 executing brd_struct for civct
 gentasklist: ntask=                     2
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS   
----------------------------------------------------------------------------------------------------
     1  1   1     1      allint zz    OX  1 1      12      12         12         12      12      12
     2  1   1    75      dg-024ext z  4X  1 1      12      12         12         12      12      12
----------------------------------------------------------------------------------------------------
 DIAGEL COUNTS: DG0X,DG2X,DG4X=                    51                     0
                     0
 diagonal elements written to file   4(cihdiag             )

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      2 vectors will be written to unit 11 beginning with logical record   1

            2 vectors will be created
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:          51 2x:           0 4x:           0
All internal counts: zz :          82 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:          81    task #     2:          37    task #
 reference space has dimension      12

    root           eigenvalues
    ----           ------------
       1          -7.9844188989
       2          -7.8889556575

 strefv generated    2 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt=                    12

         vector  1 from unit 11 written to unit 49 filename cirefv              
 ufvoutnew: ... writing  recamt=                    12

         vector  2 from unit 11 written to unit 49 filename cirefv              

 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=    12)...
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:                12
 number of initial trial vectors:                         2
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               7
 number of roots to converge:                             2
 number of iterations:                                    1
 residual norm convergence criteria:               0.000010  0.000010

          starting bk iteration   1

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:          51 2x:           0 4x:           0
All internal counts: zz :          82 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:          81    task #     2:          37    task #
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:          51 2x:           0 4x:           0
All internal counts: zz :          82 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:          81    task #     2:          37    task #

          calcsovref: tciref block   1

              civs   1       civs   2
 refs   1    1.00000       1.539025E-16
 refs   2    0.00000        1.00000    

          calcsovref: scrb block   1

              civs   1       civs   2
 civs   1    1.00000      -1.231654E-16
 civs   2    0.00000        1.00000    
 eci,repnuc=  -8.63895565748963       0.750000000000000     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1     -7.9844188989  1.7764E-15  3.8544E-29  3.1012E-15  1.0000E-05
 mr-sdci #  1  2     -7.8889556575  1.7764E-15  0.0000E+00  4.2304E-15  1.0000E-05
 
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1    1    0     0.00     0.00     0.00     0.00         0.    0.0001
    2   75    0     0.00     0.00     0.00     0.00         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0000s 
time spent in multnx:                   0.0000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.0000s 

 mr-sdci  convergence criteria satisfied after  1 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1     -7.9844188989  1.7764E-15  3.8544E-29  3.1012E-15  1.0000E-05
 mr-sdci #  1  2     -7.8889556575  1.7764E-15  0.0000E+00  4.2304E-15  1.0000E-05
 
 expansion vectors are not transformed.
 matrix-vector products are not transformed.

    3 expansion eigenvectors written to unit nvfile (= 11)
    2 matrix-vector products written to unit nhvfil (= 10)

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:                12
 number of initial trial vectors:                         3
 number of initial matrix-vector products:                2
 maximum dimension of the subspace vectors:               7
 number of roots to converge:                             2
 number of iterations:                                   60
 residual norm convergence criteria:               0.000010  0.000010

          starting ci iteration   1

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:          51 2x:           0 4x:           0
All internal counts: zz :          82 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:          81    task #     2:          37    task #

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3
 refs   1    1.00000       1.539025E-16  -2.186739E-14
 refs   2    0.00000        1.00000       4.869272E-15

          calcsovref: scrb block   1

              civs   1       civs   2       civs   3
 civs   1    1.00000       1.231654E-16    4.02928    
 civs   2    0.00000       -1.00000      -0.897210    
 civs   3    0.00000        0.00000       1.842596E+14
 eci,repnuc=  -8.63895565748963       0.750000000000000     

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1     -7.9844188989  0.0000E+00  3.8544E-29  3.1012E-15  1.0000E-05
 mr-sdci #  1  2     -7.8889556575  0.0000E+00  0.0000E+00  4.2304E-15  1.0000E-05
 mr-sdci #  1  3     -7.2908843876  8.0409E+00  0.0000E+00  4.2127E-01  1.0000E-04
 
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1    1    0     0.01     0.00     0.00     0.01         0.    0.0001
    2   75    0     0.00     0.00     0.00     0.00         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0100s 
time spent in multnx:                   0.0100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.0100s 

 mr-sdci  convergence criteria satisfied after  1 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1     -7.9844188989  0.0000E+00  3.8544E-29  3.1012E-15  1.0000E-05
 mr-sdci #  1  2     -7.8889556575  0.0000E+00  0.0000E+00  4.2304E-15  1.0000E-05
 mr-sdci #  1  3     -7.2908843876  8.0409E+00  0.0000E+00  4.2127E-01  1.0000E-04

####################CIUDGINFO####################

   ci vector at position   1 energy=   -7.984418898854
   ci vector at position   2 energy=   -7.888955657490

################END OF CIUDGINFO################

 
    2 of the   4 expansion vectors are transformed.
    2 of the   3 matrix-vector products are transformed.

    2 expansion eigenvectors written to unit nvfile (= 11)
    2 matrix-vector products written to unit nhvfil (= 10)
 maximum overlap with reference                      1 (overlap=
   1.00000000000000      )

information on vector: 1from unit 11 written to unit 48filename civout              
 maximum overlap with reference                      2 (overlap=
   1.00000000000000      )

information on vector: 2from unit 11 written to unit 48filename civout              


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =        -7.9844188989

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7    8

                                          orbital     1    2    3    4    9   10   12   13

                                         symmetry   a1   a1   a1   a1   b1   b1   b2   b2 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1 -0.912866                        +-   +-                               
 z*  1  1       2 -0.346913                        +-   +     -                          
 z*  1  1       3 -0.152598                        +-   +          -                     
 z*  1  1       5  0.081373                        +-        +     -                     
 z*  1  1       6  0.121062                        +-             +-                     
 z*  1  1       7  0.027832                        +-                  +-                
 z*  1  1       9  0.010323                        +-                       +-           
 z*  1  1      10  0.027832                        +-                            +-      
 z*  1  1      12  0.010323                        +-                                 +- 

 ci coefficient statistics:
           rq > 0.1                4
      0.1> rq > 0.01               5
     0.01> rq > 0.001              0
    0.001> rq > 0.0001             3
   0.0001> rq > 0.00001            0
  0.00001> rq > 0.000001           0
 0.000001> rq                      0
           all                    12
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:          51 2x:           0 4x:           0
All internal counts: zz :          82 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:          81    task #     2:          37    task #
  iref  icsf         v(icsf)             hv(icsf)
     1     1     -0.912866314463      7.288707053323
     2     2     -0.346912765723      2.769896842892
     3     3     -0.152598465864      1.218410074782
     4     4      0.000148694478     -0.001187239002
     5     5      0.081373020629     -0.649716283764
     6     6      0.121062328722     -0.966612345390
     7     7      0.027831725846     -0.222220157835
     8     8      0.000399373011     -0.003188761414
     9     9      0.010322939426     -0.082422672643
    10    10      0.027831726187     -0.222220160556
    11    11      0.000399358076     -0.003188642169
    12    12      0.010322939085     -0.082422669924

 number of reference csfs (nref) is    12.  root number (iroot) is  1.
 c0**2 =   1.00000000  c**2 (all zwalks) =   1.00000000

 pople ci energy extrapolation is computed with  4 correlated electrons.

 eref      =     -7.984418898854   "relaxed" cnot**2         =   1.000000000000
 eci       =     -7.984418898854   deltae = eci - eref       =   0.000000000000
 eci+dv1   =     -7.984418898854   dv1 = (1-cnot**2)*deltae  =   0.000000000000
 eci+dv2   =     -7.984418898854   dv2 = dv1 / cnot**2       =   0.000000000000
 eci+dv3   =     -7.984418898854   dv3 = dv1 / (2*cnot**2-1) =   0.000000000000
 eci+pople =     -7.984418898854   (  4e- scaled deltae )    =   0.000000000000


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 2) =        -7.8889556575

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7    8

                                          orbital     1    2    3    4    9   10   12   13

                                         symmetry   a1   a1   a1   a1   b1   b1   b2   b2 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1  0.347410                        +-   +-                               
 z*  1  1       2 -0.916373                        +-   +     -                          
 z*  1  1       3  0.056487                        +-   +          -                     
 z*  1  1       5  0.181114                        +-        +     -                     
 z*  1  1       6 -0.055479                        +-             +-                     
 z*  1  1       8  0.014525                        +-                  +     -           
 z*  1  1      11  0.014525                        +-                            +     - 

 ci coefficient statistics:
           rq > 0.1                3
      0.1> rq > 0.01               4
     0.01> rq > 0.001              5
    0.001> rq > 0.0001             0
   0.0001> rq > 0.00001            0
  0.00001> rq > 0.000001           0
 0.000001> rq                      0
           all                    12
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:          51 2x:           0 4x:           0
All internal counts: zz :          82 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:          81    task #     2:          37    task #
  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.347409565992     -2.740698661097
     2     2     -0.916373137302      7.229227045889
     3     3      0.056487166181     -0.445624749219
     4     4     -0.003568098678      0.028148572249
     5     5      0.181114186958     -1.428801789857
     6     6     -0.055478877902      0.437670407696
     7     7     -0.004398296725      0.034697967833
     8     8      0.014525032828     -0.114587339902
     9     9      0.003349449299     -0.026423656997
    10    10     -0.004398284335      0.034697870089
    11    11      0.014525039437     -0.114587392039
    12    12      0.003349436909     -0.026423559255

 number of reference csfs (nref) is    12.  root number (iroot) is  2.
 c0**2 =   1.00000000  c**2 (all zwalks) =   1.00000000

 pople ci energy extrapolation is computed with  4 correlated electrons.

 eref      =     -7.888955657490   "relaxed" cnot**2         =   1.000000000000
 eci       =     -7.888955657490   deltae = eci - eref       =   0.000000000000
 eci+dv1   =     -7.888955657490   dv1 = (1-cnot**2)*deltae  =   0.000000000000
 eci+dv2   =     -7.888955657490   dv2 = dv1 / cnot**2       =   0.000000000000
 eci+dv3   =     -7.888955657490   dv3 = dv1 / (2*cnot**2-1) =   0.000000000000
 eci+pople =     -7.888955657490   (  4e- scaled deltae )    =   0.000000000000
 passed aftci ... 
 readint2: molcas,dalton2=                     0                     0
 files%faoints=aoints              
NO coefficients and occupation numbers are written to nocoef_ci.1

################################################################################
 one electron density matrix calculation 
################################################################################


 workspace allocation information: mxcore=       13106116
sifcfg setup: record length 4096 DP
# d1 elements per record  3272
# d2 elements per record  2730
 computing final density
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
 1-DENSITY: executing task                      1                     1
 1-DENSITY: executing task                     71                     1
================================================================================
   DYZ=       0  DYX=       0  DYW=       0
   D0Z=      13  D0Y=       0  D0X=       0  D0W=       0
  DDZI=      29 DDYI=       0 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
 Trace of MO density:    4.00000000000000     
                     4 correlated and                      0 
 frozen core electrons


*****   symmetry block SYM1   *****

               no    1        no    2        no    3        no    4        no    5        no    6        no    7        no    8

      occ    2.00000        1.92898       6.740357E-02   9.379263E-05    0.00000        0.00000        0.00000        0.00000    
 
  mo    1    1.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  mo    2    0.00000       0.968031      -0.198985      -0.152712        0.00000        0.00000        0.00000        0.00000    
  mo    3    0.00000       0.236935       0.525568       0.817092        0.00000        0.00000        0.00000        0.00000    
  mo    4    0.00000       8.232884E-02   0.827153      -0.555913        0.00000        0.00000        0.00000        0.00000    
  mo    5    0.00000        0.00000        0.00000        0.00000        0.00000        1.00000        0.00000        0.00000    
  mo    6    0.00000        0.00000        0.00000        0.00000        0.00000        0.00000        1.00000        0.00000    
  mo    7    0.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000        1.00000    
  mo    8    0.00000        0.00000        0.00000        0.00000        1.00000        0.00000        0.00000        0.00000    

               no    1        no    2        no    3        no    4        no    5        no    6        no    7        no    8

      occ    2.00000        1.92898       6.740357E-02   9.379263E-05    0.00000        0.00000        0.00000        0.00000    
 
  ao    1   0.996275      -0.127517      -0.186874      -7.777897E-02  -3.317768E-02   0.200765      -4.110535E-02   2.175361E-02
  ao    2   2.017703E-02   0.346971       0.757137       0.189281       0.312742       -1.82648       0.541473      -0.442942    
  ao    3  -9.132523E-03   6.589951E-02   0.162039       0.227981      -0.114737        1.94433       0.127420      -0.170488    
  ao    4  -9.031536E-04   5.332231E-02   6.998767E-02  -0.152602       0.185049       3.327172E-03  -0.769045      -0.717590    
  ao    5   7.690645E-04   0.151852       0.397893      -0.868141       0.148635       7.713588E-02    1.18307      -0.352776    
  ao    6   6.105703E-04   0.380801      -0.753316       0.359254       0.137473       1.459734E-02   0.678749      -0.906592    
  ao    7  -5.160607E-04   0.375386      -0.237790      -3.056888E-02  -0.330642      -2.487495E-02   -1.51921        1.54168    
  ao    8  -7.241578E-04  -1.262477E-02   5.305143E-03   2.490449E-03    1.01080       6.380689E-03   4.815730E-02   9.819023E-02


*****   symmetry block SYM2   *****

               no    1        no    2        no    3

      occ   1.549717E-03   2.129382E-04    0.00000    
 
  mo    1   0.999870      -1.612272E-02    0.00000    
  mo    2   1.612272E-02   0.999870        0.00000    
  mo    3    0.00000        0.00000        1.00000    

               no    1        no    2        no    3

      occ   1.549717E-03   2.129382E-04    0.00000    
 
  ao    1   7.344492E-02   0.188344        1.05851    
  ao    2   0.533740       0.740385      -0.578629    
  ao    3   0.784601      -0.620526       7.378257E-02


*****   symmetry block SYM3   *****

               no    1        no    2        no    3

      occ   1.549717E-03   2.129382E-04    0.00000    
 
  mo    1   0.999870      -1.612212E-02    0.00000    
  mo    2   1.612212E-02   0.999870        0.00000    
  mo    3    0.00000        0.00000        1.00000    

               no    1        no    2        no    3

      occ   1.549717E-03   2.129382E-04    0.00000    
 
  ao    1   7.344492E-02   0.188344        1.05851    
  ao    2   0.533740       0.740385      -0.578629    
  ao    3   0.784601      -0.620526       7.378257E-02


 total number of electrons =    4.0000000000

 test slabel:                     4
  a1  b1  b2  a2 


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        a1  partial gross atomic populations
   ao class       1a1        2a1        3a1        4a1        5a1        6a1 
     1_ s       2.000061   0.500706   0.035740   0.000017   0.000000   0.000000
     1_ p       0.000002   0.181190   0.000794   0.000073   0.000000   0.000000
     1_ s      -0.000070   1.245841   0.030897   0.000004   0.000000   0.000000
     1_ p       0.000008   0.001240  -0.000028   0.000000   0.000000   0.000000
 
   ao class       7a1        8a1 

                        b1  partial gross atomic populations
   ao class       1b1        2b1        3b1 
     1_ p       0.000545   0.000139   0.000000
     1_ p       0.001004   0.000074   0.000000

                        b2  partial gross atomic populations
   ao class       1b2        2b2        3b2 
     1_ p       0.000545   0.000139   0.000000
     1_ p       0.001004   0.000074   0.000000


                        gross atomic populations
     ao            1_         1_
      s         2.536524   1.276672
      p         0.183427   0.003377
    total       2.719951   1.280049
 

 Total number of electrons:    4.00000000

NO coefficients and occupation numbers are written to nocoef_ci.2

################################################################################
 one electron density matrix calculation 
################################################################################


 workspace allocation information: mxcore=       13106116
sifcfg setup: record length 4096 DP
# d1 elements per record  3272
# d2 elements per record  2730
 computing final density
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    2
--------------------------------------------------------------------------------
 1-DENSITY: executing task                      1                     1
 1-DENSITY: executing task                     71                     1
================================================================================
   DYZ=       0  DYX=       0  DYW=       0
   D0Z=      13  D0Y=       0  D0X=       0  D0W=       0
  DDZI=      29 DDYI=       0 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
 Trace of MO density:    4.00000000000000     
                     4 correlated and                      0 
 frozen core electrons


*****   symmetry block SYM1   *****

               no    1        no    2        no    3        no    4        no    5        no    6        no    7        no    8

      occ    2.00000        1.43017       0.567660       1.201300E-03    0.00000        0.00000        0.00000        0.00000    
 
  mo    1    1.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000    
  mo    2    0.00000       0.790148       0.580615       0.196348        0.00000        0.00000        0.00000        0.00000    
  mo    3    0.00000      -0.610726       0.772891       0.172201        0.00000        0.00000        0.00000        0.00000    
  mo    4    0.00000      -5.177342E-02  -0.255979       0.965295        0.00000        0.00000        0.00000        0.00000    
  mo    5    0.00000        0.00000        0.00000        0.00000        0.00000        1.00000        0.00000        0.00000    
  mo    6    0.00000        0.00000        0.00000        0.00000        0.00000        0.00000        1.00000        0.00000    
  mo    7    0.00000        0.00000        0.00000        0.00000        0.00000        0.00000        0.00000        1.00000    
  mo    8    0.00000        0.00000        0.00000        0.00000        1.00000        0.00000        0.00000        0.00000    

               no    1        no    2        no    3        no    4        no    5        no    6        no    7        no    8

      occ    2.00000        1.43017       0.567660       1.201300E-03    0.00000        0.00000        0.00000        0.00000    
 
  ao    1   0.996275       6.478606E-02  -0.160379      -0.165269      -3.317768E-02   0.200765      -4.110535E-02   2.175361E-02
  ao    2   2.017703E-02  -0.292630       0.440698       0.670541       0.312742       -1.82648       0.541473      -0.442942    
  ao    3  -9.132523E-03  -0.178553       0.216713       6.106803E-02  -0.114737        1.94433       0.127420      -0.170488    
  ao    4  -9.031536E-04   8.654954E-02  -6.042079E-02   0.141023       0.185049       3.327172E-03  -0.769045      -0.717590    
  ao    5   7.690645E-04   0.399202      -0.453448       0.755030       0.148635       7.713588E-02    1.18307      -0.352776    
  ao    6   6.105703E-04   0.414761       0.462419      -0.675056       0.137473       1.459734E-02   0.678749      -0.906592    
  ao    7  -5.160607E-04   0.373168       0.232103      -7.257530E-02  -0.330642      -2.487495E-02   -1.51921        1.54168    
  ao    8  -7.241578E-04  -1.201164E-02  -7.016500E-03  -4.699570E-04    1.01080       6.380689E-03   4.815730E-02   9.819023E-02


*****   symmetry block SYM2   *****

               no    1        no    2        no    3

      occ   2.645669E-04   2.185139E-04    0.00000    
 
  mo    1   0.822468       0.568811        0.00000    
  mo    2  -0.568811       0.822468        0.00000    
  mo    3    0.00000        0.00000        1.00000    

               no    1        no    2        no    3

      occ   2.645669E-04   2.185139E-04    0.00000    
 
  ao    1  -4.989090E-02   0.195904        1.05851    
  ao    2   3.130318E-03   0.912709      -0.578629    
  ao    3   0.999175      -4.796994E-02   7.378257E-02


*****   symmetry block SYM3   *****

               no    1        no    2        no    3

      occ   2.645669E-04   2.185139E-04    0.00000    
 
  mo    1   0.822468       0.568812        0.00000    
  mo    2  -0.568812       0.822468        0.00000    
  mo    3    0.00000        0.00000        1.00000    

               no    1        no    2        no    3

      occ   2.645669E-04   2.185139E-04    0.00000    
 
  ao    1  -4.989090E-02   0.195904        1.05851    
  ao    2   3.130318E-03   0.912709      -0.578629    
  ao    3   0.999175      -4.796994E-02   7.378257E-02


 total number of electrons =    4.0000000000

 test slabel:                     4
  a1  b1  b2  a2 


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        a1  partial gross atomic populations
   ao class       1a1        2a1        3a1        4a1        5a1        6a1 
     1_ s       2.000061   0.114564   0.293347   0.000446   0.000000   0.000000
     1_ p       0.000002   0.506460   0.048101   0.000537   0.000000   0.000000
     1_ s      -0.000070   0.808803   0.226179   0.000218   0.000000   0.000000
     1_ p       0.000008   0.000346   0.000034   0.000000   0.000000   0.000000
 
   ao class       7a1        8a1 

                        b1  partial gross atomic populations
   ao class       1b1        2b1        3b1 
     1_ p       0.000001   0.000219   0.000000
     1_ p       0.000264   0.000000   0.000000

                        b2  partial gross atomic populations
   ao class       1b2        2b2        3b2 
     1_ p       0.000001   0.000219   0.000000
     1_ p       0.000264   0.000000   0.000000


                        gross atomic populations
     ao            1_         1_
      s         2.408417   1.035129
      p         0.555538   0.000915
    total       2.963956   1.036044
 

 Total number of electrons:    4.00000000

========================GLOBAL TIMING PER NODE========================
   process    vectrn  integral   segment    diagon     dmain    davidc   finalvw   driver  
--------------------------------------------------------------------------------
   #   1         0.0       0.0       0.0       0.0       0.0       0.0       0.0       0.0
--------------------------------------------------------------------------------
       1         0.0       0.0       0.0       0.0       0.0       0.0       0.0       0.0
