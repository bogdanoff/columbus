 
 program cidrt 5.9  

 distinct row table construction, reference csf selection, and internal
 walk selection for multireference single- and double-excitation
configuration interaction.

 references:  r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).
              h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. symp. 15, 91 (1981).

 programmed by: Thomas Müller (FZ Juelich)
 based on the cidrt code for single DRTs.

 version date: 16-jul-04


 This Version of Program CIDRT-MS is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIDRT-MS    **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 workspace allocation parameters: lencor=      5000 mem1=         0 ifirst=         1
 expanded "keystrokes" are being written to files:
                       
 cidrtky                                                                        
  
 
 number of DRTs to be constructed [  0]: DRT infos are being written to files:
 cidrtfl.1                                                                      
  
 starting with the input common to all drts ... 
******************** ALL   DRTs    ********************
 Spin-Orbit CI Calculation?(y,[n]) Spin-Free Calculation
 
 input the spin multiplicity [  0]: spin multiplicity, smult            :   1    singlet 
 input the total number of electrons [  0]: total number of electrons, nelt     :     4
 input the number of irreps (1:8) [  0]: point group dimension, nsym         :     4
 enter symmetry labels:(y,[n]) enter 4 labels (a4):
 enter symmetry label, default=   1
 enter symmetry label, default=   2
 enter symmetry label, default=   3
 enter symmetry label, default=   4
 symmetry labels: (symmetry, slabel)
 ( 1,  a1 ) ( 2,  b1 ) ( 3,  b2 ) ( 4,  a2 ) 
 input nmpsy(*):
 nmpsy(*)=        35  20  20  10
 
   symmetry block summary
 block(*)=         1   2   3   4
 slabel(*)=      a1  b1  b2  a2 
 nmpsy(*)=        35  20  20  10
 
 total molecular orbitals            :    85
 
 input the frozen core orbitals (sym(i),rmo(i),i=1,nfct):
 total frozen core orbitals, nfct    :     0
 no frozen core orbitals entered
 
 number of frozen core orbitals      :     0
 number of frozen core electrons     :     0
 number of internal electrons        :     4
 
 input the frozen virtual orbitals (sym(i),rmo(i),i=1,nfvt):
 total frozen virtual orbitals, nfvt :     0

 no frozen virtual orbitals entered
 
 input the internal orbitals (sym(i),rmo(i),i=1,niot):
 niot                                :    15
 
 modrt(*)=         1   2   3   4   5   6   7   8  36  37  38  56  57  58  76
 slabel(*)=      a1  a1  a1  a1  a1  a1  a1  a1  b1  b1  b1  b2  b2  b2  a2 
 
 total number of orbitals            :    85
 number of frozen core orbitals      :     0
 number of frozen virtual orbitals   :     0
 number of internal orbitals         :    15
 number of external orbitals         :    70
 
 orbital-to-level mapping vector
 map(*)=          71  72  73  74  75  76  77  78   1   2   3   4   5   6   7
                   8   9  10  11  12  13  14  15  16  17  18  19  20  21  22
                  23  24  25  26  27  79  80  81  28  29  30  31  32  33  34
                  35  36  37  38  39  40  41  42  43  44  82  83  84  45  46
                  47  48  49  50  51  52  53  54  55  56  57  58  59  60  61
                  85  62  63  64  65  66  67  68  69  70
==================== START DRT#   1====================
================================================================================
  Input for DRT number                      1
 input the molecular spatial symmetry (irrep 1:nsym) [  0]: state spatial symmetry label        :  a1 
 
 input the number of ref-csf doubly-occupied orbitals [  0]: (ref) doubly-occupied orbitals      :     0
 
 no. of internal orbitals            :    15
 no. of doubly-occ. (ref) orbitals   :     0
 no. active (ref) orbitals           :    15
 no. of active electrons             :     4
 
 input the active-orbital, active-electron occmnr(*):
   1  2  3  4  5  6  7  8 36 37 38 56 57 58 76
 input the active-orbital, active-electron occmxr(*):
   1  2  3  4  5  6  7  8 36 37 38 56 57 58 76
 
 actmo(*) =        1   2   3   4   5   6   7   8  36  37  38  56  57  58  76
 occmnr(*)=        0   0   0   0   0   0   0   0   0   0   0   0   0   0   4
 occmxr(*)=        4   4   4   4   4   4   4   4   4   4   4   4   4   4   4
 reference csf cumulative electron occupations:
 modrt(*)=         1   2   3   4   5   6   7   8  36  37  38  56  57  58  76
 occmnr(*)=        0   0   0   0   0   0   0   0   0   0   0   0   0   0   4
 occmxr(*)=        4   4   4   4   4   4   4   4   4   4   4   4   4   4   4
 
 input the active-orbital bminr(*):
   1  2  3  4  5  6  7  8 36 37 38 56 57 58 76
 input the active-orbital bmaxr(*):
   1  2  3  4  5  6  7  8 36 37 38 56 57 58 76
 reference csf b-value constraints:
 modrt(*)=         1   2   3   4   5   6   7   8  36  37  38  56  57  58  76
 bminr(*)=         0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
 bmaxr(*)=         4   4   4   4   4   4   4   4   4   4   4   4   4   4   4
 input the active orbital smaskr(*):
   1  2  3  4  5  6  7  8 36 37 38 56 57 58 76
 modrt:smaskr=
   1:1000   2:1111   3:1111   4:1111   5:1111   6:1111   7:1111   8:1111
  36:1111  37:1111  38:1111  56:1111  57:1111  58:1111  76:1111
 
 input the maximum excitation level from the reference csfs [  2]: maximum excitation from ref. csfs:  :     2
 number of internal electrons:       :     4
 
 input the internal-orbital mrsdci occmin(*):
   1  2  3  4  5  6  7  8 36 37 38 56 57 58 76
 input the internal-orbital mrsdci occmax(*):
   1  2  3  4  5  6  7  8 36 37 38 56 57 58 76
 mrsdci csf cumulative electron occupations:
 modrt(*)=         1   2   3   4   5   6   7   8  36  37  38  56  57  58  76
 occmin(*)=        0   0   0   0   0   0   0   0   0   0   0   0   0   0   2
 occmax(*)=        4   4   4   4   4   4   4   4   4   4   4   4   4   4   4
 
 input the internal-orbital mrsdci bmin(*):
   1  2  3  4  5  6  7  8 36 37 38 56 57 58 76
 input the internal-orbital mrsdci bmax(*):
   1  2  3  4  5  6  7  8 36 37 38 56 57 58 76
 mrsdci b-value constraints:
 modrt(*)=         1   2   3   4   5   6   7   8  36  37  38  56  57  58  76
 bmin(*)=          0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
 bmax(*)=          4   4   4   4   4   4   4   4   4   4   4   4   4   4   4
 
 input the internal-orbital smask(*):
   1  2  3  4  5  6  7  8 36 37 38 56 57 58 76
 modrt:smask=
   1:1111   2:1111   3:1111   4:1111   5:1111   6:1111   7:1111   8:1111
  36:1111  37:1111  38:1111  56:1111  57:1111  58:1111  76:1111
 
 internal orbital summary:
 block(*)=         1   1   1   1   1   1   1   1   2   2   2   3   3   3   4
 slabel(*)=      a1  a1  a1  a1  a1  a1  a1  a1  b1  b1  b1  b2  b2  b2  a2 
 rmo(*)=           1   2   3   4   5   6   7   8   1   2   3   1   2   3   1
 modrt(*)=         1   2   3   4   5   6   7   8  36  37  38  56  57  58  76
 
 reference csf info:
 occmnr(*)=        0   0   0   0   0   0   0   0   0   0   0   0   0   0   4
 occmxr(*)=        4   4   4   4   4   4   4   4   4   4   4   4   4   4   4
 
 bminr(*)=         0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
 bmaxr(*)=         4   4   4   4   4   4   4   4   4   4   4   4   4   4   4
 
 
 mrsdci csf info:
 occmin(*)=        0   0   0   0   0   0   0   0   0   0   0   0   0   0   2
 occmax(*)=        4   4   4   4   4   4   4   4   4   4   4   4   4   4   4
 
 bmin(*)=          0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
 bmax(*)=          4   4   4   4   4   4   4   4   4   4   4   4   4   4   4
 
******************** END   DRT#   1********************
******************** ALL   DRTs    ********************
 
 impose generalized interacting space restrictions?(y,[n]) generalized interacting space restrictions will not be imposed.

 a priori removal of distinct rows:

 input the level, a, and b values for the vertices 
 to be removed (-1/ to end).

 input level, a, and b (-1/ to end):
 no vertices marked for removal

 number of rows in the drt :  86

 manual arc removal step:


 input the level, a, b, and step values 
 for the arcs to be removed (-1/ to end).

 input the level, a, b, and step (-1/ to end):
 remarc:   0 arcs removed out of   0 specified.

 xbarz=    4200
 xbary=    1120
 xbarx=     105
 xbarw=     120
        --------
 nwalk=    5545
------------------------------------------------------------------------
Core address array: 
             1            67
------------------------------------------------------------------------
 input the range of drt levels to print (l1,l2):
 levprt(*)        -1   0
==================== START DRT#   1====================
  INPUT FOR DRT #                      1

 reference-csf selection step 1:
 total number of z-walks in the drt, nzwalk=    4200

 input the list of allowed reference symmetries:
 allowed reference symmetries:             1
 allowed reference symmetry labels:      a1 
 keep all of the z-walks as references?(y,[n]) all z-walks are initially deleted.
 
 generate walks while applying reference drt restrictions?([y],n) reference drt restrictions will be imposed on the z-walks.
 
 impose additional orbital-group occupation restrictions?(y,[n]) 
 apply primary reference occupation restrictions?(y,[n]) 
 manually select individual walks?(y,[n])
 step 1 reference csf selection complete.
       41 csfs initially selected from    4200 total walks.

 beginning step-vector based selection.
 enter [internal_orbital_step_vector/disposition] pairs:

 enter internal orbital step vector, (-1/ to end):
   1  2  3  4  5  6  7  8 36 37 38 56 57 58 76

 step 2 reference csf selection complete.
       41 csfs currently selected from    4200 total walks.

 beginning numerical walk based selection.
 enter positive walk numbers to add walks,
 negative walk numbers to delete walks, and zero to end:

 input reference walk number (0 to end) [  0]:
 numerical walk-number based selection complete.
       41 reference csfs selected from    4200 total z-walks.
 
 input the reference occupations, mu(*):
 reference occupations:
 mu(*)=            0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
 
------------------------------------------------------------------------
Core address array: 
             1            67           723          1379          5579

------------------------------------------------------------------------
 bummer (fatal):core memory, need at least      5579
