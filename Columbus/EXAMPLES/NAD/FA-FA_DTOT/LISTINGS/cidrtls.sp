 
 program cidrt 5.9  

 distinct row table construction, reference csf selection, and internal
 walk selection for multireference single- and double-excitation
configuration interaction.

 references:  r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).
              h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. symp. 15, 91 (1981).

 based on the initial version by  Ron Shepard

 extended for spin-orbit CI calculations ( Russ Pitzer, OSU)

 and large active spaces (Thomas Müller, FZ Juelich)

 version date: 16-jul-04


 This Version of Program CIDRT is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIDRT       **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 workspace allocation parameters: lencor= 222822400 mem1=         0 ifirst=         1
 expanded "keystrokes" are being written to file:
 /scratch/plasserf/74907/WORK/cidrtky                                            
 Spin-Orbit CI Calculation?(y,[n]) Spin-Free Calculation
 
 input the spin multiplicity [  0]: spin multiplicity, smult            :   1    singlet 
 input the total number of electrons [  0]: total number of electrons, nelt     :    32
 input the number of irreps (1:8) [  0]: point group dimension, nsym         :     1
 enter symmetry labels:(y,[n]) enter 1 labels (a4):
 enter symmetry label, default=   1
 symmetry labels: (symmetry, slabel)
 ( 1,  a  ) 
 input nmpsy(*):
 nmpsy(*)=        44
 
   symmetry block summary
 block(*)=         1
 slabel(*)=      a  
 nmpsy(*)=        44
 
 total molecular orbitals            :    44
 input the molecular spatial symmetry (irrep 1:nsym) [  0]: state spatial symmetry label        :  a  
 
 input the frozen core orbitals (sym(i),rmo(i),i=1,nfct):
 total frozen core orbitals, nfct    :     0
 no frozen core orbitals entered
 
 number of frozen core orbitals      :     0
 number of frozen core electrons     :     0
 number of internal electrons        :    32
 
 input the frozen virtual orbitals (sym(i),rmo(i),i=1,nfvt):
 total frozen virtual orbitals, nfvt :     0

 no frozen virtual orbitals entered
 
 input the internal orbitals (sym(i),rmo(i),i=1,niot):
 niot                                :    18
 
 modrt(*)=         1   2   3   4   5   6   7   8   9  10  11  12  13  14  15
                  16  17  18
 slabel(*)=      a   a   a   a   a   a   a   a   a   a   a   a   a   a   a  
                 a   a   a  
 
 total number of orbitals            :    44
 number of frozen core orbitals      :     0
 number of frozen virtual orbitals   :     0
 number of internal orbitals         :    18
 number of external orbitals         :    26
 
 orbital-to-level mapping vector
 map(*)=          27  28  29  30  31  32  33  34  35  36  37  38  39  40  41
                  42  43  44   1   2   3   4   5   6   7   8   9  10  11  12
                  13  14  15  16  17  18  19  20  21  22  23  24  25  26
 
 input the number of ref-csf doubly-occupied orbitals [  0]: (ref) doubly-occupied orbitals      :    12
 
 no. of internal orbitals            :    18
 no. of doubly-occ. (ref) orbitals   :    12
 no. active (ref) orbitals           :     6
 no. of active electrons             :     8
 
 input the active-orbital, active-electron occmnr(*):
  13 14 15 16 17 18
 input the active-orbital, active-electron occmxr(*):
  13 14 15 16 17 18
 
 actmo(*) =       13  14  15  16  17  18
 occmnr(*)=        0   0   0   0   0   8
 occmxr(*)=        8   8   8   8   8   8
 reference csf cumulative electron occupations:
 modrt(*)=         1   2   3   4   5   6   7   8   9  10  11  12  13  14  15
                  16  17  18
 occmnr(*)=        2   4   6   8  10  12  14  16  18  20  22  24  24  24  24
                  24  24  32
 occmxr(*)=        2   4   6   8  10  12  14  16  18  20  22  24  32  32  32
                  32  32  32
 
 input the active-orbital bminr(*):
  13 14 15 16 17 18
 input the active-orbital bmaxr(*):
  13 14 15 16 17 18
 reference csf b-value constraints:
 modrt(*)=         1   2   3   4   5   6   7   8   9  10  11  12  13  14  15
                  16  17  18
 bminr(*)=         0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
                   0   0   0
 bmaxr(*)=         0   0   0   0   0   0   0   0   0   0   0   0   8   8   8
                   8   8   8
 input the active orbital smaskr(*):
  13 14 15 16 17 18
 modrt:smaskr=
   1:1000   2:1000   3:1000   4:1000   5:1000   6:1000   7:1000   8:1000
   9:1000  10:1000  11:1000  12:1000  13:1111  14:1111  15:1111  16:1111
  17:1111  18:1111
 
 input the maximum excitation level from the reference csfs [  2]: maximum excitation from ref. csfs:  :     1
 number of internal electrons:       :    32
 
 input the internal-orbital mrsdci occmin(*):
   1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18
 input the internal-orbital mrsdci occmax(*):
   1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18
 mrsdci csf cumulative electron occupations:
 modrt(*)=         1   2   3   4   5   6   7   8   9  10  11  12  13  14  15
                  16  17  18
 occmin(*)=        2   4   6   8   9  11  13  15  17  19  21  23  23  23  23
                  23  23  31
 occmax(*)=       32  32  32  32  32  32  32  32  32  32  32  32  32  32  32
                  32  32  32
 
 input the internal-orbital mrsdci bmin(*):
   1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18
 input the internal-orbital mrsdci bmax(*):
   1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18
 mrsdci b-value constraints:
 modrt(*)=         1   2   3   4   5   6   7   8   9  10  11  12  13  14  15
                  16  17  18
 bmin(*)=          0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
                   0   0   0
 bmax(*)=         32  32  32  32  32  32  32  32  32  32  32  32  32  32  32
                  32  32  32
 
 input the internal-orbital smask(*):
   1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18
 modrt:smask=
   1:1000   2:1000   3:1000   4:1000   5:1111   6:1111   7:1111   8:1111
   9:1111  10:1111  11:1111  12:1111  13:1111  14:1111  15:1111  16:1111
  17:1111  18:1111
 
 internal orbital summary:
 block(*)=         1   1   1   1   1   1   1   1   1   1   1   1   1   1   1
                   1   1   1
 slabel(*)=      a   a   a   a   a   a   a   a   a   a   a   a   a   a   a  
                 a   a   a  
 rmo(*)=           1   2   3   4   5   6   7   8   9  10  11  12  13  14  15
                  16  17  18
 modrt(*)=         1   2   3   4   5   6   7   8   9  10  11  12  13  14  15
                  16  17  18
 
 reference csf info:
 occmnr(*)=        2   4   6   8  10  12  14  16  18  20  22  24  24  24  24
                  24  24  32
 occmxr(*)=        2   4   6   8  10  12  14  16  18  20  22  24  32  32  32
                  32  32  32
 
 bminr(*)=         0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
                   0   0   0
 bmaxr(*)=         0   0   0   0   0   0   0   0   0   0   0   0   8   8   8
                   8   8   8
 
 
 mrsdci csf info:
 occmin(*)=        2   4   6   8   9  11  13  15  17  19  21  23  23  23  23
                  23  23  31
 occmax(*)=       32  32  32  32  32  32  32  32  32  32  32  32  32  32  32
                  32  32  32
 
 bmin(*)=          0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
                   0   0   0
 bmax(*)=         32  32  32  32  32  32  32  32  32  32  32  32  32  32  32
                  32  32  32
 

 a priori removal of distinct rows:

 input the level, a, and b values for the vertices 
 to be removed (-1/ to end).

 input level, a, and b (-1/ to end):
 no vertices marked for removal
 
 impose generalized interacting space restrictions?(y,[n]) generalized interacting space restrictions will not be imposed.
 multp                     0                     0                     0
                     0                     0                     0
                     0                     0                     0
 spnir
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  hmult                     0
 lxyzir                     0                     0                     0
 spnir
  1  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  1  0  0  0  1  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  1  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  1  0  0  0  1  0  0  0  1  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  1  0  0  0  1  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0

 number of rows in the drt : 135

 manual arc removal step:


 input the level, a, b, and step values 
 for the arcs to be removed (-1/ to end).

 input the level, a, b, and step (-1/ to end):
 remarc:   0 arcs removed out of   0 specified.

 xbarz=         665
 xbary=        6930
 xbarx=       32781
 xbarw=       21455
        --------
 nwalk=       61831
 input the range of drt levels to print (l1,l2):
 levprt(*)        -1   0

 reference-csf selection step 1:
 total number of z-walks in the drt, nzwalk=     665

 input the list of allowed reference symmetries:
 allowed reference symmetries:             1
 allowed reference symmetry labels:      a  
 keep all of the z-walks as references?(y,[n]) all z-walks are initially deleted.
 
 generate walks while applying reference drt restrictions?([y],n) reference drt restrictions will be imposed on the z-walks.
 
 impose additional orbital-group occupation restrictions?(y,[n]) 
 apply primary reference occupation restrictions?(y,[n]) 
 manually select individual walks?(y,[n])
 step 1 reference csf selection complete.
      105 csfs initially selected from     665 total walks.

 beginning step-vector based selection.
 enter [internal_orbital_step_vector/disposition] pairs:

 enter internal orbital step vector, (-1/ to end):
   1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18

 step 2 reference csf selection complete.
      105 csfs currently selected from     665 total walks.

 beginning numerical walk based selection.
 enter positive walk numbers to add walks,
 negative walk numbers to delete walks, and zero to end:

 input reference walk number (0 to end) [  0]:
 numerical walk-number based selection complete.
      105 reference csfs selected from     665 total z-walks.
 
 input the reference occupations, mu(*):
 reference occupations:
 mu(*)=            2   2   2   2   2   2   2   2   2   2   2   2   0   0   0
                   0   0   0
 
 number of step vectors saved:    105

 exlimw: beginning excitation-based walk selection...

  number of valid internal walks of each symmetry:

       a  
      ----
 z       665
 y      1890
 x         0
 w         0

 csfs grouped by internal walk symmetry:

       a  
      ----
 z       665
 y     49140
 x         0
 w         0

 total csf counts:
 z-vertex:         665
 y-vertex:       49140
 x-vertex:           0
 w-vertex:           0
           --------
 total:          49805
 
 this is an obsolete prompt.(y,[n])
 final mrsdci walk selection step:

 nvalw(*)=     665    1890       0       0 nvalwt=    2555

 enter positive walk numbers to add walks,
 negative walk numbers to delete walks, and zero to end.

 input mrsdci walk number (0 to end) [  0]:
 end of manual mrsdci walk selection.
 number added=   0 number removed=   0

 nvalw(*)=     665    1890       0       0 nvalwt=    2555


 lprune: l(*,*,*) pruned with nwalk=   61831 nvalwt=    2555
 lprune:  z-drt, nprune=   237
 lprune:  y-drt, nprune=   195
 lprune: wx-drt, nprune=   328

 xbarz=         665
 xbary=        1890
 xbarx=           0
 xbarw=           0
        --------
 nwalk=        2555
 levprt(*)        -1   0

 beginning the reference csf index recomputation...

     iref   iwalk  step-vector
   ------  ------  ------------
        1       1  333333333333333300
        2       2  333333333333333120
        3       3  333333333333333102
        4       4  333333333333333030
        5       5  333333333333333012
        6       6  333333333333333003
        7       7  333333333333331320
        8       8  333333333333331302
        9       9  333333333333331230
       10      10  333333333333331212
       11      11  333333333333331203
       12      12  333333333333331122
       13      13  333333333333331032
       14      14  333333333333331023
       15      15  333333333333330330
       16      16  333333333333330312
       17      17  333333333333330303
       18      18  333333333333330132
       19      19  333333333333330123
       20      20  333333333333330033
       21      21  333333333333313320
       22      22  333333333333313302
       23      23  333333333333313230
       24      24  333333333333313212
       25      25  333333333333313203
       26      26  333333333333313122
       27      27  333333333333313032
       28      28  333333333333313023
       29      29  333333333333312330
       30      30  333333333333312312
       31      31  333333333333312303
       32      32  333333333333312132
       33      33  333333333333312123
       34      34  333333333333312033
       35      35  333333333333311322
       36      36  333333333333311232
       37      37  333333333333311223
       38      38  333333333333310332
       39      39  333333333333310323
       40      40  333333333333310233
       41      41  333333333333303330
       42      42  333333333333303312
       43      43  333333333333303303
       44      44  333333333333303132
       45      45  333333333333303123
       46      46  333333333333303033
       47      47  333333333333301332
       48      48  333333333333301323
       49      49  333333333333301233
       50      50  333333333333300333
       51      51  333333333333133320
       52      52  333333333333133302
       53      53  333333333333133230
       54      54  333333333333133212
       55      55  333333333333133203
       56      56  333333333333133122
       57      57  333333333333133032
       58      58  333333333333133023
       59      59  333333333333132330
       60      60  333333333333132312
       61      61  333333333333132303
       62      62  333333333333132132
       63      63  333333333333132123
       64      64  333333333333132033
       65      65  333333333333131322
       66      66  333333333333131232
       67      67  333333333333131223
       68      68  333333333333130332
       69      69  333333333333130323
       70      70  333333333333130233
       71      71  333333333333123330
       72      72  333333333333123312
       73      73  333333333333123303
       74      74  333333333333123132
       75      75  333333333333123123
       76      76  333333333333123033
       77      77  333333333333121332
       78      78  333333333333121323
       79      79  333333333333121233
       80      80  333333333333120333
       81      81  333333333333113322
       82      82  333333333333113232
       83      83  333333333333113223
       84      84  333333333333112332
       85      85  333333333333112323
       86      86  333333333333112233
       87      87  333333333333103332
       88      88  333333333333103323
       89      89  333333333333103233
       90      90  333333333333102333
       91      91  333333333333033330
       92      92  333333333333033312
       93      93  333333333333033303
       94      94  333333333333033132
       95      95  333333333333033123
       96      96  333333333333033033
       97      97  333333333333031332
       98      98  333333333333031323
       99      99  333333333333031233
      100     100  333333333333030333
      101     101  333333333333013332
      102     102  333333333333013323
      103     103  333333333333013233
      104     104  333333333333012333
      105     105  333333333333003333
 indx01:   105 elements set in vec01(*)

 beginning the valid upper walk index recomputation...
 indx01:  2555 elements set in vec01(*)

 beginning the final csym(*) computation...

  number of valid internal walks of each symmetry:

       a  
      ----
 z       665
 y      1890
 x         0
 w         0

 csfs grouped by internal walk symmetry:

       a  
      ----
 z       665
 y     49140
 x         0
 w         0

 total csf counts:
 z-vertex:         665
 y-vertex:       49140
 x-vertex:           0
 w-vertex:           0
           --------
 total:          49805
 
 input a title card, default=cidrt_title
 title card:
  cidrt_title                                                                   
  
 
 input a drt file name, default=cidrtfl
 drt and indexing arrays will be written to file:
 /scratch/plasserf/74907/WORK/cidrtfl                                            
 
 write the drt file?([y],n) drt file is being written...
 wrtstr:  a  
nwalk=    2555 cpos=     320 maxval=    9 cmprfactor=   87.48 %.
nwalk=    2555 cpos=      27 maxval=   99 cmprfactor=   97.89 %.
nwalk=    2555 cpos=       3 maxval=  999 cmprfactor=   99.65 %.
nwalk=    2555 cpos=       1 maxval= 9999 cmprfactor=   99.84 %.
 compressed with: nwalk=    2555 cpos=       3 maxval=  999 cmprfactor=   99.65 %.
initial index vector length:      2555
compressed index vector length:         3reduction:  99.88%
nwalk=     665 cpos=      83 maxval=    9 cmprfactor=   87.52 %.
nwalk=     665 cpos=       8 maxval=   99 cmprfactor=   97.59 %.
nwalk=     665 cpos=       2 maxval=  999 cmprfactor=   99.10 %.
nwalk=     665 cpos=       2 maxval= 9999 cmprfactor=   98.80 %.
 compressed with: nwalk=     665 cpos=       2 maxval=  999 cmprfactor=   99.10 %.
initial ref vector length:       665
compressed ref vector length:         2reduction:  99.70%
