

     ******************************************
     **    PROGRAM:              MCPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 original author: Daniel Robertson, FSU
 later revisions: Ron Shepard, ANL;
                  Michal Dallos, University Vienna



 This Version of Program mcpc is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



   ******  File header section  ******

 Headers form the restart file:
    Hermit Integral Program : SIFS version  hawk6.itc.univie. 15:40:44.981 05-Nov-11
     title                                                                          


   ******  DRT info section  ******

 Informations for the DRT no.  1
 Header form the DRT file: 
     title                                                                          
 Molecular symmetry group:   sym1
 Total number of electrons:   32
 Spin multiplicity:            1
 Number of active orbitals:    6
 Number of active electrons:   8
 Total number of CSFs:       105

   ***  Informations from the DRT number:   1

 
 Symmetry orbital summary:
 Symm.blocks:         1
 Symm.labels:         a  

 List of doubly occupied orbitals:
  1 a    2 a    3 a    4 a    5 a    6 a    7 a    8 a    9 a   10 a   11 a   12 a  

 List of active orbitals:
 13 a   14 a   15 a   16 a   17 a   18 a  


   ******  MCSCF convergence information:  ******

 MCSCF convergence criteria were satisfied.

 mcscf energy=  -227.5825318684    nuclear repulsion=   100.5855522736
 demc=             0.0000000000    wnorm=                 0.0000000038
 knorm=            0.0000000015    apxde=                 0.0000000000


 MCSCF calculation performmed for   1 symmetry.

 State averaging:
 No,  ssym, navst, wavst
  1    a      3   0.3333 0.3333 0.3333

 Input the DRT No of interest: [  1]:
In the DRT No.: 1 there are  3 states.

 Which one to take? [  1]:
 The CSFs for the state No  3 of the symmetry  a   will be printed
 according to the following print options :

 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 9) export wave function files for cioverlap (all states).
 0) end.

 input menu number [  0]: csfs will be printed based on coefficient magnitudes.

 input the coefficient threshold (end with 0.) [ 0.0000]:
 List of active orbitals:
 13 a   14 a   15 a   16 a   17 a   18 a  

   csf       coeff       coeff**2    step(*)
  -----  ------------  ------------  ------------
      7 -0.6900049189  0.4761067881  331320
      3  0.6298723197  0.3967391391  333102
     23  0.1330417643  0.0177001110  313230
     54  0.1296566450  0.0168108456  133212
     30 -0.1259027994  0.0158515149  312312
     77  0.1087562715  0.0118279266  121332
     61 -0.1078084010  0.0116226513  132303
     75  0.0995880950  0.0099177887  123123
     48  0.0836752924  0.0070015545  301323
     94  0.0807326083  0.0065177540  033132
     56  0.0686950131  0.0047190048  133122
     35  0.0673720548  0.0045389938  311322
     44  0.0669266862  0.0044791813  303132
     98  0.0634539828  0.0040264079  031323
     25  0.0617931551  0.0038183940  313203
     59 -0.0601586952  0.0036190686  132330
     90 -0.0409149526  0.0016740333  102333
    103 -0.0406924680  0.0016558770  013233
     84 -0.0236712415  0.0005603277  112332
     83  0.0195829199  0.0003834908  113223
     18  0.0146530852  0.0002147129  330132
     14 -0.0135035713  0.0001823464  331023
     40  0.0041576919  0.0000172864  310233
     64  0.0038472146  0.0000148011  132033

 input the coefficient threshold (end with 0.) [ 0.0000]:
 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 9) export wave function files for cioverlap (all states).
 0) end.

 input menu number [  0]: