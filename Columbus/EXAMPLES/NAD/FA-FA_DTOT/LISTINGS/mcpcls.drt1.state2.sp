

     ******************************************
     **    PROGRAM:              MCPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 original author: Daniel Robertson, FSU
 later revisions: Ron Shepard, ANL;
                  Michal Dallos, University Vienna



 This Version of Program mcpc is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



   ******  File header section  ******

 Headers form the restart file:
    Hermit Integral Program : SIFS version  hawk6.itc.univie. 15:40:44.981 05-Nov-11
     title                                                                          


   ******  DRT info section  ******

 Informations for the DRT no.  1
 Header form the DRT file: 
     title                                                                          
 Molecular symmetry group:   sym1
 Total number of electrons:   32
 Spin multiplicity:            1
 Number of active orbitals:    6
 Number of active electrons:   8
 Total number of CSFs:       105

   ***  Informations from the DRT number:   1

 
 Symmetry orbital summary:
 Symm.blocks:         1
 Symm.labels:         a  

 List of doubly occupied orbitals:
  1 a    2 a    3 a    4 a    5 a    6 a    7 a    8 a    9 a   10 a   11 a   12 a  

 List of active orbitals:
 13 a   14 a   15 a   16 a   17 a   18 a  


   ******  MCSCF convergence information:  ******

 MCSCF convergence criteria were satisfied.

 mcscf energy=  -227.5825318684    nuclear repulsion=   100.5855522736
 demc=             0.0000000000    wnorm=                 0.0000000038
 knorm=            0.0000000015    apxde=                 0.0000000000


 MCSCF calculation performmed for   1 symmetry.

 State averaging:
 No,  ssym, navst, wavst
  1    a      3   0.3333 0.3333 0.3333

 Input the DRT No of interest: [  1]:
In the DRT No.: 1 there are  3 states.

 Which one to take? [  1]:
 The CSFs for the state No  2 of the symmetry  a   will be printed
 according to the following print options :

 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 9) export wave function files for cioverlap (all states).
 0) end.

 input menu number [  0]: csfs will be printed based on coefficient magnitudes.

 input the coefficient threshold (end with 0.) [ 0.0000]:
 List of active orbitals:
 13 a   14 a   15 a   16 a   17 a   18 a  

   csf       coeff       coeff**2    step(*)
  -----  ------------  ------------  ------------
      2  0.6944829727  0.4823065993  333120
      8 -0.6250742894  0.3907178672  331302
     29 -0.1330774198  0.0177095997  312330
     60 -0.1291892705  0.0166898676  132312
     24  0.1260693696  0.0158934860  313212
     74 -0.1092621485  0.0119382171  123132
     55  0.1077557335  0.0116112981  133203
     78 -0.0990201722  0.0098049945  121323
     45 -0.0840461581  0.0070637567  303123
     97 -0.0803031480  0.0064485956  031332
     65 -0.0689396887  0.0047526807  131322
     26 -0.0669637339  0.0044841417  313122
     47 -0.0665265156  0.0044257773  301332
     95 -0.0637893379  0.0040690796  033123
     31 -0.0611468729  0.0037389401  312303
     53  0.0604839595  0.0036583094  133230
     89  0.0409940854  0.0016805150  103233
    104  0.0405521442  0.0016444764  012333
     82  0.0237144119  0.0005623733  113232
     85 -0.0195699655  0.0003829836  112323
     13  0.0144731337  0.0002094716  331032
     19 -0.0132470166  0.0001754834  330123
     34  0.0041349734  0.0000170980  312033
     70  0.0037931801  0.0000143882  130233

 input the coefficient threshold (end with 0.) [ 0.0000]:
 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 9) export wave function files for cioverlap (all states).
 0) end.

 input menu number [  0]: