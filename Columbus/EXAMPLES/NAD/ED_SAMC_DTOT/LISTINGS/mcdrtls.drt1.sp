
 program "mcdrt 4.1 a3"

 distinct row table specification and csf
 selection for mcscf wavefunction optimization.

 programmed by: ron shepard

 version date: 17-oct-91


 This Version of Program mcdrt is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              MCDRT       **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 expanded keystroke file:
 /scratch/runlocal/WORK/mcdrtky                                                  
 
 input the spin multiplicity [  0]: spin multiplicity:    2    doublet 
 input the total number of electrons [  0]: nelt:     31
 input the number of irreps (1-8) [  0]: nsym:      4
 enter symmetry labels:(y,[n]) enter 4 labels (a4):
 enter symmetry label, default=   1
 enter symmetry label, default=   2
 enter symmetry label, default=   3
 enter symmetry label, default=   4
 input the molecular spatial symmetry (irrep 1:nsym) [  0]: spatial symmetry is irrep number:      1
 
 input the list of doubly-occupied orbitals (sym(i),rmo(i),i=1,ndot):
 number of doubly-occupied orbitals:     14
 number of inactive electrons:     28
 number of active electrons:      3
 level(*)        1   2   3   4   5   6   7   8   9  10  11  12  13  14
 symd(*)         1   1   1   1   1   1   2   2   3   3   3   3   4   4
 slabel(*)     a1  a1  a1  a1  a1  a1  b1  b1  b2  b2  b2  b2  a2  a2 
 doub(*)         1   2   3   4   5   6   1   2   1   2   3   4   1   2
 
 input the active orbitals (sym(i),rmo(i),i=1,nact):
 nact:      4
 level(*)        1   2   3   4
 syml(*)         1   1   3   3
 slabel(*)     a1  a1  b2  b2 
 modrt(*)        7   8   5   6
 input the minimum cumulative occupation for each active level:
  a1  a1  b2  b2 
    7   8   5   6
 input the maximum cumulative occupation for each active level:
  a1  a1  b2  b2 
    7   8   5   6
 slabel(*)     a1  a1  b2  b2 
 modrt(*)        7   8   5   6
 occmin(*)       0   0   0   3
 occmax(*)       3   3   3   3
 input the minimum b value for each active level:
  a1  a1  b2  b2 
    7   8   5   6
 input the maximum b value for each active level:
  a1  a1  b2  b2 
    7   8   5   6
 slabel(*)     a1  a1  b2  b2 
 modrt(*)        7   8   5   6
 bmin(*)         0   0   0   0
 bmax(*)         3   3   3   3
 input the step masks for each active level:
 modrt:smask=
   7:1111   8:1111   5:1111   6:1111
 input the number of vertices to be deleted [  0]: number of vertices to be removed (a priori):      0
 number of rows in the drt:     14
 are any arcs to be manually removed?(y,[n])
 nwalk=      10
 input the range of drt levels to print (l1,l2):
 levprt(*)       0   4

 level  0 through level  4 of the drt:

 row lev a b syml lab rmo  l0  l1  l2  l3 isym xbar   y0    y1    y2    xp     z

  14   4 1 1   3 b2    6    0   0   0   0   1     1     0     0     0    10     0
                                            2     0     0     0     0     0     0
                                            3     0     0     0     0    10     0
                                            4     0     0     0     0     0     0
 ........................................

  10   3 1 1   3 b2    5   14   0   0   0   1     1     0     0     0     4     0
                                            2     0     0     0     0     0     0
                                            3     0     0     0     0     4     0
                                            4     0     0     0     0     0     0

  11   3 1 0   3 b2    5    0  14   0   0   1     0     0     0     0     4     0
                                            2     0     0     0     0     0     0
                                            3     1     1     0     0     2     4
                                            4     0     0     0     0     0     0

  12   3 0 2   3 b2    5    0   0  14   0   1     0     0     0     0     1     0
                                            2     0     0     0     0     0     0
                                            3     1     1     1     0     2     6
                                            4     0     0     0     0     0     0

  13   3 0 1   3 b2    5    0   0   0  14   1     1     1     1     1     2     8
                                            2     0     0     0     0     0     0
                                            3     0     0     0     0     1     0
                                            4     0     0     0     0     0     0
 ........................................

   5   2 1 1   1 a1    8   10   0   0   0   1     1     0     0     0     2     0
                                            2     0     0     0     0     0     0
                                            3     0     0     0     0     0     0
                                            4     0     0     0     0     0     0

   6   2 1 0   1 a1    8   11  10   0   0   1     0     0     0     0     3     0
                                            2     0     0     0     0     0     0
                                            3     2     1     0     0     0     2
                                            4     0     0     0     0     0     0

   7   2 0 2   1 a1    8   12   0  10   0   1     0     0     0     0     1     0
                                            2     0     0     0     0     0     0
                                            3     2     1     1     0     0     2
                                            4     0     0     0     0     0     0

   8   2 0 1   1 a1    8   13  12  11  10   1     4     3     2     1     2     2
                                            2     0     0     0     0     0     0
                                            3     0     0     0     0     0     0
                                            4     0     0     0     0     0     0

   9   2 0 0   1 a1    8    0  13   0  11   1     0     0     0     0     1     0
                                            2     0     0     0     0     0     0
                                            3     2     2     1     1     0     6
                                            4     0     0     0     0     0     0
 ........................................

   2   1 1 0   1 a1    7    6   5   0   0   1     1     1     0     0     1     0
                                            2     0     0     0     0     0     0
                                            3     2     0     0     0     0     2
                                            4     0     0     0     0     0     0

   3   1 0 1   1 a1    7    8   7   6   5   1     5     1     1     1     1     1
                                            2     0     0     0     0     0     0
                                            3     4     4     2     0     0     2
                                            4     0     0     0     0     0     0

   4   1 0 0   1 a1    7    9   8   0   6   1     4     4     0     0     1     3
                                            2     0     0     0     0     0     0
                                            3     4     2     2     2     0     2
                                            4     0     0     0     0     0     0
 ........................................

   1   0 0 0   0       0    4   3   0   2   1    10     6     1     1     1     0
                                            2     0     0     0     0     0     0
                                            3    10     6     2     2     0     2
                                            4     0     0     0     0     0     0
 ........................................

 initial csf selection step:
 total number of walks in the drt, nwalk=      10
 keep all of these walks?(y,[n]) individual walks will be generated from the drt.
 apply orbital-group occupation restrictions?(y,[n]) apply reference occupation restrictions?(y,[n]) manually select individual walks?(y,[n])
 step-vector based csf selection complete.
       10 csfs selected from      10 total walks.

 beginning step-vector based csf selection.
 enter [step_vector/disposition] pairs:

 enter the active orbital step vector, (-1/ to end):

 step-vector based csf selection complete.
       10 csfs selected from      10 total walks.

 beginning numerical walk selection:
 enter positive walk numbers to add walks,
 negative walk numbers to delete walks, and zero to end.

 input walk number (0 to end) [  0]:
 final csf selection complete.
       10 csfs selected from      10 total walks.
  drt construction and csf selection complete.
 
 input a title card, default=mdrt2_title
  title                                                                         
  
 input a drt file name, default=mcdrtfl
 drt and indexing arrays written to file:
 /scratch/runlocal/WORK/mcdrtfl                                                  
 
 write the drt file?([y],n) include step(*) vectors?([y],n) drt file is being written...


   List of selected configurations (step vectors)


   CSF#     1    3 1 0 0
   CSF#     2    1 3 0 0
   CSF#     3    1 0 3 0
   CSF#     4    1 0 2 1
   CSF#     5    1 0 1 2
   CSF#     6    1 0 0 3
   CSF#     7    0 1 3 0
   CSF#     8    0 1 2 1
   CSF#     9    0 1 1 2
   CSF#    10    0 1 0 3
