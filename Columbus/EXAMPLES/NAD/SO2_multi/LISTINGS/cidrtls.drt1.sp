 
 program cidrt 7.0  

 distinct row table construction, reference csf selection, and internal
 walk selection for multireference single- and double-excitation
configuration interaction.

 references:  r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).
              h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. symp. 15, 91 (1981).

 based on the initial version by  Ron Shepard

 extended for spin-orbit CI calculations ( Russ Pitzer, OSU)

 and large active spaces (Thomas Müller, FZ(21 Juelich)

 This Version of Program CIDRT is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de

*********************** File revision status: ***********************
* cidrt1.F9 Revision: 1.1.2.12          Date: 2012/03/28 23:01:46   * 
* cidrt2.F9 Revision: 1.1.2.21          Date: 2012/05/23 12:42:07   * 
* cidrt3.F9 Revision: 1.1.2.3           Date: 2012/02/08 18:18:52   * 
* cidrt4.F9 Revision: 1.1.2.3           Date: 2012/02/08 18:18:52   * 
********************************************************************

 workspace allocation parameters: lencor= 301465600 mem1=         0 ifirst=         1
 expanded "keystrokes" are being written to file:
 /fhgfs/global/lv70151/plasser1/1684021/WORK/cidrtky                             
 Spin-Orbit CI Calculation?(y,[n])
 Spin-Free Calculation
 
 input the spin multiplicity [  0]:
 spin multiplicity, smult            :   1    singlet 
 input the total number of electrons [  0]:
 total number of electrons, nelt     :    32
 input the number of irreps (1:8) [  0]:
 point group dimension, nsym         :     1
 enter symmetry labels:(y,[n])
 enter 1 labels (a4):
 enter symmetry label, default=   1
 symmetry labels: (symmetry, slabel)
 ( 1,  a  ) 
 input nmpsy(*):
 nmpsy(*)=        46
 
   symmetry block summary
 block(*)=         1
 slabel(*)=      a  
 nmpsy(*)=        46
 
 total molecular orbitals            :    46
 input the molecular spatial symmetry (irrep 1:nsym) [  0]:
 state spatial symmetry label        :  a  
 
 input the frozen core orbitals (sym(i),rmo(i),i=1,nfct):
 total frozen core orbitals, nfct    :     7
 
 fcorb(*)=         1   2   3   4   5   6   7
 slabel(*)=      a   a   a   a   a   a   a  
 
 number of frozen core orbitals      :     7
 number of frozen core electrons     :    14
 number of internal electrons        :    18
 
 input the frozen virtual orbitals (sym(i),rmo(i),i=1,nfvt):
 total frozen virtual orbitals, nfvt :     0

 no frozen virtual orbitals entered
 
 input the internal orbitals (sym(i),rmo(i),i=1,niot):
 niot                                :    11
 
 modrt(*)=         8   9  10  11  12  13  14  15  16  17  18
 slabel(*)=      a   a   a   a   a   a   a   a   a   a   a  
 
 total number of orbitals            :    46
 number of frozen core orbitals      :     7
 number of frozen virtual orbitals   :     0
 number of internal orbitals         :    11
 number of external orbitals         :    28
 
 orbital-to-level mapping vector
 map(*)=          -1  -1  -1  -1  -1  -1  -1  29  30  31  32  33  34  35  36
                  37  38  39   1   2   3   4   5   6   7   8   9  10  11  12
                  13  14  15  16  17  18  19  20  21  22  23  24  25  26  27
                  28
 
 input the number of ref-csf doubly-occupied orbitals [  0]:
 (ref) doubly-occupied orbitals      :     6
 
 no. of internal orbitals            :    11
 no. of doubly-occ. (ref) orbitals   :     6
 no. active (ref) orbitals           :     5
 no. of active electrons             :     6
 
 input the active-orbital, active-electron occmnr(*):
  14 15 16 17 18
 input the active-orbital, active-electron occmxr(*):
  14 15 16 17 18
 
 actmo(*) =       14  15  16  17  18
 occmnr(*)=        0   0   0   0   6
 occmxr(*)=        6   6   6   6   6
 reference csf cumulative electron occupations:
 modrt(*)=         8   9  10  11  12  13  14  15  16  17  18
 occmnr(*)=        2   4   6   8  10  12  12  12  12  12  18
 occmxr(*)=        2   4   6   8  10  12  18  18  18  18  18
 
 input the active-orbital bminr(*):
  14 15 16 17 18
 input the active-orbital bmaxr(*):
  14 15 16 17 18
 reference csf b-value constraints:
 modrt(*)=         8   9  10  11  12  13  14  15  16  17  18
 bminr(*)=         0   0   0   0   0   0   0   0   0   0   0
 bmaxr(*)=         0   0   0   0   0   0   6   6   6   6   6
 input the active orbital smaskr(*):
  14 15 16 17 18
 modrt:smaskr=
   8:1000   9:1000  10:1000  11:1000  12:1000  13:1000  14:1111  15:1111
  16:1111  17:1111  18:1111
 
 input the maximum excitation level from the reference csfs [  2]:
 maximum excitation from ref. csfs:  :     1
 number of internal electrons:       :    18
 
 input the internal-orbital mrsdci occmin(*):
   8  9 10 11 12 13 14 15 16 17 18
 input the internal-orbital mrsdci occmax(*):
   8  9 10 11 12 13 14 15 16 17 18
 mrsdci csf cumulative electron occupations:
 modrt(*)=         8   9  10  11  12  13  14  15  16  17  18
 occmin(*)=        1   3   5   7   9  11  11  11  11  11  17
 occmax(*)=       18  18  18  18  18  18  18  18  18  18  18
 
 input the internal-orbital mrsdci bmin(*):
   8  9 10 11 12 13 14 15 16 17 18
 input the internal-orbital mrsdci bmax(*):
   8  9 10 11 12 13 14 15 16 17 18
 mrsdci b-value constraints:
 modrt(*)=         8   9  10  11  12  13  14  15  16  17  18
 bmin(*)=          0   0   0   0   0   0   0   0   0   0   0
 bmax(*)=         18  18  18  18  18  18  18  18  18  18  18
 
 input the internal-orbital smask(*):
   8  9 10 11 12 13 14 15 16 17 18
 modrt:smask=
   8:1111   9:1111  10:1111  11:1111  12:1111  13:1111  14:1111  15:1111
  16:1111  17:1111  18:1111
 
 internal orbital summary:
 block(*)=         1   1   1   1   1   1   1   1   1   1   1
 slabel(*)=      a   a   a   a   a   a   a   a   a   a   a  
 rmo(*)=           8   9  10  11  12  13  14  15  16  17  18
 modrt(*)=         8   9  10  11  12  13  14  15  16  17  18
 
 reference csf info:
 occmnr(*)=        2   4   6   8  10  12  12  12  12  12  18
 occmxr(*)=        2   4   6   8  10  12  18  18  18  18  18
 
 bminr(*)=         0   0   0   0   0   0   0   0   0   0   0
 bmaxr(*)=         0   0   0   0   0   0   6   6   6   6   6
 
 
 mrsdci csf info:
 occmin(*)=        1   3   5   7   9  11  11  11  11  11  17
 occmax(*)=       18  18  18  18  18  18  18  18  18  18  18
 
 bmin(*)=          0   0   0   0   0   0   0   0   0   0   0
 bmax(*)=         18  18  18  18  18  18  18  18  18  18  18
 

 a priori removal of distinct rows:

 input the level, a, and b values for the vertices 
 to be removed (-1/ to end).

 input level, a, and b (-1/ to end):
 no vertices marked for removal
 
 impose generalized interacting space restrictions?(y,[n])
 generalized interacting space restrictions will not be imposed.
 multp(*)=
  hmult                     0
 lxyzir   0   0   0
 symmetry of spin functions (spnir)
       --------------------------Ms ----------------------------
   S     1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19
   1     1  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   2     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   3     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   4     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   5     1  0  0  0  1  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   6     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   7     0  0  0  1  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   8     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   9     1  0  0  0  1  0  0  0  1  0  0  0  0  0  0  0  0  0  0
  10     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  11     0  0  0  1  0  0  0  1  0  0  0  0  0  0  0  0  0  0  0
  12     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  13     1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  0  0  0
  14     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  15     0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  0  0  0  0
  16     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  17     1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0
  18     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  19     0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0

 number of rows in the drt :  88

 manual arc removal step:


 input the level, a, b, and step values 
 for the arcs to be removed (-1/ to end).

 input the level, a, b, and step (-1/ to end):
 remarc:   0 arcs removed out of   0 specified.

 xbarz=         290
 xbary=        2235
 xbarx=        7584
 xbarw=        5225
        --------
 nwalk=       15334
 input the range of drt levels to print (l1,l2):
 levprt(*)        -1   0

 reference-csf selection step 1:
 total number of z-walks in the drt, nzwalk=     290

 input the list of allowed reference symmetries:
 allowed reference symmetries:             1
 allowed reference symmetry labels:      a  
 keep all of the z-walks as references?(y,[n])
 all z-walks are initially deleted.
 
 generate walks while applying reference drt restrictions?([y],n)
 reference drt restrictions will be imposed on the z-walks.
 
 impose additional orbital-group occupation restrictions?(y,[n])
 
 apply primary reference occupation restrictions?(y,[n])
 
 manually select individual walks?(y,[n])

 step 1 reference csf selection complete.
       50 csfs initially selected from     290 total walks.

 beginning step-vector based selection.
 enter [internal_orbital_step_vector/disposition] pairs:

 enter internal orbital step vector, (-1/ to end):
   8  9 10 11 12 13 14 15 16 17 18

 step 2 reference csf selection complete.
       50 csfs currently selected from     290 total walks.

 beginning numerical walk based selection.
 enter positive walk numbers to add walks,
 negative walk numbers to delete walks, and zero to end:

 input reference walk number (0 to end) [  0]:

 numerical walk-number based selection complete.
       50 reference csfs selected from     290 total z-walks.
 
 input the reference occupations, mu(*):
 reference occupations:
 mu(*)=            2   2   2   2   2   2   0   0   0   0   0
 
 number of step vectors saved:     50

 exlimw: beginning excitation-based walk selection...
 exlimw: nref=                    50

  number of valid internal walks of each symmetry:

       a  
      ----
 z     290
 y     645
 x       0
 w       0

 csfs grouped by internal walk symmetry:

       a  
      ----
 z     290
 y   18060
 x       0
 w       0

 total csf counts:
 z-vertex:      290
 y-vertex:    18060
 x-vertex:        0
 w-vertex:        0
           --------
 total:       18350
 
 this is an obsolete prompt.(y,[n])

 final mrsdci walk selection step:

 nvalw(*)=     290     645       0       0 nvalwt=     935

 enter positive walk numbers to add walks,
 negative walk numbers to delete walks, and zero to end.

 input mrsdci walk number (0 to end) [  0]:

 end of manual mrsdci walk selection.
 number added=   0 number removed=   0

 nvalw(*)=     290     645       0       0 nvalwt=     935

 lprune input numv1,nwalk=                   935                 15334
 lprune input xbar(1,1),nref=                   290                    50

 lprune: l(*,*,*) pruned with nwalk=   15334 nvalwt=     935= 290 645   0   0
 lprune:  z-drt, nprune=   160
 lprune:  y-drt, nprune=   132
 lprune: wx-drt, nprune=   226

 xbarz=         290
 xbary=         645
 xbarx=           0
 xbarw=           0
        --------
 nwalk=         935
 levprt(*)        -1   0

 beginning the reference csf index recomputation...

     iref   iwalk  step-vector
   ------  ------  ------------
        1       1  33333333300
        2       2  33333333120
        3       3  33333333102
        4       4  33333333030
        5       5  33333333012
        6       6  33333333003
        7       7  33333331320
        8       8  33333331302
        9       9  33333331230
       10      10  33333331212
       11      11  33333331203
       12      12  33333331122
       13      13  33333331032
       14      14  33333331023
       15      15  33333330330
       16      16  33333330312
       17      17  33333330303
       18      18  33333330132
       19      19  33333330123
       20      20  33333330033
       21      21  33333313320
       22      22  33333313302
       23      23  33333313230
       24      24  33333313212
       25      25  33333313203
       26      26  33333313122
       27      27  33333313032
       28      28  33333313023
       29      29  33333312330
       30      30  33333312312
       31      31  33333312303
       32      32  33333312132
       33      33  33333312123
       34      34  33333312033
       35      35  33333311322
       36      36  33333311232
       37      37  33333311223
       38      38  33333310332
       39      39  33333310323
       40      40  33333310233
       41      41  33333303330
       42      42  33333303312
       43      43  33333303303
       44      44  33333303132
       45      45  33333303123
       46      46  33333303033
       47      47  33333301332
       48      48  33333301323
       49      49  33333301233
       50      50  33333300333
 indx01:    50 elements set in vec01(*)

 beginning the valid upper walk index recomputation...
 indx01:   935 elements set in vec01(*)

 beginning the final csym(*) computation...

  number of valid internal walks of each symmetry:

       a  
      ----
 z     290
 y     645
 x       0
 w       0

 csfs grouped by internal walk symmetry:

       a  
      ----
 z     290
 y   18060
 x       0
 w       0

 total csf counts:
 z-vertex:      290
 y-vertex:    18060
 x-vertex:        0
 w-vertex:        0
           --------
 total:       18350
 
 input a title card, default=cidrt_title
 title card:
  cidrt_title                                                                   
  
 
 input a drt file name, default=cidrtfl
 drt and indexing arrays will be written to file:
 /fhgfs/global/lv70151/plasser1/1684021/WORK/cidrtfl                             
 
 write the drt file?([y],n)
 drt file is being written...
 wrtstr:  a  
nwalk=     935 cpos=     117 maxval=    9 cmprfactor=   87.49 %.
nwalk=     935 cpos=      10 maxval=   99 cmprfactor=   97.86 %.
nwalk=     935 cpos=       1 maxval=  999 cmprfactor=   99.68 %.
nwalk=     935 cpos=       1 maxval= 9999 cmprfactor=   99.57 %.
 compressed with: nwalk=     935 cpos=       1 maxval=  999 cmprfactor=   99.68 %.
initial index vector length:       935
compressed index vector length:         1reduction:  99.89%
nwalk=     290 cpos=      37 maxval=    9 cmprfactor=   87.24 %.
nwalk=     290 cpos=       4 maxval=   99 cmprfactor=   97.24 %.
nwalk=     290 cpos=       2 maxval=  999 cmprfactor=   97.93 %.
 compressed with: nwalk=     290 cpos=       4 maxval=   99 cmprfactor=   97.24 %.
initial ref vector length:       290
compressed ref vector length:         4reduction:  98.62%
