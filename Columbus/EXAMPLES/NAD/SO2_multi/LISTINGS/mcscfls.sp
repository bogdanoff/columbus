

     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 This program allows the csf mixing coefficient and orbital expansion coefficient
 optimization using the graphical unitary group approach and the exponential
 operator mcscf method.
 references:  r. shepard and j. simons, ' int. j. quantum chem. symp. 14, 211 (1980).
              r. shepard, i. shavitt, and j. simons, j. chem. phys. 76, 543 (1982).
              r. shepard in "ab initio methods in quantum chemistry ii" advances in chemical
                  physics 69, edited by k. p. lawley (wiley, new york, 1987) pp. 63-200.
 Original autor: Ron Shepard, ANL
 Later revisions: Michal Dallos, University Vienna

 This Version of Program MCSCF is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.4.0.2     **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 Workspace allocation information:
       301465600 of real*8 words ( 2300.00 MB) of work space has been allocated.

 user input information:

 ======== echo of the mcscf input ========
 ------------------------------------------------------------------------
  &input
   niter=200,
   nmiter=50,
   nciitr=300,
   tol(3)=1.e-4,
   tol(2)=1.e-4,
   tol(1)=1.e-8,
   NSTATE=0,
   npath=1,3,9,10,13,17,19,21,-11,12, 2,30
   ncoupl=5,
   tol(9)=1.e-3,
   FCIORB=  1,14,20,1,15,20,1,16,20,1,17,20,1,18,20
   NAVST(1) = 3,
   WAVST(1,1)=1 ,
   WAVST(1,2)=1 ,
   WAVST(1,3)=1 ,
   NAVST(2) = 3,
   WAVST(2,1)=1 ,
   WAVST(2,2)=1 ,
   WAVST(2,3)=1 ,
  &end
 ------------------------------------------------------------------------


 ***  Integral file informations  ***


 input integral file : /fhgfs/global/lv70151/plasser1/1684021/WORK/aoints       
    

 Integral file header information:
 Hermit Integral Program : SIFS version  r24n12            15:26:06.808 10-Sep-12

 Core type energy values:
 energy( 1)=  9.781323324553E+01, ietype=   -1,    core energy of type: Nuc.Rep.
 total ao core energy =   97.813233246


   ******  Basis set information:  ******

 Number of irreps:                  1
 Total number of basis functions:  46

 irrep no.              1
 irrep label           A  
 no. of bas.fcions.    46


 ***  MCSCF optimization procedure parmeters:  ***


 maximum number of mcscf iterations:        niter=   200

 maximum number of psci micro-iterations:   nmiter=   50
 maximum r,s subspace dimension allowed:    nvrsmx=   30

 tol(1)=  1.0000E-08. . . . delta-emc convergence criterion.
 tol(2)=  1.0000E-04. . . . wnorm convergence criterion.
 tol(3)=  1.0000E-04. . . . knorm convergence criterion.
 tol(4)=  1.0000E-08. . . . apxde convergence criterion.
 tol(5)=  1.0000E-04. . . . small diagonal matrix element tolerance.
 tol(6)=  1.0000E-06. . . . minimum ci-psci residual norm.
 tol(7)=  1.0000E-05. . . . maximum ci-psci residual norm.
 tol(8)=  1.0000E+00. . . . maximum abs(k(xy)) allowed.
 tol(9)=  1.0000E-03. . . . wnorm coupling tolerance.
 tol(10)= 0.0000E+00. . . . maximum psci emergency shift parameter.
 tol(11)= 0.0000E+00. . . . minimum psci emergency shift parameter.
 tol(12)= 0.0000E+00. . . . increment of psci emergency shift parameter.


 *** State averaging informations: ***


 MCSCF calculation performed for  2 DRTs.

 DRT  first state   no.of aver.states   weights
  1   ground state          3             0.167 0.167 0.167
  2   ground state          3             0.167 0.167 0.167

 The number of hmc(*) eigenvalues and eigenvectors calculated each iteration per DRT:
 DRT.   no.of eigenv.(=ncol)
    1        4
    2        4

 Orbitals included in invariant subspaces:
   symmetry   orbital   mask
       1      14( 14)    20
       1      15( 15)    20
       1      16( 16)    20
       1      17( 17)    20
       1      18( 18)    20

 npath(*) options:
  2:  orbital-state coupling terms will be included beginning on iteration ncoupl=  5
  3:  print intermediate timing information.
  9:  suppress the drt listing.
 10:  suppress the hmc(*) eigenvector listing.
 12:  diagonalize the hmc(*) matrix iteratively.
        nunitv= 1 nciitr=** mxvadd=20 nvcimx=20
       rtolci(*),wnorm=     1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 0.0000E+00
   noldv =   0  0
 13:  get initial orbitals from the formatted file, mocoef.
 17:  print the final natural orbitals and occupations.
 19:  transform the virtual orbitals to diagonalize qvv(*).
 21:  write out the one- and two- electron density for further use (files:mcd1fl, mcd2fl).
 30:  Compute mcscf (transition) density matrices


   ******  DRT info section  ******


 Informations for the DRT no.  1

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a  
 Total number of electrons:   32
 Spin multiplicity:            1
 Number of active orbitals:    5
 Number of active electrons:   6
 Total number of CSFs:        50

 Informations for the DRT no.  2

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a  
 Total number of electrons:   32
 Spin multiplicity:            3
 Number of active orbitals:    5
 Number of active electrons:   6
 Total number of CSFs:        45
 

 faar:   0 active-active rotations allowed out of:  10 possible.


 Number of active-double rotations:        65
 Number of active-active rotations:         0
 Number of double-virtual rotations:      364
 Number of active-virtual rotations:      140
 lenbfsdef=                131071  lenbfs=                   784
  number of integrals per class 1:11 (cf adda 
 class  1 (pq|rs):         #         120
 class  2 (pq|ri):         #         975
 class  3 (pq|ia):         #        5460
 class  4 (pi|qa):         #        9100
 class  5 (pq|ra):         #        2100
 class  6 (pq|ij)/(pi|qj): #        3900
 class  7 (pq|ab):         #        6090
 class  8 (pa|qb):         #       11760
 class  9 p(bp,ai)         #       50960
 class 10p(ai,jp):        #       23660
 class 11p(ai,bj):        #       71344

 Size of orbital-Hessian matrix B:                   169359
 Size of the orbital-state Hessian matrix C:         162165
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:         331524


 Source of the initial MO coeficients:

 Input MO coefficient file: /fhgfs/global/lv70151/plasser1/1684021/WORK/mocoef          
 

               starting mcscf iteration...   1

 orbital-state coupling will not be calculated this iteration.

 *** Starting integral transformation ***

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     1, naopsy(1) =    46, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 64959, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 65000

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore= 301456084

 inoutp: segmentation information:
 in-core transformation space,   avcinc = 301284244
 address segment size,           sizesg = 301132758
 number of in-core blocks,       nincbk =         1
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:     273127 transformed 1/r12    array elements were written in      51 records.


 mosort: allocated sort2 space, avc2is=   301317937 available sort2 space, avcisx=   301318189

 trial vectors are generated internally.

 trial vector  1 is unit matrix column     7
 ciiter=  22 noldhv=  5 noldv=  5

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -547.1904446738     -645.0036779193        0.0000007569        0.0000010000
    2      -547.1403465658     -644.9535798114        0.0000001402        0.0000010000
    3      -547.0135383931     -644.8267716386        0.0000005519        0.0000010000
    4      -546.9776678543     -644.7909010999        0.0034908805        0.0100000000

 trial vectors are generated internally.

 trial vector  1 is unit matrix column    16
 ciiter=  32 noldhv= 15 noldv= 15

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -547.1456443327     -644.9588775782        0.0000001960        0.0000010000
    2      -547.1387850465     -644.9520182920        0.0000000271        0.0000010000
    3      -547.0431326694     -644.8563659149        0.0000004426        0.0000010000
    4      -546.9366675937     -644.7499008392        0.0028644684        0.0100000000
 
  tol(10)=  0.000000000000000E+000  eshsci=  6.032034520890791E-008
 Total number of micro iterations:    2

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 1.00000000 pnorm= 0.0000E+00 rznorm= 1.7704E-07 rpnorm= 0.0000E+00 noldr=  2 nnewr=  2 nolds=  0 nnews=  0
 

 fdd(*) eigenvalues. symmetry block  1
  -184.262030  -41.298915  -41.245110  -18.246684  -13.608427  -13.604247  -13.600989   -2.865178   -2.608947   -1.773348
    -1.248403   -1.216296   -1.201050

 qvv(*) eigenvalues. symmetry block  1
     0.409321    1.096278    1.207553    1.231088    1.358850    1.482896    1.624033    1.625049    1.823081    2.022284
     2.305244    2.433775    2.474311    2.543523    2.666101    2.889829    3.709504    4.073409    5.794520    5.800781
     5.804673    5.809778    5.837566    5.928181    6.155085    6.288989    6.489419    7.220620

 restrt: restart information saved on the restart file (unit= 13).

 not all mcscf convergence criteria are satisfied.
 iter=    1 emc=   -547.1119819469 demc= 5.4711E+02 wnorm= 4.8256E-07 knorm= 1.0005E-06 apxde= 8.2134E-14    *not conv.*     

               starting mcscf iteration...   2

 orbital-state coupling will not be calculated this iteration.

 *** Starting integral transformation ***

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     1, naopsy(1) =    46, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 64959, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 65000

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore= 301456084

 inoutp: segmentation information:
 in-core transformation space,   avcinc = 301284244
 address segment size,           sizesg = 301132758
 number of in-core blocks,       nincbk =         1
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:     274605 transformed 1/r12    array elements were written in      51 records.


 mosort: allocated sort2 space, avc2is=   301317937 available sort2 space, avcisx=   301318189

   4 trial vectors read from nvfile (unit= 29).
 ciiter=   1 noldhv=  4 noldv=  4

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -547.1904446665     -645.0036779121        0.0000008179        0.0000010000
    2      -547.1403465662     -644.9535798118        0.0000004662        0.0000010000
    3      -547.0135383959     -644.8267716414        0.0000007207        0.0000010000
    4      -546.9776678390     -644.7909010845        0.0034908586        0.0100000000

   4 trial vectors read from nvfile (unit= 29).
 ciiter=   1 noldhv=  4 noldv=  4

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -547.1456443344     -644.9588775799        0.0000004369        0.0000010000
    2      -547.1387850442     -644.9520182898        0.0000002991        0.0000010000
    3      -547.0431326739     -644.8563659195        0.0000005715        0.0000010000
    4      -546.9366676047     -644.7499008503        0.0028644186        0.0100000000
 
  tol(10)=  0.000000000000000E+000  eshsci=  4.811894943246538E-008
 Total number of micro iterations:    2

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 1.00000000 pnorm= 0.0000E+00 rznorm= 2.1503E-07 rpnorm= 0.0000E+00 noldr=  2 nnewr=  2 nolds=  0 nnews=  0
 

 fdd(*) eigenvalues. symmetry block  1
  -184.262030  -41.298915  -41.245110  -18.246684  -13.608427  -13.604247  -13.600989   -2.865178   -2.608947   -1.773348
    -1.248403   -1.216296   -1.201050

 qvv(*) eigenvalues. symmetry block  1
     0.409321    1.096278    1.207553    1.231088    1.358850    1.482896    1.624033    1.625049    1.823081    2.022284
     2.305243    2.433775    2.474311    2.543523    2.666101    2.889829    3.709504    4.073409    5.794520    5.800781
     5.804673    5.809778    5.837566    5.928181    6.155085    6.288989    6.489419    7.220620

 restrt: restart information saved on the restart file (unit= 13).

 all mcscf convergence criteria are satisfied.

 final mcscf convergence values:
 iter=    2 emc=   -547.1119819469 demc=-6.8212E-13 wnorm= 3.8495E-07 knorm= 1.4402E-06 apxde= 1.3470E-13    *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 wt 0.167 total energy=     -547.190444667, rel. (eV)=   0.000000
   DRT #1 state # 2 wt 0.167 total energy=     -547.140346566, rel. (eV)=   1.363239
   DRT #1 state # 3 wt 0.167 total energy=     -547.013538396, rel. (eV)=   4.813867
   DRT #2 state # 1 wt 0.167 total energy=     -547.145644334, rel. (eV)=   1.219080
   DRT #2 state # 2 wt 0.167 total energy=     -547.138785044, rel. (eV)=   1.405730
   DRT #2 state # 3 wt 0.167 total energy=     -547.043132674, rel. (eV)=   4.008565
   ------------------------------------------------------------


 MO-coefficient print-out skipped (no flag 32)
 They may be found in the MOCOEF directory.

          natural orbitals of the final iteration,block  1    -  A  
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000
               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     1.74454769     1.66401850     1.62429775
               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
  occ(*)=     0.91876671     0.04836935     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   25        MO   26        MO   27        MO   28        MO   29        MO   30        MO   31        MO   32
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   33        MO   34        MO   35        MO   36        MO   37        MO   38        MO   39        MO   40
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   41        MO   42        MO   43        MO   44        MO   45        MO   46
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
 d1(*), fmc(*), and qmc(*) written to the 1-particle density matrix file.
        679 d2(*) elements written to the 2-particle density matrix file: mcd2fl                                                      
 Computing the requested mcscf (transition) density matrices (flag 30)
 Reading mcdenin ...
 Number of density matrices (ndens):                     3
 Number of unique bra states (ndbra):                     3
 qind: F
 (Transition) density matrices:
 d1(*) written to the 1-particle density matrix file.
        679 d2(*) elements written to the 2-particle density matrix file: mcsd2fl.drt1.st01                                           
 d1(*) written to the 1-particle density matrix file.
        678 d2(*) elements written to the 2-particle density matrix file: mcsd2fl.drt1.st02                                           
 d1(*) written to the 1-particle density matrix file.
        679 d2(*) elements written to the 2-particle density matrix file: mcsd2fl.drt1.st03                                           

          state spec. NOs: DRT 1, State  1

          block  1
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000
               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     1.99835788     1.93681254     1.72448032
               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
  occ(*)=     0.27727737     0.06307189     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   25        MO   26        MO   27        MO   28        MO   29        MO   30        MO   31        MO   32
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   33        MO   34        MO   35        MO   36        MO   37        MO   38        MO   39        MO   40
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   41        MO   42        MO   43        MO   44        MO   45        MO   46
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

          state spec. NOs: DRT 1, State  2

          block  1
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000
               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     1.99461433     1.94644375     1.00484357
               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
  occ(*)=     1.00006918     0.05402917     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   25        MO   26        MO   27        MO   28        MO   29        MO   30        MO   31        MO   32
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   33        MO   34        MO   35        MO   36        MO   37        MO   38        MO   39        MO   40
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   41        MO   42        MO   43        MO   44        MO   45        MO   46
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

          state spec. NOs: DRT 1, State  3

          block  1
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000
               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     1.99292445     1.98126413     1.01253008
               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
  occ(*)=     0.99163458     0.02164676     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   25        MO   26        MO   27        MO   28        MO   29        MO   30        MO   31        MO   32
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   33        MO   34        MO   35        MO   36        MO   37        MO   38        MO   39        MO   40
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   41        MO   42        MO   43        MO   44        MO   45        MO   46
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        A   partial gross atomic populations
   ao class       1A         2A         3A         4A         5A         6A  
     1_ s       2.000004   0.000087   0.000166   2.000501   0.000003   0.000131
     1_ p       0.000000   0.000149   0.000236   0.000134   2.000012   1.999807
     1_ d       0.000000   0.000016   0.000015   0.000001   0.000003   0.000001
     3_ s       0.000000   1.999749   0.000000   0.000033  -0.000008   0.000002
     3_ p       0.000000   0.000001  -0.000001  -0.000093  -0.000018   0.000005
     3_ d       0.000000   0.000000   0.000000  -0.000003   0.000000   0.000000
     4_ s       0.000000   0.000000   1.999583  -0.000055  -0.000006   0.000019
     4_ p      -0.000003  -0.000001   0.000003  -0.000500  -0.000005   0.000020
     4_ d       0.000000   0.000000   0.000000  -0.000016   0.000019   0.000015
 
   ao class       7A         8A         9A        10A        11A        12A  
     1_ s       0.000000   0.404274   0.025390   1.170965   0.176470   0.006902
     1_ p       2.000010   0.106793   0.101726   0.095538   0.685057   0.362652
     1_ d       0.000000   0.029401   0.028118   0.001527   0.022187   0.052231
     3_ s       0.000000   0.151955   1.625195   0.122899   0.004016   0.039643
     3_ p       0.000002   0.009074   0.006456   0.037251   0.014609   0.082011
     3_ d       0.000000   0.000355   0.000428   0.000768   0.000436   0.000752
     4_ s       0.000000   1.206689   0.205503   0.337320   0.113662   0.009328
     4_ p      -0.000013   0.088624   0.007038   0.231820   0.979264   1.441654
     4_ d       0.000001   0.002834   0.000146   0.001912   0.004298   0.004826
 
   ao class      13A        14A        15A        16A        17A        18A  
     1_ s       0.000000   0.000000   0.004758   0.012087   0.000000   0.001477
     1_ p       0.863332   0.099156   0.013550   0.318176   0.472712   0.024097
     1_ d       0.034428   0.031832   0.018012   0.055223   0.017089  -0.000088
     3_ s       0.000000   0.000000   0.000009   0.014474   0.000000   0.000017
     3_ p       0.021729   1.419526   1.605859   0.988939   0.149746   0.019243
     3_ d       0.000509   0.001059   0.000576   0.002270   0.000040   0.000150
     4_ s       0.000000   0.000000  -0.000803  -0.000002   0.000000   0.000013
     4_ p       1.074297   0.193008   0.022033   0.233374   0.279462   0.003445
     4_ d       0.005705  -0.000033   0.000023  -0.000244  -0.000282   0.000018
 
   ao class      19A        20A        21A        22A        23A        24A  
 
   ao class      25A        26A        27A        28A        29A        30A  
 
   ao class      31A        32A        33A        34A        35A        36A  
 
   ao class      37A        38A        39A        40A        41A        42A  
 
   ao class      43A        44A        45A        46A  


                        gross atomic populations
     ao            1_         3_         4_
      s         5.803214   3.957983   3.871249
      p         9.143136   4.354339   4.553520
      d         0.289995   0.007341   0.019222
    total      15.236346   8.319662   8.443991
 

 Total number of electrons:   32.00000000

 Mulliken population for:
DRT 1, state 01


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        A   partial gross atomic populations
   ao class       1A         2A         3A         4A         5A         6A  
     1_ s       2.000004   0.000087   0.000166   2.000501   0.000003   0.000131
     1_ p       0.000000   0.000149   0.000236   0.000134   2.000012   1.999807
     1_ d       0.000000   0.000016   0.000015   0.000001   0.000003   0.000001
     3_ s       0.000000   1.999749   0.000000   0.000033  -0.000008   0.000002
     3_ p       0.000000   0.000001  -0.000001  -0.000093  -0.000018   0.000005
     3_ d       0.000000   0.000000   0.000000  -0.000003   0.000000   0.000000
     4_ s       0.000000   0.000000   1.999583  -0.000055  -0.000006   0.000019
     4_ p      -0.000003  -0.000001   0.000003  -0.000500  -0.000005   0.000020
     4_ d       0.000000   0.000000   0.000000  -0.000016   0.000019   0.000015
 
   ao class       7A         8A         9A        10A        11A        12A  
     1_ s       0.000000   0.404274   0.025390   1.170965   0.176470   0.006902
     1_ p       2.000010   0.106793   0.101726   0.095539   0.685057   0.362652
     1_ d       0.000000   0.029401   0.028118   0.001527   0.022187   0.052231
     3_ s       0.000000   0.151955   1.625195   0.122899   0.004016   0.039643
     3_ p       0.000002   0.009074   0.006456   0.037251   0.014609   0.082011
     3_ d       0.000000   0.000355   0.000428   0.000768   0.000436   0.000752
     4_ s       0.000000   1.206689   0.205503   0.337320   0.113662   0.009328
     4_ p      -0.000013   0.088624   0.007038   0.231820   0.979264   1.441654
     4_ d       0.000001   0.002834   0.000146   0.001912   0.004298   0.004826
 
   ao class      13A        14A        15A        16A        17A        18A  
     1_ s       0.000000   0.005418   0.013757   0.000000   0.000000   0.001956
     1_ p       0.863332   0.016132   0.406124   0.317521   0.107367   0.030555
     1_ d       0.034428   0.021574   0.066509   0.039898   0.003802  -0.000135
     3_ s       0.000000  -0.000001   0.018393   0.000000   0.000000  -0.000015
     3_ p       0.021729   1.927939   1.144304   0.982962   0.112761   0.026246
     3_ d       0.000509   0.000694   0.002885   0.001338  -0.000035   0.000189
     4_ s       0.000000  -0.000956  -0.000009   0.000000   0.000000   0.000016
     4_ p       1.074297   0.027530   0.285151   0.383030   0.053429   0.004234
     4_ d       0.005705   0.000027  -0.000301  -0.000269  -0.000047   0.000024
 
   ao class      19A        20A        21A        22A        23A        24A  
 
   ao class      25A        26A        27A        28A        29A        30A  
 
   ao class      31A        32A        33A        34A        35A        36A  
 
   ao class      37A        38A        39A        40A        41A        42A  
 
   ao class      43A        44A        45A        46A  


                        gross atomic populations
     ao            1_         3_         4_
      s         5.806024   3.961860   3.871094
      p         9.093144   4.365240   4.575572
      d         0.299575   0.008318   0.019174
    total      15.198744   8.335417   8.465839
 

 Total number of electrons:   32.00000000

 Mulliken population for:
DRT 1, state 02


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        A   partial gross atomic populations
   ao class       1A         2A         3A         4A         5A         6A  
     1_ s       2.000004   0.000087   0.000166   2.000501   0.000003   0.000131
     1_ p       0.000000   0.000149   0.000236   0.000134   2.000012   1.999807
     1_ d       0.000000   0.000016   0.000015   0.000001   0.000003   0.000001
     3_ s       0.000000   1.999749   0.000000   0.000033  -0.000008   0.000002
     3_ p       0.000000   0.000001  -0.000001  -0.000093  -0.000018   0.000005
     3_ d       0.000000   0.000000   0.000000  -0.000003   0.000000   0.000000
     4_ s       0.000000   0.000000   1.999583  -0.000055  -0.000006   0.000019
     4_ p      -0.000003  -0.000001   0.000003  -0.000500  -0.000005   0.000020
     4_ d       0.000000   0.000000   0.000000  -0.000016   0.000019   0.000015
 
   ao class       7A         8A         9A        10A        11A        12A  
     1_ s       0.000000   0.404274   0.025390   1.170965   0.176470   0.006902
     1_ p       2.000010   0.106793   0.101726   0.095539   0.685057   0.362652
     1_ d       0.000000   0.029401   0.028118   0.001527   0.022187   0.052231
     3_ s       0.000000   0.151955   1.625195   0.122899   0.004016   0.039643
     3_ p       0.000002   0.009074   0.006456   0.037251   0.014609   0.082011
     3_ d       0.000000   0.000355   0.000428   0.000768   0.000436   0.000752
     4_ s       0.000000   1.206689   0.205503   0.337320   0.113662   0.009328
     4_ p      -0.000013   0.088624   0.007038   0.231820   0.979264   1.441654
     4_ d       0.000001   0.002834   0.000146   0.001912   0.004298   0.004826
 
   ao class      13A        14A        15A        16A        17A        18A  
     1_ s       0.000000   0.000000   0.015018   0.000000   0.003072   0.001623
     1_ p       0.863332   0.004896   0.355967   0.571646   0.008298   0.027611
     1_ d       0.034428   0.028332   0.065427   0.022752   0.010879  -0.000081
     3_ s       0.000000   0.000000   0.016236   0.000000   0.000016   0.000049
     3_ p       0.021729   1.881654   1.218048   0.033473   0.965449   0.020561
     3_ d       0.000509   0.000801   0.002554   0.000250   0.000345   0.000172
     4_ s       0.000000   0.000000   0.000008   0.000000  -0.000489   0.000014
     4_ p       1.074297   0.078805   0.273469   0.377115   0.012485   0.004060
     4_ d       0.005705   0.000126  -0.000283  -0.000391   0.000015   0.000020
 
   ao class      19A        20A        21A        22A        23A        24A  
 
   ao class      25A        26A        27A        28A        29A        30A  
 
   ao class      31A        32A        33A        34A        35A        36A  
 
   ao class      37A        38A        39A        40A        41A        42A  
 
   ao class      43A        44A        45A        46A  


                        gross atomic populations
     ao            1_         3_         4_
      s         5.804606   3.959783   3.871575
      p         9.183863   4.290210   4.568132
      d         0.295237   0.007368   0.019227
    total      15.283706   8.257360   8.458934
 

 Total number of electrons:   32.00000000

 Mulliken population for:
DRT 1, state 03


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        A   partial gross atomic populations
   ao class       1A         2A         3A         4A         5A         6A  
     1_ s       2.000004   0.000087   0.000166   2.000501   0.000003   0.000131
     1_ p       0.000000   0.000149   0.000236   0.000134   2.000012   1.999807
     1_ d       0.000000   0.000016   0.000015   0.000001   0.000003   0.000001
     3_ s       0.000000   1.999749   0.000000   0.000033  -0.000008   0.000002
     3_ p       0.000000   0.000001  -0.000001  -0.000093  -0.000018   0.000005
     3_ d       0.000000   0.000000   0.000000  -0.000003   0.000000   0.000000
     4_ s       0.000000   0.000000   1.999583  -0.000055  -0.000006   0.000019
     4_ p      -0.000003  -0.000001   0.000003  -0.000500  -0.000005   0.000020
     4_ d       0.000000   0.000000   0.000000  -0.000016   0.000019   0.000015
 
   ao class       7A         8A         9A        10A        11A        12A  
     1_ s       0.000000   0.404274   0.025390   1.170965   0.176470   0.006902
     1_ p       2.000010   0.106793   0.101726   0.095539   0.685057   0.362652
     1_ d       0.000000   0.029401   0.028118   0.001527   0.022187   0.052231
     3_ s       0.000000   0.151955   1.625195   0.122899   0.004016   0.039643
     3_ p       0.000002   0.009074   0.006456   0.037251   0.014609   0.082011
     3_ d       0.000000   0.000355   0.000428   0.000768   0.000436   0.000752
     4_ s       0.000000   1.206689   0.205503   0.337320   0.113662   0.009328
     4_ p      -0.000013   0.088624   0.007038   0.231820   0.979264   1.441654
     4_ d       0.000001   0.002834   0.000146   0.001912   0.004298   0.004826
 
   ao class      13A        14A        15A        16A        17A        18A  
     1_ s       0.000000   0.005973   0.000000   0.000000   0.006169   0.000684
     1_ p       0.863332   0.017001   0.073140   0.541125   0.228127   0.010036
     1_ d       0.034428   0.021744   0.033912   0.019977   0.034293  -0.000054
     3_ s       0.000000   0.000035   0.000000   0.000000   0.010247  -0.000024
     3_ p       0.021729   1.923858   1.697168   0.121574   0.557643   0.009612
     3_ d       0.000509   0.000687   0.001101   0.000096   0.001622   0.000062
     4_ s       0.000000  -0.000975   0.000000   0.000000   0.000010   0.000006
     4_ p       1.074297   0.024573   0.175930   0.330095   0.153688   0.001317
     4_ d       0.005705   0.000031   0.000014  -0.000337  -0.000165   0.000008
 
   ao class      19A        20A        21A        22A        23A        24A  
 
   ao class      25A        26A        27A        28A        29A        30A  
 
   ao class      31A        32A        33A        34A        35A        36A  
 
   ao class      37A        38A        39A        40A        41A        42A  
 
   ao class      43A        44A        45A        46A  


                        gross atomic populations
     ao            1_         3_         4_
      s         5.797719   3.953741   3.871082
      p         9.084875   4.480881   4.507799
      d         0.277799   0.006813   0.019291
    total      15.160393   8.441436   8.398172
 

 Total number of electrons:   32.00000000

 !timer: mcscf                           cpu_time=     0.335 walltime=     0.778
 *** cpu_time / walltime =      0.430
 bummer (warning):timer: cpu_time << walltime.  If possible, increase core memory. event =                      1
