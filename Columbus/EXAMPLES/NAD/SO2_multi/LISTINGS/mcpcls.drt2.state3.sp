

     ******************************************
     **    PROGRAM:              MCPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 original author: Daniel Robertson, FSU
 later revisions: Ron Shepard, ANL;
                  Michal Dallos, University Vienna



 This Version of Program mcpc is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



   ******  File header section  ******

 Headers form the restart file:
    Hermit Integral Program : SIFS version  r24n12            15:26:06.808 10-Sep-12
     title                                                                          
     title                                                                          


   ******  DRT info section  ******

 Informations for the DRT no.  1
 Header form the DRT file: 
     title                                                                          
 Molecular symmetry group:   sym1
 Total number of electrons:   32
 Spin multiplicity:            1
 Number of active orbitals:    5
 Number of active electrons:   6
 Total number of CSFs:        50

   ***  Informations from the DRT number:   1

 
 Symmetry orbital summary:
 Symm.blocks:         1
 Symm.labels:         a  

 List of doubly occupied orbitals:
  1 a    2 a    3 a    4 a    5 a    6 a    7 a    8 a    9 a   10 a   11 a   12 a  
 13 a  

 List of active orbitals:
 14 a   15 a   16 a   17 a   18 a  

 Informations for the DRT no.  2
 Header form the DRT file: 
     title                                                                          
 Molecular symmetry group:    a  
 Total number of electrons:   32
 Spin multiplicity:            3
 Number of active orbitals:    5
 Number of active electrons:   6
 Total number of CSFs:        45

   ***  Informations from the DRT number:   2

 
 Symmetry orbital summary:
 Symm.blocks:         1
 Symm.labels:         a  

 List of doubly occupied orbitals:
  1 a    2 a    3 a    4 a    5 a    6 a    7 a    8 a    9 a   10 a   11 a   12 a  
 13 a  

 List of active orbitals:
 14 a   15 a   16 a   17 a   18 a  


   ******  MCSCF convergence information:  ******

 MCSCF convergence criteria were satisfied.

 mcscf energy=  -547.1119819469    nuclear repulsion=    97.8132332455
 demc=             0.0000000000    wnorm=                 0.0000003850
 knorm=            0.0000014402    apxde=                 0.0000000000


 MCSCF calculation performmed for   2 symmetries.

 State averaging:
 No,  ssym, navst, wavst
  1    a      3   0.1667 0.1667 0.1667
  2    a      3   0.1667 0.1667 0.1667

 Input the DRT No of interest: [  1]:
In the DRT No.: 2 there are  3 states.

 Which one to take? [  1]:
 The CSFs for the state No  3 of the symmetry sym  will be printed
 according to the following print options :

 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 9) export wave function files for cioverlap (all states).
 0) end.

 input menu number [  0]: csfs will be printed based on coefficient magnitudes.

 input the coefficient threshold (end with 0.) [ 0.0000]:
 List of active orbitals:
 14 a   15 a   16 a   17 a   18 a  

   csf       coeff       coeff**2    step(*)
  -----  ------------  ------------  ------------
      1 -0.9793336712  0.9590944396  33110
     23 -0.1597596472  0.0255231449  13031
     17  0.0697004792  0.0048581568  13301
     19  0.0513028957  0.0026319871  13130
     13  0.0467465196  0.0021852371  30311
      3 -0.0385214657  0.0014839033  33011
     40  0.0365247260  0.0013340556  03311
     42  0.0331324837  0.0010977615  03113
     15  0.0299256165  0.0008955425  30113
      4 -0.0227248307  0.0005164179  31310
     28  0.0131977314  0.0001741801  11330
     37  0.0104109745  0.0001083884  10331
     32 -0.0060728147  0.0000368791  11231
     12 -0.0040131328  0.0000161052  31013
     34 -0.0039737634  0.0000157908  11132
     39 -0.0037249782  0.0000138755  10133
     26 -0.0022248924  0.0000049501  12131
     22 -0.0020365569  0.0000041476  13103
      9  0.0018841183  0.0000035499  31112

 input the coefficient threshold (end with 0.) [ 0.0000]:
 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 9) export wave function files for cioverlap (all states).
 0) end.

 input menu number [  0]: