

     ******************************************
     **    PROGRAM:              MCPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 original author: Daniel Robertson, FSU
 later revisions: Ron Shepard, ANL;
                  Michal Dallos, University Vienna



 This Version of Program mcpc is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



   ******  File header section  ******

 Headers form the restart file:
    Hermit Integral Program : SIFS version  r24n12            15:26:06.808 10-Sep-12
     title                                                                          
     title                                                                          


   ******  DRT info section  ******

 Informations for the DRT no.  1
 Header form the DRT file: 
     title                                                                          
 Molecular symmetry group:   sym1
 Total number of electrons:   32
 Spin multiplicity:            1
 Number of active orbitals:    5
 Number of active electrons:   6
 Total number of CSFs:        50

   ***  Informations from the DRT number:   1

 
 Symmetry orbital summary:
 Symm.blocks:         1
 Symm.labels:         a  

 List of doubly occupied orbitals:
  1 a    2 a    3 a    4 a    5 a    6 a    7 a    8 a    9 a   10 a   11 a   12 a  
 13 a  

 List of active orbitals:
 14 a   15 a   16 a   17 a   18 a  

 Informations for the DRT no.  2
 Header form the DRT file: 
     title                                                                          
 Molecular symmetry group:    a  
 Total number of electrons:   32
 Spin multiplicity:            3
 Number of active orbitals:    5
 Number of active electrons:   6
 Total number of CSFs:        45

   ***  Informations from the DRT number:   2

 
 Symmetry orbital summary:
 Symm.blocks:         1
 Symm.labels:         a  

 List of doubly occupied orbitals:
  1 a    2 a    3 a    4 a    5 a    6 a    7 a    8 a    9 a   10 a   11 a   12 a  
 13 a  

 List of active orbitals:
 14 a   15 a   16 a   17 a   18 a  


   ******  MCSCF convergence information:  ******

 MCSCF convergence criteria were satisfied.

 mcscf energy=  -547.1119819469    nuclear repulsion=    97.8132332455
 demc=             0.0000000000    wnorm=                 0.0000003850
 knorm=            0.0000014402    apxde=                 0.0000000000


 MCSCF calculation performmed for   2 symmetries.

 State averaging:
 No,  ssym, navst, wavst
  1    a      3   0.1667 0.1667 0.1667
  2    a      3   0.1667 0.1667 0.1667

 Input the DRT No of interest: [  1]:
In the DRT No.: 1 there are  3 states.

 Which one to take? [  1]:
 The CSFs for the state No  2 of the symmetry  a   will be printed
 according to the following print options :

 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 9) export wave function files for cioverlap (all states).
 0) end.

 input menu number [  0]: csfs will be printed based on coefficient magnitudes.

 input the coefficient threshold (end with 0.) [ 0.0000]:
 List of active orbitals:
 14 a   15 a   16 a   17 a   18 a  

   csf       coeff       coeff**2    step(*)
  -----  ------------  ------------  ------------
      7 -0.9587152166  0.9191348665  31320
     29  0.2198482639  0.0483332591  12330
     14 -0.1387494321  0.0192514049  31023
     10 -0.0769769714  0.0059254541  31212
     32  0.0563586099  0.0031762929  12132
     34 -0.0551385401  0.0030402586  12033
     36 -0.0228719319  0.0005231253  11232
     48  0.0157805941  0.0002490271  01323
      2  0.0126935718  0.0001611268  33120
     23 -0.0110277230  0.0001216107  13230
     12  0.0066395723  0.0000440839  31122
     31 -0.0043535250  0.0000189532  12303
      5  0.0032626480  0.0000106449  33012
     27  0.0020972814  0.0000043986  13032
     38  0.0014234588  0.0000020262  10332
     22 -0.0013708474  0.0000018792  13302

 input the coefficient threshold (end with 0.) [ 0.0000]:
 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 9) export wave function files for cioverlap (all states).
 0) end.

 input menu number [  0]: