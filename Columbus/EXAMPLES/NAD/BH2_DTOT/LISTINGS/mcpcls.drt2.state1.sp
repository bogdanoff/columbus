

     ******************************************
     **    PROGRAM:              MCPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 original author: Daniel Robertson, FSU
 later revisions: Ron Shepard, ANL;
                  Michal Dallos, University Vienna



 This Version of Program mcpc is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



   ******  File header section  ******

 Headers form the restart file:
    Hermit Integral Program : SIFS version  hawk3.itc.univie. 15:42:27.405 18-Apr-11
     title                                                                          
     title                                                                          


   ******  DRT info section  ******

 Informations for the DRT no.  1
 Header form the DRT file: 
     title                                                                          
 Molecular symmetry group:   sym1
 Total number of electrons:    7
 Spin multiplicity:            2
 Number of active orbitals:    6
 Number of active electrons:   5
 Total number of CSFs:       115

   ***  Informations from the DRT number:   1

 
 Symmetry orbital summary:
 Symm.blocks:         1     2
 Symm.labels:         a'    a" 

 List of doubly occupied orbitals:
  1 a' 

 List of active orbitals:
  2 a'   3 a'   4 a'   5 a'   6 a'   1 a" 

 Informations for the DRT no.  2
 Header form the DRT file: 
     title                                                                          
 Molecular symmetry group:    a" 
 Total number of electrons:    7
 Spin multiplicity:            2
 Number of active orbitals:    6
 Number of active electrons:   5
 Total number of CSFs:        95

   ***  Informations from the DRT number:   2

 
 Symmetry orbital summary:
 Symm.blocks:         1     2
 Symm.labels:         a'    a" 

 List of doubly occupied orbitals:
  1 a' 

 List of active orbitals:
  2 a'   3 a'   4 a'   5 a'   6 a'   1 a" 


   ******  MCSCF convergence information:  ******

 MCSCF convergence criteria were satisfied.

 mcscf energy=   -25.6530063490    nuclear repulsion=     4.1965378929
 demc=             0.0000000000    wnorm=                 0.0000000605
 knorm=            0.0000000131    apxde=                 0.0000000000


 MCSCF calculation performmed for   2 symmetries.

 State averaging:
 No,  ssym, navst, wavst
  1    a'     2   0.3333 0.3333
  2    a"     1   0.3333

 Input the DRT No of interest: [  1]:
In the DRT No.: 2 there are  1 states.

 Which one to take? [  1]:
 The CSFs for the state No  1 of the symmetry  a"  will be printed
 according to the following print options :

 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 9) export wave function files for cioverlap (all states).
 0) end.

 input menu number [  0]: csfs will be printed based on coefficient magnitudes.

 input the coefficient threshold (end with 0.) [ 0.0000]:
 List of active orbitals:
  2 a'   3 a'   4 a'   5 a'   6 a'   1 a" 

   csf       coeff       coeff**2    step(*)
  -----  ------------  ------------  ------------
      2 -0.7572823132  0.5734765019  312001
      3  0.6118233880  0.3743278582  311002
      9  0.0956641955  0.0091516383  301201
     23  0.0804845644  0.0064777651  123001
     27  0.0758367342  0.0057512103  121012
     10  0.0648821673  0.0042096956  301102
      6  0.0603428248  0.0036412565  310021
     20  0.0566452998  0.0032086900  130102
     26  0.0508390005  0.0025846040  121021
     49  0.0489128000  0.0023924620  102121
     77  0.0459811172  0.0021142631  012031
     14 -0.0425967163  0.0018144802  300121
     55 -0.0402470679  0.0016198265  101122
     82 -0.0375720731  0.0014116607  011032
     38 -0.0328257876  0.0010775323  111022
     45 -0.0315220800  0.0009936415  103102
     62  0.0260716863  0.0006797328  031201
     72  0.0254010591  0.0006452138  013021
     74  0.0240360574  0.0005777321  012301
     78 -0.0234363702  0.0005492634  011302
     31 -0.0200377071  0.0004015097  120031
     54 -0.0196316306  0.0003854009  101212
     35 -0.0183171195  0.0003355169  112021
     63  0.0180975853  0.0003275226  031102
     73 -0.0178428984  0.0003183690  013012
     15  0.0171226842  0.0002931863  300112
     44  0.0164612597  0.0002709731  103201
     93 -0.0124734828  0.0001555878  001231
     53  0.0109139066  0.0001191134  101221
     36  0.0106813392  0.0001140910  112012
     43 -0.0102128725  0.0001043028  110032
     94 -0.0102114514  0.0001042737  001132
     88  0.0101158654  0.0001023307  003121
     32 -0.0076514289  0.0000585444  113002
     89 -0.0071866109  0.0000516474  003112
     59  0.0069483296  0.0000482793  100231
     39  0.0060909733  0.0000371000  110302
     60  0.0041860339  0.0000175229  100132
     68  0.0038991613  0.0000152035  030112
     84  0.0037270121  0.0000138906  010312
     67  0.0028457526  0.0000080983  030121
     50 -0.0024917560  0.0000062088  102112
     19 -0.0021039228  0.0000044265  130201

 input the coefficient threshold (end with 0.) [ 0.0000]:
 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 9) export wave function files for cioverlap (all states).
 0) end.

 input menu number [  0]: