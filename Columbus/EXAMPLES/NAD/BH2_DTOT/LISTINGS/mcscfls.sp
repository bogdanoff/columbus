

     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 This program allows the csf mixing coefficient and orbital expansion coefficient
 optimization using the graphical unitary group approach and the exponential
 operator mcscf method.
 references:  r. shepard and j. simons, ' int. j. quantum chem. symp. 14, 211 (1980).
              r. shepard, i. shavitt, and j. simons, j. chem. phys. 76, 543 (1982).
              r. shepard in "ab initio methods in quantum chemistry ii" advances in chemical
                  physics 69, edited by k. p. lawley (wiley, new york, 1987) pp. 63-200.
 Original autor: Ron Shepard, ANL
 Later revisions: Michal Dallos, University Vienna

 This Version of Program MCSCF is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.4.0.2     **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 Workspace allocation information:
       484966400 of real*8 words ( 3700.00 MB) of work space has been allocated.

 user input information:

 ======== echo of the mcscf input ========
 ------------------------------------------------------------------------
  &input
   niter=200,
   nmiter=30,
   nciitr=30,
   tol(3)=1.e-6,
   tol(2)=1.e-6,
   tol(1)=1.e-8,
   NSTATE=0,
   npath=1,3,9,10,13,17,19,21,11, 2,23
   ncoupl=5,
   FCIORB=
   1,2,20,1,3,20,1,4,20,1,5,20,1,6,20,2,1,20
   NAVST(1) = 2,
   WAVST(1,1)=1 ,
   WAVST(1,2)=1 ,
   NAVST(2) = 1,
   WAVST(2,1)=1 ,
 /&end
 ------------------------------------------------------------------------


 ***  Integral file informations  ***


 input integral file : /scratch/plasserf/69217/WORK/aoints                      
    

 Integral file header information:
 Hermit Integral Program : SIFS version  hawk3.itc.univie. 15:42:27.405 18-Apr-11

 Core type energy values:
 energy( 1)=  4.196537892870E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 total ao core energy =    4.196537893


   ******  Basis set information:  ******

 Number of irreps:                  2
 Total number of basis functions:  24

 irrep no.              1    2
 irrep label           A'   A" 
 no. of bas.fcions.    18    6
 warning: resetting tol(6) due to possible inconsistency.


 ***  MCSCF optimization procedure parmeters:  ***


 maximum number of mcscf iterations:        niter=   200

 maximum number of psci micro-iterations:   nmiter=   30
 maximum r,s subspace dimension allowed:    nvrsmx=   30

 tol(1)=  1.0000E-08. . . . delta-emc convergence criterion.
 tol(2)=  1.0000E-06. . . . wnorm convergence criterion.
 tol(3)=  1.0000E-06. . . . knorm convergence criterion.
 tol(4)=  1.0000E-08. . . . apxde convergence criterion.
 tol(5)=  1.0000E-04. . . . small diagonal matrix element tolerance.
 tol(6)=  1.2500E-07. . . . minimum ci-psci residual norm.
 tol(7)=  1.0000E-05. . . . maximum ci-psci residual norm.
 tol(8)=  1.0000E+00. . . . maximum abs(k(xy)) allowed.
 tol(9)=  1.0000E+00. . . . wnorm coupling tolerance.
 tol(10)= 0.0000E+00. . . . maximum psci emergency shift parameter.
 tol(11)= 0.0000E+00. . . . minimum psci emergency shift parameter.
 tol(12)= 0.0000E+00. . . . increment of psci emergency shift parameter.


 *** State averaging informations: ***


 MCSCF calculation performed for  2 DRTs.

 DRT  first state   no.of aver.states   weights
  1   ground state          2             0.333 0.333
  2   ground state          1             0.333

 The number of hmc(*) eigenvalues and eigenvectors calculated each iteration per DRT:
 DRT.   no.of eigenv.(=ncol)
    1        3
    2        2

 Orbitals included in invariant subspaces:
   symmetry   orbital   mask
       1       2(  2)    20
       1       3(  3)    20
       1       4(  4)    20
       1       5(  5)    20
       1       6(  6)    20
       2       1( 19)    20

 npath(*) options:
  2:  orbital-state coupling terms will be included beginning on iteration ncoupl=  5
  3:  print intermediate timing information.
  9:  suppress the drt listing.
 10:  suppress the hmc(*) eigenvector listing.
 11:  construct the hmc(*) matrix explicitly.
 13:  get initial orbitals from the formatted file, mocoef.
 17:  print the final natural orbitals and occupations.
 19:  transform the virtual orbitals to diagonalize qvv(*).
 21:  write out the one- and two- electron density for further use (files:mcd1fl, mcd2fl).
 23:  use the old integral transformation.


   ******  DRT info section  ******


 Informations for the DRT no.  1

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a' 
 Total number of electrons:    7
 Spin multiplicity:            2
 Number of active orbitals:    6
 Number of active electrons:   5
 Total number of CSFs:       115

 Informations for the DRT no.  2

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a" 
 Total number of electrons:    7
 Spin multiplicity:            2
 Number of active orbitals:    6
 Number of active electrons:   5
 Total number of CSFs:        95
 
 !timer: initialization                  cpu_time=     0.002 walltime=     0.002

 faar:   0 active-active rotations allowed out of:  10 possible.


 Number of active-double rotations:         5
 Number of active-active rotations:         0
 Number of double-virtual rotations:       12
 Number of active-virtual rotations:       65

 Size of orbital-Hessian matrix B:                     3809
 Size of the orbital-state Hessian matrix C:          26650
 Total size of the state Hessian matrix M:            31125
 Size of HESSIAN-matrix for quadratic conv.:          61584



   ****** Integral transformation section ******


 Source of the initial MO coeficients:

 Input MO coefficient file: /scratch/plasserf/69217/WORK/mocoef                         
 

               starting mcscf iteration...   1
 !timer:                                 cpu_time=     0.003 walltime=     0.003

 orbital-state coupling will not be calculated this iteration.

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     2, naopsy(1) =    18, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 16383, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 32768

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore= 484963466

 inoutp: segmentation information:
 in-core transformation space,   avcinc = 484861466
 address segment size,           sizesg = 484856010
 number of in-core blocks,       nincbk =         4
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:       8519 transformed 1/r12    array elements were written in       2 records.

 !timer: 2-e transformation              cpu_time=     0.015 walltime=     0.014

 Size of orbital-Hessian matrix B:                     3809
 Total size of the state Hessian matrix M:            31125
 Total size of HESSIAN-matrix for linear con          34934


 mosort: allocated sort2 space, avc2is=   484890852 available sort2 space, avcisx=   484891104
 !timer: mosrt1                          cpu_time=     0.001 walltime=     0.001
 !timer: mosrt2                          cpu_time=     0.001 walltime=     0.001
 !timer: mosort                          cpu_time=     0.002 walltime=     0.003
 !timer: hdiag(*) construction           cpu_time=     0.000 walltime=     0.000
 !timer: hmcft                           cpu_time=     0.000 walltime=     0.000
 !timer: hmc(*) diagonalization          cpu_time=     0.009 walltime=     0.009

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy
    1*      -25.7026242052      -29.8991620980
    2       -25.6534252045      -29.8499630974
    3       -25.4316781748      -29.6282160677
 !timer: hmcvec                          cpu_time=     0.010 walltime=     0.009
 !timer: hdiag(*) construction           cpu_time=     0.000 walltime=     0.000
 !timer: hmcft                           cpu_time=     0.000 walltime=     0.000
 !timer: hmc(*) diagonalization          cpu_time=     0.005 walltime=     0.005

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy
    1*      -25.5967402307      -29.7932781235
    2       -25.5909941113      -29.7875320042
 !timer: hmcvec                          cpu_time=     0.005 walltime=     0.005
 !timer: rdft                            cpu_time=     0.001 walltime=     0.001
 !timer: rdft                            cpu_time=     0.000 walltime=     0.001
 !timer: mfad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfva                            cpu_time=     0.001 walltime=     0.000
 !timer: mfvd                            cpu_time=     0.000 walltime=     0.000
 !timer: bvaad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvaadf                          cpu_time=     0.000 walltime=     0.000
 !timer: mqaa                            cpu_time=     0.000 walltime=     0.000
 !timer: mfaa                            cpu_time=     0.000 walltime=     0.000
 !timer: badad                           cpu_time=     0.000 walltime=     0.000
 !timer: badadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvava                           cpu_time=     0.000 walltime=     0.000
 !timer: bvavaf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvavd                           cpu_time=     0.000 walltime=     0.000
 !timer: bvavdf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvdadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdvd                           cpu_time=     0.000 walltime=     0.000
 !timer: hbcon                           cpu_time=     0.002 walltime=     0.002
 
  tol(10)=  0.000000000000000E+000  eshsci=  1.136764792524999E-002
 Total number of micro iterations:    7

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.99747588 pnorm= 0.0000E+00 rznorm= 2.9821E-06 rpnorm= 0.0000E+00 noldr=  7 nnewr=  7 nolds=  0 nnews=  0
 
 !timer: Direct Bxr time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  cpu_time=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: solvek total                    cpu_time=     0.001 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
   -15.324727

 qvv(*) eigenvalues. symmetry block  1
     0.798599    0.853448    0.946918    1.129573    1.262965    1.693811    2.450874    2.461326    3.144413    4.032889
     4.420669    4.484668

 qvv(*) eigenvalues. symmetry block  2
     0.940956    1.691663    1.761079    3.428035    3.560354
 !timer: motran                          cpu_time=     0.001 walltime=     0.001

 restrt: restart information saved on the restart file (unit= 13).
 !timer: mcscf iteration                 cpu_time=     0.036 walltime=     0.036

 not all mcscf convergence criteria are satisfied.
 iter=    1 emc=    -25.6509298801 demc= 2.5651E+01 wnorm= 9.0941E-02 knorm= 7.1006E-02 apxde= 1.9254E-03    *not conv.*     

               starting mcscf iteration...   2
 !timer:                                 cpu_time=     0.039 walltime=     0.039

 orbital-state coupling will not be calculated this iteration.

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     2, naopsy(1) =    18, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 16383, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 32768

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore= 484963466

 inoutp: segmentation information:
 in-core transformation space,   avcinc = 484861466
 address segment size,           sizesg = 484856010
 number of in-core blocks,       nincbk =         4
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:       8510 transformed 1/r12    array elements were written in       2 records.

 !timer: 2-e transformation              cpu_time=     0.006 walltime=     0.006

 Size of orbital-Hessian matrix B:                     3809
 Total size of the state Hessian matrix M:            31125
 Total size of HESSIAN-matrix for linear con          34934


 mosort: allocated sort2 space, avc2is=   484890852 available sort2 space, avcisx=   484891104
 !timer: mosrt1                          cpu_time=     0.001 walltime=     0.001
 !timer: mosrt2                          cpu_time=     0.000 walltime=     0.000
 !timer: mosort                          cpu_time=     0.002 walltime=     0.002
 !timer: hdiag(*) construction           cpu_time=     0.000 walltime=     0.000
 !timer: hmcft                           cpu_time=     0.000 walltime=     0.000
 !timer: hmc(*) diagonalization          cpu_time=     0.008 walltime=     0.009

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy
    1*      -25.7046045043      -29.9011423971
    2       -25.6558956889      -29.8524335818
    3       -25.4343787090      -29.6309166018
 !timer: hmcvec                          cpu_time=     0.009 walltime=     0.009
 !timer: hdiag(*) construction           cpu_time=     0.000 walltime=     0.000
 !timer: hmcft                           cpu_time=     0.001 walltime=     0.000
 !timer: hmc(*) diagonalization          cpu_time=     0.005 walltime=     0.005

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy
    1*      -25.5983936720      -29.7949315649
    2       -25.5936961356      -29.7902340285
 !timer: hmcvec                          cpu_time=     0.006 walltime=     0.005
 !timer: rdft                            cpu_time=     0.000 walltime=     0.001
 !timer: rdft                            cpu_time=     0.001 walltime=     0.001
 !timer: mfad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfvd                            cpu_time=     0.000 walltime=     0.000
 !timer: bvaad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvaadf                          cpu_time=     0.000 walltime=     0.000
 !timer: mqaa                            cpu_time=     0.000 walltime=     0.000
 !timer: mfaa                            cpu_time=     0.000 walltime=     0.000
 !timer: badad                           cpu_time=     0.000 walltime=     0.000
 !timer: badadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvava                           cpu_time=     0.001 walltime=     0.000
 !timer: bvavaf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvavd                           cpu_time=     0.000 walltime=     0.000
 !timer: bvavdf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvdadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdvd                           cpu_time=     0.000 walltime=     0.000
 !timer: hbcon                           cpu_time=     0.002 walltime=     0.002
 
  tol(10)=  0.000000000000000E+000  eshsci=  7.867240534709794E-004
 Total number of micro iterations:    5

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0=-0.99968232 pnorm= 0.0000E+00 rznorm= 3.1300E-06 rpnorm= 0.0000E+00 noldr=  5 nnewr=  5 nolds=  0 nnews=  0
 
 !timer: Direct Bxr time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  cpu_time=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: solvek total                    cpu_time=     0.000 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
   -15.321896

 qvv(*) eigenvalues. symmetry block  1
     0.808989    0.855898    0.956728    1.116004    1.257005    1.695295    2.449216    2.461656    3.150919    4.036588
     4.419409    4.486303

 qvv(*) eigenvalues. symmetry block  2
     0.943062    1.693029    1.762713    3.430959    3.563223
 !timer: motran                          cpu_time=     0.001 walltime=     0.001

 restrt: restart information saved on the restart file (unit= 13).
 !timer: mcscf iteration                 cpu_time=     0.026 walltime=     0.026

 not all mcscf convergence criteria are satisfied.
 iter=    2 emc=    -25.6529646217 demc= 2.0347E-03 wnorm= 6.2938E-03 knorm= 2.5204E-02 apxde= 2.8275E-05    *not conv.*     

               starting mcscf iteration...   3
 !timer:                                 cpu_time=     0.065 walltime=     0.066

 orbital-state coupling will not be calculated this iteration.

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     2, naopsy(1) =    18, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 16383, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 32768

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore= 484963466

 inoutp: segmentation information:
 in-core transformation space,   avcinc = 484861466
 address segment size,           sizesg = 484856010
 number of in-core blocks,       nincbk =         4
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:       8484 transformed 1/r12    array elements were written in       2 records.

 !timer: 2-e transformation              cpu_time=     0.006 walltime=     0.006

 Size of orbital-Hessian matrix B:                     3809
 Total size of the state Hessian matrix M:            31125
 Total size of HESSIAN-matrix for linear con          34934


 mosort: allocated sort2 space, avc2is=   484890852 available sort2 space, avcisx=   484891104
 !timer: mosrt1                          cpu_time=     0.001 walltime=     0.001
 !timer: mosrt2                          cpu_time=     0.000 walltime=     0.000
 !timer: mosort                          cpu_time=     0.001 walltime=     0.002
 !timer: hdiag(*) construction           cpu_time=     0.000 walltime=     0.000
 !timer: hmcft                           cpu_time=     0.001 walltime=     0.000
 !timer: hmc(*) diagonalization          cpu_time=     0.008 walltime=     0.009

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy
    1*      -25.7044093016      -29.9009471945
    2       -25.6559561133      -29.8524940061
    3       -25.4340993475      -29.6306372404
 !timer: hmcvec                          cpu_time=     0.010 walltime=     0.009
 !timer: hdiag(*) construction           cpu_time=     0.000 walltime=     0.000
 !timer: hmcft                           cpu_time=     0.000 walltime=     0.000
 !timer: hmc(*) diagonalization          cpu_time=     0.005 walltime=     0.005

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy
    1*      -25.5986382306      -29.7951761235
    2       -25.5937630760      -29.7903009688
 !timer: hmcvec                          cpu_time=     0.005 walltime=     0.005
 !timer: rdft                            cpu_time=     0.001 walltime=     0.001
 !timer: rdft                            cpu_time=     0.000 walltime=     0.001
 !timer: mfad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfvd                            cpu_time=     0.000 walltime=     0.000
 !timer: bvaad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvaadf                          cpu_time=     0.000 walltime=     0.000
 !timer: mqaa                            cpu_time=     0.000 walltime=     0.000
 !timer: mfaa                            cpu_time=     0.000 walltime=     0.000
 !timer: badad                           cpu_time=     0.001 walltime=     0.000
 !timer: badadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvava                           cpu_time=     0.000 walltime=     0.000
 !timer: bvavaf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvavd                           cpu_time=     0.000 walltime=     0.000
 !timer: bvavdf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvdadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdvd                           cpu_time=     0.000 walltime=     0.000
 !timer: hbcon                           cpu_time=     0.002 walltime=     0.002
 
  tol(10)=  0.000000000000000E+000  eshsci=  1.489817878821648E-004
 Total number of micro iterations:    6

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 0.99995733 pnorm= 0.0000E+00 rznorm= 1.4612E-07 rpnorm= 0.0000E+00 noldr=  6 nnewr=  6 nolds=  0 nnews=  0
 
 !timer: Direct Bxr time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  cpu_time=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: solvek total                    cpu_time=     0.000 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
   -15.321753

 qvv(*) eigenvalues. symmetry block  1
     0.811630    0.857337    0.957487    1.114370    1.252455    1.694826    2.446123    2.461556    3.151764    4.036803
     4.419003    4.486553

 qvv(*) eigenvalues. symmetry block  2
     0.942751    1.693250    1.762819    3.431361    3.563576
 !timer: motran                          cpu_time=     0.000 walltime=     0.001

 restrt: restart information saved on the restart file (unit= 13).
 !timer: mcscf iteration                 cpu_time=     0.026 walltime=     0.026

 not all mcscf convergence criteria are satisfied.
 iter=    3 emc=    -25.6530012152 demc= 3.6593E-05 wnorm= 1.1919E-03 knorm= 9.2381E-03 apxde= 3.1010E-06    *not conv.*     

               starting mcscf iteration...   4
 !timer:                                 cpu_time=     0.091 walltime=     0.092

 orbital-state coupling will not be calculated this iteration.

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     2, naopsy(1) =    18, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 16383, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 32768

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore= 484963466

 inoutp: segmentation information:
 in-core transformation space,   avcinc = 484861466
 address segment size,           sizesg = 484856010
 number of in-core blocks,       nincbk =         4
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:       8479 transformed 1/r12    array elements were written in       2 records.

 !timer: 2-e transformation              cpu_time=     0.007 walltime=     0.006

 Size of orbital-Hessian matrix B:                     3809
 Total size of the state Hessian matrix M:            31125
 Total size of HESSIAN-matrix for linear con          34934


 mosort: allocated sort2 space, avc2is=   484890852 available sort2 space, avcisx=   484891104
 !timer: mosrt1                          cpu_time=     0.001 walltime=     0.001
 !timer: mosrt2                          cpu_time=     0.000 walltime=     0.000
 !timer: mosort                          cpu_time=     0.001 walltime=     0.002
 !timer: hdiag(*) construction           cpu_time=     0.000 walltime=     0.000
 !timer: hmcft                           cpu_time=     0.000 walltime=     0.000
 !timer: hmc(*) diagonalization          cpu_time=     0.009 walltime=     0.009

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy
    1*      -25.7043265103      -29.9008644032
    2       -25.6559657093      -29.8525036022
    3       -25.4339624062      -29.6305002991
 !timer: hmcvec                          cpu_time=     0.009 walltime=     0.009
 !timer: hdiag(*) construction           cpu_time=     0.000 walltime=     0.000
 !timer: hmcft                           cpu_time=     0.000 walltime=     0.000
 !timer: hmc(*) diagonalization          cpu_time=     0.005 walltime=     0.005

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy
    1*      -25.5987243478      -29.7952622407
    2       -25.5937607182      -29.7902986111
 !timer: hmcvec                          cpu_time=     0.005 walltime=     0.005
 !timer: rdft                            cpu_time=     0.001 walltime=     0.001
 !timer: rdft                            cpu_time=     0.000 walltime=     0.001
 !timer: mfad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqva                            cpu_time=     0.001 walltime=     0.000
 !timer: mfva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfvd                            cpu_time=     0.000 walltime=     0.000
 !timer: bvaad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvaadf                          cpu_time=     0.000 walltime=     0.000
 !timer: mqaa                            cpu_time=     0.000 walltime=     0.000
 !timer: mfaa                            cpu_time=     0.000 walltime=     0.000
 !timer: badad                           cpu_time=     0.000 walltime=     0.000
 !timer: badadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvava                           cpu_time=     0.000 walltime=     0.000
 !timer: bvavaf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvavd                           cpu_time=     0.000 walltime=     0.000
 !timer: bvavdf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvdadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdvd                           cpu_time=     0.000 walltime=     0.000
 !timer: hbcon                           cpu_time=     0.002 walltime=     0.002
 
  tol(10)=  0.000000000000000E+000  eshsci=  4.979576984682532E-005
 Total number of micro iterations:    6

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0=-0.99999319 pnorm= 0.0000E+00 rznorm= 1.1977E-07 rpnorm= 0.0000E+00 noldr=  6 nnewr=  6 nolds=  0 nnews=  0
 
 !timer: Direct Bxr time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  cpu_time=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: solvek total                    cpu_time=     0.001 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
   -15.321807

 qvv(*) eigenvalues. symmetry block  1
     0.811814    0.857632    0.957260    1.114105    1.250352    1.694546    2.444582    2.461796    3.151855    4.036779
     4.418627    4.486635

 qvv(*) eigenvalues. symmetry block  2
     0.942586    1.693256    1.762824    3.431433    3.563639
 !timer: motran                          cpu_time=     0.000 walltime=     0.001

 restrt: restart information saved on the restart file (unit= 13).
 !timer: mcscf iteration                 cpu_time=     0.027 walltime=     0.026

 not all mcscf convergence criteria are satisfied.
 iter=    4 emc=    -25.6530055225 demc= 4.3073E-06 wnorm= 3.9837E-04 knorm= 3.6897E-03 apxde= 4.8870E-07    *not conv.*     

               starting mcscf iteration...   5
 !timer:                                 cpu_time=     0.118 walltime=     0.118

 orbital-state coupling will be calculated this iteration.

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     2, naopsy(1) =    18, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 16383, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 32768

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore= 484963466

 inoutp: segmentation information:
 in-core transformation space,   avcinc = 484861466
 address segment size,           sizesg = 484856010
 number of in-core blocks,       nincbk =         4
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:       8478 transformed 1/r12    array elements were written in       2 records.

 !timer: 2-e transformation              cpu_time=     0.006 walltime=     0.006

 Size of orbital-Hessian matrix B:                     3809
 Size of the orbital-state Hessian matrix C:          26650
 Total size of the state Hessian matrix M:            31125
 Size of HESSIAN-matrix for quadratic conv.:          61584


 mosort: allocated sort2 space, avc2is=   484890852 available sort2 space, avcisx=   484891104
 !timer: mosrt1                          cpu_time=     0.001 walltime=     0.001
 !timer: mosrt2                          cpu_time=     0.001 walltime=     0.000
 !timer: mosort                          cpu_time=     0.002 walltime=     0.002
 !timer: hdiag(*) construction           cpu_time=     0.000 walltime=     0.000
 !timer: hmcft                           cpu_time=     0.000 walltime=     0.000
 !timer: hmc(*) diagonalization          cpu_time=     0.009 walltime=     0.009

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy
    1*      -25.7042926961      -29.9008305889
    2       -25.6559694954      -29.8525073882
    3       -25.4339148725      -29.6304527654
 !timer: hmcvec                          cpu_time=     0.009 walltime=     0.009
 !timer: hdiag(*) construction           cpu_time=     0.000 walltime=     0.000
 !timer: hmcft                           cpu_time=     0.000 walltime=     0.000
 !timer: hmc(*) diagonalization          cpu_time=     0.005 walltime=     0.005

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy
    1*      -25.5987564386      -29.7952943315
    2       -25.5937572814      -29.7902951743
 !timer: hmcvec                          cpu_time=     0.005 walltime=     0.005
 !timer: cadu                            cpu_time=     0.000 walltime=     0.000
 !timer: cvdu                            cpu_time=     0.000 walltime=     0.000
 !timer: rdft                            cpu_time=     0.002 walltime=     0.002
 !timer: cadu                            cpu_time=     0.000 walltime=     0.000
 !timer: cvdu                            cpu_time=     0.000 walltime=     0.000
 !timer: rdft                            cpu_time=     0.001 walltime=     0.001
 !timer: mqva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfvd                            cpu_time=     0.000 walltime=     0.000
 !timer: bvaad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvaadf                          cpu_time=     0.000 walltime=     0.000
 !timer: mfad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqaa                            cpu_time=     0.000 walltime=     0.000
 !timer: mfaa                            cpu_time=     0.000 walltime=     0.000
 !timer: badad                           cpu_time=     0.001 walltime=     0.000
 !timer: badadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvava                           cpu_time=     0.000 walltime=     0.000
 !timer: bvavaf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvavd                           cpu_time=     0.000 walltime=     0.000
 !timer: bvavdf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvdadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdvd                           cpu_time=     0.000 walltime=     0.000
 !timer: hbcon                           cpu_time=     0.005 walltime=     0.005
 
  tol(10)=  0.000000000000000E+000  eshsci=  1.981902797691617E-005
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 Total number of micro iterations:   12

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0=-0.99999676 pnorm= 5.1450E-03 rznorm= 1.0907E-07 rpnorm= 4.9509E-08 noldr= 10 nnewr= 10 nolds= 11 nnews= 11
 
 !timer: Direct Bxr time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  cpu_time=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: solvek total                    cpu_time=     0.003 walltime=     0.003

 fdd(*) eigenvalues. symmetry block  1
   -15.321833

 qvv(*) eigenvalues. symmetry block  1
     0.811791    0.857733    0.957146    1.114092    1.249448    1.694428    2.443909    2.461969    3.151872    4.036763
     4.418438    4.486674

 qvv(*) eigenvalues. symmetry block  2
     0.942527    1.693250    1.762824    3.431452    3.563656
 !timer: motran                          cpu_time=     0.001 walltime=     0.001

 restrt: restart information saved on the restart file (unit= 13).
 !timer: mcscf iteration                 cpu_time=     0.031 walltime=     0.031

 not all mcscf convergence criteria are satisfied.
 iter=    5 emc=    -25.6530062100 demc= 6.8754E-07 wnorm= 1.5855E-04 knorm= 2.5465E-03 apxde= 1.3882E-07    *not conv.*     

               starting mcscf iteration...   6
 !timer:                                 cpu_time=     0.149 walltime=     0.149

 orbital-state coupling will be calculated this iteration.

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     2, naopsy(1) =    18, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 16383, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 32768

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore= 484963466

 inoutp: segmentation information:
 in-core transformation space,   avcinc = 484861466
 address segment size,           sizesg = 484856010
 number of in-core blocks,       nincbk =         4
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:       8507 transformed 1/r12    array elements were written in       2 records.

 !timer: 2-e transformation              cpu_time=     0.006 walltime=     0.006

 Size of orbital-Hessian matrix B:                     3809
 Size of the orbital-state Hessian matrix C:          26650
 Total size of the state Hessian matrix M:            31125
 Size of HESSIAN-matrix for quadratic conv.:          61584


 mosort: allocated sort2 space, avc2is=   484890852 available sort2 space, avcisx=   484891104
 !timer: mosrt1                          cpu_time=     0.001 walltime=     0.001
 !timer: mosrt2                          cpu_time=     0.000 walltime=     0.000
 !timer: mosort                          cpu_time=     0.002 walltime=     0.002
 !timer: hdiag(*) construction           cpu_time=     0.000 walltime=     0.000
 !timer: hmcft                           cpu_time=     0.000 walltime=     0.000
 !timer: hmc(*) diagonalization          cpu_time=     0.008 walltime=     0.009

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy
    1*      -25.7042694139      -29.9008073068
    2       -25.6559723432      -29.8525102360
    3       -25.4338901212      -29.6304280141
 !timer: hmcvec                          cpu_time=     0.009 walltime=     0.009
 !timer: hdiag(*) construction           cpu_time=     0.000 walltime=     0.000
 !timer: hmcft                           cpu_time=     0.000 walltime=     0.000
 !timer: hmc(*) diagonalization          cpu_time=     0.005 walltime=     0.005

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy
    1*      -25.5987772899      -29.7953151828
    2       -25.5937550941      -29.7902929869
 !timer: hmcvec                          cpu_time=     0.006 walltime=     0.005
 !timer: cadu                            cpu_time=     0.000 walltime=     0.000
 !timer: cvdu                            cpu_time=     0.000 walltime=     0.000
 !timer: rdft                            cpu_time=     0.002 walltime=     0.002
 !timer: cadu                            cpu_time=     0.000 walltime=     0.000
 !timer: cvdu                            cpu_time=     0.000 walltime=     0.000
 !timer: rdft                            cpu_time=     0.001 walltime=     0.001
 !timer: mqva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfvd                            cpu_time=     0.000 walltime=     0.000
 !timer: bvaad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvaadf                          cpu_time=     0.000 walltime=     0.000
 !timer: mfad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqaa                            cpu_time=     0.000 walltime=     0.000
 !timer: mfaa                            cpu_time=     0.000 walltime=     0.000
 !timer: badad                           cpu_time=     0.000 walltime=     0.000
 !timer: badadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvava                           cpu_time=     0.000 walltime=     0.000
 !timer: bvavaf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvavd                           cpu_time=     0.000 walltime=     0.000
 !timer: bvavdf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvdadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdvd                           cpu_time=     0.000 walltime=     0.000
 !timer: hbcon                           cpu_time=     0.004 walltime=     0.005
 
  tol(10)=  0.000000000000000E+000  eshsci=  7.920634554480152E-008
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 performing all-state projection
 Total number of micro iterations:    4

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0=-1.00000000 pnorm= 7.0468E-06 rznorm= 5.7539E-08 rpnorm= 7.7783E-08 noldr=  4 nnewr=  4 nolds=  2 nnews=  2
 
 !timer: Direct Bxr time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  cpu_time=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: solvek total                    cpu_time=     0.001 walltime=     0.001

 fdd(*) eigenvalues. symmetry block  1
   -15.321852

 qvv(*) eigenvalues. symmetry block  1
     0.811757    0.857800    0.957075    1.114145    1.248811    1.694346    2.443429    2.462132    3.151879    4.036750
     4.418300    4.486704

 qvv(*) eigenvalues. symmetry block  2
     0.942491    1.693244    1.762825    3.431461    3.563663
 !timer: motran                          cpu_time=     0.001 walltime=     0.001

 restrt: restart information saved on the restart file (unit= 13).
 !timer: mcscf iteration                 cpu_time=     0.029 walltime=     0.029

 not all mcscf convergence criteria are satisfied.
 iter=    6 emc=    -25.6530063490 demc= 1.3898E-07 wnorm= 6.3365E-07 knorm= 4.2263E-06 apxde= 4.5847E-13    *not conv.*     

               starting mcscf iteration...   7
 !timer:                                 cpu_time=     0.178 walltime=     0.179

 orbital-state coupling will be calculated this iteration.

 module tranlib input parameters:

 prnopt    =     1, chkopt    =     0,ortopt    =     0, denopt    =     0
 mapin(1 ) =     1, nsymao    =     2, naopsy(1) =    18, freeze(1) =     1
 mapout(1) =     1, nsymmo    =    -1, nmopsy(1) =    -1, fsplit    =     1
 outlab    =     0, seward    =     0, lumorb    =     0, DALTON2   =     0
 nextint   =     2
 LDAMIN    =   127, LDAMAX    = 16383, LDAINC    =    64
 LRC1MX    =    -1, LRC2MX    =    -1, LRCSCR    = 32768

 THRESH    =  5.0000E-12  [cutoff threshold]

 module tranlib: workspace lcore= 484963466

 inoutp: segmentation information:
 in-core transformation space,   avcinc = 484861466
 address segment size,           sizesg = 484856010
 number of in-core blocks,       nincbk =         4
 number of out-of-core blocks,   noutbk =         0
 number of in-core segments,     incseg =         1
 number of out-of-core segments, outseg =         0
 trmain:       8517 transformed 1/r12    array elements were written in       2 records.

 !timer: 2-e transformation              cpu_time=     0.007 walltime=     0.006

 Size of orbital-Hessian matrix B:                     3809
 Size of the orbital-state Hessian matrix C:          26650
 Total size of the state Hessian matrix M:            31125
 Size of HESSIAN-matrix for quadratic conv.:          61584


 mosort: allocated sort2 space, avc2is=   484890852 available sort2 space, avcisx=   484891104
 !timer: mosrt1                          cpu_time=     0.001 walltime=     0.001
 !timer: mosrt2                          cpu_time=     0.000 walltime=     0.000
 !timer: mosort                          cpu_time=     0.001 walltime=     0.002
 !timer: hdiag(*) construction           cpu_time=     0.001 walltime=     0.000
 !timer: hmcft                           cpu_time=     0.001 walltime=     0.000
 !timer: hmc(*) diagonalization          cpu_time=     0.009 walltime=     0.009

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy
    1*      -25.7042693760      -29.9008072688
    2       -25.6559723467      -29.8525102395
    3       -25.4338900258      -29.6304279187
 !timer: hmcvec                          cpu_time=     0.010 walltime=     0.009
 !timer: hdiag(*) construction           cpu_time=     0.000 walltime=     0.000
 !timer: hmcft                           cpu_time=     0.000 walltime=     0.000
 !timer: hmc(*) diagonalization          cpu_time=     0.005 walltime=     0.005

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy
    1*      -25.5987773244      -29.7953152173
    2       -25.5937550803      -29.7902929732
 !timer: hmcvec                          cpu_time=     0.005 walltime=     0.005
 !timer: cadu                            cpu_time=     0.000 walltime=     0.000
 !timer: cvdu                            cpu_time=     0.000 walltime=     0.000
 !timer: rdft                            cpu_time=     0.002 walltime=     0.002
 !timer: cadu                            cpu_time=     0.000 walltime=     0.000
 !timer: cvdu                            cpu_time=     0.000 walltime=     0.000
 !timer: rdft                            cpu_time=     0.001 walltime=     0.001
 !timer: mqva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfva                            cpu_time=     0.000 walltime=     0.000
 !timer: mfvd                            cpu_time=     0.000 walltime=     0.000
 !timer: bvaad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvaadf                          cpu_time=     0.000 walltime=     0.000
 !timer: mfad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqad                            cpu_time=     0.000 walltime=     0.000
 !timer: mqaa                            cpu_time=     0.000 walltime=     0.000
 !timer: mfaa                            cpu_time=     0.000 walltime=     0.000
 !timer: badad                           cpu_time=     0.000 walltime=     0.000
 !timer: badadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvava                           cpu_time=     0.001 walltime=     0.000
 !timer: bvavaf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvavd                           cpu_time=     0.000 walltime=     0.000
 !timer: bvavdf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdad                           cpu_time=     0.000 walltime=     0.000
 !timer: bvdadf                          cpu_time=     0.000 walltime=     0.000
 !timer: bvdvd                           cpu_time=     0.000 walltime=     0.000
 !timer: hbcon                           cpu_time=     0.005 walltime=     0.004
 
  tol(10)=  0.000000000000000E+000  eshsci=  7.560147371961876E-009
 performing all-state projection
 Total number of micro iterations:    1

 ***  micro: final psci convergence values:  ***
    imxov=  1 z0= 1.00000000 pnorm= 0.0000E+00 rznorm= 8.8902E-08 rpnorm= 3.2543E-09 noldr=  1 nnewr=  1 nolds=  0 nnews=  0
 
 !timer: Direct Bxr time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: Direct Cxr and Cxs time contr.  cpu_time=     0.000 walltime=     0.000
 !timer: Direct Mxs time contribution    cpu_time=     0.000 walltime=     0.000
 !timer: solvek total                    cpu_time=     0.000 walltime=     0.000

 fdd(*) eigenvalues. symmetry block  1
   -15.321852

 qvv(*) eigenvalues. symmetry block  1
     0.811757    0.857800    0.957074    1.114145    1.248810    1.694346    2.443428    2.462132    3.151879    4.036750
     4.418300    4.486704

 qvv(*) eigenvalues. symmetry block  2
     0.942491    1.693244    1.762825    3.431461    3.563663
 !timer: motran                          cpu_time=     0.001 walltime=     0.001

 restrt: restart information saved on the restart file (unit= 13).
 !timer: mcscf iteration                 cpu_time=     0.029 walltime=     0.029

 all mcscf convergence criteria are satisfied.

 final mcscf convergence values:
 iter=    7 emc=    -25.6530063490 demc= 4.6541E-13 wnorm= 6.0481E-08 knorm= 1.3066E-08 apxde= 3.4559E-16    *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 wt 0.333 total energy=      -25.704269376, rel. (eV)=   0.000000
   DRT #1 state # 2 wt 0.333 total energy=      -25.655972347, rel. (eV)=   1.314230
   DRT #2 state # 1 wt 0.333 total energy=      -25.598777324, rel. (eV)=   2.870586
   ------------------------------------------------------------


 MO-coefficient print-out skipped (no flag 32)
 They may be found in the MOCOEF directory.

          natural orbitals of the final iteration,block  1    -  A' 
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     1.95860104     1.30516999     1.28888515     0.04578138     0.03031733     0.00000000     0.00000000
               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   17        MO   18
  occ(*)=     0.00000000     0.00000000

          natural orbitals of the final iteration,block  2    -  A" 
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6
  occ(*)=     0.37124512     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
 d1(*), fmc(*), and qmc(*) written to the 1-particle density matrix file.
        183 d2(*) elements written to the 2-particle density matrix file: mcd2fl                                                      
 !timer: writing the mc density files reqcpu_time=     0.000 walltime=     0.000


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        A'  partial gross atomic populations
   ao class       1A'        2A'        3A'        4A'        5A'        6A' 
     2_ s       1.999928   0.702109   0.000000   0.691789   0.000000   0.003399
     2_ p       0.000004   0.241437   0.501199   0.511823   0.026778   0.013310
     2_ d      -0.000001   0.007148   0.024378   0.004411   0.001236   0.000394
     2_ s       0.000086   0.494742   0.386544   0.038997   0.008852   0.006451
     2_ p      -0.000052   0.009211   0.003254   0.001432   0.000031   0.000156
     2_ s       0.000086   0.494742   0.386541   0.039000   0.008852   0.006451
     2_ p      -0.000052   0.009211   0.003254   0.001432   0.000031   0.000156
 
   ao class       7A'        8A'        9A'       10A'       11A'       12A' 
 
   ao class      13A'       14A'       15A'       16A'       17A'       18A' 

                        A"  partial gross atomic populations
   ao class       1A"        2A"        3A"        4A"        5A"        6A" 
     2_ p       0.369506   0.000000   0.000000   0.000000   0.000000   0.000000
     2_ d       0.000180   0.000000   0.000000   0.000000   0.000000   0.000000
     2_ p       0.000779   0.000000   0.000000   0.000000   0.000000   0.000000
     2_ p       0.000779   0.000000   0.000000   0.000000   0.000000   0.000000


                        gross atomic populations
     ao            2_         2_         2_
      s         3.397225   0.935673   0.935673
      p         1.664058   0.014812   0.014812
      d         0.037748   0.000000   0.000000
    total       5.099031   0.950485   0.950485
 

 Total number of electrons:    7.00000000

 !timer: mcscf                           cpu_time=     0.210 walltime=     0.210
