1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      2009-03.    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


================ Computing sorting integral file structure ================

                    -----z----- -----y----- -----x----- -----w----- ---total---

                CSFs       115        1890        6280        5685       13970
      internal walks       210         210          90          70         580
valid internal walks       115         210          90          70         485
 lcore1,lcore2=             484929707             484933033
 lencor,maxblo             484966400                 32768

 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg= 484878420
 minimum size of srtscr:     65534 WP (     2 records)
 maximum size of srtscr:    196602 WP (     4 records)
========================================
 current settings:
 minbl3         276
 minbl4         276
 locmaxbl3     2364
 locmaxbuf     1182
 maxbl3       32768
 maxbl3       32768
 maxbl4       32768
 maxbuf       16390
========================================

 sorted 4-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 16390
                                        have been written (wstat,xstat=  100.00  100.00)

 sorted 3-external integrals:     1 records of integral w-combinations 
                                  1 records of integral x-combinations of length= 16390
                                        have been written (wstat,xstat=  100.00  100.00)
 Orig.  diagonal integrals:  1electron:        24
                             0ext.    :        56
                             2ext.    :       238
                             4ext.    :       306


 Orig. off-diag. integrals:  4ext.    :      7536
                             3ext.    :     10836
                             2ext.    :      7218
                             1ext.    :      2562
                             0ext.    :       378
                             2ext. SO :         0
                             1ext. SO :         0
                             0ext. SO :         0
                             1electron:        93


 Sorted integrals            3ext.  w :      9604 x :      8372
                             4ext.  w :      6048 x :      4620


 compressed index vector length=                   189
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 0,
  GSET = 0,
   DAVCOR =10,
  NCOREL = 7
  NROOT = 2
  IVMODE = 3
  NBKITR = 1
  NVBKMN = 2
  NVBKMX = 7
  RTOLBK = 1e-3,1e-3,
  NITER = 400
  NVCIMN = 4
  NVCIMX = 7
  RTOLCI = 1e-5,1e-5,
   iden=2
  CSFPRN = 10,
 /&end
 transition
   1  1  1  2
 ------------------------------------------------------------------------
transition densities: resetting nroot to    2
lodens (list->root)=  1  2
invlodens (root->list)=  1  2
 USING SEGMENTS OF EQUAL SIZE

****************  list of control variables  ****************
 lvlprt =    0      nroot  =    2      noldv  =   0      noldhv =   0
 nunitv =    2      nbkitr =    1      niter  = 400      davcor =  10
 csfprn =   10      ivmode =    3      istrt  =   0      vout   =   0
 iortls =    0      nvbkmx =    7      ibktv  =  -1      ibkthv =  -1
 nvcimx =    7      icitv  =   -1      icithv =  -1      frcsub =   0
 nvbkmn =    2      nvcimn =    4      maxseg =   4      nrfitr =  30
 ncorel =    7      nvrfmx =   16      nvrfmn =   4      iden   =   2
 itran  =    0      froot  =    0      rtmode =   0      ncouple=   1
 skipso =    F      dalton2=    0      molcas =   0      finalv =   0
 finalw =    0      cosmocalc=   0    with_tsklst=   0
 nsegwx =    1     1     1     1
 nseg0x =    1     1     1     1
 nseg1x =    1     1     1     1
 nseg2x =    1     1     1     1
 nseg3x =    1     1     1     1
 nseg4x =    1     1     1     1
 no0ex  =      0    no1ex  =      0    no2ex  =     0    no3ex  =     0
 no4ex  =      0    nodiag =      0
 cdg4ex =    1      c3ex1ex=    1      c2ex0ex=   1
 fileloc=    0     0     0     0     0     0     0     1     1     1
 directhd=   1      noaqccshift_zyxw=      0
 critical_crit=-1.00000    critical_delta= 0.05000

 ctol   = 0.010000    lrtshift=1.000000    smalld =0.001000


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-03    1.000E-05
    2        1.000E-03    1.000E-05
 Computing density:                    .drt1.state1
 Computing density:                    .drt1.state2
 Computing transition density:          drt1.state1-> drt1.state2 (.trd1to2)
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core          484966399 DP per process
 Allocating              484966399  for integral sort step ...

********** Integral sort section *************


 workspace allocation information: lencor= 484966399

 echo of the input for program cisrt:
 ------------------------------------------------------------------------
  &input
  maxbl3=32768
  maxbl4=32768
 /&end
 ------------------------------------------------------------------------
 
 ( 6) listing file:                    ciudgls             
 ( 5) input file:                      cisrtin   
 (17) cidrt file:                      cidrtfl             
 (11) transformed integrals file:      moints    
 (12) diagonal integral file:          diagint             
 (13) off-diagonal integral file:      ofdgint             
 (31) 4-external w integrals file:     fil4w               
 (32) 4-external x integrals file:     fil4x               
 (33) 3-external w integrals file:     fil3w               
 (34) 3-external x integrals file:     fil3x               
 (21) scratch da sorting file:         srtscr              
 (12) 2-e integral file [fsplit=2]:    moints2   

 input integral file header information:
 Hermit Integral Program : SIFS version  hawk3.itc.univie. 15:42:27.405 18-Apr-11
  cidrt_title DRT#1                                                              
 MO-coefficients from mcscf.x                                                    
  total ao core energy =    4.196537893                                          
 SIFS file created by program tran.      hawk3.itc.univie. 15:42:28.899 18-Apr-11

 input energy(*) values:
 energy( 1)=  4.196537892870E+00, ietype=   -1,    core energy of type: Nuc.Rep.

 total core energy =   4.196537892870E+00

 nsym = 2 nmot=  24

 symmetry  =    1    2
 slabel(*) =  A'   A" 
 nmpsy(*)  =   18    6

 info(*) =          1      8192      6552      8192      5460         0

 orbital labels, i:molab(i)=
   1:tout:001   2:tout:002   3:tout:003   4:tout:004   5:tout:005   6:tout:006   7:tout:007   8:tout:008   9:tout:009  10:tout:010
  11:tout:011  12:tout:012  13:tout:013  14:tout:014  15:tout:015  16:tout:016  17:tout:017  18:tout:018  19:tout:019  20:tout:020
  21:tout:021  22:tout:022  23:tout:023  24:tout:024

 input parameters:
 prnopt=  0
 ldamin=    4095 ldamax=   32767 ldainc=      64
 maxbuf=   16390 maxbl3=   32768 maxbl4=   32768 intmxo=     766

 drt information:
  cidrt_title DRT#1                                                              
 nmotd =  24 nfctd =   0 nfvtc =   0 nmot  =  24
 nlevel =  24 niot  =   7 lowinl=  18
 orbital-to-level map(*)
   18  19  20  21  22  23   1   2   3   4   5   6   7   8   9  10  11  12  24  13
   14  15  16  17
 compressed map(*)
   18  19  20  21  22  23   1   2   3   4   5   6   7   8   9  10  11  12  24  13
   14  15  16  17
 levsym(*)
    1   1   1   1   1   1   1   1   1   1   1   1   2   2   2   2   2   1   1   1
    1   1   1   2
 repartitioning mu(*)=
   0.  0.  0.  0.  0.  0.  0.
  ... transition density matrix calculation requested 
  ... setting rmu(*) to zero 

 indxdg: diagonal integral statistics.
 total number of integrals contributing to diagonal matrix elements:       600
 number with all external indices:       306
 number with half external - half internal indices:       238
 number with all internal indices:        56

 indxof: off-diagonal integral statistics.
    4-external integrals: num=       7536 strt=          1
    3-external integrals: num=      10836 strt=       7537
    2-external integrals: num=       7218 strt=      18373
    1-external integrals: num=       2562 strt=      25591
    0-external integrals: num=        378 strt=      28153

 total number of off-diagonal integrals:       28530



 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg= 484845939
 !timer: setup required                  cpu_time=     0.001 walltime=     0.002
 pro2e        1     301     601     901    1201    1229    1257    1557   45245   88933
   121700  129892  135352  157191

 pro2e:     24352 integrals read in     5 records.
 pro1e        1     301     601     901    1201    1229    1257    1557   45245   88933
   121700  129892  135352  157191
 pro1e: eref =    0.000000000000000E+00
 total size of srtscr:                     4  records of                  32767 
 WP =               1048544 Bytes
 !timer: first half-sort required        cpu_time=     0.006 walltime=     0.006
 putdg        1     301     601     901    1667   34434   56279    1557   45245   88933
   121700  129892  135352  157191

 putf:       4 buffers of length     766 written to file 12
 diagonal integral file completed.

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 16390 have been written.
 wstat,xstat=  100.00  100.00
 prep4e:     2 blocks of linear combinations of 4-external integrals processed.
 number of sorted 4-external integrals      10668
 number of original 4-external integrals     7536


 putf34: external integral file complete. nfilw=    31 nfilx=    32 nrecw=     1 nrecx=     1 lbufp= 16390

 putd34:     1 records of integral w-combinations and
             1 records of integral x-combinations of length= 16390 have been written.
 wstat,xstat=  100.00  100.00
 prep3e:    13 blocks of linear combinations of 3-external integrals processed.
 number of sorted 3-external integrals      17976
 number of original 3-external integrals    10836


 putf34: external integral file complete. nfilw=    33 nfilx=    34 nrecw=     1 nrecx=     1 lbufp= 16390

 putf:      16 buffers of length     766 written to file 13
 off-diagonal files sort completed.
 !timer: second half-sort required       cpu_time=     0.003 walltime=     0.003
 !timer: cisrt complete                  cpu_time=     0.010 walltime=     0.011
 executing brd_struct for cisrtinfo
cisrtinfo:
bufszi   766
 diagfile 4ext:     306 2ext:     238 0ext:      56
 fil4w,fil4x  :    7536 fil3w,fil3x :   10836
 ofdgint  2ext:    7218 1ext:    2562 0ext:     378so0ext:       0so1ext:       0so2ext:       0
buffer minbl4     276 minbl3     276 maxbl2     279nbas:  12   5   0   0   0   0   0   0 maxbuf 16390
 CIUDG version 5.9.7 ( 5-Oct-2004)

 workspace allocation information: lcore= 484966399

 core energy values from the integral file:
 energy( 1)=  4.196537892870E+00, ietype=   -1,    core energy of type: Nuc.Rep.

 total core repulsion energy =  4.196537892870E+00
 nmot  =    24 niot  =     7 nfct  =     0 nfvt  =     0
 nrow  =    47 nsym  =     2 ssym  =     1 lenbuf=  1600
 nwalk,xbar:        580      210      210       90       70
 nvalwt,nvalw:      485      115      210       90       70
 ncsft:           13970
 total number of valid internal walks:     485
 nvalz,nvaly,nvalx,nvalw =      115     210      90      70

 cisrt info file parameters:
 file number  12 blocksize    766
 mxbld    766
 nd4ext,nd2ext,nd0ext   306   238    56
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int     7536    10836     7218     2562      378        0        0        0
 minbl4,minbl3,maxbl2   276   276   279
 maxbuf 16390
 number of external orbitals per symmetry block:  12   5
 nmsym   2 number of internal orbitals   7
 bummer (warning):transition densities: resetting ref occupation number to 0                      0
 executing brd_struct for drt
 executing brd_struct for orbinf
 executing brd_struct for momap
 calcthrxt: niot,maxw1=                     7                   216
 block size     0
 pthz,pthy,pthx,pthw:   210   210    90    70 total internal walks:     580
 maxlp3,n2lp,n1lp,n0lp   216     0     0     0
 orbsym(*)= 1 1 1 1 1 1 2

 setref:      115 references kept,
               95 references were marked as invalid, out of
              210 total.
 nmb.of records onel     1
 nmb.of records 2-ext    10
 nmb.of records 1-ext     4
 nmb.of records 0-ext     1
 nmb.of records 2-int     0
 nmb.of records 1-int     0
 nmb.of records 0-int     0
 ---------memory usage in DP -----------------
 < n-ex core usage >
     routines:
    fourex            33783
    threx             34077
    twoex              2509
    onex               1103
    allin               766
    diagon             1318
               =======
   maximum            34077
 
  __ static summary __ 
   reflst               115
   hrfspc               115
               -------
   static->             115
 
  __ core required  __ 
   totstc               115
   max n-ex           34077
               -------
   totnec->           34192
 
  __ core available __ 
   totspc         484966399
   totnec -           34192
               -------
   totvec->       484932207

 number of external paths / symmetry
 vertex x      76      60
 vertex w      93      60
segment: free space=   484932207
 reducing frespc by                  1693 to              484930514 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1         115|       115|         0|       115|         0|         1|
 -------------------------------------------------------------------------------
  Y 2         210|      1890|       115|       210|       115|         2|
 -------------------------------------------------------------------------------
  X 3          90|      6280|      2005|        90|       325|         3|
 -------------------------------------------------------------------------------
  W 4          70|      5685|      8285|        70|       415|         4|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=           4DP  conft+indsym=         840DP  drtbuffer=         849 DP

dimension of the ci-matrix ->>>     13970

 executing brd_struct for civct
 gentasklist: ntask=                    20
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS   
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      90     115       6280        115      90     115
     2  4   1    25      two-ext wz   2X  4 1      70     115       5685        115      70     115
     3  4   3    26      two-ext wx*  WX  4 3      70      90       5685       6280      70      90
     4  4   3    27      two-ext wx+  WX  4 3      70      90       5685       6280      70      90
     5  2   1    11      one-ext yz   1X  2 1     210     115       1890        115     210     115
     6  3   2    15      1ex3ex yx    3X  3 2      90     210       6280       1890      90     210
     7  4   2    16      1ex3ex yw    3X  4 2      70     210       5685       1890      70     210
     8  1   1     1      allint zz    OX  1 1     115     115        115        115     115     115
     9  2   2     5      0ex2ex yy    OX  2 2     210     210       1890       1890     210     210
    10  3   3     6      0ex2ex xx*   OX  3 3      90      90       6280       6280      90      90
    11  3   3    18      0ex2ex xx+   OX  3 3      90      90       6280       6280      90      90
    12  4   4     7      0ex2ex ww*   OX  4 4      70      70       5685       5685      70      70
    13  4   4    19      0ex2ex ww+   OX  4 4      70      70       5685       5685      70      70
    14  2   2    42      four-ext y   4X  2 2     210     210       1890       1890     210     210
    15  3   3    43      four-ext x   4X  3 3      90      90       6280       6280      90      90
    16  4   4    44      four-ext w   4X  4 4      70      70       5685       5685      70      70
    17  1   1    75      dg-024ext z  OX  1 1     115     115        115        115     115     115
    18  2   2    76      dg-024ext y  OX  2 2     210     210       1890       1890     210     210
    19  3   3    77      dg-024ext x  OX  3 3      90      90       6280       6280      90      90
    20  4   4    78      dg-024ext w  OX  4 4      70      70       5685       5685      70      70
----------------------------------------------------------------------------------------------------
REDTASK #   1 TIME=  19.000 N=  1 (task/type/sgbra)=(   1/24/0) (
REDTASK #   2 TIME=  18.000 N=  1 (task/type/sgbra)=(   2/25/0) (
REDTASK #   3 TIME=  17.000 N=  1 (task/type/sgbra)=(   3/26/1) (
REDTASK #   4 TIME=  16.000 N=  1 (task/type/sgbra)=(   4/27/2) (
REDTASK #   5 TIME=  15.000 N=  1 (task/type/sgbra)=(   5/11/0) (
REDTASK #   6 TIME=  14.000 N=  1 (task/type/sgbra)=(   6/15/0) (
REDTASK #   7 TIME=  13.000 N=  1 (task/type/sgbra)=(   7/16/0) (
REDTASK #   8 TIME=  12.000 N=  1 (task/type/sgbra)=(   8/ 1/0) (
REDTASK #   9 TIME=  11.000 N=  1 (task/type/sgbra)=(   9/ 5/0) (
REDTASK #  10 TIME=  10.000 N=  1 (task/type/sgbra)=(  10/ 6/1) (
REDTASK #  11 TIME=   9.000 N=  1 (task/type/sgbra)=(  11/18/2) (
REDTASK #  12 TIME=   8.000 N=  1 (task/type/sgbra)=(  12/ 7/1) (
REDTASK #  13 TIME=   7.000 N=  1 (task/type/sgbra)=(  13/19/2) (
REDTASK #  14 TIME=   6.000 N=  1 (task/type/sgbra)=(  14/42/1) (
REDTASK #  15 TIME=   5.000 N=  1 (task/type/sgbra)=(  15/43/1) (
REDTASK #  16 TIME=   4.000 N=  1 (task/type/sgbra)=(  16/44/1) (
REDTASK #  17 TIME=   3.000 N=  1 (task/type/sgbra)=(  17/75/1) (
REDTASK #  18 TIME=   2.000 N=  1 (task/type/sgbra)=(  18/76/1) (
REDTASK #  19 TIME=   1.000 N=  1 (task/type/sgbra)=(  19/77/1) (
REDTASK #  20 TIME=   0.000 N=  1 (task/type/sgbra)=(  20/78/1) (
 initializing v-file: 1:                 13970

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      2 vectors will be written to unit 11 beginning with logical record   1

            2 vectors will be created
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        1505 2x:           0 4x:           0
All internal counts: zz :        5629 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:           0    task #     2:           0    task #     3:           0    task #     4:           0
task #     5:           0    task #     6:           0    task #     7:           0    task #     8:        4962
task #     9:           0    task #    10:           0    task #    11:           0    task #    12:           0
task #    13:           0    task #    14:           0    task #    15:           0    task #    16:           0
task #    17:         865    task #    18:           0    task #    19:           0    task #    20:           0
 reference space has dimension     115
 dsyevx: computed roots 1 to    4(converged:   4)

    root           eigenvalues
    ----           ------------
       1         -25.7042693760
       2         -25.6559723467
       3         -25.4338900258
       4         -25.4032302373

 strefv generated    2 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt=                   115

         vector  1 from unit 11 written to unit 49 filename cirefv              
 ufvoutnew: ... writing  recamt=                   115

         vector  2 from unit 11 written to unit 49 filename cirefv              

 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=   115)...
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:             13970
 number of initial trial vectors:                         2
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               7
 number of roots to converge:                             2
 number of iterations:                                    1
 residual norm convergence criteria:               0.001000  0.001000

          starting bk iteration   1

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    2.262592
ci vector #   2dasum_wr=    0.000000
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:        5280 2x:        1480 4x:         370
All internal counts: zz :        5629 yy:           0 xx:           0 ww:           0
One-external counts: yz :        9682 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:         818 wz:         810 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:           0    task #     4:           0
task #     5:        8526    task #     6:           0    task #     7:           0    task #     8:        4962
task #     9:           0    task #    10:           0    task #    11:           0    task #    12:           0
task #    13:           0    task #    14:           0    task #    15:           0    task #    16:           0
task #    17:         865    task #    18:        2004    task #    19:         718    task #    20:         532
 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    2.332551
ci vector #   2dasum_wr=    0.000000
ci vector #   3dasum_wr=    0.000000
ci vector #   4dasum_wr=    0.000000


====================================================================================================
Diagonal     counts:  0x:        5280 2x:        1480 4x:         370
All internal counts: zz :        5629 yy:           0 xx:           0 ww:           0
One-external counts: yz :        9682 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:         818 wz:         810 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:           0    task #     4:           0
task #     5:        8526    task #     6:           0    task #     7:           0    task #     8:        4962
task #     9:           0    task #    10:           0    task #    11:           0    task #    12:           0
task #    13:           0    task #    14:           0    task #    15:           0    task #    16:           0
task #    17:         865    task #    18:        2004    task #    19:         718    task #    20:         532
 Final Overlap matrix sovl

              sovl   1       sovl   2
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 Final subspace hamiltonian 

                ht   1         ht   2
   ht   1   -29.90080727
   ht   2     0.00000000   -29.85251024
Spectrum of overlapmatrix:    1.000000    1.000000

          calcsovref: tciref block   1

              civs   1       civs   2
 refs   1    1.00000       1.788263E-18
 refs   2    0.00000        1.00000    

          calcsovref: scrb block   1

                ci   1         ci   2
 civs   1    1.00000      -1.784698E-18
 civs   2    0.00000        1.00000    

          calcsovref: sovref block   1

              v      1       v      2
 ref    1    1.00000       3.564242E-21
 ref    2    0.00000        1.00000    

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     1.00000000     0.00000000
 ref:   2     0.00000000     1.00000000
 NCSF=                   115                  1890                  6280
                  5685

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -25.7042693760  3.5527E-15  5.4110E-02  3.3283E-01  1.0000E-03
 mr-sdci #  1  2    -25.6559723467 -1.7764E-14  0.0000E+00  3.3458E-01  1.0000E-03
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0014
    2   25    0     0.01     0.01     0.00     0.01         0.    0.0013
    3   26    0     0.00     0.00     0.00     0.00         0.    0.0000
    4   27    0     0.00     0.00     0.00     0.00         0.    0.0000
    5   11    0     0.00     0.00     0.00     0.00         0.    0.0085
    6   15    0     0.00     0.00     0.00     0.00         0.    0.0000
    7   16    0     0.00     0.00     0.00     0.00         0.    0.0000
    8    1    0     0.01     0.01     0.00     0.01         0.    0.0050
    9    5    0     0.00     0.00     0.00     0.00         0.    0.0000
   10    6    0     0.00     0.00     0.00     0.00         0.    0.0000
   11   18    0     0.00     0.00     0.00     0.00         0.    0.0000
   12    7    0     0.00     0.00     0.00     0.00         0.    0.0000
   13   19    0     0.00     0.00     0.00     0.00         0.    0.0000
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0000
   15   43    0     0.00     0.00     0.00     0.00         0.    0.0000
   16   44    0     0.00     0.00     0.00     0.00         0.    0.0000
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0009
   18   76    0     0.00     0.00     0.00     0.00         0.    0.0020
   19   77    0     0.00     0.00     0.00     0.00         0.    0.0007
   20   78    0     0.00     0.00     0.00     0.00         0.    0.0001
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0200s 
time spent in multnx:                   0.0200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0200s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.0400s 

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -25.7042693760  3.5527E-15  5.4110E-02  3.3283E-01  1.0000E-03
 mr-sdci #  1  2    -25.6559723467 -1.7764E-14  0.0000E+00  3.3458E-01  1.0000E-03
 
diagon:itrnv=   2
 expansion vectors are not transformed.
 matrix-vector products are not transformed.

    3 expansion eigenvectors written to unit nvfile (= 11)
    2 matrix-vector products written to unit nhvfil (= 10)

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:             13970
 number of initial trial vectors:                         3
 number of initial matrix-vector products:                2
 maximum dimension of the subspace vectors:               7
 number of roots to converge:                             2
 number of iterations:                                  400
 residual norm convergence criteria:               0.000010  0.000010

          starting ci iteration   1

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.000000
ci vector #   2dasum_wr=    1.714943
ci vector #   3dasum_wr=    0.650784
ci vector #   4dasum_wr=    1.533900


====================================================================================================
Diagonal     counts:  0x:        5280 2x:        1480 4x:         370
All internal counts: zz :        5629 yy:       10577 xx:        5740 ww:        3548
One-external counts: yz :        9682 yx:       10444 yw:        8199
Two-external counts: yy :        4346 ww:        1530 xx:        2750 xz:         818 wz:         810 wx:        2990
Three-ext.   counts: yx :        1415 yw:        1165

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:        2479    task #     4:        1239
task #     5:        8526    task #     6:        9200    task #     7:        7175    task #     8:        4962
task #     9:       11867    task #    10:        2061    task #    11:        2061    task #    12:        1071
task #    13:        1071    task #    14:         535    task #    15:         537    task #    16:         537
task #    17:         865    task #    18:        2004    task #    19:         718    task #    20:         532
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     0.03129289
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3
   ht   1   -29.90080727
   ht   2     0.00000000   -29.85251024
   ht   3    -0.05410964     0.00000000    -0.88074063
Spectrum of overlapmatrix:    0.031293    1.000000    1.000000

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3
 refs   1    1.00000       1.788263E-18  -2.535813E-13
 refs   2    0.00000        1.00000      -2.388317E-15

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3
 civs   1  -0.985981      -2.558940E-08  -0.166856    
 civs   2  -2.455650E-08    1.00000      -8.253525E-09
 civs   3  -0.943235       2.284044E-08    5.57373    

          calcsovref: sovref block   1

              v      1       v      2       v      3
 ref    1  -0.985981      -2.558940E-08  -0.166856    
 ref    2  -2.455650E-08    1.00000      -8.253538E-09

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1    -0.98598122    -0.00000003    -0.16685635
 ref:   2    -0.00000002     1.00000000    -0.00000001
 NCSF=                   115                  1890                  6280
                  5685

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -25.7560331450  5.1764E-02  2.6026E-03  7.2622E-02  1.0000E-05
 mr-sdci #  1  2    -25.6559723467  3.5527E-15  0.0000E+00  3.3458E-01  1.0000E-05
 mr-sdci #  1  3    -23.8967718448  2.8093E+01  0.0000E+00  7.9335E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.01     0.00     0.00     0.01         0.    0.0014
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0013
    3   26    0     0.00     0.00     0.00     0.00         0.    0.0025
    4   27    0     0.00     0.00     0.00     0.00         0.    0.0012
    5   11    0     0.01     0.00     0.00     0.01         0.    0.0085
    6   15    0     0.01     0.01     0.00     0.01         0.    0.0092
    7   16    0     0.03     0.01     0.01     0.02         0.    0.0072
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0050
    9    5    0     0.01     0.00     0.00     0.01         0.    0.0119
   10    6    0     0.01     0.00     0.00     0.01         0.    0.0021
   11   18    0     0.00     0.00     0.00     0.00         0.    0.0021
   12    7    0     0.01     0.00     0.00     0.01         0.    0.0011
   13   19    0     0.00     0.00     0.00     0.00         0.    0.0011
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0005
   15   43    0     0.00     0.00     0.00     0.00         0.    0.0005
   16   44    0     0.01     0.00     0.00     0.01         0.    0.0005
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0009
   18   76    0     0.00     0.00     0.00     0.00         0.    0.0020
   19   77    0     0.00     0.00     0.00     0.00         0.    0.0007
   20   78    0     0.00     0.00     0.00     0.00         0.    0.0001
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1000s 
time spent in multnx:                   0.0900s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.0200s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1000s 

          starting ci iteration   2

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.140327
ci vector #   2dasum_wr=    0.398215
ci vector #   3dasum_wr=    0.419617
ci vector #   4dasum_wr=    0.451883


====================================================================================================
Diagonal     counts:  0x:        5280 2x:        1480 4x:         370
All internal counts: zz :        5629 yy:       10577 xx:        5740 ww:        3548
One-external counts: yz :        9682 yx:       10444 yw:        8199
Two-external counts: yy :        4346 ww:        1530 xx:        2750 xz:         818 wz:         810 wx:        2990
Three-ext.   counts: yx :        1415 yw:        1165

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:        2479    task #     4:        1239
task #     5:        8526    task #     6:        9200    task #     7:        7175    task #     8:        4962
task #     9:       11867    task #    10:        2061    task #    11:        2061    task #    12:        1071
task #    13:        1071    task #    14:         535    task #    15:         537    task #    16:         537
task #    17:         865    task #    18:        2004    task #    19:         718    task #    20:         532
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     0.03129289
 sovl   4    -0.02591904    -0.00000005     0.00062278     0.00233728
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4
   ht   1   -29.90080727
   ht   2     0.00000000   -29.85251024
   ht   3    -0.05410964     0.00000000    -0.88074063
   ht   4     0.77503240     0.00000141    -0.01452595    -0.06668806
Spectrum of overlapmatrix:    0.001651    0.031306    1.000000    1.000673

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4
 refs   1    1.00000       1.788263E-18  -2.535813E-13  -2.591904E-02
 refs   2    0.00000        1.00000      -2.388317E-15  -4.740310E-08

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4
 civs   1   0.963462       8.379852E-09   0.560031       0.405580    
 civs   2  -2.256059E-08   -1.00000       7.427990E-07   8.811288E-07
 civs   3   0.980572      -1.527445E-08   -4.57194        3.21427    
 civs   4  -0.822424      -3.284723E-07    15.7345        18.8859    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4
 ref    1   0.984779       1.689354E-08   0.152208      -8.392559E-02
 ref    2   1.642485E-08   -1.00000      -3.065550E-09  -1.412316E-08

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.98477876     0.00000002     0.15220804    -0.08392559
 ref:   2     0.00000002    -1.00000000     0.00000000    -0.00000001
 NCSF=                   115                  1890                  6280
                  5685

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1    -25.7581747844  2.1416E-03  2.2698E-04  2.0055E-02  1.0000E-05
 mr-sdci #  2  2    -25.6559723467 -1.0658E-14  0.0000E+00  3.3458E-01  1.0000E-05
 mr-sdci #  2  3    -24.0756207737  1.7885E-01  0.0000E+00  8.7525E-01  1.0000E-04
 mr-sdci #  2  4    -23.6391735631  2.7836E+01  0.0000E+00  1.0219E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0014
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0013
    3   26    0     0.00     0.00     0.00     0.00         0.    0.0025
    4   27    0     0.01     0.00     0.00     0.01         0.    0.0012
    5   11    0     0.00     0.00     0.00     0.00         0.    0.0085
    6   15    0     0.02     0.01     0.00     0.02         0.    0.0092
    7   16    0     0.01     0.00     0.00     0.01         0.    0.0072
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0050
    9    5    0     0.01     0.01     0.00     0.01         0.    0.0119
   10    6    0     0.01     0.01     0.00     0.01         0.    0.0021
   11   18    0     0.01     0.00     0.00     0.01         0.    0.0021
   12    7    0     0.00     0.00     0.00     0.00         0.    0.0011
   13   19    0     0.00     0.00     0.00     0.00         0.    0.0011
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0005
   15   43    0     0.01     0.00     0.00     0.01         0.    0.0005
   16   44    0     0.00     0.00     0.00     0.00         0.    0.0005
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0009
   18   76    0     0.00     0.00     0.00     0.00         0.    0.0020
   19   77    0     0.00     0.00     0.00     0.00         0.    0.0007
   20   78    0     0.00     0.01     0.00     0.00         0.    0.0001
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0800s 
time spent in multnx:                   0.0800s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0400s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.0900s 

          starting ci iteration   3

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.049695
ci vector #   2dasum_wr=    0.131153
ci vector #   3dasum_wr=    0.114727
ci vector #   4dasum_wr=    0.114506


====================================================================================================
Diagonal     counts:  0x:        5280 2x:        1480 4x:         370
All internal counts: zz :        5629 yy:       10577 xx:        5740 ww:        3548
One-external counts: yz :        9682 yx:       10444 yw:        8199
Two-external counts: yy :        4346 ww:        1530 xx:        2750 xz:         818 wz:         810 wx:        2990
Three-ext.   counts: yx :        1415 yw:        1165

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:        2479    task #     4:        1239
task #     5:        8526    task #     6:        9200    task #     7:        7175    task #     8:        4962
task #     9:       11867    task #    10:        2061    task #    11:        2061    task #    12:        1071
task #    13:        1071    task #    14:         535    task #    15:         537    task #    16:         537
task #    17:         865    task #    18:        2004    task #    19:         718    task #    20:         532
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     0.03129289
 sovl   4    -0.02591904    -0.00000005     0.00062278     0.00233728
 sovl   5    -0.00637020    -0.00000003     0.00007408     0.00022192     0.00022365
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1   -29.90080727
   ht   2     0.00000000   -29.85251024
   ht   3    -0.05410964     0.00000000    -0.88074063
   ht   4     0.77503240     0.00000141    -0.01452595    -0.06668806
   ht   5     0.19047797     0.00000098    -0.00187889    -0.00636345    -0.00639908
Spectrum of overlapmatrix:    0.000181    0.001653    0.031306    1.000000    1.000714

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1    1.00000       1.788263E-18  -2.535813E-13  -2.591904E-02  -6.370195E-03
 refs   2    0.00000        1.00000      -2.388317E-15  -4.740310E-08  -3.294334E-08

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 civs   1   0.966632       1.319087E-08  -8.728768E-02  -0.512482      -0.607458    
 civs   2  -4.643568E-09   -1.00000       1.283659E-06  -1.498490E-06  -1.741834E-06
 civs   3   0.983770      -8.987766E-09    1.98066        4.76763       -2.14256    
 civs   4  -0.894320      -5.832040E-07   -14.5844       -4.90186       -19.3284    
 civs   5   0.827834       2.886589E-06    58.7605       -37.8799       -25.2043    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1   0.984539       9.918821E-09  -8.358939E-02  -0.144127       5.407095E-02
 ref    2   1.047834E-08   -1.00000       3.923980E-08  -1.823490E-08   4.704416E-09

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     0.98453868     0.00000001    -0.08358939    -0.14412747     0.05407095
 ref:   2     0.00000001    -1.00000000     0.00000004    -0.00000002     0.00000000
 NCSF=                   115                  1890                  6280
                  5685

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1    -25.7583626945  1.8791E-04  2.9481E-05  7.5543E-03  1.0000E-05
 mr-sdci #  3  2    -25.6559723467  3.5527E-15  0.0000E+00  3.3458E-01  1.0000E-05
 mr-sdci #  3  3    -24.4193166106  3.4370E-01  0.0000E+00  9.5114E-01  1.0000E-04
 mr-sdci #  3  4    -23.9122157586  2.7304E-01  0.0000E+00  8.7281E-01  1.0000E-04
 mr-sdci #  3  5    -23.5735569236  2.7770E+01  0.0000E+00  1.1092E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.01     0.00     0.01     0.01         0.    0.0014
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0013
    3   26    0     0.00     0.00     0.00     0.00         0.    0.0025
    4   27    0     0.01     0.00     0.00     0.01         0.    0.0012
    5   11    0     0.01     0.01     0.00     0.01         0.    0.0085
    6   15    0     0.01     0.00     0.00     0.01         0.    0.0092
    7   16    0     0.01     0.00     0.00     0.01         0.    0.0072
    8    1    0     0.01     0.01     0.00     0.01         0.    0.0050
    9    5    0     0.01     0.00     0.00     0.01         0.    0.0119
   10    6    0     0.00     0.00     0.00     0.00         0.    0.0021
   11   18    0     0.01     0.01     0.00     0.01         0.    0.0021
   12    7    0     0.00     0.00     0.00     0.00         0.    0.0011
   13   19    0     0.01     0.01     0.00     0.01         0.    0.0011
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0005
   15   43    0     0.00     0.00     0.00     0.00         0.    0.0005
   16   44    0     0.00     0.00     0.00     0.00         0.    0.0005
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0009
   18   76    0     0.01     0.01     0.00     0.01         0.    0.0020
   19   77    0     0.00     0.00     0.00     0.00         0.    0.0007
   20   78    0     0.00     0.00     0.00     0.00         0.    0.0001
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1000s 
time spent in multnx:                   0.1000s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.0500s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1000s 

          starting ci iteration   4

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.015747
ci vector #   2dasum_wr=    0.054328
ci vector #   3dasum_wr=    0.038554
ci vector #   4dasum_wr=    0.042259


====================================================================================================
Diagonal     counts:  0x:        5280 2x:        1480 4x:         370
All internal counts: zz :        5629 yy:       10577 xx:        5740 ww:        3548
One-external counts: yz :        9682 yx:       10444 yw:        8199
Two-external counts: yy :        4346 ww:        1530 xx:        2750 xz:         818 wz:         810 wx:        2990
Three-ext.   counts: yx :        1415 yw:        1165

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:        2479    task #     4:        1239
task #     5:        8526    task #     6:        9200    task #     7:        7175    task #     8:        4962
task #     9:       11867    task #    10:        2061    task #    11:        2061    task #    12:        1071
task #    13:        1071    task #    14:         535    task #    15:         537    task #    16:         537
task #    17:         865    task #    18:        2004    task #    19:         718    task #    20:         532
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     0.03129289
 sovl   4    -0.02591904    -0.00000005     0.00062278     0.00233728
 sovl   5    -0.00637020    -0.00000003     0.00007408     0.00022192     0.00022365
 sovl   6     0.00242760     0.00000001     0.00006372    -0.00010655    -0.00002724     0.00002773
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1   -29.90080727
   ht   2     0.00000000   -29.85251024
   ht   3    -0.05410964     0.00000000    -0.88074063
   ht   4     0.77503240     0.00000141    -0.01452595    -0.06668806
   ht   5     0.19047797     0.00000098    -0.00187889    -0.00636345    -0.00639908
   ht   6    -0.07258380    -0.00000035    -0.00204331     0.00318850     0.00077959    -0.00079299
Spectrum of overlapmatrix:    0.000020    0.000181    0.001655    0.031306    1.000000    1.000719

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1    1.00000       1.788263E-18  -2.535813E-13  -2.591904E-02  -6.370195E-03   2.427602E-03
 refs   2    0.00000        1.00000      -2.388317E-15  -4.740310E-08  -3.294334E-08   1.188853E-08

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1   0.964788      -5.477337E-08  -0.359812       0.650473      -0.300186      -0.315674    
 civs   2  -1.965926E-08   -1.00000      -2.074406E-07   2.574455E-06  -1.919774E-06   1.357150E-07
 civs   3   0.984178       4.151685E-09    1.34541       -3.10223       -4.48997      -9.543969E-02
 civs   4  -0.904536      -9.792762E-07   -11.9851        2.90144       -1.95838       -22.0548    
 civs   5   0.944945       6.829655E-06    49.8927        52.2396       -16.8268       -13.7147    
 civs   6   0.901991       2.892646E-05    120.261       -60.6196        111.065       -140.752    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.984403      -2.675781E-09  -7.505023E-02   9.533429E-02   0.127384       1.641174E-03
 ref    2   2.812276E-09   -1.00000       1.467876E-07  -4.706382E-09   4.778582E-08  -4.034353E-08

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.98440312     0.00000000    -0.07505023     0.09533429     0.12738384     0.00164117
 ref:   2     0.00000000    -1.00000000     0.00000015     0.00000000     0.00000005    -0.00000004
 NCSF=                   115                  1890                  6280
                  5685

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1    -25.7583892866  2.6592E-05  3.8323E-06  2.6993E-03  1.0000E-05
 mr-sdci #  4  2    -25.6559723467  2.8422E-14  0.0000E+00  3.3458E-01  1.0000E-05
 mr-sdci #  4  3    -24.7372847314  3.1797E-01  0.0000E+00  7.6538E-01  1.0000E-04
 mr-sdci #  4  4    -23.9425355718  3.0320E-02  0.0000E+00  1.0328E+00  1.0000E-04
 mr-sdci #  4  5    -23.7950555948  2.2150E-01  0.0000E+00  1.0431E+00  1.0000E-04
 mr-sdci #  4  6    -23.3198226566  2.7516E+01  0.0000E+00  1.0485E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0014
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0013
    3   26    0     0.01     0.01     0.00     0.01         0.    0.0025
    4   27    0     0.00     0.00     0.00     0.00         0.    0.0012
    5   11    0     0.01     0.01     0.00     0.01         0.    0.0085
    6   15    0     0.02     0.01     0.00     0.02         0.    0.0092
    7   16    0     0.01     0.00     0.00     0.01         0.    0.0072
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0050
    9    5    0     0.01     0.01     0.00     0.01         0.    0.0119
   10    6    0     0.01     0.00     0.00     0.01         0.    0.0021
   11   18    0     0.00     0.00     0.00     0.00         0.    0.0021
   12    7    0     0.01     0.01     0.00     0.01         0.    0.0011
   13   19    0     0.00     0.00     0.00     0.00         0.    0.0011
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0005
   15   43    0     0.00     0.00     0.00     0.00         0.    0.0005
   16   44    0     0.01     0.00     0.00     0.01         0.    0.0005
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0009
   18   76    0     0.00     0.00     0.00     0.00         0.    0.0020
   19   77    0     0.00     0.00     0.00     0.00         0.    0.0007
   20   78    0     0.00     0.00     0.00     0.00         0.    0.0001
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0900s 
time spent in multnx:                   0.0900s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0500s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1100s 

          starting ci iteration   5

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.006307
ci vector #   2dasum_wr=    0.016987
ci vector #   3dasum_wr=    0.016999
ci vector #   4dasum_wr=    0.017466


====================================================================================================
Diagonal     counts:  0x:        5280 2x:        1480 4x:         370
All internal counts: zz :        5629 yy:       10577 xx:        5740 ww:        3548
One-external counts: yz :        9682 yx:       10444 yw:        8199
Two-external counts: yy :        4346 ww:        1530 xx:        2750 xz:         818 wz:         810 wx:        2990
Three-ext.   counts: yx :        1415 yw:        1165

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:        2479    task #     4:        1239
task #     5:        8526    task #     6:        9200    task #     7:        7175    task #     8:        4962
task #     9:       11867    task #    10:        2061    task #    11:        2061    task #    12:        1071
task #    13:        1071    task #    14:         535    task #    15:         537    task #    16:         537
task #    17:         865    task #    18:        2004    task #    19:         718    task #    20:         532
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6       sovl   7
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     0.03129289
 sovl   4    -0.02591904    -0.00000005     0.00062278     0.00233728
 sovl   5    -0.00637020    -0.00000003     0.00007408     0.00022192     0.00022365
 sovl   6     0.00242760     0.00000001     0.00006372    -0.00010655    -0.00002724     0.00002773
 sovl   7    -0.00041896     0.00000000     0.00000309     0.00000600     0.00001116    -0.00000177     0.00000350
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1   -29.90080727
   ht   2     0.00000000   -29.85251024
   ht   3    -0.05410964     0.00000000    -0.88074063
   ht   4     0.77503240     0.00000141    -0.01452595    -0.06668806
   ht   5     0.19047797     0.00000098    -0.00187889    -0.00636345    -0.00639908
   ht   6    -0.07258380    -0.00000035    -0.00204331     0.00318850     0.00077959    -0.00079299
   ht   7     0.01252744     0.00000014    -0.00006993    -0.00017924    -0.00033404     0.00004882    -0.00010024
Spectrum of overlapmatrix:    0.000003    0.000020    0.000182    0.001655    0.031306    1.000000    1.000720

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1    1.00000       1.788263E-18  -2.535813E-13  -2.591904E-02  -6.370195E-03   2.427602E-03  -4.189610E-04
 refs   2    0.00000        1.00000      -2.388317E-15  -4.740310E-08  -3.294334E-08   1.188853E-08  -4.715971E-09

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1  -0.964919      -4.629267E-08   0.126849       0.742060       0.313137      -4.916994E-02  -0.299083    
 civs   2   1.956867E-08   -1.00000      -1.334472E-06   2.963965E-06   1.968113E-06  -2.695165E-07   4.760785E-07
 civs   3  -0.984221       1.281382E-08  -0.606665       -3.04283        4.40742        1.56219       0.340605    
 civs   4   0.905934      -1.235009E-06    6.87112        11.2732        1.79752        13.9451       -16.7107    
 civs   5  -0.961177       9.252697E-06   -37.5224        7.91685        18.9502       -53.8192       -41.4160    
 civs   6   -1.02701       4.391499E-05   -107.664       -73.9764       -112.842        72.2055       -124.145    
 civs   7  -0.961833       1.057372E-04   -295.047        271.679       -7.28088        351.365        249.052    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1  -0.984367      -1.091557E-08   5.002868E-02   0.106029      -0.125055      -3.969771E-02  -7.845606E-03
 ref    2   6.152969E-10   -1.00000      -3.126094E-07   8.070231E-09  -4.856596E-08   4.381404E-08  -1.781726E-08

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.98436724    -0.00000001     0.05002868     0.10602908    -0.12505453    -0.03969771    -0.00784561
 ref:   2     0.00000000    -1.00000000    -0.00000031     0.00000001    -0.00000005     0.00000004    -0.00000002

 trial vector basis is being transformed.  new dimension:   4
 NCSF=                   115                  1890                  6280
                  5685

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1    -25.7583929726  3.6860E-06  6.2756E-07  1.0565E-03  1.0000E-05
 mr-sdci #  5  2    -25.6559723467  2.4869E-14  0.0000E+00  3.3458E-01  1.0000E-05
 mr-sdci #  5  3    -25.0550871381  3.1780E-01  0.0000E+00  5.8428E-01  1.0000E-04
 mr-sdci #  5  4    -24.1496763327  2.0714E-01  0.0000E+00  9.7814E-01  1.0000E-04
 mr-sdci #  5  5    -23.7951794759  1.2388E-04  0.0000E+00  1.0540E+00  1.0000E-04
 mr-sdci #  5  6    -23.5886016405  2.6878E-01  0.0000E+00  1.0395E+00  1.0000E-04
 mr-sdci #  5  7    -23.2161942201  2.7413E+01  0.0000E+00  1.1423E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0014
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0013
    3   26    0     0.00     0.00     0.00     0.00         0.    0.0025
    4   27    0     0.01     0.00     0.00     0.01         0.    0.0012
    5   11    0     0.01     0.01     0.00     0.01         0.    0.0085
    6   15    0     0.01     0.00     0.00     0.01         0.    0.0092
    7   16    0     0.01     0.00     0.00     0.01         0.    0.0072
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0050
    9    5    0     0.01     0.00     0.00     0.01         0.    0.0119
   10    6    0     0.01     0.00     0.00     0.01         0.    0.0021
   11   18    0     0.01     0.01     0.00     0.01         0.    0.0021
   12    7    0     0.00     0.00     0.00     0.00         0.    0.0011
   13   19    0     0.01     0.01     0.00     0.01         0.    0.0011
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0005
   15   43    0     0.00     0.00     0.00     0.00         0.    0.0005
   16   44    0     0.00     0.00     0.00     0.00         0.    0.0005
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0009
   18   76    0     0.00     0.00     0.00     0.00         0.    0.0020
   19   77    0     0.00     0.00     0.00     0.00         0.    0.0007
   20   78    0     0.00     0.00     0.00     0.00         0.    0.0001
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0800s 
time spent in multnx:                   0.0800s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0300s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1000s 

          starting ci iteration   6

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.002881
ci vector #   2dasum_wr=    0.006956
ci vector #   3dasum_wr=    0.006512
ci vector #   4dasum_wr=    0.006351


====================================================================================================
Diagonal     counts:  0x:        5280 2x:        1480 4x:         370
All internal counts: zz :        5629 yy:       10577 xx:        5740 ww:        3548
One-external counts: yz :        9682 yx:       10444 yw:        8199
Two-external counts: yy :        4346 ww:        1530 xx:        2750 xz:         818 wz:         810 wx:        2990
Three-ext.   counts: yx :        1415 yw:        1165

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:        2479    task #     4:        1239
task #     5:        8526    task #     6:        9200    task #     7:        7175    task #     8:        4962
task #     9:       11867    task #    10:        2061    task #    11:        2061    task #    12:        1071
task #    13:        1071    task #    14:         535    task #    15:         537    task #    16:         537
task #    17:         865    task #    18:        2004    task #    19:         718    task #    20:         532
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00036212     0.00000000     0.00002770     0.00010037     0.00000065
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1   -29.95493087
   ht   2     0.00000000   -29.85251024
   ht   3     0.00000000     0.00000000   -29.25162503
   ht   4     0.00000000     0.00000000     0.00000000   -28.34621423
   ht   5    -0.01084789    -0.00000003    -0.00103524    -0.00289211    -0.00001878
Spectrum of overlapmatrix:    0.000001    1.000000    1.000000    1.000000    1.000000

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1  -0.984367      -1.091557E-08   5.002868E-02   0.106029      -3.681188E-04
 refs   2   6.152969E-10   -1.00000      -3.126094E-07   8.070231E-09  -9.482402E-10

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 civs   1   0.999692      -1.508613E-07   0.155458       0.234582      -0.421425    
 civs   2  -3.080176E-09   -1.00000       1.614956E-07   4.797209E-07  -8.815143E-07
 civs   3   2.487037E-04   1.420313E-07  -0.939880       0.185148      -0.289558    
 civs   4  -6.061876E-05  -2.836204E-08   2.301757E-02  -0.805510      -0.608528    
 civs   5   0.851226       4.096741E-04   -428.068       -647.091        1162.60    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1  -0.984371       1.270816E-08  -4.002821E-02  -6.885347E-02  -9.214653E-02
 ref    2   2.809880E-09    1.00000       5.385129E-07   6.964175E-08  -1.355644E-07

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1    -0.98437093     0.00000001    -0.04002821    -0.06885347    -0.09214653
 ref:   2     0.00000000     1.00000000     0.00000054     0.00000007    -0.00000014
 NCSF=                   115                  1890                  6280
                  5685

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  6  1    -25.7583935068  5.3420E-07  1.5451E-07  5.2101E-04  1.0000E-05
 mr-sdci #  6  2    -25.6559723467  1.1369E-13  0.0000E+00  3.3458E-01  1.0000E-05
 mr-sdci #  6  3    -25.1562720133  1.0118E-01  0.0000E+00  4.8554E-01  1.0000E-04
 mr-sdci #  6  4    -24.1845486565  3.4872E-02  0.0000E+00  1.0445E+00  1.0000E-04
 mr-sdci #  6  5    -24.0387900266  2.4361E-01  0.0000E+00  1.1102E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0014
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0013
    3   26    0     0.00     0.00     0.00     0.00         0.    0.0025
    4   27    0     0.01     0.01     0.00     0.01         0.    0.0012
    5   11    0     0.01     0.01     0.00     0.01         0.    0.0085
    6   15    0     0.01     0.00     0.00     0.01         0.    0.0092
    7   16    0     0.01     0.01     0.00     0.01         0.    0.0072
    8    1    0     0.01     0.01     0.00     0.01         0.    0.0050
    9    5    0     0.01     0.01     0.00     0.01         0.    0.0119
   10    6    0     0.00     0.00     0.00     0.00         0.    0.0021
   11   18    0     0.01     0.01     0.00     0.01         0.    0.0021
   12    7    0     0.01     0.00     0.00     0.01         0.    0.0011
   13   19    0     0.00     0.00     0.00     0.00         0.    0.0011
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0005
   15   43    0     0.00     0.00     0.00     0.00         0.    0.0005
   16   44    0     0.00     0.00     0.00     0.00         0.    0.0005
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0009
   18   76    0     0.01     0.01     0.00     0.01         0.    0.0020
   19   77    0     0.00     0.00     0.00     0.00         0.    0.0007
   20   78    0     0.00     0.00     0.00     0.00         0.    0.0001
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0900s 
time spent in multnx:                   0.0900s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0700s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.0900s 

          starting ci iteration   7

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.001338
ci vector #   2dasum_wr=    0.003378
ci vector #   3dasum_wr=    0.002533
ci vector #   4dasum_wr=    0.002850


====================================================================================================
Diagonal     counts:  0x:        5280 2x:        1480 4x:         370
All internal counts: zz :        5629 yy:       10577 xx:        5740 ww:        3548
One-external counts: yz :        9682 yx:       10444 yw:        8199
Two-external counts: yy :        4346 ww:        1530 xx:        2750 xz:         818 wz:         810 wx:        2990
Three-ext.   counts: yx :        1415 yw:        1165

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:        2479    task #     4:        1239
task #     5:        8526    task #     6:        9200    task #     7:        7175    task #     8:        4962
task #     9:       11867    task #    10:        2061    task #    11:        2061    task #    12:        1071
task #    13:        1071    task #    14:         535    task #    15:         537    task #    16:         537
task #    17:         865    task #    18:        2004    task #    19:         718    task #    20:         532
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00036212     0.00000000     0.00002770     0.00010037     0.00000065
 sovl   6    -0.00023245     0.00000000     0.00009189    -0.00006260    -0.00000013     0.00000018
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1   -29.95493087
   ht   2     0.00000000   -29.85251024
   ht   3     0.00000000     0.00000000   -29.25162503
   ht   4     0.00000000     0.00000000     0.00000000   -28.34621423
   ht   5    -0.01084789    -0.00000003    -0.00103524    -0.00289211    -0.00001878
   ht   6     0.00696293     0.00000003    -0.00273311     0.00189909     0.00000357    -0.00000531
Spectrum of overlapmatrix:    0.000000    0.000001    1.000000    1.000000    1.000000    1.000000

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1  -0.984367      -1.091557E-08   5.002868E-02   0.106029      -3.681188E-04   2.438675E-04
 refs   2   6.152969E-10   -1.00000      -3.126094E-07   8.070231E-09  -9.482402E-10   8.663289E-10

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1   0.999824       8.228192E-09   1.767044E-02   0.183048       0.294216      -0.710022    
 civs   2  -4.019475E-09   -1.00000      -8.188433E-07   1.325311E-06   4.939581E-07  -2.501869E-06
 civs   3   2.898824E-04   2.324652E-07  -0.786019      -0.585081       0.301779       0.163879    
 civs   4  -9.074669E-05  -7.434892E-08   7.653222E-02  -0.525326      -0.610590      -0.625717    
 civs   5    1.07976       7.274278E-04   -585.643        649.478       -953.709        570.926    
 civs   6   0.928203       1.187802E-03   -839.428        1800.66       -221.636       -2164.46    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.984361       2.844932E-08  -3.772601E-02  -6.512032E-02  -4.223065E-02  -9.723340E-02
 ref    2   4.323567E-09    1.00000       8.932986E-07  -2.024359E-07   1.192916E-07   2.864170E-08

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.98436059     0.00000003    -0.03772601    -0.06512032    -0.04223065    -0.09723340
 ref:   2     0.00000000     1.00000000     0.00000089    -0.00000020     0.00000012     0.00000003
 NCSF=                   115                  1890                  6280
                  5685

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  7  1    -25.7583936502  1.4342E-07  2.5360E-08  2.1071E-04  1.0000E-05
 mr-sdci #  7  2    -25.6559723467  2.2382E-13  0.0000E+00  3.3458E-01  1.0000E-05
 mr-sdci #  7  3    -25.2480889327  9.1817E-02  0.0000E+00  4.2007E-01  1.0000E-04
 mr-sdci #  7  4    -24.4884955906  3.0395E-01  0.0000E+00  8.8661E-01  1.0000E-04
 mr-sdci #  7  5    -24.1743687726  1.3558E-01  0.0000E+00  1.0989E+00  1.0000E-04
 mr-sdci #  7  6    -23.4643072596 -1.2429E-01  0.0000E+00  1.0798E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0014
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0013
    3   26    0     0.01     0.01     0.00     0.01         0.    0.0025
    4   27    0     0.00     0.00     0.00     0.00         0.    0.0012
    5   11    0     0.01     0.01     0.00     0.01         0.    0.0085
    6   15    0     0.01     0.01     0.00     0.01         0.    0.0092
    7   16    0     0.02     0.02     0.00     0.02         0.    0.0072
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0050
    9    5    0     0.01     0.00     0.00     0.01         0.    0.0119
   10    6    0     0.01     0.00     0.01     0.01         0.    0.0021
   11   18    0     0.00     0.00     0.00     0.00         0.    0.0021
   12    7    0     0.01     0.01     0.00     0.01         0.    0.0011
   13   19    0     0.00     0.00     0.00     0.00         0.    0.0011
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0005
   15   43    0     0.00     0.00     0.00     0.00         0.    0.0005
   16   44    0     0.01     0.00     0.00     0.01         0.    0.0005
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0009
   18   76    0     0.00     0.00     0.00     0.00         0.    0.0020
   19   77    0     0.00     0.00     0.00     0.00         0.    0.0007
   20   78    0     0.00     0.00     0.00     0.00         0.    0.0001
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0900s 
time spent in multnx:                   0.0900s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.0600s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1000s 

          starting ci iteration   8

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.000648
ci vector #   2dasum_wr=    0.001329
ci vector #   3dasum_wr=    0.001323
ci vector #   4dasum_wr=    0.001313


====================================================================================================
Diagonal     counts:  0x:        5280 2x:        1480 4x:         370
All internal counts: zz :        5629 yy:       10577 xx:        5740 ww:        3548
One-external counts: yz :        9682 yx:       10444 yw:        8199
Two-external counts: yy :        4346 ww:        1530 xx:        2750 xz:         818 wz:         810 wx:        2990
Three-ext.   counts: yx :        1415 yw:        1165

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:        2479    task #     4:        1239
task #     5:        8526    task #     6:        9200    task #     7:        7175    task #     8:        4962
task #     9:       11867    task #    10:        2061    task #    11:        2061    task #    12:        1071
task #    13:        1071    task #    14:         535    task #    15:         537    task #    16:         537
task #    17:         865    task #    18:        2004    task #    19:         718    task #    20:         532
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6       sovl   7
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00036212     0.00000000     0.00002770     0.00010037     0.00000065
 sovl   6    -0.00023245     0.00000000     0.00009189    -0.00006260    -0.00000013     0.00000018
 sovl   7     0.00011676     0.00000000    -0.00000522     0.00001259     0.00000008    -0.00000003     0.00000004
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1   -29.95493087
   ht   2     0.00000000   -29.85251024
   ht   3     0.00000000     0.00000000   -29.25162503
   ht   4     0.00000000     0.00000000     0.00000000   -28.34621423
   ht   5    -0.01084789    -0.00000003    -0.00103524    -0.00289211    -0.00001878
   ht   6     0.00696293     0.00000003    -0.00273311     0.00189909     0.00000357    -0.00000531
   ht   7    -0.00349740    -0.00000001     0.00014988    -0.00036934    -0.00000253     0.00000094    -0.00000106
Spectrum of overlapmatrix:    0.000000    0.000000    0.000001    1.000000    1.000000    1.000000    1.000000

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1  -0.984367      -1.091557E-08   5.002868E-02   0.106029      -3.681188E-04   2.438675E-04  -1.202058E-04
 refs   2   6.152969E-10   -1.00000      -3.126094E-07   8.070231E-09  -9.482402E-10   8.663289E-10  -2.116269E-10

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1   0.999746      -2.749174E-07  -0.169986       0.281629      -0.473228       0.755090      -0.321611    
 civs   2  -4.714077E-09   -1.00000       1.067149E-06  -4.073886E-07  -1.334947E-06   2.587359E-06  -4.612417E-07
 civs   3   3.038017E-04   3.119595E-07   0.653959       0.734339       7.170110E-02  -0.210382       0.253214    
 civs   4  -9.936819E-05  -1.106578E-07  -8.849621E-02   0.145342       0.775592       0.644193      -3.940449E-02
 civs   5    1.11390       8.980051E-04    621.718       -448.174        221.602       -379.933       -1244.31    
 civs   6    1.06684       1.751834E-03    1069.60       -1189.09       -934.985        2259.55       -381.930    
 civs   7   0.844690       3.010046E-03    1649.67       -3386.96        1503.58       -789.504        5856.07    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1  -0.984363       2.022724E-08   2.433675E-02   5.705480E-02   6.132546E-02   0.100287      -1.394860E-02
 ref    2   4.922678E-09    1.00000      -1.284430E-06   2.907780E-07  -1.983343E-08  -3.106585E-08  -8.709271E-09

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.98436349     0.00000002     0.02433675     0.05705480     0.06132546     0.10028682    -0.01394860
 ref:   2     0.00000000     1.00000000    -0.00000128     0.00000029    -0.00000002    -0.00000003    -0.00000001

 trial vector basis is being transformed.  new dimension:   4
 NCSF=                   115                  1890                  6280
                  5685

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  8  1    -25.7583936717  2.1421E-08  4.3006E-09  8.6648E-05  1.0000E-05
 mr-sdci #  8  2    -25.6559723467  2.3803E-13  0.0000E+00  3.3458E-01  1.0000E-05
 mr-sdci #  8  3    -25.3085693535  6.0480E-02  0.0000E+00  3.5815E-01  1.0000E-04
 mr-sdci #  8  4    -24.7947729907  3.0628E-01  0.0000E+00  6.3052E-01  1.0000E-04
 mr-sdci #  8  5    -24.3160807223  1.4171E-01  0.0000E+00  9.8533E-01  1.0000E-04
 mr-sdci #  8  6    -23.4691764360  4.8692E-03  0.0000E+00  1.0651E+00  1.0000E-04
 mr-sdci #  8  7    -23.2206839353  4.4897E-03  0.0000E+00  1.2220E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0014
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0013
    3   26    0     0.01     0.00     0.00     0.01         0.    0.0025
    4   27    0     0.00     0.00     0.00     0.00         0.    0.0012
    5   11    0     0.01     0.01     0.00     0.01         0.    0.0085
    6   15    0     0.01     0.00     0.00     0.01         0.    0.0092
    7   16    0     0.01     0.01     0.00     0.01         0.    0.0072
    8    1    0     0.01     0.01     0.00     0.01         0.    0.0050
    9    5    0     0.01     0.01     0.00     0.01         0.    0.0119
   10    6    0     0.01     0.00     0.00     0.01         0.    0.0021
   11   18    0     0.00     0.00     0.00     0.00         0.    0.0021
   12    7    0     0.01     0.00     0.00     0.01         0.    0.0011
   13   19    0     0.00     0.00     0.00     0.00         0.    0.0011
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0005
   15   43    0     0.00     0.00     0.00     0.00         0.    0.0005
   16   44    0     0.01     0.00     0.00     0.01         0.    0.0005
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0009
   18   76    0     0.00     0.00     0.00     0.00         0.    0.0020
   19   77    0     0.00     0.00     0.00     0.00         0.    0.0007
   20   78    0     0.00     0.00     0.00     0.00         0.    0.0001
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0900s 
time spent in multnx:                   0.0900s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0400s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1000s 

          starting ci iteration   9

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.000257
ci vector #   2dasum_wr=    0.000602
ci vector #   3dasum_wr=    0.000524
ci vector #   4dasum_wr=    0.000514


====================================================================================================
Diagonal     counts:  0x:        5280 2x:        1480 4x:         370
All internal counts: zz :        5629 yy:       10577 xx:        5740 ww:        3548
One-external counts: yz :        9682 yx:       10444 yw:        8199
Two-external counts: yy :        4346 ww:        1530 xx:        2750 xz:         818 wz:         810 wx:        2990
Three-ext.   counts: yx :        1415 yw:        1165

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:        2479    task #     4:        1239
task #     5:        8526    task #     6:        9200    task #     7:        7175    task #     8:        4962
task #     9:       11867    task #    10:        2061    task #    11:        2061    task #    12:        1071
task #    13:        1071    task #    14:         535    task #    15:         537    task #    16:         537
task #    17:         865    task #    18:        2004    task #    19:         718    task #    20:         532
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5    -0.00003822     0.00000000     0.00000733    -0.00000119     0.00000000
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1   -29.95493156
   ht   2     0.00000000   -29.85251024
   ht   3     0.00000000     0.00000000   -29.50510725
   ht   4     0.00000000     0.00000000     0.00000000   -28.99131088
   ht   5     0.00114498    -0.00000001    -0.00022742     0.00005325    -0.00000014

                v:   1         v:   2         v:   3         v:   4         v:   5

   eig(s)   3.475028E-09    1.00000        1.00000        1.00000        1.00000    
 
   x:   1   3.822346E-05  -0.189699       4.415253E-06  -2.060498E-02  -0.981626    
   x:   2  -1.674102E-10   9.823023E-07    1.00000       4.175013E-07   4.299304E-06
   x:   3  -7.334900E-06  -0.946146       2.294705E-07  -0.263294       0.188369    
   x:   4   1.185733E-06   0.262338       2.759026E-07  -0.964495      -3.045139E-02
   x:   5    1.00000       2.678094E-12    0.00000      -1.108592E-11   3.893892E-05
 bummer (warning):overlap matrix: # small eigenvalues=                      1

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1  -0.984363       2.022724E-08   2.433675E-02   5.705480E-02   3.990082E-05
 refs   2   4.922678E-09    1.00000      -1.284430E-06   2.907780E-07   1.588178E-10

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 civs   1   -1.00004       3.834214E-07  -0.130659       0.247047       0.585031    
 civs   2  -2.198188E-10    1.00000       1.026852E-06  -1.447242E-06  -2.962851E-06
 civs   3  -1.614219E-05   2.445068E-07  -0.947407      -0.235125      -0.250238    
 civs   4   1.733494E-05  -2.081687E-07   0.112748      -0.897632       0.426556    
 civs   5  -0.942102       1.004209E-02   -3419.24        6464.10        15306.6    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1   0.984362       3.756212E-08  -2.443810E-02  -4.219725E-02   5.310904E-02
 ref    2  -5.266522E-09    1.00000       1.732834E-06  -3.784212E-07  -8.356634E-08

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     0.98436195     0.00000004    -0.02443810    -0.04219725     0.05310904
 ref:   2    -0.00000001     1.00000000     0.00000173    -0.00000038    -0.00000008
 NCSF=                   115                  1890                  6280
                  5685

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  9  1    -25.7583936757  4.0517E-09  1.1811E-09  4.5509E-05  1.0000E-05
 mr-sdci #  9  2    -25.6559723467  4.3343E-13  0.0000E+00  3.3458E-01  1.0000E-05
 mr-sdci #  9  3    -25.3472690461  3.8700E-02  0.0000E+00  3.2044E-01  1.0000E-04
 mr-sdci #  9  4    -24.9295356433  1.3476E-01  0.0000E+00  5.7992E-01  1.0000E-04
 mr-sdci #  9  5    -24.0874162477 -2.2866E-01  0.0000E+00  1.0475E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0014
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0013
    3   26    0     0.00     0.00     0.00     0.00         0.    0.0025
    4   27    0     0.01     0.00     0.00     0.01         0.    0.0012
    5   11    0     0.01     0.00     0.00     0.01         0.    0.0085
    6   15    0     0.01     0.00     0.00     0.01         0.    0.0092
    7   16    0     0.01     0.00     0.00     0.01         0.    0.0072
    8    1    0     0.01     0.01     0.00     0.01         0.    0.0050
    9    5    0     0.01     0.00     0.00     0.01         0.    0.0119
   10    6    0     0.00     0.00     0.00     0.00         0.    0.0021
   11   18    0     0.01     0.01     0.00     0.01         0.    0.0021
   12    7    0     0.00     0.00     0.00     0.00         0.    0.0011
   13   19    0     0.01     0.01     0.00     0.01         0.    0.0011
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0005
   15   43    0     0.00     0.00     0.00     0.00         0.    0.0005
   16   44    0     0.00     0.00     0.00     0.00         0.    0.0005
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0009
   18   76    0     0.01     0.01     0.00     0.01         0.    0.0020
   19   77    0     0.00     0.00     0.00     0.00         0.    0.0007
   20   78    0     0.00     0.00     0.00     0.00         0.    0.0001
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0900s 
time spent in multnx:                   0.0900s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0400s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.0900s 

          starting ci iteration  10

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.000127
ci vector #   2dasum_wr=    0.000306
ci vector #   3dasum_wr=    0.000238
ci vector #   4dasum_wr=    0.000252


====================================================================================================
Diagonal     counts:  0x:        5280 2x:        1480 4x:         370
All internal counts: zz :        5629 yy:       10577 xx:        5740 ww:        3548
One-external counts: yz :        9682 yx:       10444 yw:        8199
Two-external counts: yy :        4346 ww:        1530 xx:        2750 xz:         818 wz:         810 wx:        2990
Three-ext.   counts: yx :        1415 yw:        1165

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:        2479    task #     4:        1239
task #     5:        8526    task #     6:        9200    task #     7:        7175    task #     8:        4962
task #     9:       11867    task #    10:        2061    task #    11:        2061    task #    12:        1071
task #    13:        1071    task #    14:         535    task #    15:         537    task #    16:         537
task #    17:         865    task #    18:        2004    task #    19:         718    task #    20:         532
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5    -0.00003822     0.00000000     0.00000733    -0.00000119     0.00000000
 sovl   6    -0.00002664     0.00000000    -0.00000358     0.00000785     0.00000000     0.00000000
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1   -29.95493156
   ht   2     0.00000000   -29.85251024
   ht   3     0.00000000     0.00000000   -29.50510725
   ht   4     0.00000000     0.00000000     0.00000000   -28.99131088
   ht   5     0.00114498    -0.00000001    -0.00022742     0.00005325    -0.00000014
   ht   6     0.00079806     0.00000000     0.00010695    -0.00023074    -0.00000004    -0.00000005

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6

   eig(s)   8.559749E-10   3.506341E-09    1.00000        1.00000        1.00000        1.00000    
 
   x:   1  -2.232950E-05   4.089286E-05  -7.985390E-02  -7.985757E-02   7.232618E-03   0.993576    
   x:   2   4.696540E-11  -1.735434E-10   0.707100      -0.707114       4.137204E-06  -3.729497E-06
   x:   3  -4.351913E-06  -6.902765E-06  -0.496509      -0.496495       0.706933      -8.495575E-02
   x:   4   7.933750E-06   3.252981E-07  -0.497103      -0.497098      -0.707244      -7.475766E-02
   x:   5   0.108695       0.994075      -8.788924E-13  -8.788369E-13   5.747429E-06  -3.851242E-05
   x:   6  -0.994075       0.108695        0.00000        0.00000      -8.273405E-06  -2.675408E-05
 bummer (warning):overlap matrix: # small eigenvalues=                      2

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1  -0.984363       2.022724E-08   2.433675E-02   5.705480E-02   3.990082E-05   2.767224E-05
 refs   2   4.922678E-09    1.00000      -1.284430E-06   2.907780E-07   1.588178E-10   7.272764E-11

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1   -1.00002       3.553997E-08  -3.788208E-02   2.479535E-02   0.131487        1.01982    
 civs   2  -4.661991E-10    1.00000       1.525248E-06  -1.340094E-06   9.899822E-07  -3.259682E-06
 civs   3  -2.040494E-05   4.291855E-07  -0.862471      -0.446304       0.303137      -2.790889E-02
 civs   4   1.776172E-05  -2.560003E-07   0.146613      -0.695588      -0.749529      -8.020827E-02
 civs   5   -1.21188       1.707568E-02   -5216.22        7800.31       -10752.6        9589.48    
 civs   6   0.982342      -2.313765E-02    6059.62       -10258.7        20360.9        24521.4    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.984363       2.214540E-08  -1.578351E-02  -4.759922E-02  -3.042409E-02   5.205788E-02
 ref    2  -5.478627E-09    1.00000       2.287749E-06  -4.762534E-07   1.564202E-07   6.422289E-08

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.98436267     0.00000002    -0.01578351    -0.04759922    -0.03042409     0.05205788
 ref:   2    -0.00000001     1.00000000     0.00000229    -0.00000048     0.00000016     0.00000006
 NCSF=                   115                  1890                  6280
                  5685

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 10  1    -25.7583936769  1.1602E-09  1.6466E-10  1.6900E-05  1.0000E-05
 mr-sdci # 10  2    -25.6559723467  5.7199E-13  0.0000E+00  3.3458E-01  1.0000E-05
 mr-sdci # 10  3    -25.3802983382  3.3029E-02  0.0000E+00  2.7494E-01  1.0000E-04
 mr-sdci # 10  4    -25.0229104014  9.3375E-02  0.0000E+00  5.1337E-01  1.0000E-04
 mr-sdci # 10  5    -24.3330711195  2.4565E-01  0.0000E+00  8.7471E-01  1.0000E-04
 mr-sdci # 10  6    -23.7585693260  2.8939E-01  0.0000E+00  1.1488E+00  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.01     0.00     0.00     0.01         0.    0.0014
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0013
    3   26    0     0.00     0.00     0.00     0.00         0.    0.0025
    4   27    0     0.01     0.00     0.00     0.01         0.    0.0012
    5   11    0     0.00     0.00     0.00     0.00         0.    0.0085
    6   15    0     0.02     0.01     0.00     0.02         0.    0.0092
    7   16    0     0.01     0.00     0.00     0.01         0.    0.0072
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0050
    9    5    0     0.01     0.01     0.00     0.01         0.    0.0119
   10    6    0     0.01     0.00     0.00     0.01         0.    0.0021
   11   18    0     0.01     0.01     0.00     0.01         0.    0.0021
   12    7    0     0.00     0.00     0.00     0.00         0.    0.0011
   13   19    0     0.01     0.01     0.00     0.01         0.    0.0011
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0005
   15   43    0     0.00     0.00     0.00     0.00         0.    0.0005
   16   44    0     0.00     0.00     0.00     0.00         0.    0.0005
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0009
   18   76    0     0.00     0.00     0.00     0.00         0.    0.0020
   19   77    0     0.00     0.00     0.00     0.00         0.    0.0007
   20   78    0     0.00     0.00     0.00     0.00         0.    0.0001
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.010000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0900s 
time spent in multnx:                   0.0900s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0400s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1000s 

          starting ci iteration  11

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.000047
ci vector #   2dasum_wr=    0.000106
ci vector #   3dasum_wr=    0.000103
ci vector #   4dasum_wr=    0.000105


====================================================================================================
Diagonal     counts:  0x:        5280 2x:        1480 4x:         370
All internal counts: zz :        5629 yy:       10577 xx:        5740 ww:        3548
One-external counts: yz :        9682 yx:       10444 yw:        8199
Two-external counts: yy :        4346 ww:        1530 xx:        2750 xz:         818 wz:         810 wx:        2990
Three-ext.   counts: yx :        1415 yw:        1165

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:        2479    task #     4:        1239
task #     5:        8526    task #     6:        9200    task #     7:        7175    task #     8:        4962
task #     9:       11867    task #    10:        2061    task #    11:        2061    task #    12:        1071
task #    13:        1071    task #    14:         535    task #    15:         537    task #    16:         537
task #    17:         865    task #    18:        2004    task #    19:         718    task #    20:         532
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6       sovl   7
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5    -0.00003822     0.00000000     0.00000733    -0.00000119     0.00000000
 sovl   6    -0.00002664     0.00000000    -0.00000358     0.00000785     0.00000000     0.00000000
 sovl   7     0.00001101     0.00000000    -0.00000091    -0.00000190     0.00000000     0.00000000     0.00000000
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1   -29.95493156
   ht   2     0.00000000   -29.85251024
   ht   3     0.00000000     0.00000000   -29.50510725
   ht   4     0.00000000     0.00000000     0.00000000   -28.99131088
   ht   5     0.00114498    -0.00000001    -0.00022742     0.00005325    -0.00000014
   ht   6     0.00079806     0.00000000     0.00010695    -0.00023074    -0.00000004    -0.00000005
   ht   7    -0.00032968     0.00000000     0.00002694     0.00005528     0.00000002     0.00000001    -0.00000001

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7

   eig(s)   1.341472E-10   8.560927E-10   3.522489E-09    1.00000        1.00000        1.00000        1.00000    
 
   x:   1  -7.870951E-06   2.242186E-05   4.156044E-05  -2.758400E-05  -0.116450      -2.745919E-03   0.993193    
   x:   2   3.175664E-11  -4.733250E-11  -1.762089E-10    1.00000      -2.686672E-04  -5.376740E-06  -3.742560E-06
   x:   3   4.912777E-07   4.347658E-06  -6.948386E-06  -1.936699E-04  -0.705595      -0.703538      -8.467485E-02
   x:   4   1.814796E-06  -7.957610E-06   1.915734E-07  -1.842718E-04  -0.698981       0.710652      -7.998953E-02
   x:   5   6.723106E-02  -0.109800       0.991677       1.971339E-11   1.044591E-07  -5.898066E-06  -3.848950E-05
   x:   6   2.019235E-02   0.993872       0.108674       4.618722E-11   1.375860E-07   8.168494E-06  -2.678595E-05
   x:   7   0.997533      -1.271804E-02  -6.903621E-02   1.788757E-10   6.904809E-07  -7.358924E-07   1.116023E-05
 bummer (warning):overlap matrix: # small eigenvalues=                      3

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1  -0.984363       2.022724E-08   2.433675E-02   5.705480E-02   3.990082E-05   2.767224E-05  -1.165520E-05
 refs   2   4.922678E-09    1.00000      -1.284430E-06   2.907780E-07   1.588178E-10   7.272764E-11  -4.403137E-11

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1   -1.00003       4.905817E-07  -0.129266       0.235888       0.343320       -1.16133       2.137771E-02
 civs   2  -4.967999E-10    1.00000       2.403762E-06  -2.609226E-06  -2.319419E-06   3.403961E-06   1.203325E-07
 civs   3  -2.006042E-05   4.485878E-07  -0.794150      -0.592352      -0.176211      -8.417226E-02   0.133770    
 civs   4   1.924510E-05  -3.622714E-07   0.171669      -0.521086       0.746065       0.358627      -0.335115    
 civs   5   -1.24512       1.985212E-02   -5840.69        7522.49        3183.00       -1357.57       -14976.2    
 civs   6    1.10339      -3.132455E-02    7862.90       -12435.4       -9966.01       -28902.0       -3018.04    
 civs   7   0.868266      -5.153152E-02    10500.6       -25414.2       -44265.7        30841.2       -61257.9    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.984362       5.347021E-08  -2.013795E-02  -2.409951E-02   6.747470E-02  -5.182663E-02  -4.011769E-03
 ref    2  -5.543984E-09    1.00000       2.654963E-06  -5.894194E-07  -1.446671E-07  -6.492300E-08  -4.953175E-08

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.98436215     0.00000005    -0.02013795    -0.02409951     0.06747470    -0.05182663    -0.00401177
 ref:   2    -0.00000001     1.00000000     0.00000265    -0.00000059    -0.00000014    -0.00000006    -0.00000005

 trial vector basis is being transformed.  new dimension:   4
 NCSF=                   115                  1890                  6280
                  5685

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 11  1    -25.7583936770  1.4295E-10  0.0000E+00  7.9296E-06  1.0000E-05
 mr-sdci # 11  2    -25.6559723467  4.5119E-13  5.9626E-02  3.3458E-01  1.0000E-05
 mr-sdci # 11  3    -25.3945720966  1.4274E-02  0.0000E+00  2.7127E-01  1.0000E-04
 mr-sdci # 11  4    -25.0960849040  7.3175E-02  0.0000E+00  4.1808E-01  1.0000E-04
 mr-sdci # 11  5    -24.6872480539  3.5418E-01  0.0000E+00  6.5316E-01  1.0000E-04
 mr-sdci # 11  6    -23.8338246503  7.5255E-02  0.0000E+00  1.1441E+00  1.0000E-04
 mr-sdci # 11  7    -23.5132999447  2.9262E-01  0.0000E+00  1.1414E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0014
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0013
    3   26    0     0.01     0.00     0.00     0.01         0.    0.0025
    4   27    0     0.00     0.00     0.00     0.00         0.    0.0012
    5   11    0     0.01     0.01     0.00     0.01         0.    0.0085
    6   15    0     0.01     0.00     0.00     0.01         0.    0.0092
    7   16    0     0.02     0.01     0.00     0.02         0.    0.0072
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0050
    9    5    0     0.01     0.00     0.00     0.01         0.    0.0119
   10    6    0     0.01     0.00     0.01     0.01         0.    0.0021
   11   18    0     0.00     0.00     0.00     0.00         0.    0.0021
   12    7    0     0.01     0.00     0.00     0.01         0.    0.0011
   13   19    0     0.00     0.00     0.00     0.00         0.    0.0011
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0005
   15   43    0     0.00     0.00     0.00     0.00         0.    0.0005
   16   44    0     0.00     0.00     0.00     0.00         0.    0.0005
   17   75    0     0.01     0.01     0.00     0.01         0.    0.0009
   18   76    0     0.00     0.00     0.00     0.00         0.    0.0020
   19   77    0     0.00     0.00     0.00     0.00         0.    0.0007
   20   78    0     0.00     0.01     0.00     0.00         0.    0.0001
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0900s 
time spent in multnx:                   0.0900s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.0400s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1100s 

          starting ci iteration  12

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.000006
ci vector #   2dasum_wr=    2.167357
ci vector #   3dasum_wr=    0.741573
ci vector #   4dasum_wr=    1.616168


====================================================================================================
Diagonal     counts:  0x:        5280 2x:        1480 4x:         370
All internal counts: zz :        5629 yy:       10577 xx:        5740 ww:        3548
One-external counts: yz :        9682 yx:       10444 yw:        8199
Two-external counts: yy :        4346 ww:        1530 xx:        2750 xz:         818 wz:         810 wx:        2990
Three-ext.   counts: yx :        1415 yw:        1165

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:        2479    task #     4:        1239
task #     5:        8526    task #     6:        9200    task #     7:        7175    task #     8:        4962
task #     9:       11867    task #    10:        2061    task #    11:        2061    task #    12:        1071
task #    13:        1071    task #    14:         535    task #    15:         537    task #    16:         537
task #    17:         865    task #    18:        2004    task #    19:         718    task #    20:         532
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5    -0.00000180     0.00000000     0.00000039    -0.00000003     0.03794567
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1   -29.95493157
   ht   2     0.00000000   -29.85251024
   ht   3     0.00000000     0.00000000   -29.59110999
   ht   4     0.00000000     0.00000000     0.00000000   -29.29262280
   ht   5     0.00005389    -0.05962637    -0.00001207     0.00000107    -1.07272255
Spectrum of overlapmatrix:    0.037946    1.000000    1.000000    1.000000    1.000000

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1   0.984362       5.347021E-08  -2.013795E-02  -2.409951E-02  -1.849350E-06
 refs   2  -5.543984E-09    1.00000       2.654963E-06  -5.894194E-07  -1.313447E-11

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 civs   1   -1.00000       1.694941E-06  -1.624669E-11  -3.989508E-12   9.078758E-06
 civs   2   1.307439E-10   0.983018       2.059338E-06   2.360655E-07  -0.183508    
 civs   3   4.761810E-15   1.335691E-06   -1.00000       5.154074E-12  -4.067005E-06
 civs   4  -1.650692E-15  -1.215034E-07   2.747447E-12    1.00000       6.355326E-07
 civs   5   2.245598E-10   0.942051      -9.028077E-06  -2.216639E-06    5.04639    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1  -0.984362      -4.515435E-08   2.013795E-02  -2.409951E-02  -3.389747E-07
 ref    2   5.674728E-09   0.983018      -5.956255E-07  -3.533538E-07  -0.183508    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1    -0.98436215    -0.00000005     0.02013795    -0.02409951    -0.00000034
 ref:   2     0.00000001     0.98301816    -0.00000060    -0.00000035    -0.18350830
 NCSF=                   115                  1890                  6280
                  5685

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 12  1    -25.7583936770  5.3291E-14  0.0000E+00  7.9296E-06  1.0000E-05
 mr-sdci # 12  2    -25.7131138148  5.7141E-02  3.4924E-03  7.8109E-02  1.0000E-05
 mr-sdci # 12  3    -25.3945720966  5.1941E-12  0.0000E+00  2.7127E-01  1.0000E-04
 mr-sdci # 12  4    -25.0960849040  2.1316E-13  0.0000E+00  4.1808E-01  1.0000E-04
 mr-sdci # 12  5    -24.0162773751 -6.7097E-01  0.0000E+00  7.5282E-01  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0014
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0013
    3   26    0     0.01     0.00     0.00     0.01         0.    0.0025
    4   27    0     0.00     0.00     0.00     0.00         0.    0.0012
    5   11    0     0.01     0.01     0.00     0.01         0.    0.0085
    6   15    0     0.01     0.00     0.00     0.01         0.    0.0092
    7   16    0     0.01     0.01     0.00     0.01         0.    0.0072
    8    1    0     0.01     0.01     0.00     0.01         0.    0.0050
    9    5    0     0.01     0.00     0.00     0.01         0.    0.0119
   10    6    0     0.01     0.00     0.00     0.01         0.    0.0021
   11   18    0     0.00     0.00     0.00     0.00         0.    0.0021
   12    7    0     0.01     0.00     0.00     0.01         0.    0.0011
   13   19    0     0.00     0.00     0.00     0.00         0.    0.0011
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0005
   15   43    0     0.00     0.00     0.00     0.00         0.    0.0005
   16   44    0     0.01     0.00     0.00     0.01         0.    0.0005
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0009
   18   76    0     0.00     0.00     0.00     0.00         0.    0.0020
   19   77    0     0.00     0.00     0.00     0.00         0.    0.0007
   20   78    0     0.00     0.00     0.00     0.00         0.    0.0001
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0900s 
time spent in multnx:                   0.0900s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0300s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.0900s 

          starting ci iteration  13

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.211130
ci vector #   2dasum_wr=    0.452616
ci vector #   3dasum_wr=    0.486559
ci vector #   4dasum_wr=    0.511875


====================================================================================================
Diagonal     counts:  0x:        5280 2x:        1480 4x:         370
All internal counts: zz :        5629 yy:       10577 xx:        5740 ww:        3548
One-external counts: yz :        9682 yx:       10444 yw:        8199
Two-external counts: yy :        4346 ww:        1530 xx:        2750 xz:         818 wz:         810 wx:        2990
Three-ext.   counts: yx :        1415 yw:        1165

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:        2479    task #     4:        1239
task #     5:        8526    task #     6:        9200    task #     7:        7175    task #     8:        4962
task #     9:       11867    task #    10:        2061    task #    11:        2061    task #    12:        1071
task #    13:        1071    task #    14:         535    task #    15:         537    task #    16:         537
task #    17:         865    task #    18:        2004    task #    19:         718    task #    20:         532
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5    -0.00000180     0.00000000     0.00000039    -0.00000003     0.03794567
 sovl   6     0.00000295     0.03851704     0.00000010    -0.00000009    -0.00083617     0.00480637
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1   -29.95493157
   ht   2     0.00000000   -29.85251024
   ht   3     0.00000000     0.00000000   -29.59110999
   ht   4     0.00000000     0.00000000     0.00000000   -29.29262280
   ht   5     0.00005389    -0.05962637    -0.00001207     0.00000107    -1.07272255
   ht   6    -0.00008824    -1.14987802    -0.00000293     0.00000268     0.01905551    -0.13907390
Spectrum of overlapmatrix:    0.003298    0.037966    1.000000    1.000000    1.000000    1.001489

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1   0.984362       5.347021E-08  -2.013795E-02  -2.409951E-02  -1.849350E-06   3.037939E-06
 refs   2  -5.543984E-09    1.00000       2.654963E-06  -5.894194E-07  -1.313447E-11   3.851704E-02

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1   -1.00000      -5.606520E-07  -1.049976E-10   1.777646E-10  -4.226099E-05  -2.923680E-05
 civs   2   9.420510E-11   0.950517      -4.220100E-06   3.199973E-06  -0.727991      -0.124558    
 civs   3   4.390531E-15   1.583325E-06    1.00000       4.599480E-11  -5.774928E-06   2.229949E-06
 civs   4  -1.504511E-15  -2.410770E-07  -2.021572E-11    1.00000       5.310398E-06   9.797451E-07
 civs   5   9.566492E-11   0.985055       1.128402E-05  -6.691911E-06    2.39977       -4.44648    
 civs   6  -1.011802E-09   0.791953       4.253696E-05  -6.443396E-05    15.8121        7.20952    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.984362       5.705816E-08  -2.013795E-02  -2.409951E-02   1.947567E-06   1.270403E-06
 ref    2   5.599217E-09   0.981020       7.326099E-08   1.287482E-07  -0.118954       0.153131    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.98436215     0.00000006    -0.02013795    -0.02409951     0.00000195     0.00000127
 ref:   2     0.00000001     0.98102021     0.00000007     0.00000013    -0.11895449     0.15313126
 NCSF=                   115                  1890                  6280
                  5685

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 13  1    -25.7583936770  3.5527E-15  0.0000E+00  7.9296E-06  1.0000E-05
 mr-sdci # 13  2    -25.7158825099  2.7687E-03  4.3482E-04  2.6143E-02  1.0000E-05
 mr-sdci # 13  3    -25.3945720966  6.0609E-12  0.0000E+00  2.7127E-01  1.0000E-04
 mr-sdci # 13  4    -25.0960849040  9.6740E-12  0.0000E+00  4.1808E-01  1.0000E-04
 mr-sdci # 13  5    -24.4467261247  4.3045E-01  0.0000E+00  9.8712E-01  1.0000E-04
 mr-sdci # 13  6    -23.9268486192  9.3024E-02  0.0000E+00  8.5281E-01  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.01     0.01     0.00     0.01         0.    0.0014
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0013
    3   26    0     0.00     0.00     0.00     0.00         0.    0.0025
    4   27    0     0.01     0.00     0.00     0.01         0.    0.0012
    5   11    0     0.00     0.00     0.00     0.00         0.    0.0085
    6   15    0     0.02     0.01     0.00     0.02         0.    0.0092
    7   16    0     0.01     0.00     0.00     0.01         0.    0.0072
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0050
    9    5    0     0.01     0.01     0.00     0.01         0.    0.0119
   10    6    0     0.02     0.01     0.01     0.01         0.    0.0021
   11   18    0     0.01     0.00     0.00     0.01         0.    0.0021
   12    7    0     0.00     0.00     0.00     0.00         0.    0.0011
   13   19    0     0.00     0.00     0.00     0.00         0.    0.0011
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0005
   15   43    0     0.01     0.00     0.00     0.01         0.    0.0005
   16   44    0     0.00     0.00     0.00     0.00         0.    0.0005
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0009
   18   76    0     0.00     0.00     0.00     0.00         0.    0.0020
   19   77    0     0.00     0.00     0.00     0.00         0.    0.0007
   20   78    0     0.00     0.00     0.00     0.00         0.    0.0001
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1000s 
time spent in multnx:                   0.0900s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.0400s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1100s 

          starting ci iteration  14

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.086734
ci vector #   2dasum_wr=    0.173429
ci vector #   3dasum_wr=    0.141464
ci vector #   4dasum_wr=    0.161080


====================================================================================================
Diagonal     counts:  0x:        5280 2x:        1480 4x:         370
All internal counts: zz :        5629 yy:       10577 xx:        5740 ww:        3548
One-external counts: yz :        9682 yx:       10444 yw:        8199
Two-external counts: yy :        4346 ww:        1530 xx:        2750 xz:         818 wz:         810 wx:        2990
Three-ext.   counts: yx :        1415 yw:        1165

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:        2479    task #     4:        1239
task #     5:        8526    task #     6:        9200    task #     7:        7175    task #     8:        4962
task #     9:       11867    task #    10:        2061    task #    11:        2061    task #    12:        1071
task #    13:        1071    task #    14:         535    task #    15:         537    task #    16:         537
task #    17:         865    task #    18:        2004    task #    19:         718    task #    20:         532
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6       sovl   7
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5    -0.00000180     0.00000000     0.00000039    -0.00000003     0.03794567
 sovl   6     0.00000295     0.03851704     0.00000010    -0.00000009    -0.00083617     0.00480637
 sovl   7    -0.00000297    -0.01138347     0.00000006     0.00000003     0.00011088    -0.00082282     0.00055950
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1   -29.95493157
   ht   2     0.00000000   -29.85251024
   ht   3     0.00000000     0.00000000   -29.59110999
   ht   4     0.00000000     0.00000000     0.00000000   -29.29262280
   ht   5     0.00005389    -0.05962637    -0.00001207     0.00000107    -1.07272255
   ht   6    -0.00008824    -1.14987802    -0.00000293     0.00000268     0.01905551    -0.13907390
   ht   7     0.00008889     0.33983193    -0.00000186    -0.00000087    -0.00264692     0.02404116    -0.01615405
Spectrum of overlapmatrix:    0.000380    0.003347    0.037966    1.000000    1.000000    1.000000    1.001618

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1   0.984362       5.347021E-08  -2.013795E-02  -2.409951E-02  -1.849350E-06   3.037939E-06  -3.060490E-06
 refs   2  -5.543984E-09    1.00000       2.654963E-06  -5.894194E-07  -1.313447E-11   3.851704E-02  -1.138347E-02

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1   -1.00000       1.682988E-06   2.581896E-10   6.423895E-10  -4.958660E-05   1.344321E-04  -4.619519E-07
 civs   2   1.844112E-10   0.955813       4.503561E-06   4.102802E-06   0.364019       0.725476      -3.488489E-02
 civs   3   3.664546E-15   1.451802E-06   -1.00000       4.308176E-11   6.359533E-06   9.967615E-07   1.701849E-06
 civs   4  -1.189834E-15  -1.865231E-07   2.886284E-11    1.00000      -1.161796E-06  -6.652226E-06   2.350433E-07
 civs   5   1.897315E-10   0.990813      -1.084803E-05  -4.709242E-06   -2.34671      -0.703682       -4.41779    
 civs   6  -2.740928E-10   0.897951      -3.511856E-05  -4.037325E-05   -15.3208       -6.31968        7.86228    
 civs   7   5.611447E-09   0.857835       5.872644E-05   1.792644E-04   -30.4975        39.4572        10.3279    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1  -0.984362      -4.679737E-08   2.013795E-02  -2.409951E-02   2.241748E-06  -6.146945E-06  -4.975531E-08
 ref    2   5.653960E-09   0.980635      -1.725758E-07  -8.232667E-08   0.121075       3.290063E-02   0.150380    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.98436215    -0.00000005     0.02013795    -0.02409951     0.00000224    -0.00000615    -0.00000005
 ref:   2     0.00000001     0.98063461    -0.00000017    -0.00000008     0.12107542     0.03290063     0.15037966

 trial vector basis is being transformed.  new dimension:   4
 NCSF=                   115                  1890                  6280
                  5685

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 14  1    -25.7583936770 -3.5527E-15  0.0000E+00  7.9296E-06  1.0000E-05
 mr-sdci # 14  2    -25.7162555662  3.7306E-04  7.4632E-05  1.1041E-02  1.0000E-05
 mr-sdci # 14  3    -25.3945720966  1.3110E-12  0.0000E+00  2.7127E-01  1.0000E-04
 mr-sdci # 14  4    -25.0960849040  8.5407E-12  0.0000E+00  4.1808E-01  1.0000E-04
 mr-sdci # 14  5    -24.4959092636  4.9183E-02  0.0000E+00  8.9284E-01  1.0000E-04
 mr-sdci # 14  6    -24.3635334891  4.3668E-01  0.0000E+00  1.0769E+00  1.0000E-04
 mr-sdci # 14  7    -23.9063362922  3.9304E-01  0.0000E+00  8.5471E-01  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0014
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0013
    3   26    0     0.01     0.00     0.00     0.01         0.    0.0025
    4   27    0     0.00     0.00     0.00     0.00         0.    0.0012
    5   11    0     0.01     0.01     0.00     0.01         0.    0.0085
    6   15    0     0.01     0.00     0.00     0.01         0.    0.0092
    7   16    0     0.01     0.01     0.00     0.01         0.    0.0072
    8    1    0     0.01     0.01     0.00     0.01         0.    0.0050
    9    5    0     0.01     0.00     0.00     0.01         0.    0.0119
   10    6    0     0.01     0.00     0.00     0.01         0.    0.0021
   11   18    0     0.00     0.00     0.00     0.00         0.    0.0021
   12    7    0     0.01     0.01     0.00     0.01         0.    0.0011
   13   19    0     0.00     0.00     0.00     0.00         0.    0.0011
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0005
   15   43    0     0.00     0.00     0.00     0.00         0.    0.0005
   16   44    0     0.01     0.00     0.00     0.01         0.    0.0005
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0009
   18   76    0     0.00     0.00     0.00     0.00         0.    0.0020
   19   77    0     0.00     0.00     0.00     0.00         0.    0.0007
   20   78    0     0.00     0.00     0.00     0.00         0.    0.0001
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0900s 
time spent in multnx:                   0.0900s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0400s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.0900s 

          starting ci iteration  15

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.026233
ci vector #   2dasum_wr=    0.087290
ci vector #   3dasum_wr=    0.060004
ci vector #   4dasum_wr=    0.068142


====================================================================================================
Diagonal     counts:  0x:        5280 2x:        1480 4x:         370
All internal counts: zz :        5629 yy:       10577 xx:        5740 ww:        3548
One-external counts: yz :        9682 yx:       10444 yw:        8199
Two-external counts: yy :        4346 ww:        1530 xx:        2750 xz:         818 wz:         810 wx:        2990
Three-ext.   counts: yx :        1415 yw:        1165

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:        2479    task #     4:        1239
task #     5:        8526    task #     6:        9200    task #     7:        7175    task #     8:        4962
task #     9:       11867    task #    10:        2061    task #    11:        2061    task #    12:        1071
task #    13:        1071    task #    14:         535    task #    15:         537    task #    16:         537
task #    17:         865    task #    18:        2004    task #    19:         718    task #    20:         532
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5    -0.00000308     0.00246727     0.00000006    -0.00000005     0.00008415
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1   -29.95493157
   ht   2     0.00000000   -29.91279346
   ht   3     0.00000000     0.00000000   -29.59110999
   ht   4     0.00000000     0.00000000     0.00000000   -29.29262280
   ht   5     0.00009212    -0.07387768    -0.00000175     0.00000141    -0.00243417
Spectrum of overlapmatrix:    0.000078    1.000000    1.000000    1.000000    1.000006

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1  -0.984362      -4.679737E-08   2.013795E-02  -2.409951E-02   3.171646E-06
 refs   2   5.653960E-09   0.980635      -1.725758E-07  -8.232667E-08   2.272323E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 civs   1   -1.00000       2.757191E-06   2.055300E-09   5.382015E-09   3.480661E-04
 civs   2  -2.442144E-11   0.997757      -1.804002E-06  -4.528551E-06  -0.287172    
 civs   3  -5.862282E-15  -1.624626E-07   -1.00000       1.220582E-10  -9.345344E-07
 civs   4  -1.612603E-15  -4.681621E-08  -1.057021E-10   -1.00000       2.067819E-05
 civs   5   3.508093E-08   0.896545       6.683277E-04   1.750087E-03    113.182    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1   0.984362       8.061301E-08  -2.013795E-02   2.409951E-02   1.584591E-05
 ref    2  -5.598193E-09   0.980472      -7.783478E-08  -3.817637E-07  -2.442469E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     0.98436215     0.00000008    -0.02013795     0.02409951     0.00001585
 ref:   2    -0.00000001     0.98047191    -0.00000008    -0.00000038    -0.02442469
 NCSF=                   115                  1890                  6280
                  5685

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 15  1    -25.7583936770  1.7764E-14  0.0000E+00  7.9296E-06  1.0000E-05
 mr-sdci # 15  2    -25.7163224792  6.6913E-05  2.5044E-05  5.5673E-03  1.0000E-05
 mr-sdci # 15  3    -25.3945720966  2.6013E-11  0.0000E+00  2.7127E-01  1.0000E-04
 mr-sdci # 15  4    -25.0960849041  1.0671E-10  0.0000E+00  4.1808E-01  1.0000E-04
 mr-sdci # 15  5    -24.6498558575  1.5395E-01  0.0000E+00  1.1550E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.01     0.01     0.00     0.01         0.    0.0014
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0013
    3   26    0     0.00     0.00     0.00     0.00         0.    0.0025
    4   27    0     0.01     0.00     0.00     0.01         0.    0.0012
    5   11    0     0.00     0.00     0.00     0.00         0.    0.0085
    6   15    0     0.02     0.01     0.00     0.02         0.    0.0092
    7   16    0     0.01     0.00     0.00     0.01         0.    0.0072
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0050
    9    5    0     0.01     0.01     0.00     0.01         0.    0.0119
   10    6    0     0.01     0.01     0.00     0.01         0.    0.0021
   11   18    0     0.01     0.01     0.00     0.01         0.    0.0021
   12    7    0     0.00     0.00     0.00     0.00         0.    0.0011
   13   19    0     0.02     0.00     0.00     0.02         0.    0.0011
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0005
   15   43    0     0.00     0.00     0.00     0.00         0.    0.0005
   16   44    0     0.00     0.00     0.00     0.00         0.    0.0005
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0009
   18   76    0     0.00     0.00     0.00     0.00         0.    0.0020
   19   77    0     0.00     0.00     0.00     0.00         0.    0.0007
   20   78    0     0.00     0.00     0.00     0.00         0.    0.0001
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1000s 
time spent in multnx:                   0.1000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0500s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1100s 

          starting ci iteration  16

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.021407
ci vector #   2dasum_wr=    0.038165
ci vector #   3dasum_wr=    0.022807
ci vector #   4dasum_wr=    0.029071


====================================================================================================
Diagonal     counts:  0x:        5280 2x:        1480 4x:         370
All internal counts: zz :        5629 yy:       10577 xx:        5740 ww:        3548
One-external counts: yz :        9682 yx:       10444 yw:        8199
Two-external counts: yy :        4346 ww:        1530 xx:        2750 xz:         818 wz:         810 wx:        2990
Three-ext.   counts: yx :        1415 yw:        1165

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:        2479    task #     4:        1239
task #     5:        8526    task #     6:        9200    task #     7:        7175    task #     8:        4962
task #     9:       11867    task #    10:        2061    task #    11:        2061    task #    12:        1071
task #    13:        1071    task #    14:         535    task #    15:         537    task #    16:         537
task #    17:         865    task #    18:        2004    task #    19:         718    task #    20:         532
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5    -0.00000308     0.00246727     0.00000006    -0.00000005     0.00008415
 sovl   6     0.00000432    -0.00373869    -0.00000010     0.00000005    -0.00001560     0.00004430
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1   -29.95493157
   ht   2     0.00000000   -29.91279346
   ht   3     0.00000000     0.00000000   -29.59110999
   ht   4     0.00000000     0.00000000     0.00000000   -29.29262280
   ht   5     0.00009212    -0.07387768    -0.00000175     0.00000141    -0.00243417
   ht   6    -0.00012941     0.11183460     0.00000280    -0.00000161     0.00043911    -0.00129259
Spectrum of overlapmatrix:    0.000029    0.000079    1.000000    1.000000    1.000000    1.000020

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1  -0.984362      -4.679737E-08   2.013795E-02  -2.409951E-02   3.171646E-06  -4.456376E-06
 refs   2   5.653960E-09   0.980635      -1.725758E-07  -8.232667E-08   2.272323E-03  -3.920718E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1   -1.00000       8.006860E-07  -1.032768E-08  -3.285348E-04  -2.445158E-08  -7.541721E-04
 civs   2  -4.807140E-10   -1.00087       8.915630E-06   0.289754       2.197670E-05   0.641966    
 civs   3  -2.869546E-14  -3.997310E-08    1.00000      -5.511951E-06  -1.395478E-10   8.386709E-06
 civs   4  -8.502460E-15  -1.665214E-08   5.102002E-10   1.104341E-04   -1.00000      -2.688909E-05
 civs   5  -7.336175E-09   -1.25953       5.792749E-04    86.1113       1.106812E-02   -74.9556    
 civs   6  -1.299434E-07   -1.08192       2.802925E-03    137.345       1.353864E-02    121.214    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.984362       8.493554E-08   2.013795E-02  -1.833313E-05   2.409951E-02  -3.474054E-05
 ref    2  -5.632563E-09  -0.980104      -1.102782E-06  -5.867413E-02  -6.297410E-06  -1.603393E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.98436215     0.00000008     0.02013795    -0.00001833     0.02409951    -0.00003474
 ref:   2    -0.00000001    -0.98010438    -0.00000110    -0.05867413    -0.00000630    -0.01603393
 NCSF=                   115                  1890                  6280
                  5685

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 16  1    -25.7583936770  0.0000E+00  0.0000E+00  7.9296E-06  1.0000E-05
 mr-sdci # 16  2    -25.7163495755  2.7096E-05  8.2335E-06  3.4782E-03  1.0000E-05
 mr-sdci # 16  3    -25.3945720967  8.8328E-11  0.0000E+00  2.7127E-01  1.0000E-04
 mr-sdci # 16  4    -25.1462320078  5.0147E-02  0.0000E+00  6.3476E-01  1.0000E-04
 mr-sdci # 16  5    -25.0960849036  4.4623E-01  0.0000E+00  4.1808E-01  1.0000E-04
 mr-sdci # 16  6    -24.2632417324 -1.0029E-01  0.0000E+00  1.2064E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0014
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0013
    3   26    0     0.00     0.00     0.00     0.00         0.    0.0025
    4   27    0     0.01     0.00     0.00     0.01         0.    0.0012
    5   11    0     0.01     0.00     0.00     0.01         0.    0.0085
    6   15    0     0.01     0.00     0.00     0.01         0.    0.0092
    7   16    0     0.01     0.00     0.00     0.01         0.    0.0072
    8    1    0     0.01     0.01     0.00     0.01         0.    0.0050
    9    5    0     0.01     0.00     0.00     0.01         0.    0.0119
   10    6    0     0.00     0.00     0.00     0.00         0.    0.0021
   11   18    0     0.01     0.01     0.00     0.01         0.    0.0021
   12    7    0     0.00     0.00     0.00     0.00         0.    0.0011
   13   19    0     0.01     0.00     0.00     0.01         0.    0.0011
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0005
   15   43    0     0.00     0.00     0.00     0.00         0.    0.0005
   16   44    0     0.00     0.00     0.00     0.00         0.    0.0005
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0009
   18   76    0     0.00     0.00     0.00     0.00         0.    0.0020
   19   77    0     0.00     0.00     0.00     0.00         0.    0.0007
   20   78    0     0.00     0.00     0.00     0.00         0.    0.0001
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0800s 
time spent in multnx:                   0.0800s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0200s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.0900s 

          starting ci iteration  17

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.010149
ci vector #   2dasum_wr=    0.024959
ci vector #   3dasum_wr=    0.022183
ci vector #   4dasum_wr=    0.020401


====================================================================================================
Diagonal     counts:  0x:        5280 2x:        1480 4x:         370
All internal counts: zz :        5629 yy:       10577 xx:        5740 ww:        3548
One-external counts: yz :        9682 yx:       10444 yw:        8199
Two-external counts: yy :        4346 ww:        1530 xx:        2750 xz:         818 wz:         810 wx:        2990
Three-ext.   counts: yx :        1415 yw:        1165

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:        2479    task #     4:        1239
task #     5:        8526    task #     6:        9200    task #     7:        7175    task #     8:        4962
task #     9:       11867    task #    10:        2061    task #    11:        2061    task #    12:        1071
task #    13:        1071    task #    14:         535    task #    15:         537    task #    16:         537
task #    17:         865    task #    18:        2004    task #    19:         718    task #    20:         532
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6       sovl   7
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5    -0.00000308     0.00246727     0.00000006    -0.00000005     0.00008415
 sovl   6     0.00000432    -0.00373869    -0.00000010     0.00000005    -0.00001560     0.00004430
 sovl   7     0.00000520    -0.00242186    -0.00000011     0.00000006    -0.00001717     0.00001298     0.00001515
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1   -29.95493157
   ht   2     0.00000000   -29.91279346
   ht   3     0.00000000     0.00000000   -29.59110999
   ht   4     0.00000000     0.00000000     0.00000000   -29.29262280
   ht   5     0.00009212    -0.07387768    -0.00000175     0.00000141    -0.00243417
   ht   6    -0.00012941     0.11183460     0.00000280    -0.00000161     0.00043911    -0.00129259
   ht   7    -0.00015590     0.07244457     0.00000333    -0.00000194     0.00051391    -0.00038077    -0.00044066
Spectrum of overlapmatrix:    0.000007    0.000030    0.000081    1.000000    1.000000    1.000000    1.000026

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1  -0.984362      -4.679737E-08   2.013795E-02  -2.409951E-02   3.171646E-06  -4.456376E-06  -5.368809E-06
 refs   2   5.653960E-09   0.980635      -1.725758E-07  -8.232667E-08   2.272323E-03  -3.920718E-03  -2.532338E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1   -1.00000      -2.548511E-06   5.081838E-08   3.131516E-04   6.074794E-08  -1.356236E-03  -1.142988E-03
 civs   2  -1.072725E-09  -0.999649      -1.864831E-05  -1.511446E-02  -3.982615E-05   0.882031       0.341709    
 civs   3  -1.364072E-13   1.709357E-07   -1.00000       5.368785E-05   1.048490E-09   1.295565E-05   1.781377E-05
 civs   4  -4.620279E-14   5.902052E-08  -3.065315E-09  -3.042384E-05   -1.00000      -5.610835E-05  -2.858530E-05
 civs   5   3.529461E-08   -1.34911       4.031524E-03    72.6528      -2.985324E-03   -27.3199        97.3973    
 civs   6   8.058072E-09   -1.34891       5.850924E-03    126.944      -8.684866E-03    127.892       -49.7087    
 civs   7  -4.452218E-07   0.812198      -1.223891E-02   -122.614      -6.227055E-03    138.286        318.430    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.984362      -7.071940E-08  -2.013795E-02   1.657084E-05   2.409951E-02  -6.241466E-05  -5.301447E-05
 ref    2  -5.529852E-09  -0.980125      -9.004305E-07  -3.694229E-02   4.063724E-06  -4.874460E-02  -5.506909E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.98436215    -0.00000007    -0.02013795     0.00001657     0.02409951    -0.00006241    -0.00005301
 ref:   2    -0.00000001    -0.98012454    -0.00000090    -0.03694229     0.00000406    -0.04874460    -0.05506909

 trial vector basis is being transformed.  new dimension:   4
 NCSF=                   115                  1890                  6280
                  5685

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 17  1    -25.7583936770  0.0000E+00  0.0000E+00  7.9296E-06  1.0000E-05
 mr-sdci # 17  2    -25.7163562627  6.6873E-06  8.1352E-07  1.2511E-03  1.0000E-05
 mr-sdci # 17  3    -25.3945720973  5.6999E-10  0.0000E+00  2.7127E-01  1.0000E-04
 mr-sdci # 17  4    -25.3149088815  1.6868E-01  0.0000E+00  3.4414E-01  1.0000E-04
 mr-sdci # 17  5    -25.0960849052  1.5905E-09  0.0000E+00  4.1808E-01  1.0000E-04
 mr-sdci # 17  6    -24.4162891357  1.5305E-01  0.0000E+00  1.0454E+00  1.0000E-04
 mr-sdci # 17  7    -23.5350402060 -3.7130E-01  0.0000E+00  1.0973E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0014
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0013
    3   26    0     0.01     0.00     0.00     0.01         0.    0.0025
    4   27    0     0.00     0.00     0.00     0.00         0.    0.0012
    5   11    0     0.01     0.01     0.00     0.01         0.    0.0085
    6   15    0     0.02     0.01     0.00     0.02         0.    0.0092
    7   16    0     0.01     0.00     0.00     0.01         0.    0.0072
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0050
    9    5    0     0.01     0.01     0.00     0.01         0.    0.0119
   10    6    0     0.01     0.01     0.00     0.01         0.    0.0021
   11   18    0     0.01     0.00     0.00     0.01         0.    0.0021
   12    7    0     0.00     0.00     0.00     0.00         0.    0.0011
   13   19    0     0.00     0.00     0.00     0.00         0.    0.0011
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0005
   15   43    0     0.01     0.00     0.01     0.01         0.    0.0005
   16   44    0     0.00     0.00     0.00     0.00         0.    0.0005
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0009
   18   76    0     0.00     0.00     0.00     0.00         0.    0.0020
   19   77    0     0.00     0.00     0.00     0.00         0.    0.0007
   20   78    0     0.00     0.00     0.00     0.00         0.    0.0001
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0900s 
time spent in multnx:                   0.0900s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.0400s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.1100s 

          starting ci iteration  18

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.002794
ci vector #   2dasum_wr=    0.007998
ci vector #   3dasum_wr=    0.007382
ci vector #   4dasum_wr=    0.008871


====================================================================================================
Diagonal     counts:  0x:        5280 2x:        1480 4x:         370
All internal counts: zz :        5629 yy:       10577 xx:        5740 ww:        3548
One-external counts: yz :        9682 yx:       10444 yw:        8199
Two-external counts: yy :        4346 ww:        1530 xx:        2750 xz:         818 wz:         810 wx:        2990
Three-ext.   counts: yx :        1415 yw:        1165

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:        2479    task #     4:        1239
task #     5:        8526    task #     6:        9200    task #     7:        7175    task #     8:        4962
task #     9:       11867    task #    10:        2061    task #    11:        2061    task #    12:        1071
task #    13:        1071    task #    14:         535    task #    15:         537    task #    16:         537
task #    17:         865    task #    18:        2004    task #    19:         718    task #    20:         532
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000375    -0.00079597    -0.00000009    -0.00015302     0.00000124
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1   -29.95493157
   ht   2     0.00000000   -29.91289416
   ht   3     0.00000000     0.00000000   -29.59110999
   ht   4     0.00000000     0.00000000     0.00000000   -29.51144677
   ht   5    -0.00011239     0.02380886     0.00000260     0.00469587    -0.00003604
Spectrum of overlapmatrix:    0.000001    1.000000    1.000000    1.000000    1.000001

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1   0.984362      -7.071940E-08  -2.013795E-02   1.657084E-05   3.870255E-06
 refs   2  -5.529852E-09  -0.980125      -9.004305E-07  -3.694229E-02   8.649028E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 civs   1   -1.00000      -3.356052E-06   3.448269E-07  -8.508157E-04  -4.854792E-03
 civs   2  -3.354627E-09    1.00071      -7.291826E-05   0.179977        1.02928    
 civs   3  -7.798690E-13   1.812644E-07   -1.00000      -1.962983E-04   8.230137E-05
 civs   4   1.039602E-09  -2.639872E-04   1.934953E-04  -0.950289       0.370616    
 civs   5  -4.114496E-06   0.894419      -9.190157E-02    226.755        1293.88    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1  -0.984362       7.926434E-08   2.013793E-02   2.828311E-05   2.331758E-04
 ref    2   5.220759E-09  -0.980039      -1.426467E-05   5.482694E-02   9.656223E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1    -0.98436215     0.00000008     0.02013793     0.00002828     0.00023318
 ref:   2     0.00000001    -0.98003867    -0.00001426     0.05482694     0.09656223
 NCSF=                   115                  1890                  6280
                  5685

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 18  1    -25.7583936770  1.0658E-14  0.0000E+00  7.9296E-06  1.0000E-05
 mr-sdci # 18  2    -25.7163569904  7.2762E-07  1.6741E-07  4.8430E-04  1.0000E-05
 mr-sdci # 18  3    -25.3945721006  3.3561E-09  0.0000E+00  2.7127E-01  1.0000E-04
 mr-sdci # 18  4    -25.3563280788  4.1419E-02  0.0000E+00  2.4725E-01  1.0000E-04
 mr-sdci # 18  5    -23.9663406026 -1.1297E+00  0.0000E+00  1.0825E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0014
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0013
    3   26    0     0.01     0.00     0.00     0.01         0.    0.0025
    4   27    0     0.00     0.00     0.00     0.00         0.    0.0012
    5   11    0     0.01     0.01     0.00     0.01         0.    0.0085
    6   15    0     0.01     0.00     0.00     0.01         0.    0.0092
    7   16    0     0.02     0.01     0.00     0.02         0.    0.0072
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0050
    9    5    0     0.01     0.00     0.00     0.01         0.    0.0119
   10    6    0     0.00     0.00     0.00     0.00         0.    0.0021
   11   18    0     0.01     0.00     0.00     0.01         0.    0.0021
   12    7    0     0.01     0.00     0.00     0.01         0.    0.0011
   13   19    0     0.00     0.00     0.00     0.00         0.    0.0011
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0005
   15   43    0     0.00     0.00     0.00     0.00         0.    0.0005
   16   44    0     0.00     0.00     0.00     0.00         0.    0.0005
   17   75    0     0.01     0.01     0.00     0.01         0.    0.0009
   18   76    0     0.00     0.00     0.00     0.00         0.    0.0020
   19   77    0     0.00     0.00     0.00     0.00         0.    0.0007
   20   78    0     0.00     0.00     0.00     0.00         0.    0.0001
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0900s 
time spent in multnx:                   0.0900s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0300s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.0900s 

          starting ci iteration  19

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.001578
ci vector #   2dasum_wr=    0.003891
ci vector #   3dasum_wr=    0.002397
ci vector #   4dasum_wr=    0.002579


====================================================================================================
Diagonal     counts:  0x:        5280 2x:        1480 4x:         370
All internal counts: zz :        5629 yy:       10577 xx:        5740 ww:        3548
One-external counts: yz :        9682 yx:       10444 yw:        8199
Two-external counts: yy :        4346 ww:        1530 xx:        2750 xz:         818 wz:         810 wx:        2990
Three-ext.   counts: yx :        1415 yw:        1165

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:        2479    task #     4:        1239
task #     5:        8526    task #     6:        9200    task #     7:        7175    task #     8:        4962
task #     9:       11867    task #    10:        2061    task #    11:        2061    task #    12:        1071
task #    13:        1071    task #    14:         535    task #    15:         537    task #    16:         537
task #    17:         865    task #    18:        2004    task #    19:         718    task #    20:         532
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000375    -0.00079597    -0.00000009    -0.00015302     0.00000124
 sovl   6    -0.00000501     0.00041612     0.00000011    -0.00004161    -0.00000038     0.00000034
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1   -29.95493157
   ht   2     0.00000000   -29.91289416
   ht   3     0.00000000     0.00000000   -29.59110999
   ht   4     0.00000000     0.00000000     0.00000000   -29.51144677
   ht   5    -0.00011239     0.02380886     0.00000260     0.00469587    -0.00003604
   ht   6     0.00015000    -0.01244721    -0.00000316     0.00123242     0.00001129    -0.00001005
Spectrum of overlapmatrix:    0.000000    0.000001    1.000000    1.000000    1.000000    1.000001

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1   0.984362      -7.071940E-08  -2.013795E-02   1.657084E-05   3.870255E-06  -5.164364E-06
 refs   2  -5.529852E-09  -0.980125      -9.004305E-07  -3.694229E-02   8.649028E-04  -4.402800E-04

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1   -1.00000      -5.059051E-07  -2.040575E-06   5.055261E-04   8.843845E-03   8.807116E-03
 civs   2  -9.225918E-09   -1.00048       1.052394E-04   9.663945E-02  -0.403201       -1.26958    
 civs   3  -6.370455E-12   5.448062E-08    1.00000       3.512029E-04  -4.049532E-05  -1.363662E-04
 civs   4   5.834518E-10   2.918536E-04   2.893282E-04  -0.915358       0.389570      -0.266250    
 civs   5   5.726388E-07   -1.08206      -0.133563        287.757        686.154       -1111.61    
 civs   6   2.329356E-05  -0.911818      -0.507584        316.571        2280.27        925.858    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.984362       9.761824E-08  -2.013785E-02  -4.582472E-05  -4.076826E-04  -4.158697E-04
 ref    2   4.790435E-09   0.980051      -6.776435E-06   4.859819E-02  -2.970305E-02  -0.114888    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.98436215     0.00000010    -0.02013785    -0.00004582    -0.00040768    -0.00041587
 ref:   2     0.00000000     0.98005115    -0.00000678     0.04859819    -0.02970305    -0.11488833
 NCSF=                   115                  1890                  6280
                  5685

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 19  1    -25.7583936770  3.5527E-15  0.0000E+00  7.9296E-06  1.0000E-05
 mr-sdci # 19  2    -25.7163571430  1.5265E-07  2.9624E-08  2.0927E-04  1.0000E-05
 mr-sdci # 19  3    -25.3945721236  2.3010E-08  0.0000E+00  2.7127E-01  1.0000E-04
 mr-sdci # 19  4    -25.3696356591  1.3308E-02  0.0000E+00  2.4033E-01  1.0000E-04
 mr-sdci # 19  5    -24.6089868601  6.4265E-01  0.0000E+00  7.7652E-01  1.0000E-04
 mr-sdci # 19  6    -23.8613202252 -5.5497E-01  0.0000E+00  1.0651E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.01     0.00     0.00     0.01         0.    0.0014
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0013
    3   26    0     0.00     0.00     0.00     0.00         0.    0.0025
    4   27    0     0.01     0.00     0.00     0.01         0.    0.0012
    5   11    0     0.00     0.00     0.00     0.00         0.    0.0085
    6   15    0     0.02     0.00     0.00     0.02         0.    0.0092
    7   16    0     0.01     0.00     0.00     0.01         0.    0.0072
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0050
    9    5    0     0.01     0.01     0.00     0.01         0.    0.0119
   10    6    0     0.01     0.01     0.00     0.01         0.    0.0021
   11   18    0     0.01     0.00     0.00     0.01         0.    0.0021
   12    7    0     0.00     0.00     0.00     0.00         0.    0.0011
   13   19    0     0.00     0.00     0.00     0.00         0.    0.0011
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0005
   15   43    0     0.02     0.00     0.01     0.01         0.    0.0005
   16   44    0     0.00     0.00     0.00     0.00         0.    0.0005
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0009
   18   76    0     0.00     0.00     0.00     0.00         0.    0.0020
   19   77    0     0.00     0.00     0.00     0.00         0.    0.0007
   20   78    0     0.00     0.01     0.00     0.00         0.    0.0001
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1000s 
time spent in multnx:                   0.0900s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.0300s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1100s 

          starting ci iteration  20

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.000665
ci vector #   2dasum_wr=    0.001425
ci vector #   3dasum_wr=    0.001234
ci vector #   4dasum_wr=    0.001387


====================================================================================================
Diagonal     counts:  0x:        5280 2x:        1480 4x:         370
All internal counts: zz :        5629 yy:       10577 xx:        5740 ww:        3548
One-external counts: yz :        9682 yx:       10444 yw:        8199
Two-external counts: yy :        4346 ww:        1530 xx:        2750 xz:         818 wz:         810 wx:        2990
Three-ext.   counts: yx :        1415 yw:        1165

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:        2479    task #     4:        1239
task #     5:        8526    task #     6:        9200    task #     7:        7175    task #     8:        4962
task #     9:       11867    task #    10:        2061    task #    11:        2061    task #    12:        1071
task #    13:        1071    task #    14:         535    task #    15:         537    task #    16:         537
task #    17:         865    task #    18:        2004    task #    19:         718    task #    20:         532
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6       sovl   7
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000375    -0.00079597    -0.00000009    -0.00015302     0.00000124
 sovl   6    -0.00000501     0.00041612     0.00000011    -0.00004161    -0.00000038     0.00000034
 sovl   7    -0.00000535     0.00009396     0.00000012    -0.00001539    -0.00000011     0.00000006     0.00000005
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1   -29.95493157
   ht   2     0.00000000   -29.91289416
   ht   3     0.00000000     0.00000000   -29.59110999
   ht   4     0.00000000     0.00000000     0.00000000   -29.51144677
   ht   5    -0.00011239     0.02380886     0.00000260     0.00469587    -0.00003604
   ht   6     0.00015000    -0.01244721    -0.00000316     0.00123242     0.00001129    -0.00001005
   ht   7     0.00016024    -0.00281075    -0.00000340     0.00045275     0.00000340    -0.00000168    -0.00000137
Spectrum of overlapmatrix:    0.000000    0.000000    0.000001    1.000000    1.000000    1.000000    1.000001

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1   0.984362      -7.071940E-08  -2.013795E-02   1.657084E-05   3.870255E-06  -5.164364E-06  -5.517738E-06
 refs   2  -5.529852E-09  -0.980125      -9.004305E-07  -3.694229E-02   8.649028E-04  -4.402800E-04  -9.093150E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1   -1.00000      -3.290678E-06   1.655696E-05   2.186492E-03   1.389036E-02  -2.360015E-02   7.237501E-03
 civs   2  -1.600402E-08    1.00052      -4.523036E-04  -0.124866      -0.117555       0.899085       0.974077    
 civs   3  -4.348019E-11   1.846681E-07  -0.999999       1.534982E-03   9.477566E-05   2.162657E-04  -8.771279E-05
 civs   4   3.514531E-09  -3.117029E-04   1.345732E-03   0.893856      -0.364284      -0.184654       0.329503    
 civs   5  -3.943407E-06    1.11211      -0.581270       -317.234       -511.807       -171.976        1230.62    
 civs   6  -4.587328E-07    1.05786      -0.796622       -439.966       -1522.28       -1858.83       -619.362    
 civs   7   1.381351E-04  -0.825333        3.43309        598.068        3662.60       -2792.34        2795.90    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1  -0.984362       7.607247E-08   2.013717E-02  -1.194016E-04  -6.633096E-04   1.102922E-03  -3.341176E-04
 ref    2   5.316427E-09  -0.980053      -6.968139E-05  -4.568842E-02   2.319471E-02   4.918151E-02   0.115937    

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1    -0.98436215     0.00000008     0.02013717    -0.00011940    -0.00066331     0.00110292    -0.00033412
 ref:   2     0.00000001    -0.98005350    -0.00006968    -0.04568842     0.02319471     0.04918151     0.11593688

 trial vector basis is being transformed.  new dimension:   4
 NCSF=                   115                  1890                  6280
                  5685

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 20  1    -25.7583936770 -3.5527E-15  0.0000E+00  7.9295E-06  1.0000E-05
 mr-sdci # 20  2    -25.7163571675  2.4450E-08  6.4189E-09  1.0541E-04  1.0000E-05
 mr-sdci # 20  3    -25.3945723107  1.8708E-07  0.0000E+00  2.7127E-01  1.0000E-04
 mr-sdci # 20  4    -25.3778373379  8.2017E-03  0.0000E+00  2.0911E-01  1.0000E-04
 mr-sdci # 20  5    -24.9426429997  3.3366E-01  0.0000E+00  7.1051E-01  1.0000E-04
 mr-sdci # 20  6    -24.3731871443  5.1187E-01  0.0000E+00  9.3182E-01  1.0000E-04
 mr-sdci # 20  7    -23.5808825419  4.5842E-02  0.0000E+00  1.1138E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0014
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0013
    3   26    0     0.01     0.00     0.00     0.01         0.    0.0025
    4   27    0     0.00     0.00     0.00     0.00         0.    0.0012
    5   11    0     0.01     0.01     0.00     0.01         0.    0.0085
    6   15    0     0.01     0.00     0.00     0.01         0.    0.0092
    7   16    0     0.01     0.01     0.00     0.01         0.    0.0072
    8    1    0     0.01     0.01     0.00     0.01         0.    0.0050
    9    5    0     0.01     0.00     0.00     0.01         0.    0.0119
   10    6    0     0.01     0.00     0.00     0.01         0.    0.0021
   11   18    0     0.00     0.00     0.00     0.00         0.    0.0021
   12    7    0     0.01     0.00     0.00     0.01         0.    0.0011
   13   19    0     0.00     0.00     0.00     0.00         0.    0.0011
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0005
   15   43    0     0.00     0.00     0.00     0.00         0.    0.0005
   16   44    0     0.00     0.00     0.00     0.00         0.    0.0005
   17   75    0     0.01     0.01     0.00     0.01         0.    0.0009
   18   76    0     0.00     0.00     0.00     0.00         0.    0.0020
   19   77    0     0.00     0.00     0.00     0.00         0.    0.0007
   20   78    0     0.00     0.01     0.00     0.00         0.    0.0001
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0900s 
time spent in multnx:                   0.0900s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0500s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1000s 

          starting ci iteration  21

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.000290
ci vector #   2dasum_wr=    0.000740
ci vector #   3dasum_wr=    0.000569
ci vector #   4dasum_wr=    0.000685


====================================================================================================
Diagonal     counts:  0x:        5280 2x:        1480 4x:         370
All internal counts: zz :        5629 yy:       10577 xx:        5740 ww:        3548
One-external counts: yz :        9682 yx:       10444 yw:        8199
Two-external counts: yy :        4346 ww:        1530 xx:        2750 xz:         818 wz:         810 wx:        2990
Three-ext.   counts: yx :        1415 yw:        1165

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:        2479    task #     4:        1239
task #     5:        8526    task #     6:        9200    task #     7:        7175    task #     8:        4962
task #     9:       11867    task #    10:        2061    task #    11:        2061    task #    12:        1071
task #    13:        1071    task #    14:         535    task #    15:         537    task #    16:         537
task #    17:         865    task #    18:        2004    task #    19:         718    task #    20:         532
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000479     0.00003672    -0.00000011    -0.00000719     0.00000001
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1   -29.95493157
   ht   2     0.00000000   -29.91289506
   ht   3     0.00000000     0.00000000   -29.59111020
   ht   4     0.00000000     0.00000000     0.00000000   -29.57437523
   ht   5    -0.00014350    -0.00109840     0.00000317     0.00021782    -0.00000020

                v:   1         v:   2         v:   3         v:   4         v:   5

   eig(s)   5.467086E-09    1.00000        1.00000        1.00000        1.00000    
 
   x:   1   4.790608E-06  -0.103844       3.063546E-04   0.986452      -0.126999    
   x:   2   3.671972E-05   0.204040       2.348190E-03  -0.103844      -0.973437    
   x:   3  -1.083281E-07   2.348190E-03   0.999993       3.063546E-04   2.871768E-03
   x:   4  -7.185416E-06   0.973437      -2.871768E-03   0.126999       0.190485    
   x:   5   -1.00000        0.00000        0.00000        0.00000      -3.772174E-05
 bummer (warning):overlap matrix: # small eigenvalues=                      1

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1  -0.984362       7.607247E-08   2.013717E-02  -1.194016E-04  -4.941066E-06
 refs   2   5.316427E-09  -0.980053      -6.968139E-05  -4.568842E-02  -3.949985E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 civs   1   -1.00000      -3.637733E-06   3.728219E-05  -3.787183E-03  -6.467996E-02
 civs   2   1.848747E-08   0.999972       2.859202E-04  -2.904364E-02  -0.495823    
 civs   3  -1.098233E-10   1.757463E-07  -0.999998      -2.414434E-03   1.032559E-03
 civs   4   3.425602E-09  -6.461342E-06   2.414742E-03  -0.992607       0.155467    
 civs   5  -5.055776E-04   0.759330       -7.78233        790.541        13501.4    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1   0.984362      -9.067271E-08  -2.013565E-02  -1.082600E-04  -3.040608E-03
 ref    2  -3.621396E-09  -0.980056      -1.346077E-05   4.258885E-02  -5.447337E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     0.98436215    -0.00000009    -0.02013565    -0.00010826    -0.00304061
 ref:   2     0.00000000    -0.98005587    -0.00001346     0.04258885    -0.05447337
 NCSF=                   115                  1890                  6280
                  5685

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 21  1    -25.7583936770  7.1054E-15  0.0000E+00  7.9295E-06  1.0000E-05
 mr-sdci # 21  2    -25.7163571723  4.8740E-09  1.6787E-09  4.4293E-05  1.0000E-05
 mr-sdci # 21  3    -25.3945726190  3.0832E-07  0.0000E+00  2.7127E-01  1.0000E-04
 mr-sdci # 21  4    -25.3820446603  4.2073E-03  0.0000E+00  2.0634E-01  1.0000E-04
 mr-sdci # 21  5    -24.1506706095 -7.9197E-01  0.0000E+00  1.1852E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0014
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0013
    3   26    0     0.00     0.00     0.00     0.00         0.    0.0025
    4   27    0     0.01     0.00     0.00     0.01         0.    0.0012
    5   11    0     0.00     0.00     0.00     0.00         0.    0.0085
    6   15    0     0.02     0.01     0.00     0.02         0.    0.0092
    7   16    0     0.01     0.00     0.00     0.01         0.    0.0072
    8    1    0     0.00     0.00     0.00     0.00         0.    0.0050
    9    5    0     0.02     0.01     0.00     0.02         0.    0.0119
   10    6    0     0.00     0.00     0.00     0.00         0.    0.0021
   11   18    0     0.01     0.00     0.00     0.01         0.    0.0021
   12    7    0     0.00     0.00     0.00     0.00         0.    0.0011
   13   19    0     0.01     0.00     0.00     0.01         0.    0.0011
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0005
   15   43    0     0.00     0.00     0.00     0.00         0.    0.0005
   16   44    0     0.00     0.00     0.00     0.00         0.    0.0005
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0009
   18   76    0     0.00     0.00     0.00     0.00         0.    0.0020
   19   77    0     0.00     0.00     0.00     0.00         0.    0.0007
   20   78    0     0.01     0.00     0.01     0.01         0.    0.0001
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0900s 
time spent in multnx:                   0.0900s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.0200s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1000s 

          starting ci iteration  22

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.000188
ci vector #   2dasum_wr=    0.000323
ci vector #   3dasum_wr=    0.000208
ci vector #   4dasum_wr=    0.000245


====================================================================================================
Diagonal     counts:  0x:        5280 2x:        1480 4x:         370
All internal counts: zz :        5629 yy:       10577 xx:        5740 ww:        3548
One-external counts: yz :        9682 yx:       10444 yw:        8199
Two-external counts: yy :        4346 ww:        1530 xx:        2750 xz:         818 wz:         810 wx:        2990
Three-ext.   counts: yx :        1415 yw:        1165

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:        2479    task #     4:        1239
task #     5:        8526    task #     6:        9200    task #     7:        7175    task #     8:        4962
task #     9:       11867    task #    10:        2061    task #    11:        2061    task #    12:        1071
task #    13:        1071    task #    14:         535    task #    15:         537    task #    16:         537
task #    17:         865    task #    18:        2004    task #    19:         718    task #    20:         532
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000479     0.00003672    -0.00000011    -0.00000719     0.00000001
 sovl   6    -0.00000491    -0.00002797     0.00000011     0.00000161     0.00000000     0.00000000
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1   -29.95493157
   ht   2     0.00000000   -29.91289506
   ht   3     0.00000000     0.00000000   -29.59111020
   ht   4     0.00000000     0.00000000     0.00000000   -29.57437523
   ht   5    -0.00014350    -0.00109840     0.00000317     0.00021782    -0.00000020
   ht   6     0.00014714     0.00083668    -0.00000307    -0.00004744     0.00000006    -0.00000010

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6

   eig(s)   2.273195E-09   5.699858E-09    1.00000        1.00000        1.00000        1.00000    
 
   x:   1  -3.493559E-06   5.905243E-06  -0.255063       0.899308       0.324792      -0.143865    
   x:   2  -1.743360E-05   4.274070E-05   5.352724E-02  -0.173664       8.911086E-02  -0.979303    
   x:   3   7.368739E-08  -1.320991E-07   0.961332       0.275309      -5.938795E-03   3.183033E-03
   x:   4  -3.215897E-07  -7.355832E-06   8.898206E-02  -0.292045       0.941559       0.142330    
   x:   5  -0.260633      -0.965438       8.419086E-11   2.354110E-13  -1.936774E-06  -3.767199E-05
   x:   6  -0.965438       0.260633       1.106157E-10    0.00000      -2.575670E-06   2.832747E-05
 bummer (warning):overlap matrix: # small eigenvalues=                      2

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1  -0.984362       7.607247E-08   2.013717E-02  -1.194016E-04  -4.941066E-06   5.066727E-06
 refs   2   5.316427E-09  -0.980053      -6.968139E-05  -4.568842E-02  -3.949985E-05   2.765277E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1   -1.00000       2.283250E-07  -2.338575E-04  -3.388553E-03   6.635494E-02   8.409879E-02
 civs   2   5.485676E-08   0.999992      -1.172368E-03  -6.426187E-03   0.317113       0.594650    
 civs   3  -4.991420E-10  -4.960893E-08   0.999984      -5.888778E-03   1.085711E-03  -1.287164E-03
 civs   4  -3.585106E-09  -1.076147E-05   5.635867E-03   0.985625       0.128714      -0.146569    
 civs   5   2.093514E-06    1.03565       -16.8419       -1360.55        6427.95       -12257.2    
 civs   6   1.963959E-03    1.05656       -64.0361       -2016.82        19778.2        5166.92    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1   0.984362       8.767086E-08   2.012514E-02  -3.967722E-04   3.138841E-03   3.950782E-03
 ref    2  -4.688936E-09  -0.980056      -2.837130E-04  -4.076208E-02  -2.365078E-02   5.094328E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.98436215     0.00000009     0.02012514    -0.00039677     0.00313884     0.00395078
 ref:   2     0.00000000    -0.98005639    -0.00028371    -0.04076208    -0.02365078     0.05094328
 NCSF=                   115                  1890                  6280
                  5685

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 22  1    -25.7583936770  0.0000E+00  0.0000E+00  7.9288E-06  1.0000E-05
 mr-sdci # 22  2    -25.7163571741  1.7737E-09  4.7584E-10  2.6485E-05  1.0000E-05
 mr-sdci # 22  3    -25.3945751105  2.4915E-06  0.0000E+00  2.7125E-01  1.0000E-04
 mr-sdci # 22  4    -25.3852866551  3.2420E-03  0.0000E+00  1.9779E-01  1.0000E-04
 mr-sdci # 22  5    -25.0651305627  9.1446E-01  0.0000E+00  7.0500E-01  1.0000E-04
 mr-sdci # 22  6    -24.0887378276 -2.8445E-01  0.0000E+00  1.1909E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0014
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0013
    3   26    0     0.01     0.00     0.00     0.01         0.    0.0025
    4   27    0     0.00     0.00     0.00     0.00         0.    0.0012
    5   11    0     0.01     0.01     0.00     0.01         0.    0.0085
    6   15    0     0.01     0.00     0.00     0.01         0.    0.0092
    7   16    0     0.01     0.01     0.00     0.01         0.    0.0072
    8    1    0     0.01     0.01     0.00     0.01         0.    0.0050
    9    5    0     0.01     0.01     0.00     0.01         0.    0.0119
   10    6    0     0.01     0.00     0.00     0.01         0.    0.0021
   11   18    0     0.00     0.00     0.00     0.00         0.    0.0021
   12    7    0     0.01     0.00     0.00     0.01         0.    0.0011
   13   19    0     0.00     0.00     0.00     0.00         0.    0.0011
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0005
   15   43    0     0.00     0.00     0.00     0.00         0.    0.0005
   16   44    0     0.00     0.00     0.00     0.00         0.    0.0005
   17   75    0     0.01     0.01     0.00     0.01         0.    0.0009
   18   76    0     0.00     0.00     0.00     0.00         0.    0.0020
   19   77    0     0.00     0.00     0.00     0.00         0.    0.0007
   20   78    0     0.00     0.00     0.00     0.00         0.    0.0001
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0900s 
time spent in multnx:                   0.0900s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0500s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.0900s 

          starting ci iteration  23

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.000105
ci vector #   2dasum_wr=    0.000196
ci vector #   3dasum_wr=    0.000156
ci vector #   4dasum_wr=    0.000170


====================================================================================================
Diagonal     counts:  0x:        5280 2x:        1480 4x:         370
All internal counts: zz :        5629 yy:       10577 xx:        5740 ww:        3548
One-external counts: yz :        9682 yx:       10444 yw:        8199
Two-external counts: yy :        4346 ww:        1530 xx:        2750 xz:         818 wz:         810 wx:        2990
Three-ext.   counts: yx :        1415 yw:        1165

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:        2479    task #     4:        1239
task #     5:        8526    task #     6:        9200    task #     7:        7175    task #     8:        4962
task #     9:       11867    task #    10:        2061    task #    11:        2061    task #    12:        1071
task #    13:        1071    task #    14:         535    task #    15:         537    task #    16:         537
task #    17:         865    task #    18:        2004    task #    19:         718    task #    20:         532
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5       sovl   6       sovl   7
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000479     0.00003672    -0.00000011    -0.00000719     0.00000001
 sovl   6    -0.00000491    -0.00002797     0.00000011     0.00000161     0.00000000     0.00000000
 sovl   7     0.00000546     0.00001914    -0.00000012     0.00000012     0.00000000     0.00000000     0.00000000
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6         ht   7
   ht   1   -29.95493157
   ht   2     0.00000000   -29.91289506
   ht   3     0.00000000     0.00000000   -29.59111020
   ht   4     0.00000000     0.00000000     0.00000000   -29.57437523
   ht   5    -0.00014350    -0.00109840     0.00000317     0.00021782    -0.00000020
   ht   6     0.00014714     0.00083668    -0.00000307    -0.00004744     0.00000006    -0.00000010
   ht   7    -0.00016347    -0.00057256     0.00000348    -0.00000361    -0.00000004     0.00000002    -0.00000003

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6         v:   7

   eig(s)   4.417905E-10   2.273859E-09   5.750970E-09    1.00000        1.00000        1.00000        1.00000    
 
   x:   1  -4.783934E-06   3.579167E-06   6.415706E-06  -2.391122E-02   0.832820      -0.528212       0.163799    
   x:   2  -1.451975E-05   1.766993E-05   4.442994E-05   5.409208E-04  -0.202663      -1.593427E-02   0.979119    
   x:   3   1.048552E-07  -7.556059E-08  -1.432922E-07  -0.999712      -2.100851E-02   1.100990E-02  -3.616984E-03
   x:   4  -8.337098E-07   3.448113E-07  -7.308342E-06   1.902306E-03  -0.514681      -0.848891      -0.120347    
   x:   5  -8.972630E-02   0.263346      -0.960520      -5.879909E-11   2.484433E-07   2.982885E-06   3.760280E-05
   x:   6   4.399382E-02   0.964516       0.260332      -1.631894E-10   7.487394E-07   1.677477E-06  -2.838494E-05
   x:   7   0.994994      -1.889829E-02  -9.812808E-02  -1.233840E-10   6.070360E-07  -3.289801E-06   1.962114E-05
 bummer (warning):overlap matrix: # small eigenvalues=                      3

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6       civs   7
 refs   1  -0.984362       7.607247E-08   2.013717E-02  -1.194016E-04  -4.941066E-06   5.066727E-06  -5.628681E-06
 refs   2   5.316427E-09  -0.980053      -6.968139E-05  -4.568842E-02  -3.949985E-05   2.765277E-05  -2.005482E-05

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 civs   1   -1.00000      -3.902662E-06   1.187347E-03  -1.089863E-02  -4.649936E-02   0.220087      -0.117785    
 civs   2   1.871065E-07   0.999978       4.015662E-03  -3.452045E-02  -9.105280E-02   0.961104      -0.156800    
 civs   3  -2.923507E-09   2.090054E-07  -0.999739      -2.313973E-02  -3.347306E-03  -2.071710E-03   1.520272E-03
 civs   4   3.951825E-09  -1.168460E-05   2.134901E-02  -0.961380       0.253563      -6.010386E-02  -0.135272    
 civs   5  -6.236939E-04    1.10494       -71.0582        2034.11        5751.27       -3946.36       -12504.1    
 civs   6  -5.424288E-04    1.32147       -145.737        4120.01        17376.8        10290.2        674.249    
 civs   7  -9.376399E-03   0.934579       -286.371        3919.81        19112.5       -27603.4        33167.2    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6       v      7
 ref    1   0.984362      -1.011874E-07  -2.007865E-02  -8.620120E-04  -2.277554E-03   1.032762E-02  -5.497295E-03
 ref    2   8.806991E-09  -0.980058      -3.214317E-04   3.272923E-02  -5.230369E-02   5.482641E-02   7.247764E-03

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.98436215    -0.00000010    -0.02007865    -0.00086201    -0.00227755     0.01032762    -0.00549729
 ref:   2     0.00000001    -0.98005773    -0.00032143     0.03272923    -0.05230369     0.05482641     0.00724776

 trial vector basis is being transformed.  new dimension:   4
 NCSF=                   115                  1890                  6280
                  5685

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 23  1    -25.7583936770  5.3291E-14  0.0000E+00  7.9251E-06  1.0000E-05
 mr-sdci # 23  2    -25.7163571746  4.4471E-10  1.1156E-10  1.4276E-05  1.0000E-05
 mr-sdci # 23  3    -25.3945872691  1.2159E-05  0.0000E+00  2.7116E-01  1.0000E-04
 mr-sdci # 23  4    -25.3889435316  3.6569E-03  0.0000E+00  1.9712E-01  1.0000E-04
 mr-sdci # 23  5    -25.2536254624  1.8849E-01  0.0000E+00  4.8793E-01  1.0000E-04
 mr-sdci # 23  6    -24.4329222706  3.4418E-01  0.0000E+00  9.6806E-01  1.0000E-04
 mr-sdci # 23  7    -23.6555120694  7.4630E-02  0.0000E+00  1.1546E+00  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.01     0.01     0.00     0.01         0.    0.0014
    2   25    0     0.00     0.00     0.00     0.00         0.    0.0013
    3   26    0     0.00     0.00     0.00     0.00         0.    0.0025
    4   27    0     0.01     0.00     0.00     0.01         0.    0.0012
    5   11    0     0.01     0.00     0.00     0.01         0.    0.0085
    6   15    0     0.02     0.01     0.00     0.02         0.    0.0092
    7   16    0     0.01     0.00     0.00     0.01         0.    0.0072
    8    1    0     0.01     0.01     0.00     0.01         0.    0.0050
    9    5    0     0.01     0.01     0.00     0.01         0.    0.0119
   10    6    0     0.01     0.00     0.00     0.01         0.    0.0021
   11   18    0     0.00     0.00     0.00     0.00         0.    0.0021
   12    7    0     0.01     0.00     0.00     0.01         0.    0.0011
   13   19    0     0.00     0.00     0.00     0.00         0.    0.0011
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0005
   15   43    0     0.00     0.00     0.00     0.00         0.    0.0005
   16   44    0     0.00     0.00     0.00     0.00         0.    0.0005
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0009
   18   76    0     0.00     0.00     0.00     0.00         0.    0.0020
   19   77    0     0.01     0.00     0.00     0.00         0.    0.0007
   20   78    0     0.00     0.00     0.00     0.00         0.    0.0001
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1100s 
time spent in multnx:                   0.1000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0400s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1100s 

          starting ci iteration  24

 =========== Executing IN-CORE method ==========
ci vector #   1dasum_wr=    0.000045
ci vector #   2dasum_wr=    0.000100
ci vector #   3dasum_wr=    0.000084
ci vector #   4dasum_wr=    0.000108


====================================================================================================
Diagonal     counts:  0x:        5280 2x:        1480 4x:         370
All internal counts: zz :        5629 yy:       10577 xx:        5740 ww:        3548
One-external counts: yz :        9682 yx:       10444 yw:        8199
Two-external counts: yy :        4346 ww:        1530 xx:        2750 xz:         818 wz:         810 wx:        2990
Three-ext.   counts: yx :        1415 yw:        1165

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:        2479    task #     4:        1239
task #     5:        8526    task #     6:        9200    task #     7:        7175    task #     8:        4962
task #     9:       11867    task #    10:        2061    task #    11:        2061    task #    12:        1071
task #    13:        1071    task #    14:         535    task #    15:         537    task #    16:         537
task #    17:         865    task #    18:        2004    task #    19:         718    task #    20:         532
 Final Overlap matrix sovl

              sovl   1       sovl   2       sovl   3       sovl   4       sovl   5
 sovl   1     1.00000000
 sovl   2     0.00000000     1.00000000
 sovl   3     0.00000000     0.00000000     1.00000000
 sovl   4     0.00000000     0.00000000     0.00000000     1.00000000
 sovl   5     0.00000559    -0.00000821    -0.00000014     0.00000060     0.00000000
 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1   -29.95493157
   ht   2     0.00000000   -29.91289507
   ht   3     0.00000000     0.00000000   -29.59112516
   ht   4     0.00000000     0.00000000     0.00000000   -29.58548142
   ht   5    -0.00016739     0.00024553     0.00000404    -0.00001832    -0.00000001

                v:   1         v:   2         v:   3         v:   4         v:   5

   eig(s)   9.093712E-11    1.00000        1.00000        1.00000        1.00000    
 
   x:   1   5.588019E-06   0.437036       7.340197E-03   0.702476       0.561670    
   x:   2  -8.208285E-06   0.358035      -1.078207E-02   0.437036      -0.825042    
   x:   3  -1.378616E-07  -1.078207E-02   0.999819       7.340197E-03  -1.385693E-02
   x:   4   6.002137E-07   0.825042       1.385693E-02  -0.561670       6.032949E-02
   x:   5   -1.00000        0.00000        0.00000        0.00000       9.948928E-06
 bummer (warning):overlap matrix: # small eigenvalues=                      1

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1   0.984362      -1.011874E-07  -2.007865E-02  -8.620120E-04   5.763592E-06
 refs   2   8.806991E-09  -0.980058      -3.214317E-04   3.272923E-02   9.022229E-06

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 civs   1   -1.00000      -4.434576E-06   3.835425E-03   2.761979E-02  -0.585323    
 civs   2  -3.020680E-07    1.00001      -5.633633E-03  -4.056912E-02   0.859777    
 civs   3  -9.059753E-09   2.066675E-07  -0.997788       6.711359E-02   1.110195E-02
 civs   4  -3.350259E-08   8.766270E-07  -6.714887E-02  -0.993619      -0.110339    
 civs   5  -3.678849E-02   0.793568       -686.364       -4942.67        104746.    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1  -0.984362       1.024828E-07   1.991165E-02  -1.790677E-03   2.741551E-02
 ref    2  -4.577066E-08  -0.980057      -2.548257E-03  -3.737574E-02   9.879588E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1    -0.98436216     0.00000010     0.01991165    -0.00179068     0.02741551
 ref:   2    -0.00000005    -0.98005693    -0.00254826    -0.03737574     0.09879588
 NCSF=                   115                  1890                  6280
                  5685

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 24  1    -25.7583936770  1.7408E-13  0.0000E+00  7.9195E-06  1.0000E-05
 mr-sdci # 24  2    -25.7163571747  8.8477E-11  3.0491E-11  5.8758E-06  1.0000E-05
 mr-sdci # 24  3    -25.3946144007  2.7132E-05  0.0000E+00  2.7088E-01  1.0000E-04
 mr-sdci # 24  4    -25.3917119839  2.7685E-03  0.0000E+00  1.8640E-01  1.0000E-04
 mr-sdci # 24  5    -24.1572029693 -1.0964E+00  0.0000E+00  1.1527E+00  1.0000E-04
 
======================== TIMING STATISTICS PER TASK    ========================
task# type node    tmult    tloop     tint    tmnx    MFLIPS     mloop 
    1   24    0     0.00     0.00     0.00     0.00         0.    0.0014
    2   25    0     0.01     0.01     0.00     0.01         0.    0.0013
    3   26    0     0.00     0.00     0.00     0.00         0.    0.0025
    4   27    0     0.01     0.00     0.00     0.01         0.    0.0012
    5   11    0     0.00     0.00     0.00     0.00         0.    0.0085
    6   15    0     0.02     0.01     0.00     0.02         0.    0.0092
    7   16    0     0.01     0.00     0.00     0.01         0.    0.0072
    8    1    0     0.01     0.00     0.01     0.00         0.    0.0050
    9    5    0     0.01     0.01     0.00     0.01         0.    0.0119
   10    6    0     0.01     0.01     0.00     0.01         0.    0.0021
   11   18    0     0.01     0.00     0.00     0.01         0.    0.0021
   12    7    0     0.00     0.00     0.00     0.00         0.    0.0011
   13   19    0     0.00     0.00     0.00     0.00         0.    0.0011
   14   42    0     0.00     0.00     0.00     0.00         0.    0.0005
   15   43    0     0.01     0.00     0.00     0.01         0.    0.0005
   16   44    0     0.00     0.00     0.00     0.00         0.    0.0005
   17   75    0     0.00     0.00     0.00     0.00         0.    0.0009
   18   76    0     0.00     0.00     0.00     0.00         0.    0.0020
   19   77    0     0.00     0.00     0.00     0.00         0.    0.0007
   20   78    0     0.00     0.00     0.00     0.00         0.    0.0001
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1000s 
time spent in multnx:                   0.0900s 
integral transfer time:                 0.0100s 
time spent for loop construction:       0.0400s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1100s 

 mr-sdci  convergence criteria satisfied after 24 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 24  1    -25.7583936770  1.7408E-13  0.0000E+00  7.9195E-06  1.0000E-05
 mr-sdci # 24  2    -25.7163571747  8.8477E-11  3.0491E-11  5.8758E-06  1.0000E-05
 mr-sdci # 24  3    -25.3946144007  2.7132E-05  0.0000E+00  2.7088E-01  1.0000E-04
 mr-sdci # 24  4    -25.3917119839  2.7685E-03  0.0000E+00  1.8640E-01  1.0000E-04
 mr-sdci # 24  5    -24.1572029693 -1.0964E+00  0.0000E+00  1.1527E+00  1.0000E-04

####################CIUDGINFO####################

   ci vector at position   1 energy=  -25.758393677015
   ci vector at position   2 energy=  -25.716357174652

################END OF CIUDGINFO################

 
diagon:itrnv=   0
    2 of the   6 expansion vectors are transformed.
    2 of the   5 matrix-vector products are transformed.

    2 expansion eigenvectors written to unit nvfile (= 11)
    2 matrix-vector products written to unit nhvfil (= 10)
maximum overlap with reference    1(overlap= 0.98436)

 information on vector: 1 from unit 11 written to unit 48 filename civout              
maximum overlap with reference    2(overlap= 0.98006)

 information on vector: 2 from unit 11 written to unit 48 filename civout              


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -25.7583936770

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7

                                          orbital     1    2    3    4    5    6   19

                                         symmetry   a'   a'   a'   a'   a'   a'   a" 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  2  1       1  0.947322                        +-   +-   +-   +                 
 z*  2  1       3 -0.074011                        +-   +-   +-             +       
 z*  2  1       5 -0.119429                        +-   +-   +     -   +            
 z*  2  1       7  0.101932                        +-   +-   +    +     -           
 z*  2  1      10  0.024485                        +-   +-   +          -   +       
 z*  2  1      11  0.024445                        +-   +-   +         +     -      
 z*  2  1      15  0.036145                        +-   +-        +-        +       
 z*  2  1      16  0.095674                        +-   +-        +    +-           
 z*  2  1      19  0.036853                        +-   +-        +         +-      
 z*  2  1      20  0.029346                        +-   +-        +              +- 
 z*  2  1      25  0.073295                        +-   +    +-   +-                
 z*  2  1      27  0.012190                        +-   +    +-    -        +       
 z*  2  1      29  0.029154                        +-   +    +-   +          -      
 z*  2  1      33  0.012176                        +-   +    +-             +-      
 z*  2  1      34 -0.057484                        +-   +    +-                  +- 
 z*  2  1      35  0.067485                        +-   +     -   +-   +            
 z*  2  1      38  0.043797                        +-   +     -   +     -   +       
 z*  2  1      39  0.071382                        +-   +     -   +    +     -      
 z*  2  1      46  0.017010                        +-   +    +    +-    -           
 z*  2  1      49 -0.016131                        +-   +    +     -    -   +       
 z*  2  1      77  0.019738                        +-        +-   +-        +       
 z*  2  1      78  0.033776                        +-        +-   +    +-           
 z*  2  1      81  0.047732                        +-        +-   +         +-      
 z*  2  1      82  0.040638                        +-        +-   +              +- 
 z*  2  1     109  0.010454                        +-             +    +-   +-      
 y   2  1     120  0.031779              5( a' )   +-   +-   +-                     
 y   2  1     121  0.011689              6( a' )   +-   +-   +-                     
 y   2  1     122  0.023080              7( a' )   +-   +-   +-                     
 y   2  1     124  0.011966              9( a' )   +-   +-   +-                     
 y   2  1     129  0.016389              2( a' )   +-   +-    -   +                 
 y   2  1     145 -0.038553              6( a' )   +-   +-    -        +            
 y   2  1     166  0.027165              3( a" )   +-   +-    -                  +  
 y   2  1     172  0.012966              4( a' )   +-   +-   +     -                
 y   2  1     176  0.025002              8( a' )   +-   +-   +     -                
 y   2  1     181  0.015923              1( a' )   +-   +-   +          -           
 y   2  1     185 -0.019648              5( a' )   +-   +-   +          -           
 y   2  1     207 -0.014084              3( a" )   +-   +-   +                    - 
 y   2  1     215 -0.010255              6( a' )   +-   +-        +-                
 y   2  1     362  0.010774              3( a' )   +-    -   +-   +                 
 y   2  1     365  0.011245              6( a' )   +-    -   +-   +                 
 y   2  1     373  0.010748              2( a' )   +-    -   +-        +            
 y   2  1     397 -0.010817              2( a" )   +-    -   +-                  +  
 y   2  1     418  0.012843              6( a' )   +-    -   +     -   +            
 y   2  1     447 -0.014623              6( a' )   +-    -   +    +     -           
 y   2  1     455 -0.010060              2( a' )   +-    -   +    +          -      
 y   2  1     732 -0.015408              5( a' )   +-   +    +-    -                
 y   2  1     747  0.013443              8( a' )   +-   +    +-         -           
 y   2  1     752  0.012862              1( a' )   +-   +    +-              -      
 y   2  1     764 -0.011413              1( a" )   +-   +    +-                   - 
 y   2  1     765 -0.020431              2( a" )   +-   +    +-                   - 
 y   2  1     794 -0.011867              2( a' )   +-   +     -    -        +       
 y   2  1     800 -0.012375              8( a' )   +-   +     -    -        +       
 y   2  1     807 -0.020960              3( a" )   +-   +     -    -             +  
 y   2  1     823  0.023193              2( a' )   +-   +     -   +          -      
 y   2  1     829  0.017465              8( a' )   +-   +     -   +          -      
 y   2  1     836  0.035720              3( a" )   +-   +     -   +               - 
 y   2  1    1176 -0.010962              3( a' )   +-        +-   +-                
 y   2  1    1211  0.013967              2( a" )   +-        +-    -             +  
 y   2  1    1229  0.013658              3( a' )   +-        +-   +          -      
 y   2  1    1240 -0.023234              2( a" )   +-        +-   +               - 
 x   2  1    2006 -0.022324    1( a' )   2( a' )   +-   +-    -                     
 x   2  1    2013 -0.014303    2( a' )   5( a' )   +-   +-    -                     
 x   2  1    2032 -0.013439    6( a' )   8( a' )   +-   +-    -                     
 x   2  1    2074  0.012443    2( a" )   3( a" )   +-   +-    -                     
 x   2  1    2448 -0.017464    2( a' )   3( a' )   +-    -    -   +                 
 x   2  1    2457 -0.010670    2( a' )   6( a' )   +-    -    -   +                 
 w   2  1    8287 -0.016876    1( a' )   2( a' )   +-   +-   +                      
 w   2  1    8297  0.015305    2( a' )   5( a' )   +-   +-   +                      
 w   2  1    8381 -0.027512    2( a' )   2( a' )   +-   +-        +                 
 w   2  1    8384 -0.013973    3( a' )   3( a' )   +-   +-        +                 
 w   2  1    8386  0.018041    2( a' )   4( a' )   +-   +-        +                 
 w   2  1    8388 -0.014294    4( a' )   4( a' )   +-   +-        +                 
 w   2  1    8399 -0.027763    6( a' )   6( a' )   +-   +-        +                 
 w   2  1    8414 -0.010476    8( a' )   8( a' )   +-   +-        +                 
 w   2  1    8462 -0.019053    3( a" )   3( a" )   +-   +-        +                 
 w   2  1    8815 -0.027641    2( a' )   3( a' )   +-   +     -   +                 
 w   2  1    8819  0.024349    3( a' )   4( a' )   +-   +     -   +                 
 w   2  1    8844  0.010922    6( a' )   8( a' )   +-   +     -   +                 
 w   2  1    8861 -0.012689    6( a' )  10( a' )   +-   +     -   +                 
 w   2  1    8879  0.011446    3( a' )  12( a' )   +-   +     -   +                 
 w   2  1    8893  0.019162    2( a" )   3( a" )   +-   +     -   +                 
 w   2  1    8896 -0.014148    2( a" )   4( a" )   +-   +     -   +                 
 w   2  1    8902  0.011852    4( a" )   5( a" )   +-   +     -   +                 
 w   2  1   10784 -0.022422    3( a' )   3( a' )   +-        +-   +                 
 w   2  1   10799 -0.013632    6( a' )   6( a' )   +-        +-   +                 
 w   2  1   10859 -0.018197    2( a" )   2( a" )   +-        +-   +                 

 ci coefficient statistics:
           rq > 0.1                3
      0.1> rq > 0.01              83
     0.01> rq > 0.001            819
    0.001> rq > 0.0001          3106
   0.0001> rq > 0.00001         2462
  0.00001> rq > 0.000001         479
 0.000001> rq                   7018
           all                 13970
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        1505 2x:           0 4x:           0
All internal counts: zz :        5629 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:        2479    task #     4:        1239
task #     5:        8526    task #     6:        9200    task #     7:        7175    task #     8:        4962
task #     9:       11867    task #    10:        2061    task #    11:        2061    task #    12:        1071
task #    13:        1071    task #    14:         535    task #    15:         537    task #    16:         537
task #    17:         865    task #    18:        2004    task #    19:         718    task #    20:         100
  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.947322290193    -24.347476113936
     2     2     -0.000001218499      0.000031288903
     3     3     -0.074011060235      1.898145490449
     4     4     -0.000006414623      0.000164907164
     5     5     -0.119429125586      3.068162109993
     6     6      0.000000971326     -0.000024989509
     7     7      0.101932116538     -2.619063671881
     8     8      0.000000007557     -0.000000210209
     9     9      0.000000730935     -0.000018746099
    10    10      0.024484590778     -0.623820642384
    11    11      0.024444745944     -0.631247310212
    12    12      0.000000271206     -0.000006966344
    13    13      0.000000231524     -0.000005909682
    14    14      0.000001186754     -0.000030484701
    15    15      0.036145147342     -0.932408470755
    16    16      0.095673797942     -2.464694543499
    17    17     -0.000000137201      0.000003517665
    18    18     -0.000000127106      0.000003307661
    19    19      0.036853100208     -0.948890010269
    20    20      0.029345547748     -0.750159806131
    21    21      0.008237111266     -0.213728713052
    22    22      0.000000005450     -0.000000124018
    23    23     -0.000000043341      0.000001074628
    24    24      0.004966689981     -0.129129051151
    25    25      0.073294634743     -1.883143009602
    26    26      0.000000365738     -0.000009498130
    27    27      0.012189536936     -0.309979727463
    28    28      0.000000062547     -0.000001645586
    29    29      0.029153840408     -0.750188186906
    30    30     -0.001709834616      0.040249908143
    31    31      0.000000077928     -0.000002041458
    32    32      0.000000345609     -0.000008897952
    33    33      0.012176171176     -0.315167413914
    34    34     -0.057484227290      1.482352185688
    35    35      0.067484519067     -1.739357802901
    36    36     -0.000000112226      0.000002813617
    37    37     -0.000000041771      0.000001102105
    38    38      0.043797360191     -1.132432297693
    39    39      0.071381696225     -1.846770577619
    40    40     -0.000000044513      0.000001182440
    41    41      0.000000250320     -0.000006488715
    42    42     -0.000000010853      0.000000261974
    43    43      0.008492400082     -0.219581295884
    44    44      0.005974242037     -0.154927175231
    45    45     -0.000000003533      0.000000079377
    46    46      0.017010412184     -0.436324904793
    47    47     -0.000000206549      0.000005308355
    48    48     -0.000000056897      0.000001483749
    49    49     -0.016131297648      0.414352670236
    50    50     -0.001854555046      0.050458956521
    51    51     -0.000000164003      0.000004202518
    52    52      0.000000530722     -0.000013651345
    53    53     -0.007672830237      0.195177547511
    54    54      0.000000048047     -0.000001271871
    55    55      0.003161175786     -0.080866806683
    56    56     -0.004695520650      0.122576454825
    57    57      0.000000023434     -0.000000565610
    58    58     -0.007203870526      0.185043925679
    59    59     -0.000000100940      0.000002595326
    60    60     -0.000000372248      0.000009543781
    61    61     -0.004184404647      0.108322348285
    62    62     -0.000116575295      0.002140342549
    63    63      0.000378106484     -0.009140291286
    64    64     -0.000000057837      0.000001479652
    65    65     -0.000000018576      0.000000511685
    66    66      0.001340264781     -0.034895595224
    67    67      0.008492349774     -0.220217350260
    68    68     -0.000000023771      0.000000597069
    69    69      0.000000044339     -0.000001135189
    70    70      0.000063890479     -0.001434438194
    71    71     -0.003405550323      0.088103158851
    72    72      0.005916978161     -0.153454078017
    73    73     -0.000000009532      0.000000220470
    74    74      0.000000035117     -0.000000853306
    75    75      0.002099809206     -0.054385545900
    76    76     -0.000000037675      0.000000938466
    77    77      0.019738350594     -0.511208279236
    78    78      0.033776057326     -0.873410378341
    79    79     -0.000000033672      0.000000890007
    80    80     -0.000000009837      0.000000242505
    81    81      0.047732180767     -1.231416642162
    82    82      0.040637941104     -1.041779858669
    83    83      0.001939329627     -0.050624094644
    84    84     -0.000000065360      0.000001690987
    85    85     -0.000000012496      0.000000320618
    86    86      0.001455240866     -0.037835587616
    87    87     -0.000000252790      0.000006501662
    88    88     -0.004619114332      0.120655656022
    89    89      0.001313832232     -0.034378886818
    90    90     -0.000000352474      0.000009040854
    91    91     -0.000000271523      0.000006970839
    92    92     -0.000000095136      0.000002460965
    93    93     -0.006192164030      0.160714850182
    94    94     -0.000911675914      0.022320652697
    95    95      0.000000005954     -0.000000138585
    96    96     -0.000000003862      0.000000095325
    97    97      0.004490655499     -0.116548699161
    98    98      0.002485454101     -0.063075279412
    99    99     -0.000000008398      0.000000197305
   100   100      0.000000068707     -0.000001803367
   101   101      0.000000040953     -0.000001053342
   102   102      0.004288528929     -0.111251530237
   103   103      0.003447767986     -0.089379448359
   104   104      0.000000043605     -0.000001084415
   105   105     -0.007756073205      0.201568926939
   106   106      0.000000065763     -0.000001682822
   107   107      0.000000005820     -0.000000152508
   108   108      0.001813492022     -0.046571639890
   109   109      0.010454145906     -0.271411931060
   110   110      0.005309099127     -0.136684310213
   111   111     -0.000000028448      0.000000739473
   112   112     -0.000000018551      0.000000487568
   113   113      0.003164563200     -0.081439690624
   114   114     -0.001055307612      0.027492535185
   115   115      0.000000014892     -0.000000337587

 number of reference csfs (nref) is   115.  root number (iroot) is  1.
 c0**2 =   0.96965667  c**2 (all zwalks) =   0.96965667

 pople ci energy extrapolation is computed with  7 correlated electrons.

 eref      =    -25.703701602356   "relaxed" cnot**2         =   0.969656670490
 eci       =    -25.758393677015   deltae = eci - eref       =  -0.054692074659
 eci+dv1   =    -25.760053216658   dv1 = (1-cnot**2)*deltae  =  -0.001659539643
 eci+dv2   =    -25.760105148398   dv2 = dv1 / cnot**2       =  -0.001711471383
 eci+dv3   =    -25.760160435317   dv3 = dv1 / (2*cnot**2-1) =  -0.001766758302
 eci+pople =    -25.759632520087   (  7e- scaled deltae )    =  -0.055930917731


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 2) =       -25.7163571747

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7

                                          orbital     1    2    3    4    5    6   19

                                         symmetry   a'   a'   a'   a'   a'   a'   a" 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  2  1       2 -0.034306                        +-   +-   +-        +            
 z*  2  1       4  0.938105                        +-   +-   +    +-                
 z*  2  1       6  0.090526                        +-   +-   +     -        +       
 z*  2  1       8  0.048117                        +-   +-   +    +          -      
 z*  2  1       9 -0.083171                        +-   +-   +         +-           
 z*  2  1      12 -0.039232                        +-   +-   +              +-      
 z*  2  1      13 -0.177859                        +-   +-   +                   +- 
 z*  2  1      14  0.033594                        +-   +-        +-   +            
 z*  2  1      18 -0.050524                        +-   +-        +    +     -      
 z*  2  1      26  0.092421                        +-   +    +-    -   +            
 z*  2  1      28  0.044599                        +-   +    +-   +     -           
 z*  2  1      36  0.045274                        +-   +     -   +-        +       
 z*  2  1      40 -0.010078                        +-   +     -   +         +-      
 z*  2  1      41  0.027876                        +-   +     -   +              +- 
 z*  2  1      45  0.012326                        +-   +     -             +    +- 
 z*  2  1      47  0.017649                        +-   +    +    +-         -      
 z*  2  1      51  0.018353                        +-   +    +     -        +-      
 z*  2  1      52 -0.060267                        +-   +    +     -             +- 
 z*  2  1      59  0.013400                        +-   +         +-    -   +       
 z*  2  1      60  0.052636                        +-   +         +-   +     -      
 z*  2  1      64 -0.013512                        +-   +          -   +    +-      
 z*  2  1      74 -0.013084                        +-   +              +     -   +- 
 z*  2  1      76  0.028034                        +-        +-   +-   +            
 z*  2  1      85  0.011395                        +-        +-        +         +- 
 z*  2  1      87  0.031213                        +-        +    +-   +-           
 z*  2  1      90  0.043965                        +-        +    +-        +-      
 z*  2  1      91  0.035093                        +-        +    +-             +- 
 z*  2  1     104 -0.011445                        +-        +              +-   +- 
 y   2  1     123  0.021656              8( a' )   +-   +-   +-                     
 y   2  1     128 -0.014822              1( a' )   +-   +-    -   +                 
 y   2  1     132  0.024114              5( a' )   +-   +-    -   +                 
 y   2  1     134  0.015063              7( a' )   +-   +-    -   +                 
 y   2  1     147 -0.018543              8( a' )   +-   +-    -        +            
 y   2  1     156 -0.013761              5( a' )   +-   +-    -             +       
 y   2  1     157 -0.010273              6( a' )   +-   +-    -             +       
 y   2  1     165  0.025276              2( a" )   +-   +-    -                  +  
 y   2  1     173 -0.034313              5( a' )   +-   +-   +     -                
 y   2  1     174 -0.034271              6( a' )   +-   +-   +     -                
 y   2  1     175 -0.017474              7( a' )   +-   +-   +     -                
 y   2  1     177 -0.011524              9( a' )   +-   +-   +     -                
 y   2  1     188  0.028004              8( a' )   +-   +-   +          -           
 y   2  1     197  0.024751              5( a' )   +-   +-   +               -      
 y   2  1     198  0.015271              6( a' )   +-   +-   +               -      
 y   2  1     199  0.014601              7( a' )   +-   +-   +               -      
 y   2  1     205 -0.012136              1( a" )   +-   +-   +                    - 
 y   2  1     206 -0.037378              2( a" )   +-   +-   +                    - 
 y   2  1     211 -0.013110              2( a' )   +-   +-        +-                
 y   2  1     217 -0.014345              8( a' )   +-   +-        +-                
 y   2  1     222  0.011053              1( a' )   +-   +-         -   +            
 y   2  1     226 -0.027838              5( a' )   +-   +-         -   +            
 y   2  1     227 -0.012466              6( a' )   +-   +-         -   +            
 y   2  1     230  0.010680              9( a' )   +-   +-         -   +            
 y   2  1     256 -0.028842              6( a' )   +-   +-        +     -           
 y   2  1     277  0.026380              3( a" )   +-   +-        +               - 
 y   2  1     403 -0.011773              3( a' )   +-    -   +    +-                
 y   2  1     406 -0.010427              6( a' )   +-    -   +    +-                
 y   2  1     443  0.010818              2( a' )   +-    -   +    +     -           
 y   2  1     454  0.011056              1( a' )   +-    -   +    +          -      
 y   2  1     458 -0.012351              5( a' )   +-    -   +    +          -      
 y   2  1     467 -0.012393              2( a" )   +-    -   +    +               - 
 y   2  1     556  0.010610              6( a' )   +-    -        +-   +            
 y   2  1     771 -0.017090              3( a' )   +-   +     -   +-                
 y   2  1     773  0.010037              5( a' )   +-   +     -   +-                
 y   2  1     774  0.044963              6( a' )   +-   +     -   +-                
 y   2  1     775  0.015500              7( a' )   +-   +     -   +-                
 y   2  1     788  0.010737              8( a' )   +-   +     -    -   +            
 y   2  1     806 -0.010555              2( a" )   +-   +     -    -             +  
 y   2  1     926  0.013042              8( a' )   +-   +    +     -    -           
 y   2  1     931  0.011028              1( a' )   +-   +    +     -         -      
 y   2  1     935 -0.014573              5( a' )   +-   +    +     -         -      
 y   2  1     943 -0.015706              1( a" )   +-   +    +     -              - 
 y   2  1     944 -0.015996              2( a" )   +-   +    +     -              - 
 y   2  1     983  0.014132              2( a' )   +-   +         +-         -      
 y   2  1     989  0.018361              8( a' )   +-   +         +-         -      
 y   2  1     996  0.025853              3( a" )   +-   +         +-              - 
 y   2  1    1175 -0.018810              2( a' )   +-        +-   +-                
 y   2  1    1177  0.021993              4( a' )   +-        +-   +-                
 y   2  1    1349  0.013266              2( a" )   +-         -   +-             +  
 y   2  1    1515  0.013819              3( a' )   +-        +    +-         -      
 y   2  1    1526 -0.022969              2( a" )   +-        +    +-              - 
 x   2  1    2082 -0.013407    1( a' )   2( a' )   +-   +-         -                
 x   2  1    2089 -0.012502    2( a' )   5( a' )   +-   +-         -                
 x   2  1    3024 -0.010071    2( a' )   3( a' )   +-    -        +-                
 w   2  1    8286  0.029729    1( a' )   1( a' )   +-   +-   +                      
 w   2  1    8296 -0.032836    1( a' )   5( a' )   +-   +-   +                      
 w   2  1    8300  0.025378    5( a' )   5( a' )   +-   +-   +                      
 w   2  1    8306  0.010087    6( a' )   6( a' )   +-   +-   +                      
 w   2  1    8321  0.013871    8( a' )   8( a' )   +-   +-   +                      
 w   2  1    8366  0.013439    2( a" )   2( a" )   +-   +-   +                      
 w   2  1    8380  0.013206    1( a' )   2( a' )   +-   +-        +                 
 w   2  1    8387 -0.010388    3( a' )   4( a' )   +-   +-        +                 
 w   2  1    8390 -0.014249    2( a' )   5( a' )   +-   +-        +                 
 w   2  1    9162  0.010084    3( a' )   5( a' )   +-   +    +     -                
 w   2  1    9493 -0.018258    2( a' )   3( a' )   +-   +         +-                
 w   2  1    9497  0.019482    3( a' )   4( a' )   +-   +         +-                
 w   2  1    9571  0.013738    2( a" )   3( a" )   +-   +         +-                
 w   2  1    9574 -0.011921    2( a" )   4( a" )   +-   +         +-                
 w   2  1   11123 -0.021917    3( a' )   3( a' )   +-        +    +-                
 w   2  1   11138 -0.014712    6( a' )   6( a' )   +-        +    +-                
 w   2  1   11198 -0.018074    2( a" )   2( a" )   +-        +    +-                

 ci coefficient statistics:
           rq > 0.1                2
      0.1> rq > 0.01              98
     0.01> rq > 0.001            946
    0.001> rq > 0.0001          3289
   0.0001> rq > 0.00001         2200
  0.00001> rq > 0.000001         393
 0.000001> rq                   7042
           all                 13970
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        1505 2x:           0 4x:           0
All internal counts: zz :        5629 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================




LOOPCOUNT per task:
task #     1:        1431    task #     2:        1299    task #     3:        2479    task #     4:        1239
task #     5:        8526    task #     6:        9200    task #     7:        7175    task #     8:        4962
task #     9:       11867    task #    10:        2061    task #    11:        2061    task #    12:        1071
task #    13:        1071    task #    14:         535    task #    15:         537    task #    16:         537
task #    17:         865    task #    18:        2004    task #    19:         718    task #    20:         100
  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.000005992482     -0.000154001715
     2     2     -0.034305967861      0.877698982263
     3     3      0.000000487726     -0.000012863274
     4     4      0.938104877511    -24.063330413406
     5     5      0.000000083643     -0.000002488696
     6     6      0.090526370772     -2.322605923778
     7     7      0.000000445883     -0.000011098137
     8     8      0.048117441375     -1.228107084549
     9     9     -0.083170649525      2.128629740308
    10    10      0.000000179224     -0.000004287536
    11    11     -0.000000252933      0.000006587495
    12    12     -0.039231937061      0.997386440974
    13    13     -0.177859411297      4.568974116334
    14    14      0.033593557170     -0.868588085524
    15    15     -0.000000711016      0.000018329970
    16    16      0.000001044321     -0.000026290129
    17    17      0.005541032702     -0.140798783774
    18    18     -0.050524293638      1.297611821078
    19    19      0.000000460940     -0.000011488213
    20    20      0.000001309419     -0.000033575375
    21    21     -0.000000015098      0.000000297096
    22    22      0.005459300017     -0.138899729415
    23    23     -0.001068332330      0.028287877508
    24    24      0.000000002094     -0.000000074517
    25    25      0.000000290115     -0.000007287310
    26    26      0.092421351348     -2.378877083185
    27    27      0.000000397178     -0.000010165202
    28    28      0.044598797891     -1.146646720083
    29    29      0.000000304692     -0.000007632729
    30    30      0.000000001044     -0.000000048571
    31    31     -0.006533189775      0.171006732993
    32    32     -0.006318967819      0.166594200874
    33    33      0.000000172000     -0.000004294120
    34    34     -0.000000593095      0.000015134001
    35    35     -0.000000394931      0.000010373846
    36    36      0.045273980323     -1.172855545984
    37    37      0.000513120263     -0.009122491122
    38    38      0.000000257719     -0.000006477653
    39    39      0.000000577979     -0.000014548971
    40    40     -0.010077686674      0.260460715981
    41    41      0.027876357751     -0.717613867485
    42    42     -0.005566937223      0.142353444096
    43    43     -0.000000088340      0.000002209000
    44    44     -0.000000012525      0.000000308180
    45    45      0.012325820998     -0.317651928741
    46    46     -0.000000182187      0.000004852801
    47    47      0.017648789621     -0.458946961024
    48    48      0.000707253528     -0.021279769425
    49    49      0.000000105715     -0.000002842355
    50    50      0.000000515139     -0.000013226325
    51    51      0.018352950601     -0.473008907166
    52    52     -0.060267049963      1.552597078175
    53    53     -0.000000077657      0.000001786223
    54    54     -0.003060956064      0.080943833244
    55    55     -0.000000025440      0.000000612004
    56    56     -0.000000015002      0.000000389587
    57    57     -0.000523883800      0.009470696697
    58    58     -0.000000025959      0.000000706573
    59    59      0.013399950748     -0.348628209901
    60    60      0.052635912787     -1.359710227729
    61    61     -0.000000099248      0.000002576758
    62    62      0.000000365926     -0.000009438834
    63    63      0.000000039381     -0.000001013881
    64    64     -0.013511928647      0.348809993321
    65    65     -0.003059947299      0.078753279233
    66    66     -0.000000078453      0.000002018558
    67    67      0.000000028945     -0.000000822864
    68    68     -0.003842718136      0.098330981812
    69    69     -0.000715054233      0.017042877189
    70    70     -0.000000003584      0.000000118222
    71    71      0.000000002124     -0.000000008332
    72    72      0.000000009241     -0.000000270549
    73    73     -0.001849639756      0.048055173348
    74    74     -0.013084166286      0.339210600035
    75    75     -0.000000000799      0.000000008109
    76    76      0.028033731663     -0.724376512740
    77    77      0.000000074443     -0.000001810791
    78    78      0.000000338679     -0.000008547432
    79    79     -0.001482969765      0.039061169521
    80    80      0.008047152670     -0.207113600773
    81    81      0.000000509409     -0.000012718450
    82    82      0.000000420579     -0.000010574176
    83    83     -0.000000045905      0.000001181686
    84    84     -0.003197960125      0.082796880067
    85    85      0.011395376304     -0.294980248167
    86    86      0.000000056205     -0.000001457804
    87    87      0.031213166901     -0.805205681109
    88    88     -0.000000004203      0.000000122897
    89    89     -0.000000062791      0.000001611460
    90    90      0.043964597440     -1.131988897322
    91    91      0.035092916948     -0.896851173613
    92    92     -0.005645420591      0.145987948884
    93    93      0.000000100387     -0.000002548697
    94    94     -0.000000079498      0.000002070453
    95    95      0.005823588892     -0.149894311110
    96    96     -0.002668798206      0.068664825900
    97    97      0.000000002607     -0.000000113604
    98    98     -0.000000006086      0.000000145302
    99    99      0.000832730661     -0.020554303244
   100   100     -0.006115117590      0.157695243273
   101   101     -0.009923032236      0.256101587836
   102   102      0.000000001052     -0.000000053093
   103   103     -0.000000019488      0.000000486629
   104   104     -0.011445257060      0.295509875608
   105   105      0.000000043876     -0.000001086867
   106   106      0.007565933446     -0.195979735082
   107   107      0.002900730829     -0.074774181086
   108   108     -0.000000051537      0.000001320710
   109   109      0.000000051872     -0.000001430221
   110   110      0.000000074696     -0.000001958025
   111   111     -0.000559443634      0.014688621031
   112   112     -0.003891535972      0.100548911435
   113   113      0.000000080444     -0.000002101309
   114   114     -0.000000005541      0.000000134557
   115   115      0.002252256259     -0.058240615741

 number of reference csfs (nref) is   115.  root number (iroot) is  2.
 c0**2 =   0.96246699  c**2 (all zwalks) =   0.96246699

 pople ci energy extrapolation is computed with  7 correlated electrons.

 eref      =    -25.654790719807   "relaxed" cnot**2         =   0.962466990653
 eci       =    -25.716357174652   deltae = eci - eref       =  -0.061566454845
 eci+dv1   =    -25.718667948977   dv1 = (1-cnot**2)*deltae  =  -0.002310774325
 eci+dv2   =    -25.718758061485   dv2 = dv1 / cnot**2       =  -0.002400886833
 eci+dv3   =    -25.718855487366   dv3 = dv1 / (2*cnot**2-1) =  -0.002498312714
 eci+pople =    -25.718100682696   (  7e- scaled deltae )    =  -0.063309962889
 passed aftci ... 
 readint2: molcas,dalton2=                     0                     0
 files%faoints=aoints              
                       Size (real*8) of d2temp for two-external contributions       6312
 
                       Size (real*8) of d2temp for all-internal contributions        378
                       Size (real*8) of d2temp for one-external contributions       2562
                       Size (real*8) of d2temp for two-external contributions       6312
size_thrext:  lsym   l1    ksym   k1strt   k1       cnt3 
                1    1    1    1   13     1056
                1    1    2    1    6      540
                1    2    1    1   13     1056
                1    2    2    1    6      540
                1    3    1    1   13     1056
                1    3    2    1    6      540
                1    4    1    1   13     1056
                1    4    2    1    6      540
                1    5    1    1   13     1056
                1    5    2    1    6      540
                1    6    1    1   13     1056
                1    6    2    1    6      540
                2    7    2    1    6     1260
                       Size (real*8) of d2temp for three-external contributions      10836
                       Size (real*8) of d2temp for four-external contributions       7815
 enough memory for temporary d2 elements on vdisk ... 
location of d2temp files... fileloc(dd012)=       1
location of d2temp files... fileloc(d3)=       1
location of d2temp files... fileloc(d4)=       1
 files%dd012ext =  unit=  22  vdsk=   1  filestart=       1
 files%d3ext =     unit=  23  vdsk=   1  filestart=   11491
 files%d4ext =     unit=  24  vdsk=   1  filestart=   44259
            0xdiag    0ext      1ext      2ext      3ext      4ext
d2off                   767      1533      4597         1         1
d2rec                     1         4         9         1         1
recsize                 766       766       766     32768     32768
d2bufferlen=          32768
maxbl3=               32768
maxbl4=               32768
  allocated                  77027  DP for d2temp 
sifcfg setup: record length 4096 DP
# d1 elements per record  3272
# d2 elements per record  2730
  The MR-CISD density will be calculated.
 item #                     1 suffix=:.drt1.state1:
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method:  0 last record  0max overlap with ref# 98% root-following 0
 MR-CISD energy:   -25.75839368   -29.95493157
 residuum:     0.00000792
 deltae:     0.00000000

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.98436216     0.00000010     0.01991165    -0.00179068     0.02741551     0.01032762    -0.00549729     0.00000000
 ref:   2    -0.00000005    -0.98005693    -0.00254826    -0.03737574     0.09879588     0.05482641     0.00724776     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.98436216     0.00000010     0.01991165    -0.00179068     0.00000000     0.00000000     0.00000000     0.00000000
 ref:   2    -0.00000005    -0.98005693    -0.00254826    -0.03737574     0.00000000     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 computing final density
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=     692  DYX=     815  DYW=     670
   D0Z=     740  D0Y=    1405  D0X=     475  D0W=     310
  DDZI=     530 DDYI=     900 DDXI=     330 DDWI=     250
  DDZE=       0 DDYE=     210 DDXE=      90 DDWE=      70
================================================================================
adding (  1) 0.00to den1(    1)=   2.0000000
adding (  2) 0.00to den1(    3)=   1.9439388
adding (  3) 0.00to den1(    6)=   1.9093041
adding (  4) 0.00to den1(   10)=   0.9919413
adding (  5) 0.00to den1(   15)=   0.0652707
adding (  6) 0.00to den1(   21)=   0.0286216
adding (  7) 0.00to den1(  172)=   0.0170523
--------------------------------------------------------------------------------
  2e-density  for root #    1
--------------------------------------------------------------------------------
 call to prpd23 iwx=1
 call to prpd23 iwx=2
 starting prpd24 .... 
d2temp:fourex offset=     0 N=  4095 init,final=   T   Fmode=   1
d2temp:fourex offset=  4095 N=  3720 init,final=   F   Fmode=   1
d2temp:fourex offset=  7816 N=     0 init,final=   F   Tmode=   1
 starting prpd24 .... 
d2temp:fourex offset=     0 N=  4095 init,final=   T   Fmode=   1
d2temp:fourex offset=  4095 N=  3720 init,final=   F   Fmode=   1
d2temp:fourex offset=  7816 N=     0 init,final=   F   Tmode=   1
================================================================================
   DYZ=     692  DYX=   10444  DYW=    8199
   D0Z=     740  D0Y=   10577  D0X=    2870  D0W=    1774
  DDZI=    1505 DDYI=    2415 DDXI=     780 DDWI=     580
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
d2il(     1)=  2.00000000 rmuval=  0.00000000den1=  2.00000000 fact=  0.00000000
d2il(     5)=  1.90889283 rmuval=  0.00000000den1=  1.94393882 fact=  0.00000000
d2il(    11)=  1.85494438 rmuval=  0.00000000den1=  1.90930410 fact=  0.00000000
d2il(    19)=  0.02581807 rmuval=  0.00000000den1=  0.99194131 fact=  0.00000000
d2il(    29)=  0.02193435 rmuval=  0.00000000den1=  0.06527073 fact=  0.00000000
d2il(    41)=  0.00836934 rmuval=  0.00000000den1=  0.02862156 fact=  0.00000000
d2il(    55)=  0.01223610 rmuval=  0.00000000den1=  0.01705228 fact=  0.00000000
 dump_thrext: d2(1:                 65536 ) maxbl3=                 32768
  entering dump_fourext .... 
d2temp:fourex offset=     1 N= 32768 init,final=   T   Fmode=   2
Trace of MO density:     7.000000
    7  correlated and     0  frozen core electrons

Natural orbital populations,block 1
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     1.94815290     1.92555600     0.99320311     0.04978761     0.02557522     0.00868115     0.00686926
              MO     9       MO    10       MO    11       MO    12       MO    13       MO    14       MO    15       MO    16
  occ(*)=     0.00464898     0.00398180     0.00251627     0.00112824     0.00098186     0.00060989     0.00047954     0.00040907
              MO    17       MO    18       MO
  occ(*)=     0.00010714     0.00005286

Natural orbital populations,block 2
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO
  occ(*)=     0.01795175     0.00504130     0.00316130     0.00066763     0.00034799     0.00008912


 total number of electrons =    7.0000000000

 test slabel:                     2
  a'  a" 


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        a'  partial gross atomic populations
   ao class       1a'        2a'        3a'        4a'        5a'        6a' 
     2_ s       1.999928   0.698363   0.000000   0.533086   0.000000   0.002868
     2_ p       0.000004   0.240149   0.739433   0.394406   0.029121   0.011228
     2_ d      -0.000001   0.007110   0.035966   0.003399   0.001344   0.000333
     2_ s       0.000086   0.492103   0.570280   0.030051   0.009627   0.005442
     2_ p      -0.000052   0.009162   0.004800   0.001104   0.000034   0.000131
     2_ s       0.000086   0.492103   0.570275   0.030053   0.009627   0.005442
     2_ p      -0.000052   0.009162   0.004801   0.001104   0.000034   0.000131
 
   ao class       7a'        8a'        9a'       10a'       11a'       12a' 
     2_ s      -0.001164   0.000000  -0.000193   0.000000   0.002641   0.000008
     2_ p       0.010720   0.007273  -0.000319  -0.000119  -0.000397   0.000002
     2_ d      -0.000187  -0.000017   0.000524   0.000261  -0.000033   0.000956
     2_ s      -0.000391  -0.000228   0.002326   0.001972   0.000148  -0.000001
     2_ p       0.000047   0.000034  -0.000008  -0.000052   0.000005   0.000082
     2_ s      -0.000391  -0.000228   0.002326   0.001972   0.000148  -0.000001
     2_ p       0.000047   0.000034  -0.000008  -0.000052   0.000005   0.000082
 
   ao class      13a'       14a'       15a'       16a'       17a'       18a' 
     2_ s       0.000084   0.000000   0.000000   0.000000   0.000003   0.000000
     2_ p       0.000017  -0.000001   0.000001   0.000000   0.000001   0.000000
     2_ d       0.000747   0.000495   0.000131   0.000026   0.000002   0.000001
     2_ s       0.000004   0.000034   0.000001   0.000000  -0.000002   0.000000
     2_ p       0.000063   0.000024   0.000172   0.000192   0.000052   0.000026
     2_ s       0.000004   0.000034   0.000001   0.000000  -0.000002   0.000000
     2_ p       0.000063   0.000024   0.000172   0.000192   0.000052   0.000026

                        a"  partial gross atomic populations
   ao class       1a"        2a"        3a"        4a"        5a"        6a" 
     2_ p       0.017868   0.005050  -0.000002   0.000000   0.000000   0.000015
     2_ d       0.000009   0.000000   0.002786   0.000629   0.000020   0.000057
     2_ p       0.000038  -0.000004   0.000189   0.000019   0.000164   0.000030
     2_ p       0.000038  -0.000004   0.000189   0.000019   0.000164   0.000353


                        gross atomic populations
     ao            2_         2_         2_
      s         3.235626   1.111452   1.111449
      p         1.454453   0.016252   0.016575
      d         0.054557   0.000000   0.000000
    total       4.744636   1.127704   1.128025
 

 Total number of electrons:    7.00036488

 item #                     2 suffix=:.drt1.state2:
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method:  0 last record  0max overlap with ref# 98% root-following 0
 MR-CISD energy:   -25.75839368   -29.95493157
 residuum:     0.00000792
 deltae:     0.00000000
================================================================================
  Reading record                      2  of civout
 INFO:ref#  2vector#  2 method:  0 last record  1max overlap with ref# 98% root-following 0
 MR-CISD energy:   -25.71635717   -29.91289507
 residuum:     0.00000588
 deltae:     0.00000000
 apxde:     0.00000000

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.98436216     0.00000010     0.01991165    -0.00179068     0.02741551     0.01032762    -0.00549729     0.00000000
 ref:   2    -0.00000005    -0.98005693    -0.00254826    -0.03737574     0.09879588     0.05482641     0.00724776     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.98436216     0.00000010     0.01991165    -0.00179068     0.00000000     0.00000000     0.00000000     0.00000000
 ref:   2    -0.00000005    -0.98005693    -0.00254826    -0.03737574     0.00000000     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 computing final density
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    2
--------------------------------------------------------------------------------
================================================================================
   DYZ=     692  DYX=     815  DYW=     670
   D0Z=     740  D0Y=    1405  D0X=     475  D0W=     310
  DDZI=     530 DDYI=     900 DDXI=     330 DDWI=     250
  DDZE=       0 DDYE=     210 DDXE=      90 DDWE=      70
================================================================================
adding (  1) 0.00to den1(    1)=   2.0000000
adding (  2) 0.00to den1(    3)=   1.9430836
adding (  3) 0.00to den1(    6)=   0.9977646
adding (  4) 0.00to den1(   10)=   1.8502791
adding (  5) 0.00to den1(   15)=   0.0423958
adding (  6) 0.00to den1(   21)=   0.0329577
adding (  7) 0.00to den1(  172)=   0.0837481
--------------------------------------------------------------------------------
  2e-density  for root #    2
--------------------------------------------------------------------------------
 call to prpd23 iwx=1
 call to prpd23 iwx=2
 starting prpd24 .... 
d2temp:fourex offset=     0 N=  4095 init,final=   T   Fmode=   1
d2temp:fourex offset=  4095 N=  3720 init,final=   F   Fmode=   1
d2temp:fourex offset=  7816 N=     0 init,final=   F   Tmode=   1
 starting prpd24 .... 
d2temp:fourex offset=     0 N=  4095 init,final=   T   Fmode=   1
d2temp:fourex offset=  4095 N=  3720 init,final=   F   Fmode=   1
d2temp:fourex offset=  7816 N=     0 init,final=   F   Tmode=   1
================================================================================
   DYZ=     692  DYX=   10444  DYW=    8199
   D0Z=     740  D0Y=   10577  D0X=    2870  D0W=    1774
  DDZI=    1505 DDYI=    2415 DDXI=     780 DDWI=     580
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
d2il(     1)=  2.00000000 rmuval=  0.00000000den1=  2.00000000 fact=  0.00000000
d2il(     5)=  1.90849499 rmuval=  0.00000000den1=  1.94308363 fact=  0.00000000
d2il(    11)=  0.02956401 rmuval=  0.00000000den1=  0.99776459 fact=  0.00000000
d2il(    19)=  1.80771359 rmuval=  0.00000000den1=  1.85027906 fact=  0.00000000
d2il(    29)=  0.01667404 rmuval=  0.00000000den1=  0.04239577 fact=  0.00000000
d2il(    41)=  0.00900240 rmuval=  0.00000000den1=  0.03295766 fact=  0.00000000
d2il(    55)=  0.07712928 rmuval=  0.00000000den1=  0.08374815 fact=  0.00000000
 dump_thrext: d2(1:                 65536 ) maxbl3=                 32768
  entering dump_fourext .... 
d2temp:fourex offset=     1 N= 32768 init,final=   T   Fmode=   2
Trace of MO density:     7.000000
    7  correlated and     0  frozen core electrons

Natural orbital populations,block 1
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     2.00000000     1.94556296     1.85766518     0.99900860     0.04214985     0.02723262     0.00977238     0.00713678
              MO     9       MO    10       MO    11       MO    12       MO    13       MO    14       MO    15       MO    16
  occ(*)=     0.00497381     0.00464220     0.00319747     0.00125061     0.00091947     0.00080167     0.00059769     0.00036820
              MO    17       MO    18       MO
  occ(*)=     0.00013302     0.00011213

Natural orbital populations,block 2
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO
  occ(*)=     0.08598505     0.00428096     0.00264836     0.00073817     0.00058351     0.00023930


 total number of electrons =    7.0000000000

 test slabel:                     2
  a'  a" 


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        a'  partial gross atomic populations
   ao class       1a'        2a'        3a'        4a'        5a'        6a' 
     2_ s       1.999928   0.697435   0.000000   0.536202   0.000000   0.003053
     2_ p       0.000004   0.239830   0.713363   0.396712   0.024654   0.011956
     2_ d      -0.000001   0.007101   0.034698   0.003419   0.001138   0.000354
     2_ s       0.000086   0.491449   0.550174   0.030226   0.008150   0.005795
     2_ p      -0.000052   0.009150   0.004631   0.001110   0.000029   0.000140
     2_ s       0.000086   0.491449   0.550169   0.030229   0.008150   0.005795
     2_ p      -0.000052   0.009150   0.004631   0.001110   0.000029   0.000140
 
   ao class       7a'        8a'        9a'       10a'       11a'       12a' 
     2_ s      -0.001310   0.000000  -0.000206   0.000000   0.003356   0.000009
     2_ p       0.012068   0.007556  -0.000341  -0.000138  -0.000504   0.000002
     2_ d      -0.000211  -0.000017   0.000560   0.000304  -0.000042   0.001060
     2_ s      -0.000440  -0.000237   0.002489   0.002299   0.000188  -0.000001
     2_ p       0.000053   0.000035  -0.000008  -0.000061   0.000006   0.000091
     2_ s      -0.000440  -0.000237   0.002489   0.002299   0.000188  -0.000001
     2_ p       0.000053   0.000035  -0.000008  -0.000061   0.000006   0.000091
 
   ao class      13a'       14a'       15a'       16a'       17a'       18a' 
     2_ s       0.000079   0.000000   0.000001   0.000000   0.000003   0.000000
     2_ p       0.000016  -0.000001   0.000002   0.000000   0.000001   0.000000
     2_ d       0.000699   0.000651   0.000163   0.000023   0.000003   0.000002
     2_ s       0.000003   0.000045   0.000002   0.000000  -0.000002  -0.000001
     2_ p       0.000059   0.000031   0.000215   0.000173   0.000065   0.000056
     2_ s       0.000003   0.000045   0.000002   0.000000  -0.000002  -0.000001
     2_ p       0.000059   0.000031   0.000215   0.000173   0.000065   0.000056

                        a"  partial gross atomic populations
   ao class       1a"        2a"        3a"        4a"        5a"        6a" 
     2_ p       0.085582   0.004288  -0.000002   0.000000   0.000000   0.000039
     2_ d       0.000042   0.000000   0.002334   0.000695   0.000034   0.000153
     2_ p       0.000181  -0.000004   0.000158   0.000021   0.000275   0.000079
     2_ p       0.000181  -0.000004   0.000158   0.000021   0.000275   0.000947


                        gross atomic populations
     ao            2_         2_         2_
      s         3.238551   1.090224   1.090221
      p         1.495088   0.016433   0.017301
      d         0.053162   0.000000   0.000000
    total       4.786801   1.106657   1.107522
 

 Total number of electrons:    7.00097981

 item #                     3 suffix=:.trd1to2:
 computing final density
 =========== Executing IN-CORE method ==========
1e-transition density (   1,   2)
--------------------------------------------------------------------------------
  1e-transition density (root #    1 -> root #   2)
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
  1e-transition density (root #    1 -> root #   2)
--------------------------------------------------------------------------------
================================================================================
   DYZ=     692  DYX=     815  DYW=     670
   D0Z=     740  D0Y=    1405  D0X=     475  D0W=     310
  DDZI=     530 DDYI=     900 DDXI=     330 DDWI=     250
  DDZE=       0 DDYE=     210 DDXE=      90 DDWE=      70
================================================================================
adding (  1) 0.00to den1(    1)=   0.0000000
adding (  2) 0.00to den1(    3)=  -0.0000003
adding (  3) 0.00to den1(    6)=   0.0000115
adding (  4) 0.00to den1(   10)=  -0.0000122
adding (  5) 0.00to den1(   15)=   0.0000005
adding (  6) 0.00to den1(   21)=   0.0000002
adding (  7) 0.00to den1(  172)=   0.0000001
2e-transition density (   1,   2)
--------------------------------------------------------------------------------
  2e-transition density (root #    1 -> root #   2)
--------------------------------------------------------------------------------
 call to prpd23 iwx=1
 call to prpd23 iwx=2
 starting prpd24 .... 
d2temp:fourex offset=     0 N=  4095 init,final=   T   Fmode=   1
d2temp:fourex offset=  4095 N=  3720 init,final=   F   Fmode=   1
d2temp:fourex offset=  7816 N=     0 init,final=   F   Tmode=   1
 starting prpd24 .... 
d2temp:fourex offset=     0 N=  4095 init,final=   T   Fmode=   1
d2temp:fourex offset=  4095 N=  3720 init,final=   F   Fmode=   1
d2temp:fourex offset=  7816 N=     0 init,final=   F   Tmode=   1
================================================================================
   DYZ=     692  DYX=   10444  DYW=    8199
   D0Z=     740  D0Y=   10577  D0X=    2870  D0W=    1774
  DDZI=    1505 DDYI=    2415 DDXI=     780 DDWI=     580
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================
d2il(     1)=  0.00000000 rmuval=  0.00000000den1=  0.00000000 fact=  0.00000000
d2il(     5)= -0.00000051 rmuval=  0.00000000den1= -0.00000032 fact=  0.00000000
d2il(    11)=  0.00002361 rmuval=  0.00000000den1=  0.00001153 fact=  0.00000000
d2il(    19)= -0.00002455 rmuval=  0.00000000den1= -0.00001217 fact=  0.00000000
d2il(    29)=  0.00000017 rmuval=  0.00000000den1=  0.00000050 fact=  0.00000000
d2il(    41)=  0.00000006 rmuval=  0.00000000den1=  0.00000020 fact=  0.00000000
d2il(    55)=  0.00000005 rmuval=  0.00000000den1=  0.00000007 fact=  0.00000000
 dump_thrext: d2(1:                 65536 ) maxbl3=                 32768
  entering dump_fourext .... 
d2temp:fourex offset=     1 N= 32768 init,final=   T   Fmode=   2
 maximum diagonal element=  5.766981201473301E-006
========================GLOBAL TIMING PER NODE========================
   process    vectrn  integral   segment    diagon     dmain    davidc   finalvw   driver  
--------------------------------------------------------------------------------
   #   1         0.0       0.0       0.0       2.5       2.5       0.0       0.0       2.8
--------------------------------------------------------------------------------
       1         0.0       0.0       0.0       2.5       2.5       0.0       0.0       2.8
 DA ...
