 total ao core energy =    4.196537893
 MCSCF calculation performed for  2 DRTs.

 DRT  first state   no.of aver. states   weights
  1   ground state          2             0.333 0.333
  2   ground state          1             0.333

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a' 
 Total number of electrons:     7
 Spin multiplicity:            2
 Number of active orbitals:    6
 Number of active electrons:   5
 Total number of CSFs:       115

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a" 
 Total number of electrons:     7
 Spin multiplicity:            2
 Number of active orbitals:    6
 Number of active electrons:   5
 Total number of CSFs:        95

 Number of active-double rotations:         5
 Number of active-active rotations:         0
 Number of double-virtual rotations:       12
 Number of active-virtual rotations:       65
 
 iter     emc (average)    demc       wnorm      knorm      apxde  qcoupl
    1    -25.6509298801  2.565E+01  9.094E-02  7.101E-02  1.925E-03  F   *not conv.*     
    2    -25.6529646217  2.035E-03  6.294E-03  2.520E-02  2.828E-05  F   *not conv.*     
    3    -25.6530012152  3.659E-05  1.192E-03  9.238E-03  3.101E-06  F   *not conv.*     
    4    -25.6530055225  4.307E-06  3.984E-04  3.690E-03  4.887E-07  F   *not conv.*     
    5    -25.6530062100  6.875E-07  1.586E-04  2.546E-03  1.388E-07  T   *not conv.*     
    6    -25.6530063490  1.390E-07  6.337E-07  4.226E-06  4.585E-13  T   *not conv.*     

 final mcscf convergence values:
    7    -25.6530063490  4.654E-13  6.048E-08  1.307E-08  3.456E-16  T   *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 wt 0.333 total energy=      -25.704269376, rel. (eV)=   0.000000
   DRT #1 state # 2 wt 0.333 total energy=      -25.655972347, rel. (eV)=   1.314230
   DRT #2 state # 1 wt 0.333 total energy=      -25.598777324, rel. (eV)=   2.870586
   ------------------------------------------------------------


