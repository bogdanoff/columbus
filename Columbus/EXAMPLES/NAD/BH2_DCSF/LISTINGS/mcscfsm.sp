 total ao core energy =    4.196537893
 MCSCF calculation performed for  2 DRTs.

 DRT  first state   no.of aver. states   weights
  1   ground state          2             0.333 0.333
  2   ground state          1             0.333

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a' 
 Total number of electrons:     7
 Spin multiplicity:            2
 Number of active orbitals:    6
 Number of active electrons:   5
 Total number of CSFs:       115

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a" 
 Total number of electrons:     7
 Spin multiplicity:            2
 Number of active orbitals:    6
 Number of active electrons:   5
 Total number of CSFs:        95

 Number of active-double rotations:      5
 Number of active-active rotations:      0
 Number of double-virtual rotations:    12
 Number of active-virtual rotations:    65

 iter=    1 emc=  -25.6509298801 demc= 2.5651E+01 wnorm= 9.0941E-02 knorm= 7.1006E-02 apxde= 1.9254E-03    *not converged* 
 iter=    2 emc=  -25.6529646217 demc= 2.0347E-03 wnorm= 6.2938E-03 knorm= 2.5204E-02 apxde= 2.8275E-05    *not converged* 
 iter=    3 emc=  -25.6530012152 demc= 3.6593E-05 wnorm= 1.1919E-03 knorm= 9.2381E-03 apxde= 3.1010E-06    *not converged* 
 iter=    4 emc=  -25.6530055225 demc= 4.3073E-06 wnorm= 3.9837E-04 knorm= 3.6897E-03 apxde= 4.8870E-07    *not converged* 
 iter=    5 emc=  -25.6530062100 demc= 6.8754E-07 wnorm= 1.5855E-04 knorm= 2.5465E-03 apxde= 1.3882E-07    *not converged* 
 iter=    6 emc=  -25.6530063490 demc= 1.3898E-07 wnorm= 6.3432E-07 knorm= 4.2266E-06 apxde= 4.5866E-13    *not converged* 

 final mcscf convergence values:
 iter=    7 emc=  -25.6530063490 demc= 4.7606E-13 wnorm= 6.0518E-08 knorm= 1.3055E-08 apxde= 6.3322E-16    *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 weight 0.333333 total energy=  -25.704269376
   DRT #1 state # 2 weight 0.333333 total energy=  -25.655972347
   DRT #2 state # 1 weight 0.333333 total energy=  -25.598777324
   ------------------------------------------------------------


