

     ******************************************
     **    PROGRAM:              MCPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 original author: Daniel Robertson, FSU
 later revisions: Ron Shepard, ANL;
                  Michal Dallos, University Vienna



 This Version of Program mcpc is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



   ******  File header section  ******

 Headers form the restart file:
    Hermit Integral Program : SIFS version  hochtor2        Thu Jun  5 15:07:51 2003
     title                                                                          
     title                                                                          


   ******  DRT info section  ******

 Informations for the DRT no.  1
 Header form the DRT file: 
     title                                                                          
 Molecular symmetry group:   sym1
 Total number of electrons:    7
 Spin multiplicity:            2
 Number of active orbitals:    6
 Number of active electrons:   5
 Total number of CSFs:       115

   ***  Informations from the DRT number:   1


 Symmetry orbital summary:
 Symm.blocks:         1     2
 Symm.labels:         a'    a" 

 List of doubly occupied orbitals:
  1 a' 

 List of active orbitals:
  2 a'   3 a'   4 a'   5 a'   6 a'   1 a" 

 Informations for the DRT no.  2
 Header form the DRT file: 
     title                                                                          
 Molecular symmetry group:    a" 
 Total number of electrons:    7
 Spin multiplicity:            2
 Number of active orbitals:    6
 Number of active electrons:   5
 Total number of CSFs:        95

   ***  Informations from the DRT number:   2


 Symmetry orbital summary:
 Symm.blocks:         1     2
 Symm.labels:         a'    a" 

 List of doubly occupied orbitals:
  1 a' 

 List of active orbitals:
  2 a'   3 a'   4 a'   5 a'   6 a'   1 a" 


   ******  MCSCF convergence information:  ******

 MCSCF convergence criteria were satisfied.

 mcscf energy=   -25.6530063490    nuclear repulsion=     4.1965378929
 demc=             0.0000000000    wnorm=                 0.0000000605
 knorm=            0.0000000131    apxde=                 0.0000000000


 MCSCF calculation performmed for   2 symmetries.

 State averaging:
 No,  ssym, navst, wavst
  1    a'     2   0.3333 0.3333
  2    a"     1   0.3333

 Input the DRT No of interest: [  1]:
In the DRT No.: 1 there are  2 states.

 Which one to take? [  1]:
 The CSFs for the state No  2 of the symmetry  a'  will be printed
 according to the following print options :

 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 0) end.

 input menu number [  0]: csfs will be printed based on coefficient magnitudes.

 input the coefficient threshold (end with 0.) [ 0.0000]:
 List of active orbitals:
  2 a'   3 a'   4 a'   5 a'   6 a'   1 a" 

   csf       coeff       coeff**2    step(*)
  -----  ------------  ------------  ------------
      4 -0.9480927998  0.8988799570  313000
     13  0.2040071253  0.0416189072  310003
     26 -0.1065197226  0.0113464513  132100
      6 -0.0938316422  0.0088043771  312010
      9  0.0773169301  0.0059779077  310300
     52  0.0712727498  0.0050798049  112003
     60 -0.0614628768  0.0037776852  103120
     36 -0.0597605796  0.0035713269  123010
     18  0.0526970499  0.0027769791  301120
     14 -0.0500284049  0.0025028413  303100
     28 -0.0489731863  0.0023983730  131200
     90 -0.0481573083  0.0023191263  013030
      8 -0.0358899256  0.0012880868  311020
     87 -0.0353964831  0.0012529110  013300
     91 -0.0327503557  0.0010725858  013003
     76 -0.0318907535  0.0010170202  033100
     12  0.0315603558  0.0009960561  310030
     41 -0.0308643079  0.0009526055  121003
     47 -0.0269573292  0.0007266976  113020
      2  0.0248282298  0.0006164410  330100
     51 -0.0201717161  0.0004068981  112030
     59 -0.0179691419  0.0003228901  103210
     74  0.0176982797  0.0003132291  100123
     45 -0.0162337136  0.0002635335  120013
     64  0.0161126322  0.0002596169  102130
     85 -0.0159885124  0.0002556325  030103
    104  0.0139709576  0.0001951877  010033
    101  0.0118179265  0.0001396634  010303
     40  0.0116091491  0.0001347723  121030
    106 -0.0102146206  0.0001043385  003130
     32  0.0101771421  0.0001035742  130120
     31  0.0090153081  0.0000812758  130210
     80 -0.0085167832  0.0000725356  031120
     92  0.0070594746  0.0000498362  012310
     95 -0.0065033656  0.0000422938  012013
    100  0.0064549780  0.0000416667  010330
     22 -0.0060090469  0.0000361086  300130
     42  0.0053080823  0.0000281757  120310
     54  0.0052386017  0.0000274429  110320
     17 -0.0049409604  0.0000244131  301210
    112  0.0046352737  0.0000214858  001123
     57 -0.0037943341  0.0000143970  110023
     84  0.0037761169  0.0000142591  030130
     68  0.0037138826  0.0000137929  101230
    107 -0.0031631531  0.0000100055  003103
    115 -0.0030751679  0.0000094567  000133
     96  0.0028668626  0.0000082189  011320
     73  0.0026704924  0.0000071315  100213
     65  0.0025290426  0.0000063961  102103
     37  0.0024823101  0.0000061619  121300
     48 -0.0021442121  0.0000045976  112300
     79  0.0015433229  0.0000023818  031210

 input the coefficient threshold (end with 0.) [ 0.0000]:
 1) print csf info by sorted index number.
 2) print csf info by contribution threshold.
 3) print csf info by csf number.
 4) set additional print options.
 5) print the entire sorted csf vector.
 6) print the entire csf vector.
 7) print the mcscf molecular orbitals.
 8) print the mcscf natural orbitals and occupation numbers.
 0) end.

 input menu number [  0]: