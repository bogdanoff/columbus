1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      09/20/00    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

================================================================================
four external integ    0.25 MB location: local disk    
three external inte    0.25 MB location: local disk    
four external integ    0.25 MB location: local disk    
three external inte    0.25 MB location: local disk    
diagonal integrals     0.12 MB location: local disk    
off-diagonal integr    0.16 MB location: local disk    
 nsubmx= 16 lenci= 13970
global arrays:       461010   (    3.52 MB)
vdisk:                    0   (    0.00 MB)
drt:                 603057   (    2.30 MB)
================================================================================
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core            2999999 DP per process
 CIUDG version 5.9.3 (05-Dec-2002)
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 0,
  GSET = 0,
   DAVCOR =10,
  NCOREL = 7
  NROOT = 2
  IVMODE = 3
  NBKITR = 1
  NVBKMN = 2
  NVBKMX = 7
  RTOLBK = 1e-3,1e-3,
  NITER = 400
  NVCIMN = 4
  NVCIMX = 16
  RTOLCI = 1e-5,1e-5,
  IDEN  = 1
  CSFPRN = 10,
 /&end
 ------------------------------------------------------------------------

 ** list of control variables **
 nrfitr =   30      nvrfmx =    7      nvrfmn =    4
 lvlprt =    0      nroot  =    2      noldv  =    0      noldhv =    0
 nunitv =    2      ntype  =    0      nbkitr =    1      niter  =  400
 ivmode =    3      vout   =    0      istrt  =    0      iortls =    0
 nvbkmx =    7      ibktv  =   -1      ibkthv =   -1      frcsub =    0
 nvcimx =    7      icitv  =   -1      icithv =   -1      maxseg =    4
 iden   =    1      itran  =    0      froot  =    0      rtmode =    0
 ftcalc =    1      lrtshift=1.0000    ncouple=    1      skipso  =   F
 ncorel =    7      csfprn  =  10      ctol   = 1.00E-02  davcor  =  10


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-03    1.000E-05
    2        1.000E-03    1.000E-05

 units and filenames:
    1: ( 6)    ciudgls                                                     
    2: ( 5)    ciudgin                                                     
    3: ( 7)    ciudgsm                                                     
    4: ( 4)    cihdiag                                                     
    5: ( 3)    cihpseu                                                     
    6: (10)    cihvfl                                                      
    7: (11)    civfl                                                       
    8: (12)    diagint                                                     
    9: (13)    ofdgint                                                     
   10: (17)    cidrtfl                                                     
   11: (20)    ciftdfl                                                     
   12: (21)    ciftofl                                                     
   13: (24)    ciftifl                                                     
   14: ( 8)    ciflind                                                     
   15: (15)    civin                                                       
   16: (16)    civout                                                      
   18: (18)    d1fl                                                        
   20: (53)    cifvfl                                                      
   21: (25)    ciscr4                                                      
   22: (41)    ciftotd                                                     
   23: (42)    flacpfd                                                     
   26: (26)    ciscr5                                                      
   27: (47)    restart                                                     
   28: (48)    civout                                                      
   29: (49)    cirefv                                                      
   31: (31)    fil4w                                                       
   32: (32)    fil4x                                                       
   33: (33)    fil3w                                                       
   34: (34)    fil3x                                                       
   36: (36)    aoints                                                      
   37: (37)    aoints2                                                     
   38: (38)    drtfil                                                      
   39: (50)    cisrtif                                                     
   42: (50)    mocoef                                                      
   45: (52)    nocoef_ci                                                   
   46: (53)    civfl_restart                                               
 ------------------------------------------------------------------------

 workspace allocation information: lcore=   2999999 mem1=1074180104 ifirst=-268379754

 integral file titles:
 Hermit Integral Program : SIFS version  hochtor2        Thu Jun  5 15:07:51 2003
  cidrt_title DRT#1                                                              
  title                                                                          
 mofmt: formatted orbitals label=morbl   hochtor2        Thu Jun  5 15:07:52 2003
 SIFS file created by program tran.      hochtor2        Thu Jun  5 15:07:52 2003

 core energy values from the integral file:
 energy( 1)=  4.196537892870E+00, ietype=   -1,    core energy of type: Nuc.Rep.

 total core repulsion energy =  4.196537892870E+00

 drt header information:
  cidrt_title DRT#1                                                              
 spnorb, spnodd, lxyzir,hmult F F 0 0 0 0
 nmot  =    24 niot  =     7 nfct  =     0 nfvt  =     0
 nrow  =    47 nsym  =     2 ssym  =     1 lenbuf=  1600
 nwalk,xbar:        580      210      210       90       70
 nvalwt,nvalw:      485      115      210       90       70
 ncsft:           13970
 total number of valid internal walks:     485
 nvalz,nvaly,nvalx,nvalw =      115     210      90      70

 cisrt info file parameters:
 file number  12 blocksize   4095
 mxbld   4095
 nd4ext,nd2ext,nd0ext   306   238    56
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int     7536    10836     7218     2562      378        0        0        0
 minbl4,minbl3,maxbl2   276   276   279
 maxbuf 32767
 number of external orbitals per symmetry block:  12   5
 nmsym   2 number of internal orbitals   7

 formula file title:
 Hermit Integral Program : SIFS version  hochtor2        Thu Jun  5 15:07:51 2003
  cidrt_title DRT#1                                                              
  title                                                                          
 mofmt: formatted orbitals label=morbl   hochtor2        Thu Jun  5 15:07:52 2003
 SIFS file created by program tran.      hochtor2        Thu Jun  5 15:07:52 2003
  cidrt_title DRT#1                                                              
 file nmb.  24 block size     0
 pthz,pthy,pthx,pthw:   210   210    90    70 total internal walks:     580
 maxlp3,n3xtlp,n2xtlp,n1xtlp,n0xtlp,n2lp,n1lp,n0lp     0     0     0     0
 orbsym(*)= 1 1 1 1 1 1 2

 setref:      115 references kept,
               95 references were marked as invalid, out of
              210 total.
 limcnvrt: found 115 valid internal walksout of  210
  walks (skipping trailing invalids)
  ... adding  115 segmentation marks segtype= 1
 limcnvrt: found 210 valid internal walksout of  210
  walks (skipping trailing invalids)
  ... adding  210 segmentation marks segtype= 2
 limcnvrt: found 90 valid internal walksout of  90
  walks (skipping trailing invalids)
  ... adding  90 segmentation marks segtype= 3
 limcnvrt: found 70 valid internal walksout of  70
  walks (skipping trailing invalids)
  ... adding  70 segmentation marks segtype= 4

 number of external paths / symmetry
 vertex x      76      60
 vertex w      93      60



                   segmentation summary for type diagonal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1         115|       115|         0|       115|         0|         1|
 -------------------------------------------------------------------------------
  Y 2         210|      1890|       115|       210|       115|         2|
 -------------------------------------------------------------------------------
  X 3          90|      6280|      2005|        90|       325|         3|
 -------------------------------------------------------------------------------
  W 4          70|      5685|      8285|        70|       415|         4|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>     13970


 297 dimension of the ci-matrix ->>>     13970


 297 dimension of the ci-matrix ->>>         7

                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      90     115       6280        115      90     115
     2  4   1    25      two-ext wz   2X  4 1      70     115       5685        115      70     115
     3  4   3    26      two-ext wx   2X  4 3      70      90       5685       6280      70      90
     4  2   1    11      one-ext yz   1X  2 1     210     115       1890        115     210     115
     5  3   2    15      1ex3ex  yx   3X  3 2      90     210       6280       1890      90     210
     6  4   2    16      1ex3ex  yw   3X  4 2      70     210       5685       1890      70     210
     7  1   1     1      allint zz    OX  1 1     115     115        115        115     115     115
     8  2   2     5      0ex2ex yy    OX  2 2     210     210       1890       1890     210     210
     9  3   3     6      0ex2ex xx    OX  3 3      90      90       6280       6280      90      90
    10  4   4     7      0ex2ex ww    OX  4 4      70      70       5685       5685      70      70
    11  1   1    75      dg-024ext z  DG  1 1     115     115        115        115     115     115
    12  2   2    45      4exdg024 y   DG  2 2     210     210       1890       1890     210     210
    13  3   3    46      4exdg024 x   DG  3 3      90      90       6280       6280      90      90
    14  4   4    47      4exdg024 w   DG  4 4      70      70       5685       5685      70      70
----------------------------------------------------------------------------------------------------
 DIAGEL COUNTS: DG0X,DG2X,DG4X= 5280 1480 370
 diagonal elements written to file   4

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      2 vectors will be written to unit 11 beginning with logical record   1

            2 vectors will be created
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      1505 2x:         0 4x:         0
All internal counts: zz :      5629 yy:         0 xx:         0 ww:         0
One-external counts: yz :         0 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         0    task #   2:         0    task #   3:         0    task #   4:         0
task #   5:         0    task #   6:         0    task #   7:      4962    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:       865    task #  12:         0
task #  13:         0    task #  14:         0    task #
 reference space has dimension     115

    root           eigenvalues
    ----           ------------
       1         -25.7042693760
       2         -25.6559723467

 strefv generated    2 initial ci vector(s).
    ---------end of vector generation---------


         vector  1 from unit 11 written to unit 49 filename cirefv                                                      

         vector  2 from unit 11 written to unit 49 filename cirefv                                                      

 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=   115)...
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:             13970
 number of initial trial vectors:                         2
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               7
 number of roots to converge:                             2
 number of iterations:                                    1
 residual norm convergence criteria:               0.001000  0.001000

          starting bk iteration   1

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      5280 2x:      1480 4x:       370
All internal counts: zz :      5629 yy:         0 xx:         0 ww:         0
One-external counts: yz :      9682 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:       818 wz:       810 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:       715    task #   2:       649    task #   3:         0    task #   4:      8526
task #   5:         0    task #   6:         0    task #   7:      4962    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:       865    task #  12:       549
task #  13:       224    task #  14:       170    task #
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      5280 2x:      1480 4x:       370
All internal counts: zz :      5629 yy:         0 xx:         0 ww:         0
One-external counts: yz :      9682 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:       818 wz:       810 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:       715    task #   2:       649    task #   3:         0    task #   4:      8526
task #   5:         0    task #   6:         0    task #   7:      4962    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:       865    task #  12:       549
task #  13:       224    task #  14:       170    task #

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     1.00000000     0.00000000
 ref:   2     0.00000000     1.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -25.7042693760  2.1316E-14  5.4110E-02  3.3283E-01  1.0000E-03
 mr-sdci #  1  2    -25.6559723467  1.4211E-14  0.0000E+00  3.3458E-01  1.0000E-03

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.00   0.00   0.00   0.00
    4   11    0   0.03   0.01   0.00   0.03
    5   15    0   0.00   0.00   0.00   0.00
    6   16    0   0.00   0.00   0.00   0.00
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.00   0.00   0.00   0.00
    9    6    0   0.00   0.00   0.00   0.00
   10    7    0   0.00   0.00   0.00   0.00
   11   75    0   0.01   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.01   0.00   0.00   0.01
   14   47    0   0.00   0.00   0.00   0.00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.0800s 
time spent in multnx:                   0.0700s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.2000s 

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -25.7042693760  2.1316E-14  5.4110E-02  3.3283E-01  1.0000E-03
 mr-sdci #  1  2    -25.6559723467  1.4211E-14  0.0000E+00  3.3458E-01  1.0000E-03

 expansion vectors are not transformed.
 matrix-vector products are not transformed.

    3 expansion eigenvectors written to unit nvfile (= 11)
    2 matrix-vector products written to unit nhvfil (= 10)

 from bk iterations: iconv=   1

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:             13970
 number of initial trial vectors:                         3
 number of initial matrix-vector products:                2
 maximum dimension of the subspace vectors:               7
 number of roots to converge:                             2
 number of iterations:                                  400
 residual norm convergence criteria:               0.000010  0.000010

          starting ci iteration   1

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      5280 2x:      1480 4x:       370
All internal counts: zz :      5629 yy:     10577 xx:      2870 ww:      1774
One-external counts: yz :      9682 yx:     10444 yw:      8199
Two-external counts: yy :      4346 ww:       765 xx:      1375 xz:       818 wz:       810 wx:      1495
Three-ext.   counts: yx :      1415 yw:      1165

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:       715    task #   2:       649    task #   3:      1239    task #   4:      8526
task #   5:      8985    task #   6:      7000    task #   7:      4962    task #   8:      8696
task #   9:      2471    task #  10:      1552    task #  11:       865    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.98598122     0.00000003    -0.16685635
 ref:   2    -0.00000002     1.00000000     0.00000001

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -25.7560331450  5.1764E-02  2.6026E-03  7.2622E-02  1.0000E-05
 mr-sdci #  1  2    -25.6559723467  0.0000E+00  0.0000E+00  3.3458E-01  1.0000E-05
 mr-sdci #  1  3    -23.8967718448  2.8093E+01  0.0000E+00  7.9335E-01  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.02   0.00   0.00   0.01
    3   26    0   0.05   0.01   0.00   0.05
    4   11    0   0.03   0.01   0.00   0.03
    5   15    0   0.09   0.02   0.00   0.09
    6   16    0   0.08   0.01   0.00   0.08
    7    1    0   0.02   0.01   0.00   0.02
    8    5    0   0.06   0.02   0.00   0.06
    9    6    0   0.06   0.01   0.00   0.05
   10    7    0   0.04   0.00   0.00   0.04
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.01   0.00   0.00   0.00
   13   46    0   0.02   0.00   0.00   0.02
   14   47    0   0.03   0.00   0.00   0.02
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.5200s 
time spent in multnx:                   0.4800s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0900s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.5400s 

          starting ci iteration   2

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      5280 2x:      1480 4x:       370
All internal counts: zz :      5629 yy:     10577 xx:      2870 ww:      1774
One-external counts: yz :      9682 yx:     10444 yw:      8199
Two-external counts: yy :      4346 ww:       765 xx:      1375 xz:       818 wz:       810 wx:      1495
Three-ext.   counts: yx :      1415 yw:      1165

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:       715    task #   2:       649    task #   3:      1239    task #   4:      8526
task #   5:      8985    task #   6:      7000    task #   7:      4962    task #   8:      8696
task #   9:      2471    task #  10:      1552    task #  11:       865    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1    -0.98477876    -0.00000002     0.15220804     0.08392559
 ref:   2     0.00000002    -1.00000000     0.00000000    -0.00000001

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1    -25.7581747844  2.1416E-03  2.2698E-04  2.0055E-02  1.0000E-05
 mr-sdci #  2  2    -25.6559723467  0.0000E+00  0.0000E+00  3.3458E-01  1.0000E-05
 mr-sdci #  2  3    -24.0756207736  1.7885E-01  0.0000E+00  8.7525E-01  1.0000E-04
 mr-sdci #  2  4    -23.6391735631  2.7836E+01  0.0000E+00  1.0219E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.02   0.00   0.00   0.02
    3   26    0   0.05   0.00   0.00   0.05
    4   11    0   0.02   0.02   0.00   0.02
    5   15    0   0.10   0.01   0.00   0.09
    6   16    0   0.08   0.01   0.00   0.08
    7    1    0   0.02   0.01   0.00   0.02
    8    5    0   0.06   0.01   0.00   0.06
    9    6    0   0.06   0.01   0.00   0.06
   10    7    0   0.04   0.01   0.00   0.04
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.01   0.01   0.00   0.01
   13   46    0   0.05   0.00   0.00   0.05
   14   47    0   0.02   0.00   0.00   0.02
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.5400s 
time spent in multnx:                   0.5300s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0900s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.5700s 

          starting ci iteration   3

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      5280 2x:      1480 4x:       370
All internal counts: zz :      5629 yy:     10577 xx:      2870 ww:      1774
One-external counts: yz :      9682 yx:     10444 yw:      8199
Two-external counts: yy :      4346 ww:       765 xx:      1375 xz:       818 wz:       810 wx:      1495
Three-ext.   counts: yx :      1415 yw:      1165

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:       715    task #   2:       649    task #   3:      1239    task #   4:      8526
task #   5:      8985    task #   6:      7000    task #   7:      4962    task #   8:      8696
task #   9:      2471    task #  10:      1552    task #  11:       865    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1    -0.98453868    -0.00000001    -0.08358939    -0.14412747    -0.05407095
 ref:   2     0.00000001    -1.00000000    -0.00000004     0.00000002     0.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1    -25.7583626945  1.8791E-04  2.9481E-05  7.5543E-03  1.0000E-05
 mr-sdci #  3  2    -25.6559723467  3.5527E-15  0.0000E+00  3.3458E-01  1.0000E-05
 mr-sdci #  3  3    -24.4193166107  3.4370E-01  0.0000E+00  9.5114E-01  1.0000E-04
 mr-sdci #  3  4    -23.9122157585  2.7304E-01  0.0000E+00  8.7281E-01  1.0000E-04
 mr-sdci #  3  5    -23.5735569234  2.7770E+01  0.0000E+00  1.1092E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.01   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.06   0.00   0.00   0.06
    4   11    0   0.04   0.01   0.00   0.03
    5   15    0   0.10   0.01   0.00   0.10
    6   16    0   0.09   0.01   0.00   0.09
    7    1    0   0.03   0.01   0.00   0.03
    8    5    0   0.06   0.01   0.00   0.06
    9    6    0   0.06   0.00   0.00   0.06
   10    7    0   0.04   0.01   0.00   0.04
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.01   0.00   0.00   0.01
   13   46    0   0.02   0.00   0.00   0.02
   14   47    0   0.02   0.00   0.00   0.02
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.5500s 
time spent in multnx:                   0.5400s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0700s 
time for vector access in mult:         0.0200s 
total time per CI iteration:            0.5900s 

          starting ci iteration   4

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      5280 2x:      1480 4x:       370
All internal counts: zz :      5629 yy:     10577 xx:      2870 ww:      1774
One-external counts: yz :      9682 yx:     10444 yw:      8199
Two-external counts: yy :      4346 ww:       765 xx:      1375 xz:       818 wz:       810 wx:      1495
Three-ext.   counts: yx :      1415 yw:      1165

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:       715    task #   2:       649    task #   3:      1239    task #   4:      8526
task #   5:      8985    task #   6:      7000    task #   7:      4962    task #   8:      8696
task #   9:      2471    task #  10:      1552    task #  11:       865    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.98440312     0.00000000     0.07505023     0.09533429    -0.12738384    -0.00164117
 ref:   2     0.00000000    -1.00000000     0.00000015     0.00000000     0.00000005    -0.00000004

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1    -25.7583892866  2.6592E-05  3.8323E-06  2.6993E-03  1.0000E-05
 mr-sdci #  4  2    -25.6559723467  1.7764E-14  0.0000E+00  3.3458E-01  1.0000E-05
 mr-sdci #  4  3    -24.7372847315  3.1797E-01  0.0000E+00  7.6538E-01  1.0000E-04
 mr-sdci #  4  4    -23.9425355718  3.0320E-02  0.0000E+00  1.0328E+00  1.0000E-04
 mr-sdci #  4  5    -23.7950555949  2.2150E-01  0.0000E+00  1.0431E+00  1.0000E-04
 mr-sdci #  4  6    -23.3198226566  2.7516E+01  0.0000E+00  1.0485E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.05   0.00   0.00   0.05
    4   11    0   0.03   0.01   0.00   0.03
    5   15    0   0.10   0.01   0.00   0.10
    6   16    0   0.08   0.02   0.00   0.08
    7    1    0   0.01   0.01   0.00   0.01
    8    5    0   0.07   0.01   0.00   0.07
    9    6    0   0.05   0.01   0.00   0.05
   10    7    0   0.04   0.00   0.00   0.03
   11   75    0   0.01   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.03   0.00   0.00   0.03
   14   47    0   0.02   0.00   0.00   0.02
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.030000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.5100s 
time spent in multnx:                   0.4900s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0700s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.5600s 

          starting ci iteration   5

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      5280 2x:      1480 4x:       370
All internal counts: zz :      5629 yy:     10577 xx:      2870 ww:      1774
One-external counts: yz :      9682 yx:     10444 yw:      8199
Two-external counts: yy :      4346 ww:       765 xx:      1375 xz:       818 wz:       810 wx:      1495
Three-ext.   counts: yx :      1415 yw:      1165

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:       715    task #   2:       649    task #   3:      1239    task #   4:      8526
task #   5:      8985    task #   6:      7000    task #   7:      4962    task #   8:      8696
task #   9:      2471    task #  10:      1552    task #  11:       865    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.98436724     0.00000001    -0.05002868     0.10602908    -0.12505453    -0.03969771    -0.00784561
 ref:   2     0.00000000    -1.00000000    -0.00000031    -0.00000001     0.00000005    -0.00000004     0.00000002

 trial vector basis is being transformed.  new dimension:   4

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1    -25.7583929726  3.6860E-06  6.2756E-07  1.0565E-03  1.0000E-05
 mr-sdci #  5  2    -25.6559723467  4.2633E-14  0.0000E+00  3.3458E-01  1.0000E-05
 mr-sdci #  5  3    -25.0550871383  3.1780E-01  0.0000E+00  5.8428E-01  1.0000E-04
 mr-sdci #  5  4    -24.1496763332  2.0714E-01  0.0000E+00  9.7814E-01  1.0000E-04
 mr-sdci #  5  5    -23.7951794759  1.2388E-04  0.0000E+00  1.0540E+00  1.0000E-04
 mr-sdci #  5  6    -23.5886016405  2.6878E-01  0.0000E+00  1.0395E+00  1.0000E-04
 mr-sdci #  5  7    -23.2161942202  2.7413E+01  0.0000E+00  1.1423E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.01   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.05   0.00   0.00   0.05
    4   11    0   0.03   0.01   0.00   0.03
    5   15    0   0.10   0.01   0.00   0.10
    6   16    0   0.08   0.01   0.00   0.08
    7    1    0   0.02   0.00   0.00   0.02
    8    5    0   0.06   0.02   0.00   0.06
    9    6    0   0.06   0.00   0.00   0.06
   10    7    0   0.04   0.00   0.00   0.04
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.03   0.00   0.00   0.02
   14   47    0   0.02   0.00   0.00   0.02
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.050000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.5100s 
time spent in multnx:                   0.5000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0600s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.5700s 

          starting ci iteration   6

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      5280 2x:      1480 4x:       370
All internal counts: zz :      5629 yy:     10577 xx:      2870 ww:      1774
One-external counts: yz :      9682 yx:     10444 yw:      8199
Two-external counts: yy :      4346 ww:       765 xx:      1375 xz:       818 wz:       810 wx:      1495
Three-ext.   counts: yx :      1415 yw:      1165

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:       715    task #   2:       649    task #   3:      1239    task #   4:      8526
task #   5:      8985    task #   6:      7000    task #   7:      4962    task #   8:      8696
task #   9:      2471    task #  10:      1552    task #  11:       865    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     0.98437093     0.00000001     0.04002821    -0.06885347     0.09214653
 ref:   2     0.00000000    -1.00000000     0.00000054    -0.00000007    -0.00000014

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  6  1    -25.7583935068  5.3420E-07  1.5451E-07  5.2101E-04  1.0000E-05
 mr-sdci #  6  2    -25.6559723467  1.1013E-13  0.0000E+00  3.3458E-01  1.0000E-05
 mr-sdci #  6  3    -25.1562720135  1.0118E-01  0.0000E+00  4.8554E-01  1.0000E-04
 mr-sdci #  6  4    -24.1845486572  3.4872E-02  0.0000E+00  1.0445E+00  1.0000E-04
 mr-sdci #  6  5    -24.0387900266  2.4361E-01  0.0000E+00  1.1102E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.02   0.00   0.00   0.02
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.05   0.01   0.00   0.05
    4   11    0   0.03   0.01   0.00   0.03
    5   15    0   0.09   0.02   0.00   0.09
    6   16    0   0.08   0.01   0.00   0.07
    7    1    0   0.02   0.01   0.00   0.01
    8    5    0   0.07   0.01   0.00   0.07
    9    6    0   0.05   0.01   0.00   0.05
   10    7    0   0.04   0.00   0.00   0.04
   11   75    0   0.01   0.01   0.00   0.01
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.03   0.00   0.00   0.03
   14   47    0   0.02   0.00   0.00   0.02
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.5200s 
time spent in multnx:                   0.5000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0900s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.5500s 

          starting ci iteration   7

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      5280 2x:      1480 4x:       370
All internal counts: zz :      5629 yy:     10577 xx:      2870 ww:      1774
One-external counts: yz :      9682 yx:     10444 yw:      8199
Two-external counts: yy :      4346 ww:       765 xx:      1375 xz:       818 wz:       810 wx:      1495
Three-ext.   counts: yx :      1415 yw:      1165

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:       715    task #   2:       649    task #   3:      1239    task #   4:      8526
task #   5:      8985    task #   6:      7000    task #   7:      4962    task #   8:      8696
task #   9:      2471    task #  10:      1552    task #  11:       865    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.98436059     0.00000003    -0.03772601    -0.06512032     0.04223065    -0.09723340
 ref:   2     0.00000000    -1.00000000    -0.00000090     0.00000020     0.00000012    -0.00000003

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  7  1    -25.7583936502  1.4342E-07  2.5360E-08  2.1071E-04  1.0000E-05
 mr-sdci #  7  2    -25.6559723467  2.2027E-13  0.0000E+00  3.3458E-01  1.0000E-05
 mr-sdci #  7  3    -25.2480889331  9.1817E-02  0.0000E+00  4.2007E-01  1.0000E-04
 mr-sdci #  7  4    -24.4884955907  3.0395E-01  0.0000E+00  8.8661E-01  1.0000E-04
 mr-sdci #  7  5    -24.1743687734  1.3558E-01  0.0000E+00  1.0989E+00  1.0000E-04
 mr-sdci #  7  6    -23.4643072588 -1.2429E-01  0.0000E+00  1.0798E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.01   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.05   0.00   0.00   0.05
    4   11    0   0.03   0.01   0.00   0.03
    5   15    0   0.10   0.01   0.00   0.10
    6   16    0   0.08   0.01   0.00   0.08
    7    1    0   0.02   0.00   0.00   0.02
    8    5    0   0.06   0.02   0.00   0.06
    9    6    0   0.06   0.00   0.00   0.06
   10    7    0   0.04   0.00   0.00   0.04
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.03   0.00   0.00   0.03
   14   47    0   0.02   0.00   0.00   0.02
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.5100s 
time spent in multnx:                   0.5100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0600s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.5400s 

          starting ci iteration   8

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      5280 2x:      1480 4x:       370
All internal counts: zz :      5629 yy:     10577 xx:      2870 ww:      1774
One-external counts: yz :      9682 yx:     10444 yw:      8199
Two-external counts: yy :      4346 ww:       765 xx:      1375 xz:       818 wz:       810 wx:      1495
Three-ext.   counts: yx :      1415 yw:      1165

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:       715    task #   2:       649    task #   3:      1239    task #   4:      8526
task #   5:      8985    task #   6:      7000    task #   7:      4962    task #   8:      8696
task #   9:      2471    task #  10:      1552    task #  11:       865    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.98436349     0.00000002    -0.02433675     0.05705480    -0.06132546    -0.10028682     0.01394860
 ref:   2     0.00000000    -1.00000000    -0.00000129    -0.00000029    -0.00000002    -0.00000003    -0.00000001

 trial vector basis is being transformed.  new dimension:   4

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  8  1    -25.7583936717  2.1421E-08  4.3006E-09  8.6648E-05  1.0000E-05
 mr-sdci #  8  2    -25.6559723467  2.4514E-13  0.0000E+00  3.3458E-01  1.0000E-05
 mr-sdci #  8  3    -25.3085693538  6.0480E-02  0.0000E+00  3.5815E-01  1.0000E-04
 mr-sdci #  8  4    -24.7947729910  3.0628E-01  0.0000E+00  6.3052E-01  1.0000E-04
 mr-sdci #  8  5    -24.3160807221  1.4171E-01  0.0000E+00  9.8533E-01  1.0000E-04
 mr-sdci #  8  6    -23.4691764351  4.8692E-03  0.0000E+00  1.0651E+00  1.0000E-04
 mr-sdci #  8  7    -23.2206839362  4.4897E-03  0.0000E+00  1.2220E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.05   0.00   0.00   0.05
    4   11    0   0.03   0.01   0.00   0.03
    5   15    0   0.10   0.01   0.00   0.10
    6   16    0   0.08   0.01   0.00   0.08
    7    1    0   0.01   0.01   0.00   0.01
    8    5    0   0.07   0.02   0.00   0.06
    9    6    0   0.06   0.00   0.00   0.06
   10    7    0   0.04   0.00   0.00   0.04
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.03   0.00   0.00   0.03
   14   47    0   0.02   0.00   0.00   0.02
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.050000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.5100s 
time spent in multnx:                   0.5000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0600s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.5700s 

          starting ci iteration   9

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      5280 2x:      1480 4x:       370
All internal counts: zz :      5629 yy:     10577 xx:      2870 ww:      1774
One-external counts: yz :      9682 yx:     10444 yw:      8199
Two-external counts: yy :      4346 ww:       765 xx:      1375 xz:       818 wz:       810 wx:      1495
Three-ext.   counts: yx :      1415 yw:      1165

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:       715    task #   2:       649    task #   3:      1239    task #   4:      8526
task #   5:      8985    task #   6:      7000    task #   7:      4962    task #   8:      8696
task #   9:      2471    task #  10:      1552    task #  11:       865    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     0.98436195     0.00000004     0.02443810    -0.04219725    -0.05310904
 ref:   2     0.00000001    -1.00000000     0.00000175     0.00000038    -0.00000008

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  9  1    -25.7583936757  4.0516E-09  1.1811E-09  4.5509E-05  1.0000E-05
 mr-sdci #  9  2    -25.6559723467  4.1922E-13  0.0000E+00  3.3458E-01  1.0000E-05
 mr-sdci #  9  3    -25.3472690465  3.8700E-02  0.0000E+00  3.2044E-01  1.0000E-04
 mr-sdci #  9  4    -24.9295356435  1.3476E-01  0.0000E+00  5.7992E-01  1.0000E-04
 mr-sdci #  9  5    -24.0874162470 -2.2866E-01  0.0000E+00  1.0475E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.02   0.00   0.00   0.02
    3   26    0   0.05   0.00   0.00   0.05
    4   11    0   0.03   0.01   0.00   0.03
    5   15    0   0.09   0.02   0.00   0.09
    6   16    0   0.08   0.01   0.00   0.08
    7    1    0   0.02   0.01   0.00   0.02
    8    5    0   0.06   0.02   0.00   0.06
    9    6    0   0.06   0.01   0.00   0.06
   10    7    0   0.04   0.00   0.00   0.04
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.01   0.00   0.00   0.01
   13   46    0   0.02   0.00   0.00   0.02
   14   47    0   0.03   0.00   0.00   0.03
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.5200s 
time spent in multnx:                   0.5200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0800s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.5400s 

          starting ci iteration  10

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      5280 2x:      1480 4x:       370
All internal counts: zz :      5629 yy:     10577 xx:      2870 ww:      1774
One-external counts: yz :      9682 yx:     10444 yw:      8199
Two-external counts: yy :      4346 ww:       765 xx:      1375 xz:       818 wz:       810 wx:      1495
Three-ext.   counts: yx :      1415 yw:      1165

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:       715    task #   2:       649    task #   3:      1239    task #   4:      8526
task #   5:      8985    task #   6:      7000    task #   7:      4962    task #   8:      8696
task #   9:      2471    task #  10:      1552    task #  11:       865    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.98436267     0.00000002     0.01578351    -0.04759922     0.03042409     0.05205788
 ref:   2     0.00000001    -1.00000000     0.00000231     0.00000048     0.00000016    -0.00000007

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 10  1    -25.7583936769  1.1602E-09  1.6466E-10  1.6900E-05  1.0000E-05
 mr-sdci # 10  2    -25.6559723467  5.8975E-13  0.0000E+00  3.3458E-01  1.0000E-05
 mr-sdci # 10  3    -25.3802983384  3.3029E-02  0.0000E+00  2.7494E-01  1.0000E-04
 mr-sdci # 10  4    -25.0229104014  9.3375E-02  0.0000E+00  5.1337E-01  1.0000E-04
 mr-sdci # 10  5    -24.3330711189  2.4565E-01  0.0000E+00  8.7471E-01  1.0000E-04
 mr-sdci # 10  6    -23.7585693267  2.8939E-01  0.0000E+00  1.1488E+00  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.05   0.00   0.00   0.05
    4   11    0   0.03   0.01   0.00   0.03
    5   15    0   0.10   0.01   0.00   0.10
    6   16    0   0.08   0.01   0.00   0.08
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.07   0.02   0.00   0.06
    9    6    0   0.06   0.00   0.00   0.06
   10    7    0   0.04   0.01   0.00   0.04
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.01   0.01   0.00   0.01
   13   46    0   0.03   0.00   0.00   0.03
   14   47    0   0.02   0.00   0.00   0.02
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.030000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.5200s 
time spent in multnx:                   0.5100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0700s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.5700s 

          starting ci iteration  11

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      5280 2x:      1480 4x:       370
All internal counts: zz :      5629 yy:     10577 xx:      2870 ww:      1774
One-external counts: yz :      9682 yx:     10444 yw:      8199
Two-external counts: yy :      4346 ww:       765 xx:      1375 xz:       818 wz:       810 wx:      1495
Three-ext.   counts: yx :      1415 yw:      1165

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:       715    task #   2:       649    task #   3:      1239    task #   4:      8526
task #   5:      8985    task #   6:      7000    task #   7:      4962    task #   8:      8696
task #   9:      2471    task #  10:      1552    task #  11:       865    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.98436215     0.00000005     0.02013795    -0.02409951    -0.06747470     0.05182663     0.00401177
 ref:   2     0.00000001    -1.00000000     0.00000268     0.00000059    -0.00000014    -0.00000007    -0.00000005

 trial vector basis is being transformed.  new dimension:   4

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 11  1    -25.7583936770  1.4296E-10  0.0000E+00  7.9296E-06  1.0000E-05
 mr-sdci # 11  2    -25.6559723467  4.5830E-13  5.9626E-02  3.3458E-01  1.0000E-05
 mr-sdci # 11  3    -25.3945720968  1.4274E-02  0.0000E+00  2.7127E-01  1.0000E-04
 mr-sdci # 11  4    -25.0960849042  7.3175E-02  0.0000E+00  4.1808E-01  1.0000E-04
 mr-sdci # 11  5    -24.6872480538  3.5418E-01  0.0000E+00  6.5316E-01  1.0000E-04
 mr-sdci # 11  6    -23.8338246512  7.5255E-02  0.0000E+00  1.1441E+00  1.0000E-04
 mr-sdci # 11  7    -23.5132999434  2.9262E-01  0.0000E+00  1.1414E+00  1.0000E-04

 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.05   0.00   0.00   0.05
    4   11    0   0.03   0.01   0.00   0.03
    5   15    0   0.10   0.01   0.00   0.10
    6   16    0   0.08   0.01   0.00   0.08
    7    1    0   0.02   0.00   0.00   0.02
    8    5    0   0.06   0.02   0.00   0.06
    9    6    0   0.06   0.01   0.00   0.06
   10    7    0   0.04   0.00   0.00   0.04
   11   75    0   0.01   0.00   0.00   0.01
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.03   0.00   0.00   0.03
   14   47    0   0.02   0.00   0.00   0.02
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.070000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.5200s 
time spent in multnx:                   0.5200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0600s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.6000s 

          starting ci iteration  12

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      5280 2x:      1480 4x:       370
All internal counts: zz :      5629 yy:     10577 xx:      2870 ww:      1774
One-external counts: yz :      9682 yx:     10444 yw:      8199
Two-external counts: yy :      4346 ww:       765 xx:      1375 xz:       818 wz:       810 wx:      1495
Three-ext.   counts: yx :      1415 yw:      1165

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:       715    task #   2:       649    task #   3:      1239    task #   4:      8526
task #   5:      8985    task #   6:      7000    task #   7:      4962    task #   8:      8696
task #   9:      2471    task #  10:      1552    task #  11:       865    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     0.98436215    -0.00000004    -0.02013795     0.02409951    -0.00000034
 ref:   2     0.00000001    -0.98301816    -0.00000060    -0.00000036     0.18350830

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 12  1    -25.7583936770  0.0000E+00  0.0000E+00  7.9296E-06  1.0000E-05
 mr-sdci # 12  2    -25.7131138148  5.7141E-02  3.4924E-03  7.8109E-02  1.0000E-05
 mr-sdci # 12  3    -25.3945720968  5.2580E-12  0.0000E+00  2.7127E-01  1.0000E-04
 mr-sdci # 12  4    -25.0960849042  2.2027E-13  0.0000E+00  4.1808E-01  1.0000E-04
 mr-sdci # 12  5    -24.0162773751 -6.7097E-01  0.0000E+00  7.5282E-01  1.0000E-04

 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.05   0.00   0.00   0.05
    4   11    0   0.03   0.01   0.00   0.02
    5   15    0   0.10   0.01   0.00   0.10
    6   16    0   0.08   0.01   0.00   0.08
    7    1    0   0.02   0.00   0.00   0.02
    8    5    0   0.06   0.02   0.00   0.06
    9    6    0   0.06   0.00   0.00   0.06
   10    7    0   0.04   0.01   0.00   0.04
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.01   0.00   0.00   0.01
   13   46    0   0.02   0.00   0.00   0.02
   14   47    0   0.02   0.00   0.00   0.02
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.5100s 
time spent in multnx:                   0.5000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0600s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.5400s 

          starting ci iteration  13

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      5280 2x:      1480 4x:       370
All internal counts: zz :      5629 yy:     10577 xx:      2870 ww:      1774
One-external counts: yz :      9682 yx:     10444 yw:      8199
Two-external counts: yy :      4346 ww:       765 xx:      1375 xz:       818 wz:       810 wx:      1495
Three-ext.   counts: yx :      1415 yw:      1165

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:       715    task #   2:       649    task #   3:      1239    task #   4:      8526
task #   5:      8985    task #   6:      7000    task #   7:      4962    task #   8:      8696
task #   9:      2471    task #  10:      1552    task #  11:       865    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.98436215     0.00000006     0.02013795     0.02409951    -0.00000194     0.00000126
 ref:   2     0.00000001    -0.98102021     0.00000007     0.00000013    -0.11895449    -0.15313126

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 13  1    -25.7583936770  0.0000E+00  0.0000E+00  7.9296E-06  1.0000E-05
 mr-sdci # 13  2    -25.7158825099  2.7687E-03  4.3482E-04  2.6143E-02  1.0000E-05
 mr-sdci # 13  3    -25.3945720968  6.1426E-12  0.0000E+00  2.7127E-01  1.0000E-04
 mr-sdci # 13  4    -25.0960849042  9.7309E-12  0.0000E+00  4.1808E-01  1.0000E-04
 mr-sdci # 13  5    -24.4467261246  4.3045E-01  0.0000E+00  9.8712E-01  1.0000E-04
 mr-sdci # 13  6    -23.9268486191  9.3024E-02  0.0000E+00  8.5281E-01  1.0000E-04

 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.02   0.00   0.00   0.02
    3   26    0   0.05   0.00   0.00   0.05
    4   11    0   0.02   0.02   0.00   0.02
    5   15    0   0.10   0.01   0.00   0.09
    6   16    0   0.08   0.01   0.00   0.08
    7    1    0   0.02   0.01   0.00   0.02
    8    5    0   0.06   0.01   0.00   0.06
    9    6    0   0.06   0.01   0.00   0.06
   10    7    0   0.04   0.01   0.00   0.04
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.01   0.01   0.00   0.01
   13   46    0   0.02   0.00   0.00   0.02
   14   47    0   0.02   0.00   0.00   0.02
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.030000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.5100s 
time spent in multnx:                   0.5000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0900s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.5500s 

          starting ci iteration  14

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      5280 2x:      1480 4x:       370
All internal counts: zz :      5629 yy:     10577 xx:      2870 ww:      1774
One-external counts: yz :      9682 yx:     10444 yw:      8199
Two-external counts: yy :      4346 ww:       765 xx:      1375 xz:       818 wz:       810 wx:      1495
Three-ext.   counts: yx :      1415 yw:      1165

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:       715    task #   2:       649    task #   3:      1239    task #   4:      8526
task #   5:      8985    task #   6:      7000    task #   7:      4962    task #   8:      8696
task #   9:      2471    task #  10:      1552    task #  11:       865    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.98436215     0.00000005    -0.02013795     0.02409951    -0.00000223     0.00000612    -0.00000005
 ref:   2     0.00000001     0.98063461    -0.00000017    -0.00000008     0.12107542     0.03290063    -0.15037966

 trial vector basis is being transformed.  new dimension:   4

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 14  1    -25.7583936770  3.5527E-15  0.0000E+00  7.9296E-06  1.0000E-05
 mr-sdci # 14  2    -25.7162555662  3.7306E-04  7.4632E-05  1.1041E-02  1.0000E-05
 mr-sdci # 14  3    -25.3945720968  1.2825E-12  0.0000E+00  2.7127E-01  1.0000E-04
 mr-sdci # 14  4    -25.0960849042  8.4164E-12  0.0000E+00  4.1808E-01  1.0000E-04
 mr-sdci # 14  5    -24.4959092635  4.9183E-02  0.0000E+00  8.9284E-01  1.0000E-04
 mr-sdci # 14  6    -24.3635334890  4.3668E-01  0.0000E+00  1.0769E+00  1.0000E-04
 mr-sdci # 14  7    -23.9063362922  3.9304E-01  0.0000E+00  8.5471E-01  1.0000E-04

 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.02   0.00   0.00   0.02
    3   26    0   0.05   0.00   0.00   0.05
    4   11    0   0.02   0.02   0.00   0.02
    5   15    0   0.10   0.01   0.00   0.09
    6   16    0   0.08   0.01   0.00   0.08
    7    1    0   0.02   0.01   0.00   0.02
    8    5    0   0.06   0.01   0.00   0.06
    9    6    0   0.06   0.01   0.00   0.06
   10    7    0   0.04   0.01   0.00   0.04
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.01   0.01   0.00   0.01
   13   46    0   0.02   0.00   0.00   0.02
   14   47    0   0.02   0.00   0.00   0.02
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.040000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.5100s 
time spent in multnx:                   0.5000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0900s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.5700s 

          starting ci iteration  15

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      5280 2x:      1480 4x:       370
All internal counts: zz :      5629 yy:     10577 xx:      2870 ww:      1774
One-external counts: yz :      9682 yx:     10444 yw:      8199
Two-external counts: yy :      4346 ww:       765 xx:      1375 xz:       818 wz:       810 wx:      1495
Three-ext.   counts: yx :      1415 yw:      1165

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:       715    task #   2:       649    task #   3:      1239    task #   4:      8526
task #   5:      8985    task #   6:      7000    task #   7:      4962    task #   8:      8696
task #   9:      2471    task #  10:      1552    task #  11:       865    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     0.98436215    -0.00000008    -0.02013795    -0.02409951    -0.00001577
 ref:   2     0.00000001     0.98047191     0.00000008    -0.00000038    -0.02442469

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 15  1    -25.7583936770  3.5527E-15  0.0000E+00  7.9296E-06  1.0000E-05
 mr-sdci # 15  2    -25.7163224792  6.6913E-05  2.5044E-05  5.5673E-03  1.0000E-05
 mr-sdci # 15  3    -25.3945720968  2.5814E-11  0.0000E+00  2.7127E-01  1.0000E-04
 mr-sdci # 15  4    -25.0960849043  1.0598E-10  0.0000E+00  4.1808E-01  1.0000E-04
 mr-sdci # 15  5    -24.6498558575  1.5395E-01  0.0000E+00  1.1550E+00  1.0000E-04

 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.05   0.00   0.00   0.05
    4   11    0   0.03   0.01   0.00   0.03
    5   15    0   0.10   0.01   0.00   0.10
    6   16    0   0.08   0.01   0.00   0.08
    7    1    0   0.01   0.01   0.00   0.01
    8    5    0   0.07   0.02   0.00   0.06
    9    6    0   0.06   0.00   0.00   0.06
   10    7    0   0.04   0.00   0.00   0.04
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.03   0.01   0.00   0.03
   14   47    0   0.02   0.00   0.00   0.02
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.019999
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.5100s 
time spent in multnx:                   0.5000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0700s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.5500s 

          starting ci iteration  16

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      5280 2x:      1480 4x:       370
All internal counts: zz :      5629 yy:     10577 xx:      2870 ww:      1774
One-external counts: yz :      9682 yx:     10444 yw:      8199
Two-external counts: yy :      4346 ww:       765 xx:      1375 xz:       818 wz:       810 wx:      1495
Three-ext.   counts: yx :      1415 yw:      1165

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:       715    task #   2:       649    task #   3:      1239    task #   4:      8526
task #   5:      8985    task #   6:      7000    task #   7:      4962    task #   8:      8696
task #   9:      2471    task #  10:      1552    task #  11:       865    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.98436215    -0.00000008     0.02013795    -0.00001824    -0.02409951     0.00003458
 ref:   2     0.00000001    -0.98010438     0.00000110     0.05867413    -0.00000625    -0.01603393

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 16  1    -25.7583936770  0.0000E+00  0.0000E+00  7.9296E-06  1.0000E-05
 mr-sdci # 16  2    -25.7163495755  2.7096E-05  8.2335E-06  3.4782E-03  1.0000E-05
 mr-sdci # 16  3    -25.3945720969  8.7162E-11  0.0000E+00  2.7127E-01  1.0000E-04
 mr-sdci # 16  4    -25.1462320081  5.0147E-02  0.0000E+00  6.3476E-01  1.0000E-04
 mr-sdci # 16  5    -25.0960849038  4.4623E-01  0.0000E+00  4.1808E-01  1.0000E-04
 mr-sdci # 16  6    -24.2632417321 -1.0029E-01  0.0000E+00  1.2064E+00  1.0000E-04

 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.06   0.00   0.00   0.06
    4   11    0   0.02   0.02   0.00   0.02
    5   15    0   0.10   0.01   0.00   0.10
    6   16    0   0.08   0.01   0.00   0.08
    7    1    0   0.02   0.01   0.00   0.02
    8    5    0   0.06   0.01   0.00   0.06
    9    6    0   0.06   0.00   0.00   0.06
   10    7    0   0.04   0.01   0.00   0.04
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.01   0.00   0.00   0.01
   13   46    0   0.02   0.00   0.00   0.02
   14   47    0   0.02   0.00   0.00   0.02
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.030001
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.5100s 
time spent in multnx:                   0.5100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0700s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.5500s 

          starting ci iteration  17

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      5280 2x:      1480 4x:       370
All internal counts: zz :      5629 yy:     10577 xx:      2870 ww:      1774
One-external counts: yz :      9682 yx:     10444 yw:      8199
Two-external counts: yy :      4346 ww:       765 xx:      1375 xz:       818 wz:       810 wx:      1495
Three-ext.   counts: yx :      1415 yw:      1165

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:       715    task #   2:       649    task #   3:      1239    task #   4:      8526
task #   5:      8985    task #   6:      7000    task #   7:      4962    task #   8:      8696
task #   9:      2471    task #  10:      1552    task #  11:       865    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.98436215     0.00000007    -0.02013795    -0.00001650     0.02409951     0.00006212     0.00005277
 ref:   2     0.00000001    -0.98012454     0.00000090    -0.03694229    -0.00000405    -0.04874460    -0.05506909

 trial vector basis is being transformed.  new dimension:   4

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 17  1    -25.7583936770 -7.1054E-15  0.0000E+00  7.9296E-06  1.0000E-05
 mr-sdci # 17  2    -25.7163562627  6.6873E-06  8.1352E-07  1.2511E-03  1.0000E-05
 mr-sdci # 17  3    -25.3945720975  5.6538E-10  0.0000E+00  2.7127E-01  1.0000E-04
 mr-sdci # 17  4    -25.3149088819  1.6868E-01  0.0000E+00  3.4414E-01  1.0000E-04
 mr-sdci # 17  5    -25.0960849054  1.5703E-09  0.0000E+00  4.1808E-01  1.0000E-04
 mr-sdci # 17  6    -24.4162891357  1.5305E-01  0.0000E+00  1.0454E+00  1.0000E-04
 mr-sdci # 17  7    -23.5350402018 -3.7130E-01  0.0000E+00  1.0973E+00  1.0000E-04

 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.06   0.00   0.00   0.06
    4   11    0   0.02   0.02   0.00   0.02
    5   15    0   0.10   0.01   0.00   0.10
    6   16    0   0.08   0.01   0.00   0.08
    7    1    0   0.02   0.01   0.00   0.02
    8    5    0   0.06   0.01   0.00   0.06
    9    6    0   0.06   0.00   0.00   0.06
   10    7    0   0.04   0.01   0.00   0.04
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.01   0.00   0.00   0.01
   13   46    0   0.02   0.00   0.00   0.02
   14   47    0   0.02   0.00   0.00   0.02
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.050000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.5100s 
time spent in multnx:                   0.5100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0700s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.5700s 

          starting ci iteration  18

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      5280 2x:      1480 4x:       370
All internal counts: zz :      5629 yy:     10577 xx:      2870 ww:      1774
One-external counts: yz :      9682 yx:     10444 yw:      8199
Two-external counts: yy :      4346 ww:       765 xx:      1375 xz:       818 wz:       810 wx:      1495
Three-ext.   counts: yx :      1415 yw:      1165

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:       715    task #   2:       649    task #   3:      1239    task #   4:      8526
task #   5:      8985    task #   6:      7000    task #   7:      4962    task #   8:      8696
task #   9:      2471    task #  10:      1552    task #  11:       865    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     0.98436215    -0.00000008     0.02013793    -0.00002815    -0.00023210
 ref:   2     0.00000001    -0.98003867     0.00001419     0.05482694     0.09656224

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 18  1    -25.7583936770  0.0000E+00  0.0000E+00  7.9296E-06  1.0000E-05
 mr-sdci # 18  2    -25.7163569904  7.2762E-07  1.6741E-07  4.8430E-04  1.0000E-05
 mr-sdci # 18  3    -25.3945721008  3.3237E-09  0.0000E+00  2.7127E-01  1.0000E-04
 mr-sdci # 18  4    -25.3563280798  4.1419E-02  0.0000E+00  2.4725E-01  1.0000E-04
 mr-sdci # 18  5    -23.9663405691 -1.1297E+00  0.0000E+00  1.0825E+00  1.0000E-04

 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.01   0.00   0.01
    3   26    0   0.05   0.00   0.00   0.05
    4   11    0   0.03   0.01   0.00   0.03
    5   15    0   0.10   0.01   0.00   0.10
    6   16    0   0.08   0.01   0.00   0.08
    7    1    0   0.01   0.01   0.00   0.01
    8    5    0   0.07   0.01   0.00   0.07
    9    6    0   0.05   0.02   0.00   0.05
   10    7    0   0.04   0.00   0.00   0.04
   11   75    0   0.01   0.00   0.00   0.01
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.03   0.00   0.00   0.03
   14   47    0   0.02   0.00   0.00   0.02
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.5100s 
time spent in multnx:                   0.5100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0800s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.5500s 

          starting ci iteration  19

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      5280 2x:      1480 4x:       370
All internal counts: zz :      5629 yy:     10577 xx:      2870 ww:      1774
One-external counts: yz :      9682 yx:     10444 yw:      8199
Two-external counts: yy :      4346 ww:       765 xx:      1375 xz:       818 wz:       810 wx:      1495
Three-ext.   counts: yx :      1415 yw:      1165

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:       715    task #   2:       649    task #   3:      1239    task #   4:      8526
task #   5:      8985    task #   6:      7000    task #   7:      4962    task #   8:      8696
task #   9:      2471    task #  10:      1552    task #  11:       865    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.98436215    -0.00000010    -0.02013785     0.00004562    -0.00040579     0.00041394
 ref:   2     0.00000000     0.98005115     0.00000675     0.04859819     0.02970306    -0.11488835

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 19  1    -25.7583936770  0.0000E+00  0.0000E+00  7.9296E-06  1.0000E-05
 mr-sdci # 19  2    -25.7163571430  1.5265E-07  2.9624E-08  2.0927E-04  1.0000E-05
 mr-sdci # 19  3    -25.3945721236  2.2799E-08  0.0000E+00  2.7127E-01  1.0000E-04
 mr-sdci # 19  4    -25.3696356614  1.3308E-02  0.0000E+00  2.4033E-01  1.0000E-04
 mr-sdci # 19  5    -24.6089869248  6.4265E-01  0.0000E+00  7.7652E-01  1.0000E-04
 mr-sdci # 19  6    -23.8613200838 -5.5497E-01  0.0000E+00  1.0651E+00  1.0000E-04

 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.05   0.00   0.00   0.05
    4   11    0   0.03   0.01   0.00   0.02
    5   15    0   0.10   0.01   0.00   0.10
    6   16    0   0.08   0.01   0.00   0.08
    7    1    0   0.02   0.00   0.00   0.02
    8    5    0   0.06   0.02   0.00   0.06
    9    6    0   0.06   0.00   0.00   0.06
   10    7    0   0.04   0.01   0.00   0.04
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.01   0.00   0.00   0.01
   13   46    0   0.02   0.00   0.00   0.02
   14   47    0   0.02   0.00   0.00   0.02
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.030001
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.5100s 
time spent in multnx:                   0.5000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0600s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.5500s 

          starting ci iteration  20

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      5280 2x:      1480 4x:       370
All internal counts: zz :      5629 yy:     10577 xx:      2870 ww:      1774
One-external counts: yz :      9682 yx:     10444 yw:      8199
Two-external counts: yy :      4346 ww:       765 xx:      1375 xz:       818 wz:       810 wx:      1495
Three-ext.   counts: yx :      1415 yw:      1165

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:       715    task #   2:       649    task #   3:      1239    task #   4:      8526
task #   5:      8985    task #   6:      7000    task #   7:      4962    task #   8:      8696
task #   9:      2471    task #  10:      1552    task #  11:       865    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.98436215     0.00000008     0.02013717    -0.00011885    -0.00066025     0.00109782     0.00033258
 ref:   2     0.00000001     0.98005350     0.00006935     0.04568842    -0.02319468    -0.04918156     0.11593687

 trial vector basis is being transformed.  new dimension:   4

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 20  1    -25.7583936770  0.0000E+00  0.0000E+00  7.9295E-06  1.0000E-05
 mr-sdci # 20  2    -25.7163571675  2.4450E-08  6.4189E-09  1.0541E-04  1.0000E-05
 mr-sdci # 20  3    -25.3945723090  1.8535E-07  0.0000E+00  2.7127E-01  1.0000E-04
 mr-sdci # 20  4    -25.3778373523  8.2017E-03  0.0000E+00  2.0911E-01  1.0000E-04
 mr-sdci # 20  5    -24.9426434235  3.3366E-01  0.0000E+00  7.1051E-01  1.0000E-04
 mr-sdci # 20  6    -24.3731871317  5.1187E-01  0.0000E+00  9.3182E-01  1.0000E-04
 mr-sdci # 20  7    -23.5808823837  4.5842E-02  0.0000E+00  1.1138E+00  1.0000E-04

 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.05   0.00   0.00   0.05
    4   11    0   0.03   0.01   0.00   0.03
    5   15    0   0.10   0.02   0.00   0.10
    6   16    0   0.08   0.01   0.00   0.08
    7    1    0   0.02   0.00   0.00   0.02
    8    5    0   0.06   0.02   0.00   0.06
    9    6    0   0.08   0.00   0.00   0.07
   10    7    0   0.04   0.00   0.00   0.04
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.03   0.01   0.00   0.03
   14   47    0   0.02   0.00   0.00   0.02
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.050000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.5300s 
time spent in multnx:                   0.5200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0700s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.5900s 

          starting ci iteration  21

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      5280 2x:      1480 4x:       370
All internal counts: zz :      5629 yy:     10577 xx:      2870 ww:      1774
One-external counts: yz :      9682 yx:     10444 yw:      8199
Two-external counts: yy :      4346 ww:       765 xx:      1375 xz:       818 wz:       810 wx:      1495
Three-ext.   counts: yx :      1415 yw:      1165

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:       715    task #   2:       649    task #   3:      1239    task #   4:      8526
task #   5:      8985    task #   6:      7000    task #   7:      4962    task #   8:      8696
task #   9:      2471    task #  10:      1552    task #  11:       865    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     0.98436215    -0.00000009     0.02013568    -0.00010776    -0.00302659
 ref:   2     0.00000000     0.98005587    -0.00001340    -0.04258883     0.05447387

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 21  1    -25.7583936770  0.0000E+00  0.0000E+00  7.9295E-06  1.0000E-05
 mr-sdci # 21  2    -25.7163571723  4.8741E-09  1.6787E-09  4.4293E-05  1.0000E-05
 mr-sdci # 21  3    -25.3945726145  3.0548E-07  0.0000E+00  2.7127E-01  1.0000E-04
 mr-sdci # 21  4    -25.3820446766  4.2073E-03  0.0000E+00  2.0634E-01  1.0000E-04
 mr-sdci # 21  5    -24.1506668437 -7.9198E-01  0.0000E+00  1.1852E+00  1.0000E-04

 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.02   0.00   0.00   0.02
    3   26    0   0.05   0.00   0.00   0.05
    4   11    0   0.03   0.01   0.00   0.03
    5   15    0   0.09   0.02   0.00   0.09
    6   16    0   0.08   0.01   0.00   0.08
    7    1    0   0.02   0.01   0.00   0.02
    8    5    0   0.06   0.02   0.00   0.06
    9    6    0   0.06   0.01   0.00   0.06
   10    7    0   0.04   0.01   0.00   0.04
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.01   0.01   0.00   0.01
   13   46    0   0.02   0.00   0.00   0.02
   14   47    0   0.03   0.01   0.00   0.03
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.5200s 
time spent in multnx:                   0.5200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.1100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.5400s 

          starting ci iteration  22

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      5280 2x:      1480 4x:       370
All internal counts: zz :      5629 yy:     10577 xx:      2870 ww:      1774
One-external counts: yz :      9682 yx:     10444 yw:      8199
Two-external counts: yy :      4346 ww:       765 xx:      1375 xz:       818 wz:       810 wx:      1495
Three-ext.   counts: yx :      1415 yw:      1165

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:       715    task #   2:       649    task #   3:      1239    task #   4:      8526
task #   5:      8985    task #   6:      7000    task #   7:      4962    task #   8:      8696
task #   9:      2471    task #  10:      1552    task #  11:       865    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.98436215    -0.00000009    -0.02012525     0.00039497    -0.00312443     0.00393259
 ref:   2     0.00000000    -0.98005639    -0.00028242    -0.04076207    -0.02365015    -0.05094398

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 22  1    -25.7583936770  1.0658E-14  0.0000E+00  7.9288E-06  1.0000E-05
 mr-sdci # 22  2    -25.7163571741  1.7737E-09  4.7580E-10  2.6485E-05  1.0000E-05
 mr-sdci # 22  3    -25.3945750832  2.4688E-06  0.0000E+00  2.7125E-01  1.0000E-04
 mr-sdci # 22  4    -25.3852867671  3.2421E-03  0.0000E+00  1.9779E-01  1.0000E-04
 mr-sdci # 22  5    -25.0651417054  9.1447E-01  0.0000E+00  7.0499E-01  1.0000E-04
 mr-sdci # 22  6    -24.0887317083 -2.8446E-01  0.0000E+00  1.1909E+00  1.0000E-04

 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.01   0.00   0.01
    3   26    0   0.05   0.00   0.00   0.05
    4   11    0   0.03   0.01   0.00   0.03
    5   15    0   0.10   0.01   0.00   0.10
    6   16    0   0.08   0.02   0.00   0.08
    7    1    0   0.01   0.01   0.00   0.01
    8    5    0   0.07   0.01   0.00   0.07
    9    6    0   0.05   0.01   0.00   0.05
   10    7    0   0.04   0.00   0.00   0.03
   11   75    0   0.01   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.03   0.00   0.00   0.03
   14   47    0   0.02   0.00   0.00   0.02
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.020000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.5100s 
time spent in multnx:                   0.4900s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0800s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.5500s 

          starting ci iteration  23

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      5280 2x:      1480 4x:       370
All internal counts: zz :      5629 yy:     10577 xx:      2870 ww:      1774
One-external counts: yz :      9682 yx:     10444 yw:      8199
Two-external counts: yy :      4346 ww:       765 xx:      1375 xz:       818 wz:       810 wx:      1495
Three-ext.   counts: yx :      1415 yw:      1165

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:       715    task #   2:       649    task #   3:      1239    task #   4:      8526
task #   5:      8985    task #   6:      7000    task #   7:      4962    task #   8:      8696
task #   9:      2471    task #  10:      1552    task #  11:       865    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.98436215     0.00000010    -0.02007918    -0.00085832    -0.00226756     0.01028088     0.00547307
 ref:   2    -0.00000001    -0.98005773     0.00032007    -0.03272815     0.05230649    -0.05483170     0.00724210

 trial vector basis is being transformed.  new dimension:   4

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 23  1    -25.7583936770  4.9738E-14  0.0000E+00  7.9252E-06  1.0000E-05
 mr-sdci # 23  2    -25.7163571746  4.4471E-10  1.1152E-10  1.4275E-05  1.0000E-05
 mr-sdci # 23  3    -25.3945871347  1.2051E-05  0.0000E+00  2.7117E-01  1.0000E-04
 mr-sdci # 23  4    -25.3889438814  3.6571E-03  0.0000E+00  1.9712E-01  1.0000E-04
 mr-sdci # 23  5    -25.2536330218  1.8849E-01  0.0000E+00  4.8792E-01  1.0000E-04
 mr-sdci # 23  6    -24.4329314254  3.4420E-01  0.0000E+00  9.6806E-01  1.0000E-04
 mr-sdci # 23  7    -23.6554729347  7.4591E-02  0.0000E+00  1.1545E+00  1.0000E-04

 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.02   0.00   0.00   0.01
    2   25    0   0.01   0.01   0.00   0.01
    3   26    0   0.05   0.00   0.00   0.05
    4   11    0   0.03   0.01   0.00   0.03
    5   15    0   0.10   0.01   0.00   0.10
    6   16    0   0.08   0.01   0.00   0.08
    7    1    0   0.01   0.01   0.00   0.01
    8    5    0   0.07   0.01   0.00   0.07
    9    6    0   0.05   0.01   0.00   0.05
   10    7    0   0.04   0.00   0.00   0.04
   11   75    0   0.01   0.01   0.00   0.01
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.03   0.01   0.00   0.03
   14   47    0   0.02   0.00   0.00   0.02
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.050000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.5200s 
time spent in multnx:                   0.5100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0900s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.5800s 

          starting ci iteration  24

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      5280 2x:      1480 4x:       370
All internal counts: zz :      5629 yy:     10577 xx:      2870 ww:      1774
One-external counts: yz :      9682 yx:     10444 yw:      8199
Two-external counts: yy :      4346 ww:       765 xx:      1375 xz:       818 wz:       810 wx:      1495
Three-ext.   counts: yx :      1415 yw:      1165

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:       715    task #   2:       649    task #   3:      1239    task #   4:      8526
task #   5:      8985    task #   6:      7000    task #   7:      4962    task #   8:      8696
task #   9:      2471    task #  10:      1552    task #  11:       865    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     0.98436216     0.00000010     0.01991335    -0.00178532    -0.02730935
 ref:   2    -0.00000005     0.98005693     0.00254099     0.03737910     0.09887614

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 24  1    -25.7583936770  1.9540E-13  0.0000E+00  7.9196E-06  1.0000E-05
 mr-sdci # 24  2    -25.7163571747  8.8530E-11  3.0451E-11  5.8730E-06  1.0000E-05
 mr-sdci # 24  3    -25.3946140709  2.6936E-05  0.0000E+00  2.7088E-01  1.0000E-04
 mr-sdci # 24  4    -25.3917134390  2.7696E-03  0.0000E+00  1.8639E-01  1.0000E-04
 mr-sdci # 24  5    -24.1568193194 -1.0968E+00  0.0000E+00  1.1528E+00  1.0000E-04


 mr-sdci  convergence criteria satisfied after 24 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 24  1    -25.7583936770  1.9540E-13  0.0000E+00  7.9196E-06  1.0000E-05
 mr-sdci # 24  2    -25.7163571747  8.8530E-11  3.0451E-11  5.8730E-06  1.0000E-05
 mr-sdci # 24  3    -25.3946140709  2.6936E-05  0.0000E+00  2.7088E-01  1.0000E-04
 mr-sdci # 24  4    -25.3917134390  2.7696E-03  0.0000E+00  1.8639E-01  1.0000E-04
 mr-sdci # 24  5    -24.1568193194 -1.0968E+00  0.0000E+00  1.1528E+00  1.0000E-04

####################CIUDGINFO####################

   ci vector at position   1 energy=  -25.758393677015
   ci vector at position   2 energy=  -25.716357174652

################END OF CIUDGINFO################


    2 of the   6 expansion vectors are transformed.
    2 of the   5 matrix-vector products are transformed.

    2 expansion eigenvectors written to unit nvfile (= 11)
    2 matrix-vector products written to unit nhvfil (= 10)
 maximum overlap with reference  1(overlap=  0.984362163)

information on vector: 1from unit 11 written to unit 16filename civout                                                      
 maximum overlap with reference  2(overlap=  0.980056928)

information on vector: 2from unit 11 written to unit 16filename civout                                                      


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -25.7583936770

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7

                                          orbital     1    2    3    4    5    6   19

                                         symmetry   a'   a'   a'   a'   a'   a'   a" 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  2  1       1 -0.947322                        +-   +-   +-   +                 
 z*  2  1       3  0.074011                        +-   +-   +-             +       
 z*  2  1       5  0.119429                        +-   +-   +     -   +            
 z*  2  1       7 -0.101932                        +-   +-   +    +     -           
 z*  2  1      10 -0.024485                        +-   +-   +          -   +       
 z*  2  1      11 -0.024445                        +-   +-   +         +     -      
 z*  2  1      15 -0.036145                        +-   +-        +-        +       
 z*  2  1      16 -0.095674                        +-   +-        +    +-           
 z*  2  1      19 -0.036853                        +-   +-        +         +-      
 z*  2  1      20 -0.029346                        +-   +-        +              +- 
 z*  2  1      25 -0.073295                        +-   +    +-   +-                
 z*  2  1      27 -0.012190                        +-   +    +-    -        +       
 z*  2  1      29 -0.029154                        +-   +    +-   +          -      
 z*  2  1      33 -0.012176                        +-   +    +-             +-      
 z*  2  1      34  0.057484                        +-   +    +-                  +- 
 z*  2  1      35 -0.067485                        +-   +     -   +-   +            
 z*  2  1      38 -0.043797                        +-   +     -   +     -   +       
 z*  2  1      39 -0.071382                        +-   +     -   +    +     -      
 z*  2  1      46 -0.017010                        +-   +    +    +-    -           
 z*  2  1      49  0.016131                        +-   +    +     -    -   +       
 z*  2  1      77 -0.019738                        +-        +-   +-        +       
 z*  2  1      78 -0.033776                        +-        +-   +    +-           
 z*  2  1      81 -0.047732                        +-        +-   +         +-      
 z*  2  1      82 -0.040638                        +-        +-   +              +- 
 z*  2  1     109 -0.010454                        +-             +    +-   +-      
 y   2  1     120 -0.031779              5( a' )   +-   +-   +-                     
 y   2  1     121 -0.011689              6( a' )   +-   +-   +-                     
 y   2  1     122 -0.023080              7( a' )   +-   +-   +-                     
 y   2  1     124 -0.011966              9( a' )   +-   +-   +-                     
 y   2  1     129 -0.016389              2( a' )   +-   +-    -   +                 
 y   2  1     145  0.038553              6( a' )   +-   +-    -        +            
 y   2  1     166 -0.027165              3( a" )   +-   +-    -                  +  
 y   2  1     172 -0.012966              4( a' )   +-   +-   +     -                
 y   2  1     176 -0.025002              8( a' )   +-   +-   +     -                
 y   2  1     181 -0.015923              1( a' )   +-   +-   +          -           
 y   2  1     185  0.019648              5( a' )   +-   +-   +          -           
 y   2  1     207  0.014084              3( a" )   +-   +-   +                    - 
 y   2  1     215  0.010255              6( a' )   +-   +-        +-                
 y   2  1     362 -0.010774              3( a' )   +-    -   +-   +                 
 y   2  1     365 -0.011245              6( a' )   +-    -   +-   +                 
 y   2  1     373 -0.010748              2( a' )   +-    -   +-        +            
 y   2  1     397  0.010817              2( a" )   +-    -   +-                  +  
 y   2  1     418 -0.012843              6( a' )   +-    -   +     -   +            
 y   2  1     447  0.014623              6( a' )   +-    -   +    +     -           
 y   2  1     455  0.010060              2( a' )   +-    -   +    +          -      
 y   2  1     732  0.015408              5( a' )   +-   +    +-    -                
 y   2  1     747 -0.013443              8( a' )   +-   +    +-         -           
 y   2  1     752 -0.012862              1( a' )   +-   +    +-              -      
 y   2  1     764  0.011413              1( a" )   +-   +    +-                   - 
 y   2  1     765  0.020431              2( a" )   +-   +    +-                   - 
 y   2  1     794  0.011867              2( a' )   +-   +     -    -        +       
 y   2  1     800  0.012375              8( a' )   +-   +     -    -        +       
 y   2  1     807  0.020960              3( a" )   +-   +     -    -             +  
 y   2  1     823 -0.023193              2( a' )   +-   +     -   +          -      
 y   2  1     829 -0.017465              8( a' )   +-   +     -   +          -      
 y   2  1     836 -0.035720              3( a" )   +-   +     -   +               - 
 y   2  1    1176  0.010962              3( a' )   +-        +-   +-                
 y   2  1    1211 -0.013967              2( a" )   +-        +-    -             +  
 y   2  1    1229 -0.013658              3( a' )   +-        +-   +          -      
 y   2  1    1240  0.023234              2( a" )   +-        +-   +               - 
 x   2  1    2006  0.022324    1( a' )   2( a' )   +-   +-    -                     
 x   2  1    2013  0.014303    2( a' )   5( a' )   +-   +-    -                     
 x   2  1    2032  0.013439    6( a' )   8( a' )   +-   +-    -                     
 x   2  1    2074 -0.012443    2( a" )   3( a" )   +-   +-    -                     
 x   2  1    2448  0.017464    2( a' )   3( a' )   +-    -    -   +                 
 x   2  1    2457  0.010670    2( a' )   6( a' )   +-    -    -   +                 
 w   2  1    8287  0.016876    1( a' )   2( a' )   +-   +-   +                      
 w   2  1    8297 -0.015305    2( a' )   5( a' )   +-   +-   +                      
 w   2  1    8381  0.027512    2( a' )   2( a' )   +-   +-        +                 
 w   2  1    8384  0.013973    3( a' )   3( a' )   +-   +-        +                 
 w   2  1    8386 -0.018041    2( a' )   4( a' )   +-   +-        +                 
 w   2  1    8388  0.014294    4( a' )   4( a' )   +-   +-        +                 
 w   2  1    8399  0.027763    6( a' )   6( a' )   +-   +-        +                 
 w   2  1    8414  0.010476    8( a' )   8( a' )   +-   +-        +                 
 w   2  1    8462  0.019053    3( a" )   3( a" )   +-   +-        +                 
 w   2  1    8815  0.027641    2( a' )   3( a' )   +-   +     -   +                 
 w   2  1    8819 -0.024349    3( a' )   4( a' )   +-   +     -   +                 
 w   2  1    8844 -0.010922    6( a' )   8( a' )   +-   +     -   +                 
 w   2  1    8861  0.012689    6( a' )  10( a' )   +-   +     -   +                 
 w   2  1    8879 -0.011446    3( a' )  12( a' )   +-   +     -   +                 
 w   2  1    8893 -0.019162    2( a" )   3( a" )   +-   +     -   +                 
 w   2  1    8896  0.014148    2( a" )   4( a" )   +-   +     -   +                 
 w   2  1    8902 -0.011852    4( a" )   5( a" )   +-   +     -   +                 
 w   2  1   10784  0.022422    3( a' )   3( a' )   +-        +-   +                 
 w   2  1   10799  0.013632    6( a' )   6( a' )   +-        +-   +                 
 w   2  1   10859  0.018197    2( a" )   2( a" )   +-        +-   +                 

 ci coefficient statistics:
           rq > 0.1                3
      0.1> rq > 0.01              83
     0.01> rq > 0.001            819
    0.001> rq > 0.0001          3106
   0.0001> rq > 0.00001         2462
  0.00001> rq > 0.000001         479
 0.000001> rq                   7018
           all                 13970
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      1505 2x:         0 4x:         0
All internal counts: zz :      5629 yy:         0 xx:         0 ww:         0
One-external counts: yz :         0 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         0    task #   2:         0    task #   3:         0    task #   4:         0
task #   5:         0    task #   6:         0    task #   7:      4962    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:       865    task #  12:         0
task #  13:         0    task #  14:         0    task #
  iref  icsf         v(icsf)             hv(icsf)
     1     1     -0.947322289776     24.347476103515
     2     2      0.000001227289     -0.000031514749
     3     3      0.074011060664     -1.898145501152
     4     4      0.000006464014     -0.000166176422
     5     5      0.119429126051     -3.068162121632
     6     6     -0.000000979376      0.000025196239
     7     7     -0.101932116955      2.619063682267
     8     8     -0.000000008084      0.000000223724
     9     9     -0.000000735522      0.000018864586
    10    10     -0.024484590908      0.623820645451
    11    11     -0.024444746040      0.631247312595
    12    12     -0.000000273118      0.000007015639
    13    13     -0.000000232910      0.000005945113
    14    14     -0.000001195494      0.000030709226
    15    15     -0.036145147517      0.932408475149
    16    16     -0.095673798361      2.464694553836
    17    17      0.000000138474     -0.000003549974
    18    18      0.000000128311     -0.000003338710
    19    19     -0.036853100384      0.948890014535
    20    20     -0.029345547811      0.750159807690
    21    21     -0.008237111257      0.213728712915
    22    22     -0.000000005435      0.000000123704
    23    23      0.000000043190     -0.000001070885
    24    24     -0.004966689983      0.129129051225
    25    25     -0.073294635013      1.883143016370
    26    26     -0.000000369592      0.000009597195
    27    27     -0.012189537053      0.309979730360
    28    28     -0.000000063534      0.000001670764
    29    29     -0.029153840504      0.750188189169
    30    30      0.001709834627     -0.040249908420
    31    31     -0.000000078449      0.000002055124
    32    32     -0.000000347980      0.000008959233
    33    33     -0.012176171235      0.315167415328
    34    34      0.057484227497     -1.482352190884
    35    35     -0.067484519305      1.739357808832
    36    36      0.000000112640     -0.000002824254
    37    37      0.000000042256     -0.000001114697
    38    38     -0.043797360345      1.132432301483
    39    39     -0.071381696476      1.846770583734
    40    40      0.000000045015     -0.000001195334
    41    41     -0.000000252553      0.000006546221
    42    42      0.000000010836     -0.000000261634
    43    43     -0.008492400093      0.219581296237
    44    44     -0.005974242047      0.154927175495
    45    45      0.000000003425     -0.000000076591
    46    46     -0.017010412297      0.436324907541
    47    47      0.000000208037     -0.000005346667
    48    48      0.000000057190     -0.000001491532
    49    49      0.016131297714     -0.414352671877
    50    50      0.001854555014     -0.050458955802
    51    51      0.000000164974     -0.000004227810
    52    52     -0.000000534187      0.000013740900
    53    53      0.007672830313     -0.195177549259
    54    54     -0.000000048522      0.000001284104
    55    55     -0.003161175786      0.080866806738
    56    56      0.004695520662     -0.122576455153
    57    57     -0.000000023326      0.000000562965
    58    58      0.007203870528     -0.185043925744
    59    59      0.000000101432     -0.000002608348
    60    60      0.000000374617     -0.000009605165
    61    61      0.004184404651     -0.108322348420
    62    62      0.000116575304     -0.002140342768
    63    63     -0.000378106491      0.009140291463
    64    64      0.000000058277     -0.000001491087
    65    65      0.000000018960     -0.000000521532
    66    66     -0.001340264789      0.034895595422
    67    67     -0.008492349775      0.220217350349
    68    68      0.000000023925     -0.000000601047
    69    69     -0.000000044542      0.000001140570
    70    70     -0.000063890474      0.001434438078
    71    71      0.003405550322     -0.088103158856
    72    72     -0.005916978168      0.153454078219
    73    73      0.000000009493     -0.000000219545
    74    74     -0.000000035009      0.000000850693
    75    75     -0.002099809208      0.054385545967
    76    76      0.000000037659     -0.000000938166
    77    77     -0.019738350669      0.511208281080
    78    78     -0.033776057437      0.873410381033
    79    79      0.000000033994     -0.000000898368
    80    80      0.000000009840     -0.000000242578
    81    81     -0.047732180946      1.231416646402
    82    82     -0.040637941274      1.041779862843
    83    83     -0.001939329638      0.050624094925
    84    84      0.000000065822     -0.000001702975
    85    85      0.000000012586     -0.000000322833
    86    86     -0.001455240865      0.037835587603
    87    87      0.000000254452     -0.000006544761
    88    88      0.004619114338     -0.120655656198
    89    89     -0.001313832233      0.034378886850
    90    90      0.000000354864     -0.000009102709
    91    91      0.000000273653     -0.000007025425
    92    92      0.000000095821     -0.000002478828
    93    93      0.006192164029     -0.160714850202
    94    94      0.000911675908     -0.022320652540
    95    95     -0.000000005964      0.000000138840
    96    96      0.000000003880     -0.000000095827
    97    97     -0.004490655499      0.116548699184
    98    98     -0.002485454097      0.063075279299
    99    99      0.000000008346     -0.000000196039
   100   100     -0.000000069260      0.000001817660
   101   101     -0.000000041193      0.000001059533
   102   102     -0.004288528934      0.111251530393
   103   103     -0.003447767991      0.089379448508
   104   104     -0.000000043703      0.000001087079
   105   105      0.007756073216     -0.201568927284
   106   106     -0.000000066191      0.000001693997
   107   107     -0.000000005897      0.000000154358
   108   108     -0.001813492028      0.046571640054
   109   109     -0.010454145915      0.271411931372
   110   110     -0.005309099133      0.136684310381
   111   111      0.000000028675     -0.000000745341
   112   112      0.000000018743     -0.000000492533
   113   113     -0.003164563202      0.081439690687
   114   114      0.001055307617     -0.027492535311
   115   115     -0.000000014834      0.000000336260

 number of reference csfs (nref) is   115.  root number (iroot) is  1.

 pople ci energy extrapolation is computed with  7 correlated electrons.

 eref      =    -25.703701602375   "relaxed" cnot**2         =   0.969656670286
 eci       =    -25.758393677015   deltae = eci - eref       =  -0.054692074641
 eci+dv1   =    -25.760053216669   dv1 = (1-cnot**2)*deltae  =  -0.001659539654
 eci+dv2   =    -25.760105148410   dv2 = dv1 / cnot**2       =  -0.001711471394
 eci+dv3   =    -25.760160435329   dv3 = dv1 / (2*cnot**2-1) =  -0.001766758314
 eci+pople =    -25.759632520095   (  7e- scaled deltae )    =  -0.055930917721


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 2) =       -25.7163571747

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7

                                          orbital     1    2    3    4    5    6   19

                                         symmetry   a'   a'   a'   a'   a'   a'   a" 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  2  1       2 -0.034306                        +-   +-   +-        +            
 z*  2  1       4  0.938105                        +-   +-   +    +-                
 z*  2  1       6  0.090526                        +-   +-   +     -        +       
 z*  2  1       8  0.048117                        +-   +-   +    +          -      
 z*  2  1       9 -0.083171                        +-   +-   +         +-           
 z*  2  1      12 -0.039232                        +-   +-   +              +-      
 z*  2  1      13 -0.177859                        +-   +-   +                   +- 
 z*  2  1      14  0.033594                        +-   +-        +-   +            
 z*  2  1      18 -0.050524                        +-   +-        +    +     -      
 z*  2  1      26  0.092421                        +-   +    +-    -   +            
 z*  2  1      28  0.044599                        +-   +    +-   +     -           
 z*  2  1      36  0.045274                        +-   +     -   +-        +       
 z*  2  1      40 -0.010078                        +-   +     -   +         +-      
 z*  2  1      41  0.027876                        +-   +     -   +              +- 
 z*  2  1      45  0.012326                        +-   +     -             +    +- 
 z*  2  1      47  0.017649                        +-   +    +    +-         -      
 z*  2  1      51  0.018353                        +-   +    +     -        +-      
 z*  2  1      52 -0.060267                        +-   +    +     -             +- 
 z*  2  1      59  0.013400                        +-   +         +-    -   +       
 z*  2  1      60  0.052636                        +-   +         +-   +     -      
 z*  2  1      64 -0.013512                        +-   +          -   +    +-      
 z*  2  1      74 -0.013084                        +-   +              +     -   +- 
 z*  2  1      76  0.028034                        +-        +-   +-   +            
 z*  2  1      85  0.011395                        +-        +-        +         +- 
 z*  2  1      87  0.031213                        +-        +    +-   +-           
 z*  2  1      90  0.043965                        +-        +    +-        +-      
 z*  2  1      91  0.035093                        +-        +    +-             +- 
 z*  2  1     104 -0.011445                        +-        +              +-   +- 
 y   2  1     123  0.021656              8( a' )   +-   +-   +-                     
 y   2  1     128 -0.014822              1( a' )   +-   +-    -   +                 
 y   2  1     132  0.024114              5( a' )   +-   +-    -   +                 
 y   2  1     134  0.015063              7( a' )   +-   +-    -   +                 
 y   2  1     147 -0.018543              8( a' )   +-   +-    -        +            
 y   2  1     156 -0.013761              5( a' )   +-   +-    -             +       
 y   2  1     157 -0.010273              6( a' )   +-   +-    -             +       
 y   2  1     165  0.025276              2( a" )   +-   +-    -                  +  
 y   2  1     173 -0.034313              5( a' )   +-   +-   +     -                
 y   2  1     174 -0.034271              6( a' )   +-   +-   +     -                
 y   2  1     175 -0.017474              7( a' )   +-   +-   +     -                
 y   2  1     177 -0.011524              9( a' )   +-   +-   +     -                
 y   2  1     188  0.028004              8( a' )   +-   +-   +          -           
 y   2  1     197  0.024751              5( a' )   +-   +-   +               -      
 y   2  1     198  0.015271              6( a' )   +-   +-   +               -      
 y   2  1     199  0.014601              7( a' )   +-   +-   +               -      
 y   2  1     205 -0.012136              1( a" )   +-   +-   +                    - 
 y   2  1     206 -0.037378              2( a" )   +-   +-   +                    - 
 y   2  1     211 -0.013110              2( a' )   +-   +-        +-                
 y   2  1     217 -0.014345              8( a' )   +-   +-        +-                
 y   2  1     222  0.011053              1( a' )   +-   +-         -   +            
 y   2  1     226 -0.027838              5( a' )   +-   +-         -   +            
 y   2  1     227 -0.012466              6( a' )   +-   +-         -   +            
 y   2  1     230  0.010680              9( a' )   +-   +-         -   +            
 y   2  1     256 -0.028842              6( a' )   +-   +-        +     -           
 y   2  1     277  0.026380              3( a" )   +-   +-        +               - 
 y   2  1     403 -0.011773              3( a' )   +-    -   +    +-                
 y   2  1     406 -0.010427              6( a' )   +-    -   +    +-                
 y   2  1     443  0.010818              2( a' )   +-    -   +    +     -           
 y   2  1     454  0.011056              1( a' )   +-    -   +    +          -      
 y   2  1     458 -0.012351              5( a' )   +-    -   +    +          -      
 y   2  1     467 -0.012393              2( a" )   +-    -   +    +               - 
 y   2  1     556  0.010610              6( a' )   +-    -        +-   +            
 y   2  1     771 -0.017090              3( a' )   +-   +     -   +-                
 y   2  1     773  0.010037              5( a' )   +-   +     -   +-                
 y   2  1     774  0.044963              6( a' )   +-   +     -   +-                
 y   2  1     775  0.015500              7( a' )   +-   +     -   +-                
 y   2  1     788  0.010737              8( a' )   +-   +     -    -   +            
 y   2  1     806 -0.010555              2( a" )   +-   +     -    -             +  
 y   2  1     926  0.013042              8( a' )   +-   +    +     -    -           
 y   2  1     931  0.011028              1( a' )   +-   +    +     -         -      
 y   2  1     935 -0.014573              5( a' )   +-   +    +     -         -      
 y   2  1     943 -0.015706              1( a" )   +-   +    +     -              - 
 y   2  1     944 -0.015996              2( a" )   +-   +    +     -              - 
 y   2  1     983  0.014132              2( a' )   +-   +         +-         -      
 y   2  1     989  0.018361              8( a' )   +-   +         +-         -      
 y   2  1     996  0.025853              3( a" )   +-   +         +-              - 
 y   2  1    1175 -0.018810              2( a' )   +-        +-   +-                
 y   2  1    1177  0.021993              4( a' )   +-        +-   +-                
 y   2  1    1349  0.013266              2( a" )   +-         -   +-             +  
 y   2  1    1515  0.013819              3( a' )   +-        +    +-         -      
 y   2  1    1526 -0.022969              2( a" )   +-        +    +-              - 
 x   2  1    2082 -0.013407    1( a' )   2( a' )   +-   +-         -                
 x   2  1    2089 -0.012502    2( a' )   5( a' )   +-   +-         -                
 x   2  1    3024 -0.010071    2( a' )   3( a' )   +-    -        +-                
 w   2  1    8286  0.029729    1( a' )   1( a' )   +-   +-   +                      
 w   2  1    8296 -0.032836    1( a' )   5( a' )   +-   +-   +                      
 w   2  1    8300  0.025378    5( a' )   5( a' )   +-   +-   +                      
 w   2  1    8306  0.010087    6( a' )   6( a' )   +-   +-   +                      
 w   2  1    8321  0.013871    8( a' )   8( a' )   +-   +-   +                      
 w   2  1    8366  0.013439    2( a" )   2( a" )   +-   +-   +                      
 w   2  1    8380  0.013206    1( a' )   2( a' )   +-   +-        +                 
 w   2  1    8387 -0.010388    3( a' )   4( a' )   +-   +-        +                 
 w   2  1    8390 -0.014249    2( a' )   5( a' )   +-   +-        +                 
 w   2  1    9162  0.010084    3( a' )   5( a' )   +-   +    +     -                
 w   2  1    9493 -0.018258    2( a' )   3( a' )   +-   +         +-                
 w   2  1    9497  0.019482    3( a' )   4( a' )   +-   +         +-                
 w   2  1    9571  0.013738    2( a" )   3( a" )   +-   +         +-                
 w   2  1    9574 -0.011921    2( a" )   4( a" )   +-   +         +-                
 w   2  1   11123 -0.021917    3( a' )   3( a' )   +-        +    +-                
 w   2  1   11138 -0.014712    6( a' )   6( a' )   +-        +    +-                
 w   2  1   11198 -0.018074    2( a" )   2( a" )   +-        +    +-                

 ci coefficient statistics:
           rq > 0.1                2
      0.1> rq > 0.01              98
     0.01> rq > 0.001            946
    0.001> rq > 0.0001          3289
   0.0001> rq > 0.00001         2200
  0.00001> rq > 0.000001         393
 0.000001> rq                   7042
           all                 13970
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:      1505 2x:         0 4x:         0
All internal counts: zz :      5629 yy:         0 xx:         0 ww:         0
One-external counts: yz :         0 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         0    task #   2:         0    task #   3:         0    task #   4:         0
task #   5:         0    task #   6:         0    task #   7:      4962    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:       865    task #  12:         0
task #  13:         0    task #  14:         0    task #
  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.000006042780     -0.000155290887
     2     2     -0.034305966678      0.877698952254
     3     3      0.000000495717     -0.000013067025
     4     4      0.938104876642    -24.063330391378
     5     5      0.000000090365     -0.000002660381
     6     6      0.090526370655     -2.322605920854
     7     7      0.000000443735     -0.000011044288
     8     8      0.048117442247     -1.228107106542
     9     9     -0.083170649785      2.128629746615
    10    10      0.000000178906     -0.000004280389
    11    11     -0.000000256024      0.000006666560
    12    12     -0.039231936749      0.997386433240
    13    13     -0.177859412368      4.568974143560
    14    14      0.033593556653     -0.868588072360
    15    15     -0.000000718384      0.000018518688
    16    16      0.000001047031     -0.000026361049
    17    17      0.005541032656     -0.140798782770
    18    18     -0.050524294330      1.297611838362
    19    19      0.000000462165     -0.000011520168
    20    20      0.000001318358     -0.000033805246
    21    21     -0.000000015047      0.000000296084
    22    22      0.005459300094     -0.138899731402
    23    23     -0.001068332159      0.028287873325
    24    24      0.000000002050     -0.000000073334
    25    25      0.000000288261     -0.000007240297
    26    26      0.092421352277     -2.378877106563
    27    27      0.000000399313     -0.000010220930
    28    28      0.044598798188     -1.146646727408
    29    29      0.000000304993     -0.000007641572
    30    30      0.000000001184     -0.000000052187
    31    31     -0.006533189737      0.171006732087
    32    32     -0.006318967703      0.166594197939
    33    33      0.000000172571     -0.000004309271
    34    34     -0.000000594942      0.000015182160
    35    35     -0.000000400698      0.000010521541
    36    36      0.045273980640     -1.172855553736
    37    37      0.000513119982     -0.009122484026
    38    38      0.000000257838     -0.000006481621
    39    39      0.000000579208     -0.000014582254
    40    40     -0.010077686700      0.260460716519
    41    41      0.027876358024     -0.717613874312
    42    42     -0.005566937057      0.142353440054
    43    43     -0.000000089076      0.000002228234
    44    44     -0.000000012734      0.000000313600
    45    45      0.012325821147     -0.317651932493
    46    46     -0.000000184872      0.000004921236
    47    47      0.017648789649     -0.458946961572
    48    48      0.000707253575     -0.021279770636
    49    49      0.000000107201     -0.000002880767
    50    50      0.000000518729     -0.000013319682
    51    51      0.018352950616     -0.473008907389
    52    52     -0.060267050210      1.552597084241
    53    53     -0.000000077407      0.000001780558
    54    54     -0.003060956033      0.080943832564
    55    55     -0.000000025647      0.000000617462
    56    56     -0.000000014959      0.000000388345
    57    57     -0.000523884069      0.009470703342
    58    58     -0.000000026073      0.000000709439
    59    59      0.013399950864     -0.348628212682
    60    60      0.052635912941     -1.359710231378
    61    61     -0.000000099914      0.000002593819
    62    62      0.000000368788     -0.000009512553
    63    63      0.000000039684     -0.000001021606
    64    64     -0.013511928600      0.348809992309
    65    65     -0.003059947624      0.078753287340
    66    66     -0.000000079132      0.000002036055
    67    67      0.000000029066     -0.000000825850
    68    68     -0.003842718046      0.098330979651
    69    69     -0.000715054374      0.017042880662
    70    70     -0.000000003538      0.000000117263
    71    71      0.000000002119     -0.000000008357
    72    72      0.000000009234     -0.000000270245
    73    73     -0.001849639652      0.048055170900
    74    74     -0.013084166639      0.339210608664
    75    75     -0.000000000836      0.000000009114
    76    76      0.028033731814     -0.724376516452
    77    77      0.000000074182     -0.000001804444
    78    78      0.000000339821     -0.000008577596
    79    79     -0.001482969762      0.039061169381
    80    80      0.008047152868     -0.207113605653
    81    81      0.000000510972     -0.000012760127
    82    82      0.000000421728     -0.000010604140
    83    83     -0.000000046338      0.000001192888
    84    84     -0.003197960066      0.082796878606
    85    85      0.011395376478     -0.294980252612
    86    86      0.000000056643     -0.000001469044
    87    87      0.031213167043     -0.805205684583
    88    88     -0.000000004101      0.000000120125
    89    89     -0.000000063280      0.000001624054
    90    90      0.043964597561     -1.131988900125
    91    91      0.035092916789     -0.896851169688
    92    92     -0.005645420549      0.145987947936
    93    93      0.000000101184     -0.000002569527
    94    94     -0.000000080152      0.000002087375
    95    95      0.005823588785     -0.149894308495
    96    96     -0.002668798294      0.068664828134
    97    97      0.000000002574     -0.000000112603
    98    98     -0.000000006093      0.000000145537
    99    99      0.000832730726     -0.020554304769
   100   100     -0.006115117496      0.157695241074
   101   101     -0.009923032334      0.256101590333
   102   102      0.000000001004     -0.000000051750
   103   103     -0.000000019694      0.000000492016
   104   104     -0.011445257226      0.295509879592
   105   105      0.000000044323     -0.000001098586
   106   106      0.007565933359     -0.195979733063
   107   107      0.002900730813     -0.074774180592
   108   108     -0.000000051986      0.000001332290
   109   109      0.000000052152     -0.000001437098
   110   110      0.000000075196     -0.000001970831
   111   111     -0.000559443626      0.014688620819
   112   112     -0.003891536039      0.100548913085
   113   113      0.000000081023     -0.000002116182
   114   114     -0.000000005523      0.000000134124
   115   115      0.002252256171     -0.058240613758

 number of reference csfs (nref) is   115.  root number (iroot) is  2.

 pople ci energy extrapolation is computed with  7 correlated electrons.

 eref      =    -25.654790719861   "relaxed" cnot**2         =   0.962466989768
 eci       =    -25.716357174652   deltae = eci - eref       =  -0.061566454791
 eci+dv1   =    -25.718667949029   dv1 = (1-cnot**2)*deltae  =  -0.002310774378
 eci+dv2   =    -25.718758061542   dv2 = dv1 / cnot**2       =  -0.002400886890
 eci+dv3   =    -25.718855487427   dv3 = dv1 / (2*cnot**2-1) =  -0.002498312776
 eci+pople =    -25.718100682737   (  7e- scaled deltae )    =  -0.063309962877
NO coefficients and occupation numbers are written to nocoef_ci.1

################################################################################
 one electron density matrix calculation 
################################################################################


 workspace allocation information: mxcore= 2999492

 space required:

    space required for calls in multd2:
       onex          216
       allin           0
       diagon        465
    max.      ---------
       maxnex        465

    total core space usage:
       maxnex        465
    max k-seg       6280
    max k-ind        210
              ---------
       totmax      13445
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=     692  DYX=     815  DYW=     670
   D0Z=     740  D0Y=    1405  D0X=     475  D0W=     310
  DDZI=     530 DDYI=     900 DDXI=     330 DDWI=     250
  DDZE=       0 DDYE=     210 DDXE=      90 DDWE=      70
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   2.0000000      1.9481529      1.9255560      0.9932031      0.0497876
   0.0255752      0.0086811      0.0068693      0.0046490      0.0039818
   0.0025163      0.0011282      0.0009819      0.0006099      0.0004795
   0.0004091      0.0001071      0.0000529


*****   symmetry block SYM2   *****

 occupation numbers of nos
   0.0179518      0.0050413      0.0031613      0.0006676      0.0003480
   0.0000891


 total number of electrons =    7.0000000000



          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!


                        A'  partial gross atomic populations
   ao class       1A'        2A'        3A'        4A'        5A'        6A' 
    B1_ s       1.999928   0.826255   0.000000   0.423651   0.000000   0.004424
    B1_ p       0.000004   0.191839   0.943586   0.494152   0.025505   0.009148
    B1_ d      -0.000001   0.005204   0.024217  -0.000741   0.000888   0.000404
    H1_ s       0.000086   0.453960   0.475258   0.036636   0.011670   0.005362
    H1_ p      -0.000052   0.008467   0.003618   0.001434   0.000027   0.000437
    H2_ s       0.000086   0.453960   0.475258   0.036636   0.011670   0.005362
    H2_ p      -0.000052   0.008467   0.003618   0.001434   0.000027   0.000437

   ao class       7A'        8A'        9A'       10A'       11A'       12A' 
    B1_ s       0.000000   0.000369   0.000012   0.002707   0.000000   0.000109
    B1_ p       0.003164   0.000358   0.001086   0.000719   0.000007  -0.000008
    B1_ d       0.001031   0.003765   0.000323   0.000451   0.001614   0.000500
    H1_ s       0.001663   0.000163   0.001068   0.000045   0.000312  -0.000007
    H1_ p       0.000580   0.001026   0.000546   0.000008   0.000135   0.000270
    H2_ s       0.001663   0.000163   0.001068   0.000045   0.000312  -0.000007
    H2_ p       0.000580   0.001026   0.000546   0.000008   0.000135   0.000270

   ao class      13A'       14A'       15A'       16A'       17A'       18A' 
    B1_ s       0.000000   0.000000  -0.000006   0.000035   0.000000   0.000004
    B1_ p       0.000486   0.000018   0.000025   0.000115   0.000011   0.000013
    B1_ d      -0.000003   0.000024   0.000301   0.000022   0.000019   0.000007
    H1_ s       0.000079   0.000007   0.000054  -0.000008   0.000012   0.000009
    H1_ p       0.000171   0.000277   0.000026   0.000126   0.000027   0.000006
    H2_ s       0.000079   0.000007   0.000054  -0.000008   0.000012   0.000009
    H2_ p       0.000171   0.000277   0.000026   0.000126   0.000027   0.000006

                        A"  partial gross atomic populations
   ao class       1A"        2A"        3A"        4A"        5A"        6A" 
    B1_ p       0.016630   0.000000   0.000196   0.000000   0.000069   0.000072
    B1_ d       0.000229   0.003775   0.001927   0.000168   0.000115   0.000004
    H1_ p       0.000546   0.000633   0.000519   0.000250   0.000082   0.000006
    H2_ p       0.000546   0.000633   0.000519   0.000250   0.000082   0.000006


                        gross atomic populations
     ao           B1_        H1_        H2_
      s         3.257487   0.986370   0.986370
      p         1.687196   0.019167   0.019167
      d         0.044243   0.000000   0.000000
    total       4.988926   1.005537   1.005537


 Total number of electrons:    7.00000000

NO coefficients and occupation numbers are written to nocoef_ci.2
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    2
--------------------------------------------------------------------------------
================================================================================
   DYZ=     692  DYX=     815  DYW=     670
   D0Z=     740  D0Y=    1405  D0X=     475  D0W=     310
  DDZI=     530 DDYI=     900 DDXI=     330 DDWI=     250
  DDZE=       0 DDYE=     210 DDXE=      90 DDWE=      70
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   2.0000000      1.9455630      1.8576652      0.9990086      0.0421499
   0.0272326      0.0097724      0.0071368      0.0049738      0.0046422
   0.0031975      0.0012506      0.0009195      0.0008017      0.0005977
   0.0003682      0.0001330      0.0001121


*****   symmetry block SYM2   *****

 occupation numbers of nos
   0.0859851      0.0042810      0.0026484      0.0007382      0.0005835
   0.0002393


 total number of electrons =    7.0000000000



          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!


                        A'  partial gross atomic populations
   ao class       1A'        2A'        3A'        4A'        5A'        6A' 
    B1_ s       1.999928   0.381937   1.379467   0.000000   0.000000   0.000558
    B1_ p       0.000004   0.355474   0.477685   0.350877   0.024963   0.014849
    B1_ d      -0.000001   0.014267   0.011613   0.024844   0.004746   0.000624
    H1_ s       0.000086   0.586592  -0.008752   0.308046   0.006090   0.005032
    H1_ p      -0.000052   0.010351   0.003201   0.003598   0.000130   0.000569
    H2_ s       0.000086   0.586591  -0.008751   0.308046   0.006090   0.005032
    H2_ p      -0.000052   0.010351   0.003201   0.003598   0.000130   0.000569

   ao class       7A'        8A'        9A'       10A'       11A'       12A' 
    B1_ s       0.005523   0.000329   0.000000   0.000835   0.000000  -0.000041
    B1_ p       0.002119   0.000756   0.001154   0.000246   0.000035   0.000055
    B1_ d       0.000497   0.004024   0.000227   0.000895   0.002138   0.000309
    H1_ s       0.000557   0.000012   0.001311   0.001002   0.000436   0.000143
    H1_ p       0.000259   0.001002   0.000485   0.000332   0.000076   0.000321
    H2_ s       0.000557   0.000012   0.001311   0.001002   0.000436   0.000143
    H2_ p       0.000259   0.001002   0.000485   0.000332   0.000076   0.000321

   ao class      13A'       14A'       15A'       16A'       17A'       18A' 
    B1_ s       0.000010   0.000000   0.000174   0.000000   0.000000  -0.000002
    B1_ p       0.000005   0.000532   0.000320   0.000019   0.000013   0.000006
    B1_ d       0.000690  -0.000005   0.000049   0.000012   0.000016   0.000009
    H1_ s       0.000056   0.000019  -0.000047   0.000004   0.000015   0.000017
    H1_ p       0.000051   0.000119   0.000074   0.000165   0.000037   0.000033
    H2_ s       0.000056   0.000019  -0.000047   0.000004   0.000015   0.000017
    H2_ p       0.000051   0.000119   0.000074   0.000165   0.000037   0.000033

                        A"  partial gross atomic populations
   ao class       1A"        2A"        3A"        4A"        5A"        6A" 
    B1_ p       0.083031   0.000023   0.000000   0.000510   0.000000   0.000081
    B1_ d       0.002371   0.002960   0.002032   0.000052   0.000136   0.000050
    H1_ p       0.000291   0.000649   0.000308   0.000088   0.000224   0.000054
    H2_ p       0.000291   0.000649   0.000308   0.000088   0.000224   0.000054


                        gross atomic populations
     ao           B1_        H1_        H2_
      s         3.768718   0.900618   0.900618
      p         1.312758   0.022366   0.022366
      d         0.072555   0.000000   0.000000
    total       5.154031   0.922984   0.922984


 Total number of electrons:    7.00000000

