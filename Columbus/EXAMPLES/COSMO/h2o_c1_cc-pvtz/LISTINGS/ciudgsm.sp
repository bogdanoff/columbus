1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 integral file titles:
 Hermit Integral Program : SIFS version  hochtor2        Thu Oct  7 15:43:44 2004
  cidrt_title                                                                    
  title                                                                          
 mofmt: formatted orbitals label=morbl   hochtor2        Thu Oct  7 15:43:52 2004
 SIFS file created by program tran.      hochtor2        Thu Oct  7 15:43:52 2004

 formula file title:
 Hermit Integral Program : SIFS version  hochtor2        Thu Oct  7 15:43:44 2004
  cidrt_title                                                                    
  title                                                                          
 mofmt: formatted orbitals label=morbl   hochtor2        Thu Oct  7 15:43:52 2004
 SIFS file created by program tran.      hochtor2        Thu Oct  7 15:43:52 2004
  cidrt_title                                                                    

 297 dimension of the ci-matrix ->>>     35511


 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -76.0788758318 -9.2968E+00  3.2342E-01  1.4769E+00  1.0000E-03
 mr-sdci #  2  1    -76.3394101013 -9.0858E+00  9.6456E-03  2.5182E-01  1.0000E-03
 mr-sdci #  3  1    -76.3477351952 -9.3380E+00  3.5524E-04  3.8982E-02  1.0000E-03
 mr-sdci #  4  1    -76.3480643824 -9.3460E+00  2.6191E-05  1.0864E-02  1.0000E-03
 mr-sdci #  5  1    -76.3480898860 -9.3463E+00  1.4350E-06  3.0284E-03  1.0000E-03
 mr-sdci #  6  1    -76.3480913128 -9.3463E+00  4.1864E-08  4.6521E-04  1.0000E-03

 mr-sdci  convergence criteria satisfied after  6 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  6  1    -76.3480913128 -9.3463E+00  4.1864E-08  4.6521E-04  1.0000E-03

 number of reference csfs (nref) is     1.  root number (iroot) is  1.

 eref      =    -76.078875831756   "relaxed" cnot**2         =   0.945825996767
 eci       =    -76.348091312767   deltae = eci - eref       =  -0.269215481011
 eci+dv1   =    -76.362675793105   dv1 = (1-cnot**2)*deltae  =  -0.014584480339
 eci+dv2   =    -76.363511147270   dv2 = dv1 / cnot**2       =  -0.015419834503
 eci+dv3   =    -76.364448008478   dv3 = dv1 / (2*cnot**2-1) =  -0.016356695711
 eci+pople =    -76.360859020974   ( 10e- scaled deltae )    =  -0.281983189218
