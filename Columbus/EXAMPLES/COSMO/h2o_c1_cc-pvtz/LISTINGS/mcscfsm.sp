 total ao core energy =    9.317913917
 MCSCF calculation performed for  1 DRT.

 DRT  first state   no.of aver. states   weights
  1   ground state          1             1.000

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a  
 Total number of electrons:    10
 Spin multiplicity:            1
 Number of active orbitals:    0
 Number of active electrons:   0
 Total number of CSFs:         1

 Number of active-double rotations:      0
 Number of active-active rotations:      0
 Number of double-virtual rotations:   265
 Number of active-virtual rotations:     0

 iter=    1 emc=  -76.0577322543 demc= 7.6058E+01 wnorm= 1.1762E-09 knorm= 1.0889E-10 apxde= 3.5019E-16    *not converged* 

 final mcscf convergence values:
 iter=    2 emc=  -76.0577322543 demc=-4.2633E-14 wnorm= 1.3984E-09 knorm= 2.0138E-11 apxde=-1.2309E-15    *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 weight 1.000000 total energy=  -76.057732254
   ------------------------------------------------------------


