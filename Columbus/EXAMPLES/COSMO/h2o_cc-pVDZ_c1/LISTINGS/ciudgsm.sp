1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 integral file titles:
 Hermit Integral Program : SIFS version  hochtor2        Thu Oct  7 14:40:51 2004
  cidrt_title                                                                    
  title                                                                          
 mofmt: formatted orbitals label=morbl   hochtor2        Thu Oct  7 14:40:52 2004
 SIFS file created by program tran.      hochtor2        Thu Oct  7 14:40:52 2004

 formula file title:
 Hermit Integral Program : SIFS version  hochtor2        Thu Oct  7 14:40:51 2004
  cidrt_title                                                                    
  title                                                                          
 mofmt: formatted orbitals label=morbl   hochtor2        Thu Oct  7 14:40:52 2004
 SIFS file created by program tran.      hochtor2        Thu Oct  7 14:40:52 2004
  cidrt_title                                                                    

 297 dimension of the ci-matrix ->>>      4656


 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -76.0479455968 -9.2970E+00  2.5188E-01  1.0015E+00  1.0000E-03
 mr-sdci #  2  1    -76.2462585708 -9.1546E+00  6.4774E-03  1.5760E-01  1.0000E-03
 mr-sdci #  3  1    -76.2535668738 -9.3457E+00  5.4631E-04  3.4900E-02  1.0000E-03
 mr-sdci #  4  1    -76.2533746201 -9.3555E+00  7.9397E-05  1.6304E-02  1.0000E-03
 mr-sdci #  5  1    -76.2536306957 -9.3527E+00  2.3011E-05  7.1128E-03  1.0000E-03
 mr-sdci #  6  1    -76.2536153391 -9.3534E+00  4.5589E-06  3.7742E-03  1.0000E-03
 mr-sdci #  7  1    -76.2536324369 -9.3533E+00  1.4294E-06  1.7508E-03  1.0000E-03
 mr-sdci #  8  1    -76.2536282771 -9.3533E+00  2.9676E-07  9.4770E-04  1.0000E-03

 mr-sdci  convergence criteria satisfied after  8 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  8  1    -76.2536282771 -9.3533E+00  2.9676E-07  9.4770E-04  1.0000E-03

 number of reference csfs (nref) is     1.  root number (iroot) is  1.

 eref      =    -76.049077211029   "relaxed" cnot**2         =   0.951678966099
 eci       =    -76.253628277133   deltae = eci - eref       =  -0.204551066104
 eci+dv1   =    -76.263512396132   dv1 = (1-cnot**2)*deltae  =  -0.009884119000
 eci+dv2   =    -76.264014257439   dv2 = dv1 / cnot**2       =  -0.010385980306
 eci+dv3   =    -76.264569808333   dv3 = dv1 / (2*cnot**2-1) =  -0.010941531200
 eci+pople =    -76.262194382415   ( 10e- scaled deltae )    =  -0.213117171385
