
 DALTON: user specified work memory size used,
          environment variable WRKMEM = "10000000            "

 Work memory size (LMWORK) :    10000000 =   76.29 megabytes.

 Default basis set library used :
        /sphome/kedziora/dalton/basis/                              


    $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    $$$$$$$$$$$  DALTON - An electronic structure program  $$$$$$$$$$$
    $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

               This is output from DALTON (beta-version 0.9) 

                          Principal authors:

            Trygve Helgaker,     University of Oslo,        Norway 
            Hans Joergen Jensen, University of Odense,      Denmark
            Poul Joergensen,     University of Aarhus,      Denmark
            Henrik Koch,         University of Aarhus,      Denmark
            Jeppe Olsen,         University of Lund,        Sweden 
            Hans Aagren,         University of Linkoeping,  Sweden 

                          Contributors:

            Torgeir Andersen,    University of Oslo,        Norway 
            Keld L. Bak,         University of Copenhagen,  Denmark
            Vebjoern Bakken,     University of Oslo,        Norway 
            Ove Christiansen,    University of Aarhus,      Denmark
            Paal Dahle,          University of Oslo,        Norway 
            Erik K. Dalskov,     University of Odense,      Denmark
            Thomas Enevoldsen,   University of Odense,      Denmark
            Asger Halkier,       University of Aarhus,      Denmark
            Hanne Heiberg,       University of Oslo,        Norway 
            Dan Jonsson,         University of Linkoeping,  Sweden 
            Sheela Kirpekar,     University of Odense,      Denmark
            Rika Kobayashi,      University of Aarhus,      Denmark
            Alfredo S. de Meras, Valencia University,       Spain  
            Kurt Mikkelsen,      University of Aarhus,      Denmark
            Patrick Norman,      University of Linkoeping,  Sweden 
            Martin J. Packer,    University of Sheffield,   UK     
            Kenneth Ruud,        University of Oslo,        Norway 
            Trond Saue,          University of Oslo,        Norway 
            Peter Taylor,        San Diego Superc. Center,  USA    
            Olav Vahtras,        University of Linkoeping,  Sweden

                                             Release Date:  August 1996
------------------------------------------------------------------------


      
     NOTE:
      
     This is an experimental code for the evaluation of molecular
     properties using (MC)SCF/CC wave functions. The authors accept
      no responsibility for the performance of the code or for the
     correctness of the results.
      
     The code (in whole or part) is not to be reproduced for further
     distribution without the written permission of T. Helgaker,
     H. J. Aa. Jensen or P. Taylor.
      
     If results obtained with this code are published, an
     appropriate citation would be:
      
     T. Helgaker, H. J. Aa. Jensen, P.Joergensen, H. Koch,
     J. Olsen, H. Aagren, T. Andersen, K. L. Bak, V. Bakken,
     O. Christiansen, P. Dahle, E. K. Dalskov, T. Enevoldsen,
     A. Halkier, H. Heiberg, D. Jonsson, S. Kirpekar, R. Kobayashi,
     A. S. de Meras, K. V. Mikkelsen, P. Norman, M. J. Packer,
     K. Ruud, T.Saue, P. R. Taylor, and O. Vahtras:
     DALTON, an electronic structure program"



     ******************************************
     **    PROGRAM:              DALTON      **
     **    PROGRAM VERSION:      5.4.0.0     **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************



 <<<<<<<<<< OUTPUT FROM GENERAL INPUT PROCESSING >>>>>>>>>>




 Default print level:        0

    Integral sections will be executed
    Starting in Integral Section -



 *************************************************************************
 ****************** Output from HERMIT input processing ******************
 *************************************************************************



 Default print level:        2


 Calculation of one- and two-electron Hamiltonian integrals.


 The following one-electron property integrals are calculated:

          - overlap integrals
          - Cartesian multipole moment integrals of orders 4 and lower
          - electronic angular momentum around the origin


 Changes of defaults for READIN:
 -------------------------------


 Maximum number of primitives per integral block :    9



 *************************************************************************
 ****************** Output from READIN input processing ******************
 *************************************************************************



  Title Cards
  -----------

                                                                          
                                                                          


                      SYMGRP:Point group information
                      ------------------------------

Point group: C1 

   * Character table

        |  E 
   -----+-----
    A   |   1

   * Direct product table

        | A  
   -----+-----
    A   | A  


  Atoms and basis sets
  --------------------

  Number of atom types:     2
  Total number of atoms:    3

  label    atoms   charge   prim    cont     basis   
  ----------------------------------------------------------------------
  O  1        1       8      26      14      [9s4p1d|3s2p1d]                        
  H  1        1       1       7       5      [4s1p|2s1p]                            
  H  2        1       1       7       5      [4s1p|2s1p]                            
  ----------------------------------------------------------------------
  ----------------------------------------------------------------------
  total:      3      10      40      24

  Spherical harmonic basis used.
  Threshold for integrals:  1.00E-15


  Cartesian Coordinates
  ---------------------

  Total number of coordinates:  9


   1   O  1     x      0.0000000000
   2            y      0.0000000000
   3            z      0.7212806600

   4   H  1     x     -1.4192363700
   5            y      0.0000000000
   6            z     -0.3606403300

   7   H  2     x      1.4192363700
   8            y      0.0000000000
   9            z     -0.3606403300



   Interatomic separations (in Angstroms):
   ---------------------------------------

            O  1        H  1        H  2

   O  1    0.000000
   H  1    0.944368    0.000000
   H  2    0.944368    1.502055    0.000000




  Bond distances (angstroms):
  ---------------------------

                  atom 1     atom 2                           distance
                  ------     ------                           --------
  bond distance:    H  1       O  1                           0.944368
  bond distance:    H  2       O  1                           0.944368


  Bond angles (degrees):
  ----------------------

                  atom 1     atom 2     atom 3                   angle
                  ------     ------     ------                   -----
  bond angle:       H  2       O  1       H  1                 105.362


  Nuclear repulsion energy :    9.317913916904


  Orbital exponents and contraction coefficients
  ----------------------------------------------


  O  1   1s    1    11720.000000    0.0007 -0.0002  0.0000
   gen. cont.  2     1759.000000    0.0055 -0.0013  0.0000
               3      400.800000    0.0278 -0.0063  0.0000
               4      113.700000    0.1048 -0.0257  0.0000
               5       37.030000    0.2831 -0.0709  0.0000
               6       13.270000    0.4487 -0.1654  0.0000
               7        5.025000    0.2710 -0.1170  0.0000
               8        1.013000    0.0155  0.5574  0.0000
               9        0.302300   -0.0026  0.5728  1.0000

  O  1   2px  10       17.700000    0.0430  0.0000
   gen. cont. 11        3.854000    0.2289  0.0000
              12        1.046000    0.5087  0.0000
              13        0.275300    0.4605  1.0000

  O  1   2py  14       17.700000    0.0430  0.0000
   gen. cont. 15        3.854000    0.2289  0.0000
              16        1.046000    0.5087  0.0000
              17        0.275300    0.4605  1.0000

  O  1   2pz  18       17.700000    0.0430  0.0000
   gen. cont. 19        3.854000    0.2289  0.0000
              20        1.046000    0.5087  0.0000
              21        0.275300    0.4605  1.0000

  O  1   3d2- 22        1.185000    1.0000

  O  1   3d1- 23        1.185000    1.0000

  O  1   3d0  24        1.185000    1.0000

  O  1   3d1+ 25        1.185000    1.0000

  O  1   3d2+ 26        1.185000    1.0000

  H  1   1s   27       13.010000    0.0197  0.0000
   gen. cont. 28        1.962000    0.1380  0.0000
              29        0.444600    0.4781  0.0000
              30        0.122000    0.5012  1.0000

  H  1   2px  31        0.727000    1.0000

  H  1   2py  32        0.727000    1.0000

  H  1   2pz  33        0.727000    1.0000

  H  2   1s   34       13.010000    0.0197  0.0000
   gen. cont. 35        1.962000    0.1380  0.0000
              36        0.444600    0.4781  0.0000
              37        0.122000    0.5012  1.0000

  H  2   2px  38        0.727000    1.0000

  H  2   2py  39        0.727000    1.0000

  H  2   2pz  40        0.727000    1.0000


  Contracted Orbitals
  -------------------

   1  O  1    1s       1     2     3     4     5     6     7     8     9
   2  O  1    1s       1     2     3     4     5     6     7     8     9
   3  O  1    1s       9
   4  O  1    2px     10    11    12    13
   5  O  1    2py     14    15    16    17
   6  O  1    2pz     18    19    20    21
   7  O  1    2px     13
   8  O  1    2py     17
   9  O  1    2pz     21
  10  O  1    3d2-    22
  11  O  1    3d1-    23
  12  O  1    3d0     24
  13  O  1    3d1+    25
  14  O  1    3d2+    26
  15  H  1    1s      27    28    29    30
  16  H  1    1s      30
  17  H  1    2px     31
  18  H  1    2py     32
  19  H  1    2pz     33
  20  H  2    1s      34    35    36    37
  21  H  2    1s      37
  22  H  2    2px     38
  23  H  2    2py     39
  24  H  2    2pz     40




  Symmetry Orbitals
  -----------------

  Number of orbitals in each symmetry:        24


  Symmetry  A  ( 1)

    1     O  1     1s         1
    2     O  1     1s         2
    3     O  1     1s         3
    4     O  1     2px        4
    5     O  1     2py        5
    6     O  1     2pz        6
    7     O  1     2px        7
    8     O  1     2py        8
    9     O  1     2pz        9
   10     O  1     3d2-      10
   11     O  1     3d1-      11
   12     O  1     3d0       12
   13     O  1     3d1+      13
   14     O  1     3d2+      14
   15     H  1     1s        15
   16     H  1     1s        16
   17     H  1     2px       17
   18     H  1     2py       18
   19     H  1     2pz       19
   20     H  2     1s        20
   21     H  2     1s        21
   22     H  2     2px       22
   23     H  2     2py       23
   24     H  2     2pz       24

  Symmetries of electric field:  A  (1)  A  (1)  A  (1)

  Symmetries of magnetic field:  A  (1)  A  (1)  A  (1)


 Copy of input to READIN
 -----------------------

INTGRL                                                                          
                                                                                
                                                                                
s   2    0           0.10E-14                                                   
       8.0    1    3    1    1    1                                             
O  1   0.000000000000000   0.000000000000000   0.721280660000000       *        
H   9   3                                                                       
      11720.00000000         0.00071000        -0.00016000         0.00000000   
       1759.00000000         0.00547000        -0.00126300         0.00000000   
        400.80000000         0.02783700        -0.00626700         0.00000000   
        113.70000000         0.10480000        -0.02571600         0.00000000   
         37.03000000         0.28306200        -0.07092400         0.00000000   
         13.27000000         0.44871900        -0.16541100         0.00000000   
          5.02500000         0.27095200        -0.11695500         0.00000000   
          1.01300000         0.01545800         0.55736800         0.00000000   
          0.30230000        -0.00258500         0.57275900         1.00000000   
H   4   2                                                                       
         17.70000000         0.04301800         0.00000000                      
          3.85400000         0.22891300         0.00000000                      
          1.04600000         0.50872800         0.00000000                      
          0.27530000         0.46053100         1.00000000                      
H   1   1                                                                       
          1.18500000         1.00000000                                         
       1.0    2    2    1    1                                                  
H  1  -1.419236370000000   0.000000000000000  -0.360640330000000       *        
H  2   1.419236370000000   0.000000000000000  -0.360640330000000       *        
H   4   2                                                                       
         13.01000000         0.01968500         0.00000000                      
          1.96200000         0.13797700         0.00000000                      
          0.44460000         0.47814800         0.00000000                      
          0.12200000         0.50124000         1.00000000                      
H   1   1                                                                       
          0.72700000         1.00000000                                         


 herdrv: noofopt= 5


 ************************************************************************
 ************************** Output from HERONE **************************
 ************************************************************************

 prop, itype T 1


   131 atomic overlap integrals written in   1 buffers.
 Percentage non-zero integrals:  43.67
 prop, itype T 2


   168 one-el. Hamil. integrals written in   1 buffers.
 Percentage non-zero integrals:  56.00
 prop, itype T 3


   131 kinetic energy integrals written in   1 buffers.
 Percentage non-zero integrals:  43.67


 inttyp= 1noptyp= 1


                    +---------------------------------+
                    ! Integrals of operator: OVERLAP  !
                    +---------------------------------+



 finopt,noofopt,last1= 0 5 0
 inttyp= 8noptyp= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000000 !
                    +---------------------------------+



 finopt,noofopt,last1= 1 5 0
 inttyp= 8noptyp= 3


                    +---------------------------------+
                    ! Integrals of operator: CM010000 !
                    +---------------------------------+

 typea=  1typeb=  0last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000100 !
                    +---------------------------------+

 typea=  1typeb=  1last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000001 !
                    +---------------------------------+

 typea=  1typeb=  2last= 2
 lstflg= 1


 finopt,noofopt,last1= 2 5 0
 inttyp= 8noptyp= 6


                    +---------------------------------+
                    ! Integrals of operator: CM020000 !
                    +---------------------------------+

 typea=  1typeb=  3last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM010100 !
                    +---------------------------------+

 typea=  1typeb=  4last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM010001 !
                    +---------------------------------+

 typea=  1typeb=  5last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000200 !
                    +---------------------------------+

 typea=  1typeb=  6last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000101 !
                    +---------------------------------+

 typea=  1typeb=  7last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000002 !
                    +---------------------------------+

 typea=  1typeb=  8last= 2
 lstflg= 1


 finopt,noofopt,last1= 3 5 0
 inttyp= 8noptyp= 10


                    +---------------------------------+
                    ! Integrals of operator: CM030000 !
                    +---------------------------------+

 typea=  1typeb=  9last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM020100 !
                    +---------------------------------+

 typea=  1typeb=  10last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM020001 !
                    +---------------------------------+

 typea=  1typeb=  11last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM010200 !
                    +---------------------------------+

 typea=  1typeb=  12last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM010101 !
                    +---------------------------------+

 typea=  1typeb=  13last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM010002 !
                    +---------------------------------+

 typea=  1typeb=  14last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000300 !
                    +---------------------------------+

 typea=  1typeb=  15last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000201 !
                    +---------------------------------+

 typea=  1typeb=  16last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000102 !
                    +---------------------------------+

 typea=  1typeb=  17last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000003 !
                    +---------------------------------+

 typea=  1typeb=  18last= 2
 lstflg= 1


 finopt,noofopt,last1= 4 5 0
 inttyp= 8noptyp= 15


                    +---------------------------------+
                    ! Integrals of operator: CM040000 !
                    +---------------------------------+

 typea=  1typeb=  19last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM030100 !
                    +---------------------------------+

 typea=  1typeb=  20last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM030001 !
                    +---------------------------------+

 typea=  1typeb=  21last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM020200 !
                    +---------------------------------+

 typea=  1typeb=  22last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM020101 !
                    +---------------------------------+

 typea=  1typeb=  23last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM020002 !
                    +---------------------------------+

 typea=  1typeb=  24last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM010300 !
                    +---------------------------------+

 typea=  1typeb=  25last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM010201 !
                    +---------------------------------+

 typea=  1typeb=  26last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM010102 !
                    +---------------------------------+

 typea=  1typeb=  27last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM010003 !
                    +---------------------------------+

 typea=  1typeb=  28last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000400 !
                    +---------------------------------+

 typea=  1typeb=  29last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000301 !
                    +---------------------------------+

 typea=  1typeb=  30last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000202 !
                    +---------------------------------+

 typea=  1typeb=  31last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000103 !
                    +---------------------------------+

 typea=  1typeb=  32last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000004 !
                    +---------------------------------+

 typea=  1typeb=  33last= 2
 lstflg= 1


 finopt,noofopt,last1= 5 5 2
 inttyp= 18noptyp= 3


                    +---------------------------------+
                    ! Integrals of operator: XANGMOM  !
                    +---------------------------------+

 angular moment
 typea=  2typeb=  6last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: YANGMOM  !
                    +---------------------------------+

 angular moment
 typea=  2typeb=  7last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: ZANGMOM  !
                    +---------------------------------+

 angular moment
 typea=  2typeb=  8last= 2
 lstflg= 2




 ************************************************************************
 ************************** Output from TWOINT **************************
 ************************************************************************

 calling sifew2:luinta,info,num,last,nrec
 calling sifew2: 11 2 4096 3272 4096 2730 2533 2 7

 Number of two-electron integrals written:     21643 (47.9%)
 Kilobytes written:                              263




 >>>> Total CPU  time used in HERMIT:   0.22 seconds
 >>>> Total wall time used in HERMIT:   1.00 seconds

- End of Integral Section
