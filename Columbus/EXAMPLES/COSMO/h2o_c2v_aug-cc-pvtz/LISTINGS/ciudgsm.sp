1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 integral file titles:
 Hermit Integral Program : SIFS version  hochtor2        Thu Oct  7 15:04:40 2004
  cidrt_title                                                                    
  title                                                                          
 mofmt: formatted orbitals label=morbl   hochtor2        Thu Oct  7 15:05:01 2004
 SIFS file created by program tran.      hochtor2        Thu Oct  7 15:05:01 2004

 formula file title:
 Hermit Integral Program : SIFS version  hochtor2        Thu Oct  7 15:04:40 2004
  cidrt_title                                                                    
  title                                                                          
 mofmt: formatted orbitals label=morbl   hochtor2        Thu Oct  7 15:05:01 2004
 SIFS file created by program tran.      hochtor2        Thu Oct  7 15:05:01 2004
  cidrt_title                                                                    

 297 dimension of the ci-matrix ->>>     25093


 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -76.0811677723 -9.2979E+00  3.2160E-01  1.5288E+00  1.0000E-03
 mr-sdci #  2  1    -76.3457543599 -9.0662E+00  1.3523E-02  3.0253E-01  1.0000E-03
 mr-sdci #  3  1    -76.3555146103 -9.3210E+00  1.8705E-03  1.0046E-01  1.0000E-03
 mr-sdci #  4  1    -76.3567888100 -9.3295E+00  2.1434E-04  3.4007E-02  1.0000E-03
 mr-sdci #  5  1    -76.3569712981 -9.3306E+00  2.9879E-05  1.3510E-02  1.0000E-03
 mr-sdci #  6  1    -76.3569968148 -9.3307E+00  2.9082E-06  3.9230E-03  1.0000E-03
 mr-sdci #  7  1    -76.3585570816 -9.3292E+00  7.5298E-06  3.5003E-03  1.0000E-03
 mr-sdci #  8  1    -76.3585652165 -9.3266E+00  8.2220E-07  1.9565E-03  1.0000E-03
 mr-sdci #  9  1    -76.3585660116 -9.3266E+00  7.5986E-08  6.0941E-04  1.0000E-03

 mr-sdci  convergence criteria satisfied after  9 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  9  1    -76.3585660116 -9.3266E+00  7.5986E-08  6.0941E-04  1.0000E-03

 number of reference csfs (nref) is     1.  root number (iroot) is  1.

 eref      =    -76.082600704098   "relaxed" cnot**2         =   0.943742406906
 eci       =    -76.358566011609   deltae = eci - eref       =  -0.275965307512
 eci+dv1   =    -76.374091155587   dv1 = (1-cnot**2)*deltae  =  -0.015525143978
 eci+dv2   =    -76.375016627651   dv2 = dv1 / cnot**2       =  -0.016450616041
 eci+dv3   =    -76.376059430918   dv3 = dv1 / (2*cnot**2-1) =  -0.017493419309
 eci+pople =    -76.372206335804   ( 10e- scaled deltae )    =  -0.289605631706
