 total ao core energy =    9.317913917
 MCSCF calculation performed for  1 DRT.

 DRT  first state   no.of aver. states   weights
  1   ground state          1             1.000

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a  
 Total number of electrons:    10
 Spin multiplicity:            1
 Number of active orbitals:    0
 Number of active electrons:   0
 Total number of CSFs:         1

 Number of active-double rotations:      0
 Number of active-active rotations:      0
 Number of double-virtual rotations:   435
 Number of active-virtual rotations:     0

 iter=    1 emc=  -76.0611606086 demc= 7.6061E+01 wnorm= 1.6015E-08 knorm= 3.0724E-09 apxde=-1.7069E-16    *not converged* 

 final mcscf convergence values:
 iter=    2 emc=  -76.0611606086 demc= 1.4211E-14 wnorm= 2.4370E-08 knorm= 3.8273E-10 apxde= 1.4272E-15    *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 weight 1.000000 total energy=  -76.061160609
   ------------------------------------------------------------


