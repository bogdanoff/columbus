
 program cipc      

 print the csf info for mrsdci wave functions

 written by: ron shepard

 version date: 06-jun-96

 This Version of Program cipc is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 workspace allocation parameters: lencor=  10000000 mem1=1108566024 ifirst=-264081224

 drt header information:
  cidrt_title                                                                    
 nmot  =    92 niot  =     5 nfct  =     0 nfvt  =     0
 nrow  =    20 nsym  =     1 ssym  =     1 lenbuf=  1600
 spnorb=     F spnodd=     F lxyzir(1:3)= 0 0 0
 nwalk,xbar:         31        1        5       10       15
 nvalwt,nvalw:       31        1        5       10       15
 ncsft:           95266
 map(*)=    88 89 90 91 92  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15
            16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35
            36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55
            56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75
            76 77 78 79 80 81 82 83 84 85 86 87
 mu(*)=      0  0  0  0  0
 syml(*) =   1  1  1  1  1
 rmo(*)=     1  2  3  4  5

 indx01:    31 indices saved in indxv(*)
===================================ROOT # 1===================================

 rdhciv: CI vector file information:
 Hermit Integral Program : SIFS version  hochtor2        Thu Oct  7 15:21:00 2004
  cidrt_title                                                                    
  title                                                                          
 mofmt: formatted orbitals label=morbl   hochtor2        Thu Oct  7 15:21:57 2004
 SIFS file created by program tran.      hochtor2        Thu Oct  7 15:21:58 2004
 energy computed by program ciudg.       hochtor2        Thu Oct  7 15:26:15 2004

 lenrec =   32768 lenci =     95266 ninfo =  6 nenrgy =  5 ntitle =  6

 Max. overlap with ref vector #        1
 Valid ci vector #        1
 Method:        0       97% overlap
 energy( 1)=  9.317913916904E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -7.635785283367E+01, ietype=-1026,   total energy of type: MRSDCI  
 energy( 3)=  7.093300765498E-04, ietype=-2055, cnvginf energy of type: CI-Resid
 energy( 4)= -9.333299756161E+00, ietype=-2056, cnvginf energy of type: CI-D.E. 
 energy( 5)=  8.196451502702E-08, ietype=-2057, cnvginf energy of type: CI-ApxDE
==================================================================================

 space is available for   4981676 coefficients.

 updated histogram parameters:
 csfmn = 0.0000E+00 csfmx = 1.0000E+00 fhist = 5.0000E-01 nhist =  20

 this program will print the csfs generated from
 the drt according to the following print options :

 1) run in batch mode: all valid roots are automatically
    analysed and csf info is printed by default contribution
    threshold 0.01 
 2) run in interactive mode

 input menu number [  0]:
================================================================================
===================================VECTOR # 1===================================
================================================================================


 rdcivnew:      62 coefficients were selected.
 workspace: ncsfmx=   95266
 ncsfmx= 95266

 histogram parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =   95266 ncsft =   95266 ncsf =      62
 nhist =  20 fhist = 0.50000

    cmin                cmax        num  '*'=     2 csfs.
 ----------          ----------   ----- ---------|---------|---------|---------|
 5.0000E-01 <= |c| < 1.0000E+00       1 *
 2.5000E-01 <= |c| < 5.0000E-01       0
 1.2500E-01 <= |c| < 2.5000E-01       0
 6.2500E-02 <= |c| < 1.2500E-01       0
 3.1250E-02 <= |c| < 6.2500E-02       0
 1.5625E-02 <= |c| < 3.1250E-02       5 ***
 7.8125E-03 <= |c| < 1.5625E-02      56 ****************************
 3.9062E-03 <= |c| < 7.8125E-03       0
 1.9531E-03 <= |c| < 3.9062E-03       0
 9.7656E-04 <= |c| < 1.9531E-03       0
 4.8828E-04 <= |c| < 9.7656E-04       0
 2.4414E-04 <= |c| < 4.8828E-04       0
 1.2207E-04 <= |c| < 2.4414E-04       0
 6.1035E-05 <= |c| < 1.2207E-04       0
 3.0518E-05 <= |c| < 6.1035E-05       0
 1.5259E-05 <= |c| < 3.0518E-05       0
 7.6294E-06 <= |c| < 1.5259E-05       0
 3.8147E-06 <= |c| < 7.6294E-06       0
 0.0000E+00 <= |c| < 3.8147E-06       0
                                  ----- ---------|---------|---------|---------|
                  total read =       62 total stored =      62

 from the selected csfs,
 min(|csfvec(:)|) = 1.0018E-02    max(|csfvec(:)|) = 9.7150E-01
 norm=  1.
 csfs will be printed based on coefficient magnitudes.

 current csfvec(*) selection parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =   95266 ncsft =   95266 ncsf =      62

 i:slabel(i) =  1: a  

 internal level =    1    2    3    4    5
 syml(*)        =    1    1    1    1    1
 label          =  a    a    a    a    a  
 rmo(*)         =    1    2    3    4    5

 printing selected csfs in sorted order from cmin = 0.01000 to cmax = 1.00000

   indcsf     c     c**2   v  lab:rmo  lab:rmo   step(*)
  ------- -------- ------- - ---- --- ---- --- ------------
        1  0.97150 0.94380 z*                    33333
    57406 -0.01932 0.00037 w  a  : 19  a  : 34 1233033
    45755 -0.01860 0.00035 w           a  : 27  333303
    57091 -0.01746 0.00030 w           a  : 19  333033
        5  0.01610 0.00026 y           a  :  9  133332
    38305 -0.01598 0.00026 w  a  : 29  a  : 35 1233330
       93  0.01553 0.00024 y           a  : 10  133323
    45744  0.01544 0.00024 w  a  : 16  a  : 27 1233303
    53403  0.01538 0.00024 w  a  : 19  a  : 27 1233123
      606 -0.01519 0.00023 x  a  : 22  a  : 24 1133322
    46227 -0.01485 0.00022 w  a  : 27  a  : 43 1233303
     8038  0.01470 0.00022 x  a  : 20  a  : 21 1133223
    41972  0.01405 0.00020 w  a  : 27  a  : 29 1233312
    38311 -0.01387 0.00019 w           a  : 35  333330
     4329  0.01359 0.00018 x  a  : 21  a  : 23 1133232
    57083  0.01353 0.00018 w  a  : 11  a  : 19 1233033
    38126 -0.01348 0.00018 w  a  :  9  a  : 29 1233330
    57106 -0.01340 0.00018 w           a  : 20  333033
    68574  0.01333 0.00018 w  a  : 18  a  : 19 1231233
    53586  0.01317 0.00017 w  a  : 27  a  : 34 1233123
    57407  0.01310 0.00017 w  a  : 20  a  : 34 1233033
    57105  0.01296 0.00017 w  a  : 19  a  : 20 1233033
    38285 -0.01291 0.00017 w  a  :  9  a  : 35 1233330
     8142 -0.01276 0.00016 x  a  : 19  a  : 27 1133223
    42131  0.01260 0.00016 w  a  : 27  a  : 35 1233312
      864 -0.01258 0.00016 x  a  : 27  a  : 35 1133322
    57421 -0.01256 0.00016 w           a  : 34  333033
    45622 -0.01246 0.00016 w           a  : 20  333303
    45736  0.01242 0.00015 w  a  :  8  a  : 27 1233303
    38302 -0.01214 0.00015 w  a  : 26  a  : 35 1233330
    38146 -0.01210 0.00015 w           a  : 29  333330
    57084  0.01205 0.00015 w  a  : 12  a  : 19 1233033
     8528  0.01203 0.00014 x  a  : 20  a  : 41 1133223
      711 -0.01161 0.00013 x  a  : 27  a  : 29 1133322
     4597  0.01159 0.00013 x  a  : 19  a  : 35 1133232
    53262 -0.01154 0.00013 w  a  : 18  a  : 19 1233123
    53260 -0.01153 0.00013 w  a  : 16  a  : 19 1233123
    38291  0.01153 0.00013 w  a  : 15  a  : 35 1233330
    68889  0.01128 0.00013 w  a  : 18  a  : 34 1231233
      650  0.01112 0.00012 x  a  :  9  a  : 27 1133322
     4259 -0.01105 0.00012 x  a  :  9  a  : 19 1133232
    38143 -0.01104 0.00012 w  a  : 26  a  : 29 1233330
    49779 -0.01103 0.00012 w  a  : 19  a  : 35 1233132
    68862  0.01101 0.00012 w  a  : 19  a  : 33 1231233
    49620 -0.01090 0.00012 w  a  : 19  a  : 29 1233132
     4444  0.01086 0.00012 x  a  : 19  a  : 29 1133232
    57122 -0.01085 0.00012 w           a  : 21  333033
     4754  0.01081 0.00012 x  a  : 21  a  : 40 1133232
    53276  0.01080 0.00012 w  a  : 18  a  : 20 1233123
    61103  0.01078 0.00012 w  a  : 18  a  : 29 1231332
    41909  0.01074 0.00012 w  a  :  9  a  : 27 1233312
    68904  0.01066 0.00011 w  a  : 33  a  : 34 1231233
    57795  0.01062 0.00011 w  a  : 34  a  : 45 1233033
    68566 -0.01061 0.00011 w  a  : 10  a  : 19 1231233
    49425 -0.01047 0.00011 w  a  :  9  a  : 19 1233132
    37895  0.01038 0.00011 w  a  :  9  a  : 15 1233330
    65033  0.01036 0.00011 w  a  : 18  a  : 33 1231323
    57098 -0.01032 0.00011 w  a  : 12  a  : 20 1233033
     4265  0.01025 0.00011 x  a  : 15  a  : 19 1133232
     1273  0.01023 0.00010 x  a  : 22  a  : 47 1133322
    53252 -0.01019 0.00010 w  a  :  8  a  : 19 1233123
    64733  0.01002 0.00010 w           a  : 18  331323
           62 csfs were printed in this range.
