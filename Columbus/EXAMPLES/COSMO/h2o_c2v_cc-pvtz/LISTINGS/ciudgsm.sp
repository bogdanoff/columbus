1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 integral file titles:
 Hermit Integral Program : SIFS version  hochtor2        Thu Oct  7 15:12:41 2004
  cidrt_title                                                                    
  title                                                                          
 mofmt: formatted orbitals label=morbl   hochtor2        Thu Oct  7 15:12:44 2004
 SIFS file created by program tran.      hochtor2        Thu Oct  7 15:12:44 2004

 formula file title:
 Hermit Integral Program : SIFS version  hochtor2        Thu Oct  7 15:12:41 2004
  cidrt_title                                                                    
  title                                                                          
 mofmt: formatted orbitals label=morbl   hochtor2        Thu Oct  7 15:12:44 2004
 SIFS file created by program tran.      hochtor2        Thu Oct  7 15:12:44 2004
  cidrt_title                                                                    

 297 dimension of the ci-matrix ->>>      6339


 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -76.0726719487 -9.2989E+00  3.1915E-01  1.4431E+00  1.0000E-03
 mr-sdci #  2  1    -76.3265816350 -9.0822E+00  1.0861E-02  2.7085E-01  1.0000E-03
 mr-sdci #  3  1    -76.3360582449 -9.3266E+00  5.7768E-04  5.6060E-02  1.0000E-03
 mr-sdci #  4  1    -76.3365604134 -9.3356E+00  6.4519E-05  1.9998E-02  1.0000E-03
 mr-sdci #  5  1    -76.3366213635 -9.3360E+00  5.1901E-06  5.4172E-03  1.0000E-03
 mr-sdci #  6  1    -76.3366263449 -9.3361E+00  5.7116E-07  1.6747E-03  1.0000E-03
 mr-sdci #  7  1    -76.3378734950 -9.3348E+00  4.2867E-06  2.7501E-03  1.0000E-03
 mr-sdci #  8  1    -76.3378780848 -9.3346E+00  4.0750E-07  1.3031E-03  1.0000E-03
 mr-sdci #  9  1    -76.3378784647 -9.3346E+00  3.7153E-08  3.9909E-04  1.0000E-03

 mr-sdci  convergence criteria satisfied after  9 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  9  1    -76.3378784647 -9.3346E+00  3.7153E-08  3.9909E-04  1.0000E-03

 number of reference csfs (nref) is     1.  root number (iroot) is  1.

 eref      =    -76.073829875564   "relaxed" cnot**2         =   0.945412667794
 eci       =    -76.337878464675   deltae = eci - eref       =  -0.264048589111
 eci+dv1   =    -76.352292172727   dv1 = (1-cnot**2)*deltae  =  -0.014413708052
 eci+dv2   =    -76.353124408106   dv2 = dv1 / cnot**2       =  -0.015245943431
 eci+dv3   =    -76.354058637686   dv3 = dv1 / (2*cnot**2-1) =  -0.016180173011
 eci+pople =    -76.350505697694   ( 10e- scaled deltae )    =  -0.276675822130
