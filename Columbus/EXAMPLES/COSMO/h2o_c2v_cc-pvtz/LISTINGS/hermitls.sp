
 DALTON: user specified work memory size used,
          environment variable WRKMEM = "10000000            "

 Work memory size (LMWORK) :    10000000 =   76.29 megabytes.

 Default basis set library used :
        /sphome/kedziora/dalton/basis/                              


    $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    $$$$$$$$$$$  DALTON - An electronic structure program  $$$$$$$$$$$
    $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

               This is output from DALTON (beta-version 0.9) 

                          Principal authors:

            Trygve Helgaker,     University of Oslo,        Norway 
            Hans Joergen Jensen, University of Odense,      Denmark
            Poul Joergensen,     University of Aarhus,      Denmark
            Henrik Koch,         University of Aarhus,      Denmark
            Jeppe Olsen,         University of Lund,        Sweden 
            Hans Aagren,         University of Linkoeping,  Sweden 

                          Contributors:

            Torgeir Andersen,    University of Oslo,        Norway 
            Keld L. Bak,         University of Copenhagen,  Denmark
            Vebjoern Bakken,     University of Oslo,        Norway 
            Ove Christiansen,    University of Aarhus,      Denmark
            Paal Dahle,          University of Oslo,        Norway 
            Erik K. Dalskov,     University of Odense,      Denmark
            Thomas Enevoldsen,   University of Odense,      Denmark
            Asger Halkier,       University of Aarhus,      Denmark
            Hanne Heiberg,       University of Oslo,        Norway 
            Dan Jonsson,         University of Linkoeping,  Sweden 
            Sheela Kirpekar,     University of Odense,      Denmark
            Rika Kobayashi,      University of Aarhus,      Denmark
            Alfredo S. de Meras, Valencia University,       Spain  
            Kurt Mikkelsen,      University of Aarhus,      Denmark
            Patrick Norman,      University of Linkoeping,  Sweden 
            Martin J. Packer,    University of Sheffield,   UK     
            Kenneth Ruud,        University of Oslo,        Norway 
            Trond Saue,          University of Oslo,        Norway 
            Peter Taylor,        San Diego Superc. Center,  USA    
            Olav Vahtras,        University of Linkoeping,  Sweden

                                             Release Date:  August 1996
------------------------------------------------------------------------


      
     NOTE:
      
     This is an experimental code for the evaluation of molecular
     properties using (MC)SCF/CC wave functions. The authors accept
      no responsibility for the performance of the code or for the
     correctness of the results.
      
     The code (in whole or part) is not to be reproduced for further
     distribution without the written permission of T. Helgaker,
     H. J. Aa. Jensen or P. Taylor.
      
     If results obtained with this code are published, an
     appropriate citation would be:
      
     T. Helgaker, H. J. Aa. Jensen, P.Joergensen, H. Koch,
     J. Olsen, H. Aagren, T. Andersen, K. L. Bak, V. Bakken,
     O. Christiansen, P. Dahle, E. K. Dalskov, T. Enevoldsen,
     A. Halkier, H. Heiberg, D. Jonsson, S. Kirpekar, R. Kobayashi,
     A. S. de Meras, K. V. Mikkelsen, P. Norman, M. J. Packer,
     K. Ruud, T.Saue, P. R. Taylor, and O. Vahtras:
     DALTON, an electronic structure program"



     ******************************************
     **    PROGRAM:              DALTON      **
     **    PROGRAM VERSION:      5.4.0.0     **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************



 <<<<<<<<<< OUTPUT FROM GENERAL INPUT PROCESSING >>>>>>>>>>




 Default print level:        0

    Integral sections will be executed
    Starting in Integral Section -



 *************************************************************************
 ****************** Output from HERMIT input processing ******************
 *************************************************************************



 Default print level:        2


 Calculation of one- and two-electron Hamiltonian integrals.


 The following one-electron property integrals are calculated:

          - overlap integrals
          - Cartesian multipole moment integrals of orders 4 and lower
          - electronic angular momentum around the origin


 Changes of defaults for READIN:
 -------------------------------


 Maximum number of primitives per integral block :   10



 *************************************************************************
 ****************** Output from READIN input processing ******************
 *************************************************************************



  Title Cards
  -----------

                                                                          
                                                                          


  Symmetry Operations
  -------------------

  Symmetry operations: 2



                      SYMGRP:Point group information
                      ------------------------------

Point group: C2v

   * The point group was generated by:

      Reflection in the yz-plane
      Reflection in the xz-plane

   * Group multiplication table

        |  E   C2z  Oxz  Oyz
   -----+--------------------
     E  |  E 
    C2z | C2z   E 
    Oxz | Oxz  Oyz   E 
    Oyz | Oyz  Oxz  C2z   E 

   * Character table

        |  E   C2z  Oxz  Oyz
   -----+--------------------
    A1  |   1    1    1    1
    B1  |   1   -1    1   -1
    B2  |   1   -1   -1    1
    A2  |   1    1   -1   -1

   * Direct product table

        | A1   B1   B2   A2 
   -----+--------------------
    A1  | A1 
    B1  | B1   A1 
    B2  | B2   A2   A1 
    A2  | A2   B2   B1   A1 


  Atoms and basis sets
  --------------------

  Number of atom types:     2
  Total number of atoms:    3

  label    atoms   charge   prim    cont     basis   
  ----------------------------------------------------------------------
  O  1        1       8      42      30      [10s5p2d1f|4s3p2d1f]                   
  H  1        2       1      11       9      [5s2p|3s2p]                            
  ----------------------------------------------------------------------
  ----------------------------------------------------------------------
  total:      3      10      64      48

  Spherical harmonic basis used.
  Threshold for integrals:  1.00E-15


  Cartesian Coordinates
  ---------------------

  Total number of coordinates:  9


   1   O  1     x      0.0000000000
   2            y      0.0000000000
   3            z      0.7212806600

   4   H  1 1   x     -1.4192363700
   5            y      0.0000000000
   6            z     -0.3606403300

   7   H  1 2   x      1.4192363700
   8            y      0.0000000000
   9            z     -0.3606403300



  Symmetry Coordinates
  --------------------

  Number of coordinates in each symmetry:   3  3  2  1


  Symmetry 1

   1   O  1  z    3
   2   H  1  x    [ 4  -  7 ]/2
   3   H  1  z    [ 6  +  9 ]/2


  Symmetry 2

   4   O  1  x    1
   5   H  1  x    [ 4  +  7 ]/2
   6   H  1  z    [ 6  -  9 ]/2


  Symmetry 3

   7   O  1  y    2
   8   H  1  y    [ 5  +  8 ]/2


  Symmetry 4

   9   H  1  y    [ 5  -  8 ]/2


   Interatomic separations (in Angstroms):
   ---------------------------------------

            O  1        H  1        H  2

   O  1    0.000000
   H  1    0.944368    0.000000
   H  2    0.944368    1.502055    0.000000




  Bond distances (angstroms):
  ---------------------------

                  atom 1     atom 2                           distance
                  ------     ------                           --------
  bond distance:    H  1       O  1                           0.944368
  bond distance:    H  2       O  1                           0.944368


  Bond angles (degrees):
  ----------------------

                  atom 1     atom 2     atom 3                   angle
                  ------     ------     ------                   -----
  bond angle:       H  2       O  1       H  1                 105.362


  Nuclear repulsion energy :    9.317913916904


  Orbital exponents and contraction coefficients
  ----------------------------------------------


  O  1   1s    1    15330.000000    0.0005 -0.0001  0.0000  0.0000
   gen. cont.  2     2299.000000    0.0039 -0.0009  0.0000  0.0000
               3      522.400000    0.0202 -0.0046  0.0000  0.0000
               4      147.300000    0.0792 -0.0187  0.0000  0.0000
               5       47.550000    0.2307 -0.0585  0.0000  0.0000
               6       16.760000    0.4331 -0.1365  0.0000  0.0000
               7        6.207000    0.3503 -0.1757  0.0000  0.0000
               8        1.752000    0.0427  0.1609  1.0000  0.0000
               9        0.688200   -0.0082  0.6034  0.0000  0.0000
              10        0.238400    0.0024  0.3788  0.0000  1.0000

  O  1   2px  11       34.460000    0.0159  0.0000  0.0000
   gen. cont. 12        7.749000    0.0997  0.0000  0.0000
              13        2.280000    0.3105  0.0000  0.0000
              14        0.715600    0.4910  1.0000  0.0000
              15        0.214000    0.3363  0.0000  1.0000

  O  1   2py  16       34.460000    0.0159  0.0000  0.0000
   gen. cont. 17        7.749000    0.0997  0.0000  0.0000
              18        2.280000    0.3105  0.0000  0.0000
              19        0.715600    0.4910  1.0000  0.0000
              20        0.214000    0.3363  0.0000  1.0000

  O  1   2pz  21       34.460000    0.0159  0.0000  0.0000
   gen. cont. 22        7.749000    0.0997  0.0000  0.0000
              23        2.280000    0.3105  0.0000  0.0000
              24        0.715600    0.4910  1.0000  0.0000
              25        0.214000    0.3363  0.0000  1.0000

  O  1   3d2- 26        2.314000    1.0000  0.0000
   seg. cont. 27        0.645000    0.0000  1.0000

  O  1   3d1- 28        2.314000    1.0000  0.0000
   seg. cont. 29        0.645000    0.0000  1.0000

  O  1   3d0  30        2.314000    1.0000  0.0000
   seg. cont. 31        0.645000    0.0000  1.0000

  O  1   3d1+ 32        2.314000    1.0000  0.0000
   seg. cont. 33        0.645000    0.0000  1.0000

  O  1   3d2+ 34        2.314000    1.0000  0.0000
   seg. cont. 35        0.645000    0.0000  1.0000

  O  1   4f3- 36        1.428000    1.0000

  O  1   4f2- 37        1.428000    1.0000

  O  1   4f1- 38        1.428000    1.0000

  O  1   4f0  39        1.428000    1.0000

  O  1   4f1+ 40        1.428000    1.0000

  O  1   4f2+ 41        1.428000    1.0000

  O  1   4f3+ 42        1.428000    1.0000

  H  1#1 1s   43       13.010000    0.0197  0.0000  0.0000
   gen. cont. 44        1.962000    0.1380  0.0000  0.0000
              45        0.444600    0.4781  0.0000  0.0000
              46        0.122000    0.5012  1.0000  0.0000
              47        0.029740    0.0000  0.0000  1.0000

  H  1#2 1s   48       13.010000    0.0197  0.0000  0.0000
   gen. cont. 49        1.962000    0.1380  0.0000  0.0000
              50        0.444600    0.4781  0.0000  0.0000
              51        0.122000    0.5012  1.0000  0.0000
              52        0.029740    0.0000  0.0000  1.0000

  H  1#1 2px  53        0.727000    1.0000  0.0000
   seg. cont. 54        0.141000    0.0000  1.0000

  H  1#2 2px  55        0.727000    1.0000  0.0000
   seg. cont. 56        0.141000    0.0000  1.0000

  H  1#1 2py  57        0.727000    1.0000  0.0000
   seg. cont. 58        0.141000    0.0000  1.0000

  H  1#2 2py  59        0.727000    1.0000  0.0000
   seg. cont. 60        0.141000    0.0000  1.0000

  H  1#1 2pz  61        0.727000    1.0000  0.0000
   seg. cont. 62        0.141000    0.0000  1.0000

  H  1#2 2pz  63        0.727000    1.0000  0.0000
   seg. cont. 64        0.141000    0.0000  1.0000


  Contracted Orbitals
  -------------------

   1  O  1    1s       1     2     3     4     5     6     7     8     9    10
   2  O  1    1s       1     2     3     4     5     6     7     8     9    10
   3  O  1    1s       8
   4  O  1    1s      10
   5  O  1    2px     11    12    13    14    15
   6  O  1    2py     16    17    18    19    20
   7  O  1    2pz     21    22    23    24    25
   8  O  1    2px     14
   9  O  1    2py     19
  10  O  1    2pz     24
  11  O  1    2px     15
  12  O  1    2py     20
  13  O  1    2pz     25
  14  O  1    3d2-    26
  15  O  1    3d1-    28
  16  O  1    3d0     30
  17  O  1    3d1+    32
  18  O  1    3d2+    34
  19  O  1    3d2-    27
  20  O  1    3d1-    29
  21  O  1    3d0     31
  22  O  1    3d1+    33
  23  O  1    3d2+    35
  24  O  1    4f3-    36
  25  O  1    4f2-    37
  26  O  1    4f1-    38
  27  O  1    4f0     39
  28  O  1    4f1+    40
  29  O  1    4f2+    41
  30  O  1    4f3+    42
  31  H  1#1  1s    43  44  45  46
  32  H  1#2  1s    48  49  50  51
  33  H  1#1  1s    46
  34  H  1#2  1s    51
  35  H  1#1  1s    47
  36  H  1#2  1s    52
  37  H  1#1  2px   53
  38  H  1#2  2px   55
  39  H  1#1  2py   57
  40  H  1#2  2py   59
  41  H  1#1  2pz   61
  42  H  1#2  2pz   63
  43  H  1#1  2px   54
  44  H  1#2  2px   56
  45  H  1#1  2py   58
  46  H  1#2  2py   60
  47  H  1#1  2pz   62
  48  H  1#2  2pz   64




  Symmetry Orbitals
  -----------------

  Number of orbitals in each symmetry:        20 14  9  5


  Symmetry  A1 ( 1)

    1     O  1     1s         1
    2     O  1     1s         2
    3     O  1     1s         3
    4     O  1     1s         4
    5     O  1     2pz        7
    6     O  1     2pz       10
    7     O  1     2pz       13
    8     O  1     3d0       16
    9     O  1     3d2+      18
   10     O  1     3d0       21
   11     O  1     3d2+      23
   12     O  1     4f0       27
   13     O  1     4f2+      29
   14     H  1     1s        31  +  32
   15     H  1     1s        33  +  34
   16     H  1     1s        35  +  36
   17     H  1     2px       37  -  38
   18     H  1     2pz       41  +  42
   19     H  1     2px       43  -  44
   20     H  1     2pz       47  +  48


  Symmetry  B1 ( 2)

   21     O  1     2px        5
   22     O  1     2px        8
   23     O  1     2px       11
   24     O  1     3d1+      17
   25     O  1     3d1+      22
   26     O  1     4f1+      28
   27     O  1     4f3+      30
   28     H  1     1s        31  -  32
   29     H  1     1s        33  -  34
   30     H  1     1s        35  -  36
   31     H  1     2px       37  +  38
   32     H  1     2pz       41  -  42
   33     H  1     2px       43  +  44
   34     H  1     2pz       47  -  48


  Symmetry  B2 ( 3)

   35     O  1     2py        6
   36     O  1     2py        9
   37     O  1     2py       12
   38     O  1     3d1-      15
   39     O  1     3d1-      20
   40     O  1     4f3-      24
   41     O  1     4f1-      26
   42     H  1     2py       39  +  40
   43     H  1     2py       45  +  46


  Symmetry  A2 ( 4)

   44     O  1     3d2-      14
   45     O  1     3d2-      19
   46     O  1     4f2-      25
   47     H  1     2py       39  -  40
   48     H  1     2py       45  -  46

  Symmetries of electric field:  B1 (2)  B2 (3)  A1 (1)

  Symmetries of magnetic field:  B2 (3)  B1 (2)  A2 (4)


 Copy of input to READIN
 -----------------------

INTGRL                                                                          
                                                                                
                                                                                
s   2    2X   Y      0.10E-14                                                   
       8.0    1    4    1    1    1    1                                        
O  1   0.000000000000000   0.000000000000000   0.721280660000000       *        
H  10   4                                                                       
      15330.00000000         0.00050800        -0.00011500         0.00000000   
                             0.00000000                                         
       2299.00000000         0.00392900        -0.00089500         0.00000000   
                             0.00000000                                         
        522.40000000         0.02024300        -0.00463600         0.00000000   
                             0.00000000                                         
        147.30000000         0.07918100        -0.01872400         0.00000000   
                             0.00000000                                         
         47.55000000         0.23068700        -0.05846300         0.00000000   
                             0.00000000                                         
         16.76000000         0.43311800        -0.13646300         0.00000000   
                             0.00000000                                         
          6.20700000         0.35026000        -0.17574000         0.00000000   
                             0.00000000                                         
          1.75200000         0.04272800         0.16093400         1.00000000   
                             0.00000000                                         
          0.68820000        -0.00815400         0.60341800         0.00000000   
                             0.00000000                                         
          0.23840000         0.00238100         0.37876500         0.00000000   
                             1.00000000                                         
H   5   3                                                                       
         34.46000000         0.01592800         0.00000000         0.00000000   
          7.74900000         0.09974000         0.00000000         0.00000000   
          2.28000000         0.31049200         0.00000000         0.00000000   
          0.71560000         0.49102600         1.00000000         0.00000000   
          0.21400000         0.33633700         0.00000000         1.00000000   
H   2   2                                                                       
          2.31400000         1.00000000         0.00000000                      
          0.64500000         0.00000000         1.00000000                      
H   1   1                                                                       
          1.42800000         1.00000000                                         
       1.0    1    2    2    2                                                  
H  1  -1.419236370000000   0.000000000000000  -0.360640330000000       *        
H   4   2                                                                       
         13.01000000         0.01968500         0.00000000                      
          1.96200000         0.13797700         0.00000000                      
          0.44460000         0.47814800         0.00000000                      
          0.12200000         0.50124000         1.00000000                      
H   1   1                                                                       
          0.02974000         1.00000000                                         
H   1   1                                                                       
          0.72700000         1.00000000                                         
H   1   1                                                                       
          0.14100000         1.00000000                                         


 herdrv: noofopt= 5


 ************************************************************************
 ************************** Output from HERONE **************************
 ************************************************************************

 prop, itype T 1


   255 atomic overlap integrals written in   1 buffers.
 Percentage non-zero integrals:  21.68
 prop, itype T 2


   375 one-el. Hamil. integrals written in   1 buffers.
 Percentage non-zero integrals:  31.89
 prop, itype T 3


   263 kinetic energy integrals written in   1 buffers.
 Percentage non-zero integrals:  22.36


 inttyp= 1noptyp= 1


                    +---------------------------------+
                    ! Integrals of operator: OVERLAP  !
                    +---------------------------------+



 finopt,noofopt,last1= 0 5 0
 inttyp= 8noptyp= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000000 !
                    +---------------------------------+



 finopt,noofopt,last1= 1 5 0
 inttyp= 8noptyp= 3


                    +---------------------------------+
                    ! Integrals of operator: CM010000 !
                    +---------------------------------+

 typea=  1typeb=  0last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000100 !
                    +---------------------------------+

 typea=  1typeb=  1last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000001 !
                    +---------------------------------+

 typea=  1typeb=  2last= 2
 lstflg= 1


 finopt,noofopt,last1= 2 5 0
 inttyp= 8noptyp= 6


                    +---------------------------------+
                    ! Integrals of operator: CM020000 !
                    +---------------------------------+

 typea=  1typeb=  3last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM010100 !
                    +---------------------------------+

 typea=  1typeb=  4last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM010001 !
                    +---------------------------------+

 typea=  1typeb=  5last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000200 !
                    +---------------------------------+

 typea=  1typeb=  6last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000101 !
                    +---------------------------------+

 typea=  1typeb=  7last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000002 !
                    +---------------------------------+

 typea=  1typeb=  8last= 2
 lstflg= 1


 finopt,noofopt,last1= 3 5 0
 inttyp= 8noptyp= 10


                    +---------------------------------+
                    ! Integrals of operator: CM030000 !
                    +---------------------------------+

 typea=  1typeb=  9last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM020100 !
                    +---------------------------------+

 typea=  1typeb=  10last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM020001 !
                    +---------------------------------+

 typea=  1typeb=  11last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM010200 !
                    +---------------------------------+

 typea=  1typeb=  12last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM010101 !
                    +---------------------------------+

 typea=  1typeb=  13last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM010002 !
                    +---------------------------------+

 typea=  1typeb=  14last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000300 !
                    +---------------------------------+

 typea=  1typeb=  15last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000201 !
                    +---------------------------------+

 typea=  1typeb=  16last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000102 !
                    +---------------------------------+

 typea=  1typeb=  17last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000003 !
                    +---------------------------------+

 typea=  1typeb=  18last= 2
 lstflg= 1


 finopt,noofopt,last1= 4 5 0
 inttyp= 8noptyp= 15


                    +---------------------------------+
                    ! Integrals of operator: CM040000 !
                    +---------------------------------+

 typea=  1typeb=  19last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM030100 !
                    +---------------------------------+

 typea=  1typeb=  20last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM030001 !
                    +---------------------------------+

 typea=  1typeb=  21last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM020200 !
                    +---------------------------------+

 typea=  1typeb=  22last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM020101 !
                    +---------------------------------+

 typea=  1typeb=  23last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM020002 !
                    +---------------------------------+

 typea=  1typeb=  24last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM010300 !
                    +---------------------------------+

 typea=  1typeb=  25last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM010201 !
                    +---------------------------------+

 typea=  1typeb=  26last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM010102 !
                    +---------------------------------+

 typea=  1typeb=  27last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM010003 !
                    +---------------------------------+

 typea=  1typeb=  28last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000400 !
                    +---------------------------------+

 typea=  1typeb=  29last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000301 !
                    +---------------------------------+

 typea=  1typeb=  30last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000202 !
                    +---------------------------------+

 typea=  1typeb=  31last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000103 !
                    +---------------------------------+

 typea=  1typeb=  32last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000004 !
                    +---------------------------------+

 typea=  1typeb=  33last= 2
 lstflg= 1


 finopt,noofopt,last1= 5 5 2
 inttyp= 18noptyp= 3


                    +---------------------------------+
                    ! Integrals of operator: XANGMOM  !
                    +---------------------------------+

 angular moment
 typea=  2typeb=  6last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: YANGMOM  !
                    +---------------------------------+

 angular moment
 typea=  2typeb=  7last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: ZANGMOM  !
                    +---------------------------------+

 angular moment
 typea=  2typeb=  8last= 2
 lstflg= 2




 ************************************************************************
 ************************** Output from TWOINT **************************
 ************************************************************************

 calling sifew2:luinta,info,num,last,nrec
 calling sifew2: 11 2 4096 3272 4096 2730 1083 2 59

 Number of two-electron integrals written:    162153 (23.4%)
 Kilobytes written:                             1966




 >>>> Total CPU  time used in HERMIT:   0.58 seconds
 >>>> Total wall time used in HERMIT:   1.00 seconds

- End of Integral Section
