
 DALTON: user specified work memory size used,
          environment variable WRKMEM = "10000000            "

 Work memory size (LMWORK) :    10000000 =   76.29 megabytes.

 Default basis set library used :
        /sphome/kedziora/dalton/basis/                              


    $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    $$$$$$$$$$$  DALTON - An electronic structure program  $$$$$$$$$$$
    $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

               This is output from DALTON (beta-version 0.9) 

                          Principal authors:

            Trygve Helgaker,     University of Oslo,        Norway 
            Hans Joergen Jensen, University of Odense,      Denmark
            Poul Joergensen,     University of Aarhus,      Denmark
            Henrik Koch,         University of Aarhus,      Denmark
            Jeppe Olsen,         University of Lund,        Sweden 
            Hans Aagren,         University of Linkoeping,  Sweden 

                          Contributors:

            Torgeir Andersen,    University of Oslo,        Norway 
            Keld L. Bak,         University of Copenhagen,  Denmark
            Vebjoern Bakken,     University of Oslo,        Norway 
            Ove Christiansen,    University of Aarhus,      Denmark
            Paal Dahle,          University of Oslo,        Norway 
            Erik K. Dalskov,     University of Odense,      Denmark
            Thomas Enevoldsen,   University of Odense,      Denmark
            Asger Halkier,       University of Aarhus,      Denmark
            Hanne Heiberg,       University of Oslo,        Norway 
            Dan Jonsson,         University of Linkoeping,  Sweden 
            Sheela Kirpekar,     University of Odense,      Denmark
            Rika Kobayashi,      University of Aarhus,      Denmark
            Alfredo S. de Meras, Valencia University,       Spain  
            Kurt Mikkelsen,      University of Aarhus,      Denmark
            Patrick Norman,      University of Linkoeping,  Sweden 
            Martin J. Packer,    University of Sheffield,   UK     
            Kenneth Ruud,        University of Oslo,        Norway 
            Trond Saue,          University of Oslo,        Norway 
            Peter Taylor,        San Diego Superc. Center,  USA    
            Olav Vahtras,        University of Linkoeping,  Sweden

                                             Release Date:  August 1996
------------------------------------------------------------------------


      
     NOTE:
      
     This is an experimental code for the evaluation of molecular
     properties using (MC)SCF/CC wave functions. The authors accept
      no responsibility for the performance of the code or for the
     correctness of the results.
      
     The code (in whole or part) is not to be reproduced for further
     distribution without the written permission of T. Helgaker,
     H. J. Aa. Jensen or P. Taylor.
      
     If results obtained with this code are published, an
     appropriate citation would be:
      
     T. Helgaker, H. J. Aa. Jensen, P.Joergensen, H. Koch,
     J. Olsen, H. Aagren, T. Andersen, K. L. Bak, V. Bakken,
     O. Christiansen, P. Dahle, E. K. Dalskov, T. Enevoldsen,
     A. Halkier, H. Heiberg, D. Jonsson, S. Kirpekar, R. Kobayashi,
     A. S. de Meras, K. V. Mikkelsen, P. Norman, M. J. Packer,
     K. Ruud, T.Saue, P. R. Taylor, and O. Vahtras:
     DALTON, an electronic structure program"



     ******************************************
     **    PROGRAM:              DALTON      **
     **    PROGRAM VERSION:      5.4.0.0     **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************



 <<<<<<<<<< OUTPUT FROM GENERAL INPUT PROCESSING >>>>>>>>>>




 Default print level:        0

    Integral sections will be executed
    Starting in Integral Section -



 *************************************************************************
 ****************** Output from HERMIT input processing ******************
 *************************************************************************



 Default print level:        2


 Calculation of one- and two-electron Hamiltonian integrals.


 The following one-electron property integrals are calculated:

          - overlap integrals
          - Cartesian multipole moment integrals of orders 4 and lower
          - electronic angular momentum around the origin


 Changes of defaults for READIN:
 -------------------------------


 Maximum number of primitives per integral block :   12



 *************************************************************************
 ****************** Output from READIN input processing ******************
 *************************************************************************



  Title Cards
  -----------

                                                                          
                                                                          


  Symmetry Operations
  -------------------

  Symmetry operations: 2



                      SYMGRP:Point group information
                      ------------------------------

Point group: C2v

   * The point group was generated by:

      Reflection in the yz-plane
      Reflection in the xz-plane

   * Group multiplication table

        |  E   C2z  Oxz  Oyz
   -----+--------------------
     E  |  E 
    C2z | C2z   E 
    Oxz | Oxz  Oyz   E 
    Oyz | Oyz  Oxz  C2z   E 

   * Character table

        |  E   C2z  Oxz  Oyz
   -----+--------------------
    A1  |   1    1    1    1
    B1  |   1   -1    1   -1
    B2  |   1   -1   -1    1
    A2  |   1    1   -1   -1

   * Direct product table

        | A1   B1   B2   A2 
   -----+--------------------
    A1  | A1 
    B1  | B1   A1 
    B2  | B2   A2   A1 
    A2  | A2   B2   B1   A1 


  Atoms and basis sets
  --------------------

  Number of atom types:     2
  Total number of atoms:    3

  label    atoms   charge   prim    cont     basis   
  ----------------------------------------------------------------------
  O  1        1       8      68      55      [12s6p3d2f1g|5s4p3d2f1g]               
  H  1        2       1      32      30      [6s3p2d1f|4s3p2d1f]                    
  ----------------------------------------------------------------------
  ----------------------------------------------------------------------
  total:      3      10     132     115

  Spherical harmonic basis used.
  Threshold for integrals:  1.00E-15


  Cartesian Coordinates
  ---------------------

  Total number of coordinates:  9


   1   O  1     x      0.0000000000
   2            y      0.0000000000
   3            z      0.7212806600

   4   H  1 1   x     -1.4192363700
   5            y      0.0000000000
   6            z     -0.3606403300

   7   H  1 2   x      1.4192363700
   8            y      0.0000000000
   9            z     -0.3606403300



  Symmetry Coordinates
  --------------------

  Number of coordinates in each symmetry:   3  3  2  1


  Symmetry 1

   1   O  1  z    3
   2   H  1  x    [ 4  -  7 ]/2
   3   H  1  z    [ 6  +  9 ]/2


  Symmetry 2

   4   O  1  x    1
   5   H  1  x    [ 4  +  7 ]/2
   6   H  1  z    [ 6  -  9 ]/2


  Symmetry 3

   7   O  1  y    2
   8   H  1  y    [ 5  +  8 ]/2


  Symmetry 4

   9   H  1  y    [ 5  -  8 ]/2


   Interatomic separations (in Angstroms):
   ---------------------------------------

            O  1        H  1        H  2

   O  1    0.000000
   H  1    0.944368    0.000000
   H  2    0.944368    1.502055    0.000000




  Bond distances (angstroms):
  ---------------------------

                  atom 1     atom 2                           distance
                  ------     ------                           --------
  bond distance:    H  1       O  1                           0.944368
  bond distance:    H  2       O  1                           0.944368


  Bond angles (degrees):
  ----------------------

                  atom 1     atom 2     atom 3                   angle
                  ------     ------     ------                   -----
  bond angle:       H  2       O  1       H  1                 105.362


  Nuclear repulsion energy :    9.317913916904


  Orbital exponents and contraction coefficients
  ----------------------------------------------


  O  1   1s    1    61420.000000    0.0001  0.0000  0.0000  0.0000  0.0000
   gen. cont.  2     9199.000000    0.0007 -0.0002  0.0000  0.0000  0.0000
               3     2091.000000    0.0037 -0.0008  0.0000  0.0000  0.0000
               4      590.900000    0.0152 -0.0035  0.0000  0.0000  0.0000
               5      192.300000    0.0524 -0.0122  0.0000  0.0000  0.0000
               6       69.320000    0.1459 -0.0363  0.0000  0.0000  0.0000
               7       26.970000    0.3053 -0.0830  0.0000  0.0000  0.0000
               8       11.100000    0.3985 -0.1521  0.0000  0.0000  0.0000
               9        4.682000    0.2170 -0.1153  0.0000  0.0000  0.0000
              10        1.428000    0.0176  0.2890  1.0000  0.0000  0.0000
              11        0.554700   -0.0025  0.5861  0.0000  1.0000  0.0000
              12        0.206700    0.0010  0.2776  0.0000  0.0000  1.0000

  O  1   2px  13       63.420000    0.0060  0.0000  0.0000  0.0000
   gen. cont. 14       14.660000    0.0418  0.0000  0.0000  0.0000
              15        4.459000    0.1611  0.0000  0.0000  0.0000
              16        1.531000    0.3567  1.0000  0.0000  0.0000
              17        0.530200    0.4483  0.0000  1.0000  0.0000
              18        0.175000    0.2449  0.0000  0.0000  1.0000

  O  1   2py  19       63.420000    0.0060  0.0000  0.0000  0.0000
   gen. cont. 20       14.660000    0.0418  0.0000  0.0000  0.0000
              21        4.459000    0.1611  0.0000  0.0000  0.0000
              22        1.531000    0.3567  1.0000  0.0000  0.0000
              23        0.530200    0.4483  0.0000  1.0000  0.0000
              24        0.175000    0.2449  0.0000  0.0000  1.0000

  O  1   2pz  25       63.420000    0.0060  0.0000  0.0000  0.0000
   gen. cont. 26       14.660000    0.0418  0.0000  0.0000  0.0000
              27        4.459000    0.1611  0.0000  0.0000  0.0000
              28        1.531000    0.3567  1.0000  0.0000  0.0000
              29        0.530200    0.4483  0.0000  1.0000  0.0000
              30        0.175000    0.2449  0.0000  0.0000  1.0000

  O  1   3d2- 31        3.775000    1.0000  0.0000  0.0000
   seg. cont. 32        1.300000    0.0000  1.0000  0.0000
              33        0.444000    0.0000  0.0000  1.0000

  O  1   3d1- 34        3.775000    1.0000  0.0000  0.0000
   seg. cont. 35        1.300000    0.0000  1.0000  0.0000
              36        0.444000    0.0000  0.0000  1.0000

  O  1   3d0  37        3.775000    1.0000  0.0000  0.0000
   seg. cont. 38        1.300000    0.0000  1.0000  0.0000
              39        0.444000    0.0000  0.0000  1.0000

  O  1   3d1+ 40        3.775000    1.0000  0.0000  0.0000
   seg. cont. 41        1.300000    0.0000  1.0000  0.0000
              42        0.444000    0.0000  0.0000  1.0000

  O  1   3d2+ 43        3.775000    1.0000  0.0000  0.0000
   seg. cont. 44        1.300000    0.0000  1.0000  0.0000
              45        0.444000    0.0000  0.0000  1.0000

  O  1   4f3- 46        2.666000    1.0000  0.0000
   seg. cont. 47        0.859000    0.0000  1.0000

  O  1   4f2- 48        2.666000    1.0000  0.0000
   seg. cont. 49        0.859000    0.0000  1.0000

  O  1   4f1- 50        2.666000    1.0000  0.0000
   seg. cont. 51        0.859000    0.0000  1.0000

  O  1   4f0  52        2.666000    1.0000  0.0000
   seg. cont. 53        0.859000    0.0000  1.0000

  O  1   4f1+ 54        2.666000    1.0000  0.0000
   seg. cont. 55        0.859000    0.0000  1.0000

  O  1   4f2+ 56        2.666000    1.0000  0.0000
   seg. cont. 57        0.859000    0.0000  1.0000

  O  1   4f3+ 58        2.666000    1.0000  0.0000
   seg. cont. 59        0.859000    0.0000  1.0000

  O  1   5g4- 60        1.846000    1.0000

  O  1   5g3- 61        1.846000    1.0000

  O  1   5g2- 62        1.846000    1.0000

  O  1   5g1- 63        1.846000    1.0000

  O  1   5g0  64        1.846000    1.0000

  O  1   5g1+ 65        1.846000    1.0000

  O  1   5g2+ 66        1.846000    1.0000

  O  1   5g3+ 67        1.846000    1.0000

  O  1   5g4+ 68        1.846000    1.0000

  H  1#1 1s   69       82.640000    0.0020  0.0000  0.0000  0.0000
   gen. cont. 70       12.410000    0.0153  0.0000  0.0000  0.0000
              71        2.824000    0.0756  0.0000  0.0000  0.0000
              72        0.797700    0.2569  1.0000  0.0000  0.0000
              73        0.258100    0.4974  0.0000  1.0000  0.0000
              74        0.089890    0.2961  0.0000  0.0000  1.0000

  H  1#2 1s   75       82.640000    0.0020  0.0000  0.0000  0.0000
   gen. cont. 76       12.410000    0.0153  0.0000  0.0000  0.0000
              77        2.824000    0.0756  0.0000  0.0000  0.0000
              78        0.797700    0.2569  1.0000  0.0000  0.0000
              79        0.258100    0.4974  0.0000  1.0000  0.0000
              80        0.089890    0.2961  0.0000  0.0000  1.0000

  H  1#1 2px  81        2.292000    1.0000  0.0000  0.0000
   seg. cont. 82        0.838000    0.0000  1.0000  0.0000
              83        0.292000    0.0000  0.0000  1.0000

  H  1#2 2px  84        2.292000    1.0000  0.0000  0.0000
   seg. cont. 85        0.838000    0.0000  1.0000  0.0000
              86        0.292000    0.0000  0.0000  1.0000

  H  1#1 2py  87        2.292000    1.0000  0.0000  0.0000
   seg. cont. 88        0.838000    0.0000  1.0000  0.0000
              89        0.292000    0.0000  0.0000  1.0000

  H  1#2 2py  90        2.292000    1.0000  0.0000  0.0000
   seg. cont. 91        0.838000    0.0000  1.0000  0.0000
              92        0.292000    0.0000  0.0000  1.0000

  H  1#1 2pz  93        2.292000    1.0000  0.0000  0.0000
   seg. cont. 94        0.838000    0.0000  1.0000  0.0000
              95        0.292000    0.0000  0.0000  1.0000

  H  1#2 2pz  96        2.292000    1.0000  0.0000  0.0000
   seg. cont. 97        0.838000    0.0000  1.0000  0.0000
              98        0.292000    0.0000  0.0000  1.0000

  H  1#1 3d2- 99        2.062000    1.0000  0.0000
   seg. cont.100        0.662000    0.0000  1.0000

  H  1#2 3d2-101        2.062000    1.0000  0.0000
   seg. cont.102        0.662000    0.0000  1.0000

  H  1#1 3d1-103        2.062000    1.0000  0.0000
   seg. cont.104        0.662000    0.0000  1.0000

  H  1#2 3d1-105        2.062000    1.0000  0.0000
   seg. cont.106        0.662000    0.0000  1.0000

  H  1#1 3d0 107        2.062000    1.0000  0.0000
   seg. cont.108        0.662000    0.0000  1.0000

  H  1#2 3d0 109        2.062000    1.0000  0.0000
   seg. cont.110        0.662000    0.0000  1.0000

  H  1#1 3d1+111        2.062000    1.0000  0.0000
   seg. cont.112        0.662000    0.0000  1.0000

  H  1#2 3d1+113        2.062000    1.0000  0.0000
   seg. cont.114        0.662000    0.0000  1.0000

  H  1#1 3d2+115        2.062000    1.0000  0.0000
   seg. cont.116        0.662000    0.0000  1.0000

  H  1#2 3d2+117        2.062000    1.0000  0.0000
   seg. cont.118        0.662000    0.0000  1.0000

  H  1#1 4f3-119        1.397000    1.0000

  H  1#2 4f3-120        1.397000    1.0000

  H  1#1 4f2-121        1.397000    1.0000

  H  1#2 4f2-122        1.397000    1.0000

  H  1#1 4f1-123        1.397000    1.0000

  H  1#2 4f1-124        1.397000    1.0000

  H  1#1 4f0 125        1.397000    1.0000

  H  1#2 4f0 126        1.397000    1.0000

  H  1#1 4f1+127        1.397000    1.0000

  H  1#2 4f1+128        1.397000    1.0000

  H  1#1 4f2+129        1.397000    1.0000

  H  1#2 4f2+130        1.397000    1.0000

  H  1#1 4f3+131        1.397000    1.0000

  H  1#2 4f3+132        1.397000    1.0000


  Contracted Orbitals
  -------------------

   1  O  1    1s       1     2     3     4     5     6     7     8     9    10    11    12
   2  O  1    1s       1     2     3     4     5     6     7     8     9    10    11    12
   3  O  1    1s      10
   4  O  1    1s      11
   5  O  1    1s      12
   6  O  1    2px     13    14    15    16    17    18
   7  O  1    2py     19    20    21    22    23    24
   8  O  1    2pz     25    26    27    28    29    30
   9  O  1    2px     16
  10  O  1    2py     22
  11  O  1    2pz     28
  12  O  1    2px     17
  13  O  1    2py     23
  14  O  1    2pz     29
  15  O  1    2px     18
  16  O  1    2py     24
  17  O  1    2pz     30
  18  O  1    3d2-    31
  19  O  1    3d1-    34
  20  O  1    3d0     37
  21  O  1    3d1+    40
  22  O  1    3d2+    43
  23  O  1    3d2-    32
  24  O  1    3d1-    35
  25  O  1    3d0     38
  26  O  1    3d1+    41
  27  O  1    3d2+    44
  28  O  1    3d2-    33
  29  O  1    3d1-    36
  30  O  1    3d0     39
  31  O  1    3d1+    42
  32  O  1    3d2+    45
  33  O  1    4f3-    46
  34  O  1    4f2-    48
  35  O  1    4f1-    50
  36  O  1    4f0     52
  37  O  1    4f1+    54
  38  O  1    4f2+    56
  39  O  1    4f3+    58
  40  O  1    4f3-    47
  41  O  1    4f2-    49
  42  O  1    4f1-    51
  43  O  1    4f0     53
  44  O  1    4f1+    55
  45  O  1    4f2+    57
  46  O  1    4f3+    59
  47  O  1    5g4-    60
  48  O  1    5g3-    61
  49  O  1    5g2-    62
  50  O  1    5g1-    63
  51  O  1    5g0     64
  52  O  1    5g1+    65
  53  O  1    5g2+    66
  54  O  1    5g3+    67
  55  O  1    5g4+    68
  56  H  1#1  1s    69  70  71  72  73  74
  57  H  1#2  1s    75  76  77  78  79  80
  58  H  1#1  1s    72
  59  H  1#2  1s    78
  60  H  1#1  1s    73
  61  H  1#2  1s    79
  62  H  1#1  1s    74
  63  H  1#2  1s    80
  64  H  1#1  2px   81
  65  H  1#2  2px   84
  66  H  1#1  2py   87
  67  H  1#2  2py   90
  68  H  1#1  2pz   93
  69  H  1#2  2pz   96
  70  H  1#1  2px   82
  71  H  1#2  2px   85
  72  H  1#1  2py   88
  73  H  1#2  2py   91
  74  H  1#1  2pz   94
  75  H  1#2  2pz   97
  76  H  1#1  2px   83
  77  H  1#2  2px   86
  78  H  1#1  2py   89
  79  H  1#2  2py   92
  80  H  1#1  2pz   95
  81  H  1#2  2pz   98
  82  H  1#1  3d2-  99
  83  H  1#2  3d2- 101
  84  H  1#1  3d1- 103
  85  H  1#2  3d1- 105
  86  H  1#1  3d0  107
  87  H  1#2  3d0  109
  88  H  1#1  3d1+ 111
  89  H  1#2  3d1+ 113
  90  H  1#1  3d2+ 115
  91  H  1#2  3d2+ 117
  92  H  1#1  3d2- 100
  93  H  1#2  3d2- 102
  94  H  1#1  3d1- 104
  95  H  1#2  3d1- 106
  96  H  1#1  3d0  108
  97  H  1#2  3d0  110
  98  H  1#1  3d1+ 112
  99  H  1#2  3d1+ 114
 100  H  1#1  3d2+ 116
 101  H  1#2  3d2+ 118
 102  H  1#1  4f3- 119
 103  H  1#2  4f3- 120
 104  H  1#1  4f2- 121
 105  H  1#2  4f2- 122
 106  H  1#1  4f1- 123
 107  H  1#2  4f1- 124
 108  H  1#1  4f0  125
 109  H  1#2  4f0  126
 110  H  1#1  4f1+ 127
 111  H  1#2  4f1+ 128
 112  H  1#1  4f2+ 129
 113  H  1#2  4f2+ 130
 114  H  1#1  4f3+ 131
 115  H  1#2  4f3+ 132




  Symmetry Orbitals
  -----------------

  Number of orbitals in each symmetry:        42 33 23 17


  Symmetry  A1 ( 1)

    1     O  1     1s         1
    2     O  1     1s         2
    3     O  1     1s         3
    4     O  1     1s         4
    5     O  1     1s         5
    6     O  1     2pz        8
    7     O  1     2pz       11
    8     O  1     2pz       14
    9     O  1     2pz       17
   10     O  1     3d0       20
   11     O  1     3d2+      22
   12     O  1     3d0       25
   13     O  1     3d2+      27
   14     O  1     3d0       30
   15     O  1     3d2+      32
   16     O  1     4f0       36
   17     O  1     4f2+      38
   18     O  1     4f0       43
   19     O  1     4f2+      45
   20     O  1     5g0       51
   21     O  1     5g2+      53
   22     O  1     5g4+      55
   23     H  1     1s        56  +  57
   24     H  1     1s        58  +  59
   25     H  1     1s        60  +  61
   26     H  1     1s        62  +  63
   27     H  1     2px       64  -  65
   28     H  1     2pz       68  +  69
   29     H  1     2px       70  -  71
   30     H  1     2pz       74  +  75
   31     H  1     2px       76  -  77
   32     H  1     2pz       80  +  81
   33     H  1     3d0       86  +  87
   34     H  1     3d1+      88  -  89
   35     H  1     3d2+      90  +  91
   36     H  1     3d0       96  +  97
   37     H  1     3d1+      98  -  99
   38     H  1     3d2+     100  + 101
   39     H  1     4f0      108  + 109
   40     H  1     4f1+     110  - 111
   41     H  1     4f2+     112  + 113
   42     H  1     4f3+     114  - 115


  Symmetry  B1 ( 2)

   43     O  1     2px        6
   44     O  1     2px        9
   45     O  1     2px       12
   46     O  1     2px       15
   47     O  1     3d1+      21
   48     O  1     3d1+      26
   49     O  1     3d1+      31
   50     O  1     4f1+      37
   51     O  1     4f3+      39
   52     O  1     4f1+      44
   53     O  1     4f3+      46
   54     O  1     5g1+      52
   55     O  1     5g3+      54
   56     H  1     1s        56  -  57
   57     H  1     1s        58  -  59
   58     H  1     1s        60  -  61
   59     H  1     1s        62  -  63
   60     H  1     2px       64  +  65
   61     H  1     2pz       68  -  69
   62     H  1     2px       70  +  71
   63     H  1     2pz       74  -  75
   64     H  1     2px       76  +  77
   65     H  1     2pz       80  -  81
   66     H  1     3d0       86  -  87
   67     H  1     3d1+      88  +  89
   68     H  1     3d2+      90  -  91
   69     H  1     3d0       96  -  97
   70     H  1     3d1+      98  +  99
   71     H  1     3d2+     100  - 101
   72     H  1     4f0      108  - 109
   73     H  1     4f1+     110  + 111
   74     H  1     4f2+     112  - 113
   75     H  1     4f3+     114  + 115


  Symmetry  B2 ( 3)

   76     O  1     2py        7
   77     O  1     2py       10
   78     O  1     2py       13
   79     O  1     2py       16
   80     O  1     3d1-      19
   81     O  1     3d1-      24
   82     O  1     3d1-      29
   83     O  1     4f3-      33
   84     O  1     4f1-      35
   85     O  1     4f3-      40
   86     O  1     4f1-      42
   87     O  1     5g3-      48
   88     O  1     5g1-      50
   89     H  1     2py       66  +  67
   90     H  1     2py       72  +  73
   91     H  1     2py       78  +  79
   92     H  1     3d2-      82  -  83
   93     H  1     3d1-      84  +  85
   94     H  1     3d2-      92  -  93
   95     H  1     3d1-      94  +  95
   96     H  1     4f3-     102  + 103
   97     H  1     4f2-     104  - 105
   98     H  1     4f1-     106  + 107


  Symmetry  A2 ( 4)

   99     O  1     3d2-      18
  100     O  1     3d2-      23
  101     O  1     3d2-      28
  102     O  1     4f2-      34
  103     O  1     4f2-      41
  104     O  1     5g4-      47
  105     O  1     5g2-      49
  106     H  1     2py       66  -  67
  107     H  1     2py       72  -  73
  108     H  1     2py       78  -  79
  109     H  1     3d2-      82  +  83
  110     H  1     3d1-      84  -  85
  111     H  1     3d2-      92  +  93
  112     H  1     3d1-      94  -  95
  113     H  1     4f3-     102  - 103
  114     H  1     4f2-     104  + 105
  115     H  1     4f1-     106  - 107

  Symmetries of electric field:  B1 (2)  B2 (3)  A1 (1)

  Symmetries of magnetic field:  B2 (3)  B1 (2)  A2 (4)


 Copy of input to READIN
 -----------------------

INTGRL                                                                          
                                                                                
                                                                                
s   2    2X   Y      0.10E-14                                                   
       8.0    1    5    1    1    1    1    1                                   
O  1   0.000000000000000   0.000000000000000   0.721280660000000       *        
H  12   5                                                                       
      61420.00000000         0.00009000        -0.00002000         0.00000000   
                             0.00000000         0.00000000                      
       9199.00000000         0.00069800        -0.00015900         0.00000000   
                             0.00000000         0.00000000                      
       2091.00000000         0.00366400        -0.00082900         0.00000000   
                             0.00000000         0.00000000                      
        590.90000000         0.01521800        -0.00350800         0.00000000   
                             0.00000000         0.00000000                      
        192.30000000         0.05242300        -0.01215600         0.00000000   
                             0.00000000         0.00000000                      
         69.32000000         0.14592100        -0.03626100         0.00000000   
                             0.00000000         0.00000000                      
         26.97000000         0.30525800        -0.08299200         0.00000000   
                             0.00000000         0.00000000                      
         11.10000000         0.39850800        -0.15209000         0.00000000   
                             0.00000000         0.00000000                      
          4.68200000         0.21698000        -0.11533100         0.00000000   
                             0.00000000         0.00000000                      
          1.42800000         0.01759400         0.28897900         1.00000000   
                             0.00000000         0.00000000                      
          0.55470000        -0.00250200         0.58612800         0.00000000   
                             1.00000000         0.00000000                      
          0.20670000         0.00095400         0.27762400         0.00000000   
                             0.00000000         1.00000000                      
H   6   4                                                                       
         63.42000000         0.00604400         0.00000000         0.00000000   
                             0.00000000                                         
         14.66000000         0.04179900         0.00000000         0.00000000   
                             0.00000000                                         
          4.45900000         0.16114300         0.00000000         0.00000000   
                             0.00000000                                         
          1.53100000         0.35673100         1.00000000         0.00000000   
                             0.00000000                                         
          0.53020000         0.44830900         0.00000000         1.00000000   
                             0.00000000                                         
          0.17500000         0.24494000         0.00000000         0.00000000   
                             1.00000000                                         
H   3   3                                                                       
          3.77500000         1.00000000         0.00000000         0.00000000   
          1.30000000         0.00000000         1.00000000         0.00000000   
          0.44400000         0.00000000         0.00000000         1.00000000   
H   2   2                                                                       
          2.66600000         1.00000000         0.00000000                      
          0.85900000         0.00000000         1.00000000                      
H   1   1                                                                       
          1.84600000         1.00000000                                         
       1.0    1    4    1    1    1    1                                        
H  1  -1.419236370000000   0.000000000000000  -0.360640330000000       *        
H   6   4                                                                       
         82.64000000         0.00200600         0.00000000         0.00000000   
                             0.00000000                                         
         12.41000000         0.01534300         0.00000000         0.00000000   
                             0.00000000                                         
          2.82400000         0.07557900         0.00000000         0.00000000   
                             0.00000000                                         
          0.79770000         0.25687500         1.00000000         0.00000000   
                             0.00000000                                         
          0.25810000         0.49736800         0.00000000         1.00000000   
                             0.00000000                                         
          0.08989000         0.29613300         0.00000000         0.00000000   
                             1.00000000                                         
H   3   3                                                                       
          2.29200000         1.00000000         0.00000000         0.00000000   
          0.83800000         0.00000000         1.00000000         0.00000000   
          0.29200000         0.00000000         0.00000000         1.00000000   
H   2   2                                                                       
          2.06200000         1.00000000         0.00000000                      
          0.66200000         0.00000000         1.00000000                      
H   1   1                                                                       
          1.39700000         1.00000000                                         


 herdrv: noofopt= 5


 ************************************************************************
 ************************** Output from HERONE **************************
 ************************************************************************

 prop, itype T 1


  1319 atomic overlap integrals written in   1 buffers.
 Percentage non-zero integrals:  19.78
 prop, itype T 2


  1893 one-el. Hamil. integrals written in   1 buffers.
 Percentage non-zero integrals:  28.38
 prop, itype T 3


  1362 kinetic energy integrals written in   1 buffers.
 Percentage non-zero integrals:  20.42


 inttyp= 1noptyp= 1


                    +---------------------------------+
                    ! Integrals of operator: OVERLAP  !
                    +---------------------------------+



 finopt,noofopt,last1= 0 5 0
 inttyp= 8noptyp= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000000 !
                    +---------------------------------+



 finopt,noofopt,last1= 1 5 0
 inttyp= 8noptyp= 3


                    +---------------------------------+
                    ! Integrals of operator: CM010000 !
                    +---------------------------------+

 typea=  1typeb=  0last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000100 !
                    +---------------------------------+

 typea=  1typeb=  1last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000001 !
                    +---------------------------------+

 typea=  1typeb=  2last= 2
 lstflg= 1


 finopt,noofopt,last1= 2 5 0
 inttyp= 8noptyp= 6


                    +---------------------------------+
                    ! Integrals of operator: CM020000 !
                    +---------------------------------+

 typea=  1typeb=  3last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM010100 !
                    +---------------------------------+

 typea=  1typeb=  4last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM010001 !
                    +---------------------------------+

 typea=  1typeb=  5last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000200 !
                    +---------------------------------+

 typea=  1typeb=  6last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000101 !
                    +---------------------------------+

 typea=  1typeb=  7last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000002 !
                    +---------------------------------+

 typea=  1typeb=  8last= 2
 lstflg= 1


 finopt,noofopt,last1= 3 5 0
 inttyp= 8noptyp= 10


                    +---------------------------------+
                    ! Integrals of operator: CM030000 !
                    +---------------------------------+

 typea=  1typeb=  9last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM020100 !
                    +---------------------------------+

 typea=  1typeb=  10last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM020001 !
                    +---------------------------------+

 typea=  1typeb=  11last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM010200 !
                    +---------------------------------+

 typea=  1typeb=  12last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM010101 !
                    +---------------------------------+

 typea=  1typeb=  13last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM010002 !
                    +---------------------------------+

 typea=  1typeb=  14last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000300 !
                    +---------------------------------+

 typea=  1typeb=  15last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000201 !
                    +---------------------------------+

 typea=  1typeb=  16last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000102 !
                    +---------------------------------+

 typea=  1typeb=  17last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000003 !
                    +---------------------------------+

 typea=  1typeb=  18last= 2
 lstflg= 1


 finopt,noofopt,last1= 4 5 0
 inttyp= 8noptyp= 15


                    +---------------------------------+
                    ! Integrals of operator: CM040000 !
                    +---------------------------------+

 typea=  1typeb=  19last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM030100 !
                    +---------------------------------+

 typea=  1typeb=  20last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM030001 !
                    +---------------------------------+

 typea=  1typeb=  21last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM020200 !
                    +---------------------------------+

 typea=  1typeb=  22last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM020101 !
                    +---------------------------------+

 typea=  1typeb=  23last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM020002 !
                    +---------------------------------+

 typea=  1typeb=  24last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM010300 !
                    +---------------------------------+

 typea=  1typeb=  25last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM010201 !
                    +---------------------------------+

 typea=  1typeb=  26last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM010102 !
                    +---------------------------------+

 typea=  1typeb=  27last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM010003 !
                    +---------------------------------+

 typea=  1typeb=  28last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000400 !
                    +---------------------------------+

 typea=  1typeb=  29last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000301 !
                    +---------------------------------+

 typea=  1typeb=  30last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000202 !
                    +---------------------------------+

 typea=  1typeb=  31last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000103 !
                    +---------------------------------+

 typea=  1typeb=  32last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000004 !
                    +---------------------------------+

 typea=  1typeb=  33last= 2
 lstflg= 1


 finopt,noofopt,last1= 5 5 2
 inttyp= 18noptyp= 3


                    +---------------------------------+
                    ! Integrals of operator: XANGMOM  !
                    +---------------------------------+

 angular moment
 typea=  2typeb=  6last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: YANGMOM  !
                    +---------------------------------+

 angular moment
 typea=  2typeb=  7last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: ZANGMOM  !
                    +---------------------------------+

 angular moment
 typea=  2typeb=  8last= 2
 lstflg= 2




 ************************************************************************
 ************************** Output from TWOINT **************************
 ************************************************************************

 calling sifew2:luinta,info,num,last,nrec
 calling sifew2: 11 2 4096 3272 4096 2730 2189 2 1915

 Number of two-electron integrals written:   5230139 (23.5%)
 Kilobytes written:                            62776




 >>>> Total CPU  time used in HERMIT:  15.43 seconds
 >>>> Total wall time used in HERMIT:  16.00 seconds

- End of Integral Section
