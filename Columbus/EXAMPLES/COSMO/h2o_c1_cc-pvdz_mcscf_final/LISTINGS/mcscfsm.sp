 total ao core energy =    9.317913917
 MCSCF calculation performed for  1 DRT.

 DRT  first state   no.of aver. states   weights
  1   ground state          1             1.000

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a  
 Total number of electrons:    10
 Spin multiplicity:            1
 Number of active orbitals:    0
 Number of active electrons:   0
 Total number of CSFs:         1

 Number of active-double rotations:      0
 Number of active-active rotations:      0
 Number of double-virtual rotations:    95
 Number of active-virtual rotations:     0

 iter=    1 emc=  -76.0270328082 demc= 7.6027E+01 wnorm= 3.7760E-09 knorm= 3.9102E-10 apxde= 3.0078E-16    *not converged* 
 iter=    2 emc=  -76.0479455968 demc= 2.0913E-02 wnorm= 1.0345E-01 knorm= 2.4548E-02 apxde= 1.0580E-03    *not converged* 
 iter=    3 emc=  -76.0490119620 demc= 1.0664E-03 wnorm= 1.2708E-03 knorm= 3.8113E-04 apxde= 1.8897E-07    *not converged* 
 iter=    4 emc=  -76.0513474700 demc= 2.3355E-03 wnorm= 1.1894E-02 knorm= 2.7083E-03 apxde= 1.3199E-05    *not converged* 
 iter=    5 emc=  -76.0513844226 demc= 3.6953E-05 wnorm= 1.2989E-04 knorm= 3.1331E-05 apxde= 1.6562E-09    *not converged* 
 iter=    6 emc=  -76.0516454779 demc= 2.6106E-04 wnorm= 1.3587E-03 knorm= 3.0581E-04 apxde= 1.6971E-07    *not converged* 
 iter=    7 emc=  -76.0516485651 demc= 3.0872E-06 wnorm= 1.5265E-05 knorm= 3.4531E-06 apxde= 2.1550E-11    *not converged* 
 iter=    8 emc=  -76.0516780487 demc= 2.9484E-05 wnorm= 1.5442E-04 knorm= 3.4682E-05 apxde= 2.1852E-09    *not converged* 
 iter=    9 emc=  -76.0516783824 demc= 3.3374E-07 wnorm= 1.9594E-06 knorm= 3.9168E-07 apxde= 3.1715E-13    *not converged* 
 iter=   10 emc=  -76.0516817238 demc= 3.3414E-06 wnorm= 1.7472E-05 knorm= 3.9304E-06 apxde= 2.8101E-11    *not converged* 
 iter=   11 emc=  -76.0516817621 demc= 3.8307E-08 wnorm= 6.5260E-07 knorm= 4.9645E-08 apxde= 1.6356E-14    *not converged* 
 iter=   12 emc=  -76.0516821403 demc= 3.7819E-07 wnorm= 2.1797E-06 knorm= 4.6309E-07 apxde= 4.0824E-13    *not converged* 

 final mcscf convergence values:
 iter=   13 emc=  -76.0516821422 demc= 1.9195E-09 wnorm= 5.4906E-07 knorm= 3.4873E-08 apxde= 9.4162E-15    *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 weight 1.000000 total energy=  -76.051682142
   ------------------------------------------------------------


