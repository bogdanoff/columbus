1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 integral file titles:
 Hermit Integral Program : SIFS version  hochtor2        Thu Oct  7 15:33:21 2004
  cidrt_title                                                                    
  title                                                                          
 mofmt: formatted orbitals label=morbl   hochtor2        Thu Oct  7 15:33:21 2004
 SIFS file created by program tran.      hochtor2        Thu Oct  7 15:33:22 2004

 formula file title:
 Hermit Integral Program : SIFS version  hochtor2        Thu Oct  7 15:33:21 2004
  cidrt_title                                                                    
  title                                                                          
 mofmt: formatted orbitals label=morbl   hochtor2        Thu Oct  7 15:33:21 2004
 SIFS file created by program tran.      hochtor2        Thu Oct  7 15:33:22 2004
  cidrt_title                                                                    

 297 dimension of the ci-matrix ->>>      4656


 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -76.0479455968 -9.2970E+00  2.5188E-01  1.0015E+00  1.0000E-03
 mr-sdci #  2  1    -76.2462585708 -9.1546E+00  6.4774E-03  1.5760E-01  1.0000E-03
 mr-sdci #  3  1    -76.2521461042 -9.3471E+00  2.5856E-04  2.9323E-02  1.0000E-03
 mr-sdci #  4  1    -76.2524007743 -9.3527E+00  1.9155E-05  7.5867E-03  1.0000E-03
 mr-sdci #  5  1    -76.2524179555 -9.3529E+00  8.1750E-07  1.7118E-03  1.0000E-03
 mr-sdci #  6  1    -76.2524187801 -9.3530E+00  3.1868E-08  3.1483E-04  1.0000E-03

 mr-sdci  convergence criteria satisfied after  6 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  6  1    -76.2524187801 -9.3530E+00  3.1868E-08  3.1483E-04  1.0000E-03

 number of reference csfs (nref) is     1.  root number (iroot) is  1.

 eref      =    -76.047945596779   "relaxed" cnot**2         =   0.951733993815
 eci       =    -76.252418780138   deltae = eci - eref       =  -0.204473183359
 eci+dv1   =    -76.262287884070   dv1 = (1-cnot**2)*deltae  =  -0.009869103933
 eci+dv2   =    -76.262788383406   dv2 = dv1 / cnot**2       =  -0.010369603268
 eci+dv3   =    -76.263342359133   dv3 = dv1 / (2*cnot**2-1) =  -0.010923578995
 eci+pople =    -76.260971065732   ( 10e- scaled deltae )    =  -0.213025468954
