1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      09/20/00    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 using llenci= -1
================================================================================
four external integ    0.25 MB location: local disk    
three external inte    0.25 MB location: local disk    
four external integ    0.25 MB location: local disk    
three external inte    0.25 MB location: local disk    
diagonal integrals     0.12 MB location: local disk    
off-diagonal integr    0.28 MB location: local disk    
computed file size in DP units
fil3w:       32767
fil3x:       32767
fil4w:       32767
fil4x:       32767
ofdgint:       16380
diagint:       36855
computed file size in DP units
fil3w:      262136
fil3x:      262136
fil4w:      262136
fil4x:      262136
ofdgint:      131040
diagint:      294840
 nsubmx= 2 lenci= 14280
global arrays:        71400   (              0.54 MB)
vdisk:                    0   (              0.00 MB)
drt:                 684782   (              2.61 MB)
================================================================================
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core           99999999 DP per process
 CIUDG version 5.9.7 ( 5-Oct-2004)
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 0,
  GSET = 0,
   DAVCOR =10,
  NCOREL = 12
  FROOT = 1
  IVMODE = 3
  NBKITR = 0
  frcsub = 2
  NVBKMN = 1
  NVBKMX = 2
  RTOLBK = 1e-3,
  NITER = 30
  NVCIMN = 1
  NVCIMX = 2
  RTOLCI = 1e-3,
  IDEN  = 1
  CSFPRN = 10,
  cosmocalc = 2
  &end
 ------------------------------------------------------------------------
 froot operation modus: follow reference vector.

 ** list of control variables **
 nrfitr =   30      nvrfmx =    2      nvrfmn =    1
 lvlprt =    0      nroot  =    1      noldv  =    0      noldhv =    0
 nunitv =    1      ntype  =    0      nbkitr =    0      niter  =   30
 ivmode =    3      vout   =    0      istrt  =    0      iortls =    0
 nvbkmx =    2      ibktv  =   -1      ibkthv =   -1      frcsub =    2
 nvcimx =    2      icitv  =   -1      icithv =   -1      maxseg =    4
 iden   =    1      itran  =    0      froot  =    1      rtmode =    0
 ftcalc =    1      lrtshift=1.0000    ncouple=    1      skipso  =   F
 ncorel =   12      csfprn  =   1      ctol   = 1.00E-02  davcor  =   1


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-03    1.000E-03

 units and filenames:
    1: ( 6)    ciudgls                                                     
    2: ( 5)    ciudgin                                                     
    3: ( 7)    ciudgsm                                                     
    4: ( 4)    cihdiag                                                     
    5: ( 3)    cihpseu                                                     
    6: (10)    cihvfl                                                      
    7: (11)    civfl                                                       
    8: (12)    diagint                                                     
    9: (13)    ofdgint                                                     
   10: (17)    cidrtfl                                                     
   11: (20)    ciftdfl                                                     
   12: (21)    ciftofl                                                     
   13: (24)    ciftifl                                                     
   14: ( 8)    ciflind                                                     
   15: (15)    civin                                                       
   16: (16)    civout                                                      
   18: (18)    d1fl                                                        
   20: (53)    cifvfl                                                      
   21: (25)    ciscr4                                                      
   22: (41)    ciftotd                                                     
   23: (42)    flacpfd                                                     
   26: (26)    ciscr5                                                      
   27: (47)    restart                                                     
   28: (48)    civout                                                      
   29: (49)    cirefv                                                      
   31: (31)    fil4w                                                       
   32: (32)    fil4x                                                       
   33: (33)    fil3w                                                       
   34: (34)    fil3x                                                       
   36: (36)    aoints                                                      
   37: (37)    aoints2                                                     
   38: (38)    drtfil                                                      
   39: (50)    cisrtif                                                     
   42: (50)    mocoef                                                      
   45: (52)    nocoef_ci                                                   
   46: (53)    civfl_restart                                               
 ------------------------------------------------------------------------

 workspace allocation information: lcore=  99999999 mem1=********** ifirst=-101192552

 integral file titles:
 Hermit Integral Program : SIFS version  ronja.itc.univieFri Sep  2 14:11:46 2005
  cidrt_title                                                                    
  title                                                                          
 mofmt: formatted orbitals label=morbl   ronja.itc.univieFri Sep  2 14:11:46 2005
 SIFS file created by program tran.      ronja.itc.univieFri Sep  2 14:11:46 2005

 core energy values from the integral file:
 energy( 1)=  3.116530982443E+01, ietype=   -1,    core energy of type: Nuc.Rep.

 total core repulsion energy =  3.116530982443E+01

 drt header information:
  cidrt_title                                                                    
 spnorb, spnodd, lxyzir,hmult F F 0 0 0 0
 nmot  =    38 niot  =     9 nfct  =     0 nfvt  =     0
 nrow  =    54 nsym  =     4 ssym  =     4 lenbuf=  1600
 nwalk,xbar:        211        8       72       65       66
 nvalwt,nvalw:      207        4       72       65       66
 ncsft:           14280
 total number of valid internal walks:     207
 nvalz,nvaly,nvalx,nvalw =        4      72      65      66

 cisrt info file parameters:
 file number  12 blocksize   4095
 mxbld   4095
 nd4ext,nd2ext,nd0ext   870   522    90
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int    29040    32805    16512     4275      474        0        0        0
 minbl4,minbl3,maxbl2   441   423   444
 maxbuf 32767
 number of external orbitals per symmetry block:  13   5   8   3
 nmsym   4 number of internal orbitals   9

 formula file title:
 Hermit Integral Program : SIFS version  ronja.itc.univieFri Sep  2 14:11:46 2005
  cidrt_title                                                                    
  title                                                                          
 mofmt: formatted orbitals label=morbl   ronja.itc.univieFri Sep  2 14:11:46 2005
 SIFS file created by program tran.      ronja.itc.univieFri Sep  2 14:11:46 2005
  cidrt_title                                                                    
 file nmb.  24 block size     0
 pthz,pthy,pthx,pthw:     8    72    65    66 total internal walks:     211
 maxlp3,n3xtlp,n2xtlp,n1xtlp,n0xtlp,n2lp,n1lp,n0lp     0     0     0     0
 orbsym(*)= 1 1 1 1 1 2 3 2 3

 setref:        1 references kept,
                2 references were marked as invalid, out of
                3 total.
 limcnvrt: found 4 valid internal walksout of  8
  walks (skipping trailing invalids)
  ... adding  4 segmentation marks segtype= 1
 limcnvrt: found 72 valid internal walksout of  72
  walks (skipping trailing invalids)
  ... adding  72 segmentation marks segtype= 2
 limcnvrt: found 65 valid internal walksout of  65
  walks (skipping trailing invalids)
  ... adding  65 segmentation marks segtype= 3
 limcnvrt: found 66 valid internal walksout of  66
  walks (skipping trailing invalids)
  ... adding  66 segmentation marks segtype= 4

 number of external paths / symmetry
 vertex x     119      89     119      79
 vertex w     148      89     119      79

 lprune: l(*,*,*) pruned with nwalk=       8 nvalwt=       4 nprune=       3

 lprune: l(*,*,*) pruned with nwalk=      72 nvalwt=      72 nprune=       0

 lprune: l(*,*,*) pruned with nwalk=      65 nvalwt=      65 nprune=      19

 lprune: l(*,*,*) pruned with nwalk=      66 nvalwt=      66 nprune=      28



                   segmentation summary for type diagonal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1           6|         4|         0|         4|         0|         1|
 -------------------------------------------------------------------------------
  Y 2          72|       522|         4|        72|         4|         2|
 -------------------------------------------------------------------------------
  X 3          65|      6755|       526|        65|        76|         3|
 -------------------------------------------------------------------------------
  W 4          66|      6999|      7281|        66|       141|         4|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>     14280

                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      65       6       6755          4      65       4
     2  4   1    25      two-ext wz   2X  4 1      66       6       6999          4      66       4
     3  4   3    26      two-ext wx   2X  4 3      66      65       6999       6755      66      65
     4  2   1    11      one-ext yz   1X  2 1      72       6        522          4      72       4
     5  3   2    15      1ex3ex  yx   3X  3 2      65      72       6755        522      65      72
     6  4   2    16      1ex3ex  yw   3X  4 2      66      72       6999        522      66      72
     7  1   1     1      allint zz    OX  1 1       6       6          4          4       4       4
     8  2   2     5      0ex2ex yy    OX  2 2      72      72        522        522      72      72
     9  3   3     6      0ex2ex xx    OX  3 3      65      65       6755       6755      65      65
    10  4   4     7      0ex2ex ww    OX  4 4      66      66       6999       6999      66      66
    11  1   1    75      dg-024ext z  DG  1 1       6       6          4          4       4       4
    12  2   2    45      4exdg024 y   DG  2 2      72      72        522        522      72      72
    13  3   3    46      4exdg024 x   DG  3 3      65      65       6755       6755      65      65
    14  4   4    47      4exdg024 w   DG  4 4      66      66       6999       6999      66      66
----------------------------------------------------------------------------------------------------
 DIAGEL COUNTS: DG0X,DG2X,DG4X= 8300 1713 203
 diagonal elements written to file   4

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       180 2x:         0 4x:         0
All internal counts: zz :        34 yy:         0 xx:         0 ww:         0
One-external counts: yz :         0 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         0    task #   2:         0    task #   3:         0    task #   4:         0
task #   5:         0    task #   6:         0    task #   7:        33    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:       122    task #  12:         0
task #  13:         0    task #  14:         0    task #
 reference space has dimension       1

    root           eigenvalues
    ----           ------------
       1        -113.7294417015

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt= 4

         vector  1 from unit 11 written to unit 49 filename cirefv                                                      
 ufvoutnew: ... writing  recamt= 4

         vector  1 from unit 11 written to unit 49 filename cirefv                                                      

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:             14280
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               2
 one root will be followed:                               1
 number of roots to converge:                             1
 number of iterations:                                   30
 residual norm convergence criteria:               0.001000

 elast before the main iterative loop 
 elast =  -113.729442


 sum of negative charges at the beginning =  -0.051460829
 sum of positive charges at the beggining =   0.051324801
 total sum at the beginning = -0.000136028


 fepsi =   1.
 fn2 =   0.338886685

 edielcorr =  -0.008277189

 qcossave*fepsi = 
  0.00152723  0.005658251  0.002798856  0.000975195  0.00577706  0.003674078
  0.004750821  0.002436246  0.00404178  0.003928031  0.007163661  0.003189795
 -0.000261484 -0.002327967 -0.00116029 -0.002744481 -0.000145108 -0.002325229
 -0.003402432 -0.001470436 -0.000156707 -0.002704921 -0.002620109 -0.000359688
 -0.001589095 -0.00055071 -0.00044257 -0.001393994 -0.002351766 -0.002211492
 -0.001993475 -0.002893342 -0.002685644 -0.001406263 -0.001781974  0.000128895
  0.000586373  0.001574635  0.001085057  0.002028837 -0.000488346 -0.001271817
 -0.001310502 -0.001511979 -0.001517367 -0.000496505 -0.001236884 -0.00110013
 -0.000773486 -0.001464238 -0.001310398

 phizero = 
 -0.0426247506 -0.0568074777 -0.0540181659 -0.0658131902 -0.0632380449
 -0.0471348331 -0.0535845994 -0.0329436518 -0.0433659777 -0.0466243252
 -0.0595459636 -0.0385588172 -0.00202700446  0.0251795421  0.0260330309
  0.0430862985 -0.00168368219  0.0397449072  0.0336447005  0.0139684299
 -0.00324113921  0.026407011  0.0122299292 -0.000642991633  0.0377471339
  0.0104896742 -0.00137221323  0.0372698298  0.0245273914  0.0352725498
  0.0429425464  0.0332200639  0.0390771649  0.0283157909  0.0405232301
 -0.0100787258 -0.0140790819 -0.0193755228 -0.0230644201 -0.0314505824
  0.0327887728  0.0269704498  0.020245921  0.0467613329  0.0430919231
  0.0360817168  0.0285394546  0.0172570268  0.0470630555  0.0451154085
  0.0388653246


 qcossave is copied to qcoszero 


 edielnew at the beginning 
 edielnew =  -0.00206929726


          starting ci iteration   1

 cosmocalc =  2
 rootcalc =  1
 repnuc =   31.1653098

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= T F

################################################################################
 one electron density matrix calculation 
################################################################################


 workspace allocation information: mxcore=99999996

 space required:

    space required for calls in multd2:
       onex          247
       allin           0
       diagon        740
    max.      ---------
       maxnex        740

    total core space usage:
       maxnex        740
    max k-seg       6999
    max k-ind         72
              ---------
       totmax      14882
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=      28  DYX=     285  DYW=     277
   D0Z=       4  D0Y=     115  D0X=      92  D0W=      90
  DDZI=      36 DDYI=     626 DDXI=     550 DDWI=     537
  DDZE=       0 DDYE=      72 DDXE=      65 DDWE=      66
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   2.0000000      2.0000000      2.0000000      2.0000000      2.0000000
   0.0000000      0.0000000      0.0000000      0.0000000      0.0000000
   0.0000000      0.0000000      0.0000000      0.0000000      0.0000000
   0.0000000      0.0000000      0.0000000


*****   symmetry block SYM2   *****

 occupation numbers of nos
   2.0000000      1.0000000      0.0000000      0.0000000      0.0000000
   0.0000000      0.0000000


*****   symmetry block SYM3   *****

 occupation numbers of nos
   2.0000000      1.0000000      0.0000000      0.0000000      0.0000000
   0.0000000      0.0000000      0.0000000      0.0000000      0.0000000


*****   symmetry block SYM4   *****

 occupation numbers of nos
   0.0000000      0.0000000      0.0000000


 total number of electrons =   16.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = "99999996"

 Work memory size (LMWORK) :    99999996 =  762.94 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***

  Confirmation of dielectric energy for excited state

  edielnew =   0.

 **************************************************
 ***Cosmo calculation for excited states***********
 **************************************************

 fn2 =   0.338886685
 qcoszero = q0*fepsi + fn2*q(delta) is done 

 root =  1


 DALTON: user specified work memory size used,
          -m = "99999996"

 Work memory size (LMWORK) :    99999996 =  762.94 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 Warning! Large contrib. to nucrep.The distance is   0.017707087
 Warning! Large contrib. to nucrep.The distance is   0.000353311222
 ** Total nuclear repulsion energy ** =  31.22868438015729
 Warning! Large contrib. to nucrep.The distance is   0.017707087
 Warning! Large contrib. to nucrep.The distance is   0.000353311222
 screening nuclear repulsion energy   0.00636339274

 Total-screening nuclear repulsion energy   31.222321


 Adding T+Vsolv ...
 maxdens  260
 *** End of DALTON-COSMO calculation ***

 DIAGEL COUNTS: DG0X,DG2X,DG4X= 8300 1713 203
 =========== Executing IN-CORE method ==========
 norm multnew:  1.


====================================================================================================
Diagonal     counts:  0x:      8300 2x:      1713 4x:       203
All internal counts: zz :        34 yy:      1315 xx:      1079 ww:       968
One-external counts: yz :       452 yx:      4545 yw:      4311
Two-external counts: yy :      1362 ww:       922 xx:      1105 xz:        51 wz:        61 wx:       990
Three-ext.   counts: yx :       682 yw:       664

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        50    task #   2:        60    task #   3:       704    task #   4:       412
task #   5:      4069    task #   6:      3725    task #   7:        33    task #   8:       972
task #   9:       877    task #  10:       671    task #  11:       122    task #  12:         1
task #  13:         1    task #  14:         1    task #
 rtolcosmo = eo,e,r,ro   0. -144.894752 -144.955543  31.222321  31.1653098
 calctciref: ... reading 
 cirefv                                                       recamt= 4

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000
 follow root  1 (overlap=  1.)

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -113.7332219980  3.7803E-03  4.1354E-01  1.0957E+00  1.0000E-03


dielectric energy =      -0.0028416007
delta diel. energy =       0.0028416007
e(nroot) + repnuc - ediel-edielcorr =              elast =    -113.7221032083
delta Cosmo energy =     113.7221032083

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.00   0.00   0.00   0.00         0.    0.0000
    2   25    0   0.01   0.00   0.00   0.01         0.    0.0000
    3   26    0   0.01   0.00   0.00   0.01         0.    0.0000
    4   11    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   15    0   0.02   0.00   0.00   0.01         0.    0.0000
    6   16    0   0.01   0.01   0.00   0.01         0.    0.0000
    7    1    0   0.00   0.00   0.00   0.00         0.    0.0000
    8    5    0   0.01   0.00   0.00   0.01         0.    0.0000
    9    6    0   0.02   0.00   0.00   0.02         0.    0.0000
   10    7    0   0.02   0.00   0.00   0.02         0.    0.0000
   11   75    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.01   0.00   0.00   0.01         0.    0.0000
   14   47    0   0.00   0.00   0.00   0.00         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1100s 
time spent in multnx:                   0.1000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0100s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.8500s 

          starting ci iteration   2

 =========== Executing IN-CORE method ==========
 norm multnew:  0.234113246


====================================================================================================
Diagonal     counts:  0x:      8300 2x:      1713 4x:       203
All internal counts: zz :        34 yy:      1315 xx:      1079 ww:       968
One-external counts: yz :       452 yx:      4545 yw:      4311
Two-external counts: yy :      1362 ww:       922 xx:      1105 xz:        51 wz:        61 wx:       990
Three-ext.   counts: yx :       682 yw:       664

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        50    task #   2:        60    task #   3:       704    task #   4:       412
task #   5:      4069    task #   6:      3725    task #   7:        33    task #   8:       972
task #   9:       877    task #  10:       671    task #  11:       122    task #  12:         1
task #  13:         1    task #  14:         1    task #
 calctciref: ... reading 
 cirefv                                                       recamt= 4

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.94985487    -0.31269111
 follow root  1 (overlap=  0.949854867)

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1   -114.0145817862  2.8136E-01  3.8450E-02  3.2538E-01  1.0000E-03
 mr-sdci #  2  2   -111.1369783022  1.4236E+02  0.0000E+00  1.7561E+00  1.0000E-04


dielectric energy =      -0.0028416007
delta diel. energy =       0.0000000000
e(nroot) + repnuc - ediel-edielcorr =              elast =    -114.0034629965
delta Cosmo energy =       0.2813597882

 root number  1 is used to define the new expansion vector.
 diagon: frcsub=2, new matrix-vector products will be computed.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.00   0.00   0.00   0.00         0.    0.0000
    2   25    0   0.01   0.00   0.00   0.01         0.    0.0000
    3   26    0   0.01   0.00   0.00   0.01         0.    0.0000
    4   11    0   0.01   0.00   0.00   0.01         0.    0.0000
    5   15    0   0.01   0.01   0.00   0.01         0.    0.0000
    6   16    0   0.02   0.00   0.00   0.02         0.    0.0000
    7    1    0   0.00   0.00   0.00   0.00         0.    0.0000
    8    5    0   0.01   0.00   0.00   0.01         0.    0.0000
    9    6    0   0.02   0.01   0.00   0.02         0.    0.0000
   10    7    0   0.01   0.00   0.00   0.01         0.    0.0000
   11   75    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.01   0.01   0.00   0.01         0.    0.0000
   14   47    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0300s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

          starting ci iteration   3

 cosmocalc =  2
 rootcalc =  1
 repnuc =   31.222321

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=      28  DYX=     285  DYW=     277
   D0Z=       4  D0Y=     115  D0X=      92  D0W=      90
  DDZI=      36 DDYI=     626 DDXI=     550 DDWI=     537
  DDZE=       0 DDYE=      72 DDXE=      65 DDWE=      66
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   2.0000000      2.0000000      1.9919265      1.9841286      1.9768007
   0.0170335      0.0099194      0.0060544      0.0039811      0.0037185
   0.0021447      0.0014822      0.0007270      0.0005839      0.0003344
   0.0001612      0.0001100      0.0000621


*****   symmetry block SYM2   *****

 occupation numbers of nos
   1.9659134      1.0111544      0.0096071      0.0062233      0.0033040
   0.0008379      0.0003336


*****   symmetry block SYM3   *****

 occupation numbers of nos
   1.9639997      1.0087059      0.0139888      0.0055143      0.0033321
   0.0012861      0.0006646      0.0004895      0.0002169      0.0001087


*****   symmetry block SYM4   *****

 occupation numbers of nos
   0.0032520      0.0015890      0.0003102


 total number of electrons =   16.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = "99999996"

 Work memory size (LMWORK) :    99999996 =  762.94 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***

  Confirmation of dielectric energy for excited state

  edielnew =  -0.000496222107

 **************************************************
 ***Cosmo calculation for excited states***********
 **************************************************

 fn2 =   0.338886685
 qcoszero = q0*fepsi + fn2*q(delta) is done 

 root =  1


 DALTON: user specified work memory size used,
          -m = "99999996"

 Work memory size (LMWORK) :    99999996 =  762.94 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 Warning! Large contrib. to nucrep.The distance is   0.017707087
 Warning! Large contrib. to nucrep.The distance is   0.000353311222
 ** Total nuclear repulsion energy ** =  31.24034222858971
 Warning! Large contrib. to nucrep.The distance is   0.017707087
 Warning! Large contrib. to nucrep.The distance is   0.000353311222
 screening nuclear repulsion energy   0.00831408191

 Total-screening nuclear repulsion energy   31.2320281


 Adding T+Vsolv ...
 maxdens  260
 *** End of DALTON-COSMO calculation ***

 DIAGEL COUNTS: DG0X,DG2X,DG4X= 8300 1713 203
 =========== Executing IN-CORE method ==========
 norm multnew:  1.


====================================================================================================
Diagonal     counts:  0x:      8300 2x:      1713 4x:       203
All internal counts: zz :        34 yy:      1315 xx:      1079 ww:       968
One-external counts: yz :       452 yx:      4545 yw:      4311
Two-external counts: yy :      1362 ww:       922 xx:      1105 xz:        51 wz:        61 wx:       990
Three-ext.   counts: yx :       682 yw:       664

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        50    task #   2:        60    task #   3:       704    task #   4:       412
task #   5:      4069    task #   6:      3725    task #   7:        33    task #   8:       972
task #   9:       877    task #  10:       671    task #  11:       122    task #  12:         1
task #  13:         1    task #  14:         1    task #
 =========== Executing IN-CORE method ==========
 norm multnew:  0.0225721878


====================================================================================================
Diagonal     counts:  0x:      8300 2x:      1713 4x:       203
All internal counts: zz :        34 yy:      1315 xx:      1079 ww:       968
One-external counts: yz :       452 yx:      4545 yw:      4311
Two-external counts: yy :      1362 ww:       922 xx:      1105 xz:        51 wz:        61 wx:       990
Three-ext.   counts: yx :       682 yw:       664

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        50    task #   2:        60    task #   3:       704    task #   4:       412
task #   5:      4069    task #   6:      3725    task #   7:        33    task #   8:       972
task #   9:       877    task #  10:       671    task #  11:       122    task #  12:         1
task #  13:         1    task #  14:         1    task #
 rtolcosmo = eo,e,r,ro   0. -145.236903 -145.273687  31.2320281  31.222321
 rtolcosmo = eo,e,r,ro   0. -142.359299 -142.710297  31.2320281  31.222321
 calctciref: ... reading 
 cirefv                                                       recamt= 4

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.95744932     0.02666511
 follow root  1 (overlap=  0.957449318)

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1   -114.0416591682  2.7077E-02  4.4436E-03  9.7739E-02  1.0000E-03
 mr-sdci #  3  2   -111.4782685068  3.4129E-01  0.0000E+00  1.6666E+00  1.0000E-04


dielectric energy =      -0.0014755321
delta diel. energy =      -0.0013660686
e(nroot) + repnuc - ediel-edielcorr =              elast =    -114.0319064471
delta Cosmo energy =       0.0284434506

 root number  1 is used to define the new expansion vector.
 diagon: frcsub=2, new matrix-vector products will be computed.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.01   0.00   0.00   0.01         0.    0.0000
    2   25    0   0.00   0.00   0.00   0.00         0.    0.0000
    3   26    0   0.02   0.00   0.00   0.02         0.    0.0000
    4   11    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   15    0   0.01   0.00   0.00   0.01         0.    0.0000
    6   16    0   0.02   0.00   0.00   0.01         0.    0.0000
    7    1    0   0.00   0.00   0.00   0.00         0.    0.0000
    8    5    0   0.01   0.00   0.00   0.01         0.    0.0000
    9    6    0   0.02   0.01   0.00   0.02         0.    0.0000
   10    7    0   0.01   0.00   0.00   0.01         0.    0.0000
   11   75    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.01   0.01   0.00   0.01         0.    0.0000
   14   47    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0200s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.9700s 

          starting ci iteration   4

 cosmocalc =  2
 rootcalc =  1
 repnuc =   31.2320281

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=      28  DYX=     285  DYW=     277
   D0Z=       4  D0Y=     115  D0X=      92  D0W=      90
  DDZI=      36 DDYI=     626 DDXI=     550 DDWI=     537
  DDZE=       0 DDYE=      72 DDXE=      65 DDWE=      66
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   2.0000000      2.0000000      1.9887039      1.9795946      1.9745454
   0.0174213      0.0128707      0.0071346      0.0049664      0.0047321
   0.0032119      0.0022190      0.0010521      0.0008333      0.0004616
   0.0002272      0.0001245      0.0000742


*****   symmetry block SYM2   *****

 occupation numbers of nos
   1.9745677      0.9994995      0.0098226      0.0064293      0.0042940
   0.0012829      0.0005248


*****   symmetry block SYM3   *****

 occupation numbers of nos
   1.9674005      1.0016803      0.0147149      0.0059671      0.0040980
   0.0020028      0.0010282      0.0006642      0.0002882      0.0001229


*****   symmetry block SYM4   *****

 occupation numbers of nos
   0.0043771      0.0025980      0.0004642


 total number of electrons =   16.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = "99999996"

 Work memory size (LMWORK) :    99999996 =  762.94 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***

  Confirmation of dielectric energy for excited state

  edielnew =  -0.000391651159

 **************************************************
 ***Cosmo calculation for excited states***********
 **************************************************

 fn2 =   0.338886685
 qcoszero = q0*fepsi + fn2*q(delta) is done 

 root =  1


 DALTON: user specified work memory size used,
          -m = "99999996"

 Work memory size (LMWORK) :    99999996 =  762.94 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 Warning! Large contrib. to nucrep.The distance is   0.017707087
 Warning! Large contrib. to nucrep.The distance is   0.000353311222
 ** Total nuclear repulsion energy ** =  31.23879246895855
 Warning! Large contrib. to nucrep.The distance is   0.017707087
 Warning! Large contrib. to nucrep.The distance is   0.000353311222
 screening nuclear repulsion energy   0.00796391843

 Total-screening nuclear repulsion energy   31.2308286


 Adding T+Vsolv ...
 maxdens  260
 *** End of DALTON-COSMO calculation ***

 DIAGEL COUNTS: DG0X,DG2X,DG4X= 8300 1713 203
 =========== Executing IN-CORE method ==========
 norm multnew:  1.


====================================================================================================
Diagonal     counts:  0x:      8300 2x:      1713 4x:       203
All internal counts: zz :        34 yy:      1315 xx:      1079 ww:       968
One-external counts: yz :       452 yx:      4545 yw:      4311
Two-external counts: yy :      1362 ww:       922 xx:      1105 xz:        51 wz:        61 wx:       990
Three-ext.   counts: yx :       682 yw:       664

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        50    task #   2:        60    task #   3:       704    task #   4:       412
task #   5:      4069    task #   6:      3725    task #   7:        33    task #   8:       972
task #   9:       877    task #  10:       671    task #  11:       122    task #  12:         1
task #  13:         1    task #  14:         1    task #
 =========== Executing IN-CORE method ==========
 norm multnew:  0.00581829298


====================================================================================================
Diagonal     counts:  0x:      8300 2x:      1713 4x:       203
All internal counts: zz :        34 yy:      1315 xx:      1079 ww:       968
One-external counts: yz :       452 yx:      4545 yw:      4311
Two-external counts: yy :      1362 ww:       922 xx:      1105 xz:        51 wz:        61 wx:       990
Three-ext.   counts: yx :       682 yw:       664

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        50    task #   2:        60    task #   3:       704    task #   4:       412
task #   5:      4069    task #   6:      3725    task #   7:        33    task #   8:       972
task #   9:       877    task #  10:       671    task #  11:       122    task #  12:         1
task #  13:         1    task #  14:         1    task #
 rtolcosmo = eo,e,r,ro   0. -145.273687 -145.274898  31.2308286  31.2320281
 rtolcosmo = eo,e,r,ro   0. -142.710297 -142.742931  31.2308286  31.2320281
 calctciref: ... reading 
 cirefv                                                       recamt= 4

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.94922567    -0.27442224
 follow root  1 (overlap=  0.949225666)

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1   -114.0440693049  2.4101E-03  5.8018E-04  3.9746E-02  1.0000E-03
 mr-sdci #  4  2   -111.5121019623  3.3833E-02  0.0000E+00  1.7830E+00  1.0000E-04


dielectric energy =      -0.0016688937
delta diel. energy =       0.0001933616
e(nroot) + repnuc - ediel-edielcorr =              elast =    -114.0341232222
delta Cosmo energy =       0.0022167751

 root number  1 is used to define the new expansion vector.
 diagon: frcsub=2, new matrix-vector products will be computed.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.01   0.00   0.00   0.01         0.    0.0000
    2   25    0   0.00   0.00   0.00   0.00         0.    0.0000
    3   26    0   0.02   0.00   0.00   0.02         0.    0.0000
    4   11    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   15    0   0.01   0.00   0.00   0.01         0.    0.0000
    6   16    0   0.02   0.00   0.00   0.01         0.    0.0000
    7    1    0   0.00   0.00   0.00   0.00         0.    0.0000
    8    5    0   0.01   0.00   0.00   0.01         0.    0.0000
    9    6    0   0.02   0.01   0.00   0.02         0.    0.0000
   10    7    0   0.01   0.00   0.00   0.01         0.    0.0000
   11   75    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.01   0.01   0.00   0.01         0.    0.0000
   14   47    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0200s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.9700s 

          starting ci iteration   5

 cosmocalc =  2
 rootcalc =  1
 repnuc =   31.2308286

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=      28  DYX=     285  DYW=     277
   D0Z=       4  D0Y=     115  D0X=      92  D0W=      90
  DDZI=      36 DDYI=     626 DDXI=     550 DDWI=     537
  DDZE=       0 DDYE=      72 DDXE=      65 DDWE=      66
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   2.0000000      2.0000000      1.9867526      1.9762977      1.9703396
   0.0206778      0.0152307      0.0082011      0.0055648      0.0053567
   0.0036939      0.0025625      0.0012375      0.0009572      0.0005328
   0.0002583      0.0001409      0.0000832


*****   symmetry block SYM2   *****

 occupation numbers of nos
   1.9705811      1.0000551      0.0109050      0.0072368      0.0049462
   0.0014861      0.0006150


*****   symmetry block SYM3   *****

 occupation numbers of nos
   1.9625222      1.0016541      0.0175674      0.0066681      0.0046255
   0.0023210      0.0012069      0.0007674      0.0003335      0.0001360


*****   symmetry block SYM4   *****

 occupation numbers of nos
   0.0049850      0.0029550      0.0005451


 total number of electrons =   16.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = "99999996"

 Work memory size (LMWORK) :    99999996 =  762.94 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***

  Confirmation of dielectric energy for excited state

  edielnew =  -0.000399671488

 **************************************************
 ***Cosmo calculation for excited states***********
 **************************************************

 fn2 =   0.338886685
 qcoszero = q0*fepsi + fn2*q(delta) is done 

 root =  1


 DALTON: user specified work memory size used,
          -m = "99999996"

 Work memory size (LMWORK) :    99999996 =  762.94 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 Warning! Large contrib. to nucrep.The distance is   0.017707087
 Warning! Large contrib. to nucrep.The distance is   0.000353311222
 ** Total nuclear repulsion energy ** =  31.24027158092803
 Warning! Large contrib. to nucrep.The distance is   0.017707087
 Warning! Large contrib. to nucrep.The distance is   0.000353311222
 screening nuclear repulsion energy   0.00820938781

 Total-screening nuclear repulsion energy   31.2320622


 Adding T+Vsolv ...
 maxdens  260
 *** End of DALTON-COSMO calculation ***

 DIAGEL COUNTS: DG0X,DG2X,DG4X= 8300 1713 203
 =========== Executing IN-CORE method ==========
 norm multnew:  1.


====================================================================================================
Diagonal     counts:  0x:      8300 2x:      1713 4x:       203
All internal counts: zz :        34 yy:      1315 xx:      1079 ww:       968
One-external counts: yz :       452 yx:      4545 yw:      4311
Two-external counts: yy :      1362 ww:       922 xx:      1105 xz:        51 wz:        61 wx:       990
Three-ext.   counts: yx :       682 yw:       664

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        50    task #   2:        60    task #   3:       704    task #   4:       412
task #   5:      4069    task #   6:      3725    task #   7:        33    task #   8:       972
task #   9:       877    task #  10:       671    task #  11:       122    task #  12:         1
task #  13:         1    task #  14:         1    task #
 =========== Executing IN-CORE method ==========
 norm multnew:  0.000327444997


====================================================================================================
Diagonal     counts:  0x:      8300 2x:      1713 4x:       203
All internal counts: zz :        34 yy:      1315 xx:      1079 ww:       968
One-external counts: yz :       452 yx:      4545 yw:      4311
Two-external counts: yy :      1362 ww:       922 xx:      1105 xz:        51 wz:        61 wx:       990
Three-ext.   counts: yx :       682 yw:       664

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        50    task #   2:        60    task #   3:       704    task #   4:       412
task #   5:      4069    task #   6:      3725    task #   7:        33    task #   8:       972
task #   9:       877    task #  10:       671    task #  11:       122    task #  12:         1
task #  13:         1    task #  14:         1    task #
 rtolcosmo = eo,e,r,ro   0. -145.274898 -145.2767  31.2320622  31.2308286
 rtolcosmo = eo,e,r,ro   0. -142.742931 -142.69769  31.2320622  31.2308286
 calctciref: ... reading 
 cirefv                                                       recamt= 4

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.95069164     0.10314963
 follow root  1 (overlap=  0.950691641)

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1   -114.0446380890  5.6878E-04  1.9507E-04  1.8151E-02  1.0000E-03
 mr-sdci #  5  2   -111.4656275225 -4.6474E-02  0.0000E+00  1.5609E+00  1.0000E-04


dielectric energy =      -0.0015337225
delta diel. energy =      -0.0001351711
e(nroot) + repnuc - ediel-edielcorr =              elast =    -114.0348271775
delta Cosmo energy =       0.0007039552

 root number  1 is used to define the new expansion vector.
 diagon: frcsub=2, new matrix-vector products will be computed.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.00   0.00   0.00   0.00         0.    0.0000
    2   25    0   0.00   0.00   0.00   0.00         0.    0.0000
    3   26    0   0.02   0.00   0.00   0.01         0.    0.0000
    4   11    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   15    0   0.02   0.00   0.00   0.01         0.    0.0000
    6   16    0   0.01   0.00   0.00   0.01         0.    0.0000
    7    1    0   0.01   0.00   0.00   0.01         0.    0.0000
    8    5    0   0.00   0.00   0.00   0.00         0.    0.0000
    9    6    0   0.02   0.00   0.00   0.02         0.    0.0000
   10    7    0   0.02   0.00   0.00   0.02         0.    0.0000
   11   75    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.01   0.00   0.00   0.01         0.    0.0000
   14   47    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.9900s 

          starting ci iteration   6

 cosmocalc =  2
 rootcalc =  1
 repnuc =   31.2320622

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=      28  DYX=     285  DYW=     277
   D0Z=       4  D0Y=     115  D0X=      92  D0W=      90
  DDZI=      36 DDYI=     626 DDXI=     550 DDWI=     537
  DDZE=       0 DDYE=      72 DDXE=      65 DDWE=      66
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   2.0000000      2.0000000      1.9868788      1.9768038      1.9708651
   0.0204919      0.0151655      0.0079815      0.0053379      0.0051237
   0.0036116      0.0025140      0.0012230      0.0009287      0.0005202
   0.0002483      0.0001365      0.0000790


*****   symmetry block SYM2   *****

 occupation numbers of nos
   1.9724466      0.9994370      0.0102718      0.0068522      0.0048003
   0.0014610      0.0006094


*****   symmetry block SYM3   *****

 occupation numbers of nos
   1.9639986      1.0010679      0.0174610      0.0063498      0.0044206
   0.0022823      0.0011989      0.0007502      0.0003255      0.0001265


*****   symmetry block SYM4   *****

 occupation numbers of nos
   0.0048050      0.0028870      0.0005390


 total number of electrons =   16.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = "99999996"

 Work memory size (LMWORK) :    99999996 =  762.94 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***

  Confirmation of dielectric energy for excited state

  edielnew =  -0.000382605818

 **************************************************
 ***Cosmo calculation for excited states***********
 **************************************************

 fn2 =   0.338886685
 qcoszero = q0*fepsi + fn2*q(delta) is done 

 root =  1


 DALTON: user specified work memory size used,
          -m = "99999996"

 Work memory size (LMWORK) :    99999996 =  762.94 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 Warning! Large contrib. to nucrep.The distance is   0.017707087
 Warning! Large contrib. to nucrep.The distance is   0.000353311222
 ** Total nuclear repulsion energy ** =  31.24032006036584
 Warning! Large contrib. to nucrep.The distance is   0.017707087
 Warning! Large contrib. to nucrep.The distance is   0.000353311222
 screening nuclear repulsion energy   0.00819953895

 Total-screening nuclear repulsion energy   31.2321205


 Adding T+Vsolv ...
 maxdens  260
 *** End of DALTON-COSMO calculation ***

 DIAGEL COUNTS: DG0X,DG2X,DG4X= 8300 1713 203
 =========== Executing IN-CORE method ==========
 norm multnew:  1.


====================================================================================================
Diagonal     counts:  0x:      8300 2x:      1713 4x:       203
All internal counts: zz :        34 yy:      1315 xx:      1079 ww:       968
One-external counts: yz :       452 yx:      4545 yw:      4311
Two-external counts: yy :      1362 ww:       922 xx:      1105 xz:        51 wz:        61 wx:       990
Three-ext.   counts: yx :       682 yw:       664

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        50    task #   2:        60    task #   3:       704    task #   4:       412
task #   5:      4069    task #   6:      3725    task #   7:        33    task #   8:       972
task #   9:       877    task #  10:       671    task #  11:       122    task #  12:         1
task #  13:         1    task #  14:         1    task #
 =========== Executing IN-CORE method ==========
 norm multnew:  0.000278243914


====================================================================================================
Diagonal     counts:  0x:      8300 2x:      1713 4x:       203
All internal counts: zz :        34 yy:      1315 xx:      1079 ww:       968
One-external counts: yz :       452 yx:      4545 yw:      4311
Two-external counts: yy :      1362 ww:       922 xx:      1105 xz:        51 wz:        61 wx:       990
Three-ext.   counts: yx :       682 yw:       664

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        50    task #   2:        60    task #   3:       704    task #   4:       412
task #   5:      4069    task #   6:      3725    task #   7:        33    task #   8:       972
task #   9:       877    task #  10:       671    task #  11:       122    task #  12:         1
task #  13:         1    task #  14:         1    task #
 rtolcosmo = eo,e,r,ro   0. -145.2767 -145.27687  31.2321205  31.2320622
 rtolcosmo = eo,e,r,ro   0. -142.69769 -143.218835  31.2321205  31.2320622
 calctciref: ... reading 
 cirefv                                                       recamt= 4

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.94865621    -0.27562620
 follow root  1 (overlap=  0.948656208)

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  6  1   -114.0447490049  1.1092E-04  4.0637E-05  1.0108E-02  1.0000E-03
 mr-sdci #  6  2   -111.9867147956  5.2109E-01  0.0000E+00  1.6710E+00  1.0000E-04


dielectric energy =      -0.0015271399
delta diel. energy =      -0.0000065826
e(nroot) + repnuc - ediel-edielcorr =              elast =    -114.0349446759
delta Cosmo energy =       0.0001174985

 root number  1 is used to define the new expansion vector.
 diagon: frcsub=2, new matrix-vector products will be computed.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.00   0.00   0.00   0.00         0.    0.0000
    2   25    0   0.01   0.00   0.00   0.01         0.    0.0000
    3   26    0   0.01   0.00   0.00   0.01         0.    0.0000
    4   11    0   0.01   0.00   0.00   0.01         0.    0.0000
    5   15    0   0.01   0.01   0.00   0.01         0.    0.0000
    6   16    0   0.01   0.00   0.00   0.01         0.    0.0000
    7    1    0   0.01   0.00   0.00   0.01         0.    0.0000
    8    5    0   0.00   0.00   0.00   0.00         0.    0.0000
    9    6    0   0.02   0.00   0.00   0.01         0.    0.0000
   10    7    0   0.02   0.00   0.00   0.02         0.    0.0000
   11   75    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.01   0.00   0.00   0.01         0.    0.0000
   14   47    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.9800s 

          starting ci iteration   7

 cosmocalc =  2
 rootcalc =  1
 repnuc =   31.2321205

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=      28  DYX=     285  DYW=     277
   D0Z=       4  D0Y=     115  D0X=      92  D0W=      90
  DDZI=      36 DDYI=     626 DDXI=     550 DDWI=     537
  DDZE=       0 DDYE=      72 DDXE=      65 DDWE=      66
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   2.0000000      2.0000000      1.9864919      1.9760627      1.9698869
   0.0212924      0.0157362      0.0082410      0.0054607      0.0052371
   0.0036882      0.0025677      0.0012511      0.0009500      0.0005324
   0.0002527      0.0001401      0.0000807


*****   symmetry block SYM2   *****

 occupation numbers of nos
   1.9715307      0.9995876      0.0105825      0.0070308      0.0049214
   0.0014917      0.0006218


*****   symmetry block SYM3   *****

 occupation numbers of nos
   1.9627729      1.0011496      0.0182152      0.0065095      0.0045309
   0.0023302      0.0012236      0.0007693      0.0003341      0.0001288


*****   symmetry block SYM4   *****

 occupation numbers of nos
   0.0049038      0.0029424      0.0005512


 total number of electrons =   16.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = "99999996"

 Work memory size (LMWORK) :    99999996 =  762.94 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***

  Confirmation of dielectric energy for excited state

  edielnew =  -0.0003777913

 **************************************************
 ***Cosmo calculation for excited states***********
 **************************************************

 fn2 =   0.338886685
 qcoszero = q0*fepsi + fn2*q(delta) is done 

 root =  1


 DALTON: user specified work memory size used,
          -m = "99999996"

 Work memory size (LMWORK) :    99999996 =  762.94 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 Warning! Large contrib. to nucrep.The distance is   0.017707087
 Warning! Large contrib. to nucrep.The distance is   0.000353311222
 ** Total nuclear repulsion energy ** =  31.24065358616039
 Warning! Large contrib. to nucrep.The distance is   0.017707087
 Warning! Large contrib. to nucrep.The distance is   0.000353311222
 screening nuclear repulsion energy   0.00825350759

 Total-screening nuclear repulsion energy   31.2324001


 Adding T+Vsolv ...
 maxdens  260
 *** End of DALTON-COSMO calculation ***

 DIAGEL COUNTS: DG0X,DG2X,DG4X= 8300 1713 203
 =========== Executing IN-CORE method ==========
 norm multnew:  1.


====================================================================================================
Diagonal     counts:  0x:      8300 2x:      1713 4x:       203
All internal counts: zz :        34 yy:      1315 xx:      1079 ww:       968
One-external counts: yz :       452 yx:      4545 yw:      4311
Two-external counts: yy :      1362 ww:       922 xx:      1105 xz:        51 wz:        61 wx:       990
Three-ext.   counts: yx :       682 yw:       664

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        50    task #   2:        60    task #   3:       704    task #   4:       412
task #   5:      4069    task #   6:      3725    task #   7:        33    task #   8:       972
task #   9:       877    task #  10:       671    task #  11:       122    task #  12:         1
task #  13:         1    task #  14:         1    task #
 =========== Executing IN-CORE method ==========
 norm multnew:  2.86649726E-05


====================================================================================================
Diagonal     counts:  0x:      8300 2x:      1713 4x:       203
All internal counts: zz :        34 yy:      1315 xx:      1079 ww:       968
One-external counts: yz :       452 yx:      4545 yw:      4311
Two-external counts: yy :      1362 ww:       922 xx:      1105 xz:        51 wz:        61 wx:       990
Three-ext.   counts: yx :       682 yw:       664

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        50    task #   2:        60    task #   3:       704    task #   4:       412
task #   5:      4069    task #   6:      3725    task #   7:        33    task #   8:       972
task #   9:       877    task #  10:       671    task #  11:       122    task #  12:         1
task #  13:         1    task #  14:         1    task #
 rtolcosmo = eo,e,r,ro   0. -145.27687 -145.277206  31.2324001  31.2321205
 rtolcosmo = eo,e,r,ro   0. -143.218835 -143.088735  31.2324001  31.2321205
 calctciref: ... reading 
 cirefv                                                       recamt= 4

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.94904417     0.09598924
 follow root  1 (overlap=  0.949044174)

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  7  1   -114.0448063984  5.7394E-05  1.8923E-05  5.3968E-03  1.0000E-03
 mr-sdci #  7  2   -111.8563347642 -1.3038E-01  0.0000E+00  1.7638E+00  1.0000E-04


dielectric energy =      -0.0014954760
delta diel. energy =      -0.0000316639
e(nroot) + repnuc - ediel-edielcorr =              elast =    -114.0350337334
delta Cosmo energy =       0.0000890575

 root number  1 is used to define the new expansion vector.
 diagon: frcsub=2, new matrix-vector products will be computed.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.01   0.00   0.00   0.00         0.    0.0000
    2   25    0   0.00   0.00   0.00   0.00         0.    0.0000
    3   26    0   0.02   0.00   0.00   0.02         0.    0.0000
    4   11    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   15    0   0.02   0.00   0.00   0.02         0.    0.0000
    6   16    0   0.01   0.01   0.00   0.01         0.    0.0000
    7    1    0   0.00   0.00   0.00   0.00         0.    0.0000
    8    5    0   0.01   0.00   0.00   0.01         0.    0.0000
    9    6    0   0.02   0.00   0.00   0.02         0.    0.0000
   10    7    0   0.01   0.00   0.00   0.01         0.    0.0000
   11   75    0   0.01   0.01   0.00   0.01         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.01   0.00   0.00   0.01         0.    0.0000
   14   47    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1300s 
time spent in multnx:                   0.1200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0200s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.9800s 

          starting ci iteration   8

 cosmocalc =  2
 rootcalc =  1
 repnuc =   31.2324001

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=      28  DYX=     285  DYW=     277
   D0Z=       4  D0Y=     115  D0X=      92  D0W=      90
  DDZI=      36 DDYI=     626 DDXI=     550 DDWI=     537
  DDZE=       0 DDYE=      72 DDXE=      65 DDWE=      66
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   2.0000000      2.0000000      1.9866161      1.9762862      1.9701010
   0.0212002      0.0156700      0.0081635      0.0053798      0.0051502
   0.0036366      0.0025326      0.0012338      0.0009359      0.0005250
   0.0002482      0.0001386      0.0000795


*****   symmetry block SYM2   *****

 occupation numbers of nos
   1.9720846      0.9994363      0.0104317      0.0069163      0.0048557
   0.0014709      0.0006128


*****   symmetry block SYM3   *****

 occupation numbers of nos
   1.9632009      1.0010424      0.0181932      0.0064056      0.0044628
   0.0022974      0.0012064      0.0007598      0.0003302      0.0001259


*****   symmetry block SYM4   *****

 occupation numbers of nos
   0.0048230      0.0029034      0.0005435


 total number of electrons =   16.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = "99999996"

 Work memory size (LMWORK) :    99999996 =  762.94 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***

  Confirmation of dielectric energy for excited state

  edielnew =  -0.000372988312

 **************************************************
 ***Cosmo calculation for excited states***********
 **************************************************

 fn2 =   0.338886685
 qcoszero = q0*fepsi + fn2*q(delta) is done 

 root =  1


 DALTON: user specified work memory size used,
          -m = "99999996"

 Work memory size (LMWORK) :    99999996 =  762.94 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 Warning! Large contrib. to nucrep.The distance is   0.017707087
 Warning! Large contrib. to nucrep.The distance is   0.000353311222
 ** Total nuclear repulsion energy ** =  31.24072753020759
 Warning! Large contrib. to nucrep.The distance is   0.017707087
 Warning! Large contrib. to nucrep.The distance is   0.000353311222
 screening nuclear repulsion energy   0.00825853192

 Total-screening nuclear repulsion energy   31.232469


 Adding T+Vsolv ...
 maxdens  260
 *** End of DALTON-COSMO calculation ***

 DIAGEL COUNTS: DG0X,DG2X,DG4X= 8300 1713 203
 =========== Executing IN-CORE method ==========
 norm multnew:  1.


====================================================================================================
Diagonal     counts:  0x:      8300 2x:      1713 4x:       203
All internal counts: zz :        34 yy:      1315 xx:      1079 ww:       968
One-external counts: yz :       452 yx:      4545 yw:      4311
Two-external counts: yy :      1362 ww:       922 xx:      1105 xz:        51 wz:        61 wx:       990
Three-ext.   counts: yx :       682 yw:       664

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        50    task #   2:        60    task #   3:       704    task #   4:       412
task #   5:      4069    task #   6:      3725    task #   7:        33    task #   8:       972
task #   9:       877    task #  10:       671    task #  11:       122    task #  12:         1
task #  13:         1    task #  14:         1    task #
 =========== Executing IN-CORE method ==========
 norm multnew:  2.78947171E-05


====================================================================================================
Diagonal     counts:  0x:      8300 2x:      1713 4x:       203
All internal counts: zz :        34 yy:      1315 xx:      1079 ww:       968
One-external counts: yz :       452 yx:      4545 yw:      4311
Two-external counts: yy :      1362 ww:       922 xx:      1105 xz:        51 wz:        61 wx:       990
Three-ext.   counts: yx :       682 yw:       664

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        50    task #   2:        60    task #   3:       704    task #   4:       412
task #   5:      4069    task #   6:      3725    task #   7:        33    task #   8:       972
task #   9:       877    task #  10:       671    task #  11:       122    task #  12:         1
task #  13:         1    task #  14:         1    task #
 rtolcosmo = eo,e,r,ro   0. -145.277206 -145.277289  31.232469  31.2324001
 rtolcosmo = eo,e,r,ro   0. -143.088735 -143.406922  31.232469  31.2324001
 calctciref: ... reading 
 cirefv                                                       recamt= 4

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.94837718    -0.27161919
 follow root  1 (overlap=  0.948377176)

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  8  1   -114.0448199614  1.3563E-05  4.5785E-06  3.2599E-03  1.0000E-03
 mr-sdci #  8  2   -112.1744532675  3.1812E-01  0.0000E+00  1.6821E+00  1.0000E-04


dielectric energy =      -0.0014884415
delta diel. energy =      -0.0000070345
e(nroot) + repnuc - ediel-edielcorr =              elast =    -114.0350543308
delta Cosmo energy =       0.0000205974

 root number  1 is used to define the new expansion vector.
 diagon: frcsub=2, new matrix-vector products will be computed.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.01   0.00   0.00   0.00         0.    0.0000
    2   25    0   0.00   0.00   0.00   0.00         0.    0.0000
    3   26    0   0.02   0.00   0.00   0.02         0.    0.0000
    4   11    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   15    0   0.02   0.00   0.00   0.02         0.    0.0000
    6   16    0   0.01   0.01   0.00   0.01         0.    0.0000
    7    1    0   0.00   0.00   0.00   0.00         0.    0.0000
    8    5    0   0.01   0.01   0.00   0.01         0.    0.0000
    9    6    0   0.02   0.00   0.00   0.02         0.    0.0000
   10    7    0   0.01   0.00   0.00   0.01         0.    0.0000
   11   75    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.01   0.01   0.00   0.01         0.    0.0000
   13   46    0   0.00   0.00   0.00   0.00         0.    0.0000
   14   47    0   0.01   0.00   0.00   0.00         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0300s 
time for vector access in mult:         0.0100s 
total time per CI iteration:            0.9800s 

          starting ci iteration   9

 cosmocalc =  2
 rootcalc =  1
 repnuc =   31.232469

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=      28  DYX=     285  DYW=     277
   D0Z=       4  D0Y=     115  D0X=      92  D0W=      90
  DDZI=      36 DDYI=     626 DDXI=     550 DDWI=     537
  DDZE=       0 DDYE=      72 DDXE=      65 DDWE=      66
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   2.0000000      2.0000000      1.9865039      1.9760543      1.9697975
   0.0214396      0.0158459      0.0082442      0.0054188      0.0051866
   0.0036600      0.0025484      0.0012412      0.0009425      0.0005286
   0.0002497      0.0001398      0.0000801


*****   symmetry block SYM2   *****

 occupation numbers of nos
   1.9717944      0.9994799      0.0105378      0.0069732      0.0048944
   0.0014801      0.0006160


*****   symmetry block SYM3   *****

 occupation numbers of nos
   1.9628034      1.0010785      0.0184337      0.0064561      0.0044994
   0.0023113      0.0012126      0.0007658      0.0003330      0.0001268


*****   symmetry block SYM4   *****

 occupation numbers of nos
   0.0048536      0.0029219      0.0005468


 total number of electrons =   16.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = "99999996"

 Work memory size (LMWORK) :    99999996 =  762.94 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***

  Confirmation of dielectric energy for excited state

  edielnew =  -0.000370799548

 **************************************************
 ***Cosmo calculation for excited states***********
 **************************************************

 fn2 =   0.338886685
 qcoszero = q0*fepsi + fn2*q(delta) is done 

 root =  1


 DALTON: user specified work memory size used,
          -m = "99999996"

 Work memory size (LMWORK) :    99999996 =  762.94 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 Warning! Large contrib. to nucrep.The distance is   0.017707087
 Warning! Large contrib. to nucrep.The distance is   0.000353311222
 ** Total nuclear repulsion energy ** =  31.24083749134197
 Warning! Large contrib. to nucrep.The distance is   0.017707087
 Warning! Large contrib. to nucrep.The distance is   0.000353311222
 screening nuclear repulsion energy   0.0082761043

 Total-screening nuclear repulsion energy   31.2325614


 Adding T+Vsolv ...
 maxdens  260
 *** End of DALTON-COSMO calculation ***

 DIAGEL COUNTS: DG0X,DG2X,DG4X= 8300 1713 203
 =========== Executing IN-CORE method ==========
 norm multnew:  1.


====================================================================================================
Diagonal     counts:  0x:      8300 2x:      1713 4x:       203
All internal counts: zz :        34 yy:      1315 xx:      1079 ww:       968
One-external counts: yz :       452 yx:      4545 yw:      4311
Two-external counts: yy :      1362 ww:       922 xx:      1105 xz:        51 wz:        61 wx:       990
Three-ext.   counts: yx :       682 yw:       664

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        50    task #   2:        60    task #   3:       704    task #   4:       412
task #   5:      4069    task #   6:      3725    task #   7:        33    task #   8:       972
task #   9:       877    task #  10:       671    task #  11:       122    task #  12:         1
task #  13:         1    task #  14:         1    task #
 =========== Executing IN-CORE method ==========
 norm multnew:  3.64307485E-06


====================================================================================================
Diagonal     counts:  0x:      8300 2x:      1713 4x:       203
All internal counts: zz :        34 yy:      1315 xx:      1079 ww:       968
One-external counts: yz :       452 yx:      4545 yw:      4311
Two-external counts: yy :      1362 ww:       922 xx:      1105 xz:        51 wz:        61 wx:       990
Three-ext.   counts: yx :       682 yw:       664

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        50    task #   2:        60    task #   3:       704    task #   4:       412
task #   5:      4069    task #   6:      3725    task #   7:        33    task #   8:       972
task #   9:       877    task #  10:       671    task #  11:       122    task #  12:         1
task #  13:         1    task #  14:         1    task #
 rtolcosmo = eo,e,r,ro   0. -145.277289 -145.277393  31.2325614  31.232469
 rtolcosmo = eo,e,r,ro   0. -143.406922 -143.339878  31.2325614  31.232469
 calctciref: ... reading 
 cirefv                                                       recamt= 4

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.94850851     0.09102046
 follow root  1 (overlap=  0.94850851)

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  9  1   -114.0448316242  1.1663E-05  2.4008E-06  1.9014E-03  1.0000E-03
 mr-sdci #  9  2   -112.1073170619 -6.7136E-02  0.0000E+00  1.7608E+00  1.0000E-04


dielectric energy =      -0.0014779867
delta diel. energy =      -0.0000104549
e(nroot) + repnuc - ediel-edielcorr =              elast =    -114.0350764485
delta Cosmo energy =       0.0000221177

 root number  1 is used to define the new expansion vector.
 diagon: frcsub=2, new matrix-vector products will be computed.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.00   0.00   0.00   0.00         0.    0.0000
    2   25    0   0.00   0.00   0.00   0.00         0.    0.0000
    3   26    0   0.02   0.00   0.00   0.02         0.    0.0000
    4   11    0   0.00   0.00   0.00   0.00         0.    0.0000
    5   15    0   0.02   0.00   0.00   0.02         0.    0.0000
    6   16    0   0.01   0.01   0.00   0.01         0.    0.0000
    7    1    0   0.00   0.00   0.00   0.00         0.    0.0000
    8    5    0   0.01   0.00   0.00   0.01         0.    0.0000
    9    6    0   0.02   0.00   0.00   0.02         0.    0.0000
   10    7    0   0.01   0.00   0.00   0.01         0.    0.0000
   11   75    0   0.01   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.01   0.00   0.00   0.01         0.    0.0000
   14   47    0   0.00   0.00   0.00   0.00         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1100s 
time spent in multnx:                   0.1000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0100s 
time for vector access in mult:         0.0200s 
total time per CI iteration:            0.9800s 

          starting ci iteration  10

 cosmocalc =  2
 rootcalc =  1
 repnuc =   31.2325614

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=      28  DYX=     285  DYW=     277
   D0Z=       4  D0Y=     115  D0X=      92  D0W=      90
  DDZI=      36 DDYI=     626 DDXI=     550 DDWI=     537
  DDZE=       0 DDYE=      72 DDXE=      65 DDWE=      66
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   2.0000000      2.0000000      1.9865581      1.9761426      1.9698869
   0.0213862      0.0158100      0.0082131      0.0053905      0.0051566
   0.0036412      0.0025353      0.0012343      0.0009376      0.0005259
   0.0002483      0.0001393      0.0000798


*****   symmetry block SYM2   *****

 occupation numbers of nos
   1.9719885      0.9994282      0.0104881      0.0069328      0.0048714
   0.0014726      0.0006125


*****   symmetry block SYM3   *****

 occupation numbers of nos
   1.9629554      1.0010512      0.0184157      0.0064186      0.0044765
   0.0022992      0.0012057      0.0007625      0.0003318      0.0001259


*****   symmetry block SYM4   *****

 occupation numbers of nos
   0.0048251      0.0029090      0.0005437


 total number of electrons =   16.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = "99999996"

 Work memory size (LMWORK) :    99999996 =  762.94 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***

  Confirmation of dielectric energy for excited state

  edielnew =  -0.000369116725

 **************************************************
 ***Cosmo calculation for excited states***********
 **************************************************

 fn2 =   0.338886685
 qcoszero = q0*fepsi + fn2*q(delta) is done 

 root =  1


 DALTON: user specified work memory size used,
          -m = "99999996"

 Work memory size (LMWORK) :    99999996 =  762.94 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 Warning! Large contrib. to nucrep.The distance is   0.017707087
 Warning! Large contrib. to nucrep.The distance is   0.000353311222
 ** Total nuclear repulsion energy ** =  31.24087111625830
 Warning! Large contrib. to nucrep.The distance is   0.017707087
 Warning! Large contrib. to nucrep.The distance is   0.000353311222
 screening nuclear repulsion energy   0.00827894049

 Total-screening nuclear repulsion energy   31.2325922


 Adding T+Vsolv ...
 maxdens  260
 *** End of DALTON-COSMO calculation ***

 DIAGEL COUNTS: DG0X,DG2X,DG4X= 8300 1713 203
 =========== Executing IN-CORE method ==========
 norm multnew:  1.


====================================================================================================
Diagonal     counts:  0x:      8300 2x:      1713 4x:       203
All internal counts: zz :        34 yy:      1315 xx:      1079 ww:       968
One-external counts: yz :       452 yx:      4545 yw:      4311
Two-external counts: yy :      1362 ww:       922 xx:      1105 xz:        51 wz:        61 wx:       990
Three-ext.   counts: yx :       682 yw:       664

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        50    task #   2:        60    task #   3:       704    task #   4:       412
task #   5:      4069    task #   6:      3725    task #   7:        33    task #   8:       972
task #   9:       877    task #  10:       671    task #  11:       122    task #  12:         1
task #  13:         1    task #  14:         1    task #
 =========== Executing IN-CORE method ==========
 norm multnew:  3.56716365E-06


====================================================================================================
Diagonal     counts:  0x:      8300 2x:      1713 4x:       203
All internal counts: zz :        34 yy:      1315 xx:      1079 ww:       968
One-external counts: yz :       452 yx:      4545 yw:      4311
Two-external counts: yy :      1362 ww:       922 xx:      1105 xz:        51 wz:        61 wx:       990
Three-ext.   counts: yx :       682 yw:       664

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        50    task #   2:        60    task #   3:       704    task #   4:       412
task #   5:      4069    task #   6:      3725    task #   7:        33    task #   8:       972
task #   9:       877    task #  10:       671    task #  11:       122    task #  12:         1
task #  13:         1    task #  14:         1    task #
 rtolcosmo = eo,e,r,ro   0. -145.277393 -145.277427  31.2325922  31.2325614
 rtolcosmo = eo,e,r,ro   0. -143.339878 -143.488586  31.2325922  31.2325614
 calctciref: ... reading 
 cirefv                                                       recamt= 4

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.94826763    -0.26741188
 follow root  1 (overlap=  0.948267634)

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 10  1   -114.0448344741  2.8500E-06  6.1836E-07  1.1730E-03  1.0000E-03
 mr-sdci # 10  2   -112.2559933826  1.4868E-01  0.0000E+00  1.6977E+00  1.0000E-04


dielectric energy =      -0.0014749494
delta diel. energy =      -0.0000030373
e(nroot) + repnuc - ediel-edielcorr =              elast =    -114.0350823357
delta Cosmo energy =       0.0000058872

 root number  1 is used to define the new expansion vector.
 diagon: frcsub=2, new matrix-vector products will be computed.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.00   0.00   0.00   0.00         0.    0.0000
    2   25    0   0.01   0.00   0.00   0.00         0.    0.0000
    3   26    0   0.01   0.00   0.00   0.01         0.    0.0000
    4   11    0   0.01   0.00   0.00   0.00         0.    0.0000
    5   15    0   0.01   0.01   0.00   0.01         0.    0.0000
    6   16    0   0.02   0.00   0.00   0.02         0.    0.0000
    7    1    0   0.00   0.00   0.00   0.00         0.    0.0000
    8    5    0   0.01   0.00   0.00   0.01         0.    0.0000
    9    6    0   0.02   0.00   0.00   0.02         0.    0.0000
   10    7    0   0.01   0.00   0.00   0.01         0.    0.0000
   11   75    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.01   0.01   0.00   0.01         0.    0.0000
   14   47    0   0.01   0.00   0.00   0.01         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0200s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.9900s 

          starting ci iteration  11

 cosmocalc =  2
 rootcalc =  1
 repnuc =   31.2325922

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=      28  DYX=     285  DYW=     277
   D0Z=       4  D0Y=     115  D0X=      92  D0W=      90
  DDZI=      36 DDYI=     626 DDXI=     550 DDWI=     537
  DDZE=       0 DDYE=      72 DDXE=      65 DDWE=      66
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   2.0000000      2.0000000      1.9865197      1.9760614      1.9697817
   0.0214659      0.0158699      0.0082412      0.0054047      0.0051699
   0.0036496      0.0025410      0.0012369      0.0009400      0.0005272
   0.0002489      0.0001398      0.0000801


*****   symmetry block SYM2   *****

 occupation numbers of nos
   1.9718844      0.9994443      0.0105268      0.0069529      0.0048854
   0.0014760      0.0006136


*****   symmetry block SYM3   *****

 occupation numbers of nos
   1.9628118      1.0010682      0.0184989      0.0064365      0.0044901
   0.0023044      0.0012079      0.0007647      0.0003329      0.0001263


*****   symmetry block SYM4   *****

 occupation numbers of nos
   0.0048362      0.0029159      0.0005449


 total number of electrons =   16.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = "99999996"

 Work memory size (LMWORK) :    99999996 =  762.94 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***

  Confirmation of dielectric energy for excited state

  edielnew =  -0.00036826072

 **************************************************
 ***Cosmo calculation for excited states***********
 **************************************************

 fn2 =   0.338886685
 qcoszero = q0*fepsi + fn2*q(delta) is done 

 root =  1


 DALTON: user specified work memory size used,
          -m = "99999996"

 Work memory size (LMWORK) :    99999996 =  762.94 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 Warning! Large contrib. to nucrep.The distance is   0.017707087
 Warning! Large contrib. to nucrep.The distance is   0.000353311222
 ** Total nuclear repulsion energy ** =  31.24091153402009
 Warning! Large contrib. to nucrep.The distance is   0.017707087
 Warning! Large contrib. to nucrep.The distance is   0.000353311222
 screening nuclear repulsion energy   0.00828538719

 Total-screening nuclear repulsion energy   31.2326261


 Adding T+Vsolv ...
 maxdens  260
 *** End of DALTON-COSMO calculation ***

 DIAGEL COUNTS: DG0X,DG2X,DG4X= 8300 1713 203
 =========== Executing IN-CORE method ==========
 norm multnew:  1.


====================================================================================================
Diagonal     counts:  0x:      8300 2x:      1713 4x:       203
All internal counts: zz :        34 yy:      1315 xx:      1079 ww:       968
One-external counts: yz :       452 yx:      4545 yw:      4311
Two-external counts: yy :      1362 ww:       922 xx:      1105 xz:        51 wz:        61 wx:       990
Three-ext.   counts: yx :       682 yw:       664

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        50    task #   2:        60    task #   3:       704    task #   4:       412
task #   5:      4069    task #   6:      3725    task #   7:        33    task #   8:       972
task #   9:       877    task #  10:       671    task #  11:       122    task #  12:         1
task #  13:         1    task #  14:         1    task #
 =========== Executing IN-CORE method ==========
 norm multnew:  5.25233133E-07


====================================================================================================
Diagonal     counts:  0x:      8300 2x:      1713 4x:       203
All internal counts: zz :        34 yy:      1315 xx:      1079 ww:       968
One-external counts: yz :       452 yx:      4545 yw:      4311
Two-external counts: yy :      1362 ww:       922 xx:      1105 xz:        51 wz:        61 wx:       990
Three-ext.   counts: yx :       682 yw:       664

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        50    task #   2:        60    task #   3:       704    task #   4:       412
task #   5:      4069    task #   6:      3725    task #   7:        33    task #   8:       972
task #   9:       877    task #  10:       671    task #  11:       122    task #  12:         1
task #  13:         1    task #  14:         1    task #
 rtolcosmo = eo,e,r,ro   0. -145.277427 -145.277464  31.2326261  31.2325922
 rtolcosmo = eo,e,r,ro   0. -143.488586 -143.465108  31.2326261  31.2325922
 calctciref: ... reading 
 cirefv                                                       recamt= 4

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.94831750     0.09084204
 follow root  1 (overlap=  0.948317495)

 trial vector basis is being transformed.  new dimension:   1

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 11  1   -114.0448378954  3.4213E-06  3.4169E-07  7.1792E-04  1.0000E-03
 mr-sdci # 11  2   -112.2324815896 -2.3512E-02  0.0000E+00  1.7445E+00  1.0000E-04


dielectric energy =      -0.0014711407
delta diel. energy =      -0.0000038087
e(nroot) + repnuc - ediel-edielcorr =              elast =    -114.0350895658
delta Cosmo energy =       0.0000072300


 mr-sdci  convergence criteria satisfied after 11 iterations.

 *************************************************
 ***Final Calc of density matrix for cosmo calc***
 *************************************************

 entering drivercid: firstcall,firstnonref= F T
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=      28  DYX=     285  DYW=     277
   D0Z=       4  D0Y=     115  D0X=      92  D0W=      90
  DDZI=      36 DDYI=     626 DDXI=     550 DDWI=     537
  DDZE=       0 DDYE=      72 DDXE=      65 DDWE=      66
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   2.0000000      2.0000000      1.9865416      1.9760967      1.9698190
   0.0214401      0.0158529      0.0082287      0.0053943      0.0051589
   0.0036427      0.0025361      0.0012343      0.0009383      0.0005262
   0.0002484      0.0001396      0.0000800


*****   symmetry block SYM2   *****

 occupation numbers of nos
   1.9719567      0.9994258      0.0105084      0.0069376      0.0048769
   0.0014733      0.0006123


*****   symmetry block SYM3   *****

 occupation numbers of nos
   1.9628686      1.0010613      0.0184884      0.0064223      0.0044819
   0.0023001      0.0012054      0.0007635      0.0003325      0.0001261


*****   symmetry block SYM4   *****

 occupation numbers of nos
   0.0048258      0.0029114      0.0005437


 total number of electrons =   16.0000000000


 root =  1


 DALTON: user specified work memory size used,
          -m = "99999996"

 Work memory size (LMWORK) :    99999996 =  762.94 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***

 cosurf and phiground = 
  4.718538350033  0.151537380667 -4.775150410537 -0.017602010000
  1.277831846546  4.499081802070 -4.856647546999 -0.020439108000
  3.554914706640  0.434730177362 -6.241707553104 -0.022840806000
  0.601831676137  1.688839704320 -7.311211587865 -0.026729433000
  0.853961350330  3.298932953517 -6.392189318242 -0.024870851000
  4.101630584993  2.090490441559 -4.985728603578 -0.018956198000
  2.815438020727  3.565773273800 -5.085991461345 -0.020403050000
  5.022104992832  1.444674720885 -3.374012694367 -0.012059909000
  2.463025966957  4.622359440136 -3.315560691514 -0.013797010000
  0.267015077594  5.235347717128 -3.292305177705 -0.014226636000
  2.453054901680  2.145000820027 -6.511371403212 -0.024376989000
  3.961857625677  3.400051089294 -3.397550430504 -0.013204524000
  1.700273331435  5.367335783769 -1.652094481704 -0.003794978000
  5.232762626303  2.318788733500  1.292321496869  0.010364451000
  4.308712786043  3.682047893518  1.518607048462  0.011346735000
  1.369817222051  1.112406280447  5.595963283614  0.022165509000
  0.266954841737  5.624490102234 -1.649992677326 -0.003793247000
  3.391795586812  0.653187041725  4.743178708421  0.019965852000
  4.778191650200  1.643851849561  2.982501004434  0.015588604000
  4.521851264195  3.735547662587 -0.164954125026  0.004516563000
  3.083605803927  4.673104607798 -1.755598471078 -0.004320226000
  5.728262532188  0.335644103218  1.225998331819  0.010215270000
  5.577247905795  1.703154017601 -0.649833471256  0.002605989000
  5.499946302588  0.663368762761 -1.933720555903 -0.004078888000
  3.740353895548  2.320031283093  3.880188121951  0.018282301000
  3.103466965079  4.947985487390 -0.560868699673  0.002650795000
  4.597186735122  3.186581966762 -1.772077868604 -0.003826478000
  0.016384477628  5.599620523069  3.593100951996  0.015026888000
  1.013951099449  6.092977502976  0.042028418810  0.005504966000
  3.356584710816  4.248008277379  2.943596622413  0.014809109000
  0.751653083140  2.684252644376  5.502521124948  0.020552071000
  1.731622711289  5.822346954394  2.302904017974  0.012338793000
  1.551374652653  4.593731277095  4.335602962697  0.016905578000
  2.903592550566  5.287811531650  0.996473409892  0.009126181000
  2.605920323420  3.127446261199  4.586018439227  0.018818061000
 end of phiground = 

 **************************************************
 ***Final cosmo calculation for excited states*****
 **************************************************

  ediel before cosmo(6) =  -0.000367785169
  elast before cosmo(6) =  -114.03509

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci # 11  1   -114.0448378954  3.4213E-06  3.4169E-07  7.1792E-04  1.0000E-03

####################CIUDGINFO####################

   followed vector at position   1 energy= -114.044837895443

################END OF CIUDGINFO################


    1 of the   2 expansion vectors are transformed.
    1 of the   1 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)

 root follwoing: root to follow is on position   1
 maximum overlap with reference  1(overlap=  0.948317495)

information on vector: 1from unit 11 written to unit 16filename civout                                                      


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =      -114.0448378954

                                                       internal orbitals

                                          level       1    2    3    4    5    6    7    8    9

                                          orbital     1    2    3    4    5   19   26   20   27

                                         symmetry   a1   a1   a1   a1   a1   b1   b2   b1   b2 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1  0.948317                        +-   +-   +-   +-   +-   +-   +-   +     - 
 z   1  1       2 -0.045531                        +-   +-   +-   +-   +-   +-   +     -   +- 
 z   1  1       3  0.120250                        +-   +-   +-   +-   +-   +    +-   +-    - 
 z   1  1       4  0.057553                        +-   +-   +-   +-   +-   +     -   +-   +- 
 y   1  1       5 -0.010939              1( b2 )   +-   +-   +-   +-   +-   +-   +-    -      
 y   1  1       6 -0.018624              2( b2 )   +-   +-   +-   +-   +-   +-   +-    -      
 y   1  1       8  0.013745              4( b2 )   +-   +-   +-   +-   +-   +-   +-    -      
 y   1  1      10  0.014891              6( b2 )   +-   +-   +-   +-   +-   +-   +-    -      
 y   1  1      13  0.018511              1( b1 )   +-   +-   +-   +-   +-   +-   +-         - 
 y   1  1      14  0.012772              2( b1 )   +-   +-   +-   +-   +-   +-   +-         - 
 y   1  1      24 -0.010317              2( b2 )   +-   +-   +-   +-   +-   +-    -   +     - 
 y   1  1      33 -0.012821              3( b1 )   +-   +-   +-   +-   +-   +-    -        +- 
 y   1  1      36 -0.034308              1( b2 )   +-   +-   +-   +-   +-   +-   +     -    - 
 y   1  1      37 -0.039648              2( b2 )   +-   +-   +-   +-   +-   +-   +     -    - 
 y   1  1      38  0.030490              3( b2 )   +-   +-   +-   +-   +-   +-   +     -    - 
 y   1  1      39  0.017005              4( b2 )   +-   +-   +-   +-   +-   +-   +     -    - 
 y   1  1      50 -0.010597              2( b2 )   +-   +-   +-   +-   +-   +-         -   +- 
 y   1  1      51  0.010162              3( b2 )   +-   +-   +-   +-   +-   +-         -   +- 
 y   1  1      58  0.012355              2( b2 )   +-   +-   +-   +-   +-    -   +-   +-      
 y   1  1      66  0.016069              2( b1 )   +-   +-   +-   +-   +-    -   +-   +     - 
 y   1  1      67 -0.012618              3( b1 )   +-   +-   +-   +-   +-    -   +-   +     - 
 y   1  1      87  0.012869              2( b1 )   +-   +-   +-   +-   +-    -   +     -   +- 
 y   1  1      91  0.022340              1( b1 )   +-   +-   +-   +-   +-   +    +-    -    - 
 y   1  1      92  0.016358              2( b1 )   +-   +-   +-   +-   +-   +    +-    -    - 
 y   1  1      93 -0.013500              3( b1 )   +-   +-   +-   +-   +-   +    +-    -    - 
 y   1  1      94  0.011855              4( b1 )   +-   +-   +-   +-   +-   +    +-    -    - 
 y   1  1      96 -0.016285              1( b2 )   +-   +-   +-   +-   +-   +     -   +-    - 
 y   1  1      98  0.010589              3( b2 )   +-   +-   +-   +-   +-   +     -   +-    - 
 y   1  1     109  0.010126              1( b1 )   +-   +-   +-   +-   +-        +-   +-    - 
 y   1  1     110 -0.018581              2( b1 )   +-   +-   +-   +-   +-        +-   +-    - 
 y   1  1     126  0.016672              2( a1 )   +-   +-   +-   +-    -   +-   +-   +     - 
 y   1  1     128 -0.017589              4( a1 )   +-   +-   +-   +-    -   +-   +-   +     - 
 y   1  1     145  0.017705              2( a1 )   +-   +-   +-   +-    -   +-   +     -   +- 
 y   1  1     147 -0.022866              4( a1 )   +-   +-   +-   +-    -   +-   +     -   +- 
 y   1  1     148 -0.015679              5( a1 )   +-   +-   +-   +-    -   +-   +     -   +- 
 y   1  1     149 -0.013301              6( a1 )   +-   +-   +-   +-    -   +-   +     -   +- 
 y   1  1     160  0.025106              4( a1 )   +-   +-   +-   +-    -   +    +-   +-    - 
 y   1  1     161  0.014379              5( a1 )   +-   +-   +-   +-    -   +    +-   +-    - 
 y   1  1     162  0.010385              6( a1 )   +-   +-   +-   +-    -   +    +-   +-    - 
 y   1  1     165 -0.010017              9( a1 )   +-   +-   +-   +-    -   +    +-   +-    - 
 y   1  1     173 -0.013055              1( a1 )   +-   +-   +-   +-   +    +-   +-    -    - 
 y   1  1     175  0.021710              3( a1 )   +-   +-   +-   +-   +    +-   +-    -    - 
 y   1  1     184 -0.010513             12( a1 )   +-   +-   +-   +-   +    +-   +-    -    - 
 y   1  1     191  0.014201              3( a1 )   +-   +-   +-   +-   +    +-    -    -   +- 
 y   1  1     254  0.012977              2( a1 )   +-   +-   +-    -   +-   +-   +     -   +- 
 y   1  1     270 -0.010589              5( a1 )   +-   +-   +-    -   +-   +    +-   +-    - 
 y   1  1     295 -0.028992              1( a1 )   +-   +-   +-   +    +-   +-   +-    -    - 
 y   1  1     296  0.012689              2( a1 )   +-   +-   +-   +    +-   +-   +-    -    - 
 y   1  1     297  0.045228              3( a1 )   +-   +-   +-   +    +-   +-   +-    -    - 
 y   1  1     299 -0.011463              5( a1 )   +-   +-   +-   +    +-   +-   +-    -    - 
 y   1  1     305  0.014266             11( a1 )   +-   +-   +-   +    +-   +-   +-    -    - 
 y   1  1     306  0.012408             12( a1 )   +-   +-   +-   +    +-   +-   +-    -    - 
 y   1  1     308  0.021430              1( a2 )   +-   +-   +-   +    +-   +-    -   +-    - 
 y   1  1     313  0.021447              3( a1 )   +-   +-   +-   +    +-   +-    -    -   +- 
 y   1  1     326  0.013852              3( a1 )   +-   +-   +-   +    +-    -   +-   +-    - 
 y   1  1     342 -0.015296              3( b1 )   +-   +-   +-   +     -   +-   +-   +-    - 
 y   1  1     346 -0.012684              2( b2 )   +-   +-   +-   +     -   +-   +-    -   +- 
 y   1  1     347  0.011543              3( b2 )   +-   +-   +-   +     -   +-   +-    -   +- 
 y   1  1     389  0.015472              2( a1 )   +-   +-    -   +-   +-   +-   +     -   +- 
 y   1  1     391 -0.011423              4( a1 )   +-   +-    -   +-   +-   +-   +     -   +- 
 y   1  1     402 -0.013097              2( a1 )   +-   +-    -   +-   +-   +    +-   +-    - 
 y   1  1     404  0.013023              4( a1 )   +-   +-    -   +-   +-   +    +-   +-    - 
 y   1  1     444 -0.014797              2( a1 )   +-   +-   +    +-   +-   +-   +-    -    - 
 y   1  1     445 -0.012386              3( a1 )   +-   +-   +    +-   +-   +-   +-    -    - 
 y   1  1     446  0.019346              4( a1 )   +-   +-   +    +-   +-   +-   +-    -    - 
 y   1  1     449  0.011203              7( a1 )   +-   +-   +    +-   +-   +-   +-    -    - 
 y   1  1     453  0.015615             11( a1 )   +-   +-   +    +-   +-   +-   +-    -    - 
 y   1  1     454  0.020173             12( a1 )   +-   +-   +    +-   +-   +-   +-    -    - 
 y   1  1     475  0.011144              4( a1 )   +-   +-   +    +-   +-    -   +-   +-    - 
 y   1  1     487 -0.019657              3( a2 )   +-   +-   +    +-   +-    -   +-    -   +- 
 y   1  1     498  0.010157              6( b2 )   +-   +-   +    +-    -   +-   +-    -   +- 
 y   1  1     500  0.011511              8( b2 )   +-   +-   +    +-    -   +-   +-    -   +- 
 x   1  1     646  0.016788    1( b1 )   1( b2 )   +-   +-   +-   +-   +-   +-    -         - 
 x   1  1     656 -0.018027    1( b1 )   3( b2 )   +-   +-   +-   +-   +-   +-    -         - 
 x   1  1     688 -0.021920    3( a1 )   1( a2 )   +-   +-   +-   +-   +-   +-    -         - 
 x   1  1     692  0.010481    7( a1 )   1( a2 )   +-   +-   +-   +-   +-   +-    -         - 
 x   1  1     850 -0.010089    2( b1 )   2( b2 )   +-   +-   +-   +-   +-    -   +-    -      
 x   1  1     860  0.018847    2( b1 )   4( b2 )   +-   +-   +-   +-   +-    -   +-    -      
 x   1  1     921  0.012433   12( a1 )   3( a2 )   +-   +-   +-   +-   +-    -   +-    -      
 x   1  1    1161 -0.011410    1( b1 )   1( b2 )   +-   +-   +-   +-   +-    -    -   +     - 
 x   1  1    1171  0.012498    1( b1 )   3( b2 )   +-   +-   +-   +-   +-    -    -   +     - 
 x   1  1    1203  0.014359    3( a1 )   1( a2 )   +-   +-   +-   +-   +-    -    -   +     - 
 x   1  1    1652  0.010203    4( a1 )   2( b2 )   +-   +-   +-   +-    -   +-   +-    -      
 x   1  1    1680 -0.014125    6( a1 )   4( b2 )   +-   +-   +-   +-    -   +-   +-    -      
 x   1  1    1758  0.011789    4( a1 )   1( b1 )   +-   +-   +-   +-    -   +-   +-         - 
 x   1  1    1773  0.010786    6( a1 )   2( b1 )   +-   +-   +-   +-    -   +-   +-         - 
 x   1  1    1782  0.010040    2( a1 )   3( b1 )   +-   +-   +-   +-    -   +-   +-         - 
 x   1  1    1784 -0.013317    4( a1 )   3( b1 )   +-   +-   +-   +-    -   +-   +-         - 
 x   1  1    1936 -0.012771    4( a1 )   1( b2 )   +-   +-   +-   +-    -   +-    -   +     - 
 x   1  1    2382  0.010488    4( a1 )   1( b1 )   +-   +-   +-   +-    -    -   +-   +     - 
 x   1  1    2396 -0.010700    5( a1 )   2( b1 )   +-   +-   +-   +-    -    -   +-   +     - 
 x   1  1    2397 -0.018430    6( a1 )   2( b1 )   +-   +-   +-   +-    -    -   +-   +     - 
 x   1  1    2443 -0.010434   13( a1 )   5( b1 )   +-   +-   +-   +-    -    -   +-   +     - 
 x   1  1    3122 -0.011013    1( a1 )   1( b1 )   +-   +-   +-    -   +-   +-   +-         - 
 x   1  1    3126 -0.010291    5( a1 )   1( b1 )   +-   +-   +-    -   +-   +-   +-         - 
 x   1  1    3300  0.010473    1( a1 )   1( b2 )   +-   +-   +-    -   +-   +-    -   +     - 
 x   1  1    5642  0.010102    2( a1 )   2( b1 )   +-   +-    -   +-   +-    -   +-   +     - 
 w   1  1    7288 -0.010720    2( b1 )   2( b2 )   +-   +-   +-   +-   +-   +-   +-           
 w   1  1    7298  0.018740    2( b1 )   4( b2 )   +-   +-   +-   +-   +-   +-   +-           
 w   1  1    7366 -0.014350    3( a1 )   3( a1 )   +-   +-   +-   +-   +-   +-   +     -      
 w   1  1    7509  0.016782    1( b1 )   1( b2 )   +-   +-   +-   +-   +-   +-   +          - 
 w   1  1    7519 -0.016146    1( b1 )   3( b2 )   +-   +-   +-   +-   +-   +-   +          - 
 w   1  1    7667 -0.011444    1( a1 )   1( a1 )   +-   +-   +-   +-   +-   +-        +     - 
 w   1  1    7670  0.010981    1( a1 )   3( a1 )   +-   +-   +-   +-   +-   +-        +     - 
 w   1  1    7672 -0.024037    3( a1 )   3( a1 )   +-   +-   +-   +-   +-   +-        +     - 
 w   1  1    7773 -0.020067    1( b2 )   1( b2 )   +-   +-   +-   +-   +-   +-        +     - 
 w   1  1    7775 -0.016066    2( b2 )   2( b2 )   +-   +-   +-   +-   +-   +-        +     - 
 w   1  1    7776  0.025069    1( b2 )   3( b2 )   +-   +-   +-   +-   +-   +-        +     - 
 w   1  1    7778 -0.026171    3( b2 )   3( b2 )   +-   +-   +-   +-   +-   +-        +     - 
 w   1  1    7809 -0.017130    1( a2 )   1( a2 )   +-   +-   +-   +-   +-   +-        +     - 
 w   1  1    7910 -0.013527    2( b1 )   4( b2 )   +-   +-   +-   +-   +-   +    +-    -      
 w   1  1    8064 -0.017224    1( b1 )   1( b1 )   +-   +-   +-   +-   +-   +    +-         - 
 w   1  1    8066  0.021843    2( b1 )   2( b1 )   +-   +-   +-   +-   +-   +    +-         - 
 w   1  1    8269  0.010143    1( b1 )   1( b2 )   +-   +-   +-   +-   +-   +     -   +     - 
 w   1  1    8279 -0.011960    1( b1 )   3( b2 )   +-   +-   +-   +-   +-   +     -   +     - 
 w   1  1    8745 -0.011060    1( b1 )   1( b1 )   +-   +-   +-   +-   +-        +-   +     - 
 w   1  1    8747 -0.028188    2( b1 )   2( b1 )   +-   +-   +-   +-   +-        +-   +     - 
 w   1  1    8801 -0.011035    3( a2 )   3( a2 )   +-   +-   +-   +-   +-        +-   +     - 
 w   1  1    8925  0.012117    6( a1 )   4( b2 )   +-   +-   +-   +-   +    +-   +-    -      
 w   1  1    9003 -0.013790    4( a1 )   1( b1 )   +-   +-   +-   +-   +    +-   +-         - 
 w   1  1    9018 -0.010358    6( a1 )   2( b1 )   +-   +-   +-   +-   +    +-   +-         - 
 w   1  1    9178 -0.012260    1( a1 )   1( b2 )   +-   +-   +-   +-   +    +-    -   +     - 
 w   1  1    9181  0.012228    4( a1 )   1( b2 )   +-   +-   +-   +-   +    +-    -   +     - 
 w   1  1    9193  0.011386    3( a1 )   2( b2 )   +-   +-   +-   +-   +    +-    -   +     - 
 w   1  1    9204  0.011517    1( a1 )   3( b2 )   +-   +-   +-   +-   +    +-    -   +     - 
 w   1  1    9206 -0.014893    3( a1 )   3( b2 )   +-   +-   +-   +-   +    +-    -   +     - 
 w   1  1    9207 -0.013313    4( a1 )   3( b2 )   +-   +-   +-   +-   +    +-    -   +     - 
 w   1  1    9284  0.011682    3( b1 )   1( a2 )   +-   +-   +-   +-   +    +-    -   +     - 
 w   1  1    9641  0.010646    5( a1 )   2( b1 )   +-   +-   +-   +-   +     -   +-   +     - 
 w   1  1    9642  0.017707    6( a1 )   2( b1 )   +-   +-   +-   +-   +     -   +-   +     - 
 w   1  1    9645 -0.010411    9( a1 )   2( b1 )   +-   +-   +-   +-   +     -   +-   +     - 
 w   1  1   10009 -0.022772    4( a1 )   4( a1 )   +-   +-   +-   +-        +-   +-   +     - 
 w   1  1   10019 -0.010219    5( a1 )   6( a1 )   +-   +-   +-   +-        +-   +-   +     - 
 w   1  1   10020 -0.018123    6( a1 )   6( a1 )   +-   +-   +-   +-        +-   +-   +     - 
 w   1  1   10039  0.010330    4( a1 )   9( a1 )   +-   +-   +-   +-        +-   +-   +     - 
 w   1  1   10044 -0.011848    9( a1 )   9( a1 )   +-   +-   +-   +-        +-   +-   +     - 
 w   1  1   10346  0.017115    1( a1 )   1( b1 )   +-   +-   +-   +    +-   +-   +-         - 
 w   1  1   10350  0.015987    5( a1 )   1( b1 )   +-   +-   +-   +    +-   +-   +-         - 
 w   1  1   10524 -0.019596    1( a1 )   1( b2 )   +-   +-   +-   +    +-   +-    -   +     - 
 w   1  1   10525  0.010112    2( a1 )   1( b2 )   +-   +-   +-   +    +-   +-    -   +     - 
 w   1  1   10528 -0.018131    5( a1 )   1( b2 )   +-   +-   +-   +    +-   +-    -   +     - 
 w   1  1   10539  0.014301    3( a1 )   2( b2 )   +-   +-   +-   +    +-   +-    -   +     - 
 w   1  1   10550  0.020140    1( a1 )   3( b2 )   +-   +-   +-   +    +-   +-    -   +     - 
 w   1  1   10551 -0.010577    2( a1 )   3( b2 )   +-   +-   +-   +    +-   +-    -   +     - 
 w   1  1   10552 -0.018499    3( a1 )   3( b2 )   +-   +-   +-   +    +-   +-    -   +     - 
 w   1  1   10554  0.022396    5( a1 )   3( b2 )   +-   +-   +-   +    +-   +-    -   +     - 
 w   1  1   10630  0.011314    3( b1 )   1( a2 )   +-   +-   +-   +    +-   +-    -   +     - 
 w   1  1   10970  0.010990    1( a1 )   1( b1 )   +-   +-   +-   +    +-    -   +-   +     - 
 w   1  1   10974  0.011559    5( a1 )   1( b1 )   +-   +-   +-   +    +-    -   +-   +     - 
 w   1  1   10988 -0.010330    6( a1 )   2( b1 )   +-   +-   +-   +    +-    -   +-   +     - 
 w   1  1   11346 -0.013681    1( a1 )   1( a1 )   +-   +-   +-   +     -   +-   +-   +     - 
 w   1  1   11351 -0.013651    3( a1 )   3( a1 )   +-   +-   +-   +     -   +-   +-   +     - 
 w   1  1   11352  0.019216    1( a1 )   4( a1 )   +-   +-   +-   +     -   +-   +-   +     - 
 w   1  1   11359  0.016112    4( a1 )   5( a1 )   +-   +-   +-   +     -   +-   +-   +     - 
 w   1  1   11366  0.010828    6( a1 )   6( a1 )   +-   +-   +-   +     -   +-   +-   +     - 
 w   1  1   11385 -0.010593    4( a1 )   9( a1 )   +-   +-   +-   +     -   +-   +-   +     - 
 w   1  1   11386 -0.010991    5( a1 )   9( a1 )   +-   +-   +-   +     -   +-   +-   +     - 
 w   1  1   11387 -0.011501    6( a1 )   9( a1 )   +-   +-   +-   +     -   +-   +-   +     - 
 w   1  1   11800 -0.015039    1( a1 )   1( a1 )   +-   +-   +-        +-   +-   +-   +     - 
 w   1  1   11803  0.010508    1( a1 )   3( a1 )   +-   +-   +-        +-   +-   +-   +     - 
 w   1  1   11805 -0.015715    3( a1 )   3( a1 )   +-   +-   +-        +-   +-   +-   +     - 
 w   1  1   11809 -0.012293    4( a1 )   4( a1 )   +-   +-   +-        +-   +-   +-   +     - 
 w   1  1   11810 -0.013858    1( a1 )   5( a1 )   +-   +-   +-        +-   +-   +-   +     - 
 w   1  1   11814 -0.012690    5( a1 )   5( a1 )   +-   +-   +-        +-   +-   +-   +     - 
 w   1  1   11942 -0.010131    1( a2 )   1( a2 )   +-   +-   +-        +-   +-   +-   +     - 
 w   1  1   12067 -0.011154    2( a1 )   4( b2 )   +-   +-   +    +-   +-   +-   +-    -      
 w   1  1   12784 -0.017172    2( a1 )   2( b1 )   +-   +-   +    +-   +-    -   +-   +     - 
 w   1  1   12786  0.011291    4( a1 )   2( b1 )   +-   +-   +    +-   +-    -   +-   +     - 
 w   1  1   12790  0.013183    8( a1 )   2( b1 )   +-   +-   +    +-   +-    -   +-   +     - 
 w   1  1   13155 -0.010834    4( a1 )   4( a1 )   +-   +-   +    +-    -   +-   +-   +     - 
 w   1  1   13177 -0.011435    4( a1 )   8( a1 )   +-   +-   +    +-    -   +-   +-   +     - 
 w   1  1   13187 -0.012243    6( a1 )   9( a1 )   +-   +-   +    +-    -   +-   +-   +     - 
 w   1  1   14063 -0.010584    4( a1 )   4( a1 )   +-   +-        +-   +-   +-   +-   +     - 

 ci coefficient statistics:
           rq > 0.1                2
      0.1> rq > 0.01             171
     0.01> rq > 0.001           3346
    0.001> rq > 0.0001          5348
   0.0001> rq > 0.00001         4439
  0.00001> rq > 0.000001         876
 0.000001> rq                     98
           all                 14280
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       180 2x:         0 4x:         0
All internal counts: zz :        34 yy:         0 xx:         0 ww:         0
One-external counts: yz :         0 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         0    task #   2:         0    task #   3:         0    task #   4:         0
task #   5:         0    task #   6:         0    task #   7:        33    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:       122    task #  12:         0
task #  13:         0    task #  14:         0    task #
  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.948317495447   -107.855395776693

 number of reference csfs (nref) is     1.  root number (iroot) is  1.

 pople ci energy extrapolation is computed with 16 correlated electrons.

 eref      =   -113.733423979281   "relaxed" cnot**2         =   0.899306072172
 eci       =   -114.044837895443   deltae = eci - eref       =  -0.311413916162
 eci+dv1   =   -114.076195385842   dv1 = (1-cnot**2)*deltae  =  -0.031357490399
 eci+dv2   =   -114.079706436164   dv2 = dv1 / cnot**2       =  -0.034868540722
 eci+dv3   =   -114.084102876098   dv3 = dv1 / (2*cnot**2-1) =  -0.039264980655
 eci+pople =   -114.078090614649   ( 16e- scaled deltae )    =  -0.344666635368
 entering drivercid: firstcall,firstnonref= F T
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=      28  DYX=     285  DYW=     277
   D0Z=       4  D0Y=     115  D0X=      92  D0W=      90
  DDZI=      36 DDYI=     626 DDXI=     550 DDWI=     537
  DDZE=       0 DDYE=      72 DDXE=      65 DDWE=      66
================================================================================


*****   symmetry block SYM1   *****

 occupation numbers of nos
   2.0000000      2.0000000      1.9865416      1.9760967      1.9698190
   0.0214401      0.0158529      0.0082287      0.0053943      0.0051589
   0.0036427      0.0025361      0.0012343      0.0009383      0.0005262
   0.0002484      0.0001396      0.0000800


*****   symmetry block SYM2   *****

 occupation numbers of nos
   1.9719567      0.9994258      0.0105084      0.0069376      0.0048769
   0.0014733      0.0006123


*****   symmetry block SYM3   *****

 occupation numbers of nos
   1.9628686      1.0010613      0.0184884      0.0064223      0.0044819
   0.0023001      0.0012054      0.0007635      0.0003325      0.0001261


*****   symmetry block SYM4   *****

 occupation numbers of nos
   0.0048258      0.0029114      0.0005437


 total number of electrons =   16.0000000000

NO coefficients and occupation numbers are written to nocoef_ci.1


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!


                        A1  partial gross atomic populations
   ao class       1A1        2A1        3A1        4A1        5A1        6A1 
    O1_ s      -0.000242   1.999660   1.728960   0.013179   0.028836   0.000581
    O1_ p      -0.000420   0.000003   0.006199   1.070753   0.337720   0.005360
    O1_ d       0.000005   0.000000   0.000436   0.006748   0.001860   0.000147
    C1_ s       2.000410   0.000255   0.142639   0.670547   0.293584   0.008024
    C1_ p       0.000002   0.000237   0.058181   0.064257   0.652765   0.000829
    C1_ d       0.000001  -0.000155   0.017492   0.002117   0.014592   0.001335
    H1_ s       0.000410   0.000000   0.030603   0.135052   0.620487   0.005328
    H1_ p      -0.000166   0.000000   0.002032   0.013444   0.019974  -0.000164

   ao class       7A1        8A1        9A1       10A1       11A1       12A1 
    O1_ s       0.000451   0.004753   0.000340   0.000010   0.000003   0.000411
    O1_ p       0.001875   0.002641   0.000244   0.000636   0.000003   0.000353
    O1_ d       0.000241   0.000012   0.000966   0.001332   0.003262   0.000369
    C1_ s       0.000255   0.000373   0.000157   0.001725   0.000189   0.000005
    C1_ p       0.007481   0.000142   0.001046  -0.000024   0.000064   0.000004
    C1_ d       0.000815   0.000105   0.002142   0.001249   0.000007   0.001133
    H1_ s       0.004499   0.000181   0.000056   0.000084   0.000067   0.000002
    H1_ p       0.000236   0.000022   0.000443   0.000148   0.000048   0.000259

   ao class      13A1       14A1       15A1       16A1       17A1       18A1 
    O1_ s      -0.000003   0.000080   0.000000   0.000006   0.000020   0.000000
    O1_ p       0.000012   0.000201   0.000000   0.000007   0.000003   0.000001
    O1_ d       0.000100   0.000360   0.000002   0.000005   0.000000   0.000000
    C1_ s       0.000183   0.000042   0.000108   0.000010   0.000011   0.000006
    C1_ p       0.000036   0.000194   0.000035   0.000000   0.000082  -0.000001
    C1_ d       0.000528   0.000038   0.000017   0.000054   0.000003   0.000003
    H1_ s       0.000383   0.000023   0.000052   0.000004   0.000003   0.000042
    H1_ p      -0.000006   0.000001   0.000312   0.000164   0.000017   0.000030

                        B1  partial gross atomic populations
   ao class       1B1        2B1        3B1        4B1        5B1        6B1 
    O1_ p       1.577567   0.164107   0.009759  -0.000113   0.000391   0.000061
    O1_ d       0.004116   0.001811   0.000038   0.000150   0.002474   0.000645
    C1_ p       0.358308   0.814896  -0.000329   0.006501   0.000276   0.000024
    C1_ d       0.029103   0.005828   0.001006   0.000193   0.001708   0.000573
    H1_ p       0.002862   0.012783   0.000035   0.000206   0.000029   0.000170

   ao class       7B1 
    O1_ p       0.000001
    O1_ d       0.000016
    C1_ p       0.000015
    C1_ d       0.000071
    H1_ p       0.000509

                        B2  partial gross atomic populations
   ao class       1B2        2B2        3B2        4B2        5B2        6B2 
    O1_ p       0.294713   0.785353   0.002249   0.002354   0.001748   0.000240
    O1_ d       0.004245  -0.000032   0.000327   0.000061   0.001500   0.001041
    C1_ p       0.927061   0.020917   0.007613   0.002605   0.000022   0.000295
    C1_ d      -0.001552   0.028703   0.000910   0.001130   0.000973   0.000004
    H1_ s       0.724250   0.166179   0.007179   0.000013   0.000148   0.000451
    H1_ p       0.014153  -0.000057   0.000210   0.000259   0.000090   0.000269

   ao class       7B2        8B2        9B2       10B2 
    O1_ p       0.000002   0.000011   0.000019   0.000001
    O1_ d       0.000014   0.000061   0.000030   0.000000
    C1_ p       0.000057   0.000319   0.000034  -0.000001
    C1_ d       0.000123   0.000222   0.000039   0.000002
    H1_ s       0.000318   0.000012   0.000073   0.000044
    H1_ p       0.000692   0.000139   0.000137   0.000080

                        A2  partial gross atomic populations
   ao class       1A2        2A2        3A2 
    O1_ d       0.000657   0.002454   0.000011
    C1_ d       0.003473   0.000205   0.000114
    H1_ p       0.000696   0.000252   0.000418


                        gross atomic populations
     ao           O1_        C1_        H1_
      s         3.777045   3.118522   1.695940
      p         4.264053   2.923942   0.070726
      d         0.035468   0.114303   0.000000
    total       8.076566   6.156768   1.766667


 Total number of electrons:   16.00000000

