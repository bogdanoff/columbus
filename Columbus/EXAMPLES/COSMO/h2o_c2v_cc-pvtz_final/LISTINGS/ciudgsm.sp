1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -76.0726719487  1.9006E-02  3.1915E-01  1.4431E+00  1.0000E-04
 mr-sdci #  2  1    -76.3265816350  2.5391E-01  1.0861E-02  2.7085E-01  1.0000E-04
 mr-sdci #  3  1    -76.3374087744  1.0827E-02  1.1296E-03  6.9779E-02  1.0000E-04
 mr-sdci #  4  1    -76.3376708111  2.6204E-04  1.5000E-04  2.6144E-02  1.0000E-04
 mr-sdci #  5  1    -76.3379884919  3.1768E-04  4.9765E-05  1.2464E-02  1.0000E-04
 mr-sdci #  6  1    -76.3380058574  1.7365E-05  9.8339E-06  6.6400E-03  1.0000E-04
 mr-sdci #  7  1    -76.3380358601  3.0003E-05  3.9040E-06  3.4764E-03  1.0000E-04
 mr-sdci #  8  1    -76.3380375348  1.6748E-06  8.7755E-07  1.9770E-03  1.0000E-04
 mr-sdci #  9  1    -76.3380440483  6.5135E-06  4.0051E-07  1.1550E-03  1.0000E-04
 mr-sdci # 10  1    -76.3380441171  6.8769E-08  1.0168E-07  6.7380E-04  1.0000E-04
 mr-sdci # 11  1    -76.3380460599  1.9428E-06  5.2925E-08  4.3784E-04  1.0000E-04
 mr-sdci # 12  1    -76.3380460021 -5.7817E-08  1.4877E-08  2.5991E-04  1.0000E-04
 mr-sdci # 13  1    -76.3380466738  6.7171E-07  8.5514E-09  1.8174E-04  1.0000E-04
 mr-sdci # 14  1    -76.3380466080 -6.5819E-08  2.5685E-09  1.0900E-04  1.0000E-04
 mr-sdci # 15  1    -76.3380468595  2.5153E-07  1.5686E-09  7.9477E-05  1.0000E-04

 mr-sdci  convergence criteria satisfied after 15 iterations.

 final mr-sdci  convergence information:
 mr-sdci # 15  1    -76.3380468595  2.5153E-07  1.5686E-09  7.9477E-05  1.0000E-04

 number of reference csfs (nref) is     1.  root number (iroot) is  1.

 eref      =    -76.073984558112   "relaxed" cnot**2         =   0.945400803818
 eci       =    -76.338046859547   deltae = eci - eref       =  -0.264062301435
 eci+dv1   =    -76.352464448947   dv1 = (1-cnot**2)*deltae  =  -0.014417589400
 eci+dv2   =    -76.353297099807   dv2 = dv1 / cnot**2       =  -0.015250240260
 eci+dv3   =    -76.354231820684   dv3 = dv1 / (2*cnot**2-1) =  -0.016184961137
 eci+pople =    -76.350677752120   ( 10e- scaled deltae )    =  -0.276693194009
