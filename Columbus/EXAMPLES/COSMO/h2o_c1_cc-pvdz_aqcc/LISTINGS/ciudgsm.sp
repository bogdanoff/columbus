1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.

 This is a AQCC calculation USING the mrci-code /  March 1997 (tm)
 =============================================
 ==========================================

 integral file titles:
 Hermit Integral Program : SIFS version  hochtor2        Thu Oct  7 15:29:03 2004
  cidrt_title                                                                    
  title                                                                          
 mofmt: formatted orbitals label=morbl   hochtor2        Thu Oct  7 15:29:04 2004
 SIFS file created by program tran.      hochtor2        Thu Oct  7 15:29:04 2004

 formula file title:
 Hermit Integral Program : SIFS version  hochtor2        Thu Oct  7 15:29:03 2004
  cidrt_title                                                                    
  title                                                                          
 mofmt: formatted orbitals label=morbl   hochtor2        Thu Oct  7 15:29:04 2004
 SIFS file created by program tran.      hochtor2        Thu Oct  7 15:29:04 2004
  cidrt_title                                                                    

 297 dimension of the ci-matrix ->>>      4656


 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  1  1    -76.0479455968 -9.2970E+00  2.5467E-01  1.0015E+00  1.0000E-03
 mraqcc  #  2  1    -76.2477236339 -9.1532E+00  6.7080E-03  1.6198E-01  1.0000E-03
 mraqcc  #  3  1    -76.2633921395 -9.3373E+00  3.6462E-03  1.5064E-01  1.0000E-03
 mraqcc  #  4  1    -76.2931380038 -1.0229E+01  1.4411E-02  1.5556E-01  1.0000E-03
 mraqcc  #  5  1    -76.3095304116 -1.0268E+01 -4.0787E-02  3.7420E-01  1.0000E-03
 mraqcc  #  6  1    -76.4668769731 -1.1486E+01 -3.0161E-01  7.5846E-01  1.0000E-03
 mraqcc  #  7  1    -76.6680047629 -1.2901E+01 -1.5697E-01  8.5700E-01  1.0000E-03
 mraqcc  #  8  1    -76.7341734007 -1.3226E+01  5.8758E-02  1.0870E+00  1.0000E-03
 mraqcc  #  9  1    -77.0642542697 -1.4013E+01 -3.3355E+00  4.5865E+00  1.0000E-03
 mraqcc  # 10  1    -79.9269499044 -2.0064E+01 -1.6116E+00  4.5751E+00  1.0000E-03
 mraqcc  # 11  1    -80.9028422013 -2.2031E+01 -1.0769E+00  4.8214E+00  1.0000E-03
 mraqcc  # 12  1    -81.3575382513 -2.2916E+01 -8.7001E-01  5.0750E+00  1.0000E-03
 mraqcc  # 13  1    -81.8701415166 -2.3468E+01 -8.8466E-01  4.4176E+00  1.0000E-03
 mraqcc  # 14  1    -82.4441001102 -2.3843E+01 -4.4721E+00  6.5737E+00  1.0000E-03
 mraqcc  # 15  1    -88.7041789005 -2.5693E+01 -9.2360E+00  9.2327E+00  1.0000E-03
 mraqcc  # 16  1    -97.6734685655 -3.1005E+01 -1.6931E+01  1.6662E+01  1.0000E-03
 mraqcc  # 17  1   -114.1434731749 -4.1003E+01 -1.1048E+01  1.0873E+01  1.0000E-03
 mraqcc  # 18  1   -124.7587772702 -4.7365E+01 -7.2139E+00  7.4700E+00  1.0000E-03
 mraqcc  # 19  1   -131.3655899917 -5.1370E+01 -4.5584E+00  5.2155E+00  1.0000E-03
 mraqcc  # 20  1   -135.4024849196 -5.3767E+01 -3.1258E+00  4.2417E+00  1.0000E-03

 mraqcc   convergence not reached after 20 iterations.

 final mraqcc   convergence information:

