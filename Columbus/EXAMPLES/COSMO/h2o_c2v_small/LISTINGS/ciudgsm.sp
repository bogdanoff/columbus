1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 integral file titles:
 Hermit Integral Program : SIFS version  hochtor2        Thu Oct  7 15:18:42 2004
  cidrt_title                                                                    
  title                                                                          
 mofmt: formatted orbitals label=morbl   hochtor2        Thu Oct  7 15:18:43 2004
 SIFS file created by program tran.      hochtor2        Thu Oct  7 15:18:43 2004

 formula file title:
 Hermit Integral Program : SIFS version  hochtor2        Thu Oct  7 15:18:42 2004
  cidrt_title                                                                    
  title                                                                          
 mofmt: formatted orbitals label=morbl   hochtor2        Thu Oct  7 15:18:43 2004
 SIFS file created by program tran.      hochtor2        Thu Oct  7 15:18:43 2004
  cidrt_title                                                                    

 297 dimension of the ci-matrix ->>>       282


 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -76.0070086514 -9.2908E+00  1.6555E-01  7.0128E-01  1.0000E-03
 mr-sdci #  2  1    -76.1313592203 -9.2202E+00  4.5399E-03  1.1706E-01  1.0000E-03
 mr-sdci #  3  1    -76.1361345948 -9.3398E+00  2.2814E-04  2.2981E-02  1.0000E-03
 mr-sdci #  4  1    -76.1363323990 -9.3444E+00  1.6450E-05  6.7784E-03  1.0000E-03
 mr-sdci #  5  1    -76.1363478003 -9.3446E+00  5.6180E-07  1.1976E-03  1.0000E-03
 mr-sdci #  6  1    -76.1363483462 -9.3446E+00  3.1458E-08  2.8771E-04  1.0000E-03

 mr-sdci  convergence criteria satisfied after  6 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  6  1    -76.1363483462 -9.3446E+00  3.1458E-08  2.8771E-04  1.0000E-03

 number of reference csfs (nref) is     1.  root number (iroot) is  1.

 eref      =    -76.007008651407   "relaxed" cnot**2         =   0.961313840775
 eci       =    -76.136348346245   deltae = eci - eref       =  -0.129339694838
 eci+dv1   =    -76.141352002274   dv1 = (1-cnot**2)*deltae  =  -0.005003656029
 eci+dv2   =    -76.141553364436   dv2 = dv1 / cnot**2       =  -0.005205018191
 eci+dv3   =    -76.141771612996   dv3 = dv1 / (2*cnot**2-1) =  -0.005423266751
 eci+pople =    -76.140614231611   ( 10e- scaled deltae )    =  -0.133605580204
