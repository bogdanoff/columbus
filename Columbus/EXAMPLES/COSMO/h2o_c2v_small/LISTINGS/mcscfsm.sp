 total ao core energy =    9.317913917
 MCSCF calculation performed for  1 DRT.

 DRT  first state   no.of aver. states   weights
  1   ground state          1             1.000

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a1 
 Total number of electrons:    10
 Spin multiplicity:            1
 Number of active orbitals:    0
 Number of active electrons:   0
 Total number of CSFs:         1

 Number of active-double rotations:      0
 Number of active-active rotations:      0
 Number of double-virtual rotations:    16
 Number of active-virtual rotations:     0

 iter=    1 emc=  -75.9798614863 demc= 7.5980E+01 wnorm= 1.4581E-09 knorm= 2.8560E-10 apxde= 4.2010E-18    *not converged* 

 final mcscf convergence values:
 iter=    2 emc=  -75.9798614863 demc= 0.0000E+00 wnorm= 1.7984E-09 knorm= 2.7265E-11 apxde=-2.2821E-15    *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 weight 1.000000 total energy=  -75.979861486
   ------------------------------------------------------------


