1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      09/20/00    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 using llenci= -1
================================================================================
 nsubmx= 6 lenci= 282
global arrays:         3666   (              0.03 MB)
vdisk:                    0   (              0.00 MB)
drt:                 252658   (              0.96 MB)
================================================================================
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core            9999999 DP per process
 CIUDG version 5.9.4 (29-Aug-2003)
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 0,
  GSET = 0,
   DAVCOR =10,
  NCOREL = 10
  NROOT = 1
  IVMODE = 3
  NBKITR = 0
  NVBKMN = 1
  NVBKMX = 6
  RTOLBK = 1e-3,
  NITER = 20
  NVCIMN = 3
  NVCIMX = 6
  RTOLCI = 1e-3,
  IDEN  = 1
  CSFPRN = 10,
  lvlprt = 5
  frcsub = 2
  cosmocalc = 1
  /end
 ------------------------------------------------------------------------

 ** list of control variables **
 nrfitr =   30      nvrfmx =    6      nvrfmn =    3
 lvlprt =    5      nroot  =    1      noldv  =    0      noldhv =    0
 nunitv =    1      ntype  =    0      nbkitr =    0      niter  =   20
 ivmode =    3      vout   =    0      istrt  =    0      iortls =    0
 nvbkmx =    6      ibktv  =   -1      ibkthv =   -1      frcsub =    2
 nvcimx =    6      icitv  =   -1      icithv =   -1      maxseg =    4
 iden   =    1      itran  =    0      froot  =    0      rtmode =    0
 ftcalc =    1      lrtshift=1.0000    ncouple=    1      skipso  =   F
 ncorel =   10      csfprn  =  10      ctol   = 1.00E-02  davcor  =  10


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-03    1.000E-03

 units and filenames:
    1: ( 6)    ciudgls                                                     
    2: ( 5)    ciudgin                                                     
    3: ( 7)    ciudgsm                                                     
    4: ( 4)    cihdiag                                                     
    5: ( 3)    cihpseu                                                     
    6: (10)    cihvfl                                                      
    7: (11)    civfl                                                       
    8: (12)    diagint                                                     
    9: (13)    ofdgint                                                     
   10: (17)    cidrtfl                                                     
   11: (20)    ciftdfl                                                     
   12: (21)    ciftofl                                                     
   13: (24)    ciftifl                                                     
   14: ( 8)    ciflind                                                     
   15: (15)    civin                                                       
   16: (16)    civout                                                      
   18: (18)    d1fl                                                        
   20: (53)    cifvfl                                                      
   21: (25)    ciscr4                                                      
   22: (41)    ciftotd                                                     
   23: (42)    flacpfd                                                     
   26: (26)    ciscr5                                                      
   27: (47)    restart                                                     
   28: (48)    civout                                                      
   29: (49)    cirefv                                                      
   31: (31)    fil4w                                                       
   32: (32)    fil4x                                                       
   33: (33)    fil3w                                                       
   34: (34)    fil3x                                                       
   36: (36)    aoints                                                      
   37: (37)    aoints2                                                     
   38: (38)    drtfil                                                      
   39: (50)    cisrtif                                                     
   42: (50)    mocoef                                                      
   45: (52)    nocoef_ci                                                   
   46: (53)    civfl_restart                                               
   54: (54)    civcosmo                                                    
 ------------------------------------------------------------------------

 workspace allocation information: lcore=   9999999 mem1=1108566024 ifirst=-264081912

 integral file titles:
 Hermit Integral Program : SIFS version  hochtor2        Thu Oct  7 15:18:42 2004
  cidrt_title                                                                    
  title                                                                          
 mofmt: formatted orbitals label=morbl   hochtor2        Thu Oct  7 15:18:43 2004
 SIFS file created by program tran.      hochtor2        Thu Oct  7 15:18:43 2004

 core energy values from the integral file:
 energy( 1)=  9.317913916904E+00, ietype=   -1,    core energy of type: Nuc.Rep.

 total core repulsion energy =  9.317913916904E+00

 drt header information:
  cidrt_title                                                                    
 spnorb, spnodd, lxyzir,hmult F F 0 0 0 0
 nmot  =    13 niot  =     5 nfct  =     0 nfvt  =     0
 nrow  =    20 nsym  =     4 ssym  =     1 lenbuf=  1600
 nwalk,xbar:         31        1        5       10       15
 nvalwt,nvalw:       31        1        5       10       15
 ncsft:             282
 total number of valid internal walks:      31
 nvalz,nvaly,nvalx,nvalw =        1       5      10      15

 cisrt info file parameters:
 file number  12 blocksize   4095
 mxbld   4095
 nd4ext,nd2ext,nd0ext    72    80    30
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int      330      564      561      288       66        0        0        0
 minbl4,minbl3,maxbl2    48    48    51
 maxbuf 32767
 number of external orbitals per symmetry block:   4   3   1   0
 nmsym   4 number of internal orbitals   5

 formula file title:
 Hermit Integral Program : SIFS version  hochtor2        Thu Oct  7 15:18:42 2004
  cidrt_title                                                                    
  title                                                                          
 mofmt: formatted orbitals label=morbl   hochtor2        Thu Oct  7 15:18:43 2004
 SIFS file created by program tran.      hochtor2        Thu Oct  7 15:18:43 2004
  cidrt_title                                                                    
 file nmb.  24 block size     0
 pthz,pthy,pthx,pthw:     1     5    10    15 total internal walks:      31
 maxlp3,n3xtlp,n2xtlp,n1xtlp,n0xtlp,n2lp,n1lp,n0lp     0     0     0     0
 orbsym(*)= 1 1 1 2 3

 setref:        1 references kept,
                0 references were marked as invalid, out of
                1 total.
 limcnvrt: found 1 valid internal walksout of  1
  walks (skipping trailing invalids)
  ... adding  1 segmentation marks segtype= 1
 limcnvrt: found 5 valid internal walksout of  5
  walks (skipping trailing invalids)
  ... adding  5 segmentation marks segtype= 2
 limcnvrt: found 10 valid internal walksout of  10
  walks (skipping trailing invalids)
  ... adding  10 segmentation marks segtype= 3
 limcnvrt: found 15 valid internal walksout of  15
  walks (skipping trailing invalids)
  ... adding  15 segmentation marks segtype= 4
 nmb.of records 4-ext     1
 nmb.of records 3-ext     1
 nmb.of records 2-ext     1
 nmb.of records 1-ext     1
 nmb.of records 0-ext     0
 nmb.of records 2-int     0
 nmb.of records 1-int     0
 nmb.of records 0-int
 mxorb=   4

 < n-ex core usage >
     routines:
    fourex       69662
    threx        32852
    twoex         4362
    onex          4144
    allin         4095
    diagon        4627
               =======
   maximum       69662

  __ static summary __ 
   reflst            1
   hrfspc            1
               -------
   static->          1

  __ core required  __ 
   totstc            1
   max n-ex      69662
               -------
   totnec->      69663

  __ core available __ 
   totspc      9999999
   totnec -      69663
               -------
   totvec->    9930336

 number of external paths / symmetry
 vertex x       9      12       4       3
 vertex w      17      12       4       3
================ pruneseg on segment    1(   1)================
 original DRT
iwalk=   1step: 3 3 3 3 3
 level  0 through level  4 of the drt:

 row lev  l0  l1  l2  l3     y0      y1      y2    xbar 

  17  4   0   0   0  20       1       1       1       1
  18  4   0   0   0   0       0       0       0       0
  19  4   0   0   0   0       0       0       0       0
 ....................................

  13  3   0   0   0  17       1       1       1       1
  14  3   0   0   0   0       0       0       0       0
  15  3   0   0   0   0       0       0       0       0
  16  3   0   0   0   0       0       0       0       0
 ....................................

   9  2   0   0   0  13       1       1       1       1
  10  2   0   0   0   0       0       0       0       0
  11  2   0   0   0   0       0       0       0       0
  12  2   0   0   0   0       0       0       0       0
 ....................................

   5  1   0   0   0   9       1       1       1       1
   6  1   0   0   0   0       0       0       0       0
   7  1   0   0   0   0       0       0       0       0
   8  1   0   0   0   0       0       0       0       0
 ....................................

   1  0   0   0   0   5       1       1       1       1
   2  0   0   0   0   0       0       0       0       0
   3  0   0   0   0   0       0       0       0       0
   4  0   0   0   0   0       0       0       0       0
 ....................................


 lprune: l(*,*,*) pruned with nwalk=       1 nvalwt=       1 nprune=       0
 ynew( 1):
  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0
  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0
  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
 xbarnew( 1):
  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  1
 limn array= 1
  1
 limvec array= 1
  2
 reduced DRT,invalid= 0
iwalk=   1step: 3 3 3 3 3
 level  0 through level  4 of the drt:

 row lev  l0  l1  l2  l3     y0      y1      y2    xbar 

  17  4   0   0   0  20       1       1       1       1
  18  4   0   0   0   0       0       0       0       0
  19  4   0   0   0   0       0       0       0       0
 ....................................

  13  3   0   0   0  17       1       1       1       1
  14  3   0   0   0   0       0       0       0       0
  15  3   0   0   0   0       0       0       0       0
  16  3   0   0   0   0       0       0       0       0
 ....................................

   9  2   0   0   0  13       1       1       1       1
  10  2   0   0   0   0       0       0       0       0
  11  2   0   0   0   0       0       0       0       0
  12  2   0   0   0   0       0       0       0       0
 ....................................

   5  1   0   0   0   9       1       1       1       1
   6  1   0   0   0   0       0       0       0       0
   7  1   0   0   0   0       0       0       0       0
   8  1   0   0   0   0       0       0       0       0
 ....................................

   1  0   0   0   0   5       1       1       1       1
   2  0   0   0   0   0       0       0       0       0
   3  0   0   0   0   0       0       0       0       0
   4  0   0   0   0   0       0       0       0       0
 ....................................

================ pruneseg on segment    2(   2)================
 original DRT
iwalk=   2step: 3 3 3 3 2
iwalk=   3step: 3 3 3 2 3
iwalk=   4step: 3 3 2 3 3
iwalk=   5step: 3 2 3 3 3
iwalk=   6step: 2 3 3 3 3
 level  0 through level  4 of the drt:

 row lev  l0  l1  l2  l3     y0      y1      y2    xbar 

  17  4   0   0   0  20       1       1       1       1
  18  4   0   0  20   0       1       1       0       1
  19  4   0   0   0   0       0       0       0       0
 ....................................

  13  3   0   0   0  17       1       1       1       1
  14  3   0   0  17  18       2       2       1       2
  15  3   0   0   0   0       0       0       0       0
  16  3   0   0   0   0       0       0       0       0
 ....................................

   9  2   0   0   0  13       1       1       1       1
  10  2   0   0  13  14       3       3       2       3
  11  2   0   0   0   0       0       0       0       0
  12  2   0   0   0   0       0       0       0       0
 ....................................

   5  1   0   0   0   9       1       1       1       1
   6  1   0   0   9  10       4       4       3       4
   7  1   0   0   0   0       0       0       0       0
   8  1   0   0   0   0       0       0       0       0
 ....................................

   1  0   0   0   0   0       0       0       0       0
   2  0   0   0   5   6       6       6       5       5
   3  0   0   0   0   0       0       0       0       0
   4  0   0   0   0   0       0       0       0       0
 ....................................


 lprune: l(*,*,*) pruned with nwalk=       5 nvalwt=       4 nprune=       0
 ynew( 2):
  0  5  0  0  1  4  0  0  1  3  0  0  1  2  0  0  1  1  0  0
  0  5  0  0  1  4  0  0  1  3  0  0  1  2  0  0  1  1  0  0
  0  4  0  0  1  3  0  0  1  2  0  0  1  1  0  0  1  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
 xbarnew( 2):
  0  5  0  0  1  4  0  0  1  3  0  0  1  2  0  0  1  1  0  1
 limn array= 5
  1  2  3  4  5
 limvec array= 5
  0  2  2  2  2
 reduced DRT,invalid= 0
iwalk=   1step: 3 3 3 3 2
iwalk=   2step: 3 3 3 2 3
iwalk=   3step: 3 3 2 3 3
iwalk=   4step: 3 2 3 3 3
iwalk=   5step: 2 3 3 3 3
 level  0 through level  4 of the drt:

 row lev  l0  l1  l2  l3     y0      y1      y2    xbar 

  17  4   0   0   0  20       1       1       1       1
  18  4   0   0  20   0       1       1       0       1
  19  4   0   0   0   0       0       0       0       0
 ....................................

  13  3   0   0   0  17       1       1       1       1
  14  3   0   0  17  18       2       2       1       2
  15  3   0   0   0   0       0       0       0       0
  16  3   0   0   0   0       0       0       0       0
 ....................................

   9  2   0   0   0  13       1       1       1       1
  10  2   0   0  13  14       3       3       2       3
  11  2   0   0   0   0       0       0       0       0
  12  2   0   0   0   0       0       0       0       0
 ....................................

   5  1   0   0   0   9       1       1       1       1
   6  1   0   0   9  10       4       4       3       4
   7  1   0   0   0   0       0       0       0       0
   8  1   0   0   0   0       0       0       0       0
 ....................................

   1  0   0   0   0   0       0       0       0       0
   2  0   0   0   5   6       5       5       4       5
   3  0   0   0   0   0       0       0       0       0
   4  0   0   0   0   0       0       0       0       0
 ....................................

================ pruneseg on segment    3(   3)================
 original DRT
iwalk=   7step: 3 3 3 2 2
iwalk=   8step: 3 3 2 3 2
iwalk=   9step: 3 3 2 2 3
iwalk=  10step: 3 2 3 3 2
iwalk=  11step: 3 2 3 2 3
iwalk=  12step: 3 2 2 3 3
iwalk=  13step: 2 3 3 3 2
iwalk=  14step: 2 3 3 2 3
iwalk=  15step: 2 3 2 3 3
iwalk=  16step: 2 2 3 3 3
 level  0 through level  4 of the drt:

 row lev  l0  l1  l2  l3     y0      y1      y2    xbar 

  17  4   0   0   0  20       1       1       1       1
  18  4   0   0  20   0       1       1       0       1
  19  4  20   0   0   0       0       0       0       1
 ....................................

  13  3   0   0   0  17       1       1       1       1
  14  3   0   0  17  18       2       2       1       2
  15  3   0   0  18   0       1       1       0       1
  16  3  17  18   0  19       2       1       1       3
 ....................................

   9  2   0   0   0  13       1       1       1       1
  10  2   0   0  13  14       3       3       2       3
  11  2   0   0  14  15       3       3       1       3
  12  2  13  14   0  16       5       3       3       6
 ....................................

   5  1   0   0   0   9       1       1       1       1
   6  1   0   0   9  10       4       4       3       4
   7  1   0   0  10  11       6       6       3       6
   8  1   9  10   0  12       9       6       6      10
 ....................................

   1  0   0   0   0   0       0       0       0       0
   2  0   0   0   0   0       0       0       0       0
   3  0   0   0   6   7      16      16      12      10
   4  0   5   6   0   8      30      26      26      15
 ....................................


 lprune: l(*,*,*) pruned with nwalk=      10 nvalwt=      10 nprune=      14
 ynew( 3):
  0  0 10  0  0  4  6  0  1  3  3  0  1  2  1  0  1  1  0  0
  0  0 10  0  0  4  6  0  1  3  3  0  1  2  1  0  1  1  0  0
  0  0  6  0  0  3  3  0  1  2  1  0  1  1  0  0  1  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
 xbarnew( 3):
  0  0 10  0  0  4  6  0  1  3  3  0  1  2  1  0  1  1  0  1
 limn array= 10
  1  2  3  4  5  6  7  8  9 10
 limvec array= 10
  2  2  2  2  2  2  2  2  2  2
 reduced DRT,invalid= 0
iwalk=   1step: 3 3 3 2 2
iwalk=   2step: 3 3 2 3 2
iwalk=   3step: 3 3 2 2 3
iwalk=   4step: 3 2 3 3 2
iwalk=   5step: 3 2 3 2 3
iwalk=   6step: 3 2 2 3 3
iwalk=   7step: 2 3 3 3 2
iwalk=   8step: 2 3 3 2 3
iwalk=   9step: 2 3 2 3 3
iwalk=  10step: 2 2 3 3 3
 level  0 through level  4 of the drt:

 row lev  l0  l1  l2  l3     y0      y1      y2    xbar 

  17  4   0   0   0  20       1       1       1       1
  18  4   0   0  20   0       1       1       0       1
  19  4   0   0   0   0       0       0       0       0
 ....................................

  13  3   0   0   0  17       1       1       1       1
  14  3   0   0  17  18       2       2       1       2
  15  3   0   0  18   0       1       1       0       1
  16  3   0   0   0   0       0       0       0       0
 ....................................

   9  2   0   0   0  13       1       1       1       1
  10  2   0   0  13  14       3       3       2       3
  11  2   0   0  14  15       3       3       1       3
  12  2   0   0   0   0       0       0       0       0
 ....................................

   5  1   0   0   0   0       0       0       0       0
   6  1   0   0   9  10       4       4       3       4
   7  1   0   0  10  11       6       6       3       6
   8  1   0   0   0   0       0       0       0       0
 ....................................

   1  0   0   0   0   0       0       0       0       0
   2  0   0   0   0   0       0       0       0       0
   3  0   0   0   6   7      10      10       6      10
   4  0   0   0   0   0       0       0       0       0
 ....................................

================ pruneseg on segment    4(   3)================
 original DRT
iwalk=  17step: 3 3 3 3 0
iwalk=  18step: 3 3 3 1 2
iwalk=  19step: 3 3 3 0 3
iwalk=  20step: 3 3 1 3 2
iwalk=  21step: 3 3 1 2 3
iwalk=  22step: 3 3 0 3 3
iwalk=  23step: 3 1 3 3 2
iwalk=  24step: 3 1 3 2 3
iwalk=  25step: 3 1 2 3 3
iwalk=  26step: 3 0 3 3 3
iwalk=  27step: 1 3 3 3 2
iwalk=  28step: 1 3 3 2 3
iwalk=  29step: 1 3 2 3 3
iwalk=  30step: 1 2 3 3 3
iwalk=  31step: 0 3 3 3 3
 level  0 through level  4 of the drt:

 row lev  l0  l1  l2  l3     y0      y1      y2    xbar 

  17  4   0   0   0  20       1       1       1       1
  18  4   0   0  20   0       1       1       0       1
  19  4  20   0   0   0       0       0       0       1
 ....................................

  13  3   0   0   0  17       1       1       1       1
  14  3   0   0  17  18       2       2       1       2
  15  3   0   0  18   0       1       1       0       1
  16  3  17  18   0  19       2       1       1       3
 ....................................

   9  2   0   0   0  13       1       1       1       1
  10  2   0   0  13  14       3       3       2       3
  11  2   0   0  14  15       3       3       1       3
  12  2  13  14   0  16       5       3       3       6
 ....................................

   5  1   0   0   0   9       1       1       1       1
   6  1   0   0   9  10       4       4       3       4
   7  1   0   0  10  11       6       6       3       6
   8  1   9  10   0  12       9       6       6      10
 ....................................

   1  0   0   0   0   0       0       0       0       0
   2  0   0   0   0   0       0       0       0       0
   3  0   0   0   6   7      16      16      12      10
   4  0   5   6   0   8      30      26      26      15
 ....................................


 lprune: l(*,*,*) pruned with nwalk=      15 nvalwt=      15 nprune=       7
 ynew( 3):
  0  0  0 14  1  4  0  9  1  3  0  5  1  2  0  2  1  1  0  0
  0  0  0 10  1  4  0  6  1  3  0  3  1  2  0  1  1  1  0  0
  0  0  0 10  1  3  0  6  1  2  0  3  1  1  0  1  1  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
 xbarnew( 3):
  0  0  0 15  1  4  0 10  1  3  0  6  1  2  0  3  1  1  1  1
 limn array= 15
  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15
 limvec array= 15
  2  2  2  2  2  2  2  2  2  2  2  2  2  2  2
 reduced DRT,invalid= 0
iwalk=   1step: 3 3 3 3 0
iwalk=   2step: 3 3 3 1 2
iwalk=   3step: 3 3 3 0 3
iwalk=   4step: 3 3 1 3 2
iwalk=   5step: 3 3 1 2 3
iwalk=   6step: 3 3 0 3 3
iwalk=   7step: 3 1 3 3 2
iwalk=   8step: 3 1 3 2 3
iwalk=   9step: 3 1 2 3 3
iwalk=  10step: 3 0 3 3 3
iwalk=  11step: 1 3 3 3 2
iwalk=  12step: 1 3 3 2 3
iwalk=  13step: 1 3 2 3 3
iwalk=  14step: 1 2 3 3 3
iwalk=  15step: 0 3 3 3 3
 level  0 through level  4 of the drt:

 row lev  l0  l1  l2  l3     y0      y1      y2    xbar 

  17  4   0   0   0  20       1       1       1       1
  18  4   0   0  20   0       1       1       0       1
  19  4  20   0   0   0       0       0       0       1
 ....................................

  13  3   0   0   0  17       1       1       1       1
  14  3   0   0  17  18       2       2       1       2
  15  3   0   0   0   0       0       0       0       0
  16  3  17  18   0  19       2       1       1       3
 ....................................

   9  2   0   0   0  13       1       1       1       1
  10  2   0   0  13  14       3       3       2       3
  11  2   0   0   0   0       0       0       0       0
  12  2  13  14   0  16       5       3       3       6
 ....................................

   5  1   0   0   0   9       1       1       1       1
   6  1   0   0   9  10       4       4       3       4
   7  1   0   0   0   0       0       0       0       0
   8  1   9  10   0  12       9       6       6      10
 ....................................

   1  0   0   0   0   0       0       0       0       0
   2  0   0   0   0   0       0       0       0       0
   3  0   0   0   0   0       0       0       0       0
   4  0   5   6   0   8      14      10      10      15
 ....................................




                   segmentation summary for type diagonal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1           1|         1|         0|         1|         0|         1|
 -------------------------------------------------------------------------------
  Y 2           5|        16|         1|         5|         1|         2|
 -------------------------------------------------------------------------------
  X 3          10|        78|        17|        10|         6|         3|
 -------------------------------------------------------------------------------
  W 4          15|       187|        95|        15|        16|         4|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>       282


 297 dimension of the ci-matrix ->>>       282


 297 dimension of the ci-matrix ->>>         7

                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      10       1         78          1      10       1
     2  4   1    25      two-ext wz   2X  4 1      15       1        187          1      15       1
     3  4   3    26      two-ext wx   2X  4 3      15      10        187         78      15      10
     4  2   1    11      one-ext yz   1X  2 1       5       1         16          1       5       1
     5  3   2    15      1ex3ex  yx   3X  3 2      10       5         78         16      10       5
     6  4   2    16      1ex3ex  yw   3X  4 2      15       5        187         16      15       5
     7  1   1     1      allint zz    OX  1 1       1       1          1          1       1       1
     8  2   2     5      0ex2ex yy    OX  2 2       5       5         16         16       5       5
     9  3   3     6      0ex2ex xx    OX  3 3      10      10         78         78      10      10
    10  4   4     7      0ex2ex ww    OX  4 4      15      15        187        187      15      15
    11  1   1    75      dg-024ext z  DG  1 1       1       1          1          1       1       1
    12  2   2    45      4exdg024 y   DG  2 2       5       5         16         16       5       5
    13  3   3    46      4exdg024 x   DG  3 3      10      10         78         78      10      10
    14  4   4    47      4exdg024 w   DG  4 4      15      15        187        187      15      15
----------------------------------------------------------------------------------------------------
 blstr diagel,allext,nmin2=    72
 onel.diag.
 -0.339333E+01-0.424384E+01-0.517452E+01-0.429185E+01-0.318550E+01-0.409743E+01-0.514825E+01-0.532554E+01-0.330430E+02-0.786345E+01
 -0.692854E+01-0.665285E+01-0.711540E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1        72        72
 blstr diagel,allext,nmin2=    72
 onel.diag.
 -0.339333E+01-0.424384E+01-0.517452E+01-0.429185E+01-0.318550E+01-0.409743E+01-0.514825E+01-0.532554E+01-0.330430E+02-0.786345E+01
 -0.692854E+01-0.665285E+01-0.711540E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1        72        72
 blstr diagel,allext,nmin2=    72
 onel.diag.
 -0.339333E+01-0.424384E+01-0.517452E+01-0.429185E+01-0.318550E+01-0.409743E+01-0.514825E+01-0.532554E+01-0.330430E+02-0.786345E+01
 -0.692854E+01-0.665285E+01-0.711540E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1        72        72
 DIAGEL COUNTS: DG0X,DG2X,DG4X= 440 145 30
 diagonal elements written to file   4

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        15 2x:         0 4x:         0
All internal counts: zz :         0 yy:         0 xx:         0 ww:         0
One-external counts: yz :         0 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         0    task #   2:         0    task #   3:         0    task #   4:         0
task #   5:         0    task #   6:         0    task #   7:        -1    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:        14    task #  12:         0
task #  13:         0    task #  14:         0    task #
 reference space has dimension       1

          hamiltonian matrix in the reference space 

              rcsf   1
 rcsf   1   -75.97986149

    root           eigenvalues
    ----           ------------
       1         -75.9798614863

          eigenvectors in the subspace basis

                v:   1
 rcsf   1     1.00000000

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt= 1

         vector  1 from unit 11 written to unit 49 filename cirefv                                                      
 ufvoutnew: ... writing  recamt= 1

         vector  1 from unit 11 written to unit 49 filename cirefv                                                      

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:               282
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             1
 number of iterations:                                   20
 residual norm convergence criteria:               0.001000

 elast before the main iterative loop 
 elast =  -75.9798615

 rtflw before main loop =  0

          starting ci iteration   1

 cosmocalc =  1
 rootcalc =  1
 repnuc =   9.31791392

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= T F

################################################################################
 one electron density matrix calculation 
################################################################################


 workspace allocation information: mxcore= 9999997

 space required:

    space required for calls in multd2:
       onex           40
       allin           0
       diagon         85
    max.      ---------
       maxnex         85

    total core space usage:
       maxnex         85
    max k-seg        187
    max k-ind         15
              ---------
       totmax        489

 total core array space available  9999997
 total used                            489
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=       3  D0X=       9  D0W=      15
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================

          ci-one-electron density block   1

                mo   1         mo   2         mo   3         mo   4         mo   5         mo   6         mo   7
   mo   1     2.00000000
   mo   2     0.00000000     2.00000000
   mo   3     0.00000000     0.00000000     2.00000000

          ci-one-electron density block   2

                mo   8         mo   9         mo  10         mo  11
   mo   8     2.00000000

          ci-one-electron density block   3

                mo  12         mo  13
   mo  12     2.00000000

          ci-one-electron density block   4


*****   symmetry block SYM1   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       2.00000000
       2       0.00000000     2.00000000
       3       0.00000000     0.00000000     2.00000000

               Column   5     Column   6     Column   7

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   2.0000000      2.0000000      2.0000000      0.0000000      0.0000000
   0.0000000      0.0000000

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7
  mo    1   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo    2   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo    3   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
  mo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
  mo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01
  mo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
  eigenvectors of nos in ao-basis, column    1
 -0.3228369E-03  0.3230443E+00  0.1321719E+00  0.7984047E+00  0.3316137E-01 -0.3194045E+00  0.1341918E+00
  eigenvectors of nos in ao-basis, column    2
  0.1000495E+01  0.2696422E-02 -0.3336425E-02 -0.2869215E-02  0.2559038E-02  0.5655623E-03  0.4957221E-03
  eigenvectors of nos in ao-basis, column    3
 -0.9955991E-02  0.8648327E+00 -0.4196472E-01 -0.1659608E+00  0.4508323E-01  0.2927345E+00 -0.1595825E+00
  eigenvectors of nos in ao-basis, column    4
  0.8043080E+00  0.3584484E+01 -0.4774623E+01 -0.1642289E+00  0.7383038E+00  0.9092698E+00 -0.7203884E-01
  eigenvectors of nos in ao-basis, column    5
 -0.4753886E-01  0.1574464E+00  0.8632391E+00 -0.2855046E+00 -0.1698922E+00 -0.9355255E-01 -0.7663530E+00
  eigenvectors of nos in ao-basis, column    6
  0.4180364E-01  0.5121776E+00  0.1250137E+00 -0.4930368E+00 -0.3163963E+00 -0.2031331E+01  0.1485333E+01
  eigenvectors of nos in ao-basis, column    7
 -0.3063047E-01 -0.2867419E+00 -0.7288835E-01 -0.1365442E+01  0.1869860E+01  0.4832592E+00 -0.1525690E+00


*****   symmetry block SYM2   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       2.00000000

               eno   1        eno   2        eno   3        eno   4
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   2.0000000      0.0000000      0.0000000      0.0000000

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4
  mo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
  mo    2   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
  mo    3   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01
  mo    4   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
  eigenvectors of nos in ao-basis, column    1
  0.7296311E+00 -0.9596331E-01 -0.5779483E+00  0.2386849E+00
  eigenvectors of nos in ao-basis, column    2
 -0.1471415E+01  0.2230179E+01  0.2402207E+00  0.6140911E+00
  eigenvectors of nos in ao-basis, column    3
 -0.4107477E+00 -0.4498567E+00 -0.2829207E-01 -0.1415984E+01
  eigenvectors of nos in ao-basis, column    4
  0.4229729E+00  0.6336095E+00  0.1903837E+01 -0.1506648E+01


*****   symmetry block SYM3   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2
       1       2.00000000

               eno   1        eno   2
 emo    1   0.100000E+01   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01

 occupation numbers of nos
   2.0000000      0.0000000

  eigenvectors of nos in mo-basis

                no   1         no   2
  mo    1   0.100000E+01   0.000000E+00
  mo    2   0.000000E+00   0.100000E+01
  eigenvectors of nos in ao-basis, column    1
  0.9249255E+00  0.9147553E-01
  eigenvectors of nos in ao-basis, column    2
 -0.1407113E+01  0.1681394E+01


 total number of electrons =   10.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***


 cosurf_and_phi
  1.262739748330  2.872870034186  1.567866969040 -0.047576150215
  1.652947872482  0.360731428865  3.496571379926 -0.066731836074
  0.594940552015  0.670555958619  3.845537291721 -0.080523453918
  2.390078334690  1.202722009900  2.566708449184 -0.041743778381
  1.989334673621  2.229216060677  2.001028520754 -0.042390324056
  2.919143888229  0.378144896597  2.099775822683 -0.018533459955
  0.105380882100  1.878884201281  3.371423292662 -0.078820441754
  0.783266721831  2.590884380442  2.520834408126 -0.067115874631
  2.011402526412  2.551496219550  0.814855178005 -0.016999346130
  0.099626804209  1.855073908563 -1.945822518898  0.046528955634
  0.119020599620  3.173161356543  1.415159765853 -0.053245630099
  0.915237473358  3.108118624501  0.463299650838 -0.028432873152
  0.462814589486  2.837210699514 -0.795516582628  0.003466236558
  1.408719892640  1.640288870679  3.148120355694 -0.067309653108
  2.650053602975  1.633766196698  1.655441764425 -0.018336760375
  1.142904167226  2.885766749848 -0.243475252462 -0.005840914357
  3.656106873706  0.713798214804  0.361832640492  0.034193739012
  2.408046317685  2.075600470298 -1.226192756897  0.042798138583
  1.295820311657  0.567673501678 -2.747601655947  0.066749447590
  3.527730862121  1.045649613724 -1.064853177123  0.051479055449
  2.622898581638  1.024710819001 -2.241122746235  0.060505498942
  3.086808508398  1.794857685487 -0.179703829578  0.033348261225
  1.615872011596  1.674366500124 -2.147504385867  0.055107705632
  0.000006852884  1.035694667087 -2.361676372823  0.066965073057
  0.298815704890  0.969607820141 -2.408807386530  0.067881344521
  0.667723897920  1.245157743773 -2.338577856151  0.063487270917
  1.218123388571  2.025110412626 -1.616576841774  0.045889605654
  2.084186643139  2.293984793080 -0.480493513306  0.025916836581
  2.876642520254  1.658030225134  0.559035126479  0.019302671731
  3.282919570196  0.368088739237  1.091983758387  0.019867150420
  0.467932162408  1.694535407938 -1.941510815499  0.053316353028
  1.299770347024  2.453875604722 -0.850324284820  0.020824581836
  2.242050270258  2.245320705184  0.385739526123  0.004331098005
  2.923104801922  1.151131828431  1.279135167181  0.003501662715
  0.427044967836  0.580963354323 -2.585108185092  0.070608953602
 end_of_phi


 nsubv after electrostatic potential =  0

  Confirmation of dielectric energy for ground state
  edielnew =   0.


 **************************************************
 ***Cosmo calculation for ground state*************
 **************************************************


 cosurf_and_qcos_from cosmo(3
  1.262739748330  2.872870034186  1.567866969040  0.005428495880
  1.652947872482  0.360731428865  3.496571379926  0.005368482408
  0.594940552015  0.670555958619  3.845537291721  0.009157485684
  2.390078334690  1.202722009900  2.566708449184  0.004507866295
  1.989334673621  2.229216060677  2.001028520754  0.003572200057
  2.919143888229  0.378144896597  2.099775822683  0.000070018078
  0.105380882100  1.878884201281  3.371423292662  0.000770781606
  0.783266721831  2.590884380442  2.520834408126  0.010326405883
  2.011402526412  2.551496219550  0.814855178005  0.002024771637
  0.099626804209  1.855073908563 -1.945822518898 -0.000326283639
  0.119020599620  3.173161356543  1.415159765853  0.004844365333
  0.915237473358  3.108118624501  0.463299650838  0.005465245260
  0.462814589486  2.837210699514 -0.795516582628  0.001473606019
  1.408719892640  1.640288870679  3.148120355694  0.010095881094
  2.650053602975  1.633766196698  1.655441764425  0.000953040608
  1.142904167226  2.885766749848 -0.243475252462  0.001150232946
  3.656106873706  0.713798214804  0.361832640492 -0.007082826791
  2.408046317685  2.075600470298 -1.226192756897 -0.005367486717
  1.295820311657  0.567673501678 -2.747601655947 -0.006425893149
  3.527730862121  1.045649613724 -1.064853177123 -0.009809048796
  2.622898581638  1.024710819001 -2.241122746235 -0.010020626326
  3.086808508398  1.794857685487 -0.179703829578 -0.004426396331
  1.615872011596  1.674366500124 -2.147504385867 -0.005053025420
  0.000006852884  1.035694667087 -2.361676372823 -0.000251483454
  0.298815704890  0.969607820141 -2.408807386530 -0.000404447015
  0.667723897920  1.245157743773 -2.338577856151 -0.001347380122
  1.218123388571  2.025110412626 -1.616576841774 -0.002096005983
  2.084186643139  2.293984793080 -0.480493513306 -0.001400267367
  2.876642520254  1.658030225134  0.559035126479 -0.001630154643
  3.282919570196  0.368088739237  1.091983758387 -0.001887027640
  0.467932162408  1.694535407938 -1.941510815499 -0.002537379047
  1.299770347024  2.453875604722 -0.850324284820 -0.000892170134
  2.242050270258  2.245320705184  0.385739526123 -0.000640924024
  2.923104801922  1.151131828431  1.279135167181 -0.001117758924
  0.427044967836  0.580963354323 -2.585108185092 -0.003492521723
 end_of_qcos

 sum of the negative charges =  -0.0662091072

 sum of the positive charges =   0.0652088788

 total sum =  -0.00100022846

 *** qcos is copied to qcosdalton ***

 fepsi =   1.
 *** qcosdalton is mult by fepsi ***
 cosurf and qcosdalton
  1.262739748330  2.872870034186  1.567866969040  0.005428495880
  1.652947872482  0.360731428865  3.496571379926  0.005368482408
  0.594940552015  0.670555958619  3.845537291721  0.009157485684
  2.390078334690  1.202722009900  2.566708449184  0.004507866295
  1.989334673621  2.229216060677  2.001028520754  0.003572200057
  2.919143888229  0.378144896597  2.099775822683  0.000070018078
  0.105380882100  1.878884201281  3.371423292662  0.000770781606
  0.783266721831  2.590884380442  2.520834408126  0.010326405883
  2.011402526412  2.551496219550  0.814855178005  0.002024771637
  0.099626804209  1.855073908563 -1.945822518898 -0.000326283639
  0.119020599620  3.173161356543  1.415159765853  0.004844365333
  0.915237473358  3.108118624501  0.463299650838  0.005465245260
  0.462814589486  2.837210699514 -0.795516582628  0.001473606019
  1.408719892640  1.640288870679  3.148120355694  0.010095881094
  2.650053602975  1.633766196698  1.655441764425  0.000953040608
  1.142904167226  2.885766749848 -0.243475252462  0.001150232946
  3.656106873706  0.713798214804  0.361832640492 -0.007082826791
  2.408046317685  2.075600470298 -1.226192756897 -0.005367486717
  1.295820311657  0.567673501678 -2.747601655947 -0.006425893149
  3.527730862121  1.045649613724 -1.064853177123 -0.009809048796
  2.622898581638  1.024710819001 -2.241122746235 -0.010020626326
  3.086808508398  1.794857685487 -0.179703829578 -0.004426396331
  1.615872011596  1.674366500124 -2.147504385867 -0.005053025420
  0.000006852884  1.035694667087 -2.361676372823 -0.000251483454
  0.298815704890  0.969607820141 -2.408807386530 -0.000404447015
  0.667723897920  1.245157743773 -2.338577856151 -0.001347380122
  1.218123388571  2.025110412626 -1.616576841774 -0.002096005983
  2.084186643139  2.293984793080 -0.480493513306 -0.001400267367
  2.876642520254  1.658030225134  0.559035126479 -0.001630154643
  3.282919570196  0.368088739237  1.091983758387 -0.001887027640
  0.467932162408  1.694535407938 -1.941510815499 -0.002537379047
  1.299770347024  2.453875604722 -0.850324284820 -0.000892170134
  2.242050270258  2.245320705184  0.385739526123 -0.000640924024
  2.923104801922  1.151131828431  1.279135167181 -0.001117758924
  0.427044967836  0.580963354323 -2.585108185092 -0.003492521723
 end of cosurf and qcosdalton

 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 **Starting calculation of Solv Mod Integrals**
 ****Module DALTON-COSMO****
 Warning! Large contrib. to nucrep.The distance is   1.37057673E-05
 ** Total nuclear repulsion energy ** =   9.36249586578760
 Warning! Large contrib. to nucrep.The distance is   1.37057673E-05
 screening nuclear repulsion energy   0.0179233803

 Total-screening nuclear repulsion energy   9.34457249


 Adding T+Vsolv ...
 maxdens  41
 *** End of DALTON-COSMO calculation ***

  original map vector  9 10 11 1 2 3 4 12 5 6 7 13 8 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0


          sorted Vsolv matrix  block   1

               nbf   1        nbf   2        nbf   3        nbf   4        nbf   5        nbf   6        nbf   7        nbf   8
  nbf   1    -3.37266008
  nbf   2    -1.02574021    -4.22683807
  nbf   3    -0.56090050    -0.75767627    -5.19337855
  nbf   4    -0.99336393    -0.51548061     0.14845872    -4.29204546
  nbf   5     0.00000000     0.00000000     0.00000000     0.00000000    -3.16175680
  nbf   6     0.00000000     0.00000000     0.00000000     0.00000000     0.94903453    -4.09289189
  nbf   7     0.00000000     0.00000000     0.00000000     0.00000000    -1.36975681     0.82814804    -5.15590624
  nbf   8     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000    -5.33801686
  nbf   9     0.18452390     0.21037606    -0.09893816     0.48711930     0.00000000     0.00000000     0.00000000     0.00000000
  nbf  10    -0.99781768    -0.82094835    -0.03601040    -1.35188172     0.00000000     0.00000000     0.00000000     0.00000000
  nbf  11     0.54747030     0.53298009     1.93017490    -0.43957480     0.00000000     0.00000000     0.00000000     0.00000000
  nbf  12     0.00000000     0.00000000     0.00000000     0.00000000     1.25228481    -0.64357631     1.61607127     0.00000000
  nbf  13     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     2.13652373

               nbf   9        nbf  10        nbf  11        nbf  12        nbf  13
  nbf   9   -33.05151022
  nbf  10     0.58376197    -7.86314330
  nbf  11     0.19725151    -0.28241962    -6.94099940
  nbf  12     0.00000000     0.00000000     0.00000000    -6.64833379
  nbf  13     0.00000000     0.00000000     0.00000000     0.00000000    -7.12617086
 insert_onel_diag: nmin2=    72

 onel.diag.
 -0.337266E+01-0.422684E+01-0.519338E+01-0.429205E+01-0.316176E+01-0.409289E+01-0.515591E+01-0.533802E+01-0.330515E+02-0.786314E+01
 -0.694100E+01-0.664833E+01-0.712617E+01

 i,start,strti,fin,filind,bufszi,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095         1        72        72

 4 external diag. modified integrals

  0.305152117 -3.37266008  0.391296914  0.321351069  0.572582582 -4.22683807
  0.349586094  0.294539037  0.451362637  0.402847816  0.602124811 -5.19337855
  0.369310705  0.314964135  0.491285293  0.442200391  0.585720584  0.455686639
  0.53809996 -4.29204546  0.36740475  0.217402198  0.342785167  0.324072639
  0.309081901  0.30054174  0.340137796  0.305885347  0.345295031  0.317796422
  0.661582609  0.358368836  0.446424453  0.415485847  0.455059205  0.429064553
  0.347475171  0.3117064  0.469763529  0.424634219  0.576372295  0.50894225
  0.586714427  0.461202627  0.298410214 -3.1617568  0.345428808  0.295530901
  0.493954762 -4.09289189  0.371232474  0.284676514  0.497909282  0.406544708
  0.595585811 -5.15590624  0.354410878  0.30839141  0.461407323  0.430967773
  0.572377226  0.513899065  0.630771519  0.459889364  0.318648256  0.308137954
  0.435721601  0.420191107  0.571676563  0.519441816  0.604906175 -5.33801686
 i,start,strti,fin,filind,bufszi,nmbuf,nmin2, nd4ext = 
         1         1         2         2        12      4095         1        72        72

 all internall diag. modified integrals

  4.73965583 -33.0515102  1.04645007  0.064160731  0.756347039 -7.8631433
  0.988463093  0.0310165861  0.670322362  0.116055162  0.742588725 -6.9409994
  0.870059666  0.0196359293  0.672561141  0.148950646  0.601332934
  0.0411197922  0.654217107 -6.64833379  1.04308566  0.0304919035  0.712655546
  0.129195784  0.66928182  0.0462150474  0.616077735  0.0284090877  0.765266517
 -7.12617086
 blstr diagel,allext,nmin2=    72
 onel.diag.
 -0.337266E+01-0.422684E+01-0.519338E+01-0.429205E+01-0.316176E+01-0.409289E+01-0.515591E+01-0.533802E+01-0.330515E+02-0.786314E+01
 -0.694100E+01-0.664833E+01-0.712617E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1        72        72
 blstr diagel,allext,nmin2=    72
 onel.diag.
 -0.337266E+01-0.422684E+01-0.519338E+01-0.429205E+01-0.316176E+01-0.409289E+01-0.515591E+01-0.533802E+01-0.330515E+02-0.786314E+01
 -0.694100E+01-0.664833E+01-0.712617E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1        72        72
 blstr diagel,allext,nmin2=    72
 onel.diag.
 -0.337266E+01-0.422684E+01-0.519338E+01-0.429205E+01-0.316176E+01-0.409289E+01-0.515591E+01-0.533802E+01-0.330515E+02-0.786314E+01
 -0.694100E+01-0.664833E+01-0.712617E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1        72        72
 DIAGEL COUNTS: DG0X,DG2X,DG4X= 440 145 30
 =========== Executing IN-CORE method ==========
 norm multnew:  1.
 threx: total number of loops= 14
 threx: total number of loops= 24
 blstr diagel,allext,nmin2=    72
 onel.diag.
 -0.337266E+01-0.422684E+01-0.519338E+01-0.429205E+01-0.316176E+01-0.409289E+01-0.515591E+01-0.533802E+01-0.330515E+02-0.786314E+01
 -0.694100E+01-0.664833E+01-0.712617E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1        72        72
 blstr diagel,allext,nmin2=    72
 onel.diag.
 -0.337266E+01-0.422684E+01-0.519338E+01-0.429205E+01-0.316176E+01-0.409289E+01-0.515591E+01-0.533802E+01-0.330515E+02-0.786314E+01
 -0.694100E+01-0.664833E+01-0.712617E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1        72        72
 blstr diagel,allext,nmin2=    72
 onel.diag.
 -0.337266E+01-0.422684E+01-0.519338E+01-0.429205E+01-0.316176E+01-0.409289E+01-0.515591E+01-0.533802E+01-0.330515E+02-0.786314E+01
 -0.694100E+01-0.664833E+01-0.712617E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1        72        72


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        12 xx:        36 ww:        79
One-external counts: yz :        25 yx:       118 yw:       166
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        39 yw:        51

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        11
task #   9:        28    task #  10:        71    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 matrix-vector product formed for vector   1
 rtolcosmo =   0.001
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference-subspace vector overlap matrix  block   1

                ci   1
 ref:   1     1.00000000

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000
 subspace has dimension  1

          (h-repnuc*1) matrix in the subspace basis

                x:   1
   x:   1   -85.35158114

          overlap matrix in the subspace basis

                x:   1
   x:   1     1.00000000

          eigenvectors and eigenvalues in the subspace basis

                v:   1

   energy   -85.35158114

   x:   1     1.00000000

          eigenvectors and eigenvalues in the schmidt orthonormalized basis

                v:   1

   energy   -85.35158114

  zx:   1     1.00000000

          <ref|baseci> overlap matrix  block   1

                ci   1
 ref:   1     1.00000000

  write the transformed vectors back to cosmofile

 rtflw =  0
 freezing =  F

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -76.0070086514 -9.2908E+00  1.6555E-01  7.0128E-01  1.0000E-03


 total energy in cosmo calc 
e(rootcalc) + repnuc=     -76.0070086514
dielectric energy =      -0.0135735826
deltaediel =       0.0135735826
e(rootcalc) + repnuc - ediel =     -75.9934350688
e(rootcalc) + repnuc - edielnew =     -76.0070086514
deltaelast =      75.9934350688

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.01   0.00   0.00   0.01         0.    0.0000
    2   25    0   0.01   0.00   0.00   0.01         0.    0.0000
    3   26    0   0.02   0.00   0.00   0.01         0.    0.0000
    4   11    0   0.01   0.00   0.00   0.01         0.    0.0000
    5   15    0   0.05   0.00   0.00   0.04         0.    0.0000
    6   16    0   0.01   0.00   0.00   0.01         0.    0.0000
    7    1    0   0.01   0.00   0.00   0.00         0.    0.0000
    8    5    0   0.02   0.00   0.00   0.01         0.    0.0000
    9    6    0   0.02   0.00   0.00   0.02         0.    0.0000
   10    7    0   0.02   0.00   0.00   0.02         0.    0.0000
   11   75    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.01   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.00   0.00   0.00   0.00         0.    0.0000
   14   47    0   0.01   0.00   0.00   0.00         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.2000s 
time spent in multnx:                   0.1400s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0300s 
total time per CI iteration:            0.8200s 

          starting ci iteration   2

 cosmocalc =  1
 rootcalc =  1
 repnuc =   9.34457249

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=       3  D0X=       9  D0W=      15
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================

          ci-one-electron density block   1

                mo   1         mo   2         mo   3         mo   4         mo   5         mo   6         mo   7
   mo   1     2.00000000
   mo   2     0.00000000     2.00000000
   mo   3     0.00000000     0.00000000     2.00000000

          ci-one-electron density block   2

                mo   8         mo   9         mo  10         mo  11
   mo   8     2.00000000

          ci-one-electron density block   3

                mo  12         mo  13
   mo  12     2.00000000

          ci-one-electron density block   4


*****   symmetry block SYM1   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       2.00000000
       2       0.00000000     2.00000000
       3       0.00000000     0.00000000     2.00000000

               Column   5     Column   6     Column   7

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   2.0000000      2.0000000      2.0000000      0.0000000      0.0000000
   0.0000000      0.0000000

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7
  mo    1   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo    2   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo    3   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
  mo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
  mo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
  mo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01
  mo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
  eigenvectors of nos in ao-basis, column    1
 -0.3228369E-03  0.3230443E+00  0.1321719E+00  0.7984047E+00  0.3316137E-01 -0.3194045E+00  0.1341918E+00
  eigenvectors of nos in ao-basis, column    2
  0.1000495E+01  0.2696422E-02 -0.3336425E-02 -0.2869215E-02  0.2559038E-02  0.5655623E-03  0.4957221E-03
  eigenvectors of nos in ao-basis, column    3
 -0.9955991E-02  0.8648327E+00 -0.4196472E-01 -0.1659608E+00  0.4508323E-01  0.2927345E+00 -0.1595825E+00
  eigenvectors of nos in ao-basis, column    4
  0.8043080E+00  0.3584484E+01 -0.4774623E+01 -0.1642289E+00  0.7383038E+00  0.9092698E+00 -0.7203884E-01
  eigenvectors of nos in ao-basis, column    5
 -0.4753886E-01  0.1574464E+00  0.8632391E+00 -0.2855046E+00 -0.1698922E+00 -0.9355255E-01 -0.7663530E+00
  eigenvectors of nos in ao-basis, column    6
  0.4180364E-01  0.5121776E+00  0.1250137E+00 -0.4930368E+00 -0.3163963E+00 -0.2031331E+01  0.1485333E+01
  eigenvectors of nos in ao-basis, column    7
 -0.3063047E-01 -0.2867419E+00 -0.7288835E-01 -0.1365442E+01  0.1869860E+01  0.4832592E+00 -0.1525690E+00


*****   symmetry block SYM2   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       2.00000000

               eno   1        eno   2        eno   3        eno   4
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   2.0000000      0.0000000      0.0000000      0.0000000

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4
  mo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
  mo    2   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
  mo    3   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01
  mo    4   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
  eigenvectors of nos in ao-basis, column    1
  0.7296311E+00 -0.9596331E-01 -0.5779483E+00  0.2386849E+00
  eigenvectors of nos in ao-basis, column    2
 -0.1471415E+01  0.2230179E+01  0.2402207E+00  0.6140911E+00
  eigenvectors of nos in ao-basis, column    3
 -0.4107477E+00 -0.4498567E+00 -0.2829207E-01 -0.1415984E+01
  eigenvectors of nos in ao-basis, column    4
  0.4229729E+00  0.6336095E+00  0.1903837E+01 -0.1506648E+01


*****   symmetry block SYM3   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2
       1       2.00000000

               eno   1        eno   2
 emo    1   0.100000E+01   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01

 occupation numbers of nos
   2.0000000      0.0000000

  eigenvectors of nos in mo-basis

                no   1         no   2
  mo    1   0.100000E+01   0.000000E+00
  mo    2   0.000000E+00   0.100000E+01
  eigenvectors of nos in ao-basis, column    1
  0.9249255E+00  0.9147553E-01
  eigenvectors of nos in ao-basis, column    2
 -0.1407113E+01  0.1681394E+01


 total number of electrons =   10.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***


 cosurf_and_phi
  1.262739748330  2.872870034186  1.567866969040 -0.047576150215
  1.652947872482  0.360731428865  3.496571379926 -0.066731836074
  0.594940552015  0.670555958619  3.845537291721 -0.080523453918
  2.390078334690  1.202722009900  2.566708449184 -0.041743778381
  1.989334673621  2.229216060677  2.001028520754 -0.042390324056
  2.919143888229  0.378144896597  2.099775822683 -0.018533459955
  0.105380882100  1.878884201281  3.371423292662 -0.078820441754
  0.783266721831  2.590884380442  2.520834408126 -0.067115874631
  2.011402526412  2.551496219550  0.814855178005 -0.016999346130
  0.099626804209  1.855073908563 -1.945822518898  0.046528955634
  0.119020599620  3.173161356543  1.415159765853 -0.053245630099
  0.915237473358  3.108118624501  0.463299650838 -0.028432873152
  0.462814589486  2.837210699514 -0.795516582628  0.003466236558
  1.408719892640  1.640288870679  3.148120355694 -0.067309653108
  2.650053602975  1.633766196698  1.655441764425 -0.018336760375
  1.142904167226  2.885766749848 -0.243475252462 -0.005840914357
  3.656106873706  0.713798214804  0.361832640492  0.034193739012
  2.408046317685  2.075600470298 -1.226192756897  0.042798138583
  1.295820311657  0.567673501678 -2.747601655947  0.066749447590
  3.527730862121  1.045649613724 -1.064853177123  0.051479055449
  2.622898581638  1.024710819001 -2.241122746235  0.060505498942
  3.086808508398  1.794857685487 -0.179703829578  0.033348261225
  1.615872011596  1.674366500124 -2.147504385867  0.055107705632
  0.000006852884  1.035694667087 -2.361676372823  0.066965073057
  0.298815704890  0.969607820141 -2.408807386530  0.067881344521
  0.667723897920  1.245157743773 -2.338577856151  0.063487270917
  1.218123388571  2.025110412626 -1.616576841774  0.045889605654
  2.084186643139  2.293984793080 -0.480493513306  0.025916836581
  2.876642520254  1.658030225134  0.559035126479  0.019302671731
  3.282919570196  0.368088739237  1.091983758387  0.019867150420
  0.467932162408  1.694535407938 -1.941510815499  0.053316353028
  1.299770347024  2.453875604722 -0.850324284820  0.020824581836
  2.242050270258  2.245320705184  0.385739526123  0.004331098005
  2.923104801922  1.151131828431  1.279135167181  0.003501662715
  0.427044967836  0.580963354323 -2.585108185092  0.070608953602
 end_of_phi


 nsubv after electrostatic potential =  1

  Confirmation of dielectric energy for ground state
  edielnew =  -0.00339339564

 =========== Executing IN-CORE method ==========
 norm multnew:  0.0636157467
 threx: total number of loops= 14
 threx: total number of loops= 24
 blstr diagel,allext,nmin2=    72
 onel.diag.
 -0.337266E+01-0.422684E+01-0.519338E+01-0.429205E+01-0.316176E+01-0.409289E+01-0.515591E+01-0.533802E+01-0.330515E+02-0.786314E+01
 -0.694100E+01-0.664833E+01-0.712617E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1        72        72
 blstr diagel,allext,nmin2=    72
 onel.diag.
 -0.337266E+01-0.422684E+01-0.519338E+01-0.429205E+01-0.316176E+01-0.409289E+01-0.515591E+01-0.533802E+01-0.330515E+02-0.786314E+01
 -0.694100E+01-0.664833E+01-0.712617E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1        72        72
 blstr diagel,allext,nmin2=    72
 onel.diag.
 -0.337266E+01-0.422684E+01-0.519338E+01-0.429205E+01-0.316176E+01-0.409289E+01-0.515591E+01-0.533802E+01-0.330515E+02-0.786314E+01
 -0.694100E+01-0.664833E+01-0.712617E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1        72        72


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        12 xx:        36 ww:        79
One-external counts: yz :        25 yx:       118 yw:       166
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        39 yw:        51

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        11
task #   9:        28    task #  10:        71    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 matrix-vector product formed for vector   2
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference-subspace vector overlap matrix  block   1

                ci   1         ci   2
 ref:   1     1.00000000     0.00000000

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.98252274    -0.18614259
 subspace has dimension  2

          (h-repnuc*1) matrix in the subspace basis

                x:   1         x:   2
   x:   1   -85.35158114
   x:   2    -0.16554915    -5.21721799

          overlap matrix in the subspace basis

                x:   1         x:   2
   x:   1     1.00000000
   x:   2     0.00000000     0.06361575

          eigenvectors and eigenvalues in the subspace basis

                v:   1         v:   2

   energy   -85.47593171   -81.88707360

   x:   1     0.98252274    -0.18614259
   x:   2     0.73801201     3.89547388

          eigenvectors and eigenvalues in the schmidt orthonormalized basis

                v:   1         v:   2

   energy   -85.47593171   -81.88707360

  zx:   1     0.98252274    -0.18614259
  zx:   2     0.18614259     0.98252274

          <ref|baseci> overlap matrix  block   1

                ci   1         ci   2
 ref:   1     1.00000000     0.00000000

  write the transformed vectors back to cosmofile

 rtflw =  0
 freezing =  F

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1    -76.1313592203 -9.2202E+00  4.5399E-03  1.1706E-01  1.0000E-03
 mr-sdci #  2  2    -72.5425011159  7.2543E+01  0.0000E+00  1.2431E+00  1.0000E-04


 total energy in cosmo calc 
e(rootcalc) + repnuc=     -76.1313592203
dielectric energy =      -0.0135735826
deltaediel =       0.0135735826
e(rootcalc) + repnuc - ediel =     -76.1177856377
e(rootcalc) + repnuc - edielnew =     -76.1313592203
deltaelast =      76.1177856377

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.02   0.00   0.00   0.01         0.    0.0000
    2   25    0   0.01   0.00   0.00   0.01         0.    0.0000
    3   26    0   0.01   0.00   0.00   0.01         0.    0.0000
    4   11    0   0.01   0.00   0.00   0.00         0.    0.0000
    5   15    0   0.02   0.00   0.00   0.01         0.    0.0000
    6   16    0   0.02   0.00   0.00   0.02         0.    0.0000
    7    1    0   0.01   0.00   0.00   0.01         0.    0.0000
    8    5    0   0.02   0.00   0.00   0.02         0.    0.0000
    9    6    0   0.01   0.00   0.00   0.01         0.    0.0000
   10    7    0   0.02   0.00   0.00   0.01         0.    0.0000
   11   75    0   0.01   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.01   0.00   0.00   0.01         0.    0.0000
   14   47    0   0.00   0.00   0.00   0.00         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1700s 
time spent in multnx:                   0.1200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0200s 
total time per CI iteration:            0.6900s 

          starting ci iteration   3

 cosmocalc =  1
 rootcalc =  1
 repnuc =   9.34457249

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=       3  D0X=       9  D0W=      15
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================

          ci-one-electron density block   1

                mo   1         mo   2         mo   3         mo   4         mo   5         mo   6         mo   7
   mo   1     1.99997317
   mo   2     0.00009778     1.99098064
   mo   3    -0.00004086     0.00343999     1.98033926
   mo   4    -0.00002321     0.00228193    -0.03258794     0.00626831
   mo   5    -0.00004263     0.00230172    -0.00624948     0.00564310     0.00655234
   mo   6    -0.00012018     0.00313766     0.00407193     0.00467022     0.00441449     0.01278225
   mo   7     0.00001191    -0.00075959    -0.00316174     0.00244170     0.00177954    -0.00215660     0.00379009

          ci-one-electron density block   2

                mo   8         mo   9         mo  10         mo  11
   mo   8     1.97768766
   mo   9    -0.01866928     0.00780092
   mo  10     0.01640323    -0.00607590     0.00655548
   mo  11     0.00103748     0.00739259    -0.00523857     0.00787652

          ci-one-electron density block   3

                mo  12         mo  13
   mo  12     1.98272640
   mo  13     0.00426893     0.01666697

          ci-one-electron density block   4


*****   symmetry block SYM1   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       1.99997317
       2       0.00009778     1.99098064
       3      -0.00004086     0.00343999     1.98033926
       4      -0.00002321     0.00228193    -0.03258794     0.00626831
       5      -0.00004263     0.00230172    -0.00624948     0.00564310
       6      -0.00012018     0.00313766     0.00407193     0.00467022
       7       0.00001191    -0.00075959    -0.00316174     0.00244170

               Column   5     Column   6     Column   7
       5       0.00655234
       6       0.00441449     0.01278225
       7       0.00177954    -0.00215660     0.00379009

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   1.9999742      1.9920312      1.9798698      0.0187278      0.0084937
   0.0014468      0.0001425

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7
  mo    1   0.999942E+00  -0.102446E-01   0.334982E-02   0.604864E-04  -0.235663E-04   0.289157E-05  -0.102193E-04
  mo    2   0.107765E-01   0.956190E+00  -0.292538E+00  -0.228613E-02   0.164616E-03   0.340752E-03  -0.271071E-03
  mo    3  -0.206013E-03   0.292535E+00   0.956103E+00   0.803077E-02   0.993443E-02   0.797201E-03   0.112146E-01
  mo    4   0.393508E-05  -0.369736E-02  -0.161335E-01   0.478374E+00   0.394369E+00   0.155038E+00   0.768974E+00
  mo    5  -0.837718E-05   0.181361E-03  -0.341342E-02   0.486302E+00   0.409327E+00  -0.673853E+00  -0.376659E+00
  mo    6  -0.438901E-04   0.211088E-02   0.146795E-02   0.730585E+00  -0.554960E+00   0.321271E+00  -0.234613E+00
  mo    7   0.223517E-05  -0.837227E-03  -0.144191E-02   0.290679E-01   0.607327E+00   0.647046E+00  -0.460041E+00
  eigenvectors of nos in ao-basis, column    1
  0.1000332E+01  0.1196654E-01 -0.3820818E-02 -0.4759443E-02  0.2959463E-02  0.3783466E-02 -0.1260622E-02
  eigenvectors of nos in ao-basis, column    2
 -0.2041866E-01  0.9173230E+00 -0.7526710E-03  0.7312178E-01  0.5668246E-01  0.1867035E+00 -0.1104994E+00
  eigenvectors of nos in ao-basis, column    3
  0.5374887E-02  0.4599807E-01  0.1310589E+00  0.8164193E+00  0.2402693E-01 -0.3831766E+00  0.1821607E+00
  eigenvectors of nos in ao-basis, column    4
 -0.1330045E-02  0.2197129E+00  0.2828631E+00 -0.1371897E+01  0.1152579E+01 -0.6563355E+00  0.2436013E+00
  eigenvectors of nos in ao-basis, column    5
  0.5038113E+00  0.2611174E+01 -0.2466392E+01  0.3515226E+00 -0.7854791E+00 -0.5874625E+00  0.3479858E+00
  eigenvectors of nos in ao-basis, column    6
  0.4750433E+00  0.1907035E+01 -0.3063134E+01 -0.2563911E+00  0.1265356E+01  0.2097756E+01 -0.1215286E+01
  eigenvectors of nos in ao-basis, column    7
 -0.4151414E+00 -0.1650192E+01  0.2831837E+01  0.3710632E+00 -0.7894535E+00  0.1578386E+00 -0.1078286E+01


*****   symmetry block SYM2   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       1.97768766
       2      -0.01866928     0.00780092
       3       0.01640323    -0.00607590     0.00655548
       4       0.00103748     0.00739259    -0.00523857     0.00787652

               eno   1        eno   2        eno   3        eno   4
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   1.9780024      0.0198091      0.0019063      0.0002028

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4
  mo    1   0.999920E+00   0.978936E-02  -0.580589E-02   0.553145E-02
  mo    2  -0.949905E-02   0.613798E+00   0.139252E+00   0.777026E+00
  mo    3   0.834777E-02  -0.508071E+00   0.822983E+00   0.253956E+00
  mo    4   0.468723E-03   0.604168E+00   0.550704E+00  -0.575939E+00
  eigenvectors of nos in ao-basis, column    1
  0.7363155E+00 -0.8534785E-01 -0.5616279E+00  0.2398270E+00
  eigenvectors of nos in ao-basis, column    2
 -0.1348856E+01  0.7484243E+00 -0.8451744E+00  0.2697068E+00
  eigenvectors of nos in ao-basis, column    3
 -0.5236479E+00  0.1687531E+01  0.1698531E+01 -0.1100328E+01
  eigenvectors of nos in ao-basis, column    4
  0.6397361E+00 -0.1473620E+01  0.3199570E+00 -0.1835237E+01


*****   symmetry block SYM3   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2
       1       1.98272640
       2       0.00426893     0.01666697

               eno   1        eno   2
 emo    1   0.100000E+01   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01

 occupation numbers of nos
   1.9827357      0.0166577

  eigenvectors of nos in mo-basis

                no   1         no   2
  mo    1   0.999998E+00  -0.217130E-02
  mo    2   0.217130E-02   0.999998E+00
  eigenvectors of nos in ao-basis, column    1
  0.9218680E+00  0.9512612E-01
  eigenvectors of nos in ao-basis, column    2
 -0.1409118E+01  0.1681192E+01


 total number of electrons =   10.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***


 cosurf_and_phi
  1.262739748330  2.872870034186  1.567866969040 -0.050158606426
  1.652947872482  0.360731428865  3.496571379926 -0.071441388004
  0.594940552015  0.670555958619  3.845537291721 -0.085597971137
  2.390078334690  1.202722009900  2.566708449184 -0.045252814527
  1.989334673621  2.229216060677  2.001028520754 -0.045295383537
  2.919143888229  0.378144896597  2.099775822683 -0.021093528187
  0.105380882100  1.878884201281  3.371423292662 -0.083438349562
  0.783266721831  2.590884380442  2.520834408126 -0.070860115142
  2.011402526412  2.551496219550  0.814855178005 -0.018105408606
  0.099626804209  1.855073908563 -1.945822518898  0.049765696757
  0.119020599620  3.173161356543  1.415159765853 -0.055809990990
  0.915237473358  3.108118624501  0.463299650838 -0.029629689116
  0.462814589486  2.837210699514 -0.795516582628  0.004205775428
  1.408719892640  1.640288870679  3.148120355694 -0.071666740566
  2.650053602975  1.633766196698  1.655441764425 -0.020356891826
  1.142904167226  2.885766749848 -0.243475252462 -0.005755674661
  3.656106873706  0.713798214804  0.361832640492  0.035590198203
  2.408046317685  2.075600470298 -1.226192756897  0.045528316587
  1.295820311657  0.567673501678 -2.747601655947  0.071190703161
  3.527730862121  1.045649613724 -1.064853177123  0.054459030973
  2.622898581638  1.024710819001 -2.241122746235  0.064359178424
  3.086808508398  1.794857685487 -0.179703829578  0.035081845330
  1.615872011596  1.674366500124 -2.147504385867  0.058796260065
  0.000006852884  1.035694667087 -2.361676372823  0.071391322515
  0.298815704890  0.969607820141 -2.408807386530  0.072371579655
  0.667723897920  1.245157743773 -2.338577856151  0.067727641446
  1.218123388571  2.025110412626 -1.616576841774  0.049017964979
  2.084186643139  2.293984793080 -0.480493513306  0.027534484281
  2.876642520254  1.658030225134  0.559035126479  0.019766532539
  3.282919570196  0.368088739237  1.091983758387  0.019756649188
  0.467932162408  1.694535407938 -1.941510815499  0.056913013652
  1.299770347024  2.453875604722 -0.850324284820  0.022430256457
  2.242050270258  2.245320705184  0.385739526123  0.004343703736
  2.923104801922  1.151131828431  1.279135167181  0.002463848253
  0.427044967836  0.580963354323 -2.585108185092  0.075288593060
 end_of_phi


 nsubv after electrostatic potential =  2

  Confirmation of dielectric energy for ground state
  edielnew =  -0.00359957658

 =========== Executing IN-CORE method ==========
 norm multnew:  0.00187421086
 threx: total number of loops= 14
 threx: total number of loops= 24
 blstr diagel,allext,nmin2=    72
 onel.diag.
 -0.337266E+01-0.422684E+01-0.519338E+01-0.429205E+01-0.316176E+01-0.409289E+01-0.515591E+01-0.533802E+01-0.330515E+02-0.786314E+01
 -0.694100E+01-0.664833E+01-0.712617E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1        72        72
 blstr diagel,allext,nmin2=    72
 onel.diag.
 -0.337266E+01-0.422684E+01-0.519338E+01-0.429205E+01-0.316176E+01-0.409289E+01-0.515591E+01-0.533802E+01-0.330515E+02-0.786314E+01
 -0.694100E+01-0.664833E+01-0.712617E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1        72        72
 blstr diagel,allext,nmin2=    72
 onel.diag.
 -0.337266E+01-0.422684E+01-0.519338E+01-0.429205E+01-0.316176E+01-0.409289E+01-0.515591E+01-0.533802E+01-0.330515E+02-0.786314E+01
 -0.694100E+01-0.664833E+01-0.712617E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1        72        72


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        12 xx:        36 ww:        79
One-external counts: yz :        25 yx:       118 yw:       166
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        39 yw:        51

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        11
task #   9:        28    task #  10:        71    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 matrix-vector product formed for vector   3
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference-subspace vector overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     1.00000000     0.00000000     0.00000000

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.98085597    -0.08270840     0.17629773
 subspace has dimension  3

          (h-repnuc*1) matrix in the subspace basis

                x:   1         x:   2         x:   3
   x:   1   -85.35158114
   x:   2    -0.16554915    -5.21721799
   x:   3    -0.00010214     0.06419092    -0.15573457

          overlap matrix in the subspace basis

                x:   1         x:   2         x:   3
   x:   1     1.00000000
   x:   2     0.00000000     0.06361575
   x:   3     0.00000000    -0.00082136     0.00187421

          eigenvectors and eigenvalues in the subspace basis

                v:   1         v:   2         v:   3

   energy   -85.48070708   -83.19695378   -81.82883223

   x:   1     0.98085597    -0.08270840     0.17629773
   x:   2     0.76440518     1.06248310    -3.75441652
   x:   3     1.05077810    22.64236826     4.77630745

          eigenvectors and eigenvalues in the schmidt orthonormalized basis

                v:   1         v:   2         v:   3

   energy   -85.48070708   -83.19695378   -81.82883223

  zx:   1     0.98085597    -0.08270840     0.17629773
  zx:   2     0.18937766     0.19424636    -0.96249907
  zx:   3     0.04536157     0.97745981     0.20619082

          <ref|baseci> overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     1.00000000     0.00000000     0.00000000

  write the transformed vectors back to cosmofile

 rtflw =  0
 freezing =  F

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1    -76.1361345948 -9.3398E+00  2.2814E-04  2.2981E-02  1.0000E-03
 mr-sdci #  3  2    -73.8523812988 -8.0347E+00  0.0000E+00  1.2628E+00  1.0000E-04
 mr-sdci #  3  3    -72.4842597395  7.2484E+01  0.0000E+00  1.1195E+00  1.0000E-04


 total energy in cosmo calc 
e(rootcalc) + repnuc=     -76.1361345948
dielectric energy =      -0.0135735826
deltaediel =       0.0135735826
e(rootcalc) + repnuc - ediel =     -76.1225610122
e(rootcalc) + repnuc - edielnew =     -76.1361345948
deltaelast =      76.1225610122

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.02   0.00   0.00   0.01         0.    0.0000
    2   25    0   0.01   0.00   0.00   0.01         0.    0.0000
    3   26    0   0.01   0.00   0.00   0.01         0.    0.0000
    4   11    0   0.01   0.00   0.00   0.01         0.    0.0000
    5   15    0   0.01   0.00   0.00   0.01         0.    0.0000
    6   16    0   0.02   0.00   0.00   0.01         0.    0.0000
    7    1    0   0.01   0.00   0.00   0.01         0.    0.0000
    8    5    0   0.02   0.00   0.00   0.02         0.    0.0000
    9    6    0   0.02   0.00   0.00   0.02         0.    0.0000
   10    7    0   0.01   0.00   0.00   0.01         0.    0.0000
   11   75    0   0.01   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.01   0.00   0.00   0.01         0.    0.0000
   14   47    0   0.00   0.00   0.00   0.00         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1600s 
time spent in multnx:                   0.1300s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0200s 
total time per CI iteration:            0.6800s 

          starting ci iteration   4

 cosmocalc =  1
 rootcalc =  1
 repnuc =   9.34457249

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=       3  D0X=       9  D0W=      15
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================

          ci-one-electron density block   1

                mo   1         mo   2         mo   3         mo   4         mo   5         mo   6         mo   7
   mo   1     1.99995510
   mo   2    -0.00000731     1.98768960
   mo   3    -0.00004709     0.00430667     1.97961163
   mo   4     0.00014631    -0.00325098    -0.02193448     0.00719520
   mo   5     0.00010368     0.00277387    -0.00878695     0.00756870     0.00977848
   mo   6    -0.00032795     0.01249375     0.00773617     0.00349917     0.00469153     0.01214415
   mo   7     0.00036638    -0.00041853    -0.01293580     0.00389943     0.00278384    -0.00219529     0.00496066

          ci-one-electron density block   2

                mo   8         mo   9         mo  10         mo  11
   mo   8     1.97419232
   mo   9    -0.00812813     0.00757764
   mo  10     0.01867676    -0.00710158     0.00933855
   mo  11    -0.01336207     0.00751335    -0.00620027     0.00855207

          ci-one-electron density block   3

                mo  12         mo  13
   mo  12     1.98374989
   mo  13     0.01939384     0.01525471

          ci-one-electron density block   4


*****   symmetry block SYM1   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       1.99995510
       2      -0.00000731     1.98768960
       3      -0.00004709     0.00430667     1.97961163
       4       0.00014631    -0.00325098    -0.02193448     0.00719520
       5       0.00010368     0.00277387    -0.00878695     0.00756870
       6      -0.00032795     0.01249375     0.00773617     0.00349917
       7       0.00036638    -0.00041853    -0.01293580     0.00389943

               Column   5     Column   6     Column   7
       5       0.00977848
       6       0.00469153     0.01214415
       7       0.00278384    -0.00219529     0.00496066

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   1.9999554      1.9897488      1.9780395      0.0206291      0.0107822
   0.0020255      0.0001543

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7
  mo    1   0.999993E+00   0.297750E-02   0.202145E-02  -0.150824E-04   0.258405E-03  -0.396885E-04   0.220041E-04
  mo    2  -0.188474E-02   0.911924E+00  -0.410300E+00  -0.337524E-02  -0.494022E-02  -0.110049E-02   0.273914E-02
  mo    3  -0.306186E-02   0.410227E+00   0.911867E+00   0.782607E-02  -0.108652E-01   0.191312E-02   0.445464E-02
  mo    4   0.110504E-03  -0.602859E-02  -0.949968E-02   0.533276E+00  -0.293918E+00   0.162278E+00   0.776381E+00
  mo    5   0.632482E-04  -0.552492E-03  -0.469161E-02   0.636966E+00  -0.226739E+00  -0.623101E+00  -0.393175E+00
  mo    6  -0.188621E-03   0.735669E-02   0.958982E-03   0.531584E+00   0.740545E+00   0.377113E+00  -0.163534E+00
  mo    7   0.204410E-03  -0.288614E-02  -0.591739E-02   0.165037E+00  -0.560047E+00   0.665727E+00  -0.464623E+00
  eigenvectors of nos in ao-basis, column    1
  0.1000675E+01  0.9138837E-03 -0.4520936E-02 -0.4839760E-02  0.2131951E-02  0.9476973E-03  0.4089259E-03
  eigenvectors of nos in ao-basis, column    2
 -0.8515770E-02  0.9075045E+00  0.2391266E-01  0.1685970E+00  0.6754793E-01  0.1385424E+00 -0.8759176E-01
  eigenvectors of nos in ao-basis, column    3
  0.1279705E-02 -0.8564591E-01  0.1571310E+00  0.8008147E+00  0.1226897E-01 -0.4058604E+00  0.1884342E+00
  eigenvectors of nos in ao-basis, column    4
  0.1177503E+00  0.8489571E+00 -0.2855863E+00 -0.1212442E+01  0.8238087E+00 -0.9403092E+00  0.4460250E+00
  eigenvectors of nos in ao-basis, column    5
 -0.4683284E+00 -0.2390013E+01  0.2336740E+01 -0.7313452E+00  0.1092322E+01  0.3387441E+00 -0.1848466E+00
  eigenvectors of nos in ao-basis, column    6
  0.4901066E+00  0.1984231E+01 -0.3143594E+01 -0.3616660E+00  0.1366247E+01  0.2037178E+01 -0.1154936E+01
  eigenvectors of nos in ao-basis, column    7
 -0.4220418E+00 -0.1693870E+01  0.2851842E+01  0.2748926E+00 -0.6560498E+00  0.2239185E+00 -0.1120397E+01


*****   symmetry block SYM2   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       1.97419232
       2      -0.00812813     0.00757764
       3       0.01867676    -0.00710158     0.00933855
       4      -0.01336207     0.00751335    -0.00620027     0.00855207

               eno   1        eno   2        eno   3        eno   4
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   1.9744960      0.0220914      0.0028196      0.0002535

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4
  mo    1   0.999922E+00  -0.118969E-01  -0.239043E-02  -0.285583E-02
  mo    2  -0.419268E-02  -0.575467E+00   0.149710E+00   0.803994E+00
  mo    3   0.953995E-02   0.582209E+00   0.765325E+00   0.274262E+00
  mo    4  -0.684235E-02  -0.574220E+00   0.625987E+00  -0.527604E+00
  eigenvectors of nos in ao-basis, column    1
  0.7453996E+00 -0.1032848E+00 -0.5612659E+00  0.2260280E+00
  eigenvectors of nos in ao-basis, column    2
  0.1318866E+01 -0.6517002E+00  0.9936489E+00 -0.4177947E+00
  eigenvectors of nos in ao-basis, column    3
 -0.6606121E+00  0.1813861E+01  0.1604574E+01 -0.9812199E+00
  eigenvectors of nos in ao-basis, column    4
  0.5600069E+00 -0.1364283E+01  0.3743134E+00 -0.1876338E+01


*****   symmetry block SYM3   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2
       1       1.98374989
       2       0.01939384     0.01525471

               eno   1        eno   2
 emo    1   0.100000E+01   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01

 occupation numbers of nos
   1.9839409      0.0150637

  eigenvectors of nos in mo-basis

                no   1         no   2
  mo    1   0.999951E+00  -0.985068E-02
  mo    2   0.985068E-02   0.999951E+00
  eigenvectors of nos in ao-basis, column    1
  0.9110196E+00  0.1080340E+00
  eigenvectors of nos in ao-basis, column    2
 -0.1416156E+01  0.1680412E+01


 total number of electrons =   10.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***


 cosurf_and_phi
  1.262739748330  2.872870034186  1.567866969040 -0.050049728902
  1.652947872482  0.360731428865  3.496571379926 -0.070685056332
  0.594940552015  0.670555958619  3.845537291721 -0.085023780144
  2.390078334690  1.202722009900  2.566708449184 -0.044445545365
  1.989334673621  2.229216060677  2.001028520754 -0.044850697548
  2.919143888229  0.378144896597  2.099775822683 -0.020114754361
  0.105380882100  1.878884201281  3.371423292662 -0.083115713793
  0.783266721831  2.590884380442  2.520834408126 -0.070714603179
  2.011402526412  2.551496219550  0.814855178005 -0.017837602617
  0.099626804209  1.855073908563 -1.945822518898  0.050058382316
  0.119020599620  3.173161356543  1.415159765853 -0.055887359426
  0.915237473358  3.108118624501  0.463299650838 -0.029619558656
  0.462814589486  2.837210699514 -0.795516582628  0.004322415418
  1.408719892640  1.640288870679  3.148120355694 -0.071152171410
  2.650053602975  1.633766196698  1.655441764425 -0.019667662559
  1.142904167226  2.885766749848 -0.243475252462 -0.005674841598
  3.656106873706  0.713798214804  0.361832640492  0.035586384962
  2.408046317685  2.075600470298 -1.226192756897  0.045225588576
  1.295820311657  0.567673501678 -2.747601655947  0.070895223341
  3.527730862121  1.045649613724 -1.064853177123  0.053880463204
  2.622898581638  1.024710819001 -2.241122746235  0.063701388455
  3.086808508398  1.794857685487 -0.179703829578  0.034964337715
  1.615872011596  1.674366500124 -2.147504385867  0.058523422036
  0.000006852884  1.035694667087 -2.361676372823  0.071707833254
  0.298815704890  0.969607820141 -2.408807386530  0.072628663063
  0.667723897920  1.245157743773 -2.338577856151  0.067845390604
  1.218123388571  2.025110412626 -1.616576841774  0.049057661263
  2.084186643139  2.293984793080 -0.480493513306  0.027569874916
  2.876642520254  1.658030225134  0.559035126479  0.020110024770
  3.282919570196  0.368088739237  1.091983758387  0.020396557136
  0.467932162408  1.694535407938 -1.941510815499  0.057223606520
  1.299770347024  2.453875604722 -0.850324284820  0.022564359075
  2.242050270258  2.245320705184  0.385739526123  0.004647161832
  2.923104801922  1.151131828431  1.279135167181  0.003249052500
  0.427044967836  0.580963354323 -2.585108185092  0.075437296621
 end_of_phi


 nsubv after electrostatic potential =  3

  Confirmation of dielectric energy for ground state
  edielnew =  -0.00358148889

 =========== Executing IN-CORE method ==========
 norm multnew:  0.000150137045
 threx: total number of loops= 14
 threx: total number of loops= 24
 blstr diagel,allext,nmin2=    72
 onel.diag.
 -0.337266E+01-0.422684E+01-0.519338E+01-0.429205E+01-0.316176E+01-0.409289E+01-0.515591E+01-0.533802E+01-0.330515E+02-0.786314E+01
 -0.694100E+01-0.664833E+01-0.712617E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1        72        72
 blstr diagel,allext,nmin2=    72
 onel.diag.
 -0.337266E+01-0.422684E+01-0.519338E+01-0.429205E+01-0.316176E+01-0.409289E+01-0.515591E+01-0.533802E+01-0.330515E+02-0.786314E+01
 -0.694100E+01-0.664833E+01-0.712617E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1        72        72
 blstr diagel,allext,nmin2=    72
 onel.diag.
 -0.337266E+01-0.422684E+01-0.519338E+01-0.429205E+01-0.316176E+01-0.409289E+01-0.515591E+01-0.533802E+01-0.330515E+02-0.786314E+01
 -0.694100E+01-0.664833E+01-0.712617E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1        72        72


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        12 xx:        36 ww:        79
One-external counts: yz :        25 yx:       118 yw:       166
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        39 yw:        51

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        11
task #   9:        28    task #  10:        71    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 matrix-vector product formed for vector   4
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference-subspace vector overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     1.00000000     0.00000000     0.00000000     0.00000000

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.98047676    -0.07383924     0.07025390     0.16815910
 subspace has dimension  4

          (h-repnuc*1) matrix in the subspace basis

                x:   1         x:   2         x:   3         x:   4
   x:   1   -85.35158114
   x:   2    -0.16554915    -5.21721799
   x:   3    -0.00010214     0.06419092    -0.15573457
   x:   4     0.00005708    -0.03785906     0.00813746    -0.01255983

          overlap matrix in the subspace basis

                x:   1         x:   2         x:   3         x:   4
   x:   1     1.00000000
   x:   2     0.00000000     0.06361575
   x:   3     0.00000000    -0.00082136     0.00187421
   x:   4     0.00000000     0.00044202    -0.00009773     0.00015014

          eigenvectors and eigenvalues in the subspace basis

                v:   1         v:   2         v:   3         v:   4

   energy   -85.48090488   -83.73162336   -83.18542626   -81.64546226

   x:   1     0.98047676    -0.07383924     0.07025390     0.16815910
   x:   2     0.76555308     0.74622292    -0.90166975    -3.75930076
   x:   3     1.09414594     5.84734456   -22.11055977     5.42540874
   x:   4     0.86699286    79.14508239    11.42097317    24.92626500

          eigenvectors and eigenvalues in the schmidt orthonormalized basis

                v:   1         v:   2         v:   3         v:   4

   energy   -85.48090488   -83.73162336   -83.18542626   -81.64546226

  zx:   1     0.98047676    -0.07383924     0.07025390     0.16815910
  zx:   2     0.19104538     0.30787530    -0.13540206    -0.92216091
  zx:   3     0.04538569     0.08372438    -0.97884640     0.18108034
  zx:   4     0.01035039     0.94485491     0.13634660     0.29757634

          <ref|baseci> overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     1.00000000     0.00000000     0.00000000     0.00000000

  write the transformed vectors back to cosmofile

 rtflw =  0
 freezing =  F

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1    -76.1363323990 -9.3444E+00  1.6450E-05  6.7784E-03  1.0000E-03
 mr-sdci #  4  2    -74.3870508751 -8.8099E+00  0.0000E+00  9.8151E-01  1.0000E-04
 mr-sdci #  4  3    -73.8408537736 -7.9880E+00  0.0000E+00  1.3058E+00  1.0000E-04
 mr-sdci #  4  4    -72.3008897783  7.2301E+01  0.0000E+00  1.0738E+00  1.0000E-04


 total energy in cosmo calc 
e(rootcalc) + repnuc=     -76.1363323990
dielectric energy =      -0.0135735826
deltaediel =       0.0135735826
e(rootcalc) + repnuc - ediel =     -76.1227588165
e(rootcalc) + repnuc - edielnew =     -76.1363323990
deltaelast =      76.1227588165

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.02   0.00   0.00   0.01         0.    0.0000
    2   25    0   0.01   0.00   0.00   0.01         0.    0.0000
    3   26    0   0.01   0.00   0.00   0.01         0.    0.0000
    4   11    0   0.01   0.00   0.00   0.01         0.    0.0000
    5   15    0   0.02   0.00   0.00   0.01         0.    0.0000
    6   16    0   0.01   0.00   0.00   0.01         0.    0.0000
    7    1    0   0.01   0.00   0.00   0.01         0.    0.0000
    8    5    0   0.02   0.00   0.00   0.02         0.    0.0000
    9    6    0   0.02   0.00   0.00   0.02         0.    0.0000
   10    7    0   0.01   0.00   0.00   0.01         0.    0.0000
   11   75    0   0.01   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.00   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.01   0.00   0.00   0.01         0.    0.0000
   14   47    0   0.00   0.00   0.00   0.00         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1600s 
time spent in multnx:                   0.1300s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0200s 
total time per CI iteration:            0.6700s 

          starting ci iteration   5

 cosmocalc =  1
 rootcalc =  1
 repnuc =   9.34457249

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=       3  D0X=       9  D0W=      15
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================

          ci-one-electron density block   1

                mo   1         mo   2         mo   3         mo   4         mo   5         mo   6         mo   7
   mo   1     1.99995694
   mo   2    -0.00001508     1.98746025
   mo   3    -0.00004264     0.00468118     1.97915865
   mo   4     0.00011595    -0.00475167    -0.03018087     0.00799730
   mo   5     0.00007208     0.00258927    -0.00555535     0.00791317     0.00982115
   mo   6    -0.00033697     0.01267740     0.00625884     0.00388563     0.00471206     0.01204502
   mo   7     0.00033897     0.00030163    -0.01270125     0.00396419     0.00283875    -0.00222215     0.00492169

          ci-one-electron density block   2

                mo   8         mo   9         mo  10         mo  11
   mo   8     1.97360612
   mo   9    -0.01156118     0.00818644
   mo  10     0.01704626    -0.00747620     0.00941478
   mo  11    -0.01429132     0.00777741    -0.00631631     0.00841741

          ci-one-electron density block   3

                mo  12         mo  13
   mo  12     1.98373227
   mo  13     0.01826892     0.01528198

          ci-one-electron density block   4


*****   symmetry block SYM1   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       1.99995694
       2      -0.00001508     1.98746025
       3      -0.00004264     0.00468118     1.97915865
       4       0.00011595    -0.00475167    -0.03018087     0.00799730
       5       0.00007208     0.00258927    -0.00555535     0.00791317
       6      -0.00033697     0.01267740     0.00625884     0.00388563
       7       0.00033897     0.00030163    -0.01270125     0.00396419

               Column   5     Column   6     Column   7
       5       0.00982115
       6       0.00471206     0.01204502
       7       0.00283875    -0.00222215     0.00492169

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   1.9999573      1.9898247      1.9774701      0.0212943      0.0107022
   0.0019329      0.0001796

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7
  mo    1   0.999992E+00   0.357199E-02   0.159252E-02   0.436956E-05   0.246203E-03  -0.365045E-04   0.179075E-04
  mo    2  -0.254371E-02   0.903497E+00  -0.428530E+00  -0.287242E-02  -0.504925E-02  -0.122023E-02   0.361892E-02
  mo    3  -0.296642E-02   0.428423E+00   0.903410E+00   0.976762E-02  -0.108291E-01   0.398911E-02   0.812621E-02
  mo    4   0.109383E-03  -0.868185E-02  -0.128347E-01   0.554585E+00  -0.274422E+00   0.177793E+00   0.765035E+00
  mo    5   0.414337E-04  -0.418889E-04  -0.317443E-02   0.630495E+00  -0.223721E+00  -0.632474E+00  -0.390373E+00
  mo    6  -0.194969E-03   0.713231E-02   0.862051E-04   0.517031E+00   0.747523E+00   0.369826E+00  -0.192527E+00
  mo    7   0.188899E-03  -0.262892E-02  -0.591278E-02   0.165793E+00  -0.561879E+00   0.656946E+00  -0.474537E+00
  eigenvectors of nos in ao-basis, column    1
  0.1000668E+01  0.3096873E-03 -0.4409841E-02 -0.4631904E-02  0.2089177E-02  0.7515566E-03  0.4974357E-03
  eigenvectors of nos in ao-basis, column    2
 -0.7481726E-02  0.9069266E+00  0.2323107E-01  0.1852921E+00  0.6783253E-01  0.1296002E+00 -0.8099736E-01
  eigenvectors of nos in ao-basis, column    3
  0.1287201E-02 -0.1036262E+00  0.1541320E+00  0.7984843E+00  0.9623555E-02 -0.4116834E+00  0.1951505E+00
  eigenvectors of nos in ao-basis, column    4
  0.1175339E+00  0.8569404E+00 -0.2703111E+00 -0.1194122E+01  0.7956690E+00 -0.9359775E+00  0.4224303E+00
  eigenvectors of nos in ao-basis, column    5
 -0.4708275E+00 -0.2394049E+01  0.2362195E+01 -0.7475793E+00  0.1099746E+01  0.3324529E+00 -0.1962152E+00
  eigenvectors of nos in ao-basis, column    6
  0.4821413E+00  0.1953053E+01 -0.3088636E+01 -0.3484060E+00  0.1346533E+01  0.2042562E+01 -0.1178706E+01
  eigenvectors of nos in ao-basis, column    7
 -0.4284851E+00 -0.1719497E+01  0.2892294E+01  0.3207520E+00 -0.7163789E+00  0.1953478E+00 -0.1102049E+01


*****   symmetry block SYM2   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       1.97360612
       2      -0.01156118     0.00818644
       3       0.01704626    -0.00747620     0.00941478
       4      -0.01429132     0.00777741    -0.00631631     0.00841741

               eno   1        eno   2        eno   3        eno   4
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   1.9739282      0.0227460      0.0026863      0.0002642

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4
  mo    1   0.999917E+00  -0.126812E-01  -0.117959E-02  -0.167612E-02
  mo    2  -0.594299E-02  -0.588530E+00   0.164496E+00   0.791542E+00
  mo    3   0.872253E-02   0.580420E+00   0.767451E+00   0.272131E+00
  mo    4  -0.732199E-02  -0.562659E+00   0.619644E+00  -0.547178E+00
  eigenvectors of nos in ao-basis, column    1
  0.7464750E+00 -0.1040846E+00 -0.5628850E+00  0.2294423E+00
  eigenvectors of nos in ao-basis, column    2
  0.1305891E+01 -0.6210989E+00  0.9938427E+00 -0.3896905E+00
  eigenvectors of nos in ao-basis, column    3
 -0.6555694E+00  0.1794295E+01  0.1605981E+01 -0.1008966E+01
  eigenvectors of nos in ao-basis, column    4
  0.5938826E+00 -0.1403799E+01  0.3652249E+00 -0.1867233E+01


*****   symmetry block SYM3   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2
       1       1.98373227
       2       0.01826892     0.01528198

               eno   1        eno   2
 emo    1   0.100000E+01   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01

 occupation numbers of nos
   1.9839018      0.0151124

  eigenvectors of nos in mo-basis

                no   1         no   2
  mo    1   0.999957E+00  -0.927966E-02
  mo    2   0.927966E-02   0.999957E+00
  eigenvectors of nos in ao-basis, column    1
  0.9118281E+00  0.1070744E+00
  eigenvectors of nos in ao-basis, column    2
 -0.1415636E+01  0.1680473E+01


 total number of electrons =   10.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***


 cosurf_and_phi
  1.262739748330  2.872870034186  1.567866969040 -0.050254459960
  1.652947872482  0.360731428865  3.496571379926 -0.071043910873
  0.594940552015  0.670555958619  3.845537291721 -0.085401227891
  2.390078334690  1.202722009900  2.566708449184 -0.044731961405
  1.989334673621  2.229216060677  2.001028520754 -0.045090142657
  2.919143888229  0.378144896597  2.099775822683 -0.020346874436
  0.105380882100  1.878884201281  3.371423292662 -0.083468776383
  0.783266721831  2.590884380442  2.520834408126 -0.071009883323
  2.011402526412  2.551496219550  0.814855178005 -0.017940433957
  0.099626804209  1.855073908563 -1.945822518898  0.050387640555
  0.119020599620  3.173161356543  1.415159765853 -0.056082938782
  0.915237473358  3.108118624501  0.463299650838 -0.029694959347
  0.462814589486  2.837210699514 -0.795516582628  0.004437362035
  1.408719892640  1.640288870679  3.148120355694 -0.071490072482
  2.650053602975  1.633766196698  1.655441764425 -0.019857642666
  1.142904167226  2.885766749848 -0.243475252462 -0.005643293389
  3.656106873706  0.713798214804  0.361832640492  0.035604862280
  2.408046317685  2.075600470298 -1.226192756897  0.045392504147
  1.295820311657  0.567673501678 -2.747601655947  0.071245992466
  3.527730862121  1.045649613724 -1.064853177123  0.054023555099
  2.622898581638  1.024710819001 -2.241122746235  0.063942129779
  3.086808508398  1.794857685487 -0.179703829578  0.035026126580
  1.615872011596  1.674366500124 -2.147504385867  0.058803160517
  0.000006852884  1.035694667087 -2.361676372823  0.072119567531
  0.298815704890  0.969607820141 -2.408807386530  0.073038949887
  0.667723897920  1.245157743773 -2.338577856151  0.068220366320
  1.218123388571  2.025110412626 -1.616576841774  0.049312277920
  2.084186643139  2.293984793080 -0.480493513306  0.027652845321
  2.876642520254  1.658030225134  0.559035126479  0.020067568481
  3.282919570196  0.368088739237  1.091983758387  0.020298751379
  0.467932162408  1.694535407938 -1.941510815499  0.057563223675
  1.299770347024  2.453875604722 -0.850324284820  0.022700223238
  2.242050270258  2.245320705184  0.385739526123  0.004603295798
  2.923104801922  1.151131828431  1.279135167181  0.003102018126
  0.427044967836  0.580963354323 -2.585108185092  0.075855489948
 end_of_phi


 nsubv after electrostatic potential =  4

  Confirmation of dielectric energy for ground state
  edielnew =  -0.0035961119

 =========== Executing IN-CORE method ==========
 norm multnew:  9.26561693E-06
 threx: total number of loops= 14
 threx: total number of loops= 24
 blstr diagel,allext,nmin2=    72
 onel.diag.
 -0.337266E+01-0.422684E+01-0.519338E+01-0.429205E+01-0.316176E+01-0.409289E+01-0.515591E+01-0.533802E+01-0.330515E+02-0.786314E+01
 -0.694100E+01-0.664833E+01-0.712617E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1        72        72
 blstr diagel,allext,nmin2=    72
 onel.diag.
 -0.337266E+01-0.422684E+01-0.519338E+01-0.429205E+01-0.316176E+01-0.409289E+01-0.515591E+01-0.533802E+01-0.330515E+02-0.786314E+01
 -0.694100E+01-0.664833E+01-0.712617E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1        72        72
 blstr diagel,allext,nmin2=    72
 onel.diag.
 -0.337266E+01-0.422684E+01-0.519338E+01-0.429205E+01-0.316176E+01-0.409289E+01-0.515591E+01-0.533802E+01-0.330515E+02-0.786314E+01
 -0.694100E+01-0.664833E+01-0.712617E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1        72        72


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        12 xx:        36 ww:        79
One-external counts: yz :        25 yx:       118 yw:       166
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        39 yw:        51

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        11
task #   9:        28    task #  10:        71    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 matrix-vector product formed for vector   5
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference-subspace vector overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     1.00000000     0.00000000     0.00000000     0.00000000     0.00000000

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1    -0.98046676     0.07296195     0.03764966     0.06768162    -0.16541823
 subspace has dimension  5

          (h-repnuc*1) matrix in the subspace basis

                x:   1         x:   2         x:   3         x:   4         x:   5
   x:   1   -85.35158114
   x:   2    -0.16554915    -5.21721799
   x:   3    -0.00010214     0.06419092    -0.15573457
   x:   4     0.00005708    -0.03785906     0.00813746    -0.01255983
   x:   5    -0.00000663     0.00439237    -0.00169912     0.00099809    -0.00077310

          overlap matrix in the subspace basis

                x:   1         x:   2         x:   3         x:   4         x:   5
   x:   1     1.00000000
   x:   2     0.00000000     0.06361575
   x:   3     0.00000000    -0.00082136     0.00187421
   x:   4     0.00000000     0.00044202    -0.00009773     0.00015014
   x:   5     0.00000000    -0.00005128     0.00001988    -0.00001190     0.00000927

          eigenvectors and eigenvalues in the subspace basis

                v:   1         v:   2         v:   3         v:   4         v:   5

   energy   -85.48092029   -83.73176014   -83.59587348   -82.89790566   -81.62452550

   x:   1    -0.98046676     0.07296195     0.03764966     0.06768162    -0.16541823
   x:   2    -0.76562016    -0.73793187    -0.38633392    -0.99138755     3.71893786
   x:   3    -1.09752376    -5.53403463   -13.00188164   -17.87770336    -6.20969451
   x:   4    -0.93448519   -78.57620487   -16.55548589    27.85686690   -21.48948315
   x:   5    -0.93625392     8.90240456  -265.60652150   221.74133863    39.74967073

          eigenvectors and eigenvalues in the schmidt orthonormalized basis

                v:   1         v:   2         v:   3         v:   4         v:   5

   energy   -85.48092029   -83.73176014   -83.59587348   -82.89790566   -81.62452550

  zx:   1    -0.98046676     0.07296195     0.03764966     0.06768162    -0.16541823
  zx:   2    -0.19097922    -0.30761749    -0.03011126    -0.18809583     0.91247558
  zx:   3    -0.04580434    -0.06744937    -0.64420854    -0.73246037    -0.20457200
  zx:   4    -0.01032537    -0.94596282     0.03803548     0.13580606    -0.29181800
  zx:   5    -0.00268737     0.02555292    -0.76238079     0.63647284     0.11409504

          <ref|baseci> overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     1.00000000     0.00000000     0.00000000     0.00000000     0.00000000

  write the transformed vectors back to cosmofile

 rtflw =  0
 freezing =  F

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1    -76.1363478003 -9.3446E+00  5.6180E-07  1.1976E-03  1.0000E-03
 mr-sdci #  5  2    -74.3871876575 -9.3444E+00  0.0000E+00  9.9432E-01  1.0000E-04
 mr-sdci #  5  3    -74.2513009977 -8.9341E+00  0.0000E+00  9.1747E-01  1.0000E-04
 mr-sdci #  5  4    -73.5533331782 -8.0921E+00  0.0000E+00  1.7810E+00  1.0000E-04
 mr-sdci #  5  5    -72.2799530171  7.2280E+01  0.0000E+00  9.5960E-01  1.0000E-04


 total energy in cosmo calc 
e(rootcalc) + repnuc=     -76.1363478003
dielectric energy =      -0.0135735826
deltaediel =       0.0135735826
e(rootcalc) + repnuc - ediel =     -76.1227742178
e(rootcalc) + repnuc - edielnew =     -76.1363478003
deltaelast =      76.1227742178

 root number  1 is used to define the new expansion vector.
======================== TIMING STATISTICS PER TASK    ========================
task# type node  tmult  tloop   tint  tmnx    MFLIPS     mloop 
    1   24    0   0.01   0.00   0.00   0.01         0.    0.0000
    2   25    0   0.01   0.00   0.00   0.01         0.    0.0000
    3   26    0   0.01   0.00   0.00   0.00         0.    0.0000
    4   11    0   0.04   0.00   0.00   0.01         0.    0.0000
    5   15    0   0.01   0.00   0.00   0.01         0.    0.0000
    6   16    0   0.02   0.00   0.00   0.02         0.    0.0000
    7    1    0   0.01   0.00   0.00   0.01         0.    0.0000
    8    5    0   0.02   0.00   0.00   0.02         0.    0.0000
    9    6    0   0.03   0.00   0.00   0.02         0.    0.0000
   10    7    0   0.02   0.00   0.00   0.02         0.    0.0000
   11   75    0   0.00   0.00   0.00   0.00         0.    0.0000
   12   45    0   0.01   0.00   0.00   0.00         0.    0.0000
   13   46    0   0.00   0.00   0.00   0.00         0.    0.0000
   14   47    0   0.01   0.00   0.00   0.00         0.    0.0000
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.030000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.2000s 
time spent in multnx:                   0.1300s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0300s 
total time per CI iteration:            0.7400s 

          starting ci iteration   6

 cosmocalc =  1
 rootcalc =  1
 repnuc =   9.34457249

 **************************************************
 ***Calc of density matrix for cosmo calculation***
 **************************************************

 entering drivercid: firstcall,firstnonref= F T
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=       3  D0X=       9  D0W=      15
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================

          ci-one-electron density block   1

                mo   1         mo   2         mo   3         mo   4         mo   5         mo   6         mo   7
   mo   1     1.99995830
   mo   2    -0.00001157     1.98745402
   mo   3    -0.00003971     0.00475596     1.97918076
   mo   4     0.00009417    -0.00493772    -0.02864254     0.00798582
   mo   5     0.00005558     0.00190006    -0.00527573     0.00793586     0.00979826
   mo   6    -0.00032561     0.01237968     0.00599860     0.00386396     0.00481493     0.01210626
   mo   7     0.00031497     0.00034342    -0.01281619     0.00398275     0.00282484    -0.00220929     0.00487126

          ci-one-electron density block   2

                mo   8         mo   9         mo  10         mo  11
   mo   8     1.97353897
   mo   9    -0.01131775     0.00824236
   mo  10     0.01639332    -0.00753395     0.00938167
   mo  11    -0.01408177     0.00783321    -0.00637747     0.00845004

          ci-one-electron density block   3

                mo  12         mo  13
   mo  12     1.98366017
   mo  13     0.01792798     0.01537213

          ci-one-electron density block   4


*****   symmetry block SYM1   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       1.99995830
       2      -0.00001157     1.98745402
       3      -0.00003971     0.00475596     1.97918076
       4       0.00009417    -0.00493772    -0.02864254     0.00798582
       5       0.00005558     0.00190006    -0.00527573     0.00793586
       6      -0.00032561     0.01237968     0.00599860     0.00386396
       7       0.00031497     0.00034342    -0.01281619     0.00398275

               Column   5     Column   6     Column   7
       5       0.00979826
       6       0.00481493     0.01210626
       7       0.00282484    -0.00220929     0.00487126

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   1.9999586      1.9898682      1.9773905      0.0213904      0.0107015
   0.0018745      0.0001710

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7
  mo    1   0.999994E+00   0.312022E-02   0.150688E-02   0.156532E-04   0.230204E-03  -0.333361E-04   0.202862E-04
  mo    2  -0.216351E-02   0.902100E+00  -0.431468E+00  -0.254741E-02  -0.504582E-02  -0.141457E-02   0.348363E-02
  mo    3  -0.270294E-02   0.431368E+00   0.902022E+00   0.927119E-02  -0.106340E-01   0.395494E-02   0.746791E-02
  mo    4   0.916242E-04  -0.847461E-02  -0.120606E-01   0.552770E+00  -0.284032E+00   0.173129E+00   0.763924E+00
  mo    5   0.331929E-04  -0.304486E-03  -0.289240E-02   0.629877E+00  -0.220616E+00  -0.631472E+00  -0.394739E+00
  mo    6  -0.185365E-03   0.694014E-02   0.109833E-04   0.520250E+00   0.745909E+00   0.373102E+00  -0.183595E+00
  mo    7   0.175297E-03  -0.265374E-02  -0.596415E-02   0.164159E+00  -0.560478E+00   0.657303E+00  -0.476262E+00
  eigenvectors of nos in ao-basis, column    1
  0.1000655E+01  0.6650883E-03 -0.4343088E-02 -0.4486384E-02  0.2128599E-02  0.7893694E-03  0.4730049E-03
  eigenvectors of nos in ao-basis, column    2
 -0.7955653E-02  0.9065325E+00  0.2395903E-01  0.1882135E+00  0.6753627E-01  0.1286488E+00 -0.8089713E-01
  eigenvectors of nos in ao-basis, column    3
  0.1167193E-02 -0.1065126E+00  0.1550263E+00  0.7976149E+00  0.9045495E-02 -0.4128287E+00  0.1952740E+00
  eigenvectors of nos in ao-basis, column    4
  0.1161902E+00  0.8496809E+00 -0.2644699E+00 -0.1197876E+01  0.8009839E+00 -0.9342278E+00  0.4224106E+00
  eigenvectors of nos in ao-basis, column    5
 -0.4690809E+00 -0.2388422E+01  0.2347743E+01 -0.7442378E+00  0.1098419E+01  0.3274767E+00 -0.1840676E+00
  eigenvectors of nos in ao-basis, column    6
  0.4825970E+00  0.1952994E+01 -0.3094477E+01 -0.3520957E+00  0.1353388E+01  0.2042824E+01 -0.1174142E+01
  eigenvectors of nos in ao-basis, column    7
 -0.4302726E+00 -0.1730985E+01  0.2898298E+01  0.3108051E+00 -0.6994087E+00  0.2072381E+00 -0.1108988E+01


*****   symmetry block SYM2   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       1.97353897
       2      -0.01131775     0.00824236
       3       0.01639332    -0.00753395     0.00938167
       4      -0.01408177     0.00783321    -0.00637747     0.00845004

               eno   1        eno   2        eno   3        eno   4
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   1.9738440      0.0228970      0.0026202      0.0002519

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4
  mo    1   0.999922E+00  -0.123521E-01  -0.102105E-02  -0.163771E-02
  mo    2  -0.581837E-02  -0.589251E+00   0.160358E+00   0.791855E+00
  mo    3   0.839003E-02   0.579220E+00   0.767072E+00   0.275744E+00
  mo    4  -0.721471E-02  -0.563146E+00   0.621197E+00  -0.544911E+00
  eigenvectors of nos in ao-basis, column    1
  0.7461285E+00 -0.1041125E+00 -0.5634984E+00  0.2298337E+00
  eigenvectors of nos in ao-basis, column    2
  0.1306638E+01 -0.6226540E+00  0.9912718E+00 -0.3870822E+00
  eigenvectors of nos in ao-basis, column    3
 -0.6561996E+00  0.1799364E+01  0.1605657E+01 -0.1001544E+01
  eigenvectors of nos in ao-basis, column    4
  0.5919757E+00 -0.1396601E+01  0.3726156E+00 -0.1871719E+01


*****   symmetry block SYM3   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2
       1       1.98366017
       2       0.01792798     0.01537213

               eno   1        eno   2
 emo    1   0.100000E+01   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01

 occupation numbers of nos
   1.9838235      0.0152088

  eigenvectors of nos in mo-basis

                no   1         no   2
  mo    1   0.999959E+00  -0.910728E-02
  mo    2   0.910728E-02   0.999959E+00
  eigenvectors of nos in ao-basis, column    1
  0.9120721E+00  0.1067847E+00
  eigenvectors of nos in ao-basis, column    2
 -0.1415478E+01  0.1680492E+01


 total number of electrons =   10.0000000000


 Driverdalton for potential called 


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***


 cosurf_and_phi
  1.262739748330  2.872870034186  1.567866969040 -0.050145780670
  1.652947872482  0.360731428865  3.496571379926 -0.070863329347
  0.594940552015  0.670555958619  3.845537291721 -0.085204797034
  2.390078334690  1.202722009900  2.566708449184 -0.044598956884
  1.989334673621  2.229216060677  2.001028520754 -0.044975539551
  2.919143888229  0.378144896597  2.099775822683 -0.020254905213
  0.105380882100  1.878884201281  3.371423292662 -0.083288781405
  0.783266721831  2.590884380442  2.520834408126 -0.070860753427
  2.011402526412  2.551496219550  0.814855178005 -0.017890887210
  0.099626804209  1.855073908563 -1.945822518898  0.050288415623
  0.119020599620  3.173161356543  1.415159765853 -0.055971102414
  0.915237473358  3.108118624501  0.463299650838 -0.029630473120
  0.462814589486  2.837210699514 -0.795516582628  0.004436266651
  1.408719892640  1.640288870679  3.148120355694 -0.071321468198
  2.650053602975  1.633766196698  1.655441764425 -0.019782693224
  1.142904167226  2.885766749848 -0.243475252462 -0.005626328523
  3.656106873706  0.713798214804  0.361832640492  0.035533239967
  2.408046317685  2.075600470298 -1.226192756897  0.045280710986
  1.295820311657  0.567673501678 -2.747601655947  0.071071095270
  3.527730862121  1.045649613724 -1.064853177123  0.053889160217
  2.622898581638  1.024710819001 -2.241122746235  0.063778067738
  3.086808508398  1.794857685487 -0.179703829578  0.034947326382
  1.615872011596  1.674366500124 -2.147504385867  0.058659534380
  0.000006852884  1.035694667087 -2.361676372823  0.071968048712
  0.298815704890  0.969607820141 -2.408807386530  0.072882159101
  0.667723897920  1.245157743773 -2.338577856151  0.068069144404
  1.218123388571  2.025110412626 -1.616576841774  0.049203654265
  2.084186643139  2.293984793080 -0.480493513306  0.027592468075
  2.876642520254  1.658030225134  0.559035126479  0.020039489075
  3.282919570196  0.368088739237  1.091983758387  0.020286578288
  0.467932162408  1.694535407938 -1.941510815499  0.057448529893
  1.299770347024  2.453875604722 -0.850324284820  0.022656691643
  2.242050270258  2.245320705184  0.385739526123  0.004605035237
  2.923104801922  1.151131828431  1.279135167181  0.003132049321
  0.427044967836  0.580963354323 -2.585108185092  0.075685997072
 end_of_phi


 nsubv after electrostatic potential =  5

  Confirmation of dielectric energy for ground state
  edielnew =  -0.0035877558

 =========== Executing IN-CORE method ==========
 norm multnew:  2.69408179E-07
 threx: total number of loops= 14
 threx: total number of loops= 24
 blstr diagel,allext,nmin2=    72
 onel.diag.
 -0.337266E+01-0.422684E+01-0.519338E+01-0.429205E+01-0.316176E+01-0.409289E+01-0.515591E+01-0.533802E+01-0.330515E+02-0.786314E+01
 -0.694100E+01-0.664833E+01-0.712617E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1        72        72
 blstr diagel,allext,nmin2=    72
 onel.diag.
 -0.337266E+01-0.422684E+01-0.519338E+01-0.429205E+01-0.316176E+01-0.409289E+01-0.515591E+01-0.533802E+01-0.330515E+02-0.786314E+01
 -0.694100E+01-0.664833E+01-0.712617E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1        72        72
 blstr diagel,allext,nmin2=    72
 onel.diag.
 -0.337266E+01-0.422684E+01-0.519338E+01-0.429205E+01-0.316176E+01-0.409289E+01-0.515591E+01-0.533802E+01-0.330515E+02-0.786314E+01
 -0.694100E+01-0.664833E+01-0.712617E+01
 i,start,strti,fin,filind,bufszi,bfszi2,nmbuf,nmin2, nd4ext = 
         1         1         2         1        12      4095      8190         1        72        72


====================================================================================================
Diagonal     counts:  0x:       440 2x:       145 4x:        30
All internal counts: zz :         0 yy:        12 xx:        36 ww:        79
One-external counts: yz :        25 yx:       118 yw:       166
Two-external counts: yy :        35 ww:       120 xx:        80 xz:        10 wz:        15 wx:       100
Three-ext.   counts: yx :        39 yw:        51

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         9    task #   2:        14    task #   3:        85    task #   4:        24
task #   5:       115    task #   6:       160    task #   7:        -1    task #   8:        11
task #   9:        28    task #  10:        71    task #  11:        14    task #  12:         1
task #  13:         1    task #  14:         1    task #
 matrix-vector product formed for vector   6
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 rtolcosmo =   0.001
 calctciref: ... reading 
 cirefv                                                       recamt= 1

          reference-subspace vector overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     1.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.98046613    -0.05842346    -0.00839266     0.09325948     0.01715829     0.16189714
 subspace has dimension  6

          (h-repnuc*1) matrix in the subspace basis

                x:   1         x:   2         x:   3         x:   4         x:   5         x:   6
   x:   1   -85.35158114
   x:   2    -0.16554915    -5.21721799
   x:   3    -0.00010214     0.06419092    -0.15573457
   x:   4     0.00005708    -0.03785906     0.00813746    -0.01255983
   x:   5    -0.00000663     0.00439237    -0.00169912     0.00099809    -0.00077310
   x:   6     0.00000024    -0.00015851     0.00005179     0.00009133    -0.00001399    -0.00002243

          overlap matrix in the subspace basis

                x:   1         x:   2         x:   3         x:   4         x:   5         x:   6
   x:   1     1.00000000
   x:   2     0.00000000     0.06361575
   x:   3     0.00000000    -0.00082136     0.00187421
   x:   4     0.00000000     0.00044202    -0.00009773     0.00015014
   x:   5     0.00000000    -0.00005128     0.00001988    -0.00001190     0.00000927
   x:   6     0.00000000     0.00000185    -0.00000061    -0.00000107     0.00000017     0.00000027

          eigenvectors and eigenvalues in the subspace basis

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6

   energy   -85.48092083   -83.89749947   -83.63850458   -83.09870799   -82.78210684   -81.57824562

   x:   1     0.98046613    -0.05842346    -0.00839266     0.09325948     0.01715829     0.16189714
   x:   2     0.76562092     0.52557247     0.11020460    -1.26989541    -0.25156030    -3.68313229
   x:   3     1.09764231     7.34433592    -7.77438308   -16.57047285   -11.43596542     6.35717202
   x:   4     0.93687749    66.34118395    24.20776988   -19.64383718    42.47796164    26.33522755
   x:   5     0.96944042   115.24711563  -244.02583260    58.00614676   211.25477889   -32.73552408
   x:   6    -0.97173448  -904.49858149   304.41242466 -1315.39576932  1047.28839344   341.98936264

          eigenvectors and eigenvalues in the schmidt orthonormalized basis

                v:   1         v:   2         v:   3         v:   4         v:   5         v:   6

   energy   -85.48092083   -83.89749947   -83.63850458   -83.09870799   -82.78210684   -81.57824562

  zx:   1     0.98046613    -0.05842346    -0.00839266     0.09325948     0.01715829     0.16189714
  zx:   2     0.19096934     0.19483917     0.14738754    -0.32220551     0.01296770    -0.89434935
  zx:   3     0.04583223     0.23912613    -0.49992792    -0.62991929    -0.50432384     0.19912139
  zx:   4     0.01041484     0.77383853     0.47722422    -0.16367624     0.22228317     0.31164529
  zx:   5     0.00275022     0.30064047    -0.69028716     0.12263933     0.64129159    -0.08255943
  zx:   6    -0.00049493    -0.46068680     0.15504589    -0.66996840     0.53341370     0.17418489

          <ref|baseci> overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     1.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

 trial vector basis is being transformed.  new dimension:   3

          transformed tciref transformation matrix  block   1

               ref   1        ref   2        ref   3
  ci:   1     0.98046613    -0.05842346    -0.00839266

  write the transformed vectors back to cosmofile

 rtflw =  0
 freezing =  F
 resid(rootcalc)   0.000287710193
 *******************************************
 ** Charges frozen from cosmo calculation **
 *******************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  6  1    -76.1363483462 -9.3446E+00  3.1458E-08  2.8771E-04  1.0000E-03
 mr-sdci #  6  2    -74.5529269893 -9.1788E+00  0.0000E+00  7.4044E-01  1.0000E-04
 mr-sdci #  6  3    -74.2939320960 -9.3019E+00  0.0000E+00  1.1106E+00  1.0000E-04
 mr-sdci #  6  4    -73.7541355041 -9.1438E+00  0.0000E+00  1.1439E+00  1.0000E-04
 mr-sdci #  6  5    -73.4375343594 -8.1870E+00  0.0000E+00  1.6663E+00  1.0000E-04
 mr-sdci #  6  6    -72.2336731321  7.2234E+01  0.0000E+00  1.0205E+00  1.0000E-04


 total energy in cosmo calc 
e(rootcalc) + repnuc=     -76.1363483462
dielectric energy =      -0.0135735826
deltaediel =       0.0135735826
e(rootcalc) + repnuc - ediel =     -76.1227747637
e(rootcalc) + repnuc - edielnew =     -76.1363483462
deltaelast =      76.1227747637


 mr-sdci  convergence criteria satisfied after  6 iterations.

 *************************************************
 ***Final Calc of density matrix for cosmo calc***
 *************************************************

 entering drivercid: firstcall,firstnonref= F T
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=       3  D0X=       9  D0W=      15
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================

          ci-one-electron density block   1

                mo   1         mo   2         mo   3         mo   4         mo   5         mo   6         mo   7
   mo   1     1.99995831
   mo   2    -0.00001134     1.98745036
   mo   3    -0.00003967     0.00476248     1.97917311
   mo   4     0.00009416    -0.00516105    -0.02880236     0.00799404
   mo   5     0.00005375     0.00183039    -0.00527076     0.00794150     0.00980341
   mo   6    -0.00032218     0.01236882     0.00597105     0.00387681     0.00481781     0.01209774
   mo   7     0.00031180     0.00035269    -0.01276861     0.00397550     0.00282322    -0.00220670     0.00487165

          ci-one-electron density block   2

                mo   8         mo   9         mo  10         mo  11
   mo   8     1.97353750
   mo   9    -0.01118092     0.00823062
   mo  10     0.01646195    -0.00753534     0.00938682
   mo  11    -0.01414165     0.00783102    -0.00638306     0.00846360

          ci-one-electron density block   3

                mo  12         mo  13
   mo  12     1.98367477
   mo  13     0.01787112     0.01535808

          ci-one-electron density block   4


*****   symmetry block SYM1   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       1.99995831
       2      -0.00001134     1.98745036
       3      -0.00003967     0.00476248     1.97917311
       4       0.00009416    -0.00516105    -0.02880236     0.00799404
       5       0.00005375     0.00183039    -0.00527076     0.00794150
       6      -0.00032218     0.01236882     0.00597105     0.00387681
       7       0.00031180     0.00035269    -0.01276861     0.00397550

               Column   5     Column   6     Column   7
       5       0.00980341
       6       0.00481781     0.01209774
       7       0.00282322    -0.00220670     0.00487165

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   1.9999586      1.9898731      1.9773790      0.0214039      0.0106867
   0.0018758      0.0001718

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7
  mo    1   0.999994E+00   0.309761E-02   0.150687E-02   0.156516E-04   0.227840E-03  -0.335181E-04   0.192364E-04
  mo    2  -0.214184E-02   0.901894E+00  -0.431899E+00  -0.245999E-02  -0.507494E-02  -0.142179E-02   0.356112E-02
  mo    3  -0.269421E-02   0.431797E+00   0.901816E+00   0.931760E-02  -0.106267E-01   0.395492E-02   0.755029E-02
  mo    4   0.918934E-04  -0.861683E-02  -0.120808E-01   0.552907E+00  -0.283385E+00   0.172585E+00   0.764186E+00
  mo    5   0.323420E-04  -0.337069E-03  -0.287483E-02   0.629833E+00  -0.221088E+00  -0.631052E+00  -0.395217E+00
  mo    6  -0.183430E-03   0.692872E-02  -0.272024E-05   0.520264E+00   0.745628E+00   0.373364E+00  -0.184163E+00
  mo    7   0.173580E-03  -0.264226E-02  -0.594316E-02   0.163822E+00  -0.560993E+00   0.657701E+00  -0.475223E+00
  eigenvectors of nos in ao-basis, column    1
  0.1000653E+01  0.6795520E-03 -0.4334665E-02 -0.4485034E-02  0.2132441E-02  0.7940059E-03  0.4690748E-03
  eigenvectors of nos in ao-basis, column    2
 -0.7961381E-02  0.9064979E+00  0.2384359E-01  0.1886602E+00  0.6756271E-01  0.1285360E+00 -0.8074523E-01
  eigenvectors of nos in ao-basis, column    3
  0.1190531E-02 -0.1068664E+00  0.1549027E+00  0.7975342E+00  0.9006996E-02 -0.4129102E+00  0.1953571E+00
  eigenvectors of nos in ao-basis, column    4
  0.1159092E+00  0.8485574E+00 -0.2627453E+00 -0.1197835E+01  0.8007572E+00 -0.9344397E+00  0.4222542E+00
  eigenvectors of nos in ao-basis, column    5
 -0.4695388E+00 -0.2390349E+01  0.2350723E+01 -0.7437117E+00  0.1097553E+01  0.3277599E+00 -0.1851787E+00
  eigenvectors of nos in ao-basis, column    6
  0.4829521E+00  0.1954468E+01 -0.3096811E+01 -0.3525681E+00  0.1354129E+01  0.2042508E+01 -0.1173169E+01
  eigenvectors of nos in ao-basis, column    7
 -0.4294533E+00 -0.1727205E+01  0.2893550E+01  0.3116245E+00 -0.6995917E+00  0.2088510E+00 -0.1109888E+01


*****   symmetry block SYM2   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       1.97353750
       2      -0.01118092     0.00823062
       3       0.01646195    -0.00753534     0.00938682
       4      -0.01414165     0.00783102    -0.00638306     0.00846360

               eno   1        eno   2        eno   3        eno   4
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   1.9738430      0.0229023      0.0026220      0.0002513

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4
  mo    1   0.999922E+00  -0.123493E-01  -0.103589E-02  -0.171526E-02
  mo    2  -0.574898E-02  -0.588866E+00   0.158480E+00   0.792520E+00
  mo    3   0.842484E-02   0.579358E+00   0.766412E+00   0.277282E+00
  mo    4  -0.724508E-02  -0.563408E+00   0.622491E+00  -0.543161E+00
  eigenvectors of nos in ao-basis, column    1
  0.7461593E+00 -0.1041894E+00 -0.5634413E+00  0.2296643E+00
  eigenvectors of nos in ao-basis, column    2
  0.1306925E+01 -0.6233238E+00  0.9914581E+00 -0.3879946E+00
  eigenvectors of nos in ao-basis, column    3
 -0.6576219E+00  0.1802679E+01  0.1604775E+01 -0.9970998E+00
  eigenvectors of nos in ao-basis, column    4
  0.5897211E+00 -0.1392014E+01  0.3759913E+00 -0.1873922E+01


*****   symmetry block SYM3   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2
       1       1.98367477
       2       0.01787112     0.01535808

               eno   1        eno   2
 emo    1   0.100000E+01   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01

 occupation numbers of nos
   1.9838370      0.0151958

  eigenvectors of nos in mo-basis

                no   1         no   2
  mo    1   0.999959E+00  -0.907827E-02
  mo    2   0.907827E-02   0.999959E+00
  eigenvectors of nos in ao-basis, column    1
  0.9121132E+00  0.1067359E+00
  eigenvectors of nos in ao-basis, column    2
 -0.1415452E+01  0.1680495E+01


 total number of electrons =   10.0000000000


 root =  1


 DALTON: user specified work memory size used,
          -m = " 9999997"

 Work memory size (LMWORK) :     9999997 =   76.29 megabytes.
 *****Module DALTON-COSMO*****
 ***Electrostatic Potential Calculation***
 *** End of DALTON-COSMO calculation ***


 cosurf_and_pot_on_the_outer_cavity
  2.073972598936  4.718525890344  2.111755307006 -0.015913780626
  2.714866756337  0.592484285675  5.279540167909 -0.028348842025
  0.977151244524  1.101353062742  5.852696389317 -0.033391153417
  3.925560684092  1.975404862085  3.752294365964 -0.017899315608
  3.267362519836  3.661361660599  2.823197971886 -0.015979755090
  4.794520096155  0.621084894014  2.985384674181 -0.009613069016
  0.173077949460  3.085961949671  5.073991710630 -0.031231190953
  1.286466029251  4.255380848585  3.676948920622 -0.024925180554
  3.303607685031  4.190688084329  0.874977219987 -0.004387449906
  0.163627211063  3.046854928202 -3.659275392660  0.024109941761
  0.195480392480  5.211736928518  1.860942604097 -0.017040171405
  1.503220315915  5.104908022693  0.297567705890 -0.006413581226
  0.760142032813  4.659957506362 -1.769964770589  0.007443344063
  2.313736498748  2.694082990980  4.707229619388 -0.027236189051
  4.352554947409  2.683369878192  2.255591816868 -0.007882415529
  1.877149624159  4.739707996010 -0.863268981216  0.002683191093
  5.557452224045  1.320533821773  0.975930811333  0.005433081554
  3.248540195407  3.839867994436 -1.961916173837  0.013713802337
  1.190922084255  1.050203102491 -4.776522637080  0.029618886729
  5.319956602612  1.934458909776 -1.663437951255  0.016630768845
  3.646016883720  1.895722139538 -3.839536654111  0.023539670553
  4.504250248226  3.320493842536 -0.025911658296  0.006508299167
  1.783017729142  3.097585149616 -3.666342687430  0.022401074524
 end_of_cosurf_and_pot_on_the_outer_cavity


 **************************************************
 ***Final cosmo calculation for ground state*******
 **************************************************

  ediel before cosmo(4 =  -0.00339339564
  elast before cosmo(4 =  -76.1227748


 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  6  1    -76.1363483462 -9.3446E+00  3.1458E-08  2.8771E-04  1.0000E-03
 mr-sdci #  6  2    -74.5529269893 -9.1788E+00  0.0000E+00  7.4044E-01  1.0000E-04
 mr-sdci #  6  3    -74.2939320960 -9.3019E+00  0.0000E+00  1.1106E+00  1.0000E-04

####################CIUDGINFO####################

   ci vector at position   1 energy=  -76.136348346245

################END OF CIUDGINFO################


    1 of the   4 expansion vectors are transformed.

          transformed tciref transformation matrix  block   1

               ref   1        ref   2        ref   3
  ci:   1     0.98046613    -0.05842346    -0.00839266
    1 of the   3 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)
 maximum overlap with reference  1(overlap=  0.980466134)
 ============== writing to civout ======================
 1 32768 282
 6 5 6
 #cirefvfl= 1 #civfl= 1 method= 0 last= 1 overlap= 98%(  0.980466134)
 Hermit Integral Program : SIFS version  hochtor2        Thu Oct  7 15:18:42 2004
  cidrt_title                                                                    
  title                                                                          
 mofmt: formatted orbitals label=morbl   hochtor2        Thu Oct  7 15:18:43 2004
 SIFS file created by program tran.      hochtor2        Thu Oct  7 15:18:43 2004
 energy computed by program ciudg.       hochtor2        Thu Oct  7 15:18:49 2004
 energy of type -1=  9.31791392
 energy of type -1026= -76.1363483
 energy of type -2055=  0.000287710193
 energy of type -2056= -9.34457194
 energy of type -2057=  3.1457615E-08

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.98046613    -0.05842346    -0.00839266     0.09325948     0.01715829     0.16189714     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.98046613    -0.05842346    -0.00839266     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 ==================================================

information on vector: 1from unit 11 written to unit 16filename civout                                                      


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -76.1363483462

                                                       internal orbitals

                                          level       1    2    3    4    5

                                          orbital     1    2    3    8   12

                                         symmetry   a1   a1   a1   b1   b2 

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1  0.980466                        +-   +-   +-   +-   +- 
 y   1  1       2  0.012502              1( b2 )   +-   +-   +-   +-    - 
 y   1  1       4 -0.012642              2( b1 )   +-   +-   +-    -   +- 
 y   1  1       5  0.010302              3( b1 )   +-   +-   +-    -   +- 
 y   1  1       6 -0.021266              1( a1 )   +-   +-    -   +-   +- 
 x   1  1      18 -0.025830    1( b1 )   1( b2 )   +-   +-   +-    -    - 
 x   1  1      19  0.013472    2( b1 )   1( b2 )   +-   +-   +-    -    - 
 x   1  1      20 -0.030887    3( b1 )   1( b2 )   +-   +-   +-    -    - 
 x   1  1      21  0.012733    1( a1 )   1( b2 )   +-   +-    -   +-    - 
 x   1  1      22  0.014334    2( a1 )   1( b2 )   +-   +-    -   +-    - 
 x   1  1      23  0.037570    3( a1 )   1( b2 )   +-   +-    -   +-    - 
 x   1  1      27 -0.022944    3( a1 )   1( b1 )   +-   +-    -    -   +- 
 x   1  1      31  0.013920    3( a1 )   2( b1 )   +-   +-    -    -   +- 
 x   1  1      35 -0.026836    3( a1 )   3( b1 )   +-   +-    -    -   +- 
 x   1  1      37  0.014980    1( a1 )   1( b2 )   +-    -   +-   +-    - 
 x   1  1      38  0.010192    2( a1 )   1( b2 )   +-    -   +-   +-    - 
 x   1  1      40  0.016937    4( a1 )   1( b2 )   +-    -   +-   +-    - 
 x   1  1      41 -0.010301    1( a1 )   1( b1 )   +-    -   +-    -   +- 
 x   1  1      52 -0.012107    4( a1 )   3( b1 )   +-    -   +-    -   +- 
 x   1  1      54  0.012512    1( a1 )   3( a1 )   +-    -    -   +-   +- 
 x   1  1      58 -0.014750    3( a1 )   4( a1 )   +-    -    -   +-   +- 
 w   1  1      96 -0.010405    1( a1 )   1( a1 )   +-   +-   +-   +-      
 w   1  1     105 -0.011998    4( a1 )   4( a1 )   +-   +-   +-   +-      
 w   1  1     112 -0.049792    1( b2 )   1( b2 )   +-   +-   +-   +-      
 w   1  1     113  0.025476    1( b1 )   1( b2 )   +-   +-   +-   +     - 
 w   1  1     114 -0.014040    2( b1 )   1( b2 )   +-   +-   +-   +     - 
 w   1  1     115  0.032094    3( b1 )   1( b2 )   +-   +-   +-   +     - 
 w   1  1     116 -0.014701    1( a1 )   1( a1 )   +-   +-   +-        +- 
 w   1  1     117 -0.020537    1( a1 )   2( a1 )   +-   +-   +-        +- 
 w   1  1     118 -0.029085    2( a1 )   2( a1 )   +-   +-   +-        +- 
 w   1  1     126 -0.027316    1( b1 )   1( b1 )   +-   +-   +-        +- 
 w   1  1     127  0.028795    1( b1 )   2( b1 )   +-   +-   +-        +- 
 w   1  1     128 -0.035865    2( b1 )   2( b1 )   +-   +-   +-        +- 
 w   1  1     129 -0.027782    1( b1 )   3( b1 )   +-   +-   +-        +- 
 w   1  1     130  0.019571    2( b1 )   3( b1 )   +-   +-   +-        +- 
 w   1  1     131 -0.024982    3( b1 )   3( b1 )   +-   +-   +-        +- 
 w   1  1     135 -0.040719    3( a1 )   1( b2 )   +-   +-   +    +-    - 
 w   1  1     136  0.010151    4( a1 )   1( b2 )   +-   +-   +    +-    - 
 w   1  1     137  0.018956    1( a1 )   1( b1 )   +-   +-   +     -   +- 
 w   1  1     138  0.019939    2( a1 )   1( b1 )   +-   +-   +     -   +- 
 w   1  1     139  0.022870    3( a1 )   1( b1 )   +-   +-   +     -   +- 
 w   1  1     141 -0.020223    1( a1 )   2( b1 )   +-   +-   +     -   +- 
 w   1  1     142 -0.037623    2( a1 )   2( b1 )   +-   +-   +     -   +- 
 w   1  1     143 -0.014529    3( a1 )   2( b1 )   +-   +-   +     -   +- 
 w   1  1     146  0.012576    2( a1 )   3( b1 )   +-   +-   +     -   +- 
 w   1  1     147  0.027209    3( a1 )   3( b1 )   +-   +-   +     -   +- 
 w   1  1     149 -0.012437    1( a1 )   1( a1 )   +-   +-        +-   +- 
 w   1  1     150 -0.013339    1( a1 )   2( a1 )   +-   +-        +-   +- 
 w   1  1     151 -0.012738    2( a1 )   2( a1 )   +-   +-        +-   +- 
 w   1  1     152 -0.010380    1( a1 )   3( a1 )   +-   +-        +-   +- 
 w   1  1     153 -0.014611    2( a1 )   3( a1 )   +-   +-        +-   +- 
 w   1  1     154 -0.041992    3( a1 )   3( a1 )   +-   +-        +-   +- 
 w   1  1     157  0.014333    3( a1 )   4( a1 )   +-   +-        +-   +- 
 w   1  1     160  0.016715    1( b1 )   2( b1 )   +-   +-        +-   +- 
 w   1  1     161 -0.022383    2( b1 )   2( b1 )   +-   +-        +-   +- 
 w   1  1     166 -0.026553    1( a1 )   1( b2 )   +-   +    +-   +-    - 
 w   1  1     167 -0.019123    2( a1 )   1( b2 )   +-   +    +-   +-    - 
 w   1  1     169 -0.029748    4( a1 )   1( b2 )   +-   +    +-   +-    - 
 w   1  1     170  0.021373    1( a1 )   1( b1 )   +-   +    +-    -   +- 
 w   1  1     171  0.015003    2( a1 )   1( b1 )   +-   +    +-    -   +- 
 w   1  1     173  0.014448    4( a1 )   1( b1 )   +-   +    +-    -   +- 
 w   1  1     174 -0.014667    1( a1 )   2( b1 )   +-   +    +-    -   +- 
 w   1  1     175 -0.028224    2( a1 )   2( b1 )   +-   +    +-    -   +- 
 w   1  1     178  0.022246    1( a1 )   3( b1 )   +-   +    +-    -   +- 
 w   1  1     179  0.019350    2( a1 )   3( b1 )   +-   +    +-    -   +- 
 w   1  1     181  0.021653    4( a1 )   3( b1 )   +-   +    +-    -   +- 
 w   1  1     182 -0.014958    1( a1 )   1( a1 )   +-   +     -   +-   +- 
 w   1  1     183 -0.018975    1( a1 )   2( a1 )   +-   +     -   +-   +- 
 w   1  1     184 -0.019011    2( a1 )   2( a1 )   +-   +     -   +-   +- 
 w   1  1     185 -0.019722    1( a1 )   3( a1 )   +-   +     -   +-   +- 
 w   1  1     186 -0.012148    2( a1 )   3( a1 )   +-   +     -   +-   +- 
 w   1  1     190 -0.024523    3( a1 )   4( a1 )   +-   +     -   +-   +- 
 w   1  1     199 -0.012724    1( a1 )   1( a1 )   +-        +-   +-   +- 
 w   1  1     200 -0.015568    1( a1 )   2( a1 )   +-        +-   +-   +- 
 w   1  1     201 -0.014755    2( a1 )   2( a1 )   +-        +-   +-   +- 
 w   1  1     205 -0.012924    1( a1 )   4( a1 )   +-        +-   +-   +- 
 w   1  1     208 -0.012317    4( a1 )   4( a1 )   +-        +-   +-   +- 

 ci coefficient statistics:
           rq > 0.1                1
      0.1> rq > 0.01              76
     0.01> rq > 0.001             96
    0.001> rq > 0.0001            93
   0.0001> rq > 0.00001           15
  0.00001> rq > 0.000001           1
 0.000001> rq                      0
           all                   282
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        15 2x:         0 4x:         0
All internal counts: zz :         0 yy:         0 xx:         0 ww:         0
One-external counts: yz :         0 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         0    task #   2:         0    task #   3:         0    task #   4:         0
task #   5:         0    task #   6:         0    task #   7:        -1    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:        14    task #  12:         0
task #  13:         0    task #  14:         0    task #
  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.980466134435    -74.522297962444

 number of reference csfs (nref) is     1.  root number (iroot) is  1.

 pople ci energy extrapolation is computed with 10 correlated electrons.

 eref      =    -76.007008651407   "relaxed" cnot**2         =   0.961313840775
 eci       =    -76.136348346245   deltae = eci - eref       =  -0.129339694838
 eci+dv1   =    -76.141352002274   dv1 = (1-cnot**2)*deltae  =  -0.005003656029
 eci+dv2   =    -76.141553364436   dv2 = dv1 / cnot**2       =  -0.005205018191
 eci+dv3   =    -76.141771612996   dv3 = dv1 / (2*cnot**2-1) =  -0.005423266751
 eci+pople =    -76.140614231611   ( 10e- scaled deltae )    =  -0.133605580204
NO coefficients and occupation numbers are written to nocoef_ci.1
 entering drivercid: firstcall,firstnonref= F T
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       5  DYX=      20  DYW=      25
   D0Z=       0  D0Y=       3  D0X=       9  D0W=      15
  DDZI=       5 DDYI=      25 DDXI=      50 DDWI=      70
  DDZE=       0 DDYE=       5 DDXE=      10 DDWE=      15
================================================================================

          ci-one-electron density block   1

                mo   1         mo   2         mo   3         mo   4         mo   5         mo   6         mo   7
   mo   1     1.99995831
   mo   2    -0.00001134     1.98745036
   mo   3    -0.00003967     0.00476248     1.97917311
   mo   4     0.00009416    -0.00516105    -0.02880236     0.00799404
   mo   5     0.00005375     0.00183039    -0.00527076     0.00794150     0.00980341
   mo   6    -0.00032218     0.01236882     0.00597105     0.00387681     0.00481781     0.01209774
   mo   7     0.00031180     0.00035269    -0.01276861     0.00397550     0.00282322    -0.00220670     0.00487165

          ci-one-electron density block   2

                mo   8         mo   9         mo  10         mo  11
   mo   8     1.97353750
   mo   9    -0.01118092     0.00823062
   mo  10     0.01646195    -0.00753534     0.00938682
   mo  11    -0.01414165     0.00783102    -0.00638306     0.00846360

          ci-one-electron density block   3

                mo  12         mo  13
   mo  12     1.98367477
   mo  13     0.01787112     0.01535808

          ci-one-electron density block   4


*****   symmetry block SYM1   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       1.99995831
       2      -0.00001134     1.98745036
       3      -0.00003967     0.00476248     1.97917311
       4       0.00009416    -0.00516105    -0.02880236     0.00799404
       5       0.00005375     0.00183039    -0.00527076     0.00794150
       6      -0.00032218     0.01236882     0.00597105     0.00387681
       7       0.00031180     0.00035269    -0.01276861     0.00397550

               Column   5     Column   6     Column   7
       5       0.00980341
       6       0.00481781     0.01209774
       7       0.00282322    -0.00220670     0.00487165

               eno   1        eno   2        eno   3        eno   4        eno   5        eno   6        eno   7
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    5   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    6   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    7   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   1.9999586      1.9898731      1.9773790      0.0214039      0.0106867
   0.0018758      0.0001718

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4         no   5         no   6         no   7
  mo    1   0.999994E+00   0.309761E-02   0.150687E-02   0.156516E-04   0.227840E-03  -0.335181E-04   0.192364E-04
  mo    2  -0.214184E-02   0.901894E+00  -0.431899E+00  -0.245999E-02  -0.507494E-02  -0.142179E-02   0.356112E-02
  mo    3  -0.269421E-02   0.431797E+00   0.901816E+00   0.931760E-02  -0.106267E-01   0.395492E-02   0.755029E-02
  mo    4   0.918934E-04  -0.861683E-02  -0.120808E-01   0.552907E+00  -0.283385E+00   0.172585E+00   0.764186E+00
  mo    5   0.323420E-04  -0.337069E-03  -0.287483E-02   0.629833E+00  -0.221088E+00  -0.631052E+00  -0.395217E+00
  mo    6  -0.183430E-03   0.692872E-02  -0.272024E-05   0.520264E+00   0.745628E+00   0.373364E+00  -0.184163E+00
  mo    7   0.173580E-03  -0.264226E-02  -0.594316E-02   0.163822E+00  -0.560993E+00   0.657701E+00  -0.475223E+00
  eigenvectors of nos in ao-basis, column    1
  0.1000653E+01  0.6795520E-03 -0.4334665E-02 -0.4485034E-02  0.2132441E-02  0.7940059E-03  0.4690748E-03
  eigenvectors of nos in ao-basis, column    2
 -0.7961381E-02  0.9064979E+00  0.2384359E-01  0.1886602E+00  0.6756271E-01  0.1285360E+00 -0.8074523E-01
  eigenvectors of nos in ao-basis, column    3
  0.1190531E-02 -0.1068664E+00  0.1549027E+00  0.7975342E+00  0.9006996E-02 -0.4129102E+00  0.1953571E+00
  eigenvectors of nos in ao-basis, column    4
  0.1159092E+00  0.8485574E+00 -0.2627453E+00 -0.1197835E+01  0.8007572E+00 -0.9344397E+00  0.4222542E+00
  eigenvectors of nos in ao-basis, column    5
 -0.4695388E+00 -0.2390349E+01  0.2350723E+01 -0.7437117E+00  0.1097553E+01  0.3277599E+00 -0.1851787E+00
  eigenvectors of nos in ao-basis, column    6
  0.4829521E+00  0.1954468E+01 -0.3096811E+01 -0.3525681E+00  0.1354129E+01  0.2042508E+01 -0.1173169E+01
  eigenvectors of nos in ao-basis, column    7
 -0.4294533E+00 -0.1727205E+01  0.2893550E+01  0.3116245E+00 -0.6995917E+00  0.2088510E+00 -0.1109888E+01


*****   symmetry block SYM2   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2     Column   3     Column   4
       1       1.97353750
       2      -0.01118092     0.00823062
       3       0.01646195    -0.00753534     0.00938682
       4      -0.01414165     0.00783102    -0.00638306     0.00846360

               eno   1        eno   2        eno   3        eno   4
 emo    1   0.100000E+01   0.000000E+00   0.000000E+00   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01   0.000000E+00   0.000000E+00
 emo    3   0.000000E+00   0.000000E+00   0.100000E+01   0.000000E+00
 emo    4   0.000000E+00   0.000000E+00   0.000000E+00   0.100000E+01

 occupation numbers of nos
   1.9738430      0.0229023      0.0026220      0.0002513

  eigenvectors of nos in mo-basis

                no   1         no   2         no   3         no   4
  mo    1   0.999922E+00  -0.123493E-01  -0.103589E-02  -0.171526E-02
  mo    2  -0.574898E-02  -0.588866E+00   0.158480E+00   0.792520E+00
  mo    3   0.842484E-02   0.579358E+00   0.766412E+00   0.277282E+00
  mo    4  -0.724508E-02  -0.563408E+00   0.622491E+00  -0.543161E+00
  eigenvectors of nos in ao-basis, column    1
  0.7461593E+00 -0.1041894E+00 -0.5634413E+00  0.2296643E+00
  eigenvectors of nos in ao-basis, column    2
  0.1306925E+01 -0.6233238E+00  0.9914581E+00 -0.3879946E+00
  eigenvectors of nos in ao-basis, column    3
 -0.6576219E+00  0.1802679E+01  0.1604775E+01 -0.9970998E+00
  eigenvectors of nos in ao-basis, column    4
  0.5897211E+00 -0.1392014E+01  0.3759913E+00 -0.1873922E+01


*****   symmetry block SYM3   *****


 first order reduced density matrix in mo basis



               Column   1     Column   2
       1       1.98367477
       2       0.01787112     0.01535808

               eno   1        eno   2
 emo    1   0.100000E+01   0.000000E+00
 emo    2   0.000000E+00   0.100000E+01

 occupation numbers of nos
   1.9838370      0.0151958

  eigenvectors of nos in mo-basis

                no   1         no   2
  mo    1   0.999959E+00  -0.907827E-02
  mo    2   0.907827E-02   0.999959E+00
  eigenvectors of nos in ao-basis, column    1
  0.9121132E+00  0.1067359E+00
  eigenvectors of nos in ao-basis, column    2
 -0.1415452E+01  0.1680495E+01


 total number of electrons =   10.0000000000



          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!


                        A1  partial gross atomic populations
   ao class       1A1        2A1        3A1        4A1        5A1        6A1 
    O1_ s       1.999599   1.805562  -0.023586   0.001745   0.006272   0.000737
    O1_ p       0.000020   0.099274   1.514700   0.010253   0.004004   0.000541
    H1_ s       0.000340   0.085037   0.486265   0.009406   0.000411   0.000598

   ao class       7A1 
    O1_ s       0.000007
    O1_ p       0.000007
    H1_ s       0.000157

                        B1  partial gross atomic populations
   ao class       1B1        2B1        3B1        4B1 
    O1_ p       1.206409   0.012454   0.001915   0.000029
    H1_ s       0.767434   0.010448   0.000707   0.000223

                        B2  partial gross atomic populations
   ao class       1B2        2B2 
    O1_ p       1.983837   0.015196


                        gross atomic populations
     ao           O1_        H1_
      s         3.790337   1.361025
      p         4.848638   0.000000
    total       8.638975   1.361025


 Total number of electrons:   10.00000000

