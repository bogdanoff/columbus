
 program cipc      

 print the csf info for mrsdci wave functions

 written by: ron shepard

 version date: 06-jun-96

 This Version of Program cipc is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 workspace allocation parameters: lencor=  10000000 mem1=1108566024 ifirst=-264082010

 drt header information:
  cidrt_title                                                                    
 nmot  =    13 niot  =     5 nfct  =     0 nfvt  =     0
 nrow  =    20 nsym  =     4 ssym  =     1 lenbuf=  1600
 spnorb=     F spnodd=     F lxyzir(1:3)= 0 0 0
 nwalk,xbar:         31        1        5       10       15
 nvalwt,nvalw:       31        1        5       10       15
 ncsft:             282
 map(*)=     9 10 11  1  2  3  4 12  5  6  7 13  8
 mu(*)=      0  0  0  0  0
 syml(*) =   1  1  1  2  3
 rmo(*)=     1  2  3  1  1

 indx01:    31 indices saved in indxv(*)
===================================ROOT # 1===================================

 rdhciv: CI vector file information:
 Hermit Integral Program : SIFS version  hochtor2        Thu Oct  7 15:18:42 2004
  cidrt_title                                                                    
  title                                                                          
 mofmt: formatted orbitals label=morbl   hochtor2        Thu Oct  7 15:18:43 2004
 SIFS file created by program tran.      hochtor2        Thu Oct  7 15:18:43 2004
 energy computed by program ciudg.       hochtor2        Thu Oct  7 15:18:49 2004

 lenrec =   32768 lenci =       282 ninfo =  6 nenrgy =  5 ntitle =  6

 Max. overlap with ref vector #        1
 Valid ci vector #        1
 Method:        0       98% overlap
 energy( 1)=  9.317913916904E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -7.613634834625E+01, ietype=-1026,   total energy of type: MRSDCI  
 energy( 3)=  2.877101926170E-04, ietype=-2055, cnvginf energy of type: CI-Resid
 energy( 4)= -9.344571939588E+00, ietype=-2056, cnvginf energy of type: CI-D.E. 
 energy( 5)=  3.145761504377E-08, ietype=-2057, cnvginf energy of type: CI-ApxDE
==================================================================================

 space is available for   4983577 coefficients.

 updated histogram parameters:
 csfmn = 0.0000E+00 csfmx = 1.0000E+00 fhist = 5.0000E-01 nhist =  20

 this program will print the csfs generated from
 the drt according to the following print options :

 1) run in batch mode: all valid roots are automatically
    analysed and csf info is printed by default contribution
    threshold 0.01 
 2) run in interactive mode

 input menu number [  0]:
================================================================================
===================================VECTOR # 1===================================
================================================================================


 rdcivnew:      77 coefficients were selected.
 workspace: ncsfmx=     282
 ncsfmx= 282

 histogram parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =     282 ncsft =     282 ncsf =      77
 nhist =  20 fhist = 0.50000

    cmin                cmax        num  '*'=     1 csfs.
 ----------          ----------   ----- ---------|---------|---------|---------|
 5.0000E-01 <= |c| < 1.0000E+00       1 *
 2.5000E-01 <= |c| < 5.0000E-01       0
 1.2500E-01 <= |c| < 2.5000E-01       0
 6.2500E-02 <= |c| < 1.2500E-01       0
 3.1250E-02 <= |c| < 6.2500E-02       7 *******
 1.5625E-02 <= |c| < 3.1250E-02      33 *********************************
 7.8125E-03 <= |c| < 1.5625E-02      36 ************************************
 3.9062E-03 <= |c| < 7.8125E-03       0
 1.9531E-03 <= |c| < 3.9062E-03       0
 9.7656E-04 <= |c| < 1.9531E-03       0
 4.8828E-04 <= |c| < 9.7656E-04       0
 2.4414E-04 <= |c| < 4.8828E-04       0
 1.2207E-04 <= |c| < 2.4414E-04       0
 6.1035E-05 <= |c| < 1.2207E-04       0
 3.0518E-05 <= |c| < 6.1035E-05       0
 1.5259E-05 <= |c| < 3.0518E-05       0
 7.6294E-06 <= |c| < 1.5259E-05       0
 3.8147E-06 <= |c| < 7.6294E-06       0
 0.0000E+00 <= |c| < 3.8147E-06       0
                                  ----- ---------|---------|---------|---------|
                  total read =       77 total stored =      77

 from the selected csfs,
 min(|csfvec(:)|) = 1.0151E-02    max(|csfvec(:)|) = 9.8047E-01
 norm=  1.
 csfs will be printed based on coefficient magnitudes.

 current csfvec(*) selection parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =     282 ncsft =     282 ncsf =      77

 i:slabel(i) =  1: a1   2: b1   3: b2   4: a2 

 internal level =    1    2    3    4    5
 syml(*)        =    1    1    1    2    3
 label          =  a1   a1   a1   b1   b2 
 rmo(*)         =    1    2    3    1    1

 printing selected csfs in sorted order from cmin = 0.01000 to cmax = 1.00000

   indcsf     c     c**2   v  lab:rmo  lab:rmo   step(*)
  ------- -------- ------- - ---- --- ---- --- ------------
        1  0.98047 0.96131 z*                    33333
      112 -0.04979 0.00248 w           b2 :  2  333330
      154 -0.04199 0.00176 w           a1 :  6  333033
      135 -0.04072 0.00166 w  a1 :  6  b2 :  2 1233132
      142 -0.03762 0.00142 w  a1 :  5  b1 :  3 1233123
       23  0.03757 0.00141 x  a1 :  6  b2 :  2 1133232
      128 -0.03587 0.00129 w           b1 :  3  333303
      115  0.03209 0.00103 w  b1 :  4  b2 :  2 1233312
       20 -0.03089 0.00095 x  b1 :  4  b2 :  2 1133322
      169 -0.02975 0.00088 w  a1 :  7  b2 :  2 1231332
      118 -0.02908 0.00085 w           a1 :  5  333303
      127  0.02879 0.00083 w  b1 :  2  b1 :  3 1233303
      175 -0.02822 0.00080 w  a1 :  5  b1 :  3 1231323
      129 -0.02778 0.00077 w  b1 :  2  b1 :  4 1233303
      126 -0.02732 0.00075 w           b1 :  2  333303
      147  0.02721 0.00074 w  a1 :  6  b1 :  4 1233123
       35 -0.02684 0.00072 x  a1 :  6  b1 :  4 1133223
      166 -0.02655 0.00071 w  a1 :  4  b2 :  2 1231332
       18 -0.02583 0.00067 x  b1 :  2  b2 :  2 1133322
      113  0.02548 0.00065 w  b1 :  2  b2 :  2 1233312
      131 -0.02498 0.00062 w           b1 :  4  333303
      190 -0.02452 0.00060 w  a1 :  6  a1 :  7 1231233
       27 -0.02294 0.00053 x  a1 :  6  b1 :  2 1133223
      139  0.02287 0.00052 w  a1 :  6  b1 :  2 1233123
      161 -0.02238 0.00050 w           b1 :  3  333033
      178  0.02225 0.00049 w  a1 :  4  b1 :  4 1231323
      181  0.02165 0.00047 w  a1 :  7  b1 :  4 1231323
      170  0.02137 0.00046 w  a1 :  4  b1 :  2 1231323
        6 -0.02127 0.00045 y           a1 :  4  133233
      117 -0.02054 0.00042 w  a1 :  4  a1 :  5 1233303
      141 -0.02022 0.00041 w  a1 :  4  b1 :  3 1233123
      138  0.01994 0.00040 w  a1 :  5  b1 :  2 1233123
      185 -0.01972 0.00039 w  a1 :  4  a1 :  6 1231233
      130  0.01957 0.00038 w  b1 :  3  b1 :  4 1233303
      179  0.01935 0.00037 w  a1 :  5  b1 :  4 1231323
      167 -0.01912 0.00037 w  a1 :  5  b2 :  2 1231332
      184 -0.01901 0.00036 w           a1 :  5  331233
      183 -0.01898 0.00036 w  a1 :  4  a1 :  5 1231233
      137  0.01896 0.00036 w  a1 :  4  b1 :  2 1233123
       40  0.01694 0.00029 x  a1 :  7  b2 :  2 1132332
      160  0.01672 0.00028 w  b1 :  2  b1 :  3 1233033
      200 -0.01557 0.00024 w  a1 :  4  a1 :  5 1230333
      171  0.01500 0.00023 w  a1 :  5  b1 :  2 1231323
       37  0.01498 0.00022 x  a1 :  4  b2 :  2 1132332
      182 -0.01496 0.00022 w           a1 :  4  331233
      201 -0.01476 0.00022 w           a1 :  5  330333
       58 -0.01475 0.00022 x  a1 :  6  a1 :  7 1132233
      116 -0.01470 0.00022 w           a1 :  4  333303
      174 -0.01467 0.00022 w  a1 :  4  b1 :  3 1231323
      153 -0.01461 0.00021 w  a1 :  5  a1 :  6 1233033
      143 -0.01453 0.00021 w  a1 :  6  b1 :  3 1233123
      173  0.01445 0.00021 w  a1 :  7  b1 :  2 1231323
       22  0.01433 0.00021 x  a1 :  5  b2 :  2 1133232
      157  0.01433 0.00021 w  a1 :  6  a1 :  7 1233033
      114 -0.01404 0.00020 w  b1 :  3  b2 :  2 1233312
       31  0.01392 0.00019 x  a1 :  6  b1 :  3 1133223
       19  0.01347 0.00018 x  b1 :  3  b2 :  2 1133322
      150 -0.01334 0.00018 w  a1 :  4  a1 :  5 1233033
      205 -0.01292 0.00017 w  a1 :  4  a1 :  7 1230333
      151 -0.01274 0.00016 w           a1 :  5  333033
       21  0.01273 0.00016 x  a1 :  4  b2 :  2 1133232
      199 -0.01272 0.00016 w           a1 :  4  330333
        4 -0.01264 0.00016 y           b1 :  3  133323
      146  0.01258 0.00016 w  a1 :  5  b1 :  4 1233123
       54  0.01251 0.00016 x  a1 :  4  a1 :  6 1132233
        2  0.01250 0.00016 y           b2 :  2  133332
      149 -0.01244 0.00015 w           a1 :  4  333033
      208 -0.01232 0.00015 w           a1 :  7  330333
      186 -0.01215 0.00015 w  a1 :  5  a1 :  6 1231233
       52 -0.01211 0.00015 x  a1 :  7  b1 :  4 1132323
      105 -0.01200 0.00014 w           a1 :  7  333330
       96 -0.01040 0.00011 w           a1 :  4  333330
      152 -0.01038 0.00011 w  a1 :  4  a1 :  6 1233033
        5  0.01030 0.00011 y           b1 :  4  133323
       41 -0.01030 0.00011 x  a1 :  4  b1 :  2 1132323
       38  0.01019 0.00010 x  a1 :  5  b2 :  2 1132332
      136  0.01015 0.00010 w  a1 :  7  b2 :  2 1233132
           77 csfs were printed in this range.
