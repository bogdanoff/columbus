 total ao core energy =   31.165309824
 MCSCF calculation performed for  2 DRTs.

 DRT  first state   no.of aver. states   weights
  1   ground state          1             0.500
  2   ground state          1             0.500

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a1 
 Total number of electrons:    16
 Spin multiplicity:            1
 Number of active orbitals:    2
 Number of active electrons:   2
 Total number of CSFs:         2

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a2 
 Total number of electrons:    16
 Spin multiplicity:            1
 Number of active orbitals:    2
 Number of active electrons:   2
 Total number of CSFs:         1

 Number of active-double rotations:      2
 Number of active-active rotations:      0
 Number of double-virtual rotations:   193
 Number of active-virtual rotations:    38

 iter=    1 emc= -113.8130827974 demc= 1.1381E+02 wnorm= 3.1290E-01 knorm= 2.6566E-01 apxde= 1.7740E-02    *not converged* 
 iter=    2 emc= -113.8320666327 demc= 1.8984E-02 wnorm= 2.1797E-02 knorm= 3.2313E-02 apxde= 2.0695E-04    *not converged* 
 iter=    3 emc= -113.8322748990 demc= 2.0827E-04 wnorm= 2.3425E-04 knorm= 2.2604E-04 apxde= 1.3918E-08    *not converged* 
 iter=    4 emc= -113.8322749129 demc= 1.3947E-08 wnorm= 7.7763E-07 knorm= 6.2053E-07 apxde= 1.4439E-13    *not converged* 

 final mcscf convergence values:
 iter=    5 emc= -113.8322749129 demc= 1.8474E-13 wnorm= 2.6857E-07 knorm= 1.9389E-08 apxde= 2.9199E-15    *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 weight 0.500000 total energy= -113.900371492
   DRT #2 state # 1 weight 0.500000 total energy= -113.764178334
   ------------------------------------------------------------


