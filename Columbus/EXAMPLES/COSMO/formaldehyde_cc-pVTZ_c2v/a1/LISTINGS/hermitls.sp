
 DALTON: user specified work memory size used,
          environment variable WRKMEM = "10000000            "

 Work memory size (LMWORK) :    10000000 =   76.29 megabytes.

 Default basis set library used :
        /sphome/kedziora/dalton/basis/                              


    $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    $$$$$$$$$$$  DALTON - An electronic structure program  $$$$$$$$$$$
    $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

               This is output from DALTON (beta-version 0.9) 

                          Principal authors:

            Trygve Helgaker,     University of Oslo,        Norway 
            Hans Joergen Jensen, University of Odense,      Denmark
            Poul Joergensen,     University of Aarhus,      Denmark
            Henrik Koch,         University of Aarhus,      Denmark
            Jeppe Olsen,         University of Lund,        Sweden 
            Hans Aagren,         University of Linkoeping,  Sweden 

                          Contributors:

            Torgeir Andersen,    University of Oslo,        Norway 
            Keld L. Bak,         University of Copenhagen,  Denmark
            Vebjoern Bakken,     University of Oslo,        Norway 
            Ove Christiansen,    University of Aarhus,      Denmark
            Paal Dahle,          University of Oslo,        Norway 
            Erik K. Dalskov,     University of Odense,      Denmark
            Thomas Enevoldsen,   University of Odense,      Denmark
            Asger Halkier,       University of Aarhus,      Denmark
            Hanne Heiberg,       University of Oslo,        Norway 
            Dan Jonsson,         University of Linkoeping,  Sweden 
            Sheela Kirpekar,     University of Odense,      Denmark
            Rika Kobayashi,      University of Aarhus,      Denmark
            Alfredo S. de Meras, Valencia University,       Spain  
            Kurt Mikkelsen,      University of Aarhus,      Denmark
            Patrick Norman,      University of Linkoeping,  Sweden 
            Martin J. Packer,    University of Sheffield,   UK     
            Kenneth Ruud,        University of Oslo,        Norway 
            Trond Saue,          University of Oslo,        Norway 
            Peter Taylor,        San Diego Superc. Center,  USA    
            Olav Vahtras,        University of Linkoeping,  Sweden

                                             Release Date:  August 1996
------------------------------------------------------------------------


      
     NOTE:
      
     This is an experimental code for the evaluation of molecular
     properties using (MC)SCF/CC wave functions. The authors accept
      no responsibility for the performance of the code or for the
     correctness of the results.
      
     The code (in whole or part) is not to be reproduced for further
     distribution without the written permission of T. Helgaker,
     H. J. Aa. Jensen or P. Taylor.
      
     If results obtained with this code are published, an
     appropriate citation would be:
      
     T. Helgaker, H. J. Aa. Jensen, P.Joergensen, H. Koch,
     J. Olsen, H. Aagren, T. Andersen, K. L. Bak, V. Bakken,
     O. Christiansen, P. Dahle, E. K. Dalskov, T. Enevoldsen,
     A. Halkier, H. Heiberg, D. Jonsson, S. Kirpekar, R. Kobayashi,
     A. S. de Meras, K. V. Mikkelsen, P. Norman, M. J. Packer,
     K. Ruud, T.Saue, P. R. Taylor, and O. Vahtras:
     DALTON, an electronic structure program"



     ******************************************
     **    PROGRAM:              DALTON      **
     **    PROGRAM VERSION:      5.4.0.0     **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************



 <<<<<<<<<< OUTPUT FROM GENERAL INPUT PROCESSING >>>>>>>>>>




 Default print level:        0

    Integral sections will be executed
    Starting in Integral Section -



 *************************************************************************
 ****************** Output from HERMIT input processing ******************
 *************************************************************************



 Default print level:        2


 Calculation of one- and two-electron Hamiltonian integrals.


 The following one-electron property integrals are calculated:

          - overlap integrals
          - Cartesian multipole moment integrals of orders 4 and lower
          - electronic angular momentum around the origin


 Changes of defaults for READIN:
 -------------------------------


 Maximum number of primitives per integral block :   10



 *************************************************************************
 ****************** Output from READIN input processing ******************
 *************************************************************************



  Title Cards
  -----------

                                                                          
                                                                          


  Symmetry Operations
  -------------------

  Symmetry operations: 2



                      SYMGRP:Point group information
                      ------------------------------

Point group: C2v

   * The point group was generated by:

      Reflection in the yz-plane
      Reflection in the xz-plane

   * Group multiplication table

        |  E   C2z  Oxz  Oyz
   -----+--------------------
     E  |  E 
    C2z | C2z   E 
    Oxz | Oxz  Oyz   E 
    Oyz | Oyz  Oxz  C2z   E 

   * Character table

        |  E   C2z  Oxz  Oyz
   -----+--------------------
    A1  |   1    1    1    1
    B1  |   1   -1    1   -1
    B2  |   1   -1   -1    1
    A2  |   1    1   -1   -1

   * Direct product table

        | A1   B1   B2   A2 
   -----+--------------------
    A1  | A1 
    B1  | B1   A1 
    B2  | B2   A2   A1 
    A2  | A2   B2   B1   A1 


  Atoms and basis sets
  --------------------

  Number of atom types:     3
  Total number of atoms:    4

  label    atoms   charge   prim    cont     basis   
  ----------------------------------------------------------------------
  O  1        1       8      42      30      [10s5p2d1f|4s3p2d1f]                   
  C  1        1       6      42      30      [10s5p2d1f|4s3p2d1f]                   
  H  1        2       1      16      14      [5s2p1d|3s2p1d]                        
  ----------------------------------------------------------------------
  ----------------------------------------------------------------------
  total:      4      16     116      88

  Spherical harmonic basis used.
  Threshold for integrals:  1.00E-15


  Cartesian Coordinates
  ---------------------

  Total number of coordinates: 12


   1   O  1     x      0.0000000000
   2            y      0.0000000000
   3            z     -2.2827900700

   4   C  1     x      0.0000000000
   5            y      0.0000000000
   6            z      0.0000000000

   7   H  1 1   x      0.0000000000
   8            y      1.7933377400
   9            z      1.1097510600

  10   H  1 2   x      0.0000000000
  11            y     -1.7933377400
  12            z      1.1097510600



  Symmetry Coordinates
  --------------------

  Number of coordinates in each symmetry:   4  3  4  1


  Symmetry 1

   1   O  1  z    3
   2   C  1  z    6
   3   H  1  y    [ 8  - 11 ]/2
   4   H  1  z    [ 9  + 12 ]/2


  Symmetry 2

   5   O  1  x    1
   6   C  1  x    4
   7   H  1  x    [ 7  + 10 ]/2


  Symmetry 3

   8   O  1  y    2
   9   C  1  y    5
  10   H  1  y    [ 8  + 11 ]/2
  11   H  1  z    [ 9  - 12 ]/2


  Symmetry 4

  12   H  1  x    [ 7  - 10 ]/2


   Interatomic separations (in Angstroms):
   ---------------------------------------

            O  1        C  1        H  1        H  2

   O  1    0.000000
   C  1    1.208000    0.000000
   H  1    2.030647    1.116000    0.000000
   H  2    2.030647    1.116000    1.897986    0.000000




  Bond distances (angstroms):
  ---------------------------

                  atom 1     atom 2                           distance
                  ------     ------                           --------
  bond distance:    C  1       O  1                           1.208000
  bond distance:    H  1       C  1                           1.116000
  bond distance:    H  2       C  1                           1.116000


  Bond angles (degrees):
  ----------------------

                  atom 1     atom 2     atom 3                   angle
                  ------     ------     ------                   -----
  bond angle:       H  1       C  1       O  1                 121.750
  bond angle:       H  2       C  1       O  1                 121.750
  bond angle:       H  2       C  1       H  1                 116.500


  Nuclear repulsion energy :   31.165309824426


  Orbital exponents and contraction coefficients
  ----------------------------------------------


  O  1   1s    1    15330.000000    0.0005 -0.0001  0.0000  0.0000
   gen. cont.  2     2299.000000    0.0039 -0.0009  0.0000  0.0000
               3      522.400000    0.0202 -0.0046  0.0000  0.0000
               4      147.300000    0.0792 -0.0187  0.0000  0.0000
               5       47.550000    0.2307 -0.0585  0.0000  0.0000
               6       16.760000    0.4331 -0.1365  0.0000  0.0000
               7        6.207000    0.3503 -0.1757  0.0000  0.0000
               8        1.752000    0.0427  0.1609  1.0000  0.0000
               9        0.688200   -0.0082  0.6034  0.0000  0.0000
              10        0.238400    0.0024  0.3788  0.0000  1.0000

  O  1   2px  11       34.460000    0.0159  0.0000  0.0000
   gen. cont. 12        7.749000    0.0997  0.0000  0.0000
              13        2.280000    0.3105  0.0000  0.0000
              14        0.715600    0.4910  1.0000  0.0000
              15        0.214000    0.3363  0.0000  1.0000

  O  1   2py  16       34.460000    0.0159  0.0000  0.0000
   gen. cont. 17        7.749000    0.0997  0.0000  0.0000
              18        2.280000    0.3105  0.0000  0.0000
              19        0.715600    0.4910  1.0000  0.0000
              20        0.214000    0.3363  0.0000  1.0000

  O  1   2pz  21       34.460000    0.0159  0.0000  0.0000
   gen. cont. 22        7.749000    0.0997  0.0000  0.0000
              23        2.280000    0.3105  0.0000  0.0000
              24        0.715600    0.4910  1.0000  0.0000
              25        0.214000    0.3363  0.0000  1.0000

  O  1   3d2- 26        2.314000    1.0000  0.0000
   seg. cont. 27        0.645000    0.0000  1.0000

  O  1   3d1- 28        2.314000    1.0000  0.0000
   seg. cont. 29        0.645000    0.0000  1.0000

  O  1   3d0  30        2.314000    1.0000  0.0000
   seg. cont. 31        0.645000    0.0000  1.0000

  O  1   3d1+ 32        2.314000    1.0000  0.0000
   seg. cont. 33        0.645000    0.0000  1.0000

  O  1   3d2+ 34        2.314000    1.0000  0.0000
   seg. cont. 35        0.645000    0.0000  1.0000

  O  1   4f3- 36        1.428000    1.0000

  O  1   4f2- 37        1.428000    1.0000

  O  1   4f1- 38        1.428000    1.0000

  O  1   4f0  39        1.428000    1.0000

  O  1   4f1+ 40        1.428000    1.0000

  O  1   4f2+ 41        1.428000    1.0000

  O  1   4f3+ 42        1.428000    1.0000

  C  1   1s   43     8236.000000    0.0005 -0.0001  0.0000  0.0000
   gen. cont. 44     1235.000000    0.0041 -0.0009  0.0000  0.0000
              45      280.800000    0.0211 -0.0045  0.0000  0.0000
              46       79.270000    0.0819 -0.0181  0.0000  0.0000
              47       25.590000    0.2348 -0.0558  0.0000  0.0000
              48        8.997000    0.4344 -0.1269  0.0000  0.0000
              49        3.319000    0.3461 -0.1704  0.0000  0.0000
              50        0.905900    0.0394  0.1404  1.0000  0.0000
              51        0.364300   -0.0090  0.5987  0.0000  0.0000
              52        0.128500    0.0024  0.3954  0.0000  1.0000

  C  1   2px  53       18.710000    0.0140  0.0000  0.0000
   gen. cont. 54        4.133000    0.0869  0.0000  0.0000
              55        1.200000    0.2902  0.0000  0.0000
              56        0.382700    0.5010  1.0000  0.0000
              57        0.120900    0.3434  0.0000  1.0000

  C  1   2py  58       18.710000    0.0140  0.0000  0.0000
   gen. cont. 59        4.133000    0.0869  0.0000  0.0000
              60        1.200000    0.2902  0.0000  0.0000
              61        0.382700    0.5010  1.0000  0.0000
              62        0.120900    0.3434  0.0000  1.0000

  C  1   2pz  63       18.710000    0.0140  0.0000  0.0000
   gen. cont. 64        4.133000    0.0869  0.0000  0.0000
              65        1.200000    0.2902  0.0000  0.0000
              66        0.382700    0.5010  1.0000  0.0000
              67        0.120900    0.3434  0.0000  1.0000

  C  1   3d2- 68        1.097000    1.0000  0.0000
   seg. cont. 69        0.318000    0.0000  1.0000

  C  1   3d1- 70        1.097000    1.0000  0.0000
   seg. cont. 71        0.318000    0.0000  1.0000

  C  1   3d0  72        1.097000    1.0000  0.0000
   seg. cont. 73        0.318000    0.0000  1.0000

  C  1   3d1+ 74        1.097000    1.0000  0.0000
   seg. cont. 75        0.318000    0.0000  1.0000

  C  1   3d2+ 76        1.097000    1.0000  0.0000
   seg. cont. 77        0.318000    0.0000  1.0000

  C  1   4f3- 78        0.761000    1.0000

  C  1   4f2- 79        0.761000    1.0000

  C  1   4f1- 80        0.761000    1.0000

  C  1   4f0  81        0.761000    1.0000

  C  1   4f1+ 82        0.761000    1.0000

  C  1   4f2+ 83        0.761000    1.0000

  C  1   4f3+ 84        0.761000    1.0000

  H  1#1 1s   85       33.870000    0.0061  0.0000  0.0000
   gen. cont. 86        5.095000    0.0453  0.0000  0.0000
              87        1.159000    0.2028  0.0000  0.0000
              88        0.325800    0.5039  1.0000  0.0000
              89        0.102700    0.3834  0.0000  1.0000

  H  1#2 1s   90       33.870000    0.0061  0.0000  0.0000
   gen. cont. 91        5.095000    0.0453  0.0000  0.0000
              92        1.159000    0.2028  0.0000  0.0000
              93        0.325800    0.5039  1.0000  0.0000
              94        0.102700    0.3834  0.0000  1.0000

  H  1#1 2px  95        1.407000    1.0000  0.0000
   seg. cont. 96        0.388000    0.0000  1.0000

  H  1#2 2px  97        1.407000    1.0000  0.0000
   seg. cont. 98        0.388000    0.0000  1.0000

  H  1#1 2py  99        1.407000    1.0000  0.0000
   seg. cont.100        0.388000    0.0000  1.0000

  H  1#2 2py 101        1.407000    1.0000  0.0000
   seg. cont.102        0.388000    0.0000  1.0000

  H  1#1 2pz 103        1.407000    1.0000  0.0000
   seg. cont.104        0.388000    0.0000  1.0000

  H  1#2 2pz 105        1.407000    1.0000  0.0000
   seg. cont.106        0.388000    0.0000  1.0000

  H  1#1 3d2-107        1.057000    1.0000

  H  1#2 3d2-108        1.057000    1.0000

  H  1#1 3d1-109        1.057000    1.0000

  H  1#2 3d1-110        1.057000    1.0000

  H  1#1 3d0 111        1.057000    1.0000

  H  1#2 3d0 112        1.057000    1.0000

  H  1#1 3d1+113        1.057000    1.0000

  H  1#2 3d1+114        1.057000    1.0000

  H  1#1 3d2+115        1.057000    1.0000

  H  1#2 3d2+116        1.057000    1.0000


  Contracted Orbitals
  -------------------

   1  O  1    1s       1     2     3     4     5     6     7     8     9    10
   2  O  1    1s       1     2     3     4     5     6     7     8     9    10
   3  O  1    1s       8
   4  O  1    1s      10
   5  O  1    2px     11    12    13    14    15
   6  O  1    2py     16    17    18    19    20
   7  O  1    2pz     21    22    23    24    25
   8  O  1    2px     14
   9  O  1    2py     19
  10  O  1    2pz     24
  11  O  1    2px     15
  12  O  1    2py     20
  13  O  1    2pz     25
  14  O  1    3d2-    26
  15  O  1    3d1-    28
  16  O  1    3d0     30
  17  O  1    3d1+    32
  18  O  1    3d2+    34
  19  O  1    3d2-    27
  20  O  1    3d1-    29
  21  O  1    3d0     31
  22  O  1    3d1+    33
  23  O  1    3d2+    35
  24  O  1    4f3-    36
  25  O  1    4f2-    37
  26  O  1    4f1-    38
  27  O  1    4f0     39
  28  O  1    4f1+    40
  29  O  1    4f2+    41
  30  O  1    4f3+    42
  31  C  1    1s      43    44    45    46    47    48    49    50    51    52
  32  C  1    1s      43    44    45    46    47    48    49    50    51    52
  33  C  1    1s      50
  34  C  1    1s      52
  35  C  1    2px     53    54    55    56    57
  36  C  1    2py     58    59    60    61    62
  37  C  1    2pz     63    64    65    66    67
  38  C  1    2px     56
  39  C  1    2py     61
  40  C  1    2pz     66
  41  C  1    2px     57
  42  C  1    2py     62
  43  C  1    2pz     67
  44  C  1    3d2-    68
  45  C  1    3d1-    70
  46  C  1    3d0     72
  47  C  1    3d1+    74
  48  C  1    3d2+    76
  49  C  1    3d2-    69
  50  C  1    3d1-    71
  51  C  1    3d0     73
  52  C  1    3d1+    75
  53  C  1    3d2+    77
  54  C  1    4f3-    78
  55  C  1    4f2-    79
  56  C  1    4f1-    80
  57  C  1    4f0     81
  58  C  1    4f1+    82
  59  C  1    4f2+    83
  60  C  1    4f3+    84
  61  H  1#1  1s    85  86  87  88  89
  62  H  1#2  1s    90  91  92  93  94
  63  H  1#1  1s    88
  64  H  1#2  1s    93
  65  H  1#1  1s    89
  66  H  1#2  1s    94
  67  H  1#1  2px   95
  68  H  1#2  2px   97
  69  H  1#1  2py   99
  70  H  1#2  2py  101
  71  H  1#1  2pz  103
  72  H  1#2  2pz  105
  73  H  1#1  2px   96
  74  H  1#2  2px   98
  75  H  1#1  2py  100
  76  H  1#2  2py  102
  77  H  1#1  2pz  104
  78  H  1#2  2pz  106
  79  H  1#1  3d2- 107
  80  H  1#2  3d2- 108
  81  H  1#1  3d1- 109
  82  H  1#2  3d1- 110
  83  H  1#1  3d0  111
  84  H  1#2  3d0  112
  85  H  1#1  3d1+ 113
  86  H  1#2  3d1+ 114
  87  H  1#1  3d2+ 115
  88  H  1#2  3d2+ 116




  Symmetry Orbitals
  -----------------

  Number of orbitals in each symmetry:        36 18 24 10


  Symmetry  A1 ( 1)

    1     O  1     1s         1
    2     O  1     1s         2
    3     O  1     1s         3
    4     O  1     1s         4
    5     O  1     2pz        7
    6     O  1     2pz       10
    7     O  1     2pz       13
    8     O  1     3d0       16
    9     O  1     3d2+      18
   10     O  1     3d0       21
   11     O  1     3d2+      23
   12     O  1     4f0       27
   13     O  1     4f2+      29
   14     C  1     1s        31
   15     C  1     1s        32
   16     C  1     1s        33
   17     C  1     1s        34
   18     C  1     2pz       37
   19     C  1     2pz       40
   20     C  1     2pz       43
   21     C  1     3d0       46
   22     C  1     3d2+      48
   23     C  1     3d0       51
   24     C  1     3d2+      53
   25     C  1     4f0       57
   26     C  1     4f2+      59
   27     H  1     1s        61  +  62
   28     H  1     1s        63  +  64
   29     H  1     1s        65  +  66
   30     H  1     2py       69  -  70
   31     H  1     2pz       71  +  72
   32     H  1     2py       75  -  76
   33     H  1     2pz       77  +  78
   34     H  1     3d1-      81  -  82
   35     H  1     3d0       83  +  84
   36     H  1     3d2+      87  +  88


  Symmetry  B1 ( 2)

   37     O  1     2px        5
   38     O  1     2px        8
   39     O  1     2px       11
   40     O  1     3d1+      17
   41     O  1     3d1+      22
   42     O  1     4f1+      28
   43     O  1     4f3+      30
   44     C  1     2px       35
   45     C  1     2px       38
   46     C  1     2px       41
   47     C  1     3d1+      47
   48     C  1     3d1+      52
   49     C  1     4f1+      58
   50     C  1     4f3+      60
   51     H  1     2px       67  +  68
   52     H  1     2px       73  +  74
   53     H  1     3d2-      79  -  80
   54     H  1     3d1+      85  +  86


  Symmetry  B2 ( 3)

   55     O  1     2py        6
   56     O  1     2py        9
   57     O  1     2py       12
   58     O  1     3d1-      15
   59     O  1     3d1-      20
   60     O  1     4f3-      24
   61     O  1     4f1-      26
   62     C  1     2py       36
   63     C  1     2py       39
   64     C  1     2py       42
   65     C  1     3d1-      45
   66     C  1     3d1-      50
   67     C  1     4f3-      54
   68     C  1     4f1-      56
   69     H  1     1s        61  -  62
   70     H  1     1s        63  -  64
   71     H  1     1s        65  -  66
   72     H  1     2py       69  +  70
   73     H  1     2pz       71  -  72
   74     H  1     2py       75  +  76
   75     H  1     2pz       77  -  78
   76     H  1     3d1-      81  +  82
   77     H  1     3d0       83  -  84
   78     H  1     3d2+      87  -  88


  Symmetry  A2 ( 4)

   79     O  1     3d2-      14
   80     O  1     3d2-      19
   81     O  1     4f2-      25
   82     C  1     3d2-      44
   83     C  1     3d2-      49
   84     C  1     4f2-      55
   85     H  1     2px       67  -  68
   86     H  1     2px       73  -  74
   87     H  1     3d2-      79  +  80
   88     H  1     3d1+      85  -  86

  Symmetries of electric field:  B1 (2)  B2 (3)  A1 (1)

  Symmetries of magnetic field:  B2 (3)  B1 (2)  A2 (4)


 Copy of input to READIN
 -----------------------

INTGRL                                                                          
                                                                                
                                                                                
s   3    2X   Y      0.10E-14                                                   
       8.0    1    4    1    1    1    1                                        
O  1   0.000000000000000   0.000000000000000  -2.282790070000000       *        
H  10   4                                                                       
      15330.00000000         0.00050800        -0.00011500         0.00000000   
                             0.00000000                                         
       2299.00000000         0.00392900        -0.00089500         0.00000000   
                             0.00000000                                         
        522.40000000         0.02024300        -0.00463600         0.00000000   
                             0.00000000                                         
        147.30000000         0.07918100        -0.01872400         0.00000000   
                             0.00000000                                         
         47.55000000         0.23068700        -0.05846300         0.00000000   
                             0.00000000                                         
         16.76000000         0.43311800        -0.13646300         0.00000000   
                             0.00000000                                         
          6.20700000         0.35026000        -0.17574000         0.00000000   
                             0.00000000                                         
          1.75200000         0.04272800         0.16093400         1.00000000   
                             0.00000000                                         
          0.68820000        -0.00815400         0.60341800         0.00000000   
                             0.00000000                                         
          0.23840000         0.00238100         0.37876500         0.00000000   
                             1.00000000                                         
H   5   3                                                                       
         34.46000000         0.01592800         0.00000000         0.00000000   
          7.74900000         0.09974000         0.00000000         0.00000000   
          2.28000000         0.31049200         0.00000000         0.00000000   
          0.71560000         0.49102600         1.00000000         0.00000000   
          0.21400000         0.33633700         0.00000000         1.00000000   
H   2   2                                                                       
          2.31400000         1.00000000         0.00000000                      
          0.64500000         0.00000000         1.00000000                      
H   1   1                                                                       
          1.42800000         1.00000000                                         
       6.0    1    4    1    1    1    1                                        
C  1   0.000000000000000   0.000000000000000   0.000000000000000       *        
H  10   4                                                                       
       8236.00000000         0.00053100        -0.00011300         0.00000000   
                             0.00000000                                         
       1235.00000000         0.00410800        -0.00087800         0.00000000   
                             0.00000000                                         
        280.80000000         0.02108700        -0.00454000         0.00000000   
                             0.00000000                                         
         79.27000000         0.08185300        -0.01813300         0.00000000   
                             0.00000000                                         
         25.59000000         0.23481700        -0.05576000         0.00000000   
                             0.00000000                                         
          8.99700000         0.43440100        -0.12689500         0.00000000   
                             0.00000000                                         
          3.31900000         0.34612900        -0.17035200         0.00000000   
                             0.00000000                                         
          0.90590000         0.03937800         0.14038200         1.00000000   
                             0.00000000                                         
          0.36430000        -0.00898300         0.59868400         0.00000000   
                             0.00000000                                         
          0.12850000         0.00238500         0.39538900         0.00000000   
                             1.00000000                                         
H   5   3                                                                       
         18.71000000         0.01403100         0.00000000         0.00000000   
          4.13300000         0.08686600         0.00000000         0.00000000   
          1.20000000         0.29021600         0.00000000         0.00000000   
          0.38270000         0.50100800         1.00000000         0.00000000   
          0.12090000         0.34340600         0.00000000         1.00000000   
H   2   2                                                                       
          1.09700000         1.00000000         0.00000000                      
          0.31800000         0.00000000         1.00000000                      
H   1   1                                                                       
          0.76100000         1.00000000                                         
       1.0    1    3    1    1    1                                             
H  1   0.000000000000000   1.793337740000000   1.109751060000000       *        
H   5   3                                                                       
         33.87000000         0.00606800         0.00000000         0.00000000   
          5.09500000         0.04530800         0.00000000         0.00000000   
          1.15900000         0.20282200         0.00000000         0.00000000   
          0.32580000         0.50390300         1.00000000         0.00000000   
          0.10270000         0.38342100         0.00000000         1.00000000   
H   2   2                                                                       
          1.40700000         1.00000000         0.00000000                      
          0.38800000         0.00000000         1.00000000                      
H   1   1                                                                       
          1.05700000         1.00000000                                         


 herdrv: noofopt= 5


 ************************************************************************
 ************************** Output from HERONE **************************
 ************************************************************************

 prop, itype T 1


   854 atomic overlap integrals written in   1 buffers.
 Percentage non-zero integrals:  21.81
 prop, itype T 2


  1192 one-el. Hamil. integrals written in   1 buffers.
 Percentage non-zero integrals:  30.44
 prop, itype T 3


   864 kinetic energy integrals written in   1 buffers.
 Percentage non-zero integrals:  22.06


 inttyp= 1noptyp= 1


                    +---------------------------------+
                    ! Integrals of operator: OVERLAP  !
                    +---------------------------------+



 finopt,noofopt,last1= 0 5 0
 inttyp= 8noptyp= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000000 !
                    +---------------------------------+



 finopt,noofopt,last1= 1 5 0
 inttyp= 8noptyp= 3


                    +---------------------------------+
                    ! Integrals of operator: CM010000 !
                    +---------------------------------+

 typea=  1typeb=  0last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000100 !
                    +---------------------------------+

 typea=  1typeb=  1last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000001 !
                    +---------------------------------+

 typea=  1typeb=  2last= 2
 lstflg= 1


 finopt,noofopt,last1= 2 5 0
 inttyp= 8noptyp= 6


                    +---------------------------------+
                    ! Integrals of operator: CM020000 !
                    +---------------------------------+

 typea=  1typeb=  3last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM010100 !
                    +---------------------------------+

 typea=  1typeb=  4last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM010001 !
                    +---------------------------------+

 typea=  1typeb=  5last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000200 !
                    +---------------------------------+

 typea=  1typeb=  6last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000101 !
                    +---------------------------------+

 typea=  1typeb=  7last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000002 !
                    +---------------------------------+

 typea=  1typeb=  8last= 2
 lstflg= 1


 finopt,noofopt,last1= 3 5 0
 inttyp= 8noptyp= 10


                    +---------------------------------+
                    ! Integrals of operator: CM030000 !
                    +---------------------------------+

 typea=  1typeb=  9last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM020100 !
                    +---------------------------------+

 typea=  1typeb=  10last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM020001 !
                    +---------------------------------+

 typea=  1typeb=  11last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM010200 !
                    +---------------------------------+

 typea=  1typeb=  12last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM010101 !
                    +---------------------------------+

 typea=  1typeb=  13last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM010002 !
                    +---------------------------------+

 typea=  1typeb=  14last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000300 !
                    +---------------------------------+

 typea=  1typeb=  15last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000201 !
                    +---------------------------------+

 typea=  1typeb=  16last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000102 !
                    +---------------------------------+

 typea=  1typeb=  17last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000003 !
                    +---------------------------------+

 typea=  1typeb=  18last= 2
 lstflg= 1


 finopt,noofopt,last1= 4 5 0
 inttyp= 8noptyp= 15


                    +---------------------------------+
                    ! Integrals of operator: CM040000 !
                    +---------------------------------+

 typea=  1typeb=  19last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM030100 !
                    +---------------------------------+

 typea=  1typeb=  20last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM030001 !
                    +---------------------------------+

 typea=  1typeb=  21last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM020200 !
                    +---------------------------------+

 typea=  1typeb=  22last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM020101 !
                    +---------------------------------+

 typea=  1typeb=  23last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM020002 !
                    +---------------------------------+

 typea=  1typeb=  24last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM010300 !
                    +---------------------------------+

 typea=  1typeb=  25last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM010201 !
                    +---------------------------------+

 typea=  1typeb=  26last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM010102 !
                    +---------------------------------+

 typea=  1typeb=  27last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM010003 !
                    +---------------------------------+

 typea=  1typeb=  28last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000400 !
                    +---------------------------------+

 typea=  1typeb=  29last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000301 !
                    +---------------------------------+

 typea=  1typeb=  30last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000202 !
                    +---------------------------------+

 typea=  1typeb=  31last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000103 !
                    +---------------------------------+

 typea=  1typeb=  32last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: CM000004 !
                    +---------------------------------+

 typea=  1typeb=  33last= 2
 lstflg= 1


 finopt,noofopt,last1= 5 5 2
 inttyp= 18noptyp= 3


                    +---------------------------------+
                    ! Integrals of operator: XANGMOM  !
                    +---------------------------------+

 angular moment
 typea=  2typeb=  6last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: YANGMOM  !
                    +---------------------------------+

 angular moment
 typea=  2typeb=  7last= 1
 lstflg= 1


                    +---------------------------------+
                    ! Integrals of operator: ZANGMOM  !
                    +---------------------------------+

 angular moment
 typea=  2typeb=  8last= 2
 lstflg= 2




 ************************************************************************
 ************************** Output from TWOINT **************************
 ************************************************************************

 calling sifew2:luinta,info,num,last,nrec
 calling sifew2: 11 2 4096 3272 4096 2730 1987 2 680

 Number of two-electron integrals written:   1858387 (24.2%)
 Kilobytes written:                            22313




 >>>> Total CPU  time used in HERMIT:   5.88 seconds
 >>>> Total wall time used in HERMIT:   6.00 seconds

- End of Integral Section
