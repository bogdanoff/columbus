1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 integral file titles:
 Hermit Integral Program : SIFS version  hochtor2        Thu Oct  7 14:50:04 2004
  cidrt_title                                                                    
  title                                                                          
 mofmt: formatted orbitals label=morbl   hochtor2        Thu Oct  7 14:50:31 2004
 SIFS file created by program tran.      hochtor2        Thu Oct  7 14:50:31 2004

 formula file title:
 Hermit Integral Program : SIFS version  hochtor2        Thu Oct  7 14:50:04 2004
  cidrt_title                                                                    
  title                                                                          
 mofmt: formatted orbitals label=morbl   hochtor2        Thu Oct  7 14:50:31 2004
 SIFS file created by program tran.      hochtor2        Thu Oct  7 14:50:31 2004
  cidrt_title                                                                    

 297 dimension of the ci-matrix ->>>    108301


 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1   -113.9254567137 -3.1140E+01  5.2476E-01  1.7659E+00  1.0000E-03
 mr-sdci #  2  1   -114.3034332381 -3.0855E+01  3.1798E-02  4.5418E-01  1.0000E-03
 mr-sdci #  3  1   -114.3297399657 -3.1207E+01  1.4405E-03  9.0741E-02  1.0000E-03
 mr-sdci #  4  1   -114.3310226473 -3.1232E+01  2.2574E-04  3.7426E-02  1.0000E-03
 mr-sdci #  5  1   -114.3312363478 -3.1233E+01  4.3016E-05  1.7203E-02  1.0000E-03
 mr-sdci #  6  1   -114.3312851747 -3.1233E+01  7.7436E-06  6.5454E-03  1.0000E-03
 mr-sdci #  7  1   -114.3292961681 -3.1235E+01  1.5179E-05  5.2428E-03  1.0000E-03
 mr-sdci #  8  1   -114.3293115416 -3.1225E+01  2.4619E-06  3.0403E-03  1.0000E-03
 mr-sdci #  9  1   -114.3293137546 -3.1225E+01  2.1892E-07  9.7570E-04  1.0000E-03

 mr-sdci  convergence criteria satisfied after  9 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  9  1   -114.3293137546 -3.1225E+01  2.1892E-07  9.7570E-04  1.0000E-03

 number of reference csfs (nref) is     2.  root number (iroot) is  1.

 eref      =   -113.923195366185   "relaxed" cnot**2         =   0.906632480655
 eci       =   -114.329313754640   deltae = eci - eref       =  -0.406118388454
 eci+dv1   =   -114.367232021130   dv1 = (1-cnot**2)*deltae  =  -0.037918266490
 eci+dv2   =   -114.371136949042   dv2 = dv1 / cnot**2       =  -0.041823194403
 eci+dv3   =   -114.375938493555   dv3 = dv1 / (2*cnot**2-1) =  -0.046624738916
 eci+pople =   -114.368918268781   ( 16e- scaled deltae )    =  -0.445722902596
