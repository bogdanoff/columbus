 total ao core energy =    9.317913917
 MCSCF calculation performed for  1 DRT.

 DRT  first state   no.of aver. states   weights
  1   ground state          1             1.000

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a  
 Total number of electrons:    10
 Spin multiplicity:            1
 Number of active orbitals:    0
 Number of active electrons:   0
 Total number of CSFs:         1

 Number of active-double rotations:      0
 Number of active-active rotations:      0
 Number of double-virtual rotations:    95
 Number of active-virtual rotations:     0

 iter=    1 emc=  -76.0270328082 demc= 7.6027E+01 wnorm= 3.7760E-09 knorm= 3.9102E-10 apxde= 2.4061E-16    *not converged* 

 final mcscf convergence values:
 iter=    2 emc=  -76.0270328082 demc= 7.1054E-14 wnorm= 4.6465E-09 knorm= 6.5100E-11 apxde=-2.8282E-15    *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 weight 1.000000 total energy=  -76.027032808
   ------------------------------------------------------------


