1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -76.0479455968  2.0913E-02  2.5188E-01  1.0015E+00  1.0000E-04
 mr-sdci #  2  1    -76.2462585708  1.9831E-01  6.4774E-03  1.5760E-01  1.0000E-04
 mr-sdci #  3  1    -76.2535668738  7.3083E-03  5.4631E-04  3.4900E-02  1.0000E-04
 mr-sdci #  4  1    -76.2533746201 -1.9225E-04  7.9397E-05  1.6304E-02  1.0000E-04
 mr-sdci #  5  1    -76.2536306957  2.5608E-04  2.3011E-05  7.1128E-03  1.0000E-04
 mr-sdci #  6  1    -76.2536153391 -1.5357E-05  4.5589E-06  3.7742E-03  1.0000E-04
 mr-sdci #  7  1    -76.2536324369  1.7098E-05  1.4294E-06  1.7508E-03  1.0000E-04
 mr-sdci #  8  1    -76.2536282771 -4.1598E-06  2.9676E-07  9.4770E-04  1.0000E-04
 mr-sdci #  9  1    -76.2536315146  3.2375E-06  9.6709E-08  4.5463E-04  1.0000E-04
 mr-sdci # 10  1    -76.2536304457 -1.0689E-06  2.0629E-08  2.4627E-04  1.0000E-04
 mr-sdci # 11  1    -76.2536313187  8.7294E-07  6.8960E-09  1.2148E-04  1.0000E-04
 mr-sdci # 12  1    -76.2536311060 -2.1267E-07  1.5005E-09  6.5529E-05  1.0000E-04

 mr-sdci  convergence criteria satisfied after 12 iterations.

 final mr-sdci  convergence information:
 mr-sdci # 12  1    -76.2536311060 -2.1267E-07  1.5005E-09  6.5529E-05  1.0000E-04

 number of reference csfs (nref) is     1.  root number (iroot) is  1.

 eref      =    -76.049079437753   "relaxed" cnot**2         =   0.951689386571
 eci       =    -76.253631106007   deltae = eci - eref       =  -0.204551668253
 eci+dv1   =    -76.263513122578   dv1 = (1-cnot**2)*deltae  =  -0.009882016571
 eci+dv2   =    -76.264014763438   dv2 = dv1 / cnot**2       =  -0.010383657431
 eci+dv3   =    -76.264570057491   dv3 = dv1 / (2*cnot**2-1) =  -0.010938951484
 eci+pople =    -76.262195236214   ( 10e- scaled deltae )    =  -0.213115798460
