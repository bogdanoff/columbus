1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.

 This is a AQCC calculation USING the mrci-code /  March 1997 (tm)
 =============================================
 ==========================================

 integral file titles:
 Hermit Integral Program : SIFS version  hochtor2        Thu Jun  5 14:26:35 2003
 /                                                                               
 Hermit Integral Program : SIFS version  niko            Fri Apr 16 12:09:51 1999
 mdrt2_title                                                                     
 mofmt: formatted orbitals label=morb    niko            Fri Apr 16 12:11:03 1999
 SIFS file created by program tran.      hochtor2        Thu Jun  5 14:26:35 2003

 formula file title:
 Hermit Integral Program : SIFS version  hochtor2        Thu Jun  5 14:26:35 2003
 /                                                                               
 Hermit Integral Program : SIFS version  niko            Fri Apr 16 12:09:51 1999
 mdrt2_title                                                                     
 mofmt: formatted orbitals label=morb    niko            Fri Apr 16 12:11:03 1999
 SIFS file created by program tran.      hochtor2        Thu Jun  5 14:26:35 2003
 cidrt_title                                                                     

 297 dimension of the ci-matrix ->>>       280


 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  1  1    -38.5360197176 -1.7764E-15  1.0625E-01  4.3642E-01  1.0000E-06
 mraqcc  #  2  1    -38.6166978443  8.0678E-02  5.2638E-03  1.0154E-01  1.0000E-06
 mraqcc  #  3  1    -38.6231415536  6.4437E-03  6.7053E-04  3.6174E-02  1.0000E-06
 mraqcc  #  4  1    -38.6239720679  8.3051E-04  9.2001E-05  1.3473E-02  1.0000E-06
 mraqcc  #  5  1    -38.6240804519  1.0838E-04  1.2028E-05  4.6663E-03  1.0000E-06
 mraqcc  #  6  1    -38.6240960690  1.5617E-05  2.2263E-06  2.0608E-03  1.0000E-06
 mraqcc  #  7  1    -38.6240993890  3.3200E-06  4.0634E-07  8.8929E-04  1.0000E-06
 mraqcc  #  8  1    -38.6240999280  5.3894E-07  5.6896E-08  3.3651E-04  1.0000E-06
 mraqcc  #  9  1    -38.6240999964  6.8407E-08  5.9022E-09  1.1144E-04  1.0000E-06
 mraqcc  # 10  1    -38.6241000039  7.4943E-09  5.5575E-10  3.5020E-05  1.0000E-06
 mraqcc  # 11  1    -38.6241000046  7.5199E-10  6.8234E-11  1.1642E-05  1.0000E-06
 mraqcc  # 12  1    -38.6241000047  9.2811E-11  7.7999E-12  4.0206E-06  1.0000E-06
 mraqcc  # 13  1    -38.6241000047  9.4431E-12  6.9659E-13  1.1591E-06  1.0000E-06
 mraqcc  # 14  1    -38.6241000047  8.9706E-13  7.6305E-14  3.9529E-07  1.0000E-06

 mraqcc   convergence criteria satisfied after 14 iterations.

 final mraqcc   convergence information:
 mraqcc  # 14  1    -38.6241000047  8.9706E-13  7.6305E-14  3.9529E-07  1.0000E-06
