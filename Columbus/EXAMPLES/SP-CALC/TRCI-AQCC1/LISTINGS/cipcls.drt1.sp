
 program cipc      

 print the csf info for mrsdci wave functions

 written by: ron shepard

 version date: 06-jun-96

 This Version of Program cipc is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 workspace allocation parameters: lencor=   8000000 mem1=1108557832 ifirst=-264082542

 drt header information:
 /                                                                               
 nmot  =    13 niot  =     4 nfct  =     1 nfvt  =     0
 nrow  =    22 nsym  =     4 ssym  =     1 lenbuf=  1600
 spnorb=     F spnodd=     F lxyzir(1:3)= 0 0 0
 nwalk,xbar:         62        7       20       15       20
 nvalwt,nvalw:       56        5       16       15       20
 ncsft:             358
 map(*)=    -1  9 10  1  2  3  4 11  5 12  6  7  8
 mu(*)=      0  0  0  0
 syml(*) =   1  1  2  3
 rmo(*)=     2  3  1  1

 indx01:    56 indices saved in indxv(*)
===================================ROOT # 1===================================

 rdhciv: CI vector file information:
 Hermit Integral Program : SIFS version  hochtor2        Thu Jun  5 14:26:35 2003
 /                                                                               
 Hermit Integral Program : SIFS version  niko            Fri Apr 16 12:09:51 1999
 mdrt2_title                                                                     
 mofmt: formatted orbitals label=morb    niko            Fri Apr 16 12:11:03 1999
 SIFS file created by program tran.      hochtor2        Thu Jun  5 14:26:35 2003
 energy computed by program ciudg.       hochtor2        Thu Jun  5 14:26:37 2003

 lenrec =    4096 lenci =       358 ninfo =  6 nenrgy =  8 ntitle =  7

 Max. overlap with ref vector #        1
 Valid ci vector #        1
 Method:        3       98% overlap
 energy( 1)=  6.203440944703E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -3.413186659445E+01, ietype=    6,   fcore energy of type: H1(*)   
 energy( 3)= -3.872075705652E+01, ietype=-1038,   total energy of type: MR-AQCC 
 energy( 4)=  4.208818816553E-07, ietype=-2055, cnvginf energy of type: CI-Resid
 energy( 5)=  1.072919530998E-12, ietype=-2056, cnvginf energy of type: CI-D.E. 
 energy( 6)=  8.276786097987E-14, ietype=-2057, cnvginf energy of type: CI-ApxDE
 energy( 7)=  1.015026156662E+00, ietype=-1039,   total energy of type: a4den   
 energy( 8)= -3.864500511170E+01, ietype=-1041,   total energy of type: Unknown 
==================================================================================

 space is available for   3997894 coefficients.

 updated histogram parameters:
 csfmn = 0.0000E+00 csfmx = 1.0000E+00 fhist = 5.0000E-01 nhist =  20

 this program will print the csfs generated from
 the drt according to the following print options :

 1) run in batch mode: all valid roots are automatically
    analysed and csf info is printed by default contribution
    threshold 0.01 
 2) run in interactive mode

 input menu number [  0]:
================================================================================
===================================VECTOR # 1===================================
================================================================================


 rdcivnew:      53 coefficients were selected.
 workspace: ncsfmx=     358
 ncsfmx= 358

 histogram parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =     358 ncsft =     358 ncsf =      53
 nhist =  20 fhist = 0.50000

    cmin                cmax        num  '*'=     1 csfs.
 ----------          ----------   ----- ---------|---------|---------|---------|
 5.0000E-01 <= |c| < 1.0000E+00       1 *
 2.5000E-01 <= |c| < 5.0000E-01       0
 1.2500E-01 <= |c| < 2.5000E-01       1 *
 6.2500E-02 <= |c| < 1.2500E-01       2 **
 3.1250E-02 <= |c| < 6.2500E-02       8 ********
 1.5625E-02 <= |c| < 3.1250E-02      20 ********************
 7.8125E-03 <= |c| < 1.5625E-02      21 *********************
 3.9062E-03 <= |c| < 7.8125E-03       0
 1.9531E-03 <= |c| < 3.9062E-03       0
 9.7656E-04 <= |c| < 1.9531E-03       0
 4.8828E-04 <= |c| < 9.7656E-04       0
 2.4414E-04 <= |c| < 4.8828E-04       0
 1.2207E-04 <= |c| < 2.4414E-04       0
 6.1035E-05 <= |c| < 1.2207E-04       0
 3.0518E-05 <= |c| < 6.1035E-05       0
 1.5259E-05 <= |c| < 3.0518E-05       0
 7.6294E-06 <= |c| < 1.5259E-05       0
 3.8147E-06 <= |c| < 7.6294E-06       0
 0.0000E+00 <= |c| < 3.8147E-06       0
                                  ----- ---------|---------|---------|---------|
                  total read =       53 total stored =      53

 from the selected csfs,
 min(|csfvec(:)|) = 1.0148E-02    max(|csfvec(:)|) = 9.6314E-01
 csfs will be printed based on coefficient magnitudes.

 current csfvec(*) selection parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =     358 ncsft =     358 ncsf =      53

 i:slabel(i) =  1:   1  2:   2  3:   3  4:   4

 frozen orbital =    1
 symfc(*)       =    1
 label          =    1
 rmo(*)         =    1

 internal level =    1    2    3    4
 syml(*)        =    1    1    2    3
 label          =    1    1    2    3
 rmo(*)         =    2    3    1    1

 printing selected csfs in sorted order from cmin = 0.01000 to cmax = 1.00000

   indcsf     c     c**2   v  lab:rmo  lab:rmo   step(*)
  ------- -------- ------- - ---- --- ---- --- ------------
        2  0.96314 0.92764 z*                    3303
        3 -0.15765 0.02485 z*                    3033
        4 -0.08332 0.00694 z*                    1233
      158 -0.07998 0.00640 w             3:  2  33300
      168  0.06021 0.00363 w    1:  4    3:  2 123102
        5 -0.05876 0.00345 z*                    0333
      221 -0.05823 0.00339 w    1:  4    3:  2 121302
      211 -0.04597 0.00211 w             3:  2  33003
      202 -0.04362 0.00190 w             1:  5  33003
      309 -0.03624 0.00131 w             1:  4  30303
      253  0.03221 0.00104 w             1:  4  31203
      147 -0.03198 0.00102 w             1:  4  33300
      169 -0.02968 0.00088 w    1:  5    3:  2 123102
       55  0.02724 0.00074 x    1:  5    3:  2 113202
      160 -0.02580 0.00067 w             3:  3  33300
        9  0.02481 0.00062 y             3:  4  13302
      227  0.02474 0.00061 w    1:  6    3:  3 121302
      200 -0.02472 0.00061 w             1:  4  33003
       97  0.02425 0.00059 x    1:  4    1:  5 112203
       54 -0.02392 0.00057 x    1:  4    3:  2 113202
       21  0.02149 0.00046 y             2:  2  13023
       77  0.02117 0.00045 x    1:  4    3:  3 112302
       59  0.02113 0.00045 x    1:  5    3:  3 113202
      173 -0.02113 0.00045 w    1:  5    3:  3 123102
       41  0.02050 0.00042 y             2:  2  11223
       17 -0.01983 0.00039 y             1:  7  13203
      320 -0.01925 0.00037 w             3:  2  30303
      255 -0.01892 0.00036 w             1:  5  31203
       15 -0.01892 0.00036 y             1:  5  13203
        8 -0.01853 0.00034 y             3:  3  13302
      201  0.01846 0.00034 w    1:  4    1:  5 123003
       45  0.01710 0.00029 y             2:  2  10323
       16  0.01395 0.00019 y             1:  6  13203
      191  0.01384 0.00019 w             3:  2  33030
       73  0.01367 0.00019 x    1:  4    3:  2 112302
      152 -0.01353 0.00018 w             1:  6  33300
      254 -0.01334 0.00018 w    1:  4    1:  5 121203
      273  0.01329 0.00018 w    1:  4    3:  2 121032
      265 -0.01322 0.00017 w    3:  2    3:  3 121203
      212  0.01312 0.00017 w    3:  2    3:  3 123003
      174 -0.01258 0.00016 w    1:  6    3:  3 123102
       14  0.01205 0.00015 y             1:  4  13203
      214 -0.01188 0.00014 w    3:  2    3:  4 123003
      222 -0.01188 0.00014 w    1:  5    3:  2 121302
      223 -0.01165 0.00014 w    1:  6    3:  2 121302
      225 -0.01157 0.00013 w    1:  4    3:  3 121302
       26 -0.01153 0.00013 y             1:  4  12303
      178 -0.01121 0.00013 w    1:  6    3:  4 123102
      314 -0.01104 0.00012 w             1:  6  30303
      258  0.01080 0.00012 w             1:  6  31203
      231  0.01071 0.00011 w    1:  6    3:  4 121302
      342  0.01046 0.00011 w             1:  4  30033
        1 -0.01015 0.00010 z*                    3330
           53 csfs were printed in this range.
