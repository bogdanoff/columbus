1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.

 This is a AQCC calculation USING the mrci-code /  March 1997 (tm)
 =============================================
 ==========================================

 integral file titles:
 Hermit Integral Program : SIFS version  hochtor2        Thu Jun  5 14:26:35 2003
 /                                                                               
 Hermit Integral Program : SIFS version  niko            Fri Apr 16 12:09:51 1999
 mdrt2_title                                                                     
 mofmt: formatted orbitals label=morb    niko            Fri Apr 16 12:11:03 1999
 SIFS file created by program tran.      hochtor2        Thu Jun  5 14:26:35 2003

 formula file title:
 Hermit Integral Program : SIFS version  hochtor2        Thu Jun  5 14:26:35 2003
 /                                                                               
 Hermit Integral Program : SIFS version  niko            Fri Apr 16 12:09:51 1999
 mdrt2_title                                                                     
 mofmt: formatted orbitals label=morb    niko            Fri Apr 16 12:11:03 1999
 SIFS file created by program tran.      hochtor2        Thu Jun  5 14:26:35 2003
 /                                                                               

 297 dimension of the ci-matrix ->>>       358


 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  1  1    -38.6450051117 -7.1054E-15  8.8869E-02  4.2072E-01  1.0000E-06
 mraqcc  #  2  1    -38.7153895998  7.0384E-02  3.9853E-03  9.1678E-02  1.0000E-06
 mraqcc  #  3  1    -38.7201095052  4.7199E-03  4.7705E-04  3.0869E-02  1.0000E-06
 mraqcc  #  4  1    -38.7206595854  5.5008E-04  7.5731E-05  1.1904E-02  1.0000E-06
 mraqcc  #  5  1    -38.7207402275  8.0642E-05  1.1192E-05  4.6022E-03  1.0000E-06
 mraqcc  #  6  1    -38.7207532796  1.3052E-05  2.3064E-06  2.0620E-03  1.0000E-06
 mraqcc  #  7  1    -38.7207564065  3.1269E-06  4.9495E-07  1.0116E-03  1.0000E-06
 mraqcc  #  8  1    -38.7207569735  5.6702E-07  6.2129E-08  3.5352E-04  1.0000E-06
 mraqcc  #  9  1    -38.7207570461  7.2587E-08  7.4099E-09  1.2108E-04  1.0000E-06
 mraqcc  # 10  1    -38.7207570552  9.1372E-09  9.9166E-10  4.5503E-05  1.0000E-06
 mraqcc  # 11  1    -38.7207570564  1.1585E-09  8.2179E-11  1.3550E-05  1.0000E-06
 mraqcc  # 12  1    -38.7207570565  1.0126E-10  8.7375E-12  4.3647E-06  1.0000E-06
 mraqcc  # 13  1    -38.7207570565  1.0671E-11  8.2057E-13  1.3427E-06  1.0000E-06
 mraqcc  # 14  1    -38.7207570565  1.0729E-12  8.2768E-14  4.2088E-07  1.0000E-06

 mraqcc   convergence criteria satisfied after 14 iterations.

 final mraqcc   convergence information:
 mraqcc  # 14  1    -38.7207570565  1.0729E-12  8.2768E-14  4.2088E-07  1.0000E-06
