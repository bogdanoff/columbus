1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      09/20/00    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

================================================================================
four external integ    0.25 MB location: local disk    
three external inte    0.25 MB location: local disk    
four external integ    0.25 MB location: local disk    
three external inte    0.25 MB location: local disk    
diagonal integrals     0.12 MB location: local disk    
off-diagonal integr    0.12 MB location: local disk    
 nsubmx= 16 lenci= 358
global arrays:        11814   (    0.09 MB)
vdisk:                    0   (    0.00 MB)
drt:                 278425   (    1.06 MB)
================================================================================
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core            7999999 DP per process
 CIUDG version 5.9.3 (05-Dec-2002)
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
   NTYPE = 3,
   gset =3,
   RTOLCI=0.0000010000,0.000001,0.00001,0.00001
   VOUT  = 1,
   froot = 1,
   NBKITR= 0,
   NITER= 80,
   NVCIMX=16,
   ivmode=3,
 /&end
 ------------------------------------------------------------------------
 froot operation modus: follow reference vector.
 bummer (warning):2:changed keyword: nbkitr=         0
 bummer (warning):changed keyword: davcor=         0

 ** list of control variables **
 nrfitr =   30      nvrfmx =   16      nvrfmn =    1
 lvlprt =    0      nroot  =    1      noldv  =    0      noldhv =    0
 nunitv =    1      ntype  =    3      nbkitr =    0      niter  =   80
 ivmode =    3      vout   =    1      istrt  =    0      iortls =    0
 nvbkmx =   16      ibktv  =   -1      ibkthv =   -1      frcsub =    0
 nvcimx =   16      icitv  =   -1      icithv =   -1      maxseg =    4
 iden   =    1      itran  =    0      froot  =    1      rtmode =    0
 ftcalc =    1      lrtshift=1.0000    ncouple=    1      skipso  =   F
 ncorel =    0      csfprn  =   1      ctol   = 1.00E-02  davcor  =   0


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-04    1.000E-06
 This is a AQCC calculation USING the mrci-code /  March 1997 (tm)
 =============================================
 ==========================================

 units and filenames:
    1: ( 6)    ciudgls                                                     
    2: ( 5)    ciudgin                                                     
    3: ( 7)    ciudgsm                                                     
    4: ( 4)    cihdiag                                                     
    5: ( 3)    cihpseu                                                     
    6: (10)    cihvfl                                                      
    7: (11)    civfl                                                       
    8: (12)    diagint                                                     
    9: (13)    ofdgint                                                     
   10: (17)    cidrtfl                                                     
   11: (20)    ciftdfl                                                     
   12: (21)    ciftofl                                                     
   13: (24)    ciftifl                                                     
   14: ( 8)    ciflind                                                     
   15: (15)    civin                                                       
   16: (16)    civout                                                      
   18: (18)    d1fl                                                        
   20: (53)    cifvfl                                                      
   21: (25)    ciscr4                                                      
   22: (41)    ciftotd                                                     
   23: (42)    flacpfd                                                     
   26: (26)    ciscr5                                                      
   27: (47)    restart                                                     
   28: (48)    civout                                                      
   29: (49)    cirefv                                                      
   31: (31)    fil4w                                                       
   32: (32)    fil4x                                                       
   33: (33)    fil3w                                                       
   34: (34)    fil3x                                                       
   36: (36)    aoints                                                      
   37: (37)    aoints2                                                     
   38: (38)    drtfil                                                      
   39: (50)    cisrtif                                                     
   42: (50)    mocoef                                                      
   45: (52)    nocoef_ci                                                   
   46: (53)    civfl_restart                                               
 ------------------------------------------------------------------------

 workspace allocation information: lcore=   7999999 mem1=1108557832 ifirst=-264082490

 integral file titles:
 Hermit Integral Program : SIFS version  hochtor2        Thu Jun  5 14:26:35 2003
 /                                                                               
 Hermit Integral Program : SIFS version  niko            Fri Apr 16 12:09:51 1999
 mdrt2_title                                                                     
 mofmt: formatted orbitals label=morb    niko            Fri Apr 16 12:11:03 1999
 SIFS file created by program tran.      hochtor2        Thu Jun  5 14:26:35 2003

 core energy values from the integral file:
 energy( 1)=  6.203440944703E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -3.413186659445E+01, ietype=    6,   fcore energy of type: H1(*)   

 total core repulsion energy = -2.792842564975E+01

 drt header information:
 /                                                                               
 spnorb, spnodd, lxyzir,hmult F F 0 0 0 0
 nmot  =    13 niot  =     4 nfct  =     1 nfvt  =     0
 nrow  =    22 nsym  =     4 ssym  =     1 lenbuf=  1600
 nwalk,xbar:         62        7       20       15       20
 nvalwt,nvalw:       56        5       16       15       20
 ncsft:             358
 total number of valid internal walks:      56
 nvalz,nvaly,nvalx,nvalw =        5      16      15      20

 cisrt info file parameters:
 file number  12 blocksize   4095
 mxbld   4095
 nd4ext,nd2ext,nd0ext    72    64    20
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int      330      432      360      156       27        0        0        0
 minbl4,minbl3,maxbl2    48    48    51
 maxbuf 32767
 number of external orbitals per symmetry block:   4   1   3   0
 nmsym   4 number of internal orbitals   4

 formula file title:
 Hermit Integral Program : SIFS version  hochtor2        Thu Jun  5 14:26:35 2003
 /                                                                               
 Hermit Integral Program : SIFS version  niko            Fri Apr 16 12:09:51 1999
 mdrt2_title                                                                     
 mofmt: formatted orbitals label=morb    niko            Fri Apr 16 12:11:03 1999
 SIFS file created by program tran.      hochtor2        Thu Jun  5 14:26:35 2003
 /                                                                               
 file nmb.  24 block size     0
 pthz,pthy,pthx,pthw:     7    20    15    20 total internal walks:      62
 maxlp3,n3xtlp,n2xtlp,n1xtlp,n0xtlp,n2lp,n1lp,n0lp     0     0     0     0
 orbsym(*)= 1 1 2 3

 setref:        5 references kept,
                0 references were marked as invalid, out of
                5 total.
 limcnvrt: found 5 valid internal walksout of  7
  walks (skipping trailing invalids)
  ... adding  5 segmentation marks segtype= 1
 limcnvrt: found 16 valid internal walksout of  20
  walks (skipping trailing invalids)
  ... adding  16 segmentation marks segtype= 2
 limcnvrt: found 15 valid internal walksout of  15
  walks (skipping trailing invalids)
  ... adding  15 segmentation marks segtype= 3
 limcnvrt: found 20 valid internal walksout of  20
  walks (skipping trailing invalids)
  ... adding  20 segmentation marks segtype= 4

 number of external paths / symmetry
 vertex x       9       4      12       3
 vertex w      17       4      12       3



                   segmentation summary for type diagonal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1           5|         5|         0|         5|         0|         1|
 -------------------------------------------------------------------------------
  Y 2          16|        44|         5|        16|         5|         2|
 -------------------------------------------------------------------------------
  X 3          15|        97|        49|        15|        21|         3|
 -------------------------------------------------------------------------------
  W 4          20|       212|       146|        20|        36|         4|
 -------------------------------------------------------------------------------

dimension of the ci-matrix ->>>       358


 297 dimension of the ci-matrix ->>>       358


 297 dimension of the ci-matrix ->>>         7

                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS
----------------------------------------------------------------------------------------------------
     1  3   1    24      two-ext xz   2X  3 1      15       5         97          5      15       5
     2  4   1    25      two-ext wz   2X  4 1      20       5        212          5      20       5
     3  4   3    26      two-ext wx   2X  4 3      20      15        212         97      20      15
     4  2   1    11      one-ext yz   1X  2 1      16       5         44          5      16       5
     5  3   2    15      1ex3ex  yx   3X  3 2      15      16         97         44      15      16
     6  4   2    16      1ex3ex  yw   3X  4 2      20      16        212         44      20      16
     7  1   1     1      allint zz    OX  1 1       5       5          5          5       5       5
     8  2   2     5      0ex2ex yy    OX  2 2      16      16         44         44      16      16
     9  3   3     6      0ex2ex xx    OX  3 3      15      15         97         97      15      15
    10  4   4     7      0ex2ex ww    OX  4 4      20      20        212        212      20      20
    11  1   1    75      dg-024ext z  DG  1 1       5       5          5          5       5       5
    12  2   2    45      4exdg024 y   DG  2 2      16      16         44         44      16      16
    13  3   3    46      4exdg024 x   DG  3 3      15      15         97         97      15      15
    14  4   4    47      4exdg024 w   DG  4 4      20      20        212        212      20      20
----------------------------------------------------------------------------------------------------
 DIAGEL COUNTS: DG0X,DG2X,DG4X= 358 156 51
 diagonal elements written to file   4

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      1 vectors will be written to unit 11 beginning with logical record   1

            1 vectors will be created
 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:        34 2x:         0 4x:         0
All internal counts: zz :        14 yy:         0 xx:         0 ww:         0
One-external counts: yz :         0 yx:         0 yw:         0
Two-external counts: yy :         0 ww:         0 xx:         0 xz:         0 wz:         0 wx:         0
Three-ext.   counts: yx :         0 yw:         0

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:         0    task #   2:         0    task #   3:         0    task #   4:         0
task #   5:         0    task #   6:         0    task #   7:        13    task #   8:         0
task #   9:         0    task #  10:         0    task #  11:        29    task #  12:         0
task #  13:         0    task #  14:         0    task #
 reference space has dimension       5

    root           eigenvalues
    ----           ------------
       1         -38.6450051117

 strefv generated    1 initial ci vector(s).
    ---------end of vector generation---------


         vector  1 from unit 11 written to unit 49 filename cirefv                                                      

         vector  1 from unit 11 written to unit 49 filename cirefv                                                      

    ------------------------------------------------------------

 setopt2: mraqcc energy evaluation      gvalue=  0.6

    ------------------------------------------------------------


    reference energies:

  reference state used is:   1with energy      -38.6450051117

  ### active excitation selection ###

 Inactive orbitals:
    there are    5 all-active excitations of which    5 are references.

    the    5 reference all-active excitation csfs

    --------------------
       1   2   3   4   5
    --------------------
  1:   2   2   2   1   0
  2:   2   2   0  -1   2
  3:   2   0   2   2   2
  4:   0   2   2   2   2

  ### end active excitation selection ###


################################################################################
 one electron density matrix calculation 
################################################################################


 workspace allocation information: mxcore= 7999992
 the acpf density matrix will be calculated

 space required:

    space required for calls in multd2:
       onex           40
       allin           0
       diagon         85
    max.      ---------
       maxnex         85

    total core space usage:
       maxnex         85
    max k-seg        212
    max k-ind         20
              ---------
       totmax        549
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=       0  DYX=       0  DYW=       0
   D0Z=       2  D0Y=       0  D0X=       0  D0W=       0
  DDZI=      16 DDYI=       0 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=       0 DDXE=       0 DDWE=       0
================================================================================

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:               358
 number of initial trial vectors:                         1
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:              16
 one root will be followed:                               1
 number of roots to converge:                             1
 number of iterations:                                   80
 residual norm convergence criteria:               0.000001

          starting ci iteration   1

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       358 2x:       156 4x:        51
All internal counts: zz :        14 yy:        55 xx:        45 ww:        58
One-external counts: yz :        92 yx:       221 yw:       292
Two-external counts: yy :       102 ww:       124 xx:       113 xz:        18 wz:        32 wx:       157
Three-ext.   counts: yx :        75 yw:        99

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        16    task #   2:        27    task #   3:       137    task #   4:        86
task #   5:       261    task #   6:       308    task #   7:        13    task #   8:        47
task #   9:        42    task #  10:        49    task #  11:        29    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1
 ref:   1     1.00000000
 follow root  1 (overlap=  1.)

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  1  1    -38.6450051117 -7.1054E-15  8.8869E-02  4.2072E-01  1.0000E-06

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.00
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.01   0.01   0.00   0.01
    9    6    0   0.02   0.00   0.00   0.02
   10    7    0   0.01   0.00   0.00   0.01
   11   75    0   0.01   0.00   0.00   0.01
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.00   0.00   0.00   0.00
   14   47    0   0.01   0.00   0.00   0.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1300s 
time spent in multnx:                   0.1200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

          starting ci iteration   2

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       358 2x:       156 4x:        51
All internal counts: zz :        14 yy:        55 xx:        45 ww:        58
One-external counts: yz :        92 yx:       221 yw:       292
Two-external counts: yy :       102 ww:       124 xx:       113 xz:        18 wz:        32 wx:       157
Three-ext.   counts: yx :        75 yw:        99

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        16    task #   2:        27    task #   3:       137    task #   4:        86
task #   5:       261    task #   6:       308    task #   7:        13    task #   8:        47
task #   9:        42    task #  10:        49    task #  11:        29    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1     0.98513965    -0.17175525
 follow root  1 (overlap=  0.985139652)

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  2  1    -38.7153895998  7.0384E-02  3.9853E-03  9.1678E-02  1.0000E-06
 mraqcc  #  2  2    -36.3294639356  8.4010E+00  0.0000E+00  8.4471E-01  1.0000E-06

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.00   0.00   0.00   0.00
    3   26    0   0.01   0.00   0.00   0.00
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.01   0.00   0.00   0.00
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.01   0.00   0.00   0.01
   10    7    0   0.02   0.00   0.00   0.02
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.00   0.00   0.00   0.00
   14   47    0   0.01   0.00   0.00   0.00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.0900s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1200s 

          starting ci iteration   3

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       358 2x:       156 4x:        51
All internal counts: zz :        14 yy:        55 xx:        45 ww:        58
One-external counts: yz :        92 yx:       221 yw:       292
Two-external counts: yy :       102 ww:       124 xx:       113 xz:        18 wz:        32 wx:       157
Three-ext.   counts: yx :        75 yw:        99

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        16    task #   2:        27    task #   3:       137    task #   4:        86
task #   5:       261    task #   6:       308    task #   7:        13    task #   8:        47
task #   9:        42    task #  10:        49    task #  11:        29    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.98239576     0.13959269     0.12414690
 follow root  1 (overlap=  0.982395758)

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  3  1    -38.7201095052  4.7199E-03  4.7705E-04  3.0869E-02  1.0000E-06
 mraqcc  #  3  2    -36.9699658799  6.4050E-01  0.0000E+00  4.6098E-01  1.0000E-06
 mraqcc  #  3  3    -36.0598656585  8.1314E+00  0.0000E+00  5.6342E-01  1.0000E-05

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.01   0.00   0.00   0.01
    9    6    0   0.02   0.00   0.00   0.02
   10    7    0   0.01   0.00   0.00   0.01
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.01   0.00   0.00   0.00
   14   47    0   0.00   0.00   0.00   0.00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

          starting ci iteration   4

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       358 2x:       156 4x:        51
All internal counts: zz :        14 yy:        55 xx:        45 ww:        58
One-external counts: yz :        92 yx:       221 yw:       292
Two-external counts: yy :       102 ww:       124 xx:       113 xz:        18 wz:        32 wx:       157
Three-ext.   counts: yx :        75 yw:        99

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        16    task #   2:        27    task #   3:       137    task #   4:        86
task #   5:       261    task #   6:       308    task #   7:        13    task #   8:        47
task #   9:        42    task #  10:        49    task #  11:        29    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1    -0.98149885    -0.11172265    -0.09996725     0.11909914
 follow root  1 (overlap=  0.98149885)

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  4  1    -38.7206595854  5.5008E-04  7.5731E-05  1.1904E-02  1.0000E-06
 mraqcc  #  4  2    -37.5019808762  5.3201E-01  0.0000E+00  0.0000E+00  1.0000E-06
 mraqcc  #  4  3    -36.4505052113  3.9064E-01  0.0000E+00  8.3748E-01  1.0000E-05
 mraqcc  #  4  4    -36.0588788094  8.1305E+00  0.0000E+00  5.5944E-01  1.0000E-05

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.00   0.00   0.00   0.00
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.01   0.00   0.00   0.01
   10    7    0   0.02   0.00   0.00   0.02
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.00   0.00   0.00   0.00
   14   47    0   0.01   0.00   0.00   0.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1200s 

          starting ci iteration   5

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       358 2x:       156 4x:        51
All internal counts: zz :        14 yy:        55 xx:        45 ww:        58
One-external counts: yz :        92 yx:       221 yw:       292
Two-external counts: yy :       102 ww:       124 xx:       113 xz:        18 wz:        32 wx:       157
Three-ext.   counts: yx :        75 yw:        99

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        16    task #   2:        27    task #   3:       137    task #   4:        86
task #   5:       261    task #   6:       308    task #   7:        13    task #   8:        47
task #   9:        42    task #  10:        49    task #  11:        29    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     0.98133179    -0.07767091     0.08830687     0.11513063     0.09950870
 follow root  1 (overlap=  0.98133179)

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  5  1    -38.7207402275  8.0642E-05  1.1192E-05  4.6022E-03  1.0000E-06
 mraqcc  #  5  2    -37.7870752982  2.8509E-01  0.0000E+00  0.0000E+00  1.0000E-06
 mraqcc  #  5  3    -37.1582353131  7.0773E-01  0.0000E+00  9.9727E-01  1.0000E-05
 mraqcc  #  5  4    -36.4376719968  3.7879E-01  0.0000E+00  7.0058E-01  1.0000E-05
 mraqcc  #  5  5    -35.9277897394  7.9994E+00  0.0000E+00  3.9954E-01  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.01   0.01   0.00   0.01
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.01   0.00   0.00   0.01
    9    6    0   0.02   0.00   0.00   0.02
   10    7    0   0.01   0.00   0.00   0.01
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.01   0.00   0.00   0.00
   14   47    0   0.00   0.00   0.00   0.00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

          starting ci iteration   6

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       358 2x:       156 4x:        51
All internal counts: zz :        14 yy:        55 xx:        45 ww:        58
One-external counts: yz :        92 yx:       221 yw:       292
Two-external counts: yy :       102 ww:       124 xx:       113 xz:        18 wz:        32 wx:       157
Three-ext.   counts: yx :        75 yw:        99

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        16    task #   2:        27    task #   3:       137    task #   4:        86
task #   5:       261    task #   6:       308    task #   7:        13    task #   8:        47
task #   9:        42    task #  10:        49    task #  11:        29    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1     0.98131097     0.05799077     0.03801141     0.12358864    -0.07375983     0.10726754
 follow root  1 (overlap=  0.981310968)

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  6  1    -38.7207532796  1.3052E-05  2.3064E-06  2.0620E-03  1.0000E-06
 mraqcc  #  6  2    -37.9435525691  1.5648E-01  0.0000E+00  0.0000E+00  1.0000E-06
 mraqcc  #  6  3    -37.4161337584  2.5790E-01  0.0000E+00  1.1593E+00  1.0000E-05
 mraqcc  #  6  4    -37.0811087504  6.4344E-01  0.0000E+00  4.8745E-01  1.0000E-05
 mraqcc  #  6  5    -36.1459924028  2.1820E-01  0.0000E+00  6.5753E-01  1.0000E-04
 mraqcc  #  6  6    -35.9225270830  7.9941E+00  0.0000E+00  3.5518E-01  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.00   0.00   0.00   0.00
    3   26    0   0.01   0.00   0.00   0.00
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.01   0.00   0.00   0.00
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.01   0.00   0.00   0.01
   10    7    0   0.02   0.00   0.00   0.02
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.00   0.00   0.00   0.00
   14   47    0   0.01   0.00   0.00   0.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1200s 

          starting ci iteration   7

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       358 2x:       156 4x:        51
All internal counts: zz :        14 yy:        55 xx:        45 ww:        58
One-external counts: yz :        92 yx:       221 yw:       292
Two-external counts: yy :       102 ww:       124 xx:       113 xz:        18 wz:        32 wx:       157
Three-ext.   counts: yx :        75 yw:        99

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        16    task #   2:        27    task #   3:       137    task #   4:        86
task #   5:       261    task #   6:       308    task #   7:        13    task #   8:        47
task #   9:        42    task #  10:        49    task #  11:        29    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7
 ref:   1     0.98129694     0.03295481    -0.08084528    -0.05742030     0.12271167    -0.07342675     0.07541622
 follow root  1 (overlap=  0.981296939)

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  7  1    -38.7207564065  3.1269E-06  4.9495E-07  1.0116E-03  1.0000E-06
 mraqcc  #  7  2    -38.1602258326  2.1667E-01  0.0000E+00  0.0000E+00  1.0000E-06
 mraqcc  #  7  3    -37.5445346808  1.2840E-01  0.0000E+00  5.1844E-01  1.0000E-05
 mraqcc  #  7  4    -37.4114115396  3.3030E-01  0.0000E+00  1.0180E+00  1.0000E-05
 mraqcc  #  7  5    -36.6444258557  4.9843E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  7  6    -35.9474554305  2.4928E-02  0.0000E+00  4.6844E-01  1.0000E-04
 mraqcc  #  7  7    -35.7458920665  7.8175E+00  0.0000E+00  3.5486E-01  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.00   0.00   0.00   0.00
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.01   0.01   0.00   0.01
    9    6    0   0.02   0.00   0.00   0.02
   10    7    0   0.01   0.00   0.00   0.01
   11   75    0   0.01   0.01   0.00   0.01
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.00   0.00   0.00   0.00
   14   47    0   0.00   0.00   0.00   0.00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1100s 
time spent in multnx:                   0.1100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0200s 
time for vector access in mult:         0.0200s 
total time per CI iteration:            0.1300s 

          starting ci iteration   8

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       358 2x:       156 4x:        51
All internal counts: zz :        14 yy:        55 xx:        45 ww:        58
One-external counts: yz :        92 yx:       221 yw:       292
Two-external counts: yy :       102 ww:       124 xx:       113 xz:        18 wz:        32 wx:       157
Three-ext.   counts: yx :        75 yw:        99

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        16    task #   2:        27    task #   3:       137    task #   4:        86
task #   5:       261    task #   6:       308    task #   7:        13    task #   8:        47
task #   9:        42    task #  10:        49    task #  11:        29    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.98129747     0.02119598    -0.07630354     0.05309338     0.11756709    -0.08297559     0.03773546    -0.07638019
 follow root  1 (overlap=  0.981297471)

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  8  1    -38.7207569735  5.6702E-07  6.2129E-08  3.5352E-04  1.0000E-06
 mraqcc  #  8  2    -38.2674427295  1.0722E-01  0.0000E+00  0.0000E+00  1.0000E-06
 mraqcc  #  8  3    -37.6720008057  1.2747E-01  0.0000E+00  3.0297E-01  1.0000E-05
 mraqcc  #  8  4    -37.5325905595  1.2118E-01  0.0000E+00  1.0139E+00  1.0000E-05
 mraqcc  #  8  5    -36.7653322072  1.2091E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  8  6    -36.1943133625  2.4686E-01  0.0000E+00  4.2077E-01  1.0000E-04
 mraqcc  #  8  7    -35.9043977186  1.5851E-01  0.0000E+00  5.2739E-01  1.0000E-04
 mraqcc  #  8  8    -35.6936365250  7.7652E+00  0.0000E+00  3.9993E-01  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.00   0.00   0.00   0.00
    5   15    0   0.02   0.00   0.00   0.02
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.00   0.00   0.00   0.00
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.01   0.00   0.00   0.01
   10    7    0   0.02   0.00   0.00   0.01
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.01   0.00   0.00   0.01
   14   47    0   0.00   0.00   0.00   0.00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

          starting ci iteration   9

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       358 2x:       156 4x:        51
All internal counts: zz :        14 yy:        55 xx:        45 ww:        58
One-external counts: yz :        92 yx:       221 yw:       292
Two-external counts: yy :       102 ww:       124 xx:       113 xz:        18 wz:        32 wx:       157
Three-ext.   counts: yx :        75 yw:        99

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        16    task #   2:        27    task #   3:       137    task #   4:        86
task #   5:       261    task #   6:       308    task #   7:        13    task #   8:        47
task #   9:        42    task #  10:        49    task #  11:        29    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.98129584    -0.02000521    -0.06995415     0.03268545    -0.10015732    -0.08321318     0.08397982    -0.00751685

                ci   9
 ref:   1     0.08143285
 follow root  1 (overlap=  0.98129584)

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  #  9  1    -38.7207570461  7.2587E-08  7.4099E-09  1.2108E-04  1.0000E-06
 mraqcc  #  9  2    -38.2902563004  2.2814E-02  0.0000E+00  0.0000E+00  1.0000E-06
 mraqcc  #  9  3    -37.7362674415  6.4267E-02  0.0000E+00  0.0000E+00  1.0000E-05
 mraqcc  #  9  4    -37.5441937185  1.1603E-02  0.0000E+00  1.1690E+00  1.0000E-05
 mraqcc  #  9  5    -37.1002062315  3.3487E-01  0.0000E+00  2.3810E-01  1.0000E-04
 mraqcc  #  9  6    -36.5794526386  3.8514E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  #  9  7    -36.1728485405  2.6845E-01  0.0000E+00  4.1099E-01  1.0000E-04
 mraqcc  #  9  8    -35.8123310563  1.1869E-01  0.0000E+00  4.3102E-01  1.0000E-04
 mraqcc  #  9  9    -35.6614218771  7.7330E+00  0.0000E+00  3.6979E-01  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.00   0.00   0.00   0.00
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.01   0.00   0.00   0.01
   10    7    0   0.02   0.00   0.00   0.02
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.00   0.00   0.00   0.00
   14   47    0   0.01   0.00   0.00   0.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1200s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1200s 

          starting ci iteration  10

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       358 2x:       156 4x:        51
All internal counts: zz :        14 yy:        55 xx:        45 ww:        58
One-external counts: yz :        92 yx:       221 yw:       292
Two-external counts: yy :       102 ww:       124 xx:       113 xz:        18 wz:        32 wx:       157
Three-ext.   counts: yx :        75 yw:        99

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        16    task #   2:        27    task #   3:       137    task #   4:        86
task #   5:       261    task #   6:       308    task #   7:        13    task #   8:        47
task #   9:        42    task #  10:        49    task #  11:        29    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.98129615     0.01734658    -0.05751977     0.02640876    -0.07970578    -0.12306615     0.00075249    -0.06416413

                ci   9         ci  10
 ref:   1     0.01431952    -0.08324713
 follow root  1 (overlap=  0.981296145)

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  # 10  1    -38.7207570552  9.1372E-09  9.9166E-10  4.5503E-05  1.0000E-06
 mraqcc  # 10  2    -38.3092442951  1.8988E-02  0.0000E+00  0.0000E+00  1.0000E-06
 mraqcc  # 10  3    -37.8274073403  9.1140E-02  0.0000E+00  0.0000E+00  1.0000E-05
 mraqcc  # 10  4    -37.5451757800  9.8206E-04  0.0000E+00  1.1984E+00  1.0000E-05
 mraqcc  # 10  5    -37.3871308072  2.8692E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 10  6    -36.7194034120  1.3995E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 10  7    -36.4707560670  2.9791E-01  0.0000E+00  4.7899E-01  1.0000E-04
 mraqcc  # 10  8    -35.9853500077  1.7302E-01  0.0000E+00  2.6645E-01  1.0000E-04
 mraqcc  # 10  9    -35.8047327413  1.4331E-01  0.0000E+00  4.3263E-01  1.0000E-04
 mraqcc  # 10 10    -35.6604765602  7.7321E+00  0.0000E+00  3.5461E-01  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.00   0.00   0.00   0.00
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.01   0.01   0.00   0.01
    9    6    0   0.02   0.00   0.00   0.02
   10    7    0   0.01   0.00   0.00   0.01
   11   75    0   0.01   0.01   0.00   0.01
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.00   0.00   0.00   0.00
   14   47    0   0.00   0.00   0.00   0.00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1100s 
time spent in multnx:                   0.1100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0200s 
time for vector access in mult:         0.0200s 
total time per CI iteration:            0.1300s 

          starting ci iteration  11

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       358 2x:       156 4x:        51
All internal counts: zz :        14 yy:        55 xx:        45 ww:        58
One-external counts: yz :        92 yx:       221 yw:       292
Two-external counts: yy :       102 ww:       124 xx:       113 xz:        18 wz:        32 wx:       157
Three-ext.   counts: yx :        75 yw:        99

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        16    task #   2:        27    task #   3:       137    task #   4:        86
task #   5:       261    task #   6:       308    task #   7:        13    task #   8:        47
task #   9:        42    task #  10:        49    task #  11:        29    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.98129606    -0.01688855     0.03738972    -0.01858839     0.08979523     0.11659675     0.04932251     0.02061617

                ci   9         ci  10         ci  11
 ref:   1     0.06164567    -0.04317542    -0.06963669
 follow root  1 (overlap=  0.981296062)

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  # 11  1    -38.7207570564  1.1585E-09  8.2179E-11  1.3550E-05  1.0000E-06
 mraqcc  # 11  2    -38.3127676335  3.5233E-03  0.0000E+00  0.0000E+00  1.0000E-06
 mraqcc  # 11  3    -37.9413541510  1.1395E-01  0.0000E+00  0.0000E+00  1.0000E-05
 mraqcc  # 11  4    -37.5557074237  1.0532E-02  0.0000E+00  1.1817E+00  1.0000E-05
 mraqcc  # 11  5    -37.5147616344  1.2763E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 11  6    -36.7511677348  3.1764E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 11  7    -36.5374942999  6.6738E-02  0.0000E+00  4.9941E-01  1.0000E-04
 mraqcc  # 11  8    -36.4528571529  4.6751E-01  0.0000E+00  4.3210E-01  1.0000E-04
 mraqcc  # 11  9    -35.8399754180  3.5243E-02  0.0000E+00  4.4120E-01  1.0000E-04
 mraqcc  # 11 10    -35.7659861684  1.0551E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 11 11    -35.6455785486  7.7172E+00  0.0000E+00  3.8232E-01  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.00   0.00   0.00   0.00
    5   15    0   0.02   0.00   0.00   0.01
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.01   0.00   0.00   0.01
    8    5    0   0.01   0.00   0.00   0.01
    9    6    0   0.02   0.00   0.00   0.02
   10    7    0   0.01   0.00   0.00   0.01
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.01   0.00   0.00   0.01
   14   47    0   0.00   0.00   0.00   0.00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.010000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0000s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

          starting ci iteration  12

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       358 2x:       156 4x:        51
All internal counts: zz :        14 yy:        55 xx:        45 ww:        58
One-external counts: yz :        92 yx:       221 yw:       292
Two-external counts: yy :       102 ww:       124 xx:       113 xz:        18 wz:        32 wx:       157
Three-ext.   counts: yx :        75 yw:        99

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        16    task #   2:        27    task #   3:       137    task #   4:        86
task #   5:       261    task #   6:       308    task #   7:        13    task #   8:        47
task #   9:        42    task #  10:        49    task #  11:        29    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.98129606    -0.01659711    -0.03213255    -0.03760111    -0.08186012     0.06910174    -0.11074430     0.01404247

                ci   9         ci  10         ci  11         ci  12
 ref:   1     0.01044945     0.06212005     0.03804468     0.07059124
 follow root  1 (overlap=  0.981296061)

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  # 12  1    -38.7207570565  1.0126E-10  8.7375E-12  4.3647E-06  1.0000E-06
 mraqcc  # 12  2    -38.3143162180  1.5486E-03  0.0000E+00  0.0000E+00  1.0000E-06
 mraqcc  # 12  3    -37.9843505628  4.2996E-02  0.0000E+00  0.0000E+00  1.0000E-05
 mraqcc  # 12  4    -37.5594215238  3.7141E-03  0.0000E+00  1.1331E+00  1.0000E-05
 mraqcc  # 12  5    -37.5503592244  3.5598E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 12  6    -36.9513571216  2.0019E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 12  7    -36.6454893124  1.0800E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 12  8    -36.5123085227  5.9451E-02  0.0000E+00  5.0476E-01  1.0000E-04
 mraqcc  # 12  9    -36.1323099961  2.9233E-01  0.0000E+00  5.1104E-01  1.0000E-04
 mraqcc  # 12 10    -35.8399457687  7.3960E-02  0.0000E+00  4.4425E-01  1.0000E-04
 mraqcc  # 12 11    -35.7614102283  1.1583E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 12 12    -35.6351712694  7.7067E+00  0.0000E+00  3.8249E-01  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.01   0.00   0.00   0.01
    3   26    0   0.01   0.00   0.00   0.01
    4   11    0   0.00   0.00   0.00   0.00
    5   15    0   0.01   0.00   0.00   0.01
    6   16    0   0.02   0.01   0.00   0.01
    7    1    0   0.00   0.00   0.00   0.00
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.01   0.00   0.00   0.01
   10    7    0   0.02   0.00   0.00   0.02
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.01   0.00   0.00   0.01
   14   47    0   0.00   0.00   0.00   0.00
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1100s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

          starting ci iteration  13

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       358 2x:       156 4x:        51
All internal counts: zz :        14 yy:        55 xx:        45 ww:        58
One-external counts: yz :        92 yx:       221 yw:       292
Two-external counts: yy :       102 ww:       124 xx:       113 xz:        18 wz:        32 wx:       157
Three-ext.   counts: yx :        75 yw:        99

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        16    task #   2:        27    task #   3:       137    task #   4:        86
task #   5:       261    task #   6:       308    task #   7:        13    task #   8:        47
task #   9:        42    task #  10:        49    task #  11:        29    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.98129606     0.01652520    -0.02814410    -0.05021377    -0.06686447    -0.05983721     0.11847110     0.00870836

                ci   9         ci  10         ci  11         ci  12         ci  13
 ref:   1     0.02990049    -0.03272580     0.05762903     0.01912083     0.07521389
 follow root  1 (overlap=  0.981296062)

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  # 13  1    -38.7207570565  1.0671E-11  8.2057E-13  1.3427E-06  1.0000E-06
 mraqcc  # 13  2    -38.3146712143  3.5500E-04  0.0000E+00  0.0000E+00  1.0000E-06
 mraqcc  # 13  3    -38.0086001845  2.4250E-02  0.0000E+00  0.0000E+00  1.0000E-05
 mraqcc  # 13  4    -37.6678315827  1.0841E-01  0.0000E+00  6.7357E-01  1.0000E-05
 mraqcc  # 13  5    -37.5511993857  8.4016E-04  0.0000E+00  5.7008E-01  1.0000E-04
 mraqcc  # 13  6    -37.2062702844  2.5491E-01  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 13  7    -36.6934390364  4.7950E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 13  8    -36.5132558585  9.4734E-04  0.0000E+00  5.1712E-01  1.0000E-04
 mraqcc  # 13  9    -36.3906442786  2.5833E-01  0.0000E+00  3.1168E-01  1.0000E-04
 mraqcc  # 13 10    -36.0159957235  1.7605E-01  0.0000E+00  7.0684E-01  1.0000E-04
 mraqcc  # 13 11    -35.8390249852  7.7615E-02  0.0000E+00  4.3861E-01  1.0000E-04
 mraqcc  # 13 12    -35.7026577183  6.7486E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 13 13    -35.6281987509  7.6998E+00  0.0000E+00  3.5971E-01  1.0000E-04

 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS PER TASK    ================
task# type node  tmult  tloop   tint  tmnx 
    1   24    0   0.01   0.00   0.00   0.01
    2   25    0   0.00   0.00   0.00   0.00
    3   26    0   0.01   0.00   0.00   0.00
    4   11    0   0.01   0.00   0.00   0.01
    5   15    0   0.01   0.01   0.00   0.01
    6   16    0   0.01   0.00   0.00   0.01
    7    1    0   0.01   0.00   0.00   0.00
    8    5    0   0.02   0.00   0.00   0.02
    9    6    0   0.01   0.00   0.00   0.01
   10    7    0   0.02   0.00   0.00   0.02
   11   75    0   0.00   0.00   0.00   0.00
   12   45    0   0.00   0.00   0.00   0.00
   13   46    0   0.00   0.00   0.00   0.00
   14   47    0   0.01   0.00   0.00   0.01
================================================================
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.010000
time for eigenvalue solver             0.000000
time for vector access                 0.000000
================================================================
time spent in mult:                     0.1200s 
time spent in multnx:                   0.1000s 
integral transfer time:                 0.0000s 
time spent for loop construction:       0.0100s 
time for vector access in mult:         0.0000s 
total time per CI iteration:            0.1300s 

          starting ci iteration  14

 =========== Executing IN-CORE method ==========


====================================================================================================
Diagonal     counts:  0x:       358 2x:       156 4x:        51
All internal counts: zz :        14 yy:        55 xx:        45 ww:        58
One-external counts: yz :        92 yx:       221 yw:       292
Two-external counts: yy :       102 ww:       124 xx:       113 xz:        18 wz:        32 wx:       157
Three-ext.   counts: yx :        75 yw:        99

SO-0ex       counts: zz :         0 yy:         0 xx:         0 ww:       0
SO-1ex       counts: yz :         0 yx:         0 yw:         0
SO-2ex       counts: yy :         0 xx:         0 wx:         0
====================================================================================================


 xx2xso2= 0


LOOPCOUNT per task:
task #   1:        16    task #   2:        27    task #   3:       137    task #   4:        86
task #   5:       261    task #   6:       308    task #   7:        13    task #   8:        47
task #   9:        42    task #  10:        49    task #  11:        29    task #  12:         1
task #  13:         1    task #  14:         1    task #

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1    -0.98129606    -0.01617038     0.02316163    -0.02762338    -0.08152468    -0.05526177     0.11860850    -0.02528075

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14
 ref:   1    -0.00224206    -0.03008378     0.03147690    -0.06115296    -0.00908199     0.07325839
 follow root  1 (overlap=  0.981296064)

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  # 14  1    -38.7207570565  1.0729E-12  8.2768E-14  4.2088E-07  1.0000E-06
 mraqcc  # 14  2    -38.3162555150  1.5843E-03  0.0000E+00  0.0000E+00  1.0000E-06
 mraqcc  # 14  3    -38.0476874475  3.9087E-02  0.0000E+00  0.0000E+00  1.0000E-05
 mraqcc  # 14  4    -37.8525061966  1.8467E-01  0.0000E+00  4.0307E-01  1.0000E-05
 mraqcc  # 14  5    -37.5709586700  1.9759E-02  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 14  6    -37.2433998420  3.7130E-02  0.0000E+00  5.3298E-01  1.0000E-04
 mraqcc  # 14  7    -36.6994984045  6.0594E-03  0.0000E+00  0.0000E+00  1.0000E-04
 mraqcc  # 14  8    -36.5388440328  2.5588E-02  0.0000E+00  3.4534E-01  1.0000E-04
 mraqcc  # 14  9    -36.5115033981  1.2086E-01  0.0000E+00  4.9795E-01  1.0000E-04
 mraqcc  # 14 10    -36.2659058520  2.4991E-01  0.0000E+00  5.0766E-01  1.0000E-04
 mraqcc  # 14 11    -36.0150914361  1.7607E-01  0.0000E+00  6.9731E-01  1.0000E-04
 mraqcc  # 14 12    -35.8361969906  1.3354E-01  0.0000E+00  3.9571E-01  1.0000E-04
 mraqcc  # 14 13    -35.6768117143  4.8613E-02  0.0000E+00  6.9273E-02  1.0000E-04
 mraqcc  # 14 14    -35.6113498998  7.6829E+00  0.0000E+00  2.8570E-01  1.0000E-04


 mraqcc   convergence criteria satisfied after 14 iterations.

 final mraqcc   convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mraqcc  # 14  1    -38.7207570565  1.0729E-12  8.2768E-14  4.2088E-07  1.0000E-06

####################CIUDGINFO####################

   followed vector at position   1 energy=  -38.720757056520

################END OF CIUDGINFO################


 a4den factor =  1.015026157 for root   1
    1 of the  15 expansion vectors are transformed.
    1 of the  14 matrix-vector products are transformed.

    1 expansion eigenvectors written to unit nvfile (= 11)
    1 matrix-vector products written to unit nhvfil (= 10)

 root follwoing: root to follow is on position   1
 maximum overlap with reference  1(overlap=  0.981296064)
 reference energy= -38.6450051
 total aqcc energy= -38.7207571
 diagonal element shift (lrtshift) =  -0.0757519448

information on vector: 1from unit 11 written to unit 16filename civout                                                      


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -38.7207570565

                                                       internal orbitals

                                          level       1    2    3    4

                                          orbital     2    3    8   10

                                         symmetry     1    1    2    3

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1 -0.010148                        +-   +-   +-      
 z*  1  1       2  0.963140                        +-   +-        +- 
 z*  1  1       3 -0.157648                        +-        +-   +- 
 z*  1  1       4 -0.083325                        +     -   +-   +- 
 z*  1  1       5 -0.058765                             +-   +-   +- 
 y   1  1       8 -0.018527              2(   3)   +-   +-         - 
 y   1  1       9  0.024815              3(   3)   +-   +-         - 
 y   1  1      14  0.012049              1(   1)   +-    -        +- 
 y   1  1      15 -0.018916              2(   1)   +-    -        +- 
 y   1  1      16  0.013952              3(   1)   +-    -        +- 
 y   1  1      17 -0.019834              4(   1)   +-    -        +- 
 y   1  1      21  0.021491              1(   2)   +-         -   +- 
 y   1  1      26 -0.011531              1(   1)    -   +-        +- 
 y   1  1      41  0.020495              1(   2)   +     -    -   +- 
 y   1  1      45  0.017096              1(   2)        +-    -   +- 
 x   1  1      54 -0.023922    1(   1)   1(   3)   +-    -         - 
 x   1  1      55  0.027237    2(   1)   1(   3)   +-    -         - 
 x   1  1      59  0.021132    2(   1)   2(   3)   +-    -         - 
 x   1  1      73  0.013671    1(   1)   1(   3)    -   +-         - 
 x   1  1      77  0.021169    1(   1)   2(   3)    -   +-         - 
 x   1  1      97  0.024247    1(   1)   2(   1)    -    -        +- 
 w   1  1     147 -0.031984    1(   1)   1(   1)   +-   +-           
 w   1  1     152 -0.013529    3(   1)   3(   1)   +-   +-           
 w   1  1     158 -0.079980    1(   3)   1(   3)   +-   +-           
 w   1  1     160 -0.025796    2(   3)   2(   3)   +-   +-           
 w   1  1     168  0.060213    1(   1)   1(   3)   +-   +          - 
 w   1  1     169 -0.029679    2(   1)   1(   3)   +-   +          - 
 w   1  1     173 -0.021127    2(   1)   2(   3)   +-   +          - 
 w   1  1     174 -0.012584    3(   1)   2(   3)   +-   +          - 
 w   1  1     178 -0.011207    3(   1)   3(   3)   +-   +          - 
 w   1  1     191  0.013841    1(   3)   1(   3)   +-        +-      
 w   1  1     200 -0.024717    1(   1)   1(   1)   +-             +- 
 w   1  1     201  0.018464    1(   1)   2(   1)   +-             +- 
 w   1  1     202 -0.043620    2(   1)   2(   1)   +-             +- 
 w   1  1     211 -0.045966    1(   3)   1(   3)   +-             +- 
 w   1  1     212  0.013122    1(   3)   2(   3)   +-             +- 
 w   1  1     214 -0.011884    1(   3)   3(   3)   +-             +- 
 w   1  1     221 -0.058226    1(   1)   1(   3)   +    +-         - 
 w   1  1     222 -0.011879    2(   1)   1(   3)   +    +-         - 
 w   1  1     223 -0.011652    3(   1)   1(   3)   +    +-         - 
 w   1  1     225 -0.011573    1(   1)   2(   3)   +    +-         - 
 w   1  1     227  0.024743    3(   1)   2(   3)   +    +-         - 
 w   1  1     231  0.010715    3(   1)   3(   3)   +    +-         - 
 w   1  1     253  0.032214    1(   1)   1(   1)   +     -        +- 
 w   1  1     254 -0.013336    1(   1)   2(   1)   +     -        +- 
 w   1  1     255 -0.018924    2(   1)   2(   1)   +     -        +- 
 w   1  1     258  0.010800    3(   1)   3(   1)   +     -        +- 
 w   1  1     265 -0.013220    1(   3)   2(   3)   +     -        +- 
 w   1  1     273  0.013286    1(   1)   1(   3)   +         +-    - 
 w   1  1     309 -0.036241    1(   1)   1(   1)        +-        +- 
 w   1  1     314 -0.011044    3(   1)   3(   1)        +-        +- 
 w   1  1     320 -0.019247    1(   3)   1(   3)        +-        +- 
 w   1  1     342  0.010459    1(   1)   1(   1)             +-   +- 

 ci coefficient statistics:
           rq > 0.1                2
      0.1> rq > 0.01              51
     0.01> rq > 0.001            166
    0.001> rq > 0.0001           110
   0.0001> rq > 0.00001           27
  0.00001> rq > 0.000001           2
 0.000001> rq                      0
           all                   358
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
   calculation for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=      18  DYX=      44  DYW=      54
   D0Z=       2  D0Y=      11  D0X=       8  D0W=      10
  DDZI=      16 DDYI=      52 DDXI=      48 DDWI=      56
  DDZE=       0 DDYE=      16 DDXE=      15 DDWE=      20
================================================================================
 root #  1: Scaling(2) with   1.01502616corresponding to ref #  1


*****   symmetry block SYM1   *****

 occupation numbers of nos
   2.0000000      1.9781181      1.9104580      0.0200098      0.0080203
   0.0026263      0.0006243


*****   symmetry block SYM2   *****

 occupation numbers of nos
   0.0755085      0.0005161


*****   symmetry block SYM3   *****

 occupation numbers of nos
   1.9678024      0.0301052      0.0050658      0.0011452


 total number of electrons =    8.0000000000

NO coefficients and occupation numbers are written to nocoef_ci.1


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!


                        A1  partial gross atomic populations
   ao class       1A1        2A1        3A1        4A1        5A1        6A1 
    C1_ s       1.994144   0.685608   1.167720   0.000752   0.004537   0.000432
    C1_ p      -0.000029   0.503921   0.765575   0.009375   0.003052   0.000199
    H1_ s       0.005885   0.788590  -0.022837   0.009883   0.000431   0.001995

   ao class       7A1 
    C1_ s       0.000173
    C1_ p       0.000262
    H1_ s       0.000189

                        B1  partial gross atomic populations
   ao class       1B1        2B1 
    C1_ p       0.075509   0.000516

                        B2  partial gross atomic populations
   ao class       1B2        2B2        3B2        4B2 
    C1_ p       1.012255   0.014979   0.002786   0.000502
    H1_ s       0.955547   0.015126   0.002280   0.000644


                        gross atomic populations
     ao           C1_        H1_
      s         3.853366   1.757731
      p         2.388902   0.000000
    total       6.242269   1.757731


 Total number of electrons:    8.00000000

