1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 integral file titles:
 Hermit Integral Program : SIFS version  hochtor2        Thu Jun  5 14:27:11 2003
 /                                                                               
 Hermit Integral Program : SIFS version  niko            Fri Apr 16 12:09:51 1999
 mdrt2_title                                                                     
 mofmt: formatted orbitals label=morb    niko            Fri Apr 16 12:11:03 1999
 SIFS file created by program tran.      hochtor2        Thu Jun  5 14:27:11 2003

 formula file title:
 Hermit Integral Program : SIFS version  hochtor2        Thu Jun  5 14:27:11 2003
 /                                                                               
 Hermit Integral Program : SIFS version  niko            Fri Apr 16 12:09:51 1999
 mdrt2_title                                                                     
 mofmt: formatted orbitals label=morb    niko            Fri Apr 16 12:11:03 1999
 SIFS file created by program tran.      hochtor2        Thu Jun  5 14:27:11 2003
 /                                                                               

 297 dimension of the ci-matrix ->>>       358


 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=     5)...
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -38.6450051117 -7.1054E-15  8.8869E-02  4.2072E-01  1.0000E-04
 mr-sdci #  1  2    -38.4074031068 -1.4211E-14  0.0000E+00  4.3501E-01  1.0000E-04

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:
 mr-sdci #  1  1    -38.6450051117 -7.1054E-15  8.8869E-02  4.2072E-01  1.0000E-04
 mr-sdci #  1  2    -38.4074031068 -1.4211E-14  0.0000E+00  4.3501E-01  1.0000E-04

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -38.7153953050  7.0390E-02  3.9119E-03  9.1455E-02  1.0000E-04
 mr-sdci #  1  2    -38.4074252190  2.2112E-05  0.0000E+00  4.3490E-01  1.0000E-04
 mr-sdci #  2  1    -38.7191416002  3.7463E-03  4.2111E-04  2.9680E-02  1.0000E-04
 mr-sdci #  2  2    -38.4076537038  2.2848E-04  0.0000E+00  4.3406E-01  1.0000E-04
 mr-sdci #  3  1    -38.7195877936  4.4619E-04  5.4651E-05  1.0722E-02  1.0000E-04
 mr-sdci #  3  2    -38.4078735227  2.1982E-04  0.0000E+00  4.3339E-01  1.0000E-04
 mr-sdci #  4  1    -38.7196450327  5.7239E-05  6.6940E-06  3.7096E-03  1.0000E-04
 mr-sdci #  4  2    -38.4080151393  1.4162E-04  0.0000E+00  4.3313E-01  1.0000E-04
 mr-sdci #  5  1    -38.7196529025  7.8698E-06  1.3630E-06  1.6042E-03  1.0000E-04
 mr-sdci #  5  2    -38.4080196561  4.5168E-06  0.0000E+00  4.3313E-01  1.0000E-04
 mr-sdci #  6  1    -38.7196545445  1.6420E-06  3.2662E-07  7.7575E-04  1.0000E-04
 mr-sdci #  6  2    -38.4083567441  3.3709E-04  0.0000E+00  4.3280E-01  1.0000E-04
 mr-sdci #  7  1    -38.7196548638  3.1929E-07  4.0866E-08  2.7322E-04  1.0000E-04
 mr-sdci #  7  2    -38.4085232875  1.6654E-04  0.0000E+00  4.3177E-01  1.0000E-04
 mr-sdci #  8  1    -38.7196548979  3.4176E-08  7.3490E-09  1.1966E-04  1.0000E-04
 mr-sdci #  8  2    -38.4085618609  3.8573E-05  0.0000E+00  4.3143E-01  1.0000E-04
 mr-sdci #  9  1    -38.7196549039  5.9915E-09  0.0000E+00  5.5207E-05  1.0000E-04
 mr-sdci #  9  2    -38.4085923606  3.0500E-05  1.0233E-01  4.3126E-01  1.0000E-04
 mr-sdci # 10  1    -38.7196549039  6.4126E-13  0.0000E+00  5.5199E-05  1.0000E-04
 mr-sdci # 10  2    -38.4888970061  8.0305E-02  5.0976E-03  9.7550E-02  1.0000E-04
 mr-sdci # 11  1    -38.7196549039  4.3325E-12  0.0000E+00  5.5304E-05  1.0000E-04
 mr-sdci # 11  2    -38.4941083304  5.2113E-03  6.4533E-04  3.5916E-02  1.0000E-04
 mr-sdci # 12  1    -38.7196549039  1.3646E-11  0.0000E+00  5.5330E-05  1.0000E-04
 mr-sdci # 12  2    -38.4947023362  5.9401E-04  1.5598E-04  1.4437E-02  1.0000E-04
 mr-sdci # 13  1    -38.7196549040  3.5014E-11  0.0000E+00  5.5156E-05  1.0000E-04
 mr-sdci # 13  2    -38.4948685376  1.6620E-04  3.2466E-05  7.2980E-03  1.0000E-04
 mr-sdci # 14  1    -38.7196549040  4.0215E-11  0.0000E+00  5.5352E-05  1.0000E-04
 mr-sdci # 14  2    -38.4948902921  2.1754E-05  4.9983E-06  2.9445E-03  1.0000E-04
 mr-sdci # 15  1    -38.7196549041  1.2048E-10  0.0000E+00  5.3755E-05  1.0000E-04
 mr-sdci # 15  2    -38.4948946731  4.3810E-06  2.1863E-06  1.6865E-03  1.0000E-04
 mr-sdci # 16  1    -38.7196549042  1.0776E-10  0.0000E+00  5.2213E-05  1.0000E-04
 mr-sdci # 16  2    -38.4948966570  1.9840E-06  1.8257E-07  5.9512E-04  1.0000E-04
 mr-sdci # 17  1    -38.7196549043  3.7186E-11  0.0000E+00  5.1233E-05  1.0000E-04
 mr-sdci # 17  2    -38.4948968227  1.6568E-07  2.4765E-08  2.2871E-04  1.0000E-04
 mr-sdci # 18  1    -38.7196549043  3.8245E-12  0.0000E+00  5.0987E-05  1.0000E-04
 mr-sdci # 18  2    -38.4948968456  2.2823E-08  4.8502E-09  9.5131E-05  1.0000E-04

 mr-sdci  convergence criteria satisfied after 18 iterations.

 final mr-sdci  convergence information:
 mr-sdci # 18  1    -38.7196549043  3.8245E-12  0.0000E+00  5.0987E-05  1.0000E-04
 mr-sdci # 18  2    -38.4948968456  2.2823E-08  4.8502E-09  9.5131E-05  1.0000E-04

 number of reference csfs (nref) is     5.  root number (iroot) is  1.

 eref      =    -38.644980748205   "relaxed" cnot**2         =   0.964246023765
 eci       =    -38.719654904282   deltae = eci - eref       =  -0.074674156078
 eci+dv1   =    -38.722324802284   dv1 = (1-cnot**2)*deltae  =  -0.002669898002
 eci+dv2   =    -38.722423801365   dv2 = dv1 / cnot**2       =  -0.002768897082
 eci+dv3   =    -38.722530424874   dv3 = dv1 / (2*cnot**2-1) =  -0.002875520592
 eci+pople =    -38.721523351670   (  6e- scaled deltae )    =  -0.076542603465

 number of reference csfs (nref) is     5.  root number (iroot) is  2.

 eref      =    -38.407198495048   "relaxed" cnot**2         =   0.950304727279
 eci       =    -38.494896845555   deltae = eci - eref       =  -0.087698350507
 eci+dv1   =    -38.499255039000   dv1 = (1-cnot**2)*deltae  =  -0.004358193446
 eci+dv2   =    -38.499482946540   dv2 = dv1 / cnot**2       =  -0.004586100985
 eci+dv3   =    -38.499736005774   dv3 = dv1 / (2*cnot**2-1) =  -0.004839160219
 eci+pople =    -38.498006530117   (  6e- scaled deltae )    =  -0.090808035069
