
 program cipc      

 print the csf info for mrsdci wave functions

 written by: ron shepard

 version date: 06-jun-96

 This Version of Program cipc is Maintained by:
    Hans Lischka
    Institute for Theoretical Chemistry
    University of Vienna
    Waeringerstr 17, A-1090 Wien, Austria
    Internet: hans.lischka@univie.ac.at



     ******************************************
     **    PROGRAM:              CIPC        **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


 workspace allocation parameters: lencor=   8000000 mem1=1108557832 ifirst=-264083054

 drt header information:
 cidrt_title                                                                     
 nmot  =    13 niot  =     4 nfct  =     1 nfvt  =     0
 nrow  =    22 nsym  =     4 ssym  =     2 lenbuf=  1600
 spnorb=     F spnodd=     F lxyzir(1:3)= 0 0 0
 nwalk,xbar:         62        7       20       15       20
 nvalwt,nvalw:       42        2       15       12       13
 ncsft:             280
 map(*)=    -1  9 10  1  2  3  4 11  5 12  6  7  8
 mu(*)=      0  0  0  0
 syml(*) =   1  1  2  3
 rmo(*)=     2  3  1  1

 indx01:    42 indices saved in indxv(*)
===================================ROOT # 1===================================

 rdhciv: CI vector file information:
 Hermit Integral Program : SIFS version  hochtor2        Thu Jun  5 14:27:11 2003
 /                                                                               
 Hermit Integral Program : SIFS version  niko            Fri Apr 16 12:09:51 1999
 mdrt2_title                                                                     
 mofmt: formatted orbitals label=morb    niko            Fri Apr 16 12:11:03 1999
 SIFS file created by program tran.      hochtor2        Thu Jun  5 14:27:11 2003
 cidrt_title                                                                     
 energy computed by program ciudg.       hochtor2        Thu Jun  5 14:27:15 2003

 lenrec =    4096 lenci =       280 ninfo =  6 nenrgy =  6 ntitle =  8

 Max. overlap with ref vector #        1
 Valid ci vector #        1
 Method:        0       98% overlap
 energy( 1)=  6.203440944703E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -3.413186659445E+01, ietype=    6,   fcore energy of type: H1(*)   
 energy( 3)= -3.862239737331E+01, ietype=-1026,   total energy of type: MRSDCI  
 energy( 4)=  8.959625558540E-05, ietype=-2055, cnvginf energy of type: CI-Resid
 energy( 5)=  4.695041511127E-08, ietype=-2056, cnvginf energy of type: CI-D.E. 
 energy( 6)=  3.815693250312E-09, ietype=-2057, cnvginf energy of type: CI-ApxDE
==================================================================================

 space is available for   3997904 coefficients.

 updated histogram parameters:
 csfmn = 0.0000E+00 csfmx = 1.0000E+00 fhist = 5.0000E-01 nhist =  20

 this program will print the csfs generated from
 the drt according to the following print options :

 1) run in batch mode: all valid roots are automatically
    analysed and csf info is printed by default contribution
    threshold 0.01 
 2) run in interactive mode

 input menu number [  0]:
================================================================================
===================================VECTOR # 1===================================
================================================================================


 rdcivnew:      53 coefficients were selected.
 workspace: ncsfmx=     280
 ncsfmx= 280

 histogram parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =     280 ncsft =     280 ncsf =      53
 nhist =  20 fhist = 0.50000

    cmin                cmax        num  '*'=     1 csfs.
 ----------          ----------   ----- ---------|---------|---------|---------|
 5.0000E-01 <= |c| < 1.0000E+00       1 *
 2.5000E-01 <= |c| < 5.0000E-01       0
 1.2500E-01 <= |c| < 2.5000E-01       0
 6.2500E-02 <= |c| < 1.2500E-01       3 ***
 3.1250E-02 <= |c| < 6.2500E-02       7 *******
 1.5625E-02 <= |c| < 3.1250E-02      23 ***********************
 7.8125E-03 <= |c| < 1.5625E-02      19 *******************
 3.9062E-03 <= |c| < 7.8125E-03       0
 1.9531E-03 <= |c| < 3.9062E-03       0
 9.7656E-04 <= |c| < 1.9531E-03       0
 4.8828E-04 <= |c| < 9.7656E-04       0
 2.4414E-04 <= |c| < 4.8828E-04       0
 1.2207E-04 <= |c| < 2.4414E-04       0
 6.1035E-05 <= |c| < 1.2207E-04       0
 3.0518E-05 <= |c| < 6.1035E-05       0
 1.5259E-05 <= |c| < 3.0518E-05       0
 7.6294E-06 <= |c| < 1.5259E-05       0
 3.8147E-06 <= |c| < 7.6294E-06       0
 0.0000E+00 <= |c| < 3.8147E-06       0
                                  ----- ---------|---------|---------|---------|
                  total read =       53 total stored =      53

 from the selected csfs,
 min(|csfvec(:)|) = 1.0199E-02    max(|csfvec(:)|) = 9.7606E-01
 csfs will be printed based on coefficient magnitudes.

 current csfvec(*) selection parameters:
 csfmn = 1.0000E-02 csfmx = 1.0000E+00 fhist = 5.0000E-01
 nhist =  20 icsfmn =       1 icsfmx =     280 ncsft =     280 ncsf =      53

 i:slabel(i) =  1:   1  2:   2  3:   3  4:   4

 frozen orbital =    1
 symfc(*)       =    1
 label          =    1
 rmo(*)         =    1

 internal level =    1    2    3    4
 syml(*)        =    1    1    2    3
 label          =    1    1    2    3
 rmo(*)         =    2    3    1    1

 printing selected csfs in sorted order from cmin = 0.01000 to cmax = 1.00000

   indcsf     c     c**2   v  lab:rmo  lab:rmo   step(*)
  ------- -------- ------- - ---- --- ---- --- ------------
        1  0.97606 0.95270 z*                    3123
      158 -0.08012 0.00642 w             3:  2  33120
       32  0.07059 0.00498 y             1:  4  11223
       15  0.06715 0.00451 y             1:  4  13023
       29 -0.06108 0.00373 y             3:  2  11322
      219 -0.04919 0.00242 w    1:  4    3:  2 121122
      180 -0.04597 0.00211 w    1:  5    2:  2 123003
       11 -0.04164 0.00173 y             2:  2  13203
      264 -0.04056 0.00164 w             1:  4  30123
      203 -0.03648 0.00133 w    1:  4    3:  2 121212
      167  0.03253 0.00106 w    1:  4    3:  2 123012
      215 -0.03118 0.00097 w    1:  4    2:  2 121203
        3  0.03106 0.00097 y             1:  4  13320
      147 -0.03103 0.00096 w             1:  4  33120
      164 -0.02670 0.00071 w    2:  2    3:  2 123102
       50  0.02614 0.00068 x    2:  2    3:  2 113202
      160 -0.02594 0.00067 w             3:  3  33120
       35  0.02532 0.00064 y             1:  7  11223
      275 -0.02453 0.00060 w             3:  2  30123
      225  0.02095 0.00044 w    1:  6    3:  3 121122
       51  0.02068 0.00043 x    2:  2    3:  3 113202
      168 -0.02043 0.00042 w    1:  5    3:  2 123012
       34  0.01964 0.00039 y             1:  6  11223
       18  0.01959 0.00038 y             1:  7  13023
       30  0.01942 0.00038 y             3:  3  11322
       54  0.01875 0.00035 x    1:  5    3:  2 113022
       89  0.01864 0.00035 x    1:  4    2:  2 112203
      105  0.01753 0.00031 x    1:  4    1:  5 112023
       13 -0.01741 0.00030 y             3:  3  13122
       97  0.01720 0.00030 x    1:  4    3:  3 112122
      165 -0.01691 0.00029 w    2:  2    3:  3 123102
       10  0.01659 0.00028 y             3:  4  13212
        8 -0.01631 0.00027 y             3:  2  13212
      172 -0.01625 0.00026 w    1:  5    3:  3 123012
       58  0.01522 0.00023 x    1:  5    3:  3 113022
      231  0.01466 0.00022 w             1:  4  31023
       20  0.01457 0.00021 y             3:  2  12312
       93  0.01420 0.00020 x    1:  4    3:  2 112122
       16 -0.01390 0.00019 y             1:  5  13023
        2 -0.01390 0.00019 z*                    1323
      152 -0.01332 0.00018 w             1:  6  33120
      233 -0.01330 0.00018 w             1:  5  31023
      209  0.01284 0.00016 w    1:  6    3:  3 121212
      216 -0.01263 0.00016 w    1:  5    2:  2 121203
       14  0.01201 0.00014 y             3:  4  13122
      269 -0.01155 0.00013 w             1:  6  30123
       24  0.01107 0.00012 y             1:  4  12123
        5  0.01102 0.00012 y             1:  6  13320
      220 -0.01094 0.00012 w    1:  5    3:  2 121122
       81  0.01091 0.00012 x    1:  4    3:  3 112212
      223 -0.01055 0.00011 w    1:  4    3:  3 121122
       38 -0.01047 0.00011 y             1:  6  10323
      277 -0.01020 0.00010 w             3:  3  30123
           53 csfs were printed in this range.
