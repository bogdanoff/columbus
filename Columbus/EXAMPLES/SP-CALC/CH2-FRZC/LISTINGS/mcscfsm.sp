 total ao core energy =    6.593365134
 MCSCF calculation performed for  1 DRT.

 DRT  first state   no.of aver. states   weights
  1   ground state          2             0.500 0.500

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a  
 Total number of electrons:     6
 Spin multiplicity:            1
 Number of active orbitals:    3
 Number of active electrons:   2
 Total number of CSFs:         6

 Number of active-double rotations:         6
 Number of active-active rotations:         0
 Number of double-virtual rotations:       14
 Number of active-virtual rotations:       21
 
 iter     emc (average)    demc       wnorm      knorm      apxde  qcoupl
    1    -38.7633738772  3.876E+01  2.504E-01  1.168E-01  1.325E-02  F   *not conv.*     
    2    -38.8289846121  6.561E-02  4.617E-02  9.316E-02  6.329E-04  F   *not conv.*     
    3    -38.8296631023  6.785E-04  1.334E-03  6.845E-01  2.640E-04  F   *not conv.*     
    4    -38.8307541283  1.091E-03  6.002E-03  6.541E-01  1.460E-03  F   *not conv.*     
    5    -38.8329225087  2.168E-03  6.948E-03  1.393E-01  1.361E-04  F   *not conv.*     
    6    -38.8330777488  1.552E-04  9.890E-04  2.099E-02  3.002E-06  F   *not conv.*     
    7    -38.8330809947  3.246E-06  1.715E-04  1.502E-03  2.767E-08  T   *not conv.*     
    8    -38.8330810224  2.770E-08  5.317E-07  1.964E-07  5.222E-14  T   *not conv.*     

 final mcscf convergence values:
    9    -38.8330810224  5.684E-14  1.801E-07  4.595E-08  4.184E-15  T   *converged*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 wt 0.500 total energy=      -38.860378443, rel. (eV)=   0.000000
   DRT #1 state # 2 wt 0.500 total energy=      -38.805783602, rel. (eV)=   1.485602
   ------------------------------------------------------------


