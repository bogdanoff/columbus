1
 program ciudg      
 multireference single and double excitation configuration
 interaction based on the graphical unitary group approach.


 references:  h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. s 15, 91 (1981).
              r. shepard, r. a. bair, r. a. eades, a. f. wagner,
                  m. j. davis, l. b. harding, and t. h. dunning,
                  int j. quantum chem. s 17, 613 (1983).
              r. ahlrichs, h.-j. boehm, c. ehrhardt, p. scharf,
                  h. schiffer, h. lischka, and m. schindler,
                  j. comp. chem. 6, 200 (1985).
              r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).

 This Version of Program CIUDG is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              CIUDG       **
     **    PROGRAM VERSION:      2009-03.    **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************


================ Computing sorting integral file structure ================

                    -----z----- -----y----- -----x----- -----w----- ---total---

                CSFs        22         147           0           0         169
      internal walks        22          21           0           0          43
valid internal walks        22          21           0           0          43
  MR-CIS calculation - skip 3- and 4-external 2e integrals
 getinfoarray: info=                     6 :                     1
                  8192                  6552                  8192
                  5460                     0
 icd(3)=                   157 ci%nnlev=                    78  l2rec=
                  8192  n2max=                  5460
 lcore1,lcore2=             353858673             353834323
 lencor,maxblo             353894400                 60000
========================================
 current settings:
 minbl3          81
 minbl4          81
 locmaxbl3      382
 locmaxbuf      191
 maxbl3       60000
 maxbl3       60000
 maxbl4       60000
 maxbuf       30006
========================================
 Orig.  diagonal integrals:  1electron:        12
                             0ext.    :        30
                             2ext.    :        70
                             4ext.    :        56


 Orig. off-diag. integrals:  4ext.    :       546
                             3ext.    :      1155
                             2ext.    :      1260
                             1ext.    :       735
                             0ext.    :       165
                             2ext. SO :         0
                             1ext. SO :         0
                             0ext. SO :         0
                             1electron:        28


 Sorted integrals            3ext.  w :         0 x :         0
                             4ext.  w :         0 x :         0


Cycle #  1 sortfile size=     98301(       3 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core 353858673
Cycle #  2 sortfile size=     98301(       3 records of    32767) #buckets=   2
distributed memory consumption per node=         0 available core 353858673
 minimum size of srtscr:     32767 WP (     1 records)
 maximum size of srtscr:     98301 WP (     3 records)
 compressed index vector length=                     1
 echo of the input for program ciudg:
 ------------------------------------------------------------------------
  &input
  NTYPE = 0,
  GSET = 0,
   DAVCOR =10,
  NCOREL = 6
  NROOT = 2
  IVMODE = 3
  NBKITR = 1
  NVBKMN = 1
  RTOLBK = 1e-3
  NITER = 20
  NVCIMN = 1
  RTOLCI = 1e-3,1e-3
  NVCIMX = 6
  NVRFMX = 6
  NVBKMX = 6
  IDEN  = 1
  CSFPRN = 10,
 /&end
 ------------------------------------------------------------------------
lodens (list->root)=  2
invlodens (root->list)= -1  1
 USING SEGMENTS OF EQUAL SIZE

****************  list of control variables  ****************
 lvlprt =    0      nroot  =    2      noldv  =   0      noldhv =   0
 nunitv =    2      nbkitr =    1      niter  =  20      davcor =  10
 csfprn =   10      ivmode =    3      istrt  =   0      vout   =   0
 iortls =    0      nvbkmx =    6      ibktv  =  -1      ibkthv =  -1
 nvcimx =    6      icitv  =   -1      icithv =  -1      frcsub =   0
 nvbkmn =    2      nvcimn =    2      maxseg =   4      nrfitr =  30
 ncorel =    6      nvrfmx =    6      nvrfmn =   2      iden   =   1
 itran  =    0      froot  =    0      rtmode =   0      ncouple=   1
 skipso =    F      dalton2=    0      molcas =   0      finalv =   0
 finalw =    0      cosmocalc=   0    with_tsklst=   0
 nsegwx =    1     1     1     1
 nseg0x =    1     1     1     1
 nseg1x =    1     1     1     1
 nseg2x =    1     1     1     1
 nseg3x =    1     1     1     1
 nseg4x =    1     1     1     1
 no0ex  =      0    no1ex  =      0    no2ex  =     0    no3ex  =     0
 no4ex  =      0    nodiag =      0
 cdg4ex =    1      c3ex1ex=    1      c2ex0ex=   1
 fileloc=    0     0     0     0     0     0     0     1     1     1
 directhd=   1      noaqccshift_zyxw=      0
 critical_crit=-1.00000    critical_delta= 0.05000

 ctol   = 0.010000    lrtshift=1.000000    smalld =0.001000


 convergence tolerances of bk and full diagonalization steps
 root #       rtolbk        rtol
 ------      --------      ------
    1        1.000E-03    1.000E-03
    2        1.000E-04    1.000E-03
 Computing density:                    .drt1.state2
 using                      1  nodes and                      1  cores.
 szdg/szodg per processor=                  3064                  3830
 Main memory management:
 global                1 DP per process
 vdisk                 0 DP per process
 stack                 0 DP per process
 core          353894399 DP per process

********** Integral sort section *************


 workspace allocation information: lencor= 353894399

 echo of the input for program cisrt:
 ------------------------------------------------------------------------
  &input
  maxbl3=60000
  maxbl4=60000
  &end
 ------------------------------------------------------------------------
 
 ( 6) listing file:                    ciudgls             
 ( 5) input file:                      cisrtin   
 (17) cidrt file:                      cidrtfl             
 (11) transformed integrals file:      moints    
 (12) diagonal integral file:          diagint             
 (13) off-diagonal integral file:      ofdgint             
 (31) 4-external w integrals file:     fil4w               
 (32) 4-external x integrals file:     fil4x               
 (33) 3-external w integrals file:     fil3w               
 (34) 3-external x integrals file:     fil3x               
 (21) scratch da sorting file:         srtscr              
 (12) 2-e integral file [fsplit=2]:    moints2   

 input integral file header information:
 Hermit Integral Program : SIFS version  r24n12            17:45:20.188 30-Aug-12
 user-specified FREEZE(*) used in program tran.                                  
 mo coefficients generated by scfpq                                              
 SIFS file created by program tran.      r24n12            17:45:20.329 30-Aug-12
  cidrt_title                                                                    
 MO-coefficients from mcscf.x                                                    
  total ao core energy =  -27.854182361                                          
 SIFS file created by program tran.      r24n12            17:45:20.709 30-Aug-12

 input energy(*) values:
 energy( 1)=  6.593365133910E+00, ietype=   -1,    core energy of type: Nuc.Rep.

 total core energy =   6.593365133910E+00

 nsym = 1 nmot=  12

 symmetry  =    1
 slabel(*) =  A  
 nmpsy(*)  =   12

 info(*) =          1      8192      6552      8192      5460         0

 orbital labels, i:molab(i)=
   1:tout:001   2:tout:002   3:tout:003   4:tout:004   5:tout:005   6:tout:006   7:tout:007   8:tout:008   9:tout:009  10:tout:010
  11:tout:011  12:tout:012

 input parameters:
 prnopt=  0
 ldamin=    4095 ldamax=   32767 ldainc=      64
 maxbuf=   30006 maxbl3=   60000 maxbl4=   60000 intmxo=     766
  Using 32 bit compression 

 drt information:
  cidrt_title                                                                    
 nmotd =  12 nfctd =   0 nfvtc =   0 nmot  =  12
 nlevel =  12 niot  =   5 lowinl=   8
 orbital-to-level map(*)
    8   9  10  11  12   1   2   3   4   5   6   7
 compressed map(*)
    8   9  10  11  12   1   2   3   4   5   6   7
 levsym(*)
    1   1   1   1   1   1   1   1   1   1   1   1
 repartitioning mu(*)=
   2.  2.  0.  0.  0.

 new core energy added to the energy(*) list.
 from the integral file: h1_core= -3.444754749468E+01

 indxdg: diagonal integral statistics.
 total number of integrals contributing to diagonal matrix elements:       156
 number with all external indices:        56
 number with half external - half internal indices:        70
 number with all internal indices:        30

 indxof: off-diagonal integral statistics.
    4-external integrals: num=        546 strt=          1
    3-external integrals: num=       1155 strt=        547
    2-external integrals: num=       1260 strt=       1702
    1-external integrals: num=        735 strt=       2962
    0-external integrals: num=        165 strt=       3697

 total number of off-diagonal integrals:        3861


 indxof(2nd)  ittp=   3 numx(ittp)=        1260
 indxof(2nd)  ittp=   4 numx(ittp)=         735
 indxof(2nd)  ittp=   5 numx(ittp)=         165

 intermediate da file sorting parameters:
 nbuk=   2 lendar=   32767 nipbk=   21844 nipsg= 353719695
 pro2e        1      79     157     235     313     328     343     421   44109   87797
   120564  128756  134216  156055

 pro2e:      1357 integrals read in     1 records.
 pro1e        1      79     157     235     313     328     343     421   44109   87797
   120564  128756  134216  156055
 pro1e: eref =   -9.676846526810909E+00
 total size of srtscr:                     3  records of                  32767 
 WP =                786408 Bytes

 new core energy added to the energy(*) list.
 from the hamiltonian repartitioning, eref= -9.676846526811E+00
 putdg        1      79     157     235    1001   33768   55613     421   44109   87797
   120564  128756  134216  156055

 putf:       4 buffers of length     766 written to file 12
 diagonal integral file completed.
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd      1260         3      1702      1702      1260      3861
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd       735         4      2962      2962       735      3861
ptofdgf: num,ittp,ipos,istrtx,numx,maxrd       165         5      3697      3697       165      3861

 putf:       5 buffers of length     766 written to file 13
 off-diagonal files sort completed.
 executing brd_struct for cisrtinfo
cisrtinfo:
bufszi   766
 diagfile 4ext:      56 2ext:      70 0ext:      30
 fil4w,fil4x  :     546 fil3w,fil3x :    1155
 ofdgint  2ext:    1260 1ext:     735 0ext:     165so0ext:       0so1ext:       0so2ext:       0
buffer minbl4      81 minbl3      81 maxbl2      84nbas:   7   0   0   0   0   0   0   0 maxbuf 30006
 CIUDG version 5.9.7 ( 5-Oct-2004)

 workspace allocation information: lcore= 353894399

 core energy values from the integral file:
 energy( 1)=  6.593365133910E+00, ietype=   -1,    core energy of type: Nuc.Rep.
 energy( 2)= -3.444754749468E+01, ietype=    6,   fcore energy of type: H1(*)   
 energy( 3)= -9.676846526811E+00, ietype=    5,   fcore energy of type: Vref(*) 

 total core repulsion energy = -3.753102888758E+01
 nmot  =    12 niot  =     5 nfct  =     0 nfvt  =     0
 nrow  =    26 nsym  =     1 ssym  =     1 lenbuf=  1600
 nwalk,xbar:         43       22       21        0        0
 nvalwt,nvalw:       43       22       21        0        0
 ncsft:             169
 total number of valid internal walks:      43
 nvalz,nvaly,nvalx,nvalw =       22      21       0       0

 cisrt info file parameters:
 file number  12 blocksize    766
 mxbld    766
 nd4ext,nd2ext,nd0ext    56    70    30
 n4ext,n3ext,n2ext,n1ext,n0ext,n2int,n1int,n0int      546     1155     1260      735      165        0        0        0
 minbl4,minbl3,maxbl2    81    81    84
 maxbuf 30006
 number of external orbitals per symmetry block:   7
 nmsym   1 number of internal orbitals   5
 executing brd_struct for drt
 executing brd_struct for orbinf
 executing brd_struct for momap
 calcthrxt: niot,maxw1=                     5                     1
 block size     0
 pthz,pthy,pthx,pthw:    22    21     0     0 total internal walks:      43
 maxlp3,n2lp,n1lp,n0lp     1     0     0     0
 orbsym(*)= 1 1 1 1 1

 setref:        6 references kept,
                0 references were marked as invalid, out of
                6 total.
 nmb.of records onel     1
 nmb.of records 2-ext     2
 nmb.of records 1-ext     1
 nmb.of records 0-ext     1
 nmb.of records 2-int     0
 nmb.of records 1-int     0
 nmb.of records 0-int     0
 ---------memory usage in DP -----------------
 < n-ex core usage >
     routines:
    fourex            60855
    threx             60019
    twoex              1319
    onex                893
    allin               766
    diagon             1296
               =======
   maximum            60855
 
  __ static summary __ 
   reflst                22
   hrfspc                22
               -------
   static->              22
 
  __ core required  __ 
   totstc                22
   max n-ex           60855
               -------
   totnec->           60877
 
  __ core available __ 
   totspc         353894399
   totnec -           60877
               -------
   totvec->       353833522

 number of external paths / symmetry
 vertex x      21
 vertex w      28
segment: free space=   353833522
 reducing frespc by                   376 to              353833146 
  for index/conft/indsym storage .
 resegmenting ...



                   segmentation summary for type all-internal
 -------------------------------------------------------------------------------
 seg.      no. of|    no. of|  starting|  internal|  starting|  starting|
  no.    internal|        ci|       csf|     walks|      walk|       DRT|
            paths|  elements|    number|     /seg.|    number|    record|
 -------------------------------------------------------------------------------
  Z 1          22|        22|         0|        22|         0|         1|
 -------------------------------------------------------------------------------
  Y 2          21|       147|        22|        21|        22|         2|
 -------------------------------------------------------------------------------
max. additional memory requirements:index=           4DP  conft+indsym=          88DP  drtbuffer=         284 DP

dimension of the ci-matrix ->>>       169

 executing brd_struct for civct
 gentasklist: ntask=                     6
                    TASKLIST
----------------------------------------------------------------------------------------------------
TASK# BRA# KET#  T-TYPE    DESCR.   SEGMENTTYPE    SEGEL              SEGCI          VWALKS   
----------------------------------------------------------------------------------------------------
     1  2   1    11      one-ext yz   1X  2 1      21      22        147         22      21      22
     2  1   1     1      allint zz    OX  1 1      22      22         22         22      22      22
     3  2   2     5      0ex2ex yy    OX  2 2      21      21        147        147      21      21
     4  2   2    42      four-ext y   4X  2 2      21      21        147        147      21      21
     5  1   1    75      dg-024ext z  OX  1 1      22      22         22         22      22      22
     6  2   2    76      dg-024ext y  OX  2 2      21      21        147        147      21      21
----------------------------------------------------------------------------------------------------
REDTASK #   1 TIME=   5.000 N=  1 (task/type/sgbra)=(   1/11/0) (
REDTASK #   2 TIME=   4.000 N=  1 (task/type/sgbra)=(   2/ 1/0) (
REDTASK #   3 TIME=   3.000 N=  1 (task/type/sgbra)=(   3/ 5/0) (
REDTASK #   4 TIME=   2.000 N=  1 (task/type/sgbra)=(   4/42/1) (
REDTASK #   5 TIME=   1.000 N=  1 (task/type/sgbra)=(   5/75/1) (
REDTASK #   6 TIME=   0.000 N=  1 (task/type/sgbra)=(   6/76/1) (
 initializing v-file: 1:                   169

    ---------trial vector generation----------

    trial vectors will be created by: 

    (ivmode= 3) diagonalizing h in the reference space.                     

      2 vectors will be written to unit 11 beginning with logical record   1

            2 vectors will be created
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         124 2x:           0 4x:           0
All internal counts: zz :         338 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 reference space has dimension       6
 dsyevx: computed roots 1 to    4(converged:   4)

    root           eigenvalues
    ----           ------------
       1         -38.8603784430
       2         -38.8057836019
       3         -38.6992147588
       4         -38.0263754459

 strefv generated    2 initial ci vector(s).
    ---------end of vector generation---------

 ufvoutnew: ... writing  recamt=                    22

         vector  1 from unit 11 written to unit 49 filename cirefv              
 ufvoutnew: ... writing  recamt=                    22

         vector  2 from unit 11 written to unit 49 filename cirefv              

 ************************************************************************
 beginning the bk-type iterative procedure (nzcsf=    22)...
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:               169
 number of initial trial vectors:                         2
 number of initial matrix-vector products:                0
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             2
 number of iterations:                                    1
 residual norm convergence criteria:               0.001000  0.000100

          starting bk iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         217 2x:          51 4x:          21
All internal counts: zz :         338 yy:           0 xx:           0 ww:           0
One-external counts: yz :         486 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         217 2x:          51 4x:          21
All internal counts: zz :         338 yy:           0 xx:           0 ww:           0
One-external counts: yz :         486 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2
   ht   1    -1.32934956
   ht   2     0.00000000    -1.27475471

          calcsovref: tciref block   1

              civs   1       civs   2
 refs   1    1.00000       3.333928E-18
 refs   2    0.00000        1.00000    

          calcsovref: scrb block   1

                ci   1         ci   2
 civs   1   -1.00000       8.381380E-15
 civs   2  -8.378046E-15   -1.00000    

          calcsovref: sovref block   1

              v      1       v      2
 ref    1   -1.00000       8.378046E-15
 ref    2  -8.378046E-15   -1.00000    

          reference overlap matrix  block   1

                ci   1         ci   2
 ref:   1    -1.00000000     0.00000000
 ref:   2     0.00000000    -1.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -38.8603784430 -6.6613E-15  1.8158E-02  1.8644E-01  1.0000E-03
 mr-sdci #  1  2    -38.8057836019  3.1086E-15  0.0000E+00  1.8680E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mr-sdci  convergence not reached after  1 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -38.8603784430 -6.6613E-15  1.8158E-02  1.8644E-01  1.0000E-03
 mr-sdci #  1  2    -38.8057836019  3.1086E-15  0.0000E+00  1.8680E-01  1.0000E-04
 
diagon:itrnv=   2
 expansion vectors are not transformed.
 matrix-vector products are not transformed.

    3 expansion eigenvectors written to unit nvfile (= 11)
    2 matrix-vector products written to unit nhvfil (= 10)

 ************************************************************************
 beginning the ci iterative diagonalization procedure... 
 ************************************************************************

               initial diagonalization conditions:

 number of configuration state functions:               169
 number of initial trial vectors:                         3
 number of initial matrix-vector products:                2
 maximum dimension of the subspace vectors:               6
 number of roots to converge:                             2
 number of iterations:                                   20
 residual norm convergence criteria:               0.001000  0.001000

          starting ci iteration   1

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         217 2x:          51 4x:          21
All internal counts: zz :         338 yy:         273 xx:           0 ww:           0
One-external counts: yz :         486 yx:           0 yw:           0
Two-external counts: yy :         153 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3
   ht   1    -1.32934956
   ht   2     0.00000000    -1.27475471
   ht   3     0.01815786     0.00000000     0.00384002

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3
 refs   1    1.00000       3.333928E-18  -1.744154E-14
 refs   2    0.00000        1.00000       7.522716E-15

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3
 civs   1   0.994862      -2.610797E-12   0.101243    
 civs   2  -2.505988E-12   -1.00000      -1.161181E-12
 civs   3  -0.960251      -7.873741E-12    9.43587    

          calcsovref: sovref block   1

              v      1       v      2       v      3
 ref    1   0.994862      -2.610800E-12   0.101243    
 ref    2  -2.513212E-12   -1.00000      -1.090197E-12

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.99486172     0.00000000     0.10124309
 ref:   2     0.00000000    -1.00000000     0.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  1  1    -38.8779045997  1.7526E-02  4.5020E-04  1.8639E-02  1.0000E-03
 mr-sdci #  1  2    -38.8057836019 -2.2204E-16  0.0000E+00  1.8680E-01  1.0000E-03
 mr-sdci #  1  3    -37.1680628115 -3.6297E-01  0.0000E+00  7.5295E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   2

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         217 2x:          51 4x:          21
All internal counts: zz :         338 yy:         273 xx:           0 ww:           0
One-external counts: yz :         486 yx:           0 yw:           0
Two-external counts: yy :         153 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4
   ht   1    -1.32934956
   ht   2     0.00000000    -1.27475471
   ht   3     0.01815786     0.00000000     0.00384002
   ht   4    -0.06245850     0.00000000     0.00150921    -0.00494163

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4
 refs   1    1.00000       3.333928E-18  -1.744154E-14   4.698606E-02
 refs   2    0.00000        1.00000       7.522716E-15  -7.343490E-13

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4
 civs   1   0.960975      -1.019750E-11   -1.09394       1.363217E-02
 civs   2  -2.669715E-12   -1.00000       2.297208E-11  -7.262637E-13
 civs   3  -0.976983      -1.070886E-11  -0.706604       -9.41176    
 civs   4   0.704847       1.415053E-10    22.3861       -2.41906    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4
 ref    1   0.994093      -3.548728E-12  -4.210921E-02  -0.100030    
 ref    2  -3.194668E-12   -1.00000       6.527540E-12   9.793687E-13

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.99409297     0.00000000    -0.04210921    -0.10002994
 ref:   2     0.00000000    -1.00000000     0.00000000     0.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  2  1    -38.8782220801  3.1748E-04  2.0508E-05  5.3844E-03  1.0000E-03
 mr-sdci #  2  2    -38.8057836019  2.2204E-16  0.0000E+00  1.8680E-01  1.0000E-03
 mr-sdci #  2  3    -38.5569624521  1.3889E+00  0.0000E+00  4.4465E-01  1.0000E-04
 mr-sdci #  2  4    -37.1518575096 -3.7917E-01  0.0000E+00  7.5180E-01  1.0000E-04
 
 root number  1 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   3

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         217 2x:          51 4x:          21
All internal counts: zz :         338 yy:         273 xx:           0 ww:           0
One-external counts: yz :         486 yx:           0 yw:           0
Two-external counts: yy :         153 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1    -1.32934956
   ht   2     0.00000000    -1.27475471
   ht   3     0.01815786     0.00000000     0.00384002
   ht   4    -0.06245850     0.00000000     0.00150921    -0.00494163
   ht   5     0.00684740     0.00000000     0.00001809     0.00047101    -0.00004870

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1    1.00000       3.333928E-18  -1.744154E-14   4.698606E-02  -5.149842E-03
 refs   2    0.00000        1.00000       7.522716E-15  -7.343490E-13  -3.464244E-13

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 civs   1   0.963727      -8.279385E-12   -1.12442       0.311886      -0.225036    
 civs   2  -2.490938E-12   -1.00000       1.503553E-11   8.189180E-11  -5.464710E-11
 civs   3  -0.978042      -1.143273E-11  -0.635681       -3.68637       -8.88157    
 civs   4   0.747162       1.668869E-10    21.1920        13.6183       -11.4921    
 civs   5   0.934515       6.473043E-10   -17.4963        196.968       -132.875    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1   0.994021      -3.771545E-12  -3.859255E-02  -6.259704E-02  -8.071831E-02
 ref    2  -3.370712E-12   -1.00000       5.529579E-12   3.629160E-12  -2.436468E-13

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     0.99402051     0.00000000    -0.03859255    -0.06259704    -0.08071831
 ref:   2     0.00000000    -1.00000000     0.00000000     0.00000000     0.00000000

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  3  1    -38.8782412450  1.9165E-05  0.0000E+00  7.4444E-04  1.0000E-03
 mr-sdci #  3  2    -38.8057836019  0.0000E+00  2.0978E-02  1.8680E-01  1.0000E-03
 mr-sdci #  3  3    -38.5619486475  4.9862E-03  0.0000E+00  4.3628E-01  1.0000E-04
 mr-sdci #  3  4    -37.7961598455  6.4430E-01  0.0000E+00  6.4986E-01  1.0000E-04
 mr-sdci #  3  5    -36.8596985898 -6.7133E-01  0.0000E+00  5.5656E-01  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   4

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         217 2x:          51 4x:          21
All internal counts: zz :         338 yy:         273 xx:           0 ww:           0
One-external counts: yz :         486 yx:           0 yw:           0
Two-external counts: yy :         153 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5         ht   6
   ht   1    -1.32934956
   ht   2     0.00000000    -1.27475471
   ht   3     0.01815786     0.00000000     0.00384002
   ht   4    -0.06245850     0.00000000     0.00150921    -0.00494163
   ht   5     0.00684740     0.00000000     0.00001809     0.00047101    -0.00004870
   ht   6     0.00000000     0.02097787     0.00000000     0.00000000     0.00000000     0.00470964

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5       civs   6
 refs   1    1.00000       3.333928E-18  -1.744154E-14   4.698606E-02  -5.149842E-03   6.371014E-12
 refs   2    0.00000        1.00000       7.522716E-15  -7.343490E-13  -3.464244E-13   1.589379E-13

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 civs   1  -0.963727       7.957729E-12   -1.12442      -0.311886      -1.135783E-10  -0.225036    
 civs   2   2.738061E-12   0.994111       7.327001E-12  -9.119940E-11   0.108368      -6.062070E-11
 civs   3   0.978042       7.252095E-12  -0.635681        3.68637      -1.814398E-10   -8.88157    
 civs   4  -0.747162      -9.727749E-11    21.1920       -13.6183      -9.416183E-10   -11.4921    
 civs   5  -0.934515      -1.269779E-09   -17.4963       -196.968      -1.980921E-08   -132.875    
 civs   6  -8.590454E-13  -0.919872      -8.960016E-11  -4.479615E-10    8.43842      -5.541665E-10

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5       v      6
 ref    1  -0.994021       4.065695E-12  -3.859255E-02   6.259704E-02  -2.045668E-12  -8.071831E-02
 ref    2   3.617835E-12   0.994111      -2.178948E-12  -1.293676E-11   0.108368      -6.217248E-12

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6
 ref:   1    -0.99402051     0.00000000    -0.03859255     0.06259704     0.00000000    -0.08071831
 ref:   2     0.00000000     0.99411085     0.00000000     0.00000000     0.10836793     0.00000000

 trial vector basis is being transformed.  new dimension:   2

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  4  1    -38.8782412450 -6.6613E-16  0.0000E+00  7.4444E-04  1.0000E-03
 mr-sdci #  4  2    -38.8251948656  1.9411E-02  1.9238E-04  1.5634E-02  1.0000E-03
 mr-sdci #  4  3    -38.5619486475  6.6613E-16  0.0000E+00  4.3628E-01  1.0000E-04
 mr-sdci #  4  4    -37.7961598455  2.7756E-16  0.0000E+00  6.4986E-01  1.0000E-04
 mr-sdci #  4  5    -37.1722732597  3.1257E-01  0.0000E+00  5.2716E-01  1.0000E-04
 mr-sdci #  4  6    -36.8596985898 -6.7133E-01  0.0000E+00  5.5656E-01  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   5

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         217 2x:          51 4x:          21
All internal counts: zz :         338 yy:         273 xx:           0 ww:           0
One-external counts: yz :         486 yx:           0 yw:           0
Two-external counts: yy :         153 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3
   ht   1    -1.34721236
   ht   2     0.00000000    -1.29416598
   ht   3     0.00000000    -0.00997451    -0.00015467

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3
 refs   1  -0.994021       4.065695E-12   5.738105E-13
 refs   2   3.617835E-12   0.994111       7.717848E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3
 civs   1   -1.00000       6.184203E-13   4.374879E-11
 civs   2   1.983058E-14   0.992247      -0.539835    
 civs   3  -5.066653E-12    1.01168        69.4938    

          calcsovref: sovref block   1

              v      1       v      2       v      3
 ref    1   0.994021       3.999965E-12  -5.805736E-12
 ref    2  -3.637225E-12   0.994212      -3.130417E-04

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3
 ref:   1     0.99402051     0.00000000     0.00000000
 ref:   2     0.00000000     0.99421163    -0.00031304

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  5  1    -38.8782412450  1.1102E-15  0.0000E+00  7.4444E-04  1.0000E-03
 mr-sdci #  5  2    -38.8253895152  1.9465E-04  2.1898E-05  4.2847E-03  1.0000E-03
 mr-sdci #  5  3    -37.9067383893 -6.5521E-01  0.0000E+00  5.8190E-01  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   6

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         217 2x:          51 4x:          21
All internal counts: zz :         338 yy:         273 xx:           0 ww:           0
One-external counts: yz :         486 yx:           0 yw:           0
Two-external counts: yy :         153 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4
   ht   1    -1.34721236
   ht   2     0.00000000    -1.29416598
   ht   3     0.00000000    -0.00997451    -0.00015467
   ht   4     0.00000000     0.02215948     0.00015956    -0.00038020

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4
 refs   1  -0.994021       4.065695E-12   5.738105E-13   3.316993E-12
 refs   2   3.617835E-12   0.994111       7.717848E-03  -1.747589E-02

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4
 civs   1   -1.00000       2.817191E-12  -3.063729E-10   5.568775E-10
 civs   2  -2.436074E-13    1.00267      -0.833379        3.08273    
 civs   3  -6.802910E-12    1.08466       -66.0974       -22.6497    
 civs   4  -1.578362E-11   0.642174       -78.7416        169.847    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4
 ref    1   0.994021       4.028701E-12   2.040146E-12   9.371360E-12
 ref    2  -3.636679E-12   0.993919       3.747856E-02  -7.846169E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4
 ref:   1     0.99402051     0.00000000     0.00000000     0.00000000
 ref:   2     0.00000000     0.99391878     0.03747856    -0.07846169

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  6  1    -38.8782412450  0.0000E+00  0.0000E+00  7.4444E-04  1.0000E-03
 mr-sdci #  6  2    -38.8254035777  1.4062E-05  2.3956E-06  1.6559E-03  1.0000E-03
 mr-sdci #  6  3    -37.9926774145  8.5939E-02  0.0000E+00  5.3600E-01  1.0000E-04
 mr-sdci #  6  4    -37.5068885664 -2.8927E-01  0.0000E+00  5.2028E-01  1.0000E-04
 
 root number  2 is used to define the new expansion vector.
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

          starting ci iteration   7

 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         217 2x:          51 4x:          21
All internal counts: zz :         338 yy:         273 xx:           0 ww:           0
One-external counts: yz :         486 yx:           0 yw:           0
Two-external counts: yy :         153 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


 Final subspace hamiltonian 

                ht   1         ht   2         ht   3         ht   4         ht   5
   ht   1    -1.34721236
   ht   2     0.00000000    -1.29416598
   ht   3     0.00000000    -0.00997451    -0.00015467
   ht   4     0.00000000     0.02215948     0.00015956    -0.00038020
   ht   5     0.00000000    -0.00560988    -0.00004778     0.00009317    -0.00002441

          calcsovref: tciref block   1

              civs   1       civs   2       civs   3       civs   4       civs   5
 refs   1  -0.994021       4.065695E-12   5.738105E-13   3.316993E-12   5.378157E-12
 refs   2   3.617835E-12   0.994111       7.717848E-03  -1.747589E-02   4.401123E-03

          calcsovref: scrb block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 civs   1   -1.00000       7.999907E-12  -2.084038E-09   2.023586E-09  -2.090723E-09
 civs   2  -1.154189E-13    1.00036      -4.753242E-02   0.666329        4.02307    
 civs   3  -7.332936E-12    1.09590       -46.9882       -51.6198        10.9652    
 civs   4  -2.099992E-11   0.740769       -104.523        97.0902        121.786    
 civs   5  -4.969528E-11   0.901679       -316.851        321.795       -466.457    

          calcsovref: sovref block   1

              v      1       v      2       v      3       v      4       v      5
 ref    1   0.994021       4.050433E-12  -6.356093E-12   1.431350E-11  -3.845085E-12
 ref    2  -3.640891E-12   0.993952       2.224014E-02  -1.646807E-02  -9.725523E-02

          reference overlap matrix  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5
 ref:   1     0.99402051     0.00000000     0.00000000     0.00000000     0.00000000
 ref:   2     0.00000000     0.99395161     0.02224014    -0.01646807    -0.09725523

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  7  1    -38.8782412450  0.0000E+00  0.0000E+00  7.4444E-04  1.0000E-03
 mr-sdci #  7  2    -38.8254057378  2.1601E-06  9.3685E-08  3.7035E-04  1.0000E-03
 mr-sdci #  7  3    -38.1578220700  1.6514E-01  0.0000E+00  4.0053E-01  1.0000E-04
 mr-sdci #  7  4    -37.6725933343  1.6570E-01  0.0000E+00  5.1863E-01  1.0000E-04
 mr-sdci #  7  5    -37.2276327969  5.5360E-02  0.0000E+00  5.3710E-01  1.0000E-04
 
================ TIMING STATISTICS FOR JOB     ================
time for subspace matrix construction  0.000000
time for cinew                         0.000000
time for eigenvalue solver             0.000000
time for vector access                 0.000000

 mr-sdci  convergence criteria satisfied after  7 iterations.

 final mr-sdci  convergence information:

   iter      root         energy      deltae       apxde    residual       rtol
        ---- ----   --------------  ----------  ----------  ----------  ----------
 mr-sdci #  7  1    -38.8782412450  0.0000E+00  0.0000E+00  7.4444E-04  1.0000E-03
 mr-sdci #  7  2    -38.8254057378  2.1601E-06  9.3685E-08  3.7035E-04  1.0000E-03
 mr-sdci #  7  3    -38.1578220700  1.6514E-01  0.0000E+00  4.0053E-01  1.0000E-04
 mr-sdci #  7  4    -37.6725933343  1.6570E-01  0.0000E+00  5.1863E-01  1.0000E-04
 mr-sdci #  7  5    -37.2276327969  5.5360E-02  0.0000E+00  5.3710E-01  1.0000E-04

####################CIUDGINFO####################

   ci vector at position   1 energy=  -38.878241244957
   ci vector at position   2 energy=  -38.825405737760

################END OF CIUDGINFO################

 
diagon:itrnv=   0
    2 of the   6 expansion vectors are transformed.
    2 of the   5 matrix-vector products are transformed.

    2 expansion eigenvectors written to unit nvfile (= 11)
    2 matrix-vector products written to unit nhvfil (= 10)
maximum overlap with reference    1(overlap= 0.99402)

 information on vector: 1 from unit 11 written to unit 48 filename civout              
maximum overlap with reference    2(overlap= 0.99395)

 information on vector: 2 from unit 11 written to unit 48 filename civout              


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 1) =       -38.8782412450

                                                       internal orbitals

                                          level       1    2    3    4    5

                                          orbital     1    2    3    4    5

                                         symmetry   a    a    a    a    a  

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       1  0.967275                        +-   +-   +-           
 z*  1  1       3  0.033211                        +-   +-   +          - 
 z*  1  1       4 -0.220464                        +-   +-        +-      
 z*  1  1       6 -0.059980                        +-   +-             +- 
 z   1  1      17 -0.049226                        +    +-    -   +-      
 y   1  1      23  0.023924              1( a  )   +-   +-    -           
 y   1  1      33  0.013752              4( a  )   +-   +-         -      
 y   1  1      59 -0.043190              2( a  )   +-    -   +          - 
 y   1  1      60  0.036082              3( a  )   +-    -   +          - 
 y   1  1      63 -0.032059              6( a  )   +-    -   +          - 
 y   1  1      94 -0.014537              2( a  )   +-   +     -         - 
 y   1  1      98 -0.012919              6( a  )   +-   +     -         - 
 y   1  1     121 -0.034878              1( a  )    -   +-   +          - 
 y   1  1     125 -0.018724              5( a  )    -   +-   +          - 
 y   1  1     127 -0.026834              7( a  )    -   +-   +          - 
 y   1  1     152  0.013894              4( a  )   +    +-    -    -      
 y   1  1     156 -0.012830              1( a  )   +    +-    -         - 
 y   1  1     162 -0.010632              7( a  )   +    +-    -         - 

 ci coefficient statistics:
           rq > 0.1                2
      0.1> rq > 0.01              16
     0.01> rq > 0.001             27
    0.001> rq > 0.0001             4
   0.0001> rq > 0.00001            0
  0.00001> rq > 0.000001           0
 0.000001> rq                    120
           all                   169
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         124 2x:           0 4x:           0
All internal counts: zz :         338 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.967275021050    -37.586645414031
     2     2     -0.000000000002      0.000000000083
     3     3      0.033210626008     -1.292022318357
     4     4     -0.220463871515      8.571847576214
     5     5     -0.000000000001      0.000000000042
     6     6     -0.059979636002      2.342466774800

 number of reference csfs (nref) is     6.  root number (iroot) is  1.
 c0**2 =   0.98892579  c**2 (all zwalks) =   0.99144496

 pople ci energy extrapolation is computed with  6 correlated electrons.

 eref      =    -38.860160793329   "relaxed" cnot**2         =   0.988925787406
 eci       =    -38.878241244957   deltae = eci - eref       =  -0.018080451628
 eci+dv1   =    -38.878441471722   dv1 = (1-cnot**2)*deltae  =  -0.000200226765
 eci+dv2   =    -38.878443713906   dv2 = dv1 / cnot**2       =  -0.000202468949
 eci+dv3   =    -38.878446006876   dv3 = dv1 / (2*cnot**2-1) =  -0.000204761919
 eci+pople =    -38.878376726183   (  6e- scaled deltae )    =  -0.018215932854


 --- list of ci coefficients ( ctol =   1.00E-02 )  total energy( 2) =       -38.8254057378

                                                       internal orbitals

                                          level       1    2    3    4    5

                                          orbital     1    2    3    4    5

                                         symmetry   a    a    a    a    a  

 path  s ms    csf#    c(i)    ext. orb.(sym)
 z*  1  1       2  0.993274                        +-   +-   +     -      
 z*  1  1       5  0.037045                        +-   +-        +     - 
 z   1  1      15 -0.025550                        +    +-   +-    -      
 z   1  1      18  0.016723                        +    +-    -   +     - 
 y   1  1      30 -0.014352              1( a  )   +-   +-         -      
 y   1  1      40  0.060486              4( a  )   +-   +-              - 
 y   1  1      73 -0.032285              2( a  )   +-    -        +     - 
 y   1  1      74  0.029758              3( a  )   +-    -        +     - 
 y   1  1      77 -0.024195              6( a  )   +-    -        +     - 
 y   1  1      91 -0.011198              6( a  )   +-   +     -    -      
 y   1  1     135 -0.027723              1( a  )    -   +-        +     - 
 y   1  1     139 -0.015019              5( a  )    -   +-        +     - 
 y   1  1     141 -0.020125              7( a  )    -   +-        +     - 
 y   1  1     149 -0.034488              1( a  )   +    +-    -    -      
 y   1  1     153  0.025527              5( a  )   +    +-    -    -      
 y   1  1     155 -0.023146              7( a  )   +    +-    -    -      

 ci coefficient statistics:
           rq > 0.1                1
      0.1> rq > 0.01              15
     0.01> rq > 0.001             22
    0.001> rq > 0.0001             1
   0.0001> rq > 0.00001            1
  0.00001> rq > 0.000001           0
 0.000001> rq                    129
           all                   169
 ========= Executing OUT-OF-CORE method ========


====================================================================================================
Diagonal     counts:  0x:         124 2x:           0 4x:           0
All internal counts: zz :         338 yy:           0 xx:           0 ww:           0
One-external counts: yz :           0 yx:           0 yw:           0
Two-external counts: yy :           0 ww:           0 xx:           0 xz:           0 wz:           0 wx:           0
Three-ext.   counts: yx :           0 yw:           0

SO-0ex       counts: zz :           0 yy:           0 xx:           0 ww:           0
SO-1ex       counts: yz :           0 yx:           0 yw:           0
SO-2ex       counts: yy :           0 xx:           0 wx:           0
====================================================================================================


  iref  icsf         v(icsf)             hv(icsf)
     1     1      0.000000000005     -0.000000000186
     2     2      0.993273597038    -38.544595452412
     3     3      0.000000000005     -0.000000000176
     4     4      0.000000000009     -0.000000000354
     5     5      0.037045286453     -1.441464500166
     6     6      0.000000000000      0.000000000003

 number of reference csfs (nref) is     6.  root number (iroot) is  2.
 c0**2 =   0.98796479  c**2 (all zwalks) =   0.98899416

 pople ci energy extrapolation is computed with  6 correlated electrons.

 eref      =    -38.805763883570   "relaxed" cnot**2         =   0.987964791822
 eci       =    -38.825405737760   deltae = eci - eref       =  -0.019641854189
 eci+dv1   =    -38.825642131564   dv1 = (1-cnot**2)*deltae  =  -0.000236393804
 eci+dv2   =    -38.825645011270   dv2 = dv1 / cnot**2       =  -0.000239273511
 eci+dv3   =    -38.825647962002   dv3 = dv1 / (2*cnot**2-1) =  -0.000242224243
 eci+pople =    -38.825565898480   (  6e- scaled deltae )    =  -0.019802014910
 passed aftci ... 
 readint2: molcas,dalton2=                     0                     0
 files%faoints=aoints              
lodens (list->root)=  1  2
                       Size (real*8) of d2temp for two-external contributions       1050
 
                       Size (real*8) of d2temp for all-internal contributions        165
                       Size (real*8) of d2temp for one-external contributions        735
                       Size (real*8) of d2temp for two-external contributions       1050
size_thrext:  lsym   l1    ksym   k1strt   k1       cnt3 
                1    1    1    1    8      231
                1    2    1    1    8      231
                1    3    1    1    8      231
                1    4    1    1    8      231
                1    5    1    1    8      231
                       Size (real*8) of d2temp for three-external contributions       1155
                       Size (real*8) of d2temp for four-external contributions        630
 serial operation: forcing vdisk for temporary dd012 matrices
location of d2temp files... fileloc(dd012)=       1
location of d2temp files... fileloc(d3)=       1
location of d2temp files... fileloc(d4)=       1
 files%dd012ext =  unit=  22  vdsk=   1  filestart=       1
 files%d3ext =     unit=  23  vdsk=   1  filestart=    3831
 files%d4ext =     unit=  24  vdsk=   1  filestart=   63831
            0xdiag    0ext      1ext      2ext      3ext      4ext
d2off                   767      1533      2299         1         1
d2rec                     1         1         2         1         1
recsize                 766       766       766     60000     60000
d2bufferlen=          60000
maxbl3=               60000
maxbl4=               60000
  allocated                 123831  DP for d2temp 
sifcfg setup: record length 4096 DP
# d1 elements per record  3272
# d2 elements per record  2730
  The MR-CISD density will be calculated.
 item #                     1 suffix=:.drt1.state1:
 read_civout: repnuc=  -37.5310288875767     
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method:  0 last record  0max overlap with ref# 99% root-following 0
 MR-CISD energy:   -38.87824124    -1.34721236
 residuum:     0.00074444

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.99402051     0.00000000     0.00000000     0.00000000     0.00000000    -0.08071831     0.00000000     0.00000000
 ref:   2     0.00000000     0.99395161     0.02224014    -0.01646807    -0.09725523     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.99402051     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
 ref:   2     0.00000000     0.99395161     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 computing final density
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    1
--------------------------------------------------------------------------------
================================================================================
   DYZ=      77  DYX=       0  DYW=       0
   D0Z=      83  D0Y=      66  D0X=       0  D0W=       0
  DDZI=      61 DDYI=      51 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=      21 DDXE=       0 DDWE=       0
================================================================================
Trace of MO density:     6.000000
    6  correlated and     0  frozen core electrons

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     1.99458049     0.00000000    -0.01630345     0.00000000    -0.00648943     0.00400356     0.00000000     0.00000000
   MO   2     0.00000000     1.99519590     0.00000000     0.00000000     0.00000000     0.00000000     0.01038983     0.01008871
   MO   3    -0.01630345     0.00000000     1.88334919     0.00000000     0.04226055     0.03286025     0.00000000     0.00000000
   MO   4     0.00000000     0.00000000     0.00000000     0.10263788     0.00000000     0.00000000     0.00000000     0.00000000
   MO   5    -0.00648943     0.00000000     0.04226055     0.00000000     0.01568149     0.00111662     0.00000000     0.00000000
   MO   6     0.00400356     0.00000000     0.03286025     0.00000000     0.00111662     0.00200274     0.00000000     0.00000000
   MO   7     0.00000000     0.01038983     0.00000000     0.00000000     0.00000000     0.00000000     0.00215599    -0.00152720
   MO   8     0.00000000     0.01008871     0.00000000     0.00000000     0.00000000     0.00000000    -0.00152720     0.00144435
   MO   9     0.00000000     0.00000000     0.00000000    -0.00530045     0.00000000     0.00000000     0.00000000     0.00000000
   MO  10     0.00361958     0.00000000    -0.01179129     0.00000000    -0.00008124     0.00054408     0.00000000     0.00000000
   MO  11     0.00000000    -0.00098049     0.00000000     0.00000000     0.00000000     0.00000000     0.00157543    -0.00120829
   MO  12     0.00378100     0.00000000     0.00482902     0.00000000     0.00044495     0.00118288     0.00000000     0.00000000

                MO   9         MO  10         MO  11         MO  12
   MO   1     0.00000000     0.00361958     0.00000000     0.00378100
   MO   2     0.00000000     0.00000000    -0.00098049     0.00000000
   MO   3     0.00000000    -0.01179129     0.00000000     0.00482902
   MO   4    -0.00530045     0.00000000     0.00000000     0.00000000
   MO   5     0.00000000    -0.00008124     0.00000000     0.00044495
   MO   6     0.00000000     0.00054408     0.00000000     0.00118288
   MO   7     0.00000000     0.00000000     0.00157543     0.00000000
   MO   8     0.00000000     0.00000000    -0.00120829     0.00000000
   MO   9     0.00041259     0.00000000     0.00000000     0.00000000
   MO  10     0.00000000     0.00047207     0.00000000     0.00054634
   MO  11     0.00000000     0.00000000     0.00120375     0.00000000
   MO  12     0.00000000     0.00054634     0.00000000     0.00086354

Natural orbital populations,block 1
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     1.99701836     1.99530151     1.88256897     0.10291198     0.01473233     0.00454642     0.00262193     0.00014300
              MO     9       MO    10       MO    11       MO    12       MO
  occ(*)=     0.00013850     0.00000907     0.00000519     0.00000274


 total number of electrons =    6.0000000000

 test slabel:                     1
  a  


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        a   partial gross atomic populations
   ao class       1a         2a         3a         4a         5a         6a  
    to_ s       1.997018   1.995302   1.882569   0.102912   0.014732   0.004546
 
   ao class       7a         8a         9a        10a        11a        12a  
    to_ s       0.002622   0.000143   0.000138   0.000009   0.000005   0.000003


                        gross atomic populations
     ao           to_
      s         6.000000
    total       6.000000
 

 Total number of electrons:    6.00000000

 item #                     2 suffix=:.drt1.state2:
 read_civout: repnuc=  -37.5310288875767     
================================================================================
  Reading record                      1  of civout
 INFO:ref#  1vector#  1 method:  0 last record  0max overlap with ref# 99% root-following 0
 MR-CISD energy:   -38.87824124    -1.34721236
 residuum:     0.00074444
================================================================================
  Reading record                      2  of civout
 INFO:ref#  2vector#  2 method:  0 last record  1max overlap with ref# 99% root-following 0
 MR-CISD energy:   -38.82540574    -1.29437685
 residuum:     0.00037035
 deltae:     0.00000216
 apxde:     0.00000009

          sovref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.99402051     0.00000000     0.00000000     0.00000000     0.00000000    -0.08071831     0.00000000     0.00000000
 ref:   2     0.00000000     0.99395161     0.02224014    -0.01646807    -0.09725523     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40

          tciref  block   1

                ci   1         ci   2         ci   3         ci   4         ci   5         ci   6         ci   7         ci   8
 ref:   1     0.99402051     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
 ref:   2     0.00000000     0.99395161     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

                ci   9         ci  10         ci  11         ci  12         ci  13         ci  14         ci  15         ci  16

                ci  17         ci  18         ci  19         ci  20         ci  21         ci  22         ci  23         ci  24

                ci  25         ci  26         ci  27         ci  28         ci  29         ci  30         ci  31         ci  32

                ci  33         ci  34         ci  35         ci  36         ci  37         ci  38         ci  39         ci  40
 computing final density
 =========== Executing IN-CORE method ==========
--------------------------------------------------------------------------------
  1e-density for root #    2
--------------------------------------------------------------------------------
================================================================================
   DYZ=      77  DYX=       0  DYW=       0
   D0Z=      83  D0Y=      66  D0X=       0  D0W=       0
  DDZI=      61 DDYI=      51 DDXI=       0 DDWI=       0
  DDZE=       0 DDYE=      21 DDXE=       0 DDWE=       0
================================================================================
Trace of MO density:     6.000000
    6  correlated and     0  frozen core electrons

           D1 (irrep)  block   1

                MO   1         MO   2         MO   3         MO   4         MO   5         MO   6         MO   7         MO   8
   MO   1     1.99489695     0.00000000     0.02325823     0.00000000     0.00368429    -0.00535931     0.00000000     0.00000000
   MO   2     0.00000000     1.99701172     0.00000000     0.00000000     0.00000000     0.00000000    -0.01133629    -0.00783850
   MO   3     0.02325823     0.00000000     0.99106993     0.00000000     0.03609247    -0.01271942     0.00000000     0.00000000
   MO   4     0.00000000     0.00000000     0.00000000     0.99630152     0.00000000     0.00000000     0.00000000     0.00000000
   MO   5     0.00368429     0.00000000     0.03609247     0.00000000     0.00971404    -0.00093389     0.00000000     0.00000000
   MO   6    -0.00535931     0.00000000    -0.01271942     0.00000000    -0.00093389     0.00222044     0.00000000     0.00000000
   MO   7     0.00000000    -0.01133629     0.00000000     0.00000000     0.00000000     0.00000000     0.00123035    -0.00094025
   MO   8     0.00000000    -0.00783850     0.00000000     0.00000000     0.00000000     0.00000000    -0.00094025     0.00094652
   MO   9     0.00000000     0.00000000     0.00000000     0.00476570     0.00000000     0.00000000     0.00000000     0.00000000
   MO  10    -0.00446719     0.00000000     0.00726856     0.00000000     0.00087470    -0.00052442     0.00000000     0.00000000
   MO  11     0.00000000    -0.00638487     0.00000000     0.00000000     0.00000000     0.00000000     0.00095114    -0.00067471
   MO  12    -0.00081965     0.00000000    -0.00202924     0.00000000    -0.00034557     0.00145444     0.00000000     0.00000000

                MO   9         MO  10         MO  11         MO  12
   MO   1     0.00000000    -0.00446719     0.00000000    -0.00081965
   MO   2     0.00000000     0.00000000    -0.00638487     0.00000000
   MO   3     0.00000000     0.00726856     0.00000000    -0.00202924
   MO   4     0.00476570     0.00000000     0.00000000     0.00000000
   MO   5     0.00000000     0.00087470     0.00000000    -0.00034557
   MO   6     0.00000000    -0.00052442     0.00000000     0.00145444
   MO   7     0.00000000     0.00000000     0.00095114     0.00000000
   MO   8     0.00000000     0.00000000    -0.00067471     0.00000000
   MO   9     0.00379985     0.00000000     0.00000000     0.00000000
   MO  10     0.00000000     0.00098814     0.00000000    -0.00026439
   MO  11     0.00000000     0.00000000     0.00081141     0.00000000
   MO  12     0.00000000    -0.00026439     0.00000000     0.00100913

Natural orbital populations,block 1
              MO     1       MO     2       MO     3       MO     4       MO     5       MO     6       MO     7       MO     8
  occ(*)=     1.99712728     1.99547151     0.99632440     0.99207104     0.00849477     0.00377697     0.00307767     0.00271914
              MO     9       MO    10       MO    11       MO    12       MO
  occ(*)=     0.00078013     0.00015122     0.00000352     0.00000236


 total number of electrons =    6.0000000000

 test slabel:                     1
  a  


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        a   partial gross atomic populations
   ao class       1a         2a         3a         4a         5a         6a  
    to_ s       1.997127   1.995472   0.996324   0.992071   0.008495   0.003777
 
   ao class       7a         8a         9a        10a        11a        12a  
    to_ s       0.003078   0.002719   0.000780   0.000151   0.000004   0.000002


                        gross atomic populations
     ao           to_
      s         6.000000
    total       6.000000
 

 Total number of electrons:    6.00000000

 DA ...
