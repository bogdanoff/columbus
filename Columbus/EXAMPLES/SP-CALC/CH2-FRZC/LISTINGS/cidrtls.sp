 
 program cidrt 7.0  

 distinct row table construction, reference csf selection, and internal
 walk selection for multireference single- and double-excitation
configuration interaction.

 references:  r. shepard, i. shavitt, r. m. pitzer, d. c. comeau, m. pepper
                  h. lischka, p. g. szalay, r. ahlrichs, f. b. brown, and
                  j.-g. zhao, int. j. quantum chem. symp. 22, 149 (1988).
              h. lischka, r. shepard, f. b. brown, and i. shavitt,
                  int. j. quantum chem. symp. 15, 91 (1981).

 based on the initial version by  Ron Shepard

 extended for spin-orbit CI calculations ( Russ Pitzer, OSU)

 and large active spaces (Thomas Müller, FZ(21 Juelich)

 This Version of Program CIDRT is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de

*********************** File revision status: ***********************
* cidrt1.F9 Revision: 1.1.2.12          Date: 2012/03/28 23:01:46   * 
* cidrt2.F9 Revision: 1.1.2.21          Date: 2012/05/23 12:42:07   * 
* cidrt3.F9 Revision: 1.1.2.3           Date: 2012/02/08 18:18:52   * 
* cidrt4.F9 Revision: 1.1.2.3           Date: 2012/02/08 18:18:52   * 
********************************************************************

 workspace allocation parameters: lencor= 353894400 mem1=         0 ifirst=         1
 expanded "keystrokes" are being written to file:
 /home/lv70151/plasser1/programs/Columbus/COL_TEST/MCSCF/CH2/FC/RUN3.mrcis/WORK/c
 Spin-Orbit CI Calculation?(y,[n])
 Spin-Free Calculation
 
 input the spin multiplicity [  0]:
 spin multiplicity, smult            :   1    singlet 
 input the total number of electrons [  0]:
 total number of electrons, nelt     :     1
 inconsistent data, nelt,smult       :     1   1
 input the total number of electrons [  0]:
 total number of electrons, nelt     :     6
 input the number of irreps (1:8) [  0]:
 point group dimension, nsym         :     1
 enter symmetry labels:(y,[n])
 enter 1 labels (a4):
 enter symmetry label, default=   1
 symmetry labels: (symmetry, slabel)
 ( 1,  a  ) 
 input nmpsy(*):
 nmpsy(*)=        12
 
   symmetry block summary
 block(*)=         1
 slabel(*)=      a  
 nmpsy(*)=        12
 
 total molecular orbitals            :    12
 input the molecular spatial symmetry (irrep 1:nsym) [  0]:
 state spatial symmetry label        :  a  
 
 input the frozen core orbitals (sym(i),rmo(i),i=1,nfct):
 total frozen core orbitals, nfct    :     0
 no frozen core orbitals entered
 
 number of frozen core orbitals      :     0
 number of frozen core electrons     :     0
 number of internal electrons        :     6
 
 input the frozen virtual orbitals (sym(i),rmo(i),i=1,nfvt):
 total frozen virtual orbitals, nfvt :     0

 no frozen virtual orbitals entered
 
 input the internal orbitals (sym(i),rmo(i),i=1,niot):
 niot                                :     5
 
 modrt(*)=         1   2   3   4   5
 slabel(*)=      a   a   a   a   a  
 
 total number of orbitals            :    12
 number of frozen core orbitals      :     0
 number of frozen virtual orbitals   :     0
 number of internal orbitals         :     5
 number of external orbitals         :     7
 
 orbital-to-level mapping vector
 map(*)=           8   9  10  11  12   1   2   3   4   5   6   7
 
 input the number of ref-csf doubly-occupied orbitals [  0]:
 (ref) doubly-occupied orbitals      :     2
 
 no. of internal orbitals            :     5
 no. of doubly-occ. (ref) orbitals   :     2
 no. active (ref) orbitals           :     3
 no. of active electrons             :     2
 
 input the active-orbital, active-electron occmnr(*):
   3  4  5
 input the active-orbital, active-electron occmxr(*):
   3  4  5
 
 actmo(*) =        3   4   5
 occmnr(*)=        0   0   2
 occmxr(*)=        2   2   2
 reference csf cumulative electron occupations:
 modrt(*)=         1   2   3   4   5
 occmnr(*)=        2   4   4   4   6
 occmxr(*)=        2   4   6   6   6
 
 input the active-orbital bminr(*):
   3  4  5
 input the active-orbital bmaxr(*):
   3  4  5
 reference csf b-value constraints:
 modrt(*)=         1   2   3   4   5
 bminr(*)=         0   0   0   0   0
 bmaxr(*)=         0   0   2   2   2
 input the active orbital smaskr(*):
   3  4  5
 modrt:smaskr=
   1:1000   2:1000   3:1111   4:1111   5:1111
 
 input the maximum excitation level from the reference csfs [  2]:
 maximum excitation from ref. csfs:  :     1
 number of internal electrons:       :     6
 
 input the internal-orbital mrsdci occmin(*):
   1  2  3  4  5
 input the internal-orbital mrsdci occmax(*):
   1  2  3  4  5
 mrsdci csf cumulative electron occupations:
 modrt(*)=         1   2   3   4   5
 occmin(*)=        1   3   3   3   5
 occmax(*)=        6   6   6   6   6
 
 input the internal-orbital mrsdci bmin(*):
   1  2  3  4  5
 input the internal-orbital mrsdci bmax(*):
   1  2  3  4  5
 mrsdci b-value constraints:
 modrt(*)=         1   2   3   4   5
 bmin(*)=          0   0   0   0   0
 bmax(*)=          6   6   6   6   6
 
 input the internal-orbital smask(*):
   1  2  3  4  5
 modrt:smask=
   1:1111   2:1111   3:1111   4:1111   5:1111
 
 internal orbital summary:
 block(*)=         1   1   1   1   1
 slabel(*)=      a   a   a   a   a  
 rmo(*)=           1   2   3   4   5
 modrt(*)=         1   2   3   4   5
 
 reference csf info:
 occmnr(*)=        2   4   4   4   6
 occmxr(*)=        2   4   6   6   6
 
 bminr(*)=         0   0   0   0   0
 bmaxr(*)=         0   0   2   2   2
 
 
 mrsdci csf info:
 occmin(*)=        1   3   3   3   5
 occmax(*)=        6   6   6   6   6
 
 bmin(*)=          0   0   0   0   0
 bmax(*)=          6   6   6   6   6
 

 a priori removal of distinct rows:

 input the level, a, and b values for the vertices 
 to be removed (-1/ to end).

 input level, a, and b (-1/ to end):
 no vertices marked for removal
 
 impose generalized interacting space restrictions?(y,[n])
 generalized interacting space restrictions will not be imposed.
 multp(*)=
  hmult                     0
 lxyzir   0   0   0
 symmetry of spin functions (spnir)
       --------------------------Ms ----------------------------
   S     1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19
   1     1  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   2     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   3     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   4     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   5     1  0  0  0  1  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   6     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   7     0  0  0  1  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   8     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
   9     1  0  0  0  1  0  0  0  1  0  0  0  0  0  0  0  0  0  0
  10     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  11     0  0  0  1  0  0  0  1  0  0  0  0  0  0  0  0  0  0  0
  12     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  13     1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  0  0  0
  14     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  15     0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  0  0  0  0
  16     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  17     1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0
  18     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0
  19     0  0  0  1  0  0  0  1  0  0  0  1  0  0  0  1  0  0  0

 number of rows in the drt :  26

 manual arc removal step:


 input the level, a, b, and step values 
 for the arcs to be removed (-1/ to end).

 input the level, a, b, and step (-1/ to end):
 remarc:   0 arcs removed out of   0 specified.

 xbarz=          22
 xbary=          54
 xbarx=          42
 xbarw=          44
        --------
 nwalk=         162
 input the range of drt levels to print (l1,l2):
 levprt(*)        -1   0

 reference-csf selection step 1:
 total number of z-walks in the drt, nzwalk=      22

 input the list of allowed reference symmetries:
 allowed reference symmetries:             1
 allowed reference symmetry labels:      a  
 keep all of the z-walks as references?(y,[n])
 all z-walks are initially deleted.
 
 generate walks while applying reference drt restrictions?([y],n)
 reference drt restrictions will be imposed on the z-walks.
 
 impose additional orbital-group occupation restrictions?(y,[n])
 
 apply primary reference occupation restrictions?(y,[n])
 
 manually select individual walks?(y,[n])

 step 1 reference csf selection complete.
        6 csfs initially selected from      22 total walks.

 beginning step-vector based selection.
 enter [internal_orbital_step_vector/disposition] pairs:

 enter internal orbital step vector, (-1/ to end):
   1  2  3  4  5

 step 2 reference csf selection complete.
        6 csfs currently selected from      22 total walks.

 beginning numerical walk based selection.
 enter positive walk numbers to add walks,
 negative walk numbers to delete walks, and zero to end:

 input reference walk number (0 to end) [  0]:

 numerical walk-number based selection complete.
        6 reference csfs selected from      22 total z-walks.
 
 input the reference occupations, mu(*):
 reference occupations:
 mu(*)=            2   2   0   0   0
 
 number of step vectors saved:      6

 exlimw: beginning excitation-based walk selection...
 exlimw: nref=                     6

  number of valid internal walks of each symmetry:

       a  
      ----
 z      22
 y      21
 x       0
 w       0

 csfs grouped by internal walk symmetry:

       a  
      ----
 z      22
 y     147
 x       0
 w       0

 total csf counts:
 z-vertex:       22
 y-vertex:      147
 x-vertex:        0
 w-vertex:        0
           --------
 total:         169
 
 this is an obsolete prompt.(y,[n])

 final mrsdci walk selection step:

 nvalw(*)=      22      21       0       0 nvalwt=      43

 enter positive walk numbers to add walks,
 negative walk numbers to delete walks, and zero to end.

 input mrsdci walk number (0 to end) [  0]:

 end of manual mrsdci walk selection.
 number added=   0 number removed=   0

 nvalw(*)=      22      21       0       0 nvalwt=      43

 lprune input numv1,nwalk=                    43                   162
 lprune input xbar(1,1),nref=                    22                     6

 lprune: l(*,*,*) pruned with nwalk=     162 nvalwt=      43=  22  21   0   0
 lprune:  z-drt, nprune=    31
 lprune:  y-drt, nprune=    30
 lprune: wx-drt, nprune=    55

 xbarz=          22
 xbary=          21
 xbarx=           0
 xbarw=           0
        --------
 nwalk=          43
 levprt(*)        -1   0

 beginning the reference csf index recomputation...

     iref   iwalk  step-vector
   ------  ------  ------------
        1       1  33300
        2       2  33120
        3       3  33102
        4       4  33030
        5       5  33012
        6       6  33003
 indx01:     6 elements set in vec01(*)

 beginning the valid upper walk index recomputation...
 indx01:    43 elements set in vec01(*)

 beginning the final csym(*) computation...

  number of valid internal walks of each symmetry:

       a  
      ----
 z      22
 y      21
 x       0
 w       0

 csfs grouped by internal walk symmetry:

       a  
      ----
 z      22
 y     147
 x       0
 w       0

 total csf counts:
 z-vertex:       22
 y-vertex:      147
 x-vertex:        0
 w-vertex:        0
           --------
 total:         169
 
 input a title card, default=cidrt_title
 title card:
  cidrt_title                                                                   
  
 
 input a drt file name, default=cidrtfl
 drt and indexing arrays will be written to file:
 /home/lv70151/plasser1/programs/Columbus/COL_TEST/MCSCF/CH2/FC/RUN3.mrcis/WORK/c
 
 write the drt file?([y],n)
 drt file is being written...
 wrtstr:  a  
nwalk=      43 cpos=       6 maxval=    9 cmprfactor=   86.05 %.
nwalk=      43 cpos=       1 maxval=   99 cmprfactor=   95.35 %.
nwalk=      43 cpos=       1 maxval=  999 cmprfactor=   93.02 %.
 compressed with: nwalk=      43 cpos=       1 maxval=   99 cmprfactor=   95.35 %.
initial index vector length:        43
compressed index vector length:         1reduction:  97.67%
nwalk=      22 cpos=       3 maxval=    9 cmprfactor=   86.36 %.
nwalk=      22 cpos=       2 maxval=   99 cmprfactor=   81.82 %.
 compressed with: nwalk=      22 cpos=       3 maxval=    9 cmprfactor=   86.36 %.
initial ref vector length:        22
compressed ref vector length:         3reduction:  86.36%
