
 Work memory size (LMWORK) :    13107200 =100   megabytes.

 Default basis set library used :
        /sphome/kedziora/dalton/basis/                              


    $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    $$$$$$$$$$$  DALTON - An electronic structure program  $$$$$$$$$$$
    $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

               This is output from DALTON (beta-version 0.9) 

                          Principal authors:

            Trygve Helgaker,     University of Oslo,        Norway 
            Hans Joergen Jensen, University of Odense,      Denmark
            Poul Joergensen,     University of Aarhus,      Denmark
            Henrik Koch,         University of Aarhus,      Denmark
            Jeppe Olsen,         University of Lund,        Sweden 
            Hans Aagren,         University of Linkoeping,  Sweden 

                          Contributors:

            Torgeir Andersen,    University of Oslo,        Norway 
            Keld L. Bak,         University of Copenhagen,  Denmark
            Vebjoern Bakken,     University of Oslo,        Norway 
            Ove Christiansen,    University of Aarhus,      Denmark
            Paal Dahle,          University of Oslo,        Norway 
            Erik K. Dalskov,     University of Odense,      Denmark
            Thomas Enevoldsen,   University of Odense,      Denmark
            Asger Halkier,       University of Aarhus,      Denmark
            Hanne Heiberg,       University of Oslo,        Norway 
            Dan Jonsson,         University of Linkoeping,  Sweden 
            Sheela Kirpekar,     University of Odense,      Denmark
            Rika Kobayashi,      University of Aarhus,      Denmark
            Alfredo S. de Meras, Valencia University,       Spain  
            Kurt Mikkelsen,      University of Aarhus,      Denmark
            Patrick Norman,      University of Linkoeping,  Sweden 
            Martin J. Packer,    University of Sheffield,   UK     
            Kenneth Ruud,        University of Oslo,        Norway 
            Trond Saue,          University of Oslo,        Norway 
            Peter Taylor,        San Diego Superc. Center,  USA    
            Olav Vahtras,        University of Linkoeping,  Sweden

                                             Release Date:  August 1996
------------------------------------------------------------------------


      
     NOTE:
      
     This is an experimental code for the evaluation of molecular
     properties using (MC)SCF/CC wave functions. The authors accept
      no responsibility for the performance of the code or for the
     correctness of the results.
      
     The code (in whole or part) is not to be reproduced for further
     distribution without the written permission of T. Helgaker,
     H. J. Aa. Jensen or P. Taylor.
      
     If results obtained with this code are published, an
     appropriate citation would be:
      
     T. Helgaker, H. J. Aa. Jensen, P.Joergensen, H. Koch,
     J. Olsen, H. Aagren, T. Andersen, K. L. Bak, V. Bakken,
     O. Christiansen, P. Dahle, E. K. Dalskov, T. Enevoldsen,
     A. Halkier, H. Heiberg, D. Jonsson, S. Kirpekar, R. Kobayashi,
     A. S. de Meras, K. V. Mikkelsen, P. Norman, M. J. Packer,
     K. Ruud, T.Saue, P. R. Taylor, and O. Vahtras:
     DALTON, an electronic structure program"



     ******************************************
     **    PROGRAM:              DALTON      **
     **    PROGRAM VERSION:      5.4.0.0     **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************



 <<<<<<<<<< OUTPUT FROM GENERAL INPUT PROCESSING >>>>>>>>>>




 Default print level:        0

    Integral sections will be executed
    Starting in Integral Section -



 *************************************************************************
 ****************** Output from HERMIT input processing ******************
 *************************************************************************



 Default print level:        2


 Calculation of one- and two-electron Hamiltonian integrals.


 The following one-electron property integrals are calculated:

          - overlap integrals
          - Cartesian multipole moment integrals of orders 4 and lower
          - electronic angular momentum around the origin


 Changes of defaults for READIN:
 -------------------------------


 Maximum number of primitives per integral block :    9



 *************************************************************************
 ****************** Output from READIN input processing ******************
 *************************************************************************



  Title Cards
  -----------

                                                                          
                                                                          


  Symmetry Operations
  -------------------

  Symmetry operations: 2



                      SYMGRP:Point group information
                      ------------------------------

Point group: C2v

   * The point group was generated by:

      Reflection in the yz-plane
      Reflection in the xz-plane

   * Group multiplication table

        |  E   C2z  Oxz  Oyz
   -----+--------------------
     E  |  E 
    C2z | C2z   E 
    Oxz | Oxz  Oyz   E 
    Oyz | Oyz  Oxz  C2z   E 

   * Character table

        |  E   C2z  Oxz  Oyz
   -----+--------------------
    A1  |   1    1    1    1
    B1  |   1   -1    1   -1
    B2  |   1   -1   -1    1
    A2  |   1    1   -1   -1

   * Direct product table

        | A1   B1   B2   A2 
   -----+--------------------
    A1  | A1 
    B1  | B1   A1 
    B2  | B2   A2   A1 
    A2  | A2   B2   B1   A1 


  Atoms and basis sets
  --------------------

  Number of atom types:     2
  Total number of atoms:    3

  label    atoms   charge   prim    cont     basis   
  ----------------------------------------------------------------------
  C  1        1       6      26      14      [9s4p1d|3s2p1d]                              
  H  1        2       1       7       5      [4s1p|2s1p]                                  
  ----------------------------------------------------------------------
  ----------------------------------------------------------------------
  total:      3       8      40      24

  Spherical harmonic basis used.
  Threshold for integrals:  1.00D-15


  Cartesian Coordinates
  ---------------------

  Total number of coordinates:  9


   1   C  1     x      0.0000000000
   2            y      0.0000000000
   3            z      0.2100110200

   4   H  1 1   x      0.0000000000
   5            y      1.8616946700
   6            z     -0.6300311600

   7   H  1 2   x      0.0000000000
   8            y     -1.8616946700
   9            z     -0.6300311600



  Symmetry Coordinates
  --------------------

  Number of coordinates in each symmetry:   3  2  3  1


  Symmetry 1

   1   C  1  z    3
   2   H  1  y    [ 5  -  8 ]/2
   3   H  1  z    [ 6  +  9 ]/2


  Symmetry 2

   4   C  1  x    1
   5   H  1  x    [ 4  +  7 ]/2


  Symmetry 3

   6   C  1  y    2
   7   H  1  y    [ 5  +  8 ]/2
   8   H  1  z    [ 6  -  9 ]/2


  Symmetry 4

   9   H  1  x    [ 4  -  7 ]/2


   Interatomic separations (in Angstroms):
   ---------------------------------------

            C  1        H  1        H  2

   C  1    0.000000
   H  1    1.080815    0.000000
   H  2    1.080815    1.970332    0.000000




  Bond distances (angstroms):
  ---------------------------

                  atom 1     atom 2                           distance
                  ------     ------                           --------
  bond distance:    H  1       C  1                           1.080815
  bond distance:    H  2       C  1                           1.080815


  Bond angles (degrees):
  ----------------------

                  atom 1     atom 2     atom 3                   angle
                  ------     ------     ------                   -----
  bond angle:       H  2       C  1       H  1                 131.428


  Nuclear repulsion energy :    6.143886296433


  Orbital exponents and contraction coefficients
  ----------------------------------------------


  C  1   1s    1     6665.000000    0.0007 -0.0001  0.0000
   gen. cont.  2     1000.000000    0.0053 -0.0012  0.0000
               3      228.000000    0.0271 -0.0057  0.0000
               4       64.710000    0.1017 -0.0233  0.0000
               5       21.060000    0.2747 -0.0640  0.0000
               6        7.495000    0.4486 -0.1500  0.0000
               7        2.797000    0.2851 -0.1273  0.0000
               8        0.521500    0.0152  0.5445  0.0000
               9        0.159600   -0.0032  0.5805  1.0000

  C  1   2px  10        9.439000    0.0381  0.0000
   gen. cont. 11        2.002000    0.2095  0.0000
              12        0.545600    0.5086  0.0000
              13        0.151700    0.4688  1.0000

  C  1   2py  14        9.439000    0.0381  0.0000
   gen. cont. 15        2.002000    0.2095  0.0000
              16        0.545600    0.5086  0.0000
              17        0.151700    0.4688  1.0000

  C  1   2pz  18        9.439000    0.0381  0.0000
   gen. cont. 19        2.002000    0.2095  0.0000
              20        0.545600    0.5086  0.0000
              21        0.151700    0.4688  1.0000

  C  1   3d2- 22        0.550000    1.0000

  C  1   3d1- 23        0.550000    1.0000

  C  1   3d0  24        0.550000    1.0000

  C  1   3d1+ 25        0.550000    1.0000

  C  1   3d2+ 26        0.550000    1.0000

  H  1#1 1s   27       13.010000    0.0197  0.0000
   gen. cont. 28        1.962000    0.1380  0.0000
              29        0.444600    0.4781  0.0000
              30        0.122000    0.5012  1.0000

  H  1#2 1s   31       13.010000    0.0197  0.0000
   gen. cont. 32        1.962000    0.1380  0.0000
              33        0.444600    0.4781  0.0000
              34        0.122000    0.5012  1.0000

  H  1#1 2px  35        0.727000    1.0000

  H  1#2 2px  36        0.727000    1.0000

  H  1#1 2py  37        0.727000    1.0000

  H  1#2 2py  38        0.727000    1.0000

  H  1#1 2pz  39        0.727000    1.0000

  H  1#2 2pz  40        0.727000    1.0000


  Contracted Orbitals
  -------------------

   1  C  1    1s       1     2     3     4     5     6     7     8     9
   2  C  1    1s       1     2     3     4     5     6     7     8     9
   3  C  1    1s       9
   4  C  1    2px     10    11    12    13
   5  C  1    2py     14    15    16    17
   6  C  1    2pz     18    19    20    21
   7  C  1    2px     13
   8  C  1    2py     17
   9  C  1    2pz     21
  10  C  1    3d2-    22
  11  C  1    3d1-    23
  12  C  1    3d0     24
  13  C  1    3d1+    25
  14  C  1    3d2+    26
  15  H  1#1  1s    27  28  29  30
  16  H  1#2  1s    31  32  33  34
  17  H  1#1  1s    30
  18  H  1#2  1s    34
  19  H  1#1  2px   35
  20  H  1#2  2px   36
  21  H  1#1  2py   37
  22  H  1#2  2py   38
  23  H  1#1  2pz   39
  24  H  1#2  2pz   40




  Symmetry Orbitals
  -----------------

  Number of orbitals in each symmetry:        11  4  7  2


  Symmetry  A1 ( 1)

    1     C  1     1s         1
    2     C  1     1s         2
    3     C  1     1s         3
    4     C  1     2pz        6
    5     C  1     2pz        9
    6     C  1     3d0       12
    7     C  1     3d2+      14
    8     H  1     1s        15  +  16
    9     H  1     1s        17  +  18
   10     H  1     2py       21  -  22
   11     H  1     2pz       23  +  24


  Symmetry  B1 ( 2)

   12     C  1     2px        4
   13     C  1     2px        7
   14     C  1     3d1+      13
   15     H  1     2px       19  +  20


  Symmetry  B2 ( 3)

   16     C  1     2py        5
   17     C  1     2py        8
   18     C  1     3d1-      11
   19     H  1     1s        15  -  16
   20     H  1     1s        17  -  18
   21     H  1     2py       21  +  22
   22     H  1     2pz       23  -  24


  Symmetry  A2 ( 4)

   23     C  1     3d2-      10
   24     H  1     2px       19  -  20

  Symmetries of electric field:  B1 (2)  B2 (3)  A1 (1)

  Symmetries of magnetic field:  B2 (3)  B1 (2)  A2 (4)


 Copy of input to READIN
 -----------------------

INTGRL                                                                          
                                                                                
                                                                                
s   2    2X   Y      0.10D-14                                                   
       6.0    1    3    1    1    1                                             
C  1   0.000000000000000   0.000000000000000   0.210011020000000       *        
H   9   3                                                                       
       6665.00000000         0.00069200        -0.00014600         0.00000000   
       1000.00000000         0.00532900        -0.00115400         0.00000000   
        228.00000000         0.02707700        -0.00572500         0.00000000   
         64.71000000         0.10171800        -0.02331200         0.00000000   
         21.06000000         0.27474000        -0.06395500         0.00000000   
          7.49500000         0.44856400        -0.14998100         0.00000000   
          2.79700000         0.28507400        -0.12726200         0.00000000   
          0.52150000         0.01520400         0.54452900         0.00000000   
          0.15960000        -0.00319100         0.58049600         1.00000000   
H   4   2                                                                       
          9.43900000         0.03810900         0.00000000                      
          2.00200000         0.20948000         0.00000000                      
          0.54560000         0.50855700         0.00000000                      
          0.15170000         0.46884200         1.00000000                      
H   1   1                                                                       
          0.55000000         1.00000000                                         
       1.0    1    2    1    1                                                  
H  1   0.000000000000000   1.861694670000000  -0.630031160000000       *        
H   4   2                                                                       
         13.01000000         0.01968500         0.00000000                      
          1.96200000         0.13797700         0.00000000                      
          0.44460000         0.47814800         0.00000000                      
          0.12200000         0.50124000         1.00000000                      
H   1   1                                                                       
          0.72700000         1.00000000                                         




 ************************************************************************
 ************************** Output from HERONE **************************
 ************************************************************************

found      80 non-vanashing overlap integrals
found     107 non-vanashing nuclear attraction integrals
found      80 non-vanashing kinetic energy integrals






 found      46 non-vanashing integrals ( typea=  1 typeb=  0)
 found      69 non-vanashing integrals ( typea=  1 typeb=  1)
 found      98 non-vanashing integrals ( typea=  1 typeb=  2)


 found      87 non-vanashing integrals ( typea=  1 typeb=  3)
 found      41 non-vanashing integrals ( typea=  1 typeb=  4)
 found      58 non-vanashing integrals ( typea=  1 typeb=  5)
 found      88 non-vanashing integrals ( typea=  1 typeb=  6)
 found      85 non-vanashing integrals ( typea=  1 typeb=  7)
 found     101 non-vanashing integrals ( typea=  1 typeb=  8)


 found      46 non-vanashing integrals ( typea=  1 typeb=  9)
 found      67 non-vanashing integrals ( typea=  1 typeb= 10)
 found     107 non-vanashing integrals ( typea=  1 typeb= 11)
 found      44 non-vanashing integrals ( typea=  1 typeb= 12)
 found      49 non-vanashing integrals ( typea=  1 typeb= 13)
 found      58 non-vanashing integrals ( typea=  1 typeb= 14)
 found      73 non-vanashing integrals ( typea=  1 typeb= 15)
 found     107 non-vanashing integrals ( typea=  1 typeb= 16)
 found      85 non-vanashing integrals ( typea=  1 typeb= 17)
 found     101 non-vanashing integrals ( typea=  1 typeb= 18)


 found      87 non-vanashing integrals ( typea=  1 typeb= 19)
 found      42 non-vanashing integrals ( typea=  1 typeb= 20)
 found      57 non-vanashing integrals ( typea=  1 typeb= 21)
 found      83 non-vanashing integrals ( typea=  1 typeb= 22)
 found      81 non-vanashing integrals ( typea=  1 typeb= 23)
 found     107 non-vanashing integrals ( typea=  1 typeb= 24)
 found      42 non-vanashing integrals ( typea=  1 typeb= 25)
 found      54 non-vanashing integrals ( typea=  1 typeb= 26)
 found      49 non-vanashing integrals ( typea=  1 typeb= 27)
 found      58 non-vanashing integrals ( typea=  1 typeb= 28)
 found      91 non-vanashing integrals ( typea=  1 typeb= 29)
 found      84 non-vanashing integrals ( typea=  1 typeb= 30)
 found     107 non-vanashing integrals ( typea=  1 typeb= 31)
 found      85 non-vanashing integrals ( typea=  1 typeb= 32)
 found     101 non-vanashing integrals ( typea=  1 typeb= 33)


 found      82 non-vanashing integrals ( typea=  2 typeb=  6)
 found      55 non-vanashing integrals ( typea=  2 typeb=  7)
 found      32 non-vanashing integrals ( typea=  2 typeb=  8)




 ************************************************************************
 ************************** Output from TWOINT **************************
 ************************************************************************


 Number of two-electron integrals written:     11412 (25.3%)
 Kilobytes written:                              219




 >>>> Total CPU  time used in HERMIT:   0.03 seconds
 >>>> Total wall time used in HERMIT:   0.00 seconds

- End of Integral Section
