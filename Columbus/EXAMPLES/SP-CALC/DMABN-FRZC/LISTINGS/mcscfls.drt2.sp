

     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.5         **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 This program allows the csf mixing coefficient and orbital expansion coefficient
 optimization using the graphical unitary group approach and the exponential
 operator mcscf method.
 references:  r. shepard and j. simons, ' int. j. quantum chem. symp. 14, 211 (1980).
              r. shepard, i. shavitt, and j. simons, j. chem. phys. 76, 543 (1982).
              r. shepard in "ab initio methods in quantum chemistry ii" advances in chemical
                  physics 69, edited by k. p. lawley (wiley, new york, 1987) pp. 63-200.
 Original autor: Ron Shepard, ANL
 Later revisions: Michal Dallos, University Vienna

 This Version of Program MCSCF is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de



     ******************************************
     **    PROGRAM:              MCSCF       **
     **    PROGRAM VERSION:      5.4.0.2     **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************

 Workspace allocation information:
       301465600 of real*8 words ( 2300.00 MB) of work space has been allocated.

 user input information:

 ======== echo of the mcscf input ========
 ------------------------------------------------------------------------
  &input
   NITER=1,NMITER=0
   NMITER=0
   nciitr=300,
   tol(3)=1.e-4,
   tol(2)=1.e-4,
   tol(1)=1.e-8,
   NSTATE=0,
   npath=1,3,9,10,13,17,19,21,-11,12, 2,-13,30,13,18
   ncoupl=5,
   tol(9)=1.e-3,
   FCIORB=  2,2,40,2,3,40,2,4,40,2,5,40,2,6,40,2,7,40,2,8,40,2,9,40,4,2,40,4,3,40
  
  
  
  
  
  
  
  
  
  
 NAVST(1) = 2
 WAVST(1,1)=1
 WAVST(1,2)=1
 /&end
 ------------------------------------------------------------------------


 ***  Integral file informations  ***


 input integral file : /fhgfs/global/lv70151/plasser1/1679223/WORK/WORK.2/aoints
    

 Integral file header information:
 Hermit Integral Program : SIFS version  r24n13            16:31:16.794 06-Sep-12
 user-specified FREEZE(*) used in program tran.                                  
 MO-coefficients from mcscf.x                                                    
  total ao core energy = -101.311908370                                          
 dummy occupation 1.0 for active orbitals                                        
 transformed using mo_mult.x                                                     
 SIFS file created by program tran.      r24n13            16:31:47.239 06-Sep-12

 Core type energy values:
 energy( 1)=  5.461735143427E+02, ietype=   -1,    core energy of type: Nuc.Rep.
 total ao core energy =  546.173514343


   ******  Basis set information:  ******

 Number of irreps:                  4
 Total number of basis functions: 185

 irrep no.              1    2    3    4
 irrep label           A1   B1   B2   A2 
 no. of bas.fcions.    69   35   56   25


 ***  MCSCF optimization procedure parmeters:  ***


 maximum number of mcscf iterations:        niter=     1

 maximum number of psci micro-iterations:   nmiter=    0
 maximum r,s subspace dimension allowed:    nvrsmx=   30

 tol(1)=  1.0000E-08. . . . delta-emc convergence criterion.
 tol(2)=  1.0000E-04. . . . wnorm convergence criterion.
 tol(3)=  1.0000E-04. . . . knorm convergence criterion.
 tol(4)=  1.0000E-08. . . . apxde convergence criterion.
 tol(5)=  1.0000E-04. . . . small diagonal matrix element tolerance.
 tol(6)=  1.0000E-06. . . . minimum ci-psci residual norm.
 tol(7)=  1.0000E-05. . . . maximum ci-psci residual norm.
 tol(8)=  1.0000E+00. . . . maximum abs(k(xy)) allowed.
 tol(9)=  1.0000E-03. . . . wnorm coupling tolerance.
 tol(10)= 0.0000E+00. . . . maximum psci emergency shift parameter.
 tol(11)= 0.0000E+00. . . . minimum psci emergency shift parameter.
 tol(12)= 0.0000E+00. . . . increment of psci emergency shift parameter.


 *** State averaging informations: ***


 MCSCF calculation performed for  1 DRT.

 DRT  first state   no.of aver.states   weights
  1   ground state          2             0.500 0.500

 The number of hmc(*) eigenvalues and eigenvectors calculated each iteration per DRT:
 DRT.   no.of eigenv.(=ncol)
    1        3

 orbital coefficients are optimized for the ground state (nstate=0).

 Orbitals included in invariant subspaces:
   symmetry   orbital   mask
       2       2( 71)    40
       2       3( 72)    40
       2       4( 73)    40
       2       5( 74)    40
       2       6( 75)    40
       2       7( 76)    40
       2       8( 77)    40
       2       9( 78)    40
       4       2(162)    40
       4       3(163)    40

 npath(*) options:
  2:  orbital-state coupling terms will be included beginning on iteration ncoupl=  5
  3:  print intermediate timing information.
  9:  suppress the drt listing.
 10:  suppress the hmc(*) eigenvector listing.
 12:  diagonalize the hmc(*) matrix iteratively.
        nunitv= 1 nciitr=** mxvadd=20 nvcimx=20
       rtolci(*),wnorm=     1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02 1.0000E-02
                            1.0000E-02 1.0000E-02 0.0000E+00
   noldv =   0
 13:  get initial orbitals from the formatted file, mocoef.
 17:  print the final natural orbitals and occupations.
 18:  skip the 2-e integral transformation on the first iteration.
 19:  transform the virtual orbitals to diagonalize qvv(*).
 21:  write out the one- and two- electron density for further use (files:mcd1fl, mcd2fl).
 30:  Compute mcscf (transition) density matrices


   ******  DRT info section  ******


 Informations for the DRT no.  1

 DRT file header:
  title                                                                          
 Molecular symmetry group:    a1 
 Total number of electrons:   56
 Spin multiplicity:            1
 Number of active orbitals:   10
 Number of active electrons:  10
 Total number of CSFs:      9996
 

 faar:   0 active-active rotations allowed out of:  29 possible.


 Number of active-double rotations:        10
 Number of active-active rotations:         0
 Number of double-virtual rotations:     1155
 Number of active-virtual rotations:      252
 lenbfsdef=                131071  lenbfs=                  6618
  number of integrals per class 1:11 (cf adda 
 class  1 (pq|rs):         #         916
 class  2 (pq|ri):         #         550
 class  3 (pq|ia):         #       63045
 class  4 (pi|qa):         #      114540
 class  5 (pq|ra):         #       13476
 class  6 (pq|ij)/(pi|qj): #       18960
 class  7 (pq|ab):         #      184031
 class  8 (pa|qb):         #      362134
 class  9 p(bp,ai)         #      291060
 class 10p(ai,jp):        #       11550
 class 11p(ai,bj):        #      697027

 Size of orbital-Hessian matrix B:                  1037152
 Size of the orbital-state Hessian matrix C:       28328664
 Total size of the state Hessian matrix M:                0
 Size of HESSIAN-matrix for quadratic conv.:       29365816


 Source of the initial MO coeficients:

 Input MO coefficient file: /fhgfs/global/lv70151/plasser1/1679223/WORK/WORK.2/mocoef   
 

               starting mcscf iteration...   1

 orbital-state coupling will not be calculated this iteration.

 *** Starting integral transformation ***

 mosort: allocated sort2 space, avc2is=   301214431 available sort2 space, avcisx=   301214683
 !timer: mosort                          cpu_time=     0.363 walltime=     0.710

 trial vectors are generated internally.

 trial vector  1 is unit matrix column    11
 ciiter=  42 noldhv=  3 noldv=  3
 !timer: hmc(*) diagonalization          cpu_time=     7.345 walltime=    10.596

 Eigenvalues of the hmc(*) matrix
             total energy     electronic energy      residual norm          rtolci(*)
    1*     -455.6503169706     -354.3384086007        0.0000004762        0.0000010000
    2      -455.4319039069     -354.1199955370        0.0000006365        0.0000010000
    3      -455.3815860243     -354.0696776545        0.0021296307        0.0100000000
 !timer: hmcvec                          cpu_time=     7.351 walltime=    10.620
 !timer: rdft                            cpu_time=     3.687 walltime=     3.773
 !timer: hbcon                           cpu_time=     4.355 walltime=     4.781
 
  tol(10)=  0.000000000000000E+000  eshsci=  1.688159577837887E-002
 *** psci convergence not reached ***
 Total number of micro iterations:    0

 ***  micro: final psci convergence values:  ***
    imxov=  0 z0= 0.00000000 pnorm= 0.0000E+00 rznorm= 0.0000E+00 rpnorm= 0.0000E+00 noldr=  0 nnewr=  1 nolds=  0 nnews=  0
 *** warning *** small z0.
 

 fdd(*) eigenvalues. symmetry block  1
    -2.610155   -2.332048   -2.268926   -2.057527   -1.854834   -1.633425   -1.472234   -1.419433   -1.255934   -1.158742
    -1.094976   -1.006585

 qvv(*) eigenvalues. symmetry block  1
     0.305798    0.375960    0.408431    0.523479    0.568421    0.736478    0.896088    0.923691    0.986133    1.157208
     1.244625    1.317557    1.386502    1.399080    1.462045    1.536967    1.642288    1.697273    1.816377    1.838354
     1.905288    1.925197    2.046916    2.186970    2.200034    2.389556    2.499554    2.575928    2.752429    2.867941
     3.029400    3.071896    3.207424    3.370838    3.519805    3.621730    3.684747    3.850175    3.887962    3.915310
     3.974389    4.067085    4.149901    4.188327    4.319861    4.403933    4.561988    4.588703    4.725176    4.934293
     5.012059    5.345195    5.478927    5.535729    5.876159    5.900310    6.012598

 fdd(*) eigenvalues. symmetry block  2
    -1.263053
 i,qaaresolved                     1  -1.05757332334689     
 i,qaaresolved                     2 -0.929067867033838     
 i,qaaresolved                     3 -0.778733009558633     
 i,qaaresolved                     4 -0.473406686348311     
 i,qaaresolved                     5  0.145060184801475     
 i,qaaresolved                     6  0.645705397513827     
 i,qaaresolved                     7  0.920559571600558     
 i,qaaresolved                     8   1.73539176131486     

 qvv(*) eigenvalues. symmetry block  2
     0.688183    1.240186    1.341825    1.453807    1.601666    1.680438    1.753093    1.895069    2.137488    2.318381
     2.496866    2.663189    3.031032    3.273000    3.452997    3.485707    3.671005    3.778577    3.882292    4.126457
     4.291919    4.696762    4.813128    5.200011    5.848036    5.980253

 fdd(*) eigenvalues. symmetry block  3
    -2.056586   -1.997415   -1.654657   -1.355113   -1.196754   -1.186467   -1.114757   -0.979224   -0.825700

 qvv(*) eigenvalues. symmetry block  3
     0.380698    0.452921    0.492612    0.538039    0.609086    0.764209    0.912864    1.000975    1.218268    1.347831
     1.442127    1.498530    1.672591    1.727728    1.745864    1.776494    1.844372    1.913536    1.996913    2.273633
     2.327813    2.448284    2.637040    2.688710    2.860270    2.953657    3.201794    3.298327    3.569914    3.705767
     3.821773    3.847557    3.968934    4.080262    4.268044    4.330487    4.515560    4.521433    4.595894    4.745211
     4.964371    5.097875    5.362797    5.459621    5.576574    5.625764    5.784772

 fdd(*) eigenvalues. symmetry block  4
    -1.187534
 i,qaaresolved                     1 -0.657528269991178     
 i,qaaresolved                     2  0.278709700397524     

 qvv(*) eigenvalues. symmetry block  4
     0.489274    1.323784    1.471385    1.612734    1.738847    2.151609    2.306198    2.482264    2.558781    2.865026
     3.289442    3.391234    3.585002    3.797140    3.904653    4.122073    4.437885    4.521576    4.541360    5.004515
     5.477096    5.722295

 restrt: restart information saved on the restart file (unit= 13).
 !timer: mcscf iteration                 cpu_time=    12.270 walltime=    16.635

 not all mcscf convergence criteria are satisfied.
 iter=    1 emc=   -455.5411104388 demc= 4.5554E+02 wnorm= 1.3505E-01 knorm= 0.0000E+00 apxde=-8.4408E-03    *not conv.*     

 final mcscf convergence values:
 iter=    1 emc=   -455.5411104388 demc= 4.5554E+02 wnorm= 1.3505E-01 knorm= 0.0000E+00 apxde=-8.4408E-03    *not conv.*     




   ---------Individual total energies for all states:----------
   DRT #1 state # 1 wt 0.500 total energy=     -455.650316971, rel. (eV)=   0.000000
   DRT #1 state # 2 wt 0.500 total energy=     -455.431903907, rel. (eV)=   5.943324
   ------------------------------------------------------------


 MO-coefficient print-out skipped (no flag 32)
 They may be found in the MOCOEF directory.

          natural orbitals of the final iteration,block  1    -  A1 
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000
               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   25        MO   26        MO   27        MO   28        MO   29        MO   30        MO   31        MO   32
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   33        MO   34        MO   35        MO   36        MO   37        MO   38        MO   39        MO   40
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   41        MO   42        MO   43        MO   44        MO   45        MO   46        MO   47        MO   48
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   49        MO   50        MO   51        MO   52        MO   53        MO   54        MO   55        MO   56
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   57        MO   58        MO   59        MO   60        MO   61        MO   62        MO   63        MO   64
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   65        MO   66        MO   67        MO   68        MO   69
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

          natural orbitals of the final iteration,block  2    -  B1 
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     1.96490899     1.95523866     1.93049185     1.48819235     0.53911630     0.06262389     0.04032069
               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     0.01104925     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   25        MO   26        MO   27        MO   28        MO   29        MO   30        MO   31        MO   32
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   33        MO   34        MO   35
  occ(*)=     0.00000000     0.00000000     0.00000000

          natural orbitals of the final iteration,block  3    -  B2 
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000
               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   25        MO   26        MO   27        MO   28        MO   29        MO   30        MO   31        MO   32
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   33        MO   34        MO   35        MO   36        MO   37        MO   38        MO   39        MO   40
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   41        MO   42        MO   43        MO   44        MO   45        MO   46        MO   47        MO   48
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   49        MO   50        MO   51        MO   52        MO   53        MO   54        MO   55        MO   56
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

          natural orbitals of the final iteration,block  4    -  A2 
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     1.87450005     0.13355798     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   25
  occ(*)=     0.00000000
 d1(*), fmc(*), and qmc(*) written to the 1-particle density matrix file.
       3239 d2(*) elements written to the 2-particle density matrix file: mcd2fl                                                      
 Computing the requested mcscf (transition) density matrices (flag 30)
 Reading mcdenin ...
 Number of density matrices (ndens):                     2
 Number of unique bra states (ndbra):                     2
 qind: F
 (Transition) density matrices:
 d1(*) written to the 1-particle density matrix file.
       3239 d2(*) elements written to the 2-particle density matrix file: mcsd2fl.drt1.st01                                           
 d1(*) written to the 1-particle density matrix file.
       3239 d2(*) elements written to the 2-particle density matrix file: mcsd2fl.drt1.st02                                           

          state spec. NOs: DRT 1, State  1

          block  1
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000
               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   25        MO   26        MO   27        MO   28        MO   29        MO   30        MO   31        MO   32
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   33        MO   34        MO   35        MO   36        MO   37        MO   38        MO   39        MO   40
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   41        MO   42        MO   43        MO   44        MO   45        MO   46        MO   47        MO   48
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   49        MO   50        MO   51        MO   52        MO   53        MO   54        MO   55        MO   56
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   57        MO   58        MO   59        MO   60        MO   61        MO   62        MO   63        MO   64
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   65        MO   66        MO   67        MO   68        MO   69
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

          block  2
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     1.98412769     1.96432712     1.94103452     1.90274050     0.10352835     0.05737316     0.03372756
               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     0.00903723     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   25        MO   26        MO   27        MO   28        MO   29        MO   30        MO   31        MO   32
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   33        MO   34        MO   35
  occ(*)=     0.00000000     0.00000000     0.00000000

          block  3
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000
               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   25        MO   26        MO   27        MO   28        MO   29        MO   30        MO   31        MO   32
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   33        MO   34        MO   35        MO   36        MO   37        MO   38        MO   39        MO   40
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   41        MO   42        MO   43        MO   44        MO   45        MO   46        MO   47        MO   48
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   49        MO   50        MO   51        MO   52        MO   53        MO   54        MO   55        MO   56
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

          block  4
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     1.90674205     0.09736183     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   25
  occ(*)=     0.00000000

          state spec. NOs: DRT 1, State  2

          block  1
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000
               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   25        MO   26        MO   27        MO   28        MO   29        MO   30        MO   31        MO   32
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   33        MO   34        MO   35        MO   36        MO   37        MO   38        MO   39        MO   40
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   41        MO   42        MO   43        MO   44        MO   45        MO   46        MO   47        MO   48
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   49        MO   50        MO   51        MO   52        MO   53        MO   54        MO   55        MO   56
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   57        MO   58        MO   59        MO   60        MO   61        MO   62        MO   63        MO   64
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   65        MO   66        MO   67        MO   68        MO   69
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

          block  2
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     1.96307008     1.95300418     1.92915488     1.08191415     0.93788117     0.06903569     0.04373819
               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     0.01018947     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   25        MO   26        MO   27        MO   28        MO   29        MO   30        MO   31        MO   32
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   33        MO   34        MO   35
  occ(*)=     0.00000000     0.00000000     0.00000000

          block  3
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000     2.00000000
               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     2.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   25        MO   26        MO   27        MO   28        MO   29        MO   30        MO   31        MO   32
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   33        MO   34        MO   35        MO   36        MO   37        MO   38        MO   39        MO   40
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   41        MO   42        MO   43        MO   44        MO   45        MO   46        MO   47        MO   48
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   49        MO   50        MO   51        MO   52        MO   53        MO   54        MO   55        MO   56
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000

          block  4
               MO    1        MO    2        MO    3        MO    4        MO    5        MO    6        MO    7        MO    8
  occ(*)=     2.00000000     1.84790353     0.16410865     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO    9        MO   10        MO   11        MO   12        MO   13        MO   14        MO   15        MO   16
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   17        MO   18        MO   19        MO   20        MO   21        MO   22        MO   23        MO   24
  occ(*)=     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000     0.00000000
               MO   25
  occ(*)=     0.00000000


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        A1  partial gross atomic populations
   ao class       1A1        2A1        3A1        4A1        5A1        6A1 
    to_ s       2.000000   2.000000   2.000000   2.000000   2.000000   2.000000
 
   ao class       7A1        8A1        9A1       10A1       11A1       12A1 
    to_ s       2.000000   2.000000   2.000000   2.000000   2.000000   2.000000
 
   ao class      13A1       14A1       15A1       16A1       17A1       18A1 
 
   ao class      19A1       20A1       21A1       22A1       23A1       24A1 
 
   ao class      25A1       26A1       27A1       28A1       29A1       30A1 
 
   ao class      31A1       32A1       33A1       34A1       35A1       36A1 
 
   ao class      37A1       38A1       39A1       40A1       41A1       42A1 
 
   ao class      43A1       44A1       45A1       46A1       47A1       48A1 
 
   ao class      49A1       50A1       51A1       52A1       53A1       54A1 
 
   ao class      55A1       56A1       57A1       58A1       59A1       60A1 
 
   ao class      61A1       62A1       63A1       64A1       65A1       66A1 
 
   ao class      67A1       68A1       69A1 

                        B1  partial gross atomic populations
   ao class       1B1        2B1        3B1        4B1        5B1        6B1 
    to_ s       2.000000   1.964909   1.955239   1.930492   1.488192   0.539116
 
   ao class       7B1        8B1        9B1       10B1       11B1       12B1 
    to_ s       0.062624   0.040321   0.011049   0.000000   0.000000   0.000000
 
   ao class      13B1       14B1       15B1       16B1       17B1       18B1 
 
   ao class      19B1       20B1       21B1       22B1       23B1       24B1 
 
   ao class      25B1       26B1       27B1       28B1       29B1       30B1 
 
   ao class      31B1       32B1       33B1       34B1       35B1 

                        B2  partial gross atomic populations
   ao class       1B2        2B2        3B2        4B2        5B2        6B2 
    to_ s       2.000000   2.000000   2.000000   2.000000   2.000000   2.000000
 
   ao class       7B2        8B2        9B2       10B2       11B2       12B2 
    to_ s       2.000000   2.000000   2.000000   0.000000   0.000000   0.000000
 
   ao class      13B2       14B2       15B2       16B2       17B2       18B2 
 
   ao class      19B2       20B2       21B2       22B2       23B2       24B2 
 
   ao class      25B2       26B2       27B2       28B2       29B2       30B2 
 
   ao class      31B2       32B2       33B2       34B2       35B2       36B2 
 
   ao class      37B2       38B2       39B2       40B2       41B2       42B2 
 
   ao class      43B2       44B2       45B2       46B2       47B2       48B2 
 
   ao class      49B2       50B2       51B2       52B2       53B2       54B2 
 
   ao class      55B2       56B2 

                        A2  partial gross atomic populations
   ao class       1A2        2A2        3A2        4A2        5A2        6A2 
    to_ s       2.000000   1.874500   0.133558   0.000000   0.000000   0.000000
 
   ao class       7A2        8A2        9A2       10A2       11A2       12A2 
 
   ao class      13A2       14A2       15A2       16A2       17A2       18A2 
 
   ao class      19A2       20A2       21A2       22A2       23A2       24A2 
 
   ao class      25A2 


                        gross atomic populations
     ao           to_
      s        56.000000
    total      56.000000
 

 Total number of electrons:   56.00000000

 Mulliken population for:
DRT 1, state 01


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        A1  partial gross atomic populations
   ao class       1A1        2A1        3A1        4A1        5A1        6A1 
    to_ s       2.000000   2.000000   2.000000   2.000000   2.000000   2.000000
 
   ao class       7A1        8A1        9A1       10A1       11A1       12A1 
    to_ s       2.000000   2.000000   2.000000   2.000000   2.000000   2.000000
 
   ao class      13A1       14A1       15A1       16A1       17A1       18A1 
 
   ao class      19A1       20A1       21A1       22A1       23A1       24A1 
 
   ao class      25A1       26A1       27A1       28A1       29A1       30A1 
 
   ao class      31A1       32A1       33A1       34A1       35A1       36A1 
 
   ao class      37A1       38A1       39A1       40A1       41A1       42A1 
 
   ao class      43A1       44A1       45A1       46A1       47A1       48A1 
 
   ao class      49A1       50A1       51A1       52A1       53A1       54A1 
 
   ao class      55A1       56A1       57A1       58A1       59A1       60A1 
 
   ao class      61A1       62A1       63A1       64A1       65A1       66A1 
 
   ao class      67A1       68A1       69A1 

                        B1  partial gross atomic populations
   ao class       1B1        2B1        3B1        4B1        5B1        6B1 
    to_ s       2.000000   1.984128   1.964327   1.941035   1.902740   0.103528
 
   ao class       7B1        8B1        9B1       10B1       11B1       12B1 
    to_ s       0.057373   0.033728   0.009037   0.000000   0.000000   0.000000
 
   ao class      13B1       14B1       15B1       16B1       17B1       18B1 
 
   ao class      19B1       20B1       21B1       22B1       23B1       24B1 
 
   ao class      25B1       26B1       27B1       28B1       29B1       30B1 
 
   ao class      31B1       32B1       33B1       34B1       35B1 

                        B2  partial gross atomic populations
   ao class       1B2        2B2        3B2        4B2        5B2        6B2 
    to_ s       2.000000   2.000000   2.000000   2.000000   2.000000   2.000000
 
   ao class       7B2        8B2        9B2       10B2       11B2       12B2 
    to_ s       2.000000   2.000000   2.000000   0.000000   0.000000   0.000000
 
   ao class      13B2       14B2       15B2       16B2       17B2       18B2 
 
   ao class      19B2       20B2       21B2       22B2       23B2       24B2 
 
   ao class      25B2       26B2       27B2       28B2       29B2       30B2 
 
   ao class      31B2       32B2       33B2       34B2       35B2       36B2 
 
   ao class      37B2       38B2       39B2       40B2       41B2       42B2 
 
   ao class      43B2       44B2       45B2       46B2       47B2       48B2 
 
   ao class      49B2       50B2       51B2       52B2       53B2       54B2 
 
   ao class      55B2       56B2 

                        A2  partial gross atomic populations
   ao class       1A2        2A2        3A2        4A2        5A2        6A2 
    to_ s       2.000000   1.906742   0.097362   0.000000   0.000000   0.000000
 
   ao class       7A2        8A2        9A2       10A2       11A2       12A2 
 
   ao class      13A2       14A2       15A2       16A2       17A2       18A2 
 
   ao class      19A2       20A2       21A2       22A2       23A2       24A2 
 
   ao class      25A2 


                        gross atomic populations
     ao           to_
      s        56.000000
    total      56.000000
 

 Total number of electrons:   56.00000000

 Mulliken population for:
DRT 1, state 02


          Mulliken population analysis


  NOTE: For HERMIT use spherical harmonics basis sets !!!
 

                        A1  partial gross atomic populations
   ao class       1A1        2A1        3A1        4A1        5A1        6A1 
    to_ s       2.000000   2.000000   2.000000   2.000000   2.000000   2.000000
 
   ao class       7A1        8A1        9A1       10A1       11A1       12A1 
    to_ s       2.000000   2.000000   2.000000   2.000000   2.000000   2.000000
 
   ao class      13A1       14A1       15A1       16A1       17A1       18A1 
 
   ao class      19A1       20A1       21A1       22A1       23A1       24A1 
 
   ao class      25A1       26A1       27A1       28A1       29A1       30A1 
 
   ao class      31A1       32A1       33A1       34A1       35A1       36A1 
 
   ao class      37A1       38A1       39A1       40A1       41A1       42A1 
 
   ao class      43A1       44A1       45A1       46A1       47A1       48A1 
 
   ao class      49A1       50A1       51A1       52A1       53A1       54A1 
 
   ao class      55A1       56A1       57A1       58A1       59A1       60A1 
 
   ao class      61A1       62A1       63A1       64A1       65A1       66A1 
 
   ao class      67A1       68A1       69A1 

                        B1  partial gross atomic populations
   ao class       1B1        2B1        3B1        4B1        5B1        6B1 
    to_ s       2.000000   1.963070   1.953004   1.929155   1.081914   0.937881
 
   ao class       7B1        8B1        9B1       10B1       11B1       12B1 
    to_ s       0.069036   0.043738   0.010189   0.000000   0.000000   0.000000
 
   ao class      13B1       14B1       15B1       16B1       17B1       18B1 
 
   ao class      19B1       20B1       21B1       22B1       23B1       24B1 
 
   ao class      25B1       26B1       27B1       28B1       29B1       30B1 
 
   ao class      31B1       32B1       33B1       34B1       35B1 

                        B2  partial gross atomic populations
   ao class       1B2        2B2        3B2        4B2        5B2        6B2 
    to_ s       2.000000   2.000000   2.000000   2.000000   2.000000   2.000000
 
   ao class       7B2        8B2        9B2       10B2       11B2       12B2 
    to_ s       2.000000   2.000000   2.000000   0.000000   0.000000   0.000000
 
   ao class      13B2       14B2       15B2       16B2       17B2       18B2 
 
   ao class      19B2       20B2       21B2       22B2       23B2       24B2 
 
   ao class      25B2       26B2       27B2       28B2       29B2       30B2 
 
   ao class      31B2       32B2       33B2       34B2       35B2       36B2 
 
   ao class      37B2       38B2       39B2       40B2       41B2       42B2 
 
   ao class      43B2       44B2       45B2       46B2       47B2       48B2 
 
   ao class      49B2       50B2       51B2       52B2       53B2       54B2 
 
   ao class      55B2       56B2 

                        A2  partial gross atomic populations
   ao class       1A2        2A2        3A2        4A2        5A2        6A2 
    to_ s       2.000000   1.847904   0.164109   0.000000   0.000000   0.000000
 
   ao class       7A2        8A2        9A2       10A2       11A2       12A2 
 
   ao class      13A2       14A2       15A2       16A2       17A2       18A2 
 
   ao class      19A2       20A2       21A2       22A2       23A2       24A2 
 
   ao class      25A2 


                        gross atomic populations
     ao           to_
      s        56.000000
    total      56.000000
 

 Total number of electrons:   56.00000000

 !timer: mcscf                           cpu_time=    12.598 walltime=    17.258
