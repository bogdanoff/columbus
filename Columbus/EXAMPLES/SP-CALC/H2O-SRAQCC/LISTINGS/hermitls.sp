
 DALTON: user specified work memory size used,
          environment variable WRKMEM = "8000000             "

 Work memory size (LMWORK) :     8000000 =   61.04 megabytes.

 Default basis set library used :
        /sphome/kedziora/dalton/basis/                              


    $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    $$$$$$$$$$$  DALTON - An electronic structure program  $$$$$$$$$$$
    $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

               This is output from DALTON (beta-version 0.9) 

                          Principal authors:

            Trygve Helgaker,     University of Oslo,        Norway 
            Hans Joergen Jensen, University of Odense,      Denmark
            Poul Joergensen,     University of Aarhus,      Denmark
            Henrik Koch,         University of Aarhus,      Denmark
            Jeppe Olsen,         University of Lund,        Sweden 
            Hans Aagren,         University of Linkoeping,  Sweden 

                          Contributors:

            Torgeir Andersen,    University of Oslo,        Norway 
            Keld L. Bak,         University of Copenhagen,  Denmark
            Vebjoern Bakken,     University of Oslo,        Norway 
            Ove Christiansen,    University of Aarhus,      Denmark
            Paal Dahle,          University of Oslo,        Norway 
            Erik K. Dalskov,     University of Odense,      Denmark
            Thomas Enevoldsen,   University of Odense,      Denmark
            Asger Halkier,       University of Aarhus,      Denmark
            Hanne Heiberg,       University of Oslo,        Norway 
            Dan Jonsson,         University of Linkoeping,  Sweden 
            Sheela Kirpekar,     University of Odense,      Denmark
            Rika Kobayashi,      University of Aarhus,      Denmark
            Alfredo S. de Meras, Valencia University,       Spain  
            Kurt Mikkelsen,      University of Aarhus,      Denmark
            Patrick Norman,      University of Linkoeping,  Sweden 
            Martin J. Packer,    University of Sheffield,   UK     
            Kenneth Ruud,        University of Oslo,        Norway 
            Trond Saue,          University of Oslo,        Norway 
            Peter Taylor,        San Diego Superc. Center,  USA    
            Olav Vahtras,        University of Linkoeping,  Sweden

                                             Release Date:  August 1996
------------------------------------------------------------------------


      
     NOTE:
      
     This is an experimental code for the evaluation of molecular
     properties using (MC)SCF/CC wave functions. The authors accept
      no responsibility for the performance of the code or for the
     correctness of the results.
      
     The code (in whole or part) is not to be reproduced for further
     distribution without the written permission of T. Helgaker,
     H. J. Aa. Jensen or P. Taylor.
      
     If results obtained with this code are published, an
     appropriate citation would be:
      
     T. Helgaker, H. J. Aa. Jensen, P.Joergensen, H. Koch,
     J. Olsen, H. Aagren, T. Andersen, K. L. Bak, V. Bakken,
     O. Christiansen, P. Dahle, E. K. Dalskov, T. Enevoldsen,
     A. Halkier, H. Heiberg, D. Jonsson, S. Kirpekar, R. Kobayashi,
     A. S. de Meras, K. V. Mikkelsen, P. Norman, M. J. Packer,
     K. Ruud, T.Saue, P. R. Taylor, and O. Vahtras:
     DALTON, an electronic structure program"



     ******************************************
     **    PROGRAM:              DALTON      **
     **    PROGRAM VERSION:      5.4.0.0     **
     **    DISTRIBUTION VERSION: 5.9.a       **
     ******************************************



 <<<<<<<<<< OUTPUT FROM GENERAL INPUT PROCESSING >>>>>>>>>>




 Default print level:        0

    Integral sections will be executed
    Starting in Integral Section -



 *************************************************************************
 ****************** Output from HERMIT input processing ******************
 *************************************************************************



 Default print level:        2


 Calculation of one- and two-electron Hamiltonian integrals.



 Changes of defaults for READIN:
 -------------------------------


 Maximum number of primitives per integral block :   10



 *************************************************************************
 ****************** Output from READIN input processing ******************
 *************************************************************************



  Title Cards
  -----------

  h2o pvdz                                                                
                                                                          


  Symmetry Operations
  -------------------

  Symmetry operations: 2



                      SYMGRP:Point group information
                      ------------------------------

Point group: C2v

   * The point group was generated by:

      Reflection in the yz-plane
      Reflection in the xz-plane

   * Group multiplication table

        |  E   C2z  Oxz  Oyz
   -----+--------------------
     E  |  E 
    C2z | C2z   E 
    Oxz | Oxz  Oyz   E 
    Oyz | Oyz  Oxz  C2z   E 

   * Character table

        |  E   C2z  Oxz  Oyz
   -----+--------------------
    A1  |   1    1    1    1
    B1  |   1   -1    1   -1
    B2  |   1   -1   -1    1
    A2  |   1    1   -1   -1

   * Direct product table

        | A1   B1   B2   A2 
   -----+--------------------
    A1  | A1 
    B1  | B1   A1 
    B2  | B2   A2   A1 
    A2  | A2   B2   B1   A1 


  Atoms and basis sets
  --------------------

  Number of atom types:     2
  Total number of atoms:    3

  label    atoms   charge   prim    cont     basis   
  ----------------------------------------------------------------------
  O  1        1       8      26      14      [9s4p1d|3s2p1d]                        
  H  1        2       1       7       5      [4s1p|2s1p]                            
  ----------------------------------------------------------------------
  ----------------------------------------------------------------------
  total:      3      10      40      24

  Spherical harmonic basis used.
  Threshold for integrals:  1.00E-15


  Cartesian Coordinates
  ---------------------

  Total number of coordinates:  9


   1   O  1     x      0.0000000000
   2            y      0.0000000000
   3            z      0.0000000000

   4   H  1 1   x      1.4314500000
   5            y      0.0000000000
   6            z      1.1083470000

   7   H  1 2   x     -1.4314500000
   8            y      0.0000000000
   9            z      1.1083470000



  Symmetry Coordinates
  --------------------

  Number of coordinates in each symmetry:   3  3  2  1


  Symmetry 1

   1   O  1  z    3
   2   H  1  x    [ 4  -  7 ]/2
   3   H  1  z    [ 6  +  9 ]/2


  Symmetry 2

   4   O  1  x    1
   5   H  1  x    [ 4  +  7 ]/2
   6   H  1  z    [ 6  -  9 ]/2


  Symmetry 3

   7   O  1  y    2
   8   H  1  y    [ 5  +  8 ]/2


  Symmetry 4

   9   H  1  y    [ 5  -  8 ]/2


   Interatomic separations (in Angstroms):
   ---------------------------------------

            O  1        H  1        H  2

   O  1    0.000000
   H  1    0.958013    0.000000
   H  2    0.958013    1.514981    0.000000




  Bond distances (angstroms):
  ---------------------------

                  atom 1     atom 2                           distance
                  ------     ------                           --------
  bond distance:    H  1       O  1                           0.958013
  bond distance:    H  2       O  1                           0.958013


  Bond angles (degrees):
  ----------------------

                  atom 1     atom 2     atom 3                   angle
                  ------     ------     ------                   -----
  bond angle:       H  2       O  1       H  1                 104.500


  Nuclear repulsion energy :    9.187211027960


  Orbital exponents and contraction coefficients
  ----------------------------------------------


  O  1   1s    1    11720.000000    0.0007 -0.0002  0.0000
   gen. cont.  2     1759.000000    0.0055 -0.0013  0.0000
               3      400.800000    0.0278 -0.0063  0.0000
               4      113.700000    0.1048 -0.0257  0.0000
               5       37.030000    0.2831 -0.0709  0.0000
               6       13.270000    0.4487 -0.1654  0.0000
               7        5.025000    0.2710 -0.1170  0.0000
               8        1.013000    0.0155  0.5574  0.0000
               9        0.302300   -0.0026  0.5728  1.0000

  O  1   2px  10       17.700000    0.0430  0.0000
   gen. cont. 11        3.854000    0.2289  0.0000
              12        1.046000    0.5087  0.0000
              13        0.275300    0.4605  1.0000

  O  1   2py  14       17.700000    0.0430  0.0000
   gen. cont. 15        3.854000    0.2289  0.0000
              16        1.046000    0.5087  0.0000
              17        0.275300    0.4605  1.0000

  O  1   2pz  18       17.700000    0.0430  0.0000
   gen. cont. 19        3.854000    0.2289  0.0000
              20        1.046000    0.5087  0.0000
              21        0.275300    0.4605  1.0000

  O  1   3d2- 22        1.185000    1.0000

  O  1   3d1- 23        1.185000    1.0000

  O  1   3d0  24        1.185000    1.0000

  O  1   3d1+ 25        1.185000    1.0000

  O  1   3d2+ 26        1.185000    1.0000

  H  1#1 1s   27       13.010000    0.0197  0.0000
   gen. cont. 28        1.962000    0.1380  0.0000
              29        0.444600    0.4781  0.0000
              30        0.122000    0.5012  1.0000

  H  1#2 1s   31       13.010000    0.0197  0.0000
   gen. cont. 32        1.962000    0.1380  0.0000
              33        0.444600    0.4781  0.0000
              34        0.122000    0.5012  1.0000

  H  1#1 2px  35        0.727000    1.0000

  H  1#2 2px  36        0.727000    1.0000

  H  1#1 2py  37        0.727000    1.0000

  H  1#2 2py  38        0.727000    1.0000

  H  1#1 2pz  39        0.727000    1.0000

  H  1#2 2pz  40        0.727000    1.0000


  Contracted Orbitals
  -------------------

   1  O  1    1s       1     2     3     4     5     6     7     8     9
   2  O  1    1s       1     2     3     4     5     6     7     8     9
   3  O  1    1s       9
   4  O  1    2px     10    11    12    13
   5  O  1    2py     14    15    16    17
   6  O  1    2pz     18    19    20    21
   7  O  1    2px     13
   8  O  1    2py     17
   9  O  1    2pz     21
  10  O  1    3d2-    22
  11  O  1    3d1-    23
  12  O  1    3d0     24
  13  O  1    3d1+    25
  14  O  1    3d2+    26
  15  H  1#1  1s    27  28  29  30
  16  H  1#2  1s    31  32  33  34
  17  H  1#1  1s    30
  18  H  1#2  1s    34
  19  H  1#1  2px   35
  20  H  1#2  2px   36
  21  H  1#1  2py   37
  22  H  1#2  2py   38
  23  H  1#1  2pz   39
  24  H  1#2  2pz   40




  Symmetry Orbitals
  -----------------

  Number of orbitals in each symmetry:        11  7  4  2


  Symmetry  A1 ( 1)

    1     O  1     1s         1
    2     O  1     1s         2
    3     O  1     1s         3
    4     O  1     2pz        6
    5     O  1     2pz        9
    6     O  1     3d0       12
    7     O  1     3d2+      14
    8     H  1     1s        15  +  16
    9     H  1     1s        17  +  18
   10     H  1     2px       19  -  20
   11     H  1     2pz       23  +  24


  Symmetry  B1 ( 2)

   12     O  1     2px        4
   13     O  1     2px        7
   14     O  1     3d1+      13
   15     H  1     1s        15  -  16
   16     H  1     1s        17  -  18
   17     H  1     2px       19  +  20
   18     H  1     2pz       23  -  24


  Symmetry  B2 ( 3)

   19     O  1     2py        5
   20     O  1     2py        8
   21     O  1     3d1-      11
   22     H  1     2py       21  +  22


  Symmetry  A2 ( 4)

   23     O  1     3d2-      10
   24     H  1     2py       21  -  22

  Symmetries of electric field:  B1 (2)  B2 (3)  A1 (1)

  Symmetries of magnetic field:  B2 (3)  B1 (2)  A2 (4)


 Copy of input to READIN
 -----------------------

INTGRL                                                                          
h2o pvdz                                                                        
                                                                                
s   2    2X   Y      0.10E-14                                                   
       8.0    1    3    1    1    1                                             
O  1   0.000000000000000   0.000000000000000   0.000000000000000       *        
H   9   3                                                                       
      11720.00000000         0.00071000        -0.00016000         0.00000000   
       1759.00000000         0.00547000        -0.00126300         0.00000000   
        400.80000000         0.02783700        -0.00626700         0.00000000   
        113.70000000         0.10480000        -0.02571600         0.00000000   
         37.03000000         0.28306200        -0.07092400         0.00000000   
         13.27000000         0.44871900        -0.16541100         0.00000000   
          5.02500000         0.27095200        -0.11695500         0.00000000   
          1.01300000         0.01545800         0.55736800         0.00000000   
          0.30230000        -0.00258500         0.57275900         1.00000000   
H   4   2                                                                       
         17.70000000         0.04301800         0.00000000                      
          3.85400000         0.22891300         0.00000000                      
          1.04600000         0.50872800         0.00000000                      
          0.27530000         0.46053100         1.00000000                      
H   1   1                                                                       
          1.18500000         1.00000000                                         
       1.0    1    2    1    1                                                  
H  1   1.431450000000000   0.000000000000000   1.108347000000000       *        
H   4   2                                                                       
         13.01000000         0.01968500         0.00000000                      
          1.96200000         0.13797700         0.00000000                      
          0.44460000         0.47814800         0.00000000                      
          0.12200000         0.50124000         1.00000000                      
H   1   1                                                                       
          0.72700000         1.00000000                                         


 herdrv: noofopt= 0


 ************************************************************************
 ************************** Output from HERONE **************************
 ************************************************************************

 prop, itype F 1


    80 atomic overlap integrals written in   1 buffers.
 Percentage non-zero integrals:  26.67
 prop, itype F 2


   107 one-el. Hamil. integrals written in   1 buffers.
 Percentage non-zero integrals:  35.67
 prop, itype F 3


    80 kinetic energy integrals written in   1 buffers.
 Percentage non-zero integrals:  26.67

 >>> Time used in ONEDRV is   0.01 seconds



 ************************************************************************
 ************************** Output from TWOINT **************************
 ************************************************************************

 calling sifew2:luinta,info,num,last,nrec
 calling sifew2: 11 2 4096 3272 4096 2730 492 2 4

 Number of two-electron integrals written:     11412 (25.3%)
 Kilobytes written:                              164



 >>> Time used in TWOINT is   0.07 seconds

 >>>> Total CPU  time used in HERMIT:   0.09 seconds
 >>>> Total wall time used in HERMIT:   0.00 seconds

- End of Integral Section
