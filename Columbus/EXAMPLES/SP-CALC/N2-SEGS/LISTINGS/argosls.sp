echo of the argos input file:
 ------------------------------------------------------------------------
  
   1  1  4  4  4 20  9  0  0  0  0  0  0  0  0
   8  1 ag  1b1g  1b2g  1b3g  1 au  1b1u  1b2u  1b3u
   7
   2  3  4
   2  5  6
   2  7  8
   3  5  7
   3  6  8
   4  5  8
   4  6  7
     2    1    6
     6    6    8    7    1    3    4
    10    1    3    4    1    2    6    8    7    6    5
    14    6    8    7    6    5    8    7    1    3    4    1    2    3    4
     2    2    1
     1    1
     1   -1
     6    6    2
     0    0    1    0    0    1
     1    0    0    1    0    0
     0    1    0    0    1    0
     0    0    1    0    0   -1
     1    0    0   -1    0    0
     0    1    0    0   -1    0
    10   12    3
    -1   -1    4    0    0    0   -1   -1    4    0    0    0
     0    0    0    0    1    0    0    0    0    0    1    0
     0    0    0    0    0    1    0    0    0    0    0    1
     1   -1    0    0    0    0    1   -1    0    0    0    0
     0    0    0    1    0    0    0    0    0    1    0    0
    -1   -1    4    0    0    0    1    1   -4    0    0    0
     0    0    0    0    1    0    0    0    0    0   -1    0
     0    0    0    0    0    1    0    0    0    0    0   -1
     1   -1    0    0    0    0   -1    1    0    0    0    0
     0    0    0    1    0    0    0    0    0   -1    0    0
    14   20    4
     0    0    4    0   -9    0   -9    0    0    0    0    0    4    0   -9
     0   -9    0    0    0
    -1    0    0    0    0   -1    0   16    0    0   -1    0    0    0    0
    -1    0   16    0    0
     0   -1    0   -1    0    0    0    0   16    0    0   -1    0   -1    0
     0    0    0   16    0
     0    0    0    0    1    0   -1    0    0    0    0    0    0    0    1
     0   -1    0    0    0
     0    0    0    0    0    0    0    0    0    1    0    0    0    0    0
     0    0    0    0    1
    -1    0    0    0    0    9    0    0    0    0   -1    0    0    0    0
     9    0    0    0    0
     0   -1    0    9    0    0    0    0    0    0    0   -1    0    9    0
     0    0    0    0    0
     0    0    4    0   -9    0   -9    0    0    0    0    0   -4    0    9
     0    9    0    0    0
    -1    0    0    0    0   -1    0   16    0    0    1    0    0    0    0
     1    0  -16    0    0
     0   -1    0   -1    0    0    0    0   16    0    0    1    0    1    0
     0    0    0  -16    0
     0    0    0    0    1    0   -1    0    0    0    0    0    0    0   -1
     0    1    0    0    0
     0    0    0    0    0    0    0    0    0    1    0    0    0    0    0
     0    0    0    0   -1
    -1    0    0    0    0    9    0    0    0    0    1    0    0    0    0
    -9    0    0    0    0
     0   -1    0    9    0    0    0    0    0    0    0    1    0   -9    0
     0    0    0    0    0
    10    1    4
   11420.0000000        0.0005230  -0.0001150   0.0000000   0.0000000
    1712.0000000        0.0040450  -0.0008950   0.0000000   0.0000000
     389.3000000        0.0207750  -0.0046240   0.0000000   0.0000000
     110.0000000        0.0807270  -0.0185280   0.0000000   0.0000000
      35.5700000        0.2330740  -0.0573390   0.0000000   0.0000000
      12.5400000        0.4335010  -0.1320760   0.0000000   0.0000000
       4.6440000        0.3474720  -0.1725100   0.0000000   0.0000000
       1.2930000        0.0412620   0.1518140   1.0000000   0.0000000
       0.5118000       -0.0085080   0.5999440   0.0000000   0.0000000
       0.1787000        0.0023840   0.3874620   0.0000000   1.0000000
     5    2    3
      26.6300000        0.0146700   0.0000000   0.0000000
       5.9480000        0.0917640   0.0000000   0.0000000
       1.7420000        0.2986830   0.0000000   0.0000000
       0.5550000        0.4984870   1.0000000   0.0000000
       0.1725000        0.3370230   0.0000000   1.0000000
     2    3    2
       1.6540000        1.0000000   0.0000000
       0.4690000        0.0000000   1.0000000
     1    4    1
       1.0930000        1.0000000
 N    3  2 7.
     0.00000000    0.00000000    1.24000000
     0.00000000    0.00000000   -1.24000000
   2  1
   1  1
   2  2
   3  3
 ------------------------------------------------------------------------
                              program "argos" 5.9
                            columbus program system

             this program computes integrals over symmetry orbitals
               of generally contracted gaussian atomic orbitals.
                        programmed by russell m. pitzer

                           version date: 20-aug-2001

references:

    symmetry analysis (equal contributions):
    r. m. pitzer, j. chem. phys. 58, 3111 (1973).

    ao integral evaluation (hondo):
    m. dupuis, j. rys, and h. f. king, j. chem. phys. 65, 111 (1976).

    general contraction of gaussian orbitals:
    r. c. raffenetti, j. chem. phys. 58, 4452 (1973).

    core potential ao integrals (meldps):
    l. e. mcmurchie and e. r. davidson, j. comput. phys. 44, 289 (1981)

    spin-orbit and core potential integrals:
    r. m. pitzer and n. w. winter, int. j. quantum chem. 40, 773 (1991)

 This Version of Program ARGOS is Maintained by:
     Thomas Mueller
     Juelich Supercomputing Centre (JSC)
     Institute of Advanced Simulation (IAS)
     D-52425 Juelich, Germany 
     Email: th.mueller@fz-juelich.de


 workspace allocation parameters: lcore= 222822400 mem1=         0 ifirst=         1

 filenames and unit numbers:
 unit description                   filename
 ---- -----------                   ----------
   6  listing file:                 argosls                                                     
   4  1-e integral file:            aoints                                                      
   8  2-e integral file [fsplit=2]: aoints2                                                     
   5  input file:                   argosin                                                     

 argos input parameters and titles:
 ngen   =  1 ns     =  1 naords =  4 ncons  =  4 ngcs   =  4 itol   = 20
 icut   =  9 aoints =  4 only1e =  0 inrm   =  0 ncrs   =  0
 l1rec  =         0      l2rec  =         0      aoint2 =  8 fsplit =  2

                                                                                
aoints SIFS file created by argos.      adler.itc.univie. 13:59:55.886 18-Apr-11


irrep            1       2       3       4       5       6       7       8
degeneracy       1       1       1       1       1       1       1       1
label            ag     b1g     b2g     b3g      au     b1u     b2u     b3u


direct product table
   ( ag) ( ag) =  ag
   ( ag) (b1g) = b1g
   ( ag) (b2g) = b2g
   ( ag) (b3g) = b3g
   ( ag) ( au) =  au
   ( ag) (b1u) = b1u
   ( ag) (b2u) = b2u
   ( ag) (b3u) = b3u
   (b1g) ( ag) = b1g
   (b1g) (b1g) =  ag
   (b1g) (b2g) = b3g
   (b1g) (b3g) = b2g
   (b1g) ( au) = b1u
   (b1g) (b1u) =  au
   (b1g) (b2u) = b3u
   (b1g) (b3u) = b2u
   (b2g) ( ag) = b2g
   (b2g) (b1g) = b3g
   (b2g) (b2g) =  ag
   (b2g) (b3g) = b1g
   (b2g) ( au) = b2u
   (b2g) (b1u) = b3u
   (b2g) (b2u) =  au
   (b2g) (b3u) = b1u
   (b3g) ( ag) = b3g
   (b3g) (b1g) = b2g
   (b3g) (b2g) = b1g
   (b3g) (b3g) =  ag
   (b3g) ( au) = b3u
   (b3g) (b1u) = b2u
   (b3g) (b2u) = b1u
   (b3g) (b3u) =  au
   ( au) ( ag) =  au
   ( au) (b1g) = b1u
   ( au) (b2g) = b2u
   ( au) (b3g) = b3u
   ( au) ( au) =  ag
   ( au) (b1u) = b1g
   ( au) (b2u) = b2g
   ( au) (b3u) = b3g
   (b1u) ( ag) = b1u
   (b1u) (b1g) =  au
   (b1u) (b2g) = b3u
   (b1u) (b3g) = b2u
   (b1u) ( au) = b1g
   (b1u) (b1u) =  ag
   (b1u) (b2u) = b3g
   (b1u) (b3u) = b2g
   (b2u) ( ag) = b2u
   (b2u) (b1g) = b3u
   (b2u) (b2g) =  au
   (b2u) (b3g) = b1u
   (b2u) ( au) = b2g
   (b2u) (b1u) = b3g
   (b2u) (b2u) =  ag
   (b2u) (b3u) = b1g
   (b3u) ( ag) = b3u
   (b3u) (b1g) = b2u
   (b3u) (b2g) = b1u
   (b3u) (b3g) =  au
   (b3u) ( au) = b3g
   (b3u) (b1u) = b2g
   (b3u) (b2u) = b1g
   (b3u) (b3u) =  ag


                     nuclear repulsion energy   19.75806452


primitive ao integrals neglected if exponential factor below 10**(-20)
contracted ao and so integrals neglected if value below 10**(- 9)
symmetry orbital integrals written on units  4  8


                                    N   atoms

                              nuclear charge   7.00

           center            x               y               z
             1             0.00000000      0.00000000      1.24000000
             2             0.00000000      0.00000000     -1.24000000
 operator      center interchanges
  1(id.)        1  2
  2(gen.)       2  1

                    1s orbitals

 orbital exponents  contraction coefficients
    11420.00       5.2299966E-04  -1.1499995E-04    0.000000        0.000000    
    1712.000       4.0449973E-03  -8.9499965E-04    0.000000        0.000000    
    389.3000       2.0774986E-02  -4.6239982E-03    0.000000        0.000000    
    110.0000       8.0726947E-02  -1.8527993E-02    0.000000        0.000000    
    35.57000       0.2330738      -5.7338977E-02    0.000000        0.000000    
    12.54000       0.4335007      -0.1320759        0.000000        0.000000    
    4.644000       0.3474718      -0.1725099        0.000000        0.000000    
    1.293000       4.1261973E-02   0.1518139        1.000000        0.000000    
   0.5118000      -8.5079944E-03   0.5999438        0.000000        0.000000    
   0.1787000       2.3839984E-03   0.3874618        0.000000        1.000000    

                     symmetry orbital labels
                     1 ag1           2 ag1           3 ag1           4 ag1
                     1b1u1           2b1u1           3b1u1           4b1u1

           symmetry orbitals
 ctr, ao    ag1   b1u1
  1, 000  1.000  1.000
  2, 000  1.000 -1.000

                    2p orbitals

 orbital exponents  contraction coefficients
    26.63000       1.4670003E-02    0.000000        0.000000    
    5.948000       9.1764022E-02    0.000000        0.000000    
    1.742000       0.2986831        0.000000        0.000000    
   0.5550000       0.4984871        1.000000        0.000000    
   0.1725000       0.3370231        0.000000        1.000000    

                     symmetry orbital labels
                     5b1u1           6b1u1           7b1u1
                     1b3u1           2b3u1           3b3u1
                     1b2u1           2b2u1           3b2u1
                     5 ag1           6 ag1           7 ag1
                     1b2g1           2b2g1           3b2g1
                     1b3g1           2b3g1           3b3g1

           symmetry orbitals
 ctr, ao   b1u1   b3u1   b2u1    ag1   b2g1   b3g1
  1, 100  0.000  1.000  0.000  0.000  1.000  0.000
  1, 010  0.000  0.000  1.000  0.000  0.000  1.000
  1, 001  1.000  0.000  0.000  1.000  0.000  0.000
  2, 100  0.000  1.000  0.000  0.000 -1.000  0.000
  2, 010  0.000  0.000  1.000  0.000  0.000 -1.000
  2, 001  1.000  0.000  0.000 -1.000  0.000  0.000

                    3d orbitals

 orbital exponents  contraction coefficients
    1.654000        1.000000        0.000000    
   0.4690000        0.000000        1.000000    

                     symmetry orbital labels
                     8 ag1          10 ag1
                     4b2g1           5b2g1
                     4b3g1           5b3g1
                     9 ag1          11 ag1
                     1b1g1           2b1g1
                     8b1u1          10b1u1
                     4b3u1           5b3u1
                     4b2u1           5b2u1
                     9b1u1          11b1u1
                     1 au1           2 au1

           symmetry orbitals
 ctr, ao    ag1   b2g1   b3g1    ag1   b1g1   b1u1   b3u1   b2u1   b1u1    au1
  1, 200 -1.000  0.000  0.000  1.000  0.000 -1.000  0.000  0.000  1.000  0.000
  1, 020 -1.000  0.000  0.000 -1.000  0.000 -1.000  0.000  0.000 -1.000  0.000
  1, 002  2.000  0.000  0.000  0.000  0.000  2.000  0.000  0.000  0.000  0.000
  1, 110  0.000  0.000  0.000  0.000  1.000  0.000  0.000  0.000  0.000  1.000
  1, 101  0.000  1.000  0.000  0.000  0.000  0.000  1.000  0.000  0.000  0.000
  1, 011  0.000  0.000  1.000  0.000  0.000  0.000  0.000  1.000  0.000  0.000
  2, 200 -1.000  0.000  0.000  1.000  0.000  1.000  0.000  0.000 -1.000  0.000
  2, 020 -1.000  0.000  0.000 -1.000  0.000  1.000  0.000  0.000  1.000  0.000
  2, 002  2.000  0.000  0.000  0.000  0.000 -2.000  0.000  0.000  0.000  0.000
  2, 110  0.000  0.000  0.000  0.000  1.000  0.000  0.000  0.000  0.000 -1.000
  2, 101  0.000  1.000  0.000  0.000  0.000  0.000 -1.000  0.000  0.000  0.000
  2, 011  0.000  0.000  1.000  0.000  0.000  0.000  0.000 -1.000  0.000  0.000

lx: b3g          ly: b2g          lz: b1g

output SIFS file header information:
                                                                                
aoints SIFS file created by argos.      adler.itc.univie. 13:59:55.886 18-Apr-11

output energy(*) values:
 energy( 1)=  1.975806451613E+01, ietype=   -1,    core energy of type: Nuc.Rep.

total core energy =  1.975806451613E+01

nsym = 8 nbft=  46

symmetry  =    1    2    3    4    5    6    7    8
slabel(*) =   ag  b1g  b2g  b3g   au  b1u  b2u  b3u
nbpsy(*)  =   11    2    5    5    2   11    5    5

info(*) =         2      4096      3272      4096      2700         0

output orbital labels, i:bfnlab(i)=
   1:  1N__1s   2:  2N__1s   3:  3N__1s   4:  4N__1s   5:  5N__2p   6:  6N__2p
   7:  7N__2p   8:  8N__3d   9:  9N__3d  10: 10N__3d  11: 11N__3d  12: 12N__3d
  13: 13N__3d  14: 14N__2p  15: 15N__2p  16: 16N__2p  17: 17N__3d  18: 18N__3d
  19: 19N__2p  20: 20N__2p  21: 21N__2p  22: 22N__3d  23: 23N__3d  24: 24N__3d
  25: 25N__3d  26: 26N__1s  27: 27N__1s  28: 28N__1s  29: 29N__1s  30: 30N__2p
  31: 31N__2p  32: 32N__2p  33: 33N__3d  34: 34N__3d  35: 35N__3d  36: 36N__3d
  37: 37N__2p  38: 38N__2p  39: 39N__2p  40: 40N__3d  41: 41N__3d  42: 42N__2p
  43: 43N__2p  44: 44N__2p  45: 45N__3d  46: 46N__3d

bfn_to_center map(*), i:map(i)
   1:  1   2:  1   3:  1   4:  1   5:  1   6:  1   7:  1   8:  1   9:  1  10:  1
  11:  1  12:  1  13:  1  14:  1  15:  1  16:  1  17:  1  18:  1  19:  1  20:  1
  21:  1  22:  1  23:  1  24:  1  25:  1  26:  1  27:  1  28:  1  29:  1  30:  1
  31:  1  32:  1  33:  1  34:  1  35:  1  36:  1  37:  1  38:  1  39:  1  40:  1
  41:  1  42:  1  43:  1  44:  1  45:  1  46:  1

bfn_to_orbital_type map(*), i:map(i)
   1:  1   2:  1   3:  1   4:  1   5:  2   6:  2   7:  2   8:  4   9:  4  10:  4
  11:  4  12:  4  13:  4  14:  2  15:  2  16:  2  17:  4  18:  4  19:  2  20:  2
  21:  2  22:  4  23:  4  24:  4  25:  4  26:  1  27:  1  28:  1  29:  1  30:  2
  31:  2  32:  2  33:  4  34:  4  35:  4  36:  4  37:  2  38:  2  39:  2  40:  4
  41:  4  42:  2  43:  2  44:  2  45:  4  46:  4


       46 symmetry orbitals,       ag:  11   b1g:   2   b2g:   5   b3g:   5
                                   au:   2   b1u:  11   b2u:   5   b3u:   5
 !timer: syminp required                 cpu_time=     0.005 walltime=     0.033

 socfpd: mcxu=    30136 mcxu2=    25396 left=222792264
 !timer: socfpd required                 cpu_time=     0.001 walltime=     0.001
 
oneint:   162 S1(*)    integrals were written in  1 records.
oneint:   162 T1(*)    integrals were written in  1 records.
oneint:   162 V1(*)    integrals were written in  1 records.
oneint:   130 X(*)     integrals were written in  1 records.
oneint:   130 Y(*)     integrals were written in  1 records.
oneint:   139 Z(*)     integrals were written in  1 records.
oneint:   130 Im(px)   integrals were written in  1 records.
oneint:   130 Im(py)   integrals were written in  1 records.
oneint:   139 Im(pz)   integrals were written in  1 records.
oneint:   130 Im(lx)   integrals were written in  1 records.
oneint:   130 Im(ly)   integrals were written in  1 records.
oneint:    58 Im(lz)   integrals were written in  1 records.
oneint:   198 XX(*)    integrals were written in  1 records.
oneint:    86 XY(*)    integrals were written in  1 records.
oneint:   130 XZ(*)    integrals were written in  1 records.
oneint:   198 YY(*)    integrals were written in  1 records.
oneint:   130 YZ(*)    integrals were written in  1 records.
oneint:   162 ZZ(*)    integrals were written in  1 records.
 
 !timer: oneint required                 cpu_time=     0.004 walltime=     0.040
 !timer: seg1mn required                 cpu_time=     0.011 walltime=     0.074

twoint:       69839 1/r12    integrals and       15 pk flags
                                 were written in    26 records.

 twoint: maximum mblu needed =     40198
 !timer: twoint required                 cpu_time=     0.144 walltime=     0.145
 
driver: 1-e  integral workspace high-water mark =     52047
driver: 2-e  integral workspace high-water mark =     91793
driver: overall argos workspace high-water mark =     91793
 !timer: argos required                  cpu_time=     0.155 walltime=     0.219
